﻿/// <copyright file="FakeSelectStatementProvider.cs" company="MeridianLink">
///     Copyright (c) MeridianLink. All rights reserved.
/// </copyright>
/// <summary>
/// Author: Michael Leinweaver
/// Date:   8/12/2016
/// Copied: 4/12/2018 from LendOSolnTest to LendOSolnTestDevs
/// </summary>
namespace LendingQB.Test.Developers.Fakes
{
    using System.Collections;
    using System.Collections.Generic;
    using global::DataAccess;

    /// <summary>
    /// Provides a fake for a loan's select statement generator.
    /// </summary>
    internal class FakeSelectStatementProvider : ISelectStatementProvider
    {
        /// <summary>
        /// Gets the value for the fields used by the generator.
        /// </summary>
        /// <value>
        /// An empty list.
        /// </value>
        public IEnumerable<string> FieldSet
        {
            get { return new string[0]; }
        }

        /// <summary>
        /// Gets the initial input fields for the select provider.
        /// </summary>
        public IEnumerable InputFieldList
        {
            get { return new string[0]; }
        }

        /// <summary>
        /// Generates an item for creating SQL select statement using <see cref="FieldSet"/>
        /// and <paramref name="additionalInputFields"/>.
        /// </summary>
        /// <param name="additionalInputFields">
        /// Any additional fields used to generate the select statement.
        /// </param>
        /// <returns>
        /// An empty select statement item.
        /// </returns>
        public CSelectStatementProvider.SqlSelectStatementItem GenerateSelectStatement(HashSet<string> additionalInputFields)
        {
            return new CSelectStatementProvider.SqlSelectStatementItem()
            {
                AppSelectFormat = string.Empty,
                AssignedEmployeeSelectFormat = string.Empty,
                AssignedTeamSelectFormat = string.Empty,
                SubmissionSnapshotSelectFormat = string.Empty,
                LoanSelectFormat = string.Empty,
                Query_Branch = string.Empty,
                Query_LoanFileA = string.Empty,
                Query_LoanFileB = string.Empty,
                Query_LoanFileC = string.Empty,
                Query_LoanFileD = string.Empty,
                Query_LoanFileE = string.Empty,
                Query_LoanFileF = string.Empty,
                Query_TrustedAccount = string.Empty
            };
        }
    }
}
