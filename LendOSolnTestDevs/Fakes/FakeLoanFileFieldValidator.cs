﻿/// <copyright file="FakeLoanFileFieldValidator.cs" company="MeridianLink">
///     Copyright (c) MeridianLink. All rights reserved.
/// </copyright>
/// <summary>
/// Author: Michael Leinweaver
/// Date:   8/12/2016
/// Copied: 4/12/2018 from LendOSolnTest to LendOSolnTestDevs
/// </summary>
namespace LendingQB.Test.Developers.Fakes
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using global::DataAccess;

    /// <summary>
    /// Provides a fake for a loan file field validator.
    /// </summary>
    internal class FakeLoanFileFieldValidator : ILoanFileFieldValidator
    {
        /// <summary>
        /// Gets a value indicating whether the validator permits
        /// saving.
        /// </summary>
        /// <value>
        /// True, as a fake loan can always save.
        /// </value>
        public bool CanSave
        {
            get { return true; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the validator requires
        /// collections to be checked.
        /// </summary>
        /// <value>
        /// False, as a fake loan does not need to have its collections checked.
        /// </value>
        public bool CheckCollection
        {
            get { return false; }
            set { }
        }

        /// <summary>
        /// Gets a value indicating whether the user has field write permissions.
        /// </summary>
        /// <value>
        /// True, as a fake loan can always have its fields written to.
        /// </value>
        public bool HasFieldWritePermissions
        {
            get { return true; }
        }

        /// <summary>
        /// Gets a value indicating whether the validator permits
        /// full write access.
        /// </summary>
        /// <value>
        /// True, as a fake loan can always have fields written to.
        /// </value>
        public bool HasFullWrite
        {
            get { return true; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the validator is disabled.
        /// </summary>
        /// <value>
        /// True, as the validator does not validate any field writes to a fake loan.
        /// </value>
        public bool IsDisabled
        {
            get { return true; }
            set { }
        }

        /// <summary>
        /// Gets a value indicating whether the validator is currently
        /// performing validation.
        /// </summary>
        /// <value>
        /// False, as the validator does not validate any field writes to a fake loan.
        /// </value>
        public bool IsDoingValidation
        {
            get { return false; }
        }

        /// <summary>
        /// Checks protected fields against a list of changes.
        /// </summary>
        /// <param name="fieldChanges">
        /// The <see cref="IEnumerable{T}"/> containing the list of changes.
        /// </param>
        /// <remarks>
        /// This method does not perform any actual validation checks.
        /// </remarks>
        public void CheckProtectedFields(IEnumerable<Tuple<string, FieldOriginalCurrentItem>> fieldChanges)
        {
        }

        /// <summary>
        /// Obtains the <see cref="LoanFieldWritePermissionDenied"/> exception
        /// based on the result of validation, if any.
        /// </summary>
        /// <returns>
        /// The exception encountered when performing validation.
        /// </returns>
        /// <remarks>
        /// This method has purposefully not been implemented.
        /// </remarks>
        public LoanFieldWritePermissionDenied GetException()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Obtains the list of protected fields for the validator.
        /// </summary>
        /// <returns>
        /// An empty list.
        /// </returns>
        public IEnumerable<string> GetProtectedFields()
        {
            return new string[0];
        }

        /// <summary>
        /// Obtains the list of field protect rules for the validator.
        /// </summary>
        /// <returns>
        /// The list of protect field rules.
        /// </returns>
        public IEnumerable<ProtectFieldRule> GetProtectFieldRules()
        {
            return Enumerable.Empty<ProtectFieldRule>();
        }

        /// <summary>
        /// Obtains the list of field write rules for the validator.
        /// </summary>
        /// <returns>
        /// The list of field write rules.
        /// </returns>
        public IEnumerable<WriteFieldRule> GetWriteFieldRules()
        {
            return Enumerable.Empty<WriteFieldRule>();
        }

        /// <summary>
        /// Obtains a list of <see cref="ProtectFieldRule"/> instances
        /// from the specified <paramref name="fieldId"/>.
        /// </summary>
        /// <param name="fieldId">
        /// The id of the field.
        /// </param>
        /// <returns>
        /// An empty list.
        /// </returns>
        public IEnumerable<ProtectFieldRule> GetProtectionFieldRule(string fieldId)
        {
            return new ProtectFieldRule[0];
        }

        /// <summary>
        /// Obtains the <see cref="WriteFieldRule"/> for the specified 
        /// <paramref name="fieldId"/>.
        /// </summary>
        /// <param name="fieldId">
        /// The id of the field.
        /// </param>
        /// <returns>
        /// An empty <see cref="WriteFieldRule"/> corresponding to the field.
        /// </returns>
        public WriteFieldRule GetWriteFieldRule(string fieldId)
        {
            return new WriteFieldRule(fieldId);
        }

        /// <summary>
        /// Writes a new value to the specified <paramref name="fieldId"/>.
        /// </summary>
        /// <typeparam name="T">
        /// The type of the field.
        /// </typeparam>
        /// <param name="fieldId">
        /// The id of the field.
        /// </param>
        /// <param name="getter">
        /// The getter for the field.
        /// </param>
        /// <param name="setter">
        /// The setter for the field.
        /// </param>
        /// <param name="newValue">
        /// The new value to be stored for the field.
        /// </param>
        public void TryToWriteField<T>(string fieldId, Func<T> getter, Action<T> setter, T newValue)
        {
            setter(newValue);
        }

        /// <summary>
        /// Writes a new value to the specified <paramref name="fieldId"/>. A
        /// parameter specified the <see cref="IEqualityComparer{T}"/> to use.
        /// </summary>
        /// <typeparam name="T">
        /// The type of the field.
        /// </typeparam>
        /// <param name="fieldId">
        /// The id of the field.
        /// </param>
        /// <param name="getter">
        /// The getter for the field.
        /// </param>
        /// <param name="setter">
        /// The setter for the field.
        /// </param>
        /// <param name="newValue">
        /// The new value to be stored for the field.
        /// </param>
        /// <param name="comparer">
        /// The <see cref="IEqualityComparer{T}"/> to use when determining if the field
        /// value has changed.
        /// </param>
        public void TryToWriteField<T>(string fieldId, Func<T> getter, Action<T> setter, T newValue, IEqualityComparer<T> comparer)
        {
            setter(newValue);
        }

        /// <summary>
        /// Validates the update to the specified <paramref name="originalDataSet"/>.
        /// </summary>
        /// <param name="collectionName">
        /// The name of the collection.
        /// </param>
        /// <param name="originalDataSet">
        /// The original values for the collection.
        /// </param>
        /// <param name="updatedDataSet">
        /// The new values for the collection.
        /// </param>
        /// <remarks>
        /// This method does not perform any actual validation checks.
        /// </remarks>
        public void UpdateCollection(string collectionName, DataSet originalDataSet, DataSet updatedDataSet)
        {
        }

        /// <summary>
        /// Validates the updated values for the closing cost set json.
        /// </summary>
        /// <param name="brokerId">
        /// The Broker ID.
        /// </param>
        /// <param name="collectionName">
        /// The name of the collection.
        /// </param>
        /// <param name="originalJson">
        /// The original content for the collection.
        /// </param>
        /// <param name="currentJson">
        /// The updated content for the collection.
        /// </param>
        /// <remarks>
        /// This method does not perform any actual validation checks.
        /// </remarks>
        public void ValidateClosingCostSet(Guid brokerId, string collectionName, string originalJson, string currentJson)
        {
        }

        /// <summary>
        /// Validates the updated values for the closing disclosure date json.
        /// </summary>
        /// <param name="brokerId">
        /// The Broker ID.
        /// </param>
        /// <param name="originalJson">
        /// The original content for the collection.
        /// </param>
        /// <param name="currentJson">
        /// The updated content for the collection.
        /// </param>
        /// <remarks>
        /// This method does not perform any actual validation checks.
        /// </remarks>
        public void ValidateClosingDisclosureDates(Guid brokerId, string originalJson, string currentJson)
        {
        }

        /// <summary>
        /// Validates the updated values for the housing expenses json.
        /// </summary>
        /// <param name="originalJson">
        /// The original content for the collection.
        /// </param>
        /// <param name="currentJson">
        /// The updated content for the collection.
        /// </param>
        /// <remarks>
        /// This method does not perform any actual validation checks.
        /// </remarks>
        public void ValidateHousingExpenses(string originalJson, string currentJson)
        {
        }

        /// <summary>
        /// Validates the updated values for the loan estimate date json.
        /// </summary>
        /// <param name="brokerId">
        /// The Broker ID.
        /// </param>
        /// <param name="originalJson">
        /// The original content for the collection.
        /// </param>
        /// <param name="currentJson">
        /// The updated content for the collection.
        /// </param>
        /// <remarks>
        /// This method does not perform any actual validation checks.
        /// </remarks>
        public void ValidateLoanDateInfos(Guid brokerId, string originalJson, string currentJson)
        {
        }

        /// <summary>
        /// Validates the changes to the selected product code filter.
        /// </summary>
        /// <param name="collectionName">The collection name.</param>
        /// <param name="originalCodes">The original codes.</param>
        /// <param name="currentCodes">The new codes.</param>
        public void ValidateProductCodeFilters(string collectionName, Dictionary<string, bool> originalCodes, Dictionary<string, bool> currentCodes)
        {
        }

        public bool CanChangeLoanStatus(E_sStatusT newStatus)
        {
            return true;
        }
    }
}
