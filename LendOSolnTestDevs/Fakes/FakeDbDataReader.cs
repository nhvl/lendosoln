﻿namespace LendingQB.Test.Developers.Fakes
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Data.Common;
    using System.Linq;

    public sealed class FakeDbDataReader : DbDataReader
    {
        private FakeDataSet dataSet;
        private FakeDataTable currentTable;
        private FakeDataRow currentRow;

        public FakeDbDataReader(FakeDataSet dataSet)
        {
            this.dataSet = dataSet;
            this.currentTable = this.dataSet.Next();
        }

        public override object this[int ordinal] => this.currentRow[ordinal];

        public override object this[string name] => this.currentRow[name];

        public override int Depth => this.dataSet.TableCount;

        public override int FieldCount => this.currentTable.FieldCount;

        public override bool HasRows => this.dataSet.HasRows;

        public override bool IsClosed => this.dataSet.IsClosed;

        public override int RecordsAffected => 0;

        public override bool GetBoolean(int ordinal)
        {
            return Convert.ToBoolean(this.currentRow[ordinal]);
        }

        public override byte GetByte(int ordinal)
        {
            return Convert.ToByte(this.currentRow[ordinal]);
        }

        public override long GetBytes(int ordinal, long dataOffset, byte[] buffer, int bufferOffset, int length)
        {
            var data = (byte[])this.currentRow[ordinal];
            Array.Copy(data, dataOffset, buffer, bufferOffset, length);
            return length;
        }

        public override char GetChar(int ordinal)
        {
            return Convert.ToChar(this.currentRow[ordinal]);
        }

        public override long GetChars(int ordinal, long dataOffset, char[] buffer, int bufferOffset, int length)
        {
            var data = (char[])this.currentRow[ordinal];
            Array.Copy(data, dataOffset, buffer, bufferOffset, length);
            return length;
        }

        public override string GetDataTypeName(int ordinal)
        {
            throw new NotImplementedException();
        }

        public override DateTime GetDateTime(int ordinal)
        {
            return Convert.ToDateTime(this.currentRow[ordinal]);
        }

        public override decimal GetDecimal(int ordinal)
        {
            return Convert.ToDecimal(this.currentRow[ordinal]);
        }

        public override double GetDouble(int ordinal)
        {
            return Convert.ToDouble(this.currentRow[ordinal]);
        }

        public override IEnumerator GetEnumerator()
        {
            yield return this.currentTable.Next();
        }

        public override Type GetFieldType(int ordinal)
        {
            throw new NotImplementedException();
        }

        public override float GetFloat(int ordinal)
        {
            return Convert.ToSingle(this.currentRow[ordinal]);
        }

        public override Guid GetGuid(int ordinal)
        {
            var datum = this.currentRow[ordinal];
            return datum is DBNull ? Guid.Empty : (Guid)datum;
        }

        public override short GetInt16(int ordinal)
        {
            return Convert.ToInt16(this.currentRow[ordinal]);
        }

        public override int GetInt32(int ordinal)
        {
            return Convert.ToInt32(this.currentRow[ordinal]);
        }

        public override long GetInt64(int ordinal)
        {
            return Convert.ToInt64(this.currentRow[ordinal]);
        }

        public override string GetName(int ordinal)
        {
            return this.currentTable.GetColumnName(ordinal);
        }

        public override int GetOrdinal(string name)
        {
            return this.currentTable.GetIndex(name);
        }

        public override string GetString(int ordinal)
        {
            return Convert.ToString(this.currentRow[ordinal]);
        }

        public override object GetValue(int ordinal)
        {
            return this.currentRow[ordinal];
        }

        public override int GetValues(object[] values)
        {
            int len = Math.Min(values.Length, this.currentTable.FieldCount);
            for (int i=0; i<len; ++i)
            {
                values[i] = this.currentRow[i];
            }

            return len;
        }

        public override bool IsDBNull(int ordinal)
        {
            return Convert.IsDBNull(this.currentRow[ordinal]);
        }

        public override bool NextResult()
        {
            this.currentRow = null;
            this.currentTable = this.dataSet.Next();
            return this.currentTable != null;
        }

        public override bool Read()
        {
            this.currentRow = this.currentTable.Next();
            return this.currentRow != null;
        }

        public sealed class FakeDataRow
        {
            private Dictionary<string, int> columnIndices;
            private object[] cells;

            public FakeDataRow(object[] cells, Dictionary<string, int> columnIndices)
            {
                this.cells = cells;
                this.columnIndices = columnIndices;
            }

            public object this[int index]
            {
                get { return this.cells[index]; }
            }

            public object this[string columnName]
            {
                get
                {
                    int index = this.columnIndices[columnName];
                    return this.cells[index];
                }
            }
        }

        public sealed class FakeDataTable
        {
            private Dictionary<string, int> columnIndices;
            private List<FakeDataRow> rows;
            private int currentIndex = -1;

            private object[] currentRow;

            public FakeDataTable(string[] columnNames)
            {
                this.columnIndices = new Dictionary<string, int>();
                for (int i=0; i<columnNames.Length; ++i)
                {
                    this.columnIndices[columnNames[i]] = i;
                }

                this.rows = new List<FakeDataRow>();
            }

            public int FieldCount { get { return this.columnIndices.Keys.Count; } }

            public bool HasRows { get { return this.rows.Count > 0; } }

            public string GetColumnName(int index)
            {
                foreach (var key in this.columnIndices.Keys)
                {
                    if (this.columnIndices[key] == index) return key;
                }

                return null;
            }

            public int GetIndex(string columnName)
            {
                return this.columnIndices[columnName];
            }

            public void BeginRow()
            {
                if (this.currentIndex < 0)
                {
                    this.currentRow = new object[this.columnIndices.Keys.Count];
                }
            }

            public void EndRow()
            {
                if (this.currentIndex < 0)
                {
                    this.rows.Add(new FakeDataRow(this.currentRow, this.columnIndices));
                    this.currentRow = null;
                }
            }

            public void AddCellValue(string name, object value)
            {
                if (this.currentIndex < 0)
                {
                    int index = this.columnIndices[name];
                    this.currentRow[index] = value;
                }
            }

            public FakeDataRow Next()
            {
                if (this.currentIndex >= this.rows.Count)
                {
                    return null;
                }

                this.currentIndex++;
                if (this.currentIndex >= this.rows.Count)
                {
                    return null;
                }
                else
                {
                    return this.rows[this.currentIndex];
                }
            }
        }

        public sealed class FakeDataSet
        {
            private List<FakeDataTable> tables;
            private int currentIndex;

            public FakeDataSet()
            {
                this.tables = new List<FakeDataTable>();
                this.currentIndex = -1;
            }

            public int TableCount { get { return this.tables.Count; } }

            public bool HasRows { get { return this.tables.Any(t => t.HasRows); } }

            public bool IsClosed { get { return this.currentIndex >= this.tables.Count; } }

            public FakeDataTable AddNewTable(params string[] columnNames)
            {
                if (this.currentIndex < 0)
                {
                    var table = new FakeDataTable(columnNames);
                    this.tables.Add(table);
                    return table;
                }
                else
                {
                    return null;
                }
            }

            public FakeDataTable Next()
            {
                if (this.IsClosed)
                {
                    return null;
                }

                this.currentIndex++;
                if (this.IsClosed)
                {
                    return null;
                }
                else
                {
                    return this.tables[this.currentIndex];
                }
            }
        }
    }
}
