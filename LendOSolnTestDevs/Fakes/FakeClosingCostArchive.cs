﻿namespace LendingQB.Test.Developers.Fakes
{
    using System;
    using System.Data;
    using System.Xml;
    using System.Xml.Schema;
    using DataAccess;
    using global::DataAccess;
    using LendersOffice.Common.SerializationTypes;

    internal class FakeClosingCostArchive : IClosingCostArchive
    {
        private decimal apr;

        public FakeClosingCostArchive(decimal apr)
        {
            this.apr = apr;
        }

        public decimal? Apr => apr;

        public ClosingCostArchive.E_ClosingCostArchiveType ClosingCostArchiveType
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public string DateArchived
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public int FileDBContentVersion
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public string FileDBKey
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public Guid? LinkedArchiveId
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public Guid Id
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public bool IsDisclosed
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public bool IsForCoCProcess
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public ClosingCostArchive.E_ClosingCostArchiveSource Source
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public ClosingCostArchive.E_ClosingCostArchiveStatus Status
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public bool StoreFieldsInline
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public bool WasBasisArchive
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public bool UsesLiveData
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public bool WasSourcedFromPendingCoC
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public ClosingCostArchive Duplicate()
        {
            throw new NotImplementedException();
        }

        public void ExtractFromLoan(CPageData dataLoan)
        {
            throw new NotImplementedException();
        }

        public void ExtractFromLoan(Guid loanId)
        {
            throw new NotImplementedException();
        }

        public void FlushToFileDBIfDirty()
        {
            throw new NotImplementedException();
        }

        public bool GetBitValue(string fieldId)
        {
            throw new NotImplementedException();
        }

        public bool GetBitValue(string fieldId, bool defaultValue)
        {
            throw new NotImplementedException();
        }

        public DataSet GetCachedAgentDataSet()
        {
            throw new NotImplementedException();
        }

        public CDateTime GetCDateTimeValue(string fieldId)
        {
            throw new NotImplementedException();
        }

        public BorrowerClosingCostSet GetClosingCostSet(LosConvert converter = null)
        {
            throw new NotImplementedException();
        }

        public int GetCountValue(string fieldId)
        {
            throw new NotImplementedException();
        }

        public DateTime GetDateTimeValue(string fieldId)
        {
            throw new NotImplementedException();
        }

        public Guid GetGuidValue(string fieldId)
        {
            throw new NotImplementedException();
        }

        public decimal GetMoneyValue(string fieldId)
        {
            throw new NotImplementedException();
        }

        public decimal GetRateValue(string fieldId)
        {
            if (fieldId == nameof(CPageBase.sApr))
            {
                return this.apr;
            }

            throw new NotImplementedException();
        }

        public XmlSchema GetSchema()
        {
            throw new NotImplementedException();
        }

        public string GetValue(string fieldId)
        {
            throw new NotImplementedException();
        }

        public bool HasValue(string fieldId)
        {
            throw new NotImplementedException();
        }

        public void OverrideValue(string fieldId, string newValue)
        {
            throw new NotImplementedException();
        }

        public void ReadXml(XmlReader reader)
        {
            throw new NotImplementedException();
        }

        public void RemoveValue(string fieldId)
        {
            throw new NotImplementedException();
        }

        public void SetId(Guid id)
        {
            throw new NotImplementedException();
        }

        public void UpdateArchiveWithLatestLoanDataForCoC(CPageData dataLoan)
        {
            throw new NotImplementedException();
        }

        public void WriteXml(XmlWriter writer)
        {
            throw new NotImplementedException();
        }
    }
}
