﻿using LqbGrammar;
using LqbGrammar.Exceptions;
using NUnit.Framework;

/// <summary>
/// This code will be executed once prior to any other unit test code because it is a setup
/// fixture in the global namespace.
/// </summary>
[SetUpFixture]
public class TestSuiteSetup
{
    /// <summary>
    /// DB ad hoc queries pass through the FOOL architectural framework, so factories need to be registered.
    /// We don't want to register live code as then test may accidentally communicate with the external world.
    /// Instead we'll only initialize the application and defer to the test methods to register whatever 
    /// mocks they need.  If they miss any then an exception will be thrown whenever some class attempts to 
    /// retrieve a needed driver implementation.
    /// </summary>
    [SetUp]
    public void InitializeApplicationFramework()
    {
        const string AppName = "LendOSolnTestDev";

        // This library is only called by test applications, never by LUnit.
        // NOTE: Although this is supposed to be called once, in fact it is getting called multiple times
        //       so the code has to be careful to only perform the application initialization the first time.
        //       LqbApplication.CreateInitializer already returns null after the first call so we can use that.

        IExceptionHandlerFactory[] arrHandlers = ExceptionHandlerUtil.GetCoreExceptionHandlerFactories();

        IApplicationInitialize iAppInit = null;
        try
        {
            iAppInit = LqbApplication.CreateInitializer(ApplicationType.UnitTest, arrHandlers);
            iAppInit?.SetName(AppName);
        }
        finally
        {
            iAppInit?.Dispose();
        }
    }
}
