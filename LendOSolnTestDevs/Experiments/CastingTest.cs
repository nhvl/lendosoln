﻿namespace LendingQB.Test.Developers.Experiments
{
    using System;
    using NUnit.Framework;

    public abstract class BaseClass { }

    public interface IWantToTestCasting { }

    public sealed class ConcreteClass : BaseClass, IWantToTestCasting { }

    public sealed class AnotherConcreteClass : BaseClass { }

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public class CastingTest
    {
        [Test]
        public void DirectCast()
        {
            var concrete = new ConcreteClass();
            var another = new AnotherConcreteClass();

            // Simple cast should work due to inheritance.
            var base1 = (BaseClass)concrete;
            var base2 = (BaseClass)another;
            Assert.IsNotNull(base1);
            Assert.IsNotNull(base2);

            // Simple cast should work due to inheritance.
            var intf = (IWantToTestCasting)concrete;
            Assert.IsNotNull(intf);

            // Casting from interface to concrete logically shouldn't work,
            // but MIGHT if the casting is done at runtime.
            var recoverConcrete = this.AttemptConcreteCastOnInterface(intf);
            Assert.IsNotNull(recoverConcrete);

            // Casting from interface to base class logically shouldn't work,
            // but MIGHT if the casting is done at runtime.
            var recoverBase = this.AttemptBaseCastOnInterface(intf);
            Assert.IsNotNull(recoverBase);

            // Casting from base class to interface logically shouldn't work,
            // but MIGHT if the casting is done at runtime.
            var tricky1 = this.AttemptInterfaceCastOnBase(base1);
            Assert.IsNotNull(tricky1);

            try
            {
                // Casting shouldn't work here, and the framework should throw an invalid case exception.
                var tricky2 = this.AttemptInterfaceCastOnBase(base2);
                Assert.Fail();
            }
            catch (InvalidCastException)
            {
                Assert.IsTrue(true);
            }
        }

        [Test]
        public void IndirectCast()
        {
            var concrete = new ConcreteClass();
            var another = new AnotherConcreteClass();

            // Simple cast should work due to inheritance.
            var base1 = concrete as BaseClass;
            var base2 = another as BaseClass;
            Assert.IsNotNull(base1);
            Assert.IsNotNull(base2);

            // Simple cast should work due to inheritance.
            var intf = concrete as IWantToTestCasting;
            Assert.IsNotNull(intf);

            // Dynamic casting from interface to concrete class should work.
            var recoverConcrete = intf as ConcreteClass;
            Assert.IsNotNull(recoverConcrete);

            // Dynamic casting from interface to base class should work.
            var recoverBase = intf as BaseClass;
            Assert.IsNotNull(recoverBase);

            // Dynamic casting from base class to interface should work.
            var tricky1 = base1 as IWantToTestCasting;
            Assert.IsNotNull(tricky1);

            // Dynamic casting from base class to interface should return null.
            var tricky2 = base2 as IWantToTestCasting;
            Assert.IsNull(tricky2);
        }

        [Test]
        public void ParseNumericViaConversionTest()
        {
            // Note: this sort of thing doesn't work with Guids

            int integer = 546;
            string identifier = integer.ToString();

            int recovered = (int)Convert.ChangeType(identifier, typeof(int));
            Assert.AreEqual(integer, recovered);
        }

        // Private method doesn't know from which concrete type the input was cast.
        private IWantToTestCasting AttemptInterfaceCastOnBase(BaseClass test)
        {
            return (IWantToTestCasting)test;
        }

        // Private method doesn't know from which concrete class the interface originated.
        private ConcreteClass AttemptConcreteCastOnInterface(IWantToTestCasting test)
        {
            return (ConcreteClass)test;
        }

        // Private method doesn't know from which concrete class the interface originated.
        private BaseClass AttemptBaseCastOnInterface(IWantToTestCasting test)
        {
            return (BaseClass)test;
        }
    }
}
