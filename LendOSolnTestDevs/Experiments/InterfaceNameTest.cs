﻿namespace LendingQB.Test.Developers.Experiments
{
    using NUnit.Framework;

    // Analogue of IReadOnlyOrderedLqbCollection
    public interface IToExpose
    {
        int ExposeCode { get; }
    }

    // Analogue of IMutableOrderedLqbCollection
    internal interface IKeepHidden : IToExpose
    {
        int HiddenCode { get; }
    }

    // Analogue of ILoanLqbCollectionContainer which 
    // would be the result of an Extract Interface 
    // operation on LoanLqbCollectionContainer
    public interface IClientService
    {
        IToExpose Service { get; }

        int RetrieveHiddenCode();
    }

    /// <summary>
    /// This class is designed to test a design where we can reuse the same
    /// name ('Service' in the TestService class) to return two different
    /// levels of an interface hierarchy, one of which is exposed to client 
    /// code and the other is reserved for internal book keeping.
    /// </summary>
    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public class InterfaceNameTest
    {
        [Test]
        public void UseClientService()
        {
            var source = new TestService() as IClientService;

            int code = source.Service.ExposeCode;

            Assert.AreEqual(1, code);
        }

        [Test]
        public void UseClientMethod()
        {
            var source = new TestService() as IClientService;

            int code = source.RetrieveHiddenCode();

            Assert.AreEqual(-1, code);
        }

        // Analogue of the LoanLqbCollectionContainer class
        private sealed class TestService : IClientService
        {
            public TestService()
            {
                this.Service = new ServiceImpl(); // quick and dirty method to instantiate the analogue of a collection
            }

            // Analog of the property that returns a collection to client code
            IToExpose IClientService.Service
            {
                get
                {
                    return this.Service;
                }
            }

            // Analog of the private property to get the mutable collection
            private IKeepHidden Service { get; set; }

            // Analog of methods exposed to clients that make use of private members of the LoanLqbCollectionContainer class.
            int IClientService.RetrieveHiddenCode()
            {
                return this.Service.HiddenCode;
            }
        }

        // Analogue of the OrderedLqbCollection class.
        private sealed class ServiceImpl : IKeepHidden
        {
            public int ExposeCode
            {
                get { return 1; }
            }

            public int HiddenCode
            {
                get { return -1; }
            }
        }
    }
}
