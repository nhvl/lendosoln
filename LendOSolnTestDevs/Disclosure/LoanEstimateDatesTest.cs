﻿namespace LendingQB.Test.Developers.Disclosure
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Core;
    using Core.Data;
    using Fakes;
    using global::DataAccess;
    using LendersOffice.Common.SerializationTypes;
    using LqbGrammar.DataTypes;
    using NSubstitute;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public class LoanEstimateDatesTest
    {
        private readonly GuidDataObjectIdentifierFactory<DataObjectKind.Consumer> consumerIdFactory = 
            new GuidDataObjectIdentifierFactory<DataObjectKind.Consumer>();

        [Test]
        public void DisclosedApr_Locked_ReturnsLockValue()
        {
            var le = LoanEstimateDates.Create(metadata: null);

            le.DisclosedApr = 5;
            le.DisclosedAprLckd = true;

            Assert.AreEqual(5, le.DisclosedApr);
        }

        [Test]
        public void DisclosedApr_WithDocVendorAprNotLocked_ReturnsDocVendorApr()
        {
            var le = LoanEstimateDates.Create(metadata: null);

            le.DisclosedApr = 5;
            le.DocVendorApr = 4;

            Assert.AreEqual(4, le.DisclosedApr);
        }

        [Test]
        public void DisclosedApr_NoDocVendorNotLocked_ReturnsArchiveValue()
        {
            var archive = this.GetFakeClosingCostArchiveWithApr(3);
            var metadata = new LoanEstimateDatesMetadata(id => archive);
            
            var le = LoanEstimateDates.Create(metadata);
            le.ArchiveId = Guid.NewGuid();

            le.DisclosedApr = 5;

            Assert.AreEqual(3, le.DisclosedApr);
        }

        [Test]
        public void LqbApr_NoAssociatedArchive_ReturnsNull()
        {
            var le = LoanEstimateDates.Create(metadata: null);

            le.ArchiveId = Guid.Empty;

            Assert.AreEqual(null, le.LqbApr);
        }

        [Test]
        public void LqbApr_WithAssociatedArchive_ReturnsArchivedApr()
        {
            var archive = this.GetFakeClosingCostArchiveWithApr(5);
            var metadata = new LoanEstimateDatesMetadata(id => archive);

            var le = LoanEstimateDates.Create(metadata);
            le.ArchiveId = Guid.NewGuid();

            Assert.AreEqual(5, le.LqbApr);
        }

        [Test]
        public void DuplicatePreservingId_Always_PreservesId()
        {
            var le = LoanEstimateDates.Create(metadata: null);

            var duplicate = le.DuplicatePreservingId();

            Assert.AreEqual(le.UniqueId, duplicate.UniqueId);
        }

        [Test]
        public void DuplicateAssigningNewId_Always_AssignsNewId()
        {
            var le = LoanEstimateDates.Create(metadata: null);

            var duplicate = le.DuplicateAssigningNewId(duplicateGetArchiveById: null);

            Assert.AreNotEqual(le.UniqueId, duplicate.UniqueId);
        }

        [Test]
        public void Duplicate_Always_CopiesDisclosedAprLckd()
        {
            var le = LoanEstimateDates.Create(metadata: null);
            le.DisclosedAprLckd = true;

            var duplicate = le.DuplicatePreservingId();

            Assert.AreEqual(true, duplicate.DisclosedAprLckd);
        }

        [Test]
        public void Duplicate_Always_CopiesDisclosedApr()
        {
            var le = LoanEstimateDates.Create(metadata: null);
            // Lock value to ensure we use the backing field and don't calculate it.
            le.DisclosedAprLckd = true;
            le.DisclosedApr = 5;

            var duplicate = le.DuplicatePreservingId();

            Assert.AreEqual(5m, duplicate.DisclosedApr);
        }

        [Test]
        public void Duplicate_Always_CopiesDocVendorApr()
        {
            var le = LoanEstimateDates.Create(metadata: null);
            le.DocVendorApr = 5;

            var duplicate = le.DuplicatePreservingId();

            Assert.AreEqual(5m, duplicate.DocVendorApr);
        }

        [Test]
        public void DuplicatePreservingId_Always_CopiesLqbApr()
        {
            var archive = this.GetFakeClosingCostArchiveWithApr(5);
            var metadata = new LoanEstimateDatesMetadata(id => archive);

            var le = LoanEstimateDates.Create(metadata);
            le.ArchiveId = Guid.NewGuid();

            var duplicate = le.DuplicatePreservingId();

            Assert.AreEqual(5m, duplicate.LqbApr);
        }

        [Test]
        public void DuplicateAssigningNewId_Always_ReturnsAprOfNewArchive()
        {
            var originalArchive = this.GetFakeClosingCostArchiveWithApr(5);
            var metadata = new LoanEstimateDatesMetadata(id => originalArchive);

            var le = LoanEstimateDates.Create(metadata);
            le.ArchiveId = Guid.NewGuid();

            var newArchive = this.GetFakeClosingCostArchiveWithApr(3);
            var duplicate = le.DuplicateAssigningNewId(duplicateGetArchiveById: id => newArchive);

            Assert.AreEqual(3m, duplicate.LqbApr);
        }

        [Test]
        public void IsEqual_DifferentDisclosedAprLckd_ReturnsFalse()
        {
            var first = LoanEstimateDates.Create(metadata: null);
            var second = LoanEstimateDates.Create(metadata: null);

            first.DisclosedAprLckd = true;
            second.DisclosedAprLckd = false;

            Assert.AreEqual(false, first.Equals(second));
        }

        [Test]
        public void IsEqual_SameDisclosedAprLckd_ReturnsTrue()
        {
            var first = LoanEstimateDates.Create(metadata: null);
            var second = LoanEstimateDates.Create(metadata: null);

            first.DisclosedAprLckd = true;
            second.DisclosedAprLckd = true;

            Assert.AreEqual(true, first.IsEqual(second));
        }

        [Test]
        public void IsEqual_DifferentDisclosedApr_ReturnsFalse()
        {
            var first = LoanEstimateDates.Create(metadata: null);
            var second = LoanEstimateDates.Create(metadata: null);

            first.DisclosedAprLckd = true;
            first.DisclosedApr = 5;
            second.DisclosedAprLckd = true;
            second.DisclosedApr = 3;

            Assert.AreEqual(false, first.IsEqual(second));
        }

        [Test]
        public void IsEqual_SameDisclosedApr_ReturnsTrue()
        {
            var first = LoanEstimateDates.Create(metadata: null);
            var second = LoanEstimateDates.Create(metadata: null);

            first.DisclosedAprLckd = true;
            first.DisclosedApr = 5;
            second.DisclosedAprLckd = true;
            second.DisclosedApr = 5;

            Assert.AreEqual(true, first.IsEqual(second));
        }

        [Test]
        public void IsEqual_DifferentDocVendorApr_ReturnsFalse()
        {
            var first = LoanEstimateDates.Create(metadata: null);
            var second = LoanEstimateDates.Create(metadata: null);

            first.DocVendorApr = 5;
            second.DocVendorApr = 3;

            Assert.AreEqual(false, first.IsEqual(second));
        }

        [Test]
        public void IsEqual_SameDocVendorApr_ReturnsTrue()
        {
            var first = LoanEstimateDates.Create(metadata: null);
            var second = LoanEstimateDates.Create(metadata: null);

            first.DocVendorApr = 5;
            second.DocVendorApr = 5;

            Assert.AreEqual(true, first.IsEqual(second));
        }

        [Test]
        public void IsEqual_DifferentLqbApr_ReturnsFalse()
        {
            var firstArchive = this.GetFakeClosingCostArchiveWithApr(5);
            var firstMetadata = new LoanEstimateDatesMetadata(id => firstArchive);

            var secondArchive = this.GetFakeClosingCostArchiveWithApr(3);
            var secondMetadata = new LoanEstimateDatesMetadata(id => secondArchive);
            
            var first = LoanEstimateDates.Create(firstMetadata);
            first.ArchiveId = new Guid("11111111-1111-1111-1111-111111111111");
            var second = LoanEstimateDates.Create(secondMetadata);
            second.ArchiveId = new Guid("22222222-2222-2222-2222-222222222222");

            Assert.AreEqual(false, first.IsEqual(second));
        }

        [Test]
        public void IsEqual_SameLqbApr_ReturnsTrue()
        {
            var firstArchive = this.GetFakeClosingCostArchiveWithApr(5);
            var firstMetadata = new LoanEstimateDatesMetadata(id => firstArchive);
            
            var secondArchive = this.GetFakeClosingCostArchiveWithApr(5);
            var secondMetadata = new LoanEstimateDatesMetadata(id => secondArchive);
            
            var first = LoanEstimateDates.Create(firstMetadata);
            var second = LoanEstimateDates.Create(secondMetadata);

            Assert.AreEqual(true, first.IsEqual(second));
        }

        [Test]
        public void UpdateConsumerIds_UpdatesIdsAsExpected()
        {
            var originalConsumers = this.ConsumerMetadataByConsumerId();
            var metadata = new LoanEstimateDatesMetadata(null, originalConsumers, LoanRescindableT.Blank);
            
            var dates = LoanEstimateDates.Create(metadata);

            var updatedConsumerId1 = consumerIdFactory.NewId();
            var updatedConsumerId2 = consumerIdFactory.NewId();

            var newIdMappings = new Dictionary<Guid, Guid>
            {
                [originalConsumers[0].ConsumerId.Value] = updatedConsumerId1.Value,
                [originalConsumers[1].ConsumerId.Value] = updatedConsumerId2.Value
            };

            dates.UpdateConsumerIds(newIdMappings);

            Assert.AreEqual(updatedConsumerId1.Value, dates.DisclosureDatesByConsumerId.First().Key);
            Assert.AreEqual(updatedConsumerId2.Value, dates.DisclosureDatesByConsumerId.Last().Key);
        }

        [Test]
        public void IssuedDate_NoBorrowerLevelDatesOnFile_UsesRegulatoryLevelDate(
            [Values(LoanRescindableT.Blank, LoanRescindableT.NotRescindable, LoanRescindableT.Rescindable)] LoanRescindableT rescindableType)
        {
            var archive = this.GetFakeClosingCostArchiveWithApr(5);
            var metadata = new LoanEstimateDatesMetadata(id => archive, null, rescindableType);
            
            var issuedDate = DateTime.Parse("6/1/2018");
            var dates = LoanEstimateDates.Create(metadata);
            dates.IssuedDate = issuedDate;

            Assert.AreEqual(issuedDate, dates.IssuedDate);
        }

        [Test]
        public void IssuedDate_BorrowerLevelDatesOnFile_Locked_UsesRegulatoryLevelIssuedDate(
            [Values(LoanRescindableT.Blank, LoanRescindableT.NotRescindable, LoanRescindableT.Rescindable)] LoanRescindableT rescindableType)
        {
            var archive = this.GetFakeClosingCostArchiveWithApr(5);
            var metadata = new LoanEstimateDatesMetadata(id => archive, this.ConsumerMetadataByConsumerId(), rescindableType);
            
            var regulatoryIssuedDate = DateTime.Parse("6/1/2018");
            var consumer1IssuedDate = DateTime.Parse("6/2/2018");
            var consumer2IssuedDate = DateTime.Parse("6/3/2018");

            var dates = LoanEstimateDates.Create(metadata);
            dates.IssuedDate = regulatoryIssuedDate;
            dates.DisclosureDatesByConsumerId.First().Value.IssuedDate = consumer1IssuedDate;
            dates.DisclosureDatesByConsumerId.Last().Value.IssuedDate = consumer2IssuedDate;

            dates.IssuedDateLckd = true;
            Assert.AreEqual(regulatoryIssuedDate, dates.IssuedDate);
        }

        [Test]
        public void IssuedDate_BorrowerLevelDatesOnFile_Unlocked_UsesCalculation(
            [Values(LoanRescindableT.Blank, LoanRescindableT.NotRescindable, LoanRescindableT.Rescindable)] LoanRescindableT rescindableType,
            [Values(E_aTypeT.TitleOnly, E_aTypeT.NonTitleSpouse, E_aTypeT.CurrentTitleOnly)] E_aTypeT manualBorrowerType)
        {
            var archive = this.GetFakeClosingCostArchiveWithApr(5);
            var metadata = new LoanEstimateDatesMetadata(id => archive, this.ConsumerMetadataByConsumerId(), rescindableType);

            var regulatoryIssuedDate = DateTime.Parse("6/1/2018");
            var consumer1IssuedDate = DateTime.Parse("6/2/2018");
            var consumer2IssuedDate = DateTime.Parse("6/3/2018");

            var dates = LoanEstimateDates.Create(metadata);
            dates.IssuedDate = regulatoryIssuedDate;
            dates.DisclosureDatesByConsumerId.First().Value.IssuedDate = consumer1IssuedDate;
            dates.DisclosureDatesByConsumerId.Last().Value.IssuedDate = consumer2IssuedDate;

            dates.IssuedDateLckd = false;
            Assert.AreEqual(consumer1IssuedDate, dates.IssuedDate);

            var existingConsumers = metadata.ConsumerMetadataByConsumerId;
            existingConsumers[0].ConsumerType = manualBorrowerType;
            metadata = new LoanEstimateDatesMetadata(id => archive, existingConsumers, metadata.LoanRescindableType);

            dates.SetMetadata(metadata);
            dates.IsManual = true;

            Assert.AreEqual(consumer1IssuedDate, dates.IssuedDate);
        }

        [Test]
        public void IssuedDate_BorrowerLevelDatesOnFile_SetMetadata_UpdatesCalculation(
            [Values(E_aTypeT.TitleOnly, E_aTypeT.CurrentTitleOnly)] E_aTypeT consumer1Type)
        {
            var archive = this.GetFakeClosingCostArchiveWithApr(5);
            var originalConsumers = this.ConsumerMetadataByConsumerId();

            var metadata = new LoanEstimateDatesMetadata(id => archive, originalConsumers, LoanRescindableT.Blank);
            
            var regulatoryIssuedDate = DateTime.Parse("6/1/2018");
            var consumer1IssuedDate = DateTime.Parse("6/2/2018");
            var consumer2IssuedDate = DateTime.Parse("6/3/2018");

            var dates = LoanEstimateDates.Create(metadata);
            dates.IssuedDate = regulatoryIssuedDate;
            dates.DisclosureDatesByConsumerId.First().Value.IssuedDate = consumer1IssuedDate;
            dates.DisclosureDatesByConsumerId.Last().Value.IssuedDate = consumer2IssuedDate;

            dates.IssuedDateLckd = false;
            Assert.AreEqual(consumer1IssuedDate, dates.IssuedDate);

            originalConsumers[0].ConsumerType = consumer1Type;

            dates.SetMetadata(new LoanEstimateDatesMetadata(id => archive, originalConsumers, metadata.LoanRescindableType));
            
            Assert.AreEqual(consumer2IssuedDate, dates.IssuedDate);
        }

        [Test]
        public void IssuedDate_BorrowerLevelDatesOnFileWithExclusions_Unlocked_HonorsExclusionWhenManual(
            [Values(LoanRescindableT.Blank, LoanRescindableT.NotRescindable, LoanRescindableT.Rescindable)] LoanRescindableT rescindableType)
        {
            var archive = this.GetFakeClosingCostArchiveWithApr(5);
            var metadata = new LoanEstimateDatesMetadata(id => archive, this.ConsumerMetadataByConsumerId(), rescindableType);
            
            var regulatoryIssuedDate = DateTime.Parse("6/1/2018");
            var consumer1IssuedDate = DateTime.Parse("6/2/2018");
            var consumer2IssuedDate = DateTime.Parse("6/3/2018");

            var dates = LoanEstimateDates.Create(metadata);
            dates.IssuedDate = regulatoryIssuedDate;

            dates.DisclosureDatesByConsumerId.First().Value.IssuedDate = consumer1IssuedDate;
            dates.DisclosureDatesByConsumerId.First().Value.ExcludeFromCalculations = true;

            dates.DisclosureDatesByConsumerId.Last().Value.IssuedDate = consumer2IssuedDate;

            dates.IssuedDateLckd = false;
            dates.IsManual = false;
            Assert.AreEqual(consumer1IssuedDate, dates.IssuedDate);

            dates.IsManual = true;
            Assert.AreEqual(consumer2IssuedDate, dates.IssuedDate);
        }

        [Test]
        public void IssuedDate_BorrowerLevelDatesOnFileAllExcluded_Unlocked_ReturnsBlank(
            [Values(LoanRescindableT.Blank, LoanRescindableT.NotRescindable, LoanRescindableT.Rescindable)] LoanRescindableT rescindableType)
        {
            var archive = this.GetFakeClosingCostArchiveWithApr(5);
            var metadata = new LoanEstimateDatesMetadata(id => archive, this.ConsumerMetadataByConsumerId(), rescindableType);

            var regulatoryIssuedDate = DateTime.Parse("6/1/2018");
            var consumer1IssuedDate = DateTime.Parse("6/2/2018");
            var consumer2IssuedDate = DateTime.Parse("6/3/2018");

            var dates = LoanEstimateDates.Create(metadata);
            dates.IssuedDate = regulatoryIssuedDate;

            dates.DisclosureDatesByConsumerId.First().Value.IssuedDate = consumer1IssuedDate;
            dates.DisclosureDatesByConsumerId.First().Value.ExcludeFromCalculations = true;

            dates.DisclosureDatesByConsumerId.Last().Value.IssuedDate = consumer2IssuedDate;
            dates.DisclosureDatesByConsumerId.Last().Value.ExcludeFromCalculations = true;

            dates.IssuedDateLckd = false;
            dates.IsManual = false;
            Assert.AreEqual(consumer1IssuedDate, dates.IssuedDate);

            dates.IsManual = true;
            Assert.AreEqual(DateTime.MinValue, dates.IssuedDate);
        }

        [Test]
        public void IssuedDate_BorrowerLevelDatesOnFileWithInvalidTypes_CalculatesAsExpected(
            [Values(LoanRescindableT.Blank, LoanRescindableT.NotRescindable, LoanRescindableT.Rescindable)] LoanRescindableT rescindableType,
            [Values(E_aTypeT.NonTitleSpouse, E_aTypeT.TitleOnly, E_aTypeT.CurrentTitleOnly)] E_aTypeT firstBorrowerType)
        {
            var archive = this.GetFakeClosingCostArchiveWithApr(5);

            var consumers = this.ConsumerMetadataByConsumerId();
            consumers[0].ConsumerType = firstBorrowerType;

            var metadata = new LoanEstimateDatesMetadata(id => archive, consumers, rescindableType);

            var regulatoryIssuedDate = DateTime.Parse("6/1/2018");
            var consumer1IssuedDate = DateTime.Parse("6/2/2018");
            var consumer2IssuedDate = DateTime.Parse("6/3/2018");

            var dates = LoanEstimateDates.Create(metadata);
            dates.IssuedDate = regulatoryIssuedDate;

            dates.DisclosureDatesByConsumerId.First().Value.IssuedDate = consumer1IssuedDate;
            dates.DisclosureDatesByConsumerId.Last().Value.IssuedDate = consumer2IssuedDate;

            Assert.AreEqual(consumer2IssuedDate, dates.IssuedDate);
        }

        [Test]
        public void IssuedDate_BorrowerLevelDatesOnFileWithoutData_ReturnsBlank(
            [Values(LoanRescindableT.Blank, LoanRescindableT.NotRescindable, LoanRescindableT.Rescindable)] LoanRescindableT rescindableType)
        {
            var archive = this.GetFakeClosingCostArchiveWithApr(5);
            var metadata = new LoanEstimateDatesMetadata(id => archive, this.ConsumerMetadataByConsumerId(), rescindableType);
            
            var regulatoryIssuedDate = DateTime.Parse("6/1/2018");

            var dates = LoanEstimateDates.Create(metadata);
            dates.IssuedDate = regulatoryIssuedDate;

            dates.IssuedDateLckd = false;
            Assert.AreEqual(DateTime.MinValue, dates.IssuedDate);
        }

        [Test]
        public void DeliveryMethod_NoBorrowerLevelDatesOnFile_UsesRegulatoryLevelDeliveryMethod(
            [Values(true, false)] bool issuedDateLocked,
            [Values(LoanRescindableT.Blank, LoanRescindableT.NotRescindable, LoanRescindableT.Rescindable)] LoanRescindableT rescindableType)
        {
            var archive = this.GetFakeClosingCostArchiveWithApr(5);
            var metadata = new LoanEstimateDatesMetadata(id => archive, null, rescindableType);
            
            var issuedDate = DateTime.Parse("6/1/2018");
            var deliveryMethod = E_DeliveryMethodT.Fax;

            var dates = LoanEstimateDates.Create(metadata);
            dates.IssuedDateLckd = issuedDateLocked;
            dates.IssuedDate = issuedDate;
            dates.DeliveryMethod = deliveryMethod;

            Assert.AreEqual(deliveryMethod, dates.DeliveryMethod);
        }

        [Test]
        public void DeliveryMethod_BorrowerLevelDatesOnFile_Locked_UsesRegulatoryLevelDeliveryMethod(
            [Values(true, false)] bool issuedDateLocked,
            [Values(LoanRescindableT.Blank, LoanRescindableT.NotRescindable, LoanRescindableT.Rescindable)] LoanRescindableT rescindableType)
        {
            var archive = this.GetFakeClosingCostArchiveWithApr(5);
            var metadata = new LoanEstimateDatesMetadata(id => archive, this.ConsumerMetadataByConsumerId(), rescindableType);

            var regulatoryIssuedDate = DateTime.Parse("6/1/2018");
            var regulatoryDeliveryMethod = E_DeliveryMethodT.Email;

            var consumer1IssuedDate = DateTime.Parse("6/2/2018");
            var consumer1DeliveryMethod = E_DeliveryMethodT.Mail;

            var consumer2IssuedDate = DateTime.Parse("6/3/2018");
            var consumer2DeliveryMethod = E_DeliveryMethodT.InPerson;

            var dates = LoanEstimateDates.Create(metadata);

            dates.IssuedDate = regulatoryIssuedDate;
            dates.DeliveryMethod = regulatoryDeliveryMethod;

            dates.DisclosureDatesByConsumerId.First().Value.IssuedDate = consumer1IssuedDate;
            dates.DisclosureDatesByConsumerId.First().Value.DeliveryMethod = consumer1DeliveryMethod;

            dates.DisclosureDatesByConsumerId.Last().Value.IssuedDate = consumer2IssuedDate;
            dates.DisclosureDatesByConsumerId.Last().Value.DeliveryMethod = consumer2DeliveryMethod;

            dates.IssuedDateLckd = issuedDateLocked;
            dates.DeliveryMethodLckd = true;
            Assert.AreEqual(regulatoryDeliveryMethod, dates.DeliveryMethod);
        }

        [Test]
        public void DeliveryMethod_BorrowerLevelDatesOnFile_Unlocked_UsesCalculation(
            [Values(true, false)] bool issuedDateLocked,
            [Values(LoanRescindableT.Blank, LoanRescindableT.NotRescindable, LoanRescindableT.Rescindable)] LoanRescindableT rescindableType,
            [Values(E_aTypeT.TitleOnly, E_aTypeT.NonTitleSpouse, E_aTypeT.CurrentTitleOnly)] E_aTypeT manualBorrowerType)
        {
            var archive = this.GetFakeClosingCostArchiveWithApr(5);
            var metadata = new LoanEstimateDatesMetadata(id => archive, this.ConsumerMetadataByConsumerId(), rescindableType);
            
            var regulatoryIssuedDate = DateTime.Parse("6/1/2018");
            var regulatoryDeliveryMethod = E_DeliveryMethodT.Email;

            var consumer1IssuedDate = DateTime.Parse("6/2/2018");
            var consumer1DeliveryMethod = E_DeliveryMethodT.Mail;

            var consumer2IssuedDate = DateTime.Parse("6/3/2018");
            var consumer2DeliveryMethod = E_DeliveryMethodT.InPerson;

            var dates = LoanEstimateDates.Create(metadata);

            dates.IssuedDate = regulatoryIssuedDate;
            dates.DeliveryMethod = regulatoryDeliveryMethod;

            dates.DisclosureDatesByConsumerId.First().Value.IssuedDate = consumer1IssuedDate;
            dates.DisclosureDatesByConsumerId.First().Value.DeliveryMethod = consumer1DeliveryMethod;

            dates.DisclosureDatesByConsumerId.Last().Value.IssuedDate = consumer2IssuedDate;
            dates.DisclosureDatesByConsumerId.Last().Value.DeliveryMethod = consumer2DeliveryMethod;

            dates.IssuedDateLckd = issuedDateLocked;
            dates.DeliveryMethodLckd = false;
            Assert.AreEqual(consumer1DeliveryMethod, dates.DeliveryMethod);

            var existingConsumers = metadata.ConsumerMetadataByConsumerId;
            existingConsumers[0].ConsumerType = manualBorrowerType;

            metadata = new LoanEstimateDatesMetadata(id => archive, existingConsumers, metadata.LoanRescindableType);
            
            dates.SetMetadata(metadata);
            dates.IsManual = true;

            Assert.AreEqual(consumer1DeliveryMethod, dates.DeliveryMethod);
        }

        [Test]
        public void DeliveryMethod_BorrowerLevelDatesOnFile_SetMetadata_UpdatesCalculation(
            [Values(E_aTypeT.TitleOnly, E_aTypeT.CurrentTitleOnly)] E_aTypeT consumer1Type)
        {
            var archive = this.GetFakeClosingCostArchiveWithApr(5);
            var consumers = this.ConsumerMetadataByConsumerId();

            var metadata = new LoanEstimateDatesMetadata(id => archive, consumers, LoanRescindableT.Blank);
            
            var regulatoryIssuedDate = DateTime.Parse("6/1/2018");
            var regulatoryDeliveryMethod = E_DeliveryMethodT.Email;

            var consumer1IssuedDate = DateTime.Parse("6/2/2018");
            var consumer1DeliveryMethod = E_DeliveryMethodT.Mail;

            var consumer2IssuedDate = DateTime.Parse("6/3/2018");
            var consumer2DeliveryMethod = E_DeliveryMethodT.InPerson;

            var dates = LoanEstimateDates.Create(metadata);

            dates.IssuedDate = regulatoryIssuedDate;
            dates.DeliveryMethod = regulatoryDeliveryMethod;

            dates.DisclosureDatesByConsumerId.First().Value.IssuedDate = consumer1IssuedDate;
            dates.DisclosureDatesByConsumerId.First().Value.DeliveryMethod = consumer1DeliveryMethod;

            dates.DisclosureDatesByConsumerId.Last().Value.IssuedDate = consumer2IssuedDate;
            dates.DisclosureDatesByConsumerId.Last().Value.DeliveryMethod = consumer2DeliveryMethod;
            
            dates.DeliveryMethodLckd = false;
            Assert.AreEqual(consumer1DeliveryMethod, dates.DeliveryMethod);

            consumers[0].ConsumerType = consumer1Type;

            dates.SetMetadata(new LoanEstimateDatesMetadata(id => archive, consumers, metadata.LoanRescindableType));
            Assert.AreEqual(consumer2DeliveryMethod, dates.DeliveryMethod);
        }

        [Test]
        public void DeliveryMethod_BorrowerLevelDatesOnFileWithExclusions_Unlocked_HonorsExclusionWhenManual(
            [Values(true, false)] bool issuedDateLocked,
            [Values(LoanRescindableT.Blank, LoanRescindableT.NotRescindable, LoanRescindableT.Rescindable)] LoanRescindableT rescindableType)
        {
            var archive = this.GetFakeClosingCostArchiveWithApr(5);
            var metadata = new LoanEstimateDatesMetadata(id => archive, this.ConsumerMetadataByConsumerId(), rescindableType);
            
            var regulatoryIssuedDate = DateTime.Parse("6/1/2018");
            var regulatoryDeliveryMethod = E_DeliveryMethodT.Email;

            var consumer1IssuedDate = DateTime.Parse("6/2/2018");
            var consumer1DeliveryMethod = E_DeliveryMethodT.Mail;

            var consumer2IssuedDate = DateTime.Parse("6/3/2018");
            var consumer2DeliveryMethod = E_DeliveryMethodT.InPerson;

            var dates = LoanEstimateDates.Create(metadata);

            dates.IssuedDate = regulatoryIssuedDate;
            dates.DeliveryMethod = regulatoryDeliveryMethod;

            dates.DisclosureDatesByConsumerId.First().Value.IssuedDate = consumer1IssuedDate;
            dates.DisclosureDatesByConsumerId.First().Value.DeliveryMethod = consumer1DeliveryMethod;
            dates.DisclosureDatesByConsumerId.First().Value.ExcludeFromCalculations = true;

            dates.DisclosureDatesByConsumerId.Last().Value.IssuedDate = consumer2IssuedDate;
            dates.DisclosureDatesByConsumerId.Last().Value.DeliveryMethod = consumer2DeliveryMethod;

            dates.IssuedDateLckd = issuedDateLocked;
            dates.DeliveryMethodLckd = false;
            dates.IsManual = false;
            Assert.AreEqual(consumer1DeliveryMethod, dates.DeliveryMethod);

            dates.IsManual = true;
            Assert.AreEqual(consumer2DeliveryMethod, dates.DeliveryMethod);
        }

        [Test]
        public void DeliveryMethod_BorrowerLevelDatesOnFileAllExcluded_Unlocked_ReturnsBlank(
            [Values(true, false)] bool issuedDateLocked,
            [Values(LoanRescindableT.Blank, LoanRescindableT.NotRescindable, LoanRescindableT.Rescindable)] LoanRescindableT rescindableType)
        {
            var archive = this.GetFakeClosingCostArchiveWithApr(5);
            var metadata = new LoanEstimateDatesMetadata(id => archive, this.ConsumerMetadataByConsumerId(), rescindableType);
            
            var regulatoryIssuedDate = DateTime.Parse("6/1/2018");
            var regulatoryDeliveryMethod = E_DeliveryMethodT.Email;

            var consumer1IssuedDate = DateTime.Parse("6/2/2018");
            var consumer1DeliveryMethod = E_DeliveryMethodT.Mail;

            var consumer2IssuedDate = DateTime.Parse("6/3/2018");
            var consumer2DeliveryMethod = E_DeliveryMethodT.InPerson;

            var dates = LoanEstimateDates.Create(metadata);

            dates.IssuedDate = regulatoryIssuedDate;
            dates.DeliveryMethod = regulatoryDeliveryMethod;

            dates.DisclosureDatesByConsumerId.First().Value.IssuedDate = consumer1IssuedDate;
            dates.DisclosureDatesByConsumerId.First().Value.DeliveryMethod = consumer1DeliveryMethod;
            dates.DisclosureDatesByConsumerId.First().Value.ExcludeFromCalculations = true;

            dates.DisclosureDatesByConsumerId.Last().Value.IssuedDate = consumer2IssuedDate;
            dates.DisclosureDatesByConsumerId.Last().Value.DeliveryMethod = consumer2DeliveryMethod;
            dates.DisclosureDatesByConsumerId.Last().Value.ExcludeFromCalculations = true;

            dates.IssuedDateLckd = issuedDateLocked;
            dates.DeliveryMethodLckd = false;
            dates.IsManual = false;
            Assert.AreEqual(consumer1DeliveryMethod, dates.DeliveryMethod);

            dates.IsManual = true;
            Assert.AreEqual(E_DeliveryMethodT.LeaveEmpty, dates.DeliveryMethod);
        }

        [Test]
        public void DeliveryMethod_BorrowerLevelDatesOnFileWithInvalidTypes_CalculatesAsExpected(
            [Values(LoanRescindableT.Blank, LoanRescindableT.NotRescindable, LoanRescindableT.Rescindable)] LoanRescindableT rescindableType,
            [Values(E_aTypeT.NonTitleSpouse, E_aTypeT.TitleOnly, E_aTypeT.CurrentTitleOnly)] E_aTypeT firstBorrowerType)
        {
            var archive = this.GetFakeClosingCostArchiveWithApr(5);

            var consumers = this.ConsumerMetadataByConsumerId();
            consumers[0].ConsumerType = firstBorrowerType;

            var metadata = new LoanEstimateDatesMetadata(id => archive, consumers, rescindableType);

            var regulatoryIssuedDate = DateTime.Parse("6/1/2018");
            var regulatoryDeliveryMethod = E_DeliveryMethodT.Email;

            var consumer1IssuedDate = DateTime.Parse("6/2/2018");
            var consumer1DeliveryMethod = E_DeliveryMethodT.Mail;

            var consumer2IssuedDate = DateTime.Parse("6/3/2018");
            var consumer2DeliveryMethod = E_DeliveryMethodT.InPerson;

            var dates = LoanEstimateDates.Create(metadata);

            dates.IssuedDate = regulatoryIssuedDate;
            dates.DeliveryMethod = regulatoryDeliveryMethod;

            dates.DisclosureDatesByConsumerId.First().Value.IssuedDate = consumer1IssuedDate;
            dates.DisclosureDatesByConsumerId.First().Value.DeliveryMethod = consumer1DeliveryMethod;

            dates.DisclosureDatesByConsumerId.Last().Value.IssuedDate = consumer2IssuedDate;
            dates.DisclosureDatesByConsumerId.Last().Value.DeliveryMethod = consumer2DeliveryMethod;

            Assert.AreEqual(consumer2DeliveryMethod, dates.DeliveryMethod);
        }

        [Test]
        public void DeliveryMethod_BorrowerLevelDatesOnFileWithoutData_ReturnsBlank(
            [Values(LoanRescindableT.Blank, LoanRescindableT.NotRescindable, LoanRescindableT.Rescindable)] LoanRescindableT rescindableType)
        {
            var archive = this.GetFakeClosingCostArchiveWithApr(5);
            var metadata = new LoanEstimateDatesMetadata(id => archive, this.ConsumerMetadataByConsumerId(), rescindableType);
            
            var regulatoryIssuedDate = DateTime.Parse("6/1/2018");

            var dates = LoanEstimateDates.Create(metadata);
            dates.IssuedDate = regulatoryIssuedDate;
            dates.IssuedDateLckd = false;
            dates.DeliveryMethod = E_DeliveryMethodT.InPerson;

            Assert.AreEqual(E_DeliveryMethodT.LeaveEmpty, dates.DeliveryMethod);
        }

        [Test]
        public void ReceivedDate_NoBorrowerLevelDatesOnFile_UsesRegulatoryLevelReceivedDate(
            [Values(LoanRescindableT.Blank, LoanRescindableT.NotRescindable, LoanRescindableT.Rescindable)] LoanRescindableT rescindableType)
        {
            var archive = this.GetFakeClosingCostArchiveWithApr(5);
            var metadata = new LoanEstimateDatesMetadata(id => archive, null, rescindableType);

            var receivedDate = DateTime.Parse("6/1/2018");
            var dates = LoanEstimateDates.Create(metadata);
            dates.ReceivedDate = receivedDate;

            Assert.AreEqual(receivedDate, dates.ReceivedDate);
        }

        [Test]
        public void ReceivedDate_BorrowerLevelDatesOnFile_Locked_UsesRegulatoryLevelReceivedDate(
            [Values(LoanRescindableT.Blank, LoanRescindableT.NotRescindable, LoanRescindableT.Rescindable)] LoanRescindableT rescindableType)
        {
            var archive = this.GetFakeClosingCostArchiveWithApr(5);
            var metadata = new LoanEstimateDatesMetadata(id => archive, this.ConsumerMetadataByConsumerId(), rescindableType);
            
            var regulatoryReceivedDate = DateTime.Parse("6/1/2018");
            var consumer1ReceivedDate = DateTime.Parse("6/2/2018");
            var consumer2ReceivedDate = DateTime.Parse("6/3/2018");

            var dates = LoanEstimateDates.Create(metadata);
            dates.ReceivedDate = regulatoryReceivedDate;
            dates.DisclosureDatesByConsumerId.First().Value.ReceivedDate = consumer1ReceivedDate;
            dates.DisclosureDatesByConsumerId.Last().Value.ReceivedDate = consumer2ReceivedDate;

            dates.ReceivedDateLckd = true;
            Assert.AreEqual(regulatoryReceivedDate, dates.ReceivedDate);
        }

        [Test]
        public void ReceivedDate_BorrowerLevelDatesOnFile_Unlocked_UsesCalculation(
            [Values(LoanRescindableT.Blank, LoanRescindableT.NotRescindable, LoanRescindableT.Rescindable)] LoanRescindableT rescindableType,
            [Values(E_aTypeT.TitleOnly, E_aTypeT.NonTitleSpouse, E_aTypeT.CurrentTitleOnly)] E_aTypeT manualBorrowerType)
        {
            var archive = this.GetFakeClosingCostArchiveWithApr(5);
            var metadata = new LoanEstimateDatesMetadata(id => archive, this.ConsumerMetadataByConsumerId(), rescindableType);

            var regulatoryReceivedDate = DateTime.Parse("6/1/2018");
            var consumer1ReceivedDate = DateTime.Parse("6/2/2018");
            var consumer2ReceivedDate = DateTime.Parse("6/3/2018");

            var dates = LoanEstimateDates.Create(metadata);
            dates.ReceivedDate = regulatoryReceivedDate;
            dates.DisclosureDatesByConsumerId.First().Value.ReceivedDate = consumer1ReceivedDate;
            dates.DisclosureDatesByConsumerId.Last().Value.ReceivedDate = consumer2ReceivedDate;

            dates.ReceivedDateLckd = false;
            Assert.AreEqual(consumer1ReceivedDate, dates.ReceivedDate);

            var existingConsumers = metadata.ConsumerMetadataByConsumerId;
            existingConsumers[0].ConsumerType = manualBorrowerType;

            metadata = new LoanEstimateDatesMetadata(id => archive, existingConsumers, metadata.LoanRescindableType);

            dates.SetMetadata(metadata);
            dates.IsManual = true;

            Assert.AreEqual(consumer1ReceivedDate, dates.ReceivedDate);
        }

        [Test]
        public void ReceivedDate_BorrowerLevelDatesOnFileWithExclusions_Unlocked_HonorsExclusionWhenManual(
            [Values(LoanRescindableT.Blank, LoanRescindableT.NotRescindable, LoanRescindableT.Rescindable)] LoanRescindableT rescindableType)
        {
            var archive = this.GetFakeClosingCostArchiveWithApr(5);
            var metadata = new LoanEstimateDatesMetadata(id => archive, this.ConsumerMetadataByConsumerId(), rescindableType);
            
            var regulatoryReceivedDate = DateTime.Parse("6/1/2018");
            var consumer1ReceivedDate = DateTime.Parse("6/2/2018");
            var consumer2ReceivedDate = DateTime.Parse("6/3/2018");

            var dates = LoanEstimateDates.Create(metadata);
            dates.ReceivedDate = regulatoryReceivedDate;

            dates.DisclosureDatesByConsumerId.First().Value.ReceivedDate = consumer1ReceivedDate;
            dates.DisclosureDatesByConsumerId.First().Value.ExcludeFromCalculations = true;

            dates.DisclosureDatesByConsumerId.Last().Value.ReceivedDate = consumer2ReceivedDate;

            dates.ReceivedDateLckd = false;
            dates.IsManual = false;
            Assert.AreEqual(consumer1ReceivedDate, dates.ReceivedDate);

            dates.IsManual = true;
            Assert.AreEqual(consumer2ReceivedDate, dates.ReceivedDate);
        }

        [Test]
        public void ReceivedDate_BorrowerLevelDatesOnFile_SetMetadata_UpdatesCalculation(
            [Values(LoanRescindableT.Blank, LoanRescindableT.NotRescindable, LoanRescindableT.Rescindable)] LoanRescindableT rescindableType,
            [Values(E_aTypeT.TitleOnly, E_aTypeT.CurrentTitleOnly)] E_aTypeT consumer1Type)
        {
            var archive = this.GetFakeClosingCostArchiveWithApr(5);
            var consumers = this.ConsumerMetadataByConsumerId();

            var metadata = new LoanEstimateDatesMetadata(id => archive, consumers, rescindableType);

            var regulatoryReceivedDate = DateTime.Parse("6/1/2018");
            var consumer1ReceivedDate = DateTime.Parse("6/2/2018");
            var consumer2ReceivedDate = DateTime.Parse("6/3/2018");

            var dates = LoanEstimateDates.Create(metadata);
            dates.ReceivedDate = regulatoryReceivedDate;
            dates.DisclosureDatesByConsumerId.First().Value.ReceivedDate = consumer1ReceivedDate;
            dates.DisclosureDatesByConsumerId.Last().Value.ReceivedDate = consumer2ReceivedDate;

            dates.ReceivedDateLckd = false;
            Assert.AreEqual(consumer1ReceivedDate, dates.ReceivedDate);
            
            consumers[0].ConsumerType = consumer1Type;

            dates.SetMetadata(new LoanEstimateDatesMetadata(id => archive, consumers, metadata.LoanRescindableType));
            Assert.AreEqual(consumer2ReceivedDate, dates.ReceivedDate);
        }

        [Test]
        public void ReceivedDate_BorrowerLevelDatesOnFileAllExcluded_Unlocked_ReturnsBlank()
        {
            var archive = this.GetFakeClosingCostArchiveWithApr(5);
            var metadata = new LoanEstimateDatesMetadata(id => archive, this.ConsumerMetadataByConsumerId(), LoanRescindableT.Blank);
            
            var regulatoryReceivedDate = DateTime.Parse("6/1/2018");
            var consumer1ReceivedDate = DateTime.Parse("6/2/2018");
            var consumer2ReceivedDate = DateTime.Parse("6/3/2018");

            var dates = LoanEstimateDates.Create(metadata);
            dates.ReceivedDate = regulatoryReceivedDate;

            dates.DisclosureDatesByConsumerId.First().Value.ReceivedDate = consumer1ReceivedDate;
            dates.DisclosureDatesByConsumerId.First().Value.ExcludeFromCalculations = true;

            dates.DisclosureDatesByConsumerId.Last().Value.ReceivedDate = consumer2ReceivedDate;
            dates.DisclosureDatesByConsumerId.Last().Value.ExcludeFromCalculations = true;

            dates.ReceivedDateLckd = false;
            dates.IsManual = false;
            Assert.AreEqual(consumer1ReceivedDate, dates.ReceivedDate);

            dates.IsManual = true;
            Assert.AreEqual(DateTime.MinValue, dates.ReceivedDate);
        }

        [Test]
        public void ReceivedDate_BorrowerLevelDatesOnFileWithInvalidTypes_CalculatesAsExpected(
            [Values(LoanRescindableT.Blank, LoanRescindableT.NotRescindable, LoanRescindableT.Rescindable)] LoanRescindableT rescindableType,
            [Values(E_aTypeT.NonTitleSpouse, E_aTypeT.TitleOnly, E_aTypeT.CurrentTitleOnly)] E_aTypeT firstBorrowerType)
        {
            var archive = this.GetFakeClosingCostArchiveWithApr(5);

            var consumers = this.ConsumerMetadataByConsumerId();
            consumers[0].ConsumerType = firstBorrowerType;

            var metadata = new LoanEstimateDatesMetadata(id => archive, consumers, rescindableType);

            var regulatoryReceivedDate = DateTime.Parse("6/1/2018");
            var consumer1ReceivedDate = DateTime.Parse("6/2/2018");
            var consumer2ReceivedDate = DateTime.Parse("6/3/2018");

            var dates = LoanEstimateDates.Create(metadata);
            dates.ReceivedDate = regulatoryReceivedDate;

            dates.DisclosureDatesByConsumerId.First().Value.ReceivedDate = consumer1ReceivedDate;
            dates.DisclosureDatesByConsumerId.Last().Value.ReceivedDate = consumer2ReceivedDate;

            Assert.AreEqual(consumer2ReceivedDate, dates.ReceivedDate);
        }

        [Test]
        public void ReceivedDate_BorrowerLevelDatesOnFileWithoutData_ReturnsBlank(
            [Values(LoanRescindableT.Blank, LoanRescindableT.NotRescindable, LoanRescindableT.Rescindable)] LoanRescindableT rescindableType)
        {
            var archive = this.GetFakeClosingCostArchiveWithApr(5);
            var metadata = new LoanEstimateDatesMetadata(id => archive, this.ConsumerMetadataByConsumerId(), rescindableType);
            
            var regulatoryReceivedDate = DateTime.Parse("6/1/2018");

            var dates = LoanEstimateDates.Create(metadata);
            dates.ReceivedDate = regulatoryReceivedDate;

            dates.ReceivedDateLckd = false;
            Assert.AreEqual(DateTime.MinValue, dates.ReceivedDate);
        }

        [Test]
        public void SignedDate_NoBorrowerLevelDatesOnFile_UsesRegulatoryLevelSignedDate(
            [Values(LoanRescindableT.Blank, LoanRescindableT.NotRescindable, LoanRescindableT.Rescindable)] LoanRescindableT rescindableType)
        {
            var archive = this.GetFakeClosingCostArchiveWithApr(5);
            var metadata = new LoanEstimateDatesMetadata(id => archive, null, rescindableType);
            
            var SignedDate = DateTime.Parse("6/1/2018");
            var dates = LoanEstimateDates.Create(metadata);
            dates.SignedDate = SignedDate;

            Assert.AreEqual(SignedDate, dates.SignedDate);
        }

        [Test]
        public void SignedDate_BorrowerLevelDatesOnFile_Locked_UsesRegulatoryLevelSignedDate(
            [Values(LoanRescindableT.Blank, LoanRescindableT.NotRescindable, LoanRescindableT.Rescindable)] LoanRescindableT rescindableType)
        {
            var archive = this.GetFakeClosingCostArchiveWithApr(5);
            var metadata = new LoanEstimateDatesMetadata(id => archive, this.ConsumerMetadataByConsumerId(), rescindableType);
            
            var regulatorySignedDate = DateTime.Parse("6/1/2018");
            var consumer1SignedDate = DateTime.Parse("6/2/2018");
            var consumer2SignedDate = DateTime.Parse("6/3/2018");

            var dates = LoanEstimateDates.Create(metadata);
            dates.SignedDate = regulatorySignedDate;
            dates.DisclosureDatesByConsumerId.First().Value.SignedDate = consumer1SignedDate;
            dates.DisclosureDatesByConsumerId.Last().Value.SignedDate = consumer2SignedDate;

            dates.SignedDateLckd = true;
            Assert.AreEqual(regulatorySignedDate, dates.SignedDate);
        }

        [Test]
        public void SignedDate_BorrowerLevelDatesOnFile_Unlocked_UsesCalculation(
            [Values(LoanRescindableT.Blank, LoanRescindableT.NotRescindable, LoanRescindableT.Rescindable)] LoanRescindableT rescindableType,
            [Values(E_aTypeT.TitleOnly, E_aTypeT.NonTitleSpouse, E_aTypeT.CurrentTitleOnly)] E_aTypeT manualBorrowerType)
        {
            var archive = this.GetFakeClosingCostArchiveWithApr(5);
            var metadata = new LoanEstimateDatesMetadata(id => archive, this.ConsumerMetadataByConsumerId(), rescindableType);

            var regulatorySignedDate = DateTime.Parse("6/1/2018");
            var consumer1SignedDate = DateTime.Parse("6/2/2018");
            var consumer2SignedDate = DateTime.Parse("6/3/2018");

            var dates = LoanEstimateDates.Create(metadata);
            dates.SignedDate = regulatorySignedDate;
            dates.DisclosureDatesByConsumerId.First().Value.SignedDate = consumer1SignedDate;
            dates.DisclosureDatesByConsumerId.Last().Value.SignedDate = consumer2SignedDate;

            dates.SignedDateLckd = false;
            Assert.AreEqual(consumer1SignedDate, dates.SignedDate);

            var existingConsumers = metadata.ConsumerMetadataByConsumerId;
            existingConsumers[0].ConsumerType = manualBorrowerType;
            metadata = new LoanEstimateDatesMetadata(id => archive, existingConsumers, metadata.LoanRescindableType);

            dates.SetMetadata(metadata);
            dates.IsManual = true;

            Assert.AreEqual(consumer1SignedDate, dates.SignedDate);
        }

        [Test]
        public void SignedDate_BorrowerLevelDatesOnFile_SetMetadata_UpdatesCalculation(
            [Values(E_aTypeT.TitleOnly, E_aTypeT.CurrentTitleOnly)] E_aTypeT consumer1Type)
        {
            var archive = this.GetFakeClosingCostArchiveWithApr(5);
            var consumers = this.ConsumerMetadataByConsumerId();

            var metadata = new LoanEstimateDatesMetadata(id => archive, consumers, LoanRescindableT.Blank);

            var regulatorySignedDate = DateTime.Parse("6/1/2018");
            var consumer1SignedDate = DateTime.Parse("6/2/2018");
            var consumer2SignedDate = DateTime.Parse("6/3/2018");

            var dates = LoanEstimateDates.Create(metadata);
            dates.SignedDate = regulatorySignedDate;
            dates.DisclosureDatesByConsumerId.First().Value.SignedDate = consumer1SignedDate;
            dates.DisclosureDatesByConsumerId.Last().Value.SignedDate = consumer2SignedDate;

            dates.SignedDateLckd = false;
            Assert.AreEqual(consumer1SignedDate, dates.SignedDate);

            consumers[0].ConsumerType = consumer1Type;

            dates.SetMetadata(new LoanEstimateDatesMetadata(id => archive, consumers, metadata.LoanRescindableType));
            Assert.AreEqual(consumer2SignedDate, dates.SignedDate);
        }

        [Test]
        public void SignedDate_BorrowerLevelDatesOnFileWithExclusions_Unlocked_HonorsExclusionWhenManual(
            [Values(LoanRescindableT.Blank, LoanRescindableT.NotRescindable, LoanRescindableT.Rescindable)] LoanRescindableT rescindableType)
        {
            var archive = this.GetFakeClosingCostArchiveWithApr(5);
            var metadata = new LoanEstimateDatesMetadata(id => archive, this.ConsumerMetadataByConsumerId(), rescindableType);
           
            var regulatorySignedDate = DateTime.Parse("6/1/2018");
            var consumer1SignedDate = DateTime.Parse("6/2/2018");
            var consumer2SignedDate = DateTime.Parse("6/3/2018");

            var dates = LoanEstimateDates.Create(metadata);
            dates.SignedDate = regulatorySignedDate;

            dates.DisclosureDatesByConsumerId.First().Value.SignedDate = consumer1SignedDate;
            dates.DisclosureDatesByConsumerId.First().Value.ExcludeFromCalculations = true;

            dates.DisclosureDatesByConsumerId.Last().Value.SignedDate = consumer2SignedDate;

            dates.SignedDateLckd = false;
            dates.IsManual = false;
            Assert.AreEqual(consumer1SignedDate, dates.SignedDate);

            dates.IsManual = true;
            Assert.AreEqual(consumer2SignedDate, dates.SignedDate);
        }

        [Test]
        public void SignedDate_BorrowerLevelDatesOnFileAllExcluded_Unlocked_ReturnsBlank(
            [Values(LoanRescindableT.Blank, LoanRescindableT.NotRescindable, LoanRescindableT.Rescindable)] LoanRescindableT rescindableType)
        {
            var archive = this.GetFakeClosingCostArchiveWithApr(5);
            var metadata = new LoanEstimateDatesMetadata(id => archive, this.ConsumerMetadataByConsumerId(), rescindableType);
            
            var regulatorySignedDate = DateTime.Parse("6/1/2018");
            var consumer1SignedDate = DateTime.Parse("6/2/2018");
            var consumer2SignedDate = DateTime.Parse("6/3/2018");

            var dates = LoanEstimateDates.Create(metadata);
            dates.SignedDate = regulatorySignedDate;

            dates.DisclosureDatesByConsumerId.First().Value.SignedDate = consumer1SignedDate;
            dates.DisclosureDatesByConsumerId.First().Value.ExcludeFromCalculations = true;

            dates.DisclosureDatesByConsumerId.Last().Value.SignedDate = consumer2SignedDate;
            dates.DisclosureDatesByConsumerId.Last().Value.ExcludeFromCalculations = true;

            dates.SignedDateLckd = false;
            dates.IsManual = false;
            Assert.AreEqual(consumer1SignedDate, dates.SignedDate);

            dates.IsManual = true;
            Assert.AreEqual(DateTime.MinValue, dates.SignedDate);
        }

        [Test]
        public void SignedDate_BorrowerLevelDatesOnFileWithInvalidTypes_CalculatesAsExpected(
            [Values(LoanRescindableT.Blank, LoanRescindableT.NotRescindable, LoanRescindableT.Rescindable)] LoanRescindableT rescindableType,
            [Values(E_aTypeT.NonTitleSpouse, E_aTypeT.TitleOnly, E_aTypeT.CurrentTitleOnly)] E_aTypeT firstBorrowerType)
        {
            var archive = this.GetFakeClosingCostArchiveWithApr(5);

            var consumers = this.ConsumerMetadataByConsumerId();
            consumers[0].ConsumerType = firstBorrowerType;

            var metadata = new LoanEstimateDatesMetadata(id => archive, consumers, rescindableType);

            var regulatorySignedDate = DateTime.Parse("6/1/2018");
            var consumer1SignedDate = DateTime.Parse("6/2/2018");
            var consumer2SignedDate = DateTime.Parse("6/3/2018");

            var dates = LoanEstimateDates.Create(metadata);
            dates.SignedDate = regulatorySignedDate;

            dates.DisclosureDatesByConsumerId.First().Value.SignedDate = consumer1SignedDate;
            dates.DisclosureDatesByConsumerId.Last().Value.SignedDate = consumer2SignedDate;

            Assert.AreEqual(consumer2SignedDate, dates.SignedDate);
        }

        [Test]
        public void SignedDate_BorrowerLevelDatesOnFileWithoutData_ReturnsBlank(
            [Values(LoanRescindableT.Blank, LoanRescindableT.NotRescindable, LoanRescindableT.Rescindable)] LoanRescindableT rescindableType)
        {
            var archive = this.GetFakeClosingCostArchiveWithApr(5);
            var metadata = new LoanEstimateDatesMetadata(id => archive, this.ConsumerMetadataByConsumerId(), rescindableType);
            
            var regulatorySignedDate = DateTime.Parse("6/1/2018");

            var dates = LoanEstimateDates.Create(metadata);
            dates.SignedDate = regulatorySignedDate;

            dates.SignedDateLckd = false;
            Assert.AreEqual(DateTime.MinValue, dates.SignedDate);
        }

        private IClosingCostArchive GetFakeClosingCostArchiveWithApr(decimal apr)
        {
            ////var archive = Substitute.For<IClosingCostArchive>();
            ////archive.Apr.Returns(apr);
            var archive = new FakeClosingCostArchive(apr);
            return archive;
        }

        private IReadOnlyList<BorrowerDisclosureMetadata> ConsumerMetadataByConsumerId()
        {
            return new[]
            {
                new BorrowerDisclosureMetadata() { ConsumerId = consumerIdFactory.NewId(), ConsumerType = E_aTypeT.Individual },
                new BorrowerDisclosureMetadata() { ConsumerId = consumerIdFactory.NewId(), ConsumerType = E_aTypeT.CoSigner }
            };
        }
    }
}
