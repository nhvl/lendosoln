﻿namespace LendingQB.Test.Developers.Disclosure
{
    using System;
    using System.Linq;
    using Fakes;
    using global::DataAccess;
    using LendersOffice.Common;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public class LoanEstimateDatesInfoTest
    {
        private const string jsonWithoutVersion = @"
            {
              ""loanEstimateDatesList"": [
                {
                  ""archiveDate"": ""1\/30\/2017 02:49:43 PM"",
                  ""archiveId"": ""34d37b3d-c287-4da1-8e23-fe36dfeb7738"",
                  ""createdDate"": ""1\/30\/2017"",
                  ""issuedDate"": ""1\/31\/2017"",
                  ""deliveryMethod"": 1,
                  ""disableManualArchiveAssociation"": false,
                  ""isCandidateInitial"": false,
                  ""isInitial"": false,
                  ""isManual"": false,
                  ""lastDisclosedTRIDLoanProductDescription"": """",
                  ""receivedDate"": ""2\/2\/2017"",
                  ""signedDate"": ""2\/3\/2017"",
                  ""transactionId"": ""60f94253-b0dc-4399-b208-81c60d2053be"",
                  ""uniqueId"": ""80318065-8bf7-4482-9cbe-0e8fde1c9ace""
                },
                {
                  ""archiveDate"": ""<-- NONE -->"",
                  ""archiveId"": ""11111111-1111-1111-1111-111111111111"",
                  ""createdDate"": ""1\/26\/2017"",
                  ""issuedDate"": ""1\/27\/2017"",
                  ""deliveryMethod"": 2,
                  ""disableManualArchiveAssociation"": false,
                  ""isCandidateInitial"": false,
                  ""isInitial"": true,
                  ""isManual"": true,
                  ""lastDisclosedTRIDLoanProductDescription"": """",
                  ""receivedDate"": ""2\/6\/2017"",
                  ""signedDate"": ""2\/7\/2017"",
                  ""transactionId"": """",
                  ""uniqueId"": ""f627cfbd-4cd7-4888-9555-ce53c7359bfa""
                }
              ]
            }";

        [Test]
        public void FromJson_EmptyJson_NoExceptionIsNull()
        {
            var json = "";
            var dates = LoanEstimateDatesInfo.FromJson(json, null);
            Assert.AreEqual(null, dates);
        }

        [Test]
        public void FromJson_JsonContainsNoVersion_DefaultsToVersion0()
        {
            var dates = LoanEstimateDatesInfo.FromJson(jsonWithoutVersion, metadata: null);

            Assert.AreEqual(0, dates.Version);
        }

        [Test]
        public void FromJson_WithArchiveSpecified_PassesOnArchiveToDates()
        {
            ////var archive = Substitute.For<IClosingCostArchive>();
            ////archive.Apr.Returns(5);
            var archive = new FakeClosingCostArchive(5);

            var metadata = new LoanEstimateDatesMetadata(id => archive);
            var dates = LoanEstimateDatesInfo.FromJson(jsonWithoutVersion, metadata);

            Assert.AreEqual(2, dates.LoanEstimateDatesList.Count);
            foreach (var le in dates.LoanEstimateDatesList)
            {
                Assert.AreEqual(5m, le.LqbApr);
            }
        }

        [Test]
        public void FromJson_AfterSerializeVersion0Collection_IsMostRecentVersion()
        {
            ////var archive = Substitute.For<IClosingCostArchive>();
            ////archive.Apr.Returns(5);
            var archive = new FakeClosingCostArchive(5);

            var metadata = new LoanEstimateDatesMetadata(id => archive);
            var dates = LoanEstimateDatesInfo.FromJson(jsonWithoutVersion, metadata);
            var updatedJson = ObsoleteSerializationHelper.JsonSerialize(dates);

            dates = LoanEstimateDatesInfo.FromJson(updatedJson, metadata: null);

            Assert.AreEqual(LoanEstimateDatesInfo.GetMostRecentVersion(), dates.Version);
        }

        [Test]
        public void FromJson_PriorToBorrowerLevelDates_UsesStoredValues()
        {
            var dates = LoanEstimateDatesInfo.FromJson(jsonWithoutVersion, metadata: null);

            var firstDate = dates.LoanEstimateDatesList.First();
            Assert.AreEqual(DateTime.Parse("1/31/2017"), firstDate.IssuedDate);
            Assert.AreEqual(E_DeliveryMethodT.Email, firstDate.DeliveryMethod);
            Assert.AreEqual(DateTime.Parse("2/2/2017"), firstDate.ReceivedDate);
            Assert.AreEqual(DateTime.Parse("2/3/2017"), firstDate.SignedDate);

            var secondDate = dates.LoanEstimateDatesList.Last();
            Assert.AreEqual(DateTime.Parse("1/27/2017"), secondDate.IssuedDate);
            Assert.AreEqual(E_DeliveryMethodT.Fax, secondDate.DeliveryMethod);
            Assert.AreEqual(DateTime.Parse("2/6/2017"), secondDate.ReceivedDate);
            Assert.AreEqual(DateTime.Parse("2/7/2017"), secondDate.SignedDate);
        }

        [Test]
        public void DuplicatePreservingIds_Always_MaintainsIds()
        {
            var dates = LoanEstimateDatesInfo.FromJson(jsonWithoutVersion, metadata: null);
        
            Assert.AreEqual(new Guid("80318065-8bf7-4482-9cbe-0e8fde1c9ace"), dates.LoanEstimateDatesList[0].UniqueId);
            Assert.AreEqual(new Guid("f627cfbd-4cd7-4888-9555-ce53c7359bfa"), dates.LoanEstimateDatesList[1].UniqueId);
        }

        [Test]
        public void Create_Always_AssignsMostRecentVersion()
        {
            var dates = LoanEstimateDatesInfo.Create();

            Assert.AreEqual(LoanEstimateDatesInfo.GetMostRecentVersion(), dates.Version);
        }
    }
}
