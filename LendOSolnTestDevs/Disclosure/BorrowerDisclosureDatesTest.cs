﻿namespace LendingQB.Test.Developers.Disclosure
{
    using System;
    using global::DataAccess;
    using NUnit.Framework;

    [TestFixture]
    public class BorrowerDisclosureDatesTest
    {
        [Test]
        public void Equals_ConsumerName_ReturnsExpectedResults()
        {
            var firstInstance = new BorrowerDisclosureDates();
            var secondInstance = new BorrowerDisclosureDates();

            firstInstance.ConsumerName = "Joe Schmoe";
            Assert.IsFalse(firstInstance.Equals(secondInstance));

            secondInstance.ConsumerName = "Joe Schmoe";
            Assert.IsTrue(firstInstance.Equals(secondInstance));
        }

        [Test]
        public void Equals_ExcludeFromCalculations_ReturnsExpectedResults()
        {
            var firstInstance = new BorrowerDisclosureDates();
            var secondInstance = new BorrowerDisclosureDates();

            firstInstance.ExcludeFromCalculations = true;
            Assert.IsFalse(firstInstance.Equals(secondInstance));

            secondInstance.ExcludeFromCalculations = true;
            Assert.IsTrue(firstInstance.Equals(secondInstance));
        }

        [Test]
        public void Equals_IssuedDates_ReturnsExpectedResults()
        {
            var firstInstance = new BorrowerDisclosureDates();
            var secondInstance = new BorrowerDisclosureDates();

            firstInstance.IssuedDate = DateTime.Parse("5/1/2018").Date;
            Assert.IsFalse(firstInstance.Equals(secondInstance));

            secondInstance.IssuedDate = DateTime.Parse("5/1/2018").Date;
            Assert.IsTrue(firstInstance.Equals(secondInstance));
        }

        [Test]
        public void Equals_DeliveryMethod_ReturnsExpectedResults()
        {
            var firstInstance = new BorrowerDisclosureDates();
            var secondInstance = new BorrowerDisclosureDates();

            firstInstance.DeliveryMethod = E_DeliveryMethodT.Email;
            Assert.IsFalse(firstInstance.Equals(secondInstance));

            secondInstance.DeliveryMethod = E_DeliveryMethodT.Email;
            Assert.IsTrue(firstInstance.Equals(secondInstance));
        }

        [Test]
        public void Equals_ReceivedDate_ReturnsExpectedResults()
        {
            var firstInstance = new BorrowerDisclosureDates();
            var secondInstance = new BorrowerDisclosureDates();

            firstInstance.ReceivedDate = DateTime.Parse("5/1/2018").Date;
            Assert.IsFalse(firstInstance.Equals(secondInstance));

            secondInstance.ReceivedDate = DateTime.Parse("5/1/2018").Date;
            Assert.IsTrue(firstInstance.Equals(secondInstance));
        }

        [Test]
        public void Equals_SignedDate_ReturnsExpectedResults()
        {
            var firstInstance = new BorrowerDisclosureDates();
            var secondInstance = new BorrowerDisclosureDates();

            firstInstance.SignedDate = DateTime.Parse("5/1/2018").Date;
            Assert.IsFalse(firstInstance.Equals(secondInstance));

            secondInstance.SignedDate = DateTime.Parse("5/1/2018").Date;
            Assert.IsTrue(firstInstance.Equals(secondInstance));
        }
    }
}
