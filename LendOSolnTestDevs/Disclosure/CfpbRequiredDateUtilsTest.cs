﻿namespace LendingQB.Test.Developers.Disclosure
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using global::DataAccess;
    using LqbGrammar.DataTypes;
    using NUnit.Framework;

    [TestFixture]
    public class CfpbRequiredDateUtilsTest
    {
        [Test]
        public void AreBorrowerLevelDatesEqual_DifferentCount_ReturnsExpectedResult()
        {
            var firstInstanceDates = Enumerable.Empty<KeyValuePair<Guid, BorrowerDisclosureDates>>().ToList().AsReadOnly();
            var secondInstanceDates = new[] { new KeyValuePair<Guid, BorrowerDisclosureDates>(Guid.NewGuid(), new BorrowerDisclosureDates()) }.ToList().AsReadOnly();

            var result = CfpbRequiredDateUtils.AreBorrowerLevelDatesEqual(firstInstanceDates, secondInstanceDates);
            Assert.IsFalse(result);
        }

        [Test]
        public void AreBorrowerLevelDatesEqual_SameCountDifferentValues_ReturnsExpectedResult()
        {
            var consumerId = Guid.NewGuid();

            var firstInstanceDate = new BorrowerDisclosureDates()
            {
                IssuedDate = DateTime.Parse("5/1/2018").Date
            };

            var secondInstanceDate = new BorrowerDisclosureDates()
            {
                IssuedDate = DateTime.Parse("5/8/2018").Date
            };

            var firstInstanceDates = new[] { new KeyValuePair<Guid, BorrowerDisclosureDates>(consumerId, firstInstanceDate) }.ToList().AsReadOnly();
            var secondInstanceDates = new[] { new KeyValuePair<Guid, BorrowerDisclosureDates>(consumerId, secondInstanceDate) }.ToList().AsReadOnly();

            var result = CfpbRequiredDateUtils.AreBorrowerLevelDatesEqual(firstInstanceDates, secondInstanceDates);
            Assert.IsFalse(result);
        }

        [Test]
        public void AreBorrowerLevelDatesEqual_SameCountSameValues_ReturnsExpectedResult()
        {
            var consumerId = Guid.NewGuid();

            var firstInstanceDate = new BorrowerDisclosureDates()
            {
                IssuedDate = DateTime.Parse("5/1/2018").Date
            };

            var secondInstanceDate = new BorrowerDisclosureDates()
            {
                IssuedDate = DateTime.Parse("5/1/2018").Date
            };

            var firstInstanceDates = new[] { new KeyValuePair<Guid, BorrowerDisclosureDates>(consumerId, firstInstanceDate) }.ToList().AsReadOnly();
            var secondInstanceDates = new[] { new KeyValuePair<Guid, BorrowerDisclosureDates>(consumerId, secondInstanceDate) }.ToList().AsReadOnly();

            var result = CfpbRequiredDateUtils.AreBorrowerLevelDatesEqual(firstInstanceDates, secondInstanceDates);
            Assert.IsTrue(result);
        }

        [Test]
        public void AreConsumerTypeMappingsEqual_DifferentCount_ReturnsExpectedResult()
        {
            var firstInstance = new Dictionary<DataObjectIdentifier<DataObjectKind.Consumer, Guid>, E_aTypeT>();
            var secondInstance = new Dictionary<DataObjectIdentifier<DataObjectKind.Consumer, Guid>, E_aTypeT>()
            {
                [DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(Guid.NewGuid())] = E_aTypeT.Individual
            };

            var result = CfpbRequiredDateUtils.AreConsumerTypeMappingsEqual(firstInstance, secondInstance);
            Assert.IsFalse(result);
        }

        [Test]
        public void AreConsumerTypeMappingsEqual_SameCountDifferentValues_ReturnsExpectedResult()
        {
            var consumerId = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(Guid.NewGuid());
            var firstInstance = new Dictionary<DataObjectIdentifier<DataObjectKind.Consumer, Guid>, E_aTypeT>()
            {
                [consumerId] = E_aTypeT.Individual
            };
            var secondInstance = new Dictionary<DataObjectIdentifier<DataObjectKind.Consumer, Guid>, E_aTypeT>()
            {
                [consumerId] = E_aTypeT.CoSigner
            };

            var result = CfpbRequiredDateUtils.AreConsumerTypeMappingsEqual(firstInstance, secondInstance);
            Assert.IsFalse(result);
        }

        [Test]
        public void AreConsumerTypeMappingsEqual_SameCountSameValues_ReturnsExpectedResult()
        {
            var consumerId = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(Guid.NewGuid());
            var firstInstance = new Dictionary<DataObjectIdentifier<DataObjectKind.Consumer, Guid>, E_aTypeT>()
            {
                [consumerId] = E_aTypeT.Individual
            };
            var secondInstance = new Dictionary<DataObjectIdentifier<DataObjectKind.Consumer, Guid>, E_aTypeT>()
            {
                [consumerId] = E_aTypeT.Individual
            };

            var result = CfpbRequiredDateUtils.AreConsumerTypeMappingsEqual(firstInstance, secondInstance);
            Assert.IsTrue(result);
        }

        [Test]
        public void NullableDateToString_NullDate_ReturnsNull()
        {
            var result = CfpbRequiredDateUtils.NullableDateToString(null);
            Assert.IsNull(result);
        }

        [Test]
        public void NullableDateToString_InvalidDate_ReturnsNull()
        {
            var result = CfpbRequiredDateUtils.NullableDateToString(DateTime.MinValue);
            Assert.IsNull(result);

            result = CfpbRequiredDateUtils.NullableDateToString(DateTime.MaxValue);
            Assert.IsNull(result);
        }

        [Test]
        public void NullableDateToString_ValidDate_ReturnsExpectedResult()
        {
            var expected = "5/01/2018";
            var result = CfpbRequiredDateUtils.NullableDateToString(DateTime.Parse(expected));
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void StringToNullableDate_NullBlankString_ReturnsNull()
        {
            var result = CfpbRequiredDateUtils.StringToNullableDate(null);
            Assert.IsNull(result);

            result = CfpbRequiredDateUtils.StringToNullableDate(string.Empty);
            Assert.IsNull(result);
        }

        [Test]
        public void StringToNullableDate_InvalidDateString_ReturnsNull()
        {
            var result = CfpbRequiredDateUtils.StringToNullableDate(DateTime.MinValue.ToString());
            Assert.IsNull(result);

            result = CfpbRequiredDateUtils.StringToNullableDate(DateTime.MaxValue.ToString());
            Assert.IsNull(result);
        }

        [Test]
        public void StringToNullableDate_ValidDateString_ReturnsExpectedResult()
        {
            var expected = DateTime.Parse("5/1/2018");
            var result = CfpbRequiredDateUtils.StringToNullableDate(expected.ToShortDateString());

            Assert.IsTrue(result.HasValue);
            Assert.AreEqual(expected, result.Value);
        }

        [Test]
        public void DeepCopyDisclosureDatesByConsumerId_PerformsDeepCopy()
        {
            var consumerId = Guid.NewGuid();

            var consumerName = "Joe Schmoe";
            var excludeFromCalculations = true;
            var issuedDate = DateTime.Parse("5/1/2018").Date;
            var deliveryMethod = E_DeliveryMethodT.Email;
            var receivedDate = DateTime.Parse("5/8/2018").Date;
            var signedDate = DateTime.Parse("5/15/2018").Date;

            var dates = new BorrowerDisclosureDates()
            {
                ConsumerName = consumerName,
                ExcludeFromCalculations = excludeFromCalculations,
                IssuedDate = issuedDate,
                DeliveryMethod = deliveryMethod,
                ReceivedDate = receivedDate,
                SignedDate = signedDate
            };

            var dateList = new[] { new KeyValuePair<Guid, BorrowerDisclosureDates>(consumerId, dates) };
            var copy = CfpbRequiredDateUtils.DeepCopyDisclosureDatesByConsumerId(dateList);

            Assert.AreEqual(1, copy.Count());

            var datesCopy = copy.First();

            Assert.IsFalse(object.ReferenceEquals(dates, datesCopy));

            Assert.AreEqual(consumerName, datesCopy.Value.ConsumerName);
            Assert.AreEqual(consumerId, datesCopy.Key);
            Assert.AreEqual(excludeFromCalculations, datesCopy.Value.ExcludeFromCalculations);
            Assert.AreEqual(issuedDate, datesCopy.Value.IssuedDate);
            Assert.AreEqual(deliveryMethod, datesCopy.Value.DeliveryMethod);
            Assert.AreEqual(receivedDate, datesCopy.Value.ReceivedDate);
            Assert.AreEqual(signedDate, datesCopy.Value.SignedDate);
        }
    }
}
