﻿namespace LendingQB.Test.Developers.Disclosure
{
    using System;
    using System.Linq;
    using Fakes;
    using global::DataAccess;
    using LendersOffice.Common;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public class ClosingDisclosureDatesInfoTest
    {
        private const string jsonWithoutVersion = @"
            {
              ""closingDisclosureDatesList"": [
                {
                  ""IsPostClosing"": false,
                  ""archiveDate"": ""2\/8\/2017"",
                  ""archiveId"": ""11111111-1111-1111-1111-111111111111"",
                  ""createdDate"": ""2\/8\/2017"",
                  ""issuedDate"": ""2\/9\/2017"",
                  ""deliveryMethod"": 1,
                  ""disableManualArchiveAssociation"": false,
                  ""isCandidateInitial"": false,
                  ""isDisclosurePostClosingDueToCureForToleranceViolation"": false,
                  ""isDisclosurePostClosingDueToNonNumericalClericalError"": false,
                  ""isDisclosurePostClosingDueToNumericalChangeInAmountPaidByBorrower"": false,
                  ""isDisclosurePostClosingDueToNumericalChangeInAmountPaidBySeller"": false,
                  ""isFinal"": false,
                  ""isInitial"": true,
                  ""isManual"": true,
                  ""isPreview"": false,
                  ""lastDisclosedTRIDLoanProductDescription"": """",
                  ""postConsummationKnowledgeOfEventDate"": """",
                  ""postConsummationRedisclosureReasonDate"": """",
                  ""receivedDate"": ""2\/10\/2017"",
                  ""signedDate"": ""2\/11\/2017"",
                  ""transactionId"": """",
                  ""uniqueId"": ""9e641974-11d5-43b1-a87e-ba326b77fce0""
                },
                {
                  ""IsPostClosing"": false,
                  ""archiveDate"": ""2\/8\/2017"",
                  ""archiveId"": ""22222222-2222-2222-2222-222222222222"",
                  ""createdDate"": ""2\/8\/2017"",
                  ""issuedDate"": ""2\/9\/2017"",
                  ""deliveryMethod"": 2,
                  ""disableManualArchiveAssociation"": false,
                  ""isCandidateInitial"": false,
                  ""isDisclosurePostClosingDueToCureForToleranceViolation"": false,
                  ""isDisclosurePostClosingDueToNonNumericalClericalError"": false,
                  ""isDisclosurePostClosingDueToNumericalChangeInAmountPaidByBorrower"": false,
                  ""isDisclosurePostClosingDueToNumericalChangeInAmountPaidBySeller"": false,
                  ""isFinal"": false,
                  ""isInitial"": false,
                  ""isManual"": true,
                  ""isPreview"": false,
                  ""lastDisclosedTRIDLoanProductDescription"": """",
                  ""postConsummationKnowledgeOfEventDate"": """",
                  ""postConsummationRedisclosureReasonDate"": """",
                  ""receivedDate"": ""2\/10\/2017"",
                  ""signedDate"": ""2\/11\/2017"",
                  ""transactionId"": """",
                  ""uniqueId"": ""33333333-3333-3333-3333-333333333333""
                }
              ]
            }";

        [Test]
        public void FromJson_EmptyJson_NoExceptionIsNull()
        {
            var json = "";
            var dates = ClosingDisclosureDatesInfo.FromJson(json, null);
            Assert.AreEqual(null, dates);
        }

        [Test]
        public void FromJson_JsonContainsNoVersion_DefaultsToVersion0()
        {
            var dates = ClosingDisclosureDatesInfo.FromJson(jsonWithoutVersion, metadata: null);

            Assert.AreEqual(0, dates.Version);
        }

        [Test]
        public void FromJson_WithArchiveSpecified_PassesOnArchiveToDates()
        {
            ////var archive = Substitute.For<IClosingCostArchive>();
            ////archive.Apr.Returns(5);
            var archive = new FakeClosingCostArchive(5);

            var metadata = new ClosingDisclosureDatesMetadata(id => archive);
            
            var dates = ClosingDisclosureDatesInfo.FromJson(jsonWithoutVersion, metadata);

            Assert.AreEqual(2, dates.ClosingDisclosureDatesList.Count);
            foreach (var le in dates.ClosingDisclosureDatesList)
            {
                Assert.AreEqual(5m, le.LqbApr);
            }
        }

        [Test]
        public void FromJson_AfterSerializeVersion0Collection_IsMostRecentVersion()
        {
            //var archive = Substitute.For<IClosingCostArchive>();
            //archive.Apr.Returns(5);
            var archive = new FakeClosingCostArchive(5);

            var metadata = new ClosingDisclosureDatesMetadata(id => archive);
            
            var dates = ClosingDisclosureDatesInfo.FromJson(jsonWithoutVersion, metadata);
            var updatedJson = ObsoleteSerializationHelper.JsonSerialize(dates);

            dates = ClosingDisclosureDatesInfo.FromJson(updatedJson, metadata: null);

            Assert.AreEqual(ClosingDisclosureDatesInfo.GetMostRecentVersion(), dates.Version);
        }

        [Test]
        public void FromJson_PriorToBorrowerLevelDates_UsesStoredValues()
        {
            var dates = ClosingDisclosureDatesInfo.FromJson(jsonWithoutVersion, metadata: null);

            var firstDate = dates.ClosingDisclosureDatesList.First();
            Assert.AreEqual(DateTime.Parse("2/9/2017"), firstDate.IssuedDate);
            Assert.AreEqual(E_DeliveryMethodT.Email, firstDate.DeliveryMethod);
            Assert.AreEqual(DateTime.Parse("2/10/2017"), firstDate.ReceivedDate);
            Assert.AreEqual(DateTime.Parse("2/11/2017"), firstDate.SignedDate);

            var secondDate = dates.ClosingDisclosureDatesList.Last();
            Assert.AreEqual(DateTime.Parse("2/9/2017"), secondDate.IssuedDate);
            Assert.AreEqual(E_DeliveryMethodT.Fax, secondDate.DeliveryMethod);
            Assert.AreEqual(DateTime.Parse("2/10/2017"), secondDate.ReceivedDate);
            Assert.AreEqual(DateTime.Parse("2/11/2017"), secondDate.SignedDate);
        }

        [Test]
        public void DuplicatePreservingIds_Always_MaintainsIds()
        {
            var dates = ClosingDisclosureDatesInfo.FromJson(jsonWithoutVersion, metadata: null);
        
            Assert.AreEqual(new Guid("9e641974-11d5-43b1-a87e-ba326b77fce0"), dates.ClosingDisclosureDatesList[0].UniqueId);
            Assert.AreEqual(new Guid("33333333-3333-3333-3333-333333333333"), dates.ClosingDisclosureDatesList[1].UniqueId);
        }

        [Test]
        public void Create_Always_AssignsMostRecentVersion()
        {
            var dates = ClosingDisclosureDatesInfo.Create();

            Assert.AreEqual(ClosingDisclosureDatesInfo.GetMostRecentVersion(), dates.Version);
        }
    }
}
