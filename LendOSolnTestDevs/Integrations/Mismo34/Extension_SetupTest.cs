﻿namespace LendingQB.Test.Developers.Integrations.Mismo34
{
    using System.Linq;
    using System.Xml.Serialization;
    using global::LendingQB.Test.Developers.Utils;
    using NUnit.Framework;

    /// <summary>
    /// Holds tests for checking if the MISMO 3.4 extensions are set up correctly.
    /// </summary>
    [TestFixture]
    public class Extension_SetupTest
    {
        /// <summary>
        /// Checks that the XmlElement attributes in the OTHER_BASE child classes have not set a valid Order property.
        /// </summary>
        [Test]
        public static void XmlElementHasNoOrder_OtherBaseChildren_Passes()
        {
            CheckForOrderAttribute<Mismo3Specification.Version4Schema.OTHER_BASE>();
        }

        /// <summary>
        /// Checks that the XmlElement attributes in the MISMO_BASE child classes have not set a valid Order property.
        /// </summary>
        [Test]
        public static void XmlElementHasNoOrder_MismoBaseChildren_Passes()
        {
            CheckForOrderAttribute<Mismo3Specification.Version4Schema.MISMO_BASE>();
        }

        /// <summary>
        /// Ensures that the child classes of OTHER_BASE are serializable via XmlSerializer.
        /// </summary>
        [Test]
        public static void AttemptCreateSerializer_OtherBase_NoExceptions()
        {
            AttemptCreateSerializer<Mismo3Specification.Version4Schema.OTHER_BASE>();
        }

        /// <summary>
        /// Ensures that the child classes of OTHER_BASE are serializable via XmlSerializer.
        /// </summary>
        [Test]
        public static void AttemptCreateSerializer_MismoBase_NoExceptions()
        {
            AttemptCreateSerializer<Mismo3Specification.Version4Schema.OTHER_BASE>();
        }

        /// <summary>
        /// Checks that the XmlElement attributes in the BaseType have not set a valid Order property.
        /// </summary>
        /// <typeparam name="BaseType">The base type.</typeparam>
        private static void CheckForOrderAttribute<BaseType>() where BaseType : class
        {
            var childClassTypes = TestUtilities.GetTypes<BaseType>();

            foreach (var type in childClassTypes)
            {
                var xmlElementMembers = type.GetMembers().Select(prop => (XmlElementAttribute[])prop.GetCustomAttributes(typeof(XmlElementAttribute), false)).Where(items => items.Any());
                Assert.IsFalse(xmlElementMembers.Any(attributes => attributes.Any(attribute => attribute.Order != -1)), $"Type: {type}");

                var xmlElementProperties = type.GetProperties().Select(prop => (XmlElementAttribute[])prop.GetCustomAttributes(typeof(XmlElementAttribute), false)).Where(items => items.Any());
                Assert.IsFalse(xmlElementProperties.Any(attributes => attributes.Any(attribute => attribute.Order != -1)), $"Type: {type}");
            }
        }

        /// <summary>
        /// Ensures that the child classes of the BaseType are serializable via XmlSerializer.
        /// </summary>
        /// <typeparam name="BaseType">The base type.</typeparam>
        private static void AttemptCreateSerializer<BaseType>() where BaseType : class
        {
            var childClassTypes = TestUtilities.GetTypes<BaseType>();

            foreach (var childClassType in childClassTypes)
            {
                Assert.DoesNotThrow(() =>
                {
                    var serializer = new XmlSerializer(childClassType);
                });
            }
        }
    }
}
