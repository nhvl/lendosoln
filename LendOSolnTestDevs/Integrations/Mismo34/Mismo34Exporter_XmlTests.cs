﻿namespace LendingQB.Test.Developers.Integrations.Mismo34
{
    using System.Collections.Generic;
    using System.Xml.Linq;
    using System.Xml.XPath;
    using global::DataAccess;
    using LendersOffice.Conversions.Mismo3.Version4;
    using Mismo3Specification;
    using Mismo3Specification.Version4Schema;
    using NUnit.Framework;

    /// <summary>
    /// Defines tests specific to the XML serialization of the MISMO 3.4 request.
    /// Use this class for schema validations, XML namespaces, attributes, 
    ///   and formatting details like whether a container serializes.
    /// Most exporter logic should be handled in the container tests;
    ///   avoid duplicating that stuff here.
    /// </summary>
    [TestFixture]
    [Category(CategoryConstants.IntegrationTest)]
    public class Mismo34Exporter_XmlTests
    {
        /// <summary>
        /// Ensures that the basic exporter serializes the base MESSAGE element without
        /// XML declaration and with the right namespace.
        /// </summary>
        [Test]
        public static void SerializePayload_DefaultLoan_ContainsMessage()
        {
            using (var tempLoan = Utils.LoanForIntegrationTest.Create(Utils.TestAccountType.LoTest001, useUladDataLayer: true))
            {
                var loan = tempLoan.CreateNewPageData();
                loan.InitLoad();
                loan.SetFormatTarget(FormatTarget.MismoClosing);
                var exporter = new Mismo34Exporter(loan);
                var mismoString = exporter.SerializePayload();

                var parsedXml = XDocument.Parse(mismoString);

                Assert.IsNull(parsedXml.Declaration);
                Assert.AreEqual(XName.Get("MESSAGE", Mismo3Constants.MismoNamespace), parsedXml.Root.Name);
                Assert.AreEqual(parsedXml.Root.Attribute("MISMOLogicalDataDictionaryIdentifier").Value, "3.4.0[B324]");
                Assert.AreEqual(parsedXml.Root.Attribute("MISMOReferenceModelIdentifier").Value, "3.4.0[B324]");
            }
        }

        [Test]
        public static void SerializePayload_DefaultLoan_ContainsSubjectLoan()
        {
            using (var tempLoan = Utils.LoanForIntegrationTest.Create(Utils.TestAccountType.LoTest001, useUladDataLayer: true))
            {
                var loan = tempLoan.CreateNewPageData();
                loan.InitLoad();
                loan.SetFormatTarget(FormatTarget.MismoClosing);
                var exporter = new Mismo34Exporter(loan);
                var mismoString = exporter.SerializePayload();

                var parsedXml = XDocument.Parse(mismoString);
                var nsManager = new System.Xml.XmlNamespaceManager(new System.Xml.NameTable());
                nsManager.AddNamespace("m", Mismo3Constants.MismoNamespace);
                var loanElement = parsedXml.XPathSelectElement("//m:MESSAGE/m:DEAL_SETS/m:DEAL_SET/m:DEALS/m:DEAL/m:LOANS/m:LOAN", nsManager);

                Assert.IsNotNull(loanElement);
                Assert.AreEqual(loanElement.Attribute("LoanRoleType").Value, "SubjectLoan");
                Assert.AreEqual(loanElement.Attribute("SequenceNumber").Value, "1");
            }
        }
    }
}
