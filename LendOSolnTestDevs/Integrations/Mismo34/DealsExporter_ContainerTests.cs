﻿namespace LendingQB.Test.Developers.Integrations.Mismo34
{
    using global::DataAccess;
    using LendersOffice.Conversions.Mismo3.Version4;
    using NUnit.Framework;

    /// <summary>
    /// Verifies that the proper values are set and the proper containers added by the
    /// PARTIES container exporter.
    /// </summary>
    [TestFixture]
    [Category(CategoryConstants.IntegrationTest)]
    public class DealsBuilder_ContainerTests
    {
        /// <summary>
        /// Makes sure the first DEAL container has the expected sub-containers populated.
        /// </summary>
        [Test]
        public static void DealsContainer_DefaultLoan_SubElementsPresent()
        {
            using (var tempLoan = Utils.LoanForIntegrationTest.Create(Utils.TestAccountType.LoTest001, useUladDataLayer: true))
            {
                var loan = tempLoan.CreateNewPageData();
                loan.InitLoad();
                loan.SetFormatTarget(FormatTarget.MismoClosing);
                var options = new Mismo34ExporterOptions();
                var exportData = new Mismo34ExporterCache(loan, options);
                var dealExporter = new DealBuilder(loan, exportData);

                var dealContainer = dealExporter.CreateContainer(sequenceNumber: 1);

                Assert.IsNotNull(dealContainer.Loans);

                // These containers are non-null, but won't be serialized.
                Assert.IsNotNull(dealContainer.Parties);
                Assert.IsNotNull(dealContainer.Relationships);
            }
        }
    }
}
