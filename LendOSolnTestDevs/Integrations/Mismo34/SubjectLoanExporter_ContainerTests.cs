﻿namespace LendingQB.Test.Developers.Integrations.Mismo34
{
    using System.Collections.Generic;
    using global::DataAccess;
    using LendersOffice.Conversions.Mismo3.Version4;
    using Mismo3Specification.Version4Schema;
    using NUnit.Framework;

    /// <summary>
    /// Verifies that the proper values are set and the proper containers added by the
    /// DEALS container exporter.
    /// </summary>
    [TestFixture]
    [Category(CategoryConstants.IntegrationTest)]
    public class SubjectLoanBuilder_ContainerTests
    {
        /// <summary>
        /// Make sure the LOAN container contains the correct properties.
        /// </summary>
        [Test]
        public static void LoanContainer_DefaultLoan_SetsValues()
        {
            using (var tempLoan = Utils.LoanForIntegrationTest.Create(Utils.TestAccountType.LoTest001, useUladDataLayer: true))
            {
                var loan = tempLoan.CreateNewPageData();
                loan.InitLoad();
                loan.SetFormatTarget(FormatTarget.MismoClosing);
                var options = new Mismo34ExporterOptions();
                var exportData = new Mismo34ExporterCache(loan, options);
                var loanExporter = new SubjectLoanBuilder(
                    dataLoan: loan,
                    exportData: exportData,
                    sequenceNumber: 1);

                var loanContainer = loanExporter.CreateContainer();

                Assert.AreEqual(loanContainer.LoanRoleType, LoanRoleBase.SubjectLoan);
                Assert.AreEqual(1, loanContainer.SequenceNumber);
            }
        }
    }
}
