﻿namespace LendingQB.Test.Developers.Integrations.Mismo34
{
    using System.Collections.Generic;
    using global::DataAccess;
    using LendersOffice.Conversions.Mismo3.Version4;
    using Mismo3Specification.Version4Schema;
    using NUnit.Framework;

    /// <summary>
    /// Verifies that the proper values are set and the proper containers added by the
    /// PARTIES container exporter.
    /// </summary>
    [TestFixture]
    [Category(CategoryConstants.IntegrationTest)]
    public class PartiesBuilder_ContainerTests
    {
        [Test]
        public static void PartiesContainer_DefaultLoan_NonEmptyList()
        {
            using (var tempLoan = Utils.LoanForIntegrationTest.Create(Utils.TestAccountType.LoTest001, useUladDataLayer: true))
            {
                var loan = tempLoan.CreateNewPageData();
                loan.InitLoad();
                loan.SetFormatTarget(FormatTarget.MismoClosing);
                var options = new Mismo34ExporterOptions();
                var exportData = new Mismo34ExporterCache(loan, options);
                var partiesExporter = new PartiesBuilder(loan, exportData);

                var partiesContainer = partiesExporter.CreateContainer();

                // Empty for now, but this test will change as we implement more.
                CollectionAssert.IsNotEmpty(partiesContainer.PartyList);
            }
        }
    }
}
