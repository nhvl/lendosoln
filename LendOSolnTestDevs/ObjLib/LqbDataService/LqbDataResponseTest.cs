﻿namespace LendingQB.Test.Developers.ObjLib.LqbDataService
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Core.Commands;
    using Core.Data;
    using global::DataAccess;
    using global::DataAccess.PathDispatch;
    using LendersOffice.ObjLib.LqbDataService;
    using LqbGrammar.DataTypes.PathDispatch;
    using Newtonsoft.Json.Linq;
    using NUnit.Framework;
    using LqbGrammar.DataTypes;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public class LqbDataResponseTest
    {
        [Test]
        public void ToJObject_NoResponses_ReturnsExpectedFormat()
        {
            var response = new LqbDataResponse();

            var json = response.ToJObject();

            var expectedResponseToken = json.SelectToken("data.responses");
            Assert.AreEqual(0, expectedResponseToken.Children().Count());
        }

        [Test]
        public void ToJObject_WithGetResponse_ReturnsExpectedFormat()
        {
            var root = this.GetGetResponse();
            var response = this.CreateResponse(null, null, root, null, null, null);
            this.CheckGetResponse(response);
        }

        [Test]
        public void ToJObject_GetResponseWithEmptyCollection_ReturnsExpectedFormat()
        {
            var root = new GetResponseNode(
                pathComponent: null,
                value: null,
                children: new List<GetResponseNode>()
                {
                    new GetResponseNode(
                        pathComponent: new DataPathBasicElement("loan"),
                        value: null,
                        children: new List<GetResponseNode>()
                        {
                            new GetResponseNode(
                                pathComponent: new DataPathBasicElement("Assets"),
                                children: null,
                                value: NSubstitute.Substitute.For<IPathResolvableCollection>())
                        })
                });
            var response = new LqbDataResponse();
            response.GetResponse = ServiceMessage.Create("1", "get", root);

            var json = response.ToJObject();

            var expectedResponseToken = json.SelectToken("data.responses");
            Assert.AreEqual(1, expectedResponseToken.Children().Count());

            var assetsCollection = json.SelectToken("data.responses[?(@.name == 'get')].returns.loan.Assets");
            Assert.AreEqual(JTokenType.Object, assetsCollection.Type);
        }

        [Test]
        public void ToJObject_AddEntityResponse()
        {
            var ownerId = Guid.NewGuid().ToString("D");
            var assetId = Guid.NewGuid().ToString("D");

            var root = this.GetAddEntityResponse(ownerId, assetId);
            var response = this.CreateResponse(root, null, null, null, null, null);
            this.CheckAddEntityResponse(ownerId, assetId, response);
        }

        [Test]
        public void ToJObject_AddEntityAndGet()
        {
            var ownerId = Guid.NewGuid().ToString("D");
            var assetId = Guid.NewGuid().ToString("D");

            var addEntity = this.GetAddEntityResponse(ownerId, assetId);
            var getData = this.GetGetResponse();
            var response = this.CreateResponse(addEntity, null, getData, null, null, null);

            this.CheckAddEntityResponse(ownerId, assetId, response);
            this.CheckGetResponse(response);
        }

        [Test]
        public void ToJObject_TwoAddEntityResponses_ReturnsTwoResponseObjects()
        {
            var ownerId1 = Guid.NewGuid().ToString();
            var assetId1 = Guid.NewGuid().ToString();
            var response1 = this.GetAddEntityResponse(ownerId1, assetId1, "1");

            var ownerId2 = Guid.NewGuid().ToString();
            var assetId2 = Guid.NewGuid().ToString();
            var response2 = this.GetAddEntityResponse(ownerId2, assetId2, "2");

            var response = new LqbDataResponse();
            response.AddEntityResponses = new[] { response1, response2 };
            var responseJson = response.ToJObject();

            Assert.AreEqual(2, responseJson.SelectToken("data.responses").Children().Count());

            var addEntityToken = responseJson.SelectToken("data.responses[?(@.operationId == '1')].returns");
            Assert.AreEqual(4, addEntityToken.Children().Count());
            Assert.AreEqual("loan", (string)addEntityToken["path"]);
            Assert.AreEqual("Assets", (string)addEntityToken["collectionName"]);
            Assert.AreEqual(ownerId1, (string)addEntityToken["ownerId"]);
            Assert.AreEqual(assetId1, (string)addEntityToken["id"]);

            addEntityToken = responseJson.SelectToken("data.responses[?(@.operationId == '2')].returns");
            Assert.AreEqual(4, addEntityToken.Children().Count());
            Assert.AreEqual("loan", (string)addEntityToken["path"]);
            Assert.AreEqual("Assets", (string)addEntityToken["collectionName"]);
            Assert.AreEqual(ownerId2, (string)addEntityToken["ownerId"]);
            Assert.AreEqual(assetId2, (string)addEntityToken["id"]);
        }


        [Test]
        public void ToJObject_WithGetAssetTotalsResponseForUladApp_ReturnsExpectedFormat()
        {
            var command = new GetTotals<DataObjectKind.Asset>(DataPath.Create("loan"), TotalsType.UladApplication, new Guid("11111111-1111-1111-1111-111111111111"));
            var totals = new AssetTotals(subtotalReo: Money.Create(1).Value, subtotalLiquid: Money.Create(2).Value, subtotalIlliquid: Money.Create(3).Value);
            var assetTotalsResponse = ServiceMessage.Create("2", "getAssetTotals", new GetAssetTotalsResponse(command, totals, new FormatEntityAsString(FormatTarget.Webform, FormatDirection.ToRep)));

            var response = this.CreateResponse(null, null, null, assetTotalsResponse, null, null);
            var responseJson = response.ToJObject();
            var commandResponse = responseJson.SelectToken("data.responses[?(@.name == 'getAssetTotals')].returns");

            Assert.AreEqual("2", (string)responseJson.SelectToken("data.responses[?(@.name == 'getAssetTotals')].operationId"));

            Assert.AreEqual("loan", (string)commandResponse.SelectToken("path"));
            Assert.AreEqual("UladApplication", (string)commandResponse.SelectToken("type"));
            Assert.AreEqual("11111111-1111-1111-1111-111111111111", (string)commandResponse.SelectToken("id"));
            Assert.AreEqual("$1.00", (string)commandResponse.SelectToken("subtotalReo"));
            Assert.AreEqual("$2.00", (string)commandResponse.SelectToken("subtotalLiquid"));
            Assert.AreEqual("$3.00", (string)commandResponse.SelectToken("subtotalIlliquid"));
            Assert.AreEqual("$6.00", (string)commandResponse.SelectToken("total"));
        }

        [Test]
        public void ToJObject_WithGetAssetTotalsResponseForLegacyApp_ReturnsExpectedFormat()
        {
            var command = new GetTotals<DataObjectKind.Asset>(DataPath.Create("loan"), TotalsType.LegacyApplication, new Guid("11111111-1111-1111-1111-111111111111"));
            var totals = new AssetTotals(subtotalReo: Money.Create(1).Value, subtotalLiquid: Money.Create(2).Value, subtotalIlliquid: Money.Create(3).Value);
            var assetTotalsResponse = ServiceMessage.Create("3", "getAssetTotals", new GetAssetTotalsResponse(command, totals, new FormatEntityAsString(FormatTarget.Webform, FormatDirection.ToRep)));

            var response = this.CreateResponse(null, null, null, assetTotalsResponse, null, null);
            var responseJson = response.ToJObject();
            var commandResponse = responseJson.SelectToken("data.responses[?(@.name == 'getAssetTotals')].returns");

            Assert.AreEqual("3", (string)responseJson.SelectToken("data.responses[?(@.name == 'getAssetTotals')].operationId"));

            Assert.AreEqual("loan", (string)commandResponse.SelectToken("path"));
            Assert.AreEqual("LegacyApplication", (string)commandResponse.SelectToken("type"));
            Assert.AreEqual("11111111-1111-1111-1111-111111111111", (string)commandResponse.SelectToken("id"));
            Assert.AreEqual("$1.00", (string)commandResponse.SelectToken("subtotalReo"));
            Assert.AreEqual("$2.00", (string)commandResponse.SelectToken("subtotalLiquid"));
            Assert.AreEqual("$3.00", (string)commandResponse.SelectToken("subtotalIlliquid"));
            Assert.AreEqual("$6.00", (string)commandResponse.SelectToken("total"));
        }

        [Test]
        public void ToJObject_WithGetAssetTotalsResponseForLoan_ReturnsExpectedFormat()
        {
            var command = new GetTotals<DataObjectKind.Asset>(DataPath.Create("loan"), TotalsType.Loan, id: null);
            var totals = new AssetTotals(subtotalReo: Money.Create(1).Value, subtotalLiquid: Money.Create(2).Value, subtotalIlliquid: Money.Create(3).Value);
            var assetTotalsResponse = ServiceMessage.Create("4", "getAssetTotals", new GetAssetTotalsResponse(command, totals, new FormatEntityAsString(FormatTarget.Webform, FormatDirection.ToRep)));

            var response = this.CreateResponse(null, null, null, assetTotalsResponse, null, null);
            var responseJson = response.ToJObject();
            var commandResponse = responseJson.SelectToken("data.responses[?(@.name == 'getAssetTotals')].returns");

            Assert.AreEqual("4", (string)responseJson.SelectToken("data.responses[?(@.name == 'getAssetTotals')].operationId"));

            Assert.AreEqual(6, commandResponse.Children().Count());
            Assert.AreEqual("loan", (string)commandResponse.SelectToken("path"));
            Assert.AreEqual("Loan", (string)commandResponse.SelectToken("type"));
            Assert.AreEqual("$1.00", (string)commandResponse.SelectToken("subtotalReo"));
            Assert.AreEqual("$2.00", (string)commandResponse.SelectToken("subtotalLiquid"));
            Assert.AreEqual("$3.00", (string)commandResponse.SelectToken("subtotalIlliquid"));
            Assert.AreEqual("$6.00", (string)commandResponse.SelectToken("total"));
        }

        [Test]
        public void ToJObject_WithGetAssetTotalsResponseForConsumer_ReturnsExpectedFormat()
        {
            var command = new GetTotals<DataObjectKind.Asset>(DataPath.Create("loan"), TotalsType.Consumer, new Guid("11111111-1111-1111-1111-111111111111"));
            var totals = new AssetTotals(subtotalReo: Money.Create(1).Value, subtotalLiquid: Money.Create(2).Value, subtotalIlliquid: Money.Create(3).Value);
            var assetTotalsResponse = ServiceMessage.Create("5", "getAssetTotals", new GetAssetTotalsResponse(command, totals, new FormatEntityAsString(FormatTarget.Webform, FormatDirection.ToRep)));

            var response = this.CreateResponse(null, null, null, assetTotalsResponse, null, null);
            var responseJson = response.ToJObject();
            var commandResponse = responseJson.SelectToken("data.responses[?(@.name == 'getAssetTotals')].returns");

            Assert.AreEqual("5", (string)responseJson.SelectToken("data.responses[?(@.name == 'getAssetTotals')].operationId"));

            Assert.AreEqual("loan", (string)commandResponse.SelectToken("path"));
            Assert.AreEqual("Consumer", (string)commandResponse.SelectToken("type"));
            Assert.AreEqual("11111111-1111-1111-1111-111111111111", (string)commandResponse.SelectToken("id"));
            Assert.AreEqual("$1.00", (string)commandResponse.SelectToken("subtotalReo"));
            Assert.AreEqual("$2.00", (string)commandResponse.SelectToken("subtotalLiquid"));
            Assert.AreEqual("$3.00", (string)commandResponse.SelectToken("subtotalIlliquid"));
            Assert.AreEqual("$6.00", (string)commandResponse.SelectToken("total"));
        }

        [Test]
        public void ToJObject_WithGetAssetTotalsResponsesForLoanAndConsumer_ReturnsExpectedFormat()
        {
            var consumerCommand = new GetTotals<DataObjectKind.Asset>(DataPath.Create("loan"), TotalsType.Consumer, new Guid("11111111-1111-1111-1111-111111111111"));
            var consumerTotals = new AssetTotals(subtotalReo: Money.Create(1).Value, subtotalLiquid: Money.Create(2).Value, subtotalIlliquid: Money.Create(3).Value);
            var consumerAssetTotalsResponse = ServiceMessage.Create("6", "getAssetTotals", new GetAssetTotalsResponse(consumerCommand, consumerTotals, new FormatEntityAsString(FormatTarget.Webform, FormatDirection.ToRep)));
            var loanCommand = new GetTotals<DataObjectKind.Asset>(DataPath.Create("loan"), TotalsType.Loan, id: null);
            var loanTotals = new AssetTotals(subtotalReo: Money.Create(4).Value, subtotalLiquid: Money.Create(5).Value, subtotalIlliquid: Money.Create(6).Value);
            var loanAssetTotalsResponse = ServiceMessage.Create("7", "getAssetTotals", new GetAssetTotalsResponse(loanCommand, loanTotals, new FormatEntityAsString(FormatTarget.Webform, FormatDirection.ToRep)));

            var response = new LqbDataResponse();
            response.GetAssetTotalsResponses = new[] { consumerAssetTotalsResponse, loanAssetTotalsResponse };
            var responseJson = response.ToJObject();

            var expectedResponseToken = responseJson.SelectToken("data.responses");
            Assert.AreEqual(2, expectedResponseToken.Children().Count());

            var commandResponse = expectedResponseToken.SelectToken("[?(@.operationId == '6')].returns");
            Assert.AreEqual("loan", (string)commandResponse.SelectToken("path"));
            Assert.AreEqual("Consumer", (string)commandResponse.SelectToken("type"));
            Assert.AreEqual("11111111-1111-1111-1111-111111111111", (string)commandResponse.SelectToken("id"));
            Assert.AreEqual("$1.00", (string)commandResponse.SelectToken("subtotalReo"));
            Assert.AreEqual("$2.00", (string)commandResponse.SelectToken("subtotalLiquid"));
            Assert.AreEqual("$3.00", (string)commandResponse.SelectToken("subtotalIlliquid"));
            Assert.AreEqual("$6.00", (string)commandResponse.SelectToken("total"));

            commandResponse = expectedResponseToken.SelectToken("[?(@.operationId == '7')].returns");
            Assert.AreEqual("loan", (string)commandResponse.SelectToken("path"));
            Assert.AreEqual("Loan", (string)commandResponse.SelectToken("type"));
            Assert.AreEqual("$4.00", (string)commandResponse.SelectToken("subtotalReo"));
            Assert.AreEqual("$5.00", (string)commandResponse.SelectToken("subtotalLiquid"));
            Assert.AreEqual("$6.00", (string)commandResponse.SelectToken("subtotalIlliquid"));
            Assert.AreEqual("$15.00", (string)commandResponse.SelectToken("total"));
        }

        [Test]
        public void ToJObject_WithGetLiabilityTotalsResponseForUladApp_ReturnsExpectedFormat()
        {
            var command = new GetTotals<DataObjectKind.Liability>(DataPath.Create("loan"), TotalsType.UladApplication, new Guid("11111111-1111-1111-1111-111111111111"));
            var totals = new LiabilityTotals(balance: Money.Create(1).Value, payment: Money.Create(2).Value, paidOff: Money.Create(3).Value);
            var liabilityTotalsResponse = ServiceMessage.Create("2", "getLiabilityTotals", new GetLiabilityTotalsResponse(command, totals, new FormatEntityAsString(FormatTarget.Webform, FormatDirection.ToRep)));

            var response = this.CreateResponse(null, null, null, null, liabilityTotalsResponse, null);
            var responseJson = response.ToJObject();
            var commandResponse = responseJson.SelectToken("data.responses[?(@.name == 'getLiabilityTotals')].returns");

            Assert.AreEqual("2", (string)responseJson.SelectToken("data.responses[?(@.name == 'getLiabilityTotals')].operationId"));

            Assert.AreEqual("loan", (string)commandResponse.SelectToken("path"));
            Assert.AreEqual("UladApplication", (string)commandResponse.SelectToken("type"));
            Assert.AreEqual("11111111-1111-1111-1111-111111111111", (string)commandResponse.SelectToken("id"));
            Assert.AreEqual("$1.00", (string)commandResponse.SelectToken("balance"));
            Assert.AreEqual("$2.00", (string)commandResponse.SelectToken("payment"));
            Assert.AreEqual("$3.00", (string)commandResponse.SelectToken("paidOff"));
        }

        [Test]
        public void ToJObject_WithGetLiabilityTotalsResponseForLegacyApp_ReturnsExpectedFormat()
        {
            var command = new GetTotals<DataObjectKind.Liability>(DataPath.Create("loan"), TotalsType.LegacyApplication, new Guid("11111111-1111-1111-1111-111111111111"));
            var totals = new LiabilityTotals(balance: Money.Create(1).Value, payment: Money.Create(2).Value, paidOff: Money.Create(3).Value);
            var liabilityTotalsResponse = ServiceMessage.Create("3", "getLiabilityTotals", new GetLiabilityTotalsResponse(command, totals, new FormatEntityAsString(FormatTarget.Webform, FormatDirection.ToRep)));

            var response = this.CreateResponse(null, null, null, null, liabilityTotalsResponse, null);
            var responseJson = response.ToJObject();
            var commandResponse = responseJson.SelectToken("data.responses[?(@.name == 'getLiabilityTotals')].returns");

            Assert.AreEqual("3", (string)responseJson.SelectToken("data.responses[?(@.name == 'getLiabilityTotals')].operationId"));

            Assert.AreEqual("loan", (string)commandResponse.SelectToken("path"));
            Assert.AreEqual("LegacyApplication", (string)commandResponse.SelectToken("type"));
            Assert.AreEqual("11111111-1111-1111-1111-111111111111", (string)commandResponse.SelectToken("id"));
            Assert.AreEqual("$1.00", (string)commandResponse.SelectToken("balance"));
            Assert.AreEqual("$2.00", (string)commandResponse.SelectToken("payment"));
            Assert.AreEqual("$3.00", (string)commandResponse.SelectToken("paidOff"));
        }

        [Test]
        public void ToJObject_WithGetLiabilityTotalsResponseForLoan_ReturnsExpectedFormat()
        {
            var command = new GetTotals<DataObjectKind.Liability>(DataPath.Create("loan"), TotalsType.Loan, id: null);
            var totals = new LiabilityTotals(balance: Money.Create(1).Value, payment: Money.Create(2).Value, paidOff: Money.Create(3).Value);
            var liabilityTotalsResponse = ServiceMessage.Create("4", "getLiabilityTotals", new GetLiabilityTotalsResponse(command, totals, new FormatEntityAsString(FormatTarget.Webform, FormatDirection.ToRep)));

            var response = this.CreateResponse(null, null, null, null, liabilityTotalsResponse, null);
            var responseJson = response.ToJObject();
            var commandResponse = responseJson.SelectToken("data.responses[?(@.name == 'getLiabilityTotals')].returns");

            Assert.AreEqual("4", (string)responseJson.SelectToken("data.responses[?(@.name == 'getLiabilityTotals')].operationId"));

            Assert.AreEqual(5, commandResponse.Children().Count());
            Assert.AreEqual("loan", (string)commandResponse.SelectToken("path"));
            Assert.AreEqual("Loan", (string)commandResponse.SelectToken("type"));
            Assert.AreEqual("$1.00", (string)commandResponse.SelectToken("balance"));
            Assert.AreEqual("$2.00", (string)commandResponse.SelectToken("payment"));
            Assert.AreEqual("$3.00", (string)commandResponse.SelectToken("paidOff"));
        }

        [Test]
        public void ToJObject_WithGetLiabilityTotalsResponseForConsumer_ReturnsExpectedFormat()
        {
            var command = new GetTotals<DataObjectKind.Liability>(DataPath.Create("loan"), TotalsType.Consumer, new Guid("11111111-1111-1111-1111-111111111111"));
            var totals = new LiabilityTotals(balance: Money.Create(1).Value, payment: Money.Create(2).Value, paidOff: Money.Create(3).Value);
            var liabilityTotalsResponse = ServiceMessage.Create("5", "getLiabilityTotals", new GetLiabilityTotalsResponse(command, totals, new FormatEntityAsString(FormatTarget.Webform, FormatDirection.ToRep)));

            var response = this.CreateResponse(null, null, null, null, liabilityTotalsResponse, null);
            var responseJson = response.ToJObject();
            var commandResponse = responseJson.SelectToken("data.responses[?(@.name == 'getLiabilityTotals')].returns");

            Assert.AreEqual("5", (string)responseJson.SelectToken("data.responses[?(@.name == 'getLiabilityTotals')].operationId"));

            Assert.AreEqual("loan", (string)commandResponse.SelectToken("path"));
            Assert.AreEqual("Consumer", (string)commandResponse.SelectToken("type"));
            Assert.AreEqual("11111111-1111-1111-1111-111111111111", (string)commandResponse.SelectToken("id"));
            Assert.AreEqual("$1.00", (string)commandResponse.SelectToken("balance"));
            Assert.AreEqual("$2.00", (string)commandResponse.SelectToken("payment"));
            Assert.AreEqual("$3.00", (string)commandResponse.SelectToken("paidOff"));
        }

        [Test]
        public void ToJObject_WithGetLiabilityTotalsResponsesForLoanAndConsumer_ReturnsExpectedFormat()
        {
            var consumerCommand = new GetTotals<DataObjectKind.Liability>(DataPath.Create("loan"), TotalsType.Consumer, new Guid("11111111-1111-1111-1111-111111111111"));
            var consumerTotals = new LiabilityTotals(balance: Money.Create(1).Value, payment: Money.Create(2).Value, paidOff: Money.Create(3).Value);
            var consumerLiabilityTotalsResponse = ServiceMessage.Create("6", "getLiabilityTotals", new GetLiabilityTotalsResponse(consumerCommand, consumerTotals, new FormatEntityAsString(FormatTarget.Webform, FormatDirection.ToRep)));
            var loanCommand = new GetTotals<DataObjectKind.Liability>(DataPath.Create("loan"), TotalsType.Loan, id: null);
            var loanTotals = new LiabilityTotals(balance: Money.Create(4).Value, payment: Money.Create(5).Value, paidOff: Money.Create(6).Value);
            var loanLiabilityTotalsResponse = ServiceMessage.Create("7", "getLiabilityTotals", new GetLiabilityTotalsResponse(loanCommand, loanTotals, new FormatEntityAsString(FormatTarget.Webform, FormatDirection.ToRep)));

            var response = new LqbDataResponse();
            response.GetLiabilityTotalsResponses = new[] { consumerLiabilityTotalsResponse, loanLiabilityTotalsResponse };
            var responseJson = response.ToJObject();

            var expectedResponseToken = responseJson.SelectToken("data.responses");
            Assert.AreEqual(2, expectedResponseToken.Children().Count());

            var commandResponse = expectedResponseToken.SelectToken("[?(@.operationId == '6')].returns");
            Assert.AreEqual("loan", (string)commandResponse.SelectToken("path"));
            Assert.AreEqual("Consumer", (string)commandResponse.SelectToken("type"));
            Assert.AreEqual("11111111-1111-1111-1111-111111111111", (string)commandResponse.SelectToken("id"));
            Assert.AreEqual("$1.00", (string)commandResponse.SelectToken("balance"));
            Assert.AreEqual("$2.00", (string)commandResponse.SelectToken("payment"));
            Assert.AreEqual("$3.00", (string)commandResponse.SelectToken("paidOff"));

            commandResponse = expectedResponseToken.SelectToken("[?(@.operationId == '7')].returns");
            Assert.AreEqual("loan", (string)commandResponse.SelectToken("path"));
            Assert.AreEqual("Loan", (string)commandResponse.SelectToken("type"));
            Assert.AreEqual("$4.00", (string)commandResponse.SelectToken("balance"));
            Assert.AreEqual("$5.00", (string)commandResponse.SelectToken("payment"));
            Assert.AreEqual("$6.00", (string)commandResponse.SelectToken("paidOff"));
        }

        [Test]
        public void ToJObject_WithGetRealPropertyTotalsResponseForUladApp_ReturnsExpectedFormat()
        {
            var command = new GetTotals<DataObjectKind.RealProperty>(DataPath.Create("loan"), TotalsType.UladApplication, new Guid("11111111-1111-1111-1111-111111111111"));
            var totals = new RealPropertyTotals(marketValue: Money.Create(1).Value, mortgageAmount: Money.Create(2).Value, rentalNetRentalIncome: Money.Create(3).Value, retainedNetRentalIncome: Money.Create(4).Value);
            var getRealPropertyTotalsResponse = ServiceMessage.Create("2", "getRealPropertyTotals", new GetRealPropertyTotalsResponse(command, totals, new FormatEntityAsString(FormatTarget.Webform, FormatDirection.ToRep)));

            var response = this.CreateResponse(null, null, null, null, null, getRealPropertyTotalsResponse);
            var responseJson = response.ToJObject();
            var commandResponse = responseJson.SelectToken("data.responses[?(@.name == 'getRealPropertyTotals')].returns");

            Assert.AreEqual("2", (string)responseJson.SelectToken("data.responses[?(@.name == 'getRealPropertyTotals')].operationId"));

            Assert.AreEqual("loan", (string)commandResponse.SelectToken("path"));
            Assert.AreEqual("UladApplication", (string)commandResponse.SelectToken("type"));
            Assert.AreEqual("11111111-1111-1111-1111-111111111111", (string)commandResponse.SelectToken("id"));
            Assert.AreEqual("$1.00", (string)commandResponse.SelectToken("marketValue"));
            Assert.AreEqual("$2.00", (string)commandResponse.SelectToken("mortgageAmount"));
            Assert.AreEqual("$3.00", (string)commandResponse.SelectToken("rentalNetRentalIncome"));
            Assert.AreEqual("$4.00", (string)commandResponse.SelectToken("retainedNetRentalIncome"));
        }

        [Test]
        public void ToJObject_WithGetRealPropertyTotalsResponsesForLoanAndConsumer_ReturnsExpectedFormat()
        {
            var consumerCommand = new GetTotals<DataObjectKind.RealProperty>(DataPath.Create("loan"), TotalsType.Consumer, new Guid("11111111-1111-1111-1111-111111111111"));
            var consumerTotals = new RealPropertyTotals(marketValue: Money.Create(1).Value, mortgageAmount: Money.Create(2).Value, rentalNetRentalIncome: Money.Create(3).Value, retainedNetRentalIncome: Money.Create(4).Value);
            var consumerRealPropertyTotalsResponse = ServiceMessage.Create("6", "getRealPropertyTotals", new GetRealPropertyTotalsResponse(consumerCommand, consumerTotals, new FormatEntityAsString(FormatTarget.Webform, FormatDirection.ToRep)));
            var loanCommand = new GetTotals<DataObjectKind.RealProperty>(DataPath.Create("loan"), TotalsType.Loan, id: null);
            var loanTotals = new RealPropertyTotals(marketValue: Money.Create(5).Value, mortgageAmount: Money.Create(6).Value, rentalNetRentalIncome: Money.Create(7).Value, retainedNetRentalIncome: Money.Create(8).Value);
            var loanRealPropertyTotalsResponse = ServiceMessage.Create("7", "getRealPropertyTotals", new GetRealPropertyTotalsResponse(loanCommand, loanTotals, new FormatEntityAsString(FormatTarget.Webform, FormatDirection.ToRep)));

            var response = new LqbDataResponse();
            response.GetRealPropertyTotalsResponses = new[] { consumerRealPropertyTotalsResponse, loanRealPropertyTotalsResponse };
            var responseJson = response.ToJObject();

            var expectedResponseToken = responseJson.SelectToken("data.responses");
            Assert.AreEqual(2, expectedResponseToken.Children().Count());

            var commandResponse = expectedResponseToken.SelectToken("[?(@.operationId == '6')].returns");
            Assert.AreEqual("loan", (string)commandResponse.SelectToken("path"));
            Assert.AreEqual("Consumer", (string)commandResponse.SelectToken("type"));
            Assert.AreEqual("11111111-1111-1111-1111-111111111111", (string)commandResponse.SelectToken("id"));
            Assert.AreEqual("$1.00", (string)commandResponse.SelectToken("marketValue"));
            Assert.AreEqual("$2.00", (string)commandResponse.SelectToken("mortgageAmount"));
            Assert.AreEqual("$3.00", (string)commandResponse.SelectToken("rentalNetRentalIncome"));
            Assert.AreEqual("$4.00", (string)commandResponse.SelectToken("retainedNetRentalIncome"));

            commandResponse = expectedResponseToken.SelectToken("[?(@.operationId == '7')].returns");
            Assert.AreEqual("loan", (string)commandResponse.SelectToken("path"));
            Assert.AreEqual("Loan", (string)commandResponse.SelectToken("type"));
            Assert.AreEqual("$5.00", (string)commandResponse.SelectToken("marketValue"));
            Assert.AreEqual("$6.00", (string)commandResponse.SelectToken("mortgageAmount"));
            Assert.AreEqual("$7.00", (string)commandResponse.SelectToken("rentalNetRentalIncome"));
            Assert.AreEqual("$8.00", (string)commandResponse.SelectToken("retainedNetRentalIncome"));
        }

        [Test]
        public void ToJObject_WithGetIncomeSourceTotalsResponseForUladApp_ReturnsExpectedFormat()
        {
            var command = new GetTotals<DataObjectKind.IncomeSource>(DataPath.Create("loan"), TotalsType.UladApplication, new Guid("11111111-1111-1111-1111-111111111111"));
            var totals = new IncomeSourceTotals(monthlyAmount: Money.Create(1).Value);
            var getIncomeSourceTotalsResponse = ServiceMessage.Create("2", "getIncomeSourceTotals", new GetIncomeSourceTotalsResponse(command, totals, new FormatEntityAsString(FormatTarget.Webform, FormatDirection.ToRep)));

            var response = new LqbDataResponse { GetIncomeSourceTotalsResponses = new[] { getIncomeSourceTotalsResponse, } };
            var responseJson = response.ToJObject();
            var commandResponse = responseJson.SelectToken("data.responses[?(@.name == 'getIncomeSourceTotals')].returns");

            Assert.AreEqual("2", (string)responseJson.SelectToken("data.responses[?(@.name == 'getIncomeSourceTotals')].operationId"));

            Assert.AreEqual("loan", (string)commandResponse.SelectToken("path"));
            Assert.AreEqual("UladApplication", (string)commandResponse.SelectToken("type"));
            Assert.AreEqual("11111111-1111-1111-1111-111111111111", (string)commandResponse.SelectToken("id"));
            Assert.AreEqual("$1.00", (string)commandResponse.SelectToken("monthlyAmount"));
        }

        [Test]
        public void ToJObject_WithGetIncomeSourceTotalsResponsesForLoanAndConsumer_ReturnsExpectedFormat()
        {
            var consumerCommand = new GetTotals<DataObjectKind.IncomeSource>(DataPath.Create("loan"), TotalsType.Consumer, new Guid("11111111-1111-1111-1111-111111111111"));
            var consumerTotals = new IncomeSourceTotals(monthlyAmount: Money.Create(1).Value);
            var consumerIncomeSourceTotalsResponse = ServiceMessage.Create("6", "getIncomeSourceTotals", new GetIncomeSourceTotalsResponse(consumerCommand, consumerTotals, new FormatEntityAsString(FormatTarget.Webform, FormatDirection.ToRep)));
            var loanCommand = new GetTotals<DataObjectKind.IncomeSource>(DataPath.Create("loan"), TotalsType.Loan, id: null);
            var loanTotals = new IncomeSourceTotals(monthlyAmount: Money.Create(5).Value);
            var loanIncomeSourceTotalsResponse = ServiceMessage.Create("7", "getIncomeSourceTotals", new GetIncomeSourceTotalsResponse(loanCommand, loanTotals, new FormatEntityAsString(FormatTarget.Webform, FormatDirection.ToRep)));

            var response = new LqbDataResponse()
            {
                GetIncomeSourceTotalsResponses = new[] { consumerIncomeSourceTotalsResponse, loanIncomeSourceTotalsResponse },
            };
            var responseJson = response.ToJObject();

            var expectedResponseToken = responseJson.SelectToken("data.responses");
            Assert.AreEqual(2, expectedResponseToken.Children().Count());

            var commandResponse = expectedResponseToken.SelectToken("[?(@.operationId == '6')].returns");
            Assert.AreEqual("loan", (string)commandResponse.SelectToken("path"));
            Assert.AreEqual("Consumer", (string)commandResponse.SelectToken("type"));
            Assert.AreEqual("11111111-1111-1111-1111-111111111111", (string)commandResponse.SelectToken("id"));
            Assert.AreEqual("$1.00", (string)commandResponse.SelectToken("monthlyAmount"));

            commandResponse = expectedResponseToken.SelectToken("[?(@.operationId == '7')].returns");
            Assert.AreEqual("loan", (string)commandResponse.SelectToken("path"));
            Assert.AreEqual("Loan", (string)commandResponse.SelectToken("type"));
            Assert.AreEqual("$5.00", (string)commandResponse.SelectToken("monthlyAmount"));
        }

        [Test]
        public void ToJObject_BorrowerManagementAddRecord_ReturnsGoodData()
        {
            Guid id = new Guid("93D5131B-BAA1-4BE6-8C32-5C54EE2FAE96");
            var response = new LqbDataResponse
            {
                BorrowerManagementAddResponses = new[] { ServiceMessage.Create("2", "addBorrowerToApplication", new BorrowerManagementAddResponse(id)) }
            };

            JObject responseJson = response.ToJObject();

            var expectedResponseToken = responseJson.SelectToken("data.responses");
            Assert.AreEqual(1, expectedResponseToken.Children().Count());
            var operationResponse = expectedResponseToken.Children().First();
            Assert.AreEqual("addBorrowerToApplication", (string)operationResponse.SelectToken("name"));
            Assert.AreEqual("2", (string)operationResponse.SelectToken("operationId"));
            Assert.AreEqual(id.ToString(), (string)operationResponse.SelectToken("returns.id"));
        }

        [Test]
        public void ToJObject_AddAssociationResponse()
        {
            var firstId = Guid.NewGuid().ToString("D");
            var secondId = Guid.NewGuid().ToString("D");
            var associationId = Guid.NewGuid().ToString("D");

            var root = this.GetAddAssociationResponse(firstId, secondId, associationId);
            var response = this.CreateResponse(null, root, null, null, null, null);
            this.CheckAddAssociationResponse(firstId, secondId, associationId, response);
        }

        [Test]
        public void ToJObject_TwoAddAssociationResponses_ReturnsExpectedValues()
        {
            var firstId1 = Guid.NewGuid().ToString("D");
            var secondId1 = Guid.NewGuid().ToString("D");
            var associationId1 = Guid.NewGuid().ToString("D");
            var assocResponse1 = this.GetAddAssociationResponse(firstId1, secondId1, associationId1, "1");
            var firstId2 = Guid.NewGuid().ToString("D");
            var secondId2 = Guid.NewGuid().ToString("D");
            var associationId2 = Guid.NewGuid().ToString("D");
            var assocResponse2 = this.GetAddAssociationResponse(firstId2, secondId2, associationId2, "2");

            var response = new LqbDataResponse();
            response.AddAssociationResponses = new[] { assocResponse1, assocResponse2 };
            var responseJson = response.ToJObject();

            var expectedResponseToken = responseJson.SelectToken("data.responses");
            Assert.AreEqual(2, expectedResponseToken.Children().Count());

            var addAssociationToken = expectedResponseToken.SelectToken("[?(@.operationId == '1')].returns");
            Assert.AreEqual(5, addAssociationToken.Children().Count());
            Assert.AreEqual("loan", (string)addAssociationToken["path"]);
            Assert.AreEqual("RealPropertyLiabilities", (string)addAssociationToken["associationSet"]);
            Assert.AreEqual(firstId1, (string)addAssociationToken["firstId"]);
            Assert.AreEqual(secondId1, (string)addAssociationToken["secondId"]);
            Assert.AreEqual(associationId1, (string)addAssociationToken["id"]);

            addAssociationToken = expectedResponseToken.SelectToken("[?(@.operationId == '2')].returns");
            Assert.AreEqual(5, addAssociationToken.Children().Count());
            Assert.AreEqual("loan", (string)addAssociationToken["path"]);
            Assert.AreEqual("RealPropertyLiabilities", (string)addAssociationToken["associationSet"]);
            Assert.AreEqual(firstId2, (string)addAssociationToken["firstId"]);
            Assert.AreEqual(secondId2, (string)addAssociationToken["secondId"]);
            Assert.AreEqual(associationId2, (string)addAssociationToken["id"]);
        }

        [Test]
        public void ToJObject_AddAssociationAndGet()
        {
            var firstId = Guid.NewGuid().ToString("D");
            var secondId = Guid.NewGuid().ToString("D");
            var associationId = Guid.NewGuid().ToString("D");

            var addAssociation = this.GetAddAssociationResponse(firstId, secondId, associationId);
            var getData = this.GetGetResponse();
            var response = this.CreateResponse(null, addAssociation, getData, null, null, null);

            this.CheckAddAssociationResponse(firstId, secondId, associationId, response);
            this.CheckGetResponse(response);
        }

        private void CheckAddEntityResponse(string ownerId, string assetId, LqbDataResponse response)
        {
            var json = response.ToJObject();

            var expectedResponseToken = json.SelectToken("data.responses");

            Assert.AreEqual("1", (string)json.SelectToken("data.responses[?(@.name == 'addEntity')].operationId"));
            var addEntityToken = expectedResponseToken.SelectToken("[?(@.name == 'addEntity')].returns");
            Assert.AreEqual(4, addEntityToken.Children().Count());
            Assert.AreEqual("loan", (string)addEntityToken["path"]);
            Assert.AreEqual("Assets", (string)addEntityToken["collectionName"]);
            Assert.AreEqual(ownerId, (string)addEntityToken["ownerId"]);
            Assert.AreEqual(assetId, (string)addEntityToken["id"]);
        }

        private void CheckAddAssociationResponse(string firstId, string secondId, string associationId, LqbDataResponse response)
        {
            var json = response.ToJObject();

            var expectedResponseToken = json.SelectToken("data.responses");
            Assert.AreEqual("2", (string)expectedResponseToken.SelectToken("[?(@.name == 'addAssociation')].operationId"));
            var addAssociationToken = expectedResponseToken.SelectToken("[?(@.name == 'addAssociation')].returns");
            Assert.AreEqual(5, addAssociationToken.Children().Count());

            Assert.AreEqual("loan", (string)addAssociationToken["path"]);
            Assert.AreEqual("RealPropertyLiabilities", (string)addAssociationToken["associationSet"]);
            Assert.AreEqual(firstId, (string)addAssociationToken["firstId"]);
            Assert.AreEqual(secondId, (string)addAssociationToken["secondId"]);
            Assert.AreEqual(associationId, (string)addAssociationToken["id"]);
        }

        private LqbDataResponse CreateResponse(
            ServiceMessage<AddEntityResponse> addEntity,
            ServiceMessage<AddAssociationResponse> addAssociation,
            ServiceMessage<GetResponseNode> getResponseNode,
            ServiceMessage<GetAssetTotalsResponse> getAssetTotalsResponse,
            ServiceMessage<GetLiabilityTotalsResponse> getLiabilityTotalsResponse,
            ServiceMessage<GetRealPropertyTotalsResponse> getRealPropertyTotalsResponse)
        {
            var response = new LqbDataResponse();

            if (addEntity != null)
            {
                response.AddEntityResponses = new[] { addEntity };
            }

            if (addAssociation != null)
            {
                response.AddAssociationResponses = new[] { addAssociation };
            }

            if (getAssetTotalsResponse != null)
            {
                response.GetAssetTotalsResponses = new[] { getAssetTotalsResponse };
            }

            if (getLiabilityTotalsResponse != null)
            {
                response.GetLiabilityTotalsResponses = new[] { getLiabilityTotalsResponse };
            }

            if (getRealPropertyTotalsResponse != null)
            {
                response.GetRealPropertyTotalsResponses = new[] { getRealPropertyTotalsResponse };
            }

            response.GetResponse = getResponseNode;

            return response;
        }

        private ServiceMessage<AddEntityResponse> GetAddEntityResponse(string ownerId, string assetId, string operationId = "1")
        {
            var command = new AddEntity(DataPath.Create("loan"), "Assets", Guid.Parse(ownerId), additionalOwnerIds: null, fields: null, index: null);
            return ServiceMessage.Create(operationId, "addEntity", new AddEntityResponse(command, Guid.Parse(assetId)));
        }

        private ServiceMessage<AddAssociationResponse> GetAddAssociationResponse(string firstId, string secondId, string associationId, string operationId = "2")
        {
            var command = new AddAssociation(DataPath.Create("loan"), "RealPropertyLiabilities", Guid.Parse(firstId), Guid.Parse(secondId));
            return ServiceMessage.Create(operationId, "addAssociation", new AddAssociationResponse(command, Guid.Parse(associationId)));
        }

        private void CheckGetResponse(LqbDataResponse response)
        {
            var json = response.ToJObject();

            Assert.AreEqual("3", (string)json.SelectToken("data.responses.[?(@.name == 'get')].operationId"));

            var getResponse = json.SelectToken("data.responses.[?(@.name == 'get')].returns");
            var loanType = (string)getResponse.SelectToken("loan.sLT");
            Assert.AreEqual("3", loanType);

            var accountNum = (string)getResponse.SelectToken("loan.Assets['11111111-1111-1111-1111-111111111111'].AccountNum");
            Assert.AreEqual("Account 123", accountNum);
        }

        private ServiceMessage<GetResponseNode> GetGetResponse()
        {
            var root = new GetResponseNode(
                pathComponent: null,
                value: null,
                children: new List<GetResponseNode>()
                {
                    new GetResponseNode(
                        pathComponent: new DataPathBasicElement("loan"),
                        value: null,
                        children: new List<GetResponseNode>()
                        {
                            new GetResponseNode(pathComponent: new DataPathBasicElement("sLT"), children: null, value: "3" ),
                            new GetResponseNode(
                                pathComponent: new DataPathBasicElement("Assets"),
                                value: null,
                                children: new List<GetResponseNode>()
                                {
                                    new GetResponseNode(
                                        pathComponent: new DataPathSelectionElement(new SelectIdExpression("11111111-1111-1111-1111-111111111111")),
                                        value: null,
                                        children: new List<GetResponseNode>()
                                        {
                                            new GetResponseNode(
                                                pathComponent: new DataPathBasicElement("AccountNum"),
                                                children: null,
                                                value: "Account 123")
                                        })
                                }
                            )
                        })
                });

            return ServiceMessage.Create("3", "get", root);
        }
    }
}
