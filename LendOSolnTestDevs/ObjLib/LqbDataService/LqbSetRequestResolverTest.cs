﻿namespace LendingQB.Test.Developers.ObjLib.LqbDataService
{
    using System;
    using System.Collections.Generic;
    using Core;
    using Core.Data;
    using global::DataAccess;
    using global::DataAccess.PathDispatch;
    using LendersOffice.ObjLib.LqbDataService;
    using LqbGrammar.DataTypes;
    using LqbGrammar.DataTypes.PathDispatch;
    using NSubstitute;
    using NUnit.Framework;

    [TestFixture]
    public class LqbSetRequestResolverTest
    {
        [Test]
        public void ResolveSetRequest_LeafNodeParentDoesNotImplementIPathSettable_ThrowsException()
        {
            var root = new RequestNode(
                pathComponent: null,
                children: new List<RequestNode>()
                {
                    new RequestNode(
                        pathComponent: new DataPathBasicElement("AccountName"),
                        children: null,
                        value: "Account 1234")
                });
            var pathResolvableSeedValue = Substitute.For<IPathResolvable>();

            Assert.Throws<CBaseException>(() => LqbSetRequestResolver.ResolveSetRequest(root, pathResolvableSeedValue, Substitute.For<IParseFromString>()));
        }

        [Test]
        public void ResolveSetRequest_LeafNodeParentDoesNotImplementIPathResolvable_ThrowsException()
        {
            var root = new RequestNode(
                pathComponent: null,
                children: new List<RequestNode>()
                {
                    new RequestNode(
                        pathComponent: new DataPathBasicElement("Assets"),
                        children: new List<RequestNode>()
                        {
                            new RequestNode(
                                pathComponent: new DataPathSelectionElement(new SelectIdExpression(Guid.NewGuid().ToString())),
                                children: new List<RequestNode>()
                                {
                                    new RequestNode(pathComponent: new DataPathBasicElement(nameof(Asset.AccountName)), children: null, value: "Test Name")
                                })
                        })
                });
            var pathResolvableSeedValue = Substitute.For<IPathResolvable>();
            pathResolvableSeedValue.GetElement(Arg.Any<IDataPathElement>()).Returns(Substitute.For<IPathSettable>());

            Assert.Throws<CBaseException>(() => LqbSetRequestResolver.ResolveSetRequest(root, pathResolvableSeedValue, Substitute.For<IParseFromString>()));
        }

        [Test]
        public void ResolveSetRequest_LeafNode_SetsPathComponentToValue()
        {
            var root = new RequestNode(
                pathComponent: null,
                children: new List<RequestNode>()
                {
                    new RequestNode(
                        pathComponent: new DataPathBasicElement("AccountName"),
                        children: null,
                        value: "Account 1234")
                });
            var pathResolvableSeedValue = Substitute.For<IPathResolvable, IPathSettable>();

            LqbSetRequestResolver.ResolveSetRequest(root, pathResolvableSeedValue, Substitute.For<IParseFromString>());

            ((IPathSettable)pathResolvableSeedValue).Received().SetElement(new DataPathBasicElement("AccountName"), "Account 1234");
        }

        [Test]
        public void ResolveSetRequest_SetAssetFields_UpdatesFields()
        {
            var idFactory = new GuidDataObjectIdentifierFactory<DataObjectKind.Asset>();
            var assetCollection = LqbCollection<DataObjectKind.Asset, Guid, Asset>.Create(
                Name.Create("Assets").Value,
                idFactory,
                null);
            var id1 = idFactory.NewId();
            assetCollection.Add(id1, new Asset());
            var root = new RequestNode(
                pathComponent: null,
                children: new List<RequestNode>()
                {
                    new RequestNode(
                        pathComponent: new DataPathBasicElement("Assets"),
                        children: new List<RequestNode>()
                        {
                            new RequestNode(
                                pathComponent: new DataPathSelectionElement(new SelectIdExpression(id1.ToString())),
                                children: new List<RequestNode>()
                                {
                                    new RequestNode(pathComponent: new DataPathBasicElement(nameof(Asset.AccountName)), children: null, value: "Test Name")
                                })
                        })
                });
            var pathResolvableSeedValue = Substitute.For<IPathResolvable>();
            pathResolvableSeedValue.GetElement(Arg.Any<IDataPathElement>()).Returns(assetCollection);

            var parser = new ParseEntityFromString(FormatTarget.Webform, FormatDirection.ToRep);

            LqbSetRequestResolver.ResolveSetRequest(root, pathResolvableSeedValue, parser);

            Assert.AreEqual("Test Name", assetCollection[id1].AccountName.ToString());
        }
    }
}
