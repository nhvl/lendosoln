﻿namespace LendingQB.Test.Developers.ObjLib.LqbDataService
{
    using global::DataAccess;
    using LendersOffice.ObjLib.LqbDataService;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    class DataFormattingTest
    {
        [Test]
        public void FormatEnumAsInteger()
        {
            var enumValue = E_AssetRegularT.BridgeLoanNotDeposited;
            Assert.AreEqual(18, (int)enumValue);

            var formatter = new FormatForEndpoint(FormatTarget.Webform, FormatDirection.ToRep);
            string formatted = formatter.FormatEnum<E_AssetRegularT>(enumValue);
            Assert.AreEqual("18", formatted);
        }

        [Test]
        public void ParseIntegerToEnum()
        {
            string input = "18";

            var parser = new ParseForEndpoint(FormatTarget.Webform, FormatDirection.ToDb);
            var enumValue = parser.TryParseEnum<E_AssetRegularT>(input);
            Assert.IsTrue(enumValue != null);
            Assert.AreEqual(E_AssetRegularT.BridgeLoanNotDeposited, enumValue);
        }

        [Test]
        public void ParseBadEnumValue_ReturnsNull()
        {
            string input = "666"; // bad input

            var parser = new ParseForEndpoint(FormatTarget.Webform, FormatDirection.ToDb);
            var enumValue = parser.TryParseEnum<E_AssetRegularT>(input);
            Assert.IsTrue(enumValue == null);
        }
    }
}
