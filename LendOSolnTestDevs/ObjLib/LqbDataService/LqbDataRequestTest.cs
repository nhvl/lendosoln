﻿namespace LendingQB.Test.Developers.ObjLib.LqbDataService
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Core.Commands;
    using global::DataAccess;
    using LendersOffice.ObjLib.LqbDataService;
    using LqbGrammar.DataTypes;
    using LqbGrammar.DataTypes.PathDispatch;
    using Newtonsoft.Json.Linq;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public class LqbDataRequestTest
    {
        private readonly LosConvert converter = new LosConvert();

        [Test]
        public void GetRequestNodeTree_Always_ReturnsCommandWithSpecifiedTransactionId()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""get"",
                            ""operationId"": ""0"",
                            ""args"": {
                                ""loan"": {
                                    ""sLT"": null,
                                    ""sLAmtCalc"": null
                                }
                            }
                        }
                    ]
                }
            }";
            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            Assert.AreEqual("0", loanDataRequest.GetRequestNodeTree.OperationId);
        }

        [Test]
        public void GetRequestNodeTree_Always_ReturnsRootNodeWithNullPathElement()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""get"",
                            ""operationId"": ""1"",
                            ""args"": {
                                ""loan"": {
                                    ""sLT"": null,
                                    ""sLAmtCalc"": null
                                }
                            }
                        }
                    ]
                }
            }";
            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            var tree = loanDataRequest.GetRequestNodeTree.Message;

            Assert.AreEqual(null, tree.PathComponent);
        }

        [Test]
        public void GetRequestNodeTree_Always_BeginsFromGetElement()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""get"",
                            ""operationId"": ""2"",
                            ""args"": {
                                ""loan"": {
                                    ""sLT"": null,
                                    ""sLAmtCalc"": null
                                }
                            }
                        }
                    ]
                }
            }";
            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            var tree = loanDataRequest.GetRequestNodeTree.Message;

            Assert.AreEqual(1, tree.Children.Count);
        }

        [Test]
        public void GetRequestNodeTree_RequestTwoLoanFields_ReturnsLoanNodeWithTwoChildren()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""get"",
                            ""operationId"": ""3"",
                            ""args"": {
                                ""loan"": {
                                    ""sLT"": null,
                                    ""sLAmtCalc"": null
                                }
                            }
                        }
                    ]
                }
            }";
            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            var tree = loanDataRequest.GetRequestNodeTree.Message;

            var loan = tree.Children.Single();
            Assert.AreEqual(2, loan.Children.Count);
            Assert.AreEqual(new DataPathBasicElement("sLT"), loan.Children[0].PathComponent);
            Assert.AreEqual(new DataPathBasicElement("sLAmtCalc"), loan.Children[1].PathComponent);
        }

        [Test]
        public void GetRequestNodeTree_RequestCollectionElementById_ReturnsExpectedElementTypes()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""get"",
                            ""operationId"": ""4"",
                            ""args"": {
                                ""loan"": {
                                    ""Assets"": {
                                        ""11111111-1111-1111-1111-111111111111"": {
                                            ""AccountNum"": null
                                        }
                                    }
                                }
                            }
                        }
                    ]
                }
            }";
            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            var tree = loanDataRequest.GetRequestNodeTree.Message;

            var loan = tree.Children.Single();
            Assert.AreEqual(1, loan.Children.Count);
            Assert.AreEqual(new DataPathCollectionElement("Assets"), loan.Children[0].PathComponent);

            var assets = loan.Children.Single();
            Assert.AreEqual(new DataPathSelectionElement(new SelectIdExpression("11111111-1111-1111-1111-111111111111")), assets.Children[0].PathComponent);
        }

        [Test]
        public void GetRequestNodeTree_RequestCollectionElementByIdWithSpecificFields_ReturnsExpectedElementTypes()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""get"",
                            ""operationId"": ""5"",
                            ""args"": {
                                ""loan"": {
                                    ""Assets"": {
                                        ""11111111-1111-1111-1111-111111111111"": {
                                            ""AccountNum"": null
                                        }
                                    }
                                }
                            }
                        }
                    ]
                }
            }";
            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            var tree = loanDataRequest.GetRequestNodeTree.Message;

            var loan = tree.Children.Single();
            Assert.AreEqual(1, loan.Children.Count);
            Assert.AreEqual(new DataPathCollectionElement("Assets"), loan.Children[0].PathComponent);

            var assets = loan.Children.Single();
            Assert.AreEqual(new DataPathSelectionElement(new SelectIdExpression("11111111-1111-1111-1111-111111111111")), assets.Children[0].PathComponent);

            var assetRecord = assets.Children.Single();
            Assert.AreEqual(new DataPathBasicElement("AccountNum"), assetRecord.Children[0].PathComponent);
        }

        [Test]
        public void GetRequestNodeTree_EmploymentRecordAndIncomeRecords_ReturnsExpectedElementTypes()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""get"",
                            ""operationId"": ""0"",
                            ""args"": {
                                ""loan"": {
                                    ""EmploymentRecords"": {
                                        ""11111111-1111-1111-1111-111111111111"": {
    									    IncomeRecords: {
                                                ""22222222-2222-2222-2222-222222222222"": {
                                                    IncomeType: null,
                                                    MonthlyAmount: null
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    ]
                }
            }";
            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            var tree = loanDataRequest.GetRequestNodeTree.Message;

            var loan = tree.Children.Single();
            Assert.AreEqual(1, loan.Children.Count);
            Assert.AreEqual(new DataPathCollectionElement("EmploymentRecords"), loan.Children[0].PathComponent);

            var employmentRecords = loan.Children.Single();
            Assert.AreEqual(new DataPathSelectionElement(new SelectIdExpression("11111111-1111-1111-1111-111111111111")), employmentRecords.Children[0].PathComponent);

            var employmentRecord = employmentRecords.Children.Single();
            Assert.AreEqual(new DataPathCollectionElement("IncomeRecords"), employmentRecord.Children[0].PathComponent);

            var incomeRecords = employmentRecord.Children.Single();
            Assert.AreEqual(new DataPathSelectionElement(new SelectIdExpression("22222222-2222-2222-2222-222222222222")), incomeRecords.Children[0].PathComponent);

            var incomeRecord = incomeRecords.Children.Single();
            Assert.AreEqual(new DataPathBasicElement("IncomeType"), incomeRecord.Children[0].PathComponent);
            Assert.AreEqual(new DataPathBasicElement("MonthlyAmount"), incomeRecord.Children[1].PathComponent);
        }

        [Test]
        public void GetRequestNodeTree_NoGetRequest_ReturnsNull()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": []
                }
            }";
            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            var getRoot = loanDataRequest.GetRequestNodeTree;

            Assert.AreEqual(null, getRoot);
        }

        [Test]
        public void LoanDependencies_Always_ReturnsTopLevelLoanChildren()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""get"",
                            ""operationId"": ""6"",
                            ""args"": {
                                ""loan"": {
                                    ""sLAmtCalc"": null,
                                    ""sLT"": null,
                                    ""Assets"": {
                                        ""11111111-1111-1111-1111-111111111111"": {
                                            ""AccountNum"": null
                                        }
                                    },
                                    ""Consumers"": {
                                        ""Any"": {
                                            ""FirstNm"": null
                                        }
                                    }
                                }
                            }
                        }
                    ]
                }
            }";
            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            var dependencies = loanDataRequest.LoanDependencies;

            CollectionAssert.AreEquivalent(new[] { "sLAmtCalc", "sLT", "Assets", "Consumers", "FirstNm" }, dependencies);
        }

        [Test]
        public void LoanDependencies_GetAndSetRequests_ReturnsExpectedFields()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        
                        {
                            ""name"": ""get"",
                            ""operationId"": ""7"",
                            ""args"": {
                                ""loan"": {
                                    ""sLAmtCalc"": null,
                                }
                            }
                        },
                        {
                            ""name"": ""set"",
                            ""operationId"": ""8"",
                            ""args"": {
                                ""loan"": {
                                    ""sLT"": ""3"",
                                }
                            }
                        }
                    ]
                }
            }";
            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            var dependencies = loanDataRequest.LoanDependencies;

            CollectionAssert.AreEquivalent(new[] { "sLAmtCalc", "sLT" }, dependencies);
        }

        [Test]
        public void LoanDependencies_GetAndSetRequestsForNestedEntity_ReturnsExpectedFields()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        
                        {
                            ""name"": ""get"",
                            ""operationId"": ""7"",
                            ""args"": {
                                ""loan"": {
                                    ""Consumers"": {
                                        ""00000000-0000-0200-0000-000000000000"": {
                                            ""Name"": null,
                                            ""FirstName"": null,
                                            ""Age"": null,
                                            ""BusPhone"": null
                                        },
                                        ""00000000-0000-0300-0000-000000000000"": {
                                            ""Name"": null,
                                            ""LastName"": null
                                        },
                                    },
                                    ""LegacyApplications"": {
                                        ""Any"": {
                                            ""AppName"": null,
                                            ""InterviewerMethodType"": null
                                        }
                                    }
                                }
                            }
                        },
                        {
                            ""name"": ""set"",
                            ""operationId"": ""8"",
                            ""args"": {
                                ""loan"": {
                                    ""Consumers"": {
                                        ""Any"": {
                                            ""MiddleName"": ""Arthur"",
                                            ""FicoScore"": 800
                                        }
                                    },
                                    ""LegacyApplications"": {
                                        ""Any"": {
                                            ""Form1003InterviewDate"": ""2/1/2019"",
                                            ""Form1003InterviewDateLocked"": ""True"",
                                            ""BorrowerTypeType"": ""Individual""
                                        }
                                    }
                                }
                            }
                        }
                    ]
                }
            }";
            var expected = new[] { "Consumers", "Name", "FirstName", "Age", "BusPhone", "LastName", "LegacyApplications", "AppName", "InterviewerMethodType", "MiddleName", "FicoScore", "Form1003InterviewDate", "Form1003InterviewDateLocked", "BorrowerTypeType", };
            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            var actual = loanDataRequest.LoanDependencies;

            CollectionAssert.AreEquivalent(expected, actual);
        }

        [Test]
        public void LoanDependencies_NoLoanRequest_ReturnsEmpty()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": []
                }
            }";
            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            var dependencies = loanDataRequest.LoanDependencies;

            CollectionAssert.AreEquivalent(Enumerable.Empty<string>(), dependencies);
        }

        [Test]
        public void LoanDependencies_RemoveEntityCommand_ReturnsCollectionName()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""removeEntity"",
                            ""operationId"": ""9"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""collectionName"": ""Assets"",
                                ""id"": ""11111111-1111-1111-1111-111111111111""
                            }
                        }
                    ]
                }
            }";
            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            var dependencies = loanDataRequest.LoanDependencies;

            CollectionAssert.AreEquivalent(new[] { "Assets" }, dependencies);
        }

        [Test]
        public void LoanDependencies_RemoveEntityCommandFromNestedCollection_ReturnsExpectedDependencies()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""removeEntity"",
                            ""operationId"": ""0"",
                            ""args"": {
                                ""path"": ""loan.EmploymentRecords[11111111-1111-1111-1111-111111111111]"",
                                ""collectionName"": ""IncomeRecords"",
                                ""id"": ""22222222-2222-2222-2222-222222222222""
                            }
                        }
                    ]
                }
            }";

            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            var dependencies = loanDataRequest.LoanDependencies;

            CollectionAssert.AreEquivalent(new[] { nameof(CPageBase.EmploymentRecords) }, dependencies);
        }

        [Test]
        public void LoanDependencies_RemoveEntityCommandFromLoanLevelAndNestedCollection_ReturnsExpectedDependencies()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""removeEntity"",
                            ""operationId"": ""0"",
                            ""args"": {
                                ""path"": ""loan.EmploymentRecords[11111111-1111-1111-1111-111111111111]"",
                                ""collectionName"": ""IncomeRecords"",
                                ""id"": ""22222222-2222-2222-2222-222222222222""
                            }
                        },
                        {
                            ""name"": ""removeEntity"",
                            ""operationId"": ""16"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""collectionName"": ""Assets"",
                                ""ownerId"": ""33333333-3333-3333-3333-333333333333"",
                                ""id"": ""44444444-4444-4444-4444-444444444444""
                            }
                        }
                    ]
                }
            }";

            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            var dependencies = loanDataRequest.LoanDependencies;

            CollectionAssert.AreEquivalent(new[] { nameof(CPageBase.EmploymentRecords), nameof(CPageBase.Assets), }, dependencies);
        }

        [Test]
        public void LoanDependencies_MultipleRemoveEntityCommands_ReturnsExpectedDependencies()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""removeEntity"",
                            ""operationId"": ""10"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""collectionName"": ""Assets"",
                                ""id"": ""11111111-1111-1111-1111-111111111111""
                            }
                        },
                        {
                            ""name"": ""removeEntity"",
                            ""operationId"": ""11"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""collectionName"": ""Liabilities"",
                                ""id"": ""11111111-1111-1111-1111-111111111111""
                            }
                        },
                        {
                            ""name"": ""removeEntity"",
                            ""operationId"": ""12"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""collectionName"": ""RealProperties"",
                                ""id"": ""11111111-1111-1111-1111-111111111111""
                            }
                        }
                    ]
                }
            }";
            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            var dependencies = loanDataRequest.LoanDependencies;

            CollectionAssert.AreEquivalent(new[] { "Assets", "Liabilities", "RealProperties" }, dependencies);
        }

        [Test]
        public void LoanDependencies_RemoveAssociationCommand_ReturnsAssociationSet()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""removeAssociation"",
                            ""operationId"": ""13"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""associationSet"": ""RealPropertyLiabilities"",
                                ""id"": ""11111111-1111-1111-1111-111111111111""
                            }
                        }
                    ]
                }
            }";
            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            var dependencies = loanDataRequest.LoanDependencies;

            CollectionAssert.AreEquivalent(new[] { "RealPropertyLiabilities" }, dependencies);
        }

        [Test]
        public void LoanDependencies_MultipleRemoveAssociationCommands_ReturnsAllAssociationSets()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""removeAssociation"",
                            ""operationId"": ""14"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""associationSet"": ""RealPropertyLiabilities"",
                                ""id"": ""11111111-1111-1111-1111-111111111111""
                            }
                        },
                        {
                            ""name"": ""removeAssociation"",
                            ""operationId"": ""15"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""associationSet"": ""ConsumerLiabilities"",
                                ""id"": ""11111111-1111-1111-1111-111111111111""
                            }
                        }
                    ]
                }
            }";
            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            var dependencies = loanDataRequest.LoanDependencies;

            CollectionAssert.AreEquivalent(new[] { "RealPropertyLiabilities", "ConsumerLiabilities" }, dependencies);
        }

        [Test]
        public void LoanDependencies_AddEntityCommand_ReturnsExpectedDependencies()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""addEntity"",
                            ""operationId"": ""16"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""collectionName"": ""Assets"",
                                ""ownerId"": ""11111111-1111-1111-1111-111111111111"",
                                ""id"": null
                            }
                        }
                    ]
                }
            }";

            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            var dependencies = loanDataRequest.LoanDependencies;

            CollectionAssert.AreEquivalent(new[] { nameof(CPageBase.Assets), nameof(CPageBase.sfHandleAdd) }, dependencies);
        }

        [Test]
        public void LoanDependencies_AddEntityCommandToNestedCollection_ReturnsExpectedDependencies()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""addEntity"",
                            ""operationId"": ""0"",
                            ""args"": {
                                ""path"": ""loan.EmploymentRecords[11111111-1111-1111-1111-111111111111]"",
                                ""collectionName"": ""IncomeRecords"",
                                ""id"": null
                            }
                        }
                    ]
                }
            }";

            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            var dependencies = loanDataRequest.LoanDependencies;

            CollectionAssert.AreEquivalent(new[] { nameof(CPageBase.EmploymentRecords) }, dependencies);
        }

        [Test]
        public void LoanDependencies_AddEntityCommandToLoanLevelAndNestedCollection_ReturnsExpectedDependencies()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""addEntity"",
                            ""operationId"": ""0"",
                            ""args"": {
                                ""path"": ""loan.EmploymentRecords[11111111-1111-1111-1111-111111111111]"",
                                ""collectionName"": ""IncomeRecords"",
                                ""id"": null
                            }
                        },
                        {
                            ""name"": ""addEntity"",
                            ""operationId"": ""16"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""collectionName"": ""Assets"",
                                ""ownerId"": ""11111111-1111-1111-1111-111111111111"",
                                ""id"": null
                            }
                        }
                    ]
                }
            }";

            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            var dependencies = loanDataRequest.LoanDependencies;

            CollectionAssert.AreEquivalent(new[] { nameof(CPageBase.EmploymentRecords), nameof(CPageBase.sfHandleAdd), nameof(CPageBase.Assets) }, dependencies);
        }

        [Test]
        public void LoanDependencies_MultipleAddEntityCommands_ReturnsExpectedDependencies()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""addEntity"",
                            ""operationId"": ""17"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""collectionName"": ""Assets"",
                                ""ownerId"": ""11111111-1111-1111-1111-111111111111"",
                                ""id"": null
                            }
                        },
                        {
                            ""name"": ""addEntity"",
                            ""operationId"": ""18"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""collectionName"": ""RealProperties"",
                                ""ownerId"": ""11111111-1111-1111-1111-111111111111"",
                                ""id"": null
                            }
                        },
                        {
                            ""name"": ""addEntity"",
                            ""operationId"": ""19"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""collectionName"": ""Liabilities"",
                                ""ownerId"": ""11111111-1111-1111-1111-111111111111"",
                                ""id"": null
                            }
                        }
                    ]
                }
            }";

            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            var dependencies = loanDataRequest.LoanDependencies;

            CollectionAssert.AreEquivalent(new[] { nameof(CPageBase.Assets), nameof(CPageBase.Liabilities), nameof(CPageBase.RealProperties), nameof(CPageBase.sfHandleAdd) }, dependencies);
        }

        [Test]
        public void LoanDependencies_AddAssociationCommand_ReturnsAssociationSet()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""addAssociation"",
                            ""operationId"": ""20"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""associationSet"": ""RealPropertyLiabilities"",
                                ""firstId"": ""11111111-1111-1111-1111-111111111111"",
                                ""secondId"": ""22222222-2222-2222-2222-222222222222""
                            }
                        }
                    ]
                }
            }";

            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            var dependencies = loanDataRequest.LoanDependencies;

            CollectionAssert.AreEquivalent(new[] { "RealPropertyLiabilities" }, dependencies);
        }

        [Test]
        public void LoanDependencies_MultipleAddAssociationCommands_ReturnsAllAssociationSets()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""addAssociation"",
                            ""operationId"": ""21"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""associationSet"": ""RealPropertyLiabilities"",
                                ""firstId"": ""11111111-1111-1111-1111-111111111111"",
                                ""secondId"": ""22222222-2222-2222-2222-222222222222""
                            }
                        },
                        {
                            ""name"": ""addAssociation"",
                            ""operationId"": ""22"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""associationSet"": ""ConsumerAssets"",
                                ""firstId"": ""11111111-1111-1111-1111-111111111111"",
                                ""secondId"": ""22222222-2222-2222-2222-222222222222""
                            }
                        }
                    ]
                }
            }";

            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            var dependencies = loanDataRequest.LoanDependencies;

            CollectionAssert.AreEquivalent(new[] { "RealPropertyLiabilities", "ConsumerAssets" }, dependencies);
        }

        [Test]
        public void LoanDependencies_SetOwnershipCommand_ReturnsAssociationName()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""setOwnership"",
                            ""operationId"": ""23"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""associationName"": ""ConsumerAssets"",
                                ""id"": ""11111111-1111-1111-1111-111111111111"",
                                ""primaryOwnerId"": ""22222222-2222-2222-2222-222222222222"",
                                ""additionalOwnerIds"": [""33333333-3333-3333-3333-333333333333"", ""44444444-4444-4444-4444-444444444444""]
                            }
                        }
                    ]
                }
            }";

            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            var dependencies = loanDataRequest.LoanDependencies;

            CollectionAssert.AreEquivalent(new[] { "ConsumerAssets" }, dependencies);
        }

        [Test]
        public void LoanDependencies_SetOrderCommand_ReturnsCollectionName()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""setOrder"",
                            ""operationId"": ""24"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""collectionName"": ""Assets"",
                                ""order"": [""33333333-3333-3333-3333-333333333333"", ""44444444-4444-4444-4444-444444444444""]
                            }
                        }
                    ]
                }
            }";

            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            var dependencies = loanDataRequest.LoanDependencies;

            CollectionAssert.AreEquivalent(new[] { "Assets" }, dependencies);
        }

        [Test]
        public void LoanDependencies_AddLiabilityBalanceAndPaymentToRealProperty_ReturnsHandler()
        {
            string json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""addLiabilityBalanceAndPaymentToRealProperty"",
                            ""operationId"": ""25"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""liabilityId"": ""33333333-3333-3333-3333-333333333333"",
                                ""realPropertyId"": ""44444444-4444-4444-4444-444444444444""
                            }
                        }
                    ]
                }
            }";

            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            var dependencies = loanDataRequest.LoanDependencies;

            CollectionAssert.AreEquivalent(new[] { nameof(CPageData.HandleAddLiabilityBalanceAndPaymentToRealProperty) }, dependencies);
        }

        [Test]
        public void LoanDependencies_GetAssetTotalsCommand_ReturnsExpectedValues()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""getAssetTotals"",
                            ""operationId"": ""25"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""type"": ""Loan""
                            }
                        }
                    ]
                }
            }";

            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            var dependencies = loanDataRequest.LoanDependencies;

            CollectionAssert.AreEquivalent(new[] { "HandleGetAssetTotals" }, dependencies);
        }

        [Test]
        public void LoanDependencies_GetLiabilityTotalsCommand_ReturnsExpectedValues()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""getLiabilityTotals"",
                            ""operationId"": ""25"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""type"": ""Loan""
                            }
                        }
                    ]
                }
            }";

            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            var dependencies = loanDataRequest.LoanDependencies;

            CollectionAssert.AreEquivalent(new[] { "HandleGetLiabilityTotals" }, dependencies);
        }

        [Test]
        public void LoanDependencies_GetRealPropertyTotalsCommand_ReturnsExpectedValues()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""getRealPropertyTotals"",
                            ""operationId"": ""25"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""type"": ""UladApplication"",
                                ""id"": ""33333333-3333-3333-3333-333333333333""
                            }
                        }
                    ]
                }
            }";

            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            var dependencies = loanDataRequest.LoanDependencies;

            CollectionAssert.AreEquivalent(new[] { nameof(CPageBase.HandleGetRealPropertyTotals) }, dependencies);
        }

        [Test]
        public void LoanDependencies_GetIncomeSourceTotalsCommand_ReturnsExpectedValues()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""getIncomeSourceTotals"",
                            ""operationId"": ""25"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""type"": ""UladApplication"",
                                ""id"": ""33333333-3333-3333-3333-333333333333""
                            }
                        }
                    ]
                }
            }";

            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            var dependencies = loanDataRequest.LoanDependencies;

            CollectionAssert.AreEquivalent(new[] { nameof(CPageBase.HandleGetIncomeSourceTotals) }, dependencies);
        }

        [Test]
        public void SetRequestNodeTree_NoSetRequest_ReturnsNull()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": []
                }
            }";
            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            var setRoot = loanDataRequest.SetRequestNodeTree;

            Assert.AreEqual(null, setRoot);
        }

        [Test]
        public void SetRequestNodeTree_Always_ReturnsCommandWithSpecifiedTransactionId()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""set"",
                            ""operationId"": ""26"",
                            ""args"": {
                                ""loan"": {
                                    ""sLT"": ""3""
                                }
                            }
                        }
                    ]
                }
            }";
            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            var operationId = loanDataRequest.SetRequestNodeTree.OperationId;

            Assert.AreEqual("26", operationId);
        }

        [Test]
        public void SetRequestNodeTree_WithSetRequest_ReturnsRootWithNullPathComponent()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""set"",
                            ""operationId"": ""26"",
                            ""args"": {
                                ""loan"": {
                                    ""sLT"": ""3""
                                }
                            }
                        }
                    ]
                }
            }";
            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            var setRoot = loanDataRequest.SetRequestNodeTree.Message;

            Assert.AreEqual(null, setRoot.PathComponent);
        }

        [Test]
        public void SetRequestNodeTree_SetTwoLoanLevelFields_ReturnsRootWithTwoChildren()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""set"",
                            ""operationId"": ""27"",
                            ""args"": {
                                ""loan"": {
                                    ""sLT"": ""3"",
                                    ""sLAmtCalc"": ""$100,000.00""
                                }
                            }
                        }
                    ]
                }
            }";
            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            var setRoot = loanDataRequest.SetRequestNodeTree.Message;

            var loan = setRoot.Children.Single();
            Assert.AreEqual(2, loan.Children.Count);
            Assert.AreEqual(new DataPathBasicElement("sLT"), loan.Children[0].PathComponent);
            Assert.AreEqual("3", loan.Children[0].Value);
            Assert.AreEqual(new DataPathBasicElement("sLAmtCalc"), loan.Children[1].PathComponent);
            Assert.AreEqual("$100,000.00", loan.Children[1].Value);
        }

        [Test]
        public void RemoveEntityCommands_ValidCommandFormat_HasSpecifiedValues()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""removeEntity"",
                            ""operationId"": ""28"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""collectionName"": ""Assets"",
                                ""id"": ""11111111-1111-1111-1111-111111111111""
                            }
                        }
                    ]
                }
            }";
            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            var operationId = loanDataRequest.RemoveEntityCommands.Single().OperationId;
            Assert.AreEqual("28", operationId);

            var removeEntity = loanDataRequest.RemoveEntityCommands.Single().Message;
            Assert.IsNotNull(removeEntity);
            Assert.AreEqual(DataPath.Create("loan"), removeEntity.Path);
            Assert.AreEqual("Assets", removeEntity.CollectionName);
            Assert.AreEqual(new Guid("11111111-1111-1111-1111-111111111111"), removeEntity.Id);
        }

        [Test]
        public void RemoveEntityCommands_ValidCommandFormatMultipleSpecified_HaveSpecifiedValues()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""removeEntity"",
                            ""operationId"": ""29"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""collectionName"": ""Assets"",
                                ""id"": ""11111111-1111-1111-1111-111111111111""
                            }
                        },
                        {
                            ""name"": ""removeEntity"",
                            ""operationId"": ""30"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""collectionName"": ""RealProperties"",
                                ""id"": ""22222222-2222-2222-2222-222222222222""
                            }
                        }
                    ]
                }
            }";
            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            var removeEntityCommands = loanDataRequest.RemoveEntityCommands;
            Assert.AreEqual(2, removeEntityCommands.Count);

            var operationId = removeEntityCommands[0].OperationId;
            Assert.AreEqual("29", operationId);
            var removeEntity = removeEntityCommands[0].Message;
            Assert.AreEqual(DataPath.Create("loan"), removeEntity.Path);
            Assert.AreEqual("Assets", removeEntity.CollectionName);
            Assert.AreEqual(new Guid("11111111-1111-1111-1111-111111111111"), removeEntity.Id);

            operationId = removeEntityCommands[1].OperationId;
            Assert.AreEqual("30", operationId);
            removeEntity = removeEntityCommands[1].Message;
            Assert.AreEqual(DataPath.Create("loan"), removeEntity.Path);
            Assert.AreEqual("RealProperties", removeEntity.CollectionName);
            Assert.AreEqual(new Guid("22222222-2222-2222-2222-222222222222"), removeEntity.Id);
        }

        [Test]
        public void RemoveAssociationCommands_ValidCommandFormat_HasSpecifiedValues()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""removeAssociation"",
                            ""operationId"": ""31"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""associationSet"": ""RealPropertyLiabilities"",
                                ""id"": ""11111111-1111-1111-1111-111111111111""
                            }
                        }
                    ]
                }
            }";
            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            var operationId = loanDataRequest.RemoveAssociationCommands.Single().OperationId;
            Assert.AreEqual("31", operationId);

            var removeAssociation = loanDataRequest.RemoveAssociationCommands.Single().Message;
            Assert.IsNotNull(removeAssociation);
            Assert.AreEqual(DataPath.Create("loan"), removeAssociation.Path);
            Assert.AreEqual("RealPropertyLiabilities", removeAssociation.AssociationSet);
            Assert.AreEqual(new Guid("11111111-1111-1111-1111-111111111111"), removeAssociation.Id);
        }

        [Test]
        public void RemoveAssociationCommands_ValidCommandFormatMultipleSpecified_HaveSpecifiedValues()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""removeAssociation"",
                            ""operationId"": ""32"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""associationSet"": ""RealPropertyLiabilities"",
                                ""id"": ""11111111-1111-1111-1111-111111111111""
                            }
                        },
                        {
                            ""name"": ""removeAssociation"",
                            ""operationId"": ""33"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""associationSet"": ""ConsumerLiabilities"",
                                ""id"": ""22222222-2222-2222-2222-222222222222""
                            }
                        }
                    ]
                }
            }";
            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            var removeAssociationCommands = loanDataRequest.RemoveAssociationCommands;

            var operationId = removeAssociationCommands[0].OperationId;
            Assert.AreEqual("32", operationId);
            var removeAssociation = removeAssociationCommands[0].Message;
            Assert.AreEqual(DataPath.Create("loan"), removeAssociation.Path);
            Assert.AreEqual("RealPropertyLiabilities", removeAssociation.AssociationSet);
            Assert.AreEqual(new Guid("11111111-1111-1111-1111-111111111111"), removeAssociation.Id);

            operationId = removeAssociationCommands[1].OperationId;
            Assert.AreEqual("33", operationId);
            removeAssociation = removeAssociationCommands[1].Message;
            Assert.AreEqual(DataPath.Create("loan"), removeAssociation.Path);
            Assert.AreEqual("ConsumerLiabilities", removeAssociation.AssociationSet);
            Assert.AreEqual(new Guid("22222222-2222-2222-2222-222222222222"), removeAssociation.Id);
        }

        [Test]
        public void AddEntityCommands_ValidCommandFormat_HasSpecifiedValues()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""addEntity"",
                            ""operationId"": ""34"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""collectionName"": ""Assets"",
                                ""ownerId"": ""11111111-1111-1111-1111-111111111111"",
                                ""additionalOwnerIds"": [""22222222-2222-2222-2222-222222222222"", ""33333333-3333-3333-3333-333333333333""],
                                ""index"": 1,
                                ""id"": null
                            }
                        }
                    ]
                }
            }";

            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            var operationId = loanDataRequest.AddEntityCommands.Single().OperationId;
            Assert.AreEqual("34", operationId);

            var addEntity = loanDataRequest.AddEntityCommands.Single().Message;
            Assert.IsNotNull(addEntity);
            Assert.AreEqual(DataPath.Create("loan"), addEntity.Path);
            Assert.AreEqual("Assets", addEntity.CollectionName);
            CollectionAssert.AreEqual(new[] { new Guid("22222222-2222-2222-2222-222222222222"), new Guid("33333333-3333-3333-3333-333333333333") }, addEntity.AdditionalOwnerIds);
            Assert.AreEqual(1, addEntity.Index);
        }

        [Test]
        public void AddEntityCommands_ValidCommandFormat_WithPropValues()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""addEntity"",
                            ""operationId"": ""34"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""collectionName"": ""Assets"",
                                ""ownerId"": ""11111111-1111-1111-1111-111111111111"",
                                ""additionalOwnerIds"": [""22222222-2222-2222-2222-222222222222"", ""33333333-3333-3333-3333-333333333333""],
                                ""fields"": {
                                    ""AssetType"": ""3""
                                },
                                ""index"": 1,
                                ""id"": null
                            }
                        }
                    ]
                }
            }";

            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            var operationId = loanDataRequest.AddEntityCommands.Single().OperationId;
            Assert.AreEqual("34", operationId);

            var addEntity = loanDataRequest.AddEntityCommands.Single().Message;
            Assert.IsNotNull(addEntity);
            Assert.AreEqual(DataPath.Create("loan"), addEntity.Path);
            Assert.AreEqual("Assets", addEntity.CollectionName);
            CollectionAssert.AreEqual(new[] { new Guid("22222222-2222-2222-2222-222222222222"), new Guid("33333333-3333-3333-3333-333333333333") }, addEntity.AdditionalOwnerIds);
            Assert.AreEqual(1, addEntity.Fields.Children.Count);
            Assert.AreEqual(1, addEntity.Index);
        }

        [Test]
        public void AddEntityCommands_ValidCommandFormatMultipleSpecified_HaveSpecifiedValues()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""addEntity"",
                            ""operationId"": ""35"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""collectionName"": ""Assets"",
                                ""ownerId"": ""11111111-1111-1111-1111-111111111111"",
                                ""id"": null
                            }
                        },
                        {
                            ""name"": ""addEntity"",
                            ""operationId"": ""36"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""collectionName"": ""PublicRecords"",
                                ""ownerId"": ""22222222-2222-2222-2222-222222222222"",
                                ""id"": null
                            }
                        },
                        {
                            ""name"": ""addEntity"",
                            ""operationId"": ""37"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""collectionName"": ""CounselingEvents"",
                                ""attendeeIds"": [
                                    ""33333333-3333-3333-3333-333333333333"",
                                    ""44444444-4444-4444-4444-444444444444""
                                ]
                            }
                        }
                    ]
                }
            }";

            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            IReadOnlyList<ServiceMessage<AddEntity>> addEntityCommands = loanDataRequest.AddEntityCommands;
            Assert.AreEqual(3, addEntityCommands.Count);

            string operationId = addEntityCommands[0].OperationId;
            Assert.AreEqual("35", operationId);
            AddEntity addEntity = addEntityCommands[0].Message;
            Assert.AreEqual(DataPath.Create("loan"), addEntity.Path);
            Assert.AreEqual("Assets", addEntity.CollectionName);
            Assert.AreEqual(new Guid("11111111-1111-1111-1111-111111111111"), addEntity.OwnerId);

            operationId = addEntityCommands[1].OperationId;
            Assert.AreEqual("36", operationId);
            addEntity = addEntityCommands[1].Message;
            Assert.AreEqual(DataPath.Create("loan"), addEntity.Path);
            Assert.AreEqual("PublicRecords", addEntity.CollectionName);
            Assert.AreEqual(new Guid("22222222-2222-2222-2222-222222222222"), addEntity.OwnerId);

            Assert.AreEqual("37", addEntityCommands[2].OperationId);
            addEntity = addEntityCommands[2].Message;
            Assert.AreEqual(DataPath.Create("loan"), addEntity.Path);
            Assert.AreEqual("CounselingEvents", addEntity.CollectionName);
            Assert.AreEqual(new Guid("33333333-3333-3333-3333-333333333333"), addEntity.OwnerId);
            CollectionAssert.AreEqual(new[] { new Guid("44444444-4444-4444-4444-444444444444"), }, addEntity.AdditionalOwnerIds);
        }

        [Test]
        public void AddAssociationCommands_ValidCommandFormat_HasSpecifiedValues()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""addAssociation"",
                            ""operationId"": ""37"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""associationSet"": ""RealPropertyLiabilities"",
                                ""firstId"": ""11111111-1111-1111-1111-111111111111"",
                                ""secondId"": ""22222222-2222-2222-2222-222222222222""
                            }
                        }
                    ]
                }
            }";

            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            var operationId = loanDataRequest.AddAssociationCommands.Single().OperationId;
            Assert.AreEqual("37", operationId);

            var addAssociation = loanDataRequest.AddAssociationCommands.Single().Message;
            Assert.IsNotNull(addAssociation);
            Assert.AreEqual(DataPath.Create("loan"), addAssociation.Path);
            Assert.AreEqual("RealPropertyLiabilities", addAssociation.AssociationSet);
            Assert.AreEqual(Guid.Parse("11111111-1111-1111-1111-111111111111"), addAssociation.FirstIdentifier);
            Assert.AreEqual(Guid.Parse("22222222-2222-2222-2222-222222222222"), addAssociation.SecondIdentifier);
        }

        [Test]
        public void AddAssociationCommands_ValidCommandFormatMultipleSpecified_HaveSpecifiedValues()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""addAssociation"",
                            ""operationId"": ""38"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""associationSet"": ""RealPropertyLiabilities"",
                                ""firstId"": ""11111111-1111-1111-1111-111111111111"",
                                ""secondId"": ""22222222-2222-2222-2222-222222222222""
                            }
                        },
                        {
                            ""name"": ""addAssociation"",
                            ""operationId"": ""39"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""associationSet"": ""ConsumerLiabilities"",
                                ""firstId"": ""33333333-3333-3333-3333-333333333333"",
                                ""secondId"": ""44444444-4444-4444-4444-444444444444""
                            }
                        }
                    ]
                }
            }";

            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            var addAssociationCommands = loanDataRequest.AddAssociationCommands;

            var operationId = addAssociationCommands[0].OperationId;
            Assert.AreEqual("38", operationId);
            var addAssociation = addAssociationCommands[0].Message;
            Assert.AreEqual(DataPath.Create("loan"), addAssociation.Path);
            Assert.AreEqual("RealPropertyLiabilities", addAssociation.AssociationSet);
            Assert.AreEqual(Guid.Parse("11111111-1111-1111-1111-111111111111"), addAssociation.FirstIdentifier);
            Assert.AreEqual(Guid.Parse("22222222-2222-2222-2222-222222222222"), addAssociation.SecondIdentifier);

            operationId = addAssociationCommands[1].OperationId;
            Assert.AreEqual("39", operationId);
            addAssociation = addAssociationCommands[1].Message;
            Assert.AreEqual(DataPath.Create("loan"), addAssociation.Path);
            Assert.AreEqual("ConsumerLiabilities", addAssociation.AssociationSet);
            Assert.AreEqual(Guid.Parse("33333333-3333-3333-3333-333333333333"), addAssociation.FirstIdentifier);
            Assert.AreEqual(Guid.Parse("44444444-4444-4444-4444-444444444444"), addAssociation.SecondIdentifier);
        }

        [Test]
        public void SetOwnershipCommand_ValidCommandFormat_HasSpecifiedValues()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""setOwnership"",
                            ""operationId"": ""40"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""associationName"": ""ConsumerAssets"",
                                ""id"": ""11111111-1111-1111-1111-111111111111"",
                                ""primaryOwnerId"": ""22222222-2222-2222-2222-222222222222"",
                                ""additionalOwnerIds"": [""33333333-3333-3333-3333-333333333333"", ""44444444-4444-4444-4444-444444444444""]
                            }
                        }
                    ]
                }
            }";
            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            var operationId = loanDataRequest.SetOwnershipCommand.OperationId;
            Assert.AreEqual("40", operationId);

            var command = loanDataRequest.SetOwnershipCommand.Message;
            Assert.IsNotNull(command);
            Assert.AreEqual(DataPath.Create("loan"), command.Path);
            Assert.AreEqual("ConsumerAssets", command.AssociationName);
            Assert.AreEqual(new Guid("11111111-1111-1111-1111-111111111111"), command.RecordId);
            Assert.AreEqual(new Guid("22222222-2222-2222-2222-222222222222"), command.PrimaryOwnerId.Value);
            CollectionAssert.AreEquivalent(new[] { new Guid("33333333-3333-3333-3333-333333333333"), new Guid("44444444-4444-4444-4444-444444444444") }, command.AdditionalOwnerIds.Select(id => id.Value));
        }

        [Test]
        public void SetAttendanceCommand_SetOwnershipCommandFormat_ThrowsException()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""setAttendance"",
                            ""operationId"": ""40"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""associationName"": ""CounselingEventAttendances"",
                                ""id"": ""11111111-1111-1111-1111-111111111111"",
                                ""primaryOwnerId"": ""22222222-2222-2222-2222-222222222222"",
                                ""additionalOwnerIds"": [""33333333-3333-3333-3333-333333333333"", ""44444444-4444-4444-4444-444444444444""]
                            }
                        }
                    ]
                }
            }";
            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            ServiceMessage<SetOwnership> command;
            Assert.Throws<CBaseException>(() => command = loanDataRequest.SetOwnershipCommand);
        }

        [Test]
        public void SetAttendanceCommand_ValidCommandFormat_HasSpecifiedValues()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""setAttendance"",
                            ""operationId"": ""40"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""associationName"": ""CounselingEventAttendances"",
                                ""id"": ""11111111-1111-1111-1111-111111111111"",
                                ""attendeeIds"": [
                                    ""22222222-2222-2222-2222-222222222222"",
                                    ""33333333-3333-3333-3333-333333333333"",
                                    ""44444444-4444-4444-4444-444444444444""
                                ]
                            }
                        }
                    ]
                }
            }";
            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            string operationId = loanDataRequest.SetOwnershipCommand.OperationId;
            Assert.AreEqual("40", operationId);

            SetOwnership command = loanDataRequest.SetOwnershipCommand.Message;
            Assert.IsNotNull(command);
            Assert.AreEqual(DataPath.Create("loan"), command.Path);
            Assert.AreEqual("CounselingEventAttendances", command.AssociationName);
            Assert.AreEqual(new Guid("11111111-1111-1111-1111-111111111111"), command.RecordId);
            Assert.AreEqual(new Guid("22222222-2222-2222-2222-222222222222"), command.PrimaryOwnerId.Value);
            CollectionAssert.AreEquivalent(new[] { new Guid("33333333-3333-3333-3333-333333333333"), new Guid("44444444-4444-4444-4444-444444444444") }, command.AdditionalOwnerIds.Select(id => id.Value));
        }

        [Test]
        public void SetOrderCommand_ValidCommandFormat_HasExpectedValues()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""setOrder"",
                            ""operationId"": ""41"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""collectionName"": ""Assets"",
                                ""order"": [""33333333-3333-3333-3333-333333333333"", ""44444444-4444-4444-4444-444444444444""]
                            }
                        }
                    ]
                }
            }";

            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            var operationId = loanDataRequest.SetOrderCommand.OperationId;
            Assert.AreEqual("41", operationId);

            var command = loanDataRequest.SetOrderCommand.Message;
            Assert.IsNotNull(command);
            Assert.AreEqual(DataPath.Create("loan"), command.Path);
            Assert.AreEqual("Assets", command.CollectionName);
            CollectionAssert.AreEqual(new[] { new Guid("33333333-3333-3333-3333-333333333333"), new Guid("44444444-4444-4444-4444-444444444444") }, command.Order);
        }

        [Test]
        public void SetOrderCommand_ValidConsumerCommandFormat_HasExpectedValues()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""setOrder"",
                            ""operationId"": ""41"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""collectionName"": ""Consumers"",
                                ""uladApplicationId"": ""55555555-5555-5555-5555-555555555555"",
                                ""order"": [""33333333-3333-3333-3333-333333333333"", ""44444444-4444-4444-4444-444444444444""]
                            }
                        }
                    ]
                }
            }";

            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            var operationId = loanDataRequest.SetOrderCommand.OperationId;
            Assert.AreEqual("41", operationId);

            var command = loanDataRequest.SetOrderCommand.Message;
            Assert.IsNotNull(command);
            Assert.AreEqual(DataPath.Create("loan"), command.Path);
            Assert.AreEqual("Consumers", command.CollectionName);
            CollectionAssert.AreEqual(new[] { new Guid("33333333-3333-3333-3333-333333333333"), new Guid("44444444-4444-4444-4444-444444444444") }, command.Order);
        }

        [Test]
        public void SetOrderCommand_NoPathSpecified_ThrowsException()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""setOrder"",
                            ""operationId"": ""42"",
                            ""args"": {
                                ""collectionName"": ""Assets"",
                                ""order"": [""33333333-3333-3333-3333-333333333333"", ""44444444-4444-4444-4444-444444444444""]
                            }
                        }
                    ]
                }
            }";

            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            ServiceMessage<SetOrder> command;
            Assert.Throws<CBaseException>(() => command = loanDataRequest.SetOrderCommand);
        }

        [Test]
        public void SetOrderCommand_NoCollectionNameSpecified_ThrowsException()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""setOrder"",
                            ""operationId"": ""43"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""order"": [""33333333-3333-3333-3333-333333333333"", ""44444444-4444-4444-4444-444444444444""]
                            }
                        }
                    ]
                }
            }";

            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            ServiceMessage<SetOrder> command;
            Assert.Throws<CBaseException>(() => command = loanDataRequest.SetOrderCommand);
        }

        [Test]
        public void SetOrderCommand_NoOrderSpecified_ThrowsException()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""setOrder"",
                            ""operationId"": ""44"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""collectionName"": ""Assets"",
                            }
                        }
                    ]
                }
            }";

            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            ServiceMessage<SetOrder> command;
            Assert.Throws<CBaseException>(() => command = loanDataRequest.SetOrderCommand);
        }

        [Test]
        public void SetOrderCommand_OrderWithInvalidIds_ThrowsException()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""setOrder"",
                            ""operationId"": ""45"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""collectionName"": ""Assets"",
                                ""order"": [""LALALALALA""]
                            }
                        }
                    ]
                }
            }";

            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            ServiceMessage<SetOrder> command;
            Assert.Throws<CBaseException>(() => command = loanDataRequest.SetOrderCommand);
        }

        [Test]
        public void SetOrderCommand_ConsumerEmptyUladAppId_ThrowsException()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""setOrder"",
                            ""operationId"": ""41"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""collectionName"": ""Consumers"",
                                ""uladApplicationId"": """",
                                ""order"": [""33333333-3333-3333-3333-333333333333"", ""44444444-4444-4444-4444-444444444444""]
                            }
                        }
                    ]
                }
            }";

            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            ServiceMessage<SetOrder> command;
            Assert.Throws<CBaseException>(() => command = loanDataRequest.SetOrderCommand);
        }

        [Test]
        public void SetOrderCommand_ConsumerInvalidUladAppId_ThrowsException()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""setOrder"",
                            ""operationId"": ""41"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""collectionName"": ""Consumers"",
                                ""uladApplicationId"": ""1963"",
                                ""order"": [""33333333-3333-3333-3333-333333333333"", ""44444444-4444-4444-4444-444444444444""]
                            }
                        }
                    ]
                }
            }";

            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            ServiceMessage<SetOrder> command;
            Assert.Throws<CBaseException>(() => command = loanDataRequest.SetOrderCommand);
        }

        [Test]
        public void SetOrderCommand_ConsumerIntegerUladAppId_ThrowsException()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""setOrder"",
                            ""operationId"": ""41"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""collectionName"": ""Consumers"",
                                ""uladApplicationId"": 1963,
                                ""order"": [""33333333-3333-3333-3333-333333333333"", ""44444444-4444-4444-4444-444444444444""]
                            }
                        }
                    ]
                }
            }";

            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            ServiceMessage<SetOrder> command;
            Assert.Throws<CBaseException>(() => command = loanDataRequest.SetOrderCommand);
        }

        [Test]
        public void RemoveEntityCommands_NoPathSpecified_ThrowsException()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""removeEntity"",
                            ""operationId"": ""46"",
                            ""args"": {
                                ""collectionName"": ""Assets"",
                                ""id"": ""33333333-3333-3333-3333-333333333333""
                            }
                        }
                    ]
                }
            }";

            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            IReadOnlyList<ServiceMessage<RemoveEntity>> commands;
            Assert.Throws<CBaseException>(() => commands = loanDataRequest.RemoveEntityCommands);
        }

        [Test]
        public void RemoveEntityCommands_NoCollectionNameSpecified_ThrowsException()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""removeEntity"",
                            ""operationId"": ""47"",
                            ""args"": {
                                ""path"" : ""loan"",
                                ""id"": ""33333333-3333-3333-3333-333333333333""
                            }
                        }
                    ]
                }
            }";

            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            IReadOnlyList<ServiceMessage<RemoveEntity>> commands;
            Assert.Throws<CBaseException>(() => commands = loanDataRequest.RemoveEntityCommands);
        }

        [Test]
        public void RemoveEntityCommands_NoIdSpecified_ThrowsException()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""removeEntity"",
                            ""operationId"": ""48"",
                            ""args"": {
                                ""path"" : ""loan"",
                                ""collectionName"": ""Assets""
                            }
                        }
                    ]
                }
            }";

            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            IReadOnlyList<ServiceMessage<RemoveEntity>> commands;
            Assert.Throws<CBaseException>(() => commands = loanDataRequest.RemoveEntityCommands);
        }

        [Test]
        public void RemoveEntityCommands_NotAGuidIdSpecified_ThrowsException()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""removeEntity"",
                            ""operationId"": ""49"",
                            ""args"": {
                                ""path"" : ""loan"",
                                ""collectionName"": ""Assets"",
                                ""id"": ""33333333-3333-3333-3333-33333333333X""
                            }
                        }
                    ]
                }
            }";

            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            IReadOnlyList<ServiceMessage<RemoveEntity>> commands;
            Assert.Throws<CBaseException>(() => commands = loanDataRequest.RemoveEntityCommands);
        }

        [Test]
        public void AddEntityCommands_NoPathSpecified_ThrowsException()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""addEntity"",
                            ""operationId"": ""50"",
                            ""args"": {
                                ""collectionName"": ""Assets"",
                                ""ownerId"": ""33333333-3333-3333-3333-333333333333""
                            }
                        }
                    ]
                }
            }";

            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            IReadOnlyList<ServiceMessage<AddEntity>> commands;
            Assert.Throws<CBaseException>(() => commands = loanDataRequest.AddEntityCommands);
        }

        [Test]
        public void AddEntityCommands_NoCollectionNameSpecified_ThrowsException()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""addEntity"",
                            ""operationId"": ""51"",
                            ""args"": {
                                ""path"" : ""loan"",
                                ""ownerId"": ""33333333-3333-3333-3333-333333333333""
                            }
                        }
                    ]
                }
            }";

            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            IReadOnlyList<ServiceMessage<AddEntity>> commands;
            Assert.Throws<CBaseException>(() => commands = loanDataRequest.AddEntityCommands);
        }

        [Test]
        public void AddEntityCommands_NoOwnerIdSpecifiedForLoanLevelCollection_ThrowsException()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""addEntity"",
                            ""operationId"": ""52"",
                            ""args"": {
                                ""path"" : ""loan"",
                                ""collectionName"": ""Assets""
                            }
                        }
                    ]
                }
            }";

            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            IReadOnlyList<ServiceMessage<AddEntity>> commands;
            Assert.Throws<CBaseException>(() => commands = loanDataRequest.AddEntityCommands);
        }

        [Test]
        public void AddEntityCommands_NotAGuidOwnerIdSpecified_ThrowsException()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""addEntity"",
                            ""operationId"": ""53"",
                            ""args"": {
                                ""path"" : ""loan"",
                                ""collectionName"": ""Assets"",
                                ""ownerId"": ""33333333-3333-3333-3333-33333333333X""
                            }
                        }
                    ]
                }
            }";

            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            IReadOnlyList<ServiceMessage<AddEntity>> commands;
            Assert.Throws<CBaseException>(() => commands = loanDataRequest.AddEntityCommands);
        }

        [Test]
        public void SetOwnershipCommand_NoPathSpecified_ThrowsException()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""setOwnership"",
                            ""operationId"": ""54"",
                            ""args"": {
                                ""associationName"": ""ConsumerAssets"",
                                ""id"": ""33333333-3333-3333-3333-333333333333"",
                                ""primaryOwnerId"": ""33333333-3333-3333-3333-333333333333""
                            }
                        }
                    ]
                }
            }";

            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            ServiceMessage<SetOwnership> command;
            Assert.Throws<CBaseException>(() => command = loanDataRequest.SetOwnershipCommand);
        }

        [Test]
        public void SetOwnershipCommand_NoCollectionNameSpecified_ThrowsException()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""setOwnership"",
                            ""operationId"": ""55"",
                            ""args"": {
                                ""path"" : ""loan"",
                                ""id"": ""33333333-3333-3333-3333-333333333333"",
                                ""primaryOwnerId"": ""33333333-3333-3333-3333-333333333333""
                            }
                        }
                    ]
                }
            }";

            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            ServiceMessage<SetOwnership> command;
            Assert.Throws<CBaseException>(() => command = loanDataRequest.SetOwnershipCommand);
        }

        [Test]
        public void SetOwnershipCommand_NoIdSpecified_ThrowsException()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""setOwnership"",
                            ""operationId"": ""56"",
                            ""args"": {
                                ""path"" : ""loan"",
                                ""associationName"": ""ConsumerAssets"",
                                ""primaryOwnerId"": ""33333333-3333-3333-3333-333333333333""
                            }
                        }
                    ]
                }
            }";

            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            ServiceMessage<SetOwnership> command;
            Assert.Throws<CBaseException>(() => command = loanDataRequest.SetOwnershipCommand);
        }

        [Test]
        public void SetOwnershipCommand_NotAGuidIdSpecified_ThrowsException()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""setOwnership"",
                            ""operationId"": ""57"",
                            ""args"": {
                                ""path"" : ""loan"",
                                ""associationName"": ""ConsumerAssets"",
                                ""id"": ""33333333-3333-3333-3333-33333333333X"",
                                ""primaryOwnerId"": ""33333333-3333-3333-3333-333333333333""
                            }
                        }
                    ]
                }
            }";

            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            ServiceMessage<SetOwnership> command;
            Assert.Throws<CBaseException>(() => command = loanDataRequest.SetOwnershipCommand);
        }

        [Test]
        public void SetOwnershipCommand_NoPrimaryOwnerIdSpecified_ThrowsException()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""setOwnership"",
                            ""operationId"": ""58"",
                            ""args"": {
                                ""path"" : ""loan"",
                                ""associationName"": ""ConsumerAssets"",
                                ""id"": ""33333333-3333-3333-3333-333333333333""
                            }
                        }
                    ]
                }
            }";

            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            ServiceMessage<SetOwnership> command;
            Assert.Throws<CBaseException>(() => command = loanDataRequest.SetOwnershipCommand);
        }

        [Test]
        public void SetOwnershipCommand_NotAGuidPrimaryOwnerIdSpecified_ThrowsException()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""setOwnership"",
                            ""operationId"": ""59"",
                            ""args"": {
                                ""path"" : ""loan"",
                                ""associationName"": ""ConsumerAssets"",
                                ""id"": ""33333333-3333-3333-3333-333333333333"",
                                ""primaryOwnerId"": ""33333333-3333-3333-3333-33333333333X""
                            }
                        }
                    ]
                }
            }";

            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            ServiceMessage<SetOwnership> command;
            Assert.Throws<CBaseException>(() => command = loanDataRequest.SetOwnershipCommand);
        }

        [Test]
        public void AddLiabilityBalanceAndPaymentToRealPropertyCommand_ValidCommandFormat_HasExpectedValues()
        {
            var liabilityId = new Guid("4d4b162b-e02f-4448-b324-5ed53c4251cc");
            var realPropertyId = new Guid("ea3064fe-9547-4bbb-b25e-feb9d5f5516c");
            string json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""addLiabilityBalanceAndPaymentToRealProperty"",
                            ""operationId"": ""26"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""liabilityId"": ""4d4b162b-e02f-4448-b324-5ed53c4251cc"",
                                ""realPropertyId"": ""ea3064fe-9547-4bbb-b25e-feb9d5f5516c""
                            }
                        }
                    ]
                }
            }";

            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            IReadOnlyList<ServiceMessage<AddLiabilityBalanceAndPaymentToRealProperty>> commands = loanDataRequest.AddLiabilityBalanceAndPaymentToRealPropertyCommands;
            Assert.AreEqual(1, commands.Count);
            Assert.AreEqual(liabilityId, commands.First().Message.LiabilityIdentifier.Value);
            Assert.AreEqual(realPropertyId, commands.First().Message.RealPropertyIdentifier.Value);
        }

        [Test]
        public void AddLiabilityBalanceAndPaymentToRealPropertyCommand_LiabilityIdSpecifiedNotAGuid_ThrowsException()
        {
            string json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""addLiabilityBalanceAndPaymentToRealProperty"",
                            ""operationId"": ""26"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""liabilityId"": ""455"",
                                ""realPropertyId"": ""ea3064fe-9547-4bbb-b25e-feb9d5f5516c""
                            }
                        }
                    ]
                }
            }";

            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            Assert.Throws<CBaseException>(() => Console.WriteLine(loanDataRequest.AddLiabilityBalanceAndPaymentToRealPropertyCommands));
        }

        [Test]
        public void AddLiabilityBalanceAndPaymentToRealPropertyCommand_RealPropertyIdSpecifiedNotAGuid_ThrowsException()
        {
            // The invalid realPropertyId is subtle, but there's an 'x' instead of a valid hex digit
            string json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""addLiabilityBalanceAndPaymentToRealProperty"",
                            ""operationId"": ""26"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""liabilityId"": ""2853a81d-33ba-4ec8-842e-94d844539d58"",
                                ""realPropertyId"": ""eax064fe-9547-4bbb-b25e-feb9d5f5516c""
                            }
                        }
                    ]
                }
            }";

            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            Assert.Throws<CBaseException>(() => Console.WriteLine(loanDataRequest.AddLiabilityBalanceAndPaymentToRealPropertyCommands));
        }

        [Test]
        public void GetAssetTotalsCommands_ValidCommandFormatUladApp_HasExpectedValues()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""getAssetTotals"",
                            ""operationId"": ""60"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""type"": ""UladApplication"",
                                ""id"": ""33333333-3333-3333-3333-333333333333""
                            }
                        }
                    ]
                }
            }";

            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            var operationId = loanDataRequest.GetAssetTotalsCommands.Single().OperationId;
            Assert.AreEqual("60", operationId);

            var command = loanDataRequest.GetAssetTotalsCommands.Single().Message;
            Assert.AreEqual(DataPath.Create("loan"), command.Path);
            Assert.AreEqual(TotalsType.UladApplication, command.TotalsType);
            Assert.AreEqual(new Guid("33333333-3333-3333-3333-333333333333"), command.Id);
        }

        [Test]
        public void GetAssetTotalsCommands_ValidCommandFormatConsumer_HasExpectedValues()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""getAssetTotals"",
                            ""operationId"": ""61"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""type"": ""Consumer"",
                                ""id"": ""33333333-3333-3333-3333-333333333333""
                            }
                        }
                    ]
                }
            }";

            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            var command = loanDataRequest.GetAssetTotalsCommands.Single().Message;

            Assert.AreEqual(DataPath.Create("loan"), command.Path);
            Assert.AreEqual(TotalsType.Consumer, command.TotalsType);
            Assert.AreEqual(new Guid("33333333-3333-3333-3333-333333333333"), command.Id);
        }

        [Test]
        public void GetAssetTotalsCommands_ValidCommandFormatLegacyApp_HasExpectedValues()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""getAssetTotals"",
                            ""operationId"": ""62"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""type"": ""LegacyApplication"",
                                ""id"": ""33333333-3333-3333-3333-333333333333""
                            }
                        }
                    ]
                }
            }";

            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            var command = loanDataRequest.GetAssetTotalsCommands.Single().Message;

            Assert.AreEqual(DataPath.Create("loan"), command.Path);
            Assert.AreEqual(TotalsType.LegacyApplication, command.TotalsType);
            Assert.AreEqual(new Guid("33333333-3333-3333-3333-333333333333"), command.Id);
        }

        [Test]
        public void GetAssetTotalsCommands_NoPathSpecified_ThrowsException()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""getAssetTotals"",
                            ""operationId"": ""63"",
                            ""args"": {
                            }
                        }
                    ]
                }
            }";

            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            IReadOnlyList<ServiceMessage<GetTotals<DataObjectKind.Asset>>> commands;
            Assert.Throws<CBaseException>(() => commands = loanDataRequest.GetAssetTotalsCommands);
        }

        [Test]
        public void GetAssetTotalsCommands_LoanLevel_HasExpectedValues()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""getAssetTotals"",
                            ""operationId"": ""64"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""type"": ""Loan""
                            }
                        }
                    ]
                }
            }";

            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            var command = loanDataRequest.GetAssetTotalsCommands.Single().Message;

            Assert.AreEqual(DataPath.Create("loan"), command.Path);
            Assert.AreEqual(TotalsType.Loan, command.TotalsType);
            Assert.AreEqual(null, command.Id);
        }

        [Test]
        public void GetAssetTotalsCommands_InvalidTypeSpecified_ThrowsException()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""getAssetTotals"",
                            ""operationId"": ""65"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""type"": ""MEOW"",
                                ""id"": ""44444444-4444-4444-4444-444444444444""
                            }
                        }
                    ]
                }
            }";

            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            IReadOnlyList<ServiceMessage<GetTotals<DataObjectKind.Asset>>> commands;
            Assert.Throws<CBaseException>(() => commands = loanDataRequest.GetAssetTotalsCommands);
        }

        [Test]
        public void GetAssetTotalsCommands_MultipleSpecified_HaveExpectedValues()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""getAssetTotals"",
                            ""operationId"": ""66"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""type"": ""Loan""
                            }
                        },
                        {
                            ""name"": ""getAssetTotals"",
                            ""operationId"": ""67"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""type"": ""Consumer"",
                                ""id"": ""33333333-3333-3333-3333-333333333333""
                            }
                        }
                    ]
                }
            }";

            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            var getAssetTotalsCommands = loanDataRequest.GetAssetTotalsCommands;
            Assert.AreEqual(2, getAssetTotalsCommands.Count);

            var operationId = getAssetTotalsCommands[0].OperationId;
            Assert.AreEqual("66", operationId);
            var command = getAssetTotalsCommands[0].Message;
            Assert.AreEqual(DataPath.Create("loan"), command.Path);
            Assert.AreEqual(TotalsType.Loan, command.TotalsType);
            Assert.AreEqual(null, command.Id);

            operationId = getAssetTotalsCommands[1].OperationId;
            Assert.AreEqual("67", operationId);
            command = getAssetTotalsCommands[1].Message;
            Assert.AreEqual(DataPath.Create("loan"), command.Path);
            Assert.AreEqual(TotalsType.Consumer, command.TotalsType);
            Assert.AreEqual(new Guid("33333333-3333-3333-3333-333333333333"), command.Id);
        }

        [Test]
        public void GetLiabilityTotalsCommands_ValidCommandFormatUladApp_HasExpectedValues()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""getLiabilityTotals"",
                            ""operationId"": ""60"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""type"": ""UladApplication"",
                                ""id"": ""33333333-3333-3333-3333-333333333333""
                            }
                        }
                    ]
                }
            }";

            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            var operationId = loanDataRequest.GetLiabilityTotalsCommands.Single().OperationId;
            Assert.AreEqual("60", operationId);

            var command = loanDataRequest.GetLiabilityTotalsCommands.Single().Message;
            Assert.AreEqual(DataPath.Create("loan"), command.Path);
            Assert.AreEqual(TotalsType.UladApplication, command.TotalsType);
            Assert.AreEqual(new Guid("33333333-3333-3333-3333-333333333333"), command.Id);
        }

        [Test]
        public void GetLiabilityTotalsCommands_ValidCommandFormatConsumer_HasExpectedValues()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""getLiabilityTotals"",
                            ""operationId"": ""61"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""type"": ""Consumer"",
                                ""id"": ""33333333-3333-3333-3333-333333333333""
                            }
                        }
                    ]
                }
            }";

            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            var command = loanDataRequest.GetLiabilityTotalsCommands.Single().Message;

            Assert.AreEqual(DataPath.Create("loan"), command.Path);
            Assert.AreEqual(TotalsType.Consumer, command.TotalsType);
            Assert.AreEqual(new Guid("33333333-3333-3333-3333-333333333333"), command.Id);
        }

        [Test]
        public void GetLiabilityTotalsCommands_ValidCommandFormatLegacyApp_HasExpectedValues()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""getLiabilityTotals"",
                            ""operationId"": ""62"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""type"": ""LegacyApplication"",
                                ""id"": ""33333333-3333-3333-3333-333333333333""
                            }
                        }
                    ]
                }
            }";

            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            var command = loanDataRequest.GetLiabilityTotalsCommands.Single().Message;

            Assert.AreEqual(DataPath.Create("loan"), command.Path);
            Assert.AreEqual(TotalsType.LegacyApplication, command.TotalsType);
            Assert.AreEqual(new Guid("33333333-3333-3333-3333-333333333333"), command.Id);
        }

        [Test]
        public void GetLiabilityTotalsCommands_NoPathSpecified_ThrowsException()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""getLiabilityTotals"",
                            ""operationId"": ""63"",
                            ""args"": {
                                ""type"": ""Consumer"",
                                ""id"": ""33333333-3333-3333-3333-333333333333""
                            }
                        }
                    ]
                }
            }";

            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            IReadOnlyList<ServiceMessage<GetTotals<DataObjectKind.Liability>>> commands;
            Assert.Throws<CBaseException>(() => commands = loanDataRequest.GetLiabilityTotalsCommands);
        }

        [Test]
        public void GetLiabilityTotalsCommands_LoanLevel_HasExpectedValues()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""getLiabilityTotals"",
                            ""operationId"": ""64"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""type"": ""Loan""
                            }
                        }
                    ]
                }
            }";

            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            var command = loanDataRequest.GetLiabilityTotalsCommands.Single().Message;

            Assert.AreEqual(DataPath.Create("loan"), command.Path);
            Assert.AreEqual(TotalsType.Loan, command.TotalsType);
            Assert.AreEqual(null, command.Id);
        }

        [Test]
        public void GetLiabilityTotalsCommands_InvalidGuidIdSpecified_ThrowsException()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""getLiabilityTotals"",
                            ""operationId"": ""65"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""type"": ""UladApplication"",
                                ""consumerId"": ""MEOW""
                            }
                        }
                    ]
                }
            }";

            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            IReadOnlyList<ServiceMessage<GetTotals<DataObjectKind.Liability>>> commands;
            Assert.Throws<CBaseException>(() => commands = loanDataRequest.GetLiabilityTotalsCommands);
        }

        [Test]
        public void GetLiabilityTotalsCommands_MultipleSpecified_HaveExpectedValues()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""getLiabilityTotals"",
                            ""operationId"": ""66"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""type"": ""Loan""
                            }
                        },
                        {
                            ""name"": ""getLiabilityTotals"",
                            ""operationId"": ""67"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""type"": ""Consumer"",
                                ""id"": ""33333333-3333-3333-3333-333333333333""
                            }
                        }
                    ]
                }
            }";

            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            var getLiabilityTotalsCommands = loanDataRequest.GetLiabilityTotalsCommands;
            Assert.AreEqual(2, getLiabilityTotalsCommands.Count);

            var operationId = getLiabilityTotalsCommands[0].OperationId;
            Assert.AreEqual("66", operationId);
            var command = getLiabilityTotalsCommands[0].Message;
            Assert.AreEqual(DataPath.Create("loan"), command.Path);
            Assert.AreEqual(TotalsType.Loan, command.TotalsType);
            Assert.AreEqual(null, command.Id);

            operationId = getLiabilityTotalsCommands[1].OperationId;
            Assert.AreEqual("67", operationId);
            command = getLiabilityTotalsCommands[1].Message;
            Assert.AreEqual(DataPath.Create("loan"), command.Path);
            Assert.AreEqual(TotalsType.Consumer, command.TotalsType);
            Assert.AreEqual(new Guid("33333333-3333-3333-3333-333333333333"), command.Id);
        }

        [Test]
        public void GetRealPropertyTotalsCommands_ValidCommandFormatUladApp_HasExpectedValues()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""getRealPropertyTotals"",
                            ""operationId"": ""60"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""type"": ""UladApplication"",
                                ""id"": ""33333333-3333-3333-3333-333333333333""
                            }
                        }
                    ]
                }
            }";

            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            var operationId = loanDataRequest.GetRealPropertyTotalsCommands.Single().OperationId;
            Assert.AreEqual("60", operationId);

            var command = loanDataRequest.GetRealPropertyTotalsCommands.Single().Message;
            Assert.AreEqual(DataPath.Create("loan"), command.Path);
            Assert.AreEqual(TotalsType.UladApplication, command.TotalsType);
            Assert.AreEqual(new Guid("33333333-3333-3333-3333-333333333333"), command.Id);
        }

        [Test]
        public void GetRealPropertyTotalsCommands_MultipleSpecified_HaveExpectedValues()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""getRealPropertyTotals"",
                            ""operationId"": ""66"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""type"": ""Loan""
                            }
                        },
                        {
                            ""name"": ""getRealPropertyTotals"",
                            ""operationId"": ""67"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""type"": ""Consumer"",
                                ""id"": ""33333333-3333-3333-3333-333333333333""
                            }
                        }
                    ]
                }
            }";

            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            var getRealPropertyTotalsCommands = loanDataRequest.GetRealPropertyTotalsCommands;
            Assert.AreEqual(2, getRealPropertyTotalsCommands.Count);

            var operationId = getRealPropertyTotalsCommands[0].OperationId;
            Assert.AreEqual("66", operationId);
            var command = getRealPropertyTotalsCommands[0].Message;
            Assert.AreEqual(DataPath.Create("loan"), command.Path);
            Assert.AreEqual(TotalsType.Loan, command.TotalsType);
            Assert.AreEqual(null, command.Id);

            operationId = getRealPropertyTotalsCommands[1].OperationId;
            Assert.AreEqual("67", operationId);
            command = getRealPropertyTotalsCommands[1].Message;
            Assert.AreEqual(DataPath.Create("loan"), command.Path);
            Assert.AreEqual(TotalsType.Consumer, command.TotalsType);
            Assert.AreEqual(new Guid("33333333-3333-3333-3333-333333333333"), command.Id);
        }

        [Test]
        public void GetIncomeSourceTotalsCommands_ValidCommandFormatUladApp_HasExpectedValues()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""getIncomeSourceTotals"",
                            ""operationId"": ""60"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""type"": ""UladApplication"",
                                ""id"": ""33333333-3333-3333-3333-333333333333""
                            }
                        }
                    ]
                }
            }";

            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            var operationId = loanDataRequest.GetIncomeSourceTotalsCommands.Single().OperationId;
            Assert.AreEqual("60", operationId);

            var command = loanDataRequest.GetIncomeSourceTotalsCommands.Single().Message;
            Assert.AreEqual(DataPath.Create("loan"), command.Path);
            Assert.AreEqual(TotalsType.UladApplication, command.TotalsType);
            Assert.AreEqual(new Guid("33333333-3333-3333-3333-333333333333"), command.Id);
        }

        [Test]
        public void GetIncomeSourceTotalsCommands_MultipleSpecified_HaveExpectedValues()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""getIncomeSourceTotals"",
                            ""operationId"": ""66"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""type"": ""Loan""
                            }
                        },
                        {
                            ""name"": ""getIncomeSourceTotals"",
                            ""operationId"": ""67"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""type"": ""Consumer"",
                                ""id"": ""33333333-3333-3333-3333-333333333333""
                            }
                        }
                    ]
                }
            }";

            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            var getIncomeSourceTotalsCommands = loanDataRequest.GetIncomeSourceTotalsCommands;
            Assert.AreEqual(2, getIncomeSourceTotalsCommands.Count);

            var operationId = getIncomeSourceTotalsCommands[0].OperationId;
            Assert.AreEqual("66", operationId);
            var command = getIncomeSourceTotalsCommands[0].Message;
            Assert.AreEqual(DataPath.Create("loan"), command.Path);
            Assert.AreEqual(TotalsType.Loan, command.TotalsType);
            Assert.AreEqual(null, command.Id);

            operationId = getIncomeSourceTotalsCommands[1].OperationId;
            Assert.AreEqual("67", operationId);
            command = getIncomeSourceTotalsCommands[1].Message;
            Assert.AreEqual(DataPath.Create("loan"), command.Path);
            Assert.AreEqual(TotalsType.Consumer, command.TotalsType);
            Assert.AreEqual(new Guid("33333333-3333-3333-3333-333333333333"), command.Id);
        }

        [Test]
        public void SetPrimaryApplication_NoApplicationType_Throws()
        {
            string json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""setPrimaryApplication"",
                            ""operationId"": ""76"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""applicationId"": ""11111111-1111-1111-1111-111111111111""
                            }
                        }
                    ]
                }
            }";

            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            IReadOnlyList<ServiceMessage<BorrowerManagementCommand>> commands;
            Assert.Throws<CBaseException>(() => commands = loanDataRequest.BorrowerManagementCommands);
        }

        [Test]
        public void SetPrimaryApplication_BadApplicationType_Throws()
        {
            string json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""setPrimaryApplication"",
                            ""operationId"": ""76"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""applicationType"": ""MEOW"",
                                ""applicationId"": ""11111111-1111-1111-1111-111111111111""
                            }
                        }
                    ]
                }
            }";

            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            IReadOnlyList<ServiceMessage<BorrowerManagementCommand>> commands;
            Assert.Throws<CBaseException>(() => commands = loanDataRequest.BorrowerManagementCommands);
        }

        [Test]
        public void SetPrimaryApplication_InvalidApplicationId_Throws()
        {
            string json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""setPrimaryApplication"",
                            ""operationId"": ""76"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""applicationType"": ""LegacyApplication"",
                                ""applicationId"": ""11111111-1111-1111-1111-1X1111111111""
                            }
                        }
                    ]
                }
            }";

            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            IReadOnlyList<ServiceMessage<BorrowerManagementCommand>> commands;
            Assert.Throws<CBaseException>(() => commands = loanDataRequest.BorrowerManagementCommands);
        }

        [Test]
        public void SetPrimaryApplication_ValidData_GivesAReasonableObject()
        {
            string json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""setPrimaryApplication"",
                            ""operationId"": ""76"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""applicationType"": ""UladApplication"",
                                ""applicationId"": ""11111111-1111-1111-1111-111111111111""
                            }
                        }
                    ]
                }
            }";

            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            var commands = loanDataRequest.BorrowerManagementCommands;
            Assert.IsNotNull(commands);
            Assert.AreEqual(1, commands.Count);
            Assert.AreEqual("76", commands[0].OperationId);
            Assert.IsInstanceOf<BorrowerManagementCommand>(commands[0].Message);
        }

        [Test]
        public void AddBorrowerToApplication_NoApplicationType_Throws()
        {
            string json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""addBorrowerToApplication"",
                            ""operationId"": ""76"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""applicationId"": ""11111111-1111-1111-1111-111111111111""
                            }
                        }
                    ]
                }
            }";

            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            IReadOnlyList<ServiceMessage<BorrowerManagementCommand>> commands;
            Assert.Throws<CBaseException>(() => commands = loanDataRequest.BorrowerManagementCommands);
        }

        [Test]
        public void AddBorrowerToApplication_BadApplicationType_Throws()
        {
            string json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""addBorrowerToApplication"",
                            ""operationId"": ""76"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""applicationType"": ""MEOW"",
                                ""applicationId"": ""11111111-1111-1111-1111-111111111111""
                            }
                        }
                    ]
                }
            }";

            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            IReadOnlyList<ServiceMessage<BorrowerManagementCommand>> commands;
            Assert.Throws<CBaseException>(() => commands = loanDataRequest.BorrowerManagementCommands);
        }

        [Test]
        public void AddBorrowerToApplication_InvalidApplicationId_Throws()
        {
            string json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""addBorrowerToApplication"",
                            ""operationId"": ""76"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""applicationType"": ""LegacyApplication"",
                                ""applicationId"": ""11111111-1111-1111-1111-1X1111111111""
                            }
                        }
                    ]
                }
            }";

            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            IReadOnlyList<ServiceMessage<BorrowerManagementCommand>> commands;
            Assert.Throws<CBaseException>(() => commands = loanDataRequest.BorrowerManagementCommands);
        }

        [Test]
        public void AddBorrowerToApplication_ValidData_GivesAReasonableObject()
        {
            string json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""addBorrowerToApplication"",
                            ""operationId"": ""76"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""applicationType"": ""UladApplication"",
                                ""applicationId"": ""11111111-1111-1111-1111-111111111111""
                            }
                        }
                    ]
                }
            }";

            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            var commands = loanDataRequest.BorrowerManagementCommands;
            Assert.IsNotNull(commands);
            Assert.AreEqual(1, commands.Count);
            Assert.AreEqual("76", commands[0].OperationId);
            Assert.IsInstanceOf<BorrowerManagementCommand>(commands[0].Message);
        }

        [Test]
        public void AddApplication_NoApplicationType_Throws()
        {
            string json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""addApplication"",
                            ""operationId"": ""76"",
                            ""args"": {
                                ""path"": ""loan""
                            }
                        }
                    ]
                }
            }";

            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            IReadOnlyList<ServiceMessage<BorrowerManagementCommand>> commands;
            Assert.Throws<CBaseException>(() => commands = loanDataRequest.BorrowerManagementCommands);
        }

        [Test]
        public void AddApplication_BadApplicationType_Throws()
        {
            string json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""addApplication"",
                            ""operationId"": ""76"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""applicationType"": ""MEOW""
                            }
                        }
                    ]
                }
            }";

            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            IReadOnlyList<ServiceMessage<BorrowerManagementCommand>> commands;
            Assert.Throws<CBaseException>(() => commands = loanDataRequest.BorrowerManagementCommands);
        }

        [Test]
        public void AddApplication_ValidData_GivesAReasonableObject()
        {
            string json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""addApplication"",
                            ""operationId"": ""76"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""applicationType"": ""UladApplication"",
                                ""applicationId"": ""11111111-1111-1111-1111-111111111111""
                            }
                        }
                    ]
                }
            }";

            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            var commands = loanDataRequest.BorrowerManagementCommands;
            Assert.IsNotNull(commands);
            Assert.AreEqual(1, commands.Count);
            Assert.AreEqual("76", commands[0].OperationId);
            Assert.IsInstanceOf<BorrowerManagementCommand>(commands[0].Message);
        }

        [Test]
        public void DeleteApplication_NoApplicationType_Throws()
        {
            string json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""deleteApplication"",
                            ""operationId"": ""76"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""applicationId"": ""11111111-1111-1111-1111-111111111111""
                            }
                        }
                    ]
                }
            }";

            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            IReadOnlyList<ServiceMessage<BorrowerManagementCommand>> commands;
            Assert.Throws<CBaseException>(() => commands = loanDataRequest.BorrowerManagementCommands);
        }

        [Test]
        public void DeleteApplication_BadApplicationType_Throws()
        {
            string json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""deleteApplication"",
                            ""operationId"": ""76"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""applicationType"": ""MEOW"",
                                ""applicationId"": ""11111111-1111-1111-1111-111111111111""
                            }
                        }
                    ]
                }
            }";

            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            IReadOnlyList<ServiceMessage<BorrowerManagementCommand>> commands;
            Assert.Throws<CBaseException>(() => commands = loanDataRequest.BorrowerManagementCommands);
        }

        [Test]
        public void DeleteApplication_InvalidApplicationId_Throws()
        {
            string json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""deleteApplication"",
                            ""operationId"": ""76"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""applicationType"": ""LegacyApplication"",
                                ""applicationId"": ""11111111-1111-1111-1111-1X1111111111""
                            }
                        }
                    ]
                }
            }";

            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            IReadOnlyList<ServiceMessage<BorrowerManagementCommand>> commands;
            Assert.Throws<CBaseException>(() => commands = loanDataRequest.BorrowerManagementCommands);
        }

        [Test]
        public void DeleteApplication_ValidData_GivesAReasonableObject()
        {
            string json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""deleteApplication"",
                            ""operationId"": ""76"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""applicationType"": ""UladApplication"",
                                ""applicationId"": ""11111111-1111-1111-1111-111111111111""
                            }
                        }
                    ]
                }
            }";

            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            var commands = loanDataRequest.BorrowerManagementCommands;
            Assert.IsNotNull(commands);
            Assert.AreEqual(1, commands.Count);
            Assert.AreEqual("76", commands[0].OperationId);
            Assert.IsInstanceOf<BorrowerManagementCommand>(commands[0].Message);
        }

        [Test]
        public void DeleteBorrower_ValidData_GivesAReasonableObject()
        {
            string json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""deleteBorrower"",
                            ""operationId"": ""76"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""consumerId"": ""11111111-1111-1111-1111-111111111111""
                            }
                        }
                    ]
                }
            }";

            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            var commands = loanDataRequest.BorrowerManagementCommands;
            Assert.IsNotNull(commands);
            Assert.AreEqual(1, commands.Count);
            Assert.AreEqual("76", commands[0].OperationId);
            Assert.IsInstanceOf<BorrowerManagementCommand>(commands[0].Message);

            json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""deleteBorrower"",
                            ""operationId"": ""76"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""applicationType"": ""LegacyApplication"",
                                ""applicationId"": ""11111111-1111-1111-1111-111111111111""
                            }
                        }
                    ]
                }
            }";

            loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            commands = loanDataRequest.BorrowerManagementCommands;
            Assert.IsNotNull(commands);
            Assert.AreEqual(1, commands.Count);
            Assert.AreEqual("76", commands[0].OperationId);
            Assert.IsInstanceOf<BorrowerManagementCommand>(commands[0].Message);
        }

        [Test]
        public void SetPrimaryBorrower_ValidData_GivesAReasonableObject()
        {
            string json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""setPrimaryBorrower"",
                            ""operationId"": ""76"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""consumerId"": ""11111111-1111-1111-1111-111111111111""
                            }
                        }
                    ]
                }
            }";

            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            var commands = loanDataRequest.BorrowerManagementCommands;
            Assert.IsNotNull(commands);
            Assert.AreEqual(1, commands.Count);
            Assert.AreEqual("76", commands[0].OperationId);
            Assert.IsInstanceOf<BorrowerManagementCommand>(commands[0].Message);
        }

        [Test]
        public void SwapBorrowers_ValidData_GivesAReasonableObject()
        {
            string json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""swapBorrowers"",
                            ""operationId"": ""76"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""applicationType"": ""LegacyApplication"",
                                ""applicationId"": ""11111111-1111-1111-1111-111111111111""
                            }
                        }
                    ]
                }
            }";

            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            var commands = loanDataRequest.BorrowerManagementCommands;
            Assert.IsNotNull(commands);
            Assert.AreEqual(1, commands.Count);
            Assert.AreEqual("76", commands[0].OperationId);
            Assert.IsInstanceOf<BorrowerManagementCommand>(commands[0].Message);
        }

        [Test]
        public void SwapBorrowers_OnUladApplication_Throws()
        {
            string json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""swapBorrowers"",
                            ""operationId"": ""76"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""applicationType"": ""UladApplication"",
                                ""applicationId"": ""11111111-1111-1111-1111-111111111111""
                            }
                        }
                    ]
                }
            }";

            var loanDataRequest = new LqbDataRequest(JObject.Parse(json), converter);

            IReadOnlyList<ServiceMessage<BorrowerManagementCommand>> commands;
            Assert.Throws<CBaseException>(() => commands = loanDataRequest.BorrowerManagementCommands);
        }
    }
}
