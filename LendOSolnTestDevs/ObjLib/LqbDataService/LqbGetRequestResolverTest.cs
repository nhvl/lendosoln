﻿namespace LendingQB.Test.Developers.ObjLib.LqbDataService
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using global::DataAccess;
    using global::DataAccess.PathDispatch;
    using global::LendingQB.Core;
    using global::LendingQB.Core.Data;
    using LendersOffice.ObjLib.LqbDataService;
    using LqbGrammar.DataTypes;
    using LqbGrammar.DataTypes.PathDispatch;
    using NSubstitute;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public class LqbGetRequestResolverTest
    {
        [Test]
        public void ResolveRequest_IPathResolvableLeafNode_ThrowsException()
        {
            var root = new RequestNode(
                pathComponent: null,
                children: new List<RequestNode>()
                {
                    new RequestNode(pathComponent: new DataPathBasicElement("Test"), children: null)
                });
            var pathResolvableSeedValue = Substitute.For<IPathResolvable>();
            pathResolvableSeedValue.GetElement(Arg.Any<IDataPathElement>()).Returns(new TestPathResolvableLeafNodeNoSchema());

            Assert.Throws<CBaseException>(() => LqbGetRequestResolver.ResolveGetRequest(root, pathResolvableSeedValue, null));
        }

        private class TestPathResolvableLeafNodeNoSchema : IPathResolvable
        {
            public object GetElement(IDataPathElement element)
            {
                throw new NotImplementedException();
            }
        }

        [Test]
        public void ResolveRequest_NodeWithChildrenDoesNotImplementIPathResolvable_ThrowsException()
        {
            var root = new RequestNode(
                pathComponent: null,
                children: new List<RequestNode>()
                {
                    new RequestNode(
                        pathComponent: new DataPathBasicElement("Test"),
                        children: new List<RequestNode>()
                        {
                            new RequestNode(new DataPathBasicElement("Test"), children: null)
                        })

                });
            var pathResolvableSeedValue = Substitute.For<IPathResolvable>();
            pathResolvableSeedValue.GetElement(Arg.Any<IDataPathElement>()).Returns(new TestNoIPathResolvable());


            Assert.Throws<CBaseException>(() => LqbGetRequestResolver.ResolveGetRequest(root, pathResolvableSeedValue, null));
        }

        private class TestNoIPathResolvable
        {
        }

        [Test]
        public void ResolveRequest_MoreThanOneAnyNodeChildForCollection_ThrowsException()
        {
            var collection = new PathResolvableCollection();
            var parent = new PathResolvableCollectionParent(collection);
            var root = new RequestNode(
                pathComponent: null,
                children: new List<RequestNode>()
                {
                    new RequestNode(
                        pathComponent: new DataPathCollectionElement("Collection"),
                        children: new List<RequestNode>()
                        {
                            new RequestNode(
                                pathComponent: new DataPathSelectionElement(new SelectIdExpression(DataPath.WholeCollectionIdentifier)),
                                children: null),
                            new RequestNode(
                                pathComponent: new DataPathSelectionElement(new SelectIdExpression(DataPath.WholeCollectionIdentifier)),
                                children: null)
                        })
                });
            var pathResolvableSeedValue = Substitute.For<IPathResolvable>();
            pathResolvableSeedValue.GetElement(Arg.Any<IDataPathElement>()).Returns(Substitute.For<IPathResolvableCollection>());

            Assert.Throws<CBaseException>(() => LqbGetRequestResolver.ResolveGetRequest(root, parent, null));
        }

        private class PathResolvableCollectionParent : IPathResolvable
        {
            public PathResolvableCollectionParent(PathResolvableCollection collection)
            {
                this.Collection = collection;
            }

            public object GetElement(IDataPathElement element)
            {
                return this.Collection;
            }

            private PathResolvableCollection Collection { get; }
        }

        private class PathResolvableCollection : IPathResolvableCollection
        {
            public IEnumerable<DataPathSelectionElement> GetAllKeys()
            {
                throw new NotImplementedException();
            }

            public object GetElement(IDataPathElement element)
            {
                throw new NotImplementedException();
            }
        }

        [Test]
        public void ResolveRequest_CollectionRecordLeafNode_ThrowsException()
        {
            var root = new RequestNode(
                pathComponent: null,
                children: new List<RequestNode>()
                {
                    new RequestNode(pathComponent: new DataPathBasicElement("Test"), children: null)
                });
            var pathResolvableSeedValue = Substitute.For<IPathResolvable>();
            pathResolvableSeedValue.GetElement(Arg.Any<IDataPathElement>()).Returns(Substitute.For<IPathResolvable>());

            Assert.Throws<CBaseException>(() => LqbGetRequestResolver.ResolveGetRequest(root, pathResolvableSeedValue, null));
        }

        [Test]
        public void ResolveRequest_CollectionHasAnyChildWithSubsetOfFields_ExpandsAnyToAllAvailableIdsAndSelectedFields()
        {
            var idFactory = new GuidDataObjectIdentifierFactory<DataObjectKind.Asset>();
            var assetCollection = LqbCollection<DataObjectKind.Asset, Guid, Asset>.Create(
                Name.Create("Assets").Value,
                idFactory,
                null);
            var id1 = idFactory.NewId();
            assetCollection.Add(id1, new Asset());
            var id2 = idFactory.NewId();
            assetCollection.Add(id2, new Asset());
            var root = new RequestNode(
                pathComponent: null,
                children: new List<RequestNode>()
                {
                    new RequestNode(
                        pathComponent: new DataPathBasicElement("Assets"),
                        children: new List<RequestNode>()
                        {
                            new RequestNode(
                                pathComponent: new DataPathSelectionElement(new SelectIdExpression(DataPath.WholeCollectionIdentifier)),
                                children: new List<RequestNode>()
                                {
                                    new RequestNode(pathComponent: new DataPathBasicElement(nameof(Asset.AccountName)), children: null)
                                })
                        })
                });
            var pathResolvableSeedValue = Substitute.For<IPathResolvable>();
            pathResolvableSeedValue.GetElement(Arg.Any<IDataPathElement>()).Returns(assetCollection);

            var formatter = new FormatEntityAsString(FormatTarget.Webform, FormatDirection.ToRep);

            var responseRoot = LqbGetRequestResolver.ResolveGetRequest(root, pathResolvableSeedValue, formatter);

            var assetsNode = responseRoot.Children[0];
            Assert.AreEqual(2, assetsNode.Children.Count);
            Assert.AreEqual(false, assetsNode.Children.Any(n => n.PathComponent.Name == DataPath.WholeCollectionIdentifier));

            foreach (var assetId in assetsNode.Children)
            {
                Assert.AreEqual(1, assetId.Children.Count);
            }
        }

        [Test]
        public void ResolveRequest_CollectionLeafNode_ThrowsException()
        {
            var idFactory = new GuidDataObjectIdentifierFactory<DataObjectKind.Asset>();
            var assetCollection = LqbCollection<DataObjectKind.Asset, Guid, Asset>.Create(
                Name.Create("Assets").Value,
                idFactory,
                null);
            var root = new RequestNode(
                pathComponent: null,
                children: new List<RequestNode>()
                {
                    new RequestNode(
                        pathComponent: new DataPathBasicElement("Assets"),
                        children: null)
                });
            var pathResolvableSeedValue = Substitute.For<IPathResolvable>();
            pathResolvableSeedValue.GetElement(Arg.Any<IDataPathElement>()).Returns(assetCollection);

            Assert.Throws<CBaseException>(() => LqbGetRequestResolver.ResolveGetRequest(root, pathResolvableSeedValue, null));
        }

        [Test]
        public void ResolveRequest_GetDateWithFormatterSpecified_ReturnsExpectedDateFormat()
        {
            var idFactory = new GuidDataObjectIdentifierFactory<DataObjectKind.Asset>();
            var assetCollection = LqbCollection<DataObjectKind.Asset, Guid, Asset>.Create(
                Name.Create("Assets").Value,
                idFactory,
                null);
            var id1 = idFactory.NewId();
            assetCollection.Add(id1, new Asset()
            {
                AssetType = E_AssetT.Checking,
                PrepDate = UnzonedDate.Create(2018, 1, 2)
            });
            var formatter = new FormatEntityAsString(FormatTarget.GinnieNet, FormatDirection.ToRep);
            var root = new RequestNode(
                pathComponent: null,
                children: new List<RequestNode>()
                {
                    new RequestNode(
                        pathComponent: new DataPathBasicElement("Assets"),
                        children: new List<RequestNode>()
                        {
                            new RequestNode(
                                pathComponent: new DataPathSelectionElement(new SelectIdExpression(id1.ToString())),
                                children: new List<RequestNode>()
                                {
                                    new RequestNode(pathComponent: new DataPathBasicElement(nameof(Asset.PrepDate)), children: null)
                                })
                        })
                });
            var pathResolvableSeedValue = Substitute.For<IPathResolvable>();
            pathResolvableSeedValue.GetElement(Arg.Any<IDataPathElement>()).Returns(assetCollection);

            var responseRoot = LqbGetRequestResolver.ResolveGetRequest(root, pathResolvableSeedValue, formatter);

            var value = responseRoot
                .Children[0] // Assets
                    .Children[0] // Asset
                        .Children[0].Value; // PrepDate
            Assert.AreEqual("20180102", value);
        }
    }
}
