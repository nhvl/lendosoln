﻿namespace LendingQB.Test.Developers.ObjLib.LqbDataService
{
    using System;
    using System.Linq;
    using LendersOffice.ObjLib.LqbDataService;
    using NUnit.Framework;
    using Newtonsoft.Json.Linq;
    using LqbGrammar.DataTypes;
    using LendersOffice.Security;
    using Utils;
    using global::DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LqbGrammar.DataTypes.PathDispatch;

    [TestFixture]
    [Category(CategoryConstants.IntegrationTest)]
    public class LqbDataServiceTest
    {
        private AbstractUserPrincipal principal;
        private DataObjectIdentifier<DataObjectKind.Loan, Guid> loanId;
        private DataObjectIdentifier<DataObjectKind.ClientCompany, Guid> brokerId;
        private DataObjectIdentifier<DataObjectKind.Consumer, Guid> consumer1Id;
        private DataObjectIdentifier<DataObjectKind.Consumer, Guid> consumer2Id;
        private DataObjectIdentifier<DataObjectKind.Consumer, Guid> consumer3Id;
        private DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> legacyApp1Id;
        private DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> legacyApp2Id;
        private DataObjectIdentifier<DataObjectKind.UladApplication, Guid> uladApp1Id;
        private DataObjectIdentifier<DataObjectKind.UladApplication, Guid> uladApp2Id;
        private DataObjectIdentifier<DataObjectKind.ConsumerAssetAssociation, Guid> primaryAsset1OwnershipId;
        private DataObjectIdentifier<DataObjectKind.ConsumerAssetAssociation, Guid> secondaryAsset1OwnershipId;
        private DataObjectIdentifier<DataObjectKind.CounselingEvent, Guid> counselingEventId;
        private DataObjectIdentifier<DataObjectKind.EmploymentRecord, Guid> employmentRecordId;
        private DataObjectIdentifier<DataObjectKind.IncomeRecord, Guid> incomeRecordId;
        private Guid asset1Id;
        private Guid asset2Id;
        private Guid asset3Id;
        private int initialAssetCount;

        [TestFixtureSetUp]
        public void FixtureSetUp()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                this.principal = LoginTools.LoginAs(TestAccountType.LoTest001);
                this.brokerId = DataObjectIdentifier<DataObjectKind.ClientCompany, Guid>.Create(principal.BrokerId);
                var creator = CLoanFileCreator.GetCreator(
                    this.principal,
                    LendersOffice.Audit.E_LoanCreationSource.UserCreateFromBlank);
                this.loanId = DataObjectIdentifier<DataObjectKind.Loan, Guid>.Create(creator.CreateBlankUladLoanFile());

                var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(this.loanId.Value, typeof(LqbDataServiceTest));
                loan.AddNewApp();
                loan.InitSave(ConstAppDavid.SkipVersionCheck);

                this.legacyApp1Id = loan.LegacyApplications.Keys.First();

                this.consumer1Id = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(loan.GetAppData(0).aBConsumerId);
                loan.Consumers[consumer1Id].FirstName = PersonName.Create("Meow");
                loan.AddCoborrowerToLegacyApplication(loan.GetAppData(0).aAppId.ToIdentifier<DataObjectKind.LegacyApplication>());

                this.consumer2Id = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(loan.GetAppData(0).aCConsumerId);

                this.legacyApp2Id = DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>.Create(loan.GetAppData(1).aAppId);
                this.consumer3Id = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(loan.GetAppData(1).aBConsumerId);

                loan.sBorrowerApplicationCollectionT = E_sLqbCollectionT.UseLqbCollections;

                this.uladApp1Id = loan.UladApplications.Keys.ElementAt(0);
                this.uladApp2Id = loan.UladApplications.Keys.ElementAt(1);

                loan.sLT = E_sLT.FHA;
                loan.sLAmtCalc = 250000m;
                loan.sLAmtLckd = true;

                var asset1 = loan.GetAppData(0).aAssetCollection.AddRegularRecord();
                asset1.AccNum = "AccountNum1";
                asset1.OwnerT = E_AssetOwnerT.Joint;
                asset1.AssetT = E_AssetRegularT.BridgeLoanNotDeposited;
                asset1.Val = 1;
                this.asset1Id = asset1.RecordId;

                var asset2 = loan.GetAppData(0).aAssetCollection.AddRegularRecord();
                asset2.AccNum = "AccountNum2";
                asset2.Val = 2;
                this.asset2Id = asset2.RecordId;

                var asset3 = loan.GetAppData(0).aAssetCollection.AddRegularRecord();
                asset3.AccNum = "AccountNum3";
                asset3.Val = 3;
                this.asset3Id = asset3.RecordId;

                this.primaryAsset1OwnershipId = loan.ConsumerAssets.Single(a => a.Value.AssetId.Value == this.asset1Id && a.Value.IsPrimary).Key;
                this.secondaryAsset1OwnershipId = loan.ConsumerAssets.Single(a => a.Value.AssetId.Value == this.asset1Id && !a.Value.IsPrimary).Key;

                this.initialAssetCount = loan.Assets.Count;

                var liability1 = loan.GetAppData(0).aLiaCollection.AddRegularRecord();
                liability1.Bal = 4;

                var liability2 = loan.GetAppData(0).aLiaCollection.AddRegularRecord();
                liability2.Pmt = 5;

                var liability3 = loan.GetAppData(0).aLiaCollection.AddRegularRecord();
                liability3.WillBePdOff = true;
                liability3.PayoffAmtLckd = true;
                liability3.PayoffAmt = 6;
                liability3.PayoffTimingLckd = true;
                liability3.PayoffTiming = E_Timing.At_Closing;

                var realProperty1 = loan.GetAppData(0).aReCollection.AddRegularRecord();
                realProperty1.StatT = E_ReoStatusT.Rental;
                realProperty1.Val = 1;

                var incomeSource1 = new Core.Data.IncomeSource { IncomeType = IncomeType.BaseIncome, MonthlyAmountData = 1860M };
                var incomeSource2 = new Core.Data.IncomeSource { IncomeType = IncomeType.Bonuses, MonthlyAmountData = 600M };
                loan.AddIncomeSource(consumer1Id, incomeSource1);
                loan.AddIncomeSource(consumer1Id, incomeSource2);

                this.counselingEventId = loan.AddCounselingEvent(this.consumer1Id, new Core.Data.CounselingEvent());

                this.employmentRecordId = loan.AddEmploymentRecord(this.consumer1Id, new Core.Data.EmploymentRecord(loan.CreateEmploymentRecordDefaultsProvider()) { EmployerName = EntityName.Create("Test") });
                this.incomeRecordId = loan.EmploymentRecords[employmentRecordId].IncomeRecords.Add(new Core.Data.IncomeRecord() { IncomeType = IncomeType.ContractBasis, MonthlyAmount = Money.Create(50) });

                loan.Save();
            }
        }

        [TestFixtureTearDown]
        public void FixtureTearDown()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                Tools.DeclareLoanFileInvalid(
                    this.principal,
                    this.loanId.Value,
                    sendNotifications: false,
                    generateLinkLoanMsgEvent: false);
            }
        }

        [SetUp]
        public void SetUp()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);
                var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(this.loanId.Value, typeof(LqbDataServiceTest));
                loan.InitSave(ConstAppDavid.SkipVersionCheck);

                // This is removed in one of the tests, restore here.
                bool update = !loan.GetAppData(0).aAssetCollection.HasRecordOf(this.asset1Id);
                if (update)
                {
                    if (loan.Assets.ContainsKey(DataObjectIdentifier<DataObjectKind.Asset, Guid>.Create(this.asset1Id)))
                    {
                        var remove = new Core.Commands.RemoveEntity(DataPath.Create("loan"), "Assets", this.asset1Id);
                        loan.HandleRemove(remove);
                    }

                    var asset1 = loan.GetAppData(0).aAssetCollection.EnsureRegularRecordOf(this.asset1Id);
                    ((IAssetRegular)asset1).AccNum = "AccountNum1";
                    ((IAssetRegular)asset1).OwnerT = E_AssetOwnerT.Joint;

                    this.primaryAsset1OwnershipId = loan.ConsumerAssets.Single(a => a.Value.AssetId.Value == this.asset1Id && a.Value.IsPrimary).Key;
                    this.secondaryAsset1OwnershipId = loan.ConsumerAssets.Single(a => a.Value.AssetId.Value == this.asset1Id && !a.Value.IsPrimary).Key;

                    loan.Save();
                }
            }
        }

        [Test]
        public void ProcessRequest_GetSimpleLoanLevelFields_ReturnsExpectedResult()
        {
            var json = @"
{
    ""data"": {
        ""version"": 1739,
        ""requests"": [
            {
                ""name"": ""get"",
                ""operationId"": ""82"",
                ""args"": {
                    ""loan"": {
                        ""sLAmtCalc"": null,
                        ""sLT"": null,
                    }
                }
            }
        ]
    }
}";
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.ConfigurationQuery, FoolHelper.DriverType.SqlQuery, FoolHelper.DriverType.RegularExpression);

                var responseJson = LqbDataService.ProcessRequest(this.loanId, LqbDataServiceOperation.Load, json);
                var response = JObject.Parse(responseJson);

                Assert.AreEqual("1", (string)response.SelectToken("data.responses[?(@.operationId == '82')].returns.loan.sLT"));
                Assert.AreEqual("$250,000.00", (string)response.SelectToken("data.responses[?(@.operationId == '82')].returns.loan.sLAmtCalc"));
            }
        }

        [Test]
        public void ProcessRequest_GetLoanLevelBoolField_ReturnsFalseInsteadOfNo()
        {
            var json = @"
{
    ""data"": {
        ""version"": 1739,
        ""requests"": [
            {
                ""name"": ""get"",
                ""operationId"": ""82"",
                ""args"": {
                    ""loan"": {
                        ""sIsManualUw"": null
                    }
                }
            }
        ]
    }
}";
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.ConfigurationQuery, FoolHelper.DriverType.SqlQuery, FoolHelper.DriverType.RegularExpression);

                var responseJson = LqbDataService.ProcessRequest(this.loanId, LqbDataServiceOperation.Load, json);
                var response = JObject.Parse(responseJson);

                Assert.AreEqual("False", (string)response.SelectToken("data.responses[?(@.operationId == '82')].returns.loan.sIsManualUw"));
            }
        }

        [Test]
        public void ProcessRequest_GetSpecificEnumFieldsFromSingleConsumerRecord_ReturnsNumeric()
        {
            var json = @"
{
    ""data"": {
        ""version"": 1739,
        ""requests"": [
            {
                ""name"": ""get"",
                ""operationId"": ""42"",
                ""args"": {
                    ""loan"": {
                        ""Assets"": {
                            """ + this.asset1Id.ToString() + @""": {
                                ""AssetType"" : null
                            }
                        }
                    }
                }
            }
        ]
    }
}";

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.ConfigurationQuery, FoolHelper.DriverType.SqlQuery, FoolHelper.DriverType.RegularExpression);

                var responseJson = LqbDataService.ProcessRequest(this.loanId, LqbDataServiceOperation.Load, json);
                var response = JObject.Parse(responseJson);

                Assert.AreEqual(18, (int)E_AssetRegularT.BridgeLoanNotDeposited);
                Assert.AreEqual("18", (string)response.SelectToken("data.responses.[?(@.name == 'get')].returns.loan.Assets['" + this.asset1Id.ToString() + "'].AssetType"));
            }
        }

        [Test]
        public void ProcessRequest_GetSpecificFieldsFromSingleCollectionRecord_ReturnsExpectedResult()
        {
            var json = @"
{
    ""data"": {
        ""version"": 1739,
        ""requests"": [
            {
                ""name"": ""get"",
                ""operationId"": ""83"",
                ""args"": {
                    ""loan"": {
                        ""Assets"": {
                            """ + this.asset2Id.ToString() + @""": {
                                ""AccountNum"": null
                            }
                        }
                    }
                }
            }
        ]
    }
}";

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.ConfigurationQuery, FoolHelper.DriverType.SqlQuery, FoolHelper.DriverType.RegularExpression);

                var responseJson = LqbDataService.ProcessRequest(this.loanId, LqbDataServiceOperation.Load, json);
                var response = JObject.Parse(responseJson);

                Assert.AreEqual("AccountNum2", (string)response.SelectToken("data.responses[?(@.name == 'get')].returns.loan.Assets['" + this.asset2Id.ToString() + "'].AccountNum"));
            }
        }

        [Test]
        public void ProcessRequest_GetSpecificFieldsFromEntireAssetCollection_ReturnsExpectedResult()
        {
            var json = @"
{
    ""data"": {
        ""version"": 1739,
        ""requests"": [
            {
                ""name"": ""get"",
                ""operationId"": ""84"",
                ""args"": {
                    ""loan"": {
                        ""Assets"": {
                            ""Any"": {
                                ""AccountNum"": null
                            }
                        }
                    }
                }
            }
        ]
    }
}";

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.ConfigurationQuery, FoolHelper.DriverType.SqlQuery, FoolHelper.DriverType.RegularExpression);

                var responseJson = LqbDataService.ProcessRequest(this.loanId, LqbDataServiceOperation.Load, json);
                var response = JObject.Parse(responseJson);

                var assets = response.SelectToken("data.responses[?(@.name == 'get')].returns.loan.Assets");
                Assert.AreEqual(3, assets.Count());
                Assert.AreEqual("AccountNum1", (string)assets.SelectToken("['" + this.asset1Id.ToString()  + "'].AccountNum"));
                Assert.AreEqual("AccountNum2", (string)assets.SelectToken("['" + this.asset2Id.ToString()  + "'].AccountNum"));
                Assert.AreEqual("AccountNum3", (string)assets.SelectToken("['" + this.asset3Id.ToString()  + "'].AccountNum"));
            }
        }

        [Test]
        public void ProcessRequest_GetCustomMappingFieldsFromLiabilityCollection_ReturnsExpectedResult()
        {
            string json = @"
{
    ""data"": {
        ""version"": 1739,
        ""requests"": [
            {
                ""name"": ""get"",
                ""operationId"": ""84"",
                ""args"": {
                    ""loan"": {
                        ""Liabilities"": {
                            ""Any"": {
                                ""PmlAuditTrail"": {
                                    ""Any"": {
                                        ""UserName"": null,
                                        ""EventDate"": null
                                    }
                                }
                            }
                        }
                    }
                }
            }
        ]
    }
}";
            using (var loanRef = LoanForIntegrationTest.Create(TestAccountType.LoTest001, useUladDataLayer: true))
            {
                var loanBefore = loanRef.CreateNewPageDataWithBypass();
                loanBefore.InitSave();
                var liability = new Core.Data.Liability(new Core.Data.LiabilityDefaultsProvider(CPageBaseExtractor.Extract(loanBefore)));
                liability.PmlAuditTrail = new System.Collections.Generic.List<LiabilityPmlAuditTrailEvent>
                {
                    new LiabilityPmlAuditTrailEvent("user name", "login id", "action", "field", "value", "event date"),
                    new LiabilityPmlAuditTrailEvent("user name2", "login id2", "action2", "field2", "value2", "event date2"),
                };
                var liabilityId = loanBefore.AddLiability(loanBefore.UladApplicationConsumers.Values.First().ConsumerId, liability);
                loanBefore.Save();

                string responseJson = LqbDataService.ProcessRequest(loanRef.Identifier, LqbDataServiceOperation.Load, json);
                var response = JObject.Parse(responseJson);

                var items = response.SelectToken("data.responses[?(@.name == 'get')].returns.loan.Liabilities['" + liabilityId + "'].PmlAuditTrail");
                Assert.AreEqual(2, items.Count());
                Assert.AreEqual("user name", (string)items.SelectToken("['0'].UserName"));
                Assert.AreEqual("event date", (string)items.SelectToken("['0'].EventDate"));
                Assert.AreEqual("user name2", (string)items.SelectToken("['1'].UserName"));
                Assert.AreEqual("event date2", (string)items.SelectToken("['1'].EventDate"));
            }
        }

        [Test]
        public void ProcessRequest_GetEmploymentFieldsAndIncomeRecords_ReturnsExpectedResult()
        {
            var json = @"
{
    ""data"": {
        ""version"": 1739,
        ""requests"": [
            {
                ""name"": ""get"",
                ""operationId"": ""82"",
                ""args"": {
                    ""loan"": {
                        ""EmploymentRecords"": {
                            ""any"": {
                                ""EmployerName"": null,
                                ""IncomeRecords"": {
                                    ""any"": {
                                        ""IncomeType"": null,
                                        ""MonthlyAmount"": null
                                    }
                                }
                            }
                        }
                    }
                }
            }
        ]
    }
}";
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.ConfigurationQuery, FoolHelper.DriverType.SqlQuery, FoolHelper.DriverType.RegularExpression);

                var responseJson = LqbDataService.ProcessRequest(this.loanId, LqbDataServiceOperation.Load, json);
                var response = JObject.Parse(responseJson);

                var items = response.SelectToken("data.responses[?(@.name == 'get')].returns.loan.EmploymentRecords['" + this.employmentRecordId + "']");
                Assert.AreEqual(2, items.Count());
                Assert.AreEqual("Test", (string)items.SelectToken("EmployerName"));
                Assert.AreEqual("43", (string)items.SelectToken("IncomeRecords['" + this.incomeRecordId.ToString() + "'].IncomeType"));
                Assert.AreEqual("$50.00", (string)items.SelectToken("IncomeRecords['" + this.incomeRecordId.ToString() + "'].MonthlyAmount"));
            }
        }

        [Test]
        public void ProcessRequest_SetCustomMappingFieldsFromLiabilityCollection_Throws()
        {
            string json = @"
{
    ""data"": {
        ""version"": 1739,
        ""requests"": [
            {
                ""name"": ""set"",
                ""operationId"": ""84"",
                ""args"": {
                    ""loan"": {
                        ""Liabilities"": {
                            ""#LIABILITY_ID#"": {
                                ""PmlAuditTrail"": {
                                    ""0"": {
                                        ""UserName"": ""MY HACK USER NAME""
                                    }
                                }
                            }
                        }
                    }
                }
            }
        ]
    }
}";
            using (var loanRef = LoanForIntegrationTest.Create(TestAccountType.LoTest001, useUladDataLayer: true))
            {
                var loanBefore = loanRef.CreateNewPageDataWithBypass();
                loanBefore.InitSave();
                var liability = new Core.Data.Liability(new Core.Data.LiabilityDefaultsProvider(CPageBaseExtractor.Extract(loanBefore)));
                liability.PmlAuditTrail = new System.Collections.Generic.List<LiabilityPmlAuditTrailEvent>
                {
                    new LiabilityPmlAuditTrailEvent("user name", "login id", "action", "field", "value", "event date"),
                    new LiabilityPmlAuditTrailEvent("user name2", "login id2", "action2", "field2", "value2", "event date2"),
                };
                var liabilityId = loanBefore.AddLiability(loanBefore.UladApplicationConsumers.Values.First().ConsumerId, liability);
                loanBefore.Save();
                json = json.Replace("#LIABILITY_ID#", liabilityId.ToString());

                // We want to be careful that this test passes - audits should not be directly settable via any api
                Assert.Throws<NotSupportedException>(() => LqbDataService.ProcessRequest(loanRef.Identifier, LqbDataServiceOperation.Save, json));
            }
        }

        [Test]
        public void ProcessRequest_GetSpecificFieldsFromSingleConsumerRecord_ReturnsExpectedResult()
        {
            var json = @"
{
    ""data"": {
        ""version"": 1739,
        ""requests"": [
            {
                ""name"": ""get"",
                ""operationId"": ""85"",
                ""args"": {
                    ""loan"": {
                        ""Consumers"": {
                            """ + this.consumer1Id.ToString() + @""": {
                                ""FirstName"": null
                            }
                        }
                    }
                }
            }
        ]
    }
}";

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.ConfigurationQuery, FoolHelper.DriverType.SqlQuery, FoolHelper.DriverType.RegularExpression);

                var responseJson = LqbDataService.ProcessRequest(this.loanId, LqbDataServiceOperation.Load, json);
                var response = JObject.Parse(responseJson);

                Assert.AreEqual("Meow", (string)response.SelectToken("data.responses[?(@.name == 'get')].returns.loan.Consumers['" + this.consumer1Id.ToString()  + "'].FirstName"));
            }
        }

        [Test]
        public void ProcessRequest_SetAssetFields_UpdatesEntity()
        {
            var json = @"
{
    ""data"": {
        ""version"": 1739,
        ""requests"": [
            {
                ""name"": ""set"",
                ""operationId"": ""86"",
                ""args"": {
                    ""loan"": {
                        ""Assets"": {
                            """ + this.asset1Id.ToString() + @""": {
                                ""AccountName"": ""Test Name"",
                                ""Desc"": ""Test Description"",
                                ""Value"": ""$1,234.56""
                            }
                        }
                    }
                }
            }
        ]
    }
}";

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.ConfigurationQuery, FoolHelper.DriverType.SqlQuery, FoolHelper.DriverType.RegularExpression);

                var responseJson = LqbDataService.ProcessRequest(this.loanId, LqbDataServiceOperation.Save, json);

                var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                    this.loanId.Value,
                    typeof(LqbDataServiceTest));
                loan.InitLoad();
                var asset = loan.Assets[DataObjectIdentifier<DataObjectKind.Asset, Guid>.Create(this.asset1Id)];

                Assert.AreEqual(DescriptionField.Create("Test Name"), asset.AccountName);
                Assert.AreEqual(DescriptionField.Create("Test Description"), asset.Desc);
                Assert.AreEqual(Money.Create(1234.56m), asset.Value);
            }
        }

        [Test]
        public void ProcessRequest_SetConsumerFields_UpdatesEntity()
        {
            var json = @"
{
    ""data"": {
        ""version"": 1739,
        ""requests"": [
            {
                ""name"": ""set"",
                ""operationId"": ""87"",
                ""args"": {
                    ""loan"": {
                        ""Consumers"": {
                            """ + this.consumer1Id.ToString() + @""": {
                                ""FirstName"": ""Cool Cat"",
                                ""Age"": ""35"",
                                ""OtherIncFrom1008"": ""$1,234.56""
                            }
                        }
                    }
                }
            }
        ]
    }
}";

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.ConfigurationQuery, FoolHelper.DriverType.SqlQuery, FoolHelper.DriverType.RegularExpression);

                var responseJson = LqbDataService.ProcessRequest(this.loanId, LqbDataServiceOperation.Save, json);

                var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                    this.loanId.Value,
                    typeof(LqbDataServiceTest));
                loan.InitLoad();
                var consumer = loan.Consumers[consumer1Id];

                Assert.AreEqual(PersonName.Create("Cool Cat"), consumer.FirstName);
                Assert.AreEqual(Count<UnitType.Year>.Create(35), consumer.Age);
                Assert.AreEqual(Money.Create(1234.56m), consumer.OtherIncFrom1008);
            }
        }

        [Test]
        public void ProcessRequest_SetLegacyAppFields_UpdatesEntity()
        {
            var json = @"
{
    ""data"": {
        ""version"": 1739,
        ""requests"": [
            {
                ""name"": ""set"",
                ""operationId"": ""88"",
                ""args"": {
                    ""loan"": {
                        ""LegacyApplications"": {
                            """ + this.legacyApp1Id.ToString() + @""": {
                                ""Manner"": ""Mild"",
                                ""InterviewerMethodType"": ""1"",
                                ""InterviewerMethodTLocked"": ""True"",
                                ""LiquidAssets"": ""$1,234.56""
                            }
                        }
                    }
                }
            }
        ]
    }
}";

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.ConfigurationQuery, FoolHelper.DriverType.SqlQuery, FoolHelper.DriverType.RegularExpression);

                var responseJson = LqbDataService.ProcessRequest(this.loanId, LqbDataServiceOperation.Save, json);

                var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                    this.loanId.Value,
                    typeof(LqbDataServiceTest));
                loan.InitLoad();
                var legacyApp = loan.LegacyApplications[this.legacyApp1Id];

                Assert.AreEqual(DescriptionField.Create("Mild"), legacyApp.Manner);
                Assert.AreEqual(E_aIntrvwrMethodT.FaceToFace, legacyApp.InterviewerMethodType);
                Assert.AreEqual((Money)1234.56M, legacyApp.LiquidAssets);
            }
        }

        [Test]
        public void ProcessRequest_SetEmploymentFieldsAndIncomeRecords_UpdatesEntities()
        {
            var json = @"
{
    ""data"": {
        ""version"": 1739,
        ""requests"": [
            {
                ""name"": ""set"",
                ""operationId"": ""82"",
                ""args"": {
                    ""loan"": {
                        ""EmploymentRecords"": {
                            """ + this.employmentRecordId.ToString() +  @""": {
                                ""EmployerName"": ""Updated"",
                                ""IncomeRecords"": {
                                    """ + this.incomeRecordId.ToString() + @""": {
                                        ""IncomeType"": ""15"",
                                        ""MonthlyAmount"": ""$100.00""
                                    }
                                }
                            }
                        }
                    }
                }
            }
        ]
    }
}";
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.ConfigurationQuery, FoolHelper.DriverType.SqlQuery, FoolHelper.DriverType.RegularExpression);

                var responseJson = LqbDataService.ProcessRequest(this.loanId, LqbDataServiceOperation.Save, json);

                var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                    this.loanId.Value,
                    typeof(LqbDataServiceTest));
                loan.InitLoad();
                var employment = loan.EmploymentRecords[this.employmentRecordId];
                var income = employment.IncomeRecords[this.incomeRecordId];

                Assert.AreEqual("Updated", employment.EmployerName.ToString());
                Assert.AreEqual(IncomeType.MilitaryVariableHousingAllowance, income.IncomeType);
                Assert.AreEqual((Money)100m, income.MonthlyAmount);
            }
        }
        [Test]
        public void ProcessRequest_RemoveAsset_UpdatesCollection()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""removeEntity"",
                            ""operationId"": ""68"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""collectionName"": ""Assets"",
                                ""id"": """+ this.asset1Id + @"""
                            }
                        }
                    ]
                }
            }";

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.ConfigurationQuery, FoolHelper.DriverType.SqlQuery, FoolHelper.DriverType.RegularExpression);

                var responseJson = LqbDataService.ProcessRequest(this.loanId, LqbDataServiceOperation.Save, json);

                var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                    this.loanId.Value,
                    typeof(LqbDataServiceTest));
                loan.InitLoad();

                Assert.AreEqual(false, loan.Assets.ContainsKey(DataObjectIdentifier<DataObjectKind.Asset, Guid>.Create(this.asset1Id)));
                Assert.AreEqual(false, loan.ConsumerAssets.Values.Any(a => a.AssetId.Value == this.asset1Id));
            }
        }

        [Test]
        public void ProcessRequest_AddAsset()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""addEntity"",
                            ""operationId"": ""69"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""collectionName"": ""Assets"",
                                ""ownerId"": """ + this.consumer1Id.ToString() + @""",
                                ""additionalOwnerIds"": [""" + this.consumer2Id.ToString() + @"""],
                                ""id"": null
                            }
                        }
                    ]
                }
            }";

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.ConfigurationQuery, FoolHelper.DriverType.SqlQuery, FoolHelper.DriverType.RegularExpression);

                var responseJson = LqbDataService.ProcessRequest(this.loanId, LqbDataServiceOperation.Save, json);

                var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                    this.loanId.Value,
                    typeof(LqbDataServiceTest));
                loan.InitLoad();

                Assert.AreEqual(this.initialAssetCount + 1, loan.Assets.Count);

                var response = JObject.Parse(responseJson);
                Assert.AreEqual("69", (string)response.SelectToken("data.responses[?(@.name == 'addEntity')].operationId"));
                var id = (string)response.SelectToken("data.responses[?(@.name == 'addEntity')].returns.id");
                Assert.IsNotNullOrEmpty(id);

                var key = DataObjectIdentifier<DataObjectKind.Asset, Guid>.Create(id).Value;
                Assert.IsTrue(loan.Assets.ContainsKey(key));

                var primaryAssociation = loan.ConsumerAssets.Values.Where(a => a.AssetId == key && a.IsPrimary);
                Assert.AreEqual(1, primaryAssociation.Count());
                Assert.AreEqual(consumer1Id, primaryAssociation.Single().ConsumerId);

                var additionalAssociation = loan.ConsumerAssets.Values.Where(a => a.AssetId == key && !a.IsPrimary);
                Assert.AreEqual(1, additionalAssociation.Count());
                Assert.AreEqual(consumer2Id, additionalAssociation.Single().ConsumerId);

                // Undo the addition so other tests won't fail.
                this.RemoveAsset(key.Value);
            }
        }

        [Test]
        public void ProcessRequest_AddAssetWithPropValues()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""addEntity"",
                            ""operationId"": ""69"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""collectionName"": ""Assets"",
                                ""ownerId"": """ + this.consumer1Id.ToString() + @""",
                                ""additionalOwnerIds"": [""" + this.consumer2Id.ToString() + @"""],
                                ""fields"": {
                                    ""AssetType"": ""3"",
                                    ""State"": ""CA""
                                },
                                ""id"": null
                            }
                        }
                    ]
                }
            }";

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.ConfigurationQuery, FoolHelper.DriverType.SqlQuery, FoolHelper.DriverType.RegularExpression);

                var responseJson = LqbDataService.ProcessRequest(this.loanId, LqbDataServiceOperation.Save, json);

                var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                    this.loanId.Value,
                    typeof(LqbDataServiceTest));
                loan.InitLoad();

                Assert.AreEqual(this.initialAssetCount + 1, loan.Assets.Count);

                var response = JObject.Parse(responseJson);
                Assert.AreEqual("69", (string)response.SelectToken("data.responses[?(@.name == 'addEntity')].operationId"));
                var id = (string)response.SelectToken("data.responses[?(@.name == 'addEntity')].returns.id");
                Assert.IsNotNullOrEmpty(id);

                var key = DataObjectIdentifier<DataObjectKind.Asset, Guid>.Create(id).Value;
                Assert.IsTrue(loan.Assets.ContainsKey(key));

                var asset = loan.Assets[key];
                Assert.AreEqual(E_AssetT.Checking, asset.AssetType.Value);
                Assert.AreEqual("CA", asset.State.Value.ToString());

                var primaryAssociation = loan.ConsumerAssets.Values.Where(a => a.AssetId == key && a.IsPrimary);
                Assert.AreEqual(1, primaryAssociation.Count());
                Assert.AreEqual(consumer1Id, primaryAssociation.Single().ConsumerId);

                var additionalAssociation = loan.ConsumerAssets.Values.Where(a => a.AssetId == key && !a.IsPrimary);
                Assert.AreEqual(1, additionalAssociation.Count());
                Assert.AreEqual(consumer2Id, additionalAssociation.Single().ConsumerId);

                // Undo the addition so other tests won't fail.
                this.RemoveAsset(key.Value);
            }
        }

        [Test]
        public void ProcessRequest_AddIncomeRecordToEmploymentWithPropValues_Succeeds()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""addEntity"",
                            ""operationId"": ""70"",
                            ""args"": {
                                ""path"": ""loan.EmploymentRecords[" + this.employmentRecordId.ToString() + @"]"",
                                ""collectionName"": ""IncomeRecords"",
                                ""fields"": {
                                    ""IncomeType"": ""1"",
                                    ""MonthlyAmount"": ""$5.00""
                                },
                                ""id"": null
                            }
                        }
                    ]
                }
            }";

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.ConfigurationQuery, FoolHelper.DriverType.SqlQuery, FoolHelper.DriverType.RegularExpression);

                var responseJson = LqbDataService.ProcessRequest(this.loanId, LqbDataServiceOperation.Save, json);

                var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                    this.loanId.Value,
                    typeof(LqbDataServiceTest));
                loan.InitLoad();

                var response = JObject.Parse(responseJson);
                Assert.AreEqual("70", (string)response.SelectToken("data.responses[?(@.name == 'addEntity')].operationId"));
                var id = (string)response.SelectToken("data.responses[?(@.name == 'addEntity')].returns.id");
                Assert.IsNotNullOrEmpty(id);

                var key = DataObjectIdentifier<DataObjectKind.IncomeRecord, Guid>.Create(id).Value;
                Assert.IsTrue(loan.EmploymentRecords[this.employmentRecordId].IncomeRecords.ContainsKey(key));

                var income = loan.EmploymentRecords[this.employmentRecordId].IncomeRecords[key];
                Assert.AreEqual(IncomeType.Overtime, income.IncomeType);
                Assert.AreEqual((Money)5, income.MonthlyAmount);
            }
        }

        [Test]
        public void ProcessRequest_RemoveIncomeRecordFromEmployment_Succeeds()
        {
            DataObjectIdentifier<DataObjectKind.IncomeRecord, Guid> addedId;
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.ConfigurationQuery, FoolHelper.DriverType.SqlQuery, FoolHelper.DriverType.RegularExpression);

                var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                    this.loanId.Value,
                    typeof(LqbDataServiceTest));
                loan.InitSave();

                addedId = loan.EmploymentRecords[this.employmentRecordId].IncomeRecords.Add(new Core.Data.IncomeRecord());

                loan.Save();
            }

            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""removeEntity"",
                            ""operationId"": ""70"",
                            ""args"": {
                                ""path"": ""loan.EmploymentRecords[" + this.employmentRecordId.ToString() + @"]"",
                                ""collectionName"": ""IncomeRecords"",
                                ""id"": """ + addedId.ToString() + @"""
                            }
                        }
                    ]
                }
            }";

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.ConfigurationQuery, FoolHelper.DriverType.SqlQuery, FoolHelper.DriverType.RegularExpression);

                var responseJson = LqbDataService.ProcessRequest(this.loanId, LqbDataServiceOperation.Save, json);

                var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                    this.loanId.Value,
                    typeof(LqbDataServiceTest));
                loan.InitLoad();

                Assert.IsFalse(loan.EmploymentRecords[this.employmentRecordId].IncomeRecords.ContainsKey(addedId));
            }
        }

        [Test]
        public void ProcessRequest_AddTwoAssets_AddsBoth()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""addEntity"",
                            ""operationId"": ""70"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""collectionName"": ""Assets"",
                                ""ownerId"": """+ this.consumer1Id.ToString() + @""",
                                ""id"": null
                            }
                        },
                        {
                            ""name"": ""addEntity"",
                            ""operationId"": ""71"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""collectionName"": ""Assets"",
                                ""ownerId"": """+ this.consumer2Id.ToString() + @""",
                                ""id"": null
                            }
                        }
                    ]
                }
            }";

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.ConfigurationQuery, FoolHelper.DriverType.SqlQuery, FoolHelper.DriverType.RegularExpression);

                var responseJson = LqbDataService.ProcessRequest(this.loanId, LqbDataServiceOperation.Save, json);

                var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                    this.loanId.Value,
                    typeof(LqbDataServiceTest));
                loan.InitLoad();

                Assert.AreEqual(this.initialAssetCount + 2, loan.Assets.Count);

                var response = JObject.Parse(responseJson);
                Assert.AreEqual("70", (string)response.SelectToken("data.responses[0].operationId"));
                var id1 = (string)response.SelectToken("data.responses[0].returns.id");
                Assert.IsNotNullOrEmpty(id1);
                Assert.AreEqual("71", (string)response.SelectToken("data.responses[1].operationId"));
                var id2 = (string)response.SelectToken("data.responses[1].returns.id");
                Assert.IsNotNullOrEmpty(id2);

                var key1 = DataObjectIdentifier<DataObjectKind.Asset, Guid>.Create(id1).Value;
                Assert.IsTrue(loan.Assets.ContainsKey(key1));

                var key2 = DataObjectIdentifier<DataObjectKind.Asset, Guid>.Create(id2).Value;
                Assert.IsTrue(loan.Assets.ContainsKey(key2));

                // Undo the addition so other tests won't fail.
                this.RemoveAsset(key1.Value);
                this.RemoveAsset(key2.Value);
            }
        }
        [Test]
        public void ProcessRequest_AddEntities_AddAssociation()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.ConfigurationQuery, FoolHelper.DriverType.SqlQuery, FoolHelper.DriverType.RegularExpression);

                var tuple = this.AddAssociatedLiablityAndRealProperty();
                var liabilityId = tuple.Item1;
                var propertyId = tuple.Item2;
                var associationId = tuple.Item3;

                var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                    this.loanId.Value,
                    typeof(LqbDataServiceTest));
                loan.InitLoad();

                var keyL = DataObjectIdentifier<DataObjectKind.Liability, Guid>.Create(liabilityId);
                Assert.IsTrue(loan.Liabilities.ContainsKey(keyL));

                var keyP = DataObjectIdentifier<DataObjectKind.RealProperty, Guid>.Create(propertyId);
                Assert.IsTrue(loan.RealProperties.ContainsKey(keyP));

                var keyA = DataObjectIdentifier<DataObjectKind.RealPropertyLiabilityAssociation, Guid>.Create(associationId);
                Assert.IsTrue(loan.RealPropertyLiabilities.ContainsKey(keyA));
            }
        }

        [Test]
        public void ProcessRequest_AddAssociatedEmploymentAndIncome_AssociationsExistInLoadedCollections()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.ConfigurationQuery, FoolHelper.DriverType.SqlQuery, FoolHelper.DriverType.RegularExpression);

                var tuple = this.AddAssociatedIncomeSourceAndEmploymentRecord();
                var incomeId = tuple.Item1;
                var employmentId = tuple.Item2;
                var associationId = tuple.Item3;

                var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                    this.loanId.Value,
                    typeof(LqbDataServiceTest));
                loan.InitLoad();

                var keyE = DataObjectIdentifier<DataObjectKind.EmploymentRecord, Guid>.Create(employmentId);
                Assert.IsTrue(loan.EmploymentRecords.ContainsKey(keyE));

                var keyI = DataObjectIdentifier<DataObjectKind.IncomeSource, Guid>.Create(incomeId);
                Assert.IsTrue(loan.IncomeSources.ContainsKey(keyI));

                var keyA = DataObjectIdentifier<DataObjectKind.IncomeSourceEmploymentRecordAssociation, Guid>.Create(associationId);
                Assert.IsTrue(loan.IncomeSourceEmploymentRecords.ContainsKey(keyA));
            }
        }

        [Test]
        public void ProcessRequest_RemoveAssociation()
        {
            var jsonTemplate = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""removeAssociation"",
                            ""operationId"": ""72"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""associationSet"": ""RealPropertyLiabilities"",
                                ""id"": ""ASSOCIATION_ID_HERE""
                            }
                        }
                    ]
                }
            }";

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.ConfigurationQuery, FoolHelper.DriverType.SqlQuery, FoolHelper.DriverType.RegularExpression);

                var tuple = this.AddAssociatedLiablityAndRealProperty();
                var liabilityId = tuple.Item1;
                var propertyId = tuple.Item2;
                var associationId = tuple.Item3;

                string json = jsonTemplate.Replace("ASSOCIATION_ID_HERE", associationId.ToString("D"));
                var responseJson = LqbDataService.ProcessRequest(this.loanId, LqbDataServiceOperation.Save, json);

                var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                    this.loanId.Value,
                    typeof(LqbDataServiceTest));
                loan.InitLoad();

                var keyL = DataObjectIdentifier<DataObjectKind.Liability, Guid>.Create(liabilityId);
                Assert.IsTrue(loan.Liabilities.ContainsKey(keyL));

                var keyP = DataObjectIdentifier<DataObjectKind.RealProperty, Guid>.Create(propertyId);
                Assert.IsTrue(loan.RealProperties.ContainsKey(keyP));

                var keyA = DataObjectIdentifier<DataObjectKind.RealPropertyLiabilityAssociation, Guid>.Create(associationId);
                Assert.IsFalse(loan.RealPropertyLiabilities.ContainsKey(keyA));
            }
        }

        [Test]
        public void ProcessRequest_RemoveIncomeEmploymentAssociation_RemovedFromLoadedCollections()
        {
            var jsonTemplate = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""removeAssociation"",
                            ""operationId"": ""72"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""associationSet"": ""IncomeSourceEmploymentRecords"",
                                ""id"": ""ASSOCIATION_ID_HERE""
                            }
                        }
                    ]
                }
            }";

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.ConfigurationQuery, FoolHelper.DriverType.SqlQuery, FoolHelper.DriverType.RegularExpression);

                var tuple = this.AddAssociatedIncomeSourceAndEmploymentRecord();
                var incomeId = tuple.Item1;
                var employmentId = tuple.Item2;
                var associationId = tuple.Item3;

                string json = jsonTemplate.Replace("ASSOCIATION_ID_HERE", associationId.ToString("D"));
                var responseJson = LqbDataService.ProcessRequest(this.loanId, LqbDataServiceOperation.Save, json);

                var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                    this.loanId.Value,
                    typeof(LqbDataServiceTest));
                loan.InitLoad();

                var keyI = DataObjectIdentifier<DataObjectKind.IncomeSource, Guid>.Create(incomeId);
                Assert.IsTrue(loan.IncomeSources.ContainsKey(keyI));

                var keyE = DataObjectIdentifier<DataObjectKind.EmploymentRecord, Guid>.Create(employmentId);
                Assert.IsTrue(loan.EmploymentRecords.ContainsKey(keyE));

                var keyA = DataObjectIdentifier<DataObjectKind.IncomeSourceEmploymentRecordAssociation, Guid>.Create(associationId);
                Assert.IsFalse(loan.IncomeSourceEmploymentRecords.ContainsKey(keyA));
            }
        }

        [Test]
        public void ProcessRequest_SwapIsPrimaryForOwnership_UpdatesAssociations()
        {
            var json = @"
{
    ""data"": {
        ""version"": 1739,
        ""requests"": [
            {
                ""name"": ""set"",
                ""operationId"": ""73"",
                ""args"": {
                    ""loan"": {
                        ""ConsumerAssets"": {
                            """ + this.primaryAsset1OwnershipId.ToString() + @""": {
                                ""IsPrimary"": ""False""
                            },
                            """ + this.secondaryAsset1OwnershipId.ToString() + @""": {
                                ""IsPrimary"": ""True""
                            }
                        }
                    }
                }
            }
        ]
    }
}";

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.ConfigurationQuery, FoolHelper.DriverType.SqlQuery, FoolHelper.DriverType.RegularExpression);

                LqbDataService.ProcessRequest(this.loanId, LqbDataServiceOperation.Save, json);

                var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                    this.loanId.Value,
                    typeof(LqbDataServiceTest));
                loan.InitLoad();

                Assert.AreEqual(false, loan.ConsumerAssets[this.primaryAsset1OwnershipId].IsPrimary);
                Assert.AreEqual(true, loan.ConsumerAssets[this.secondaryAsset1OwnershipId].IsPrimary);
            }
        }

        [Test]
        public void ProcessRequest_SetOwnershipForAsset_UpdatesAssociations()
        {
            var json = @"
{
    ""data"": {
        ""version"": 1739,
        ""requests"": [
            {
                ""name"": ""setOwnership"",
                ""operationId"": ""74"",
                ""args"": {
                    ""path"": ""loan"",
                    ""associationName"": ""ConsumerAssets"",
                    ""id"": """ + this.asset1Id.ToString() + @""",
                    ""primaryOwnerId"": """ + this.consumer3Id.ToString() + @""",
                    ""additionalOwnerIds"": [""" + this.consumer1Id.ToString() + @"""]
                }
            }
        ]
    }
}";

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.ConfigurationQuery, FoolHelper.DriverType.SqlQuery, FoolHelper.DriverType.RegularExpression);

                LqbDataService.ProcessRequest(this.loanId, LqbDataServiceOperation.Save, json);

                var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                    this.loanId.Value,
                    typeof(LqbDataServiceTest));
                loan.InitLoad();

                var asset1OwnershipAssociations = loan.ConsumerAssets.Where(a => a.Value.AssetId.Value == this.asset1Id);
                Assert.AreEqual(2, asset1OwnershipAssociations.Count());

                var primaryAssociation = asset1OwnershipAssociations.Single(a => a.Value.IsPrimary);
                Assert.AreEqual(consumer3Id, primaryAssociation.Value.ConsumerId);
                Assert.AreEqual(this.asset1Id, primaryAssociation.Value.AssetId.Value);
                Assert.AreEqual(this.legacyApp2Id, primaryAssociation.Value.AppId);

                var secondaryAssociation = asset1OwnershipAssociations.Single(a => !a.Value.IsPrimary);
                Assert.AreEqual(consumer1Id, secondaryAssociation.Value.ConsumerId);
                Assert.AreEqual(this.asset1Id, secondaryAssociation.Value.AssetId.Value);
                Assert.AreEqual(this.legacyApp1Id, secondaryAssociation.Value.AppId);
            }
        }

        [Test]
        public void ProcessRequest_SwapPrimaryBorrOwnershipAndSecondaryCoborrOwnershipForAsset_UpdatesAssociations()
        {
            var json = @"
{
    ""data"": {
        ""version"": 1739,
        ""requests"": [
            {
                ""name"": ""setOwnership"",
                ""operationId"": ""75"",
                ""args"": {
                    ""path"": ""loan"",
                    ""associationName"": ""ConsumerAssets"",
                    ""id"": """ + this.asset1Id.ToString() + @""",
                    ""primaryOwnerId"": """ + this.consumer2Id.ToString() + @""",
                    ""additionalOwnerIds"": [""" + this.consumer1Id.ToString() + @"""]
                }
            }
        ]
    }
}";

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.ConfigurationQuery, FoolHelper.DriverType.SqlQuery, FoolHelper.DriverType.RegularExpression);

                LqbDataService.ProcessRequest(this.loanId, LqbDataServiceOperation.Save, json);

                var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                    this.loanId.Value,
                    typeof(LqbDataServiceTest));
                loan.InitLoad();

                var asset1OwnershipAssociations = loan.ConsumerAssets.Where(a => a.Value.AssetId.Value == this.asset1Id);
                Assert.AreEqual(2, asset1OwnershipAssociations.Count());

                var primaryAssociation = asset1OwnershipAssociations.Single(a => a.Value.IsPrimary);
                Assert.AreEqual(consumer2Id, primaryAssociation.Value.ConsumerId);
                Assert.AreEqual(this.asset1Id, primaryAssociation.Value.AssetId.Value);
                Assert.AreEqual(this.legacyApp1Id, primaryAssociation.Value.AppId);

                var secondaryAssociation = asset1OwnershipAssociations.Single(a => !a.Value.IsPrimary);
                Assert.AreEqual(consumer1Id, secondaryAssociation.Value.ConsumerId);
                Assert.AreEqual(this.asset1Id, secondaryAssociation.Value.AssetId.Value);
                Assert.AreEqual(this.legacyApp1Id, secondaryAssociation.Value.AppId);
            }
        }

        [Test]
        public void ProcessRequest_SwapPrimaryCoborrOwnershipAndSecondaryBorrOwnershipForAsset_UpdatesAssociations()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.ConfigurationQuery, FoolHelper.DriverType.SqlQuery, FoolHelper.DriverType.RegularExpression);

                var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                    this.loanId.Value,
                    typeof(LqbDataServiceTest));
                loan.InitSave(ConstAppDavid.SkipVersionCheck);

                var primaryAssociation = loan.ConsumerAssets.Values.Single(a => a.AssetId.Value == this.asset1Id && a.IsPrimary);
                var secondaryAssociation = loan.ConsumerAssets.Values.Single(a => a.AssetId.Value == this.asset1Id && !a.IsPrimary);

                primaryAssociation.IsPrimary = false;
                secondaryAssociation.IsPrimary = true;

                loan.Save();
            }

            var json = @"
{
    ""data"": {
        ""version"": 1739,
        ""requests"": [
            {
                ""name"": ""setOwnership"",
                ""operationId"": ""76"",
                ""args"": {
                    ""path"": ""loan"",
                    ""associationName"": ""ConsumerAssets"",
                    ""id"": """ + this.asset1Id.ToString() + @""",
                    ""primaryOwnerId"": """ + this.consumer1Id.ToString() + @""",
                    ""additionalOwnerIds"": [""" + this.consumer2Id.ToString() + @"""]
                }
            }
        ]
    }
}";

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.ConfigurationQuery, FoolHelper.DriverType.SqlQuery, FoolHelper.DriverType.RegularExpression);

                LqbDataService.ProcessRequest(this.loanId, LqbDataServiceOperation.Save, json);

                var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                    this.loanId.Value,
                    typeof(LqbDataServiceTest));
                loan.InitLoad();

                var asset1OwnershipAssociations = loan.ConsumerAssets.Where(a => a.Value.AssetId.Value == this.asset1Id);
                Assert.AreEqual(2, asset1OwnershipAssociations.Count());

                var primaryAssociation = asset1OwnershipAssociations.Single(a => a.Value.IsPrimary);
                Assert.AreEqual(consumer1Id, primaryAssociation.Value.ConsumerId);
                Assert.AreEqual(this.asset1Id, primaryAssociation.Value.AssetId.Value);
                Assert.AreEqual(this.legacyApp1Id, primaryAssociation.Value.AppId);

                var secondaryAssociation = asset1OwnershipAssociations.Single(a => !a.Value.IsPrimary);
                Assert.AreEqual(consumer2Id, secondaryAssociation.Value.ConsumerId);
                Assert.AreEqual(this.asset1Id, secondaryAssociation.Value.AssetId.Value);
                Assert.AreEqual(this.legacyApp1Id, secondaryAssociation.Value.AppId);
            }
        }

        [Test]
        public void ProcessRequest_SetAttendanceForCounselingEvent_UpdatesAssociations()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.SqlQuery);

                var loanBefore = CPageData.CreateUsingSmartDependencyWithSecurityBypass(this.loanId.Value, typeof(LqbDataServiceTest));
                loanBefore.InitLoad();
                Assert.AreEqual(1, loanBefore.CounselingEvents.Count);
                Assert.AreEqual(1, loanBefore.CounselingEventAttendances.Count);
                CollectionAssert.AreEquivalent(new[] { this.consumer1Id }, loanBefore.CounselingEventAttendances.Values.Select(a => a.ConsumerId));

                string json = @"
{
    ""data"": {
        ""version"": 1739,
        ""requests"": [
            {
                ""name"": ""setAttendance"",
                ""operationId"": ""40"",
                ""args"": {
                    ""path"": ""loan"",
                    ""associationName"": ""CounselingEventAttendances"",
                    ""id"": """ + this.counselingEventId + @""",
                    ""attendeeIds"": [
                        """ + this.consumer1Id + @""",
                        """ + this.consumer2Id + @"""
                    ]
                }
            }
        ]
    }
}";
                LqbDataService.ProcessRequest(this.loanId, LqbDataServiceOperation.Save, json);

                var loanAfter = CPageData.CreateUsingSmartDependencyWithSecurityBypass(this.loanId.Value, typeof(LqbDataServiceTest));
                loanAfter.InitLoad();
                Assert.AreEqual(1, loanAfter.CounselingEvents.Count);
                Assert.AreEqual(2, loanAfter.CounselingEventAttendances.Count);
                CollectionAssert.AreEquivalent(new[] { this.consumer1Id, this.consumer2Id }, loanAfter.CounselingEventAttendances.Values.Select(a => a.ConsumerId));
            }
        }


        [Test]
        public void ProcessRequest_ReOrderAssets_UpdatesCollectionOrder()
        {

            var json = @"
{
    ""data"": {
        ""version"": 1739,
        ""requests"": [
            {
                ""name"": ""setOrder"",
                ""operationId"": ""77"",
                ""args"": {
                    ""path"": ""loan"",
                    ""collectionName"": ""Assets"",
                    ""order"": [""" + string.Join("\", \"", new[] { this.asset3Id, this.asset2Id, this.asset1Id }) + @"""]
                }
            }
        ]
    }
}";

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.ConfigurationQuery, FoolHelper.DriverType.SqlQuery, FoolHelper.DriverType.RegularExpression);

                LqbDataService.ProcessRequest(this.loanId, LqbDataServiceOperation.Save, json);

                var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                    this.loanId.Value,
                    typeof(LqbDataServiceTest));
                loan.InitLoad();

                CollectionAssert.AreEqual(new[] { this.asset3Id, this.asset2Id, this.asset1Id }, loan.Assets.Keys.Select(id => id.Value));
            }
        }

        [Test]
        public void ProcessRequest_ReOrderConsumers_UpdatesCollectionOrder()
        {

            var jsonReOrder = @"
{
    ""data"": {
        ""version"": 1739,
        ""requests"": [
            {
                ""name"": ""setOrder"",
                ""operationId"": ""77"",
                ""args"": {
                    ""path"": ""loan"",
                    ""collectionName"": ""Consumers"",
                    ""uladApplicationId"": """ + this.uladApp1Id.ToString() + @""",
                    ""order"": [""" + string.Join("\", \"", new[] { this.consumer2Id, this.consumer1Id }) + @"""]
                }
            }
        ]
    }
}";

            var jsonRestoreOrder = @"
{
    ""data"": {
        ""version"": 1739,
        ""requests"": [
            {
                ""name"": ""setOrder"",
                ""operationId"": ""77"",
                ""args"": {
                    ""path"": ""loan"",
                    ""collectionName"": ""Consumers"",
                    ""uladApplicationId"": """ + this.uladApp1Id.ToString() + @""",
                    ""order"": [""" + string.Join("\", \"", new[] { this.consumer1Id, this.consumer2Id }) + @"""]
                }
            }
        ]
    }
}";

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.ConfigurationQuery, FoolHelper.DriverType.SqlQuery, FoolHelper.DriverType.RegularExpression);

                // check order prior to the reorder
                var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                    this.loanId.Value,
                    typeof(LqbDataServiceTest));
                loan.InitLoad();

                // The loan should start out in the order echoing the legacy order since we didn't do any manipulations
                CollectionAssert.AreEqual(new[] { this.consumer1Id, this.consumer2Id, this.consumer3Id }, loan.Consumers.Keys);

                LqbDataService.ProcessRequest(this.loanId, LqbDataServiceOperation.Save, jsonReOrder);

                loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                    this.loanId.Value,
                    typeof(LqbDataServiceTest));
                loan.InitLoad();

                CollectionAssert.AreEqual(new[] { this.consumer2Id, this.consumer1Id, this.consumer3Id }, loan.Consumers.Keys);

                // If the code gets here then the reorder works, so we will use the same operation to return the file to the correct state so other tests aren't impacted.
                LqbDataService.ProcessRequest(this.loanId, LqbDataServiceOperation.Save, jsonRestoreOrder);
            }
        }

        [Test]
        public void ProcessRequest_AddLiabilityBalanceAndPaymentToRealProperty_UpdatesTheRealProperty()
        {
            string json = @"
{
    ""data"": {
        ""version"": 1739,
        ""requests"": [
            {
                ""name"": ""addLiabilityBalanceAndPaymentToRealProperty"",
                ""operationId"": ""77"",
                ""args"": {
                    ""path"": ""loan"",
                    ""liabilityId"": ""#LIABILITY_ID#"",
                    ""realPropertyId"": ""#REAL_PROPERTY_ID#""
                }
            }
        ]
    }
}";

            using (var loanRef = LoanForIntegrationTest.Create(TestAccountType.LoTest001, useUladDataLayer: true))
            {
                var loanBefore = loanRef.CreateNewPageDataWithBypass();
                loanBefore.InitSave();
                var consumer = loanBefore.Consumers.Keys.First();
                Guid rawLiabilityId = loanBefore.HandleAdd(new Core.Commands.AddEntity(default(DataPath), nameof(CPageData.Liabilities), consumer.Value, Enumerable.Empty<Guid>(), null, null), null);
                Guid rawRealPropertyId = loanBefore.HandleAdd(new Core.Commands.AddEntity(default(DataPath), nameof(CPageData.RealProperties), consumer.Value, Enumerable.Empty<Guid>(), null, null), null);
                loanBefore.HandleAdd(new Core.Commands.AddAssociation(default(DataPath), nameof(CPageData.RealPropertyLiabilities), rawRealPropertyId, rawLiabilityId));
                DataObjectIdentifier<DataObjectKind.Liability, Guid> liabilityId = rawLiabilityId.ToIdentifier<DataObjectKind.Liability>();
                DataObjectIdentifier<DataObjectKind.RealProperty, Guid> realPropertyId = rawRealPropertyId.ToIdentifier<DataObjectKind.RealProperty>();
                Core.Data.Liability liabilityBefore = loanBefore.Liabilities[liabilityId];
                liabilityBefore.Bal = 30000M;
                liabilityBefore.Pmt = 250M;
                Core.Data.RealProperty propertyBefore = loanBefore.RealProperties[realPropertyId];
                propertyBefore.MtgAmt = 123456M;
                propertyBefore.MtgPmt = 1000M;
                loanBefore.Save();
                json = json.Replace("#LIABILITY_ID#", liabilityId.ToString()).Replace("#REAL_PROPERTY_ID#", realPropertyId.ToString());

                LqbDataService.ProcessRequest(loanRef.Identifier, LqbDataServiceOperation.Save, json);

                var loan = loanRef.CreateNewPageDataWithBypass();
                loan.InitLoad();
                Core.Data.Liability liability = loan.Liabilities[liabilityId];
                Core.Data.RealProperty property = loan.RealProperties[realPropertyId];
                Assert.AreEqual((Money)30000M, liability.Bal);
                Assert.AreEqual((Money)250M, liability.Pmt);
                Assert.AreEqual((Money)153456M, property.MtgAmt);
                Assert.AreEqual((Money)1250M, property.MtgPmt);
            }
        }

        [Test]
        public void ProcessRequest_AddLiabilityBalanceAndPaymentToRealProperty_ForBlankRealProperty_SetsTheRealPropertyToTheLiabilityValues()
        {
            string json = @"
{
    ""data"": {
        ""version"": 1739,
        ""requests"": [
            {
                ""name"": ""addLiabilityBalanceAndPaymentToRealProperty"",
                ""operationId"": ""77"",
                ""args"": {
                    ""path"": ""loan"",
                    ""liabilityId"": ""#LIABILITY_ID#"",
                    ""realPropertyId"": ""#REAL_PROPERTY_ID#""
                }
            }
        ]
    }
}";

            using (var loanRef = LoanForIntegrationTest.Create(TestAccountType.LoTest001, useUladDataLayer: true))
            {
                var loanBefore = loanRef.CreateNewPageDataWithBypass();
                loanBefore.InitSave();
                var consumer = loanBefore.Consumers.Keys.First();
                Guid rawLiabilityId = loanBefore.HandleAdd(new Core.Commands.AddEntity(default(DataPath), nameof(CPageData.Liabilities), consumer.Value, Enumerable.Empty<Guid>(), null, null), null);
                Guid rawRealPropertyId = loanBefore.HandleAdd(new Core.Commands.AddEntity(default(DataPath), nameof(CPageData.RealProperties), consumer.Value, Enumerable.Empty<Guid>(), null, null), null);
                loanBefore.HandleAdd(new Core.Commands.AddAssociation(default(DataPath), nameof(CPageData.RealPropertyLiabilities), rawRealPropertyId, rawLiabilityId));
                DataObjectIdentifier<DataObjectKind.Liability, Guid> liabilityId = rawLiabilityId.ToIdentifier<DataObjectKind.Liability>();
                DataObjectIdentifier<DataObjectKind.RealProperty, Guid> realPropertyId = rawRealPropertyId.ToIdentifier<DataObjectKind.RealProperty>();
                Core.Data.Liability liabilityBefore = loanBefore.Liabilities[liabilityId];
                liabilityBefore.Bal = 25000M;
                liabilityBefore.Pmt = 275M;
                Core.Data.RealProperty propertyBefore = loanBefore.RealProperties[realPropertyId];
                Assert.IsNull(propertyBefore.MtgAmt);
                Assert.IsNull(propertyBefore.MtgPmt);
                loanBefore.Save();
                json = json.Replace("#LIABILITY_ID#", liabilityId.ToString()).Replace("#REAL_PROPERTY_ID#", realPropertyId.ToString());

                LqbDataService.ProcessRequest(loanRef.Identifier, LqbDataServiceOperation.Save, json);

                var loan = loanRef.CreateNewPageDataWithBypass();
                loan.InitLoad();
                Core.Data.Liability liability = loan.Liabilities[liabilityId];
                Core.Data.RealProperty property = loan.RealProperties[realPropertyId];
                Assert.AreEqual((Money)25000M, liability.Bal);
                Assert.AreEqual((Money)275M, liability.Pmt);
                Assert.AreEqual((Money)25000M, property.MtgAmt);
                Assert.AreEqual((Money)275M, property.MtgPmt);
            }
        }

        [Test]
        public void ProcessRequest_AddLiabilityBalanceAndPaymentToRealProperty_ForBlankLiability_DoesNothing()
        {
            string json = @"
{
    ""data"": {
        ""version"": 1739,
        ""requests"": [
            {
                ""name"": ""addLiabilityBalanceAndPaymentToRealProperty"",
                ""operationId"": ""77"",
                ""args"": {
                    ""path"": ""loan"",
                    ""liabilityId"": ""#LIABILITY_ID#"",
                    ""realPropertyId"": ""#REAL_PROPERTY_ID#""
                }
            }
        ]
    }
}";

            using (var loanRef = LoanForIntegrationTest.Create(TestAccountType.LoTest001, useUladDataLayer: true))
            {
                var loanBefore = loanRef.CreateNewPageDataWithBypass();
                loanBefore.InitSave();
                var consumer = loanBefore.Consumers.Keys.First();
                Guid rawLiabilityId = loanBefore.HandleAdd(new Core.Commands.AddEntity(default(DataPath), nameof(CPageData.Liabilities), consumer.Value, Enumerable.Empty<Guid>(), null, null), null);
                Guid rawRealPropertyId = loanBefore.HandleAdd(new Core.Commands.AddEntity(default(DataPath), nameof(CPageData.RealProperties), consumer.Value, Enumerable.Empty<Guid>(), null, null), null);
                loanBefore.HandleAdd(new Core.Commands.AddAssociation(default(DataPath), nameof(CPageData.RealPropertyLiabilities), rawRealPropertyId, rawLiabilityId));
                DataObjectIdentifier<DataObjectKind.Liability, Guid> liabilityId = rawLiabilityId.ToIdentifier<DataObjectKind.Liability>();
                DataObjectIdentifier<DataObjectKind.RealProperty, Guid> realPropertyId = rawRealPropertyId.ToIdentifier<DataObjectKind.RealProperty>();
                Core.Data.Liability liabilityBefore = loanBefore.Liabilities[liabilityId];
                Assert.IsNull(liabilityBefore.Bal);
                Assert.IsNull(liabilityBefore.Pmt);
                Core.Data.RealProperty propertyBefore = loanBefore.RealProperties[realPropertyId];
                propertyBefore.MtgAmt = 50025.87M;
                propertyBefore.MtgPmt = 1200M;
                loanBefore.Save();
                json = json.Replace("#LIABILITY_ID#", liabilityId.ToString()).Replace("#REAL_PROPERTY_ID#", realPropertyId.ToString());

                LqbDataService.ProcessRequest(loanRef.Identifier, LqbDataServiceOperation.Save, json);

                var loan = loanRef.CreateNewPageDataWithBypass();
                loan.InitLoad();
                Core.Data.Liability liability = loan.Liabilities[liabilityId];
                Core.Data.RealProperty property = loan.RealProperties[realPropertyId];
                Assert.IsNull(liability.Bal);
                Assert.IsNull(liability.Pmt);
                Assert.AreEqual((Money)50025.87M, property.MtgAmt);
                Assert.AreEqual((Money)1200M, property.MtgPmt);
            }
        }

        [Test]
        public void ProcessRequest_AddLiabilityBalanceAndPaymentToRealProperty_ForBlankLiabilityAndRealProperty_SetsTheRealPropertyToNonNullValues()
        {
            string json = @"
{
    ""data"": {
        ""version"": 1739,
        ""requests"": [
            {
                ""name"": ""addLiabilityBalanceAndPaymentToRealProperty"",
                ""operationId"": ""77"",
                ""args"": {
                    ""path"": ""loan"",
                    ""liabilityId"": ""#LIABILITY_ID#"",
                    ""realPropertyId"": ""#REAL_PROPERTY_ID#""
                }
            }
        ]
    }
}";

            using (var loanRef = LoanForIntegrationTest.Create(TestAccountType.LoTest001, useUladDataLayer: true))
            {
                var loanBefore = loanRef.CreateNewPageDataWithBypass();
                loanBefore.InitSave();
                var consumer = loanBefore.Consumers.Keys.First();
                Guid rawLiabilityId = loanBefore.HandleAdd(new Core.Commands.AddEntity(default(DataPath), nameof(CPageData.Liabilities), consumer.Value, Enumerable.Empty<Guid>(), null, null), null);
                Guid rawRealPropertyId = loanBefore.HandleAdd(new Core.Commands.AddEntity(default(DataPath), nameof(CPageData.RealProperties), consumer.Value, Enumerable.Empty<Guid>(), null, null), null);
                loanBefore.HandleAdd(new Core.Commands.AddAssociation(default(DataPath), nameof(CPageData.RealPropertyLiabilities), rawRealPropertyId, rawLiabilityId));
                DataObjectIdentifier<DataObjectKind.Liability, Guid> liabilityId = rawLiabilityId.ToIdentifier<DataObjectKind.Liability>();
                DataObjectIdentifier<DataObjectKind.RealProperty, Guid> realPropertyId = rawRealPropertyId.ToIdentifier<DataObjectKind.RealProperty>();
                Core.Data.Liability liabilityBefore = loanBefore.Liabilities[liabilityId];
                Assert.IsNull(liabilityBefore.Bal);
                Assert.IsNull(liabilityBefore.Pmt);
                Core.Data.RealProperty propertyBefore = loanBefore.RealProperties[realPropertyId];
                Assert.IsNull(propertyBefore.MtgAmt);
                Assert.IsNull(propertyBefore.MtgPmt);
                loanBefore.Save();
                json = json.Replace("#LIABILITY_ID#", liabilityId.ToString()).Replace("#REAL_PROPERTY_ID#", realPropertyId.ToString());

                LqbDataService.ProcessRequest(loanRef.Identifier, LqbDataServiceOperation.Save, json);

                var loan = loanRef.CreateNewPageDataWithBypass();
                loan.InitLoad();
                Core.Data.Liability liability = loan.Liabilities[liabilityId];
                Core.Data.RealProperty property = loan.RealProperties[realPropertyId];
                Assert.IsNull(liability.Bal);
                Assert.IsNull(liability.Pmt);
                Assert.AreEqual((Money)0, property.MtgAmt);
                Assert.AreEqual((Money)0, property.MtgPmt);
            }
        }

        [Test]
        public void ProcessRequest_GetLiabilityTotals_ReturnsExpectedValue()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""getLiabilityTotals"",
                            ""operationId"": ""6"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""type"": ""Loan""
                            }
                        },
                    ]
                }
            }";

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.ConfigurationQuery, FoolHelper.DriverType.SqlQuery, FoolHelper.DriverType.RegularExpression);

                var response = LqbDataService.ProcessRequest(this.loanId, LqbDataServiceOperation.Load, json);
                var responseJson = JObject.Parse(response);

                var expectedResponseToken = responseJson.SelectToken("data.responses");
                Assert.AreEqual(1, expectedResponseToken.Children().Count());
                var commandResponse = expectedResponseToken.SelectToken("[?(@.operationId == '6')].returns");
                Assert.AreEqual("loan", (string)commandResponse.SelectToken("path"));
                Assert.AreEqual("Loan", (string)commandResponse.SelectToken("type"));
                Assert.AreEqual("$4.00", (string)commandResponse.SelectToken("balance"));
                Assert.AreEqual("$5.00", (string)commandResponse.SelectToken("payment"));
                Assert.AreEqual("$6.00", (string)commandResponse.SelectToken("paidOff"));
            }
        }

        [Test]
        public void ProcessRequest_GetLiabilityTotalsWithWillBePaidOffLiabilityUsingDefaultsProvider_ReturnsExpectedValue()
        {
            string json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""getLiabilityTotals"",
                            ""operationId"": ""6"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""type"": ""Loan""
                            }
                        },
                    ]
                }
            }";

            using (var loanRef = LoanForIntegrationTest.Create(TestAccountType.LoTest001, useUladDataLayer: true))
            {
                var loanBefore = loanRef.CreateNewPageDataWithBypass();
                loanBefore.InitSave();
                Core.Data.ILiabilityDefaultsProvider defaultsProvider = (new Core.Data.LiabilityDefaultsFactory()).Create(CPageBaseExtractor.Extract(loanBefore));
                var liability = new Core.Data.Liability(defaultsProvider)
                {
                    Bal = 489.46M,
                    Pmt = 123.23M,
                    PayoffAmtData = 234.35M,
                    WillBePdOff = true,
                    PayoffTimingLockedData = false,
                };
                loanBefore.AddLiability(loanBefore.Apps.First().aBConsumerId.ToIdentifier<DataObjectKind.Consumer>(), liability);
                loanBefore.Save();
                Assert.IsFalse(loanBefore.sIsRefinancing);

                var response = LqbDataService.ProcessRequest(loanRef.Identifier, LqbDataServiceOperation.Load, json);
                var responseJson = JObject.Parse(response);

                var expectedResponseToken = responseJson.SelectToken("data.responses");
                Assert.AreEqual(1, expectedResponseToken.Children().Count());
                var commandResponse = expectedResponseToken.SelectToken("[?(@.operationId == '6')].returns");
                Assert.AreEqual("loan", (string)commandResponse.SelectToken("path"));
                Assert.AreEqual("Loan", (string)commandResponse.SelectToken("type"));
                Assert.AreEqual("$489.46", (string)commandResponse.SelectToken("balance"));
                Assert.AreEqual("$123.23", (string)commandResponse.SelectToken("payment"));
                Assert.AreEqual("$0.00", (string)commandResponse.SelectToken("paidOff"));
            }
        }

        [Test]
        public void ProcessRequest_GetRealPropertyTotals_ReturnsExpectedValue()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""getRealPropertyTotals"",
                            ""operationId"": ""6"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""type"": ""Loan""
                            }
                        },
                    ]
                }
            }";

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.ConfigurationQuery, FoolHelper.DriverType.SqlQuery, FoolHelper.DriverType.RegularExpression);

                var response = LqbDataService.ProcessRequest(this.loanId, LqbDataServiceOperation.Load, json);
                var responseJson = JObject.Parse(response);

                var expectedResponseToken = responseJson.SelectToken("data.responses");
                Assert.AreEqual(1, expectedResponseToken.Children().Count());
                var commandResponse = expectedResponseToken.SelectToken("[?(@.operationId == '6')].returns");
                Assert.AreEqual("loan", (string)commandResponse.SelectToken("path"));
                Assert.AreEqual("Loan", (string)commandResponse.SelectToken("type"));
                Assert.AreEqual("$1.00", (string)commandResponse.SelectToken("marketValue"));
                Assert.AreEqual("$0.00", (string)commandResponse.SelectToken("mortgageAmount"));
                Assert.AreEqual("$0.00", (string)commandResponse.SelectToken("rentalNetRentalIncome"));
                Assert.AreEqual("$0.00", (string)commandResponse.SelectToken("retainedNetRentalIncome"));
            }
        }

        [Test]
        public void ProcessRequest_GetIncomeSourceTotals_ReturnsExpectedValue()
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""getIncomeSourceTotals"",
                            ""operationId"": ""7"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""type"": ""Loan""
                            }
                        },
                    ]
                }
            }";

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.ConfigurationQuery, FoolHelper.DriverType.SqlQuery, FoolHelper.DriverType.RegularExpression);

                var response = LqbDataService.ProcessRequest(this.loanId, LqbDataServiceOperation.Load, json);
                var responseJson = JObject.Parse(response);

                var expectedResponseToken = responseJson.SelectToken("data.responses");
                Assert.AreEqual(1, expectedResponseToken.Children().Count());
                var commandResponse = expectedResponseToken.SelectToken("[?(@.operationId == '7')].returns");
                Assert.AreEqual("loan", (string)commandResponse.SelectToken("path"));
                Assert.AreEqual("Loan", (string)commandResponse.SelectToken("type"));

                Assert.AreEqual("$2,460.00", (string)commandResponse.SelectToken("monthlyAmount"));
            }
        }

        [Test]
        public void SetPrimaryBorrower_CoborrowerToBePrimary_SwapBorrowersDone()
        {
            Guid localLoanId;
            Guid secondAppId;
            Guid coborrowerId;
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.StoredProcedure, FoolHelper.DriverType.SqlQuery);

                // Create an independent loan so other tests aren't affected
                var creator = CLoanFileCreator.GetCreator(principal, LendersOffice.Audit.E_LoanCreationSource.UserCreateFromBlank);
                localLoanId = creator.CreateBlankUladLoanFile();
                secondAppId = LoanTestUtilities.AddNewLegacyAndUladApplicationToLoan(localLoanId, true);

                var loan = new CPageData(localLoanId, nameof(LqbDataServiceTest), new string[] { "sSyncUladAndLegacyApps", "afDelMarriedCobor", "afSwapMarriedBorAndCobor", "UladApplications", "UladApplicationConsumers" });
                loan.InitLoad();

                var appData = loan.GetAppData(secondAppId);
                coborrowerId = appData.aCConsumerId;

                // Call the service
                var json = @"
                {
                    ""data"": {
                        ""version"": 1739,
                        ""requests"": [
                            {
                                ""name"": ""setPrimaryBorrower"",
                                ""operationId"": ""78"",
                                ""args"": {
                                    ""path"": ""loan"",
                                    ""consumerId"": """ + coborrowerId.ToString("D") + @"""
                                }
                            }
                        ]
                    }
                }";

                LqbDataService.ProcessRequest(localLoanId.ToIdentifier<DataObjectKind.Loan>(), LqbDataServiceOperation.Save, json);

                // Confirm the result
                loan = new CPageData(localLoanId, nameof(LqbDataServiceTest), new string[] { "sSyncUladAndLegacyApps", "afDelMarriedCobor", "afSwapMarriedBorAndCobor", "UladApplications", "UladApplicationConsumers" });
                loan.InitLoad();
                var loanData = CPageBaseExtractor.Extract(loan);
                var aggregate = loanData.loanLqbCollectionContainer;

                var association = aggregate.UladApplicationConsumers.Values.Single(a => a.ConsumerId.Value == coborrowerId);
                Assert.AreEqual(secondAppId, association.LegacyAppId.Value);
                Assert.IsTrue(association.IsPrimary);

                appData = loan.GetAppData(association.LegacyAppId.Value);
                Assert.AreEqual(coborrowerId, appData.aBConsumerId); // coborrower is now borrower on the legacy application
            }
        }

        [Test]
        public void ProcessRequest_AddAssetAndSetAccountNumber_Works()
        {
            using (var loanRef = LoanForIntegrationTest.Create(TestAccountType.LoTest001, useUladDataLayer: true))
            {
                var loanBefore = loanRef.CreateNewPageDataWithBypass();
                loanBefore.InitLoad();
                string json = @"
{
    ""data"": {
        ""version"": 1739,
        ""requests"": [
            {
                ""name"": ""addEntity"",
                ""operationId"": ""1"",
                ""args"": {
                    ""path"": ""loan"",
                    ""collectionName"": ""Assets"",
                    ""ownerId"": """ + loanBefore.GetAppData(0).aBConsumerId + @""",
                    ""fields"": {
                        ""AccountNum"": ""156987""
                    }
                }
            }
        ]
    }
}";
                string responseJson = LqbDataService.ProcessRequest(loanRef.Identifier, LqbDataServiceOperation.Save, json);

                var loanAfter = loanRef.CreateNewPageDataWithBypass();
                loanAfter.InitLoad();
                Assert.AreEqual(1, loanAfter.Assets.Count);
                Assert.AreEqual(BankAccountNumber.Create("156987").Value, loanAfter.Assets.Values.Single().AccountNum);

                var assetId = loanAfter.Assets.Single().Key;
                json = @"
{
    ""data"": {
        ""version"": 1739,
        ""requests"": [
            {
                ""name"": ""set"",
                ""operationId"": ""1"",
                ""args"": {
                    ""loan"": { ""Assets"": { """ + assetId + @""": {
                        ""AccountNum"": ""7147086950""
                    }           }             }
                }
            }
        ]
    }
}";

                responseJson = LqbDataService.ProcessRequest(loanRef.Identifier, LqbDataServiceOperation.Save, json);

                loanAfter = loanRef.CreateNewPageDataWithBypass();
                loanAfter.InitLoad();
                Assert.AreEqual(1, loanAfter.Assets.Count);
                Assert.AreEqual(BankAccountNumber.Create("7147086950").Value, loanAfter.Assets.Values.Single().AccountNum);
            }
        }

        [Test]
        public void ProcessRequest_AddHousingHistoryEntryAndSetAddress_Works()
        {
            using (var loanRef = LoanForIntegrationTest.Create(TestAccountType.LoTest001, useUladDataLayer: true))
            {
                var loanBefore = loanRef.CreateNewPageDataWithBypass();
                loanBefore.InitLoad();
                string json = @"
{
    ""data"": {
        ""version"": 1739,
        ""requests"": [
            {
                ""name"": ""addEntity"",
                ""operationId"": ""1"",
                ""args"": {
                    ""path"": ""loan"",
                    ""collectionName"": ""HousingHistoryEntries"",
                    ""ownerId"": """ + loanBefore.GetAppData(0).aBConsumerId + @""",
                    ""fields"": {
                        ""Address"": {
                            ""$type"": ""UnitedStatesAddress"",
                            ""AddressNumber"": ""1600"",
                            ""StreetName"": ""Sunflower"",
                            ""StreetSuffix"": ""Ave"",
                            ""City"": ""Costa Mesa"",
                            ""UsState"": ""CA"",
                            ""Zipcode"": ""92626""
                        },
                        ""TimeAtAddress"": ""36"",
                        ""StartDate"": ""12/2015"",
                        ""EndDate"": ""12/15/2018"",
                    }
                }
            }
        ]
    }
}";
                string responseJson = LqbDataService.ProcessRequest(loanRef.Identifier, LqbDataServiceOperation.Save, json);

                var loanAfter = loanRef.CreateNewPageDataWithBypass();
                loanAfter.InitLoad();
                Assert.AreEqual(1, loanAfter.HousingHistoryEntries.Count);
                Core.Data.HousingHistoryEntry entry = loanAfter.HousingHistoryEntries.Values.Single();
                Assert.IsInstanceOf(typeof(UnitedStatesPostalAddress), entry.Address);
                var a = (UnitedStatesPostalAddress)entry.Address;
                Assert.AreEqual(CountryCodeIso3.UnitedStates, a.CountryCode);
                Assert.AreEqual(UspsAddressNumber.Create("1600").Value, a.AddressNumber);
                Assert.AreEqual(StreetName.Create("Sunflower").Value, a.StreetName);
                Assert.AreEqual(StreetSuffix.Create("Ave").Value, a.StreetSuffix);
                Assert.AreEqual(City.Create("Costa Mesa").Value, a.City);
                Assert.AreEqual(UnitedStatesPostalState.CreateWithValidation("CA").Value, a.UsState);
                Assert.AreEqual(Zipcode.CreateWithValidation("92626").Value, a.Zipcode);
                Assert.AreEqual((Count<UnitType.Month>)36, entry.TimeAtAddress);
                Assert.AreEqual(ApproximateDate.CreateWithMonthPrecision(2015, 12).Value, entry.StartDate);
                Assert.AreEqual(ApproximateDate.CreateWithDayPrecision(2018, 12, 15).Value, entry.EndDate);

                var id = loanAfter.HousingHistoryEntries.Single().Key;
                json = @"
{
    ""data"": {
        ""version"": 1739,
        ""requests"": [
            {
                ""name"": ""set"",
                ""operationId"": ""1"",
                ""args"": {
                    ""loan"": { ""HousingHistoryEntries"": { """ + id + @""": {
                        ""Address"": {
                            ""AddressNumber"": ""1506"",
                            ""StreetPreDirection"": ""SW"",
                            ""StreetName"": ""Broadway"",
                            ""StreetSuffix"": ""Dr"",
                            ""City"": ""Portland"",
                            ""UsState"": ""OR"",
                            ""Zipcode"": ""97201""
                        },
                        ""TimeAtAddress"": ""51"",
                        ""StartDate"": ""8/2011"",
                        ""EndDate"": ""11/2015"",
                    }           }                              }
                }
            }
        ]
    }
}";

                responseJson = LqbDataService.ProcessRequest(loanRef.Identifier, LqbDataServiceOperation.Save, json);

                loanAfter = loanRef.CreateNewPageDataWithBypass();
                loanAfter.InitLoad();
                Assert.AreEqual(1, loanAfter.HousingHistoryEntries.Count);
                entry = loanAfter.HousingHistoryEntries.Values.Single();
                Assert.IsInstanceOf(typeof(UnitedStatesPostalAddress), entry.Address);
                a = (UnitedStatesPostalAddress)entry.Address;
                Assert.AreEqual(CountryCodeIso3.UnitedStates, a.CountryCode);
                Assert.AreEqual(UspsAddressNumber.Create("1506").Value, a.AddressNumber);
                Assert.AreEqual(UspsDirectional.Create("SW").Value, a.StreetPreDirection);
                Assert.AreEqual(StreetName.Create("Broadway").Value, a.StreetName);
                Assert.AreEqual(StreetSuffix.Create("Dr").Value, a.StreetSuffix);
                Assert.AreEqual(City.Create("Portland").Value, a.City);
                Assert.AreEqual(UnitedStatesPostalState.CreateWithValidation("OR").Value, a.UsState);
                Assert.AreEqual(Zipcode.CreateWithValidation("97201").Value, a.Zipcode);
                Assert.AreEqual((Count<UnitType.Month>)51, entry.TimeAtAddress);
                Assert.AreEqual(ApproximateDate.CreateWithMonthPrecision(2011, 8).Value, entry.StartDate);
                Assert.AreEqual(ApproximateDate.CreateWithMonthPrecision(2015, 11).Value, entry.EndDate);

                // Test partial update
                json = @"
{
    ""data"": {
        ""version"": 1739,
        ""requests"": [
            {
                ""name"": ""set"",
                ""operationId"": ""1"",
                ""args"": {
                    ""loan"": { ""HousingHistoryEntries"": { """ + id + @""": {
                        ""Address"": {
                            ""City"": ""Schenectady"",
                            ""UsState"": ""NY"",
                            ""Zipcode"": ""12345"",
                            ""StreetPostDirection"": ""NE"",
                        },
                    }           }                              }
                }
            }
        ]
    }
}";
                responseJson = LqbDataService.ProcessRequest(loanRef.Identifier, LqbDataServiceOperation.Save, json);

                loanAfter = loanRef.CreateNewPageDataWithBypass();
                loanAfter.InitLoad();
                Assert.AreEqual(1, loanAfter.HousingHistoryEntries.Count);
                entry = loanAfter.HousingHistoryEntries.Values.Single();
                Assert.IsInstanceOf(typeof(UnitedStatesPostalAddress), entry.Address);
                a = (UnitedStatesPostalAddress)entry.Address;
                Assert.AreEqual(CountryCodeIso3.UnitedStates, a.CountryCode);
                Assert.AreEqual(UspsAddressNumber.Create("1506").Value, a.AddressNumber);
                Assert.AreEqual(UspsDirectional.Create("SW").Value, a.StreetPreDirection);
                Assert.AreEqual(StreetName.Create("Broadway").Value, a.StreetName);
                Assert.AreEqual(UspsDirectional.Create("NE").Value, a.StreetPostDirection);
                Assert.AreEqual(StreetSuffix.Create("Dr").Value, a.StreetSuffix);
                Assert.AreEqual(City.Create("Schenectady").Value, a.City);
                Assert.AreEqual(UnitedStatesPostalState.CreateWithValidation("NY").Value, a.UsState);
                Assert.AreEqual(Zipcode.CreateWithValidation("12345").Value, a.Zipcode);
                Assert.AreEqual((Count<UnitType.Month>)51, entry.TimeAtAddress);
                Assert.AreEqual(ApproximateDate.CreateWithMonthPrecision(2011, 8).Value, entry.StartDate);
                Assert.AreEqual(ApproximateDate.CreateWithMonthPrecision(2015, 11).Value, entry.EndDate);

                // Setting a not supported property doesn't work
                json = @"
{
    ""data"": {
        ""version"": 1739,
        ""requests"": [
            {
                ""name"": ""set"",
                ""operationId"": ""1"",
                ""args"": {
                    ""loan"": { ""HousingHistoryEntries"": { """ + id + @""": {
                        ""Address"": {
                            ""State"": ""CA""
                        }
                    }           }                              }
                }
            }
        ]
    }
}";
                Assert.Throws<ArgumentException>(() => LqbDataService.ProcessRequest(loanRef.Identifier, LqbDataServiceOperation.Save, json));

                loanAfter = loanRef.CreateNewPageDataWithBypass();
                loanAfter.InitLoad();
                Assert.AreEqual(1, loanAfter.HousingHistoryEntries.Count);
                entry = loanAfter.HousingHistoryEntries.Values.Single();
                Assert.IsInstanceOf(typeof(UnitedStatesPostalAddress), entry.Address);
                a = (UnitedStatesPostalAddress)entry.Address;
                Assert.AreEqual(UnitedStatesPostalState.CreateWithValidation("NY").Value, a.UsState);

                // Test address type update results in new instance
                json = @"
{
    ""data"": {
        ""version"": 1739,
        ""requests"": [
            {
                ""name"": ""set"",
                ""operationId"": ""1"",
                ""args"": {
                    ""loan"": { ""HousingHistoryEntries"": { """ + id + @""": {
                        ""Address"": {
                            ""$type"": ""GeneralAddress"",
                            ""StreetAddress"": ""123 Main Street"",
                        }
                    }           }                              }
                }
            }
        ]
    }
}";
                responseJson = LqbDataService.ProcessRequest(loanRef.Identifier, LqbDataServiceOperation.Save, json);

                loanAfter = loanRef.CreateNewPageDataWithBypass();
                loanAfter.InitLoad();
                Assert.AreEqual(1, loanAfter.HousingHistoryEntries.Count);
                entry = loanAfter.HousingHistoryEntries.Values.Single();
                Assert.IsInstanceOf(typeof(GeneralPostalAddress), entry.Address);
                var ga = (GeneralPostalAddress)entry.Address;
                Assert.AreEqual(StreetAddress.Create("123 Main Street").Value, ga.StreetAddress);
                Assert.AreEqual(null, ga.City);
                Assert.AreEqual(null, ga.State);
                Assert.AreEqual(null, ga.PostalCode);
                Assert.AreEqual(null, ga.CountryCode);
                Assert.AreEqual((Count<UnitType.Month>)51, entry.TimeAtAddress);
                Assert.AreEqual(ApproximateDate.CreateWithMonthPrecision(2011, 8).Value, entry.StartDate);
                Assert.AreEqual(ApproximateDate.CreateWithMonthPrecision(2015, 11).Value, entry.EndDate);

                // test clearing the address
                json = @"
{
    ""data"": {
        ""version"": 1739,
        ""requests"": [
            {
                ""name"": ""set"",
                ""operationId"": ""1"",
                ""args"": {
                    ""loan"": { ""HousingHistoryEntries"": { """ + id + @""": {
                        ""Address"": null
                    }           }                              }
                }
            }
        ]
    }
}";
                responseJson = LqbDataService.ProcessRequest(loanRef.Identifier, LqbDataServiceOperation.Save, json);

                loanAfter = loanRef.CreateNewPageDataWithBypass();
                loanAfter.InitLoad();
                Assert.AreEqual(1, loanAfter.HousingHistoryEntries.Count);
                entry = loanAfter.HousingHistoryEntries.Values.Single();
                Assert.IsNull(entry.Address);

                // test setting the address from null
                json = @"
{
    ""data"": {
        ""version"": 1739,
        ""requests"": [
            {
                ""name"": ""set"",
                ""operationId"": ""1"",
                ""args"": {
                    ""loan"": { ""HousingHistoryEntries"": { """ + id + @""": {
                        ""Address"": {
                            ""$type"": ""UnitedStatesAddress"",
                        }
                    }           }                              }
                }
            }
        ]
    }
}";
                responseJson = LqbDataService.ProcessRequest(loanRef.Identifier, LqbDataServiceOperation.Save, json);

                loanAfter = loanRef.CreateNewPageDataWithBypass();
                loanAfter.InitLoad();
                Assert.AreEqual(1, loanAfter.HousingHistoryEntries.Count);
                entry = loanAfter.HousingHistoryEntries.Values.Single();
                Assert.IsInstanceOf<UnitedStatesPostalAddress>(entry.Address);
                Assert.AreEqual(null, entry.Address.StreetAddress);
                Assert.AreEqual(null, entry.Address.City);
                Assert.AreEqual(null, entry.Address.State);
                Assert.AreEqual(null, entry.Address.PostalCode);
                Assert.AreEqual(CountryCodeIso3.UnitedStates, entry.Address.CountryCode);
            }
        }

        [Test]
        public void ProcessRequest_GetHousingHistoryEntryWithAddress_Works()
        {
            using (var loanRef = LoanForIntegrationTest.Create(TestAccountType.LoTest001, useUladDataLayer: true))
            {
                CPageData loanBefore = loanRef.CreateNewPageDataWithBypass();
                loanBefore.InitSave();
                CAppData appBefore = loanBefore.GetAppData(0);
                var entry1Id = loanBefore.AddHousingHistoryEntry(appBefore.aBConsumerIdentifier, new Core.Data.HousingHistoryEntry
                {
                    TimeAtAddress = (Count<UnitType.Month>)63,
                    IsPresentAddress = true,
                    StartDate = ApproximateDate.CreateWithDayPrecision(2013, 09, 03).Value,
                    Address = (new UnitedStatesPostalAddress.Builder()
                    {
                        AddressNumber = UspsAddressNumber.Create("1600").Value,
                        StreetName = StreetName.Create("Sunflower").Value,
                        StreetSuffix = StreetSuffix.Create("Ave").Value,
                        City = City.Create("Costa Mesa").Value,
                        UsState = UnitedStatesPostalState.CreateWithValidation("CA").Value,
                        Zipcode = Zipcode.CreateWithValidation("92626").Value,
                    }).GetAddress(),
                });
                var entry2Id = loanBefore.AddHousingHistoryEntry(appBefore.aBConsumerIdentifier, new Core.Data.HousingHistoryEntry
                {
                    TimeAtAddress = (Count<UnitType.Month>)2,
                    IsPresentAddress = false,
                    StartDate = ApproximateDate.CreateWithYearPrecision(2013).Value,
                    EndDate = ApproximateDate.CreateWithMonthPrecision(2013, 09).Value,
                    Address = (new GeneralPostalAddress.Builder()
                    {
                        StreetAddress = StreetAddress.Create("5 Rowe Avenue").Value,
                        City = City.Create("Rivervale").Value,
                        State = AdministrativeArea.Create("WA").Value,
                        PostalCode = PostalCode.Create("6103").Value,
                        CountryCode = CountryCodeIso3.Australia,
                    }).GetAddress(),
                });
                var entry3Id = loanBefore.AddHousingHistoryEntry(appBefore.aBConsumerIdentifier, new Core.Data.HousingHistoryEntry
                {
                    IsPresentAddress = false,
                    Address = null,
                });
                loanBefore.Save();

                string json = @"
{
    ""data"": {
        ""version"": 1739,
        ""requests"": [
            {
                ""name"": ""get"",
                ""operationId"": ""2"",
                ""args"": {
                    ""loan"": { ""HousingHistoryEntries"": { ""any"": {
                        ""Address"": null,
                        ""IsPresentAddress"": null,
                        ""TimeAtAddress"": null,
                        ""StartDate"": null,
                        ""EndDate"": null,
                    }           }                            }
                }
            }
        ]
    }
}";
                string responseJson = LqbDataService.ProcessRequest(loanRef.Identifier, LqbDataServiceOperation.Load, json);
                JObject response = JObject.Parse(responseJson);

                JObject housingHistoryJson = (JObject)response.SelectToken("data.responses[?(@.name == 'get')].returns.loan.HousingHistoryEntries");
                CollectionAssert.AreEquivalent(new[] { entry1Id.ToString(), entry2Id.ToString(), entry3Id.ToString(), }, housingHistoryJson.Properties().Select(p => p.Name));
                JObject entry1Json = (JObject)housingHistoryJson[entry1Id.ToString()];
                Assert.AreEqual("63", (string)entry1Json["TimeAtAddress"]);
                Assert.AreEqual("True", (string)entry1Json["IsPresentAddress"]);
                Assert.AreEqual("9/3/2013", (string)entry1Json["StartDate"]);
                Assert.AreEqual(string.Empty, (string)entry1Json["EndDate"]);
                Assert.AreEqual("UnitedStatesAddress", (string)entry1Json["Address"]["$type"]);
                Assert.AreEqual("1600", (string)entry1Json["Address"]["AddressNumber"]);
                Assert.AreEqual("Sunflower", (string)entry1Json["Address"]["StreetName"]);
                Assert.AreEqual("Ave", (string)entry1Json["Address"]["StreetSuffix"]);
                Assert.AreEqual("1600 Sunflower Ave", (string)entry1Json["Address"]["StreetAddress"]);
                Assert.AreEqual(string.Empty, (string)entry1Json["Address"]["UnparsedStreetAddress"]);
                Assert.AreEqual("Costa Mesa", (string)entry1Json["Address"]["City"]);
                Assert.AreEqual("CA", (string)entry1Json["Address"]["UsState"]);
                Assert.AreEqual("92626", (string)entry1Json["Address"]["Zipcode"]);
                Assert.AreEqual("USA", (string)entry1Json["Address"]["CountryCode"]);
                Assert.AreEqual("United States", (string)entry1Json["Address"]["Country"]);
                JObject entry2Json = (JObject)housingHistoryJson[entry2Id.ToString()];
                Assert.AreEqual("2", (string)entry2Json["TimeAtAddress"]);
                Assert.AreEqual("False", (string)entry2Json["IsPresentAddress"]);
                Assert.AreEqual("2013", (string)entry2Json["StartDate"]);
                Assert.AreEqual("9/2013", (string)entry2Json["EndDate"]);
                Assert.AreEqual("GeneralAddress", (string)entry2Json["Address"]["$type"]);
                Assert.AreEqual("5 Rowe Avenue", (string)entry2Json["Address"]["StreetAddress"]);
                Assert.AreEqual("Rivervale", (string)entry2Json["Address"]["City"]);
                Assert.AreEqual("WA", (string)entry2Json["Address"]["State"]);
                Assert.AreEqual("6103", (string)entry2Json["Address"]["PostalCode"]);
                Assert.AreEqual("AUS", (string)entry2Json["Address"]["CountryCode"]);
                Assert.AreEqual("Australia", (string)entry2Json["Address"]["Country"]);
                JObject entry3Json = (JObject)housingHistoryJson[entry3Id.ToString()];
                Assert.AreEqual("False", (string)entry3Json["IsPresentAddress"]);
                Assert.AreEqual(JValue.CreateNull(), entry3Json["Address"]);

                // Querying specific fields works by chance, but only if done on a non-null instance.
                // There isn't a way to get type specific fields on a mixed set of entries.
                loanBefore = loanRef.CreateNewPageDataWithBypass();
                loanBefore.InitSave();
                loanBefore.RemoveHousingHistoryEntry(entry3Id);
                loanBefore.Save();
                json = @"
{
    ""data"": {
        ""version"": 1739,
        ""requests"": [
            {
                ""name"": ""get"",
                ""operationId"": ""2"",
                ""args"": {
                    ""loan"": { ""HousingHistoryEntries"": { ""any"": {
                        ""Address"": {
                            ""$type"": null,
                            ""StreetAddress"": null,
                            ""City"": null,
                            ""State"": null,
                        }
                    }           }                            }
                }
            }
        ]
    }
}";
                responseJson = LqbDataService.ProcessRequest(loanRef.Identifier, LqbDataServiceOperation.Load, json);
                response = JObject.Parse(responseJson);

                housingHistoryJson = (JObject)response.SelectToken("data.responses[?(@.name == 'get')].returns.loan.HousingHistoryEntries");
                entry1Json = (JObject)housingHistoryJson[entry1Id.ToString()];
                Assert.AreEqual("UnitedStatesAddress", (string)entry1Json["Address"]["$type"]);
                Assert.AreEqual(null, (string)entry1Json["Address"]["AddressNumber"]);
                Assert.AreEqual(null, (string)entry1Json["Address"]["StreetName"]);
                Assert.AreEqual(null, (string)entry1Json["Address"]["StreetSuffix"]);
                Assert.AreEqual("1600 Sunflower Ave", (string)entry1Json["Address"]["StreetAddress"]);
                Assert.AreEqual(null, (string)entry1Json["Address"]["UnparsedStreetAddress"]);
                Assert.AreEqual("Costa Mesa", (string)entry1Json["Address"]["City"]);
                Assert.AreEqual("CA", (string)entry1Json["Address"]["State"]);
                Assert.AreEqual(null, (string)entry1Json["Address"]["UsState"]);
                Assert.AreEqual(null, (string)entry1Json["Address"]["Zipcode"]);
                Assert.AreEqual(null, (string)entry1Json["Address"]["PostalCode"]);
                Assert.AreEqual(null, (string)entry1Json["Address"]["CountryCode"]);
                Assert.AreEqual(null, (string)entry1Json["Address"]["Country"]);
                entry2Json = (JObject)housingHistoryJson[entry2Id.ToString()];
                Assert.AreEqual("GeneralAddress", (string)entry2Json["Address"]["$type"]);
                Assert.AreEqual("5 Rowe Avenue", (string)entry2Json["Address"]["StreetAddress"]);
                Assert.AreEqual("Rivervale", (string)entry2Json["Address"]["City"]);
                Assert.AreEqual("WA", (string)entry2Json["Address"]["State"]);
                Assert.AreEqual(null, (string)entry2Json["Address"]["PostalCode"]);
                Assert.AreEqual(null, (string)entry2Json["Address"]["CountryCode"]);
                Assert.AreEqual(null, (string)entry2Json["Address"]["Country"]);
            }
        }
        [Test]
        public void ProcessRequest_GetAndSetMailingAddressAndPostClosingAddress_Works()
        {
            using (var loanRef = LoanForIntegrationTest.Create(TestAccountType.LoTest001, useUladDataLayer: true))
            {
                CPageData loanBefore = loanRef.CreateNewPageDataWithBypass();
                loanBefore.InitSave();
                CAppData appBefore = loanBefore.GetAppData(0);
                loanBefore.AddCoborrowerToLegacyApplication(appBefore.aAppIdentifier);
                const string subjectPropertyAddr = "4713 Anapaula Ct";
                const string subjectPropertyCity = "Green Bay";
                const string subjectPropertyState = "WI";
                const string subjectPropertyZip = "54311";
                loanBefore.sSpAddr = subjectPropertyAddr;
                loanBefore.sSpCity = subjectPropertyCity;
                loanBefore.sSpState = subjectPropertyState;
                loanBefore.sSpZip = subjectPropertyZip;
                loanBefore.Save();
                Assert.IsNull(appBefore.aBMailingAddress);
                Assert.IsNull(appBefore.aCMailingAddress);
                Assert.IsNull(appBefore.aBPostClosingAddress);
                Assert.IsNull(appBefore.aCPostClosingAddress);
                string json = @"
{
    ""data"": {
        ""version"": 1739,
        ""requests"": [
            {
                ""name"": ""set"",
                ""operationId"": ""1"",
                ""args"": {
                    ""loan"": { ""Consumers"": {
                        """ + appBefore.aBConsumerId + @""": {
                            ""MailingAddressSourceType"": ""2"",
                            ""MailingAddressData"": {
                                ""$type"": ""GeneralAddress"",
                                ""StreetAddress"": ""3402 Agate Pl"",
                                ""City"": ""Coquitlam"",
                                ""State"": ""BC"",
                                ""PostalCode"": ""V3E 3A3"",
                                ""CountryCode"": ""CAN""
                            },
                            ""PostClosingAddressSourceTypeLocked"": true,
                            ""PostClosingAddressSourceType"": ""3"",
                            ""PostClosingAddressData"": {
                                ""$type"": ""UnitedStatesAddress"",
                                ""AddressNumber"": ""2080"",
                                ""StreetPreDirection"": ""W"",
                                ""StreetName"": ""Muirwood"",
                                ""StreetSuffix"": ""Dr"",
                                ""City"": ""Green Bay"",
                                ""UsState"": ""WI"",
                                ""Zipcode"": ""54313""
                            }
                        },
                        """ + appBefore.aCConsumerId + @""": {
                            ""MailingAddressSourceType"": ""1"",
                            ""PostClosingAddressSourceTypeLocked"": true,
                            ""PostClosingAddressSourceType"": ""0"",
                        }
                    }           }
                }
            }
        ]
    }
}";
                string responseJson = LqbDataService.ProcessRequest(loanRef.Identifier, LqbDataServiceOperation.Save, json);

                var loanAfter1 = loanRef.CreateNewPageDataWithBypass();
                loanAfter1.InitLoad();
                CAppData appAfter1 = loanAfter1.GetAppData(0);
                Assert.AreEqual(E_aAddrMailSourceT.Other, appAfter1.aBAddrMailSourceT);
                var borrMailingAddress = appAfter1.aBMailingAddress;
                Assert.IsInstanceOf<GeneralPostalAddress>(borrMailingAddress);
                Assert.AreEqual("3402 Agate Pl", borrMailingAddress.StreetAddress.ToString());
                Assert.AreEqual("Coquitlam", borrMailingAddress.City.ToString());
                Assert.AreEqual("BC", borrMailingAddress.State.ToString());
                Assert.AreEqual("V3E 3A3", borrMailingAddress.PostalCode.ToString());
                Assert.AreEqual(CountryCodeIso3.Canada, borrMailingAddress.CountryCode);
                Assert.AreEqual(E_aAddrPostSourceT.Other, appAfter1.aBAddrPostSourceT);
                var borrPostClosingAddress = appAfter1.aBPostClosingAddress;
                Assert.IsInstanceOf<UnitedStatesPostalAddress>(borrPostClosingAddress);
                var borrPostClosingUsAddress = (UnitedStatesPostalAddress)borrPostClosingAddress;
                Assert.AreEqual("2080", borrPostClosingUsAddress.AddressNumber.ToString());
                Assert.AreEqual("W", borrPostClosingUsAddress.StreetPreDirection.ToString());
                Assert.AreEqual("Muirwood", borrPostClosingUsAddress.StreetName.ToString());
                Assert.AreEqual("Dr", borrPostClosingUsAddress.StreetSuffix.ToString());
                Assert.AreEqual("2080 W Muirwood Dr", borrPostClosingAddress.StreetAddress.ToString());
                Assert.AreEqual("Green Bay", borrPostClosingAddress.City.ToString());
                Assert.AreEqual("WI", borrPostClosingAddress.State.ToString());
                Assert.AreEqual("54313", borrPostClosingAddress.PostalCode.ToString());
                Assert.AreEqual(CountryCodeIso3.UnitedStates, borrPostClosingAddress.CountryCode);
                Assert.AreEqual(E_aAddrMailSourceT.SubjectPropertyAddress, appAfter1.aCAddrMailSourceT);
                var coboMailingAddress = appAfter1.aCMailingAddress;
                Assert.IsInstanceOf<UnitedStatesPostalAddress>(coboMailingAddress);
                Assert.AreEqual(subjectPropertyAddr, coboMailingAddress.StreetAddress.ToString());
                Assert.AreEqual(subjectPropertyCity, coboMailingAddress.City.ToString());
                Assert.AreEqual(subjectPropertyState, coboMailingAddress.State.ToString());
                Assert.AreEqual(subjectPropertyZip, coboMailingAddress.PostalCode.ToString());
                Assert.AreEqual(CountryCodeIso3.UnitedStates, coboMailingAddress.CountryCode);
                Assert.AreEqual(E_aAddrPostSourceT.PresentAddress, appAfter1.aCAddrPostSourceT);
                var coboPostClosingAddress = appAfter1.aCPostClosingAddress;
                Assert.IsNull(coboPostClosingAddress);

                // GET
                json = @"
{
    ""data"": {
        ""version"": 1739,
        ""requests"": [
            {
                ""name"": ""get"",
                ""operationId"": ""1"",
                ""args"": {
                    ""loan"": { ""Consumers"": { ""ANY"": {
                        ""MailingAddressSourceType"": null,
                        ""MailingAddress"": null,
                        ""MailingAddressData"": null,
                        ""PostClosingAddressSourceTypeLocked"": null,
                        ""PostClosingAddressSourceType"": null,
                        ""PostClosingAddress"": null,
                        ""PostClosingAddressData"": null
                    }           }                }
                }
            }
        ]
    }
}";
                responseJson = LqbDataService.ProcessRequest(loanRef.Identifier, LqbDataServiceOperation.Load, json);
                JObject response = JObject.Parse(responseJson);

                JObject consumersJson = (JObject)response.SelectToken("data.responses[?(@.name == 'get')].returns.loan.Consumers");
                JObject borrJson = (JObject)consumersJson[appBefore.aBConsumerId.ToString()];
                Assert.AreEqual("2", (string)borrJson["MailingAddressSourceType"]);
                JObject borrMailingAddressJson = (JObject)borrJson["MailingAddress"];
                Assert.AreEqual("GeneralAddress", (string)borrMailingAddressJson["$type"]);
                Assert.AreEqual("3402 Agate Pl", (string)borrMailingAddressJson["StreetAddress"]);
                Assert.AreEqual("Coquitlam", (string)borrMailingAddressJson["City"]);
                Assert.AreEqual("BC", (string)borrMailingAddressJson["State"]);
                Assert.AreEqual("V3E 3A3", (string)borrMailingAddressJson["PostalCode"]);
                Assert.AreEqual("CAN", (string)borrMailingAddressJson["CountryCode"]);
                Assert.IsTrue(JToken.DeepEquals(borrMailingAddressJson, (JObject)borrJson["MailingAddressData"]), "Expecting mailing address and mailing address data to be equal");

                Assert.AreEqual("True", (string)borrJson["PostClosingAddressSourceTypeLocked"]);
                Assert.AreEqual("3", (string)borrJson["PostClosingAddressSourceType"]);
                JObject borrPostClosingAddressJson = (JObject)borrJson["PostClosingAddress"];
                Assert.AreEqual("UnitedStatesAddress", (string)borrPostClosingAddressJson["$type"]);
                Assert.AreEqual("2080", (string)borrPostClosingAddressJson["AddressNumber"]);
                Assert.AreEqual("W", (string)borrPostClosingAddressJson["StreetPreDirection"]);
                Assert.AreEqual("Muirwood", (string)borrPostClosingAddressJson["StreetName"]);
                Assert.AreEqual("Dr", (string)borrPostClosingAddressJson["StreetSuffix"]);
                Assert.AreEqual("2080 W Muirwood Dr", (string)borrPostClosingAddressJson["StreetAddress"]);
                Assert.AreEqual(string.Empty, (string)borrPostClosingAddressJson["UnparsedStreetAddress"]);
                Assert.AreEqual("Green Bay", (string)borrPostClosingAddressJson["City"]);
                Assert.AreEqual("WI", (string)borrPostClosingAddressJson["UsState"]);
                Assert.AreEqual("54313", (string)borrPostClosingAddressJson["Zipcode"]);
                Assert.AreEqual("USA", (string)borrPostClosingAddressJson["CountryCode"]);
                Assert.AreEqual("United States", (string)borrPostClosingAddressJson["Country"]);
                Assert.IsTrue(JToken.DeepEquals(borrPostClosingAddressJson, (JObject)borrJson["PostClosingAddressData"]), "Expecting post-closing address and post-closing address data to be equal");

                JObject coboJson = (JObject)consumersJson[appBefore.aCConsumerId.ToString()];
                Assert.AreEqual("1", (string)coboJson["MailingAddressSourceType"]);
                JObject coboMailingAddressJson = (JObject)coboJson["MailingAddress"];
                Assert.AreEqual("UnitedStatesAddress", (string)coboMailingAddressJson["$type"]);
                Assert.AreEqual(string.Empty, (string)coboMailingAddressJson["AddressNumber"]);
                Assert.AreEqual(string.Empty, (string)coboMailingAddressJson["StreetPreDirection"]);
                Assert.AreEqual(string.Empty, (string)coboMailingAddressJson["StreetName"]);
                Assert.AreEqual(string.Empty, (string)coboMailingAddressJson["StreetSuffix"]);
                Assert.AreEqual(subjectPropertyAddr, (string)coboMailingAddressJson["StreetAddress"]);
                Assert.AreEqual(subjectPropertyAddr, (string)coboMailingAddressJson["UnparsedStreetAddress"]);
                Assert.AreEqual(subjectPropertyCity, (string)coboMailingAddressJson["City"]);
                Assert.AreEqual(subjectPropertyState, (string)coboMailingAddressJson["UsState"]);
                Assert.AreEqual(subjectPropertyZip, (string)coboMailingAddressJson["Zipcode"]);
                Assert.AreEqual("USA", (string)coboMailingAddressJson["CountryCode"]);
                Assert.AreEqual("United States", (string)coboMailingAddressJson["Country"]);

                Assert.AreEqual("True", (string)coboJson["PostClosingAddressSourceTypeLocked"]);
                Assert.AreEqual("0", (string)coboJson["PostClosingAddressSourceType"]);
                Assert.AreEqual(JValue.CreateNull(), coboJson["PostClosingAddress"]);
            }
        }

        private void RemoveAsset(Guid assetId)
        {
            var json = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""removeEntity"",
                            ""operationId"": ""78"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""collectionName"": ""Assets"",
                                ""id"": """ + assetId.ToString("D") + @"""
                            }
                        }
                    ]
                }
            }";

            var responseJson = LqbDataService.ProcessRequest(this.loanId, LqbDataServiceOperation.Save, json);
        }

        private Tuple<Guid, Guid, Guid> AddAssociatedLiablityAndRealProperty()
        {
            var json1 = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""addEntity"",
                            ""operationId"": ""79"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""collectionName"": ""Liabilities"",
                                ""ownerId"": """ + this.consumer1Id.ToString() + @""",
                                ""id"": null
                            }
                        }
                    ]
                }
            }";

            var json2 = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""addEntity"",
                            ""operationId"": ""80"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""collectionName"": ""RealProperties"",
                                ""ownerId"": """ + this.consumer1Id.ToString() + @""",
                                ""id"": null
                            }
                        }
                    ]
                }
            }";

            var json3Template = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""addAssociation"",
                            ""operationId"": ""81"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""associationSet"": ""RealPropertyLiabilities"",
                                ""firstId"": ""PROPERTY_ID_HERE"",
                                ""secondId"": ""LIABILITY_ID_HERE"",
                                ""id"": null
                            }
                        }
                    ]
                }
            }";

            var responseJson = LqbDataService.ProcessRequest(this.loanId, LqbDataServiceOperation.Save, json1);

            var response = JObject.Parse(responseJson);
            var id = (string)response.SelectToken("data.responses[?(@.name == 'addEntity')].returns.id");
            Assert.AreEqual("79", (string)response.SelectToken("data.responses[?(@.name == 'addEntity')].operationId"));
            Assert.IsNotNullOrEmpty(id);

            Guid liabilityId = Guid.Parse(id);

            responseJson = LqbDataService.ProcessRequest(this.loanId, LqbDataServiceOperation.Save, json2);

            response = JObject.Parse(responseJson);
            id = (string)response.SelectToken("data.responses[?(@.name == 'addEntity')].returns.id");
            Assert.AreEqual("80", (string)response.SelectToken("data.responses[?(@.name == 'addEntity')].operationId"));
            Assert.IsNotNullOrEmpty(id);

            Guid propertyId = Guid.Parse(id);

            string json3 = json3Template.Replace("PROPERTY_ID_HERE", propertyId.ToString("D")).Replace("LIABILITY_ID_HERE", liabilityId.ToString("D"));
            responseJson = LqbDataService.ProcessRequest(this.loanId, LqbDataServiceOperation.Save, json3);

            response = JObject.Parse(responseJson);
            id = (string)response.SelectToken("data.responses[?(@.name == 'addAssociation')].returns.id");
            Assert.AreEqual("81", (string)response.SelectToken("data.responses[?(@.name == 'addAssociation')].operationId"));
            Assert.IsNotNullOrEmpty(id);

            Guid associationId = Guid.Parse(id);

            return new Tuple<Guid, Guid, Guid>(liabilityId, propertyId, associationId);
        }

        private Tuple<Guid, Guid, Guid> AddAssociatedIncomeSourceAndEmploymentRecord()
        {
            var json1 = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""addEntity"",
                            ""operationId"": ""79"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""collectionName"": ""EmploymentRecords"",
                                ""ownerId"": """ + this.consumer1Id.ToString() + @""",
                                ""id"": null
                            }
                        }
                    ]
                }
            }";

            var json2 = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""addEntity"",
                            ""operationId"": ""80"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""collectionName"": ""IncomeSources"",
                                ""ownerId"": """ + this.consumer1Id.ToString() + @""",
                                ""id"": null
                            }
                        }
                    ]
                }
            }";

            var json3Template = @"
            {
                ""data"": {
                    ""version"": 1739,
                    ""requests"": [
                        {
                            ""name"": ""addAssociation"",
                            ""operationId"": ""81"",
                            ""args"": {
                                ""path"": ""loan"",
                                ""associationSet"": ""IncomeSourceEmploymentRecords"",
                                ""firstId"": ""INCOME_ID_HERE"",
                                ""secondId"": ""EMPLOYMENT_ID_HERE"",
                                ""id"": null
                            }
                        }
                    ]
                }
            }";

            var responseJson = LqbDataService.ProcessRequest(this.loanId, LqbDataServiceOperation.Save, json1);

            var response = JObject.Parse(responseJson);
            var id = (string)response.SelectToken("data.responses[?(@.name == 'addEntity')].returns.id");
            Assert.AreEqual("79", (string)response.SelectToken("data.responses[?(@.name == 'addEntity')].operationId"));
            Assert.IsNotNullOrEmpty(id);

            Guid employmentId = Guid.Parse(id);

            responseJson = LqbDataService.ProcessRequest(this.loanId, LqbDataServiceOperation.Save, json2);

            response = JObject.Parse(responseJson);
            id = (string)response.SelectToken("data.responses[?(@.name == 'addEntity')].returns.id");
            Assert.AreEqual("80", (string)response.SelectToken("data.responses[?(@.name == 'addEntity')].operationId"));
            Assert.IsNotNullOrEmpty(id);

            Guid incomeId = Guid.Parse(id);

            string json3 = json3Template.Replace("INCOME_ID_HERE", incomeId.ToString("D")).Replace("EMPLOYMENT_ID_HERE", employmentId.ToString("D"));
            responseJson = LqbDataService.ProcessRequest(this.loanId, LqbDataServiceOperation.Save, json3);

            response = JObject.Parse(responseJson);
            id = (string)response.SelectToken("data.responses[?(@.name == 'addAssociation')].returns.id");
            Assert.AreEqual("81", (string)response.SelectToken("data.responses[?(@.name == 'addAssociation')].operationId"));
            Assert.IsNotNullOrEmpty(id);

            Guid associationId = Guid.Parse(id);

            return new Tuple<Guid, Guid, Guid>(incomeId, employmentId, associationId);
        }
    }
}
