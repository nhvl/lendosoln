﻿namespace LendingQB.Test.Developers.Adapter.Drivers
{
    using System.Linq;
    using global::Adapter.Drivers;
    using NUnit.Framework;
    using Utils;

    [TestFixture]
    [Category(CategoryConstants.IntegrationTest)]
    public class ConnectionStringProviderHelperTest
    {
        [Test]
        public void LoadConnectionStrings()
        {
            // NOTE: I ran this against an XML file that has the encryption key and IV encrypted and in plain text.
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.Bootstrap, FoolHelper.DriverType.StoredProcedure);

                var connectionStrings = ConnectionStringProviderHelper.ListAllMainConnectionStrings(false);
                Assert.IsTrue(connectionStrings.Count() > 0);
            }
        }
    }
}
