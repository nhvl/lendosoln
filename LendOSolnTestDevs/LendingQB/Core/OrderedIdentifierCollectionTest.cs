﻿namespace LendingQB.Test.Developers.LendingQB.Core
{
    using System;
    using global::DataAccess;
    using LqbGrammar.DataTypes;
    using global::LendingQB.Core;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public sealed class OrderedIdentifierCollectionTest
    {
        private DataObjectIdentifier<DataObjectKind.Asset, Guid> id0;
        private DataObjectIdentifier<DataObjectKind.Asset, Guid> id1;
        private DataObjectIdentifier<DataObjectKind.Asset, Guid> id2;
        private DataObjectIdentifier<DataObjectKind.Asset, Guid> id3;
        private DataObjectIdentifier<DataObjectKind.Asset, Guid> id4;

        public OrderedIdentifierCollectionTest()
        {
            var factory = new GuidDataObjectIdentifierFactory<DataObjectKind.Asset>();
            this.id0 = factory.NewId();
            this.id1 = factory.NewId();
            this.id2 = factory.NewId();
            this.id3 = factory.NewId();
            this.id4 = factory.NewId();
        }

        [Test]
        public void DifferentIdentifiers()
        {
            Assert.AreNotEqual(this.id0, this.id1);
            Assert.AreNotEqual(this.id0, this.id2);
            Assert.AreNotEqual(this.id0, this.id3);
            Assert.AreNotEqual(this.id0, this.id4);

            Assert.AreNotEqual(this.id1, this.id0);
            Assert.AreNotEqual(this.id1, this.id2);
            Assert.AreNotEqual(this.id1, this.id3);
            Assert.AreNotEqual(this.id1, this.id4);

            Assert.AreNotEqual(this.id2, this.id0);
            Assert.AreNotEqual(this.id2, this.id1);
            Assert.AreNotEqual(this.id2, this.id3);
            Assert.AreNotEqual(this.id2, this.id4);

            Assert.AreNotEqual(this.id3, this.id0);
            Assert.AreNotEqual(this.id3, this.id1);
            Assert.AreNotEqual(this.id3, this.id2);
            Assert.AreNotEqual(this.id3, this.id4);

            Assert.AreNotEqual(this.id4, this.id0);
            Assert.AreNotEqual(this.id4, this.id1);
            Assert.AreNotEqual(this.id4, this.id2);
            Assert.AreNotEqual(this.id4, this.id3);
        }

        [Test]
        public void GetAtPosition()
        {
            var collection = this.InitializeCollectionOrder();
            var at0 = collection.GetIdentifierAt(0);
            var at1 = collection.GetIdentifierAt(1);
            var at2 = collection.GetIdentifierAt(2);
            var at3 = collection.GetIdentifierAt(3);
            var at4 = collection.GetIdentifierAt(4);

            Assert.AreEqual(this.id0, at0);
            Assert.AreEqual(this.id1, at1);
            Assert.AreEqual(this.id2, at2);
            Assert.AreEqual(this.id3, at3);
            Assert.AreEqual(this.id4, at4);
        }

        [Test]
        public void GetAtTooEarly()
        {
            var collection = this.InitializeCollectionOrder();
            Assert.Throws<IndexOutOfRangeException>(() => collection.GetIdentifierAt(-1));
        }

        [Test]
        public void GetAtTooLate()
        {
            var collection = this.InitializeCollectionOrder();
            Assert.Throws<IndexOutOfRangeException>(() => collection.GetIdentifierAt(collection.Count));
        }

        [Test]
        public void GetPosition()
        {
            var collection = this.InitializeCollectionOrder();
            int p0 = collection.GetPositionOf(this.id0);
            int p1 = collection.GetPositionOf(this.id1);
            int p2 = collection.GetPositionOf(this.id2);
            int p3 = collection.GetPositionOf(this.id3);
            int p4 = collection.GetPositionOf(this.id4);

            Assert.AreEqual(0, p0);
            Assert.AreEqual(1, p1);
            Assert.AreEqual(2, p2);
            Assert.AreEqual(3, p3);
            Assert.AreEqual(4, p4);
        }

        [Test]
        public void GetPositionUnknown()
        {
            var factory = new GuidDataObjectIdentifierFactory<DataObjectKind.Asset>();
            var unknownId = factory.NewId();

            var collection = this.InitializeCollectionOrder();
            Assert.Throws<CBaseException>(() => collection.GetPositionOf(unknownId));
        }

        [Test]
        public void Count()
        {
            var order = this.InitializeCollectionOrder();
            Assert.AreEqual(5, order.Count);
        }

        [Test]
        public void MoveBackFirst()
        {
            var order = this.InitializeCollectionOrder();
            order.MoveBack(this.id0);
            Assert.AreEqual(5, order.Count);

            var at0 = order.GetIdentifierAt(0);
            var at1 = order.GetIdentifierAt(1);
            var at2 = order.GetIdentifierAt(2);
            var at3 = order.GetIdentifierAt(3);
            var at4 = order.GetIdentifierAt(4);

            Assert.AreEqual(this.id1, at0);
            Assert.AreEqual(this.id0, at1);
            Assert.AreEqual(this.id2, at2);
            Assert.AreEqual(this.id3, at3);
            Assert.AreEqual(this.id4, at4);
        }

        [Test]
        public void MoveBackMiddle()
        {
            var order = this.InitializeCollectionOrder();
            order.MoveBack(this.id2);
            Assert.AreEqual(5, order.Count);

            var at0 = order.GetIdentifierAt(0);
            var at1 = order.GetIdentifierAt(1);
            var at2 = order.GetIdentifierAt(2);
            var at3 = order.GetIdentifierAt(3);
            var at4 = order.GetIdentifierAt(4);

            Assert.AreEqual(this.id0, at0);
            Assert.AreEqual(this.id1, at1);
            Assert.AreEqual(this.id3, at2);
            Assert.AreEqual(this.id2, at3);
            Assert.AreEqual(this.id4, at4);
        }

        [Test]
        public void MoveBackLast()
        {
            var order = this.InitializeCollectionOrder();
            order.MoveBack(this.id4);
            Assert.AreEqual(5, order.Count);

            var at0 = order.GetIdentifierAt(0);
            var at1 = order.GetIdentifierAt(1);
            var at2 = order.GetIdentifierAt(2);
            var at3 = order.GetIdentifierAt(3);
            var at4 = order.GetIdentifierAt(4);

            Assert.AreEqual(this.id0, at0);
            Assert.AreEqual(this.id1, at1);
            Assert.AreEqual(this.id2, at2);
            Assert.AreEqual(this.id3, at3);
            Assert.AreEqual(this.id4, at4);
        }

        [Test]
        public void MoveForwardFirst()
        {
            var order = this.InitializeCollectionOrder();
            order.MoveForward(this.id0);
            Assert.AreEqual(5, order.Count);

            var at0 = order.GetIdentifierAt(0);
            var at1 = order.GetIdentifierAt(1);
            var at2 = order.GetIdentifierAt(2);
            var at3 = order.GetIdentifierAt(3);
            var at4 = order.GetIdentifierAt(4);

            Assert.AreEqual(this.id0, at0);
            Assert.AreEqual(this.id1, at1);
            Assert.AreEqual(this.id2, at2);
            Assert.AreEqual(this.id3, at3);
            Assert.AreEqual(this.id4, at4);
        }

        [Test]
        public void MoveForwardMiddle()
        {
            var order = this.InitializeCollectionOrder();
            order.MoveForward(this.id2);
            Assert.AreEqual(5, order.Count);

            var at0 = order.GetIdentifierAt(0);
            var at1 = order.GetIdentifierAt(1);
            var at2 = order.GetIdentifierAt(2);
            var at3 = order.GetIdentifierAt(3);
            var at4 = order.GetIdentifierAt(4);

            Assert.AreEqual(this.id0, at0);
            Assert.AreEqual(this.id2, at1);
            Assert.AreEqual(this.id1, at2);
            Assert.AreEqual(this.id3, at3);
            Assert.AreEqual(this.id4, at4);
        }

        [Test]
        public void MoveForwardLast()
        {
            var order = this.InitializeCollectionOrder();
            order.MoveForward(this.id4);
            Assert.AreEqual(5, order.Count);

            var at0 = order.GetIdentifierAt(0);
            var at1 = order.GetIdentifierAt(1);
            var at2 = order.GetIdentifierAt(2);
            var at3 = order.GetIdentifierAt(3);
            var at4 = order.GetIdentifierAt(4);

            Assert.AreEqual(this.id0, at0);
            Assert.AreEqual(this.id1, at1);
            Assert.AreEqual(this.id2, at2);
            Assert.AreEqual(this.id4, at3);
            Assert.AreEqual(this.id3, at4);
        }

        [Test]
        public void Remove()
        {
            var order = this.InitializeCollectionOrder();
            Assert.AreEqual(5, order.Count);

            var mgr = order as IMutableOrderedIdentifierCollection<DataObjectKind.Asset, Guid>;
            mgr.Remove(this.id2);
            Assert.AreEqual(4, order.Count);
        }

        [Test]
        public void RemoveUnknown()
        {
            var factory = new GuidDataObjectIdentifierFactory<DataObjectKind.Asset>();
            var unknownId = factory.NewId();

            var order = this.InitializeCollectionOrder();
            Assert.AreEqual(5, order.Count);

            var mgr = order as IMutableOrderedIdentifierCollection<DataObjectKind.Asset, Guid>;
            mgr.Remove(unknownId);
            Assert.AreEqual(5, order.Count);
        }

        [Test]
        public void InsertAt_NegativeIndex_ThrowsException()
        {
            var order = this.InitializeCollectionOrder() as IMutableOrderedIdentifierCollection<DataObjectKind.Asset, Guid>;
            var id = this.NewId();

            Assert.Throws<IndexOutOfRangeException>(() => order.InsertAt(-1, id));
        }

        [Test]
        public void InsertAt_IndexOutOfBounds_ThrowsException()
        {
            var order = this.InitializeCollectionOrder() as IMutableOrderedIdentifierCollection<DataObjectKind.Asset, Guid>;
            var id = this.NewId();
            var maxIndex = order.Count;

            Assert.Throws<IndexOutOfRangeException>(() => order.InsertAt(maxIndex + 1, id));
        }

        [Test]
        public void InsertAt_CountOfCollection_AddsToEnd()
        {
            var order = this.InitializeCollectionOrder() as IMutableOrderedIdentifierCollection<DataObjectKind.Asset, Guid>;
            var id = this.NewId();
            var maxIndex = order.Count;

            order.InsertAt(maxIndex, id);

            var at0 = order.GetIdentifierAt(0);
            var at1 = order.GetIdentifierAt(1);
            var at2 = order.GetIdentifierAt(2);
            var at3 = order.GetIdentifierAt(3);
            var at4 = order.GetIdentifierAt(4);
            var at5 = order.GetIdentifierAt(5);
            Assert.AreEqual(this.id0, at0);
            Assert.AreEqual(this.id1, at1);
            Assert.AreEqual(this.id2, at2);
            Assert.AreEqual(this.id3, at3);
            Assert.AreEqual(this.id4, at4);
            Assert.AreEqual(id, at5);
        }

        [Test]
        public void InsertAt_IndexZero_AddsToFront()
        {
            var order = this.InitializeCollectionOrder() as IMutableOrderedIdentifierCollection<DataObjectKind.Asset, Guid>;
            var id = this.NewId();

            order.InsertAt(0, id);

            var at0 = order.GetIdentifierAt(0);
            var at1 = order.GetIdentifierAt(1);
            var at2 = order.GetIdentifierAt(2);
            var at3 = order.GetIdentifierAt(3);
            var at4 = order.GetIdentifierAt(4);
            var at5 = order.GetIdentifierAt(5);
            Assert.AreEqual(id, at0);
            Assert.AreEqual(this.id0, at1);
            Assert.AreEqual(this.id1, at2);
            Assert.AreEqual(this.id2, at3);
            Assert.AreEqual(this.id3, at4);
            Assert.AreEqual(this.id4, at5);
        }

        [Test]
        public void InsertAt_MiddleIndex_AddsExpected()
        {
            var order = this.InitializeCollectionOrder() as IMutableOrderedIdentifierCollection<DataObjectKind.Asset, Guid>;
            var id = this.NewId();

            order.InsertAt(2, id);

            var at0 = order.GetIdentifierAt(0);
            var at1 = order.GetIdentifierAt(1);
            var at2 = order.GetIdentifierAt(2);
            var at3 = order.GetIdentifierAt(3);
            var at4 = order.GetIdentifierAt(4);
            var at5 = order.GetIdentifierAt(5);
            Assert.AreEqual(this.id0, at0);
            Assert.AreEqual(this.id1, at1);
            Assert.AreEqual(id, at2);
            Assert.AreEqual(this.id2, at3);
            Assert.AreEqual(this.id3, at4);
            Assert.AreEqual(this.id4, at5);
        }

        [Test]
        public void InsertAt_DuplicateId_ThrowsException()
        {
            var order = this.InitializeCollectionOrder() as IMutableOrderedIdentifierCollection<DataObjectKind.Asset, Guid>;

            Assert.Throws<CBaseException>(() => order.InsertAt(2, id0));
        }

        [Test]
        public void Add_DuplicateDataObjectId_ThrowsException()
        {
            var order = this.InitializeCollectionOrder() as IMutableOrderedIdentifierCollection<DataObjectKind.Asset, Guid>;

            Assert.Throws<CBaseException>(() => order.Add(id0));
        }

        [Test]
        public void Add_DuplicateIdValue_ThrowsException()
        {
            var order = this.InitializeCollectionOrder() as IInitializeCollectionOrder<Guid>;

            Assert.Throws<CBaseException>(() => order.Add(id0.Value));
        }

        private DataObjectIdentifier<DataObjectKind.Asset, Guid> NewId()
        {
            var factory = new GuidDataObjectIdentifierFactory<DataObjectKind.Asset>();
            return factory.NewId();
        }

        private IOrderedIdentifierCollection<DataObjectKind.Asset, Guid> InitializeCollectionOrder()
        {
            var order = OrderedIdentifierCollection<DataObjectKind.Asset, Guid>.Create();
            var mgr = order as IMutableOrderedIdentifierCollection<DataObjectKind.Asset, Guid>;
            mgr.Add(this.id0);
            mgr.Add(this.id1);
            mgr.Add(this.id2);
            mgr.Add(this.id3);
            mgr.Add(this.id4);

            return order;
        }
    }
}
