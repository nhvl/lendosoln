﻿namespace LendingQB.Test.Developers.LendingQB.Core
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using global::LendingQB.Core;
    using LqbGrammar.DataTypes;
    using LqbGrammar.DataTypes.PathDispatch;
    using NUnit.Framework;

    [TestFixture]
    public class ReadOnlyLqbCollectionTest
    {
        [Test]
        public void GetAllKeys_Always_ReturnsAllIdsFromCollection()
        {
            var name = Name.Create("Test").Value;
            var idFactory = new GuidDataObjectIdentifierFactory<TestObjectKind>();
            var value1 = new TestObject();
            var value2 = new TestObject();
            var value3 = new TestObject();
            var data = new Dictionary<DataObjectIdentifier<TestObjectKind, Guid>, TestObject>()
            {
                [idFactory.Create(new Guid("11111111-1111-1111-1111-111111111111"))] = value1,
                [idFactory.Create(new Guid("22222222-2222-2222-2222-222222222222"))] = value2,
                [idFactory.Create(new Guid("33333333-3333-3333-3333-333333333333"))] = value3,
            };
            var collectionFactory = new ReadOnlyLqbCollectionFactory();
            var collection = collectionFactory.Create(
                name,
                idFactory,
                data);

            IEnumerable<DataPathSelectionElement> keys = collection.GetAllKeys();

            var expected = new[]
            {
                new DataPathSelectionElement(new SelectIdExpression("11111111-1111-1111-1111-111111111111")),
                new DataPathSelectionElement(new SelectIdExpression("22222222-2222-2222-2222-222222222222")),
                new DataPathSelectionElement(new SelectIdExpression("33333333-3333-3333-3333-333333333333")),
            };
            CollectionAssert.AreEquivalent(expected, keys);
        }

        [Test]
        public void GetElement_Always_ReturnsCollectionRecord()
        {
            var name = Name.Create("Test").Value;
            var idFactory = new GuidDataObjectIdentifierFactory<TestObjectKind>();
            var value1 = new TestObject();
            var data = new Dictionary<DataObjectIdentifier<TestObjectKind, Guid>, TestObject>()
            {
                [idFactory.Create(new Guid("11111111-1111-1111-1111-111111111111"))] = value1,
            };
            var collectionFactory = new ReadOnlyLqbCollectionFactory();
            var collection = collectionFactory.Create(
                name,
                idFactory,
                data);

            object retrievedElement = collection.GetElement(new DataPathSelectionElement(new SelectIdExpression("11111111-1111-1111-1111-111111111111")));

            Assert.AreEqual(value1, retrievedElement);
        }
    }
}
