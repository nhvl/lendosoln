﻿namespace LendingQB.Test.Developers.LendingQB.Core
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using global::LendingQB.Core;
    using LqbGrammar.DataTypes;
    using LqbGrammar.DataTypes.PathDispatch;
    using NSubstitute;
    using NUnit.Framework;

    /// <summary>
    /// Simple test to cover the functionality provided by the <see cref="LqbCollection{TIdValue, TValue, TValueKind}"/> class.
    /// </summary>
    /// <remarks>
    /// Test results depend on reference equality. If the collection is updated to copy objects to a new reference the
    /// tests will need to be updated.
    /// </remarks>
    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public class LqbCollectionTest
    {
        [Test]
        public void Add_Always_ReturnsIdThatCanAccessElement()
        {
            var name = Name.Create("Test").Value;
            var idFactory = new GuidDataObjectIdentifierFactory<TestObjectKind>();
            var collection = LqbCollection<TestObjectKind, Guid, TestObject>.Create(
                name,
                idFactory);

            var item = new TestObject();
            DataObjectIdentifier<TestObjectKind, Guid> id = collection.Add(item);

            // Reference equality should be fine here.
            Assert.AreEqual(item, collection[id]);
        }

        [Test]
        public void Remove_Always_RemovesItemFromCollection()
        {
            var name = Name.Create("Test").Value;
            var idFactory = new GuidDataObjectIdentifierFactory<TestObjectKind>();
            var id = idFactory.NewId();
            var data = new Dictionary<DataObjectIdentifier<TestObjectKind, Guid>, TestObject>()
            {
                [id] = new TestObject()
            };
            var collection = LqbCollection<TestObjectKind, Guid, TestObject>.Create(
                name,
                idFactory,
                null,
                data);

            collection.Remove(id);
            bool containsKey = collection.ContainsKey(id);
            int numItems = collection.Values.Count();

            Assert.AreEqual(false, containsKey);
            Assert.AreEqual(0, numItems);
        }

        [Test]
        public void ContainsKey_KeyExists_ReturnsTrue()
        {
            var name = Name.Create("Test").Value;
            var idFactory = new GuidDataObjectIdentifierFactory<TestObjectKind>();
            var id = idFactory.NewId();
            var data = new Dictionary<DataObjectIdentifier<TestObjectKind, Guid>, TestObject>()
            {
                [id] = new TestObject()
            };
            var collection = LqbCollection<TestObjectKind, Guid, TestObject>.Create(
                name,
                idFactory,
                null,
                data);

            bool containsKey = collection.ContainsKey(id);

            Assert.AreEqual(true, containsKey);
        }

        [Test]
        public void ContainsKey_KeyDoesNotExist_ReturnsFalse()
        {
            var name = Name.Create("Test").Value;
            var idFactory = new GuidDataObjectIdentifierFactory<TestObjectKind>();
            var id = idFactory.NewId();
            var collection = LqbCollection<TestObjectKind, Guid, TestObject>.Create(
                name,
                idFactory);

            bool containsKey = collection.ContainsKey(id);

            Assert.AreEqual(false, containsKey);
        }

        [Test]
        public void TryGetValue_KeyExists_ReturnsTrueAndOutputsSameItem()
        {
            var name = Name.Create("Test").Value;
            var idFactory = new GuidDataObjectIdentifierFactory<TestObjectKind>();
            var id = idFactory.NewId();
            var item = new TestObject();
            var data = new Dictionary<DataObjectIdentifier<TestObjectKind, Guid>, TestObject>()
            {
                [id] = item
            };

            var collection = LqbCollection<TestObjectKind, Guid, TestObject>.Create(
                name,
                idFactory,
                null,
                data);

            TestObject outItem;
            bool containsKey = collection.TryGetValue(id, out outItem);

            Assert.AreEqual(true, containsKey);
            Assert.AreEqual(item, outItem);
        }

        [Test]
        public void TryGetValue_KeyDoesNotExist_ReturnsFalseAndOutputsDefaultItem()
        {
            var name = Name.Create("Test").Value;
            var idFactory = new GuidDataObjectIdentifierFactory<TestObjectKind>();
            var id = idFactory.NewId();
            var collection = LqbCollection<TestObjectKind, Guid, TestObject>.Create(
                name,
                idFactory);

            TestObject outItem = new TestObject(); // Initialize just so we don't start with the value we're testing against...
            bool containsKey = collection.TryGetValue(id, out outItem);

            Assert.AreEqual(false, containsKey);
            Assert.AreEqual(default(TestObject), outItem);
        }

        [Test]
        public void Keys_ThreeKeysExist_ReturnsAllKeys()
        {
            var name = Name.Create("Test").Value;
            var idFactory = new GuidDataObjectIdentifierFactory<TestObjectKind>();
            var id1 = idFactory.NewId();
            var id2 = idFactory.NewId();
            var id3 = idFactory.NewId();
            var data = new Dictionary<DataObjectIdentifier<TestObjectKind, Guid>, TestObject>()
            {
                [id1] = new TestObject(),
                [id2] = new TestObject(),
                [id3] = new TestObject(),
            };
            var collection = LqbCollection<TestObjectKind, Guid, TestObject>.Create(
                name,
                idFactory,
                null,
                data);

            IEnumerable<DataObjectIdentifier<TestObjectKind, Guid>> keys = collection.Keys;

            CollectionAssert.AreEquivalent(new[] { id1, id2, id3 }, keys);
        }

        [Test]
        public void Values_ThreeValuesExist_ReturnsAllThreeValues()
        {
            var name = Name.Create("Test").Value;
            var idFactory = new GuidDataObjectIdentifierFactory<TestObjectKind>();
            var value1 = new TestObject();
            var value2 = new TestObject();
            var value3 = new TestObject();
            var data = new Dictionary<DataObjectIdentifier<TestObjectKind, Guid>, TestObject>()
            {
                [idFactory.NewId()] = value1,
                [idFactory.NewId()] = value2,
                [idFactory.NewId()] = value3,
            };
            var collection = LqbCollection<TestObjectKind, Guid, TestObject>.Create(
                name,
                idFactory,
                null,
                data);

            IEnumerable<TestObject> values = collection.Values;

            CollectionAssert.AreEquivalent(new[] { value1, value2, value3 }, values);
        }

        [Test]
        public void Add_Always_NotifiesObserver()
        {
            var name = Name.Create("Test").Value;
            var idFactory = new GuidDataObjectIdentifierFactory<TestObjectKind>();
            var observer = Substitute.For<ILqbCollectionObserver<TestObjectKind, Guid, TestObject>>();
            var collection = LqbCollection<TestObjectKind, Guid, TestObject>.Create(
                name,
                idFactory,
                observer);
            var item = new TestObject();


            DataObjectIdentifier<TestObjectKind, Guid> id = collection.Add(item);

            observer.Received(1).NotifyAdd(id, item);
        }

        [Test]
        public void Remove_ExistingItem_NotifiesObserver()
        {
            var name = Name.Create("Test").Value;
            var idFactory = new GuidDataObjectIdentifierFactory<TestObjectKind>();
            var observer = Substitute.For<ILqbCollectionObserver<TestObjectKind, Guid, TestObject>>();
            var id = idFactory.NewId();
            var item = new TestObject();
            var data = new Dictionary<DataObjectIdentifier<TestObjectKind, Guid>, TestObject>()
            {
                [id] = item
            };
            var collection = LqbCollection<TestObjectKind, Guid, TestObject>.Create(
                name,
                idFactory,
                observer,
                data);

            collection.Remove(id);

            observer.Received(1).NotifyDelete(id, item);
        }

        [Test]
        public void Remove_ItemDoesNotExist_DoesNotNotifyObserver()
        {
            var name = Name.Create("Test").Value;
            var idFactory = new GuidDataObjectIdentifierFactory<TestObjectKind>();
            var observer = Substitute.For<ILqbCollectionObserver<TestObjectKind, Guid, TestObject>>();
            var id = idFactory.NewId();
            var collection = LqbCollection<TestObjectKind, Guid, TestObject>.Create(
                name,
                idFactory,
                observer);

            collection.Remove(id);

            observer.DidNotReceive().NotifyDelete(id, Arg.Any<TestObject>());
        }

        [Test]
        public void GetUpdates_AddedSingleObject_ReturnsExpected()
        {
            var name = Name.Create("Test").Value;
            var idFactory = new GuidDataObjectIdentifierFactory<TestObjectKind>();
            var observer = Substitute.For<ILqbCollectionObserver<TestObjectKind, Guid, TestObject>>();
            var collection = LqbCollection<TestObjectKind, Guid, TestObject>.Create(
                name,
                idFactory,
                observer);
            var item = new TestObject();

            collection.BeginTrackingChanges();
            var id = collection.Add(item);
            var changes = collection.GetChanges();

            Assert.AreEqual(1, changes.AddedRecords.Count());
            Assert.AreEqual(0, changes.UpdatedRecords.Count());
            Assert.AreEqual(0, changes.RemovedRecords.Count());

            Assert.AreEqual(id, changes.AddedRecords.First().Key);
            Assert.AreEqual(item, changes.AddedRecords.First().Value);
        }

        [Test]
        public void GetUpdates_RemovedSingleObject_ReturnsExpected()
        {
            var name = Name.Create("Test").Value;
            var idFactory = new GuidDataObjectIdentifierFactory<TestObjectKind>();
            var observer = Substitute.For<ILqbCollectionObserver<TestObjectKind, Guid, TestObject>>();
            var id = idFactory.NewId();
            var item = new TestObject();
            var data = new Dictionary<DataObjectIdentifier<TestObjectKind, Guid>, TestObject>()
            {
                [id] = item
            };
            var collection = LqbCollection<TestObjectKind, Guid, TestObject>.Create(
                name,
                idFactory,
                observer,
                data);

            collection.BeginTrackingChanges();
            collection.Remove(id);
            var changes = collection.GetChanges();

            Assert.AreEqual(0, changes.AddedRecords.Count());
            Assert.AreEqual(0, changes.UpdatedRecords.Count());
            Assert.AreEqual(1, changes.RemovedRecords.Count());

            Assert.AreEqual(id, changes.RemovedRecords.First());
        }

        [Test]
        public void GetUpdates_UpdatedSingleObject_ReturnsExpected()
        {
            var name = Name.Create("Test").Value;
            var idFactory = new GuidDataObjectIdentifierFactory<TestObjectKind>();
            var observer = Substitute.For<ILqbCollectionObserver<TestObjectKind, Guid, TestObject>>();
            var id = idFactory.NewId();
            var item = new TestObject(new[] { new KeyValuePair<string, object>("Field", "Value") });
            var data = new Dictionary<DataObjectIdentifier<TestObjectKind, Guid>, TestObject>()
            {
                [id] = item
            };
            var collection = LqbCollection<TestObjectKind, Guid, TestObject>.Create(
                name,
                idFactory,
                observer,
                data);

            collection.BeginTrackingChanges();
            var changes = collection.GetChanges();

            Assert.AreEqual(0, changes.AddedRecords.Count());
            Assert.AreEqual(1, changes.UpdatedRecords.Count());
            Assert.AreEqual(0, changes.RemovedRecords.Count());

            Assert.AreEqual(id, changes.UpdatedRecords.First().Key);
            Assert.AreEqual(item, changes.UpdatedRecords.First().Value);
        }

        [Test]
        [ExpectedException(ExpectedException = typeof(InvalidOperationException))]
        public void GetUpdates_BeginTrackingChangesNotCalled_ThrowsException()
        {
            var name = Name.Create("Test").Value;
            var idFactory = new GuidDataObjectIdentifierFactory<TestObjectKind>();
            var collection = LqbCollection<TestObjectKind, Guid, TestObject>.Create(
                name,
                idFactory);

            var changes = collection.GetChanges();
        }

        [Test]
        [ExpectedException(ExpectedException = typeof(InvalidOperationException))]
        public void BeginTrackingChanges_CalledMultipleTimes_ThrowsException()
        {
            var name = Name.Create("Test").Value;
            var idFactory = new GuidDataObjectIdentifierFactory<TestObjectKind>();
            var collection = LqbCollection<TestObjectKind, Guid, TestObject>.Create(
                name,
                idFactory);

            collection.BeginTrackingChanges();
            collection.BeginTrackingChanges();
        }

        [Test]
        public void GetAllKeys_Always_ReturnsAllIdsFromCollection()
        {
            var name = Name.Create("Test").Value;
            var idFactory = new GuidDataObjectIdentifierFactory<TestObjectKind>();
            var value1 = new TestObject();
            var value2 = new TestObject();
            var value3 = new TestObject();
            var data = new Dictionary<DataObjectIdentifier<TestObjectKind, Guid>, TestObject>()
            {
                [idFactory.Create(new Guid("11111111-1111-1111-1111-111111111111"))] = value1,
                [idFactory.Create(new Guid("22222222-2222-2222-2222-222222222222"))] = value2,
                [idFactory.Create(new Guid("33333333-3333-3333-3333-333333333333"))] = value3,
            };
            var collection = LqbCollection<TestObjectKind, Guid, TestObject>.Create(
                name,
                idFactory,
                null,
                data);

            IEnumerable<DataPathSelectionElement> keys = collection.GetAllKeys();

            var expected = new[]
            {
                new DataPathSelectionElement(new SelectIdExpression("11111111-1111-1111-1111-111111111111")),
                new DataPathSelectionElement(new SelectIdExpression("22222222-2222-2222-2222-222222222222")),
                new DataPathSelectionElement(new SelectIdExpression("33333333-3333-3333-3333-333333333333")),
            };
            CollectionAssert.AreEquivalent(expected, keys);
        }

        [Test]
        public void GetElement_Always_ReturnsCollectionRecord()
        {
            var name = Name.Create("Test").Value;
            var idFactory = new GuidDataObjectIdentifierFactory<TestObjectKind>();
            var value1 = new TestObject();
            var data = new Dictionary<DataObjectIdentifier<TestObjectKind, Guid>, TestObject>()
            {
                [idFactory.Create(new Guid("11111111-1111-1111-1111-111111111111"))] = value1,
            };
            var collection = LqbCollection<TestObjectKind, Guid, TestObject>.Create(
                name,
                idFactory,
                null,
                data);

            object retrievedElement = collection.GetElement(new DataPathSelectionElement(new SelectIdExpression("11111111-1111-1111-1111-111111111111")));

            Assert.AreEqual(value1, retrievedElement);
        }
    }

    // public so it can be faked by NSub.
    public class TestObjectKind : DataObjectKind
    {
    }

    // public so it can be faked by NSub.
    public class TestObject : ITrackChanges
    {
        private IEnumerable<KeyValuePair<string, object>> updates;

        public TestObject()
        {
            this.updates = Enumerable.Empty<KeyValuePair<string, object>>();
        }

        public TestObject(IEnumerable<KeyValuePair<string, object>> updates)
        {
            this.updates = updates;
        }

        public bool HasChanges
        {
            get
            {
                return this.GetChanges().Any();
            }
        }

        public IEnumerable<KeyValuePair<string, object>> GetChanges()
        {
            return this.updates;
        }
    }
}
