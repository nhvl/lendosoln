﻿namespace LendingQB.Test.Developers.LendingQB.Core
{
    using System;
    using System.Linq;
    using LendersOffice.Security;
    using NUnit.Framework;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;
    using LendersOffice.Drivers.SqlServerDB;
    using Utils;
    using global::LendingQB.Core.Data;
    using System.Data;
    using global::DataAccess;
    using DataAccess.EntityShim;
    using NSubstitute;
    using LendersOffice.Constants;
    using System.Data.Common;

    [TestFixture]
    [Category(CategoryConstants.IntegrationTest)]
    class LoanLqbCollectionContainerTest
    {
        private AbstractUserPrincipal principal;
        private DataObjectIdentifier<DataObjectKind.Loan, Guid> loanId;
        private DataObjectIdentifier<DataObjectKind.ClientCompany, Guid> brokerId;
        private IDbConnection connection;
        private IDbTransaction transaction;

        [TestFixtureSetUp]
        public void FixtureSetUp()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                this.principal = LoginTools.LoginAs(TestAccountType.LoTest001);
                this.brokerId = DataObjectIdentifier<DataObjectKind.ClientCompany, Guid>.Create(principal.BrokerId);
                var creator = CLoanFileCreator.GetCreator(
                    this.principal,
                    LendersOffice.Audit.E_LoanCreationSource.UserCreateFromBlank);
                this.loanId = DataObjectIdentifier<DataObjectKind.Loan, Guid>.Create(creator.CreateBlankLoanFile());
            }
        }

        [TestFixtureTearDown]
        public void FixtureTearDown()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                Tools.DeclareLoanFileInvalid(
                    this.principal,
                    this.loanId.Value,
                    sendNotifications: false,
                    generateLinkLoanMsgEvent: false);
            }
        }

        [SetUp]
        public void SetUp()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                this.connection = DbConnectionInfo.GetConnectionInfo(this.brokerId.Value).GetConnection();
                this.connection.Open();
                this.transaction = connection.BeginTransaction();
            }
        }

        [TearDown]
        public void TearDown()
        {
            this.transaction.Rollback();
            this.transaction.Dispose();
            this.connection.Dispose();
        }

        [Test]
        public void LoadAll()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                var collectionContainer = this.CreateContainer(Substitute.For<ILoanLqbCollectionContainerLoanDataProvider>());
                RegisterAllCollections(collectionContainer);

                this.LoadContainer(collectionContainer);

                Assert.AreEqual(0, collectionContainer.Assets.Count);
                Assert.AreEqual(0, collectionContainer.EmploymentRecords.Count);
                Assert.AreEqual(0, collectionContainer.Liabilities.Count);
                Assert.AreEqual(0, collectionContainer.PublicRecords.Count);
                Assert.AreEqual(0, collectionContainer.RealProperties.Count);
                Assert.AreEqual(0, collectionContainer.VaPreviousLoans.Count);
                Assert.AreEqual(0, collectionContainer.VorRecords.Count);
                Assert.AreEqual(0, collectionContainer.RealPropertyLiabilities.Count);
                Assert.AreEqual(0, collectionContainer.ConsumerAssets.Count);
            }
        }

        [Test]
        public void SaveAndLoadNewRecord()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                var loan = new CPageData(this.loanId.Value, new[] { nameof(CPageData.LegacyApplicationConsumers) });
                loan.InitLoad();
                var collectionContainer = this.CreateContainer(CPageBaseExtractor.Extract(loan));
                collectionContainer.RegisterCollection(nameof(ILoanLqbCollectionContainer.Liabilities));
                collectionContainer.RegisterCollection(nameof(ILoanLqbCollectionContainer.ConsumerLiabilities));
                collectionContainer.RegisterCollection(nameof(ILoanLqbCollectionContainer.RealProperties));
                collectionContainer.RegisterCollection(nameof(ILoanLqbCollectionContainer.ConsumerRealProperties));
                collectionContainer.RegisterCollection(nameof(ILoanLqbCollectionContainer.RealPropertyLiabilities));

                this.LoadContainer(collectionContainer);
                collectionContainer.BeginTrackingChanges();

                Assert.AreEqual(0, collectionContainer.Liabilities.Count);
                Assert.AreEqual(0, collectionContainer.RealProperties.Count);
                Assert.AreEqual(0, collectionContainer.RealPropertyLiabilities.Count);

                var consumerId = loan.LegacyApplicationConsumers.Values.First().ConsumerId;
                var liabilityId = collectionContainer.Add(consumerId, new Liability(Substitute.For<ILiabilityDefaultsProvider>()));
                var realPropertyId = collectionContainer.Add(consumerId, new RealProperty());

                var shim = (IShimContainer)collectionContainer;
                shim.AssociateLiabilityWithRealProperty(liabilityId.Value, realPropertyId.Value);

                Assert.AreEqual(1, collectionContainer.Liabilities.Count);
                Assert.AreEqual(1, collectionContainer.RealProperties.Count);
                Assert.AreEqual(1, collectionContainer.RealPropertyLiabilities.Count);

                this.SaveContainer(collectionContainer);

                this.LoadContainer(collectionContainer);
                Assert.AreEqual(1, collectionContainer.Liabilities.Count);
                Assert.AreEqual(1, collectionContainer.RealProperties.Count);
                Assert.AreEqual(1, collectionContainer.RealPropertyLiabilities.Count);
            }
        }

        [Test]
        public void UpdateLiabilityId()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                var loan = new CPageData(this.loanId.Value, new[] { nameof(CPageData.LegacyApplicationConsumers) });
                loan.InitLoad();
                var loanData = CPageBaseExtractor.Extract(loan);
                var collectionContainer = this.CreateContainer(loanData);

                // Load the collection container and save a new Liability.
                collectionContainer.RegisterCollection(nameof(ILoanLqbCollectionContainer.Liabilities));
                collectionContainer.RegisterCollection(nameof(ILoanLqbCollectionContainer.RealProperties));
                collectionContainer.RegisterCollection(nameof(ILoanLqbCollectionContainer.RealPropertyLiabilities));
                collectionContainer.RegisterCollection(nameof(ILoanLqbCollectionContainer.ConsumerLiabilities));
                collectionContainer.RegisterCollection(nameof(ILoanLqbCollectionContainer.ConsumerRealProperties));
                this.LoadContainer(collectionContainer);
                collectionContainer.BeginTrackingChanges();
                var consumerId = loan.LegacyApplicationConsumers.Values.First().ConsumerId;

                var initialLiabilityId = collectionContainer.Add(consumerId, LiabilityCollectionShimTest.CreateLiability());
                var consumerLiabilityId = collectionContainer.ConsumerLiabilities.First().Key;

                var realPropertyId = collectionContainer.Add(consumerId, new RealProperty());
                var consumerRealPropertyId = collectionContainer.ConsumerRealProperties.First().Key;

                var shim = (IShimContainer)collectionContainer;
                shim.AssociateLiabilityWithRealProperty(initialLiabilityId.Value, realPropertyId.Value);
                var realPropertyLiabilityId = collectionContainer.RealPropertyLiabilities.First().Key;

                this.SaveContainer(collectionContainer);

                // Load, update liability id, save.
                collectionContainer = this.CreateContainer(loanData);
                shim = (IShimContainer)collectionContainer;
                collectionContainer.RegisterCollection(nameof(ILoanLqbCollectionContainer.Liabilities));
                collectionContainer.RegisterCollection(nameof(ILoanLqbCollectionContainer.RealProperties));
                collectionContainer.RegisterCollection(nameof(ILoanLqbCollectionContainer.RealPropertyLiabilities));
                collectionContainer.RegisterCollection(nameof(ILoanLqbCollectionContainer.ConsumerLiabilities));
                collectionContainer.RegisterCollection(nameof(ILoanLqbCollectionContainer.ConsumerRealProperties));
                this.LoadContainer(collectionContainer);
                collectionContainer.BeginTrackingChanges();
                var newLiabilityId = DataObjectIdentifier<DataObjectKind.Liability, Guid>.Create(Guid.NewGuid());

                shim.UpdateLiabilityId(initialLiabilityId.Value, newLiabilityId.Value);
                this.SaveContainer(collectionContainer);

                // Verify no record exists for original id, record exists for new id,
                // and the records are equal.
                collectionContainer = this.CreateContainer(loanData);
                shim = (IShimContainer)collectionContainer;
                collectionContainer.RegisterCollection(nameof(ILoanLqbCollectionContainer.Liabilities));
                collectionContainer.RegisterCollection(nameof(ILoanLqbCollectionContainer.RealProperties));
                collectionContainer.RegisterCollection(nameof(ILoanLqbCollectionContainer.RealPropertyLiabilities));
                collectionContainer.RegisterCollection(nameof(ILoanLqbCollectionContainer.ConsumerLiabilities));
                collectionContainer.RegisterCollection(nameof(ILoanLqbCollectionContainer.ConsumerRealProperties));
                this.LoadContainer(collectionContainer);
                #region Verify Liability
                var liabilities = collectionContainer.Liabilities;
                Assert.AreEqual(false, liabilities.ContainsKey(initialLiabilityId));
                Assert.AreEqual(true, liabilities.ContainsKey(newLiabilityId));

                var defaultLiability = LiabilityCollectionShimTest.CreateLiability();
                var loadedLiability = liabilities[newLiabilityId];
                Assert.AreEqual(defaultLiability.DebtType, loadedLiability.DebtType);
                Assert.AreEqual(defaultLiability.UsedInRatio, loadedLiability.UsedInRatio);
                Assert.AreEqual(defaultLiability.Pmt, loadedLiability.Pmt);
                Assert.AreEqual(defaultLiability.RemainMon, loadedLiability.RemainMon);
                Assert.AreEqual(defaultLiability.WillBePdOff, loadedLiability.WillBePdOff);
                Assert.AreEqual(defaultLiability.AccountName, loadedLiability.AccountName);
                Assert.AreEqual(defaultLiability.AccountNum, loadedLiability.AccountNum);
                Assert.AreEqual(defaultLiability.Attention, loadedLiability.Attention);
                Assert.AreEqual(defaultLiability.AutoYearMakeAsString, loadedLiability.AutoYearMakeAsString);
                Assert.AreEqual(defaultLiability.Bal, loadedLiability.Bal);
                Assert.AreEqual(defaultLiability.CompanyAddress, loadedLiability.CompanyAddress);
                Assert.AreEqual(defaultLiability.CompanyCity, loadedLiability.CompanyCity);
                Assert.AreEqual(defaultLiability.CompanyFax, loadedLiability.CompanyFax);
                Assert.AreEqual(defaultLiability.CompanyName, loadedLiability.CompanyName);
                Assert.AreEqual(defaultLiability.CompanyPhone, loadedLiability.CompanyPhone);
                Assert.AreEqual(defaultLiability.CompanyState, loadedLiability.CompanyState);
                Assert.AreEqual(defaultLiability.CompanyZip, loadedLiability.CompanyZip);
                Assert.AreEqual(defaultLiability.Desc, loadedLiability.Desc);
                Assert.AreEqual(defaultLiability.DueInMonAsString, loadedLiability.DueInMonAsString);
                Assert.AreEqual(defaultLiability.ExcludeFromUw, loadedLiability.ExcludeFromUw);
                Assert.AreEqual(defaultLiability.FullyIndexedPitiPmt, loadedLiability.FullyIndexedPitiPmt);
                Assert.AreEqual(defaultLiability.IncludeInBk, loadedLiability.IncludeInBk);
                Assert.AreEqual(defaultLiability.IncludeInFc, loadedLiability.IncludeInFc);
                Assert.AreEqual(defaultLiability.IncludeInReposession, loadedLiability.IncludeInReposession);
                Assert.AreEqual(defaultLiability.IsForAuto, loadedLiability.IsForAuto);
                Assert.AreEqual(defaultLiability.IsMtgFhaInsured, loadedLiability.IsMtgFhaInsured);
                Assert.AreEqual(defaultLiability.IsPiggyBack, loadedLiability.IsPiggyBack);
                Assert.AreEqual(defaultLiability.IsSeeAttachment, loadedLiability.IsSeeAttachment);
                Assert.AreEqual(defaultLiability.IsSp1stMtgData, loadedLiability.IsSp1stMtgData);
                Assert.AreEqual(defaultLiability.Late30AsString, loadedLiability.Late30AsString);
                Assert.AreEqual(defaultLiability.Late60AsString, loadedLiability.Late60AsString);
                Assert.AreEqual(defaultLiability.Late90PlusAsString, loadedLiability.Late90PlusAsString);
                Assert.AreEqual(defaultLiability.OriginalDebtAmt, loadedLiability.OriginalDebtAmt);
                Assert.AreEqual(defaultLiability.OriginalTermAsString, loadedLiability.OriginalTermAsString);
                Assert.AreEqual(defaultLiability.PayoffAmt, loadedLiability.PayoffAmt);
                Assert.AreEqual(defaultLiability.PayoffAmtLocked, loadedLiability.PayoffAmtLocked);
                Assert.AreEqual(defaultLiability.PayoffTimingData, loadedLiability.PayoffTimingData);
                Assert.AreEqual(defaultLiability.PayoffTimingLocked, loadedLiability.PayoffTimingLocked);
                Assert.AreEqual(defaultLiability.PrepDate, loadedLiability.PrepDate);
                Assert.AreEqual(defaultLiability.Rate, loadedLiability.Rate);
                Assert.AreEqual(defaultLiability.ReconcileStatusType, loadedLiability.ReconcileStatusType);
                Assert.AreEqual(defaultLiability.UsedInRatio, loadedLiability.UsedInRatio);
                Assert.AreEqual(defaultLiability.VerifExpiresDate, loadedLiability.VerifExpiresDate);
                Assert.AreEqual(defaultLiability.VerifRecvDate, loadedLiability.VerifRecvDate);
                Assert.AreEqual(defaultLiability.VerifReorderedDate, loadedLiability.VerifReorderedDate);
                Assert.AreEqual(defaultLiability.VerifSentDate, loadedLiability.VerifSentDate);
                Assert.AreEqual(defaultLiability.OwedTo, loadedLiability.OwedTo);
                Assert.AreEqual(defaultLiability.JobExpenseDesc, loadedLiability.JobExpenseDesc);
                #endregion

                Assert.AreEqual(false, collectionContainer.RealPropertyLiabilities.ContainsKey(realPropertyLiabilityId));
                Assert.AreEqual(1, collectionContainer.RealPropertyLiabilities.Count);
                Assert.AreEqual(realPropertyId, collectionContainer.RealPropertyLiabilities.Single().Value.RealPropertyId);
                Assert.AreEqual(newLiabilityId, collectionContainer.RealPropertyLiabilities.Single().Value.LiabilityId);

                Assert.AreEqual(false, collectionContainer.ConsumerLiabilities.ContainsKey(consumerLiabilityId));
                Assert.AreEqual(1, collectionContainer.ConsumerLiabilities.Count);
                Assert.AreEqual(consumerId, collectionContainer.ConsumerLiabilities.Single().Value.ConsumerId);
                Assert.AreEqual(newLiabilityId, collectionContainer.ConsumerLiabilities.Single().Value.LiabilityId);
                Assert.AreEqual(true, collectionContainer.ConsumerLiabilities.Single().Value.IsPrimary);
            }
        }

        [Test]
        public void GetRealPropertyIdLinkedToLiability_NoAssociation_ReturnsEmptyGuid()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                var loanData = Substitute.For<ILoanLqbCollectionContainerLoanDataProvider>();
                loanData.LegacyApplicationConsumers.Returns(Fakes.FakeLegacyApplicationConsumers.ForSingleLegacyApplication(Guid.Empty, Guid.Empty, null));
                var collectionContainer = this.CreateContainer(loanData);
                collectionContainer.RegisterCollection(nameof(ILoanLqbCollectionContainer.ConsumerLiabilities));
                collectionContainer.RegisterCollection(nameof(ILoanLqbCollectionContainer.Liabilities));
                collectionContainer.RegisterCollection(nameof(ILoanLqbCollectionContainer.ConsumerRealProperties));
                collectionContainer.RegisterCollection(nameof(ILoanLqbCollectionContainer.RealProperties));
                collectionContainer.RegisterCollection(nameof(ILoanLqbCollectionContainer.RealPropertyLiabilities));
                this.LoadContainer(collectionContainer);

                var consumerId = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(Guid.Empty);
                var liabilityId = collectionContainer.Add(consumerId, new Liability(Substitute.For<ILiabilityDefaultsProvider>()));

                var shim = collectionContainer as IShimContainer;
                var matchingRealPropertyId = shim.GetRealPropertyIdLinkedToLiability(liabilityId.Value);

                Assert.AreEqual(Guid.Empty, matchingRealPropertyId);
            }
        }

        [Test]
        public void GetRealPropertyIdLinkedToLiability_WithMatchingAssociation_ReturnsRealPropertyId()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                var loanData = Substitute.For<ILoanLqbCollectionContainerLoanDataProvider>();
                loanData.LegacyApplicationConsumers.Returns(Fakes.FakeLegacyApplicationConsumers.ForSingleLegacyApplication(Guid.Empty, Guid.Empty, null));
                var collectionContainer = this.CreateContainer(loanData);
                collectionContainer.RegisterCollection(nameof(ILoanLqbCollectionContainer.ConsumerLiabilities));
                collectionContainer.RegisterCollection(nameof(ILoanLqbCollectionContainer.Liabilities));
                collectionContainer.RegisterCollection(nameof(ILoanLqbCollectionContainer.ConsumerRealProperties));
                collectionContainer.RegisterCollection(nameof(ILoanLqbCollectionContainer.RealProperties));
                collectionContainer.RegisterCollection(nameof(ILoanLqbCollectionContainer.RealPropertyLiabilities));
                this.LoadContainer(collectionContainer);

                var consumerId = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(Guid.Empty);
                var liabilityId = collectionContainer.Add(consumerId, new Liability(Substitute.For<ILiabilityDefaultsProvider>()));
                var realPropertyId = collectionContainer.Add(consumerId, new RealProperty());

                var shim = (IShimContainer)collectionContainer;
                shim.AssociateLiabilityWithRealProperty(liabilityId.Value, realPropertyId.Value);

                var retrievedId = shim.GetRealPropertyIdLinkedToLiability(liabilityId.Value);
                Assert.AreEqual(realPropertyId.Value, retrievedId);
            }
        }

        [Test]
        public void AssociateLiabilityWithRealProperty_NoExistingAssociations_CreatesNewAssociation()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                ILoanLqbCollectionContainerLoanDataProvider loanData = Substitute.For<ILoanLqbCollectionContainerLoanDataProvider>();
                loanData.LegacyApplicationConsumers.Returns(Fakes.FakeLegacyApplicationConsumers.ForSingleLegacyApplication(Guid.Empty, Guid.Empty, null));
                var collectionContainer = this.CreateContainer(loanData);
                collectionContainer.RegisterCollection(nameof(ILoanLqbCollectionContainer.ConsumerLiabilities));
                collectionContainer.RegisterCollection(nameof(ILoanLqbCollectionContainer.Liabilities));
                collectionContainer.RegisterCollection(nameof(ILoanLqbCollectionContainer.RealProperties));
                collectionContainer.RegisterCollection(nameof(ILoanLqbCollectionContainer.ConsumerRealProperties));
                collectionContainer.RegisterCollection(nameof(ILoanLqbCollectionContainer.RealPropertyLiabilities));
                this.LoadContainer(collectionContainer);

                var consumerId = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(Guid.Empty);
                var liabilityId = collectionContainer.Add(consumerId, new Liability(Substitute.For<ILiabilityDefaultsProvider>()));
                var realPropertyId = collectionContainer.Add(consumerId, new RealProperty());

                var shim = (IShimContainer)collectionContainer;
                shim.AssociateLiabilityWithRealProperty(liabilityId.Value, realPropertyId.Value);

                var association = collectionContainer.RealPropertyLiabilities.Values.Single();
                Assert.AreEqual(realPropertyId, association.RealPropertyId);
                Assert.AreEqual(liabilityId, association.LiabilityId);
            }
        }

        [Test]
        public void AssociateLiabilityWithRealProperty_NoExistingAssociationsAndSave_CreatesAndPersistsNewAssociation()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                var loan = new CPageData(this.loanId.Value, new[] { nameof(CPageData.LegacyApplicationConsumers) });
                loan.InitLoad();
                var collectionContainer = this.CreateContainer(CPageBaseExtractor.Extract(loan));
                collectionContainer.RegisterCollection(nameof(ILoanLqbCollectionContainer.Liabilities));
                collectionContainer.RegisterCollection(nameof(ILoanLqbCollectionContainer.ConsumerLiabilities));
                collectionContainer.RegisterCollection(nameof(ILoanLqbCollectionContainer.RealProperties));
                collectionContainer.RegisterCollection(nameof(ILoanLqbCollectionContainer.ConsumerRealProperties));
                collectionContainer.RegisterCollection(nameof(ILoanLqbCollectionContainer.RealPropertyLiabilities));
                this.LoadContainer(collectionContainer);
                collectionContainer.BeginTrackingChanges();

                var consumerId = loan.LegacyApplicationConsumers.Values.First().ConsumerId;
                var liabilityId = collectionContainer.Add(consumerId, new Liability(Substitute.For<ILiabilityDefaultsProvider>()));
                var realPropertyId = collectionContainer.Add(consumerId, new RealProperty());

                var shim = (IShimContainer)collectionContainer;
                shim.AssociateLiabilityWithRealProperty(liabilityId.Value, realPropertyId.Value);
                this.SaveContainer(collectionContainer);

                this.LoadContainer(collectionContainer);
                var association = collectionContainer.RealPropertyLiabilities.Values.Single();
                Assert.AreEqual(realPropertyId, association.RealPropertyId);
                Assert.AreEqual(liabilityId, association.LiabilityId);
            }
        }

        [Test]
        public void LegacyApplicationConsumers_AccessedAfterInitLoad_SuccessfullyPullsFromLoan()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                var providerFactory = new LoanLqbCollectionProviderFactory();
                var collectionProvider = providerFactory.CreateStoredProcedureProvider(this.loanId, null, null, null, nameof(LoanLqbCollectionContainerTest));
                var containerFactory = new LoanLqbCollectionContainerFactory();
                var loan = new CPageBaseWrapped(this.loanId.Value, "Test", CSelectStatementProvider.GetProviderForTargets(new[] { "sLT" }, false));
                loan.InitLoad();
                var container = containerFactory.Create(collectionProvider, loan, loan, null);

                var legacyAppConsumers = container.LegacyApplicationConsumers;

                Assert.IsNotNull(legacyAppConsumers);
                Assert.AreEqual(true, legacyAppConsumers.Count > 0);
            }
        }

        [Test]
        public void LegacyApplicationConsumers_AccessedAfterInitSave_SuccessfullyPullsFromLoan()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                var providerFactory = new LoanLqbCollectionProviderFactory();
                var collectionProvider = providerFactory.CreateStoredProcedureProvider(this.loanId, null, null, null, nameof(LoanLqbCollectionContainerTest));
                var containerFactory = new LoanLqbCollectionContainerFactory();
                var loan = new CPageBaseWrapped(this.loanId.Value, "Test", CSelectStatementProvider.GetProviderForTargets(new[] { "sLT" }, false));
                loan.InitSave(ConstAppDavid.SkipVersionCheck);
                var container = containerFactory.Create(collectionProvider, loan, loan, null);

                var legacyAppConsumers = container.LegacyApplicationConsumers;

                Assert.IsNotNull(legacyAppConsumers);
                Assert.AreEqual(true, legacyAppConsumers.Count > 0);
            }
        }

        private ILoanLqbCollectionContainer CreateContainer(ILoanLqbCollectionContainerLoanDataProvider loanData)
        {
            Func<DbConnection> connectionFactory = () =>
            {
                Guid brokerId;
                var info = DbConnectionInfo.GetConnectionInfoByLoanId(this.loanId.Value, out brokerId);
                return info.GetConnection();
            };
            var providerFactory = new LoanLqbCollectionProviderFactory();
            var collectionProvider = providerFactory.CreateStoredProcedureProvider(this.loanId, null, null, connectionFactory, nameof(LoanLqbCollectionContainerTest));

            var containerFactory = new LoanLqbCollectionContainerFactory();
            return containerFactory.Create(
                collectionProvider,
                loanData,
                Substitute.For<ILoanLqbCollectionBorrowerManagement>(),
                null);
        }

        private void LoadContainer(ILoanLqbCollectionContainer collectionContainer)
        {
            collectionContainer.Load(
                this.brokerId,
                this.connection,
                this.transaction);
        }

        private void SaveContainer(ILoanLqbCollectionContainer collectionContainer)
        {
            collectionContainer.Save(
                this.connection,
                this.transaction);
        }

        private void RegisterAllCollections(ILoanLqbCollectionContainer collectionContainer)
        {
            foreach(var collection in LoanLqbCollectionContainer.LoanCollections)
            {
                collectionContainer.RegisterCollection(collection);
            }
        }

    }
}
