﻿namespace LendingQB.Test.Developers.LendingQB.Core
{
    using System;
    using System.Collections;
    using global::LendingQB.Core;
    using global::LendingQB.Core.Data;
    using LendersOffice.Common;
    using LqbGrammar.DataTypes;
    using LqbGrammar.DataTypes.PathDispatch;
    using NSubstitute;
    using NUnit.Framework;

    [TestFixture]
    public class BaseLqbCollectionDecoratorTest
    {
        [Test]
        public void Indexer_Always_PassesThrough()
        {
            var contained = this.GetFakeCollection();
            var decorator = this.GetDecorator(contained);
            var id = Guid.NewGuid().ToIdentifier<DataObjectKind.RealProperty>();
            var asset = new RealProperty();

            var record = decorator[id];

            var test = contained.Received()[id];
        }

        [Test]
        public void HasChanges_Always_PassesThrough()
        {
            var contained = this.GetFakeCollection();
            var decorator = this.GetDecorator(contained);

            var hasChanges = decorator.HasChanges;

            var test = contained.Received().HasChanges;
        }

        [Test]
        public void Keys_Always_PassesThrough()
        {
            var contained = this.GetFakeCollection();
            var decorator = this.GetDecorator(contained);

            var keys = decorator.Keys;

            var test = contained.Received().Keys;
        }

        [Test]
        public void Values_Always_PassesThrough()
        {
            var contained = this.GetFakeCollection();
            var decorator = this.GetDecorator(contained);

            var values = decorator.Values;

            var test = contained.Received().Values;
        }

        [Test]
        public void Count_Always_PassesThrough()
        {
            var contained = this.GetFakeCollection();
            var decorator = this.GetDecorator(contained);

            var count = decorator.Count;

            var test = contained.Received().Count;
        }

        [Test]
        public void Add_Always_PassesThrough()
        {
            var contained = this.GetFakeCollection();
            var decorator = this.GetDecorator(contained);
            var property = new RealProperty();

            var id = decorator.Add(property);

            contained.Received().Add(property);
        }

        [Test]
        public void AddWithId_Always_PassesThrough()
        {
            var contained = this.GetFakeCollection();
            var decorator = this.GetDecorator(contained);
            var id = Guid.NewGuid().ToIdentifier<DataObjectKind.RealProperty>();
            var property = new RealProperty();

            decorator.Add(id, property);

            contained.Received().Add(id, property);
        }

        [Test]
        public void AddWithoutTracking_Always_PassesThrough()
        {
            var contained = this.GetFakeCollection();
            var decorator = this.GetDecorator(contained);
            var id = Guid.NewGuid().ToIdentifier<DataObjectKind.RealProperty>();
            var property = new RealProperty();

            decorator.AddWithoutTracking(id, property);

            contained.Received().AddWithoutTracking(id, property);
        }

        [Test]
        public void BeginTrackingChanges_Always_PassesThrough()
        {
            var contained = this.GetFakeCollection();
            var decorator = this.GetDecorator(contained);

            decorator.BeginTrackingChanges();

            contained.Received().BeginTrackingChanges();
        }

        [Test]
        public void ContainsKey_Always_PassesThrough()
        {
            var contained = this.GetFakeCollection();
            var decorator = this.GetDecorator(contained);
            var id = Guid.NewGuid().ToIdentifier<DataObjectKind.RealProperty>();

            var contains = decorator.ContainsKey(id);

            contained.Received().ContainsKey(id);
        }

        [Test]
        public void GetAllKeys_Always_PassesThrough()
        {
            var contained = this.GetFakeCollection();
            var decorator = this.GetDecorator(contained);

            var allKeys = decorator.GetAllKeys();

            contained.Received().GetAllKeys();
        }

        [Test]
        public void GetChanges_Always_PassesThrough()
        {
            var contained = this.GetFakeCollection();
            var decorator = this.GetDecorator(contained);

            var changes = decorator.GetChanges();

            contained.Received().GetChanges();
        }

        [Test]
        public void GetElement_Always_PassesThrough()
        {
            var contained = this.GetFakeCollection();
            var decorator = this.GetDecorator(contained);
            var dataPath = Substitute.For<IDataPathElement>();

            var element = decorator.GetElement(dataPath);

            contained.Received().GetElement(dataPath);
        }

        [Test]
        public void GetEnumerator_Always_PassesThrough()
        {
            var contained = this.GetFakeCollection();
            var decorator = this.GetDecorator(contained);

            var enumerator = decorator.GetEnumerator();

            contained.Received().GetEnumerator();
        }

        [Test]
        public void Remove_Always_PassesThrough()
        {
            var contained = this.GetFakeCollection();
            var decorator = this.GetDecorator(contained);
            var id = Guid.NewGuid().ToIdentifier<DataObjectKind.RealProperty>();

            decorator.Remove(id);

            contained.Received().Remove(id);
        }

        [Test]
        public void TryGetValue_Always_PassesThrough()
        {
            var contained = this.GetFakeCollection();
            var decorator = this.GetDecorator(contained);
            var id = Guid.NewGuid().ToIdentifier<DataObjectKind.RealProperty>();

            RealProperty value;
            var exists = decorator.TryGetValue(id, out value);

            contained.Received().TryGetValue(id, out value);
        }

        [Test]
        public void IEnumerableGetEnumerator_Always_PassesThrough()
        {
            var contained = this.GetFakeCollection();
            var decorator = this.GetDecorator(contained);

            var enumerator = ((IEnumerable)decorator).GetEnumerator();

            contained.Received().GetEnumerator();
        }

        private IMutableLqbCollection<DataObjectKind.RealProperty, Guid, RealProperty> GetFakeCollection()
        {
            return Substitute.For<IMutableLqbCollection<DataObjectKind.RealProperty, Guid, RealProperty>, IEnumerable>();
        }

        private TestBaseLqbCollectionDecorator GetDecorator(IMutableLqbCollection<DataObjectKind.RealProperty, Guid, RealProperty> toDecorate)
        {
            return new TestBaseLqbCollectionDecorator(toDecorate);
        }
    }

    internal class TestBaseLqbCollectionDecorator : BaseLqbCollectionDecorator<DataObjectKind.RealProperty, Guid, RealProperty>
    {
        public TestBaseLqbCollectionDecorator(IMutableLqbCollection<DataObjectKind.RealProperty, Guid, RealProperty> contained)
            : base(contained)
        {
        }
    }
}
