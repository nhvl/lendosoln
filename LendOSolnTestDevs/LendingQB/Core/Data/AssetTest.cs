﻿namespace LendingQB.Test.Developers.LendingQB.Core.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using NUnit.Framework;
    using global::LendingQB.Core.Data;

    [TestFixture]
    public class AssetTest
    {
        [Test]
        public void IsLiquidAsset_NullAssetType_ReturnsNull()
        {
            var asset = new Asset();

            // Default value is null, but set it to null just to be explicit.
            asset.AssetType = null;

            Assert.AreEqual(null, asset.IsLiquidAsset);
        }
    }
}
