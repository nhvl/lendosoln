﻿namespace LendingQB.Test.Developers.LendingQB.Core.Data
{
    using global::LendingQB.Core.Data;
    using global::DataAccess;
    using NUnit.Framework;
    using LqbGrammar.DataTypes;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public class AssetTotalsTest
    {
        [Test]
        public void Create_SingleLiquidAssetValue5_ReturnsLiquidAssetValue5()
        {
            var realProperties = new RealProperty[0];
            var asset = new Asset();
            asset.AssetType = E_AssetT.Checking;
            asset.Value = Money.Create(5);
            // Sanity check.
            Assert.AreEqual(true, asset.IsLiquidAsset);

            var totals = AssetTotals.Create(new[] { asset }, realProperties, includeCashDeposit: true);

            Assert.AreEqual(Money.Zero, totals.SubtotalIlliquid);
            Assert.AreEqual(Money.Create(5), totals.SubtotalLiquid);
            Assert.AreEqual(Money.Create(5), totals.Total);
        }

        [Test]
        public void Create_SingleIlliquidAssetValue5_ReturnsIlliquidAssetValue5()
        {
            var realProperties = new RealProperty[0];
            var asset = new Asset();
            asset.AssetType = E_AssetT.OtherIlliquidAsset;
            asset.Value = Money.Create(5);
            // Sanity check.
            Assert.AreEqual(false, asset.IsLiquidAsset);

            var totals = AssetTotals.Create(new[] { asset }, realProperties, includeCashDeposit: true);

            Assert.AreEqual(Money.Create(5), totals.SubtotalIlliquid);
            Assert.AreEqual(Money.Zero, totals.SubtotalLiquid);
            Assert.AreEqual(Money.Create(5), totals.Total);
        }

        [Test]
        public void Create_SingleCashDepositValue5IncludeCashDeposit_ReturnsLiquidAssetValue5()
        {
            var realProperties = new RealProperty[0];
            var asset = new Asset();
            asset.AssetType = E_AssetT.CashDeposit;
            asset.Value = Money.Create(5);
            // Sanity check.
            Assert.AreEqual(true, asset.IsLiquidAsset);

            var totals = AssetTotals.Create(new[] { asset }, realProperties, includeCashDeposit: true);

            Assert.AreEqual(Money.Zero, totals.SubtotalIlliquid);
            Assert.AreEqual(Money.Create(5), totals.SubtotalLiquid);
            Assert.AreEqual(Money.Create(5), totals.Total);
        }

        [Test]
        public void Create_SingleCashDepositValue5ExcludeCashDeposit_ReturnsLiquidAssetValue0()
        {
            var realProperties = new RealProperty[0];
            var asset = new Asset();
            asset.AssetType = E_AssetT.CashDeposit;
            asset.Value = Money.Create(5);
            // Sanity check.
            Assert.AreEqual(true, asset.IsLiquidAsset);

            var totals = AssetTotals.Create(new[] { asset }, realProperties, includeCashDeposit: false);

            Assert.AreEqual(Money.Zero, totals.SubtotalIlliquid);
            Assert.AreEqual(Money.Zero, totals.SubtotalLiquid);
            Assert.AreEqual(Money.Zero, totals.Total);
        }

        [Test]
        public void Create_SingleRealPropertyNotSoldMarketValue5_ReturnsRealPropertyValue5()
        {
            var assets = new Asset[0];
            var realProperty = new RealProperty();
            realProperty.Status = E_ReoStatusT.Rental;
            realProperty.MarketValue = Money.Create(5);

            var totals = AssetTotals.Create(assets, new[] { realProperty }, includeCashDeposit: true);

            Assert.AreEqual(Money.Create(5), totals.SubtotalReo);
            Assert.AreEqual(Money.Zero, totals.SubtotalIlliquid);
            Assert.AreEqual(Money.Zero, totals.SubtotalLiquid);
            Assert.AreEqual(Money.Create(5), totals.Total);
        }

        [Test]
        public void Create_SingleRealPropertySoldMarketValue5_ReturnsRealPropertyValue0()
        {
            var assets = new Asset[0];
            var realProperty = new RealProperty();
            realProperty.Status = E_ReoStatusT.Sale;
            realProperty.MarketValue = Money.Create(5);

            var totals = AssetTotals.Create(assets, new[] { realProperty }, includeCashDeposit: true);

            Assert.AreEqual(Money.Zero, totals.SubtotalReo);
            Assert.AreEqual(Money.Zero, totals.SubtotalIlliquid);
            Assert.AreEqual(Money.Zero, totals.SubtotalLiquid);
            Assert.AreEqual(Money.Zero, totals.Total);
        }
    }
}
