﻿namespace LendingQB.Test.Developers.LendingQB.Core.Data
{
    using System;
    using global::DataAccess;
    using global::LendingQB.Core;
    using global::LendingQB.Core.Data;
    using LqbGrammar.DataTypes;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public sealed class UladApplicationConsumerUnitTest
    {
        [Test]
        public void ConstructAssociation_ShouldSucceed()
        {
            var appId = CreateUladIdentifier();
            var borrowerId = CreateConsumerIdentifier();
            var legAppId = CreateLegacyAppIdentifier();
            var association = CreateAssociation(appId, borrowerId, legAppId);
            Assert.IsNotNull(association);
        }

        [Test]
        public void AddConsumersToOneApplication_ShouldSucceed()
        {
            var appId = CreateUladIdentifier();
            var borrowerId = CreateConsumerIdentifier();
            var coborrowerId = CreateConsumerIdentifier();
            var legAppId = CreateLegacyAppIdentifier();
            var association1 = CreateAssociation(appId, borrowerId, legAppId);
            var association2 = CreateAssociation(appId, coborrowerId, legAppId);

            var assocSet = CreateAssociationSet();
            assocSet.Add(association1);
            assocSet.Add(association2);
            Assert.AreEqual(2, assocSet.Count);
        }

        [Test]
        public void AddSameConsumerToTwoApplications_ShouldFail()
        {
            var appId1 = CreateUladIdentifier();
            var appId2 = CreateUladIdentifier();
            var borrowerId = CreateConsumerIdentifier();
            var legAppId = CreateLegacyAppIdentifier();
            var association1 = CreateAssociation(appId1, borrowerId, legAppId);
            var association2 = CreateAssociation(appId2, borrowerId, legAppId);

            var assocSet = CreateAssociationSet();
            assocSet.Add(association1);
            Assert.Throws<CBaseException>(() => assocSet.Add(association2));
        }

        internal static DataObjectIdentifier<DataObjectKind.Consumer, Guid> CreateConsumerIdentifier()
        {
            var guid = Guid.NewGuid();
            return DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(guid);
        }

        internal static DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> CreateLegacyAppIdentifier()
        {
            var guid = Guid.NewGuid();
            return DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>.Create(guid);
        }

        internal static DataObjectIdentifier<DataObjectKind.UladApplication, Guid> CreateUladIdentifier()
        {
            var guid = Guid.NewGuid();
            return DataObjectIdentifier<DataObjectKind.UladApplication, Guid>.Create(guid);
        }

        internal static UladApplicationConsumerAssociation CreateAssociation(
            DataObjectIdentifier<DataObjectKind.UladApplication, Guid> uladId,
            DataObjectIdentifier<DataObjectKind.Consumer, Guid> borrowerId,
            DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> appId)
        {
            return new UladApplicationConsumerAssociation(uladId, borrowerId, appId);
        }

        internal static IMutableLqbAssociationSet<DataObjectKind.UladApplicationConsumerAssociation, Guid, UladApplicationConsumerAssociation> CreateAssociationSet()
        {
            var name = Name.Create("Test").Value;
            var identifierFactory = new GuidDataObjectIdentifierFactory<DataObjectKind.UladApplicationConsumerAssociation>();
            int firstCardinality = -1;
            int secondCardinality = 1;

            return LqbAssociationSet<DataObjectKind.UladApplicationConsumerAssociation, Guid, UladApplicationConsumerAssociation>.Create(name, identifierFactory, firstCardinality, secondCardinality);
        }
    }
}