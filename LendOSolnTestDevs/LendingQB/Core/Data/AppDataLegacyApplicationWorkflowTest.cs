﻿namespace LendingQB.Test.Developers.LendingQB.Core.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Utils;
    using LendersOffice.Security;
    using NUnit.Framework;
    using global::DataAccess;
    using LendersOffice.Constants;
    using System.Threading;
    using LqbGrammar.DataTypes;

    // This was used to manually verify on my local. I'm going to exclude it from NUnit
    // because it is so fragile.
    [Ignore]
    [TestFixture]
    [Category(CategoryConstants.IntegrationTest)]
    public class AppDataLegacyApplicationWorkflowTest
    {
        private Guid thinhTest2BrokerId = new Guid("839127ef-72d1-4873-bfdf-18a23416b146");
        private Guid geoffreyf1UserId = new Guid("102e2ed9-6c10-42a6-bc46-da2e9935edac");
        private Guid geoffreyf2UserId = new Guid("9a29c724-f515-498b-ba55-639f31e3f826");
        private AbstractUserPrincipal principal;
        private Guid loanId;

        [TestFixtureSetUp]
        public void FixtureSetUp()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);
                helper.RegisterRealDriver(FoolHelper.DriverType.MessageQueue);
                helper.RegisterRealDriver(FoolHelper.DriverType.RegularExpression);

                this.principal = PrincipalFactory.Create(thinhTest2BrokerId, geoffreyf2UserId, "B", true, false);
                var creator = CLoanFileCreator.GetCreator(
                    this.principal,
                    LendersOffice.Audit.E_LoanCreationSource.UserCreateFromBlank);
                this.loanId = creator.CreateBlankLoanFile();

                Thread.CurrentPrincipal = this.principal;
            }
        }

        [TestFixtureTearDown]
        public void FixtureTearDown()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);
                helper.RegisterRealDriver(FoolHelper.DriverType.MessageQueue);
                helper.RegisterRealDriver(FoolHelper.DriverType.RegularExpression);

                Tools.DeclareLoanFileInvalid(
                    this.principal,
                    this.loanId,
                    sendNotifications: false,
                    generateLinkLoanMsgEvent: false);
            }
        }

        [TearDown]
        public void TearDown()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);
                helper.RegisterRealDriver(FoolHelper.DriverType.MessageQueue);
                helper.RegisterRealDriver(FoolHelper.DriverType.RegularExpression);

                var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                    this.loanId,
                    typeof(AppDataLegacyApplicationCAppDataMappingTest));
                loan.InitSave(ConstAppDavid.SkipVersionCheck);
                loan.GetAppData(0).aBDob = CDateTime.InvalidWrapValue;
                loan.GetAppData(0).aAltNm1 = "";
                loan.Save();
            }
        }

        [Test]
        public void ViolateFieldProtectionRule_SetValuesThroughCAppData_ThrowsException()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);
                helper.RegisterRealDriver(FoolHelper.DriverType.MessageQueue);
                helper.RegisterRealDriver(FoolHelper.DriverType.RegularExpression);

                var loan = CPageData.CreateUsingSmartDependency(
                    this.loanId,
                    typeof(AppDataLegacyApplicationWorkflowTest));
                loan.InitSave(ConstAppDavid.SkipVersionCheck);
                loan.GetAppData(0).aBDob_rep = "5/1/90";
                loan.Save();

                loan.InitSave(ConstAppDavid.SkipVersionCheck);
                loan.GetAppData(0).aAltNm1 = "Alternate Name";

                Assert.Throws<LoanFieldWritePermissionDenied>(() => loan.Save());
            }
        }

        [Test]
        public void ViolateFieldProtectionRule_SetValuesThroughAppDataLegacyApplication_ThrowsException()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);
                helper.RegisterRealDriver(FoolHelper.DriverType.MessageQueue);
                helper.RegisterRealDriver(FoolHelper.DriverType.RegularExpression);

                var loan = CPageData.CreateUsingSmartDependency(
                    this.loanId,
                    typeof(AppDataLegacyApplicationWorkflowTest));
                loan.InitSave(ConstAppDavid.SkipVersionCheck);
                loan.Consumers.ElementAt(0).Value.Dob = UnzonedDate.Create(1990, 5, 1);
                loan.Save();

                loan.InitSave(ConstAppDavid.SkipVersionCheck);
                loan.LegacyApplications.ElementAt(0).Value.AltName1 = PersonName.Create("Alternate Name");

                Assert.Throws<LoanFieldWritePermissionDenied>(() => loan.Save());
            }
        }
    }
}
