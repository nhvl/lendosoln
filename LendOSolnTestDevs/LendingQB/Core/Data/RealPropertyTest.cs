﻿namespace LendingQB.Test.Developers.LendingQB.Core.Data
{
    using global::DataAccess;
    using global::LendingQB.Core.Data;
    using LqbGrammar.DataTypes;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public class RealPropertyTest
    {
        [Test]
        public void NetValue_ValidMarketValueAndMortgageAmount_EqualsMarketValueMinusMortgageAmount()
        {
            var property = new RealProperty();

            property.MarketValue = Money.Create(3);
            property.MtgAmt = Money.Create(2);

            Assert.AreEqual(Money.Create(1), property.NetValue);
        }

        [Test]
        public void NetValue_ValidMarketValueNullMortgageAmount_EqualsNull()
        {
            var property = new RealProperty();

            property.MarketValue = Money.Create(3);
            property.MtgAmt = null;

            Assert.AreEqual(null, property.NetValue);
        }

        [Test]
        public void NetValue_NullMarketValueValidMortgageAmount_EqualsNull()
        {
            var property = new RealProperty();

            property.MarketValue = null;
            property.MtgAmt = Money.Create(2);

            Assert.AreEqual(null, property.NetValue);
        }

        [Test]
        public void NetValue_NullMarketValueNullMortgageAmount_EqualsNull()
        {
            var property = new RealProperty();

            property.MarketValue = null;
            property.MtgAmt = null;

            Assert.AreEqual(null, property.NetValue);
        }

        [Test]
        public void NetRentInc_NullNetRentIncLocked_ReturnsNull()
        {
            var property = new RealProperty();

            property.NetRentIncLocked = null;

            Assert.AreEqual(null, property.NetRentInc);
        }

        [Test]
        public void NetRentInc_LockedWithNullData_ReturnsNull()
        {
            var property = new RealProperty();

            property.NetRentIncLocked = true;
            property.NetRentIncData = null;

            Assert.AreEqual(null, property.NetRentInc);
        }

        [Test]
        public void NetRentInc_LockedWithNonNullData_ReturnsDataValue()
        {
            var property = new RealProperty();

            property.NetRentIncLocked = true;
            property.NetRentIncData = Money.OneDollar;

            Assert.AreEqual(Money.OneDollar, property.NetRentInc);
        }

        [Test]
        public void NetRentInc_NotLockedWithValidCalc_ReturnsCalcValue()
        {
            var property = new RealProperty();

            property.Status = E_ReoStatusT.Rental;
            property.OccR = Percentage.Create(100);
            property.GrossRentInc = Money.Create(1500);
            property.HExp = Money.Zero;
            property.MtgPmt = Money.Zero;
            property.NetRentIncLocked = false;
            property.NetRentIncData = Money.OneDollar;

            Assert.AreEqual(Money.Create(1500), property.NetRentInc);
        }

        private object[][] NetRentIncTestCases = new object[][]
        {
            new object[] { 1, null, true, Percentage.Create(1), Money.OneDollar, Money.OneDollar, Money.OneDollar, null },
            new object[] { 1, E_ReoStatusT.Residence, null, Percentage.Create(1), Money.OneDollar, Money.OneDollar, Money.OneDollar, null },
            new object[] { 1, E_ReoStatusT.Rental, false, null, Money.OneDollar, Money.OneDollar, Money.OneDollar, null },
            new object[] { 1, E_ReoStatusT.Rental, false, Percentage.Create(1), null, Money.OneDollar, Money.OneDollar, null },
            new object[] { 1, E_ReoStatusT.Rental, false, Percentage.Create(1), Money.OneDollar, null, Money.OneDollar, null },
            new object[] { 1, E_ReoStatusT.Rental, false, Percentage.Create(1), Money.OneDollar, Money.OneDollar, null, null },
            new object[] { 1, E_ReoStatusT.Rental, false, Percentage.Create(1), Money.OneDollar, Money.OneDollar, null, null },
            new object[] { 1, E_ReoStatusT.Sale, true, Percentage.Create(100), Money.Create(1500), Money.Create(250), Money.Create(750), Money.Zero },
            new object[] { 1, E_ReoStatusT.Residence, false, Percentage.Create(100), Money.Create(1500), Money.Create(250), Money.Create(750), Money.Zero },
            new object[] { 1, E_ReoStatusT.PendingSale, false, Percentage.Create(100), Money.Create(1500), Money.Create(250), Money.Create(750), Money.Zero },
            new object[] { 1, E_ReoStatusT.Residence, true, Percentage.Create(100), Money.Create(1500), Money.Create(250), Money.Create(750), Money.Create(500) },
            new object[] { 1, E_ReoStatusT.PendingSale, true, Percentage.Create(100), Money.Create(1500), Money.Create(250), Money.Create(750), Money.Create(500) },
            new object[] { 1, E_ReoStatusT.Rental, true, Percentage.Create(100), Money.Create(1500), Money.Create(250), Money.Create(750), Money.Create(500) },
        };

        [TestCaseSource(nameof(NetRentIncTestCases))]
        public void NetRentIncCalc_VariousInputs_ReturnsExpected(
            int id,
            E_ReoStatusT? status,
            bool? isForceCalcNetRentalInc,
            Percentage? occR,
            Money? grossRentInc,
            Money? housingExp,
            Money? mtgPmt,
            Money? expected)
        {
            var property = new RealProperty();

            property.Status = status;
            property.IsForceCalcNetRentalInc = isForceCalcNetRentalInc;
            property.OccR = occR;
            property.GrossRentInc = grossRentInc;
            property.HExp = housingExp;
            property.MtgPmt = mtgPmt;

            Assert.AreEqual(expected, property.NetRentIncCalc);
        }

        [Test]
        [TestCase(global::DataAccess.E_ReoTypeT._2_4Plx, "2-4PLX")]
        [TestCase(global::DataAccess.E_ReoTypeT.ComNR, "COM-NR")]
        [TestCase(global::DataAccess.E_ReoTypeT.ComR, "COM-R")]
        [TestCase(global::DataAccess.E_ReoTypeT.Condo, "CONDO")]
        [TestCase(global::DataAccess.E_ReoTypeT.Coop, "COOP")]
        [TestCase(global::DataAccess.E_ReoTypeT.Farm, "FARM")]
        [TestCase(global::DataAccess.E_ReoTypeT.Land, "LAND")]
        [TestCase(global::DataAccess.E_ReoTypeT.LeaveBlank, "")]
        [TestCase(global::DataAccess.E_ReoTypeT.Mixed, "MIXED")]
        [TestCase(global::DataAccess.E_ReoTypeT.Mobil, "MOBIL")]
        [TestCase(global::DataAccess.E_ReoTypeT.Multi, "MULTI")]
        [TestCase(global::DataAccess.E_ReoTypeT.SFR, "SFR")]
        [TestCase(global::DataAccess.E_ReoTypeT.Town, "TOWN")]
        [TestCase(global::DataAccess.E_ReoTypeT.Other, "OTHER")]
        public void TypeCode_ReturnsCorrectValue(global::DataAccess.E_ReoTypeT enumValue, string expectedStringValue)
        {
            var result = LendersOffice.CalculatedFields.RealProperty.TypeCode(enumValue);
            Assert.AreEqual(expectedStringValue, result);
        }

        [Test]
        [TestCase("2-4PLX", global::DataAccess.E_ReoTypeT._2_4Plx)]
        [TestCase("4PLX", global::DataAccess.E_ReoTypeT._2_4Plx)] // Incorrect value from case 473311
        [TestCase("COM-NR", global::DataAccess.E_ReoTypeT.ComNR)]
        [TestCase("COM-R", global::DataAccess.E_ReoTypeT.ComR)]
        [TestCase("CONDO", global::DataAccess.E_ReoTypeT.Condo)]
        [TestCase("COOP", global::DataAccess.E_ReoTypeT.Coop)]
        [TestCase("FARM", global::DataAccess.E_ReoTypeT.Farm)]
        [TestCase("LAND", global::DataAccess.E_ReoTypeT.Land)]
        [TestCase("", global::DataAccess.E_ReoTypeT.LeaveBlank)]
        [TestCase("MIXED", global::DataAccess.E_ReoTypeT.Mixed)]
        [TestCase("MOBIL", global::DataAccess.E_ReoTypeT.Mobil)]
        [TestCase("MULTI", global::DataAccess.E_ReoTypeT.Multi)]
        [TestCase("SFR", global::DataAccess.E_ReoTypeT.SFR)]
        [TestCase("TOWN", global::DataAccess.E_ReoTypeT.Town)]
        [TestCase("OTHER", global::DataAccess.E_ReoTypeT.Other)]
        public void TypeFromCode_ReturnsCorrectValue(string stringValue, global::DataAccess.E_ReoTypeT expectedEnumValue)
        {
            var result = LendersOffice.CalculatedFields.RealProperty.TypeFromCode(stringValue);
            Assert.AreEqual(expectedEnumValue, result);
        }
    }
}
