﻿namespace LendingQB.Test.Developers.LendingQB.Core.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using global::LendingQB.Core.Commands;
    using global::LendingQB.Core.Data;
    using LendersOffice.Common;
    using LendersOffice.ObjLib.LqbDataService;
    using LqbGrammar.DataTypes;
    using LqbGrammar.DataTypes.PathDispatch;
    using NSubstitute;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public class EmploymentRecordTest
    {
        [Test]
        public void EmploymentYears_NullEmploymentStartDate_ReturnsNull()
        {
            var employmentRecord = this.GetEmploymentRecord();

            Assert.AreEqual(null, employmentRecord.EmploymentYears);
        }

        [Test]
        public void EmploymentYears_BeganMarch2010EndedFebruary2015_Returns4()
        {
            var employmentRecord = this.GetEmploymentRecord();

            employmentRecord.EmploymentStartDate = UnzonedDate.Create(2010, 3, 1);
            employmentRecord.EmploymentEndDate = UnzonedDate.Create(2015, 2, 1);

            Assert.AreEqual(Count<UnitType.Year>.Create(4), employmentRecord.EmploymentYears);
        }

        [Test]
        public void EmploymentYears_BeganMarchFirst2010EndedMarchFirst2015_Returns5()
        {
            var employmentRecord = this.GetEmploymentRecord();

            employmentRecord.EmploymentStartDate = UnzonedDate.Create(2010, 3, 1);
            employmentRecord.EmploymentEndDate = UnzonedDate.Create(2015, 3, 1);

            Assert.AreEqual(Count<UnitType.Year>.Create(5), employmentRecord.EmploymentYears);
        }

        [Test]
        public void EmploymentYears_BeganMarchFirst2010NoEndDate_ReturnsExpectedResult()
        {
            var employmentRecord = this.GetEmploymentRecord();

            var marchFirst2010 = new DateTime(2010, 3, 1);
            employmentRecord.EmploymentStartDate = UnzonedDate.Create(marchFirst2010);
            employmentRecord.EmploymentEndDate = null;

            bool partialYear = IsBeforeDateInYear(DateTime.Today, marchFirst2010);
            int expected = DateTime.Today.Year - marchFirst2010.Year - (partialYear ? 1 : 0);

            Assert.AreEqual(Count<UnitType.Year>.Create(expected), employmentRecord.EmploymentYears);
        }

        [Test]
        public void ProfessionYears_BeganFebruaryFifteenth1980_ReturnsExpectedResult()
        {
            var employmentRecord = this.GetEmploymentRecord();

            var februaryFifteenth1980 = new DateTime(1980, 2, 15);
            employmentRecord.ProfessionStartDate = UnzonedDate.Create(februaryFifteenth1980);
            employmentRecord.EmploymentEndDate = null;

            bool partialYear = IsBeforeDateInYear(DateTime.Today, februaryFifteenth1980);
            int expected = DateTime.Today.Year - februaryFifteenth1980.Year - (partialYear ? 1 : 0);

            Assert.AreEqual(Count<UnitType.Year>.Create(expected), employmentRecord.ProfessionYears);
        }

        [Test]
        public void VerifHasSignature_NoSignature_ReturnsFalse()
        {
            var employmentRecord = this.GetEmploymentRecord();

            Assert.AreEqual(false, employmentRecord.VerifHasSignature);
        }

        [Test]
        public void VerifHasSignature_HasSignature_ReturnsTrue()
        {
            var employmentRecord = this.GetEmploymentRecord();

            employmentRecord.SetVerifSignature(
                DataObjectIdentifier<DataObjectKind.Employee, Guid>.Create(Guid.Empty),
                DataObjectIdentifier<DataObjectKind.Signature, Guid>.Create(Guid.Empty));

            Assert.AreEqual(true, employmentRecord.VerifHasSignature);
        }

        [Test]
        public void SetVerifSignature_Always_SetsVerifSigningEmployeeId()
        {
            var employmentRecord = this.GetEmploymentRecord();
            var employeeId = DataObjectIdentifier<DataObjectKind.Employee, Guid>.Create(new Guid("11111111-1111-1111-1111-111111111111"));
            var signatureId = DataObjectIdentifier<DataObjectKind.Signature, Guid>.Create(new Guid("22222222-2222-2222-2222-222222222222"));

            employmentRecord.SetVerifSignature(
                employeeId,
                signatureId);

            Assert.AreEqual(employeeId, employmentRecord.VerifSigningEmployeeId);
        }

        [Test]
        public void SetVerifSignature_Always_SetsVerifSignatureImgId()
        {
            var employmentRecord = this.GetEmploymentRecord();
            var employeeId = DataObjectIdentifier<DataObjectKind.Employee, Guid>.Create(new Guid("11111111-1111-1111-1111-111111111111"));
            var signatureId = DataObjectIdentifier<DataObjectKind.Signature, Guid>.Create(new Guid("22222222-2222-2222-2222-222222222222"));

            employmentRecord.SetVerifSignature(
                employeeId,
                signatureId);

            Assert.AreEqual(signatureId, employmentRecord.VerifSignatureImgId);
        }

        [Test]
        public void HandleAdd_IncomeRecord_AddsRecord()
        {
            var employment = this.GetEmploymentRecord();
            var addEntity = new AddEntity(null, nameof(EmploymentRecord.IncomeRecords), null);

            Guid id = ((IAddEntityHandler)employment).HandleAdd(addEntity, null);

            var identifier = id.ToIdentifier<DataObjectKind.IncomeRecord>();
            Assert.IsTrue(employment.IncomeRecords.ContainsKey(identifier));
        }

        [Test]
        public void HandleAdd_IncomeRecordWithSpecifiedFields_AddsRecordWithValues()
        {
            var employment = this.GetEmploymentRecord();
            var requestNode = new RequestNode(
                null,
                new[]
                {
                    new RequestNode(new DataPathBasicElement(nameof(IncomeRecord.IncomeType)), null, "10"),
                    new RequestNode(new DataPathBasicElement(nameof(IncomeRecord.MonthlyAmount)), null, "$100.00")
                });
            var addEntity = new AddEntity(null, nameof(EmploymentRecord.IncomeRecords), requestNode);
            var parser = new FormatEntityFactory().CreateStringParser(global::DataAccess.FormatTarget.Webform, global::DataAccess.FormatDirection.ToDb);

            Guid id = ((IAddEntityHandler)employment).HandleAdd(addEntity, parser);

            var identifier = id.ToIdentifier<DataObjectKind.IncomeRecord>();
            var incomeRecord = employment.IncomeRecords[identifier];
            Assert.AreEqual(IncomeType.MilitaryClothesAllowance, incomeRecord.IncomeType);
            Assert.AreEqual((Money)100, incomeRecord.MonthlyAmount);
        }

        [Test]
        public void HandleAdd_NotIncomeRecords_Throws()
        {
            var employment = this.GetEmploymentRecord();
            var addEntity = new AddEntity(null, "NONEXISTENT_NESTED_COLLECTION", null);

            Assert.Throws<global::DataAccess.CBaseException>(() => ((IAddEntityHandler)employment).HandleAdd(addEntity, null));
        }

        [Test]
        public void HasChanges_OnlyIncomeRecordCollectionModified_ReturnsTrue()
        {
            var employment = this.GetEmploymentRecord();
            employment.IncomeRecords.Add(new IncomeRecord());

            bool hasChanges = employment.HasChanges;

            Assert.IsTrue(hasChanges);
        }

        [Test]
        public void GetChanges_UpdatedIncomeRecordCollection_ReturnsWholeCollection()
        {
            var employment = this.GetEmploymentRecord();
            employment.IncomeRecords.Add(new IncomeRecord());

            IEnumerable<KeyValuePair<string, object>> changes = employment.GetChanges();

            Assert.AreEqual(1, changes.Count());
            Assert.AreEqual(nameof(EmploymentRecord.IncomeRecords), changes.Single().Key);
            Assert.AreEqual(employment.IncomeRecords, changes.Single().Value);
        }

        [Test]
        public void HandleRemove_ValidIncomeRecordId_RemovesRecord()
        {
            var employment = this.GetEmploymentRecord();
            var incomeId = employment.IncomeRecords.Add(new IncomeRecord());
            Assert.AreEqual(1, employment.IncomeRecords.Count);
            var removeEntity = new RemoveEntity(null, "IncomeRecords", incomeId.Value);

            ((IRemoveEntityHandler)employment).HandleRemove(removeEntity);

            Assert.AreEqual(0, employment.IncomeRecords.Count);
        }

        [Test]
        public void HandleRemove_InvalidCollectionName_Throws()
        {
            var employment = this.GetEmploymentRecord();
            var removeEntity = new RemoveEntity(null, "NONEXISTENT_NESTED_COLLECTION", Guid.Empty);

            Assert.Throws<global::DataAccess.CBaseException>(() => ((IRemoveEntityHandler)employment).HandleRemove(removeEntity));
        }

        /// <summary>
        /// Indicates if <paramref name="firstDateTime"/> occurs before <paramref name="secondDateTime"/> if both dates are in the same year.
        /// </summary>
        private static bool IsBeforeDateInYear(DateTime firstDateTime, DateTime secondDateTime)
        {
            return firstDateTime.Month < secondDateTime.Month || (firstDateTime.Month == secondDateTime.Month && firstDateTime.Day < secondDateTime.Day);
        }

        private EmploymentRecord GetEmploymentRecord()
        {
            return new EmploymentRecord(Substitute.For<IEmploymentRecordDefaultsProvider>());
        }
    }
}
