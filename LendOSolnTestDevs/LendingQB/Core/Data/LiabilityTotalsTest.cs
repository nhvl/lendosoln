﻿namespace LendingQB.Test.Developers.LendingQB.Core.Data
{
    using System;
    using global::DataAccess;
    using global::LendingQB.Core.Data;
    using LqbGrammar.DataTypes;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public class LiabilityTotalsTest
    {
        [Test]
        public void Create_NullInput_ThrowsException()
        {
            Assert.Throws<ArgumentNullException>(() => LiabilityTotals.Create(null));
        }

        [Test]
        public void Create_Always_SumsBalanceOfAllLiabilities()
        {
            var liabilities = new[]
            {
                new Liability(defaultsProvider: null) { Bal = Money.Create(1) },
                new Liability(defaultsProvider: null) { Bal = Money.Create(2) },
                new Liability(defaultsProvider: null) { Bal = Money.Create(3) }
            };

            var totals = LiabilityTotals.Create(liabilities);

            Assert.AreEqual(Money.Create(6), totals.Balance);
        }

        [Test]
        public void Create_WithSingleLiabilityNotUsedInRatio_ExcludesThatLiabilityFromPayment()
        {
            var liabilities = new[]
            {
                new Liability(defaultsProvider: null) { DebtType = E_DebtT.Revolving, UsedInRatioData = true, Pmt = Money.Create(1) },
                new Liability(defaultsProvider: null) { DebtType = E_DebtT.Revolving, UsedInRatioData = false,  Pmt = Money.Create(2) }
            };
            // Sanity checks -- the field is calculated.
            Assert.AreEqual(true, liabilities[0].UsedInRatio);
            Assert.AreEqual(false, liabilities[1].UsedInRatio);

            var totals = LiabilityTotals.Create(liabilities);

            Assert.AreEqual(Money.Create(1), totals.Payment);
        }

        [Test]
        public void Create__WithSingleLiabilityMarkedToBePaidOffAtClosing_OnlyIncludesThatLiabilityInPayoffAmount()
        {
            var liabilities = new[]
            {
                new Liability(defaultsProvider: null) { WillBePdOff = true, PayoffTimingLockedData = true, PayoffTimingData = PayoffTiming.AtClosing, PayoffAmtLocked = true, PayoffAmtData = Money.Create(1) },
                new Liability(defaultsProvider: null) { WillBePdOff = true, PayoffTimingLockedData = true, PayoffTimingData = PayoffTiming.BeforeClosing, PayoffAmtLocked = true, PayoffAmtData = Money.Create(2) },
                new Liability(defaultsProvider: null) { PayoffAmtLocked = true, PayoffAmtData = Money.Create(3) }
            };
            // Sanity checks -- payoff amount is calculated.
            Assert.AreEqual(Money.Create(1), liabilities[0].PayoffAmt);
            Assert.AreEqual(Money.Create(2), liabilities[1].PayoffAmt);
            Assert.AreEqual(Money.Create(3), liabilities[2].PayoffAmt);

            var totals = LiabilityTotals.Create(liabilities);

            Assert.AreEqual(Money.Create(1), totals.PaidOff);
        }
    }
}
