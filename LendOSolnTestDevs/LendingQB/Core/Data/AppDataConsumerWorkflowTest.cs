﻿namespace LendingQB.Test.Developers.LendingQB.Core.Data
{
    using System;
    using System.Linq;
    using Utils;
    using LendersOffice.Security;
    using NUnit.Framework;
    using global::DataAccess;
    using LendersOffice.Constants;
    using System.Threading;
    using LqbGrammar.DataTypes;

    // This was used to manually verify on my local. I'm going to exclude it from NUnit
    // because it is so fragile.
    [Ignore]
    [TestFixture]
    [Category(CategoryConstants.IntegrationTest)]
    public class AppDataConsumerWorkflowTest
    {
        private Guid thinhTest2BrokerId = new Guid("839127ef-72d1-4873-bfdf-18a23416b146");
        private Guid geoffreyf1UserId = new Guid("102e2ed9-6c10-42a6-bc46-da2e9935edac");
        private Guid geoffreyf2UserId = new Guid("9a29c724-f515-498b-ba55-639f31e3f826");
        private AbstractUserPrincipal principal;
        private Guid loanId;

        [TestFixtureSetUp]
        public void FixtureSetUp()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);
                helper.RegisterRealDriver(FoolHelper.DriverType.MessageQueue);
                helper.RegisterRealDriver(FoolHelper.DriverType.RegularExpression);

                this.principal = PrincipalFactory.Create(thinhTest2BrokerId, geoffreyf2UserId, "B", true, false);
                var creator = CLoanFileCreator.GetCreator(
                    this.principal,
                    LendersOffice.Audit.E_LoanCreationSource.UserCreateFromBlank);
                this.loanId = creator.CreateBlankLoanFile();

                Thread.CurrentPrincipal = this.principal;
            }
        }

        [TestFixtureTearDown]
        public void FixtureTearDown()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);
                helper.RegisterRealDriver(FoolHelper.DriverType.MessageQueue);
                helper.RegisterRealDriver(FoolHelper.DriverType.RegularExpression);

                Tools.DeclareLoanFileInvalid(
                    this.principal,
                    this.loanId,
                    sendNotifications: false,
                    generateLinkLoanMsgEvent: false);
            }
        }

        [TearDown]
        public void TearDown()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);
                helper.RegisterRealDriver(FoolHelper.DriverType.MessageQueue);
                helper.RegisterRealDriver(FoolHelper.DriverType.RegularExpression);

                var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                    this.loanId,
                    typeof(AppDataConsumerWorkflowTest));
                loan.InitSave(ConstAppDavid.SkipVersionCheck);
                loan.GetAppData(0).aBDob = CDateTime.InvalidWrapValue;
                loan.GetAppData(0).aBCellPhone = "";
                loan.Save();
            }
        }

        [Test]
        public void ViolateFieldProtectionRule_SetValuesThroughCAppData_ThrowsException()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);
                helper.RegisterRealDriver(FoolHelper.DriverType.MessageQueue);
                helper.RegisterRealDriver(FoolHelper.DriverType.RegularExpression);

                CPageData loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                    this.loanId,
                    typeof(AppDataConsumerWorkflowTest));
                loan.InitSave(ConstAppDavid.SkipVersionCheck);
                loan.GetAppData(0).aBDob_rep = "5/1/90";
                loan.Save();

                loan = CPageData.CreateUsingSmartDependency(
                    this.loanId,
                    typeof(AppDataConsumerWorkflowTest));
                loan.InitSave(ConstAppDavid.SkipVersionCheck);
                loan.GetAppData(0).aBCellPhone = "111-111-1111";

                Assert.Throws<LoanFieldWritePermissionDenied>(() => loan.Save());
            }
        }

        [Test]
        public void ViolateFieldProtectionRule_SetValuesThroughAppDataConsumer_ThrowsException()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);
                helper.RegisterRealDriver(FoolHelper.DriverType.MessageQueue);
                helper.RegisterRealDriver(FoolHelper.DriverType.RegularExpression);

                CPageData loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                    this.loanId,
                    typeof(AppDataConsumerWorkflowTest));
                loan.InitSave(ConstAppDavid.SkipVersionCheck);
                loan.Consumers.ElementAt(0).Value.Dob = UnzonedDate.Create(1990, 5, 1);
                loan.Save();

                loan = CPageData.CreateUsingSmartDependency(
                    this.loanId,
                    typeof(AppDataConsumerWorkflowTest));
                loan.InitSave(ConstAppDavid.SkipVersionCheck);
                loan.Consumers.ElementAt(0).Value.CellPhone = PhoneNumber.Create("111-111-1111");

                Assert.Throws<LoanFieldWritePermissionDenied>(() => loan.Save());
            }
        }
    }
}
