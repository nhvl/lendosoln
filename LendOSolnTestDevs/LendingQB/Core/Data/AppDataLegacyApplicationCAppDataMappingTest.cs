﻿namespace LendingQB.Test.Developers.LendingQB.Core.Data
{
    using System;
    using Utils;
    using LendersOffice.Security;
    using NUnit.Framework;
    using global::DataAccess;
    using LendersOffice.Constants;
    using global::LendingQB.Core.Data;
    using LqbGrammar.DataTypes;
    using global::LendingQB.Core;

    [TestFixture]
    [Category(CategoryConstants.IntegrationTest)]
    public class AppDataLegacyApplicationCAppDataMappingTest
    {
        private AbstractUserPrincipal principal;
        private Guid loanId;

        [TestFixtureSetUp]
        public void FixtureSetUp()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                this.principal = LoginTools.LoginAs(TestAccountType.LoTest001);
                var creator = CLoanFileCreator.GetCreator(
                    this.principal,
                    LendersOffice.Audit.E_LoanCreationSource.UserCreateFromBlank);
                this.loanId = creator.CreateBlankLoanFile();
            }
        }

        [TestFixtureTearDown]
        public void FixtureTearDown()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                Tools.DeclareLoanFileInvalid(
                    this.principal,
                    this.loanId,
                    sendNotifications: false,
                    generateLinkLoanMsgEvent: false);
            }
        }

        [TearDown]
        public void TearDown()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                    this.loanId,
                    typeof(AppDataLegacyApplicationCAppDataMappingTest));
                loan.InitSave(ConstAppDavid.SkipVersionCheck);
                loan.GetAppData(0).aAltNm1 = "";
                loan.Save();
            }
        }

        [Test]
        public void GetLegacyApplicationAltName1_LoanLoadedViaSmartDependency_DoesNotThrowException()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                    this.loanId,
                    typeof(AppDataLegacyApplicationCAppDataMappingTest));
                loan.InitLoad();
                var legacyAppId = DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>.Create(loan.GetAppData(0).aAppId);
                IReadOnlyLqbCollection<DataObjectKind.LegacyApplication, Guid, ILegacyApplication> legacyApplications = loan.LegacyApplications;

                var firstName = legacyApplications[legacyAppId].AltName1;
            }
        }

        [Test]
        public void SetLegacyApplicationAltName1_Always_PersistsOnLoanSave()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                    this.loanId,
                    typeof(AppDataLegacyApplicationCAppDataMappingTest));
                loan.InitSave(ConstAppDavid.SkipVersionCheck);
                var legacyAppId = DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>.Create(loan.GetAppData(0).aAppId);

                loan.LegacyApplications[legacyAppId].AltName1 = PersonName.Create("Test");
                loan.Save();
                loan.InitLoad();

                Assert.AreEqual("Test", loan.LegacyApplications[legacyAppId].AltName1.ToString());
            }
        }

        [Test]
        public void SetaAltNm1OnPrimaryApp_Always_SetsAltName1OfFirstLegacyApplication()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                    this.loanId,
                    typeof(AppDataLegacyApplicationCAppDataMappingTest));
                loan.InitLoad();
                var primaryApp = loan.GetAppData(0);
                var legacyAppId = DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>.Create(primaryApp.aAppId);

                primaryApp.aAltNm1 = "Test";

                Assert.AreEqual("Test", loan.LegacyApplications[legacyAppId].AltName1.ToString());
            }
        }

        [Test]
        public void SetaAltNm1OnPrimaryApp_Always_PersistsAltName1OfFirstLegacyApplication()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                    this.loanId,
                    typeof(AppDataLegacyApplicationCAppDataMappingTest));
                loan.InitSave(ConstAppDavid.SkipVersionCheck);
                var primaryApp = loan.GetAppData(0);
                var legacyAppId = DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>.Create(primaryApp.aAppId);

                primaryApp.aAltNm1 = "Test";
                loan.Save();
                loan.InitLoad();

                Assert.AreEqual("Test", loan.LegacyApplications[legacyAppId].AltName1.ToString());
            }
        }
    }
}
