﻿namespace LendingQB.Test.Developers.LendingQB.Core.Data
{
    using System;
    using System.Linq;
    using global::LendingQB.Core.Data;
    using LendersOffice.Common;
    using LqbGrammar.DataTypes;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public class IncomeSourceTotalsTest
    {
        [Test]
        public void Create_NullInput_ThrowsException()
        {
            Assert.Throws<ArgumentNullException>(() => IncomeSourceTotals.Create(null));
        }

        [Test]
        public void Create_NoItems_MonthlyAmountZero()
        {
            var items = new IncomeSource[]
            {
            };

            var totals = IncomeSourceTotals.Create(items);

            Assert.AreEqual(Money.Zero, totals.MonthlyAmount);
        }

        [Test]
        public void Create_OneItem_MonthlyAmountEqualToItem()
        {
            var items = new IncomeSource[]
            {
                new IncomeSource { MonthlyAmountData = 487933.23M, },
            };

            var totals = IncomeSourceTotals.Create(items);

            Assert.AreEqual(items.Single().MonthlyAmountData, totals.MonthlyAmount);
        }

        [Test]
        public void Create_ManyItems_MonthlyAmountIsSum()
        {
            var items = new IncomeSource[]
            {
                new IncomeSource { MonthlyAmountData = 487933.23M, },
                new IncomeSource { MonthlyAmountData = 1234.57M, },
                new IncomeSource { MonthlyAmountData = 101.85M, },
            };

            var totals = IncomeSourceTotals.Create(items);

            Assert.AreEqual(Money.Create(489269.65M).ForceValue(), totals.MonthlyAmount);
        }
    }
}
