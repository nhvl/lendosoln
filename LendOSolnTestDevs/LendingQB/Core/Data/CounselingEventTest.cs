﻿namespace LendingQB.Test.Developers.LendingQB.Core.Data
{
    using global::DataAccess;
    using global::LendingQB.Core.Data;
    using LqbGrammar.DataTypes;
    using LqbGrammar.DataTypes.PathDispatch;
    using NUnit.Framework;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public class CounselingEventTest
    {
        [Test]
        public void Create_Instantiates_EmptyObject()
        {
            CounselingEvent counselingEvent = new CounselingEvent();
            Assert.That(counselingEvent.CompletedDate, Is.Null, "Counseling event completed date should be null on init.");
            Assert.That(counselingEvent.CounselingFormat, Is.Null, "Counseling event format should be null on init.");
            Assert.That(counselingEvent.CounselingType, Is.Null, "Counseling event type should be null on init.");
            Assert.That(counselingEvent.HousingCounselingAgency, Is.Null, "Counseling event housing counseling agency should be null on init.");
            Assert.That(counselingEvent.HousingCounselingHudAgencyID, Is.Null, "Counseling event housing counseling agency hud agency id should be null on init.");
            Assert.That(counselingEvent.HasChanges, Is.False, "Counseling event should have no changes on init.");
            Assert.That(counselingEvent.GetChanges(), Is.Empty, "Counseling event changes should be empty on init.");
        }

        [Test]
        public void Create_FromOtherCounselingEvent_CopiesSuccessfully()
        {
            CounselingEvent counselingEvent = new CounselingEvent();
            counselingEvent.CompletedDate = UnzonedDate.Create(DateTime.Today);
            counselingEvent.CounselingFormat = CounselingFormatType.Internet;
            counselingEvent.CounselingType = CounselingType.Education;
            counselingEvent.HousingCounselingAgency = EntityName.Create("test agency");
            counselingEvent.HousingCounselingHudAgencyID = ThirdPartyIdentifier.Create("21080114_CRIM_Z_122233");

            CounselingEvent otherCounselingEvent = new CounselingEvent(counselingEvent);

            Assert.That(otherCounselingEvent.CompletedDate, Is.EqualTo(counselingEvent.CompletedDate), "Counseling event completed dates should match.");
            Assert.That(otherCounselingEvent.CounselingFormat, Is.EqualTo(counselingEvent.CounselingFormat), "Counseling event formats should match.");
            Assert.That(otherCounselingEvent.CounselingType, Is.EqualTo(counselingEvent.CounselingType), "Counseling event types should match.");
            Assert.That(otherCounselingEvent.HousingCounselingAgency, Is.EqualTo(counselingEvent.HousingCounselingAgency), "Counseling event housing counseling agencies should match.");
            Assert.That(otherCounselingEvent.HousingCounselingHudAgencyID, Is.EqualTo(counselingEvent.HousingCounselingHudAgencyID), "Counseling event housing counseling hud agency ids should match.");
        }

        [Test]
        public void ChangeTracking_NonDefault_StillTracks()
        {
            var container = new DictionaryContainer(new Dictionary<string, object>());
            var changeTracker = new ChangeTrackingDataContainer(container);
            container["CompletedDate"] = null;
            container["CounselingFormat"] = null;
            container["CounselingType"] = null;
            container["HousingCounselingAgency"] = null;
            container["HousingCounselingHudAgencyID"] = null;

            var completedDate = UnzonedDate.Create(DateTime.Today);
            var counselingFormat = CounselingFormatType.Internet;
            var counselingType = CounselingType.Education;
            var housingCounselingAgency = EntityName.Create("test agency");
            var housingCounselingHudAgencyID = ThirdPartyIdentifier.Create("21080114_CRIM_Z_122233");

            CounselingEvent destCounselingEvent = new CounselingEvent(changeTracker);
            destCounselingEvent.CompletedDate = completedDate;
            destCounselingEvent.CounselingFormat = counselingFormat;
            destCounselingEvent.CounselingType = counselingType;
            destCounselingEvent.HousingCounselingAgency = housingCounselingAgency;
            destCounselingEvent.HousingCounselingHudAgencyID = housingCounselingHudAgencyID;

            Assert.That(changeTracker[nameof(destCounselingEvent.CompletedDate)], Is.EqualTo(completedDate));
            Assert.That(changeTracker[nameof(destCounselingEvent.CounselingFormat)], Is.EqualTo(counselingFormat));
            Assert.That(changeTracker[nameof(destCounselingEvent.CounselingType)], Is.EqualTo(counselingType));
            Assert.That(changeTracker[nameof(destCounselingEvent.HousingCounselingAgency)], Is.EqualTo(housingCounselingAgency));
            Assert.That(changeTracker[nameof(destCounselingEvent.HousingCounselingHudAgencyID)], Is.EqualTo(housingCounselingHudAgencyID));
        }

        [Test]
        public void ValuesRetrieved_FromSupportedPaths_AsExpected()
        {
            var completedDate = UnzonedDate.Create(DateTime.Today);
            var counselingFormat = CounselingFormatType.Internet;
            var counselingType = CounselingType.Education;
            var housingCounselingAgency = EntityName.Create("test agency");
            var housingCounselingHudAgencyID = ThirdPartyIdentifier.Create("21080114_CRIM_Z_122233");

            CounselingEvent destCounselingEvent = new CounselingEvent();
            destCounselingEvent.CompletedDate = completedDate;
            destCounselingEvent.CounselingFormat = counselingFormat;
            destCounselingEvent.CounselingType = counselingType;
            destCounselingEvent.HousingCounselingAgency = housingCounselingAgency;
            destCounselingEvent.HousingCounselingHudAgencyID = housingCounselingHudAgencyID;

            var counselingEventCompletedDatePath = new DataPathBasicElement("completeddate");
            var counselingEventFormatPath = new DataPathBasicElement("counselingformat");
            var counselingEventTypePath = new DataPathBasicElement("counselingtype");
            var counselingEventHousingAgencyPath = new DataPathBasicElement("housingcounselingagency");
            var counselingEventHousingHudAgencyIDPath = new DataPathBasicElement("housingcounselinghudagencyid");

            Assert.That(destCounselingEvent.GetElement(counselingEventCompletedDatePath), Is.EqualTo(completedDate));
            Assert.That(destCounselingEvent.GetElement(counselingEventFormatPath), Is.EqualTo(counselingFormat));
            Assert.That(destCounselingEvent.GetElement(counselingEventTypePath), Is.EqualTo(counselingType));
            Assert.That(destCounselingEvent.GetElement(counselingEventHousingAgencyPath), Is.EqualTo(housingCounselingAgency));
            Assert.That(destCounselingEvent.GetElement(counselingEventHousingHudAgencyIDPath), Is.EqualTo(housingCounselingHudAgencyID));
        }

        [Test]
        public void ValuesRetrieved_FromPathsWithDifferentCasing_AsExpected()
        {
            var completedDate = UnzonedDate.Create(DateTime.Today);
            var counselingFormat = CounselingFormatType.Internet;
            var counselingType = CounselingType.Education;
            var housingCounselingAgency = EntityName.Create("test agency");
            var housingCounselingHudAgencyID = ThirdPartyIdentifier.Create("21080114_CRIM_Z_122233");

            CounselingEvent destCounselingEvent = new CounselingEvent();
            destCounselingEvent.CompletedDate = completedDate;
            destCounselingEvent.CounselingFormat = counselingFormat;
            destCounselingEvent.CounselingType = counselingType;
            destCounselingEvent.HousingCounselingAgency = housingCounselingAgency;
            destCounselingEvent.HousingCounselingHudAgencyID = housingCounselingHudAgencyID;

            var counselingEventCompletedDatePath = new DataPathBasicElement("cOmPlETedDaTe");
            var counselingEventFormatPath = new DataPathBasicElement("CoUnSeLiNgFoRMaT");
            var counselingEventTypePath = new DataPathBasicElement("CoUnSeLiNgTyPe");
            var counselingEventHousingAgencyPath = new DataPathBasicElement("hOUsInGCouNseLInGAgEnCy");
            var counselingEventHousingHudAgencyIDPath = new DataPathBasicElement("hOusInGCouNSeLiNgHudAgEnCyId");

            Assert.That(destCounselingEvent.GetElement(counselingEventCompletedDatePath), Is.EqualTo(completedDate));
            Assert.That(destCounselingEvent.GetElement(counselingEventFormatPath), Is.EqualTo(counselingFormat));
            Assert.That(destCounselingEvent.GetElement(counselingEventTypePath), Is.EqualTo(counselingType));
            Assert.That(destCounselingEvent.GetElement(counselingEventHousingAgencyPath), Is.EqualTo(housingCounselingAgency));
            Assert.That(destCounselingEvent.GetElement(counselingEventHousingHudAgencyIDPath), Is.EqualTo(housingCounselingHudAgencyID));
        }

        [Test]
        public void ValuesRetrieved_UnsupportedPaths_ExceptionThrown()
        {
            CounselingEvent counselingEvent = new CounselingEvent();
            var badFieldPath = new DataPathBasicElement("somefieldthatdoesntexist");
            Assert.Throws<ArgumentException>(() => counselingEvent.GetElement(badFieldPath));
        }

        [Test]
        public void ValuesRetrieved_UnsupportedCollectionPaths_ExceptionThrown()
        {
            CounselingEvent counselingEvent = new CounselingEvent();
            var completedDate = new DataPathCollectionElement("completeddate");
            Assert.Throws<ArgumentException>(() => counselingEvent.GetElement(completedDate));
        }
    }
}
