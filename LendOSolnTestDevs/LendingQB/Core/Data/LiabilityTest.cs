﻿namespace LendingQB.Test.Developers.LendingQB.Core.Data
{
    using System;
    using global::DataAccess;
    using global::LendingQB.Core.Data;
    using LqbGrammar.DataTypes;
    using NSubstitute;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public class LiabilityTest
    {
        [Test]
        public void VerifHasSignature_NoSignature_ReturnsFalse()
        {
            var liability = this.GetLiability();

            Assert.AreEqual(false, liability.VerifHasSignature);
        }

        [Test]
        public void VerifHasSignature_HasSignature_ReturnsTrue()
        {
            var liability = this.GetLiability();

            liability.SetVerifSignature(
                DataObjectIdentifier<DataObjectKind.Employee, Guid>.Create(Guid.Empty),
                DataObjectIdentifier<DataObjectKind.Signature, Guid>.Create(Guid.Empty));

            Assert.AreEqual(true, liability.VerifHasSignature);
        }

        [Test]
        public void ClearVerifSignature_Always_SetsEmployeeIdAndSignatureIdToNull()
        {
            var liability = this.GetLiability();

            liability.SetVerifSignature(
                DataObjectIdentifier<DataObjectKind.Employee, Guid>.Create(Guid.Empty),
                DataObjectIdentifier<DataObjectKind.Signature, Guid>.Create(Guid.Empty));

            liability.ClearVerifSignature();

            Assert.AreEqual(null, liability.VerifSigningEmployeeId);
            Assert.AreEqual(null, liability.VerifSignatureImgId);
        }

        [Test]
        public void SetVerifSignature_Always_SetsVerifSigningEmployeeId()
        {
            var liability = this.GetLiability();
            var employeeId = DataObjectIdentifier<DataObjectKind.Employee, Guid>.Create(new Guid("11111111-1111-1111-1111-111111111111"));
            var signatureId = DataObjectIdentifier<DataObjectKind.Signature, Guid>.Create(new Guid("22222222-2222-2222-2222-222222222222"));

            liability.SetVerifSignature(
                employeeId,
                signatureId);

            Assert.AreEqual(employeeId, liability.VerifSigningEmployeeId);
        }

        [Test]
        public void SetVerifSignature_Always_SetsVerifSignatureImgId()
        {
            var liability = this.GetLiability();
            var employeeId = DataObjectIdentifier<DataObjectKind.Employee, Guid>.Create(new Guid("11111111-1111-1111-1111-111111111111"));
            var signatureId = DataObjectIdentifier<DataObjectKind.Signature, Guid>.Create(new Guid("22222222-2222-2222-2222-222222222222"));

            liability.SetVerifSignature(
                employeeId,
                signatureId);

            Assert.AreEqual(signatureId, liability.VerifSignatureImgId);
        }

        [Test]
        public void UsedInRatio_ByDefault_ReturnsTrue()
        {
            var liability = this.GetLiability();

            Assert.AreEqual(true, liability.UsedInRatio);
        }

        [Test]
        public void UsedInRatio_DebtTypeMortgage_ReturnsFalse()
        {
            var liability = this.GetLiability();

            liability.DebtType = E_DebtT.Mortgage;
            liability.UsedInRatioData = true;

            Assert.AreEqual(false, liability.UsedInRatio);
        }

        [Test]
        public void UsedInRatio_DebtTypeNotMortgage_ReturnsNotUsedInRatioData()
        {
            var liability = this.GetLiability();

            liability.DebtType = E_DebtT.Revolving;
            liability.UsedInRatioData = false;

            Assert.AreEqual(false, liability.UsedInRatio);
        }

        [Test]
        public void PayoffAmt_PayoffAmtLockedNull_ReturnsNull()
        {
            var liability = this.GetLiability();

            Assert.AreEqual(null, liability.PayoffAmt);
        }

        [Test]
        public void PayoffAmt_PayoffAmtLockedTrue_ReturnsPayoffAmtData()
        {
            var liability = this.GetLiability();

            liability.PayoffAmtData = Money.Create(1);
            liability.PayoffAmtLocked = true;

            Assert.AreEqual(Money.Create(1), liability.PayoffAmt);
        }

        [Test]
        public void PayoffAmt_PayoffAmtLockedFalse_ReturnsBal()
        {
            var liability = this.GetLiability();

            liability.Bal = Money.Create(2);
            liability.PayoffAmtLocked = false;

            Assert.AreEqual(Money.Create(2), liability.PayoffAmt);
        }

        [Test]
        public void PayoffTimingLocked_WillBePdOffNull_ReturnsNull()
        {
            var liability = this.GetLiability();

            Assert.AreEqual(null, liability.PayoffTimingLocked);
        }

        [Test]
        public void PayoffTimingLocked_WillBePdOffTrue_ReturnsPayoffTimingLockedData()
        {
            var liability = this.GetLiability();

            liability.WillBePdOff = true;
            liability.PayoffTimingLockedData = false;

            Assert.AreEqual(false, liability.PayoffTimingLocked);
        }

        [Test]
        public void PayoffTimingLocked_WillBePdOffFalse_ReturnsFalse()
        {
            var liability = this.GetLiability();

            liability.WillBePdOff = false;
            liability.PayoffTimingLockedData = true;

            Assert.AreEqual(false, liability.PayoffTimingLocked);
        }

        [Test]
        [TestCase(null, true, PayoffTiming.AtClosing, true, ExpectedResult = null)]
        [TestCase(false, true, PayoffTiming.AtClosing, true, ExpectedResult = PayoffTiming.Blank)]
        [TestCase(true, null, PayoffTiming.AtClosing, true, ExpectedResult = null)]
        [TestCase(true, true, null, true, ExpectedResult = null)]
        [TestCase(true, true, PayoffTiming.BeforeClosing, true, ExpectedResult = PayoffTiming.BeforeClosing)]
        [TestCase(true, false, PayoffTiming.BeforeClosing, true, ExpectedResult = PayoffTiming.AtClosing)]
        [TestCase(true, false, PayoffTiming.AtClosing, false, ExpectedResult = PayoffTiming.BeforeClosing)]
        public PayoffTiming? PayoffTimingTest(bool? willBePdOff, bool? payoffTimingLckd, PayoffTiming? payoffTiming, bool isRefi)
        {
            var fakeLoanData = Substitute.For<ILoanLqbCollectionContainerLoanDataProvider>();
            fakeLoanData.sIsRefinancing.Returns(isRefi);

            var defaultsFactory = new LiabilityDefaultsFactory();
            var defaultsProvider = defaultsFactory.Create(fakeLoanData);

            return LendersOffice.CalculatedFields.Liability.PayoffTiming(
                willBePdOff,
                payoffTimingLckd,
                payoffTiming,
                defaultsProvider);
        }

        private global::LendingQB.Core.Data.Liability GetLiability()
        {
            return new Liability(Substitute.For<ILiabilityDefaultsProvider>());
        }
    }
}
