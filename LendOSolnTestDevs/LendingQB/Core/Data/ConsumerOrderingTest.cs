﻿namespace LendingQB.Test.Developers.LendingQB.Core.Data
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;
    using global::DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Security;
    using LqbGrammar.DataTypes;
    using NUnit.Framework;
    using Utils;

    [TestFixture]
    [Category(CategoryConstants.IntegrationTest)]
    public sealed class ConsumerOrderingTest
    {
        private AbstractUserPrincipal principal;
        private Guid loanId;

        [TestFixtureSetUp]
        public void Login()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                this.principal = LoginTools.LoginAs(TestAccountType.LoTest001);
            }
        }

        [SetUp]
        public void InitializeLoan()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                var creator = CLoanFileCreator.GetCreator(
                    this.principal,
                    LendersOffice.Audit.E_LoanCreationSource.UserCreateFromBlank);
                this.loanId = creator.CreateBlankUladLoanFile();
            }

            this.AddConsumersToLoan();
        }

        [TearDown]
        public void RetireLoan()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                Tools.DeclareLoanFileInvalid(
                    this.principal,
                    this.loanId,
                    sendNotifications: false,
                    generateLinkLoanMsgEvent: false);
            }
        }

        [Test]
        public void ChangeOrder_Save_OrderReloadsCorrectly()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                var loan = CPageData.CreateUsingSmartDependency(this.loanId, typeof(ConsumerOrderingTest));
                loan.InitSave(ConstAppDavid.SkipVersionCheck);
                var consumers = loan.Consumers;
                Assert.AreEqual(4, consumers.Count());

                var first = consumers.ElementAt(0).Key;
                var second = consumers.ElementAt(1).Key;

                var orderItf = consumers.OrderingInterface;
                orderItf.MoveForward(second);
                loan.Save();

                loan = CPageData.CreateUsingSmartDependency(this.loanId, typeof(ConsumerOrderingTest));
                loan.InitLoad();
                consumers = loan.Consumers;

                Assert.AreEqual(second, consumers.ElementAt(0).Key);
                Assert.AreEqual(first, consumers.ElementAt(1).Key);
            }
        }

        [Test]
        public void ChangeOrder_CallConsumersProperty_CorrectOrderWhileWorkingWithoutSaving()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                var loan = CPageData.CreateUsingSmartDependency(this.loanId, typeof(ConsumerOrderingTest));
                loan.InitLoad();
                var consumers = loan.Consumers;
                Assert.AreEqual(4, consumers.Count());

                var first = consumers.ElementAt(0).Key;
                var second = consumers.ElementAt(1).Key;

                var orderItf = consumers.OrderingInterface;
                orderItf.MoveForward(second);

                consumers = loan.Consumers; // The Consumer collection is recreated on the fly every time - checking the identifier ordering is preserved

                Assert.AreEqual(second, consumers.ElementAt(0).Key);
                Assert.AreEqual(first, consumers.ElementAt(1).Key);
            }
        }

        private void AddConsumersToLoan()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                var loan = CPageData.CreateUsingSmartDependency(this.loanId, typeof(ConsumerOrderingTest));
                int index = loan.AddNewApp();

                loan.InitSave(ConstAppDavid.SkipVersionCheck);
                var firstApp = loan.GetAppData(0);
                var secondApp = loan.GetAppData(1);
                loan.AddCoborrowerToLegacyApplication(firstApp.aAppId.ToIdentifier<DataObjectKind.LegacyApplication>());
                loan.AddCoborrowerToLegacyApplication(secondApp.aAppId.ToIdentifier<DataObjectKind.LegacyApplication>());

                loan.Save();
            }
        }

        private List<Guid> PullOrderFromDatabase()
        {
            var list = new List<Guid>();
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                Action<IDataReader> readHandler = r =>
                {
                    if (r.Read())
                    {
                        string order = Convert.ToString(r["SortOrder"]);
                        string[] items = order.Split(',');
                        foreach (var guid in items)
                        {
                            list.Add(Guid.Parse(guid));
                        }
                    }
                };

                string sql = "SELECT SortOrder FROM [dbo].[LOAN_COLLECTION_SORT_ORDER] WHERE CollectionName = 'Consumer' AND LoanId = @loanId;";
                SqlParameter[] parameters = new SqlParameter[] { new SqlParameter("@loanId", this.loanId) };
                DBSelectUtility.ProcessDBData(this.principal.BrokerId, sql, TimeoutInSeconds.Default, parameters, readHandler);
            }

            return list;
        }
    }
}
