﻿namespace LendingQB.Test.Developers.LendingQB.Core.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using global::LendingQB.Core.Data;
    using LendersOffice.Common;
    using LqbGrammar.DataTypes;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public class HousingHistoryEntryTest
    {
        [Test]
        public void SetConsumer_Always_SetsConsumerIdAndLegacyAppId()
        {
            var entry = new HousingHistoryEntry();
            var consumerId = Guid.NewGuid().ToIdentifier<DataObjectKind.Consumer>();
            var legacyAppId = Guid.NewGuid().ToIdentifier<DataObjectKind.LegacyApplication>();

            entry.SetConsumer(consumerId, legacyAppId);

            Assert.AreEqual(consumerId, entry.ConsumerId);
            Assert.AreEqual(legacyAppId, entry.LegacyAppId);
        }
    }
}
