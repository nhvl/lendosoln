﻿namespace LendingQB.Test.Developers.LendingQB.Core.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using global::DataAccess;
    using global::LendingQB.Core.Data;
    using LqbGrammar.DataTypes;
    using LqbGrammar.DataTypes.PathDispatch;
    using NUnit.Framework;

    [TestFixture]
    public class ReadOnlyLegacyApplicationConsumerAssociationTest
    {
        private static readonly DataObjectIdentifier<DataObjectKind.Consumer, Guid> ConsumerId = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create("11111111-1111-1111-1111-111111111111").Value;
        private static readonly DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> LegacyApplicationId = DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>.Create("22222222-2222-2222-2222-222222222222").Value;

        [Test]
        public void GetElement_ConsumerId_ReturnsExpectedValue()
        {
            var association = new ReadOnlyLegacyApplicationConsumerAssociation(LegacyApplicationId, ConsumerId, true);

            var retrieved = association.GetElement(new DataPathBasicElement("ConsumerId"));

            Assert.AreEqual(retrieved, ConsumerId);
        }

        [Test]
        public void GetElement_LegacyApplicationId_ReturnsExpectedValue()
        {
            var association = new ReadOnlyLegacyApplicationConsumerAssociation(LegacyApplicationId, ConsumerId, true);

            var retrieved = association.GetElement(new DataPathBasicElement("LegacyApplicationId"));

            Assert.AreEqual(retrieved, LegacyApplicationId);
        }

        [Test]
        public void GetElement_IsPrimary_ReturnsExpectedValue()
        {
            var association = new ReadOnlyLegacyApplicationConsumerAssociation(LegacyApplicationId, ConsumerId, true);

            var retrieved = association.GetElement(new DataPathBasicElement("IsPrimary"));

            Assert.AreEqual(retrieved, true);
        }

        [Test]
        public void GetElement_SelectionDataPathSupplied_ThrowsException()
        {
            var association = new ReadOnlyLegacyApplicationConsumerAssociation(LegacyApplicationId, ConsumerId, true);

            Assert.Throws<CBaseException>(() => association.GetElement(new DataPathSelectionElement(new SelectIdExpression(DataPath.WholeCollectionIdentifier))));
        }

        [TestFixture]
        public class StringFormatterTest
        {
            [Test]
            public void GetElement_ConsumerId_ReturnsExpectedValue()
            {
                var association = new ReadOnlyLegacyApplicationConsumerAssociation(LegacyApplicationId, ConsumerId, true);
                var formatter = association.GetStringFormatter(new FormatEntityAsString(FormatTarget.Webform, FormatDirection.ToRep), null);

                var retrieved = formatter.GetElement(new DataPathBasicElement("ConsumerId"));

                Assert.AreEqual(retrieved, ConsumerId.ToString());
            }

            [Test]
            public void GetElement_LegacyApplicationId_ReturnsExpectedValue()
            {
                var association = new ReadOnlyLegacyApplicationConsumerAssociation(LegacyApplicationId, ConsumerId, true);
                var formatter = association.GetStringFormatter(new FormatEntityAsString(FormatTarget.Webform, FormatDirection.ToRep), null);

                var retrieved = formatter.GetElement(new DataPathBasicElement("LegacyApplicationId"));

                Assert.AreEqual(retrieved, LegacyApplicationId.ToString());
            }

            [Test]
            public void GetElement_IsPrimary_ReturnsExpectedValue()
            {
                var association = new ReadOnlyLegacyApplicationConsumerAssociation(LegacyApplicationId, ConsumerId, true);
                var formatter = association.GetStringFormatter(new FormatEntityAsString(FormatTarget.Webform, FormatDirection.ToRep), null);

                var retrieved = formatter.GetElement(new DataPathBasicElement("IsPrimary"));

                Assert.AreEqual(retrieved, bool.TrueString);
            }

            [Test]
            public void GetElement_SelectionDataPathSupplied_ThrowsException()
            {
                var association = new ReadOnlyLegacyApplicationConsumerAssociation(LegacyApplicationId, ConsumerId, true);
                var formatter = association.GetStringFormatter(new FormatEntityAsString(FormatTarget.Webform, FormatDirection.ToRep), null);

                Assert.Throws<CBaseException>(() => formatter.GetElement(new DataPathSelectionElement(new SelectIdExpression(DataPath.WholeCollectionIdentifier))));
            }
        }
    }
}
