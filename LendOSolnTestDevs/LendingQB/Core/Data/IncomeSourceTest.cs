﻿namespace LendingQB.Test.Developers.LendingQB.Core.Data
{
    using System;
    using System.Collections.Generic;
    using global::DataAccess;
    using global::LendingQB.Core.Data;
    using LqbGrammar.DataTypes;
    using LqbGrammar.DataTypes.PathDispatch;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public class IncomeSourceTest
    {
        [Test]
        public void Create_Instantiates_EmptyObject()
        {
            IncomeSource source = new IncomeSource();
            Assert.That(source.IncomeType, Is.Null, "Income type should be null on init.");
            Assert.That(source.Description, Is.Null, "Income source should be null on init.");
            Assert.That(source.MonthlyAmountData, Is.Null, "Income monthly amount data should be null on init.");
            Assert.That(source.HasChanges, Is.False, "Income should have no changes on init.");
            Assert.That(source.GetChanges(), Is.Empty, "Income source changes should be empty on init.");
        }

        [Test]
        public void Create_FromOtherIncome_CopiesSuccesfully()
        {
            IncomeSource source = new IncomeSource();
            source.Description = DescriptionField.Create("Test");
            source.IncomeType = IncomeType.BaseIncome;
            source.MonthlyAmountData = Money.Create(100m);

            IncomeSource otherIncome = new IncomeSource(source);

            Assert.That(otherIncome.Description, Is.EqualTo(source.Description), "Income descriptions should match.");
            Assert.That(otherIncome.IncomeType, Is.EqualTo(source.IncomeType), "Source income types should match.");
            Assert.That(otherIncome.MonthlyAmountData, Is.EqualTo(source.MonthlyAmountData), "Monthly amount data should match.");
            Assert.That(source.HasChanges, Is.True, "The new income should have changes.");
        }

        [Test]
        public void ChangeTracking_NonDefault_StillTracks()
        {
            var container = new DictionaryContainer(new Dictionary<string,object>());
            var changeTracker = new ChangeTrackingDataContainer(container);
            container["Description"] = null;
            container["IncomeType"] = null;
            container["MonthlyAmountData"] = null;

            var desc = DescriptionField.Create("Test"); 
            var incomeType = IncomeType.BaseIncome; 
            var money = Money.Create(100m);

            IncomeSource dest = new IncomeSource(changeTracker);
            dest.Description = desc;
            dest.IncomeType = incomeType;
            dest.MonthlyAmountData = money;

            Assert.That(changeTracker[nameof(dest.IncomeType)], Is.EqualTo(incomeType));
            Assert.That(changeTracker[nameof(dest.Description)], Is.EqualTo(desc));
            Assert.That(changeTracker[nameof(dest.MonthlyAmountData)], Is.EqualTo(money));
        }

        [Test]
        public void ValuesRetrieved_FromSupportedPaths_AsExpected()
        {
            var desc = DescriptionField.Create("Test");
            var incomeType = IncomeType.BaseIncome;
            var money = Money.Create(100m);

            IncomeSource dest = new IncomeSource();
            dest.Description = desc;
            dest.IncomeType = incomeType;
            dest.MonthlyAmountData = money;

            var incomeTypePath = new DataPathBasicElement("incometype"); 
            var descPath = new DataPathBasicElement("description"); 
            var moneyPath = new DataPathBasicElement("monthlyamountdata");

            Assert.That(dest.GetElement(incomeTypePath), Is.EqualTo(incomeType));
            Assert.That(dest.GetElement(descPath), Is.EqualTo(desc));
            Assert.That(dest.GetElement(moneyPath), Is.EqualTo(money));
        }


        [Test]
        public void ValuesRetrieved_FromPathsWithDifferentCasing_AsExpected()
        {
            var desc = DescriptionField.Create("Test");
            var incomeType = IncomeType.BaseIncome;
            var money = Money.Create(100m);

            IncomeSource dest = new IncomeSource();
            dest.Description = desc;
            dest.IncomeType = incomeType;
            dest.MonthlyAmountData = money;


            var incomeTypePath = new DataPathBasicElement("inCometype");
            var descPath = new DataPathBasicElement("descripTion");
            var moneyPath = new DataPathBasicElement("monthlyaMountdata");

            Assert.That(dest.GetElement(incomeTypePath), Is.EqualTo(incomeType));
            Assert.That(dest.GetElement(descPath), Is.EqualTo(desc));
            Assert.That(dest.GetElement(moneyPath), Is.EqualTo(money));
        }

        [Test]
        public void ValuesRetrieved_UnsupportedPaths_ExceptionThrown()
        {
            IncomeSource source = new IncomeSource();
            var badFieldPath = new DataPathBasicElement("somefieldthatdoesntexist");
            Assert.Throws<ArgumentException>(() => source.GetElement(badFieldPath));
        }

        [Test]
        public void ValuesRetrieved_UnsupportedCollectionPaths_ExceptionThrown()
        {
            IncomeSource source = new IncomeSource();
            var incometype = new DataPathCollectionElement("incometype");
            Assert.Throws<ArgumentException>(() => source.GetElement(incometype));
        }
    }
}
