﻿namespace LendingQB.Test.Developers.LendingQB.Core.Data
{
    using System;
    using global::DataAccess;
    using global::LendingQB.Core.Data;
    using LqbGrammar.DataTypes;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public class RealPropertyTotalsTest
    {
        [Test]
        public void Create_NullInput_ThrowsException()
        {
            Assert.Throws<ArgumentNullException>(() => RealPropertyTotals.Create(null));
        }

        [Test]
        public void Create_WithSoldProperty_ExcludesSoldPropertyFromMarketValueTotal()
        {
            var realProperties = new[]
            {
                new RealProperty() { Status = E_ReoStatusT.Sale,  MarketValue = Money.Create(100) },
                new RealProperty() { Status = null, MarketValue = Money.Create(2) },
                new RealProperty() { Status = E_ReoStatusT.PendingSale, MarketValue = Money.Create(3) },
                new RealProperty() { Status = E_ReoStatusT.Rental, MarketValue = Money.Create(4) },
                new RealProperty() { Status = E_ReoStatusT.Residence, MarketValue = Money.Create(5) },
            };

            var totals = RealPropertyTotals.Create(realProperties);

            Assert.AreEqual(Money.Create(14), totals.MarketValue);
        }

        [Test]
        public void Create_WithSoldProperty_ExcludesSoldPropertyFromMortgageAmountTotal()
        {
            var realProperties = new[]
            {
                new RealProperty() { Status = E_ReoStatusT.Sale,  MtgAmt = Money.Create(100) },
                new RealProperty() { Status = null, MtgAmt = Money.Create(2) },
                new RealProperty() { Status = E_ReoStatusT.PendingSale, MtgAmt = Money.Create(3) },
                new RealProperty() { Status = E_ReoStatusT.Rental, MtgAmt = Money.Create(4) },
                new RealProperty() { Status = E_ReoStatusT.Residence, MtgAmt = Money.Create(5) },
            };

            var totals = RealPropertyTotals.Create(realProperties);

            Assert.AreEqual(Money.Create(14), totals.MortgageAmount);
        }

        [Test]
        public void Create_WithRentalProperty_OnlyIncludesRentalPropertyInRentalNetRentalIncome()
        {
            var realProperties = new[]
            {
                new RealProperty() { Status = E_ReoStatusT.Sale,  NetRentIncData = Money.Create(1), NetRentIncLocked = true },
                new RealProperty() { Status = null, NetRentIncData = Money.Create(2), NetRentIncLocked= true },
                new RealProperty() { Status = E_ReoStatusT.PendingSale, NetRentIncData = Money.Create(3), NetRentIncLocked= true },
                new RealProperty() { Status = E_ReoStatusT.Rental, NetRentIncData = Money.Create(100), NetRentIncLocked= true },
                new RealProperty() { Status = E_ReoStatusT.Residence, NetRentIncData = Money.Create(5), NetRentIncLocked= true },
            };

            var totals = RealPropertyTotals.Create(realProperties);

            Assert.AreEqual(Money.Create(100), totals.RentalNetRentalIncome);
        }

        [Test]
        public void Create_WithRetainedProperty_OnlyIncludesRetainedPropertyInRetainedNetRentalIncome()
        {
            var realProperties = new[]
            {
                new RealProperty() { Status = E_ReoStatusT.Sale,  NetRentIncData = Money.Create(1), NetRentIncLocked = true },
                new RealProperty() { Status = null, NetRentIncData = Money.Create(2), NetRentIncLocked = true },
                new RealProperty() { Status = E_ReoStatusT.PendingSale, NetRentIncData = Money.Create(3), NetRentIncLocked = true },
                new RealProperty() { Status = E_ReoStatusT.Rental, NetRentIncData = Money.Create(4), NetRentIncLocked = true },
                new RealProperty() { Status = E_ReoStatusT.Residence, NetRentIncData = Money.Create(100), NetRentIncLocked = true },
            };

            var totals = RealPropertyTotals.Create(realProperties);

            Assert.AreEqual(Money.Create(100), totals.RetainedNetRentalIncome);
        }
    }
}
