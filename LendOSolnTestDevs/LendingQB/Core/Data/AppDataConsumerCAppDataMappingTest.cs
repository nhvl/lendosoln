﻿namespace LendingQB.Test.Developers.LendingQB.Core.Data
{
    using System;
    using System.Linq;
    using Utils;
    using LendersOffice.Security;
    using NUnit.Framework;
    using global::DataAccess;
    using global::LendingQB.Core.Data;
    using LendersOffice.Constants;
    using LqbGrammar.DataTypes;
    using global::LendingQB.Core;

    [TestFixture]
    [Category(CategoryConstants.IntegrationTest)]
    public class AppDataConsumerCAppDataMappingTest
    {
        private AbstractUserPrincipal principal;
        private Guid loanId;

        [TestFixtureSetUp]
        public void FixtureSetUp()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                this.principal = LoginTools.LoginAs(TestAccountType.LoTest001);
                var creator = CLoanFileCreator.GetCreator(
                    this.principal,
                    LendersOffice.Audit.E_LoanCreationSource.UserCreateFromBlank);
                this.loanId = creator.CreateBlankUladLoanFile();
            }
        }

        [TestFixtureTearDown]
        public void FixtureTearDown()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                Tools.DeclareLoanFileInvalid(
                    this.principal,
                    this.loanId,
                    sendNotifications: false,
                    generateLinkLoanMsgEvent: false);
            }
        }

        [TearDown]
        public void TearDown()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                    this.loanId,
                    typeof(AppDataConsumerCAppDataMappingTest));
                loan.InitSave(ConstAppDavid.SkipVersionCheck);
                loan.GetAppData(0).aBFirstNm = "";
                loan.GetAppData(0).aCFirstNm = "";
                loan.GetAppData(0).aBSsn = "";
                loan.Save();
            }
        }

        [Test]
        public void GetConsumerFirstNm_LoanLoadedViaSmartDependency_DoesNotThrowException()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                    this.loanId,
                    typeof(AppDataConsumerCAppDataMappingTest));
                loan.InitLoad();
                IReadOnlyLqbCollection<DataObjectKind.Consumer, Guid, IConsumer> consumers = loan.Consumers;

                var firstName = loan.Consumers.ElementAt(0).Value.FirstName;
            }
        }

        [Test]
        public void SetConsumerFirstNm_Always_PersistsOnLoanSave()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                    this.loanId,
                    typeof(AppDataConsumerCAppDataMappingTest));
                loan.InitSave(ConstAppDavid.SkipVersionCheck);

                var consumerId = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(loan.GetAppData(0).aBConsumerId);
                loan.Consumers[consumerId].FirstName = PersonName.Create("Test");
                loan.Save();
                loan.InitLoad();

                Assert.AreEqual("Test", loan.Consumers[consumerId].FirstName.ToString());
            }
        }

        [Test]
        public void SetaBFirstNmOnPrimaryApp_Always_SetsFirstNameOfFirstConsumer()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                    this.loanId,
                    typeof(AppDataConsumerCAppDataMappingTest));
                loan.InitLoad();
                var primaryApp = loan.GetAppData(0);
                var consumerId = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(primaryApp.aBConsumerId);

                primaryApp.aBFirstNm = "Test";

                Assert.AreEqual("Test", loan.Consumers[consumerId].FirstName.ToString());
            }
        }

        [Test]
        public void SetaBFirstNmOnPrimaryApp_Always_PersistsFirstNameOfFirstConsumer()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                    this.loanId,
                    typeof(AppDataConsumerCAppDataMappingTest));
                loan.InitSave(ConstAppDavid.SkipVersionCheck);
                var primaryApp = loan.GetAppData(0);
                var consumerId = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(primaryApp.aBConsumerId);

                primaryApp.aBFirstNm = "Test";
                loan.Save();
                loan.InitLoad();

                Assert.AreEqual("Test", loan.Consumers[consumerId].FirstName.ToString());
            }
        }

        [Test]
        public void Aliases_NoDataEntered_ReturnsNoElements()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                    this.loanId,
                    typeof(AppDataConsumerCAppDataMappingTest));
                loan.InitLoad();
                var primaryApp = loan.GetAppData(0);
                var consumerId = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(primaryApp.aBConsumerId);
                var consumer = loan.Consumers[consumerId];

                Assert.AreEqual(0, primaryApp.aBAliases.Count());
                Assert.AreEqual(0, consumer.Aliases.Count());
            }
        }

        [Test]
        public void Aliases_ValidDataEnteredToApp_MatchesBetweenAppDataAndConsumer()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                    this.loanId,
                    typeof(AppDataConsumerCAppDataMappingTest));
                loan.InitLoad();
                var aliases = new[]
                {
                    "Geoff",
                    "Geoffrey",
                    "G-off"
                };
                var primaryApp = loan.GetAppData(0);
                var consumerId = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(primaryApp.aBConsumerId);
                var consumer = loan.Consumers[consumerId];

                primaryApp.aBAliases = aliases;

                CollectionAssert.AreEqual(aliases, consumer.Aliases.Select(p => p.ToString()));
            }
        }

        [Test]
        public void CreditAuthorizationDate_SetToValidValueThenToNullInConsumer_ResultsInInvalidCDateTimeInApp()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                    this.loanId,
                    typeof(AppDataConsumerCAppDataMappingTest));
                loan.InitLoad();
                var primaryApp = loan.GetAppData(0);
                var consumerId = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(primaryApp.aBConsumerId);
                var consumer = loan.Consumers[consumerId];

                consumer.CreditAuthorizationDate = UnzonedDate.Create(2018, 4, 20);
                Assert.AreEqual(UnzonedDate.Create(2018, 4, 20), consumer.CreditAuthorizationDate);
                Assert.AreEqual(UnzonedDate.Create(2018, 4, 20).Value.Date, DateTime.Parse(primaryApp.aBCreditAuthorizationD_rep));

                consumer.CreditAuthorizationDate = null;
                Assert.AreEqual(null, consumer.CreditAuthorizationDate);
                Assert.AreEqual("", primaryApp.aBCreditAuthorizationD_rep);
            }
        }

        [Test]
        public void SetSsn_ValidValue_Succeeds()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                // Verify we can set the SSN through the Consumer directly.
                var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                    this.loanId,
                    typeof(AppDataConsumerCAppDataMappingTest));
                loan.InitSave();
                var consumer = loan.Consumers.Values.Single();

                consumer.Ssn = SocialSecurityNumber.Create("111111111");

                loan.Save();
                loan.InitSave();
                consumer = loan.Consumers.Values.Single();
                Assert.AreEqual(SocialSecurityNumber.Create("111111111"), consumer.Ssn);

                // Verify we can set the SSN through the Consumer string formatter.
                var formatter = new FormatEntityAsString(FormatTarget.Webform, FormatDirection.ToRep);
                var parser = new ParseEntityFromString(FormatTarget.Webform, FormatDirection.ToDb);
                var consumerFormatter = consumer.GetStringFormatter(formatter, parser) as IConsumerStringFormatter;

                consumerFormatter.Ssn = "222222222";

                loan.Save();
                loan.InitLoad();
                consumer = loan.Consumers.Values.Single();
                Assert.AreEqual(SocialSecurityNumber.Create("222222222"), consumer.Ssn);
            }
        }
    }
}
