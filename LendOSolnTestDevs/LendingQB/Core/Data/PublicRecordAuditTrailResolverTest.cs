﻿namespace LendingQB.Test.Developers.LendingQB.Core.Data
{
    using global::LendingQB.Core;
    using global::LendingQB.Core.Data;
    using global::LendingQB.Test.Developers.LendingQB.Core.Mapping;
    using LqbGrammar.DataTypes;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public sealed class PublicRecordAuditTrailResolverTest
    {
        [Test]
        public void InitializationCount()
        {
            var pr = this.CreatePublicRecord();
            Assert.AreEqual(1, pr.AuditTrail.Count);
        }

        [Test]
        public void ChangeCourtName()
        {
            var pr = this.CreatePublicRecord();
            int firstCount = pr.AuditTrail.Count;
            pr.CourtName = EntityName.Create("New Court");

            var resolver = new PublicRecordAuditTrailResolver(pr, null);
            resolver.ResolveChanges();

            Assert.AreEqual(firstCount + 1, pr.AuditTrail.Count);
        }

        [Test]
        public void ChangeCourtNameToBlank()
        {
            var pr = this.CreatePublicRecord();
            int firstCount = pr.AuditTrail.Count;
            pr.CourtName = null;

            var resolver = new PublicRecordAuditTrailResolver(pr, null);
            resolver.ResolveChanges();

            Assert.AreEqual(firstCount + 1, pr.AuditTrail.Count);
        }

        [Test]
        public void RoundTripWithSaves()
        {
            var pr = this.CreatePublicRecord();
            int firstCount = pr.AuditTrail.Count;
            string firstValue = pr.CourtName.Value.ToString();
            pr.CourtName = null;

            var resolver = new PublicRecordAuditTrailResolver(pr, null);
            resolver.ResolveChanges();

            Assert.AreEqual(firstCount + 1, pr.AuditTrail.Count);

            pr.CourtName = EntityName.Create(firstValue);
            resolver = new PublicRecordAuditTrailResolver(pr, null);
            resolver.ResolveChanges();

            Assert.AreEqual(firstCount + 2, pr.AuditTrail.Count);
        }

        [Test]
        public void RoundTripWithoutSaves()
        {
            var pr = this.CreatePublicRecord();
            int firstCount = pr.AuditTrail.Count;
            string firstValue = pr.CourtName.Value.ToString();

            pr.CourtName = null;
            pr.CourtName = EntityName.Create(firstValue);

            var resolver = new PublicRecordAuditTrailResolver(pr, null);
            resolver.ResolveChanges();

            Assert.AreEqual(firstCount, pr.AuditTrail.Count);
        }

        private PublicRecord CreatePublicRecord()
        {
            return PublicRecordCollectionMapperTest.GetDefaultTestRecord();
        }
    }
}
