﻿namespace LendingQB.Test.Developers.LendingQB.Core.Data
{
    using System;
    using global::LendingQB.Core.Data;
    using LqbGrammar.DataTypes;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public class VorRecordTest
    {
        [Test]
        public void VerifHasSignature_NoSignature_ReturnsFalse()
        {
            var vor = new VorRecord();

            Assert.AreEqual(false, vor.VerifHasSignature);
        }

        [Test]
        public void VerifHasSignature_HasSignature_ReturnsTrue()
        {
            var vor = new VorRecord();

            vor.SetVerifSignature(
                DataObjectIdentifier<DataObjectKind.Employee, Guid>.Create(Guid.Empty),
                DataObjectIdentifier<DataObjectKind.Signature, Guid>.Create(Guid.Empty));

            Assert.AreEqual(true, vor.VerifHasSignature);
        }

        [Test]
        public void SetVerifSignature_Always_SetsVerifSigningEmployeeId()
        {
            var vorRecord = new VorRecord();
            var employeeId = DataObjectIdentifier<DataObjectKind.Employee, Guid>.Create(new Guid("11111111-1111-1111-1111-111111111111"));
            var signatureId = DataObjectIdentifier<DataObjectKind.Signature, Guid>.Create(new Guid("22222222-2222-2222-2222-222222222222"));

            vorRecord.SetVerifSignature(
                employeeId,
                signatureId);

            Assert.AreEqual(employeeId, vorRecord.VerifSigningEmployeeId);
        }

        [Test]
        public void SetVerifSignature_Always_SetsVerifSignatureImgId()
        {
            var vorRecord = new VorRecord();
            var employeeId = DataObjectIdentifier<DataObjectKind.Employee, Guid>.Create(new Guid("11111111-1111-1111-1111-111111111111"));
            var signatureId = DataObjectIdentifier<DataObjectKind.Signature, Guid>.Create(new Guid("22222222-2222-2222-2222-222222222222"));

            vorRecord.SetVerifSignature(
                employeeId,
                signatureId);

            Assert.AreEqual(signatureId, vorRecord.VerifSignatureImgId);
        }

        [Test]
        public void SetLegacyRecordId_Always_SetsLegacyRecordId()
        {
            var vorRecord = new VorRecord();
            var value = global::DataAccess.VorSpecialRecordType.CoborrowerPrevious1;

            vorRecord.SetLegacyRecordSpecialType(value);

            Assert.AreEqual(value, vorRecord.LegacyRecordSpecialType);
        }
    }
}
