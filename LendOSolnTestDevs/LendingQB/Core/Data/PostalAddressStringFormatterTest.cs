﻿namespace LendingQB.Core.Data
{
    using LendingQB.Test.Developers;
    using LqbGrammar.DataTypes;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public class PostalAddressStringFormatterTest
    {
        private IFormatAsString formatter = new FormatAsStringBase();
        private IParseFromString parser = new ParseFromStringBase();

        [Test]
        public void UsAddress_SetsExpectedValues()
        {
            var formatter = new PostalAddressStringFormatter(null, this.formatter, this.parser);
            formatter.SetType(typeof(DataAccess.UnitedStatesPostalAddress));
            formatter.AddressNumber = "1221";
            formatter.StreetName = "Fort Branch";
            formatter.StreetSuffix = "Blvd";
            formatter.UnitIdentifier = "#2";
            formatter.City = "Austin";
            formatter.UsState = "TX";
            formatter.Zipcode = "78721";

            Assert.IsInstanceOf<DataAccess.UnitedStatesPostalAddress>(formatter.Instance);
            DataAccess.UnitedStatesPostalAddress instance = (DataAccess.UnitedStatesPostalAddress)formatter.Instance;

            Assert.AreEqual(StreetAddress.Create("1221 Fort Branch Blvd #2").Value, instance.StreetAddress);
            Assert.AreEqual(null, instance.UnparsedStreetAddress);
            Assert.AreEqual(City.Create("Austin").Value, instance.City);
            Assert.AreEqual(UnitedStatesPostalState.Create("TX").Value, instance.UsState);
            Assert.AreEqual(Zipcode.Create("78721").Value, instance.Zipcode);
            Assert.AreEqual(CountryCodeIso3.UnitedStates, instance.CountryCode);
        }

        [Test]
        public void GeneralAddress_SetsExpectedValues()
        {
            var formatter = new PostalAddressStringFormatter(null, this.formatter, this.parser);
            formatter.SetType(typeof(DataAccess.GeneralPostalAddress));
            formatter.StreetAddress = "13 King's Rd";
            formatter.City = "Penzance";
            formatter.State = "Cornwall";
            formatter.PostalCode = "TR18 4LH";
            formatter.CountryCode = CountryCodeIso3.UnitedKingdom.ToString();

            Assert.IsInstanceOf<DataAccess.GeneralPostalAddress>(formatter.Instance);
            DataAccess.GeneralPostalAddress instance = (DataAccess.GeneralPostalAddress)formatter.Instance;

            Assert.IsNotNull(instance);
            Assert.AreEqual(StreetAddress.Create("13 King's Rd").Value, instance.StreetAddress);
            Assert.AreEqual(City.Create("Penzance").Value, instance.City);
            Assert.AreEqual(AdministrativeArea.Create("Cornwall").Value, instance.State);
            Assert.AreEqual(PostalCode.Create("TR18 4LH").Value, instance.PostalCode);
            Assert.AreEqual(CountryCodeIso3.UnitedKingdom, instance.CountryCode);
        }

        [Test]
        public void UsAddress_GetsExpectedValues()
        {
            var builder = new DataAccess.UnitedStatesPostalAddress.Builder()
            {
                AddressNumber = UspsAddressNumber.Create("1221").Value,
                StreetName = StreetName.Create("Fort Branch").Value,
                StreetSuffix = StreetSuffix.Create("Blvd").Value,
                UnitIdentifier = AddressUnitIdentifier.Create("#2").Value,
                City = City.Create("Austin").Value,
                UsState = UnitedStatesPostalState.Create("TX").Value,
                Zipcode = Zipcode.Create("78721").Value,
            };

            var formatter = new PostalAddressStringFormatter(builder.GetAddress(), this.formatter, this.parser);

            Assert.AreEqual("1221 Fort Branch Blvd #2", formatter.StreetAddress);
            Assert.IsNullOrEmpty(formatter.UnparsedStreetAddress);
            Assert.AreEqual("1221", formatter.AddressNumber);
            Assert.AreEqual("Fort Branch", formatter.StreetName);
            Assert.AreEqual("Blvd", formatter.StreetSuffix);
            Assert.AreEqual("#2", formatter.UnitIdentifier);
            Assert.AreEqual("Austin", formatter.City);
            Assert.AreEqual("TX", formatter.State);
            Assert.AreEqual("TX", formatter.UsState);
            Assert.AreEqual("78721", formatter.PostalCode);
            Assert.AreEqual("78721", formatter.Zipcode);
            Assert.AreEqual(CountryCodeIso3.UnitedStates.ToString(), formatter.CountryCode);
            Assert.AreEqual(CountryCodeIso3.UnitedStates.ToCountryName().ToString(), formatter.Country);

            Assert.IsInstanceOf<DataAccess.UnitedStatesPostalAddress>(formatter.Instance);
            DataAccess.UnitedStatesPostalAddress instance = (DataAccess.UnitedStatesPostalAddress)formatter.Instance;

            Assert.IsNotNull(instance);
            Assert.AreEqual(StreetAddress.Create("1221 Fort Branch Blvd #2").Value, instance.StreetAddress);
            Assert.AreEqual(null, instance.UnparsedStreetAddress);
            Assert.AreEqual(City.Create("Austin").Value, instance.City);
            Assert.AreEqual(UnitedStatesPostalState.Create("TX").Value, instance.UsState);
            Assert.AreEqual(Zipcode.Create("78721").Value, instance.Zipcode);
            Assert.AreEqual(CountryCodeIso3.UnitedStates, instance.CountryCode);
        }

        [Test]
        public void GeneralAddress_GetsExpectedValues()
        {
            var builder = new DataAccess.GeneralPostalAddress.Builder()
            {
                StreetAddress = StreetAddress.Create("13 King's Rd").Value,
                City = City.Create("Penzance").Value,
                State = AdministrativeArea.Create("Cornwall").Value,
                PostalCode = PostalCode.Create("TR18 4LH").Value,
                CountryCode = CountryCodeIso3.UnitedKingdom,
            };

            var formatter = new PostalAddressStringFormatter(builder.GetAddress(), this.formatter, this.parser);

            Assert.AreEqual("13 King's Rd", formatter.StreetAddress);
            Assert.AreEqual("Penzance", formatter.City);
            Assert.AreEqual("Cornwall", formatter.State);
            Assert.AreEqual("TR18 4LH", formatter.PostalCode);
            Assert.AreEqual(CountryCodeIso3.UnitedKingdom.ToString(), formatter.CountryCode);

            Assert.IsInstanceOf<DataAccess.GeneralPostalAddress>(formatter.Instance);
            DataAccess.GeneralPostalAddress instance = (DataAccess.GeneralPostalAddress)formatter.Instance;
            Assert.IsNotNull(instance);
            Assert.AreEqual(StreetAddress.Create("13 King's Rd").Value, instance.StreetAddress);
            Assert.AreEqual(City.Create("Penzance").Value, instance.City);
            Assert.AreEqual(AdministrativeArea.Create("Cornwall").Value, instance.State);
            Assert.AreEqual(PostalCode.Create("TR18 4LH").Value, instance.PostalCode);
            Assert.AreEqual(CountryCodeIso3.UnitedKingdom, instance.CountryCode);
        }
    }
}
