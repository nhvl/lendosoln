﻿namespace LendingQB.Test.Developers.LendingQB.Core.Data
{
    using System;
    using System.Xml;
    using System.Xml.Linq;
    using System.Xml.Schema;
    using global::LendingQB.Test.Developers.Utils;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public class DataSchemaTest
    {
        [Test]
        public void EntityData_Always_ValidatesAgainstSchema()
        {
            ValidateTextResourceXmlAgainstSchema("EntityData.xml", "EntityDataSchema.xsd");
        }

        [Test]
        public void AssociationData_Always_ValidatesAgainstSchema()
        {
            ValidateTextResourceXmlAgainstSchema("AssociationData.xml", "AssociationDataSchema.xsd");
        }

        [Test]
        public void CodeGenTypeData_Always_ValidatesAgainstSchema()
        {
            ValidateTextResourceXmlAgainstSchema("CodeGenTypeData.xml", "CodeGenTypeDataSchema.xsd");
        }

        private static void ValidateTextResourceXmlAgainstSchema(string xmlFileName, string schemaFileName)
        {
            string xml = TestUtilities.ReadTextResource(xmlFileName);
            var xmlFile = XDocument.Parse(xml);
            var schemaSet = new XmlSchemaSet();
            using (var sr = new System.IO.StringReader(TestUtilities.ReadTextResource(schemaFileName)))
            using (var xmlReader = XmlReader.Create(sr))
            {
                schemaSet.Add(string.Empty, xmlReader);
            }

            try
            {
                xmlFile.Validate(schemaSet, null);
            }
            catch (Exception e)
            {
                var errors = new[]
                {
                    xmlFileName + " does not satisfy " + schemaFileName,
                    e.Message,
                    e.StackTrace
                };

                Assert.Fail(string.Join(Environment.NewLine, errors));
            }
        }
    }
}
