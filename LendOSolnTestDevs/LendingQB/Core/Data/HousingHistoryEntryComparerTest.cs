﻿namespace LendingQB.Test.Developers.LendingQB.Core.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using global::LendingQB.Core.Data;
    using LqbGrammar.DataTypes;
    using NUnit.Framework;

    [TestFixture]
    public class HousingHistoryEntryComparerTest
    {
        [Test]
        public void Compare_FirstRecordPrimary_ReturnsLessThanZero()
        {
            var x = new HousingHistoryEntry();
            x.IsPresentAddress = true;
            var y = new HousingHistoryEntry();
            var comparer = new HousingHistoryEntryComparer();

            var result = comparer.Compare(x, y);

            Assert.IsTrue(result < 0);
        }

        [Test]
        public void Compare_SecondRecordPrimary_ReturnsGreaterThanZero()
        {
            var x = new HousingHistoryEntry();
            var y = new HousingHistoryEntry();
            y.IsPresentAddress = true;
            var comparer = new HousingHistoryEntryComparer();

            var result = comparer.Compare(x, y);

            Assert.IsTrue(result > 0);
        }

        [Test]
        public void Compare_BothRecordsPresent_ReturnsZero()
        {
            var x = new HousingHistoryEntry();
            x.IsPresentAddress = true;
            var y = new HousingHistoryEntry();
            y.IsPresentAddress = true;
            var comparer = new HousingHistoryEntryComparer();

            var result = comparer.Compare(x, y);

            Assert.IsTrue(result == 0);
        }

        [Test]
        public void Compare_FirstRecordHasMoreRecentEndDate_ReturnsLessThanZero()
        {
            var x = new HousingHistoryEntry();
            x.EndDate = ApproximateDate.CreateWithMonthPrecision(2018, 7);
            var y = new HousingHistoryEntry();
            y.EndDate = ApproximateDate.CreateWithMonthPrecision(2018, 6);
            var comparer = new HousingHistoryEntryComparer();

            var result = comparer.Compare(x, y);

            Assert.IsTrue(result < 0);
        }

        [Test]
        public void Compare_SecondRecordHasMoreRecentEndDate_ReturnsGreaterThanZero()
        {
            var x = new HousingHistoryEntry();
            x.EndDate = ApproximateDate.CreateWithMonthPrecision(2018, 6);
            var y = new HousingHistoryEntry();
            y.EndDate = ApproximateDate.CreateWithMonthPrecision(2018, 7);
            var comparer = new HousingHistoryEntryComparer();

            var result = comparer.Compare(x, y);

            Assert.IsTrue(result > 0);
        }

        [Test]
        public void Compare_RecordsHaveSameEndDate_ReturnsZero()
        {
            var x = new HousingHistoryEntry();
            x.EndDate = ApproximateDate.CreateWithMonthPrecision(2018, 6);
            var y = new HousingHistoryEntry();
            y.EndDate = ApproximateDate.CreateWithMonthPrecision(2018, 6);
            var comparer = new HousingHistoryEntryComparer();

            var result = comparer.Compare(x, y);

            Assert.IsTrue(result == 0);
        }

        [Test]
        public void Compare_FirstRecordHasEarlierMinimumEndDate_ReturnsGreaterThanZero()
        {
            var x = new HousingHistoryEntry();
            x.EndDate = ApproximateDate.CreateWithYearPrecision(2018);
            var y = new HousingHistoryEntry();
            y.EndDate = ApproximateDate.CreateWithMonthPrecision(2018, 7);
            var comparer = new HousingHistoryEntryComparer();

            var result = comparer.Compare(x, y);

            Assert.IsTrue(result > 0);
        }

        [Test]
        public void Compare_SecondRecordHasEarlierMinimumEndDate_ReturnsLessThanZero()
        {
            var x = new HousingHistoryEntry();
            x.EndDate = ApproximateDate.CreateWithMonthPrecision(2018, 7);
            var y = new HousingHistoryEntry();
            y.EndDate = ApproximateDate.CreateWithYearPrecision(2018);
            var comparer = new HousingHistoryEntryComparer();

            var result = comparer.Compare(x, y);

            Assert.IsTrue(result < 0);
        }

        [Test]
        public void Compare_FirstRecordHasBlankEndDate_ReturnsGreaterThanZero()
        {
            var x = new HousingHistoryEntry();
            Assert.IsNull(x.EndDate);
            var y = new HousingHistoryEntry();
            y.EndDate = ApproximateDate.CreateWithYearPrecision(2018);
            var comparer = new HousingHistoryEntryComparer();

            var result = comparer.Compare(x, y);

            Assert.IsTrue(result > 0);
        }

        [Test]
        public void Compare_SecondRecordHasBlankEndDate_ReturnsLessThanZero()
        {
            var x = new HousingHistoryEntry();
            x.EndDate = ApproximateDate.CreateWithYearPrecision(2018);
            var y = new HousingHistoryEntry();
            Assert.IsNull(y.EndDate);
            var comparer = new HousingHistoryEntryComparer();

            var result = comparer.Compare(x, y);

            Assert.IsTrue(result < 0);
        }

        [Test]
        public void Compare_BothBlankEndDate_ReturnsLessThanZero()
        {
            var x = new HousingHistoryEntry();
            Assert.IsNull(x.EndDate);
            var y = new HousingHistoryEntry();
            Assert.IsNull(y.EndDate);
            var comparer = new HousingHistoryEntryComparer();

            var result = comparer.Compare(x, y);

            Assert.IsTrue(result == 0);
        }
    }
}
