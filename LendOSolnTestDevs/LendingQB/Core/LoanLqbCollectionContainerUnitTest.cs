﻿namespace LendingQB.Test.Developers.LendingQB.Core
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Linq;
    using Fakes;
    using global::DataAccess;
    using global::LendingQB.Core;
    using global::LendingQB.Core.Data;
    using LendersOffice.Common;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;
    using NSubstitute;
    using NUnit.Framework;
    using Utils;

    using ConsumerIdentifier = LqbGrammar.DataTypes.DataObjectIdentifier<LqbGrammar.DataTypes.DataObjectKind.Consumer, System.Guid>;
    using LegacyAppIdentifier = LqbGrammar.DataTypes.DataObjectIdentifier<LqbGrammar.DataTypes.DataObjectKind.LegacyApplication, System.Guid>;
    using EmploymentIdentifier = LqbGrammar.DataTypes.DataObjectIdentifier<LqbGrammar.DataTypes.DataObjectKind.EmploymentRecord, System.Guid>;
    using IncomeIdentifier = LqbGrammar.DataTypes.DataObjectIdentifier<LqbGrammar.DataTypes.DataObjectKind.IncomeSource, System.Guid>;
    using LiabilityIdentifier = LqbGrammar.DataTypes.DataObjectIdentifier<LqbGrammar.DataTypes.DataObjectKind.Liability, System.Guid>;
    using PropertyIdentifier = LqbGrammar.DataTypes.DataObjectIdentifier<LqbGrammar.DataTypes.DataObjectKind.RealProperty, System.Guid>;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public class LoanLqbCollectionContainerUnitTest
    {
        private static readonly DataObjectIdentifier<DataObjectKind.Consumer, Guid> ConsumerId1 = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(new Guid("11111111-1111-1111-1111-111111111111"));
        private static readonly DataObjectIdentifier<DataObjectKind.Consumer, Guid> ConsumerId2 = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(new Guid("22222222-2222-2222-2222-222222222222"));
        private static readonly DataObjectIdentifier<DataObjectKind.Consumer, Guid> ConsumerId3 = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(new Guid("33333333-3333-3333-3333-333333333333"));
        private static readonly DataObjectIdentifier<DataObjectKind.Consumer, Guid> ConsumerId4 = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(new Guid("44444444-4444-4444-4444-444444444444"));
        private static readonly DataObjectIdentifier<DataObjectKind.Consumer, Guid> ConsumerId5 = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(new Guid("88888888-8888-8888-8888-888888888888"));
        private static readonly DataObjectIdentifier<DataObjectKind.Consumer, Guid> ConsumerId6 = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(new Guid("99999999-9999-9999-9999-999999999999"));
        private static readonly DataObjectIdentifier<DataObjectKind.Consumer, Guid> ConsumerId7 = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(new Guid("11111111-1111-1111-1111-111111111112"));
        private static readonly DataObjectIdentifier<DataObjectKind.Consumer, Guid> ConsumerId8 = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(new Guid("11111111-1111-1111-1111-111111111113"));
        private static readonly DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> AppId = DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>.Create(new Guid("55555555-5555-5555-5555-555555555555"));
        private static readonly DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> AppId2 = DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>.Create(new Guid("66666666-6666-6666-6666-666666666666"));
        private static readonly DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> AppId3 = DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>.Create(new Guid("11111111-2222-3333-4444-555555555555"));
        private static readonly DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> AppId4 = DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>.Create(new Guid("11111111-2222-3333-4444-555555555556"));
        private static readonly Guid DefaultRecordId = new Guid("77777777-7777-7777-7777-777777777777");

        private EmploymentIdentifier employmentId;
        private IncomeIdentifier incomeId;
        private LiabilityIdentifier liabilityId;
        private PropertyIdentifier propertyId;

        private static DataObjectIdentifier<T, Guid> GetDefaultEntityId<T>() where T : DataObjectKind
        {
            return DataObjectIdentifier<T, Guid>.Create(DefaultRecordId);
        }

        [Test]
        public void Add_GeneratedMethodWithoutSpecifyingRecordId_AddsRecordAndPrimaryAssociation()
        {
            var container = this.GetCollectionContainer();

            var recordId = container.Add(
                ConsumerId1,
                new Asset());

            Assert.AreEqual(1, container.Assets.Count);
            Assert.AreEqual(recordId, container.Assets.Keys.Single());
            Assert.AreEqual(1, container.ConsumerAssets.Count);
            Assert.AreEqual(ConsumerId1, container.ConsumerAssets.Values.Single().ConsumerId);
            Assert.AreEqual(AppId, container.ConsumerAssets.Values.Single().AppId);
            Assert.AreEqual(recordId, container.ConsumerAssets.Values.Single().AssetId);
            Assert.AreEqual(true, container.ConsumerAssets.Values.Single().IsPrimary);
        }

        [Test]
        public void Add_HousingHistoryEntry_AddsRecordForConsumer()
        {
            var container = this.GetCollectionContainer(this.GetLoanDataWithLegacyAppsAndConsumerAssociations());

            var recordId = container.Add(
                ConsumerId1,
                new HousingHistoryEntry());

            Assert.AreEqual(1, container.HousingHistoryEntries.Count);
            Assert.AreEqual(recordId, container.HousingHistoryEntries.Keys.Single());
            var entry = container.GetHousingHistoryEntriesForConsumer(ConsumerId1).Single();
            Assert.AreEqual(AppId, entry.Value.LegacyAppId);
        }

        [Test]
        public void Insert_GeneratedMethodWithoutSpecifyingRecordId_AddsRecordAtIndexAndPrimaryAssociation()
        {
            var container = this.GetCollectionContainer();
            var recordId1 = container.Add(ConsumerId1, new Asset());
            var recordId2 = container.Add(ConsumerId1, new Asset());

            var recordId3 = container.Insert(1, ConsumerId2, new Asset());

            Assert.AreEqual(3, container.Assets.Count);
            Assert.AreEqual(true, container.Assets.ContainsKey(recordId3));
            Assert.AreEqual(recordId3, container.Assets.ElementAt(1).Key);

            Assert.AreEqual(3, container.ConsumerAssets.Count);
            var association = container.ConsumerAssets.Values.Single(a => a.AssetId == recordId3);
            Assert.AreEqual(ConsumerId2, association.ConsumerId);
            Assert.AreEqual(AppId, association.AppId);
            Assert.AreEqual(recordId3, association.AssetId);
            Assert.AreEqual(true, association.IsPrimary);
        }

        [Test]
        public void Add_GeneratedMethodSpecifyingRecordId_AddsRecordAndPrimaryAssociation()
        {
            var container = this.GetCollectionContainer();
            var recordId = GetDefaultEntityId<DataObjectKind.Asset>();
                
            container.Add(
                ConsumerId1,
                recordId,
                new Asset());

            Assert.AreEqual(1, container.Assets.Count);
            Assert.AreEqual(recordId, container.Assets.Keys.Single());
            Assert.AreEqual(1, container.ConsumerAssets.Count);
            Assert.AreEqual(ConsumerId1, container.ConsumerAssets.Values.Single().ConsumerId);
            Assert.AreEqual(AppId, container.ConsumerAssets.Values.Single().AppId);
            Assert.AreEqual(recordId, container.ConsumerAssets.Values.Single().AssetId);
            Assert.AreEqual(true, container.ConsumerAssets.Values.Single().IsPrimary);
        }

        [Test]
        public void RemoveAssetFromLegacyApplication_GeneratedMethod_RemovesRecordAndAssociation()
        {
            var container = this.GetCollectionContainer();

            // Add Record.
            var recordId = container.Add(
                ConsumerId1,
                new Asset());
            Assert.AreEqual(1, container.Assets.Count);
            Assert.AreEqual(recordId, container.Assets.Keys.Single());
            Assert.AreEqual(1, container.ConsumerAssets.Count);

            // Remove Record.
            container.RemoveAssetFromLegacyApplication(recordId, AppId, ConsumerId1, ConsumerId2);

            Assert.AreEqual(0, container.Assets.Count);
            Assert.AreEqual(0, container.ConsumerAssets.Count);
        }

        [Test]
        public void RemoveFromLegacyApplication_GeneratedMethodPrimaryOwnershipWithCrossApp_ThrowsException()
        {
            var container = this.GetCollectionContainer();

            // Add Record.
            var recordId = container.Add(
                ConsumerId1,
                new Asset());

            // Add cross-app ownership.
            container.AddOwnership(ConsumerId3, recordId);

            Assert.AreEqual(1, container.Assets.Count);
            Assert.AreEqual(recordId, container.Assets.Keys.Single());
            Assert.AreEqual(2, container.ConsumerAssets.Count);

            // Remove Record.
            // Should throw since we are deleting the primary ownership association.
            Assert.Throws<CBaseException>(() => container.RemoveAssetFromLegacyApplication(recordId, AppId, ConsumerId1, ConsumerId2));

            // State should not be corrupted after the exception is thrown.
            Assert.AreEqual(1, container.Assets.Count);
            Assert.AreEqual(recordId, container.Assets.Keys.Single());
            Assert.AreEqual(2, container.ConsumerAssets.Count);
        }

        [Test]
        public void RemoveFromLegacyApplication_GeneratedMethodWithCrossAppPrimary_RemovesAssociationButNotRecord()
        {
            var container = this.GetCollectionContainer();

            // Add Record.
            var recordId = container.Add(
                ConsumerId1,
                new Asset());

            // Add cross-app ownership.
            container.AddOwnership(ConsumerId3, recordId);

            Assert.AreEqual(1, container.Assets.Count);
            Assert.AreEqual(recordId, container.Assets.Keys.Single());
            Assert.AreEqual(2, container.ConsumerAssets.Count);
            Assert.AreEqual(1, container.GetOrderForApplication<DataObjectKind.Asset>(AppId).Count);
            Assert.AreEqual(1, container.GetOrderForApplication<DataObjectKind.Asset>(AppId2).Count);

            // Remove Record from App 2.
            container.RemoveAssetFromLegacyApplication(recordId, AppId2, ConsumerId2, ConsumerId3);

            // Should still have record in entity collection and primary ownership association.
            Assert.AreEqual(1, container.Assets.Count);
            Assert.AreEqual(recordId, container.Assets.Keys.Single());
            Assert.AreEqual(1, container.ConsumerAssets.Count);
            var association = container.ConsumerAssets.Single().Value;
            Assert.AreEqual(ConsumerId1, association.ConsumerId);
            Assert.AreEqual(recordId, association.AssetId);
            Assert.AreEqual(AppId, association.AppId);
            Assert.AreEqual(true, association.IsPrimary);
            Assert.AreEqual(1, container.GetOrderForApplication<DataObjectKind.Asset>(AppId).Count);
            Assert.AreEqual(0, container.GetOrderForApplication<DataObjectKind.Asset>(AppId2).Count);
        }

        [Test]
        public void AddPrimaryEmployment_Always_AddsPrimaryRecord()
        {
            var container = this.GetCollectionContainer();

            var recordId = container.Add(
                ConsumerId1,
                this.GetNewEmploymentRecord(true));

            Assert.AreEqual(1, container.EmploymentRecords.Count);
            Assert.AreEqual(recordId, container.EmploymentRecords.Keys.Single());
            var employmentRecord = container.EmploymentRecords.Values.Single();
            Assert.AreEqual(ConsumerId1, employmentRecord.ConsumerId);
            Assert.AreEqual(AppId, employmentRecord.LegacyAppId);
            Assert.AreEqual(true, employmentRecord.IsPrimary);
        }

        [Test]
        public void Add_EmploymentWithoutSpecifyingRecordId_AddsRecordAndAssociation()
        {
            var container = this.GetCollectionContainer();

            var recordId = container.Add(
                ConsumerId1,
                this.GetNewEmploymentRecord());

            Assert.AreEqual(1, container.EmploymentRecords.Count);
            Assert.AreEqual(recordId, container.EmploymentRecords.Keys.Single());
            var employmentRecord = container.EmploymentRecords.Values.Single();
            Assert.AreEqual(ConsumerId1, employmentRecord.ConsumerId);
            Assert.AreEqual(AppId, employmentRecord.LegacyAppId);
            Assert.AreEqual(false, employmentRecord.IsPrimary);
        }

        [Test]
        public void Insert_EmploymentRecordWithoutSpecifyingRecordId_AddsRecordAtIndexForConsumer()
        {
            var container = this.GetCollectionContainer();
            var recordId1 = container.Add(ConsumerId1, this.GetNewEmploymentRecord());
            var recordId2 = container.Add(ConsumerId1, this.GetNewEmploymentRecord());

            var recordId3 = container.Insert(1, ConsumerId1, this.GetNewEmploymentRecord());

            Assert.AreEqual(3, container.EmploymentRecords.Count);
            Assert.AreEqual(true, container.EmploymentRecords.ContainsKey(recordId3));
            Assert.AreEqual(recordId3, container.GetEmploymentRecordsForConsumer(ConsumerId1).ElementAt(1).Key);

            var inserted = container.EmploymentRecords[recordId3];
            Assert.AreEqual(ConsumerId1, inserted.ConsumerId);
            Assert.AreEqual(AppId, inserted.LegacyAppId);
            Assert.AreEqual(false, inserted.IsPrimary);
        }

        [Test]
        public void Add_EmploymentSpecifyingRecordId_AddsRecordAndPrimaryAssociation()
        {
            var container = this.GetCollectionContainer();
            var recordId = GetDefaultEntityId<DataObjectKind.EmploymentRecord>();
                
            container.Add(
                ConsumerId1,
                recordId,
                this.GetNewEmploymentRecord());

            Assert.AreEqual(1, container.EmploymentRecords.Count);
            Assert.AreEqual(recordId, container.EmploymentRecords.Keys.Single());
            var employment = container.EmploymentRecords[recordId];
            Assert.AreEqual(ConsumerId1, employment.ConsumerId);
            Assert.AreEqual(AppId, employment.LegacyAppId);
            Assert.AreEqual(false, employment.IsPrimary);
        }

        [Test]
        public void Remove_Employment_RemovesRecord()
        {
            var container = this.GetCollectionContainer();

            // Add Record.
            var recordId = container.Add(
                ConsumerId1,
                this.GetNewEmploymentRecord());
            Assert.AreEqual(1, container.EmploymentRecords.Count);
            Assert.AreEqual(recordId, container.EmploymentRecords.Keys.Single());

            // Remove Record.
            container.Remove(recordId);

            Assert.AreEqual(0, container.EmploymentRecords.Count);
        }

        [Test]
        public void Remove_HousingHistoryEntry_RemovesRecord()
        {
            var container = this.GetCollectionContainer(this.GetLoanDataWithLegacyAppsAndConsumerAssociations());
            var recordId = container.Add(ConsumerId1, new HousingHistoryEntry());
            Assert.AreEqual(1, container.HousingHistoryEntries.Count);
            Assert.AreEqual(recordId, container.HousingHistoryEntries.Keys.Single());

            container.Remove(recordId);

            Assert.AreEqual(0, container.HousingHistoryEntries.Count);
        }

        [Test]
        public void AddPrimaryEmployment_Always_AddsIdToConsumerLevelOrdering()
        {
            var container = this.GetCollectionContainer();

            var recordId = container.Add(
                ConsumerId1,
                this.GetNewEmploymentRecord(true));

            var order = container.GetOrderForConsumer<DataObjectKind.EmploymentRecord>(ConsumerId1);
            Assert.AreEqual(1, order.Count);
            Assert.AreEqual(recordId, order.First());
        }

        [Test]
        public void Add_EmploymentWithoutSpecifyingRecordId_AddsIdToConsumerLevelOrdering()
        {
            var container = this.GetCollectionContainer();

            var recordId = container.Add(
                ConsumerId1,
                this.GetNewEmploymentRecord());

            var order = container.GetOrderForConsumer<DataObjectKind.EmploymentRecord>(ConsumerId1);
            Assert.AreEqual(1, order.Count);
            Assert.AreEqual(recordId, order.First());
        }

        [Test]
        public void Add_EmploymentSpecifyingRecordId_AddsIdToConsumerLevelOrdering()
        {
            var container = this.GetCollectionContainer();
            var recordId = GetDefaultEntityId<DataObjectKind.EmploymentRecord>();
                
            container.Add(
                ConsumerId1,
                recordId,
                this.GetNewEmploymentRecord());

            var order = container.GetOrderForConsumer<DataObjectKind.EmploymentRecord>(ConsumerId1);
            Assert.AreEqual(1, order.Count);
            Assert.AreEqual(recordId, order.First());
        }

        [Test]
        public void Remove_Employment_RemovesIdFromConsumerLevelOrdering()
        {
            var container = this.GetCollectionContainer();

            // Add Record.
            var recordId = container.Add(
                ConsumerId1,
                this.GetNewEmploymentRecord());
            Assert.AreEqual(1, container.EmploymentRecords.Count);
            Assert.AreEqual(recordId, container.EmploymentRecords.Keys.Single());

            // Remove Record.
            container.Remove(recordId);

            var order = container.GetOrderForConsumer<DataObjectKind.EmploymentRecord>(ConsumerId1);
            Assert.AreEqual(0, order.Count);
        }

        [Test]
        public void GetIsSpMtg_DebtTypeNull_ReturnsNull()
        {
            var collectionContainer = this.GetCollectionContainer();

            var liability = new Liability(Substitute.For<ILiabilityDefaultsProvider>());
            liability.DebtType = null;
            var id = collectionContainer.Add(ConsumerId1, liability);

            bool? isSpMtg = collectionContainer.GetIsSpMtg(id, liability);

            Assert.AreEqual(null, isSpMtg);
        }

        [Test]
        public void GetIsSpMtg_DebtTypeNotMortgage_ReturnsFalse()
        {
            var collectionContainer = this.GetCollectionContainer();

            var liability = new Liability(Substitute.For<ILiabilityDefaultsProvider>());
            liability.DebtType = E_DebtT.Revolving;
            var id = collectionContainer.Add(ConsumerId1, liability);

            bool? isSpMtg = collectionContainer.GetIsSpMtg(id, liability);

            Assert.AreEqual(false, isSpMtg);
        }

        [Test]
        public void GetIsSpMtg_DebtTypeMortgageNoAssociatedRealProperty_ReturnsTrue()
        {
            var collectionContainer = this.GetCollectionContainer();

            var liability = new Liability(Substitute.For<ILiabilityDefaultsProvider>());
            liability.DebtType = E_DebtT.Mortgage;
            var id = collectionContainer.Add(ConsumerId1, liability);

            bool? isSpMtg = collectionContainer.GetIsSpMtg(id, liability);

            Assert.AreEqual(true, isSpMtg);
        }

        [Test]
        public void GetIsSpMtg_DebtTypeMortgageWithAssociatedRealProperty_ReturnsRealPropertyValue()
        {
            var collectionContainer = this.GetCollectionContainer();

            var liability = new Liability(Substitute.For<ILiabilityDefaultsProvider>());
            liability.DebtType = E_DebtT.Mortgage;
            var liabilityId = collectionContainer.Add(ConsumerId1, liability);
            var realProperty = new RealProperty();
            realProperty.IsSubjectProp = false;
            var realPropertyId = collectionContainer.Add(ConsumerId1, realProperty);

            var shim = (IShimContainer)collectionContainer;
            shim.AssociateLiabilityWithRealProperty(liabilityId.Value, realPropertyId.Value);

            bool? isSpMtg = collectionContainer.GetIsSpMtg(liabilityId, liability);

            Assert.AreEqual(false, isSpMtg);
        }

        [Test]
        public void SetIsSpMtg_ToTrueNoExistingRealPropertyAssociation_CreatesAssociationWithSubjectPropertyRealProperty()
        {
            var collectionContainer = this.GetCollectionContainer();

            var liability = new Liability(Substitute.For<ILiabilityDefaultsProvider>());
            liability.DebtType = null;
            var liabilityId = collectionContainer.Add(ConsumerId1, liability);

            collectionContainer.SetIsSpMtg(liabilityId, liability, true);

            Assert.AreEqual(1, collectionContainer.RealPropertyLiabilities.Count);
            Assert.AreEqual(1, collectionContainer.RealProperties.Count);

            var realProperty = collectionContainer.RealProperties.Single();
            Assert.AreEqual(true, realProperty.Value.IsSubjectProp);

            var association = collectionContainer.RealPropertyLiabilities.Values.Single();
            Assert.AreEqual(liabilityId, association.LiabilityId);
            Assert.AreEqual(realProperty.Key, association.RealPropertyId);
        }

        [Test]
        public void SetIsSpMtg_ToTrueWithExistingSubjectPropertyRealPropertyAssociation_DoesNothing()
        {
            var collectionContainer = this.GetCollectionContainer();

            var liability = new Liability(Substitute.For<ILiabilityDefaultsProvider>());
            var liabilityId = collectionContainer.Add(ConsumerId1, liability);
            var realProperty = new RealProperty();
            realProperty.IsSubjectProp = true;
            var realPropertyId = collectionContainer.Add(ConsumerId1, realProperty);

            var shim = (IShimContainer)collectionContainer;
            shim.AssociateLiabilityWithRealProperty(liabilityId.Value, realPropertyId.Value);

            collectionContainer.SetIsSpMtg(liabilityId, liability, true);

            Assert.AreEqual(1, collectionContainer.RealPropertyLiabilities.Count);
            Assert.AreEqual(1, collectionContainer.RealProperties.Count);
        }

        [Test]
        public void SetIsSpMtg_ToFalseWithExistingSubjectPropertyRealPropertyAssociation_RemovesAssociation()
        {
            var collectionContainer = this.GetCollectionContainer();

            var liability = this.GetNewLiability();
            var liabilityId = collectionContainer.Add(ConsumerId1, liability);
            var realProperty = new RealProperty();
            realProperty.IsSubjectProp = true;
            var realPropertyId = collectionContainer.Add(ConsumerId1, realProperty);

            var shim = (IShimContainer)collectionContainer;
            shim.AssociateLiabilityWithRealProperty(liabilityId.Value, realPropertyId.Value);

            collectionContainer.SetIsSpMtg(liabilityId, liability, false);

            Assert.AreEqual(0, collectionContainer.RealPropertyLiabilities.Count);
            Assert.AreEqual(1, collectionContainer.RealProperties.Count);
        }

        [Test]
        public void SetIsSpMtg_ToFalseWithExistingRealPropertyAssociation_DoesNothing()
        {
            var collectionContainer = this.GetCollectionContainer();

            var liability = this.GetNewLiability();
            var liabilityId = collectionContainer.Add(ConsumerId1, liability);
            var realProperty = new RealProperty();
            realProperty.IsSubjectProp = false;
            var realPropertyId = collectionContainer.Add(ConsumerId1, realProperty);

            var shim = (IShimContainer)collectionContainer;
            shim.AssociateLiabilityWithRealProperty(liabilityId.Value, realPropertyId.Value);

            collectionContainer.SetIsSpMtg(liabilityId, liability, false);

            Assert.AreEqual(1, collectionContainer.RealPropertyLiabilities.Count);
            Assert.AreEqual(1, collectionContainer.RealProperties.Count);
        }

        [Test]
        public void GetIsSp1stMtg_DebtTypeNullMortgage_ReturnsNull()
        {
            var collectionContainer = this.GetCollectionContainer();

            var liability = this.GetNewLiability();
            liability.DebtType = null;
            var id = collectionContainer.Add(ConsumerId1, liability);

            bool? isSp1stMtg = collectionContainer.GetIsSp1stMtg(id, liability);

            Assert.AreEqual(null, isSp1stMtg);
        }

        [Test]
        public void GetIsSp1stMtg_DebtTypeNotMortgage_ReturnsFalse()
        {
            var collectionContainer = this.GetCollectionContainer();

            var liability = this.GetNewLiability();
            liability.DebtType = E_DebtT.Revolving;
            var id = collectionContainer.Add(ConsumerId1, liability);

            bool? isSp1stMtg = collectionContainer.GetIsSp1stMtg(id, liability);

            Assert.AreEqual(false, isSp1stMtg);
        }

        [Test]
        public void GetIsSp1stMtg_IsSubjectPropertyMortgage_ReturnsIsSp1stMtgData()
        {
            var collectionContainer = this.GetCollectionContainer();

            var liability = this.GetNewLiability();
            liability.DebtType = E_DebtT.Mortgage;
            liability.IsSp1stMtgData = true;
            var id = collectionContainer.Add(ConsumerId1, liability);

            bool? isSp1stMtg = collectionContainer.GetIsSp1stMtg(id, liability);

            Assert.AreEqual(true, isSp1stMtg);
        }

        [Test]
        public void SetIsSp1stMtg_ToFalse_SetsIsSp1stMtgData()
        {
            var collectionContainer = this.GetCollectionContainer();

            var liability = this.GetNewLiability();
            liability.DebtType = E_DebtT.Mortgage;
            liability.IsSp1stMtgData = true;
            var id = collectionContainer.Add(ConsumerId1, liability);

            collectionContainer.SetIsSp1stMtg(id, liability, false);

            Assert.AreEqual(false, liability.IsSp1stMtgData);
        }

        [Test]
        public void SetIsSp1stMtg_ToTrue_ClearsIsSp1stMtgDataForAllLiabilities()
        {
            var collectionContainer = this.GetCollectionContainer();

            var liability1 = this.GetNewLiability();
            liability1.DebtType = E_DebtT.Mortgage;
            liability1.IsSp1stMtgData = true;
            var id1 = collectionContainer.Add(ConsumerId1, liability1);

            var liability2 = this.GetNewLiability();
            liability2.DebtType = E_DebtT.Mortgage;
            liability2.IsSp1stMtgData = false;
            var id2 = collectionContainer.Add(ConsumerId1, liability2);

            var liability3 = this.GetNewLiability();
            liability3.DebtType = E_DebtT.Mortgage;
            liability3.IsSp1stMtgData = false;
            var id3 = collectionContainer.Add(ConsumerId1, liability3);

            collectionContainer.SetIsSp1stMtg(id2, liability2, true);

            Assert.AreEqual(false, liability1.IsSp1stMtgData);
            Assert.AreEqual(true, liability2.IsSp1stMtgData);
            Assert.AreEqual(false, liability3.IsSp1stMtgData);
        }

        [Test]
        public void GetEmploymentsForConsumer_TwoAssociated_ReturnsTwoRecords()
        {
            var container = this.GetCollectionContainer();

            var employment1Id = container.Add(ConsumerId1, this.GetNewEmploymentRecord());
            var employment2Id = container.Add(ConsumerId1, this.GetNewEmploymentRecord());
            var consumer2Employment1Id = container.Add(ConsumerId2, this.GetNewEmploymentRecord());
            var consumer2Employment2Id = container.Add(ConsumerId2, this.GetNewEmploymentRecord());

            var associated = container.GetEmploymentRecordsForConsumer(ConsumerId1);
            Assert.AreEqual(2, associated.Count());
        }

        [Test]
        public void GetHousingHistoryEntriesForConsumer_TwoAssociated_ReturnsTwoRecords()
        {
            var container = this.GetCollectionContainer(this.GetLoanDataWithLegacyAppsAndConsumerAssociations());

            var housingHistory1Id = container.Add(ConsumerId1, new HousingHistoryEntry());
            var housingHistory2Id = container.Add(ConsumerId1, new HousingHistoryEntry());
            var consumer2HousingHistory1Id = container.Add(ConsumerId2, new HousingHistoryEntry());
            var consumer2HousingHistory2Id = container.Add(ConsumerId2, new HousingHistoryEntry());

            var associated = container.GetHousingHistoryEntriesForConsumer(ConsumerId1);
            Assert.AreEqual(2, associated.Count());
        }

        [Test]
        public void GetAssetsForConsumers_CrossAppOwnership_OnlyReturnsPrimaryOwnershipRecords()
        {
            var container = this.GetCollectionContainer();

            var asset1Id = container.Add(ConsumerId1, new Asset());
            var asset2Id = container.Add(ConsumerId1, new Asset());
            var asset3Id = container.Add(ConsumerId3, new Asset());
            container.AddOwnership(ConsumerId1, asset3Id);

            var associated = container.GetAssetsForLegacyApplication(AppId, ConsumerId1, ConsumerId2);

            Assert.AreEqual(2, associated.Count());
            CollectionAssert.AreEquivalent(associated.Select(a => a.Key), new[] { asset1Id, asset2Id });
        }

        [Test]
        public void Add_AssetNoIdSpecified_AddsIdToAppLevelOrdering()
        {
            var container = this.GetCollectionContainer();

            var asset1Id = container.Add(ConsumerId1, new Asset());

            var order = container.GetOrderForApplication<DataObjectKind.Asset>(AppId);
            Assert.AreEqual(1, order.Count);
            Assert.AreEqual(asset1Id, order.First());
        }

        [Test]
        public void Add_AssetSpecifyingId_AddsIdToAppLevelOrdering()
        {
            var container = this.GetCollectionContainer();
            var recordId = GetDefaultEntityId<DataObjectKind.Asset>();

            container.Add(ConsumerId1, recordId, new Asset());

            var order = container.GetOrderForApplication<DataObjectKind.Asset>(AppId);
            Assert.AreEqual(1, order.Count);
            Assert.AreEqual(recordId, order.First());
        }

        [Test]
        public void RemoveAssetFromLegacyApplication_Always_RemovesIdFromAppLevelOrdering()
        {
            var container = this.GetCollectionContainer();
            var assetId = container.Add(ConsumerId1, new Asset());
            Assert.AreEqual(1, container.GetOrderForApplication<DataObjectKind.Asset>(AppId).Count);

            container.RemoveAssetFromLegacyApplication(assetId, AppId, ConsumerId1, ConsumerId2);

            Assert.AreEqual(0, container.GetOrderForApplication<DataObjectKind.Asset>(AppId).Count);
        }

        [Test]
        public void Remove_Asset_RemovesItemFromCollection()
        {
            var container = this.GetCollectionContainer();
            var assetId = container.Add(ConsumerId1, new Asset());
            Assert.AreEqual(1, container.Assets.Count);

            container.Remove(assetId);

            Assert.AreEqual(0, container.Assets.Count);
        }

        [Test]
        public void Remove_Asset_RemovesIdFromLoanAndAppLevelOrders()
        {
            var container = this.GetCollectionContainer();
            var assetId = container.Add(ConsumerId1, new Asset());
            Assert.AreEqual(1, container.Assets.OrderingInterface.Count);
            Assert.AreEqual(1, container.GetOrderForApplication<DataObjectKind.Asset>(AppId).Count);

            container.Remove(assetId);

            Assert.AreEqual(0, container.Assets.OrderingInterface.Count);
            Assert.AreEqual(0, container.GetOrderForApplication<DataObjectKind.Asset>(AppId).Count);
        }

        [Test]
        public void SetOwnership_MaintainPrimaryOwnerAddAdditional_OnlyAdds1Association()
        {
            var container = this.GetCollectionContainer(this.GetLoanDataWithLegacyAppsAndConsumerAssociations());
            var assetId = container.Add(ConsumerId1, new Asset());
            var primaryOwnershipAssociation = container.ConsumerAssets.Single();

            container.SetOwnership(assetId, ConsumerId1, new[] { ConsumerId2 });

            // Verify original association is still in tact.
            Assert.AreEqual(true, container.ConsumerAssets.ContainsKey(primaryOwnershipAssociation.Key));
            Assert.AreEqual(ConsumerId1, container.ConsumerAssets[primaryOwnershipAssociation.Key].ConsumerId);
            Assert.AreEqual(assetId, container.ConsumerAssets[primaryOwnershipAssociation.Key].AssetId);

            // Verify we added one.
            Assert.AreEqual(2, container.ConsumerAssets.Count);
            var newAssociation = container.ConsumerAssets.Single(a => a.Key != primaryOwnershipAssociation.Key);
            Assert.AreEqual(ConsumerId2, newAssociation.Value.ConsumerId);
            Assert.AreEqual(assetId, newAssociation.Value.AssetId);
        }

        [Test]
        public void SetOwnership_FlipPrimarySecondaryRemoveExistingPrimary_RemovesOldPrimaryAssocAndUpdatesExisting()
        {
            var container = this.GetCollectionContainer(this.GetLoanDataWithLegacyAppsAndConsumerAssociations());

            var assetId = container.Add(ConsumerId1, new Asset());
            container.AddOwnership(ConsumerId2, assetId);

            var originalPrimaryAssociation = container.ConsumerAssets.First(kvp => kvp.Value.IsPrimary);

            container.SetOwnership(assetId, ConsumerId2, null);

            Assert.AreEqual(false, container.ConsumerAssets.ContainsKey(originalPrimaryAssociation.Key));

            Assert.AreEqual(1, container.ConsumerAssets.Count);
            var newAssociation = container.ConsumerAssets.Single();
            Assert.AreEqual(ConsumerId2, newAssociation.Value.ConsumerId);
            Assert.AreEqual(assetId, newAssociation.Value.AssetId);
        }

        [Test]
        public void SetOwnership_RemoveExistingPrimaryAndMakeSecondaryPrimary_RemovesOldPrimaryAssocAndUpdatesExisting()
        {
            var container = this.GetCollectionContainer(this.GetLoanDataWithLegacyAppsAndConsumerAssociations());
            var assetId = container.Add(ConsumerId1, new Asset());
            var originalPrimaryAssociation = container.ConsumerAssets.Single();

            container.SetOwnership(assetId, ConsumerId2, null);

            Assert.AreEqual(false, container.ConsumerAssets.ContainsKey(originalPrimaryAssociation.Key));

            Assert.AreEqual(1, container.ConsumerAssets.Count);
            var newAssociation = container.ConsumerAssets.Single();
            Assert.AreEqual(ConsumerId2, newAssociation.Value.ConsumerId);
            Assert.AreEqual(assetId, newAssociation.Value.AssetId);
        }

        [Test]
        public void SetOwnership_NoAdditionalOwnersWithExisting_RemovesNonPrimaryAssociations()
        {
            var container = this.GetCollectionContainer();
            var assetId = container.Add(ConsumerId1, new Asset());
            var primaryOwnershipAssociation = container.ConsumerAssets.Single();
            container.AddOwnership(ConsumerId2, assetId);

            container.SetOwnership(assetId, ConsumerId1, null);

            Assert.AreEqual(1, container.ConsumerAssets.Count);
            Assert.AreEqual(true, container.ConsumerAssets.ContainsKey(primaryOwnershipAssociation.Key));
            Assert.AreEqual(ConsumerId1, container.ConsumerAssets[primaryOwnershipAssociation.Key].ConsumerId);
            Assert.AreEqual(assetId, container.ConsumerAssets[primaryOwnershipAssociation.Key].AssetId);
        }

        [Test]
        public void SetOwnership_ChangeAdditionalBorrowers_RemovesAndAddsExpectedAssociations()
        {
            var container = this.GetCollectionContainer(this.GetLoanDataWithLegacyAppsAndConsumerAssociations());
            var assetId = container.Add(ConsumerId1, new Asset());
            var primaryOwnershipAssociation = container.ConsumerAssets.Single();
            container.AddOwnership(ConsumerId2, assetId);
            container.AddOwnership(ConsumerId4, assetId);
            container.AddOwnership(ConsumerId6, assetId);

            container.SetOwnership(assetId, ConsumerId1, new[] { ConsumerId3, ConsumerId5 });

            Assert.AreEqual(3, container.ConsumerAssets.Count);
            Assert.AreEqual(0, container.ConsumerAssets.Values.Count(a => a.ConsumerId == ConsumerId2));
            Assert.AreEqual(0, container.ConsumerAssets.Values.Count(a => a.ConsumerId == ConsumerId4));
            Assert.AreEqual(0, container.ConsumerAssets.Values.Count(a => a.ConsumerId == ConsumerId6));
            Assert.AreEqual(1, container.ConsumerAssets.Values.Count(a => a.ConsumerId == ConsumerId1));
            Assert.AreEqual(1, container.ConsumerAssets.Values.Count(a => a.ConsumerId == ConsumerId3));
            Assert.AreEqual(1, container.ConsumerAssets.Values.Count(a => a.ConsumerId == ConsumerId5));
        }

        [Test]
        public void SetOwnership_SwapPrimaryAndSecondaryOwner_OnlyTogglesIsPrimary()
        {
            var container = this.GetCollectionContainer();
            var assetId = container.Add(ConsumerId1, new Asset());
            var originalPrimaryOwnershipAssociation = container.ConsumerAssets.Single();
            var originalAdditionalOwnershipAssociationId = container.AddOwnership(ConsumerId5, assetId);

            container.SetOwnership(assetId, ConsumerId5, new[] { ConsumerId1 });

            Assert.AreEqual(false, container.ConsumerAssets[originalPrimaryOwnershipAssociation.Key].IsPrimary);
            Assert.AreEqual(true, container.ConsumerAssets[originalAdditionalOwnershipAssociationId].IsPrimary);
        }

        [Test]
        public void SetOwnership_UpdatePrimaryAndSecondaryOwner_CorrectlyUpdatesAppId()
        {
            var container = this.GetCollectionContainer(this.GetLoanDataWithLegacyAppsAndConsumerAssociations());
            var assetId = container.Add(ConsumerId1, new Asset());
            var originalPrimaryOwnershipAssociation = container.ConsumerAssets.Single();
            var originalAdditionalOwnershipAssociationId = container.AddOwnership(ConsumerId5, assetId);

            container.SetOwnership(assetId, ConsumerId6, new[] { ConsumerId2 });

            Assert.AreEqual(false, container.ConsumerAssets.ContainsKey(originalPrimaryOwnershipAssociation.Key));
            Assert.AreEqual(false, container.ConsumerAssets.ContainsKey(originalAdditionalOwnershipAssociationId));

            var consumerId6Assoc = container.ConsumerAssets.Values.Single(a => a.ConsumerId == ConsumerId6);
            Assert.AreEqual(true, consumerId6Assoc.IsPrimary);
            Assert.AreEqual(AppId3, consumerId6Assoc.AppId);

            var consumerId2Assoc = container.ConsumerAssets.Values.Single(a => a.ConsumerId == ConsumerId2);
            Assert.AreEqual(false, consumerId2Assoc.IsPrimary);
            Assert.AreEqual(AppId, consumerId2Assoc.AppId);
        }

        [Test]
        public void SetOwnership_ToggleOwnershipWithinSameLegacyApplication_DoesNotAddDuplicateItemToAppOrder()
        {
            var container = this.GetCollectionContainer(this.GetLoanDataWithLegacyAppsAndConsumerAssociations());
            var assetId = container.Add(ConsumerId1, new Asset());

            container.SetOwnership(assetId, ConsumerId2, null);

            Assert.AreEqual(1, container.GetOrderForApplication<DataObjectKind.Asset>(AppId).Count);
        }

        [Test]
        public void GetAssetsForUladApplication_Always_ReturnsAssetsWithPrimaryOwnersOnUladApp()
        {
            // Add 2 ULAD applications.
            var uladApplications = LqbCollection<DataObjectKind.UladApplication, Guid, UladApplication>.Create(
                Name.Create("UladApplications").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.UladApplication>(),
                null);
            var uladApp1Id = uladApplications.Add(new UladApplication());
            var uladApp2Id = uladApplications.Add(new UladApplication());
            var orderedUladApplications = OrderedLqbCollection<DataObjectKind.UladApplication, Guid, UladApplication>.Create(
                uladApplications,
                OrderedIdentifierCollection<DataObjectKind.UladApplication, Guid>.Create() as IMutableOrderedIdentifierCollection<DataObjectKind.UladApplication, Guid>);

            // Associate Consumer 1 and Consumer 2 with Ulad App 1.
            // Associate Consumer 3 with Ulad App 2.
            var uladApplicationConsumers = LqbAssociationSet<DataObjectKind.UladApplicationConsumerAssociation, Guid, UladApplicationConsumerAssociation>.Create(
                Name.Create("UladApplicationConsumers").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.UladApplicationConsumerAssociation>(),
                2,
                1);
            var uladApp1Consumer1Assoc = new UladApplicationConsumerAssociation(uladApp1Id, ConsumerId1, AppId);
            var uladApp1Consumer2Assoc = new UladApplicationConsumerAssociation(uladApp1Id, ConsumerId2, AppId);
            var uladApp2Consumer3Assoc = new UladApplicationConsumerAssociation(uladApp2Id, ConsumerId3, AppId2);
            uladApplicationConsumers.Add(uladApp1Consumer1Assoc);
            uladApplicationConsumers.Add(uladApp1Consumer2Assoc);
            uladApplicationConsumers.Add(uladApp2Consumer3Assoc);

            var fakeCollectionProvider = new FakeUladAppLqbCollectionProvider(orderedUladApplications, uladApplicationConsumers);

            var collectionContainer = this.GetCollectionContainer(null, fakeCollectionProvider);

            // Make Consumer 1, Consumer 2, and Consumer 3 primary owner of an asset. Consumer 3 will be primary owner of 2 assets.
            // Make Consumer 1 and Consumer 2 each a secondary owner of Consumer 3 asset.
            var consumer1PrimaryAssetId = collectionContainer.Add(ConsumerId1, new Asset());
            var consumer2PrimaryAssetId = collectionContainer.Add(ConsumerId2, new Asset());
            var consumer3PrimaryAssetId1 = collectionContainer.Add(ConsumerId3, new Asset());
            var consumer3PrimaryAssetId2 = collectionContainer.Add(ConsumerId3, new Asset());
            collectionContainer.AddOwnership(ConsumerId1, consumer3PrimaryAssetId1);
            collectionContainer.AddOwnership(ConsumerId2, consumer3PrimaryAssetId2);

            // Only expect the Consumer 1 and Consumer 2 primary assets.
            var assetsForUladApp1 = collectionContainer.GetAssetsForUladApplication(uladApp1Id);
            CollectionAssert.AreEquivalent(new[] { consumer1PrimaryAssetId, consumer2PrimaryAssetId }, assetsForUladApp1.Select(a => a.Key));

            // Expect the 2 where Consumer 3 is the primary owner.
            var assetsForUladApp2 = collectionContainer.GetAssetsForUladApplication(uladApp2Id);
            CollectionAssert.AreEquivalent(new[] { consumer3PrimaryAssetId1, consumer3PrimaryAssetId2 }, assetsForUladApp2.Select(a => a.Key));
        }

        [Test]
        public void GetAssetsForConsumer_Always_ReturnsAssetsWhereConsumerIsPrimaryOrCoOwner()
        {
            var container = this.GetCollectionContainer();
            var assetId1 = container.Add(ConsumerId1, new Asset());
            var assetId2 = container.Add(ConsumerId1, new Asset());
            var assetId3 = container.Add(ConsumerId2, new Asset());
            container.AddOwnership(ConsumerId1, assetId3);
            var assetId4 = container.Add(ConsumerId2, new Asset());

            var consumer1Assets = container.GetAssetsForConsumer(ConsumerId1);
            var consumer2Assets = container.GetAssetsForConsumer(ConsumerId2);

            CollectionAssert.AreEqual(new[] { assetId1, assetId2, assetId3 }, consumer1Assets.Select(a => a.Key));
            CollectionAssert.AreEqual(new[] { assetId3, assetId4 }, consumer2Assets.Select(a => a.Key));
        }

        [Test]
        public void DuplicateUladApplications_SinglePrimaryUladApplication_CopiesAppAndAssociation()
        {
            var uladApplications = LqbCollection<DataObjectKind.UladApplication, Guid, UladApplication>.Create(
                Name.Create("UladApplications").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.UladApplication>(),
                null);
            var uladApp1Id = uladApplications.Add(new UladApplication() { IsPrimary = true });
            var orderedUladApplications = OrderedLqbCollection<DataObjectKind.UladApplication, Guid, UladApplication>.Create(
                uladApplications,
                OrderedIdentifierCollection<DataObjectKind.UladApplication, Guid>.Create() as IMutableOrderedIdentifierCollection<DataObjectKind.UladApplication, Guid>);

            var uladApplicationConsumers = LqbAssociationSet<DataObjectKind.UladApplicationConsumerAssociation, Guid, UladApplicationConsumerAssociation>.Create(
                Name.Create("UladApplicationConsumers").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.UladApplicationConsumerAssociation>(),
                2,
                1);
            var uladApp1Consumer1Assoc = new UladApplicationConsumerAssociation(uladApp1Id, ConsumerId1, AppId);
            uladApp1Consumer1Assoc.IsPrimary = true;
            uladApplicationConsumers.Add(uladApp1Consumer1Assoc);

            var fakeCollectionProvider = new FakeUladAppLqbCollectionProvider(orderedUladApplications, uladApplicationConsumers);

            var sourceLoanDataFake = Substitute.For<ILoanLqbCollectionContainerLoanDataProvider>();
            sourceLoanDataFake.ActiveConsumerIdentifiers.Returns(new[] { ConsumerId1 });
            var destLoanDataFake = Substitute.For<ILoanLqbCollectionContainerLoanDataProvider>();
            destLoanDataFake.ActiveConsumerIdentifiers.Returns(new[] { ConsumerId2 });
            var sourceCollectionContainer = this.GetCollectionContainer(sourceLoanDataFake, fakeCollectionProvider);
            var destinationCollectionContainer = this.GetCollectionContainer(destLoanDataFake);
            var consumerIdMap = new Dictionary<DataObjectIdentifier<DataObjectKind.Consumer, Guid>, DataObjectIdentifier<DataObjectKind.Consumer, Guid>>()
            {
                [ConsumerId1] = ConsumerId2
            };
            var legacyAppIdMap = new Dictionary<DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>, DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>>()
            {
                [AppId] = AppId2
            };

            destinationCollectionContainer.DuplicateUladApplications(sourceCollectionContainer, consumerIdMap, legacyAppIdMap);

            Assert.AreEqual(1, destinationCollectionContainer.UladApplications.Count);
            var destinationUladApp = destinationCollectionContainer.UladApplications.Values.Single();
            Assert.AreEqual(true, destinationUladApp.IsPrimary);

            Assert.AreEqual(1, destinationCollectionContainer.UladApplicationConsumers.Count);
            var destinationUladAppConsumer = destinationCollectionContainer.UladApplicationConsumers.Values.Single();
            Assert.AreEqual(true, destinationUladAppConsumer.IsPrimary);
            Assert.AreEqual(AppId2, destinationUladAppConsumer.LegacyAppId);
        }

        [Test]
        public void DuplicateUladApplications_SingleAppMultipleBorrowers_CopiesAppsAndAssociations()
        {
            var uladApplications = LqbCollection<DataObjectKind.UladApplication, Guid, UladApplication>.Create(
                Name.Create("UladApplications").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.UladApplication>(),
                null);
            var uladApp1Id = uladApplications.Add(new UladApplication() { IsPrimary = true });
            var orderedUladApplications = OrderedLqbCollection<DataObjectKind.UladApplication, Guid, UladApplication>.Create(
                uladApplications,
                OrderedIdentifierCollection<DataObjectKind.UladApplication, Guid>.Create() as IMutableOrderedIdentifierCollection<DataObjectKind.UladApplication, Guid>);

            var uladApplicationConsumers = LqbAssociationSet<DataObjectKind.UladApplicationConsumerAssociation, Guid, UladApplicationConsumerAssociation>.Create(
                Name.Create("UladApplicationConsumers").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.UladApplicationConsumerAssociation>(),
                -1,
                1);
            var uladApp1Consumer1Assoc = new UladApplicationConsumerAssociation(uladApp1Id, ConsumerId1, AppId);
            uladApp1Consumer1Assoc.IsPrimary = true;
            uladApplicationConsumers.Add(uladApp1Consumer1Assoc);
            var uladApp1Consumer2Assoc = new UladApplicationConsumerAssociation(uladApp1Id, ConsumerId2, AppId);
            uladApp1Consumer2Assoc.IsPrimary = false;
            uladApplicationConsumers.Add(uladApp1Consumer2Assoc);
            var uladApp1Consumer3Assoc = new UladApplicationConsumerAssociation(uladApp1Id, ConsumerId3, AppId);
            uladApp1Consumer3Assoc.IsPrimary = false;
            uladApplicationConsumers.Add(uladApp1Consumer3Assoc);

            var fakeCollectionProvider = new FakeUladAppLqbCollectionProvider(orderedUladApplications, uladApplicationConsumers);

            var sourceLoanDataFake = Substitute.For<ILoanLqbCollectionContainerLoanDataProvider>();
            sourceLoanDataFake.ActiveConsumerIdentifiers.Returns(new[] { ConsumerId1, ConsumerId2, ConsumerId3 });
            var destLoanDataFake = Substitute.For<ILoanLqbCollectionContainerLoanDataProvider>();
            destLoanDataFake.ActiveConsumerIdentifiers.Returns(new[] { ConsumerId4, ConsumerId5, ConsumerId6 });
            var sourceCollectionContainer = this.GetCollectionContainer(sourceLoanDataFake, fakeCollectionProvider);
            var destinationCollectionContainer = this.GetCollectionContainer(destLoanDataFake);
            var consumerIdMap = new Dictionary<DataObjectIdentifier<DataObjectKind.Consumer, Guid>, DataObjectIdentifier<DataObjectKind.Consumer, Guid>>()
            {
                [ConsumerId1] = ConsumerId4,
                [ConsumerId2] = ConsumerId5,
                [ConsumerId3] = ConsumerId6
            };
            var legacyAppIdMap = new Dictionary<DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>, DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>>()
            {
                [AppId] = AppId2
            };

            destinationCollectionContainer.DuplicateUladApplications(sourceCollectionContainer, consumerIdMap, legacyAppIdMap);

            Assert.AreEqual(1, destinationCollectionContainer.UladApplications.Count);
            var destinationUladApp = destinationCollectionContainer.UladApplications.Values.Single();
            Assert.AreEqual(true, destinationUladApp.IsPrimary);

            Assert.AreEqual(3, destinationCollectionContainer.UladApplicationConsumers.Count);
            var destinationUladAppConsumer = destinationCollectionContainer.UladApplicationConsumers.Values.Single(a => a.ConsumerId == ConsumerId4);
            Assert.AreEqual(true, destinationUladAppConsumer.IsPrimary);
            Assert.AreEqual(AppId2, destinationUladAppConsumer.LegacyAppId);

            destinationUladAppConsumer = destinationCollectionContainer.UladApplicationConsumers.Values.Single(a => a.ConsumerId == ConsumerId5);
            Assert.AreEqual(false, destinationUladAppConsumer.IsPrimary);
            Assert.AreEqual(AppId2, destinationUladAppConsumer.LegacyAppId);

            destinationUladAppConsumer = destinationCollectionContainer.UladApplicationConsumers.Values.Single(a => a.ConsumerId == ConsumerId6);
            Assert.AreEqual(false, destinationUladAppConsumer.IsPrimary);
            Assert.AreEqual(AppId2, destinationUladAppConsumer.LegacyAppId);
        }

        [Test]
        public void DuplicateUladApplications_MultipleAppsMultipleBorrowers_CopiesAppsAndAssociations()
        {
            var uladApplications = LqbCollection<DataObjectKind.UladApplication, Guid, UladApplication>.Create(
                Name.Create("UladApplications").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.UladApplication>(),
                null);
            var uladApp1Id = uladApplications.Add(new UladApplication() { IsPrimary = true });
            var uladApp2Id = uladApplications.Add(new UladApplication() { IsPrimary = false });
            var orderedUladApplications = OrderedLqbCollection<DataObjectKind.UladApplication, Guid, UladApplication>.Create(
                uladApplications,
                OrderedIdentifierCollection<DataObjectKind.UladApplication, Guid>.Create() as IMutableOrderedIdentifierCollection<DataObjectKind.UladApplication, Guid>);

            var uladApplicationConsumers = LqbAssociationSet<DataObjectKind.UladApplicationConsumerAssociation, Guid, UladApplicationConsumerAssociation>.Create(
                Name.Create("UladApplicationConsumers").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.UladApplicationConsumerAssociation>(),
                -1,
                1);
            var uladApp1Consumer1Assoc = new UladApplicationConsumerAssociation(uladApp1Id, ConsumerId1, AppId);
            uladApp1Consumer1Assoc.IsPrimary = true;
            uladApplicationConsumers.Add(uladApp1Consumer1Assoc);
            var uladApp1Consumer2Assoc = new UladApplicationConsumerAssociation(uladApp1Id, ConsumerId2, AppId);
            uladApp1Consumer2Assoc.IsPrimary = false;
            uladApplicationConsumers.Add(uladApp1Consumer2Assoc);
            var uladApp2Consumer3Assoc = new UladApplicationConsumerAssociation(uladApp2Id, ConsumerId3, AppId2);
            uladApp2Consumer3Assoc.IsPrimary = true;
            uladApplicationConsumers.Add(uladApp2Consumer3Assoc);
            var uladApp2Consumer4Assoc = new UladApplicationConsumerAssociation(uladApp2Id, ConsumerId4, AppId2);
            uladApp2Consumer4Assoc.IsPrimary = false;
            uladApplicationConsumers.Add(uladApp2Consumer4Assoc);

            var fakeCollectionProvider = new FakeUladAppLqbCollectionProvider(orderedUladApplications, uladApplicationConsumers);

            var sourceLoanDataFake = Substitute.For<ILoanLqbCollectionContainerLoanDataProvider>();
            sourceLoanDataFake.ActiveConsumerIdentifiers.Returns(new[] { ConsumerId1, ConsumerId2, ConsumerId3, ConsumerId4 });
            var destLoanDataFake = Substitute.For<ILoanLqbCollectionContainerLoanDataProvider>();
            destLoanDataFake.ActiveConsumerIdentifiers.Returns(new[] { ConsumerId5, ConsumerId6, ConsumerId7, ConsumerId8 });
            var sourceCollectionContainer = this.GetCollectionContainer(sourceLoanDataFake, fakeCollectionProvider);
            var destinationCollectionContainer = this.GetCollectionContainer(destLoanDataFake);
            var consumerIdMap = new Dictionary<DataObjectIdentifier<DataObjectKind.Consumer, Guid>, DataObjectIdentifier<DataObjectKind.Consumer, Guid>>()
            {
                [ConsumerId1] = ConsumerId5,
                [ConsumerId2] = ConsumerId6,
                [ConsumerId3] = ConsumerId7,
                [ConsumerId4] = ConsumerId8
            };
            var legacyAppIdMap = new Dictionary<DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>, DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>>()
            {
                [AppId] = AppId3,
                [AppId2] = AppId4
            };

            destinationCollectionContainer.DuplicateUladApplications(sourceCollectionContainer, consumerIdMap, legacyAppIdMap);

            Assert.AreEqual(2, destinationCollectionContainer.UladApplications.Count);
            Assert.AreEqual(1, destinationCollectionContainer.UladApplications.Values.Count(a => a.IsPrimary));

            Assert.AreEqual(4, destinationCollectionContainer.UladApplicationConsumers.Count);
            var destinationUladAppConsumer = destinationCollectionContainer.UladApplicationConsumers.Values.Single(a => a.ConsumerId == ConsumerId5);
            Assert.AreEqual(true, destinationUladAppConsumer.IsPrimary);
            Assert.AreEqual(AppId3, destinationUladAppConsumer.LegacyAppId);

            destinationUladAppConsumer = destinationCollectionContainer.UladApplicationConsumers.Values.Single(a => a.ConsumerId == ConsumerId6);
            Assert.AreEqual(false, destinationUladAppConsumer.IsPrimary);
            Assert.AreEqual(AppId3, destinationUladAppConsumer.LegacyAppId);

            destinationUladAppConsumer = destinationCollectionContainer.UladApplicationConsumers.Values.Single(a => a.ConsumerId == ConsumerId7);
            Assert.AreEqual(true, destinationUladAppConsumer.IsPrimary);
            Assert.AreEqual(AppId4, destinationUladAppConsumer.LegacyAppId);

            destinationUladAppConsumer = destinationCollectionContainer.UladApplicationConsumers.Values.Single(a => a.ConsumerId == ConsumerId8);
            Assert.AreEqual(false, destinationUladAppConsumer.IsPrimary);
            Assert.AreEqual(AppId4, destinationUladAppConsumer.LegacyAppId);
        }

        [Test]
        public void DuplicateUladApplications_DestinationHasAppsAndAssociations_ClearsExistingAppsAndAssociations()
        {
            var uladApplications = LqbCollection<DataObjectKind.UladApplication, Guid, UladApplication>.Create(
                Name.Create("UladApplications").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.UladApplication>(),
                null);
            var uladApp1Id = uladApplications.Add(new UladApplication() { IsPrimary = true });
            var orderedUladApplications = OrderedLqbCollection<DataObjectKind.UladApplication, Guid, UladApplication>.Create(
                uladApplications,
                OrderedIdentifierCollection<DataObjectKind.UladApplication, Guid>.Create() as IMutableOrderedIdentifierCollection<DataObjectKind.UladApplication, Guid>);

            var uladApplicationConsumers = LqbAssociationSet<DataObjectKind.UladApplicationConsumerAssociation, Guid, UladApplicationConsumerAssociation>.Create(
                Name.Create("UladApplicationConsumers").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.UladApplicationConsumerAssociation>(),
                -1,
                1);
            var uladApp1Consumer1Assoc = new UladApplicationConsumerAssociation(uladApp1Id, ConsumerId1, AppId);
            uladApp1Consumer1Assoc.IsPrimary = true;
            uladApplicationConsumers.Add(uladApp1Consumer1Assoc);

            var fakeCollectionProvider = new FakeUladAppLqbCollectionProvider(orderedUladApplications, uladApplicationConsumers);

            var sourceLoanDataFake = Substitute.For<ILoanLqbCollectionContainerLoanDataProvider>();
            sourceLoanDataFake.ActiveConsumerIdentifiers.Returns(new[] { ConsumerId1 });
            var destLoanDataFake = Substitute.For<ILoanLqbCollectionContainerLoanDataProvider>();
            destLoanDataFake.ActiveConsumerIdentifiers.Returns(new[] { ConsumerId2 });
            var sourceCollectionContainer = this.GetCollectionContainer(sourceLoanDataFake);
            var destinationCollectionContainer = this.GetCollectionContainer(destLoanDataFake, fakeCollectionProvider);
            var consumerIdMap = new Dictionary<DataObjectIdentifier<DataObjectKind.Consumer, Guid>, DataObjectIdentifier<DataObjectKind.Consumer, Guid>>()
            {
                [ConsumerId1] = ConsumerId2,
            };
            var legacyAppIdMap = new Dictionary<DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>, DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>>()
            {
                [AppId] = AppId2,
            };

            destinationCollectionContainer.DuplicateUladApplications(sourceCollectionContainer, consumerIdMap, legacyAppIdMap);

            Assert.AreEqual(0, destinationCollectionContainer.UladApplications.Count);
            Assert.AreEqual(0, destinationCollectionContainer.UladApplicationConsumers.Count);
        }

        [Test]
        public void RemoveCoborrowerFromLegacyApplication_NoCoborrowerDefined_ThrowsException()
        {
            var idFactory = new GuidDataObjectIdentifierFactory<DataObjectKind.LegacyApplicationConsumerAssociation>();
            var associationDict = new Dictionary<DataObjectIdentifier<DataObjectKind.LegacyApplicationConsumerAssociation, Guid>, ILegacyApplicationConsumerAssociation>()
            {
                [idFactory.NewId()] = new ReadOnlyLegacyApplicationConsumerAssociation(AppId, ConsumerId1, isPrimary: true),
            };
            var legacyAppConsumers = new ReadOnlyLqbCollection<DataObjectKind.LegacyApplicationConsumerAssociation, Guid, ILegacyApplicationConsumerAssociation>(
                Name.Create("LegacyApplicationConsumers").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.LegacyApplicationConsumerAssociation>(),
                associationDict);
            var loanDataSubstitute = Substitute.For<ILoanLqbCollectionContainerLoanDataProvider>();
            loanDataSubstitute.LegacyApplicationConsumers.Returns(legacyAppConsumers);

            var collectionContainer = this.GetCollectionContainer(loanDataSubstitute);

            Assert.Throws<CBaseException>(() => collectionContainer.RemoveCoborrowerFromLegacyApplication(AppId));
        }

        [Test]
        public void RemoveCoborrowerFromLegacyApplication_Always_CallsBorrowerManagementToRemove()
        {
            // Has 3 legacy apps, each with 2 consumers defined.
            var loanDataSubstitute = this.GetLoanDataWithLegacyAppsAndConsumerAssociations();
            var uladApps = this.GetEmptyUladAppCollection();
            var uladAppConsumers = this.GetEmptyUladAppConsumersAssociationSet();
            this.AddUladAppWithConsumers(uladApps, uladAppConsumers, true, ConsumerId1, AppId, ConsumerId2);
            var collectionProviderSubstitute = new FakeUladAppLqbCollectionProvider(uladApps, uladAppConsumers);
            var borrowerManagementSubstitute = Substitute.For<ILoanLqbCollectionBorrowerManagement>();
            var collectionContainer = this.GetCollectionContainer(loanDataSubstitute, collectionProviderSubstitute, borrowerManagementSubstitute);

            collectionContainer.RemoveCoborrowerFromLegacyApplication(AppId);

            borrowerManagementSubstitute
                .Received()
                .BorrowerManagementOnly_RemoveCoborrowerFromLegacyApplication(AppId);
        }

        [Test]
        public void RemoveCoborrowerFromLegacyApplication_Always_RemovesConsumerFromUladApplication()
        {
            // Has 3 legacy apps, each with 2 consumers defined.
            var loanDataSubstitute = this.GetLoanDataWithLegacyAppsAndConsumerAssociations();
            var uladApps = this.GetEmptyUladAppCollection();
            var uladAppConsumers = this.GetEmptyUladAppConsumersAssociationSet();
            this.AddUladAppWithConsumers(uladApps, uladAppConsumers, true, ConsumerId1, AppId, ConsumerId2);
            var collectionProviderSubstitute = new FakeUladAppLqbCollectionProvider(uladApps, uladAppConsumers);
            var borrowerManagementSubstitute = Substitute.For<ILoanLqbCollectionBorrowerManagement>();
            var collectionContainer = this.GetCollectionContainer(loanDataSubstitute, collectionProviderSubstitute, borrowerManagementSubstitute);

            collectionContainer.RemoveCoborrowerFromLegacyApplication(AppId);

            Assert.AreEqual(1, collectionContainer.UladApplicationConsumers.Count);
            var association = collectionContainer.UladApplicationConsumers.Values.Single();
            Assert.AreEqual(ConsumerId1, association.ConsumerId);
            Assert.AreEqual(true, association.IsPrimary);
            Assert.AreEqual(AppId, association.LegacyAppId);
        }

        [Test]
        public void RemoveCoborrowerFromLegacyApplication_WithExclusiveOwnerships_ThrowsException()
        {
            // Has 3 legacy apps, each with 2 consumers defined.
            var loanDataSubstitute = this.GetLoanDataWithLegacyAppsAndConsumerAssociations();
            var uladApps = this.GetEmptyUladAppCollection();
            var uladAppConsumers = this.GetEmptyUladAppConsumersAssociationSet();
            this.AddUladAppWithConsumers(uladApps, uladAppConsumers, true, ConsumerId1, AppId, ConsumerId2);
            var collectionProviderSubstitute = new FakeUladAppLqbCollectionProvider(uladApps, uladAppConsumers);
            var borrowerManagementSubstitute = Substitute.For<ILoanLqbCollectionBorrowerManagement>();
            var collectionContainer = this.GetCollectionContainer(loanDataSubstitute, collectionProviderSubstitute, borrowerManagementSubstitute);
            var borrowerAssetId = collectionContainer.Add(ConsumerId1, new Asset());
            var coborrowerAssetId = collectionContainer.Add(ConsumerId2, new Asset());

            Assert.Throws<CBaseException>(() => collectionContainer.RemoveCoborrowerFromLegacyApplication(AppId));
        }

        [Test]
        public void RemoveCoborrowerFromLegacyApplication_WithOnlySharedOwnerships_ThrowsException()
        {
            // Has 3 legacy apps, each with 2 consumers defined.
            var loanDataSubstitute = this.GetLoanDataWithLegacyAppsAndConsumerAssociations();
            var uladApps = this.GetEmptyUladAppCollection();
            var uladAppConsumers = this.GetEmptyUladAppConsumersAssociationSet();
            this.AddUladAppWithConsumers(uladApps, uladAppConsumers, true, ConsumerId1, AppId, ConsumerId2);
            var collectionProviderSubstitute = new FakeUladAppLqbCollectionProvider(uladApps, uladAppConsumers);
            var borrowerManagementSubstitute = Substitute.For<ILoanLqbCollectionBorrowerManagement>();
            var collectionContainer = this.GetCollectionContainer(loanDataSubstitute, collectionProviderSubstitute, borrowerManagementSubstitute);
            var borrowerAssetId = collectionContainer.Add(ConsumerId1, new Asset());
            var coborrowerAssetId = collectionContainer.Add(ConsumerId2, new Asset());
            collectionContainer.AddOwnership(ConsumerId2, borrowerAssetId);
            collectionContainer.AddOwnership(ConsumerId1, coborrowerAssetId);

            Assert.AreEqual(2, collectionContainer.Assets.Count);
            Assert.AreEqual(4, collectionContainer.ConsumerAssets.Count);

            Assert.Throws<CBaseException>(() => collectionContainer.RemoveCoborrowerFromLegacyApplication(AppId));
        }

        [Test]
        public void RemoveCoborrowerFromLegacyApplication_ConsumerOnlyBorrowerOnUladApp_RemovesUladApp()
        {
            // Has 3 legacy apps, each with 2 consumers defined.
            var loanDataSubstitute = this.GetLoanDataWithLegacyAppsAndConsumerAssociations();
            var uladApps = this.GetEmptyUladAppCollection();
            var uladAppConsumers = this.GetEmptyUladAppConsumersAssociationSet();
            this.AddUladAppWithConsumers(uladApps, uladAppConsumers, true, ConsumerId1, AppId);
            this.AddUladAppWithConsumers(uladApps, uladAppConsumers, false, ConsumerId2, AppId);
            var collectionProviderSubstitute = new FakeUladAppLqbCollectionProvider(uladApps, uladAppConsumers);
            var borrowerManagementSubstitute = Substitute.For<ILoanLqbCollectionBorrowerManagement>();
            var collectionContainer = this.GetCollectionContainer(loanDataSubstitute, collectionProviderSubstitute, borrowerManagementSubstitute);

            collectionContainer.RemoveCoborrowerFromLegacyApplication(AppId);

            Assert.AreEqual(1, collectionContainer.UladApplications.Count);
            Assert.AreEqual(1, collectionContainer.UladApplicationConsumers.Count);
            var association = collectionContainer.UladApplicationConsumers.Values.Single();
            Assert.AreEqual(ConsumerId1, association.ConsumerId);
        }

        [Test]
        public void RemoveCoborrowerFromLegacyApplication_ConsumerPrimaryBorrowerOnUladApp_SetsOtherBorrowerAsPrimary()
        {
            // Has 3 legacy apps, each with 2 consumers defined.
            var loanDataSubstitute = this.GetLoanDataWithLegacyAppsAndConsumerAssociations();
            var uladApps = this.GetEmptyUladAppCollection();
            var uladAppConsumers = this.GetEmptyUladAppConsumersAssociationSet();
            this.AddUladAppWithConsumers(uladApps, uladAppConsumers, true, ConsumerId2, AppId, ConsumerId1);
            var collectionProviderSubstitute = new FakeUladAppLqbCollectionProvider(uladApps, uladAppConsumers);
            var borrowerManagementSubstitute = Substitute.For<ILoanLqbCollectionBorrowerManagement>();
            var collectionContainer = this.GetCollectionContainer(loanDataSubstitute, collectionProviderSubstitute, borrowerManagementSubstitute);

            collectionContainer.RemoveCoborrowerFromLegacyApplication(AppId);

            Assert.AreEqual(1, collectionContainer.UladApplicationConsumers.Count);
            var association = collectionContainer.UladApplicationConsumers.Values.Single();
            Assert.AreEqual(ConsumerId1, association.ConsumerId);
            Assert.AreEqual(true, association.IsPrimary);
        }

        [Test]
        public void RemoveCoborrowerFromLegacyApplication_ConsumerOnlyBorrowerOnPrimaryUladApp_RemovesUladAppAndSetsExpectedUladAppAsPrimary()
        {
            // Has 3 legacy apps, each with 2 consumers defined.
            var loanDataSubstitute = this.GetLoanDataWithLegacyAppsAndConsumerAssociations();
            var uladApps = this.GetEmptyUladAppCollection();
            var uladAppConsumers = this.GetEmptyUladAppConsumersAssociationSet();
            var expectedPrimaryUladAppId = this.AddUladAppWithConsumers(uladApps, uladAppConsumers, false, ConsumerId1, AppId);
            this.AddUladAppWithConsumers(uladApps, uladAppConsumers, true, ConsumerId2, AppId);
            this.AddUladAppWithConsumers(uladApps, uladAppConsumers, false, ConsumerId3, AppId, ConsumerId4, ConsumerId5, ConsumerId6);
            var collectionProviderSubstitute = new FakeUladAppLqbCollectionProvider(uladApps, uladAppConsumers);
            var borrowerManagementSubstitute = Substitute.For<ILoanLqbCollectionBorrowerManagement>();
            var collectionContainer = this.GetCollectionContainer(loanDataSubstitute, collectionProviderSubstitute, borrowerManagementSubstitute);

            collectionContainer.RemoveCoborrowerFromLegacyApplication(AppId);

            Assert.AreEqual(1, collectionContainer.UladApplications.Count(a => a.Value.IsPrimary));

            var app = collectionContainer.UladApplications.Single(a => a.Value.IsPrimary);
            Assert.AreEqual(expectedPrimaryUladAppId, app.Key);
        }

        [Test]
        public void Load_WhenCalled_NotifiesObserverForRegisteredCollections()
        {
            var containerFactory = new LoanLqbCollectionContainerFactory();
            var providerFactory = new LoanLqbCollectionProviderFactory();
            var fakeObserver = Substitute.For<ILoanLqbCollectionContainerObserver>();

            var container = containerFactory.Create(
                providerFactory.CreateEmptyProvider(),
                Substitute.For<ILoanLqbCollectionContainerLoanDataProvider>(),
                Substitute.For<ILoanLqbCollectionBorrowerManagement>(),
                fakeObserver);
            container.RegisterCollection(nameof(ILoanLqbCollectionContainer.Assets));
            container.RegisterCollection(nameof(ILoanLqbCollectionContainer.RealProperties));
            container.RegisterCollection(nameof(ILoanLqbCollectionContainer.Liabilities));

            container.Load(
                DataObjectIdentifier<DataObjectKind.ClientCompany, Guid>.Create(Guid.Empty),
                Substitute.For<IDbConnection>(),
                Substitute.For<IDbTransaction>());

            fakeObserver.Received().NotifyCollectionLoad(nameof(ILoanLqbCollectionContainer.Assets));
            fakeObserver.Received().NotifyCollectionLoad(nameof(ILoanLqbCollectionContainer.RealProperties));
            fakeObserver.Received().NotifyCollectionLoad(nameof(ILoanLqbCollectionContainer.Liabilities));
        }

        [Test]
        public void InitializeUladAppsToMatchLegacyApps_UladAppsExist_ThrowsException()
        {
            // Add 2 ULAD applications.
            var uladApplications = LqbCollection<DataObjectKind.UladApplication, Guid, UladApplication>.Create(
                Name.Create("UladApplications").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.UladApplication>(),
                null);
            var uladApp1Id = uladApplications.Add(new UladApplication());
            var orderedUladApplications = OrderedLqbCollection<DataObjectKind.UladApplication, Guid, UladApplication>.Create(
                uladApplications,
                OrderedIdentifierCollection<DataObjectKind.UladApplication, Guid>.Create() as IMutableOrderedIdentifierCollection<DataObjectKind.UladApplication, Guid>);

            var fakeCollectionProvider = new FakeUladAppLqbCollectionProvider(orderedUladApplications, null);

            var container = this.GetCollectionContainer(null, fakeCollectionProvider);
            
            Assert.Throws<CBaseException>(() => container.InitializeUladAppsToMatchLegacyApps());
        }

        [Test]
        public void InitializeUladAppsToMatchLegacyApps_SingleLegacyAppSingleConsumer_CreatesUladAppAndConsumerAssoc()
        {
            var legacyAppDict = new Dictionary<DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>, ILegacyApplication>();
            var primaryLegacyApp = Substitute.For<ILegacyApplication>();
            primaryLegacyApp.IsPrimary.Returns(true);
            legacyAppDict.Add(AppId, primaryLegacyApp);
            var collectionFactory = new ReadOnlyLqbCollectionFactory();
            var legacyAppCollection = collectionFactory.Create(Name.Create("LegacyApplications").Value, new GuidDataObjectIdentifierFactory<DataObjectKind.LegacyApplication>(), legacyAppDict);
            var assocIdFactory = new GuidDataObjectIdentifierFactory<DataObjectKind.LegacyApplicationConsumerAssociation>();
            var associationDict = new Dictionary<DataObjectIdentifier<DataObjectKind.LegacyApplicationConsumerAssociation, Guid>, ILegacyApplicationConsumerAssociation>()
            {
                [assocIdFactory.NewId()] = new ReadOnlyLegacyApplicationConsumerAssociation(AppId, ConsumerId1, isPrimary: true),
            };
            var legacyAppConsumers = new ReadOnlyLqbCollection<DataObjectKind.LegacyApplicationConsumerAssociation, Guid, ILegacyApplicationConsumerAssociation>(Name.Create("LegacyApplicationConsumers").Value, new GuidDataObjectIdentifierFactory<DataObjectKind.LegacyApplicationConsumerAssociation>(), associationDict);
            var loanDataSubstitute = Substitute.For<ILoanLqbCollectionContainerLoanDataProvider>();
            loanDataSubstitute.LegacyApplications.Returns(legacyAppCollection);
            loanDataSubstitute.LegacyApplicationConsumers.Returns(legacyAppConsumers);
            var container = this.GetCollectionContainer(loanDataSubstitute);

            container.InitializeUladAppsToMatchLegacyApps();

            Assert.AreEqual(1, container.UladApplications.Count);
            Assert.AreEqual(1, container.UladApplicationConsumers.Count);
            var app = container.UladApplications.Values.Single();
            Assert.AreEqual(true, app.IsPrimary);
            var assoc = container.UladApplicationConsumers.Values.Single();
            Assert.AreEqual(container.UladApplications.Keys.Single(), assoc.UladApplicationId);
            Assert.AreEqual(ConsumerId1, assoc.ConsumerId);
            Assert.AreEqual(true, assoc.IsPrimary);
            Assert.AreEqual(AppId, assoc.LegacyAppId);
        }
        
        [Test]
        public void InitializeUladAppsToMatchLegacyApps_MultiAppMultiConsumer_CreatesUladAppAndConsumerAssoc()
        {
            var loanData = this.GetLoanDataWithLegacyAppsAndConsumerAssociations();
            var container = this.GetCollectionContainer(loanData);

            container.InitializeUladAppsToMatchLegacyApps();

            Assert.AreEqual(3, container.UladApplications.Count);
            Assert.AreEqual(6, container.UladApplicationConsumers.Count);
            Assert.AreEqual(1, container.UladApplications.Values.Count(a => a.IsPrimary));
            var primaryAppId = container.UladApplications.Single(a => a.Value.IsPrimary).Key;
            var primaryAppConsumerIds = container.UladApplicationConsumers.Values
                .Where(a => a.UladApplicationId == primaryAppId)
                .Select(a => a.ConsumerId);
            CollectionAssert.AreEquivalent(new[] { ConsumerId1, ConsumerId2}, primaryAppConsumerIds);
            Assert.IsTrue(container.UladApplicationConsumers.Values.Single(a => a.ConsumerId == ConsumerId1).IsPrimary);
            Assert.IsFalse(container.UladApplicationConsumers.Values.Single(a => a.ConsumerId == ConsumerId2).IsPrimary);
            
            Assert.DoesNotThrow(() =>
                container.UladApplicationConsumers.Values
                    .Where(a => a.ConsumerId == ConsumerId3 || a.ConsumerId == ConsumerId4)
                    .Select(a => a.UladApplicationId)
                    .Distinct()
                    .Single());
            Assert.IsTrue(container.UladApplicationConsumers.Values.Single(a => a.ConsumerId == ConsumerId3).IsPrimary);
            Assert.IsFalse(container.UladApplicationConsumers.Values.Single(a => a.ConsumerId == ConsumerId4).IsPrimary);
            
            Assert.DoesNotThrow(() =>
                container.UladApplicationConsumers.Values
                    .Where(a => a.ConsumerId == ConsumerId5 || a.ConsumerId == ConsumerId6)
                    .Select(a => a.UladApplicationId)
                    .Distinct()
                    .Single());
            Assert.IsTrue(container.UladApplicationConsumers.Values.Single(a => a.ConsumerId == ConsumerId5).IsPrimary);
            Assert.IsFalse(container.UladApplicationConsumers.Values.Single(a => a.ConsumerId == ConsumerId6).IsPrimary);
        }

        [Test]
        public void Load_WhenCalled_SetsIsLoaded()
        {
            var containerFactory = new LoanLqbCollectionContainerFactory();
            var container = containerFactory.Create(
                new LoanLqbEmptyCollectionProvider(),
                Substitute.For<ILoanLqbCollectionContainerLoanDataProvider>(),
                Substitute.For<ILoanLqbCollectionBorrowerManagement>(),
                null);

            container.Load(
                DataObjectIdentifier<DataObjectKind.ClientCompany, Guid>.Create(Guid.Empty),
                null,
                null);

            Assert.IsTrue(container.IsLoaded);
        }

        [Test]
        public void Load_Always_LoadsUladAppsAndAssociations()
        {
            // We have fixup code that runs on save that requires the ULAD apps and associations
            // to be around. Therefore, we should always load these even if they aren't explicitly
            // registered.
            var containerFactory = new LoanLqbCollectionContainerFactory();
            var providerFactory = new LoanLqbCollectionProviderFactory();
            var fakeObserver = Substitute.For<ILoanLqbCollectionContainerObserver>();

            var container = containerFactory.Create(
                providerFactory.CreateEmptyProvider(),
                Substitute.For<ILoanLqbCollectionContainerLoanDataProvider>(),
                Substitute.For<ILoanLqbCollectionBorrowerManagement>(),
                fakeObserver);
            container.RegisterCollection(nameof(ILoanLqbCollectionContainer.Assets));
            container.RegisterCollection(nameof(ILoanLqbCollectionContainer.RealProperties));
            container.RegisterCollection(nameof(ILoanLqbCollectionContainer.Liabilities));

            container.Load(
                DataObjectIdentifier<DataObjectKind.ClientCompany, Guid>.Create(Guid.Empty),
                Substitute.For<IDbConnection>(),
                Substitute.For<IDbTransaction>());

            Assert.IsNotNull(container.UladApplications);
            Assert.IsNotNull(container.UladApplicationConsumers);
            fakeObserver.Received().NotifyCollectionLoad(nameof(ILoanLqbCollectionContainer.UladApplications));
            fakeObserver.Received().NotifyCollectionLoad(nameof(ILoanLqbCollectionContainer.UladApplicationConsumers));
        }
        [Test]
        public void CreateData_FromScratch_Succeeds()
        {
            var container = this.CreateData();

            Assert.IsNotNull(container);
            Assert.AreEqual(1, container.EmploymentRecords.Count);
            Assert.AreEqual(1, container.IncomeSources.Count);
            Assert.AreEqual(1, container.Liabilities.Count);
            Assert.AreEqual(1, container.RealProperties.Count);

            Assert.AreEqual(1, container.ConsumerIncomeSources.Count);
            Assert.AreEqual(1, container.IncomeSourceEmploymentRecords.Count);

            Assert.AreEqual(1, container.ConsumerLiabilities.Count);
            Assert.AreEqual(1, container.ConsumerRealProperties.Count);
            Assert.AreEqual(1, container.RealPropertyLiabilities.Count);
        }

        [Test]
        public void RemoveEmployment_TriangularAssociationsExist_LinksToConsumerAndIncomeSourceRemoved()
        {
            var container = this.CreateData();
            container.Remove(this.employmentId);

            Assert.AreEqual(0, container.EmploymentRecords.Count);
            Assert.AreEqual(1, container.IncomeSources.Count);
            Assert.AreEqual(1, container.Liabilities.Count);
            Assert.AreEqual(1, container.RealProperties.Count);

            Assert.AreEqual(1, container.ConsumerIncomeSources.Count);
            Assert.AreEqual(0, container.IncomeSourceEmploymentRecords.Count);

            Assert.AreEqual(1, container.ConsumerLiabilities.Count);
            Assert.AreEqual(1, container.ConsumerRealProperties.Count);
            Assert.AreEqual(1, container.RealPropertyLiabilities.Count);
        }

        [Test]
        public void RemoveIncome_TriangularAssociationsExist_LinksToConsumerAndEmploymentRecordRemoved()
        {
            var container = this.CreateData();
            container.Remove(this.incomeId);

            Assert.AreEqual(1, container.EmploymentRecords.Count);
            Assert.AreEqual(0, container.IncomeSources.Count);
            Assert.AreEqual(1, container.Liabilities.Count);
            Assert.AreEqual(1, container.RealProperties.Count);

            Assert.AreEqual(0, container.ConsumerIncomeSources.Count);
            Assert.AreEqual(0, container.IncomeSourceEmploymentRecords.Count);

            Assert.AreEqual(1, container.ConsumerLiabilities.Count);
            Assert.AreEqual(1, container.ConsumerRealProperties.Count);
            Assert.AreEqual(1, container.RealPropertyLiabilities.Count);
        }

        [Test]
        public void RemoveLiability_TriangularAssociationsExist_LinksToConsumerAndRealPropertyRemoved()
        {
            var container = this.CreateData();
            container.Remove(this.liabilityId);

            Assert.AreEqual(1, container.EmploymentRecords.Count);
            Assert.AreEqual(1, container.IncomeSources.Count);
            Assert.AreEqual(0, container.Liabilities.Count);
            Assert.AreEqual(1, container.RealProperties.Count);

            Assert.AreEqual(1, container.ConsumerIncomeSources.Count);
            Assert.AreEqual(1, container.IncomeSourceEmploymentRecords.Count);

            Assert.AreEqual(0, container.ConsumerLiabilities.Count);
            Assert.AreEqual(1, container.ConsumerRealProperties.Count);
            Assert.AreEqual(0, container.RealPropertyLiabilities.Count);
        }

        [Test]
        public void RemoveProperty_TriangularAssociationsExist_LinksToConsumerAndLiabilityRemoved()
        {
            var container = this.CreateData();
            container.Remove(this.propertyId);

            Assert.AreEqual(1, container.EmploymentRecords.Count);
            Assert.AreEqual(1, container.IncomeSources.Count);
            Assert.AreEqual(1, container.Liabilities.Count);
            Assert.AreEqual(0, container.RealProperties.Count);

            Assert.AreEqual(1, container.ConsumerIncomeSources.Count);
            Assert.AreEqual(1, container.IncomeSourceEmploymentRecords.Count);

            Assert.AreEqual(1, container.ConsumerLiabilities.Count);
            Assert.AreEqual(0, container.ConsumerRealProperties.Count);
            Assert.AreEqual(0, container.RealPropertyLiabilities.Count);
        }

        [Test]
        public void GetAssets_WhenAssetsCollectionNotRegistered_ThrowsException()
        {
            var container = this.GetCollectionContainer(collectionsToRegister: new[] { nameof(ILoanLqbCollectionContainer.IncomeSources) });

            IReadOnlyOrderedLqbCollection<DataObjectKind.Asset, Guid, Asset> assets;
            Assert.Throws<CBaseException>(() => assets = container.Assets);

        }

        [Test]
        public void GetConsumerAssets_WhenConsumerAssetsAssociationNotRegistered_ThrowsException()
        {
            var container = this.GetCollectionContainer(collectionsToRegister: new[] { nameof(ILoanLqbCollectionContainer.IncomeSources) });

            IReadOnlyLqbAssociationSet<DataObjectKind.ConsumerAssetAssociation, Guid, ConsumerAssetAssociation> consumerAssets;
            Assert.Throws<CBaseException>(() => consumerAssets = container.ConsumerAssets);
        }

        [Test]
        public void Save_ManyChanges_OneDatabaseCall()
        {
            const string DefaultColumnNames = "LoanId";

            // First create the data required for the Load operation
            var dataSet = new FakeDbDataReader.FakeDataSet();

            // Tables can be empty...and since they are empty we won't actually be reading the individual columns.
            var tableSortOrders = dataSet.AddNewTable(DefaultColumnNames);
            var tableAssets = dataSet.AddNewTable(DefaultColumnNames);
            var tableConsumerAssets = dataSet.AddNewTable(DefaultColumnNames);
            var tablePublicRecords = dataSet.AddNewTable(DefaultColumnNames);
            var tableConsumerPublicRecords = dataSet.AddNewTable(DefaultColumnNames);
            var tableUladApps = dataSet.AddNewTable(DefaultColumnNames);
            var tableUladAppConAssocs = dataSet.AddNewTable(DefaultColumnNames);

            var databaseReader = new FakeDbDataReader(dataSet);

            // Create the database mocks
            var selMock = Substitute.For<ISqlDriver>();
            selMock.Select(Arg.Any<DbConnection>(), Arg.Any<DbTransaction>(), Arg.Any<SQLQueryString>(), Arg.Any<IEnumerable<DbParameter>>()).Returns(databaseReader);
            selMock.Update(Arg.Any<DbConnection>(), Arg.Any<DbTransaction>(), Arg.Any<SQLQueryString>(), Arg.Any<IEnumerable<DbParameter>>()).Returns(ModifiedRowCount.Create(10).Value);

            var selFactory = Substitute.For<ISqlDriverFactory>();
            selFactory.Create(Arg.Any<TimeoutInSeconds>()).Returns(selMock);

            var spMock = Substitute.For<IStoredProcedureDriver>();
            spMock.ExecuteNonQuery(Arg.Any<IDbConnection>(), Arg.Any<IDbTransaction>(), Arg.Any<StoredProcedureName>(), Arg.Any<IEnumerable<DbParameter>>()).Returns(ModifiedRowCount.Create(1).Value);

            var spFactory = Substitute.For<IStoredProcedureDriverFactory>();
            spFactory.Create(Arg.Any<TimeoutInSeconds>()).Returns(spMock);

            // Run the test code
            using (var helper = new FoolHelper())
            {
                helper.RegisterMockDriverFactory<ISqlDriverFactory>(selFactory);
                helper.RegisterMockDriverFactory<IStoredProcedureDriverFactory>(spFactory);

                var providerFactory = new LoanLqbCollectionProviderFactory();
                var provider = providerFactory.CreateStoredProcedureProvider(
                    Guid.NewGuid().ToIdentifier<DataObjectKind.Loan>(),
                    spMock,
                    null,
                    () => null,
                    "Save_ManyChanges_OneDatabaseCall");

                var loanMock = Substitute.For<ILoanLqbCollectionContainerLoanDataProvider>();
                loanMock.LegacyApplications.Returns(GetLegacyApplications());
                loanMock.Consumers.Returns(GetLegacyConsumers());
                loanMock.LegacyApplicationConsumers.Returns(GetLegacyApplicationConsumers());

                var container = this.GetCollectionContainer(
                    collectionProvider: provider,
                    collectionsToRegister: new[] { nameof(ILoanLqbCollectionContainer.Assets), nameof(ILoanLqbCollectionContainer.ConsumerAssets), nameof(ILoanLqbCollectionContainer.PublicRecords), nameof(ILoanLqbCollectionContainer.ConsumerPublicRecords) },
                    loanData: loanMock,
                    suppressLoad: false);
                container.BeginTrackingChanges();

                var asset1 = new Asset();
                asset1.AccountName = DescriptionField.Create("First Asset");
                container.Add(ConsumerId1, asset1);

                var asset2 = new Asset();
                asset2.AccountName = DescriptionField.Create("Second Asset");
                container.Add(ConsumerId1, asset2);

                var pubRec = new PublicRecord();
                pubRec.CourtName = EntityName.Create("Court of Public Opinion");
                container.Add(ConsumerId1, pubRec);

                container.Save(null, null);

                // Prior to aggregation, Update wasn't called and Select returned 1 (because Load calls were already aggregated)
                selMock.Received(1).Select(Arg.Any<DbConnection>(), Arg.Any<DbTransaction>(), Arg.Any<SQLQueryString>(), Arg.Any<IEnumerable<DbParameter>>());
                selMock.Received(1).Update(Arg.Any<DbConnection>(), Arg.Any<DbTransaction>(), Arg.Any<SQLQueryString>(), Arg.Any<IEnumerable<DbParameter>>());

                // Prior to aggregation, this was 10 but after aggregation all 10 calls aggregated to a single Update call in ISqlDriver
                spMock.Received(0).ExecuteNonQuery(Arg.Any<IDbConnection>(), Arg.Any<IDbTransaction>(), Arg.Any<StoredProcedureName>(), Arg.Any<IEnumerable<DbParameter>>());
            }
        }

        [Test]
        public void Add_CounselingEvent_AddsRecordAndAssociation()
        {
            var container = this.GetCollectionContainer(this.GetLoanDataWithLegacyAppsAndConsumerAssociations());

            var recordId = container.Add(ConsumerId1, new CounselingEvent());

            Assert.AreEqual(1, container.CounselingEvents.Count);
            Assert.AreEqual(recordId, container.CounselingEvents.Keys.Single());
            Assert.AreEqual(1, container.CounselingEventAttendances.Count);
            Assert.AreEqual(ConsumerId1, container.CounselingEventAttendances.Values.Single().ConsumerId);
            Assert.AreEqual(recordId, container.CounselingEventAttendances.Values.Single().CounselingEventId);
        }

        [Test]
        public void Insert_CounselingEvent_AddsRecordAtIndexAndAssociation()
        {
            var container = this.GetCollectionContainer(this.GetLoanDataWithLegacyAppsAndConsumerAssociations());
            var recordId1 = container.Add(ConsumerId1, new CounselingEvent());
            var recordId2 = container.Add(ConsumerId1, new CounselingEvent());

            var recordId3 = container.Insert(1, ConsumerId1, new CounselingEvent());

            Assert.AreEqual(3, container.CounselingEvents.Count);
            Assert.AreEqual(true, container.CounselingEvents.ContainsKey(recordId3));
            Assert.AreEqual(recordId3, container.CounselingEvents.ElementAt(1).Key);

            var association = container.CounselingEventAttendances.Values.Single(a => a.CounselingEventId == recordId3);
            Assert.AreEqual(ConsumerId1, association.ConsumerId);
        }

        [Test]
        public void ValidateVor_TwoNullSpecialTypes_IsValid()
        {
            var container = this.GetConcreteCollectionContainer(this.GetLoanDataWithLegacyAppsAndConsumerAssociations());

            container.Add(ConsumerId1, new VorRecord());
            container.Add(ConsumerId1, new VorRecord());

            bool isValid = container.ValidateUniqueValues();
            Assert.IsTrue(isValid);
        }

        [Test]
        public void ValidateVor_NullPlusSpecialType_IsValid()
        {
            var container = this.GetConcreteCollectionContainer(this.GetLoanDataWithLegacyAppsAndConsumerAssociations());

            container.Add(ConsumerId1, new VorRecord());

            var specialVor = new VorRecord();
            specialVor.SetLegacyRecordSpecialType(VorSpecialRecordType.BorrowerPresent);
            container.Add(ConsumerId1, specialVor);

            bool isValid = container.ValidateUniqueValues();
            Assert.IsTrue(isValid);
        }

        [Test]
        public void ValidateVor_DistinctSpecialTypes_IsValid()
        {
            var container = this.GetConcreteCollectionContainer(this.GetLoanDataWithLegacyAppsAndConsumerAssociations());

            var specialVor1 = new VorRecord();
            specialVor1.SetLegacyRecordSpecialType(VorSpecialRecordType.BorrowerPresent);
            container.Add(ConsumerId1, specialVor1);

            var specialVor2 = new VorRecord();
            specialVor2.SetLegacyRecordSpecialType(VorSpecialRecordType.CoborrowerPresent);
            container.Add(ConsumerId1, specialVor2);

            bool isValid = container.ValidateUniqueValues();
            Assert.IsTrue(isValid);
        }

        [Test]
        public void ValidateVor_SameSpecialTypes_NotValid()
        {
            var container = this.GetConcreteCollectionContainer(this.GetLoanDataWithLegacyAppsAndConsumerAssociations());

            var specialVor1 = new VorRecord();
            specialVor1.SetLegacyRecordSpecialType(VorSpecialRecordType.BorrowerPresent);
            container.Add(ConsumerId1, specialVor1);

            var specialVor2 = new VorRecord();
            specialVor2.SetLegacyRecordSpecialType(VorSpecialRecordType.BorrowerPresent);
            container.Add(ConsumerId1, specialVor2);

            bool isValid = container.ValidateUniqueValues();
            Assert.IsFalse(isValid);
        }

        [Test]
        public void ValidateVor_SameSpecialTypesAcrossApplications_IsValid()
        {
            var container = this.GetConcreteCollectionContainer(this.GetLoanDataWithLegacyAppsAndConsumerAssociations());

            var specialVor1 = new VorRecord();
            specialVor1.SetLegacyRecordSpecialType(VorSpecialRecordType.BorrowerPresent);
            container.Add(ConsumerId1, specialVor1);

            var specialVor2 = new VorRecord();
            specialVor2.SetLegacyRecordSpecialType(VorSpecialRecordType.BorrowerPresent);
            container.Add(ConsumerId3, specialVor2); // different legacy application via different owning consumer

            bool isValid = container.ValidateUniqueValues();
            Assert.IsTrue(isValid);
        }

        private static IReadOnlyLqbCollection<DataObjectKind.LegacyApplication, Guid, ILegacyApplication> GetLegacyApplications()
        {
            var dict = new Dictionary<DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>, ILegacyApplication>();

            foreach (var appId in new LegacyAppIdentifier[] { AppId, AppId2, AppId3, AppId4 })
            {
                var appMock = Substitute.For<ILegacyApplication>();

                dict.Add(appId, appMock);
            }

            var collectionFactory = new ReadOnlyLqbCollectionFactory();
            return collectionFactory.Create(
                Name.Create("LegacyApplications").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.LegacyApplication>(),
                dict);
        }

        private static IReadOnlyOrderedLqbCollection<DataObjectKind.Consumer, Guid, IConsumer> GetLegacyConsumers()
        {
            var dict = new Dictionary<DataObjectIdentifier<DataObjectKind.Consumer, Guid>, IConsumer>();

            var order = OrderedIdentifierCollection<DataObjectKind.Consumer, Guid>.Create();
            var orderInit = order as IInitializeCollectionOrder<Guid>;
            foreach (var conId in new ConsumerIdentifier[] {ConsumerId1, ConsumerId2, ConsumerId3, ConsumerId4, ConsumerId5, ConsumerId6, ConsumerId7, ConsumerId8})
            {
                var conMock = Substitute.For<IConsumer>();

                dict.Add(conId, conMock);
                orderInit.Add(conId.Value);
            }

            var collectionFactory = new ReadOnlyLqbCollectionFactory();
            var collection = collectionFactory.Create(
                Name.Create("Consumers").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.Consumer>(),
                dict);
            return ReadOnlyOrderedLqbCollection<DataObjectKind.Consumer, Guid, IConsumer>.Create(collection, order);
        }

        private static IReadOnlyLqbAssociationSet<DataObjectKind.LegacyApplicationConsumerAssociation, Guid, ILegacyApplicationConsumerAssociation> GetLegacyApplicationConsumers()
        {
            var dict = new Dictionary<DataObjectIdentifier<DataObjectKind.LegacyApplicationConsumerAssociation, Guid>, ILegacyApplicationConsumerAssociation>();
            var associationFactory = new LegacyApplicationConsumerAssociationFactory();

            var association = associationFactory.Create(AppId, ConsumerId1, true);
            var associationId = Guid.NewGuid().ToIdentifier<DataObjectKind.LegacyApplicationConsumerAssociation>();
            dict.Add(associationId, association);

            association = associationFactory.Create(AppId, ConsumerId2, false);
            associationId = Guid.NewGuid().ToIdentifier<DataObjectKind.LegacyApplicationConsumerAssociation>();
            dict.Add(associationId, association);

            association = associationFactory.Create(AppId2, ConsumerId3, true);
            associationId = Guid.NewGuid().ToIdentifier<DataObjectKind.LegacyApplicationConsumerAssociation>();
            dict.Add(associationId, association);

            association = associationFactory.Create(AppId2, ConsumerId4, false);
            associationId = Guid.NewGuid().ToIdentifier<DataObjectKind.LegacyApplicationConsumerAssociation>();
            dict.Add(associationId, association);

            association = associationFactory.Create(AppId3, ConsumerId5, true);
            associationId = Guid.NewGuid().ToIdentifier<DataObjectKind.LegacyApplicationConsumerAssociation>();
            dict.Add(associationId, association);

            association = associationFactory.Create(AppId3, ConsumerId6, false);
            associationId = Guid.NewGuid().ToIdentifier<DataObjectKind.LegacyApplicationConsumerAssociation>();
            dict.Add(associationId, association);

            association = associationFactory.Create(AppId4, ConsumerId7, true);
            associationId = Guid.NewGuid().ToIdentifier<DataObjectKind.LegacyApplicationConsumerAssociation>();
            dict.Add(associationId, association);

            association = associationFactory.Create(AppId4, ConsumerId8, false);
            associationId = Guid.NewGuid().ToIdentifier<DataObjectKind.LegacyApplicationConsumerAssociation>();
            dict.Add(associationId, association);

            var factory = new ReadOnlyLqbAssociationSetFactory();
            return factory.Create(
                LqbGrammar.DataTypes.Name.Create("LegacyApplicationConsumers").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.LegacyApplicationConsumerAssociation>(),
                dict);
        }

        internal class FakeUladAppLqbCollectionProvider : LoanLqbEmptyCollectionProvider
        {
            public FakeUladAppLqbCollectionProvider(
                IMutableOrderedLqbCollection<DataObjectKind.UladApplication, Guid, UladApplication> uladApps,
                IMutableLqbAssociationSet<DataObjectKind.UladApplicationConsumerAssociation, Guid, UladApplicationConsumerAssociation> uladAppConsumers)
            {
                this.UladApplications = uladApps;
                if (uladAppConsumers != null)
                {
                    this.UladApplicationConsumers = uladAppConsumers;

                    foreach (var uladAppId in uladApps.Keys)
                    {
                        var order = this.RetrieveCollectionOrder<DataObjectKind.Consumer>(uladApplicationId: uladAppId);
                        foreach (var uladAppConsumer in uladAppConsumers.Values.Where(c => c.UladApplicationId == uladAppId))
                        {
                            order.Add(uladAppConsumer.ConsumerId);
                        }
                    }
                }
            }

            public IMutableOrderedLqbCollection<DataObjectKind.UladApplication, Guid, UladApplication> UladApplications { get; private set; }

            public IMutableLqbAssociationSet<DataObjectKind.UladApplicationConsumerAssociation, Guid, UladApplicationConsumerAssociation> UladApplicationConsumers { get; private set; }

            public override IMutableOrderedLqbCollection<DataObjectKind.UladApplication, Guid, UladApplication> LoadUladApplications()
            {
                return this.UladApplications;
            }

            public override IMutableLqbAssociationSet<DataObjectKind.UladApplicationConsumerAssociation, Guid, UladApplicationConsumerAssociation> LoadUladApplicationConsumers()
            {
                return this.UladApplicationConsumers;
            }
        }

        private ILoanLqbCollectionContainer CreateData()
        {
            var providerFactory = new LoanLqbCollectionProviderFactory();
            var loanData = Substitute.For<ILoanLqbCollectionContainerLoanDataProvider>();
            var container = new LoanLqbCollectionContainer(providerFactory.CreateEmptyProvider(), loanData, null, null);
            container.RegisterCollections(LoanLqbCollectionContainer.LoanCollections);
            container.Load(Guid.Empty.ToIdentifier<DataObjectKind.ClientCompany>(), Substitute.For<IDbConnection>(), Substitute.For<IDbTransaction>());

            var legacyAppId = LegacyAppIdentifier.Create(Guid.NewGuid());
            var borrowerId = ConsumerIdentifier.Create(Guid.NewGuid());

            var consumerFactory = new LegacyApplicationConsumerAssociationFactory();
            var factory = new ReadOnlyLqbAssociationSetFactory();
            loanData.LegacyApplicationConsumers.Returns(factory.Create(
                Name.Create(nameof(ILoanLqbCollectionContainer.LegacyApplicationConsumers)).ForceValue(),
                new GuidDataObjectIdentifierFactory<DataObjectKind.LegacyApplicationConsumerAssociation>(),
                new Dictionary<DataObjectIdentifier<DataObjectKind.LegacyApplicationConsumerAssociation, Guid>, ILegacyApplicationConsumerAssociation>
                {
                    [Guid.NewGuid().ToIdentifier<DataObjectKind.LegacyApplicationConsumerAssociation>()] = consumerFactory.Create(legacyAppId, borrowerId, isPrimary: true),
                }));

            var employment = this.CreatePrimaryEmployment();
            var income = this.CreateIncome();
            var liability = this.CreateLiability();
            var property = this.CreateProperty();

            this.employmentId = container.Add(borrowerId, employment);
            this.incomeId = container.Add(borrowerId, income);
            this.liabilityId = container.Add(borrowerId, liability);
            this.propertyId = container.Add(borrowerId, property);

            var incomeEmploymentId = container.AddIncomeSourceEmploymentRecordAssociation(incomeId, employmentId);
            var properyLiabilityId = container.AddRealPropertyLiabilityAssociation(propertyId, liabilityId);

            return container;
        }

        private EmploymentRecord CreatePrimaryEmployment()
        {
            var employment = new EmploymentRecord(Substitute.For<IEmploymentRecordDefaultsProvider>());
            employment.IsPrimary = true;
            return employment;
        }

        private IncomeSource CreateIncome()
        {
            var income = new IncomeSource();
            return income;
        }

        private Liability CreateLiability()
        {
            var liability = new Liability(defaultsProvider: null);
            return liability;
        }

        private RealProperty CreateProperty()
        {
            var property = new RealProperty();
            return property;
        }

        private ILoanLqbCollectionContainerLoanDataProvider GetLoanDataWithLegacyAppsAndConsumerAssociations()
        {
            var legacyAppDict = new Dictionary<DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>, ILegacyApplication>();
            var primaryLegacyApp = Substitute.For<ILegacyApplication>();
            primaryLegacyApp.IsPrimary.Returns(true);
            legacyAppDict.Add(AppId, primaryLegacyApp);
            var app2 = Substitute.For<ILegacyApplication>();
            app2.IsPrimary.Returns(false);
            legacyAppDict.Add(AppId2, app2);
            var app3 = Substitute.For<ILegacyApplication>();
            app3.IsPrimary.Returns(false);
            legacyAppDict.Add(AppId3, app3);
            var collectionFactory = new ReadOnlyLqbCollectionFactory();
            var legacyAppCollection = collectionFactory.Create(
                Name.Create("LegacyApplications").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.LegacyApplication>(),
                legacyAppDict);

            var assocIdFactory = new GuidDataObjectIdentifierFactory<DataObjectKind.LegacyApplicationConsumerAssociation>();
            var associationDict = new Dictionary<DataObjectIdentifier<DataObjectKind.LegacyApplicationConsumerAssociation, Guid>, ILegacyApplicationConsumerAssociation>()
            {
                [assocIdFactory.NewId()] = new ReadOnlyLegacyApplicationConsumerAssociation(AppId, ConsumerId1, isPrimary: true),
                [assocIdFactory.NewId()] = new ReadOnlyLegacyApplicationConsumerAssociation(AppId, ConsumerId2, isPrimary: false),
                [assocIdFactory.NewId()] = new ReadOnlyLegacyApplicationConsumerAssociation(AppId2, ConsumerId3, isPrimary: true),
                [assocIdFactory.NewId()] = new ReadOnlyLegacyApplicationConsumerAssociation(AppId2, ConsumerId4, isPrimary: false),
                [assocIdFactory.NewId()] = new ReadOnlyLegacyApplicationConsumerAssociation(AppId3, ConsumerId5, isPrimary: true),
                [assocIdFactory.NewId()] = new ReadOnlyLegacyApplicationConsumerAssociation(AppId3, ConsumerId6, isPrimary: false),
            };
            var legacyAppConsumers = new ReadOnlyLqbCollection<DataObjectKind.LegacyApplicationConsumerAssociation, Guid, ILegacyApplicationConsumerAssociation>(
                Name.Create("LegacyApplicationConsumers").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.LegacyApplicationConsumerAssociation>(),
                associationDict);
            var loanDataSubstitute = Substitute.For<ILoanLqbCollectionContainerLoanDataProvider>();
            loanDataSubstitute.LegacyApplications.Returns(legacyAppCollection);
            loanDataSubstitute.LegacyApplicationConsumers.Returns(legacyAppConsumers);
            return loanDataSubstitute;
        }

        private IMutableOrderedLqbCollection<DataObjectKind.UladApplication, Guid, UladApplication> GetEmptyUladAppCollection()
        {
            var uladApplications = LqbCollection<DataObjectKind.UladApplication, Guid, UladApplication>.Create(
                Name.Create("UladApplications").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.UladApplication>(),
                null);

            return OrderedLqbCollection<DataObjectKind.UladApplication, Guid, UladApplication>.Create(
                uladApplications,
                OrderedIdentifierCollection<DataObjectKind.UladApplication, Guid>.Create() as IMutableOrderedIdentifierCollection<DataObjectKind.UladApplication, Guid>);
        }

        private IMutableLqbAssociationSet<DataObjectKind.UladApplicationConsumerAssociation, Guid, UladApplicationConsumerAssociation> GetEmptyUladAppConsumersAssociationSet()
        {
            return LqbAssociationSet<DataObjectKind.UladApplicationConsumerAssociation, Guid, UladApplicationConsumerAssociation>.Create(
                Name.Create("UladApplicationConsumers").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.UladApplicationConsumerAssociation>(),
                -1,
                1);
        }

        private DataObjectIdentifier<DataObjectKind.UladApplication, Guid> AddUladAppWithConsumers(
            IMutableLqbCollection<DataObjectKind.UladApplication, Guid, UladApplication> uladApps,
            IMutableLqbAssociationSet<DataObjectKind.UladApplicationConsumerAssociation, Guid, UladApplicationConsumerAssociation> uladAppConsumers,
            bool isPrimaryApp,
            DataObjectIdentifier<DataObjectKind.Consumer, Guid> primaryConsumerId,
            DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> legacyAppId,
            params DataObjectIdentifier<DataObjectKind.Consumer, Guid>[] secondaryConsumerIds)
        {
            var uladApp = new UladApplication()
            {
                IsPrimary = isPrimaryApp
            };

            var uladAppId = uladApps.Add(uladApp);

            var primaryAssoc = new UladApplicationConsumerAssociation(uladAppId, primaryConsumerId, AppId)
            {
                IsPrimary = true
            };

            uladAppConsumers.Add(primaryAssoc);

            foreach (var secondaryConsumerId in secondaryConsumerIds ?? Enumerable.Empty<DataObjectIdentifier<DataObjectKind.Consumer, Guid>>())
            {
                var secondaryAssoc = new UladApplicationConsumerAssociation(uladAppId, secondaryConsumerId, AppId)
                {
                    IsPrimary = false
                };

                uladAppConsumers.Add(secondaryAssoc);
            }

            return uladAppId;
        }

        private Liability GetNewLiability()
        {
            return new Liability(Substitute.For<ILiabilityDefaultsProvider>());
        }

        private EmploymentRecord GetNewEmploymentRecord(bool isPrimary = false)
        {
            return new EmploymentRecord(Substitute.For<IEmploymentRecordDefaultsProvider>())
            {
                IsPrimary = isPrimary
            };
        }

        private LoanLqbCollectionContainer GetConcreteCollectionContainer(
            ILoanLqbCollectionContainerLoanDataProvider loanData = null,
            ILoanLqbCollectionProvider collectionProvider = null,
            ILoanLqbCollectionBorrowerManagement borrMgmt = null,
            IEnumerable<string> collectionsToRegister = null,
            bool suppressLoad = false)
        {
            return (LoanLqbCollectionContainer)this.GetCollectionContainer(loanData, collectionProvider, borrMgmt, collectionsToRegister, suppressLoad);
        }

        private ILoanLqbCollectionContainer GetCollectionContainer(
            ILoanLqbCollectionContainerLoanDataProvider loanData = null,
            ILoanLqbCollectionProvider collectionProvider = null,
            ILoanLqbCollectionBorrowerManagement borrMgmt = null,
            IEnumerable<string> collectionsToRegister = null,
            bool suppressLoad = false)
        {
            if (collectionProvider == null)
            {
                var providerFactory = new LoanLqbCollectionProviderFactory();
                collectionProvider = providerFactory.CreateEmptyProvider();
            }

            var containerFactory = new LoanLqbCollectionContainerFactory();
            var collectionContainer = containerFactory.Create(
                collectionProvider,
                loanData ?? this.GetLoanDataWithLegacyAppsAndConsumerAssociations(),
                borrMgmt ?? Substitute.For<ILoanLqbCollectionBorrowerManagement>(),
                null);

            foreach (var collection in collectionsToRegister ?? LoanLqbCollectionContainer.LoanCollections)
            {
                collectionContainer.RegisterCollection(collection);
            }

            if (!suppressLoad)
            {
                collectionContainer.Load(
                    DataObjectIdentifier<DataObjectKind.ClientCompany, Guid>.Create(Guid.Empty),
                    null,
                    null);
            }

            return collectionContainer;
        }
    }
}
