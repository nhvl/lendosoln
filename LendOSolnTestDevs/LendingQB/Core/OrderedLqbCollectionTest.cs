﻿namespace LendingQB.Test.Developers.LendingQB.Core
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using global::LendingQB.Core;
    using global::LendingQB.Core.Data;
    using LqbGrammar.DataTypes;
    using NUnit.Framework;
    using Utils;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public sealed class OrderedLqbCollectionTest
    {
        private DataObjectIdentifier<DataObjectKind.Asset, Guid> created0;
        private DataObjectIdentifier<DataObjectKind.Asset, Guid> created1;
        private DataObjectIdentifier<DataObjectKind.Asset, Guid> created2;
        private DataObjectIdentifier<DataObjectKind.Asset, Guid> created3;
        private DataObjectIdentifier<DataObjectKind.Asset, Guid> created4;

        [Test]
        public void DifferentIdentifiers()
        {
            this.InitializeCollection();

            Assert.AreNotEqual(created0, created1);
            Assert.AreNotEqual(created2, created1);
            Assert.AreNotEqual(created3, created1);
            Assert.AreNotEqual(created4, created1);

            Assert.AreNotEqual(created0, created2);
            Assert.AreNotEqual(created1, created2);
            Assert.AreNotEqual(created3, created2);
            Assert.AreNotEqual(created4, created2);

            Assert.AreNotEqual(created0, created3);
            Assert.AreNotEqual(created1, created3);
            Assert.AreNotEqual(created2, created3);
            Assert.AreNotEqual(created4, created3);

            Assert.AreNotEqual(created0, created4);
            Assert.AreNotEqual(created1, created4);
            Assert.AreNotEqual(created2, created4);
            Assert.AreNotEqual(created3, created4);
        }

        [Test]
        public void ExpectedOrder()
        {
            var collection = this.InitializeCollection();
            this.CheckOrder(collection, created0, created1, created2, created3, created4);
        }

        [Test]
        public void ModifiedOrder()
        {
            var collection = this.InitializeCollection();
            var ordered = this.InitializeOrderedCollection(collection);
            var getmodifier = (IReadOnlyOrderedLqbCollection<DataObjectKind.Asset, Guid, Asset>)ordered;
            var modifier = getmodifier.OrderingInterface;

            modifier.MoveForward(this.created2);

            // confirm original collection has not been changed
            this.CheckOrder(collection, created0, created1, created2, created3, created4);

            // confirm ordered version has been changed
            this.CheckOrder(ordered, created0, created2, created1, created3, created4);
        }

        [Test]
        public void Add()
        {
            var collection = this.InitializeCollection();
            var ordered = this.InitializeOrderedCollection(collection);
            var getmodifier = (IReadOnlyOrderedLqbCollection<DataObjectKind.Asset, Guid, Asset>)ordered;
            var modifier = getmodifier.OrderingInterface;

            var addedId = ordered.Add(new Asset());
            Assert.AreEqual(6, collection.Count);
            Assert.AreEqual(6, ordered.Count);
            Assert.AreEqual(6, modifier.Count);

            this.CheckOrder(ordered, created0, created1, created2, created3, created4, addedId);
            this.CheckOrder(modifier, created0, created1, created2, created3, created4, addedId);
        }

        [Test]
        public void AddAtIndex_ToMiddleOfCollection_ResultsInExpectedOrder()
        {
            var collection = this.InitializeCollection();
            var ordered = this.InitializeOrderedCollection(collection);
            var getmodifier = (IReadOnlyOrderedLqbCollection<DataObjectKind.Asset, Guid, Asset>)ordered;
            var modifier = getmodifier.OrderingInterface;

            var addedId = ordered.Insert(3, new Asset());
            Assert.AreEqual(6, collection.Count);
            Assert.AreEqual(6, ordered.Count);
            Assert.AreEqual(6, modifier.Count);

            this.CheckOrder(ordered, created0, created1, created2, addedId, created3, created4);
            this.CheckOrder(modifier, created0, created1, created2, addedId, created3, created4);
        }

        [Test]
        public void Remove()
        {
            var collection = this.InitializeCollection();
            var ordered = this.InitializeOrderedCollection(collection);
            var getmodifier = (IReadOnlyOrderedLqbCollection<DataObjectKind.Asset, Guid, Asset>)ordered;
            var modifier = getmodifier.OrderingInterface;

            ordered.Remove(created1);
            Assert.AreEqual(4, collection.Count);
            Assert.AreEqual(4, ordered.Count);
            Assert.AreEqual(4, modifier.Count);

            this.CheckOrder(ordered, created0, created2, created3, created4);
            this.CheckOrder(modifier, created0, created2, created3, created4);
        }

        [Test]
        public void Constructor_OrderContainsExtraIds_RemovesExtraIdsFromOrder()
        {
            var idFactory = new GuidDataObjectIdentifierFactory<DataObjectKind.Asset>();
            var unorderedCollection = LqbCollection<DataObjectKind.Asset, Guid, Asset>.Create(Name.Create("Test").Value, idFactory);
            var order = OrderedIdentifierCollection<DataObjectKind.Asset, Guid>.Create() as IMutableOrderedIdentifierCollection<DataObjectKind.Asset, Guid>;
            order.Add(idFactory.NewId());
            order.Add(idFactory.NewId());

            var orderedCollection = OrderedLqbCollection<DataObjectKind.Asset, Guid, Asset>.Create(unorderedCollection, order);

            Assert.AreEqual(0, order.Count);
            Assert.AreEqual(0, orderedCollection.Keys.Count());
            Assert.AreEqual(0, orderedCollection.Values.Count());
        }

        [Test]
        public void Constructor_OrderMissingItems_AppendsMissingItemsToOrder()
        {
            var idFactory = new GuidDataObjectIdentifierFactory<DataObjectKind.Asset>();
            var unorderedCollection = LqbCollection<DataObjectKind.Asset, Guid, Asset>.Create(Name.Create("Test").Value, idFactory);
            unorderedCollection.Add(new Asset());
            unorderedCollection.Add(new Asset());
            unorderedCollection.Add(new Asset());
            var order = OrderedIdentifierCollection<DataObjectKind.Asset, Guid>.Create() as IMutableOrderedIdentifierCollection<DataObjectKind.Asset, Guid>;

            var orderedCollection = OrderedLqbCollection<DataObjectKind.Asset, Guid, Asset>.Create(unorderedCollection, order);

            Assert.AreEqual(3, order.Count);
            Assert.AreEqual(3, orderedCollection.Keys.Count());
            Assert.AreEqual(3, orderedCollection.Values.Count());
        }

        private void CheckOrder(IEnumerable<KeyValuePair<DataObjectIdentifier<DataObjectKind.Asset, Guid>, Asset>> enumerator, params DataObjectIdentifier<DataObjectKind.Asset, Guid>[] expectedIds)
        {
            int index = 0;
            foreach (var item in enumerator)
            {
                var id = item.Key;
                Assert.AreEqual(expectedIds[index++], id);
            }
        }

        private void CheckOrder(IEnumerable<DataObjectIdentifier<DataObjectKind.Asset, Guid>> enumerator, params DataObjectIdentifier<DataObjectKind.Asset, Guid>[] expectedIds)
        {
            int index = 0;
            foreach (var item in enumerator)
            {
                Assert.AreEqual(expectedIds[index++], item);
            }
        }

        private IMutableLqbCollection<DataObjectKind.Asset, Guid, Asset> InitializeCollection()
        {
            var loanId = DataObjectIdentifier<DataObjectKind.Asset, Guid>.Create(Guid.NewGuid());

            var name = Name.Create("Asset").Value;
            var collection = LqbCollection<DataObjectKind.Asset, Guid, Asset>.Create(name, new GuidDataObjectIdentifierFactory<DataObjectKind.Asset>());
            this.created0 = collection.Add(new Asset());
            this.created1 = collection.Add(new Asset());
            this.created2 = collection.Add(new Asset());
            this.created3 = collection.Add(new Asset());
            this.created4 = collection.Add(new Asset());

            return collection;
        }

        private IMutableOrderedLqbCollection<DataObjectKind.Asset, Guid, Asset> InitializeOrderedCollection(IMutableLqbCollection<DataObjectKind.Asset, Guid, Asset> collection)
        {
            var ordering = OrderedIdentifierCollection<DataObjectKind.Asset, Guid>.Create() as IMutableOrderedIdentifierCollection<DataObjectKind.Asset, Guid>;
            var initializer = (IInitializeCollectionOrder<Guid>)ordering;
            foreach (var id in collection)
            {
                initializer.Add(id.Key.Value);
            }

            var order = OrderedLqbCollection<DataObjectKind.Asset, Guid, Asset>.Create(collection, ordering);
            return order;
        }
    }
}
