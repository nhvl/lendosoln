﻿namespace LendingQB.Test.Developers.LendingQB.Core.Commands
{
    using global::DataAccess;
    using global::LendingQB.Core.Commands;
    using global::LendingQB.Test.Developers.Utils;
    using LqbGrammar.DataTypes;
    using NUnit.Framework;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    [TestFixture]
    [Category(CategoryConstants.IntegrationTest)]
    public class BorrowerManagementCommandTests
    {
        public static LqbGrammar.DataTypes.PathDispatch.DataPath LoanPath => LqbGrammar.DataTypes.PathDispatch.DataPath.Create("loan");

        [Test]
        public void SetPrimaryApplication_NoApplicationId_IsNotValid()
        {
            var command = BorrowerManagementCommand.CreateSetPrimaryApplication(LoanPath, null, null);

            Assert.Throws<CBaseException>(command.ValidateCommand);
        }

        [Test]
        public void SetPrimaryApplication_NoApplicationId_Throws()
        {
            var command = BorrowerManagementCommand.CreateSetPrimaryApplication(LoanPath, null, null);
            using (var loan = LoanForIntegrationTest.Create(TestAccountType.LoTest001, true))
            {
                Assert.Throws<InvalidOperationException>(() => command.Handle(loan.Identifier));
            }
        }

        [Test]
        public void SetPrimaryApplication_LegacyApplicationId_Works()
        {
            using (var loan = LoanForIntegrationTest.Create(TestAccountType.LoTest001, true))
            {
                var secondAppId = CPageData.AddBlankLegacyApplication(loan.Identifier);

                var loan2 = loan.CreateNewPageDataWithBypass();
                loan2.InitLoad();
                var primaryApps = loan2.LegacyApplications.Where(a => a.Value.IsPrimary);
                var nonPrimaryApps = loan2.LegacyApplications.Where(a => !(a.Value.IsPrimary));
                Assert.AreEqual(1, primaryApps.Count());
                Assert.AreEqual(1, nonPrimaryApps.Count());
                Assert.AreEqual(secondAppId, nonPrimaryApps.First().Key);

                var command = BorrowerManagementCommand.CreateSetPrimaryApplication(LoanPath, secondAppId.Value, null);

                command.Handle(loan.Identifier);

                var loan3 = loan.CreateNewPageDataWithBypass();
                loan3.InitLoad();
                Assert.AreEqual(secondAppId, loan3.LegacyApplications.Single(a => a.Value.IsPrimary).Key);
                Assert.AreEqual(1, loan2.LegacyApplications.Where(a => !(a.Value.IsPrimary)).Count());
                Assert.AreEqual(2, loan3.LegacyApplications.Count());
            }
        }

        [Test]
        public void SetPrimaryApplication_UladApplication_Works()
        {
            using (var loan = LoanForIntegrationTest.Create(TestAccountType.LoTest001, true))
            {
                var secondAppId = CPageData.AddBlankUladApplication(loan.Identifier).Key;

                var loan2 = loan.CreateNewPageDataWithBypass();
                loan2.InitLoad();
                var primaryApps = loan2.UladApplications.Where(a => a.Value.IsPrimary);
                var nonPrimaryApps = loan2.UladApplications.Where(a => !a.Value.IsPrimary);
                Assert.AreEqual(1, primaryApps.Count());
                Assert.AreEqual(1, nonPrimaryApps.Count());
                Assert.AreEqual(secondAppId, nonPrimaryApps.First().Key);

                var command = BorrowerManagementCommand.CreateSetPrimaryApplication(LoanPath, null, secondAppId.Value);

                command.Handle(loan.Identifier);

                var loan3 = loan.CreateNewPageDataWithBypass();
                loan3.InitLoad();
                Assert.AreEqual(secondAppId, loan3.UladApplications.Single(a => a.Value.IsPrimary).Key);
                Assert.AreEqual(1, loan2.UladApplications.Where(a => !a.Value.IsPrimary).Count());
                Assert.AreEqual(2, loan3.UladApplications.Count());
            }
        }

        [Test]
        public void AddBorrowerToApplication_NoId_Throws()
        {
            using (var loan = LoanForIntegrationTest.Create(TestAccountType.LoTest001, true))
            {
                var command = BorrowerManagementCommand.CreateAddBorrowerToApplication(LoanPath, null, null);

                Assert.Throws<InvalidOperationException>(() => command.Handle(loan.Identifier));
            }
        }

        [Test]
        public void AddBorrowerToApplication_LegacyApplicationNoCoborrower_Works()
        {
            using (var loan = LoanForIntegrationTest.Create(TestAccountType.LoTest001, true))
            {
                DataObjectIdentifier<DataObjectKind.Consumer, Guid> borrConsumerId;
                DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> appId;
                {
                    var loan1 = loan.CreateNewPageDataWithBypass();
                    loan1.InitLoad();
                    var borrower = loan1.LegacyApplicationConsumers.Single().Value;
                    borrConsumerId = borrower.ConsumerId;
                    appId = borrower.LegacyApplicationId;
                }

                var command = BorrowerManagementCommand.CreateAddBorrowerToApplication(LoanPath, appId.Value, null);

                Guid? coborrConsumerId = command.Handle(loan.Identifier);

                {
                    Assert.IsTrue(coborrConsumerId.HasValue);
                    Assert.AreNotEqual(borrConsumerId.Value, coborrConsumerId.Value);
                    var loan2 = loan.CreateNewPageDataWithBypass();
                    loan2.InitLoad();
                    Assert.AreEqual(2, loan2.LegacyApplicationConsumers.Count());
                    Assert.AreEqual(1, loan2.LegacyApplications.Count());
                    Assert.AreEqual(2, loan2.LegacyApplicationConsumers.Count(a => a.Value.LegacyApplicationId == appId));
                    var borrower = loan2.LegacyApplicationConsumers.Single(c => c.Value.ConsumerId == borrConsumerId).Value;
                    var coborrower = loan2.LegacyApplicationConsumers.Single(c => c.Value.ConsumerId.Value == coborrConsumerId.Value).Value;
                    Assert.AreEqual(appId, coborrower.LegacyApplicationId);
                    Assert.IsTrue(borrower.IsPrimary);
                    Assert.IsFalse(coborrower.IsPrimary);
                }
            }
        }

        [Test]
        public void AddBorrowerToApplication_LegacyApplicationWithCoborrower_Throws()
        {
            using (var loan = LoanForIntegrationTest.Create(TestAccountType.LoTest001, true))
            {
                DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> appId;
                {
                    var loan1 = loan.CreateNewPageDataWithBypass();
                    loan1.InitSave();
                    appId = loan1.LegacyApplications.Single().Key;
                    loan1.AddCoborrowerToLegacyApplication(appId);
                    loan1.Save();
                }

                var command = BorrowerManagementCommand.CreateAddBorrowerToApplication(LoanPath, appId.Value, null);

                Assert.Throws<CBaseException>(() => command.Handle(loan.Identifier));
            }
        }

        [Test]
        public void AddBorrowerToApplication_UladApplicationWithoutCoborrower_Works()
        {
            using (var loan = LoanForIntegrationTest.Create(TestAccountType.LoTest001, true))
            {
                HashSet<DataObjectIdentifier<DataObjectKind.Consumer, Guid>> existingConsumerIds = new HashSet<DataObjectIdentifier<DataObjectKind.Consumer, Guid>>();
                DataObjectIdentifier<DataObjectKind.UladApplication, Guid> appId;
                {
                    var loan1 = loan.CreateNewPageDataWithBypass();
                    loan1.InitLoad();
                    existingConsumerIds.UnionWith(loan1.UladApplicationConsumers.Select(c => c.Value.ConsumerId));
                    appId = loan1.UladApplications.Single().Key;
                }

                var command = BorrowerManagementCommand.CreateAddBorrowerToApplication(LoanPath, null, appId.Value);

                Guid? coborrConsumerId = command.Handle(loan.Identifier);

                {
                    Assert.IsTrue(coborrConsumerId.HasValue);
                    CollectionAssert.DoesNotContain(existingConsumerIds, coborrConsumerId);
                    var loan2 = loan.CreateNewPageDataWithBypass();
                    loan2.InitLoad();
                    Assert.AreEqual(2, loan2.UladApplicationConsumers.Count());
                    Assert.AreEqual(1, loan2.UladApplications.Count());
                    Assert.AreEqual(2, loan2.UladApplicationConsumers.Count(a => a.Value.UladApplicationId == appId));
                    CollectionAssert.IsSubsetOf(existingConsumerIds, loan2.UladApplicationConsumers.Select(c => c.Value.ConsumerId));
                    CollectionAssert.Contains(loan2.UladApplicationConsumers.Select(c => c.Value.ConsumerId.Value), coborrConsumerId.Value);
                    var coborrower = loan2.UladApplicationConsumers.Single(c => c.Value.ConsumerId.Value == coborrConsumerId.Value).Value;
                    Assert.AreEqual(appId, coborrower.UladApplicationId);
                    Assert.IsFalse(coborrower.IsPrimary);
                }
            }
        }

        [Test]
        public void AddBorrowerToApplication_UladApplicationWithMultipleApplications_Works()
        {
            using (var loan = LoanForIntegrationTest.Create(TestAccountType.LoTest001, true))
            {
                HashSet<DataObjectIdentifier<DataObjectKind.Consumer, Guid>> existingConsumerIds = new HashSet<DataObjectIdentifier<DataObjectKind.Consumer, Guid>>();
                var appId = CPageData.AddBlankUladApplication(loan.Identifier).Key;
                CPageData.AddConsumerToUladApplication(loan.Identifier, appId);
                {
                    var loan1 = loan.CreateNewPageDataWithBypass();
                    loan1.InitLoad();
                    Assert.AreEqual(3, loan1.UladApplicationConsumers.Count);
                    Assert.AreEqual(2, loan1.UladApplicationConsumers.Count(c => c.Value.UladApplicationId == appId));
                    existingConsumerIds.UnionWith(loan1.UladApplicationConsumers.Select(c => c.Value.ConsumerId));
                }


                Assert.AreEqual(3, existingConsumerIds.Count);
                var command = BorrowerManagementCommand.CreateAddBorrowerToApplication(LoanPath, null, appId.Value);

                Guid? coborrConsumerId = command.Handle(loan.Identifier);

                {
                    Assert.IsTrue(coborrConsumerId.HasValue);
                    CollectionAssert.DoesNotContain(existingConsumerIds, coborrConsumerId);
                    var loan2 = loan.CreateNewPageDataWithBypass();
                    loan2.InitLoad();
                    Assert.AreEqual(4, loan2.UladApplicationConsumers.Count());
                    Assert.AreEqual(2, loan2.UladApplications.Count());
                    Assert.AreEqual(3, loan2.UladApplicationConsumers.Count(a => a.Value.UladApplicationId == appId));
                    CollectionAssert.IsSubsetOf(existingConsumerIds, loan2.UladApplicationConsumers.Select(c => c.Value.ConsumerId));
                    CollectionAssert.Contains(loan2.UladApplicationConsumers.Select(c => c.Value.ConsumerId.Value), coborrConsumerId.Value);
                    CollectionAssert.AreEquivalent(
                        existingConsumerIds.Concat(new[] { DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(coborrConsumerId.Value) }),
                        loan2.UladApplicationConsumers.Select(c => c.Value.ConsumerId));
                    var coborrower = loan2.UladApplicationConsumers.Single(c => c.Value.ConsumerId.Value == coborrConsumerId.Value).Value;
                    Assert.AreEqual(appId, coborrower.UladApplicationId);
                    Assert.IsFalse(coborrower.IsPrimary);
                }
            }
        }

        [Test]
        public void AddApplication_LegacyApplication_Works()
        {
            using (var loan = LoanForIntegrationTest.Create(TestAccountType.LoTest001, true))
            {
                DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> existingAppId;
                {
                    var loan1 = loan.CreateNewPageDataWithBypass();
                    loan1.InitLoad();
                    existingAppId = loan1.LegacyApplications.Single().Key;
                }

                var command = BorrowerManagementCommand.CreateAddApplication(LoanPath, isLegacyApplication: true);

                Guid? newAppId = command.Handle(loan.Identifier);

                {
                    Assert.IsTrue(newAppId.HasValue);
                    Assert.AreNotEqual(existingAppId.Value, newAppId.Value);
                    var loan2 = loan.CreateNewPageDataWithBypass();
                    loan2.InitLoad();
                    Assert.AreEqual(2, loan2.LegacyApplications.Count());
                    var newApp = loan2.LegacyApplications.Single(l => l.Key.Value == newAppId.Value);
                    Assert.AreEqual(newAppId.Value, newApp.Key.Value);
                    Assert.IsFalse(newApp.Value.IsPrimary);
                }
            }
        }

        [Test]
        public void AddApplication_UladApplication_Works()
        {
            using (var loan = LoanForIntegrationTest.Create(TestAccountType.LoTest001, true))
            {
                DataObjectIdentifier<DataObjectKind.UladApplication, Guid> existingAppId;
                {
                    var loan1 = loan.CreateNewPageDataWithBypass();
                    loan1.InitLoad();
                    existingAppId = loan1.UladApplications.Single().Key;
                }

                var command = BorrowerManagementCommand.CreateAddApplication(LoanPath, isLegacyApplication: false);

                Guid? newAppId = command.Handle(loan.Identifier);

                {
                    Assert.IsTrue(newAppId.HasValue);
                    Assert.AreNotEqual(existingAppId.Value, newAppId.Value);
                    var loan2 = loan.CreateNewPageDataWithBypass();
                    loan2.InitLoad();
                    Assert.AreEqual(2, loan2.UladApplications.Count());
                    var newApp = loan2.UladApplications.Single(l => l.Key.Value == newAppId.Value);
                    Assert.AreEqual(newAppId.Value, newApp.Key.Value);
                    Assert.IsFalse(newApp.Value.IsPrimary);
                }
            }
        }

        [Test]
        public void DeleteApplication_OnlyLegacyApplication_Throws()
        {
            using (var loan = LoanForIntegrationTest.Create(TestAccountType.LoTest001, true))
            {
                DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> appToDelete;
                {
                    var loan1 = loan.CreateNewPageDataWithBypass();
                    loan1.InitLoad();
                    appToDelete = loan1.LegacyApplications.Single().Key;
                }

                var command = BorrowerManagementCommand.CreateDeleteApplication(LoanPath, appToDelete.Value, null);
                Assert.Throws<CBaseException>(() => command.Handle(loan.Identifier));
            }
        }

        [Test]
        public void DeleteApplication_OnlyUladApplication_Throws()
        {
            using (var loan = LoanForIntegrationTest.Create(TestAccountType.LoTest001, true))
            {
                DataObjectIdentifier<DataObjectKind.UladApplication, Guid> appToDelete;
                {
                    var loan1 = loan.CreateNewPageDataWithBypass();
                    loan1.InitLoad();
                    appToDelete = loan1.UladApplications.Single().Key;
                }

                var command = BorrowerManagementCommand.CreateDeleteApplication(LoanPath, null, appToDelete.Value);
                Assert.Throws<CBaseException>(() => command.Handle(loan.Identifier));
            }
        }

        [Test]
        public void DeleteApplication_SecondLegacyApplication_Works()
        {
            using (var loan = LoanForIntegrationTest.Create(TestAccountType.LoTest001, true))
            {
                CPageData.AddBlankLegacyApplication(loan.Identifier);
                var loan1 = loan.CreateNewPageDataWithBypass();
                loan1.InitLoad();
                var appToKeep = loan1.LegacyApplications.First().Key;
                var secondApplication = loan1.LegacyApplications.Skip(1).Single();
                var appToDelete = secondApplication.Key;
                Assert.IsFalse(secondApplication.Value.IsPrimary);

                var command = BorrowerManagementCommand.CreateDeleteApplication(LoanPath, appToDelete.Value, null);
                command.Handle(loan.Identifier);

                var loan2 = loan.CreateNewPageDataWithBypass();
                loan2.InitLoad();
                Assert.AreEqual(1, loan2.LegacyApplications.Count());
                Assert.AreEqual(appToKeep, loan2.LegacyApplications.Single().Key);
            }
        }

        [Test]
        public void DeleteApplication_SecondUladApplication_Works()
        {
            using (var loan = LoanForIntegrationTest.Create(TestAccountType.LoTest001, true))
            {
                var appToDelete = CPageData.AddBlankUladApplication(loan.Identifier).Key;

                var loan1 = loan.CreateNewPageDataWithBypass();
                loan1.InitLoad();
                var appToKeep = loan1.UladApplications.First().Key;
                Assert.AreNotEqual(appToKeep, appToDelete);
                Assert.AreEqual(2, loan1.UladApplications.Count);
                Assert.IsFalse(loan1.UladApplications[appToDelete].IsPrimary);

                var command = BorrowerManagementCommand.CreateDeleteApplication(LoanPath, null, appToDelete.Value);
                command.Handle(loan.Identifier);

                {
                    var loan2 = loan.CreateNewPageDataWithBypass();
                    loan2.InitLoad();
                    Assert.AreEqual(1, loan2.UladApplications.Count());
                    Assert.AreEqual(appToKeep, loan2.UladApplications.Single().Key);
                }
            }
        }

        [Test]
        public void DeleteApplication_PrimaryLegacyApplicationWithSecond_Works()
        {
            using (var loan = LoanForIntegrationTest.Create(TestAccountType.LoTest001, true))
            {
                var appToKeep = CPageData.AddBlankLegacyApplication(loan.Identifier);

                var loan1 = loan.CreateNewPageDataWithBypass();
                loan1.InitLoad();
                var appToDelete = loan1.LegacyApplications.Single(a => a.Value.IsPrimary).Key;
                Assert.AreNotEqual(appToKeep, appToDelete);
                Assert.AreEqual(2, loan1.LegacyApplications.Count);
                Assert.IsFalse(loan1.LegacyApplications[appToKeep].IsPrimary);

                var command = BorrowerManagementCommand.CreateDeleteApplication(LoanPath, appToDelete.Value, null);
                command.Handle(loan.Identifier);

                var loan2 = loan.CreateNewPageDataWithBypass();
                loan2.InitLoad();
                Assert.AreEqual(1, loan2.LegacyApplications.Count);
                Assert.AreEqual(appToKeep, loan2.LegacyApplications.Single().Key);
                Assert.IsTrue(loan2.LegacyApplications.Single().Value.IsPrimary);
            }
        }

        [Test]
        public void DeleteApplication_PrimaryUladApplicationWithSecond_Works()
        {
            using (var loan = LoanForIntegrationTest.Create(TestAccountType.LoTest001, true))
            {
                var appToKeep = CPageData.AddBlankUladApplication(loan.Identifier).Key;

                var loan1 = loan.CreateNewPageDataWithBypass();
                loan1.InitLoad();
                var appToDelete = loan1.UladApplications.Single(a => a.Value.IsPrimary).Key;
                Assert.AreNotEqual(appToKeep, appToDelete);
                Assert.AreEqual(2, loan1.UladApplications.Count);
                Assert.IsFalse(loan1.UladApplications[appToKeep].IsPrimary);

                var command = BorrowerManagementCommand.CreateDeleteApplication(LoanPath, null, appToDelete.Value);
                command.Handle(loan.Identifier);

                {
                    var loan2 = loan.CreateNewPageDataWithBypass();
                    loan2.InitLoad();
                    Assert.AreEqual(1, loan2.UladApplications.Count());
                    Assert.AreEqual(appToKeep, loan2.UladApplications.Single().Key);
                    Assert.IsTrue(loan2.UladApplications.Single().Value.IsPrimary);
                }
            }
        }

        [Test]
        public void DeleteBorrower_OnlyBorrowerOnUladApplication_Throws()
        {
            using (var loan = LoanForIntegrationTest.Create(TestAccountType.LoTest001, true))
            {
                DataObjectIdentifier<DataObjectKind.Consumer, Guid> borrowerToDelete;
                {
                    var loan1 = loan.CreateNewPageDataWithBypass();
                    loan1.InitLoad();
                    Assert.AreEqual(1, loan1.UladApplicationConsumers.Count);
                    borrowerToDelete = loan1.UladApplicationConsumers.Single().Value.ConsumerId;
                }

                var command = BorrowerManagementCommand.CreateDeleteBorrower(LoanPath, borrowerToDelete.Value);
                Assert.Throws<CBaseException>(() => command.Handle(loan.Identifier));
            }
        }

        [Test]
        public void DeleteBorrower_OnlyBorrowerOnPrimaryUladApplicationWithSecondApp_ReplacesPrimaryApp()
        {
            using (var loan = LoanForIntegrationTest.Create(TestAccountType.LoTest001, true))
            {
                CPageData.AddBlankUladApplication(loan.Identifier);
                DataObjectIdentifier<DataObjectKind.Consumer, Guid> borrowerToDelete;
                {
                    var loan1 = loan.CreateNewPageDataWithBypass();
                    loan1.InitLoad();
                    Assert.AreEqual(2, loan1.UladApplications.Count);
                    Assert.AreEqual(2, loan1.UladApplicationConsumers.Count);
                    var borrower = loan1.UladApplicationConsumers.First().Value;
                    borrowerToDelete = borrower.ConsumerId;
                    Assert.AreEqual(1, loan1.UladApplicationConsumers.Count(c => c.Value.UladApplicationId == borrower.UladApplicationId));
                    Assert.IsTrue(loan1.UladApplications[borrower.UladApplicationId].IsPrimary);
                }

                var command = BorrowerManagementCommand.CreateDeleteBorrower(LoanPath, borrowerToDelete.Value);
                command.Handle(loan.Identifier);

                {
                    var loan2 = loan.CreateNewPageDataWithBypass();
                    loan2.InitLoad();
                    Assert.AreEqual(1, loan2.UladApplications.Count);
                    Assert.AreEqual(1, loan2.UladApplicationConsumers.Count);
                    Assert.AreNotEqual(borrowerToDelete, loan2.UladApplicationConsumers.Single().Value.ConsumerId);
                    Assert.IsTrue(loan2.UladApplications.Single().Value.IsPrimary);
                }
            }
        }

        [Test]
        public void DeleteBorrower_TwoBorrowersOnUladApplication_DeletesBorrower()
        {
            using (var loan = LoanForIntegrationTest.Create(TestAccountType.LoTest001, true))
            {
                var loan1 = loan.CreateNewPageDataWithBypass();
                loan1.InitLoad();
                var borrowerToKeep = CPageData.AddConsumerToUladApplication(loan.Identifier, loan1.UladApplications.First().Key);

                loan1 = loan.CreateNewPageDataWithBypass();
                loan1.InitLoad();
                Assert.AreEqual(2, loan1.UladApplicationConsumers.Count);
                var borrowerToDelete = loan1.UladApplicationConsumers.Single(b => b.Value.IsPrimary).Value.ConsumerId;

                var command = BorrowerManagementCommand.CreateDeleteBorrower(LoanPath, borrowerToDelete.Value);
                command.Handle(loan.Identifier);

                var loan2 = loan.CreateNewPageDataWithBypass();
                loan2.InitLoad();
                Assert.AreEqual(1, loan2.UladApplicationConsumers.Count());
                Assert.AreEqual(borrowerToKeep, loan2.UladApplicationConsumers.Single().Value.ConsumerId);
                Assert.IsTrue(loan2.UladApplicationConsumers.Single().Value.IsPrimary);
            }
        }

        [Test]
        public void DeleteCoborrower_WithCoborrower_CoborrowerIsGone()
        {
            using (var testLoan = LoanForIntegrationTest.Create(TestAccountType.LoTest001, true))
            {
                var loan = testLoan.CreateNewPageDataWithBypass();
                loan.InitSave();
                var legacyAppId = loan.LegacyApplications.First().Key;
                var coborrConsumerId = loan.AddCoborrowerToLegacyApplication(legacyAppId);
                loan.Save();

                loan = testLoan.CreateNewPageDataWithBypass();
                loan.InitLoad();
                Assert.AreEqual(1, loan.LegacyApplications.Count);
                Assert.AreEqual(2, loan.LegacyApplicationConsumers.Count);
                Assert.AreEqual(2, loan.LegacyApplicationConsumers.Count(c => c.Value.LegacyApplicationId == legacyAppId));
                var borrConsumerId = loan.LegacyApplicationConsumers.Single(c => c.Value.IsPrimary).Value.ConsumerId;
                Assert.AreNotEqual(borrConsumerId, coborrConsumerId);

                var command = BorrowerManagementCommand.CreateDeleteCoBorrower(LoanPath, legacyAppId.Value);
                command.Handle(testLoan.Identifier);

                loan = testLoan.CreateNewPageDataWithBypass();
                loan.InitLoad();
                Assert.AreEqual(1, loan.LegacyApplicationConsumers.Count);
                Assert.AreEqual(borrConsumerId, loan.LegacyApplicationConsumers.Single().Value.ConsumerId);
            }
        }

        [Test]
        public void SetPrimaryBorrower_NonPrimaryConsumerId_BecomesPrimary()
        {
            using (var loan = LoanForIntegrationTest.Create(TestAccountType.LoTest001, true))
            {
                var loan1 = loan.CreateNewPageDataWithBypass();
                loan1.InitLoad();
                var uladAppId = loan1.UladApplications.First().Key;
                CPageData.AddConsumerToUladApplication(loan.Identifier, uladAppId);
                var consumerId = CPageData.AddConsumerToUladApplication(loan.Identifier, uladAppId);

                loan1 = loan.CreateNewPageDataWithBypass();
                loan1.InitLoad();
                Assert.AreEqual(1, loan1.UladApplications.Count);
                uladAppId = loan1.UladApplications.Single().Key;
                Assert.AreEqual(3, loan1.UladApplicationConsumers.Count(c => c.Value.UladApplicationId == uladAppId));
                Assert.AreEqual(1, loan1.UladApplicationConsumers.Count(c => c.Value.UladApplicationId == uladAppId && (c.Value.IsPrimary)));
                Assert.AreNotEqual(consumerId, loan1.UladApplicationConsumers.Single(c => c.Value.UladApplicationId == uladAppId && (c.Value.IsPrimary)).Value.ConsumerId);

                var command = BorrowerManagementCommand.CreateSetPrimaryBorrower(LoanPath, consumerId.Value);
                command.Handle(loan.Identifier);

                    var loan2 = loan.CreateNewPageDataWithBypass();
                    loan2.InitLoad();
                    Assert.AreEqual(1, loan2.UladApplications.Count);
                    Assert.AreEqual(3, loan2.UladApplicationConsumers.Count(c => c.Value.UladApplicationId == uladAppId));
                    Assert.AreEqual(1, loan2.UladApplicationConsumers.Count(c => c.Value.UladApplicationId == uladAppId && (c.Value.IsPrimary)));
                    Assert.AreEqual(consumerId, loan2.UladApplicationConsumers.Single(c => c.Value.UladApplicationId == uladAppId && (c.Value.IsPrimary)).Value.ConsumerId);
            }
        }

        [Test]
        public void SwapBorrowers_NoCoborrower_SwapsBorrowerToCoborrower()
        {
            using (var loan = LoanForIntegrationTest.Create(TestAccountType.LoTest001, true))
            {
                DataObjectIdentifier<DataObjectKind.Consumer, Guid> borrowerConsumerId;
                DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> appId;
                {
                    var loan1 = loan.CreateNewPageDataWithBypass();
                    loan1.InitLoad();
                    Assert.AreEqual(1, loan1.LegacyApplications.Count);
                    appId = loan1.LegacyApplications.Single().Key;
                    Assert.AreEqual(1, loan1.LegacyApplicationConsumers.Count);
                    borrowerConsumerId = loan1.LegacyApplicationConsumers.Single().Value.ConsumerId;
                }

                var command = BorrowerManagementCommand.CreateSwapBorrowers(LoanPath, appId.Value);
                command.Handle(loan.Identifier);

                {
                    var loan2 = loan.CreateNewPageDataWithBypass();
                    loan2.InitLoad();
                    Assert.AreEqual(1, loan2.LegacyApplications.Count);
                    Assert.AreEqual(2, loan2.LegacyApplicationConsumers.Count);
                    Assert.AreEqual(borrowerConsumerId, loan2.LegacyApplicationConsumers.Skip(1).Single().Value.ConsumerId);
                    Assert.IsFalse(loan2.LegacyApplicationConsumers.Single(c => c.Value.ConsumerId == borrowerConsumerId).Value.IsPrimary);
                }
            }
        }
    }
}
