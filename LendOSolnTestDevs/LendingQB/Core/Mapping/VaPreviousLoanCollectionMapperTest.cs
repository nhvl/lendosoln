﻿namespace LendingQB.Test.Developers.LendingQB.Core.Mapping
{
    using System;
    using System.Data.Common;
    using DataAccess;
    using LendersOffice.Security;
    using NUnit.Framework;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;
    using LendersOffice.Drivers.SqlServerDB;
    using Utils;
    using global::LendingQB.Core.Mapping;
    using global::LendingQB.Core;
    using global::LendingQB.Core.Data;
    using System.Data;
    using global::DataAccess;

    [TestFixture]
    [Category(CategoryConstants.IntegrationTest)]
    public class VaPreviousLoanCollectionMapperTest
    {
        private AbstractUserPrincipal principal;
        private DataObjectIdentifier<DataObjectKind.Loan, Guid> loanId;
        private DataObjectIdentifier<DataObjectKind.ClientCompany, Guid> brokerId;
        private IStoredProcedureDriver storedProcedureDriver;
        private IDbConnection connection;
        private IDbTransaction transaction;

        [TestFixtureSetUp]
        public void FixtureSetUp()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                this.principal = LoginTools.LoginAs(TestAccountType.LoTest001);
                this.brokerId = DataObjectIdentifier<DataObjectKind.ClientCompany, Guid>.Create(principal.BrokerId);
                var creator = CLoanFileCreator.GetCreator(
                    this.principal,
                    LendersOffice.Audit.E_LoanCreationSource.UserCreateFromBlank);
                this.loanId = DataObjectIdentifier<DataObjectKind.Loan, Guid>.Create(creator.CreateBlankLoanFile());

                var factory = new StoredProcedureDriverFactory();
                this.storedProcedureDriver = factory.Create(TimeoutInSeconds.Default);
            }
        }

        [TestFixtureTearDown]
        public void FixtureTearDown()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                Tools.DeclareLoanFileInvalid(
                    this.principal,
                    this.loanId.Value,
                    sendNotifications: false,
                    generateLinkLoanMsgEvent: false);
            }
        }

        [SetUp]
        public void SetUp()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                this.connection = DbConnectionInfo.GetConnectionInfo(this.brokerId.Value).GetConnection();
                this.connection.Open();
                this.transaction = connection.BeginTransaction();
            }
        }

        [TearDown]
        public void TearDown()
        {
            this.transaction.Rollback();
            this.transaction.Dispose();
            this.connection.Dispose();
        }

        [Test]
        public void Load_AssumingNoDatabaseIssues_Succeeds()
        {
            var mapper = new VaPreviousLoanCollectionMapper(
                this.loanId,
                this.brokerId,
                this.storedProcedureDriver);

            this.LoadCollection(mapper);
        }

        [Test]
        public void Save_NewRecord_PersistsEnteredDataPoints()
        {
            var mapper = new VaPreviousLoanCollectionMapper(
                this.loanId,
                this.brokerId,
                this.storedProcedureDriver);
            IMutableLqbCollection<DataObjectKind.VaPreviousLoan, Guid, VaPreviousLoan> collection = LqbCollection<DataObjectKind.VaPreviousLoan, Guid, VaPreviousLoan>.Create(
                Name.Create("Test").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.VaPreviousLoan>());
            VaPreviousLoan originalRecord = GetDefaultTestRecord();

            collection.BeginTrackingChanges();
            var id = collection.Add(originalRecord);
            this.SaveCollection(mapper, collection);
            collection = this.LoadCollection(mapper);
            var loadedRecord = collection[id];

            Assert.AreEqual(originalRecord.City, loadedRecord.City);
            Assert.AreEqual(originalRecord.CityState, loadedRecord.CityState);
            Assert.AreEqual(originalRecord.DateOfLoan, loadedRecord.DateOfLoan);
            Assert.AreEqual(originalRecord.IsStillOwned, loadedRecord.IsStillOwned);
            Assert.AreEqual(originalRecord.LoanTypeDesc, loadedRecord.LoanTypeDesc);
            Assert.AreEqual(originalRecord.State, loadedRecord.State);
            Assert.AreEqual(originalRecord.Zip, loadedRecord.Zip);
            Assert.AreEqual(originalRecord.LoanDate, loadedRecord.LoanDate);
            Assert.AreEqual(originalRecord.PropertySoldDate, loadedRecord.PropertySoldDate);
            Assert.AreEqual(originalRecord.StreetAddress, loadedRecord.StreetAddress);
            Assert.AreEqual(originalRecord.VaLoanNumber, loadedRecord.VaLoanNumber);
        }

        [Test]
        public void Save_EmptyRecord_PersistsEmptyRecord()
        {
            var mapper = new VaPreviousLoanCollectionMapper(
                this.loanId,
                this.brokerId,
                this.storedProcedureDriver);
            IMutableLqbCollection<DataObjectKind.VaPreviousLoan, Guid, VaPreviousLoan> collection = LqbCollection<DataObjectKind.VaPreviousLoan, Guid, VaPreviousLoan>.Create(
                Name.Create("Test").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.VaPreviousLoan>());
            var originalRecord = new VaPreviousLoan();

            collection.BeginTrackingChanges();
            var id = collection.Add(originalRecord);
            this.SaveCollection(mapper, collection);
            collection = this.LoadCollection(mapper);
            var loadedRecord = collection[id];

            Assert.AreEqual(null, loadedRecord.City);
            Assert.AreEqual(null, loadedRecord.CityState);
            Assert.AreEqual(null, loadedRecord.DateOfLoan);
            Assert.AreEqual(null, loadedRecord.IsStillOwned);
            Assert.AreEqual(null, loadedRecord.LoanTypeDesc);
            Assert.AreEqual(null, loadedRecord.State);
            Assert.AreEqual(null, loadedRecord.Zip);
            Assert.AreEqual(null, loadedRecord.LoanDate);
            Assert.AreEqual(null, loadedRecord.PropertySoldDate);
            Assert.AreEqual(null, loadedRecord.StreetAddress);
            Assert.AreEqual(null, loadedRecord.VaLoanNumber);
        }

        [Test]
        public void Save_UpdateExistingRecord_PersistsUpdatedDataPoints()
        {
            var mapper = new VaPreviousLoanCollectionMapper(
                this.loanId,
                this.brokerId,
                this.storedProcedureDriver);
            IMutableLqbCollection<DataObjectKind.VaPreviousLoan, Guid, VaPreviousLoan> collection = LqbCollection<DataObjectKind.VaPreviousLoan, Guid, VaPreviousLoan>.Create(
                Name.Create("Test").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.VaPreviousLoan>());
            var originalRecord = GetDefaultTestRecord();

            collection.BeginTrackingChanges();
            var id = collection.Add(originalRecord);
            this.SaveCollection(mapper, collection);

            collection = this.LoadCollection(mapper);
            collection.BeginTrackingChanges();
            var loadedRecord = collection[id];
            loadedRecord.City = City.Create("Fountain Valley").Value;
            loadedRecord.CityState = CityState.Create("FV, CA").Value;
            loadedRecord.DateOfLoan = DescriptionField.Create("2/2/17").Value;
            loadedRecord.IsStillOwned = BooleanKleene.Indeterminant;
            loadedRecord.LoanTypeDesc = DescriptionField.Create("Updated Description").Value;
            loadedRecord.State = UnitedStatesPostalState.Create("AZ").Value;
            loadedRecord.Zip = Zipcode.Create("92708").Value;
            loadedRecord.LoanDate = UnzonedDate.Create(DateTime.Parse("3/3/17")).Value;
            loadedRecord.PropertySoldDate = UnzonedDate.Create(DateTime.Parse("4/4/17")).Value;
            loadedRecord.StreetAddress = StreetAddress.Create("123 Talbert Ave").Value;
            loadedRecord.VaLoanNumber = VaLoanNumber.Create("54321").Value;

            this.SaveCollection(mapper, collection);

            collection = this.LoadCollection(mapper);
            var updatedRecord = collection[id];

            Assert.AreEqual(City.Create("Fountain Valley"), updatedRecord.City);
            Assert.AreEqual(CityState.Create("FV, CA"), updatedRecord.CityState);
            Assert.AreEqual(DescriptionField.Create("2/2/17"), updatedRecord.DateOfLoan);
            Assert.AreEqual(BooleanKleene.Indeterminant, updatedRecord.IsStillOwned);
            Assert.AreEqual(DescriptionField.Create("Updated Description"), updatedRecord.LoanTypeDesc);
            Assert.AreEqual(UnitedStatesPostalState.Create("AZ"), updatedRecord.State);
            Assert.AreEqual(Zipcode.Create("92708"), updatedRecord.Zip);
            Assert.AreEqual(UnzonedDate.Create(DateTime.Parse("3/3/17")), updatedRecord.LoanDate);
            Assert.AreEqual(UnzonedDate.Create(DateTime.Parse("4/4/17")), updatedRecord.PropertySoldDate);
            Assert.AreEqual(StreetAddress.Create("123 Talbert Ave"), updatedRecord.StreetAddress);
            Assert.AreEqual(VaLoanNumber.Create("54321"), updatedRecord.VaLoanNumber);
        }

        [Test]
        public void Save_DeleteExistingRecord_RemovesRecordFromDatabase()
        {
            var mapper = new VaPreviousLoanCollectionMapper(
                this.loanId,
                this.brokerId,
                this.storedProcedureDriver);
            IMutableLqbCollection<DataObjectKind.VaPreviousLoan, Guid, VaPreviousLoan> collection = LqbCollection<DataObjectKind.VaPreviousLoan, Guid, VaPreviousLoan>.Create(
                Name.Create("Test").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.VaPreviousLoan>());
            var record = new VaPreviousLoan();

            collection.BeginTrackingChanges();
            var id = collection.Add(record);
            this.SaveCollection(mapper, collection);

            collection = this.LoadCollection(mapper);
            collection.BeginTrackingChanges();
            collection.Remove(id);
            this.SaveCollection(mapper, collection);

            collection = this.LoadCollection(mapper);

            Assert.AreEqual(false, collection.ContainsKey(id));
        }

        public static VaPreviousLoan GetDefaultTestRecord()
        {
            var record = new VaPreviousLoan();
            record.City = City.Create("Costa Mesa").Value;
            record.CityState = CityState.Create("Costa Mesa, CA").Value;
            record.DateOfLoan = DescriptionField.Create("01/01/18").Value;
            record.IsStillOwned = BooleanKleene.True;
            record.LoanTypeDesc = DescriptionField.Create("LoanTypeDesc").Value;
            record.State = UnitedStatesPostalState.Create("CA").Value;
            record.Zip = Zipcode.Create("92626").Value;
            record.LoanDate = UnzonedDate.Create(DateTime.Parse("02/02/18")).Value;
            record.PropertySoldDate = UnzonedDate.Create(DateTime.Parse("03/03/18")).Value;
            record.StreetAddress = StreetAddress.Create("3560 Hyland").Value;
            record.VaLoanNumber = VaLoanNumber.Create("12345").Value;
            return record;
        }

        private IMutableLqbCollection<DataObjectKind.VaPreviousLoan, Guid, VaPreviousLoan> LoadCollection(
            ILqbCollectionSqlMapper<DataObjectKind.VaPreviousLoan, Guid, VaPreviousLoan> mapper)
        {
            return Load(mapper, this.connection, this.transaction);
        }

        internal static IMutableLqbCollection<DataObjectKind.VaPreviousLoan, Guid, VaPreviousLoan> Load(
            ILqbCollectionSqlMapper<DataObjectKind.VaPreviousLoan, Guid, VaPreviousLoan> mapper,
            IDbConnection connection, IDbTransaction transaction)
        {
            return mapper.Load(
                connection,
                transaction,
                Name.Create("Test").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.VaPreviousLoan>(),
                null,
                new DataReaderConverter());
        }

        private void SaveCollection(
            ILqbCollectionSqlMapper<DataObjectKind.VaPreviousLoan, Guid, VaPreviousLoan> mapper,
            IMutableLqbCollection<DataObjectKind.VaPreviousLoan, Guid, VaPreviousLoan> collection)
        {
            Save(mapper, collection, this.connection, this.transaction);
        }

        internal static void Save(
            ILqbCollectionSqlMapper<DataObjectKind.VaPreviousLoan, Guid, VaPreviousLoan> mapper,
            IMutableLqbCollection<DataObjectKind.VaPreviousLoan, Guid, VaPreviousLoan> collection,
            IDbConnection connection, IDbTransaction transaction)
        {
            mapper.Save(
                connection,
                transaction,
                new SqlParameterConverter(),
                collection);
        }
    }
}
