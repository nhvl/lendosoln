﻿namespace LendingQB.Test.Developers.LendingQB.Core.Mapping
{
    using System;
    using System.Data;
    using System.Data.Common;
    using DataAccess;
    using LendersOffice.Drivers.SqlServerDB;
    using LendersOffice.Security;
    using global::LendingQB.Core;
    using global::LendingQB.Core.Data;
    using global::LendingQB.Core.Mapping;
    using LqbGrammar;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;
    using NUnit.Framework;
    using Utils;
    using global::DataAccess;

    [TestFixture]
    [Category(CategoryConstants.IntegrationTest)]
    public sealed class RealPropertyCollectionMapperTest
    {
        private const string VoeVerifierId = "DBB06C9D-74C6-4C1D-90E0-9A14C34CF213";

        private DbConnection connection;
        private IDbTransaction transaction;
        private IStoredProcedureDriver storedProcedureDriver;

        private Guid loanId;
        private AbstractUserPrincipal principal;

        [TestFixtureSetUp]
        public void FixtureSetup()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                this.CreateLoan();

                var factory = new StoredProcedureDriverFactory();
                this.storedProcedureDriver = factory.Create(TimeoutInSeconds.Default);
            }
        }

        [TestFixtureTearDown]
        public void FixtureTearDown()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                this.InactiveLoanFile(this.loanId, this.principal);
            }
        }

        [SetUp]
        public void MethodSetUp()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                this.connection = DbConnectionInfo.GetConnectionInfo(this.principal.BrokerId).GetConnection();
                this.connection.Open();
                this.transaction = connection.BeginTransaction();
            }
        }

        [TearDown]
        public void MethodTearDown()
        {
            this.transaction.Rollback();
            this.transaction.Dispose();
            this.connection.Close();
        }

        [Test]
        public void Save_Empty()
        {
            var entity = new RealProperty();
            this.CreateAndLoad(entity);
        }

        [Test]
        public void Save_Populated()
        {
            var entity = GetDefaultTestRecord();
            this.CreateAndLoad(entity);
        }

        [Test]
        public void Update()
        {
            var loanIdentifier = DataObjectIdentifier<DataObjectKind.Loan, Guid>.Create(this.loanId);
            var brokerIdentifier = DataObjectIdentifier<DataObjectKind.ClientCompany, Guid>.Create(this.principal.BrokerId);

            var original = GetDefaultTestRecord();
            IMutableLqbCollection<DataObjectKind.RealProperty, Guid, RealProperty> collection = LqbCollection<DataObjectKind.RealProperty, Guid, RealProperty>.Create(
                Name.Create("Test").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.RealProperty>());
            collection.BeginTrackingChanges();
            var entityId = collection.Add(original);
            Assert.AreNotEqual(Guid.Empty, entityId.Value);

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.Json);

                // save data
                var mapper = new RealPropertyCollectionMapper(
                    loanIdentifier,
                    brokerIdentifier,
                    this.storedProcedureDriver);
                this.SaveCollection(mapper, collection);

                // load data
                collection = this.LoadCollection(mapper);
                var updateEntity = collection[entityId];

                // update data
                collection.BeginTrackingChanges();
                updateEntity.StreetAddress = StreetAddress.Create("666 Hellispont Dr");

                var empty = new RealProperty();
                var emptyId = collection.Add(empty);

                // save updates
                this.SaveCollection(mapper, collection);

                // re-load
                collection = this.LoadCollection(mapper);
                var loadedEntity = collection[entityId];
                var emptyEntity = collection[emptyId];

                // check field values
                this.CompareValues(updateEntity, loadedEntity);
                this.CompareValues(empty, emptyEntity);
            }
        }

        [Test]
        public void Delete()
        {
            var loanIdentifier = DataObjectIdentifier<DataObjectKind.Loan, Guid>.Create(this.loanId);
            var brokerIdentifier = DataObjectIdentifier<DataObjectKind.ClientCompany, Guid>.Create(this.principal.BrokerId);

            IMutableLqbCollection<DataObjectKind.RealProperty, Guid, RealProperty> collection = LqbCollection<DataObjectKind.RealProperty, Guid, RealProperty>.Create(
                Name.Create("Test").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.RealProperty>());
            collection.BeginTrackingChanges();

            var original = GetDefaultTestRecord();
            var entityId = collection.Add(original);

            var empty = new RealProperty();
            var emptyId = collection.Add(empty);

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.Json);

                // save data
                var mapper = new RealPropertyCollectionMapper(
                    loanIdentifier,
                    brokerIdentifier,
                    this.storedProcedureDriver);
                this.SaveCollection(mapper, collection);

                // load data
                collection = this.LoadCollection(mapper);
                var loadedEntity = collection[entityId];
                var loadedEmpty = collection[emptyId];

                // delete data
                collection.BeginTrackingChanges();
                collection.Remove(emptyId);

                // save updates
                this.SaveCollection(mapper, collection);

                // re-load
                collection = this.LoadCollection(mapper);
                Assert.AreEqual(1, collection.Count);
            }
        }

        private void CreateAndLoad(RealProperty original)
        {
            var loanIdentifier = DataObjectIdentifier<DataObjectKind.Loan, Guid>.Create(this.loanId);
            var brokerIdentifier = DataObjectIdentifier<DataObjectKind.ClientCompany, Guid>.Create(this.principal.BrokerId);

            IMutableLqbCollection<DataObjectKind.RealProperty, Guid, RealProperty> collection = LqbCollection<DataObjectKind.RealProperty, Guid, RealProperty>.Create(
                Name.Create("Test").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.RealProperty>());
            collection.BeginTrackingChanges();
            var entityId = collection.Add(original);
            Assert.AreNotEqual(Guid.Empty, entityId.Value);

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.Json);

                // save data
                var mapper = new RealPropertyCollectionMapper(
                    loanIdentifier,
                    brokerIdentifier,
                    this.storedProcedureDriver);
                this.SaveCollection(mapper, collection);

                // load data
                collection = this.LoadCollection(mapper);
                var loadedEntity = collection[entityId];

                // check field values
                this.CompareValues(original, loadedEntity);
            }
        }

        private void CompareValues(RealProperty original, RealProperty loadedEntity)
        {
            Assert.IsTrue(loadedEntity.City == original.City);
            Assert.IsTrue(loadedEntity.GrossRentInc == original.GrossRentInc);
            Assert.IsTrue(loadedEntity.HExp == original.HExp);
            Assert.IsTrue(loadedEntity.IsEmptyCreated == original.IsEmptyCreated);
            Assert.IsTrue(loadedEntity.IsPrimaryResidence == original.IsPrimaryResidence);
            Assert.IsTrue(loadedEntity.IsSubjectProp == original.IsSubjectProp);
            Assert.IsTrue(loadedEntity.MarketValue == original.MarketValue);
            Assert.IsTrue(loadedEntity.MtgAmt == original.MtgAmt);
            Assert.IsTrue(loadedEntity.MtgPmt == original.MtgPmt);
            Assert.IsTrue(loadedEntity.NetRentInc == original.NetRentInc);
            Assert.IsTrue(loadedEntity.NetRentIncLocked == original.NetRentIncLocked);
            Assert.IsTrue(loadedEntity.OccR == original.OccR);
            Assert.IsTrue(loadedEntity.State == original.State);
            Assert.IsTrue(loadedEntity.Status == original.Status);
            Assert.IsTrue(loadedEntity.StreetAddress == original.StreetAddress);
            Assert.IsTrue(loadedEntity.Type == original.Type);
            Assert.IsTrue(loadedEntity.Zip == original.Zip);
            Assert.IsTrue(loadedEntity.IsForceCalcNetRentalInc == original.IsForceCalcNetRentalInc);
        }

        internal static RealProperty GetDefaultTestRecord()
        {
            var record = new RealProperty();
            record.City = City.Create("Costa Mesa");
            record.GrossRentInc = Money.Create(2500m);
            record.HExp = Money.Create(1200m);
            record.IsEmptyCreated = false;
            record.IsPrimaryResidence = true;
            record.IsSubjectProp = true;
            record.MarketValue = Money.Create(2500000m);
            record.MtgAmt = Money.Create(3000000m);
            record.MtgPmt = Money.Create(10000m);
            record.NetRentIncData = Money.Create(12000m);
            record.NetRentIncLocked = true;
            record.OccR = Percentage.Create(25m);
            record.State = UnitedStatesPostalState.Create("CA");
            record.Status = E_ReoStatusT.Residence;
            record.StreetAddress = StreetAddress.Create("3215 Main Street");
            record.Type = E_ReoTypeT.Farm;
            record.Zip = Zipcode.Create("92626");
            record.IsForceCalcNetRentalInc = true;

            return record;
        }

        private IMutableLqbCollection<DataObjectKind.RealProperty, Guid, RealProperty> LoadCollection(
            ILqbCollectionSqlMapper<DataObjectKind.RealProperty, Guid, RealProperty> mapper)
        {
            return RealPropertyCollectionMapperTest.LoadCollection(mapper, this.connection, this.transaction);
        }

        private void SaveCollection(
            ILqbCollectionSqlMapper<DataObjectKind.RealProperty, Guid, RealProperty> mapper,
            IMutableLqbCollection<DataObjectKind.RealProperty, Guid, RealProperty> collection)
        {
            RealPropertyCollectionMapperTest.SaveCollection(mapper, collection, this.connection, this.transaction);
        }

        internal static IMutableLqbCollection<DataObjectKind.RealProperty, Guid, RealProperty> LoadCollection(
            ILqbCollectionSqlMapper<DataObjectKind.RealProperty, Guid, RealProperty> mapper,
            DbConnection connection, IDbTransaction transaction)
        {
            return mapper.Load(
                connection,
                transaction,
                Name.Create("Test").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.RealProperty>(),
                null,
                new DataReaderConverter());
        }

        internal static void SaveCollection(
            ILqbCollectionSqlMapper<DataObjectKind.RealProperty, Guid, RealProperty> mapper,
            IMutableLqbCollection<DataObjectKind.RealProperty, Guid, RealProperty> collection,
            DbConnection connection, IDbTransaction transaction)
        {
            mapper.Save(
                connection,
                transaction,
                new SqlParameterConverter(),
                collection);
        }

        // Create a new test loan and return <brokerid, loanid>
        private void CreateLoan()
        {
            this.principal = LoginTools.LoginAs(TestAccountType.LoTest001);

            var creator = CLoanFileCreator.GetCreator(principal, LendersOffice.Audit.E_LoanCreationSource.UserCreateFromBlank);
            this.loanId = creator.CreateBlankLoanFile();
        }

        private void InactiveLoanFile(Guid loanId, AbstractUserPrincipal principal)
        {
            Tools.DeclareLoanFileInvalid(
                principal,
                loanId,
                sendNotifications: false,
                generateLinkLoanMsgEvent: false);
        }
    }
}
