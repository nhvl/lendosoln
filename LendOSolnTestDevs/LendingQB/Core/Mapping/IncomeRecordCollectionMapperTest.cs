﻿namespace LendingQB.Test.Developers.LendingQB.Core.Mapping
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using global::LendingQB.Core;
    using global::LendingQB.Core.Data;
    using global::LendingQB.Core.Mapping;
    using global::LendingQB.Test.Developers.FOOL;
    using global::LendingQB.Test.Developers.Utils;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Queries;
    using NSubstitute;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public class IncomeRecordCollectionMapperTest
    {
        [Test]
        public void Create_AfterToJsonOnIncomeRecordCollection_RoundTripsAsExpected()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterMockDriverFactory(this.GetFakeStageConfig());

                var collection = OrderedLqbCollection<DataObjectKind.IncomeRecord, Guid, IncomeRecord>.Create(
                    Name.Create("Test").Value,
                    new GuidDataObjectIdentifierFactory<DataObjectKind.IncomeRecord>());
                var income1Id = collection.Add(new IncomeRecord()
                {
                    IncomeType = IncomeType.AccessoryUnitIncome,
                    MonthlyAmount = Money.Create(5)
                });
                var income2Id = collection.Add(new IncomeRecord()
                {
                    IncomeType = IncomeType.CapitalGains,
                    MonthlyAmount = Money.Create(10)
                });

                var json = IncomeRecordCollectionMapper.ToJson(collection);
                var mapper = new IncomeRecordCollectionMapper();
                var deserializedCollection = mapper.Create(json);

                Assert.AreEqual(2, deserializedCollection.Count);
                Assert.IsTrue(deserializedCollection.ContainsKey(income1Id));
                var income1 = deserializedCollection[income1Id];
                Assert.AreEqual(IncomeType.AccessoryUnitIncome, income1.IncomeType);
                Assert.AreEqual(Money.Create(5), income1.MonthlyAmount);
                var income2 = deserializedCollection[income2Id];
                Assert.AreEqual(IncomeType.CapitalGains, income2.IncomeType);
                Assert.AreEqual(Money.Create(10), income2.MonthlyAmount);
            }
        }

        [Test]
        public void Create_AfterToJsonOnReOrderedIncomeRecordCollection_MaintainsOrder()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterMockDriverFactory(this.GetFakeStageConfig());

                var collection = OrderedLqbCollection<DataObjectKind.IncomeRecord, Guid, IncomeRecord>.Create(
                    Name.Create("Test").Value,
                    new GuidDataObjectIdentifierFactory<DataObjectKind.IncomeRecord>());
                var income1Id = collection.Add(new IncomeRecord()
                {
                    IncomeType = IncomeType.AccessoryUnitIncome,
                    MonthlyAmount = Money.Create(5)
                });
                var income2Id = collection.Add(new IncomeRecord()
                {
                    IncomeType = IncomeType.CapitalGains,
                    MonthlyAmount = Money.Create(10)
                });
                collection.OrderingInterface.MoveForward(income2Id);

                var json = IncomeRecordCollectionMapper.ToJson(collection);
                var mapper = new IncomeRecordCollectionMapper();
                var deserializedCollection = mapper.Create(json);

                Assert.AreEqual(2, deserializedCollection.Count);
                CollectionAssert.AreEqual(new[] { income2Id, income1Id }, collection.Keys);
            }
        }

        [Test]
        public void Create_AfterToJsonOnEmptyCollection_RoundTripsAsExpected()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterMockDriverFactory(this.GetFakeStageConfig());

                var collection = OrderedLqbCollection<DataObjectKind.IncomeRecord, Guid, IncomeRecord>.Create(
                    Name.Create("Test").Value,
                    new GuidDataObjectIdentifierFactory<DataObjectKind.IncomeRecord>());

                var json = IncomeRecordCollectionMapper.ToJson(collection);
                var mapper = new IncomeRecordCollectionMapper();
                var deserializedCollection = mapper.Create(json);

                Assert.AreEqual(0, deserializedCollection.Count);
            }
        }

        [Test]
        public void ToJson_NullCollection_Throws()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterMockDriverFactory(this.GetFakeStageConfig());

                Assert.Throws<ArgumentNullException>(() => IncomeRecordCollectionMapper.ToJson(null));
            }
        }

        private IConfigurationQueryFactory GetFakeStageConfig()
        {
            var fakeConfigFactory = new FakeConfigurationQueryFactory();
            fakeConfigFactory.StageConfig.Add("UseJsonNetHtmlEscaping", Tuple.Create(0, ""));
            return fakeConfigFactory;
        }
    }
}
