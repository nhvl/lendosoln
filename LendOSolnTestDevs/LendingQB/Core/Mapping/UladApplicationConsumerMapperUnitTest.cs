﻿namespace LendingQB.Test.Developers.LendingQB.Core.Mapping
{
    using System;
    using System.Data;
    using global::DataAccess;
    using global::LendingQB.Core.Data;
    using global::LendingQB.Core.Mapping;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;
    using NSubstitute;
    using NUnit.Framework;
    using Data;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public sealed class UladApplicationConsumerMapperUnitTest
    {
        [Test]
        public void AddPrimaryConsumerAndRemovePreviousPrimary_ThenSave_ShouldSucceed()
        {
            var appId = UladApplicationConsumerUnitTest.CreateUladIdentifier();
            var borrowerId = UladApplicationConsumerUnitTest.CreateConsumerIdentifier();
            var legAppId = UladApplicationConsumerUnitTest.CreateLegacyAppIdentifier();
            var association1 = UladApplicationConsumerUnitTest.CreateAssociation(appId, borrowerId, legAppId);
            association1.IsPrimary = true;

            var assocSet = UladApplicationConsumerUnitTest.CreateAssociationSet();
            assocSet.BeginTrackingChanges();
            assocSet.Add(association1);

            var connection = Substitute.For<IDbConnection>();
            var transaction = Substitute.For<IDbTransaction>();
            var pramConverter = new SqlParameterConverter();
            var mapper = CreateMapper();
            mapper.Save(connection, transaction, pramConverter, assocSet);

            var coborrowerId = UladApplicationConsumerUnitTest.CreateConsumerIdentifier();
            var association2 = UladApplicationConsumerUnitTest.CreateAssociation(appId, coborrowerId, legAppId);
            association1.IsPrimary = false;
            association2.IsPrimary = true;
            assocSet.Add(association2);

            mapper.Save(connection, transaction, pramConverter, assocSet);
        }

        [Test]
        public void SaveApplicationWithMultiplePrimaryConsumers_ShouldFail()
        {
            var appId = UladApplicationConsumerUnitTest.CreateUladIdentifier();
            var borrowerId = UladApplicationConsumerUnitTest.CreateConsumerIdentifier();
            var coborrowerId = UladApplicationConsumerUnitTest.CreateConsumerIdentifier();
            var legAppId = UladApplicationConsumerUnitTest.CreateLegacyAppIdentifier();

            var association1 = UladApplicationConsumerUnitTest.CreateAssociation(appId, borrowerId, legAppId);
            association1.IsPrimary = true;

            var association2 = UladApplicationConsumerUnitTest.CreateAssociation(appId, coborrowerId, legAppId);
            association2.IsPrimary = true;

            var assocSet = UladApplicationConsumerUnitTest.CreateAssociationSet();
            assocSet.BeginTrackingChanges();
            assocSet.Add(association1);
            assocSet.Add(association2);

            var connection = Substitute.For<IDbConnection>();
            var transaction = Substitute.For<IDbTransaction>();
            var pramConverter = new SqlParameterConverter();
            var mapper = CreateMapper();
            Assert.Throws<CBaseException>(() => mapper.Save(connection, transaction, pramConverter, assocSet));
        }

        [Test]
        public void SaveApplicationWithNoPrimaryConsumers_ShouldFail()
        {
            var appId = UladApplicationConsumerUnitTest.CreateUladIdentifier();
            var borrowerId = UladApplicationConsumerUnitTest.CreateConsumerIdentifier();
            var coborrowerId = UladApplicationConsumerUnitTest.CreateConsumerIdentifier();
            var legAppId = UladApplicationConsumerUnitTest.CreateLegacyAppIdentifier();

            var association1 = UladApplicationConsumerUnitTest.CreateAssociation(appId, borrowerId, legAppId);
            association1.IsPrimary = false;

            var association2 = UladApplicationConsumerUnitTest.CreateAssociation(appId, coborrowerId, legAppId);
            association2.IsPrimary = false;

            var assocSet = UladApplicationConsumerUnitTest.CreateAssociationSet();
            assocSet.BeginTrackingChanges();
            assocSet.Add(association1);
            assocSet.Add(association2);

            var connection = Substitute.For<IDbConnection>();
            var transaction = Substitute.For<IDbTransaction>();
            var pramConverter = new SqlParameterConverter();
            var mapper = CreateMapper();
            Assert.Throws<CBaseException>(() => mapper.Save(connection, transaction, pramConverter, assocSet));
        }

        [Test]
        public void SaveTwoApplicationsWithMultipleConsumersEach_CorrectPrimaryConsumers_ShouldSucceed()
        {
            var appId1 = UladApplicationConsumerUnitTest.CreateUladIdentifier();
            var appId2 = UladApplicationConsumerUnitTest.CreateUladIdentifier();
            var borrowerId1 = UladApplicationConsumerUnitTest.CreateConsumerIdentifier();
            var borrowerId2 = UladApplicationConsumerUnitTest.CreateConsumerIdentifier();
            var borrowerId3 = UladApplicationConsumerUnitTest.CreateConsumerIdentifier();
            var borrowerId4 = UladApplicationConsumerUnitTest.CreateConsumerIdentifier();
            var legAppId = UladApplicationConsumerUnitTest.CreateLegacyAppIdentifier();

            var association1 = UladApplicationConsumerUnitTest.CreateAssociation(appId1, borrowerId1, legAppId);
            var association2 = UladApplicationConsumerUnitTest.CreateAssociation(appId1, borrowerId2, legAppId);
            var association3 = UladApplicationConsumerUnitTest.CreateAssociation(appId2, borrowerId3, legAppId);
            var association4 = UladApplicationConsumerUnitTest.CreateAssociation(appId2, borrowerId4, legAppId);
            association1.IsPrimary = true;
            association3.IsPrimary = true;

            var assocSet = UladApplicationConsumerUnitTest.CreateAssociationSet();
            assocSet.BeginTrackingChanges();
            assocSet.Add(association1);
            assocSet.Add(association2);
            assocSet.Add(association3);
            assocSet.Add(association4);

            var connection = Substitute.For<IDbConnection>();
            var transaction = Substitute.For<IDbTransaction>();
            var pramConverter = new SqlParameterConverter();
            var mapper = CreateMapper();
            mapper.Save(connection, transaction, pramConverter, assocSet);
        }

        private static UladApplicationConsumerMapper CreateMapper()
        {
            var loanId = DataObjectIdentifier<DataObjectKind.Loan, Guid>.Create(Guid.NewGuid());
            var driver = Substitute.For<IStoredProcedureDriver>();

            return new UladApplicationConsumerMapper(loanId, driver);
        }
    }
}