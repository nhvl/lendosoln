﻿namespace LendingQB.Test.Developers.LendingQB.Core.Mapping
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using global::DataAccess;
    using global::LendingQB.Core;
    using global::LendingQB.Core.Data;
    using global::LendingQB.Core.Mapping;
    using global::LendingQB.Test.Developers.FOOL;
    using global::LendingQB.Test.Developers.Utils;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;
    using LqbGrammar.Queries;
    using NSubstitute;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public class EmploymentRecordCollectionMapperUnitTest
    {
        private static readonly DataObjectIdentifier<DataObjectKind.Consumer, Guid> ConsumerId = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(Guid.Empty);
        private static readonly DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> AppId = DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>.Create(Guid.Empty);
        private static readonly DataObjectIdentifier<DataObjectKind.EmploymentRecord, Guid> EmploymentId1 = DataObjectIdentifier<DataObjectKind.EmploymentRecord, Guid>.Create(new Guid("11111111-1111-1111-1111-111111111111"));
        private static readonly DataObjectIdentifier<DataObjectKind.EmploymentRecord, Guid> EmploymentId2 = DataObjectIdentifier<DataObjectKind.EmploymentRecord, Guid>.Create(new Guid("22222222-2222-2222-2222-222222222222"));

        [Test]
        public void Save_Empty_NoException()
        {
            var mapper = this.GetMapper();
            var collection = this.GetCollection();

            mapper.Save(
                NSubstitute.Substitute.For<IDbConnection>(),
                NSubstitute.Substitute.For<IDbTransaction>(),
                NSubstitute.Substitute.For<ISqlParameterConverter>(),
                collection);
        }

        [Test]
        public void Save_MultiplePrimaryForSingleConsumer_ThrowsException()
        {
            var mapper = this.GetMapper();
            var collection = this.GetCollection();

            var primary1 = this.GetNewEmploymentRecord();
            primary1.IsPrimary = true;
            primary1.SetConsumer(ConsumerId, AppId);
            collection.Add(primary1);
            var primary2 = this.GetNewEmploymentRecord();
            primary2.IsPrimary = true;
            primary2.SetConsumer(ConsumerId, AppId);
            collection.Add(primary2);

            Assert.Throws<CBaseException>(() => mapper.Save(
                NSubstitute.Substitute.For<IDbConnection>(),
                NSubstitute.Substitute.For<IDbTransaction>(),
                NSubstitute.Substitute.For<ISqlParameterConverter>(),
                collection));
        }

        [Test]
        public void Save_MultipleEmploymentsSinglePrimaryForSingleConsumer_Succeeds()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterMockDriverFactory(this.GetFakeStageConfig());

                var mapper = this.GetMapper();
                var collection = this.GetCollection();

                var primary = this.GetNewEmploymentRecord();
                primary.IsPrimary = true;
                primary.SetConsumer(ConsumerId, AppId);
                collection.Add(primary);
                var secondary = this.GetNewEmploymentRecord();
                secondary.IsPrimary = false;
                secondary.SetConsumer(ConsumerId, AppId);
                collection.Add(secondary);

                mapper.Save(
                    NSubstitute.Substitute.For<IDbConnection>(),
                    NSubstitute.Substitute.For<IDbTransaction>(),
                    NSubstitute.Substitute.For<ISqlParameterConverter>(),
                    collection);
            }
        }

        [Test]
        public void Save_MultipleEmploymentsNoPrimaryForSingleConsumer_Succeeds()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterMockDriverFactory(this.GetFakeStageConfig());

                var mapper = this.GetMapper();
                var collection = this.GetCollection();

                var entity1 = this.GetNewEmploymentRecord();
                entity1.IsPrimary = false;
                entity1.SetConsumer(ConsumerId, AppId);
                collection.Add(entity1);
                var entity2 = this.GetNewEmploymentRecord();
                entity2.IsPrimary = false;
                entity2.SetConsumer(ConsumerId, AppId);
                collection.Add(entity2);

                mapper.Save(
                    NSubstitute.Substitute.For<IDbConnection>(),
                    NSubstitute.Substitute.For<IDbTransaction>(),
                    NSubstitute.Substitute.For<ISqlParameterConverter>(),
                    collection);
            }
        }

        private IConfigurationQueryFactory GetFakeStageConfig()
        {
            var fakeValues = new Dictionary<string, Tuple<int, string>>()
            {
                ["UseJsonNetHtmlEscaping"] = Tuple.Create(0, "")
            };
            var fakeStageConfig = new FakeStageConfigurationQuery(fakeValues);
            var fakeConfigFactory = new FakeConfigurationQueryFactory(Substitute.For<ISiteConfigurationQuery>(), fakeStageConfig);
            return fakeConfigFactory;
        }

        private EmploymentRecord GetNewEmploymentRecord()
        {
            return new EmploymentRecord(Substitute.For<IEmploymentRecordDefaultsProvider>());
        }

        private EmploymentRecordCollectionMapper GetMapper()
        {
            return new EmploymentRecordCollectionMapper(
                DataObjectIdentifier<DataObjectKind.Loan, Guid>.Create(Guid.Empty),
                DataObjectIdentifier<DataObjectKind.ClientCompany, Guid>.Create(Guid.Empty),
                Substitute.For<IEmploymentRecordDefaultsProvider>(),
                new EmploymentRecordEmploymentRecordVoeDataMapper(),
                new IncomeRecordCollectionMapper(),
                NSubstitute.Substitute.For<IStoredProcedureDriver>());
        }

        private IMutableLqbCollection<DataObjectKind.EmploymentRecord, Guid, EmploymentRecord> GetCollection()
        {
            var collection = LqbCollection<DataObjectKind.EmploymentRecord, Guid, EmploymentRecord>.Create(
                Name.Create("EmploymentRecords").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.EmploymentRecord>(),
                null);

            collection.BeginTrackingChanges();

            return collection;
        }
    }
}
