﻿namespace LendingQB.Test.Developers.LendingQB.Core.Mapping
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using global::DataAccess;
    using global::LendingQB.Core;
    using global::LendingQB.Core.Data;
    using global::LendingQB.Core.Mapping;
    using LendersOffice.Common;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;
    using NSubstitute;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public class HousingHistoryEntryCollectionMapperUnitTest
    {
        [Test]
        public void EnsurePresentAddressesDoNotHaveEndDate_SinglePresentAddressNoEndDate_ReturnsTrue()
        {
            var collection = this.GetCollection(
                new HousingHistoryEntry
                {
                    IsPresentAddress = true,
                });
            var mapper = this.GetMapper();

            bool valid = mapper.EnsurePresentAddressesDoNotHaveEndDate(collection);

            Assert.IsTrue(valid);
        }

        [Test]
        public void EnsurePresentAddressesDoNotHaveEndDate_SinglePresentAddressWithEndDate_ReturnsFalse()
        {
            var collection = this.GetCollection(
                new[]
                {
                    new HousingHistoryEntry
                    {
                        IsPresentAddress = true,
                        EndDate = ApproximateDate.CreateWithYearPrecision(2017)
                    },
                    new HousingHistoryEntry
                    {
                        IsPresentAddress = false,
                        EndDate = ApproximateDate.CreateWithYearPrecision(2017)
                    },
                    new HousingHistoryEntry
                    {
                        IsPresentAddress = true,
                    }
                });
            var mapper = this.GetMapper();

            bool valid = mapper.EnsurePresentAddressesDoNotHaveEndDate(collection);

            Assert.IsFalse(valid);
        }

        [Test]
        public void EnsurePresentAddressesDoNotHaveEndDate_NoPresentAddress_ReturnsTrue()
        {
            var collection = this.GetCollection(new HousingHistoryEntry());
            var mapper = this.GetMapper();

            bool valid = mapper.EnsurePresentAddressesDoNotHaveEndDate(collection);

            Assert.IsTrue(valid);
        }

        [Test]
        public void EnsurePresentAddressesDoNotHaveEndDate_EmptyCollection_ReturnsTrue()
        {
            var collection = this.GetCollection(new HousingHistoryEntry[] { });
            var mapper = this.GetMapper();

            bool valid = mapper.EnsurePresentAddressesDoNotHaveEndDate(collection);

            Assert.IsTrue(valid);
        }

        [Test]
        public void Save_SinglePresentAddressPerConsumer_DoesNotThrowException()
        {
            var legacyAppId = Guid.Empty.ToIdentifier<DataObjectKind.LegacyApplication>();
            var entry1 = new HousingHistoryEntry { IsPresentAddress = true };
            entry1.SetConsumer(Guid.NewGuid().ToIdentifier<DataObjectKind.Consumer>(), legacyAppId);
            var entry2 = new HousingHistoryEntry { IsPresentAddress = true };
            entry2.SetConsumer(Guid.NewGuid().ToIdentifier<DataObjectKind.Consumer>(), legacyAppId);
            var collection = this.GetCollection(new[] { entry1, entry2 });
            var mapper = this.GetMapper();

            mapper.Save(Substitute.For<IDbConnection>(), null, new SqlParameterConverter(), collection);
        }

        [Test]
        public void Save_MultiplePresentAddressesForSingleConsumer_ThrowsException()
        {
            var consumerId = Guid.NewGuid().ToIdentifier<DataObjectKind.Consumer>();
            var legacyAppId = Guid.Empty.ToIdentifier<DataObjectKind.LegacyApplication>();
            var present1 = new HousingHistoryEntry { IsPresentAddress = true };
            present1.SetConsumer(consumerId, legacyAppId);
            var present2 = new HousingHistoryEntry { IsPresentAddress = true };
            present2.SetConsumer(consumerId, legacyAppId);
            var collection = this.GetCollection(new[] { present1, present2 });
            var mapper = this.GetMapper();

            Assert.Throws<CBaseException>(() => mapper.Save(null, null, null, collection));
        }

        [Test]
        public void Save_NoPresentAddresses_DoesNotThrowException()
        {
            var consumerId = Guid.NewGuid().ToIdentifier<DataObjectKind.Consumer>();
            var legacyAppId = Guid.Empty.ToIdentifier<DataObjectKind.LegacyApplication>();
            var entry1 = new HousingHistoryEntry();
            entry1.SetConsumer(consumerId, legacyAppId);
            var entry2 = new HousingHistoryEntry();
            entry2.SetConsumer(consumerId, legacyAppId);
            var collection = this.GetCollection(new[] { entry1, entry2 });
            var mapper = this.GetMapper();

            mapper.Save(Substitute.For<IDbConnection>(), null, new SqlParameterConverter(), collection);
        }

        private HousingHistoryEntryCollectionMapper GetMapper()
        {
            return new HousingHistoryEntryCollectionMapper(
                Guid.Empty.ToIdentifier<DataObjectKind.Loan>(),
                Guid.Empty.ToIdentifier<DataObjectKind.ClientCompany>(),
                new PostalAddressMapper(),
                Substitute.For<IStoredProcedureDriver>());
        }

        private IMutableLqbCollection<DataObjectKind.HousingHistoryEntry, Guid, HousingHistoryEntry> GetCollection(HousingHistoryEntry entry)
        {
            return this.GetCollection(new[] { entry });
        }

        private IMutableLqbCollection<DataObjectKind.HousingHistoryEntry, Guid, HousingHistoryEntry> GetCollection(IEnumerable<HousingHistoryEntry> entries)
        {
            var name = Name.Create("Test").Value;
            var idFactory = new GuidDataObjectIdentifierFactory<DataObjectKind.HousingHistoryEntry>();
            var collection = LqbCollection<DataObjectKind.HousingHistoryEntry, Guid, HousingHistoryEntry>.Create(name, idFactory);

            foreach (var entry in entries)
            {
                collection.Add(entry);
            }

            collection.BeginTrackingChanges();

            return collection;
        }
    }

}
