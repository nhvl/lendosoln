﻿namespace LendingQB.Test.Developers.LendingQB.Core.Mapping
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using global::LendingQB.Core;
    using global::LendingQB.Core.Data;
    using LqbGrammar.DataTypes;
    using LqbGrammar.DataTypes.PathDispatch;
    using NUnit.Framework;

    [TestFixture]
    public class SortedLqbCollectionTest
    {
        private DataObjectIdentifier<DataObjectKind.HousingHistoryEntry, Guid> id1;
        private DataObjectIdentifier<DataObjectKind.HousingHistoryEntry, Guid> id2;
        private DataObjectIdentifier<DataObjectKind.HousingHistoryEntry, Guid> id3;
        private DataObjectIdentifier<DataObjectKind.HousingHistoryEntry, Guid> id4;
        private DataObjectIdentifier<DataObjectKind.HousingHistoryEntry, Guid> id5;
        private HousingHistoryEntry entry1;
        private HousingHistoryEntry entry2;
        private HousingHistoryEntry entry3;
        private HousingHistoryEntry entry4;
        private HousingHistoryEntry entry5;
        private IEnumerable<KeyValuePair<DataObjectIdentifier<DataObjectKind.HousingHistoryEntry, Guid>, HousingHistoryEntry>> expectedOrder;


        [Test]
        public void Keys_Always_SortsAsExpected()
        {
            var baseCollection = this.GetBaseCollection();
            var sortedCollection = this.GetSortedCollection(baseCollection);

            var keys = sortedCollection.Keys;

            var expected = this.expectedOrder.Select(kvp => kvp.Key);
            CollectionAssert.AreEqual(expected, keys);
        }

        [Test]
        public void GetAllKeys_Always_SortsAsExpected()
        {
            var baseCollection = this.GetBaseCollection();
            var sortedCollection = this.GetSortedCollection(baseCollection);

            var pathableKeys = sortedCollection.GetAllKeys();

            var expected = this.expectedOrder.Select(kvp => new DataPathSelectionElement(new SelectIdExpression(kvp.Key.Value.ToString())));
            CollectionAssert.AreEqual(expected, pathableKeys);
        }

        [Test]
        public void Values_Always_SortsAsExpected()
        {
            var baseCollection = this.GetBaseCollection();
            var sortedCollection = this.GetSortedCollection(baseCollection);

            var values = sortedCollection.Values;

            var expected = this.expectedOrder.Select(kvp => kvp.Value);
            CollectionAssert.AreEqual(expected, values);
        }

        [Test]
        public void Enumerator_Always_SortsAsExpected()
        {
            var baseCollection = this.GetBaseCollection();
            var sortedCollection = this.GetSortedCollection(baseCollection);

            var kvps = sortedCollection.Select(kvp => kvp);

            CollectionAssert.AreEqual(this.expectedOrder, kvps);
        }

        [Test]
        public void Add_WhileEnumerating_DoesNotAffectEnumerator()
        {
            var baseCollection = this.GetBaseCollection();
            var sortedCollection = this.GetSortedCollection(baseCollection);
            int i = 0;

            foreach (var entry in sortedCollection)
            {
                sortedCollection.Add(new HousingHistoryEntry());
                i++;
            }

            Assert.AreEqual(5, i);
        }

        [Test]
        public void Remove_WhileEnumerating_DoesNotAffectEnumerator()
        {
            var baseCollection = this.GetBaseCollection();
            var sortedCollection = this.GetSortedCollection(baseCollection);
            int i = 0;

            foreach (var entry in sortedCollection)
            {
                sortedCollection.Remove(entry.Key);
                i++;
            }

            Assert.AreEqual(5, i);
        }

        [Test]
        public void UpdateRecord_WhileEnumerating_DoesNotAffectEnumerator()
        {
            var baseCollection = this.GetBaseCollection();
            var sortedCollection = this.GetSortedCollection(baseCollection);
            var keys = new List<DataObjectIdentifier<DataObjectKind.HousingHistoryEntry, Guid>>();

            foreach (var entry in sortedCollection)
            {
                // This will push back entry 2 to the end of the list.
                if (entry.Key == this.id2)
                {
                    entry.Value.IsPresentAddress = false;
                }
                else if (entry.Key == this.id3)
                {
                    entry.Value.IsPresentAddress = true;
                }

                keys.Add(entry.Key);
            }

            var expected = this.expectedOrder.Select(kvp => kvp.Key);
            CollectionAssert.AreEqual(expected, keys);

            // Now the order should be different.
            var newExpected = new[] { this.id3, this.id5, this.id4, this.id1, this.id2 };
            CollectionAssert.AreEqual(newExpected, sortedCollection.Keys);
        }

        private IMutableLqbCollection<DataObjectKind.HousingHistoryEntry, Guid, HousingHistoryEntry> GetBaseCollection()
        {
            var name = Name.Create("Test").Value;
            var idFactory = new GuidDataObjectIdentifierFactory<DataObjectKind.HousingHistoryEntry>();
            this.id1 = idFactory.NewId();
            this.entry1 = new HousingHistoryEntry
            {
                IsPresentAddress = false,
                EndDate = ApproximateDate.CreateWithMonthPrecision(2016, 6)
            };
            this.id2 = idFactory.NewId();
            this.entry2 = new HousingHistoryEntry
            {
                IsPresentAddress = true,
            };
            this.id3 = idFactory.NewId();
            this.entry3 = new HousingHistoryEntry
            {
                IsPresentAddress = false,
            };
            this.id4 = idFactory.NewId();
            this.entry4 = new HousingHistoryEntry
            {
                IsPresentAddress = false,
                EndDate = ApproximateDate.CreateWithDayPrecision(2017, 1, 1)
            };
            this.id5 = idFactory.NewId();
            this.entry5 = new HousingHistoryEntry
            {
                IsPresentAddress = false,
                EndDate = ApproximateDate.CreateWithYearPrecision(2018)
            };

            var data = new Dictionary<DataObjectIdentifier<DataObjectKind.HousingHistoryEntry, Guid>, HousingHistoryEntry>()
            {
                [this.id1] = this.entry1,
                [this.id2] = this.entry2,
                [this.id3] = this.entry3,
                [this.id4] = this.entry4,
                [this.id5] = this.entry5,
            };

            var expectedKeyOrder = new[] { this.id2, this.id5, this.id4, this.id1, this.id3 };
            var expectedValueOrder = new[] { this.entry2, this.entry5, this.entry4, this.entry1, this.entry3 };
            this.expectedOrder = expectedKeyOrder.Zip(
                expectedValueOrder,
                (key, value) => new KeyValuePair<DataObjectIdentifier<DataObjectKind.HousingHistoryEntry, Guid>, HousingHistoryEntry>(key, value));

            return LqbCollection<DataObjectKind.HousingHistoryEntry, Guid, HousingHistoryEntry>.Create(
                name,
                idFactory,
                null,
                data);
        }

        private IMutableLqbCollection<DataObjectKind.HousingHistoryEntry, Guid, HousingHistoryEntry> GetSortedCollection(
            IMutableLqbCollection<DataObjectKind.HousingHistoryEntry, Guid, HousingHistoryEntry> baseCollection)
        {
            return SortedLqbCollection<DataObjectKind.HousingHistoryEntry, Guid, HousingHistoryEntry>.Create(
                baseCollection,
                new HousingHistoryEntryComparer());
        }
    }
}
