﻿namespace LendingQB.Test.Developers.LendingQB.Core.Mapping
{
    using System;
    using System.Data;
    using global::DataAccess;
    using global::LendingQB.Core;
    using global::LendingQB.Core.Data;
    using global::LendingQB.Core.Mapping;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;
    using NSubstitute;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public sealed class UladApplicationCollectionMapperUnitTest
    {
        [Test]
        public void CreateOnePrimaryApplicationAndSave_ShouldSucceed()
        {
            var uladApp = UladApplicationCollectionMapperTest.GetDefaultTestRecord();
            uladApp.IsPrimary = true;

            var collection = CreateCollection();
            collection.BeginTrackingChanges();
            collection.Add(uladApp);

            var connection = Substitute.For<IDbConnection>();
            var transaction = Substitute.For<IDbTransaction>();

            var mapper = CreateMapper();
            mapper.Save(connection, transaction, new SqlParameterConverter(), collection);
        }

        [Test]
        public void CreateOneNotPrimaryApplicationAndSave_ShouldFail()
        {
            var uladApp = UladApplicationCollectionMapperTest.GetDefaultTestRecord();
            uladApp.IsPrimary = false;

            var collection = CreateCollection();
            collection.BeginTrackingChanges();
            collection.Add(uladApp);

            var connection = Substitute.For<IDbConnection>();
            var transaction = Substitute.For<IDbTransaction>();

            var mapper = CreateMapper();
            Assert.Throws<CBaseException>(() => mapper.Save(connection, transaction, new SqlParameterConverter(), collection));
        }

        [Test]
        public void CreateTwoApplicationsWithOnePrimaryAndSave_ShouldSucceed()
        {
            var uladApp1 = UladApplicationCollectionMapperTest.GetDefaultTestRecord();
            uladApp1.IsPrimary = true;

            var uladApp2 = UladApplicationCollectionMapperTest.GetDefaultTestRecord();
            uladApp2.IsPrimary = false;

            var collection = CreateCollection();
            collection.BeginTrackingChanges();
            collection.Add(uladApp1);
            collection.Add(uladApp2);

            var connection = Substitute.For<IDbConnection>();
            var transaction = Substitute.For<IDbTransaction>();

            var mapper = CreateMapper();
            mapper.Save(connection, transaction, new SqlParameterConverter(), collection);
        }

        [Test]
        public void CreateTwoApplicationsWithoutPrimaryAndSave_ShouldFail()
        {
            var uladApp1 = UladApplicationCollectionMapperTest.GetDefaultTestRecord();
            uladApp1.IsPrimary = false;

            var uladApp2 = UladApplicationCollectionMapperTest.GetDefaultTestRecord();
            uladApp2.IsPrimary = false;

            var collection = CreateCollection();
            collection.BeginTrackingChanges();
            collection.Add(uladApp1);
            collection.Add(uladApp2);

            var connection = Substitute.For<IDbConnection>();
            var transaction = Substitute.For<IDbTransaction>();

            var mapper = CreateMapper();
            Assert.Throws<CBaseException>(() => mapper.Save(connection, transaction, new SqlParameterConverter(), collection));
        }

        [Test]
        public void SwitchPrimaryApplication_ShouldSucceed()
        {
            var uladApp1 = UladApplicationCollectionMapperTest.GetDefaultTestRecord();
            uladApp1.IsPrimary = true;

            var collection = CreateCollection();
            collection.BeginTrackingChanges();
            collection.Add(uladApp1);

            var connection = Substitute.For<IDbConnection>();
            var transaction = Substitute.For<IDbTransaction>();

            var mapper = CreateMapper();
            mapper.Save(connection, transaction, new SqlParameterConverter(), collection);

            var uladApp2 = UladApplicationCollectionMapperTest.GetDefaultTestRecord();
            uladApp1.IsPrimary = false;
            uladApp2.IsPrimary = true;
            collection.Add(uladApp2);
            mapper.Save(connection, transaction, new SqlParameterConverter(), collection);
        }

        private static IMutableLqbCollection<DataObjectKind.UladApplication, Guid, UladApplication> CreateCollection()
        {
            var name = Name.Create("UladApplication").Value;
            var identifierFactory = new GuidDataObjectIdentifierFactory<DataObjectKind.UladApplication>();

            return LqbCollection<DataObjectKind.UladApplication, Guid, UladApplication>.Create(name, identifierFactory);
        }

        private static UladApplicationCollectionMapper CreateMapper()
        {
            var loanId = DataObjectIdentifier<DataObjectKind.Loan, Guid>.Create(Guid.NewGuid());
            var brokerId = DataObjectIdentifier<DataObjectKind.ClientCompany, Guid>.Create(Guid.NewGuid());
            var driver = Substitute.For<IStoredProcedureDriver>();

            return new UladApplicationCollectionMapper(loanId, brokerId, driver);
        }
    }
}