﻿namespace LendingQB.Test.Developers.LendingQB.Core.Mapping
{
    using System;
    using System.Data;
    using System.Data.Common;
    using Fakes;
    using LendersOffice.Constants;
    using LendersOffice.Drivers.SqlServerDB;
    using LendersOffice.Security;
    using global::DataAccess;
    using global::LendingQB.Core;
    using global::LendingQB.Core.Data;
    using global::LendingQB.Core.Mapping;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;
    using NUnit.Framework;
    using Utils;

    [TestFixture]
    [Category(CategoryConstants.IntegrationTest)]
    public sealed class ConsumerRealPropertyMapperTest
    {
        private const int MaxFirstCardinality = -1;
        private const int MaxSecondCardinality = -1;

        private DbConnection connection;
        private IDbTransaction transaction;
        private IStoredProcedureDriver storedProcedureDriver;

        private Guid loanId;
        private Guid appId;
        private Guid borrowerId;
        private AbstractUserPrincipal principal;

        [TestFixtureSetUp]
        public void FixtureSetup()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                this.CreateLoan();

                var factory = new StoredProcedureDriverFactory();
                this.storedProcedureDriver = factory.Create(TimeoutInSeconds.Default);
            }
        }

        [SetUp]
        public void MethodSetUp()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                this.connection = DbConnectionInfo.GetConnectionInfo(this.principal.BrokerId).GetConnection();
                this.connection.Open();
                this.transaction = connection.BeginTransaction();
            }
        }

        [TearDown]
        public void MethodTearDown()
        {
            this.transaction.Rollback();
            this.transaction.Dispose();
            this.connection.Close();
        }


        [Test]
        public void Save_Populated()
        {
            var loanIdentifier = DataObjectIdentifier<DataObjectKind.Loan, Guid>.Create(this.loanId);
            var brokerIdentifier = DataObjectIdentifier<DataObjectKind.ClientCompany, Guid>.Create(this.principal.BrokerId);
            var borrowerIdentifier = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(this.borrowerId);
            var appIdentifier = DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>.Create(this.appId);

            var property = RealPropertyCollectionMapperTest.GetDefaultTestRecord();
            IMutableLqbCollection<DataObjectKind.RealProperty, Guid, RealProperty> collectionRP = LqbCollection<DataObjectKind.RealProperty, Guid, RealProperty>.Create(
                Name.Create("Property").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.RealProperty>());
            collectionRP.BeginTrackingChanges();
            var propertyId = collectionRP.Add(property);
            Assert.AreNotEqual(Guid.Empty, propertyId.Value);

            var association = new ConsumerRealPropertyAssociation(borrowerIdentifier, propertyId, appIdentifier);
            association.IsPrimary = true;
            IMutableLqbAssociationSet<DataObjectKind.ConsumerRealPropertyAssociation, Guid, ConsumerRealPropertyAssociation> associationSet = LqbAssociationSet<DataObjectKind.ConsumerRealPropertyAssociation, Guid, ConsumerRealPropertyAssociation>.Create(
                Name.Create("Test").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.ConsumerRealPropertyAssociation>(), MaxFirstCardinality, MaxSecondCardinality);
            associationSet.BeginTrackingChanges();
            var assocId = associationSet.Add(association);
            Assert.AreNotEqual(Guid.Empty, assocId.Value);

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.Json);

                // save data
                var mapperRP = new RealPropertyCollectionMapper(
                    loanIdentifier,
                    brokerIdentifier,
                    this.storedProcedureDriver);
                RealPropertyCollectionMapperTest.SaveCollection(mapperRP, collectionRP, this.connection, this.transaction);

                var mapperA = new ConsumerRealPropertyMapper(
                    loanIdentifier,
                    this.storedProcedureDriver);
                ConsumerRealPropertyMapperTest.SaveAssociationSet(mapperA, associationSet, this.connection, this.transaction);

                // re-load
                associationSet = ConsumerRealPropertyMapperTest.LoadAssociationSet(mapperA, this.connection, this.transaction);
                var loaded = associationSet[assocId];

                // check field values
                Assert.AreEqual(borrowerIdentifier, loaded.ConsumerId);
                Assert.AreEqual(propertyId, loaded.RealPropertyId);
            }
        }

        [Test]
        public void DeleteTest()
        {
            var loanIdentifier = DataObjectIdentifier<DataObjectKind.Loan, Guid>.Create(this.loanId);
            var brokerIdentifier = DataObjectIdentifier<DataObjectKind.ClientCompany, Guid>.Create(this.principal.BrokerId);
            var borrowerIdentifier = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(this.borrowerId);
            var appIdentifier = DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>.Create(this.appId);

            var property1 = RealPropertyCollectionMapperTest.GetDefaultTestRecord();
            var property2 = RealPropertyCollectionMapperTest.GetDefaultTestRecord();
            IMutableLqbCollection<DataObjectKind.RealProperty, Guid, RealProperty> collectionL = LqbCollection<DataObjectKind.RealProperty, Guid, RealProperty>.Create(
                Name.Create("Property").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.RealProperty>());
            collectionL.BeginTrackingChanges();
            var firstRealPropertyId = collectionL.Add(property1);
            var secondRealPropertyId = collectionL.Add(property1);

            var association1 = new ConsumerRealPropertyAssociation(borrowerIdentifier, firstRealPropertyId, appIdentifier);
            association1.IsPrimary = true;
            var association2 = new ConsumerRealPropertyAssociation(borrowerIdentifier, secondRealPropertyId, appIdentifier);
            association2.IsPrimary = true;
            IMutableLqbAssociationSet<DataObjectKind.ConsumerRealPropertyAssociation, Guid, ConsumerRealPropertyAssociation> associationSet = LqbAssociationSet<DataObjectKind.ConsumerRealPropertyAssociation, Guid, ConsumerRealPropertyAssociation>.Create(
                Name.Create("Test").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.ConsumerRealPropertyAssociation>(), MaxFirstCardinality, MaxSecondCardinality);
            associationSet.BeginTrackingChanges();
            var firstAssocId = associationSet.Add(association1);
            var secondAssocId = associationSet.Add(association2);

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.Json);

                var mapperRP = new RealPropertyCollectionMapper(
                    loanIdentifier,
                    brokerIdentifier,
                    this.storedProcedureDriver);
                RealPropertyCollectionMapperTest.SaveCollection(mapperRP, collectionL, this.connection, this.transaction);

                var mapperA = new ConsumerRealPropertyMapper(
                    loanIdentifier,
                    this.storedProcedureDriver);
                SaveAssociationSet(mapperA, associationSet, this.connection, this.transaction);

                associationSet = LoadAssociationSet(mapperA, this.connection, this.transaction);
                Assert.AreEqual(2, associationSet.Count);

                associationSet.BeginTrackingChanges();
                associationSet.Remove(firstAssocId);
                SaveAssociationSet(mapperA, associationSet, this.connection, this.transaction);

                associationSet = LoadAssociationSet(mapperA, this.connection, this.transaction);
                Assert.AreEqual(1, associationSet.Count);
            }
        }

        internal static IMutableLqbAssociationSet<DataObjectKind.ConsumerRealPropertyAssociation, Guid, ConsumerRealPropertyAssociation> LoadAssociationSet(
            ILqbCollectionSqlMapper<DataObjectKind.ConsumerRealPropertyAssociation, Guid, ConsumerRealPropertyAssociation> mapper,
            IDbConnection connection, IDbTransaction transaction)
        {
            return mapper.Load(
                connection,
                transaction,
                Name.Create("Test").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.ConsumerRealPropertyAssociation>(),
                null,
                new DataReaderConverter()) as IMutableLqbAssociationSet<DataObjectKind.ConsumerRealPropertyAssociation, Guid, ConsumerRealPropertyAssociation>;
        }

        internal static void SaveAssociationSet(
            ILqbCollectionSqlMapper<DataObjectKind.ConsumerRealPropertyAssociation, Guid, ConsumerRealPropertyAssociation> mapper,
            IMutableLqbAssociationSet<DataObjectKind.ConsumerRealPropertyAssociation, Guid, ConsumerRealPropertyAssociation> associationSet,
            IDbConnection connection, IDbTransaction transaction)
        {
            mapper.Save(
                connection,
                transaction,
                new SqlParameterConverter(),
                associationSet);
        }

        // Create a new test loan and return <brokerid, loanid>
        private void CreateLoan()
        {
            this.principal = LoginTools.LoginAs(TestAccountType.LoTest001);

            var dataLoan = FakePageData.Create(appCount: 1);
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            var dataApp = dataLoan.GetAppData(0);
            dataApp.aBFirstNm = "Frosty";
            dataApp.aBLastNm = "Snowman";
            dataApp.aBSsn = "555-55-5555";
            dataLoan.Save();

            this.loanId = dataLoan.sLId;
            this.appId = dataApp.aAppId;
            this.borrowerId = dataApp.aBConsumerId;
        }
    }
}
