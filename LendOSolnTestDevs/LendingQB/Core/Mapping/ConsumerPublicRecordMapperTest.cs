﻿namespace LendingQB.Test.Developers.LendingQB.Core.Mapping
{
    using System;
    using System.Data;
    using System.Data.Common;
    using Fakes;
    using LendersOffice.Constants;
    using LendersOffice.Drivers.SqlServerDB;
    using LendersOffice.Security;
    using global::DataAccess;
    using global::LendingQB.Core;
    using global::LendingQB.Core.Data;
    using global::LendingQB.Core.Mapping;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;
    using NUnit.Framework;
    using Utils;

    [TestFixture]
    [Category(CategoryConstants.IntegrationTest)]
    public sealed class ConsumerPublicRecordMapperTest
    {
        private const int MaxFirstCardinality = -1;
        private const int MaxSecondCardinality = -1;

        private DbConnection connection;
        private IDbTransaction transaction;
        private IStoredProcedureDriver storedProcedureDriver;

        private Guid loanId;
        private Guid appId;
        private Guid borrowerId;
        private AbstractUserPrincipal principal;

        [TestFixtureSetUp]
        public void FixtureSetup()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                this.CreateLoan();

                var factory = new StoredProcedureDriverFactory();
                this.storedProcedureDriver = factory.Create(TimeoutInSeconds.Default);
            }
        }

        [SetUp]
        public void MethodSetUp()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                this.connection = DbConnectionInfo.GetConnectionInfo(this.principal.BrokerId).GetConnection();
                this.connection.Open();
                this.transaction = connection.BeginTransaction();
            }
        }

        [TearDown]
        public void MethodTearDown()
        {
            this.transaction.Rollback();
            this.transaction.Dispose();
            this.connection.Close();
        }

        [Test]
        public void Save_Populated()
        {
            var loanIdentifier = DataObjectIdentifier<DataObjectKind.Loan, Guid>.Create(this.loanId);
            var brokerIdentifier = DataObjectIdentifier<DataObjectKind.ClientCompany, Guid>.Create(this.principal.BrokerId);
            var borrowerIdentifier = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(this.borrowerId);
            var appIdentifier = DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>.Create(this.appId);

            var pubRec = PublicRecordCollectionMapperTest.GetDefaultTestRecord();
            IMutableLqbCollection<DataObjectKind.PublicRecord, Guid, PublicRecord> collectionPR = LqbCollection<DataObjectKind.PublicRecord, Guid, PublicRecord>.Create(
                Name.Create("PublicRecord").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.PublicRecord>());
            collectionPR.BeginTrackingChanges();
            var pubRecId = collectionPR.Add(pubRec);
            Assert.AreNotEqual(Guid.Empty, pubRecId.Value);

            var association = new ConsumerPublicRecordAssociation(borrowerIdentifier, pubRecId, appIdentifier);
            association.IsPrimary = true;
            IMutableLqbAssociationSet<DataObjectKind.ConsumerPublicRecordAssociation, Guid, ConsumerPublicRecordAssociation> associationSet = LqbAssociationSet<DataObjectKind.ConsumerPublicRecordAssociation, Guid, ConsumerPublicRecordAssociation>.Create(
                Name.Create("Test").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.ConsumerPublicRecordAssociation>(), MaxFirstCardinality, MaxSecondCardinality);
            associationSet.BeginTrackingChanges();
            var assocId = associationSet.Add(association);
            Assert.AreNotEqual(Guid.Empty, assocId.Value);

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.Json);

                // save data
                var customMapper = new PublicRecordAuditTrailMapper();
                var mapperPR = new PublicRecordCollectionMapper(
                    loanIdentifier,
                    brokerIdentifier,
                    null,
                    customMapper,
                    this.storedProcedureDriver);
                PublicRecordCollectionMapperTest.Save(mapperPR, collectionPR, this.connection, this.transaction);

                var mapperA = new ConsumerPublicRecordMapper(
                    loanIdentifier,
                    this.storedProcedureDriver);
                SaveAssociationSet(mapperA, associationSet, this.connection, this.transaction);

                // re-load
                associationSet = LoadAssociationSet(mapperA, this.connection, this.transaction);
                var loaded = associationSet[assocId];

                // check field values
                Assert.AreEqual(borrowerIdentifier, loaded.ConsumerId);
                Assert.AreEqual(pubRecId, loaded.PublicRecordId);
            }
        }

        [Test]
        public void DeleteTest()
        {
            var loanIdentifier = DataObjectIdentifier<DataObjectKind.Loan, Guid>.Create(this.loanId);
            var brokerIdentifier = DataObjectIdentifier<DataObjectKind.ClientCompany, Guid>.Create(this.principal.BrokerId);
            var borrowerIdentifier = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(this.borrowerId);
            var appIdentifier = DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>.Create(this.appId);

            var pubrec1 = PublicRecordCollectionMapperTest.GetDefaultTestRecord();
            var pubrec2 = PublicRecordCollectionMapperTest.GetDefaultTestRecord();
            IMutableLqbCollection<DataObjectKind.PublicRecord, Guid, PublicRecord> collectionPR = LqbCollection<DataObjectKind.PublicRecord, Guid, PublicRecord>.Create(
                Name.Create("PublicRecord").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.PublicRecord>());
            collectionPR.BeginTrackingChanges();
            var firstPublicRecordId = collectionPR.Add(pubrec1);
            var secondPublicRecordId = collectionPR.Add(pubrec2);

            var association1 = new ConsumerPublicRecordAssociation(borrowerIdentifier, firstPublicRecordId, appIdentifier);
            association1.IsPrimary = true;
            var association2 = new ConsumerPublicRecordAssociation(borrowerIdentifier, secondPublicRecordId, appIdentifier);
            association2.IsPrimary = true;
            IMutableLqbAssociationSet<DataObjectKind.ConsumerPublicRecordAssociation, Guid, ConsumerPublicRecordAssociation> associationSet = LqbAssociationSet<DataObjectKind.ConsumerPublicRecordAssociation, Guid, ConsumerPublicRecordAssociation>.Create(
                Name.Create("Test").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.ConsumerPublicRecordAssociation>(), MaxFirstCardinality, MaxSecondCardinality);
            associationSet.BeginTrackingChanges();
            var firstAssocId = associationSet.Add(association1);
            var secondAssocId = associationSet.Add(association2);

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.Json);

                var customMapper = new PublicRecordAuditTrailMapper();
                var mapperPR = new PublicRecordCollectionMapper(
                    loanIdentifier,
                    brokerIdentifier,
                    null,
                    customMapper,
                    this.storedProcedureDriver);
                PublicRecordCollectionMapperTest.Save(mapperPR, collectionPR, this.connection, this.transaction);

                var mapper = new ConsumerPublicRecordMapper(
                    loanIdentifier,
                    this.storedProcedureDriver);
                SaveAssociationSet(mapper, associationSet, this.connection, this.transaction);

                associationSet = LoadAssociationSet(mapper, this.connection, this.transaction);
                Assert.AreEqual(2, associationSet.Count);

                associationSet.BeginTrackingChanges();
                associationSet.Remove(firstAssocId);
                SaveAssociationSet(mapper, associationSet, this.connection, this.transaction);

                associationSet = LoadAssociationSet(mapper, this.connection, this.transaction);
                Assert.AreEqual(1, associationSet.Count);
            }
        }

        internal static IMutableLqbAssociationSet<DataObjectKind.ConsumerPublicRecordAssociation, Guid, ConsumerPublicRecordAssociation> LoadAssociationSet(
            ILqbCollectionSqlMapper<DataObjectKind.ConsumerPublicRecordAssociation, Guid, ConsumerPublicRecordAssociation> mapper,
            IDbConnection connection, IDbTransaction transaction)
        {
            return mapper.Load(
                connection,
                transaction,
                Name.Create("Test").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.ConsumerPublicRecordAssociation>(),
                null,
                new DataReaderConverter()) as IMutableLqbAssociationSet<DataObjectKind.ConsumerPublicRecordAssociation, Guid, ConsumerPublicRecordAssociation>;
        }

        internal static void SaveAssociationSet(
            ILqbCollectionSqlMapper<DataObjectKind.ConsumerPublicRecordAssociation, Guid, ConsumerPublicRecordAssociation> mapper,
            IMutableLqbAssociationSet<DataObjectKind.ConsumerPublicRecordAssociation, Guid, ConsumerPublicRecordAssociation> associationSet,
            IDbConnection connection, IDbTransaction transaction)
        {
            mapper.Save(
                connection,
                transaction,
                new SqlParameterConverter(),
                associationSet);
        }

        // Create a new test loan and return <brokerid, loanid>
        private void CreateLoan()
        {
            this.principal = LoginTools.LoginAs(TestAccountType.LoTest001);

            var dataLoan = FakePageData.Create(appCount: 1);
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            var dataApp = dataLoan.GetAppData(0);
            dataApp.aBFirstNm = "Frosty";
            dataApp.aBLastNm = "Snowman";
            dataApp.aBSsn = "555-55-5555";
            dataLoan.Save();

            this.loanId = dataLoan.sLId;
            this.appId = dataApp.aAppId;
            this.borrowerId = dataApp.aBConsumerId;
        }
    }
}
