﻿namespace LendingQB.Test.Developers.LendingQB.Core.Mapping
{
    using System;
    using System.Data;
    using System.Data.Common;
    using Fakes;
    using LendersOffice.Constants;
    using LendersOffice.Drivers.SqlServerDB;
    using LendersOffice.Security;
    using global::DataAccess;
    using global::LendingQB.Core;
    using global::LendingQB.Core.Data;
    using global::LendingQB.Core.Mapping;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;
    using NUnit.Framework;
    using Utils;

    [TestFixture]
    [Category(CategoryConstants.IntegrationTest)]
    public sealed class UladApplicationConsumerMapperTest
    {
        private const int MaxFirstCardinality = -1;
        private const int MaxSecondCardinality = 1;

        private DbConnection connection;
        private IDbTransaction transaction;
        private IStoredProcedureDriver storedProcedureDriver;

        private Guid loanId;
        private Guid appId;
        private Guid borrowerId;
        private Guid coborrowerId;
        private AbstractUserPrincipal principal;

        [TestFixtureSetUp]
        public void FixtureSetup()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                this.CreateLoan();

                var factory = new StoredProcedureDriverFactory();
                this.storedProcedureDriver = factory.Create(TimeoutInSeconds.Default);
            }
        }

        [TestFixtureTearDown]
        public void FixtureTearDown()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                this.InactiveLoanFile(this.loanId, this.principal);
            }
        }

        [SetUp]
        public void MethodSetUp()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                this.connection = DbConnectionInfo.GetConnectionInfo(this.principal.BrokerId).GetConnection();
                this.connection.Open();
                this.transaction = connection.BeginTransaction();
            }
        }

        [TearDown]
        public void MethodTearDown()
        {
            this.transaction.Rollback();
            this.transaction.Dispose();
            this.connection.Close();
        }

        [Test]
        public void Save_Populated()
        {
            var loanIdentifier = DataObjectIdentifier<DataObjectKind.Loan, Guid>.Create(this.loanId);
            var brokerIdentifier = DataObjectIdentifier<DataObjectKind.ClientCompany, Guid>.Create(this.principal.BrokerId);
            var borrowerIdentifier = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(this.borrowerId);
            var appIdentifier = DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>.Create(this.appId);

            var uladApp = UladApplicationCollectionMapperTest.GetDefaultTestRecord();
            IMutableLqbCollection<DataObjectKind.UladApplication, Guid, UladApplication> collectionU = LqbCollection<DataObjectKind.UladApplication, Guid, UladApplication>.Create(
                Name.Create("UladApplication").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.UladApplication>());
            collectionU.BeginTrackingChanges();
            var uladId = collectionU.Add(uladApp);
            Assert.AreNotEqual(Guid.Empty, uladId.Value);

            var association = new UladApplicationConsumerAssociation(uladId, borrowerIdentifier, appIdentifier);
            association.IsPrimary = true;
            IMutableLqbAssociationSet<DataObjectKind.UladApplicationConsumerAssociation, Guid, UladApplicationConsumerAssociation> associationSet = LqbAssociationSet<DataObjectKind.UladApplicationConsumerAssociation, Guid, UladApplicationConsumerAssociation>.Create(
                Name.Create("Test").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.UladApplicationConsumerAssociation>(), MaxFirstCardinality, MaxSecondCardinality);
            associationSet.BeginTrackingChanges();
            var assocId = associationSet.Add(association);
            Assert.AreNotEqual(Guid.Empty, assocId.Value);

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.StoredProcedure);

                // save data
                var mapper = new UladApplicationCollectionMapper(
                    loanIdentifier,
                    brokerIdentifier,
                    this.storedProcedureDriver);
                UladApplicationCollectionMapperTest.Save(mapper, collectionU, this.connection, this.transaction);

                var mapperA = new UladApplicationConsumerMapper(
                    loanIdentifier,
                    this.storedProcedureDriver);
                SaveAssociationSet(mapperA, associationSet, this.connection, this.transaction);

                // re-load
                associationSet = LoadAssociationSet(mapperA, this.connection, this.transaction);
                var loaded = associationSet[assocId];

                // check field values
                Assert.AreEqual(appIdentifier, loaded.LegacyAppId);
                Assert.AreEqual(borrowerIdentifier, loaded.ConsumerId);
            }
        }

        [Test]
        public void DeleteTest()
        {
            var loanIdentifier = DataObjectIdentifier<DataObjectKind.Loan, Guid>.Create(this.loanId);
            var brokerIdentifier = DataObjectIdentifier<DataObjectKind.ClientCompany, Guid>.Create(this.principal.BrokerId);
            var borrowerIdentifier = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(this.borrowerId);
            var coborrowerIdentifier = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(this.coborrowerId);
            var appIdentifier = DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>.Create(this.appId);

            var uladApp1 = UladApplicationCollectionMapperTest.GetDefaultTestRecord();
            var uladApp2 = UladApplicationCollectionMapperTest.GetDefaultTestRecord();
            uladApp2.IsPrimary = false;

            IMutableLqbCollection<DataObjectKind.UladApplication, Guid, UladApplication> collectionU = LqbCollection<DataObjectKind.UladApplication, Guid, UladApplication>.Create(
                Name.Create("UladApplication").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.UladApplication>());
            collectionU.BeginTrackingChanges();
            var uladId1 = collectionU.Add(uladApp1);
            var uladId2 = collectionU.Add(uladApp2);
            Assert.AreNotEqual(Guid.Empty, uladId1.Value);
            Assert.AreNotEqual(Guid.Empty, uladId2.Value);
            Assert.AreNotEqual(uladId1.Value, uladId2.Value);

            var association1 = new UladApplicationConsumerAssociation(uladId1, borrowerIdentifier, appIdentifier);
            var association2 = new UladApplicationConsumerAssociation(uladId2, coborrowerIdentifier, appIdentifier);
            association1.IsPrimary = true;
            association2.IsPrimary = true;
            IMutableLqbAssociationSet<DataObjectKind.UladApplicationConsumerAssociation, Guid, UladApplicationConsumerAssociation> associationSet = LqbAssociationSet<DataObjectKind.UladApplicationConsumerAssociation, Guid, UladApplicationConsumerAssociation>.Create(
                Name.Create("Test").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.UladApplicationConsumerAssociation>(), MaxFirstCardinality, MaxSecondCardinality);
            associationSet.BeginTrackingChanges();
            var firstAssocId = associationSet.Add(association1);
            var secondAssocId = associationSet.Add(association2);

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.StoredProcedure);

                var mapper = new UladApplicationCollectionMapper(
                    loanIdentifier,
                    brokerIdentifier,
                    this.storedProcedureDriver);
                UladApplicationCollectionMapperTest.Save(mapper, collectionU, this.connection, this.transaction);

                var mapperA = new UladApplicationConsumerMapper(
                    loanIdentifier,
                    this.storedProcedureDriver);
                SaveAssociationSet(mapperA, associationSet, this.connection, this.transaction);

                associationSet = LoadAssociationSet(mapperA, this.connection, this.transaction);
                Assert.AreEqual(2, associationSet.Count);

                associationSet.BeginTrackingChanges();
                associationSet.Remove(firstAssocId);
                SaveAssociationSet(mapperA, associationSet, this.connection, this.transaction);

                associationSet = LoadAssociationSet(mapperA, this.connection, this.transaction);
                Assert.AreEqual(1, associationSet.Count);
            }
        }

        internal static IMutableLqbAssociationSet<DataObjectKind.UladApplicationConsumerAssociation, Guid, UladApplicationConsumerAssociation> LoadAssociationSet(
            ILqbCollectionSqlMapper<DataObjectKind.UladApplicationConsumerAssociation, Guid, UladApplicationConsumerAssociation> mapper,
            IDbConnection connection, IDbTransaction transaction)
        {
            return mapper.Load(
                connection,
                transaction,
                Name.Create("Test").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.UladApplicationConsumerAssociation>(),
                null,
                new DataReaderConverter()) as IMutableLqbAssociationSet<DataObjectKind.UladApplicationConsumerAssociation, Guid, UladApplicationConsumerAssociation>;
        }

        internal static void SaveAssociationSet(
            ILqbCollectionSqlMapper<DataObjectKind.UladApplicationConsumerAssociation, Guid, UladApplicationConsumerAssociation> mapper,
            IMutableLqbAssociationSet<DataObjectKind.UladApplicationConsumerAssociation, Guid, UladApplicationConsumerAssociation> associationSet,
            IDbConnection connection, IDbTransaction transaction)
        {
            mapper.Save(
                connection,
                transaction,
                new SqlParameterConverter(),
                associationSet);
        }

        // Create a new test loan and return <brokerid, loanid>
        private void CreateLoan()
        {
            this.principal = LoginTools.LoginAs(TestAccountType.LoTest001);

            var dataLoan = FakePageData.Create(appCount: 1);
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            var dataApp = dataLoan.GetAppData(0);
            dataApp.aBFirstNm = "Frosty";
            dataApp.aBLastNm = "Snowman";
            dataApp.aBSsn = "555-55-5555";
            dataApp.aCFirstNm = "Windy";
            dataApp.aCLastNm = "Snowman";
            dataApp.aCSsn = "444-44-4444";
            dataLoan.Save();

            this.loanId = dataLoan.sLId;
            this.appId = dataApp.aAppId;
            this.borrowerId = dataApp.aBConsumerId;
            this.coborrowerId = dataApp.aCConsumerId;
        }

        private void InactiveLoanFile(Guid loanId, AbstractUserPrincipal principal)
        {
            Tools.DeclareLoanFileInvalid(
                principal,
                loanId,
                sendNotifications: false,
                generateLinkLoanMsgEvent: false);
        }
    }
}