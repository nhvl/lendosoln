﻿namespace LendingQB.Test.Developers.LendingQB.Core.Mapping
{
    using System;
    using System.Data;
    using System.Data.Common;
    using global::DataAccess;
    using LendersOffice.Drivers.SqlServerDB;
    using LendersOffice.Security;
    using global::LendingQB.Core;
    using global::LendingQB.Core.Mapping;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;
    using NUnit.Framework;
    using Utils;
    using System.Collections.Generic;
    using LendersOffice.Constants;

    [TestFixture]
    [Category(CategoryConstants.IntegrationTest)]
    public class StoredProcedureCollectionOrderMapperTest
    {
        private DbConnection connection;
        private IDbTransaction transaction;
        private IStoredProcedureDriver storedProcedureDriver;

        private DataObjectIdentifier<DataObjectKind.Loan, Guid> loanId;
        private DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> primaryAppId;
        private DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> secondaryAppId;
        private DataObjectIdentifier<DataObjectKind.Consumer, Guid> primaryAppConsumerId;
        private DataObjectIdentifier<DataObjectKind.Consumer, Guid> secondaryAppConsumerId;
        private AbstractUserPrincipal principal;

        private Guid created0 = Guid.NewGuid();
        private Guid created1 = Guid.NewGuid();
        private Guid created2 = Guid.NewGuid();
        private Guid created3 = Guid.NewGuid();
        private Guid created4 = Guid.NewGuid();
        private Guid created5 = Guid.NewGuid();
        private Guid created6 = Guid.NewGuid();
        private Guid created7 = Guid.NewGuid();
        private Guid created8 = Guid.NewGuid();
        private Guid created9 = Guid.NewGuid();
        private Guid created10 = Guid.NewGuid();
        private Guid created11 = Guid.NewGuid();
        private Guid created12 = Guid.NewGuid();
        private Guid created13 = Guid.NewGuid();

        [TestFixtureSetUp]
        public void FixtureSetup()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                this.CreateLoan();

                var factory = new StoredProcedureDriverFactory();
                this.storedProcedureDriver = factory.Create(TimeoutInSeconds.Default);
            }
        }

        [TestFixtureTearDown]
        public void FixtureTearDown()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                this.InactiveLoanFile(this.loanId.Value, this.principal);
            }
        }

        [SetUp]
        public void MethodSetUp()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                this.connection = DbConnectionInfo.GetConnectionInfo(this.principal.BrokerId).GetConnection();
                this.connection.Open();
                this.transaction = connection.BeginTransaction();
            }
        }

        [TearDown]
        public void MethodTearDown()
        {
            this.transaction.Rollback();
            this.transaction.Dispose();
            this.connection.Close();
        }

        [Test]
        public void InsertThenLoadAll()
        {
            var order = this.InitializeOrdering();
            var mapper = new StoredProcedureCollectionOrderMapper(this.loanId, this.storedProcedureDriver);
            mapper.AddCollectionOrder<DataObjectKind.Asset>(order);
            mapper.Save(this.connection, this.transaction);

            mapper = new StoredProcedureCollectionOrderMapper(this.loanId, this.storedProcedureDriver);
            mapper.Load(this.connection, this.transaction);

            order = mapper.RetrieveCollectionOrder<DataObjectKind.Asset>();
            Assert.AreEqual(5, order.Count);

            order = mapper.RetrieveCollectionOrder<DataObjectKind.Asset>(this.primaryAppId);
            Assert.AreEqual(0, order.Count);

        }

        [Test]
        public void InsertThenLoadAll_MultiAppMultiConsumer()
        {
            var order = this.InitializeOrdering();
            var mapper = new StoredProcedureCollectionOrderMapper(this.loanId, this.storedProcedureDriver);

            mapper.AddCollectionOrder<DataObjectKind.Asset>(order);

            order = this.InitializeOrdering(this.created5, this.created6);
            mapper.AddCollectionOrder<DataObjectKind.Asset>(order, this.primaryAppId);

            order = this.InitializeOrdering(this.created7, this.created8);
            mapper.AddCollectionOrder<DataObjectKind.Asset>(order, this.secondaryAppId);

            order = this.InitializeOrdering(this.created9, this.created10, this.created11);
            mapper.AddCollectionOrder<DataObjectKind.Asset>(order, consumerId: this.primaryAppConsumerId);

            order = this.InitializeOrdering(this.created12, this.created13);
            mapper.AddCollectionOrder<DataObjectKind.Asset>(order, consumerId: this.secondaryAppConsumerId);

            mapper.Save(this.connection, this.transaction);

            mapper = new StoredProcedureCollectionOrderMapper(this.loanId, this.storedProcedureDriver);
            mapper.Load(this.connection, this.transaction);

            order = mapper.RetrieveCollectionOrder<DataObjectKind.Asset>();

            VerifyOrder(order, created0, created1, created2, created3, created4);

            order = mapper.RetrieveCollectionOrder<DataObjectKind.Asset>(this.primaryAppId);
            VerifyOrder(order, created5, created6);

            order = mapper.RetrieveCollectionOrder<DataObjectKind.Asset>(this.secondaryAppId);
            VerifyOrder(order, created7, created8);

            order = mapper.RetrieveCollectionOrder<DataObjectKind.Asset>(consumerId: this.primaryAppConsumerId);
            VerifyOrder(order, created9, created10, created11);

            order = mapper.RetrieveCollectionOrder<DataObjectKind.Asset>(consumerId: this.secondaryAppConsumerId);
            VerifyOrder(order, created12, created13);
        }

        [Test]
        public void InsertThenUpdate()
        {
            var order = this.InitializeOrdering();
            var mapper = new StoredProcedureCollectionOrderMapper(this.loanId, this.storedProcedureDriver);
            mapper.AddCollectionOrder<DataObjectKind.Asset>(order);
            mapper.Save(this.connection, this.transaction);

            var middleId = DataObjectIdentifier<DataObjectKind.Asset, Guid>.Create(this.created2);
            order.MoveBack(middleId);

            mapper = new StoredProcedureCollectionOrderMapper(this.loanId, this.storedProcedureDriver);
            mapper.AddCollectionOrder(order);
            mapper.Save(this.connection, this.transaction);

            mapper = new StoredProcedureCollectionOrderMapper(this.loanId, this.storedProcedureDriver);
            mapper.Load(this.connection, this.transaction);

            order = mapper.RetrieveCollectionOrder<DataObjectKind.Asset>();

            VerifyOrder(order, created0, created1, created3, created2, created4);
        }

        [Test]
        public void InsertThenUpdate_MultiAppMultiConsumer()
        {
            var order = this.InitializeOrdering(this.created0, this.created1);
            var mapper = new StoredProcedureCollectionOrderMapper(this.loanId, this.storedProcedureDriver);
            mapper.AddCollectionOrder<DataObjectKind.Asset>(order);

            order = this.InitializeOrdering(this.created2, this.created3, this.created4, this.created5, this.created6);
            mapper.AddCollectionOrder<DataObjectKind.Asset>(order, this.primaryAppId);

            order = this.InitializeOrdering(this.created7, this.created8);
            mapper.AddCollectionOrder<DataObjectKind.Asset>(order, this.secondaryAppId);

            order = this.InitializeOrdering(this.created9, this.created10, this.created11);
            mapper.AddCollectionOrder<DataObjectKind.Asset>(order, consumerId: this.primaryAppConsumerId);

            order = this.InitializeOrdering(this.created12, this.created13);
            mapper.AddCollectionOrder<DataObjectKind.Asset>(order, consumerId: this.secondaryAppConsumerId);

            mapper.Save(this.connection, this.transaction);

            var middleId = DataObjectIdentifier<DataObjectKind.Asset, Guid>.Create(this.created4);
            var primaryAppOrder = mapper.RetrieveCollectionOrder<DataObjectKind.Asset>(this.primaryAppId);
            primaryAppOrder.MoveBack(middleId);

            var consumer2FirstId = DataObjectIdentifier<DataObjectKind.Asset, Guid>.Create(created12);
            var secondConsumerOrder = mapper.RetrieveCollectionOrder<DataObjectKind.Asset>(consumerId: this.secondaryAppConsumerId);
            secondConsumerOrder.MoveBack(consumer2FirstId);

            mapper = new StoredProcedureCollectionOrderMapper(this.loanId, this.storedProcedureDriver);
            mapper.AddCollectionOrder(primaryAppOrder, this.primaryAppId);
            mapper.AddCollectionOrder(secondConsumerOrder, consumerId: this.secondaryAppConsumerId);
            mapper.Save(this.connection, this.transaction);

            mapper = new StoredProcedureCollectionOrderMapper(this.loanId, this.storedProcedureDriver);
            mapper.Load(this.connection, this.transaction);

            order = mapper.RetrieveCollectionOrder<DataObjectKind.Asset>();
            VerifyOrder(order, created0, created1);

            order = mapper.RetrieveCollectionOrder<DataObjectKind.Asset>(this.primaryAppId);
            VerifyOrder(order, created2, created3, created5, created4, created6);

            order = mapper.RetrieveCollectionOrder<DataObjectKind.Asset>(this.secondaryAppId);
            VerifyOrder(order, created7, created8);

            order = mapper.RetrieveCollectionOrder<DataObjectKind.Asset>(consumerId: this.primaryAppConsumerId);
            VerifyOrder(order, created9, created10, created11);

            order = mapper.RetrieveCollectionOrder<DataObjectKind.Asset>(consumerId: this.secondaryAppConsumerId);
            VerifyOrder(order, created13, created12);
        }

        private IOrderedIdentifierCollection<DataObjectKind.Asset, Guid> InitializeOrdering()
        {
            var order = OrderedIdentifierCollection<DataObjectKind.Asset, Guid>.Create();
            var construct = (IInitializeCollectionOrder<Guid>)order;
            construct.Add(this.created0);
            construct.Add(this.created1);
            construct.Add(this.created2);
            construct.Add(this.created3);
            construct.Add(this.created4);

            return order;
        }

        private IOrderedIdentifierCollection<DataObjectKind.Asset, Guid> InitializeOrdering(params Guid[] ids)
        {
            var order = OrderedIdentifierCollection<DataObjectKind.Asset, Guid>.Create();
            var construct = (IInitializeCollectionOrder<Guid>)order;
            foreach(var id in ids )
            {
                construct.Add(id);
            }
            return order;
        }

        private static void VerifyOrder(IOrderedIdentifierCollection<DataObjectKind.Asset, Guid> order, params Guid[] expected)
        {
            Assert.AreEqual(expected.Length, order.Count);
            var count = order.Count;
            for (int i = 0; i < count; i++)
            {
                Assert.AreEqual(expected[i], order.GetIdentifierAt(i).Value);
            }
        }

        // Create a new test loan and set loanid and primary appid
        private void CreateLoan()
        {
            this.principal = LoginTools.LoginAs(TestAccountType.LoTest001);

            var creator = CLoanFileCreator.GetCreator(principal, LendersOffice.Audit.E_LoanCreationSource.UserCreateFromBlank);
            var id = creator.CreateBlankLoanFile();

            this.loanId = DataObjectIdentifier<DataObjectKind.Loan, Guid>.Create(id);
            AssignAppIdsAndConsumerIds();
        }

        private void AssignAppIdsAndConsumerIds()
        {
            var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                this.loanId.Value,
                typeof(StoredProcedureCollectionOrderMapperTest));

            int newAppIndex = loan.AddNewApp();
            loan.InitSave(ConstAppDavid.SkipVersionCheck);

            var primaryApp = loan.GetAppData(0);
            this.primaryAppId = DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>.Create(primaryApp.aAppId);
            this.primaryAppConsumerId = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(primaryApp.aBConsumerId);

            var secondApp = loan.GetAppData(newAppIndex);
            this.secondaryAppId = DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>.Create(secondApp.aAppId);
            this.secondaryAppConsumerId = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(secondApp.aBConsumerId);

            loan.Save();
        }

        private void InactiveLoanFile(Guid loanId, AbstractUserPrincipal principal)
        {
            Tools.DeclareLoanFileInvalid(
                principal,
                loanId,
                sendNotifications: false,
                generateLinkLoanMsgEvent: false);
        }
    }
}
