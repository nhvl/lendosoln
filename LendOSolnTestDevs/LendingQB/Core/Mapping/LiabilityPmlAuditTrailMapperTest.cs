﻿namespace LendingQB.Test.Developers.LendingQB.Core.Mapping
{
    using System;
    using System.Collections.Generic;
    using global::DataAccess;
    using global::LendingQB.Core.Mapping;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public class LiabilityPmlAuditTrailMapperTest
    {
        [Test]
        public void ToSqlParameter_NullValue_ReturnsParameterWithDbNullValue()
        {
            var mapper = new LiabilityPmlAuditTrailMapper();

            var param = mapper.ToSqlParameter("TestField", null);

            Assert.AreEqual(DBNull.Value, param.Value);
        }

        [Test]
        public void ToSqlParameter_EmptyList_ReturnsParameterWithDbNullValue()
        {
            var mapper = new LiabilityPmlAuditTrailMapper();

            var param = mapper.ToSqlParameter("TestField", new List<LiabilityPmlAuditTrailEvent>());

            Assert.AreEqual(DBNull.Value, param.Value);
        }
    }
}
