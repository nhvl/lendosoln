﻿namespace LendingQB.Test.Developers.LendingQB.Core.Mapping
{
    using System;
    using System.Data;
    using System.Data.Common;
    using DataAccess;
    using LendersOffice.Drivers.SqlServerDB;
    using LendersOffice.Security;
    using global::LendingQB.Core;
    using global::LendingQB.Core.Data;
    using global::LendingQB.Core.Mapping;
    using LqbGrammar;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;
    using NUnit.Framework;
    using Utils;

    using EmpVerifId = LqbGrammar.DataTypes.DataObjectIdentifier<LqbGrammar.DataTypes.DataObjectKind.Employee, System.Guid>;
    using SigVerifId = LqbGrammar.DataTypes.DataObjectIdentifier<LqbGrammar.DataTypes.DataObjectKind.Signature, System.Guid>;
    using global::DataAccess;
    using NSubstitute;

    [TestFixture]
    [Category(CategoryConstants.IntegrationTest)]
    public sealed class EmploymentRecordCollectionMapperTest
    {
        private const string VoeVerifierId = "DBB06C9D-74C6-4C1D-90E0-9A14C34CF213";

        private DbConnection connection;
        private IDbTransaction transaction;
        private IStoredProcedureDriver storedProcedureDriver;

        private Guid loanId;
        private AbstractUserPrincipal principal;

        [TestFixtureSetUp]
        public void FixtureSetup()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                this.CreateLoan();

                var factory = new StoredProcedureDriverFactory();
                this.storedProcedureDriver = factory.Create(TimeoutInSeconds.Default);
            }
        }

        [TestFixtureTearDown]
        public void FixtureTearDown()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                this.InactiveLoanFile(this.loanId, this.principal);
            }
        }

        [SetUp]
        public void MethodSetUp()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                this.connection = DbConnectionInfo.GetConnectionInfo(this.principal.BrokerId).GetConnection();
                this.connection.Open();
                this.transaction = connection.BeginTransaction();
            }
        }

        [TearDown]
        public void MethodTearDown()
        {
            this.transaction.Rollback();
            this.transaction.Dispose();
            this.connection.Close();
        }

        [Test]
        public void Save_Empty()
        {
            var entity = new EmploymentRecord(NSubstitute.Substitute.For<IEmploymentRecordDefaultsProvider>());
            this.CreateAndLoad(entity);
        }

        [Test]
        public void Save_Populated()
        {
            var entity = GetDefaultTestRecord();
            this.CreateAndLoad(entity);
        }

        [Test]
        public void Update()
        {
            var loanIdentifier = DataObjectIdentifier<DataObjectKind.Loan, Guid>.Create(this.loanId);
            var brokerIdentifier = DataObjectIdentifier<DataObjectKind.ClientCompany, Guid>.Create(this.principal.BrokerId);

            var original = GetDefaultTestRecord();
            IMutableLqbCollection<DataObjectKind.EmploymentRecord, Guid, EmploymentRecord> collection = LqbCollection<DataObjectKind.EmploymentRecord, Guid, EmploymentRecord>.Create(
                Name.Create("Test").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.EmploymentRecord>());
            collection.BeginTrackingChanges();
            var entityId = collection.Add(original);
            Assert.AreNotEqual(Guid.Empty, entityId.Value);

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.Json);

                // save data
                var voeDataMapper = new EmploymentRecordEmploymentRecordVoeDataMapper();
                var incomeRecordMapper = new IncomeRecordCollectionMapper();
                var mapper = new EmploymentRecordCollectionMapper(
                    loanIdentifier,
                    brokerIdentifier,
                    Substitute.For<IEmploymentRecordDefaultsProvider>(),
                    voeDataMapper,
                    incomeRecordMapper,
                    this.storedProcedureDriver);
                this.SaveCollection(mapper, collection);

                // load data
                collection = this.LoadCollection(mapper);
                var updateEntity = collection[entityId];

                // update data
                collection.BeginTrackingChanges();
                updateEntity.EmployerName = EntityName.Create("WHO band");
                var voeItem = CreateVoeData("CODE_C");
                updateEntity.EmploymentRecordVoeData.Add(voeItem);

                var empty = new EmploymentRecord(NSubstitute.Substitute.For<IEmploymentRecordDefaultsProvider>());
                var emptyId = collection.Add(empty);

                // save updates
                this.SaveCollection(mapper, collection);

                // re-load
                collection = this.LoadCollection(mapper);
                var loadedEntity = collection[entityId];
                var emptyEntity = collection[emptyId];

                // check field values
                this.CompareValues(updateEntity, loadedEntity);
                this.CompareValues(empty, emptyEntity);
            }
        }

        [Test]
        public void Delete()
        {
            var loanIdentifier = DataObjectIdentifier<DataObjectKind.Loan, Guid>.Create(this.loanId);
            var brokerIdentifier = DataObjectIdentifier<DataObjectKind.ClientCompany, Guid>.Create(this.principal.BrokerId);

            IMutableLqbCollection<DataObjectKind.EmploymentRecord, Guid, EmploymentRecord> collection = LqbCollection<DataObjectKind.EmploymentRecord, Guid, EmploymentRecord>.Create(
                Name.Create("Test").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.EmploymentRecord>());
            collection.BeginTrackingChanges();

            var original = GetDefaultTestRecord();
            var entityId = collection.Add(original);

            var empty = new EmploymentRecord(NSubstitute.Substitute.For<IEmploymentRecordDefaultsProvider>());
            var emptyId = collection.Add(empty);

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.Json);

                // save data
                var voeDataMapper = new EmploymentRecordEmploymentRecordVoeDataMapper();
                var incomeRecordMapper = new IncomeRecordCollectionMapper();
                var mapper = new EmploymentRecordCollectionMapper(
                    loanIdentifier,
                    brokerIdentifier,
                    NSubstitute.Substitute.For<IEmploymentRecordDefaultsProvider>(),
                    voeDataMapper,
                    incomeRecordMapper,
                    this.storedProcedureDriver);
                this.SaveCollection(mapper, collection);

                // load data
                collection = this.LoadCollection(mapper);
                var loadedEntity = collection[entityId];
                var loadedEmpty = collection[emptyId];

                // delete data
                collection.BeginTrackingChanges();
                collection.Remove(emptyId);

                // save updates
                this.SaveCollection(mapper, collection);

                // re-load
                collection = this.LoadCollection(mapper);
                Assert.AreEqual(1, collection.Count);
            }
        }

        private void CreateAndLoad(EmploymentRecord original)
        {
            var loanIdentifier = DataObjectIdentifier<DataObjectKind.Loan, Guid>.Create(this.loanId);
            var brokerIdentifier = DataObjectIdentifier<DataObjectKind.ClientCompany, Guid>.Create(this.principal.BrokerId);

            IMutableLqbCollection<DataObjectKind.EmploymentRecord, Guid, EmploymentRecord> collection = LqbCollection<DataObjectKind.EmploymentRecord, Guid, EmploymentRecord>.Create(
                Name.Create("Test").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.EmploymentRecord>());
            collection.BeginTrackingChanges();
            var entityId = collection.Add(original);
            Assert.AreNotEqual(Guid.Empty, entityId.Value);

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.Json);

                // save data
                var voeDataMapper = new EmploymentRecordEmploymentRecordVoeDataMapper();
                var incomeRecordMapper = new IncomeRecordCollectionMapper();
                var mapper = new EmploymentRecordCollectionMapper(
                    loanIdentifier,
                    brokerIdentifier,
                    Substitute.For<IEmploymentRecordDefaultsProvider>(),
                    voeDataMapper,
                    incomeRecordMapper,
                    this.storedProcedureDriver);
                this.SaveCollection(mapper, collection);

                // load data
                collection = this.LoadCollection(mapper);
                var loadedEntity = collection[entityId];

                // check field values
                this.CompareValues(original, loadedEntity);
            }
        }

        private void CompareValues(EmploymentRecord original, EmploymentRecord loadedEntity)
        {
            Assert.IsTrue(loadedEntity.Attention == original.Attention);
            Assert.IsTrue(loadedEntity.EmployerAddress == original.EmployerAddress);
            Assert.IsTrue(loadedEntity.EmployerCity == original.EmployerCity);
            Assert.IsTrue(loadedEntity.EmployerFax == original.EmployerFax);
            Assert.IsTrue(loadedEntity.EmployerName == original.EmployerName);
            //// Temporary for case 475067 until the code changes match the DB schema changes. Assert.IsTrue(loadedEntity.EmployerPhone == original.EmployerPhone);
            Assert.IsTrue(loadedEntity.EmployerState == original.EmployerState);
            Assert.IsTrue(loadedEntity.EmployerZip == original.EmployerZip);
            Assert.IsTrue(loadedEntity.EmploymentEndDate == original.EmploymentEndDate);
            Assert.IsTrue(loadedEntity.EmploymentStartDate == original.EmploymentStartDate);
            Assert.IsTrue(loadedEntity.EmploymentStatusType == original.EmploymentStatusType);
            Assert.IsTrue(loadedEntity.EmploymentYears == original.EmploymentYears);
            Assert.IsTrue(loadedEntity.IsCurrent == original.IsCurrent);
            Assert.IsTrue(loadedEntity.IsSeeAttachment == original.IsSeeAttachment);
            Assert.IsTrue(loadedEntity.IsSelfEmployed == original.IsSelfEmployed);
            Assert.IsTrue(loadedEntity.JobTitle == original.JobTitle);
            //// Temporary for case 475067 until the code changes match the DB schema changes. Assert.IsTrue(loadedEntity.MonthlyIncome == original.MonthlyIncome);
            Assert.IsTrue(loadedEntity.PrepDate == original.PrepDate);
            Assert.IsTrue(loadedEntity.PrimaryEmploymentStartDate == original.PrimaryEmploymentStartDate);
            Assert.IsTrue(loadedEntity.ProfessionStartDate == original.ProfessionStartDate);
            Assert.IsTrue(loadedEntity.ProfessionYears == original.ProfessionYears);
            Assert.IsTrue(loadedEntity.VerifExpiresDate == original.VerifExpiresDate);
            Assert.IsTrue(loadedEntity.VerifExpiresDate == original.VerifExpiresDate);
            Assert.IsTrue(loadedEntity.VerifRecvDate == original.VerifRecvDate);
            Assert.IsTrue(loadedEntity.VerifReorderedDate == original.VerifReorderedDate);
            Assert.IsTrue(loadedEntity.VerifSentDate == original.VerifSentDate);
            Assert.IsTrue(loadedEntity.VerifSignatureImgId == original.VerifSignatureImgId);
            Assert.IsTrue(loadedEntity.VerifSigningEmployeeId == original.VerifSigningEmployeeId);
            if (original.EmploymentRecordVoeData != null)
            {
                Assert.IsTrue(loadedEntity.EmploymentRecordVoeData.Count == original.EmploymentRecordVoeData.Count);
                for (int i = 0; i < original.EmploymentRecordVoeData.Count; ++i)
                {
                    var org = original.EmploymentRecordVoeData[i];
                    var ldd = loadedEntity.EmploymentRecordVoeData[i];

                    Assert.IsTrue(ldd.EmployeeIdVoe == org.EmployeeIdVoe);
                    Assert.IsTrue(ldd.EmployerCodeVoe == org.EmployerCodeVoe);
                }
            }

            Assert.AreEqual(loadedEntity.IncomeRecords.Count, original.IncomeRecords.Count);
            if (original.IncomeRecords.Count > 0)
            {
                foreach (var originalIncomeKvp in original.IncomeRecords)
                {
                    var originalIncome = originalIncomeKvp.Value;
                    var loadedIncome = loadedEntity.IncomeRecords[originalIncomeKvp.Key];

                    Assert.AreEqual(loadedIncome.IncomeType, originalIncome.IncomeType);
                    Assert.AreEqual(loadedIncome.MonthlyAmount, originalIncome.MonthlyAmount);
                }
            }
        }

        public static EmploymentRecord GetDefaultTestRecord()
        {
            var record = new EmploymentRecord(NSubstitute.Substitute.For<IEmploymentRecordDefaultsProvider>());
            record.Attention = EntityName.Create("Kieth Moon");
            record.EmployerAddress = StreetAddress.Create("1600 Sunflower Ave");
            record.EmployerCity = City.Create("Costa Mesa").Value;
            record.EmployerFax = PhoneNumber.Create("714-957-6334");
            record.EmployerName = EntityName.Create("MeridianLink");
            record.EmployerPhone = PhoneNumber.Create("714-957-6334");
            record.EmployerState = UnitedStatesPostalState.Create("CA");
            record.EmployerZip = Zipcode.Create("92626");
            record.EmploymentEndDate = UnzonedDate.Create("3/17/2017");
            record.EmploymentStartDate = UnzonedDate.Create("12/24/1998");
            record.EmploymentStatusType = E_EmplmtStat.Previous;
            record.IsCurrent = false;
            record.IsSeeAttachment = true;
            record.IsSelfEmployed = false;
            record.JobTitle = DescriptionField.Create("Drummer");
            record.MonthlyIncome = Money.Create(2000000.17m);
            record.PrepDate = UnzonedDate.Create("1/4/2004");
            record.ProfessionStartDate = UnzonedDate.Create("12/24/1998");
            record.VerifExpiresDate = UnzonedDate.Create("5/14/2018");
            record.VerifRecvDate = UnzonedDate.Create("3/14/2018");
            record.VerifSentDate = UnzonedDate.Create("2/2/2018");

            var guid = new Guid(VoeVerifierId);
            var signer = EmpVerifId.Create(guid);
            var signature = SigVerifId.Create(Guid.NewGuid());
            record.SetVerifSignature(signer, signature);

            record.EmploymentRecordVoeData = new System.Collections.Generic.List<EmploymentRecordVOEData>();
            record.EmploymentRecordVoeData.AddRange(GetDefaultVoes());

            record.IncomeRecords.Add(new IncomeRecord { IncomeType = IncomeType.TipIncome, MonthlyAmount = Money.Create(500) });

            return record;
        }

        private static EmploymentRecordVOEData[] GetDefaultVoes()
        {
            var items = new EmploymentRecordVOEData[2];

            var item = CreateVoeData("CODE_A");
            items[0] = item;

            item = CreateVoeData("CODE_B");
            items[1] = item;

            return items;
        }

        private static EmploymentRecordVOEData CreateVoeData(string employerCode)
        {
            var item = new EmploymentRecordVOEData();
            item.EmployeeIdVoe = VoeVerifierId;
            item.EmployerCodeVoe = employerCode;
            return item;
        }

        private IMutableLqbCollection<DataObjectKind.EmploymentRecord, Guid, EmploymentRecord> LoadCollection(
            ILqbCollectionSqlMapper<DataObjectKind.EmploymentRecord, Guid, EmploymentRecord> mapper)
        {
            return mapper.Load(
                this.connection,
                this.transaction,
                Name.Create("Test").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.EmploymentRecord>(),
                null,
                new DataReaderConverter());
        }

        internal static void SaveEmpCollection(
            IDbConnection connection,
            IDbTransaction transaction,
            ILqbCollectionSqlMapper<DataObjectKind.EmploymentRecord, Guid, EmploymentRecord> mapper,
            IMutableLqbCollection<DataObjectKind.EmploymentRecord, Guid, EmploymentRecord> collection)
        {
            mapper.Save(
                connection,
                transaction,
                new SqlParameterConverter(),
                collection);
        }

        private void SaveCollection(
            ILqbCollectionSqlMapper<DataObjectKind.EmploymentRecord, Guid, EmploymentRecord> mapper,
            IMutableLqbCollection<DataObjectKind.EmploymentRecord, Guid, EmploymentRecord> collection)
        {
            SaveEmpCollection(this.connection, this.transaction, mapper, collection);
        }

        // Create a new test loan and return <brokerid, loanid>
        private void CreateLoan()
        {
            this.principal = LoginTools.LoginAs(TestAccountType.LoTest001);

            var creator = CLoanFileCreator.GetCreator(principal, LendersOffice.Audit.E_LoanCreationSource.UserCreateFromBlank);
            this.loanId = creator.CreateBlankLoanFile();
        }

        private void InactiveLoanFile(Guid loanId, AbstractUserPrincipal principal)
        {
            Tools.DeclareLoanFileInvalid(
                principal,
                loanId,
                sendNotifications: false,
                generateLinkLoanMsgEvent: false);
        }
    }
}
