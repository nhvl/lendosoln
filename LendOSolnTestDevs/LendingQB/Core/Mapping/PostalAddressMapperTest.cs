﻿namespace LendingQB.Test.Developers.LendingQB.Core.Mapping
{
    using global::DataAccess;
    using global::LendingQB.Core.Mapping;
    using global::LqbGrammar.DataTypes;
    using NUnit.Framework;
    using Utils;

    [TestFixture]
    [Category(CategoryConstants.IntegrationTest)]
    public sealed class PostalAddressMapperTest
    {
        private const int BiggestJsonLength = 906;

        private int StandardUnparsedAddressLength;
        private int StandardAddressNumberLength;
        private int StandardStreetPreDirectionLength;
        private int StandardStreetNameLength;
        private int StandardStreetSuffixLength;
        private int StandardUnitTypeLength;
        private int StandardUnitIdentifierLength;
        private int StandardUsAddressLength;

        [TestFixtureSetUp]
        public void FixtureSetup()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.Json);

                var builder = PostalAddressMapperUnitTests.PopulateGoodUsAddressData();
                this.StandardUnparsedAddressLength = builder.UnparsedStreetAddress.Value.ToString().Length;
                this.StandardAddressNumberLength = builder.AddressNumber.Value.ToString().Length;
                this.StandardStreetPreDirectionLength = builder.StreetPreDirection.Value.ToString().Length;
                this.StandardStreetNameLength = builder.StreetName.Value.ToString().Length;
                this.StandardStreetSuffixLength = builder.StreetSuffix.Value.ToString().Length;
                this.StandardUnitTypeLength = builder.UnitType.Value.ToString().Length;
                this.StandardUnitIdentifierLength = builder.UnitIdentifier.Value.ToString().Length;

                var address = builder.GetAddress();
                var mapper = new PostalAddressMapper();
                var pram = mapper.ToSqlParameter("@address", address);

                this.StandardUsAddressLength = ((string)pram.Value).Length;
            }
        }

        [Test]
        public void Mapper_GeneralAddress_RoundTripMatches()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.Json);

                var address = global::LendingQB.Test.Developers.DataAccess.Address.GeneralPostalAddressTest.CreateAddress();

                var mapper = new PostalAddressMapper();
                var sqlParam = mapper.ToSqlParameter("@address", address);
                string json = (string)sqlParam.Value;

                var recovered = mapper.Create(json);
                Assert.AreEqual(address.GetType(), recovered.GetType());

                var general = (GeneralPostalAddress)recovered;
                Assert.AreEqual(address.City, general.City);
                Assert.AreEqual(address.Country, general.Country);
                Assert.AreEqual(address.CountryCode, general.CountryCode);
                Assert.AreEqual(address.PostalCode, general.PostalCode);
                Assert.AreEqual(address.State, general.State);
                Assert.AreEqual(address.StreetAddress, general.StreetAddress);
            }
        }

        [Test]
        public void Mapper_UnparsedUSA_RoundTripMatches()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.Json);

                var address = global::LendingQB.Test.Developers.DataAccess.Address.UnitedStatesPostalAddressTest.CreateUnparsedAddress();

                var mapper = new PostalAddressMapper();
                var sqlParam = mapper.ToSqlParameter("@address", address);
                string json = (string)sqlParam.Value;

                var recovered = mapper.Create(json);
                Assert.AreEqual(address.GetType(), recovered.GetType());

                var unparsed = (UnitedStatesPostalAddress)recovered;
                Assert.AreEqual(address.City, unparsed.City);
                Assert.AreEqual(address.Country, unparsed.Country);
                Assert.AreEqual(address.CountryCode, unparsed.CountryCode);
                Assert.AreEqual(address.PostalCode, unparsed.PostalCode);
                Assert.AreEqual(address.State, unparsed.State);
                Assert.AreEqual(address.StreetAddress, unparsed.StreetAddress);

                Assert.IsNull(unparsed.AddressNumber);
                Assert.IsNull(unparsed.StreetName);
                Assert.IsNull(unparsed.StreetPostDirection);
                Assert.IsNull(unparsed.StreetPreDirection);
                Assert.IsNull(unparsed.StreetSuffix);
                Assert.IsNull(unparsed.UnitIdentifier);
                Assert.IsNull(unparsed.UnitType);
            }
        }

        [Test]
        public void Mapper_ParsedUSA_RoundTripMatches()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.Json);

                var address = global::LendingQB.Test.Developers.DataAccess.Address.UnitedStatesPostalAddressTest.CreateParsedAddress();

                var mapper = new PostalAddressMapper();
                var sqlParam = mapper.ToSqlParameter("@address", address);
                string json = (string)sqlParam.Value;

                var recovered = mapper.Create(json);
                Assert.AreEqual(address.GetType(), recovered.GetType());

                var parsed = (UnitedStatesPostalAddress)recovered;
                Assert.AreEqual(address.City, parsed.City);
                Assert.AreEqual(address.Country, parsed.Country);
                Assert.AreEqual(address.CountryCode, parsed.CountryCode);
                Assert.AreEqual(address.PostalCode, parsed.PostalCode);
                Assert.AreEqual(address.State, parsed.State);
                Assert.AreEqual(address.StreetAddress, parsed.StreetAddress);

                Assert.AreEqual(address.AddressNumber, parsed.AddressNumber);
                Assert.AreEqual(address.StreetPreDirection, parsed.StreetPreDirection);
                Assert.AreEqual(address.StreetName, parsed.StreetName);
                Assert.AreEqual(address.StreetSuffix, parsed.StreetSuffix);
                Assert.AreEqual(address.StreetPostDirection, parsed.StreetPostDirection);
                Assert.AreEqual(address.UnitType, parsed.UnitType);
                Assert.AreEqual(address.UnitIdentifier, parsed.UnitIdentifier);
            }
        }

        [Test]
        public void Mapper_BiggestUSA_CalculateJsonLength()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.Json);

                // Create address components that each have the maximum database size for the individual field lengths
                var addressData = new PostalAddressData();
                addressData.AddressNumber = new string('3', 16);
                addressData.AddressType = "UsNormal";
                addressData.City = new string('a', 256);
                addressData.CountryCode = "USA";
                addressData.PostalCode = new string('1', 10);
                addressData.State = new string('s', 2);
                addressData.StreetAddress = new string('d', 256);
                addressData.StreetName = new string('f', 32);
                addressData.StreetPreDirection = new string ('g', 16);
                addressData.StreetPostDirection = new string('h', 16);
                addressData.StreetSuffix = new string('j', 32);
                addressData.UnitIdentifier = new string('k', 16);
                addressData.UnitType = new string('l', 16);

                var address = (UnitedStatesPostalAddress)addressData.BuildPostalAddress();

                Assert.IsTrue(address.AddressNumber != null && !string.IsNullOrEmpty(address.AddressNumber.Value.ToString()));
                Assert.IsTrue(address.Country != null && !string.IsNullOrEmpty(address.Country.Value.ToString()));
                Assert.IsTrue(!string.IsNullOrEmpty(address.CountryCode.ToString()));
                Assert.IsTrue(!string.IsNullOrEmpty(address.PostalCode.ToString()));
                Assert.IsTrue(!string.IsNullOrEmpty(address.State.ToString()));
                Assert.IsTrue(!string.IsNullOrEmpty(address.StreetAddress.ToString()));
                Assert.IsTrue(address.StreetName != null && !string.IsNullOrEmpty(address.StreetName.Value.ToString()));
                Assert.IsTrue(address.StreetPostDirection != null && !string.IsNullOrEmpty(address.StreetPostDirection.Value.ToString()));
                Assert.IsTrue(address.StreetPreDirection != null && !string.IsNullOrEmpty(address.StreetPreDirection.Value.ToString()));
                Assert.IsTrue(address.StreetSuffix != null && !string.IsNullOrEmpty(address.StreetSuffix.Value.ToString()));
                Assert.IsTrue(address.UnitIdentifier != null && !string.IsNullOrEmpty(address.UnitIdentifier.Value.ToString()));
                Assert.IsTrue(address.UnitType != null && !string.IsNullOrEmpty(address.UnitType.Value.ToString()));
                Assert.IsTrue(address.UnparsedStreetAddress != null && !string.IsNullOrEmpty(address.UnparsedStreetAddress.Value.ToString()));
                Assert.IsTrue(!string.IsNullOrEmpty(address.UsState.ToString()));
                Assert.IsTrue(!string.IsNullOrEmpty(address.Zipcode.ToString()));

                var mapper = new PostalAddressMapper();
                var sqlParam = mapper.ToSqlParameter("@address", address);
                string json = (string)sqlParam.Value;

                Assert.AreEqual(BiggestJsonLength, json.Length);
            }
        }

        [Test]
        public void UsAddress_StreetAddressBlank_GoodParameter()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.Json);

                var builder = PostalAddressMapperUnitTests.PopulateGoodUsAddressData();
                builder.UnparsedStreetAddress = null;

                var address = builder.GetAddress();
                var mapper = new PostalAddressMapper();
                var pram = mapper.ToSqlParameter("@address", address);
                Assert.AreEqual(this.StandardUsAddressLength - this.StandardUnparsedAddressLength + 2, ((string)pram.Value).Length);
            }
        }

        [Test]
        public void UsAddress_AddressNumberBlank_GoodParameter()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.Json);

                var builder = PostalAddressMapperUnitTests.PopulateGoodUsAddressData();
                builder.AddressNumber = null;

                var address = builder.GetAddress();
                var mapper = new PostalAddressMapper();
                var pram = mapper.ToSqlParameter("@address", address);
                Assert.AreEqual(this.StandardUsAddressLength - this.StandardAddressNumberLength + 2, ((string)pram.Value).Length);
            }
        }

        [Test]
        public void UsAddress_DirectionalBlank_GoodParameter()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.Json);

                var builder = PostalAddressMapperUnitTests.PopulateGoodUsAddressData();
                builder.StreetPreDirection = null;

                var address = builder.GetAddress();
                var mapper = new PostalAddressMapper();
                var pram = mapper.ToSqlParameter("@address", address);
                Assert.AreEqual(this.StandardUsAddressLength - this.StandardStreetPreDirectionLength + 2, ((string)pram.Value).Length);
            }
        }

        [Test]
        public void UsAddress_StreetNameBlank_GoodParameter()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.Json);

                var builder = PostalAddressMapperUnitTests.PopulateGoodUsAddressData();
                builder.StreetName = null;

                var address = builder.GetAddress();
                var mapper = new PostalAddressMapper();
                var pram = mapper.ToSqlParameter("@address", address);
                Assert.AreEqual(this.StandardUsAddressLength - this.StandardStreetNameLength + 2, ((string)pram.Value).Length);
            }
        }

        [Test]
        public void UsAddress_StreetSuffixBlank_GoodParameter()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.Json);

                var builder = PostalAddressMapperUnitTests.PopulateGoodUsAddressData();
                builder.StreetSuffix = null;

                var address = builder.GetAddress();
                var mapper = new PostalAddressMapper();
                var pram = mapper.ToSqlParameter("@address", address);
                Assert.AreEqual(this.StandardUsAddressLength - this.StandardStreetSuffixLength + 2, ((string)pram.Value).Length);
            }
        }

        [Test]
        public void UsAddress_UnitTypeBlank_GoodParameter()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.Json);

                var builder = PostalAddressMapperUnitTests.PopulateGoodUsAddressData();
                builder.UnitType = null;

                var address = builder.GetAddress();
                var mapper = new PostalAddressMapper();
                var pram = mapper.ToSqlParameter("@address", address);
                Assert.AreEqual(this.StandardUsAddressLength - this.StandardUnitTypeLength + 2, ((string)pram.Value).Length);
            }
        }

        [Test]
        public void UsAddress_UnitIdentifierBlank_GoodParameter()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.Json);

                var builder = PostalAddressMapperUnitTests.PopulateGoodUsAddressData();
                builder.UnitIdentifier = null;

                var address = builder.GetAddress();
                var mapper = new PostalAddressMapper();
                var pram = mapper.ToSqlParameter("@address", address);
                Assert.AreEqual(this.StandardUsAddressLength - this.StandardUnitIdentifierLength + 2, ((string)pram.Value).Length);
            }
        }

        [Test]
        public void UsParsedAddress_MissingField_DeserializesAsNull()
        {
            string json = "{\"AddressType\":\"UsNormal\",\"StreetAddress\":null,\"City\":\"Costa Mesa\",\"State\":\"CA\",\"PostalCode\":\"92646\",\"CountryCode\":\"USA\",\"AddressNumber\":\"1600\",\"StreetPostDirection\":\"Southwest\",\"StreetSuffix\":\"Ave\",\"StreetName\":\"Sunflower\",\"UnitType\":\"Suite\",\"UnitIdentifier\":\"100\"}";

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.Json);

                var mapper = new PostalAddressMapper();
                var recovered = mapper.Create(json);
                Assert.AreEqual(typeof(UnitedStatesPostalAddress), recovered.GetType());

                var address = (UnitedStatesPostalAddress)recovered;
                Assert.IsNull(address.StreetPreDirection);
            }
        }

        [Test]
        public void UsParsedAddress_ExtraField_IgnoredOnDeserialization()
        {
            string json = "{\"AddressType\":\"UsNormal\",\"StreetAddress\":null,\"City\":\"Costa Mesa\",\"State\":\"CA\",\"PostalCode\":\"92646\",\"CountryCode\":\"USA\",\"AddressNumber\":\"1600\",\"StreetPreDirection\":\"East\",\"StreetPostDirection\":\"Southwest\",\"Extra\":\"Fake Field\",\"StreetSuffix\":\"Ave\",\"StreetName\":\"Sunflower\",\"UnitType\":\"Suite\",\"UnitIdentifier\":\"100\"}";

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.Json);

                var mapper = new PostalAddressMapper();
                var recovered = mapper.Create(json);
                Assert.AreEqual(typeof(UnitedStatesPostalAddress), recovered.GetType());
            }
        }
    }
}
