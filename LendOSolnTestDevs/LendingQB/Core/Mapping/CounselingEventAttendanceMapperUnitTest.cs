﻿namespace LendingQB.Test.Developers.LendingQB.Core.Mapping
{
    using global::LendingQB.Core;
    using global::LendingQB.Core.Data;
    using global::LendingQB.Core.Mapping;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;
    using NSubstitute;
    using NUnit.Framework;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public class CounselingEventAttendanceMapperUnitTest
    {
        private static readonly DataObjectIdentifier<DataObjectKind.Consumer, Guid> ConsumerId = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(Guid.Empty);
        private static readonly DataObjectIdentifier<DataObjectKind.CounselingEvent, Guid> CounselingEventId = DataObjectIdentifier<DataObjectKind.CounselingEvent, Guid>.Create(new Guid("11111111-1111-1111-1111-111111111111"));
        private static readonly DataObjectIdentifier<DataObjectKind.CounselingEvent, Guid> CounselingEventId2 = DataObjectIdentifier<DataObjectKind.CounselingEvent, Guid>.Create(new Guid("22222222-2222-2222-2222-222222222222"));

        [Test]
        public void Save_Empty_NoException()
        {
            var driver = NSubstitute.Substitute.For<IStoredProcedureDriver>();
            var mapper = this.GetMapper(driver);
            var associations = this.GetAssociationSet();

            mapper.Save(
                Substitute.For<IDbConnection>(),
                Substitute.For<IDbTransaction>(),
                Substitute.For<ISqlParameterConverter>(),
                associations);

            driver.DidNotReceiveWithAnyArgs().ExecuteNonQuery(null, null, StoredProcedureName.Invalid, null);
        }

        private CounselingEventAttendanceMapper GetMapper(IStoredProcedureDriver driver)
        {
            return new CounselingEventAttendanceMapper(DataObjectIdentifier.Create<DataObjectKind.Loan>(Guid.Empty), driver);
        }

        private IMutableLqbCollection<DataObjectKind.CounselingEventAttendance, Guid, CounselingEventAttendance> GetAssociationSet()
        {
            var associationSet = LqbCollection<DataObjectKind.CounselingEventAttendance, Guid, CounselingEventAttendance>.Create(
                Name.Create("CounselingEventAttendance").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.CounselingEventAttendance>(),
                null);

            associationSet.BeginTrackingChanges();

            return associationSet;
        }
    }
}
