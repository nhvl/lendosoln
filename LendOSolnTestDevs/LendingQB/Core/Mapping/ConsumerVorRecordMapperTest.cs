﻿namespace LendingQB.Test.Developers.LendingQB.Core.Mapping
{
    using System;
    using System.Data;
    using System.Data.Common;
    using Fakes;
    using LendersOffice.Constants;
    using LendersOffice.Drivers.SqlServerDB;
    using LendersOffice.Security;
    using global::DataAccess;
    using global::LendingQB.Core;
    using global::LendingQB.Core.Data;
    using global::LendingQB.Core.Mapping;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;
    using NUnit.Framework;
    using Utils;

    [TestFixture]
    [Category(CategoryConstants.IntegrationTest)]
    public sealed class ConsumerVorRecordMapperTest
    {
        private const int MaxFirstCardinality = -1;
        private const int MaxSecondCardinality = -1;

        private DbConnection connection;
        private IDbTransaction transaction;
        private IStoredProcedureDriver storedProcedureDriver;

        private Guid loanId;
        private Guid appId;
        private Guid borrowerId;
        private AbstractUserPrincipal principal;

        [TestFixtureSetUp]
        public void FixtureSetup()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                this.CreateLoan();

                var factory = new StoredProcedureDriverFactory();
                this.storedProcedureDriver = factory.Create(TimeoutInSeconds.Default);
            }
        }

        [SetUp]
        public void MethodSetUp()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                this.connection = DbConnectionInfo.GetConnectionInfo(this.principal.BrokerId).GetConnection();
                this.connection.Open();
                this.transaction = connection.BeginTransaction();
            }
        }

        [TearDown]
        public void MethodTearDown()
        {
            this.transaction.Rollback();
            this.transaction.Dispose();
            this.connection.Close();
        }

        [Test]
        public void Save_Populated()
        {
            var loanIdentifier = DataObjectIdentifier<DataObjectKind.Loan, Guid>.Create(this.loanId);
            var brokerIdentifier = DataObjectIdentifier<DataObjectKind.ClientCompany, Guid>.Create(this.principal.BrokerId);
            var borrowerIdentifier = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(this.borrowerId);
            var appIdentifier = DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>.Create(this.appId);

            var vor = VorRecordCollectionMapperTest.GetDefaultTestRecord();
            IMutableLqbCollection<DataObjectKind.VorRecord, Guid, VorRecord> collectionV = LqbCollection<DataObjectKind.VorRecord, Guid, VorRecord>.Create(
                Name.Create("VorRecord").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.VorRecord>());
            collectionV.BeginTrackingChanges();
            var vorId = collectionV.Add(vor);
            Assert.AreNotEqual(Guid.Empty, vorId.Value);

            var association = new ConsumerVorRecordAssociation(borrowerIdentifier, vorId, appIdentifier);
            association.IsPrimary = true;
            IMutableLqbAssociationSet<DataObjectKind.ConsumerVorRecordAssociation, Guid, ConsumerVorRecordAssociation> associationSet = LqbAssociationSet<DataObjectKind.ConsumerVorRecordAssociation, Guid, ConsumerVorRecordAssociation>.Create(
                Name.Create("Test").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.ConsumerVorRecordAssociation>(), MaxFirstCardinality, MaxSecondCardinality);
            associationSet.BeginTrackingChanges();
            var assocId = associationSet.Add(association);
            Assert.AreNotEqual(Guid.Empty, assocId.Value);

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.Json);

                // save data
                var mapperV = new VorRecordCollectionMapper(
                    loanIdentifier,
                    brokerIdentifier,
                    this.storedProcedureDriver);
                VorRecordCollectionMapperTest.Save(mapperV, collectionV, this.connection, this.transaction);

                var mapperA = new ConsumerVorRecordMapper(
                    loanIdentifier,
                    this.storedProcedureDriver);
                SaveAssociationSet(mapperA, associationSet, this.connection, this.transaction);

                // re-load
                associationSet = LoadAssociationSet(mapperA, this.connection, this.transaction);
                var loaded = associationSet[assocId];

                // check field values
                Assert.AreEqual(borrowerIdentifier, loaded.ConsumerId);
                Assert.AreEqual(vorId, loaded.VorRecordId);
            }
        }

        [Test]
        public void DeleteTest()
        {
            var loanIdentifier = DataObjectIdentifier<DataObjectKind.Loan, Guid>.Create(this.loanId);
            var brokerIdentifier = DataObjectIdentifier<DataObjectKind.ClientCompany, Guid>.Create(this.principal.BrokerId);
            var borrowerIdentifier = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(this.borrowerId);
            var appIdentifier = DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>.Create(this.appId);

            var vorrec1 = VorRecordCollectionMapperTest.GetDefaultTestRecord();
            var vorrec2 = VorRecordCollectionMapperTest.GetDefaultTestRecord();
            IMutableLqbCollection<DataObjectKind.VorRecord, Guid, VorRecord> collectionV = LqbCollection<DataObjectKind.VorRecord, Guid, VorRecord>.Create(
                Name.Create("VorRecord").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.VorRecord>());
            collectionV.BeginTrackingChanges();
            var firstVorRecordId = collectionV.Add(vorrec1);
            var secondVorRecordId = collectionV.Add(vorrec1);

            var association1 = new ConsumerVorRecordAssociation(borrowerIdentifier, firstVorRecordId, appIdentifier);
            association1.IsPrimary = true;
            var association2 = new ConsumerVorRecordAssociation(borrowerIdentifier, secondVorRecordId, appIdentifier);
            association2.IsPrimary = true;
            IMutableLqbAssociationSet<DataObjectKind.ConsumerVorRecordAssociation, Guid, ConsumerVorRecordAssociation> associationSet = LqbAssociationSet<DataObjectKind.ConsumerVorRecordAssociation, Guid, ConsumerVorRecordAssociation>.Create(
                Name.Create("Test").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.ConsumerVorRecordAssociation>(), MaxFirstCardinality, MaxSecondCardinality);
            associationSet.BeginTrackingChanges();
            var firstAssocId = associationSet.Add(association1);
            var secondAssocId = associationSet.Add(association2);

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.Json);

                var mapperV = new VorRecordCollectionMapper(
                    loanIdentifier,
                    brokerIdentifier,
                    this.storedProcedureDriver);
                VorRecordCollectionMapperTest.Save(mapperV, collectionV, this.connection, this.transaction);

                var mapper = new ConsumerVorRecordMapper(
                    loanIdentifier,
                    this.storedProcedureDriver);
                SaveAssociationSet(mapper, associationSet, this.connection, this.transaction);

                associationSet = LoadAssociationSet(mapper, this.connection, this.transaction);
                Assert.AreEqual(2, associationSet.Count);

                associationSet.BeginTrackingChanges();
                associationSet.Remove(firstAssocId);
                SaveAssociationSet(mapper, associationSet, this.connection, this.transaction);

                associationSet = LoadAssociationSet(mapper, this.connection, this.transaction);
                Assert.AreEqual(1, associationSet.Count);
            }
        }

        internal static IMutableLqbAssociationSet<DataObjectKind.ConsumerVorRecordAssociation, Guid, ConsumerVorRecordAssociation> LoadAssociationSet(
            ILqbCollectionSqlMapper<DataObjectKind.ConsumerVorRecordAssociation, Guid, ConsumerVorRecordAssociation> mapper,
            IDbConnection connection, IDbTransaction transaction)
        {
            return mapper.Load(
                connection,
                transaction,
                Name.Create("Test").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.ConsumerVorRecordAssociation>(),
                null,
                new DataReaderConverter()) as IMutableLqbAssociationSet<DataObjectKind.ConsumerVorRecordAssociation, Guid, ConsumerVorRecordAssociation>;
        }

        internal static void SaveAssociationSet(
            ILqbCollectionSqlMapper<DataObjectKind.ConsumerVorRecordAssociation, Guid, ConsumerVorRecordAssociation> mapper,
            IMutableLqbAssociationSet<DataObjectKind.ConsumerVorRecordAssociation, Guid, ConsumerVorRecordAssociation> associationSet,
            IDbConnection connection, IDbTransaction transaction)
        {
            mapper.Save(
                connection,
                transaction,
                new SqlParameterConverter(),
                associationSet);
        }

        // Create a new test loan and return <brokerid, loanid>
        private void CreateLoan()
        {
            this.principal = LoginTools.LoginAs(TestAccountType.LoTest001);

            var dataLoan = FakePageData.Create(appCount: 1);
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            var dataApp = dataLoan.GetAppData(0);
            dataApp.aBFirstNm = "Frosty";
            dataApp.aBLastNm = "Snowman";
            dataApp.aBSsn = "555-55-5555";
            dataLoan.Save();

            this.loanId = dataLoan.sLId;
            this.appId = dataApp.aAppId;
            this.borrowerId = dataApp.aBConsumerId;
        }
    }
}
