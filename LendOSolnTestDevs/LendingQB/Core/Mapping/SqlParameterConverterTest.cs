﻿namespace LendingQB.Test.Developers.LendingQB.Core.Mapping
{
    using System;
    using DataAccess;
    using global::DataAccess;
    using global::LendingQB.Core.Mapping;
    using LqbGrammar.DataTypes;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public class SqlParameterConverterTest
    {
        [Test]
        public void ToSqlParameter_IntBasedEnum_ReturnsParameterWithExpectedValue()
        {
            var converter = new SqlParameterConverter();
            var enumValue = E_aAddrMailSourceT.Other;

            var parameter = converter.ToSqlParameter("Test", enumValue);

            Assert.AreEqual(2, parameter.Value);
        }

        [Test]
        public void ToSqlParameter_ByteBasedEnum_ReturnsParameterWithExpectedValue()
        {
            var converter = new SqlParameterConverter();
            var enumValue = E_ConsumerResponseStatusT.PendingResponse;

            var parameter = converter.ToSqlParameter("Test", enumValue);

            Assert.AreEqual(1, parameter.Value);
        }

        [Test]
        public void ToSqlParameter_NullableIntBasedEnumField_ReturnsParameterWithExpectedValue()
        {
            var converter = new SqlParameterConverter();
            E_aAddrMailSourceT? enumValue = E_aAddrMailSourceT.PresentAddress;

            var parameter = converter.ToSqlParameter("Test", enumValue);

            Assert.AreEqual(0, parameter.Value);
        }

        [Test]
        public void ToSqlParameter_NullEnumField_ReturnsParameterWithExpectedValue()
        {
            var converter = new SqlParameterConverter();
            E_aAddrMailSourceT? enumValue = null;

            var parameter = converter.ToSqlParameter("Test", enumValue);

            Assert.AreEqual(DBNull.Value, parameter.Value);
        }

        [Test]
        public void ToSqlParameter_StreetAddress_ReturnsParameterWithExpectedValue()
        {
            var converter = new SqlParameterConverter();
            StreetAddress address = StreetAddress.Create("123 ABCD").Value;

            var parameter = converter.ToSqlParameter("Test", address);

            Assert.AreEqual("123 ABCD", parameter.Value);
        }

        [Test]
        public void ToSqlParameter_NullableStreetAddress_ReturnsParameterWithExpectedValue()
        {
            var converter = new SqlParameterConverter();
            StreetAddress? address = StreetAddress.Create("123 ABCD");

            var parameter = converter.ToSqlParameter("Test", address);

            Assert.AreEqual("123 ABCD", parameter.Value);
        }

        [Test]
        public void ToSqlParameter_NullStreetAddress_ReturnsParameterWithExpectedValue()
        {
            var converter = new SqlParameterConverter();
            StreetAddress? address = null;

            var parameter = converter.ToSqlParameter("Test", address);

            Assert.AreEqual(DBNull.Value, parameter.Value);
        }

        [Test]
        public void ToSqlParameter_Money_ReturnsParameterWithExpectedValue()
        {
            var converter = new SqlParameterConverter();
            Money? money = Money.Create(1234m);

            var parameter = converter.ToSqlParameter("Test", money);

            Assert.AreEqual(1234m, (decimal)parameter.Value);
        }

        [Test]
        public void ToSqlParameter_PersonNameTooLong_ThrowsException()
        {
            var converter = new SqlParameterConverter();
            PersonName? name = PersonName.Create("Really Really Really Really Really Really Really Really Really Really Really Really Really Really Really Really Really Really Really Really Really Really Really Really Really Long Person Name");

            Assert.Throws<DataTruncationException>(() => converter.ToSqlParameter("Test", name));
        }
    }
}
