﻿namespace LendingQB.Test.Developers.LendingQB.Core.Mapping
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using global::LendingQB.Core.Mapping;
    using LqbGrammar.DataTypes;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public class AliasesFieldMapperTest
    {
        [Test]
        public void ToSqlParameter_NullInput_ReturnsParameterDbNull()
        {
            IEnumerable<PersonName> aliases = null;
            var mapper = new ConsumerAliasesMapper();

            var parameter = mapper.ToSqlParameter("Aliases", aliases);

            Assert.AreEqual(DBNull.Value, parameter.Value);
        }

        [Test]
        public void ToSqlParameter_EmptyInput_ReturnsParameterWithStringValue()
        {
            using (var helper = new Utils.FoolHelper())
            {
                FOOL.FakeConfigurationQueryFactory.RegisterFake(helper);

                IEnumerable<PersonName> aliases = Enumerable.Empty<PersonName>();
                var mapper = new ConsumerAliasesMapper();

                var parameter = mapper.ToSqlParameter("Aliases", aliases);

                Assert.AreEqual(typeof(string), parameter.Value.GetType());
            }
        }

        [Test]
        public void Create_AfterCallingToSqlParameter_RoundTripsData()
        {
            using (var helper = new Utils.FoolHelper())
            {
                FOOL.FakeConfigurationQueryFactory.RegisterFake(helper);

                var names = new[]
                {
                    PersonName.Create("Geoffrey").Value,
                    PersonName.Create("Geoff").Value,
                    PersonName.Create("G-off").Value,
                };
                var mapper = new ConsumerAliasesMapper();

                var parameter = mapper.ToSqlParameter("Aliases", names);
                var roundTripNames = mapper.Create(parameter.Value);

                CollectionAssert.AreEqual(names, roundTripNames);
            }
        }
    }
}
