﻿namespace LendingQB.Test.Developers.LendingQB.Core.Mapping
{
    using System;
    using System.Data.Common;
    using LendersOffice.Security;
    using NUnit.Framework;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;
    using LendersOffice.Drivers.SqlServerDB;
    using Utils;
    using System.Data;
    using global::LendingQB.Core.Mapping;
    using global::LendingQB.Core;
    using global::LendingQB.Core.Data;
    using global::DataAccess;
    using System.Linq;

    [TestFixture]
    [Category(CategoryConstants.IntegrationTest)]
    public class HousingHistoryEntryCollectionMapperTest
    {
        private AbstractUserPrincipal principal;
        private DataObjectIdentifier<DataObjectKind.Loan, Guid> loanId;
        private DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> legacyAppId1;
        private DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> legacyAppId2;
        private DataObjectIdentifier<DataObjectKind.Consumer, Guid> consumerId1;
        private DataObjectIdentifier<DataObjectKind.Consumer, Guid> consumerId2;
        private DataObjectIdentifier<DataObjectKind.ClientCompany, Guid> brokerId;
        private IStoredProcedureDriver storedProcedureDriver;
        private IDbConnection connection;
        private IDbTransaction transaction;

        [TestFixtureSetUp]
        public void FixtureSetUp()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                this.principal = LoginTools.LoginAs(TestAccountType.LoTest001);
                this.brokerId = DataObjectIdentifier<DataObjectKind.ClientCompany, Guid>.Create(principal.BrokerId);
                var creator = CLoanFileCreator.GetCreator(
                    this.principal,
                    LendersOffice.Audit.E_LoanCreationSource.UserCreateFromBlank);
                this.loanId = DataObjectIdentifier<DataObjectKind.Loan, Guid>.Create(creator.CreateBlankUladLoanFile());
                CPageData.AddBlankLegacyApplication(this.loanId, withSecurityBypass: true);
                var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                    this.loanId.Value,
                    typeof(HousingHistoryEntryCollectionMapperTest));
                loan.InitSave();
                this.legacyAppId1 = loan.LegacyApplications.First().Key;
                this.legacyAppId2 = loan.LegacyApplications.ElementAt(1).Key;
                this.consumerId1 = loan.Consumers.First().Key;
                this.consumerId2 = loan.Consumers.ElementAt(1).Key;

                var factory = new StoredProcedureDriverFactory();
                this.storedProcedureDriver = factory.Create(TimeoutInSeconds.Default);
            }
        }

        [TestFixtureTearDown]
        public void FixtureTearDown()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                Tools.DeclareLoanFileInvalid(
                    this.principal,
                    this.loanId.Value,
                    sendNotifications: false,
                    generateLinkLoanMsgEvent: false);
            }
        }

        [SetUp]
        public void SetUp()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                this.connection = DbConnectionInfo.GetConnectionInfo(this.brokerId.Value).GetConnection();
                this.connection.Open();
                this.transaction = connection.BeginTransaction();
            }
        }

        [TearDown]
        public void TearDown()
        {
            this.transaction.Rollback();
            this.transaction.Dispose();
            this.connection.Dispose();
        }

        [Test]
        public void Load_AssumingNoDatabaseIssues_Succeeds()
        {
            var mapper = new HousingHistoryEntryCollectionMapper(
                this.loanId,
                this.brokerId,
                new PostalAddressMapper(),
                this.storedProcedureDriver);

            this.LoadCollection(mapper);
        }

        [Test]
        public void Save_NewRecord_PersistsEnteredDataPoints()
        {
            var mapper = new HousingHistoryEntryCollectionMapper(
                this.loanId,
                this.brokerId,
                new PostalAddressMapper(),
                this.storedProcedureDriver);
            IMutableLqbCollection<DataObjectKind.HousingHistoryEntry, Guid, HousingHistoryEntry> collection = LqbCollection<DataObjectKind.HousingHistoryEntry, Guid, HousingHistoryEntry>.Create(
                Name.Create("Test").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.HousingHistoryEntry>());
            HousingHistoryEntry originalRecord = GetDefaultTestRecord();

            collection.BeginTrackingChanges();
            var id = collection.Add(originalRecord);
            this.SaveCollection(mapper, collection);
            collection = this.LoadCollection(mapper);
            var loadedRecord = collection[id];

            var expectedAddress = GetDefaultPostalAddress();
            Assert.AreEqual(expectedAddress.StreetAddress, loadedRecord.Address.StreetAddress);
            Assert.AreEqual(expectedAddress.City, loadedRecord.Address.City);
            Assert.AreEqual(expectedAddress.State, loadedRecord.Address.State);
            Assert.AreEqual(expectedAddress.PostalCode, loadedRecord.Address.PostalCode);
            Assert.AreEqual(ApproximateDate.CreateWithDayPrecision(2018, 2, 1), loadedRecord.EndDate);
            Assert.AreEqual(false, loadedRecord.IsPresentAddress);
            Assert.AreEqual(Money.Create(500), loadedRecord.MonthlyExpense);
            Assert.AreEqual(ResidencyType.Rent, loadedRecord.ResidencyType);
            Assert.AreEqual(ApproximateDate.CreateWithMonthPrecision(2016, 1), loadedRecord.StartDate);
            Assert.AreEqual(Count<UnitType.Month>.Create(25), loadedRecord.TimeAtAddress);
            Assert.AreEqual(this.consumerId1, loadedRecord.ConsumerId);
            Assert.AreEqual(this.legacyAppId1, loadedRecord.LegacyAppId);
        }

        [Test]
        public void Save_WithConsumerAndLegacyAppAssigned_PersistsEmptyRecord()
        {
            var mapper = new HousingHistoryEntryCollectionMapper(
                this.loanId,
                this.brokerId,
                new PostalAddressMapper(),
                this.storedProcedureDriver);
            IMutableLqbCollection<DataObjectKind.HousingHistoryEntry, Guid, HousingHistoryEntry> collection = LqbCollection<DataObjectKind.HousingHistoryEntry, Guid, HousingHistoryEntry>.Create(
                Name.Create("Test").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.HousingHistoryEntry>());
            var originalRecord = new HousingHistoryEntry();
            originalRecord.SetConsumer(this.consumerId1, this.legacyAppId1);

            collection.BeginTrackingChanges();
            var id = collection.Add(originalRecord);
            this.SaveCollection(mapper, collection);
            collection = this.LoadCollection(mapper);
            var loadedRecord = collection[id];

            Assert.IsNull(loadedRecord.Address);
            Assert.IsNull(loadedRecord.EndDate);
            Assert.IsNull(loadedRecord.IsPresentAddress);
            Assert.IsNull(loadedRecord.MonthlyExpense);
            Assert.IsNull(loadedRecord.ResidencyType);
            Assert.IsNull(loadedRecord.StartDate);
            Assert.IsNull(loadedRecord.TimeAtAddress);
            Assert.AreEqual(this.consumerId1, loadedRecord.ConsumerId);
            Assert.AreEqual(this.legacyAppId1, loadedRecord.LegacyAppId);
        }

        [Test]
        public void Save_UpdateExistingRecord_PersistsUpdatedDataPoints()
        {
            var mapper = new HousingHistoryEntryCollectionMapper(
                this.loanId,
                this.brokerId,
                new PostalAddressMapper(),
                this.storedProcedureDriver);
            IMutableLqbCollection<DataObjectKind.HousingHistoryEntry, Guid, HousingHistoryEntry> collection = LqbCollection<DataObjectKind.HousingHistoryEntry, Guid, HousingHistoryEntry>.Create(
                Name.Create("Test").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.HousingHistoryEntry>());
            var originalRecord = GetDefaultTestRecord();

            collection.BeginTrackingChanges();
            var id = collection.Add(originalRecord);
            this.SaveCollection(mapper, collection);

            collection = this.LoadCollection(mapper);
            collection.BeginTrackingChanges();
            var loadedRecord = collection[id];

            var updatedAddress = GetUpdatedPostalAddress();
            loadedRecord.Address = updatedAddress;
            loadedRecord.EndDate = null;
            loadedRecord.IsPresentAddress = true;
            loadedRecord.MonthlyExpense = Money.Create(0);
            loadedRecord.ResidencyType = ResidencyType.LivingRentFree;
            loadedRecord.StartDate = ApproximateDate.CreateWithYearPrecision(2010);
            loadedRecord.TimeAtAddress = Count<UnitType.Month>.Create(108);
            loadedRecord.SetConsumer(this.consumerId2, this.legacyAppId2);

            this.SaveCollection(mapper, collection);

            collection = this.LoadCollection(mapper);
            var updatedRecord = collection[id];

            var expectedAddress = updatedAddress;
            Assert.AreEqual(expectedAddress.StreetAddress, loadedRecord.Address.StreetAddress);
            Assert.AreEqual(expectedAddress.City, loadedRecord.Address.City);
            Assert.AreEqual(expectedAddress.State, loadedRecord.Address.State);
            Assert.AreEqual(expectedAddress.PostalCode, loadedRecord.Address.PostalCode);
            Assert.AreEqual(null, loadedRecord.EndDate);
            Assert.AreEqual(true, loadedRecord.IsPresentAddress);
            Assert.AreEqual(Money.Create(0), loadedRecord.MonthlyExpense);
            Assert.AreEqual(ResidencyType.LivingRentFree, loadedRecord.ResidencyType);
            Assert.AreEqual(ApproximateDate.CreateWithYearPrecision(2010), loadedRecord.StartDate);
            Assert.AreEqual(Count<UnitType.Month>.Create(108), loadedRecord.TimeAtAddress);
            Assert.AreEqual(this.consumerId2, loadedRecord.ConsumerId);
            Assert.AreEqual(this.legacyAppId2, loadedRecord.LegacyAppId);
        }

        private static PostalAddress GetUpdatedPostalAddress()
        {
            var builder = new UnitedStatesPostalAddress.Builder();
            builder.UnparsedStreetAddress = StreetAddress.Create("1 Infinite Loop");
            builder.City = City.Create("Cupertino").Value;
            builder.UsState = UnitedStatesPostalState.Create("CA").Value;
            builder.Zipcode = Zipcode.Create("95014").Value;
            return builder.GetAddress();
        }

        [Test]
        public void Save_DeleteExistingRecord_RemovesRecordFromDatabase()
        {
            var mapper = new HousingHistoryEntryCollectionMapper(
                this.loanId,
                this.brokerId,
                new PostalAddressMapper(),
                this.storedProcedureDriver);
            IMutableLqbCollection<DataObjectKind.HousingHistoryEntry, Guid, HousingHistoryEntry> collection = LqbCollection<DataObjectKind.HousingHistoryEntry, Guid, HousingHistoryEntry>.Create(
                Name.Create("Test").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.HousingHistoryEntry>());
            var record = this.GetDefaultTestRecord();

            collection.BeginTrackingChanges();
            var id = collection.Add(record);
            this.SaveCollection(mapper, collection);

            collection = this.LoadCollection(mapper);
            collection.BeginTrackingChanges();
            collection.Remove(id);
            this.SaveCollection(mapper, collection);

            collection = this.LoadCollection(mapper);

            Assert.AreEqual(false, collection.ContainsKey(id));
        }

        private HousingHistoryEntry GetDefaultTestRecord()
        {
            var record = new HousingHistoryEntry();

            record.Address = GetDefaultPostalAddress();
            record.EndDate = ApproximateDate.CreateWithDayPrecision(2018, 2, 1);
            record.IsPresentAddress = false;
            record.MonthlyExpense = Money.Create(500);
            record.ResidencyType = ResidencyType.Rent;
            record.StartDate = ApproximateDate.CreateWithMonthPrecision(2016, 1);
            record.TimeAtAddress = Count<UnitType.Month>.Create(25);
            record.SetConsumer(this.consumerId1, this.legacyAppId1);

            return record;
        }

        private static UnitedStatesPostalAddress GetDefaultPostalAddress()
        {
            var builder = new UnitedStatesPostalAddress.Builder();
            builder.UnparsedStreetAddress = StreetAddress.Create("123 Main St. Apt 8E");
            builder.City = City.Create("Costa Mesa").Value;
            builder.UsState = UnitedStatesPostalState.Create("CA").Value;
            builder.Zipcode = Zipcode.Create("92626").Value;
            return builder.GetAddress();
        }

        private IMutableLqbCollection<DataObjectKind.HousingHistoryEntry, Guid, HousingHistoryEntry> LoadCollection(
            ILqbCollectionSqlMapper<DataObjectKind.HousingHistoryEntry, Guid, HousingHistoryEntry> mapper)
        {
            return Load(mapper, this.connection, this.transaction);
        }

        internal static IMutableLqbCollection<DataObjectKind.HousingHistoryEntry, Guid, HousingHistoryEntry> Load(
            ILqbCollectionSqlMapper<DataObjectKind.HousingHistoryEntry, Guid, HousingHistoryEntry> mapper,
            IDbConnection connection, IDbTransaction transaction)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.ConfigurationQuery);

                return mapper.Load(
                    connection,
                    transaction,
                    Name.Create("Test").Value,
                    new GuidDataObjectIdentifierFactory<DataObjectKind.HousingHistoryEntry>(),
                    null,
                    new DataReaderConverter());
            }
        }

        private void SaveCollection(
            ILqbCollectionSqlMapper<DataObjectKind.HousingHistoryEntry, Guid, HousingHistoryEntry> mapper,
            IMutableLqbCollection<DataObjectKind.HousingHistoryEntry, Guid, HousingHistoryEntry> collection)
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.ConfigurationQuery);

                mapper.Save(
                    this.connection,
                    this.transaction,
                    new SqlParameterConverter(),
                    collection);
            }
        }

        internal static void Save(
            ILqbCollectionSqlMapper<DataObjectKind.HousingHistoryEntry, Guid, HousingHistoryEntry> mapper,
            IMutableLqbCollection<DataObjectKind.HousingHistoryEntry, Guid, HousingHistoryEntry> collection,
            IDbConnection connection, IDbTransaction transaction)
        {
            mapper.Save(
                connection,
                transaction,
                new SqlParameterConverter(),
                collection);
        }

        private DbConnection GetConnection()
        {
            return DbConnectionInfo.GetConnectionInfo(this.brokerId.Value).GetConnection();
        }
    }
}
