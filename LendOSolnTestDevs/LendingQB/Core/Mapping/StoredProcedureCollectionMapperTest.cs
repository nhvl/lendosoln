﻿namespace LendingQB.Test.Developers.LendingQB.Core.Mapping
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;
    using DataAccess;
    using global::DataAccess;
    using global::LendingQB.Core;
    using global::LendingQB.Core.Mapping;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;
    using NSubstitute;
    using NUnit.Framework;

    /// <summary>
    /// Provides unit tests to exercise the StoredProcedureCollectionMapper abstract class.
    /// It is verifying a lot of implementation details, and if maintaining it becomes a
    /// problem we can delete the problematic tests.
    /// </summary>
    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public class StoredProcedureCollectionMapperTest
    {
        [Test]
        public void Load_Always_CallsLoadStoredProcedure()
        {
            var driver = Substitute.For<IStoredProcedureDriver>();
            var mapper = new TestMapper(driver);

            var collection = mapper.Load(
                Substitute.For<IDbConnection>(),
                Substitute.For<IDbTransaction>(),
                Name.Create("Test").Value,
                new GuidDataObjectIdentifierFactory<TestObjectKind>(),
                Substitute.For<ILqbCollectionObserver<TestObjectKind, Guid, ITestObject>>(),
                Substitute.For<IDataReaderConverter>());

            driver.Received()
                .ExecuteReader(
                    Arg.Any<IDbConnection>(),
                    Arg.Any<IDbTransaction>(),
                    StoredProcedureName.Create("LoadAllRecords").Value,
                    Arg.Any<IEnumerable<SqlParameter>>());
        }

        [Test]
        public void Load_Always_ProvidesExpectedSqlParameters()
        {
            IEnumerable<SqlParameter> parametersUsed = null;
            var driver = Substitute.For<IStoredProcedureDriver>();
            driver.ExecuteReader(
                    Arg.Any<IDbConnection>(),
                    Arg.Any<IDbTransaction>(),
                    StoredProcedureName.Create("LoadAllRecords").Value,
                    Arg.Do<IEnumerable<SqlParameter>>(parameters => parametersUsed = parameters));
            var mapper = new TestMapper(driver);

            var collection = mapper.Load(
                Substitute.For<IDbConnection>(),
                Substitute.For<IDbTransaction>(),
                Name.Create("Test").Value,
                new GuidDataObjectIdentifierFactory<TestObjectKind>(),
                Substitute.For<ILqbCollectionObserver<TestObjectKind, Guid, ITestObject>>(),
                Substitute.For<IDataReaderConverter>());

            Assert.AreEqual(1, parametersUsed.Count(p => p.ParameterName == "LoanId"));
            Assert.AreEqual(new Guid("11111111-1111-1111-1111-111111111111"), parametersUsed.Single(p => p.ParameterName == "LoanId").Value);
            Assert.AreEqual(1, parametersUsed.Count(p => p.ParameterName == "BrokerId"));
            Assert.AreEqual(new Guid("22222222-2222-2222-2222-222222222222"), parametersUsed.Single(p => p.ParameterName == "BrokerId").Value);
        }

        [Test]
        public void Load_Always_ReturnsCollectionThatUsesObserver()
        {
            var driver = Substitute.For<IStoredProcedureDriver>();
            var mapper = new TestMapper(driver);
            var observer = Substitute.For<ILqbCollectionObserver<TestObjectKind, Guid, ITestObject>>();
            var record = Substitute.For<ITestObject>();

            var collection = mapper.Load(
                Substitute.For<IDbConnection>(),
                Substitute.For<IDbTransaction>(),
                Name.Create("Test").Value,
                new GuidDataObjectIdentifierFactory<TestObjectKind>(),
                observer,
                Substitute.For<IDataReaderConverter>());
            var id = collection.Add(record);

            observer.Received(1).NotifyAdd(id, record);
        }

        [Test]
        public void Load_StoredProcedureReturnsThreeRecords_ReturnsCollectionWithThreeRecords()
        {
            var driver = Substitute.For<IStoredProcedureDriver>();
            var reader = Substitute.For<IDataReader>();
            reader["TestObjectId"].Returns(Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid());
            reader.Read().Returns(true, true, true, false);
            driver.ExecuteReader(Arg.Any<IDbConnection>(), Arg.Any<IDbTransaction>(), StoredProcedureName.Create("LoadAllRecords").Value, Arg.Any<IEnumerable<SqlParameter>>())
                .Returns(reader);
            var mapper = new TestMapper(driver);

            var collection = mapper.Load(
                Substitute.For<IDbConnection>(),
                Substitute.For<IDbTransaction>(),
                Name.Create("Test").Value,
                new GuidDataObjectIdentifierFactory<TestObjectKind>(),
                Substitute.For<ILqbCollectionObserver<TestObjectKind, Guid, ITestObject>>(),
                Substitute.For<IDataReaderConverter>());

            Assert.AreEqual(3, collection.Count);
        }

        [Test]
        public void Save_TwoAddedRecords_CallsCreateStoredProcedureTwice()
        {
            var driver = Substitute.For<IStoredProcedureDriver>();
            var mapper = new TestMapper(driver);
            var collection = Substitute.For<IMutableLqbCollection<TestObjectKind, Guid, ITestObject>>();
            var addedRecords = new List<KeyValuePair<DataObjectIdentifier<TestObjectKind, Guid>, ITestObject>>()
            {
                new KeyValuePair<DataObjectIdentifier<TestObjectKind, Guid>, ITestObject>(DataObjectIdentifier<TestObjectKind, Guid>.Create(Guid.NewGuid()), Substitute.For<ITestObject>()),
                new KeyValuePair<DataObjectIdentifier<TestObjectKind, Guid>, ITestObject>(DataObjectIdentifier<TestObjectKind, Guid>.Create(Guid.NewGuid()), Substitute.For<ITestObject>())
            };
            collection.GetChanges()
                .Returns(new LqbCollectionChangeRecords<TestObjectKind, Guid, ITestObject>(
                    added: addedRecords,
                    updated: Enumerable.Empty<KeyValuePair<DataObjectIdentifier<TestObjectKind, Guid>, ITestObject>>(),
                    removed: Enumerable.Empty<DataObjectIdentifier<TestObjectKind, Guid>>()));

            mapper.Save(
                Substitute.For<IDbConnection>(),
                Substitute.For<IDbTransaction>(),
                Substitute.For<ISqlParameterConverter>(),
                collection);

            driver.Received(2)
                .ExecuteNonQuery(
                    Arg.Any<IDbConnection>(),
                    Arg.Any<IDbTransaction>(),
                    StoredProcedureName.Create("Create").Value,
                    Arg.Any<IEnumerable<SqlParameter>>());
        }

        [Test]
        public void Save_FiveUpdatedRecords_CallsUpdateStoredProcedureFiveTimes()
        {
            var driver = Substitute.For<IStoredProcedureDriver>();
            var mapper = new TestMapper(driver);
            var collection = Substitute.For<IMutableLqbCollection<TestObjectKind, Guid, ITestObject>>();
            var updatedRecords = new List<KeyValuePair<DataObjectIdentifier<TestObjectKind, Guid>, ITestObject>>()
            {
                new KeyValuePair<DataObjectIdentifier<TestObjectKind, Guid>, ITestObject>(DataObjectIdentifier<TestObjectKind, Guid>.Create(Guid.NewGuid()), Substitute.For<ITestObject>()),
                new KeyValuePair<DataObjectIdentifier<TestObjectKind, Guid>, ITestObject>(DataObjectIdentifier<TestObjectKind, Guid>.Create(Guid.NewGuid()), Substitute.For<ITestObject>()),
                new KeyValuePair<DataObjectIdentifier<TestObjectKind, Guid>, ITestObject>(DataObjectIdentifier<TestObjectKind, Guid>.Create(Guid.NewGuid()), Substitute.For<ITestObject>()),
                new KeyValuePair<DataObjectIdentifier<TestObjectKind, Guid>, ITestObject>(DataObjectIdentifier<TestObjectKind, Guid>.Create(Guid.NewGuid()), Substitute.For<ITestObject>()),
                new KeyValuePair<DataObjectIdentifier<TestObjectKind, Guid>, ITestObject>(DataObjectIdentifier<TestObjectKind, Guid>.Create(Guid.NewGuid()), Substitute.For<ITestObject>()),
            };
            collection.GetChanges()
                .Returns(new LqbCollectionChangeRecords<TestObjectKind, Guid, ITestObject>(
                    added: Enumerable.Empty<KeyValuePair<DataObjectIdentifier<TestObjectKind, Guid>, ITestObject>>(),
                    updated: updatedRecords,
                    removed: Enumerable.Empty<DataObjectIdentifier<TestObjectKind, Guid>>()));

            mapper.Save(
                Substitute.For<IDbConnection>(),
                Substitute.For<IDbTransaction>(),
                Substitute.For<ISqlParameterConverter>(),
                collection);

            driver.Received(5)
                .ExecuteNonQuery(
                    Arg.Any<IDbConnection>(),
                    Arg.Any<IDbTransaction>(),
                    StoredProcedureName.Create("Update").Value,
                    Arg.Any<IEnumerable<SqlParameter>>());
        }

        [Test]
        public void Save_ThreeDeletedRecords_CallsDeleteStoredProcedureThreeTimes()
        {
            var driver = Substitute.For<IStoredProcedureDriver>();
            var mapper = new TestMapper(driver);
            var collection = Substitute.For<IMutableLqbCollection<TestObjectKind, Guid, ITestObject>>();
            var removedRecords = new List<DataObjectIdentifier<TestObjectKind, Guid>>()
            {
                DataObjectIdentifier<TestObjectKind, Guid>.Create(Guid.NewGuid()),
                DataObjectIdentifier<TestObjectKind, Guid>.Create(Guid.NewGuid()),
                DataObjectIdentifier<TestObjectKind, Guid>.Create(Guid.NewGuid())
            };
            collection.GetChanges()
                .Returns(new LqbCollectionChangeRecords<TestObjectKind, Guid, ITestObject>(
                    added: Enumerable.Empty<KeyValuePair<DataObjectIdentifier<TestObjectKind, Guid>, ITestObject>>(),
                    updated: Enumerable.Empty<KeyValuePair<DataObjectIdentifier<TestObjectKind, Guid>, ITestObject>>(),
                    removed: removedRecords));

            mapper.Save(
                Substitute.For<IDbConnection>(),
                Substitute.For<IDbTransaction>(),
                Substitute.For<ISqlParameterConverter>(),
                collection);

            driver.Received(3)
                .ExecuteNonQuery(
                    Arg.Any<IDbConnection>(),
                    Arg.Any<IDbTransaction>(),
                    StoredProcedureName.Create("Delete").Value,
                    Arg.Any<IEnumerable<SqlParameter>>());
        }

        [Test]
        public void Save_SingleAddedRecord_ProvidesExpectedSqlParameters()
        {
            IEnumerable<SqlParameter> parametersUsed = null;
            var driver = Substitute.For<IStoredProcedureDriver>();
            driver.ExecuteNonQuery(
                    Arg.Any<IDbConnection>(),
                    Arg.Any<IDbTransaction>(),
                    StoredProcedureName.Create("Create").Value,
                    Arg.Do<IEnumerable<SqlParameter>>(parameters => parametersUsed = parameters));
            var mapper = new TestMapper(driver);
            var collection = Substitute.For<IMutableLqbCollection<TestObjectKind, Guid, ITestObject>>();
            var id = DataObjectIdentifier<TestObjectKind, Guid>.Create(Guid.NewGuid());
            var addedRecords = new List<KeyValuePair<DataObjectIdentifier<TestObjectKind, Guid>, ITestObject>>()
            {
                new KeyValuePair<DataObjectIdentifier<TestObjectKind, Guid>, ITestObject>(id, Substitute.For<ITestObject>()),
            };
            collection.GetChanges()
                .Returns(new LqbCollectionChangeRecords<TestObjectKind, Guid, ITestObject>(
                    added: addedRecords,
                    updated: Enumerable.Empty<KeyValuePair<DataObjectIdentifier<TestObjectKind, Guid>, ITestObject>>(),
                    removed: Enumerable.Empty<DataObjectIdentifier<TestObjectKind, Guid>>()));

            mapper.Save(
                Substitute.For<IDbConnection>(),
                Substitute.For<IDbTransaction>(),
                Substitute.For<ISqlParameterConverter>(),
                collection);

            Assert.AreEqual(1, parametersUsed.Count(p => p.ParameterName == "TestObjectId"));
            Assert.AreEqual(id.Value, parametersUsed.Single(p => p.ParameterName == "TestObjectId").Value);
            Assert.AreEqual(1, parametersUsed.Count(p => p.ParameterName == "Field"));
            Assert.AreEqual("Value", parametersUsed.Single(p => p.ParameterName == "Field").Value);
            Assert.AreEqual(1, parametersUsed.Count(p => p.ParameterName == "LoanId"));
            Assert.AreEqual(new Guid("11111111-1111-1111-1111-111111111111"), parametersUsed.Single(p => p.ParameterName == "LoanId").Value);
            Assert.AreEqual(1, parametersUsed.Count(p => p.ParameterName == "BrokerId"));
            Assert.AreEqual(new Guid("22222222-2222-2222-2222-222222222222"), parametersUsed.Single(p => p.ParameterName == "BrokerId").Value);
        }

        [Test]
        public void Save_SingleUpdatedRecord_ProvidesExpectedSqlParameters()
        {
            IEnumerable<SqlParameter> parametersUsed = null;
            var driver = Substitute.For<IStoredProcedureDriver>();
            driver.ExecuteNonQuery(
                    Arg.Any<IDbConnection>(),
                    Arg.Any<IDbTransaction>(),
                    StoredProcedureName.Create("Update").Value,
                    Arg.Do<IEnumerable<SqlParameter>>(parameters => parametersUsed = parameters));
            var mapper = new TestMapper(driver);
            var collection = Substitute.For<IMutableLqbCollection<TestObjectKind, Guid, ITestObject>>();
            var id = DataObjectIdentifier<TestObjectKind, Guid>.Create(Guid.NewGuid());
            var updatedRecords = new List<KeyValuePair<DataObjectIdentifier<TestObjectKind, Guid>, ITestObject>>()
            {
                new KeyValuePair<DataObjectIdentifier<TestObjectKind, Guid>, ITestObject>(id, Substitute.For<ITestObject>()),
            };
            collection.GetChanges()
                .Returns(new LqbCollectionChangeRecords<TestObjectKind, Guid, ITestObject>(
                    added: Enumerable.Empty<KeyValuePair<DataObjectIdentifier<TestObjectKind, Guid>, ITestObject>>(),
                    updated: updatedRecords,
                    removed: Enumerable.Empty<DataObjectIdentifier<TestObjectKind, Guid>>()));

            mapper.Save(
                Substitute.For<IDbConnection>(),
                Substitute.For<IDbTransaction>(),
                Substitute.For<ISqlParameterConverter>(),
                collection);

            Assert.AreEqual(1, parametersUsed.Count(p => p.ParameterName == "TestObjectId"));
            Assert.AreEqual(id.Value, parametersUsed.Single(p => p.ParameterName == "TestObjectId").Value);
            Assert.AreEqual(1, parametersUsed.Count(p => p.ParameterName == "Field"));
            Assert.AreEqual("Value", parametersUsed.Single(p => p.ParameterName == "Field").Value);
            Assert.AreEqual(1, parametersUsed.Count(p => p.ParameterName == "LoanId"));
            Assert.AreEqual(new Guid("11111111-1111-1111-1111-111111111111"), parametersUsed.Single(p => p.ParameterName == "LoanId").Value);
            Assert.AreEqual(1, parametersUsed.Count(p => p.ParameterName == "BrokerId"));
            Assert.AreEqual(new Guid("22222222-2222-2222-2222-222222222222"), parametersUsed.Single(p => p.ParameterName == "BrokerId").Value);
        }

        [Test]
        public void Save_SingleRemovedRecord_ProvidesExpectedSqlParameters()
        {
            IEnumerable<SqlParameter> parametersUsed = null;
            var driver = Substitute.For<IStoredProcedureDriver>();
            driver.ExecuteNonQuery(
                    Arg.Any<IDbConnection>(),
                    Arg.Any<IDbTransaction>(),
                    StoredProcedureName.Create("Delete").Value,
                    Arg.Do<IEnumerable<SqlParameter>>(parameters => parametersUsed = parameters));
            var mapper = new TestMapper(driver);
            var collection = Substitute.For<IMutableLqbCollection<TestObjectKind, Guid, ITestObject>>();
            var id = DataObjectIdentifier<TestObjectKind, Guid>.Create(Guid.NewGuid());
            var removedRecords = new[] { id };
            collection.GetChanges()
                .Returns(new LqbCollectionChangeRecords<TestObjectKind, Guid, ITestObject>(
                    added: Enumerable.Empty<KeyValuePair<DataObjectIdentifier<TestObjectKind, Guid>, ITestObject>>(),
                    updated: Enumerable.Empty<KeyValuePair<DataObjectIdentifier<TestObjectKind, Guid>, ITestObject>>(),
                    removed: removedRecords));

            mapper.Save(
                Substitute.For<IDbConnection>(),
                Substitute.For<IDbTransaction>(),
                Substitute.For<ISqlParameterConverter>(),
                collection);

            Assert.AreEqual(1, parametersUsed.Count(p => p.ParameterName == "TestObjectId"));
            Assert.AreEqual(id.Value, parametersUsed.Single(p => p.ParameterName == "TestObjectId").Value);
            Assert.AreEqual(1, parametersUsed.Count(p => p.ParameterName == "LoanId"));
            Assert.AreEqual(new Guid("11111111-1111-1111-1111-111111111111"), parametersUsed.Single(p => p.ParameterName == "LoanId").Value);
            Assert.AreEqual(1, parametersUsed.Count(p => p.ParameterName == "BrokerId"));
            Assert.AreEqual(new Guid("22222222-2222-2222-2222-222222222222"), parametersUsed.Single(p => p.ParameterName == "BrokerId").Value);
        }

        private class TestMapper : StoredProcedureCollectionMapper<TestObjectKind, Guid, ITestObject>
        {
            public TestMapper(IStoredProcedureDriver storedProcedureDriver)
                : base(storedProcedureDriver)
            {
            }

            protected override StoredProcedureName CreateStoredProcedureName => StoredProcedureName.Create("Create").Value;

            protected override StoredProcedureName LoadAllRecordsStoredProcedureName => StoredProcedureName.Create("LoadAllRecords").Value;

            protected override StoredProcedureName UpdateStoredProcedureName => StoredProcedureName.Create("Update").Value;

            protected override StoredProcedureName DeleteStoredProcedureName => StoredProcedureName.Create("Delete").Value;

            protected override string IdColumnName => "TestObjectId";

            protected override ITestObject CreateRecord(IChangeTrackingDataContainer data)
            {
                return Substitute.For<ITestObject>();
            }

            protected override IEnumerable<SqlParameter> GetFilterSqlParameters()
            {
                return new[] 
                {
                    new SqlParameter("LoanId", new Guid("11111111-1111-1111-1111-111111111111")),
                    new SqlParameter("BrokerId", new Guid("22222222-2222-2222-2222-222222222222"))
                };
            }

            protected override IEnumerable<SqlParameter> GetSaveParameters(ITestObject value, ISqlParameterConverter sqlParameterConverter)
            {
                yield return new SqlParameter("Field", "Value");
            }

            protected override IDataContainer LoadRecord(IDataReader reader, IDataReaderConverter dataReaderConverter)
            {
                return Substitute.For<IDataContainer>();
            }
        }

        internal class TestObjectKind : DataObjectKind { }

        internal interface ITestObject : ITrackChanges
        {
        }
    }
}
