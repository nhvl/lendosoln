﻿namespace LendingQB.Test.Developers.LendingQB.Core.Mapping
{
    using System;
    using System.Data.Common;
    using DataAccess;
    using LendersOffice.Security;
    using NUnit.Framework;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;
    using LendersOffice.Drivers.SqlServerDB;
    using Utils;
    using System.Data;
    using global::LendingQB.Core.Mapping;
    using global::LendingQB.Core;
    using global::LendingQB.Core.Data;
    using global::DataAccess;

    [TestFixture]
    [Category(CategoryConstants.IntegrationTest)]
    public sealed class UladApplicationCollectionMapperTest
    {
        private DbConnection connection;
        private IDbTransaction transaction;
        private IStoredProcedureDriver storedProcedureDriver;

        private Guid loanId;
        private AbstractUserPrincipal principal;

        [TestFixtureSetUp]
        public void FixtureSetup()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                this.CreateLoan();

                var factory = new StoredProcedureDriverFactory();
                this.storedProcedureDriver = factory.Create(TimeoutInSeconds.Default);
            }
        }

        [TestFixtureTearDown]
        public void FixtureTearDown()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                this.InactiveLoanFile(this.loanId, this.principal);
            }
        }

        [SetUp]
        public void MethodSetUp()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                this.connection = DbConnectionInfo.GetConnectionInfo(this.principal.BrokerId).GetConnection();
                this.connection.Open();
                this.transaction = connection.BeginTransaction();
            }
        }

        [TearDown]
        public void MethodTearDown()
        {
            this.transaction.Rollback();
            this.transaction.Dispose();
            this.connection.Close();
        }

        [Test]
        public void Save_Empty_Fail_No_Primary()
        {
            var entity = new UladApplication();
            Assert.Throws<CBaseException>(() => this.CreateAndLoad(entity));
        }

        [Test]
        public void Save_Populated()
        {
            var entity = GetDefaultTestRecord();
            this.CreateAndLoad(entity);
        }

        [Test]
        public void Update()
        {
            var loanIdentifier = DataObjectIdentifier<DataObjectKind.Loan, Guid>.Create(this.loanId);
            var brokerIdentifier = DataObjectIdentifier<DataObjectKind.ClientCompany, Guid>.Create(this.principal.BrokerId);

            var original = GetDefaultTestRecord();
            IMutableLqbCollection<DataObjectKind.UladApplication, Guid, UladApplication> collection = LqbCollection<DataObjectKind.UladApplication, Guid, UladApplication>.Create(
                Name.Create("Test").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.UladApplication>());
            collection.BeginTrackingChanges();
            var entityId = collection.Add(original);
            Assert.AreNotEqual(Guid.Empty, entityId.Value);

            using (var helper = new FoolHelper())
            {
                // save data
                var mapper = new UladApplicationCollectionMapper(
                    loanIdentifier,
                    brokerIdentifier,
                    this.storedProcedureDriver);
                this.SaveCollection(mapper, collection);

                // load data
                collection = this.LoadCollection(mapper);
                var updateEntity = collection[entityId];

                // update data
                collection.BeginTrackingChanges();
                updateEntity.IsPrimary = false;

                var another = GetDefaultTestRecord();
                var anotherId = collection.Add(another);

                // save updates
                this.SaveCollection(mapper, collection);

                // re-load
                collection = this.LoadCollection(mapper);
                var loadedEntity = collection[entityId];
                var anotherEntity = collection[anotherId];

                // check field values
                this.CompareValues(updateEntity, loadedEntity);
                this.CompareValues(another, anotherEntity);
            }
        }

        [Test]
        public void Delete()
        {
            var loanIdentifier = DataObjectIdentifier<DataObjectKind.Loan, Guid>.Create(this.loanId);
            var brokerIdentifier = DataObjectIdentifier<DataObjectKind.ClientCompany, Guid>.Create(this.principal.BrokerId);

            IMutableLqbCollection<DataObjectKind.UladApplication, Guid, UladApplication> collection = LqbCollection<DataObjectKind.UladApplication, Guid, UladApplication>.Create(
                Name.Create("Test").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.UladApplication>());
            collection.BeginTrackingChanges();

            var original = GetDefaultTestRecord();
            var entityId = collection.Add(original);

            var empty = new UladApplication();
            var emptyId = collection.Add(empty);

            using (var helper = new FoolHelper())
            {
                // save data
                var mapper = new UladApplicationCollectionMapper(
                    loanIdentifier,
                    brokerIdentifier,
                    this.storedProcedureDriver);
                this.SaveCollection(mapper, collection);

                // load data
                collection = this.LoadCollection(mapper);
                var loadedEntity = collection[entityId];
                var loadedEmpty = collection[emptyId];

                // delete data
                collection.BeginTrackingChanges();
                collection.Remove(emptyId);

                // save updates
                this.SaveCollection(mapper, collection);

                // re-load
                collection = this.LoadCollection(mapper);
                Assert.AreEqual(1, collection.Count);
            }
        }

        private void CreateAndLoad(UladApplication original)
        {
            var loanIdentifier = DataObjectIdentifier<DataObjectKind.Loan, Guid>.Create(this.loanId);
            var brokerIdentifier = DataObjectIdentifier<DataObjectKind.ClientCompany, Guid>.Create(this.principal.BrokerId);

            IMutableLqbCollection<DataObjectKind.UladApplication, Guid, UladApplication> collection = LqbCollection<DataObjectKind.UladApplication, Guid, UladApplication>.Create(
                Name.Create("Test").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.UladApplication>());
            collection.BeginTrackingChanges();
            var entityId = collection.Add(original);
            Assert.AreNotEqual(Guid.Empty, entityId.Value);

            using (var helper = new FoolHelper())
            {
                // save data
                var mapper = new UladApplicationCollectionMapper(
                    loanIdentifier,
                    brokerIdentifier,
                    this.storedProcedureDriver);
                this.SaveCollection(mapper, collection);

                // load data
                collection = this.LoadCollection(mapper);
                var loadedEntity = collection[entityId];

                // check field values
                this.CompareValues(original, loadedEntity);
            }
        }

        private void CompareValues(UladApplication original, UladApplication loadedEntity)
        {
            Assert.IsTrue(loadedEntity.IsPrimary == original.IsPrimary);
        }

        public static UladApplication GetDefaultTestRecord()
        {
            var record = new UladApplication();
            record.IsPrimary = true;

            return record;
        }

        private IMutableLqbCollection<DataObjectKind.UladApplication, Guid, UladApplication> LoadCollection(
            ILqbCollectionSqlMapper<DataObjectKind.UladApplication, Guid, UladApplication> mapper)
        {
            return Load(mapper, this.connection, this.transaction);
        }

        internal static IMutableLqbCollection<DataObjectKind.UladApplication, Guid, UladApplication> Load(
            ILqbCollectionSqlMapper<DataObjectKind.UladApplication, Guid, UladApplication> mapper,
            IDbConnection connection, IDbTransaction transaction)
        {
            return mapper.Load(
                connection,
                transaction,
                Name.Create("Test").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.UladApplication>(),
                null,
                new DataReaderConverter());
        }

        private void SaveCollection(
            ILqbCollectionSqlMapper<DataObjectKind.UladApplication, Guid, UladApplication> mapper,
            IMutableLqbCollection<DataObjectKind.UladApplication, Guid, UladApplication> collection)
        {
            Save(mapper, collection, this.connection, this.transaction);
        }

        internal static void Save(
            ILqbCollectionSqlMapper<DataObjectKind.UladApplication, Guid, UladApplication> mapper,
            IMutableLqbCollection<DataObjectKind.UladApplication, Guid, UladApplication> collection,
            IDbConnection connection, IDbTransaction transaction)
        {
            mapper.Save(
                connection,
                transaction,
                new SqlParameterConverter(),
                collection);
        }

        private void CreateLoan()
        {
            this.principal = LoginTools.LoginAs(TestAccountType.LoTest001);

            var creator = CLoanFileCreator.GetCreator(principal, LendersOffice.Audit.E_LoanCreationSource.UserCreateFromBlank);
            this.loanId = creator.CreateBlankLoanFile();
        }

        private void InactiveLoanFile(Guid loanId, AbstractUserPrincipal principal)
        {
            Tools.DeclareLoanFileInvalid(
                principal,
                loanId,
                sendNotifications: false,
                generateLinkLoanMsgEvent: false);
        }
    }
}