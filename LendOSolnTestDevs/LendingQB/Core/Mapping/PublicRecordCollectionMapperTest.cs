﻿namespace LendingQB.Test.Developers.LendingQB.Core.Mapping
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Text;
    using DataAccess;
    using LendersOffice.CreditReport;
    using LendersOffice.Drivers.SqlServerDB;
    using LendersOffice.Security;
    using global::LendingQB.Core;
    using global::LendingQB.Core.Data;
    using global::LendingQB.Core.Mapping;
    using LqbGrammar;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;
    using NUnit.Framework;
    using Utils;

    using global::DataAccess;

    [TestFixture]
    [Category(CategoryConstants.IntegrationTest)]
    public sealed class PublicRecordCollectionMapperTest
    {
        private DbConnection connection;
        private IDbTransaction transaction;
        private IStoredProcedureDriver storedProcedureDriver;

        private Guid loanId;
        private AbstractUserPrincipal principal;

        [TestFixtureSetUp]
        public void FixtureSetup()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                this.CreateLoan();

                var factory = new StoredProcedureDriverFactory();
                this.storedProcedureDriver = factory.Create(TimeoutInSeconds.Default);
            }
        }

        [TestFixtureTearDown]
        public void FixtureTearDown()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                this.InactiveLoanFile(this.loanId, this.principal);
            }
        }

        [SetUp]
        public void MethodSetUp()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                this.connection = DbConnectionInfo.GetConnectionInfo(this.principal.BrokerId).GetConnection();
                this.connection.Open();
                this.transaction = connection.BeginTransaction();
            }
        }

        [TearDown]
        public void MethodTearDown()
        {
            this.transaction.Rollback();
            this.transaction.Dispose();
            this.connection.Close();
        }

        [Test]
        public void Save_Empty()
        {
            var entity = new PublicRecord();
            this.CreateAndLoad(entity);
        }

        [Test]
        public void Save_Populated()
        {
            var entity = GetDefaultTestRecord();
            this.CreateAndLoad(entity);
        }

        [Test]
        public void Update()
        {
            var loanIdentifier = DataObjectIdentifier<DataObjectKind.Loan, Guid>.Create(this.loanId);
            var brokerIdentifier = DataObjectIdentifier<DataObjectKind.ClientCompany, Guid>.Create(this.principal.BrokerId);

            var original = GetDefaultTestRecord();
            IMutableLqbCollection<DataObjectKind.PublicRecord, Guid, PublicRecord> collection = LqbCollection<DataObjectKind.PublicRecord, Guid, PublicRecord>.Create(
                Name.Create("Test").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.PublicRecord>());
            collection.BeginTrackingChanges();
            var entityId = collection.Add(original);
            Assert.AreNotEqual(Guid.Empty, entityId.Value);

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.Json);

                // save data
                var customMapper = new PublicRecordAuditTrailMapper();
                var mapper = new PublicRecordCollectionMapper(
                    loanIdentifier,
                    brokerIdentifier,
                    null,
                    customMapper,
                    this.storedProcedureDriver);
                this.SaveCollection(mapper, collection);

                // load data
                collection = this.LoadCollection(mapper);
                var updateEntity = collection[entityId];

                // update data
                collection.BeginTrackingChanges();
                updateEntity.CourtName = EntityName.Create("Star Chamber");
                var auditItem = CreateAuditItem("King Lear", "12/24/1945", "Performance for the joint allied forces in Europe");
                updateEntity.AuditTrail.Add(auditItem);

                var empty = new PublicRecord();
                var emptyId = collection.Add(empty);

                // save updates
                this.SaveCollection(mapper, collection);

                // re-load
                collection = this.LoadCollection(mapper);
                var loadedEntity = collection[entityId];
                var emptyEntity = collection[emptyId];

                // check field values
                this.CompareValues(updateEntity, loadedEntity);
                this.CompareValues(empty, emptyEntity);
            }
        }

        [Test]
        public void Delete()
        {
            var loanIdentifier = DataObjectIdentifier<DataObjectKind.Loan, Guid>.Create(this.loanId);
            var brokerIdentifier = DataObjectIdentifier<DataObjectKind.ClientCompany, Guid>.Create(this.principal.BrokerId);

            IMutableLqbCollection<DataObjectKind.PublicRecord, Guid, PublicRecord> collection = LqbCollection<DataObjectKind.PublicRecord, Guid, PublicRecord>.Create(
                Name.Create("Test").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.PublicRecord>());
            collection.BeginTrackingChanges();

            var original = GetDefaultTestRecord();
            var entityId = collection.Add(original);

            var empty = new PublicRecord();
            var emptyId = collection.Add(empty);

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.Json);

                // save data
                var customMapper = new PublicRecordAuditTrailMapper();
                var mapper = new PublicRecordCollectionMapper(
                    loanIdentifier,
                    brokerIdentifier,
                    null,
                    customMapper,
                    this.storedProcedureDriver);
                this.SaveCollection(mapper, collection);

                // load data
                collection = this.LoadCollection(mapper);
                var loadedEntity = collection[entityId];
                var loadedEmpty = collection[emptyId];

                // delete data
                collection.BeginTrackingChanges();
                collection.Remove(emptyId);

                // save updates
                this.SaveCollection(mapper, collection);

                // re-load
                collection = this.LoadCollection(mapper);
                Assert.AreEqual(1, collection.Count);
            }
        }

        private void CreateAndLoad(PublicRecord original)
        {
            var loanIdentifier = DataObjectIdentifier<DataObjectKind.Loan, Guid>.Create(this.loanId);
            var brokerIdentifier = DataObjectIdentifier<DataObjectKind.ClientCompany, Guid>.Create(this.principal.BrokerId);

            IMutableLqbCollection<DataObjectKind.PublicRecord, Guid, PublicRecord> collection = LqbCollection<DataObjectKind.PublicRecord, Guid, PublicRecord>.Create(
                Name.Create("Test").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.PublicRecord>());
            collection.BeginTrackingChanges();
            var entityId = collection.Add(original);
            Assert.AreNotEqual(Guid.Empty, entityId.Value);

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.Json);

                // save data
                var customMapper = new PublicRecordAuditTrailMapper();
                var mapper = new PublicRecordCollectionMapper(
                    loanIdentifier,
                    brokerIdentifier,
                    null,
                    customMapper,
                    this.storedProcedureDriver);
                this.SaveCollection(mapper, collection);

                // load data
                collection = this.LoadCollection(mapper);
                var loadedEntity = collection[entityId];

                // check field values
                this.CompareValues(original, loadedEntity);
            }
        }

        private void CompareValues(PublicRecord original, PublicRecord loadedEntity)
        {
            Assert.IsTrue(loadedEntity.BankruptcyLiabilitiesAmount == original.BankruptcyLiabilitiesAmount);
            Assert.IsTrue(loadedEntity.BkFileDate == original.BkFileDate);
            Assert.IsTrue(loadedEntity.CourtName == original.CourtName);
            Assert.IsTrue(loadedEntity.DispositionDate == original.DispositionDate);
            Assert.IsTrue(loadedEntity.DispositionType == original.DispositionType);
            Assert.IsTrue(loadedEntity.IdFromCreditReport == original.IdFromCreditReport);
            Assert.IsTrue(loadedEntity.IncludeInPricing == original.IncludeInPricing);
            Assert.IsTrue(loadedEntity.LastEffectiveDate == original.LastEffectiveDate);
            Assert.IsTrue(loadedEntity.ReportedDate == original.ReportedDate);
            Assert.IsTrue(loadedEntity.Type == original.Type);
            if (original.AuditTrail != null)
            {
                Assert.IsTrue(loadedEntity.AuditTrail.Count == original.AuditTrail.Count);
                for (int i = 0; i < original.AuditTrail.Count; ++i)
                {
                    var org = original.AuditTrail[i];
                    var ldd = loadedEntity.AuditTrail[i];

                    Assert.IsTrue(ldd.UserName == org.UserName);
                    Assert.IsTrue(ldd.EventDate == org.EventDate);
                    Assert.IsTrue(ldd.Description == org.Description);
                }
            }
        }

        public static PublicRecord GetDefaultTestRecord()
        {
            var record = new PublicRecord();
            record.BankruptcyLiabilitiesAmount = Money.Create(2000000.17m);
            record.BkFileDate = UnzonedDate.Create("2018-01-14");
            record.CourtName = EntityName.Create("Kangaroo Justice Center");
            record.DispositionDate = UnzonedDate.Create("2018-01-14");
            record.DispositionType = E_CreditPublicRecordDispositionType.Withdrawn;
            record.IdFromCreditReport = ThirdPartyIdentifier.Create("21080114_CRIM_Z_122233");
            record.IncludeInPricing = true;
            record.LastEffectiveDate = UnzonedDate.Create("2017-12-04");
            record.ReportedDate = UnzonedDate.Create("2017-12-08");
            record.Type = E_CreditPublicRecordType.ForcibleDetainer;

            record.AuditTrail = new List<CPublicRecordAuditItem>();
            record.AuditTrail.AddRange(GetDefaultAuditItems(record));

            return record;
        }

        private static CPublicRecordAuditItem[] GetDefaultAuditItems(PublicRecord record)
        {
            const string userName = "Happy Gilligan";
            const string timestamp = "3/17/2018";

            var sb = new StringBuilder();
            sb.AppendLine("Liabilities Amount: [Blank] -> " + record.BankruptcyLiabilitiesAmount.Value.ToString());
            sb.AppendLine("File Date: [Blank] -> " + record.BkFileDate.Value.ToString());
            sb.AppendLine("Court Name: [Blank] -> " + record.CourtName.Value.ToString());
            sb.AppendLine("Disposition Date: [Blank] -> " + record.DispositionDate.Value.ToString());
            sb.AppendLine("Disposition: [Blank] -> " + record.DispositionType.Value.ToString());
            sb.AppendLine("IdFromCreditReport: [Blank] -> " + record.IdFromCreditReport.Value.ToString());
            sb.AppendLine("IncludeInPricing: [Blank] -> " + record.IncludeInPricing.Value.ToString());
            sb.AppendLine("Last Effective Date: [Blank] -> " + record.LastEffectiveDate.Value.ToString());
            sb.AppendLine("Reported Date: [Blank] -> " + record.ReportedDate.Value.ToString());
            sb.AppendLine("Type: [Blank] -> " + record.Type.Value.ToString());

            var items = new CPublicRecordAuditItem[1];
            items[0] = new CPublicRecordAuditItem(userName, timestamp, sb.ToString());

            return items;
        }

        private static CPublicRecordAuditItem CreateAuditItem(string username, string timestamp, string description)
        {
            return new CPublicRecordAuditItem(username, timestamp, description);
        }

        private IMutableLqbCollection<DataObjectKind.PublicRecord, Guid, PublicRecord> LoadCollection(
            ILqbCollectionSqlMapper<DataObjectKind.PublicRecord, Guid, PublicRecord> mapper)
        {
            return Load(mapper, this.connection, this.transaction);
        }

        internal static IMutableLqbCollection<DataObjectKind.PublicRecord, Guid, PublicRecord> Load(
            ILqbCollectionSqlMapper<DataObjectKind.PublicRecord, Guid, PublicRecord> mapper,
            IDbConnection connection, IDbTransaction transaction)
        {
            return mapper.Load(
                connection,
                transaction,
                Name.Create("Test").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.PublicRecord>(),
                null,
                new DataReaderConverter());
        }

        private void SaveCollection(
            ILqbCollectionSqlMapper<DataObjectKind.PublicRecord, Guid, PublicRecord> mapper,
            IMutableLqbCollection<DataObjectKind.PublicRecord, Guid, PublicRecord> collection)
        {
            Save(mapper, collection, this.connection, this.transaction);
        }

        internal static void Save(
            ILqbCollectionSqlMapper<DataObjectKind.PublicRecord, Guid, PublicRecord> mapper,
            IMutableLqbCollection<DataObjectKind.PublicRecord, Guid, PublicRecord> collection,
            IDbConnection connection, IDbTransaction transaction)
        {
            mapper.Save(
                connection,
                transaction,
                new SqlParameterConverter(),
                collection);
        }

        // Create a new test loan and return <brokerid, loanid>
        private void CreateLoan()
        {
            this.principal = LoginTools.LoginAs(TestAccountType.LoTest001);

            var creator = CLoanFileCreator.GetCreator(principal, LendersOffice.Audit.E_LoanCreationSource.UserCreateFromBlank);
            this.loanId = creator.CreateBlankLoanFile();
        }

        private void InactiveLoanFile(Guid loanId, AbstractUserPrincipal principal)
        {
            Tools.DeclareLoanFileInvalid(
                principal,
                loanId,
                sendNotifications: false,
                generateLinkLoanMsgEvent: false);
        }
    }
}
