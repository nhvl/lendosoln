﻿namespace LendingQB.Test.Developers.LendingQB.Core.Mapping
{
    using System;
    using System.Data.Common;
    using DataAccess;
    using LendersOffice.Security;
    using NUnit.Framework;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;
    using LendersOffice.Drivers.SqlServerDB;
    using Utils;
    using global::LendingQB.Core.Mapping;
    using global::LendingQB.Core;
    using global::LendingQB.Core.Data;
    using System.Data;
    using global::DataAccess;

    [TestFixture]
    public class VorRecordCollectionMapperTest
    {
        private AbstractUserPrincipal principal;
        private DataObjectIdentifier<DataObjectKind.Loan, Guid> loanId;
        private DataObjectIdentifier<DataObjectKind.ClientCompany, Guid> brokerId;
        private IStoredProcedureDriver storedProcedureDriver;
        private IDbConnection connection;
        private IDbTransaction transaction;

        [TestFixtureSetUp]
        public void FixtureSetUp()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                this.principal = LoginTools.LoginAs(TestAccountType.LoTest001);
                this.brokerId = DataObjectIdentifier<DataObjectKind.ClientCompany, Guid>.Create(principal.BrokerId);
                var creator = CLoanFileCreator.GetCreator(
                    this.principal,
                    LendersOffice.Audit.E_LoanCreationSource.UserCreateFromBlank);
                this.loanId = DataObjectIdentifier<DataObjectKind.Loan, Guid>.Create(creator.CreateBlankLoanFile());

                var factory = new StoredProcedureDriverFactory();
                this.storedProcedureDriver = factory.Create(TimeoutInSeconds.Default);
            }
        }

        [TestFixtureTearDown]
        public void FixtureTearDown()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                Tools.DeclareLoanFileInvalid(
                    this.principal,
                    this.loanId.Value,
                    sendNotifications: false,
                    generateLinkLoanMsgEvent: false);
            }
        }

        [SetUp]
        public void SetUp()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                this.connection = DbConnectionInfo.GetConnectionInfo(this.brokerId.Value).GetConnection();
                this.connection.Open();
                this.transaction = connection.BeginTransaction();
            }
        }

        [TearDown]
        public void TearDown()
        {
            this.transaction.Rollback();
            this.transaction.Dispose();
            this.connection.Dispose();
        }

        [Test]
        public void Load_AssumingNoDatabaseIssues_Succeeds()
        {
            var mapper = new VorRecordCollectionMapper(
                this.loanId,
                this.brokerId,
                this.storedProcedureDriver);

            this.LoadCollection(mapper);
        }

        [Test]
        public void Save_NewRecord_PersistsEnteredDataPoints()
        {
            var mapper = new VorRecordCollectionMapper(
                this.loanId,
                this.brokerId,
                this.storedProcedureDriver);
            IMutableLqbCollection<DataObjectKind.VorRecord, Guid, VorRecord> collection = LqbCollection<DataObjectKind.VorRecord, Guid, VorRecord>.Create(
                Name.Create("Test").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.VorRecord>());
            VorRecord originalRecord = GetDefaultTestRecord();

            collection.BeginTrackingChanges();
            var id = collection.Add(originalRecord);
            this.SaveCollection(mapper, collection);
            collection = this.LoadCollection(mapper);
            var loadedRecord = collection[id];

            Assert.AreEqual(DescriptionField.Create("Important Account"), loadedRecord.AccountName);
            Assert.AreEqual(StreetAddress.Create("123 Main St"), loadedRecord.StreetAddressFor);
            Assert.AreEqual(StreetAddress.Create("456 Main St"), loadedRecord.StreetAddressTo);
            Assert.AreEqual(EntityName.Create("Spongebob"), loadedRecord.Attention);
            Assert.AreEqual(City.Create("Costa Mesa"), loadedRecord.CityFor);
            Assert.AreEqual(City.Create("Fountain Valley"), loadedRecord.CityTo);
            Assert.AreEqual(DescriptionField.Create("Verbose Description Placeholder"), loadedRecord.Description);
            Assert.AreEqual(PhoneNumber.Create("000-000-0000"), loadedRecord.FaxTo);
            Assert.AreEqual(false, loadedRecord.IsSeeAttachment);
            Assert.AreEqual(EntityName.Create("Super Attentive Landlords R Us"), loadedRecord.LandlordCreditorName);
            Assert.AreEqual(VorSpecialRecordType.BorrowerPrevious2, loadedRecord.LegacyRecordSpecialType);
            Assert.AreEqual(PhoneNumber.Create("111-111-1111"), loadedRecord.PhoneTo);
            Assert.AreEqual(UnzonedDate.Create(2018, 1, 1), loadedRecord.PrepDate);
            Assert.AreEqual(UnitedStatesPostalState.Create("CA"), loadedRecord.StateFor);
            Assert.AreEqual(UnitedStatesPostalState.Create("AZ"), loadedRecord.StateTo);
            Assert.AreEqual(UnzonedDate.Create(2018, 2, 2), loadedRecord.VerifExpDate);
            Assert.AreEqual(UnzonedDate.Create(2018, 3, 3), loadedRecord.VerifRecvDate);
            Assert.AreEqual(UnzonedDate.Create(2018, 4, 4), loadedRecord.VerifReorderedDate);
            Assert.AreEqual(UnzonedDate.Create(2018, 5, 5), loadedRecord.VerifSentDate);
            Assert.AreEqual(E_VorT.Mortgage, loadedRecord.VerifType);
            Assert.AreEqual(Zipcode.Create("92626"), loadedRecord.ZipFor);
            Assert.AreEqual(Zipcode.Create("92708"), loadedRecord.ZipTo);
        }

        [Test]
        public void Save_EmptyRecord_PersistsEmptyRecord()
        {
            var mapper = new VorRecordCollectionMapper(
                this.loanId,
                this.brokerId,
                this.storedProcedureDriver);
            IMutableLqbCollection<DataObjectKind.VorRecord, Guid, VorRecord> collection = LqbCollection<DataObjectKind.VorRecord, Guid, VorRecord>.Create(
                Name.Create("Test").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.VorRecord>());
            var originalRecord = new VorRecord();

            collection.BeginTrackingChanges();
            var id = collection.Add(originalRecord);
            this.SaveCollection(mapper, collection);
            collection = this.LoadCollection(mapper);
            var loadedRecord = collection[id];

            Assert.AreEqual(null, loadedRecord.AccountName);
            Assert.AreEqual(null, loadedRecord.StreetAddressFor);
            Assert.AreEqual(null, loadedRecord.StreetAddressTo);
            Assert.AreEqual(null, loadedRecord.Attention);
            Assert.AreEqual(null, loadedRecord.CityFor);
            Assert.AreEqual(null, loadedRecord.CityTo);
            Assert.AreEqual(null, loadedRecord.Description);
            Assert.AreEqual(null, loadedRecord.FaxTo);
            Assert.AreEqual(null, loadedRecord.IsSeeAttachment);
            Assert.AreEqual(null, loadedRecord.LandlordCreditorName);
            Assert.AreEqual(null, loadedRecord.LegacyRecordSpecialType);
            Assert.AreEqual(null, loadedRecord.PhoneTo);
            Assert.AreEqual(null, loadedRecord.PrepDate);
            Assert.AreEqual(null, loadedRecord.StateFor);
            Assert.AreEqual(null, loadedRecord.StateTo);
            Assert.AreEqual(null, loadedRecord.VerifExpDate);
            Assert.AreEqual(null, loadedRecord.VerifRecvDate);
            Assert.AreEqual(null, loadedRecord.VerifReorderedDate);
            Assert.AreEqual(null, loadedRecord.VerifSentDate);
            Assert.AreEqual(null, loadedRecord.VerifType);
            Assert.AreEqual(null, loadedRecord.ZipFor);
            Assert.AreEqual(null, loadedRecord.ZipTo);
        }

        [Test]
        public void Save_UpdateExistingRecord_PersistsUpdatedDataPoints()
        {
            var mapper = new VorRecordCollectionMapper(
                this.loanId,
                this.brokerId,
                this.storedProcedureDriver);
            IMutableLqbCollection<DataObjectKind.VorRecord, Guid, VorRecord> collection = LqbCollection<DataObjectKind.VorRecord, Guid, VorRecord>.Create(
                Name.Create("Test").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.VorRecord>());
            var originalRecord = GetDefaultTestRecord();

            collection.BeginTrackingChanges();
            var id = collection.Add(originalRecord);
            this.SaveCollection(mapper, collection);

            collection = this.LoadCollection(mapper);
            collection.BeginTrackingChanges();
            var loadedRecord = collection[id];
            loadedRecord.AccountName = DescriptionField.Create("Updated Important Account");
            loadedRecord.StreetAddressFor = StreetAddress.Create("789 Main St");
            loadedRecord.StreetAddressTo = StreetAddress.Create("012 Main St");
            loadedRecord.Attention = EntityName.Create("Patrick");
            loadedRecord.CityFor = City.Create("Huntington Beach");
            loadedRecord.CityTo = City.Create("Beverly Hills");
            loadedRecord.Description = DescriptionField.Create("Less Verbose Description Placeholder");
            loadedRecord.FaxTo = PhoneNumber.Create("001-001-0001");
            loadedRecord.IsSeeAttachment = true;
            loadedRecord.LandlordCreditorName = EntityName.Create("Super Not Attentive Landlords R Us");
            loadedRecord.SetLegacyRecordSpecialType(VorSpecialRecordType.CoborrowerPrevious1);
            loadedRecord.PhoneTo = PhoneNumber.Create("112-112-1112");
            loadedRecord.PrepDate = UnzonedDate.Create(2017, 1, 1);
            loadedRecord.StateFor = UnitedStatesPostalState.Create("CO");
            loadedRecord.StateTo = UnitedStatesPostalState.Create("NM");
            loadedRecord.VerifExpDate = UnzonedDate.Create(2017, 2, 2);
            loadedRecord.VerifRecvDate = UnzonedDate.Create(2017, 3, 3);
            loadedRecord.VerifReorderedDate = UnzonedDate.Create(2017, 4, 4);
            loadedRecord.VerifSentDate = UnzonedDate.Create(2017, 5, 5);
            loadedRecord.VerifType = E_VorT.Rental;
            loadedRecord.ZipFor = Zipcode.Create("92648");
            loadedRecord.ZipTo = Zipcode.Create("90210");

            this.SaveCollection(mapper, collection);

            collection = this.LoadCollection(mapper);
            var updatedRecord = collection[id];

            Assert.AreEqual(DescriptionField.Create("Updated Important Account"), updatedRecord.AccountName);
            Assert.AreEqual(StreetAddress.Create("789 Main St"), updatedRecord.StreetAddressFor);
            Assert.AreEqual(StreetAddress.Create("012 Main St"), updatedRecord.StreetAddressTo);
            Assert.AreEqual(EntityName.Create("Patrick"), updatedRecord.Attention);
            Assert.AreEqual(City.Create("Huntington Beach"), updatedRecord.CityFor);
            Assert.AreEqual(City.Create("Beverly Hills"), updatedRecord.CityTo);
            Assert.AreEqual(DescriptionField.Create("Less Verbose Description Placeholder"), updatedRecord.Description);
            Assert.AreEqual(PhoneNumber.Create("001-001-0001"), updatedRecord.FaxTo);
            Assert.AreEqual(true, updatedRecord.IsSeeAttachment);
            Assert.AreEqual(EntityName.Create("Super Not Attentive Landlords R Us"), updatedRecord.LandlordCreditorName);
            Assert.AreEqual(VorSpecialRecordType.CoborrowerPrevious1, updatedRecord.LegacyRecordSpecialType);
            Assert.AreEqual(PhoneNumber.Create("112-112-1112"), updatedRecord.PhoneTo);
            Assert.AreEqual(UnzonedDate.Create(2017, 1, 1), updatedRecord.PrepDate);
            Assert.AreEqual(UnitedStatesPostalState.Create("CO"), updatedRecord.StateFor);
            Assert.AreEqual(UnitedStatesPostalState.Create("NM"), updatedRecord.StateTo);
            Assert.AreEqual(UnzonedDate.Create(2017, 2, 2), updatedRecord.VerifExpDate);
            Assert.AreEqual(UnzonedDate.Create(2017, 3, 3), updatedRecord.VerifRecvDate);
            Assert.AreEqual(UnzonedDate.Create(2017, 4, 4), updatedRecord.VerifReorderedDate);
            Assert.AreEqual(UnzonedDate.Create(2017, 5, 5), updatedRecord.VerifSentDate);
            Assert.AreEqual(E_VorT.Rental, updatedRecord.VerifType);
            Assert.AreEqual(Zipcode.Create("92648"), updatedRecord.ZipFor);
            Assert.AreEqual(Zipcode.Create("90210"), updatedRecord.ZipTo);
        }

        [Test]
        public void Save_DeleteExistingRecord_RemovesRecordFromDatabase()
        {
            var mapper = new VorRecordCollectionMapper(
                this.loanId,
                this.brokerId,
                this.storedProcedureDriver);
            IMutableLqbCollection<DataObjectKind.VorRecord, Guid, VorRecord> collection = LqbCollection<DataObjectKind.VorRecord, Guid, VorRecord>.Create(
                Name.Create("Test").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.VorRecord>());
            var record = new VorRecord();

            collection.BeginTrackingChanges();
            var id = collection.Add(record);
            this.SaveCollection(mapper, collection);

            collection = this.LoadCollection(mapper);
            collection.BeginTrackingChanges();
            collection.Remove(id);
            this.SaveCollection(mapper, collection);

            collection = this.LoadCollection(mapper);

            Assert.AreEqual(false, collection.ContainsKey(id));
        }

        public static VorRecord GetDefaultTestRecord()
        {
            var record = new VorRecord();
            //DataObjectIdentifier<DataObjectKind.Signature, Guid>? VerifSignatureImgId { get; }
            //DataObjectIdentifier<DataObjectKind.Employee, Guid>? VerifSigningEmployeeId { get; }
            record.AccountName = DescriptionField.Create("Important Account");
            record.StreetAddressFor = StreetAddress.Create("123 Main St");
            record.StreetAddressTo = StreetAddress.Create("456 Main St");
            record.Attention = EntityName.Create("Spongebob");
            record.CityFor = City.Create("Costa Mesa");
            record.CityTo = City.Create("Fountain Valley");
            record.Description = DescriptionField.Create("Verbose Description Placeholder");
            record.FaxTo = PhoneNumber.Create("000-000-0000");
            record.IsSeeAttachment = false;
            record.LandlordCreditorName = EntityName.Create("Super Attentive Landlords R Us");
            record.SetLegacyRecordSpecialType(VorSpecialRecordType.BorrowerPrevious2);
            record.PhoneTo = PhoneNumber.Create("111-111-1111");
            record.PrepDate = UnzonedDate.Create(2018, 1, 1);
            record.StateFor = UnitedStatesPostalState.Create("CA");
            record.StateTo = UnitedStatesPostalState.Create("AZ");
            record.VerifExpDate = UnzonedDate.Create(2018, 2, 2);
            record.VerifRecvDate = UnzonedDate.Create(2018, 3, 3);
            record.VerifReorderedDate = UnzonedDate.Create(2018, 4, 4);
            record.VerifSentDate = UnzonedDate.Create(2018, 5, 5);
            record.VerifType = E_VorT.Mortgage;
            record.ZipFor = Zipcode.Create("92626");
            record.ZipTo = Zipcode.Create("92708");
            return record;
        }

        private IMutableLqbCollection<DataObjectKind.VorRecord, Guid, VorRecord> LoadCollection(
            ILqbCollectionSqlMapper<DataObjectKind.VorRecord, Guid, VorRecord> mapper)
        {
            return Load(mapper, this.connection, this.transaction);
        }

        internal static IMutableLqbCollection<DataObjectKind.VorRecord, Guid, VorRecord> Load(
            ILqbCollectionSqlMapper<DataObjectKind.VorRecord, Guid, VorRecord> mapper,
            IDbConnection connection, IDbTransaction transaction)
        {
            return mapper.Load(
                connection,
                transaction,
                Name.Create("Test").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.VorRecord>(),
                null,
                new DataReaderConverter());
        }

        private void SaveCollection(
            ILqbCollectionSqlMapper<DataObjectKind.VorRecord, Guid, VorRecord> mapper,
            IMutableLqbCollection<DataObjectKind.VorRecord, Guid, VorRecord> collection)
        {
            Save(mapper, collection, this.connection, this.transaction);
        }

        internal static void Save(
            ILqbCollectionSqlMapper<DataObjectKind.VorRecord, Guid, VorRecord> mapper,
            IMutableLqbCollection<DataObjectKind.VorRecord, Guid, VorRecord> collection,
            IDbConnection connection, IDbTransaction transaction)
        {
            mapper.Save(
                connection,
                transaction,
                new SqlParameterConverter(),
                collection);
        }
    }
}
