﻿namespace LendingQB.Test.Developers.LendingQB.Core.Mapping
{
    using System;
    using System.Data;
    using System.Data.Common;
    using LendersOffice.Drivers.SqlServerDB;
    using LendersOffice.Security;
    using global::DataAccess;
    using global::LendingQB.Core;
    using global::LendingQB.Core.Data;
    using global::LendingQB.Core.Mapping;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;
    using NUnit.Framework;
    using Utils;
    using LqbGrammar.Exceptions;

    [TestFixture]
    [Category(CategoryConstants.IntegrationTest)]
    public sealed class RealPropertyLiabilityMapperTest
    {
        // Values taken from the file LendersOfficeLib/LendingQB/Core/AssociationData.xml
        // NOTE: cardinalities are set based on value of the OTHER phrase
        const int MaxFirstCardinality = -1;
        const int MaxSecondCardinality = 1; // unbounded

        private DbConnection connection;
        private IDbTransaction transaction;
        private IStoredProcedureDriver storedProcedureDriver;

        private Guid loanId;
        private AbstractUserPrincipal principal;

        [TestFixtureSetUp]
        public void FixtureSetup()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                this.CreateLoan();

                var factory = new StoredProcedureDriverFactory();
                this.storedProcedureDriver = factory.Create(TimeoutInSeconds.Default);
            }
        }

        [TestFixtureTearDown]
        public void FixtureTearDown()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                InactiveLoanFile(this.loanId, this.principal);
            }
        }

        [SetUp]
        public void MethodSetUp()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                this.connection = DbConnectionInfo.GetConnectionInfo(this.principal.BrokerId).GetConnection();
                this.connection.Open();
                this.transaction = connection.BeginTransaction();
            }
        }

        [TearDown]
        public void MethodTearDown()
        {
            this.transaction.Rollback();
            this.transaction.Dispose();
            this.connection.Close();
        }

        [Test]
        public void Add_MultipleRealProperty_SameLiability_ExpectFail()
        {
            var loanIdentifier = DataObjectIdentifier<DataObjectKind.Loan, Guid>.Create(this.loanId);
            var brokerIdentifier = DataObjectIdentifier<DataObjectKind.ClientCompany, Guid>.Create(this.principal.BrokerId);

            var property1 = RealPropertyCollectionMapperTest.GetDefaultTestRecord();
            var property2 = RealPropertyCollectionMapperTest.GetDefaultTestRecord();
            IMutableLqbCollection<DataObjectKind.RealProperty, Guid, RealProperty> collectionRP = LqbCollection<DataObjectKind.RealProperty, Guid, RealProperty>.Create(
                Name.Create("Property").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.RealProperty>());
            collectionRP.BeginTrackingChanges();
            var firstPropertyId = collectionRP.Add(property1);
            var secondPropertyId = collectionRP.Add(property2);

            var liability = LiabilityCollectionMapperTest.GetDefaultTestRecord();
            IMutableLqbCollection<DataObjectKind.Liability, Guid, Liability> collectionL = LqbCollection<DataObjectKind.Liability, Guid, Liability>.Create(
                Name.Create("Liability").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.Liability>());
            collectionL.BeginTrackingChanges();
            var liabilityId = collectionL.Add(liability);

            var association1 = new RealPropertyLiabilityAssociation(firstPropertyId, liabilityId);
            var association2 = new RealPropertyLiabilityAssociation(secondPropertyId, liabilityId);
            IMutableLqbAssociationSet<DataObjectKind.RealPropertyLiabilityAssociation, Guid, RealPropertyLiabilityAssociation> associationSet = LqbAssociationSet<DataObjectKind.RealPropertyLiabilityAssociation, Guid, RealPropertyLiabilityAssociation>.Create(
                Name.Create("Test").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.RealPropertyLiabilityAssociation>(), MaxFirstCardinality, MaxSecondCardinality);
            associationSet.BeginTrackingChanges();
            var assocId = associationSet.Add(association1);

            Assert.Throws<CBaseException>(() => associationSet.Add(association2));
        }

        [Test]
        public void Add_SameRealProperty_MultipleLiability_ExpectSuccess()
        {
            var loanIdentifier = DataObjectIdentifier<DataObjectKind.Loan, Guid>.Create(this.loanId);
            var brokerIdentifier = DataObjectIdentifier<DataObjectKind.ClientCompany, Guid>.Create(this.principal.BrokerId);

            var property = RealPropertyCollectionMapperTest.GetDefaultTestRecord();
            IMutableLqbCollection<DataObjectKind.RealProperty, Guid, RealProperty> collectionRP = LqbCollection<DataObjectKind.RealProperty, Guid, RealProperty>.Create(
                Name.Create("Property").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.RealProperty>());
            collectionRP.BeginTrackingChanges();
            var propertyId = collectionRP.Add(property);

            var liability1 = LiabilityCollectionMapperTest.GetDefaultTestRecord();
            var liability2 = LiabilityCollectionMapperTest.GetDefaultTestRecord();
            IMutableLqbCollection<DataObjectKind.Liability, Guid, Liability> collectionL = LqbCollection<DataObjectKind.Liability, Guid, Liability>.Create(
                Name.Create("Liability").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.Liability>());
            collectionL.BeginTrackingChanges();
            var firstLiabilityId = collectionL.Add(liability1);
            var secondLiabilityId = collectionL.Add(liability2);

            var association1 = new RealPropertyLiabilityAssociation(propertyId, firstLiabilityId);
            var association2 = new RealPropertyLiabilityAssociation(propertyId, secondLiabilityId);
            IMutableLqbAssociationSet<DataObjectKind.RealPropertyLiabilityAssociation, Guid, RealPropertyLiabilityAssociation> associationSet = LqbAssociationSet<DataObjectKind.RealPropertyLiabilityAssociation, Guid, RealPropertyLiabilityAssociation>.Create(
                Name.Create("Test").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.RealPropertyLiabilityAssociation>(), MaxFirstCardinality, MaxSecondCardinality);
            associationSet.BeginTrackingChanges();
            var assocId = associationSet.Add(association1);

            // adding the second should be ok for OneToMany cardinality
            assocId = associationSet.Add(association2);
        }

        [Test]
        public void Save_Populated()
        {
            var loanIdentifier = DataObjectIdentifier<DataObjectKind.Loan, Guid>.Create(this.loanId);
            var brokerIdentifier = DataObjectIdentifier<DataObjectKind.ClientCompany, Guid>.Create(this.principal.BrokerId);

            var property = RealPropertyCollectionMapperTest.GetDefaultTestRecord();
            IMutableLqbCollection<DataObjectKind.RealProperty, Guid, RealProperty> collectionRP = LqbCollection<DataObjectKind.RealProperty, Guid, RealProperty>.Create(
                Name.Create("Property").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.RealProperty>());
            collectionRP.BeginTrackingChanges();
            var propertyId = collectionRP.Add(property);
            Assert.AreNotEqual(Guid.Empty, propertyId.Value);

            var liability = LiabilityCollectionMapperTest.GetDefaultTestRecord();
            IMutableLqbCollection<DataObjectKind.Liability, Guid, Liability> collectionL = LqbCollection<DataObjectKind.Liability, Guid, Liability>.Create(
                Name.Create("Liability").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.Liability>());
            collectionL.BeginTrackingChanges();
            var liabilityId = collectionL.Add(liability);
            Assert.AreNotEqual(Guid.Empty, liabilityId.Value);

            var association = new RealPropertyLiabilityAssociation(propertyId, liabilityId);
            IMutableLqbAssociationSet<DataObjectKind.RealPropertyLiabilityAssociation, Guid, RealPropertyLiabilityAssociation> associationSet = LqbAssociationSet<DataObjectKind.RealPropertyLiabilityAssociation, Guid, RealPropertyLiabilityAssociation>.Create(
                Name.Create("Test").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.RealPropertyLiabilityAssociation>(), MaxFirstCardinality, MaxSecondCardinality);
            associationSet.BeginTrackingChanges();
            var assocId = associationSet.Add(association);
            Assert.AreNotEqual(Guid.Empty, assocId.Value);

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.Json);

                // save data
                var mapperRP = new RealPropertyCollectionMapper(
                    loanIdentifier,
                    brokerIdentifier,
                    this.storedProcedureDriver);
                RealPropertyCollectionMapperTest.SaveCollection(mapperRP, collectionRP, this.connection, this.transaction);

                var pmlAuditTrailMapper = new LiabilityPmlAuditTrailMapper();
                var mapperL = new LiabilityCollectionMapper(
                    loanIdentifier,
                    brokerIdentifier,
                    NSubstitute.Substitute.For<ILiabilityDefaultsProvider>(),
                    pmlAuditTrailMapper,
                    this.storedProcedureDriver);
                LiabilityCollectionMapperTest.SaveCollection(mapperL, collectionL, this.connection, this.transaction);

                var mapperA = new RealPropertyLiabilityMapper(
                    loanIdentifier,
                    this.storedProcedureDriver);
                RealPropertyLiabilityMapperTest.SaveAssociationSet(mapperA, associationSet, this.connection, this.transaction);

                // re-load
                associationSet = RealPropertyLiabilityMapperTest.LoadAssociationSet(mapperA, this.connection, this.transaction);
                var loaded = associationSet[assocId];

                // check field values
                Assert.AreEqual(propertyId.Value, loaded.RealPropertyId.Value);
                Assert.AreEqual(liabilityId.Value, loaded.LiabilityId.Value);
            }
        }

        [Test]
        public void DeleteTest()
        {
            var loanIdentifier = DataObjectIdentifier<DataObjectKind.Loan, Guid>.Create(this.loanId);
            var brokerIdentifier = DataObjectIdentifier<DataObjectKind.ClientCompany, Guid>.Create(this.principal.BrokerId);

            var property = RealPropertyCollectionMapperTest.GetDefaultTestRecord();
            IMutableLqbCollection<DataObjectKind.RealProperty, Guid, RealProperty> collectionRP = LqbCollection<DataObjectKind.RealProperty, Guid, RealProperty>.Create(
                Name.Create("Property").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.RealProperty>());
            collectionRP.BeginTrackingChanges();
            var propertyId = collectionRP.Add(property);

            var liability1 = LiabilityCollectionMapperTest.GetDefaultTestRecord();
            var liability2 = LiabilityCollectionMapperTest.GetDefaultTestRecord();
            IMutableLqbCollection<DataObjectKind.Liability, Guid, Liability> collectionL = LqbCollection<DataObjectKind.Liability, Guid, Liability>.Create(
                Name.Create("Liability").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.Liability>());
            collectionL.BeginTrackingChanges();
            var firstLiabilityId = collectionL.Add(liability1);
            var secondLiabilityId = collectionL.Add(liability1);

            var association1 = new RealPropertyLiabilityAssociation(propertyId, firstLiabilityId);
            var association2 = new RealPropertyLiabilityAssociation(propertyId, secondLiabilityId);
            IMutableLqbAssociationSet<DataObjectKind.RealPropertyLiabilityAssociation, Guid, RealPropertyLiabilityAssociation> associationSet = LqbAssociationSet<DataObjectKind.RealPropertyLiabilityAssociation, Guid, RealPropertyLiabilityAssociation>.Create(
                Name.Create("Test").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.RealPropertyLiabilityAssociation>(), MaxFirstCardinality, MaxSecondCardinality);
            associationSet.BeginTrackingChanges();
            var firstAssocId = associationSet.Add(association1);
            var secondAssocId = associationSet.Add(association2);

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.Json);

                var mapperRP = new RealPropertyCollectionMapper(
                    loanIdentifier,
                    brokerIdentifier,
                    this.storedProcedureDriver);
                RealPropertyCollectionMapperTest.SaveCollection(mapperRP, collectionRP, this.connection, this.transaction);

                var pmlAuditTrailMapper = new LiabilityPmlAuditTrailMapper();
                var mapperL = new LiabilityCollectionMapper(
                    loanIdentifier,
                    brokerIdentifier,
                    NSubstitute.Substitute.For<ILiabilityDefaultsProvider>(),
                    pmlAuditTrailMapper,
                    this.storedProcedureDriver);
                LiabilityCollectionMapperTest.SaveCollection(mapperL, collectionL, this.connection, this.transaction);

                var mapperA = new RealPropertyLiabilityMapper(
                    loanIdentifier,
                    this.storedProcedureDriver);
                SaveAssociationSet(mapperA, associationSet, this.connection, this.transaction);

                associationSet = LoadAssociationSet(mapperA, this.connection, this.transaction);
                Assert.AreEqual(2, associationSet.Count);

                associationSet.BeginTrackingChanges();
                associationSet.Remove(firstAssocId);
                SaveAssociationSet(mapperA, associationSet, this.connection, this.transaction);

                associationSet = LoadAssociationSet(mapperA, this.connection, this.transaction);
                Assert.AreEqual(1, associationSet.Count);
            }
        }

        internal static IMutableLqbAssociationSet<DataObjectKind.RealPropertyLiabilityAssociation, Guid, RealPropertyLiabilityAssociation> LoadAssociationSet(
            ILqbCollectionSqlMapper<DataObjectKind.RealPropertyLiabilityAssociation, Guid, RealPropertyLiabilityAssociation> mapper,
            IDbConnection connection, IDbTransaction transaction)
        {
            return mapper.Load(
                connection,
                transaction,
                Name.Create("Test").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.RealPropertyLiabilityAssociation>(),
                null,
                new DataReaderConverter()) as IMutableLqbAssociationSet<DataObjectKind.RealPropertyLiabilityAssociation, Guid, RealPropertyLiabilityAssociation>;
        }

        internal static void SaveAssociationSet(
            ILqbCollectionSqlMapper<DataObjectKind.RealPropertyLiabilityAssociation, Guid, RealPropertyLiabilityAssociation> mapper,
            IMutableLqbAssociationSet<DataObjectKind.RealPropertyLiabilityAssociation, Guid, RealPropertyLiabilityAssociation> associationSet,
            IDbConnection connection, IDbTransaction transaction)
        {
            mapper.Save(
                connection,
                transaction,
                new SqlParameterConverter(),
                associationSet);
        }

        // Create a new test loan and return <brokerid, loanid>
        private void CreateLoan()
        {
            var tuple = CreatePrincipleAndNewLoan();
            this.principal = tuple.Item1;
            this.loanId = tuple.Item2;
        }

        public static Tuple<AbstractUserPrincipal, Guid> CreatePrincipleAndNewLoan()
        {
            var principal = LoginTools.LoginAs(TestAccountType.LoTest001);

            var creator = CLoanFileCreator.GetCreator(principal, LendersOffice.Audit.E_LoanCreationSource.UserCreateFromBlank);
            var loanId = creator.CreateBlankLoanFile();

            return new Tuple<AbstractUserPrincipal, Guid>(principal, loanId);
        }

        internal static void InactiveLoanFile(Guid loanId, AbstractUserPrincipal principal)
        {
            Tools.DeclareLoanFileInvalid(
                principal,
                loanId,
                sendNotifications: false,
                generateLinkLoanMsgEvent: false);
        }
    }
}
