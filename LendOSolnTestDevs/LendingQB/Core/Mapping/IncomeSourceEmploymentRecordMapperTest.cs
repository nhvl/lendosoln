﻿namespace LendingQB.Test.Developers.LendingQB.Core.Mapping
{
    using System;
    using System.Data;
    using System.Data.Common;
    using LendersOffice.Drivers.SqlServerDB;
    using LendersOffice.Security;
    using global::DataAccess;
    using global::LendingQB.Core;
    using global::LendingQB.Core.Data;
    using global::LendingQB.Core.Mapping;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;
    using NUnit.Framework;
    using Utils;
    using LqbGrammar.Exceptions;
    using NSubstitute;

    [TestFixture]
    [Category(CategoryConstants.IntegrationTest)]
    public sealed class IncomeSourceEmploymentRecordMapperTest
    {
        // Values taken from the file LendersOfficeLib/LendingQB/Core/AssociationData.xml
        // NOTE: cardinalities are set based on value of the OTHER phrase
        const int MaxFirstCardinality = -1;
        const int MaxSecondCardinality = 1;

        private DbConnection connection;
        private IDbTransaction transaction;
        private IStoredProcedureDriver storedProcedureDriver;

        private Guid loanId;
        private AbstractUserPrincipal principal;

        [TestFixtureSetUp]
        public void FixtureSetup()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                this.CreateLoan();

                var factory = new StoredProcedureDriverFactory();
                this.storedProcedureDriver = factory.Create(TimeoutInSeconds.Default);
            }
        }

        [TestFixtureTearDown]
        public void FixtureTearDown()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                DeactivateLoanFile(this.loanId, this.principal);
            }
        }

        [SetUp]
        public void MethodSetUp()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                this.connection = DbConnectionInfo.GetConnectionInfo(this.principal.BrokerId).GetConnection();
                this.connection.Open();
                this.transaction = connection.BeginTransaction();
            }
        }

        [TearDown]
        public void MethodTearDown()
        {
            this.transaction.Rollback();
            this.transaction.Dispose();
            this.connection.Close();
        }

        [Test]
        public void Add_MultipleIncomeSource_SameEmploymentRecord_ExpectFail()
        {
            var loanIdentifier = DataObjectIdentifier<DataObjectKind.Loan, Guid>.Create(this.loanId);
            var brokerIdentifier = DataObjectIdentifier<DataObjectKind.ClientCompany, Guid>.Create(this.principal.BrokerId);

            var income1 = GetDefaultIncomeTestRecord();
            var income2 = GetDefaultIncomeTestRecord();
            IMutableLqbCollection<DataObjectKind.IncomeSource, Guid, IncomeSource> collectionIS = LqbCollection<DataObjectKind.IncomeSource, Guid, IncomeSource>.Create(
                Name.Create("IncomeSource").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.IncomeSource>());
            collectionIS.BeginTrackingChanges();
            var firstIncomeId = collectionIS.Add(income1);
            var secondIncomeId = collectionIS.Add(income2);

            var employment = EmploymentRecordCollectionMapperTest.GetDefaultTestRecord();
            IMutableLqbCollection<DataObjectKind.EmploymentRecord, Guid, EmploymentRecord> collectionER = LqbCollection<DataObjectKind.EmploymentRecord, Guid, EmploymentRecord>.Create(
                Name.Create("EmploymentRecord").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.EmploymentRecord>());
            collectionER.BeginTrackingChanges();
            var employmentId = collectionER.Add(employment);

            var association1 = new IncomeSourceEmploymentRecordAssociation(firstIncomeId, employmentId);
            var association2 = new IncomeSourceEmploymentRecordAssociation(secondIncomeId, employmentId);
            IMutableLqbAssociationSet<DataObjectKind.IncomeSourceEmploymentRecordAssociation, Guid, IncomeSourceEmploymentRecordAssociation> associationSet = LqbAssociationSet<DataObjectKind.IncomeSourceEmploymentRecordAssociation, Guid, IncomeSourceEmploymentRecordAssociation>.Create(
                Name.Create("Test").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.IncomeSourceEmploymentRecordAssociation>(), MaxFirstCardinality, MaxSecondCardinality);
            associationSet.BeginTrackingChanges();
            var assocId = associationSet.Add(association1);

            Assert.Throws<CBaseException>(() => associationSet.Add(association2));
        }

        [Test]
        public void Add_SameIncomeSource_MultipleEmploymentRecord_ExpectSuccess()
        {
            var loanIdentifier = DataObjectIdentifier<DataObjectKind.Loan, Guid>.Create(this.loanId);
            var brokerIdentifier = DataObjectIdentifier<DataObjectKind.ClientCompany, Guid>.Create(this.principal.BrokerId);

            var income = GetDefaultIncomeTestRecord();
            IMutableLqbCollection<DataObjectKind.IncomeSource, Guid, IncomeSource> collectionIS = LqbCollection<DataObjectKind.IncomeSource, Guid, IncomeSource>.Create(
                Name.Create("IncomeSource").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.IncomeSource>());
            collectionIS.BeginTrackingChanges();
            var incomeId = collectionIS.Add(income);

            var employment1 = EmploymentRecordCollectionMapperTest.GetDefaultTestRecord();
            var employment2 = EmploymentRecordCollectionMapperTest.GetDefaultTestRecord();
            IMutableLqbCollection<DataObjectKind.EmploymentRecord, Guid, EmploymentRecord> collectionER = LqbCollection<DataObjectKind.EmploymentRecord, Guid, EmploymentRecord>.Create(
                Name.Create("EmploymentRecord").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.EmploymentRecord>());
            collectionER.BeginTrackingChanges();
            var firstEmploymentId = collectionER.Add(employment1);
            var secondEmploymentId = collectionER.Add(employment2);

            var association1 = new IncomeSourceEmploymentRecordAssociation(incomeId, firstEmploymentId);
            var association2 = new IncomeSourceEmploymentRecordAssociation(incomeId, secondEmploymentId);
            IMutableLqbAssociationSet<DataObjectKind.IncomeSourceEmploymentRecordAssociation, Guid, IncomeSourceEmploymentRecordAssociation> associationSet = LqbAssociationSet<DataObjectKind.IncomeSourceEmploymentRecordAssociation, Guid, IncomeSourceEmploymentRecordAssociation>.Create(
                Name.Create("Test").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.IncomeSourceEmploymentRecordAssociation>(), MaxFirstCardinality, MaxSecondCardinality);
            associationSet.BeginTrackingChanges();
            var assocId = associationSet.Add(association1);

            // adding the second should be ok for OneToMany cardinality
            assocId = associationSet.Add(association2);
        }

        [Test]
        public void Save_Populated()
        {
            var loanIdentifier = DataObjectIdentifier<DataObjectKind.Loan, Guid>.Create(this.loanId);
            var brokerIdentifier = DataObjectIdentifier<DataObjectKind.ClientCompany, Guid>.Create(this.principal.BrokerId);

            var income = GetDefaultIncomeTestRecord();
            IMutableLqbCollection<DataObjectKind.IncomeSource, Guid, IncomeSource> collectionIS = LqbCollection<DataObjectKind.IncomeSource, Guid, IncomeSource>.Create(
                Name.Create("IncomeSource").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.IncomeSource>());
            collectionIS.BeginTrackingChanges();
            var incomeId = collectionIS.Add(income);
            Assert.AreNotEqual(Guid.Empty, incomeId.Value);

            var employment = EmploymentRecordCollectionMapperTest.GetDefaultTestRecord();
            IMutableLqbCollection<DataObjectKind.EmploymentRecord, Guid, EmploymentRecord> collectionER = LqbCollection<DataObjectKind.EmploymentRecord, Guid, EmploymentRecord>.Create(
                Name.Create("EmploymentRecord").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.EmploymentRecord>());
            collectionER.BeginTrackingChanges();
            var employmentId = collectionER.Add(employment);
            Assert.AreNotEqual(Guid.Empty, employmentId.Value);

            var association = new IncomeSourceEmploymentRecordAssociation(incomeId, employmentId);
            IMutableLqbAssociationSet<DataObjectKind.IncomeSourceEmploymentRecordAssociation, Guid, IncomeSourceEmploymentRecordAssociation> associationSet = LqbAssociationSet<DataObjectKind.IncomeSourceEmploymentRecordAssociation, Guid, IncomeSourceEmploymentRecordAssociation>.Create(
                Name.Create("Test").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.IncomeSourceEmploymentRecordAssociation>(), MaxFirstCardinality, MaxSecondCardinality);
            associationSet.BeginTrackingChanges();
            var assocId = associationSet.Add(association);
            Assert.AreNotEqual(Guid.Empty, assocId.Value);

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.StoredProcedure);

                // save data
                var mapperIS = new IncomeSourceCollectionMapper(
                    loanIdentifier,
                    brokerIdentifier,
                    this.storedProcedureDriver);
                SaveCollection(mapperIS, collectionIS, this.connection, this.transaction);

                var voeDataMapper = new EmploymentRecordEmploymentRecordVoeDataMapper();
                var incomeRecordMapper = new IncomeRecordCollectionMapper();
                var mapperER = new EmploymentRecordCollectionMapper(
                    loanIdentifier,
                    brokerIdentifier,
                    Substitute.For<IEmploymentRecordDefaultsProvider>(),
                    voeDataMapper,
                    incomeRecordMapper,
                    this.storedProcedureDriver);
                EmploymentRecordCollectionMapperTest.SaveEmpCollection(this.connection, this.transaction, mapperER, collectionER);

                var mapperA = new IncomeSourceEmploymentRecordMapper(
                    loanIdentifier,
                    this.storedProcedureDriver);
                SaveAssociationSet(mapperA, associationSet, this.connection, this.transaction);

                // re-load
                associationSet = LoadAssociationSet(mapperA, this.connection, this.transaction);
                var loaded = associationSet[assocId];

                // check field values
                Assert.AreEqual(incomeId.Value, loaded.IncomeSourceId.Value);
                Assert.AreEqual(employmentId.Value, loaded.EmploymentRecordId.Value);
            }
        }

        [Test]
        public void DeleteTest()
        {
            var loanIdentifier = DataObjectIdentifier<DataObjectKind.Loan, Guid>.Create(this.loanId);
            var brokerIdentifier = DataObjectIdentifier<DataObjectKind.ClientCompany, Guid>.Create(this.principal.BrokerId);

            var income = GetDefaultIncomeTestRecord();
            IMutableLqbCollection<DataObjectKind.IncomeSource, Guid, IncomeSource> collectionIS = LqbCollection<DataObjectKind.IncomeSource, Guid, IncomeSource>.Create(
                Name.Create("IncomeSource").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.IncomeSource>());
            collectionIS.BeginTrackingChanges();
            var incomeId = collectionIS.Add(income);

            var employment1 = EmploymentRecordCollectionMapperTest.GetDefaultTestRecord();
            var employment2 = EmploymentRecordCollectionMapperTest.GetDefaultTestRecord();
            IMutableLqbCollection<DataObjectKind.EmploymentRecord, Guid, EmploymentRecord> collectionER = LqbCollection<DataObjectKind.EmploymentRecord, Guid, EmploymentRecord>.Create(
                Name.Create("EmploymentRecord").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.EmploymentRecord>());
            collectionER.BeginTrackingChanges();
            var firstEmploymentId = collectionER.Add(employment1);
            var secondEmploymentId = collectionER.Add(employment2);

            var association1 = new IncomeSourceEmploymentRecordAssociation(incomeId, firstEmploymentId);
            var association2 = new IncomeSourceEmploymentRecordAssociation(incomeId, secondEmploymentId);
            IMutableLqbAssociationSet<DataObjectKind.IncomeSourceEmploymentRecordAssociation, Guid, IncomeSourceEmploymentRecordAssociation> associationSet = LqbAssociationSet<DataObjectKind.IncomeSourceEmploymentRecordAssociation, Guid, IncomeSourceEmploymentRecordAssociation>.Create(
                Name.Create("Test").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.IncomeSourceEmploymentRecordAssociation>(), MaxFirstCardinality, MaxSecondCardinality);
            associationSet.BeginTrackingChanges();
            var firstAssocId = associationSet.Add(association1);
            var secondAssocId = associationSet.Add(association2);

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.StoredProcedure);

                // save data
                var mapperIS = new IncomeSourceCollectionMapper(
                    loanIdentifier,
                    brokerIdentifier,
                    this.storedProcedureDriver);
                SaveCollection(mapperIS, collectionIS, this.connection, this.transaction);

                var voeDataMapper = new EmploymentRecordEmploymentRecordVoeDataMapper();
                var incomeRecordMapper = new IncomeRecordCollectionMapper();
                var mapperER = new EmploymentRecordCollectionMapper(
                    loanIdentifier,
                    brokerIdentifier,
                    Substitute.For<IEmploymentRecordDefaultsProvider>(),
                    voeDataMapper,
                    incomeRecordMapper,
                    this.storedProcedureDriver);
                EmploymentRecordCollectionMapperTest.SaveEmpCollection(this.connection, this.transaction, mapperER, collectionER);

                var mapperA = new IncomeSourceEmploymentRecordMapper(
                    loanIdentifier,
                    this.storedProcedureDriver);
                SaveAssociationSet(mapperA, associationSet, this.connection, this.transaction);

                associationSet = LoadAssociationSet(mapperA, this.connection, this.transaction);
                Assert.AreEqual(2, associationSet.Count);

                associationSet.BeginTrackingChanges();
                associationSet.Remove(firstAssocId);
                SaveAssociationSet(mapperA, associationSet, this.connection, this.transaction);

                associationSet = LoadAssociationSet(mapperA, this.connection, this.transaction);
                Assert.AreEqual(1, associationSet.Count);
            }
        }

        // Create a new test loan and return <brokerid, loanid>
        private void CreateLoan()
        {
            var tuple = CreatePrincipleAndNewLoan();
            this.principal = tuple.Item1;
            this.loanId = tuple.Item2;
        }

        public static Tuple<AbstractUserPrincipal, Guid> CreatePrincipleAndNewLoan()
        {
            var principal = LoginTools.LoginAs(TestAccountType.LoTest001);

            var creator = CLoanFileCreator.GetCreator(principal, LendersOffice.Audit.E_LoanCreationSource.UserCreateFromBlank);
            var loanId = creator.CreateBlankUladLoanFile();

            return new Tuple<AbstractUserPrincipal, Guid>(principal, loanId);
        }

        internal static void DeactivateLoanFile(Guid loanId, AbstractUserPrincipal principal)
        {
            Tools.DeclareLoanFileInvalid(
                principal,
                loanId,
                sendNotifications: false,
                generateLinkLoanMsgEvent: false);
        }

        internal static IncomeSource GetDefaultIncomeTestRecord()
        {
            var record = new IncomeSource();
            record.Description = DescriptionField.Create("Income Source");
            record.IncomeType = IncomeType.BaseIncome;
            record.MonthlyAmountData = Money.Create(1000000.72m);
 
            return record;
        }

        internal static void SaveCollection(
            ILqbCollectionSqlMapper<DataObjectKind.IncomeSource, Guid, IncomeSource> mapper,
            IMutableLqbCollection<DataObjectKind.IncomeSource, Guid, IncomeSource> collection,
            DbConnection connection, IDbTransaction transaction)
        {
            mapper.Save(
                connection,
                transaction,
                new SqlParameterConverter(),
                collection);
        }

        internal static IMutableLqbAssociationSet<DataObjectKind.IncomeSourceEmploymentRecordAssociation, Guid, IncomeSourceEmploymentRecordAssociation> LoadAssociationSet(
            ILqbCollectionSqlMapper<DataObjectKind.IncomeSourceEmploymentRecordAssociation, Guid, IncomeSourceEmploymentRecordAssociation> mapper,
            IDbConnection connection, IDbTransaction transaction)
        {
            return mapper.Load(
                connection,
                transaction,
                Name.Create("Test").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.IncomeSourceEmploymentRecordAssociation>(),
                null,
                new DataReaderConverter()) as IMutableLqbAssociationSet<DataObjectKind.IncomeSourceEmploymentRecordAssociation, Guid, IncomeSourceEmploymentRecordAssociation>;
        }

        internal static void SaveAssociationSet(
            ILqbCollectionSqlMapper<DataObjectKind.IncomeSourceEmploymentRecordAssociation, Guid, IncomeSourceEmploymentRecordAssociation> mapper,
            IMutableLqbAssociationSet<DataObjectKind.IncomeSourceEmploymentRecordAssociation, Guid, IncomeSourceEmploymentRecordAssociation> associationSet,
            IDbConnection connection, IDbTransaction transaction)
        {
            mapper.Save(
                connection,
                transaction,
                new SqlParameterConverter(),
                associationSet);
        }
    }
}
