﻿namespace LendingQB.Test.Developers.LendingQB.Core.Mapping
{
    using System;
    using System.Data;
    using System.Data.Common;
    using Fakes;
    using global::DataAccess;
    using global::LendingQB.Core;
    using global::LendingQB.Core.Data;
    using global::LendingQB.Core.Mapping;
    using LendersOffice.Constants;
    using LendersOffice.Drivers.SqlServerDB;
    using LendersOffice.Security;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;
    using NUnit.Framework;
    using Utils;

    [TestFixture]
    [Category(CategoryConstants.IntegrationTest)]
    public sealed class ConsumerAssetMapperTest
    {
        private const int MaxFirstCardinality = -1;
        private const int MaxSecondCardinality = -1;

        private DbConnection connection;
        private IDbTransaction transaction;
        private IStoredProcedureDriver storedProcedureDriver;

        private Guid loanId;
        private Guid appId;
        private Guid borrowerId;
        private AbstractUserPrincipal principal;

        [TestFixtureSetUp]
        public void FixtureSetup()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                this.CreateLoan();

                var factory = new StoredProcedureDriverFactory();
                this.storedProcedureDriver = factory.Create(TimeoutInSeconds.Default);
            }
        }

        [SetUp]
        public void MethodSetUp()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                this.connection = DbConnectionInfo.GetConnectionInfo(this.principal.BrokerId).GetConnection();
                this.connection.Open();
                this.transaction = connection.BeginTransaction();
            }
        }

        [TearDown]
        public void MethodTearDown()
        {
            this.transaction.Rollback();
            this.transaction.Dispose();
            this.connection.Close();
        }

        [Test]
        public void Save_Populated()
        {
            var loanIdentifier = DataObjectIdentifier<DataObjectKind.Loan, Guid>.Create(this.loanId);
            var brokerIdentifier = DataObjectIdentifier<DataObjectKind.ClientCompany, Guid>.Create(this.principal.BrokerId);
            var borrowerIdentifier = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(this.borrowerId);
            var appIdentifier = DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>.Create(this.appId);

            var asset = AssetCollectionMapperTest.GetDefaultTestRecord();
            IMutableLqbCollection<DataObjectKind.Asset, Guid, Asset> collectionA = LqbCollection<DataObjectKind.Asset, Guid, Asset>.Create(
                Name.Create("Asset").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.Asset>());
            collectionA.BeginTrackingChanges();
            var assetId = collectionA.Add(asset);
            Assert.AreNotEqual(Guid.Empty, assetId.Value);

            var association = new ConsumerAssetAssociation(borrowerIdentifier, assetId, appIdentifier);
            association.IsPrimary = true;
            IMutableLqbAssociationSet<DataObjectKind.ConsumerAssetAssociation, Guid, ConsumerAssetAssociation> associationSet = LqbAssociationSet<DataObjectKind.ConsumerAssetAssociation, Guid, ConsumerAssetAssociation>.Create(
                Name.Create("Test").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.ConsumerAssetAssociation>(), MaxFirstCardinality, MaxSecondCardinality);
            associationSet.BeginTrackingChanges();
            var assocId = associationSet.Add(association);
            Assert.AreNotEqual(Guid.Empty, assocId.Value);

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.StoredProcedure);

                // save data
                var mapperA = new AssetCollectionMapper(
                    loanIdentifier,
                    brokerIdentifier,
                    this.storedProcedureDriver);
                AssetCollectionMapperTest.Save(mapperA, collectionA, this.connection, this.transaction);

                var mapper = new ConsumerAssetMapper(
                    loanIdentifier,
                    this.storedProcedureDriver);
                ConsumerAssetMapperTest.SaveAssociationSet(mapper, associationSet, this.connection, this.transaction);

                // re-load
                associationSet = ConsumerAssetMapperTest.LoadAssociationSet(mapper, this.connection, this.transaction);
                var loaded = associationSet[assocId];

                // check field values
                Assert.AreEqual(borrowerIdentifier, loaded.ConsumerId);
                Assert.AreEqual(assetId, loaded.AssetId);
            }
        }

        [Test]
        public void DeleteTest()
        {
            var loanIdentifier = DataObjectIdentifier<DataObjectKind.Loan, Guid>.Create(this.loanId);
            var brokerIdentifier = DataObjectIdentifier<DataObjectKind.ClientCompany, Guid>.Create(this.principal.BrokerId);
            var borrowerIdentifier = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(this.borrowerId);
            var appIdentifier = DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>.Create(this.appId);

            var asset1 = AssetCollectionMapperTest.GetDefaultTestRecord();
            var asset2 = AssetCollectionMapperTest.GetDefaultTestRecord();
            IMutableLqbCollection<DataObjectKind.Asset, Guid, Asset> collectionA = LqbCollection<DataObjectKind.Asset, Guid, Asset>.Create(
                Name.Create("Asset").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.Asset>());
            collectionA.BeginTrackingChanges();
            var firstAssetId = collectionA.Add(asset1);
            var secondAssetId = collectionA.Add(asset1);

            var association1 = new ConsumerAssetAssociation(borrowerIdentifier, firstAssetId, appIdentifier);
            association1.IsPrimary = true;
            var association2 = new ConsumerAssetAssociation(borrowerIdentifier, secondAssetId, appIdentifier);
            association2.IsPrimary = true;
            IMutableLqbAssociationSet<DataObjectKind.ConsumerAssetAssociation, Guid, ConsumerAssetAssociation> associationSet = LqbAssociationSet<DataObjectKind.ConsumerAssetAssociation, Guid, ConsumerAssetAssociation>.Create(
                Name.Create("Test").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.ConsumerAssetAssociation>(), MaxFirstCardinality, MaxSecondCardinality);
            associationSet.BeginTrackingChanges();
            var firstAssocId = associationSet.Add(association1);
            var secondAssocId = associationSet.Add(association2);

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.Json);

                var mapperA = new AssetCollectionMapper(
                    loanIdentifier,
                    brokerIdentifier,
                    this.storedProcedureDriver);
                AssetCollectionMapperTest.Save(mapperA, collectionA, this.connection, this.transaction);

                var mapper = new ConsumerAssetMapper(
                    loanIdentifier,
                    this.storedProcedureDriver);
                SaveAssociationSet(mapper, associationSet, this.connection, this.transaction);

                associationSet = LoadAssociationSet(mapper, this.connection, this.transaction);
                Assert.AreEqual(2, associationSet.Count);

                associationSet.BeginTrackingChanges();
                associationSet.Remove(firstAssocId);
                SaveAssociationSet(mapper, associationSet, this.connection, this.transaction);

                associationSet = LoadAssociationSet(mapper, this.connection, this.transaction);
                Assert.AreEqual(1, associationSet.Count);
            }
        }

        private static IMutableLqbAssociationSet<DataObjectKind.ConsumerAssetAssociation, Guid, ConsumerAssetAssociation> LoadAssociationSet(
            ILqbCollectionSqlMapper<DataObjectKind.ConsumerAssetAssociation, Guid, ConsumerAssetAssociation> mapper,
            IDbConnection connection, IDbTransaction transaction)
        {
            return mapper.Load(
                connection,
                transaction,
                Name.Create("Test").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.ConsumerAssetAssociation>(),
                null,
                new DataReaderConverter()) as IMutableLqbAssociationSet<DataObjectKind.ConsumerAssetAssociation, Guid, ConsumerAssetAssociation>;
        }

        private static void SaveAssociationSet(
            ILqbCollectionSqlMapper<DataObjectKind.ConsumerAssetAssociation, Guid, ConsumerAssetAssociation> mapper,
            IMutableLqbAssociationSet<DataObjectKind.ConsumerAssetAssociation, Guid, ConsumerAssetAssociation> associationSet,
            IDbConnection connection, IDbTransaction transaction)
        {
            mapper.Save(
                connection,
                transaction,
                new SqlParameterConverter(),
                associationSet);
        }

        // Create a new test loan and store the identifiers
        private void CreateLoan()
        {
            this.principal = LoginTools.LoginAs(TestAccountType.LoTest001);

            var dataLoan = FakePageData.Create(appCount: 1);
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            var dataApp = dataLoan.GetAppData(0);
            dataApp.aBFirstNm = "Frosty";
            dataApp.aBLastNm = "Snowman";
            dataApp.aBSsn = "555-55-5555";
            dataLoan.Save();

            this.loanId = dataLoan.sLId;
            this.appId = dataApp.aAppId;
            this.borrowerId = dataApp.aBConsumerId;
        }
    }
}
