﻿namespace LendingQB.Test.Developers.LendingQB.Core.Mapping
{
    using System;
    using global::LendingQB.Core;
    using global::LendingQB.Core.Mapping;
    using LqbGrammar.DataTypes;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public sealed class OrderCollectionLookupTest
    {
        [Test]
        public void GetCollectionOrderAfterAdd_WithoutName()
        {
            var lookup = new OrderCollectionLookup();

            var appId = DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>.Create(Guid.NewGuid());
            var collectionOrder = CollectionUtil.CreateCollectionOrder<Guid>("Asset");

            IInitializeCollectionOrder<Guid> retrieved;
            lookup.AddCollectionOrder<DataObjectKind.Asset>(collectionOrder, appId, null, null);
            bool found = lookup.TryGetValue<DataObjectKind.Asset>(appId, null, null, out retrieved);

            Assert.IsTrue(found);
        }

        [Test]
        public void GetCollectionOrderAfterAdd_WithName()
        {
            var lookup = new OrderCollectionLookup();

            string name = "Asset";
            var appId = DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>.Create(Guid.NewGuid());
            var collectionOrder = CollectionUtil.CreateCollectionOrder<Guid>(name);

            IInitializeCollectionOrder<Guid> retrieved;
            lookup.AddCollectionOrder(collectionOrder, appId, null, null, name);
            bool found = lookup.TryGetValue(appId, null, null, name, out retrieved);

            Assert.IsTrue(found);
        }

        [Test]
        public void ClearTest()
        {
            var lookup = new OrderCollectionLookup();

            string name = "Asset";
            var appId = DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>.Create(Guid.NewGuid());
            var collectionOrder = CollectionUtil.CreateCollectionOrder<Guid>(name);
            lookup.AddCollectionOrder(collectionOrder, appId, null, null, name);

            int count = this.GetCountFromLookup(lookup);
            Assert.AreEqual(1, count);

            lookup.Clear();
            count = this.GetCountFromLookup(lookup);
            Assert.AreEqual(0, count);
        }

        [Test]
        public void CountIsThreeForDifferentCollections()
        {
            var lookup = new OrderCollectionLookup();

            string name = "Asset";
            var appId = DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>.Create(Guid.NewGuid());
            var consumerId = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(Guid.NewGuid());
            var uladId = DataObjectIdentifier<DataObjectKind.UladApplication, Guid>.Create(Guid.NewGuid());
            var collectionOrder1 = CollectionUtil.CreateCollectionOrder<Guid>(name);
            var collectionOrder2 = CollectionUtil.CreateCollectionOrder<Guid>(name);
            var collectionOrder3 = CollectionUtil.CreateCollectionOrder<Guid>(name);
            lookup.AddCollectionOrder(collectionOrder1, appId, null, null, name);
            lookup.AddCollectionOrder(collectionOrder2, null, consumerId, null, name);
            lookup.AddCollectionOrder(collectionOrder3, null, null, uladId, name);

            int count = this.GetCountFromLookup(lookup);
            Assert.AreEqual(3, count);
        }

        [Test]
        public void CheckOrderKeyData()
        {
            var lookup = new OrderCollectionLookup();

            string name = "Asset";
            var appId = DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>.Create(Guid.NewGuid());
            var consumerId = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(Guid.NewGuid());
            var uladId = DataObjectIdentifier<DataObjectKind.UladApplication, Guid>.Create(Guid.NewGuid());
            var collectionOrder1 = CollectionUtil.CreateCollectionOrder<Guid>(name);
            var collectionOrder2 = CollectionUtil.CreateCollectionOrder<Guid>(name);
            var collectionOrder3 = CollectionUtil.CreateCollectionOrder<Guid>(name);
            lookup.AddCollectionOrder(collectionOrder1, appId, null, null, name);
            lookup.AddCollectionOrder(collectionOrder2, null, consumerId, null, name);
            lookup.AddCollectionOrder(collectionOrder3, null, null, uladId, name);

            int count = 0;
            foreach (var keyValue in lookup.OrderCollections)
            {
                count++;

                var key = keyValue.Key;
                var value = keyValue.Value;

                if (count == 1)
                {
                    Assert.AreEqual(appId.Value, key.LegacyApplicationId);
                    Assert.AreEqual(name, key.CollectionName);
                    Assert.IsTrue(key.ConsumerId == null);
                    Assert.IsTrue(key.UladApplicationId == null);

                    Assert.IsTrue(object.ReferenceEquals(collectionOrder1, value));
                }
                else if (count == 2)
                {
                    Assert.AreEqual(consumerId.Value, key.ConsumerId);
                    Assert.AreEqual(name, key.CollectionName);
                    Assert.IsTrue(key.LegacyApplicationId == null);
                    Assert.IsTrue(key.UladApplicationId == null);

                    Assert.IsTrue(object.ReferenceEquals(collectionOrder2, value));
                }
                else if (count == 3)
                {
                    Assert.AreEqual(uladId.Value, key.UladApplicationId);
                    Assert.AreEqual(name, key.CollectionName);
                    Assert.IsTrue(key.LegacyApplicationId == null);
                    Assert.IsTrue(key.ConsumerId == null);

                    Assert.IsTrue(object.ReferenceEquals(collectionOrder3, value));
                }
            }

            Assert.AreEqual(3, count);
        }

        private int GetCountFromLookup(OrderCollectionLookup lookup)
        {
            int count = 0;
            foreach (var keyValue in lookup.OrderCollections)
            {
                count++;
            }

            return count;
        }
    }
}
