﻿namespace LendingQB.Test.Developers.LendingQB.Core.Mapping
{
    using System;
    using System.Data.Common;
    using DataAccess;
    using LendersOffice.Security;
    using NUnit.Framework;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;
    using LendersOffice.Drivers.SqlServerDB;
    using Utils;
    using System.Data;
    using global::LendingQB.Core.Mapping;
    using global::LendingQB.Core;
    using global::LendingQB.Core.Data;
    using global::DataAccess;

    [TestFixture]
    [Category(CategoryConstants.IntegrationTest)]
    public class AssetCollectionMapperTest
    {
        private AbstractUserPrincipal principal;
        private DataObjectIdentifier<DataObjectKind.Loan, Guid> loanId;
        private DataObjectIdentifier<DataObjectKind.ClientCompany, Guid> brokerId;
        private IStoredProcedureDriver storedProcedureDriver;
        private IDbConnection connection;
        private IDbTransaction transaction;

        [TestFixtureSetUp]
        public void FixtureSetUp()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                this.principal = LoginTools.LoginAs(TestAccountType.LoTest001);
                this.brokerId = DataObjectIdentifier<DataObjectKind.ClientCompany, Guid>.Create(principal.BrokerId);
                var creator = CLoanFileCreator.GetCreator(
                    this.principal,
                    LendersOffice.Audit.E_LoanCreationSource.UserCreateFromBlank);
                this.loanId = DataObjectIdentifier<DataObjectKind.Loan, Guid>.Create(creator.CreateBlankLoanFile());

                var factory = new StoredProcedureDriverFactory();
                this.storedProcedureDriver = factory.Create(TimeoutInSeconds.Default);
            }
        }

        [TestFixtureTearDown]
        public void FixtureTearDown()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                Tools.DeclareLoanFileInvalid(
                    this.principal,
                    this.loanId.Value,
                    sendNotifications: false,
                    generateLinkLoanMsgEvent: false);
            }
        }

        [SetUp]
        public void SetUp()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                this.connection = DbConnectionInfo.GetConnectionInfo(this.brokerId.Value).GetConnection();
                this.connection.Open();
                this.transaction = connection.BeginTransaction();
            }
        }

        [TearDown]
        public void TearDown()
        {
            this.transaction.Rollback();
            this.transaction.Dispose();
            this.connection.Dispose();
        }

        [Test]
        public void Load_AssumingNoDatabaseIssues_Succeeds()
        {
            var mapper = new AssetCollectionMapper(
                this.loanId,
                this.brokerId,
                this.storedProcedureDriver);

            this.LoadCollection(mapper);
        }

        [Test]
        public void Save_NewRecord_PersistsEnteredDataPoints()
        {
            var mapper = new AssetCollectionMapper(
                this.loanId,
                this.brokerId,
                this.storedProcedureDriver);
            IMutableLqbCollection<DataObjectKind.Asset, Guid, Asset> collection = LqbCollection<DataObjectKind.Asset, Guid, Asset>.Create(
                Name.Create("Test").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.Asset>());
            Asset originalRecord = GetDefaultTestRecord();

            collection.BeginTrackingChanges();
            var id = collection.Add(originalRecord);
            this.SaveCollection(mapper, collection);
            collection = this.LoadCollection(mapper);
            var loadedRecord = collection[id];

            Assert.AreEqual(originalRecord.AccountName, loadedRecord.AccountName);
            Assert.AreEqual(originalRecord.AccountNum, loadedRecord.AccountNum);
            Assert.AreEqual(originalRecord.AssetCashDepositType, loadedRecord.AssetCashDepositType);
            Assert.AreEqual(originalRecord.AssetType, loadedRecord.AssetType);
            Assert.AreEqual(originalRecord.Attention, loadedRecord.Attention);
            Assert.AreEqual(originalRecord.City, loadedRecord.City);
            Assert.AreEqual(originalRecord.CompanyName, loadedRecord.CompanyName);
            Assert.AreEqual(originalRecord.DepartmentName, loadedRecord.DepartmentName);
            Assert.AreEqual(originalRecord.Desc, loadedRecord.Desc);
            Assert.AreEqual(originalRecord.FaceValue, loadedRecord.FaceValue);
            Assert.AreEqual(originalRecord.GiftSource, loadedRecord.GiftSource);
            Assert.AreEqual(originalRecord.IsEmptyCreated, loadedRecord.IsEmptyCreated);
            Assert.AreEqual(originalRecord.IsSeeAttachment, loadedRecord.IsSeeAttachment);
            Assert.AreEqual(originalRecord.OtherTypeDesc, loadedRecord.OtherTypeDesc);
            Assert.AreEqual(originalRecord.Phone, loadedRecord.Phone);
            Assert.AreEqual(originalRecord.PrepDate, loadedRecord.PrepDate);
            Assert.AreEqual(originalRecord.State, loadedRecord.State);
            Assert.AreEqual(originalRecord.StreetAddress, loadedRecord.StreetAddress);
            Assert.AreEqual(originalRecord.Value, loadedRecord.Value);
            Assert.AreEqual(originalRecord.VerifExpiresDate, loadedRecord.VerifExpiresDate);
            Assert.AreEqual(originalRecord.VerifRecvDate, loadedRecord.VerifRecvDate);
            Assert.AreEqual(originalRecord.VerifReorderedDate, loadedRecord.VerifReorderedDate);
            Assert.AreEqual(originalRecord.VerifSentDate, loadedRecord.VerifSentDate);
            Assert.AreEqual(originalRecord.Zip, loadedRecord.Zip);
        }

        [Test]
        public void Save_EmptyRecord_PersistsEmptyRecord()
        {
            var mapper = new AssetCollectionMapper(
                this.loanId,
                this.brokerId,
                this.storedProcedureDriver);
            IMutableLqbCollection<DataObjectKind.Asset, Guid, Asset> collection = LqbCollection<DataObjectKind.Asset, Guid, Asset>.Create(
                Name.Create("Test").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.Asset>());
            var originalRecord = new Asset();

            collection.BeginTrackingChanges();
            var id = collection.Add(originalRecord);
            this.SaveCollection(mapper, collection);
            collection = this.LoadCollection(mapper);
            var loadedRecord = collection[id];

            Assert.AreEqual(null, loadedRecord.AccountName);
            Assert.AreEqual(null, loadedRecord.AccountNum);
            Assert.AreEqual(null, loadedRecord.AssetCashDepositType);
            Assert.AreEqual(null, loadedRecord.AssetType);
            Assert.AreEqual(null, loadedRecord.Attention);
            Assert.AreEqual(null, loadedRecord.City);
            Assert.AreEqual(null, loadedRecord.CompanyName);
            Assert.AreEqual(null, loadedRecord.DepartmentName);
            Assert.AreEqual(null, loadedRecord.Desc);
            Assert.AreEqual(null, loadedRecord.FaceValue);
            Assert.AreEqual(null, loadedRecord.GiftSourceData);
            Assert.AreEqual(null, loadedRecord.IsEmptyCreated);
            Assert.AreEqual(null, loadedRecord.IsSeeAttachment);
            Assert.AreEqual(null, loadedRecord.OtherTypeDesc);
            Assert.AreEqual(null, loadedRecord.Phone);
            Assert.AreEqual(null, loadedRecord.PrepDate);
            Assert.AreEqual(null, loadedRecord.State);
            Assert.AreEqual(null, loadedRecord.StreetAddress);
            Assert.AreEqual(null, loadedRecord.Value);
            Assert.AreEqual(null, loadedRecord.VerifExpiresDate);
            Assert.AreEqual(null, loadedRecord.VerifRecvDate);
            Assert.AreEqual(null, loadedRecord.VerifReorderedDate);
            Assert.AreEqual(null, loadedRecord.VerifSentDate);
            Assert.AreEqual(null, loadedRecord.Zip);
        }

        [Test]
        public void Save_UpdateExistingRecord_PersistsUpdatedDataPoints()
        {
            var mapper = new AssetCollectionMapper(
                this.loanId,
                this.brokerId,
                this.storedProcedureDriver);
            IMutableLqbCollection<DataObjectKind.Asset, Guid, Asset> collection = LqbCollection<DataObjectKind.Asset, Guid, Asset>.Create(
                Name.Create("Test").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.Asset>());
            var originalRecord = GetDefaultTestRecord();

            collection.BeginTrackingChanges();
            var id = collection.Add(originalRecord);
            this.SaveCollection(mapper, collection);

            collection = this.LoadCollection(mapper);
            collection.BeginTrackingChanges();
            var loadedRecord = collection[id];

            loadedRecord.AccountName = DescriptionField.Create("Wells Savings");
            loadedRecord.AccountNum = BankAccountNumber.Create("002792-82");
            loadedRecord.AssetCashDepositType =  E_AssetCashDepositT.CashDeposit1;
            loadedRecord.AssetType =  E_AssetT.Savings;
            loadedRecord.Attention = EntityName.Create("Billy Banker");
            loadedRecord.City = City.Create("Costa Mesa");
            loadedRecord.CompanyName = EntityName.Create("Wells Fargo");
            loadedRecord.DepartmentName = DescriptionField.Create("Accounts");
            loadedRecord.Desc = DescriptionField.Create("Wells Fargo Savings Acct.");
            loadedRecord.FaceValue = Money.Create( 190493.78m );
            loadedRecord.GiftSourceData =  E_GiftFundSourceT.Blank;
            loadedRecord.IsEmptyCreated = false;
            loadedRecord.IsSeeAttachment = false;
            loadedRecord.OtherTypeDesc = DescriptionField.Create("None.");
            loadedRecord.Phone = PhoneNumber.Create("188855555555");
            loadedRecord.PrepDate = UnzonedDate.Create(DateTime.Parse("11/7/16"));
            loadedRecord.State = UnitedStatesPostalState.Create("CA");
            loadedRecord.StreetAddress = StreetAddress.Create("123 Bank Blvd.");
            loadedRecord.Value = Money.Create(18520.11m);
            loadedRecord.VerifExpiresDate = UnzonedDate.Create(DateTime.Parse("11/8/16"));
            loadedRecord.VerifRecvDate = UnzonedDate.Create(DateTime.Parse("11/9/16"));
            loadedRecord.VerifReorderedDate = UnzonedDate.Create(DateTime.Parse("11/10/16"));
            loadedRecord.VerifSentDate = UnzonedDate.Create(DateTime.Parse("11/11/16"));
            loadedRecord.Zip = Zipcode.Create("92626");

            this.SaveCollection(mapper, collection);

            collection = this.LoadCollection(mapper);
            var updatedRecord = collection[id];

            Assert.AreEqual(DescriptionField.Create("Wells Savings"), updatedRecord.AccountName);
            Assert.AreEqual(BankAccountNumber.Create("002792-82"), updatedRecord.AccountNum);
            Assert.AreEqual(E_AssetCashDepositT.CashDeposit1, updatedRecord.AssetCashDepositType);
            Assert.AreEqual(E_AssetT.Savings, updatedRecord.AssetType);
            Assert.AreEqual(EntityName.Create("Billy Banker"), updatedRecord.Attention);
            Assert.AreEqual(City.Create("Costa Mesa"), updatedRecord.City);
            Assert.AreEqual(EntityName.Create("Wells Fargo"), updatedRecord.CompanyName);
            Assert.AreEqual(DescriptionField.Create("Accounts"), updatedRecord.DepartmentName);
            Assert.AreEqual(DescriptionField.Create("Wells Fargo Savings Acct."), updatedRecord.Desc);
            Assert.AreEqual(Money.Create(190493.78m), updatedRecord.FaceValue);
            Assert.AreEqual(E_GiftFundSourceT.Blank, updatedRecord.GiftSource);
            Assert.AreEqual(false, updatedRecord.IsEmptyCreated);
            Assert.AreEqual(false, updatedRecord.IsSeeAttachment);
            Assert.AreEqual(DescriptionField.Create("None."), updatedRecord.OtherTypeDesc);
            Assert.AreEqual(PhoneNumber.Create("188855555555"), updatedRecord.Phone);
            Assert.AreEqual(UnzonedDate.Create(DateTime.Parse("11/7/16")), updatedRecord.PrepDate);
            Assert.AreEqual(UnitedStatesPostalState.Create("CA"), updatedRecord.State);
            Assert.AreEqual(StreetAddress.Create("123 Bank Blvd."), updatedRecord.StreetAddress);
            Assert.AreEqual(Money.Create(18520.11m), updatedRecord.Value);
            Assert.AreEqual(UnzonedDate.Create(DateTime.Parse("11/8/16")), updatedRecord.VerifExpiresDate);
            Assert.AreEqual(UnzonedDate.Create(DateTime.Parse("11/9/16")), updatedRecord.VerifRecvDate);
            Assert.AreEqual(UnzonedDate.Create(DateTime.Parse("11/10/16")), updatedRecord.VerifReorderedDate);
            Assert.AreEqual(UnzonedDate.Create(DateTime.Parse("11/11/16")), updatedRecord.VerifSentDate);
            Assert.AreEqual(Zipcode.Create("92626"), updatedRecord.Zip);
        }

        [Test]
        public void Save_DeleteExistingRecord_RemovesRecordFromDatabase()
        {
            var mapper = new AssetCollectionMapper(
                this.loanId,
                this.brokerId,
                this.storedProcedureDriver);
            IMutableLqbCollection<DataObjectKind.Asset, Guid, Asset> collection = LqbCollection<DataObjectKind.Asset, Guid, Asset>.Create(
                Name.Create("Test").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.Asset>());
            var record = new Asset();

            collection.BeginTrackingChanges();
            var id = collection.Add(record);
            this.SaveCollection(mapper, collection);

            collection = this.LoadCollection(mapper);
            collection.BeginTrackingChanges();
            collection.Remove(id);
            this.SaveCollection(mapper, collection);

            collection = this.LoadCollection(mapper);

            Assert.AreEqual(false, collection.ContainsKey(id));
        }

        public static Asset GetDefaultTestRecord()
        {
            var record = new Asset();
            record.AccountName = DescriptionField.Create("Wells Savings");
            record.AccountNum = BankAccountNumber.Create("002792-82");
            record.AssetCashDepositType = E_AssetCashDepositT.CashDeposit1;
            record.AssetType = E_AssetT.Savings;
            record.Attention = EntityName.Create("Billy Banker");
            record.City = City.Create("Costa Mesa");
            record.CompanyName = EntityName.Create("Wells Fargo");
            record.DepartmentName = DescriptionField.Create("Accounts");
            record.Desc = DescriptionField.Create("Wells Fargo Savings Acct.");
            record.FaceValue = Money.Create(190493.78m);
            record.GiftSourceData = E_GiftFundSourceT.Blank;
            record.IsEmptyCreated = false;
            record.IsSeeAttachment = false;
            record.OtherTypeDesc = DescriptionField.Create("None.");
            record.Phone = PhoneNumber.Create("188855555555");
            record.PrepDate = UnzonedDate.Create(DateTime.Parse("11/7/16"));
            record.State = UnitedStatesPostalState.Create("CA");
            record.StreetAddress = StreetAddress.Create("123 Bank Blvd.");
            record.Value = Money.Create(18520.11m);
            record.VerifExpiresDate = UnzonedDate.Create(DateTime.Parse("11/8/16"));
            record.VerifRecvDate = UnzonedDate.Create(DateTime.Parse("11/9/16"));
            record.VerifReorderedDate = UnzonedDate.Create(DateTime.Parse("11/10/16"));
            record.VerifSentDate = UnzonedDate.Create(DateTime.Parse("11/11/16"));
            record.Zip = Zipcode.Create("92626");

            return record;
        }

        private IMutableLqbCollection<DataObjectKind.Asset, Guid, Asset> LoadCollection(
            ILqbCollectionSqlMapper<DataObjectKind.Asset, Guid, Asset> mapper)
        {
            return Load(mapper, this.connection, this.transaction);
        }

        internal static IMutableLqbCollection<DataObjectKind.Asset, Guid, Asset> Load(
            ILqbCollectionSqlMapper<DataObjectKind.Asset, Guid, Asset> mapper,
            IDbConnection connection, IDbTransaction transaction)
        {
            return mapper.Load(
                connection,
                transaction,
                Name.Create("Test").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.Asset>(),
                null,
                new DataReaderConverter());
        }

        private void SaveCollection(
            ILqbCollectionSqlMapper<DataObjectKind.Asset, Guid, Asset> mapper,
            IMutableLqbCollection<DataObjectKind.Asset, Guid, Asset> collection)
        {
            mapper.Save(
                this.connection,
                this.transaction,
                new SqlParameterConverter(),
                collection);
        }

        internal static void Save(
            ILqbCollectionSqlMapper<DataObjectKind.Asset, Guid, Asset> mapper,
            IMutableLqbCollection<DataObjectKind.Asset, Guid, Asset> collection,
            IDbConnection connection, IDbTransaction transaction)
        {
            mapper.Save(
                connection,
                transaction,
                new SqlParameterConverter(),
                collection);
        }

        private DbConnection GetConnection()
        {
            return DbConnectionInfo.GetConnectionInfo(this.brokerId.Value).GetConnection();
        }
    }
}
