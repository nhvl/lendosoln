﻿namespace LendingQB.Test.Developers.LendingQB.Core.Mapping
{
    using System;
    using System.Data;
    using System.Data.Common;
    using Fakes;
    using LendersOffice.Constants;
    using LendersOffice.Drivers.SqlServerDB;
    using LendersOffice.Security;
    using global::DataAccess;
    using global::LendingQB.Core;
    using global::LendingQB.Core.Data;
    using global::LendingQB.Core.Mapping;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;
    using NUnit.Framework;
    using Utils;

    [TestFixture]
    [Category(CategoryConstants.IntegrationTest)]
    public sealed class ConsumerLiabilityMapperTest
    {
        private const int MaxFirstCardinality = -1;
        private const int MaxSecondCardinality = -1;

        private DbConnection connection;
        private IDbTransaction transaction;
        private IStoredProcedureDriver storedProcedureDriver;

        private Guid loanId;
        private Guid appId;
        private Guid borrowerId;
        private AbstractUserPrincipal principal;

        [TestFixtureSetUp]
        public void FixtureSetup()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                this.CreateLoan();

                var factory = new StoredProcedureDriverFactory();
                this.storedProcedureDriver = factory.Create(TimeoutInSeconds.Default);
            }
        }

        [SetUp]
        public void MethodSetUp()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                this.connection = DbConnectionInfo.GetConnectionInfo(this.principal.BrokerId).GetConnection();
                this.connection.Open();
                this.transaction = connection.BeginTransaction();
            }
        }

        [TearDown]
        public void MethodTearDown()
        {
            this.transaction.Rollback();
            this.transaction.Dispose();
            this.connection.Close();
        }

        [Test]
        public void Save_Populated()
        {
            var loanIdentifier = DataObjectIdentifier<DataObjectKind.Loan, Guid>.Create(this.loanId);
            var brokerIdentifier = DataObjectIdentifier<DataObjectKind.ClientCompany, Guid>.Create(this.principal.BrokerId);
            var borrowerIdentifier = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(this.borrowerId);
            var appIdentifier = DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>.Create(this.appId);

            var liability = LiabilityCollectionMapperTest.GetDefaultTestRecord();
            IMutableLqbCollection<DataObjectKind.Liability, Guid, Liability> collectionL = LqbCollection<DataObjectKind.Liability, Guid, Liability>.Create(
                Name.Create("Liability").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.Liability>());
            collectionL.BeginTrackingChanges();
            var liabilityId = collectionL.Add(liability);
            Assert.AreNotEqual(Guid.Empty, liabilityId.Value);

            var association = new ConsumerLiabilityAssociation(borrowerIdentifier, liabilityId, appIdentifier);
            association.IsPrimary = true;
            IMutableLqbAssociationSet<DataObjectKind.ConsumerLiabilityAssociation, Guid, ConsumerLiabilityAssociation> associationSet = LqbAssociationSet<DataObjectKind.ConsumerLiabilityAssociation, Guid, ConsumerLiabilityAssociation>.Create(
                Name.Create("Test").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.ConsumerLiabilityAssociation>(), MaxFirstCardinality, MaxSecondCardinality);
            associationSet.BeginTrackingChanges();
            var assocId = associationSet.Add(association);
            Assert.AreNotEqual(Guid.Empty, assocId.Value);

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.Json);

                // save data
                var pmlAuditTrailMapper = new LiabilityPmlAuditTrailMapper();
                var mapperL = new LiabilityCollectionMapper(
                    loanIdentifier,
                    brokerIdentifier,
                    NSubstitute.Substitute.For<ILiabilityDefaultsProvider>(),
                    pmlAuditTrailMapper,
                    this.storedProcedureDriver);
                LiabilityCollectionMapperTest.SaveCollection(mapperL, collectionL, this.connection, this.transaction);

                var mapperA = new ConsumerLiabilityMapper(
                    loanIdentifier,
                    this.storedProcedureDriver);
                SaveAssociationSet(mapperA, associationSet, this.connection, this.transaction);

                // re-load
                associationSet = ConsumerLiabilityMapperTest.LoadAssociationSet(mapperA, this.connection, this.transaction);
                var loaded = associationSet[assocId];

                // check field values
                Assert.AreEqual(borrowerIdentifier, loaded.ConsumerId);
                Assert.AreEqual(liabilityId, loaded.LiabilityId);
            }
        }

        [Test]
        public void DeleteTest()
        {
            var loanIdentifier = DataObjectIdentifier<DataObjectKind.Loan, Guid>.Create(this.loanId);
            var brokerIdentifier = DataObjectIdentifier<DataObjectKind.ClientCompany, Guid>.Create(this.principal.BrokerId);
            var borrowerIdentifier = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(this.borrowerId);
            var appIdentifier = DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>.Create(this.appId);

            var liability1 = LiabilityCollectionMapperTest.GetDefaultTestRecord();
            var liability2 = LiabilityCollectionMapperTest.GetDefaultTestRecord();
            IMutableLqbCollection<DataObjectKind.Liability, Guid, Liability> collectionL = LqbCollection<DataObjectKind.Liability, Guid, Liability>.Create(
                Name.Create("Liability").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.Liability>());
            collectionL.BeginTrackingChanges();
            var firstLiabilityId = collectionL.Add(liability1);
            var secondLiabilityId = collectionL.Add(liability1);

            var association1 = new ConsumerLiabilityAssociation(borrowerIdentifier, firstLiabilityId, appIdentifier);
            association1.IsPrimary = true;
            var association2 = new ConsumerLiabilityAssociation(borrowerIdentifier, secondLiabilityId, appIdentifier);
            association2.IsPrimary = true;
            IMutableLqbAssociationSet<DataObjectKind.ConsumerLiabilityAssociation, Guid, ConsumerLiabilityAssociation> associationSet = LqbAssociationSet<DataObjectKind.ConsumerLiabilityAssociation, Guid, ConsumerLiabilityAssociation>.Create(
                Name.Create("Test").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.ConsumerLiabilityAssociation>(), MaxFirstCardinality, MaxSecondCardinality);
            associationSet.BeginTrackingChanges();
            var firstAssocId = associationSet.Add(association1);
            var secondAssocId = associationSet.Add(association2);

            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.Json);

                var customMapper = new LiabilityPmlAuditTrailMapper();
                var mapperL = new LiabilityCollectionMapper(
                    loanIdentifier,
                    brokerIdentifier,
                    NSubstitute.Substitute.For<ILiabilityDefaultsProvider>(),
                    customMapper,
                    this.storedProcedureDriver);
                LiabilityCollectionMapperTest.SaveCollection(mapperL, collectionL, this.connection, this.transaction);

                var mapper = new ConsumerLiabilityMapper(
                    loanIdentifier,
                    this.storedProcedureDriver);
                SaveAssociationSet(mapper, associationSet, this.connection, this.transaction);

                associationSet = LoadAssociationSet(mapper, this.connection, this.transaction);
                Assert.AreEqual(2, associationSet.Count);

                associationSet.BeginTrackingChanges();
                associationSet.Remove(firstAssocId);
                SaveAssociationSet(mapper, associationSet, this.connection, this.transaction);

                associationSet = LoadAssociationSet(mapper, this.connection, this.transaction);
                Assert.AreEqual(1, associationSet.Count);
            }
        }

        internal static IMutableLqbAssociationSet<DataObjectKind.ConsumerLiabilityAssociation, Guid, ConsumerLiabilityAssociation> LoadAssociationSet(
            ILqbCollectionSqlMapper<DataObjectKind.ConsumerLiabilityAssociation, Guid, ConsumerLiabilityAssociation> mapper,
            IDbConnection connection, IDbTransaction transaction)
        {
            return mapper.Load(
                connection,
                transaction,
                Name.Create("Test").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.ConsumerLiabilityAssociation>(),
                null,
                new DataReaderConverter()) as IMutableLqbAssociationSet<DataObjectKind.ConsumerLiabilityAssociation, Guid, ConsumerLiabilityAssociation>;
        }

        internal static void SaveAssociationSet(
            ILqbCollectionSqlMapper<DataObjectKind.ConsumerLiabilityAssociation, Guid, ConsumerLiabilityAssociation> mapper,
            IMutableLqbAssociationSet<DataObjectKind.ConsumerLiabilityAssociation, Guid, ConsumerLiabilityAssociation> associationSet,
            IDbConnection connection, IDbTransaction transaction)
        {
            mapper.Save(
                connection,
                transaction,
                new SqlParameterConverter(),
                associationSet);
        }

        // Create a new test loan and return <brokerid, loanid>
        private void CreateLoan()
        {
            this.principal = LoginTools.LoginAs(TestAccountType.LoTest001);

            var dataLoan = FakePageData.Create(appCount: 1);
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            var dataApp = dataLoan.GetAppData(0);
            dataApp.aBFirstNm = "Frosty";
            dataApp.aBLastNm = "Snowman";
            dataApp.aBSsn = "555-55-5555";
            dataLoan.Save();

            this.loanId = dataLoan.sLId;
            this.appId = dataApp.aAppId;
            this.borrowerId = dataApp.aBConsumerId;
        }
    }
}
