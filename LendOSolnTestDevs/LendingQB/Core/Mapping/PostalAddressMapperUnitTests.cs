﻿namespace LendingQB.Test.Developers.LendingQB.Core.Mapping
{
    using System;
    using global::DataAccess;
    using global::LendingQB.Core.Mapping;
    using global::LqbGrammar.DataTypes;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public sealed class PostalAddressMapperUnitTests
    {
        private Utils.FoolHelper helper;

        [TestFixtureSetUp]
        public void SetUp()
        {
            this.helper = new Utils.FoolHelper();
            var fakeStageConfig = FOOL.FakeConfigurationQueryFactory.RegisterFake(helper).StageConfig;
            fakeStageConfig.Add(nameof(LendersOffice.Constants.StageConfigDatum.UseJsonNetHtmlEscaping), Tuple.Create(1, string.Empty));
        }

        [TestFixtureTearDown]
        public void TearDown()
        {
            this.helper.Dispose();
        }

        [Test]
        public void GeneralAddress_CityTooLong_ThrowsException()
        {
            var builder = PopulateGoodGeneralAddressData();
            builder.City = City.Create(new string('x', 257)).Value;

            var address = builder.GetAddress();
            var mapper = new PostalAddressMapper();
            Assert.Throws<DataTruncationException>(() => mapper.ToSqlParameter("@address", address));
        }

        [Test]
        public void GeneralAddress_CityNull_DoesNotThrowException()
        {
            var builder = PopulateGoodGeneralAddressData();
            builder.City = null;

            var address = builder.GetAddress();
            var mapper = new PostalAddressMapper();
            Assert.DoesNotThrow(() => mapper.ToSqlParameter("@address", address));
        }

        [Test]
        public void GeneralAddress_StateTooLong_ThrowsException()
        {
            var builder = PopulateGoodGeneralAddressData();
            builder.State = AdministrativeArea.Create(new string('x', 33)).Value;

            var address = builder.GetAddress();
            var mapper = new PostalAddressMapper();
            Assert.Throws<DataTruncationException>(() => mapper.ToSqlParameter("@address", address));
        }

        [Test]
        public void GeneralAddress_StateNull_DoesNotThrowException()
        {
            var builder = PopulateGoodGeneralAddressData();
            builder.State = null;

            var address = builder.GetAddress();
            var mapper = new PostalAddressMapper();
            Assert.DoesNotThrow(() => mapper.ToSqlParameter("@address", address));
        }

        [Test]
        public void GeneralAddress_PostalCodeTooLong_ThrowsException()
        {
            var builder = PopulateGoodGeneralAddressData();
            builder.PostalCode = PostalCode.Create(new string('x', 11)).Value;

            var address = builder.GetAddress();
            var mapper = new PostalAddressMapper();
            Assert.Throws<DataTruncationException>(() => mapper.ToSqlParameter("@address", address));
        }

        [Test]
        public void GeneralAddress_PostalCodeNull_DoesNotThrowException()
        {
            var builder = PopulateGoodGeneralAddressData();
            builder.PostalCode = null;

            var address = builder.GetAddress();
            var mapper = new PostalAddressMapper();
            Assert.DoesNotThrow(() => mapper.ToSqlParameter("@address", address));
        }

        [Test]
        public void GeneralAddress_CountryCodeTooLong_ThrowsException()
        {
            var builder = PopulateGoodGeneralAddressData();
            builder.CountryCode = CountryCodeIso3.Create(new string('x', 4)).Value;

            var address = builder.GetAddress();
            var mapper = new PostalAddressMapper();
            Assert.Throws<DataTruncationException>(() => mapper.ToSqlParameter("@address", address));
        }

        [Test]
        public void GeneralAddress_CountryCodeNull_DoesNotThrowException()
        {
            var builder = PopulateGoodGeneralAddressData();
            builder.CountryCode = null;

            var address = builder.GetAddress();
            var mapper = new PostalAddressMapper();
            Assert.DoesNotThrow(() => mapper.ToSqlParameter("@address", address));
        }

        [Test]
        public void GeneralAddress_StreetAddressTooLong_ThrowsException()
        {
            var builder = PopulateGoodGeneralAddressData();
            builder.StreetAddress = StreetAddress.Create(new string('x', 257)).Value;

            var address = builder.GetAddress();
            var mapper = new PostalAddressMapper();
            Assert.Throws<DataTruncationException>(() => mapper.ToSqlParameter("@address", address));
        }

        [Test]
        public void GeneralAddress_StreetAddressNull_DoesNotThrowException()
        {
            var builder = PopulateGoodGeneralAddressData();
            builder.StreetAddress = null;

            var address = builder.GetAddress();
            var mapper = new PostalAddressMapper();
            Assert.DoesNotThrow(() => mapper.ToSqlParameter("@address", address));
        }

        [Test]
        public void UsAddress_CityTooLong_ThrowsException()
        {
            var builder = PopulateGoodUsAddressData();
            builder.City = City.Create(new string('x', 257)).Value;

            var address = builder.GetAddress();
            var mapper = new PostalAddressMapper();
            Assert.Throws<DataTruncationException>(() => mapper.ToSqlParameter("@address", address));
        }

        [Test]
        public void UsAddress_CityNull_DoesNotThrowException()
        {
            var builder = PopulateGoodUsAddressData();
            builder.City = null;

            var address = builder.GetAddress();
            var mapper = new PostalAddressMapper();
            Assert.DoesNotThrow(() => mapper.ToSqlParameter("@address", address));
        }

        [Test]
        public void UsAddress_StateTooLong_ThrowsException()
        {
            var builder = PopulateGoodUsAddressData();
            builder.UsState = UnitedStatesPostalState.Create(new string('x', 3)).Value;

            var address = builder.GetAddress();
            var mapper = new PostalAddressMapper();
            Assert.Throws<DataTruncationException>(() => mapper.ToSqlParameter("@address", address));
        }

        [Test]
        public void UsAddress_StateNull_DoesNotThrowException()
        {
            var builder = PopulateGoodUsAddressData();
            builder.UsState = null;

            var address = builder.GetAddress();
            var mapper = new PostalAddressMapper();
            Assert.DoesNotThrow(() => mapper.ToSqlParameter("@address", address));
        }

        [Test]
        public void UsAddress_ZipcodeTooLong_ThrowsException()
        {
            var builder = PopulateGoodUsAddressData();
            builder.Zipcode = Zipcode.Create(new string('x', 11)).Value;

            var address = builder.GetAddress();
            var mapper = new PostalAddressMapper();
            Assert.Throws<DataTruncationException>(() => mapper.ToSqlParameter("@address", address));
        }

        [Test]
        public void UsAddress_ZipcodeNull_DoesNotThrowException()
        {
            var builder = PopulateGoodUsAddressData();
            builder.Zipcode = null;

            var address = builder.GetAddress();
            var mapper = new PostalAddressMapper();
            Assert.DoesNotThrow(() => mapper.ToSqlParameter("@address", address));
        }

        [Test]
        public void UsAddress_StreetAddressTooLong_ThrowsException()
        {
            var builder = PopulateGoodUsAddressData();
            builder.UnparsedStreetAddress = StreetAddress.Create(new string('x', 257)).Value;

            var address = builder.GetAddress();
            var mapper = new PostalAddressMapper();
            Assert.Throws<DataTruncationException>(() => mapper.ToSqlParameter("@address", address));
        }

        [Test]
        public void UsAddress_AddressNumberTooLong_ThrowsException()
        {
            var builder = PopulateGoodUsAddressData();
            builder.AddressNumber = UspsAddressNumber.Create(new string('x', 17)).Value;

            var address = builder.GetAddress();
            var mapper = new PostalAddressMapper();
            Assert.Throws<DataTruncationException>(() => mapper.ToSqlParameter("@address", address));
        }

        [Test]
        public void UsAddress_DirectionalTooLong_ThrowsException()
        {
            var builder = PopulateGoodUsAddressData();
            builder.StreetPreDirection = UspsDirectional.Create(new string('x', 17)).Value;

            var address = builder.GetAddress();
            var mapper = new PostalAddressMapper();
            Assert.Throws<DataTruncationException>(() => mapper.ToSqlParameter("@address", address));
        }

        [Test]
        public void UsAddress_StreetNameTooLong_ThrowsException()
        {
            var builder = PopulateGoodUsAddressData();
            builder.StreetName = StreetName.Create(new string('x', 33)).Value;

            var address = builder.GetAddress();
            var mapper = new PostalAddressMapper();
            Assert.Throws<DataTruncationException>(() => mapper.ToSqlParameter("@address", address));
        }

        [Test]
        public void UsAddress_StreetSuffixTooLong_ThrowsException()
        {
            var builder = PopulateGoodUsAddressData();
            builder.StreetSuffix = StreetSuffix.Create(new string('x', 33)).Value;

            var address = builder.GetAddress();
            var mapper = new PostalAddressMapper();
            Assert.Throws<DataTruncationException>(() => mapper.ToSqlParameter("@address", address));
        }

        [Test]
        public void UsAddress_UnitTypeTooLong_ThrowsException()
        {
            var builder = PopulateGoodUsAddressData();
            builder.UnitType = AddressUnitType.Create(new string('x', 17)).Value;

            var address = builder.GetAddress();
            var mapper = new PostalAddressMapper();
            Assert.Throws<DataTruncationException>(() => mapper.ToSqlParameter("@address", address));
        }

        [Test]
        public void UsAddress_UnitIdentifierTooLong_ThrowsException()
        {
            var builder = PopulateGoodUsAddressData();
            builder.UnitIdentifier = AddressUnitIdentifier.Create(new string('x', 17)).Value;

            var address = builder.GetAddress();
            var mapper = new PostalAddressMapper();
            Assert.Throws<DataTruncationException>(() => mapper.ToSqlParameter("@address", address));
        }

        public static GeneralPostalAddress.Builder PopulateGoodGeneralAddressData()
        {
            string countryCode = "MEX";
            string postalCode = "MX-561";
            string state = "DF";
            string city = "Ciudad de Mexico";
            string streetAddress = "541 Camino Ejercito";

            var builder = new GeneralPostalAddress.Builder();
            builder.CountryCode = CountryCodeIso3.CreateWithValidation(countryCode);
            builder.PostalCode = PostalCode.CreateWithValidation(postalCode, builder.CountryCode.Value);
            builder.State = AdministrativeArea.Create(state);
            builder.City = City.Create(city);
            builder.StreetAddress = StreetAddress.Create(streetAddress);

            return builder;
        }

        public static UnitedStatesPostalAddress.Builder PopulateGoodUsAddressData()
        {
            string zipcode = "92646";
            string state = "CA";
            string city = "Costa Mesa";
            string streetAddress = "1600 Sunflower Ave";
            string number = "1600";
            string preDir = "East";
            string street = "Sunflower";
            string postDir = "Southwest";
            string streetType = "Ave";
            string unitType = "Suite";
            string unitNum = "100";

            var builder = new UnitedStatesPostalAddress.Builder();
            builder.Zipcode = Zipcode.CreateWithValidation(zipcode);
            builder.UsState = UnitedStatesPostalState.Create(state);
            builder.City = City.Create(city);
            builder.UnparsedStreetAddress = StreetAddress.Create(streetAddress);
            builder.AddressNumber = UspsAddressNumber.Create(number);
            builder.StreetPreDirection = UspsDirectional.CreateWithValidation(preDir);
            builder.StreetName = StreetName.Create(street);
            builder.StreetPostDirection = UspsDirectional.CreateWithValidation(postDir);
            builder.StreetSuffix = StreetSuffix.Create(streetType);
            builder.UnitType = AddressUnitType.Create(unitType);
            builder.UnitIdentifier = AddressUnitIdentifier.Create(unitNum);

            return builder;
        }
    }
}
