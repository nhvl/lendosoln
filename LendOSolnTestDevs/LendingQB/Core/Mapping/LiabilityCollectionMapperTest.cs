﻿namespace LendingQB.Test.Developers.LendingQB.Core.Mapping
{
    using System;
    using System.Data.Common;
    using DataAccess;
    using LendersOffice.Security;
    using NUnit.Framework;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;
    using LendersOffice.Drivers.SqlServerDB;
    using Utils;
    using System.Data;
    using global::LendingQB.Core.Mapping;
    using global::LendingQB.Core;
    using global::LendingQB.Core.Data;
    using System.Collections.Generic;
    using global::DataAccess;
    using NSubstitute;

    [TestFixture]
    [Category(CategoryConstants.IntegrationTest)]
    public class LiabilityCollectionMapperTest
    {
        private AbstractUserPrincipal principal;
        private DataObjectIdentifier<DataObjectKind.Loan, Guid> loanId;
        private DataObjectIdentifier<DataObjectKind.ClientCompany, Guid> brokerId;
        private IStoredProcedureDriver storedProcedureDriver;
        private IDbConnection connection;
        private IDbTransaction transaction;

        [TestFixtureSetUp]
        public void FixtureSetUp()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                this.principal = LoginTools.LoginAs(TestAccountType.LoTest001);
                this.brokerId = DataObjectIdentifier<DataObjectKind.ClientCompany, Guid>.Create(principal.BrokerId);
                var creator = CLoanFileCreator.GetCreator(
                    this.principal,
                    LendersOffice.Audit.E_LoanCreationSource.UserCreateFromBlank);
                this.loanId = DataObjectIdentifier<DataObjectKind.Loan, Guid>.Create(creator.CreateBlankLoanFile());

                var factory = new StoredProcedureDriverFactory();
                this.storedProcedureDriver = factory.Create(TimeoutInSeconds.Default);
            }
        }

        [TestFixtureTearDown]
        public void FixtureTearDown()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                Tools.DeclareLoanFileInvalid(
                    this.principal,
                    this.loanId.Value,
                    sendNotifications: false,
                    generateLinkLoanMsgEvent: false);
            }
        }

        [SetUp]
        public void SetUp()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                this.connection = DbConnectionInfo.GetConnectionInfo(this.brokerId.Value).GetConnection();
                this.connection.Open();
                this.transaction = connection.BeginTransaction();
            }
        }

        [TearDown]
        public void TearDown()
        {
            this.transaction.Rollback();
            this.transaction.Dispose();
            this.connection.Dispose();
        }

        [Test]
        public void Load_AssumingNoDatabaseIssues_Succeeds()
        {
            var pmlAuditTrailMapper = new LiabilityPmlAuditTrailMapper();

            var mapper = new LiabilityCollectionMapper(
                this.loanId,
                this.brokerId,
                NSubstitute.Substitute.For<ILiabilityDefaultsProvider>(),
                pmlAuditTrailMapper,
                this.storedProcedureDriver);

            this.LoadCollection(mapper);
        }

        [Test]
        public void Save_NewRecord_PersistsEnteredDataPoints()
        {
            var pmlAuditTrailMapper = new LiabilityPmlAuditTrailMapper();
            var mapper = new LiabilityCollectionMapper(
                this.loanId,
                this.brokerId,
                NSubstitute.Substitute.For<ILiabilityDefaultsProvider>(),
                pmlAuditTrailMapper,
                this.storedProcedureDriver);
            IMutableLqbCollection<DataObjectKind.Liability, Guid, Liability> collection = LqbCollection<DataObjectKind.Liability, Guid, Liability>.Create(
                Name.Create("Test").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.Liability>());
            Liability originalRecord = GetDefaultTestRecord();

            collection.BeginTrackingChanges();
            var id = collection.Add(originalRecord);
            this.SaveCollection(mapper, collection);
            collection = this.LoadCollection(mapper);
            var loadedRecord = collection[id];

            Assert.AreEqual(originalRecord.DebtType, loadedRecord.DebtType);
            Assert.AreEqual(originalRecord.UsedInRatio, loadedRecord.UsedInRatio);
            Assert.AreEqual(originalRecord.Pmt, loadedRecord.Pmt);
            Assert.AreEqual(originalRecord.RemainMon, loadedRecord.RemainMon);
            Assert.AreEqual(originalRecord.WillBePdOff, loadedRecord.WillBePdOff);
            Assert.AreEqual(originalRecord.AccountName, loadedRecord.AccountName);
            Assert.AreEqual(originalRecord.AccountNum, loadedRecord.AccountNum);
            Assert.AreEqual(originalRecord.Attention, loadedRecord.Attention);
            Assert.AreEqual(originalRecord.AutoYearMakeAsString, loadedRecord.AutoYearMakeAsString);
            Assert.AreEqual(originalRecord.Bal, loadedRecord.Bal);
            Assert.AreEqual(originalRecord.CompanyAddress, loadedRecord.CompanyAddress);
            Assert.AreEqual(originalRecord.CompanyCity, loadedRecord.CompanyCity);
            Assert.AreEqual(originalRecord.CompanyFax, loadedRecord.CompanyFax);
            Assert.AreEqual(originalRecord.CompanyName, loadedRecord.CompanyName);
            Assert.AreEqual(originalRecord.CompanyPhone, loadedRecord.CompanyPhone);
            Assert.AreEqual(originalRecord.CompanyState, loadedRecord.CompanyState);
            Assert.AreEqual(originalRecord.CompanyZip, loadedRecord.CompanyZip);
            Assert.AreEqual(originalRecord.Desc, loadedRecord.Desc);
            Assert.AreEqual(originalRecord.DueInMonAsString, loadedRecord.DueInMonAsString);
            Assert.AreEqual(originalRecord.ExcludeFromUw, loadedRecord.ExcludeFromUw);
            Assert.AreEqual(originalRecord.FullyIndexedPitiPmt, loadedRecord.FullyIndexedPitiPmt);
            Assert.AreEqual(originalRecord.IncludeInBk, loadedRecord.IncludeInBk);
            Assert.AreEqual(originalRecord.IncludeInFc, loadedRecord.IncludeInFc);
            Assert.AreEqual(originalRecord.IncludeInReposession, loadedRecord.IncludeInReposession);
            Assert.AreEqual(originalRecord.IsForAuto, loadedRecord.IsForAuto);
            Assert.AreEqual(originalRecord.IsMtgFhaInsured, loadedRecord.IsMtgFhaInsured);
            Assert.AreEqual(originalRecord.IsPiggyBack, loadedRecord.IsPiggyBack);
            Assert.AreEqual(originalRecord.IsSeeAttachment, loadedRecord.IsSeeAttachment);
            Assert.AreEqual(originalRecord.IsSp1stMtgData, loadedRecord.IsSp1stMtgData);
            Assert.AreEqual(originalRecord.Late30AsString, loadedRecord.Late30AsString);
            Assert.AreEqual(originalRecord.Late60AsString, loadedRecord.Late60AsString);
            Assert.AreEqual(originalRecord.Late90PlusAsString, loadedRecord.Late90PlusAsString);
            Assert.AreEqual(originalRecord.OriginalDebtAmt, loadedRecord.OriginalDebtAmt);
            Assert.AreEqual(originalRecord.OriginalTermAsString, loadedRecord.OriginalTermAsString);
            Assert.AreEqual(originalRecord.PayoffAmt, loadedRecord.PayoffAmt);
            Assert.AreEqual(originalRecord.PayoffAmtLocked, loadedRecord.PayoffAmtLocked);
            Assert.AreEqual(originalRecord.PayoffTimingData, loadedRecord.PayoffTimingData);
            Assert.AreEqual(originalRecord.PayoffTimingLocked, loadedRecord.PayoffTimingLocked);
            Assert.AreEqual(originalRecord.PrepDate, loadedRecord.PrepDate);
            Assert.AreEqual(originalRecord.Rate, loadedRecord.Rate);
            Assert.AreEqual(originalRecord.ReconcileStatusType, loadedRecord.ReconcileStatusType);
            Assert.AreEqual(originalRecord.UsedInRatio, loadedRecord.UsedInRatio);
            Assert.AreEqual(originalRecord.VerifExpiresDate, loadedRecord.VerifExpiresDate);
            Assert.AreEqual(originalRecord.VerifRecvDate, loadedRecord.VerifRecvDate);
            Assert.AreEqual(originalRecord.VerifReorderedDate, loadedRecord.VerifReorderedDate);
            Assert.AreEqual(originalRecord.VerifSentDate, loadedRecord.VerifSentDate);
            Assert.AreEqual(originalRecord.OwedTo, loadedRecord.OwedTo);
            Assert.AreEqual(originalRecord.JobExpenseDesc, loadedRecord.JobExpenseDesc);
        }

        [Test]
        public void Save_EmptyRecord_PersistsEmptyRecord()
        {
            var pmlAuditTrailMapper = new LiabilityPmlAuditTrailMapper();

            var mapper = new LiabilityCollectionMapper(
                this.loanId,
                this.brokerId,
                NSubstitute.Substitute.For<ILiabilityDefaultsProvider>(),
                pmlAuditTrailMapper,
                this.storedProcedureDriver);
            IMutableLqbCollection<DataObjectKind.Liability, Guid, Liability> collection = LqbCollection<DataObjectKind.Liability, Guid, Liability>.Create(
                Name.Create("Test").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.Liability>());
            var originalRecord = new Liability(Substitute.For<ILiabilityDefaultsProvider>());

            collection.BeginTrackingChanges();
            var id = collection.Add(originalRecord);
            this.SaveCollection(mapper, collection);
            collection = this.LoadCollection(mapper);
            var loadedRecord = collection[id];

            Assert.AreEqual(null, loadedRecord.DebtType);
            Assert.AreEqual(null, loadedRecord.Pmt);
            Assert.AreEqual(null, loadedRecord.RemainMon);
            Assert.AreEqual(null, loadedRecord.WillBePdOff);
            Assert.AreEqual(null, loadedRecord.AccountName);
            Assert.AreEqual(null, loadedRecord.AccountNum);
            Assert.AreEqual(null, loadedRecord.Attention);
            Assert.AreEqual(null, loadedRecord.AutoYearMakeAsString);
            Assert.AreEqual(null, loadedRecord.Bal);
            Assert.AreEqual(null, loadedRecord.CompanyAddress);
            Assert.AreEqual(null, loadedRecord.CompanyCity);
            Assert.AreEqual(null, loadedRecord.CompanyFax);
            Assert.AreEqual(null, loadedRecord.CompanyName);
            Assert.AreEqual(null, loadedRecord.CompanyPhone);
            Assert.AreEqual(null, loadedRecord.CompanyState);
            Assert.AreEqual(null, loadedRecord.CompanyZip);
            Assert.AreEqual(null, loadedRecord.Desc);
            Assert.AreEqual(null, loadedRecord.DueInMonAsString);
            Assert.AreEqual(null, loadedRecord.ExcludeFromUw);
            Assert.AreEqual(null, loadedRecord.FullyIndexedPitiPmt);
            Assert.AreEqual(null, loadedRecord.IncludeInBk);
            Assert.AreEqual(null, loadedRecord.IncludeInFc);
            Assert.AreEqual(null, loadedRecord.IncludeInReposession);
            Assert.AreEqual(null, loadedRecord.IsForAuto);
            Assert.AreEqual(null, loadedRecord.IsMtgFhaInsured);
            Assert.AreEqual(null, loadedRecord.IsPiggyBack);
            Assert.AreEqual(null, loadedRecord.IsSeeAttachment);
            Assert.AreEqual(null, loadedRecord.IsSp1stMtgData);
            Assert.AreEqual(null, loadedRecord.Late30AsString);
            Assert.AreEqual(null, loadedRecord.Late60AsString);
            Assert.AreEqual(null, loadedRecord.Late90PlusAsString);
            Assert.AreEqual(null, loadedRecord.OriginalDebtAmt);
            Assert.AreEqual(null, loadedRecord.OriginalTermAsString);
            Assert.AreEqual(null, loadedRecord.PayoffAmt);
            Assert.AreEqual(null, loadedRecord.PayoffAmtLocked);
            Assert.AreEqual(null, loadedRecord.PayoffTimingData);
            Assert.AreEqual(null, loadedRecord.PayoffTimingLocked);
            Assert.AreEqual(null, loadedRecord.PrepDate);
            Assert.AreEqual(null, loadedRecord.Rate);
            Assert.AreEqual(null, loadedRecord.ReconcileStatusType);
            Assert.AreEqual(null, loadedRecord.UsedInRatioData);
            Assert.AreEqual(null, loadedRecord.VerifExpiresDate);
            Assert.AreEqual(null, loadedRecord.VerifRecvDate);
            Assert.AreEqual(null, loadedRecord.VerifReorderedDate);
            Assert.AreEqual(null, loadedRecord.VerifSentDate);
            Assert.AreEqual(null, loadedRecord.OwedTo);
            Assert.AreEqual(null, loadedRecord.JobExpenseDesc);
        }

        [Test]
        public void Save_UpdateExistingRecord_PersistsUpdatedDataPoints()
        {
            var pmlAuditTrailMapper = new LiabilityPmlAuditTrailMapper();

            var mapper = new LiabilityCollectionMapper(
                this.loanId,
                this.brokerId,
                NSubstitute.Substitute.For<ILiabilityDefaultsProvider>(),
                pmlAuditTrailMapper,
                this.storedProcedureDriver);
            IMutableLqbCollection<DataObjectKind.Liability, Guid, Liability> collection = LqbCollection<DataObjectKind.Liability, Guid, Liability>.Create(
                Name.Create("Test").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.Liability>());
            var originalRecord = GetDefaultTestRecord();

            collection.BeginTrackingChanges();
            var id = collection.Add(originalRecord);
            this.SaveCollection(mapper, collection);

            collection = this.LoadCollection(mapper);
            collection.BeginTrackingChanges();
            var loadedRecord = collection[id];

            loadedRecord.DebtType = E_DebtT.JobRelatedExpense;
            loadedRecord.Pmt = Money.Create(392.19m);
            loadedRecord.RemainMon = LiabilityRemainingMonths.Create("Unknown.");
            loadedRecord.WillBePdOff = false;
            loadedRecord.AccountName = DescriptionField.Create("Honda Civic");
            loadedRecord.AccountNum = ThirdPartyIdentifier.Create("302-a11");
            loadedRecord.Attention = EntityName.Create("Jim T. Honda");
            loadedRecord.AutoYearMakeAsString = YearAsString.Create("1994");
            loadedRecord.Bal = Money.Create(5303.22m);
            loadedRecord.CompanyAddress = StreetAddress.Create("123 Bank Blvd.");
            loadedRecord.CompanyCity = City.Create("Costa Mesa");
            loadedRecord.CompanyFax = PhoneNumber.Create("188855555555");
            loadedRecord.CompanyName = EntityName.Create("Honda Motor Financial");
            loadedRecord.CompanyPhone = PhoneNumber.Create("188855555555");
            loadedRecord.CompanyState = UnitedStatesPostalState.Create("OH");
            loadedRecord.CompanyZip = Zipcode.Create("92626");
            loadedRecord.Desc = DescriptionField.Create("1994 Honda Civic Auto Loan");
            loadedRecord.DueInMonAsString = CountString.Create("Seven");
            loadedRecord.ExcludeFromUw = true;
            loadedRecord.FullyIndexedPitiPmt = Money.Create(103.11m);
            loadedRecord.IncludeInBk = true;
            loadedRecord.IncludeInFc = true;
            loadedRecord.IncludeInReposession = true;
            loadedRecord.IsForAuto = true;
            loadedRecord.IsMtgFhaInsured = true;
            loadedRecord.IsPiggyBack = true;
            loadedRecord.IsSeeAttachment = true;
            loadedRecord.IsSp1stMtgData = true;
            loadedRecord.Late30AsString = CountString.Create("3");
            loadedRecord.Late60AsString = CountString.Create("2");
            loadedRecord.Late90PlusAsString = CountString.Create("1");
            loadedRecord.OriginalDebtAmt = Money.Create(11520.11m);
            loadedRecord.OriginalTermAsString = CountString.Create("240");
            loadedRecord.PayoffAmtData = Money.Create(5220.91m);
            loadedRecord.PayoffAmtLocked = true;
            loadedRecord.PayoffTimingData = PayoffTiming.BeforeClosing;
            loadedRecord.PayoffTimingLockedData = false;
            loadedRecord.PrepDate = UnzonedDate.Create(DateTime.Parse("11/5/16"));
            loadedRecord.Rate = Percentage.Create(4);
            loadedRecord.ReconcileStatusType = E_LiabilityReconcileStatusT.CreditReportOnly;
            loadedRecord.UsedInRatioData = true;
            loadedRecord.VerifExpiresDate = UnzonedDate.Create(DateTime.Parse("11/6/16"));
            loadedRecord.VerifRecvDate = UnzonedDate.Create(DateTime.Parse("11/7/16"));
            loadedRecord.VerifReorderedDate = UnzonedDate.Create(DateTime.Parse("11/8/16"));
            loadedRecord.VerifSentDate = UnzonedDate.Create(DateTime.Parse("11/9/16"));
            loadedRecord.OwedTo = PersonName.Create("Zeke J. Honda");
            loadedRecord.JobExpenseDesc = DescriptionField.Create("None.");
            loadedRecord.PmlAuditTrail = CreateDefaultEvents();

            this.SaveCollection(mapper, collection);

            collection = this.LoadCollection(mapper);
            var updatedRecord = collection[id];

            Assert.AreEqual(E_DebtT.JobRelatedExpense, updatedRecord.DebtType);
            Assert.AreEqual(Money.Create(392.19m), updatedRecord.Pmt);
            Assert.AreEqual(LiabilityRemainingMonths.Create("Unknown."), updatedRecord.RemainMon);
            Assert.AreEqual(false, updatedRecord.WillBePdOff);
            Assert.AreEqual(DescriptionField.Create("Honda Civic"), updatedRecord.AccountName);
            Assert.AreEqual(ThirdPartyIdentifier.Create("302-a11"), updatedRecord.AccountNum);
            Assert.AreEqual(EntityName.Create("Jim T. Honda"), updatedRecord.Attention);
            Assert.AreEqual(YearAsString.Create("1994"), updatedRecord.AutoYearMakeAsString);
            Assert.AreEqual(Money.Create(5303.22m), updatedRecord.Bal);
            Assert.AreEqual(StreetAddress.Create("123 Bank Blvd."), updatedRecord.CompanyAddress);
            Assert.AreEqual(City.Create("Costa Mesa"), updatedRecord.CompanyCity);
            Assert.AreEqual(PhoneNumber.Create("188855555555"), updatedRecord.CompanyFax);
            Assert.AreEqual(EntityName.Create("Honda Motor Financial"), updatedRecord.CompanyName);
            Assert.AreEqual(PhoneNumber.Create("188855555555"), updatedRecord.CompanyPhone);
            Assert.AreEqual(UnitedStatesPostalState.Create("OH"), updatedRecord.CompanyState);
            Assert.AreEqual(Zipcode.Create("92626"), updatedRecord.CompanyZip);
            Assert.AreEqual(DescriptionField.Create("1994 Honda Civic Auto Loan"), updatedRecord.Desc);
            Assert.AreEqual(CountString.Create("Seven"), updatedRecord.DueInMonAsString);
            Assert.AreEqual(true, updatedRecord.ExcludeFromUw);
            Assert.AreEqual(Money.Create(103.11m), updatedRecord.FullyIndexedPitiPmt);
            Assert.AreEqual(true, updatedRecord.IncludeInBk);
            Assert.AreEqual(true, updatedRecord.IncludeInFc);
            Assert.AreEqual(true, updatedRecord.IncludeInReposession);
            Assert.AreEqual(true, updatedRecord.IsForAuto);
            Assert.AreEqual(true, updatedRecord.IsMtgFhaInsured);
            Assert.AreEqual(true, updatedRecord.IsPiggyBack);
            Assert.AreEqual(true, updatedRecord.IsSeeAttachment);
            Assert.AreEqual(true, updatedRecord.IsSp1stMtgData);
            Assert.AreEqual(CountString.Create("3"), updatedRecord.Late30AsString);
            Assert.AreEqual(CountString.Create("2"), updatedRecord.Late60AsString);
            Assert.AreEqual(CountString.Create("1"), updatedRecord.Late90PlusAsString);
            Assert.AreEqual(Money.Create(11520.11m), updatedRecord.OriginalDebtAmt);
            Assert.AreEqual(CountString.Create("240"), updatedRecord.OriginalTermAsString);
            Assert.AreEqual(Money.Create(5220.91m), updatedRecord.PayoffAmt);
            Assert.AreEqual(true, updatedRecord.PayoffAmtLocked);
            Assert.AreEqual(PayoffTiming.BeforeClosing, updatedRecord.PayoffTimingData);
            Assert.AreEqual(false, updatedRecord.PayoffTimingLocked);
            Assert.AreEqual(UnzonedDate.Create(DateTime.Parse("11/5/16")), updatedRecord.PrepDate);
            Assert.AreEqual(Percentage.Create(4), updatedRecord.Rate);
            Assert.AreEqual(E_LiabilityReconcileStatusT.CreditReportOnly, updatedRecord.ReconcileStatusType);
            Assert.AreEqual(true, updatedRecord.UsedInRatio);
            Assert.AreEqual(UnzonedDate.Create(DateTime.Parse("11/6/16")), updatedRecord.VerifExpiresDate);
            Assert.AreEqual(UnzonedDate.Create(DateTime.Parse("11/7/16")), updatedRecord.VerifRecvDate);
            Assert.AreEqual(UnzonedDate.Create(DateTime.Parse("11/8/16")), updatedRecord.VerifReorderedDate);
            Assert.AreEqual(UnzonedDate.Create(DateTime.Parse("11/9/16")), updatedRecord.VerifSentDate);
            Assert.AreEqual(PersonName.Create("Zeke J. Honda"), updatedRecord.OwedTo);
            Assert.AreEqual(DescriptionField.Create("None."), updatedRecord.JobExpenseDesc);
        }

        [Test]
        public void Save_DeleteExistingRecord_RemovesRecordFromDatabase()
        {
            var pmlAuditTrailMapper = new LiabilityPmlAuditTrailMapper();

            var mapper = new LiabilityCollectionMapper(
                this.loanId,
                this.brokerId,
                NSubstitute.Substitute.For<ILiabilityDefaultsProvider>(),
                pmlAuditTrailMapper,
                this.storedProcedureDriver);
            IMutableLqbCollection<DataObjectKind.Liability, Guid, Liability> collection = LqbCollection<DataObjectKind.Liability, Guid, Liability>.Create(
                Name.Create("Test").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.Liability>());
            var record = new Liability(Substitute.For<ILiabilityDefaultsProvider>());

            collection.BeginTrackingChanges();
            var id = collection.Add(record);
            this.SaveCollection(mapper, collection);

            collection = this.LoadCollection(mapper);
            collection.BeginTrackingChanges();
            collection.Remove(id);
            this.SaveCollection(mapper, collection);

            collection = this.LoadCollection(mapper);

            Assert.AreEqual(false, collection.ContainsKey(id));
        }

        internal static Liability GetDefaultTestRecord()
        {
            var record = new Liability(Substitute.For<ILiabilityDefaultsProvider>());

            record.DebtType = E_DebtT.Installment;
            record.Pmt = Money.Create(392.19m);
            record.RemainMon = LiabilityRemainingMonths.Create("Unknown.");
            record.WillBePdOff = false;
            record.AccountName = DescriptionField.Create("Honda Civic");
            record.AccountNum = ThirdPartyIdentifier.Create("302-a11");
            record.Attention = EntityName.Create("Jim T. Honda");
            record.AutoYearMakeAsString = YearAsString.Create("1994");
            record.Bal = Money.Create(5303.22m);
            record.CompanyAddress = StreetAddress.Create("123 Bank Blvd.");
            record.CompanyCity = City.Create("Costa Mesa");
            record.CompanyFax = PhoneNumber.Create("188855555555");
            record.CompanyName = EntityName.Create("Honda Motor Financial");
            record.CompanyPhone = PhoneNumber.Create("188855555555");
            record.CompanyState = UnitedStatesPostalState.Create("OH");
            record.CompanyZip = Zipcode.Create("92626");
            record.Desc = DescriptionField.Create("1994 Honda Civic Auto Loan");
            record.DueInMonAsString = CountString.Create("Seven");
            record.ExcludeFromUw = true;
            record.FullyIndexedPitiPmt = Money.Create(103.11m);
            record.IncludeInBk = true;
            record.IncludeInFc = true;
            record.IncludeInReposession = true;
            record.IsForAuto = true;
            record.IsMtgFhaInsured = true;
            record.IsPiggyBack = true;
            record.IsSeeAttachment = true;
            record.IsSp1stMtgData = true;
            record.Late30AsString = CountString.Create("3");
            record.Late60AsString = CountString.Create("2");
            record.Late90PlusAsString = CountString.Create("1");
            record.OriginalDebtAmt = Money.Create(11520.11m);
            record.OriginalTermAsString = CountString.Create("240");
            record.PayoffAmtData = Money.Create(5220.91m);
            record.PayoffAmtLocked = true;
            record.PayoffTimingData = PayoffTiming.BeforeClosing;
            record.PayoffTimingLockedData = false;
            record.PrepDate = UnzonedDate.Create(DateTime.Parse("11/5/16"));
            record.Rate = Percentage.Create(4);
            record.ReconcileStatusType = E_LiabilityReconcileStatusT.CreditReportOnly;
            record.UsedInRatioData = true;
            record.VerifExpiresDate = UnzonedDate.Create(DateTime.Parse("11/6/16"));
            record.VerifRecvDate = UnzonedDate.Create(DateTime.Parse("11/7/16"));
            record.VerifReorderedDate = UnzonedDate.Create(DateTime.Parse("11/8/16"));
            record.VerifSentDate = UnzonedDate.Create(DateTime.Parse("11/9/16"));
            record.OwedTo = PersonName.Create("Zeke J. Honda");
            record.JobExpenseDesc = DescriptionField.Create("None.");
            record.PmlAuditTrail = CreateDefaultEvents();

            return record;
        }

        private static List<LiabilityPmlAuditTrailEvent> CreateDefaultEvents()
        {
            var list = new List<LiabilityPmlAuditTrailEvent>();
            list.Add(new LiabilityPmlAuditTrailEvent(
                "User0",
                "UserZero",
                "Liability Create",
                "",
                "",
                Tools.GetDateTimeDescription(DateTime.Parse("11/1/16"))));

            list.Add(new LiabilityPmlAuditTrailEvent(
                "User1",
                "UserOne",
                "Field Update",
                "Used In Ratio",
                "Yes",
                Tools.GetDateTimeDescription(DateTime.Parse("11/1/16"))));

            return list;
        }

        private IMutableLqbCollection<DataObjectKind.Liability, Guid, Liability> LoadCollection(
            ILqbCollectionSqlMapper<DataObjectKind.Liability, Guid, Liability> mapper)
        {
            return LoadCollection(mapper, this.connection, this.transaction);
        }

        private void SaveCollection(
            ILqbCollectionSqlMapper<DataObjectKind.Liability, Guid, Liability> mapper,
            IMutableLqbCollection<DataObjectKind.Liability, Guid, Liability> collection)
        {
            SaveCollection(mapper, collection, this.connection, this.transaction);
        }

        internal static IMutableLqbCollection<DataObjectKind.Liability, Guid, Liability> LoadCollection(
            ILqbCollectionSqlMapper<DataObjectKind.Liability, Guid, Liability> mapper,
            IDbConnection connection, IDbTransaction transaction)
        {
            return mapper.Load(
                connection,
                transaction,
                Name.Create("Test").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.Liability>(),
                null,
                new DataReaderConverter());
        }

        internal static void SaveCollection(
            ILqbCollectionSqlMapper<DataObjectKind.Liability, Guid, Liability> mapper,
            IMutableLqbCollection<DataObjectKind.Liability, Guid, Liability> collection,
            IDbConnection connection, IDbTransaction transaction)
        {
            mapper.Save(
                connection,
                transaction,
                new SqlParameterConverter(),
                collection);
        }

        private DbConnection GetConnection()
        {
            return DbConnectionInfo.GetConnectionInfo(this.brokerId.Value).GetConnection();
        }
    }
}
