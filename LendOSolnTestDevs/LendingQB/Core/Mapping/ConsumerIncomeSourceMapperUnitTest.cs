﻿namespace LendingQB.Test.Developers.LendingQB.Core.Mapping
{
    using CommonProjectLib.Common;
    using global::LendingQB.Core;
    using global::LendingQB.Core.Data;
    using global::LendingQB.Core.Mapping;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;
    using NUnit.Framework;
    using System;
    using System.Data;
    using NSubstitute;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public class ConsumerIncomeSourceMapperUnitTest
    {
        private static readonly DataObjectIdentifier<DataObjectKind.Consumer, Guid> ConsumerId = DataObjectIdentifier<DataObjectKind.Consumer, Guid>.Create(Guid.Empty);
        private static readonly DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid> AppId = DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>.Create(Guid.Empty);
        private static readonly DataObjectIdentifier<DataObjectKind.IncomeSource, Guid> IncomeSourceId = DataObjectIdentifier<DataObjectKind.IncomeSource, Guid>.Create(new Guid("11111111-1111-1111-1111-111111111111"));
        private static readonly DataObjectIdentifier<DataObjectKind.IncomeSource, Guid> IncomeSourceId2 = DataObjectIdentifier<DataObjectKind.IncomeSource, Guid>.Create(new Guid("22222222-2222-2222-2222-222222222222"));

        [Test]
        public void Save_Empty_NoException()
        {
            var driver = NSubstitute.Substitute.For<IStoredProcedureDriver>();
            var mapper = this.GetMapper(driver);
            var associations = this.GetAssociationSet();

            mapper.Save(
                Substitute.For<IDbConnection>(),
                Substitute.For<IDbTransaction>(),
                Substitute.For<ISqlParameterConverter>(),
                associations);

            driver.DidNotReceiveWithAnyArgs().ExecuteNonQuery(null, null, StoredProcedureName.Invalid, null);
        }

        [Test]
        public void Save_MultiplePrimaryForSingleConsumer_ThrowsException()
        {
            var mapper = this.GetMapper(NSubstitute.Substitute.For<IStoredProcedureDriver>());
            var associations = this.GetAssociationSet();

            var primary1 = new ConsumerIncomeSourceAssociation(ConsumerId, IncomeSourceId, AppId);
            primary1.IsPrimary = true;
            associations.Add(primary1);

            var primary2 = new ConsumerIncomeSourceAssociation(ConsumerId, IncomeSourceId, AppId);
            primary2.IsPrimary = true;
            associations.Add(primary2);

            // Here we are testing ValidateForSave
            Assert.Throws<global::DataAccess.CBaseException>(() => mapper.Save(
                Substitute.For<IDbConnection>(),
                Substitute.For<IDbTransaction>(),
                Substitute.For<ISqlParameterConverter>(),
                associations));
        }

        [Test]
        public void Save_MultipleIncomeSinglePrimaryForSingleConsumer_Succeeds()
        {
            //Testing ValidateForSave 
            var driver = NSubstitute.Substitute.For<IStoredProcedureDriver>();
            var mapper = this.GetMapper(driver);
            var associations = this.GetAssociationSet();

            var primary = new ConsumerIncomeSourceAssociation(ConsumerId, IncomeSourceId, AppId);
            primary.IsPrimary = true;
            associations.Add(primary);
            var secondary = new ConsumerIncomeSourceAssociation(ConsumerId, IncomeSourceId, AppId);
            secondary.IsPrimary = false;
            associations.Add(secondary);

            mapper.Save(
                Substitute.For<IDbConnection>(),
                Substitute.For<IDbTransaction>(),
                Substitute.For<ISqlParameterConverter>(),
                associations);

            // Here we are testing ValidateForSave
            driver.ReceivedWithAnyArgs().ExecuteNonQuery(null, null, StoredProcedureName.Invalid, null);
        }

        [Test]
        public void Save_MultipleIncomesNoPrimaryForSingleConsumer_ThrowsException()
        {
            var driver = Substitute.For<IStoredProcedureDriver>();
            var mapper = this.GetMapper(driver);
            var associations = this.GetAssociationSet();

            var association1 = new ConsumerIncomeSourceAssociation(ConsumerId, IncomeSourceId, AppId);
            association1.IsPrimary = false;
            associations.Add(association1);
            var association2 = new ConsumerIncomeSourceAssociation(ConsumerId, IncomeSourceId, AppId);
            association2.IsPrimary = false;
            associations.Add(association2);

            
            // Here we are testing ValidateForSave
            Assert.Throws<global::DataAccess.CBaseException>(() => mapper.Save(
                Substitute.For<IDbConnection>(),
                Substitute.For<IDbTransaction>(),
                Substitute.For<ISqlParameterConverter>(),
                associations));
        }

        private ConsumerIncomeSourceMapper GetMapper(IStoredProcedureDriver driver)
        {
            return new ConsumerIncomeSourceMapper(DataObjectIdentifier.Create<DataObjectKind.Loan>(Guid.Empty), driver);
        }


        private IMutableLqbCollection<DataObjectKind.ConsumerIncomeSourceAssociation, Guid, ConsumerIncomeSourceAssociation> GetAssociationSet()
        {
            var associationSet = LqbCollection<DataObjectKind.ConsumerIncomeSourceAssociation, Guid, ConsumerIncomeSourceAssociation>.Create(
                Name.Create("ConsumerIncomeSourceAssociations").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.ConsumerIncomeSourceAssociation>(),
                null);

            associationSet.BeginTrackingChanges();

            return associationSet;
        }
    }
}
