﻿namespace LendingQB.Test.Developers.LendingQB.Core
{
    using System;
    using System.Collections.Generic;
    using global::DataAccess;
    using LqbGrammar.DataTypes;
    using global::LendingQB.Core;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public sealed class DerivedOrderedIdentifierCollectionTest
    {
        private DataObjectIdentifier<DataObjectKind.Asset, Guid> id00;
        private DataObjectIdentifier<DataObjectKind.Asset, Guid> id01;
        private DataObjectIdentifier<DataObjectKind.Asset, Guid> id10;
        private DataObjectIdentifier<DataObjectKind.Asset, Guid> id11;
        private DataObjectIdentifier<DataObjectKind.Asset, Guid> id12;
        private DataObjectIdentifier<DataObjectKind.Asset, Guid> id20;
        private DataObjectIdentifier<DataObjectKind.Asset, Guid> id21;
        private DataObjectIdentifier<DataObjectKind.Asset, Guid> id22;
        private DataObjectIdentifier<DataObjectKind.Asset, Guid> id23;

        public DerivedOrderedIdentifierCollectionTest()
        {
            var factory = new GuidDataObjectIdentifierFactory<DataObjectKind.Asset>();
            this.id00 = factory.NewId();
            this.id01 = factory.NewId();
            this.id10 = factory.NewId();
            this.id11 = factory.NewId();
            this.id12 = factory.NewId();
            this.id20 = factory.NewId();
            this.id21 = factory.NewId();
            this.id22 = factory.NewId();
            this.id23 = factory.NewId();
        }

        [Test]
        public void Count_InitializedCollection_CorrectValue()
        {
            var order = this.CreateCollection();
            Assert.AreEqual(9, order.Count);
        }

        [Test]
        public void GetEnumerator_InitializedCollection_ContainsAllValuesOnce()
        {
            var values = this.GetAllValues();

            int count = 0;
            var order = this.CreateCollection();
            foreach (var item in order)
            {
                ++count;
                Assert.IsTrue(values.Contains(item));
                values.Remove(item);
            }

            Assert.AreEqual(9, count);
            Assert.AreEqual(0, values.Count);
        }

        [Test]
        public void GetIdentifierAt_WithinRange_CorrectValueReturned()
        {
            var order = this.CreateCollection();

            var id0 = order.GetIdentifierAt(0);
            var id1 = order.GetIdentifierAt(1);
            var id2 = order.GetIdentifierAt(2);
            var id3 = order.GetIdentifierAt(3);
            var id4 = order.GetIdentifierAt(4);
            var id5 = order.GetIdentifierAt(5);
            var id6 = order.GetIdentifierAt(6);
            var id7 = order.GetIdentifierAt(7);
            var id8 = order.GetIdentifierAt(8);

            Assert.AreEqual(this.id00, id0);
            Assert.AreEqual(this.id01, id1);
            Assert.AreEqual(this.id10, id2);
            Assert.AreEqual(this.id11, id3);
            Assert.AreEqual(this.id12, id4);
            Assert.AreEqual(this.id20, id5);
            Assert.AreEqual(this.id21, id6);
            Assert.AreEqual(this.id22, id7);
            Assert.AreEqual(this.id23, id8);
        }

        [Test]
        public void GetIdentifierAt_NegativePosition_Throws()
        {
            var order = this.CreateCollection();

            Assert.Throws<IndexOutOfRangeException>(() => order.GetIdentifierAt(-1));
        }

        [Test]
        public void GetIdentifierAt_BeyondRange_Throws()
        {
            var order = this.CreateCollection();

            Assert.Throws<IndexOutOfRangeException>(() => order.GetIdentifierAt(9));
        }

        [Test]
        public void GetPositionOf_ValidValues_CorrectPositions()
        {
            var order = this.CreateCollection();

            int pos0 = order.GetPositionOf(this.id00);
            int pos1 = order.GetPositionOf(this.id01);
            int pos2 = order.GetPositionOf(this.id10);
            int pos3 = order.GetPositionOf(this.id11);
            int pos4 = order.GetPositionOf(this.id12);
            int pos5 = order.GetPositionOf(this.id20);
            int pos6 = order.GetPositionOf(this.id21);
            int pos7 = order.GetPositionOf(this.id22);
            int pos8 = order.GetPositionOf(this.id23);

            Assert.AreEqual(0, pos0);
            Assert.AreEqual(1, pos1);
            Assert.AreEqual(2, pos2);
            Assert.AreEqual(3, pos3);
            Assert.AreEqual(4, pos4);
            Assert.AreEqual(5, pos5);
            Assert.AreEqual(6, pos6);
            Assert.AreEqual(7, pos7);
            Assert.AreEqual(8, pos8);
        }

        [Test]
        public void GetPositionOf_InvalidValue_Throws()
        {
            var order = this.CreateCollection();

            var guid = Guid.NewGuid();
            var invalidId = DataObjectIdentifier<DataObjectKind.Asset, Guid>.Create(guid);

            Assert.Throws<CBaseException>(() => order.GetPositionOf(invalidId));
        }

        [Test]
        public void MoveBack_OneStepWithinSubCollection_OrderChanged()
        {
            var order = this.CreateCollection();

            var idToMoveBack = this.id21;
            var idSwapped = this.id22;

            int posFirst = order.GetPositionOf(idToMoveBack);
            int posSecond = order.GetPositionOf(idSwapped);
            Assert.AreNotEqual(0, posFirst);
            Assert.AreNotEqual(0, posSecond);
            Assert.IsTrue(posFirst + 1 == posSecond);

            order.MoveBack(idToMoveBack);

            int posMoved = order.GetPositionOf(idToMoveBack);
            int posSwapped = order.GetPositionOf(idSwapped);

            Assert.AreEqual(posSecond, posMoved);
            Assert.AreEqual(posFirst, posSwapped);
        }

        [Test]
        public void MoveBack_LastInSubCollection_NoChange()
        {
            var order = this.CreateCollection();

            var idMoved = this.id12;
            int posPrior = order.GetPositionOf(idMoved);

            order.MoveBack(idMoved);
            int posAfter = order.GetPositionOf(idMoved);

            Assert.AreEqual(posPrior, posAfter);
        }

        [Test]
        public void MoveForward_OneStepWithinSubCollection_OrderChanged()
        {
            var order = this.CreateCollection();

            var idToMoveForward = this.id22;
            var idSwapped = this.id21;

            int posFirst = order.GetPositionOf(idSwapped);
            int posSecond = order.GetPositionOf(idToMoveForward);
            Assert.AreNotEqual(0, posFirst);
            Assert.AreNotEqual(0, posSecond);
            Assert.IsTrue(posFirst + 1 == posSecond);

            order.MoveForward(idToMoveForward);

            int posMoved = order.GetPositionOf(idToMoveForward);
            int posSwapped = order.GetPositionOf(idSwapped);

            Assert.AreEqual(posFirst, posMoved);
            Assert.AreEqual(posSecond, posSwapped);
        }

        [Test]
        public void MoveForward_FirstInSubCollection_NoChange()
        {
            var order = this.CreateCollection();

            var idMoved = this.id10;
            int posPrior = order.GetPositionOf(idMoved);

            order.MoveForward(idMoved);
            int posAfter = order.GetPositionOf(idMoved);

            Assert.AreEqual(posPrior, posAfter);
        }

        [Test]
        public void Reorder_InitializedCollection_Throws()
        {
            var order = this.CreateCollection();

            var prevOrder = new DataObjectIdentifier<DataObjectKind.Asset, Guid>[] {this.id00, this.id01,
                                                                                    this.id10, this.id11, this.id12,
                                                                                    this.id20, this.id21, this.id22, this.id23};
            var newOrder = new DataObjectIdentifier<DataObjectKind.Asset, Guid>[] {this.id10, this.id11,
                                                                                    this.id00, this.id01, this.id12,
                                                                                    this.id21, this.id22, this.id23, this.id20};

            Assert.Throws<NotImplementedException>(() => order.ReOrder(prevOrder, newOrder));
        }

        private HashSet<DataObjectIdentifier<DataObjectKind.Asset, Guid>> GetAllValues()
        {
            var set = new HashSet<DataObjectIdentifier<DataObjectKind.Asset, Guid>>();
            set.Add(id00);
            set.Add(id01);
            set.Add(id10);
            set.Add(id11);
            set.Add(id12);
            set.Add(id20);
            set.Add(id21);
            set.Add(id22);
            set.Add(id23);

            return set;
        }

        private IOrderedIdentifierCollection<DataObjectKind.Asset, Guid> CreateCollection()
        {
            var list = new List<IOrderedIdentifierCollection<DataObjectKind.Asset, Guid>>();

            var order = OrderedIdentifierCollection<DataObjectKind.Asset, Guid>.Create();
            var init = order as IInitializeCollectionOrder<Guid>;
            init.Add(this.id00.Value);
            init.Add(this.id01.Value);
            list.Add(order);

            order = OrderedIdentifierCollection<DataObjectKind.Asset, Guid>.Create();
            init = order as IInitializeCollectionOrder<Guid>;
            init.Add(this.id10.Value);
            init.Add(this.id11.Value);
            init.Add(this.id12.Value);
            list.Add(order);

            order = OrderedIdentifierCollection<DataObjectKind.Asset, Guid>.Create();
            init = order as IInitializeCollectionOrder<Guid>;
            init.Add(this.id20.Value);
            init.Add(this.id21.Value);
            init.Add(this.id22.Value);
            init.Add(this.id23.Value);
            list.Add(order);

            return new DerivedOrderedIdentifierCollection<DataObjectKind.Asset, Guid>(list);
        }
    }
}
