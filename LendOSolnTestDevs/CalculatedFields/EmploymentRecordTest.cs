﻿namespace LendingQB.Test.Developers.CalculatedFields
{
    using LendersOffice;
    using LqbGrammar.DataTypes;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public sealed class EmploymentRecordTest
    {
        [Test]
        public void MonthPortionOfEmploymentTest()
        {
            var start = UnzonedDate.Create(2005, 7, 1);
            var end = UnzonedDate.Create(2018, 5, 11);

            var count = CalculatedFields.EmploymentRecord.MonthPortionOfEmployment(start.Value, end.Value);
            Assert.AreEqual(10, count.Value);
        }

        [Test]
        public void YearPortionOfEmploymentTest()
        {
            var start = UnzonedDate.Create(2005, 7, 1);
            var end = UnzonedDate.Create(2018, 5, 11);

            var count = CalculatedFields.EmploymentRecord.YearPortionOfEmployment(start.Value, end.Value);
            Assert.AreEqual(12, count.Value);
        }
    }
}
