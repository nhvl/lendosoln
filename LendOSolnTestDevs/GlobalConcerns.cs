﻿namespace LendingQB.Test.Developers
{
    using LqbGrammar;
    using LqbGrammar.Drivers;
    using NUnit.Framework;
    using Utils;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public sealed class GlobalConcerns
    {
        [Test]
        public void RegistrationsShouldGoAway()
        {
            using (var helper = new FoolHelper())
            {
                if (!helper.RegisterRealDriver(FoolHelper.DriverType.Base64Encoding))
                {
                    return;
                }

                var goodFactory = GenericLocator<IBase64EncodingDriverFactory>.Factory;
                Assert.IsNotNull(goodFactory, "Good factory should not be null");
            }

            var nullFactory = GenericLocator<IBase64EncodingDriverFactory>.Factory;
            Assert.IsNull(nullFactory, "Factory should be null after FoolHelper.Dispose() has been called");
        }
    }
}
