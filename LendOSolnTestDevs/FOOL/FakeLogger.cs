﻿using System.Collections.Generic;
using LendersOffice.Logging;

namespace LendingQB.Test.Developers.FOOL
{
    public class FakeLogger : ILogger
    {
        public void Log(LoggingLevel level, string message, Dictionary<string, string> properties)
        {
        }

        public void Log(LoggingLevel level, string message)
        {
        }

        public class Factory : ILoggerFactory
        {
            private ILogger instance = new FakeLogger();

            public ILogger GetLogger() => this.instance;
        }
    }
}
