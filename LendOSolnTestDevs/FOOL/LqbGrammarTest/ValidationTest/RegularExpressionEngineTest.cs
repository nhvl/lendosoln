﻿// <copyright file="RegularExpressionEngineTest.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Douglas Telfer
//    Date:   09/20/2016
// </summary>

namespace LendingQB.Test.Developers.FOOL.LqbGrammarTest.ValidationTest
{
    using System;
    using System.Reflection;
    using LqbGrammar;
    using LqbGrammar.Adapters;
    using LqbGrammar.DataTypes;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public class RegularExpressionEngineTest
    {
        /// <summary>
        /// The static RegularExpressionString fields in the RegularExpressionString class bypass validation
        /// on construction. Test them here.
        /// </summary>
        [Test]
        public void TestRegexStrings()
        {
            Type regexStringsType = typeof(RegularExpressionString);
            FieldInfo[] fields = regexStringsType.GetFields(BindingFlags.Static | BindingFlags.Public);

            foreach (FieldInfo field in fields)
            {
                if (field.FieldType == typeof(RegularExpressionString))
                {
                    string fieldName = field.Name;

                    if (fieldName == "Invalid") continue;

                    RegularExpressionString classRegexString = (RegularExpressionString)field.GetValue(null);
                    string pattern = classRegexString.ToString();
                    RegularExpressionString? parsedRegexString = RegularExpressionString.Create(pattern);

                    Assert.IsTrue(parsedRegexString.HasValue, "RegularExpressionString '" + fieldName + "' failed to parse");

                    Assert.AreEqual(classRegexString, parsedRegexString.Value, "Parsed RegularExpressionString '" + fieldName + "' is not equal to its static RegularExpressionString field");
                }
            }
        }
    }
}
