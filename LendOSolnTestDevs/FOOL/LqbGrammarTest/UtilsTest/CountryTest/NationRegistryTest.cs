﻿namespace LendingQB.Test.Developers.FOOL.LqbGrammarTest.UtilsTest.CountryTest
{
    using global::LqbGrammar.Utils.Country;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public sealed class NationRegistryTest
    {
        [Test]
        public void GetData_UnitedStates_CorrectValues()
        {
            var data = NationRegistry.GetData(Nation.UnitedStates);
            Assert.AreEqual("United States", data.Country);
            Assert.AreEqual("USA", data.Iso_3166_3);
            Assert.AreEqual("US", data.Iso_3166_2);
        }

        [Test]
        public void GetData_Canada_CorrectValues()
        {
            var data = NationRegistry.GetData(Nation.Canada);
            Assert.AreEqual("Canada", data.Country);
            Assert.AreEqual("CAN", data.Iso_3166_3);
            Assert.AreEqual("CA", data.Iso_3166_2);
        }

        [Test]
        public void GetData_Mexico_CorrectValues()
        {
            var data = NationRegistry.GetData(Nation.Mexico);
            Assert.AreEqual("Mexico", data.Country);
            Assert.AreEqual("MEX", data.Iso_3166_3);
            Assert.AreEqual("MX", data.Iso_3166_2);
        }

        [Test]
        public void GetNationFromCountryCode_ISO_3166_2_GoodValues_ExpectedCountryReturned()
        {
            var nation = NationRegistry.GetNationFromCountryCode_ISO_3166_2("US");
            Assert.AreEqual(Nation.UnitedStates, nation);

            nation = NationRegistry.GetNationFromCountryCode_ISO_3166_2("CA");
            Assert.AreEqual(Nation.Canada, nation);

            nation = NationRegistry.GetNationFromCountryCode_ISO_3166_2("MX");
            Assert.AreEqual(Nation.Mexico, nation);
        }

        [Test]
        public void GetNationFromCountryCode_ISO_3166_2_BadValues_ReturnsNull()
        {
            var nation = NationRegistry.GetNationFromCountryCode_ISO_3166_2("XX");
            Assert.AreEqual(null, nation);

            nation = NationRegistry.GetNationFromCountryCode_ISO_3166_2(string.Empty);
            Assert.AreEqual(null, nation);

            nation = NationRegistry.GetNationFromCountryCode_ISO_3166_2(null);
            Assert.AreEqual(null, nation);
        }

        [Test]
        public void GetNationFromCountryCode_ISO_3166_3_GoodValues_ExpectedCountryReturned()
        {
            var nation = NationRegistry.GetNationFromCountryCode_ISO_3166_3("USA");
            Assert.AreEqual(Nation.UnitedStates, nation);

            nation = NationRegistry.GetNationFromCountryCode_ISO_3166_3("CAN");
            Assert.AreEqual(Nation.Canada, nation);

            nation = NationRegistry.GetNationFromCountryCode_ISO_3166_3("MEX");
            Assert.AreEqual(Nation.Mexico, nation);
        }

        [Test]
        public void GetNationFromCountryCode_ISO_3166_3_BadValues_ReturnsNull()
        {
            var nation = NationRegistry.GetNationFromCountryCode_ISO_3166_3("XX");
            Assert.AreEqual(null, nation);

            nation = NationRegistry.GetNationFromCountryCode_ISO_3166_3(string.Empty);
            Assert.AreEqual(null, nation);

            nation = NationRegistry.GetNationFromCountryCode_ISO_3166_3(null);
            Assert.AreEqual(null, nation);
        }
    }
}
