﻿namespace LendingQB.Test.Developers.FOOL.LqbGrammarTest.UtilsTest
{
    using System;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Utils;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public class TimeUtilsTest
    {
        [Test]
        public void CalcWholeMonthsTest_StartAtBegin_EndBeginPrevMonth()
        {
            string start = "2005-02-01";
            string end = "2018-01-01";
            int expectedMonths = 155;

            this.ExecuteTest(start, end, expectedMonths);
        }

        [Test]
        public void CalcWholeMonthsTest_StartAtBegin_EndNearEndPrevMonth()
        {
            string start = "2005-02-01";
            string end = "2018-01-30";
            int expectedMonths = 155;

            this.ExecuteTest(start, end, expectedMonths);
        }

        [Test]
        public void CalcWholeMonthsTest_StartAtBegin_EndAtEndPrevMonth()
        {
            string start = "2005-02-01";
            string end = "2018-01-31";
            int expectedMonths = 155;

            this.ExecuteTest(start, end, expectedMonths);
        }

        [Test]
        public void CalcWholeMonthsTest_StartAtBegin_EndBeginCurrMonth()
        {
            string start = "2005-02-01";
            string end = "2018-02-01";
            int expectedMonths = 156;

            this.ExecuteTest(start, end, expectedMonths);
        }

        [Test]
        public void CalcWholeMonthsTest_StartAtBegin_EndNearEndCurrMonth()
        {
            string start = "2005-02-01";
            string end = "2018-02-27";
            int expectedMonths = 156;

            this.ExecuteTest(start, end, expectedMonths);
        }

        [Test]
        public void CalcWholeMonthsTest_StartAtBegin_EndAtEndCurrMonth()
        {
            string start = "2005-02-01";
            string end = "2018-02-28";
            int expectedMonths = 156;

            this.ExecuteTest(start, end, expectedMonths);
        }

        [Test]
        public void CalcWholeMonthsTest_StartAtBegin_EndBeginNextMonth()
        {
            string start = "2005-02-01";
            string end = "2018-03-01";
            int expectedMonths = 157;

            this.ExecuteTest(start, end, expectedMonths);
        }

        [Test]
        public void CalcWholeMonthsTest_StartAtBegin_EndNearEndNextMonth()
        {
            string start = "2005-02-01";
            string end = "2018-03-30";
            int expectedMonths = 157;

            this.ExecuteTest(start, end, expectedMonths);
        }

        [Test]
        public void CalcWholeMonthsTest_StartAtBegin_EndAtEndNextMonth()
        {
            string start = "2005-02-01";
            string end = "2018-03-31";
            int expectedMonths = 157;

            this.ExecuteTest(start, end, expectedMonths);
        }

        //---------------------------------------------------------------------

        [Test]
        public void CalcWholeMonthsTest_StartAtMiddle_EndPreMidPrevMonth()
        {
            string start = "2005-02-13";
            string end = "2018-01-12";
            int expectedMonths = 154;

            this.ExecuteTest(start, end, expectedMonths);
        }

        [Test]
        public void CalcWholeMonthsTest_StartAtMiddle_EndMidPrevMonth()
        {
            string start = "2005-02-13";
            string end = "2018-01-13";
            int expectedMonths = 155;

            this.ExecuteTest(start, end, expectedMonths);
        }

        [Test]
        public void CalcWholeMonthsTest_StartAtMiddle_EndPostMidPrevMonth()
        {
            string start = "2005-02-13";
            string end = "2018-01-14";
            int expectedMonths = 155;

            this.ExecuteTest(start, end, expectedMonths);
        }

        [Test]
        public void CalcWholeMonthsTest_StartAtMiddle_EndPreMidCurrMonth()
        {
            string start = "2005-02-13";
            string end = "2018-02-12";
            int expectedMonths = 155;

            this.ExecuteTest(start, end, expectedMonths);
        }

        [Test]
        public void CalcWholeMonthsTest_StartAtMiddle_EndMidCurrMonth()
        {
            string start = "2005-02-13";
            string end = "2018-02-13";
            int expectedMonths = 156;

            this.ExecuteTest(start, end, expectedMonths);
        }

        [Test]
        public void CalcWholeMonthsTest_StartAtMiddle_EndPostMidCurrMonth()
        {
            string start = "2005-02-13";
            string end = "2018-02-14";
            int expectedMonths = 156;

            this.ExecuteTest(start, end, expectedMonths);
        }

        [Test]
        public void CalcWholeMonthsTest_StartAtMiddle_EndPreMidNextMonth()
        {
            string start = "2005-02-13";
            string end = "2018-03-12";
            int expectedMonths = 156;

            this.ExecuteTest(start, end, expectedMonths);
        }

        [Test]
        public void CalcWholeMonthsTest_StartAtMiddle_EndMidNextMonth()
        {
            string start = "2005-02-13";
            string end = "2018-03-13";
            int expectedMonths = 157;

            this.ExecuteTest(start, end, expectedMonths);
        }

        [Test]
        public void CalcWholeMonthsTest_StartAtMiddle_EndPostMidNextMonth()
        {
            string start = "2005-02-13";
            string end = "2018-03-14";
            int expectedMonths = 157;

            this.ExecuteTest(start, end, expectedMonths);
        }

        //---------------------------------------------------------------------

        [Test]
        public void CalcWholeMonthsTest_StartAtEnd28_EndBeginPrevMonth()
        {
            string start = "2005-02-28";
            string end = "2018-01-01";
            int expectedMonths = 154;

            this.ExecuteTest(start, end, expectedMonths);
        }

        [Test]
        public void CalcWholeMonthsTest_StartAtEnd28_EndNearEndPrevMonth()
        {
            string start = "2005-02-28";
            string end = "2018-01-30";
            int expectedMonths = 154;

            this.ExecuteTest(start, end, expectedMonths);
        }

        [Test]
        public void CalcWholeMonthsTest_StartAtEnd28_EndAtEndPrevMonth()
        {
            string start = "2005-02-28";
            string end = "2018-01-31";
            int expectedMonths = 155;

            this.ExecuteTest(start, end, expectedMonths);
        }

        [Test]
        public void CalcWholeMonthsTest_StartAtEnd28_EndBeginCurrMonth()
        {
            string start = "2005-02-28";
            string end = "2018-02-01";
            int expectedMonths = 155;

            this.ExecuteTest(start, end, expectedMonths);
        }

        [Test]
        public void CalcWholeMonthsTest_StartAtEnd28_EndNearEndCurrMonth()
        {
            string start = "2005-02-28";
            string end = "2018-02-27";
            int expectedMonths = 155;

            this.ExecuteTest(start, end, expectedMonths);
        }

        [Test]
        public void CalcWholeMonthsTest_StartAtEnd28_EndAtEndCurrMonth()
        {
            string start = "2005-02-28";
            string end = "2018-02-28";
            int expectedMonths = 156;

            this.ExecuteTest(start, end, expectedMonths);
        }

        [Test]
        public void CalcWholeMonthsTest_StartAtEnd28_EndBeginNextMonth()
        {
            string start = "2005-02-28";
            string end = "2018-03-01";
            int expectedMonths = 156;

            this.ExecuteTest(start, end, expectedMonths);
        }

        [Test]
        public void CalcWholeMonthsTest_StartAtEnd28_EndNearEndNextMonth()
        {
            string start = "2005-02-28";
            string end = "2018-03-30";
            int expectedMonths = 156;

            this.ExecuteTest(start, end, expectedMonths);
        }

        [Test]
        public void CalcWholeMonthsTest_StartAtEnd28_EndAtEndNextMonth()
        {
            string start = "2005-02-28";
            string end = "2018-03-31";
            int expectedMonths = 157;

            this.ExecuteTest(start, end, expectedMonths);
        }

        //---------------------------------------------------------------------

        [Test]
        public void CalcWholeMonthsTest_StartAtEnd29_EndBeginPrevMonth()
        {
            string start = "2004-02-29";
            string end = "2017-01-01";
            int expectedMonths = 154;

            this.ExecuteTest(start, end, expectedMonths);
        }

        [Test]
        public void CalcWholeMonthsTest_StartAtEnd29_EndNearEndPrevMonth()
        {
            string start = "2004-02-29";
            string end = "2017-01-30";
            int expectedMonths = 154;

            this.ExecuteTest(start, end, expectedMonths);
        }

        [Test]
        public void CalcWholeMonthsTest_StartAtEnd29_EndAtEndPrevMonth()
        {
            string start = "2004-02-29";
            string end = "2017-01-31";
            int expectedMonths = 155;

            this.ExecuteTest(start, end, expectedMonths);
        }

        [Test]
        public void CalcWholeMonthsTest_StartAtEnd29_EndBeginCurrMonth()
        {
            string start = "2004-02-29";
            string end = "2017-02-01";
            int expectedMonths = 155;

            this.ExecuteTest(start, end, expectedMonths);
        }

        [Test]
        public void CalcWholeMonthsTest_StartAtEnd29_EndNearEndCurrMonth()
        {
            string start = "2004-02-29";
            string end = "2017-02-27";
            int expectedMonths = 155;

            this.ExecuteTest(start, end, expectedMonths);
        }

        [Test]
        public void CalcWholeMonthsTest_StartAtEnd29_EndAtEndCurrMonth()
        {
            string start = "2004-02-29";
            string end = "2017-02-28";
            int expectedMonths = 156;

            this.ExecuteTest(start, end, expectedMonths);
        }

        [Test]
        public void CalcWholeMonthsTest_StartAtEnd29_EndBeginNextMonth()
        {
            string start = "2004-02-29";
            string end = "2017-03-01";
            int expectedMonths = 156;

            this.ExecuteTest(start, end, expectedMonths);
        }

        [Test]
        public void CalcWholeMonthsTest_StartAtEnd29_EndNearEndNextMonth()
        {
            string start = "2004-02-29";
            string end = "2017-03-30";
            int expectedMonths = 156;

            this.ExecuteTest(start, end, expectedMonths);
        }

        [Test]
        public void CalcWholeMonthsTest_StartAtEnd29_EndAtEndNextMonth()
        {
            string start = "2004-02-29";
            string end = "2017-03-31";
            int expectedMonths = 157;

            this.ExecuteTest(start, end, expectedMonths);
        }

        //---------------------------------------------------------------------

        [Test]
        public void CalcWholeMonthsTest_StartAtEnd30_EndBeginPrevMonth()
        {
            string start = "2005-04-30";
            string end = "2018-03-01";
            int expectedMonths = 154;

            this.ExecuteTest(start, end, expectedMonths);
        }

        [Test]
        public void CalcWholeMonthsTest_StartAtEnd30_EndNearEndPrevMonth()
        {
            string start = "2005-04-30";
            string end = "2018-03-30";
            int expectedMonths = 154;

            this.ExecuteTest(start, end, expectedMonths);
        }

        [Test]
        public void CalcWholeMonthsTest_StartAtEnd30_EndAtEndPrevMonth()
        {
            string start = "2005-04-30";
            string end = "2018-03-31";
            int expectedMonths = 155;

            this.ExecuteTest(start, end, expectedMonths);
        }

        [Test]
        public void CalcWholeMonthsTest_StartAtEnd30_EndBeginCurrMonth()
        {
            string start = "2005-04-30";
            string end = "2018-04-01";
            int expectedMonths = 155;

            this.ExecuteTest(start, end, expectedMonths);
        }

        [Test]
        public void CalcWholeMonthsTest_StartAtEnd30_EndNearEndCurrMonth()
        {
            string start = "2005-04-30";
            string end = "2018-04-29";
            int expectedMonths = 155;

            this.ExecuteTest(start, end, expectedMonths);
        }

        [Test]
        public void CalcWholeMonthsTest_StartAtEnd30_EndAtEndCurrMonth()
        {
            string start = "2005-04-30";
            string end = "2018-04-30";
            int expectedMonths = 156;

            this.ExecuteTest(start, end, expectedMonths);
        }

        [Test]
        public void CalcWholeMonthsTest_StartAtEnd30_EndBeginNextMonth()
        {
            string start = "2005-04-30";
            string end = "2018-05-01";
            int expectedMonths = 156;

            this.ExecuteTest(start, end, expectedMonths);
        }

        [Test]
        public void CalcWholeMonthsTest_StartAtEnd30_EndNearEndNextMonth()
        {
            string start = "2005-04-30";
            string end = "2018-05-30";
            int expectedMonths = 156;
            this.ExecuteTest(start, end, expectedMonths);
        }

        [Test]
        public void CalcWholeMonthsTest_StartAtEnd30_EndAtEndNextMonth()
        {
            string start = "2005-04-30";
            string end = "2018-05-31";
            int expectedMonths = 157;

            this.ExecuteTest(start, end, expectedMonths);
        }

        //---------------------------------------------------------------------

        [Test]
        public void CalcWholeMonthsTest_StartAtEnd31_EndBeginPrevMonth()
        {
            string start = "2005-05-31";
            string end = "2018-04-01";
            int expectedMonths = 154;

            this.ExecuteTest(start, end, expectedMonths);
        }

        [Test]
        public void CalcWholeMonthsTest_StartAtEnd31_EndNearEndPrevMonth()
        {
            string start = "2005-05-31";
            string end = "2018-04-29";
            int expectedMonths = 154;

            this.ExecuteTest(start, end, expectedMonths);
        }

        [Test]
        public void CalcWholeMonthsTest_StartAtEnd31_EndAtEndPrevMonth()
        {
            string start = "2005-05-31";
            string end = "2018-04-30";
            int expectedMonths = 155;

            this.ExecuteTest(start, end, expectedMonths);
        }

        [Test]
        public void CalcWholeMonthsTest_StartAtEnd31_EndBeginCurrMonth()
        {
            string start = "2005-05-31";
            string end = "2018-05-01";
            int expectedMonths = 155;

            this.ExecuteTest(start, end, expectedMonths);
        }

        [Test]
        public void CalcWholeMonthsTest_StartAtEnd31_EndNearEndCurrMonth()
        {
            string start = "2005-05-31";
            string end = "2018-05-30";
            int expectedMonths = 155;

            this.ExecuteTest(start, end, expectedMonths);
        }

        [Test]
        public void CalcWholeMonthsTest_StartAtEnd31_EndAtEndCurrMonth()
        {
            string start = "2005-05-31";
            string end = "2018-05-31";
            int expectedMonths = 156;

            this.ExecuteTest(start, end, expectedMonths);
        }

        [Test]
        public void CalcWholeMonthsTest_StartAtEnd31_EndBeginNextMonth()
        {
            string start = "2005-05-31";
            string end = "2018-06-01";
            int expectedMonths = 156;

            this.ExecuteTest(start, end, expectedMonths);
        }

        [Test]
        public void CalcWholeMonthsTest_StartAtEnd31_EndNearEndNextMonth()
        {
            string start = "2005-05-31";
            string end = "2018-06-29";
            int expectedMonths = 156;

            this.ExecuteTest(start, end, expectedMonths);
        }

        [Test]
        public void CalcWholeMonthsTest_StartAtEnd31_EndAtEndNextMonth()
        {
            string start = "2005-05-31";
            string end = "2018-06-30";
            int expectedMonths = 157;

            this.ExecuteTest(start, end, expectedMonths);
        }

        private void ExecuteTest(string start, string end, int expectedMonths)
        {
            int expectedWholeYears = expectedMonths / 12;

            var startDate = UnzonedDate.Create(start);
            var endDate = UnzonedDate.Create(end);

            var count = TimeUtils.CalcWholeMonths(startDate, endDate);
            var years = TimeUtils.CalcWholeYears(startDate, endDate);
            var duration = TimeUtils.CalculateYearDuration(startDate, endDate);

            int durationYears = Convert.ToInt32(Math.Floor(duration.Value.Value));
            decimal remainder = duration.Value.Value - durationYears;
            int durationMonths = Convert.ToInt32(Math.Floor(12 * remainder)); // note that this is the remainder in months

            Assert.AreEqual(expectedMonths, count.Value.Value);
            Assert.AreEqual(expectedWholeYears, years.Value.Value);
            Assert.AreEqual(expectedWholeYears, durationYears);

            // We expect the duration calculation to be approximate because it is difficult to determine the
            // correct number of days to use for a year.  However, we know that it should be accurate to within
            // a day, which can cause an error in the number of whole months when the day is on either side of a
            // whole month increment.
            int roundError = durationMonths -  (expectedMonths - 12 * expectedWholeYears);
            Assert.IsTrue(roundError == 0 || roundError == 1 || roundError == -1, "Round Error = " + roundError.ToString());
        }
    }
}
