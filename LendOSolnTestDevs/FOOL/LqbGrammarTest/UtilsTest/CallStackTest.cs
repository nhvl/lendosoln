﻿namespace LendingQB.Test.Developers.FOOL.LqbGrammarTest.UtilsTest
{
    using LqbGrammar.Utils;
    using NUnit.Framework;

    [TestFixture]
    public sealed class CallStackTest
    {
        [Test]
        [Category(CategoryConstants.UnitTest)]
        public void Test_NoType()
        {
            var stack = new CallStack(null);
            Assert.AreEqual(this.GetType().FullName, stack.ClassName);
            Assert.AreEqual("Test_NoType", stack.MethodName);
        }

        // [Test]
        [Category(CategoryConstants.UnitTest)]
        public void Test_Helper()
        {
            var helper = new CallHelper();
            helper.MakeCall(this);
        }

        private void Test_Helper_Impl()
        {
            var stack = new CallStack(typeof(CallHelper));
            Assert.AreEqual(this.GetType().FullName, stack.ClassName);
            Assert.AreEqual("Test_Helper", stack.MethodName);
        }

        private class CallHelper
        {
            public void MakeCall(CallStackTest tester)
            {
                this.ForwardCall(tester);
            }

            private void ForwardCall(CallStackTest tester)
            {
                tester.Test_Helper_Impl();
            }
        }
    }
}
