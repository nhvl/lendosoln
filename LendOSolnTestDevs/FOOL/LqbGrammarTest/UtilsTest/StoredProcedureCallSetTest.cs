﻿namespace LendingQB.Test.Developers.FOOL.LqbGrammarTest.UtilsTest
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Linq;
    using global::DataAccess;
    using global::LendersOffice.Drivers.SqlServerDB;
    using global::LendingQB.Core.Commands;
    using LendersOffice.Constants;
    using LqbGrammar.DataTypes;
    using LqbGrammar.DataTypes.PathDispatch;
    using LqbGrammar.Utils;
    using NUnit.Framework;
    using Utils;

    [TestFixture]
    public class StoredProcedureCallSetTest
    {
        [Test]
        [Category(CategoryConstants.UnitTest)]
        public void NoPramsAndWithParams_Aggregate_Works()
        {
            string firstSpName = "FIRST_SP";
            string secondSpName = "SECOND_SP";

            string firstPramName = "@intVal";
            int firstPramValue = 42;

            string secondPramName = "@stringVal";
            string secondPramValue = "Second Pram";

            var p1 = new SqlParameter(firstPramName, firstPramValue);
            var p2 = new SqlParameter(secondPramName, secondPramValue);
            p2.SqlDbType = System.Data.SqlDbType.VarChar;

            var prams = new SqlParameter[] { p1, p2 };

            var sp1 = StoredProcedureName.Create(firstSpName).Value;
            var sp2 = StoredProcedureName.Create(secondSpName).Value;

            var aggregator = new StoredProcedureCallSet();
            aggregator.Add(sp1, null);
            aggregator.Add(sp2, prams);

            SQLQueryString query;
            IEnumerable<DbParameter> allPrams;
            aggregator.AggregateCalls(out query, out allPrams);

            string sql = query.Value;
            Assert.IsTrue(sql.Contains("EXEC " + firstSpName + ";"));
            Assert.IsTrue(sql.Contains("EXEC " + secondSpName));
            Assert.IsTrue(sql.Contains(firstPramName + " = @p_0, "));
            Assert.IsTrue(sql.Contains(secondPramName + " = @p_1;"));
            Assert.AreEqual(2, allPrams.Count());

            var globalPram1 = allPrams.ElementAt(0);
            var globalPram2 = allPrams.ElementAt(1);
            Assert.AreEqual("@p_0", globalPram1.ParameterName);
            Assert.AreEqual("@p_1", globalPram2.ParameterName);
            Assert.IsTrue((int)globalPram1.Value == firstPramValue);
            Assert.IsTrue((string)globalPram2.Value == secondPramValue);
        }

        [Test]
        [Category(CategoryConstants.UnitTest)]
        public void TwoCallsWithPrams_Aggregate_Works()
        {
            string firstSpName = "FIRST_SP";
            string secondSpName = "SECOND_SP";

            string firstPramName = "@intVal";
            int firstPramValue = 42;

            string secondPramName = "@stringVal";
            string secondPramValue = "Second Pram";

            string thirdPramName = "@guidVal";
            Guid thirdPramValue = Guid.NewGuid();

            var p1 = new SqlParameter(firstPramName, firstPramValue);

            var p2 = new SqlParameter(secondPramName, secondPramValue);
            p2.SqlDbType = System.Data.SqlDbType.VarChar;

            var p3 = new SqlParameter(thirdPramName, thirdPramValue);
            p3.SqlDbType = System.Data.SqlDbType.UniqueIdentifier;

            var prams1 = new SqlParameter[] { p1, p2 };
            var prams2 = new SqlParameter[] { p3 };

            var sp1 = StoredProcedureName.Create(firstSpName).Value;
            var sp2 = StoredProcedureName.Create(secondSpName).Value;

            var aggregator = new StoredProcedureCallSet();
            aggregator.Add(sp1, prams1);
            aggregator.Add(sp2, prams2);

            SQLQueryString query;
            IEnumerable<DbParameter> allPrams;
            aggregator.AggregateCalls(out query, out allPrams);

            string sql = query.Value;
            Assert.IsTrue(sql.Contains("EXEC " + firstSpName));
            Assert.IsTrue(sql.Contains("EXEC " + secondSpName));
            Assert.IsTrue(sql.Contains(firstPramName + " = @p_0, "));
            Assert.IsTrue(sql.Contains(secondPramName + " = @p_1;"));
            Assert.IsTrue(sql.Contains(thirdPramName + " = @p_2;"));
            Assert.AreEqual(3, allPrams.Count());

            var globalPram1 = allPrams.ElementAt(0);
            var globalPram2 = allPrams.ElementAt(1);
            var globalPram3 = allPrams.ElementAt(2);
            Assert.AreEqual("@p_0", globalPram1.ParameterName);
            Assert.AreEqual("@p_1", globalPram2.ParameterName);
            Assert.AreEqual("@p_2", globalPram3.ParameterName);
            Assert.IsTrue((int)globalPram1.Value == firstPramValue);
            Assert.IsTrue((string)globalPram2.Value == secondPramValue);
            Assert.IsTrue((Guid)globalPram3.Value == thirdPramValue);
        }

        [Test]
        [Category(CategoryConstants.UnitTest)]
        public void PramsWithoutAtPrefix_Aggregate_Works()
        {
            string firstSpName = "FIRST_SP";
            string secondSpName = "SECOND_SP";

            string firstPramName = "intVal";
            int firstPramValue = 42;

            string secondPramName = "stringVal";
            string secondPramValue = "Second Pram";

            string thirdPramName = "guidVal";
            Guid thirdPramValue = Guid.NewGuid();

            var p1 = new SqlParameter(firstPramName, firstPramValue);

            var p2 = new SqlParameter(secondPramName, secondPramValue);
            p2.SqlDbType = System.Data.SqlDbType.VarChar;

            var p3 = new SqlParameter(thirdPramName, thirdPramValue);
            p3.SqlDbType = System.Data.SqlDbType.UniqueIdentifier;

            var prams1 = new SqlParameter[] { p1, p2 };
            var prams2 = new SqlParameter[] { p3 };

            var sp1 = StoredProcedureName.Create(firstSpName).Value;
            var sp2 = StoredProcedureName.Create(secondSpName).Value;

            var aggregator = new StoredProcedureCallSet();
            aggregator.Add(sp1, prams1);
            aggregator.Add(sp2, prams2);

            SQLQueryString query;
            IEnumerable<DbParameter> allPrams;
            aggregator.AggregateCalls(out query, out allPrams);

            string sql = query.Value;
            Assert.IsTrue(sql.Contains("EXEC " + firstSpName));
            Assert.IsTrue(sql.Contains("EXEC " + secondSpName));
            Assert.IsTrue(sql.Contains(firstPramName + " = @p_0, "));
            Assert.IsTrue(sql.Contains(secondPramName + " = @p_1;"));
            Assert.IsTrue(sql.Contains(thirdPramName + " = @p_2;"));
            Assert.AreEqual(3, allPrams.Count());

            var globalPram1 = allPrams.ElementAt(0);
            var globalPram2 = allPrams.ElementAt(1);
            var globalPram3 = allPrams.ElementAt(2);
            Assert.AreEqual("@p_0", globalPram1.ParameterName);
            Assert.AreEqual("@p_1", globalPram2.ParameterName);
            Assert.AreEqual("@p_2", globalPram3.ParameterName);
            Assert.IsTrue((int)globalPram1.Value == firstPramValue);
            Assert.IsTrue((string)globalPram2.Value == secondPramValue);
            Assert.IsTrue((Guid)globalPram3.Value == thirdPramValue);
        }

        [Test]
        [Category(CategoryConstants.IntegrationTest)]
        public void RealCalls_Aggregate_ReturnsDataSet()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.SqlQuery, FoolHelper.DriverType.StoredProcedure);

                var principal = LoginTools.LoginAs(TestAccountType.LoTest001);
                var creator = CLoanFileCreator.GetCreator(
                    principal,
                    LendersOffice.Audit.E_LoanCreationSource.UserCreateFromBlank);

                var loanId = creator.CreateBlankUladLoanFile();

                var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                    loanId,
                    typeof(StoredProcedureCallSetTest));
                loan.InitSave(ConstAppDavid.SkipVersionCheck);
                var appData = loan.GetAppData(0);

                var addCommand = new AddEntity(DataPath.Create("loan"), nameof(CPageBase.Assets), appData.aBConsumerId, null, null, null);
                loan.HandleAdd(addCommand, null);

                addCommand = new AddEntity(DataPath.Create("loan"), nameof(CPageBase.Liabilities), appData.aBConsumerId, null, null, null);
                loan.HandleAdd(addCommand, null);

                loan.Save();

                var firstSpName = "ASSET_RetrieveByLoanIdBrokerId";
                var secondSpName = "LIABILITY_RetrieveByLoanIdBrokerId";

                var pram1 = new SqlParameter("@LoanId", loanId);
                pram1.SqlDbType = SqlDbType.UniqueIdentifier;

                var pram2 = new SqlParameter("@BrokerId", principal.BrokerId);
                pram2.SqlDbType = SqlDbType.UniqueIdentifier;

                var pram3 = new SqlParameter("@LoanId", loanId);
                pram1.SqlDbType = SqlDbType.UniqueIdentifier;

                var pram4 = new SqlParameter("@BrokerId", principal.BrokerId);
                pram2.SqlDbType = SqlDbType.UniqueIdentifier;

                var aggregator = new StoredProcedureCallSet();
                aggregator.Add(StoredProcedureName.Create(firstSpName).Value, new SqlParameter[] { pram1, pram2 });
                aggregator.Add(StoredProcedureName.Create(secondSpName).Value, new SqlParameter[] { pram3, pram4 });

                SQLQueryString query;
                IEnumerable<DbParameter> allPrams;
                aggregator.AggregateCalls(out query, out allPrams);

                using (var connection = DbAccessUtils.GetConnection(principal.BrokerId, false))
                {
                    var ds = new DataSet();
                    SqlServerHelper.FillDataSet(connection, null, ds, query, allPrams.Select(p => (SqlParameter)p), TimeoutInSeconds.Default);
                    Assert.AreEqual(2, ds.Tables.Count);
                    Assert.AreEqual(1, ds.Tables[0].Rows.Count);
                    Assert.AreEqual(1, ds.Tables[1].Rows.Count);
                }
            }
        }

        [Test]
        [Category(CategoryConstants.IntegrationTest)]
        public void RealCallsWithoutAtPrefix_Aggregate_ReturnsDataSet()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDrivers(FoolHelper.DriverType.SqlQuery, FoolHelper.DriverType.StoredProcedure);

                var principal = LoginTools.LoginAs(TestAccountType.LoTest001);
                var creator = CLoanFileCreator.GetCreator(
                    principal,
                    LendersOffice.Audit.E_LoanCreationSource.UserCreateFromBlank);

                var loanId = creator.CreateBlankUladLoanFile();

                var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                    loanId,
                    typeof(StoredProcedureCallSetTest));
                loan.InitSave(ConstAppDavid.SkipVersionCheck);
                var appData = loan.GetAppData(0);

                var addCommand = new AddEntity(DataPath.Create("loan"), nameof(CPageBase.Assets), appData.aBConsumerId, null, null, null);
                loan.HandleAdd(addCommand, null);

                addCommand = new AddEntity(DataPath.Create("loan"), nameof(CPageBase.Liabilities), appData.aBConsumerId, null, null, null);
                loan.HandleAdd(addCommand, null);

                loan.Save();

                var firstSpName = "ASSET_RetrieveByLoanIdBrokerId";
                var secondSpName = "LIABILITY_RetrieveByLoanIdBrokerId";

                var pram1 = new SqlParameter("LoanId", loanId);
                pram1.SqlDbType = SqlDbType.UniqueIdentifier;

                var pram2 = new SqlParameter("BrokerId", principal.BrokerId);
                pram2.SqlDbType = SqlDbType.UniqueIdentifier;

                var pram3 = new SqlParameter("LoanId", loanId);
                pram1.SqlDbType = SqlDbType.UniqueIdentifier;

                var pram4 = new SqlParameter("BrokerId", principal.BrokerId);
                pram2.SqlDbType = SqlDbType.UniqueIdentifier;

                var aggregator = new StoredProcedureCallSet();
                aggregator.Add(StoredProcedureName.Create(firstSpName).Value, new SqlParameter[] { pram1, pram2 });
                aggregator.Add(StoredProcedureName.Create(secondSpName).Value, new SqlParameter[] { pram3, pram4 });

                SQLQueryString query;
                IEnumerable<DbParameter> allPrams;
                aggregator.AggregateCalls(out query, out allPrams);

                using (var connection = DbAccessUtils.GetConnection(principal.BrokerId, false))
                {
                    var ds = new DataSet();
                    SqlServerHelper.FillDataSet(connection, null, ds, query, allPrams.Select(p => (SqlParameter)p), TimeoutInSeconds.Default);
                    Assert.AreEqual(2, ds.Tables.Count);
                    Assert.AreEqual(1, ds.Tables[0].Rows.Count);
                    Assert.AreEqual(1, ds.Tables[1].Rows.Count);
                }
            }
        }
    }
}
