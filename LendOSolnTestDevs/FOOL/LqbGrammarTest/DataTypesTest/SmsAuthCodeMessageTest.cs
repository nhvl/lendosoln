﻿namespace LendingQB.Test.Developers.FOOL.LqbGrammarTest.DataTypesTest
{
    using LqbGrammar.DataTypes;
    using NUnit.Framework;

    [TestFixture]
    public sealed class SmsAuthCodeMessageTest
    {
        [Test]
        [Category(CategoryConstants.UnitTest)]
        public void TestLendingQB()
        {
            string source = "LendingQB";
            string code = "1234";

            var message = SmsAuthCodeMessage.Create(source, code);
            Assert.IsTrue(message != null);
        }

        [Test]
        [Category(CategoryConstants.UnitTest)]
        public void TestBlank()
        {
            string source = "";
            string code = "1234";

            var message = SmsAuthCodeMessage.Create(source, code);
            Assert.IsTrue(message != null);
        }

        [Test]
        [Category(CategoryConstants.UnitTest)]
        public void TestBrokerName()
        {
            string source = "SFMC, LP";
            string code = "1234";

            var message = SmsAuthCodeMessage.Create(source, code);
            Assert.IsTrue(message != null);
        }

        [Test]
        [Category(CategoryConstants.UnitTest)]
        public void TestScript()
        {
            string source = "<script></script>";
            string code = "1234";

            var message = SmsAuthCodeMessage.Create(source, code);
            Assert.IsTrue(message == null);
        }

        [Test]
        [Category(CategoryConstants.UnitTest)]
        public void TestBadAuthCode()
        {
            string source = "LendingQB";
            string code = "0123";

            var message = SmsAuthCodeMessage.Create(source, code);
            Assert.IsTrue(message == null);
        }

        [Test]
        [Category(CategoryConstants.UnitTest)]
        public void TestTooLongAuthCode()
        {
            string source = "LendingQB";
            string code = "12345";

            var message = SmsAuthCodeMessage.Create(source, code);
            Assert.IsTrue(message == null);
        }
    }
}
