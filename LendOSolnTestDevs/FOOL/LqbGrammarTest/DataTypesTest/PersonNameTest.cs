﻿namespace LendingQB.Test.Developers.FOOL.LqbGrammarTest.DataTypesTest
{
    using System.Diagnostics;
    using LqbGrammar.DataTypes;
    using NUnit.Framework;

    [TestFixture]
    public sealed class PersonNameTest
    {
        [Test]
        public void ShortValuesTest()
        {
            var name = "Some Gal";
            Test(name, expectValid: true);
        }

        [Test]
        public void LongValuesTest()
        {
            var names = new System.Collections.Generic.List<string>()
            {
                "Longnamemanmcman Hisnameisreallylonga",
                "Abcdefghijklmnop Qrstuvwxyzabcdefghij",
            };
            names.Add(string.Format("{0} {0}", new string('a', 20)));

            foreach (var name in names)
            {
                Test(name, expectValid: true);
            }
        }

        [Test]
        public void InvalidValuesTest()
        {
            var names = new System.Collections.Generic.List<string>()
            {
                "asd^a fdsaa",
                "Abcdefgh^jklmnop Qrstuvwxy^abcdefghij",
            };

            foreach (var name in names)
            {
                Test(name, expectValid:false);
            }
        }

        /// <summary>
        /// Tests performance and that the name is valid or not as expected.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="expectValid">True</param>
        private void Test(string name, bool expectValid)
        {
            var sw = new Stopwatch();
            sw.Start();
            var pn = PersonName.CreateWithValidation(name);
            sw.Stop();

            if(expectValid)
            {
                Assert.That(pn != null && pn != PersonName.BadName, name + " was somehow invalid.");
            }
            else
            {
                Assert.That(pn == null, name + " was somehow valid.");
            }

            Assert.That(sw.ElapsedMilliseconds < 2000, "it took more than 2 seconds to create a person with name " + name);
        }
    }
}
