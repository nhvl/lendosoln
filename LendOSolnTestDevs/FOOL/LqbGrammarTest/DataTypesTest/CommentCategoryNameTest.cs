﻿using LqbGrammar.DataTypes;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LendingQB.Test.Developers.FOOL.LqbGrammarTest.DataTypesTest
{
    [TestFixture]
    public class CommentCategoryNameTest
    {
        [Test]
        public void CreateValidsTest()
        {
            var valids = new string[]
            {
                "asdf",
                ".  2as  d f #", // spaces ok.
                "A", // single character ok.
                "1234567890123456789012345", // 25 chars ok.
                "-_./aZ5,;!@#$%^&*() :" // these are ok too.
            };

            foreach (var s in valids)
            {
                Test(s, true);
            }
        }

        [Test]
        public void CreateInvalidsTest()
        {
            var invalids = new string[]
            {
                string.Empty,
                " asdf", // no leading spaces
                "ASDF ", // no trailing spaces
                " ", // no leading/trailing spaces
                "12345678901234567890123456", // can't exceed 25 chars.
                "a<a", // no <
                "a>a", // no >
            };

            foreach (var s in invalids)
            {
                Test(s, false);
            }
        }

        private void Test(string testName, bool isValid)
        {
            var c = CommentCategoryName.Create(testName);

            if (isValid)
            {
                Assert.That(c != null, $"'{testName}' should have been a valid category name but was not.");
            }
            else
            {
                Assert.That(c == null, $"'{testName}' should not be a valid category name but is.");
            }
        }
    }
}
