﻿namespace LendingQB.Test.Developers.FOOL.LqbGrammarTest.DataTypesTest
{
    using System;
    using LqbGrammar.DataTypes;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public sealed class ZonedTimeTest
    {
        [Test]
        public void CreateWithTimespan()
        {
            var now = DateTime.Now;
            var today = DateTime.Today;
            var span = now - today;
            now = new DateTime(now.Ticks, DateTimeKind.Unspecified); // needed to make the eastOffset below

            // Create our version of the time in the Eastern timezone
            var eastTime = ZonedTime.Create(span, LqbTimeZone.EasternTime);
            Assert.IsTrue(eastTime != null);

            // Create the .net version of the time in the Eastern timezone
            var eastTZ = (TimeZoneInfo)LqbTimeZone.EasternTime;
            var offset = eastTZ.BaseUtcOffset;
            if (eastTZ.IsDaylightSavingTime(now))
            {
                var hour = new TimeSpan(1, 0, 0);
                offset = offset.Add(hour);
            }

            var eastOffset = new DateTimeOffset(now, offset);

            // Gather our calculated DateTimeOffset
            var calculated = eastTime.Value.TimeForToday;

            // Confirm ours matches the .net version
            Assert.IsTrue(eastOffset == calculated);
        }

        [Test]
        public void CreateWithString()
        {
            var now = DateTime.Now;
            var today = DateTime.Today;
            var span = now - today;

            var time = ZonedTime.Create(span, LqbTimeZone.PacificTime);
            string serialized = time.ToString();

            var recovered = ZonedTime.Create(serialized);
            Assert.IsTrue(recovered != null);
            var calculated = recovered.Value.TimeForToday;
            Assert.IsTrue(now == calculated.DateTime);
        }
    }
}
