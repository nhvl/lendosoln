﻿namespace LendingQB.Test.Developers.FOOL.LqbGrammarTest.DataTypesTest
{
    using LqbGrammar.DataTypes;
    using NUnit.Framework;
    using Utils;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public sealed class MoneyTest
    {
        const decimal OddEndsWithFour = 1.374m;
        const decimal OddEndsWithFive = 1.375m;
        const decimal OddEndsWithSix = 1.376m;

        const decimal EvenEndsWithFour = 1.384m;
        const decimal EvenEndsWithFive = 1.385m;
        const decimal EvenEndsWithSix = 1.386m;

        [Test]
        public void RoundingTest()
        {
            var odd4 = Money.Create(OddEndsWithFour).Value;
            var odd5 = Money.Create(OddEndsWithFive).Value;
            var odd6 = Money.Create(OddEndsWithSix).Value;
            var even4 = Money.Create(EvenEndsWithFour).Value;
            var even5 = Money.Create(EvenEndsWithFive).Value;
            var even6 = Money.Create(EvenEndsWithSix).Value;

            Assert.IsTrue(odd4 == 1.37m);
            Assert.IsTrue(odd5 == 1.38m);
            Assert.IsTrue(odd6 == 1.38m);

            Assert.IsTrue(even4 == 1.38m);
            Assert.IsTrue(even5 == 1.39m);
            Assert.IsTrue(even6 == 1.39m);

            odd4 = -odd4;
            odd5 = -odd5;
            odd6 = -odd6;
            even4 = -even4;
            even5 = -even5;
            even6 = -even6;

            Assert.IsTrue(odd4 == -1.37m);
            Assert.IsTrue(odd5 == -1.38m);
            Assert.IsTrue(odd6 == -1.38m);

            Assert.IsTrue(even4 == -1.38m);
            Assert.IsTrue(even5 == -1.39m);
            Assert.IsTrue(even6 == -1.39m);
        }

        [Test]
        public void AdditionTest()
        {
            var odd4 = Money.Create(OddEndsWithFour).Value;
            var odd5 = Money.Create(OddEndsWithFive).Value;
            var odd6 = Money.Create(OddEndsWithSix).Value;

            var total = odd4 + odd5 + odd6;
            Assert.AreEqual(3 * 1.38m - 0.01m, (decimal)total);
        }

        [Test]
        public void SubtractionTest()
        {
            var even4 = Money.Create(EvenEndsWithFour).Value;
            var even5 = Money.Create(EvenEndsWithFive).Value;
            var even6 = Money.Create(EvenEndsWithSix).Value;

            var total = even6 - even5 - even4;
            Assert.AreEqual(-1.38m, (decimal)total);

            even4 = -even4;
            even5 = -even5;
            even6 = -even6;

            total = even6 - even5 - even4;
            Assert.AreEqual(1.38m, (decimal)total);
        }
    }
}
