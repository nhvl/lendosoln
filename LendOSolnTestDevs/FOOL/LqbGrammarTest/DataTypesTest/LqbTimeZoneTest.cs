﻿namespace LendingQB.Test.Developers.FOOL.LqbGrammarTest.DataTypesTest
{
    using LqbGrammar.DataTypes;
    using LqbGrammar.Enumerations;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public class LqbTimeZoneTest
    {
        [Test]
        public void ConstructorArgOrder()
        {
            var pacific = LqbTimeZone.PacificTime;

            Assert.AreEqual("Pacific Time", pacific.GeneralName);
            Assert.AreEqual("PT", pacific.GeneralAbbreviation);
            Assert.AreEqual("PST", pacific.StandardAbbreviation);
            Assert.AreEqual("PDT", pacific.DaylightAbbreviation);
        }

        [Test]
        public void RetrieveFromEnumeration()
        {
            var enumVal = E_sTimeZoneT.AlaskaTime;
            var timezone = LqbTimeZone.Create(enumVal);
            Assert.IsTrue(object.ReferenceEquals(timezone, LqbTimeZone.AlaskaTime));

            enumVal = E_sTimeZoneT.CentralTime;
            timezone = LqbTimeZone.Create(enumVal);
            Assert.IsTrue(object.ReferenceEquals(timezone, LqbTimeZone.CentralTime));

            enumVal = E_sTimeZoneT.EasternTime;
            timezone = LqbTimeZone.Create(enumVal);
            Assert.IsTrue(object.ReferenceEquals(timezone, LqbTimeZone.EasternTime));

            enumVal = E_sTimeZoneT.HawaiiTime;
            timezone = LqbTimeZone.Create(enumVal);
            Assert.IsTrue(object.ReferenceEquals(timezone, LqbTimeZone.HawaiiTime));

            enumVal = E_sTimeZoneT.MountainTime;
            timezone = LqbTimeZone.Create(enumVal);
            Assert.IsTrue(object.ReferenceEquals(timezone, LqbTimeZone.MountainTime));

            enumVal = E_sTimeZoneT.PacificTime;
            timezone = LqbTimeZone.Create(enumVal);
            Assert.IsTrue(object.ReferenceEquals(timezone, LqbTimeZone.PacificTime));
        }

        [Test]
        public void RetrieveFromAbbreviation()
        {
            var abbrev = LqbTimeZone.AlaskaTime.GeneralAbbreviation;
            var timezone = LqbTimeZone.Create(abbrev);
            Assert.IsTrue(object.ReferenceEquals(timezone, LqbTimeZone.AlaskaTime));

            abbrev = LqbTimeZone.CentralTime.GeneralAbbreviation;
            timezone = LqbTimeZone.Create(abbrev);
            Assert.IsTrue(object.ReferenceEquals(timezone, LqbTimeZone.CentralTime));

            abbrev = LqbTimeZone.EasternTime.GeneralAbbreviation;
            timezone = LqbTimeZone.Create(abbrev);
            Assert.IsTrue(object.ReferenceEquals(timezone, LqbTimeZone.EasternTime));

            abbrev = LqbTimeZone.HawaiiTime.GeneralAbbreviation;
            timezone = LqbTimeZone.Create(abbrev);
            Assert.IsTrue(object.ReferenceEquals(timezone, LqbTimeZone.HawaiiTime));

            abbrev = LqbTimeZone.MountainTime.GeneralAbbreviation;
            timezone = LqbTimeZone.Create(abbrev);
            Assert.IsTrue(object.ReferenceEquals(timezone, LqbTimeZone.MountainTime));

            abbrev = LqbTimeZone.PacificTime.GeneralAbbreviation;
            timezone = LqbTimeZone.Create(abbrev);
            Assert.IsTrue(object.ReferenceEquals(timezone, LqbTimeZone.PacificTime));
        }
    }
}
