﻿namespace LendingQB.Test.Developers.FOOL.LqbGrammarTest.DataTypesTest
{
    using System;
    using LqbGrammar.DataTypes;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public sealed class YearTest
    {
        [Test]
        public void CreateFromInteger()
        {
            int thisYear = DateTime.Now.Year;

            var year = Year.Create(thisYear);

            Assert.IsTrue(year != null);
            Assert.AreEqual(thisYear, year.Value.Value);
        }

        [Test]
        public void CreateFromString()
        {
            int thisYear = DateTime.Now.Year;

            var year = Year.Create(thisYear);
            string serialized = year.Value.ToString();

            var recovered = Year.Create(serialized);
            Assert.IsTrue(recovered != null);
            Assert.AreEqual(thisYear, recovered.Value.Value);
        }

        [Test]
        public void EqualityTests()
        {
            int thisYear = DateTime.Now.Year;
            int nextYear = thisYear + 1;

            var year = Year.Create(thisYear).Value;
            var yearSame = Year.Create(thisYear).Value;
            var next = Year.Create(nextYear).Value;

            Assert.IsTrue(year == yearSame);
            Assert.IsFalse(year == next);

            Assert.IsFalse(year != yearSame);
            Assert.IsTrue(year != next);
        }

        [Test]
        public void ComparisonTests()
        {
            int thisYear = DateTime.Now.Year;
            int nextYear = thisYear + 1;

            var year = Year.Create(thisYear).Value;
            var yearSame = Year.Create(thisYear).Value;
            var next = Year.Create(nextYear).Value;

            Assert.IsFalse(year < yearSame);
            Assert.IsTrue(year < next);
            Assert.IsFalse(next < year);

            Assert.IsTrue(year <= yearSame);
            Assert.IsTrue(year <= next);
            Assert.IsFalse(next <= year);

            Assert.IsFalse(year > yearSame);
            Assert.IsFalse(year > next);
            Assert.IsTrue(next > year);

            Assert.IsTrue(year >= yearSame);
            Assert.IsFalse(year >= next);
            Assert.IsTrue(next >= year);
        }
    }
}
