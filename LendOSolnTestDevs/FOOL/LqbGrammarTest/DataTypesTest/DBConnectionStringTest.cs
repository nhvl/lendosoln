﻿namespace LendingQB.Test.Developers.FOOL.LqbGrammarTest.DataTypesTest
{
    using System.Data.SqlClient;
    using LqbGrammar.DataTypes;
    using NUnit.Framework;

    [TestFixture]
    public class DBConnectionStringTest
    {
        [Test]
        public void CreateValidValues()
        {
            var standardConnectionString = DBConnectionString.Create("Data Source=DataSourceTest;Initial Catalog=InitialCatalogTest;User ID=UserIDTest;Password=PasswordTest;Encrypt=False");
            Assert.IsTrue(standardConnectionString.HasValue);

            var roundTripVerificationBuilder = new SqlConnectionStringBuilder(standardConnectionString.Value.Value);
            Assert.AreEqual("DataSourceTest", roundTripVerificationBuilder.DataSource);
            Assert.AreEqual("InitialCatalogTest", roundTripVerificationBuilder.InitialCatalog);
            Assert.AreEqual("UserIDTest", roundTripVerificationBuilder.UserID);
            Assert.AreEqual("PasswordTest", roundTripVerificationBuilder.Password);
            Assert.AreEqual(false, roundTripVerificationBuilder.Encrypt);
        }

        [Test]
        public void CreateInvalidValues()
        {
            var emptyConnectionString = DBConnectionString.Create("");
            Assert.IsFalse(emptyConnectionString.HasValue);

            var invalidFieldNameConnectionString = DBConnectionString.Create("Data Source=DataSourceTest;Initial Catalog=InitialCatalogTest;User ID=UserIDTest;Password=PasswordTest;Encrypt=False;InvalidField=InvalidValue");
            Assert.IsFalse(invalidFieldNameConnectionString.HasValue);

            var invalidFieldValueConnectionString = DBConnectionString.Create("Data Source=DataSourceTest;Initial Catalog=InitialCatalogTest;User ID=UserIDTest;Password=PasswordTest;Encrypt=InvalidValue");
            Assert.IsFalse(invalidFieldValueConnectionString.HasValue);
        }
    }
}