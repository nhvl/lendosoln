﻿namespace LendingQB.Test.Developers.FOOL.LqbGrammarTest.DataTypesTest
{
    using LqbGrammar.DataTypes;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public class ApproximateDateTest
    {
        [Test]
        public void CreateDayPrecisionApproximateDates()
        {
            var dayPrecisionDate = ApproximateDate.CreateWithDayPrecision(2018, 10, 26);

            Assert.IsNotNull(dayPrecisionDate);
            Assert.AreEqual(UnzonedDate.Create(2018, 10, 26).Value, dayPrecisionDate.Value.MinimumDate(), "Minimum date is wrong");
            Assert.AreEqual(UnzonedDate.Create(2018, 10, 26).Value, dayPrecisionDate.Value.MiddleDate(), "Middle date is wrong");
            Assert.AreEqual(UnzonedDate.Create(2018, 10, 26).Value, dayPrecisionDate.Value.MaximumDate(), "Maximum date is wrong");
        }

        [Test]
        public void CreateMonthPrecisionApproximateDates()
        {
            var monthPrecisionDate = ApproximateDate.CreateWithMonthPrecision(2018, 10);

            Assert.IsNotNull(monthPrecisionDate);
            Assert.AreEqual(UnzonedDate.Create(2018, 10, 1).Value, monthPrecisionDate.Value.MinimumDate(), "Minimum date is wrong");
            Assert.AreEqual(UnzonedDate.Create(2018, 10, 16).Value, monthPrecisionDate.Value.MiddleDate(), "Middle date is wrong");
            Assert.AreEqual(UnzonedDate.Create(2018, 10, 31).Value, monthPrecisionDate.Value.MaximumDate(), "Maximum date is wrong");

            monthPrecisionDate = ApproximateDate.CreateWithMonthPrecision(2016, 2);

            Assert.IsNotNull(monthPrecisionDate);
            Assert.AreEqual(UnzonedDate.Create(2016, 2, 1).Value, monthPrecisionDate.Value.MinimumDate(), "Minimum date is wrong");
            Assert.AreEqual(UnzonedDate.Create(2016, 2, 16).Value, monthPrecisionDate.Value.MiddleDate(), "Middle date is wrong");
            Assert.AreEqual(UnzonedDate.Create(2016, 2, 29).Value, monthPrecisionDate.Value.MaximumDate(), "Maximum date is wrong");
        }

        [Test]
        public void CreateYearPrecisionApproximateDates()
        {
            var yearPrecisionDate = ApproximateDate.CreateWithYearPrecision(2112);

            Assert.IsNotNull(yearPrecisionDate);
            Assert.AreEqual(UnzonedDate.Create(2112, 1, 1).Value, yearPrecisionDate.Value.MinimumDate(), "Minimum date is wrong");
            Assert.AreEqual(UnzonedDate.Create(2112, 7, 1).Value, yearPrecisionDate.Value.MiddleDate(), "Middle date is wrong");
            Assert.AreEqual(UnzonedDate.Create(2112, 12, 31).Value, yearPrecisionDate.Value.MaximumDate(), "Maximum date is wrong");
        }

        [TestCase(2018, 1, 1, "General", true)]
        [TestCase(2018, 1, 0, "General", true)]
        [TestCase(2018, 0, 0, "General", true)]
        [TestCase(2018, 0, 1, "General", false)]
        [TestCase(2018, 1, 1, "Day", true)]
        [TestCase(2018, 1, 0, "Day", false)]
        [TestCase(2018, 0, 0, "Day", false)]
        [TestCase(2018, 1, 0, "Month", true)]
        [TestCase(2018, 0, 0, "Month", false)]
        [TestCase(2018, 0, 0, "Year", true)]
        [TestCase(2018, -2, 1, "General", false)]
        [TestCase(2018, 1, -1, "General", false)]
        [TestCase(2018, -1, 0, "Month", false)]
        public void TestValidation(int year, int month, int day, string createType, bool isValid)
        {
            ApproximateDate? testDate;

            switch(createType)
            {
                case "General":
                    testDate = ApproximateDate.Create(year, month, day);
                    break;
                case "Day":
                    testDate = ApproximateDate.CreateWithDayPrecision(year, month, day);
                    break;
                case "Month":
                    testDate = ApproximateDate.CreateWithMonthPrecision(year, month);
                    break;
                case "Year":
                    testDate = ApproximateDate.CreateWithYearPrecision(year);
                    break;
                default:
                    Assert.Fail($"Unhandled ApproximateDate creation type {createType}");
                    return;
            }

            Assert.AreEqual(isValid, testDate.HasValue);
        }

        [TestCase(2018, 10, 26, 2018, 12, 26, 2, true)]
        [TestCase(2018, 10, 26, 2018, 12, 26, 3, false)]
        [TestCase(2018,  9, 27, 2018, 12, 26, 3, true)]
        [TestCase(2018,  9,  1, 2018, 12, 17, 3, true)]
        [TestCase(2018,  8, 31, 2018, 12, 17, 3, false)]
        [TestCase(2018,  9,  1, 2018, 12, 18, 3, false)]
        [TestCase(2018,  9, 27, 2018, 12,  0, 3, true)]
        [TestCase(2018,  9, 27, 2018, 11,  0, 3, false)]
        [TestCase(2018,  0,  0, 2018, 11,  0, 3, true)]
        [TestCase(2018,  0,  0, 2019,  0,  0, 3, true)]
        [TestCase(2017,  0,  0, 2019,  0,  0, 3, false)]
        public void TestConsistentTimeSpan(int startYear, int startMonth, int startDay, int endYear, int endMonth, int endDay, int monthSpan, bool isValid)
        {
            ApproximateDate startDate = ApproximateDate.Create(startYear, startMonth, startDay).Value;
            ApproximateDate endDate = ApproximateDate.Create(endYear, endMonth, endDay).Value;
            Count<UnitType.Month> span = Count<UnitType.Month>.Create(monthSpan).Value;

            Assert.AreEqual(isValid, ApproximateDate.IsConsistentTimeSpan(startDate, span, endDate));
        }

        [TestCase("6-1-2017", 2017, 6, 1)]
        [TestCase("6-2017", 2017, 6, 0)]
        [TestCase("2017", 2017, 0, 0)]
        [TestCase("", 0, 0, 0)]
        [TestCase("banana", 0, 0, 0)]
        [TestCase("2017-06-01", 0, 0, 0)]
        [TestCase("20170601", 0, 0, 0)]
        [TestCase("01-01-2001", 2001, 1, 1)]
        [TestCase("001-01-2001", 0, 0, 0)]
        [TestCase("06.1.17", 2017, 6, 1)]
        [TestCase("3.14", 2014, 3, 0)]
        [TestCase("3.1415926", 0, 0, 0)]
        [TestCase("22/7", 0, 0, 0)]
        [TestCase("12/2073", 2073, 12, 0)]
        [TestCase("2/30/2030", 0, 0, 0)]
        public void TestFriendlyStringParsing(string testString, int expectedYear, int expectedMonth, int expectedDay)
        {
            var actual = ApproximateDate.Create(testString);

            if (!actual.HasValue)
            {
                Assert.AreEqual(expectedYear, 0);
                Assert.AreEqual(expectedMonth, 0);
                Assert.AreEqual(expectedDay, 0);
            }
            else
            {
                var expected = ApproximateDate.Create(expectedYear, expectedMonth, expectedDay);

                Assert.AreEqual(expected, actual);
            }
        }

        [TestCase("2017-06-01|D", 2017, 6, 1)]
        [TestCase("2017-06-01|M", 2017, 6, 0)]
        [TestCase("2017-06-01|Y", 2017, 0, 0)]
        [TestCase("", 0, 0, 0)]
        [TestCase("banana", 0, 0, 0)]
        [TestCase("20170601", 0, 0, 0)]
        [TestCase("2001-01-01|D", 2001, 1, 1)]
        [TestCase("001-01-2001", 0, 0, 0)]
        [TestCase("06.1.17", 0, 0, 0)]
        [TestCase("3.14", 0, 0, 0)]
        [TestCase("3.1415926", 0, 0, 0)]
        [TestCase("22/7", 0, 0, 0)]
        [TestCase("12/2073", 0, 0, 0)]
        [TestCase("2/30/2030", 0, 0, 0)]
        public void TestDatabaseStringParsing(string testString, int expectedYear, int expectedMonth, int expectedDay)
        {
            var actual = ApproximateDate.CreateFromDatabaseValue(testString);

            if (!actual.HasValue)
            {
                Assert.AreEqual(expectedYear, 0);
                Assert.AreEqual(expectedMonth, 0);
                Assert.AreEqual(expectedDay, 0);
            }
            else
            {
                var expected = ApproximateDate.Create(expectedYear, expectedMonth, expectedDay);

                Assert.AreEqual(expected, actual);
            }
        }

        [TestCase("Day", 2017, 6, 15, "2017-06-15|D", "6/15/2017")]
        [TestCase("Month", 2017, 6, 0, "2017-06-01|M", "6/2017")]
        [TestCase("Year", 2017, 0, 0, "2017-01-01|Y", "2017")]
        public void TestStringOutputs(string precision, int year, int month, int day, string expectedDatabase, string expectedFriendly)
        {
            ApproximateDate date;
            if (precision == "Day")
            {
                date = ApproximateDate.CreateWithDayPrecision(year, month, day).Value;
            }
            else if (precision == "Month")
            {
                date = ApproximateDate.CreateWithMonthPrecision(year, month).Value;
            }
            else
            {
                date = ApproximateDate.CreateWithYearPrecision(year).Value;
            }

            Assert.AreEqual(expectedDatabase, date.ToString("D"));
            Assert.AreEqual(expectedDatabase, (string)date.Value);

            Assert.AreEqual(expectedFriendly, date.ToString("F"));
            Assert.AreEqual(expectedFriendly, date.ToString(string.Empty));
            Assert.AreEqual(expectedFriendly, date.ToString());
        }
    }
}
