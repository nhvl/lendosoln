﻿namespace LendingQB.Test.Developers.FOOL.LqbGrammarTest.DataTypesTest
{
    using LqbGrammar.DataTypes;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public class LqbMessageQueueTest
    {
        [Test]
        public void Create()
        {
            var messageQueue = new System.Messaging.MessageQueue();
            var queue = new LqbMessageQueue(messageQueue);
        }

        [Test]
        public void Dispose()
        {
            var messageQueue = new System.Messaging.MessageQueue();
            using (var queue = new LqbMessageQueue(messageQueue))
            {
                Assert.IsTrue(queue.Value == messageQueue);
            }
        }

        [Test]
        [Ignore("Passed, but exception halts execution so ignore from now on")]
        [ExpectedException(typeof(LqbGrammar.Exceptions.DeveloperException))]
        public void Unexpected()
        {
            var messageQueue = new System.Messaging.MessageQueue();

            LqbMessageQueue badRef = null;
            using (var queue = new LqbMessageQueue(messageQueue))
            {
                badRef = queue;
            }
            var badQueue = badRef.Value;
        }
    }
}
