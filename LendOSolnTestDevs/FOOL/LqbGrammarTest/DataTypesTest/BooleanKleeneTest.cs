﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LqbGrammar.DataTypes;
using NUnit.Framework;

namespace LendingQB.Test.Developers.FOOL.LqbGrammarTest.DataTypesTest
{
    [TestFixture]
    public class BooleanKleeneTest
    {
        [Test]
        public void AndOperatorTest()
        {
            Assert.AreEqual(true & true ? BooleanKleene.True : BooleanKleene.False, BooleanKleene.True & BooleanKleene.True);
            Assert.AreEqual(true & false ? BooleanKleene.True : BooleanKleene.False, BooleanKleene.True & BooleanKleene.False);
            Assert.AreEqual(false & false ? BooleanKleene.True : BooleanKleene.False, BooleanKleene.False & BooleanKleene.False);

            Assert.AreEqual(BooleanKleene.Indeterminant, BooleanKleene.True & BooleanKleene.Indeterminant);
            Assert.AreEqual(BooleanKleene.False, BooleanKleene.False & BooleanKleene.Indeterminant);
            Assert.AreEqual(BooleanKleene.Indeterminant, BooleanKleene.Indeterminant & BooleanKleene.Indeterminant);
        }

        [Test]
        public void OrOperatorTest()
        {
            Assert.AreEqual(true | true ? BooleanKleene.True : BooleanKleene.False, BooleanKleene.True | BooleanKleene.True);
            Assert.AreEqual(true | false ? BooleanKleene.True : BooleanKleene.False, BooleanKleene.True | BooleanKleene.False);
            Assert.AreEqual(false | false ? BooleanKleene.True : BooleanKleene.False, BooleanKleene.False | BooleanKleene.False);

            Assert.AreEqual(BooleanKleene.True, BooleanKleene.True | BooleanKleene.Indeterminant);
            Assert.AreEqual(BooleanKleene.Indeterminant, BooleanKleene.False | BooleanKleene.Indeterminant);
            Assert.AreEqual(BooleanKleene.Indeterminant, BooleanKleene.Indeterminant & BooleanKleene.Indeterminant);
        }

        [Test]
        public void XorOperatorTest()
        {
            Assert.AreEqual(true ^ true ? BooleanKleene.True : BooleanKleene.False, BooleanKleene.True ^ BooleanKleene.True);
            Assert.AreEqual(true ^ false ? BooleanKleene.True : BooleanKleene.False, BooleanKleene.True ^ BooleanKleene.False);
            Assert.AreEqual(false ^ false ? BooleanKleene.True : BooleanKleene.False, BooleanKleene.False ^ BooleanKleene.False);

            Assert.AreEqual(BooleanKleene.Indeterminant, BooleanKleene.True ^ BooleanKleene.Indeterminant);
            Assert.AreEqual(BooleanKleene.Indeterminant, BooleanKleene.False ^ BooleanKleene.Indeterminant);
            Assert.AreEqual(BooleanKleene.Indeterminant, BooleanKleene.Indeterminant ^ BooleanKleene.Indeterminant);
        }
    }
}
