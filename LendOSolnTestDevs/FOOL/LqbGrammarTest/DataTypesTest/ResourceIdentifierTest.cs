﻿namespace LendingQB.Test.Developers.FOOL.LqbGrammarTest.DataTypesTest
{
    using System;
    using LqbGrammar.DataTypes;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public sealed class ResourceIdentifierTest
    {
        [Test]
        public void GoodValues()
        {
            Guid guid = Guid.NewGuid();
            BrokerIdentifier brokerId = BrokerIdentifier.Create(guid.ToString()).Value;
            string loanNumber = "x77_34_q";

            var id = ResourceIdentifier.CreateForLoanFile(brokerId, loanNumber);
            Assert.IsTrue(id != null);
        }

        [Test]
        public void BlankValue()
        {
            Guid guid = Guid.NewGuid();
            BrokerIdentifier brokerId = BrokerIdentifier.Create(guid.ToString()).Value;
            string loanNumber = string.Empty;

            var id = ResourceIdentifier.CreateForLoanFile(brokerId, loanNumber);
            string result = id.Value.ToString();

            Assert.IsTrue(id != null);
            Assert.AreEqual(73, result.Length);
        }

        [Test]
        public void XssAttack()
        {
            Guid guid = Guid.NewGuid();
            BrokerIdentifier brokerId = BrokerIdentifier.Create(guid.ToString()).Value;
            string loanNumber = "<script> alert('test'); </script>";

            var id = ResourceIdentifier.CreateForLoanFile(brokerId, loanNumber);
            Assert.IsTrue(id == null);
        }
    }
}
