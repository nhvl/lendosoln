﻿namespace LendingQB.Test.Developers.FOOL.LqbGrammarTest.DataTypesTest
{
    using System;
    using LqbGrammar.DataTypes;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public sealed class ZonedDateTest
    {
        [Test]
        public void CreateWithData()
        {
            var todayDate = DateTime.Today;

            var today = ZonedDate.Create(todayDate.Year, todayDate.Month, todayDate.Day, LqbTimeZone.PacificTime);
            Assert.IsTrue(today != null);

            var offset = today.Value.Date;
            var local = offset.DateTime;
            Assert.IsTrue(todayDate == local);
        }

        [Test]
        public void CreateWithString()
        {
            var todayDate = DateTime.Today;

            var today = ZonedDate.Create(todayDate.Year, todayDate.Month, todayDate.Day, LqbTimeZone.PacificTime).Value;
            string serialized = today.ToString();

            var recovered = ZonedDate.Create(serialized);
            Assert.IsTrue(recovered != null);

            var offset = recovered.Value.Date;
            var local = offset.DateTime;
            Assert.IsTrue(todayDate == local);
        }

        [Test]
        public void EqualityTests()
        {
            var todayDate = DateTime.Today;
            var tomorrowDate = todayDate.AddDays(1);

            var today = ZonedDate.Create(todayDate.Year, todayDate.Month, todayDate.Day, LqbTimeZone.HawaiiTime).Value;
            var todaySame = ZonedDate.Create(todayDate.Year, todayDate.Month, todayDate.Day, LqbTimeZone.HawaiiTime).Value;
            var tomorrow = ZonedDate.Create(tomorrowDate.Year, tomorrowDate.Month, tomorrowDate.Day, LqbTimeZone.HawaiiTime).Value;

            Assert.IsTrue(today == todaySame);
            Assert.IsFalse(today == tomorrow);

            Assert.IsFalse(today != todaySame);
            Assert.IsTrue(today != tomorrow);
        }

        [Test]
        public void ComparisonTests()
        {
            var todayDate = DateTime.Today;
            var tomorrowDate = todayDate.AddDays(1);

            var today = ZonedDate.Create(todayDate.Year, todayDate.Month, todayDate.Day, LqbTimeZone.HawaiiTime).Value;
            var todaySame = ZonedDate.Create(todayDate.Year, todayDate.Month, todayDate.Day, LqbTimeZone.HawaiiTime).Value;
            var tomorrow = ZonedDate.Create(tomorrowDate.Year, tomorrowDate.Month, tomorrowDate.Day, LqbTimeZone.HawaiiTime).Value;

            Assert.IsFalse(today < todaySame);
            Assert.IsTrue(today < tomorrow);
            Assert.IsFalse(tomorrow < today);

            Assert.IsTrue(today <= todaySame);
            Assert.IsTrue(today <= tomorrow);
            Assert.IsFalse(tomorrow <= today);

            Assert.IsFalse(today > todaySame);
            Assert.IsFalse(today > tomorrow);
            Assert.IsTrue(tomorrow > today);

            Assert.IsTrue(today >= todaySame);
            Assert.IsFalse(today >= tomorrow);
            Assert.IsTrue(tomorrow >= today);
        }
    }
}
