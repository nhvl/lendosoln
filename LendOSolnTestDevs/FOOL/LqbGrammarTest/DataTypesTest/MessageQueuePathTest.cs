﻿namespace LendingQB.Test.Developers.FOOL.LqbGrammarTest.DataTypesTest
{
    using LqbGrammar.DataTypes;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public class MessageQueuePathTest
    {
        [Test]
        public void GoodPathOS()
        {
            string good = @"FormatName:DIRECT=OS:loextdevweb1.middleearth.com\private$\lodev_LendingQBLog";
            var path = MessageQueuePath.Create(good);
            Assert.IsTrue(path != null);
            Assert.AreEqual(good, path.Value.ToString());
        }

        [Test]
        public void GoodPathTCP()
        {
            string good = @"FormatName:DIRECT=TCP:127.0.0.1\private$\lodev_LendingQBLog";
            var path = MessageQueuePath.Create(good);
            Assert.IsTrue(path != null);
            Assert.AreEqual(good, path.Value.ToString());
        }

        [Test]
        public void BadPath()
        {
            string bad = "BAD_PATH";
            var path = MessageQueuePath.Create(bad);
            Assert.IsTrue(path == null);
        }
    }
}
