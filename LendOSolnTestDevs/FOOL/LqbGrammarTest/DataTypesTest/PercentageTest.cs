﻿namespace LendingQB.Test.Developers.FOOL.LqbGrammarTest.DataTypesTest
{
    using System;
    using LqbGrammar.DataTypes;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public sealed class PercentageTest
    {
        [Test]
        public void CreateFromDecimal()
        {
            decimal thisPercentage = 1.25m;

            var percent = Percentage.Create(thisPercentage);

            Assert.IsTrue(percent != null);
            Assert.AreEqual(thisPercentage, (decimal)percent.Value);
        }

        [Test]
        public void CreateFromString()
        {
            decimal thisPercentage = 1.25m;

            var percent = Percentage.Create(thisPercentage);
            string serialized = percent.Value.ToString();

            var recovered = Percentage.Create(serialized);
            Assert.IsTrue(recovered != null);
            Assert.AreEqual(thisPercentage, (decimal)recovered.Value);
        }

        [Test]
        public void EqualityTests()
        {
            decimal thisPercentage = 1.25m;
            decimal nextPercentage = thisPercentage + 1;

            var percent = Percentage.Create(thisPercentage).Value;
            var percentSame = Percentage.Create(thisPercentage).Value;
            var next = Percentage.Create(nextPercentage).Value;

            Assert.IsTrue(percent == percentSame);
            Assert.IsFalse(percent == next);

            Assert.IsFalse(percent != percentSame);
            Assert.IsTrue(percent != next);
        }

        [Test]
        public void ComparisonTests()
        {
            decimal thisPercentage = 1.25m;
            decimal nextPercentage = thisPercentage + 1;

            var percent = Percentage.Create(thisPercentage).Value;
            var percentSame = Percentage.Create(thisPercentage).Value;
            var next = Percentage.Create(nextPercentage).Value;

            Assert.IsFalse(percent < percentSame);
            Assert.IsTrue(percent < next);
            Assert.IsFalse(next < percent);

            Assert.IsTrue(percent <= percentSame);
            Assert.IsTrue(percent <= next);
            Assert.IsFalse(next <= percent);

            Assert.IsFalse(percent > percentSame);
            Assert.IsFalse(percent > next);
            Assert.IsTrue(next > percent);

            Assert.IsTrue(percent >= percentSame);
            Assert.IsFalse(percent >= next);
            Assert.IsTrue(next >= percent);
        }
    }
}