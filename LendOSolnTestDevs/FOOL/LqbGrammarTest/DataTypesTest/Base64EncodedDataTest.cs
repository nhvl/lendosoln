﻿namespace LendingQB.Test.Developers.FOOL.LqbGrammarTest.DataTypesTest
{
    using System;
    using System.Text;
    using LqbGrammar.DataTypes;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    class Base64EncodedDataTest
    {
        [Test]
        public void CreateValidValues()
        {
            var validBase64String = Convert.ToBase64String(Encoding.UTF8.GetBytes("Unit testing allows the programmer to refactor code or upgrade system libraries at a later date, and make sure the module still works correctly (e.g., in regression testing)."));
            var base64Value = Base64EncodedData.Create(validBase64String);
            Assert.IsTrue(base64Value.HasValue);
            Assert.AreEqual(validBase64String, base64Value.Value.Value);

            base64Value = Base64EncodedData.Create(string.Empty);
            Assert.IsTrue(base64Value.HasValue);
            Assert.AreEqual(string.Empty, base64Value.Value.Value);

            validBase64String = "5abc";
            base64Value = Base64EncodedData.Create(validBase64String);
            Assert.IsTrue(base64Value.HasValue);

            validBase64String = " 5abc";
            base64Value = Base64EncodedData.Create(validBase64String);
            Assert.IsTrue(base64Value.HasValue);

            validBase64String = "5abc ";
            base64Value = Base64EncodedData.Create(validBase64String);
            Assert.IsTrue(base64Value.HasValue);

            validBase64String = " 5abc ";
            base64Value = Base64EncodedData.Create(validBase64String);
            Assert.IsTrue(base64Value.HasValue);

            validBase64String = "5ab=";
            base64Value = Base64EncodedData.Create(validBase64String);
            Assert.IsTrue(base64Value.HasValue);

            validBase64String = "5a==";
            base64Value = Base64EncodedData.Create(validBase64String);
            Assert.IsTrue(base64Value.HasValue);
        }

        [Test]
        public void CreateInvalidValues()
        {
            string nullBase64 = null;
            var base64Value = Base64EncodedData.Create(nullBase64);
            Assert.IsFalse(base64Value.HasValue);

            var paddingOnlyBase64 = "=";
            base64Value = Base64EncodedData.Create(paddingOnlyBase64);
            Assert.IsFalse(base64Value.HasValue);

            paddingOnlyBase64 = "==";
            base64Value = Base64EncodedData.Create(paddingOnlyBase64);
            Assert.IsFalse(base64Value.HasValue);

            var invalidCharacterBase64 = "V)5pdA==";
            base64Value = Base64EncodedData.Create(invalidCharacterBase64);
            Assert.IsFalse(base64Value.HasValue);

            var invalidLengthBase64 = "W5pdCBUZXN0aW5n";
            base64Value = Base64EncodedData.Create(invalidLengthBase64);
            Assert.IsFalse(base64Value.HasValue);

            var invalidPaddingBase64 = "VGVzdGluZ===";
            base64Value = Base64EncodedData.Create(invalidPaddingBase64);
            Assert.IsFalse(base64Value.HasValue);

            var paddingCharInMiddle = "5a=b";
            base64Value = Base64EncodedData.Create(paddingCharInMiddle);
            Assert.IsFalse(base64Value.HasValue);

            paddingCharInMiddle = "5=ab";
            base64Value = Base64EncodedData.Create(paddingCharInMiddle);
            Assert.IsFalse(base64Value.HasValue);

            var paddingCharAtStart = "=5ab";
            base64Value = Base64EncodedData.Create(paddingCharAtStart);
            Assert.IsFalse(base64Value.HasValue);
        }
    }
}
