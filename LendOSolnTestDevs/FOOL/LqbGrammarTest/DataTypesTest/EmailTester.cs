﻿namespace LendingQB.Test.Developers.FOOL.LqbGrammarTest.DataTypesTest
{
    using LqbGrammar.DataTypes;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public sealed class EmailTester
    {
        [Test]
        public void SimpleEmail()
        {
            string email = "aland@meridianlink.com";
            var symantic = EmailAddress.Create(email);

            Assert.IsTrue(symantic != null);
            Assert.AreEqual(email, symantic.Value.ToString());
        }

        [Test]
        public void ComplexEmail()
        {
            string email = "\"Michael Leinweaver\" <michaell2@lendingqb.com>";
            string email2 = "\"TIFFANY MCINTOSH\"<TIFFANY.MCINTOSH@NFLP.COM>";
            string email3 = "\"Multi Spaces\"     <multi@spaces.com>";
            var symantic = EmailAddress.Create(email);
            var symantic2 = EmailAddress.Create(email2);
            var symantic3 = EmailAddress.Create(email3);

            Assert.IsTrue(symantic != null);
            Assert.IsTrue(symantic2 != null);
            Assert.IsTrue(symantic3 != null);

            Assert.AreEqual("michaell2@lendingqb.com", symantic.Value.ToString());
            Assert.AreEqual("Michael Leinweaver", symantic.Value.DisplayName);

            Assert.AreEqual("TIFFANY.MCINTOSH@NFLP.COM", symantic2.Value.ToString());
            Assert.AreEqual("TIFFANY MCINTOSH", symantic2.Value.DisplayName);

            Assert.AreEqual("multi@spaces.com", symantic3.Value.ToString());
            Assert.AreEqual("Multi Spaces", symantic3.Value.DisplayName);
        }

        [Test]
        public void StackTraceAsEmailSubject()
        {
            // Use a subject from a live email that the existing FOOL emailer could not handle.
            string subject = "[94] [LoException]-System.TypeInitializationException: The type initializer for 'LendersOffice.PdfForm.PdfForm' threw an exception. ---> System.Exception: LendersOffice.Pdf.C1003_10PDF ---> System.IO.DirectoryNotFoundException: Could not find a part of th";

            var test = EmailSubject.Create(subject);
            Assert.IsTrue(test != null);
        }

        [Test]
        public void XmlCommentAsEmailSubject()
        {
            string subject = "<!--------------- xml comment embedded in error message --------------------->";

            var test = EmailSubject.Create(subject);
            Assert.IsTrue(test != null);
        }
    }
}
