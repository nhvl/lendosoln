﻿namespace LendingQB.Test.Developers.FOOL.LqbGrammarTest.DataTypesTest
{
    using LqbGrammar.DataTypes.PathDispatch;
    using NUnit.Framework;

    [TestFixture]
    public class DataPathTest
    {
        [Test]
        public void SimplePathParsing()
        {
            DataPath testPath = DataPath.Create("LOAN.LOAN_TERMS.Term");

            Assert.AreEqual(3, testPath.Length);
            Assert.AreEqual("LOAN", testPath.Head.Name);
            testPath = testPath.Tail;
            Assert.AreEqual("LOAN_TERMS", testPath.Head.Name);
            testPath = testPath.Tail;
            Assert.AreEqual("Term", testPath.Head.Name);
            testPath = testPath.Tail;
            Assert.AreEqual(0, testPath.Length);
        }

        [Test]
        public void CollectionPathParsing()
        {
            DataPath testPath = DataPath.Create("LOAN.CONSUMERS[CONS0001].LastName");
            Assert.AreEqual("LOAN.CONSUMERS[CONS0001].LastName", testPath.ToString());

            Assert.AreEqual(4, testPath.Length);
            Assert.AreEqual("LOAN", testPath.Head.Name);

            testPath = testPath.Tail;
            Assert.AreEqual("CONSUMERS", testPath.Head.Name);
            Assert.IsInstanceOf<DataPathCollectionElement>(testPath.Head);
            DataPathCollectionElement consumerColl = (DataPathCollectionElement)testPath.Head;
            Assert.AreEqual("CONSUMERS", consumerColl.Name);

            testPath = testPath.Tail;
            Assert.AreEqual("CONS0001", testPath.Head.Name);
            Assert.IsInstanceOf<DataPathSelectionElement>(testPath.Head);
            DataPathSelectionElement consumerId = (DataPathSelectionElement)testPath.Head;
            Assert.AreEqual("CONS0001", consumerId.Name);
            Assert.AreEqual("[CONS0001]", consumerId.ToString());

            testPath = testPath.Tail;
            Assert.IsInstanceOf<DataPathBasicElement>(testPath.Head);
            Assert.AreEqual("LastName", testPath.Head.Name);

            testPath = testPath.Tail;
            Assert.AreEqual(0, testPath.Length);
        }
    }
}