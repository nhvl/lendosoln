﻿namespace LendingQB.Test.Developers.FOOL.LqbGrammarTest.DataTypesTest
{
    using System;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Enumerations;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public class InstantTest
    {
        [Test]
        public void LocalVsUtc()
        {
            var now = DateTime.Now;
            var utc = now.ToUniversalTime();

            var instantNow = Instant.Create(now).Value;
            var instantUtc = Instant.Create(utc).Value;

            Assert.IsTrue(instantNow == instantUtc);
        }

        [Test]
        public void CreateFromDateTime()
        {
            DateTime now = DateTime.Now;

            var instantNow = Instant.Create(now).Value;

            Assert.IsTrue(instantNow != null);
        }

        [Test]
        public void CreateFromDateTimeOffset()
        {
            DateTimeOffset now = DateTimeOffset.Now;

            var instantNow = Instant.Create(now).Value;

            Assert.IsTrue(instantNow != null);
        }

        [Test]
        public void CreateFromString()
        {
            var now = DateTime.Now;

            var instantNow = Instant.Create(now).Value;
            string serialized = instantNow.ToString();

            var recovered = Instant.Create(serialized).Value;
            Assert.IsTrue(instantNow == recovered); 
        }
    }
}
