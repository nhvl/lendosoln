﻿namespace LendingQB.Test.Developers.FOOL.LqbGrammarTest.DataTypesTest
{
    using LqbGrammar.DataTypes;
    using NUnit.Framework;
    using NUnit.Framework.Constraints;

    [TestFixture]
    [Category("UNIT")]
    public class MimeTypeTest
    {
        [Test]
        public void ApplicationCsv()
        {
            var data = MimeType.Application.Csv;
            Assert.AreEqual("application", data.MediaType.ToString());
            Assert.AreEqual("csv", data.SubMediaType.ToString());
            Assert.AreEqual("application/csv", data.ToString());
        }

        [Test]
        public void ApplicationDoc()
        {
            var data = MimeType.Application.Doc;
            Assert.AreEqual("application", data.MediaType.ToString());
            Assert.AreEqual("doc", data.SubMediaType.ToString());
            Assert.AreEqual("application/doc", data.ToString());
        }

        [Test]
        public void ApplicationDownload()
        {
            var data = MimeType.Application.Download;
            Assert.AreEqual("application", data.MediaType.ToString());
            Assert.AreEqual("download", data.SubMediaType.ToString());
            Assert.AreEqual("application/download", data.ToString());
        }

        [Test]
        public void ApplicationExcel()
        {
            var data = MimeType.Application.Excel;
            Assert.AreEqual("application", data.MediaType.ToString());
            Assert.AreEqual("vnd.ms-excel", data.SubMediaType.ToString());
            Assert.AreEqual("application/vnd.ms-excel", data.ToString());
        }

        [Test]
        public void ApplicationJson()
        {
            var data = MimeType.Application.Json;
            Assert.AreEqual("application", data.MediaType.ToString());
            Assert.AreEqual("json", data.SubMediaType.ToString());
            Assert.AreEqual("application/json", data.ToString());
        }

        [Test]
        public void ApplicationOctetStream()
        {
            var data = MimeType.Application.OctetStream;
            Assert.AreEqual("application", data.MediaType.ToString());
            Assert.AreEqual("octet-stream", data.SubMediaType.ToString());
            Assert.AreEqual("application/octet-stream", data.ToString());
        }

        [Test]
        public void ApplicationPdf()
        {
            var data = MimeType.Application.Pdf;
            Assert.AreEqual("application", data.MediaType.ToString());
            Assert.AreEqual("pdf", data.SubMediaType.ToString());
            Assert.AreEqual("application/pdf", data.ToString());
        }

        [Test]
        public void ApplicationSpreadsheetXml()
        {
            var data = MimeType.Application.SpreadsheetXml;
            Assert.AreEqual("application", data.MediaType.ToString());
            Assert.AreEqual("vnd.openxmlformats-officedocument.spreadsheetml.sheet", data.SubMediaType.ToString());
            Assert.AreEqual("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", data.ToString());
        }

        [Test]
        public void ApplicationText()
        {
            var data = MimeType.Application.Text;
            Assert.AreEqual("application", data.MediaType.ToString());
            Assert.AreEqual("text", data.SubMediaType.ToString());
            Assert.AreEqual("application/text", data.ToString());
        }

        [Test]
        public void ApplicationUrlEncoded()
        {
            var data = MimeType.Application.UrlEncoded;
            Assert.AreEqual("application", data.MediaType.ToString());
            Assert.AreEqual("x-www-form-urlencoded", data.SubMediaType.ToString());
            Assert.AreEqual("application/x-www-form-urlencoded", data.ToString());
        }

        [Test]
        public void ApplicationXhtml()
        {
            var data = MimeType.Application.Xhtml;
            Assert.AreEqual("application", data.MediaType.ToString());
            Assert.AreEqual("xhtml+xml", data.SubMediaType.ToString());
            Assert.AreEqual("application/xhtml+xml", data.ToString());
        }

        [Test]
        public void ApplicationXml()
        {
            var data = MimeType.Application.Xml;
            Assert.AreEqual("application", data.MediaType.ToString());
            Assert.AreEqual("xml", data.SubMediaType.ToString());
            Assert.AreEqual("application/xml", data.ToString());
        }

        [Test]
        public void ApplicationZip()
        {
            var data = MimeType.Application.Zip;
            Assert.AreEqual("application", data.MediaType.ToString());
            Assert.AreEqual("zip", data.SubMediaType.ToString());
            Assert.AreEqual("application/zip", data.ToString());
        }

        [Test]
        public void ImageGif()
        {
            var data = MimeType.Image.Gif;
            Assert.AreEqual("image", data.MediaType.ToString());
            Assert.AreEqual("gif", data.SubMediaType.ToString());
            Assert.AreEqual("image/gif", data.ToString());
        }

        [Test]
        public void ImageJpeg()
        {
            var data = MimeType.Image.Jpeg;
            Assert.AreEqual("image", data.MediaType.ToString());
            Assert.AreEqual("jpeg", data.SubMediaType.ToString());
            Assert.AreEqual("image/jpeg", data.ToString());
        }

        [Test]
        public void ImagePng()
        {
            var data = MimeType.Image.Png;
            Assert.AreEqual("image", data.MediaType.ToString());
            Assert.AreEqual("png", data.SubMediaType.ToString());
            Assert.AreEqual("image/png", data.ToString());
        }

        [Test]
        public void TextCommaSeparatedValues()
        {
            var data = MimeType.Text.CommaSeparatedValues;
            Assert.AreEqual("text", data.MediaType.ToString());
            Assert.AreEqual("comma-separated-values", data.SubMediaType.ToString());
            Assert.AreEqual("text/comma-separated-values", data.ToString());
        }

        [Test]
        public void TextCsv()
        {
            var data = MimeType.Text.Csv;
            Assert.AreEqual("text", data.MediaType.ToString());
            Assert.AreEqual("csv", data.SubMediaType.ToString());
            Assert.AreEqual("text/csv", data.ToString());
        }

        [Test]
        public void TextHtml()
        {
            var data = MimeType.Text.Html;
            Assert.AreEqual("text", data.MediaType.ToString());
            Assert.AreEqual("html", data.SubMediaType.ToString());
            Assert.AreEqual("text/html", data.ToString());
        }

        [Test]
        public void TextJavascript()
        {
            var data = MimeType.Text.Javascript;
            Assert.AreEqual("text", data.MediaType.ToString());
            Assert.AreEqual("javascript", data.SubMediaType.ToString());
            Assert.AreEqual("text/javascript", data.ToString());
        }

        [Test]
        public void TextPlain()
        {
            var data = MimeType.Text.Plain;
            Assert.AreEqual("text", data.MediaType.ToString());
            Assert.AreEqual("plain", data.SubMediaType.ToString());
            Assert.AreEqual("text/plain", data.ToString());
        }

        [Test]
        public void TextXml()
        {
            var data = MimeType.Text.Xml;
            Assert.AreEqual("text", data.MediaType.ToString());
            Assert.AreEqual("xml", data.SubMediaType.ToString());
            Assert.AreEqual("text/xml", data.ToString());
        }

        [Test]
        public void MultiPartFormData()
        {
            var data = MimeType.MultiPart.CreateFormData("this_is_the_boundary");
            Assert.AreEqual("multipart", data.MediaType.ToString());
            Assert.AreEqual("form-data", data.SubMediaType.ToString());
            Assert.AreEqual("multipart/form-data; boundary=\"this_is_the_boundary\"", data.ToString());
        }

        [Test]
        public void Wildcard()
        {
            var data = MimeType.WildCard;
            Assert.AreEqual("*", data.MediaType.ToString());
            Assert.AreEqual("*", data.SubMediaType.ToString());
            Assert.AreEqual("*/*", data.ToString());
        }
    }
}
