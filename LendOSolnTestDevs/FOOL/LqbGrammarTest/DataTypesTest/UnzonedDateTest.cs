﻿namespace LendingQB.Test.Developers.FOOL.LqbGrammarTest.DataTypesTest
{
    using System;
    using LqbGrammar.DataTypes;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public class UnzonedDateTest
    {
        [TestCase(2018, 2, 1, true)]
        [TestCase(2018, 2, 28, true)]
        [TestCase(2018, 2, 29, false)]
        [TestCase(2018, 4, 30, true)]
        [TestCase(2018, 4, 31, false)]
        [TestCase(2018, 5, 31, true)]
        [TestCase(2018, 5, 32, false)]
        [TestCase(2016, 2, 29, true)]
        [TestCase(2016, 2, 30, false)]
        [TestCase(2016, 1, 1, true)]
        [TestCase(2016, 1, 0, false)]
        [TestCase(2016, 0, 1, false)]
        [TestCase(2016, 1, -3, false)]
        [TestCase(2016, -5, 1, false)]
        public void CheckValidDates(int year, int month, int day, bool isValid)
        {
            UnzonedDate? maybeDate;

            maybeDate = UnzonedDate.Create(year, month, day);
            Assert.AreEqual(isValid, maybeDate.HasValue);
        }

        [Test]
        public void CreateWithValues()
        {
            const string name = "AJD";

            var date = this.Create(name);
            Assert.IsTrue(date != null);

            var full = this.CreateDatetime(name);
            Assert.IsTrue(full == date.Value.Date);
        }

        [Test]
        public void CreateWithDate()
        {
            const string name = "RAD";

            var full = this.CreateDatetime(name);
            var date = UnzonedDate.Create(full);

            Assert.IsTrue(date != null);
            Assert.IsTrue(full == date.Value.Date);
        }

        [Test]
        public void CreateWithString()
        {
            const string name = "RED";

            var full = this.CreateDatetime(name);
            var date = UnzonedDate.Create(full).Value;

            string serialized = date.ToString();
            Assert.AreEqual(UnzonedDate.FormatString.Length, serialized.Length);

            var recovered = UnzonedDate.Create(serialized);
            Assert.IsTrue(recovered != null);
            Assert.IsTrue(date.Date == recovered.Value.Date);
        }

        [Test]
        public void EqualityTests()
        {
            var ajd = this.Create("AJD").Value;
            var rad = this.Create("RAD").Value;
            var red = this.Create("RED").Value;

            var ajdCopy = ajd;
            var radCopy = rad;
            var redCopy = red;

            Assert.IsTrue(ajd == ajdCopy);
            Assert.IsTrue(rad.Equals(radCopy));
            Assert.IsTrue(red.Equals((object)redCopy));

            Assert.IsTrue(ajd != rad);
            Assert.IsTrue(ajd != red);
            Assert.IsTrue(rad != ajd);
            Assert.IsTrue(rad != red);
            Assert.IsTrue(red != ajd);
            Assert.IsTrue(red != rad);
        }

        [Test]
        public void InequalityTests()
        {
            var ajd = this.Create("AJD").Value;
            var rad = this.Create("RAD").Value;
            var red = this.Create("RED").Value;

            var ajdCopy = ajd;
            var radCopy = rad;
            var redCopy = red;

            Assert.IsFalse(rad < ajd);
            Assert.IsFalse(rad <= ajd);
            Assert.IsTrue(rad > ajd);
            Assert.IsTrue(rad >= ajd);

            Assert.IsFalse(rad < red);
            Assert.IsFalse(rad <= red);
            Assert.IsTrue(rad > red);
            Assert.IsTrue(rad >= red);

            Assert.IsFalse(rad < radCopy);
            Assert.IsTrue(rad <= radCopy);
            Assert.IsFalse(rad > radCopy);
            Assert.IsTrue(rad >= radCopy);

            Assert.IsTrue(ajd < rad);
            Assert.IsTrue(ajd <= rad);
            Assert.IsFalse(ajd > rad);
            Assert.IsFalse(ajd >= rad);

            Assert.IsFalse(ajd < red);
            Assert.IsFalse(ajd <= red);
            Assert.IsTrue(ajd > red);
            Assert.IsTrue(ajd >= red);

            Assert.IsFalse(ajd < ajdCopy);
            Assert.IsTrue(ajd <= ajdCopy);
            Assert.IsFalse(ajd > ajdCopy);
            Assert.IsTrue(ajd >= ajdCopy);

            Assert.IsTrue(red < rad);
            Assert.IsTrue(red <= rad);
            Assert.IsFalse(red > rad);
            Assert.IsFalse(red >= rad);

            Assert.IsTrue(red < ajd);
            Assert.IsTrue(red <= ajd);
            Assert.IsFalse(red > ajd);
            Assert.IsFalse(red >= ajd);

            Assert.IsFalse(red < redCopy);
            Assert.IsTrue(red <= redCopy);
            Assert.IsFalse(red > redCopy);
            Assert.IsTrue(red >= redCopy);
        }

        private UnzonedDate? Create(string name)
        {
            var tuple = this.GetDateInfo(name);
            return (tuple != null) ? UnzonedDate.Create(tuple.Item1, tuple.Item2, tuple.Item3) : null;
        }

        private DateTime CreateDatetime(string name)
        {
            var tuple = this.GetDateInfo(name);
            return (tuple != null) ? new DateTime(tuple.Item1, tuple.Item2, tuple.Item3) : DateTime.MinValue;
        }

        private Tuple<int, int, int> GetDateInfo(string name)
        {
            switch (name)
            {
                case "AJD":
                    {
                        int year = 2004;
                        int month = 1;
                        int day = 4;

                        return new Tuple<int, int, int>(year, month, day);
                    }
                case "RAD":
                    {
                        int year = 2005;
                        int month = 5;
                        int day = 14;

                        return new Tuple<int, int, int>(year, month, day);
                    }
                case "RED":
                    {
                        int year = 1996;
                        int month = 9;
                        int day = 26;

                        return new Tuple<int, int, int>(year, month, day);
                    }
                default:
                    return null;
            }
        }
    }
}
