﻿namespace LendingQB.Test.Developers.FOOL.LqbGrammarTest.DataTypesTest
{
    using System.Xml.Linq;
    using LqbGrammar.DataTypes;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public class LqbXmlElementTest
    {
        [Test]
        public void Null()
        {
            XElement nullElement = null;
            LqbXmlElement? test = LqbXmlElement.Create(nullElement);
            Assert.IsTrue(test == null);
        }

        [Test]
        public void ValidElement()
        {
            XDocument xdoc = new XDocument(new XElement(XName.Get("root")));
            LqbXmlElement? test = LqbXmlElement.Create(xdoc.Root);
            Assert.IsTrue(test != null);
            Assert.AreSame(xdoc.Root, test.Value.Contained);
        }
    }
}
