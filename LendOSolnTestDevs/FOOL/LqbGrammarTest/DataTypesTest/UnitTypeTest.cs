﻿namespace LendingQB.Test.Developers.FOOL.LqbGrammarTest.DataTypesTest
{
    using LqbGrammar.DataTypes;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public class UnitTypeTest
    {
        [Test]
        public void GetUnitNames()
        {
            this.CheckNames(UnitType.Dollar.Instance, "dollar", "dollars");
            this.CheckNames(UnitType.Day.Instance, "day", "days");
            this.CheckNames(UnitType.Month.Instance, "month", "months");
            this.CheckNames(UnitType.Year.Instance, "year", "years");
        }

        [Test]
        public void ConversionTest()
        {
            var quantity = Quantity<UnitType.Day>.Create(3m).Value;

            var hour = UnitConvert.Time.Convert<UnitType.Time.Hour, UnitType.Time.Day>(quantity);
            decimal converted = (decimal)hour;

            Assert.AreEqual(72m, converted);
        }

        private void CheckNames(UnitType item, string expectedSingular, string expectedPlural)
        {
            string singular = item.SingularUnitName;
            string plural = item.PluralUnitName;

            Assert.AreEqual(expectedSingular, singular);
            Assert.AreEqual(expectedPlural, plural);
        }
    }
}
