﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LqbGrammar.DataTypes;
using NUnit.Framework;

namespace LendingQB.Test.Developers.FOOL.LqbGrammarTest.DataTypesTest
{
    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public class EmployerTaxIdentificationNumberTest
    {
        [Test]
        public void Create_AlphabeticInput_ReturnsNull()
        {
            var invalid = "abcdefghi";

            var ein = EmployerTaxIdentificationNumber.Create(invalid);

            Assert.AreEqual(null, ein);
        }

        [Test]
        public void Create_FiveDigits_ReturnsNull()
        {
            var invalid = "12345";

            var ein = EmployerTaxIdentificationNumber.Create(invalid);

            Assert.AreEqual(null, ein);
        }

        [Test]
        public void Create_TenDigits_ReturnsNull()
        {
            var invalid = "1234567890";

            var ein = EmployerTaxIdentificationNumber.Create(invalid);

            Assert.AreEqual(null, ein);
        }

        [Test]
        public void Create_ValidInputWithDash_RemovesDash()
        {
            var test = "12-3456789";

            var ein = EmployerTaxIdentificationNumber.Create(test).Value;

            Assert.AreEqual("123456789", ein.ToString());
        }

        [Test]
        public void Create_ValidInputWithoutDash_Succeeds()
        {
            var test = "123456789";

            var ein = EmployerTaxIdentificationNumber.Create(test).Value;

            Assert.AreEqual("123456789", ein.ToString());
        }
    }
}
