﻿namespace LendingQB.Test.Developers.FOOL.LqbGrammarTest.DataTypesTest
{
    using LqbGrammar.DataTypes;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public sealed class CountryCodeIso3Test
    {
        [TestCase("MEX")]
        [TestCase("TEXAS")]
        [TestCase("666")]
        [TestCase("p4ssw04d!")]
        public void Create_NoValidation_TakesWhatever(string testValue)
        {
            var code = CountryCodeIso3.Create(testValue);
            Assert.IsTrue(code != null);
        }

        [TestCase(null)]
        [TestCase("")]
        [TestCase(" \t ")]
        public void Create_BlankInput_ReturnsNull(string value)
        {
            var code = CountryCodeIso3.Create(null);
            Assert.IsTrue(code == null);
        }

        [TestCase("MEX", false)]
        [TestCase("CAN", false)]
        [TestCase("USA", false)]
        [TestCase("TEXAS", true)]
        [TestCase("666", true)]
        [TestCase("p4ssw04d!", true)]
        public void Create_WithValidation_ReturnsExpected(string testValue, bool returnNull)
        {
            var code = CountryCodeIso3.CreateWithValidation(testValue);
            Assert.AreEqual(returnNull, code == null);
       }

        [TestCase("MEX", "Mexico")]
        [TestCase("CAN", "Canada")]
        [TestCase("USA", "United States")]
        public void ToCountryName_ReturnsExpected(string testValue, string expected)
        {
            var code = CountryCodeIso3.CreateWithValidation(testValue).Value;
            Assert.AreEqual(expected, code.ToCountryName().ToString());
        }

        [TestCase("MEX", "MX")]
        [TestCase("CAN", "CA")]
        [TestCase("USA", "US")]
        public void ToIso2_ReturnsExpected(string testValue, string expected)
        {
            var code = CountryCodeIso3.CreateWithValidation(testValue).Value;
            Assert.AreEqual(expected, code.ToIso2().ToString());
        }
    }
}
