﻿namespace LendingQB.Test.Developers.FOOL.LqbGrammarTest.DataTypesTest
{
    using System;
    using LqbGrammar.DataTypes;
    using NUnit.Framework;
    using System.IO;
    using System.Runtime.Serialization.Formatters.Binary;

    [TestFixture]
    public sealed class SecurityTokenTest
    {
        [Test]
        public void GoodValuesSerDeser()
        {
            var brokerIdString = Guid.NewGuid().ToString();
            var userIdString = Guid.NewGuid().ToString();
            var fullNameString = "Some Guy";

            var token = new SecurityToken();
            token.BrokerId = BrokerIdentifier.Create(brokerIdString).Value;
            token.UserId = UserAccountIdentifier.Create(userIdString).Value;
            token.Fullname = PersonName.Create(fullNameString).Value;
            
            using (var ms = new MemoryStream())
            {
                var bf = new BinaryFormatter();
                bf.Serialize(ms, token);
                ms.Seek(0, SeekOrigin.Begin);
                SecurityToken token2 = (SecurityToken)bf.Deserialize(ms);

                Assert.That(token2.BrokerId.ToString() == brokerIdString);
                Assert.That(token2.UserId.ToString() == userIdString);
                Assert.That(token2.Fullname.ToString() == fullNameString);
            }
        }

        [Test]
        public void BadValuesSerDeser()
        {
            var brokerIdString = "nope";
            var userIdString = "not an identifier.";
            var fullNameString = "Mr & Mrs BadName";

            var token = new SecurityToken();
            var brokerId = BrokerIdentifier.Create(brokerIdString);
            var userId = UserAccountIdentifier.Create(userIdString);
            var personName = PersonName.Create(fullNameString);

            token.BrokerId = brokerId.HasValue ? brokerId.Value : BrokerIdentifier.BadIdentifier;
            token.UserId = userId.HasValue ? userId.Value : UserAccountIdentifier.BadIdentifier;
            token.Fullname = personName.HasValue ? personName.Value : PersonName.BadName;

            using (var ms = new MemoryStream())
            {
                var bf = new BinaryFormatter();
                bf.Serialize(ms, token);
                ms.Seek(0, SeekOrigin.Begin);
                SecurityToken token2 = (SecurityToken)bf.Deserialize(ms);

                Assert.That(token2.BrokerId.ToString() == BrokerIdentifier.BadIdentifier.ToString());
                Assert.That(token2.UserId.ToString() == UserAccountIdentifier.BadIdentifier.ToString());
                Assert.That(token2.Fullname.ToString() == PersonName.BadName.ToString());
            }
        }
    }
}
