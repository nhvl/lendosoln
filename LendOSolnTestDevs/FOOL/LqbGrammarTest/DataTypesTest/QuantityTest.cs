﻿namespace LendingQB.Test.Developers.FOOL.LqbGrammarTest.DataTypesTest
{
    using LqbGrammar.DataTypes;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public class QuantityTest
    {
        [Test]
        public void EqualityTests()
        {
            var money1 = Money.Create(1.12m).Value;
            var money2 = Money.Create(2.57m).Value;
            var money2same = Money.Create(2.57m).Value;

            Assert.IsTrue(money1 != money2);
            Assert.IsTrue(money2 == money2same);
            Assert.IsTrue(!money1.Equals(money2));
            Assert.IsTrue(money2.Equals(money2same));
        }

        [Test]
        public void InequalityTests()
        {
            var money1 = Money.Create(1.12m).Value;
            var money2 = Money.Create(2.57m).Value;
            var money2same = Money.Create(2.57m).Value;

            Assert.IsTrue(money1 <= money2);
            Assert.IsTrue(money2 <= money2same);
            Assert.IsTrue(money1 < money2);
            Assert.IsFalse(money2 < money2same);
            Assert.IsTrue(money2 >= money1);
            Assert.IsTrue(money2 >= money2same);
            Assert.IsTrue(money2 > money1);
            Assert.IsFalse(money2 > money2same);
        }

        [Test]
        public void AdditionTest()
        {
            var money1 = Money.Create(1.12m).Value;
            var money2 = Money.Create(2.57m).Value;
            var money3 = Money.Create(3.69m).Value; // note: is the sum on purpose

            var sum = money1 + money2;

            Assert.IsTrue(money3 == sum);
        }

        [Test]
        public void SubtractionTest()
        {
            var money1 = Money.Create(1.12m).Value;
            var money2 = Money.Create(2.57m).Value; // note: is equidistant on purpose
            var money3 = Money.Create(4.02m).Value;

            var diff1 = money3 - money2;
            var diff2 = money2 - money1;

            Assert.IsTrue(diff1 == diff2);
        }
    }
}
