﻿namespace LendingQB.Test.Developers.FOOL.LqbGrammarTest.DataTypesTest
{
    using LqbGrammar.DataTypes;
    using NUnit.Framework;

    using DayCount = LqbGrammar.DataTypes.Count<LqbGrammar.DataTypes.UnitType.Day>;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public class CountTest
    {
        [Test]
        public void EqualityTests()
        {
            var days1 = DayCount.Create(1).Value;
            var days2 = DayCount.Create(2).Value;
            var days2same = DayCount.Create(2).Value;

            Assert.IsTrue(days1 != days2);
            Assert.IsTrue(days2 == days2same);
            Assert.IsTrue(!days1.Equals(days2));
            Assert.IsTrue(days2.Equals(days2same));
        }

        [Test]
        public void InequalityTests()
        {
            var days1 = DayCount.Create(1).Value;
            var days2 = DayCount.Create(2).Value;
            var days2same = DayCount.Create(2).Value;

            Assert.IsTrue(days1 <= days2);
            Assert.IsTrue(days2 <= days2same);
            Assert.IsTrue(days1 < days2);
            Assert.IsFalse(days2 < days2same);
            Assert.IsTrue(days2 >= days1);
            Assert.IsTrue(days2 >= days2same);
            Assert.IsTrue(days2 > days1);
            Assert.IsFalse(days2 > days2same);
        }

        [Test]
        public void AdditionTest()
        {
            var days1 = DayCount.Create(1).Value;
            var days2 = DayCount.Create(2).Value;
            var days3 = DayCount.Create(3).Value;

            var sum = days1 + days2;

            Assert.IsTrue(days3 == sum);
        }

        [Test]
        public void SubtractionTest()
        {
            var days1 = DayCount.Create(1).Value;
            var days2 = DayCount.Create(2).Value;
            var days3 = DayCount.Create(3).Value;

            var sum1 = days3 - days2;
            var sum2 = days3 - days1;
            var sum1same = days2 - days1;

            Assert.IsTrue(sum1 == days1);
            Assert.IsTrue(sum2 == days2);
            Assert.IsTrue(sum1same == sum1);
        }
    }
}
