﻿namespace LendingQB.Test.Developers.FOOL.LqbGrammarTest.DataTypesTest
{
    using System;
    using LqbGrammar.DataTypes;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public sealed class ZonedDateTimeTest
    {
        [Test]
        public void CreateWithDateTime()
        {
            var now = DateTime.Now;

            var test = ZonedDateTime.Create(now, LqbTimeZone.PacificTime);
            Assert.IsTrue(test != null);
            Assert.IsTrue(now == test.Value.DateTime.LocalDateTime);
        }

        [Test]
        public void CreateWithString()
        {
            var now = DateTime.Now;

            var test = ZonedDateTime.Create(now, LqbTimeZone.PacificTime);
            string serialized = test.ToString();

            var recovered = ZonedDateTime.Create(serialized);
            Assert.IsTrue(recovered != null);
            Assert.AreEqual(now, recovered.Value.DateTime.LocalDateTime);
        }

        [Test]
        public void TestEquality()
        {
            var now = DateTime.Now;
            var ahead = now.AddHours(1);

            var testNow = ZonedDateTime.Create(now, LqbTimeZone.PacificTime).Value;
            var testAhead = ZonedDateTime.Create(ahead, LqbTimeZone.MountainTime).Value;

            Assert.IsTrue(testNow == testAhead);
        }

        [Test]
        public void TestInequality()
        {
            var now = DateTime.Now;
            System.Threading.Thread.Sleep(1);
            var later = DateTime.Now;

            var testNow = ZonedDateTime.Create(now, LqbTimeZone.PacificTime).Value;
            var testLater = ZonedDateTime.Create(later, LqbTimeZone.PacificTime).Value;

            Assert.IsTrue(testNow != testLater);
        }
    }
}
