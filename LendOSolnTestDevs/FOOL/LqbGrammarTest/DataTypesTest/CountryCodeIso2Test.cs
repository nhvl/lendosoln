﻿namespace LendingQB.Test.Developers.FOOL.LqbGrammarTest.DataTypesTest
{
    using LqbGrammar.DataTypes;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public sealed class CountryCodeIso2Test
    {
        [TestCase("MX")]
        [TestCase("TEXAS")]
        [TestCase("666")]
        [TestCase("p4ssw04d!")]
        public void Create_NoValidation_TakesWhatever(string testValue)
        {
            var code = CountryCodeIso2.Create(testValue);
            Assert.IsTrue(code != null);
        }

        [TestCase(null)]
        [TestCase("")]
        [TestCase(" \t ")]
        public void Create_BlankInput_ReturnsNull(string value)
        {
            var code = CountryCodeIso2.Create(value);
            Assert.IsTrue(code == null);
        }

        [TestCase("MX", false)]
        [TestCase("CA", false)]
        [TestCase("US", false)]
        [TestCase("TEXAS", true)]
        [TestCase("666", true)]
        [TestCase("p4ssw04d!", true)]
        public void Create_WithValidation_ReturnsExpected(string testValue, bool returnNull)
        {
            var code = CountryCodeIso2.CreateWithValidation(testValue);
            Assert.AreEqual(returnNull, code == null);
        }

        [TestCase("MX", "MEX")]
        [TestCase("CA", "CAN")]
        [TestCase("US", "USA")]
        public void ToIso3_ReturnsExpected(string testValue, string expected)
        {
            var code = CountryCodeIso2.CreateWithValidation(testValue).Value;
            Assert.AreEqual(expected, code.ToIso3().ToString());
        }
    }
}
