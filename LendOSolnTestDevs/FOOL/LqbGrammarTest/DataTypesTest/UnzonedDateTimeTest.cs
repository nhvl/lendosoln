﻿namespace LendingQB.Test.Developers.FOOL.LqbGrammarTest.DataTypesTest
{
    using System;
    using LqbGrammar.DataTypes;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public sealed class UnzonedDateTimeTest
    {
        [Test]
        public void CreateWithDateTime()
        {
            var now = DateTime.Now;

            var test = UnzonedDateTime.Create(now);
            Assert.IsTrue(test != null);
            Assert.IsTrue(now == test.Value.DateTime);
        }

        [Test]
        public void CreateWithString()
        {
            var now = DateTime.Now;

            var test = UnzonedDateTime.Create(now).Value;
            string serialized = test.ToString();
            Assert.AreEqual(UnzonedDateTime.FormatString.Length, serialized.Length);

            var recovered = UnzonedDateTime.Create(serialized);
            Assert.IsTrue(recovered != null);
            Assert.AreEqual(now, recovered.Value.DateTime);
        }

        [Test]
        public void TestEquality()
        {
            var now = DateTime.Now;
            var copy = now;

            var testNow = UnzonedDateTime.Create(now).Value;
            var testCopy = UnzonedDateTime.Create(copy).Value;

            Assert.IsTrue(testNow == testCopy);
        }

        [Test]
        public void TestInequality()
        {
            var now = DateTime.Now;
            System.Threading.Thread.Sleep(1);
            var later = DateTime.Now;

            var testNow = UnzonedDateTime.Create(now).Value;
            var testLater = UnzonedDateTime.Create(later).Value;

            Assert.IsTrue(testNow != testLater);
        }
    }
}
