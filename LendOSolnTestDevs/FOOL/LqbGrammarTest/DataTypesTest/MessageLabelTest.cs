﻿namespace LendingQB.Test.Developers.FOOL.LqbGrammarTest.DataTypesTest
{
    using LqbGrammar.DataTypes;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public class MessageLabelTest
    {
        [Test]
        public void GoodLabel()
        {
            string good = "MESSAGE_FOR_THE_AGES_SPECIFICALLY_2016";
            var label = MessageLabel.Create(good);
            Assert.IsTrue(label != null);
            Assert.AreEqual(good, label.Value.ToString());
        }

        [Test]
        public void BadLabel()
        {
            string bad = "666_is_really_bad.";
            var label = MessageLabel.Create(bad);
            Assert.IsTrue(label == null);
        }
    }
}
