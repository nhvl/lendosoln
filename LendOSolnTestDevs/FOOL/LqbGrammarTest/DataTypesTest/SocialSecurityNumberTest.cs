﻿namespace LendingQB.Test.Developers.FOOL.LqbGrammarTest.DataTypesTest
{
    using LqbGrammar.DataTypes;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public class SocialSecurityNumberTest
    {
        [Test]
        public void BadValue()
        {
            string bad = "01294567";

            var ssn = SocialSecurityNumber.Create(bad);
            Assert.IsTrue(ssn == null);
        }

        [Test]
        public void BadOneDash()
        {
            string bad = "012-345678";

            var ssn = SocialSecurityNumber.Create(bad);
            Assert.IsTrue(ssn == null);
        }

        [Test]
        public void GoodNoDashes()
        {
            string good = "012-34-5678";
            string stripped = good.Replace("-", string.Empty);

            var ssn = SocialSecurityNumber.Create(stripped);
            Assert.IsTrue(ssn != null);
            Assert.AreEqual(good, ssn.Value.UnmaskedSsn);
        }

        [Test]
        public void GoodTwoDashes()
        {
            string good = "012-34-5678";

            var ssn = SocialSecurityNumber.Create(good);
            Assert.IsTrue(ssn != null);
            Assert.AreEqual(good, ssn.Value.UnmaskedSsn);
        }
    }
}
