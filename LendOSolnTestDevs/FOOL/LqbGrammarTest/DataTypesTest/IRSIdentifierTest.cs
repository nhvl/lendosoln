﻿namespace LendingQB.Test.Developers.FOOL.LqbGrammarTest.DataTypesTest
{
    using System;
    using LqbGrammar.DataTypes;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public class IRSIdentifierTest
    {
        [Test]
        public void BadEinCreateTest()
        {
            string ein = "01-456321";

            var test = IRSIdentifier.Create(ein);
            Assert.IsTrue(test == null);
        }

        [Test]
        public void BadSsnCreateTest()
        {
            string ssn = "123-45-678";

            var test = IRSIdentifier.Create(ssn);
            Assert.IsTrue(test == null);
        }

        [Test]
        public void GoodEinCreateTest()
        {
            string ein = "01-4563219";

            var test = IRSIdentifier.Create(ein);
            Assert.IsTrue(test != null);
            Assert.IsTrue(test.Value.IsSSN == BooleanKleene.False);
            Assert.AreEqual(ein, test.Value.ToString());
        }

        [Test]
        public void GoodSsnCreateTest()
        {
            string ssn = "123-45-6789";

            var test = IRSIdentifier.Create(ssn);
            Assert.IsTrue(test != null);
            Assert.IsTrue(test.Value.IsSSN == BooleanKleene.True);
            Assert.AreEqual(ssn, test.Value.ToString());
        }

        [Test]
        public void CastEinToIrs()
        {
            string ein = "01-4563219";
            string canonical = ein.Replace("-", string.Empty);
            var id = EmployerTaxIdentificationNumber.Create(ein);
            Assert.IsTrue(id != null);

            IRSIdentifier irs = id.Value;

            Assert.IsTrue(irs.IsSSN == BooleanKleene.False);
            Assert.AreEqual(canonical, irs.ToString());
        }

        [Test]
        public void CastSsnToIrs()
        {
            string ssn = "123-45-6789";
            var id = SocialSecurityNumber.Create(ssn);
            Assert.IsTrue(id != null);

            IRSIdentifier irs = id.Value;

            Assert.IsTrue(irs.IsSSN == BooleanKleene.True);
            Assert.AreEqual(ssn, irs.ToString());
        }

        [Test]
        public void CastIrsFromEinToEin()
        {
            string ein = "01-4563219";
            string canonical = ein.Replace("-", string.Empty);
            var irs = IRSIdentifier.Create(ein);
            Assert.IsTrue(irs != null);

            EmployerTaxIdentificationNumber specific = (EmployerTaxIdentificationNumber)irs.Value;
            Assert.AreEqual(canonical, specific.ToString());
        }

        [Test]
        [ExpectedException(typeof(InvalidCastException))]
        public void CastIrsFromSsnToEin()
        {
            string ssn = "123-45-6789";
            var irs = IRSIdentifier.Create(ssn);
            Assert.IsTrue(irs != null);

            EmployerTaxIdentificationNumber specific = (EmployerTaxIdentificationNumber)irs.Value;
        }

        [Test]
        [ExpectedException(typeof(InvalidCastException))]
        public void CastIrsFromEinToSsn()
        {
            string ein = "01-4563219";
            var irs = IRSIdentifier.Create(ein);
            Assert.IsTrue(irs != null);

            SocialSecurityNumber ssn = (SocialSecurityNumber)irs.Value;
        }

        [Test]
        public void CastIrsFromSsnToSSn()
        {
            string ssn = "123-45-6789";
            var irs = IRSIdentifier.Create(ssn);
            Assert.IsTrue(irs != null);

            SocialSecurityNumber specific = (SocialSecurityNumber)irs.Value;
            Assert.AreEqual(ssn, specific.UnmaskedSsn);
        }

        [Test]
        public void CastIrsFromAnbiguousSsnToSSn()
        {
            string ssn = "123-45-6789";
            string stripped = ssn.Replace("-", string.Empty);
            var irs = IRSIdentifier.Create(stripped);
            Assert.IsTrue(irs != null);
            Assert.IsTrue(irs.Value.IsSSN == BooleanKleene.Indeterminant);

            SocialSecurityNumber specific = (SocialSecurityNumber)irs.Value;
            Assert.AreEqual(ssn, specific.UnmaskedSsn);
        }

        [Test]
        public void ExplicitCastForImplicit()
        {
            string ssn = "123-45-6789";
            var id = SocialSecurityNumber.Create(ssn);
            Assert.IsTrue(id != null);

            IRSIdentifier ambiguous = (IRSIdentifier)id.Value;

            Assert.IsTrue(ambiguous.IsSSN == BooleanKleene.True);
            Assert.AreEqual(ssn, ambiguous.ToString());
        }
    }
}
