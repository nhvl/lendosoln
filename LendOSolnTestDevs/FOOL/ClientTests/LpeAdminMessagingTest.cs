﻿namespace LendingQB.Test.Developers.FOOL.ClientTests
{
    using System;
    using System.IO;
    using LendersOffice.Common;
    using LendersOffice.Drivers.Emailer;
    using LqbGrammar.Drivers.Emailer;
    using NUnit.Framework;
    using Utils;

    [TestFixture]
    [Category(CategoryConstants.LqbMocks)]
    [Category(CategoryConstants.IntegrationTest)]
    public class LpeAdminMessagingTest
    {
        [Test]
        public void SendEmail()
        {
            using (var helper = new FoolHelper())
            {
                if (!helper.RegisterMockDriverFactory<IEmailDriverFactory>(new MockEmailDriverFactory()))
                {
                    return;
                }

                helper.RegisterRealDriver(FoolHelper.DriverType.ConfigurationQuery);

                var error = E_LpeErrorAreaT.MapProcessor;
                var effectiveDate = DateTime.Now;
                string sFilePath = @"E:\SRE\PROD\Templates\DownloadPath\FLAGSTAR_TIER.XLS";
                string finalInfo = "Test error message.  This is not the actual error, but it may cause trouble in some scenarios if the regular expression is too strict; currently it forbids apostrophes.";
                var subject = "Effective [" + effectiveDate + "] Error process " + Path.GetFileName(sFilePath);

                LpeAdminMessaging.SendEmail(error, subject, finalInfo);
            }
        }
    }
}
