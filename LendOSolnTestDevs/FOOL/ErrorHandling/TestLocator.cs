﻿namespace LendingQB.Test.Developers.FOOL.ErrorHandling
{
    using LqbGrammar;
    using NUnit.Framework;
    using Utils;

    interface IDummyInterface
    {
    }

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public sealed class TestLocator
    {
        [Test]
        public void ValidRegistration()
        {
            using (var helper = new FoolHelper())
            {
                if (!helper.RegisterMockDriverFactory<IDummyInterface>(new DummyImpl()))
                {
                    return;
                }

                var item = GenericLocator<IDummyInterface>.Factory;
                Assert.IsNotNull(item);
            }
       }

        [Test]
        public void NonRegistration()
        {
            var item = GenericLocator<IDummyInterface>.Factory;
            Assert.IsNull(item);
        }

        private class DummyImpl : IDummyInterface
        {
        }
    }
}
