﻿namespace LendingQB.Test.Developers.FOOL.ErrorHandling
{
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;
    using LqbGrammar.Exceptions;
    using NUnit.Framework;
    using Utils;

    [TestFixture]
    [Category(CategoryConstants.LqbMocks)]
    [Category(CategoryConstants.IntegrationTest)]
    public sealed class LogException
    {
        [Test]
        public void TestTryCatch()
        {
            using (var helper = new FoolHelper())
            {
                var factory = new LendersOffice.Drivers.MessageQueue.MockMessageQueueDriverFactory(null, null);
                if (!helper.RegisterMockDriverFactory<IMessageQueueDriverFactory>(factory))
                {
                    return;
                }

                helper.RegisterRealDriver(FoolHelper.DriverType.Logger);

                try
                {
                    var context = new StandardContext();
                    context.Category = "UNIT TEST";
                    context.LoanId = "Loan Identifier here";
                    context.ClientIp = "127.0.0.1";

                    throw new DeveloperException(ErrorMessage.SystemError, context);
                }
                catch (LqbException ex)
                {
                    ex.PassToHandler();

                    string message = factory.MessageBody;
                    Assert.IsFalse(string.IsNullOrEmpty(message));
                }
            }
        }

        [Test]
        public void TestSimpleContext()
        {
            using (var helper = new FoolHelper())
            {
                var factory = new LendersOffice.Drivers.MessageQueue.MockMessageQueueDriverFactory(null, null);
                if (!helper.RegisterMockDriverFactory<IMessageQueueDriverFactory>(factory))
                {
                    return;
                }

                helper.RegisterRealDriver(FoolHelper.DriverType.Logger);

                try
                {
                    var context = new SimpleContext("SIMPLE CONTEXT HERE");

                    throw new DeveloperException(ErrorMessage.SystemError, context);
                }
                catch (LqbException ex)
                {
                    ex.PassToHandler();

                    string message = factory.MessageBody;
                    Assert.IsFalse(string.IsNullOrEmpty(message));
                }
            }
        }
    }
}
