﻿namespace LendingQB.Test.Developers.FOOL
{
    using LqbGrammar.Queries;
    using System;
    using System.Collections.Generic;

    public class FakeConfigurationQueryFactory : IConfigurationQueryFactory
    {
        private ISiteConfigurationQuery siteConfig;
        private IStageConfigurationQuery stageConfig;

        public FakeConfigurationQueryFactory(ISiteConfigurationQuery siteConfig, IStageConfigurationQuery stageConfig)
        {
            this.siteConfig = siteConfig;
            this.stageConfig = stageConfig;
        }

        public FakeConfigurationQueryFactory()
        {
            this.siteConfig = null;
            this.stageConfig = null;
        }

        public Dictionary<string, string> SiteConfig { get; } = new Dictionary<string, string>();

        public Dictionary<string, Tuple<int, string>> StageConfig { get; } = new Dictionary<string, Tuple<int, string>>();

        public static FakeConfigurationQueryFactory RegisterFake(Utils.FoolHelper helper)
        {
            var instance = new FakeConfigurationQueryFactory();
            helper.RegisterMockDriverFactory<LqbGrammar.Queries.IConfigurationQueryFactory>(instance);
            return instance;
        }

        public ISiteConfigurationQuery CreateSiteConfiguration() => this.siteConfig ?? new FakeSiteConfigurationQuery { Config = this.SiteConfig };

        public IStageConfigurationQuery CreateStageConfiguration() => this.stageConfig ?? new FakeStageConfigurationQuery { Config = this.StageConfig };

        private class FakeSiteConfigurationQuery : ISiteConfigurationQuery
        {
            public Dictionary<string, string> Config { get; set; }

            public Dictionary<string, string> ReadAllValues() => this.Config;
        }

        private class FakeStageConfigurationQuery : IStageConfigurationQuery
        {
            public Dictionary<string, Tuple<int, string>> Config { get; set; }

            public Dictionary<string, Tuple<int, string>> ReadAllValues() => this.Config;
        }
    }
}
