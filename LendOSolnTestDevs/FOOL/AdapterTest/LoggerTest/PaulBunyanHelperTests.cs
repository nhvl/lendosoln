﻿namespace LendingQB.Test.Developers.FOOL.AdapterTest.LoggerTest
{
    using System;
    using System.Linq;
    using LendersOffice.Common;
    using LendersOffice.Drivers.Logger;
    using LqbGrammar;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;
    using LqbGrammar.Queries;
    using NSubstitute;
    using NUnit.Framework;

    /// <summary>
    /// Tests of the PaulBunyanHelper static helper class.
    /// </summary>
    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public class PaulBunyanHelperTests
    {
        /// <summary>
        /// Ensures that logging logic will work on all environments without exception, where some configuration values might not be present.
        /// </summary>
        /// <param name="msMqLogConnectStr">The log connection string which PBHelper gets from the ConstStage configuration.</param>
        /// <param name="msMqPbLogConnectStr">The log connection string which PBHelper gets from the ConstSite configuration.</param>
        [Test, Combinatorial]
        public void PaulBunyanLogWithDriver_QueueConstantsMissing_LogsToTargets(
            [Values(null, "", @"FormatName:DIRECT=OS:localhost\private$\DevLogMSMQ", @"FORMATNAME:DIRECT=OS:SOMEONE22WRK89\private$\DevLogMSMQ", "Invalid Connection String")] string msMqLogConnectStr,
            [Values(null, "", @"FormatName:DIRECT=OS:localhost\private$\DevLogMSMQ", @"FORMATNAME:DIRECT=OS:SOMEONE22WRK89\private$\DevLogMSMQ", "Invalid Connection String")] string msMqPbLogConnectStr)
        {
            int? logTargetCount = null;
            var mockLoggingDriver = Substitute.For<ILoggingDriver>();
            var mockLoggingDriverFactory = Substitute.For<ILoggingDriverFactory>();
            mockLoggingDriverFactory.Create().ReturnsForAnyArgs(mockLoggingDriver).AndDoes(callInfo => logTargetCount = callInfo.ArgAt<LoggingTargetInfo[]>(0).Length);
            LqbApplication.Current.RegisterFactory(mockLoggingDriverFactory);

            var siteConfigQueryDriver = Substitute.For<ISiteConfigurationQuery>();
            var stageConfigQueryDriver = Substitute.For<IStageConfigurationQuery>();
            var configQueryFactory = Substitute.For<IConfigurationQueryFactory>();
            configQueryFactory.CreateSiteConfiguration().Returns(siteConfigQueryDriver);
            configQueryFactory.CreateStageConfiguration().Returns(stageConfigQueryDriver);
            var configDict = new System.Collections.Generic.Dictionary<string, string>();
            if (MessageQueuePath.Create(msMqPbLogConnectStr) != null)
            {
                configDict.Add(nameof(LendersOffice.Constants.ConstSite.MsMqPbLogConnectStr), msMqPbLogConnectStr);
            }

            if (MessageQueuePath.Create(msMqLogConnectStr) != null)
            {
                configDict.Add(nameof(LendersOffice.Constants.ConstSite.MsMqLogConnectStr), msMqLogConnectStr);
            }

            siteConfigQueryDriver.ReadAllValues().Returns(configDict);
            stageConfigQueryDriver.ReadAllValues().Returns(configDict.ToDictionary(kvp => kvp.Key, kvp => new Tuple<int, string>(0, kvp.Value)));
            LqbApplication.Current.RegisterFactory(configQueryFactory);

            var message = "This is a test logging message";
            var category = "TEST";
            Assert.DoesNotThrow(() => PaulBunyanHelper.Log(LendersOffice.Logging.LoggingLevel.Info, category, message, null, useDriver: true));

            Assert.AreEqual(configDict.Count, logTargetCount);
            mockLoggingDriver.Received(1).LogInfo(LogMessage.Create(message).Value, typeof(LoggingHelper), Arg.Any<System.Collections.Generic.Dictionary<LogPropertyName, LogPropertyValue>>());
        }

        [TestFixtureTearDown]
        public void FixtureTearDown()
        {
            LqbApplication.Current.RestoreFactories();
        }
    }
}
