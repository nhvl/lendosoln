﻿namespace LendingQB.Test.Developers.FOOL.AdapterTest.LoggerTest
{
    using System.Collections.Generic;
    using global::Adapter.Logger;
    using LqbGrammar.DataTypes;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public sealed class LoggingTargetInfoFactoryTest
    {
        [Test]
        public void CreateEmail()
        {
            var server = EmailServerName.Create("smtp.meridianlink.com").Value;
            var port = PortNumber.Create(25).Value;
            var email = EmailAddress.Create("aland@lendingqb.com").Value;

            var list = new List<EmailAddress>();
            list.Add(email);

            var target = LoggingTargetInfoFactory.CreateEmailInfo(server, port, email, list);
            Assert.IsNotNull(target);
            Assert.IsTrue(target.GetType().ToString().Contains("EmailTargetData"));
        }

        [Test]
        public void CreateMsmq()
        {
            string good = @"FormatName:DIRECT=OS:loextdevweb1.middleearth.com\private$\lodev_LendingQBLog";
            var path = MessageQueuePath.Create(good).Value;

            var target = LoggingTargetInfoFactory.CreateMsmqInfo(path);
            Assert.IsNotNull(target);
            Assert.IsTrue(target.GetType().ToString().Contains("MsmqTargetData"));
        }
    }
}
