﻿namespace LendingQB.Test.Developers.FOOL.AdapterTest
{
    using System.Security.Cryptography;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public sealed class EncryptionKeyTest
    {
        [Test]
        public void AffirmDefaultParams()
        {
            // These are the values used in the KMS
            const int kmsKeySize = 256;
            const int kmsBlockSize = 128;
            const int kmsIVSize = kmsBlockSize;

            // This code is the same as found in Adapter.AesEncryptor.GenerateKey().
            // The NUnit test is to confirm that our encryption keys are consistent with the KMS.
            //
            // However, the KMS does not generate and store an IV, so even though our sizes are
            // consistent, we may need to degrade our system or beef up the KMS!
            using (var algorithm = new RijndaelManaged())
            {
                algorithm.GenerateIV();
                algorithm.GenerateKey();

                Assert.AreEqual(kmsKeySize, algorithm.KeySize);
                Assert.AreEqual(kmsBlockSize, algorithm.BlockSize);

                Assert.AreEqual(kmsKeySize, algorithm.Key.Length * 8);
                Assert.AreEqual(kmsIVSize, algorithm.IV.Length * 8);
            }
        }
    }
}
