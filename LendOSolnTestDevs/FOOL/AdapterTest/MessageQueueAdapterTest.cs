﻿namespace LendingQB.Test.Developers.FOOL.AdapterTest
{
    using LqbGrammar;
    using LqbGrammar.Adapters;
    using LqbGrammar.DataTypes;
    using NUnit.Framework;
    using Utils;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    [Category(CategoryConstants.LqbMocks)]
    public class MessageQueueAdapterTest
    {
        private const string LocalQueue = @"FormatName:DIRECT=OS:ALANDWRK.meridianlink.com\private$\devlogmsmq";

        [Test]
        public void LocalQueueName()
        {
            var path = MessageQueuePath.Create(LocalQueue);
            Assert.IsTrue(path != null);
            Assert.AreEqual(LocalQueue, path.Value.ToString());
        }

        [Test]
        public void PrepareForJSON()
        {
            using (var helper = new FoolHelper())
            {
                if (!helper.RegisterRealDriver(FoolHelper.DriverType.MessageQueue))
                {
                    return;
                }

                var queue = GetQueue();
                var adapter = GetAdapter();

                var lqbQueue = adapter.PrepareSourceQueueForJSON(queue);

                Assert.AreEqual(LocalQueue, lqbQueue.Value.Path);
                Assert.IsTrue(lqbQueue.Value.MessageReadPropertyFilter.ArrivedTime);
            }
        }

        [Test]
        public void PrepareForXML()
        {
            using (var helper = new FoolHelper())
            {
                if (!helper.RegisterRealDriver(FoolHelper.DriverType.MessageQueue))
                {
                    return;
                }

                var queue = GetQueue();
                var adapter = GetAdapter();

                var lqbQueue = adapter.PrepareSourceQueueForXML(queue);

                Assert.AreEqual(LocalQueue, lqbQueue.Value.Path);
                Assert.IsTrue(lqbQueue.Value.MessageReadPropertyFilter.ArrivedTime);
                Assert.IsTrue(lqbQueue.Value.Formatter is global::Adapter.XDocumentMessageFormatter);
            }
        }

        [Test]
        [Ignore("This test is tied to Alan's machine so cannot be run by anyone else.")]
        public void SendAndReceiveJSON()
        {
            using (var helper = new FoolHelper())
            {
                if (!helper.RegisterRealDrivers(FoolHelper.DriverType.MessageQueue, FoolHelper.DriverType.Json))
                {
                    return;
                }

                var queue = GetQueue();
                var label = MessageLabel.Create("Alan_Test");
                var adapter = GetAdapter();
                var person = JsonAdapterTest.CreatePerson();

                adapter.SendJSON(queue, label, person);
                System.Threading.Thread.Sleep(1000);

                System.DateTime arriveTime;
                JsonAdapterTest.SimplePerson recovered;
                using (var readQueue = adapter.PrepareSourceQueueForJSON(queue))
                {
                    recovered = adapter.ReceiveJSON<JsonAdapterTest.SimplePerson>(readQueue, TimeoutInSeconds.Thirty, out arriveTime);
                }

                Assert.IsTrue(arriveTime > System.DateTime.MinValue);
                Assert.IsTrue(arriveTime < System.DateTime.MaxValue);
                Assert.IsNotNull(recovered);
                Assert.AreEqual(person.ID, recovered.ID);
                Assert.AreEqual(person.FirstName, recovered.FirstName);
                Assert.AreEqual(person.LastName, recovered.LastName);
                Assert.AreEqual(person.Sex, recovered.Sex);
            }
        }

        private MessageQueuePath GetQueue()
        {
            return MessageQueuePath.Create(LocalQueue).Value;
        }

        private IMessageQueueAdapter GetAdapter()
        {
            var factory = GenericLocator<IMessageQueueAdapterFactory>.Factory;
            return factory.Create();
        }
    }
}
