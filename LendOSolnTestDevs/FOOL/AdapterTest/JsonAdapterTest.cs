﻿namespace LendingQB.Test.Developers.FOOL.AdapterTest
{
    using LqbGrammar;
    using LqbGrammar.Adapters;
    using LqbGrammar.Drivers;
    using NUnit.Framework;
    using Utils;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    [Category(CategoryConstants.LqbMocks)]
    public class JsonAdapterTest
    {
        public enum Sex { Male, Female }

        public class SimplePerson
        {
            public int ID { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public Sex Sex { get; set; }
        }

        [Test]
        public void DefaultSerialize()
        {
            using (var helper = new FoolHelper())
            {
                if (!helper.RegisterRealDriver(FoolHelper.DriverType.Json))
                {
                    return;
                }

                var person = CreatePerson();
                var adapter = CreateDefaultAdapter();

                Assert.Throws<LqbGrammar.Exceptions.DeveloperException>(() => adapter.Serialize(person));
            }
        }

        [Test]
        public void DataContractSerialize()
        {
            using (var helper = new FoolHelper())
            {
                if (!helper.RegisterRealDriver(FoolHelper.DriverType.Json))
                {
                    return;
                }

                var data = new EDocs.EdocsPageMetadata();
                data.PdfHeight = 700;
                data.PdfWidth = 200;
                data.ReferenceDocumentId = System.Guid.NewGuid();
                data.Rotation = 90;

                var adapter = CreateDataContractAdapter();
                string json = adapter.Serialize(data);

                var recovered = adapter.Deserialize<EDocs.EdocsPageMetadata>(json);
                Assert.AreEqual(data.PdfHeight, recovered.PdfHeight);
                Assert.AreEqual(data.PdfWidth, recovered.PdfWidth);
                Assert.AreEqual(data.ReferenceDocumentId, recovered.ReferenceDocumentId);
                Assert.AreEqual(data.Rotation, recovered.Rotation);
            }
        }

        [Test]
        public void JavascriptSerialize()
        {
            using (var helper = new FoolHelper())
            {
                if (!helper.RegisterRealDriver(FoolHelper.DriverType.Json))
                {
                    return;
                }

                var data = new EDocs.EdocsPageMetadata();
                data.PdfHeight = 700;
                data.PdfWidth = 200;
                data.ReferenceDocumentId = System.Guid.NewGuid();
                data.Rotation = 90;

                var adapter = CreateJavascriptAdapter();
                string json = adapter.Serialize(data);

                var recovered = adapter.Deserialize<EDocs.EdocsPageMetadata>(json);
                Assert.AreEqual(data.PdfHeight, recovered.PdfHeight);
                Assert.AreEqual(data.PdfWidth, recovered.PdfWidth);
                Assert.AreEqual(data.ReferenceDocumentId, recovered.ReferenceDocumentId);
                Assert.AreEqual(data.Rotation, recovered.Rotation);
            }
        }

        [Test]
        public void SimpleSerialize()
        {
            using (var helper = new FoolHelper())
            {
                if (!helper.RegisterRealDriver(FoolHelper.DriverType.Json))
                {
                    return;
                }

                FakeConfigurationQueryFactory.RegisterFake(helper); // JSON.net checks ConstStage.UseJsonNetHtmlEscaping

                var person = CreatePerson();

                var adapter = CreateSimpleAdapter();
                string json = adapter.Serialize(person);

                var recovered = adapter.Deserialize<SimplePerson>(json);
                Assert.AreEqual(person.ID, recovered.ID);
                Assert.AreEqual(person.FirstName, recovered.FirstName);
                Assert.AreEqual(person.LastName, recovered.LastName);
                Assert.AreEqual(person.Sex, recovered.Sex);
            }
        }

        [Test]
        public void NameHandlingSerialize()
        {
            using (var helper = new FoolHelper())
            {
                if (!helper.RegisterRealDriver(FoolHelper.DriverType.Json))
                {
                    return;
                }

                FakeConfigurationQueryFactory.RegisterFake(helper); // JSON.net checks ConstStage.UseJsonNetHtmlEscaping

                var person = CreatePerson();

                var adapter = CreateNameHandlingAdapter();
                string json = adapter.Serialize(person);

                var recovered = adapter.Deserialize<SimplePerson>(json);
                Assert.AreEqual(person.ID, recovered.ID);
                Assert.AreEqual(person.FirstName, recovered.FirstName);
                Assert.AreEqual(person.LastName, recovered.LastName);
                Assert.AreEqual(person.Sex, recovered.Sex);
            }
        }

        [Test]
        public void FancySerialize()
        {
            using (var helper = new FoolHelper())
            {
                if (!helper.RegisterRealDriver(FoolHelper.DriverType.Json))
                {
                    return;
                }

                FakeConfigurationQueryFactory.RegisterFake(helper); // JSON.net checks ConstStage.UseJsonNetHtmlEscaping

                var person = CreatePerson();

                var adapter = CreateFancyAdapter();
                string json = adapter.Serialize(person);

                var recovered = adapter.Deserialize<SimplePerson>(json);
                Assert.AreEqual(person.ID, recovered.ID);
                Assert.AreEqual(person.FirstName, recovered.FirstName);
                Assert.AreEqual(person.LastName, recovered.LastName);
                Assert.AreEqual(person.Sex, recovered.Sex);
            }
        }

        [Test]
        public void IdentitySanitize()
        {
            using (var helper = new FoolHelper())
            {
                if (!helper.RegisterRealDriver(FoolHelper.DriverType.Json))
                {
                    return;
                }

                FakeConfigurationQueryFactory.RegisterFake(helper); // JSON.net checks ConstStage.UseJsonNetHtmlEscaping

                var person = CreatePerson();
                var adapter = CreateFancyAdapter(); // important to use Fancy here because the sanitize code adds some CRLF's when not in original.
                string json = adapter.Serialize(person);

                string sanitized = adapter.SanitizeJsonString(json);

                Assert.AreEqual(json, sanitized);
            }
        }

        [Test]
        public void AlteredBySanitize()
        {
            using (var helper = new FoolHelper())
            {
                if (!helper.RegisterRealDriver(FoolHelper.DriverType.Json))
                {
                    return;
                }

                FakeConfigurationQueryFactory.RegisterFake(helper); // JSON.net checks ConstStage.UseJsonNetHtmlEscaping

                var person = CreatePerson();
                person.LastName += System.Convert.ToChar(0x5); // adding an invalid xml character
                var adapter = CreateFancyAdapter(); // important to use Fancy here because the sanitize code adds some CRLF's when not in original.
                string json = adapter.Serialize(person);

                string sanitized = adapter.SanitizeJsonString(json);

                Assert.AreNotEqual(json, sanitized);
            }
        }

        private IJsonAdapter CreateDefaultAdapter()
        {
            var factory = GenericLocator<IJsonAdapterFactory>.Factory;
            var adapter = factory.Create(JsonSerializer.Default, JsonSerializerOptions.None);
            return adapter;
        }

        private IJsonAdapter CreateDataContractAdapter()
        {
            var factory = GenericLocator<IJsonAdapterFactory>.Factory;
            var adapter = factory.Create(JsonSerializer.DataContract, JsonSerializerOptions.None);
            return adapter;
        }

        private IJsonAdapter CreateJavascriptAdapter()
        {
            var factory = GenericLocator<IJsonAdapterFactory>.Factory;
            var adapter = factory.Create(JsonSerializer.Javascript, JsonSerializerOptions.None);
            return adapter;
        }

        private IJsonAdapter CreateSimpleAdapter()
        {
            var factory = GenericLocator<IJsonAdapterFactory>.Factory;
            var adapter = factory.Create(JsonSerializer.JsonConvert, JsonSerializerOptions.None);
            return adapter;
        }

        private IJsonAdapter CreateNameHandlingAdapter()
        {
            var factory = GenericLocator<IJsonAdapterFactory>.Factory;
            var adapter = factory.Create(JsonSerializer.JsonConvert, JsonSerializerOptions.TypeNameHandling_All);
            return adapter;
        }

        private IJsonAdapter CreateFancyAdapter()
        {
            var factory = GenericLocator<IJsonAdapterFactory>.Factory;
            var adapter = factory.Create(JsonSerializer.JsonConvert, JsonSerializerOptions.IndentedPlusStringEnumConverter);
            return adapter;
        }

        internal static SimplePerson CreatePerson()
        {
            return new SimplePerson() { ID = 5, FirstName = "Harry", LastName = "Houdini", Sex = Sex.Male };
        }
    }
}
