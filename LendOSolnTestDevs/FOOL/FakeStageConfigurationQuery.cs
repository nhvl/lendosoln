﻿namespace LendingQB.Test.Developers.FOOL
{
    using System;
    using System.Collections.Generic;
    using LqbGrammar.Queries;

    public class FakeStageConfigurationQuery : IStageConfigurationQuery
    {
        private Dictionary<string, Tuple<int, string>> fakeValues;

        public FakeStageConfigurationQuery(Dictionary<string, Tuple<int, string>> fakeValues)
        {
            this.fakeValues = fakeValues;
        }

        public Dictionary<string, Tuple<int, string>> ReadAllValues()
        {
            return this.fakeValues;
        }
    }
}
