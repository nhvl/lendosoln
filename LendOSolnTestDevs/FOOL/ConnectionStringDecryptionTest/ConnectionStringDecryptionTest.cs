﻿namespace LendingQB.Test.Developers.FOOL.ConnectionStringDecryptionTest
{
    using LendersOffice.Drivers.ConnectionStringDecryption;
    using LqbGrammar;
    using LqbGrammar.Adapters;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;
    using NUnit.Framework;
    using Utils;

    [TestFixture]
    [Category(CategoryConstants.LqbMocks)]
    [Category(CategoryConstants.IntegrationTest)]
    class ConnectionStringDecryptionTest
    {
        private readonly LocalFilePath ChangedFilePath = LocalFilePath.Create(TestUtilities.GetFilePath("ConnectionString/ConnectionStringKey_Changed.xml")).Value;
        private readonly LocalFilePath NotChangedFilePath = LocalFilePath.Create(TestUtilities.GetFilePath("ConnectionString/ConnectionStringKey_NotChanged.xml")).Value;
        private readonly LocalFilePath NotDeployedFilePath = LocalFilePath.Create(TestUtilities.GetFilePath("ConnectionString/ConnectionStringKey_NotDeployed.xml")).Value;

        private readonly DBConnectionString ConnectionStringPlainText = DBConnectionString.Create(@"Data Source=DBServerTest;Initial Catalog=DBTest;User ID=LoUser1;Password=LoPassword1;Encrypt=False").Value;
        private readonly DBConnectionString ConnectionStringEncryptedOldKey = DBConnectionString.Create(@"Data Source=DBServerTest;Initial Catalog=DBTest;User ID=""DspvuaZcICjRA7XMbKYDaA=="";Password=""al72anufXv+MAO/0hsZFlw=="";Encrypt=False").Value;
        private readonly DBConnectionString ConnectionStringEncryptedNewKey = DBConnectionString.Create(@"Data Source=DBServerTest;Initial Catalog=DBTest;User ID=""u3AePnD7d1p5/EqZqI+YdQ=="";Password=""m9qtZF3dy+b1Q47CeXrCJA=="";Encrypt=False").Value;
        private readonly DBConnectionString ConnectionStringUnknownKey = DBConnectionString.Create(@"Data Source=DBServerTest;Initial Catalog=DBTest;User ID=""WJE8gK/X0gAwwUiZZnn1nA=="";Password=""tm6Ni+2P2ujm965UAyWVAw=="";Encrypt=False").Value;

        private IConnectionStringDecryptionDriver CreateXmlFileDriver(LocalFilePath filePath)
        {
            var factory = GenericLocator<IConnectionStringDecryptionDriverFactory>.Factory;
            var driver = factory.CreateXmlFileDecrypter(filePath);
            return driver;
        }

        [Test]
        public void KeysNotDeployedTest()
        {
            using (var helper = new FoolHelper())
            {
                if (!helper.RegisterMockDriverFactory<IConnectionStringDecryptionDriverFactory>(new MockConnectionStringDecryptionDriverFactory()))
                {
                    return;
                }

                helper.RegisterRealDrivers(FoolHelper.DriverType.FileSystem, FoolHelper.DriverType.Base64Encoding);

                // CurrentKey: Blank
                // PreviousKey: Blank
                var connectionStringDecrypter = CreateXmlFileDriver(NotDeployedFilePath);

                // All test should not decrypt because we have no keys.
                DBConnectionString resultConnectionString;
                bool decryptionSuccessful = connectionStringDecrypter.TryDecryptConnectionString(ConnectionStringPlainText, out resultConnectionString);
                Assert.That(!decryptionSuccessful);

                decryptionSuccessful = connectionStringDecrypter.TryDecryptConnectionString(ConnectionStringEncryptedOldKey, out resultConnectionString);
                Assert.That(!decryptionSuccessful);

                decryptionSuccessful = connectionStringDecrypter.TryDecryptConnectionString(ConnectionStringEncryptedNewKey, out resultConnectionString);
                Assert.That(!decryptionSuccessful);

                decryptionSuccessful = connectionStringDecrypter.TryDecryptConnectionString(ConnectionStringUnknownKey, out resultConnectionString);
                Assert.That(!decryptionSuccessful);
            }
        }

        [Test]
        public void KeyUnchangedFileTest()
        {
            using (var helper = new FoolHelper())
            {
                if (!helper.RegisterMockDriverFactory<IConnectionStringDecryptionDriverFactory>(new MockConnectionStringDecryptionDriverFactory()))
                {
                    return;
                }

                helper.RegisterRealDrivers(FoolHelper.DriverType.FileSystem, FoolHelper.DriverType.Base64Encoding);

                // CurrentKey: Old
                // PreviousKey: Blank
                var connectionStringDecrypter = CreateXmlFileDriver(NotChangedFilePath);

                // Decryption failure using the current key, previous key missing.
                DBConnectionString resultConnectionString;
                bool decryptionSuccessful = connectionStringDecrypter.TryDecryptConnectionString(ConnectionStringPlainText, out resultConnectionString);
                Assert.That(!decryptionSuccessful);

                // Successful decryption using the current key.
                decryptionSuccessful = connectionStringDecrypter.TryDecryptConnectionString(ConnectionStringEncryptedOldKey, out resultConnectionString);
                Assert.That(decryptionSuccessful);
                Assert.AreEqual(ConnectionStringPlainText.Value, resultConnectionString.Value);

                // Decryption failure using the current key, previous key missing.
                decryptionSuccessful = connectionStringDecrypter.TryDecryptConnectionString(ConnectionStringEncryptedNewKey, out resultConnectionString);
                Assert.That(!decryptionSuccessful);

                // Fail to decrypt both attempts.
                decryptionSuccessful = connectionStringDecrypter.TryDecryptConnectionString(ConnectionStringUnknownKey, out resultConnectionString);
                Assert.That(!decryptionSuccessful);
            }
        }

        [Test]
        public void KeyChangedFileTest()
        {
            using (var helper = new FoolHelper())
            {
                if (!helper.RegisterMockDriverFactory<IConnectionStringDecryptionDriverFactory>(new MockConnectionStringDecryptionDriverFactory()))
                {
                    return;
                }

                helper.RegisterRealDrivers(FoolHelper.DriverType.FileSystem, FoolHelper.DriverType.Base64Encoding);

                // CurrentKey: New
                // PreviousKey: Old
                var connectionStringDecrypter = CreateXmlFileDriver(ChangedFilePath);

                // Fail to decrypt both attempts.
                DBConnectionString resultConnectionString;
                bool decryptionSuccessful = connectionStringDecrypter.TryDecryptConnectionString(ConnectionStringPlainText, out resultConnectionString);
                Assert.That(!decryptionSuccessful);

                // Successful decryption using previous key.
                decryptionSuccessful = connectionStringDecrypter.TryDecryptConnectionString(ConnectionStringEncryptedOldKey, out resultConnectionString);
                Assert.That(decryptionSuccessful);
                Assert.AreEqual(ConnectionStringPlainText.Value, resultConnectionString.Value);

                // Should be a decryption failure on the current, falling through
                // to success on the second key.
                decryptionSuccessful = connectionStringDecrypter.TryDecryptConnectionString(ConnectionStringEncryptedNewKey, out resultConnectionString);
                Assert.That(decryptionSuccessful);
                Assert.AreEqual(ConnectionStringPlainText.Value, resultConnectionString.Value);

                // Fail to decrypt both attempts.
                decryptionSuccessful = connectionStringDecrypter.TryDecryptConnectionString(ConnectionStringUnknownKey, out resultConnectionString);
                Assert.That(!decryptionSuccessful);
            }
        }
    }
}
