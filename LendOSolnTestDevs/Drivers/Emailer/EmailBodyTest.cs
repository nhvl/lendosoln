﻿namespace LendingQB.Test.Developers.Drivers.Emailer
{
    using LqbGrammar.Drivers.Emailer;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public sealed class EmailBodyTest
    {
        [Test]
        public void TestEmpty()
        {
            var body = new EmailTextBody();
            body.AppendText("hello");
            body.AppendText(string.Empty);
            Assert.AreEqual("hello", body.ToString());
        }

        [Test]
        public void TestSampleUserInput()
        {
            string sampleInput = "Borrower O'Neil has fico of 700+, his # is 321-322-2222.";

            var body = new EmailTextBody();
            body.AppendText(sampleInput);

            Assert.AreEqual(sampleInput, body.ToString());
        }
    }
}
