﻿namespace LendingQB.Test.Developers.Drivers.Emailer
{
    using LqbGrammar;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers.Emailer;
    using NUnit.Framework;
    using Utils;

    [TestFixture]
    [Category(CategoryConstants.IntegrationTest)]
    [Ignore("VERIFIED")]
    public sealed class TestSend
    {
        private const string Server = "smtp.meridianlink.com";
        private const string From = "aland@lendingqb.com";
        private const string To = "aland@meridianlink.com";

        private const string Subject = "TEST EMAIL - Please Ignore";

        [Test]
        public void SendPdfFileAttachment()
        {
            var info = new System.IO.FileInfo(@"c:\Temp\firstDigraph.txt.pdf");
            var attachment = TypedResource<System.IO.FileInfo>.CreateFile(info, MimeType.Application.Pdf.ToString(), "PDF");

            var email = CreateEmail("This email should contain an attachment generated from FileInfo for a pdf file.");
            email.AddAttachment(attachment);
            SendEmail(email);
        }

        [Test]
        public void SendCsvFileAttachment()
        {
            var info = new System.IO.FileInfo(@"c:\Temp\Titles.txt");
            var attachment = TypedResource<System.IO.FileInfo>.CreateFile(info, MimeType.Text.Csv.ToString(), "CSV");

            var email = CreateEmail("This email should contain an attachment generated from FileInfo for a csv file.");
            email.AddAttachment(attachment);
            SendEmail(email);
        }

        [Test]
        public void SendStringAttachment()
        {
            var attachment = TypedResource<string>.CreateText("Text attachment should come across as ok.", MimeType.Text.Plain.ToString(), "TEXT");

            var email = CreateEmail("This email should contain an attachment generated from a string.");
            email.AddAttachment(attachment);
            SendEmail(email);
        }

        [Test]
        public void SendByteArrayAttachment()
        {
            var bytes = System.IO.File.ReadAllBytes(@"c:\Temp\firstDigraph.txt.pdf");
            var attachment = TypedResource<byte[]>.CreateBinary(bytes, MimeType.Application.Pdf.ToString(), "BYTES");

            var email = CreateEmail("This email should contain an attachment generated from a byte array.");
            email.AddAttachment(attachment);
            SendEmail(email);
        }

        private bool SendEmail(EmailPackage email)
        {
            var server = EmailServerName.Create("smtp.meridianlink.com");
            var port = PortNumber.Create(25);
            if (server == null)
            {
                return false;
            }

            using (var helper = new FoolHelper())
            {
                if (!helper.RegisterRealDriver(FoolHelper.DriverType.EmailSender))
                {
                    return false;
                }

                var factory = GenericLocator<IEmailDriverFactory>.Factory;
                var emailer = factory.Create(server.Value, port.Value);
                emailer.SendEmail(email);

                return true;
            }
        }

        private EmailPackage CreateEmail(string text)
        {
            var to = EmailAddress.Create(To);
            if (to == null)
            {
                return null;
            }

            var from = EmailAddress.Create(From);
            if (from == null)
            {
                return null;
            }

            var subject = EmailSubject.Create(Subject);
            if (subject == null)
            {
                return null;
            }

            var body = new EmailTextBody();
            body.AppendText(text);

            return new EmailPackage(LendersOffice.Constants.ConstAppDavid.SystemBrokerGuid, from.Value, subject.Value, body, to.Value);
        }
    }
}
