﻿namespace LendingQB.Test.Developers.Drivers.Emailer
{
    using System;
    using System.Collections.Generic;
    using LendersOffice.Constants;
    using LendersOffice.Drivers.Emailer;
    using LqbGrammar;
    using LqbGrammar.Adapters;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers.Emailer;
    using NUnit.Framework;
    using Utils;

    [TestFixture]
    public sealed class TestReceive
    {
        [Test]
        [Category(CategoryConstants.UnitTest)]
        [Category(CategoryConstants.LqbMocks)] // using a different sort of mock in that the test message is going to be used which may interfere with the website if run via LUnit.
        [Category(CategoryConstants.ExpectFailureOnLocalhost)]
        [Ignore("Tests in LendOSolnTestDevs are expected to be executed on localhost so there is no point in running this.")]
        public void ReceiveTestMessage()
        {
            using (var helper = new FoolHelper())
            {
                if (!helper.RegisterRealDriver(FoolHelper.DriverType.EmailReceiver))
                {
                    return;
                }

                // Make sure we are dealing with the testable adapter
                var adapterFactory = GenericLocator<IReceiveEmailAdapterFactory>.Factory;
                var testFactory = adapterFactory as global::Adapter.Emailer.OpenPop3AdapterFactory;
                if (testFactory == null)
                {
                    return;
                }

                // Force use of the test message
                testFactory.UseTestMessage = true;

                var receiver = EmailReceiverDriverFactory.CreateEmailReceiver(EmailServerName.BadName, "fakeuser", "fakepassword");
                int count = receiver.Count;
                Assert.AreEqual(1, count);

                var identifier = receiver.GetIdentifier(0);
                Assert.AreEqual("123456789", identifier.ToString());

                var email = receiver.GetEmail(0);
                bool isARS = email.IsAutoResponseSuppressed;
                Assert.IsTrue(isARS);

                var sent = email.DateSent;
                string sentStr = sent.ToString("O");
                var sentDT = DateTime.Parse(sentStr, null, System.Globalization.DateTimeStyles.RoundtripKind);
                var now = DateTime.UtcNow;
                var diff = now - sentDT;
                Assert.IsTrue(diff.TotalMinutes > 10);
                Assert.IsTrue(diff.TotalMinutes < 11);

                byte[] rawBytes = email.RawMessage;
                string raw = System.Text.Encoding.ASCII.GetString(rawBytes);
                Assert.AreEqual("FAKE EMAIL RAW MESSAGE", raw);

                var package = email.Contents;
                string from = package.From.ToString();
                Assert.AreEqual("from@meridianlink.com", from);

                string to = package.To[0].ToString();
                Assert.AreEqual("to@meridianlink.com", to);

                string cc = package.Cc[0].ToString();
                Assert.AreEqual("cc@meridianlink.com", cc);

                string bcc = package.Bcc[0].ToString();
                Assert.AreEqual("bcc@meridianlink.com", bcc);

                string subject = package.Subject.ToString();
                Assert.AreEqual("FAKE SUBJECT", subject);

                string body = package.Body.ToString();
                Assert.AreEqual("FAKE EMAIL BODY", body);

                var file = package.BinaryAttachments[0];
                Assert.AreEqual("FakeFile.pdf", file.Name);
                Assert.AreEqual("application/pdf", file.MimeType);

                string text = System.Text.Encoding.ASCII.GetString(file.Data);
                Assert.AreEqual("FAKE FILE CONTENT", text);
            }
        }

        [Test]
        [Category(CategoryConstants.IntegrationTest)]
        [Ignore("VERIFIED")]
        public void LivePull()
        {
            using (var helper = new FoolHelper())
            {
                if (!helper.RegisterRealDrivers(FoolHelper.DriverType.EmailSender, FoolHelper.DriverType.EmailReceiver))
                {
                    return;
                }

                string receiver = ConstStage.EmailReceivingServer;
                string address = ConstStage.TaskAliasEmailAddress;
                ////string password = ConstStage.TaskEmailPassword;
                string password = "<REMOVED>";

                var content = "TEST - PLEASE IGNORE - " + Guid.NewGuid().ToString("N");
                bool sent = SendEmail(address, content);
                Assert.IsTrue(sent);

                var server = EmailServerName.Create(receiver).Value;

                var factory = GenericLocator<IReceiveEmailDriverFactory>.Factory;

                int count = 0;
                var timeout = DateTime.Now.AddMinutes(10);

                while (count == 0)
                {
                    using (var driver = factory.Create(server, address, password))
                    {
                        count = driver.Count;
                        if (count == 0)
                        {
                            if (DateTime.Now > timeout)
                            {
                                break;
                            }

                            System.Threading.Thread.Sleep(1000);
                            continue;
                        }

                        var list = new List<int>();
                        for (int i = 0; i < count; ++i)
                        {
                            var data = driver.GetEmail(i);
                            var email = data.Contents;
                            if (email.From.ToString() == "aland@lendingqb.com")
                            {
                                // don't bother checking the body since the system adds spurious CRLFs
                                list.Add(i);
                                Assert.AreEqual(1, email.BinaryAttachments.Count);
                            }
                        }

                        Assert.IsTrue(list.Count > 0);

                        list.Reverse();
                        foreach (int index in list)
                        {
                            driver.DeleteEmail(index);
                        }
                    }
                }

                Assert.IsTrue(count > 0);
            }
        }

        private bool SendEmail(string address, string content)
        {
            var server = EmailServerName.Create("smtp.meridianlink.com");
            var port = PortNumber.Create(25);
            //var port = PortNumber.Create(110); // I'm not sure why this is here, but it causes failure on developer machine.
            if (server == null)
            {
                return false;
            }

            var factory = GenericLocator<IEmailDriverFactory>.Factory;
            var emailer = factory.Create(server.Value, port.Value);

            var to = EmailAddress.Create(address);
            if (to == null)
            {
                return false;
            }

            var subject = EmailSubject.Create(content);
            if (subject == null)
            {
                return false;
            }

            var body = new EmailTextBody();
            body.AppendText(content);

            var from = EmailAddress.Create("aland@lendingqb.com");

            var info = new System.IO.FileInfo(@"c:\Temp\firstDigraph.txt.pdf");
            var attachment = TypedResource<System.IO.FileInfo>.CreateFile(info, MimeType.Application.Pdf.ToString(), "PDF");

            EmailPackage email = new EmailPackage(ConstAppDavid.SystemBrokerGuid, from.Value, subject.Value, body, to.Value);
            email.AddAttachment(attachment);
            emailer.SendEmail(email);

            return true;
        }
    }
}
