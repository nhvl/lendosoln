﻿namespace LendingQB.Test.Developers.Drivers.FileDB
{
    using System;
    using LqbGrammar;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;
    using NUnit.Framework;
    using Utils;
    using global::DataAccess;

    [TestFixture]
    [Category(CategoryConstants.IntegrationTest)]
    public class FileDbDriver
    {
        private const string CONTENT = "THIS IS A TEST STRING!$%^#";

        [Test]
        public void SaveAndRetrieveNewFile()
        {
            using (var helper = new FoolHelper())
            {
                if (!helper.RegisterRealDriver(FoolHelper.DriverType.FileDB))
                {
                    return;
                }

                var fileType = E_FileDB.Temp;
                string type = FileDBTools.GetDatabaseName(fileType);
                var location = FileStorageIdentifier.TryParse(type).Value;
                var key = FileIdentifier.TryParse(Guid.NewGuid().ToString()).Value;

                try
                {
                    var driver = GetDriver();

                    SaveFile(driver, location, key);
                    string data = RetrieveFile(driver, location, key);

                    Assert.AreEqual(CONTENT, data);
                }
                finally
                {
                    this.DeleteFile(key);
                }
            }
        }

        [Test]
        [Ignore]
        public void CopyFile()
        {
            using (var helper = new FoolHelper())
            {
                if (!helper.RegisterRealDriver(FoolHelper.DriverType.FileDB))
                {
                    return;
                }

                var fileType = E_FileDB.Temp;
                string type = FileDBTools.GetDatabaseName(fileType);
                var location = FileStorageIdentifier.TryParse(type).Value;
                var key = FileIdentifier.TryParse(Guid.NewGuid().ToString()).Value;

                try
                {
                    var driver = GetDriver();

                    SaveFile(driver, location, key);

                    string data = string.Empty;
                    Action<IFileToken> useHandler = delegate (IFileToken token)
                    {
                        string guid = Guid.NewGuid().ToString();
                        var newKey = FileIdentifier.TryParse(guid).Value;

                        driver.ReSaveFile(location, newKey, token);
                        data = RetrieveFile(driver, location, newKey);

                        DeleteFile(newKey);
                    };

                    driver.UseFile(location, key, useHandler);
                    Assert.AreEqual(CONTENT, data);
                }
                finally
                {
                    this.DeleteFile(key);
                }

            }
        }

        private void DeleteFile(FileIdentifier key)
        {
            FileDBTools.Delete(E_FileDB.Temp, key.ToString());
        }

        private void SaveFile(IFileDbDriver driver, FileStorageIdentifier location, FileIdentifier key)
        {
            Action<LocalFilePath> saveHandler = delegate(LocalFilePath path)
            {
                SaveFile(path.Value, CONTENT);
            };

            driver.SaveNewFile(location, key, saveHandler);
        }

        private string RetrieveFile(IFileDbDriver driver, FileStorageIdentifier location, FileIdentifier key)
        {
            string data = null;
            Action<LocalFilePath> readHandler = delegate(LocalFilePath path)
            {
                data = ReadFile(path.Value);
            };

            driver.RetrieveFile(location, key, readHandler);
            return data;
        }

        private void SaveFile(string path, string data)
        {
            using (var writer = System.IO.File.CreateText(path))
            {
                writer.Write(data);
            }
        }

        private string ReadFile(string path)
        {
            using (var reader = System.IO.File.OpenText(path))
            {
                return reader.ReadToEnd();
            }
        }

        private IFileDbDriver GetDriver()
        {
            IFileDbDriverFactory factory = GenericLocator<IFileDbDriverFactory>.Factory;
            return factory.Create();
        }
    }
}
