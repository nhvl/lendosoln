﻿namespace LendingQB.Test.Developers.Drivers.FileDB
{
    using System;
    using global::Adapter;
    using global::DataAccess;
    using LendersOffice.Drivers.Base64Encoding;
    using LendersOffice.Drivers.ConnectionStringDecryption;
    using LendersOffice.Drivers.DataEncryption;
    using LendersOffice.Drivers.Encryption;
    using LendersOffice.Drivers.FileDB;
    using LendersOffice.Drivers.FileSystem;
    using LendersOffice.Drivers.SqlServerDB;
    using LqbGrammar.Adapters;
    using LqbGrammar.Drivers;
    using LqbGrammar.Drivers.FileSystem;
    using LqbGrammar.Queries;
    using LqbGrammar.Exceptions;
    using NUnit.Framework;
    using Utils;

    [TestFixture]
    [Category(CategoryConstants.IntegrationTest)]
    public class EncryptionControl
    {
        const string FileContents = "It was the best of times, it was the worst of times.";

        private string key;

        [SetUp]
        public void SetUp()
        {
            this.key = Guid.NewGuid().ToString("D").Replace("-", string.Empty);
        }

        [TearDown]
        public void TearDown()
        {
            var configFactory = this.BuildMockConfigFactory(false);
            using (var helper = new FoolHelper())
            {
                this.RegisterRealDrivers(helper);
                helper.RegisterMockDriverFactory<IConfigurationQueryFactory>(configFactory);

                FileDBTools.Delete(E_FileDB.Temp, key);
            }
        }

        [Test]
        public void SaveWithOldCode_RetrieveWithOldCode_Works()
        {
            var configFactory = this.BuildMockConfigFactory(false);
            using (var helper = new FoolHelper())
            {
                helper.RegisterMockDriverFactory<IConfigurationQueryFactory>(configFactory);

                FileDBTools.WriteData(E_FileDB.Temp, this.key, FileContents);

                string recoveredContents = FileDBTools.ReadDataText(E_FileDB.Temp, this.key);
                Assert.AreEqual(FileContents, recoveredContents);
            }
        }

        [Test]
        public void SaveWithOldCode_RetrieveWithNewCode_Works()
        {
            var configFactory = this.BuildMockConfigFactory(false);
            using (var helper = new FoolHelper())
            {
                helper.RegisterMockDriverFactory<IConfigurationQueryFactory>(configFactory);

                FileDBTools.WriteData(E_FileDB.Temp, this.key, FileContents);
            }

            configFactory = this.BuildMockConfigFactory(true);
            using (var helper = new FoolHelper())
            {
                this.RegisterRealDrivers(helper);
                helper.RegisterMockDriverFactory<IConfigurationQueryFactory>(configFactory);

                string recoveredContents = FileDBTools.ReadDataText(E_FileDB.Temp, this.key);
                Assert.AreEqual(FileContents, recoveredContents);
            }
        }

        [Test]
        public void SaveWithNewCode_RetrieveWithOldCode_Throws()
        {
            var configFactory = this.BuildMockConfigFactory(true);
            using (var helper = new FoolHelper())
            {
                this.RegisterRealDrivers(helper);
                helper.RegisterMockDriverFactory<IConfigurationQueryFactory>(configFactory);

                FileDBTools.WriteData(E_FileDB.Temp, this.key, FileContents);
            }

            configFactory = this.BuildMockConfigFactory(false);
            using (var helper = new FoolHelper())
            {
                helper.RegisterMockDriverFactory<IConfigurationQueryFactory>(configFactory);

                Assert.Throws<ApplicationException>(() => FileDBTools.ReadDataText(E_FileDB.Temp, this.key));
            }
        }

        [Test]
        public void SaveWithNewCode_RetrieveWithNewCode_Works()
        {
            var configFactory = this.BuildMockConfigFactory(true);
            using (var helper = new FoolHelper())
            {
                this.RegisterRealDrivers(helper);
                helper.RegisterMockDriverFactory<IConfigurationQueryFactory>(configFactory);

                FileDBTools.WriteData(E_FileDB.Temp, this.key, FileContents);

                string recoveredContents = FileDBTools.ReadDataText(E_FileDB.Temp, this.key);
                Assert.AreEqual(FileContents, recoveredContents);
            }
        }

        [Test]
        public void SaveNoEncryptFlag_RetrieveWithOldCode_WorksToDemonstrateFileNotEncrypted()
        {
            var configFactory = this.BuildMockConfigFactory(true, false);
            using (var helper = new FoolHelper())
            {
                this.RegisterRealDrivers(helper);
                helper.RegisterMockDriverFactory<IConfigurationQueryFactory>(configFactory);

                FileDBTools.WriteData(E_FileDB.Temp, this.key, FileContents);
            }

            configFactory = this.BuildMockConfigFactory(false);
            using (var helper = new FoolHelper())
            {
                this.RegisterRealDrivers(helper);
                helper.RegisterMockDriverFactory<IConfigurationQueryFactory>(configFactory);

                string recoveredContents = FileDBTools.ReadDataText(E_FileDB.Temp, this.key);
                Assert.AreEqual(FileContents, recoveredContents);
            }
        }

        [Test]
        public void SaveWithEncryptFlag_RetrieveWithOldCode_ThrowsToDemonstrateFileIsEncrypted()
        {
            var configFactory = this.BuildMockConfigFactory(true, true);
            using (var helper = new FoolHelper())
            {
                this.RegisterRealDrivers(helper);
                helper.RegisterMockDriverFactory<IConfigurationQueryFactory>(configFactory);

                FileDBTools.WriteData(E_FileDB.Temp, this.key, FileContents);
            }

            configFactory = this.BuildMockConfigFactory(false);
            using (var helper = new FoolHelper())
            {
                this.RegisterRealDrivers(helper);
                helper.RegisterMockDriverFactory<IConfigurationQueryFactory>(configFactory);

                Assert.Throws<ApplicationException>(() => FileDBTools.ReadDataText(E_FileDB.Temp, this.key));
            }
        }

        [Test]
        public void SaveNoEncryptFlag_RetrieveNoEncryptFlag_Works()
        {
            var configFactory = this.BuildMockConfigFactory(true, false);
            using (var helper = new FoolHelper())
            {
                this.RegisterRealDrivers(helper);
                helper.RegisterMockDriverFactory<IConfigurationQueryFactory>(configFactory);

                FileDBTools.WriteData(E_FileDB.Temp, this.key, FileContents);

                string recoveredContents = FileDBTools.ReadDataText(E_FileDB.Temp, this.key);
                Assert.AreEqual(FileContents, recoveredContents);
            }
        }

        [Test]
        public void SaveNoEncryptFlag_RetrieveWithEncryptFlag_Works()
        {
            var configFactory = this.BuildMockConfigFactory(true, false);
            using (var helper = new FoolHelper())
            {
                this.RegisterRealDrivers(helper);
                helper.RegisterMockDriverFactory<IConfigurationQueryFactory>(configFactory);

                FileDBTools.WriteData(E_FileDB.Temp, this.key, FileContents);
            }

            configFactory = this.BuildMockConfigFactory(true, true);
            using (var helper = new FoolHelper())
            {
                this.RegisterRealDrivers(helper);
                helper.RegisterMockDriverFactory<IConfigurationQueryFactory>(configFactory);

                string recoveredContents = FileDBTools.ReadDataText(E_FileDB.Temp, this.key);
                Assert.AreEqual(FileContents, recoveredContents);
            }
        }

        [Test]
        public void SaveWithEncryptFlag_RetrieveWithNoEncryptFlag_Works()
        {
            var configFactory = this.BuildMockConfigFactory(true, true);
            using (var helper = new FoolHelper())
            {
                this.RegisterRealDrivers(helper);
                helper.RegisterMockDriverFactory<IConfigurationQueryFactory>(configFactory);

                FileDBTools.WriteData(E_FileDB.Temp, this.key, FileContents);
            }

            configFactory = this.BuildMockConfigFactory(true, false);
            using (var helper = new FoolHelper())
            {
                this.RegisterRealDrivers(helper);
                helper.RegisterMockDriverFactory<IConfigurationQueryFactory>(configFactory);

                string recoveredContents = FileDBTools.ReadDataText(E_FileDB.Temp, this.key);
                Assert.AreEqual(FileContents, recoveredContents);
            }
        }

        [Test]
        public void SaveWithEncryptFlag_RetrieveWithEncryptFlag_Works()
        {
            var configFactory = this.BuildMockConfigFactory(true, true);
            using (var helper = new FoolHelper())
            {
                this.RegisterRealDrivers(helper);
                helper.RegisterMockDriverFactory<IConfigurationQueryFactory>(configFactory);

                FileDBTools.WriteData(E_FileDB.Temp, this.key, FileContents);

                string recoveredContents = FileDBTools.ReadDataText(E_FileDB.Temp, this.key);
                Assert.AreEqual(FileContents, recoveredContents);
            }
        }

        private void RegisterRealDrivers(FoolHelper helper)
        {
            helper.RegisterRealDriver(FoolHelper.DriverType.FileDB);
        }

        private IConfigurationQueryFactory BuildMockConfigFactory(bool newFileDb, bool filedbEncryption = true)
        {
            var configUtil = new ConfigUtil();
            configUtil.AddStageConfigValue("UseNewFileDb", newFileDb ? 1 : 0);
            configUtil.AddStageConfigValue("FileDBSiteCodeForTempFile", "LOBETA_TEMP");
            if (newFileDb)
            {
                configUtil.AddStageConfigValue("UseFileDbEncryption", filedbEncryption ? 1 : 0);
            }

            return configUtil.BuildMockConfigFactory();
        }
    }
}
