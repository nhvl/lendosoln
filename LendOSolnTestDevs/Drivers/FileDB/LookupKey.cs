﻿namespace LendingQB.Test.Developers.Drivers.FileDB
{
    using System;
    using global::DataAccess;
    using LendersOffice.Drivers.FileDB;
    using LqbGrammar.DataTypes;
    using NUnit.Framework;
    using Utils;

    [TestFixture]
    [Category(CategoryConstants.IntegrationTest)]
    public class LookupKey
    {
        [Test]
        public void CalculateLookup()
        {
            const int ITERATIONS = 1000;

            using (var helper = new FoolHelper())
            {
                if (!helper.RegisterRealDriver(FoolHelper.DriverType.FileDB))
                {
                    return;
                }

                string dbname = FileDBTools.GetDatabaseName(E_FileDB.Temp);
                var location = FileStorageIdentifier.TryParse(dbname).Value;

                int min = 0x10000;
                int max = -1;
                for (int i = 0; i < ITERATIONS; ++i)
                {
                    string fileid = Guid.NewGuid().ToString();
                    var key = FileIdentifier.TryParse(fileid).Value;

                    int lookup = FileDbEncryptionKeyLookup.CalculateLookup(location, key);
                    if (lookup < min) min = lookup;
                    if (lookup > max) max = lookup;
                    Assert.IsTrue(lookup >= 0);
                    Assert.IsTrue(lookup <= 0xFFFF);
                }

                Assert.IsTrue(min < 0x10000);
                Assert.IsTrue(max > -1);
                Assert.AreNotEqual(min, max); // ensure that not all results are the same
            }
        }

        [Test]
        [ExpectedException(typeof(LqbGrammar.Exceptions.DeveloperException))]
        public void PrimaryKeyRange_Low()
        {
            using (var helper = new FoolHelper())
            {
                if (!helper.RegisterRealDriver(FoolHelper.DriverType.FileDB))
                {
                    return;
                }

                int low = -1;
                var key = FileDbEncryptionKeyRetriever.RetrieveKey(low);
            }
        }

        [Test]
        [ExpectedException(typeof(LqbGrammar.Exceptions.DeveloperException))]
        public void PrimaryKeyRange_High()
        {
            using (var helper = new FoolHelper())
            {
                if (!helper.RegisterRealDriver(FoolHelper.DriverType.FileDB))
                {
                    return;
                }

                int high = 0x10000;
                var key = FileDbEncryptionKeyRetriever.RetrieveKey(high);
            }
        }

        [Test]
        [Ignore("Data is in test DB but probably not in production DB so the behavior is indeterminate right now.")]
        public void PrimaryKeyRange_Boundary_Low()
        {
            using (var helper = new FoolHelper())
            {
                if (!helper.RegisterRealDriver(FoolHelper.DriverType.FileDB))
                {
                    return;
                }

                int low = 0;
                var key = FileDbEncryptionKeyRetriever.RetrieveKey(low);
                Assert.IsNull(key); // null because no data in database yet
            }
        }

        [Test]
        [Ignore("Data is in test DB but probably not in production DB so the behavior is indeterminate right now.")]
        public void PrimaryKeyRange_Boundary_High()
        {
            using (var helper = new FoolHelper())
            {
                if (!helper.RegisterRealDriver(FoolHelper.DriverType.FileDB))
                {
                    return;
                }

                int high = 0xFFFF;
                var key = FileDbEncryptionKeyRetriever.RetrieveKey(high);
                Assert.IsNull(key); // null because no data in database yet
            }
        }
    }
}
