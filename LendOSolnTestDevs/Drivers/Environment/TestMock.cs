﻿namespace LendingQB.Test.Developers.Drivers.Environment
{
    using System;
    using LendersOffice.Drivers.Environment;
    using LqbGrammar;
    using LqbGrammar.Adapters;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;
    using NUnit.Framework;
    using Utils;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    [Category(CategoryConstants.LqbMocks)]
    public sealed class TestMock
    {
        [Test]
        public void FirstHalfProperiesMatch()
        {
            var data = new MockEnvironmentData();
            data.CurrentDirectory = "directory";
            data.MachineName = MachineName.Create("DEVWRK").Value;

            using (var helper = new FoolHelper())
            {
                if (!helper.RegisterRealDriver(FoolHelper.DriverType.Environment)) // Mock driver uses real adapter!
                {
                    return;
                }

                var factory = new MockEnvironmentDriverFactory(data);
                helper.RegisterMockDriverFactory<IEnvironmentDriverFactory>(factory);

                string dir = EnvironmentHelper.CurrentDirectory;
                string machine = EnvironmentHelper.MachineName.ToString();
                string newline = EnvironmentHelper.NewLine;
                int count = EnvironmentHelper.ProcessorCount;

                Assert.AreEqual("directory", dir);
                Assert.AreEqual("DEVWRK", machine);
                Assert.AreEqual(Environment.NewLine, newline);
                Assert.AreEqual(Environment.ProcessorCount, count);
            }

            var adapter = GenericLocator<IEnvironmentAdapterFactory>.Factory;
            Assert.IsNull(adapter);
        }

        [Test]
        public void SecondHalfProperiesMatch()
        {
            var data = new MockEnvironmentData();
            data.ProcessorCount = 666;

            using (var helper = new FoolHelper())
            {
                if (!helper.RegisterRealDriver(FoolHelper.DriverType.Environment)) // Mock driver uses real adapter!
                {
                    return;
                }

                var factory = new MockEnvironmentDriverFactory(data);
                helper.RegisterMockDriverFactory<IEnvironmentDriverFactory>(factory);

                string dir = EnvironmentHelper.CurrentDirectory;
                string machine = EnvironmentHelper.MachineName.ToString();
                int count = EnvironmentHelper.ProcessorCount;

                Assert.AreEqual(Environment.CurrentDirectory, dir);
                Assert.AreEqual(Environment.MachineName, machine);
                Assert.AreEqual(666, count);
            }

            var adapter = GenericLocator<IEnvironmentAdapterFactory>.Factory;
            Assert.IsNull(adapter);
        }
    }
}
