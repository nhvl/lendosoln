﻿namespace LendingQB.Test.Developers.Drivers.Environment
{
    using System;
    using LendersOffice.Drivers.Environment;
    using NUnit.Framework;
    using Utils;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public sealed class TestDriver
    {
        [Test]
        public void AllPropertiesMatch()
        {
            using (var helper = new FoolHelper())
            {
                if (!helper.RegisterRealDriver(FoolHelper.DriverType.Environment))
                {
                    return;
                }

                string dir = EnvironmentHelper.CurrentDirectory;
                string machine = EnvironmentHelper.MachineName.ToString();
                string newline = EnvironmentHelper.NewLine;
                int count = EnvironmentHelper.ProcessorCount;

                Assert.AreEqual(Environment.CurrentDirectory, dir);
                Assert.AreEqual(Environment.MachineName, machine);
                Assert.AreEqual(Environment.NewLine, newline);
                Assert.AreEqual(Environment.ProcessorCount, count);
            }
        }
    }
}
