﻿namespace LendingQB.Test.Developers.Drivers.Logger
{
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public sealed class DataTypes
    {
        [Test]
        public void LogMessage()
        {
            // Allow null
            var message = LqbGrammar.DataTypes.LogMessage.Create(null);
            Assert.IsTrue(message != null);

            // Allow empty
            message = LqbGrammar.DataTypes.LogMessage.Create(string.Empty);
            Assert.IsTrue(message != null);

            // Allow anything
            message = LqbGrammar.DataTypes.LogMessage.Create("<script>var i = 0;</script>");
            Assert.IsTrue(message != null);
        }

        [Test]
        public void LogPropertyName()
        {
            // Disallow null
            var name = LqbGrammar.DataTypes.LogPropertyName.Create(null);
            Assert.IsTrue(name == null);

            // Disallow empty
            name = LqbGrammar.DataTypes.LogPropertyName.Create(null);
            Assert.IsTrue(name == null);

            // Disallow symbols
            name = LqbGrammar.DataTypes.LogPropertyName.Create("Call-Up");
            Assert.IsTrue(name == null);

            // Confirm values actually in use
            name = LqbGrammar.DataTypes.LogPropertyName.Create("RequestUniqueId");
            Assert.IsTrue(name != null);

            // Confirm values actually in use
            name = LqbGrammar.DataTypes.LogPropertyName.Create("Category");
            Assert.IsTrue(name != null);

            // Confirm values actually in use
            name = LqbGrammar.DataTypes.LogPropertyName.Create("sLId");
            Assert.IsTrue(name != null);

            // Confirm values actually in use
            name = LqbGrammar.DataTypes.LogPropertyName.Create("CorrelationId");
            Assert.IsTrue(name != null);

            // Confirm values actually in use
            name = LqbGrammar.DataTypes.LogPropertyName.Create("ClientIp");
            Assert.IsTrue(name != null);

            // Confirm values actually in use
            name = LqbGrammar.DataTypes.LogPropertyName.Create("UniqueClientIdCookie");
            Assert.IsTrue(name != null);

            // Confirm values actually in use
            name = LqbGrammar.DataTypes.LogPropertyName.Create("error_unique_id");
            Assert.IsTrue(name != null);
        }

        [Test]
        public void LogPropertyValue()
        {
            // Allow null
            var value = LqbGrammar.DataTypes.LogPropertyValue.Create(null);
            Assert.IsTrue(value != null);

            // Allow empty
            value = LqbGrammar.DataTypes.LogPropertyValue.Create(string.Empty);
            Assert.IsTrue(value != null);

            // Allow anything
            value = LqbGrammar.DataTypes.LogPropertyValue.Create("<script>var i = 0;</script>");
            Assert.IsTrue(value != null);
        }
    }
}
