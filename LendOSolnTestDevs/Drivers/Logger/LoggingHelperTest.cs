﻿namespace LendingQB.Test.Developers.Drivers.Logger
{
    using System.Collections.Generic;
    using LqbGrammar;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;
    using LendersOffice.Drivers.Logger;
    using NUnit.Framework;
    using Utils;

    [TestFixture]
    public sealed class LoggingHelperTest
    {
        [Test]
        [Category(CategoryConstants.UnitTest)]
        [Category(CategoryConstants.LqbMocks)]
        public void LogError()
        {
            const string testMessage = "THIS IS A TEST, THIS IS ONLY A TEST";
            const string propName = "Australia";
            const string propValue = "Six cases of Fosters Lager";

            using (var helper = new FoolHelper())
            {
                var factory = new MockLoggingDriverFactory();
                if (!helper.RegisterMockDriverFactory<ILoggingDriverFactory>(factory))
                {
                    return;
                }

                string good = @"FormatName:DIRECT=OS:ALANDWRK.meridianlink.com\private$\devlogmsmq";
                var path = MessageQueuePath.Create(good).Value;
                var target = LoggingHelper.CreateMsmqInfo(path);

                var message = LogMessage.Create(testMessage).Value;
                var name = LogPropertyName.Create(propName).Value;
                var value = LogPropertyValue.Create(propValue).Value;

                var dict = new Dictionary<LogPropertyName, LogPropertyValue>();
                dict[name] = value;

                LoggingHelper.Log(LendersOffice.Logging.LoggingLevel.Error, message, dict, target);

                Assert.AreEqual("Error", factory.LogType);
                Assert.AreEqual(testMessage, factory.Message);
                Assert.AreEqual(typeof(LoggingHelper).FullName, factory.BoundaryType);

                Assert.IsNotNull(factory.Properties);
                foreach (var key in factory.Properties.Keys)
                {
                    Assert.AreEqual(propName, key);
                    Assert.AreEqual(propValue, factory.Properties[key]);
                }
            }
        }

        [Test]
        [Category(CategoryConstants.IntegrationTest)]
        [Ignore("VERIFIED")]
        public void FullTest()
        {
            const string testMessage = "THIS IS A TEST, THIS IS ONLY A TEST";
            const string propName = "Australia";
            const string propValue = "Six cases of Fosters Lager";

            using (var helper = new FoolHelper())
            {
                if (!helper.RegisterRealDriver(FoolHelper.DriverType.Logger))
                {
                    return;
                }

                string good = @"FormatName:DIRECT=OS:ALANDWRK.meridianlink.com\private$\devlogmsmq";
                var path = MessageQueuePath.Create(good).Value;
                var msmqTarget = LoggingHelper.CreateMsmqInfo(path);

                var server = EmailServerName.Create("smtp.meridianlink.com").Value;
                var port = PortNumber.Create(25).Value;
                var emailFrom = EmailAddress.Create("aland@lendingqb.com").Value;
                var emailTo = EmailAddress.Create("aland@meridianlink.com").Value;
                var recipients = new List<EmailAddress>();
                recipients.Add(emailTo);
                var emailTarget = LoggingHelper.CreateEmailInfo(server, port, emailFrom, recipients);

                var message = LogMessage.Create(testMessage).Value;
                var name = LogPropertyName.Create(propName).Value;
                var value = LogPropertyValue.Create(propValue).Value;

                var dict = new Dictionary<LogPropertyName, LogPropertyValue>();
                dict[name] = value;

                LoggingHelper.Log(LendersOffice.Logging.LoggingLevel.Error, message, dict, emailTarget, msmqTarget);
            }
        }
    }
}
