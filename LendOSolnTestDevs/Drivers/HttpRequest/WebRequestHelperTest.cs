﻿namespace LendingQB.Test.Developers.Drivers.HttpRequest
{
    using System.Collections.Specialized;
    using System.IO;
    using System.Web;
    using System.Xml.Linq;
    using LendersOffice.Drivers.HttpRequest;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers.HttpRequest;
    using NUnit.Framework;
    using Utils;

    [TestFixture]
    [Category(CategoryConstants.IntegrationTest)]
    [Ignore("Getting 404 errors")]
    public sealed class WebRequestHelperTest
    {
        private const string URL = "http://localhost/LendersOfficeApp/LOAdmin/Test/DumpRequest.aspx";

        [Test]
        public void DefaultSettings()
        {
            using (var helper = new FoolHelper())
            {
                if (!helper.RegisterRealDriver(FoolHelper.DriverType.HttpRequest))
                {
                    return;
                }

                string path = null;

                try
                {
                    var options = new HttpRequestOptions();

                    WebRequestHelper.ExecuteCommunication(GetUrl(), options);
                    Assert.AreEqual(System.Net.HttpStatusCode.OK, options.ResponseStatusCode);

                    path = CleanPath(options.ResponseBody);
                    Assert.IsFalse(string.IsNullOrEmpty(path));

                    var data = ParseFile(path);
                    Assert.AreEqual("GET", data["HttpMethod"]);
                }
                finally
                {
                    DeleteTempFile(path);
                }
            }
        }

        [Test]
        public void PostString()
        {
            using (var helper = new FoolHelper())
            {
                if (!helper.RegisterRealDriver(FoolHelper.DriverType.HttpRequest))
                {
                    return;
                }

                string path = null;

                try
                {
                    string post = "Linkin Park";

                    var options = new HttpRequestOptions();
                    options.Method = HttpMethod.Post;
                    options.MimeType = MimeType.Text.Plain;
                    options.PostData = new StringContent(post);

                    WebRequestHelper.ExecuteCommunication(GetUrl(), options);
                    Assert.AreEqual(System.Net.HttpStatusCode.OK, options.ResponseStatusCode);

                    path = CleanPath(options.ResponseBody);
                    Assert.IsFalse(string.IsNullOrEmpty(path));

                    var data = ParseFile(path);
                    Assert.AreEqual("POST", data["HttpMethod"]);
                    Assert.AreEqual(MimeType.Text.Plain.ToString(), data["ContentType"]);
                    Assert.AreEqual(post, data["Body"]);
                }
                finally
                {
                    DeleteTempFile(path);
                }
            }
        }

        [Test]
        public void PostFormData()
        {
            using (var helper = new FoolHelper())
            {
                if (!helper.RegisterRealDriver(FoolHelper.DriverType.HttpRequest))
                {
                    return;
                }

                string path = null;

                try
                {
                    string post = string.Format("cartoon={0}&country={1}&bestfood={2}", HttpUtility.UrlEncode("Mickey Mouse"), HttpUtility.UrlEncode("United States"), HttpUtility.UrlEncode("Tommy's World Famous Burgers"));

                    var options = new HttpRequestOptions();
                    options.Method = HttpMethod.Post;
                    options.MimeType = MimeType.Application.UrlEncoded;
                    options.PostData = new StringContent(post);

                    WebRequestHelper.ExecuteCommunication(GetUrl(), options);
                    Assert.AreEqual(System.Net.HttpStatusCode.OK, options.ResponseStatusCode);

                    path = CleanPath(options.ResponseBody);
                    Assert.IsFalse(string.IsNullOrEmpty(path));

                    var data = ParseFile(path);

                    Assert.AreEqual("POST", data["HttpMethod"]);
                    Assert.AreEqual(MimeType.Application.UrlEncoded.ToString(), data["ContentType"]);
                    Assert.AreEqual("Mickey Mouse", data["Form.cartoon"]);
                    Assert.AreEqual("United States", data["Form.country"]);
                    Assert.AreEqual("Tommy's World Famous Burgers", data["Form.bestfood"]);
                }
                finally
                {
                    DeleteTempFile(path);
                }
            }
        }

        [Test]
        public void PostBinary()
        {
            using (var helper = new FoolHelper())
            {
                if (!helper.RegisterRealDriver(FoolHelper.DriverType.HttpRequest))
                {
                    return;
                }

                string path = null;

                try
                {
                    string post = "Woke up in a Soho doorway, the policeman knew my name.  He said \"You can sleep at home at night, if you can stand up and walk away.\"";
                    var encoding = new System.Text.UTF8Encoding(false);
                    byte[] bytes = encoding.GetBytes(post);

                    var options = new HttpRequestOptions();
                    options.Method = HttpMethod.Post;
                    options.MimeType = MimeType.Application.OctetStream;
                    options.PostData = new ByteContent(bytes);

                    WebRequestHelper.ExecuteCommunication(GetUrl(), options);
                    Assert.AreEqual(System.Net.HttpStatusCode.OK, options.ResponseStatusCode);

                    path = CleanPath(options.ResponseBody);
                    Assert.IsFalse(string.IsNullOrEmpty(path));

                    var data = ParseFile(path);

                    Assert.AreEqual("POST", data["HttpMethod"]);
                    Assert.AreEqual(MimeType.Application.OctetStream.ToString(), data["ContentType"]);
                    Assert.AreEqual(post, data["Body"]);
                }
                finally
                {
                    DeleteTempFile(path);
                }
            }
        }

        [Test]
        public void PostXml()
        {
            using (var helper = new FoolHelper())
            {
                if (!helper.RegisterRealDriver(FoolHelper.DriverType.HttpRequest))
                {
                    return;
                }

                string path = null;

                try
                {
                    string xml = "<song><performer>Freur</performer><title>Doot Doot</title></song>";
                    var post = LqbXmlElement.Create(xml).Value;

                    var options = new HttpRequestOptions();
                    options.Method = HttpMethod.Post;
                    options.MimeType = MimeType.Text.Xml;
                    options.PostData = new XmlContent(post);

                    WebRequestHelper.ExecuteCommunication(GetUrl(), options);
                    Assert.AreEqual(System.Net.HttpStatusCode.OK, options.ResponseStatusCode);

                    path = CleanPath(options.ResponseBody);
                    Assert.IsFalse(string.IsNullOrEmpty(path));

                    var data = ParseFile(path);

                    Assert.AreEqual("POST", data["HttpMethod"]);
                    Assert.AreEqual(MimeType.Text.Xml.ToString(), data["ContentType"]);

                    string body = data["Body"];
                    var postDoc = LqbXmlElement.Create(body).Value;
                    var first = postDoc.Contained.FirstNode as XElement;
                    var second = postDoc.Contained.LastNode as XElement;
                    Assert.AreEqual("Freur", first.Value);
                    Assert.AreEqual("Doot Doot", second.Value);
                }
                finally
                {
                    DeleteTempFile(path);
                }
            }
        }

        private LqbAbsoluteUri GetUrl()
        {
            var semanticUrl = LqbAbsoluteUri.Create(URL);
            return semanticUrl.Value;
        }

        private NameValueCollection ParseFile(string path)
        {
            var dict = new NameValueCollection();
            foreach (var line in LendersOffice.Drivers.Gateways.TextFileHelper.ReadLines(path))
            {
                int index = line.IndexOf('=');
                var name = line.Substring(0, index);
                var value = line.Substring(index + 1);
                dict[name] = value;
            }

            return dict;
        }

        private string CleanPath(string path)
        {
            using (StringReader reader = new StringReader(path))
            {
                path = reader.ReadLine();
            }

            return path.Replace("&#x5C;", "\\"); // fix issue caused by our AntiXss encoding
        }

        private void DeleteTempFile(string path)
        {
            if (string.IsNullOrEmpty(path))
            {
                return;
            }

            try
            {
                if (File.Exists(path))
                {
                    File.Delete(path);
                }
            }
            catch {}
        }
    }
}
