﻿namespace LendingQB.Test.Developers.Drivers.Integrations
{
    using System;
    using System.Net;
    using LendersOffice.Drivers.Integrations;
    using LqbGrammar;
    using LqbGrammar.Adapters;
    using LqbGrammar.Commands;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;
    using LqbGrammar.Drivers.HttpRequest;
    using LqbGrammar.Exceptions;
    using LqbGrammar.Queries;
    using NSubstitute;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public class IntegrationHttpDriverTests
    {
        private IHttpRequestAdapter MockHttpAdapter;
        private ILoggingDriver MockLoggingDriver;
        private const string pbLogConnectStr = @"FormatName:DIRECT=OS:localhost\private$\DevLogMSMQ";
        private const string responseBody = "You have integrated! Your SSN is 000-00-0000.";
        private const string webExceptionResponseBody = "Web exception response stream!";
        private const string maskedResponseBody = "You have integrated! Your SSN is ***-**-**** (masked for logging).";
        private const string requestForLogging = "username='admin' password='*****'";
        private const string requestContent = "username='admin' password='admin'";
        private const string logHeader = "Integration Driver Test";
        private static readonly LqbAbsoluteUri uri = LqbAbsoluteUri.Create("http://localhost/").Value;
        private static readonly HttpRequestOptions requestForHttp = new HttpRequestOptions
        {
            PostData = new StringContent(requestContent, length: requestContent.Length)
        };
        private static readonly Result<string> parsedSuccessResponse = Result<string>.Success("Parsed response!");

        /// <summary>
        /// Initializes port-wrapping substitutions for the code under test which will not be modified by the tests.
        /// </summary>
        public IntegrationHttpDriverTests()
        {
            var configQueryDriver = Substitute.For<ISiteConfigurationQuery>();
            var configQueryFactory = Substitute.For<IConfigurationQueryFactory>();
            configQueryFactory.CreateSiteConfiguration().Returns(configQueryDriver);
            configQueryDriver.ReadAllValues().Returns(new System.Collections.Generic.Dictionary<string, string>
            {
                { nameof(LendersOffice.Constants.ConstSite.MsMqPbLogConnectStr), pbLogConnectStr },
                { nameof(LendersOffice.Constants.ConstSite.MsMqLogConnectStr), pbLogConnectStr }
            });
            LqbApplication.Current.RegisterFactory(configQueryFactory);
        }

        [TestFixtureTearDown]
        public void FixtureTearDown()
        {
            LqbApplication.Current.RestoreFactories();
        }

        /// <summary>
        /// Sets up before each test by resetting the mocks that are used in multiple tests.
        /// </summary>
        [SetUp]
        public void SetUp()
        {
            this.MockHttpAdapter = Substitute.For<IHttpRequestAdapter>();
            var httpAdapterFactory = Substitute.For<IHttpRequestAdapterFactory>();
            httpAdapterFactory.Create().Returns(MockHttpAdapter);
            LqbApplication.Current.RegisterFactory(httpAdapterFactory);

            this.MockLoggingDriver = Substitute.For<ILoggingDriver>();
            var loggingDriverFactory = Substitute.For<ILoggingDriverFactory>();
            loggingDriverFactory.Create().ReturnsForAnyArgs(MockLoggingDriver);
            LqbApplication.Current.RegisterFactory(loggingDriverFactory);
        }

        /// <summary>
        /// Tests a completely successful communication scenario. 
        /// Makes sure that the proper <see cref="IIntegrationHttpCommunication{TResponse}"/> methods are called and the expected logging and HTTP calls are made.
        /// </summary>
        [Test]
        public void ExecuteCommunication_Success()
        {
            // Arrange
            IIntegrationHttpCommunication<string> communication = ArrangeCommunication(successfulResponse: true, responseBody: responseBody);
            var integrationDriver = new IntegrationHttpDriver<string>();

            // Act
            var communicationResult = integrationDriver.ExecuteCommunication(communication);

            // Assert
            AssertCommon(communication);
            MockLoggingDriver.Received(1).LogInfo(
                Arg.Is<LogMessage>(message =>
                    message.ToString().StartsWith($"=== {logHeader} Response ===")
                    && message.ToString().Contains(maskedResponseBody)
                    && !message.ToString().Contains("Response could not be read.")),
                typeof(LendersOffice.Drivers.Logger.LoggingHelper),
                Arg.Any<System.Collections.Generic.Dictionary<LogPropertyName, LogPropertyValue>>());
            communication.Received().MaskResponseString(responseBody);
            Assert.AreEqual(parsedSuccessResponse, communicationResult);

            communication.DidNotReceive().HandleWebException(Arg.Any<WebException>());
        }

        /// <summary>
        /// Tests a completely successful communication scenario. 
        /// Makes sure that the proper <see cref="IIntegrationHttpCommunication{TResponse}"/> methods are called and the expected logging and HTTP calls are made.
        /// </summary>
        [Test]
        public void ExecuteCommunication_NoLogging_DoesntLog()
        {
            // Arrange
            IIntegrationHttpCommunication<string> communication = ArrangeCommunication(successfulResponse: true, responseBody: responseBody);
            communication.ShouldLog.Returns(false);
            var integrationDriver = new IntegrationHttpDriver<string>();

            // Act
            var communicationResult = integrationDriver.ExecuteCommunication(communication);

            // Assert
            communication.DidNotReceive().GetRequestForLogging();
            MockLoggingDriver.DidNotReceiveWithAnyArgs().LogInfo(Arg.Any<LogMessage>(), null, null);
            communication.Received(1).GenerateHttpRequest();
            MockHttpAdapter.Received(1).ExecuteCommunication(uri, requestForHttp);
            communication.DidNotReceive().MaskResponseString(responseBody);
            Assert.AreEqual(parsedSuccessResponse, communicationResult);

            communication.DidNotReceive().HandleWebException(Arg.Any<WebException>());
        }

        /// <summary>
        /// Tests a completely successful communication scenario. 
        /// Makes sure that the proper <see cref="IIntegrationHttpCommunication{TResponse}"/> methods are called and the expected logging and HTTP calls are made.
        /// </summary>
        [Test]
        public void ExecuteCommunication_SuccessNoResponseBody()
        {
            // Arrange
            IIntegrationHttpCommunication<string> communication = ArrangeCommunication(successfulResponse: true, responseBody: null, dumpPath: LocalFilePath.Create("/somewhere.txt"));
            var integrationDriver = new IntegrationHttpDriver<string>();

            // Act
            var communicationResult = integrationDriver.ExecuteCommunication(communication);

            // Assert
            AssertCommon(communication);
            MockLoggingDriver.Received(1).LogInfo(
                Arg.Is<LogMessage>(message =>
                    message.ToString().StartsWith($"=== {logHeader} Response ===")
                    && message.ToString().Contains("somewhere.txt")
                    && message.ToString().Contains("Response could not be read.")),
                typeof(LendersOffice.Drivers.Logger.LoggingHelper),
                Arg.Any<System.Collections.Generic.Dictionary<LogPropertyName, LogPropertyValue>>());
            communication.DidNotReceive().MaskResponseString(responseBody);
            Assert.AreEqual(parsedSuccessResponse, communicationResult);

            communication.DidNotReceive().HandleWebException(Arg.Any<WebException>());
        }

        /// <summary>
        /// Tests a communication scenario with an unsuccessful communication but no exceptions. 
        /// Makes sure that the proper <see cref="IIntegrationHttpCommunication{TResponse}"/> methods are called and the expected logging and HTTP calls are made.
        /// </summary>
        [Test]
        public void ExecuteCommunication_Failure()
        {
            // Arrange
            var communication = ArrangeCommunication(successfulResponse: false, responseBody: responseBody);
            communication.MaskResponseString(responseBody).Returns((string)null);
            communication.HandleWebException(Arg.Any<WebException>()).Returns(true);
            var integrationDriver = new IntegrationHttpDriver<string>();

            // Act
            var communicationResult = integrationDriver.ExecuteCommunication(communication);

            // Assert
            AssertCommon(communication);
            MockLoggingDriver.Received(1).LogError(
                Arg.Is<LogMessage>(message => message.ToString().StartsWith($"=== {logHeader} Response ===")),
                typeof(LendersOffice.Drivers.Logger.LoggingHelper),
                Arg.Any<System.Collections.Generic.Dictionary<LogPropertyName, LogPropertyValue>>());
            Assert.AreEqual(false, communicationResult.HasError);
            Assert.AreEqual(null, communicationResult.Value);

            communication.DidNotReceive().ParseResponse(Arg.Any<HttpRequestOptions>());
            communication.DidNotReceive().HandleWebException(Arg.Any<WebException>());
        }

        /// <summary>
        /// Tests a communication scenario with an unsuccessful communication because of a WebException without a response body. 
        /// Makes sure that the proper <see cref="IIntegrationHttpCommunication{TResponse}"/> methods are called and the expected logging and HTTP calls are made.
        /// </summary>
        [Test]
        public void ExecuteCommunication_WebExceptionNoResponse()
        {
            // Arrange
            var thrownException = new WebException(); // No response body
            var communication = ArrangeCommunication(successfulResponse: false, thrownException: thrownException);
            communication.HandleWebException(Arg.Any<WebException>()).Returns(true);
            var integrationDriver = new IntegrationHttpDriver<string>();

            // Act
            var communicationResult = integrationDriver.ExecuteCommunication(communication);

            // Assert
            AssertCommon(communication);
            MockLoggingDriver.Received(1).LogError(
                Arg.Is<LogMessage>(message =>
                    message.ToString().StartsWith($"=== {logHeader} Communication Error ===")
                    && message.ToString().Split(new[] { Environment.NewLine }, StringSplitOptions.None)[2].Contains("No Response Body.")),
                typeof(LendersOffice.Drivers.Logger.LoggingHelper),
                Arg.Any<System.Collections.Generic.Dictionary<LogPropertyName, LogPropertyValue>>());
            Assert.AreEqual(true, communicationResult.HasError);
            Assert.AreEqual(thrownException, communicationResult.Error);
            communication.Received().HandleWebException(thrownException);

            communication.DidNotReceive().ParseResponse(Arg.Any<HttpRequestOptions>());
            MockLoggingDriver.DidNotReceive().LogError(
                Arg.Is<LogMessage>(message => message.ToString().StartsWith($"=== {logHeader} Response ===")),
                typeof(LendersOffice.Drivers.Logger.LoggingHelper),
                Arg.Any<System.Collections.Generic.Dictionary<LogPropertyName, LogPropertyValue>>());
            MockLoggingDriver.DidNotReceive().LogInfo(
               Arg.Is<LogMessage>(message => message.ToString().StartsWith($"=== {logHeader} Response ===")),
               typeof(LendersOffice.Drivers.Logger.LoggingHelper),
               Arg.Any<System.Collections.Generic.Dictionary<LogPropertyName, LogPropertyValue>>());
        }

        /// <summary>
        /// Tests a communication scenario with an unsuccessful communication because of exceptions. 
        /// Makes sure that the proper <see cref="IIntegrationHttpCommunication{TResponse}"/> methods are called and the expected logging and HTTP calls are made.
        /// </summary>
        [Test]
        public void ExecuteCommunication_WebExceptionWithResponse()
        {
            // Arrange
            var thrownException = CreateWebException();
            var communication = ArrangeCommunication(successfulResponse: false, thrownException: thrownException);
            communication.HandleWebException(Arg.Any<WebException>()).Returns(true);
            var integrationDriver = new IntegrationHttpDriver<string>();

            // Act
            var communicationResult = integrationDriver.ExecuteCommunication(communication);

            // Assert
            MockLoggingDriver.Received().LogError(
                Arg.Is<LogMessage>(message =>
                    message.ToString().StartsWith($"=== {logHeader} Communication Error ===")
                    && message.ToString().Split(new[] { Environment.NewLine }, StringSplitOptions.None)[3].Contains(webExceptionResponseBody)
                    && !message.ToString().Contains("No Response Body.")),
                typeof(LendersOffice.Drivers.Logger.LoggingHelper),
                Arg.Any<System.Collections.Generic.Dictionary<LogPropertyName, LogPropertyValue>>());
            Assert.AreEqual(true, communicationResult.HasError);
            Assert.AreEqual(thrownException, communicationResult.Error);
            communication.Received().HandleWebException(thrownException);

            communication.DidNotReceive().ParseResponse(Arg.Any<HttpRequestOptions>());
            MockLoggingDriver.DidNotReceive().LogError(
                Arg.Is<LogMessage>(message => message.ToString().StartsWith($"=== {logHeader} Response ===")),
                typeof(LendersOffice.Drivers.Logger.LoggingHelper),
                Arg.Any<System.Collections.Generic.Dictionary<LogPropertyName, LogPropertyValue>>());
            MockLoggingDriver.DidNotReceive().LogInfo(
               Arg.Is<LogMessage>(message => message.ToString().StartsWith($"=== {logHeader} Response ===")),
               typeof(LendersOffice.Drivers.Logger.LoggingHelper),
               Arg.Any<System.Collections.Generic.Dictionary<LogPropertyName, LogPropertyValue>>());
        }

        /// <summary>
        /// Tests a communication scenario with an unsuccessful communication because of an LQB exception. 
        /// Makes sure that the proper <see cref="IIntegrationHttpCommunication{TResponse}"/> methods are called and the expected logging and HTTP calls are made.
        /// </summary>
        [Test]
        public void ExecuteCommunication_LqbException()
        {
            // Arrange
            var webException = CreateWebException();
            var thrownException = new DeveloperException(ErrorMessage.SystemError, webException);
            var communication = ArrangeCommunication(successfulResponse: false, thrownException: thrownException);
            communication.HandleWebException(webException).Returns(true);
            var integrationDriver = new IntegrationHttpDriver<string>();

            // Act
            var communicationResult = integrationDriver.ExecuteCommunication(communication);

            // Assert
            AssertCommon(communication);
            MockLoggingDriver.Received().LogError(
                Arg.Is<LogMessage>(message =>
                    message.ToString().StartsWith($"=== {logHeader} Communication Error ===")
                    && message.ToString().Split(new[] { Environment.NewLine }, StringSplitOptions.None)[3].Contains(webExceptionResponseBody)
                    && !message.ToString().Contains("No Response Body.")),
                typeof(LendersOffice.Drivers.Logger.LoggingHelper),
                Arg.Any<System.Collections.Generic.Dictionary<LogPropertyName, LogPropertyValue>>());
            Assert.AreEqual(true, communicationResult.HasError);
            Assert.AreEqual(thrownException, communicationResult.Error);
            communication.Received().HandleWebException(webException);

            communication.DidNotReceive().ParseResponse(Arg.Any<HttpRequestOptions>());
            MockLoggingDriver.DidNotReceive().LogError(
                Arg.Is<LogMessage>(message => message.ToString().StartsWith($"=== {logHeader} Response ===")),
                typeof(LendersOffice.Drivers.Logger.LoggingHelper),
                Arg.Any<System.Collections.Generic.Dictionary<LogPropertyName, LogPropertyValue>>());
            MockLoggingDriver.DidNotReceive().LogInfo(
               Arg.Is<LogMessage>(message => message.ToString().StartsWith($"=== {logHeader} Response ===")),
               typeof(LendersOffice.Drivers.Logger.LoggingHelper),
               Arg.Any<System.Collections.Generic.Dictionary<LogPropertyName, LogPropertyValue>>());
        }

        /// <summary>
        /// Tests a communication scenario with an unsuccessful communication because of a non-LQB, non-Web exception. 
        /// Makes sure that the proper <see cref="IIntegrationHttpCommunication{TResponse}"/> methods are called and the expected logging and HTTP calls are made.
        /// </summary>
        [Test]
        public void ExecuteCommunication_OtherException()
        {
            var thrownException = new Exception("This passes through!");
            var communication = ArrangeCommunication(successfulResponse: false, thrownException: thrownException);
            var integrationDriver = new IntegrationHttpDriver<string>();

            var exc = Assert.Throws<Exception>(() => integrationDriver.ExecuteCommunication(communication));
            Assert.AreEqual(thrownException, exc);

            AssertCommon(communication);
            communication.DidNotReceive().HandleWebException(Arg.Any<WebException>());
            communication.DidNotReceive().ParseResponse(Arg.Any<HttpRequestOptions>());
            MockLoggingDriver.DidNotReceive().LogError(
                Arg.Is<LogMessage>(message => message.ToString().StartsWith($"=== {logHeader} Response ===")),
                typeof(LendersOffice.Drivers.Logger.LoggingHelper),
                Arg.Any<System.Collections.Generic.Dictionary<LogPropertyName, LogPropertyValue>>());
            MockLoggingDriver.DidNotReceive().LogInfo(
                Arg.Is<LogMessage>(message => message.ToString().StartsWith($"=== {logHeader} Response ===")),
                typeof(LendersOffice.Drivers.Logger.LoggingHelper),
                Arg.Any<System.Collections.Generic.Dictionary<LogPropertyName, LogPropertyValue>>());
        }

        /// <summary>
        /// Tests a communication scenario with an unsuccessful communication because of a non-LQB, non-Web exception. 
        /// Makes sure that the proper <see cref="IIntegrationHttpCommunication{TResponse}"/> methods are called and the expected logging and HTTP calls are made.
        /// </summary>
        [Test]
        public void ExecuteCommunication_WebException_HandleReturnsFalse()
        {
            var thrownException = new WebException("This passes through!");
            var communication = ArrangeCommunication(successfulResponse: false, thrownException: thrownException);
            communication.HandleWebException(thrownException).Returns(false);
            var integrationDriver = new IntegrationHttpDriver<string>();

            try
            {
                var communicationResult = integrationDriver.ExecuteCommunication(communication);
                Assert.Fail("System.Net.WebException was caught inside the driver when communication.HandleWebException returned false.");
            }
            catch (Exception e)
            {
                AssertCommon(communication);
                Assert.AreEqual(e, thrownException);
                communication.Received().HandleWebException(thrownException);
                communication.DidNotReceive().ParseResponse(Arg.Any<HttpRequestOptions>());
                MockLoggingDriver.DidNotReceive().LogError(
                    Arg.Is<LogMessage>(message => message.ToString().StartsWith($"=== {logHeader} Response ===")),
                    typeof(LendersOffice.Drivers.Logger.LoggingHelper),
                    Arg.Any<System.Collections.Generic.Dictionary<LogPropertyName, LogPropertyValue>>());
                MockLoggingDriver.DidNotReceive().LogInfo(
                   Arg.Is<LogMessage>(message => message.ToString().StartsWith($"=== {logHeader} Response ===")),
                   typeof(LendersOffice.Drivers.Logger.LoggingHelper),
                   Arg.Any<System.Collections.Generic.Dictionary<LogPropertyName, LogPropertyValue>>());
            }
        }

        /// <summary>
        /// Gathers common assertions among tests in this class.
        /// </summary>
        /// <param name="communication">A specific communication substitute object to assert against.</param>
        public void AssertCommon(IIntegrationHttpCommunication<string> communication)
        {
            communication.Received(1).GetRequestForLogging();
            MockLoggingDriver.Received(1).LogInfo(
                Arg.Is<LogMessage>(message => message.ToString().StartsWith($"=== {logHeader} Request ===")),
                typeof(LendersOffice.Drivers.Logger.LoggingHelper),
                Arg.Any<System.Collections.Generic.Dictionary<LogPropertyName, LogPropertyValue>>());
            communication.Received(1).GenerateHttpRequest();
            MockHttpAdapter.Received(1).ExecuteCommunication(uri, requestForHttp);
            MockLoggingDriver.Received(1).LogInfo(
                Arg.Is<LogMessage>(message => message.ToString().StartsWith($"=== {logHeader} Request Timer ===")),
                typeof(LendersOffice.Drivers.Logger.LoggingHelper),
                Arg.Any<System.Collections.Generic.Dictionary<LogPropertyName, LogPropertyValue>>());
        }

        /// <summary>
        /// Creates and sets common return values for a new <see cref="IIntegrationHttpCommunication{TResponse}"/> object.
        /// </summary>
        /// <returns>A new <see cref="IIntegrationHttpCommunication{TResponse}"/> object.</returns>
        private IIntegrationHttpCommunication<string> ArrangeCommunication(bool successfulResponse, string responseBody = null, LocalFilePath? dumpPath = null, Exception thrownException = null)
        {
            var communication = Substitute.For<IIntegrationHttpCommunication<string>>();
            communication.GetRequestForLogging().Returns(requestForLogging);
            communication.LogHeader.Returns(logHeader);
            communication.LogRequestTiming.Returns(true);
            communication.ShouldLog.Returns(true);
            communication.GenerateHttpRequest().Returns(requestForHttp);
            communication.GetEndpointPath().Returns(uri);
            communication.MaskResponseString(responseBody).Returns(maskedResponseBody);
            communication.ParseResponse(requestForHttp).Returns(parsedSuccessResponse);

            this.MockHttpAdapter.ExecuteCommunication(uri, requestForHttp).Returns(successfulResponse).AndDoes(x =>
            {
                if (thrownException == null)
                {
                    requestForHttp.ResponseBody = responseBody;
                    requestForHttp.ResponseDumpPath = dumpPath;
                }
                else throw thrownException;
            });
            return communication;
        }

        /// <summary>
        /// Creates a web exception with a web response stream.
        /// </summary>
        /// <returns>A new web exception populated with a web response.</returns>
        private WebException CreateWebException()
        {
            var failedWebResponse = Substitute.For<WebResponse>();
            failedWebResponse.GetResponseStream().Returns(new System.IO.MemoryStream(System.Text.Encoding.UTF8.GetBytes(webExceptionResponseBody)));
            return new WebException("communication Error", null, WebExceptionStatus.ProtocolError, failedWebResponse);
        }

        /// <summary>
        /// Populates properties of the request to an <see cref="HttpRequestOptions"/>.
        /// </summary>
        /// <param name="request">The options object ot populate response properties of.</param>
        private void PopulateResponse(HttpRequestOptions request)
        {
            request.ResponseBody = responseBody;
        }
    }
}
