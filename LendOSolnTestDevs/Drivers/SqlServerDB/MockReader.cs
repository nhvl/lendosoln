﻿namespace LendingQB.Test.Developers.Drivers.SqlServerDB
{
    using System;
    using System.Data;
    using LendersOffice.Drivers.SqlServerDB;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public sealed class MockReader
    {
        [Test]
        public void TestSingleTable()
        {
            var ownerId = Guid.NewGuid();

            var reader = new MockDataReader();
            reader.BeginConstruction();
            reader.BeginTable();
            reader.BeginRow();
            reader.AddColumn("ID", 1);
            reader.AddColumn("CategoryName", "Hiccup");
            reader.AddColumn("OwnerID", ownerId);
            reader.AddColumn("Created", DateTime.Now.AddMinutes(-5));
            reader.BeginRow();
            reader.AddColumn("ID", 2);
            reader.AddColumn("CategoryName", "Jacknife");
            reader.AddColumn("OwnerID", ownerId);
            reader.AddColumn("Created", DateTime.Now.AddMinutes(-4));
            reader.BeginRow();
            reader.AddColumn("ID", 3);
            reader.AddColumn("CategoryName", "Karaoke");
            reader.AddColumn("OwnerID", ownerId);
            reader.AddColumn("Created", DateTime.Now.AddMinutes(-3));
            reader.EndConstruction();

            using (IDataReader dr = reader as IDataReader)
            {
                int rowCount = 0;
                while (dr.Read())
                {
                    rowCount++;
                    var guid = dr.GetGuid(2);
                    Assert.AreEqual(ownerId, guid);

                    DateTime dt = Convert.ToDateTime(dr["Created"]);
                    Assert.IsTrue(dt < DateTime.Now && dt > DateTime.MinValue);

                    string name = (string)dr["CategoryName"];
                    Assert.IsFalse(string.IsNullOrEmpty(name));

                    int id = (int)dr.GetValue(0);
                    Assert.IsTrue(id >= 1 && id <= 3);
                }

                Assert.AreEqual(3, rowCount);
            }
        }

        [Test]
        public void TestMultiTables()
        {
            var ownerId = Guid.NewGuid();

            var reader = new MockDataReader();
            reader.BeginConstruction();

            reader.BeginTable();
            reader.BeginRow();
            reader.AddColumn("ID", 1);
            reader.AddColumn("CategoryName", "Hiccup");
            reader.AddColumn("OwnerID", ownerId);
            reader.AddColumn("Created", DateTime.Now.AddMinutes(-5));
            reader.BeginRow();
            reader.AddColumn("ID", 2);
            reader.AddColumn("CategoryName", "Jacknife");
            reader.AddColumn("OwnerID", ownerId);
            reader.AddColumn("Created", DateTime.Now.AddMinutes(-4));
            reader.BeginRow();
            reader.AddColumn("ID", 3);
            reader.AddColumn("CategoryName", "Karaoke");
            reader.AddColumn("OwnerID", ownerId);
            reader.AddColumn("Created", DateTime.Now.AddMinutes(-3));

            reader.BeginTable();
            reader.BeginRow();
            reader.AddColumn("ID", 1);
            reader.AddColumn("CategoryId", 2);
            reader.AddColumn("Ordinal", 1);
            reader.BeginRow();
            reader.AddColumn("ID", 2);
            reader.AddColumn("CategoryId", 3);
            reader.AddColumn("Ordinal", 2);
            reader.BeginRow();
            reader.AddColumn("ID", 3);
            reader.AddColumn("CategoryId", 1);
            reader.AddColumn("Ordinal", 3);

            reader.EndConstruction();

            using (IDataReader dr = reader as IDataReader)
            {
                int tableCount = 0;
                while (dr.NextResult())
                {
                    tableCount++;

                    if (tableCount == 1)
                    {
                        int rowCount = 0;
                        while (dr.Read())
                        {
                            rowCount++;
                            var guid = dr.GetGuid(2);
                            Assert.AreEqual(ownerId, guid);

                            DateTime dt = Convert.ToDateTime(dr["Created"]);
                            Assert.IsTrue(dt < DateTime.Now && dt > DateTime.MinValue);

                            string name = (string)dr["CategoryName"];
                            Assert.IsFalse(string.IsNullOrEmpty(name));

                            int id = (int)dr.GetValue(0);
                            Assert.IsTrue(id >= 1 && id <= 3);
                        }

                        Assert.AreEqual(3, rowCount);
                    }
                    else if (tableCount == 2)
                    {
                        int rowCount = 0;
                        while (dr.Read())
                        {
                            rowCount++;
                            int id = (int)dr.GetValue(0);
                            Assert.IsTrue(id >= 1 && id <= 3);

                            int order = (int)dr["Ordinal"];
                            Assert.AreEqual(id, order);

                            int catId = (int)dr["CategoryId"];
                            Assert.AreNotEqual(id, catId);
                        }

                        Assert.AreEqual(3, rowCount);
                    }
                }

                Assert.AreEqual(2, tableCount);
            }
        }
    }
}
