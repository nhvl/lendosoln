﻿namespace LendingQB.Test.Developers.Drivers.SqlServerDB
{
    using System;
    using System.Data.SqlClient;
    using LendersOffice.Drivers.SqlServerDB;
    using LendersOfficeApp.LOAdmin.Broker;
    using LqbGrammar;
    using LqbGrammar.Drivers;
    using NUnit.Framework;
    using Utils;

    [TestFixture]
    /// <summary>
    /// Unit tester for changes to EditUser.aspx.cs that use the new SQL driver functionality.
    /// </summary>
    public sealed class DriverInterface
    {
        /// <summary>
        /// Unit test method for TestColumnName.
        /// </summary>
        [Test]
        [Category(CategoryConstants.UnitTest)]
        public void TestColumnName()
        {
            using (var helper = new FoolHelper())
            {
                if (!helper.RegisterRealDriver(FoolHelper.DriverType.RegularExpression))
                {
                    return;
                }

                EditUser.TestColumnName("OpsToUseNewLoanEditorUI");
            }
        }

        /// <summary>
        /// Unit test method for FormulateUpdateUserQuery.
        /// </summary>
        [Test]
        [Category(CategoryConstants.UnitTest)]
        public void FormulateUpdateUserQuery()
        {
            var sql = EditUser.FormulateUpdateUserQuery("OpsToUseNewLoanEditorUI");
            Assert.IsFalse(string.IsNullOrEmpty(sql.Value));
        }

        /// <summary>
        /// Unit test method for FormulateUpdateUserQuery.
        /// </summary>
        [Test]
        [Category(CategoryConstants.LqbMocks)]
        [Category(CategoryConstants.IntegrationTest)]
        public void ExecuteUpdateUserQuery()
        {
            using (var helper = new FoolHelper())
            {
                if (!helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery)) // Needed to get core services registered
                {
                    return;
                }

                var factory = new MockModifyDriverFactory(1);
                helper.RegisterMockDriverFactory<ISqlDriverFactory>(factory);

                var brokerId = Guid.Parse("BE9439A4-25AC-41B0-9A86-7DA324C2F966");
                var sqlQuery = EditUser.FormulateUpdateUserQuery("OpsToUseNewLoanEditorUI");
                var p1 = new SqlParameter("@id", 666);
                var p2 = new SqlParameter("@value", "Satan");
                var arrParams = new SqlParameter[] { p1, p2 };

                var modCount = EditUser.ExecuteUpdateUserQuery(brokerId, sqlQuery, arrParams);
                Assert.AreEqual(1, modCount.Value);

                var queryImpl = factory.Driver;
                Assert.IsFalse(string.IsNullOrEmpty(queryImpl.GetQuery()));
                Assert.AreEqual("666", queryImpl.GetParamValue("@id"));
                Assert.AreEqual("Satan", queryImpl.GetParamValue("@value"));
            }
        }

        [Test]
        [Category(CategoryConstants.UnitTest)]
        public void BinaryLiteral()
        {
            var literal = "0x2a";
            var value = BrokerExport.ConvertBinaryLiteral(literal);
            Assert.AreEqual(42, value[0]);

            literal = "0x2a2b";
            value = BrokerExport.ConvertBinaryLiteral(literal);
            Assert.AreEqual(42, value[0]);
            Assert.AreEqual(43, value[1]);
        }
    }
}
