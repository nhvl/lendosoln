﻿namespace LendingQB.Test.Developers.Drivers.SqlServerDB
{
    using System;
    using global::DataAccess;
    using NUnit.Framework;
    using Utils;

    [TestFixture]
    public sealed class ErrorTest
    {
        [Test]
        [Category(CategoryConstants.IntegrationTest)]
        [ExpectedException(typeof(LqbGrammar.Exceptions.ServerException))]
        public void TryLogSqlStatment()
        {
            using (var helper = new FoolHelper())
            {
                if (!helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery))
                {
                    return;
                }

                Guid brokerId = LoginTools.LoginAs(TestAccountType.LoTest001).BrokerId;
                var ds = new System.Data.DataSet();
                string query = "RAISERROR 'Testing Error Handling';";
                DBSelectUtility.FillDataSet(brokerId, ds, query, null, null);
            }
        }
    }
}
