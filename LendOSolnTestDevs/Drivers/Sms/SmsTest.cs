﻿namespace LendingQB.Test.Developers.Drivers.Sms
{
    using System.Xml;
    using LendersOffice.Drivers.Sms;
    using LqbGrammar;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;
    using NUnit.Framework;
    using Utils;

    [TestFixture]
    [Category(CategoryConstants.IntegrationTest)]
    [Category(CategoryConstants.LqbMocks)]
    [Ignore("VERIFIED - and it requires execution on a webserver hosing the LendersOfficeApp website.")]
    public sealed class SmsTest
    {
        [Test]
        public void SendMessageToTestPage()
        {
            using (var helper = new FoolHelper())
            {
                string path = null;
                try
                {
                    if (!helper.RegisterRealDriver(FoolHelper.DriverType.Sms)) // need to get the adapter registered
                    {
                        return;
                    }

                    helper.RegisterRealDriver(FoolHelper.DriverType.HttpRequest);

                    var factory = new MockSmsDriverFactory(true, false);
                    helper.RegisterMockDriverFactory<ISmsDriverFactory>(factory);

                    var phone = PhoneNumber.Create("(714)555-1234");
                    var message = SmsAuthCodeMessage.Create("LendingQB", "1234");

                    SmsHelper.SendMessage(phone.Value, message.Value);

                    string request = factory.TestPageRequest;
                    var doc = new XmlDocument();
                    doc.LoadXml(request);
                    VerifyXML(doc, message.Value.ToString());

                    path = factory.TestPageResponse.Replace("&#x5C;", "\\").Replace(System.Environment.NewLine, string.Empty);
                    Assert.IsTrue(System.IO.File.Exists(path));

                    string[] lines = System.IO.File.ReadAllLines(path);
                    foreach (string line in lines)
                    {
                        if (line.StartsWith("Body="))
                        {
                            string text = line.Substring(5);

                            doc = new XmlDocument();
                            doc.LoadXml(text);
                            VerifyXML(doc, message.Value.ToString());
                        }
                    }
                }
                finally
                {
                    if (path != null && System.IO.File.Exists(path))
                    {
                        System.IO.File.Delete(path);
                    }
                }
            }
        }

        private void VerifyXML(XmlDocument doc, string message)
        {
            var root = doc.DocumentElement;
            Assert.AreEqual("SMS", root.GetAttribute("type"));

            var user = (XmlElement)root.SelectSingleNode("USER");
            Assert.IsNotNull(user);
            Assert.AreEqual("Password", user.GetAttribute("password"));

            var template = (XmlElement)root.SelectSingleNode("TEMPLATE");
            Assert.AreEqual(message, template.InnerText);
        }
    }
}
