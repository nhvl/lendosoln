﻿namespace LendingQB.Test.Developers.Drivers.Gateways
{
    using System;
    using System.IO;
    using LendersOffice.Drivers.Gateways;
    using NUnit.Framework;
    using Utils;

    [TestFixture]
    [Category(CategoryConstants.IntegrationTest)]
    public sealed class TextFileTest
    {
        private const string TEXT = "I am a bit peckish (audience storms stage in disgust)";

        private string path;

        [SetUp]
        public void SetUp()
        {
            this.path = Path.GetTempFileName();
            File.WriteAllText(this.path, TEXT);
        }

        [TearDown]
        public void TearDown()
        {
            if (File.Exists(this.path))
            {
                File.Delete(this.path);
            }
        }

        [Test]
        public void AppendString()
        {
            using (var helper = new FoolHelper())
            {
                if (!helper.RegisterRealDriver(FoolHelper.DriverType.FileSystem))
                {
                    return;
                }

                string temp = Path.GetTempFileName();
                try
                {
                    File.WriteAllText(temp, TEXT);

                    TextFileHelper.AppendString(temp, TEXT);

                    var read = File.ReadAllText(temp);
                    Assert.AreEqual(2 * TEXT.Length, read.Length);
                }
                finally
                {
                    if (File.Exists(temp))
                    {
                        File.Delete(temp);
                    }
                }
            }
        }

        [Test]
        public void OpenForAppend()
        {
            using (var helper = new FoolHelper())
            {
                if (!helper.RegisterRealDriver(FoolHelper.DriverType.FileSystem))
                {
                    return;
                }

                string temp = Path.GetTempFileName();
                try
                {
                    File.WriteAllText(temp, TEXT);

                    Action<TextFileHelper.LqbTextWriter> handler = delegate (TextFileHelper.LqbTextWriter writer)
                    {
                        writer.Append(TEXT);
                    };

                    TextFileHelper.OpenForAppend(temp, handler);

                    var read = File.ReadAllText(temp);
                    Assert.AreEqual(2 * TEXT.Length, read.Length);
                }
                finally
                {
                    if (File.Exists(temp))
                    {
                        File.Delete(temp);
                    }
                }
            }
        }

        [Test]
        public void WriteStringNoBOM()
        {
            using (var helper = new FoolHelper())
            {
                if (!helper.RegisterRealDriver(FoolHelper.DriverType.FileSystem))
                {
                    return;
                }

                string temp = Path.GetTempFileName();
                try
                {
                    TextFileHelper.WriteString(temp, TEXT, false);

                    var read = File.ReadAllBytes(temp);
                    Assert.AreEqual(TEXT.Length, read.Length);
                }
                finally
                {
                    if (File.Exists(temp))
                    {
                        File.Delete(temp);
                    }
                }
            }
        }

        [Test]
        public void WriteStringWithBOM()
        {
            using (var helper = new FoolHelper())
            {
                if (!helper.RegisterRealDriver(FoolHelper.DriverType.FileSystem))
                {
                    return;
                }

                string temp = Path.GetTempFileName();
                try
                {
                    TextFileHelper.WriteString(temp, TEXT, true);

                    var read = File.ReadAllBytes(temp);
                    Assert.AreEqual(TEXT.Length + 3, read.Length);
                }
                finally
                {
                    if (File.Exists(temp))
                    {
                        File.Delete(temp);
                    }
                }
            }
        }

        [Test]
        public void OpenNew()
        {
            using (var helper = new FoolHelper())
            {
                if (!helper.RegisterRealDriver(FoolHelper.DriverType.FileSystem))
                {
                    return;
                }

                string temp = Path.GetTempFileName();
                try
                {
                    Action<TextFileHelper.LqbTextWriter> handler = delegate (TextFileHelper.LqbTextWriter writer)
                    {
                        writer.Append(TEXT);
                    };

                    TextFileHelper.OpenNew(temp, handler);

                    var read = File.ReadAllText(temp);
                    Assert.AreEqual(TEXT.Length, read.Length);
                }
                finally
                {
                    if (File.Exists(temp))
                    {
                        File.Delete(temp);
                    }
                }
            }
        }

        [Test]
        public void ReadFile()
        {
            using (var helper = new FoolHelper())
            {
                if (!helper.RegisterRealDriver(FoolHelper.DriverType.FileSystem))
                {
                    return;
                }

                string read = TextFileHelper.ReadFile(this.path);

                Assert.AreEqual(TEXT.Length, read.Length);
            }
        }

        [Test]
        public void ReadLines()
        {
            using (var helper = new FoolHelper())
            {
                if (!helper.RegisterRealDriver(FoolHelper.DriverType.FileSystem))
                {
                    return;
                }

                string temp = Path.GetTempFileName();
                try
                {
                    string[] lines = new string[3];
                    lines[0] = TEXT;
                    lines[1] = TEXT + TEXT;
                    lines[2] = TEXT + TEXT + TEXT;

                    File.WriteAllLines(temp, lines);

                    var read = TextFileHelper.ReadLines(temp);

                    int i = 1;
                    foreach (string line in read)
                    {
                        Assert.AreEqual(i++ * TEXT.Length, line.Length);
                    }
                }
                finally
                {
                    if (File.Exists(temp))
                    {
                        File.Delete(temp);
                    }
                }
            }
        }

        [Test]
        public void OpenRead()
        {
            using (var helper = new FoolHelper())
            {
                if (!helper.RegisterRealDriver(FoolHelper.DriverType.FileSystem))
                {
                    return;
                }

                string read = null;

                Action<TextFileHelper.LqbTextReader> handler = delegate (TextFileHelper.LqbTextReader reader)
                {
                    read = reader.ReadLine();
                };

                TextFileHelper.OpenRead(this.path, handler);

                Assert.IsNotNull(read);
                Assert.AreEqual(TEXT.Length, read.Length);
            }
        }
    }
}
