﻿namespace LendingQB.Test.Developers.Drivers.Gateways
{
    using System;
    using System.IO;
    using System.Text;
    using LendersOffice.Drivers.Gateways;
    using NUnit.Framework;
    using Utils;

    [TestFixture]
    [Category(CategoryConstants.IntegrationTest)]
    public sealed class BinaryFileTest
    {
        private const string TEXT = "Q: What is black and white, red all over, and cannot fit through revolving doors? A: A nun with a spear through her head.";

        private string path;
        private byte[] data;

        [SetUp]
        public void SetUp()
        {
            this.path = Path.GetTempFileName();
            this.data = Encoding.ASCII.GetBytes(TEXT);
            File.WriteAllBytes(this.path, this.data);
        }

        [TearDown]
        public void TearDown()
        {
            if (File.Exists(this.path))
            {
                File.Delete(this.path);
            }
        }

        [Test]
        public void WriteAllBytes()
        {
            using (var helper = new FoolHelper())
            {
                if (!helper.RegisterRealDriver(FoolHelper.DriverType.FileSystem))
                {
                    return;
                }

                string temp = Path.GetTempFileName();
                try
                {
                    BinaryFileHelper.WriteAllBytes(temp, this.data);

                    var read = File.ReadAllBytes(temp);
                    Assert.AreEqual(read.Length, this.data.Length);
                }
                finally
                {
                    if (File.Exists(temp))
                    {
                        File.Delete(temp);
                    }
                }
            }
        }

        [Test]
        public void OpenNew()
        {
            using (var helper = new FoolHelper())
            {
                if (!helper.RegisterRealDriver(FoolHelper.DriverType.FileSystem))
                {
                    return;
                }

                string temp = Path.GetTempFileName();
                try
                {
                    Action<BinaryFileHelper.LqbBinaryStream> handler = delegate (BinaryFileHelper.LqbBinaryStream lqbStream)
                    {
                        using (BinaryWriter writer = new BinaryWriter(lqbStream.Stream))
                        {
                            writer.Write(this.data);
                        }
                    };

                    BinaryFileHelper.OpenNew(temp, handler);

                    var read = File.ReadAllBytes(temp);
                    Assert.AreEqual(this.data.Length, read.Length);
                }
                finally
                {
                    if (File.Exists(temp))
                    {
                        File.Delete(temp);
                    }
                }
            }
        }

        [Test]
        public void OpenRead()
        {
            using (var helper = new FoolHelper())
            {
                if (!helper.RegisterRealDriver(FoolHelper.DriverType.FileSystem))
                {
                    return;
                }

                byte[] read = null;
                Action<BinaryFileHelper.LqbBinaryStream> handler = delegate (BinaryFileHelper.LqbBinaryStream lqbStream)
                {
                    using (BinaryReader reader = new BinaryReader(lqbStream.Stream))
                    {
                        read = reader.ReadBytes(this.data.Length + 100);
                    }
                };

                BinaryFileHelper.OpenRead(this.path, handler);

                Assert.AreEqual(this.data.Length, read.Length);
            }
        }

        [Test]
        public void ReadAllBytes()
        {
            using (var helper = new FoolHelper())
            {
                if (!helper.RegisterRealDriver(FoolHelper.DriverType.FileSystem))
                {
                    return;
                }

                var read = BinaryFileHelper.ReadAllBytes(this.path);

                Assert.AreEqual(this.data.Length, read.Length);
            }
        }
    }
}
