﻿namespace LendingQB.Test.Developers.Drivers.Gateways
{
    using System;
    using System.Xml.Linq;
    using LendersOffice.Drivers.Gateways;
    using NUnit.Framework;
    using Utils;

    [TestFixture]
    [Category(CategoryConstants.IntegrationTest)]
    [Ignore("This test is tied to Alan's machine so cannot be run by anyone else.")]
    public class MessageQueueHelperTest
    {
        private const string LocalQueue = @"FormatName:DIRECT=OS:ALANDWRK.meridianlink.com\private$\devlogmsmq";

        [Test]
        public void SendAndReceiveXML()
        {
            using (var helper = new FoolHelper())
            {
                if (!helper.RegisterRealDriver(FoolHelper.DriverType.MessageQueue))
                {
                    return;
                }

                Guid brokerId = Guid.NewGuid();
                XDocument xdoc = CreateXML(brokerId);

                DateTime initTime = DateTime.Now;
                System.Threading.Thread.Sleep(1000);
                MessageQueueHelper.SendXML(LocalQueue, null, xdoc);
                System.Threading.Thread.Sleep(1000);
                DateTime postTime = DateTime.Now;

                DateTime arrivalTime;
                var queue = MessageQueueHelper.PrepareForXML(LocalQueue, false);
                var recovered = MessageQueueHelper.ReceiveXML(queue, null, out arrivalTime);

                Assert.IsTrue(arrivalTime > initTime);
                Assert.IsTrue(arrivalTime < postTime);
                Assert.AreEqual(GetXmlString(xdoc), GetXmlString(recovered));
            }
        }

        [Test]
        public void SendAndReceiveXMLWithFilterDefaults()
        {
            using (var helper = new FoolHelper())
            {
                if (!helper.RegisterRealDriver(FoolHelper.DriverType.MessageQueue))
                {
                    return;
                }

                Guid brokerId = Guid.NewGuid();
                XDocument xdoc = CreateXML(brokerId);

                DateTime initTime = DateTime.Now;
                System.Threading.Thread.Sleep(1000);
                MessageQueueHelper.SendXML(LocalQueue, null, xdoc);
                System.Threading.Thread.Sleep(1000);
                DateTime postTime = DateTime.Now;

                DateTime arrivalTime;
                var queue = MessageQueueHelper.PrepareForXML(LocalQueue, true);
                var recovered = MessageQueueHelper.ReceiveXML(queue, null, out arrivalTime);

                Assert.IsTrue(arrivalTime > initTime);
                Assert.IsTrue(arrivalTime < postTime);
                Assert.AreEqual(GetXmlString(xdoc), GetXmlString(recovered));
            }
        }

        [Test]
        public void SendAndReceiveJSON()
        {
            using (var helper = new FoolHelper())
            {
                if (!helper.RegisterRealDrivers(FoolHelper.DriverType.MessageQueue, FoolHelper.DriverType.Json))
                {
                    return;
                }

                var person = CreatePerson();

                DateTime initTime = DateTime.Now;
                System.Threading.Thread.Sleep(1000);
                MessageQueueHelper.SendJSON(LocalQueue, null, person);
                System.Threading.Thread.Sleep(1000);
                DateTime postTime = DateTime.Now;

                DateTime arrivalTime;
                var queue = MessageQueueHelper.PrepareForXML(LocalQueue, false);
                var recovered = MessageQueueHelper.ReceiveJSON<SimplePerson>(queue, null, out arrivalTime);

                Assert.IsTrue(arrivalTime > initTime);
                Assert.IsTrue(arrivalTime < postTime);
                Assert.AreEqual(person.FirstName, recovered.FirstName);
                Assert.AreEqual(person.LastName, recovered.LastName);
                Assert.AreEqual(person.IsMale, recovered.IsMale);
            }
        }

        private string GetXmlString(XDocument doc)
        {
            string xml = doc.ToString();
            return xml.StartsWith(Environment.NewLine) ? xml.Substring(Environment.NewLine.Length) : xml;
        }

        private XDocument CreateXML(Guid brokerId)
        {
            XDocument xdoc = new XDocument();
            XElement fields = new XElement(
                                             "fields",
                                             new XElement("brokerId", brokerId.ToString("D")),
                                             new XElement("desc", "Descriptive text"),
                                             new XElement("notes", "Notes go here."),
                                             new XElement("level", "plumb"),
                                             new XElement("userType", "swashbuckling ne'er do well"),
                                             new XElement("loginNm", "SmileyPete"));
            xdoc.Add(fields);
            return xdoc;
        }

        private SimplePerson CreatePerson()
        {
            return new SimplePerson { FirstName = "Alan", LastName = "Daughton", IsMale = true };
        }

        private class SimplePerson
        {
            public string FirstName;
            public string LastName;
            public bool IsMale;
        }
    }
}
