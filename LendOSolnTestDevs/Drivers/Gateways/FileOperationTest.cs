﻿namespace LendingQB.Test.Developers.Drivers.Gateways
{
    using System;
    using System.IO;
    using LendersOffice.Drivers.Gateways;
    using NUnit.Framework;
    using Utils;

    [TestFixture]
    [Category(CategoryConstants.IntegrationTest)]
    public sealed class FileOperationTest
    {
        private const string TEXT = "Hello everybody, today is a beautiful day.";

        private DateTime start;
        private string path;

        [SetUp]
        public void SetUp()
        {
            this.start = DateTime.Now;
            System.Threading.Thread.Sleep(100);

            this.path = Path.GetTempFileName();
            File.WriteAllText(this.path, TEXT);
        }

        [TearDown]
        public void TearDown()
        {
            if (File.Exists(this.path))
            {
                File.Delete(this.path);
            }
        }

        [Test]
        public void Copy()
        {
            using (var helper = new FoolHelper())
            {
                if (!helper.RegisterRealDriver(FoolHelper.DriverType.FileSystem))
                {
                    return;
                }

                string folder = Path.GetTempPath();
                string filename = Guid.NewGuid().ToString("N");
                string target = Path.Combine(folder, filename);

                string copied;
                try
                {
                    FileOperationHelper.Copy(this.path, target, false);
                    copied = File.ReadAllText(target);
                }
                finally
                {
                    if (File.Exists(target))
                    {
                        File.Delete(target);
                    }
                }

                Assert.AreEqual(TEXT, copied);
            }
        }

        [Test]
        public void Move()
        {
            using (var helper = new FoolHelper())
            {
                if (!helper.RegisterRealDriver(FoolHelper.DriverType.FileSystem))
                {
                    return;
                }

                string folder = Path.GetTempPath();
                string filename = Guid.NewGuid().ToString("N");
                string target = Path.Combine(folder, filename);

                string copied;
                try
                {
                    FileOperationHelper.Move(this.path, target);
                    copied = File.ReadAllText(target);
                }
                finally
                {
                    if (File.Exists(target) && !File.Exists(this.path))
                    {
                        File.Move(target, this.path);
                    }
                }

                Assert.AreEqual(TEXT, copied);
            }
        }

        [Test]
        public void Delete()
        {
            using (var helper = new FoolHelper())
            {
                if (!helper.RegisterRealDriver(FoolHelper.DriverType.FileSystem))
                {
                    return;
                }

                Assert.IsTrue(File.Exists(this.path));
                FileOperationHelper.Delete(this.path);
                Assert.IsFalse(File.Exists(this.path));

                this.SetUp();
            }
        }

        [Test]
        public void Exists()
        {
            using (var helper = new FoolHelper())
            {
                if (!helper.RegisterRealDriver(FoolHelper.DriverType.FileSystem))
                {
                    return;
                }

                bool exists = FileOperationHelper.Exists(this.path);
                Assert.IsTrue(exists);
            }
        }

        [Test]
        public void TurnOffReadOnly()
        {
            using (var helper = new FoolHelper())
            {
                if (!helper.RegisterRealDriver(FoolHelper.DriverType.FileSystem))
                {
                    return;
                }

                Assert.IsFalse(GetReadOnlyFlag());
                SetReadOnlyFlag();
                Assert.IsTrue(GetReadOnlyFlag());

                FileOperationHelper.TurnOffReadOnly(this.path);
                Assert.IsFalse(GetReadOnlyFlag());
            }
        }

        [Test]
        public void GetLastWriteTime()
        {
            using (var helper = new FoolHelper())
            {
                if (!helper.RegisterRealDriver(FoolHelper.DriverType.FileSystem))
                {
                    return;
                }

                Assert.IsTrue(this.start > DateTime.Today);

                var last = FileOperationHelper.GetLastWriteTime(this.path);
                Assert.IsTrue(last > this.start);
            }
        }

        [Test]
        public void SetLastWriteTime()
        {
            using (var helper = new FoolHelper())
            {
                if (!helper.RegisterRealDriver(FoolHelper.DriverType.FileSystem))
                {
                    return;
                }

                Assert.IsTrue(this.start > DateTime.Today);

                var now = DateTime.Now;
                FileOperationHelper.SetLastWriteTime(this.path, now);
                var file = File.GetLastWriteTime(this.path);
                Assert.AreEqual(now, file);

                File.SetLastWriteTime(this.path, this.start);
            }
        }

        private bool GetReadOnlyFlag()
        {
            var attributes = File.GetAttributes(this.path);
            return (attributes & FileAttributes.ReadOnly) == FileAttributes.ReadOnly;
        }

        private void SetReadOnlyFlag()
        {
            var attributes = File.GetAttributes(this.path) | FileAttributes.ReadOnly;
            File.SetAttributes(this.path, attributes);
        }
    }
}
