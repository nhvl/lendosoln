﻿namespace LendingQB.Test.Developers.ConversationLogService.CIL
{
    using System;
    using System.Data.SqlClient;
    using ConversationLogLib;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public sealed class EncryptionManager
    {
        [Test]
        public void EncryptThenDecrypt()
        {
            string text = "Encryption test";
            var good = UnitTestHelper.CIL.EncryptionManager.EncryptThenDecrypt(text);
            Assert.IsTrue(good);
        }

        [Test]
        public void DecryptDbConnection()
        {
            string userId = "LoDevConvo_User";
            string pwd = "r!chGazelle28";

            byte[] encrypted = ConversationLogLib.Interface.EncryptionManager.Encrypt(userId);
            string userId64 = Convert.ToBase64String(encrypted);

            encrypted = ConversationLogLib.Interface.EncryptionManager.Encrypt(pwd);
            string pwd64 = Convert.ToBase64String(encrypted);

            string dsn = string.Format("Data Source=11.12.13.23;Initial Catalog=LoDevConvo;User ID={0};Password={1}", userId64, pwd64);
            string connectionString = ConversationLogLib.Interface.EncryptionManager.DecryptDbConnection(dsn);

            var builder = new SqlConnectionStringBuilder(connectionString);
            Assert.AreEqual(userId, builder.UserID);
            Assert.AreEqual(pwd, builder.Password);
        }
    }
}
