﻿namespace LendingQB.Test.Developers.ConversationLogService.DAL.DSL
{
    using System;
    using ConversationLogLib;
    using LendersOffice.Drivers.SqlServerDB;
    using LqbGrammar.Drivers;
    using NUnit.Framework;
    using Utils;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    [Category(CategoryConstants.LqbMocks)]
    public sealed class ManageConversations
    {
        [Test]
        public void BeginConversation()
        {
            long catId = 3;
            long commenterid = 43;
            string resourceType = "LF";
            string resourceId = Guid.NewGuid().ToString();
            byte[] content = DataCallHelper.GenerateContent();
            int? permissionLevelId = null;

            using (var helper = new FoolHelper())
            {
                var ds = DataCallHelper.BeginConversation((long)1, DateTime.UtcNow.AddMinutes(-1), (long)7, commenterid, content, catId, Guid.NewGuid().ToString(), "Bankruptcy", true, "Bankurptsee", permissionLevelId);

                var factory = new MockStoredProcedureDriverFactory(ds);
                if (!helper.RegisterMockDriverFactory<IStoredProcedureDriverFactory>(factory))
                {
                    return;
                }

                bool good = UnitTestHelper.DAL.DSL.ManageConversationsTester.BeginConversation(catId, commenterid, resourceType, resourceId, content, permissionLevelId);
                Assert.IsTrue(good);

                var driver = factory.GetLastDriver();
                Assert.AreEqual("BeginConversation", driver.GetQuery());
                Assert.AreEqual(driver.GetParamValue("@CategoryId"), catId.ToString());
                Assert.AreEqual(driver.GetParamValue("@CommenterId"), commenterid.ToString());
                Assert.AreEqual(driver.GetParamValue("@ResourceType"), resourceType);
                Assert.AreEqual(driver.GetParamValue("@ResourceId"), resourceId);
                Assert.AreEqual(driver.GetParamValue("@PermissionLevelId"), permissionLevelId.ToString());
            }
        }

        [Test]
        public void ReplyToComment()
        {
            long parentId = 7;
            long commenterId = 53;
            byte[] content = DataCallHelper.GenerateContent();

            using (var helper = new FoolHelper())
            {
                var ds = DataCallHelper.ReplyToComment((long)1, DateTime.UtcNow.AddMinutes(-1), (long)7, commenterId, content);

                var factory = new MockStoredProcedureDriverFactory(ds);
                if (!helper.RegisterMockDriverFactory<IStoredProcedureDriverFactory>(factory))
                {
                    return;
                }

                bool good = UnitTestHelper.DAL.DSL.ManageConversationsTester.ReplyToComment(parentId, commenterId, content);
                Assert.IsTrue(good);

                var driver = factory.GetLastDriver();
                Assert.AreEqual("ReplyToComment", driver.GetQuery());
                Assert.AreEqual(driver.GetParamValue("@ParentId"), parentId.ToString());
                Assert.AreEqual(driver.GetParamValue("@CommenterId"), commenterId.ToString());
            }
        }

        [Test]
        public void GetAllComments()
        {
            string resourceType = "LF";
            string resourceId = Guid.NewGuid().ToString();

            using (var helper = new FoolHelper())
            {
                var ds = DataCallHelper.GetAllComments(resourceType, resourceId);

                var factory = new MockStoredProcedureDriverFactory(ds);
                if (!helper.RegisterMockDriverFactory<IStoredProcedureDriverFactory>(factory))
                {
                    return;
                }

                bool good = UnitTestHelper.DAL.DSL.ManageConversationsTester.GetAllComments(resourceType, resourceId, ds.Tables[0].Rows.Count, ds.Tables[1].Rows.Count, ds.Tables[2].Rows.Count, ds.Tables[3].Rows.Count);
                Assert.IsTrue(good);

                var driver = factory.GetLastDriver();
                Assert.AreEqual("GetAllComments", driver.GetQuery());
                Assert.AreEqual(driver.GetParamValue("@ResourceType"), resourceType);
                Assert.AreEqual(driver.GetParamValue("@ResourceId"), resourceId);
            }
        }
    }
}
