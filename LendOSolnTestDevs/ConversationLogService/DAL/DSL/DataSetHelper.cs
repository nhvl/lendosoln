﻿namespace LendingQB.Test.Developers.ConversationLogService.DAL.DSL
{
    using System;
    using System.Data;
    using System.Xml;

    internal static class DataSetHelper
    {
        public static void SetCommentSchema(DataTable dt)
        {
            dt.Columns.Add("ID", typeof(long));
            dt.Columns.Add("CreatedUTC", typeof(DateTime));
            dt.Columns.Add("ConversationId", typeof(long));
            dt.Columns.Add("CommenterId", typeof(long));
            dt.Columns.Add("Content", typeof(byte[]));
            dt.Columns.Add("HiddenByPersonId", typeof(long));
        }

        public static void AddCommentRow(DataTable dt, long id, DateTime utc, long conversationId, long commenterid, byte[] content)
        {
            var dr = dt.NewRow();
            dr["ID"] = id;
            dr["CreatedUTC"] = utc;
            dr["ConversationId"] = conversationId;
            dr["CommenterId"] = commenterid;
            dr["Content"] = content;
            dr["HiddenByPersonId"] = DBNull.Value;
            dt.Rows.Add(dr);
        }

        public static void SetCategorySchema(DataTable dt)
        {
            dt.Columns.Add("ID", typeof(long));
            dt.Columns.Add("OwnerId", typeof(string));
            dt.Columns.Add("CategoryName", typeof(string));
            dt.Columns.Add("Active", typeof(bool));
            dt.Columns.Add("DisplayName", typeof(string));
            dt.Columns.Add("DefaultPermissionLevelId", typeof(int));
        }

        public static void AddCategoryRow(DataTable dt, long id, string ownerId, string categoryName, bool active, string displayName, int? defaultPermissionLevelId)
        {
            var dr = dt.NewRow();
            dr["ID"] = id;
            dr["OwnerId"] = ownerId;
            dr["CategoryName"] = categoryName;
            dr["Active"] = active;
            dr["DisplayName"] = displayName;
            dr["DefaultPermissionLevelId"] = (object)defaultPermissionLevelId ?? DBNull.Value;
            dt.Rows.Add(dr);
        }

        public static void SetConversationSchema(DataTable dt)
        {
            dt.Columns.Add("ID", typeof(long));
            dt.Columns.Add("CategoryId", typeof(long));
            dt.Columns.Add("ResourceType", typeof(string));
            dt.Columns.Add("ResourceIdentifier", typeof(string));
            dt.Columns.Add("CommentTree", typeof(string)); // yes, a string and not SqlXml...that is how SQL Server returns the xml
            dt.Columns.Add("PermissionLevelId", typeof(int));
        }

        public static void AddConversationRow(DataTable dt, long id, long categoryId, string resourceType, string resourceId, XmlDocument doc, long? permissionLevelId)
        {
            var dr = dt.NewRow();
            dr["ID"] = id;
            dr["CategoryId"] = categoryId;
            dr["ResourceType"] = resourceType;
            dr["ResourceIdentifier"] = resourceId;
            dr["CommentTree"] = doc.DocumentElement.OuterXml;
            dr["PermissionLevelId"] = (object)permissionLevelId ?? DBNull.Value;
            dt.Rows.Add(dr);
        }

        public static void SetCommenterSchema(DataTable dt)
        {
            dt.Columns.Add("ID", typeof(long));
            dt.Columns.Add("Identifier", typeof(string));
            dt.Columns.Add("Fullname", typeof(string));
        }

        public static void AddCommenterRow(DataTable dt, long id, string identifier, string fullname)
        {
            var dr = dt.NewRow();
            dr["ID"] = id;
            dr["Identifier"] = identifier;
            dr["Fullname"] = fullname;
            dt.Rows.Add(dr);
        }
    }
}
