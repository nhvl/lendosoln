﻿namespace LendingQB.Test.Developers.ConversationLogService.DAL.DSL
{
    using System;
    using System.Data;
    using System.Xml;
    using LendersOffice.Drivers.SqlServerDB;
    using LqbGrammar.DataTypes;

    internal static class DataCallHelper
    {
        public static DataSet InsertOrUpdateCommenter(long id, string identifier, string fullname)
        {
            var ds = new DataSet();
            var dt = ds.Tables.Add();
            DataSetHelper.SetCommenterSchema(dt);
            DataSetHelper.AddCommenterRow(dt, id, identifier, fullname);
            return ds;
        }

        public static DataSet BeginConversation(long commentId, DateTime utc, long conversationId, long commenterId, byte[] content, long categoryId, string ownerId, string categoryName, bool active, string displayName, int? defaultPermissionLevelId)
        {
            var ds = new DataSet();

            var dt = ds.Tables.Add();
            DataSetHelper.SetCommentSchema(dt);
            DataSetHelper.AddCommentRow(dt, commentId, utc, conversationId, commenterId, content);

            dt = ds.Tables.Add();
            DataSetHelper.SetCategorySchema(dt);
            DataSetHelper.AddCategoryRow(dt, categoryId, ownerId, categoryName, active, displayName, defaultPermissionLevelId);

            return ds;
        }

        public static DataSet ReplyToComment(long commentId, DateTime utc, long conversationId, long commenterId, byte[] content)
        {
            var ds = new DataSet();
            var dt = ds.Tables.Add();
            DataSetHelper.SetCommentSchema(dt);
            DataSetHelper.AddCommentRow(dt, commentId, utc, conversationId, commenterId, content);
            return ds;
        }

        public static DataSet GetAllComments(string resourceType, string resourceId)
        {
            // Return a VERY SPECIFIC structure, because otherwise the interface will be too messy

            string ownerId = Guid.NewGuid().ToString();
            XmlDocument doc1 = FirstConversation();
            XmlDocument doc2 = SecondConversation();
            byte[] content = GenerateContent();

            var ds = new DataSet();

            // Conversations
            var dt = ds.Tables.Add();
            DataSetHelper.SetConversationSchema(dt);
            DataSetHelper.AddConversationRow(dt, (long)1, (long)1, resourceType, resourceId, doc1, null);
            DataSetHelper.AddConversationRow(dt, (long)2, (long)2, resourceType, resourceId, doc2, null);

            // Comments
            dt = ds.Tables.Add();
            DataSetHelper.SetCommentSchema(dt);
            DataSetHelper.AddCommentRow(dt, (long)1, DateTime.UtcNow.AddMinutes(-5), (long)1, (long)1, content);
            DataSetHelper.AddCommentRow(dt, (long)2, DateTime.UtcNow.AddMinutes(-4), (long)1, (long)2, content);
            DataSetHelper.AddCommentRow(dt, (long)3, DateTime.UtcNow.AddMinutes(-3), (long)1, (long)3, content);
            DataSetHelper.AddCommentRow(dt, (long)4, DateTime.UtcNow.AddMinutes(-2), (long)2, (long)3, content);
            DataSetHelper.AddCommentRow(dt, (long)5, DateTime.UtcNow.AddMinutes(-1), (long)2, (long)2, content);

            // Categories
            dt = ds.Tables.Add();
            DataSetHelper.SetCategorySchema(dt);
            DataSetHelper.AddCategoryRow(dt, (long)1, ownerId, "Lien Search", true, "Lean Sierch", null);
            DataSetHelper.AddCategoryRow(dt, (long)2, ownerId, "Fraud Check", true, "Frodd Cheque", null);

            // Commenters
            dt = ds.Tables.Add();
            DataSetHelper.SetCommenterSchema(dt);
            DataSetHelper.AddCommenterRow(dt, (long)1, Guid.NewGuid().ToString(), "Stanley Kubrik");
            DataSetHelper.AddCommenterRow(dt, (long)2, Guid.NewGuid().ToString(), "Fritz Lang");
            DataSetHelper.AddCommenterRow(dt, (long)3, Guid.NewGuid().ToString(), "Cecil B. DeMille");

            return ds;
        }

        public static DataSet InsertOrUpdateCategory(long categoryId, string ownerId, string categoryName, bool active, string displayName, int? defaultPermissionLevelId)
        {
            var ds = new DataSet();
            var dt = ds.Tables.Add();
            DataSetHelper.SetCategorySchema(dt);
            DataSetHelper.AddCategoryRow(dt, categoryId, ownerId, categoryName, active, displayName, defaultPermissionLevelId);
            return ds;
        }

        public static ModifiedRowCount DeleteCategoryOrders(int deletedCount)
        {
            return ModifiedRowCount.Create(deletedCount).Value;
        }


        public static Tuple<IDataReader, int> GetAllCategories(string ownerId)
        {
            // Return a VERY SPECIFIC structure, because otherwise the interface will be too messy

            int rowCount = 0;
            var reader = new MockDataReader();
            reader.BeginConstruction();
            reader.BeginTable();
            reader.BeginRow();
            reader.AddColumn("ID", (long)1);
            reader.AddColumn("OwnerId", ownerId);
            reader.AddColumn("CategoryName", "Check Liens");
            reader.AddColumn("Active", false);
            reader.AddColumn("DisplayName", "Check Leans");
            reader.AddColumn("DefaultPermissionLevelId", DBNull.Value);
            rowCount++;
            reader.BeginRow();
            reader.AddColumn("ID", (long)2);
            reader.AddColumn("OwnerId", ownerId);
            reader.AddColumn("CategoryName", "Check Nearby Prices");
            reader.AddColumn("Active", true);
            reader.AddColumn("DisplayName", "Check Nierby Prices");
            reader.AddColumn("DefaultPermissionLevelId", DBNull.Value);
            rowCount++;
            reader.BeginRow();
            reader.AddColumn("ID", (long)3);
            reader.AddColumn("OwnerId", ownerId);
            reader.AddColumn("CategoryName", "Tax Verification");
            reader.AddColumn("Active", true);
            reader.AddColumn("DisplayName", "Tax Verificashun");
            reader.AddColumn("DefaultPermissionLevelId", DBNull.Value);
            rowCount++;
            reader.EndConstruction();

            return new Tuple<IDataReader, int>(reader, rowCount);
        }

        public static byte[] GenerateContent()
        {
            string comment = "Now is the time for all good men to come to the aid of their country.";
            byte[] content = System.Text.Encoding.ASCII.GetBytes(comment);
            return content;
        }

        private static XmlDocument FirstConversation()
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml("<C/>");

            XmlElement root = doc.DocumentElement;
            root.SetAttribute("id", "1");

            XmlElement elem = (XmlElement)doc.CreateElement("C");
            elem.SetAttribute("id", "2");
            root.AppendChild(elem);

            elem = (XmlElement)doc.CreateElement("C");
            elem.SetAttribute("id", "3");
            root.AppendChild(elem);

            return doc;
        }

        private static XmlDocument SecondConversation()
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml("<C/>");

            XmlElement root = doc.DocumentElement;
            root.SetAttribute("id", "4");

            XmlElement elem = (XmlElement)doc.CreateElement("C");
            elem.SetAttribute("id", "5");
            root.AppendChild(elem);

            return doc;
        }
    }
}
