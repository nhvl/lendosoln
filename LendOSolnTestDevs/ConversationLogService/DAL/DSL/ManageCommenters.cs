﻿namespace LendingQB.Test.Developers.ConversationLogService.DAL.DSL
{
    using System;
    using ConversationLogLib;
    using LendersOffice.Drivers.SqlServerDB;
    using LqbGrammar.Drivers;
    using NUnit.Framework;
    using Utils;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    [Category(CategoryConstants.LqbMocks)]
    public sealed class ManageCommenters
    {
        [Test]
        public void InsertOrUpdateCommenter()
        {
            string targetId = Guid.NewGuid().ToString();
            string targetName = "Adolf Menjou";

            using (var helper = new FoolHelper())
            {
                var ds = DataCallHelper.InsertOrUpdateCommenter((long)1, targetId, targetName);

                var factory = new MockStoredProcedureDriverFactory(ds);
                if (helper.RegisterMockDriverFactory<IStoredProcedureDriverFactory>(factory))
                {
                    return;
                }

                // tests parsing code for returned data
                bool good = UnitTestHelper.DAL.DSL.ManageCommentersTester.InsertOrUpdateCommenter(targetId, targetName);
                Assert.IsTrue(good);

                // tests assumptions about the stored procedure call that would have been made
                var driver = factory.GetLastDriver();
                Assert.AreEqual("InsertOrUpdateCommenter", driver.GetQuery());
                Assert.AreEqual(targetId, driver.GetParamValue("@Identifier"));
                Assert.AreEqual(targetName, driver.GetParamValue("@Fullname"));
            }
        }
    }
}
