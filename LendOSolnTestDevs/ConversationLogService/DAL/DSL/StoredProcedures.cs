﻿namespace LendingQB.Test.Developers.ConversationLogService.DAL.DSL
{
    using ConversationLogLib;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.IntegrationTest)]
    [Ignore("VERIFIED")]
    public sealed class StoredProcedures
    {
        [Test]
        public void InsertOrUpdateCategory()
        {
            bool good = UnitTestHelper.DAL.DSL.StoredProcedureTester.ManageCategories.InsertOrUpdateCategory();
            Assert.IsTrue(good);
        }

        [Test]
        public void DeleteCategoryOrders()
        {
            bool good = UnitTestHelper.DAL.DSL.StoredProcedureTester.ManageCategories.DeleteCategoryOrders();
            Assert.IsTrue(good);
        }

        [Test]
        public void InsertCategoryOrder()
        {
            bool good = UnitTestHelper.DAL.DSL.StoredProcedureTester.ManageCategories.InsertCategoryOrder();
            Assert.IsTrue(good);
        }

        [Test]
        public void GetAllCategories()
        {
            bool good = UnitTestHelper.DAL.DSL.StoredProcedureTester.ManageCategories.GetAllCategories();
            Assert.IsTrue(good);
        }

        [Test]
        public void InsertOrUpdateCommenter()
        {
            bool good = UnitTestHelper.DAL.DSL.StoredProcedureTester.ManageCommenters.InsertOrUpdateCommenter();
            Assert.IsTrue(good);
        }

        [Test]
        public void BeginConversation()
        {
            bool good = UnitTestHelper.DAL.DSL.StoredProcedureTester.ManageConversations.BeginConversation();
            Assert.IsTrue(good);
        }

        [Test]
        public void ReplyToComment()
        {
            bool good = UnitTestHelper.DAL.DSL.StoredProcedureTester.ManageConversations.ReplyToComment();
            Assert.IsTrue(good);
        }

        [Test]
        public void GetAllComments()
        {
            bool good = UnitTestHelper.DAL.DSL.StoredProcedureTester.ManageConversations.GetAllComments();
            Assert.IsTrue(good);
        }
    }
}
