﻿namespace LendingQB.Test.Developers.ConversationLogService.DAL.DSL
{
    using System;
    using ConversationLogLib;
    using LendersOffice.Drivers.SqlServerDB;
    using LqbGrammar.Drivers;
    using NUnit.Framework;
    using Utils;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    [Category(CategoryConstants.LqbMocks)]
    public sealed class ManageCategories
    {
        [Test]
        public void InsertOrUpdateCategory()
        {
            var ownerId = Guid.NewGuid().ToString();
            var catName = "Loan Application PreQualification";
            var displayName = "LAP";
            var active = true;
            int? defaultPermissionLevelId = null;

            using (var helper = new FoolHelper())
            {
                var ds = DataCallHelper.InsertOrUpdateCategory((long)1, ownerId, catName, active, displayName, defaultPermissionLevelId);

                var factory = new MockStoredProcedureDriverFactory(ds);
                if (!helper.RegisterMockDriverFactory<IStoredProcedureDriverFactory>(factory))
                {
                    return;
                }

                bool good = UnitTestHelper.DAL.DSL.ManageCategoriesTester.InsertOrUpdateCatetory(ownerId, catName, active, displayName, defaultPermissionLevelId);
                Assert.IsTrue(good);

                var driver = factory.GetLastDriver();
                Assert.AreEqual("InsertOrUpdateCategory", driver.GetQuery());
                Assert.AreEqual(driver.GetParamValue("@OwnerId"), ownerId);
                Assert.AreEqual(driver.GetParamValue("@CategoryName"), catName);
                Assert.AreEqual(driver.GetParamValue("@Active"), active.ToString());
                Assert.AreEqual(driver.GetParamValue("@DisplayName"), displayName);
            }
        }

        [Test]
        public void DeleteCategoryOrders()
        {
            var ownerId = Guid.NewGuid().ToString();

            using (var helper = new FoolHelper())
            {
                var rowCount = DataCallHelper.DeleteCategoryOrders(3);

                var factory = new MockStoredProcedureDriverFactory(rowCount);
                if (!helper.RegisterMockDriverFactory<IStoredProcedureDriverFactory>(factory))
                {
                    return;
                }

                bool good = UnitTestHelper.DAL.DSL.ManageCategoriesTester.DeleteCategoryOrders(ownerId);
                Assert.IsTrue(good);

                var driver = factory.GetLastDriver();
                Assert.AreEqual("DeleteCategoryOrders", driver.GetQuery());
                Assert.AreEqual(driver.GetParamValue("@OwnerId"), ownerId);
            }
        }

        [Test]
        public void InsertCategoryOrder()
        {
            long catId = 12;
            int ordinal = 5;

            long fakePK = 1;

            using (var helper = new FoolHelper())
            {
                var factory = new MockStoredProcedureDriverFactory(fakePK);
                if (!helper.RegisterMockDriverFactory<IStoredProcedureDriverFactory>(factory))
                {
                    return;
                }

                bool good = UnitTestHelper.DAL.DSL.ManageCategoriesTester.InsertCategoryOrder(catId, ordinal);
                Assert.IsTrue(good);

                var driver = factory.GetLastDriver();
                Assert.AreEqual("InsertCategoryOrder", driver.GetQuery());
                Assert.AreEqual(driver.GetParamValue("@CategoryId"), catId.ToString());
                Assert.AreEqual(driver.GetParamValue("@Ordinal"), ordinal.ToString());
            }
        }

        [Test]
        public void GetAllCategories()
        {
            var ownerId = Guid.NewGuid().ToString();

            using (var helper = new FoolHelper())
            {
                var tuple = DataCallHelper.GetAllCategories(ownerId);
                int rowCount = tuple.Item2;
                var reader = tuple.Item1;

                var factory = new MockStoredProcedureDriverFactory(reader);
                if (!helper.RegisterMockDriverFactory<IStoredProcedureDriverFactory>(factory))
                {
                    return;
                }

                bool good = UnitTestHelper.DAL.DSL.ManageCategoriesTester.GetAllCategories(ownerId, rowCount);
                Assert.IsTrue(good);

                var driver = factory.GetLastDriver();
                Assert.AreEqual("GetAllCategories", driver.GetQuery());
                Assert.AreEqual(driver.GetParamValue("@OwnerId"), ownerId);
            }
        }
    }
}
