﻿namespace LendingQB.Test.Developers.ConversationLogService.DAL.DIL
{
    using System;
    using ConversationLogLib;
    using DSL;
    using LendersOffice.Drivers.SqlServerDB;
    using LqbGrammar.Drivers;
    using NUnit.Framework;
    using Utils;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    [Category(CategoryConstants.LqbMocks)]
    public sealed class CommentBridge
    {
        [Test]
        public void BeginConversation()
        {
            long commenterId = 212;
            long categoryId = 57;
            long genericId = 109;
            string userId = Guid.NewGuid().ToString();
            string fullname = "John Cleese";
            string resourceType = "LF";
            string resourceId = Guid.NewGuid().ToString();
            int? defaultPermissionLevelId = null;
            byte[] comment = DataCallHelper.GenerateContent();

            using (var helper = new FoolHelper())
            {
                var first = DataCallHelper.InsertOrUpdateCommenter(commenterId, userId, fullname);
                var second = DataCallHelper.BeginConversation(genericId, DateTime.UtcNow.AddMinutes(-1), genericId, commenterId, comment, categoryId, Guid.NewGuid().ToString(), "Silly Walks", true, "Sally Wilks", defaultPermissionLevelId);

                var factory = new MockSprocMultiCallDriverFactory(first, second);
                bool registered = helper.RegisterMockDriverFactory<IStoredProcedureDriverFactory>(factory);
                if (!registered)
                {
                    return;
                }

                bool good = UnitTestHelper.DAL.DIL.CommentBridgeTester.BeginConversation(userId, fullname, resourceType, resourceId, categoryId, comment, defaultPermissionLevelId);
                Assert.IsTrue(good);

                var driver = factory.GetCallDriver(0);
                Assert.AreEqual("InsertOrUpdateCommenter", driver.GetQuery());

                driver = factory.GetCallDriver(1);
                Assert.AreEqual("BeginConversation", driver.GetQuery());
            }
        }

        [Test]
        public void MakeReply()
        {
            long commenterId = 212;
            long conversationId = 34;
            string userId = Guid.NewGuid().ToString();
            string fullname = "Michael Palin";
            long commentId = 444;
            byte[] comment = DataCallHelper.GenerateContent();

            using (var helper = new FoolHelper())
            {
                var first = DataCallHelper.InsertOrUpdateCommenter(commenterId, userId, fullname);
                var second = DataCallHelper.ReplyToComment(commentId, DateTime.UtcNow.AddMinutes(-1), conversationId, commenterId, comment);

                var factory = new MockSprocMultiCallDriverFactory(first, second);
                if (!helper.RegisterMockDriverFactory<IStoredProcedureDriverFactory>(factory))
                {
                    return;
                }

                bool good = UnitTestHelper.DAL.DIL.CommentBridgeTester.MakeReply(userId, fullname, commentId, comment);
                Assert.IsTrue(good);

                var driver = factory.GetCallDriver(0);
                Assert.AreEqual("InsertOrUpdateCommenter", driver.GetQuery());

                driver = factory.GetCallDriver(1);
                Assert.AreEqual("ReplyToComment", driver.GetQuery());
            }
        }

        [Test]
        public void GetAllComments()
        {
            string resourceType = "LF";
            string resourceId = Guid.NewGuid().ToString();

            using (var helper = new FoolHelper())
            {
                var ds = DataCallHelper.GetAllComments(resourceType, resourceId);

                var factory = new MockStoredProcedureDriverFactory(ds);
                if (!helper.RegisterMockDriverFactory<IStoredProcedureDriverFactory>(factory))
                {
                    return;
                }

                bool good = UnitTestHelper.DAL.DIL.CommentBridgeTester.GetAllComments(resourceType, resourceId, ds.Tables[0].Rows.Count, ds.Tables[1].Rows.Count);
                Assert.IsTrue(good);

                var driver = factory.GetLastDriver();
                Assert.AreEqual("GetAllComments", driver.GetQuery());
            }
        }
    }
}
