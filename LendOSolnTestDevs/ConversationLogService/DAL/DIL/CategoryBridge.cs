﻿namespace LendingQB.Test.Developers.ConversationLogService.DAL.DIL
{
    using System;
    using ConversationLogLib;
    using DSL;
    using LendersOffice.Drivers.SqlServerDB;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;
    using NUnit.Framework;
    using Utils;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    [Category(CategoryConstants.LqbMocks)]
    public sealed class CategoryBridge
    {
        [Test]
        public void InsertOrUpdateCategory()
        {
            long categoryId = 987;
            string ownerId = Guid.NewGuid().ToString();
            string categoryName = "Create Chaos";
            string displayName = "Chaos";
            bool active = true;
            int? defaultPermissionLevelId = null;

            using (var helper = new FoolHelper())
            {
                var ds = DataCallHelper.InsertOrUpdateCategory(categoryId, ownerId, categoryName, active, displayName, defaultPermissionLevelId);
                var factory = new MockStoredProcedureDriverFactory(ds);
                if (!helper.RegisterMockDriverFactory<IStoredProcedureDriverFactory>(factory))
                {
                    return;
                }

                var good = UnitTestHelper.DAL.DIL.CategoryBridgeTester.InsertOrUpdateCategory(ownerId, categoryName, active, displayName, defaultPermissionLevelId);
                Assert.IsTrue(good);

                var driver = factory.GetLastDriver();
                Assert.AreEqual("InsertOrUpdateCategory", driver.GetQuery());
            }
        }

        [Test]
        public void GetAllCategories()
        {
            string ownerId = Guid.NewGuid().ToString();

            using (var helper = new FoolHelper())
            {
                var tuple = DataCallHelper.GetAllCategories(ownerId);
                var factory = new MockStoredProcedureDriverFactory(tuple.Item1);
                if (!helper.RegisterMockDriverFactory<IStoredProcedureDriverFactory>(factory))
                {
                    return;
                }

                var good = UnitTestHelper.DAL.DIL.CategoryBridgeTester.GetAllCategories(ownerId, tuple.Item2);
                Assert.IsTrue(good);

                var driver = factory.GetLastDriver();
                Assert.AreEqual("GetAllCategories", driver.GetQuery());
            }
        }

        [Test]
        [Ignore("Hard-coded PK is no longer in the DB.")]
        public void SetCategoryOrder()
        {
            string ownerId = Guid.NewGuid().ToString();
            long[] catIds = new long[] { 1, 2, 3, 4 };

            var deleteCount = ModifiedRowCount.Create(catIds.Length).Value;
            long pk = 33;

            using (var helper = new FoolHelper())
            {
                var factory = new MockSprocMultiCallDriverFactory(deleteCount, pk++, pk++, pk++, pk++);
                if (!helper.RegisterMockDriverFactory<IStoredProcedureDriverFactory>(factory))
                {
                    return;
                }

                var good = UnitTestHelper.DAL.DIL.CategoryBridgeTester.SetCategoryOrder(ownerId, catIds);
                Assert.IsTrue(good);

                var driver = factory.GetCallDriver(0);
                Assert.AreEqual("DeleteCategoryOrders", driver.GetQuery());

                driver = factory.GetCallDriver(1);
                Assert.AreEqual("InsertCategoryOrder", driver.GetQuery());

                driver = factory.GetCallDriver(2);
                Assert.AreEqual("InsertCategoryOrder", driver.GetQuery());

                driver = factory.GetCallDriver(3);
                Assert.AreEqual("InsertCategoryOrder", driver.GetQuery());

                driver = factory.GetCallDriver(4);
                Assert.AreEqual("InsertCategoryOrder", driver.GetQuery());
            }
        }
    }
}
