﻿namespace LendingQB.Test.Developers.CodeGeneration.LendingQB.Core.Data
{
    using System;
    using System.Data;
    using System.Data.Common;
    using global::DataAccess;
    using Developers.LendingQB.Core.Mapping;
    using global::LendingQB.Core;
    using global::LendingQB.Core.Data;
    using LendersOffice.Drivers.SqlServerDB;
    using LendersOffice.Security;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;
    using LqbGrammar.Exceptions;
    using NUnit.Framework;
    using Utils;

    [TestFixture]
    [Category(CategoryConstants.IntegrationTest)]
    public sealed class ManyToManyTest
    {
        const int MaxFirstCardinality = -1;
        const int MaxSecondCardinality = -1;

        private DbConnection connection;
        private IDbTransaction transaction;
        private IStoredProcedureDriver storedProcedureDriver;

        private Guid loanId;
        private AbstractUserPrincipal principal;

        [TestFixtureSetUp]
        public void FixtureSetup()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                this.CreateLoan();

                var factory = new StoredProcedureDriverFactory();
                this.storedProcedureDriver = factory.Create(TimeoutInSeconds.Default);
            }
        }

        [TestFixtureTearDown]
        public void FixtureTearDown()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                RealPropertyLiabilityMapperTest.InactiveLoanFile(this.loanId, this.principal);
            }
        }

        [SetUp]
        public void MethodSetUp()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                this.connection = DbConnectionInfo.GetConnectionInfo(this.principal.BrokerId).GetConnection();
                this.connection.Open();
                this.transaction = connection.BeginTransaction();
            }
        }

        [TearDown]
        public void MethodTearDown()
        {
            this.transaction.Rollback();
            this.transaction.Dispose();
            this.connection.Close();
        }

        [Test]
        public void AddMultiple()
        {
            var loanIdentifier = DataObjectIdentifier<DataObjectKind.Loan, Guid>.Create(this.loanId);
            var brokerIdentifier = DataObjectIdentifier<DataObjectKind.ClientCompany, Guid>.Create(this.principal.BrokerId);

            var property1 = RealPropertyCollectionMapperTest.GetDefaultTestRecord();
            var property2 = RealPropertyCollectionMapperTest.GetDefaultTestRecord();

            IMutableLqbCollection<DataObjectKind.RealProperty, Guid, RealProperty> collectionRP = LqbCollection<DataObjectKind.RealProperty, Guid, RealProperty>.Create(
                Name.Create("Property").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.RealProperty>());
            collectionRP.BeginTrackingChanges();
            var firstPropertyId = collectionRP.Add(property1);
            var secondPropertyId = collectionRP.Add(property2);

            var liability1 = LiabilityCollectionMapperTest.GetDefaultTestRecord();
            var liability2 = LiabilityCollectionMapperTest.GetDefaultTestRecord();
            IMutableLqbCollection<DataObjectKind.Liability, Guid, Liability> collectionL = LqbCollection<DataObjectKind.Liability, Guid, Liability>.Create(
                Name.Create("Liability").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.Liability>());
            collectionL.BeginTrackingChanges();
            var firstLiabilityId = collectionL.Add(liability1);
            var secondLiabilityId = collectionL.Add(liability2);

            var association11 = new RealPropertyLiabilityManyToMany(firstPropertyId, firstLiabilityId);
            var association12 = new RealPropertyLiabilityManyToMany(firstPropertyId, secondLiabilityId);
            var association21 = new RealPropertyLiabilityManyToMany(secondPropertyId, firstLiabilityId);
            var association22 = new RealPropertyLiabilityManyToMany(secondPropertyId, secondLiabilityId);
            IMutableLqbAssociationSet<DataObjectKind.RealPropertyLiabilityAssociation, Guid, RealPropertyLiabilityManyToMany> associationSet = LqbAssociationSet<DataObjectKind.RealPropertyLiabilityAssociation, Guid, RealPropertyLiabilityManyToMany>.Create(
                Name.Create("Test").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.RealPropertyLiabilityAssociation>(), MaxFirstCardinality, MaxSecondCardinality);
            associationSet.BeginTrackingChanges();
            var assocId11 = associationSet.Add(association11);
            var assocId12 = associationSet.Add(association12);
            var assocId21 = associationSet.Add(association21);
            var assocId22 = associationSet.Add(association22);

            Assert.AreEqual(4, associationSet.Count);
        }

        [Test]
        public void AddSame_ExpectFail()
        {
            var loanIdentifier = DataObjectIdentifier<DataObjectKind.Loan, Guid>.Create(this.loanId);
            var brokerIdentifier = DataObjectIdentifier<DataObjectKind.ClientCompany, Guid>.Create(this.principal.BrokerId);

            var property = RealPropertyCollectionMapperTest.GetDefaultTestRecord();
            IMutableLqbCollection<DataObjectKind.RealProperty, Guid, RealProperty> collectionRP = LqbCollection<DataObjectKind.RealProperty, Guid, RealProperty>.Create(
                Name.Create("Property").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.RealProperty>());
            collectionRP.BeginTrackingChanges();
            var propertyId = collectionRP.Add(property);

            var liability = LiabilityCollectionMapperTest.GetDefaultTestRecord();
            IMutableLqbCollection<DataObjectKind.Liability, Guid, Liability> collectionL = LqbCollection<DataObjectKind.Liability, Guid, Liability>.Create(
                Name.Create("Liability").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.Liability>());
            collectionL.BeginTrackingChanges();
            var liabilityId = collectionL.Add(liability);

            var association1 = new RealPropertyLiabilityManyToMany(propertyId, liabilityId);
            var association2 = new RealPropertyLiabilityManyToMany(propertyId, liabilityId);
            IMutableLqbAssociationSet<DataObjectKind.RealPropertyLiabilityAssociation, Guid, RealPropertyLiabilityManyToMany> associationSet = LqbAssociationSet<DataObjectKind.RealPropertyLiabilityAssociation, Guid, RealPropertyLiabilityManyToMany>.Create(
                Name.Create("Test").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.RealPropertyLiabilityAssociation>(), MaxFirstCardinality, MaxSecondCardinality);
            associationSet.BeginTrackingChanges();
            var assocId11 = associationSet.Add(association1);

            Assert.Throws<CBaseException>(() => associationSet.Add(association2));
        }

        private void CreateLoan()
        {
            var tuple = RealPropertyLiabilityMapperTest.CreatePrincipleAndNewLoan();
            this.principal = tuple.Item1;
            this.loanId = tuple.Item2;
        }
    }
}
