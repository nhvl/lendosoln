﻿namespace LendingQB.Test.Developers.CodeGeneration.LendingQB.Core.Data
{
    using System;
    using global::DataAccess;
    using global::LendingQB.Core.Data;
    using LqbGrammar.DataTypes;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public sealed class RepperTest
    {
        [Test]
        public void TestCreateEntity()
        {
            var entity = this.InitializeEntity();
            Assert.IsNotNull(entity);
            Assert.IsTrue(entity.BankAccountNumber != null);
        }

        [Test]
        public void TestRepperEmpty()
        {
            var entity = this.CreateEntity();

            var factory = new TestEntity.StringFormatterFactory();
            var repper = factory.Create(entity, null, null);

            Assert.AreEqual(string.Empty, repper.BankAccountNumber);
            Assert.AreEqual(string.Empty, repper.City);
            Assert.AreEqual(string.Empty, repper.CityState);
            Assert.AreEqual(string.Empty, repper.Country);
            Assert.AreEqual(string.Empty, repper.CountString);
            Assert.AreEqual(string.Empty, repper.DependentAges);
            Assert.AreEqual(string.Empty, repper.DescriptionField);
            Assert.AreEqual(string.Empty, repper.EmailAddress);
            Assert.AreEqual(string.Empty, repper.EntityName);
            Assert.AreEqual(string.Empty, repper.FhaCode);
            Assert.AreEqual(string.Empty, repper.FicoCreditScore);
            Assert.AreEqual(string.Empty, repper.FormattedString);
            Assert.AreEqual(string.Empty, repper.Instant);
            Assert.AreEqual(string.Empty, repper.IRSIdentifier);
            Assert.AreEqual(string.Empty, repper.LiabilityRemainingMonths);
            Assert.AreEqual(string.Empty, repper.LongDescriptionField);
            Assert.AreEqual(string.Empty, repper.LqbMismoIdentifier);
            Assert.AreEqual(string.Empty, repper.MilitaryServiceNumber);
            Assert.AreEqual(string.Empty, repper.Money);
            Assert.AreEqual(string.Empty, repper.Month);
            Assert.AreEqual(string.Empty, repper.Name);
            Assert.AreEqual(string.Empty, repper.NameSuffix);
            Assert.AreEqual(string.Empty, repper.OrderRank);
            Assert.AreEqual(string.Empty, repper.Percentage);
            Assert.AreEqual(string.Empty, repper.Percentile);
            Assert.AreEqual(string.Empty, repper.PersonName);
            Assert.AreEqual(string.Empty, repper.PhoneNumber);
            Assert.AreEqual(string.Empty, repper.SocialSecurityNumber);
            Assert.AreEqual(string.Empty, repper.StreetAddress);
            Assert.AreEqual(string.Empty, repper.TaxFormNumber);
            Assert.AreEqual(string.Empty, repper.ThirdPartyIdentifier);
            Assert.AreEqual(string.Empty, repper.UnitedStatesPostalState);
            Assert.AreEqual(string.Empty, repper.UnitedStatesState);
            Assert.AreEqual(string.Empty, repper.UnzonedDate);
            Assert.AreEqual(string.Empty, repper.VaDateOfLoan);
            Assert.AreEqual(string.Empty, repper.VaEntitlementCode);
            Assert.AreEqual(string.Empty, repper.VaLoanNumber);
            Assert.AreEqual(string.Empty, repper.Year);
            Assert.AreEqual(string.Empty, repper.YearAsString);
            Assert.AreEqual(string.Empty, repper.Zipcode);
        }

        [Test]
        public void TestRepperFull()
        {
            var entity = this.InitializeEntity();

            var factory = new TestEntity.StringFormatterFactory();
            var repper = factory.Create(entity, null, null);

            Assert.IsFalse(string.IsNullOrEmpty(repper.BankAccountNumber));
            Assert.IsFalse(string.IsNullOrEmpty(repper.Boolean));
            Assert.IsFalse(string.IsNullOrEmpty(repper.BooleanKleene));
            Assert.IsFalse(string.IsNullOrEmpty(repper.City));
            Assert.IsFalse(string.IsNullOrEmpty(repper.CityState));
            Assert.IsFalse(string.IsNullOrEmpty(repper.Country));
            Assert.IsFalse(string.IsNullOrEmpty(repper.CountString));
            Assert.IsFalse(string.IsNullOrEmpty(repper.DependentAges));
            Assert.IsFalse(string.IsNullOrEmpty(repper.DescriptionField));
            Assert.IsFalse(string.IsNullOrEmpty(repper.EmailAddress));
            Assert.IsFalse(string.IsNullOrEmpty(repper.EntityName));
            Assert.IsFalse(string.IsNullOrEmpty(repper.FhaCode));
            Assert.IsFalse(string.IsNullOrEmpty(repper.FicoCreditScore));
            Assert.IsFalse(string.IsNullOrEmpty(repper.FormattedString));
            Assert.IsFalse(string.IsNullOrEmpty(repper.Fuzzy));
            Assert.IsFalse(string.IsNullOrEmpty(repper.Instant));
            Assert.IsFalse(string.IsNullOrEmpty(repper.IRSIdentifier));
            Assert.IsFalse(string.IsNullOrEmpty(repper.LiabilityRemainingMonths));
            Assert.IsFalse(string.IsNullOrEmpty(repper.LongDescriptionField));
            Assert.IsFalse(string.IsNullOrEmpty(repper.LqbMismoIdentifier));
            Assert.IsFalse(string.IsNullOrEmpty(repper.MilitaryServiceNumber));
            Assert.IsFalse(string.IsNullOrEmpty(repper.Money));
            Assert.IsFalse(string.IsNullOrEmpty(repper.Month));
            Assert.IsFalse(string.IsNullOrEmpty(repper.Name));
            Assert.IsFalse(string.IsNullOrEmpty(repper.NameSuffix));
            Assert.IsFalse(string.IsNullOrEmpty(repper.OrderRank));
            Assert.IsFalse(string.IsNullOrEmpty(repper.Percentage));
            Assert.IsFalse(string.IsNullOrEmpty(repper.Percentile));
            Assert.IsFalse(string.IsNullOrEmpty(repper.PersonName));
            Assert.IsFalse(string.IsNullOrEmpty(repper.PhoneNumber));
            Assert.IsFalse(string.IsNullOrEmpty(repper.SocialSecurityNumber));
            Assert.IsFalse(string.IsNullOrEmpty(repper.StreetAddress));
            Assert.IsFalse(string.IsNullOrEmpty(repper.TaxFormNumber));
            Assert.IsFalse(string.IsNullOrEmpty(repper.ThirdPartyIdentifier));
            Assert.IsFalse(string.IsNullOrEmpty(repper.UnitedStatesPostalState));
            Assert.IsFalse(string.IsNullOrEmpty(repper.UnitedStatesState));
            Assert.IsFalse(string.IsNullOrEmpty(repper.UnzonedDate));
            Assert.IsFalse(string.IsNullOrEmpty(repper.VaDateOfLoan));
            Assert.IsFalse(string.IsNullOrEmpty(repper.VaEntitlementCode));
            Assert.IsFalse(string.IsNullOrEmpty(repper.VaLoanNumber));
            Assert.IsFalse(string.IsNullOrEmpty(repper.Year));
            Assert.IsFalse(string.IsNullOrEmpty(repper.YearAsString));
            Assert.IsFalse(string.IsNullOrEmpty(repper.Zipcode));
        }

        [Test]
        public void ParserTest()
        {
            // Quick check to ensure that the setter methods for the repper work.
            var entity = this.InitializeEntity();

            var factory = new TestEntity.StringFormatterFactory();
            var repper = factory.Create(entity, null, null);

            string changedValue = "NEW_VALUE";
            repper.BankAccountNumber = changedValue;

            Assert.AreEqual(changedValue, entity.BankAccountNumber.Value.ToString());
        }

        private TestEntity CreateEntity()
        {
            return new TestEntity();
        }

        private TestEntity InitializeEntity()
        {
            var entity = new TestEntity();

            entity.BankAccountNumber = BankAccountNumber.Create("BAC_445566");
            entity.Boolean = true;
            entity.BooleanKleene = BooleanKleene.Indeterminant;
            entity.City = City.Create("Newark");
            entity.CityState = CityState.Create("Newark, NJ");
            entity.Country = Country.Create("US");
            entity.CountString = CountString.Create("666");
            entity.DependentAges = DependentAges.Create("13, 14, 21");
            entity.DescriptionField = DescriptionField.Create("It was a lovely yellow with black top.");
            entity.EmailAddress = EmailAddress.Create("goofy@loonytunes.com");
            entity.EntityName = EntityName.Create("NT Industries");
            entity.FhaCode = FhaCode.Create("FHA-111222333");
            entity.FicoCreditScore = FicoCreditScore.Create(812);
            entity.FormattedString = FormattedString.Create("Field0\tField1\tField2");
            entity.Fuzzy = E_TriState.Yes;
            entity.Instant = Instant.Create(DateTime.Now);
            entity.IRSIdentifier = IRSIdentifier.Create("111-22-3344");
            entity.LiabilityRemainingMonths = LiabilityRemainingMonths.Create("7");
            entity.LongDescriptionField = LongDescriptionField.Create("Huey was a corrupt governor of Louisiana");
            entity.LqbMismoIdentifier = LqbMismoIdentifier.Create("BOR_0001");
            entity.MilitaryServiceNumber = MilitaryServiceNumber.Create("ZZ-33-335567");
            entity.Money = Money.Create(0.25m);
            entity.Month = Month.Create(3);
            entity.Name = Name.Create("Jiminy Cricket");
            entity.NameSuffix = NameSuffix.Create("JR");
            entity.OrderRank = OrderRank.Create(25);
            entity.Percentage = Percentage.Create(25m);
            entity.Percentile = Percentile.Create(89);
            entity.PersonName = PersonName.Create("Zebediah Outlandish");
            entity.PhoneNumber = PhoneNumber.Create("714-555-9445");
            entity.SocialSecurityNumber = SocialSecurityNumber.Create("111-22-3333");
            entity.StreetAddress = StreetAddress.Create("1313 Mockingbird Ln");
            entity.TaxFormNumber = TaxFormNumber.Create("TX--3322");
            entity.ThirdPartyIdentifier = ThirdPartyIdentifier.Create("Q96-5318008");
            entity.UnitedStatesPostalState = UnitedStatesPostalState.Create("NJ");
            entity.UnitedStatesState = UnitedStatesState.Create("NJ");
            entity.UnzonedDate = UnzonedDate.Create(2018, 3, 24);
            entity.VaDateOfLoan = VaDateOfLoan.Create("3/24/2018");
            entity.VaEntitlementCode = VaEntitlementCode.Create("I-WANT-IT-NOW");
            entity.VaLoanNumber = VaLoanNumber.Create("VA-0123");
            entity.Year = Year.Create(2000);
            entity.YearAsString = YearAsString.Create("2000");
            entity.Zipcode = Zipcode.Create("11332");

            return entity;
        }
    }
}
