﻿namespace LendingQB.Test.Developers.CodeGeneration.LendingQB.Core.Data
{
    using System;
    using System.Data;
    using System.Data.Common;
    using Developers.LendingQB.Core.Mapping;
    using global::DataAccess;
    using global::LendingQB.Core;
    using global::LendingQB.Core.Data;
    using LendersOffice.Drivers.SqlServerDB;
    using LendersOffice.Security;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;
    using LqbGrammar.Exceptions;
    using NUnit.Framework;
    using Utils;

    [TestFixture]
    [Category(CategoryConstants.IntegrationTest)]
    public sealed class TwoToThreeTest
    {
        // NOTE: cardinalities are set based on value of the OTHER phrase
        const int MaxFirstCardinality = 3;
        const int MaxSecondCardinality = 2;

        private DbConnection connection;
        private IDbTransaction transaction;
        private IStoredProcedureDriver storedProcedureDriver;

        private Guid loanId;
        private AbstractUserPrincipal principal;

        [TestFixtureSetUp]
        public void FixtureSetup()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                this.CreateLoan();

                var factory = new StoredProcedureDriverFactory();
                this.storedProcedureDriver = factory.Create(TimeoutInSeconds.Default);
            }
        }

        [TestFixtureTearDown]
        public void FixtureTearDown()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                RealPropertyLiabilityMapperTest.InactiveLoanFile(this.loanId, this.principal);
            }
        }

        [SetUp]
        public void MethodSetUp()
        {
            using (var helper = new FoolHelper())
            {
                helper.RegisterRealDriver(FoolHelper.DriverType.SqlQuery);

                this.connection = DbConnectionInfo.GetConnectionInfo(this.principal.BrokerId).GetConnection();
                this.connection.Open();
                this.transaction = connection.BeginTransaction();
            }
        }

        [TearDown]
        public void MethodTearDown()
        {
            this.transaction.Rollback();
            this.transaction.Dispose();
            this.connection.Close();
        }

        [Test]
        public void AddMultiple()
        {
            var loanIdentifier = DataObjectIdentifier<DataObjectKind.Loan, Guid>.Create(this.loanId);
            var brokerIdentifier = DataObjectIdentifier<DataObjectKind.ClientCompany, Guid>.Create(this.principal.BrokerId);

            var property1 = RealPropertyCollectionMapperTest.GetDefaultTestRecord();
            var property2 = RealPropertyCollectionMapperTest.GetDefaultTestRecord();
            IMutableLqbCollection<DataObjectKind.RealProperty, Guid, RealProperty> collectionRP = LqbCollection<DataObjectKind.RealProperty, Guid, RealProperty>.Create(
                Name.Create("Property").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.RealProperty>());
            collectionRP.BeginTrackingChanges();
            var firstPropertyId = collectionRP.Add(property1);
            var secondPropertyId = collectionRP.Add(property2);

            var liability1 = LiabilityCollectionMapperTest.GetDefaultTestRecord();
            var liability2 = LiabilityCollectionMapperTest.GetDefaultTestRecord();
            var liability3 = LiabilityCollectionMapperTest.GetDefaultTestRecord();
            IMutableLqbCollection<DataObjectKind.Liability, Guid, Liability> collectionL = LqbCollection<DataObjectKind.Liability, Guid, Liability>.Create(
                Name.Create("Liability").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.Liability>());
            collectionL.BeginTrackingChanges();
            var firstLiabilityId = collectionL.Add(liability1);
            var secondLiabilityId = collectionL.Add(liability2);
            var thirdLiabilityId = collectionL.Add(liability2);

            var association11 = new RealPropertyLiabilityTwoToThree(firstPropertyId, firstLiabilityId);
            var association12 = new RealPropertyLiabilityTwoToThree(firstPropertyId, secondLiabilityId);
            var association13 = new RealPropertyLiabilityTwoToThree(firstPropertyId, thirdLiabilityId);
            var association21 = new RealPropertyLiabilityTwoToThree(secondPropertyId, firstLiabilityId);
            var association22 = new RealPropertyLiabilityTwoToThree(secondPropertyId, secondLiabilityId);
            var association23 = new RealPropertyLiabilityTwoToThree(secondPropertyId, thirdLiabilityId);
            IMutableLqbAssociationSet<DataObjectKind.RealPropertyLiabilityAssociation, Guid, RealPropertyLiabilityTwoToThree> associationSet = LqbAssociationSet<DataObjectKind.RealPropertyLiabilityAssociation, Guid, RealPropertyLiabilityTwoToThree>.Create(
                Name.Create("Test").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.RealPropertyLiabilityAssociation>(), MaxFirstCardinality, MaxSecondCardinality);
            associationSet.BeginTrackingChanges();
            var assocId11 = associationSet.Add(association11);
            var assocId12 = associationSet.Add(association12);
            var assocId13 = associationSet.Add(association13);
            var assocId21 = associationSet.Add(association21);
            var assocId22 = associationSet.Add(association22);
            var assocId23 = associationSet.Add(association23);

            Assert.AreEqual(6, associationSet.Count);
        }

        [Test]
        public void TooManyLiabilities_ExpectFail()
        {
            var loanIdentifier = DataObjectIdentifier<DataObjectKind.Loan, Guid>.Create(this.loanId);
            var brokerIdentifier = DataObjectIdentifier<DataObjectKind.ClientCompany, Guid>.Create(this.principal.BrokerId);

            var property1 = RealPropertyCollectionMapperTest.GetDefaultTestRecord();
            var property2 = RealPropertyCollectionMapperTest.GetDefaultTestRecord();
            IMutableLqbCollection<DataObjectKind.RealProperty, Guid, RealProperty> collectionRP = LqbCollection<DataObjectKind.RealProperty, Guid, RealProperty>.Create(
                Name.Create("Property").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.RealProperty>());
            collectionRP.BeginTrackingChanges();
            var firstPropertyId = collectionRP.Add(property1);
            var secondPropertyId = collectionRP.Add(property2);

            var liability1 = LiabilityCollectionMapperTest.GetDefaultTestRecord();
            var liability2 = LiabilityCollectionMapperTest.GetDefaultTestRecord();
            var liability3 = LiabilityCollectionMapperTest.GetDefaultTestRecord();
            var liability4 = LiabilityCollectionMapperTest.GetDefaultTestRecord();
            IMutableLqbCollection<DataObjectKind.Liability, Guid, Liability> collectionL = LqbCollection<DataObjectKind.Liability, Guid, Liability>.Create(
                Name.Create("Liability").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.Liability>());
            collectionL.BeginTrackingChanges();
            var firstLiabilityId = collectionL.Add(liability1);
            var secondLiabilityId = collectionL.Add(liability2);
            var thirdLiabilityId = collectionL.Add(liability2);
            var fourthLiabilityId = collectionL.Add(liability2);

            var association11 = new RealPropertyLiabilityTwoToThree(firstPropertyId, firstLiabilityId);
            var association12 = new RealPropertyLiabilityTwoToThree(firstPropertyId, secondLiabilityId);
            var association13 = new RealPropertyLiabilityTwoToThree(firstPropertyId, thirdLiabilityId);
            var association14 = new RealPropertyLiabilityTwoToThree(firstPropertyId, fourthLiabilityId);
            var association21 = new RealPropertyLiabilityTwoToThree(secondPropertyId, firstLiabilityId);
            var association22 = new RealPropertyLiabilityTwoToThree(secondPropertyId, secondLiabilityId);
            var association23 = new RealPropertyLiabilityTwoToThree(secondPropertyId, thirdLiabilityId);
            var association24 = new RealPropertyLiabilityTwoToThree(secondPropertyId, fourthLiabilityId);
            IMutableLqbAssociationSet<DataObjectKind.RealPropertyLiabilityAssociation, Guid, RealPropertyLiabilityTwoToThree> associationSet = LqbAssociationSet<DataObjectKind.RealPropertyLiabilityAssociation, Guid, RealPropertyLiabilityTwoToThree>.Create(
                Name.Create("Test").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.RealPropertyLiabilityAssociation>(), MaxFirstCardinality, MaxSecondCardinality);
            associationSet.BeginTrackingChanges();
            var assocId11 = associationSet.Add(association11);
            var assocId12 = associationSet.Add(association12);
            var assocId13 = associationSet.Add(association13);

            Assert.Throws<CBaseException>(() => associationSet.Add(association14));

            var assocId21 = associationSet.Add(association21);
            var assocId22 = associationSet.Add(association22);
            var assocId23 = associationSet.Add(association23);

            Assert.Throws<CBaseException>(() => associationSet.Add(association24));
        }

        [Test]
        public void TooManyProperties_ExpectFail()
        {
            var loanIdentifier = DataObjectIdentifier<DataObjectKind.Loan, Guid>.Create(this.loanId);
            var brokerIdentifier = DataObjectIdentifier<DataObjectKind.ClientCompany, Guid>.Create(this.principal.BrokerId);

            var property1 = RealPropertyCollectionMapperTest.GetDefaultTestRecord();
            var property2 = RealPropertyCollectionMapperTest.GetDefaultTestRecord();
            var property3 = RealPropertyCollectionMapperTest.GetDefaultTestRecord();
            IMutableLqbCollection<DataObjectKind.RealProperty, Guid, RealProperty> collectionRP = LqbCollection<DataObjectKind.RealProperty, Guid, RealProperty>.Create(
                Name.Create("Property").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.RealProperty>());
            collectionRP.BeginTrackingChanges();
            var firstPropertyId = collectionRP.Add(property1);
            var secondPropertyId = collectionRP.Add(property2);
            var thirdPropertyId = collectionRP.Add(property2);

            var liability1 = LiabilityCollectionMapperTest.GetDefaultTestRecord();
            var liability2 = LiabilityCollectionMapperTest.GetDefaultTestRecord();
            var liability3 = LiabilityCollectionMapperTest.GetDefaultTestRecord();
            IMutableLqbCollection<DataObjectKind.Liability, Guid, Liability> collectionL = LqbCollection<DataObjectKind.Liability, Guid, Liability>.Create(
                Name.Create("Liability").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.Liability>());
            collectionL.BeginTrackingChanges();
            var firstLiabilityId = collectionL.Add(liability1);
            var secondLiabilityId = collectionL.Add(liability2);
            var thirdLiabilityId = collectionL.Add(liability2);

            var association11 = new RealPropertyLiabilityTwoToThree(firstPropertyId, firstLiabilityId);
            var association12 = new RealPropertyLiabilityTwoToThree(firstPropertyId, secondLiabilityId);
            var association13 = new RealPropertyLiabilityTwoToThree(firstPropertyId, thirdLiabilityId);
            var association21 = new RealPropertyLiabilityTwoToThree(secondPropertyId, firstLiabilityId);
            var association22 = new RealPropertyLiabilityTwoToThree(secondPropertyId, secondLiabilityId);
            var association23 = new RealPropertyLiabilityTwoToThree(secondPropertyId, thirdLiabilityId);
            var association31 = new RealPropertyLiabilityTwoToThree(thirdPropertyId, firstLiabilityId);
            var association32 = new RealPropertyLiabilityTwoToThree(thirdPropertyId, secondLiabilityId);
            var association33 = new RealPropertyLiabilityTwoToThree(thirdPropertyId, thirdLiabilityId);
            IMutableLqbAssociationSet<DataObjectKind.RealPropertyLiabilityAssociation, Guid, RealPropertyLiabilityTwoToThree> associationSet = LqbAssociationSet<DataObjectKind.RealPropertyLiabilityAssociation, Guid, RealPropertyLiabilityTwoToThree>.Create(
                Name.Create("Test").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.RealPropertyLiabilityAssociation>(), MaxFirstCardinality, MaxSecondCardinality);
            associationSet.BeginTrackingChanges();
            var assocId11 = associationSet.Add(association11);
            var assocId12 = associationSet.Add(association12);
            var assocId13 = associationSet.Add(association13);
            var assocId21 = associationSet.Add(association21);
            var assocId22 = associationSet.Add(association22);
            var assocId23 = associationSet.Add(association23);

            Assert.Throws<CBaseException>(() => associationSet.Add(association31));

            Assert.Throws<CBaseException>(() => associationSet.Add(association32));

            Assert.Throws<CBaseException>(() => associationSet.Add(association33));
        }

        [Test]
        public void AddSame_ExpectFail()
        {
            var loanIdentifier = DataObjectIdentifier<DataObjectKind.Loan, Guid>.Create(this.loanId);
            var brokerIdentifier = DataObjectIdentifier<DataObjectKind.ClientCompany, Guid>.Create(this.principal.BrokerId);

            var property = RealPropertyCollectionMapperTest.GetDefaultTestRecord();
            IMutableLqbCollection<DataObjectKind.RealProperty, Guid, RealProperty> collectionRP = LqbCollection<DataObjectKind.RealProperty, Guid, RealProperty>.Create(
                Name.Create("Property").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.RealProperty>());
            collectionRP.BeginTrackingChanges();
            var propertyId = collectionRP.Add(property);

            var liability = LiabilityCollectionMapperTest.GetDefaultTestRecord();
            IMutableLqbCollection<DataObjectKind.Liability, Guid, Liability> collectionL = LqbCollection<DataObjectKind.Liability, Guid, Liability>.Create(
                Name.Create("Liability").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.Liability>());
            collectionL.BeginTrackingChanges();
            var liabilityId = collectionL.Add(liability);

            var association1 = new RealPropertyLiabilityTwoToThree(propertyId, liabilityId);
            var association2 = new RealPropertyLiabilityTwoToThree(propertyId, liabilityId);
            IMutableLqbAssociationSet<DataObjectKind.RealPropertyLiabilityAssociation, Guid, RealPropertyLiabilityTwoToThree> associationSet = LqbAssociationSet<DataObjectKind.RealPropertyLiabilityAssociation, Guid, RealPropertyLiabilityTwoToThree>.Create(
                Name.Create("Test").Value,
                new GuidDataObjectIdentifierFactory<DataObjectKind.RealPropertyLiabilityAssociation>(), MaxFirstCardinality, MaxSecondCardinality);
            associationSet.BeginTrackingChanges();
            var assocId11 = associationSet.Add(association1);

            Assert.Throws<CBaseException>(() => associationSet.Add(association2));
        }

        private void CreateLoan()
        {
            var tuple = RealPropertyLiabilityMapperTest.CreatePrincipleAndNewLoan();
            this.principal = tuple.Item1;
            this.loanId = tuple.Item2;
        }
    }
}
