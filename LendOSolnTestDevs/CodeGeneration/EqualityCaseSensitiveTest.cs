﻿namespace LendingQB.Test.Developers.CodeGeneration
{
    using LqbGrammar.DataTypes;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public class EqualityCaseSensitiveTest
    {
        [Test]
        public void IsCaseSensitive()
        {
            string test = "Everyting counts in large amounts";
            string lower = test.ToLower();

            var mixed = EqualityCaseSensitiveTrue.Create(test).Value;
            var pure = EqualityCaseSensitiveTrue.Create(lower).Value;

            int hashMixed = mixed.GetHashCode();
            int hashPure = pure.GetHashCode();

            Assert.IsFalse(mixed == pure);
            Assert.AreNotEqual(hashMixed, hashPure);
        }

        [Test]
        public void IsNotCaseSensitive()
        {
            string test = "He's a Pinball Wizard, there has to be a twist, a Pinball Wizard with such a supple wrist.";
            string lower = test.ToLower();

            var mixed = EqualityCaseSensitiveFalse.Create(test).Value;
            var pure = EqualityCaseSensitiveFalse.Create(lower).Value;

            int hashMixed = mixed.GetHashCode();
            int hashPure = pure.GetHashCode();

            Assert.IsTrue(mixed == pure);
            Assert.AreEqual(hashMixed, hashPure);
        }

        [TestCase(null)]
        [TestCase("")]
        [TestCase("  \t\r\n ")]
        public void NullOrWhitespaceIsInvalid(string input)
        {
            Assert.IsNull(EqualityCaseSensitiveTrue.Create(input));
        }

        [TestCase("A clean value, without leading or trailing whitespace.")]
        [TestCase("    \t   A clean value, without leading or trailing whitespace.")]
        [TestCase("A clean value, without leading or trailing whitespace.   \r\n         ")]
        public void FieldValuesAreTrimmed(string value)
        {
            EqualityCaseSensitiveTrue expected = EqualityCaseSensitiveTrue.Create("A clean value, without leading or trailing whitespace.").Value;
            Assert.AreEqual(expected, EqualityCaseSensitiveTrue.Create(value));
        }
    }
}
