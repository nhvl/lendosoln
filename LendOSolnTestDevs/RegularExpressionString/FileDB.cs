﻿namespace LendingQB.Test.Developers.RegularExpressionString
{
    using System;
    using DataAccess;
    using global::DataAccess;
    using LqbGrammar.DataTypes;
    using NUnit.Framework;
    using Utils;

    [TestFixture]
    public class FileDB
    {
        [Test]
        [Category(CategoryConstants.IntegrationTest)]
        public void FileDBNames()
        {
            using (var helper = new FoolHelper())
            {
                if (!helper.RegisterRealDriver(FoolHelper.DriverType.ConfigurationQuery))
                {
                    return;
                }

                string name = FileDBTools.GetDatabaseName(E_FileDB.Normal);
                FileStorageIdentifier? id = FileStorageIdentifier.TryParse(name);
                Assert.IsTrue(id.HasValue);

                name = FileDBTools.GetDatabaseName(E_FileDB.EDMS);
                id = FileStorageIdentifier.TryParse(name);
                Assert.IsTrue(id.HasValue);

                name = FileDBTools.GetDatabaseName(E_FileDB.Temp);
                id = FileStorageIdentifier.TryParse(name);
                Assert.IsTrue(id.HasValue);
            }
        }

        [Test]
        [Category(CategoryConstants.UnitTest)]
        public void Simple()
        {
            string key = Guid.NewGuid().ToString();
            FileIdentifier? id = FileIdentifier.TryParse(key);
            Assert.IsTrue(id.HasValue);
        }

        [Test]
        [Category(CategoryConstants.UnitTest)]
        public void SimpleN()
        {
            string key = Guid.NewGuid().ToString("N");
            FileIdentifier? id = FileIdentifier.TryParse(key);
            Assert.IsTrue(id.HasValue);
        }

        [Test]
        [Category(CategoryConstants.UnitTest)]
        public void Prefixed()
        {
            string key = Guid.NewGuid().ToString("N");

            string prefixed = "DU_FINDINGS_HTML_" + key;
            FileIdentifier? id = FileIdentifier.TryParse(prefixed);
            Assert.IsTrue(id.HasValue);

            prefixed = "FREDDIE_XML_" + key;
            id = FileIdentifier.TryParse(prefixed);
            Assert.IsTrue(id.HasValue);

            prefixed = "LPEFirstResult_" + key;
            id = FileIdentifier.TryParse(prefixed);
            Assert.IsTrue(id.HasValue);

            prefixed = "LPESecondResult_" + key;
            id = FileIdentifier.TryParse(prefixed);
            Assert.IsTrue(id.HasValue);

            key = Guid.NewGuid().ToString();
            prefixed = "audit_" + key;
            id = FileIdentifier.TryParse(prefixed);
            Assert.IsTrue(id.HasValue);
        }

        [Test]
        [Category(CategoryConstants.UnitTest)]
        public void Suffixed()
        {
            string key = Guid.NewGuid().ToString("N");

            string suffixed = key + "_GLOBAL_DMS_INFO_XML";
            FileIdentifier? id = FileIdentifier.TryParse(suffixed);
            Assert.IsTrue(id.HasValue);

            suffixed = key + "_EDOCS_ANNOTATIONS_XML";
            id = FileIdentifier.TryParse(suffixed);
            Assert.IsTrue(id.HasValue);

            key = Guid.NewGuid().ToString();
            suffixed = key + ".logo.gif";
            id = FileIdentifier.TryParse(suffixed);
            Assert.IsTrue(id.HasValue);

            suffixed = key + ".profilepic.jpeg";
            id = FileIdentifier.TryParse(suffixed);
            Assert.IsTrue(id.HasValue);
        }

        [Test]
        [Category(CategoryConstants.UnitTest)]
        public void WithData()
        {
            DateTime now = DateTime.Now;
            string date = now.ToString("yyyy_MM_HH_mm");
            string key = "FileDbKeyReports" + date + ".txt";
            FileIdentifier? id = FileIdentifier.TryParse(key);
            Assert.IsTrue(id.HasValue);

            string guid = Guid.NewGuid().ToString();
            int page = 0;
            key = string.Format("{0}_{1}_full", guid, page);
            id = FileIdentifier.TryParse(key);
            Assert.IsTrue(id.HasValue);

            page = 210;
            key = string.Format("{0}_{1}_thumb", guid, page);
            id = FileIdentifier.TryParse(key);
            Assert.IsTrue(id.HasValue);
        }
    }
}
