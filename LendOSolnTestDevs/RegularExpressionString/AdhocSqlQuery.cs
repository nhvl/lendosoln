﻿namespace LendingQB.Test.Developers.RegularExpressionString
{
    using LqbGrammar.DataTypes;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public class AdhocSqlQuery
    {
        [Test]
        public void Delete()
        {
            string sql = "DELETE FROM UserList WHERE UserId = @id";
            var sqlQuery = SQLQueryString.Create(sql);
            Assert.IsTrue(sqlQuery != null);
        }

        [Test]
        public void Select()
        {
            string sql = "SELECT * FROM UserList WHERE UserId = @id";
            var sqlQuery = SQLQueryString.Create(sql);
            Assert.IsTrue(sqlQuery != null);
        }

        [Test]
        public void SelectMultiline()
        {
            string sql = @"SELECT * FROM UserList
WHERE UserId = @id";

            var sqlQuery = SQLQueryString.Create(sql);
            Assert.IsTrue(sqlQuery != null);
        }
    }
}
