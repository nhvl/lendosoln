﻿namespace LendingQB.Test.Developers
{
    /// <summary>
    /// Values to use with the NUnit Category attribute to mark tests.
    /// </summary>
    public static class CategoryConstants
    {

        /// <summary>
        /// If a test method uses mocks registered with LqbApplication.Current, please mark it with this category.
        /// </summary>
        /// <remarks>
        /// This was a logical control to prevent mocks from being registered in LUnit.  Since we are moving all such 
        /// unit tests into this library (from LendOSolnTest) there is no longer any danger of LUnit seeing mocks.
        /// However, it is still a good idea to use this category so we can quickly find the tests.  While proper 
        /// unit tests are going to be written around constructor/property dependency injection and IoC containers, 
        /// we may still keep the GenericLocator around for a while, plus behavioral tests may still rely on 
        /// service locator dependency injection until the old code base is refactored.
        /// </remarks>
        public const string LqbMocks = "LQB.Mocks";

        /// <summary>
        /// If a test has no side-effects and is designed to test a small bit of code, please mark it with this category.
        /// </summary>
        /// <remarks>
        /// Going forward we plan to use a mocking library, probably NSubstitute, along with an IoC container for the 
        /// dependency injection.  These technologies make proper unit tests very easy to write.
        /// 
        /// Unit tests are designed for testing new code and can and should be thrown away when design changes cause
        /// the tests to break.  It is generally a good idea to keep them around while not broken as they can be 
        /// used for regression tests when the implementation details change while the public interface remains fixed 
        /// for a particular class.
        /// </remarks>
        public const string UnitTest = "UNIT";

        /// <summary>
        /// A behavioral test is one for which an operation (user command) is tested with a set of inputs 
        /// and the resulting output is checked for correctness.
        /// </summary>
        /// <remarks>
        /// These tests are typically written against the top level command that 
        /// implements a user action and are designed to test the entire call stack.  The bulk of the regression testing 
        /// would consist of behavioral tests.  The external ports should be mocked out, either by using a service locator 
        /// or registering appropriate mock classes with the IoC container.
        /// </remarks>
        public const string BehavioralTest = "BEHAVIOR";

        /// <summary>
        /// Categorization tests are the first step in a refactoring or regression testing.  They are used to discover a command's correct 
        /// output to various inputs.  Tests are only marked as categorization tests during the exploratory phase, after which 
        /// they are re-marked as behavioral tests.
        /// </summary>
        /// <remarks>
        /// If one wishes to add behavioral tests to existing code, e.g. by wrapping the top-level code in a command object, 
        /// one will need to know the expected behavior as various inputs are applied.  A categorization test is a test where 
        /// the output is originally unknown and the asserts are expected to get triggered.  As the tests are run, the asserts 
        /// will be modified so they pass.  Once all the categorization tests pass, they can be re-marked as behavioral tests.
        /// </remarks>
        public const string CategorizationTest = "CATEGORIZE";

        /// <summary>
        /// If a test is designed to test against an external component or system, please mark it with this category.
        /// </summary>
        /// <remarks>
        /// Integration test call out of the application and so take too much time to be included in a suite of unit 
        /// tests.  These tests are designed to be executed by a developer one at a time while implementing new code 
        /// that integrates with an external port.  Once complete, these tests can be marked with Ignore and resurrected 
        /// only when changes to the port-calling code become necessary (e.g., a data provider makes a change to the 
        /// communication protocol).
        /// </remarks>
        public const string IntegrationTest = "INTEGRATION";

        /// <summary>
        /// Indicates that we don't expect the test to pass when running the suite on our local machines.
        /// </summary>
        public const string ExpectFailureOnLocalhost = "FAILLOCAL";

        /// <summary>
        /// Indicates that a test is checking the code style against established guidelines.
        /// </summary>
        public const string CodeStyleCheck = "CODESTYLE";
    }
}
