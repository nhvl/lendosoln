﻿namespace LendersOffice.Conversions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Linq;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Security;
    using LendingQB.Core.Data;
    using LendingQB.Test.Developers;
    using LendingQB.Test.Developers.Utils;
    using LqbGrammar.DataTypes;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.IntegrationTest)]
    public class LOFormatExporterTest
    {
        public static XElement CreateField(string id, object value = null)
        {
            return new XElement(
                "field",
                new XAttribute("id", id),
                value);
        }

        [Test]
        public void Export_ObsoleteOtherIncomeFieldsAndOtherIncomeCollection_StillReturnsExpectedData()
        {
            using (LoanForIntegrationTest loanRef = LoanForIntegrationTest.Create(TestAccountType.LoTest001, useUladDataLayer: false))
            {
                string income0Desc = OtherIncome.GetDescription(E_aOIDescT.AutomobileExpenseAccount);
                string income1Desc = OtherIncome.GetDescription(E_aOIDescT.NotesReceivableInstallment);
                string income2Desc = OtherIncome.GetDescription(E_aOIDescT.EmploymentRelatedAssets);
                string income3Desc = OtherIncome.GetDescription(E_aOIDescT.DefinedContributionPlan);
                const string income4Desc = "An unrecognized income description - this should end up mapping to type other";
                Assert.That(new[] { income0Desc, income1Desc, income2Desc, income3Desc, }, Has.None.Null.Or.Empty);
                var loanBefore = loanRef.CreateNewPageDataWithBypass();
                loanBefore.InitSave();
                var app = loanBefore.GetAppData(0);
                loanBefore.AddCoborrowerToLegacyApplication(app.aAppId.ToIdentifier<DataObjectKind.LegacyApplication>());
                app.aBBaseI = 1000.84M;
                app.aCBaseI = 7500.79M;
                var otherIncomeList = app.aOtherIncomeList;
                otherIncomeList[0].IsForCoBorrower = false;
                otherIncomeList[0].Desc = income0Desc;
                otherIncomeList[0].Amount = 2345.23M;
                otherIncomeList[1].IsForCoBorrower = true;
                otherIncomeList[1].Desc = income1Desc;
                otherIncomeList[1].Amount = 176.79M;
                otherIncomeList[2].IsForCoBorrower = false;
                otherIncomeList[2].Desc = income2Desc;
                otherIncomeList[2].Amount = 359.45M;
                otherIncomeList.Add(new OtherIncome
                {
                    IsForCoBorrower = true,
                    Desc = income3Desc,
                    Amount = 7314.08M,
                });
                otherIncomeList.Add(new OtherIncome
                {
                    IsForCoBorrower = false,
                    Desc = income4Desc,
                    Amount = 134.34M,
                });
                app.aOtherIncomeList = otherIncomeList;
                loanBefore.Save();
                Assert.AreEqual(5, app.aOtherIncomeList.Count);
                string[] fieldIds = new[]
                {
                    "aO1IForC",
                    "aO1IDesc",
                    "aO1I",
                    "aO2IForC",
                    "aO2IDesc",
                    "aO2I",
                    "aO3IForC",
                    "aO3IDesc",
                    "aO3I",
                };
                var query = new XElement(
                    "LOXmlFormat",
                    new XAttribute("version", "1.0"),
                    new XElement(
                        "loan",
                        new XElement(
                            "applicant",
                            fieldIds.Select(id => CreateField(id)),
                            new XElement(
                                "collection",
                                new XAttribute("id", "aOtherIncomeCollection")))));

                XElement result = Export(loanRef, query);

                XElement applicantElement = result?.Element("loan")?.Element("applicant");
                IEnumerable<XElement> fields = applicantElement?.Elements("field") ?? Enumerable.Empty<XElement>();
                Assert.AreEqual(9, fields.Count());
                Func<string, string> getFieldValue = id => fields.SingleOrDefault(f => f.Attribute("id")?.Value == id)?.Value;
                Assert.AreEqual("False", getFieldValue(fieldIds[0]));
                Assert.AreEqual(income0Desc, getFieldValue(fieldIds[1]));
                Assert.AreEqual("$2,345.23", getFieldValue(fieldIds[2]));
                Assert.AreEqual("True", getFieldValue(fieldIds[3]));
                Assert.AreEqual(income1Desc, getFieldValue(fieldIds[4]));
                Assert.AreEqual("$176.79", getFieldValue(fieldIds[5]));
                Assert.AreEqual("False", getFieldValue(fieldIds[6]));
                Assert.AreEqual(income2Desc, getFieldValue(fieldIds[7]));
                Assert.AreEqual("$359.45", getFieldValue(fieldIds[8]));
                XElement[][] expectedOtherIncomeFields = new[]
                {
                    new[] { CreateField("IsForCoborrower", "False"), CreateField("IncomeDesc", income0Desc), CreateField("MonthlyAmt", "$2,345.23"), },
                    new[] { CreateField("IsForCoborrower", "True"), CreateField("IncomeDesc", income1Desc), CreateField("MonthlyAmt", "$176.79"), },
                    new[] { CreateField("IsForCoborrower", "False"), CreateField("IncomeDesc", income2Desc), CreateField("MonthlyAmt", "$359.45"), },
                    new[] { CreateField("IsForCoborrower", "True"), CreateField("IncomeDesc", income3Desc), CreateField("MonthlyAmt", "$7,314.08"), },
                    new[] { CreateField("IsForCoborrower", "False"), CreateField("IncomeDesc", income4Desc), CreateField("MonthlyAmt", "$134.34"), },
                };
                var otherIncomeElements = applicantElement?.Elements("collection").SingleOrDefault(e => e.Attribute("id")?.Value == "aOtherIncomeCollection")?.Elements("record") ?? Enumerable.Empty<XElement>();
                Assert.AreEqual(expectedOtherIncomeFields.Length, otherIncomeElements.Count());
                for (int i = 0; i < expectedOtherIncomeFields.Length; ++i)
                {
                    Assert.That(otherIncomeElements.ElementAt(i).Elements().Select(e => e.ToString()), Is.EquivalentTo(expectedOtherIncomeFields[i].Select(e => e.ToString())), $"Other Income[{i}]");
                }

                // Part 2: Migrate the loan and repeat
                var loanWithMigration = loanRef.CreateNewPageDataWithBypass();
                loanWithMigration.InitSave();
                loanWithMigration.MigrateToUseLqbCollections();
                loanWithMigration.MigrateToIncomeSourceCollection();
                loanWithMigration.Save();
                Assert.AreEqual(7, loanWithMigration.IncomeSources.Count);

                result = Export(loanRef, query);

                applicantElement = result?.Element("loan")?.Element("applicant");
                fields = applicantElement?.Elements("field") ?? Enumerable.Empty<XElement>();
                Assert.AreEqual(9, fields.Count());
                Assert.AreEqual("False", getFieldValue(fieldIds[0]));
                Assert.AreEqual("Automobile/Expense Account Income", getFieldValue(fieldIds[1]));
                Assert.AreEqual("$2,345.23", getFieldValue(fieldIds[2]));
                Assert.AreEqual("True", getFieldValue(fieldIds[3]));
                Assert.AreEqual("Notes Receivable/Installment", getFieldValue(fieldIds[4]));
                Assert.AreEqual("$176.79", getFieldValue(fieldIds[5]));
                Assert.AreEqual("False", getFieldValue(fieldIds[6]));
                Assert.AreEqual("Employment Related Assets", getFieldValue(fieldIds[7]));
                Assert.AreEqual("$359.45", getFieldValue(fieldIds[8]));
                otherIncomeElements = applicantElement?.Elements("collection").SingleOrDefault(e => e.Attribute("id")?.Value == "aOtherIncomeCollection")?.Elements("record") ?? Enumerable.Empty<XElement>();
                Assert.AreEqual(expectedOtherIncomeFields.Length, otherIncomeElements.Count());
                for (int i = 0; i < expectedOtherIncomeFields.Length; ++i)
                {
                    Assert.That(otherIncomeElements.ElementAt(i).Elements().Select(e => e.ToString()), Is.EquivalentTo(expectedOtherIncomeFields[i].Select(e => e.ToString())), $"Other Income[{i}]");
                }
            }
        }

        private XElement Export(LoanForIntegrationTest loanRef, XElement input)
        {
            return XElement.Parse(LOFormatExporter.Export(loanRef.Id, loanRef.Principal, input.ToString(SaveOptions.DisableFormatting)));
        }
    }
}
