﻿namespace LendersOffice.Conversions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Linq;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Security;
    using LendingQB.Core.Data;
    using LendingQB.Test.Developers;
    using LendingQB.Test.Developers.Utils;
    using LqbGrammar.DataTypes;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.IntegrationTest)]
    public class LOFormatImporterTest
    {
        [Test]
        public void Import_ObsoleteOtherIncomeFieldsOnLegacyIncomeHardCodedFields_StillSetsFirst3Incomes()
        {
            using (LoanForIntegrationTest loanRef = LoanForIntegrationTest.Create(TestAccountType.LoTest001, useUladDataLayer: false))
            {
                loanRef.SetupAppsAndPrimaryBorrowers(2);
                var loanBefore = loanRef.CreateNewPageDataWithBypass();
                loanBefore.InitSave();
                loanBefore.AddCoborrowerToLegacyApplication(loanBefore.GetAppData(0).aAppId.ToIdentifier<DataObjectKind.LegacyApplication>());
                loanBefore.Save();
                Assert.That(loanBefore, Has.Property(nameof(CPageData.sIsIncomeCollectionEnabled)).EqualTo(false));
                var data = new XElement(
                    "LOXmlFormat",
                    new XAttribute("version", "1.0"),
                    new XElement(
                        "loan",
                        new XElement(
                            "applicant",
                            new XAttribute("id", "000-00-0000"),
                            CreateField("aO1IForC", "False"),
                            CreateField("aO1IDesc", "Unemployment/Welfare Income"),
                            CreateField("aO1I", "$65.54"),
                            CreateField("aO2IForC", "True"),
                            CreateField("aO2IDesc", "Royalty Payment"),
                            CreateField("aO2I", "$64.98"),
                            CreateField("aO3IForC", "False"),
                            CreateField("aO3IDesc", "Boarder Income"),
                            CreateField("aO3I", "$67.56")),
                        new XElement(
                            "applicant",
                            new XAttribute("id", "000-00-0002"),
                            CreateField("aO1IForC", "True"),
                            CreateField("aO1IDesc", "Alimony"),
                            CreateField("aO1I", "$89.68"),
                            CreateField("aO2IForC", "False"),
                            CreateField("aO2IDesc", "Royalty Payment"),
                            CreateField("aO2I", "$84.98"),
                            CreateField("aO3IForC", "False"),
                            CreateField("aO3IDesc", "Boarder Income"),
                            CreateField("aO3I", "$69.56"))));

                LoFormatImporterResult result = Import(loanRef, data);

                Assert.AreEqual(LoFormatImporterResultStatus.OkWithWarning, result.ResultStatus);
                Assert.That(result.ErrorMessages, Is.StringContaining("aOtherIncomeCollection")); // Just checking that it has something to point it to the new collection
                var loanAfter = loanRef.CreateNewPageDataWithBypass();
                loanAfter.InitLoad();
                List<OtherIncome> otherIncomes = loanAfter.GetAppData(0).aOtherIncomeList;
                Assert.That(otherIncomes, Has.Count.AtLeast(3).And.None.Null);
                Assert.AreEqual(false, otherIncomes[0].IsForCoBorrower);
                Assert.AreEqual("Unemployment/Welfare Income", otherIncomes[0].Desc);
                Assert.AreEqual(65.54M, otherIncomes[0].Amount);
                Assert.AreEqual(true, otherIncomes[1].IsForCoBorrower);
                Assert.AreEqual("Royalty Payment", otherIncomes[1].Desc);
                Assert.AreEqual(64.98M, otherIncomes[1].Amount);
                Assert.AreEqual(false, otherIncomes[2].IsForCoBorrower);
                Assert.AreEqual("Boarder Income", otherIncomes[2].Desc);
                Assert.AreEqual(67.56M, otherIncomes[2].Amount);
                otherIncomes = loanAfter.GetAppData(1).aOtherIncomeList;
                Assert.That(otherIncomes, Has.Count.AtLeast(3).And.None.Null);
                Assert.AreEqual(true, otherIncomes[0].IsForCoBorrower);
                Assert.AreEqual("Alimony", otherIncomes[0].Desc);
                Assert.AreEqual(89.68M, otherIncomes[0].Amount);
                Assert.AreEqual(false, otherIncomes[1].IsForCoBorrower);
                Assert.AreEqual("Royalty Payment", otherIncomes[1].Desc);
                Assert.AreEqual(84.98M, otherIncomes[1].Amount);
                Assert.AreEqual(false, otherIncomes[2].IsForCoBorrower);
                Assert.AreEqual("Boarder Income", otherIncomes[2].Desc);
                Assert.AreEqual(69.56M, otherIncomes[2].Amount);
            }
        }

        [Test]
        public void Import_ObsoleteOtherIncomeFieldsOnIncomeSourceCollection_ReturnsWarnings()
        {
            using (LoanForIntegrationTest loanRef = LoanForIntegrationTest.Create(TestAccountType.LoTest001, useUladDataLayer: false))
            {
                loanRef.SetupAppsAndPrimaryBorrowers(2);
                var loanBefore = loanRef.CreateNewPageDataWithBypass();
                loanBefore.InitSave();
                loanBefore.MigrateToUseLqbCollections();
                loanBefore.MigrateToIncomeSourceCollection();
                loanBefore.Save();
                Assert.That(loanBefore, Has.Property(nameof(CPageData.sIsIncomeCollectionEnabled)).EqualTo(true));
                var data = new XElement(
                    "LOXmlFormat",
                    new XAttribute("version", "1.0"),
                    new XElement(
                        "loan",
                        new XElement(
                            "applicant",
                            new XAttribute("id", "000-00-0000"),
                            CreateField("aO1IForC", "False"),
                            CreateField("aO1IDesc", "Unemployment/Welfare Income"),
                            CreateField("aO1I", "$65.54"),
                            CreateField("aO2IForC", "True"),
                            CreateField("aO2IDesc", "Royalty Payment"),
                            CreateField("aO2I", "$64.98"),
                            CreateField("aO3IForC", "False"),
                            CreateField("aO3IDesc", "Boarder Income"),
                            CreateField("aO3I", "$67.56")),
                        new XElement(
                            "applicant",
                            new XAttribute("id", "000-00-0002"),
                            CreateField("aO1IForC", "True"),
                            CreateField("aO1IDesc", "Alimony"),
                            CreateField("aO1I", "$89.68"),
                            CreateField("aO2IForC", "False"),
                            CreateField("aO2IDesc", "Royalty Payment"),
                            CreateField("aO2I", "$84.98"),
                            CreateField("aO3IForC", "False"),
                            CreateField("aO3IDesc", "Boarder Income"),
                            CreateField("aO3I", "$69.56"))));

                LoFormatImporterResult result = Import(loanRef, data);

                Assert.AreEqual(LoFormatImporterResultStatus.OkWithWarning, result.ResultStatus);
                Assert.That(result.ErrorMessages, Is.StringContaining("aOtherIncomeCollection")); // Just checking that it has something to point it to the new collection
                var loanAfter = loanRef.CreateNewPageDataWithBypass();
                loanAfter.InitLoad();
                var otherIncomes = loanAfter.Apps.SelectMany(a => a.aOtherIncomeList);
                Assert.That(otherIncomes.Select(o => o.IsForCoBorrower), Is.All.EqualTo(false));
                Assert.That(otherIncomes.Select(o => o.Desc), Is.All.EqualTo(string.Empty));
                Assert.That(otherIncomes.Select(o => o.Amount), Is.All.EqualTo(0M));
            }
        }

        [Test]
        public void Import_OtherIncomeCollectionOnLegacyIncomeHardCodedFields_StillSetsValues()
        {
            using (LoanForIntegrationTest loanRef = LoanForIntegrationTest.Create(TestAccountType.LoTest001, useUladDataLayer: false))
            {
                loanRef.SetupAppsAndPrimaryBorrowers(2);
                var loanBefore = loanRef.CreateNewPageDataWithBypass();
                loanBefore.InitLoad();
                Assert.That(loanBefore, Has.Property(nameof(CPageData.sIsIncomeCollectionEnabled)).EqualTo(false));
                var data = new XElement(
                    "LOXmlFormat",
                    new XAttribute("version", "1.0"),
                    new XElement(
                        "loan",
                        new XElement(
                            "applicant",
                            new XAttribute("id", "000-00-0000"),
                            new XElement(
                                "collection",
                                new XAttribute("id", "aOtherIncomeCollection"),
                                (new[]
                                {
                                    new[] { CreateField("IsForCoborrower", "False"), CreateField("IncomeDesc", "Unemployment/Welfare Income"), CreateField("MonthlyAmt", "$65.54"), },
                                    new[] { CreateField("IsForCoborrower", "True"), CreateField("IncomeDesc", "Royalty Payment"), CreateField("MonthlyAmt", "$64.98"), },
                                    new[] { CreateField("IsForCoborrower", "False"), CreateField("IncomeDesc", "Boarder Income"), CreateField("MonthlyAmt", "$67.56"), },
                                    new[] { CreateField("IsForCoborrower", "True"), CreateField("IncomeDesc", "Boarder Income"), CreateField("MonthlyAmt", "$38.61"), },
                                }).Select(recordFields => new XElement("record", recordFields)))),
                        new XElement(
                            "applicant",
                            new XAttribute("id", "000-00-0002"),
                            new XElement(
                                "collection",
                                new XAttribute("id", "aOtherIncomeCollection"),
                                (new[]
                                {
                                    new[] { CreateField("IsForCoborrower", "True"), CreateField("IncomeDesc", "Alimony"), CreateField("MonthlyAmt", "$365.54"), },
                                    new[] { CreateField("IsForCoborrower", "False"), CreateField("IncomeDesc", "Royalty Payment"), CreateField("MonthlyAmt", "$64.98"), },
                                    new[] { CreateField("IsForCoborrower", "False"), CreateField("IncomeDesc", "Boarder Income"), CreateField("MonthlyAmt", "$68.56"), },
                                    new[] { CreateField("IsForCoborrower", "True"), CreateField("IncomeDesc", "Boarder Income"), CreateField("MonthlyAmt", "$38.61"), },
                                }).Select(recordFields => new XElement("record", recordFields))))));

                LoFormatImporterResult result = Import(loanRef, data);

                Assert.AreEqual(LoFormatImporterResultStatus.Ok, result.ResultStatus);
                var loanAfter = loanRef.CreateNewPageDataWithBypass();
                loanAfter.InitLoad();
                var otherIncomeList1 = loanAfter.GetAppData(0).aOtherIncomeList;
                Assert.That(otherIncomeList1, Has.Count.EqualTo(4));
                Assert.That(otherIncomeList1[0], Has.Property(nameof(OtherIncome.IsForCoBorrower)).EqualTo(false) // using constraints this hard is a little abusive, but I'm trying to save space
                    .And.Property(nameof(OtherIncome.Desc)).EqualTo("Unemployment/Welfare Income").And.Property(nameof(OtherIncome.Amount)).EqualTo(65.54M));
                Assert.That(otherIncomeList1[1], Has.Property(nameof(OtherIncome.IsForCoBorrower)).EqualTo(true)
                    .And.Property(nameof(OtherIncome.Desc)).EqualTo("Royalty Payment").And.Property(nameof(OtherIncome.Amount)).EqualTo(64.98M));
                Assert.That(otherIncomeList1[2], Has.Property(nameof(OtherIncome.IsForCoBorrower)).EqualTo(false)
                    .And.Property(nameof(OtherIncome.Desc)).EqualTo("Boarder Income").And.Property(nameof(OtherIncome.Amount)).EqualTo(67.56M));
                Assert.That(otherIncomeList1[3], Has.Property(nameof(OtherIncome.IsForCoBorrower)).EqualTo(true)
                    .And.Property(nameof(OtherIncome.Desc)).EqualTo("Boarder Income").And.Property(nameof(OtherIncome.Amount)).EqualTo(38.61M));
                var otherIncomeList2 = loanAfter.GetAppData(1).aOtherIncomeList;
                Assert.That(otherIncomeList2, Has.Count.EqualTo(4));
                Assert.That(otherIncomeList2[0], Has.Property(nameof(OtherIncome.IsForCoBorrower)).EqualTo(true) // using constraints this hard is a little abusive, but I'm trying to save space
                    .And.Property(nameof(OtherIncome.Desc)).EqualTo("Alimony").And.Property(nameof(OtherIncome.Amount)).EqualTo(365.54M));
                Assert.That(otherIncomeList2[1], Has.Property(nameof(OtherIncome.IsForCoBorrower)).EqualTo(false)
                    .And.Property(nameof(OtherIncome.Desc)).EqualTo("Royalty Payment").And.Property(nameof(OtherIncome.Amount)).EqualTo(64.98M));
                Assert.That(otherIncomeList2[2], Has.Property(nameof(OtherIncome.IsForCoBorrower)).EqualTo(false)
                    .And.Property(nameof(OtherIncome.Desc)).EqualTo("Boarder Income").And.Property(nameof(OtherIncome.Amount)).EqualTo(68.56M));
                Assert.That(otherIncomeList2[3], Has.Property(nameof(OtherIncome.IsForCoBorrower)).EqualTo(true)
                    .And.Property(nameof(OtherIncome.Desc)).EqualTo("Boarder Income").And.Property(nameof(OtherIncome.Amount)).EqualTo(38.61M));
            }
        }

        [Test]
        public void Import_OtherIncomeCollectionOnIncomeSourceCollection_ClearsLegacyAppOtherIncomeAndSetsIt()
        {
            using (LoanForIntegrationTest loanRef = LoanForIntegrationTest.Create(TestAccountType.LoTest001, useUladDataLayer: false))
            {
                loanRef.SetupAppsAndPrimaryBorrowers(2);
                var loanBefore = loanRef.CreateNewPageDataWithBypass();
                loanBefore.InitSave();
                loanBefore.AddCoborrowerToLegacyApplication(loanBefore.GetAppData(1).aAppId.ToIdentifier<DataObjectKind.LegacyApplication>());
                loanBefore.MigrateToUseLqbCollections();
                loanBefore.MigrateToIncomeSourceCollection();
                var incomeSources = new List<IncomeSource>
                {
                    new IncomeSource { IncomeType = IncomeType.BaseIncome, MonthlyAmountData = 5200.51M },
                    new IncomeSource { IncomeType = IncomeType.Alimony, MonthlyAmountData = 234.12M },
                    new IncomeSource { IncomeType = IncomeType.MilitaryClothesAllowance, MonthlyAmountData = 1145.10M },
                    new IncomeSource { IncomeType = IncomeType.PublicAssistance, MonthlyAmountData = 97.54M },
                };
                incomeSources.ForEach(incomeSource => loanBefore.AddIncomeSource(loanBefore.GetAppData(0).aBConsumerId.ToIdentifier<DataObjectKind.Consumer>(), incomeSource));
                incomeSources.ForEach(incomeSource => loanBefore.AddIncomeSource(loanBefore.GetAppData(1).aBConsumerId.ToIdentifier<DataObjectKind.Consumer>(), new IncomeSource(incomeSource)));
                incomeSources.ForEach(incomeSource => loanBefore.AddIncomeSource(loanBefore.GetAppData(1).aCConsumerId.ToIdentifier<DataObjectKind.Consumer>(), new IncomeSource(incomeSource)));
                loanBefore.Save();
                Assert.That(loanBefore.GetAppData(0), Has.Property(nameof(CAppData.aHasCoborrower)).EqualTo(false));
                Assert.That(loanBefore.GetAppData(1), Has.Property(nameof(CAppData.aHasCoborrower)).EqualTo(true));
                Assert.That(loanBefore, Has.Property(nameof(CPageData.sIsIncomeCollectionEnabled)).EqualTo(true));
                Assert.AreEqual(3, loanBefore.GetAppData(0).aOtherIncomeList.Count);
                Assert.AreEqual(5200.51M, loanBefore.GetAppData(0).aBBaseI);
                Assert.AreEqual(1476.76M, loanBefore.GetAppData(0).aOtherIncomeList.Sum(o => o.Amount));
                Assert.AreEqual(6, loanBefore.GetAppData(1).aOtherIncomeList.Count);
                Assert.AreEqual(2953.52M, loanBefore.GetAppData(1).aOtherIncomeList.Sum(o => o.Amount));
                var data = new XElement(
                    "LOXmlFormat",
                    new XAttribute("version", "1.0"),
                    new XElement(
                        "loan",
                        new XElement(
                            "applicant",
                            new XAttribute("id", "000-00-0000"),
                            new XElement(
                                "collection",
                                new XAttribute("id", "aOtherIncomeCollection"),
                                (new[]
                                {
                                    new[] { CreateField("IsForCoborrower", "False"), CreateField("IncomeDesc", "Unemployment/Welfare Income"), CreateField("MonthlyAmt", "$65.54"), },
                                    new[] { CreateField("IsForCoborrower", "True"), CreateField("IncomeDesc", "Royalty Payment"), CreateField("MonthlyAmt", "$64.98"), },
                                    new[] { CreateField("IsForCoborrower", "False"), CreateField("IncomeDesc", "Boarder Income"), CreateField("MonthlyAmt", "$67.56"), },
                                    new[] { CreateField("IsForCoborrower", "True"), CreateField("IncomeDesc", "Boarder Income"), CreateField("MonthlyAmt", "$38.61"), },
                                }).Select(recordFields => new XElement("record", recordFields)))),
                        new XElement(
                            "applicant",
                            new XAttribute("id", "000-00-0002"),
                            new XElement(
                                "collection",
                                new XAttribute("id", "aOtherIncomeCollection"),
                                (new[]
                                {
                                    new[] { CreateField("IsForCoborrower", "True"), CreateField("IncomeDesc", "Alimony"), CreateField("MonthlyAmt", "$365.54"), },
                                    new[] { CreateField("IsForCoborrower", "False"), CreateField("IncomeDesc", "Royalty Payment"), CreateField("MonthlyAmt", "$64.98"), },
                                    new[] { CreateField("IsForCoborrower", "False"), CreateField("IncomeDesc", "Boarder Income"), CreateField("MonthlyAmt", "$68.56"), },
                                    new[] { CreateField("IsForCoborrower", "True"), CreateField("IncomeDesc", "Boarder Income"), CreateField("MonthlyAmt", "$38.61"), },
                                }).Select(recordFields => new XElement("record", recordFields))))));

                LoFormatImporterResult result = Import(loanRef, data);

                var loanAfter = loanRef.CreateNewPageDataWithBypass();
                loanAfter.InitLoad();

                // 2018-12 tj - There's a bug in LOFormatImporter that causes aHasCoborrower to be set to true.  While Case 476826
                // should address this, this assume statement is intended to ignore this test while this bug is live.
                Assume.That(loanAfter.GetAppData(0), Has.Property(nameof(CAppData.aHasCoborrower)).EqualTo(false));
                Assert.AreEqual(LoFormatImporterResultStatus.OkWithWarning, result.ResultStatus);
                Assert.That(result.ErrorMessages, Is.StringContaining("aOtherIncomeCollection")); // Just checking that it has something to point it to the new collection
                Assert.AreEqual(5200.51M, loanAfter.GetAppData(0).aBBaseI);
                Assert.AreEqual(3, loanAfter.GetAppData(0).aOtherIncomeList.Count);
                Assert.AreEqual(1476.76M, loanAfter.GetAppData(0).aOtherIncomeList.Sum(o => o.Amount)); // No import occurred, since there wasn't a co-borrower, but we provided incomes for a co-borrower
                Assert.AreEqual(5200.51M, loanAfter.GetAppData(1).aBBaseI);
                Assert.AreEqual(4, loanAfter.GetAppData(1).aOtherIncomeList.Count);
                Assert.AreEqual(537.69M, loanAfter.GetAppData(1).aOtherIncomeList.Sum(o => o.Amount));
            }
        }

        [Test]
        public void Import_ConsumerIncomeFieldsOnLegacyIncomeFields_StillSetsValues()
        {
            using (LoanForIntegrationTest loanRef = LoanForIntegrationTest.Create(TestAccountType.LoTest001, useUladDataLayer: false))
            {
                loanRef.SetupAppsAndPrimaryBorrowers(1);
                var loanBefore = loanRef.CreateNewPageDataWithBypass();
                loanBefore.InitLoad();
                Assert.That(loanBefore, Has.Property(nameof(CPageData.sIsIncomeCollectionEnabled)).EqualTo(false));
                var data = new XElement(
                    "LOXmlFormat",
                    new XAttribute("version", "1.0"),
                    new XElement(
                        "loan",
                        new XElement(
                            "applicant",
                            new XAttribute("id", "000-00-0000"),
                            CreateField("aBBaseI", "1"),
                            CreateField("aBOvertimeI", "2"),
                            CreateField("aBBonusesI", "3"),
                            CreateField("aBCommisionI", "4"),
                            CreateField("aBDividendI", "5"),
                            CreateField("aCBaseI", "6"),
                            CreateField("aCOvertimeI", "7"),
                            CreateField("aCBonusesI", "8"),
                            CreateField("aCCommisionI", "9"),
                            CreateField("aCDividendI", "10"))));

                LoFormatImporterResult result = Import(loanRef, data);

                Assert.AreEqual(LoFormatImporterResultStatus.Ok, result.ResultStatus);
                var loanAfter = loanRef.CreateNewPageDataWithBypass();
                loanAfter.InitLoad();
                var appAfter = loanAfter.GetAppData(0);
                Assert.That(appAfter.aBBaseI, Is.EqualTo(1));
                Assert.That(appAfter.aBOvertimeI, Is.EqualTo(2));
                Assert.That(appAfter.aBBonusesI, Is.EqualTo(3));
                Assert.That(appAfter.aBCommisionI, Is.EqualTo(4));
                Assert.That(appAfter.aBDividendI, Is.EqualTo(5));
                Assert.That(appAfter.aCBaseI, Is.EqualTo(6));
                Assert.That(appAfter.aCOvertimeI, Is.EqualTo(7));
                Assert.That(appAfter.aCBonusesI, Is.EqualTo(8));
                Assert.That(appAfter.aCCommisionI, Is.EqualTo(9));
                Assert.That(appAfter.aCDividendI, Is.EqualTo(10));
            }
        }

        [Test]
        public void Import_ConsumerIncomeFieldsOnIncomeSourceCollectionWithNoExistingRecords_StillSetsValues()
        {
            using (LoanForIntegrationTest loanRef = LoanForIntegrationTest.Create(TestAccountType.LoTest001, useUladDataLayer: false))
            {
                loanRef.SetupAppsAndPrimaryBorrowers(1);
                var loanBefore = loanRef.CreateNewPageDataWithBypass();
                loanBefore.InitSave();
                loanBefore.AddCoborrowerToLegacyApplication(loanBefore.GetAppData(0).aAppId.ToIdentifier<DataObjectKind.LegacyApplication>());
                loanBefore.MigrateToUseLqbCollections();
                loanBefore.MigrateToIncomeSourceCollection();
                loanBefore.Save();
                Assert.That(loanBefore.sIsIncomeCollectionEnabled, Is.EqualTo(true));
                Assert.That(loanBefore.IncomeSources.Count, Is.EqualTo(0));
                var data = new XElement(
                    "LOXmlFormat",
                    new XAttribute("version", "1.0"),
                    new XElement(
                        "loan",
                        new XElement(
                            "applicant",
                            new XAttribute("id", "000-00-0000"),
                            CreateField("aBBaseI", "1"),
                            CreateField("abovertimei", "2"),
                            CreateField("ABBONUSESI", "3"),
                            CreateField("abcomMISIONI", "4"),
                            CreateField("ABDIVidendi", "5"),
                            CreateField("aCBaseI", "6"),
                            CreateField("acovertimei", "7"),
                            CreateField("ACBONUSESI", "8"),
                            CreateField("ACCOMMisioni", "9"),
                            CreateField("acdiviDENDI", "10"))));

                LoFormatImporterResult result = Import(loanRef, data);

                Assert.AreEqual(LoFormatImporterResultStatus.Ok, result.ResultStatus);
                var loanAfter = loanRef.CreateNewPageDataWithBypass();
                loanAfter.InitLoad();
                var appAfter = loanAfter.GetAppData(0);
                Assert.That(appAfter.aBBaseI, Is.EqualTo(1));
                Assert.That(appAfter.aBOvertimeI, Is.EqualTo(2));
                Assert.That(appAfter.aBBonusesI, Is.EqualTo(3));
                Assert.That(appAfter.aBCommisionI, Is.EqualTo(4));
                Assert.That(appAfter.aBDividendI, Is.EqualTo(5));
                Assert.That(appAfter.aCBaseI, Is.EqualTo(6));
                Assert.That(appAfter.aCOvertimeI, Is.EqualTo(7));
                Assert.That(appAfter.aCBonusesI, Is.EqualTo(8));
                Assert.That(appAfter.aCCommisionI, Is.EqualTo(9));
                Assert.That(appAfter.aCDividendI, Is.EqualTo(10));
            }
        }

        [Test]
        public void Import_ConsumerIncomeFieldsOnIncomeSourceCollectionWithExistingRecords_DoesNotSetFieldsAndReturnsWarnings()
        {
            using (LoanForIntegrationTest loanRef = LoanForIntegrationTest.Create(TestAccountType.LoTest001, useUladDataLayer: false))
            {
                loanRef.SetupAppsAndPrimaryBorrowers(1);
                var loanBefore = loanRef.CreateNewPageDataWithBypass();
                loanBefore.InitSave();
                var appBefore = loanBefore.GetAppData(0);
                loanBefore.AddCoborrowerToLegacyApplication(appBefore.aAppId.ToIdentifier<DataObjectKind.LegacyApplication>());
                appBefore.aBBaseI = 1;
                appBefore.aBOvertimeI = 2;
                appBefore.aBBonusesI = 3;
                appBefore.aBCommisionI = 4;
                appBefore.aBDividendI = 5;
                appBefore.aCBaseI = 6;
                appBefore.aCOvertimeI = 7;
                appBefore.aCBonusesI = 8;
                appBefore.aCCommisionI = 9;
                appBefore.aCDividendI = 10;
                loanBefore.MigrateToUseLqbCollections();
                loanBefore.MigrateToIncomeSourceCollection();
                loanBefore.Save();
                Assert.That(loanBefore.sIsIncomeCollectionEnabled, Is.EqualTo(true));
                Assert.That(loanBefore.IncomeSources.Count, Is.EqualTo(10));
                var data = new XElement(
                    "LOXmlFormat",
                    new XAttribute("version", "1.0"),
                    new XElement(
                        "loan",
                        new XElement(
                            "applicant",
                            new XAttribute("id", "000-00-0000"),
                            CreateField("aBBaseI", "10"),
                            CreateField("aBOvertimeI", "11"),
                            CreateField("aBBonusesI", "12"),
                            CreateField("aBCommisionI", "13"),
                            CreateField("aBDividendI", "14"),
                            CreateField("aCBaseI", "15"),
                            CreateField("aCOvertimeI", "16"),
                            CreateField("aCBonusesI", "17"),
                            CreateField("aCCommisionI", "18"),
                            CreateField("aCDividendI", "19"))));

                LoFormatImporterResult result = Import(loanRef, data);

                Assert.AreEqual(LoFormatImporterResultStatus.OkWithWarning, result.ResultStatus);
                StringAssert.Contains("aBBaseI", result.ErrorMessages);
                StringAssert.Contains("aBOvertimeI", result.ErrorMessages);
                StringAssert.Contains("aBBonusesI", result.ErrorMessages);
                StringAssert.Contains("aBCommisionI", result.ErrorMessages);
                StringAssert.Contains("aBDividendI", result.ErrorMessages);
                StringAssert.Contains("aCBaseI", result.ErrorMessages);
                StringAssert.Contains("aCOvertimeI", result.ErrorMessages);
                StringAssert.Contains("aCBonusesI", result.ErrorMessages);
                StringAssert.Contains("aCCommisionI", result.ErrorMessages);
                StringAssert.Contains("aCDividendI", result.ErrorMessages);
                var loanAfter = loanRef.CreateNewPageDataWithBypass();
                loanAfter.InitLoad();
                var appAfter = loanAfter.GetAppData(0);
                Assert.That(appAfter.aBBaseI, Is.EqualTo(1));
                Assert.That(appAfter.aBOvertimeI, Is.EqualTo(2));
                Assert.That(appAfter.aBBonusesI, Is.EqualTo(3));
                Assert.That(appAfter.aBCommisionI, Is.EqualTo(4));
                Assert.That(appAfter.aBDividendI, Is.EqualTo(5));
                Assert.That(appAfter.aCBaseI, Is.EqualTo(6));
                Assert.That(appAfter.aCOvertimeI, Is.EqualTo(7));
                Assert.That(appAfter.aCBonusesI, Is.EqualTo(8));
                Assert.That(appAfter.aCCommisionI, Is.EqualTo(9));
                Assert.That(appAfter.aCDividendI, Is.EqualTo(10));
            }
        }

        private static LoFormatImporterResult Import(LoanForIntegrationTest loanRef, XElement data)
        {
            return LOFormatImporter.Import(loanRef.Id, loanRef.Principal, data.ToString(SaveOptions.DisableFormatting));
        }

        private static XElement CreateField(string id, object value) => LOFormatExporterTest.CreateField(id, value);
    }
}