﻿namespace LendingQB.Test.Developers.Utils
{
    using System;
    using System.Collections;

    internal static class RangeLimits
    {
        public static class CreditPublicRecordType
        {
            public const int Min = 0;
            public const int Max = 43;

            public static IEnumerable Enumerable()
            {
                return Enum.GetValues(typeof(LendersOffice.CreditReport.E_CreditPublicRecordType));
            }
        }

        public static class DispositionType
        {
            public const int Min = 0;
            public const int Max = 33;

            public static IEnumerable Enumerable()
            {
                return Enum.GetValues(typeof(LendersOffice.CreditReport.E_CreditPublicRecordDispositionType));
            }
        }

        public static class FormatTarget
        {
            public const int Min = 0;
            public const int Max = 14;

            public static IEnumerable Enumerable()
            {
                return Enum.GetValues(typeof(global::DataAccess.FormatTarget));
            }
        }

        public static class ReoStatus
        {
            public const int Min = 0;
            public const int Max = 3;

            public static IEnumerable Enumerable()
            {
                return Enum.GetValues(typeof(global::DataAccess.E_ReoStatusT));
            }
        }

        public static class ReoType
        {
            public const int Min = 0;
            public const int Max = 13;

            public static IEnumerable Enumerable()
            {
                return Enum.GetValues(typeof(global::DataAccess.E_ReoTypeT));
            }
        }

        public static class VorType
        {
            public const int Min = 0;
            public const int Max = 3;

            public static IEnumerable Enumberable()
            {
                return Enum.GetValues(typeof(global::DataAccess.E_VorT));
            }
        }
    }
}
