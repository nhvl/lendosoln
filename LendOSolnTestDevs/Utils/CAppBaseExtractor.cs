﻿namespace LendingQB.Test.Developers.Utils
{
    using System;
    using System.Reflection;
    using global::DataAccess;

    /// <summary>
    /// Tool for extraction of the CAppBase instance held by a CAppData instance.
    /// </summary>
    public static class CAppBaseExtractor
    {
        /// <summary>
        /// Extract the CAppBase instance held by a CAppData instance.
        /// </summary>
        /// <param name="appData">The CAppData instance.</param>
        /// <returns>The contained CAppBase instance.</returns>
        public static CAppBase Extract(CAppData appData)
        {
            var fieldInfo = appData.GetType().GetField(CAppData.GetAppBaseMemberDataName(), BindingFlags.NonPublic | BindingFlags.Instance);
            return fieldInfo.GetValue(appData) as CAppBase;
        }
    }
}
