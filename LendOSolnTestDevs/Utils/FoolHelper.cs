﻿namespace LendingQB.Test.Developers.Utils
{
    using System;
    using global::Adapter;
    using global::Adapter.Drivers;
    using global::Adapter.Drivers.ConnectionStringProvider;
    using global::Adapter.Emailer;
    using global::Adapter.MethodInvoke;
    using LendersOffice.Drivers.Base64Encoding;
    using LendersOffice.Drivers.Configuration;
    using LendersOffice.Drivers.ConnectionStringDecryption;
    using LendersOffice.Drivers.ConversationLog;
    using LendersOffice.Drivers.DataEncryption;
    using LendersOffice.Drivers.Emailer;
    using LendersOffice.Drivers.Encryption;
    using LendersOffice.Drivers.Environment;
    using LendersOffice.Drivers.FileDB;
    using LendersOffice.Drivers.FileSystem;
    using LendersOffice.Drivers.HttpRequest;
    using LendersOffice.Drivers.Json;
    using LendersOffice.Drivers.Logger;
    using LendersOffice.Drivers.MessageQueue;
    using LendersOffice.Drivers.MethodInvoke;
    using LendersOffice.Drivers.NetFramework;
    using LendersOffice.Drivers.Security;
    using LendersOffice.Drivers.Sms;
    using LendersOffice.Drivers.SqlServerDB;
    using LendersOffice.Queries.Configuration;
    using LqbGrammar;
    using LqbGrammar.Adapters;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;
    using LqbGrammar.Drivers.Emailer;
    using LqbGrammar.Drivers.FileSystem;
    using LqbGrammar.Drivers.ConversationLog;
    using LqbGrammar.Drivers.HttpRequest;
    using LqbGrammar.Exceptions;
    using LqbGrammar.Queries;

    public sealed class FoolHelper : IDisposable
    {
        private const string DisposedExceptionName = "FoolHelper";

        static FoolHelper()
        {
            var detector = new LUnitDetector();
            bool ok = LqbApplication.Current.RegisterFactory<ILUnitDetector>(detector);
            if (ok)
            {
                LqbApplication.Current.RestoreFactories();
            }

            LUnitDetected = !ok;
        }

        ~FoolHelper()
        {
            this.Dispose(false);
        }

        private static bool LUnitDetected { get; set; }

        private bool IsDead { get; set; }

        private bool IsDisposed { get; set; }

        private bool CoreServicesRegistered { get; set; }

        public enum DriverType
        {
            ApplicationSettings,
            Base64Encoding,
            Bootstrap,
            ConfigurationQuery,
            EmailReceiver,
            EmailSender,
            Encryption,
            Environment,
            FileDB,
            FileSystem,
            HttpRequest,
            Json,
            Logger,
            MessageQueue,
            MethodInvoke,
            RegularExpression,
            Sms,
            SqlQuery,
            StoredProcedure
        }

        public bool RegisterRealDrivers(params DriverType[] types)
        {
            if (this.IsDead)
            {
                throw new ObjectDisposedException(DisposedExceptionName);
            }

            if (LUnitDetected)
            {
                return false;
            }

            foreach (var type in types)
            {
                this.RegisterRealDriver(type);
            }

            return true;
        }

        public bool RegisterRealDriver(DriverType type)
        {
            if (this.IsDead)
            {
                throw new ObjectDisposedException(DisposedExceptionName);
            }

            if (LUnitDetected)
            {
                return false;
            }

            var application = LqbApplication.Current;
            if (this.DriverRequiresCoreServices(type))
            {
                this.RegisterCoreServices();
            }

            this.RegisterDriverFactory(type, application);
            return true;
        }

        public bool RegisterMockDriverFactory<T>(T factory) where T : class
        {
            if (this.IsDead)
            {
                throw new ObjectDisposedException(DisposedExceptionName);
            }

            if (!LUnitDetected)
            {
                LqbApplication.Current.RegisterFactory(factory);
            }

            return true;
        }

        public void Dispose()
        {
            this.IsDead = true;
            if (!this.IsDisposed)
            {
                this.Dispose(true);
                this.IsDisposed = true;
                GC.SuppressFinalize(this);
            }
        }

        private void RegisterCoreServices()
        {
            if (!this.CoreServicesRegistered)
            {
                var application = LqbApplication.Current;
                this.RegisterDriverFactory(DriverType.ApplicationSettings, application);
                this.RegisterDriverFactory(DriverType.Base64Encoding, application);
                this.RegisterDriverFactory(DriverType.ConfigurationQuery, application);
                this.RegisterDriverFactory(DriverType.Encryption, application);
                this.RegisterDriverFactory(DriverType.FileSystem, application);
                this.RegisterDriverFactory(DriverType.StoredProcedure, application);

                application.RegisterFactory<LendersOffice.Logging.ILoggerFactory>(new LendersOffice.Logging.LoggerManager.MSMQFactory());
                this.CoreServicesRegistered = true;
            }
        }

        private bool DriverRequiresCoreServices(DriverType driverType)
        {
            switch (driverType)
            {
                case DriverType.ApplicationSettings:
                case DriverType.Base64Encoding:
                case DriverType.ConfigurationQuery:
                case DriverType.Encryption:
                case DriverType.FileSystem:
                case DriverType.StoredProcedure:
                case DriverType.EmailReceiver:
                case DriverType.EmailSender:
                case DriverType.FileDB:
                case DriverType.HttpRequest:
                case DriverType.Json:
                case DriverType.Logger:
                case DriverType.MessageQueue:
                case DriverType.Sms:
                case DriverType.SqlQuery:
                    return true;
                case DriverType.Bootstrap:
                case DriverType.Environment:
                case DriverType.MethodInvoke:
                case DriverType.RegularExpression:
                    return false;
                default:
                    throw new DeveloperException(ErrorMessage.SystemError);
            }
        }

        private void RegisterDriverFactory(DriverType driverType, LqbApplication application)
        {
            switch (driverType)
            {
                case DriverType.ApplicationSettings:
                    application.RegisterFactory<IApplicationSettingsAdapterFactory>(new ApplicationSettingsAdapterFactory());
                    application.RegisterFactory<IApplicationSettingsDriverFactory>(new ApplicationSettingsDriverFactory());
                    break;
                case DriverType.Base64Encoding:
                    application.RegisterFactory<IBase64EncodingAdapterFactory>(new Base64EncodingAdapterFactory());
                    application.RegisterFactory<IBase64EncodingDriverFactory>(new Base64EncodingDriverFactory());
                    break;
                case DriverType.ConfigurationQuery:
                    application.RegisterFactory<IConfigurationQueryFactory>(new ConfigurationQueryFactory());
                    break;
                case DriverType.Encryption:
                    application.RegisterFactory<IConnectionStringDecryptionDriverFactory>(new ConnectionStringDecryptionDriverFactory());
                    application.RegisterFactory<IDataEncryptionAdapterFactory>(new DataEncryptionAdapterFactory());
                    application.RegisterFactory<IDataEncryptionDriverFactory>(new DataEncryptionDriverFactory());
                    application.RegisterFactory<IEncryptionDriverFactory>(new EncryptionDriverFactory());
                    application.RegisterFactory<IEncryptionKeyDriverFactory>(new CachingKeyDriverDecoratorFactory());
                    break;
                case DriverType.FileSystem:
                    application.RegisterFactory<IFileSystemAdapterFactory>(new FileSystemAdapterFactory());
                    application.RegisterFactory<IFileSystemDriverFactory>(new FileSystemDriverFactory());
                    break;
                case DriverType.StoredProcedure:
                    application.RegisterFactory<IStoredProcedureAdapterFactory>(new StoredProcedureAdapterFactory());
                    application.RegisterFactory<IStoredProcedureDriverFactory>(new StoredProcedureDriverFactory());
                    break;
                case DriverType.Bootstrap:
                    application.RegisterFactory<IConnectionStringProviderDriverFactory>(new ConnectionStringProviderDriverFactory());
                    application.RegisterFactory<IEncryptionKeyDriverFactory>(new DbConnectionOnlyEncryptionKeyDriver.Factory());
                    break;
                case DriverType.EmailReceiver:
                    this.RegisterDriverFactory(DriverType.RegularExpression, application);
                    application.RegisterFactory<IReceiveEmailAdapterFactory>(new OpenPop3AdapterFactory());
                    application.RegisterFactory<IReceiveEmailDriverFactory>(new EmailReceiverDriverFactory());
                    break;
                case DriverType.EmailSender:
                    application.RegisterFactory<IEmailAdapterFactory>(new EmailAdapterFactory());
                    application.RegisterFactory<IEmailDriverFactory>(new EmailDriverFactory());
                    break;
                case DriverType.Environment:
                    application.RegisterFactory<IEnvironmentAdapterFactory>(new EnvironmentAdapterFactory());
                    application.RegisterFactory<IEnvironmentDriverFactory>(new EnvironmentDriverFactory());
                    break;
                case DriverType.FileDB:
                    application.RegisterFactory<IFileDbAdapterFactory>(new FileDbAdapterFactory());
                    application.RegisterFactory<IFileDbDriverFactory>(new FileDbDriverFactory());
                    break;
                case DriverType.HttpRequest:
                    application.RegisterFactory<IHttpRequestAdapterFactory>(new HttpRequestAdapterFactory());
                    application.RegisterFactory<IHttpRequestDriverFactory>(new HttpRequestDriverFactory());
                    break;
                case DriverType.Json:
                    application.RegisterFactory<IJsonAdapterFactory>(new JsonAdapterFactory());
                    application.RegisterFactory<IJsonDriverFactory>(new JsonDriverFactory());
                    break;
                case DriverType.Logger:
                    application.RegisterFactory<ILoggingDriverFactory>(new CompoundLoggingDriverFactory());
                    break;
                case DriverType.MessageQueue:
                    application.RegisterFactory<IMessageQueueAdapterFactory>(new MessageQueueAdapterFactory());
                    application.RegisterFactory<IMessageQueueDriverFactory>(new MessageQueueDriverFactory());
                    break;
                case DriverType.MethodInvoke:
                    application.RegisterFactory<IMethodInvokeAdapterFactory>(new DirectMethodCallAdapterFactory());
                    application.RegisterFactory<IMethodInvokeDriverFactory>(new MethodInvokeDriverFactory());
                    break;
                case DriverType.RegularExpression:
                    application.RegisterFactory<IRegularExpressionAdapterFactory>(new RegularExpressionAdapterFactory());
                    application.RegisterFactory<IRegularExpressionDriverFactory>(new RegularExpressionDriverFactory());
                    break;
                case DriverType.Sms:
                    application.RegisterFactory<ISmsAdapterFactory>(new BananaSmsAdapterFactory());
                    application.RegisterFactory<ISmsDriverFactory>(new LoggingBananaSmsDriverFactory());
                    break;
                case DriverType.SqlQuery:
                    application.RegisterFactory<ISqlAdapterFactory>(new SqlServerAdapterFactory());
                    application.RegisterFactory<ISqlDriverFactory>(new SqlServerDriverFactory());
                    break;
                default:
                    throw new DeveloperException(ErrorMessage.SystemError);
            }
        }

        private void Dispose(bool disposing)
        {
            if (disposing)
            {
                this.RestoreFactories();
            }
            else
            {
                try
                {
                    this.RestoreFactories();
                }
                catch
                {
                    // don't allow exceptions on the finalizer thread
                }
            }
        }

        private void RestoreFactories()
        {
            LqbApplication.Current.RestoreFactories();
        }

        private interface ILUnitDetector
        {
        }

        private class LUnitDetector : ILUnitDetector
        {
        }
    }
}
