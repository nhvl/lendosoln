﻿namespace LendingQB.Test.Developers.Utils
{
    using System;
    using System.Linq;
    using global::DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Helper class to create a new new applications in (legacy, ulad) pairs.
    /// </summary>
    public static class LoanTestUtilities
    {
        /// <summary>
        /// Add the pair (LegacyApplication, UladApplication) to a loan.
        /// </summary>
        /// <param name="loanId">The identifier for the loan.</param>
        /// <param name="hasCoborrower">If true, the new LegacyApplication will be marked so that the coborrower is considered active.</param>
        /// <returns>The aAppId for the new LegacyApplication.</returns>
        public static Guid AddNewLegacyAndUladApplicationToLoan(Guid loanId, bool hasCoborrower)
        {
            var loanData = CPageData.CreateUsingSmartDependencyWithSecurityBypass(loanId, typeof(LoanTestUtilities));
            int pos = loanData.AddNewApp();
            loanData.InitSave(ConstAppDavid.SkipVersionCheck);

            var appData = loanData.GetAppData(pos);
            if (hasCoborrower)
            {
                loanData.AddCoborrowerToLegacyApplication(appData.aAppId.ToIdentifier<DataObjectKind.LegacyApplication>());
            }

            loanData.Save();
            return appData.aAppId;
        }

        /// <summary>
        /// Add a new legacy application and associate the borrower(s) with the indicated ULAD application.
        /// </summary>
        /// <remarks>
        /// The interface for adding a consumer is odd in that the state must be in InitLoad and InitLoad must be called again to pick up the changes,
        /// because the database is modified immediately so Save shouldn't be called. Another oddity is that if the ulad application has only a single
        /// consumer and the associated legacy application only has the borrower, then adding a consumer to the ULAD application will add it as the
        /// coborrower of the legacy application.  To prevent that we need to add a temporary coborrower and remove it later.
        /// </remarks>
        /// <param name="loanId">The identifier for the loan.</param>
        /// <param name="uladAppId">The identifier for the ULAD application.</param>
        /// <param name="hasCoborrower">If true, the new LegacyApplication will be marked so that the coborrower is considered active.</param>
        /// <returns>The aAppId for the new LegacyApplication.</returns>
        public static Guid AddNewLegacyApplicationToExistingUladApplication(Guid loanId, DataObjectIdentifier<DataObjectKind.UladApplication, Guid> uladAppId, bool hasCoborrower)
        {
            var tempConsumerId = AddCoborrowerIfMissing(loanId, uladAppId);

            var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(loanId, typeof(LoanTestUtilities));
            loan.InitLoad();
            var loanData = ExtractLoanData(loan);
            var aggregate = ExtractAggregate(loanData);

            var borrowerIdentifier = aggregate.AddConsumerToUladApplication(uladAppId);
            if (hasCoborrower)
            {
                loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(loanId, typeof(LoanTestUtilities));
                loan.InitSave();
                var legacyAppId = loan.UladApplicationConsumers.Values.Single(a => a.ConsumerId == borrowerIdentifier).LegacyAppId;

                loanData = ExtractLoanData(loan);
                aggregate = ExtractAggregate(loanData);

                aggregate.AddCoborrowerToLegacyApplication(legacyAppId);
                loan.Save();
            }

            RemoveCoborrowerIfExists(loanId, tempConsumerId);

            loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(loanId, typeof(LoanTestUtilities));
            loan.InitLoad();

            return loan.UladApplicationConsumers.Values.Single(a => a.ConsumerId == borrowerIdentifier).LegacyAppId.Value;
        }

        private static DataObjectIdentifier<DataObjectKind.Consumer, Guid>? AddCoborrowerIfMissing(Guid loanId, DataObjectIdentifier<DataObjectKind.UladApplication, Guid> uladAppId)
        {
            // If the ULAD application has only one consumer and the associated legacy app has only one borrower, then we need to add the coborrower temporarily and remove it later :(
            var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(loanId, typeof(LoanTestUtilities));
            loan.InitSave();
            int countConsumers = loan.UladApplicationConsumers.Values.Count(a => a.UladApplicationId == uladAppId);
            if (countConsumers == 1)
            {
                var legacyAppId = loan.UladApplicationConsumers.Values.Single(a => a.UladApplicationId == uladAppId).LegacyAppId;
                int countLegacyBorrowers = loan.LegacyApplicationConsumers.Values.Count(a => a.LegacyApplicationId == legacyAppId);
                if (countLegacyBorrowers == 1)
                {
                    var loanData = ExtractLoanData(loan);
                    var aggregate = ExtractAggregate(loanData);
                    var coborrowerId = aggregate.AddCoborrowerToLegacyApplication(legacyAppId);
                    loan.Save();
                    return coborrowerId;
                }
            }

            return null;
        }

        private static void RemoveCoborrowerIfExists(Guid loanId, DataObjectIdentifier<DataObjectKind.Consumer, Guid>? coborrowerId)
        {
            if (coborrowerId != null)
            {
                var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(loanId, typeof(LoanTestUtilities));
                loan.InitSave();

                var legacyAppId = loan.UladApplicationConsumers.Values.Single(a => a.ConsumerId == coborrowerId).LegacyAppId;
                loan.RemoveCoborrowerFromLegacyApplication(legacyAppId);
                loan.Save();
            }
        }

        private static CPageBase ExtractLoanData(CPageData pageData)
        {
            return CPageBaseExtractor.Extract(pageData);
        }

        private static ILoanLqbCollectionContainer ExtractAggregate(CPageBase pageBase)
        {
            return pageBase.loanLqbCollectionContainer;
        }
    }
}