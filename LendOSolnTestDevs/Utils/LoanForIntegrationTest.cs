﻿namespace LendingQB.Test.Developers.Utils
{
    using System;
    using global::DataAccess;
    using LqbGrammar.DataTypes;

    public enum TestFileType
    {
        RegularLoan,
        TestFile,
        UladLoan
    }

    /// <summary>
    /// Creates a temporary disposable loan in the database.  When this instance is disposed, the loan will be deleted and the adapters removed.
    /// </summary>
    public class LoanForIntegrationTest : IDisposable
    {
        private LoanForIntegrationTest(LendersOffice.Security.AbstractUserPrincipal principal, Guid loanId)
        {
            this.Principal = principal;
            this.Id = loanId;
            this.Identifier = DataObjectIdentifier<DataObjectKind.Loan, Guid>.Create(loanId);
        }

        public LendersOffice.Security.AbstractUserPrincipal Principal { get; }

        public Guid Id { get; }

        public DataObjectIdentifier<DataObjectKind.Loan, Guid> Identifier { get; }

        public FoolHelper Helper { get; private set; }

        public static LoanForIntegrationTest Create(TestAccountType accountType, bool useUladDataLayer)
        {
            var type = useUladDataLayer ? TestFileType.UladLoan : TestFileType.RegularLoan;
            return Create(type, accountType);
        }

        public static LoanForIntegrationTest Create(TestFileType fileType, TestAccountType? accountType = null)
        {
            FoolHelper helper = null;
            try
            {
                helper = new FoolHelper();
                helper.RegisterRealDrivers(FoolHelper.DriverType.StoredProcedure, FoolHelper.DriverType.SqlQuery, FoolHelper.DriverType.MessageQueue);

                var principal = LoginTools.LoginAs(accountType ?? TestAccountType.LoTest001);
                var loanCreator = CLoanFileCreator.GetCreator(principal, LendersOffice.Audit.E_LoanCreationSource.UserCreateFromBlank);

                Guid loanId;
                switch (fileType)
                {
                    case TestFileType.RegularLoan:
                        loanId = loanCreator.CreateBlankLoanFile();
                        break;
                    case TestFileType.UladLoan:
                        loanId = loanCreator.CreateBlankUladLoanFile();
                        break;
                    case TestFileType.TestFile:
                        loanId = loanCreator.CreateNewTestFile();
                        break;
                    default:
                        throw new UnhandledEnumException(fileType);
                }

                return new LoanForIntegrationTest(principal, loanId)
                {
                    Helper = helper,
                };
            }
            catch
            {
                helper?.Dispose();
                throw;
            }
        }

        public void Dispose()
        {
            try
            {
                if (this.Principal != null && this.Id != Guid.Empty)
                {
                    Tools.SystemDeclareLoanFileInvalid(this.Principal, this.Id, false, false);
                }
            }
            catch (Exception)
            {
            }
            finally
            {
                this.Helper?.Dispose();
            }
        }

        public CPageData CreateNewPageData() => CPageData.CreateUsingSmartDependency(this.Id, this.GetCallingType());

        public CFullAccessPageData CreateNewPageDataWithBypass() => CPageData.CreateUsingSmartDependencyWithSecurityBypass(this.Id, this.GetCallingType());

        /// <summary>
        /// Ensures that the apps on the loan matches <paramref name="count"/>, and sets the SSNs to be <c>000-00-000N</c>, where N is the legacy app index multiplied by 2.
        /// </summary>
        /// <param name="count">The number of apps desired on the loan.</param>
        public void SetupAppsAndPrimaryBorrowers(int count)
        {
            var loan = this.CreateNewPageDataWithBypass();
            loan.InitSave();
            int appsToCreate = count - loan.nApps;
            if (appsToCreate != 0)
            {
                for (int i = 0; i < appsToCreate; ++i)
                {
                    this.CreateNewPageDataWithBypass().AddNewApp();
                }

                for (int i = 0; i > appsToCreate; --i)
                {
                    this.CreateNewPageDataWithBypass().DelApp(loan.nApps - 1 + i);
                }

                loan = this.CreateNewPageDataWithBypass();
                loan.InitSave();
            }

            NUnit.Framework.Assert.AreEqual(count, loan.nApps);
            for (int i = 0; i < loan.nApps; ++i)
            {
                loan.GetAppData(i).aBSsn = $"000-00-{(i * 2).ToString("D4")}";
            }

            NUnit.Framework.Assert.AreEqual("000-00-0000", loan.GetAppData(0).aBSsn);
            loan.Save();
        }

        private Type GetCallingType()
        {
            Type thisType = this.GetType();
            var s = new System.Diagnostics.StackTrace();
            for (int i = 0; i < s.FrameCount; ++i)
            {
                Type foundType = s.GetFrame(i).GetMethod().ReflectedType;
                if (foundType != thisType)
                {
                    return foundType;
                }
            }

            return null;
        }
    }
}
