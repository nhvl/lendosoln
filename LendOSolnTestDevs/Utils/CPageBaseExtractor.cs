﻿namespace LendingQB.Test.Developers.Utils
{
    using System;
    using System.Reflection;
    using global::DataAccess;

    /// <summary>
    /// Tool for extraction of the CPageBase instance held by a CPageData instance.
    /// </summary>
    public static class CPageBaseExtractor
    {
        /// <summary>
        /// Extract the CPageBase instance held by a CPageData instance.
        /// </summary>
        /// <param name="loanData">The CPageData instance.</param>
        /// <returns>The contained CPageBase instance.</returns>
        public static CPageBase Extract(CPageData loanData)
        {
            var fieldInfo = loanData.GetType().GetField(Helper.PageBaseName, BindingFlags.NonPublic | BindingFlags.Instance);
            return fieldInfo.GetValue(loanData) as CPageBase;
        }

        private sealed class Helper : CPageData
        {
            private Helper()
                : base(Guid.NewGuid())
            { }

            public static string PageBaseName
            {
                get
                {
                    return nameof(m_pageBase);
                }
            }
        }
    }
}
