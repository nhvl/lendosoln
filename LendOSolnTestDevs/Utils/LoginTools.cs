﻿namespace LendingQB.Test.Developers.Utils
{
    using System;
    using System.Security.Principal;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Security;
    using DataAccess;

    using PMLSecurity = PriceMyLoan.Security;
    using global::DataAccess;
    using LqbGrammar.Drivers.SecurityEventLogging;

    public class LoginTools
    {
        public const string DefaultLoginNm = "lotest001";
        public const string DefaultPassword = "lodblodb2";

        private static Guid x_testBrokerPmlLenderSiteId;
        private static Guid GetTestBrokerPmlLenderSiteId()
        {
            if (Guid.Empty == x_testBrokerPmlLenderSiteId)
            {
                // First get the broker id.
                AbstractUserPrincipal principal = LoginAs(TestAccountType.LoTest001);
                BrokerDB brokerDB = BrokerDB.RetrieveById(principal.BrokerId);

                x_testBrokerPmlLenderSiteId = brokerDB.PmlSiteID;
            }
            return x_testBrokerPmlLenderSiteId;
        }

        public static AbstractUserPrincipal Login(string loginName, string password)
        {
            PrincipalFactory.E_LoginProblem loginProblem = PrincipalFactory.E_LoginProblem.None;

            AbstractUserPrincipal principal = PrincipalFactory.CreateWithFailureType(loginName, password, out loginProblem, true, false, LoginSource.Website);

            if (null == principal)
            {
                throw new CBaseException(ErrorMessages.GenericAccessDenied, "Invalid username/password");
            }

            return principal;

        }

        private static AbstractUserPrincipal LoginPml(string loginName, string password, string customerCode)
        {
            PMLSecurity.PrincipalFactory.E_LoginProblem loginProblem = PMLSecurity.PrincipalFactory.E_LoginProblem.None;

            IPrincipal principal = PMLSecurity.PrincipalFactory.CreateWithFailureType(loginName, password, GetTestBrokerPmlLenderSiteId(), out loginProblem, false);

            if (null == principal)
                throw new CBaseException(ErrorMessages.GenericAccessDenied, "Invalid username/password");

            System.Threading.Thread.CurrentPrincipal = principal; // 11/6/2008 dd - This should be set in PrincipalFactory.
            return principal as AbstractUserPrincipal;

        }

        public static AbstractUserPrincipal LoginAs(TestAccountType type)
        {
            switch (type)
            {
                case TestAccountType.Corporate_Underwriter:
                    return Login("Corporate_Underwriter", "lodblodb2");
                case TestAccountType.Corporate_LockDesk:
                    return Login("Corporate_LockDesk", "lodblodb2");
                case TestAccountType.Corporate_Processor:
                    return Login("Corporate_Processor", "lodblodb2");
                case TestAccountType.Corporate_LoanOfficer:
                    return Login("Corporate_LoanOfficer", "lodblodb2");
                case TestAccountType.Corporate_LenderAccountExec:
                    return Login("Corporate_LenderAccountExec", "lodblodb2");
                case TestAccountType.Corporate_Manager:
                    return Login("Corporate_Manager", "lodblodb2");

                case TestAccountType.Branch1_Underwriter:
                    return Login("Branch1_Underwriter", "lodblodb2");
                case TestAccountType.Branch1_LockDesk:
                    return Login("Branch1_LockDesk", "lodblodb2");
                case TestAccountType.Branch1_Processor:
                    return Login("Branch1_Processor", "lodblodb2");
                case TestAccountType.Branch1_LoanOfficer:
                    return Login("Branch1_LoanOfficer", "lodblodb2");
                case TestAccountType.Branch1_LenderAccountExec:
                    return Login("Branch1_LenderAccountExec", "lodblodb2");
                case TestAccountType.Branch1_Manager:
                    return Login("Branch1_Manager", "lodblodb2");

                case TestAccountType.MainBranch_Underwriter:
                    return Login("Main_Underwriter", "lodblodb2");
                case TestAccountType.MainBranch_LockDesk:
                    return Login("Main_LockDesk", "lodblodb2");
                case TestAccountType.MainBranch_Processor:
                    return Login("Main_Processor", "lodblodb2");
                case TestAccountType.MainBranch_LoanOfficer:
                    return Login("Main_LoanOfficer", "lodblodb2");
                case TestAccountType.MainBranch_LenderAccountExec:
                    return Login("Main_LenderAccountExec", "lodblodb2");
                case TestAccountType.MainBranch_Manager:
                    return Login("Main_Manager", "lodblodb2");


                case TestAccountType.Underwriter:
                    return Login("Individual_Underwriter", "lodblodb2");
                case TestAccountType.LockDesk:
                    return Login("Individual_LockDesk", "lodblodb2");
                case TestAccountType.Processor:
                    return Login("Individual_Processor", "lodblodb2");
                case TestAccountType.LoanOfficer:
                    return Login("Individual_LoanOfficer", "lodblodb2");
                case TestAccountType.LenderAccountExec:
                    return Login("Individual_LenderAccountExec", "lodblodb2");
                case TestAccountType.Manager:
                    return Login("Individual_Manager", "lodblodb2");

                case TestAccountType.Pml_Supervisor:
                    return LoginPml("PML_Supervisor", "lodblodb2", "AUTOTEST");
                case TestAccountType.Pml_User0:
                    return LoginPml("PML_User0", "lodblodb2", "AUTOTEST");
                case TestAccountType.Pml_User1:
                    return LoginPml("PML_User1", "lodblodb2", "AUTOTEST");
                case TestAccountType.Pml_User2:
                    return LoginPml("PML_User2", "lodblodb2", "AUTOTEST");
                case TestAccountType.Pml_BP1:
                    return LoginPml("Pml_BP1", "lodblodb2", "AUTOTEST");
                case TestAccountType.Pml_LO1:
                    return LoginPml("Pml_LO1", "lodblodb2", "AUTOTEST");
                case TestAccountType.LoTest001:
                    return Login("lotest001", "lodblodb2");
                case TestAccountType.Corporate_InternalAdmin:
                    return Login("Corporate_InternalAdmin", "lodblodb2");
                case TestAccountType.LockDesk_AllPerFieldPerm:
                    return Login("Individual_AllPerFieldPermissions", "lodblodb2");
                case TestAccountType.LockDesk_LockDeskPerFieldPerm:
                    return Login("Individual_LockDeskPerFieldPerm", "lodblodb2");
                case TestAccountType.LockDesk_CloserPerFieldPerm:
                    return Login("Individual_CloserPerFieldPerm", "lodblodb2");
                case TestAccountType.LockDesk_AccountantPerFieldPerm:
                    return Login("Individual_AccountantPerFieldPerm", "lodblodb2");
                default:
                    throw new CBaseException(ErrorMessages.Generic, "Unsupport Account=" + type);
            }
        }
    }
}
