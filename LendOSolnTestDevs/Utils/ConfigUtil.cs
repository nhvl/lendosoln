﻿namespace LendingQB.Test.Developers.Utils
{
    using System;
    using System.Collections.Generic;
    using LqbGrammar.Queries;
    using NSubstitute;

    public class ConfigUtil
    {
        public ConfigUtil()
        {
            this.StageBuilder = new StageConfigBuilder();
            this.SiteBuilder = new SiteConfigBuilder();
        }

        private StageConfigBuilder StageBuilder { get; set; }

        private SiteConfigBuilder SiteBuilder { get; set; }

        public void AddStageConfigValue(string name, int value)
        {
            this.StageBuilder.AddValue(name, value, null);
        }

        public void AddStageConfigValue(string name, string value)
        {
            this.StageBuilder.AddValue(name, null, value);
        }

        public IConfigurationQueryFactory BuildMockConfigFactory()
        {
            var stageDriver = Substitute.For<IStageConfigurationQuery>();
            stageDriver.ReadAllValues().Returns(this.StageBuilder.GetConfiguration());

            var siteDriver = Substitute.For<ISiteConfigurationQuery>();
            siteDriver.ReadAllValues().Returns(this.SiteBuilder.GetConfiguration());

            var configFactory = Substitute.For<IConfigurationQueryFactory>();
            configFactory.CreateSiteConfiguration().Returns(siteDriver);
            configFactory.CreateStageConfiguration().Returns(stageDriver);

            return configFactory;
        }

        private class StageConfigBuilder
        {
            public StageConfigBuilder()
            {
                this.Values = new Dictionary<string, Tuple<int, string>>();
            }

            private Dictionary<string, Tuple<int, string>> Values { get; set; }

            public void AddValue(string name, int? intValue, string stringValue)
            {
                int normValue = intValue == null ? 0 : intValue.Value;
                this.Values[name] = new Tuple<int, string>(normValue, stringValue);
            }

            public Dictionary<string, Tuple<int, string>> GetConfiguration()
            {
                return this.Values;
            }
        }

        private class SiteConfigBuilder
        {
            public SiteConfigBuilder()
            {
                this.Values = new Dictionary<string, string>();
            }

            private Dictionary<string, string> Values { get; set; }

            public void AddValue(string name, string value)
            {
                this.Values[name] = value;
            }

            public Dictionary<string, string> GetConfiguration()
            {
                return this.Values;
            }
        }
    }
}
