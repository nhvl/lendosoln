﻿namespace LendingQB.Test.Developers.Utils
{
    using LqbGrammar;
    using LqbGrammar.Queries;

    public static class TestDataRootFolder
    {
        /// <summary>
        /// Gets the test folder root from configuration.  This must be called prior to using FoolHelper!
        /// </summary>
        /// <value>The test folder root from configuration.</value>
        static public string Location
        {
            get
            {
                using (var helper = new FoolHelper())
                {
                    if (!helper.RegisterRealDriver(FoolHelper.DriverType.ApplicationSettings))
                    {
                        return null;
                    }

                    string path = LendersOffice.Constants.ConstSite.TestDataFolder;
                    if (!path.Contains("LendOSolnTestDevs") && path.Contains("LendOSolnTest"))
                    {
                        path = path.Replace("LendOSolnTest", "LendOSolnTestDevs");
                    }

                    return LendersOffice.Constants.ConstSite.TestDataFolder;
                }
            }
        }
    }
}
