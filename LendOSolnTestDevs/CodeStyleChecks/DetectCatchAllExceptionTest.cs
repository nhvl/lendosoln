﻿namespace LendingQB.Test.Developers.CodeStyleChecks
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using NUnit.Framework;
    using System.Reflection;
    using DavidDao.Reflection;
    using System.Reflection.Emit;
    using PML = PriceMyLoan.UI;
    using DataAccess;
    using LendersOffice.Conversions.ComplianceEase;
    using global::DataAccess;

    /// <summary>
    /// This class will go through our dll to ensure that our code do not contains catch all exception.
    /// 
    /// Here are the example of catch all exception
    /// 
    /// try {
    /// .....
    /// }
    /// catch {
    /// .....
    /// }
    /// 
    /// OR
    /// 
    /// try {
    /// .....
    /// } catch (Exception) {
    /// .....
    /// }
    /// 
    ///     /// 6/16/2014 - As of today, it is no longer ALLOW to modify the test case to include ignore function.
    ///      Email David Dao (david@meridianlink.com) to create an exception.
    ///      The email MUST explain why it is desire to silently fail.
    /// </summary>
    [TestFixture]
    [Category(CategoryConstants.CodeStyleCheck)]
    [Category(CategoryConstants.UnitTest)]
    public class DetectCatchAllExceptionTest
    {
        private List<Assembly> GetAssemblyList()
        {
            // 7/7/2010 dd - Get a list of assembly to test.

            List<Assembly> list = new List<Assembly>();

            list.Add(typeof(CPageData).Assembly); // LendersOfficeLib
            list.Add(typeof(LendersOffice.logout).Assembly); // LendersOfficeApp
            list.Add(typeof(PML.index).Assembly); // StandAlonePricingEngine

            return list;
        }

        [Test]
        public void MainValidation()
        {
            // 7/7/2010 dd - Skip these classes because it contains reference to Excel Interop.
            List<string> skipClasses = new List<string>()
            {
                "LendersOfficeApp.ObjLib.RulesManagement.ExcelApp",
                "LendersOfficeApp.ObjLib.RulesManagement.WorkSheets",
                "LendersOfficeApp.ObjLib.RulesManagement.WorkBook",
                "LendersOfficeApp.ObjLib.RulesManagement.WorkSheet",
                "LendersOfficeApp.ObjLib.RulesManagement.SchemaFile",

                // 4/5/2017 ML - NightlyTaskRunnerProcessor has a RunParallel method that is inlined, 
                // so we skip the class instead of trying to skip based on the compiler-generated 
                // method name. Once the parallel method has been vetted and replaces the old sequential 
                // method, we can remove this entry in favor of adding the name to manualSkipMethods.
                "LendersOffice.ObjLib.Task.NightlyTaskRunnerProcessor", 
            };

            // 6/15/2015 dd - Manually allow methods to have catch all exception. 
            // Each entry should contains the case number and why it MUST have catch all.
            HashSet<string> manualSkipMethods = new HashSet<string>() 
            {
                "LendersOffice.ObjLib.Disclosure.NightlyDisclosureRunner::ProcessEConsentExpiration", // 6/15/2015 dd - OPM 217402 - To allow process of other loan.
                "LendersOffice.ObjLib.Disclosure.NightlyDisclosureRunner::ProcessEConsentExpirationForLoans", // 6/17/2015 gf - OPM 217402 - To allow process of other loan.
                "EDocs.Utils.EDocConversionQueue::Enqueue", // 7/2/2015 AV - 219038 Enqueuing Documents For conversion should not cause request to fail because its easy to requeue after fixing.
                "DataAccess.Tools::ConvertHtmlFileToPdfFile",  //8/6/2015 AV - 222087 - The method tries one more time and then throws any exception hence the catch all.
                "LendersOffice.QueryProcessor.ScheduleCustomReport::RunReports", //8/6/2015 AV - Added These per DD request.
                "LendersOffice.QueryProcessor.ScheduleCustomReport::RunAndEmail", //8/6/2015 AV - Added These per DD request.
                "LendersOfficeApp.LOAdmin.Manage.UpdateForm::RestartRasterizer_Click", //8/18/2015 AV - 223394 - The whole point of the method is to get the call stack. No need to bubble error.
                "DataAccess.CDateTime::GetSafeDateTimeWithTimeForComputation", // 10/3/2015 ML - 227744 - This method contains a fallback value to return in case an exception is thrown to prevent the exception from bubbling up.
                "LendersOffice.Edocs.DocMagic.PDFProcessor::UploadDocuments", // 10/28/15 GF - We still want a catch all here to avoid interfering with queue processing.
                "LendersOffice.Edocs.DocMagic.PDFCancellableRunnable::UploadDocuments", // 10/28/15 GF - We still want a catch all here to avoid interfering with queue processing.
                "LendersOfficeApp.LOAdmin.Manage.UpdateForm::LicneseUpdateByLoanOfficer_OnClick", //1/4/2016 AV - Temporary Migration Not going to be required soon
                "LendersOfficeApp.LOAdmin.Manage.UpdateForm::LicenseUpdateByLoanOfficer_NEW", //11/29/2016 ad - New version of the above method that is exempted.
                "LendersOffice.ObjLib.LoanFileCache.LoanUpdater::Run", // 2/19/2016 ejm - This class contains migrations. We want to catch all exceptions.
                "LendersOffice.ObjLib.LoanFileCache.LoanUpdater::RunNEW", // 11/29/2016 ad - New version of the above method that is exempted.
                "DataAccess.CBase::ToMoneyString6DecimalDigits", // 6/1/2016 EM - This method parses a string up to 6 decimal digits.  Same logic as the other rounding methods.  
                "LendersOffice.ObjLib.Disclosure.NightlyDisclosureRunner::SendEmail", // 8/16/2016 GF - This method was extracted from a method included on the exception whitelist.
                "LendersOffice.Common.MSMQLogger::LogImpl", // 11/15/2016 AD - The containing class was copied without modification from CommonProjectLib.Logging.MSMQLogger which has been marked as obsolete.
                "LendersOfficeApp.WebService.QuickPricer::RunQuickPricerV2", // 11/22/2016 GF - We don't want to throw exceptions out of this web service. We catch and return an error response.
                "DataAccess.CCcTemplateBase::InitSaveNEW", // 11/30/16 GF - This is a new version of a method that was formerly in the whitelist. It will likely be renamed to the old name eventually, but in the meantime I think it can be ignored.
                "LendersOffice.RatePrice.DerivationJob::DeriveNEW", // 11/30/16 GF - This is a new version of a method that was formerly in the whitelist. It will likely be renamed to the old name eventually, but in the meantime I think it can be ignored.
                "DataAccess.Utilities.EmployeeRole::ClearAssignment", // 12/22/16 AV - This was code moved from another place in the system.
                "DataAccess.Utilities.EmployeeRole::AssignEmployee", // 12/22/16 AV - This was code moved from another place in the system.
                "DataAccess.Utilities.EmployeeRole::Search", // 12/22/16 AV - This was code moved from another place in the system.
                "DataAccess.CBase::ToMoneyStringWithHmdaNa", // 1/9/2017 ml start - these methods handle HMDA "Not Applicable" along with rep versions of the values.
                "DataAccess.CBase::ToRateStringWithHmdaNa",
                "DataAccess.CBase::ToCountStringWithHmdaNa", // 1/9/2017 end
                "DataAccess.CBase::ToMoneyStringWithHmdaNaAndBlank", // 10/18/17 jk - another HMDA "Not Applicable" method handler that also supports empty string values.
                "DataAccess.CBase::ToCountStringWithHmdaNaAndBlank", // 11/15/17 jk - another HMDA "Not Applicable" method handler that also supports empty string values.
                "DataAccess.CBase::ToCountStringCreditScore",       // 11/22/17 jk - moved out the credit score _rep field that had catch all logic into a separate method.
                "DataAccess.LosConvert::ToCountStringCreditScore",  // 11/22/17 jk - moved out the credit score _rep field that had catch all logic into a separate method.
                "DataAccess.CLoanFileCreator::BeginCreateFileFromSourceLoan", // 7/10/2017 SK for DT - this was renamed and previously ignored.
                "DataAccess.AutoExpiredTextCache::AddToCache", // 1/31/2017 - AV Refactoring current code.
                "LUnit.LUnitFactory::LUnitFactory", // 2/14/2017 - AD factory switching between legacy and new LUnit implementations
                "LUnit.WebRunner::CreateTestManager", // 3/1/2017 - AD new LUnit
                "LUnit.Legacy.TestClass::Run", // 2/10/2017 - AD cleaning up LUnit -> moved files
                "LUnit.Legacy.TestClass::RunMethodImp", // 2/10/2017 - AD cleaning up LUnit -> moved files
                "LUnit.Legacy.TestClass+RunMethodProxy::Run", // 2/10/2017 - AD cleaning up LUnit -> moved files
                "LUnit.Legacy.TestMethod::GetStackTrace", // 3/1/2017 - AD cleaning up LUnit -> moved files
                "LUnit.Legacy.TestMethod::Run", // 2/10/2017 - AD cleaning up LUnit -> moved files
                "LUnitEngine.Runners.ClassRunner::PreRunSetInstance", // 2/14/2017 - AD new LUnit
                "LUnitEngine.Runners.MethodRunner::HandleRun", // 3/1/2017 - AD new LUnit
                "LUnitEngine.Runners.MethodRunner::HandleRunOnThread", // 2/14/2017 - AD new LUnit
                "LUnitEngine.Runners.MethodRunner::HandleTimeout", // 3/1/2017 - AD new LUnit
                "LUnitEngine.Runners.MethodRunner::RunTearDown", // 3/1/2017 - AD new LUnit
                "LUnitEngine.Runners.Utility::RunSetUpOrTearDownMethod", // 2/14/2017 - AD new LUnit
                "LendersOffice.DistributeUnderwriting.LpeTaskReceiver::RunTask", // 3/2/2017 - AV Do not want the LpeTaskReceiver to crash as it will bring down the thread.
                "LendersOffice.Drivers.Logger.CompoundLoggingDriver::CallAdapter", // 3/27/2017 - AD logging should not generate exceptions
                "LendersOfficeApp.newlos.Services.OrderCreditService+OrderCreditServiceItem::SetBorrowerInfo", // 3/21/2017 TJ - Legacy method moved from OrderCreditService to OrderCreditServiceItem
                "LendersOfficeApp.newlos.Services.OrderCreditService+OrderCreditServiceItem::OrderCredit", // 3/21/2017 TJ - Legacy method moved from OrderCreditService to OrderCreditServiceItem
                "PriceMyLoan.webapp.DisclosuresServiceItem::PlaceOrder", // 3/22/2017 ML - This method handles naked Exceptions by displaying a friendly error message to the user.
                "Adapter.MethodInvoke.MessageServiceHandler::Process", // 3/22/2017 AD - Message service handler methods cannot throw exceptions
                "Adapter.MethodInvoke.MessageServiceHandler::Execute", // 3/27/2017 AD - Retry the method invocation in light of an exception.
                "LendersOffice.ObjLib.FieldInfoCache.FieldInfoCacheManager::GetCachedProperties", // 3/28/2017 ML - This method returns null if an error occurs while retrieving data from the field info cache.
                "LendersOffice.Admin.BranchDB::set_m_fieldSet", // 4/26/2017 DT - Exclude BranchDB methods that fail now due to namespace changes
                "LendersOffice.Admin.BranchDB::LogIfLicenseXmlChanged", // 4/26/2017 DT - Exclude BranchDB methods that fail now due to namespace changes
                "LendersOffice.Admin.BrokerDB::Save", // 4/26/2017 DT - Exclude BrokerDB methods that fail now due to namespace changes
                "LendersOffice.Admin.BrokerDB::GetPacificTimeFromLenderTimezone", // 4/26/2017 DT - Exclude BrokerDB methods that fail now due to namespace changes
                "LendersOffice.Admin.BrokerDB::GetLenderTimeFromPacificTime", // 4/26/2017 DT - Exclude BrokerDB methods that fail now due to namespace changes
                "LendersOffice.Admin.EmployeeDB::SetDataFromSqlReader", // 4/26/2017 DT - Exclude EmployeeDB methods that fail now due to namespace changes
                "LendersOffice.Admin.EmployeeDB::Save", // 4/26/2017 DT - Exclude EmployeeDB methods that fail now due to namespace changes
                "LendersOffice.Admin.EmployeeDB::get_Pipeline", // 4/26/2017 DT - Exclude EmployeeDB methods that fail now due to namespace changes
                "LendersOffice.Admin.EmployeeDB::get_AnonymousQuickPricerUrl", // 4/26/2017 DT - Exclude EmployeeDB methods that fail now due to namespace changes
                "LendersOffice.Rolodex.RolodexDB::PullInfoFromReader", // 10/6/2017 DT - Exclude methods that fail now due to namespace changes
                "LendersOffice.Rolodex.RolodexDB::Retrieve", // 10/6/2017 DT - Exclude methods that fail now due to namespace changes
                "LendersOffice.Rolodex.RolodexDB::Save", // 10/6/2017 DT - Exclude methods that fail now due to namespace changes
                "LendersOffice.RatePrice.Helpers.PolicyParser::BuildPolicyTree", // 10/6/2017 DT - Exclude methods that fail now due to namespace changes
                "LendersOffice.RatePrice.Helpers.PolicyParser::ParseRuleEntry", // 10/6/2017 DT - Exclude methods that fail now due to namespace changes
                "LendersOffice.RatePrice.Helpers.PolicyNodeConstructor::UpdateDb", // 10/6/2017 DT - Exclude methods that fail now due to namespace changes
                "LendersOffice.Pdf.CAmortizationPDF::AddAmortizationTable", // 10/6/2017 DT - Exclude methods that fail now due to namespace changes
                "LendersOffice.Pdf.CCustomPDF::ApplyData", // 10/6/2017 DT - Exclude methods that fail now due to namespace changes
                "LendersOffice.Pdf.CLoanSummaryPDF::get_UseUnderwritingConditions", // 10/6/2017 DT - Exclude methods that fail now due to namespace changes
                "LendersOffice.Pdf.CLoanTaskPDF::ApplyData", // 10/6/2017 DT - Exclude methods that fail now due to namespace changes
                "LendersOffice.Pdf.CMortgageLoanCommitment_1PDF::get_UseUnderwritingConditions", // 10/6/2017 DT - Exclude methods that fail now due to namespace changes
                "LendersOffice.Pdf.PDFHandler::DisplayPDF", // 10/6/2017 DT - Exclude methods that fail now due to namespace changes
                "LendersOffice.Pdf.CTruthInLending32PDF::ApplyData", // 10/6/2017 DT - Exclude methods that fail now due to namespace changes
                "LendersOffice.Pdf.CTruthInLendingObsoletePDF::ApplyData", // 10/6/2017 DT - Exclude methods that fail now due to namespace changes
                "LendersOffice.Pdf.CTruthInLendingPDF::ApplyData", // 10/6/2017 DT - Exclude methods that fail now due to namespace changes
                "LendersOfficeApp.los.Template.LpBatchEdit::btnOK_Click", // 5/15/2017 ML - Moved catch all handling from inside data save method to within button click to allow display error messages to users.
                "LendersOfficeApp.Template.BatchDerivationJob::InsertClick", // 5/23/2017 Thien - Display error to internal user. In fact, this method cloned from legacy LendersOfficeApp.Template.CreateDerivationJob::InsertClick
                "LendersOffice.Events.RateLockBroken::Send", // 5/26/17 GF - I pulled this method out of a page and into a class in the lib. It had a catch all and I don't want to change behavior.
                "LendersOfficeApp.los.RatePrice.CApplicantPrice::ComputePricesImpl", // 7/6/2017 AD - Changing the name of a seemingly exempt method caused it to generate TeamCity errors.
                "LendersOfficeApp.los.RatePrice.AltComputePrices::ComputePrices", // 7/6/2017 AD - Copy/paste of previously exempted method.
                "PriceMyLoan.webapp.RolodexListV2::GetDataTabPmlUsers", // 8/16/2017 ML - Copy/paste of previously exempted method.
                "DataAccess.ParameterReporting::LoadClosingCostSet", // 8/8/2017 ML - Default to the legacy closing cost list when a broker's closing cost setup is invalid.
                "LendersOffice.Conversions.LOFormatImporter::SetAppraisalOrderTracking", 
                "LendersOffice.Integration.Appraisals.AppraisalVendorFactory::AttachGDMSDocumentsToOrder",
                "LendersOffice.Integration.Appraisals.AppraisalVendorFactory::GetGdmsCcType",
                "DataAccess.CPageBase::SqlDataAdapter_RowUpdating", // 11/7/17 GF - Renamed m_sqlDataAdapter_RowUpdating method that was on whitelist already.
                "DataAccess.CPageBase::PrepareUpdateCommandParameters",  // 11/7/17 GF - Renamed PrepareCmdParams method that was on whitelist already.
                "LendersOffice.Integration.Appraisals.AppraisalVendorFactory::GetGdmsCcType", 
                "LendersOfficeApp.newlos.Test.Irs4506TVendorXsltTester::ClickTransform", // 1/11/18 SK - opm 463659 since these are test methods for internal users, we can bubble up the error.
                "LendersOfficeApp.newlos.Test.Irs4506TVendorXsltTester::ClickExport", // 1/11/18 SK - opm 463659 since these are test methods for internal users, we can bubble up the error.
                "LendersOfficeApp.newlos.Test.Irs4506TVendorXsltTester::ClickGet4506TRequestXml", // 1/11/18 SK - opm 463659 since these are test methods for internal users, we can bubble up the error.
                "LendersOfficeApp.newlos.Test.Irs4506TVendorXsltTester::ClickCompareXsltAnd4506tXml", // 1/11/18 SK - opm 463659 since these are test methods for internal users, we can bubble up the error.
                "LendersOfficeApp.newlos.Test.LoXmlWithXsltTester::ClickTransform", // 1/11/18 SK - opm 463659 since these are test methods for internal users, we can bubble up the error.
                "LendersOfficeApp.newlos.Test.LoXmlWithXsltTester::ClickExport", // 1/11/18 SK - opm 463659 since these are test methods for internal users, we can bubble up the error.
                "LendersOffice.ObjLib.BackgroundJobs.BackgroundJobProcessor::RunProcessor", // 12/4/2017 ejm - This method needs to be able to catch all exceptions thrown when running a job so that it can set the job to invalid before rethrowing.
                "LendersOffice.Audit.AbstractAuditItem::LoadDetailContentIfNeed", // 3/21/2018 Copy/paste of previously constructor.
                "LendersOffice.QueryProcessor.Schema::AddSpecial", // 3/20/2018 ML - This method is replicated from other whitelisted Schema methods.
                "LendersOffice.ObjLib.Appraisal.AppraisalOrderView::Save", // 3/20/2018 ejm - Old code moved to new file.
                "LendersOfficeApp.los.Template.InsertExternalProduct::DoInsert", // 6/8/2018 EM - Existing Code that got updated
                "LendersOfficeApp.los.Template.CreateDerivationJob::DoInsert", // 6/9/2018 VL - Existing Code that got updated
                "LendersOffice.CalculatedFields+Liability::Audit", // 5/17/18 gf - copied old code to new file
                "LendersOffice.ObjLib.CustomMergeFields.CustomMergeFieldRepository::GetCustomFieldNumber", // 6/21/2018 ML - This method was previously whitelisted and was moved to a new namespace.
                "LendersOfficeApp.los.CustomForms.CustomFormList::ProcessPostbackCommand", // 6/21/2018 ML - This method was previously whitelisted and was moved to a new namespace.
                "LendersOffice.ObjLib.LqbDataService.LqbDataServiceHandler::ProcessRequest", // 7/16/18 GF - We want to catch all exceptions to control the response from the endpoint.
                "LendersOfficeApp.newlos.RunBrowserXtVerificationCheck::AddInterceptLog", // 8/24/2018 ML start - Log-in issues are SLA 1 violations. At support's request, redirect user to next step on error.
                "LendersOfficeApp.newlos.RunBrowserXtVerificationCheck::AddBypassLog", 
                "LendersOfficeApp.newlos.RunBrowserXtVerificationCheck::OnInit",
                "LendersOfficeApp.newlos.RunBrowserXtVerificationCheck::Page_Load", // 8/24/2018 ML end
                "LendersOffice.ObjLib.BackgroundJobs.MIFrameworkRequestJob::PerformAndUpdateJob", // 9/17/2018 ejm - Need to catch any exception once the MI request has finished.
                "DataAccess.CPageBase::DelAppImpl",  // 9/10/18 GF - Extracted old code into new method.
                "LendersOffice.ObjLib.BackgroundJobs.CreditReportRequestJob::PerformAndUpdateJob", // 9/26/2018 ejm - Need to catch any exceptions once job has finished.
                "LendersOfficeApp.newlos.Services.OrderCreditService+OrderCreditServiceItem::PollForCreditReportRequestResults", // 9/26/2018 ejm - old code into new method
                "LendersOfficeApp.newlos.Services.OrderCreditService+OrderCreditServiceItem::OrderCreditSynchronously", // 9/26/2018 ejm - old code into new method
                "LendersOffice.Pdf.Async.AsyncPdfGenerationHelper::GenerateSinglePdf",  // 9/21/18 ML - Extracted old code into new method.
                "LendersOffice.Pdf.Async.AsyncPdfGenerationHelper::GeneratePaymentStatementPdf",  // 10/22/18 JE - Like GenerateSinglePdf, requires a catchall for graceful failure.
                "LendersOffice.Pdf.PDFHandler::SubmitPaymentStatementPdfJob",  // 10/22/18 JE - Uses catchall to fail gracefully.
                "LendersOffice.ObjLib.TPO.PMLLogoHandler::ProcessRequest", // 11/29/2018 ML - Extracted old code into new namespace.
                "LendersOffice.ObjLib.Edocs.Handlers.DocumentUploader::ProcessRequest", // 2/12/2019 ML OPM 478415  - Return consistent error to client
            };

            StringBuilder sb = new StringBuilder() ;
            foreach (Assembly assembly in GetAssemblyList())
            {
                foreach (Type t in assembly.GetTypes())
                {
                    if (skipClasses.Any(skipClass => t.FullName.Contains(skipClass))) // || skipQueueProcessingClasses.Contains(t.FullName))
                    {
                        continue;
                    }
                    MethodInfo[] methodList = t.GetMethods(BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly);
                    foreach (MethodInfo method in methodList)
                    {
                        bool bHasGeneralExceptionHandling = IsContainsCatchAllException(method);
                        if (bHasGeneralExceptionHandling)
                        {
                            string str = t.FullName + "::" + method.Name;
                            if (manualSkipMethods.Contains(str) == false && CatchAllHardcodeIgnoreList.IgnoreList.IsIgnore(str) == false)
                            {
                                sb.AppendLine(str);
                            }
                        }
                    }

                }
            }
            Assert.IsTrue(sb.Length == 0, "These methods contains catch all:" + Environment.NewLine + sb.ToString());

        }
        private bool IsContainsCatchAllException(MethodInfo method)
        {
            if (null == method)
            {
                return false;
            }
            MethodBody methodBody = method.GetMethodBody();
            if (null == methodBody)
            {
                return false;
            }

            byte[] bytes = methodBody.GetILAsByteArray();
            foreach (var exceptionClause in methodBody.ExceptionHandlingClauses)
            {
                switch (exceptionClause.Flags)
                {
                    case ExceptionHandlingClauseOptions.Clause:

                        if (exceptionClause.CatchType == typeof(System.Exception) || exceptionClause.CatchType == typeof(System.Object))
                        {
                            byte[] handlerBytes = new byte[exceptionClause.HandlerLength];
                            Array.Copy(bytes, exceptionClause.HandlerOffset, handlerBytes, 0, exceptionClause.HandlerLength);

                            if (ContainsThrowClause(handlerBytes) == false)
                            {
                                return true;
                            }
                        }
                        break;
                    case ExceptionHandlingClauseOptions.Fault:
                    case ExceptionHandlingClauseOptions.Filter:
                    case ExceptionHandlingClauseOptions.Finally:
                        continue;
                }
            }
            return false;
        }
        private bool ContainsThrowClause(byte[] bytes)
        {
            List<ILInstruction> ilList = MSILParser.Parse(bytes);

            foreach (ILInstruction o in ilList)
            {
                if (o.OpCode == OpCodes.Throw || o.OpCode == OpCodes.Rethrow)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
