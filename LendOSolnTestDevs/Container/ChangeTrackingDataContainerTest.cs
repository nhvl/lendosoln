﻿namespace LendingQB.Test.Developers.Container
{
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess;
    using global::DataAccess;
    using LqbGrammar.DataTypes;
    using NSubstitute;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public class ChangeTrackingDataContainerTest
    {
        [Test]
        public void Size_Always_EqualsWrappedSize()
        {
            var wrappedContainer = Substitute.For<IDataContainer>();
            wrappedContainer.Size.Returns(5);

            var container = new ChangeTrackingDataContainer(wrappedContainer);

            Assert.AreEqual(5, container.Size);
        }

        [Test]
        public void Contains_FieldExists_ReturnsTrue()
        {
            var wrappedContainer = Substitute.For<IDataContainer>();
            wrappedContainer.Contains("Field").Returns(true);

            var container = new ChangeTrackingDataContainer(wrappedContainer);

            Assert.AreEqual(true, container.Contains("Field"));
        }

        [Test]
        public void Contains_FieldDoesNotExist_ReturnsFalse()
        {
            var wrappedContainer = Substitute.For<IDataContainer>();
            wrappedContainer.Contains("Field").Returns(false);

            var container = new ChangeTrackingDataContainer(wrappedContainer);

            Assert.AreEqual(false, container.Contains("Field"));
        }

        [Test]
        public void Indexer_NoUpdates_ReturnsValueFromWrapped()
        {
            var wrappedContainer = Substitute.For<IDataContainer>();
            wrappedContainer.Contains("Field").Returns(true);
            wrappedContainer["Field"].Returns("Value");

            var container = new ChangeTrackingDataContainer(wrappedContainer);

            Assert.AreEqual("Value", container["Field"]);
        }

        [Test]
        public void Indexer_AfterFieldUpdate_ReturnsUpdatedValue()
        {
            var wrappedContainer = Substitute.For<IDataContainer>();
            wrappedContainer.Contains("Field").Returns(true);
            wrappedContainer["Field"].Returns("Value");

            var container = new ChangeTrackingDataContainer(wrappedContainer);
            container["Field"] = "Updated";

            Assert.AreEqual("Updated", container["Field"]);
        }

        [Test]
        public void Indexer_UpdateField_DoesNotUpdateWrapped()
        {
            var wrappedContainer = Substitute.For<IDataContainer>();
            wrappedContainer.Contains("Field").Returns(true);
            wrappedContainer["Field"].Returns("Value");

            var container = new ChangeTrackingDataContainer(wrappedContainer);
            container["Field"] = "Updated";

            Assert.AreEqual("Value", wrappedContainer["Field"]);
        }

        [Test]
        public void GetUpdates_SingleUpdatedField_ReturnsFieldAndValue()
        {
            var wrappedContainer = Substitute.For<IDataContainer>();
            wrappedContainer.Contains("Field1").Returns(true);
            wrappedContainer.Contains("Field2").Returns(true);
            wrappedContainer["Field1"].Returns("Value1");
            wrappedContainer["Field2"].Returns("Value2");

            var container = new ChangeTrackingDataContainer(wrappedContainer);
            container["Field1"] = "Updated";
            var updates = container.GetChanges();

            Assert.AreEqual(1, updates.Count());
            var updatedField = updates.Single();
            Assert.AreEqual("Field1", updatedField.Key);
            Assert.AreEqual("Updated", updatedField.Value);
        }

        [Test]
        public void GetUpdates_SingleFieldUpdatedMultipleTimes_ReturnsFieldAndMostRecentValue()
        {
            var wrappedContainer = Substitute.For<IDataContainer>();
            wrappedContainer.Contains("Field1").Returns(true);
            wrappedContainer.Contains("Field2").Returns(true);
            wrappedContainer["Field1"].Returns("Value1");
            wrappedContainer["Field2"].Returns("Value2");

            var container = new ChangeTrackingDataContainer(wrappedContainer);
            container["Field1"] = "Updated1";
            container["Field1"] = "Updated2";
            var updates = container.GetChanges();

            Assert.AreEqual(1, updates.Count());
            var updatedField = updates.Single();
            Assert.AreEqual("Field1", updatedField.Key);
            Assert.AreEqual("Updated2", updatedField.Value);
        }

        [Test]
        [ExpectedException(ExpectedException = typeof(KeyNotFoundException))]
        public void Indexer_RetrieveFieldThatDoesNotExist_ThrowsException()
        {
            var wrappedContainer = Substitute.For<IDataContainer>();
            wrappedContainer.Contains("Field").Returns(false);

            var container = new ChangeTrackingDataContainer(wrappedContainer);
            var value = container["Field"];
        }

        [Test]
        [ExpectedException(ExpectedException = typeof(KeyNotFoundException))]
        public void Indexer_SetFieldThatDoesNotExist_ThrowsException()
        {
            var wrappedContainer = Substitute.For<IDataContainer>();
            wrappedContainer.Contains("Field").Returns(false);

            var container = new ChangeTrackingDataContainer(wrappedContainer);
            container["Field"] = "Updated";
        }

        [Test]
        public void Indexer_SetPersonNameFieldToCurrentValue_DoesNotConsiderValueUpdated()
        {
            var wrappedContainer = Substitute.For<IDataContainer>();
            wrappedContainer.Contains("Field").Returns(true);
            wrappedContainer["Field"].Returns(PersonName.Create("Test"));

            var container = new ChangeTrackingDataContainer(wrappedContainer);
            container["Field"] = PersonName.Create("Test");
            int numUpdatedFields = container.GetChanges().Count();

            Assert.AreEqual(0, numUpdatedFields);
        }

        [Test]
        public void Indexer_SetNullFieldToNewValue_ConsidersValueUpdated()
        {
            var wrappedContainer = Substitute.For<IDataContainer>();
            wrappedContainer.Contains("Field").Returns(true);
            wrappedContainer["Field"].Returns(null);

            var container = new ChangeTrackingDataContainer(wrappedContainer);
            container["Field"] = PersonName.Create("Test");
            int numUpdatedFields = container.GetChanges().Count();

            Assert.AreEqual(1, numUpdatedFields);
        }

        [Test]
        public void Indexer_SetNullFieldToNull_DoesNotConsiderValueUpdated()
        {
            var wrappedContainer = Substitute.For<IDataContainer>();
            wrappedContainer.Contains("Field").Returns(true);
            wrappedContainer["Field"].Returns(null);

            var container = new ChangeTrackingDataContainer(wrappedContainer);
            container["Field"] = null;
            int numUpdatedFields = container.GetChanges().Count();

            Assert.AreEqual(0, numUpdatedFields);
        }

        [Test]
        public void Indexer_SetFieldToNull_ConsidersValueUpdated()
        {
            var wrappedContainer = Substitute.For<IDataContainer>();
            wrappedContainer.Contains("Field").Returns(true);
            wrappedContainer["Field"].Returns(PersonName.Create("Test"));

            var container = new ChangeTrackingDataContainer(wrappedContainer);
            container["Field"] = null;
            int numUpdatedFields = container.GetChanges().Count();

            Assert.AreEqual(1, numUpdatedFields);
        }
    }
}
