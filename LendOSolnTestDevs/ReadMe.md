﻿# Overview

`LendOSolnTestDevs` is a project for holding tests that will be executed by 
developers as part of their day to day work.

## Purpose

The Foundation Team has decided that we will be writing a lot of unit tests 
using an IoC container and mocking library.  In preparation we wish to 
seperate these tests from LUnit to prevent contamination both ways.  We want 
to protect LUnit from any possibility of corruption from mocks.  We also wish 
to keep developers unhindered from the long-running smoke tests so they can 
get in the habit of running the full test suite.

## Details

There are several different types of tests that developers will write and 
execute.  The class `CategoryConstants` gives a description of each type. 
The intent is to make use of the `Nunit.Framework.Category` attribute to 
mark the tests appropriately.  Any given test class may include tests from 
different categories, so attempting to physically separate different test 
categories from each other is likely to be clumsy.  The use of the category 
attribute is a clean way to distinguish them and execute the correct set 
of tests at the correct time.

Since a lot of the current unit/behavioral tests make use of drivers from 
the FOOL framework, the `TestSuiteSetup` class does the application 
initialization.  Unlike the `LendOSolnTest` project, the initialization 
code does not register the normal driver factories.  It has been decided that 
doing so can cause a test to accidentally make use of a driver that calls 
out on an external port.  Instead, any attempt to use a driver that hasn't 
been mocked will result in a runtime exception and the test writer will 
be notified of the need to register the mock.

To simplify working with the FOOL framework, the class `FoolHelper` should 
be used.  It understands what core services are required during application 
startup (e.g., initialization of `ConstStage`) and handles those details for 
you.  The helper also implements IDisposable to clean up the `GenericLocator` 
after the execution of each test run.  If you find yourself writing test 
code that is going to hit an external port, or you wish to register mocks, 
please do so by calling methods on the `FoolHelper` class.

Please be mindful of the difference between unit tests and integration 
tests.  Any test that hits one or more external ports is an integration 
test.  If you can be sure that all the external ports are mocked out or 
aren't hit, then you have a potential unit test.  Please mark the tests 
correctly.  You can use an `/include:UNIT` startup argument for NUnit so that 
only unit tests are run as part of your regular practice.  This will make 
executions of the full suite quite short, on the order of a few seconds.  That 
way you can run the full test suite frequently and make that part of your 
every day coding habits.

You can make files with test data in the `TestData` folder.  If you do so, 
please use the properties to set the Build Action to Embedded Resource. 
You can then read the full text of the file using the utility method 
`Utils.TestUtilities.ReadTextResource`.  Code that gets hold of the test 
data as an embedded resource can be marked as a unit test.  If instead you 
load the file from disk you must mark the test as an integration test since 
you are calling an external port.
