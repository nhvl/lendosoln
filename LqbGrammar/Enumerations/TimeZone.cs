﻿namespace LqbGrammar.Enumerations
{
    /// <summary>
    /// Sometimes database datetime values are accompanied with a column that
    /// carries one of these values to indicate the time zone for the datetime value.
    /// Note that daylight savings time is NOT included here, the datetime value is 
    /// used to determine that because all the time zones here are within the United States.
    /// </summary>
    public enum E_sTimeZoneT
    {
        /// <summary>
        /// The time zone is unknown.
        /// </summary>
        LeaveBlank = 0,

        /// <summary>
        /// The time zone is Pacific Time.
        /// </summary>
        PacificTime = 1,

        /// <summary>
        /// The time zone is Mountain Time.
        /// </summary>
        MountainTime = 2,

        /// <summary>
        /// The time zone is Central Time.
        /// </summary>
        CentralTime = 3,

        /// <summary>
        /// The time zone is Eastern Time.
        /// </summary>
        EasternTime = 4,

        /// <summary>
        /// The time zone is Alaskan Time.
        /// </summary>
        AlaskaTime = 5,

        /// <summary>
        /// The time zone is Hawaiian Time.
        /// </summary>
        HawaiiTime = 6,

        /// <summary>
        /// The time zone is that of the user viewing the data, 
        /// as determined by the user's configured location.
        /// </summary>
        DisplayLocalTime = 7
    }
}
