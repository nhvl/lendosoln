﻿namespace LqbGrammar.Utils
{
    using System;

    /// <summary>
    /// Encapsulates logic for dealing with checking and upgrading a resource that
    /// expires, in a thread safe way.
    /// </summary>
    public class ExpiringResource
    {
        /// <summary>
        /// Flag that is seen by all threads, true when some thread has been assigned to carry out the update.
        /// </summary>
        private bool updateInProgress;

        /// <summary>
        /// The function that reports whether or not the resouces has expired.
        /// </summary>
        private IsExpiredDelegate isExpiredMethod;

        /// <summary>
        /// The function that updates the resource.
        /// </summary>
        private HandleExpiredDelegate handleExpiredMethod;

        /// <summary>
        /// Initializes a new instance of the <see cref="ExpiringResource"/> class.
        /// </summary>
        public ExpiringResource()
        {
            this.updateInProgress = false;
        }

        /// <summary>
        /// Return true if the expiration condition is reached.
        /// </summary>
        /// <returns>True if the expiration condition is reached.</returns>
        public delegate bool IsExpiredDelegate();

        /// <summary>
        /// Do whatever is expected to happen upon expiration, AND reset switches used to detect
        /// expiration condition.
        /// </summary>
        public delegate void HandleExpiredDelegate();

        /// <summary>
        /// Sets the IsExpiredMethod.
        /// </summary>
        /// <value>The IsExpired method.</value>
        public IsExpiredDelegate IsExpired
        {
            set { this.isExpiredMethod = value; }
        }

        /// <summary>
        /// Sets the HandleExpiredMethod.
        /// </summary>
        /// <value>The HandleExpired method.</value>
        public HandleExpiredDelegate HandleExpired
        {
            set { this.handleExpiredMethod = value; }
        }

        /// <summary>
        /// Call this method before using a resource.  This will call the IsExpired
        /// delegate, and if it returns true it will call the HandleExpired delegate
        /// in a thread safe manner.
        /// IMPORTANT: WHEN USING THE RESOURCE, YOU SHOULD ALWAYS REFERENCE IT FROM A
        /// LOCAL VARIABLE, NOT THE MEMBER DATA VARIABLE.  ALSO, WHEN UPDATING, YOU SHOULD CREATE
        /// A NEW OBJECT AND FULLY INITIALIZE AND ONLY THEN ASSIGN IT TO THE MEMBER VARIABLE.  THIS
        /// IS IMPORTANT TO PRESERVE THREAD SAFETY.
        /// </summary>
        /// <param name="lockingObject">The object that will be locked to enforce thread safety.</param>
        public void CheckExpired(object lockingObject)
        {
            bool doUpdate = false;

            // Thread-safe checking function, releases lock so the potentially
            // costly resource update is done when unlocked and only by one thread.
            if (!this.updateInProgress && this.isExpiredMethod())
            {
                lock (lockingObject)
                {
                    if (!this.updateInProgress && this.isExpiredMethod())
                    {
                        this.updateInProgress = true;
                        doUpdate = true;
                    }
                }
            }

            if (doUpdate)
            {
                try
                {
                    this.handleExpiredMethod();
                }
                finally
                {
                    this.updateInProgress = false;
                }
            }
        }
    }
}
