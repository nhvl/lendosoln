﻿namespace LqbGrammar.Utils
{
    using System;
    using System.IO;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Utility for working with temporary files in the LendingQB system.
    /// </summary>
    public static class TempFileUtils
    {
        /// <summary>
        /// Folder where temporary files will be stored.
        /// </summary>
        public const string TempFolder = @"C:\LOTemp";

        /// <summary>
        /// Take a file name and return the path to that file within the temp file folder.
        /// </summary>
        /// <param name="tempFileName">The name of the temporary file.</param>
        /// <returns>The full path to the temporary file.</returns>
        public static LocalFilePath Name2Path(string tempFileName)
        {
            string path = Path.Combine(TempFolder, tempFileName);

            LocalFilePath? obj = LocalFilePath.Create(path);
            if (obj == null)
            {
                throw new DeveloperException(ErrorMessage.SystemError);
            }

            return obj.Value;
        }

        /// <summary>
        /// Generates a temporary file location.
        /// </summary>
        /// <returns>A temporary file location.</returns>
        public static LocalFilePath NewTempFilePath()
        {
            return Name2Path(Guid.NewGuid().ToString());
        }
    }
}
