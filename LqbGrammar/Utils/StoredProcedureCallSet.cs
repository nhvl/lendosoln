﻿namespace LqbGrammar.Utils
{
    using System.Collections.Generic;
    using System.Data.Common;
    using System.Text;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Utility class for gathering multiple stored procedures together for a single database call.
    /// </summary>
    public sealed class StoredProcedureCallSet
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="StoredProcedureCallSet"/> class.
        /// </summary>
        public StoredProcedureCallSet()
        {
            this.Calls = new List<StoredProcedureCallData>();
        }

        /// <summary>
        /// Gets the number of stored procedure calls that will be aggregated.
        /// </summary>
        public int CallCount
        {
            get { return this.Calls.Count; }
        }

        /// <summary>
        /// Gets or sets the list of stored procedure calls.
        /// </summary>
        private List<StoredProcedureCallData> Calls { get; set; }

        /// <summary>
        /// Add a stored procedure call to the set.
        /// </summary>
        /// <param name="name">The name of the stored procedure.</param>
        /// <param name="parameters">The SQL parameters passed into the stored procedure.</param>
        public void Add(StoredProcedureName name, IEnumerable<DbParameter> parameters)
        {
            this.Calls.Add(new StoredProcedureCallData(name, parameters));
        }

        /// <summary>
        /// Retrieve the result of wrapping up the added calls into a single ad hoc SQL query.
        /// </summary>
        /// <param name="query">The aggregate ad hoc SQL query.</param>
        /// <param name="allParameters">The union of the SQL parameters.</param>
        public void AggregateCalls(out SQLQueryString query, out IEnumerable<DbParameter> allParameters)
        {
            const string Prefix = "p";

            query = SQLQueryString.Invalid;
            allParameters = null;

            if (this.Calls.Count > 0)
            {
                int pramCount = 0;
                var globalCommand = new StringBuilder();
                var globalParameters = new List<DbParameter>();
                foreach (var call in this.Calls)
                {
                    var arguments = new StringBuilder();
                    foreach (var pram in call.Parameters)
                    {
                        string pramName = pram.ParameterName;
                        if (!pramName.StartsWith("@"))
                        {
                            // SqlParameter does this for us, but since we are creating the sql query as a string we have to do this ourselves
                            pramName = "@" + pramName;
                        }

                        string prefixedName = $"@{Prefix}_{pramCount++}";
                        pram.ParameterName = prefixedName;
                        arguments.Append(arguments.Length > 0 ? ", " : string.Empty);
                        arguments.Append($"{pramName} = {prefixedName}");
                        globalParameters.Add(pram);
                    }

                    globalCommand.Append($"EXEC {call.StoredProcedureName.ToString()}");
                    globalCommand.AppendLine(arguments.Length == 0 ? ";" : $" {arguments.ToString()};");
                }

                query = SQLQueryString.Create(globalCommand.ToString()).Value;
                allParameters = globalParameters;

                this.Calls.Clear();
            }
        }
    }
}
