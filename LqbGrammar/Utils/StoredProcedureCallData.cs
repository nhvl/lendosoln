﻿namespace LqbGrammar.Utils
{
    using System.Collections.Generic;
    using System.Data.Common;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// POD class to hold a stored procedure name and the sql parameters to be passed into the stored procedure.
    /// </summary>
    internal sealed class StoredProcedureCallData
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="StoredProcedureCallData"/> class.
        /// </summary>
        /// <param name="name">The name of the stored procedure.</param>
        /// <param name="parameters">The parameters to be passed into the stored procedure.</param>
        public StoredProcedureCallData(StoredProcedureName name, IEnumerable<DbParameter> parameters)
        {
            this.StoredProcedureName = name;
            this.Parameters = parameters ?? new List<DbParameter>();
        }

        /// <summary>
        /// Gets the name of the stored procedure.
        /// </summary>
        public StoredProcedureName StoredProcedureName { get; }

        /// <summary>
        /// Gets the parameters to be passed into the stored procedure.
        /// </summary>
        public IEnumerable<DbParameter> Parameters { get; }
    }
}
