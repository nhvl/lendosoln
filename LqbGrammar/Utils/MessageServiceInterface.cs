﻿namespace MSDNMessageService.Interface
{
    using System.Messaging;
    using System.Runtime.InteropServices;

    /// <summary>
    /// Return values from MSDNMessageService message handler methods.
    /// </summary>
    public enum ProcessMessageReturn
    {
        /// <summary>
        /// The message was processed successfully.
        /// </summary>
        ReturnGood,

        /// <summary>
        /// The message processing failed and the message will be placed into a designated queue.
        /// </summary>
        ReturnBad,

        /// <summary>
        /// The message processing was aborted and the message will be discarded.
        /// </summary>
        ReturnAbort
    }

    /// <summary>
    /// Interface supported by any class that can process messages.
    /// </summary>
    [ComVisible(true)]
    public interface IProcessMessage
    {
        /// <summary>
        /// Method on which the message is passed into the class for processing.
        /// </summary>
        /// <param name="inputMessage">The message pulled from a queue and will be processed.</param>
        /// <returns>Result of processing the message and instruction for how it will be disposed.</returns>
        ProcessMessageReturn Process(Message inputMessage);

        /// <summary>
        /// Method to called to release resources.
        /// </summary>
        void Release();
    }
}
