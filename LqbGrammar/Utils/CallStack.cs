﻿namespace LqbGrammar.Utils
{
    using System;
    using System.Diagnostics;
    using System.Security;
    using System.Text;

    /// <summary>
    /// Process a call stack to yield useful information.
    /// </summary>
    public sealed class CallStack
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CallStack"/> class.
        /// </summary>
        /// <param name="boundaryType">Type just below the method of interest, usually the type of a top-level logging utility.</param>
        public CallStack(Type boundaryType)
        {
            this.ProcessCallStack(boundaryType);
        }

        /// <summary>
        /// Gets the name of the class containing the method of interest.
        /// </summary>
        /// <value>The name of the class containing the method of interest.</value>
        public string ClassName { get; private set; }

        /// <summary>
        /// Gets the name of the file containing the method of interest.
        /// </summary>
        /// <value>The name of the file containing the method of interest.</value>
        public string FileName { get; private set; }

        /// <summary>
        /// Gets the name of the method of interest.
        /// </summary>
        /// <value>The name of the method of interest.</value>
        public string MethodName { get; private set; }

        /// <summary>
        /// Gets the line number within the method of interest where the call stack is getting processed.
        /// </summary>
        /// <value>The line number within the method of interest.</value>
        public int LineNumber { get; private set; }

        /// <summary>
        /// Gets the stack trace ending with the method of interest.
        /// </summary>
        /// <value>The stack trace ending with the method of interest.</value>
        public string StackTrace { get; private set; }

        /// <summary>
        /// Walk the call stack until the boundary type is found, then digest the rest.
        /// </summary>
        /// <param name="boundaryType">Type just below the method of interest, usually the type of a top-level logging utility.</param>
        private void ProcessCallStack(Type boundaryType)
        {
            try
            {
                var trace = new StackTrace(true);
                if (boundaryType == null)
                {
                    boundaryType = this.GetType(); // we don't want the CallStack constructor and this method (ProcessCallStack) to be included in the call stack
                }

                bool done = this.ProcessStackTrace(trace, boundaryType);
                if (!done)
                {
                    this.RecordFrameData(trace, 0);
                }
            }
            catch (SecurityException)
            {
                // do nothing, the call stack cannot be processed.
                return;
            }
        }

        /// <summary>
        /// Walk the stack until past the boundary type, then record the results.
        /// </summary>
        /// <param name="trace">The full stack trace.</param>
        /// <param name="boundaryType">Type just below the method of interest, usually the type of a top-level logging utility.</param>
        /// <returns>True if the stack was processed, false otherwise.</returns>
        private bool ProcessStackTrace(StackTrace trace, Type boundaryType)
        {
            // locate the boundaryType
            int frameIndex = this.LocateBoundaryStart(trace, boundaryType);
            if (frameIndex >= trace.FrameCount)
            {
                return false;
            }

            // advance to beyond the boundaryType
            frameIndex = this.LocateBoundaryEnd(frameIndex, trace, boundaryType);
            if (frameIndex >= trace.FrameCount)
            {
                return false;
            }

            this.RecordFrameData(trace, frameIndex);
            return true;
        }

        /// <summary>
        /// Walk the stack until the boundary type is found.
        /// </summary>
        /// <param name="trace">The full stack trace.</param>
        /// <param name="boundaryType">Type just below the method of interest, usually the type of a top-level logging utility.</param>
        /// <returns>The index in the stack where the boundary type is first detected.</returns>
        private int LocateBoundaryStart(StackTrace trace, Type boundaryType)
        {
            int frameIndex = 0;
            while (frameIndex < trace.FrameCount)
            {
                var frame = trace.GetFrame(frameIndex);
                if (frame.GetMethod().DeclaringType != boundaryType)
                {
                    frameIndex++;
                }
                else
                {
                    break;
                }
            }

            return frameIndex;
        }

        /// <summary>
        /// Walk the stack beginning at the start of boundary, continuing until the boundary has been past.
        /// </summary>
        /// <param name="boundaryStart">The frame index where the boundary type was first detected.</param>
        /// <param name="trace">The full stack trace.</param>
        /// <param name="boundaryType">Type just below the method of interest, usually the type of a top-level logging utility.</param>
        /// <returns>The index just beyond the boundary, which points to the method of interest.</returns>
        private int LocateBoundaryEnd(int boundaryStart, StackTrace trace, Type boundaryType)
        {
            int frameIndex = boundaryStart;
            while (frameIndex < trace.FrameCount)
            {
                var frame = trace.GetFrame(frameIndex);
                if (frame.GetMethod().DeclaringType == boundaryType)
                {
                    frameIndex++;
                }
                else
                {
                    break;
                }
            }

            return frameIndex;
        }

        /// <summary>
        /// Record the data in the stack frame beginning at the frame index.
        /// </summary>
        /// <param name="trace">The full stack trace.</param>
        /// <param name="frameIndex">The index that points to the method of interest.</param>
        private void RecordFrameData(StackTrace trace, int frameIndex)
        {
            var frame = trace.GetFrame(frameIndex);

            this.LineNumber = frame.GetFileLineNumber();
            this.FileName = frame.GetFileName() ?? string.Empty;

            var method = frame.GetMethod();
            if (method != null)
            {
                this.MethodName = method.Name;
                if (method.DeclaringType != null)
                {
                    this.ClassName = method.DeclaringType.FullName;
                }
            }

            var sb = new StringBuilder();
            while (frame != null)
            {
                sb.AppendLine(frame.ToString());
                if (++frameIndex < trace.FrameCount)
                {
                    frame = trace.GetFrame(frameIndex);
                }
                else
                {
                    frame = null;
                }
            }

            this.StackTrace = sb.ToString();
        }
    }
}
