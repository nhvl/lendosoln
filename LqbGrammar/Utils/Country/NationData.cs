﻿namespace LqbGrammar.Utils.Country
{
    using LqbGrammar.DataTypes;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Gather together various data items assocated with nations
    /// so they get bound together.
    /// </summary>
    /// <remarks>
    /// The information here is comes from the external world, typically
    /// defined by standards bodies.  None of this is business logic.  This
    /// class can be considered a utility for organizing string tables.
    /// </remarks>
    internal sealed class NationData
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NationData"/> class.
        /// </summary>
        /// <param name="country">The name of the country that is used by LQB.</param>
        /// <param name="iso2">The country code as defined by the ISO-3166-1 alpha-2 standard.</param>
        /// <param name="iso3">The country code as defined by the ISO-3166-1 alpha-3 standard.</param>
        public NationData(string country, string iso2, string iso3)
        {
            // LQB uses ISO-3166-1 alpha-3 for storage, so we cannot store values that are not defined within that standard.
            if (string.IsNullOrWhiteSpace(iso3))
            {
                throw new DeveloperException(ErrorMessage.SystemError);
            }

            this.Country = country;
            this.Iso_3166_2 = iso2;
            this.Iso_3166_3 = iso3;
        }

        /// <summary>
        /// Gets the name of the country that is used by LQB.
        /// </summary>
        public string Country { get; }

        /// <summary>
        /// Gets the value defined by the ISO-3166-1 alpha-2 standard.
        /// </summary>
        public string Iso_3166_2 { get; }

        /// <summary>
        /// Gets the value defined by the ISO-3166-1 alpha-3 standard.
        /// </summary>
        public string Iso_3166_3 { get; }
    }
}
