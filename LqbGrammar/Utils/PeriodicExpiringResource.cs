﻿namespace LqbGrammar.Utils
{
    using System;

    /// <summary>
    /// Expiring resource that expires on a regular schedule.
    /// </summary>
    public class PeriodicExpiringResource : ExpiringResource
    {
        /// <summary>
        /// The DateTime for when the resource expires.
        /// </summary>
        private HeapExpiredDateTime expireTime;

        /// <summary>
        /// The time during which the current resource is good.
        /// </summary>
        private int periodInMilliseconds;

        /// <summary>
        /// The method used to reload the expired resource.
        /// </summary>
        private HandleExpiredDelegate handleExpiredMethod2;

        /// <summary>
        /// Initializes a new instance of the <see cref="PeriodicExpiringResource"/> class.
        /// </summary>
        /// <param name="periodInMilliseconds">The period in which a resource is invalid prior to expiration.</param>
        /// <param name="handleExpiredMethod">The method to call for updating the resource.</param>
        public PeriodicExpiringResource(int periodInMilliseconds, HandleExpiredDelegate handleExpiredMethod)
        {
            this.periodInMilliseconds = periodInMilliseconds;
            this.expireTime = new HeapExpiredDateTime(DateTime.Now.AddMilliseconds(periodInMilliseconds));
            this.handleExpiredMethod2 = handleExpiredMethod;
            this.IsExpired = new IsExpiredDelegate(this.PeriodIsOver);
            this.HandleExpired = new HandleExpiredDelegate(this.HandleExpired2);
        }

        /// <summary>
        /// Method to determine whether or not the resource has expired.
        /// </summary>
        /// <returns>Ture if the resource is expired, false otherwise.</returns>
        private bool PeriodIsOver()
        {
            var heapValue = this.expireTime;
            return heapValue.IsPast();
        }

        /// <summary>
        /// Method that is called the the code in the ExpiringResouce class to update the resource.
        /// This method will defer the resource update to the handleExpiredMethod2 method, then
        /// reset the clock on the resource expiration.
        /// </summary>
        private void HandleExpired2()
        {
            this.handleExpiredMethod2();

            this.expireTime = new HeapExpiredDateTime(DateTime.Now.AddMilliseconds(this.periodInMilliseconds));
        }

        /// <summary>
        /// An expiration date time value that is stored on the heap.
        /// </summary>
        private class HeapExpiredDateTime
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="HeapExpiredDateTime"/> class.
            /// </summary>
            /// <param name="value">The date time value.</param>
            public HeapExpiredDateTime(DateTime value)
            {
                this.Value = value;
            }

            /// <summary>
            /// Gets or sets the date time value.
            /// </summary>
            private DateTime Value { get; set; }

            /// <summary>
            /// Determine whether or not the stored value is in the past.
            /// </summary>
            /// <returns>True if the stored value is in the past, false otherwise.</returns>
            public bool IsPast()
            {
                return DateTime.Now > this.Value;
            }
        }
    }
}
