﻿namespace LqbGrammar.Utils
{
    using System;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Utility methods for time calculations.
    /// </summary>
    public static class TimeUtils
    {
        /// <summary>
        /// Calculate the duration between two dates as a quantity of years.
        /// </summary>
        /// <remarks>
        /// This calculation can become very complicated in the face of leap years.  Unless this becomes an issue, this
        /// code is going to ignore that.  The calculation of the variable yearlen would probably depend on the relationship
        /// between the end date, the start date advanced by a number of years until it is less than a year prior to the end date,
        /// and the nearest leap day.
        /// </remarks>
        /// <param name="beginDate">The begin date from which the number of years is calculated.</param>
        /// <param name="endDate">The end date for the time period. If you pass null, it will default to the current date.</param>
        /// <returns>The calculated duration in units of years.</returns>
        public static Quantity<UnitType.Year>? CalculateYearDuration(UnzonedDate? beginDate, UnzonedDate? endDate)
        {
            var years = CalcWholeYears(beginDate, endDate);
            if (years != null)
            {
                var advancedDate = beginDate.Value.Date.AddYears(years.Value.Value);
                var finishDate = (endDate == null) ? DateTime.Today : endDate.Value.Date;
                if (advancedDate == finishDate)
                {
                    return Quantity<UnitType.Year>.Create(years.Value.Value);
                }

                var ts = finishDate - advancedDate;
                decimal days = ts.Days;

                var leapBoundaryS = new DateTime(advancedDate.Year, 3, 1);
                var leapBoundaryF = new DateTime(finishDate.Year, 3, 1);
                if (advancedDate <= leapBoundaryS || finishDate <= leapBoundaryF)
                {
                    int yearLen = DateTime.IsLeapYear(advancedDate.Year) ? 366 : 365;
                    return Quantity<UnitType.Year>.Create(years.Value.Value + (days / yearLen));
                }
                else
                {
                    int yearLen = DateTime.IsLeapYear(finishDate.Year) ? 366 : 365;
                    return Quantity<UnitType.Year>.Create(years.Value.Value + (days / yearLen));
                }
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Calculate the number of whole years that have elapsed between the begin date and end date.
        /// </summary>
        /// <param name="beginDate">The begin date from which the number of years is calculated.</param>
        /// <param name="endDate">The end date for the time period. If you pass null, it will default to the current date.</param>
        /// <returns>The calculated number of years.</returns>
        public static Count<UnitType.Year>? CalcWholeYears(UnzonedDate? beginDate, UnzonedDate? endDate)
        {
            var months = CalcWholeMonths(beginDate, endDate);
            if (months != null)
            {
                int years = months.Value.Value / 12;
                return Count<UnitType.Year>.Create(years);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Calculate the number of whole months that have elapsed between the begin date and end date.
        /// </summary>
        /// <param name="beginDate">The begin date from which the number of months is calculated.</param>
        /// <param name="endDate">The end date for the time period. If you pass null, it will default to the current date.</param>
        /// <returns>The calculated number of months.</returns>
        public static Count<UnitType.Month>? CalcWholeMonths(UnzonedDate? beginDate, UnzonedDate? endDate)
        {
            if (beginDate != null)
            {
                var start = beginDate.Value.Date;
                var end = endDate?.Date ?? DateTime.Today;
                if (end < start)
                {
                    return null;
                }

                if (start.Day == 1)
                {
                    return CalcWholeMonthsStartIsFirst(start, end);
                }
                else if (start.Day == DateTime.DaysInMonth(start.Year, start.Month))
                {
                    return CalcWholeMonthsStartIsLast(start, end);
                }
                else
                {
                    return CalcWholeMonthsStartIsMiddle(start, end);
                }
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Calculate the number of whole months that have elapsed between the start date and end date when the start data is the first of the month.
        /// </summary>
        /// <param name="start">The start date from which the number of months is calculated.</param>
        /// <param name="end">The end date for the time period.</param>
        /// <returns>The calculated number of months.</returns>
        private static Count<UnitType.Month>? CalcWholeMonthsStartIsFirst(DateTime start, DateTime end)
        {
            var adjust = new DateTime(end.Year, start.Month, start.Day);

            if (adjust == end)
            {
                var count = 12 * (end.Year - start.Year);
                return Count<UnitType.Month>.Create(count);
            }
            else if (adjust < end)
            {
                var count = (12 * (end.Year - start.Year)) + end.Month - start.Month;
                return Count<UnitType.Month>.Create(count);
            }
            else
            {
                var count = (12 * (end.Year - start.Year - 1)) + 12 + end.Month - start.Month;
                return Count<UnitType.Month>.Create(count);
            }
        }

        /// <summary>
        /// Calculate the number of whole months that have elapsed between the start date and end date when the start data is the last of the month.
        /// </summary>
        /// <param name="start">The start date from which the number of months is calculated.</param>
        /// <param name="end">The end date for the time period.</param>
        /// <returns>The calculated number of months.</returns>
        private static Count<UnitType.Month>? CalcWholeMonthsStartIsLast(DateTime start, DateTime end)
        {
            // A leap day wreaks havoc on the adjust calculation below, so special handling done here...
            if (start.Month == 2 && start.Day == 29)
            {
                if (end.Month == 2 && end.Day == DateTime.DaysInMonth(end.Year, end.Month))
                {
                    var count = 12 * (end.Year - start.Year);
                    return Count<UnitType.Month>.Create(count);
                }
                else if (end.Month == 2 && end.Day == 28)
                {
                    // The end date must precede a leap day since the 28th isn't the last day of February
                    var count = (12 * (end.Year - start.Year)) - 1;
                    return Count<UnitType.Month>.Create(count);
                }
                else
                {
                    // Adjust the start date to a value that enables the adjust calculation below
                    start = start.AddDays(-1);
                }
            }

            var adjust = new DateTime(end.Year, start.Month, start.Day);

            if (adjust == end)
            {
                var count = 12 * (end.Year - start.Year);
                return Count<UnitType.Month>.Create(count);
            }
            else if (adjust < end)
            {
                var count = (12 * (end.Year - start.Year)) + end.Month - start.Month - 1;
                if (end.Day == DateTime.DaysInMonth(end.Year, end.Month))
                {
                    count++;
                }

                return Count<UnitType.Month>.Create(count);
            }
            else
            {
                var count = (12 * (end.Year - start.Year - 1)) + 12 + end.Month - start.Month - 1;
                if (end.Day == DateTime.DaysInMonth(end.Year, end.Month))
                {
                    count++;
                }

                return Count<UnitType.Month>.Create(count);
            }
        }

        /// <summary>
        /// Calculate the number of whole months that have elapsed between the start date and end date when the start data is in the middle of the month.
        /// </summary>
        /// <param name="start">The start date from which the number of months is calculated.</param>
        /// <param name="end">The end date for the time period.</param>
        /// <returns>The calculated number of months.</returns>
        private static Count<UnitType.Month>? CalcWholeMonthsStartIsMiddle(DateTime start, DateTime end)
        {
            var adjust = new DateTime(end.Year, start.Month, start.Day);

            if (adjust == end)
            {
                var count = 12 * (end.Year - start.Year);
                return Count<UnitType.Month>.Create(count);
            }
            else if (adjust < end)
            {
                var count = (12 * (end.Year - start.Year)) + end.Month - start.Month - 1;
                if (start.Day <= end.Day)
                {
                    count++;
                }

                return Count<UnitType.Month>.Create(count);
            }
            else
            {
                var count = (12 * (end.Year - start.Year - 1)) + 12 + end.Month - start.Month - 1;
                if (start.Day <= end.Day)
                {
                    count++;
                }

                return Count<UnitType.Month>.Create(count);
            }
        }
    }
}
