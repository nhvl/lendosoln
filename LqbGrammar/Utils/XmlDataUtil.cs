﻿namespace LqbGrammar.Utils
{
    using System.Text;

    /// <summary>
    /// Utility class for manipulating data that can appear within XML documents.
    /// </summary>
    public static class XmlDataUtil
    {
        /// <summary>
        /// Ensure that a string that should be valid XML doesn't contain any illegal characters.
        /// </summary>
        /// <param name="xml">The input xml string.</param>
        /// <returns>The xml string with illegal characters removed.</returns>
        public static string SantizeXmlString(string xml)
        {
            if (string.IsNullOrEmpty(xml))
            {
                return string.Empty;
            }

            StringBuilder sb = new StringBuilder(xml.Length);
            foreach (char ch in xml)
            {
                if (IsLegalXmlChar(ch))
                {
                    sb.Append(ch);
                }
            }

            return sb.ToString();
        }

        /// <summary>
        /// Test the input character to determinew whether or not is legal for an xml document.
        /// </summary>
        /// <param name="ch">The input character that is being tested.</param>
        /// <returns>True if the input character is legal for xml, false otherwise.</returns>
        public static bool IsLegalXmlChar(int ch)
        {
            // The list of allowable characters http://www.w3.org/TR/REC-xml/#charsets
            return ch == 0x9 || ch == 0xA || ch == 0xD || (ch >= 0x20 && ch <= 0xD7FF) || (ch >= 0xE000 && ch <= 0xFFFD) || (ch >= 0x10000 && ch <= 0x10FFFF);
        }
    }
}
