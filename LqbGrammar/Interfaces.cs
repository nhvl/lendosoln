﻿namespace LqbGrammar
{
    /// <summary>
    /// Marker interface for POD classes that encapsulate a
    /// set of user input data.  Note that the data members
    /// should be semantic data types.
    /// </summary>
    public interface ILqbInputData
    {
    }

    /// <summary>
    /// Marker interface for interfaces that involve
    /// issuing commands to the business logic that
    /// result in changes to the system.
    /// </summary>
    public interface ILqbCommand
    {
    }

    /// <summary>
    /// Marker interface for interfaces that involve
    /// requesting data from the business logic.  No
    /// data changes will result, these are simply
    /// queries.
    /// </summary>
    public interface ILqbQuery
    {
    }

    /// <summary>
    /// Marker interface for interfaces that involve
    /// business logic objects that drive interactions
    /// with non-LQB code.  Note that the inheriting
    /// interfaces will be defined in LqbGrammar but
    /// the implementations will be held in the modules
    /// that contain the business logic.
    /// </summary>
    public interface ILqbDriver
    {
    }

    /// <summary>
    /// Marker interface for interfaces that involve
    /// thin adapters around non-LQB code.  Note that
    /// the inheriting interfaces will be defined in
    /// LqbGrammar but the impelmentations will
    /// be held in the Adapter module.
    /// </summary>
    public interface ILqbAdapter
    {
    }

    /// <summary>
    /// Marker interface for interfaces that 
    /// are factories for creating adapters 
    /// and drivers.  Note that the inheriting
    /// interfaces will be defined in LqbGrammar
    /// but the implemenations will be held in 
    /// other modules.
    /// </summary>
    public interface ILqbFactory
    {
    }
}
