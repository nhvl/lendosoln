﻿namespace LqbGrammar.Validation
{
    using System.Collections.Generic;

    /// <summary>
    /// Regular expression string values are centralized here.  When modifying
    /// this class, make sure the changes are echoed on the DataTypes.RegularExpressionString class.
    /// </summary>
    internal static class RegExStrings
    {
        /// <summary>
        /// Regular expression string for a comment category name.
        /// NOTE: This is just a guess...waiting for confirmation from design.
        /// </summary>
        public const string CommentCategoryName = "[-_./a-zA-Z0-9,;!@#$%^&*():]|[-_./a-zA-Z0-9,;!@#$%^&*():][-_./a-zA-Z0-9,;!@#$%^&*(): ]{0,23}[-_./a-zA-Z0-9,;!@#$%^&*():]";

        /// <summary>
        /// Regular expression string for a conversion log permission level name.
        /// NOTE: This is just a guess based on CommentCategoryName.
        /// </summary>
        public const string ConversationLogPermissionLevelName = "[-_./a-zA-Z0-9,;!@#$%^&*():]|[-_./a-zA-Z0-9,;!@#$%^&*():][-_./a-zA-Z0-9,;!@#$%^&*(): ]{0,38}[-_./a-zA-Z0-9,;!@#$%^&*():]";

        /// <summary>
        /// Regular expression string for a conversion log permission level description.
        /// NOTE: This is just a guess based on ConversationLogPermissionLevelName.
        /// </summary>
        public const string ConversationLogPermissionLevelDescription = "[-_./a-zA-Z0-9,;!@#$%^&*():]|[-_./a-zA-Z0-9,;!@#$%^&*():][-_./a-zA-Z0-9,;!@#$%^&*(): ]{0,998}[-_./a-zA-Z0-9,;!@#$%^&*():]";

        /// <summary>
        /// Regular expression for a name that determines the name of a configuration item.
        /// </summary>
        public const string ConfigurationItemName = @"[_a-zA-Z0-9:\.]+";

        /// <summary>
        /// Regular expression for validating a database column name when it is input to a method.
        /// </summary>
        public const string DBColumnName = "[a-zA-Z0-9_]+";

        /// <summary>
        /// Regular expression string for disallowed characters in a person's partial name (first, middle, or last).
        /// </summary>
        public const string DisallowedNamePartialCharacters = @"[^-a-zA-Z' ,.]";

        /// <summary>
        /// Regular expression for validating a database table name when it is input to a method.
        /// </summary>
        public const string DBTableName = @"([a-zA-Z0-9_]+\.)?[a-zA-Z0-9_]+"; // schema_name.table_name...may need more work on the schema name if it can appear in square brackets

        /// <summary>
        /// Regular expression string for a package name in the document integration.
        /// </summary>
        public const string DocumentIntegrationPackageName = @".+";

        /// <summary>
        /// Regular expression string for an email address.
        /// Note: copied from LendersOffice.Constants.ConstApp.EmailValidationExpression.
        /// </summary>
        public const string EmailAddress = @"[A-Za-z0-9&._%+-]+@[A-Za-z0-9&.-]+\.[A-Za-z]{2,}";

        /// <summary>
        /// Regular expression for an Outlook-style email address, which combines the display name with the email address.
        /// </summary>
        public const string EmailAddressOutlookStyle = "\"" + PersonName + "\" *<" + EmailAddress + ">";

        /// <summary>
        /// Regular expression string for the body text of an email.  Since we don't want to restrict the user's content, we are going to permit everything for now.
        /// </summary>
        public const string EmailBody = @".*";

        /// <summary>
        /// Regular expression string for the MeridianLink email server.
        /// </summary>
        public const string EmailServer = @"smtp\.meridianlink\.com|icy\.assistdirect\.com|mailer1\.mortgagecreditlink\.com"; // if we use a different value we can craft a more generic regular expression

        /// <summary>
        /// Regular expression string for a Pop3 UID per RFC 1939.
        /// </summary>
        public const string Pop3Uid = "[0-9]{0,70}"; // Per David Dao, actual values are sequential integers.  It is an optional feature so zero should be included as well.

        /// <summary>
        /// Regular expression string to check the string representation of binary data from the database.
        /// </summary>
        public const string DbBinaryDataInStringForm = "NULL|0x[0-9a-fA-F]*";

        /// <summary>
        /// Regular expression string for the subject line of an email.
        /// </summary>
        public const string EmailSubject = @"[-a-zA-Z0-9_=|\(\)\[\]\.,!$/:;>'\s]+";

        /// <summary>
        /// Regular expression for a feature name used in defining feature flags.
        /// The definition is broad enough to include guids should that choice be made.
        /// </summary>
        public const string FeatureName = @"[-_a-zA-Z0-9]+";

        /// <summary>
        /// Regular expression string for a key used to access a particular file in the FileDB system.
        /// </summary>
        public const string FileDBKey = @"[_a-zA-Z0-9][-._a-zA-Z0-9]+";

        /// <summary>
        /// Regular expression string for the name of a repository in the FileDB system.
        /// </summary>
        public const string FileDBRepositoryName = @"[a-zA-Z][-_a-zA-Z]+";

        /// <summary>
        /// Regular expression string for a valid loan name under the HMDA 2018 rules for legal entity identifiers.
        /// </summary>
        public const string HmdaLegalIdentityIdentifierValidLoanName = @"[a-zA-Z0-9]{1,23}";

        /// <summary>
        /// Regular expression string for valid legal entity identifiers under the HMDA 2018 rules.
        /// </summary>
        public const string LegalEntityIdentifier = @"[a-zA-Z0-9]{20}";

        /// <summary>
        /// Regular expression string for a valid log message.  We may tighten the constraints later.
        /// </summary>
        public const string LogMessage = @".*";

        /// <summary>
        /// Regular expression string for a valid property name for the logging system.
        /// </summary>
        public const string LogPropertyName = @"[a-zA-Z0-9_]{1,50}";

        /// <summary>
        /// Regular expression string for a valid property value for the logging system.  We may tighten the constraints later.
        /// </summary>
        public const string LogPropertyValue = @".*";

        /// <summary>
        /// Regular expression string for an application name.
        /// </summary>
        public const string LqbApplicationName = "[a-zA-Z]+";

        /// <summary>
        /// Regular expression for the name assigned to a computer participating in LQB applications.
        /// </summary>
        public const string MachineName = "[a-zA-Z][-a-zA-Z0-9.]*";

        /// <summary>
        /// Regular expression string for the label of a message sent to a message queue.
        /// </summary>
        public const string MessageLabel = @"[a-zA-Z][-_./a-zA-Z0-9]+";

        /// <summary>
        /// Regular expression string for a path that defines the location of a message queue.
        /// </summary>
        public const string MessageQueuePath = @"(FormatName|FORMATNAME):DIRECT=(OS|TCP):[a-zA-Z0-9.]+\\private\$\\[a-zA-Z][a-zA-Z0-9_]+";

        /// <summary>
        /// Validates the MIME multipart content-type's boundary parameter requirements.
        /// </summary>
        public const string MimeMultipartBoundary = @"[a-zA-Z0-9'()+_,-./:=? ]{0,69}[a-zA-Z0-9'()+_,-./:=?]";

        /// <summary>
        /// Regular expression string for a MIME Content-Type header field token such as media type, subtype, parameter name, or unquoted parameter value.
        /// </summary>
        /// <remarks>
        /// See https://tools.ietf.org/html/rfc2045#section-5.1 for the RFC specification of the MIME Content-Type header field. 
        /// Per the RFC spec, the token may contain 1 or more characters in the set:
        ///     "any (US-ASCII) CHAR except SPACE, CTLs, or <![CDATA["(" / ")" / "<" / ">" / "@" / "," / ";" / ":" / "\" / <"> "/" / "[" / "]" / "?" / "="]]>"
        /// <para />
        /// The RFC 2045 spec is updated by RFC 2231 (https://tools.ietf.org/html/rfc2231), which allows additional information encoding within the content-type parameter attribute value.
        /// Until we wish to use the additional information, we won't need to support the newer specification.
        /// </remarks>
        public const string MimeTypeToken = @"[!#$%&'*+-.^_`|~0-9A-Za-z]+";

        /// <summary>
        /// Regular expression string for a person's name.
        /// </summary>
        public const string PersonName = @"[a-zA-Z][-a-zA-Z .,']*[a-zA-Z]";

        /// <summary>
        /// Regular expression string for a valid phone number.
        /// </summary>
        /// <remarks>
        /// Breakdown for the regex:
        ///   [-. (]?             #Allow certain non numeric characters that may appear before the Area Code.
        ///   (\d{3})             #GROUP 1: The Area Code. Required.
        ///   [-. )]?             #Allow certain non numeric characters that may appear between the Area Code and the Exchange number.
        ///   [ ]?                #Allow a space after the area code
        ///   (\d{3})             #GROUP 2: The Exchange number. Required.
        ///   [-. ]?              #Allow certain non numeric characters that may appear between the Exchange number and the Subscriber number.
        ///   (\d{4,})            #Group 3: The Subscriber Number. Required.
        /// </remarks>
        public const string PhoneNumber = @"[-. (]?(\d{3})[-. )]?[ ]?(\d{3})[-. ]?(\d{4,})";

        /// <summary>
        /// Regular expression string for a valid employer identification number.
        /// Note: This is nearly duplicated in a JavaScript RegExp in PmlCompanyList.ascx.
        /// Consider updating that reference if this needs to be updated.
        /// </summary>
        public const string EmployerTaxIdentificationNumber = @"(\d{2})-?(\d{7})";

        /// <summary>
        /// Regular expression string for validate the SHA 256 checksum.
        /// </summary>
        public const string SHA256Checksum = @"[a-f0-9]{64}";

        /// <summary>
        /// Regular expression for a string that can be recorded as the context of an error message.
        /// </summary>
        public const string SimpleContext = ".+";

        /// <summary>
        /// Regular expression string for a "simple" XPath of just element names.
        /// </summary>
        /// <remarks>
        /// <c>[A-Za-z_]</c> is a conservative take on the <c>NameStartChar</c> from https://www.w3.org/TR/REC-xml/#NT-Name, and adding a digit character is a likewise conservative
        /// <c>NameChar</c>.  We'll add the funky characters as the need arises, but that may force us to build the expression up, since this is already a little dizzying.
        /// </remarks>
        public const string SimpleXPath = @"/?(?:[A-Za-z_][A-Za-z_0-9]*/)*[A-Za-z_][A-Za-z_0-9]*";

        /// <summary>
        /// Regular expression for a message sent via SMS to give the authentication code to a user.
        /// </summary>
        public const string SmsAuthCodeMessage = @"[- a-zA-Z0-9!#&'(),;./?_`~]{0,100} Authentication Code: [1-9]{4}";

        /// <summary>
        /// Regular expression string for a query string for SQL Server.
        /// For now we'll accept anything, but we can refine later.
        /// </summary>
        public const string SQLQuery = ".+";

        /// <summary>
        /// Regular expression string for text that is written in standard English.
        /// </summary>
        public const string StandardEnglish = @"[a-zA-Z][-a-zA-Z0-9.,;:'""!$%&*()?/\s]*";

        /// <summary>
        /// Regular expression string used to validate the name of a stored procedure.
        /// </summary>
        public const string StoredProcedureName = @"[_a-zA-Z0-9]+";

        /// <summary>
        /// Regular expression string for a valid zipcode.
        /// </summary>
        public const string Zipcode = @"\d{5}(-\d{4})?";

        /// <summary>
        /// Regular expression string for a valid company id.
        /// </summary>
        public const string RolodexCompanyID = @"^[A-Za-z\d-]{0,25}$";

        /// <summary>
        /// Regular expression string for a SASS variable value.
        /// </summary>
        /// <remarks>
        /// Obtained from http://stackoverflow.com/a/1636354 .
        /// </remarks>
        public const string SassVariableValue = "#(?:[0-9a-fA-F]{3}){1,2}";

        /// <summary>
        /// Regular expression string for a Social Security Number.
        /// </summary>
        public const string SocialSecurityNumber = @"(\d{9})|(\d{3}-\d{2}-\d{4})";

        /// <summary>
        /// Regular expression for Word files with the DocX extension.
        /// </summary>
        public const string DocXFile = @"^(.*?).((d|D)(o|O)(c|C)(x|X))";

        /// <summary>
        /// Collection of regular expression strings that should allow multiline values.
        /// </summary>
        private static readonly HashSet<string> MultilineValues;

        /// <summary>
        /// Initializes static members of the <see cref="RegExStrings" /> class.
        /// </summary>
        static RegExStrings()
        {
            MultilineValues = new HashSet<string>();
            MultilineValues.Add(StandardEnglish);
            MultilineValues.Add(SQLQuery);
            MultilineValues.Add(EmailBody);
            MultilineValues.Add(LogMessage);
        }

        /// <summary>
        /// Determine whether the regular expression string should permit multiline values.
        /// </summary>
        /// <param name="regexString">The regular expression string under examination.</param>
        /// <returns>True if the regular expression string should permit multiline values, false otherwise.</returns>
        internal static bool AllowMultiLine(string regexString)
        {
            return MultilineValues.Contains(regexString);
        }
    }
}
