﻿namespace LqbGrammar.Validation
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Utility for generating a RegularExpressionResult, it is our
    /// chief means of hiding the .net framework from the rest of the
    /// symantic data types validation code.  For business logic in
    /// other modules, regular expressions will be used via a Driver/Adapter
    /// pair.
    /// </summary>
    internal static class RegularExpressionEngine
    {
        /// <summary>
        /// Pre-compiled regular expressions are cached here.  This is supposed to
        /// happen within the .net framework, but that caching mechanism leaks
        /// memory so we will rely on our own cache.
        /// </summary>
        private static Dictionary<string, System.Text.RegularExpressions.Regex> dict;

        /// <summary>
        /// Initializes static members of the <see cref="RegularExpressionEngine" /> class.
        /// </summary>
        static RegularExpressionEngine()
        {
            dict = new Dictionary<string, System.Text.RegularExpressions.Regex>();

            // Use reflection to register the Regular expressions
            // to simplify adding new expressions to RegExStrings.
            Type type = typeof(RegExStrings);
            foreach (FieldInfo field in type.GetFields())
            {
                if (field.IsLiteral)
                {
                    // for now just gather the valid regular expressions and ignore those that don't compile
                    // NOTE: For our pre-defined regular expression strings, we don't want to include the
                    //       boundary operators since ASP.NET validators add them implicitely.  Instead we'll
                    //       add them here.  We will not do this for regular expressions that are added to
                    //       the cache via calls to Execute().  It may be that the intent is to leave out the
                    //       boundary conditions so we don't want to enforce that there.
                    string regEx = (string)field.GetRawConstantValue();
                    bool allowMultiline = RegExStrings.AllowMultiLine(regEx);
                    string fullRegEx = string.Format("^(?:{0})$", regEx);
                    System.Text.RegularExpressions.Regex regex = CreateImpl(fullRegEx, allowMultiline);
                    if (regex != null)
                    {
                        dict[regEx] = regex;
                    }
                }
            }
        }

        /// <summary>
        /// Validate a string with the assigned regular expression.
        /// </summary>
        /// <param name="validationTarget">String to be validated.</param>
        /// <param name="regex">Regular expression used to validate the string.</param>
        /// <returns>The validation result.</returns>
        public static RegularExpressionResult Execute(string validationTarget, LqbGrammar.DataTypes.RegularExpressionString regex)
        {
            if (regex == LqbGrammar.DataTypes.RegularExpressionString.Invalid)
            {
                throw new DeveloperException(ErrorMessage.SystemError);
            }

            string expression = regex.ToString();
            System.Text.RegularExpressions.Regex localRegex = null;

            var dict = RegularExpressionEngine.dict; // grab current dictionary so operations only touch the existing object
            if (dict.ContainsKey(expression))
            {
                localRegex = dict[expression];
            }
            else
            {
                // want to cache the new expression, but do so in a thread-safe manner,
                // so copy the existing object and add it to the copy,
                // then replace the shared object with this new one - it is expected that
                // these sorts of operations are infrequent and transient so if we miss
                // an occasional instance due to simultaneous replacements the cost will
                // be small and the missed expression will get picked up the next time
                localRegex = CreateImpl(expression, false);
                if (localRegex != null)
                {
                    var newDict = new Dictionary<string, System.Text.RegularExpressions.Regex>(dict);
                    newDict[expression] = localRegex;
                    RegularExpressionEngine.dict = newDict;
                }
                else
                {
                    throw new DeveloperException(ErrorMessage.SystemError);
                }
            }

            return new RegularExpressionResult(validationTarget, localRegex);
        }

        /// <summary>
        /// Takes in a regular expression string and validates it to confirm that it conforms to
        /// the syntax for regular expressions.  Then returns the validated regular expression.
        /// </summary>
        /// <param name="expression">Regular expression string.</param>
        /// <param name="allowMultiline">True if the regular expression should support multiline values, false otherwise.</param>
        /// <returns>Pre-compiled regular expression if the input string is valid, null otherwise.</returns>
        private static System.Text.RegularExpressions.Regex CreateImpl(string expression, bool allowMultiline)
        {
            TimeSpan timeout = TimeSpan.FromMilliseconds(1000);

            try
            {
                var options = System.Text.RegularExpressions.RegexOptions.Compiled;
                if (allowMultiline)
                {
                    options |= System.Text.RegularExpressions.RegexOptions.Multiline;
                }

                return new System.Text.RegularExpressions.Regex(expression, options, timeout);
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
