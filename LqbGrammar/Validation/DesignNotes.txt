﻿The Validation folder will contain classes that are useful for validating data.

The primary means for validation is regular expressions.  These provide a convenient
language for declaring lexical constraints on strings.  Since all data arrives into
a web application as strings, the set of regular expressions is usually sufficient
to give full validation support.

The .net framework library contains support for regular expressions and we would like
to reuse this implementation.  Normally, .net framework classes would be wrapped by
an implementation of the ILqbAdapter interface.  However, such implementation classes
would live in the Adapter library so we cannot use them within the LqbGrammar library
without introducing circular references.  Since we need regular expressions to
implement the validation logic within our semantic data types, we are forced to use
the .net framework classes as-is here.  However, we can limit the exposure by using
them internally as implementation details of the semantic data types.  The regular
expression strings can be public since they are not bound to the .net framework and
they will be required in the GUI for client-side validation.

Note: for regular expressions we will want to isolate the usage of the framework classes
      as much as possible, in keeping with the overall wrapper concept.
