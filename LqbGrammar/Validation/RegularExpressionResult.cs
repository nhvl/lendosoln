﻿namespace LqbGrammar.Validation
{
    using System.Text.RegularExpressions;

    /// <summary>
    /// Utility class to wrap the needed functionality of a regular expression.
    /// </summary>
    internal class RegularExpressionResult
    {
        /// <summary>
        /// The string that was passed into the regular expression validation engine.
        /// </summary>
        private string stringToValidate;

        /// <summary>
        /// The local cache of the result of executing the regular expression.
        /// </summary>
        private Match matchCache;

        /// <summary>
        /// The regular expression that was passed into the regular expression validation engine.
        /// </summary>
        private Regex regex;

        /// <summary>
        /// Initializes a new instance of the <see cref="RegularExpressionResult"/> class.
        /// </summary>
        /// <param name="validationTarget">String that was passed into the regular expression validation engine.</param>
        /// <param name="regex">Regular expression that was passed into the regular expression validation engine.</param>
        public RegularExpressionResult(string validationTarget, Regex regex)
        {
            this.stringToValidate = validationTarget;
            this.regex = regex;
        }

        /// <summary>
        /// Gets the value of the result of the regex evaluation.
        /// </summary>
        /// <value>The value of the reult of the regex evaluation.</value>
        private Match Match
        {
            get
            {
                if (this.matchCache == null)
                {
                    this.matchCache = this.regex.Match(this.stringToValidate);
                }

                return this.matchCache;
            }
        }

        /// <summary>
        /// Determine whether or not the string was valid.
        /// </summary>
        /// <returns>True if the string was valid, false otherwise.</returns>
        public bool IsMatch()
        {
            return this.Match.Success;
        }

        /// <summary>
        /// Gets the group number by index.
        /// </summary>
        /// <param name="index">The index of the group.</param>
        /// <returns>The value of the given group.</returns>
        public string GetGroup(int index)
        {
            return this.Match.Groups[index].Value;
        }

        /// <summary>
        /// Gets a group by name.
        /// </summary>
        /// <param name="name">The name of the group.</param>
        /// <returns>The value of the given group.</returns>
        public string GetGroup(string name)
        {
            return this.Match.Groups[name].Value;
        }
    }
}
