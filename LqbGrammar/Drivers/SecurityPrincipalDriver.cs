﻿namespace LqbGrammar.Drivers
{
    /// <summary>
    /// Interface for working with the security principal.  There is no matching adapter as this
    /// is an interface to the LQB security model that can be accessed by adapters that need it.
    /// Note that this interface is working directly with the .net string type rather than a semantic data type.
    /// The reason - this is a very specialized interface to transmit LQB principals across
    /// application boundaries.  Typically other adapters are involved in the cross-application
    /// communication and the serialized principal would appear as member data within structures
    /// specific to the particular serialization needs of those adapters.
    /// </summary>
    public interface ISecurityPrincipalDriver : ILqbDriver
    {
        /// <summary>
        /// Retrieve a serialization of the current thread's registered principal.
        /// </summary>
        /// <returns>The serialization of the principal.</returns>
        string SerializePrincipal();

        /// <summary>
        /// Recover a principal and register it as the current thread's principal.
        /// </summary>
        /// <param name="serialized">Serialization of a principal.</param>
        void RecoverPrincipal(string serialized);
    }

    /// <summary>
    /// Interface for creating implemenations of the ISecurityPrincipalDriver interface.
    /// </summary>
    public interface ISecurityPrincipalDriverFactory : ILqbFactory
    {
        /// <summary>
        /// Create an implementation of the ISecurityPrincipalDriver interface.
        /// </summary>
        /// <returns>An implementation of the ISecurityPrincipalDriver interface.</returns>
        ISecurityPrincipalDriver Create();
    }
}
