﻿namespace LqbGrammar.Drivers
{
    /// <summary>
    /// Factory for creating implementations of open XML document drivers.
    /// </summary>
    public interface IOpenXmlDocumentDriverFactory : ILqbFactory
    {
        /// <summary>
        /// Create and return an implementation of the open XML document driver.
        /// </summary>
        /// <returns>
        /// Implementation of the open XML document driver.
        /// </returns>
        IOpenXmlDocumentDriver Create();
    }
}
