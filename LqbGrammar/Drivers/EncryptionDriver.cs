﻿namespace LqbGrammar.Drivers
{
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Interface for handling encryption.  The methods are overly-elaborate,
    /// but this is done to ensure that the conversion between byte[] and
    /// strings is done consistently and remove that burden from the client.
    /// </summary>
    public interface IEncryptionDriver : ILqbDriver
    {
        /// <summary>
        /// Low-level encryption.
        /// </summary>
        /// <param name="keyId">The identifier for the key to use.</param>
        /// <param name="inputData">Data to encrypt.</param>
        /// <returns>The encrypted data.</returns>
        byte[] EncryptBytes(EncryptionKeyIdentifier keyId, byte[] inputData);

        /// <summary>
        /// Mid-level encryption.
        /// </summary>
        /// <param name="keyId">The identifier for the key to use.</param>
        /// <param name="inputData">String to encrypt.</param>
        /// <returns>The encrypted string.</returns>
        byte[] EncryptString(EncryptionKeyIdentifier keyId, string inputData);

        /// <summary>
        /// High-level encryption.
        /// </summary>
        /// <param name="keyId">The identifier for the key to use.</param>
        /// <param name="inputData">String to encrypt.</param>
        /// <returns>The encrypted string base64 encoded.</returns>
        string EncryptAndEncode(EncryptionKeyIdentifier keyId, string inputData);

        /// <summary>
        /// Low-level decryption.
        /// </summary>
        /// <param name="keyId">The identifier for the key to use.</param>
        /// <param name="encrypted">Data to decrypt.</param>
        /// <returns>The decrypted data.</returns>
        byte[] DecryptBytes(EncryptionKeyIdentifier keyId, byte[] encrypted);

        /// <summary>
        /// Mid-level decryption.
        /// </summary>
        /// <param name="keyId">The identifier for the key to use.</param>
        /// <param name="encrypted">Data to decrypt.</param>
        /// <returns>The decrypted string.</returns>
        string DecryptString(EncryptionKeyIdentifier keyId, byte[] encrypted);

        /// <summary>
        /// High-level decryption.
        /// </summary>
        /// <param name="keyId">The identifier for the key to use.</param>
        /// <param name="encrypted">Data to decrypt.</param>
        /// <returns>The decrypted string.</returns>
        string DecodeAndDecrypt(EncryptionKeyIdentifier keyId, string encrypted);
    }

    /// <summary>
    /// Factory interface used to create implementations of the IEncryptionDriver interface.
    /// </summary>
    public interface IEncryptionDriverFactory : ILqbFactory
    {
        /// <summary>
        /// Create an implementation of the IEncryptionDriver interface.
        /// </summary>
        /// <returns>An implementation of the IEncryptionDriver interface.</returns>
        IEncryptionDriver Create();
    }
}
