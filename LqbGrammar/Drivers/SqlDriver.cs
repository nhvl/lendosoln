﻿namespace LqbGrammar.Drivers
{
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using DataTypes;

    /// <summary>
    /// LQB uses different logging options and may add more in the future.
    /// This enumeration allows expansion of the options while keeping the
    /// interface methods simple.
    /// </summary>
    public enum SqlDecoratorType
    {
        /// <summary>
        /// The driver factory will return the same implementation as the simple Create() method.
        /// </summary>
        Default,

        /// <summary>
        /// Return the driver that is a simple passthrough to the adapter.
        /// </summary>
        None,

        /// <summary>
        /// Return the driver that logs the calls and errors.
        /// </summary>
        Logging,

        /// <summary>
        /// Return the driver that logs timing statistics.
        /// </summary>
        StopWatch
    }

    /// <summary>
    /// Defines the CRUD interface to SQL Server.
    /// </summary>
    public interface ISqlDriver : ILqbDriver
    {
        /// <summary>
        /// Insert a record into a table but do not retrieve the primary key.
        /// This is usually used for tables where the primary key is a set
        /// of columns rather than a generated key.
        /// </summary>
        /// <param name="connection">The connection to the DB.</param>
        /// <param name="transaction">If using a transaction, pass it in here, else pass in null.</param>
        /// <param name="query">The query string.</param>
        /// <param name="parameters">The query parameters.</param>
        void InsertNoKey(DbConnection connection, DbTransaction transaction, SQLQueryString query, IEnumerable<DbParameter> parameters);

        /// <summary>
        /// Insert a record into a table that has a uniqueidentifier-valued primary key.
        /// </summary>
        /// <param name="connection">The connection to the DB.</param>
        /// <param name="transaction">If using a transaction, pass it in here, else pass in null.</param>
        /// <param name="query">The query string.</param>
        /// <param name="parameters">The query parameters.</param>
        /// <returns>The primary key of the inserted record.</returns>
        PrimaryKeyGuid InsertGetGuid(DbConnection connection, DbTransaction transaction, SQLQueryString query, IEnumerable<DbParameter> parameters);

        /// <summary>
        /// Insert a record into a table that has an integer-valued primary key.
        /// </summary>
        /// <param name="connection">The connection to the DB.</param>
        /// <param name="transaction">If using a transaction, pass it in here, else pass in null.</param>
        /// <param name="query">The query string.</param>
        /// <param name="parameters">The query parameters.</param>
        /// <returns>The primary key of the inserted record.</returns>
        PrimaryKeyInt32 InsertGetInt32(DbConnection connection, DbTransaction transaction, SQLQueryString query, IEnumerable<DbParameter> parameters);

        /// <summary>
        /// Retrieve data from a SQL database.
        /// </summary>
        /// <param name="connection">The connection to the DB.</param>
        /// <param name="transaction">If using a transaction, pass it in here, else pass in null.</param>
        /// <param name="query">The query string.</param>
        /// <param name="parameters">The query parameters.</param>
        /// <returns>The retrieved data.</returns>
        DbDataReader Select(DbConnection connection, DbTransaction transaction, SQLQueryString query, IEnumerable<DbParameter> parameters);

        /// <summary>
        /// Retrieve data from a SQL database and populate the input data set.
        /// </summary>
        /// <param name="connection">The connection to the DB.</param>
        /// <param name="transaction">If using a transaction, pass it in here, else pass in null.</param>
        /// <param name="fillTarget">The data set that will be filled.</param>
        /// <param name="query">The query string.</param>
        /// <param name="parameters">The query parameters.</param>
        void FillDataSet(DbConnection connection, DbTransaction transaction, DataSet fillTarget, SQLQueryString query, IEnumerable<DbParameter> parameters);

        /// <summary>
        /// Retrieve data from a SQL database and populate the input data set using the pre-prepared adapter.
        /// </summary>
        /// <param name="adapter">The adapter used to fill the data set.</param>
        /// <param name="fillTarget">The data set that will be filled.</param>
        /// <param name="tableName">Optional value that is the table name for updating.</param>
        void FillDataSet(DbDataAdapter adapter, DataSet fillTarget, DBTableName? tableName);

        /// <summary>
        /// Update data to a SQL database and with changes in the data set using the pre-prepared adapter.
        /// </summary>
        /// <param name="adapter">The adapter used to update the data set.</param>
        /// <param name="withChanges">The data set that will be updated to the SQL database.</param>
        /// <param name="tableName">Optional value that is the table name for updating.</param>
        /// <returns>The number of rows that were modified.</returns>
        ModifiedRowCount UpdateDataSet(DbDataAdapter adapter, DataSet withChanges, DBTableName? tableName);

        /// <summary>
        /// Update data held in a SQL database.
        /// </summary>
        /// <param name="connection">The connection to the DB.</param>
        /// <param name="transaction">If using a transaction, pass it in here, else pass in null.</param>
        /// <param name="query">The query string.</param>
        /// <param name="parameters">The query parameters.</param>
        /// <returns>The number of rows that were modified.</returns>
        ModifiedRowCount Update(DbConnection connection, DbTransaction transaction, SQLQueryString query, IEnumerable<DbParameter> parameters);

        /// <summary>
        /// Delete data from a SQL database.
        /// </summary>
        /// <param name="connection">The connection to the DB.</param>
        /// <param name="transaction">If using a transaction, pass it in here, else pass in null.</param>
        /// <param name="query">The query string.</param>
        /// <param name="parameters">The query parameters.</param>
        /// <returns>The number of rows that were deleted.</returns>
        ModifiedRowCount Delete(DbConnection connection, DbTransaction transaction, SQLQueryString query, IEnumerable<DbParameter> parameters);
    }

    /// <summary>
    /// Factory for creating an implementation of ISqlDriver.
    /// </summary>
    public interface ISqlDriverFactory : ILqbFactory
    {
        /// <summary>
        /// Create and return an implementation of ISqlDriver.
        /// </summary>
        /// <param name="timeout">The timeout to use when communicating with a database.</param>
        /// <returns>An implementation of ISqlDriver.</returns>
        ISqlDriver Create(TimeoutInSeconds timeout);

        /// <summary>
        /// Create and return the requested implementation of ISqlDriver.
        /// </summary>
        /// <param name="timeout">The timeout to use when communicating with a database.</param>
        /// <param name="decorator">The desired driver type.</param>
        /// <returns>An implementation of ISqlDriver.</returns>
        ISqlDriver Create(TimeoutInSeconds timeout, SqlDecoratorType decorator);
    }
}
