﻿namespace LqbGrammar.Drivers.ConversationLog
{
    using LqbGrammar.DataTypes;

    /// <summary>
    /// This contains the data to uniquely identify a resource in the 
    /// LQB system.  This would typically represent a primary key for 
    /// some table in an LQB database.  Conceptually such values are 
    /// only unique within the context of that table, so we include 
    /// information related to the table...expressed more semantically 
    /// as a resource type.
    /// </summary>
    public class ResourceId
    {
        /// <summary>
        /// Gets or sets the resource type.
        /// </summary>
        /// <value>The resource type.</value>
        public ResourceType Type { get; set; }

        /// <summary>
        /// Gets or sets the identifier used to retrieve the resource.
        /// </summary>
        /// <value>The resource identifier.</value>
        public ResourceIdentifier Identifier { get; set; }
    }
}
