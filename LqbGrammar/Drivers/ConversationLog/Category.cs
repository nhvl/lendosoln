﻿namespace LqbGrammar.Drivers.ConversationLog
{
    using LqbGrammar.DataTypes;

    /// <summary>
    /// A category is a named item in the conversation log system that 
    /// is created and managed by a broker and/or customer.
    /// </summary>
    public class Category
    {
        /// <summary>
        /// Gets or sets the identity information for a category.
        /// </summary>
        /// <value>The identity information for a category.</value>
        public CategoryReference Identity { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether given category can be assigned to new posts.
        /// </summary>
        /// <value>The active flag.</value>
        public bool IsActive { get; set; }

        /// <summary>
        /// Gets or sets the a name to be displayed to users for this category.
        /// </summary>
        /// <value>The name to be displayed for the category.</value>
        public CommentCategoryName DisplayName { get; set; }

        /// <summary>
        /// Gets or sets the id of the default permission level associated with this category.
        /// </summary>
        /// <value>The id of the default permission level associated with this category, or null if none.</value>
        public int? DefaultPermissionLevelId { get; set; }
    }
}
