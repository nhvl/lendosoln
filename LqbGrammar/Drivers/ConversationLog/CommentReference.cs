﻿namespace LqbGrammar.Drivers.ConversationLog
{
    using LqbGrammar.DataTypes;

    /// <summary>
    /// A comment reference is the data that is used to 
    /// identify a single comment.
    /// </summary>
    public class CommentReference
    {
        /// <summary>
        /// Gets or sets the primary key.
        /// The conversation log database uses BigInt primary keys.
        /// This should only be used by the Adapter.
        /// </summary>
        /// <value>The primary key.</value>
        public long Id { get; set; }
    }
}
