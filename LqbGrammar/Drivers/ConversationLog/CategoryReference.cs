﻿namespace LqbGrammar.Drivers.ConversationLog
{
    using LqbGrammar.DataTypes;

    /// <summary>
    /// A category reference is the data that is used to 
    /// identify a single category.
    /// </summary>
    public class CategoryReference
    {
        /// <summary>
        /// Gets or sets the primary key.
        /// The conversation log database uses BigInt primary keys.
        /// This should only be used by the Adapter.
        /// </summary>
        /// <value>The primary key.  Initialized to -1.</value>
        public long Id { get; set; } = -1;

        /// <summary>
        /// Gets or sets the owner identifier for the category, which is being used as a pseudo-namespace.
        /// </summary>
        /// <value>The identifier for the owner of the category.</value>
        public CommentCategoryNamespace OwnerId { get; set; }

        /// <summary>
        /// Gets or sets the name of the category.
        /// </summary>
        /// <value>The name of the category.</value>
        public CommentCategoryName Name { get; set; }
    }
}
