﻿namespace LqbGrammar.Drivers.ConversationLog
{
    using LqbGrammar.DataTypes;

    /// <summary>
    /// A comment is a single post or reply made by a user as 
    /// a contribution to a conversation.
    /// </summary>
    public class Comment
    {
        /// <summary>
        /// Gets or sets the identity information for a comment.
        /// </summary>
        /// <value>The identity information for a comment.</value>
        public CommentReference Identity { get; set; }

        /// <summary>
        /// Gets or sets the timestamp for the comment's creation.
        /// </summary>
        /// <value>The timestamp for the comment's creation.</value>
        public LqbEventDate Created { get; set; }

        /// <summary>
        /// Gets or sets the identifier for the user account that created the comment.
        /// </summary>
        /// <value>The identifier for the user account that created the comment.</value>
        public UserAccountIdentifier CommenterId { get; set; }

        /// <summary>
        /// Gets or sets the full name of the user that created the comment, at the time 
        /// of creation.  This may be changed if the same user posts a subsequent comment 
        /// after his/her name has changed.
        /// </summary>
        /// <value>The full name of the user that created the comment.</value>
        public PersonName Commenter { get; set; }

        /// <summary>
        /// Gets or sets the data making up the comment.
        /// </summary>
        /// <value>The data making up the comment.</value>
        public CommentLogEntry Value { get; set; }

        /// <summary>
        /// Gets or sets the depth from the top conversation that the comment is.  Top is 0.
        /// </summary>
        /// <value>The depth of the current comment relative to the top.</value>
        public Depth Depth { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether or not the comment is hidden.
        /// </summary>
        /// <value>Whether or not the comment is hidden.</value>
        public bool IsHidden { get; set; }

        /// <summary>
        /// Gets or sets the name of the person who hid the comment, if it is hidden.
        /// </summary>
        /// <value>The full name of the person who hid the comment, or null if not hidden.</value>
        public PersonName? HiderFullName { get; set; }
    }
}
