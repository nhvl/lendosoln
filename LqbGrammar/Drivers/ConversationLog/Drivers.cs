﻿namespace LqbGrammar.Drivers.ConversationLog
{
    using System.Collections.Generic;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Interface used to manage categories in the comment log system.
    /// </summary>
    public interface ICategoryDriver : ILqbDriver
    {
        /// <summary>
        /// Retrieve all the categories relevant to the logged in user, in the current order.
        /// </summary>
        /// <param name="secToken">Security token for the authenticated principal making this method call.</param>
        /// <returns>The list of categories.</returns>
        List<Category> GetAllCategories(SecurityToken secToken);

        /// <summary>
        /// Either create a new category or modify and existing category.
        /// </summary>
        /// <param name="secToken">Security token for the authenticated principal making this method call.</param>
        /// <param name="category">A category in the comment log system.</param>
        /// <returns>The current state of the category in the log system after the method has completed.</returns>
        Category SetCategory(SecurityToken secToken, Category category);

        /// <summary>
        /// Set the order in which categories are to be presented to users of the comment system.
        /// </summary>
        /// <param name="secToken">Security token for the authenticated principal making this method call.</param>
        /// <param name="categoryOrder">An ordered list of category references, the order will be reflected in subsequent GUI views.</param>
        void SetCategoryOrder(SecurityToken secToken, List<CategoryReference> categoryOrder);
    }

    /// <summary>
    /// Interface to manage comments in the comment log system.
    /// </summary>
    public interface ICommentDriver : ILqbDriver
    {
        /// <summary>
        /// Retrieve all conversations that have been attached to an LQB resource.
        /// </summary>
        /// <param name="secToken">Security token for the authenticated principal making this method call.</param>
        /// <param name="resource">The LQB resource.</param>
        /// <returns>All conversations.  The order is determined by the category order, followed by the creation dates of the conversations.</returns>
        IEnumerable<Conversation> GetAllConversations(SecurityToken secToken, ResourceId resource);

        /// <summary>
        /// Post a new comment to a category, beginning a new conversation.
        /// </summary>
        /// <param name="secToken">Security token for the authenticated principal making this method call.</param>
        /// <param name="resource">The LQB resource.</param>
        /// <param name="category">The category under which this comment is posted.</param>
        /// <param name="comment">The comment data.</param>
        /// <param name="permissionLevelId">The id of the permission level associated with the conversation, or null if not yet migrated.</param>
        /// <param name="bypassPermissionCheck">Bit to bypass the permission check.</param>
        void Post(SecurityToken secToken, ResourceId resource, CategoryReference category, Comment comment, long? permissionLevelId, bool bypassPermissionCheck = false);

        /// <summary>
        /// Reply to an existing comment in the conversation log system.
        /// </summary>
        /// <param name="secToken">Security token for the authenticated principal making this method call.</param>
        /// <param name="parent">The comment toward which this reply is directed.</param>
        /// <param name="comment">The comment data that is the reply.</param>
        void Reply(SecurityToken secToken, CommentReference parent, Comment comment);

        /// <summary>
        /// Hides an existing comment.
        /// </summary>
        /// <param name="secToken">Security token for the calling principal.</param>
        /// <param name="commentId">An identifier of the comment to be hidden.</param>
        /// <returns>True iff it succeeded.</returns>
        bool HideComment(SecurityToken secToken, CommentReference commentId);

        /// <summary>
        /// Unhides an existing comment.
        /// </summary>
        /// <param name="secToken">Security token for the calling principal.</param>
        /// <param name="commentId">An identifier of the comment to be unhidden.</param>
        /// <returns>True iff it succeeded.</returns>
        bool ShowComment(SecurityToken secToken, CommentReference commentId);
    }

    /// <summary>
    /// Interface used to create implementations of the interfaces involved in managing the conversation log system.
    /// </summary>
    public interface ICommentLogDriverFactory : ILqbFactory
    {
        /// <summary>
        /// Create an implementation of the category management interface.
        /// </summary>
        /// <param name="location">The location of the service.</param>
        /// <param name="port">The port on which the service is hosted.</param>
        /// <returns>An implementation of the category management interface.</returns>
        ICategoryDriver CreateCategoryDriver(string location, int port);

        /// <summary>
        /// Create an implementation of the comment management interface.
        /// </summary>
        /// <param name="location">The location of the service.</param>
        /// <param name="port">The port on which the service is hosted.</param>
        /// <returns>An implementation of the comment management interface.</returns>
        ICommentDriver CreateCommentDriver(string location, int port);
    }
}
