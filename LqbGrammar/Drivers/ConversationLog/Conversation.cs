﻿namespace LqbGrammar.Drivers.ConversationLog
{
    using System.Collections.Generic;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// A convesation is started when a user posts a comment under a category, as opposed to
    /// replying to a pre-existing comment.
    /// </summary>
    public class Conversation
    {
        /// <summary>
        /// Gets or sets a reference to the category under which the conversation was started.
        /// </summary>
        /// <value>A reference to the category under which the conversation was started.</value>
        public CategoryReference Category { get; set; }

        /// <summary>
        /// Gets or sets the set of comments that compose the conversation.  The comments will be
        /// ordered so that threads are preserved as best as possible.  This means that the 
        /// GUI code should be able to display them in the order in which they appear here.
        /// </summary>
        /// <value>An ordered set of comments belonging to the conversation.</value>
        public IEnumerable<Comment> Comments { get; set; }

        /// <summary>
        /// Gets or sets the id of the permission level associated with the conversation. <para></para>
        /// Null will mean not yet migrated, which will be interpreted as the Default.
        /// </summary>
        /// <value>The id of the permission level associated with the conversation, or null if not yet migrated.</value>
        public long? PermissionLevelId { get; set; }
    }
}
