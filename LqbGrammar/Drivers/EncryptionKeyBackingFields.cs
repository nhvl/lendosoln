﻿namespace LqbGrammar.Drivers
{
    /// <summary>
    /// The backing fields of an encryption key.
    /// </summary>
    public class EncryptionKeyBackingFields
    {
        /// <summary>
        /// Gets or sets the key version.
        /// </summary>
        public int Version { get; set; }

        /// <summary>
        /// Gets or sets the byte array for Field1.
        /// </summary>
        public byte[] Field1 { get; set; }

        /// <summary>
        /// Gets or sets the byte array for Field2.
        /// </summary>
        public byte[] Field2 { get; set; }
    }
}
