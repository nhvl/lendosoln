﻿namespace LqbGrammar.Drivers.Emailer
{
    using System.IO;
    using System.Text;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Exceptions;
    using LqbGrammar.Validation;

    /// <summary>
    /// We support different types of email, currently plain text and html.
    /// This base class will define the common interface for all email body
    /// types.
    /// </summary>
    public abstract class EmailBody
    {
        /// <summary>
        /// The body is held within a string builder during construction.
        /// </summary>
        private StringBuilder sb;

        /// <summary>
        /// Initializes a new instance of the <see cref="EmailBody"/> class.
        /// </summary>
        protected EmailBody()
        {
            this.sb = new StringBuilder(500); // emails are reasonably large, so start off with plenty of room
        }

        /// <summary>
        /// Append text to the email body.
        /// </summary>
        /// <param name="text">The text to be appended to the email's body.</param>
        public void AppendText(string text)
        {
            if (string.IsNullOrEmpty(text))
            {
                return;
            }

            if (!this.ValidateInput(text))
            {
                throw new DeveloperException(ErrorMessage.SystemError);
            }

            this.sb.Append(text);
        }

        /// <summary>
        /// Append an object to the email body, using the object's ToString() method.
        /// </summary>
        /// <typeparam name="T">Type of the object to be added to the email body.</typeparam>
        /// <param name="item">Type instance to be added to the email body.</param>
        public void AppendText<T>(T item)
        {
            string text = item.ToString();
            this.AppendText(text);
        }

        /// <summary>
        /// Append a line of text to the email body.  The text will be validated.
        /// </summary>
        /// <param name="line">The line of text to be added to the email body.</param>
        public void AppendLine(string line)
        {
            if (line == null)
            {
                return;
            }

            using (var sr = new StringReader(line))
            {
                string singleLine = sr.ReadLine();
                while (singleLine != null)
                {
                    if (string.IsNullOrEmpty(singleLine))
                    {
                        this.sb.AppendLine();
                    }
                    else
                    {
                        if (!this.ValidateInput(singleLine))
                        {
                            throw new DeveloperException(ErrorMessage.SystemError);
                        }

                        this.sb.AppendLine(singleLine);
                    }

                    singleLine = sr.ReadLine();
                }
            }
        }

        /// <summary>
        /// Override the implementation inherited from the Object class.
        /// </summary>
        /// <returns>The body of the email message.</returns>
        public override string ToString()
        {
            return this.sb.ToString();
        }

        /// <summary>
        /// Default validator allows anything.
        /// </summary>
        /// <param name="input">Text to be validated.</param>
        /// <returns>True if the input is valid, false otherwise.</returns>
        protected virtual bool ValidateInput(string input)
        {
            var result = RegularExpressionEngine.Execute(input, RegularExpressionString.EmailBody);
            return result.IsMatch();
        }
    }
}
