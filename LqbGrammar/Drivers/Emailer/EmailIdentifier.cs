﻿namespace LqbGrammar.Drivers.Emailer
{
    using System;

    /// <summary>
    /// Encapsulate the concept of an email identifier (UID),
    /// the form of which may be specific to the particular email server
    /// technology.  Accordingly server-specific identifier classes
    /// will inherit from this and perform their own validation.
    /// </summary>
    public abstract class EmailIdentifier : IEquatable<EmailIdentifier>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EmailIdentifier"/> class.
        /// </summary>
        /// <param name="identifier">The identifier that is being encapsulated.</param>
        protected EmailIdentifier(string identifier)
        {
            this.Value = identifier;
        }

        /// <summary>
        /// Gets or sets the value of the identifier.
        /// </summary>
        private string Value { get; set; }

        /// <summary>
        /// Create an identifier from stored value.
        /// </summary>
        /// <param name="storedIdentifier">Identifier value from some storage location.</param>
        /// <returns>The email identifier.</returns>
        public static EmailIdentifier Create(string storedIdentifier)
        {
            return new RecoveredIdentifer(storedIdentifier);
        }

        /// <summary>
        /// Static method that checks for equality between two instances.
        /// </summary>
        /// <param name="lhs">Instance on the left side of the == operator.</param>
        /// <param name="rhs">Instance on the right side of the == operator.</param>
        /// <returns>True if the two instances are equal, false otherwise.</returns>
        public static new bool Equals(object lhs, object rhs)
        {
            if (!(lhs is EmailIdentifier))
            {
                return false;
            }

            if (!(rhs is EmailIdentifier))
            {
                return false;
            }

            return ((EmailIdentifier)lhs).Equals((EmailIdentifier)rhs);
        }

        /// <summary>
        /// The == operator is implemented here.
        /// </summary>
        /// <param name="lhs">Instance on the left of the == operator.</param>
        /// <param name="rhs">Instance on the right of the == operator.</param>
        /// <returns>True if the two instances are equal, false otherwise.</returns>
        public static bool operator ==(EmailIdentifier lhs, EmailIdentifier rhs)
        {
            return lhs.Equals(rhs);
        }

        /// <summary>
        /// The != operator is implemented here.
        /// </summary>
        /// <param name="lhs">Instance on the left of the != operator.</param>
        /// <param name="rhs">Instance on the right of the != operator.</param>
        /// <returns>True if the two instances are not equal, false otherwise.</returns>
        public static bool operator !=(EmailIdentifier lhs, EmailIdentifier rhs)
        {
            return !lhs.Equals(rhs);
        }

        /// <summary>
        /// Return the encapsulated value.
        /// </summary>
        /// <returns>Encapsulated value.</returns>
        public override string ToString()
        {
            return this.Value;
        }

        /// <summary>
        /// Compare this identifier with another identifer.
        /// </summary>
        /// <param name="other">The other identifier.</param>
        /// <returns>True if the identifiers match, false otherwise.</returns>
        public bool Equals(EmailIdentifier other)
        {
            return object.ReferenceEquals(other, null) ? false : this.Value == other.Value;
        }

        /// <summary>
        /// Override the Equals method inherited from the Object class.
        /// </summary>
        /// <param name="obj">Instance that is checked for equality with this instance.</param>
        /// <returns>True if obj is equal to this instance, false otherwise.</returns>
        public override bool Equals(object obj)
        {
            if (!(obj is EmailIdentifier))
            {
                return false;
            }

            return Equals((EmailIdentifier)obj);
        }

        /// <summary>
        /// Override of the method inherited from the Object class.
        /// </summary>
        /// <returns>An integer that is the hash code for this instance.</returns>
        public override int GetHashCode()
        {
            return this.Value.GetHashCode();
        }

        /// <summary>
        /// Class used to resurrect an identifier from a stored value.
        /// </summary>
        private sealed class RecoveredIdentifer : EmailIdentifier
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="RecoveredIdentifer"/> class.
            /// </summary>
            /// <param name="identifier">The identifier that is being encapsulated.</param>
            public RecoveredIdentifer(string identifier)
                : base(identifier)
            {
            }
        }
    }
}
