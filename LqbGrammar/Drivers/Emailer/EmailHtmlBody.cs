﻿namespace LqbGrammar.Drivers.Emailer
{
    using System.Collections.Generic;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// This class incapsulates an email body that holds it's content as html.
    /// </summary>
    public sealed class EmailHtmlBody : EmailBody
    {
        // Assume that the business logic has taken care of the html validation
        // because that is best handled using the AntiXss module.  In the future
        // we may place that behind an adapter and then we can use it here
        // via a driver, but we don't yet have that in place.

        /// <summary>
        /// Images to be used in the email body will be held here.
        /// </summary>
        private List<TypedResource<byte[]>> images;

        /// <summary>
        /// Initializes a new instance of the <see cref="EmailHtmlBody"/> class.
        /// </summary>
        public EmailHtmlBody() : base()
        {
            this.images = new List<TypedResource<byte[]>>();
        }

        /// <summary>
        /// Gets the list of images that will appear within the email body.
        /// </summary>
        /// <value>The list of images.</value>
        public List<TypedResource<byte[]>> LinkedResources
        {
            get { return this.images; }
        }

        /// <summary>
        /// Add a linked resource to the email body.
        /// The current LendingQB code only adds linked resources as byte[].
        /// </summary>
        /// <param name="resource">The resource that will be linked from within the email body.</param>
        public void AddLinkedResource(TypedResource<byte[]> resource)
        {
            this.images.Add(resource);
        }
    }
}
