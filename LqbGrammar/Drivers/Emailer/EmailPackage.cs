﻿namespace LqbGrammar.Drivers.Emailer
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using DataTypes;
    using Exceptions;

    /// <summary>
    /// This class is used to gather up all the components
    /// of an email, representing the full email contents
    /// that are ready for delivery.
    /// </summary>
    public sealed class EmailPackage
    {
        /// <summary>
        /// Binary resources are held here.
        /// </summary>
        private List<TypedResource<byte[]>> binary;

        /// <summary>
        /// String resources are held here.
        /// </summary>
        private List<TypedResource<string>> text;

        /// <summary>
        /// Resources contained within files are held here.
        /// </summary>
        private List<TypedResource<FileInfo>> file;

        /// <summary>
        /// Email addresses that will be placed into the carbon-copy field.
        /// </summary>
        private List<EmailAddress> cc;

        /// <summary>
        /// Email addresses that will be placed into the blind carbon-copy field.
        /// </summary>
        private List<EmailAddress> bcc;

        /// <summary>
        /// Initializes a new instance of the <see cref="EmailPackage"/> class.
        /// </summary>
        /// <param name="brokerId">The broker ID.</param>
        /// <param name="from">The from address.</param>
        /// <param name="subject">The subject line.</param>
        /// <param name="body">The body of the email.</param>
        /// <param name="to">The list of to addresses.</param>
        public EmailPackage(Guid brokerId, EmailAddress from, EmailSubject subject, EmailBody body, params EmailAddress[] to)
        {
            this.BrokerId = brokerId;
            this.From = from;
            this.To = to;
            this.Subject = subject;
            this.Body = body;
        }

        /// <summary>
        /// Gets the ID of the Broker this email was sent from.
        /// </summary>
        /// <value>Broker ID.</value>
        public Guid BrokerId { get; private set; }

        /// <summary>
        /// Gets the email address of the sender.
        /// </summary>
        /// <value>The from address.</value>
        public EmailAddress From { get; private set; }

        /// <summary>
        /// Gets or sets the email address of the sender.
        /// Used for email spoofing.
        /// </summary>
        /// <value>The from address.</value>
        public EmailAddress Sender { get; set; }

        /// <summary>
        /// Gets the array of email addresses of the receivers.
        /// </summary>
        /// <value>The list of to addresses.</value>
        public EmailAddress[] To { get; private set; }

        /// <summary>
        /// Gets the subject line for the email.
        /// </summary>
        /// <value>The subject line.</value>
        public EmailSubject Subject { get; private set; }

        /// <summary>
        /// Gets the email body.
        /// </summary>
        /// <value>The body of the email.</value>
        public EmailBody Body { get; private set; }

        /// <summary>
        /// Gets the addresses for the carbon-copy line.
        /// </summary>
        /// <value>The carbon-copy line.</value>
        public List<EmailAddress> Cc
        {
            get { return this.cc; }
        }

        /// <summary>
        /// Gets the addresses for the blind carbon-copy line.
        /// </summary>
        /// <value>The blind carbon-copy line.</value>
        public List<EmailAddress> Bcc
        {
            get { return this.bcc; }
        }

        /// <summary>
        /// Gets the list of attachments that are binary resources.
        /// </summary>
        /// <value>The binary attachments.</value>
        public List<TypedResource<byte[]>> BinaryAttachments
        {
            get { return this.binary; }
        }

        /// <summary>
        /// Gets the list of attachments that are text resources.
        /// </summary>
        /// <value>The text attachments.</value>
        public List<TypedResource<string>> TextAttachments
        {
            get { return this.text; }
        }

        /// <summary>
        /// Gets the list of attachments that are contained within files.
        /// </summary>
        /// <value>The file attachments.</value>
        public List<TypedResource<FileInfo>> FileAttachments
        {
            get { return this.file; }
        }

        /// <summary>
        /// Updates the from address for the package.
        /// </summary>
        /// <param name="newAddress">
        /// The new address.
        /// </param>
        public void UpdateFromAddress(EmailAddress newAddress)
        {
            if (newAddress == EmailAddress.BadEmail)
            {
                throw new DeveloperException(ErrorMessage.SystemError);
            }

            this.From = newAddress;
        }

        /// <summary>
        /// Add an address to the carbon-copy line.
        /// </summary>
        /// <param name="cc">The email address to add to the carbon-copy line.</param>
        public void AddCc(EmailAddress cc)
        {
            if (this.cc == null)
            {
                this.cc = new List<EmailAddress>();
            }

            this.cc.Add(cc);
        }

        /// <summary>
        /// Add an adress to the blind carbon-copy line.
        /// </summary>
        /// <param name="bcc">The email address to add to the blind carbon-copy line.</param>
        public void AddBcc(EmailAddress bcc)
        {
            if (this.bcc == null)
            {
                this.bcc = new List<EmailAddress>();
            }

            this.bcc.Add(bcc);
        }

        /// <summary>
        /// Add a binary attachment to the email.
        /// </summary>
        /// <param name="resource">The binary resource to be attached to the email.</param>
        public void AddAttachment(TypedResource<byte[]> resource)
        {
            if (this.binary == null)
            {
                this.binary = new List<TypedResource<byte[]>>();
            }

            this.binary.Add(resource);
        }

        /// <summary>
        /// Add a string resource as an attachment to the email.
        /// </summary>
        /// <param name="resource">The resource to be attached to the email.</param>
        public void AddAttachment(TypedResource<string> resource)
        {
            if (this.text == null)
            {
                this.text = new List<TypedResource<string>>();
            }

            this.text.Add(resource);
        }

        /// <summary>
        /// Add a resource to the email that is referenced with information about the containing file.
        /// </summary>
        /// <param name="resource">The resource to be attached to the email.</param>
        public void AddAttachment(TypedResource<FileInfo> resource)
        {
            if (this.file == null)
            {
                this.file = new List<TypedResource<FileInfo>>();
            }

            this.file.Add(resource);
        }
    }
}
