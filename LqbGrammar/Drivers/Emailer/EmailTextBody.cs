﻿namespace LqbGrammar.Drivers.Emailer
{
    using LqbGrammar.DataTypes;
    using LqbGrammar.Validation;

    /// <summary>
    /// Encapsulates the body of an email message that is composed of simple text.
    /// </summary>
    public sealed class EmailTextBody : EmailBody
    {
        /// <summary>
        /// Validate the text of the email's body to ensure nothing dangerous is placed within it.
        /// </summary>
        /// <param name="input">The text to be validated.</param>
        /// <returns>True if the text is valid, false otherwise.</returns>
        protected override bool ValidateInput(string input)
        {
            RegularExpressionResult result = RegularExpressionEngine.Execute(input, RegularExpressionString.EmailBody);
            return result.IsMatch();
        }
    }
}    