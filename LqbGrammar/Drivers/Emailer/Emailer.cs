﻿namespace LqbGrammar.Drivers.Emailer
{
    using System;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Interface that defines a driver for sending email.
    /// </summary>
    public interface IEmailDriver : ILqbDriver
    {
        /// <summary>
        /// Send an email message.
        /// </summary>
        /// <param name="email">The data from which the email message will be composed.</param>
        void SendEmail(EmailPackage email);
    }

    /// <summary>
    /// Interface uses to extract data from a received email.
    /// </summary>
    public interface IReceivedEmailData
    {
        /// <summary>
        /// Gets the contents of the email at the time it was sent.
        /// </summary>
        /// <value>The contents of the email at the time it was sent.</value>
        EmailPackage Contents { get; }

        /// <summary>
        /// Gets the timestamp of when the email was sent.
        /// </summary>
        /// <value>The timestamp of when the email was sent.</value>
        LqbEventDate DateSent { get; }

        /// <summary>
        /// Gets the full raw message that was received.
        /// </summary>
        /// <value>The full raw message that was received.</value>
        byte[] RawMessage { get; }

        /// <summary>
        /// Gets a value indicating whether the X-Auto-Response-Suppress header was set.
        /// </summary>
        /// <value>A value indicating whether the X-Auto-Response-Suppress header was set.</value>
        bool IsAutoResponseSuppressed { get; }
    }

    /// <summary>
    /// Interface defining a driver for receiving email.
    /// Note that receiving is an operation that involves maintaining a
    /// connection to the server, so this interface inherits from
    /// IDisposable to ensure that the connection is closed.
    /// </summary>
    public interface IReceiveEmailDriver : ILqbDriver, IDisposable
    {
        /// <summary>
        /// Gets the number of email messages available on the server.
        /// </summary>
        /// <value>The number of email messages available on the server.</value>
        int Count { get; }

        /// <summary>
        /// Retrieve an identifer for the email with the input index.
        /// </summary>
        /// <param name="index">A value in the range [0, Count).</param>
        /// <returns>An identifier for the specified email.</returns>
        EmailIdentifier GetIdentifier(int index);

        /// <summary>
        /// Retrieve the email data for the input index.
        /// </summary>
        /// <param name="index">A value in the range [0, Count).</param>
        /// <returns>The data for the specified email.</returns>
        IReceivedEmailData GetEmail(int index);

        /// <summary>
        /// Delete the email data for the input index.
        /// </summary>
        /// <param name="index">A value in the range [0, Count).</param>
        void DeleteEmail(int index);
    }

    /// <summary>
    /// Interface that defines a factory for creating instances of objects that can send emails.
    /// </summary>
    public interface IEmailDriverFactory : ILqbFactory
    {
        /// <summary>
        /// Create an instance of a class that can send emails.
        /// </summary>
        /// <param name="server">The server name for sending emails.</param>
        /// <param name="portNumber">The port number in which to use to connect.</param>
        /// <returns>An email driver implementation.</returns>
        IEmailDriver Create(EmailServerName server, PortNumber portNumber);
    }

    /// <summary>
    /// Factory interface for creating an implementation of the IReceiveEmailDriver interface.
    /// </summary>
    public interface IReceiveEmailDriverFactory
    {
        /// <summary>
        /// Create an email driver.
        /// </summary>
        /// <param name="server">The email server name that will be used for sending the email.</param>
        /// <param name="userName">The user name for logging into the email server.</param>
        /// <param name="password">The password for logging into the email server.</param>
        /// <returns>An email driver.</returns>
        IReceiveEmailDriver Create(EmailServerName server, string userName, string password);
    }
}
