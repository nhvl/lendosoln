﻿namespace LqbGrammar.Drivers
{
    using System.Collections.Generic;
    using DataTypes;

    /// <summary>
    /// Defines the interface to open XML documents.
    /// </summary>
    public interface IOpenXmlDocumentDriver : ILqbDriver
    {
        /// <summary>
        /// Sets values for the list of fields in the open XML document.
        /// </summary>
        /// <param name="localFilePath">
        /// The path to the document.
        /// </param>
        /// <param name="fieldNameValueDict">
        /// The field name - value dictionary.
        /// </param>
        void SetTextFieldsOnDocument(LocalFilePath localFilePath, IDictionary<string, string> fieldNameValueDict);
    }
}
