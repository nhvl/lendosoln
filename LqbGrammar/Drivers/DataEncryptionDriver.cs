﻿namespace LqbGrammar.Drivers
{
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Decrypt data with an arbitrary key.
    /// </summary>
    public interface IDataEncryptionDriver : ILqbDriver
    {
        /// <summary>
        /// Encrypt the data with the key.
        /// </summary>
        /// <param name="unencryptedData">The data to be encrypted.</param>
        /// <param name="encryptionKey">The key to use in the encryption.</param>
        /// <returns>The encrypted data.</returns>
        byte[] Encrypt(byte[] unencryptedData, EncryptionKey encryptionKey);

        /// <summary>
        /// Decrypt the provided encrypted data.
        /// </summary>
        /// <param name="encryptedData">The encrypted data.</param>
        /// <param name="encryptionKey">The key to use in decryption.</param>
        /// <returns>The decrypted data.</returns>
        byte[] Decrypt(byte[] encryptedData, EncryptionKey encryptionKey);

        /// <summary>
        /// Try to decrypt the provided encrypted data.
        /// </summary>
        /// <param name="encryptedData">The encrypted data.</param>
        /// <param name="encryptionKey">The key data to use in decryption.</param>
        /// <param name="decodedDecryptedValue">The resulting decoded and decrypted value.</param>
        /// <returns>True if decoding and decryption was successful.  Otherwise false.</returns>
        bool TryDecrypt(byte[] encryptedData, EncryptionKey encryptionKey, out byte[] decodedDecryptedValue);
        
        /// <summary>
        /// Decode and decrypt the base64 encoded data.
        /// </summary>
        /// <param name="encodedEncryptedData">Base64 encoded encrypted data.</param>
        /// <param name="encryptionKey">The key to use for decryption.</param>
        /// <returns>The decoded and decrypted value.</returns>
        byte[] DecodeAndDecrypt(Base64EncodedData encodedEncryptedData, EncryptionKey encryptionKey);

        /// <summary>
        /// Encrypt and encode in base64.
        /// </summary>
        /// <param name="unencryptedData">The data to encrypt and encode.</param>
        /// <param name="encryptionKey">The key to use for encryption.</param>
        /// <returns>The encoded encrypted value.</returns>
        Base64EncodedData EncryptAndEncode(byte[] unencryptedData, EncryptionKey encryptionKey);
    }

    /// <summary>
    /// Factory for creating an implementation of IDataEncryptionDriver.
    /// </summary>
    public interface IDataEncryptionDriverFactory : ILqbFactory
    {
        /// <summary>
        /// Create and return an implementation of IDataEncryptionDriver.
        /// </summary>
        /// <returns>An implementation of IDataEncryptionDriver.</returns>
        IDataEncryptionDriver Create();
    }
}
