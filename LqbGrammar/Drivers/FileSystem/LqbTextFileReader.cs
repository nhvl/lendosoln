﻿namespace LqbGrammar.Drivers.FileSystem
{
    using System.IO;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Wrapper around a StreamReader instance.
    /// </summary>
    public sealed class LqbTextFileReader
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LqbTextFileReader"/> class.
        /// </summary>
        /// <param name="reader">The StreamReader used to perform the work.</param>
        public LqbTextFileReader(StreamReader reader)
        {
            if (reader == null)
            {
                throw new DeveloperException(ErrorMessage.SystemError);
            }

            this.Reader = reader;
        }

        /// <summary>
        /// Gets the encapsulated stream reader.
        /// </summary>
        /// <value>The encapsulated stream reader.</value>
        public StreamReader Reader { get; private set; }

        /// <summary>
        /// Read a line of text from the text file,
        /// or return null when at the end of the file.
        /// </summary>
        /// <returns>A line of text from the file, or null.</returns>
        public string ReadLine()
        {
            return this.Reader.ReadLine();
        }
    }
}
