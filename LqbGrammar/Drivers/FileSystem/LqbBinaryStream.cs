﻿namespace LqbGrammar.Drivers.FileSystem
{
    using System.IO;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Wrapper around a FileStream instance.
    /// </summary>
    public sealed class LqbBinaryStream
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LqbBinaryStream"/> class.
        /// </summary>
        /// <param name="stream">The contained FileStream.</param>
        public LqbBinaryStream(FileStream stream)
        {
            if (stream == null)
            {
                // The most likely culprit is a problem with the underlying file system
                throw new ServerException(ErrorMessage.SystemError);
            }

            this.Stream = stream;
        }

        /// <summary>
        /// Gets the underlying FileStream so client code can pass it into a utility.
        /// </summary>
        /// <value>The underlying FileStream.</value>
        public FileStream Stream { get; private set; }
    }
}
