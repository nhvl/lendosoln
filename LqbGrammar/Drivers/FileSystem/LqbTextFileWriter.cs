﻿namespace LqbGrammar.Drivers.FileSystem
{
    using System.IO;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Wrapper around a StreamWriter instance.
    /// </summary>
    public sealed class LqbTextFileWriter
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LqbTextFileWriter"/> class.
        /// </summary>
        /// <param name="writer">The StreamWriter used to perform the work.</param>
        public LqbTextFileWriter(StreamWriter writer)
        {
            if (writer == null)
            {
                throw new DeveloperException(ErrorMessage.SystemError);
            }

            this.Writer = writer;
        }

        /// <summary>
        /// Gets the encapsulated stream writer.
        /// </summary>
        /// <value>The encapsulated stream writer.</value>
        public StreamWriter Writer { get; private set; }

        /// <summary>
        /// Append text to a file without adding a new line.
        /// </summary>
        /// <param name="text">The text that will be added to the file.</param>
        public void Append(string text)
        {
            this.Writer.Write(text);
        }

        /// <summary>
        /// Append text to a file and add a new line.
        /// </summary>
        /// <param name="text">The text that will be added to the file.</param>
        public void AppendLine(string text)
        {
            this.Writer.WriteLine(text);
        }
    }
}
