﻿namespace LqbGrammar.Drivers
{
    using Commands;
    using DataTypes;

    /// <summary>
    /// Interface for a driver that handles logging and execution of integration communications.
    /// </summary>
    /// <typeparam name="TResponse">The type of response object that will be output by the communication.</typeparam>
    public interface IIntegrationDriver<TResponse> : ILqbDriver
    {
        /// <summary>
        /// Executes a communication with a vendor over HTTP, using the given communication object.
        /// </summary>
        /// <param name="communication">The communication object which handles business logic like serialization and saving response data.</param>
        /// <returns>A result containing the parsed response or an error.</returns>
        Result<TResponse> ExecuteCommunication(IIntegrationHttpCommunication<TResponse> communication);
    }

    /// <summary>
    /// Factory for creating implementations of the <see cref="IIntegrationDriver{TResponse}"/> interface.
    /// </summary>
    /// <typeparam name="TResponse">The type of response object that will be returned from the created driver.</typeparam>
    public interface IIntegrationDriverFactory<TResponse> : ILqbFactory
    {
        /// <summary>
        /// Create an implementation of the IIntegrationDriver interface.
        /// </summary>
        /// <returns>An implementation of the IIntegrationDriver interface.</returns>
        IIntegrationDriver<TResponse> Create();
    }
}
