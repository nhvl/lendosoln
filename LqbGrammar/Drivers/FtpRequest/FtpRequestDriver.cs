﻿namespace LqbGrammar.Drivers.FtpRequest
{
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Allow client code to determine which FTP service provider to use.
    /// </summary>
    public enum FtpTechnology
    {
        /// <summary>
        /// Use the System.Net.FtpWebRequest.
        /// </summary>
        SystemNet,

        /// <summary>
        /// Use the WinSCP library.
        /// </summary>
        WinSCP
    }

    /// <summary>
    /// Interface used to issue requests over ftp.
    /// </summary>
    public interface IFtpRequestDriver : ILqbDriver
    {
        /// <summary>
        /// Carry out the communication with the indicated URL.
        /// </summary>
        /// <param name="url">The target URL.</param>
        /// <param name="options">The options that control the request and response parsing.</param>
        /// <returns>True if the communication was successful, false otherwise.</returns>
        bool ExecuteCommunication(LqbAbsoluteUri url, FtpRequestOptions options);
    }

    /// <summary>
    /// Factory for creating implementations of the IFtpRequestDriver interface.
    /// </summary>
    public interface IFtpRequestDriverFactory : ILqbFactory
    {
        /// <summary>
        /// Create an implementation of the IFtpRequestDriver interface.
        /// </summary>
        /// <param name="provider">The technology provider to use for sending the FTP messages.</param>
        /// <returns>An implementation of the IFtpRequestDriver interface.</returns>
        IFtpRequestDriver Create(FtpTechnology provider);
    }
}
