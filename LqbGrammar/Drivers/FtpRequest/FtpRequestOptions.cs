﻿namespace LqbGrammar.Drivers.FtpRequest
{
    using System;
    using System.Net;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Gather together the options that are available
    /// when making an ftp request and parsing the
    /// response.
    /// </summary>
    public sealed class FtpRequestOptions
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FtpRequestOptions"/> class.
        /// </summary>
        public FtpRequestOptions()
        {
            this.Protocol = FileTransferProtocol.Ftp;
            this.Mode = FileTransferProtocol.SecurityMode.None;
            this.UserName = null;
            this.Password = null;
            this.UploadFile = LocalFilePath.Invalid;
            this.UsePassive = true;
            this.HostKeyFingerprint = null;
            this.HostCertificateFingerprint = null;
        }

        /// <summary>
        /// Gets or sets the protocol used to carry out the file transfer operation.
        /// </summary>
        /// <value>The protocol used to carry out the file transfer operation.</value>
        public FileTransferProtocol Protocol { get; set; }

        /// <summary>
        /// Gets or sets the underlying algorithm for the negotiated security.
        /// </summary>
        /// <value>The underlying algorithm for the negotiated security.</value>
        public FileTransferProtocol.SecurityMode Mode { get; set; }

        /// <summary>
        /// Gets or sets the network user name to use.
        /// </summary>
        /// <value>The network user name to use.</value>
        public string UserName { get; set; }

        /// <summary>
        /// Gets or sets the network password to use.
        /// </summary>
        /// <value>The network password to use.</value>
        public string Password { get; set; }

        /// <summary>
        /// Gets or sets the path to the local file that is to be uploaded.
        /// </summary>
        /// <value>The path to the local file that is to be uploaded.</value>
        public LocalFilePath UploadFile { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the client should initiate a connection on the data port.
        /// </summary>
        /// <value>A value indicating whether the client should initiate a connection on the data port.</value>
        public bool UsePassive { get; set; }

        /// <summary>
        /// Gets or sets the host key fingerprint.
        /// </summary>
        /// <value>The host key fingerprint.</value>
        public string HostKeyFingerprint { get; set; }

        /// <summary>
        /// Gets or sets the host certificate fingerprint.
        /// </summary>
        /// <value>The host certificate fingerprint.</value>
        public string HostCertificateFingerprint { get; set; }

        /// <summary>
        /// Gets or sets the response status code from the communication.
        /// </summary>
        /// <value>The response status code from the communication.</value>
        public FtpStatusCode StatusCode { get; set; }

        /// <summary>
        /// Gets or sets the response status description from the communication.
        /// </summary>
        /// <value>The response status description from the communication.</value>
        public string StatusDescription { get; set; }

        /// <summary>
        /// Gets or sets an error message for the LQB logging framework.
        /// </summary>
        /// <value>An error message for the LQB logging framework.</value>
        public string ErrorMessage { get; set; }
    }
}
