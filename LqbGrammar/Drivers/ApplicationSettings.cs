﻿namespace LqbGrammar.Drivers
{
    using System.Collections.Generic;

    /// <summary>
    /// Interface used to retrieve the AppSettings values
    /// from an application's configuration file.
    /// </summary>
    public interface IApplicationSettingsDriver : ILqbDriver
    {
        /// <summary>
        /// Retrieve the application settings.
        /// Note that the name and value of each setting is not encapsulated into a data type for
        /// verification.  We will defer that to a wrapper for the driver to simplify the implementation
        /// since we are dealing with a collection class rather than individual values.
        /// </summary>
        /// <returns>A dictionary with the name/value pairs from the application settings.</returns>
        Dictionary<string, string> ReadAllSettings();
    }

    /// <summary>
    /// Factory interface for creation of implementations of IApplicationSettingsDriver.
    /// </summary>
    public interface IApplicationSettingsDriverFactory : ILqbFactory
    {
        /// <summary>
        /// Create an instance of an implementation of IApplicationSettingsDriver.
        /// </summary>
        /// <returns>An instance of an implementation of IApplicationSettingsDriver.</returns>
        IApplicationSettingsDriver Create();
    }
}
