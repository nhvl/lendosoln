﻿namespace LqbGrammar.Drivers.SecurityEventLogging
{
    /// <summary>
    /// Enum that defines the principal type that will be preserved even in "become-user" event.
    /// In the future, potentially add "InternalPlus" type that may be used to log extra information or to give extra permissions.
    /// </summary>
    public enum InitialPrincipalTypeT
    {
        /// <summary>
        /// Non internal user.
        /// </summary>
        NonInternal = 0,

        /// <summary>
        /// Internal user, potentially a through a become-user event, or just a user in LOAdmin.
        /// </summary>
        Internal = 1,

        /// <summary>
        /// System user for processes such as Task System, overnight pricing, etc.
        /// </summary>
        System = 2,
    }
}
