﻿namespace LqbGrammar.Drivers.SecurityEventLogging
{
    using System.Data;
    using System.Data.Common;

    /// <summary>
    /// Initializes new security event log driver factory.
    /// </summary>
    public interface ISecurityEventLogDriverFactory : ILqbFactory
    {
        /// <summary>
        /// Creates a new security event log driver.
        /// </summary>
        /// <returns>ISecurityEventLogDriver object.</returns>
        ISecurityEventLogDriver Create();
    }
}
