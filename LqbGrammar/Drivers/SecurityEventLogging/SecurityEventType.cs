﻿namespace LqbGrammar.Drivers.SecurityEventLogging
{
    /// <summary>
    /// Security event type.
    /// </summary>
    public enum SecurityEventType
    {
        /// <summary>
        /// All event types.
        /// </summary>
        AllEvents = 0,

        /// <summary>
        /// Account creation.
        /// </summary>
        AccountCreation = 1,

        /// <summary>
        /// Changing user settings.
        /// </summary>
        ChangingUserSettings = 2,

        /// <summary>
        /// Account locked due to too many incorrect attempts.
        /// </summary>
        AccountLocking = 3,

        /// <summary>
        /// Log in event.
        /// </summary>
        LogIn = 4,

        /// <summary>
        ///  Log out event.
        /// </summary>
        LogOut = 5,

        /// <summary>
        /// Internal LQB user becoming user.
        /// </summary>
        BecomeUser = 6,

        /// <summary>
        /// Sensitive report export.
        /// </summary>
        SensitiveReportExport = 7,

        /// <summary>
        /// Web service authentication.
        /// </summary>
        WebserviceAuthentication = 8,
        
        /// <summary>
        /// Failed log in event.
        /// </summary>
        FailedLogin = 9,

        /// <summary>
        /// Failed web service authentication.
        /// </summary>
        FailedWebserviceAuthentication = 10,
    }
}
