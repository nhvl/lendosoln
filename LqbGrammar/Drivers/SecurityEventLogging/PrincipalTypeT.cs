﻿namespace LqbGrammar.Drivers.SecurityEventLogging
{
    /// <summary>
    /// Principal type.
    /// </summary>
    public enum PrincipalTypeT
    {
        /// <summary>
        /// Unknown principal type.
        /// </summary>
        Unknown = 0,

        /// <summary>
        /// Lenders office principal type.
        /// </summary>
        LendersOffice = 1,

        /// <summary>
        /// Originator portal principal type.
        /// </summary>
        OriginatorPortal = 2,
        
        /// <summary>
        /// Consumer portal principal type.
        /// </summary>
        ConsumerPortal = 3,

        /// <summary>
        /// Internal principal type.
        /// </summary>
        Internal = 4,
    }
}
