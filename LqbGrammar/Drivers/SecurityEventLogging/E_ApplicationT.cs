﻿namespace LqbGrammar.Drivers.SecurityEventLogging
{
    /// <summary>
    /// Principal's application type.
    /// </summary>
    public enum E_ApplicationT
    {
        /// <summary>
        /// LendersOffice application.
        /// </summary>
        LendersOffice = 0,
        
        /// <summary>
        /// PriceMyLoan application.
        /// </summary>
        PriceMyLoan = 1,

        /// <summary>
        /// ConsumerPortal application.
        /// </summary>
        ConsumerPortal = 2,

        /// <summary>
        /// Application type will be defined from principal Type.
        /// </summary>
        ToBeDefineFromUserType = 3
    }
}
