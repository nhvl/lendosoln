﻿namespace LqbGrammar.Drivers.SecurityEventLogging
{
    /// <summary>
    /// Login source.
    /// </summary>
    public enum LoginSource
    {
        /// <summary>
        /// Logging in from the website.
        /// </summary>
        Website = 0,

        /// <summary>
        /// Logging in using webservice.
        /// </summary>
        Webservice = 1,
    }
}
