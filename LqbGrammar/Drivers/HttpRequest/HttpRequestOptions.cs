﻿namespace LqbGrammar.Drivers.HttpRequest
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Net;
    using System.Security.Cryptography.X509Certificates;
    using System.Xml;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Gather together the options that are available
    /// when making an http web request and parsing the
    /// response.
    /// </summary>
    public sealed class HttpRequestOptions
    {
        /// <summary>
        /// Don't read in responses longer than this value.
        /// </summary>
        public const long MaximumResponseLength = 0x10000000; // 10 MB

        /// <summary>
        /// The list of mime-types for the Accept header.
        /// </summary>
        private List<MimeType> acceptOptions;

        /// <summary>
        /// Collection of un-restricted headers to be added to the request.
        /// </summary>
        private NameValueCollection requestHeaders;

        /// <summary>
        /// Collection of cookies to be added to the request.
        /// </summary>
        private CookieContainer requestCookies;

        /// <summary>
        /// Initializes a new instance of the <see cref="HttpRequestOptions"/> class.
        /// </summary>
        public HttpRequestOptions()
        {
            // member data
            this.acceptOptions = null;
            this.requestHeaders = null;
            this.requestCookies = null;

            // post data
            this.PostData = null;

            // HttpWebRequest properties - I'm assigning the default values as documented at MSDN
            this.Method = HttpMethod.Get;
            this.KeepAlive = true;
            this.ContentLength = null;
            this.Encoding = null;
            this.ServicePointConnectionLimit = null;
            this.MaximumResponseHeadersLength = null;
            this.MimeType = null;
            this.PreAuthenticate = false;
            this.Timeout = TimeoutInSeconds.Create(100).Value;
            this.UserAgent = null;
            this.AllowAutoRedirect = true;
            this.MaximumAutomaticRedirections = 50;
            this.SetDecompressionMethods = false;
            this.SetAcceptDefaultLanguage = false;
            this.Credentials = null;
            this.UseHTTPProtocol10 = false;
            this.Referrer = null;
            this.ClientCertificates = null;

            // parsed HttpResponse
            this.IgnoreResponse = false;
            this.ResponseFileName = null;
            this.XmlResponseDelegate = null;
            this.XmlReaderSettings = null;
            this.ResponseBody = null;
            this.ResponseStatusCode = HttpStatusCode.InternalServerError; // 500
            this.ResponseStatusDescription = "NO STATUS CODE AVAILABLE";
            this.ResponseCookieContainer = new CookieContainer(); // Setting to null will signal that response cookies aren't desired.
            this.ResponseHeaders = new NameValueCollection(); // Setting to null will signal that response headers aren't desired.
        }

        /// <summary>
        /// Gets or sets the streamable post data for the request.
        /// </summary>
        public IStreamableContent PostData { get; set; }

        /// <summary>
        /// Gets or sets the path to which the response should be written,
        /// instead of adding the full response to the ResponseBody property.
        /// </summary>
        /// <value>The path to which the response should be written.</value>
        public LocalFilePath? ResponseFileName { get; set; }

        /// <summary>
        /// Gets or sets a delegate that will process xml from the response,
        /// instead of adding the full response to the ResponseBody property.
        /// </summary>
        /// <value>A delegate that will process xml from the response.</value>
        public Action<XmlReader> XmlResponseDelegate { get; set; }

        /// <summary>
        /// Gets or sets settings that influence how xml data is read from the response stream.
        /// </summary>
        /// <value>Settings that influence how xml data is read from the response stream.</value>
        public XmlReaderSettings XmlReaderSettings { get; set; }

        /// <summary>
        /// Gets or sets the path to write the response stream if the code
        /// decides not to return the data in memory, for whatever reason.
        /// </summary>
        /// <value>The path to write the response stream.</value>
        public LocalFilePath? ResponseDumpPath { get; set; }

        /// <summary>
        /// Gets or sets the http method for the request.
        /// If this property isn't set then the library's default will be used.
        /// </summary>
        /// <value>The http method for the request.</value>
        public HttpMethod Method { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether or not the KeepAlive header should be set.
        /// </summary>
        /// <value>Keep alive flag.</value>
        public bool KeepAlive { get; set; }

        /// <summary>
        /// Gets or sets the length of the request body.  If this property
        /// isn't set then the library will set it.  However, it may be
        /// advantageous to set explicitely so the library won't need to 
        /// wait for the entire post data to be written to the network
        /// stream prior to sending any of it across the wire.
        /// </summary>
        /// <value>The length of the request body.</value>
        public long? ContentLength { get; set; }

        /// <summary>
        /// Gets or sets the character encoding used for the request body, to be placed in the Content-Type header.
        /// If this property isn't set then the library's default will be used.
        /// </summary>
        /// <value>The character encoding used for the request body.</value>
        public CharacterEncoding? Encoding { get; set; }

        /// <summary>
        /// Gets or sets the maximum number of connections allowed on the ServicePoint object.  When null the default value is used.
        /// </summary>
        /// <value>The maximum number of connections allowed on the ServicePoint object.</value>
        public int? ServicePointConnectionLimit { get; set; }

        /// <summary>
        /// Gets or sets a value limiting the length of the headers section in the response.
        /// If this property isn't set then the library's default will be used.
        /// </summary>
        /// <value>The maximum length of the headers section of the response.</value>
        public int? MaximumResponseHeadersLength { get; set; }

        /// <summary>
        /// Gets or sets the mime-type to be placed in the Content-Type header.
        /// If this value isn't set then the library's default behavior will kick in.
        /// </summary>
        /// <value>The mime-type to be placed in the Content-Type header.</value>
        public MimeType MimeType { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to send an Authorization header with the request.
        /// </summary>
        /// <value>The PreAuthenticate flag.</value>
        public bool PreAuthenticate { get; set; }

        /// <summary>
        /// Gets or sets the timeout to use.
        /// </summary>
        /// <value>The timeout to use.</value>
        public TimeoutInSeconds Timeout { get; set; }

        /// <summary>
        /// Gets or sets the user agent to use.
        /// If this property isn't set then the library's default will be used.
        /// </summary>
        /// <value>The user agent to use.</value>
        public HttpUserAgent? UserAgent { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether AutoRedirect is allowed.
        /// </summary>
        /// <value>The AllowAutoRedirect flag.</value>
        public bool AllowAutoRedirect { get; set; }

        /// <summary>
        /// Gets or sets a value to notify the code how many times redirections
        /// may be followed prior to returning an error.
        /// </summary>
        /// <value>The maximum number of redirections that will be followed.</value>
        public int MaximumAutomaticRedirections { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether or not to indicate the response can be compressed.
        /// </summary>
        /// <value>Response compression flag.</value>
        public bool SetDecompressionMethods { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether or not to set the accept language header
        /// to US English, which is the only value used in the LQB code.
        /// </summary>
        /// <value>Flag indicating the default language should be set.</value>
        public bool SetAcceptDefaultLanguage { get; set; }

        /// <summary>
        /// Gets or sets the network credentials to use.
        /// </summary>
        /// <value>The network credentials to use.</value>
        public ICredentials Credentials { get; set; }

        /// <summary>
        /// Gets the collection of Accept mime-types.
        /// </summary>
        /// <value>The collection of Accept mime-types.</value>
        public List<MimeType> AcceptOptions
        {
            get
            {
                return this.acceptOptions;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to override the default protocol (1.1) to use 1.0 instead.
        /// </summary>
        /// <value>Protocol flag.</value>
        public bool UseHTTPProtocol10 { get; set; }

        /// <summary>
        /// Gets or sets the Referer(sic) header.
        /// </summary>
        /// <value>The Referer(sic) header.</value>
        public LqbHttpReferrer? Referrer { get; set; }

        /// <summary>
        /// Gets the collection of un-restricted header values.
        /// </summary>
        /// <value>The collection of un-restricted header values.</value>
        public NameValueCollection RequestHeaders
        {
            get
            {
                return this.requestHeaders;
            }
        }

        /// <summary>
        /// Gets the container of cookies to send.
        /// </summary>
        /// <value>The container of cookies.</value>
        public CookieContainer RequestCookieContainer
        {
            get
            {
                return this.requestCookies;
            }
        }

        /// <summary>
        /// Gets or sets the client certificates used in authentication
        /// by the target server.
        /// </summary>
        /// <value>The client certificates used in authentication.</value>
        public X509CertificateCollection ClientCertificates { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the response should
        /// be parsed or ignored.  The reason to ignore the response is
        /// because the calling code isn't going to use it.
        /// </summary>
        /// <value>Flag instructing code to not bother parsing the response.</value>
        public bool IgnoreResponse { get; set; }

        /// <summary>
        /// Gets or sets the body of the response.
        /// </summary>
        /// <value>The body of the response.</value>
        public string ResponseBody { get; set; }

        /// <summary>
        /// Gets or sets the container of cookies from the response.  It
        /// is an option because if it is null then the response cookies
        /// will not be gathered, but if is not null then the response
        /// cookies will be added to the container.
        /// </summary>
        /// <value>The container to receive response cookies.</value>
        public CookieContainer ResponseCookieContainer { get; set; }

        /// <summary>
        /// Gets or sets the response headers.  It is an options because
        /// if it is null then the response headers will not be gathered,
        /// but if it is not null then the response headers will be 
        /// added to the dictionary.
        /// </summary>
        /// <value>The response headers.</value>
        public NameValueCollection ResponseHeaders { get; set; }

        /// <summary>
        /// Gets or sets the response status code, which will always be 
        /// written here.
        /// </summary>
        /// <value>The response status code.</value>
        public HttpStatusCode ResponseStatusCode { get; set; }

        /// <summary>
        /// Gets or sets the response status description, which will
        /// always be written here.
        /// </summary>
        /// <value>The response status description.</value>
        public string ResponseStatusDescription { get; set; }

        /// <summary>
        /// Add a mime type to the Accept header.
        /// </summary>
        /// <param name="mimeType">The mime type to add.</param>
        public void AddAcceptOption(MimeType mimeType)
        {
            if (this.acceptOptions == null)
            {
                this.acceptOptions = new List<MimeType>();
            }

            this.acceptOptions.Add(mimeType);
        }

        /// <summary>
        /// An un-restricted http header value.  No validation is done
        /// here.  Instead, if a restricted header gets added then 
        /// the .net framework will throw an exception when it 
        /// receives the errant value.
        /// </summary>
        /// <param name="name">The name of the header.</param>
        /// <param name="value">The value of the header.</param>
        public void AddHeader(string name, string value)
        {
            if (this.requestHeaders == null)
            {
                this.requestHeaders = new NameValueCollection();
            }

            this.requestHeaders.Add(name, value);
        }

        /// <summary>
        /// Add a cookie to the request.
        /// </summary>
        /// <param name="url">The url to which the cookie will be sent.</param>
        /// <param name="name">The cookie's name.</param>
        /// <param name="value">The cookie's value.</param>
        public void AddCookie(string url, string name, string value)
        {
            if (this.requestCookies == null)
            {
                this.requestCookies = new CookieContainer();
            }

            var uri = new Uri(url);
            string domain = uri.Authority;

            this.requestCookies.Add(new Cookie(name, value, string.Empty, domain));
        }
    }
}
