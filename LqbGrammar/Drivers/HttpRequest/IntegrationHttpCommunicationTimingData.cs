﻿namespace LqbGrammar.Drivers.HttpRequest
{
    /// <summary>
    /// Provides additional timing data for HTTP requests.
    /// </summary>
    public class IntegrationHttpCommunicationTimingData
    {
        /// <summary>
        /// Gets or sets the log category for the timing data.
        /// </summary>
        public string LogCategory { get; set; }

        /// <summary>
        /// Gets or sets the log message for the timing data.
        /// </summary>
        public string LogMessage { get; set; }
    }
}
