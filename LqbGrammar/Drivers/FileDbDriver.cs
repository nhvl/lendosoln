﻿namespace LqbGrammar.Drivers
{
    using System;
    using DataTypes;

    /// <summary>
    /// When a stored file is to be modified and re-stored, use 
    /// this interface to provide the local filename in a 
    /// re-usable way.  The idea is to get the client away 
    /// from using FileDB3.FileHandle because the Dispose
    /// method is then put at risk.
    /// </summary>
    public interface IFileToken
    {
        /// <summary>
        /// Gets the local file name that the file token represents.
        /// </summary>
        /// <value>The path on the local machine that contains the file.</value>
        LocalFilePath LocalFileName { get; }
    }

    /// <summary>
    /// Interface used by a driver to look at the inner workings of 
    /// an IFileToken implementation. This is necessary to determine 
    /// if and when a new encryption key needs to be calculated when 
    /// storing a file in an alternate location.  This is an ugly 
    /// approach but was the best I could think.
    /// </summary>
    public interface IMeasureFileToken
    {
        /// <summary>
        /// Gets the identifier for the FileDB storage location.
        /// </summary>
        /// <value>The identifier for the FileDB storage location.</value>
        FileStorageIdentifier StorageIdentifier { get; }

        /// <summary>
        /// Gets the identifier for the FileDB file.
        /// </summary>
        /// <value>The identifier for the FileDB file.</value>
        FileIdentifier FileIdentifier { get; }

        /// <summary>
        /// Gets the encryption key for the FileDB file.
        /// </summary>
        /// <value>The encryption key for the FileDB file.</value>
        byte[] EncryptionKey { get; }
    }

    /// <summary>
    /// Interface to work with the FileDB system.
    /// </summary>
    public interface IFileDbDriver : ILqbDriver
    {
        /// <summary>
        /// Save a new file into the file storage.
        /// </summary>
        /// <param name="location">The location of the file storage repository.</param>
        /// <param name="key">The file identifier used to retrieve the file later.</param>
        /// <param name="saveFileHandler">A delegate which will save the file to the location provided in the LocalFilePath.</param>
        void SaveNewFile(FileStorageIdentifier location, FileIdentifier key, Action<LocalFilePath> saveFileHandler);

        /// <summary>
        /// Retrieve a file from the file storage.
        /// </summary>
        /// <param name="location">The location of the file storage repository.</param>
        /// <param name="key">The file identifier previously used to save the file.</param>
        /// <param name="readFileHandler">A delegate which will read the file from the location provided in the LocalFilePath.</param>
        void RetrieveFile(FileStorageIdentifier location, FileIdentifier key, Action<LocalFilePath> readFileHandler);

        /// <summary>
        /// Retrieve a file from the file storage and return a token that can be used to re-store the file.
        /// </summary>
        /// <param name="location">The location of the file storage repository.</param>
        /// <param name="key">The file identifier previously used to save the file.</param>
        /// <param name="useFileHandler">A delegate which will read the file from the location provided in the IFileToken, and can re-store the file with this token.</param>
        void UseFile(FileStorageIdentifier location, FileIdentifier key, Action<IFileToken> useFileHandler);

        /// <summary>
        /// Save a file that was previously retrieved from the file storage using the UseFile method.
        /// </summary>
        /// <param name="location">The location of the file storage repository.</param>
        /// <param name="key">The file identifier used to retrieve the file later, this can be the same as or different from the key passed into the UseFile method.</param>
        /// <param name="token">A file token that holds the location of the file in the local environment.</param>
        void ReSaveFile(FileStorageIdentifier location, FileIdentifier key, IFileToken token);

        /// <summary>
        /// Delete the file from the file storage.
        /// </summary>
        /// <param name="location">The location of the file storage repository.</param>
        /// <param name="key">The file identifier used to delete.</param>
        void DeleteFile(FileStorageIdentifier location, FileIdentifier key);

        /// <summary>
        /// Check whether the given file exist in the file storage.
        /// </summary>
        /// <param name="location">The location of the file storage repository.</param>
        /// <param name="key">The file identifier.</param>
        /// <returns>True if the file exist false otherwise.</returns>
        bool FileExists(FileStorageIdentifier location, FileIdentifier key);
    }

    /// <summary>
    /// Interface used to create an implementation of the IFileDbDriver interface.
    /// </summary>
    public interface IFileDbDriverFactory : ILqbFactory
    {
        /// <summary>
        /// Create an implementation of the IFileDbDriver interface.
        /// </summary>
        /// <returns>An implementation of the IFileDbDriver interface.</returns>
        IFileDbDriver Create();
    }
}
