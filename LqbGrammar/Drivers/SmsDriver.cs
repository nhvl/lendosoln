﻿namespace LqbGrammar.Drivers
{
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Driver for sending SMS messages.  Each message type will get its own method
    /// to make message content validation easier to manage.
    /// </summary>
    public interface ISmsDriver : ILqbDriver
    {
        /// <summary>
        /// Send an authentication code message via SMS.
        /// </summary>
        /// <param name="number">The phone number to which the SMS message will be sent.</param>
        /// <param name="message">A message containing an LQB authentication code.</param>
        /// <returns>True if OK response recieved, false otherwise.</returns>
        bool SendAuthenticationCode(PhoneNumber number, SmsAuthCodeMessage message);
    }

    /// <summary>
    /// Factory for creating implementations of the ISmsDriver interace.
    /// </summary>
    public interface ISmsDriverFactory : ILqbFactory
    {
        /// <summary>
        /// Create an implementation of the ISmsDriver interface.
        /// </summary>
        /// <returns>An implementation of the ISmsDriver interface.</returns>
        ISmsDriver Create();
    }
}
