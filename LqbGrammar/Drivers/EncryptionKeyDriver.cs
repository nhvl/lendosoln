﻿namespace LqbGrammar.Drivers
{
    using System.Collections.Generic;
    using DataTypes;

    /// <summary>
    /// Interface used to manage encryption keys.
    /// </summary>
    public interface IEncryptionKeyDriver : ILqbDriver
    {
        /// <summary>
        /// Gets an encryption key by its identifier.
        /// </summary>
        /// <param name="identifier">The identifier.</param>
        /// <returns>The key associated with the identifier.</returns>
        EncryptionKey GetKey(EncryptionKeyIdentifier identifier);

        /// <summary>
        /// Generates a new encryption key and returns the key identifier and
        /// the key.
        /// </summary>
        /// <returns>The new key identifier and the key.</returns>
        KeyValuePair<EncryptionKeyIdentifier, EncryptionKey> GenerateKey();
    }

    /// <summary>
    /// Factory interface used to create implementations of the IEncryptionKeyDriver interface.
    /// </summary>
    public interface IEncryptionKeyDriverFactory : ILqbFactory
    {
        /// <summary>
        /// Create an implementation of the IEncryptionKeyDriver interface.
        /// </summary>
        /// <returns>An implementation of the IEncryptionKeyDriver interface.</returns>
        IEncryptionKeyDriver Create();
    }
}
