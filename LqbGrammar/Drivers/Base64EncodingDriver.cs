﻿namespace LqbGrammar.Drivers
{
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Defines the interface for encoding and decoding base64.
    /// </summary>
    public interface IBase64EncodingDriver : ILqbDriver
    {
        /// <summary>
        /// Encode the value in base64.
        /// </summary>
        /// <param name="unencodedData">The value to be encoded.</param>
        /// <returns>The base64 encoded value.</returns>
        Base64EncodedData Encode(byte[] unencodedData);

        /// <summary>
        /// Decode the value in base64.
        /// </summary>
        /// <param name="base64EncodedData">The encoded value.</param>
        /// <returns>The decoded value.</returns>
        byte[] Decode(Base64EncodedData base64EncodedData);
    }

    /// <summary>
    /// Factory for creating an implementation of IBase64EncodingDriver.
    /// </summary>
    public interface IBase64EncodingDriverFactory : ILqbFactory
    {
        /// <summary>
        /// Create and return an implementation of IBase64EncodingDriver.
        /// </summary>
        /// <returns>An implementation of IBase64EncodingDriver.</returns>
        IBase64EncodingDriver Create();
    }
}
