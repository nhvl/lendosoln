﻿Interfaces that inherit from ILqbDriver will be defined in this folder.

These interfaces will be implemented by classes in the business modules
of the LendOSoln and their factories will be registered with the service
locator.  These classes are charged with translating the higher-level
business logic concepts into low-level semantic data types and then
calling into ILqbAdapter interfaces.  Depending upon the specific scenario,
these classes may also add some business logic of their own.  For example,
a driver that calls over the internet to a data provider may wish to do
multiple retries in the face of connection failures.

Normally the language of these interfaces will consist of semantic
data types and not types provided by the .net framework.  However,
for the SqlDriver an exception to this rule is made.  Normally
we'd want to create adapters around the classes in the System.Data.SqlClient
namespace so we can switch-out our database provider in the future.
Since we are planning to move from locally hosted instances of SQL Server
to Azure-hosted SQL Server, we are committing to SQL Server for
several years in the forseeable future.  Given that commitment, we
aren't going to bother with the complexity of a new set of adapters.
Our SqlDriver will be defined using SqlClient classes.
