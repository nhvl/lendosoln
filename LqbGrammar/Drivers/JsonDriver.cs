﻿namespace LqbGrammar.Drivers
{
    using System.IO;

    /// <summary>
    /// There are several independent serialization implemenations, encapsulated here.
    /// </summary>
    public enum JsonSerializer
    {
        /// <summary>
        /// The default value (currently Newtonsoft.JsonSerializer).
        /// </summary>
        Default,

        /// <summary>
        /// Use a data contract, built into attributes on the target type's fields.
        /// </summary>
        DataContract,

        /// <summary>
        /// A flexible conversion tool with multiple options.
        /// </summary>
        JsonConvert,

        /// <summary>
        /// Used when interacting with javascript.
        /// </summary>
        Javascript
    }

    /// <summary>
    /// Options to control the serialization to and from JSON.
    /// </summary>
    public enum JsonSerializerOptions
    {
        /// <summary>
        /// No options, just use a standard implementation for a serializer.
        /// </summary>
        None,

        /// <summary>
        /// Used with JsonSerializer.JsonConvert, assigns Settings.TypeNameHandling to All.
        /// </summary>
        TypeNameHandling_All,

        /// <summary>
        /// Used with JsonSerializer.JsonConvert, uses Indenting and also a conversion of enumeration values to their string equivalents.
        /// </summary>
        IndentedPlusStringEnumConverter,
    }

    /// <summary>
    /// Interface used to serialize/deserialize objects to/from JSON.
    /// </summary>
    public interface IJsonDriver : ILqbDriver
    {
        /// <summary>
        /// Serialize an object of type T to a JSON string.
        /// </summary>
        /// <typeparam name="T">The type of the object to be serialized.</typeparam>
        /// <param name="forSerialization">An instance of the type to serialize.</param>
        /// <param name="sanitize">True if the output json should be sanitized.</param>
        /// <returns>The serialization of the instance to a JSON string.</returns>
        string Serialize<T>(T forSerialization, bool sanitize) where T : class, new();

        /// <summary>
        /// Serialize an object of type T to a JSON string.
        /// </summary>
        /// <typeparam name="T">The type of the object to be serialized.</typeparam>
        /// <typeparam name="R">The type used as the resolver.</typeparam>
        /// <param name="forSerialization">An instance of the type to serialize.</param>
        /// <param name="resolver">A class that is used for type resolution during serialization.</param>
        /// <param name="sanitize">True if the output json should be sanitized.</param>
        /// <returns>The serialization of the instance to a JSON string.</returns>
        string Serialize<T, R>(T forSerialization, R resolver, bool sanitize) where T : class, new() where R : class;

        /// <summary>
        /// Deserialize a JSON string back to an instance of the indicated type.
        /// </summary>
        /// <typeparam name="T">The type of the object to be deserialize.</typeparam>
        /// <param name="serialized">The JSON serialization of an instance of the target type.</param>
        /// <param name="sanitize">True if the input json should be sanitized.</param>
        /// <returns>The deserialized instance of the target type.</returns>
        T Deserialize<T>(string serialized, bool sanitize) where T : class, new();

        /// <summary>
        /// Deserialize a JSON string back to an instance of the indicated type.
        /// </summary>
        /// <typeparam name="T">The type of the object to be deserialize.</typeparam>
        /// <typeparam name="R">The type used as the converter.</typeparam>
        /// <param name="reader">The JSON serialization of an instance of the target type.</param>
        /// <param name="converter">The converter used during deserialization.</param>
        /// <returns>The deserialized instance of the target type.</returns>
        T Deserialize<T, R>(StreamReader reader, R converter) where T : class, new() where R : class;
    }

    /// <summary>
    /// Factory for creation of implementations of IJsonDriver.
    /// </summary>
    public interface IJsonDriverFactory : ILqbFactory
    {
        /// <summary>
        /// Create an instance of a class that implements the IJsonDriver interface.
        /// </summary>
        /// <param name="serializer">The type of serializer to use.</param>
        /// <param name="options">Options that control the serializer's behavior.</param>
        /// <returns>An implementation of IJsonDriver.</returns>
        IJsonDriver Create(JsonSerializer serializer, JsonSerializerOptions options);
    }
}
