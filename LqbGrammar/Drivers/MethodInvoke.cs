﻿namespace LqbGrammar.Drivers
{
    using System;
    using System.Linq.Expressions;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Any class that contains a method that will be called via the IMethodInvokeDriver
    /// needs to implement this interface.
    /// </summary>
    /// <typeparam name="T">Type of the class that contains the method.</typeparam>
    /// <typeparam name="S">Type of the argument to the method.</typeparam>
    public interface IMethodInvokeClass<T, S> where T : new()
    {
        /// <summary>
        /// Prepare for a streamlined method call.
        /// </summary>
        /// <returns>The streamlined method call.</returns>
        SingleArgumentMethod<T, S> GetMethod();
    }

    /// <summary>
    /// Driver interface for invoking a method.
    /// </summary>
    public interface IMethodInvokeDriver : ILqbDriver
    {
        /// <summary>
        /// Determine whether or not the call will be made directly or done on a different thread.
        /// This will allow the client code to make efficient calls when a direct call is made.
        /// </summary>
        /// <returns>True if the method will be called using a different thread, rather than directly.</returns>
        bool InvokeIsOffThread();

        /// <summary>
        /// Execute a method that accepts an argument of type S and returns void.
        /// </summary>
        /// <typeparam name="T">Type of the class that contains the method.</typeparam>
        /// <typeparam name="S">Type of the argument to the method.</typeparam>
        /// <param name="call">The method that will get invoked.</param>
        /// <param name="previousCallId">An identifier that links this call to the end of a previous call, or null.</param>
        /// <returns>A job identifier, or null if there is no identifier.</returns>
        MethodInvokeIdentifier Run<T, S>(SingleArgumentMethod<T, S> call, MethodInvokeIdentifier previousCallId) where T : new();

        /// <summary>
        /// After waiting for a delay time, execute a method that accepts an argument of type S and returns void.
        /// </summary>
        /// <typeparam name="T">Type of the class that contains the method.</typeparam>
        /// <typeparam name="S">Type of the argument to the method.</typeparam>
        /// <param name="call">The method that will get invoked.</param>
        /// <param name="delay">The delay time to wait prior to executing the method.</param>
        /// <returns>A job identifier, or null if there is no identifier.</returns>
        MethodInvokeIdentifier RunAfterDelay<T, S>(SingleArgumentMethod<T, S> call, TimeoutInSeconds delay) where T : new();
    }

    /// <summary>
    /// Factory interface for creating implementations of the IMethodInvokeDriver interface.
    /// </summary>
    public interface IMethodInvokeDriverFactory : ILqbFactory
    {
        /// <summary>
        /// Create an implementation of the IMethodInvokeDriver interface.
        /// </summary>
        /// <returns>An implementation of the IMethodInvokeDriver interface.</returns>
        IMethodInvokeDriver Create();
    }

    /// <summary>
    /// Encapsulate the notion of a single parameter method call.
    /// It is assumed that the action delegate calls a static method.
    /// </summary>
    /// <typeparam name="T">The type of the class that contains the method.</typeparam>
    /// <typeparam name="S">The type of the argument to the method.</typeparam>
    public struct SingleArgumentMethod<T, S> where T : new()
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SingleArgumentMethod{T,S}"/> struct.
        /// </summary>
        /// <param name="expression">The expression that will be invoked.</param>
        /// <param name="argumentValue">The value of the argument to the method.</param>
        public SingleArgumentMethod(Expression<Action<T>> expression, S argumentValue)
        {
            this.Argument = argumentValue;
            this.Expression = expression;
        }

        /// <summary>
        /// Gets the value for the argument passed to the expression.
        /// </summary>
        /// <value>The value for the argument passed to the expression.</value>
        public S Argument { get; private set; }

        /// <summary>
        /// Gets the expression that will be invoked.
        /// </summary>
        /// <value>The expression that will be invoked.</value>
        public Expression<Action<T>> Expression { get; private set; }

        /// <summary>
        /// Utility method to execute the method contained by a SingleArgumentMethod.
        /// </summary>
        /// <param name="call">The SingleArgumentMethod that contains the method to be executed.</param>
        public static void Execute(SingleArgumentMethod<T, S> call)
        {
            T instance = new T();

            var lambda = (LambdaExpression)call.Expression;
            var del = lambda.Compile();

            del.DynamicInvoke(instance);
        }
    }
}
