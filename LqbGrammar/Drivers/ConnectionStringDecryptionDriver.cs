﻿namespace LqbGrammar.Drivers
{
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Interface for decrypting connection strings.
    /// </summary>
    public interface IConnectionStringDecryptionDriver : ILqbDriver
    {
        /// <summary>
        /// Decrypts the given connection string.
        /// </summary>
        /// <param name="encryptedConnectionString">The encrypted connection string.</param>
        /// <param name="decryptedConnectionString">The resulting decrypted connection string.</param>
        /// <returns>True if decryption was successful.  Otherwise false.</returns>
        bool TryDecryptConnectionString(DBConnectionString encryptedConnectionString, out DBConnectionString decryptedConnectionString);
    }

    /// <summary>
    /// Factory interface used to create implementations of the IConnectionStringDecryptionDriver interface.
    /// </summary>
    public interface IConnectionStringDecryptionDriverFactory : ILqbFactory
    {
        /// <summary>
        /// Create an implementation of the IConnectionStringDecryptionDriver interface.
        /// </summary>
        /// <param name="configFileLocation">The expected location of the config file.</param>
        /// <returns>An implementation of the IConnectionStringDecryptionDriver interface.</returns>
        IConnectionStringDecryptionDriver CreateXmlFileDecrypter(LocalFilePath configFileLocation);
    }
}
