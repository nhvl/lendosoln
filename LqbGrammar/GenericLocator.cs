﻿namespace LqbGrammar
{
    using LqbGrammar.DataTypes;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// This is our implementation of the locator service, a
    /// very simple IoC container.  Factories will be registered
    /// here and the actual implementation classes will be created
    /// using these factories.  That way the initialization code
    /// should be very fast as it is merely the instantiation of
    /// a set of stateless objects.
    /// </summary>
    /// <typeparam name="T">A factory class.</typeparam>
    public static class GenericLocator<T> where T : class
    {
        /// <summary>
        /// Factory objects are registered and stored in this field.
        /// </summary>
        private static T factory;

        /// <summary>
        /// Gets a previously registed factory.
        /// </summary>
        /// <value>The factory.</value>
        public static T Factory
        {
            get
            {
                T factory = GenericLocator<T>.factory;
                if (factory == null && LqbApplication.Current.Type != ApplicationType.UnitTest)
                {
                    throw new DeveloperException(ErrorMessage.SystemError);
                }

                return factory;
            }
        }

        /// <summary>
        /// During initialization an exception may occur prior to registration
        /// of all the interfaces.  The exception handlers will call this 
        /// method which returns null when the requested interface isn't available.
        /// While exposed as public, this method should only be called by unit
        /// tests from outside the LqbGrammar module.
        /// </summary>
        /// <remarks>
        /// It is assumed that all interfaces will be registered once the initialization
        /// process is complete and so there will never be a null returned afterward.
        /// Accordingly, we aren't going to do any checking for the status of the 
        /// initialization (e.g., checking LqbApplication.Current for null).
        /// </remarks>
        /// <returns>The factory, or null.</returns>
        public static T GetFactoryDuringInitialization()
        {
            return GenericLocator<T>.factory;
        }

        /// <summary>
        /// Register a factory for later retrieval.
        /// method.  
        /// </summary>
        /// <param name="registrationTarget">The factory being registered.</param>
        internal static void RegisterFactory(T registrationTarget)
        {
            factory = registrationTarget;
        }
    }
}
