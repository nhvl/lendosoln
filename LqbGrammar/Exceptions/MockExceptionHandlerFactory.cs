﻿namespace LqbGrammar.Exceptions
{
#if DEBUG
    /// <summary>
    /// Factory used for creating mock exception handlers.
    /// </summary>
    public sealed class MockExceptionHandlerFactory : IExceptionHandlerFactory<MockException>
    {
        // It is the factory that is recoverable after exceptions are handled
        // so we have to store the exception data here for later processing in tests

        /// <summary>
        /// Error message from the exception handler.
        /// </summary>
        private string message;

        /// <summary>
        /// Error context from the exception handler.
        /// </summary>
        private ErrorContext context;

        /// <summary>
        /// Gets the error message.
        /// </summary>
        /// <value>The error message.</value>
        public string Message
        {
            get { return this.message; }
        }

        /// <summary>
        /// Gets the error context.
        /// </summary>
        /// <value>The error context.</value>
        public ErrorContext Context
        {
            get { return this.context; }
        }

        /// <summary>
        /// Register this factory instance with the locator service.
        /// </summary>
        public void AutoRegister()
        {
            // Note that we want mock handlers to be cleared after unit tests,
            // so don't use RegisterPermanentFactory()
            GenericLocator<IExceptionHandlerFactory<MockException>>.RegisterFactory(this);
        }

        /// <summary>
        /// Create an exception handler instance.
        /// </summary>
        /// <returns>The exception handler.</returns>
        public IExceptionHandler<MockException> Create()
        {
            return new MockExceptionHandler(this);
        }

        /// <summary>
        /// Set the error message and context for later inspection by unit test code.
        /// </summary>
        /// <param name="message">The error message.</param>
        /// <param name="context">The error context.</param>
        internal void SetData(string message, ErrorContext context)
        {
            this.message = message;
            this.context = context;
        }
    }
#endif
}
