﻿namespace LqbGrammar.Exceptions
{
    using System.Collections.Generic;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// The context for a validation error, containing one or more error messages for the user with details about what input was invalid.
    /// </summary>
    public class ValidationErrorContext : ErrorContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ValidationErrorContext"/> class.
        /// </summary>
        /// <param name="errorMessages">The error messages for a validation error.</param>
        public ValidationErrorContext(IReadOnlyList<ValidationErrorMessage> errorMessages)
        {
            if (errorMessages.Count == 0)
            {
                throw new DeveloperException(ErrorMessage.SystemError);
            }

            this.ValidationErrors = errorMessages;
        }

        /// <summary>
        /// Gets the list of validation errors.
        /// </summary>
        /// <value>The list of validation errors.</value>
        public IReadOnlyList<ValidationErrorMessage> ValidationErrors { get; }
    }
}
