﻿namespace LqbGrammar.Exceptions
{
    /// <summary>
    /// Factory for creating a handler for server exceptions.
    /// </summary>
    internal sealed class ServerExceptionHandlerFactory : IExceptionHandlerFactory<ServerException>
    {
        /// <summary>
        /// Register this factory with the locator service.
        /// </summary>
        public void AutoRegister()
        {
            GenericLocator<IExceptionHandlerFactory<ServerException>>.RegisterFactory(this);
        }

        /// <summary>
        /// Create a handler for a server exception.
        /// </summary>
        /// <returns>An exception handler.</returns>
        public IExceptionHandler<ServerException> Create()
        {
            return new ServerExceptionHandler();
        }
    }
}
