﻿namespace LqbGrammar.Exceptions
{
    /// <summary>
    /// Sometimes error messages will need data injected into them via a string.Format() construction.
    /// This class holds these values.
    /// </summary>
    internal static class ErrorMessageTemplates
    {
        /// <summary>
        /// Gets the message template used when there is a failure to populate a DataSet.
        /// </summary>
        public static string FillDataSetAccessFailed
        {
            get { return "Db access error in LoggingDriver.FillDataSet(). Sql statement is = {0}"; }
        }

        /// <summary>
        /// Gets the message template to be used when the user lacks permission. <para/>
        /// 0 = operation type, e.g. "reply to"; <para></para>
        /// 1 = resource type, e.g. "conversation with permission level id: x"; <para></para>
        /// </summary>
        public static string PermissionDenied
        {
            get
            {
                return "You lack permission to {0} the {1}.";
            }
        }

        /// <summary>
        /// Gets the message template used when there are duplicates in the call to the methodname. <para/>
        /// 0 = methodName e.g. SetCategoryOrder<para/>
        /// 1 = objectName e.g. category <para/> 
        /// 2 = ownerId.
        /// </summary>
        public static string ListContainsDuplicates
        {
            get { return "Duplicates existed in the list of {1} instances for owner {2} when calling {0}."; }
        }

        /// <summary>
        /// Gets the message template used when the call to the method does not contain one of the existing object instances for the owner. <para/>
        /// 0 = methodName e.g. SetCategoryOrder <para/>
        /// 1 = objectName e.g. category <para/>
        /// 2 = objectId e.g. category id <para/> 
        /// 3 = ownerId.
        /// </summary>
        public static string IsMissingObjectInstance
        {
            get { return "When calling {0}, a {1} with id: {2} existed for owner {3} but was not passed in."; }
        }

        /// <summary>
        /// Gets the message template used when the call to method has an object with id not belonging to the owner.
        /// 0 = methodName e.g. SetCategoryOrder <para/>
        /// 1 = objectName e.g. category <para/>
        /// 2 = object id e.g. category id <para/>
        /// 3 = ownerId.
        /// </summary>
        public static string HasExtraObject
        {
            get { return "When calling {0} user passed in {1} with id: {2} which did not belong to owner: {3}."; }
        }

        /// <summary>
        /// Gets the message template used when we could not find the {0} specified.<para/>
        /// 0 = the thing not found.
        /// </summary>
        public static string NotFound
        {
            get
            {
                return "Could not find the specified {0}.";
            }
        }

        /// <summary>
        /// Gets the message template used when an invalid value is detected.
        /// 0 = Field name,
        /// 1 = Received value.
        /// </summary>
        public static string InvalidValue
        {
            get { return "Invalid value for field {0}: {1}."; }
        }
    }
}
