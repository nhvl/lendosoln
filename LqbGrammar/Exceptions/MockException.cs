﻿namespace LqbGrammar.Exceptions
{
#if DEBUG
    using System;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Mock exception that can be used in unit tests.
    /// </summary>
    public sealed class MockException : LqbException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MockException"/> class.
        /// </summary>
        /// <param name="error">The error message.</param>
        public MockException(ErrorMessage error)
            : base(error)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MockException"/> class.
        /// </summary>
        /// <param name="error">The error message.</param>
        /// <param name="inner">Contained inner exception.</param>
        public MockException(ErrorMessage error, Exception inner)
            : base(error, inner)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MockException"/> class.
        /// </summary>
        /// <param name="error">The error message.</param>
        /// <param name="context">The error context.</param>
        public MockException(ErrorMessage error, ErrorContext context)
            : base(error, context)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MockException"/> class.
        /// </summary>
        /// <param name="error">The error message.</param>
        /// <param name="context">The error context.</param>
        /// <param name="inner">Contained inner exception.</param>
        public MockException(ErrorMessage error, ErrorContext context, Exception inner)
            : base(error, context, inner)
        {
        }

        /// <summary>
        /// Pass this exception to the appropriate handler.
        /// </summary>
        public override void PassToHandler()
        {
            ExceptionHandlerUtil.HandleException<MockException>(this);
        }
    }
#endif
}
