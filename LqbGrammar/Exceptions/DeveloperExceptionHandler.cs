﻿namespace LqbGrammar.Exceptions
{
    using LqbGrammar.Drivers;

    /// <summary>
    /// Use this when some problem is the result of poor coding or configuration.
    /// </summary>
    internal sealed class DeveloperExceptionHandler : IExceptionHandler<DeveloperException>
    {
        /// <summary>
        /// Handle a developer exception.
        /// </summary>
        /// <remarks>
        /// Although this may be called at any time, during initialization it is 
        /// possible for a factory to not yet have been registered.  Accordingly, 
        /// we use the method that returns null rather than throw an exception 
        /// when the requested factory is unavailable.
        /// </remarks>
        /// <param name="exception">Exception to be handled.</param>
        public void HandleException(DeveloperException exception)
        {
            var factory = GenericLocator<ILoggingDriverFactory>.GetFactoryDuringInitialization();
            if (factory != null)
            {
                var driver = factory.CreateExceptionLogger();
                driver.LogException(exception);
            }
        }
    }
}
