﻿namespace LqbGrammar.Exceptions
{
    using System;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Exception thrown when a server error occurs.
    /// </summary>
    public sealed class ServerException : LqbException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ServerException"/> class.
        /// </summary>
        /// <param name="error">The error message.</param>
        public ServerException(ErrorMessage error)
            : base(error)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ServerException"/> class.
        /// </summary>
        /// <param name="error">The error message.</param>
        /// <param name="inner">Contained inner exception.</param>
        public ServerException(ErrorMessage error, Exception inner)
            : base(error, inner)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ServerException"/> class.
        /// </summary>
        /// <param name="error">The error message.</param>
        /// <param name="context">The error context.</param>
        public ServerException(ErrorMessage error, ErrorContext context)
            : base(error, context)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ServerException"/> class.
        /// </summary>
        /// <param name="error">The error message.</param>
        /// <param name="context">The error context.</param>
        /// <param name="inner">Contained inner exception.</param>
        public ServerException(ErrorMessage error, ErrorContext context, Exception inner)
            : base(error, context, inner)
        {
        }

        /// <summary>
        /// Pass this exception instance to the appropriate handler.
        /// </summary>
        public override void PassToHandler()
        {
            ExceptionHandlerUtil.HandleException<ServerException>(this);
        }
    }
}
