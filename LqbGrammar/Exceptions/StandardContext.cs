﻿namespace LqbGrammar.Exceptions
{
    using System.Collections.Generic;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Context that implements the standard log properties.
    /// </summary>
    public sealed class StandardContext : ErrorContext
    {
        /// <summary>
        /// Gets or sets the value that is represented as "RequestUniqueId" in the standard logging properties.
        /// </summary>
        /// <value>The value that is represented as "RequestUniqueId" in the standard logging properties.</value>
        public string RequestUniqueId { get; set; }

        /// <summary>
        /// Gets or sets the value that is represented as "Category" in the standard logging properties.
        /// </summary>
        /// <value>The value that is represented as "Category" in the standard logging properties.</value>
        public string Category { get; set; }

        /// <summary>
        /// Gets or sets the value that is represented as "sLId" in the standard logging properties.
        /// </summary>
        /// <value>The value that is represented as "sLId" in the standard logging properties.</value>
        public string LoanId { get; set; }

        /// <summary>
        /// Gets or sets the value that is represented as "CorrelationId" in the standard logging properties.
        /// </summary>
        /// <value>The value that is represented as "CorrelationId" in the standard logging properties.</value>
        public string CorrelationId { get; set; }

        /// <summary>
        /// Gets or sets the value that is represented as "ClientIp" in the standard logging properties.
        /// </summary>
        /// <value>The value that is represented as "ClientIp" in the standard logging properties.</value>
        public string ClientIp { get; set; }

        /// <summary>
        /// Gets or sets the value that is represented as "UniqueClientIdCookie" in the standard logging properties.
        /// </summary>
        /// <value>The value that is represented as "UniqueClientIdCookie" in the standard logging properties.</value>
        public string UniqueClientIdCookie { get; set; }

        /// <summary>
        /// Gets or sets the value that is represented as "error_unique_id" in the standard logging properties.
        /// </summary>
        /// <value>The value that is represented as "error_unique_id" in the standard logging properties.</value>
        public string ErrorUniqueId { get; set; }

        /// <summary>
        /// Exception handlers will call this method first to get the context
        /// information as a structured set of properties.  If this returns 
        /// null then the Serialize() method will be called.
        /// </summary>
        /// <returns>Structured set of properties.</returns>
        public override Dictionary<LogPropertyName, LogPropertyValue> GetProperties()
        {
            var properties = new Dictionary<LogPropertyName, LogPropertyValue>();

            this.TestAndHandleProperty("RequestUniqueId", this.RequestUniqueId, properties);
            this.TestAndHandleProperty("Category", this.Category, properties);
            this.TestAndHandleProperty("sLId", this.LoanId, properties);
            this.TestAndHandleProperty("CorrelationId", this.CorrelationId, properties);
            this.TestAndHandleProperty("ClientIp", this.ClientIp, properties);
            this.TestAndHandleProperty("UniqueClientIdCookie", this.UniqueClientIdCookie, properties);
            this.TestAndHandleProperty("error_unique_id", this.ErrorUniqueId, properties);

            return properties;
        }

        /// <summary>
        /// If the input value is populated, add it to the dictionary.
        /// </summary>
        /// <param name="name">Name of the property.</param>
        /// <param name="value">The property's value.</param>
        /// <param name="properties">Collection of properties.</param>
        private void TestAndHandleProperty(string name, string value, Dictionary<LogPropertyName, LogPropertyValue> properties)
        {
            if (!string.IsNullOrEmpty(value))
            {
                this.CreateAndAddProperty(name, value, properties);
            }
        }

        /// <summary>
        /// Encapsulate the arguments into semantic data types and add the property to the dictionary.
        /// </summary>
        /// <param name="name">Name of the property.</param>
        /// <param name="value">The property's value.</param>
        /// <param name="properties">Collection of properties.</param>
        private void CreateAndAddProperty(string name, string value, Dictionary<LogPropertyName, LogPropertyValue> properties)
        {
            var propName = LogPropertyName.Create(name).Value;
            var propValue = LogPropertyValue.Create(value);
            if (propValue != null)
            {
                properties[propName] = propValue.Value;
            }
        }
    }
}
