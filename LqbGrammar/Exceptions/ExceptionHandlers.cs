﻿namespace LqbGrammar.Exceptions
{
    using System;

    /// <summary>
    /// Create an exception handler.
    /// </summary>
    public interface IExceptionHandlerFactory
    {
        /// <summary>
        /// To ensure that the exception handlers are registered first,
        /// applications will be required to create an array of IExceptionHandlerFactory
        /// objects and then each of these will register themselves.
        /// </summary>
        void AutoRegister();
    }

    /// <summary>
    /// Different exception types will need to be handled differently.
    /// This interface encapsulates that concept.
    /// </summary>
    /// <typeparam name="T">The type of exception associated with this handler.</typeparam>
    public interface IExceptionHandler<T> where T : LqbException
    {
        /// <summary>
        /// Handle the exception.
        /// </summary>
        /// <param name="exception">The exception that will be handled.</param>
        void HandleException(T exception);
    }

    /// <summary>
    /// Factory for creation of exception handling instance.
    /// </summary>
    /// <typeparam name="T">The type of exception that will be created.</typeparam>
    public interface IExceptionHandlerFactory<T> : IExceptionHandlerFactory where T : LqbException
    {
        /// <summary>
        /// Create an exception handler.
        /// </summary>
        /// <returns>The created exception handler.</returns>
        IExceptionHandler<T> Create();
    }

    /// <summary>
    /// This utility class handles the interaction with the locator service
    /// to simplify the implementation of each exception type's PassToHandler() method.
    /// </summary>
    public static class ExceptionHandlerUtil
    {
        /// <summary>
        /// Handle the exception.
        /// </summary>
        /// <typeparam name="T">The type of exception that is handled.</typeparam>
        /// <param name="exception">Instance of the exception that is handled.</param>
        public static void HandleException<T>(T exception) where T : LqbException
        {
            try
            {
                IExceptionHandlerFactory<T> iFactory = GenericLocator<IExceptionHandlerFactory<T>>.Factory;
                IExceptionHandler<T> iHandler = iFactory.Create();
                iHandler.HandleException(exception);
            }
            catch (Exception)
            {
                // do nothing
            }
        }

        /// <summary>
        /// Helper method to deliver the exception handler factories to the application.
        /// </summary>
        /// <returns>Array of exception handler factories.</returns>
        public static IExceptionHandlerFactory[] GetCoreExceptionHandlerFactories()
        {
            IExceptionHandlerFactory[] arrFactories = new IExceptionHandlerFactory[]
            {
                new DeveloperExceptionHandlerFactory(),
                new ServerExceptionHandlerFactory()
            };
            return arrFactories;
        }
    }
}
