﻿namespace LqbGrammar.Exceptions
{
    /// <summary>
    /// Factory for creating a handler for developer exceptions.
    /// </summary>
    internal sealed class DeveloperExceptionHandlerFactory : IExceptionHandlerFactory<DeveloperException>
    {
        /// <summary>
        /// Register this factory with the locator service.
        /// </summary>
        public void AutoRegister()
        {
            GenericLocator<IExceptionHandlerFactory<DeveloperException>>.RegisterFactory(this);
        }

        /// <summary>
        /// Create a handler for a developer exception.
        /// </summary>
        /// <returns>An exception handler.</returns>
        public IExceptionHandler<DeveloperException> Create()
        {
            return new DeveloperExceptionHandler();
        }
    }
}
