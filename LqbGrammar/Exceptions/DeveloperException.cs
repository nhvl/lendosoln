﻿namespace LqbGrammar.Exceptions
{
    using System;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Use this when some problem is the result of poor coding or configuration.
    /// </summary>
    public sealed class DeveloperException : LqbException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DeveloperException"/> class.
        /// </summary>
        /// <param name="error">The error message.</param>
        public DeveloperException(ErrorMessage error)
            : base(error)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DeveloperException"/> class.
        /// </summary>
        /// <param name="error">The error message.</param>
        /// <param name="inner">Contained inner exception.</param>
        public DeveloperException(ErrorMessage error, Exception inner)
            : base(error, inner)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DeveloperException"/> class.
        /// </summary>
        /// <param name="error">The error message.</param>
        /// <param name="context">The error context.</param>
        public DeveloperException(ErrorMessage error, ErrorContext context)
            : base(error, context)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DeveloperException"/> class.
        /// </summary>
        /// <param name="error">The error message.</param>
        /// <param name="context">The error context.</param>
        /// <param name="inner">Contained inner exception.</param>
        public DeveloperException(ErrorMessage error, ErrorContext context, Exception inner)
            : base(error, context, inner)
        {
        }

        /// <summary>
        /// Pass this exception instance to the appropriate handler.
        /// </summary>
        public override void PassToHandler()
        {
            ExceptionHandlerUtil.HandleException<DeveloperException>(this);
        }
    }
}
