﻿namespace LqbGrammar.Exceptions
{
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Exception thrown when a validation error occurs due to invalid user input.
    /// </summary>
    public class ValidationException : LqbException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ValidationException"/> class.
        /// </summary>
        /// <param name="errorMessage">The validation error message to display to the user.</param>
        public ValidationException(ValidationErrorMessage errorMessage)
            : this(new ValidationErrorContext(new[] { errorMessage }))
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ValidationException"/> class.
        /// </summary>
        /// <param name="validationContext">The validation error context containing information about what validation errors occurred.</param>
        public ValidationException(ValidationErrorContext validationContext)
            : base(ErrorMessage.ValidationError, validationContext)
        {
        }

        /// <summary>
        /// Gets the validation error context containing information to help the user correct invalid input.
        /// </summary>
        /// <value>The validation error context.</value>
        public new ValidationErrorContext Context => (ValidationErrorContext)base.Context;

        /// <summary>
        /// Pass this exception instance to the appropriate handler.
        /// </summary>
        public override void PassToHandler()
        {
            ExceptionHandlerUtil.HandleException(this);
        }
    }
}
