﻿namespace LqbGrammar.Exceptions
{
    using System;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Base class for exceptions thrown in the new LQB architecture.
    /// </summary>
    public abstract class LqbException : Exception
    {
        /// <summary>
        /// Context when the error was thrown.
        /// </summary>
        private ErrorContext context;

        /// <summary>
        /// Initializes a new instance of the <see cref="LqbException"/> class.
        /// </summary>
        /// <param name="error">The error message.</param>
        protected LqbException(ErrorMessage error) : base(error.ToString())
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LqbException"/> class.
        /// </summary>
        /// <param name="error">The error message.</param>
        /// <param name="inner">Contained inner exception.</param>
        protected LqbException(ErrorMessage error, Exception inner)
            : base(error.ToString(), inner)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LqbException"/> class.
        /// </summary>
        /// <param name="error">The error message.</param>
        /// <param name="context">The error context.</param>
        protected LqbException(ErrorMessage error, ErrorContext context)
            : base(error.ToString())
        {
            this.context = context;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LqbException"/> class.
        /// </summary>
        /// <param name="error">The error message.</param>
        /// <param name="context">The error context.</param>
        /// <param name="inner">Contained inner exception.</param>
        protected LqbException(ErrorMessage error, ErrorContext context, Exception inner)
            : base(error.ToString(), inner)
        {
            this.context = context;
        }

        /// <summary>
        /// Prevents a default instance of the <see cref="LqbException"/> class from being created.
        /// </summary>
        private LqbException()
        {
        }

        /// <summary>
        /// Gets the context information, or null.
        /// </summary>
        /// <value>The error context.</value>
        public ErrorContext Context
        {
            get { return this.context; }
        }

        /// <summary>
        /// This is a convenience method so that error handling code is as simple as:
        /// try {...}
        /// catch (LqbException ex)
        /// {
        ///     ex.PassToHandler();
        /// }
        /// which avoids the need for a catch clause for each LqbException-inheriting type.
        /// </summary>
        public abstract void PassToHandler();
    }
}
