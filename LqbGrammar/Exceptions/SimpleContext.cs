﻿namespace LqbGrammar.Exceptions
{
    using System;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Validation;

    /// <summary>
    /// When context information for an error is a simple string, use this class.
    /// </summary>
    public sealed class SimpleContext : ErrorContext
    {
        /// <summary>
        /// The context string.
        /// </summary>
        private string context;

        /// <summary>
        /// Initializes a new instance of the <see cref="SimpleContext"/> class.
        /// </summary>
        /// <param name="context">The context information for an error.</param>
        public SimpleContext(string context)
        {
            RegularExpressionResult result = RegularExpressionEngine.Execute(context, RegularExpressionString.SimpleContext);
            if (!result.IsMatch())
            {
                this.context = ErrorMessages.BadContext;
            }
            else
            {
                this.context = context;
            }
        }

        /// <summary>
        /// Return a string representation of the context information.
        /// </summary>
        /// <returns>The context information.</returns>
        public override string Serialize()
        {
            return this.context;
        }
    }
}
