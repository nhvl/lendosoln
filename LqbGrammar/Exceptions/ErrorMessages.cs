﻿namespace LqbGrammar.Exceptions
{
    /// <summary>
    /// Standard error message values are centralized here.
    /// When changing this class, please echo the change in the DataTypes.ErrorMessage class.
    /// </summary>
    internal static class ErrorMessages
    {
        /// <summary>
        /// A generic error message for validation errors.
        /// </summary>
        public const string ValidationError = "A validation error occurred.";

        /// <summary>
        /// Generic system error message.
        /// </summary>
        private static string systemError = "A system error has occurred.  If this condition persists, please notify support.";

        /// <summary>
        /// If a developer attempts to add context to an exception but the context fails validity checks, this can be used as the context.
        /// </summary>
        private static string badContext = "The context for this error was invalid";

        /// <summary>
        /// If a value is missing or incorrect in the applications's configuration, return this error.
        /// </summary>
        private static string badConfiguration = "The configuration data is incorrect or incomplete";

        /// <summary>
        /// Gets the generic system error message.
        /// </summary>
        public static string SystemError
        {
            get { return systemError; }
        }

        /// <summary>
        /// Gets the error context string to be used when a developer attemts to use invalid context.
        /// </summary>
        public static string BadContext
        {
            get { return badContext; }
        }

        /// <summary>
        /// Gets the error string to be used when a configuration data is missing or erroneous.
        /// </summary>
        public static string BadConfiguration
        {
            get { return badConfiguration; }
        }
    }
}
