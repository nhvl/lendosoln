﻿namespace LqbGrammar.Exceptions
{
    using System.Collections.Generic;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Base class for error context information.
    /// The new exception classes will accept an optional
    /// context instance which handlers can serialize
    /// into internally viewable messages.
    /// </summary>
    public abstract class ErrorContext
    {
        /// <summary>
        /// This is the method which exception handlers
        /// will be calling to gather context information
        /// if the context doesn't return it's data as a 
        /// set of properties.
        /// </summary>
        /// <returns>Serialized representation of the data held within the error context instance.</returns>
        public virtual string Serialize()
        {
            return null;
        }

        /// <summary>
        /// Exception handlers will call this method first to get the context
        /// information as a structured set of properties.  If this returns 
        /// null then the Serialize() method will be called.
        /// </summary>
        /// <returns>Structured set of properties.</returns>
        public virtual Dictionary<LogPropertyName, LogPropertyValue> GetProperties()
        {
            return null;
        }
    }
}
