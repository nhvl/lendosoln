﻿namespace LqbGrammar.Exceptions
{
    using LqbGrammar.Drivers;

    /// <summary>
    /// Use this when some problem is the result of poor coding or configuration.
    /// </summary>
    internal sealed class ServerExceptionHandler : IExceptionHandler<ServerException>
    {
        /// <summary>
        /// Handle a server exception.
        /// </summary>
        /// <remarks>
        /// Although this may be called at any time, during initialization it is 
        /// possible for a factory to not yet have been registered.  Accordingly, 
        /// we use the method that returns null rather than throw an exception 
        /// when the requested factory is unavailable.
        /// </remarks>
        /// <param name="exception">Exception to be handled.</param>
        public void HandleException(ServerException exception)
        {
            var factory = GenericLocator<ILoggingDriverFactory>.GetFactoryDuringInitialization();
            if (factory != null)
            {
                var driver = factory.CreateExceptionLogger();
                driver.LogException(exception);
            }
        }
    }
}
