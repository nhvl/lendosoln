﻿namespace LqbGrammar.Exceptions
{
#if DEBUG
    /// <summary>
    /// Mock exception handler that can be used for unit tests.
    /// </summary>
    public sealed class MockExceptionHandler : IExceptionHandler<MockException>
    {
        /// <summary>
        /// Reference to the factory used to create this handler.
        /// </summary>
        private MockExceptionHandlerFactory factory;

        /// <summary>
        /// Initializes a new instance of the <see cref="MockExceptionHandler"/> class.
        /// </summary>
        /// <param name="factory">An instance of the factory that is creating this instance.</param>
        internal MockExceptionHandler(MockExceptionHandlerFactory factory)
        {
            this.factory = factory;
        }

        /// <summary>
        /// Handle the exception by passing the error back up to the factory so
        /// the unit test code can inspect the error message details.
        /// </summary>
        /// <param name="exception">The exception that is being handled.</param>
        public void HandleException(MockException exception)
        {
            this.factory.SetData(exception.Message, exception.Context);
        }
    }
#endif
}
