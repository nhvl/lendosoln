﻿namespace LqbGrammar
{
    using System;
    using System.Web;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// An application will use this interface to initialize
    /// the fields of an LqbApplication object.
    /// </summary>
    public interface IApplicationInitialize : System.IDisposable
    {
        /// <summary>
        /// Set the name of the application.
        /// </summary>
        /// <param name="name">The name of the application.</param>
        void SetName(string name);

        /// <summary>
        /// Register a factory class with a locator service.
        /// </summary>
        /// <typeparam name="T">Type of the factory being registered.</typeparam>
        /// <param name="factory">Instance of the factory being registered.</param>
        void Register<T>(T factory) where T : class;
    }

    /// <summary><![CDATA[
    /// The first thing that an application must do at startup is
    /// create an array of exception handler factories and then
    /// initialize the LqbApplication.  Then each of the other
    /// interface factories must be created and registered.
    /// <para />
    /// Application startup code should look like this:
    /// <para />
    /// IExceptionHandlerFactory[] arrHandlers = ExceptionHandlerUtil.GetCoreExceptionHandlerFactories();
    /// using (IApplicationInitializer iAppInit = LqbApplication.CreateInitializer(arrHandlers))
    /// {
    ///      iAppInit.SetName("ApplicationName");
    ///      ...other SetXXX() methods
    /// <para />
    ///      (In the following, the creation of the factory classes may depend on properties of the LqbApplication instance,
    ///       but that won't be available yet.  However, the code that sets those properties is just above so the property
    ///       values ARE available for use.  Also, while I've written these registration statements as local, in actuality
    ///       they will done within calls to methods hosted by each module so that the factories can be marked as internal.)
    /// <para />
    ///      iAppInit.RegisterIBusinessCommandFactory>(new BusinessCommandFactory());
    ///      ...other Command, Query, Driver, Adapter interface factories
    /// }
    /// ]]></summary>
    public class LqbApplication
    {
        /// <summary>
        /// Singleton instance of the application class.
        /// </summary>
        private static LqbApplication instance;

        /// <summary>
        /// The name of the application.
        /// </summary>
        private ApplicationName appName = ApplicationName.BadName;

        /// <summary>
        /// The type of the application.
        /// </summary>
        private ApplicationType appType;

        /// <summary>
        /// Testing applications will register mocks, so this allows the code to restore the state of the generic locator service.
        /// </summary>
        private LocatorSnapshot snapshot;

        /// <summary>
        /// Initializes a new instance of the <see cref="LqbApplication"/> class.
        /// </summary>
        /// <param name="type">The application type.</param>
        private LqbApplication(ApplicationType type)
        {
            this.appType = type;

            if (type == ApplicationType.UnitTest)
            {
                this.snapshot = new LocatorSnapshot();
            }
        }

        /// <summary>
        /// Gets the application singleton instance after the initialization is complete.
        /// </summary>
        /// <value>The singleton instance.</value>
        public static LqbApplication Current
        {
            get { return instance; }
        }

        /// <summary>
        /// Gets the name of the application.
        /// </summary>
        /// <value>The name of the application.</value>
        public ApplicationName Name
        {
            get { return this.appName; }
        }

        /// <summary>
        /// Gets the type of the application.
        /// </summary>
        /// <value>The type of the application.</value>
        public ApplicationType Type
        {
            get { return this.appType; }
        }

        /// <summary>
        /// Register the exception handler factories, create an instance of LqbApplication and
        /// hand back an initializer for setting the properties of the application.
        /// </summary>
        /// <param name="exceptionHandlerFactories">The exception handler factories passed in together.</param>
        /// <returns>An instance of a class that can be used to configure the application.</returns>
        public static IApplicationInitialize CreateInitializer(IExceptionHandlerFactory[] exceptionHandlerFactories)
        {
            return CreateInitializer(ApplicationType.Normal, exceptionHandlerFactories);
        }

        /// <summary>
        /// Register the exception handler factories, create an instance of LqbApplication and
        /// hand back an initializer for setting the properties of the application.
        /// </summary>
        /// <param name="type">The type of the application that is being initialized.</param>
        /// <param name="exceptionHandlerFactories">The exception handler factories passed in together.</param>
        /// <returns>An instance of a class that can be used to configure the application.</returns>
        public static IApplicationInitialize CreateInitializer(ApplicationType type, IExceptionHandlerFactory[] exceptionHandlerFactories)
        {
            if (instance != null)
            {
                return null; // This method can only be called once
            }

            // Make sure the exception handlers are registered before any other code is executed
            foreach (var iReg in exceptionHandlerFactories)
            {
                iReg.AutoRegister();
            }

            LqbApplication app = new LqbApplication(type);
            return new Initializer(app);
        }

        /// <summary>
        /// Allow unit tests to overwrite factories with mocks.  This will
        /// throw a DeveloperException if not called from a unit test.
        /// </summary>
        /// <typeparam name="T">The factory interface type.</typeparam>
        /// <param name="factory">The mock factory instance.</param>
        /// <returns>True if the factory was registered, false otherwise.</returns>
        public bool RegisterFactory<T>(T factory) where T : class
        {
            // NEVER allow the registration of mocks from within
            // a web application.  The decision to do the LUnit
            // detection this way was made after consultation with Tim J.
            if (HttpContext.Current != null)
            {
                return false;
            }

            if (this.Type == ApplicationType.UnitTest)
            {
                GenericLocator<T>.RegisterFactory(factory);
                this.snapshot.RegisterLate<T>(factory);
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Restore the factories that are in the snapshot.
        /// </summary>
        public void RestoreFactories()
        {
            if (this.Type == ApplicationType.UnitTest)
            {
                this.snapshot.Restore();
            }
        }

        /// <summary>
        /// This class is used to initialize the application.
        /// </summary>
        private sealed class Initializer : IApplicationInitialize
        {
            /// <summary>
            /// The standard disposed flag.
            /// </summary>
            private bool disposed;

            /// <summary>
            /// The application instance being initialized.
            /// </summary>
            private LqbApplication app;

            /// <summary>
            /// Initializes a new instance of the <see cref="Initializer"/> class.
            /// </summary>
            /// <param name="app">The application to be initialized.</param>
            internal Initializer(LqbApplication app)
            {
                this.app = app;
            }

            /// <summary>
            /// Finalizes an instance of the <see cref="Initializer" /> class.
            /// </summary>
            ~Initializer()
            {
                this.Dispose(false);
            }

            /// <summary>
            /// Set the name of the application.
            /// </summary>
            /// <param name="name">The unvalidated name, it will be validated here.</param>
            public void SetName(string name)
            {
                if (this.disposed)
                {
                    throw new DeveloperException(ErrorMessage.SystemError);
                }

                ApplicationName? appName = ApplicationName.Create(name);
                if (appName == null)
                {
                    throw new DeveloperException(ErrorMessage.SystemError);
                }

                this.app.appName = appName.Value;
            }

            /// <summary>
            /// This is an instance method on purpose.  We want to ensure
            /// that the initialization code has been called prior to
            /// registering any interface factories. 
            /// </summary>
            /// <typeparam name="T">Type of the factory being registered.</typeparam>
            /// <param name="factory">Instance of the factory being registered.</param>
            public void Register<T>(T factory) where T : class
            {
                if (this.disposed)
                {
                    throw new DeveloperException(ErrorMessage.SystemError);
                }

                GenericLocator<T>.RegisterFactory(factory);
                if (this.app.Type == ApplicationType.UnitTest)
                {
                    this.app.snapshot.Register<T>(factory);
                }
            }

            /// <summary>
            /// Implementation of IDisposable.
            /// </summary>
            public void Dispose()
            {
                this.Dispose(true);
            }

            /// <summary>
            /// Standard helper method for implementing IDisposable.
            /// </summary>
            /// <param name="disposing">True if called from the Dispose method,
            /// false if called by the Finalizer method.</param>
            private void Dispose(bool disposing)
            {
                if (this.disposed)
                {
                    return;
                }

                this.disposed = true;
                if (disposing)
                {
                    LqbApplication.instance = this.app;
                }
            }
        }
    }
}
