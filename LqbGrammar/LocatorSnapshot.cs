﻿namespace LqbGrammar
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// This class will hold a list of factories and 
    /// add them to the generic locator service when
    /// necessary.
    /// </summary>
    internal class LocatorSnapshot
    {
        /// <summary>
        /// A list of all the factories in the snapshot.
        /// </summary>
        private Dictionary<Type, IRestoreFactory> list;

        /// <summary>
        /// A set of all the types that were registered after initialization.
        /// </summary>
        private Dictionary<Type, IRestoreFactory> overwrites;

        /// <summary>
        /// Initializes a new instance of the <see cref="LocatorSnapshot"/> class.
        /// </summary>
        public LocatorSnapshot()
        {
            this.list = new Dictionary<Type, IRestoreFactory>();
            this.overwrites = new Dictionary<Type, IRestoreFactory>();
        }

        /// <summary>
        /// Interface that gives a common type to all the factory item holder
        /// instances that implement the restore feature.
        /// </summary>
        private interface IRestoreFactory
        {
            /// <summary>
            /// Gets the type of the item.
            /// </summary>
            /// <value>The type of the item.</value>
            Type ItemType { get; }

            /// <summary>
            /// Restore a factor by registering it with the generic locator service.
            /// </summary>
            void Restore();

            /// <summary>
            /// Remove a factory from the generic locator service.
            /// </summary>
            void Remove();
        }

        /// <summary>
        /// Add a factory to the snapshot.  Note this will overwrite any existing registration
        /// of the same type.  This is a feature as some applications may choose to register 
        /// factories with the utilities then overwrite specific factories.
        /// </summary>
        /// <typeparam name="T">The type of the factory interface.</typeparam>
        /// <param name="factory">The factory.</param>
        public void Register<T>(T factory) where T : class
        {
            // Note that using a key of System.Type here allows us to 
            // easily overwrite the existing factory.
            this.list[typeof(T)] = new FactoryItem<T>(factory);
        }

        /// <summary>
        /// Add a factory to the set of overwrites.  We want to make sure that all the
        /// overwrites have been removed, even when they aren't actuall overwriting
        /// anything.
        /// </summary>
        /// <typeparam name="T">The type of the factory interface.</typeparam>
        /// <param name="factory">The factory.</param>
        public void RegisterLate<T>(T factory) where T : class
        {
            this.overwrites[typeof(T)] = new FactoryItem<T>(factory);
        }

        /// <summary>
        /// Restore all the factories in this snapshot.
        /// </summary>
        public void Restore()
        {
            foreach (var key in this.overwrites.Keys)
            {
                this.overwrites[key].Remove();
            }

            this.overwrites = new Dictionary<Type, IRestoreFactory>();

            foreach (var key in this.list.Keys)
            {
                this.list[key].Restore();
            }
        }

        /// <summary>
        /// This class holds a factory of a specific interface type.
        /// </summary>
        /// <typeparam name="T">The factory interface type.</typeparam>
        private class FactoryItem<T> : IRestoreFactory where T : class
        {
            /// <summary>
            /// Factory instance.
            /// </summary>
            private T item;

            /// <summary>
            /// Initializes a new instance of the <see cref="FactoryItem{T}"/> class.
            /// </summary>
            /// <param name="item">The factory instance to hold and restore.</param>
            public FactoryItem(T item)
            {
                this.item = item;
            }

            /// <summary>
            /// Gets the System.Type of the factory interface.
            /// </summary>
            public Type ItemType
            {
                get { return typeof(T);  }
            }

            /// <summary>
            /// Restore the factory item.
            /// </summary>
            public void Restore()
            {
                GenericLocator<T>.RegisterFactory(this.item);
            }

            /// <summary>
            /// Remove the factory item.
            /// </summary>
            public void Remove()
            {
                GenericLocator<T>.RegisterFactory(null);
            }
        }
    }
}
