﻿namespace LqbGrammar
{
    using System;
    using DataTypes;
    using Drivers.SecurityEventLogging;

    /// <summary>
    /// AbstractUserPrincipal isn't accessible from the LqbGrammar project, so here's a proxy to be able to inject.
    /// </summary>
    public interface IUserPrincipal
    {
        /// <summary>
        /// Gets the user's ID.
        /// </summary>
        /// <value>User Identifier.</value>
        Guid UserId { get; }

        /// <summary>
        /// Gets the ID of the broker that the user belongs to.
        /// </summary>
        /// <value>Broker Identifier.</value>
        Guid BrokerId { get; }

        /// <summary>
        /// Gets the user's first name.
        /// </summary>
        string FirstName { get; }

        /// <summary>
        /// Gets the user's last name.
        /// </summary>
        string LastName { get; }

        /// <summary>
        /// Gets the user's login name.
        /// </summary>
        string LoginNm { get; }

        /// <summary>
        /// Gets the principal's type.
        /// </summary>
        string Type { get; }

        /// <summary>
        /// Gets the principal's initial type.
        /// </summary>
        InitialPrincipalTypeT InitialPrincipalType { get; }

        /// <summary>
        /// Gets the principal's application type.
        /// </summary>
        E_ApplicationT ApplicationType { get; }

        /// <summary>
        /// Gets the principal's portal mode.
        /// </summary>
        E_PortalMode PortalMode { get; }
    }
}
