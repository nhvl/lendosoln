﻿namespace LqbGrammar.DataTypes
{
    using System;
    using LqbGrammar.Exceptions;
    using LqbGrammar.Validation;

    /// <summary>
    /// Encapsulate a log message, with the format validated.
    /// </summary>
    public struct LogMessage : System.IEquatable<LogMessage>
    {
        /// <summary>
        /// This is the contained log message.
        /// </summary>
        private string message;

        /// <summary>
        /// Initializes a new instance of the <see cref="LogMessage"/> struct.
        /// </summary>
        /// <param name="message">The log message.</param>
        private LogMessage(string message)
        {
            this.message = message;
        }

        /// <summary>
        /// Validate and encapsulate a log message.
        /// </summary>
        /// <param name="message">The log message.</param>
        /// <returns>The encapsulated log message if it is valid, or null.</returns>
        public static LogMessage? Create(string message)
        {
            if (message == null)
            {
                message = string.Empty;
            }

            // Skip the regular expression check since it doesn't currently do anything
            // and it may adversely impact performance as we do a lot of logging of large messages.
            ////RegularExpressionResult result = RegularExpressionEngine.Execute(message, RegularExpressionString.LogMessage);
            ////if (!result.IsMatch())
            ////{
            ////    return null;
            ////}

            return new LogMessage(message);
        }

        /// <summary>
        /// An error message should be loggable, so we will permit the logging
        /// framework to accept error messages.
        /// </summary>
        /// <param name="error">The error message that will be cast to a log message.</param>
        public static implicit operator LogMessage(ErrorMessage error)
        {
            var msg = LogMessage.Create(error.ToString());
            if (msg == null)
            {
                throw new DeveloperException(ErrorMessage.SystemError);
            }

            return msg.Value;
        }

        /// <summary>
        /// Static method used for checking equality of two instances.
        /// </summary>
        /// <param name="lhs">Instance on the left side of the == operator.</param>
        /// <param name="rhs">Instance on the right side of the == operator.</param>
        /// <returns>True if the instances are equal, false otherwise.</returns>
        public static new bool Equals(object lhs, object rhs)
        {
            if (!(lhs is LogMessage))
            {
                return false;
            }

            if (!(rhs is LogMessage))
            {
                return false;
            }

            return ((LogMessage)lhs).Equals((LogMessage)rhs);
        }

        /// <summary>
        /// Implements the == operator.
        /// </summary>
        /// <param name="lhs">Instance on the left side of the == operator.</param>
        /// <param name="rhs">Instance on the right side of the == operator.</param>
        /// <returns>True if the instances are equal, false otherwise.</returns>
        public static bool operator ==(LogMessage lhs, LogMessage rhs)
        {
            return lhs.Equals(rhs);
        }

        /// <summary>
        /// Implements the != oeprator.
        /// </summary>
        /// <param name="lhs">Instance on the left side of the != operator.</param>
        /// <param name="rhs">Instance on the right side of the != operator.</param>
        /// <returns>True if the instances are not equal, false otherwise.</returns>
        public static bool operator !=(LogMessage lhs, LogMessage rhs)
        {
            return !lhs.Equals(rhs);
        }

        /// <summary>
        /// Overrides the method inherited from the Object class.
        /// </summary>
        /// <returns>The log message.</returns>
        public override string ToString()
        {
            return this.message;
        }

        /// <summary>
        /// Implement the IEquatable interface.
        /// </summary>
        /// <param name="other">Instance that is compared with this instance for equality.</param>
        /// <returns>True if the instances are equal, false otherwise.</returns>
        public bool Equals(LogMessage other)
        {
            return string.Compare(this.message, other.message, StringComparison.OrdinalIgnoreCase) == 0;
        }

        /// <summary>
        /// Override of the method inherited from the Object class.
        /// </summary>
        /// <param name="obj">Instance that is compared with this instance for equality.</param>
        /// <returns>True if the instances are equal, false otherwise.</returns>
        public override bool Equals(object obj)
        {
            if (!(obj is LogMessage))
            {
                return false;
            }

            return Equals((LogMessage)obj);
        }

        /// <summary>
        /// Override the method inherited from the Object class.
        /// </summary>
        /// <returns>An integer that is the hash of this instance.</returns>
        public override int GetHashCode()
        {
            return this.message.GetHashCode();
        }
    }
}
