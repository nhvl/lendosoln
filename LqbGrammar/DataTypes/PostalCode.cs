﻿namespace LqbGrammar.DataTypes
{
    /// <summary>
    /// Encapsulate a postal code for any country.
    /// </summary>
    public partial struct PostalCode
    {
        /// <summary>
        /// Attempt to parse a string into a <see cref="UnitedStatesState"/>.
        /// </summary>
        /// <param name="value">The string to parse.</param>
        /// <param name="country">The country associated with the postal code.</param>
        /// <returns>A <see cref="PostalCode"/> of the specified value, or null.</returns>
        public static PostalCode? CreateWithValidation(string value, CountryCodeIso3 country)
        {
            if (value == null)
            {
                return null;
            }
            else if (!PassesAdvancedValidation(value, country))
            {
                return null;
            }

            return new PostalCode(value);
        }

        /// <summary>
        /// Validate the postal code against the semantic rules for the associated country.
        /// </summary>
        /// <param name="value">The postal code.</param>
        /// <param name="country">The country associated with the postal code.</param>
        /// <returns>True if the postal code is semantically valid, false otherwise.</returns>
        private static bool PassesAdvancedValidation(string value, CountryCodeIso3 country)
        {
            // NOTE: for now we will only validate US zipcodes.  We can add validation for other countries as the rules are established.
            if (country == CountryCodeIso3.UnitedStates)
            {
                var zipcode = Zipcode.CreateWithValidation(value);
                return zipcode != null;
            }
            else
            {
                return true;
            }
        }
    }
}
