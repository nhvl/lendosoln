﻿namespace LqbGrammar.DataTypes
{
    /// <summary>
    /// Encapsulate the values that LQB assigns to
    /// the Referer http header.  Note that the 
    /// header is misspelled and the spelling of
    /// this class name is correct.
    /// No instances can be created, all values must be
    /// hard-coded here.
    /// </summary>
    public struct LqbHttpReferrer
    {
        /// <summary>
        /// An instance of a bad LqbHttpReferrer that can be used for variable initialization.
        /// </summary>
        public static readonly LqbHttpReferrer Invalid = new LqbHttpReferrer(null);

        /// <summary>
        /// Used by the AssociatedBot.
        /// </summary>
        public static readonly LqbHttpReferrer Associated = new LqbHttpReferrer("https://www.associatedtpo.com");

        /// <summary>
        /// Used by the AstoriaBot.
        /// </summary>
        public static readonly LqbHttpReferrer Astoria = new LqbHttpReferrer("https://www.astoriamortgage.com");

        /// <summary>
        /// Used by the EnvoyBot.
        /// </summary>
        public static readonly LqbHttpReferrer Envoy = new LqbHttpReferrer("https://cld.envoymortgage.com/");

        /// <summary>
        /// Used by the FAMCBot.
        /// </summary>
        public static readonly LqbHttpReferrer FAMC = new LqbHttpReferrer("https://franklinamerican.com/ext/general?npage=home");

        /// <summary>
        /// Used by the FloridaCapitalBankBot.
        /// </summary>
        public static readonly LqbHttpReferrer FloridaCapitalBank = new LqbHttpReferrer("https://www.flcbmtg.com/Login?ReturnUrl=%2f");

        /// <summary>
        /// Used by the FreddieMacBot.
        /// </summary>
        public static readonly LqbHttpReferrer FreddieMac = new LqbHttpReferrer("https://sell.freddiemac.com/dispatch");

        /// <summary>
        /// Used by the NYCBMortgageBot.
        /// </summary>
        public static readonly LqbHttpReferrer NYCBMortgage = new LqbHttpReferrer("https://www.nycbmortgage.com/MtgMktg/rates/");

        /// <summary>
        /// Used by the ProvidentWholesaleBot.
        /// </summary>
        public static readonly LqbHttpReferrer ProvidentWholesale = new LqbHttpReferrer("https://pfloans.provident.com/secure/login.aspx?returnUrl=/default.aspx?go=rates");

        /// <summary>
        /// The LqbHttpReferrer value.
        /// </summary>
        private string referrer;

        /// <summary>
        /// Initializes a new instance of the <see cref="LqbHttpReferrer"/> struct.
        /// </summary>
        /// <param name="referrer">The contained referrer string.</param>
        private LqbHttpReferrer(string referrer)
        {
            this.referrer = referrer;
        }

        /// <summary>
        /// Override ToString().
        /// </summary>
        /// <returns>The contained referrer string.</returns>
        public override string ToString()
        {
            return this.referrer;
        }
    }
}
