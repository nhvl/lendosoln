﻿namespace LqbGrammar.DataTypes
{
    using System;
    using LqbGrammar.Enumerations;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Encapsulate the concept of a precise instant of time.
    /// </summary>
    public struct Instant : ISemanticType, IEquatable<Instant>, IComparable, IComparable<Instant>
    {
        /// <summary>
        /// Format string used when serializing/deserializing an Instant.
        /// </summary>
        public const string FormatString = "yyyy-MM-ddTHH:mm:ss.fffffffZ";

        /// <summary>
        /// Initializes a new instance of the <see cref="Instant"/> struct.
        /// </summary>
        /// <param name="utc">A DateTime representing the UTC.</param>
        private Instant(DateTime utc)
        {
            this.DateTimeUTC = utc;
        }

        /// <summary>
        /// Gets the datetime as UTC.
        /// </summary>
        /// <value>The datetime as UTC.</value>
        public DateTime DateTimeUTC { get; private set; }

        /// <summary>
        /// Gets DateTimeUTC as an object.
        /// </summary>
        object ISemanticType.Value => this.DateTimeUTC;

        /// <summary>
        /// Create an Instant from a datetime without timezone information.
        /// </summary>
        /// <param name="value">The datetime.</param>
        /// <returns>An Instant instance.</returns>
        public static Instant? Create(DateTime value)
        {
            switch (value.Kind)
            {
                case DateTimeKind.Local:
                    {
                        DateTime utc = value.ToUniversalTime();
                        return new Instant(utc);
                    }

                case DateTimeKind.Unspecified:
                    {
                        DateTime local = new DateTime(value.Ticks, DateTimeKind.Local);
                        DateTime utc = local.ToUniversalTime();
                        return new Instant(utc);
                    }

                case DateTimeKind.Utc:
                    {
                        return new Instant(value);
                    }

                default:
                    throw new DeveloperException(ErrorMessage.SystemError);
            }
        }

        /// <summary>
        /// Create an Instant from a datetime and offset from UTC.
        /// </summary>
        /// <param name="value">The datetime and offset.</param>
        /// <returns>An Instant instance.</returns>
        public static Instant? Create(DateTimeOffset value)
        {
            return new Instant(value.UtcDateTime);
        }

        /// <summary>
        /// Create an Instant from a string representation.
        /// </summary>
        /// <param name="value">The string representation of an instant.</param>
        /// <returns>An Instant instance, or null.</returns>
        public static Instant? Create(string value)
        {
            DateTimeOffset result;
            if (DateTimeOffset.TryParseExact(value, FormatString, System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.AssumeUniversal, out result))
            {
                return new Instant(result.UtcDateTime);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// The == operator is implemented here.
        /// </summary>
        /// <param name="lhs">Instance on the left of the == operator.</param>
        /// <param name="rhs">Instance on the right of the == operator.</param>
        /// <returns>True if the two instances are equal, false otherwise.</returns>
        public static bool operator ==(Instant lhs, Instant rhs)
        {
            return lhs.Equals(rhs);
        }

        /// <summary>
        /// The != operator is implemented here.
        /// </summary>
        /// <param name="lhs">Instance on the left of the != operator.</param>
        /// <param name="rhs">Instance on the right of the != operator.</param>
        /// <returns>True if the two instances are not equal, false otherwise.</returns>
        public static bool operator !=(Instant lhs, Instant rhs)
        {
            return !lhs.Equals(rhs);
        }

        /// <summary>
        /// The less than operator is implemented here.
        /// </summary>
        /// <param name="lhs">Operand on the left of the operator.</param>
        /// <param name="rhs">Operand on the right of the operator.</param>
        /// <returns>The result of applying the operator to the two operands.</returns>
        public static bool operator <(Instant lhs, Instant rhs)
        {
            return lhs.CompareTo(rhs) < 0;
        }

        /// <summary>
        /// The less than or equal operator is implemented here.
        /// </summary>
        /// <param name="lhs">Operand on the left of the operator.</param>
        /// <param name="rhs">Operand on the right of the operator.</param>
        /// <returns>The result of applying the operator to the two operands.</returns>
        public static bool operator <=(Instant lhs, Instant rhs)
        {
            return lhs.CompareTo(rhs) <= 0;
        }

        /// <summary>
        /// The greater than operator is implemented here.
        /// </summary>
        /// <param name="lhs">Operand on the left of the operator.</param>
        /// <param name="rhs">Operand on the right of the operator.</param>
        /// <returns>The result of applying the operator to the two operands.</returns>
        public static bool operator >(Instant lhs, Instant rhs)
        {
            return lhs.CompareTo(rhs) > 0;
        }

        /// <summary>
        /// The greater than or equal operator is implemented here.
        /// </summary>
        /// <param name="lhs">Operand on the left of the operator.</param>
        /// <param name="rhs">Operand on the right of the operator.</param>
        /// <returns>The result of applying the operator to the two operands.</returns>
        public static bool operator >=(Instant lhs, Instant rhs)
        {
            return lhs.CompareTo(rhs) >= 0;
        }

        /// <summary>
        /// Override the Equals method inherited from the Object class.
        /// </summary>
        /// <param name="obj">Instance that is checked for equality with this instance.</param>
        /// <returns>True if obj is equal to this instance, false otherwise.</returns>
        public override bool Equals(object obj)
        {
            if (!(obj is Instant))
            {
                return false;
            }

            return this.Equals((Instant)obj);
        }

        /// <summary>
        /// Implementation of the IEquatable interface.
        /// </summary>
        /// <param name="other">Instance that is compared to this instance for equality.</param>
        /// <returns>True if the instances are equal, false otherwise.</returns>
        public bool Equals(Instant other)
        {
            return this.CompareTo(other) == 0;
        }

        /// <summary>
        /// Compare this instance with another object.
        /// </summary>
        /// <param name="obj">The other object.</param>
        /// <returns>The result of the comparison, or an exception is thrown if the other object is not an instance of the Instant struct.</returns>
        public int CompareTo(object obj)
        {
            if (obj is Instant)
            {
                return this.CompareTo((Instant)obj);
            }
            else
            {
                throw new DeveloperException(ErrorMessage.SystemError);
            }
        }

        /// <summary>
        /// Compare this instance with another Instant.
        /// </summary>
        /// <param name="other">The other Instant.</param>
        /// <returns>The result of the comparison, zero if equal, greater than zero if this instance is greater, less than zero of this instance is lesser.</returns>
        public int CompareTo(Instant other)
        {
            return this.DateTimeUTC.CompareTo(other.DateTimeUTC);
        }

        /// <summary>
        /// Override of the method inherited from the Object class.
        /// </summary>
        /// <returns>An integer that is the hash code for this instance.</returns>
        public override int GetHashCode()
        {
            return this.DateTimeUTC.GetHashCode();
        }

        /// <summary>
        /// Override of the method inherited from the Object class.
        /// </summary>
        /// <returns>A string representation of an Instant.</returns>
        public override string ToString()
        {
            return this.DateTimeUTC.ToString(FormatString);
        }
    }
}
