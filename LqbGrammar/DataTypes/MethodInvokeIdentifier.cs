﻿namespace LqbGrammar.DataTypes
{
    /// <summary>
    /// Method invocation libraries like Hangfire may
    /// present identifiers.  These need to be treated
    /// as blind data structures to minimize the possibility
    /// of mischief.  For example, we have no way of knowing
    /// whether or not the Hangfire database is vulnerable
    /// to SQL injection.  By tightly binding the identifiers
    /// we can prevent our own interface from being an
    /// attack vector.  This base class will provide
    /// the only public face of such identifiers.
    /// </summary>
    public abstract class MethodInvokeIdentifier
    {
        /// <summary>
        /// Prevent code from attempting to expose the hidden value.
        /// </summary>
        /// <returns>The default value from System.Object.ToString().</returns>
        public sealed override string ToString()
        {
            return base.ToString();
        }
    }
}
