﻿namespace LqbGrammar.DataTypes
{
    /// <summary>
    /// An interface that allows access to the base type encapsulated by
    /// a semantic type.
    /// </summary>
    public interface ISemanticType
    {
        /// <summary>
        /// Gets the underlying value.
        /// </summary>
        object Value { get; }
    }
}
