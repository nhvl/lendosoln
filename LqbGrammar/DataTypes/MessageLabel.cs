﻿namespace LqbGrammar.DataTypes
{
    using LqbGrammar.Exceptions;
    using LqbGrammar.Validation;

    /// <summary>
    /// Encapsulate and validate a label for a message sent to a message queue.
    /// </summary>
    public struct MessageLabel : System.IEquatable<MessageLabel>
    {
        /// <summary>
        /// An invalid instance that can be used for variable initialization.
        /// </summary>
        public static readonly MessageLabel BadLabel = new MessageLabel(string.Empty);

        /// <summary>
        /// The message label.
        /// </summary>
        private string label;

        /// <summary>
        /// Initializes a new instance of the <see cref="MessageLabel"/> struct.
        /// </summary>
        /// <param name="label">The label.</param>
        private MessageLabel(string label)
        {
            this.label = label;
        }

        /// <summary>
        /// Validate an message label.
        /// </summary>
        /// <param name="label">The message label.</param>
        /// <returns>The validated message label, or null if the input label is invalid.</returns>
        public static MessageLabel? Create(string label)
        {
            if (string.IsNullOrEmpty(label))
            {
                return null;
            }

            RegularExpressionResult result = RegularExpressionEngine.Execute(label, RegularExpressionString.MessageLabel);
            if (!result.IsMatch())
            {
                return null;
            }

            return new MessageLabel(label);
        }

        /// <summary>
        /// Compare two objects for equality and they are both the same validated message label.
        /// </summary>
        /// <param name="lhs">The object on the left side of the == operator.</param>
        /// <param name="rhs">The object on the right side of the == operator.</param>
        /// <returns>True if both objects are instances of the MessageLabel class and they are equal to each other.</returns>
        public static new bool Equals(object lhs, object rhs)
        {
            if (!(lhs is MessageLabel))
            {
                return false;
            }

            if (!(rhs is MessageLabel))
            {
                return false;
            }

            return ((MessageLabel)lhs).Equals((MessageLabel)rhs);
        }

        /// <summary>
        /// Implement the equality operator for a validated message label.
        /// </summary>
        /// <param name="lhs">Instance on the left side of the == operator.</param>
        /// <param name="rhs">Instance on the right side of the == operator.</param>
        /// <returns>True if the instances are equal.</returns>
        public static bool operator ==(MessageLabel lhs, MessageLabel rhs)
        {
            return lhs.Equals(rhs);
        }

        /// <summary>
        /// Implement the inequality operator for a validated message label.
        /// </summary>
        /// <param name="lhs">The instance on the left side of the != operator.</param>
        /// <param name="rhs">The instance on the right side of the != operator.</param>
        /// <returns>Return true if the two instances are not equal, false otherwise.</returns>
        public static bool operator !=(MessageLabel lhs, MessageLabel rhs)
        {
            return !lhs.Equals(rhs);
        }

        /// <summary>
        /// Override the implementation inherited from the Object class.
        /// </summary>
        /// <returns>The message label.</returns>
        public override string ToString()
        {
            return this.label;
        }

        /// <summary>
        /// Compare this instance with another instance for equality.
        /// </summary>
        /// <param name="other">The other instance.</param>
        /// <returns>True if the two instances are equal, false otherwise.</returns>
        public bool Equals(MessageLabel other)
        {
            return this.label == other.label;
        }

        /// <summary>
        /// Compares this instance with another object for equality.
        /// </summary>
        /// <param name="obj">The other object.</param>
        /// <returns>True if the other object is a valid message label and the two objects have the same value.</returns>
        public override bool Equals(object obj)
        {
            if (!(obj is MessageLabel))
            {
                return false;
            }

            return Equals((MessageLabel)obj);
        }

        /// <summary>
        /// Override the implementation inherited from the Object class.
        /// </summary>
        /// <returns>An integer that is the hash of this instance.</returns>
        public override int GetHashCode()
        {
            return this.label.GetHashCode();
        }
    }
}
