﻿namespace LqbGrammar.DataTypes
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Security.Permissions;

    /// <summary>
    /// A security token is intended to provide both identity
    /// and authorization details for a principal within any 
    /// of the LQB applications.
    /// </summary>
    [Serializable]
    public sealed class SecurityToken : ISerializable
    {
        /// <summary>
        /// Version for the implementation, used to ensure proper de-serialization.
        /// </summary>
        private const int Version = 201707;

        /// <summary>
        /// A version that will never be used.
        /// </summary>
        private const int InvalidVersion = -1;

        /// <summary>
        /// The delimiter for lists of things like role ids and group ids.
        /// </summary>
        private const string Delimiter = ",";

        /// <summary>
        /// Initializes a new instance of the <see cref="SecurityToken"/> class.
        /// </summary>
        public SecurityToken()
        {
            this.RoleIds = new PrimaryKeyGuid[0];
            this.EmployeeGroupIds = new GroupIdentifier[0];
            this.InstanceVersion = Version;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SecurityToken"/> class. <para></para>
        /// Note that this method should not throw errors.  As a result it populates invalid data if it finds bad data in the serializationInfo object. <para></para>
        /// Consequently you should check the resulting token for validity.
        /// </summary>
        /// <param name="info">Object from which the serialized fields are pulled.</param>
        /// <param name="context">The parameter is not used.</param>
        private SecurityToken(SerializationInfo info, StreamingContext context)
        {
            int version = this.GetSafeInt32(info, "version", InvalidVersion);
            this.InstanceVersion = version;

            string unvalidatedUserIdString = this.GetSafeString(info, "userId", null);
            string unvalidatedBrokerIdString = this.GetSafeString(info, "brokerId", null);
            string unvalidatedFullNameString = this.GetSafeString(info, "fullName", null);

            UserAccountIdentifier? uid = UserAccountIdentifier.Create(unvalidatedUserIdString);
            if (uid == null)
            {
                uid = UserAccountIdentifier.BadIdentifier;
            }

            BrokerIdentifier? bid = BrokerIdentifier.Create(unvalidatedBrokerIdString);
            if (bid == null)
            {
                bid = BrokerIdentifier.BadIdentifier;
            }

            PersonName? fn = null;
            try
            {
                fn = PersonName.CreateWithValidation(unvalidatedFullNameString);
            }
            catch (Exception)
            {
                fn = null;
            }

            if (fn == null)
            {
                fn = PersonName.BadName;
            }

            this.UserId = uid.Value;
            this.BrokerId = bid.Value;
            this.Fullname = fn.Value;

            if (version < 201707)
            {
                // null will mean full privileges, which is what we want initially to continue with what they had before.
                this.RoleIds = null;
                this.EmployeeGroupIds = null;
            }
            else 
            {
                // if(version >= 201707)
                string unvalidatedRoleIdsString = this.GetSafeString(info, "roleIds", null);
                string unvalidatedEmployeeGroupIdsString = this.GetSafeString(info, "employeeGroupIds", null);

                if (unvalidatedRoleIdsString == null)
                {
                    this.RoleIds = null;
                }
                else if (unvalidatedRoleIdsString == string.Empty)
                {
                    this.RoleIds = new PrimaryKeyGuid[0];
                }
                else
                {
                    // if unvalidatedRoleIdsString is not null or empty.
                    var roleIdParts = unvalidatedRoleIdsString.Split(new string[] { Delimiter }, StringSplitOptions.None);
                    this.RoleIds = roleIdParts.Select(roleIdString => { var r = PrimaryKeyGuid.Create(roleIdString); return r.HasValue ? r.Value : PrimaryKeyGuid.Invalid; }).ToArray();
                }

                if (unvalidatedEmployeeGroupIdsString == null)
                {
                    this.EmployeeGroupIds = null;
                }
                else if (unvalidatedEmployeeGroupIdsString == string.Empty)
                {
                    this.EmployeeGroupIds = new GroupIdentifier[0];
                }
                else 
                {
                    // if unvalidatedEmployeeGroupIdsString is not null or empty.
                    var groupIdParts = unvalidatedEmployeeGroupIdsString.Split(new string[] { Delimiter }, StringSplitOptions.None);
                    this.EmployeeGroupIds = groupIdParts.Select(groupIdString => { var g = GroupIdentifier.Create(groupIdString); return g.HasValue ? g.Value : GroupIdentifier.BadIdentifier; }).ToArray();
                }
            }
        }

        /// <summary>
        /// Gets or sets the user id for the logged in user.
        /// </summary>
        /// <value>The user id of the logged in user.</value>
        public UserAccountIdentifier UserId { get; set; }

        /// <summary>
        /// Gets or sets the fullname (firstname + lastname) for the logged in user.
        /// </summary>
        /// <value>The fullname for the logged in user.</value>
        public PersonName Fullname { get; set; }

        /// <summary>
        /// Gets or sets the BrokerId for the logged in user.  This will be null for internal users.
        /// </summary>
        /// <value>The BrokerId for the logged in user.</value>
        public BrokerIdentifier BrokerId { get; set; }

        /// <summary>
        /// Gets or sets the role ids for the logged in user. <para></para>
        /// Default is empty array, which will mean no roles. <para></para>
        /// Null will mean full roles since that's what security tokens with previous versions will have. <para></para>
        /// </summary>
        /// <value>The role ids for the logged in user, or null.</value>
        public PrimaryKeyGuid[] RoleIds { get; set; }

        /// <summary>
        /// Gets or sets the group identifiers for the logged in user. <para></para>
        /// Default is empty array, which will mean no groups. <para></para>
        /// Null will mean full groups since that's what security tokens with previous versions will have. <para></para>
        /// </summary>
        /// <value>The group identifiers for the logged in user, or null.</value>
        public GroupIdentifier[] EmployeeGroupIds { get; set; }

        /// <summary>
        /// Gets or sets the version of this instance.  This represents the version of when the object was initially created the first time, and is preserved across serialization/deserialization.
        /// </summary>
        /// <value>The version id of the object instance.</value>
        public int InstanceVersion { get; set; }

        /// <summary>
        /// Checks if the security token is valid.
        /// </summary>
        /// <param name="token">The security token in question.</param>
        /// <returns>The validity of the security token.</returns>
        public static bool IsValid(SecurityToken token)
        {
            if (token == null)
            {
                return false;
            }

            return token.IsValid();
        }

        /// <summary>
        /// Add instance data to the serializer.
        /// </summary>
        /// <param name="info">Object to which the instance data is added.</param>
        /// <param name="context">The parameter is not used.</param>
        [SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter = true)]
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("version", this.InstanceVersion);
            info.AddValue("userId", this.UserId.ToString());
            info.AddValue("brokerId", this.BrokerId.ToString());
            info.AddValue("fullName", this.Fullname.ToString());
            info.AddValue("roleIds", this.RoleIds == null ? null : string.Join(Delimiter, this.RoleIds.Select(r => r.ToString())));
            info.AddValue("employeeGroupIds", this.EmployeeGroupIds == null ? null : string.Join(Delimiter, this.EmployeeGroupIds.Select(g => g.ToString())));
        }

        /// <summary>
        /// Checks if the security token is valid.
        /// </summary>
        /// <returns>The validity of this insance.</returns>
        public bool IsValid()
        {
            if (this.BrokerId == BrokerIdentifier.BadIdentifier)
            {
                return false;
            }

            if (this.UserId == UserAccountIdentifier.BadIdentifier)
            {
                return false;
            }

            if (this.Fullname == PersonName.BadName)
            {
                return false;
            }

            if (this.RoleIds == null)
            {
                if (this.InstanceVersion >= 201707)
                {
                    return false;
                }
            }
            else
            {
                if (this.RoleIds.Any(r => r == PrimaryKeyGuid.Invalid))
                {
                    return false;
                }
            }

            if (this.EmployeeGroupIds == null)
            {
                if (this.InstanceVersion >= 201707)
                {
                    return false;
                }
            }
            else
            {
                if (this.EmployeeGroupIds.Any(g => g == GroupIdentifier.BadIdentifier))
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Gets a string from info without throwing errors.
        /// </summary>
        /// <param name="info">Object from which the serialized fields are pulled.</param>
        /// <param name="name">The name of the field of interest.</param>
        /// <param name="defaultOnError">The default return value if there's an error.</param>
        /// <returns>The value from the serialized fields or the default on error.</returns>
        private string GetSafeString(SerializationInfo info, string name, string defaultOnError)
        {
            if (name == null)
            {
                return defaultOnError;
            }

            if (info == null)
            {
                return defaultOnError;
            }

            try
            {
                return info.GetString(name);
            }
            catch (InvalidCastException)
            {
                return defaultOnError;
            }
            catch (SerializationException)
            {
                return defaultOnError;
            }
        }

        /// <summary>
        /// Gets an int from info without throwing errors.
        /// </summary>
        /// <param name="info">Object from which the serialized fields are pulled.</param>
        /// <param name="name">The name of the field of interest.</param>
        /// <param name="defaultOnError">The default return value if there's an error.</param>
        /// <returns>The value from the serialized fields or the default on error.</returns>
        private int GetSafeInt32(SerializationInfo info, string name, int defaultOnError)
        {
            if (name == null)
            {
                return defaultOnError;
            }

            if (info == null)
            {
                return defaultOnError;
            }

            try
            {
                return info.GetInt32(name);
            }
            catch (InvalidCastException)
            {
                return defaultOnError;
            }
            catch (SerializationException)
            {
                return defaultOnError;
            }
        }
    }
}
