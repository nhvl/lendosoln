﻿namespace LqbGrammar.DataTypes
{
    using System;

    /// <summary>
    /// Encapsulate the concept of an identifier for brokers in the LQB system.
    /// </summary>
    public struct BrokerIdentifier : System.IEquatable<BrokerIdentifier>
    {
        /// <summary>
        /// An instance of the class that fails validation, can be used for initialization when necessary.
        /// </summary>
        public static readonly BrokerIdentifier BadIdentifier = new BrokerIdentifier(Guid.Empty);

        /// <summary>
        /// Initializes a new instance of the <see cref="BrokerIdentifier"/> struct.
        /// </summary>
        /// <param name="value">The value.</param>
        private BrokerIdentifier(Guid value)
        {
            this.Value = value;
        }

        /// <summary>
        /// Gets the encapsulated value, all identifiers are guids.
        /// </summary>
        /// <value>The guid of the broker id.</value>
        public Guid Value { get; private set; }

        /// <summary>
        /// Attempt to parse the input value into a validated broker identifier.
        /// </summary>
        /// <param name="value">Input value that will be parsed.</param>
        /// <returns>Valid broker identifier, or null.</returns>
        public static BrokerIdentifier? Create(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return null;
            }

            Guid guid;
            bool ok = Guid.TryParse(value, out guid);
            if (ok && guid != Guid.Empty)
            {
                return new BrokerIdentifier(guid);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Static method that checks for equality between two instances.
        /// </summary>
        /// <param name="lhs">Instance on the left side of the == operator.</param>
        /// <param name="rhs">Instance on the right side of the == operator.</param>
        /// <returns>True if the two instances are equal, false otherwise.</returns>
        public static new bool Equals(object lhs, object rhs)
        {
            if (!(lhs is BrokerIdentifier))
            {
                return false;
            }

            if (!(rhs is BrokerIdentifier))
            {
                return false;
            }

            return ((BrokerIdentifier)lhs).Equals((BrokerIdentifier)rhs);
        }

        /// <summary>
        /// The == operator is implemented here.
        /// </summary>
        /// <param name="lhs">Instance on the left of the == operator.</param>
        /// <param name="rhs">Instance on the right of the == operator.</param>
        /// <returns>True if the two instances are equal, false otherwise.</returns>
        public static bool operator ==(BrokerIdentifier lhs, BrokerIdentifier rhs)
        {
            return lhs.Equals(rhs);
        }

        /// <summary>
        /// The != operator is implemented here.
        /// </summary>
        /// <param name="lhs">Instance on the left of the != operator.</param>
        /// <param name="rhs">Instance on the right of the != operator.</param>
        /// <returns>True if the two instances are not equal, false otherwise.</returns>
        public static bool operator !=(BrokerIdentifier lhs, BrokerIdentifier rhs)
        {
            return !lhs.Equals(rhs);
        }

        /// <summary>
        /// Return the encapsulated value.
        /// </summary>
        /// <returns>Encapsulated value.</returns>
        public override string ToString()
        {
            return this.Value.ToString();
        }

        /// <summary>
        /// Implement IEquatable.
        /// </summary>
        /// <param name="other">Instance that is checked for equality with this instance.</param>
        /// <returns>True if other is equal to this instance, false otherwise.</returns>
        public bool Equals(BrokerIdentifier other)
        {
            return this.Value == other.Value;
        }

        /// <summary>
        /// Override the Equals method inherited from the Object class.
        /// </summary>
        /// <param name="obj">Instance that is checked for equality with this instance.</param>
        /// <returns>True if obj is equal to this instance, false otherwise.</returns>
        public override bool Equals(object obj)
        {
            if (!(obj is BrokerIdentifier))
            {
                return false;
            }

            return this.Equals((BrokerIdentifier)obj);
        }

        /// <summary>
        /// Override of the method inherited from the Object class.
        /// </summary>
        /// <returns>An integer that is the hash code for this instance.</returns>
        public override int GetHashCode()
        {
            return this.Value.GetHashCode();
        }
    }
}
