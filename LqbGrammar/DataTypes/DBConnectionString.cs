﻿namespace LqbGrammar.DataTypes
{
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;

    /// <summary>
    /// Represents a connection string to a SQL database.
    /// </summary>
    public struct DBConnectionString : IEquatable<DBConnectionString>
    {
        /// <summary>
        /// An invalid value that can be used for initialization.
        /// </summary>
        public static readonly DBConnectionString Invalid = new DBConnectionString(string.Empty);

        /// <summary>
        /// Initializes a new instance of the <see cref="DBConnectionString"/> struct.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        private DBConnectionString(string connectionString)
        {
            this.Value = connectionString;
        }

        /// <summary>
        /// Gets the connection string value.
        /// </summary>
        /// <value>The connection string value.</value>
        public string Value { get; private set; }

        /// <summary>
        /// The == operator is implemented here.
        /// </summary>
        /// <param name="lhs">Instance on the left of the == operator.</param>
        /// <param name="rhs">Instance on the right of the == operator.</param>
        /// <returns>True if the two instances are equal, false otherwise.</returns>
        public static bool operator ==(DBConnectionString lhs, DBConnectionString rhs)
        {
            return lhs.Equals(rhs);
        }

        /// <summary>
        /// The != operator is implemented here.
        /// </summary>
        /// <param name="lhs">Instance on the left of the != operator.</param>
        /// <param name="rhs">Instance on the right of the != operator.</param>
        /// <returns>True if the two instances are not equal, false otherwise.</returns>
        public static bool operator !=(DBConnectionString lhs, DBConnectionString rhs)
        {
            return !lhs.Equals(rhs);
        }

        /// <summary>
        /// Validate the input connection string and create it.
        /// </summary>
        /// <param name="connectionString">The connection string data to validate.</param>
        /// <returns>The valid connection string or null.</returns>
        public static DBConnectionString? Create(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
            {
                return null;
            }
            
            SqlConnectionStringBuilder builder;
            try
            {
                builder = new SqlConnectionStringBuilder(connectionString);
            }
            catch (SystemException exc)
                when (exc is KeyNotFoundException || exc is FormatException || exc is ArgumentException)
            {
                return null;
            }

            return new DBConnectionString(builder.ConnectionString);
        }

        /// <summary>
        /// Implementation of the IEquatable interface.
        /// </summary>
        /// <param name="other">Instance that is compared to this instance for equality.</param>
        /// <returns>True if the instances are equal, false otherwise.</returns>
        public bool Equals(DBConnectionString other)
        {
            return this.Value.Equals(other.Value, StringComparison.OrdinalIgnoreCase);
        }

        /// <summary>
        /// Override the Equals method inherited from the Object class.
        /// </summary>
        /// <param name="obj">Instance that is checked for equality with this instance.</param>
        /// <returns>True if obj is equal to this instance, false otherwise.</returns>
        public override bool Equals(object obj)
        {
            if (!(obj is DBConnectionString))
            {
                return false;
            }

            return this.Equals((DBConnectionString)obj);
        }

        /// <summary>
        /// Override of the method inherited from the Object class.
        /// </summary>
        /// <returns>An integer that is the hash code for this instance.</returns>
        public override int GetHashCode()
        {
            return this.Value.GetHashCode();
        }
    }
}
