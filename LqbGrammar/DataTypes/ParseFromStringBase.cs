﻿namespace LqbGrammar.DataTypes
{
    using System;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Manually implement methods for the ParseFromStringBase class.
    /// </summary>
    public partial class ParseFromStringBase
    {
        /// <summary>
        /// Attempt to parse the input string into a boolean.
        /// </summary>
        /// <param name="value">The input string to be parsed.</param>
        /// <returns>A boolean, or null.</returns>
        public virtual bool? TryParseBoolean(string value)
        {
            bool result;
            bool ok = bool.TryParse(value, out result);
            if (ok)
            {
                return result;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Parse the input string into a boolean.
        /// </summary>
        /// <param name="value">The input string to be parsed, or the default value if the string cannot be parsed.</param>
        /// <param name="defaultValue">The value to return if the input string cannot be parsed.</param>
        /// <returns>The boolean, or the default value.</returns>
        public virtual bool ParseBoolean(string value, bool defaultValue)
        {
            var parsed = this.TryParseBoolean(value);
            return parsed == null ? defaultValue : parsed.Value;
        }

        /// <summary>
        /// Attempt to parse the input string into a DataObjectIdentifier instance.
        /// </summary>
        /// <typeparam name="TValueKind">The <see cref="DataObjectKind"/> of the value being identified.</typeparam>
        /// <typeparam name="TIdValue">The type of the identifier used to refer to the value.</typeparam>
        /// <param name="value">The input string to be parsed.</param>
        /// <returns>A DataObjectIdentifier, or null.</returns>
        public virtual DataObjectIdentifier<TValueKind, TIdValue>? TryParseDataObjectIdentifier<TValueKind, TIdValue>(string value)
            where TValueKind : DataObjectKind
            where TIdValue : struct, IEquatable<TIdValue>
        {
            return DataObjectIdentifier<TValueKind, TIdValue>.Create(value);
        }

        /// <summary>
        /// Parse the input string and return the result, or the default value if the string cannot be parsed.
        /// </summary>
        /// <typeparam name="TValueKind">The <see cref="DataObjectKind"/> of the value being identified.</typeparam>
        /// <typeparam name="TIdValue">The type of the identifier used to refer to the value.</typeparam>
        /// <param name="value">The input string to be parsed.</param>
        /// <param name="defaultValue">The value to return if the input string cannot be parsed.</param>
        /// <returns>A DataObjectIdentifier instance.</returns>
        public virtual DataObjectIdentifier<TValueKind, TIdValue> ParseDataObjectIdentifier<TValueKind, TIdValue>(string value, DataObjectIdentifier<TValueKind, TIdValue> defaultValue)
            where TValueKind : DataObjectKind
            where TIdValue : struct, IEquatable<TIdValue>
        {
            var parsed = this.TryParseDataObjectIdentifier<TValueKind, TIdValue>(value);
            return parsed == null ? defaultValue : parsed.Value;
        }

        /// <summary>
        /// Parse the input string into an enumeration value.
        /// </summary>
        /// <typeparam name="E">The expected type, which must be an enumeration.</typeparam>
        /// <param name="value">The input string to be parsed.</param>
        /// <returns>The enumeration value, or null.</returns>
        public virtual E? TryParseEnum<E>(string value)
            where E : struct
        {
            if (!typeof(E).IsEnum)
            {
                throw new DeveloperException(ErrorMessage.SystemError);
            }

            E parsed;
            bool ok = Enum.TryParse<E>(value, out parsed);
            if (ok)
            {
                return parsed;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Parse the input string and return the result, or the default value if the string cannot be parsed.
        /// </summary>
        /// <typeparam name="E">The expected type, which should be an enumeration.</typeparam>
        /// <param name="value">The input string to be parsed.</param>
        /// <param name="defaultValue">The value to return if the input string cannot be parsed.</param>
        /// <returns>The parsed enumeration value, or the default value.</returns>
        public virtual E ParseEnum<E>(string value, E defaultValue)
            where E : struct
        {
            var parsed = this.TryParseEnum<E>(value);
            return parsed == null ? defaultValue : parsed.Value;
        }
    }
}
