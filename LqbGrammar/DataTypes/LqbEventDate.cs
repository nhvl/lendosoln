﻿namespace LqbGrammar.DataTypes
{
    using System;

    /// <summary>
    /// Encapsulate the concept of an event datetime in the LQB system.
    /// It is important to realize that the stored time is in UTC.
    /// </summary>
    public struct LqbEventDate : System.IEquatable<LqbEventDate>
    {
        /// <summary>
        /// An instance of the class that fails validation, can be used for initialization when necessary.
        /// </summary>
        public static readonly LqbEventDate BadDate = new LqbEventDate(DateTime.SpecifyKind(DateTime.MinValue, DateTimeKind.Utc));

        /// <summary>
        /// Initial development on the first LQB application started sometime in 2002.  As such the 
        /// earliest possible test data should carry timestamps for that year.
        /// </summary>
        private static readonly DateTime MinValue = new DateTime(2002, 1, 1);

        /// <summary>
        /// Encapsulated value, which is always of kind UTC.
        /// </summary>
        private DateTime value;

        /// <summary>
        /// Initializes a new instance of the <see cref="LqbEventDate"/> struct.
        /// </summary>
        /// <param name="value">The value.</param>
        private LqbEventDate(DateTime value)
        {
            this.value = value;
        }

        /// <summary>
        /// Ensure the datetime is within a valid range for an LQB event, and that the time used was UTC.
        /// </summary>
        /// <param name="value">The datetime to test.</param>
        /// <returns>Valid event date, or null.</returns>
        public static LqbEventDate? Create(DateTime value)
        {
            if (value.Kind != DateTimeKind.Utc)
            {
                return null;
            }

            if (value != DateTime.MinValue)
            {
                if (value < MinValue)
                {
                    return null;
                }
                else
                {
                    return new LqbEventDate(value);
                }
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Static method that checks for equality between two instances.
        /// </summary>
        /// <param name="lhs">Instance on the left side of the == operator.</param>
        /// <param name="rhs">Instance on the right side of the == operator.</param>
        /// <returns>True if the two instances are equal, false otherwise.</returns>
        public static new bool Equals(object lhs, object rhs)
        {
            if (!(lhs is LqbEventDate))
            {
                return false;
            }

            if (!(rhs is LqbEventDate))
            {
                return false;
            }

            return ((LqbEventDate)lhs).Equals((LqbEventDate)rhs);
        }

        /// <summary>
        /// The == operator is implemented here.
        /// </summary>
        /// <param name="lhs">Instance on the left of the == operator.</param>
        /// <param name="rhs">Instance on the right of the == operator.</param>
        /// <returns>True if the two instances are equal, false otherwise.</returns>
        public static bool operator ==(LqbEventDate lhs, LqbEventDate rhs)
        {
            return lhs.Equals(rhs);
        }

        /// <summary>
        /// The != operator is implemented here.
        /// </summary>
        /// <param name="lhs">Instance on the left of the != operator.</param>
        /// <param name="rhs">Instance on the right of the != operator.</param>
        /// <returns>True if the two instances are not equal, false otherwise.</returns>
        public static bool operator !=(LqbEventDate lhs, LqbEventDate rhs)
        {
            return !lhs.Equals(rhs);
        }        

        /// <summary>
        /// Return the default string representation of the datetime value.
        /// </summary>
        /// <returns>The string value of the date.</returns>
        public override string ToString()
        {
            return this.value.ToString();
        }

        /// <summary>
        /// Return the string value of the datetime using the given format.
        /// </summary>
        /// <param name="format">The format string, same values as used for System.DateTime.</param>
        /// <returns>The string value of the date.</returns>
        public string ToString(string format)
        {
            return this.value.ToString(format);
        }

        /// <summary>
        /// Return the string value of the datetime using the given format provider.
        /// </summary>
        /// <param name="formatProvider">The format provider used to control the generation of the formatted datetime string.</param>
        /// <returns>The string value of the date.</returns>
        public string ToString(IFormatProvider formatProvider)
        {
            return this.value.ToString(formatProvider);
        }

        /// <summary>
        /// Return the string value of the datetime using the given format and format provider.
        /// </summary>
        /// <param name="format">The format string, same values as used for System.DateTime.</param>
        /// <param name="formatProvider">The format provider used to control the generation of the formatted datetime string.</param>
        /// <returns>The string value of the date.</returns>
        public string ToString(string format, IFormatProvider formatProvider)
        {
            return this.value.ToString(format, formatProvider);
        }

        /// <summary>
        /// Implement IEquatable.
        /// </summary>
        /// <param name="other">Instance that is checked for equality with this instance.</param>
        /// <returns>True if other is equal to this instance, false otherwise.</returns>
        public bool Equals(LqbEventDate other)
        {
            return this.value == other.value;
        }

        /// <summary>
        /// Override the Equals method inherited from the Object class.
        /// </summary>
        /// <param name="obj">Instance that is checked for equality with this instance.</param>
        /// <returns>True if obj is equal to this instance, false otherwise.</returns>
        public override bool Equals(object obj)
        {
            if (!(obj is LqbEventDate))
            {
                return false;
            }

            return this.Equals((LqbEventDate)obj);
        }

        /// <summary>
        /// Override of the method inherited from the Object class.
        /// </summary>
        /// <returns>An integer that is the hash code for this instance.</returns>
        public override int GetHashCode()
        {
            return this.value.GetHashCode();
        }
    }
}
