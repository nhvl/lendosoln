﻿namespace LqbGrammar.DataTypes
{
    using System;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// A ZonedDateTime represents an event that occurs within a time zone
    /// at a precise date and time, however the time is the reading on a clock
    /// and so can be ambiguous if it occurs within an hour of a Fall Back 
    /// daylight savings time event.  For this reason ZonedDateTimes are not comparable.
    /// </summary>
    public struct ZonedDateTime : IEquatable<ZonedDateTime>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ZonedDateTime"/> struct.
        /// </summary>
        /// <param name="datetime">The datetime component.</param>
        /// <param name="timezone">The time zone component.</param>
        private ZonedDateTime(UnzonedDateTime datetime, LqbTimeZone timezone)
        {
            this.DatetimeComponent = datetime;
            this.Zone = timezone;
        }

        /// <summary>
        /// Gets the equivalent DateTimeOffset for this datetime.
        /// </summary>
        /// <value>The equivalent DateTimeOffset for this datetime.</value>
        public DateTimeOffset DateTime
        {
            get
            {
                var info = (TimeZoneInfo)this.Zone;
                var datetime = this.DatetimeComponent.DateTime;
                var offset = info.GetUtcOffset(datetime);
                return new DateTimeOffset(datetime, offset);
            }
        }

        /// <summary>
        /// Gets or sets the datetime component of the ZonedDateTime.
        /// </summary>
        /// <value>The datetime component of the ZonedDateTime.</value>
        private UnzonedDateTime DatetimeComponent { get; set; }

        /// <summary>
        /// Gets or sets the time zone component of the ZonedDateTime.
        /// </summary>
        /// <value>The time zone component of the ZonedDateTime.</value>
        private LqbTimeZone Zone { get; set; }

        /// <summary>
        /// Create a ZonedDateTime from a datetime and timezone.
        /// </summary>
        /// <param name="datetime">The datetime.</param>
        /// <param name="timezone">The timezone.</param>
        /// <returns>A ZonedDateTime instance, or null.</returns>
        public static ZonedDateTime? Create(DateTime datetime, LqbTimeZone timezone)
        {
            var unzoned = UnzonedDateTime.Create(datetime);
            if (unzoned == null)
            {
                return null;
            }

            return new ZonedDateTime(unzoned.Value, timezone);
        }

        /// <summary>
        /// Create a ZonedDateTime from a string representation.
        /// </summary>
        /// <param name="value">The string representation.</param>
        /// <returns>A ZonedDateTime instance, or null.</returns>
        public static ZonedDateTime? Create(string value)
        {
            string working = value.Trim();
            int index = working.LastIndexOf(' ');
            if (index <= 0)
            {
                return null;
            }

            try
            {
                var timezone = LqbTimeZone.Create(working.Substring(index + 1));
                var datetime = UnzonedDateTime.Create(working.Substring(0, index).TrimEnd());
                if (datetime == null)
                {
                    return null;
                }
                else
                {
                    return new ZonedDateTime(datetime.Value, timezone);
                }
            }
            catch (LqbException)
            {
                return null;
            }
        }

        /// <summary>
        /// Implement the equality operator for ZonedDateTimes.
        /// </summary>
        /// <param name="lhs">The ZonedDateTime on the left-hand side of the == operator.</param>
        /// <param name="rhs">The ZonedDateTime on the right-hand side of the == operator.</param>
        /// <returns>True if the ZonedDateTimes match, false otherwise.</returns>
        public static bool operator ==(ZonedDateTime lhs, ZonedDateTime rhs)
        {
            return lhs.Equals(rhs);
        }

        /// <summary>
        /// Implement the inequality operator for ZonedDateTimes.
        /// </summary>
        /// <param name="lhs">The ZonedDateTime on the left-hand side of the != operator.</param>
        /// <param name="rhs">The ZonedDateTime on the right-hand side of the != operator.</param>
        /// <returns>True if the ZonedDateTimes do not match, false otherwise.</returns>
        public static bool operator !=(ZonedDateTime lhs, ZonedDateTime rhs)
        {
            return !lhs.Equals(rhs);
        }

        /// <summary>
        /// Override the implementation inherited from System.Object.
        /// </summary>
        /// <param name="obj">The object being compared with this instance.</param>
        /// <returns>True if the object is an ZonedDateTime and the two datetimes are equal, false otherwise.</returns>
        public override bool Equals(object obj)
        {
            return obj is ZonedDateTime && this.Equals((ZonedDateTime)obj);
        }

        /// <summary>
        /// Implement IEquatable&lt;ZonedDateTime&gt;.
        /// </summary>
        /// <param name="other">The datetime being compared with this instance.</param>
        /// <returns>True if the two datetimes are equal, false otherwise.</returns>
        public bool Equals(ZonedDateTime other)
        {
            var thisUTC = this.DateTime.UtcDateTime;
            var otherUTC = other.DateTime.UtcDateTime;
            return thisUTC.Equals(otherUTC);
        }

        /// <summary>
        /// Override the implementation inherited from System.Object.
        /// </summary>
        /// <returns>A hash code for the current instance.</returns>
        public override int GetHashCode()
        {
            return this.DateTime.UtcDateTime.GetHashCode();
        }

        /// <summary>
        /// Override the implementation inherited from System.Object.
        /// </summary>
        /// <returns>A string representation of this instance.</returns>
        public override string ToString()
        {
            return string.Format("{0} {1}", this.DatetimeComponent.ToString(), this.Zone.GeneralAbbreviation);
        }
    }
}
