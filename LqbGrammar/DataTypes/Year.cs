﻿namespace LqbGrammar.DataTypes
{
    using System;
    using LqbGrammar.Exceptions;

    using storage = System.Int32;

    /// <summary>
    /// Encapsulate the concept of a single calendar year, and NOT a quantity of time measured in years.
    /// </summary>
    public struct Year : ISemanticType, IEquatable<Year>, IComparable, IComparable<Year>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Year"/> struct.
        /// </summary>
        /// <param name="value">The calendar year that this instance represents.</param>
        private Year(storage value)
        {
            this.Value = value;
        }

        /// <summary>
        /// Gets the encapsulated value for the calendar year.
        /// </summary>
        /// <value>The encapsulated value for the calendar year.</value>
        public storage Value { get; private set; }

        /// <summary>
        /// Gets the encapsulated year.
        /// </summary>
        object ISemanticType.Value => this.Value;

        /// <summary>
        /// Parse a representation of the year's value into a Year instance.
        /// </summary>
        /// <param name="representation">A string representation of the year.</param>
        /// <returns>A year instance, or null if the string representation is invalid.</returns>
        public static Year? Create(string representation)
        {
            if (string.IsNullOrEmpty(representation))
            {
                return null;
            }

            storage year;
            if (!storage.TryParse(representation, out year))
            {
                return null;
            }

            return new Year(year);
        }

        /// <summary>
        /// Create a Year instance from an integer value.
        /// </summary>
        /// <param name="value">The integer value for the year.</param>
        /// <returns>A Year instance, or null.</returns>
        public static Year? Create(int value)
        {
            if (value > storage.MaxValue || value < storage.MinValue)
            {
                return null;
            }

            storage year = (storage)value;
            return new Year(year);
        }

        /// <summary>
        /// The == operator is implemented here.
        /// </summary>
        /// <param name="lhs">Instance on the left of the == operator.</param>
        /// <param name="rhs">Instance on the right of the == operator.</param>
        /// <returns>True if the two instances are equal, false otherwise.</returns>
        public static bool operator ==(Year lhs, Year rhs)
        {
            return lhs.Equals(rhs);
        }

        /// <summary>
        /// The != operator is implemented here.
        /// </summary>
        /// <param name="lhs">Instance on the left of the != operator.</param>
        /// <param name="rhs">Instance on the right of the != operator.</param>
        /// <returns>True if the two instances are not equal, false otherwise.</returns>
        public static bool operator !=(Year lhs, Year rhs)
        {
            return !lhs.Equals(rhs);
        }

        /// <summary>
        /// The less than operator is implemented here.
        /// </summary>
        /// <param name="lhs">Operand on the left of the operator.</param>
        /// <param name="rhs">Operand on the right of the operator.</param>
        /// <returns>The result of applying the operator to the two operands.</returns>
        public static bool operator <(Year lhs, Year rhs)
        {
            return lhs.CompareTo(rhs) < 0;
        }

        /// <summary>
        /// The less than or equal operator is implemented here.
        /// </summary>
        /// <param name="lhs">Operand on the left of the operator.</param>
        /// <param name="rhs">Operand on the right of the operator.</param>
        /// <returns>The result of applying the operator to the two operands.</returns>
        public static bool operator <=(Year lhs, Year rhs)
        {
            return lhs.CompareTo(rhs) <= 0;
        }

        /// <summary>
        /// The greater than operator is implemented here.
        /// </summary>
        /// <param name="lhs">Operand on the left of the operator.</param>
        /// <param name="rhs">Operand on the right of the operator.</param>
        /// <returns>The result of applying the operator to the two operands.</returns>
        public static bool operator >(Year lhs, Year rhs)
        {
            return lhs.CompareTo(rhs) > 0;
        }

        /// <summary>
        /// The greater than or equal operator is implemented here.
        /// </summary>
        /// <param name="lhs">Operand on the left of the operator.</param>
        /// <param name="rhs">Operand on the right of the operator.</param>
        /// <returns>The result of applying the operator to the two operands.</returns>
        public static bool operator >=(Year lhs, Year rhs)
        {
            return lhs.CompareTo(rhs) >= 0;
        }

        /// <summary>
        /// Override the Equals method inherited from the Object class.
        /// </summary>
        /// <param name="obj">Object instance that is checked for equality with this instance.</param>
        /// <returns>True if obj is equal to this instance, false otherwise.</returns>
        public override bool Equals(object obj)
        {
            if (!(obj is Year))
            {
                return false;
            }

            return this.Equals((Year)obj);
        }

        /// <summary>
        /// Implementation of the IEquatable interface.
        /// </summary>
        /// <param name="other">Instance that is compared to this instance for equality.</param>
        /// <returns>True if the instances are equal, false otherwise.</returns>
        public bool Equals(Year other)
        {
            return this.CompareTo(other) == 0;
        }

        /// <summary>
        /// Compare this instance with another object.
        /// </summary>
        /// <param name="obj">The other object.</param>
        /// <returns>The result of the comparison, or an exception is thrown if the other object is not an instance of the Instant struct.</returns>
        public int CompareTo(object obj)
        {
            if (obj is Year)
            {
                return this.CompareTo((Year)obj);
            }
            else
            {
                throw new DeveloperException(ErrorMessage.SystemError);
            }
        }

        /// <summary>
        /// Compare this instance with another Year.
        /// </summary>
        /// <param name="other">The other Year.</param>
        /// <returns>The result of the comparison, zero if equal, greater than zero if this instance is greater, less than zero of this instance is lesser.</returns>
        public int CompareTo(Year other)
        {
            return this.Value.CompareTo(other.Value);
        }

        /// <summary>
        /// Override of the method inherited from the Object class.
        /// </summary>
        /// <returns>An integer that is the hash code for this instance.</returns>
        public override int GetHashCode()
        {
            return this.Value.GetHashCode();
        }

        /// <summary>
        /// Override of t he method inherited from the Object class.
        /// </summary>
        /// <returns>A string representation of this instance.</returns>
        public override string ToString()
        {
            return this.Value.ToString();
        }
    }
}
