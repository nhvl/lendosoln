﻿namespace LqbGrammar.DataTypes
{
    using System;
    using System.Collections.Generic;
    using LqbGrammar.Exceptions;
    using LqbGrammar.Validation;

    /// <summary>
    /// Encapsulate a stored procedure name so that it can be validated prior to use.
    /// </summary>
    public struct StoredProcedureName : System.IEquatable<StoredProcedureName>
    {
        /// <summary>
        /// An instance of a bad stored procedure name that can be used for variable initialization.
        /// </summary>
        public static readonly StoredProcedureName Invalid = new StoredProcedureName(string.Empty);

        /// <summary>
        /// The stored procedure name.
        /// </summary>
        private string procedureName;

        /// <summary>
        /// Initializes a new instance of the <see cref="StoredProcedureName"/> struct.
        /// </summary>
        /// <param name="name">The stored procedure name.</param>
        private StoredProcedureName(string name)
        {
            this.procedureName = name;
        }

        /// <summary>
        /// Parse the input stored procedure name and validate it.
        /// </summary>
        /// <param name="name">The stored procedure name that is parsed.</param>
        /// <returns>The validated stored procedure name, or null.</returns>
        public static StoredProcedureName? Create(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                return null;
            }

            RegularExpressionResult result = RegularExpressionEngine.Execute(name, RegularExpressionString.StoredProcedureName);
            if (!result.IsMatch())
            {
                return null;
            }

            return new StoredProcedureName(name);
        }

        /// <summary>
        /// Compare to objects for equality and that they are instances of the StoredProcedureName class.
        /// </summary>
        /// <param name="lhs">The object on the left side of the == operator.</param>
        /// <param name="rhs">The object on the right side of the == operator.</param>
        /// <returns>True if both objects are instances of the StoredProcedureName class and equal to each other.</returns>
        public static new bool Equals(object lhs, object rhs)
        {
            if (!(lhs is StoredProcedureName))
            {
                return false;
            }

            if (!(rhs is StoredProcedureName))
            {
                return false;
            }

            return ((StoredProcedureName)lhs).Equals((StoredProcedureName)rhs);
        }

        /// <summary>
        /// The equalitiy operator.
        /// </summary>
        /// <param name="lhs">The instance that is on the left side of the == operator.</param>
        /// <param name="rhs">The instance that is on the right side of the == operator.</param>
        /// <returns>True if the instances are equal, false otherwise.</returns>
        public static bool operator ==(StoredProcedureName lhs, StoredProcedureName rhs)
        {
            return lhs.Equals(rhs);
        }

        /// <summary>
        /// The inequality operator.
        /// </summary>
        /// <param name="lhs">The instance that is on the left side of the != operator.</param>
        /// <param name="rhs">The instance that is on the right side of the != operator.</param>
        /// <returns>True if the two instances are not equal, false otherwise.</returns>
        public static bool operator !=(StoredProcedureName lhs, StoredProcedureName rhs)
        {
            return !lhs.Equals(rhs);
        }

        /// <summary>
        /// Override the implementation inherited from the Object class.
        /// </summary>
        /// <returns>The stored procedure name.</returns>
        public override string ToString()
        {
            return this.procedureName;
        }

        /// <summary>
        /// Compare this instance with another instance for equality.
        /// </summary>
        /// <param name="other">The other instance.</param>
        /// <returns>True if the two instances are equal.</returns>
        public bool Equals(StoredProcedureName other)
        {
            return this.procedureName == other.procedureName;
        }

        /// <summary>
        /// Compare this instance with another object for equality.
        /// </summary>
        /// <param name="obj">The other object.</param>
        /// <returns>True if the other object is an instance of the StoredProcedureName class and it is equal to this instance.</returns>
        public override bool Equals(object obj)
        {
            if (!(obj is StoredProcedureName))
            {
                return false;
            }

            return Equals((StoredProcedureName)obj);
        }

        /// <summary>
        /// Override the implementation inherited from the Object class.
        /// </summary>
        /// <returns>An integer that is the hash of this object.</returns>
        public override int GetHashCode()
        {
            return this.procedureName.GetHashCode();
        }
    }
}
