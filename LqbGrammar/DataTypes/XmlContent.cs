﻿namespace LqbGrammar.DataTypes
{
    using System.IO;
    using System.Text;
    using System.Xml;

    /// <summary>
    /// Represents XML content that to write to a stream (for an HTTP request, for instance).
    /// </summary>
    public class XmlContent : IStreamableContent
    {
        /// <summary>
        /// The XML content.
        /// </summary>
        private LqbXmlElement content;

        /// <summary>
        /// The XML writer settings to use for writing the content.
        /// </summary>
        private XmlWriterSettings writerSettings;

        /// <summary>
        /// Initializes a new instance of the <see cref="XmlContent"/> class.
        /// </summary>
        /// <param name="content">The XML content.</param>
        /// <param name="writerSettings">The XML writer settings to use when writing the content.</param>
        public XmlContent(LqbXmlElement content, XmlWriterSettings writerSettings = null)
        {
            this.content = content;
            this.writerSettings = writerSettings ?? new XmlWriterSettings() { Encoding = new UTF8Encoding(false) };
        }

        /// <summary>
        /// Gets the length of the content, in bytes.
        /// For XML, this is difficult to work out without actually serializing the data, and at that point it can be done by the Stream if necessary.
        /// </summary>
        /// <returns>The length of the content to be written.</returns>
        public long? GetContentLengthBytes()
        {
            return null;
        }

        /// <summary>
        /// Writes the content to the given stream.
        /// </summary>
        /// <param name="writeTo">The stream to write to.</param>
        public void WriteToStream(Stream writeTo)
        {
            using (var xw = XmlWriter.Create(writeTo, this.writerSettings))
            {
                this.content.Contained.Save(xw);
            }
        }
    }
}
