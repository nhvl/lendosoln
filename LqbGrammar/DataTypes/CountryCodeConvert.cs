﻿namespace LqbGrammar.DataTypes
{
    using LqbGrammar.Utils.Country;

    /// <summary>
    /// Conversion methods between ISO-3166-1 alpha-3 and other country code standards.
    /// </summary>
    public static class CountryCodeConvert
    {
        /// <summary>
        /// Retrieve the name, as used in the LQB system, from the country code.
        /// </summary>
        /// <remarks>
        /// It is expected this will be needed when exporting data.
        /// </remarks>
        /// <param name="iso3">Country code according to the ISO-3166-1 alpha-3 standard.</param>
        /// <returns>The name of the country.</returns>
        public static Country? ToCountryName(this CountryCodeIso3 iso3)
        {
            var nation = NationRegistry.GetNationFromCountryCode_ISO_3166_3(iso3.ToString());
            if (nation == null)
            {
                return null;
            }

            var name = NationRegistry.GetData(nation).Country;
            return Country.Create(name).Value;
        }

        /// <summary>
        /// Convert ISO-3166-1 alpha-3 to ISO-3166-1 alpha-2.
        /// </summary>
        /// <remarks>
        /// It is expected this will be needed when exporting data.
        /// </remarks>
        /// <param name="iso3">Country code according to the ISO-3166-1 alpha-3 standard.</param>
        /// <returns>The equivalent country code according to the ISO-3166-1 alpha-2 standard, or null.</returns>
        public static CountryCodeIso2? ToIso2(this CountryCodeIso3 iso3)
        {
            var nation = NationRegistry.GetNationFromCountryCode_ISO_3166_3(iso3.ToString());
            if (nation == null)
            {
                return null;
            }

            var code = NationRegistry.GetData(nation).Iso_3166_2;
            return string.IsNullOrEmpty(code) ? (CountryCodeIso2?)null : CountryCodeIso2.Create(code);
        }

        /// <summary>
        /// Convert ISO-3166-1 alpha-2 to ISO-3166-1 alpha-3.
        /// </summary>
        /// <remarks>
        /// It is expected this will be needed when importing data.
        /// </remarks>
        /// <param name="iso2">Country code according to the ISO-3166-1 alpha-2 standard.</param>
        /// <returns>The equivalent country code according to the ISO-3166-1 alpha-3 standard, or null.</returns>
        public static CountryCodeIso3? ToIso3(this CountryCodeIso2 iso2)
        {
            var nation = NationRegistry.GetNationFromCountryCode_ISO_3166_2(iso2.ToString());
            if (nation == null)
            {
                return null;
            }

            var code = NationRegistry.GetData(nation).Iso_3166_3;
            return string.IsNullOrEmpty(code) ? (CountryCodeIso3?)null : CountryCodeIso3.Create(code).Value;
        }
    }
}
