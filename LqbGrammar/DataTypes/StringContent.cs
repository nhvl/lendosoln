﻿namespace LqbGrammar.DataTypes
{
    using System.IO;
    using System.Text;

    /// <summary>
    /// Represents string content that to write to a stream (for an HTTP request, for instance).
    /// </summary>
    public class StringContent : IStreamableContent
    {
        /// <summary>
        /// The string content.
        /// </summary>
        private string content;

        /// <summary>
        /// The encoding to use for writing the content.
        /// </summary>
        private Encoding encoding;

        /// <summary>
        /// The length of <see cref="content"/>, when encoded using UTF-8.
        /// </summary>
        private long contentByteLength;

        /// <summary>
        /// Initializes a new instance of the <see cref="StringContent"/> class.
        /// </summary>
        /// <param name="content">The string content.</param>
        /// <param name="encoding">The encoding to use when writing the content.</param>
        /// <param name="length">The length of the content when encoded into bytes. If not provided, this will be calculated.</param>
        public StringContent(string content, Encoding encoding = null, long? length = null)
        {
            this.content = content;
            this.encoding = encoding ?? new UTF8Encoding(false);
            if (this.content != null)
            {
                this.contentByteLength = length ?? this.encoding.GetBytes(this.content).Length;
            }
        }

        /// <summary>
        /// Implicitly converts this class to a string.
        /// </summary>
        /// <param name="content">The StringContent to convert to string.</param>
        public static implicit operator string(StringContent content)
        {
            return content.content;
        }

        /// <summary>
        /// Implicitly converts a string to an instance of this class.
        /// </summary>
        /// <param name="content">The string to convert to StringContent.</param>
        public static implicit operator StringContent(string content)
        {
            return new StringContent(content);
        }

        /// <summary>
        /// Gets the length of the internal string.
        /// </summary>
        /// <returns>The length of the content to be written.</returns>
        public long? GetContentLengthBytes()
        {
            return this.contentByteLength;
        }

        /// <summary>
        /// Writes the string to the given stream.
        /// </summary>
        /// <param name="writeTo">The stream to write to.</param>
        public void WriteToStream(Stream writeTo)
        {
            using (var sw = new StreamWriter(writeTo, this.encoding))
            {
                sw.Write(this.content);
            }
        }
    }
}
