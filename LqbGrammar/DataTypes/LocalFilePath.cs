﻿namespace LqbGrammar.DataTypes
{
    using System;
    using System.IO;
    using System.Security;

    /// <summary>
    /// Represents a valid an absolute local file path. A valid file path does not mean a file or directory exists.
    /// </summary>
    public struct LocalFilePath : System.IEquatable<LocalFilePath>
    {
        /// <summary>
        /// An invalid value that can be used for initialization.
        /// </summary>
        public static readonly LocalFilePath Invalid = new LocalFilePath(string.Empty);

        /// <summary>
        /// Initializes a new instance of the <see cref="LocalFilePath"/> struct.
        /// </summary>
        /// <param name="path">The file or directory for which to obtain absolute path information.</param>
        private LocalFilePath(string path)
        {
            this.Value = path;
        }

        /// <summary>
        /// Gets the absolute path.
        /// </summary>
        /// <value>The absolute path.</value>
        public string Value { get; private set; }

        /// <summary>
        /// Validate the input string.
        /// </summary>
        /// <param name="path">The path to validate. Note this does not test if the path exists.</param>
        /// <returns>The valid path, or null.</returns>
        public static LocalFilePath? Create(string path)
        {
            if (string.IsNullOrEmpty(path))
            {
                return null;
            }

            // 11/14/2016 - dd - For now I will rely on .NET Framework Path.GetFullPath to perform the validation of path.
            string validPath = null;

            try
            {
                validPath = Path.GetFullPath(path);
            }
            catch (ArgumentException)
            {
                return null;
            }
            catch (SecurityException)
            {
                return null;
            }
            catch (NotSupportedException)
            {
                return null;
            }
            catch (PathTooLongException)
            {
                return null;
            }

            if (string.IsNullOrEmpty(validPath))
            {
                return null;
            }
            else
            {
                return new LocalFilePath(validPath);
            }
        }

        /// <summary>
        /// Compare two instances for equality.
        /// </summary>
        /// <param name="lhs">Instance on the left side.</param>
        /// <param name="rhs">Instance on the right side.</param>
        /// <returns>True if the instances are equal, false otherwise.</returns>
        public static new bool Equals(object lhs, object rhs)
        {
            if (!(lhs is LocalFilePath))
            {
                return false;
            }

            if (!(rhs is LocalFilePath))
            {
                return false;
            }

            return ((LocalFilePath)lhs).Equals((LocalFilePath)rhs);
        }

        /// <summary>
        /// Implementation of the == operator.
        /// </summary>
        /// <param name="lhs">Instance on the left side.</param>
        /// <param name="rhs">Instance on the right side.</param>
        /// <returns>True if the instances are equal, false otherwise.</returns>
        public static bool operator ==(LocalFilePath lhs, LocalFilePath rhs)
        {
            return lhs.Equals(rhs);
        }

        /// <summary>
        /// Implementation of the != operator.
        /// </summary>
        /// <param name="lhs">Instance on the left side.</param>
        /// <param name="rhs">Instance on the right side.</param>
        /// <returns>True if the instances are not equal, false otherwise.</returns>
        public static bool operator !=(LocalFilePath lhs, LocalFilePath rhs)
        {
            return !lhs.Equals(rhs);
        }

        /// <summary>
        /// Implementation of the IEquatable interface.
        /// </summary>
        /// <param name="other">Instance that is compared to this instance for equality.</param>
        /// <returns>True if the instances are equal, false otherwise.  The comparison is case insensitive since Windows paths are case insensitive.</returns>
        public bool Equals(LocalFilePath other)
        {
            return this.Value.Equals(other.Value, StringComparison.OrdinalIgnoreCase);
        }

        /// <summary>
        /// Override the method inherited from the Object class.
        /// </summary>
        /// <param name="obj">The instance that will be compared with this instance for equality.</param>
        /// <returns>True if the instances are equal, false otherwise.</returns>
        public override bool Equals(object obj)
        {
            if (!(obj is LocalFilePath))
            {
                return false;
            }

            return Equals((LocalFilePath)obj);
        }

        /// <summary>
        /// Override of the method inherited from the Object class.
        /// </summary>
        /// <returns>An integer that is the hash of this instance.</returns>
        public override int GetHashCode()
        {
            return this.Value.ToLower().GetHashCode();
        }
    }
}