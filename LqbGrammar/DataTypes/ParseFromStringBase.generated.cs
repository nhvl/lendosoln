﻿// <auto-generated />
namespace LqbGrammar.DataTypes
{
    /// <summary>
    /// Generate semantic types by parsing input strings.
    /// </summary>
    public partial class ParseFromStringBase : IParseFromString
    {
		public virtual AddressUnitType? TryParseAddressUnitType(string value)
		{
			return AddressUnitType.Create(value);
		}

		public virtual AddressUnitType ParseAddressUnitType(string value, AddressUnitType defaultValue)
		{
			var parsed = this.TryParseAddressUnitType(value);
			return parsed == null ? defaultValue : parsed.Value;
		}

		public virtual AddressUnitIdentifier? TryParseAddressUnitIdentifier(string value)
		{
			return AddressUnitIdentifier.Create(value);
		}

		public virtual AddressUnitIdentifier ParseAddressUnitIdentifier(string value, AddressUnitIdentifier defaultValue)
		{
			var parsed = this.TryParseAddressUnitIdentifier(value);
			return parsed == null ? defaultValue : parsed.Value;
		}

		public virtual AdministrativeArea? TryParseAdministrativeArea(string value)
		{
			return AdministrativeArea.Create(value);
		}

		public virtual AdministrativeArea ParseAdministrativeArea(string value, AdministrativeArea defaultValue)
		{
			var parsed = this.TryParseAdministrativeArea(value);
			return parsed == null ? defaultValue : parsed.Value;
		}

		public virtual ApproximateDate? TryParseApproximateDate(string value)
		{
			return ApproximateDate.Create(value);
		}

		public virtual ApproximateDate ParseApproximateDate(string value, ApproximateDate defaultValue)
		{
			var parsed = this.TryParseApproximateDate(value);
			return parsed == null ? defaultValue : parsed.Value;
		}

		public virtual BankAccountNumber? TryParseBankAccountNumber(string value)
		{
			return BankAccountNumber.Create(value);
		}

		public virtual BankAccountNumber ParseBankAccountNumber(string value, BankAccountNumber defaultValue)
		{
			var parsed = this.TryParseBankAccountNumber(value);
			return parsed == null ? defaultValue : parsed.Value;
		}

		public virtual BooleanKleene? TryParseBooleanKleene(string value)
		{
			return BooleanKleene.Create(value);
		}

		public virtual BooleanKleene ParseBooleanKleene(string value, BooleanKleene defaultValue)
		{
			var parsed = this.TryParseBooleanKleene(value);
			return parsed == null ? defaultValue : parsed.Value;
		}

		public virtual City? TryParseCity(string value)
		{
			return City.Create(value);
		}

		public virtual City ParseCity(string value, City defaultValue)
		{
			var parsed = this.TryParseCity(value);
			return parsed == null ? defaultValue : parsed.Value;
		}

		public virtual CityState? TryParseCityState(string value)
		{
			return CityState.Create(value);
		}

		public virtual CityState ParseCityState(string value, CityState defaultValue)
		{
			var parsed = this.TryParseCityState(value);
			return parsed == null ? defaultValue : parsed.Value;
		}

		public virtual Count<T>? TryParseCount<T>(string value)
			where T : UnitType
		{
			return Count<T>.Create(value);
		}

		public virtual Count<T> ParseCount<T>(string value, Count<T> defaultValue)
			where T : UnitType
		{
			var parsed = this.TryParseCount<T>(value);
			return parsed == null ? defaultValue : parsed.Value;
		}

		public virtual Country? TryParseCountry(string value)
		{
			return Country.Create(value);
		}

		public virtual Country ParseCountry(string value, Country defaultValue)
		{
			var parsed = this.TryParseCountry(value);
			return parsed == null ? defaultValue : parsed.Value;
		}

		public virtual CountryCodeIso2? TryParseCountryCodeIso2(string value)
		{
			return CountryCodeIso2.Create(value);
		}

		public virtual CountryCodeIso2 ParseCountryCodeIso2(string value, CountryCodeIso2 defaultValue)
		{
			var parsed = this.TryParseCountryCodeIso2(value);
			return parsed == null ? defaultValue : parsed.Value;
		}

		public virtual CountryCodeIso3? TryParseCountryCodeIso3(string value)
		{
			return CountryCodeIso3.Create(value);
		}

		public virtual CountryCodeIso3 ParseCountryCodeIso3(string value, CountryCodeIso3 defaultValue)
		{
			var parsed = this.TryParseCountryCodeIso3(value);
			return parsed == null ? defaultValue : parsed.Value;
		}

		public virtual CountString? TryParseCountString(string value)
		{
			return CountString.Create(value);
		}

		public virtual CountString ParseCountString(string value, CountString defaultValue)
		{
			var parsed = this.TryParseCountString(value);
			return parsed == null ? defaultValue : parsed.Value;
		}

		public virtual DescriptionField? TryParseDescriptionField(string value)
		{
			return DescriptionField.Create(value);
		}

		public virtual DescriptionField ParseDescriptionField(string value, DescriptionField defaultValue)
		{
			var parsed = this.TryParseDescriptionField(value);
			return parsed == null ? defaultValue : parsed.Value;
		}

		public virtual DependentAges? TryParseDependentAges(string value)
		{
			return DependentAges.Create(value);
		}

		public virtual DependentAges ParseDependentAges(string value, DependentAges defaultValue)
		{
			var parsed = this.TryParseDependentAges(value);
			return parsed == null ? defaultValue : parsed.Value;
		}

		public virtual EmailAddress? TryParseEmailAddress(string value)
		{
			return EmailAddress.Create(value);
		}

		public virtual EmailAddress ParseEmailAddress(string value, EmailAddress defaultValue)
		{
			var parsed = this.TryParseEmailAddress(value);
			return parsed == null ? defaultValue : parsed.Value;
		}

		public virtual EntityName? TryParseEntityName(string value)
		{
			return EntityName.Create(value);
		}

		public virtual EntityName ParseEntityName(string value, EntityName defaultValue)
		{
			var parsed = this.TryParseEntityName(value);
			return parsed == null ? defaultValue : parsed.Value;
		}

		public virtual FhaCode? TryParseFhaCode(string value)
		{
			return FhaCode.Create(value);
		}

		public virtual FhaCode ParseFhaCode(string value, FhaCode defaultValue)
		{
			var parsed = this.TryParseFhaCode(value);
			return parsed == null ? defaultValue : parsed.Value;
		}

		public virtual FicoCreditScore? TryParseFicoCreditScore(string value)
		{
			return FicoCreditScore.Create(value);
		}

		public virtual FicoCreditScore ParseFicoCreditScore(string value, FicoCreditScore defaultValue)
		{
			var parsed = this.TryParseFicoCreditScore(value);
			return parsed == null ? defaultValue : parsed.Value;
		}

		public virtual FormattedString? TryParseFormattedString(string value)
		{
			return FormattedString.Create(value);
		}

		public virtual FormattedString ParseFormattedString(string value, FormattedString defaultValue)
		{
			var parsed = this.TryParseFormattedString(value);
			return parsed == null ? defaultValue : parsed.Value;
		}

		public virtual Instant? TryParseInstant(string value)
		{
			return Instant.Create(value);
		}

		public virtual Instant ParseInstant(string value, Instant defaultValue)
		{
			var parsed = this.TryParseInstant(value);
			return parsed == null ? defaultValue : parsed.Value;
		}

		public virtual IRSIdentifier? TryParseIRSIdentifier(string value)
		{
			return IRSIdentifier.Create(value);
		}

		public virtual IRSIdentifier ParseIRSIdentifier(string value, IRSIdentifier defaultValue)
		{
			var parsed = this.TryParseIRSIdentifier(value);
			return parsed == null ? defaultValue : parsed.Value;
		}

		public virtual LiabilityRemainingMonths? TryParseLiabilityRemainingMonths(string value)
		{
			return LiabilityRemainingMonths.Create(value);
		}

		public virtual LiabilityRemainingMonths ParseLiabilityRemainingMonths(string value, LiabilityRemainingMonths defaultValue)
		{
			var parsed = this.TryParseLiabilityRemainingMonths(value);
			return parsed == null ? defaultValue : parsed.Value;
		}

		public virtual LongDescriptionField? TryParseLongDescriptionField(string value)
		{
			return LongDescriptionField.Create(value);
		}

		public virtual LongDescriptionField ParseLongDescriptionField(string value, LongDescriptionField defaultValue)
		{
			var parsed = this.TryParseLongDescriptionField(value);
			return parsed == null ? defaultValue : parsed.Value;
		}

		public virtual LqbMismoIdentifier? TryParseLqbMismoIdentifier(string value)
		{
			return LqbMismoIdentifier.Create(value);
		}

		public virtual LqbMismoIdentifier ParseLqbMismoIdentifier(string value, LqbMismoIdentifier defaultValue)
		{
			var parsed = this.TryParseLqbMismoIdentifier(value);
			return parsed == null ? defaultValue : parsed.Value;
		}

		public virtual MilitaryServiceNumber? TryParseMilitaryServiceNumber(string value)
		{
			return MilitaryServiceNumber.Create(value);
		}

		public virtual MilitaryServiceNumber ParseMilitaryServiceNumber(string value, MilitaryServiceNumber defaultValue)
		{
			var parsed = this.TryParseMilitaryServiceNumber(value);
			return parsed == null ? defaultValue : parsed.Value;
		}

		public virtual Money? TryParseMoney(string value)
		{
			return Money.Create(value);
		}

		public virtual Money ParseMoney(string value, Money defaultValue)
		{
			var parsed = this.TryParseMoney(value);
			return parsed == null ? defaultValue : parsed.Value;
		}

		public virtual Month? TryParseMonth(string value)
		{
			return Month.Create(value);
		}

		public virtual Month ParseMonth(string value, Month defaultValue)
		{
			var parsed = this.TryParseMonth(value);
			return parsed == null ? defaultValue : parsed.Value;
		}

		public virtual Name? TryParseName(string value)
		{
			return Name.Create(value);
		}

		public virtual Name ParseName(string value, Name defaultValue)
		{
			var parsed = this.TryParseName(value);
			return parsed == null ? defaultValue : parsed.Value;
		}

		public virtual NameSuffix? TryParseNameSuffix(string value)
		{
			return NameSuffix.Create(value);
		}

		public virtual NameSuffix ParseNameSuffix(string value, NameSuffix defaultValue)
		{
			var parsed = this.TryParseNameSuffix(value);
			return parsed == null ? defaultValue : parsed.Value;
		}

		public virtual OrderRank? TryParseOrderRank(string value)
		{
			return OrderRank.Create(value);
		}

		public virtual OrderRank ParseOrderRank(string value, OrderRank defaultValue)
		{
			var parsed = this.TryParseOrderRank(value);
			return parsed == null ? defaultValue : parsed.Value;
		}

		public virtual Percentage? TryParsePercentage(string value)
		{
			return Percentage.Create(value);
		}

		public virtual Percentage ParsePercentage(string value, Percentage defaultValue)
		{
			var parsed = this.TryParsePercentage(value);
			return parsed == null ? defaultValue : parsed.Value;
		}

		public virtual Percentile? TryParsePercentile(string value)
		{
			return Percentile.Create(value);
		}

		public virtual Percentile ParsePercentile(string value, Percentile defaultValue)
		{
			var parsed = this.TryParsePercentile(value);
			return parsed == null ? defaultValue : parsed.Value;
		}

		public virtual PersonName? TryParsePersonName(string value)
		{
			return PersonName.Create(value);
		}

		public virtual PersonName ParsePersonName(string value, PersonName defaultValue)
		{
			var parsed = this.TryParsePersonName(value);
			return parsed == null ? defaultValue : parsed.Value;
		}

		public virtual PhoneNumber? TryParsePhoneNumber(string value)
		{
			return PhoneNumber.Create(value);
		}

		public virtual PhoneNumber ParsePhoneNumber(string value, PhoneNumber defaultValue)
		{
			var parsed = this.TryParsePhoneNumber(value);
			return parsed == null ? defaultValue : parsed.Value;
		}

		public virtual PostalCode? TryParsePostalCode(string value)
		{
			return PostalCode.Create(value);
		}

		public virtual PostalCode ParsePostalCode(string value, PostalCode defaultValue)
		{
			var parsed = this.TryParsePostalCode(value);
			return parsed == null ? defaultValue : parsed.Value;
		}

		public virtual Quantity<T>? TryParseQuantity<T>(string value)
			where T : UnitType
		{
			return Quantity<T>.Create(value);
		}

		public virtual Quantity<T> ParseQuantity<T>(string value, Quantity<T> defaultValue)
			where T : UnitType
		{
			var parsed = this.TryParseQuantity<T>(value);
			return parsed == null ? defaultValue : parsed.Value;
		}

		public virtual SocialSecurityNumber? TryParseSocialSecurityNumber(string value)
		{
			return SocialSecurityNumber.Create(value);
		}

		public virtual SocialSecurityNumber ParseSocialSecurityNumber(string value, SocialSecurityNumber defaultValue)
		{
			var parsed = this.TryParseSocialSecurityNumber(value);
			return parsed == null ? defaultValue : parsed.Value;
		}

		public virtual StreetAddress? TryParseStreetAddress(string value)
		{
			return StreetAddress.Create(value);
		}

		public virtual StreetAddress ParseStreetAddress(string value, StreetAddress defaultValue)
		{
			var parsed = this.TryParseStreetAddress(value);
			return parsed == null ? defaultValue : parsed.Value;
		}

		public virtual StreetName? TryParseStreetName(string value)
		{
			return StreetName.Create(value);
		}

		public virtual StreetName ParseStreetName(string value, StreetName defaultValue)
		{
			var parsed = this.TryParseStreetName(value);
			return parsed == null ? defaultValue : parsed.Value;
		}

		public virtual StreetSuffix? TryParseStreetSuffix(string value)
		{
			return StreetSuffix.Create(value);
		}

		public virtual StreetSuffix ParseStreetSuffix(string value, StreetSuffix defaultValue)
		{
			var parsed = this.TryParseStreetSuffix(value);
			return parsed == null ? defaultValue : parsed.Value;
		}

		public virtual TaxFormNumber? TryParseTaxFormNumber(string value)
		{
			return TaxFormNumber.Create(value);
		}

		public virtual TaxFormNumber ParseTaxFormNumber(string value, TaxFormNumber defaultValue)
		{
			var parsed = this.TryParseTaxFormNumber(value);
			return parsed == null ? defaultValue : parsed.Value;
		}

		public virtual ThirdPartyIdentifier? TryParseThirdPartyIdentifier(string value)
		{
			return ThirdPartyIdentifier.Create(value);
		}

		public virtual ThirdPartyIdentifier ParseThirdPartyIdentifier(string value, ThirdPartyIdentifier defaultValue)
		{
			var parsed = this.TryParseThirdPartyIdentifier(value);
			return parsed == null ? defaultValue : parsed.Value;
		}

		public virtual UnitedStatesPostalState? TryParseUnitedStatesPostalState(string value)
		{
			return UnitedStatesPostalState.Create(value);
		}

		public virtual UnitedStatesPostalState ParseUnitedStatesPostalState(string value, UnitedStatesPostalState defaultValue)
		{
			var parsed = this.TryParseUnitedStatesPostalState(value);
			return parsed == null ? defaultValue : parsed.Value;
		}

		public virtual UnitedStatesState? TryParseUnitedStatesState(string value)
		{
			return UnitedStatesState.Create(value);
		}

		public virtual UnitedStatesState ParseUnitedStatesState(string value, UnitedStatesState defaultValue)
		{
			var parsed = this.TryParseUnitedStatesState(value);
			return parsed == null ? defaultValue : parsed.Value;
		}

		public virtual UnzonedDate? TryParseUnzonedDate(string value)
		{
			return UnzonedDate.Create(value);
		}

		public virtual UnzonedDate ParseUnzonedDate(string value, UnzonedDate defaultValue)
		{
			var parsed = this.TryParseUnzonedDate(value);
			return parsed == null ? defaultValue : parsed.Value;
		}

		public virtual UspsAddressNumber? TryParseUspsAddressNumber(string value)
		{
			return UspsAddressNumber.Create(value);
		}

		public virtual UspsAddressNumber ParseUspsAddressNumber(string value, UspsAddressNumber defaultValue)
		{
			var parsed = this.TryParseUspsAddressNumber(value);
			return parsed == null ? defaultValue : parsed.Value;
		}

		public virtual UspsDirectional? TryParseUspsDirectional(string value)
		{
			return UspsDirectional.Create(value);
		}

		public virtual UspsDirectional ParseUspsDirectional(string value, UspsDirectional defaultValue)
		{
			var parsed = this.TryParseUspsDirectional(value);
			return parsed == null ? defaultValue : parsed.Value;
		}

		public virtual VaDateOfLoan? TryParseVaDateOfLoan(string value)
		{
			return VaDateOfLoan.Create(value);
		}

		public virtual VaDateOfLoan ParseVaDateOfLoan(string value, VaDateOfLoan defaultValue)
		{
			var parsed = this.TryParseVaDateOfLoan(value);
			return parsed == null ? defaultValue : parsed.Value;
		}

		public virtual VaEntitlementCode? TryParseVaEntitlementCode(string value)
		{
			return VaEntitlementCode.Create(value);
		}

		public virtual VaEntitlementCode ParseVaEntitlementCode(string value, VaEntitlementCode defaultValue)
		{
			var parsed = this.TryParseVaEntitlementCode(value);
			return parsed == null ? defaultValue : parsed.Value;
		}

		public virtual VaLoanNumber? TryParseVaLoanNumber(string value)
		{
			return VaLoanNumber.Create(value);
		}

		public virtual VaLoanNumber ParseVaLoanNumber(string value, VaLoanNumber defaultValue)
		{
			var parsed = this.TryParseVaLoanNumber(value);
			return parsed == null ? defaultValue : parsed.Value;
		}

		public virtual Year? TryParseYear(string value)
		{
			return Year.Create(value);
		}

		public virtual Year ParseYear(string value, Year defaultValue)
		{
			var parsed = this.TryParseYear(value);
			return parsed == null ? defaultValue : parsed.Value;
		}

		public virtual YearAsString? TryParseYearAsString(string value)
		{
			return YearAsString.Create(value);
		}

		public virtual YearAsString ParseYearAsString(string value, YearAsString defaultValue)
		{
			var parsed = this.TryParseYearAsString(value);
			return parsed == null ? defaultValue : parsed.Value;
		}

		public virtual Zipcode? TryParseZipcode(string value)
		{
			return Zipcode.Create(value);
		}

		public virtual Zipcode ParseZipcode(string value, Zipcode defaultValue)
		{
			var parsed = this.TryParseZipcode(value);
			return parsed == null ? defaultValue : parsed.Value;
		}

	}
}
