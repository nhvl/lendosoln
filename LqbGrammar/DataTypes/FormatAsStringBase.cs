﻿namespace LqbGrammar.DataTypes
{
    using System;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Manual methods for the FormatAsStringBase class.
    /// </summary>
    public partial class FormatAsStringBase
    {
        /// <summary>
        /// Format a boolean into a string.
        /// </summary>
        /// <param name="value">The boolean to be formatted.</param>
        /// <returns>A string representation of the boolean.</returns>
        public virtual string Format(bool? value)
        {
            return this.Format(value, string.Empty);
        }

        /// <summary>
        /// Format a boolean into a string.
        /// </summary>
        /// <param name="value">The boolean to be formatted.</param>
        /// <param name="defaultValue">The default value to use when the input is null.</param>
        /// <returns>A string representation of the boolean.</returns>
        public virtual string Format(bool? value, string defaultValue)
        {
            return value == null ? defaultValue : value.Value.ToString();
        }

        /// <summary>
        /// Format a DataObjectIdentifier into a string.
        /// </summary>
        /// <typeparam name="TValueKind">The <see cref="DataObjectKind"/> of the value being identified.</typeparam>
        /// <typeparam name="TIdValue">The type of the identifier used to refer to the value.</typeparam>
        /// <param name="value">The DataObjectIdentifier to be formatted.</param>
        /// <returns>A string representation of the DataObjectIdentifer.</returns>
        public virtual string Format<TValueKind, TIdValue>(DataObjectIdentifier<TValueKind, TIdValue> value)
            where TValueKind : DataObjectKind
            where TIdValue : struct, IEquatable<TIdValue>
        {
            return this.Format((DataObjectIdentifier<TValueKind, TIdValue>?)value, string.Empty);
        }

        /// <summary>
        /// Format a DataObjectIdentifier into a string.
        /// </summary>
        /// <typeparam name="TValueKind">The <see cref="DataObjectKind"/> of the value being identified.</typeparam>
        /// <typeparam name="TIdValue">The type of the identifier used to refer to the value.</typeparam>
        /// <param name="value">The DataObjectIdentifier to be formatted.</param>
        /// <returns>A string representation of the DataObjectIdentifer.</returns>
        public virtual string Format<TValueKind, TIdValue>(DataObjectIdentifier<TValueKind, TIdValue>? value)
            where TValueKind : DataObjectKind
            where TIdValue : struct, IEquatable<TIdValue>
        {
            return this.Format(value, string.Empty);
        }

        /// <summary>
        /// Format a DataObjectIdentifier into a string.
        /// </summary>
        /// <typeparam name="TValueKind">The <see cref="DataObjectKind"/> of the value being identified.</typeparam>
        /// <typeparam name="TIdValue">The type of the identifier used to refer to the value.</typeparam>
        /// <param name="value">The DataObjectIdentifier to be formatted.</param>
        /// <param name="defaultValue">The default value to use when the input is null.</param>
        /// <returns>A string representation of the DataObjectIdentifer.</returns>
        public virtual string Format<TValueKind, TIdValue>(DataObjectIdentifier<TValueKind, TIdValue>? value, string defaultValue)
            where TValueKind : DataObjectKind
            where TIdValue : struct, IEquatable<TIdValue>
        {
            return value == null ? defaultValue : value.Value.ToString();
        }

        /// <summary>
        /// Format an enumeration value into a string.
        /// </summary>
        /// <typeparam name="E">The enumeration type.</typeparam>
        /// <param name="value">The enumeration value.</param>
        /// <returns>A string represenation of the enumeration value.</returns>
        public virtual string FormatEnum<E>(E? value)
            where E : struct
        {
            return this.FormatEnum<E>(value, string.Empty);
        }

        /// <summary>
        /// Format an enumeration value into a string.
        /// </summary>
        /// <typeparam name="E">The enumeration type.</typeparam>
        /// <param name="value">The enumeration value.</param>
        /// <param name="defaultValue">The default value to use when the input is null.</param>
        /// <returns>A string represenation of the enumeration value.</returns>
        public virtual string FormatEnum<E>(E? value, string defaultValue)
            where E : struct
        {
            if (!typeof(E).IsEnum)
            {
                throw new DeveloperException(ErrorMessage.SystemError);
            }

            if (value == null)
            {
                return defaultValue;
            }
            else
            {
                return Enum.GetName(typeof(E), value.Value);
            }
        }
    }
}
