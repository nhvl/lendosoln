﻿namespace LqbGrammar.DataTypes
{
    /// <summary>
    /// Base class for types of data objects, which have identifiers that are
    /// usually primary keys in the database.
    /// </summary>
    public abstract class DataObjectKind
    {
        /// <summary>
        /// The data object kind for an asset.
        /// </summary>
        public sealed class Asset : DataObjectKind
        {
        }

        /// <summary>
        /// The data object kind for a BrokerDB.
        /// </summary>
        public sealed class ClientCompany : DataObjectKind
        {
        }

        /// <summary>
        /// The data object kind for a borrower in the ULAD data layer.
        /// </summary>
        public sealed class Consumer : DataObjectKind
        {
        }

        /// <summary>
        /// The data object kind for an association between a consumer and an asset.
        /// </summary>
        public sealed class ConsumerAssetAssociation : DataObjectKind
        {
        }

        /// <summary>
        /// The data object kind for an association between a consumer and an income source record.
        /// </summary>
        public sealed class ConsumerIncomeSourceAssociation : DataObjectKind
        {
        }

        /// <summary>
        /// The data object kind for an association between a consumer and a liability.
        /// </summary>
        public sealed class ConsumerLiabilityAssociation : DataObjectKind
        {
        }

        /// <summary>
        /// The data object kind for an association between a consumer and a public record.
        /// </summary>
        public sealed class ConsumerPublicRecordAssociation : DataObjectKind
        {
        }

        /// <summary>
        /// The data object kind for an association between a consumer and a real property record that signifies ownership.
        /// </summary>
        public sealed class ConsumerRealPropertyAssociation : DataObjectKind
        {
        }

        /// <summary>
        /// The data object kind for an association between a consumer and a VA previous loan.
        /// </summary>
        public sealed class ConsumerVaPreviousLoanAssociation : DataObjectKind
        {
        }

        /// <summary>
        /// The data object kind for an association between a consumer and a VOR record.
        /// </summary>
        public sealed class ConsumerVorRecordAssociation : DataObjectKind
        {
        }

        /// <summary>
        /// The data object kind for a counseling event.
        /// </summary>
        public sealed class CounselingEvent : DataObjectKind
        {
        }

        /// <summary>
        /// The data object kind for a counseling event attendance association.
        /// </summary>
        public sealed class CounselingEventAttendance : DataObjectKind
        {
        }

        /// <summary>
        /// The data object kind for an employee.
        /// </summary>
        public sealed class Employee : DataObjectKind
        {
        }

        /// <summary>
        /// The data object kind for an employment record.
        /// </summary>
        public sealed class EmploymentRecord : DataObjectKind
        {
        }

        /// <summary>
        /// The data object kind for a housing history entry.
        /// </summary>
        public sealed class HousingHistoryEntry : DataObjectKind
        {
        }

        /// <summary>
        /// The data object kind for an income record.
        /// </summary>
        public sealed class IncomeRecord : DataObjectKind
        {
        }

        /// <summary>
        /// The data object kind for an income source.
        /// </summary>
        public sealed class IncomeSource : DataObjectKind
        {
        }

        /// <summary>
        /// The data object kind for an association between an income source and an employment record.
        /// </summary>
        public sealed class IncomeSourceEmploymentRecordAssociation : DataObjectKind
        {
        }

        /// <summary>
        /// The data object kind for a loan application.
        /// </summary>
        public sealed class LegacyApplication : DataObjectKind
        {
        }

        /// <summary>
        /// The data object kind for an association between a legacy application and a consumer.
        /// </summary>
        public sealed class LegacyApplicationConsumerAssociation : DataObjectKind
        {
        }

        /// <summary>
        /// The data object kind for a liability.
        /// </summary>
        public sealed class Liability : DataObjectKind
        {
        }

        /// <summary>
        /// The data object kind for a loan file.
        /// </summary>
        public sealed class Loan : DataObjectKind
        {
        }

        /// <summary>
        /// The data object kind for a public record.
        /// </summary>
        public sealed class PublicRecord : DataObjectKind
        {
        }

        /// <summary>
        /// The data object kind for a real property record.
        /// </summary>
        public sealed class RealProperty : DataObjectKind
        {
        }

        /// <summary>
        /// The data object kind for an association between a real property record and a liability.
        /// </summary>
        public sealed class RealPropertyLiabilityAssociation : DataObjectKind
        {
        }

        /// <summary>
        /// The data object kind for a signature file.
        /// </summary>
        public sealed class Signature : DataObjectKind
        {
        }

        /// <summary>
        /// The data object kind for a ULAD application.
        /// </summary>
        public sealed class UladApplication : DataObjectKind
        {
        }

        /// <summary>
        /// The data object kind for an association between a ULAD application and a consumer.
        /// </summary>
        public sealed class UladApplicationConsumerAssociation : DataObjectKind
        {
        }

        /// <summary>
        /// The data object kind for a VA previous loan record.
        /// </summary>
        public sealed class VaPreviousLoan : DataObjectKind
        {
        }

        /// <summary>
        /// The data object kind for a verification of rent record.
        /// </summary>
        public sealed class VorRecord : DataObjectKind
        {
        }
    }
}
