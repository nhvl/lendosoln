﻿namespace LqbGrammar.DataTypes
{
    using LqbGrammar.Validation;

    /// <summary>
    /// Encapsulate validation logic for a comment added to the comment log.
    /// </summary>
    public struct CommentLogEntry : System.IEquatable<CommentLogEntry>
    {
        /// <summary>
        /// An instance of the class that fails validation, can be used for initialization when necessary.
        /// </summary>
        public static readonly CommentLogEntry BadComment = new CommentLogEntry(null); // string.Empty won't work because empty comments are allowed\

        /// <summary>
        /// An instance of the class that is empty, for stripped comments.
        /// </summary>
        public static readonly CommentLogEntry EmptyComment = new CommentLogEntry(string.Empty);

        /// <summary>
        /// Encapsulated value.
        /// </summary>
        private string comment;

        /// <summary>
        /// Initializes a new instance of the <see cref="CommentLogEntry"/> struct.
        /// </summary>
        /// <param name="comment">The comment text.</param>
        private CommentLogEntry(string comment)
        {
            this.comment = comment;
        }

        /// <summary>
        /// Attempt to parse the input text into a validated comment.
        /// </summary>
        /// <param name="comment">Input text that will be parsed.</param>
        /// <returns>Valid comment, or null.</returns>
        public static CommentLogEntry? Create(string comment)
        {
            string clean = ReplaceProblematicWordChars(comment);
            if (IsValidComment(clean))
            {
                return new CommentLogEntry(clean);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Static method that checks for equality between two instances.
        /// </summary>
        /// <param name="lhs">Instance on the left side of the == operator.</param>
        /// <param name="rhs">Instance on the right side of the == operator.</param>
        /// <returns>True if the two instances are equal, false otherwise.</returns>
        public static new bool Equals(object lhs, object rhs)
        {
            if (!(lhs is CommentLogEntry))
            {
                return false;
            }

            if (!(rhs is CommentLogEntry))
            {
                return false;
            }

            return ((CommentLogEntry)lhs).Equals((CommentLogEntry)rhs);
        }

        /// <summary>
        /// The == operator is implemented here.
        /// </summary>
        /// <param name="lhs">Instance on the left of the == operator.</param>
        /// <param name="rhs">Instance on the right of the == operator.</param>
        /// <returns>True if the two instances are equal, false otherwise.</returns>
        public static bool operator ==(CommentLogEntry lhs, CommentLogEntry rhs)
        {
            return lhs.Equals(rhs);
        }

        /// <summary>
        /// The != operator is implemented here.
        /// </summary>
        /// <param name="lhs">Instance on the left of the != operator.</param>
        /// <param name="rhs">Instance on the right of the != operator.</param>
        /// <returns>True if the two instances are not equal, false otherwise.</returns>
        public static bool operator !=(CommentLogEntry lhs, CommentLogEntry rhs)
        {
            return !lhs.Equals(rhs);
        }

        /// <summary>
        /// Return the encapsulated value.
        /// </summary>
        /// <returns>Encapsulated value.</returns>
        public override string ToString()
        {
            return this.comment;
        }

        /// <summary>
        /// Implement IEquatable.
        /// </summary>
        /// <param name="other">Instance that is checked for equality with this instance.</param>
        /// <returns>True if other is equal to this instance, false otherwise.</returns>
        public bool Equals(CommentLogEntry other)
        {
            return this.comment == other.comment;
        }

        /// <summary>
        /// Override the Equals method inherited from the Object class.
        /// </summary>
        /// <param name="obj">Instance that is checked for equality with this instance.</param>
        /// <returns>True if obj is equal to this instance, false otherwise.</returns>
        public override bool Equals(object obj)
        {
            if (!(obj is CommentLogEntry))
            {
                return false;
            }

            return Equals((CommentLogEntry)obj);
        }

        /// <summary>
        /// Override of the method inherited from the Object class.
        /// </summary>
        /// <returns>An integer that is the hash code for this instance.</returns>
        public override int GetHashCode()
        {
            return this.comment.GetHashCode();
        }

        /// <summary>
        /// When people copy/paste text from a Word document, all sorts of problems
        /// ensue.  For example, Word uses idiosycratic encodings for common ASCII
        /// characters (the apostrophe for instance).  We don't wish to return an 
        /// error to the user as the workaround for them would be painful.  We can 
        /// just substitute better values for known bad characters and the user 
        /// can be happily oblivious.
        /// </summary>
        /// <param name="comment">The text that is passed into the LQB system by a user.</param>
        /// <returns>A clean version of the text with appropriate substitutions if/when necessary.</returns>
        private static string ReplaceProblematicWordChars(string comment)
        {
            // We will need to do some research and identify which Word characters
            // use goofy encodings and convert those to reasonable Unicode equivalents.
            return comment;
        }

        /// <summary>
        /// Comments are an opportunity for hackers.  We need to establish some 
        /// security restrictions on the acceptable content, while at the same 
        /// time not being so restrictive that our users get angry.  The dividing 
        /// line must be based on the typical usage scenario, which is a decision 
        /// for the design team.  As yet, they haven't considered this issue so 
        /// this method will be a pass-through until the decision has been made.
        /// </summary>
        /// <param name="comment">The comment that is checked.</param>
        /// <returns>True if the comment text conforms to our security policy, false otherwise.</returns>
        private static bool IsValidComment(string comment)
        {
            // We will need to establish a policy on what content is
            // considered valid and what is considered invalid.
            // For example, one approach could be to only permit
            // text that returns unaltered when passed through
            // the AntiXSS SafeHTML method.
            return true;
        }
    }
}
