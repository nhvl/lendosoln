﻿namespace LqbGrammar.DataTypes
{
    using System;
    using LqbGrammar.Exceptions;
    using LqbGrammar.Validation;

    /// <summary>
    /// Represents an error message for the user about invalid user input.
    /// </summary>
    public partial struct ValidationErrorMessage
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ValidationErrorMessage"/> struct.
        /// </summary>
        /// <param name="message">The error message.</param>
        private ValidationErrorMessage(string message)
        {
            this.Message = message;
        }

        /// <summary>
        /// Gets the contained message.
        /// </summary>
        /// <value>The contained message.</value>
        public string Message { get; }

        /// <summary>
        /// Validate and encapsulate an error message.
        /// </summary>
        /// <param name="message">The error message.</param>
        /// <returns>The encapsulated error message if it is valid, or null.</returns>
        public static ValidationErrorMessage? Create(string message)
        {
            // validation error messages will commonly report back portions of user input, so
            // the only valid regex is .+, which is weaker than IsNullOrWhiteSpace.
            if (!string.IsNullOrWhiteSpace(message))
            {
                return new ValidationErrorMessage(message);
            }

            return null;
        }

        /// <summary>
        /// Overrides the method inherited from the Object class.
        /// </summary>
        /// <returns>The error message.</returns>
        public override string ToString()
        {
            return this.Message;
        }
    }

    /// <summary>
    /// Contains the implementation of <see cref="IEquatable{ValidationErrorMessage}"/> using the <see cref="Message"/> property.
    /// </summary>
    public partial struct ValidationErrorMessage : IEquatable<ValidationErrorMessage>
    {
        /// <summary>
        /// Implements the == operator.
        /// </summary>
        /// <param name="lhs">Instance on the left side of the == operator.</param>
        /// <param name="rhs">Instance on the right side of the == operator.</param>
        /// <returns>True if the instances are equal, false otherwise.</returns>
        public static bool operator ==(ValidationErrorMessage lhs, ValidationErrorMessage rhs)
        {
            return lhs.Equals(rhs);
        }

        /// <summary>
        /// Implements the != operator.
        /// </summary>
        /// <param name="lhs">Instance on the left side of the != operator.</param>
        /// <param name="rhs">Instance on the right side of the != operator.</param>
        /// <returns>True if the instances are not equal, false otherwise.</returns>
        public static bool operator !=(ValidationErrorMessage lhs, ValidationErrorMessage rhs)
        {
            return !lhs.Equals(rhs);
        }

        /// <summary>
        /// Determines if two instances are equal.
        /// </summary>
        /// <param name="other">Instance that is compared with this instance for equality.</param>
        /// <returns>True if the instances are equal, false otherwise.</returns>
        public bool Equals(ValidationErrorMessage other)
        {
            return StringComparer.OrdinalIgnoreCase.Equals(this.Message, other.Message);
        }

        /// <summary>
        /// Override of the method inherited from the Object class.
        /// </summary>
        /// <param name="obj">Instance that is compared with this instance for equality.</param>
        /// <returns>True if the instances are equal, false otherwise.</returns>
        public override bool Equals(object obj)
        {
            return obj is ValidationErrorMessage && this.Equals((ValidationErrorMessage)obj);
        }

        /// <summary>
        /// Override the method inherited from the Object class.
        /// </summary>
        /// <returns>An integer that is the hash of this instance.</returns>
        public override int GetHashCode()
        {
            return StringComparer.OrdinalIgnoreCase.GetHashCode(this.Message);
        }
    }
}
