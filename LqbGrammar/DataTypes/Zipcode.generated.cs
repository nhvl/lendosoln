﻿// <auto-generated />
namespace LqbGrammar.DataTypes
{
    using System;
    using LqbGrammar.Validation;

    /// <summary>
    /// Encapsulate a zipcode.
    /// </summary>
    public partial struct Zipcode : ISemanticType, IEquatable<Zipcode>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Zipcode"/> struct.
        /// </summary>
        /// <param name="value">The value.</param>
        private Zipcode(string value)
        {
            this.Value = value;
        }

        /// <summary>
        /// Gets or sets the encapsulated value.
        /// </summary>
        /// <value>The encapsulated Value.</value>
        private string Value { get; set; }

        /// <summary>
        /// Gets the encapsulated value as an object.
        /// </summary>
        object ISemanticType.Value => this.Value;

        /// <summary>
        /// Attempt to parse the input string into a <see cref="Zipcode"/>.
        /// </summary>
        /// <param name="value">The string to parse.</param>
        /// <returns>A <see cref="Zipcode"/> of the specified value, or null.</returns>
        public static Zipcode? Create(string value)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                return null;
            }

            return new Zipcode(value.Trim());
        }

        /// <summary>
        /// Attempt to parse and validate the input string into a <see cref="Zipcode"/>.
        /// </summary>
        /// <param name="value">The string to parse and validate.</param>
        /// <returns>A <see cref="Zipcode"/> of the specified value, or null.</returns>
        public static Zipcode? CreateWithValidation(string value)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                return null;
            }

            string trimmedValue = value.Trim();
            if (!RegularExpressionEngine.Execute(trimmedValue, RegularExpressionString.Zipcode).IsMatch())
            {
                return null;
            }

            return new Zipcode(trimmedValue);
        }

        /// <summary>
        /// The static equality method.
        /// </summary>
        /// <param name="lhs">The object on the left side of the operator.</param>
        /// <param name="rhs">The object on the right side of the operator.</param>
        /// <returns>True if both objects are instances of <see cref="Zipcode"/> and are equal, false otherwise.</returns>
        public static new bool Equals(object lhs, object rhs)
        {
            if (!(lhs is Zipcode))
            {
                return false;
            }

            if (!(rhs is Zipcode))
            {
                return false;
            }

            return ((Zipcode)lhs).Equals((Zipcode)rhs);
        }

        /// <summary>
        /// The == operator is defined here.
        /// </summary>
        /// <param name="lhs"><see cref="Zipcode"/> on the left side of the operator.</param>
        /// <param name="rhs"><see cref="Zipcode"/> on the right side of the operator.</param>
        /// <returns>True if the two values are equal, false otherwise.</returns>
        public static bool operator ==(Zipcode lhs, Zipcode rhs)
        {
            return lhs.Equals(rhs);
        }

        /// <summary>
        /// The != operator is defined here.
        /// </summary>
        /// <param name="lhs"><see cref="Zipcode"/> on the left side of the operator.</param>
        /// <param name="rhs"><see cref="Zipcode"/> on the right side of the operator.</param>
        /// <returns>True if the two values are not equal, false otherwise.</returns>
        public static bool operator !=(Zipcode lhs, Zipcode rhs)
        {
            return !lhs.Equals(rhs);
        }

        /// <summary>
        /// Implement IEquatable.
        /// </summary>
        /// <param name="other">Instance that is checked for equality with this instance.</param>
        /// <returns>True if other is equal to this instance, false otherwise.</returns>
        public bool Equals(Zipcode other)
        {
            return string.Compare(this.Value, other.Value, StringComparison.OrdinalIgnoreCase) == 0;
        }

        /// <summary>
        /// Override the equality method from System.Object.
        /// <summary>
        /// <param name="obj">The object being compared with this instance.</param>
        /// <returns>True if the object is an instance of <see cref="Zipcode"/> and is equal to this instance, false otherwise.</returns>
        public override bool Equals(object obj)
        {
            return (obj is Zipcode) && this.Equals((Zipcode)obj);
        }

        /// <summary>
        /// Override of the System.GetHashCode method.
        /// </summary>
        /// <returns>An integer that is the hash code for this instance.</returns>
        public override int GetHashCode()
        {
            return this.Value.ToLower().GetHashCode();
        }

        /// <summary>
        /// Returns a string representation of the <see cref="Zipcode"/>.
        /// </summary>
        /// <returns>A string representation of the <see cref="Zipcode"/>.</returns>
        public override string ToString()
        {
            return this.Value;
        }
    }
}