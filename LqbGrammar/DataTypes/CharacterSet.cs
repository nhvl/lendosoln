﻿namespace LqbGrammar.DataTypes
{
    /// <summary>
    /// Encapsulate the concept of character set, which is different from
    /// the concept of encoding.  For a discussion of the difference, see
    /// http://skeletonschema.info/Blog/XmlEncodings.php.
    /// </summary>
    public struct CharacterSet
    {
        /// <summary>
        /// The ASCII character set.
        /// </summary>
        public static readonly CharacterSet Ascii = new CharacterSet("us-ascii");

        /// <summary>
        /// The ISO-8850-1 character set, AKA Latin1.
        /// </summary>
        public static readonly CharacterSet ISOx8859x1 = new CharacterSet("iso-8859-1");

        /// <summary>
        /// The Latin1 character set.
        /// </summary>
        public static readonly CharacterSet Latin1 = ISOx8859x1;

        /// <summary>
        /// The character set used by older versions of the Windows OS.
        /// </summary>
        public static readonly CharacterSet Windows = new CharacterSet("windows-1252");

        /// <summary>
        /// The Unicode character set.
        /// </summary>
        public static readonly CharacterSet Unicode = new CharacterSet("unicode");

        /// <summary>
        /// The string that is used to characterize the character set.
        /// </summary>
        private string charset;

        /// <summary>
        /// Initializes a new instance of the <see cref="CharacterSet"/> struct.
        /// </summary>
        /// <param name="charset">The string that is used to characterize the character set.</param>
        private CharacterSet(string charset)
        {
            this.charset = charset;
        }

        /// <summary>
        /// Override the Object.ToString() method.
        /// </summary>
        /// <returns>The string that is used to characterize the character set.</returns>
        public override string ToString()
        {
            return this.charset;
        }
    }
}
