﻿namespace LqbGrammar.DataTypes
{
    using System;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// LQB stores a (time, timezone) in the database and this type encapsulates that use case.
    /// </summary>
    /// <remarks>
    /// There is no point in implementing IEquatible&lt;ZonedTime&gt; because these times don't have any
    /// meaning until they get saturated with a date.
    /// </remarks>
    public struct ZonedTime
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ZonedTime"/> struct.
        /// </summary>
        /// <param name="time">The time value.</param>
        /// <param name="timezone">The timezone value.</param>
        private ZonedTime(TimeSpan time, LqbTimeZone timezone)
        {
            this.Time = time;
            this.Zone = timezone;
        }

        /// <summary>
        /// Gets a DateTimeOffset that corresponds to the ZonedTime when saturated with today's date.
        /// </summary>
        /// <value>A DateTimeOffset that corresponds to the ZonedTime when saturated with today's date.</value>
        public DateTimeOffset TimeForToday
        {
            get
            {
                var today = DateTime.Today;
                var target = today + this.Time;
                if (target.Kind != DateTimeKind.Unspecified)
                {
                    target = new DateTime(target.Ticks, DateTimeKind.Unspecified);
                }

                var info = (TimeZoneInfo)this.Zone;
                var offset = info.GetUtcOffset(target);
                return new DateTimeOffset(target, offset);
            }
        }

        /// <summary>
        /// Gets the time value as a TimeSpan.
        /// </summary>
        /// <value>The time value as a TimeSpan.</value>
        public TimeSpan Time { get; private set; }

        /// <summary>
        /// Gets or sets the time zone.
        /// </summary>
        /// <value>The time zone.</value>
        private LqbTimeZone Zone { get; set; }

        /// <summary>
        /// Create a ZonedTime instance from the time and timezone.
        /// </summary>
        /// <param name="time">The time as a TimeSpan value.</param>
        /// <param name="timezone">The timezone.</param>
        /// <returns>A Zonedtime instance.</returns>
        public static ZonedTime? Create(TimeSpan time, LqbTimeZone timezone)
        {
            if (time.Ticks < 0)
            {
                return null;
            }
            else if (time.Days > 0)
            {
                return null;
            }
            else
            {
                return new ZonedTime(time, timezone);
            }
        }

        /// <summary>
        /// Create a ZonedTime instance from a string representation.
        /// </summary>
        /// <param name="value">The string representation.</param>
        /// <returns>A ZonedTime instance, or null.</returns>
        public static ZonedTime? Create(string value)
        {
            string working = value.Trim();
            int index = working.LastIndexOf(' ');
            if (index <= 0)
            {
                return null;
            }

            try
            {
                var timezone = LqbTimeZone.Create(working.Substring(index + 1));

                TimeSpan time;
                if (TimeSpan.TryParse(working.Substring(0, index).TrimEnd(), out time))
                {
                    return Create(time, timezone);
                }
                else
                {
                    return null;
                }
            }
            catch (LqbException)
            {
                return null;
            }
        }

        /// <summary>
        /// Override the implementation inherited from System.Object.
        /// </summary>
        /// <returns>A string representation of the ZonedTime.</returns>
        public override string ToString()
        {
            return string.Format("{0} {1}", this.Time.ToString(), this.Zone.GeneralAbbreviation);
        }
    }
}
