﻿namespace LqbGrammar.DataTypes
{
    using System.Collections.Generic;

    /// <summary>
    /// Encapsulate the concept of a directional indicator for a US address.
    /// </summary>
    public partial struct UspsDirectional
    {
        /// <summary>
        /// Defines the recognized U.S. directional indicators.
        /// </summary>
        private static readonly HashSet<string> RecognizedDirectionals = new HashSet<string>()
        {
            "N",
            "S",
            "E",
            "W",
            "NE",
            "NW",
            "SE",
            "SW",
            "NORTH",
            "SOUTH",
            "EAST",
            "WEST",
            "NORTH EAST",
            "NORTH WEST",
            "SOUTH EAST",
            "SOUTH WEST",
            "NORTHEAST",
            "NORTHWEST",
            "SOUTHEAST",
            "SOUTHWEST"
        };

        /// <summary>
        /// Gets a value indicating whether the specified value is a valid U.S. directional.
        /// </summary>
        /// <param name="value">The value to validate.</param>
        /// <returns>True if the value is a valid U.S. directional, false otherwise.</returns>
        private static bool PassesAdvancedValidation(string value)
        {
            return RecognizedDirectionals.Contains(value.ToUpper());
        }
    }
}
