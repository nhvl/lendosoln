﻿// <auto-generated />
namespace LqbGrammar.DataTypes
{
	using LqbGrammar.Utils.Country;

    public partial struct CountryCodeIso2
    {
        /// <summary>
        /// Determine whether the country code is valid within the ISO-3166-1 alpha-2 standard.
        /// </summary>
        /// <param name="value">The country code.</param>
        /// <returns>True if the country code is valid, false otherwise.</returns>
        private static bool PassesAdvancedValidation(string value)
        {
            var nation = NationRegistry.GetNationFromCountryCode_ISO_3166_2(value);
            return nation != null;
        }

		public static readonly CountryCodeIso2 Afghanistan = new CountryCodeIso2(NationRegistry.GetData(Nation.Afghanistan).Iso_3166_2);
		public static readonly CountryCodeIso2 AlandIslands = new CountryCodeIso2(NationRegistry.GetData(Nation.AlandIslands).Iso_3166_2);
		public static readonly CountryCodeIso2 Albania = new CountryCodeIso2(NationRegistry.GetData(Nation.Albania).Iso_3166_2);
		public static readonly CountryCodeIso2 Algeria = new CountryCodeIso2(NationRegistry.GetData(Nation.Algeria).Iso_3166_2);
		public static readonly CountryCodeIso2 AmericanSamoa = new CountryCodeIso2(NationRegistry.GetData(Nation.AmericanSamoa).Iso_3166_2);
		public static readonly CountryCodeIso2 Andorra = new CountryCodeIso2(NationRegistry.GetData(Nation.Andorra).Iso_3166_2);
		public static readonly CountryCodeIso2 Angola = new CountryCodeIso2(NationRegistry.GetData(Nation.Angola).Iso_3166_2);
		public static readonly CountryCodeIso2 Anguilla = new CountryCodeIso2(NationRegistry.GetData(Nation.Anguilla).Iso_3166_2);
		public static readonly CountryCodeIso2 Antarctica = new CountryCodeIso2(NationRegistry.GetData(Nation.Antarctica).Iso_3166_2);
		public static readonly CountryCodeIso2 AntiguaAndBarbuda = new CountryCodeIso2(NationRegistry.GetData(Nation.AntiguaAndBarbuda).Iso_3166_2);
		public static readonly CountryCodeIso2 Argentina = new CountryCodeIso2(NationRegistry.GetData(Nation.Argentina).Iso_3166_2);
		public static readonly CountryCodeIso2 Armenia = new CountryCodeIso2(NationRegistry.GetData(Nation.Armenia).Iso_3166_2);
		public static readonly CountryCodeIso2 Aruba = new CountryCodeIso2(NationRegistry.GetData(Nation.Aruba).Iso_3166_2);
		public static readonly CountryCodeIso2 Australia = new CountryCodeIso2(NationRegistry.GetData(Nation.Australia).Iso_3166_2);
		public static readonly CountryCodeIso2 Austria = new CountryCodeIso2(NationRegistry.GetData(Nation.Austria).Iso_3166_2);
		public static readonly CountryCodeIso2 Azerbaijan = new CountryCodeIso2(NationRegistry.GetData(Nation.Azerbaijan).Iso_3166_2);
		public static readonly CountryCodeIso2 Bahamas = new CountryCodeIso2(NationRegistry.GetData(Nation.Bahamas).Iso_3166_2);
		public static readonly CountryCodeIso2 Bahrain = new CountryCodeIso2(NationRegistry.GetData(Nation.Bahrain).Iso_3166_2);
		public static readonly CountryCodeIso2 Bangladesh = new CountryCodeIso2(NationRegistry.GetData(Nation.Bangladesh).Iso_3166_2);
		public static readonly CountryCodeIso2 Barbados = new CountryCodeIso2(NationRegistry.GetData(Nation.Barbados).Iso_3166_2);
		public static readonly CountryCodeIso2 Belarus = new CountryCodeIso2(NationRegistry.GetData(Nation.Belarus).Iso_3166_2);
		public static readonly CountryCodeIso2 Belgium = new CountryCodeIso2(NationRegistry.GetData(Nation.Belgium).Iso_3166_2);
		public static readonly CountryCodeIso2 Belize = new CountryCodeIso2(NationRegistry.GetData(Nation.Belize).Iso_3166_2);
		public static readonly CountryCodeIso2 Benin = new CountryCodeIso2(NationRegistry.GetData(Nation.Benin).Iso_3166_2);
		public static readonly CountryCodeIso2 Bermuda = new CountryCodeIso2(NationRegistry.GetData(Nation.Bermuda).Iso_3166_2);
		public static readonly CountryCodeIso2 Bhutan = new CountryCodeIso2(NationRegistry.GetData(Nation.Bhutan).Iso_3166_2);
		public static readonly CountryCodeIso2 Bolivia = new CountryCodeIso2(NationRegistry.GetData(Nation.Bolivia).Iso_3166_2);
		public static readonly CountryCodeIso2 BonaireAndSinEustatiusAndSaba = new CountryCodeIso2(NationRegistry.GetData(Nation.BonaireAndSinEustatiusAndSaba).Iso_3166_2);
		public static readonly CountryCodeIso2 BosiaAndHerzegovina = new CountryCodeIso2(NationRegistry.GetData(Nation.BosiaAndHerzegovina).Iso_3166_2);
		public static readonly CountryCodeIso2 Botswana = new CountryCodeIso2(NationRegistry.GetData(Nation.Botswana).Iso_3166_2);
		public static readonly CountryCodeIso2 BouvetIsland = new CountryCodeIso2(NationRegistry.GetData(Nation.BouvetIsland).Iso_3166_2);
		public static readonly CountryCodeIso2 Brazil = new CountryCodeIso2(NationRegistry.GetData(Nation.Brazil).Iso_3166_2);
		public static readonly CountryCodeIso2 BritishIndianOceanTerritory = new CountryCodeIso2(NationRegistry.GetData(Nation.BritishIndianOceanTerritory).Iso_3166_2);
		public static readonly CountryCodeIso2 BruneiDarussalam = new CountryCodeIso2(NationRegistry.GetData(Nation.BruneiDarussalam).Iso_3166_2);
		public static readonly CountryCodeIso2 Bulgaria = new CountryCodeIso2(NationRegistry.GetData(Nation.Bulgaria).Iso_3166_2);
		public static readonly CountryCodeIso2 BurkinaFaso = new CountryCodeIso2(NationRegistry.GetData(Nation.BurkinaFaso).Iso_3166_2);
		public static readonly CountryCodeIso2 Burundi = new CountryCodeIso2(NationRegistry.GetData(Nation.Burundi).Iso_3166_2);
		public static readonly CountryCodeIso2 CaboVerde = new CountryCodeIso2(NationRegistry.GetData(Nation.CaboVerde).Iso_3166_2);
		public static readonly CountryCodeIso2 Cambodia = new CountryCodeIso2(NationRegistry.GetData(Nation.Cambodia).Iso_3166_2);
		public static readonly CountryCodeIso2 Cameroon = new CountryCodeIso2(NationRegistry.GetData(Nation.Cameroon).Iso_3166_2);
		public static readonly CountryCodeIso2 Canada = new CountryCodeIso2(NationRegistry.GetData(Nation.Canada).Iso_3166_2);
		public static readonly CountryCodeIso2 CaymanIslands = new CountryCodeIso2(NationRegistry.GetData(Nation.CaymanIslands).Iso_3166_2);
		public static readonly CountryCodeIso2 CentralAfricanRepublic = new CountryCodeIso2(NationRegistry.GetData(Nation.CentralAfricanRepublic).Iso_3166_2);
		public static readonly CountryCodeIso2 Chad = new CountryCodeIso2(NationRegistry.GetData(Nation.Chad).Iso_3166_2);
		public static readonly CountryCodeIso2 Chile = new CountryCodeIso2(NationRegistry.GetData(Nation.Chile).Iso_3166_2);
		public static readonly CountryCodeIso2 China = new CountryCodeIso2(NationRegistry.GetData(Nation.China).Iso_3166_2);
		public static readonly CountryCodeIso2 ChristmasIsland = new CountryCodeIso2(NationRegistry.GetData(Nation.ChristmasIsland).Iso_3166_2);
		public static readonly CountryCodeIso2 CocosIslands = new CountryCodeIso2(NationRegistry.GetData(Nation.CocosIslands).Iso_3166_2);
		public static readonly CountryCodeIso2 Colombia = new CountryCodeIso2(NationRegistry.GetData(Nation.Colombia).Iso_3166_2);
		public static readonly CountryCodeIso2 Comoros = new CountryCodeIso2(NationRegistry.GetData(Nation.Comoros).Iso_3166_2);
		public static readonly CountryCodeIso2 Congo = new CountryCodeIso2(NationRegistry.GetData(Nation.Congo).Iso_3166_2);
		public static readonly CountryCodeIso2 DemocraticRepublicOfTheCongo = new CountryCodeIso2(NationRegistry.GetData(Nation.DemocraticRepublicOfTheCongo).Iso_3166_2);
		public static readonly CountryCodeIso2 CookIslands = new CountryCodeIso2(NationRegistry.GetData(Nation.CookIslands).Iso_3166_2);
		public static readonly CountryCodeIso2 CostaRica = new CountryCodeIso2(NationRegistry.GetData(Nation.CostaRica).Iso_3166_2);
		public static readonly CountryCodeIso2 CoteDIvoire = new CountryCodeIso2(NationRegistry.GetData(Nation.CoteDIvoire).Iso_3166_2);
		public static readonly CountryCodeIso2 Croatia = new CountryCodeIso2(NationRegistry.GetData(Nation.Croatia).Iso_3166_2);
		public static readonly CountryCodeIso2 Cuba = new CountryCodeIso2(NationRegistry.GetData(Nation.Cuba).Iso_3166_2);
		public static readonly CountryCodeIso2 Curacao = new CountryCodeIso2(NationRegistry.GetData(Nation.Curacao).Iso_3166_2);
		public static readonly CountryCodeIso2 Cyprus = new CountryCodeIso2(NationRegistry.GetData(Nation.Cyprus).Iso_3166_2);
		public static readonly CountryCodeIso2 Czechia = new CountryCodeIso2(NationRegistry.GetData(Nation.Czechia).Iso_3166_2);
		public static readonly CountryCodeIso2 Denmark = new CountryCodeIso2(NationRegistry.GetData(Nation.Denmark).Iso_3166_2);
		public static readonly CountryCodeIso2 Djibouti = new CountryCodeIso2(NationRegistry.GetData(Nation.Djibouti).Iso_3166_2);
		public static readonly CountryCodeIso2 Dominica = new CountryCodeIso2(NationRegistry.GetData(Nation.Dominica).Iso_3166_2);
		public static readonly CountryCodeIso2 DominicanRepublic = new CountryCodeIso2(NationRegistry.GetData(Nation.DominicanRepublic).Iso_3166_2);
		public static readonly CountryCodeIso2 Ecuador = new CountryCodeIso2(NationRegistry.GetData(Nation.Ecuador).Iso_3166_2);
		public static readonly CountryCodeIso2 Egypt = new CountryCodeIso2(NationRegistry.GetData(Nation.Egypt).Iso_3166_2);
		public static readonly CountryCodeIso2 ElSalvador = new CountryCodeIso2(NationRegistry.GetData(Nation.ElSalvador).Iso_3166_2);
		public static readonly CountryCodeIso2 EquatorialGuinea = new CountryCodeIso2(NationRegistry.GetData(Nation.EquatorialGuinea).Iso_3166_2);
		public static readonly CountryCodeIso2 Eritrea = new CountryCodeIso2(NationRegistry.GetData(Nation.Eritrea).Iso_3166_2);
		public static readonly CountryCodeIso2 Estonia = new CountryCodeIso2(NationRegistry.GetData(Nation.Estonia).Iso_3166_2);
		public static readonly CountryCodeIso2 Eswatini = new CountryCodeIso2(NationRegistry.GetData(Nation.Eswatini).Iso_3166_2);
		public static readonly CountryCodeIso2 Ethiopia = new CountryCodeIso2(NationRegistry.GetData(Nation.Ethiopia).Iso_3166_2);
		public static readonly CountryCodeIso2 FalklandIslands = new CountryCodeIso2(NationRegistry.GetData(Nation.FalklandIslands).Iso_3166_2);
		public static readonly CountryCodeIso2 FaroeIslands = new CountryCodeIso2(NationRegistry.GetData(Nation.FaroeIslands).Iso_3166_2);
		public static readonly CountryCodeIso2 Fiji = new CountryCodeIso2(NationRegistry.GetData(Nation.Fiji).Iso_3166_2);
		public static readonly CountryCodeIso2 Finland = new CountryCodeIso2(NationRegistry.GetData(Nation.Finland).Iso_3166_2);
		public static readonly CountryCodeIso2 France = new CountryCodeIso2(NationRegistry.GetData(Nation.France).Iso_3166_2);
		public static readonly CountryCodeIso2 FrenchGuiana = new CountryCodeIso2(NationRegistry.GetData(Nation.FrenchGuiana).Iso_3166_2);
		public static readonly CountryCodeIso2 FrenchPolynesia = new CountryCodeIso2(NationRegistry.GetData(Nation.FrenchPolynesia).Iso_3166_2);
		public static readonly CountryCodeIso2 FrenchSouthernTerritories = new CountryCodeIso2(NationRegistry.GetData(Nation.FrenchSouthernTerritories).Iso_3166_2);
		public static readonly CountryCodeIso2 Gabon = new CountryCodeIso2(NationRegistry.GetData(Nation.Gabon).Iso_3166_2);
		public static readonly CountryCodeIso2 Gambia = new CountryCodeIso2(NationRegistry.GetData(Nation.Gambia).Iso_3166_2);
		public static readonly CountryCodeIso2 Georgia = new CountryCodeIso2(NationRegistry.GetData(Nation.Georgia).Iso_3166_2);
		public static readonly CountryCodeIso2 Germany = new CountryCodeIso2(NationRegistry.GetData(Nation.Germany).Iso_3166_2);
		public static readonly CountryCodeIso2 Ghana = new CountryCodeIso2(NationRegistry.GetData(Nation.Ghana).Iso_3166_2);
		public static readonly CountryCodeIso2 Gibraltar = new CountryCodeIso2(NationRegistry.GetData(Nation.Gibraltar).Iso_3166_2);
		public static readonly CountryCodeIso2 Greece = new CountryCodeIso2(NationRegistry.GetData(Nation.Greece).Iso_3166_2);
		public static readonly CountryCodeIso2 Greenland = new CountryCodeIso2(NationRegistry.GetData(Nation.Greenland).Iso_3166_2);
		public static readonly CountryCodeIso2 Grenada = new CountryCodeIso2(NationRegistry.GetData(Nation.Grenada).Iso_3166_2);
		public static readonly CountryCodeIso2 Guadeloupe = new CountryCodeIso2(NationRegistry.GetData(Nation.Guadeloupe).Iso_3166_2);
		public static readonly CountryCodeIso2 Guam = new CountryCodeIso2(NationRegistry.GetData(Nation.Guam).Iso_3166_2);
		public static readonly CountryCodeIso2 Guatemala = new CountryCodeIso2(NationRegistry.GetData(Nation.Guatemala).Iso_3166_2);
		public static readonly CountryCodeIso2 Guernsey = new CountryCodeIso2(NationRegistry.GetData(Nation.Guernsey).Iso_3166_2);
		public static readonly CountryCodeIso2 Guinea = new CountryCodeIso2(NationRegistry.GetData(Nation.Guinea).Iso_3166_2);
		public static readonly CountryCodeIso2 GuineaBissau = new CountryCodeIso2(NationRegistry.GetData(Nation.GuineaBissau).Iso_3166_2);
		public static readonly CountryCodeIso2 Guyana = new CountryCodeIso2(NationRegistry.GetData(Nation.Guyana).Iso_3166_2);
		public static readonly CountryCodeIso2 Haiti = new CountryCodeIso2(NationRegistry.GetData(Nation.Haiti).Iso_3166_2);
		public static readonly CountryCodeIso2 HeardAndMcDonaldIslands = new CountryCodeIso2(NationRegistry.GetData(Nation.HeardAndMcDonaldIslands).Iso_3166_2);
		public static readonly CountryCodeIso2 HolySee = new CountryCodeIso2(NationRegistry.GetData(Nation.HolySee).Iso_3166_2);
		public static readonly CountryCodeIso2 Honduras = new CountryCodeIso2(NationRegistry.GetData(Nation.Honduras).Iso_3166_2);
		public static readonly CountryCodeIso2 HongKong = new CountryCodeIso2(NationRegistry.GetData(Nation.HongKong).Iso_3166_2);
		public static readonly CountryCodeIso2 Hungary = new CountryCodeIso2(NationRegistry.GetData(Nation.Hungary).Iso_3166_2);
		public static readonly CountryCodeIso2 Iceland = new CountryCodeIso2(NationRegistry.GetData(Nation.Iceland).Iso_3166_2);
		public static readonly CountryCodeIso2 India = new CountryCodeIso2(NationRegistry.GetData(Nation.India).Iso_3166_2);
		public static readonly CountryCodeIso2 Indonesia = new CountryCodeIso2(NationRegistry.GetData(Nation.Indonesia).Iso_3166_2);
		public static readonly CountryCodeIso2 Iran = new CountryCodeIso2(NationRegistry.GetData(Nation.Iran).Iso_3166_2);
		public static readonly CountryCodeIso2 Iraq = new CountryCodeIso2(NationRegistry.GetData(Nation.Iraq).Iso_3166_2);
		public static readonly CountryCodeIso2 Ireland = new CountryCodeIso2(NationRegistry.GetData(Nation.Ireland).Iso_3166_2);
		public static readonly CountryCodeIso2 IsleOfMan = new CountryCodeIso2(NationRegistry.GetData(Nation.IsleOfMan).Iso_3166_2);
		public static readonly CountryCodeIso2 Israel = new CountryCodeIso2(NationRegistry.GetData(Nation.Israel).Iso_3166_2);
		public static readonly CountryCodeIso2 Italy = new CountryCodeIso2(NationRegistry.GetData(Nation.Italy).Iso_3166_2);
		public static readonly CountryCodeIso2 Jamaica = new CountryCodeIso2(NationRegistry.GetData(Nation.Jamaica).Iso_3166_2);
		public static readonly CountryCodeIso2 Japan = new CountryCodeIso2(NationRegistry.GetData(Nation.Japan).Iso_3166_2);
		public static readonly CountryCodeIso2 Jersey = new CountryCodeIso2(NationRegistry.GetData(Nation.Jersey).Iso_3166_2);
		public static readonly CountryCodeIso2 Jordan = new CountryCodeIso2(NationRegistry.GetData(Nation.Jordan).Iso_3166_2);
		public static readonly CountryCodeIso2 Kazakhstan = new CountryCodeIso2(NationRegistry.GetData(Nation.Kazakhstan).Iso_3166_2);
		public static readonly CountryCodeIso2 Kenya = new CountryCodeIso2(NationRegistry.GetData(Nation.Kenya).Iso_3166_2);
		public static readonly CountryCodeIso2 Kiribati = new CountryCodeIso2(NationRegistry.GetData(Nation.Kiribati).Iso_3166_2);
		public static readonly CountryCodeIso2 NorthKorea = new CountryCodeIso2(NationRegistry.GetData(Nation.NorthKorea).Iso_3166_2);
		public static readonly CountryCodeIso2 SouthKorea = new CountryCodeIso2(NationRegistry.GetData(Nation.SouthKorea).Iso_3166_2);
		public static readonly CountryCodeIso2 Kuwait = new CountryCodeIso2(NationRegistry.GetData(Nation.Kuwait).Iso_3166_2);
		public static readonly CountryCodeIso2 Kyrgyzstan = new CountryCodeIso2(NationRegistry.GetData(Nation.Kyrgyzstan).Iso_3166_2);
		public static readonly CountryCodeIso2 Laos = new CountryCodeIso2(NationRegistry.GetData(Nation.Laos).Iso_3166_2);
		public static readonly CountryCodeIso2 Latvia = new CountryCodeIso2(NationRegistry.GetData(Nation.Latvia).Iso_3166_2);
		public static readonly CountryCodeIso2 Lebanon = new CountryCodeIso2(NationRegistry.GetData(Nation.Lebanon).Iso_3166_2);
		public static readonly CountryCodeIso2 Lesotho = new CountryCodeIso2(NationRegistry.GetData(Nation.Lesotho).Iso_3166_2);
		public static readonly CountryCodeIso2 Liberia = new CountryCodeIso2(NationRegistry.GetData(Nation.Liberia).Iso_3166_2);
		public static readonly CountryCodeIso2 Libya = new CountryCodeIso2(NationRegistry.GetData(Nation.Libya).Iso_3166_2);
		public static readonly CountryCodeIso2 Liechtenstein = new CountryCodeIso2(NationRegistry.GetData(Nation.Liechtenstein).Iso_3166_2);
		public static readonly CountryCodeIso2 Lithuania = new CountryCodeIso2(NationRegistry.GetData(Nation.Lithuania).Iso_3166_2);
		public static readonly CountryCodeIso2 Luxembourg = new CountryCodeIso2(NationRegistry.GetData(Nation.Luxembourg).Iso_3166_2);
		public static readonly CountryCodeIso2 Macao = new CountryCodeIso2(NationRegistry.GetData(Nation.Macao).Iso_3166_2);
		public static readonly CountryCodeIso2 Macedonia = new CountryCodeIso2(NationRegistry.GetData(Nation.Macedonia).Iso_3166_2);
		public static readonly CountryCodeIso2 Madagascar = new CountryCodeIso2(NationRegistry.GetData(Nation.Madagascar).Iso_3166_2);
		public static readonly CountryCodeIso2 Malawi = new CountryCodeIso2(NationRegistry.GetData(Nation.Malawi).Iso_3166_2);
		public static readonly CountryCodeIso2 Malaysia = new CountryCodeIso2(NationRegistry.GetData(Nation.Malaysia).Iso_3166_2);
		public static readonly CountryCodeIso2 Maldives = new CountryCodeIso2(NationRegistry.GetData(Nation.Maldives).Iso_3166_2);
		public static readonly CountryCodeIso2 Mali = new CountryCodeIso2(NationRegistry.GetData(Nation.Mali).Iso_3166_2);
		public static readonly CountryCodeIso2 Malta = new CountryCodeIso2(NationRegistry.GetData(Nation.Malta).Iso_3166_2);
		public static readonly CountryCodeIso2 MarshallIslands = new CountryCodeIso2(NationRegistry.GetData(Nation.MarshallIslands).Iso_3166_2);
		public static readonly CountryCodeIso2 Martinique = new CountryCodeIso2(NationRegistry.GetData(Nation.Martinique).Iso_3166_2);
		public static readonly CountryCodeIso2 Mauritania = new CountryCodeIso2(NationRegistry.GetData(Nation.Mauritania).Iso_3166_2);
		public static readonly CountryCodeIso2 Mauritius = new CountryCodeIso2(NationRegistry.GetData(Nation.Mauritius).Iso_3166_2);
		public static readonly CountryCodeIso2 Mayotte = new CountryCodeIso2(NationRegistry.GetData(Nation.Mayotte).Iso_3166_2);
		public static readonly CountryCodeIso2 Mexico = new CountryCodeIso2(NationRegistry.GetData(Nation.Mexico).Iso_3166_2);
		public static readonly CountryCodeIso2 Micronesia = new CountryCodeIso2(NationRegistry.GetData(Nation.Micronesia).Iso_3166_2);
		public static readonly CountryCodeIso2 Moldova = new CountryCodeIso2(NationRegistry.GetData(Nation.Moldova).Iso_3166_2);
		public static readonly CountryCodeIso2 Monaco = new CountryCodeIso2(NationRegistry.GetData(Nation.Monaco).Iso_3166_2);
		public static readonly CountryCodeIso2 Mongolia = new CountryCodeIso2(NationRegistry.GetData(Nation.Mongolia).Iso_3166_2);
		public static readonly CountryCodeIso2 Montenegro = new CountryCodeIso2(NationRegistry.GetData(Nation.Montenegro).Iso_3166_2);
		public static readonly CountryCodeIso2 Montserrat = new CountryCodeIso2(NationRegistry.GetData(Nation.Montserrat).Iso_3166_2);
		public static readonly CountryCodeIso2 Morocco = new CountryCodeIso2(NationRegistry.GetData(Nation.Morocco).Iso_3166_2);
		public static readonly CountryCodeIso2 Mozambique = new CountryCodeIso2(NationRegistry.GetData(Nation.Mozambique).Iso_3166_2);
		public static readonly CountryCodeIso2 Myanmar = new CountryCodeIso2(NationRegistry.GetData(Nation.Myanmar).Iso_3166_2);
		public static readonly CountryCodeIso2 Namibia = new CountryCodeIso2(NationRegistry.GetData(Nation.Namibia).Iso_3166_2);
		public static readonly CountryCodeIso2 Nauru = new CountryCodeIso2(NationRegistry.GetData(Nation.Nauru).Iso_3166_2);
		public static readonly CountryCodeIso2 Nepal = new CountryCodeIso2(NationRegistry.GetData(Nation.Nepal).Iso_3166_2);
		public static readonly CountryCodeIso2 Netherlands = new CountryCodeIso2(NationRegistry.GetData(Nation.Netherlands).Iso_3166_2);
		public static readonly CountryCodeIso2 NewCaledonia = new CountryCodeIso2(NationRegistry.GetData(Nation.NewCaledonia).Iso_3166_2);
		public static readonly CountryCodeIso2 NewZealand = new CountryCodeIso2(NationRegistry.GetData(Nation.NewZealand).Iso_3166_2);
		public static readonly CountryCodeIso2 Nicaragua = new CountryCodeIso2(NationRegistry.GetData(Nation.Nicaragua).Iso_3166_2);
		public static readonly CountryCodeIso2 Niger = new CountryCodeIso2(NationRegistry.GetData(Nation.Niger).Iso_3166_2);
		public static readonly CountryCodeIso2 Nigeria = new CountryCodeIso2(NationRegistry.GetData(Nation.Nigeria).Iso_3166_2);
		public static readonly CountryCodeIso2 Niue = new CountryCodeIso2(NationRegistry.GetData(Nation.Niue).Iso_3166_2);
		public static readonly CountryCodeIso2 NorfolkIsland = new CountryCodeIso2(NationRegistry.GetData(Nation.NorfolkIsland).Iso_3166_2);
		public static readonly CountryCodeIso2 NorthernMarianaIslands = new CountryCodeIso2(NationRegistry.GetData(Nation.NorthernMarianaIslands).Iso_3166_2);
		public static readonly CountryCodeIso2 Norway = new CountryCodeIso2(NationRegistry.GetData(Nation.Norway).Iso_3166_2);
		public static readonly CountryCodeIso2 Oman = new CountryCodeIso2(NationRegistry.GetData(Nation.Oman).Iso_3166_2);
		public static readonly CountryCodeIso2 Pakistan = new CountryCodeIso2(NationRegistry.GetData(Nation.Pakistan).Iso_3166_2);
		public static readonly CountryCodeIso2 Palau = new CountryCodeIso2(NationRegistry.GetData(Nation.Palau).Iso_3166_2);
		public static readonly CountryCodeIso2 Palestine = new CountryCodeIso2(NationRegistry.GetData(Nation.Palestine).Iso_3166_2);
		public static readonly CountryCodeIso2 Panama = new CountryCodeIso2(NationRegistry.GetData(Nation.Panama).Iso_3166_2);
		public static readonly CountryCodeIso2 PapuaNewGuinea = new CountryCodeIso2(NationRegistry.GetData(Nation.PapuaNewGuinea).Iso_3166_2);
		public static readonly CountryCodeIso2 Paraguay = new CountryCodeIso2(NationRegistry.GetData(Nation.Paraguay).Iso_3166_2);
		public static readonly CountryCodeIso2 Peru = new CountryCodeIso2(NationRegistry.GetData(Nation.Peru).Iso_3166_2);
		public static readonly CountryCodeIso2 Philippines = new CountryCodeIso2(NationRegistry.GetData(Nation.Philippines).Iso_3166_2);
		public static readonly CountryCodeIso2 Pitcairn = new CountryCodeIso2(NationRegistry.GetData(Nation.Pitcairn).Iso_3166_2);
		public static readonly CountryCodeIso2 Poland = new CountryCodeIso2(NationRegistry.GetData(Nation.Poland).Iso_3166_2);
		public static readonly CountryCodeIso2 Portugal = new CountryCodeIso2(NationRegistry.GetData(Nation.Portugal).Iso_3166_2);
		public static readonly CountryCodeIso2 PuertoRico = new CountryCodeIso2(NationRegistry.GetData(Nation.PuertoRico).Iso_3166_2);
		public static readonly CountryCodeIso2 Qatar = new CountryCodeIso2(NationRegistry.GetData(Nation.Qatar).Iso_3166_2);
		public static readonly CountryCodeIso2 Reunion = new CountryCodeIso2(NationRegistry.GetData(Nation.Reunion).Iso_3166_2);
		public static readonly CountryCodeIso2 Romania = new CountryCodeIso2(NationRegistry.GetData(Nation.Romania).Iso_3166_2);
		public static readonly CountryCodeIso2 Russia = new CountryCodeIso2(NationRegistry.GetData(Nation.Russia).Iso_3166_2);
		public static readonly CountryCodeIso2 Rwanda = new CountryCodeIso2(NationRegistry.GetData(Nation.Rwanda).Iso_3166_2);
		public static readonly CountryCodeIso2 SaintBarthelemy = new CountryCodeIso2(NationRegistry.GetData(Nation.SaintBarthelemy).Iso_3166_2);
		public static readonly CountryCodeIso2 SaintHelenaAndAscensionAndTristanDaCunha = new CountryCodeIso2(NationRegistry.GetData(Nation.SaintHelenaAndAscensionAndTristanDaCunha).Iso_3166_2);
		public static readonly CountryCodeIso2 SaintKittsAndNevis = new CountryCodeIso2(NationRegistry.GetData(Nation.SaintKittsAndNevis).Iso_3166_2);
		public static readonly CountryCodeIso2 SaintLucia = new CountryCodeIso2(NationRegistry.GetData(Nation.SaintLucia).Iso_3166_2);
		public static readonly CountryCodeIso2 FrenchSaintMartin = new CountryCodeIso2(NationRegistry.GetData(Nation.FrenchSaintMartin).Iso_3166_2);
		public static readonly CountryCodeIso2 SaintPierreAndMiquelon = new CountryCodeIso2(NationRegistry.GetData(Nation.SaintPierreAndMiquelon).Iso_3166_2);
		public static readonly CountryCodeIso2 SaintVincentAndGrenadines = new CountryCodeIso2(NationRegistry.GetData(Nation.SaintVincentAndGrenadines).Iso_3166_2);
		public static readonly CountryCodeIso2 Samoa = new CountryCodeIso2(NationRegistry.GetData(Nation.Samoa).Iso_3166_2);
		public static readonly CountryCodeIso2 SanMarino = new CountryCodeIso2(NationRegistry.GetData(Nation.SanMarino).Iso_3166_2);
		public static readonly CountryCodeIso2 SaoTomeAndPrincipe = new CountryCodeIso2(NationRegistry.GetData(Nation.SaoTomeAndPrincipe).Iso_3166_2);
		public static readonly CountryCodeIso2 SaudiArabia = new CountryCodeIso2(NationRegistry.GetData(Nation.SaudiArabia).Iso_3166_2);
		public static readonly CountryCodeIso2 Senegal = new CountryCodeIso2(NationRegistry.GetData(Nation.Senegal).Iso_3166_2);
		public static readonly CountryCodeIso2 Serbia = new CountryCodeIso2(NationRegistry.GetData(Nation.Serbia).Iso_3166_2);
		public static readonly CountryCodeIso2 Seychelles = new CountryCodeIso2(NationRegistry.GetData(Nation.Seychelles).Iso_3166_2);
		public static readonly CountryCodeIso2 SierraLeone = new CountryCodeIso2(NationRegistry.GetData(Nation.SierraLeone).Iso_3166_2);
		public static readonly CountryCodeIso2 Singapore = new CountryCodeIso2(NationRegistry.GetData(Nation.Singapore).Iso_3166_2);
		public static readonly CountryCodeIso2 DutchSaintMaarten = new CountryCodeIso2(NationRegistry.GetData(Nation.DutchSaintMaarten).Iso_3166_2);
		public static readonly CountryCodeIso2 Slovakia = new CountryCodeIso2(NationRegistry.GetData(Nation.Slovakia).Iso_3166_2);
		public static readonly CountryCodeIso2 Slovenia = new CountryCodeIso2(NationRegistry.GetData(Nation.Slovenia).Iso_3166_2);
		public static readonly CountryCodeIso2 SolomonIslands = new CountryCodeIso2(NationRegistry.GetData(Nation.SolomonIslands).Iso_3166_2);
		public static readonly CountryCodeIso2 Somalia = new CountryCodeIso2(NationRegistry.GetData(Nation.Somalia).Iso_3166_2);
		public static readonly CountryCodeIso2 SouthAfrica = new CountryCodeIso2(NationRegistry.GetData(Nation.SouthAfrica).Iso_3166_2);
		public static readonly CountryCodeIso2 SouthGeorgiaAndSouthSandwichIslands = new CountryCodeIso2(NationRegistry.GetData(Nation.SouthGeorgiaAndSouthSandwichIslands).Iso_3166_2);
		public static readonly CountryCodeIso2 SouthSudan = new CountryCodeIso2(NationRegistry.GetData(Nation.SouthSudan).Iso_3166_2);
		public static readonly CountryCodeIso2 Spain = new CountryCodeIso2(NationRegistry.GetData(Nation.Spain).Iso_3166_2);
		public static readonly CountryCodeIso2 SriLanka = new CountryCodeIso2(NationRegistry.GetData(Nation.SriLanka).Iso_3166_2);
		public static readonly CountryCodeIso2 Sudan = new CountryCodeIso2(NationRegistry.GetData(Nation.Sudan).Iso_3166_2);
		public static readonly CountryCodeIso2 Suriname = new CountryCodeIso2(NationRegistry.GetData(Nation.Suriname).Iso_3166_2);
		public static readonly CountryCodeIso2 SvalbardAndJanMayen = new CountryCodeIso2(NationRegistry.GetData(Nation.SvalbardAndJanMayen).Iso_3166_2);
		public static readonly CountryCodeIso2 Sweden = new CountryCodeIso2(NationRegistry.GetData(Nation.Sweden).Iso_3166_2);
		public static readonly CountryCodeIso2 Switzerland = new CountryCodeIso2(NationRegistry.GetData(Nation.Switzerland).Iso_3166_2);
		public static readonly CountryCodeIso2 Syria = new CountryCodeIso2(NationRegistry.GetData(Nation.Syria).Iso_3166_2);
		public static readonly CountryCodeIso2 Taiwan = new CountryCodeIso2(NationRegistry.GetData(Nation.Taiwan).Iso_3166_2);
		public static readonly CountryCodeIso2 Tajikistan = new CountryCodeIso2(NationRegistry.GetData(Nation.Tajikistan).Iso_3166_2);
		public static readonly CountryCodeIso2 Tanzania = new CountryCodeIso2(NationRegistry.GetData(Nation.Tanzania).Iso_3166_2);
		public static readonly CountryCodeIso2 Thailand = new CountryCodeIso2(NationRegistry.GetData(Nation.Thailand).Iso_3166_2);
		public static readonly CountryCodeIso2 TimorLeste = new CountryCodeIso2(NationRegistry.GetData(Nation.TimorLeste).Iso_3166_2);
		public static readonly CountryCodeIso2 Togo = new CountryCodeIso2(NationRegistry.GetData(Nation.Togo).Iso_3166_2);
		public static readonly CountryCodeIso2 Tokelau = new CountryCodeIso2(NationRegistry.GetData(Nation.Tokelau).Iso_3166_2);
		public static readonly CountryCodeIso2 Tonga = new CountryCodeIso2(NationRegistry.GetData(Nation.Tonga).Iso_3166_2);
		public static readonly CountryCodeIso2 TrinidadAndTobago = new CountryCodeIso2(NationRegistry.GetData(Nation.TrinidadAndTobago).Iso_3166_2);
		public static readonly CountryCodeIso2 Tunisia = new CountryCodeIso2(NationRegistry.GetData(Nation.Tunisia).Iso_3166_2);
		public static readonly CountryCodeIso2 Turkey = new CountryCodeIso2(NationRegistry.GetData(Nation.Turkey).Iso_3166_2);
		public static readonly CountryCodeIso2 Turkmenistan = new CountryCodeIso2(NationRegistry.GetData(Nation.Turkmenistan).Iso_3166_2);
		public static readonly CountryCodeIso2 TurksAndCaicosIslands = new CountryCodeIso2(NationRegistry.GetData(Nation.TurksAndCaicosIslands).Iso_3166_2);
		public static readonly CountryCodeIso2 Tuvalu = new CountryCodeIso2(NationRegistry.GetData(Nation.Tuvalu).Iso_3166_2);
		public static readonly CountryCodeIso2 Uganda = new CountryCodeIso2(NationRegistry.GetData(Nation.Uganda).Iso_3166_2);
		public static readonly CountryCodeIso2 Ukraine = new CountryCodeIso2(NationRegistry.GetData(Nation.Ukraine).Iso_3166_2);
		public static readonly CountryCodeIso2 UnitedArabEmirates = new CountryCodeIso2(NationRegistry.GetData(Nation.UnitedArabEmirates).Iso_3166_2);
		public static readonly CountryCodeIso2 UnitedKingdom = new CountryCodeIso2(NationRegistry.GetData(Nation.UnitedKingdom).Iso_3166_2);
		public static readonly CountryCodeIso2 UnitedStates = new CountryCodeIso2(NationRegistry.GetData(Nation.UnitedStates).Iso_3166_2);
		public static readonly CountryCodeIso2 UsMinorOutlyingIslands = new CountryCodeIso2(NationRegistry.GetData(Nation.UsMinorOutlyingIslands).Iso_3166_2);
		public static readonly CountryCodeIso2 Uruguay = new CountryCodeIso2(NationRegistry.GetData(Nation.Uruguay).Iso_3166_2);
		public static readonly CountryCodeIso2 Uzbekistan = new CountryCodeIso2(NationRegistry.GetData(Nation.Uzbekistan).Iso_3166_2);
		public static readonly CountryCodeIso2 Vanuatu = new CountryCodeIso2(NationRegistry.GetData(Nation.Vanuatu).Iso_3166_2);
		public static readonly CountryCodeIso2 Venezuela = new CountryCodeIso2(NationRegistry.GetData(Nation.Venezuela).Iso_3166_2);
		public static readonly CountryCodeIso2 VietNam = new CountryCodeIso2(NationRegistry.GetData(Nation.VietNam).Iso_3166_2);
		public static readonly CountryCodeIso2 BritishVirginIslands = new CountryCodeIso2(NationRegistry.GetData(Nation.BritishVirginIslands).Iso_3166_2);
		public static readonly CountryCodeIso2 UsVirginIslands = new CountryCodeIso2(NationRegistry.GetData(Nation.UsVirginIslands).Iso_3166_2);
		public static readonly CountryCodeIso2 WallisAndFutuna = new CountryCodeIso2(NationRegistry.GetData(Nation.WallisAndFutuna).Iso_3166_2);
		public static readonly CountryCodeIso2 WesternSahara = new CountryCodeIso2(NationRegistry.GetData(Nation.WesternSahara).Iso_3166_2);
		public static readonly CountryCodeIso2 Yemen = new CountryCodeIso2(NationRegistry.GetData(Nation.Yemen).Iso_3166_2);
		public static readonly CountryCodeIso2 Zambia = new CountryCodeIso2(NationRegistry.GetData(Nation.Zambia).Iso_3166_2);
		public static readonly CountryCodeIso2 Zimbabwe = new CountryCodeIso2(NationRegistry.GetData(Nation.Zimbabwe).Iso_3166_2);
    }
}
