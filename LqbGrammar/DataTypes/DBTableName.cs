﻿namespace LqbGrammar.DataTypes
{
    using System;
    using System.Collections.Generic;
    using LqbGrammar.Exceptions;
    using LqbGrammar.Validation;

    /// <summary>
    /// Encapsulate a database table name so that it can be validated prior to use.
    /// </summary>
    public struct DBTableName : System.IEquatable<DBTableName>
    {
        /// <summary>
        /// An instance of a bad table name that can be used for variable initialization.
        /// </summary>
        public static readonly DBTableName Invalid = new DBTableName(string.Empty);

        /// <summary>
        /// The database table name.
        /// </summary>
        private string tableName;

        /// <summary>
        /// Initializes a new instance of the <see cref="DBTableName"/> struct.
        /// </summary>
        /// <param name="name">The table name.</param>
        private DBTableName(string name)
        {
            this.tableName = name;
        }

        /// <summary>
        /// Parse the input table name and validate it.
        /// </summary>
        /// <param name="name">The table name that is parsed.</param>
        /// <returns>The validated table name, or null.</returns>
        public static DBTableName? Create(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                return null;
            }

            RegularExpressionResult result = RegularExpressionEngine.Execute(name, RegularExpressionString.DBTableName);
            if (!result.IsMatch())
            {
                return null;
            }

            return new DBTableName(name);
        }

        /// <summary>
        /// Compare to objects for equality and that they are instances of the DBTableName class.
        /// </summary>
        /// <param name="lhs">The object on the left side of the == operator.</param>
        /// <param name="rhs">The object on the right side of the == operator.</param>
        /// <returns>True if both objects are instances of the DBTableName class and equal to each other.</returns>
        public static new bool Equals(object lhs, object rhs)
        {
            if (!(lhs is DBTableName))
            {
                return false;
            }

            if (!(rhs is DBTableName))
            {
                return false;
            }

            return ((DBTableName)lhs).Equals((DBTableName)rhs);
        }

        /// <summary>
        /// The equalitiy operator.
        /// </summary>
        /// <param name="lhs">The instance that is on the left side of the == operator.</param>
        /// <param name="rhs">The instance that is on the right side of the == operator.</param>
        /// <returns>True if the instances are equal, false otherwise.</returns>
        public static bool operator ==(DBTableName lhs, DBTableName rhs)
        {
            return lhs.Equals(rhs);
        }

        /// <summary>
        /// The inequality operator.
        /// </summary>
        /// <param name="lhs">The instance that is on the left side of the != operator.</param>
        /// <param name="rhs">The instance that is on the right side of the != operator.</param>
        /// <returns>True if the two instances are not equal, false otherwise.</returns>
        public static bool operator !=(DBTableName lhs, DBTableName rhs)
        {
            return !lhs.Equals(rhs);
        }

        /// <summary>
        /// Override the implementation inherited from the Object class.
        /// </summary>
        /// <returns>The table name.</returns>
        public override string ToString()
        {
            return this.tableName;
        }

        /// <summary>
        /// Compare this instance with another instance for equality.
        /// </summary>
        /// <param name="other">The other instance.</param>
        /// <returns>True if the two instances are equal.</returns>
        public bool Equals(DBTableName other)
        {
            return this.tableName == other.tableName;
        }

        /// <summary>
        /// Compare this instance with another object for equality.
        /// </summary>
        /// <param name="obj">The other object.</param>
        /// <returns>True if the other object is an instance of the DBTableName class and it is equal to this instance.</returns>
        public override bool Equals(object obj)
        {
            if (!(obj is DBTableName))
            {
                return false;
            }

            return Equals((DBTableName)obj);
        }

        /// <summary>
        /// Override the implementation inherited from the Object class.
        /// </summary>
        /// <returns>An integer that is the hash of this object.</returns>
        public override int GetHashCode()
        {
            return this.tableName.GetHashCode();
        }
    }
}
