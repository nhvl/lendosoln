﻿namespace LqbGrammar.DataTypes
{
    using System;
    using LqbGrammar.Exceptions;
    using LqbGrammar.Validation;

    /// <summary>
    /// Encapsulate and validate the name of an email server.
    /// </summary>
    public struct EmailServerName : System.IEquatable<EmailServerName>
    {
        /// <summary>
        /// The standard port for sending emails.
        /// </summary>
        public const int StandardEmailPort = 25;

        /// <summary>
        /// The standard port for pulling emails from a pop3 server.
        /// </summary>
        public const int Pop3Port = 110;

        /// <summary>
        /// An instance of an invalid value that can be used for variable initialization.
        /// </summary>
        public static readonly EmailServerName BadName = new EmailServerName(string.Empty);

        /// <summary>
        /// The name of the email server.
        /// </summary>
        private string name;

        /// <summary>
        /// Initializes a new instance of the <see cref="EmailServerName"/> struct.
        /// </summary>
        /// <param name="name">The server name.</param>
        private EmailServerName(string name)
        {
            this.name = name;
        }

        /// <summary>
        /// Validate the input email server name.
        /// </summary>
        /// <param name="name">The input email server name to be validated.</param>
        /// <returns>The validated value, or null.</returns>
        public static EmailServerName? Create(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                return null;
            }

            RegularExpressionResult result = RegularExpressionEngine.Execute(name, RegularExpressionString.EmailServer);
            if (!result.IsMatch())
            {
                return null;
            }

            return new EmailServerName(name);
        }

        /// <summary>
        /// Compare two objects for equality and that they are both validated email server names.
        /// </summary>
        /// <param name="lhs">Object on the left side of the == operator.</param>
        /// <param name="rhs">Object on the right side of the == operator.</param>
        /// <returns>True if both objects are instances of the EmailServerName class, and are equal to each other.</returns>
        public static new bool Equals(object lhs, object rhs)
        {
            if (!(lhs is EmailServerName))
            {
                return false;
            }

            if (!(rhs is EmailServerName))
            {
                return false;
            }

            return ((EmailServerName)lhs).Equals((EmailServerName)rhs);
        }

        /// <summary>
        /// The equality operator for comparing two instances of a valid email server name.
        /// </summary>
        /// <param name="lhs">The instance on the left side of the == operator.</param>
        /// <param name="rhs">The instance on the right side of the == operator.</param>
        /// <returns>True if the two instances have the same name.</returns>
        public static bool operator ==(EmailServerName lhs, EmailServerName rhs)
        {
            return lhs.Equals(rhs);
        }

        /// <summary>
        /// The inequality operator for comparing two instances of a valid email server name.
        /// </summary>
        /// <param name="lhs">The instance on the left side of the != operator.</param>
        /// <param name="rhs">The instance on the right side of the != operator.</param>
        /// <returns>True if the two instances are not equal to each other.</returns>
        public static bool operator !=(EmailServerName lhs, EmailServerName rhs)
        {
            return !lhs.Equals(rhs);
        }

        /// <summary>
        /// Override the implementation inherited from the Object class.
        /// </summary>
        /// <returns>The email server name.</returns>
        public override string ToString()
        {
            return this.name;
        }

        /// <summary>
        /// Test this instance against another for equality.
        /// </summary>
        /// <param name="other">The other instance.</param>
        /// <returns>True if the two instances are equal, false otherwise.</returns>
        public bool Equals(EmailServerName other)
        {
            return string.Compare(this.name, other.name, StringComparison.OrdinalIgnoreCase) == 0;
        }

        /// <summary>
        /// Test this instance against another object for equality.
        /// </summary>
        /// <param name="obj">The other object.</param>
        /// <returns>True if the other object is a validated email server name and the names are equal.</returns>
        public override bool Equals(object obj)
        {
            if (!(obj is EmailServerName))
            {
                return false;
            }

            return Equals((EmailServerName)obj);
        }

        /// <summary>
        /// Override the implementation inherited from the Object class.
        /// </summary>
        /// <returns>An integer that is the hash of this instance.</returns>
        public override int GetHashCode()
        {
            return this.name.GetHashCode();
        }
    }
}
