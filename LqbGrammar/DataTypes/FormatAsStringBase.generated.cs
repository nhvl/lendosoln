﻿// <auto-generated />
namespace LqbGrammar.DataTypes
{
    using System;

    /// <summary>
    /// Base class providing default implementation for formatting semantic types into string values.
    /// </summary>
    public partial class FormatAsStringBase : IFormatAsString
    {
		public virtual string Format(AddressUnitType? value)
		{
			return this.Format(value, string.Empty);
		}
		public virtual string Format(AddressUnitType? value, string defaultValue)
		{
			return value == null ? defaultValue : value.Value.ToString();
		}
		public virtual string Format(AddressUnitIdentifier? value)
		{
			return this.Format(value, string.Empty);
		}
		public virtual string Format(AddressUnitIdentifier? value, string defaultValue)
		{
			return value == null ? defaultValue : value.Value.ToString();
		}
		public virtual string Format(AdministrativeArea? value)
		{
			return this.Format(value, string.Empty);
		}
		public virtual string Format(AdministrativeArea? value, string defaultValue)
		{
			return value == null ? defaultValue : value.Value.ToString();
		}
		public virtual string Format(ApproximateDate? value)
		{
			return this.Format(value, string.Empty);
		}
		public virtual string Format(ApproximateDate? value, string defaultValue)
		{
			return value == null ? defaultValue : value.Value.ToString();
		}
		public virtual string Format(BankAccountNumber? value)
		{
			return this.Format(value, string.Empty);
		}
		public virtual string Format(BankAccountNumber? value, string defaultValue)
		{
			return value == null ? defaultValue : value.Value.ToString();
		}
		public virtual string Format(BooleanKleene? value)
		{
			return this.Format(value, string.Empty);
		}
		public virtual string Format(BooleanKleene? value, string defaultValue)
		{
			return value == null ? defaultValue : value.Value.ToString();
		}
		public virtual string Format(City? value)
		{
			return this.Format(value, string.Empty);
		}
		public virtual string Format(City? value, string defaultValue)
		{
			return value == null ? defaultValue : value.Value.ToString();
		}
		public virtual string Format(CityState? value)
		{
			return this.Format(value, string.Empty);
		}
		public virtual string Format(CityState? value, string defaultValue)
		{
			return value == null ? defaultValue : value.Value.ToString();
		}
		public virtual string Format<T>(Count<T>? value)
			where T : UnitType
		{
			return this.Format<T>(value, string.Empty);
		}
		public virtual string Format<T>(Count<T>? value, string defaultValue)
			where T : UnitType
		{
			return value == null ? defaultValue : value.Value.ToString();
		}
		public virtual string Format(Country? value)
		{
			return this.Format(value, string.Empty);
		}
		public virtual string Format(Country? value, string defaultValue)
		{
			return value == null ? defaultValue : value.Value.ToString();
		}
		public virtual string Format(CountryCodeIso2? value)
		{
			return this.Format(value, string.Empty);
		}
		public virtual string Format(CountryCodeIso2? value, string defaultValue)
		{
			return value == null ? defaultValue : value.Value.ToString();
		}
		public virtual string Format(CountryCodeIso3? value)
		{
			return this.Format(value, string.Empty);
		}
		public virtual string Format(CountryCodeIso3? value, string defaultValue)
		{
			return value == null ? defaultValue : value.Value.ToString();
		}
		public virtual string Format(CountString? value)
		{
			return this.Format(value, string.Empty);
		}
		public virtual string Format(CountString? value, string defaultValue)
		{
			return value == null ? defaultValue : value.Value.ToString();
		}
		public virtual string Format(DescriptionField? value)
		{
			return this.Format(value, string.Empty);
		}
		public virtual string Format(DescriptionField? value, string defaultValue)
		{
			return value == null ? defaultValue : value.Value.ToString();
		}
		public virtual string Format(DependentAges? value)
		{
			return this.Format(value, string.Empty);
		}
		public virtual string Format(DependentAges? value, string defaultValue)
		{
			return value == null ? defaultValue : value.Value.ToString();
		}
		public virtual string Format(EmailAddress? value)
		{
			return this.Format(value, string.Empty);
		}
		public virtual string Format(EmailAddress? value, string defaultValue)
		{
			return value == null ? defaultValue : value.Value.ToString();
		}
		public virtual string Format(EntityName? value)
		{
			return this.Format(value, string.Empty);
		}
		public virtual string Format(EntityName? value, string defaultValue)
		{
			return value == null ? defaultValue : value.Value.ToString();
		}
		public virtual string Format(FhaCode? value)
		{
			return this.Format(value, string.Empty);
		}
		public virtual string Format(FhaCode? value, string defaultValue)
		{
			return value == null ? defaultValue : value.Value.ToString();
		}
		public virtual string Format(FicoCreditScore? value)
		{
			return this.Format(value, string.Empty);
		}
		public virtual string Format(FicoCreditScore? value, string defaultValue)
		{
			return value == null ? defaultValue : value.Value.ToString();
		}
		public virtual string Format(FormattedString? value)
		{
			return this.Format(value, string.Empty);
		}
		public virtual string Format(FormattedString? value, string defaultValue)
		{
			return value == null ? defaultValue : value.Value.ToString();
		}
		public virtual string Format(Instant? value)
		{
			return this.Format(value, string.Empty);
		}
		public virtual string Format(Instant? value, string defaultValue)
		{
			return value == null ? defaultValue : value.Value.ToString();
		}
		public virtual string Format(IRSIdentifier? value)
		{
			return this.Format(value, string.Empty);
		}
		public virtual string Format(IRSIdentifier? value, string defaultValue)
		{
			return value == null ? defaultValue : value.Value.ToString();
		}
		public virtual string Format(LiabilityRemainingMonths? value)
		{
			return this.Format(value, string.Empty);
		}
		public virtual string Format(LiabilityRemainingMonths? value, string defaultValue)
		{
			return value == null ? defaultValue : value.Value.ToString();
		}
		public virtual string Format(LongDescriptionField? value)
		{
			return this.Format(value, string.Empty);
		}
		public virtual string Format(LongDescriptionField? value, string defaultValue)
		{
			return value == null ? defaultValue : value.Value.ToString();
		}
		public virtual string Format(LqbMismoIdentifier? value)
		{
			return this.Format(value, string.Empty);
		}
		public virtual string Format(LqbMismoIdentifier? value, string defaultValue)
		{
			return value == null ? defaultValue : value.Value.ToString();
		}
		public virtual string Format(MilitaryServiceNumber? value)
		{
			return this.Format(value, string.Empty);
		}
		public virtual string Format(MilitaryServiceNumber? value, string defaultValue)
		{
			return value == null ? defaultValue : value.Value.ToString();
		}
		public virtual string Format(Money? value)
		{
			return this.Format(value, string.Empty);
		}
		public virtual string Format(Money? value, string defaultValue)
		{
			return value == null ? defaultValue : value.Value.ToString();
		}
		public virtual string Format(Month? value)
		{
			return this.Format(value, string.Empty);
		}
		public virtual string Format(Month? value, string defaultValue)
		{
			return value == null ? defaultValue : value.Value.ToString();
		}
		public virtual string Format(Name? value)
		{
			return this.Format(value, string.Empty);
		}
		public virtual string Format(Name? value, string defaultValue)
		{
			return value == null ? defaultValue : value.Value.ToString();
		}
		public virtual string Format(NameSuffix? value)
		{
			return this.Format(value, string.Empty);
		}
		public virtual string Format(NameSuffix? value, string defaultValue)
		{
			return value == null ? defaultValue : value.Value.ToString();
		}
		public virtual string Format(OrderRank? value)
		{
			return this.Format(value, string.Empty);
		}
		public virtual string Format(OrderRank? value, string defaultValue)
		{
			return value == null ? defaultValue : value.Value.ToString();
		}
		public virtual string Format(Percentage? value)
		{
			return this.Format(value, string.Empty);
		}
		public virtual string Format(Percentage? value, string defaultValue)
		{
			return value == null ? defaultValue : value.Value.ToString();
		}
		public virtual string Format(Percentile? value)
		{
			return this.Format(value, string.Empty);
		}
		public virtual string Format(Percentile? value, string defaultValue)
		{
			return value == null ? defaultValue : value.Value.ToString();
		}
		public virtual string Format(PersonName? value)
		{
			return this.Format(value, string.Empty);
		}
		public virtual string Format(PersonName? value, string defaultValue)
		{
			return value == null ? defaultValue : value.Value.ToString();
		}
		public virtual string Format(PhoneNumber? value)
		{
			return this.Format(value, string.Empty);
		}
		public virtual string Format(PhoneNumber? value, string defaultValue)
		{
			return value == null ? defaultValue : value.Value.ToString();
		}
		public virtual string Format(PostalCode? value)
		{
			return this.Format(value, string.Empty);
		}
		public virtual string Format(PostalCode? value, string defaultValue)
		{
			return value == null ? defaultValue : value.Value.ToString();
		}
		public virtual string Format<T>(Quantity<T>? value)
			where T : UnitType
		{
			return this.Format<T>(value, string.Empty);
		}
		public virtual string Format<T>(Quantity<T>? value, string defaultValue)
			where T : UnitType
		{
			return value == null ? defaultValue : value.Value.ToString();
		}
		public virtual string Format(SocialSecurityNumber? value)
		{
			return this.Format(value, string.Empty);
		}
		public virtual string Format(SocialSecurityNumber? value, string defaultValue)
		{
			return value == null ? defaultValue : value.Value.ToString();
		}
		public virtual string Format(StreetAddress? value)
		{
			return this.Format(value, string.Empty);
		}
		public virtual string Format(StreetAddress? value, string defaultValue)
		{
			return value == null ? defaultValue : value.Value.ToString();
		}
		public virtual string Format(StreetName? value)
		{
			return this.Format(value, string.Empty);
		}
		public virtual string Format(StreetName? value, string defaultValue)
		{
			return value == null ? defaultValue : value.Value.ToString();
		}
		public virtual string Format(StreetSuffix? value)
		{
			return this.Format(value, string.Empty);
		}
		public virtual string Format(StreetSuffix? value, string defaultValue)
		{
			return value == null ? defaultValue : value.Value.ToString();
		}
		public virtual string Format(TaxFormNumber? value)
		{
			return this.Format(value, string.Empty);
		}
		public virtual string Format(TaxFormNumber? value, string defaultValue)
		{
			return value == null ? defaultValue : value.Value.ToString();
		}
		public virtual string Format(ThirdPartyIdentifier? value)
		{
			return this.Format(value, string.Empty);
		}
		public virtual string Format(ThirdPartyIdentifier? value, string defaultValue)
		{
			return value == null ? defaultValue : value.Value.ToString();
		}
		public virtual string Format(UnitedStatesPostalState? value)
		{
			return this.Format(value, string.Empty);
		}
		public virtual string Format(UnitedStatesPostalState? value, string defaultValue)
		{
			return value == null ? defaultValue : value.Value.ToString();
		}
		public virtual string Format(UnitedStatesState? value)
		{
			return this.Format(value, string.Empty);
		}
		public virtual string Format(UnitedStatesState? value, string defaultValue)
		{
			return value == null ? defaultValue : value.Value.ToString();
		}
		public virtual string Format(UnzonedDate? value)
		{
			return this.Format(value, string.Empty);
		}
		public virtual string Format(UnzonedDate? value, string defaultValue)
		{
			return value == null ? defaultValue : value.Value.ToString();
		}
		public virtual string Format(UspsAddressNumber? value)
		{
			return this.Format(value, string.Empty);
		}
		public virtual string Format(UspsAddressNumber? value, string defaultValue)
		{
			return value == null ? defaultValue : value.Value.ToString();
		}
		public virtual string Format(UspsDirectional? value)
		{
			return this.Format(value, string.Empty);
		}
		public virtual string Format(UspsDirectional? value, string defaultValue)
		{
			return value == null ? defaultValue : value.Value.ToString();
		}
		public virtual string Format(VaDateOfLoan? value)
		{
			return this.Format(value, string.Empty);
		}
		public virtual string Format(VaDateOfLoan? value, string defaultValue)
		{
			return value == null ? defaultValue : value.Value.ToString();
		}
		public virtual string Format(VaEntitlementCode? value)
		{
			return this.Format(value, string.Empty);
		}
		public virtual string Format(VaEntitlementCode? value, string defaultValue)
		{
			return value == null ? defaultValue : value.Value.ToString();
		}
		public virtual string Format(VaLoanNumber? value)
		{
			return this.Format(value, string.Empty);
		}
		public virtual string Format(VaLoanNumber? value, string defaultValue)
		{
			return value == null ? defaultValue : value.Value.ToString();
		}
		public virtual string Format(Year? value)
		{
			return this.Format(value, string.Empty);
		}
		public virtual string Format(Year? value, string defaultValue)
		{
			return value == null ? defaultValue : value.Value.ToString();
		}
		public virtual string Format(YearAsString? value)
		{
			return this.Format(value, string.Empty);
		}
		public virtual string Format(YearAsString? value, string defaultValue)
		{
			return value == null ? defaultValue : value.Value.ToString();
		}
		public virtual string Format(Zipcode? value)
		{
			return this.Format(value, string.Empty);
		}
		public virtual string Format(Zipcode? value, string defaultValue)
		{
			return value == null ? defaultValue : value.Value.ToString();
		}
	}
}
