﻿namespace LqbGrammar.DataTypes
{
    using System.IO;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// A resource that can be associated with a mime-type.
    /// </summary>
    /// <typeparam name="T">The type of the resource.</typeparam>
    public struct TypedResource<T>
    {
        /// <summary>
        /// Gets the name of the typed resource.
        /// It is often desirable, but optional, to specify
        /// a name for a resource.
        /// </summary>
        /// <value>The name of the typed resource.</value>
        public string Name { get; private set; }

        /// <summary>
        /// Gets the MimeType is required data for a typed resource.
        /// </summary>
        /// <value>The mime-type of the typed resource.</value>
        public string MimeType { get; private set; }

        /// <summary>
        /// Gets the data is required for a typed resource.
        /// </summary>
        /// <value>The data for the typed resource.</value>
        public T Data { get; private set; }

        /// <summary>
        /// Validate the mime-type for a string resource and return the validated data.
        /// </summary>
        /// <param name="data">Data that is held.</param>
        /// <param name="mimeType">The type of data that is held.</param>
        /// <param name="name">An optional name for the resource.</param>
        /// <returns>The validated text resource.</returns>
        public static TypedResource<string> CreateText(string data, string mimeType, string name)
        {
            if (string.IsNullOrEmpty(data))
            {
                throw new DeveloperException(ErrorMessage.SystemError);
            }

            if (string.IsNullOrEmpty(mimeType))
            {
                throw new DeveloperException(ErrorMessage.SystemError);
            }

            CheckTextMimeType(mimeType);

            var resource = new TypedResource<string>();
            resource.Data = data;
            resource.MimeType = mimeType;
            if (!string.IsNullOrEmpty(name))
            {
                resource.Name = name;
            }

            return resource;
        }

        /// <summary>
        /// Validate the mime-type for a binary resource and return the validated data.
        /// </summary>
        /// <param name="data">Data that is held.</param>
        /// <param name="mimeType">The type of data that is held.</param>
        /// <param name="name">An optional name for the resource.</param>
        /// <returns>The validated binary resource.</returns>
        public static TypedResource<byte[]> CreateBinary(byte[] data, string mimeType, string name)
        {
            if (data == null)
            {
                throw new DeveloperException(ErrorMessage.SystemError);
            }

            if (data.Length == 0)
            {
                throw new DeveloperException(ErrorMessage.SystemError);
            }

            if (string.IsNullOrEmpty(mimeType))
            {
                throw new DeveloperException(ErrorMessage.SystemError);
            }

            CheckBinaryMimeType(mimeType);

            var resource = new TypedResource<byte[]>();
            resource.Data = data;
            resource.MimeType = mimeType;
            if (!string.IsNullOrEmpty(name))
            {
                resource.Name = name;
            }

            return resource;
        }

        /// <summary>
        /// Validate the mime-type for a resource held in a file and return the validated data.
        /// </summary>
        /// <param name="info">File information for the file that contains the resource.</param>
        /// <param name="mimeType">The type of data that is held.</param>
        /// <param name="name">An optional name for the resource.</param>
        /// <returns>The validated file resource.</returns>
        public static TypedResource<FileInfo> CreateFile(FileInfo info, string mimeType, string name)
        {
            if (info == null)
            {
                throw new DeveloperException(ErrorMessage.SystemError);
            }

            if (string.IsNullOrEmpty(mimeType))
            {
                throw new DeveloperException(ErrorMessage.SystemError);
            }

            var resource = new TypedResource<FileInfo>();
            resource.Data = info;
            resource.MimeType = mimeType;
            if (!string.IsNullOrEmpty(name))
            {
                resource.Name = name;
            }

            return resource;
        }

        /// <summary>
        /// Validate that the mime-type for string data is recognized by our system.
        /// </summary>
        /// <param name="mimeType">The mime-type to be checked.</param>
        internal static void CheckTextMimeType(string mimeType)
        {
            switch (mimeType)
            {
                case System.Net.Mime.MediaTypeNames.Text.Plain:
                case System.Net.Mime.MediaTypeNames.Text.Html:
                case "text/csv":
                    break;
                default:
                    throw new DeveloperException(ErrorMessage.SystemError);
            }
        }

        /// <summary>
        /// Validate that the mime-type for binary data is recognized by our system.
        /// </summary>
        /// <param name="mimeType">The mime-type to be checked.</param>
        internal static void CheckBinaryMimeType(string mimeType)
        {
            switch (mimeType)
            {
                case System.Net.Mime.MediaTypeNames.Application.Pdf:
                case System.Net.Mime.MediaTypeNames.Image.Gif:
                    break;
                default:
                    throw new DeveloperException(ErrorMessage.SystemError);
            }
        }
    }
}
