﻿namespace LqbGrammar.DataTypes
{
    using System;

    /// <summary>
    /// Encapsulate the notion of different protocols for achieving
    /// file transfer operation.
    /// </summary>
    public struct FileTransferProtocol : IEquatable<FileTransferProtocol>
    {
        /// <summary>
        /// FTP method.
        /// </summary>
        public static readonly FileTransferProtocol Ftp = new FileTransferProtocol("FTP");

        /// <summary>
        /// SFTP method.
        /// </summary>
        public static readonly FileTransferProtocol Sftp = new FileTransferProtocol("SFTP");

        /// <summary>
        /// SCP method.
        /// </summary>
        public static readonly FileTransferProtocol Scp = new FileTransferProtocol("SCP");

        /// <summary>
        /// The protocol string value.
        /// </summary>
        private string method;

        /// <summary>
        /// Initializes a new instance of the <see cref="FileTransferProtocol"/> struct.
        /// </summary>
        /// <param name="method">The protocol string value.</param>
        private FileTransferProtocol(string method)
        {
            this.method = method;
        }

        /// <summary>
        /// Compare two objects for equality and that they are instances of the FileTransferProtocol class.
        /// </summary>
        /// <param name="lhs">The object on the left side of the == operator.</param>
        /// <param name="rhs">The object on the right side of the == operator.</param>
        /// <returns>True if both objects are instances of the FileTransferProtocol class and equal to each other.</returns>
        public static new bool Equals(object lhs, object rhs)
        {
            if (!(lhs is FileTransferProtocol))
            {
                return false;
            }

            if (!(rhs is FileTransferProtocol))
            {
                return false;
            }

            return ((FileTransferProtocol)lhs).Equals((FileTransferProtocol)rhs);
        }

        /// <summary>
        /// The equality operator.
        /// </summary>
        /// <param name="lhs">The instance that is on the left side of the == operator.</param>
        /// <param name="rhs">The instance that is on the right side of the == operator.</param>
        /// <returns>True if the instances are equal, false otherwise.</returns>
        public static bool operator ==(FileTransferProtocol lhs, FileTransferProtocol rhs)
        {
            return lhs.Equals(rhs);
        }

        /// <summary>
        /// The inequality operator.
        /// </summary>
        /// <param name="lhs">The instance that is on the left side of the != operator.</param>
        /// <param name="rhs">The instance that is on the right side of the != operator.</param>
        /// <returns>True if the two instances are not equal, false otherwise.</returns>
        public static bool operator !=(FileTransferProtocol lhs, FileTransferProtocol rhs)
        {
            return !lhs.Equals(rhs);
        }

        /// <summary>
        /// Attempts to parse the file transfer protocol from a string. Returns null if the string doesn't match expected methods.
        /// </summary>
        /// <param name="method">The method string to parse as one of the static readonly values of this class.</param>
        /// <returns><see cref="Sftp"/>, <see cref="Ftp"/>, <see cref="Scp"/>, or null.</returns>
        public static FileTransferProtocol? Parse(string method)
        {
            if (StringComparer.OrdinalIgnoreCase.Equals(method, nameof(Sftp)))
            {
                return Sftp;
            }
            else if (StringComparer.OrdinalIgnoreCase.Equals(method, nameof(Ftp)))
            {
                return Ftp;
            }
            else if (StringComparer.OrdinalIgnoreCase.Equals(method, nameof(Scp)))
            {
                return Scp;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Override the Object.ToString() method.
        /// </summary>
        /// <returns>The method string value.</returns>
        public override string ToString()
        {
            return this.method;
        }

        /// <summary>
        /// Evaluate equality.
        /// </summary>
        /// <param name="other">Another instance being compared to this instance.</param>
        /// <returns>True if equals, false otherwise.</returns>
        public bool Equals(FileTransferProtocol other)
        {
            return this.method == other.method;
        }

        /// <summary>
        /// Compare this instance with another object for equality.
        /// </summary>
        /// <param name="obj">The other object.</param>
        /// <returns>True if the other object is an instance of the EmailAddress class and it is equal to this instance.</returns>
        public override bool Equals(object obj)
        {
            if (!(obj is FileTransferProtocol))
            {
                return false;
            }

            return Equals((FileTransferProtocol)obj);
        }

        /// <summary>
        /// Override the implementation inherited from the Object class.
        /// </summary>
        /// <returns>An integer that is the hash of this object.</returns>
        public override int GetHashCode()
        {
            return this.method.GetHashCode();
        }

        /// <summary>
        /// Encapsulate the underlying algorithm used for secure communications.
        /// </summary>
        public struct SecurityMode : IEquatable<SecurityMode>
        {
            /// <summary>
            /// None would normally be used in association with a non-secure protocol.
            /// </summary>
            public static readonly SecurityMode None = new SecurityMode(nameof(None));

            /// <summary>
            /// Implicit permits the networking infrastructure choose its own algorithm.
            /// </summary>
            public static readonly SecurityMode Implicit = new SecurityMode(nameof(Implicit));

            /// <summary>
            /// Force the use of Secure Sockets Layer.
            /// </summary>
            public static readonly SecurityMode ExplicitSsl = new SecurityMode(nameof(ExplicitSsl));

            /// <summary>
            /// Force the use of Transmission Layer Security.
            /// </summary>
            public static readonly SecurityMode ExplicitTls = new SecurityMode(nameof(ExplicitTls));

            /// <summary>
            /// The algorithm's string value.
            /// </summary>
            private string mode;

            /// <summary>
            /// Initializes a new instance of the <see cref="SecurityMode"/> struct.
            /// </summary>
            /// <param name="mode">The protocol string value.</param>
            private SecurityMode(string mode)
            {
                this.mode = mode;
            }

            /// <summary>
            /// Compare two objects for equality and that they are instances of the SecurityMode class.
            /// </summary>
            /// <param name="lhs">The object on the left side of the == operator.</param>
            /// <param name="rhs">The object on the right side of the == operator.</param>
            /// <returns>True if both objects are instances of the SecurityMode class and equal to each other.</returns>
            public static new bool Equals(object lhs, object rhs)
            {
                if (!(lhs is SecurityMode))
                {
                    return false;
                }

                if (!(rhs is SecurityMode))
                {
                    return false;
                }

                return ((SecurityMode)lhs).Equals((SecurityMode)rhs);
            }

            /// <summary>
            /// The equality operator.
            /// </summary>
            /// <param name="lhs">The instance that is on the left side of the == operator.</param>
            /// <param name="rhs">The instance that is on the right side of the == operator.</param>
            /// <returns>True if the instances are equal, false otherwise.</returns>
            public static bool operator ==(SecurityMode lhs, SecurityMode rhs)
            {
                return lhs.Equals(rhs);
            }

            /// <summary>
            /// The inequality operator.
            /// </summary>
            /// <param name="lhs">The instance that is on the left side of the != operator.</param>
            /// <param name="rhs">The instance that is on the right side of the != operator.</param>
            /// <returns>True if the two instances are not equal, false otherwise.</returns>
            public static bool operator !=(SecurityMode lhs, SecurityMode rhs)
            {
                return !lhs.Equals(rhs);
            }

            /// <summary>
            /// Attempts to parse the security mode from a string. Returns null if the string doesn't match expected modes.
            /// </summary>
            /// <param name="mode">The security mode string to parse as one of the static readonly values of this class.</param>
            /// <returns><see cref="None"/>, <see cref="Implicit"/>, <see cref="ExplicitSsl"/>, <see cref="ExplicitTls"/>, or null.</returns>
            public static SecurityMode? Parse(string mode)
            {
                if (StringComparer.OrdinalIgnoreCase.Equals(mode, nameof(None)))
                {
                    return None;
                }
                else if (StringComparer.OrdinalIgnoreCase.Equals(mode, nameof(Implicit)))
                {
                    return Implicit;
                }
                else if (StringComparer.OrdinalIgnoreCase.Equals(mode, nameof(ExplicitSsl)))
                {
                    return ExplicitSsl;
                }
                else if (StringComparer.OrdinalIgnoreCase.Equals(mode, nameof(ExplicitTls)))
                {
                    return ExplicitTls;
                }
                else
                {
                    return null;
                }
            }

            /// <summary>
            /// Override the Object.ToString() method.
            /// </summary>
            /// <returns>The method string value.</returns>
            public override string ToString()
            {
                return this.mode;
            }

            /// <summary>
            /// Evaluate equality.
            /// </summary>
            /// <param name="other">Another instance being compared to this instance.</param>
            /// <returns>True if equals, false otherwise.</returns>
            public bool Equals(SecurityMode other)
            {
                return this.mode == other.mode;
            }

            /// <summary>
            /// Compare this instance with another object for equality.
            /// </summary>
            /// <param name="obj">The other object.</param>
            /// <returns>True if the other object is an instance of the EmailAddress class and it is equal to this instance.</returns>
            public override bool Equals(object obj)
            {
                if (!(obj is SecurityMode))
                {
                    return false;
                }

                return Equals((SecurityMode)obj);
            }

            /// <summary>
            /// Override the implementation inherited from the Object class.
            /// </summary>
            /// <returns>An integer that is the hash of this object.</returns>
            public override int GetHashCode()
            {
                return this.mode.GetHashCode();
            }
        }
    }
}
