﻿namespace LqbGrammar.DataTypes.PathDispatch
{
    /// <summary>
    /// Provides an interface for elements of a data path.
    /// </summary>
    public interface IDataPathElement
    {
        /// <summary>
        /// Gets the name of the data location stored in the data path element.
        /// </summary>
        /// <value>The name of the data location.</value>
        string Name { get; }
    }
}
