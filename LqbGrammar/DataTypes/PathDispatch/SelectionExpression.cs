﻿namespace LqbGrammar.DataTypes.PathDispatch
{
    using System;

    /// <summary>
    /// A marker interface for a type that encapsulates a selection expression for
    /// a <see cref="DataPath"/>.
    /// </summary>
    public interface ISelectionExpression
    {
    }

    /// <summary>
    /// Encapsulates an expression to select an element with a specified
    /// identifier from a collection.
    /// </summary>
    public struct SelectIdExpression : ISelectionExpression, IEquatable<SelectIdExpression>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SelectIdExpression"/> struct.
        /// Generates an expression from an <see cref="IDataObjectIdentifier"/>.
        /// </summary>
        /// <param name="id">The identifier to select.</param>
        public SelectIdExpression(IDataObjectIdentifier id)
            : this(id.ToString())
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SelectIdExpression"/> struct.
        /// Generates an expression from a string.
        /// </summary>
        /// <param name="idString">The identfier as a string.</param>
        public SelectIdExpression(string idString) : this()
        {
            this.IdString = idString;
        }

        /// <summary>
        /// Gets the identifier string.
        /// </summary>
        /// <value>The identfier string.</value>
        public string IdString { get; }

        /// <summary>
        /// Returns a default string representation of this element.
        /// </summary>
        /// <returns>A default string representation of this element.</returns>
        public override string ToString()
        {
            return this.IdString;
        }

        /// <summary>
        /// Implement IEquatable.
        /// </summary>
        /// <param name="other">Instance that is checked for equality with this instance.</param>
        /// <returns>True if other is equal to this instance, false otherwise.</returns>
        public bool Equals(SelectIdExpression other)
        {
            return this.IdString.Equals(other.IdString);
        }

        /// <summary>
        /// Override the Equals method inherited from the object class.
        /// </summary>
        /// <param name="obj">Instance that is checked for equality with this instance.</param>
        /// <returns>True if obj is equal to this instance, false otherwise.</returns>
        public override bool Equals(object obj)
        {
            return (obj is SelectIdExpression) && this.Equals((SelectIdExpression)obj);
        }

        /// <summary>
        /// Override of the GetHashCode method inherited from the object class.
        /// </summary>
        /// <returns>An integer this is the hash code for this instance.</returns>
        public override int GetHashCode()
        {
            return this.IdString.GetHashCode();
        }
    }

    /// <summary>
    /// A null object for specifying that no object of the collection should
    /// be selected.
    /// </summary>
    public struct NoSelectionExpression : ISelectionExpression
    {
    }
}
