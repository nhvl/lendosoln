﻿namespace LqbGrammar.DataTypes.PathDispatch
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using LqbGrammar.Validation;

    /// <summary>
    /// Encapsulates information about a path to data within a nested structure,
    /// which basically amounts to having a list of data path elements.
    /// Traversal of the path is accomplished using simple Head/Tail properties
    /// and recursion.
    /// </summary>
    public class DataPath : IEquatable<DataPath>, IDataObjectIdentifier
    {
        /// <summary>
        /// Header for path strings.
        /// </summary>
        public const string PathHeader = "[PATH]";

        /// <summary>
        /// Marks data path identifier as subset identifier.
        /// </summary>
        public const string SubsetMarker = "SUBSET:";

        /// <summary>
        /// Used in place of an identifier to imply that the entire collection should be searched.
        /// </summary>
        public const string WholeCollectionIdentifier = "ANY";

        /// <summary>
        /// The pattern string used to identify and validate element names within a string representation of a DataPath.
        /// </summary>
        private static string dataPathElementRegexString = "[A-Za-z][A-Za-z0-9_]*";

        /// <summary>
        /// The pattern string used to identify and validate identifiers within a string representation of a DataPath.
        /// </summary>
        private static string dataPathIdentifierRegexString = "[-A-Za-z0-9_]+";

        /// <summary>
        /// The pattern string used to identify and validate collection selectors within a string representation of a DataPath.
        /// </summary>
        private static string dataPathCollectionRegexString = @"^(?<element>" + dataPathElementRegexString + @")\[(?<id>"
            + WholeCollectionIdentifier + "|" + WholeCollectionIdentifier.ToLower() + "|" + "(" + SubsetMarker + "|" + SubsetMarker.ToLower() + ")?"
            + dataPathIdentifierRegexString + @")\]$";

        /// <summary>
        /// The RegularExpressionString used to identify and validate element names within a string representation of a DataPath.
        /// </summary>
        private static RegularExpressionString dataPathElementPattern = RegularExpressionString.Create("^" + dataPathElementRegexString + "$").Value;

        /// <summary>
        /// The RegularExpressionString used to identify and validate collection selectors within a string representation of a DataPath.
        /// </summary>
        private static RegularExpressionString dataPathCollectionPattern = RegularExpressionString.Create(dataPathCollectionRegexString).Value;

        /// <summary>
        /// The path. Bazinga!.
        /// </summary>
        private List<IDataPathElement> path;

        /// <summary>
        /// Initializes a new instance of the <see cref="DataPath"/> class.
        /// </summary>
        /// <param name="path">The list of elements.</param>
        public DataPath(List<IDataPathElement> path)
        {
            this.path = path;
        }

        /// <summary>
        /// Gets an empty DataPath instance.
        /// </summary>
        /// <value>An empty DataPath instance.</value>
        public static DataPath Empty
        {
            get { return new DataPath(new List<IDataPathElement>()); }
        }

        /// <summary>
        /// Gets the length of the path.
        /// </summary>
        /// <value>The length of the path.</value>
        public int Length
        {
            get { return this.path.Count; }
        }

        /// <summary>
        /// Gets the first element of the path.
        /// </summary>
        /// <value>The first element of the path.</value>
        public IDataPathElement Head
        {
            get { return this.path.First(); }
        }

        /// <summary>
        /// Gets the last element of the path.
        /// </summary>
        /// <value>The last element of the path.</value>
        public IDataPathElement Last
        {
            get { return this.path.Last(); }
        }

        /// <summary>
        /// Gets the rest of the path after the Head.
        /// </summary>
        /// <value>The rest of the path the Head.</value>
        public DataPath Tail
        {
            get { return new DataPath(this.path.Skip(1).ToList()); }
        }

        /// <summary>
        /// Gets a read-only version of the path's list.
        /// </summary>
        /// <value>A read-only version of the path's list.</value>
        public IReadOnlyCollection<IDataPathElement> PathList
        {
            get { return this.path.AsReadOnly(); }
        }

        /// <summary>
        /// Attempts to parse a path specified as a string into a DataPath object.
        /// </summary>
        /// <param name="pathString">The string to parse.</param>
        /// <returns>The DataPath parsed from the string.</returns>
        public static DataPath Create(string pathString)
        {
            // Remove PATH header.
            if (pathString.StartsWith(DataPath.PathHeader, StringComparison.OrdinalIgnoreCase))
            {
                pathString = pathString.Substring(DataPath.PathHeader.Length);
            }

            string[] elementStrings = pathString.Split('.');

            List<IDataPathElement> elements = new List<IDataPathElement>();

            foreach (var elementString in elementStrings)
            {
                RegularExpressionResult elementResult = RegularExpressionEngine.Execute(elementString, dataPathElementPattern);

                if (elementResult.IsMatch())
                {
                    elements.Add(new DataPathBasicElement(elementString));
                    continue;
                }

                RegularExpressionResult collectionResult = RegularExpressionEngine.Execute(elementString, dataPathCollectionPattern);

                if (collectionResult.IsMatch())
                {
                    elements.Add(new DataPathCollectionElement(collectionResult.GetGroup("element")));
                    elements.Add(new DataPathSelectionElement(new SelectIdExpression(collectionResult.GetGroup("id"))));
                    continue;
                }

                throw new ArgumentException("Bad path element: '" + elementString + "'");
            }

            return new DataPath(elements);
        }

        /// <summary>
        /// Returns a new DataPath with the given element appended to the end.
        /// </summary>
        /// <param name="element">The element to append.</param>
        /// <returns>A new DataPath with the given element appened to the end.</returns>
        public DataPath Append(IDataPathElement element)
        {
            var result = new List<IDataPathElement>(this.path);
            result.Add(element);
            return new DataPath(result);
        }

        /// <summary>
        /// Returns a new DataPath with the given element prepended to the front.
        /// </summary>
        /// <param name="element">The element to prepend.</param>
        /// <returns>A new DataPath with the given element prepended to the front.</returns>
        public DataPath Prepend(IDataPathElement element)
        {
            List<IDataPathElement> result = new List<IDataPathElement> { element };
            result.AddRange(this.path);
            return new DataPath(result);
        }

        /// <summary>
        /// Returns a default string representation of this element.
        /// </summary>
        /// <returns>A default string representation of this element.</returns>
        public override string ToString()
        {
            StringBuilder result = new StringBuilder(this.path.First().Name);

            foreach (var element in this.path.Skip(1))
            {
                if (!(element is DataPathSelectionElement))
                {
                    result.Append(".");
                }

                result.Append(element.ToString());
            }

            return result.ToString();
        }

        /// <summary>
        /// Implement IEquatable.
        /// </summary>
        /// <param name="other">Instance that is checked for equality with this instance.</param>
        /// <returns>True if other is equal to this instance, false otherwise.</returns>
        public bool Equals(DataPath other)
        {
            if (object.ReferenceEquals(other, null))
            {
                return false;
            }

            if (object.ReferenceEquals(this, other))
            {
                return true;
            }

            return this.path.SequenceEqual(other.path);
        }

        /// <summary>
        /// Override the Equals method inherited from the object class.
        /// </summary>
        /// <param name="obj">Instance that is checked for equality with this instance.</param>
        /// <returns>True if obj is equal to this instance, false otherwise.</returns>
        public override bool Equals(object obj)
        {
            return this.Equals(obj as DataPath);
        }

        /// <summary>
        /// Override of the GetHashCode method inherited from the object class.
        /// </summary>
        /// <returns>An integer this is the hash code for this instance.</returns>
        public override int GetHashCode()
        {
            ////return this.path.GetHashCode();
            return 0; // List<T> does not override object.GetHashCode, and while this object is immutable, it's not currently worth hashing the elements of the list
        }
    }
}
