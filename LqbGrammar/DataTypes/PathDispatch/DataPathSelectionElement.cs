﻿namespace LqbGrammar.DataTypes.PathDispatch
{
    using System;

    /// <summary>
    /// Encapsulates information about the functional equivalent of an indexer or
    /// predicate for a collection whose contents can vary at run time.
    /// </summary>
    public class DataPathSelectionElement : IDataPathElement, IEquatable<DataPathSelectionElement>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DataPathSelectionElement"/> class. 
        /// </summary>
        /// <param name="selectionExp">The selection expression.</param>
        public DataPathSelectionElement(ISelectionExpression selectionExp)
        {
            this.SelectionExpression = selectionExp;
        }

        /// <summary>
        /// Gets the name of the data location stored in the data path element.
        /// </summary>
        /// <value>The name of the data location.</value>
        public string Name
        {
            get { return this.SelectionExpression.ToString(); }
        }

        /// <summary>
        /// Gets the selection expression for this element.
        /// </summary>
        /// <value>The selection expression for this element.</value>
        public ISelectionExpression SelectionExpression { get; }

        /// <summary>
        /// Returns a default string representation of this element.
        /// </summary>
        /// <returns>A default string representation of this element.</returns>
        public override string ToString()
        {
            return "[" + this.Name + "]";
        }

        /// <summary>
        /// Implement IEquatable.
        /// </summary>
        /// <param name="other">Instance that is checked for equality with this instance.</param>
        /// <returns>True if other is equal to this instance, false otherwise.</returns>
        public bool Equals(DataPathSelectionElement other)
        {
            if (object.ReferenceEquals(other, null))
            {
                return false;
            }

            if (object.ReferenceEquals(this, other))
            {
                return true;
            }

            return this.SelectionExpression.Equals(other.SelectionExpression);
        }

        /// <summary>
        /// Override the Equals method inherited from the object class.
        /// </summary>
        /// <param name="obj">Instance that is checked for equality with this instance.</param>
        /// <returns>True if obj is equal to this instance, false otherwise.</returns>
        public override bool Equals(object obj)
        {
            return this.Equals(obj as DataPathSelectionElement);
        }

        /// <summary>
        /// Override of the GetHashCode method inherited from the object class.
        /// </summary>
        /// <returns>An integer this is the hash code for this instance.</returns>
        public override int GetHashCode()
        {
            return this.SelectionExpression.GetHashCode();
        }
    }
}
