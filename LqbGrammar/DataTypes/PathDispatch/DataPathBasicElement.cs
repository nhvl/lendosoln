﻿namespace LqbGrammar.DataTypes.PathDispatch
{
    using System;

    /// <summary>
    /// Encapsulates information about the functional equivalent of a property
    /// of a class, which is a named location that is shared between all instances
    /// of that class.
    /// This is used for non-collection elements, i.e. elements that cannot be
    /// followed by a selection element.
    /// </summary>
    public class DataPathBasicElement : IDataPathElement, IEquatable<DataPathBasicElement>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DataPathBasicElement"/> class. 
        /// </summary>
        /// <param name="name">The name of the path element.</param>
        public DataPathBasicElement(string name)
        {
            this.Name = name;
        }

        /// <summary>
        /// Gets the name of the data location stored in the data path element.
        /// </summary>
        /// <value>The name of the data location.</value>
        public string Name { get; }

        /// <summary>
        /// Returns a default string representation of this element.
        /// </summary>
        /// <returns>A default string representation of this element.</returns>
        public override string ToString()
        {
            return this.Name;
        }

        /// <summary>
        /// Implement IEquatable.
        /// </summary>
        /// <param name="other">Instance that is checked for equality with this instance.</param>
        /// <returns>True if other is equal to this instance, false otherwise.</returns>
        public bool Equals(DataPathBasicElement other)
        {
            if (object.ReferenceEquals(other, null))
            {
                return false;
            }

            if (object.ReferenceEquals(this, other))
            {
                return true;
            }

            return this.Name.Equals(other.Name);
        }

        /// <summary>
        /// Override the Equals method inherited from the object class.
        /// </summary>
        /// <param name="obj">Instance that is checked for equality with this instance.</param>
        /// <returns>True if obj is equal to this instance, false otherwise.</returns>
        public override bool Equals(object obj)
        {
            return this.Equals(obj as DataPathBasicElement);
        }

        /// <summary>
        /// Override of the GetHashCode method inherited from the object class.
        /// </summary>
        /// <returns>An integer this is the hash code for this instance.</returns>
        public override int GetHashCode()
        {
            return this.Name.GetHashCode();
        }
    }
}
