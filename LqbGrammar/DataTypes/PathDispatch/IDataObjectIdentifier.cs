namespace LqbGrammar.DataTypes.PathDispatch
{
    /// <summary>
    /// Provides a marker interface to identify an object as an identifier.
    /// Identifiers should generally be structs that implement a sensible
    /// value-based Equals override, and should also implement IEquatable
    /// for its own type.
    /// </summary>
    public interface IDataObjectIdentifier
    {
    }

    /// <summary>
    /// Provides an interface to identify an object as an identifier.
    /// Includes a type argument to indicate the kind of object that the
    /// identifier identifies and disallow identifiers of different kinds
    /// of things to be interchanged or mixed.
    /// </summary>
    /// <typeparam name="T">The type of object that is being identified.</typeparam>
    public interface IDataObjectIdentifier<T> : IDataObjectIdentifier
    {
    }
}