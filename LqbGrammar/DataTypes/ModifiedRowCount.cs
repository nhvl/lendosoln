﻿namespace LqbGrammar.DataTypes
{
    using System;

    /// <summary>
    /// Wrapper class for the number of rows modified by a SQL command.
    /// </summary>
    public struct ModifiedRowCount : IEquatable<ModifiedRowCount>
    {
        /// <summary>
        /// Invalid value that can be used for initialization.
        /// </summary>
        public static readonly ModifiedRowCount Invalid = new ModifiedRowCount(-1);

        /// <summary>
        /// The integer value.
        /// </summary>
        private int value;

        /// <summary>
        /// Initializes a new instance of the <see cref="ModifiedRowCount"/> struct.
        /// </summary>
        /// <param name="value">The count to be encapsulated.</param>
        private ModifiedRowCount(int value)
        {
            this.value = value;
        }

        /// <summary>
        /// Gets the encapsulated integer.
        /// </summary>
        /// <value>The integer count is returned.</value>
        public int Value
        {
            get { return this.value; }
        }

        /// <summary>
        /// Validate the input count.
        /// </summary>
        /// <param name="value">The count to be validated.</param>
        /// <returns>Validated count, or null.</returns>
        public static ModifiedRowCount? Create(int value)
        {
            if (value >= 0)
            {
                return new ModifiedRowCount(value);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Compare two instances for equality.
        /// </summary>
        /// <param name="lhs">Instance on the left side of the == operator.</param>
        /// <param name="rhs">Instance on the right side of the == operator.</param>
        /// <returns>True if the instances are equal, false otherwise.</returns>
        public static new bool Equals(object lhs, object rhs)
        {
            if (!(lhs is ModifiedRowCount))
            {
                return false;
            }

            if (!(rhs is ModifiedRowCount))
            {
                return false;
            }

            return ((ModifiedRowCount)lhs).Equals((ModifiedRowCount)rhs);
        }

        /// <summary>
        /// Implementation of the == operator.
        /// </summary>
        /// <param name="lhs">Instance on the left side of the == operator.</param>
        /// <param name="rhs">Instance on the right side of the == operator.</param>
        /// <returns>True if the instances are equal, false otherwise.</returns>
        public static bool operator ==(ModifiedRowCount lhs, ModifiedRowCount rhs)
        {
            return lhs.Equals(rhs);
        }

        /// <summary>
        /// Implementation of the != operator.
        /// </summary>
        /// <param name="lhs">Instance on the left side of the != operator.</param>
        /// <param name="rhs">Instance on the right side of the != operator.</param>
        /// <returns>True if the instances are not equal, false otherwise.</returns>
        public static bool operator !=(ModifiedRowCount lhs, ModifiedRowCount rhs)
        {
            return !lhs.Equals(rhs);
        }

        /// <summary>
        /// Override the method inherited from the Object class.
        /// </summary>
        /// <returns>The integer count that is encapsulated.</returns>
        public override string ToString()
        {
            return this.Value.ToString();
        }

        /// <summary>
        /// Implementation of the IEquatable interface.
        /// </summary>
        /// <param name="other">Instance that is compared to this instance for equality.</param>
        /// <returns>True if the instances are equal, false otherwise.</returns>
        public bool Equals(ModifiedRowCount other)
        {
            return this.value == other.value;
        }

        /// <summary>
        /// Override the method inherited from the Object class.
        /// </summary>
        /// <param name="obj">The instance that will be compared with this instance for equality.</param>
        /// <returns>True if the instances are equal, flase otherwise.</returns>
        public override bool Equals(object obj)
        {
            if (!(obj is ModifiedRowCount))
            {
                return false;
            }

            return Equals((ModifiedRowCount)obj);
        }

        /// <summary>
        /// Override of the method inherited from the Object class.
        /// </summary>
        /// <returns>An integer that is the hash of this instance.</returns>
        public override int GetHashCode()
        {
            return this.value.GetHashCode();
        }
    }
}
