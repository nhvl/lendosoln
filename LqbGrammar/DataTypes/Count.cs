﻿namespace LqbGrammar.DataTypes
{
    using System;

    /// <summary>
    /// Encapsulate the concept of a count (possibly negative) of a number of units.
    /// </summary>
    /// <typeparam name="T">The type of the units being counted.</typeparam>
    public struct Count<T> : ISemanticType, IComparable, IComparable<Count<T>>, IEquatable<Count<T>>
        where T : UnitType
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Count{T}"/> struct.
        /// </summary>
        /// <param name="value">The number of units.</param>
        private Count(int value)
        {
            this.Value = value;
        }

        /// <summary>
        /// Gets the number of units as an object.
        /// </summary>
        object ISemanticType.Value => this.Value;

        /// <summary>
        /// Gets the number of units.
        /// </summary>
        /// <value>The number of units.</value>
        public int Value { get; private set; }

        /// <summary>
        /// When a count value is received in text format, this method can be used to recover the count.
        /// </summary>
        /// <param name="value">The count value in text format.</param>
        /// <returns>The true count value, or null if there is a format error.</returns>
        public static Count<T>? Create(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return null;
            }

            int intResult = 0;
            if (!int.TryParse(value, out intResult))
            {
                return null;
            }

            return new Count<T>(intResult);
        }

        /// <summary>
        /// When a count value is received as an integer, this method can be used to recover the count.
        /// </summary>
        /// <param name="value">The count value as an integer.</param>
        /// <returns>The true count value, should never be null but the signature allows it for consistency across all semantic data types.</returns>
        public static Count<T>? Create(int value)
        {
            return (Count<T>)value;
        }

        /// <summary>
        /// Cast an integer to a count.
        /// </summary>
        /// <param name="value">The count value as an integer.</param>
        public static explicit operator Count<T>(int value)
        {
            return new Count<T>(value);
        }

        /// <summary>
        /// Cast a count to an integer.
        /// </summary>
        /// <param name="count">The count to be cast.</param>
        public static explicit operator int(Count<T> count)
        {
            return count.Value;
        }

        /// <summary>
        /// Addition operator for two counts.
        /// </summary>
        /// <param name="lhs">The first operand.</param>
        /// <param name="rhs">The second operand.</param>
        /// <returns>The sum of the two counts.</returns>
        public static Count<T> operator +(Count<T> lhs, Count<T> rhs)
        {
            return new Count<T>(lhs.Value + rhs.Value);
        }

        /// <summary>
        /// Difference operator for two counts.
        /// </summary>
        /// <param name="lhs">The first operand.</param>
        /// <param name="rhs">The second operand.</param>
        /// <returns>The difference between two counts.</returns>
        public static Count<T> operator -(Count<T> lhs, Count<T> rhs)
        {
            return new Count<T>(lhs.Value - rhs.Value);
        }

        /// <summary>
        /// Equality operator for two counts.
        /// </summary>
        /// <param name="lhs">The first operand.</param>
        /// <param name="rhs">The second operand.</param>
        /// <returns>True if the two counts match, false otherwise.</returns>
        public static bool operator ==(Count<T> lhs, Count<T> rhs)
        {
            return lhs.CompareTo(rhs) == 0;
        }

        /// <summary>
        /// The inequality operator for two counts.
        /// </summary>
        /// <param name="lhs">The first operand.</param>
        /// <param name="rhs">The second operand.</param>
        /// <returns>False if the two counts match, true otherwise.</returns>
        public static bool operator !=(Count<T> lhs, Count<T> rhs)
        {
            return lhs.CompareTo(rhs) != 0;
        }

        /// <summary>
        /// The less than operator for two counts.
        /// </summary>
        /// <param name="lhs">The first operand.</param>
        /// <param name="rhs">The second operand.</param>
        /// <returns>True if the inequality operator is satisfied, false otherwise.</returns>
        public static bool operator <(Count<T> lhs, Count<T> rhs)
        {
            return lhs.CompareTo(rhs) < 0;
        }

        /// <summary>
        /// The greater than operator for two counts.
        /// </summary>
        /// <param name="lhs">The first operand.</param>
        /// <param name="rhs">The second operand.</param>
        /// <returns>True if the inequality operator is satisfied, false otherwise.</returns>
        public static bool operator >(Count<T> lhs, Count<T> rhs)
        {
            return lhs.CompareTo(rhs) > 0;
        }

        /// <summary>
        /// The less than or equals operator for two counts.
        /// </summary>
        /// <param name="lhs">The first operand.</param>
        /// <param name="rhs">The second operand.</param>
        /// <returns>True if the inequality operator is satisfied, false otherwise.</returns>
        public static bool operator <=(Count<T> lhs, Count<T> rhs)
        {
            return lhs.CompareTo(rhs) <= 0;
        }

        /// <summary>
        /// The greater than or equals operator for two counts.
        /// </summary>
        /// <param name="lhs">The first operand.</param>
        /// <param name="rhs">The second operand.</param>
        /// <returns>True if the inequality operator is satisfied, false otherwise.</returns>
        public static bool operator >=(Count<T> lhs, Count<T> rhs)
        {
            return lhs.CompareTo(rhs) >= 0;
        }

        /// <summary>
        /// Determine when another count matches this count.
        /// </summary>
        /// <param name="other">The other count.</param>
        /// <returns>True if the two counts match, false otherwise.</returns>
        public bool Equals(Count<T> other)
        {
            return this.CompareTo(other) == 0;
        }

        /// <summary>
        /// If an object is a comparable count, determine whether it matches this count.
        /// </summary>
        /// <param name="other">The other object.</param>
        /// <returns>True if the other object is a comparable count and it matches this count.</returns>
        public override bool Equals(object other)
        {
            return this.CompareTo(other) == 0;
        }

        /// <summary>
        /// If an object is a comparable count, determine the comparison result.
        /// </summary>
        /// <param name="other">The other object.</param>
        /// <returns>The comparison result for the two comparable counts, or throws an ArgumentExeption if the two are not comparable.</returns>
        public int CompareTo(object other)
        {
            if (other is Count<T>)
            {
                return this.CompareTo((Count<T>)other);
            }
            else
            {
                throw new ArgumentException("Input parameter is not a comparable count.");
            }
        }

        /// <summary>
        /// Calculate the comparison result between two counts.
        /// </summary>
        /// <param name="other">The other count which is compared to this count.</param>
        /// <returns>The comparison result.</returns>
        public int CompareTo(Count<T> other)
        {
            return this.Value.CompareTo(other.Value);
        }

        /// <summary>
        /// Calculate a hash value for this count.
        /// </summary>
        /// <returns>A hash value.</returns>
        public override int GetHashCode()
        {
            return this.Value.GetHashCode();
        }

        /// <summary>
        /// Retrieve a string representation of this count.
        /// </summary>
        /// <returns>A string representation of this count.</returns>
        public override string ToString()
        {
            return this.Value.ToString();
        }
    }
}
