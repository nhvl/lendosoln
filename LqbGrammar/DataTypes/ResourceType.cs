﻿namespace LqbGrammar.DataTypes
{
    using System;
    using System.Collections.Generic;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// This struct represents a resource type in the LQB system.  It is intended to 
    /// mimic an enumeration as seen by csharp code, but also deal with encoded 
    /// values that are stored within a database.
    /// <para />
    /// NOTE: If this looks odd it is because it is the result of code generation that deals with
    ///       various complicating factors like obsolete (legacy) values, empty/space as values 
    ///       and even different database types used to encode logical enumerations.
    /// </summary>
    [Serializable]
    public struct ResourceType : System.IEquatable<ResourceType>
    {
        /// <summary>
        /// Enumeration-like value that represents a loan file.
        /// </summary>
        public static readonly ResourceType LoanFile = new ResourceType("LoanFile", "LF");

        /// <summary>
        /// Map from the encoding of an enumeration (within a database) to the enumeration value.
        /// </summary>
        private static Dictionary<string, ResourceType> map;

        /// <summary>
        /// The enumeration value as seen by csharp code.
        /// </summary>
        private string enumValue;

        /// <summary>
        /// The enumeration value as encoded (within a database).
        /// </summary>
        private string enumEncoded;

        /// <summary>
        /// Initializes static members of the <see cref="ResourceType" /> struct.
        /// </summary>
        static ResourceType()
        {
            map = new Dictionary<string, ResourceType>();
            map["LF"] = LoanFile;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ResourceType"/> struct.
        /// </summary>
        /// <param name="enumValue">The enumeration value as seen by csharp code.</param>
        /// <param name="encodedValue">The encoding of the enumeration value which would be written to a database.</param>
        private ResourceType(string enumValue, string encodedValue)
        {
            this.enumValue = enumValue;
            this.enumEncoded = encodedValue;
        }

        /// <summary>
        /// Gets the enumeration value as seen by csharp code.
        /// </summary>
        /// <value>The enumeration value as seen by csharp code.</value>
        internal string EnumValue
        {
            get { return this.enumValue; }
        }

        /// <summary>
        /// Gets the encoded enumeration value which would be written to a database.
        /// </summary>
        /// <value>The encoded enumeration value which would be written to a database.</value>
        internal string EncodedValue
        {
            get { return this.enumEncoded; }
        }

        /// <summary>
        /// Attempt to create an enumeration value from an encoded value.
        /// </summary>
        /// <param name="encValue">The encoded value (from a database).</param>
        /// <returns>The enumeration value, or null if the encoded value isn't recognized.</returns>
        public static ResourceType? Create(string encValue)
        {
            if (map.ContainsKey(encValue))
            {
                return map[encValue];
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// This method mimics the GetValues method of a csharp enumeration.
        /// </summary>
        /// <returns>The list of enumeration values.</returns>
        public static string[] GetValues()
        {
            return new string[] { "LoanFile" };
        }

        /// <summary>
        /// This method mimics the Parse method of a csharp enumeration.
        /// </summary>
        /// <param name="enumValue">The stringified representation of the enumeration value.</param>
        /// <returns>The enumeration value.</returns>
        public static ResourceType Parse(string enumValue)
        {
            switch (enumValue)
            {
                case "LoanFile":
                    return LoanFile;
                default:
                    throw new DeveloperException(ErrorMessage.SystemError);
            }
        }

        /// <summary>
        /// Equality operator for this enumeration.
        /// </summary>
        /// <param name="lhs">The enumeration value on the left of the == operator.</param>
        /// <param name="rhs">The enumeration value on the right of the == operator.</param>
        /// <returns>True if the two enumeration values are equal, false otherwise.</returns>
        public static bool operator ==(ResourceType lhs, ResourceType rhs)
        {
            return (lhs.enumValue == rhs.enumValue) && (lhs.enumEncoded == rhs.enumEncoded);
        }

        /// <summary>
        /// Inequality operator for this enumeration.
        /// </summary>
        /// <param name="lhs">The enumeration value on the left of the != operator.</param>
        /// <param name="rhs">The enumeration value on the right of the != operator.</param>
        /// <returns>True if the two enumeration values are not equal, false otherwise.</returns>
        public static bool operator !=(ResourceType lhs, ResourceType rhs)
        {
            return !(lhs == rhs);
        }

        /// <summary>
        /// Compare this instance with another instance for equality.
        /// </summary>
        /// <param name="other">The other instance.</param>
        /// <returns>True if this instance is equals to the other instance, false otherwise.</returns>
        public bool Equals(ResourceType other)
        {
            return this == other;
        }

        /// <summary>
        /// Override of the Object.Equals method, given explicitely to ensure the correct semantics.
        /// </summary>
        /// <param name="obj">The object that is to be compared with this instance.</param>
        /// <returns>True if the comparison object is equal to this instance.</returns>
        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }

            if (!(obj is ResourceType))
            {
                return false;
            }

            return this == (ResourceType)obj;
        }

        /// <summary>
        /// Utility method used to get the encoded value that would be written into a database.
        /// This method can only return non-obsolete encoded values.
        /// </summary>
        /// <returns>The encoded value for this enumeration value.</returns>
        public string GetTypeValueCurrent()
        {
            return this.GetTypeValueLegacy();
        }

        /// <summary>
        /// Override Object.ToString so it returns what's expected for an enumeration value.
        /// </summary>
        /// <returns>The enumeration value as seen be csharp.</returns>
        public override string ToString()
        {
            return this.enumValue;
        }

        /// <summary>
        /// Override Object.GetHashCode() so the hash is reasonable.
        /// </summary>
        /// <returns>A hash for this enumeration value.</returns>
        public override int GetHashCode()
        {
            string type = this.GetType().ToString();
            return string.Format("{0}.{1}", type, this.enumValue).GetHashCode();
        }

        /// <summary>
        /// Return the encoded version of the enumeration value.
        /// </summary>
        /// <returns>The encoded version of the enumeration value.</returns>
        private string GetTypeValueLegacy()
        {
            return this.EncodedValue;
        }
    }
}
