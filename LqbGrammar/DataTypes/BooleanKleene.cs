﻿namespace LqbGrammar.DataTypes
{
    using System;

    /// <summary>
    /// Kleene 3-valued logic of true, false and indeterminant.
    /// </summary>
    public struct BooleanKleene : ISemanticType, IEquatable<BooleanKleene>
    {
        /// <summary>
        /// Singleton instance representing a TRUE.
        /// </summary>
        public static readonly BooleanKleene True = new BooleanKleene(1);

        /// <summary>
        /// Singleton instance representing a FALSE.
        /// </summary>
        public static readonly BooleanKleene False = new BooleanKleene(2);

        /// <summary>
        /// Singleton instance representing a INDETERMINANT.
        /// </summary>
        public static readonly BooleanKleene Indeterminant = new BooleanKleene(0);

        /// <summary>
        /// Initializes a new instance of the <see cref="BooleanKleene"/> struct.
        /// </summary>
        /// <param name="value">The internal representation of the value.</param>
        private BooleanKleene(byte value)
        {
            this.Value = value;
        }

        /// <summary>
        /// Gets the internal value as an object.
        /// </summary>
        object ISemanticType.Value => this.Value;

        /// <summary>
        /// Gets or sets the internal representation of the value.
        /// </summary>
        private byte Value { get; set; }

        /// <summary>
        /// Attempts to create a BooleanKleene struct from the given value.
        /// </summary>
        /// <param name="value">The value to use.</param>
        /// <returns>The BooleanKleene or null if there was a parse error.</returns>
        public static BooleanKleene? Create(byte value)
        {
            if (value == 0)
            {
                return Indeterminant;
            }
            else if (value == 1)
            {
                return True;
            }
            else if (value == 2)
            {
                return False;
            }

            return null;
        }

        /// <summary>
        /// Attempts to create a BooleanKleene struct from the given value.
        /// </summary>
        /// <param name="value">The value to use.</param>
        /// <returns>The BooleanKleene or null if there was a parse error.</returns>
        public static BooleanKleene? Create(string value)
        {
            if (value == "True")
            {
                return True;
            }
            else if (value == "False")
            {
                return False;
            }
            else if (value == "Indeterminant")
            {
                return Indeterminant;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// The NOT operator.
        /// </summary>
        /// <param name="operand">The operand to which the operator is applied.</param>
        /// <returns>The result of applying the NOT operator.</returns>
        public static BooleanKleene operator !(BooleanKleene operand)
        {
            if (operand == True)
            {
                return False;
            }
            else if (operand == False)
            {
                return True;
            }
            else
            {
                return Indeterminant;
            }
        }

        /// <summary>
        /// The AND operator.
        /// </summary>
        /// <param name="lhs">The operand on the left of the operator.</param>
        /// <param name="rhs">The operand on the right of the operator.</param>
        /// <returns>The result of applying the AND operator to the two operands.</returns>
        public static BooleanKleene operator &(BooleanKleene lhs, BooleanKleene rhs)
        {
            if (lhs == False || rhs == False)
            {
                return False;
            }
            else if (lhs == Indeterminant || rhs == Indeterminant)
            {
                return Indeterminant;
            }
            else
            {
                return True;
            }
        }

        /// <summary>
        /// The OR operator.
        /// </summary>
        /// <param name="lhs">The operand on the left of the operator.</param>
        /// <param name="rhs">The operand on the right of the operator.</param>
        /// <returns>The result of applying the OR operator to the two operands.</returns>
        public static BooleanKleene operator |(BooleanKleene lhs, BooleanKleene rhs)
        {
            if (lhs == True || rhs == True)
            {
                return True;
            }
            else if (lhs == Indeterminant || rhs == Indeterminant)
            {
                return Indeterminant;
            }
            else
            {
                return False;
            }
        }

        /// <summary>
        /// The XOR operator.
        /// </summary>
        /// <param name="lhs">The operand on the left of the operator.</param>
        /// <param name="rhs">The operand on the right of the operator.</param>
        /// <returns>The result of applying the XOR operator to the two operands.</returns>
        public static BooleanKleene operator ^(BooleanKleene lhs, BooleanKleene rhs)
        {
            if (lhs == False && rhs == False)
            {
                return False;
            }
            else if (lhs == True && rhs == True)
            {
                return False;
            }
            else if (lhs != Indeterminant && rhs != Indeterminant)
            {
                return True;
            }
            else
            {
                return Indeterminant;
            }
        }

        /// <summary>
        /// Compare two objects under the requirement that they must both be instances of the BooleanKleene struct.
        /// </summary>
        /// <param name="lhs">First instance under comparison.</param>
        /// <param name="rhs">Second instance under comparison.</param>
        /// <returns>True if the two instances hold equivalent values, false otherwise.</returns>
        public static new bool Equals(object lhs, object rhs)
        {
            if (!(lhs is BooleanKleene))
            {
                return false;
            }

            if (!(rhs is BooleanKleene))
            {
                return false;
            }

            return ((BooleanKleene)lhs).Equals((BooleanKleene)rhs);
        }

        /// <summary>
        /// Equality operator for instances of the BooleanKleene struct.
        /// </summary>
        /// <param name="lhs">First instance under comparison.</param>
        /// <param name="rhs">Second instance under comparison.</param>
        /// <returns>True if the two instances hold equivalent values, false otherwise.</returns>
        public static bool operator ==(BooleanKleene lhs, BooleanKleene rhs)
        {
            return lhs.Equals(rhs);
        }

        /// <summary>
        /// Inequality operator for instances of the BooleanKleene struct.
        /// </summary>
        /// <param name="lhs">First instance under comparison.</param>
        /// <param name="rhs">Second instance under comparison.</param>
        /// <returns>True if the two instances do not hold equivalent values, false otherwise.</returns>
        public static bool operator !=(BooleanKleene lhs, BooleanKleene rhs)
        {
            return !lhs.Equals(rhs);
        }

        /// <summary>
        /// Compare this identifer with another to determine whether they hold equivalent values.
        /// </summary>
        /// <param name="other">The other instance being compared with this instance.</param>
        /// <returns>True if the two instances hold equivalent values, false otherwise.</returns>
        public bool Equals(BooleanKleene other)
        {
            return this.Value == other.Value;
        }

        /// <summary>
        /// Determine whether an object is of the correct type and has the same value as this instance.
        /// </summary>
        /// <param name="obj">The object that is being compared with this instance.</param>
        /// <returns>True if the object matches the type and value of this instance.</returns>
        public override bool Equals(object obj)
        {
            return (obj is BooleanKleene) && this.Equals((BooleanKleene)obj);
        }

        /// <summary>
        /// Calculate a hash code consistent with the equality semantics.
        /// </summary>
        /// <returns>The hash code.</returns>
        public override int GetHashCode()
        {
            return this.Value.GetHashCode();
        }

        /// <summary>
        /// Retrieve a string representation of the value.
        /// </summary>
        /// <returns>A string representation of the value.</returns>
        public override string ToString()
        {
            if (this == True)
            {
                return "True";
            }
            else if (this == False)
            {
                return "False";
            }
            else
            {
                return "Indeterminant";
            }
        }
    }
}
