﻿namespace LqbGrammar.DataTypes
{
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// A two-letter abbreviation representing a United States Postal Service state code.<para/>
    /// It is meant to cover any addressable state, territory, freely associated state, or military mailing code.
    /// </summary>
    /// <remarks>
    /// The abbreviation must be uppercase to be valid.
    /// </remarks>
    public partial struct UnitedStatesPostalState
    {
        /// <summary>
        /// The supported United States Postal Service state codes.
        /// </summary>
        private static readonly HashSet<string> UnitedStatesPostalStateCodes = new HashSet<string>(new[]
        {
            "AA", // Armed Forces Americas
            "AE", // Armed Forces Europe
            "AK", // Alaska
            "AL", // Alabama
            "AP", // Armed Forces Pacific
            "AR", // Arkansas
            "AS", // American Samoa
            "AZ", // Arizona
            "CA", // California
            "CO", // Colorado
            "CT", // Connecticut
            "DC", // District of Columbia
            "DE", // Delaware
            "FL", // Florida
            "FM", // Federated States Of Micronesia
            "GA", // Georgia
            "GU", // Guam
            "HI", // Hawaii
            "IA", // Iowa
            "ID", // Idaho
            "IL", // Illinois
            "IN", // Indiana
            "KS", // Kansas
            "KY", // Kentucky
            "LA", // Louisiana
            "MA", // Massachusetts
            "MD", // Maryland
            "ME", // Maine
            "MH", // Marshall Islands
            "MI", // Michigan
            "MN", // Minnesota
            "MO", // Missouri
            "MP", // Northern Mariana Islands
            "MS", // Mississippi
            "MT", // Montana
            "NC", // North Carolina
            "ND", // North Dakota
            "NE", // Nebraska
            "NH", // New Hampshire
            "NJ", // New Jersey
            "NM", // New Mexico
            "NV", // Nevada
            "NY", // New York
            "OH", // Ohio
            "OK", // Oklahoma
            "OR", // Oregon
            "PA", // Pennsylvania
            "PR", // Puerto Rico
            "PW", // Palau
            "RI", // Rhode Island
            "SC", // South Carolina
            "SD", // South Dakota
            "TN", // Tennessee
            "TX", // Texas
            "UT", // Utah
            "VA", // Virginia
            "VI", // Virgin Islands
            "VT", // Vermont
            "WA", // Washington
            "WI", // Wisconsin
            "WV", // West Virginia
            "WY", // Wyoming
        });

        /// <summary>
        /// Gets a value indicating whether the value provided is a valid state code.
        /// </summary>
        /// <param name="value">The value to validate.</param>
        /// <returns>True if the value represents a valid state code. False otherwise.</returns>
        private static bool PassesAdvancedValidation(string value)
        {
            return UnitedStatesPostalStateCodes.Contains(value);
        }
    }
}
