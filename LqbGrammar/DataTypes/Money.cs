﻿namespace LqbGrammar.DataTypes
{
    using System;

    /// <summary>
    /// Represents a monetary value in currency of the United States.
    /// </summary>
    /// <remarks>
    /// The contained value is always rounded to dollars and cents.  Fractional cents are disallowed.
    /// </remarks>
    public struct Money : ISemanticType, IComparable, IComparable<Money>, IEquatable<Money>
    {
        /// <summary>
        /// Constant value representing zero.
        /// </summary>
        public static readonly Money Zero = new Money(0.00m, true);

        /// <summary>
        /// Constant value representing one dollar.
        /// </summary>
        public static readonly Money OneDollar = new Money(1.00m, true);

        /// <summary>
        /// Constant value representing one cent.
        /// </summary>
        public static readonly Money OneCent = new Money(0.01m, true);

        /// <summary>
        /// Initializes a new instance of the <see cref="Money"/> struct.
        /// </summary>
        /// <param name="value">The value that is encapsulated.</param>
        /// <param name="alreadyRounded">True if the input value is guaranteed to already be rounded, false otherwise.</param>
        private Money(decimal value, bool alreadyRounded)
        {
            if (!alreadyRounded)
            {
                value = Round(value);
            }

            this.InternalValue = Quantity<UnitType.Dollar>.Create(value).Value;
        }

        /// <summary>
        /// Gets the underlying value.
        /// </summary>
        public decimal Value
        {
            get
            {
                return (decimal)this.InternalValue;
            }
        }

        /// <summary>
        /// Gets the encapsulated value as an object.
        /// </summary>
        object ISemanticType.Value => this.Value;

        /// <summary>
        /// Gets or sets the value in its internal representation.
        /// </summary>
        private Quantity<UnitType.Dollar> InternalValue { get; set; }

        /// <summary>
        /// Create an instance of Money from a decimal.
        /// </summary>
        /// <param name="value">The decimal value.</param>
        /// <returns>The instance of the Money type.</returns>
        public static Money? Create(decimal value)
        {
            return new Money(value, false);
        }

        /// <summary>
        /// Create an instance of Money from a string.
        /// </summary>
        /// <param name="value">The string value.</param>
        /// <returns>The instance of the Money type, or null.</returns>
        public static Money? Create(string value)
        {
            decimal test;
            bool ok = decimal.TryParse(value, System.Globalization.NumberStyles.Currency, System.Globalization.NumberFormatInfo.CurrentInfo, out test);
            if (ok)
            {
                return Create(test);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Implicit cast of a decimal value to a monetary value.
        /// </summary>
        /// <param name="amount">The decimal value that is to be converted to Money.</param>
        public static implicit operator Money(decimal amount)
        {
            return new Money(amount, false);
        }

        /// <summary>
        /// Implicit cast of a Quantity&lt;UnitType.Dollar&gt; value to a monetary value.
        /// </summary>
        /// <remarks>
        /// The Quantity&lt;UnitType.Dollar&gt; will be rounded to dollars and cents.
        /// </remarks>
        /// <param name="amount">The decimal value that is to be converted to Money.</param>
        public static implicit operator Money(Quantity<UnitType.Dollar> amount)
        {
            return new Money((decimal)amount, false);
        }

        /// <summary>
        /// Explicit cast of a Money value to a Quantity&lt;UnitType.Dollar&gt; value.
        /// </summary>
        /// <param name="amount">The Money value.</param>
        public static explicit operator Quantity<UnitType.Dollar>(Money amount)
        {
            return amount.InternalValue;
        }

        /// <summary>
        /// Explicit cast of a Money value to a decimal.
        /// </summary>
        /// <param name="amount">The Money value.</param>
        public static explicit operator decimal(Money amount)
        {
            return (decimal)amount.Value;
        }

        /// <summary>
        /// Addition operator that keeps the full accuracy.
        /// </summary>
        /// <param name="mv1">First operand in the addition.</param>
        /// <param name="mv2">Second operand in the addition.</param>
        /// <returns>The sum of the two operands.</returns>
        public static Money operator +(Money mv1, Money mv2)
        {
            return new Money((decimal)(mv1.InternalValue + mv2.InternalValue), true);
        }

        /// <summary>
        /// Subtraction operator that keeps the full accuracy.
        /// </summary>
        /// <param name="mv1">First operand in the subtraction.</param>
        /// <param name="mv2">Second operand in the subtraction.</param>
        /// <returns>The difference of the two operands.</returns>
        public static Money operator -(Money mv1, Money mv2)
        {
            return new Money((decimal)(mv1.InternalValue - mv2.InternalValue), true);
        }

        /// <summary>
        /// Unitary operator that is occasionally used to emphasize that negation is not being done, implemented here for syntactic completeness.
        /// </summary>
        /// <param name="mv">The operand.</param>
        /// <returns>A copy of the operand, unchanged.</returns>
        public static Money operator +(Money mv)
        {
            return mv;
        }

        /// <summary>
        /// Unitary negation operator that changes the sign of the amount.
        /// </summary>
        /// <param name="mv">The operand which will have its value negated.</param>
        /// <returns>The negated value.</returns>
        public static Money operator -(Money mv)
        {
            decimal neg = -(decimal)mv;
            return new Money(neg, true);
        }

        /// <summary>
        /// Equality operator, evaluates true if the values are equal.
        /// </summary>
        /// <param name="mv1">The first operand.</param>
        /// <param name="mv2">The second operand.</param>
        /// <returns>True if the two operands have the same values, false otherwise.</returns>
        public static bool operator ==(Money mv1, Money mv2)
        {
            return mv1.Equals(mv2);
        }

        /// <summary>
        /// Inequality operatory, evaluates true if the values are not equal.
        /// </summary>
        /// <param name="mv1">The first operand.</param>
        /// <param name="mv2">The second operand.</param>
        /// <returns>True if the two operands do not have the same values, false otherwise.</returns>
        public static bool operator !=(Money mv1, Money mv2)
        {
            return !mv1.Equals(mv2);
        }

        /// <summary>
        /// Less than operator, evaluates to true if the values satisfy the semantics.
        /// </summary>
        /// <param name="mv1">The first operand.</param>
        /// <param name="mv2">The second operand.</param>
        /// <returns>True if the values satisfy the semantics, false otherwise.</returns>
        public static bool operator <(Money mv1, Money mv2)
        {
            return mv1.CompareTo(mv2) < 0;
        }

        /// <summary>
        /// Less than or equals operator, evaluates to true if the values satisfy the semantics.
        /// </summary>
        /// <param name="mv1">The first operand.</param>
        /// <param name="mv2">The second operand.</param>
        /// <returns>True if the values satisfy the semantics, false otherwise.</returns>
        public static bool operator <=(Money mv1, Money mv2)
        {
            return mv1.CompareTo(mv2) <= 0;
        }

        /// <summary>
        /// Greater than operator, evaluates to true if the values satisfy the semantics.
        /// </summary>
        /// <param name="mv1">The first operand.</param>
        /// <param name="mv2">The second operand.</param>
        /// <returns>True if the values satisfy the semantics, false otherwise.</returns>
        public static bool operator >(Money mv1, Money mv2)
        {
            return mv1.CompareTo(mv2) > 0;
        }

        /// <summary>
        /// Greater than or equals operator, evaluates to true if the values satisfy the semantics.
        /// </summary>
        /// <param name="mv1">The first operand.</param>
        /// <param name="mv2">The second operand.</param>
        /// <returns>True if the values satisfy the semantics, false otherwise.</returns>
        public static bool operator >=(Money mv1, Money mv2)
        {
            return mv1.CompareTo(mv2) >= 0;
        }

        /// <summary>
        /// Compare this Money instance to an arbitrary object.
        /// </summary>
        /// <param name="obj">The object under comparison.</param>
        /// <returns>The result of the comparison.</returns>
        public int CompareTo(object obj)
        {
            if (object.ReferenceEquals(obj, null))
            {
                throw new ArgumentNullException("obj");
            }

            if (obj is Money)
            {
                var money = (Money)obj;
                return this.CompareTo(money);
            }
            else if (obj is Quantity<UnitType.Dollar>)
            {
                var dollar = (Quantity<UnitType.Dollar>)obj;
                return this.InternalValue.CompareTo(dollar);
            }
            else if (obj is decimal)
            {
                decimal thisValue = (decimal)this;
                decimal thatValue = (decimal)obj;
                return thisValue.CompareTo(thatValue);
            }

            decimal converted = Convert.ToDecimal(obj); // allow the exception to escape if the conversion fails
            decimal value = (decimal)this;
            return value.CompareTo(converted);
        }

        /// <summary>
        /// Compare this Money instance with another Money instance.
        /// </summary>
        /// <param name="other">The Money instance under comparison.</param>
        /// <returns>The result of the comparison.</returns>
        public int CompareTo(Money other)
        {
            return this.InternalValue.CompareTo(other.InternalValue);
        }

        /// <summary>
        /// Override the Equals method inherited from System.Object.
        /// </summary>
        /// <param name="obj">Object under comparison.</param>
        /// <returns>True if this value matches the object under comparison, false otherwise.</returns>
        public override bool Equals(object obj)
        {
            if (object.ReferenceEquals(obj, null))
            {
                return false;
            }

            if (obj is Money)
            {
                var money = (Money)obj;
                return this.CompareTo(money) == 0;
            }
            else if (obj is Quantity<UnitType.Dollar>)
            {
                var dollar = (Quantity<UnitType.Dollar>)obj;
                return this.InternalValue.CompareTo(dollar) == 0;
            }
            else if (obj is decimal)
            {
                decimal thisValue = (decimal)this;
                decimal thatValue = (decimal)obj;
                return thisValue.CompareTo(thatValue) == 0;
            }

            try
            {
                decimal converted = Convert.ToDecimal(obj);
                decimal value = (decimal)this;
                return value.CompareTo(converted) == 0;
            }
            catch (FormatException)
            {
                return false;
            }
            catch (InvalidCastException)
            {
                return false;
            }
            catch (OverflowException)
            {
                return false;
            }
        }

        /// <summary>
        /// Determine whether this Money instance holds the same value as another Money instance.
        /// </summary>
        /// <param name="other">The other Money instance.</param>
        /// <returns>True if the two instances hold the same value, false otherwise.</returns>
        public bool Equals(Money other)
        {
            return this.CompareTo(other) == 0;
        }

        /// <summary>
        /// Calculate a hash for this instance's value for consistency with the equality semantics.
        /// </summary>
        /// <returns>A hash value for this instance.</returns>
        public override int GetHashCode()
        {
            return this.InternalValue.GetHashCode();
        }

        /// <summary>
        /// Return the default string representation of the value.
        /// </summary>
        /// <returns>The default string representation of the value.</returns>
        public override string ToString()
        {
            decimal value = (decimal)this;
            return value.ToString();
        }

        /// <summary>
        /// Return a string representation based on the input format specifier, which accepts the same specifiers as the decimal type.
        /// </summary>
        /// <param name="format">Format specifier, which accepts the same specifiers as the decimal type.</param>
        /// <returns>A string representation based on the input format specifier.</returns>
        public string ToString(string format)
        {
            decimal value = (decimal)this;
            return value.ToString(format);
        }

        /// <summary>
        /// Return a string representation based on the input format provider, which accepts the same providers as the decimal type.
        /// </summary>
        /// <param name="provider">Format provider, which accepts the same providers as the decimal type.</param>
        /// <returns>A string representation based on the input format provider.</returns>
        public string ToString(IFormatProvider provider)
        {
            decimal value = (decimal)this;
            return value.ToString(provider);
        }

        /// <summary>
        /// Return a string representation based on the input format specifier and provider, which accepts the same values as the decimal type.
        /// </summary>
        /// <param name="format">Format specifier, which accepts the same specifiers as the decimal type.</param>
        /// <param name="provider">Format provider, which accepts the same providers as the decimal type.</param>
        /// <returns>A string representation based on the input format specifier and provider.</returns>
        public string ToString(string format, IFormatProvider provider)
        {
            decimal value = (decimal)this;
            return value.ToString(format, provider);
        }

        /// <summary>
        /// Implement the rounding algorithm, using commercial rounding (AwayFromZero), not banker's rounding (ToEven).
        /// </summary>
        /// <param name="unrounded">The decimal value that requires rounding.</param>
        /// <returns>The rounded value.</returns>
        private static decimal Round(decimal unrounded)
        {
            return decimal.Round(unrounded, 2, MidpointRounding.AwayFromZero);
        }
    }
}
