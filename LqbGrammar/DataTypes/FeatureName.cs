﻿namespace LqbGrammar.DataTypes
{
    using LqbGrammar.Exceptions;
    using LqbGrammar.Validation;

    /// <summary>
    /// Encapsulate and validate the name of a feature.
    /// </summary>
    public struct FeatureName : System.IEquatable<FeatureName>
    {
        /// <summary>
        /// An instance of an invalid value that can be used for variable initialization.
        /// </summary>
        public static readonly FeatureName BadName = new FeatureName(string.Empty);

        /// <summary>
        /// The name of the feature.
        /// </summary>
        private string name;

        /// <summary>
        /// Initializes a new instance of the <see cref="FeatureName"/> struct.
        /// </summary>
        /// <param name="name">The feature name.</param>
        private FeatureName(string name)
        {
            this.name = name;
        }

        /// <summary>
        /// Validate the input feature name.
        /// </summary>
        /// <param name="name">The input feature name to be validated.</param>
        /// <returns>A validated feature name, or null.</returns>
        public static FeatureName? Create(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                return null;
            }

            RegularExpressionResult result = RegularExpressionEngine.Execute(name, RegularExpressionString.FeatureName);
            if (!result.IsMatch())
            {
                return null;
            }

            return new FeatureName(name);
        }

        /// <summary>
        /// Compare two objects for equality and that they are both validated feature names.
        /// </summary>
        /// <param name="lhs">Object on the left side of the == operator.</param>
        /// <param name="rhs">Object on the right side of the == operator.</param>
        /// <returns>True if both objects are instances of the FeatureName class, and are equal to each other.</returns>
        public static new bool Equals(object lhs, object rhs)
        {
            if (!(lhs is FeatureName))
            {
                return false;
            }

            if (!(rhs is FeatureName))
            {
                return false;
            }

            return ((FeatureName)lhs).Equals((FeatureName)rhs);
        }

        /// <summary>
        /// The equality operator for comparing two instances of a valid feature name.
        /// </summary>
        /// <param name="lhs">The instance on the left side of the == operator.</param>
        /// <param name="rhs">The instance on the right side of the == operator.</param>
        /// <returns>True if the two instances have the same name.</returns>
        public static bool operator ==(FeatureName lhs, FeatureName rhs)
        {
            return lhs.Equals(rhs);
        }

        /// <summary>
        /// The inequality operator for comparing two instances of a valid feature name.
        /// </summary>
        /// <param name="lhs">The instance on the left side of the != operator.</param>
        /// <param name="rhs">The instance on the right side of the != operator.</param>
        /// <returns>True if the two instances are not equal to each other.</returns>
        public static bool operator !=(FeatureName lhs, FeatureName rhs)
        {
            return !lhs.Equals(rhs);
        }

        /// <summary>
        /// Override the implementation inherited from the Object class.
        /// </summary>
        /// <returns>The feature name.</returns>
        public override string ToString()
        {
            return this.name;
        }

        /// <summary>
        /// Test this instance against another for equality.
        /// </summary>
        /// <param name="other">The other instance.</param>
        /// <returns>True if the two instances are equal, false otherwise.</returns>
        public bool Equals(FeatureName other)
        {
            return this.name == other.name;
        }

        /// <summary>
        /// Test this instance against another object for equality.
        /// </summary>
        /// <param name="obj">The other object.</param>
        /// <returns>True if the other object is a validated feature name and the names are equal.</returns>
        public override bool Equals(object obj)
        {
            if (!(obj is FeatureName))
            {
                return false;
            }

            return Equals((FeatureName)obj);
        }

        /// <summary>
        /// Override the implementation inherited from the Object class.
        /// </summary>
        /// <returns>An integer that is the hash of this instance.</returns>
        public override int GetHashCode()
        {
            return this.name.GetHashCode();
        }
    }
}
