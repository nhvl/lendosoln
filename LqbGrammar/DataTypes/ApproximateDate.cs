﻿namespace LqbGrammar.DataTypes
{
    using System;
    using System.Text.RegularExpressions;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Encapsulate the concept of an approximate date, which can be either a year;
    /// a year and a month; or a year, month, and day, depending on how precise the
    /// source (often the borrower) is.
    /// </summary>
    public struct ApproximateDate : ISemanticType
    {
        /// <summary>
        /// Identifier for day precision.
        /// </summary>
        private const string DayPrecisionIdentifier = "D";

        /// <summary>
        /// Identifier for month precision.
        /// </summary>
        private const string MonthPrecisionIdentifier = "M";

        /// <summary>
        /// Identifer for year precision.
        /// </summary>
        private const string YearPrecisionIdentifier = "Y";

        /// <summary>
        /// The date that whose components capture our approximate date data.
        /// </summary>
        private UnzonedDate innerDate;

        /// <summary>
        /// The precision of the approximate date.
        /// </summary>
        private ApproximateDatePrecision precision;

        /// <summary>
        /// Initializes a new instance of the <see cref="ApproximateDate"/> struct.
        /// </summary>
        /// <param name="innerDate">The inner date.</param>
        /// <param name="precision">The precision.</param>
        private ApproximateDate(UnzonedDate innerDate, ApproximateDatePrecision precision)
        {
            this.innerDate = innerDate;
            this.precision = precision;
        }

        /// <summary>
        /// Gets the underlying value, which is used to populate the database.
        /// </summary>
        public object Value => this.ToString("D");

        /// <summary>
        /// Create an instance of ApproximateDate from a stored value.
        /// </summary>
        /// <remarks>
        /// The default implementation for reading from the database uses this CreateFromDatabaseValue() method.
        /// Accordingly, it will assume the format as generated using ToString("D").
        /// </remarks>
        /// <param name="value">A string representation of the date.</param>
        /// <returns>An instance of ApproximateDate.</returns>
        public static ApproximateDate? CreateFromDatabaseValue(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return null;
            }

            string[] pieces = value.Split('|');
            if (pieces.Length != 2)
            {
                return null;
            }

            var unzoned = UnzonedDate.Create(pieces[0]);
            if (unzoned == null)
            {
                return null;
            }

            var date = unzoned.Value.Date;

            if (pieces[1] == DayPrecisionIdentifier)
            {
                return ApproximateDate.CreateWithDayPrecision(date.Year, date.Month, date.Day).Value;
            }
            else if (pieces[1] == MonthPrecisionIdentifier)
            {
                return ApproximateDate.CreateWithMonthPrecision(date.Year, date.Month).Value;
            }
            else if (pieces[1] == YearPrecisionIdentifier)
            {
                return ApproximateDate.CreateWithYearPrecision(date.Year).Value;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Create an instance of ApproximateDate from a user friendly string (year | month/year | month/day/year).
        /// </summary>
        /// <param name="strValue">A user friendly string representation of the date.</param>
        /// <returns>An instance of ApproximateDate.</returns>
        /// <remarks>This is used by the generated <c>ParseFromStringBase</c> implementation, so it should match the output of <see cref="ToString()"/>.</remarks>
        public static ApproximateDate? Create(string strValue)
        {
            Regex userInputRegex = new Regex(@"^(?:(?<month>\d{1,2})[-/.](?:(?<day>\d{1,2})[-/.])?)?(?<year>\d{4}|\d{2})$");
            Match matchResult = userInputRegex.Match(strValue);

            if (!matchResult.Success)
            {
                return null;
            }

            string dayStr = matchResult.Groups["day"].Value;
            string monthStr = matchResult.Groups["month"].Value;
            string yearStr = matchResult.Groups["year"].Value;

            int dd = string.IsNullOrEmpty(dayStr) ? 0 : int.Parse(dayStr);
            int mm = string.IsNullOrEmpty(monthStr) ? 0 : int.Parse(monthStr);
            int yyyy = int.Parse(yearStr);

            if (yearStr.Length <= 2)
            {
                if (yyyy < 30)
                {
                    yyyy += 2000;
                }
                else
                {
                    yyyy += 1900;
                }
            }

            return Create(yyyy, mm, dd);
        }

        /// <summary>
        /// Creates an approximate date with year precision, or no date at all.
        /// </summary>
        /// <param name="year">The year value.</param>
        /// <returns>An approximate date that represents a date sometime in the given year, or null if the inputs are invalid.</returns>
        public static ApproximateDate? CreateWithYearPrecision(int year)
        {
            return Create(year, 0, 0);
        }

        /// <summary>
        /// Creates an approximate date with month precision, or no date at all.
        /// </summary>
        /// <param name="year">The year value.</param>
        /// <param name="month">The month value.</param>
        /// <returns>An approximate date that represents a date sometime in the given year and month, or null if the inputs are invalid.</returns>
        public static ApproximateDate? CreateWithMonthPrecision(int year, int month)
        {
            if (month == 0)
            {
                return null;
            }

            return Create(year, month, 0);
        }

        /// <summary>
        /// Creates an approximate date with day precision (i.e. an exact date), or no date at all.
        /// </summary>
        /// <param name="year">The year value.</param>
        /// <param name="month">The month value.</param>
        /// <param name="day">The day value.</param>
        /// <returns>An approximate date that represents an exact date, or null if the inputs do not represent a valid date.</returns>
        public static ApproximateDate? CreateWithDayPrecision(int year, int month, int day)
        {
            if (month == 0 || day == 0)
            {
                return null;
            }

            return Create(year, month, day);
        }

        /// <summary>
        /// Creates an approximate date with year, month, or day precision, depending on the inputs.
        /// </summary>
        /// <param name="year">The year value.</param>
        /// <param name="month">The month value, or 0 if the month is not known.</param>
        /// <param name="day">The day value, or 0 if the day is not known.</param>
        /// <returns>The specified approximate date, or null if the input would represent an invalid date.</returns>
        public static ApproximateDate? Create(int year, int month, int day)
        {
            ApproximateDatePrecision precision;

            if (day == 0 && month == 0)
            {
                precision = ApproximateDatePrecision.Year.Instance;
                month = 1;
                day = 1;
            }
            else if (day == 0)
            {
                precision = ApproximateDatePrecision.Month.Instance;
                day = 1;
            }
            else
            {
                precision = ApproximateDatePrecision.Day.Instance;
            }

            var innerDate = UnzonedDate.Create(year, month, day);

            if (innerDate.HasValue)
            {
                return new ApproximateDate(innerDate.Value, precision);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Determines if any dates that could be represented in the starting approximate date
        /// are the specified number of months away from some date that could be represented in
        /// the ending approximate date.
        /// </summary>
        /// <param name="periodStart">The starting approximate date.</param>
        /// <param name="months">The span of months.</param>
        /// <param name="periodEnd">The ending approximate date.</param>
        /// <returns>True if there exists a date in the starting approximate date range that is exactly the given number of months before some date in the ending approximate date range.</returns>
        public static bool IsConsistentTimeSpan(ApproximateDate periodStart, Count<UnitType.Month> months, ApproximateDate periodEnd)
        {
            // If they are both day precision but the span is months, check to see if the month span is "close enough".
            if (ReferenceEquals(periodStart.precision, ApproximateDatePrecision.Day.Instance) && ReferenceEquals(periodEnd.precision, ApproximateDatePrecision.Day.Instance))
            {
                int daysFromSpanEnd = periodStart.innerDate.Date.AddMonths(months.Value).Subtract(periodEnd.innerDate.Date).Days;
                return Math.Abs(daysFromSpanEnd) <= 16; // Is it within about half a month of the indicated span (erring on the side of permissiveness)?
            }

            DateTime minEndFromStart = periodStart.MinimumDate().Date.AddMonths(months.Value);
            DateTime maxEndFromStart = periodStart.MaximumDate().Date.AddMonths(months.Value);
            DateTime minEnd = periodEnd.MinimumDate().Date;
            DateTime maxEnd = periodEnd.MaximumDate().Date;

            // Test for interval overlap.
            return minEnd <= maxEndFromStart && minEndFromStart <= maxEnd;
        }

        /// <summary>
        /// Get the earliest date that could be represented by the approximate date.
        /// </summary>
        /// <returns>The earliest date that could be represented by the approximate date.</returns>
        public UnzonedDate MinimumDate()
        {
            return this.innerDate;
        }

        /// <summary>
        /// Get the date in the middle (by approximate calendar reckoning) of the range that could be represented by the approximate date.
        /// </summary>
        /// <returns>The date in the middle (by approximate calendar reckoning) of the range that could be represented by the approximate date.</returns>
        public UnzonedDate MiddleDate()
        {
            return this.precision.MiddleOfPeriod(this.innerDate).Value;
        }

        /// <summary>
        /// Get the latest date that could be represented by the approximate date.
        /// </summary>
        /// <returns>The latest date that could be represented by the approximate date.</returns>
        public UnzonedDate MaximumDate()
        {
            return this.precision.EndOfPeriod(this.innerDate).Value;
        }

        /// <summary>
        /// Override the implementation inherited from System.Object.
        /// </summary>
        /// <returns>A user-friendly string representation of this instance.</returns>
        public override string ToString()
        {
            if (this.precision is ApproximateDatePrecision.Year)
            {
                return this.innerDate.Date.Year.ToString();
            }
            else if (this.precision is ApproximateDatePrecision.Month)
            {
                var date = this.innerDate.Date;
                return $"{date.Month.ToString()}/{date.Year.ToString()}";
            }
            else
            {
                // must be ApproximateDatePrecision.Day
                var date = this.innerDate.Date;
                return $"{date.Month.ToString()}/{date.Day.ToString()}/{date.Year.ToString()}";
            }
        }

        /// <summary>
        /// Generate a string representation of the approximate date based upon a format code.
        /// </summary>
        /// <remarks>
        /// If the format is 'D' this method returns the database format.  If F, null or empty it returns
        /// the same friendly format as ToString().  Otherwise an exception is thrown.
        /// </remarks>
        /// <param name="format">Format code to determine how the approximate date is generated.</param>
        /// <returns>A string represenation of the approximate date.</returns>
        public string ToString(string format)
        {
            if (string.IsNullOrEmpty(format) || format.ToUpper() == "F")
            {
                return this.ToString();
            }
            else if (format.ToUpper() == "D")
            {
                string date = this.innerDate.ToString();

                string identifier = DayPrecisionIdentifier;
                if (this.precision is ApproximateDatePrecision.Month)
                {
                    identifier = MonthPrecisionIdentifier;
                }
                else if (this.precision is ApproximateDatePrecision.Year)
                {
                    identifier = YearPrecisionIdentifier;
                }

                return $"{date}|{identifier}";
            }
            else
            {
                throw new DeveloperException(ErrorMessage.SystemError);
            }
        }
    }

    /// <summary>
    /// Encapsulate the concept of the precision of an approximate date.
    /// </summary>
    public abstract class ApproximateDatePrecision
    {
        /// <summary>
        /// Computes the end of the period from the start of the period.
        /// </summary>
        /// <param name="periodStartDate">The period start date.</param>
        /// <returns>The date of the end of the period.</returns>
        public abstract UnzonedDate? EndOfPeriod(UnzonedDate periodStartDate);

        /// <summary>
        /// Computes the middle of the period from the start of the period.
        /// </summary>
        /// <param name="periodStartDate">The period start date.</param>
        /// <returns>The date of the middle of the period.</returns>
        public abstract UnzonedDate? MiddleOfPeriod(UnzonedDate periodStartDate);

        /// <summary>
        /// Encapsulate the concept of an approximate date precise to the day.
        /// </summary>
        public sealed class Day : ApproximateDatePrecision
        {
            /// <summary>
            /// The field to store the singleton.
            /// </summary>
            private static readonly Lazy<Day> Singleton = new Lazy<Day>(() => new Day());

            /// <summary>
            /// Prevents a default instance of the <see cref="Day"/> class from being created.
            /// </summary>
            private Day()
            {
            }

            /// <summary>
            /// Gets the value of the Instance singleton.
            /// </summary>
            /// <remarks>Singleton pattern being used to ensure reference equality for tests.</remarks>
            public static Day Instance
            {
                get { return Singleton.Value; }
            }

            /// <summary>
            /// Computes the end of the period from the start of the period.
            /// </summary>
            /// <param name="periodStartDate">The period start date.</param>
            /// <returns>The date of the end of the period.</returns>
            public override UnzonedDate? EndOfPeriod(UnzonedDate periodStartDate)
            {
                return periodStartDate;
            }

            /// <summary>
            /// Computes the middle of the period from the start of the period.
            /// </summary>
            /// <param name="periodStartDate">The period start date.</param>
            /// <returns>The date of the middle of the period.</returns>
            public override UnzonedDate? MiddleOfPeriod(UnzonedDate periodStartDate)
            {
                return periodStartDate;
            }
        }

        /// <summary>
        /// Encapsulate the concept of an approximate date precise to the month.
        /// </summary>
        public sealed class Month : ApproximateDatePrecision
        {
            /// <summary>
            /// The field to store the singleton.
            /// </summary>
            private static readonly Lazy<Month> Singleton = new Lazy<Month>(() => new Month());

            /// <summary>
            /// Prevents a default instance of the <see cref="Month"/> class from being created.
            /// </summary>
            private Month()
            {
            }

            /// <summary>
            /// Gets the value of the Instance singleton.
            /// </summary>
            /// <remarks>Singleton pattern being used to ensure reference equality for tests.</remarks>
            public static Month Instance
            {
                get { return Singleton.Value; }
            }

            /// <summary>
            /// Computes the end of the period from the start of the period.
            /// </summary>
            /// <param name="periodStartDate">The period start date.</param>
            /// <returns>The date of the end of the period.</returns>
            public override UnzonedDate? EndOfPeriod(UnzonedDate periodStartDate)
            {
                return UnzonedDate.Create(periodStartDate.Date.AddMonths(1).AddDays(-1));
            }

            /// <summary>
            /// Computes the middle of the period from the start of the period.
            /// </summary>
            /// <param name="periodStartDate">The period start date.</param>
            /// <returns>The date of the middle of the period.</returns>
            public override UnzonedDate? MiddleOfPeriod(UnzonedDate periodStartDate)
            {
                return UnzonedDate.Create(periodStartDate.Date.AddDays(15));
            }
        }

        /// <summary>
        /// Encapsulate the concept of an approximate date precise to the year.
        /// </summary>
        public sealed class Year : ApproximateDatePrecision
        {
            /// <summary>
            /// The field to store the singleton.
            /// </summary>
            private static readonly Lazy<Year> Singleton = new Lazy<Year>(() => new Year());

            /// <summary>
            /// Prevents a default instance of the <see cref="Year"/> class from being created.
            /// </summary>
            private Year()
            {
            }

            /// <summary>
            /// Gets the value of the Instance singleton.
            /// </summary>
            /// <remarks>Singleton pattern being used to ensure reference equality for tests.</remarks>
            public static Year Instance
            {
                get { return Singleton.Value; }
            }

            /// <summary>
            /// Computes the end of the period from the start of the period.
            /// </summary>
            /// <param name="periodStartDate">The period start date.</param>
            /// <returns>The date of the end of the period.</returns>
            public override UnzonedDate? EndOfPeriod(UnzonedDate periodStartDate)
            {
                return UnzonedDate.Create(periodStartDate.Date.AddYears(1).AddDays(-1));
            }

            /// <summary>
            /// Computes the middle of the period from the start of the period.
            /// </summary>
            /// <param name="periodStartDate">The period start date.</param>
            /// <returns>The date of the middle of the period.</returns>
            public override UnzonedDate? MiddleOfPeriod(UnzonedDate periodStartDate)
            {
                return UnzonedDate.Create(periodStartDate.Date.AddMonths(6));
            }
        }
    }
}
