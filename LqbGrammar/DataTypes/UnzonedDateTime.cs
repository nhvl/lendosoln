﻿namespace LqbGrammar.DataTypes
{
    using System;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// An UnzonedDateTime represents a date and time in some unspecified time zone.
    /// </summary>
    public struct UnzonedDateTime : IEquatable<UnzonedDateTime>
    {
        /// <summary>
        /// Format string used when serializing/deserializing an UnzonedDateTime.
        /// </summary>
        public const string FormatString = "yyyy-MM-ddTHH:mm:ss.fffffff";

        /// <summary>
        /// Initializes a new instance of the <see cref="UnzonedDateTime"/> struct.
        /// </summary>
        /// <param name="value">The encapsulated value.</param>
        private UnzonedDateTime(DateTime value)
        {
            this.DateTime = value;
        }

        /// <summary>
        /// Gets the encapsulated DateTime.
        /// </summary>
        /// <value>The encapsulated DateTime.</value>
        public DateTime DateTime { get; private set; }

        /// <summary>
        /// Create an UnzonedDateTime from a datetime.
        /// </summary>
        /// <param name="value">The datetime.</param>
        /// <returns>An UnzonedDateTime instance, or null.</returns>
        public static UnzonedDateTime? Create(DateTime value)
        {
            switch (value.Kind)
            {
                case DateTimeKind.Local:
                    {
                        DateTime unspecified = new DateTime(value.Ticks, DateTimeKind.Unspecified);
                        return new UnzonedDateTime(unspecified);
                    }

                case DateTimeKind.Unspecified:
                    {
                        return new UnzonedDateTime(value);
                    }

                case DateTimeKind.Utc: // An Instant should be created instead.
                    return null;

                default:
                    throw new DeveloperException(ErrorMessage.SystemError);
            }
        }

        /// <summary>
        /// Create an UnzonedDateTime from a string representation.
        /// </summary>
        /// <param name="value">The string representation.</param>
        /// <returns>An UnzonedDateTime instance, or null.</returns>
        public static UnzonedDateTime? Create(string value)
        {
            DateTime result;
            if (DateTime.TryParseExact(value, FormatString, System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.AssumeLocal, out result))
            {
                return Create(result);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Implement the equality operator for UnzonedDateTimes.
        /// </summary>
        /// <param name="lhs">The UnzonedDateTime on the left-hand side of the == operator.</param>
        /// <param name="rhs">The UnzonedDateTime on the right-hand side of the == operator.</param>
        /// <returns>True if the UnzonedDateTimes represent the same wall clock date and time.</returns>
        public static bool operator ==(UnzonedDateTime lhs, UnzonedDateTime rhs)
        {
            return lhs.Equals(rhs);
        }

        /// <summary>
        /// Implement the inequality operator for UnzonedDateTimes.
        /// </summary>
        /// <param name="lhs">The UnzonedDateTime on the left-hand side of the != operator.</param>
        /// <param name="rhs">The UnzonedDateTime on the right-hand side of the != operator.</param>
        /// <returns>True if the UnzonedDateTimes represent different wall clock dates or times.</returns>
        public static bool operator !=(UnzonedDateTime lhs, UnzonedDateTime rhs)
        {
            return !lhs.Equals(rhs);
        }

        /// <summary>
        /// Override the implementation inherited from System.Object.
        /// </summary>
        /// <param name="obj">The object being compared with this instance.</param>
        /// <returns>True if the object is an UnzonedDateTime and the two datetimes are equal, false otherwise.</returns>
        public override bool Equals(object obj)
        {
            return obj is UnzonedDateTime && this.Equals((UnzonedDateTime)obj);
        }

        /// <summary>
        /// Implement IEquatable&lt;UnzonedDateTime&gt;.
        /// </summary>
        /// <param name="other">The datetime being compared with this instance.</param>
        /// <returns>True if the two datetimes are equal, false otherwise.</returns>
        public bool Equals(UnzonedDateTime other)
        {
            return this.DateTime.Equals(other.DateTime);
        }

        /// <summary>
        /// Override the implementation inherited from System.Object.
        /// </summary>
        /// <returns>A hash code for the current instance.</returns>
        public override int GetHashCode()
        {
            return this.DateTime.GetHashCode();
        }

        /// <summary>
        /// Override the implementation inherited from System.Object.
        /// </summary>
        /// <returns>A string representation of this instance.</returns>
        public override string ToString()
        {
            return this.DateTime.ToString(FormatString);
        }
    }
}
