﻿namespace LqbGrammar.DataTypes
{
    using System;
    using LqbGrammar.Validation;

    /// <summary>
    /// Semantic type for social security numbers.
    /// </summary>
    public struct SocialSecurityNumber : ISemanticType, IEquatable<SocialSecurityNumber>
    {
        /// <summary>
        /// Unmasked SSN with no dashes.
        /// </summary>
        private string unmaskedSsn;

        /// <summary>
        /// Initializes a new instance of the <see cref="SocialSecurityNumber"/> struct.
        /// </summary>
        /// <param name="unmaskedSsn">The unmasked SSN value.</param>
        private SocialSecurityNumber(string unmaskedSsn)
        {
            this.unmaskedSsn = unmaskedSsn;
        }

        /// <summary>
        /// Gets the unmasked SSN, with dashes.
        /// </summary>
        public string UnmaskedSsn
        {
            get
            {
                return $"{unmaskedSsn.Substring(0, 3)}-{unmaskedSsn.Substring(3, 2)}-{unmaskedSsn.Substring(5)}";
            }
        }

        /// <summary>
        /// Gets the last four digits of the SSN.
        /// </summary>
        public string LastFour
        {
            get
            {
                return this.unmaskedSsn.Substring(5);
            }
        }

        /// <summary>
        /// Gets the masked SSN, with only the last four digits unmasked.
        /// </summary>
        public string MaskedSsn
        {
            get
            {
                return $"***-**-{unmaskedSsn.Substring(5)}";
            }
        }

        /// <summary>
        /// Gets the encapsulated social security number without masking.
        /// </summary>
        object ISemanticType.Value => this.unmaskedSsn;

        /// <summary>
        /// Returns a Social Security Number if the input value is valid.
        /// </summary>
        /// <param name="unmaskedValue">The unmasked SSN value. Must be the full 9 digits, with or without dashes.</param>
        /// <returns>A valid Social Security Number or null.</returns>
        public static SocialSecurityNumber? Create(string unmaskedValue)
        {
            if (string.IsNullOrWhiteSpace(unmaskedValue))
            {
                return null;
            }

            RegularExpressionResult result = RegularExpressionEngine.Execute(unmaskedValue, RegularExpressionString.SocialSecurityNumber);
            if (!result.IsMatch())
            {
                return null;
            }

            return new SocialSecurityNumber(unmaskedValue.Replace("-", string.Empty));  // strip out dashes.
        }

        /// <summary>
        /// Static method that checks for equality between two instances.
        /// </summary>
        /// <param name="lhs">Instance on the left side of the == operator.</param>
        /// <param name="rhs">Instance on the right side of the == operator.</param>
        /// <returns>True if the two instances are equal, false otherwise.</returns>
        public static new bool Equals(object lhs, object rhs)
        {
            if (!(lhs is SocialSecurityNumber))
            {
                return false;
            }

            if (!(rhs is SocialSecurityNumber))
            {
                return false;
            }

            return ((SocialSecurityNumber)lhs).Equals((SocialSecurityNumber)rhs);
        }

        /// <summary>
        /// The == operator is implemented here.
        /// </summary>
        /// <param name="lhs">Instance on the left of the == operator.</param>
        /// <param name="rhs">Instance on the right of the == operator.</param>
        /// <returns>True if the two instances are equal, false otherwise.</returns>
        public static bool operator ==(SocialSecurityNumber lhs, SocialSecurityNumber rhs)
        {
            return lhs.Equals(rhs);
        }

        /// <summary>
        /// The != operator is implemented here.
        /// </summary>
        /// <param name="lhs">Instance on the left of the != operator.</param>
        /// <param name="rhs">Instance on the right of the != operator.</param>
        /// <returns>True if the two instances are not equal, false otherwise.</returns>
        public static bool operator !=(SocialSecurityNumber lhs, SocialSecurityNumber rhs)
        {
            return !lhs.Equals(rhs);
        }

        /// <summary>
        /// Override the Equals method inherited from the Object class.
        /// </summary>
        /// <param name="obj">Instance that is checked for equality with this instance.</param>
        /// <returns>True if obj is equal to this instance, false otherwise.</returns>
        public override bool Equals(object obj)
        {
            if (!(obj is SocialSecurityNumber))
            {
                return false;
            }

            return this.Equals((SocialSecurityNumber)obj);
        }

        /// <summary>
        /// Implement IEquatable.
        /// </summary>
        /// <param name="other">Instance that is checked for equality with this instance.</param>
        /// <returns>True if other is equal to this instance, false otherwise.</returns>
        public bool Equals(SocialSecurityNumber other)
        {
            return this.unmaskedSsn == other.unmaskedSsn;
        }

        /// <summary>
        /// Returns the masked SSN in string format.
        /// </summary>
        /// <returns>The masked SSN in string format.</returns>
        /// <remarks>
        /// Having ToString return the masked SSN should help prevent accidentally exposing the unmasked SSN.
        /// If the unmasked SSN is needed instead, then it should be explicitly called for using the provided property.
        /// </remarks>
        public override string ToString()
        {
            return this.MaskedSsn;
        }

        /// <summary>
        /// Override of the method inherited from the Object class.
        /// </summary>
        /// <returns>An integer that is the hash code for this instance.</returns>
        public override int GetHashCode()
        {
            return this.unmaskedSsn.GetHashCode();
        }
    }
}
