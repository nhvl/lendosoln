﻿namespace LqbGrammar.DataTypes
{
    using System;

    /// <summary>
    /// When sending an Http request, a method must be
    /// specified, which can be one of a small set of values
    /// that are encapsulated here.
    /// </summary>
    public struct HttpMethod : IEquatable<HttpMethod>
    {
        /// <summary>
        /// GET method.
        /// </summary>
        public static readonly HttpMethod Get = new HttpMethod("GET");

        /// <summary>
        /// POST method.
        /// </summary>
        public static readonly HttpMethod Post = new HttpMethod("POST");

        /// <summary>
        /// HEAD method.
        /// </summary>
        public static readonly HttpMethod Head = new HttpMethod("HEAD");

        /// <summary>
        /// PUT method.
        /// </summary>
        public static readonly HttpMethod Put = new HttpMethod("PUT");

        /// <summary>
        /// DELETE method.
        /// </summary>
        public static readonly HttpMethod Delete = new HttpMethod("DELETE");

        /// <summary>
        /// TRACE method.
        /// </summary>
        public static readonly HttpMethod Trace = new HttpMethod("TRACE");

        /// <summary>
        /// OPTIONS method.
        /// </summary>
        public static readonly HttpMethod Options = new HttpMethod("OPTIONS");

        /// <summary>
        /// The method string value.
        /// </summary>
        private string method;

        /// <summary>
        /// Initializes a new instance of the <see cref="HttpMethod"/> struct.
        /// </summary>
        /// <param name="method">The method string value.</param>
        private HttpMethod(string method)
        {
            this.method = method;
        }

        /// <summary>
        /// Compare two objects for equality and that they are instances of the HttpMethod class.
        /// </summary>
        /// <param name="lhs">The object on the left side of the == operator.</param>
        /// <param name="rhs">The object on the right side of the == operator.</param>
        /// <returns>True if both objects are instances of the HttpMethod class and equal to each other.</returns>
        public static new bool Equals(object lhs, object rhs)
        {
            if (!(lhs is HttpMethod))
            {
                return false;
            }

            if (!(rhs is HttpMethod))
            {
                return false;
            }

            return ((HttpMethod)lhs).Equals((HttpMethod)rhs);
        }

        /// <summary>
        /// The equality operator.
        /// </summary>
        /// <param name="lhs">The instance that is on the left side of the == operator.</param>
        /// <param name="rhs">The instance that is on the right side of the == operator.</param>
        /// <returns>True if the instances are equal, false otherwise.</returns>
        public static bool operator ==(HttpMethod lhs, HttpMethod rhs)
        {
            return lhs.Equals(rhs);
        }

        /// <summary>
        /// The inequality operator.
        /// </summary>
        /// <param name="lhs">The instance that is on the left side of the != operator.</param>
        /// <param name="rhs">The instance that is on the right side of the != operator.</param>
        /// <returns>True if the two instances are not equal, false otherwise.</returns>
        public static bool operator !=(HttpMethod lhs, HttpMethod rhs)
        {
            return !lhs.Equals(rhs);
        }

        /// <summary>
        /// Override the Object.ToString() method.
        /// </summary>
        /// <returns>The method string value.</returns>
        public override string ToString()
        {
            return this.method;
        }

        /// <summary>
        /// Evaluate equality.
        /// </summary>
        /// <param name="other">Another instance being compared to this instance.</param>
        /// <returns>True if equals, false otherwise.</returns>
        public bool Equals(HttpMethod other)
        {
            return this.method == other.method;
        }

        /// <summary>
        /// Compare this instance with another object for equality.
        /// </summary>
        /// <param name="obj">The other object.</param>
        /// <returns>True if the other object is an instance of the EmailAddress class and it is equal to this instance.</returns>
        public override bool Equals(object obj)
        {
            if (!(obj is HttpMethod))
            {
                return false;
            }

            return Equals((HttpMethod)obj);
        }

        /// <summary>
        /// Override the implementation inherited from the Object class.
        /// </summary>
        /// <returns>An integer that is the hash of this object.</returns>
        public override int GetHashCode()
        {
            return this.method.GetHashCode();
        }
    }
}
