﻿namespace LqbGrammar.DataTypes
{
    /// <summary>
    /// The name of a person.
    /// </summary>
    public partial struct PersonName
    {
        /// <summary>
        /// An instance of an invalid value that can be used for variable initialization.
        /// </summary>
        public static readonly PersonName BadName = new PersonName(string.Empty);

        /// <summary>
        /// An instance that represents David Dao, which is hardcoded in some utilities.
        /// </summary>
        public static readonly PersonName DavidDao = new PersonName("David Dao");

        /// <summary>
        /// Sometimes the name is already in the system, and we just have to move forward with a name. <para/>
        /// This in turn calls PersonName.Create with what it considers to be a safe name.
        /// </summary>
        /// <param name="firstName">The person's first name.</param>
        /// <param name="lastName">The person's last name.</param>
        /// <returns>Validated name, or null.</returns>
        public static PersonName? CreateWithReplace(string firstName, string lastName)
        {
            firstName = firstName ?? string.Empty;
            lastName = lastName ?? string.Empty;
            if (string.IsNullOrEmpty(firstName) && string.IsNullOrEmpty(lastName))
            {
                return null;
            }

            var safeFirstName = System.Text.RegularExpressions.Regex.Replace(firstName, RegularExpressionString.DisallowedNamePartialCharactersSTRANGE.ToString(), " ");
            var safeLastName = System.Text.RegularExpressions.Regex.Replace(lastName, RegularExpressionString.DisallowedNamePartialCharactersSTRANGE.ToString(), " ");
            var safeName = (safeFirstName + " " + safeLastName).Trim();

            return Create(safeName);
        }
    }
}
