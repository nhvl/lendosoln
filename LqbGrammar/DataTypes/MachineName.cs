﻿namespace LqbGrammar.DataTypes
{
    using System;
    using LqbGrammar.Validation;

    /// <summary>
    /// Encapsulate and validate the name of a computer that participates in the LQB system.
    /// </summary>
    public struct MachineName : IEquatable<MachineName>
    {
        /// <summary>
        /// An instance of an invalid value that can be used for variable initialization.
        /// </summary>
        public static readonly MachineName BadName = new MachineName(string.Empty);

        /// <summary>
        /// The name of the LQB machine.
        /// </summary>
        private string name;

        /// <summary>
        /// Initializes a new instance of the <see cref="MachineName"/> struct.
        /// </summary>
        /// <param name="name">The LQB machine name.</param>
        private MachineName(string name)
        {
            this.name = name;
        }

        /// <summary>
        /// Validate the input LQB machine name.
        /// </summary>
        /// <param name="name">The input LQB machine name to be validated.</param>
        /// <returns>The validated value, or null.</returns>
        public static MachineName? Create(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                return null;
            }

            RegularExpressionResult result = RegularExpressionEngine.Execute(name, RegularExpressionString.MachineName);
            if (!result.IsMatch())
            {
                return null;
            }

            return new MachineName(name);
        }

        /// <summary>
        /// Compare two objects for equality and that they are both validated LQB machine names.
        /// </summary>
        /// <param name="lhs">Object on the left side of the == operator.</param>
        /// <param name="rhs">Object on the right side of the == operator.</param>
        /// <returns>True if both objects are instances of the MachineName class, and are equal to each other.</returns>
        public static new bool Equals(object lhs, object rhs)
        {
            if (!(lhs is MachineName))
            {
                return false;
            }

            if (!(rhs is MachineName))
            {
                return false;
            }

            return ((MachineName)lhs).Equals((MachineName)rhs);
        }

        /// <summary>
        /// The equality operator for comparing two instances of a valid LQB machine name.
        /// </summary>
        /// <param name="lhs">The instance on the left side of the == operator.</param>
        /// <param name="rhs">The instance on the right side of the == operator.</param>
        /// <returns>True if the two instances have the same name.</returns>
        public static bool operator ==(MachineName lhs, MachineName rhs)
        {
            return lhs.Equals(rhs);
        }

        /// <summary>
        /// The inequality operator for comparing two instances of a valid LQB machine name.
        /// </summary>
        /// <param name="lhs">The instance on the left side of the != operator.</param>
        /// <param name="rhs">The instance on the right side of the != operator.</param>
        /// <returns>True if the two instances are not equal to each other.</returns>
        public static bool operator !=(MachineName lhs, MachineName rhs)
        {
            return !lhs.Equals(rhs);
        }

        /// <summary>
        /// Override the implementation inherited from the Object class.
        /// </summary>
        /// <returns>The LQB machine name.</returns>
        public override string ToString()
        {
            return this.name;
        }

        /// <summary>
        /// Test this instance against another for equality.
        /// </summary>
        /// <param name="other">The other instance.</param>
        /// <returns>True if the two instances are equal, false otherwise.</returns>
        public bool Equals(MachineName other)
        {
            return string.Compare(this.name, other.name, StringComparison.OrdinalIgnoreCase) == 0;
        }

        /// <summary>
        /// Test this instance against another object for equality.
        /// </summary>
        /// <param name="obj">The other object.</param>
        /// <returns>True if the other object is a validated LQB machine name and the names are equal.</returns>
        public override bool Equals(object obj)
        {
            if (!(obj is MachineName))
            {
                return false;
            }

            return Equals((MachineName)obj);
        }

        /// <summary>
        /// Override the implementation inherited from the Object class.
        /// </summary>
        /// <returns>An integer that is the hash of this instance.</returns>
        public override int GetHashCode()
        {
            return this.name.GetHashCode();
        }
    }
}
