﻿namespace LqbGrammar.DataTypes
{
    using System;

    /// <summary>
    /// Encapsulate the concept of a URI value and hold
    /// various hard-coded URIs that the code may
    /// need.
    /// </summary>
    public struct LqbAbsoluteUri
    {
        /// <summary>
        /// An instance of a bad LqbUri that can be used for variable initialization.
        /// </summary>
        public static readonly LqbAbsoluteUri BadURI = new LqbAbsoluteUri(null);

        /// <summary>
        /// The URI value.
        /// </summary>
        private string uri;

        /// <summary>
        /// Initializes a new instance of the <see cref="LqbAbsoluteUri"/> struct.
        /// </summary>
        /// <param name="uri">The contained XML element object.</param>
        private LqbAbsoluteUri(string uri)
        {
            this.uri = uri;
        }

        /// <summary>
        /// Wrap the input URI.
        /// </summary>
        /// <param name="test">The input URI.</param>
        /// <returns>The wrapped URI, or null if the input is not valid.</returns>
        public static LqbAbsoluteUri? Create(string test)
        {
            if (!Validate(test))
            {
                return null;
            }
            else
            {
                return new LqbAbsoluteUri(test);
            }
        }

        /// <summary>
        /// Override ToString().
        /// </summary>
        /// <returns>The contained URI.</returns>
        public override string ToString()
        {
            return this.uri;
        }

        /// <summary>
        /// Gets the scheme for this URI.
        /// </summary>
        /// <returns>The scheme for this URI.</returns>
        public string GetScheme()
        {
            return new Uri(this.uri).Scheme;
        }

        /// <summary>
        /// Converts this object into its .NET representation.
        /// </summary>
        /// <returns>A new Uri object.</returns>
        public Uri GetUri()
        {
            return new Uri(this.uri);
        }

        /// <summary>
        /// Validate the URI.
        /// </summary>
        /// <param name="test">A URI sting.</param>
        /// <returns>True if the input URI is valid, false otherwise.</returns>
        private static bool Validate(string test)
        {
            return Uri.IsWellFormedUriString(test, UriKind.Absolute);
        }
    }
}
