﻿namespace LqbGrammar.DataTypes
{
    using System;

    /// <summary>
    /// Encapsulates the result of some operation, which can either be a valid value or an error.
    /// </summary>
    /// <typeparam name="TValue">The type of the value that the operation would return if successful.</typeparam>
    public class Result<TValue>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Result{TValue}"/> class containing a valid value.
        /// </summary>
        /// <param name="value">The valid result value.</param>
        public Result(TValue value)
        {
            this.Value = value;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Result{TValue}"/> class containing an error result.
        /// </summary>
        /// <param name="error">The error result value.</param>
        public Result(Exception error)
        {
            this.Error = error;
        }

        /// <summary>
        /// Gets the valid value result, or <see cref="default(TValue)"/> if this is an error result.
        /// </summary>
        /// <value>The valid value result, or <see cref="default(TValue)"/> if this is an error result.</value>
        public TValue Value { get; }

        /// <summary>
        /// Gets the error result value, or null if this is a valid value result.
        /// </summary>
        /// <value>The error result value, or null if this is a valid value result.</value>
        public Exception Error { get; }

        /// <summary>
        /// Gets a value indicating whether the current instance is an error result.
        /// </summary>
        /// <value>A value indicating whether the current instance is an error result.</value>
        public bool HasError => this.Error != null;

        /// <summary>
        /// Factory method to create a new instance of the <see cref="Result{TValue}"/> class
        /// indicating a valid result.
        /// </summary>
        /// <param name="value">The valid value result.</param>
        /// <returns>A new instance of the <see cref="Result{TValue}"/> class indicating a valid result.</returns>
        public static Result<TValue> Success(TValue value)
        {
            return new Result<TValue>(value);
        }

        /// <summary>
        /// Factory method to create a new instance of the <see cref="Result{TValue}"/> class
        /// indicating an error result.
        /// </summary>
        /// <param name="error">The error value result.</param>
        /// <returns>A new instance of the <see cref="Result{TValue}"/> class indicating an error result.</returns>
        public static Result<TValue> Failure(Exception error)
        {
            return new Result<TValue>(error);
        }
    }
}
