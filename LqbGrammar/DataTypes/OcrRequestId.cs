﻿namespace LqbGrammar.DataTypes
{
    using System;

    /// <summary>
    /// Encapsulate the concept of an identifier for document capture requests in the LQB system.
    /// </summary>
    public struct OcrRequestId : IEquatable<OcrRequestId>
    {
        /// <summary>
        /// An instance of the class that fails validation, can be used for initialization when necessary.
        /// </summary>
        public static readonly OcrRequestId BadIdentifier = new OcrRequestId(Guid.Empty);

        /// <summary>
        /// Initializes a new instance of the <see cref="OcrRequestId"/> struct.
        /// </summary>
        /// <param name="value">The value.</param>
        private OcrRequestId(Guid value)
        {
            this.Value = value;
        }

        /// <summary>
        /// Gets the encapsulated value, all identifiers are guids.
        /// </summary>
        /// <value>The guid of the OCR request.</value>
        public Guid Value { get; private set; }

        /// <summary>
        /// Attempt to parse the input value into a validated identifier.
        /// </summary>
        /// <param name="value">Input value that will be parsed.</param>
        /// <returns>Valid identifier or null.</returns>
        public static OcrRequestId? Create(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return null;
            }

            Guid guid;
            bool ok = Guid.TryParse(value, out guid);
            if (ok && guid != Guid.Empty)
            {
                return new OcrRequestId(guid);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Creates and returns a new instance of the <see cref="OcrRequestId"/> struct.
        /// </summary>
        /// <returns>
        /// The new instance.
        /// </returns>
        public static OcrRequestId Generate()
        {
            var newId = Guid.NewGuid();
            return new OcrRequestId(newId);
        }

        /// <summary>
        /// Static method that checks for equality between two instances.
        /// </summary>
        /// <param name="lhs">Instance on the left side of the == operator.</param>
        /// <param name="rhs">Instance on the right side of the == operator.</param>
        /// <returns>True if the two instances are equal, false otherwise.</returns>
        public static new bool Equals(object lhs, object rhs)
        {
            if (!(lhs is OcrRequestId))
            {
                return false;
            }

            if (!(rhs is OcrRequestId))
            {
                return false;
            }

            return ((OcrRequestId)lhs).Equals((OcrRequestId)rhs);
        }

        /// <summary>
        /// The == operator is implemented here.
        /// </summary>
        /// <param name="lhs">Instance on the left of the == operator.</param>
        /// <param name="rhs">Instance on the right of the == operator.</param>
        /// <returns>True if the two instances are equal, false otherwise.</returns>
        public static bool operator ==(OcrRequestId lhs, OcrRequestId rhs)
        {
            return lhs.Equals(rhs);
        }

        /// <summary>
        /// The != operator is implemented here.
        /// </summary>
        /// <param name="lhs">Instance on the left of the != operator.</param>
        /// <param name="rhs">Instance on the right of the != operator.</param>
        /// <returns>True if the two instances are not equal, false otherwise.</returns>
        public static bool operator !=(OcrRequestId lhs, OcrRequestId rhs)
        {
            return !lhs.Equals(rhs);
        }

        /// <summary>
        /// Return the encapsulated value.
        /// </summary>
        /// <returns>Encapsulated value.</returns>
        public override string ToString()
        {
            return this.Value.ToString();
        }

        /// <summary>
        /// Implement IEquatable.
        /// </summary>
        /// <param name="other">Instance that is checked for equality with this instance.</param>
        /// <returns>True if other is equal to this instance, false otherwise.</returns>
        public bool Equals(OcrRequestId other)
        {
            return this.Value == other.Value;
        }

        /// <summary>
        /// Override the Equals method inherited from the Object class.
        /// </summary>
        /// <param name="obj">Instance that is checked for equality with this instance.</param>
        /// <returns>True if obj is equal to this instance, false otherwise.</returns>
        public override bool Equals(object obj)
        {
            if (!(obj is OcrRequestId))
            {
                return false;
            }

            return this.Equals((OcrRequestId)obj);
        }

        /// <summary>
        /// Override of the method inherited from the Object class.
        /// </summary>
        /// <returns>An integer that is the hash code for this instance.</returns>
        public override int GetHashCode()
        {
            return this.Value.GetHashCode();
        }
    }
}
