﻿namespace LqbGrammar.DataTypes
{
    using System.IO;

    /// <summary>
    /// Wraps a Stream so that it can write itself to another Stream.
    /// </summary>
    public class StreamContent : IStreamableContent
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="StreamContent"/> class.
        /// </summary>
        /// <param name="contentStream">The content stream to read from.</param>
        /// <param name="length">The length of the content, if known.</param>
        public StreamContent(Stream contentStream, long? length = null)
        {
            this.Content = contentStream;
            this.Length = length;
        }

        /// <summary>
        /// Gets the stream containing the content.
        /// </summary>
        private Stream Content { get; }

        /// <summary>
        /// Gets the length of the content, if known.
        /// </summary>
        private long? Length { get; }

        /// <summary>
        /// Gets the count of bytes in the content.
        /// </summary>
        /// <returns>The count of bytes.</returns>
        public long? GetContentLengthBytes()
        {
            return this.Length;
        }

        /// <summary>
        /// Writes to the given stream.
        /// </summary>
        /// <param name="writeTo">The stream to write to.</param>
        public void WriteToStream(Stream writeTo)
        {
            this.Content.CopyTo(writeTo);
        }
    }
}
