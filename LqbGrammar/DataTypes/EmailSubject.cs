﻿namespace LqbGrammar.DataTypes
{
    using LqbGrammar.Exceptions;
    using LqbGrammar.Validation;

    /// <summary>
    /// Encapsulate and validate an email's subject line.
    /// </summary>
    public struct EmailSubject : System.IEquatable<EmailSubject>
    {
        /// <summary>
        /// An invalid instance that can be used for variable initialization.
        /// </summary>
        public static readonly EmailSubject BadSubject = new EmailSubject(string.Empty);

        /// <summary>
        /// The email subject line.
        /// </summary>
        private string subject;

        /// <summary>
        /// Initializes a new instance of the <see cref="EmailSubject"/> struct.
        /// </summary>
        /// <param name="subject">The subject.</param>
        private EmailSubject(string subject)
        {
            this.subject = subject;
        }

        /// <summary>
        /// Validate an email subject line.
        /// </summary>
        /// <param name="subject">The email subject line.</param>
        /// <returns>The validated email subject line, or null.</returns>
        public static EmailSubject? Create(string subject)
        {
            if (string.IsNullOrEmpty(subject))
            {
                return null;
            }

            // XML comments find their way into the email subject field.
            // I don't want to include the '<' character in the regular expression
            // because that will open up XSS attacks.  Instead I'll just validate
            // after removal of the opening phrase of the XML comments.
            // [Yes, this reasoning should also apply to the email body, but
            //  that is an issue for another day...]
            string clean = subject.Replace("<!--", string.Empty);

            RegularExpressionResult result = RegularExpressionEngine.Execute(clean, RegularExpressionString.EmailSubject);
            if (!result.IsMatch())
            {
                return null;
            }

            return new EmailSubject(subject);
        }

        /// <summary>
        /// Compare two objects for equality and they are both the same validated email subject line.
        /// </summary>
        /// <param name="lhs">The object on the left side of the == operator.</param>
        /// <param name="rhs">The object on the right side of the == operator.</param>
        /// <returns>True if both objects are instances of the EmailSubject class and they are equal to each other.</returns>
        public static new bool Equals(object lhs, object rhs)
        {
            if (!(lhs is EmailSubject))
            {
                return false;
            }

            if (!(rhs is EmailSubject))
            {
                return false;
            }

            return ((EmailSubject)lhs).Equals((EmailSubject)rhs);
        }

        /// <summary>
        /// Implement the equality operator for a validated email subject line.
        /// </summary>
        /// <param name="lhs">Instance on the left side of the == operator.</param>
        /// <param name="rhs">Instance on the right side of the == operator.</param>
        /// <returns>True if the instances are equal.</returns>
        public static bool operator ==(EmailSubject lhs, EmailSubject rhs)
        {
            return lhs.Equals(rhs);
        }

        /// <summary>
        /// Implement the inequality operator for a validated email subject line.
        /// </summary>
        /// <param name="lhs">The instance on the left side of the != operator.</param>
        /// <param name="rhs">The instance on the right side of the != operator.</param>
        /// <returns>Return true if the two instances are not equal, false otherwise.</returns>
        public static bool operator !=(EmailSubject lhs, EmailSubject rhs)
        {
            return !lhs.Equals(rhs);
        }

        /// <summary>
        /// Override the implementation inherited from the Object class.
        /// </summary>
        /// <returns>The email subject line.</returns>
        public override string ToString()
        {
            return this.subject;
        }

        /// <summary>
        /// Compare this instance with another instance for equality.
        /// </summary>
        /// <param name="other">The other instance.</param>
        /// <returns>True if the two instances are equal, false otherwise.</returns>
        public bool Equals(EmailSubject other)
        {
            return this.subject == other.subject;
        }
        
        /// <summary>
        /// Compares this instance with another object for equality.
        /// </summary>
        /// <param name="obj">The other object.</param>
        /// <returns>True if the other object is a valid email subject line and the two objects have the same value.</returns>
        public override bool Equals(object obj)
        {
            if (!(obj is EmailSubject))
            {
                return false;
            }

            return Equals((EmailSubject)obj);
        }

        /// <summary>
        /// Override the implementation inherited from the Object class.
        /// </summary>
        /// <returns>An integer that is the hash of this instance.</returns>
        public override int GetHashCode()
        {
            return this.subject.GetHashCode();
        }
    }
}
