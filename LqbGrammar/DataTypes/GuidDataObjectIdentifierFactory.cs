﻿namespace LqbGrammar.DataTypes
{
    using System;

    /// <summary>
    /// A factory for generating data object identifiers where the key is a Guid.
    /// </summary>
    /// <typeparam name="TValueKind">The type of value to identify.</typeparam>
    public class GuidDataObjectIdentifierFactory<TValueKind> : IDataObjectIdentifierFactory<TValueKind, Guid>
        where TValueKind : DataObjectKind
    {
        /// <summary>
        /// Creates an identifier from a string. Returns null if there is a parse error.
        /// </summary>
        /// <param name="idString">The string containing the id.</param>
        /// <returns>The parsed identifier.</returns>
        public DataObjectIdentifier<TValueKind, Guid>? Create(string idString)
        {
            Guid id;
            if (!Guid.TryParse(idString, out id))
            {
                return null;
            }

            return DataObjectIdentifier<TValueKind, Guid>.Create(id);
        }

        /// <summary>
        /// Creates a new identifier from an id value.
        /// </summary>
        /// <param name="idValue">The value for the id.</param>
        /// <returns>The new identifier.</returns>
        public DataObjectIdentifier<TValueKind, Guid> Create(Guid idValue)
        {
            return DataObjectIdentifier<TValueKind, Guid>.Create(idValue);
        }

        /// <summary>
        /// Generates a new identifier.
        /// </summary>
        /// <returns>A new identifier.</returns>
        public DataObjectIdentifier<TValueKind, Guid> NewId()
        {
            Guid id = Guid.NewGuid();
            return DataObjectIdentifier<TValueKind, Guid>.Create(id);
        }
    }
}
