﻿namespace LqbGrammar.DataTypes
{
    using System;

    /// <summary>
    /// Encapsulate the concept of an identifier for user account in the LQB system.
    /// </summary>
    public struct UserAccountIdentifier : System.IEquatable<UserAccountIdentifier>
    {
        /// <summary>
        /// An instance of the class that fails validation, can be used for initialization when necessary.
        /// </summary>
        public static readonly UserAccountIdentifier BadIdentifier = new UserAccountIdentifier(Guid.Empty);

        /// <summary>
        /// Encapsulated value, all identifiers are guids.
        /// </summary>
        private Guid value;

        /// <summary>
        /// Initializes a new instance of the <see cref="UserAccountIdentifier"/> struct.
        /// </summary>
        /// <param name="value">The value.</param>
        private UserAccountIdentifier(Guid value)
        {
            this.value = value;
        }

        /// <summary>
        /// Attempt to parse the input value into a validated user account identifier.
        /// </summary>
        /// <param name="value">Input value that will be parsed.</param>
        /// <returns>Valid user account identifier, or null.</returns>
        public static UserAccountIdentifier? Create(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return null;
            }

            Guid guid;
            bool ok = Guid.TryParse(value, out guid);
            if (ok && guid != Guid.Empty)
            {
                return new UserAccountIdentifier(guid);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Static method that checks for equality between two instances.
        /// </summary>
        /// <param name="lhs">Instance on the left side of the == operator.</param>
        /// <param name="rhs">Instance on the right side of the == operator.</param>
        /// <returns>True if the two instances are equal, false otherwise.</returns>
        public static new bool Equals(object lhs, object rhs)
        {
            if (!(lhs is UserAccountIdentifier))
            {
                return false;
            }

            if (!(rhs is UserAccountIdentifier))
            {
                return false;
            }

            return ((UserAccountIdentifier)lhs).Equals((UserAccountIdentifier)rhs);
        }

        /// <summary>
        /// The == operator is implemented here.
        /// </summary>
        /// <param name="lhs">Instance on the left of the == operator.</param>
        /// <param name="rhs">Instance on the right of the == operator.</param>
        /// <returns>True if the two instances are equal, false otherwise.</returns>
        public static bool operator ==(UserAccountIdentifier lhs, UserAccountIdentifier rhs)
        {
            return lhs.Equals(rhs);
        }

        /// <summary>
        /// The != operator is implemented here.
        /// </summary>
        /// <param name="lhs">Instance on the left of the != operator.</param>
        /// <param name="rhs">Instance on the right of the != operator.</param>
        /// <returns>True if the two instances are not equal, false otherwise.</returns>
        public static bool operator !=(UserAccountIdentifier lhs, UserAccountIdentifier rhs)
        {
            return !lhs.Equals(rhs);
        }

        /// <summary>
        /// Return the encapsulated value.
        /// </summary>
        /// <returns>Encapsulated value.</returns>
        public override string ToString()
        {
            return this.value.ToString();
        }

        /// <summary>
        /// Implement IEquatable.
        /// </summary>
        /// <param name="other">Instance that is checked for equality with this instance.</param>
        /// <returns>True if other is equal to this instance, false otherwise.</returns>
        public bool Equals(UserAccountIdentifier other)
        {
            return this.value == other.value;
        }

        /// <summary>
        /// Override the Equals method inherited from the Object class.
        /// </summary>
        /// <param name="obj">Instance that is checked for equality with this instance.</param>
        /// <returns>True if obj is equal to this instance, false otherwise.</returns>
        public override bool Equals(object obj)
        {
            if (!(obj is UserAccountIdentifier))
            {
                return false;
            }

            return this.Equals((UserAccountIdentifier)obj);
        }

        /// <summary>
        /// Override of the method inherited from the Object class.
        /// </summary>
        /// <returns>An integer that is the hash code for this instance.</returns>
        public override int GetHashCode()
        {
            return this.value.GetHashCode();
        }
    }
}
