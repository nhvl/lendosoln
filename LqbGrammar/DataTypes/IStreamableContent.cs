﻿namespace LqbGrammar.DataTypes
{
    using System.IO;

    /// <summary>
    /// Represents content to be written to a stream.
    /// Implementations:
    /// <seealso cref="StringContent"/>,
    /// <seealso cref="ByteContent"/>,
    /// <seealso cref="StreamContent"/>,
    /// <seealso cref="XmlContent"/>
    /// Other implementations to think about:
    /// XmlSerializerContent
    /// JsonNetContent.
    /// </summary>
    public interface IStreamableContent
    {
        /// <summary>
        /// Writes the content to the given stream.
        /// </summary>
        /// <param name="writeTo">A stream prepared for writing.</param>
        void WriteToStream(Stream writeTo);

        /// <summary>
        /// Gets the number of bytes that will be written to the stream.
        /// </summary>
        /// <returns>The number of bytes that will be written, or null if the number of bytes is unknown.</returns>
        long? GetContentLengthBytes();
    }
}
