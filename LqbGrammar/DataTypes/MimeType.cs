﻿namespace LqbGrammar.DataTypes
{
    using System;
    using System.Collections.Generic;
    using Exceptions;

    /// <summary>
    /// Base class for mime-types.
    /// </summary>
    public abstract class MimeType
    {
        /// <summary>
        /// A */* instance.
        /// </summary>
        public static readonly MimeType WildCard = new WildCardImpl();

        /// <summary>
        /// Initializes a new instance of the <see cref="MimeType"/> class, representing a MIME Content-Type header value.
        /// </summary>
        /// <remarks>
        /// This class is abstract and the constructor is protected in order to encourage the semantic use of its subclasses.
        /// Please consider using or adding a subclass instead of an ad-hoc content type.
        /// </remarks>
        /// <param name="mediaType">The media type (text, application, image, multipart, etc).</param>
        /// <param name="subType">The media subtype (jpeg, pdf, form-data, etc).</param>
        protected MimeType(MimeTypeToken mediaType, MimeTypeToken subType)
        {
            this.MediaType = mediaType;
            this.SubMediaType = subType;
        }

        /// <summary>
        /// Gets or sets the media-type for this mime-type.
        /// </summary>
        /// <value>The media-type for this mime-type.</value>
        public virtual MimeTypeToken MediaType
        {
            get; protected set;
        }

        /// <summary>
        /// Gets or sets the sub media-type for this mime-type.
        /// </summary>
        /// <value>The sub media-type for this mime-type.</value>
        public virtual MimeTypeToken SubMediaType
        {
            get; protected set;
        }

        /// <summary>
        /// Gets or sets a list of type parameters that come after the content type value.
        /// </summary>
        /// <example>
        /// Ex.
        /// "text/*" content types have the "charset" parameter.
        /// "multipart/*" content types have the "boundary" parameter.
        /// </example>
        /// <value>Type parameters.</value>
        public List<MimeTypeParameter> Parameters { get; protected set; } = new List<MimeTypeParameter>();

        /// <summary>
        /// Create an instance of the wildcard mime-type with input quality factor,
        /// which must be in the format 0.x where x in [1-9].
        /// </summary>
        /// <param name="quality">The quality factor.</param>
        /// <returns>A WildCard mime-type.</returns>
        public static MimeType CreateQualifiedWildCard(string quality)
        {
            return new WildCardImpl(quality);
        }

        /// <summary>
        /// A string representation of the mime-type suitable for being the value in an MIME Content-Type Header Field.
        /// </summary>
        /// <returns>String representation of the mime-type.</returns>
        public override string ToString()
        {
            string contentTypeString = $"{this.MediaType}/{this.SubMediaType}";
            
            foreach (MimeTypeParameter parameter in this.Parameters)
            {
                contentTypeString += "; " + parameter.ToString();
            }

            return contentTypeString;
        }

        /// <summary>
        /// Defines a parameter for a MIME Content-type. For example, "text/plain" could have a parameter of "charset=US-ASCII".
        /// </summary>
        public struct MimeTypeParameter
        {
            /// <summary>
            /// The parameter's name, also known as the "attribute" of the parameter.
            /// </summary>
            public readonly MimeTypeToken Attribute;

            /// <summary>
            /// A quoted string value for the parameter. Note that " and \r characters should be preceded by a \ character for properly escaping.
            /// </summary>
            public readonly string Value;

            /// <summary>
            /// Initializes a new instance of the <see cref="MimeTypeParameter"/> struct.
            /// </summary>
            /// <param name="attribute">The attribute of the parameter.</param>
            /// <param name="value">The value of the parameter.</param>
            public MimeTypeParameter(MimeTypeToken attribute, string value)
            {
                this.Attribute = attribute;
                this.Value = value;
            }

            /// <summary>
            /// Overrides the default <see cref="ToString"/> implementation.
            /// </summary>
            /// <returns>The parameter as it should appear in a content-type string.</returns>
            public override string ToString()
            {
                return $"{Attribute}=\"{Value}\"";
            }
        }

        /// <summary>
        /// A semantic type for validating MIME type tokens, including those in media types, subtypes, and parameters.
        /// </summary>
        public struct MimeTypeToken
        {
            /// <summary>
            /// Provides a pre-validated token to avoid the overhead of regex checks.
            /// </summary>
            public static readonly MimeTypeToken Application = new MimeTypeToken("application");

            /// <summary>
            /// Internal storage of the token value.
            /// </summary>
            private readonly string token;

            /// <summary>
            /// Initializes a new instance of the <see cref="MimeTypeToken"/> struct.
            /// This constructor is hidden from external callers. Use <see cref="Create(string)"/> instead.
            /// </summary>
            /// <param name="token">The token string to represent.</param>
            private MimeTypeToken(string token)
            {
                this.token = token;
            }

            /// <summary>
            /// Creates a new MIME Type token, validating for accepted characters.
            /// </summary>
            /// <param name="token">The token string to use to create a new instance.</param>
            /// <returns>If validation succeeds, a new token containing the given token string. Otherwise null.</returns>
            public static MimeTypeToken? Create(string token)
            {
                if (string.IsNullOrEmpty(token))
                {
                    return null;
                }

                Validation.RegularExpressionResult result = Validation.RegularExpressionEngine.Execute(token, RegularExpressionString.MimeTypeToken);
                if (!result.IsMatch())
                {
                    return null;
                }

                return new MimeTypeToken(token);
            }

            /// <summary>
            /// Overrides the default <see cref="ToString"/> implementation.
            /// </summary>
            /// <returns>The token string.</returns>
            public override string ToString()
            {
                return this.token;
            }
        }

        /// <summary>
        /// Pseudo-namespace class for the application mime-types.
        /// </summary>
        public static class Application
        {
            /// <summary>
            /// An application/csv instance.
            /// </summary>
            public static readonly MimeType Csv = new CsvImpl();

            /// <summary>
            /// An application/doc instance.
            /// </summary>
            public static readonly MimeType Doc = new DocImpl();

            /// <summary>
            /// An application/download instance.
            /// </summary>
            public static readonly MimeType Download = new DownloadImpl();

            /// <summary>
            /// An application/vnd.ms-excel instance.
            /// </summary>
            public static readonly MimeType Excel = new ExcelImpl();

            /// <summary>
            /// An application/json instance.
            /// </summary>
            public static readonly MimeType Json = new JsonImpl();

            /// <summary>
            /// An application/octet-stream instance.
            /// </summary>
            public static readonly MimeType OctetStream = new OctetStreamImpl();

            /// <summary>
            /// An application/x-ofx instance.
            /// </summary>
            public static readonly MimeType Ofx = new OfxImpl();

            /// <summary>
            /// An application/pdf instance.
            /// </summary>
            public static readonly MimeType Pdf = new PdfImpl();

            /// <summary>
            /// An application/vnd.openxmlformats-officedocument.spreadsheetml.sheet instance.
            /// </summary>
            public static readonly MimeType SpreadsheetXml = new SpreadsheetXmlImpl();

            /// <summary>
            /// An application/text instance.
            /// </summary>
            public static readonly MimeType Text = new TextImpl();

            /// <summary>
            /// An application/x-www-form-urlencoded instance.
            /// </summary>
            public static readonly MimeType UrlEncoded = new UrlEncodedImpl();

            /// <summary>
            /// An application/xhtml+xml instance.
            /// </summary>
            public static readonly MimeType Xhtml = new XhtmlImpl();

            /// <summary>
            /// An application/xml instance.
            /// </summary>
            public static readonly MimeType Xml = new XmlImpl();

            /// <summary>
            /// An application/zip instance.
            /// </summary>
            public static readonly MimeType Zip = new ZipImpl();

            /// <summary>
            /// Intermediate class to provide the common value of the MediaType.
            /// </summary>
            private abstract class ApplicationMimeType : MimeType
            {
                /// <summary>
                /// Initializes a new instance of the <see cref="ApplicationMimeType"/> class with the base "application" media-type.
                /// </summary>
                /// <param name="subType">The media sub-type to use for the full content type.</param>
                protected ApplicationMimeType(MimeTypeToken subType) : base(MimeTypeToken.Application, subType)
                { 
                }
            }

            /// <summary>
            /// Implementation of the application/csv mime-type.
            /// </summary>
            private sealed class CsvImpl : ApplicationMimeType
            {
                /// <summary>
                /// Initializes a new instance of the <see cref="CsvImpl"/> class representing the "application/csv" content type value.
                /// </summary>
                internal CsvImpl() : base(MimeTypeToken.Create("csv").Value)
                {
                }
            }

            /// <summary>
            /// Implementation of the application/doc mime-type.
            /// </summary>
            private sealed class DocImpl : ApplicationMimeType
            {
                /// <summary>
                /// Initializes a new instance of the <see cref="DocImpl"/> class representing the "application/doc" content type value.
                /// </summary>
                internal DocImpl() : base(MimeTypeToken.Create("doc").Value)
                {
                }
            }

            /// <summary>
            /// Implementation of the application/download mime-type.
            /// </summary>
            private sealed class DownloadImpl : ApplicationMimeType
            {
                /// <summary>
                /// Initializes a new instance of the <see cref="DownloadImpl"/> class representing the "application/download" content type value.
                /// </summary>
                internal DownloadImpl() : base(MimeTypeToken.Create("download").Value)
                {
                }
            }

            /// <summary>
            /// Implementation of the application/vnd.ms-excel mime-type.
            /// </summary>
            private sealed class ExcelImpl : ApplicationMimeType
            {
                /// <summary>
                /// Initializes a new instance of the <see cref="ExcelImpl"/> class representing the "application/vnd.ms-excel" content type value.
                /// </summary>
                internal ExcelImpl() : base(MimeTypeToken.Create("vnd.ms-excel").Value)
                {
                }
            }

            /// <summary>
            /// Implementation of the application/json mime-type.
            /// </summary>
            private sealed class JsonImpl : ApplicationMimeType
            {
                /// <summary>
                /// Initializes a new instance of the <see cref="JsonImpl"/> class representing the "application/json" content type value.
                /// </summary>
                internal JsonImpl() : base(MimeTypeToken.Create("json").Value)
                {
                }
            }

            /// <summary>
            /// Implementation of the application/octet-stream mime-type.
            /// </summary>
            private sealed class OctetStreamImpl : ApplicationMimeType
            {
                /// <summary>
                /// Initializes a new instance of the <see cref="OctetStreamImpl"/> class representing the "application/octet-stream" content type value.
                /// </summary>
                internal OctetStreamImpl() : base(MimeTypeToken.Create("octet-stream").Value)
                {
                }
            }

            /// <summary>
            /// Implementation of the application/x-ofx mime-type.
            /// </summary>
            private sealed class OfxImpl : ApplicationMimeType
            {
                /// <summary>
                /// Initializes a new instance of the <see cref="OfxImpl"/> class representing the "application/x-ofx" content type value.
                /// </summary>
                internal OfxImpl() : base(MimeTypeToken.Create("x-ofx").Value)
                {
                }
            }

            /// <summary>
            /// Implementation of the application/pdf mime-type.
            /// </summary>
            private sealed class PdfImpl : ApplicationMimeType
            {
                /// <summary>
                /// Initializes a new instance of the <see cref="PdfImpl"/> class representing the "application/pdf" content type value.
                /// </summary>
                internal PdfImpl() : base(MimeTypeToken.Create("pdf").Value)
                {
                }
            }

            /// <summary>
            /// Implementation of the application/vnd.openxmlformats-officedocument.spreadsheetml.sheet mime-type.
            /// </summary>
            private sealed class SpreadsheetXmlImpl : ApplicationMimeType
            {
                /// <summary>
                /// Initializes a new instance of the <see cref="SpreadsheetXmlImpl"/> class representing the "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" content type value.
                /// </summary>
                internal SpreadsheetXmlImpl() : base(MimeTypeToken.Create("vnd.openxmlformats-officedocument.spreadsheetml.sheet").Value)
                {
                }
            }

            /// <summary>
            /// Implementation of the application/text mime-type.
            /// </summary>
            private sealed class TextImpl : ApplicationMimeType
            {
                /// <summary>
                /// Initializes a new instance of the <see cref="TextImpl"/> class representing the "application/text" content type value.
                /// </summary>
                internal TextImpl() : base(MimeTypeToken.Create("text").Value)
                {
                }
            }

            /// <summary>
            /// Implementation of the application/x-www-form-urlencoded mime-type.
            /// </summary>
            private sealed class UrlEncodedImpl : ApplicationMimeType
            {
                /// <summary>
                /// Initializes a new instance of the <see cref="UrlEncodedImpl"/> class representing the "application/x-www-form-urlencoded" content type value.
                /// </summary>
                internal UrlEncodedImpl() : base(MimeTypeToken.Create("x-www-form-urlencoded").Value)
                {
                }
            }

            /// <summary>
            /// Implementation of the application/xhtml+xml mime-type.
            /// </summary>
            private sealed class XhtmlImpl : ApplicationMimeType
            {
                /// <summary>
                /// Initializes a new instance of the <see cref="XhtmlImpl"/> class representing the "application/xhtml+xml" content type value.
                /// </summary>
                internal XhtmlImpl() : base(MimeTypeToken.Create("xhtml+xml").Value)
                {
                }
            }

            /// <summary>
            /// Implementation of the application/xml mime-type.
            /// </summary>
            private sealed class XmlImpl : ApplicationMimeType
            {
                /// <summary>
                /// Initializes a new instance of the <see cref="XmlImpl"/> class representing the "application/xml" content type value.
                /// </summary>
                internal XmlImpl() : base(MimeTypeToken.Create("xml").Value)
                {
                }
            }

            /// <summary>
            /// Implementation of the application/zip mime-type.
            /// </summary>
            private sealed class ZipImpl : ApplicationMimeType
            {
                /// <summary>
                /// Initializes a new instance of the <see cref="ZipImpl"/> class representing the "application/zip" content type value.
                /// </summary>
                internal ZipImpl() : base(MimeTypeToken.Create("zip").Value)
                {
                }
            }
        }

        /// <summary>
        /// Pseudo-namespace class for the image mime-types.
        /// </summary>
        public static class Image
        {
            /// <summary>
            /// An image/gif instance.
            /// </summary>
            public static readonly MimeType Gif = new GifImpl();

            /// <summary>
            /// An image/jpeg instance.
            /// </summary>
            public static readonly MimeType Jpeg = new JpegImpl();

            /// <summary>
            /// An image/png instance.
            /// </summary>
            public static readonly MimeType Png = new PngImpl();

            /// <summary>
            /// Intermediate class to provide the common value of the MediaType.
            /// </summary>
            private abstract class ImageMimeType : MimeType
            {
                /// <summary>
                /// Initializes a new instance of the <see cref="ImageMimeType"/> class with the base "image" media-type.
                /// </summary>
                /// <param name="subType">The media sub-type to use for the full content type.</param>
                protected ImageMimeType(MimeTypeToken subType) : base(MimeTypeToken.Create("image").Value, subType)
                {
                }
            }

            /// <summary>
            /// Implementation of the image/gif mime-type.
            /// </summary>
            private sealed class GifImpl : ImageMimeType
            {
                /// <summary>
                /// Initializes a new instance of the <see cref="GifImpl"/> class representing the "application/gif" content type value.
                /// </summary>
                internal GifImpl() : base(MimeTypeToken.Create("gif").Value)
                {
                }
            }

            /// <summary>
            /// Implementation of the image/jpeg mime-type.
            /// </summary>
            private sealed class JpegImpl : ImageMimeType
            {
                /// <summary>
                /// Initializes a new instance of the <see cref="JpegImpl"/> class representing the "application/jpeg" content type value.
                /// </summary>
                internal JpegImpl() : base(MimeTypeToken.Create("jpeg").Value)
                {
                }
            }

            /// <summary>
            /// Implementation of the image/png mime-type.
            /// </summary>
            private sealed class PngImpl : ImageMimeType
            {
                /// <summary>
                /// Initializes a new instance of the <see cref="PngImpl"/> class representing the "application/png" content type value.
                /// </summary>
                internal PngImpl() : base(MimeTypeToken.Create("png").Value)
                {
                }
            }
        }

        /// <summary>
        /// Pseudo-namespace class for the multipart mime-types.
        /// </summary>
        public static class MultiPart
        {
            /// <summary>
            /// Create an instance of the multipart/form-data mime-type with boundary value.
            /// </summary>
            /// <param name="boundary">The boundary value.</param>
            /// <returns>An instance of the multipart/form-data mime-type.</returns>
            public static MimeType CreateFormData(string boundary)
            {
                if (string.IsNullOrEmpty(boundary))
                {
                    throw new DeveloperException(ErrorMessage.SystemError);
                }

                return new FormDataImpl(boundary);
            }

            /// <summary>
            /// Create an instance of the multipart/related mime-type with boundary value.
            /// </summary>
            /// <param name="boundary">The boundary value.</param>
            /// <param name="type">The type parameter value. Ex. "application/x-ofx".</param>
            /// <param name="capitalize">Whether to capitalize "Multipart/Related".</param>
            /// <returns>An instance of the multipart/related mime-type.</returns>
            public static MimeType CreateRelated(string boundary, string type = null, bool capitalize = false)
            {
                if (string.IsNullOrEmpty(boundary))
                {
                    throw new DeveloperException(ErrorMessage.SystemError);
                }

                return new RelatedImpl(boundary, type, capitalize);
            }

            /// <summary>
            /// Intermediate class to provide the common value of the MediaTypeLength.
            /// </summary>
            public class MultiPartMimeType : MimeType
            {
                /// <summary>
                /// Initializes a new instance of the <see cref="MultiPartMimeType"/> class with the base "multipart" media-type.
                /// Includes the given boundary parameter.
                /// </summary>
                /// <param name="subType">The media sub-type to use for the full content type.</param>
                /// <param name="boundary">The boundary parameter for the multipart content.</param>
                public MultiPartMimeType(MimeTypeToken subType, string boundary) : base(MimeTypeToken.Create("multipart").Value, subType)
                {
                    this.Parameters.Add(new MimeTypeParameter(MimeTypeToken.Create("boundary").Value, boundary));
                }
            }

            /// <summary>
            /// Implementation of the multi-part/form-data mime-type.
            /// </summary>
            private sealed class FormDataImpl : MultiPartMimeType
            {
                /// <summary>
                /// Initializes a new instance of the <see cref="FormDataImpl"/> class.
                /// </summary>
                /// <param name="boundary">The boundary value.</param>
                public FormDataImpl(string boundary) : base(MimeTypeToken.Create("form-data").Value, boundary)
                {
                }
            }

            /// <summary>
            /// Implementation of the multi-part/related mime-type.
            /// </summary>
            private sealed class RelatedImpl : MultiPartMimeType
            {
                /// <summary>
                /// Initializes a new instance of the <see cref="RelatedImpl"/> class.
                /// </summary>
                /// <param name="boundary">The boundary value.</param>
                /// <param name="type">The type parameter value. Ex. "application/ofx".</param>
                /// <param name="capitalize">Whether to capitalize "Multipart/Related".</param>
                public RelatedImpl(string boundary, string type, bool capitalize = false) : base(MimeTypeToken.Create("related").Value, boundary)
                {
                    // Equifax doesn't accept "mulipart/related", only accepts "Multipart/Related" capitalized.
                    if (capitalize)
                    {
                        this.MediaType = MimeTypeToken.Create("Multipart").Value;
                        this.SubMediaType = MimeTypeToken.Create("Related").Value;
                    }

                    if (!string.IsNullOrEmpty(type))
                    {
                        this.Parameters.Add(new MimeTypeParameter(MimeTypeToken.Create("type").Value, type));
                    }
                }
            }
        }

        /// <summary>
        /// Pseudo-namespace class for the text mime-types.
        /// </summary>
        public static class Text
        {
            /// <summary>
            /// A text/comma-separated-values instance.
            /// </summary>
            public static readonly MimeType CommaSeparatedValues = new CommasImpl();

            /// <summary>
            /// A text/csv instance.
            /// </summary>
            public static readonly MimeType Csv = new CsvImpl();

            /// <summary>
            /// A text/html instance.
            /// </summary>
            public static readonly MimeType Html = new HtmlImpl();

            /// <summary>
            /// A text/javascript instance.
            /// </summary>
            public static readonly MimeType Javascript = new JavascriptImpl();

            /// <summary>
            /// A text/plain instance.
            /// </summary>
            public static readonly MimeType Plain = new PlainImpl();

            /// <summary>
            /// A text/xml instance.
            /// </summary>
            public static readonly MimeType Xml = new XmlImpl();

            /// <summary>
            /// Intermediate class to provide the common value of the MediaType.
            /// </summary>
            private abstract class TextMimeType : MimeType
            {
                /// <summary>
                /// Initializes a new instance of the <see cref="TextMimeType"/> class with the base "text" media-type.
                /// </summary>
                /// <param name="subType">The media sub-type to use for the full content type.</param>
                protected TextMimeType(MimeTypeToken subType) : base(MimeTypeToken.Create("text").Value, subType)
                {
                }
            }

            /// <summary>
            /// Implementation of the text/comma-separated-values mime-type.
            /// </summary>
            private sealed class CommasImpl : TextMimeType
            {
                /// <summary>
                /// Initializes a new instance of the <see cref="CommasImpl"/> class representing the "application/comma-separated-values" content type value.
                /// </summary>
                internal CommasImpl() : base(MimeTypeToken.Create("comma-separated-values").Value)
                {
                }
            }

            /// <summary>
            /// Implementation of the text/csv mime-type.
            /// </summary>
            private sealed class CsvImpl : TextMimeType
            {
                /// <summary>
                /// Initializes a new instance of the <see cref="CsvImpl"/> class representing the "application/csv" content type value.
                /// </summary>
                internal CsvImpl() : base(MimeTypeToken.Create("csv").Value)
                {
                }
            }

            /// <summary>
            /// Implementation of the text/html mime-type.
            /// </summary>
            private sealed class HtmlImpl : TextMimeType
            {
                /// <summary>
                /// Initializes a new instance of the <see cref="HtmlImpl"/> class representing the "application/html" content type value.
                /// </summary>
                internal HtmlImpl() : base(MimeTypeToken.Create("html").Value)
                {
                }
            }

            /// <summary>
            /// Implementation of the text/javascript mime-type.
            /// </summary>
            private sealed class JavascriptImpl : TextMimeType
            {
                /// <summary>
                /// Initializes a new instance of the <see cref="JavascriptImpl"/> class representing the "application/javascript" content type value.
                /// </summary>
                internal JavascriptImpl() : base(MimeTypeToken.Create("javascript").Value)
                {
                }
            }

            /// <summary>
            /// Implementation of the text/plain mime-type.
            /// </summary>
            private sealed class PlainImpl : TextMimeType
            {
                /// <summary>
                /// Initializes a new instance of the <see cref="PlainImpl"/> class representing the "application/plain" content type value.
                /// </summary>
                internal PlainImpl() : base(MimeTypeToken.Create("plain").Value)
                {
                }
            }

            /// <summary>
            /// Implementation of the text/xml mime-type.
            /// </summary>
            private sealed class XmlImpl : TextMimeType
            {
                /// <summary>
                /// Initializes a new instance of the <see cref="XmlImpl"/> class representing the "application/xml" content type value.
                /// </summary>
                internal XmlImpl() : base(MimeTypeToken.Create("xml").Value)
                {
                }
            }
        }

        /// <summary>
        /// Implementation of the */* mime-type.
        /// </summary>
        private sealed class WildCardImpl : MimeType
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="WildCardImpl"/> class.
            /// </summary>
            public WildCardImpl() : base(MimeTypeToken.Create("*").Value, MimeTypeToken.Create("*").Value)
            {
            }

            /// <summary>
            /// Initializes a new instance of the <see cref="WildCardImpl"/> class.
            /// </summary>
            /// <param name="quality">The optional quality clause.</param>
            public WildCardImpl(string quality) : this()
            {
                if (quality != null && quality.Length == 3 && quality.StartsWith("0.") && char.IsDigit(quality[2]))
                {
                    this.Parameters.Add(new MimeTypeParameter(MimeTypeToken.Create("q").Value, quality));
                }
                else
                {
                    throw new DeveloperException(ErrorMessage.SystemError);
                }
            }
        }
    }
}
