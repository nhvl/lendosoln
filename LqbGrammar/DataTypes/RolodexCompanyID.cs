﻿namespace LqbGrammar.DataTypes
{
    using LqbGrammar.Validation;

    /// <summary>
    /// Encapsulates and validates a rolodex company id.
    /// </summary>
    public struct RolodexCompanyID : System.IEquatable<RolodexCompanyID>
    {
        /// <summary>
        /// The rolodex company id.
        /// </summary>
        private string rolodexCompanyId;

        /// <summary>
        /// Initializes a new instance of the <see cref="RolodexCompanyID"/> struct.
        /// </summary>
        /// <param name="rolodexCompanyId">The rolodex company id.</param>
        private RolodexCompanyID(string rolodexCompanyId)
        {
            this.rolodexCompanyId = rolodexCompanyId;
        }

        /// <summary>
        /// Validate and add rolodex company id.
        /// </summary>
        /// <param name="rolodexCompanyId">The rolodex company id.</param>
        /// <returns>The validated rolodex company id or null.</returns>
        public static RolodexCompanyID? Create(string rolodexCompanyId)
        {
            if (string.IsNullOrEmpty(rolodexCompanyId))
            {
                return null;
            }

            RegularExpressionResult result = RegularExpressionEngine.Execute(rolodexCompanyId, RegularExpressionString.RolodexCompanyID);
            if (!result.IsMatch())
            {
                return null;
            }

            return new RolodexCompanyID(rolodexCompanyId);
        }

        /// <summary>
        /// Compare two objects for equality and they are both the same rolodex company id.
        /// </summary>
        /// <param name="lhs">The object on the left side of the == operator.</param>
        /// <param name="rhs">The object on the right side of the == operator.</param>
        /// <returns>True if both objects are instances of the RolodexCompanyId class and they are equal to each other, false otherwise.</returns>
        public static new bool Equals(object lhs, object rhs)
        {
            if (!(lhs is RolodexCompanyID))
            {
                return false;
            }

            if (!(rhs is RolodexCompanyID))
            {
                return false;
            }

            return ((RolodexCompanyID)lhs).Equals((RolodexCompanyID)rhs);
        }

        /// <summary>
        /// Implement the equality operator for a validated rolodex company id.
        /// </summary>
        /// <param name="lhs">Instance on the left side of the == operator.</param>
        /// <param name="rhs">Instance on the right side of the == operator.</param>
        /// <returns>True if the instances are equal, false otherwise.</returns>
        public static bool operator ==(RolodexCompanyID lhs, RolodexCompanyID rhs)
        {
            return lhs.Equals(rhs);
        }

        /// <summary>
        /// Implement the inequality operator for a validated rolodex company id.
        /// </summary>
        /// <param name="lhs">The instance on the left side of the != operator.</param>
        /// <param name="rhs">The instance on the right side of the != operator.</param>
        /// <returns>Return true if the two instances are not equal, false otherwise.</returns>
        public static bool operator !=(RolodexCompanyID lhs, RolodexCompanyID rhs)
        {
            return !lhs.Equals(rhs);
        }

        /// <summary>
        /// Returns the rolodex company id as a string.
        /// </summary>
        /// <returns>The rolodex company id as a string.</returns>
        public override string ToString()
        {
            return this.rolodexCompanyId;
        }

        /// <summary>
        /// Compare this instance with another instance for equality.
        /// </summary>
        /// <param name="other">The other instance.</param>
        /// <returns>True if the two instances are equal, false otherwise.</returns>
        public bool Equals(RolodexCompanyID other)
        {
            return this.rolodexCompanyId == other.rolodexCompanyId;
        }

        /// <summary>
        /// Compares this instance with another object for equality.
        /// </summary>
        /// <param name="obj">The other object.</param>
        /// <returns>True if the other object is a valid phone number and the two objects have the same value.</returns>
        public override bool Equals(object obj)
        {
            if (!(obj is RolodexCompanyID))
            {
                return false;
            }

            return Equals((RolodexCompanyID)obj);
        }

        /// <summary>
        /// Override the implementation inherited from the Object class.
        /// </summary>
        /// <returns>An integer that is the hash of this instance.</returns>
        public override int GetHashCode()
        {
            return this.rolodexCompanyId.GetHashCode();
        }
    }
}
