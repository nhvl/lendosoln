﻿namespace LqbGrammar.DataTypes
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Represents a U.S. state or other major subdivision of the U.S., via the two-letter abbreviation.<para/>
    /// This definition intentionally excludes U.S.P.S. delivery codes, because this is intended to be an actual
    /// U.S. location, rather than simply a mailing designation. (I'm looking at you, Armed Forces Canada).
    /// </summary>
    /// <remarks>
    /// The state abbreviation must be uppercase to be valid.
    /// </remarks>
    public partial struct UnitedStatesState : ISemanticType
    {
        /// <summary>
        /// Defines the recognized U.S. states.
        /// </summary>
        private static readonly HashSet<string> RecognizedStates = new HashSet<string>()
        {
            "AK", // Alaska
            "AL", // Alabama
            "AR", // Arkansas
            "AS", // American Samoa
            "AZ", // Arizona
            "CA", // California
            "CO", // Colorado
            "CT", // Connecticut
            "DC", // District Of Columbia
            "DE", // Delaware
            "FL", // Florida
            "GA", // Georgia
            "GU", // Guam
            "HI", // Hawaii
            "IA", // Iowa
            "ID", // Idaho
            "IL", // Illinois
            "IN", // Indiana
            "KS", // Kansas
            "KY", // Kentucky
            "LA", // Louisiana
            "MA", // Massachusetts
            "MD", // Maryland
            "ME", // Maine
            "MI", // Michigan
            "MN", // Minnesota
            "MO", // Missouri
            "MP", // Northern Mariana Islands
            "MS", // Mississippi
            "MT", // Montana
            "NC", // North Carolina
            "ND", // North Dakota
            "NE", // Nebraska
            "NH", // New Hampshire
            "NJ", // New Jersey
            "NM", // New Mexico
            "NV", // Nevada
            "NY", // New York
            "OH", // Ohio
            "OK", // Oklahoma
            "OR", // Oregon
            "PA", // Pennsylvania
            "PR", // Puerto Rico
            "RI", // Rhode Island
            "SC", // South Carolina
            "SD", // South Dakota
            "TN", // Tennessee
            "TX", // Texas
            "UT", // Utah
            "VA", // Virginia
            "VI", // Virgin Islands
            "VT", // Vermont
            "WA", // Washington
            "WI", // Wisconsin
            "WV", // West Virginia
            "WY", // Wyoming
        };

        /// <summary>
        /// Gets the two-letter abbreviation for the state.
        /// </summary>
        /// <value>The two-letter abbreviation for the state.</value>
        public string StateAbbreviation => this.Value;

        /// <summary>
        /// Gets a value indicating whether the specified value is a valid U.S. state.
        /// </summary>
        /// <param name="value">The value to validate.</param>
        /// <returns><see langword="true"/>if the value is a valid U.S. state. Otherwise, false.</returns>
        private static bool PassesAdvancedValidation(string value)
        {
            return RecognizedStates.Contains(value);
        }
    }
}
