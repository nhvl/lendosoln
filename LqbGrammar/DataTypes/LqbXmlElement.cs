﻿namespace LqbGrammar.DataTypes
{
    using System.Xml;
    using System.Xml.Linq;

    /// <summary>
    /// Wrapper around a library reference to an XML element object.
    /// Currently the LQB code works with the Linq XML library so this
    /// class accepts and contains an XElement instance.  If this 
    /// ever changes then we can modify this class and continue using 
    /// the driver/adapter interfaces as-is.  Ideally, we would want to 
    /// wrap up the formulation of XML as well so client code would only 
    /// work with LQB data types.  However, there is a lot of work to 
    /// do that and we would then lose the ability to work seemlessly 
    /// within the Linq libraries.  That is a very high cost and not worth 
    /// the very tiny benefit of protecting client code from the native 
    /// XML libraries.  Then the question arises as to why even have this 
    /// wrapper class then?  The answer is that we may wish to impose 
    /// validation beyond what the native library gives.
    /// </summary>
    public struct LqbXmlElement
    {
        /// <summary>
        /// An instance of a bad LqbXmlElement that can be used for variable initialization.
        /// </summary>
        public static readonly LqbXmlElement BadXML = new LqbXmlElement(null);

        /// <summary>
        /// Initializes a new instance of the <see cref="LqbXmlElement"/> struct.
        /// </summary>
        /// <param name="element">The contained XML element object.</param>
        private LqbXmlElement(XElement element)
        {
            this.Contained = element;
        }

        /// <summary>
        /// Gets the contained XML element.
        /// </summary>
        /// <value>The contained XML element.</value>
        public XElement Contained
        {
            get; private set;
        }

        /// <summary>
        /// Wrap the input XML element object.
        /// </summary>
        /// <param name="test">The input XML element object.</param>
        /// <returns>The wrapped XML element object, or null if the input is not valid.</returns>
        public static LqbXmlElement? Create(XElement test)
        {
            if (!Validate(test))
            {
                return null;
            }
            else
            {
                return new LqbXmlElement(test);
            }
        }

        /// <summary>
        /// Parse and wrap the input XML string.
        /// </summary>
        /// <param name="test">The input XML string.</param>
        /// <returns>The wrapped XML element object, or null if the input is not valid XML.</returns>
        public static LqbXmlElement? Create(string test)
        {
            try
            {
                XElement elem = XElement.Parse(test, LoadOptions.PreserveWhitespace);
                return new LqbXmlElement(elem);
            }
            catch (XmlException)
            {
                return null;
            }
        }

        /// <summary>
        /// Validate the XML element object.
        /// </summary>
        /// <param name="test">An instance of an XML element object.</param>
        /// <returns>True if the input XML element object is valid, false otherwise.</returns>
        private static bool Validate(XElement test)
        {
            // In the future we may want do some checking
            // if the .net library permits errors.  For example,
            // XmlDocument.CreateElement() allows invalid NCNames.
            return test != null;
        }
    }
}
