﻿namespace LqbGrammar.DataTypes
{
    using System;

    /// <summary>
    /// Represents data that is base64 encoded.
    /// </summary>
    public struct Base64EncodedData : IEquatable<Base64EncodedData>
    {
        /// <summary>
        /// An invalid value that can be used for initialization.
        /// </summary>
        public static readonly Base64EncodedData Invalid = new Base64EncodedData(string.Empty);

        /// <summary>
        /// Initializes a new instance of the <see cref="Base64EncodedData"/> struct.
        /// </summary>
        /// <param name="encodedData">The base64 encoded data.</param> 
        private Base64EncodedData(string encodedData)
        {
            this.Value = encodedData;
        }

        /// <summary>
        /// Gets the absolute value.
        /// </summary>
        /// <value>The absolute base64 encoded data.</value>
        public string Value { get; private set; }

        /// <summary>
        /// The == operator is implemented here.
        /// </summary>
        /// <param name="lhs">Instance on the left of the == operator.</param>
        /// <param name="rhs">Instance on the right of the == operator.</param>
        /// <returns>True if the two instances are equal, false otherwise.</returns>
        public static bool operator ==(Base64EncodedData lhs, Base64EncodedData rhs)
        {
            return lhs.Equals(rhs);
        }

        /// <summary>
        /// The != operator is implemented here.
        /// </summary>
        /// <param name="lhs">Instance on the left of the != operator.</param>
        /// <param name="rhs">Instance on the right of the != operator.</param>
        /// <returns>True if the two instances are not equal, false otherwise.</returns>
        public static bool operator !=(Base64EncodedData lhs, Base64EncodedData rhs)
        {
            return !lhs.Equals(rhs);
        }

        /// <summary>
        /// Validate the input data.
        /// </summary>
        /// <param name="encodedData">The data to validate.</param>
        /// <returns>The valid base64 encoded data, or null.</returns>
        public static Base64EncodedData? Create(string encodedData)
        {
            if (IsValidEncodedData(encodedData?.Trim()))
            {
                return new Base64EncodedData(encodedData);
            }
            
            return null;
        }

        /// <summary>
        /// Implementation of the IEquatable interface.
        /// </summary>
        /// <param name="other">Instance that is compared to this instance for equality.</param>
        /// <returns>True if the instances are equal, false otherwise.</returns>
        public bool Equals(Base64EncodedData other)
        {
            return this.Value.Equals(other.Value, StringComparison.OrdinalIgnoreCase);
        }

        /// <summary>
        /// Override the Equals method inherited from the Object class.
        /// </summary>
        /// <param name="obj">Instance that is checked for equality with this instance.</param>
        /// <returns>True if obj is equal to this instance, false otherwise.</returns>
        public override bool Equals(object obj)
        {
            if (!(obj is Base64EncodedData))
            {
                return false;
            }

            return this.Equals((Base64EncodedData)obj);
        }

        /// <summary>
        /// Override of the method inherited from the Object class.
        /// </summary>
        /// <returns>An integer that is the hash code for this instance.</returns>
        public override int GetHashCode()
        {
            return this.Value.GetHashCode();
        }

        /// <summary>
        /// Determines whether the specified encoded data is valid.
        /// </summary>
        /// <param name="encodedData">
        /// The data to validate.
        /// </param>
        /// <returns>
        /// True if the encoded data is valid, false otherwise.
        /// </returns>
        /// <remarks>
        /// Validation logic adapted from https://stackoverflow.com/a/23955827.
        /// </remarks>
        private static bool IsValidEncodedData(string encodedData)
        {
            if (encodedData == null || encodedData.Length % 4 != 0)
            {
                return false;
            }

            // Empty is a legal value that decodes to empty value per RFC 4648.
            if (encodedData == string.Empty)
            {
                return true;
            }

            var terminalIndex = encodedData.Length - 1;
            
            // Adjust index for trailing padding characters.
            // Any other padding characters in the string
            // after this adjustment are illegal.
            if (encodedData[terminalIndex] == '=')
            {
                terminalIndex--;
            }
            
            if (encodedData[terminalIndex] == '=')
            {
                terminalIndex--;
            }

            for (var i = 0; i <= terminalIndex; ++i)
            {
                if (!IsValidBase64Character(encodedData[i]))
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Determines whether the specified character is a valid base 64 character.
        /// </summary>
        /// <param name="value">
        /// The character to validate.
        /// </param>
        /// <returns>
        /// True if the character is a valid base 64 character, false otherwise.
        /// </returns>
        private static bool IsValidBase64Character(char value)
        {
            return (value >= 'A' && value <= 'Z')
                || (value >= 'a' && value <= 'z')
                || (value >= '0' && value <= '9')
                || value == '+'
                || value == '/';
        }
    }
}
