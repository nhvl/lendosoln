﻿namespace LqbGrammar.DataTypes
{
    using System;

    /// <summary>
    /// Encapsulate the concept of a quantity (possibly negative) of an amount of units.
    /// </summary>
    /// <typeparam name="T">The type of the units being counted.</typeparam>
    public struct Quantity<T> : ISemanticType, IComparable, IComparable<Quantity<T>>, IEquatable<Quantity<T>>
        where T : UnitType
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Quantity{T}"/> struct.
        /// </summary>
        /// <param name="value">The amount of units.</param>
        private Quantity(decimal value)
        {
            this.Value = value;
        }

        /// <summary>
        /// Gets the underlying decimal value as an object.
        /// </summary>
        object ISemanticType.Value => this.Value;

        /// <summary>
        /// Gets the amount of units.
        /// </summary>
        /// <value>The amount of units.</value>
        public decimal Value { get; private set; }

        /// <summary>
        /// When a quantity value is received in text format, this method can be used to recover the quantity.
        /// </summary>
        /// <param name="value">The quantity value in text format.</param>
        /// <returns>The true quantity value, or null if there is a format error.</returns>
        public static Quantity<T>? Create(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return null;
            }

            decimal result = 0;
            if (!decimal.TryParse(value, out result))
            {
                return null;
            }

            return new Quantity<T>(result);
        }

        /// <summary>
        /// When a quantity value is received as a decimal, this method can be used to recover the quantity.
        /// </summary>
        /// <param name="value">The quantity value as a decimal.</param>
        /// <returns>The true quantity value, should never be null but the signature allows it for consistency across all semantic data types.</returns>
        public static Quantity<T>? Create(decimal value)
        {
            return (Quantity<T>)value;
        }

        /// <summary>
        /// Cast a decimal to a quantity.
        /// </summary>
        /// <param name="value">The quantity value as a decimal.</param>
        public static explicit operator Quantity<T>(decimal value)
        {
            return new Quantity<T>(value);
        }

        /// <summary>
        /// Cast a quantity to a decimal.
        /// </summary>
        /// <param name="quantity">The quantity to be cast.</param>
        public static explicit operator decimal(Quantity<T> quantity)
        {
            return quantity.Value;
        }

        /// <summary>
        /// Addition operator for two quantities.
        /// </summary>
        /// <param name="lhs">The first operand.</param>
        /// <param name="rhs">The second operand.</param>
        /// <returns>The sum of the two quantities.</returns>
        public static Quantity<T> operator +(Quantity<T> lhs, Quantity<T> rhs)
        {
            return new Quantity<T>(lhs.Value + rhs.Value);
        }

        /// <summary>
        /// Difference operator for two quantities.
        /// </summary>
        /// <param name="lhs">The first operand.</param>
        /// <param name="rhs">The second operand.</param>
        /// <returns>The difference between two quantities.</returns>
        public static Quantity<T> operator -(Quantity<T> lhs, Quantity<T> rhs)
        {
            return new Quantity<T>(lhs.Value - rhs.Value);
        }

        /// <summary>
        /// Equality operator for two quantities.
        /// </summary>
        /// <param name="lhs">The first operand.</param>
        /// <param name="rhs">The second operand.</param>
        /// <returns>True if the two quantities match, false otherwise.</returns>
        public static bool operator ==(Quantity<T> lhs, Quantity<T> rhs)
        {
            return lhs.CompareTo(rhs) == 0;
        }

        /// <summary>
        /// The inequality operator for two quantities.
        /// </summary>
        /// <param name="lhs">The first operand.</param>
        /// <param name="rhs">The second operand.</param>
        /// <returns>False if the two quantities match, true otherwise.</returns>
        public static bool operator !=(Quantity<T> lhs, Quantity<T> rhs)
        {
            return lhs.CompareTo(rhs) != 0;
        }

        /// <summary>
        /// The less than operator for two quantities.
        /// </summary>
        /// <param name="lhs">The first operand.</param>
        /// <param name="rhs">The second operand.</param>
        /// <returns>True if the inequality operator is satisfied, false otherwise.</returns>
        public static bool operator <(Quantity<T> lhs, Quantity<T> rhs)
        {
            return lhs.CompareTo(rhs) < 0;
        }

        /// <summary>
        /// The greater than operator for two quantities.
        /// </summary>
        /// <param name="lhs">The first operand.</param>
        /// <param name="rhs">The second operand.</param>
        /// <returns>True if the inequality operator is satisfied, false otherwise.</returns>
        public static bool operator >(Quantity<T> lhs, Quantity<T> rhs)
        {
            return lhs.CompareTo(rhs) > 0;
        }

        /// <summary>
        /// The less than or equals operator for two quantities.
        /// </summary>
        /// <param name="lhs">The first operand.</param>
        /// <param name="rhs">The second operand.</param>
        /// <returns>True if the inequality operator is satisfied, false otherwise.</returns>
        public static bool operator <=(Quantity<T> lhs, Quantity<T> rhs)
        {
            return lhs.CompareTo(rhs) <= 0;
        }

        /// <summary>
        /// The greater than or equals operator for two quantities.
        /// </summary>
        /// <param name="lhs">The first operand.</param>
        /// <param name="rhs">The second operand.</param>
        /// <returns>True if the inequality operator is satisfied, false otherwise.</returns>
        public static bool operator >=(Quantity<T> lhs, Quantity<T> rhs)
        {
            return lhs.CompareTo(rhs) >= 0;
        }

        /// <summary>
        /// Determine when another quantity matches this quantity.
        /// </summary>
        /// <param name="other">The other quantity.</param>
        /// <returns>True if the two quantities match, false otherwise.</returns>
        public bool Equals(Quantity<T> other)
        {
            return this.CompareTo(other) == 0;
        }

        /// <summary>
        /// If an object is a comparable quantity, determine whether it matches this quantity.
        /// </summary>
        /// <param name="other">The other object.</param>
        /// <returns>True if the other object is a comparable quantity and it matches this quantity.</returns>
        public override bool Equals(object other)
        {
            return this.CompareTo(other) == 0;
        }

        /// <summary>
        /// If an object is a comparable quantity, determine the comparison result.
        /// </summary>
        /// <param name="other">The other object.</param>
        /// <returns>The comparison result for the two comparable quantities, or throws an ArgumentExeption if the two are not comparable.</returns>
        public int CompareTo(object other)
        {
            if (other is Quantity<T>)
            {
                return this.CompareTo((Quantity<T>)other);
            }
            else
            {
                throw new ArgumentException("Input parameter is not a comparable quantity.");
            }
        }

        /// <summary>
        /// Calculate the comparison result between two quantities.
        /// </summary>
        /// <param name="other">The other quantity which is compared to this quantity.</param>
        /// <returns>The comparison result.</returns>
        public int CompareTo(Quantity<T> other)
        {
            return this.Value.CompareTo(other.Value);
        }

        /// <summary>
        /// Calculate a hash value for this quantity.
        /// </summary>
        /// <returns>A hash value.</returns>
        public override int GetHashCode()
        {
            return this.Value.GetHashCode();
        }

        /// <summary>
        /// Retrieve a string representation of this quantity.
        /// </summary>
        /// <returns>A string representation of this quantity.</returns>
        public override string ToString()
        {
            return this.Value.ToString();
        }
    }
}
