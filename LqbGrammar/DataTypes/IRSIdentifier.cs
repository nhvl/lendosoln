﻿namespace LqbGrammar.DataTypes
{
    using System;
    using Validation;

    /// <summary>
    /// Container for either a social security number of an employee id number.
    /// </summary>
    public struct IRSIdentifier : ISemanticType, IEquatable<IRSIdentifier>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="IRSIdentifier"/> struct.
        /// </summary>
        /// <param name="value">The value of the identifier.</param>
        /// <param name="isSSN">True if the value is a social security number, false if an EIN, indeterminant otherwise.</param>
        private IRSIdentifier(string value, BooleanKleene isSSN)
        {
            this.Value = value;
            this.IsSSN = isSSN;
        }

        /// <summary>
        /// Gets a value indicating whether this instance contains a social security number.
        /// </summary>
        /// <value>A value indicating whether this instance contains a social security number.</value>
        public BooleanKleene IsSSN { get; private set; }

        /// <summary>
        /// Gets the value.
        /// </summary>
        object ISemanticType.Value => this.Value;

        /// <summary>
        /// Gets or sets the contained identifier.
        /// </summary>
        /// <value>The contained identifier.</value>
        private string Value { get; set; }

        /// <summary>
        /// Create an instance of the IRSIdentifier struct when the input value is correctly formatted.
        /// </summary>
        /// <param name="value">A string representation of an IRS identifier.</param>
        /// <returns>An instance of the IRSIdentifier struct when the input value is correctly formatted, otherwise null.</returns>
        public static IRSIdentifier? Create(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return null;
            }

            var result = RegularExpressionEngine.Execute(value, RegularExpressionString.SocialSecurityNumber);
            if (result.IsMatch())
            {
                if (value.Length == 11)
                {
                    return new IRSIdentifier(value, BooleanKleene.True);
                }
                else if (value.Length == 9)
                {
                    return new IRSIdentifier(value, BooleanKleene.Indeterminant);
                }
            }

            result = RegularExpressionEngine.Execute(value, RegularExpressionString.EmployerTaxIdentificationNumber);
            if (result.IsMatch())
            {
                if (value.Length == 10)
                {
                    return new IRSIdentifier(value, BooleanKleene.False);
                }
                else if (value.Length == 9)
                {
                    return new IRSIdentifier(value, BooleanKleene.Indeterminant);
                }
            }

            return null;
        }

        /// <summary>
        /// Cast an IRSIdentifier to a SocialSecurityNumber.
        /// </summary>
        /// <param name="id">The IRSIdentifier.</param>
        public static explicit operator SocialSecurityNumber(IRSIdentifier id)
        {
            if (id.IsSSN != BooleanKleene.False)
            {
                var test = SocialSecurityNumber.Create(id.Value);
                if (test != null)
                {
                    return test.Value;
                }
            }

            throw new InvalidCastException("Cannot cast this instance of IRSIdentifier to a SocialSecurityNumber.");
        }

        /// <summary>
        /// Cast a SocialSecurityNumber to an IRSIdentifier.
        /// </summary>
        /// <param name="id">The SocialSecurityNumber.</param>
        public static implicit operator IRSIdentifier(SocialSecurityNumber id)
        {
            return new IRSIdentifier(id.UnmaskedSsn, BooleanKleene.True);
        }

        /// <summary>
        /// Cast an IRSIdentifier to an EmployerTaxIdentificationNumber.
        /// </summary>
        /// <param name="id">The IRSIdentifier.</param>
        public static explicit operator EmployerTaxIdentificationNumber(IRSIdentifier id)
        {
            if (id.IsSSN != BooleanKleene.True)
            {
                var test = EmployerTaxIdentificationNumber.Create(id.Value);
                if (test != null)
                {
                    return test.Value;
                }
            }

            throw new InvalidCastException("Cannot cast this instance of IRSIdentifier to an EmployerTaxIdentificationNumber.");
        }

        /// <summary>
        /// Cast av EmployerTaxIdentificationNumber to an IRSIdentifier.
        /// </summary>
        /// <param name="id">The EmployerTaxIdentificationNumber.</param>
        public static implicit operator IRSIdentifier(EmployerTaxIdentificationNumber id)
        {
            return new IRSIdentifier(id.ToString(), BooleanKleene.False);
        }

        /// <summary>
        /// Compare two objects under the requirement that they must both be instances of the IRSIdentifier struct.
        /// </summary>
        /// <param name="lhs">First instance under comparison.</param>
        /// <param name="rhs">Second instance under comparison.</param>
        /// <returns>True if the two instances hold equivalent values, false otherwise.</returns>
        public static new bool Equals(object lhs, object rhs)
        {
            if (!(lhs is IRSIdentifier))
            {
                return false;
            }

            if (!(rhs is IRSIdentifier))
            {
                return false;
            }

            return ((IRSIdentifier)lhs).Equals((IRSIdentifier)rhs);
        }

        /// <summary>
        /// Equality operator for instances of the IRSIdentifier struct.
        /// </summary>
        /// <param name="lhs">First instance under comparison.</param>
        /// <param name="rhs">Second instance under comparison.</param>
        /// <returns>True if the two instances hold equivalent values, false otherwise.</returns>
        public static bool operator ==(IRSIdentifier lhs, IRSIdentifier rhs)
        {
            return lhs.Equals(rhs);
        }

        /// <summary>
        /// Inequality operator for instances of the IRSIdentifier struct.
        /// </summary>
        /// <param name="lhs">First instance under comparison.</param>
        /// <param name="rhs">Second instance under comparison.</param>
        /// <returns>True if the two instances do not hold equivalent values, false otherwise.</returns>
        public static bool operator !=(IRSIdentifier lhs, IRSIdentifier rhs)
        {
            return !lhs.Equals(rhs);
        }

        /// <summary>
        /// Compare this identifer with another to determine whether they hold equivalent values.
        /// </summary>
        /// <param name="other">The other instance being compared with this instance.</param>
        /// <returns>True if the two instances hold equivalent values, false otherwise.</returns>
        public bool Equals(IRSIdentifier other)
        {
            if (this.IsSSN == BooleanKleene.True && other.IsSSN == BooleanKleene.False)
            {
                return false;
            }
            else if (this.IsSSN == BooleanKleene.False && other.IsSSN == BooleanKleene.True)
            {
                return false;
            }

            string lhs = Canonicalize(this.Value);
            string rhs = Canonicalize(other.Value);
            return lhs == rhs;
        }

        /// <summary>
        /// Determine whether an object is of the correct type and has the same value as this instance.
        /// </summary>
        /// <param name="obj">The object that is being compared with this instance.</param>
        /// <returns>True if the object matches the type and value of this instance.</returns>
        public override bool Equals(object obj)
        {
            return (obj is IRSIdentifier) && this.Equals((IRSIdentifier)obj);
        }

        /// <summary>
        /// Calculate a hash code consistent with the equality semantics.
        /// </summary>
        /// <returns>The hash code.</returns>
        public override int GetHashCode()
        {
            string val = this.IsSSN.ToString() + Canonicalize(this.Value);
            return val.GetHashCode();
        }

        /// <summary>
        /// Retrieve the contained value.
        /// </summary>
        /// <returns>The contained value.</returns>
        public override string ToString()
        {
            return this.Value;
        }

        /// <summary>
        /// Strip dashes from the value so it is simply a string of 9 digits.
        /// </summary>
        /// <param name="input">The value to canonicalize.</param>
        /// <returns>The canonicalized value.</returns>
        private static string Canonicalize(string input)
        {
            return input.Replace("-", string.Empty);
        }
    }
}
