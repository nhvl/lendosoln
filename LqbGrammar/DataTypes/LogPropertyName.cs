﻿namespace LqbGrammar.DataTypes
{
    using System;
    using LqbGrammar.Validation;

    /// <summary>
    /// Encapsulate a log property name, with the format validated.
    /// </summary>
    public struct LogPropertyName : System.IEquatable<LogPropertyName>
    {
        /// <summary>
        /// This is the contained log property name.
        /// </summary>
        private string name;

        /// <summary>
        /// Initializes a new instance of the <see cref="LogPropertyName"/> struct.
        /// </summary>
        /// <param name="name">The log property name.</param>
        private LogPropertyName(string name)
        {
            this.name = name;
        }

        /// <summary>
        /// Validate and encapsulate a log property name.
        /// </summary>
        /// <param name="name">The log property name.</param>
        /// <returns>The encapsulated log property name if it is valid, or null.</returns>
        public static LogPropertyName? Create(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                return null;
            }

            RegularExpressionResult result = RegularExpressionEngine.Execute(name, RegularExpressionString.LogPropertyName);
            if (!result.IsMatch())
            {
                return null;
            }

            return new LogPropertyName(name);
        }

        /// <summary>
        /// Static method used for checking equality of two instances.
        /// </summary>
        /// <param name="lhs">Instance on the left side of the == operator.</param>
        /// <param name="rhs">Instance on the right side of the == operator.</param>
        /// <returns>True if the instances are equal, false otherwise.</returns>
        public static new bool Equals(object lhs, object rhs)
        {
            if (!(lhs is LogPropertyName))
            {
                return false;
            }

            if (!(rhs is LogPropertyName))
            {
                return false;
            }

            return ((LogPropertyName)lhs).Equals((LogPropertyName)rhs);
        }

        /// <summary>
        /// Implements the == operator.
        /// </summary>
        /// <param name="lhs">Instance on the left side of the == operator.</param>
        /// <param name="rhs">Instance on the right side of the == operator.</param>
        /// <returns>True if the instances are equal, false otherwise.</returns>
        public static bool operator ==(LogPropertyName lhs, LogPropertyName rhs)
        {
            return lhs.Equals(rhs);
        }

        /// <summary>
        /// Implements the != oeprator.
        /// </summary>
        /// <param name="lhs">Instance on the left side of the != operator.</param>
        /// <param name="rhs">Instance on the right side of the != operator.</param>
        /// <returns>True if the instances are not equal, false otherwise.</returns>
        public static bool operator !=(LogPropertyName lhs, LogPropertyName rhs)
        {
            return !lhs.Equals(rhs);
        }

        /// <summary>
        /// Overrides the method inherited from the Object class.
        /// </summary>
        /// <returns>The log property name.</returns>
        public override string ToString()
        {
            return this.name;
        }

        /// <summary>
        /// Implement the IEquatable interface.
        /// </summary>
        /// <param name="other">Instance that is compared with this instance for equality.</param>
        /// <returns>True if the instances are equal, false otherwise.</returns>
        public bool Equals(LogPropertyName other)
        {
            return string.Compare(this.name, other.name, StringComparison.OrdinalIgnoreCase) == 0;
        }

        /// <summary>
        /// Override of the method inherited from the Object class.
        /// </summary>
        /// <param name="obj">Instance that is compared with this instance for equality.</param>
        /// <returns>True if the instances are equal, false otherwise.</returns>
        public override bool Equals(object obj)
        {
            if (!(obj is LogPropertyName))
            {
                return false;
            }

            return Equals((LogPropertyName)obj);
        }

        /// <summary>
        /// Override the method inherited from the Object class.
        /// </summary>
        /// <returns>An integer that is the hash of this instance.</returns>
        public override int GetHashCode()
        {
            return this.name.GetHashCode();
        }
    }
}
