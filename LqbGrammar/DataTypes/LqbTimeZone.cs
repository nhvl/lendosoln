﻿namespace LqbGrammar.DataTypes
{
    using System;
    using LqbGrammar.Enumerations;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Time zone information for those time zones used within the LQB System.
    /// </summary>
    public sealed class LqbTimeZone : IEquatable<LqbTimeZone>
    {
        /// <summary>
        /// The standard LqbTimeZone object for the US Pacific Time Zone.
        /// </summary>
        public static readonly LqbTimeZone PacificTime = new LqbTimeZone(TimeZoneInfo.FindSystemTimeZoneById("Pacific Standard Time"), "Pacific Time", "PT", "PST", "PDT");

        /// <summary>
        /// The standard LqbTimeZone object for the US Mountain Time Zone.
        /// </summary>
        public static readonly LqbTimeZone MountainTime = new LqbTimeZone(TimeZoneInfo.FindSystemTimeZoneById("Mountain Standard Time"), "Mountain Time", "MT", "MST", "MDT");

        /// <summary>
        /// The standard LqbTimeZone object for the US Central Time Zone.
        /// </summary>
        public static readonly LqbTimeZone CentralTime = new LqbTimeZone(TimeZoneInfo.FindSystemTimeZoneById("Central Standard Time"), "Central Time", "CT", "CST", "CDT");

        /// <summary>
        /// The standard LqbTimeZone object for the US Eastern Time Zone.
        /// </summary>
        public static readonly LqbTimeZone EasternTime = new LqbTimeZone(TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time"), "Eastern Time", "ET", "EST", "EDT");

        /// <summary>
        /// The standard LqbTimeZone object for the US Hawaii Time Zone.
        /// Note that Hawaii does not observe daylight saving time.
        /// </summary>
        public static readonly LqbTimeZone HawaiiTime = new LqbTimeZone(TimeZoneInfo.FindSystemTimeZoneById("Hawaiian Standard Time"), "Hawaii Time", "HT", "HST", "HDT");

        /// <summary>
        /// The standard LqbTimeZone object for the US Alaska Time Zone.
        /// </summary>
        public static readonly LqbTimeZone AlaskaTime = new LqbTimeZone(TimeZoneInfo.FindSystemTimeZoneById("Alaskan Standard Time"), "Alaska Time", "AKT", "AKST", "AKDT");

        /// <summary>
        /// Initializes a new instance of the <see cref="LqbTimeZone"/> class.
        /// </summary>
        /// <param name="timezone">The associated framework library time zone.</param>
        /// <param name="generalName">The standard name for the timezone.</param>
        /// <param name="generalAbbrev">The general abbreviation for the timezone.</param>
        /// <param name="standardAbbrev">The standard abbreviation for the timezone.</param>
        /// <param name="daylightAbbrev">The daylight abbreviation for the timezone.</param>
        private LqbTimeZone(TimeZoneInfo timezone, string generalName, string generalAbbrev, string standardAbbrev, string daylightAbbrev)
        {
            this.Zone = timezone;
            this.GeneralName = generalName;
            this.GeneralAbbreviation = generalAbbrev;
            this.StandardAbbreviation = standardAbbrev;
            this.DaylightAbbreviation = daylightAbbrev;
        }

        /// <summary>
        /// Gets a value indicating whether this timezone supports daylight savings time.
        /// </summary>
        /// <value>A value indicating whether this timezone supports daylight savings time.</value>
        public bool SupportsDaylightSavingTime
        {
            get { return this.Zone.SupportsDaylightSavingTime; }
        }

        /// <summary>
        /// Gets the standard name for the timezone.
        /// </summary>
        /// <value>The standard name for the timezone.</value>
        public string GeneralName { get; private set; }

        /// <summary>
        /// Gets the general abbreviation (no daylight savings time indicator) for the timezone.
        /// </summary>
        /// <value>The general abbreviation for the timezone.</value>
        public string GeneralAbbreviation { get; private set; }

        /// <summary>
        /// Gets the standard abbreviation (indicator that daylight savings is not in effect) for the timezone.
        /// </summary>
        /// <value>The standard abbreviation for the timezone.</value>
        public string StandardAbbreviation { get; private set; }

        /// <summary>
        /// Gets the daylight abbreviation (indicator that daylight savings is in effect) for the timezone.
        /// </summary>
        /// <value>The daylight abbreviation for the timezone.</value>
        public string DaylightAbbreviation { get; private set; }

        /// <summary>
        /// Gets or sets the associated framework library time zone.
        /// </summary>
        /// <value>The associated framework library time zone.</value>
        private TimeZoneInfo Zone { get; set; }

        /// <summary>
        /// Retrieve the appropriate timezone based upon the input enumerated value.
        /// </summary>
        /// <param name="timezone">The enumerated value.</param>
        /// <returns>The matching timezone object.</returns>
        public static LqbTimeZone Create(E_sTimeZoneT timezone)
        {
            switch (timezone)
            {
                case E_sTimeZoneT.AlaskaTime:
                    return AlaskaTime;
                case E_sTimeZoneT.CentralTime:
                    return CentralTime;
                case E_sTimeZoneT.EasternTime:
                    return EasternTime;
                case E_sTimeZoneT.HawaiiTime:
                    return HawaiiTime;
                case E_sTimeZoneT.MountainTime:
                    return MountainTime;
                case E_sTimeZoneT.PacificTime:
                    return PacificTime;
                case E_sTimeZoneT.DisplayLocalTime:
                case E_sTimeZoneT.LeaveBlank:
                    return null;
                default:
                    throw new DeveloperException(ErrorMessage.SystemError);
            }
        }

        /// <summary>
        /// Retrieve the appropriate timezone based upon the input abbreviation.
        /// </summary>
        /// <param name="generalAbbreviation">The abbreviation which must be the general abbreviation for one of the supported time zones.</param>
        /// <returns>The matching timezone object.</returns>
        public static LqbTimeZone Create(string generalAbbreviation)
        {
            if (generalAbbreviation == AlaskaTime.GeneralAbbreviation)
            {
                return AlaskaTime;
            }
            else if (generalAbbreviation == CentralTime.GeneralAbbreviation)
            {
                return CentralTime;
            }
            else if (generalAbbreviation == EasternTime.GeneralAbbreviation)
            {
                return EasternTime;
            }
            else if (generalAbbreviation == HawaiiTime.GeneralAbbreviation)
            {
                return HawaiiTime;
            }
            else if (generalAbbreviation == MountainTime.GeneralAbbreviation)
            {
                return MountainTime;
            }
            else if (generalAbbreviation == PacificTime.GeneralAbbreviation)
            {
                return PacificTime;
            }
            else
            {
                throw new DeveloperException(ErrorMessage.SystemError);
            }
        }

        /// <summary>
        /// Cast the input timezone into the equvalent framework library class.
        /// </summary>
        /// <param name="tz">An LQB time zone.</param>
        public static explicit operator TimeZoneInfo(LqbTimeZone tz)
        {
            return tz.Zone;
        }

        /// <summary>
        /// Implement the equality operator.
        /// </summary>
        /// <param name="lhs">Operand on the left of the equality operator.</param>
        /// <param name="rhs">Operand on the right of the equality operator.</param>
        /// <returns>True of the operands are equal, false otherwise.</returns>
        public static bool operator ==(LqbTimeZone lhs, LqbTimeZone rhs)
        {
            return lhs.Equals(rhs);
        }

        /// <summary>
        /// Implement the inequality operator.
        /// </summary>
        /// <param name="lhs">Operand on the left of the inequality operator.</param>
        /// <param name="rhs">Operand on the right of the inequality operator.</param>
        /// <returns>True of the operands are not equal, false otherwise.</returns>
        public static bool operator !=(LqbTimeZone lhs, LqbTimeZone rhs)
        {
            return !lhs.Equals(rhs);
        }

        /// <summary>
        /// Implement the IEquatable&lt;LqbTimeZone&gt; interface.
        /// </summary>
        /// <param name="other">Time zone being compared with this time zone.</param>
        /// <returns>True if the two time zones are equal, false otherwise.</returns>
        public bool Equals(LqbTimeZone other)
        {
            return this.Zone.Equals(other.Zone);
        }

        /// <summary>
        /// Override the implementation inherited from System.Object.
        /// </summary>
        /// <param name="obj">The object which is compared with this instance for equality.</param>
        /// <returns>True if the input object is an instance of LqbTimeZone and it equals this instance, false otherwise.</returns>
        public override bool Equals(object obj)
        {
            return obj is LqbTimeZone && this.Equals((LqbTimeZone)obj);
        }

        /// <summary>
        /// Override the implementation inherited from System.Object.
        /// </summary>
        /// <returns>An integer hash value consistent with the equality semantics.</returns>
        public override int GetHashCode()
        {
            return this.Zone.GetHashCode();
        }

        /// <summary>
        /// Override the implementation inherited from System.Object.
        /// </summary>
        /// <returns>A string representation of this time zone.</returns>
        public override string ToString()
        {
            return this.GeneralName;
        }
    }
}
