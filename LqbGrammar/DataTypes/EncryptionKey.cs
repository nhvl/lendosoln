﻿namespace LqbGrammar.DataTypes
{
    using Drivers;

    /// <summary>
    /// An encryption algorithm will require a key that is specific
    /// to that algorithm.  This class will serve as the base class
    /// for encryption keys.  The concrete classes will be tightly
    /// bound to the algorithms.  All public uses of these will
    /// be via this base class.
    /// </summary>
    public abstract class EncryptionKey
    {
        /// <summary>
        /// Return the GetType().ToString() that is the default
        /// implementation.  This prevents sub classes from
        /// exposing the key data via the ToString()
        /// functionality.
        /// </summary>
        /// <returns>The GetType().ToString() value.</returns>
        public sealed override string ToString()
        {
            return base.ToString();
        }

        /// <summary>
        /// Use the appropriate encryption algorithm to handle an encrypt/decrypt operation.
        /// </summary>
        /// <param name="decrypt">True if decryption is desired, false if encryption is desired.</param>
        /// <param name="data">The data to encrypt/decrypt.</param>
        /// <returns>The decrypted/encrypted data.</returns>
        public abstract byte[] InvokeEncryption(bool decrypt, byte[] data);

        /// <summary>
        /// Gets the backing fields required to persist the encryption key.
        /// </summary>
        /// <returns>The backing fields required to persist the encryption key.</returns>
        public abstract EncryptionKeyBackingFields GetBackingFields();
    }
}
