﻿namespace LqbGrammar.DataTypes
{
    using LqbGrammar.Validation;

    /// <summary>
    /// Encapsulate validation logic for a comment category.
    /// </summary>
    public struct CommentCategoryName : System.IEquatable<CommentCategoryName>
    {
        /// <summary>
        /// An instance of the class that fails validation, can be used for initialization when necessary.
        /// </summary>
        public static readonly CommentCategoryName BadCategory = new CommentCategoryName(string.Empty);

        /// <summary>
        /// Encapsulated value.
        /// </summary>
        private string name;

        /// <summary>
        /// Initializes a new instance of the <see cref="CommentCategoryName"/> struct.
        /// </summary>
        /// <param name="name">The name of the category.</param>
        private CommentCategoryName(string name)
        {
            this.name = name;
        }

        /// <summary>
        /// Attempt to parse the input name into a validated category name.
        /// </summary>
        /// <param name="name">Input name that will be parsed.</param>
        /// <returns>Valid category name, or null.</returns>
        public static CommentCategoryName? Create(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                return null;
            }

            RegularExpressionResult result = RegularExpressionEngine.Execute(name, RegularExpressionString.CommentCategoryName);
            if (!result.IsMatch())
            {
                return null;
            }

            return new CommentCategoryName(name);
        }

        /// <summary>
        /// Static method that checks for equality between two instances.
        /// </summary>
        /// <param name="lhs">Instance on the left side of the == operator.</param>
        /// <param name="rhs">Instance on the right side of the == operator.</param>
        /// <returns>True if the two instances are equal, false otherwise.</returns>
        public static new bool Equals(object lhs, object rhs)
        {
            if (!(lhs is CommentCategoryName))
            {
                return false;
            }

            if (!(rhs is CommentCategoryName))
            {
                return false;
            }

            return ((CommentCategoryName)lhs).Equals((CommentCategoryName)rhs);
        }

        /// <summary>
        /// The == operator is implemented here.
        /// </summary>
        /// <param name="lhs">Instance on the left of the == operator.</param>
        /// <param name="rhs">Instance on the right of the == operator.</param>
        /// <returns>True if the two instances are equal, false otherwise.</returns>
        public static bool operator ==(CommentCategoryName lhs, CommentCategoryName rhs)
        {
            return lhs.Equals(rhs);
        }

        /// <summary>
        /// The != operator is implemented here.
        /// </summary>
        /// <param name="lhs">Instance on the left of the != operator.</param>
        /// <param name="rhs">Instance on the right of the != operator.</param>
        /// <returns>True if the two instances are not equal, false otherwise.</returns>
        public static bool operator !=(CommentCategoryName lhs, CommentCategoryName rhs)
        {
            return !lhs.Equals(rhs);
        }

        /// <summary>
        /// Return the encapsulated value.
        /// </summary>
        /// <returns>Encapsulated value.</returns>
        public override string ToString()
        {
            return this.name;
        }

        /// <summary>
        /// Implement IEquatable.
        /// </summary>
        /// <param name="other">Instance that is checked for equality with this instance.</param>
        /// <returns>True if other is equal to this instance, false otherwise.</returns>
        public bool Equals(CommentCategoryName other)
        {
            return this.name == other.name;
        }

        /// <summary>
        /// Override the Equals method inherited from the Object class.
        /// </summary>
        /// <param name="obj">Instance that is checked for equality with this instance.</param>
        /// <returns>True if obj is equal to this instance, false otherwise.</returns>
        public override bool Equals(object obj)
        {
            if (!(obj is CommentCategoryName))
            {
                return false;
            }

            return Equals((CommentCategoryName)obj);
        }

        /// <summary>
        /// Override of the method inherited from the Object class.
        /// </summary>
        /// <returns>An integer that is the hash code for this instance.</returns>
        public override int GetHashCode()
        {
            return this.name.GetHashCode();
        }
    }
}
