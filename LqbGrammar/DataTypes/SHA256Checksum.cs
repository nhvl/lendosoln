﻿namespace LqbGrammar.DataTypes
{
    using System;
    using LqbGrammar.Validation;

    /// <summary>
    /// Represents a valid SHA 256 checksum.
    /// </summary>
    public struct SHA256Checksum : IEquatable<SHA256Checksum>
    {
        /// <summary>
        /// An invalid value that can be used for initialization.
        /// </summary>
        public static readonly SHA256Checksum Invalid = new SHA256Checksum(string.Empty);

        /// <summary>
        /// Initializes a new instance of the <see cref="SHA256Checksum"/> struct.
        /// </summary>
        /// <param name="value">The SHA-256 checksum.</param>
        private SHA256Checksum(string value)
        {
            this.Value = value;
        }

        /// <summary>
        /// Gets the SHA-256 checksum value.
        /// </summary>
        /// <value>SHA-256 checksum.</value>
        public string Value { get; private set; }

        /// <summary>
        /// Validate the input string.
        /// </summary>
        /// <param name="value">The string value to validate.</param>
        /// <returns>The validated checksum, or null.</returns>
        public static SHA256Checksum? Create(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return null;
            }

            if (value.Length != 64)
            {
                return null;
            }

            RegularExpressionResult result = RegularExpressionEngine.Execute(value, RegularExpressionString.SHA256Checksum);

            bool isValid = result.IsMatch();
            if (!isValid)
            {
                return null;
            }
            else
            {
                return new SHA256Checksum(value);
            }
        }

        /// <summary>
        /// Compare two instances for equality.
        /// </summary>
        /// <param name="lhs">Instance on the left side.</param>
        /// <param name="rhs">Instance on the right side.</param>
        /// <returns>True if the instances are equal, false otherwise.</returns>
        public static new bool Equals(object lhs, object rhs)
        {
            if (!(lhs is SHA256Checksum))
            {
                return false;
            }

            if (!(rhs is SHA256Checksum))
            {
                return false;
            }

            return ((SHA256Checksum)lhs).Equals((SHA256Checksum)rhs);
        }

        /// <summary>
        /// Implementation of the == operator.
        /// </summary>
        /// <param name="lhs">Instance on the left side.</param>
        /// <param name="rhs">Instance on the right side.</param>
        /// <returns>True if the instances are equal, false otherwise.</returns>
        public static bool operator ==(SHA256Checksum lhs, SHA256Checksum rhs)
        {
            return lhs.Equals(rhs);
        }

        /// <summary>
        /// Implementation of the != operator.
        /// </summary>
        /// <param name="lhs">Instance on the left side.</param>
        /// <param name="rhs">Instance on the right side.</param>
        /// <returns>True if the instances are not equal, false otherwise.</returns>
        public static bool operator !=(SHA256Checksum lhs, SHA256Checksum rhs)
        {
            return !lhs.Equals(rhs);
        }

        /// <summary>
        /// Implementation of the IEquatable interface.
        /// </summary>
        /// <param name="other">Instance that is compared to this instance for equality.</param>
        /// <returns>True if the instances are equal, false otherwise.</returns>
        public bool Equals(SHA256Checksum other)
        {
            return this.Value == other.Value;
        }

        /// <summary>
        /// Override the method inherited from the Object class.
        /// </summary>
        /// <param name="obj">The instance that will be compared with this instance for equality.</param>
        /// <returns>True if the instances are equal, false otherwise.</returns>
        public override bool Equals(object obj)
        {
            return obj is SHA256Checksum && this.Equals((SHA256Checksum)obj);
        }

        /// <summary>
        /// Override of the method inherited from the Object class.
        /// </summary>
        /// <returns>An integer that is the hash of this instance.</returns>
        public override int GetHashCode()
        {
            return this.Value.GetHashCode();
        }
    }
}
