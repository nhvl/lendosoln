﻿namespace LqbGrammar.DataTypes
{
    using System;
    using System.Messaging;
    using Exceptions;

    /// <summary>
    /// Wrapper around a MessageQueue framework object, to prevent leakage of the framework to the client code.
    /// While there is a public constructor and public property that exposes the MessageQueue class to clients,
    /// the intent is that only adapters will use these.
    /// </summary>
    public sealed class LqbMessageQueue : IDisposable
    {
        /// <summary>
        /// Flag set when Dispose is called, prevents other methods from being called subsequently.
        /// </summary>
        private bool dead;

        /// <summary>
        /// Flag set within the Dispose method, allows for multiple Dispose calls without causing errors.
        /// </summary>
        private bool disposed;

        /// <summary>
        /// The encapsulated message queue.
        /// </summary>
        private MessageQueue queue;

        /// <summary>
        /// Initializes a new instance of the <see cref="LqbMessageQueue"/> class.
        /// </summary>
        /// <param name="queue">The message queue to encapsulate.</param>
        public LqbMessageQueue(MessageQueue queue)
        {
            this.queue = queue;
        }

        /// <summary>
        /// Finalizes an instance of the <see cref="LqbMessageQueue" /> class.
        /// </summary>
        ~LqbMessageQueue()
        {
            this.Dispose(false);
        }

        /// <summary>
        /// Gets the encapsulated message queue.  This property is intended for use by appropriate adapters.
        /// </summary>
        /// <value>The encapsulated message queue.</value>
        public MessageQueue Value
        {
            get
            {
                // Q: Why have a public method that returns a library type?  Doesn't that defeat the goal of hiding the library?
                // A: Yes, theoretically but No philosophically.  The intent is for this to be only used by adapters.
                //    It is a limitation of csharp that forces me to expose this publically.
                if (this.dead)
                {
                    throw new DeveloperException(ErrorMessage.SystemError);
                }

                return this.queue;
            }

            private set
            {
                this.queue = value;
            }
        }

        /// <summary>
        /// Dispose of this instance's resources.
        /// </summary>
        public void Dispose()
        {
            this.dead = true;
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Dispose of this instance's resources safely,
        /// depending on the timing of the call.
        /// </summary>
        /// <param name="disposing">True if called from a Dispose(), false if called from the finalizer.</param>
        private void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                this.disposed = true;
                if (disposing && this.queue != null)
                {
                    this.queue.Dispose();
                    this.queue = null;
                }
            }
        }
    }
}
