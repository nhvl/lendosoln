﻿namespace LqbGrammar.DataTypes
{
    using System.Collections.Generic;

    /// <summary>
    /// Security event log search results.
    /// </summary>
    public class SecurityEventLogSearchResults
    {
        /// <summary>
        /// Gets or sets security event logs.
        /// </summary>
        public IEnumerable<SecurityEventLog> SecurityEventLogs { get; set; }

        /// <summary>
        /// Gets or sets total security event logs count.
        /// </summary>
        public int TotalSecurityEventLogsCount { get; set; }
    }
}
