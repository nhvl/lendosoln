﻿namespace LqbGrammar.DataTypes
{
    using System;

    /// <summary>
    /// A generic object identifier.
    /// </summary>
    /// <typeparam name="TValueKind">The <see cref="DataObjectKind"/> of the value being identified.</typeparam>
    /// <typeparam name="TIdValue">The type of the identifier used to refer to the value.</typeparam>
    public struct DataObjectIdentifier<TValueKind, TIdValue> : IEquatable<DataObjectIdentifier<TValueKind, TIdValue>>
        where TValueKind : DataObjectKind
        where TIdValue : struct, IEquatable<TIdValue>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DataObjectIdentifier{TValueKind, TIdValue}"/> struct.
        /// </summary>
        /// <param name="id">The identifier.</param>
        private DataObjectIdentifier(TIdValue id)
        {
            this.Value = id;
        }

        /// <summary>
        /// Gets the value of the identifier.
        /// </summary>
        /// <value>The value of the identifier.</value>
        public TIdValue Value { get; private set; }

        /// <summary>
        /// Creates a <see cref="DataObjectIdentifier{TValueKind, TIdValue}"/> using the given id.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>The new identifier.</returns>
        public static DataObjectIdentifier<TValueKind, TIdValue> Create(TIdValue id)
        {
            return new DataObjectIdentifier<TValueKind, TIdValue>(id);
        }

        /// <summary>
        /// Creates a <see cref="DataObjectIdentifier{TValueKind, TIdValue}"/> using the given id, if it has a value.
        /// </summary>
        /// <param name="id">The identifier, or null.</param>
        /// <returns>The new identifier, or null if no identifier was provided.</returns>
        public static DataObjectIdentifier<TValueKind, TIdValue>? Create(TIdValue? id)
        {
            return id.HasValue ? new DataObjectIdentifier<TValueKind, TIdValue>(id.Value) : (DataObjectIdentifier<TValueKind, TIdValue>?)null;
        }

        /// <summary>
        /// Creates a <see cref="DataObjectIdentifier{TValueKind, TIdValue}"/> using the given id.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>The new identifier.</returns>
        public static DataObjectIdentifier<TValueKind, TIdValue>? Create(string id)
        {
            if (typeof(TIdValue) == typeof(Guid))
            {
                Guid guid;
                bool ok = Guid.TryParse(id, out guid);
                if (ok)
                {
                    var casted = (TIdValue)Convert.ChangeType(guid, typeof(Guid));
                    return Create(casted);
                }
                else
                {
                    return null;
                }
            }
            else
            {
                // Assume TIdValue is a numeric type and allow parsing exceptions to escape
                var parsed = (TIdValue)Convert.ChangeType(id, typeof(TIdValue));
                return Create(parsed);
            }
        }

        /// <summary>
        /// Creates a <see cref="DataObjectIdentifier{K, Guid}"/> using the given id.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>The new identifier.</returns>
        public static DataObjectIdentifier<TValueKind, Guid>? CreateFromGuidString(string id)
        {
            Guid guid;
            bool ok = Guid.TryParse(id, out guid);
            if (ok)
            {
                return DataObjectIdentifier<TValueKind, Guid>.Create(guid);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Equality operator.
        /// </summary>
        /// <param name="id1">First operand.</param>
        /// <param name="id2">Second operand.</param>
        /// <returns>True if the two identifiers have the same value, false otherwise.</returns>
        public static bool operator ==(DataObjectIdentifier<TValueKind, TIdValue> id1, DataObjectIdentifier<TValueKind, TIdValue> id2)
        {
            return id1.Equals(id2);
        }

        /// <summary>
        /// Inequality operator.
        /// </summary>
        /// <param name="id1">First operand.</param>
        /// <param name="id2">Second operand.</param>
        /// <returns>True if the two identifiers do not have the same value, false otherwise.</returns>
        public static bool operator !=(DataObjectIdentifier<TValueKind, TIdValue> id1, DataObjectIdentifier<TValueKind, TIdValue> id2)
        {
            return !id1.Equals(id2);
        }

        /// <summary>
        /// Determines whether this value is the same as another value.
        /// </summary>
        /// <param name="other">The other value.</param>
        /// <returns>True if this value is the same as the other value.</returns>
        public bool Equals(DataObjectIdentifier<TValueKind, TIdValue> other)
        {
            return this.Value.Equals(other.Value);
        }

        /// <summary>
        /// Determines whether this value is the same as another value.
        /// </summary>
        /// <param name="other">An object that is compared with this value.</param>
        /// <returns>True if the object is not null, has the same type and value as this instance, false otherwise.</returns>
        public override bool Equals(object other)
        {
            if (other == null)
            {
                return false;
            }

            if (other is DataObjectIdentifier<TValueKind, TIdValue>)
            {
                var compare = (DataObjectIdentifier<TValueKind, TIdValue>)other;
                return this.Equals(compare);
            }

            return false;
        }

        /// <summary>
        /// Get the hash code for this value.
        /// </summary>
        /// <returns>The hash code for this value.</returns>
        public override int GetHashCode()
        {
            return this.Value.GetHashCode();
        }

        /// <summary>
        /// Gets the default string representation of this value.
        /// </summary>
        /// <returns>The default string representation of this value.</returns>
        public override string ToString()
        {
            return this.Value.ToString();
        }
    }

    /// <summary>
    /// A helper class to create DataObjectIdentifier instances.
    /// </summary>
    public static class DataObjectIdentifier
    {
        /// <summary>
        /// Instantiates a data object identifier of TValueKind and a unique identifier.
        /// </summary>
        /// <typeparam name="TValueKind">The kind of identifier.</typeparam>
        /// <param name="id">The value of the identifier.</param>
        /// <returns>An identifier for a specific kind with the given id.</returns>
        public static DataObjectIdentifier<TValueKind, Guid> Create<TValueKind>(Guid id)
            where TValueKind : DataObjectKind
        {
            return DataObjectIdentifier<TValueKind, Guid>.Create(id);
        }
    }
}
