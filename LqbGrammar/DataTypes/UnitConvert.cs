﻿namespace LqbGrammar.DataTypes
{
    using System;
    using System.Reflection;

    /// <summary>
    /// Convert quantities with different, commensurate, units.
    /// </summary>
    public static class UnitConvert
    {
        /// <summary>
        /// Perform time conversions.
        /// </summary>
        public static class Time
        {
            /// <summary>
            /// Convert a quantity from source to target units.
            /// </summary>
            /// <typeparam name="T">The target units.</typeparam>
            /// <typeparam name="S">The source units.</typeparam>
            /// <param name="source">The source quantity under unit conversion.</param>
            /// <returns>The same quantity expressed in the target units.</returns>
            public static Quantity<T> Convert<T, S>(Quantity<S> source)
                where T : UnitType.Time
                where S : UnitType.Time
            {
                int mult = LqbGrammar.DataTypes.UnitType.Time.GetFactorToSeconds<S>();
                int div = LqbGrammar.DataTypes.UnitType.Time.GetFactorToSeconds<T>();

                decimal value = (decimal)source;
                decimal converted = mult * value / div;
                return Quantity<T>.Create(converted).Value;
            }
        }
    }
}
