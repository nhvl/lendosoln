﻿namespace LqbGrammar.DataTypes
{
    using System;

    /// <summary>
    /// Interface for an ID factory.
    /// </summary>
    /// <typeparam name="TValueKind">The type of values that the identifiers identify.</typeparam>
    /// <typeparam name="TIdValue">The type of the identifiers.</typeparam>
    public interface IDataObjectIdentifierFactory<TValueKind, TIdValue>
        where TValueKind : DataObjectKind
        where TIdValue : struct, IEquatable<TIdValue>
    {
        /// <summary>
        /// Generate a new identifier.
        /// </summary>
        /// <returns>The new identifier.</returns>
        DataObjectIdentifier<TValueKind, TIdValue> NewId();

        /// <summary>
        /// Generate a new identifier from a string.
        /// </summary>
        /// <param name="idString">The string to parse into an identifier.</param>
        /// <returns>The new identifier.</returns>
        DataObjectIdentifier<TValueKind, TIdValue>? Create(string idString);

        /// <summary>
        /// Creates a new identifier from an id value.
        /// </summary>
        /// <param name="idValue">The value for the id.</param>
        /// <returns>The new identifier.</returns>
        DataObjectIdentifier<TValueKind, TIdValue> Create(TIdValue idValue);
    }
}
