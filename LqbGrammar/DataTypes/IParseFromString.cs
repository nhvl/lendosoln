﻿namespace LqbGrammar.DataTypes
{
    using System;

    /// <summary>
    /// Manually defined methods for the IParseFromString interface.
    /// </summary>
    public partial interface IParseFromString
    {
        /// <summary>
        /// Attempt to parse the input string into a boolean.
        /// </summary>
        /// <param name="value">The input string to be parsed.</param>
        /// <returns>A boolean, or null.</returns>
        bool? TryParseBoolean(string value);

        /// <summary>
        /// Parse the input string into a boolean.
        /// </summary>
        /// <param name="value">The input string to be parsed.</param>
        /// <param name="defaultValue">The value to return if the input string cannot be parsed.</param>
        /// <returns>The boolean, or the default value.</returns>
        bool ParseBoolean(string value, bool defaultValue);

        /// <summary>
        /// Attempt to parse the input string into a DataObjectIdentifier instance.
        /// </summary>
        /// <typeparam name="TValueKind">The <see cref="DataObjectKind"/> of the value being identified.</typeparam>
        /// <typeparam name="TIdValue">The type of the identifier used to refer to the value.</typeparam>
        /// <param name="value">The input string to be parsed.</param>
        /// <returns>A DataObjectIdentifier, or null.</returns>
        DataObjectIdentifier<TValueKind, TIdValue>? TryParseDataObjectIdentifier<TValueKind, TIdValue>(string value)
            where TValueKind : DataObjectKind
            where TIdValue : struct, IEquatable<TIdValue>;

        /// <summary>
        /// Parse the input string and return the result, or the default value if the string cannot be parsed.
        /// </summary>
        /// <typeparam name="TValueKind">The <see cref="DataObjectKind"/> of the value being identified.</typeparam>
        /// <typeparam name="TIdValue">The type of the identifier used to refer to the value.</typeparam>
        /// <param name="value">The input string to be parsed.</param>
        /// <param name="defaultValue">The value to return if the input string cannot be parsed.</param>
        /// <returns>A DataObjectIdentifier instance.</returns>
        DataObjectIdentifier<TValueKind, TIdValue> ParseDataObjectIdentifier<TValueKind, TIdValue>(string value, DataObjectIdentifier<TValueKind, TIdValue> defaultValue)
            where TValueKind : DataObjectKind
            where TIdValue : struct, IEquatable<TIdValue>;

        /// <summary>
        /// Parse the input string into an enumeration value.
        /// </summary>
        /// <typeparam name="E">The expected type, which must be an enumeration.</typeparam>
        /// <param name="value">The input string to be parsed.</param>
        /// <returns>The enumeration value, or null.</returns>
        E? TryParseEnum<E>(string value) where E : struct;

        /// <summary>
        /// Parse the input string and return the result, or the default value if the string cannot be parsed.
        /// </summary>
        /// <typeparam name="E">The expected type, which should be an enumeration.</typeparam>
        /// <param name="value">The input string to be parsed.</param>
        /// <param name="defaultValue">The value to return if the input string cannot be parsed.</param>
        /// <returns>The parsed enumeration value, or the default value.</returns>
        E ParseEnum<E>(string value, E defaultValue) where E : struct;
    }
}
