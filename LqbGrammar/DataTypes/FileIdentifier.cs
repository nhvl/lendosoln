﻿namespace LqbGrammar.DataTypes
{
    using LqbGrammar.Exceptions;
    using LqbGrammar.Validation;

    /// <summary>
    /// Encapsulate and validate an identifier for a file sent to a file storage repository.
    /// </summary>
    public struct FileIdentifier : System.IEquatable<FileIdentifier>
    {
        /// <summary>
        /// An invalid instance that can be used for variable initialization.
        /// </summary>
        public static readonly FileIdentifier BadIdentifier = new FileIdentifier(string.Empty);

        /// <summary>
        /// The file identifier.
        /// </summary>
        private string identifier;

        /// <summary>
        /// Initializes a new instance of the <see cref="FileIdentifier"/> struct.
        /// </summary>
        /// <param name="identifier">The identifier.</param>
        private FileIdentifier(string identifier)
        {
            this.identifier = identifier;
        }

        /// <summary>
        /// Validate a file identifier.
        /// </summary>
        /// <param name="identifier">The file identifier.</param>
        /// <returns>The validated file identifier, or null if the input identifier is invalid.</returns>
        public static FileIdentifier? TryParse(string identifier)
        {
            if (string.IsNullOrEmpty(identifier))
            {
                return null;
            }

            RegularExpressionResult result = RegularExpressionEngine.Execute(identifier, RegularExpressionString.FileDBKey);
            if (!result.IsMatch())
            {
                return null;
            }

            return new FileIdentifier(identifier);
        }

        /// <summary>
        /// Compare two objects for equality and they are both the same validated file identifier.
        /// </summary>
        /// <param name="lhs">The object on the left side of the == operator.</param>
        /// <param name="rhs">The object on the right side of the == operator.</param>
        /// <returns>True if both objects are instances of the FileIdentifier class and they are equal to each other.</returns>
        public static new bool Equals(object lhs, object rhs)
        {
            if (!(lhs is FileIdentifier))
            {
                return false;
            }

            if (!(rhs is FileIdentifier))
            {
                return false;
            }

            return ((FileIdentifier)lhs).Equals((FileIdentifier)rhs);
        }

        /// <summary>
        /// Implement the equality operator for a validated file identifier.
        /// </summary>
        /// <param name="lhs">Instance on the left side of the == operator.</param>
        /// <param name="rhs">Instance on the right side of the == operator.</param>
        /// <returns>True if the instances are equal.</returns>
        public static bool operator ==(FileIdentifier lhs, FileIdentifier rhs)
        {
            return lhs.Equals(rhs);
        }

        /// <summary>
        /// Implement the inequality operator for a validated file identifier.
        /// </summary>
        /// <param name="lhs">The instance on the left side of the != operator.</param>
        /// <param name="rhs">The instance on the right side of the != operator.</param>
        /// <returns>Return true if the two instances are not equal, false otherwise.</returns>
        public static bool operator !=(FileIdentifier lhs, FileIdentifier rhs)
        {
            return !lhs.Equals(rhs);
        }

        /// <summary>
        /// Override the implementation inherited from the Object class.
        /// </summary>
        /// <returns>The file identifier.</returns>
        public override string ToString()
        {
            return this.identifier;
        }

        /// <summary>
        /// Compare this instance with another instance for equality.
        /// </summary>
        /// <param name="other">The other instance.</param>
        /// <returns>True if the two instances are equal, false otherwise.</returns>
        public bool Equals(FileIdentifier other)
        {
            // Since file identifiers are actually part of the path within FileDB, the comparison needs to be case insensitive.
            return this.identifier.Equals(other.identifier, System.StringComparison.OrdinalIgnoreCase);
        }

        /// <summary>
        /// Compares this instance with another object for equality.
        /// </summary>
        /// <param name="obj">The other object.</param>
        /// <returns>True if the other object is a valid file identifier and the two objects have the same value.</returns>
        public override bool Equals(object obj)
        {
            if (!(obj is FileIdentifier))
            {
                return false;
            }

            return Equals((FileIdentifier)obj);
        }

        /// <summary>
        /// Override the implementation inherited from the Object class.
        /// </summary>
        /// <returns>An integer that is the hash of this instance.</returns>
        public override int GetHashCode()
        {
            return this.identifier.GetHashCode();
        }
    }
}
