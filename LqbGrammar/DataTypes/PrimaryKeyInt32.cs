﻿namespace LqbGrammar.DataTypes
{
    using System;

    /// <summary>
    /// Wrapper class for an integer primary key in SQL Server.
    /// </summary>
    public struct PrimaryKeyInt32 : IEquatable<PrimaryKeyInt32>
    {
        /// <summary>
        /// Invalid value that can be used for initialization.
        /// </summary>
        public static readonly PrimaryKeyInt32 Invalid = new PrimaryKeyInt32(0);

        /// <summary>
        /// The integer value.
        /// </summary>
        private int value;

        /// <summary>
        /// Initializes a new instance of the <see cref="PrimaryKeyInt32"/> struct.
        /// </summary>
        /// <param name="value">The integer to be encapsulated.</param>
        private PrimaryKeyInt32(int value)
        {
            this.value = value;
        }

        /// <summary>
        /// Gets the encapsulated integer.
        /// </summary>
        /// <value>The integer primary key is returned.</value>
        public int Value
        {
            get { return this.value; }
        }

        /// <summary>
        /// Validate the input integer.
        /// </summary>
        /// <param name="value">The integer to be validated.</param>
        /// <returns>The validated value, or null.</returns>
        public static PrimaryKeyInt32? Create(int value)
        {
            if (value > 0)
            {
                return new PrimaryKeyInt32(value);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Compare two instances for equality.
        /// </summary>
        /// <param name="lhs">Instance on the left side of the == operator.</param>
        /// <param name="rhs">Instance on the right side of the == operator.</param>
        /// <returns>True if the instances are equal, false otherwise.</returns>
        public static new bool Equals(object lhs, object rhs)
        {
            if (!(lhs is PrimaryKeyInt32))
            {
                return false;
            }

            if (!(rhs is PrimaryKeyInt32))
            {
                return false;
            }

            return ((PrimaryKeyInt32)lhs).Equals((PrimaryKeyInt32)rhs);
        }

        /// <summary>
        /// Implementation of the == operator.
        /// </summary>
        /// <param name="lhs">Instance on the left side of the == operator.</param>
        /// <param name="rhs">Instance on the right side of the == operator.</param>
        /// <returns>True if the instances are equal, false otherwise.</returns>
        public static bool operator ==(PrimaryKeyInt32 lhs, PrimaryKeyInt32 rhs)
        {
            return lhs.Equals(rhs);
        }

        /// <summary>
        /// Implementation of the != operator.
        /// </summary>
        /// <param name="lhs">Instance on the left side of the != operator.</param>
        /// <param name="rhs">Instance on the right side of the != operator.</param>
        /// <returns>True if the instances are not equal, false otherwise.</returns>
        public static bool operator !=(PrimaryKeyInt32 lhs, PrimaryKeyInt32 rhs)
        {
            return !lhs.Equals(rhs);
        }

        /// <summary>
        /// Override the method inherited from the Object class.
        /// </summary>
        /// <returns>The integer primary key that is encapsulated.</returns>
        public override string ToString()
        {
            return this.Value.ToString();
        }

        /// <summary>
        /// Implementation of the IEquatable interface.
        /// </summary>
        /// <param name="other">Instance that is compared to this instance for equality.</param>
        /// <returns>True if the instances are equal, false otherwise.</returns>
        public bool Equals(PrimaryKeyInt32 other)
        {
            return this.value == other.value;
        }

        /// <summary>
        /// Override the method inherited from the Object class.
        /// </summary>
        /// <param name="obj">The instance that will be compared with this instance for equality.</param>
        /// <returns>True if the instances are equal, flase otherwise.</returns>
        public override bool Equals(object obj)
        {
            if (!(obj is PrimaryKeyInt32))
            {
                return false;
            }

            return Equals((PrimaryKeyInt32)obj);
        }

        /// <summary>
        /// Override of the method inherited from the Object class.
        /// </summary>
        /// <returns>An integer that is the hash of this instance.</returns>
        public override int GetHashCode()
        {
            return this.value.GetHashCode();
        }
    }
}
