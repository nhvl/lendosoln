﻿namespace LqbGrammar.DataTypes
{
    using System;

    /// <summary>
    /// Encapsulate the concept of an identifier for some resource in the LQB system.  There may be more 
    /// specific types, but they should implicitely castable to this type.
    /// </summary>
    public struct ResourceIdentifier : System.IEquatable<ResourceIdentifier>
    {
        /// <summary>
        /// An instance of the class that fails validation, can be used for initialization when necessary.
        /// </summary>
        public static readonly ResourceIdentifier BadIdentifier = new ResourceIdentifier(string.Empty);

        /// <summary>
        /// For loan files we want to replace an empty value of sLRefNm with this unique value.
        /// </summary>
        private const string EmptyReplacement = "A4F7DC23-32E4-460F-950B-A01B560B3E99";

        /// <summary>
        /// A list of strings to search for to detect an XSS attack.  We cannot use a 
        /// regular expression as there is no constraint on a loan's sLRefNm value and 
        /// there are several examples in production where the open and close angle 
        /// brackets are used.  Accordingly we cannot use a whitelist so we are going
        /// to use a blacklist that can detect naive XSS attempts.
        /// </summary>
        private static readonly string[] ScriptIndicators = new string[] { "<script", "&lt;script", "&#60;script", "&#x3c;script" };

        /// <summary>
        /// Encapsulated value.
        /// </summary>
        private string value;

        /// <summary>
        /// Initializes a new instance of the <see cref="ResourceIdentifier"/> struct.
        /// </summary>
        /// <param name="value">The value.</param>
        private ResourceIdentifier(string value)
        {
            this.value = value;
        }

        /// <summary>
        /// Static method that checks for equality between two instances.
        /// </summary>
        /// <param name="lhs">Instance on the left side of the == operator.</param>
        /// <param name="rhs">Instance on the right side of the == operator.</param>
        /// <returns>True if the two instances are equal, false otherwise.</returns>
        public static new bool Equals(object lhs, object rhs)
        {
            if (!(lhs is ResourceIdentifier))
            {
                return false;
            }

            if (!(rhs is ResourceIdentifier))
            {
                return false;
            }

            return ((ResourceIdentifier)lhs).Equals((ResourceIdentifier)rhs);
        }

        /// <summary>
        /// The == operator is implemented here.
        /// </summary>
        /// <param name="lhs">Instance on the left of the == operator.</param>
        /// <param name="rhs">Instance on the right of the == operator.</param>
        /// <returns>True if the two instances are equal, false otherwise.</returns>
        public static bool operator ==(ResourceIdentifier lhs, ResourceIdentifier rhs)
        {
            return lhs.Equals(rhs);
        }

        /// <summary>
        /// The != operator is implemented here.
        /// </summary>
        /// <param name="lhs">Instance on the left of the != operator.</param>
        /// <param name="rhs">Instance on the right of the != operator.</param>
        /// <returns>True if the two instances are not equal, false otherwise.</returns>
        public static bool operator !=(ResourceIdentifier lhs, ResourceIdentifier rhs)
        {
            return !lhs.Equals(rhs);
        }

        /// <summary>
        /// Attempt to parse the input name into a validated resource identifier.
        /// </summary>
        /// <param name="value">Input identifier that will be parsed.</param>
        /// <returns>Valid resouce identifier, or null.</returns>
        public static ResourceIdentifier? Create(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return null;
            }

            // For loan files in the conversation log, we are going to use BrokerId:sLRefNm.  BrokerId is a guid but sLRefNm is unrestrained.
            // For other resource types we'll continue to assume a guid.
            string guidPart = value;
            string remainingPart = null;
            int splitPosition = value.IndexOf(':');
            if (splitPosition > 0)
            {
                guidPart = value.Substring(0, splitPosition);
                remainingPart = value.Substring(splitPosition + 1);
            }

            Guid guid;
            bool ok = Guid.TryParse(guidPart, out guid);
            if (!ok || guid == Guid.Empty)
            {
                return null;
            }

            if (remainingPart == null)
            {
                return new ResourceIdentifier(guidPart);
            }

            string working = remainingPart.ToLower();
            foreach (string indicator in ScriptIndicators)
            {
                int scriptIndex = working.IndexOf(indicator);
                if (scriptIndex >= 0)
                {
                    return null;
                }
            }

            return new ResourceIdentifier(value);
        }

        /// <summary>
        /// Create a resource identifier for a loan file, which has special formatting.
        /// </summary>
        /// <param name="brokerId">The brokerid.</param>
        /// <param name="brokerUniqueLoanNumber">A unique identifier for the loan file.</param>
        /// <returns>A resource identifier, or null if there is a problem.</returns>
        public static ResourceIdentifier? CreateForLoanFile(BrokerIdentifier brokerId, string brokerUniqueLoanNumber)
        {
            // There is a unique constraint in the LQB database for the (BrokerId, sLRefNm) pair,
            // but it is possible (and there is currently one example) for sLRefNm to be empty
            // (not null, though...the column doesn't accept NULL).  I want to ensure that these
            // errant blank values don't propagate.  This is a HACK around an unfortunate circumstance.
            string loanNumber = brokerUniqueLoanNumber.Length == 0 ? EmptyReplacement : brokerUniqueLoanNumber;
            string identifier = string.Format("{0}:{1}", brokerId.ToString(), loanNumber);
            return Create(identifier);
        }

        /// <summary>
        /// Return the encapsulated value.
        /// </summary>
        /// <returns>Encapsulated value.</returns>
        public override string ToString()
        {
            return this.value;
        }

        /// <summary>
        /// Implement IEquatable.
        /// </summary>
        /// <param name="other">Instance that is checked for equality with this instance.</param>
        /// <returns>True if other is equal to this instance, false otherwise.</returns>
        public bool Equals(ResourceIdentifier other)
        {
            return this.value == other.value;
        }

        /// <summary>
        /// Override the Equals method inherited from the Object class.
        /// </summary>
        /// <param name="obj">Instance that is checked for equality with this instance.</param>
        /// <returns>True if obj is equal to this instance, false otherwise.</returns>
        public override bool Equals(object obj)
        {
            if (!(obj is ResourceIdentifier))
            {
                return false;
            }

            return this.Equals((ResourceIdentifier)obj);
        }

        /// <summary>
        /// Override of the method inherited from the Object class.
        /// </summary>
        /// <returns>An integer that is the hash code for this instance.</returns>
        public override int GetHashCode()
        {
            return this.value.GetHashCode();
        }
    }
}
