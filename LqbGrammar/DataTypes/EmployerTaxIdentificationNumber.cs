﻿namespace LqbGrammar.DataTypes
{
    using Validation;

    /// <summary>
    /// Encapsulates and validates an employer tax identification number.
    /// </summary>
    public struct EmployerTaxIdentificationNumber
    {
        /// <summary>
        /// The employer tax identification number.
        /// </summary>
        private string employerTaxId;

        /// <summary>
        /// Initializes a new instance of the <see cref="EmployerTaxIdentificationNumber"/> struct.
        /// </summary>
        /// <param name="employerTaxId">The employeer tax identification number.</param>
        private EmployerTaxIdentificationNumber(string employerTaxId)
        {
            this.employerTaxId = employerTaxId;
        }

        /// <summary>
        /// Validate and create an employer tax identification number.
        /// </summary>
        /// <param name="employerIdNum">The employer tax identification number.</param>
        /// <returns>The validated phone number or null.</returns>
        public static EmployerTaxIdentificationNumber? Create(string employerIdNum)
        {
            if (string.IsNullOrEmpty(employerIdNum))
            {
                return null;
            }

            RegularExpressionResult result = RegularExpressionEngine.Execute(employerIdNum, RegularExpressionString.EmployerTaxIdentificationNumber);
            if (!result.IsMatch())
            {
                return null;
            }

            // We want the value we maintain to be just 9 digits, no dash.
            return new EmployerTaxIdentificationNumber(result.GetGroup(1) + result.GetGroup(2));
        }

        /// <summary>
        /// Compare two objects for equality and they are both the same employer tax identification number.
        /// </summary>
        /// <param name="lhs">The object on the left side of the == operator.</param>
        /// <param name="rhs">The object on the right side of the == operator.</param>
        /// <returns>True if both objects are instances of the EmployerTaxIdentificationNumber class and they are equal to each other.</returns>
        public static new bool Equals(object lhs, object rhs)
        {
            if (!(lhs is EmployerTaxIdentificationNumber))
            {
                return false;
            }

            if (!(rhs is EmployerTaxIdentificationNumber))
            {
                return false;
            }

            return ((EmployerTaxIdentificationNumber)lhs).Equals((EmployerTaxIdentificationNumber)rhs);
        }

        /// <summary>
        /// Implement the equality operator for a validated employer tax identification number.
        /// </summary>
        /// <param name="lhs">Instance on the left side of the == operator.</param>
        /// <param name="rhs">Instance on the right side of the == operator.</param>
        /// <returns>True if the instances are equal.</returns>
        public static bool operator ==(EmployerTaxIdentificationNumber lhs, EmployerTaxIdentificationNumber rhs)
        {
            return lhs.Equals(rhs);
        }

        /// <summary>
        /// Implement the inequality operator for a validated employer tax identification number.
        /// </summary>
        /// <param name="lhs">The instance on the left side of the != operator.</param>
        /// <param name="rhs">The instance on the right side of the != operator.</param>
        /// <returns>Return true if the two instances are not equal, false otherwise.</returns>
        public static bool operator !=(EmployerTaxIdentificationNumber lhs, EmployerTaxIdentificationNumber rhs)
        {
            return !lhs.Equals(rhs);
        }

        /// <summary>
        /// Returns the employer tax identification number as a string.
        /// </summary>
        /// <returns>The employer tax identification number as a string.</returns>
        public override string ToString()
        {
            return this.employerTaxId;
        }

        /// <summary>
        /// Compare this instance with another instance for equality.
        /// </summary>
        /// <param name="other">The other instance.</param>
        /// <returns>True if the two instances are equal, false otherwise.</returns>
        public bool Equals(EmployerTaxIdentificationNumber other)
        {
            return this.employerTaxId == other.employerTaxId;
        }

        /// <summary>
        /// Compares this instance with another object for equality.
        /// </summary>
        /// <param name="obj">The other object.</param>
        /// <returns>True if the other object is a valid employer tax identification number and the two objects have the same value.</returns>
        public override bool Equals(object obj)
        {
            if (!(obj is EmployerTaxIdentificationNumber))
            {
                return false;
            }

            return Equals((EmployerTaxIdentificationNumber)obj);
        }

        /// <summary>
        /// Override the implementation inherited from the Object class.
        /// </summary>
        /// <returns>An integer that is the hash of this instance.</returns>
        public override int GetHashCode()
        {
            return this.employerTaxId.GetHashCode();
        }
    }
}
