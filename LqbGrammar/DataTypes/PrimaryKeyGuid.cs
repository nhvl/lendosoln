﻿namespace LqbGrammar.DataTypes
{
    using System;

    /// <summary>
    /// Wrapper class for Guid that holds a unique identifier primary key in SQL Server.
    /// </summary>
    public struct PrimaryKeyGuid : IEquatable<PrimaryKeyGuid>
    {
        /// <summary>
        /// Invalid value that can be used for initialization.
        /// </summary>
        public static readonly PrimaryKeyGuid Invalid = new PrimaryKeyGuid(Guid.Empty);

        /// <summary>
        /// The guid value.
        /// </summary>
        private Guid guid;

        /// <summary>
        /// Initializes a new instance of the <see cref="PrimaryKeyGuid"/> struct.
        /// </summary>
        /// <param name="guid">The guid to be encapsulated.</param>
        private PrimaryKeyGuid(Guid guid)
        {
            this.guid = guid;
        }

        /// <summary>
        /// Gets the encapsulated guid.
        /// </summary>
        /// <value>The guid is returned.</value>
        public Guid Value
        {
            get { return this.guid; }
        }

        /// <summary>
        /// Validate the input string version of a Guid.
        /// </summary>
        /// <param name="guid">The query string to be validated.</param>
        /// <returns>The validated guid, or null.</returns>
        public static PrimaryKeyGuid? Create(string guid)
        {
            if (string.IsNullOrEmpty(guid))
            {
                return null;
            }

            Guid parsedGuid;
            bool isValid = Guid.TryParse(guid, out parsedGuid);
            if (!isValid)
            {
                return null;
            }

            if (parsedGuid == Invalid.Value)
            {
                return null;
            }

            return new PrimaryKeyGuid(parsedGuid);
        }

        /// <summary>
        /// Validate the input guid.
        /// </summary>
        /// <param name="guid">The guid to be validated.</param>
        /// <returns>The validated guid, or null.</returns>
        public static PrimaryKeyGuid? Create(Guid guid)
        {
            if (guid == Invalid.Value)
            {
                return null;
            }

            return new PrimaryKeyGuid(guid);
        }

        /// <summary>
        /// Compare two instances for equality.
        /// </summary>
        /// <param name="lhs">Instance on the left side of the == operator.</param>
        /// <param name="rhs">Instance on the right side of the == operator.</param>
        /// <returns>True if the instances are equal, false otherwise.</returns>
        public static new bool Equals(object lhs, object rhs)
        {
            if (!(lhs is PrimaryKeyGuid))
            {
                return false;
            }

            if (!(rhs is PrimaryKeyGuid))
            {
                return false;
            }

            return ((PrimaryKeyGuid)lhs).Equals((PrimaryKeyGuid)rhs);
        }

        /// <summary>
        /// Implementation of the == operator.
        /// </summary>
        /// <param name="lhs">Instance on the left side of the == operator.</param>
        /// <param name="rhs">Instance on the right side of the == operator.</param>
        /// <returns>True if the instances are equal, false otherwise.</returns>
        public static bool operator ==(PrimaryKeyGuid lhs, PrimaryKeyGuid rhs)
        {
            return lhs.Equals(rhs);
        }

        /// <summary>
        /// Implementation of the != operator.
        /// </summary>
        /// <param name="lhs">Instance on the left side of the != operator.</param>
        /// <param name="rhs">Instance on the right side of the != operator.</param>
        /// <returns>True if the instances are not equal, false otherwise.</returns>
        public static bool operator !=(PrimaryKeyGuid lhs, PrimaryKeyGuid rhs)
        {
            return !lhs.Equals(rhs);
        }

        /// <summary>
        /// Override the method inherited from the Object class.
        /// </summary>
        /// <returns>The regular expression string that is encapsulated.</returns>
        public override string ToString()
        {
            return this.Value.ToString();
        }

        /// <summary>
        /// Return a formatted string, using the same format specification as System.Guid.
        /// </summary>
        /// <param name="format">The format string.</param>
        /// <returns>A string version of the encapsulated Guid.</returns>
        public string ToString(string format)
        {
            return this.Value.ToString(format);
        }

        /// <summary>
        /// Implementation of the IEquatable interface.
        /// </summary>
        /// <param name="other">Instance that is compared to this instance for equality.</param>
        /// <returns>True if the instances are equal, false otherwise.</returns>
        public bool Equals(PrimaryKeyGuid other)
        {
            return this.guid == other.guid;
        }

        /// <summary>
        /// Override the method inherited from the Object class.
        /// </summary>
        /// <param name="obj">The instance that will be compared with this instance for equality.</param>
        /// <returns>True if the instances are equal, flase otherwise.</returns>
        public override bool Equals(object obj)
        {
            if (!(obj is PrimaryKeyGuid))
            {
                return false;
            }

            return Equals((PrimaryKeyGuid)obj);
        }

        /// <summary>
        /// Override of the method inherited from the Object class.
        /// </summary>
        /// <returns>An integer that is the hash of this instance.</returns>
        public override int GetHashCode()
        {
            return this.guid.GetHashCode();
        }
    }
}
