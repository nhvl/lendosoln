﻿namespace LqbGrammar.DataTypes
{
    using System.IO;

    /// <summary>
    /// The simplest kind of streamable content, wrapping a byte array so that it can write itself to a Stream.
    /// </summary>
    public class ByteContent : IStreamableContent
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ByteContent"/> class.
        /// </summary>
        /// <param name="content">The bytes composing the content.</param>
        public ByteContent(byte[] content)
        {
            this.Content = content;
        }

        /// <summary>
        /// Gets the bytes of the content.
        /// </summary>
        public byte[] Content { get; }

        /// <summary>
        /// Gets the count of bytes in the content.
        /// </summary>
        /// <returns>The count of bytes.</returns>
        public long? GetContentLengthBytes()
        {
            return this.Content.Length;
        }

        /// <summary>
        /// Writes to the given stream.
        /// </summary>
        /// <param name="writeTo">The stream to write to.</param>
        public void WriteToStream(Stream writeTo)
        {
            writeTo.Write(this.Content, 0, this.Content.Length);
        }
    }
}
