﻿namespace LqbGrammar.DataTypes
{
    using System;

    /// <summary>
    /// Provides a generic way to wrap sensitive data.
    /// </summary>
    /// <typeparam name="T">The type of the sensitive data.</typeparam>
    public struct Sensitive<T> : IEquatable<Sensitive<T>>
    {
        /// <summary>
        /// The sensitive data.
        /// </summary>
        private T data;

        /// <summary>
        /// Initializes a new instance of the <see cref="Sensitive{T}"/> struct.
        /// </summary>
        /// <param name="data">The sensitive data to wrap.</param>
        public Sensitive(T data)
        {
            this.data = data;
        }

        /// <summary>
        /// Gets the sensitive data.
        /// </summary>
        /// <value>The sensitive data.</value>
        public T Value
        {
            get { return this.data; }
        }

        /// <summary>
        /// Allows implicit conversion from <typeparamref name="T"/> to <see cref="Sensitive{T}"/>.
        /// </summary>
        /// <param name="t">The data item to wrap as sensitive.</param>
        public static implicit operator Sensitive<T>(T t)
        {
            return new Sensitive<T>(t);
        }

        /// <summary>
        /// Implements the equality operator.
        /// </summary>
        /// <param name="lhs">Instance on the left side of the == operator.</param>
        /// <param name="rhs">Instance on the right side of the == operator.</param>
        /// <returns>True if the instances are equal.</returns>
        public static bool operator ==(Sensitive<T> lhs, Sensitive<T> rhs)
        {
            return lhs.Equals(rhs);
        }

        /// <summary>
        /// Implements the inequality operator.
        /// </summary>
        /// <param name="lhs">The instance on the left side of the != operator.</param>
        /// <param name="rhs">The instance on the right side of the != operator.</param>
        /// <returns>Return true if the two instances are not equal, false otherwise.</returns>
        public static bool operator !=(Sensitive<T> lhs, Sensitive<T> rhs)
        {
            return !lhs.Equals(rhs);
        }

        /// <summary>
        /// Gets a string representation of the sensitive data.
        /// </summary>
        /// <remarks>
        /// This is meant to ensure backwards-compatibility and avoid outputting
        /// Sensitive`1 as the data representation. Eventually, this should be
        /// removed.
        /// </remarks>
        /// <returns>A string representation of the sensitive data.</returns>
        public override string ToString()
        {
            return this.Value?.ToString() ?? string.Empty;
        }

        /// <summary>
        /// Determines whether the current instance is equal to the other instance.
        /// </summary>
        /// <param name="other">The other instance.</param>
        /// <returns>True if equal. Otherwise, false.</returns>
        public bool Equals(Sensitive<T> other)
        {
            if (this.Value == null && other.Value == null)
            {
                return true;
            }
            else if (this.Value != null)
            {
                return this.Value.Equals(other.Value);
            }
            else
            {
                return other.Value.Equals(this.Value);
            }
        }

        /// <summary>
        /// Compares this instance with another object for equality.
        /// </summary>
        /// <param name="obj">The other object.</param>
        /// <returns>True if equal. Otherwise, false.</returns>
        public override bool Equals(object obj)
        {
            if (!(obj is Sensitive<T>))
            {
                return false;
            }

            return this.Equals((Sensitive<T>)obj);
        }

        /// <summary>
        /// Override the implementation inherited from the Object class.
        /// </summary>
        /// <returns>An integer that is the hash of this instance.</returns>
        public override int GetHashCode()
        {
            return this.Value == null ? 0 : this.Value.GetHashCode();
        }
    }
}
