﻿namespace LqbGrammar.DataTypes
{
    using System;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Encapsulate a percentage, which when multiplied by a Quantity gives back
    /// another Quantity such that 100% * Q = Q.
    /// </summary>
    /// <remarks>
    /// TODO: handle arithmetic with quantity later because the Quantity struct
    ///       is currently in an independent branch.
    /// </remarks>
    public struct Percentage : ISemanticType, IEquatable<Percentage>, IComparable, IComparable<Percentage>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Percentage"/> struct.
        /// </summary>
        /// <param name="value">The percent value.</param>
        private Percentage(decimal value)
        {
            this.Value = value;
        }

        /// <summary>
        /// Gets the underlying value.
        /// </summary>
        /// <value>The underlying value.</value>
        object ISemanticType.Value
        {
            get
            {
                return this.Value;
            }
        }

        /// <summary>
        /// Gets or sets the percent value.
        /// </summary>
        /// <value>The percent value.</value>
        private decimal Value { get; set; }

        /// <summary>
        /// Create a Percentage instance using the input value.
        /// </summary>
        /// <param name="value">The percentage value.</param>
        /// <returns>A Percentage instance.</returns>
        public static Percentage? Create(decimal value)
        {
            return new Percentage(value);
        }

        /// <summary>
        /// Create a Percentage instance from a string representation.
        /// </summary>
        /// <param name="representation">The string representation.</param>
        /// <returns>A Percentage instance, or null.</returns>
        public static Percentage? Create(string representation)
        {
            decimal value;
            if (decimal.TryParse(representation, out value))
            {
                return new Percentage(value);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Allow a Percentage to be cast to a decimal.
        /// </summary>
        /// <param name="percent">The percentage.</param>
        public static explicit operator decimal(Percentage percent)
        {
            return percent.Value;
        }

        /// <summary>
        /// The == operator is implemented here.
        /// </summary>
        /// <param name="lhs">Instance on the left of the == operator.</param>
        /// <param name="rhs">Instance on the right of the == operator.</param>
        /// <returns>True if the two instances are equal, false otherwise.</returns>
        public static bool operator ==(Percentage lhs, Percentage rhs)
        {
            return lhs.Equals(rhs);
        }

        /// <summary>
        /// The != operator is implemented here.
        /// </summary>
        /// <param name="lhs">Instance on the left of the != operator.</param>
        /// <param name="rhs">Instance on the right of the != operator.</param>
        /// <returns>True if the two instances are not equal, false otherwise.</returns>
        public static bool operator !=(Percentage lhs, Percentage rhs)
        {
            return !lhs.Equals(rhs);
        }

        /// <summary>
        /// The less than operator is implemented here.
        /// </summary>
        /// <param name="lhs">Operand on the left of the operator.</param>
        /// <param name="rhs">Operand on the right of the operator.</param>
        /// <returns>The result of applying the operator to the two operands.</returns>
        public static bool operator <(Percentage lhs, Percentage rhs)
        {
            return lhs.CompareTo(rhs) < 0;
        }

        /// <summary>
        /// The less than or equal operator is implemented here.
        /// </summary>
        /// <param name="lhs">Operand on the left of the operator.</param>
        /// <param name="rhs">Operand on the right of the operator.</param>
        /// <returns>The result of applying the operator to the two operands.</returns>
        public static bool operator <=(Percentage lhs, Percentage rhs)
        {
            return lhs.CompareTo(rhs) <= 0;
        }

        /// <summary>
        /// The greater than operator is implemented here.
        /// </summary>
        /// <param name="lhs">Operand on the left of the operator.</param>
        /// <param name="rhs">Operand on the right of the operator.</param>
        /// <returns>The result of applying the operator to the two operands.</returns>
        public static bool operator >(Percentage lhs, Percentage rhs)
        {
            return lhs.CompareTo(rhs) > 0;
        }

        /// <summary>
        /// The greater than or equal operator is implemented here.
        /// </summary>
        /// <param name="lhs">Operand on the left of the operator.</param>
        /// <param name="rhs">Operand on the right of the operator.</param>
        /// <returns>The result of applying the operator to the two operands.</returns>
        public static bool operator >=(Percentage lhs, Percentage rhs)
        {
            return lhs.CompareTo(rhs) >= 0;
        }

        /// <summary>
        /// Override the Equals method inherited from the Object class.
        /// </summary>
        /// <param name="obj">Object instance that is checked for equality with this instance.</param>
        /// <returns>True if obj is equal to this instance, false otherwise.</returns>
        public override bool Equals(object obj)
        {
            if (!(obj is Percentage))
            {
                return false;
            }

            return this.Equals((Percentage)obj);
        }

        /// <summary>
        /// Implementation of the IEquatable interface.
        /// </summary>
        /// <param name="other">Instance that is compared to this instance for equality.</param>
        /// <returns>True if the instances are equal, false otherwise.</returns>
        public bool Equals(Percentage other)
        {
            return this.CompareTo(other) == 0;
        }

        /// <summary>
        /// Compare this instance with another object.
        /// </summary>
        /// <param name="obj">The other object.</param>
        /// <returns>The result of the comparison, or an exception is thrown if the other object is not an instance of the Instant struct.</returns>
        public int CompareTo(object obj)
        {
            if (obj is Percentage)
            {
                return this.CompareTo((Percentage)obj);
            }
            else
            {
                throw new DeveloperException(ErrorMessage.SystemError);
            }
        }

        /// <summary>
        /// Compare this instance with another Percentage.
        /// </summary>
        /// <param name="other">The other Percentage.</param>
        /// <returns>The result of the comparison, zero if equal, greater than zero if this instance is greater, less than zero of this instance is lesser.</returns>
        public int CompareTo(Percentage other)
        {
            return this.Value.CompareTo(other.Value);
        }

        /// <summary>
        /// Override of the method inherited from the Object class.
        /// </summary>
        /// <returns>An integer that is the hash code for this instance.</returns>
        public override int GetHashCode()
        {
            return this.Value.GetHashCode();
        }

        /// <summary>
        /// Override of t he method inherited from the Object class.
        /// </summary>
        /// <returns>A string representation of this instance.</returns>
        public override string ToString()
        {
            return this.Value.ToString();
        }
    }
}
