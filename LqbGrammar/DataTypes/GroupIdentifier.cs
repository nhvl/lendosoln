﻿namespace LqbGrammar.DataTypes
{
    using System;

    /// <summary>
    /// Encapsulate the concept of an identifier for a group in the LQB system.
    /// </summary>
    public struct GroupIdentifier : System.IEquatable<GroupIdentifier>
    {
        /// <summary>
        /// An instance of the class that fails validation, can be used for initialization when necessary.
        /// </summary>
        public static readonly GroupIdentifier BadIdentifier = new GroupIdentifier(BrokerIdentifier.BadIdentifier, Guid.Empty);

        /// <summary>
        /// Initializes a new instance of the <see cref="GroupIdentifier"/> struct.
        /// </summary>
        /// <param name="brokerId">The broker identifier for the group.</param>
        /// <param name="groupId">The id of the group.</param>
        private GroupIdentifier(BrokerIdentifier brokerId, Guid groupId)
        {
            this.BrokerId = brokerId;
            this.GroupId = groupId;
        }

        /// <summary>
        /// Gets the encapsulated broker identifier.
        /// </summary>
        /// <value>The identifier of the broker for the group.</value>
        public BrokerIdentifier BrokerId { get; private set; }

        /// <summary>
        /// Gets the encapsulated group identifier.
        /// </summary>
        /// <value>The identifier of the group.</value>
        public Guid GroupId { get; private set; }

        /// <summary>
        /// Attempt to parse the input value into a validated group identifier.
        /// </summary>
        /// <param name="value">Input value that will be parsed.</param>
        /// <returns>Valid group identifier, or null.</returns>
        public static GroupIdentifier? Create(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return null;
            }

            var parts = value.Split('|');
            if (parts.Length != 2)
            {
                return null;
            }

            var brokerId = BrokerIdentifier.Create(parts[0]);
            if (brokerId == null)
            {
                return null;
            }

            if (brokerId == BrokerIdentifier.BadIdentifier)
            {
                return null;
            }

            Guid groupId;
            bool ok = Guid.TryParse(parts[1], out groupId);
            if (ok && groupId != Guid.Empty)
            {
                return new GroupIdentifier(brokerId.Value, groupId);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Creates a group identifier with the specified id and brokerid, or returns null if the inputs aren't valid.
        /// </summary>
        /// <param name="brokerId">The broker the group belongs to.</param>
        /// <param name="groupIdString">The id of the group.</param>
        /// <returns>A group identifer, or null if the inputs are invalid.</returns>
        public static GroupIdentifier? Create(BrokerIdentifier brokerId, string groupIdString)
        {
            if (string.IsNullOrEmpty(groupIdString))
            {
                return null;
            }

            if (brokerId == BrokerIdentifier.BadIdentifier)
            {
                return null;
            }

            Guid groupId;
            bool ok = Guid.TryParse(groupIdString, out groupId);
            if (ok && groupId != Guid.Empty)
            {
                return new GroupIdentifier(brokerId, groupId);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Creates a group identifier with the specified id and brokerid, or returns null if the inputs aren't valid.
        /// </summary>
        /// <param name="brokerId">The broker the group belongs to.</param>
        /// <param name="groupId">The id of the group.</param>
        /// <returns>A group identifer, or null if the inputs are invalid.</returns>
        public static GroupIdentifier? Create(BrokerIdentifier brokerId, Guid groupId)
        {
            if (brokerId == BrokerIdentifier.BadIdentifier)
            {
                return null;
            }

            if (groupId == Guid.Empty)
            {
                return null;
            }

            return new GroupIdentifier(brokerId, groupId);
        }

        /// <summary>
        /// Static method that checks for equality between two instances.
        /// </summary>
        /// <param name="lhs">Instance on the left side of the == operator.</param>
        /// <param name="rhs">Instance on the right side of the == operator.</param>
        /// <returns>True if the two instances are equal, false otherwise.</returns>
        public static new bool Equals(object lhs, object rhs)
        {
            if (!(lhs is GroupIdentifier))
            {
                return false;
            }

            if (!(rhs is GroupIdentifier))
            {
                return false;
            }

            return ((GroupIdentifier)lhs).Equals((GroupIdentifier)rhs);
        }

        /// <summary>
        /// The == operator is implemented here.
        /// </summary>
        /// <param name="lhs">Instance on the left of the == operator.</param>
        /// <param name="rhs">Instance on the right of the == operator.</param>
        /// <returns>True if the two instances are equal, false otherwise.</returns>
        public static bool operator ==(GroupIdentifier lhs, GroupIdentifier rhs)
        {
            return lhs.Equals(rhs);
        }

        /// <summary>
        /// The != operator is implemented here.
        /// </summary>
        /// <param name="lhs">Instance on the left of the != operator.</param>
        /// <param name="rhs">Instance on the right of the != operator.</param>
        /// <returns>True if the two instances are not equal, false otherwise.</returns>
        public static bool operator !=(GroupIdentifier lhs, GroupIdentifier rhs)
        {
            return !lhs.Equals(rhs);
        }

        /// <summary>
        /// Return the encapsulated value.
        /// </summary>
        /// <returns>Encapsulated value.</returns>
        public override string ToString()
        {
            return this.BrokerId.ToString() + "|" + this.GroupId.ToString();
        }

        /// <summary>
        /// Implement IEquatable.
        /// </summary>
        /// <param name="other">Instance that is checked for equality with this instance.</param>
        /// <returns>True if other is equal to this instance, false otherwise.</returns>
        public bool Equals(GroupIdentifier other)
        {
            return this.GroupId == other.GroupId
                && this.BrokerId == other.BrokerId;
        }

        /// <summary>
        /// Override the Equals method inherited from the Object class.
        /// </summary>
        /// <param name="obj">Instance that is checked for equality with this instance.</param>
        /// <returns>True if obj is equal to this instance, false otherwise.</returns>
        public override bool Equals(object obj)
        {
            if (!(obj is GroupIdentifier))
            {
                return false;
            }

            return this.Equals((GroupIdentifier)obj);
        }

        /// <summary>
        /// Override of the method inherited from the Object class.
        /// </summary>
        /// <returns>An integer that is the hash code for this instance.</returns>
        public override int GetHashCode()
        {
            unchecked
            {
                int hash = 17;
                hash = (hash * 23) + this.BrokerId.GetHashCode();
                hash = (hash * 23) + this.GroupId.GetHashCode();
                return hash;
            }
        }
    }
}
