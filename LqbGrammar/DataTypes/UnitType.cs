﻿namespace LqbGrammar.DataTypes
{
    using System;
    using System.Collections.Generic;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Base class for unit types, which represent
    /// units of measuring quantities.
    /// </summary>
    public abstract class UnitType
    {
        /// <summary>
        /// Gets the singular version of the unit name.
        /// </summary>
        /// <value>The singular version of the unit name.</value>
        public virtual string SingularUnitName
        {
            get
            {
                var type = this.GetType();
                return type.IsSealed ? type.Name.ToLower() : null;
            }
        }

        /// <summary>
        /// Gets the plural version of the unit name.
        /// </summary>
        /// <value>The plural version of the unit name.</value>
        public virtual string PluralUnitName
        {
            get
            {
                string singular = this.SingularUnitName;
                return singular != null ? singular + "s" : null;
            }
        }

        /// <summary>
        /// Gets the standard abbreviation for the unit type.
        /// </summary>
        /// <value>The standard abbreviation for the unit type.</value>
        public virtual string StandardAbbreviation
        {
            get
            {
                return this.SingularUnitName;
            }
        }

        /// <summary>
        /// Encapsulate the concept of a quantity of time.
        /// </summary>
        public abstract class Time : UnitType
        {
            /// <summary>
            /// Cache the factor to second values to avoid reflection.
            /// </summary>
            private static readonly Dictionary<Type, int> Factors;

            /// <summary>
            /// Initializes static members of the <see cref="Time" /> class.
            /// </summary>
            static Time()
            {
                // Only include values which make sense
                Factors = new Dictionary<Type, int>();
                Factors[typeof(Second)] = Second.Instance.FactorToSeconds;
                Factors[typeof(Minute)] = Minute.Instance.FactorToSeconds;
                Factors[typeof(Hour)] = Hour.Instance.FactorToSeconds;
                Factors[typeof(Day)] = Day.Instance.FactorToSeconds;
            }

            /// <summary>
            /// Gets the factor required to convert a unit of time to a number of seconds.
            /// </summary>
            /// <value>The factor required to convert a unit of time to a number of seconds.</value>
            public abstract int FactorToSeconds { get; }

            /// <summary>
            /// Retrieve the cached factor to seconds value for the indicated type.
            /// </summary>
            /// <typeparam name="T">The type for which the factor to second value is desired.</typeparam>
            /// <returns>The factor to seconds value.</returns>
            public static int GetFactorToSeconds<T>() where T : Time
            {
                var type = typeof(T);
                if (Factors.ContainsKey(type))
                {
                    return Factors[typeof(T)];
                }
                else
                {
                    throw new DeveloperException(ErrorMessage.SystemError);
                }
            }
        }

        /// <summary>
        /// Encapsulate the concept of a quantity of money.
        /// </summary>
        public abstract class Money : UnitType
        {
        }

        /// <summary>
        /// Second is a specific quantity of time.
        /// </summary>
        public sealed class Second : Time
        {
            /// <summary>
            /// Singleton instance.
            /// </summary>
            public static readonly Second Instance = new Second();

            /// <summary>
            /// Prevents a default instance of the <see cref="Second" /> class from being created.
            /// </summary>
            private Second()
            {
            }

            /// <summary>
            /// Gets the standard abbreviation for the unit type.
            /// </summary>
            /// <value>The standard abbreviation for the unit type.</value>
            public override string StandardAbbreviation
            {
                get
                {
                    return "s";
                }
            }

            /// <summary>
            /// Gets the factor required to convert a unit of time to a number of seconds.
            /// </summary>
            /// <value>The factor required to convert a unit of time to a number of seconds.</value>
            public override int FactorToSeconds
            {
                get { return 1; }
            }
        }

        /// <summary>
        /// Minute is a specific quantity of time.
        /// </summary>
        public sealed class Minute : Time
        {
            /// <summary>
            /// Singleton instance.
            /// </summary>
            public static readonly Minute Instance = new Minute();

            /// <summary>
            /// Prevents a default instance of the <see cref="Minute" /> class from being created.
            /// </summary>
            private Minute()
            {
            }

            /// <summary>
            /// Gets the standard abbreviation for the unit type.
            /// </summary>
            /// <value>The standard abbreviation for the unit type.</value>
            public override string StandardAbbreviation
            {
                get
                {
                    return "min";
                }
            }

            /// <summary>
            /// Gets the factor required to convert a unit of time to a number of seconds.
            /// </summary>
            /// <value>The factor required to convert a unit of time to a number of seconds.</value>
            public override int FactorToSeconds
            {
                get { return 60; }
            }
        }

        /// <summary>
        /// Hour is a specific quantity of time.
        /// </summary>
        public sealed class Hour : Time
        {
            /// <summary>
            /// Singleton instance.
            /// </summary>
            public static readonly Hour Instance = new Hour();

            /// <summary>
            /// Prevents a default instance of the <see cref="Hour" /> class from being created.
            /// </summary>
            private Hour()
            {
            }

            /// <summary>
            /// Gets the standard abbreviation for the unit type.
            /// </summary>
            /// <value>The standard abbreviation for the unit type.</value>
            public override string StandardAbbreviation
            {
                get
                {
                    return "hr";
                }
            }

            /// <summary>
            /// Gets the factor required to convert a unit of time to a number of seconds.
            /// </summary>
            /// <value>The factor required to convert a unit of time to a number of seconds.</value>
            public override int FactorToSeconds
            {
                get { return 3600; }
            }
        }

        /// <summary>
        /// Day is a specific quantity of time.
        /// </summary>
        public sealed class Day : Time
        {
            /// <summary>
            /// Singleton instance.
            /// </summary>
            public static readonly Day Instance = new Day();

            /// <summary>
            /// Prevents a default instance of the <see cref="Day" /> class from being created.
            /// </summary>
            private Day()
            {
            }

            /// <summary>
            /// Gets the factor required to convert a unit of time to a number of seconds.
            /// </summary>
            /// <value>The factor required to convert a unit of time to a number of seconds.</value>
            public override int FactorToSeconds
            {
                get { return 24 * 3600; }
            }
        }

        /// <summary>
        /// Month is a specific quantity of time.
        /// </summary>
        public sealed class Month : Time
        {
            /// <summary>
            /// Singleton instance.
            /// </summary>
            public static readonly Month Instance = new Month();

            /// <summary>
            /// Prevents a default instance of the <see cref="Month" /> class from being created.
            /// </summary>
            private Month()
            {
            }

            /// <summary>
            /// Gets the factor required to convert a unit of time to a number of seconds.
            /// </summary>
            /// <value>The factor required to convert a unit of time to a number of seconds.</value>
            public override int FactorToSeconds
            {
                get
                {
                    // Since there isn't a fixed number, we won't allow this method to get called.
                    throw new DeveloperException(ErrorMessage.SystemError);
                }
            }
        }

        /// <summary>
        /// Year is a specific quantity of time.
        /// </summary>
        public sealed class Year : Time
        {
            /// <summary>
            /// Singleton instance.
            /// </summary>
            public static readonly Year Instance = new Year();

            /// <summary>
            /// Prevents a default instance of the <see cref="Year" /> class from being created.
            /// </summary>
            private Year()
            {
            }

            /// <summary>
            /// Gets the factor required to convert a unit of time to a number of seconds.
            /// </summary>
            /// <value>The factor required to convert a unit of time to a number of seconds.</value>
            public override int FactorToSeconds
            {
                get
                {
                    // Since there isn't a fixed number, we won't allow this method to get called.
                    throw new DeveloperException(ErrorMessage.SystemError);
                }
            }
        }

        /// <summary>
        /// Dollar is the base quantity of money in US currency.
        /// </summary>
        public sealed class Dollar : Money
        {
            /// <summary>
            /// Singleton instance.
            /// </summary>
            public static readonly Dollar Instance = new Dollar();

            /// <summary>
            /// Prevents a default instance of the <see cref="Dollar" /> class from being created.
            /// </summary>
            private Dollar()
            {
            }
        }

        /// <summary>
        /// We have some count fields that are meant to indicate the number of people/humans.
        /// </summary>
        public sealed class Human : UnitType
        {
            /// <summary>
            /// Singleton instance.
            /// </summary>
            public static readonly Human Instance = new Human();

            /// <summary>
            /// Prevents a default instance of the <see cref="Human" /> class from being created.
            /// </summary>
            private Human()
            {
            }
        }

        /// <summary>
        /// Unit representing a late payment on a financial debt obligation, 
        /// such as a mortgage or credit card.
        /// </summary>
        public sealed class Late : UnitType
        {
            /// <summary>
            /// Singleton instance.
            /// </summary>
            public static readonly Late Instance = new Late();

            /// <summary>
            /// Prevents a default instance of the <see cref="Late" /> class from being created.
            /// </summary>
            private Late()
            {
            }
        }
    }
}
