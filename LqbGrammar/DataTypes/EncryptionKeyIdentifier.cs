﻿namespace LqbGrammar.DataTypes
{
    using System;

    /// <summary>
    /// Encapsulate the concept of an identifier for an encryption key.
    /// </summary>
    public struct EncryptionKeyIdentifier : IEquatable<EncryptionKeyIdentifier>
    {
        /// <summary>
        /// An instance of the class that fails validation, can be used for initialization when necessary.
        /// </summary>
        public static readonly EncryptionKeyIdentifier BadIdentifier = new EncryptionKeyIdentifier(Guid.Empty);

        /// <summary>
        /// Identifier for the default encryption key.
        /// </summary>
        public static readonly EncryptionKeyIdentifier Default = new EncryptionKeyIdentifier(Guid.Parse("AAFE2ACE-2CA4-40FD-BC56-3FA79EA9F7E9"));

        /// <summary>
        /// Identifier for the BarcodeUploader encryption key.
        /// </summary>
        public static readonly EncryptionKeyIdentifier BarcodeUploader = new EncryptionKeyIdentifier(Guid.Parse("5AB6FE5E-C593-4047-BF9E-B28EF7203F80"));

        /// <summary>
        /// Identifier for the DocFramework encryption key.
        /// </summary>
        public static readonly EncryptionKeyIdentifier DocFramework = new EncryptionKeyIdentifier(Guid.Parse("FC4E0F7A-E7C7-4C79-AB90-00781FEF1255"));

        /// <summary>
        /// Identifier for the OCR encryption key.
        /// </summary>
        public static readonly EncryptionKeyIdentifier OCR = new EncryptionKeyIdentifier(Guid.Parse("FDD5CF0A-C171-4E10-9634-51D2D23068FA"));

        /// <summary>
        /// Identifier for the ComplianceEagle encryption key.
        /// </summary>
        public static readonly EncryptionKeyIdentifier ComplianceEagle = new EncryptionKeyIdentifier(Guid.Parse("7E98A7CD-8646-4597-9A74-DD69DE4AD779"));

        /// <summary>
        /// Identifier for the FnmaEarlyCheck encryption key.
        /// </summary>
        public static readonly EncryptionKeyIdentifier FnmaEarlyCheck = new EncryptionKeyIdentifier(Guid.Parse("8C90F0E4-D20D-435C-81CC-72E10FB03B78"));

        /// <summary>
        /// Identifier for the MeritMatrix encryption key.
        /// </summary>
        public static readonly EncryptionKeyIdentifier MeritMatrix = new EncryptionKeyIdentifier(Guid.Parse("96616B23-86B7-49FA-B23C-B20EF3971354"));

        /// <summary>
        /// Identifier for the DbConnectionString encryption key.
        /// </summary>
        public static readonly EncryptionKeyIdentifier DbConnectionString = new EncryptionKeyIdentifier(Guid.Parse("E05E6768-6AEA-46C5-B981-3D4C572F2015"));

        /// <summary>
        /// Identifier for the ThirdParties encryption key.
        /// </summary>
        public static readonly EncryptionKeyIdentifier ThirdParties = new EncryptionKeyIdentifier(Guid.Parse("{B8E1AC0F-0FE0-4246-9EED-7A02EA4DC9CE}"));

        /// <summary>
        /// Identifier for the Client Certificate password for third party calls.
        /// </summary>
        public static readonly EncryptionKeyIdentifier ClientCertificatePassword = new EncryptionKeyIdentifier(Guid.Parse("7c14b353-b941-490f-bbc6-75687f9239f9"));

        /// <summary>
        /// Identifier for KTA.
        /// </summary>
        public static readonly EncryptionKeyIdentifier Kta = new EncryptionKeyIdentifier(Guid.Parse("1665e9b2-9a5f-43d2-b519-4719ce2ccc9c"));

        /// <summary>
        /// Identifier for LPA user password.
        /// </summary>
        public static readonly EncryptionKeyIdentifier LpaUserPassword = new EncryptionKeyIdentifier(Guid.Parse("56E4364A-123C-44F9-BAF1-000016A79349"));

        /// <summary>
        /// Identifier for Audit password.
        /// </summary>
        public static readonly EncryptionKeyIdentifier AuditKeyPassword = new EncryptionKeyIdentifier(Guid.Parse("15BCE724-9089-43E8-B321-CB97AF402FE5"));

        /// <summary>
        /// Identifier for encrypt/decrypt data with web service proxy.
        /// </summary>
        public static readonly EncryptionKeyIdentifier WebserviceProxy = new EncryptionKeyIdentifier(new Guid("6346E55A-C305-49B6-A72C-E08994CFFAD9"));

        /// <summary>
        /// Encapsulated value, all identifiers are guids.
        /// </summary>
        private Guid value;

        /// <summary>
        /// Initializes a new instance of the <see cref="EncryptionKeyIdentifier"/> struct.
        /// </summary>
        /// <param name="value">The value.</param>
        private EncryptionKeyIdentifier(Guid value)
        {
            this.value = value;
        }

        /// <summary>
        /// Return the encapsulated value.
        /// </summary>
        public Guid Value => this.value;

        /// <summary>
        /// Attempt to parse the input value into a validated encryption key identifier.
        /// </summary>
        /// <param name="value">Input value that will be parsed.</param>
        /// <returns>Valid encryption key identifier, or null.</returns>
        public static EncryptionKeyIdentifier? Create(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return null;
            }

            Guid guid;
            bool ok = Guid.TryParse(value, out guid);
            if (ok && guid != Guid.Empty)
            {
                return new EncryptionKeyIdentifier(guid);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Attempt to parse the input value into a validated encryption key identifier.
        /// </summary>
        /// <param name="value">Input value that will be parsed.</param>
        /// <returns>Valid encryption key identifier, or null.</returns>
        public static EncryptionKeyIdentifier? Create(Guid value)
        {
            if (value != Guid.Empty)
            {
                return new EncryptionKeyIdentifier(value);
            }

            return null;
        }

        /// <summary>
        /// Static method that checks for equality between two instances.
        /// </summary>
        /// <param name="lhs">Instance on the left side of the == operator.</param>
        /// <param name="rhs">Instance on the right side of the == operator.</param>
        /// <returns>True if the two instances are equal, false otherwise.</returns>
        public static new bool Equals(object lhs, object rhs)
        {
            if (!(lhs is EncryptionKeyIdentifier))
            {
                return false;
            }

            if (!(rhs is EncryptionKeyIdentifier))
            {
                return false;
            }

            return ((EncryptionKeyIdentifier)lhs).Equals((EncryptionKeyIdentifier)rhs);
        }

        /// <summary>
        /// The == operator is implemented here.
        /// </summary>
        /// <param name="lhs">Instance on the left of the == operator.</param>
        /// <param name="rhs">Instance on the right of the == operator.</param>
        /// <returns>True if the two instances are equal, false otherwise.</returns>
        public static bool operator ==(EncryptionKeyIdentifier lhs, EncryptionKeyIdentifier rhs)
        {
            return lhs.Equals(rhs);
        }

        /// <summary>
        /// The != operator is implemented here.
        /// </summary>
        /// <param name="lhs">Instance on the left of the != operator.</param>
        /// <param name="rhs">Instance on the right of the != operator.</param>
        /// <returns>True if the two instances are not equal, false otherwise.</returns>
        public static bool operator !=(EncryptionKeyIdentifier lhs, EncryptionKeyIdentifier rhs)
        {
            return !lhs.Equals(rhs);
        }

        /// <summary>
        /// Return the encapsulated value in string format.
        /// </summary>
        /// <returns>Encapsulated value.</returns>
        public override string ToString()
        {
            return this.value.ToString("D");
        }

        /// <summary>
        /// Implement IEquatable.
        /// </summary>
        /// <param name="other">Instance that is checked for equality with this instance.</param>
        /// <returns>True if other is equal to this instance, false otherwise.</returns>
        public bool Equals(EncryptionKeyIdentifier other)
        {
            return this.value == other.value;
        }

        /// <summary>
        /// Override the Equals method inherited from the Object class.
        /// </summary>
        /// <param name="obj">Instance that is checked for equality with this instance.</param>
        /// <returns>True if obj is equal to this instance, false otherwise.</returns>
        public override bool Equals(object obj)
        {
            if (!(obj is EncryptionKeyIdentifier))
            {
                return false;
            }

            return this.Equals((EncryptionKeyIdentifier)obj);
        }

        /// <summary>
        /// Override of the method inherited from the Object class.
        /// </summary>
        /// <returns>An integer that is the hash code for this instance.</returns>
        public override int GetHashCode()
        {
            return this.value.GetHashCode();
        }
    }
}
