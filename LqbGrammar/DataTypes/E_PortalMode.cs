﻿namespace LqbGrammar.DataTypes
{
    /// <summary>
    /// Principal's portal mode.
    /// </summary>
    public enum E_PortalMode
    {
        /// <summary>
        /// Blank portal mode.
        /// </summary>
        Blank = 0,

        /// <summary>
        /// Broker portal mode.
        /// </summary>
        Broker = 1,

        /// <summary>
        /// Mini correspondent portal mode.
        /// </summary>
        MiniCorrespondent = 2,

        /// <summary>
        /// Correspondent portal mode.
        /// </summary>
        Correspondent = 3,

        /// <summary>
        /// Retail portal mode.
        /// </summary>
        Retail = 4
    }
}
