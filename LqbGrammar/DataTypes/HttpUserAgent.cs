﻿namespace LqbGrammar.DataTypes
{
    /// <summary>
    /// Occasionally the LQB code will set an explicit
    /// user agent string.  The values used are gathered
    /// here in order to encapsulate the concept.
    /// </summary>
    public struct HttpUserAgent
    {
        /// <summary>
        /// Mozilla5 user agent.
        /// </summary>
        public static readonly HttpUserAgent Mozilla5 = new HttpUserAgent("Mozilla/5.0");

        /// <summary>
        /// IE9 user agent.
        /// </summary>
        public static readonly HttpUserAgent IE9 = new HttpUserAgent("Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 7.1; Trident/5.0)");

        /// <summary>
        /// IE6 user agent.
        /// </summary>
        public static readonly HttpUserAgent IE8 = new HttpUserAgent("Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; Trident/4.0)");

        /// <summary>
        /// IE6 user agent.
        /// </summary>
        public static readonly HttpUserAgent IE6 = new HttpUserAgent("Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0; .NET CLR 1.0.3705)");

        /// <summary>
        /// LQB WebBots use this user agent.
        /// </summary>
        public static readonly HttpUserAgent LqbWebBot = IE6;

        /// <summary>
        /// The user agent string value.
        /// </summary>
        private string userAgent;

        /// <summary>
        /// Initializes a new instance of the <see cref="HttpUserAgent"/> struct.
        /// </summary>
        /// <param name="userAgent">The user agent string value.</param>
        private HttpUserAgent(string userAgent)
        {
            this.userAgent = userAgent;
        }

        /// <summary>
        /// Override the Object.ToString() method.
        /// </summary>
        /// <returns>The user agent string value.</returns>
        public override string ToString()
        {
            return this.userAgent;
        }
    }
}
