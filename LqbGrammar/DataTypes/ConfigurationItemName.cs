﻿namespace LqbGrammar.DataTypes
{
    using LqbGrammar.Exceptions;
    using LqbGrammar.Validation;

    /// <summary>
    /// Encapsulate and validate the name of a configuration item name.
    /// </summary>
    public struct ConfigurationItemName : System.IEquatable<ConfigurationItemName>
    {
        /// <summary>
        /// An instance of an invalid value that can be used for variable initialization.
        /// </summary>
        public static readonly ConfigurationItemName BadName = new ConfigurationItemName(string.Empty);

        /// <summary>
        /// The name of the configuration item name.
        /// </summary>
        private string name;

        /// <summary>
        /// Initializes a new instance of the <see cref="ConfigurationItemName"/> struct.
        /// </summary>
        /// <param name="name">The configuration item name.</param>
        private ConfigurationItemName(string name)
        {
            this.name = name;
        }

        /// <summary>
        /// Validate the input configuration item name.
        /// </summary>
        /// <param name="name">The input configuration item name to be validated.</param>
        /// <returns>The validated name, or null.</returns>
        public static ConfigurationItemName? Create(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                return null;
            }

            RegularExpressionResult result = RegularExpressionEngine.Execute(name, RegularExpressionString.ConfigurationItemName);
            if (!result.IsMatch())
            {
                return null;
            }

            return new ConfigurationItemName(name);
        }

        /// <summary>
        /// Compare two objects for equality and that they are both validated configuration item names.
        /// </summary>
        /// <param name="lhs">Object on the left side of the == operator.</param>
        /// <param name="rhs">Object on the right side of the == operator.</param>
        /// <returns>True if both objects are instances of the ConfigurationItemName class, and are equal to each other.</returns>
        public static new bool Equals(object lhs, object rhs)
        {
            if (!(lhs is ConfigurationItemName))
            {
                return false;
            }

            if (!(rhs is ConfigurationItemName))
            {
                return false;
            }

            return ((ConfigurationItemName)lhs).Equals((ConfigurationItemName)rhs);
        }

        /// <summary>
        /// The equality operator for comparing two instances of a valid configuration item name.
        /// </summary>
        /// <param name="lhs">The instance on the left side of the == operator.</param>
        /// <param name="rhs">The instance on the right side of the == operator.</param>
        /// <returns>True if the two instances have the same name.</returns>
        public static bool operator ==(ConfigurationItemName lhs, ConfigurationItemName rhs)
        {
            return lhs.Equals(rhs);
        }

        /// <summary>
        /// The inequality operator for comparing two instances of a valid configuration item name.
        /// </summary>
        /// <param name="lhs">The instance on the left side of the != operator.</param>
        /// <param name="rhs">The instance on the right side of the != operator.</param>
        /// <returns>True if the two instances are not equal to each other.</returns>
        public static bool operator !=(ConfigurationItemName lhs, ConfigurationItemName rhs)
        {
            return !lhs.Equals(rhs);
        }

        /// <summary>
        /// Override the implementation inherited from the Object class.
        /// </summary>
        /// <returns>The configuration item name.</returns>
        public override string ToString()
        {
            return this.name;
        }

        /// <summary>
        /// Test this instance against another for equality.
        /// </summary>
        /// <param name="other">The other instance.</param>
        /// <returns>True if the two instances are equal, false otherwise.</returns>
        public bool Equals(ConfigurationItemName other)
        {
            return this.name == other.name;
        }

        /// <summary>
        /// Test this instance against another object for equality.
        /// </summary>
        /// <param name="obj">The other object.</param>
        /// <returns>True if the other object is a validated configuration item name and the names are equal.</returns>
        public override bool Equals(object obj)
        {
            if (!(obj is ConfigurationItemName))
            {
                return false;
            }

            return Equals((ConfigurationItemName)obj);
        }

        /// <summary>
        /// Override the implementation inherited from the Object class.
        /// </summary>
        /// <returns>An integer that is the hash of this instance.</returns>
        public override int GetHashCode()
        {
            return this.name.GetHashCode();
        }
    }
}
