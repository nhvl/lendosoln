﻿namespace LqbGrammar.DataTypes
{
    using System;
    using System.Data;
    using Drivers.SecurityEventLogging;

    /// <summary>
    /// Security event log object.
    /// </summary>
    public class SecurityEventLog
    {
        /// <summary>
        /// Initializes a new instance of the<see cref="SecurityEventLog" /> class.
        /// </summary>
        /// <param name="reader">IDataReader object.</param>
        public SecurityEventLog(IDataReader reader)
        {
            this.EventType = (SecurityEventType)Convert.ToInt32(reader["EventType"]);
            this.DescriptionText = (string)reader["DescriptionText"];
            this.UserFirstNm = (string)reader["UserFirstNm"];
            this.UserLastNm = (string)reader["UserLastNm"];
            this.LoginNm = (string)reader["LoginNm"];
            this.IsInternalUserAction = (bool)reader["IsInternalUserAction"];
            this.IsSystemAction = (bool)reader["IsSystemAction"];
            this.CreatedDate = DateTime.Parse(reader["CreatedDate"].ToString());
            this.ClientIP = (string)reader["ClientIP"];
            this.PrincipalType = (PrincipalTypeT)Convert.ToInt32(reader["PrincipalType"]);
        }

        /// <summary>
        /// Gets or sets created date.
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets client IP address.
        /// </summary>
        public string ClientIP { get; set; }

        /// <summary>
        /// Gets or sets security Event type.
        /// </summary>
        public SecurityEventType EventType { get; set; }

        /// <summary>
        /// Gets or sets broker id.
        /// </summary>
        public Guid BrokerId { get; set; }

        /// <summary>
        /// Gets or sets user id.
        /// </summary>
        public Guid? UserId { get; set; }

        /// <summary>
        /// Gets or sets description text.
        /// </summary>
        public string DescriptionText { get; set; }

        /// <summary>
        /// Gets or sets user first name.
        /// </summary>
        public string UserFirstNm { get; set; }

        /// <summary>
        /// Gets or sets user last name.
        /// </summary>
        public string UserLastNm { get; set; }

        /// <summary>
        /// Gets or sets user log in name.
        /// </summary>
        public string LoginNm { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the user the log is internal.
        /// </summary>
        public bool IsInternalUserAction { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the user the log is from system.
        /// </summary>
        public bool IsSystemAction { get; set; }

        /// <summary>
        /// Gets or sets principal type.
        /// </summary>
        public PrincipalTypeT PrincipalType { get; set; }

        /// <summary>
        /// Gets user full name.
        /// </summary>
        public string UserFullNm
        {
            get
            {
                string fullNm = this.UserFirstNm + " " + this.UserLastNm;

                if (string.IsNullOrWhiteSpace(fullNm))
                {
                    return "N/A";
                }

                if (this.IsInternalUserAction)
                {
                    return "LQB User (aliased as " + fullNm + ")";
                }

                if (this.IsSystemAction)
                {
                    return "LQB System (aliased as " + fullNm + ")";
                }

                return fullNm;
            }
        }

        /// <summary>
        /// Gets security event type to be sent to the page.
        /// </summary>
        public string SecurityEventType
        {
            get
            {
                switch (this.EventType)
                {
                    case Drivers.SecurityEventLogging.SecurityEventType.AccountCreation:
                        return "Account Creation";
                    case Drivers.SecurityEventLogging.SecurityEventType.AccountLocking:
                        return "Account Lock";
                    case Drivers.SecurityEventLogging.SecurityEventType.BecomeUser:
                        return "Become User";
                    case Drivers.SecurityEventLogging.SecurityEventType.ChangingUserSettings:
                        return "Changing User Settings";
                    case Drivers.SecurityEventLogging.SecurityEventType.LogIn:
                        return "Log In";
                    case Drivers.SecurityEventLogging.SecurityEventType.LogOut:
                        return "Log Out";
                    case Drivers.SecurityEventLogging.SecurityEventType.SensitiveReportExport:
                        return "Sensitive Report Export";
                    case Drivers.SecurityEventLogging.SecurityEventType.WebserviceAuthentication:
                        return "Webservice Authentication";
                    case Drivers.SecurityEventLogging.SecurityEventType.FailedLogin:
                        return "Failed Log In";
                    case Drivers.SecurityEventLogging.SecurityEventType.FailedWebserviceAuthentication:
                        return "Failed Webservice Authentication";
                    default:
                        throw new NotImplementedException("This SecurityEventType is not yet supported");
                }
            }
        }

        /// <summary>
        /// Gets time stamp formatted for the page.
        /// </summary>
        public string TimeStamp
        {
            get
            {
                return this.CreatedDate.ToString("G") + " P" + (TimeZoneInfo.FindSystemTimeZoneById("Pacific Standard Time").IsDaylightSavingTime(this.CreatedDate) ? "DT" : "ST");
            }
        }

        /// <summary>
        /// Gets user type formatted for the page.
        /// </summary>
        public string UserType
        {
            get
            {
                if (this.IsInternalUserAction || this.IsSystemAction)
                {
                    return "System";
                }

                if (this.UserId == Guid.Empty)
                {
                    return "Broker";
                }

                switch (this.PrincipalType)
                {
                    case PrincipalTypeT.LendersOffice:
                        return "Employee";
                    case PrincipalTypeT.OriginatorPortal:
                        return "OC User";
                    case PrincipalTypeT.ConsumerPortal:
                        return "Consumer";
                    case PrincipalTypeT.Internal:
                        return "System";
                    case PrincipalTypeT.Unknown:
                        return "Unknown";
                    default:
                        throw new NotImplementedException($"Principal type {this.PrincipalType.ToString()} has not yet been implemented.");
                }                
            }
        }
    }
}
