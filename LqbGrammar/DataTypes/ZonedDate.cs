﻿namespace LqbGrammar.DataTypes
{
    using System;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Encapsulate an event that occurs within a timezone and on a given date,
    /// with no greater precision than that.
    /// </summary>
    public struct ZonedDate : IEquatable<ZonedDate>, IComparable, IComparable<ZonedDate>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ZonedDate"/> struct.
        /// </summary>
        /// <param name="date">The date information.</param>
        /// <param name="timezone">The time zone.</param>
        private ZonedDate(UnzonedDate date, LqbTimeZone timezone)
        {
            this.UnzonedDate = date;
            this.Zone = timezone;
        }

        /// <summary>
        /// Gets the equivalent DateTimeOffset for this date.
        /// </summary>
        /// <value>The equivalent DateTimeOffset for this date.</value>
        public DateTimeOffset Date
        {
            get
            {
                var info = (TimeZoneInfo)this.Zone;
                var date = this.UnzonedDate.Date;
                var offset = info.GetUtcOffset(date);
                return new DateTimeOffset(date, offset);
            }
        }

        /// <summary>
        /// Gets or sets the date information.
        /// </summary>
        /// <value>The date information.</value>
        private UnzonedDate UnzonedDate { get; set; }

        /// <summary>
        /// Gets or sets the time zone.
        /// </summary>
        /// <value>The time zone.</value>
        private LqbTimeZone Zone { get; set; }

        /// <summary>
        /// Create an instance of ZonedDate.
        /// </summary>
        /// <param name="year">The year value.</param>
        /// <param name="month">The month value.</param>
        /// <param name="day">The day value.</param>
        /// <param name="timezone">The timezone.</param>
        /// <returns>An instance of ZonedDate.</returns>
        public static ZonedDate? Create(int year, int month, int day, LqbTimeZone timezone)
        {
            var unzoned = UnzonedDate.Create(year, month, day);
            if (unzoned == null)
            {
                return null;
            }

            return new ZonedDate(unzoned.Value, timezone);
        }

        /// <summary>
        /// Create an instance of ZonedDate.
        /// </summary>
        /// <param name="value">A string representation of the date.</param>
        /// <returns>An instance of ZonedDate.</returns>
        public static ZonedDate? Create(string value)
        {
            string working = value.Trim();
            int index = working.LastIndexOf(' ');
            if (index <= 0)
            {
                return null;
            }

            try
            {
                var timezone = LqbTimeZone.Create(working.Substring(index + 1));
                var date = UnzonedDate.Create(working.Substring(0, index).TrimEnd());
                if (date == null)
                {
                    return null;
                }
                else
                {
                    return new ZonedDate(date.Value, timezone);
                }
            }
            catch (LqbException)
            {
                return null;
            }
        }

        /// <summary>
        /// Implements the equality operator for ZonedDates.
        /// </summary>
        /// <param name="lhs">The ZonedDate on the left-hand side of the == operator.</param>
        /// <param name="rhs">The ZonedDate on the right-hand side of the == operator.</param>
        /// <returns>True if the ZonedDates represent the same calendar date in the same time zone.</returns>
        public static bool operator ==(ZonedDate lhs, ZonedDate rhs)
        {
            return lhs.CompareTo(rhs) == 0;
        }

        /// <summary>
        /// Implements the inequality operator for ZonedDates.
        /// </summary>
        /// <param name="lhs">The ZonedDate on the left-hand side of the != operator.</param>
        /// <param name="rhs">The ZonedDate on the right-hand side of the != operator.</param>
        /// <returns>True if the ZonedDates represent different calendar dates or have different time zones.</returns>
        public static bool operator !=(ZonedDate lhs, ZonedDate rhs)
        {
            return lhs.CompareTo(rhs) != 0;
        }

        /// <summary>
        /// Implement the less than operator for ZonedDates.
        /// </summary>
        /// <param name="lhs">The ZonedDate on the left-hand side of the &lt; operator.</param>
        /// <param name="rhs">The ZonedDate on the right-hand side of the &lt; operator.</param>
        /// <returns>The operator result.</returns>
        public static bool operator <(ZonedDate lhs, ZonedDate rhs)
        {
            return lhs.CompareTo(rhs) < 0;
        }

        /// <summary>
        /// Implement the greater than operator for ZonedDates.
        /// </summary>
        /// <param name="lhs">The ZonedDate on the left-hand side of the &gt; operator.</param>
        /// <param name="rhs">The ZonedDate on the right-hand side of the &gt; operator.</param>
        /// <returns>The operator result.</returns>
        public static bool operator >(ZonedDate lhs, ZonedDate rhs)
        {
            return lhs.CompareTo(rhs) > 0;
        }

        /// <summary>
        /// Implement the less than or equal operator for ZonedDates.
        /// </summary>
        /// <param name="lhs">The ZonedDate on the left-hand side of the &lt;= operator.</param>
        /// <param name="rhs">The ZonedDate on the right-hand side of the &lt;= operator.</param>
        /// <returns>The operator result.</returns>
        public static bool operator <=(ZonedDate lhs, ZonedDate rhs)
        {
            return lhs.CompareTo(rhs) <= 0;
        }

        /// <summary>
        /// Implement the greater than or equal operator for ZonedDates.
        /// </summary>
        /// <param name="lhs">The ZonedDate on the left-hand side of the &gt;= operator.</param>
        /// <param name="rhs">The ZonedDate on the right-hand side of the &gt;= operator.</param>
        /// <returns>The operator result.</returns>
        public static bool operator >=(ZonedDate lhs, ZonedDate rhs)
        {
            return lhs.CompareTo(rhs) >= 0;
        }

        /// <summary>
        /// Override the implementation inherited from System.Object.
        /// </summary>
        /// <param name="obj">The object being compared with this instance.</param>
        /// <returns>True if the object is an ZonedDate and the two dates are equal, false otherwise.</returns>
        public override bool Equals(object obj)
        {
            return obj is ZonedDate && this.Equals((ZonedDate)obj);
        }

        /// <summary>
        /// Implement IEquatable&lt;ZonedDate&gt;.
        /// </summary>
        /// <param name="other">The date being compared with this instance.</param>
        /// <returns>True if the two dates are equal, false otherwise.</returns>
        public bool Equals(ZonedDate other)
        {
            return this.CompareTo(other) == 0;
        }

        /// <summary>
        /// Implement IComparable.
        /// </summary>
        /// <param name="obj">The object being compared with this instance.</param>
        /// <returns>The result of the comparison calculation.</returns>
        public int CompareTo(object obj)
        {
            if (obj is ZonedDate)
            {
                return this.CompareTo((ZonedDate)obj);
            }
            else
            {
                throw new InvalidCastException("Object cannot be compared with ZonedDate");
            }
        }

        /// <summary>
        /// Implement IComparble&lt;ZonedDate&gt;.
        /// </summary>
        /// <param name="other">The date being compared with this instance.</param>
        /// <returns>The result of the comparison calculation.</returns>
        public int CompareTo(ZonedDate other)
        {
            var thisUTC = this.Date.UtcDateTime;
            var otherUTC = other.Date.UtcDateTime;
            return thisUTC.CompareTo(otherUTC);
        }

        /// <summary>
        /// Override the implementation inherited from System.Object.
        /// </summary>
        /// <returns>An integer hash code consistent with the equality semantics.</returns>
        public override int GetHashCode()
        {
            return this.Date.UtcDateTime.GetHashCode();
        }

        /// <summary>
        /// Override the implementation inherited from System.Object.
        /// </summary>
        /// <returns>A string representation of the ZonedDate.</returns>
        public override string ToString()
        {
            return string.Format("{0} {1}", this.UnzonedDate.ToString(), this.Zone.GeneralAbbreviation);
        }
    }
}
