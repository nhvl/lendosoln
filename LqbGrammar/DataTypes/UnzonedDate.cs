﻿namespace LqbGrammar.DataTypes
{
    using System;

    using dayStorage = System.Byte;
    using monthStorage = System.Byte;
    using yearStorage = System.Int16;

    /// <summary>
    /// Encapsulate the concept of a calendar date, and NOT a span of time equivalent to a day.
    /// </summary>
    public struct UnzonedDate : ISemanticType, IEquatable<UnzonedDate>, IComparable, IComparable<UnzonedDate>
    {
        /// <summary>
        /// Format string used when serializing/deserializing an UnzonedDate.
        /// </summary>
        public const string FormatString = "yyyy-MM-dd";

        /// <summary>
        /// Initializes a new instance of the <see cref="UnzonedDate"/> struct.
        /// </summary>
        /// <param name="year">The year value.</param>
        /// <param name="month">The month value.</param>
        /// <param name="day">The day value.</param>
        private UnzonedDate(yearStorage year, monthStorage month, dayStorage day)
        {
            this.StoredYear = year;
            this.StoredMonth = month;
            this.StoredDay = day;
        }

        /// <summary>
        /// Gets the equivalent DateTime for this date.
        /// </summary>
        /// <value>The equivalent DateTime for this date.</value>
        public DateTime Date
        {
            get { return new DateTime((int)this.StoredYear, (int)this.StoredMonth, (int)this.StoredDay, 0, 0, 0, DateTimeKind.Unspecified); }
        }

        /// <summary>
        /// Gets the underlying DateTime value as an object.
        /// </summary>
        object ISemanticType.Value => this.Date;

        /// <summary>
        /// Gets or sets the stored year value.
        /// </summary>
        /// <value>The stored year value.</value>
        private yearStorage StoredYear { get; set; }

        /// <summary>
        /// Gets or sets the stored month value.
        /// </summary>
        /// <value>The stored month value.</value>
        private monthStorage StoredMonth { get; set; }

        /// <summary>
        /// Gets or sets the stored day value.
        /// </summary>
        /// <value>The stored day value.</value>
        private dayStorage StoredDay { get; set; }

        /// <summary>
        /// Create an instance of UnzonedDate.
        /// </summary>
        /// <param name="year">The year value.</param>
        /// <param name="month">The month value.</param>
        /// <param name="day">The day value.</param>
        /// <returns>An instance of UnzonedDate.</returns>
        public static UnzonedDate? Create(int year, int month, int day)
        {
            if (year > yearStorage.MaxValue || year < yearStorage.MinValue)
            {
                return null;
            }
            else if (month > 12 || month < 1)
            {
                return null;
            }
            else if (day > DateTime.DaysInMonth(year, month) || day < 1)
            {
                return null;
            }
            else
            {
                return new UnzonedDate((yearStorage)year, (monthStorage)month, (dayStorage)day);
            }
        }

        /// <summary>
        /// Create an instance of UnzonedDate.
        /// </summary>
        /// <param name="date">A DateTime instance.</param>
        /// <returns>An instance of UnzonedDate.</returns>
        public static UnzonedDate? Create(DateTime date)
        {
            return Create(date.Year, date.Month, date.Day);
        }

        /// <summary>
        /// Create an instance of UnzonedDate.
        /// </summary>
        /// <param name="value">A string representation of the date.</param>
        /// <returns>An instance of UnzonedDate.</returns>
        public static UnzonedDate? Create(string value)
        {
            DateTimeOffset result;
            if (DateTimeOffset.TryParseExact(value, FormatString, System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.AssumeUniversal, out result))
            {
                return Create(result.Year, result.Month, result.Day);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Implement the equality operator for UnzonedDates.
        /// </summary>
        /// <param name="lhs">The UnzonedDate on the left-hand side of the == operator.</param>
        /// <param name="rhs">The UnzonedDate on the right-hand side of the == operator.</param>
        /// <returns>True if the UnzonedDates represent the same wall clock date and time.</returns>
        public static bool operator ==(UnzonedDate lhs, UnzonedDate rhs)
        {
            return lhs.CompareTo(rhs) == 0;
        }

        /// <summary>
        /// Implement the inequality operator for UnzonedDates.
        /// </summary>
        /// <param name="lhs">The UnzonedDate on the left-hand side of the != operator.</param>
        /// <param name="rhs">The UnzonedDate on the right-hand side of the != operator.</param>
        /// <returns>True if the UnzonedDates represent different wall clock dates or times.</returns>
        public static bool operator !=(UnzonedDate lhs, UnzonedDate rhs)
        {
            return lhs.CompareTo(rhs) != 0;
        }

        /// <summary>
        /// Implement the less than operator for UnzonedDates.
        /// </summary>
        /// <param name="lhs">The UnzonedDate on the left-hand side of the &lt; operator.</param>
        /// <param name="rhs">The UnzonedDate on the right-hand side of the &lt; operator.</param>
        /// <returns>True if the left-hand side represents a calendar date before the right-hand side.</returns>
        public static bool operator <(UnzonedDate lhs, UnzonedDate rhs)
        {
            return lhs.CompareTo(rhs) < 0;
        }

        /// <summary>
        /// Implement the greater than operator for UnzonedDates.
        /// </summary>
        /// <param name="lhs">The UnzonedDate on the left-hand side of the &gt; operator.</param>
        /// <param name="rhs">The UnzonedDate on the right-hand side of the &gt; operator.</param>
        /// <returns>True if the left-hand side represents a calendar date after the right-hand side.</returns>
        public static bool operator >(UnzonedDate lhs, UnzonedDate rhs)
        {
            return lhs.CompareTo(rhs) > 0;
        }

        /// <summary>
        /// Implement the less than or equal operator for UnzonedDates.
        /// </summary>
        /// <param name="lhs">The UnzonedDate on the left-hand side of the &lt;= operator.</param>
        /// <param name="rhs">The UnzonedDate on the right-hand side of the &lt;= operator.</param>
        /// <returns>True if the left-hand side represents a calendar date on or before the right-hand side.</returns>
        public static bool operator <=(UnzonedDate lhs, UnzonedDate rhs)
        {
            return lhs.CompareTo(rhs) <= 0;
        }

        /// <summary>
        /// Implement the greater than or equal operator for UnzonedDates.
        /// </summary>
        /// <param name="lhs">The UnzonedDate on the left-hand side of the &gt;= operator.</param>
        /// <param name="rhs">The UnzonedDate on the right-hand side of the &gt;= operator.</param>
        /// <returns>True if the left-hand side represents a calendar date on or after the right-hand side.</returns>
        public static bool operator >=(UnzonedDate lhs, UnzonedDate rhs)
        {
            return lhs.CompareTo(rhs) >= 0;
        }

        /// <summary>
        /// Override the implementation inherited from System.Object.
        /// </summary>
        /// <param name="obj">The object being compared with this instance.</param>
        /// <returns>True if the object is an UnzonedDate and the two dates are equal, false otherwise.</returns>
        public override bool Equals(object obj)
        {
            return obj is UnzonedDate && this.Equals((UnzonedDate)obj);
        }

        /// <summary>
        /// Implement IEquatable&lt;UnzonedDate&gt;.
        /// </summary>
        /// <param name="other">The date being compared with this instance.</param>
        /// <returns>True if the two dates are equal, false otherwise.</returns>
        public bool Equals(UnzonedDate other)
        {
            return this.CompareTo(other) == 0;
        }

        /// <summary>
        /// Implement IComparable.
        /// </summary>
        /// <param name="obj">The object being compared with this instance.</param>
        /// <returns>The result of the comparison calculation.</returns>
        public int CompareTo(object obj)
        {
            if (obj is UnzonedDate)
            {
                return this.CompareTo((UnzonedDate)obj);
            }
            else
            {
                throw new InvalidCastException("Object cannot be compared with UnzonedDate");
            }
        }

        /// <summary>
        /// Implement IComparable&lt;UnzonedDate&gt;.
        /// </summary>
        /// <param name="other">The date being compared with this instance.</param>
        /// <returns>The result of the comparison calculation.</returns>
        public int CompareTo(UnzonedDate other)
        {
            return this.Date.CompareTo(other.Date);
        }

        /// <summary>
        /// Override the implementation inherited from System.Object.
        /// </summary>
        /// <returns>An integer hash code consistent with the equality semantics.</returns>
        public override int GetHashCode()
        {
            return this.ToString().GetHashCode();
        }

        /// <summary>
        /// Override the implementation inherited from System.Object.
        /// </summary>
        /// <returns>A string representation of this instance.</returns>
        public override string ToString()
        {
            return this.Date.ToString(FormatString);
        }
    }
}
