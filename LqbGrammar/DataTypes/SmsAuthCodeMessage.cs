﻿namespace LqbGrammar.DataTypes
{
    using LqbGrammar.Validation;

    /// <summary>
    /// Encapsulate a validated message containing an authentication code for transmission via SMS.
    /// </summary>
    public struct SmsAuthCodeMessage : System.IEquatable<SmsAuthCodeMessage>
    {
        /// <summary>
        /// An instance of a bad authentication code that can be used for variable initialization.
        /// </summary>
        public static readonly SmsAuthCodeMessage Invalid = new SmsAuthCodeMessage(string.Empty);

        /// <summary>
        /// The authentication method.
        /// </summary>
        private string message;

        /// <summary>
        /// Initializes a new instance of the <see cref="SmsAuthCodeMessage"/> struct.
        /// </summary>
        /// <param name="message">The SMS authentication message.</param>
        private SmsAuthCodeMessage(string message)
        {
            this.message = message;
        }

        /// <summary>
        /// Parse the input authentication code and validate it.
        /// </summary>
        /// <param name="source">The source in the message.</param>
        /// <param name="code">The authentication code.</param>
        /// <returns>The validated message, or null.</returns>
        public static SmsAuthCodeMessage? Create(string source, string code)
        {
            string message = string.Format("{0} Authentication Code: {1}", source, code);
            RegularExpressionResult result = RegularExpressionEngine.Execute(message, RegularExpressionString.SmsAuthCodeMessage);
            if (!result.IsMatch())
            {
                return null;
            }

            return new SmsAuthCodeMessage(message);
        }

        /// <summary>
        /// Compare to objects for equality and that they are instances of the SmsAuthCodeMessage class.
        /// </summary>
        /// <param name="lhs">The object on the left side of the == operator.</param>
        /// <param name="rhs">The object on the right side of the == operator.</param>
        /// <returns>True if both objects are instances of the SmsAuthCodeMessage class and equal to each other.</returns>
        public static new bool Equals(object lhs, object rhs)
        {
            if (!(lhs is SmsAuthCodeMessage))
            {
                return false;
            }

            if (!(rhs is SmsAuthCodeMessage))
            {
                return false;
            }

            return ((SmsAuthCodeMessage)lhs).Equals((SmsAuthCodeMessage)rhs);
        }

        /// <summary>
        /// The equalitiy operator.
        /// </summary>
        /// <param name="lhs">The instance that is on the left side of the == operator.</param>
        /// <param name="rhs">The instance that is on the right side of the == operator.</param>
        /// <returns>True if the instances are equal, false otherwise.</returns>
        public static bool operator ==(SmsAuthCodeMessage lhs, SmsAuthCodeMessage rhs)
        {
            return lhs.Equals(rhs);
        }

        /// <summary>
        /// The inequality operator.
        /// </summary>
        /// <param name="lhs">The instance that is on the left side of the != operator.</param>
        /// <param name="rhs">The instance that is on the right side of the != operator.</param>
        /// <returns>True if the two instances are not equal, false otherwise.</returns>
        public static bool operator !=(SmsAuthCodeMessage lhs, SmsAuthCodeMessage rhs)
        {
            return !lhs.Equals(rhs);
        }

        /// <summary>
        /// Override the implementation inherited from the Object class.
        /// </summary>
        /// <returns>The authentication code.</returns>
        public override string ToString()
        {
            return this.message;
        }

        /// <summary>
        /// Compare this instance with another instance for equality.
        /// </summary>
        /// <param name="other">The other instance.</param>
        /// <returns>True if the two instances are equal.</returns>
        public bool Equals(SmsAuthCodeMessage other)
        {
            return this.message == other.message;
        }

        /// <summary>
        /// Compare this instance with another object for equality.
        /// </summary>
        /// <param name="obj">The other object.</param>
        /// <returns>True if the other object is an instance of the SmsAuthCodeMessage class and it is equal to this instance.</returns>
        public override bool Equals(object obj)
        {
            if (!(obj is SmsAuthCodeMessage))
            {
                return false;
            }

            return Equals((SmsAuthCodeMessage)obj);
        }

        /// <summary>
        /// Override the implementation inherited from the Object class.
        /// </summary>
        /// <returns>An integer that is the hash of this object.</returns>
        public override int GetHashCode()
        {
            return this.message.GetHashCode();
        }
    }
}
