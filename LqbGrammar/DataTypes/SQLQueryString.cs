﻿namespace LqbGrammar.DataTypes
{
    using LqbGrammar.Validation;

    /// <summary>
    /// Wrapper around a query string for interacting with an SQL database.
    /// </summary>
    public struct SQLQueryString : System.IEquatable<SQLQueryString>
    {
        /// <summary>
        /// In invalid value that can be used for initialization.
        /// </summary>
        public static readonly SQLQueryString Invalid = new SQLQueryString(string.Empty);

        /// <summary>
        /// The query string.
        /// </summary>
        private string query;

        /// <summary>
        /// Initializes a new instance of the <see cref="SQLQueryString"/> struct.
        /// </summary>
        /// <param name="query">The query string to be wrapped.</param>
        private SQLQueryString(string query)
        {
            this.query = query;
        }

        /// <summary>
        /// Gets the encapsulated query string.
        /// </summary>
        /// <value>The query string is returned.</value>
        public string Value
        {
            get { return this.query; }
        }

        /// <summary>
        /// Validate the input query string.
        /// </summary>
        /// <param name="query">The query string to be validated.</param>
        /// <returns>The validated query string, or null.</returns>
        public static SQLQueryString? Create(string query)
        {
            if (string.IsNullOrEmpty(query))
            {
                return null;
            }

            bool isValid = Validate(query);
            if (!isValid)
            {
                return null;
            }
            else
            {
                return new SQLQueryString(query);
            }
        }

        /// <summary>
        /// Compare two instances for equality.
        /// </summary>
        /// <param name="lhs">Instance on the left side of the == operator.</param>
        /// <param name="rhs">Instance on the right side of the == operator.</param>
        /// <returns>True if the instances are equal, false otherwise.</returns>
        public static new bool Equals(object lhs, object rhs)
        {
            if (!(lhs is SQLQueryString))
            {
                return false;
            }

            if (!(rhs is SQLQueryString))
            {
                return false;
            }

            return ((SQLQueryString)lhs).Equals((SQLQueryString)rhs);
        }

        /// <summary>
        /// Implementation of the == operator.
        /// </summary>
        /// <param name="lhs">Instance on the left side of the == operator.</param>
        /// <param name="rhs">Instance on the right side of the == operator.</param>
        /// <returns>True if the instances are equal, false otherwise.</returns>
        public static bool operator ==(SQLQueryString lhs, SQLQueryString rhs)
        {
            return lhs.Equals(rhs);
        }

        /// <summary>
        /// Implementation of the != operator.
        /// </summary>
        /// <param name="lhs">Instance on the left side of the != operator.</param>
        /// <param name="rhs">Instance on the right side of the != operator.</param>
        /// <returns>True if the instances are not equal, false otherwise.</returns>
        public static bool operator !=(SQLQueryString lhs, SQLQueryString rhs)
        {
            return !lhs.Equals(rhs);
        }

        /// <summary>
        /// Override the method inherited from the Object class.
        /// </summary>
        /// <returns>The regular expression string that is encapsulated.</returns>
        public override string ToString()
        {
            return this.Value;
        }

        /// <summary>
        /// Implementation of the IEquatable interface.
        /// </summary>
        /// <param name="other">Instance that is compared to this instance for equality.</param>
        /// <returns>True if the instances are equal, false otherwise.</returns>
        public bool Equals(SQLQueryString other)
        {
            return this.query == other.query;
        }

        /// <summary>
        /// Override the method inherited from the Object class.
        /// </summary>
        /// <param name="obj">The instance that will be compared with this instance for equality.</param>
        /// <returns>True if the instances are equal, flase otherwise.</returns>
        public override bool Equals(object obj)
        {
            if (!(obj is SQLQueryString))
            {
                return false;
            }

            return Equals((SQLQueryString)obj);
        }

        /// <summary>
        /// Override of the method inherited from the Object class.
        /// </summary>
        /// <returns>An integer that is the hash of this instance.</returns>
        public override int GetHashCode()
        {
            return this.query.GetHashCode();
        }

        /// <summary>
        /// Validate the query.
        /// </summary>
        /// <param name="query">The query to be validated.</param>
        /// <returns>True if the query is valid, false otherwise.</returns>
        private static bool Validate(string query)
        {
            RegularExpressionResult result = RegularExpressionEngine.Execute(query, RegularExpressionString.SQLQuery);
            return result.IsMatch();
        }
    }
}
