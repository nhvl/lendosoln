﻿namespace LqbGrammar.DataTypes
{
    using System;
    using System.Collections.Generic;
    using LqbGrammar.Exceptions;
    using LqbGrammar.Validation;

    /// <summary>
    /// Encapsulate an email address so that it can be validated prior to use.
    /// </summary>
    public struct EmailAddress : ISemanticType, System.IEquatable<EmailAddress>
    {
        /// <summary>
        /// An instance of a bad email address that can be used for variable initialization.
        /// </summary>
        public static readonly EmailAddress BadEmail = new EmailAddress(string.Empty, null);

        /// <summary>
        /// Email address for David Dao.
        /// </summary>
        public static readonly EmailAddress DavidDao = new EmailAddress("davidd@meridianlink.com", PersonName.DavidDao.ToString());

        /// <summary>
        /// Price My Loan support address.
        /// </summary>
        public static readonly EmailAddress PriceMyLoanSupport = new EmailAddress("support@pricemyloan.com", null);

        /// <summary>
        /// Delimiters that we will use to decompose a string consisting of multiple email addresses.
        /// </summary>
        public static char[] DELIMS = new char[]
        {
            ',', ';'
        };

        /// <summary>
        /// The from label used when sending an email on behalf of another email address.
        /// </summary>
        private const string OnBehalfOfLabel = "On behalf of ";

        /// <summary>
        /// The email address.
        /// </summary>
        private string email;

        /// <summary>
        /// Optional display name.
        /// </summary>
        private string displayName;

        /// <summary>
        /// Initializes a new instance of the <see cref="EmailAddress"/> struct.
        /// </summary>
        /// <param name="email">The email address.</param>
        /// <param name="displayName">The display name, which is allowed to be null.</param>
        private EmailAddress(string email, string displayName)
        {
            this.email = email;
            this.displayName = displayName;
        }

        /// <summary>
        /// Gets the display name.
        /// </summary>
        /// <value>The display name.</value>
        public string DisplayName
        {
            get { return this.displayName; }
        }

        /// <summary>
        /// The email address value.
        /// </summary>
        object ISemanticType.Value => this.email;

        /// <summary>
        /// Parse the input email address and validate it.
        /// </summary>
        /// <param name="email">The email that is parsed.</param>
        /// <returns>The validated email, or null.</returns>
        public static EmailAddress? Create(string email)
        {
            if (string.IsNullOrEmpty(email))
            {
                return null;
            }

            var result = RegularExpressionEngine.Execute(email, RegularExpressionString.EmailAddressOutlookStyle);
            if (result.IsMatch())
            {
                return CreateForOutlookAddress(email);
            }

            result = RegularExpressionEngine.Execute(email, RegularExpressionString.EmailAddress);
            if (!result.IsMatch())
            {
                return null;
            }

            return new EmailAddress(email, null);
        }

        /// <summary>
        /// Parse the input email address and display name, validate both.
        /// </summary>
        /// <param name="email">The email that is parsed.</param>
        /// <param name="displayName">The display name, which must be a person's name.</param>
        /// <returns>The validated email, or null.</returns>
        public static EmailAddress? Create(string email, string displayName)
        {
            EmailAddress? check = EmailAddress.Create(email);
            if (check == null)
            {
                return null;
            }

            if (string.IsNullOrEmpty(displayName))
            {
                return null;
            }

            var result = RegularExpressionEngine.Execute(displayName, RegularExpressionString.PersonName);
            if (!result.IsMatch())
            {
                return null;
            }

            return new EmailAddress(email, displayName);
        }

        /// <summary>
        /// Creates and validates an email address used
        /// on behalf of another address.
        /// </summary>
        /// <param name="email">
        /// The email that is parsed.
        /// </param>
        /// <param name="onBehalfOfAddress">
        /// The email being sent on behalf.
        /// </param>
        /// <returns>
        /// The validated input or null.
        /// </returns>
        public static EmailAddress? Create(string email, EmailAddress onBehalfOfAddress)
        {
            var emailCheck = EmailAddress.Create(email);
            if (emailCheck == null)
            {
                return null;
            }

            if (onBehalfOfAddress == EmailAddress.BadEmail)
            {
                return null;
            }

            return new EmailAddress(email, OnBehalfOfLabel + onBehalfOfAddress.email);
        }

        /// <summary>
        /// Parse an email address that may be the composition of multiple addresses, and validate each.
        /// </summary>
        /// <param name="emailValue">The potentially composite email address.</param>
        /// <returns>List of validated email addresses.</returns>
        public static List<EmailAddress> ParsePotentialList(string emailValue)
        {
            var list = new List<EmailAddress>();
            if (!string.IsNullOrEmpty(emailValue))
            {
                string[] arr = emailValue.Split(DELIMS);
                foreach (string s in arr)
                {
                    EmailAddress? email = Create(s);
                    if (email != null)
                    {
                        list.Add(email.Value);
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// Compare to objects for equality and that they are instances of the EmailAddress class.
        /// </summary>
        /// <param name="lhs">The object on the left side of the == operator.</param>
        /// <param name="rhs">The object on the right side of the == operator.</param>
        /// <returns>True if both objects are instances of the EmailAddress class and equal to each other.</returns>
        public static new bool Equals(object lhs, object rhs)
        {
            if (!(lhs is EmailAddress))
            {
                return false;
            }

            if (!(rhs is EmailAddress))
            {
                return false;
            }

            return ((EmailAddress)lhs).Equals((EmailAddress)rhs);
        }

        /// <summary>
        /// The equalitiy operator.
        /// </summary>
        /// <param name="lhs">The instance that is on the left side of the == operator.</param>
        /// <param name="rhs">The instance that is on the right side of the == operator.</param>
        /// <returns>True if the instances are equal, false otherwise.</returns>
        public static bool operator ==(EmailAddress lhs, EmailAddress rhs)
        {
            return lhs.Equals(rhs);
        }

        /// <summary>
        /// The inequality operator.
        /// </summary>
        /// <param name="lhs">The instance that is on the left side of the != operator.</param>
        /// <param name="rhs">The instance that is on the right side of the != operator.</param>
        /// <returns>True if the two instances are not equal, false otherwise.</returns>
        public static bool operator !=(EmailAddress lhs, EmailAddress rhs)
        {
            return !lhs.Equals(rhs);
        }

        /// <summary>
        /// Override the implementation inherited from the Object class.
        /// </summary>
        /// <returns>The email address.</returns>
        public override string ToString()
        {
            return this.email;
        }

        /// <summary>
        /// Compare this instance with another instance for equality.
        /// </summary>
        /// <param name="other">The other instance.</param>
        /// <returns>True if the two instances are equal.</returns>
        public bool Equals(EmailAddress other)
        {
            return string.Compare(this.email, other.email, StringComparison.OrdinalIgnoreCase) == 0;
        }

        /// <summary>
        /// Compare this instance with another object for equality.
        /// </summary>
        /// <param name="obj">The other object.</param>
        /// <returns>True if the other object is an instance of the EmailAddress class and it is equal to this instance.</returns>
        public override bool Equals(object obj)
        {
            if (!(obj is EmailAddress))
            {
                return false;
            }

            return Equals((EmailAddress)obj);
        }

        /// <summary>
        /// Override the implementation inherited from the Object class.
        /// </summary>
        /// <returns>An integer that is the hash of this object.</returns>
        public override int GetHashCode()
        {
            return this.email.GetHashCode();
        }

        /// <summary>
        /// Parse the input email address and validate it.
        /// </summary>
        /// <param name="email">The email that is parsed.</param>
        /// <returns>The validated email, or null.</returns>
        private static EmailAddress? CreateForOutlookAddress(string email)
        {
            int index = email.LastIndexOf('\"');
            string displayName = email.Substring(1, index - 1).Trim();

            index = email.IndexOf('<', index);
            string address = email.Substring(index + 1, email.Length - index - 2);

            return new EmailAddress(address, displayName);
        }
    }
}
