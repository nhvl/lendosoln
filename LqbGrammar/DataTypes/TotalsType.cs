﻿namespace LqbGrammar.DataTypes
{
    /// <summary>
    /// Indicates the type of totals that are being retrieved for a totals command.
    /// </summary>
    public enum TotalsType
    {
        /// <summary>
        /// Retrieve totals at the loan level.
        /// </summary>
        Loan = 0,

        /// <summary>
        /// Retrieve totals at the ULAD app level.
        /// </summary>
        UladApplication = 1,

        /// <summary>
        /// Retrieve totals at the legacy app level.
        /// </summary>
        LegacyApplication = 2,

        /// <summary>
        /// Retrieve totals at the consumer level.
        /// </summary>
        Consumer = 3
    }
}
