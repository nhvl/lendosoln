﻿namespace LqbGrammar.DataTypes
{
    using System;
    using Drivers.SecurityEventLogging;
    
    /// <summary>
    /// Security event logs filter.
    /// </summary>
    public class SecurityEventLogsFilter
    {
        /// <summary>
        /// Gets or sets broker id.
        /// </summary>
        public Guid BrokerId { get; set; }

        /// <summary>
        /// Gets or sets from date filter.
        /// </summary>
        public DateTime? FromDate { get; set; }

        /// <summary>
        /// Gets or sets to date filter.
        /// </summary>
        public DateTime? FilterToDate { get; set; }

        /// <summary>
        /// Gets or sets security event type.
        /// </summary>
        public SecurityEventType EventType { get; set; }

        /// <summary>
        /// Gets or sets IP address.
        /// </summary>
        public string IpAddr { get; set; }

        /// <summary>
        /// Gets or sets employee user id.
        /// </summary>
        public Guid EmployeeUserId { get; set; }
    }
}
