﻿namespace LqbGrammar.DataTypes
{
    using LqbGrammar.Exceptions;
    using LqbGrammar.Validation;

    /// <summary>
    /// Encapsulate the concept of a regular expression string.
    /// For regular expressions used for validating semantic data types,
    /// the RegularExpressionEngine will already have validated the
    /// formats, so we can just use the simple constructor here.
    /// For regular expression strings that come from outside this
    /// module, the <see cref="Create(string)"/> method must be used and 
    /// the format of the string will be checked.
    /// </summary>
    public struct RegularExpressionString : System.IEquatable<RegularExpressionString>
    {
        /// <summary>
        /// Invalid regular expression string that can be used for initialization when necessary.
        /// </summary>
        public static readonly RegularExpressionString Invalid = new RegularExpressionString(string.Empty);

        /// <summary>
        /// Regular expression for application names.
        /// </summary>
        public static readonly RegularExpressionString LqbApplicationName = new RegularExpressionString(RegExStrings.LqbApplicationName);

        /// <summary>
        /// Regular expression for a string used to assign context information to an error message.
        /// </summary>
        public static readonly RegularExpressionString SimpleContext = new RegularExpressionString(RegExStrings.SimpleContext);

        /// <summary>
        /// Regular expression for a message sent via SMS to give the authentication code to a user.
        /// </summary>
        public static readonly RegularExpressionString SmsAuthCodeMessage = new RegularExpressionString(RegExStrings.SmsAuthCodeMessage);

        /// <summary>
        /// Regular expression for text written in standard English.
        /// </summary>
        public static readonly RegularExpressionString StandardEnglish = new RegularExpressionString(RegExStrings.StandardEnglish);

        /// <summary>
        /// Regular expression for an email address.
        /// </summary>
        public static readonly RegularExpressionString EmailAddress = new RegularExpressionString(RegExStrings.EmailAddress);

        /// <summary>
        /// Regular expression for an Outlook-style email address, which combines the display name with the email address.
        /// </summary>
        public static readonly RegularExpressionString EmailAddressOutlookStyle = new RegularExpressionString(RegExStrings.EmailAddressOutlookStyle);

        /// <summary>
        /// Regular expression for the MeridianLink email server.
        /// </summary>
        public static readonly RegularExpressionString EmailServer = new RegularExpressionString(RegExStrings.EmailServer);

        /// <summary>
        /// Regular expression for the the subject line of an email.
        /// </summary>
        public static readonly RegularExpressionString EmailSubject = new RegularExpressionString(RegExStrings.EmailSubject);

        /// <summary>
        /// Regular expression for the the body text of an email.
        /// </summary>
        public static readonly RegularExpressionString EmailBody = new RegularExpressionString(RegExStrings.EmailBody);

        /// <summary>
        /// Regular expression string for a person's name.
        /// </summary>
        public static readonly RegularExpressionString PersonName = new RegularExpressionString(RegExStrings.PersonName);

        /// <summary>
        /// Regular expression string for a Pop3 UID.
        /// </summary>
        public static readonly RegularExpressionString Pop3Uid = new RegularExpressionString(RegExStrings.Pop3Uid);

        /// <summary>
        /// Regular expression string to check the string representation of binary data from the database.
        /// </summary>
        public static readonly RegularExpressionString DbBinaryDataInStringForm = new RegularExpressionString(RegExStrings.DbBinaryDataInStringForm);

        /// <summary>
        /// Regular expression string for disallowed characters in a person's partial name (first, middle, or last). <para/>
        /// NOTE: For now, marking this as strange since it's not used in the normal way.  It's only used in combination with Regex.
        /// </summary>
        public static readonly RegularExpressionString DisallowedNamePartialCharactersSTRANGE = new RegularExpressionString(RegExStrings.DisallowedNamePartialCharacters);

        /// <summary>
        /// Regular expression for a database column name.
        /// </summary>
        public static readonly RegularExpressionString DBColumnName = new RegularExpressionString(RegExStrings.DBColumnName);

        /// <summary>
        /// Regular expression for a database table name.
        /// </summary>
        public static readonly RegularExpressionString DBTableName = new RegularExpressionString(RegExStrings.DBTableName);

        /// <summary>
        /// Regular expression for a package name in the document integration.
        /// </summary>
        public static readonly RegularExpressionString DocumentIntegrationPackageName = new RegularExpressionString(RegExStrings.DocumentIntegrationPackageName);

        /// <summary>
        /// Regular expression for a feature's name, used for defining a feature flag.
        /// </summary>
        public static readonly RegularExpressionString FeatureName = new RegularExpressionString(RegExStrings.FeatureName);

        /// <summary>
        /// Regular expression for a name that determines the name of a configuration item.
        /// </summary>
        public static readonly RegularExpressionString ConfigurationItemName = new RegularExpressionString(RegExStrings.ConfigurationItemName);

        /// <summary>
        /// Regular expression for the name assigned to a computer participating in LQB applications.
        /// </summary>
        public static readonly RegularExpressionString MachineName = new RegularExpressionString(RegExStrings.MachineName);

        /// <summary>
        /// Regular expression string used to validate the name of a stored procedure.
        /// </summary>
        public static readonly RegularExpressionString StoredProcedureName = new RegularExpressionString(RegExStrings.StoredProcedureName);

        /// <summary>
        /// Regular expression for SHA-256 checksum.
        /// </summary>
        public static readonly RegularExpressionString SHA256Checksum = new RegularExpressionString(RegExStrings.SHA256Checksum);

        /// <summary>
        /// Regular expression for the label of a message sent to a message queue.
        /// </summary>
        public static readonly RegularExpressionString MessageLabel = new RegularExpressionString(RegExStrings.MessageLabel);

        /// <summary>
        /// Regular expression for a path that defines the location of a message queue.
        /// </summary>
        public static readonly RegularExpressionString MessageQueuePath = new RegularExpressionString(RegExStrings.MessageQueuePath);

        /// <summary>
        /// Regular expression for the boundary parameter to a MIME multipart content-type.
        /// </summary>
        public static readonly RegularExpressionString MimeMultipartBoundary = new RegularExpressionString(RegExStrings.MimeMultipartBoundary);

        /// <summary>
        /// Regular expression for a token that is part of the MIME Content-Type header field.
        /// </summary>
        public static readonly RegularExpressionString MimeTypeToken = new RegularExpressionString(RegExStrings.MimeTypeToken);

        /// <summary>
        /// Regular expression for the name of a repository in the FileDB system.
        /// </summary>
        public static readonly RegularExpressionString FileDBRepositoryName = new RegularExpressionString(RegExStrings.FileDBRepositoryName);

        /// <summary>
        /// Regular expression for a key used to access a particular file in the FileDB system.
        /// </summary>
        public static readonly RegularExpressionString FileDBKey = new RegularExpressionString(RegExStrings.FileDBKey);

        /// <summary>
        /// Regular expression for the name of a comment category.
        /// </summary>
        public static readonly RegularExpressionString CommentCategoryName = new RegularExpressionString(RegExStrings.CommentCategoryName);

        /// <summary>
        /// Regular expression for the name of a conversation log permission level name.
        /// </summary>
        public static readonly RegularExpressionString ConversationLogPermissionLevelName = new RegularExpressionString(RegExStrings.ConversationLogPermissionLevelName);

        /// <summary>
        /// Regular expression for the name of a conversation log permission level description.
        /// </summary>
        public static readonly RegularExpressionString ConversationLogPermissionLevelDescription = new RegularExpressionString(RegExStrings.ConversationLogPermissionLevelDescription);

        /// <summary>
        /// Regular expression for a SQL query string.
        /// </summary>
        public static readonly RegularExpressionString SQLQuery = new RegularExpressionString(RegExStrings.SQLQuery);

        /// <summary>
        /// Regular expression for a valid loan name under 2018 HMDA rules for legal entity identifiers.
        /// </summary>
        public static readonly RegularExpressionString HmdaLegalIdentityIdentifierValidLoanName = new RegularExpressionString(RegExStrings.HmdaLegalIdentityIdentifierValidLoanName);

        /// <summary>
        /// Regular expression for a valid legal entity identifier under 2018 HMDA rules.
        /// </summary>
        public static readonly RegularExpressionString LegalEntityIdentifier = new RegularExpressionString(RegExStrings.LegalEntityIdentifier);

        /// <summary>
        /// Regular expression for a valid log message.
        /// </summary>
        public static readonly RegularExpressionString LogMessage = new RegularExpressionString(RegExStrings.LogMessage);

        /// <summary>
        /// Regular expression for a valid log message.
        /// </summary>
        public static readonly RegularExpressionString LogPropertyName = new RegularExpressionString(RegExStrings.LogPropertyName);

        /// <summary>
        /// Regular expression for a valid log message.
        /// </summary>
        public static readonly RegularExpressionString LogPropertyValue = new RegularExpressionString(RegExStrings.LogPropertyValue);

        /// <summary>
        /// Regular expression for a simple XPath.
        /// </summary>
        public static readonly RegularExpressionString SimpleXPath = new RegularExpressionString(RegExStrings.SimpleXPath);

        /// <summary>
        /// Regular expression for a valid zipcode.
        /// </summary>
        public static readonly RegularExpressionString Zipcode = new RegularExpressionString(RegExStrings.Zipcode);

        /// <summary>
        /// Regular expression for a valid phone number.
        /// </summary>
        public static readonly RegularExpressionString PhoneNumber = new RegularExpressionString(RegExStrings.PhoneNumber);

        /// <summary>
        /// Regular expression for a valid employer identification number.
        /// </summary>
        public static readonly RegularExpressionString EmployerTaxIdentificationNumber = new RegularExpressionString(RegExStrings.EmployerTaxIdentificationNumber);

        /// <summary>
        /// Regular expression for a valid company id.
        /// </summary>
        public static readonly RegularExpressionString RolodexCompanyID = new RegularExpressionString(RegExStrings.RolodexCompanyID);

        /// <summary>
        /// Regular expression for SASS variable values.
        /// </summary>
        public static readonly RegularExpressionString SassVariableValue = new RegularExpressionString(RegExStrings.SassVariableValue);

        /// <summary>
        /// Regular expression for Social Security Numbers.
        /// </summary>
        public static readonly RegularExpressionString SocialSecurityNumber = new RegularExpressionString(RegExStrings.SocialSecurityNumber);

        /// <summary>
        /// Regular expression for Word files with the DocX extension.
        /// </summary>
        public static readonly RegularExpressionString DocXFile = new RegularExpressionString(RegExStrings.DocXFile);

        /// <summary>
        /// Encapsulated regular expression string.
        /// </summary>
        private string regex;

        /// <summary>
        /// Initializes a new instance of the <see cref="RegularExpressionString"/> struct.
        /// </summary>
        /// <param name="regEx">The encapsulated regular expression string.</param>
        private RegularExpressionString(string regEx)
        {
            this.regex = regEx;
        }

        /// <summary>
        /// Validate a regular expression string and encapsulate it.
        /// </summary>
        /// <param name="regEx">The regular expression string to be validated.</param>
        /// <returns>The validated regular expression, or null.</returns>
        public static RegularExpressionString? Create(string regEx)
        {
            if (string.IsNullOrEmpty(regEx))
            {
                return null;
            }

            var regexString = new RegularExpressionString(regEx);

            bool isValid = Validate(regexString);
            if (isValid)
            {
                return regexString;
            }

            return null;
        }

        /// <summary>
        /// Compare two instances for equality.
        /// </summary>
        /// <param name="lhs">Instance on the left side of the == operator.</param>
        /// <param name="rhs">Instance on the right side of the == operator.</param>
        /// <returns>True if the instances are equal, false otherwise.</returns>
        public static new bool Equals(object lhs, object rhs)
        {
            if (!(lhs is RegularExpressionString))
            {
                return false;
            }

            if (!(rhs is RegularExpressionString))
            {
                return false;
            }

            return ((RegularExpressionString)lhs).Equals((RegularExpressionString)rhs);
        }

        /// <summary>
        /// Implementation of the == operator.
        /// </summary>
        /// <param name="lhs">Instance on the left side of the == operator.</param>
        /// <param name="rhs">Instance on the right side of the == operator.</param>
        /// <returns>True if the instances are equal, false otherwise.</returns>
        public static bool operator ==(RegularExpressionString lhs, RegularExpressionString rhs)
        {
            return lhs.Equals(rhs);
        }

        /// <summary>
        /// Implementation of the != operator.
        /// </summary>
        /// <param name="lhs">Instance on the left side of the != operator.</param>
        /// <param name="rhs">Instance on the right side of the != operator.</param>
        /// <returns>True if the instances are not equal, false otherwise.</returns>
        public static bool operator !=(RegularExpressionString lhs, RegularExpressionString rhs)
        {
            return !lhs.Equals(rhs);
        }

        /// <summary>
        /// Override the method inherited from the Object class.
        /// </summary>
        /// <returns>The regular expression string that is encapsulated.</returns>
        public override string ToString()
        {
            return this.regex;
        }

        /// <summary>
        /// Implementation of the <see cref="System.IEquatable{T}"/> interface.
        /// </summary>
        /// <param name="other">Instance that is compared to this instance for equality.</param>
        /// <returns>True if the instances are equal, false otherwise.</returns>
        public bool Equals(RegularExpressionString other)
        {
            return this.regex == other.regex;
        }

        /// <summary>
        /// Override the method inherited from the Object class.
        /// </summary>
        /// <param name="obj">The instance that will be compared with this instance for equality.</param>
        /// <returns>True if the instances are equal, false otherwise.</returns>
        public override bool Equals(object obj)
        {
            if (!(obj is RegularExpressionString))
            {
                return false;
            }

            return Equals((RegularExpressionString)obj);
        }

        /// <summary>
        /// Override of the method inherited from the Object class.
        /// </summary>
        /// <returns>An integer that is the hash of this instance.</returns>
        public override int GetHashCode()
        {
            return this.regex.GetHashCode();
        }

        /// <summary>
        /// Utility method for validating a regular expression string.
        /// </summary>
        /// <param name="regEx">The regular expression string to be validated.</param>
        /// <returns>True if the regular expression string is valid, false otherwise.</returns>
        /// <remarks>
        /// This method performs the validation in two steps. First, we verify that the
        /// specified regular expression can be utilized by the engine to create a result.
        /// Second, we verify that the result can be used to determine if a match exists.
        /// </remarks>
        private static bool Validate(RegularExpressionString regEx)
        {
            try
            {
                var result = RegularExpressionEngine.Execute(string.Empty, regEx);
                return result.IsMatch() || true;
            }
            catch (DeveloperException)
            {
                return false;
            }
        }
    }
}
