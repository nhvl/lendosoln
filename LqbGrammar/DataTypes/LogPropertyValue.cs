﻿namespace LqbGrammar.DataTypes
{
    using System;
    using LqbGrammar.Validation;

    /// <summary>
    /// Encapsulate a log property value, with the format validated.
    /// </summary>
    public struct LogPropertyValue : System.IEquatable<LogPropertyValue>
    {
        /// <summary>
        /// This is the contained log property value.
        /// </summary>
        private string value;

        /// <summary>
        /// Initializes a new instance of the <see cref="LogPropertyValue"/> struct.
        /// </summary>
        /// <param name="value">The log property value.</param>
        private LogPropertyValue(string value)
        {
            this.value = value;
        }

        /// <summary>
        /// Validate and encapsulate a log property value.
        /// </summary>
        /// <param name="value">The log property value.</param>
        /// <returns>The encapsulated log property value if it is valid, or null.</returns>
        public static LogPropertyValue? Create(string value)
        {
            if (value == null)
            {
                value = string.Empty;
            }

            RegularExpressionResult result = RegularExpressionEngine.Execute(value, RegularExpressionString.LogPropertyValue);
            if (!result.IsMatch())
            {
                return null;
            }

            return new LogPropertyValue(value);
        }

        /// <summary>
        /// Static method used for checking equality of two instances.
        /// </summary>
        /// <param name="lhs">Instance on the left side of the == operator.</param>
        /// <param name="rhs">Instance on the right side of the == operator.</param>
        /// <returns>True if the instances are equal, false otherwise.</returns>
        public static new bool Equals(object lhs, object rhs)
        {
            if (!(lhs is LogPropertyValue))
            {
                return false;
            }

            if (!(rhs is LogPropertyValue))
            {
                return false;
            }

            return ((LogPropertyValue)lhs).Equals((LogPropertyValue)rhs);
        }

        /// <summary>
        /// Implements the == operator.
        /// </summary>
        /// <param name="lhs">Instance on the left side of the == operator.</param>
        /// <param name="rhs">Instance on the right side of the == operator.</param>
        /// <returns>True if the instances are equal, false otherwise.</returns>
        public static bool operator ==(LogPropertyValue lhs, LogPropertyValue rhs)
        {
            return lhs.Equals(rhs);
        }

        /// <summary>
        /// Implements the != oeprator.
        /// </summary>
        /// <param name="lhs">Instance on the left side of the != operator.</param>
        /// <param name="rhs">Instance on the right side of the != operator.</param>
        /// <returns>True if the instances are not equal, false otherwise.</returns>
        public static bool operator !=(LogPropertyValue lhs, LogPropertyValue rhs)
        {
            return !lhs.Equals(rhs);
        }

        /// <summary>
        /// Overrides the method inherited from the Object class.
        /// </summary>
        /// <returns>The log property value.</returns>
        public override string ToString()
        {
            return this.value;
        }

        /// <summary>
        /// Implement the IEquatable interface.
        /// </summary>
        /// <param name="other">Instance that is compared with this instance for equality.</param>
        /// <returns>True if the instances are equal, false otherwise.</returns>
        public bool Equals(LogPropertyValue other)
        {
            return string.Compare(this.value, other.value, StringComparison.OrdinalIgnoreCase) == 0;
        }

        /// <summary>
        /// Override of the method inherited from the Object class.
        /// </summary>
        /// <param name="obj">Instance that is compared with this instance for equality.</param>
        /// <returns>True if the instances are equal, false otherwise.</returns>
        public override bool Equals(object obj)
        {
            if (!(obj is LogPropertyValue))
            {
                return false;
            }

            return Equals((LogPropertyValue)obj);
        }

        /// <summary>
        /// Override the method inherited from the Object class.
        /// </summary>
        /// <returns>An integer that is the hash of this instance.</returns>
        public override int GetHashCode()
        {
            return this.value.GetHashCode();
        }
    }
}
