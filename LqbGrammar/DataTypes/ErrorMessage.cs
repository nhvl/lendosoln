﻿namespace LqbGrammar.DataTypes
{
    using System;
    using LqbGrammar.Exceptions;
    using LqbGrammar.Validation;

    /// <summary>
    /// Encapsulate an error message, with the format validated.
    /// </summary>
    public struct ErrorMessage : System.IEquatable<ErrorMessage>
    {
        //// We'll place all standard error messages here.  For custom messages, the code will need to
        //// craft them with TryParse, which is tedious and should discourage that practice.

        /// <summary>
        /// Generic error message for system errors.
        /// </summary>
        public static readonly ErrorMessage SystemError = new ErrorMessage(ErrorMessages.SystemError);

        /// <summary>
        /// If a value is missing or incorrect in the applications's configuration, return this error.
        /// </summary>
        public static readonly ErrorMessage BadConfiguration = new ErrorMessage(ErrorMessages.BadConfiguration);

        /// <summary>
        /// Generic error message for validation errors.
        /// </summary>
        public static readonly ErrorMessage ValidationError = new ErrorMessage(ErrorMessages.ValidationError);

        /// <summary>
        /// This is the contained error message.
        /// </summary>
        private string message;

        /// <summary>
        /// Initializes a new instance of the <see cref="ErrorMessage"/> struct.
        /// </summary>
        /// <param name="message">The error message.</param>
        private ErrorMessage(string message)
        {
            this.message = message;
        }

        /// <summary>
        /// When a template is used to generate error messages with certain fields as variable, this
        /// method is used to create and validate the error message.
        /// </summary>
        /// <param name="template">Template used to generate the error message.</param>
        /// <param name="parameters">The values that will populate the error message variable fields.</param>
        /// <returns>A validated error message.</returns>
        public static ErrorMessage FromTemplate(ErrorMessageTemplate template, params string[] parameters)
        {
            string full = string.Format(template.ToString(), parameters);

            ErrorMessage? message = Create(full);
            if (message == null)
            {
                throw new DeveloperException(ErrorMessage.SystemError);
            }

            return message.Value;
        }

        /// <summary>
        /// When a template is used to generate error messages with certain fields as variable, this
        /// method is used to create and validate the error message.  This should only be used when
        /// the parameters contain SQL, as the validation is much less strict.
        /// </summary>
        /// <param name="template">Template used to generate the error message.</param>
        /// <param name="parameters">The values that will populate the error message variable fields.</param>
        /// <returns>A validated error message.</returns>
        public static ErrorMessage FromTemplateWithSql(ErrorMessageTemplate template, params string[] parameters)
        {
            string full = string.Format(template.ToString(), parameters);

            ErrorMessage? message = CreateWithSql(full);
            if (message == null)
            {
                throw new DeveloperException(ErrorMessage.SystemError);
            }

            return message.Value;
        }

        /// <summary>
        /// Static method used for checking equality of two instances.
        /// </summary>
        /// <param name="lhs">Instance on the left side of the == operator.</param>
        /// <param name="rhs">Instance on the right side of the == operator.</param>
        /// <returns>True if the instances are equal, false otherwise.</returns>
        public static new bool Equals(object lhs, object rhs)
        {
            if (!(lhs is ErrorMessage))
            {
                return false;
            }

            if (!(rhs is ErrorMessage))
            {
                return false;
            }

            return ((ErrorMessage)lhs).Equals((ErrorMessage)rhs);
        }

        /// <summary>
        /// Implements the == operator.
        /// </summary>
        /// <param name="lhs">Instance on the left side of the == operator.</param>
        /// <param name="rhs">Instance on the right side of the == operator.</param>
        /// <returns>True if the instances are equal, false otherwise.</returns>
        public static bool operator ==(ErrorMessage lhs, ErrorMessage rhs)
        {
            return lhs.Equals(rhs);
        }

        /// <summary>
        /// Implements the != oeprator.
        /// </summary>
        /// <param name="lhs">Instance on the left side of the != operator.</param>
        /// <param name="rhs">Instance on the right side of the != operator.</param>
        /// <returns>True if the instances are not equal, false otherwise.</returns>
        public static bool operator !=(ErrorMessage lhs, ErrorMessage rhs)
        {
            return !lhs.Equals(rhs);
        }

        /// <summary>
        /// Overrides the method inherited from the Object class.
        /// </summary>
        /// <returns>The error message.</returns>
        public override string ToString()
        {
            return this.message;
        }

        /// <summary>
        /// Implement the IEquatable interface.
        /// </summary>
        /// <param name="other">Instance that is compared with this instance for equality.</param>
        /// <returns>True if the instances are equal, false otherwise.</returns>
        public bool Equals(ErrorMessage other)
        {
            return string.Compare(this.message, other.message, StringComparison.OrdinalIgnoreCase) == 0;
        }

        /// <summary>
        /// Override of the method inherited from the Object class.
        /// </summary>
        /// <param name="obj">Instance that is compared with this instance for equality.</param>
        /// <returns>True if the instances are equal, false otherwise.</returns>
        public override bool Equals(object obj)
        {
            if (!(obj is ErrorMessage))
            {
                return false;
            }

            return Equals((ErrorMessage)obj);
        }

        /// <summary>
        /// Override the method inherited from the Object class.
        /// </summary>
        /// <returns>An integer that is the hash of this instance.</returns>
        public override int GetHashCode()
        {
            return this.message.GetHashCode();
        }

        /// <summary>
        /// Validate and encapsulate an error message.
        /// </summary>
        /// <param name="message">The error message.</param>
        /// <returns>The encapsulated error message if it is valid, or null.</returns>
        internal static ErrorMessage? Create(string message)
        {
            if (string.IsNullOrEmpty(message))
            {
                return null;
            }

            RegularExpressionResult result = RegularExpressionEngine.Execute(message, RegularExpressionString.StandardEnglish);
            if (!result.IsMatch())
            {
                return null;
            }

            return new ErrorMessage(message);
        }

        /// <summary>
        /// Validate and encapsulate an error message that contains SQL.
        /// </summary>
        /// <param name="message">The error message.</param>
        /// <returns>The encapsulated error message if it is valid, or null.</returns>
        private static ErrorMessage? CreateWithSql(string message)
        {
            if (string.IsNullOrEmpty(message))
            {
                return null;
            }

            RegularExpressionResult result = RegularExpressionEngine.Execute(message, RegularExpressionString.SQLQuery);
            if (!result.IsMatch())
            {
                return null;
            }

            return new ErrorMessage(message);
        }
    }

    /// <summary>
    /// Error message template strings are represented to the outside
    /// via this class.
    /// </summary>
    public struct ErrorMessageTemplate
    {
        /// <summary>
        /// Use when there is an error to populate a DataSet.
        /// </summary>
        public static ErrorMessageTemplate FillDataSetAccessFailed = new ErrorMessageTemplate(ErrorMessageTemplates.FillDataSetAccessFailed);

        /// <summary>
        /// Use when the user lacks permission. <para/>
        /// 0 = operation type, e.g. "reply to"; <para></para>
        /// 1 = resource type, e.g. "conversation with permission level id: x"; <para></para>
        /// </summary>
        public static ErrorMessageTemplate PermissionDenied = new ErrorMessageTemplate(ErrorMessageTemplates.PermissionDenied);

        /// <summary>
        /// Use this when there are duplicates in the call to the specified method.
        /// 0 = methodName e.g. SetCategoryOrder<para/>
        /// 1 = objectName e.g. category <para/> 
        /// 2 = ownerId.
        /// </summary>
        public static ErrorMessageTemplate ListContainsDuplicates = new ErrorMessageTemplate(ErrorMessageTemplates.ListContainsDuplicates);

        /// <summary>
        /// Use this when the call to method has an object with id not belonging to the owner.
        /// 0 = methodName e.g. SetCategoryOrder <para/>
        /// 1 = objectName e.g. category <para/>
        /// 2 = object id e.g. category id <para/>
        /// 3 = ownerId.
        /// </summary>
        public static ErrorMessageTemplate HasExtraObject = new ErrorMessageTemplate(ErrorMessageTemplates.HasExtraObject);

        /// <summary>
        /// Use this when you couldn't find the thing you're looking for.
        /// </summary>
        /// 0 = the thing not found
        public static ErrorMessageTemplate NotFound = new ErrorMessageTemplate(ErrorMessageTemplates.NotFound);

        /// <summary>
        /// Use this when the call to the method does not contain one of the existing object instances for the owner. <para/>
        /// 0 = methodName e.g. SetCategoryOrder <para/>
        /// 1 = objectName e.g. category <para/>
        /// 2 = objectId e.g. category id <para/> 
        /// 3 = ownerId.
        /// </summary>
        public static ErrorMessageTemplate IsMissingObjectInstance = new ErrorMessageTemplate(ErrorMessageTemplates.IsMissingObjectInstance);

        /// <summary>
        /// For when an invalid value is detected.
        /// 0 = Field name.
        /// 1 = Value received.
        /// </summary>
        public static ErrorMessageTemplate InvalidFieldValue = new ErrorMessageTemplate(ErrorMessageTemplates.InvalidValue);

        /// <summary>
        /// The encapsulated template string.
        /// </summary>
        private string template;

        /// <summary>
        /// Initializes a new instance of the <see cref="ErrorMessageTemplate"/> struct.
        /// </summary>
        /// <param name="template">The template string.</param>
        private ErrorMessageTemplate(string template)
        {
            this.template = template;
        }

        /// <summary>
        /// Override the method inherited from the Object class.
        /// </summary>
        /// <returns>The template string.</returns>
        public override string ToString()
        {
            return this.template;
        }
    }
}
