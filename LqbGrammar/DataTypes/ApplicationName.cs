﻿namespace LqbGrammar.DataTypes
{
    using LqbGrammar.Exceptions;
    using LqbGrammar.Validation;

    /// <summary>
    /// Encapsulate validation logic for an application's name.
    /// </summary>
    public struct ApplicationName : System.IEquatable<ApplicationName>
    {
        /// <summary>
        /// An instance of the class that fails validation, can be used for initialization when necessary.
        /// </summary>
        public static readonly ApplicationName BadName = new ApplicationName(string.Empty);

        /// <summary>
        /// Encapsulated value.
        /// </summary>
        private string name;

        /// <summary>
        /// Initializes a new instance of the <see cref="ApplicationName"/> struct.
        /// </summary>
        /// <param name="name">The value.</param>
        private ApplicationName(string name)
        {
            this.name = name;
        }

        /// <summary>
        /// Static method that checks for equality between two instances.
        /// </summary>
        /// <param name="lhs">Instance on the left side of the == operator.</param>
        /// <param name="rhs">Instance on the right side of the == operator.</param>
        /// <returns>True if the two instances are equal, false otherwise.</returns>
        public static new bool Equals(object lhs, object rhs)
        {
            if (!(lhs is ApplicationName))
            {
                return false;
            }

            if (!(rhs is ApplicationName))
            {
                return false;
            }

            return ((ApplicationName)lhs).Equals((ApplicationName)rhs);
        }

        /// <summary>
        /// The == operator is implemented here.
        /// </summary>
        /// <param name="lhs">Instance on the left of the == operator.</param>
        /// <param name="rhs">Instance on the right of the == operator.</param>
        /// <returns>True if the two instances are equal, false otherwise.</returns>
        public static bool operator ==(ApplicationName lhs, ApplicationName rhs)
        {
            return lhs.Equals(rhs);
        }

        /// <summary>
        /// The != operator is implemented here.
        /// </summary>
        /// <param name="lhs">Instance on the left of the != operator.</param>
        /// <param name="rhs">Instance on the right of the != operator.</param>
        /// <returns>True if the two instances are not equal, false otherwise.</returns>
        public static bool operator !=(ApplicationName lhs, ApplicationName rhs)
        {
            return !lhs.Equals(rhs);
        }

        /// <summary>
        /// Return the encapsulated value.
        /// </summary>
        /// <returns>Encapsulated value.</returns>
        public override string ToString()
        {
            return this.name;
        }

        /// <summary>
        /// Implement IEquatable.
        /// </summary>
        /// <param name="other">Instance that is checked for equality with this instance.</param>
        /// <returns>True if other is equal to this instance, false otherwise.</returns>
        public bool Equals(ApplicationName other)
        {
            return this.name == other.name;
        }

        /// <summary>
        /// Override the Equals method inherited from the Object class.
        /// </summary>
        /// <param name="obj">Instance that is checked for equality with this instance.</param>
        /// <returns>True if obj is equal to this instance, false otherwise.</returns>
        public override bool Equals(object obj)
        {
            if (!(obj is ApplicationName))
            {
                return false;
            }

            return Equals((ApplicationName)obj);
        }

        /// <summary>
        /// Override of the method inherited from the Object class.
        /// </summary>
        /// <returns>An integer that is the hash code for this instance.</returns>
        public override int GetHashCode()
        {
            return this.name.GetHashCode();
        }

        /// <summary>
        /// Attempt to parse the input name into a validated application name.
        /// </summary>
        /// <param name="name">Input name that will be parsed.</param>
        /// <returns>Valid application name, or null.</returns>
        internal static ApplicationName? Create(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                return null;
            }

            RegularExpressionResult result = RegularExpressionEngine.Execute(name, RegularExpressionString.LqbApplicationName);
            if (!result.IsMatch())
            {
                return null;
            }

            return new ApplicationName(name);
        }
    }
}
