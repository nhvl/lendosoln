﻿namespace LqbGrammar.DataTypes
{
    using System;

    /// <summary>
    /// Manually defined methods for the IFormatAsString interface.
    /// </summary>
    public partial interface IFormatAsString
    {
        /// <summary>
        /// Format a boolean into a string.
        /// </summary>
        /// <param name="value">The boolean to be formatted.</param>
        /// <returns>A string representation of the boolean.</returns>
        string Format(bool? value);

        /// <summary>
        /// Format a boolean into a string.
        /// </summary>
        /// <param name="value">The boolean to be formatted.</param>
        /// <param name="defaultValue">The default value to use when the input is null.</param>
        /// <returns>A string representation of the boolean.</returns>
        string Format(bool? value, string defaultValue);

        /// <summary>
        /// Format a DataObjectIdentifier into a string.
        /// </summary>
        /// <typeparam name="TValueKind">The <see cref="DataObjectKind"/> of the value being identified.</typeparam>
        /// <typeparam name="TIdValue">The type of the identifier used to refer to the value.</typeparam>
        /// <param name="value">The DataObjectIdentifier to be formatted.</param>
        /// <returns>A string representation of the DataObjectIdentifer.</returns>
        string Format<TValueKind, TIdValue>(DataObjectIdentifier<TValueKind, TIdValue> value)
            where TValueKind : DataObjectKind
            where TIdValue : struct, IEquatable<TIdValue>;

        /// <summary>
        /// Format a DataObjectIdentifier into a string.
        /// </summary>
        /// <typeparam name="TValueKind">The <see cref="DataObjectKind"/> of the value being identified.</typeparam>
        /// <typeparam name="TIdValue">The type of the identifier used to refer to the value.</typeparam>
        /// <param name="value">The DataObjectIdentifier to be formatted.</param>
        /// <returns>A string representation of the DataObjectIdentifer.</returns>
        string Format<TValueKind, TIdValue>(DataObjectIdentifier<TValueKind, TIdValue>? value)
            where TValueKind : DataObjectKind
            where TIdValue : struct, IEquatable<TIdValue>;

        /// <summary>
        /// Format a DataObjectIdentifier into a string.
        /// </summary>
        /// <typeparam name="TValueKind">The <see cref="DataObjectKind"/> of the value being identified.</typeparam>
        /// <typeparam name="TIdValue">The type of the identifier used to refer to the value.</typeparam>
        /// <param name="value">The DataObjectIdentifier to be formatted.</param>
        /// <param name="defaultValue">The default value to use when the input is null.</param>
        /// <returns>A string representation of the DataObjectIdentifer.</returns>
        string Format<TValueKind, TIdValue>(DataObjectIdentifier<TValueKind, TIdValue>? value, string defaultValue)
            where TValueKind : DataObjectKind
            where TIdValue : struct, IEquatable<TIdValue>;

        /// <summary>
        /// Format an enumeration value into a string.
        /// </summary>
        /// <typeparam name="E">The enumeration type.</typeparam>
        /// <param name="value">The enumeration value.</param>
        /// <returns>A string represenation of the enumeration value.</returns>
        string FormatEnum<E>(E? value) where E : struct;

        /// <summary>
        /// Format an enumeration value into a string.
        /// </summary>
        /// <typeparam name="E">The enumeration type.</typeparam>
        /// <param name="value">The enumeration value.</param>
        /// <param name="defaultValue">The default value to use when the input is null.</param>
        /// <returns>A string represenation of the enumeration value.</returns>
        string FormatEnum<E>(E? value, string defaultValue) where E : struct;
    }
}
