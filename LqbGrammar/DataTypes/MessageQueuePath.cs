﻿namespace LqbGrammar.DataTypes
{
    using LqbGrammar.Exceptions;
    using LqbGrammar.Validation;

    /// <summary>
    /// Encapsulate and validate a path for a message queue.
    /// </summary>
    public struct MessageQueuePath : System.IEquatable<MessageQueuePath>
    {
        /// <summary>
        /// An invalid instance that can be used for variable initialization.
        /// </summary>
        public static readonly MessageQueuePath BadPath = new MessageQueuePath(string.Empty);

        /// <summary>
        /// The message queue path.
        /// </summary>
        private string path;

        /// <summary>
        /// Initializes a new instance of the <see cref="MessageQueuePath"/> struct.
        /// </summary>
        /// <param name="path">The path of the message queue.</param>
        private MessageQueuePath(string path)
        {
            this.path = path;
        }

        /// <summary>
        /// Validate a message queue path.
        /// </summary>
        /// <param name="path">The message queue path.</param>
        /// <returns>The validated message queue path, or null if the input path is invalid.</returns>
        public static MessageQueuePath? Create(string path)
        {
            if (string.IsNullOrEmpty(path))
            {
                return null;
            }

            RegularExpressionResult result = RegularExpressionEngine.Execute(path, RegularExpressionString.MessageQueuePath);
            if (!result.IsMatch())
            {
                return null;
            }

            return new MessageQueuePath(path);
        }

        /// <summary>
        /// Compare two objects for equality and they are both the same validated message path.
        /// </summary>
        /// <param name="lhs">The object on the left side of the == operator.</param>
        /// <param name="rhs">The object on the right side of the == operator.</param>
        /// <returns>True if both objects are instances of the MessageQueuePath class and they are equal to each other.</returns>
        public static new bool Equals(object lhs, object rhs)
        {
            if (!(lhs is MessageQueuePath))
            {
                return false;
            }

            if (!(rhs is MessageQueuePath))
            {
                return false;
            }

            return ((MessageQueuePath)lhs).Equals((MessageQueuePath)rhs);
        }

        /// <summary>
        /// Implement the equality operator for a validated message queue path.
        /// </summary>
        /// <param name="lhs">Instance on the left side of the == operator.</param>
        /// <param name="rhs">Instance on the right side of the == operator.</param>
        /// <returns>True if the instances are equal.</returns>
        public static bool operator ==(MessageQueuePath lhs, MessageQueuePath rhs)
        {
            return lhs.Equals(rhs);
        }

        /// <summary>
        /// Implement the inequality operator for a validated message queue path.
        /// </summary>
        /// <param name="lhs">The instance on the left side of the != operator.</param>
        /// <param name="rhs">The instance on the right side of the != operator.</param>
        /// <returns>Return true if the two instances are not equal, false otherwise.</returns>
        public static bool operator !=(MessageQueuePath lhs, MessageQueuePath rhs)
        {
            return !lhs.Equals(rhs);
        }

        /// <summary>
        /// Override the implementation inherited from the Object class.
        /// </summary>
        /// <returns>The message path.</returns>
        public override string ToString()
        {
            return this.path;
        }

        /// <summary>
        /// Compare this instance with another instance for equality.
        /// </summary>
        /// <param name="other">The other instance.</param>
        /// <returns>True if the two instances are equal, false otherwise.</returns>
        public bool Equals(MessageQueuePath other)
        {
            return this.path == other.path;
        }

        /// <summary>
        /// Compares this instance with another object for equality.
        /// </summary>
        /// <param name="obj">The other object.</param>
        /// <returns>True if the other object is a valid message queue path and the two objects have the same value.</returns>
        public override bool Equals(object obj)
        {
            if (!(obj is MessageQueuePath))
            {
                return false;
            }

            return Equals((MessageQueuePath)obj);
        }

        /// <summary>
        /// Override the implementation inherited from the Object class.
        /// </summary>
        /// <returns>An integer that is the hash of this instance.</returns>
        public override int GetHashCode()
        {
            return this.path.GetHashCode();
        }
    }
}
