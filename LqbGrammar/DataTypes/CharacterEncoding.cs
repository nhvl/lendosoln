﻿namespace LqbGrammar.DataTypes
{
    /// <summary>
    /// Encapsulate the concept of character encoding, which is different from
    /// the concept of character set.  For a discussion of the difference, see
    /// http://skeletonschema.info/Blog/XmlEncodings.php.
    /// </summary>
    public struct CharacterEncoding
    {
        /// <summary>
        /// The ASCII character encoding.
        /// </summary>
        public static readonly CharacterEncoding Ascii = new CharacterEncoding(CharacterSet.Ascii, null);

        /// <summary>
        /// The ISO-8850-1 character encoding, AKA Latin1.
        /// </summary>
        public static readonly CharacterEncoding ISOx8859x1 = new CharacterEncoding(CharacterSet.ISOx8859x1, null);

        /// <summary>
        /// The Latin1 character encoding.
        /// </summary>
        public static readonly CharacterEncoding Latin1 = ISOx8859x1;

        /// <summary>
        /// The character encoding used by older versions of the Windows OS.
        /// </summary>
        public static readonly CharacterEncoding Windows = new CharacterEncoding(CharacterSet.Windows, null);

        /// <summary>
        /// The UTF-8 character encoding.
        /// </summary>
        public static readonly CharacterEncoding Utf8 = new CharacterEncoding(CharacterSet.Unicode, "utf-8");

        /// <summary>
        /// The UTF-16 character encoding.
        /// </summary>
        public static readonly CharacterEncoding Utf16 = new CharacterEncoding(CharacterSet.Unicode, "utf-16");

        /// <summary>
        /// A character encoding consists of an underlying character set.
        /// </summary>
        private CharacterSet charset;

        /// <summary>
        /// In addition to a character set, a character encoding consists of a rule
        /// that determines how each character from the set is mapped to a set of bytes.
        /// </summary>
        private string encodingRule;

        /// <summary>
        /// Initializes a new instance of the <see cref="CharacterEncoding"/> struct.
        /// </summary>
        /// <param name="charset">The underlying character set.</param>
        /// <param name="encodingRule">The encoding rule.</param>
        private CharacterEncoding(CharacterSet charset, string encodingRule)
        {
            this.charset = charset;
            this.encodingRule = encodingRule;
        }

        /// <summary>
        /// Override the Object.ToString() method.
        /// </summary>
        /// <returns>The string that is used to characterize the character encoding.</returns>
        public override string ToString()
        {
            return (this.encodingRule == null) ? this.charset.ToString() : this.encodingRule;
        }
    }
}
