﻿namespace LqbGrammar.DataTypes
{
    using System;

    /// <summary>
    /// Wrapper class for a timeout.
    /// </summary>
    public struct TimeoutInSeconds : IEquatable<TimeoutInSeconds>
    {
        /// <summary>
        /// Invalid value that can be used for initialization.
        /// </summary>
        public static readonly TimeoutInSeconds Invalid = new TimeoutInSeconds(-1);

        /// <summary>
        /// Use this value when you want to set a timeout to the default value.
        /// </summary>
        public static readonly TimeoutInSeconds Default = new TimeoutInSeconds(0);

        /// <summary>
        /// This is a common value used in the code, so we'll have a hard-coded instance.
        /// </summary>
        public static readonly TimeoutInSeconds Thirty = new TimeoutInSeconds(30);

        /// <summary>
        /// This is a common value used in the code, so we'll have a hard-coded instance.
        /// </summary>
        public static readonly TimeoutInSeconds Sixty = new TimeoutInSeconds(60);

        /// <summary>
        /// The integer value.
        /// </summary>
        private int value;

        /// <summary>
        /// Initializes a new instance of the <see cref="TimeoutInSeconds"/> struct.
        /// </summary>
        /// <param name="value">The timeout to be encapsulated.</param>
        private TimeoutInSeconds(int value)
        {
            this.value = value;
        }

        /// <summary>
        /// Gets the encapsulated integer.
        /// </summary>
        /// <value>The integer timeout is returned.</value>
        public int Value
        {
            get { return this.value; }
        }

        /// <summary>
        /// Validate the input timeout.
        /// </summary>
        /// <param name="value">The timeout to be validated.</param>
        /// <returns>The validated timeout, or null.</returns>
        public static TimeoutInSeconds? Create(int value)
        {
            if (value > 0)
            {
                return new TimeoutInSeconds(value);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Compare two instances for equality.
        /// </summary>
        /// <param name="lhs">Instance on the left side of the == operator.</param>
        /// <param name="rhs">Instance on the right side of the == operator.</param>
        /// <returns>True if the instances are equal, false otherwise.</returns>
        public static new bool Equals(object lhs, object rhs)
        {
            if (!(lhs is TimeoutInSeconds))
            {
                return false;
            }

            if (!(rhs is TimeoutInSeconds))
            {
                return false;
            }

            return ((TimeoutInSeconds)lhs).Equals((TimeoutInSeconds)rhs);
        }

        /// <summary>
        /// Implementation of the == operator.
        /// </summary>
        /// <param name="lhs">Instance on the left side of the == operator.</param>
        /// <param name="rhs">Instance on the right side of the == operator.</param>
        /// <returns>True if the instances are equal, false otherwise.</returns>
        public static bool operator ==(TimeoutInSeconds lhs, TimeoutInSeconds rhs)
        {
            return lhs.Equals(rhs);
        }

        /// <summary>
        /// Implementation of the != operator.
        /// </summary>
        /// <param name="lhs">Instance on the left side of the != operator.</param>
        /// <param name="rhs">Instance on the right side of the != operator.</param>
        /// <returns>True if the instances are not equal, false otherwise.</returns>
        public static bool operator !=(TimeoutInSeconds lhs, TimeoutInSeconds rhs)
        {
            return !lhs.Equals(rhs);
        }

        /// <summary>
        /// Override the method inherited from the Object class.
        /// </summary>
        /// <returns>The integer count that is encapsulated.</returns>
        public override string ToString()
        {
            return this.Value.ToString();
        }

        /// <summary>
        /// Implementation of the IEquatable interface.
        /// </summary>
        /// <param name="other">Instance that is compared to this instance for equality.</param>
        /// <returns>True if the instances are equal, false otherwise.</returns>
        public bool Equals(TimeoutInSeconds other)
        {
            return this.value == other.value;
        }

        /// <summary>
        /// Override the method inherited from the Object class.
        /// </summary>
        /// <param name="obj">The instance that will be compared with this instance for equality.</param>
        /// <returns>True if the instances are equal, flase otherwise.</returns>
        public override bool Equals(object obj)
        {
            if (!(obj is TimeoutInSeconds))
            {
                return false;
            }

            return Equals((TimeoutInSeconds)obj);
        }

        /// <summary>
        /// Override of the method inherited from the Object class.
        /// </summary>
        /// <returns>An integer that is the hash of this instance.</returns>
        public override int GetHashCode()
        {
            return this.value.GetHashCode();
        }
    }
}
