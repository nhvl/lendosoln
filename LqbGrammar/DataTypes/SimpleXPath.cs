﻿namespace LqbGrammar.DataTypes
{
    using System;
    using System.Linq;
    using LqbGrammar.DataTypes.PathDispatch;
    using LqbGrammar.Validation;

    /// <summary>
    /// Represents a simple data path of element names, separated by the conventional slash ('/') character.
    /// </summary>
    public partial struct SimpleXPath
    {
        /// <summary>
        ///  Initializes a new instance of the <see cref="SimpleXPath"/> struct.
        /// </summary>
        /// <param name="path">The contained data path.</param>
        private SimpleXPath(DataPath path)
        {
            this.DataPath = path;
        }

        /// <summary>
        /// Gets the contained data path of the XPath.
        /// </summary>
        /// <value>The contained data path of the XPath.</value>
        public DataPath DataPath { get; }

        /// <summary>
        /// Parses the input simple XPath and validates it.
        /// </summary>
        /// <param name="xpath">The XPath to validate.</param>
        /// <returns>The validated simple XPath, or null.</returns>
        public static SimpleXPath? Create(string xpath)
        {
            if (!string.IsNullOrEmpty(xpath) && RegularExpressionEngine.Execute(xpath, RegularExpressionString.SimpleXPath).IsMatch())
            {
                var dataPathElements = xpath.Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries).Select(entry => new DataPathBasicElement(entry));
                return new SimpleXPath(new DataPath(dataPathElements.ToList<IDataPathElement>()));
            }

            return null;
        }

        /// <summary>
        /// Returns the canonical string representation of the path.
        /// </summary>
        /// <returns>The canonical string representation of the path.</returns>
        public override string ToString()
        {
            return string.Join("/", this.DataPath.PathList);
        }
    }

    /// <summary>
    /// Contains the implementation of <see cref="IEquatable{SimpleXPath}"/> using the <see cref="DataPath"/> property.
    /// </summary>
    public partial struct SimpleXPath : IEquatable<SimpleXPath>
    {
        /// <summary>
        /// The equality operator.
        /// </summary>
        /// <param name="lhs">The instance that is on the left side of the == operator.</param>
        /// <param name="rhs">The instance that is on the right side of the == operator.</param>
        /// <returns>True if the instances are equal, false otherwise.</returns>
        public static bool operator ==(SimpleXPath lhs, SimpleXPath rhs)
        {
            return lhs.Equals(rhs);
        }

        /// <summary>
        /// The inequality operator.
        /// </summary>
        /// <param name="lhs">The instance that is on the left side of the != operator.</param>
        /// <param name="rhs">The instance that is on the right side of the != operator.</param>
        /// <returns>True if the two instances are not equal, false otherwise.</returns>
        public static bool operator !=(SimpleXPath lhs, SimpleXPath rhs)
        {
            return !lhs.Equals(rhs);
        }

        /// <summary>
        /// Determines if two instances are equal.
        /// </summary>
        /// <param name="other">Instance that is checked for equality with this instance.</param>
        /// <returns>True if other is equal to this instance, false otherwise.</returns>
        public bool Equals(SimpleXPath other)
        {
            return this.DataPath.Equals(other.DataPath);
        }

        /// <summary>
        /// Override the Equals method inherited from the object class.
        /// </summary>
        /// <param name="obj">Instance that is checked for equality with this instance.</param>
        /// <returns>True if <paramref name="obj"/> is equal to this instance, false otherwise.</returns>
        public override bool Equals(object obj)
        {
            return obj is SimpleXPath && this.Equals((SimpleXPath)obj);
        }

        /// <summary>
        /// Override of the GetHashCode method inherited from the object class.
        /// </summary>
        /// <returns>An integer this is the hash code for this instance.</returns>
        public override int GetHashCode()
        {
            return this.DataPath.GetHashCode();
        }
    }
}
