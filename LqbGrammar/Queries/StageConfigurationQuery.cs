﻿namespace LqbGrammar.Queries
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// LQB uses two categories of configuration data, Stage and Site.
    /// This interface defines the semantics of the Stage category
    /// of configuration data.
    /// </summary>
    public interface IStageConfigurationQuery : ILqbQuery
    {
        /// <summary>
        /// Retrieve all the stage configuration values.
        /// </summary>
        /// <returns>A dictionary containing the stage configuration values.</returns>
        Dictionary<string, Tuple<int, string>> ReadAllValues();
    }
}
