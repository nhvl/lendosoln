﻿namespace LqbGrammar.Queries
{
    /// <summary>
    /// Factory interface from creating interfaces to configuration sources.
    /// </summary>
    public interface IConfigurationQueryFactory
    {
        /// <summary>
        /// Create an interface implementation for retrieving site configuration data.
        /// </summary>
        /// <returns>An implementation of the ISiteConfigurationQuery interface.</returns>
        ISiteConfigurationQuery CreateSiteConfiguration();

        /// <summary>
        /// Create an interface implementation for retrieving stage configuration data.
        /// </summary>
        /// <returns>An implementation of the IStageConfigurationQuery interface.</returns>
        IStageConfigurationQuery CreateStageConfiguration();
    }
}
