﻿namespace LqbGrammar.Queries
{
    using System.Collections.Generic;

    /// <summary>
    /// LQB uses two categories of configuration data, Stage and Site.
    /// This interface defines the semantics of the Site category
    /// of configuration data.
    /// </summary>
    public interface ISiteConfigurationQuery : ILqbQuery
    {
        /// <summary>
        /// Retrieve all the site configuration values.
        /// </summary>
        /// <returns>A dictionary containing the site configuration values.</returns>
        Dictionary<string, string> ReadAllValues();
    }
}
