﻿namespace LqbGrammar.Commands
{
    using System.Collections.Generic;

    /// <summary>
    /// Defines an interface for representing status messages and their accompanying statuses.
    /// </summary>
    /// <typeparam name="TStatus">The type of the object representing the status. This will usually be string or an enum type.</typeparam>
    /// <typeparam name="TMessage">The type of the messages that might accompany a status. This will usually be string but may contain additional information, such as message severity.</typeparam>
    public interface IStatusDetails<TStatus, TMessage>
    {
        /// <summary>
        /// Gets the status type being expressed by the object. For example, Error/Warning/OK.
        /// </summary>
        /// <value>Status type.</value>
        TStatus Status
        {
            get;
        }

        /// <summary>
        /// Gets a list of messages associated with <see cref="Status"/>. For example, a list of error or warning messages, or a confirmation number.
        /// </summary>
        /// <value>List of messages.</value>
        IList<TMessage> Messages
        {
            get;
        }
    }
}
