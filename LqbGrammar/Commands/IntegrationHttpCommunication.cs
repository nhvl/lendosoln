﻿namespace LqbGrammar.Commands
{
    using System.Net;
    using DataTypes;
    using Drivers.HttpRequest;

    /// <summary>
    /// Defines methods to be executed in the process of communicating via HTTP with an integration vendor.
    /// </summary>
    /// <typeparam name="TResponse">The type of response object that comes back from the communication.</typeparam>
    public interface IIntegrationHttpCommunication<TResponse> : ILqbCommand
    {
        /// <summary>
        /// Gets the label to use as a header for messages related to this communication in the logs.
        /// </summary>
        string LogHeader { get; }

        /// <summary>
        /// Gets a value indicating whether the request timing should be logged.
        /// </summary>
        bool LogRequestTiming { get; }

        /// <summary>
        /// Gets a value indicating whether anything should be logged.
        /// </summary>
        bool ShouldLog { get; }

        /// <summary>
        /// Called before the request is sent to get the request data for logging.
        /// </summary>
        /// <returns>A loggable version of the request that will be sent.</returns>
        string GetRequestForLogging();

        /// <summary>
        /// Gets the URI path for the communication endpoint.
        /// </summary>
        /// <returns>The endpoint path.</returns>
        LqbAbsoluteUri GetEndpointPath();

        /// <summary>
        /// Generates options for the HTTP request. Called immediately before sending the request.
        /// </summary>
        /// <returns>Options describing the HTTP request being sent.</returns>
        HttpRequestOptions GenerateHttpRequest();

        /// <summary>
        /// Generates additional timing data for the communication, if applicable.
        /// </summary>
        /// <returns>
        /// The additional timing data for the communication, if any.
        /// </returns>
        IntegrationHttpCommunicationTimingData GetAdditionalTimingData();

        /// <summary>
        /// Provides a masking function to remove passwords and other sensitive information from the response string and prepare it for logging.
        /// If the response body was null (for instance, if the response body was written to a file instead of loaded in memory), this method will not be called.
        /// </summary>
        /// <param name="responseBody">The response body to mask.</param>
        /// <returns>The masked response, ready for logging.</returns>
        string MaskResponseString(string responseBody);

        /// <summary>
        /// Handles a web exception. The exception will be automatically logged, but this method can be used for rolling back side-effects or sending additional notifications.
        /// </summary>
        /// <param name="exception">The exception to handle.</param>
        /// <returns>Returns false to continue throwing the exception. Returns true to have the driver capture the exception in its return <see cref="Result{TResponse}"/>.</returns>
        bool HandleWebException(WebException exception);

        /// <summary>
        /// Converts the web response into a result expressing whether it was successful or not, and a response object if successful.
        /// Called after the request, when the request is executed successfully (Generally, this means a 2xx response code).
        /// Any additional response processing can also happen in this method.
        /// </summary>
        /// <param name="response">The web response to parse the response object from.</param>
        /// <returns>A result object with an error or response object.</returns>
        Result<TResponse> ParseResponse(HttpRequestOptions response);
    }
}
