﻿namespace LqbGrammar.Adapters
{
    using System;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;

    /// <summary>
    /// Adapter interface for invoking a method.
    /// </summary>
    public interface IMethodInvokeAdapter : ILqbAdapter
    {
        /// <summary>
        /// Gets a key that a driver can use to retrieve the control information.  If null then no control information will be set.
        /// </summary>
        /// <value>A key that a driver can use to retrieve the control information.</value>
        string ControlInformationKey { get; }

        /// <summary>
        /// Determine whether or not the call will be made directly or done on a different thread.
        /// This will allow the client code to make efficient calls when a direct call is made.
        /// </summary>
        /// <returns>True if the method will be called using a different thread, rather than directly.</returns>
        bool InvokeIsOffThread();

        /// <summary>
        /// Different adapters may require additional data to route the method calls, or other configuration information.
        /// </summary>
        /// <param name="control">Control information that a particular adapter may require.</param>
        void SetControlInformation(string control);

        /// <summary>
        /// Execute a method that accepts an argument of type S and returns void.
        /// </summary>
        /// <typeparam name="T">Type of the class that contains the method.</typeparam>
        /// <typeparam name="S">Type of the argument to the method.</typeparam>
        /// <param name="call">The method that will get invoked.</param>
        /// <param name="previousCallId">An identifier that links this call to the end of a previous call, or null.</param>
        /// <returns>A job identifier, or null if there is no identifier.</returns>
        MethodInvokeIdentifier Run<T, S>(SingleArgumentMethod<T, S> call, MethodInvokeIdentifier previousCallId) where T : new();

        /// <summary>
        /// After waiting for a delay time, execute a method that accepts an argument of type S and returns void.
        /// </summary>
        /// <typeparam name="T">Type of the class that contains the method.</typeparam>
        /// <typeparam name="S">Type of the argument to the method.</typeparam>
        /// <param name="call">The method that will get invoked.</param>
        /// <param name="delay">The delay time to wait prior to executing the method.</param>
        /// <returns>A job identifier, or null if there is no identifier.</returns>
        MethodInvokeIdentifier RunAfterDelay<T, S>(SingleArgumentMethod<T, S> call, TimeoutInSeconds delay) where T : new();

        // Hangfire also has recurring jobs, but the scheduling uses cron expressions so prior to designing the
        // interface I'd like to get an idea of what sort of schedules are used in LQB.
    }

    /// <summary>
    /// Factory interface for creating implementations of the IMethodInvokeAdapter interface.
    /// </summary>
    public interface IMethodInvokeAdapterFactory : ILqbFactory
    {
        /// <summary>
        /// Create an implementation of the IMethodInvokeAdapter interface.
        /// </summary>
        /// <returns>An implementation of the IMethodInvokeAdapter interface.</returns>
        IMethodInvokeAdapter Create();
    }
}
