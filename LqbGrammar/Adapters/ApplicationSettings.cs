﻿namespace LqbGrammar.Adapters
{
    using System.Collections.Generic;

    /// <summary>
    /// Interface used to retrieve the AppSettings values
    /// from an application's configuration file.
    /// </summary>
    public interface IApplicationSettingsAdapter : ILqbAdapter
    {
        /// <summary>
        /// Retrieve the application settings.
        /// Note that the name and value of each setting is not encapsulated into a data type for
        /// verification.  We will defer that to the drivers to simplify the implementation
        /// since we are dealing with a collection class rather than individual values.
        /// </summary>
        /// <returns>A dictionary with the name/value pairs from the application settings.</returns>
        Dictionary<string, string> ReadAllSettings();
    }

    /// <summary>
    /// Factory interface for creation of implementations of IApplicationSettingsAdapter.
    /// </summary>
    public interface IApplicationSettingsAdapterFactory : ILqbFactory
    {
        /// <summary>
        /// Create an instance of an implementation of IApplicationSettingsAdapter.
        /// </summary>
        /// <returns>An instance of an implementation of IApplicationSettingsAdapter.</returns>
        IApplicationSettingsAdapter Create();
    }
}
