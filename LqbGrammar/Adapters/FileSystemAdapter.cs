﻿namespace LqbGrammar.Adapters
{
    using System;
    using System.Collections.Generic;
    using LqbGrammar;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers.FileSystem;

    /// <summary>
    /// Adapter interface for performing file operations.
    /// </summary>
    public interface IFileOperationAdapter : ILqbAdapter
    {
        /// <summary>
        /// Copy the source file to the target file.
        /// </summary>
        /// <param name="source">The source file.</param>
        /// <param name="target">The target file.</param>
        /// <param name="allowOverwrite">True if overwriting an existing file is allowed, false otherwise.</param>
        void Copy(LocalFilePath source, LocalFilePath target, bool allowOverwrite);

        /// <summary>
        /// Move the source file to the target location if the target doesn't already exist.
        /// If the target exists an exception will be thrown, the Copy and Delete methods 
        /// must be used instead.
        /// </summary>
        /// <param name="source">The file that will be moved.</param>
        /// <param name="target">The location to which the file will be moved.</param>
        void Move(LocalFilePath source, LocalFilePath target);

        /// <summary>
        /// Deletes the specified file.
        /// </summary>
        /// <param name="path">The file to delete.</param>
        void Delete(LocalFilePath path);

        /// <summary>
        /// Detects whether or not a specified file exists.
        /// </summary>
        /// <param name="path">The file to check.</param>
        /// <returns>True if the file exists, false otherwise.</returns>
        bool Exists(LocalFilePath path);

        /// <summary>
        /// Ensure the specified file is not read-only.
        /// </summary>
        /// <param name="path">The specified file.</param>
        void TurnOffReadOnly(LocalFilePath path);

        /// <summary>
        /// Retrieve the timestamp when the file was last modified.
        /// </summary>
        /// <param name="path">The specified file.</param>
        /// <returns>The timestamp when the file was last modified.</returns>
        LqbEventDate GetLastWriteTime(LocalFilePath path);

        /// <summary>
        /// Set the last write time propertry for the specified file.
        /// </summary>
        /// <param name="path">The specified file.</param>
        /// <param name="timestamp">The value that is to be set as the last write time.</param>
        void SetLastWriteTime(LocalFilePath path, LqbEventDate timestamp);
    }

    /// <summary>
    /// Adapter interface for working with binary files.
    /// </summary>
    public interface IBinaryFileAdapter : ILqbAdapter
    {
        /// <summary>
        /// Creates a new file, writes the specified byte array to the file,
        /// and then closes the file.  If the target file already exists, it is overwritten.
        /// </summary>
        /// <param name="path">The file to which the data is written.</param>
        /// <param name="bytes">The data that is written to the file.</param>
        void WriteAllBytes(LocalFilePath path, byte[] bytes);

        /// <summary>
        /// Create or overwrite a binary file.
        /// </summary>
        /// <param name="path">The file to create or overwrite.</param>
        /// <param name="writeHandler">Delegate that writes data to the file.</param>
        void OpenNew(LocalFilePath path, Action<LqbBinaryStream> writeHandler);

        /// <summary>
        /// Open an existing binary file for reading.
        /// </summary>
        /// <param name="path">The file to open.</param>
        /// <param name="readHandler">Delegate that reads data from the file.</param>
        void OpenRead(LocalFilePath path, Action<LqbBinaryStream> readHandler);

        /// <summary>
        /// Read all the data within a binary file.
        /// </summary>
        /// <param name="path">The binary file from which the data will be read.</param>
        /// <returns>The data read from the binary file.</returns>
        byte[] ReadAllBytes(LocalFilePath path);
    }

    /// <summary>
    /// Interface used to work with text files.
    /// </summary>
    public interface ITextFileAdapter : ILqbAdapter
    {
        /// <summary>
        /// Opens the file, appends the string to the file, and then closes the file.
        /// If the file does not exist, this method creates a file, writes the specified
        /// string to the file, then closes the file.
        /// </summary>
        /// <param name="path">The file to which the text is appended.</param>
        /// <param name="text">The text that is appended to the file.</param>
        void AppendString(LocalFilePath path, string text);

        /// <summary>
        /// Open an existing text file in append mode, or create a new file if it
        /// does not exist, for writing.
        /// </summary>
        /// <param name="path">The file to which text can be appended.</param>
        /// <param name="writeHandler">Delegate that handles writing text to the file.</param>
        void OpenForAppend(LocalFilePath path, Action<LqbTextFileWriter> writeHandler);

        /// <summary>
        /// Creates a new text file if it doesn't exist, or overwrite an existing file,
        /// with the specified text and then closes the file.
        /// </summary>
        /// <param name="path">The file to which the text is written.</param>
        /// <param name="text">The text that is written to the file.</param>
        /// <param name="writeBOM">True if the a byte order mark should be included at the beginning of the file.</param>
        void WriteString(LocalFilePath path, string text, bool writeBOM);

        /// <summary>
        /// Create a new text file if it doesn't exist, or overwrite an existing text file.
        /// </summary>
        /// <param name="path">The file to which text can be written.</param>
        /// <param name="writeHandler">Delegate that handles writing text to the file.</param>
        void OpenNew(LocalFilePath path, Action<LqbTextFileWriter> writeHandler);

        /// <summary>
        /// Open, read all the contents, then close a text file.
        /// </summary>
        /// <param name="path">The file that is to be read.</param>
        /// <returns>The file contents.</returns>
        string ReadFile(LocalFilePath path);

        /// <summary>
        /// Open, read all contents as a set of lines, then close a text file.
        /// Note that this method yields lines before they are all in memory
        /// so it is efficient for large files.
        /// </summary>
        /// <param name="path">The file that is to be read.</param>
        /// <returns>The file contents as a set of lines.</returns>
        IEnumerable<string> ReadLines(LocalFilePath path);

        /// <summary>
        /// Open a text file for reading.
        /// </summary>
        /// <param name="path">The file that is available for reading.</param>
        /// <param name="readHandler">Delegate the reads the file.</param>
        void OpenRead(LocalFilePath path, Action<LqbTextFileReader> readHandler);
    }

    /// <summary>
    /// Adapter interface for reading/writing XML to/from local files.
    /// </summary>
    public interface IXmlFileAdapter : ILqbAdapter
    {
        /// <summary>
        /// Read a file containing XML.
        /// </summary>
        /// <param name="path">The file to read.</param>
        /// <returns>The XML data.</returns>
        LqbXmlElement Load(LocalFilePath path);

        /// <summary>
        /// Write XML data to a file.
        /// </summary>
        /// <param name="path">The file to which the XML is to be written.</param>
        /// <param name="xml">The XML data.</param>
        void Save(LocalFilePath path, LqbXmlElement xml);
    }

    /// <summary>
    /// Interface used to create drivers that interact with the local file system.
    /// </summary>
    public interface IFileSystemAdapterFactory : ILqbFactory
    {
        /// <summary>
        /// Create a adapter used to carry out file operations.
        /// </summary>
        /// <returns>A file operation adapter.</returns>
        IFileOperationAdapter CreateFileOperationAdapter();

        /// <summary>
        /// Create a adapter used to work with binary files.
        /// </summary>
        /// <returns>A binary file adapter.</returns>
        IBinaryFileAdapter CreateBinaryFileAdapter();

        /// <summary>
        /// Create a adapter used to work with text files.
        /// </summary>
        /// <returns>A text file adapter.</returns>
        ITextFileAdapter CreateTextFileAdapter();

        /// <summary>
        /// Create a adapter used to work with XML files.
        /// </summary>
        /// <returns>An XML file adapter.</returns>
        IXmlFileAdapter CreateXmlFileAdapter();
    }
}