﻿namespace LqbGrammar.Adapters
{
    using DataTypes;

    /// <summary>
    /// Defines the interface for base64 value encoding and decoding.
    /// </summary>
    public interface IBase64EncodingAdapter : ILqbAdapter
    {
        /// <summary>
        /// Encode the value in base64.
        /// </summary>
        /// <param name="unencodedData">The value to be encoded.</param>
        /// <returns>The base64 encoded value.</returns>
        Base64EncodedData Encode(byte[] unencodedData);

        /// <summary>
        /// Decode the value in base64.
        /// </summary>
        /// <param name="base64EncodedData">The encoded value.</param>
        /// <returns>The decoded value.</returns>
        byte[] Decode(Base64EncodedData base64EncodedData);
    }

    /// <summary>
    /// Factory for creating implementations of the IBase64EncodingAdapter interface.
    /// </summary>
    public interface IBase64EncodingAdapterFactory : ILqbFactory
    {
        /// <summary>
        /// Create an implementation of the IBase64EncodingAdapter interface.
        /// </summary>
        /// <returns>An implementation of the IBase64EncodingAdapter interface.</returns>
        IBase64EncodingAdapter Create();
    }
}
