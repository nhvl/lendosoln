﻿namespace LqbGrammar.Adapters
{
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Defines the interface to stored procedures in SQL Server.
    /// </summary>
    public interface IStoredProcedureAdapter : ILqbAdapter
    {
        /// <summary>
        /// Execute a stored procedure that modifies data.
        /// </summary>
        /// <param name="conn">The connection to the DB.</param>
        /// <param name="transaction">If using a transaction, pass it in here, else pass in null.</param>
        /// <param name="procedureName">The name of the stored procedure.</param>
        /// <param name="parameters">The procedure's parameters.</param>
        /// <returns>The number of rows modified by the stored procedure.</returns>
        ModifiedRowCount ExecuteNonQuery(IDbConnection conn, IDbTransaction transaction, StoredProcedureName procedureName, IEnumerable<DbParameter> parameters);

        /// <summary>
        /// Call a stored procedure that returns a single value.
        /// </summary>
        /// <param name="conn">The connection to the DB.</param>
        /// <param name="transaction">If using a transaction, pass it in here, else pass in null.</param>
        /// <param name="procedureName">The name of the stored procedure.</param>
        /// <param name="parameters">The procedure's parameters.</param>
        /// <returns>The single valued return data.</returns>
        object ExecuteScalar(IDbConnection conn, IDbTransaction transaction, StoredProcedureName procedureName, IEnumerable<DbParameter> parameters);

        /// <summary>
        /// Call a stored procedure that returns a row set.
        /// </summary>
        /// <param name="conn">The connection to the DB.</param>
        /// <param name="transaction">If using a transaction, pass it in here, else pass in null.</param>
        /// <param name="procedureName">The name of the stored procedure.</param>
        /// <param name="parameters">The procedure's parameters.</param>
        /// <returns>An interface used to iterate through the returned row set.</returns>
        IDataReader ExecuteReader(IDbConnection conn, IDbTransaction transaction, StoredProcedureName procedureName, IEnumerable<DbParameter> parameters);

        /// <summary>
        /// Call a stored procedure that returns data, with the full data stored in a DataSet.
        /// </summary>
        /// <param name="conn">The connection to the DB.</param>
        /// <param name="transaction">If using a transaction, pass it in here, else pass in null.</param>
        /// <param name="procedureName">The name of the stored procedure.</param>
        /// <param name="parameters">The procedure's parameters.</param>
        /// <returns>The data set containing the full returned data.</returns>
        DataSet ExecuteDataSet(IDbConnection conn, IDbTransaction transaction, StoredProcedureName procedureName, IEnumerable<DbParameter> parameters);
    }

    /// <summary>
    /// Factory for creating an implementation of IStoredProcedureAdapter.
    /// </summary>
    public interface IStoredProcedureAdapterFactory : ILqbFactory
    {
        /// <summary>
        /// Create and return an implementation of IStoredProcedureAdapter.
        /// </summary>
        /// <param name="factory">The database provider factory.</param>
        /// <param name="timeout">The timeout to use when communicating with a database.</param>
        /// <returns>An implementation of IStoredProcedureAdapter.</returns>
        IStoredProcedureAdapter Create(DbProviderFactory factory, TimeoutInSeconds timeout);
    }
}
