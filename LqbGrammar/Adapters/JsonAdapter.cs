﻿namespace LqbGrammar.Adapters
{
    using System.IO;
    using Drivers;

    /// <summary>
    /// Interface used to serialize/deserialize objects to/from JSON.
    /// </summary>
    public interface IJsonAdapter : ILqbAdapter
    {
        /// <summary>
        /// Serialize an object of type T to a JSON string.
        /// </summary>
        /// <typeparam name="T">The type of the object to be serialized.</typeparam>
        /// <param name="forSerialization">An instance of the type to serialize.</param>
        /// <returns>The serialization of the instance to a JSON string.</returns>
        string Serialize<T>(T forSerialization) where T : class, new();

        /// <summary>
        /// Serialize an object of type T to a JSON string.
        /// </summary>
        /// <typeparam name="T">The type of the object to be serialized.</typeparam>
        /// <typeparam name="R">The type used as the resolver.</typeparam>
        /// <param name="forSerialization">An instance of the type to serialize.</param>
        /// <param name="resolver">A class that is used for type resolution during serialization.</param>
        /// <returns>The serialization of the instance to a JSON string.</returns>
        string Serialize<T, R>(T forSerialization, R resolver) where T : class, new() where R : class;

        /// <summary>
        /// Deserialize a JSON string back to an instance of the indicated type.
        /// </summary>
        /// <typeparam name="T">The type of the object to be deserialize.</typeparam>
        /// <param name="serialized">The JSON serialization of an instance of the target type.</param>
        /// <returns>The deserialized instance of the target type.</returns>
        T Deserialize<T>(string serialized) where T : class, new();

        /// <summary>
        /// Deserialize a JSON string back to an instance of the indicated type.
        /// </summary>
        /// <typeparam name="T">The type of the object to be deserialize.</typeparam>
        /// <typeparam name="R">The type used as the converter.</typeparam>
        /// <param name="reader">The JSON serialization of an instance of the target type.</param>
        /// <param name="converter">The converter used during deserialization.</param>
        /// <returns>The deserialized instance of the target type.</returns>
        T Deserialize<T, R>(StreamReader reader, R converter) where T : class, new() where R : class;

        /// <summary>
        /// Sanitizes the json blob directly before it gets passed on to whatever is going to use it.
        /// Will go through each token in the json blob and sanitize each string token. 
        /// </summary>
        /// <param name="json">The json to sanitize.</param>
        /// <returns>The sanitized json blob.</returns>
        string SanitizeJsonString(string json);
    }

    /// <summary>
    /// Factory for creation of implementations of IJsonAdapter.
    /// </summary>
    public interface IJsonAdapterFactory : ILqbFactory
    {
        /// <summary>
        /// Create an instance of a class that implements the IJsonAdapter interface.
        /// </summary>
        /// <param name="serializer">The type of serializer to use.</param>
        /// <param name="options">Options that control the serializer's behavior.</param>
        /// <returns>An implementation of IJsonAdapter.</returns>
        IJsonAdapter Create(JsonSerializer serializer, JsonSerializerOptions options);
    }
}
