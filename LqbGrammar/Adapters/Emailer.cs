﻿namespace LqbGrammar.Adapters
{
    using System;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers.Emailer;

    /// <summary>
    /// Interface defining an adapter to a utility that sends email.
    /// </summary>
    public interface IEmailAdapter : ILqbAdapter
    {
        /// <summary>
        /// Send an email.
        /// </summary>
        /// <param name="email">The data that will be used to populate and send an email.</param>
        void SendEmail(EmailPackage email);
    }

    /// <summary>
    /// Interface defining an adapter to a utility that receives email.
    /// Note that receiving is an operation that involves maintaining a
    /// connection to the server, so this interface inherits from
    /// IDisposable to ensure that the connection is closed.
    /// </summary>
    public interface IReceiveEmailAdapter : ILqbAdapter, IDisposable
    {
        /// <summary>
        /// Gets the number of email messages available on the server.
        /// </summary>
        /// <value>The number of email messages available on the server.</value>
        int Count { get; }

        /// <summary>
        /// Retrieve an identifer for the email with the input index.
        /// </summary>
        /// <param name="index">A value in the range [0, Count).</param>
        /// <returns>An identifier for the specified email.</returns>
        EmailIdentifier GetIdentifier(int index);

        /// <summary>
        /// Retrieve the email data for the input index.
        /// </summary>
        /// <param name="index">A value in the range [0, Count).</param>
        /// <returns>The data for the specified email.</returns>
        IReceivedEmailData GetEmail(int index);

        /// <summary>
        /// Delete the email data for the input index.
        /// </summary>
        /// <param name="index">A value in the range [0, Count).</param>
        void DeleteEmail(int index);
    }

    /// <summary>
    /// Factory interface for creating an implementation of the IEmailAdapter interface.
    /// </summary>
    public interface IEmailAdapterFactory : ILqbFactory
    {
        /// <summary>
        /// Create an email adapter.
        /// </summary>
        /// <param name="server">The email server name that will be used for sending the email.</param>
        /// <param name="port">The port number of the server.</param>
        /// <returns>An email adapter.</returns>
        IEmailAdapter Create(EmailServerName server, PortNumber port);
    }

    /// <summary>
    /// Factory interface for creating an implementation of the IReceiveEmailAdapter interface.
    /// </summary>
    public interface IReceiveEmailAdapterFactory
    {
        /// <summary>
        /// Create an email adapter.
        /// </summary>
        /// <param name="server">The email server name that will be used for sending the email.</param>
        /// <param name="userName">The user name for logging into the email server.</param>
        /// <param name="password">The password for logging into the email server.</param>
        /// <param name="port">The port on which the email server is listening for requests.</param>
        /// <returns>An email adapter.</returns>
        IReceiveEmailAdapter Create(EmailServerName server, string userName, string password, int port);
    }
}
