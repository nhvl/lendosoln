﻿namespace LqbGrammar.Adapters
{
    using Drivers.HttpRequest;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Interface used to issue requests over http.
    /// </summary>
    public interface IHttpRequestAdapter : ILqbAdapter
    {
        /// <summary>
        /// Carry out the communication with the indicated URL.
        /// </summary>
        /// <param name="url">The target URL.</param>
        /// <param name="options">The options that control the request and response parsing.</param>
        /// <returns>True if the communication was successful, false otherwise.</returns>
        bool ExecuteCommunication(LqbAbsoluteUri url, HttpRequestOptions options);
    }

    /// <summary>
    /// Factory for creating implementations of the IHttpRequestAdapter interface.
    /// </summary>
    public interface IHttpRequestAdapterFactory : ILqbFactory
    {
        /// <summary>
        /// Create an implementation of the IHttpRequestAdapter interface.
        /// </summary>
        /// <returns>An implementation of the IHttpRequestAdapter interface.</returns>
        IHttpRequestAdapter Create();
    }
}
