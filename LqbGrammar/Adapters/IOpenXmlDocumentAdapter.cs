﻿namespace LqbGrammar.Adapters
{
    using System.Collections.Generic;
    using DataTypes;

    /// <summary>
    /// Defines the interface to open XML documents.
    /// </summary>
    public interface IOpenXmlDocumentAdapter : ILqbAdapter
    {
        /// <summary>
        /// Sets values for the list of fields in the open XML document.
        /// </summary>
        /// <param name="localFilePath">
        /// The path to the document.
        /// </param>
        /// <param name="fieldNameValuePairs">
        /// The list of field name - value pairs.
        /// </param>
        void SetTextFieldsOnDocument(LocalFilePath localFilePath, IEnumerable<KeyValuePair<string, string>> fieldNameValuePairs);
    }
}
