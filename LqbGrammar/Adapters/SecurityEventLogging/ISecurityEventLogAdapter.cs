﻿namespace LqbGrammar.Adapters.SecurityEventLogging
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using DataTypes;
    using Drivers.SecurityEventLogging;

    /// <summary>
    /// Security event log adapter interface.
    /// </summary>
    public interface ISecurityEventLogAdapter : ILqbAdapter
    {
        /// <summary>
        /// Creates a new security event log.
        /// </summary>
        /// <param name="principal">IUserPrincipal object.</param>
        /// <param name="clientIp">Client IP.</param>
        /// <param name="eventType">Security event type.</param>
        /// <param name="description">Log description.</param>
        /// <param name="conn">IDbConnection object.</param>
        /// <param name="trans">IDbTransaction object.</param>
        void CreateSecurityEventLog(IUserPrincipal principal, string clientIp, SecurityEventType eventType, string description, IDbConnection conn, IDbTransaction trans);

        /// <summary>
        /// Search security event logs.
        /// </summary>
        /// <param name="filter">Security Events Log filter object.</param>
        /// <param name="currentPage">Current page count in the search.</param>
        /// <param name="searchResultsPerPage">Number of search results per page.</param>
        /// <param name="conn">IDbConnection object.</param>
        /// <param name="maxSearchResults">Maximum number allowed for search.</param>
        /// <returns>Search results view model.</returns>
        SecurityEventLogSearchResults SearchSecurityEventLog(SecurityEventLogsFilter filter, int currentPage, int searchResultsPerPage, IDbConnection conn, int maxSearchResults);

        /// <summary>
        /// Gets security event logs for download.
        /// </summary>
        /// <param name="filter">Security Events Log filter object.</param>
        /// <param name="conn">IDbConnection object.</param>
        /// <param name="maxDownloadResults">Maximum number allowed for download.</param>
        /// <returns>Security event logs.</returns>
        IEnumerable<SecurityEventLog> GetSecurityEventLogsForDownload(SecurityEventLogsFilter filter, IDbConnection conn, int maxDownloadResults);
    }
}
