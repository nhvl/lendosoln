﻿namespace LqbGrammar.Adapters.SecurityEventLogging
{
    using System.Data;
    using System.Data.Common;

    /// <summary>
    /// Security event log adapter factory.
    /// </summary>
    public interface ISecurityEventLogAdapterFactory : ILqbFactory
    {
        /// <summary>
        /// Creates new security event log adapter.
        /// </summary>
        /// <returns>Security event log adapter.</returns>
        ISecurityEventLogAdapter Create();
    }
}
