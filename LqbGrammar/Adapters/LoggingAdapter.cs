﻿namespace LqbGrammar.Adapters
{
    using System;
    using System.Collections.Generic;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;

    /// <summary>
    /// Interface used to write messages to a log.  The design of the interface is
    /// based on what currently exists in the LendingQB code, plus the strong
    /// desire to not introduce yet another enumeration for severity level.
    /// </summary>
    public interface ILoggingAdapter : ILqbAdapter
    {
        /// <summary>
        /// Log a trace message.
        /// </summary>
        /// <param name="message">The message to log.</param>
        /// <param name="boundaryType">Type of class that is used as the starting point for logging messages.</param>
        /// <param name="properties">Optional set of properties that will be included in the log.</param>
        void LogTrace(LogMessage message, Type boundaryType, Dictionary<LogPropertyName, LogPropertyValue> properties);

        /// <summary>
        /// Log an informational message.
        /// </summary>
        /// <param name="message">The message to log.</param>
        /// <param name="boundaryType">Type of class that is used as the starting point for logging messages.</param>
        /// <param name="properties">Optional set of properties that will be included in the log.</param>
        void LogInfo(LogMessage message, Type boundaryType, Dictionary<LogPropertyName, LogPropertyValue> properties);

        /// <summary>
        /// Log debug information.
        /// </summary>
        /// <param name="message">The message to log.</param>
        /// <param name="boundaryType">Type of class that is used as the starting point for logging messages.</param>
        /// <param name="properties">Optional set of properties that will be included in the log.</param>
        void LogDebug(LogMessage message, Type boundaryType, Dictionary<LogPropertyName, LogPropertyValue> properties);

        /// <summary>
        /// Log a warning message.
        /// </summary>
        /// <param name="message">The message to log.</param>
        /// <param name="boundaryType">Type of class that is used as the starting point for logging messages.</param>
        /// <param name="properties">Optional set of properties that will be included in the log.</param>
        void LogWarning(LogMessage message, Type boundaryType, Dictionary<LogPropertyName, LogPropertyValue> properties);

        /// <summary>
        /// Log a bug report.
        /// </summary>
        /// <param name="message">The message to log.</param>
        /// <param name="boundaryType">Type of class that is used as the starting point for logging messages.</param>
        /// <param name="properties">Optional set of properties that will be included in the log.</param>
        void LogBug(LogMessage message, Type boundaryType, Dictionary<LogPropertyName, LogPropertyValue> properties);

        /// <summary>
        /// Log an error message.
        /// </summary>
        /// <param name="message">The message to log.</param>
        /// <param name="boundaryType">Type of class that is used as the starting point for logging messages.</param>
        /// <param name="properties">Optional set of properties that will be included in the log.</param>
        void LogError(LogMessage message, Type boundaryType, Dictionary<LogPropertyName, LogPropertyValue> properties);

        /// <summary>
        /// Log an error message using the input stack trace rather than pulling it from the call stack.
        /// </summary>
        /// <param name="message">The message to log.</param>
        /// <param name="stackTrace">Stack trace to use in the log.</param>
        /// <param name="properties">Optional set of properties that will be included in the log.</param>
        void LogError(LogMessage message, string stackTrace, Dictionary<LogPropertyName, LogPropertyValue> properties);
    }

    /// <summary>
    /// Interface for creating implementations of the ILoggingAdapter interface.
    /// </summary>
    public interface ILoggingAdapterFactory : ILqbFactory
    {
        /// <summary>
        /// Create an implementation of the ILoggingAdapter interface.
        /// </summary>
        /// <param name="data">The data that will initialize the adapter.</param>
        /// <returns>An implementation of the ILoggingAdapter interface.</returns>
        ILoggingAdapter Create(LoggingTargetInfo data);
    }
}
