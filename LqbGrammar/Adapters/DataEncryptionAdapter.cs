﻿namespace LqbGrammar.Adapters
{
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Interface describing IDataEncryptionAdapter, which allows for decryption
    /// by an arbitrary key.
    /// </summary>
    public interface IDataEncryptionAdapter : ILqbAdapter
    {
        /// <summary>
        /// Decrypt the provided encrypted data.
        /// </summary>
        /// <param name="encryptedData">The encrypted data.</param>
        /// <param name="encryptionKey">The key to use in decryption.</param>
        /// <returns>The decrypted data.</returns>
        byte[] Decrypt(byte[] encryptedData, EncryptionKey encryptionKey);

        /// <summary>
        /// Encrypt the data with the key.
        /// </summary>
        /// <param name="unencryptedData">The data to be encrypted.</param>
        /// <param name="encryptionKey">The key to use in the encryption.</param>
        /// <returns>The encrypted data.</returns>
        byte[] Encrypt(byte[] unencryptedData, EncryptionKey encryptionKey);
    }

    /// <summary>
    /// Factory for creating implementations of the IEncryptionAdapter interface.
    /// </summary>
    public interface IDataEncryptionAdapterFactory : ILqbFactory
    {
        /// <summary>
        /// Create an implementation of the IEncryptionAdapter interface.
        /// </summary>
        /// <returns>An implementation of the IEncryptionAdapter interface.</returns>
        IDataEncryptionAdapter Create();

        /// <summary>
        /// Create an encryption key from its component parts.
        /// </summary>
        /// <param name="version">The version number for the encryption key.</param>
        /// <param name="key">The encryption key.</param>
        /// <param name="iv">The optional initialization vector, or null.</param>
        /// <param name="blockSize">Optional parameter to override the default block size (128).</param>
        /// <returns>The initialized encryption key.</returns>
        EncryptionKey CreateAesKey(int version, byte[] key, byte[] iv, int? blockSize);

        /// <summary>
        /// Generates a new encryption key.
        /// </summary>
        /// <returns>An encryption key.</returns>
        EncryptionKey GenerateKey();
    }
}
