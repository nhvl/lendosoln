﻿namespace LqbGrammar.Adapters
{
    using LqbGrammar;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Adapter interface to get environment information.
    /// </summary>
    public interface IEnvironmentAdapter : ILqbAdapter
    {
        /// <summary>
        /// Gets the preferred newline string for the current environment.
        /// </summary>
        /// <value>The preferred newline string for the current environment.</value>
        string NewLine { get; }

        /// <summary>
        /// Gets the name of the machine on which the calling code is running.
        /// </summary>
        /// <value>The name of the machine on which the calling code is running.</value>
        MachineName MachineName { get; }

        /// <summary>
        /// Gets the current working directory of the application in which the calling code is running.
        /// </summary>
        /// <value>The current working directory of the application in which the calling code is running.</value>
        string CurrentDirectory { get; }

        /// <summary>
        /// Gets the number of processors hosted by the machine on which the calling code is running.
        /// </summary>
        /// <value>The number of processors hosted by the machine on which the calling code is running.</value>
        int ProcessorCount { get; }

        /// <summary>
        /// Gets the current stack trace information.
        /// </summary>
        /// <value>The current stack trace information.</value>
        string StackTrace { get; }

        /// <summary>
        /// Terminates the current process and returns the exit code to the operating system.
        /// </summary>
        /// <param name="code">The exit code returned to the operating system.</param>
        void Exit(int code);
    }

    /// <summary>
    /// Factory for creating implementations of the IEnvironmentAdapter interface.
    /// </summary>
    public interface IEnvironmentAdapterFactory
    {
        /// <summary>
        /// Create an implementation of the IEnvironmentAdapter interface.
        /// </summary>
        /// <returns>An implementation of the IEnvironmentAdapter interface.</returns>
        IEnvironmentAdapter Create();
    }
}
