﻿namespace LqbGrammar.Adapters
{
    using System;
    using DataTypes;

    /// <summary>
    /// Interface to work with the FileDB system.
    /// </summary>
    public interface IFileDbAdapter : ILqbAdapter
    {
        /// <summary>
        /// Save a new file into the file storage.
        /// </summary>
        /// <param name="location">The location of the file storage repository.</param>
        /// <param name="key">The file identifier used to retrieve the file later.</param>
        /// <param name="encryptionKey">Encryption key to use, or null.</param>
        /// <param name="saveFileHandler">A delegate which will save the file to the location provided in the LocalFilePath.</param>
        void SaveNewFile(FileStorageIdentifier location, FileIdentifier key, byte[] encryptionKey, Action<LocalFilePath> saveFileHandler);

        /// <summary>
        /// Retrieve a file from the file storage.
        /// </summary>
        /// <param name="location">The location of the file storage repository.</param>
        /// <param name="key">The file identifier previously used to save the file.</param>
        /// <param name="encryptionKey">Encryption key to use, or null.</param>
        /// <param name="readFileHandler">A delegate which will read the file from the location provided in the LocalFilePath.</param>
        void RetrieveFile(FileStorageIdentifier location, FileIdentifier key, byte[] encryptionKey, Action<LocalFilePath> readFileHandler);

        /// <summary>
        /// Retrieve a file from the file storage and return a token that can be used to re-store the file.
        /// </summary>
        /// <param name="location">The location of the file storage repository.</param>
        /// <param name="key">The file identifier previously used to save the file.</param>
        /// <param name="encryptionKey">Encryption key to use, or null.</param>
        /// <param name="useFileHandler">A delegate which will read the file from the location provided in the IFileToken, and can re-store the file with this token.</param>
        void UseFile(FileStorageIdentifier location, FileIdentifier key, byte[] encryptionKey, Action<LqbGrammar.Drivers.IFileToken> useFileHandler);

        /// <summary>
        /// Save a file that was previously retrieved from the file storage using the UseFile method.
        /// </summary>
        /// <param name="location">The location of the file storage repository.</param>
        /// <param name="key">The file identifier used to retrieve the file later, this can be the same as or different from the key passed into the UseFile method.</param>
        /// <param name="encryptionKey">Encryption key to use, or null.</param>
        /// <param name="token">A file token that holds the location of the file in the local environment.</param>
        void ReSaveFile(FileStorageIdentifier location, FileIdentifier key, byte[] encryptionKey, LqbGrammar.Drivers.IFileToken token);

        /// <summary>
        /// Delete the file from the file storage.
        /// </summary>
        /// <param name="location">The location of the file storage repository.</param>
        /// <param name="key">The file identifier used to delete.</param>
        void DeleteFile(FileStorageIdentifier location, FileIdentifier key);

        /// <summary>
        /// Check whether the given file exist in the file storage.
        /// </summary>
        /// <param name="location">The location of the file storage repository.</param>
        /// <param name="key">The file identifier.</param>
        /// <returns>True if the file exist false otherwise.</returns>
        bool FileExists(FileStorageIdentifier location, FileIdentifier key);
    }

    /// <summary>
    /// Interface to work with the AWS system.
    /// </summary>
    public interface IAwsFileStorageAdapter : ILqbAdapter
    {
        /// <summary>
        /// Save a new file into the file storage.
        /// </summary>
        /// <param name="location">The location of the file storage repository.</param>
        /// <param name="key">The file identifier used to retrieve the file later.</param>
        /// <param name="saveFileHandler">A delegate which will save the file to the location provided in the LocalFilePath.</param>
        void SaveNewFile(FileStorageIdentifier location, FileIdentifier key, Action<LocalFilePath> saveFileHandler);

        /// <summary>
        /// Retrieve a file from the file storage.
        /// </summary>
        /// <param name="location">The location of the file storage repository.</param>
        /// <param name="key">The file identifier previously used to save the file.</param>
        /// <param name="readFileHandler">A delegate which will read the file from the location provided in the LocalFilePath.</param>
        void RetrieveFile(FileStorageIdentifier location, FileIdentifier key, Action<LocalFilePath> readFileHandler);

        /// <summary>
        /// Retrieve a file from the file storage and return a token that can be used to re-store the file.
        /// </summary>
        /// <param name="location">The location of the file storage repository.</param>
        /// <param name="key">The file identifier previously used to save the file.</param>
        /// <param name="useFileHandler">A delegate which will read the file from the location provided in the IFileToken, and can re-store the file with this token.</param>
        void UseFile(FileStorageIdentifier location, FileIdentifier key, Action<LqbGrammar.Drivers.IFileToken> useFileHandler);

        /// <summary>
        /// Save a file that was previously retrieved from the file storage using the UseFile method.
        /// </summary>
        /// <param name="location">The location of the file storage repository.</param>
        /// <param name="key">The file identifier used to retrieve the file later, this can be the same as or different from the key passed into the UseFile method.</param>
        /// <param name="token">A file token that holds the location of the file in the local environment.</param>
        void ReSaveFile(FileStorageIdentifier location, FileIdentifier key, LqbGrammar.Drivers.IFileToken token);

        /// <summary>
        /// Delete the file from the file storage.
        /// </summary>
        /// <param name="location">The location of the file storage repository.</param>
        /// <param name="key">The file identifier used to delete.</param>
        void DeleteFile(FileStorageIdentifier location, FileIdentifier key);

        /// <summary>
        /// Check whether the given file exist in the file storage.
        /// </summary>
        /// <param name="location">The location of the file storage repository.</param>
        /// <param name="key">The file identifier.</param>
        /// <returns>True if the file exist false otherwise.</returns>
        bool FileExists(FileStorageIdentifier location, FileIdentifier key);
    }

    /// <summary>
    /// Interface used to create an implementation of the IFileDbAdapter interface.
    /// </summary>
    public interface IFileDbAdapterFactory : ILqbFactory
    {
        /// <summary>
        /// Create an implementation of the IFileDbAdapter interface.
        /// </summary>
        /// <returns>An implementation of the IFileDbAdapter interface.</returns>
        IFileDbAdapter Create();
    }

    /// <summary>
    /// Interface used to create an implementation of the IAwsFileStorageAdapter interface.
    /// </summary>
    public interface IAwsFileStorageAdapterFactory : ILqbFactory
    {
        /// <summary>
        /// Create an implementation of the IAwsFileStorageAdapter interface.
        /// </summary>
        /// <returns>An implementation of the IAwsFileStorageAdapter interface.</returns>
        IAwsFileStorageAdapter Create();
    }
}
