﻿namespace LqbGrammar.Adapters
{
    using System;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Interface that defines interaction with a message queue.
    /// </summary>
    public interface IMessageQueueAdapter : ILqbAdapter
    {
        /// <summary>
        /// Send XML to a message queue.
        /// </summary>
        /// <param name="queue">The path to the message queue.</param>
        /// <param name="label">An optional label to attach to the message.</param>
        /// <param name="xml">The XML to be sent.</param>
        void SendXML(MessageQueuePath queue, MessageLabel? label, LqbXmlElement xml);

        /// <summary>
        /// Send a JSON representation of an object to a message queue.
        /// </summary>
        /// <param name="queue">The path to the message queue.</param>
        /// <param name="label">An optional label to attach to the message.</param>
        /// <param name="obj">The object to send.</param>
        void SendJSON(MessageQueuePath queue, MessageLabel? label, object obj);

        /// <summary>
        /// Initialize a message queue for reading XML messages.
        /// </summary>
        /// <param name="queue">The path to the message queue.</param>
        /// <returns>The message queue.</returns>
        LqbMessageQueue PrepareSourceQueueForXML(MessageQueuePath queue);

        /// <summary>
        /// Initialize a message queue for reading JSON messages.
        /// </summary>
        /// <param name="queue">The path to the message queue.</param>
        /// <returns>The message queue.</returns>
        LqbMessageQueue PrepareSourceQueueForJSON(MessageQueuePath queue);

        /// <summary>
        /// Read an XML message from the message queue, waiting for the timeout.
        /// </summary>
        /// <param name="queue">The message queue.</param>
        /// <param name="timeout">The time to wait for a message prior to returning.  If null is passed in then the wait will be very short (1/10 millisecond).</param>
        /// <param name="arrivalTime">The time the message arrived in the queue.</param>
        /// <returns>The received message, or null if there are no messages within the timeout waiting period.</returns>
        LqbXmlElement? ReceiveXML(LqbMessageQueue queue, TimeoutInSeconds? timeout, out DateTime arrivalTime);

        /// <summary>
        /// Read a JSON message from the message queue and deserialize to the specified type, waiting for the timeout.
        /// </summary>
        /// <typeparam name="T">The type that expected for the JSON data.</typeparam>
        /// <param name="queue">The message queue.</param>
        /// <param name="timeout">The time to wait for a message prior to returning.  If null is passed in then the wait will be very short (1/10 millisecond).</param>
        /// <param name="arrivalTime">The time the message arrived in the queue.</param>
        /// <returns>A deserialized instance of the expected type, or null if there are no messages within the timeout waiting period.</returns>
        T ReceiveJSON<T>(LqbMessageQueue queue, TimeoutInSeconds? timeout, out DateTime arrivalTime) where T : class, new();
    }

    /// <summary>
    /// Interface used to create implementations of the IMessageQueueAdapter interface.
    /// </summary>
    public interface IMessageQueueAdapterFactory : ILqbFactory
    {
        /// <summary>
        /// Create an implementation of the IMessageQueueAdapter interface.
        /// </summary>
        /// <returns>An implementation of the IMessageQueueAdapter interface.</returns>
        IMessageQueueAdapter Create();
    }
}
