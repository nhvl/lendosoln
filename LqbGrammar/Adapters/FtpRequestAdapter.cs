﻿namespace LqbGrammar.Adapters
{
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers.FtpRequest;

    /// <summary>
    /// Interface used to issue requests over ftp.
    /// </summary>
    public interface IFtpRequestAdapter : ILqbAdapter
    {
        /// <summary>
        /// Carry out the communication with the indicated URL.
        /// </summary>
        /// <param name="url">The target URL.</param>
        /// <param name="options">The options that control the request and response parsing.</param>
        /// <returns>True if the communication was successful, false otherwise.</returns>
        bool ExecuteCommunication(LqbAbsoluteUri url, FtpRequestOptions options);
    }

    /// <summary>
    /// Factory for creating implementations of the IFtpRequestAdapter interface.
    /// </summary>
    public interface IFtpRequestAdapterFactory : ILqbFactory
    {
        /// <summary>
        /// Create an implementation of the IFtpRequestAdapter interface.
        /// </summary>
        /// <returns>An implementation of the IFtpRequestAdapter interface.</returns>
        IFtpRequestAdapter Create();
    }
}
