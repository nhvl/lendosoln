﻿namespace LqbGrammar.Adapters
{
    using System;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// A driver may want to monitor the activity of the SMS adapter.
    /// </summary>
    public interface ISmsMonitor
    {
        /// <summary>
        /// The actual message sent to the SMS provider, which may differ from the content of the SMS message.
        /// </summary>
        /// <param name="message">The full message passed to the SMS provider.</param>
        void BeforeSend(string message);

        /// <summary>
        /// The adapter only returns a boolean to flag success or failure of the SMS message.
        /// More detailed information can be reported to this interface.
        /// </summary>
        /// <param name="code">A success or error code can be passed here.</param>
        /// <param name="response">A response message can be passed here.</param>
        void AfterSend(string code, string response);

        /// <summary>
        /// If an exception is thrown by the SMS provider, this is called instead of AfterSend.
        /// </summary>
        /// <param name="ex">The exception thrown by the SMS provider.</param>
        void OnException(Exception ex);
    }

    /// <summary>
    /// Adapter for sending SMS messages.  Each message type will get its own method
    /// to make message content validation easier to manage.
    /// </summary>
    public interface ISmsAdapter : ILqbAdapter
    {
        /// <summary>
        /// Send an authentication code message via SMS.
        /// </summary>
        /// <param name="number">The phone number to which the SMS message will be sent.</param>
        /// <param name="message">A message containing an LQB authentication code.</param>
        /// <param name="monitor">Optional monitor that an adapter may use to get provider-specific details about the SMS communication.</param>
        /// <returns>True if OK response recieved, false otherwise.</returns>
        bool SendAuthenticationCode(PhoneNumber number, SmsAuthCodeMessage message, ISmsMonitor monitor);
    }

    /// <summary>
    /// Factory interface for creating implementations of the ISmsAdapter interface.
    /// </summary>
    public interface ISmsAdapterFactory : ILqbFactory
    {
        /// <summary>
        /// Create an implemenation of the ISmsAdapter interface.
        /// </summary>
        /// <param name="source">The phone number uses as the source of the SMS messages.</param>
        /// <param name="controlInformation">There may be additional information to configure a particular SMS provider.</param>
        /// <returns>An implementation of the ISmsAdapter interface.</returns>
        ISmsAdapter Create(PhoneNumber source, params string[] controlInformation);
    }
}
