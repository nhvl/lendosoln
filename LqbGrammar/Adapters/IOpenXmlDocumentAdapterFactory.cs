﻿namespace LqbGrammar.Adapters
{
    /// <summary>
    /// Factory for creating implementations of open XML document adapters.
    /// </summary>
    public interface IOpenXmlDocumentAdapterFactory : ILqbFactory
    {
        /// <summary>
        /// Create and return an implementation of the open XML document adapter.
        /// </summary>
        /// <returns>
        /// Implementation of the open XML document adapter.
        /// </returns>
        IOpenXmlDocumentAdapter Create();
    }
}
