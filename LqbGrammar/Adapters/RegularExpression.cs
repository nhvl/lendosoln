﻿namespace LqbGrammar.Adapters
{
    using DataTypes;

    /// <summary>
    /// Interface to a regular expression library.
    /// NOTE: we can add a search method later if/when needed.
    /// </summary>
    public interface IRegularExpressionAdapter : ILqbAdapter
    {
        /// <summary>
        /// Check a string against a regular expression.
        /// </summary>
        /// <param name="regex">The regular expression.</param>
        /// <param name="validationTarget">The string to be checked.</param>
        /// <returns>True if string matches the regular expression, false otherwise.</returns>
        bool IsMatch(RegularExpressionString regex, string validationTarget);

        /// <summary>
        /// In a specified input string, replaces all strings that match a specified regular expression with a specified replacement string.
        /// </summary>
        /// <param name="regex">The regular expression.</param>
        /// <param name="input">The string to search for a match.</param>
        /// <param name="replacement">The replacement string.</param>
        /// <returns>
        /// A new string that is identical to the input string, except that the replacement string takes the place of each matched string.
        /// If pattern is not matched in the current instance, the method returns the current instance unchanged.
        /// </returns>
        string Replace(RegularExpressionString regex, string input, string replacement);
    }

    /// <summary>
    /// Interface to a factory that will create implementations of IRegularExpressionAdapter.
    /// </summary>
    public interface IRegularExpressionAdapterFactory : ILqbFactory
    {
        /// <summary>
        /// Create an implementation of the interface IRegularExpressionAdapter.
        /// </summary>
        /// <returns>An implementation of the interface IRegularExpressionAdapter.</returns>
        IRegularExpressionAdapter Create();
    }
}
