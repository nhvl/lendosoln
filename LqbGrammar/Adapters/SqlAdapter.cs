﻿namespace LqbGrammar.Adapters
{
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using DataTypes;

    /// <summary>
    /// Defines the CRUD interface to SQL Server.
    /// </summary>
    public interface ISqlAdapter : ILqbAdapter
    {
        /// <summary>
        /// Insert a record into a table but do not retrieve the primary key.
        /// This is usually used for tables where the primary key is a set
        /// of columns rather than a generated key.
        /// </summary>
        /// <param name="connection">The connection to the DB.</param>
        /// <param name="transaction">If using a transaction, pass it in here, else pass in null.</param>
        /// <param name="query">The query string.</param>
        /// <param name="parameters">The query parameters.</param>
        void InsertNoKey(DbConnection connection, DbTransaction transaction, SQLQueryString query, IEnumerable<DbParameter> parameters);

        /// <summary>
        /// Insert a record into a table that has a uniqueidentifier-valued primary key.
        /// </summary>
        /// <param name="connection">The connection to the DB.</param>
        /// <param name="transaction">If using a transaction, pass it in here, else pass in null.</param>
        /// <param name="query">The query string.</param>
        /// <param name="parameters">The query parameters.</param>
        /// <returns>The primary key of the inserted record.</returns>
        PrimaryKeyGuid InsertGetGuid(DbConnection connection, DbTransaction transaction, SQLQueryString query, IEnumerable<DbParameter> parameters);

        /// <summary>
        /// Insert a record into a table that has an integer-valued primary key.
        /// </summary>
        /// <param name="connection">The connection to the DB.</param>
        /// <param name="transaction">If using a transaction, pass it in here, else pass in null.</param>
        /// <param name="query">The query string.</param>
        /// <param name="parameters">The query parameters.</param>
        /// <returns>The primary key of the inserted record.</returns>
        PrimaryKeyInt32 InsertGetInt32(DbConnection connection, DbTransaction transaction, SQLQueryString query, IEnumerable<DbParameter> parameters);

        /// <summary>
        /// Retrieve data from a SQL database.
        /// </summary>
        /// <param name="connection">The connection to the DB.</param>
        /// <param name="transaction">If using a transaction, pass it in here, else pass in null.</param>
        /// <param name="query">The query string.</param>
        /// <param name="parameters">The query parameters.</param>
        /// <returns>The retrieved data.</returns>
        DbDataReader Select(DbConnection connection, DbTransaction transaction, SQLQueryString query, IEnumerable<DbParameter> parameters);

        /// <summary>
        /// Retrieve data from a SQL database and populate the input data set.
        /// </summary>
        /// <param name="connection">The connection to the DB.</param>
        /// <param name="transaction">If using a transaction, pass it in here, else pass in null.</param>
        /// <param name="datasetToFill">The data set that will be filled.</param>
        /// <param name="query">The query string.</param>
        /// <param name="parameters">The query parameters.</param>
        void FillDataSet(DbConnection connection, DbTransaction transaction, DataSet datasetToFill, SQLQueryString query, IEnumerable<DbParameter> parameters);

        /// <summary>
        /// Retrieve data from a SQL database and populate the input data set using the pre-prepared adapter.
        /// </summary>
        /// <param name="adapter">The adapter used to fill the data set.</param>
        /// <param name="datasetToFill">The data set that will be filled.</param>
        /// <param name="tableName">Optional value that is the table name for updating.</param>
        void FillDataSet(DbDataAdapter adapter, DataSet datasetToFill, DBTableName? tableName);

        /// <summary>
        /// Update data to a SQL database and with changes in the data set using the pre-prepared adapter.
        /// </summary>
        /// <param name="adapter">The adapter used to update the data set.</param>
        /// <param name="withChanges">The data set that will be updated to the SQL database.</param>
        /// <param name="tableName">Optional value that is the table name for updating.</param>
        /// <returns>The number of rows that were modified.</returns>
        ModifiedRowCount UpdateDataSet(DbDataAdapter adapter, DataSet withChanges, DBTableName? tableName);

        /// <summary>
        /// Update data held in a SQL database.
        /// </summary>
        /// <param name="connection">The connection to the DB.</param>
        /// <param name="transaction">If using a transaction, pass it in here, else pass in null.</param>
        /// <param name="query">The query string.</param>
        /// <param name="parameters">The query parameters.</param>
        /// <returns>The number of rows that were modified.</returns>
        ModifiedRowCount Update(DbConnection connection, DbTransaction transaction, SQLQueryString query, IEnumerable<DbParameter> parameters);

        /// <summary>
        /// Delete data from a SQL database.
        /// </summary>
        /// <param name="connection">The connection to the DB.</param>
        /// <param name="transaction">If using a transaction, pass it in here, else pass in null.</param>
        /// <param name="query">The query string.</param>
        /// <param name="parameters">The query parameters.</param>
        /// <returns>The number of rows that were deleted.</returns>
        ModifiedRowCount Delete(DbConnection connection, DbTransaction transaction, SQLQueryString query, IEnumerable<DbParameter> parameters);
    }

    /// <summary>
    /// Factory for creating an implementation of ISqlAdapter.
    /// </summary>
    public interface ISqlAdapterFactory : ILqbFactory
    {
        /// <summary>
        /// Create and return an implementation of ISqlAdapter.
        /// </summary>
        /// <param name="factory">The database provider factory.</param>
        /// <param name="timeout">The timeout to use when communicating with a database.</param>
        /// <returns>An implementation of ISqlAdapter.</returns>
        ISqlAdapter Create(DbProviderFactory factory, TimeoutInSeconds timeout);
    }
}
