﻿namespace LqbGrammar
{
    /// <summary>
    /// Enumeration to mark an application's logical type so that code can behave differently in different scenarios.
    /// </summary>
    public enum ApplicationType
    {
        /// <summary>
        /// A normal application is one where code is intended to run as-is.
        /// </summary>
        Normal,

        /// <summary>
        /// A unit test application will handle location services differently.
        /// </summary>
        UnitTest
    }
}
