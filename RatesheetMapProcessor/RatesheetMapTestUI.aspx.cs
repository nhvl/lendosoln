
namespace RatesheetMap
{
    using System;
    using System.Collections;
    using System.ComponentModel;
    using System.Data;
    using System.Drawing;
    using System.IO;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Web;
    using System.Web.SessionState;
    using System.Web.UI;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;
    using RatesheetMap.Extensions;

    public partial class RatesheetMapTestUI : System.Web.UI.Page
	{
		#region Variables
		protected System.Web.UI.WebControls.TextBox m_ratesheetFilenameTB;



		public Boolean ShowTestingElements
		{
			get 
			{ 
				return true;
			}
		}
		#endregion

		protected void PageLoad(object sender, System.EventArgs e)
		{
			ClientScript.RegisterStartupScript(this.GetType(), "SetFocus", "<script>document.getElementById('" + m_ratesheetFilenameNew.ClientID + "').focus();</script>");
			
			if(!Page.IsPostBack)
			{
				HttpCookie cookie = HttpContext.Current.Request.Cookies.Get("balurdo");
				if(null != cookie && cookie.Value.Trim() != "")
				{
					m_SAEName.Text = cookie.Value.Trim();
				}
			}
		}

		private ArrayList GetMapLocations(string ratesheetLocation)
		{
			string ratesheetNameNoPath = System.IO.Path.GetFileName(ratesheetLocation);
			ArrayList filenames = new ArrayList();
			string rsFilenameNoExtension = System.IO.Path.GetFileNameWithoutExtension(ratesheetLocation);
			string mapFilename = rsFilenameNoExtension + "_MAP.XLS";
			string combined = System.IO.Path.Combine(RatesheetMapProcessor.RS_MAP_TEST_FILES_PATH, mapFilename);
			if(!File.Exists(combined))
			{
				try
				{
					filenames = GetMapFilenamesFromRef(combined);
				}
				catch(Exception)
				{
					throw;
				}
				if(filenames.Count == 0)
					throw new Exception(String.Format("Cannot compress map - Unable to find a map file or reference file for ratesheet '{0}'. Looking for '{1}' or .REF", ratesheetNameNoPath, combined));
			}
			else
			{
				filenames.Add(combined);
			}
			return filenames;
		}

		private ArrayList GetMapFilenamesFromRef(string fullPath)
		{
			string mapPath = "";
			string line = null;
			string refFilename = "";
			try
			{
				ArrayList mapFilenamesFromRef = new ArrayList();
				refFilename = System.IO.Path.ChangeExtension(fullPath, ".ref");

				if(!File.Exists(refFilename))
					return mapFilenamesFromRef;

				mapPath = System.IO.Path.GetDirectoryName(fullPath);
				StreamReader reader = new StreamReader(refFilename);
				while ((line = reader.ReadLine()) != null) 
				{
					if(line.Trim() != "")
					{
						string refEntry = System.IO.Path.GetFileNameWithoutExtension(line.Trim()) + ".XLS";
						string mapFilename = System.IO.Path.Combine(mapPath, refEntry);
						if(File.Exists(mapFilename))
							mapFilenamesFromRef.Add(mapFilename);
						else
							throw new Exception(String.Format("Map filename '{0}' found in reference file '{1}' does not exist.", mapFilename, refFilename));
					}
				}
				return mapFilenamesFromRef;
			}
			catch(Exception e)
			{
				if(line == null)
					line = "";
				throw new Exception(String.Format("Unable to get map filename(s) from reference file '{0}' - {1}  Map path = '{2}', map filename = '{3}'.", refFilename, e.Message, mapPath, line.Trim()), e);
			}
		}

		protected void OnTestParseStrClick(object sender, System.EventArgs e)
		{
            m_parseStrResult.InnerText = ApplyParseStringMultiple(m_parseStr.Text.ToLower(), m_inputVal.Text.ToLower(), m_wildcard.Text.ToLower()).ConvertPercent();
		}

		protected void OnTestParseDateTimeClick(object sender, System.EventArgs e)
		{
			try
			{
				DateTime.Parse(m_dateTimeTest.Text);
				m_parseStrResult.InnerText = "Success - is valid DateTime value";
			}
			catch
			{
				m_parseStrResult.InnerText = "Fail - is NOT valid DateTime value";
			}
		}

		protected void OnTestIsMatchClick(object sender, System.EventArgs e)
		{
			string regex = RatesheetMapCommon.WildcardToRegexFullString(m_parseStr.Text.ToLower(), m_wildcard.Text.ToLower());
			Match match = Regex.Match(m_inputVal.Text.ToLower(), regex);
			m_parseStrResult.InnerText = match.Success ? "Matched!" : "Did not match";
		}

		private string ApplyParseStringMultiple(string parseStr, string inputValue, string wildcard)
		{
			string finalVal = inputValue;
			string replace = "%f"; //default
			int index = parseStr.IndexOf("%f");
			if(index < 0)
			{
				index = parseStr.IndexOf("%s");
				replace = "%s";
			}

			if(index < 0)
				return "No valid delimiters found (%f or %s)";

			String[] segments = Regex.Split(parseStr, replace);
			int segIndex = -1;
			foreach(string segment in segments)
			{
				Match match = Regex.Match(finalVal, RatesheetMapCommon.WildcardToRegex(segment, wildcard));
				if(!match.Success)
				{
					return String.Format("Unable to match all parts of ParseStr value '{0}'.  The part that cannot be found is '{1}'.", parseStr, segment);
				}
				else
				{
					string matchedString = match.Groups[0].ToString();
					segIndex = finalVal.IndexOf(matchedString);
					int segLength = matchedString.Length;
					finalVal = finalVal.Remove(segIndex, segLength);
				}
			}

            if (replace.Equals("%f"))
			{
				try
				{
					if(finalVal.Equals(""))
						finalVal = "0.000";
					else
						finalVal = double.Parse(finalVal.Trim()).ToString();
				}
				catch(Exception e)
				{
					return String.Format("Unable to parse floating point value using ParseStr attribute '{0}'.  DETAILS: {1}.", parseStr, e.ToString());
				}
			}

			return finalVal;
		}	

		protected void OnRunCompressFileClick(object sender, System.EventArgs e)
		{
			try
			{
				string fileName = m_mapFilenameTB.Text.Trim();
				string compactedFilename = "";
				
				if(fileName != "")
				{
					string sFilePath = System.IO.Path.Combine(RatesheetMapProcessor.RS_MAP_TEST_FILES_PATH, fileName);
					compactedFilename = InputFileRep.ConvertToCPX(sFilePath);
					m_Result.Text = "Successfully created " + compactedFilename;
				}
			}
			catch(Exception exc)
			{
				m_Result.Text = exc.Message;
				LogHelper.LogError(exc);
			}
		}

		private void CompressMapFile(string fileName)
		{
			try
			{
				string compactedFilename = "";
				
				if(fileName != "")
				{
					string sFilePath = System.IO.Path.Combine(RatesheetMapProcessor.RS_MAP_TEST_FILES_PATH, fileName);
					compactedFilename = InputFileRep.ConvertToCPX(sFilePath);
					//m_Result.Text = "Successfully created " + compactedFilename;
				}
			}
			catch(Exception exc)
			{
				m_Result.Text = exc.Message;
				LogHelper.LogError(exc);
				return;
			}
		}

		
		protected void OnRunRSProcessorUpdated2Click(object sender, System.EventArgs e)
		{
			try
			{
				if(m_SAEName.Text.Trim().Equals(""))
					m_SAEName.Text = "The unnamed entity that is too ashamed of his map to admit ownership.";
				
				HttpCookie cookie = new HttpCookie("balurdo", m_SAEName.Text);
				cookie.HttpOnly = true; // 3/5/2009 dd - OPM 27896 - Protected all cookies with HttpOnly
				HttpContext.Current.Response.Cookies.Add(cookie);
				
				string fileName = m_ratesheetFilenameNew.Text.Trim();
				if(fileName != "")
				{
					string sFilePath = System.IO.Path.Combine(RatesheetMapProcessor.FILES_PATH, fileName);
					if(m_compressMapFirst.Checked == true)
					{
						ArrayList mapLocations = GetMapLocations(sFilePath);
						foreach(string filename in mapLocations)
						{
							CompressMapFile(filename);
						} 
					}
					//TimerHelper t = new TimerHelper();
					//t.start();
					RatesheetMapProcessor processor = new RatesheetMapProcessor(sFilePath, true /*run standalone mode*/, m_SAEName.Text, (m_displayRegionTracingInfo.SelectedItem.Value == "1"), (m_SAEName.Text.ToLower().Equals("diana") /*IsDevTester*/));
					//t.stop();
					
					m_Result.Text = processor.ToString();
				}
			}
			catch(Exception a)
			{
				m_Result.Text = a.Message;
				LogHelper.LogError(a);
			}
		}
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);

		}
		#endregion
	}
}
