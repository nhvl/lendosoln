<%@ Page language="c#" Codebehind="RatesheetMapTestUI.aspx.cs" AutoEventWireup="false" Inherits="RatesheetMap.RatesheetMapTestUI" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
    <title>RatesheetMapTestUI</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
  </HEAD>
  <body MS_POSITIONING="GridLayout">
    <form id="RatesheetMapTestUI" method="post" runat="server">
    If you have made changes to your map, please compress it first.
    <br>
    <br> 
    <table>
    <tr style="DISPLAY:none">
		<td>
			Map Name: 
		</td>
		<td >
			<asp:TextBox ID="m_mapFilenameTB" Runat=server Width=300px></asp:TextBox>&nbsp;&nbsp;
		</td>
		<td>
			<asp:Button ID="m_RunCompressMap" OnClick="OnRunCompressFileClick" Runat=server Text="Compress Map File"></asp:Button>
		</td>
    </tr>
    <tr style="DISPLAY:none">
    <td colspan=4>
    
    Note: Putting your name helps me to quickly contact the person running the map so I can
    <br>explain things to you.
	<br>Putting a fake name only means you will waste more time figuring it out 
	<br>(Shaka Zulu and Dr. Worm, this means YOU!)
    </td>
    </tr>
    <tr>
		<td>
			SAE Name: 
		</td>
		<td colspan=2>
			<asp:TextBox ID="m_SAEName" Runat=server Width=430px></asp:TextBox>
		</td>
    </tr>
    <tr>
		<td>
			Compress map first?
		</td>
		<td>
			<asp:CheckBox ID="m_compressMapFirst" Runat=server Width=300px></asp:CheckBox>&nbsp;&nbsp;
		</td>
    </tr>
    <tr>
		<td>
			Display code debugging info? 
		</td>
		<td >
			<asp:RadioButtonList ID="m_displayRegionTracingInfo" Runat=server RepeatDirection="Horizontal">
				<asp:ListItem Value="0" Selected=True>No</asp:ListItem>
				<asp:ListItem Value="1" >Yes</asp:ListItem>
			</asp:RadioButtonList>
			
		</td>
    </tr>
    <tr>
		<td>
			Ratesheet Name: 
		</td>
		<td >
			<asp:TextBox ID="m_ratesheetFilenameNew" Runat=server Width=300px></asp:TextBox>&nbsp;&nbsp;<asp:Button ID="Button1" OnClick="OnRunRSProcessorUpdated2Click" Runat=server Text="Run Processor"></asp:Button><br>
		</td>
		<td style="DISPLAY:none">
			<asp:Button ID="Button2" OnClick="OnRunRSProcessorUpdated2Click" Runat=server Text="Test Me!"></asp:Button><br>
		</td>
    </tr>
    <% if( ShowTestingElements ) { %>
    <tr>
		<td colspan = 5>
		<br><br><br><b>ParseStr Tester:</b>
			<table>
				<tr>
					<td>
						ParseStr = &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:TextBox ID="m_parseStr" Runat=server Width=200px></asp:TextBox>&nbsp;&nbsp;
					</td>
				</tr>
				<tr>
					<td>
						Input Value = &nbsp;&nbsp;<asp:TextBox ID="m_inputVal" Runat=server Width=200px></asp:TextBox>&nbsp;&nbsp;
					</td>
				</tr>
				<tr>
					<td>
						Wildcard = &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:TextBox ID="m_wildcard" Runat=server Width=200px></asp:TextBox>&nbsp;&nbsp; (leave blank if not using a wildcard)
					</td>
				</tr>
				<tr>
					<td>
						<asp:Button ID="m_TestParseString" OnClick="OnTestParseStrClick" Runat=server Text="ParseStr result"></asp:Button> &nbsp;&nbsp;<asp:Button ID="m_TestMatch" OnClick="OnTestIsMatchClick" Runat=server Text="Is Match?"></asp:Button><br>
					</td>
					
				</tr>
				 <tr>
					<td colspan = 5>
						<br><b>DateTime Tester:</b>
					</td>
				</tr>
				<tr>
					<td>
						Input Value = &nbsp;&nbsp;<asp:TextBox ID="m_dateTimeTest" Runat=server Width=200px></asp:TextBox>&nbsp;
						<asp:Button ID="m_parseDateTime" OnClick="OnTestParseDateTimeClick" Runat=server Text="Is Valid DateTime?"></asp:Button> <br>
					</td>
				</tr>
				<tr>
					<td colspan=5>
					<br>
						<b>Result =</b>  &nbsp;&nbsp; <span id="m_parseStrResult" runat="server" style="width: 600px"></span>
					</td>
				</tr>
			</table>
		</td>
    </tr>
   
    <% } %>
	</table>
	<br>
	<ml:PassthroughLiteral id=m_Result Runat=server></ml:PassthroughLiteral>
     </form>
  </body>
</HTML>
