﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Runtime.Remoting.Channels;
using PdfRasterizerLib;

namespace PdfRasterizerService
{
    public partial class PdfRasterizerService : ServiceBase
    {
        IChannel m_channel = null;
        public PdfRasterizerService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            m_channel = PdfRasterizerFactory.RegisterService();
        }

        protected override void OnStop()
        {
            if (null != m_channel)
            {
                ChannelServices.UnregisterChannel(m_channel);
            }
            m_channel = null;
        }
    }
}
