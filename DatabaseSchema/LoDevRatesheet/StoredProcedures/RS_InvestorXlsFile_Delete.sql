
CREATE PROCEDURE [dbo].[RS_InvestorXlsFile_Delete]
	@InvestorXlsFileId as bigint 
AS
BEGIN
	DELETE FROM INVESTOR_XLS_FILE
	WHERE InvestorXlsFileId = @InvestorXlsFileId     
END
