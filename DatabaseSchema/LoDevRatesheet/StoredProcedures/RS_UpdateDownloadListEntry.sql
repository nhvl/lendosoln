
ALTER PROCEDURE [dbo].[RS_UpdateDownloadListEntry]
	@ID as int,
	@Description as varchar(250),
	@URL as varchar(250),
	@FileName as varchar(500),
	@Active as char(1),
	@Conversion as varchar(50) = NULL,
	@BotType as varchar(50) = NULL,
	@Login as varchar(50) = NULL,
	@Password as varchar(50) = NULL,
	@LastRatesheetTimestamp as datetime = NULL,
	@Token as varchar(200) = NULL,
	@Note as varchar(1000) = NULL
AS
BEGIN
	SET NOCOUNT ON;

	UPDATE DOWNLOAD_LIST
	SET
		Description = @Description,
		URL = @URL,
		FileName = @FileName,
		Active = @Active,
		Conversion = @Conversion,
		BotType = @BotType,
		Login = @Login,
		Password = @Password,
		LastRatesheetTimestamp = @LastRatesheetTimestamp,
		Token = @Token,
		Note = @Note
WHERE ID = @ID
END
