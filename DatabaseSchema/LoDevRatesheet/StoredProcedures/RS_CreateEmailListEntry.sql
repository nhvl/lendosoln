ALTER PROCEDURE [dbo].[RS_CreateEmailListEntry]
    @Sender as varchar(100) = NULL,
    @Body as varchar(100) = NULL,
    @MailFileName as varchar(50) = NULL,
    @TargetFileName as varchar(max) = NULL,
    @Active as char(1),
    @BotType as varchar(50) = NULL,
    @Subject  as varchar(100) = NULL,
    @Subject1 as varchar(200) = NULL,
    @Subject2 as varchar(200) = NULL,
    @Subject3 as varchar(200) = NULL,
    @Subject4 as varchar(200) = NULL,
    @Subject5 as varchar(200) = NULL,
    @Subject6 as varchar(200) = NULL,
    @Subject7 as varchar(200) = NULL,
    @Subject8 as varchar(200) = NULL,
    @Subject9 as varchar(200) = NULL,
    @Description as varchar(250) = '',
    @Note as varchar(1000) = ''
AS
BEGIN
    SET NOCOUNT ON;

    INSERT INTO EMAIL_DOWNLOAD_LIST (Sender, Body, MailFileName, TargetFileName, Active, BotType,
        Subject,
        Subject1,
        Subject2,
        Subject3,
        Subject4,
        Subject5,
        Subject6,
        Subject7,
        Subject8,
        Subject9,
        Description,
        Note

    ) VALUES(@Sender, @Body, @MailFileName, @TargetFileName, @Active, @BotType,
        @Subject,
        @Subject1,
        @Subject2,
        @Subject3,
        @Subject4,
        @Subject5,
        @Subject6,
        @Subject7,
        @Subject8,
        @Subject9,
        @Description,
        @Note
    )
END
