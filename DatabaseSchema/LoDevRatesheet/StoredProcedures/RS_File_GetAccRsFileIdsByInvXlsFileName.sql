

CREATE PROCEDURE [dbo].[RS_File_GetAccRsFileIdsByInvXlsFileName]
	@InvestorXlsFileName varchar(100)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

		SELECT LpeAcceptableRsFileId 
		FROM  LPE_ACCEPTABLE_RS_FILE where InvestorXlsFileId in 
		( 
			SELECT InvestorXlsFileId 
			FROM  INVESTOR_XLS_FILE 
			WHERE InvestorXlsFileName=@InvestorXlsFileName
		)

END
