CREATE PROCEDURE [dbo].[RS_FileVersion_Update]
	@LpeAcceptableRsFileId		varchar(100),	
    @VersionNumber				bigint,
    @FirstEffectiveDateTime     DateTime,
    @LatestEffectiveDateTime    DateTime,
    @IsExpirationIssuedByInvestor bit
AS
BEGIN
	UPDATE LPE_ACCEPTABLE_RS_FILE_VERSION
         set FirstEffectiveDateTime = @FirstEffectiveDateTime, 
             LatestEffectiveDateTime = @LatestEffectiveDateTime,
             IsExpirationIssuedByInvestor = @IsExpirationIssuedByInvestor

		WHERE LpeAcceptableRsFileId = @LpeAcceptableRsFileId and
              VersionNumber = @VersionNumber
END

