-- =============================================
-- Author:		Scott Kibler
-- Description:
	-- Gets the name of the catalog for the table.
-- =============================================
CREATE PROCEDURE [dbo].[Justin_Testing_GetTableCatalogByTableName]
	-- Add the parameters for the stored procedure here
	@TableName varchar(200)	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT table_catalog
	FROM INFORMATION_SCHEMA.tables 
	where table_name = @TableName
	and table_schema = 'dbo'
END
