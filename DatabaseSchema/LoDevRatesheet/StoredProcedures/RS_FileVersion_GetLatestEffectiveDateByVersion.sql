



CREATE PROCEDURE [dbo].[RS_FileVersion_GetLatestEffectiveDateByVersion]
	@LpeAcceptableRsFileId as varchar(100),
	@VersionNumber as bigint
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT LatestEffectiveDateTime 
		FROM LPE_ACCEPTABLE_RS_FILE_VERSION
		WHERE LpeAcceptableRsFileId = @LpeAcceptableRsFileId
        AND VersionNumber = @VersionNumber

END



