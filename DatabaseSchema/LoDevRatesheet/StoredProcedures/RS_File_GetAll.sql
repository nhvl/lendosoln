



CREATE PROCEDURE [dbo].[RS_File_GetAll]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * 
		FROM LPE_ACCEPTABLE_RS_FILE
		ORDER BY LpeAcceptableRsFileId
END



