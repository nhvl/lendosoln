-- =============================================
-- Author:		Justin Jia
-- ALTER date: June 14, 2012
-- Description:	Gets all column information by the table name, such as column_size, is_nullable.
-- =============================================
CREATE PROCEDURE [dbo].[Justin_Testing_GetCheckConstraintByPattern]
	-- Add the parameters for the stored procedure here
	@LikePattern varchar(200) = '%',
	@NotLikePattern varchar(200) = '%'
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT OBJECT_NAME(sco.object_id) AS tableName, sco.name as columnName, sco.is_nullable, sch.[definition], * 
	FROM SYS.COLUMNS as sco
	inner join SYS.check_constraints as sch on sch.parent_object_id = sco.object_id
	and sch.name = 'chk_' + sco.name
	where sch.[definition] not like @LikePattern
	and sch.[definition] like @NotLikePattern
END
