ALTER PROCEDURE [dbo].[RS_ListInvestorXlsFileWithFileVersionDateLaterThan]
	@FileVersionDateTime datetime
AS
BEGIN

SELECT InvestorXlsFileId, InvestorXlsFileName, FileVersionDateTime 
FROM investor_xls_file
WHERE FileVersionDateTime < @FileVersionDateTime
AND InvestorXlsFileName NOT IN ('AUTOTEST_INVESTOR_RATE_SHEET',
'AUTOTEST_INVESTOR_RATE_SHEET_MANUAL_NEWVERSION',
'AUTOTEST_INVESTOR_RATE_SHEET_MANUAL_OLDVERSION',
'BILLYJOEBOBRS.XLS')
AND ExcludeFromInvPricingWarnings = 0
ORDER BY fileversiondatetime desc

END
