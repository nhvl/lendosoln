
-- =============================================
-- Author:		Eric Mallare
-- Create date: 3/28/2017
-- Description:	Gets the acceptable RS files' info.
-- =============================================
CREATE PROCEDURE [dbo].[GetAllRsFilesById]
	@RsFileIdsXml xml = null
AS
BEGIN
	IF @RsFileIdsXml IS NULL
	BEGIN
		SELECT 
			larf.LpeAcceptableRsFileId, larf.DeploymentType, larf.IsBothRsMapAndBotWorking, larf.InvestorXlsFileId
		FROM 
			LPE_ACCEPTABLE_RS_FILE larf
	END
	ELSE
	BEGIN
		SELECT 
			larf.LpeAcceptableRsFileId, larf.DeploymentType, larf.IsBothRsMapAndBotWorking, larf.InvestorXlsFileId
		FROM 
			LPE_ACCEPTABLE_RS_FILE larf JOIN
			@RsFileIdsXml.nodes('root/file/@fileId') as T(Item) ON larf.LpeAcceptableRsFileId=T.Item.value('.', 'varchar(100)') 
	END
END

