ALTER PROCEDURE dbo.INVESTOR_XLS_FILE_ListModifiedSince 
    @ModifiedD datetime
AS
BEGIN

    -- List all records modified after the given date.
    SELECT InvestorXlsFileId, InvestorXlsFileName, EffectiveDateTimeWorksheetName, EffectiveDateTimeLandMarkText, EffectiveDateTimeRowOffset, EffectiveDateTimeColumnOffset, FileVersionDateTime, ModifiedD
    FROM INVESTOR_XLS_FILE
    WHERE ModifiedD > @ModifiedD

END
GO
