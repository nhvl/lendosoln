ALTER PROCEDURE [dbo].[LPE_ACCEPTABLE_RS_FILE_VERSION_SyncRecord]
	@LpeAcceptableRsFileId varchar(100),
	@VersionNumber bigint,
	@FirstEffectiveDateTime datetime,
	@LatestEffectiveDateTime datetime,
	@IsExpirationIssuedByInvestor bit,
	@EntryCreatedD datetime

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	UPDATE LPE_Acceptable_RS_File_Version
	SET FirstEffectiveDateTime=@FirstEffectiveDateTime,
	LatestEffectiveDateTime = @LatestEffectiveDateTime,
	IsExpirationIssuedByInvestor = @IsExpirationIssuedByInvestor,
	EntryCreatedD = @EntryCreatedD
	WHERE LpeAcceptableRsFileId=@LpeAcceptableRsFileId AND VersionNumber=@VersionNumber

	IF @@ROWCOUNT = 0
	BEGIN
	    SET IDENTITY_INSERT [dbo].LPE_Acceptable_RS_File_Version ON

	    INSERT INTO LPE_Acceptable_RS_File_Version (LpeAcceptableRsFileId,VersionNumber,FirstEffectiveDateTime,LatestEffectiveDateTime,IsExpirationIssuedByInvestor,EntryCreatedD,ModifiedD)
	    VALUES (@LpeAcceptableRsFileId,@VersionNumber,@FirstEffectiveDateTime,@LatestEffectiveDateTime,@IsExpirationIssuedByInvestor,@EntryCreatedD,GETDATE())

		SET IDENTITY_INSERT [dbo].LPE_Acceptable_RS_File_Version OFF

	END

END