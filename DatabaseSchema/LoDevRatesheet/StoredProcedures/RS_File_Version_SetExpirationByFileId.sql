CREATE PROCEDURE dbo.RS_File_Version_SetExpirationByFileId
	@IsExpirationIssuedByInvestor bit,
	@LpeAcceptableRsFileId varchar(100)
AS

	-- Update the expiration for latest file version.
	UPDATE LPE_ACCEPTABLE_RS_FILE_VERSION
	   SET IsExpirationIssuedByInvestor = @IsExpirationIssuedByInvestor
	WHERE LpeAcceptableRsFileId = @LpeAcceptableRsFileId
	  AND VersionNumber IN (SELECT MAX(VersionNumber) FROM LPE_ACCEPTABLE_RS_FILE_VERSION WHERE LpeAcceptableRsFileId=@LpeAcceptableRsFileId)