ALTER PROCEDURE [dbo].[RS_RetrieveOutputFileList]  
	@Enabled char(1) = NULL,
	@Dirty char(1) = NULL,
	@FileName varchar(50) = NULL
AS
	SELECT 
		OUF.* 
	FROM 
		OUTPUT_FILE ouf
	WHERE
		((@Enabled IS NULL)		OR (ouf.Enabled = @Enabled))
	AND
		((@Dirty IS NULL)		OR (ouf.Dirty = @Dirty))
	AND
		((@FileName IS NULL)	OR (ouf.FileName LIKE '' + @FileName + '%'))




