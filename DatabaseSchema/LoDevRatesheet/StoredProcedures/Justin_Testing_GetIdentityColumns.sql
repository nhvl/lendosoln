-- =============================================
-- Author:		Scott Kibler
-- Description:	Gets the tablenames and columns that are identity columns.
-- =============================================
CREATE PROCEDURE [dbo].[Justin_Testing_GetIdentityColumns]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT DISTINCT st.name as table_name, sc.name as column_name
	FROM 
			sys.tables st
		INNER JOIN 
			sys.columns as sc 
		ON sc.object_id = st.object_id
	WHERE 
		sc.is_identity = 1
END
