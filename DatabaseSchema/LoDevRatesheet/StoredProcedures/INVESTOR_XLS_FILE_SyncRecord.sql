ALTER PROCEDURE [dbo].[INVESTOR_XLS_FILE_SyncRecord]
	@InvestorXlsFileId bigint,
	@InvestorXlsFileName varchar(100),
	@EffectiveDateTimeWorksheetName varchar(100),
	@EffectiveDateTimeLandMarkText varchar(100),
	@EffectiveDateTimeRowOffset int,
	@EffectiveDateTimeColumnOffset int,
	@FileVersionDateTime datetime

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	UPDATE INVESTOR_XLS_FILE
	SET InvestorXlsFileName=@InvestorXlsFileName,
        EffectiveDateTimeWorksheetName = @EffectiveDateTimeWorksheetName,
        EffectiveDateTimeLandMarkText = @EffectiveDateTimeLandMarkText,
        EffectiveDateTimeRowOffset = @EffectiveDateTimeRowOffset,
        EffectiveDateTimeColumnOffset = @EffectiveDateTimeColumnOffset,
        FileVersionDateTime = @FileVersionDateTime
	WHERE InvestorXlsFileId=@InvestorXlsFileId

	IF @@ROWCOUNT = 0
	BEGIN
	    SET IDENTITY_INSERT [dbo].INVESTOR_XLS_FILE ON

	    INSERT INTO INVESTOR_XLS_FILE (InvestorXlsFileId,InvestorXlsFileName,EffectiveDateTimeWorksheetName,EffectiveDateTimeLandMarkText,EffectiveDateTimeRowOffset,EffectiveDateTimeColumnOffset,FileVersionDateTime,ModifiedD)
	    VALUES (@InvestorXlsFileId,@InvestorXlsFileName,@EffectiveDateTimeWorksheetName,@EffectiveDateTimeLandMarkText,@EffectiveDateTimeRowOffset,@EffectiveDateTimeColumnOffset,@FileVersionDateTime,GETDATE())

		SET IDENTITY_INSERT [dbo].INVESTOR_XLS_FILE OFF

	END

END