


CREATE PROCEDURE [dbo].[RS_File_GetByRatesheetId]
	@InvestorXlsFileId as bigint
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * 
		FROM LPE_ACCEPTABLE_RS_FILE
		WHERE InvestorXlsFileId = @InvestorXlsFileId
END


