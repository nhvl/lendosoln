-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE RS_UpdateDownloadListRatesheetTimestamp
	@LastRatesheetTimestamp DateTime,
	@Id Int
AS
BEGIN
	UPDATE DOWNLOAD_LIST
	SET LastRatesheetTimestamp = @LastRatesheetTimestamp
	WHERE ID = @Id
END
