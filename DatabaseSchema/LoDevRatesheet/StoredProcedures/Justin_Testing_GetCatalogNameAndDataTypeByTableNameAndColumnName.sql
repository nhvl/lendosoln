-- =============================================
-- Author:		Scott Kibler
-- Description:		
-- Gets the column catalog and data type by the table name and column name.
-- =============================================
CREATE PROCEDURE [dbo].[Justin_Testing_GetCatalogNameAndDataTypeByTableNameAndColumnName]
	@TableName varchar(200),
	@ColumnName varchar(200)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
    
	SELECT 
			isk.table_catalog, 
			isk.data_type
	FROM 
			INFORMATION_SCHEMA.columns as isk
	WHERE 
			isk.table_name = @TableName 
		AND isk.column_name like @ColumnName
		AND isk.table_schema = 'dbo'
END
