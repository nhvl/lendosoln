



CREATE PROCEDURE [dbo].[RS_RetrieveDownloadListEntryByID]
	@DownloadListEntryId as bigint  
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT *
		FROM DOWNLOAD_LIST
		WHERE ID = @DownloadListEntryId

END


