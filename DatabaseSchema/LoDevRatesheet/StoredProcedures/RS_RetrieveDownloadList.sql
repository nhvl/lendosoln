ALTER PROCEDURE [dbo].[RS_RetrieveDownloadList]
	@Active char(1) = NULL,
	@Login varchar(50) = NULL,
	@Description varchar(250) = NULL,
	@FileName varchar(500) = NULL	
AS
	SELECT 
		dl.* 
	FROM
		DOWNLOAD_LIST dl
	WHERE
		((@Active IS NULL) OR (dl.Active = @Active))
	AND
		((@Login IS NULL) OR (dl.Login LIKE '' + @Login + '%'))
	AND
		((@Description IS NULL) OR (dl.Description LIKE '' + @Description + '%'))
	AND
		((@FileName IS NULL) OR (dl.FileName LIKE '' + @FileName + '%'))