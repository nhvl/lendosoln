
CREATE PROCEDURE [dbo].[RS_FileVersion_Create]
	@LpeAcceptableRsFileId		varchar(100),	
    @FirstEffectiveDateTime     DateTime,
    @LatestEffectiveDateTime    DateTime,
    @IsExpirationIssuedByInvestor bit,
	@VersionNumber bigint out

    
AS
BEGIN
	INSERT INTO LPE_ACCEPTABLE_RS_FILE_VERSION
         (LpeAcceptableRsFileId,
          FirstEffectiveDateTime, LatestEffectiveDateTime,
          IsExpirationIssuedByInvestor )
    VALUES (@LpeAcceptableRsFileId,
          @FirstEffectiveDateTime, @LatestEffectiveDateTime,
          @IsExpirationIssuedByInvestor )		  
    SET @VersionNumber = SCOPE_IDENTITY()
END
