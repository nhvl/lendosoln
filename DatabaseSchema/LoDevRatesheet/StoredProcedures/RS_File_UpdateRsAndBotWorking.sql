

CREATE PROCEDURE [dbo].[RS_File_UpdateRsAndBotWorking]
@InvestorXlsFileId			bigint,
@IsBothRsMapAndBotWorking   bit

AS
BEGIN
		UPDATE LPE_ACCEPTABLE_RS_FILE
        set IsBothRsMapAndBotWorking = @IsBothRsMapAndBotWorking 
		WHERE InvestorXlsFileId = @InvestorXlsFileId
END