-- =============================================
-- Author:		Justin Jia
-- CREATE date: June 14, 2012
-- Description:	Gets all foreign keys and reference keys and corresponding tables.
-- =============================================
CREATE PROCEDURE [dbo].[Justin_Testing_GetAllForeignReferenceKeyAndTables]
	-- Add the parameters for the stored procedure here
	@IsIdentity varchar(50) = '%'
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT distinct
	OBJECT_NAME(c1.object_id) FK_TABLE_NAME,
	c1.name FK_COLUMN_NAME,
	c2.is_identity,
	OBJECT_NAME(c2.object_id) REFERENCED_TABLE_NAME,
	c2.name REFERENCED_COLUMN_NAME
	FROM  sys.foreign_key_columns fkc 
	INNER JOIN  sys.columns c1 ON fkc.parent_column_id = c1.column_id AND fkc.parent_object_id = c1.object_id
	INNER JOIN  sys.columns c2 ON fkc.referenced_column_id = c2.column_id AND fkc.referenced_object_id = c2.object_id
	where c2.is_identity like @isIdentity
END
