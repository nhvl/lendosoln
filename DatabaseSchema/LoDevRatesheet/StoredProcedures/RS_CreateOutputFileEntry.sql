






CREATE PROCEDURE [dbo].[RS_CreateOutputFileEntry]
	@FileName as varchar(50),
	@ExportType as char(1),
	@Worksheet as varchar(50),
	@Enabled as char(1),
	@Dirty as char(1),
	@UpdateMonitorEmails as varchar(1000) = NULL
AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO OUTPUT_FILE (FileName, ExportType, Worksheet, Enabled, Dirty, UpdateMonitorEmails, ErrorCount)
	VALUES(@FileName, @ExportType, @Worksheet, @Enabled, @Dirty, @UpdateMonitorEmails, 0)

END





