





CREATE PROCEDURE [dbo].[RS_UpdateOutputFileEntry]
	@ID as int,	
	@FileName as varchar(50),
	@ExportType as char(1),
	@Worksheet as varchar(50),
	@Enabled as char(1),
	@Dirty as char(1),
	@UpdateMonitorEmails as varchar(1000) = NULL
AS
BEGIN
	SET NOCOUNT ON;

	UPDATE OUTPUT_FILE 
	SET
		FileName = @FileName,
		ExportType = @ExportType,
		Worksheet = @Worksheet,
		Enabled = @Enabled,
		Dirty = @Dirty,
		UpdateMonitorEmails = @UpdateMonitorEmails
WHERE ID = @ID
END




