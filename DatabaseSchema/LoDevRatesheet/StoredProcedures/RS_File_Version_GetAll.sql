CREATE PROCEDURE [dbo].[RS_File_Version_GetAll]
@LpeAcceptableRsFileId as varchar(100)
AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT TOP 50 * 
		FROM LPE_ACCEPTABLE_RS_FILE_VERSION
		WHERE LpeAcceptableRsFileId = @LpeAcceptableRsFileId
		ORDER BY VersionNumber DESC
END



