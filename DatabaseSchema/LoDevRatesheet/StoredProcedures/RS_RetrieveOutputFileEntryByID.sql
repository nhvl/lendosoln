




CREATE PROCEDURE [dbo].[RS_RetrieveOutputFileEntryByID]
	@Id as bigint  
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT *
		FROM OUTPUT_FILE
		WHERE ID = @Id

END



