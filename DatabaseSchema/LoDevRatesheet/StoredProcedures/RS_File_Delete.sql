
CREATE PROCEDURE [dbo].[RS_File_Delete]
	@LpeAcceptableRsFileId as varchar(1000)
AS
BEGIN

	DELETE FROM LPE_ACCEPTABLE_RS_FILE
		WHERE LpeAcceptableRsFileId = @LpeAcceptableRsFileId
END
