




CREATE PROCEDURE [dbo].[RS_RetrieveEmailDownloadListEntryByID]
	@Id as bigint  
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT *
		FROM EMAIL_DOWNLOAD_LIST
		WHERE ID = @Id

END



