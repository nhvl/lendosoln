ALTER PROCEDURE [dbo].[LPE_ACCEPTABLE_RS_FILE_SyncRecord]
	@LpeAcceptableRsFileId varchar(100),
	@DeploymentType varchar(100),
	@IsBothRsMapAndBotWorking bit,
	@InvestorXlsFileId bigint

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	UPDATE LPE_ACCEPTABLE_RS_FILE
	SET DeploymentType = @DeploymentType,
	    IsBothRsMapAndBotWorking = @IsBothRsMapAndBotWorking,
	    InvestorXlsFileId = @InvestorXlsFileId
	WHERE LpeAcceptableRsFileId=@LpeAcceptableRsFileId

	IF @@ROWCOUNT = 0
	BEGIN

	    INSERT INTO LPE_ACCEPTABLE_RS_FILE (LpeAcceptableRsFileId,DeploymentType,IsBothRsMapAndBotWorking,InvestorXlsFileId, ModifiedD)
	    VALUES (@LpeAcceptableRsFileId,@DeploymentType,@IsBothRsMapAndBotWorking,@InvestorXlsFileId,GETDATE())

	END

END