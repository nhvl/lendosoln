

CREATE PROCEDURE [dbo].[Investor_Xls_File_ListAll]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT InvestorXlsFileId, InvestorXlsFileName 
		FROM INVESTOR_XLS_FILE
END