-- =============================================
-- Author:		Eric Mallare
-- Create date: 3/28/2017
-- Description:	Gets the latest RS file versions by id
-- =============================================
ALTER PROCEDURE [dbo].[GetLatestRsFileVersionsById]
	@RsFileIdsXml xml = null
AS
BEGIN
	IF @RsFileIdsXml IS NULL
	BEGIN
		SELECT 
			rsfv.LpeAcceptableRsFileId, rsfv.VersionNumber, rsfv.FirstEffectiveDateTime, rsfv.LatestEffectiveDateTime, rsfv.IsExpirationIssuedByInvestor
		FROM 
			LPE_ACCEPTABLE_RS_FILE_VERSION rsfv
		WHERE
			rsfv.VersionNumber=(	
								SELECT
									max(VersionNumber)
								FROM
									LPE_ACCEPTABLE_RS_FILE_VERSION rsfv2
								WHERE
									rsfv2.LpeAcceptableRsFileId=rsfv.LpeAcceptableRsFileId
							  )
	END
	ELSE
	BEGIN
		SELECT 
			rsfv.LpeAcceptableRsFileId, rsfv.VersionNumber, rsfv.FirstEffectiveDateTime, rsfv.LatestEffectiveDateTime, rsfv.IsExpirationIssuedByInvestor
		FROM 
			LPE_ACCEPTABLE_RS_FILE_VERSION rsfv JOIN
			@RsFileIdsXml.nodes('root/file/@fileId') as T(Item) ON rsfv.LpeAcceptableRsFileId=T.Item.value('.', 'varchar(100)')
		WHERE
			rsfv.VersionNumber=(	
								SELECT
									max(VersionNumber)
								FROM
									LPE_ACCEPTABLE_RS_FILE_VERSION rsfv2
								WHERE
									rsfv2.LpeAcceptableRsFileId=rsfv.LpeAcceptableRsFileId
							  )
	END
END