

CREATE PROCEDURE [dbo].[RS_File_Create]
	@LpeAcceptableRsFileId		varchar(100),	
	@DeploymentType				varchar(100),	
	@IsBothRsMapAndBotWorking   bit,
	@InvestorXlsFileId			bigint
AS
BEGIN
INSERT INTO LPE_ACCEPTABLE_RS_FILE
           (LpeAcceptableRsFileId, DeploymentType, IsBothRsMapAndBotWorking, InvestorXlsFileId)
     VALUES (@LpeAcceptableRsFileId, @DeploymentType, @IsBothRsMapAndBotWorking, @InvestorXlsFileId)
END

