



CREATE PROCEDURE [dbo].[RS_InvestorXlsFile_GetLatestEffectiveDate]
	@InvestorXlsFileName as varchar(100)  
AS
BEGIN
declare @ratesheet_id bigint
declare @output_file_id varchar(100)
set @ratesheet_id = (select top(1) investorxlsfileid from investor_xls_file where investorxlsfilename = @InvestorXlsFileName)
set @output_file_id = (select top(1) LpeAcceptableRsFileId from lpe_acceptable_rs_file where investorxlsfileid = @ratesheet_id)

SELECT top (1) LatestEffectiveDateTime 
		FROM LPE_ACCEPTABLE_RS_FILE_VERSION
		WHERE LpeAcceptableRsFileId = @output_file_id
              order by VersionNumber DESC

END



