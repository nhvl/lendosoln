ALTER PROCEDURE [dbo].[RS_InvestorXlsFile_Create]
	@InvestorXlsFileId as bigint out,
	@InvestorXlsFileName as varchar(100),
	@EffectiveDateTimeWorksheetName as varchar(100),
	@EffectiveDateTimeLandMarkText as varchar(100),
	@EffectiveDateTimeRowOffset as int,
	@EffectiveDateTimeColumnOffset as int,
	@ExcludeFromInvPricingWarnings as bit
	 
AS
BEGIN
	INSERT INTO INVESTOR_XLS_FILE
         (InvestorXlsFileName, EffectiveDateTimeWorksheetName,
          EffectiveDateTimeLandMarkText, EffectiveDateTimeRowOffset, EffectiveDateTimeColumnOffset, ExcludeFromInvPricingWarnings)
    VALUES (
          @InvestorXlsFileName, @EffectiveDateTimeWorksheetName,
          @EffectiveDateTimeLandMarkText,@EffectiveDateTimeRowOffset, @EffectiveDateTimeColumnOffset, @ExcludeFromInvPricingWarnings )
	set @InvestorXlsFileId = SCOPE_IDENTITY()
END
