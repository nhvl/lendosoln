ALTER PROCEDURE [dbo].[RS_InvestorXlsFile_GetByName]
	@InvestorXlsFileName varchar(100) 
AS
BEGIN
	SELECT InvestorXlsFileId, InvestorXlsFileName, EffectiveDateTimeWorksheetName, EffectiveDateTimeLandMarkText, EffectiveDateTimeRowOffset, EffectiveDateTimeColumnOffset, FileVersionDateTime, ExcludeFromInvPricingWarnings
	FROM INVESTOR_XLS_FILE
	WHERE InvestorXlsFileName = @InvestorXlsFileName
END
