ALTER PROCEDURE [dbo].[RS_InvestorXlsFile_Update]
	@InvestorXlsFileId as bigint,
	@InvestorXlsFileName as varchar(100),
	@EffectiveDateTimeWorksheetName as varchar(100),
	@EffectiveDateTimeLandMarkText as varchar(100),
	@EffectiveDateTimeRowOffset as int,
	@EffectiveDateTimeColumnOffset as int,
	@ExcludeFromInvPricingWarnings as bit
AS
	BEGIN
	UPDATE INVESTOR_XLS_FILE
	SET 
		InvestorXlsFileName = @InvestorXlsFileName,
		EffectiveDateTimeWorksheetName = @EffectiveDateTimeWorksheetName,
		EffectiveDateTimeLandMarkText = @EffectiveDateTimeLandMarkText,
		EffectiveDateTimeRowOffset = @EffectiveDateTimeRowOffset,
		EffectiveDateTimeColumnOffset = @EffectiveDateTimeColumnOffset,
		ExcludeFromInvPricingWarnings = @ExcludeFromInvPricingWarnings
	
	WHERE 
		InvestorXlsFileId = @InvestorXlsFileId     
END