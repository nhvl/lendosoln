
ALTER PROCEDURE [dbo].[RS_CreateDownloadListEntry]
	@Description as varchar(250),
	@URL as varchar(250),
	@FileName as varchar(500),
	@Active as char(1),
	@Conversion as varchar(50) = NULL,
	@BotType as varchar(50) = NULL,
	@Login as varchar(50) = NULL,
	@Password as varchar(50) = NULL,
	@LastRatesheetTimestamp as datetime = NULL,
	@Token as varchar(200) = NULL,
	@Note as varchar(1000) = NULL
AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO DOWNLOAD_LIST (Description, URL, FileName, Active, Conversion, BotType, Login, Password, ErrorCount, LastRatesheetTimestamp, Token, Note)
	VALUES(@Description, @URL, @FileName, @Active, @Conversion, @BotType, @Login, @Password, 0, @LastRatesheetTimestamp, @Token, @Note)

END
