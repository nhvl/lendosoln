-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE RS_OutputFile_ListAll
AS
BEGIN
	SELECT ID, FileName, ExportType, Worksheet, Enabled, Dirty, dUpdated, LastUlResult, ErrorCount, UpdateMonitorEmails
	FROM OUTPUT_FILE
END
