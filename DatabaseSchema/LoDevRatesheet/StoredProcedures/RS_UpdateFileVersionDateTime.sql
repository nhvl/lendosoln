
CREATE PROCEDURE [dbo].[RS_UpdateFileVersionDateTime]
	@InvestorXlsFileName as varchar(100),
	@FileVersionDateTime as datetime
AS
	BEGIN
	UPDATE INVESTOR_XLS_FILE
	SET 
		FileVersionDateTime = @FileVersionDateTime
	WHERE 
		InvestorXlsFileName = @InvestorXlsFileName     
END