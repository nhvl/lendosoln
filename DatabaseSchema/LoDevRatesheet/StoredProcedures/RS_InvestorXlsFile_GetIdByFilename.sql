


CREATE PROCEDURE [dbo].[RS_InvestorXlsFile_GetIdByFilename]
	@InvestorXlsFileName as varchar(100)  
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT InvestorXlsFileId 
		FROM INVESTOR_XLS_FILE
		WHERE InvestorXlsFileName = @InvestorXlsFileName

END
