


CREATE PROCEDURE [dbo].[RS_InvestorXlsFile_GetById]
	@InvestorXlsFileId as bigint  
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT *
		FROM INVESTOR_XLS_FILE
		WHERE InvestorXlsFileId = @InvestorXlsFileId

END
