-- =============================================
-- Author:		Timothy Jewell
-- Create date: 7/29/2014
-- Description:	Set whether the download list entry is active
-- =============================================
CREATE PROCEDURE [dbo].[RS_UpdateDownloadListEntryActiveByID]
	@ID as int,
	@Active as char(1)
AS
BEGIN
	SET NOCOUNT ON;

	UPDATE DOWNLOAD_LIST 
	SET Active = @Active
	WHERE ID = @ID
END
