
CREATE PROCEDURE [dbo].[RS_File_GetAll2]
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;

    SELECT f.*, fv.LatestEffectiveDateTime, fv.IsExpirationIssuedByInvestor
    FROM LPE_ACCEPTABLE_RS_FILE_VERSION fv
    INNER JOIN (SELECT LpeAcceptableRsFileId, max(VersionNumber) max_VersionNumber
                FROM LPE_ACCEPTABLE_RS_FILE_VERSION
                group by LpeAcceptableRsFileId) fv2
        ON fv.LpeAcceptableRsFileId = fv2.LpeAcceptableRsFileId AND fv.VersionNumber = fv2.max_VersionNumber
    RIGHT JOIN LPE_ACCEPTABLE_RS_FILE f ON fv.LpeAcceptableRsFileId = f.LpeAcceptableRsFileId
    ORDER BY f.LpeAcceptableRsFileId
END
