-- =============================================
-- Author:		Scott Kibler
-- Description:
	-- Gets the names of the columns for the table.
-- =============================================
CREATE PROCEDURE [dbo].[Justin_Testing_GetColumnNamesByTableName]
	-- Add the parameters for the stored procedure here
	@TableName varchar(200)	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT column_name
	FROM INFORMATION_SCHEMA.columns 
	where table_name = @TableName
	and table_schema = 'dbo'
END
