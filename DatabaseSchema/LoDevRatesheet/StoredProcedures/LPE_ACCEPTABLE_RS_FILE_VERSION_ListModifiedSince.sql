ALTER PROCEDURE dbo.LPE_ACCEPTABLE_RS_FILE_VERSION_ListModifiedSince 
    @ModifiedD datetime
AS
BEGIN

    -- List all records modified after the given date.
    SELECT LpeAcceptableRsFileId, VersionNumber, FirstEffectiveDateTime, LatestEffectiveDateTime, IsExpirationIssuedByInvestor, EntryCreatedD, ModifiedD
    FROM LPE_ACCEPTABLE_RS_FILE_VERSION
    WHERE ModifiedD > @ModifiedD

END
GO
