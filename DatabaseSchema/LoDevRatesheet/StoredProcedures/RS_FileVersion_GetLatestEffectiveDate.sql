



ALTER PROCEDURE [dbo].[RS_FileVersion_GetLatestEffectiveDate]
	@LpeAcceptableRsFileId as varchar(100)  
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT TOP 1 LatestEffectiveDateTime, IsExpirationIssuedByInvestor
		FROM LPE_ACCEPTABLE_RS_FILE_VERSION
		WHERE LpeAcceptableRsFileId = @LpeAcceptableRsFileId
              order by VersionNumber DESC

END



