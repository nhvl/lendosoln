

CREATE PROCEDURE [dbo].[RS_File_ExpireOnNewRSDetected] 
	@InvestorXlsFileName varchar(100)
AS
BEGIN
	DECLARE @InvestorXlsFileId varchar(200)
select @InvestorXlsFileId = investorXlsFileId from investor_xls_file ix where investorXlsFilename = @InvestorXlsFileName

update LPE_ACCEPTABLE_RS_FILE 
set DeploymentType = 'UseVersionAfterGeneratingAnOutputFileOk'
where InvestorXlsFileId = @InvestorXlsFileId and DeploymentType = 'UseVersion'
END

