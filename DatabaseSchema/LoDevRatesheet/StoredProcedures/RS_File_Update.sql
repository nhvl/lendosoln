
CREATE PROCEDURE [dbo].[RS_File_Update]
	@LpeAcceptableRsFileId		varchar(100),	
	@DeploymentType				varchar(100),	
	@IsBothRsMapAndBotWorking   bit,
	@InvestorXlsFileId			bigint
AS
BEGIN
	UPDATE LPE_ACCEPTABLE_RS_FILE
         set DeploymentType = @DeploymentType, 
             IsBothRsMapAndBotWorking = @IsBothRsMapAndBotWorking,
			 InvestorXlsFileId = @InvestorXlsFileId
	WHERE LpeAcceptableRsFileId = @LpeAcceptableRsFileId
END


