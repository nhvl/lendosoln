-- =============================================
-- Author:		Justin Jia
-- CREATE date: June 14, 2012
-- Description:	Gets non-view table name.
-- =============================================
CREATE PROCEDURE [dbo].[Justin_Testing_GetAllNonViewTableNames]
	-- Add the parameters for the stored procedure here
	
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT DISTINCT TABLE_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME NOT LIKE 'view_%'
END
