ALTER PROCEDURE dbo.LPE_ACCEPTABLE_RS_FILE_ListModifiedSince 
    @ModifiedD datetime
AS
BEGIN

    -- List all records modified after the given date.
    SELECT LpeAcceptableRsFileId, DeploymentType, IsBothRsMapAndBotWorking, InvestorXlsFileId, ModifiedD
    FROM LPE_ACCEPTABLE_RS_FILE
    WHERE ModifiedD > @ModifiedD

END
GO
