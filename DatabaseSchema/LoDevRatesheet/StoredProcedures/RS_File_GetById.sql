

CREATE PROCEDURE [dbo].[RS_File_GetById]
	@LpeAcceptableRsFileId as varchar(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
		SELECT LpeAcceptableRsFileId,DeploymentType,IsBothRsMapAndBotWorking, InvestorXlsFileId
		FROM LPE_ACCEPTABLE_RS_FILE
		WHERE LpeAcceptableRsFileId = @LpeAcceptableRsFileId
END

