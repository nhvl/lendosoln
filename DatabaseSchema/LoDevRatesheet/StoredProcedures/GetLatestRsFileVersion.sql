

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[GetLatestRsFileVersion] 
	@LpeAcceptableRsFileId varchar(100)
AS
BEGIN
	SELECT TOP 1 VersionNumber, FirstEffectiveDateTime, LatestEffectiveDateTime, IsExpirationIssuedByInvestor
	FROM LPE_ACCEPTABLE_RS_FILE_VERSION
	WHERE LpeAcceptableRsFileId = @LpeAcceptableRsFileId
	ORDER BY VersionNumber DESC
END


GO


