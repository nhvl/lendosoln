


CREATE PROCEDURE [dbo].[RS_FileVersion_SetIsExpirationIssuedByInvestorByInvXlsFile]
	@InvestorXlsFileName varchar(100),
	@IsExpirationIssuedByInvestor bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- TODO - use table variable here instead
UPDATE LPE_ACCEPTABLE_RS_FILE_VERSION
SET IsExpirationIssuedByInvestor = @IsExpirationIssuedByInvestor
FROM 
(

	SELECT LpeAcceptableRsFileId as rsId, Max(VersionNumber) as MaxVersionNumber
	FROM  LPE_ACCEPTABLE_RS_FILE_VERSION 
	WHERE LpeAcceptableRsFileId in 
	( 
		SELECT LpeAcceptableRsFileId 
		FROM  LPE_ACCEPTABLE_RS_FILE where InvestorXlsFileId in 
		( 
			SELECT InvestorXlsFileId 
			FROM  INVESTOR_XLS_FILE 
			WHERE InvestorXlsFileName=@InvestorXlsFileName 
		)
	)

	GROUP BY LpeAcceptableRsFileId 

) 
AS a 
WHERE a.rsid = LpeAcceptableRsFileId AND VersionNumber = a.MaxVersionNumber

END
