-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE GetInvestorFileVersionDateTimeByAcceptableRsFileId 
	@LpeAcceptableRsFileId varchar(100)
AS
BEGIN
	SELECT FileVersionDateTime 
	FROM Investor_Xls_File xls JOIN Lpe_Acceptable_Rs_File rs ON xls.InvestorXlsFileId = rs.InvestorXlsFileId
	WHERE rs.LpeAcceptableRsFileId = @LpeAcceptableRsFileId
END
