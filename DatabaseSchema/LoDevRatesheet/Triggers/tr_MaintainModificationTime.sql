

CREATE trigger tr_MaintainModificationTime on lpe_acceptable_rs_file_version

after update
as


declare @nowD datetime
set @nowD = getdate()

update lpe_acceptable_rs_file_version
set ModifiedD = @nowD
from inserted ins
where lpe_acceptable_rs_file_version.lpeAcceptableRsFileId = ins.lpeAcceptableRsFileId
and lpe_acceptable_rs_file_version.versionNumber = ins.versionNumber 
