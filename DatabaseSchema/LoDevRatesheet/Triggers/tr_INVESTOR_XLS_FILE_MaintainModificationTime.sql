CREATE TRIGGER [dbo].[tr_INVESTOR_XLS_FILE_MaintainModificationTime] on [dbo].[INVESTOR_XLS_FILE]
AFTER UPDATE
AS

UPDATE INVESTOR_XLS_FILE
SET ModifiedD = GETDATE()
FROM inserted ins
WHERE INVESTOR_XLS_FILE.InvestorXlsFileId = ins.InvestorXlsFileId
