
CREATE TRIGGER [UpdateOutputFilesLog] ON [dbo].[OUTPUT_FILE] 
FOR UPDATE 
AS
DECLARE @oldBit CHAR(1)
DECLARE @newBit CHAR(1)

IF UPDATE(Dirty)
BEGIN
SELECT @oldBit = (SELECT Dirty FROM Deleted)
SELECT @newBit = (SELECT Dirty FROM Inserted)

IF @oldBit <> @newBit
BEGIN
INSERT INTO Output_File_Log
(FileName, ExportType, Worksheet, Dirty, dUpdated)
SELECT
FileName, ExportType, Worksheet, Dirty, dUpdated
FROM Inserted
END

END

