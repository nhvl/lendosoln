-- =============================================
-- Author: Geoff Feltman
-- Create date: 1/25/18
-- Description:	Adds a new encryption key.
-- =============================================
CREATE PROCEDURE [dbo].[ENCRYPTION_KEYS_Insert]
	@Identifier uniqueidentifier,
	@Description varchar(200),
	@Version int,
	@Field1 varbinary(max),
	@Field2 varbinary(max)	
AS
BEGIN
	INSERT INTO [dbo].[ENCRYPTION_KEYS] (Identifier, Description, Version, Field1, Field2)
	VALUES (@Identifier, @Description, @Version, @Field1, @Field2)
END

GO


