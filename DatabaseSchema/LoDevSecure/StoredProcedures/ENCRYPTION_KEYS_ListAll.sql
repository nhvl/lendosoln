-- ==============================================
-- Author:		Alan Daughton
-- Create date: 3/8/2017
-- Description:	Retrieves all encryption keys
--              from the secure database.
-- ==============================================
ALTER PROCEDURE [dbo].[ENCRYPTION_KEYS_ListAll]
AS
BEGIN
	SELECT [Id], [Identifier], [Description], [Version], [Field1], [Field2]
	FROM [dbo].[ENCRYPTION_KEYS] ORDER BY [Identifier] ASC, [Version] DESC
END
