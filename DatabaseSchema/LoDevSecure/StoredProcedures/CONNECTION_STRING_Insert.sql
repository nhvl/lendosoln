-- =============================================
-- Author: Matthew Flynn
-- Create date: 7/10/18
-- Description:	Adds a new connection string.
-- =============================================
ALTER PROCEDURE [dbo].[CONNECTION_STRING_Insert]
	  @Id uniqueidentifier, 
	  @Description varchar(500), 
	  @DataSource varchar(100), 
	  @InitialCatalog varchar(50), 
	  @UserId varchar(100),
	  @Password varchar(100),
	  @FailoverPartner varchar(100),
	  @Encrypt bit,
	  @ReadOnlyUserId varchar(100),
	  @ReadOnlyPassword varchar(100),
	  @ConnectionType varchar(30),
	  @ConnectionCode varchar(2)
AS
BEGIN
	INSERT INTO [dbo].[CONNECTION_STRING] (
	  Id, 
	  Description, 
	  DataSource, 
	  InitialCatalog, 
	  UserId,
	  Password,
	  FailoverPartner,
	  Encrypt,
	  ReadOnlyUserId,
	  ReadOnlyPassword,
	  ConnectionType,
	  ConnectionCode)
	VALUES (
	  @Id, 
	  @Description, 
	  @DataSource, 
	  @InitialCatalog, 
	  @UserId,
	  @Password,
	  @FailoverPartner,
	  @Encrypt,
	  @ReadOnlyUserId,
	  @ReadOnlyPassword,
	  @ConnectionType,
	  @ConnectionCode)
	
END