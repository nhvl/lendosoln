-- ==============================================
-- Author:		Justin Lara
-- Create date: 9/8/2016
-- Description:	Retrieves all database connection
--              strings from the secure database.
-- ==============================================
ALTER PROCEDURE [dbo].[CONNECTION_STRING_ListAll]
AS
BEGIN
	SELECT Id, DataSource, InitialCatalog, UserID,
	Password, FailoverPartner, Encrypt, 
	Description, ReadOnlyUserId, ReadOnlyPassword,	ConnectionType, ConnectionCode
	FROM CONNECTION_STRING
END
