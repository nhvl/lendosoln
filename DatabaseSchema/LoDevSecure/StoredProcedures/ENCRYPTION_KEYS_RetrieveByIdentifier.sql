-- =============================================
-- Author: Geoff Feltman
-- Create date: 1/26/18
-- Description:	Retrieve a single connection string by identifier.
-- =============================================
ALTER PROCEDURE dbo.ENCRYPTION_KEYS_RetrieveByIdentifier
	@Identifier uniqueidentifier
AS
BEGIN
	SELECT Description, Version, Field1, Field2
	FROM ENCRYPTION_KEYS
	WHERE Identifier = @Identifier
END
