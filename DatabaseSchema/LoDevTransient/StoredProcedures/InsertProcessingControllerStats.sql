CREATE PROCEDURE InsertProcessingControllerStats
	@StatReportD datetime,
	@StatContent varchar(5000)
AS
BEGIN
	INSERT INTO LPE_PROCESSING_CONTROLLER_STATS(StatReportD, StatContent) VALUES (@StatReportD, @StatContent)
END
