-- Return all ids of non-DB cache that is expired
CREATE PROCEDURE CleanupTempCache
AS
  declare  @DeleteTime datetime
  set @DeleteTime  = getdate()
  
  DELETE FROM TEXT_TEMP_CACHE
  WHERE @DeleteTime > CacheAutoExpiredD
  AND  CacheLocationType = 'DB' 
  
  SELECT @@rowcount 
  
  -- Cant delete filedb entries since we need to delete the filedb content. 
  SELECT CacheId FROM TEXT_TEMP_CACHE
  WHERE @DeleteTime > CacheAutoExpiredD
  AND CacheLocationType = 'FileDB' 