


CREATE PROCEDURE [dbo].[CaptureLpeResultByBatchId_5] 
	@RequestBatchId UniqueIdentifier
AS
	SELECT RequestNumberOfRequestsInBatch, ResultDebugInfo, ResultDoneD
	FROM Lpe_Distribute_Result_5 WITH (NOLOCK)
	WHERE RequestBatchId = @RequestBatchId



