
-- Returns the temp cache and type
CREATE       PROCEDURE [dbo].[GetTempCache]
  @CacheId varchar(100)
  
AS
  SELECT CacheContent, CacheLocationType
  FROM TEXT_TEMP_CACHE
  WHERE @CacheId = CacheId
  IF(0!=@@error)
  BEGIN
    RAISERROR('Error in the select statement in GetTempCache sp', 16, 1);
    RETURN -100;
  END
RETURN 0