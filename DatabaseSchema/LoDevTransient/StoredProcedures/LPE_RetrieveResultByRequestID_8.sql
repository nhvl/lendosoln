







CREATE PROCEDURE [dbo].[LPE_RetrieveResultByRequestID_8] 
	@RequestBatchID UniqueIdentifier
AS
SET TRANSACTION ISOLATION LEVEL READ COMMITTED
BEGIN TRANSACTION
	DECLARE @LockResult Int
	EXEC @LockResult = sp_getapplock @Resource='LPE_RESULT_LOCK_8', @LockMode = 'Exclusive'
	IF @LockResult < 0
	BEGIN
		 RAISERROR('Unable to lock LPE_RESULT_LOCK_8 resource.', 16, 1)
  		RETURN
 	END 
	SELECT LpeRequestIntId, ResultDoneD
 	FROM LPE_DISTRIBUTE_RESULT_8
	WHERE RequestBatchId = @RequestBatchID
                   AND GETDATE() < RequestExpirationD


COMMIT TRANSACTION





