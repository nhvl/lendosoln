

/*
Set new JobStatus. We can set as pending when we are coming from 'Error'.
We can always set 'Error'.
*/

CREATE    PROCEDURE [dbo].[UpdateDeriveJob]
	@JobId int
	, @JobStatus varchar(100)
	, @JobXmlContent varchar(5000) = NULL
		
AS

IF ( @JobStatus = 'Pending' 
	AND @JobId IN (SELECT JobId from LPE_DERIVE_JOB WHERE JobStatus = 'Error') )
	UPDATE LPE_DERIVE_JOB
		SET
		JobStatus = 'Pending'
		WHERE JobId = @JobId
ELSE
	UPDATE LPE_DERIVE_JOB
		SET
		JobStatus = @JobStatus
		, JobXmlContent = COALESCE(@JobXmlContent, JobXmlContent)
		WHERE JobId = @JobId

IF 0 != @@ERROR
	BEGIN
		RAISERROR('Error updating LPE_DERIVE_JOB in UpdateDeriveJob sp', 16, 1);
		RETURN -100
	END
	RETURN 0;





