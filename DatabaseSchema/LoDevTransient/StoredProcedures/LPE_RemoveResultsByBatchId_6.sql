







CREATE PROCEDURE [dbo].[LPE_RemoveResultsByBatchId_6]
	@RequestBatchId UniqueIdentifier
AS
BEGIN
SET TRANSACTION ISOLATION LEVEL READ COMMITTED
BEGIN TRANSACTION
	DECLARE @LockResult Int
	EXEC @LockResult = sp_getapplock @Resource='LPE_RESULT_LOCK_6', @LockMode = 'Exclusive'
	IF @LockResult < 0
	BEGIN
		 RAISERROR('Unable to lock LPE_RESULT_LOCK_6 resource.', 16, 1)
  		RETURN
 	END 


	DELETE
 	FROM LPE_DISTRIBUTE_RESULT_6
	WHERE RequestBatchId = @RequestBatchID

	--INSERT INTO Lpe_Result_Done (RequestBatchId) VALUES (@RequestBatchId)

COMMIT TRANSACTION
END








