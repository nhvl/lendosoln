-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ListLoansNeedForNightlyBasedOnRecentModifyLoan]
	@StartDate datetime,
	@EndDate DateTime 
AS
BEGIN

SELECT DISTINCT mod.sLId AS LoanId
	FROM LOAN_FILE_RECENT_MODIFICATION mod
	WHERE 
		ActionDate BETWEEN @StartDate AND @EndDate

END
