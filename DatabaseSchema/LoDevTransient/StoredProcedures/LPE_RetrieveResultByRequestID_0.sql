







CREATE PROCEDURE [dbo].[LPE_RetrieveResultByRequestID_0] 
	@RequestBatchID UniqueIdentifier
AS
SET TRANSACTION ISOLATION LEVEL READ COMMITTED
BEGIN TRANSACTION
	DECLARE @LockResult Int
	EXEC @LockResult = sp_getapplock @Resource='LPE_RESULT_LOCK_0', @LockMode = 'Exclusive'
	IF @LockResult < 0
	BEGIN
		 RAISERROR('Unable to lock LPE_RESULT_LOCK_0 resource.', 16, 1)
  		RETURN
 	END 
	SELECT LpeRequestIntId, ResultDoneD
 	FROM LPE_DISTRIBUTE_RESULT_0
	WHERE RequestBatchId = @RequestBatchID
                   AND GETDATE() < RequestExpirationD


COMMIT TRANSACTION





