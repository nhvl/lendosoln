


CREATE PROCEDURE [dbo].[LPE_InsertResult_6] 
	@RequestBatchId UniqueIdentifier,
	@RequestNumberOfRequestsInBatch int,
	@ResultDebugInfo varchar(500),
	@ResultContent Text = '',
	@LpeRequestIntId BigInt,
	@RequestExpirationD DateTime
AS
SET TRANSACTION ISOLATION LEVEL READ COMMITTED
BEGIN TRANSACTION
	DECLARE @LockResult Int
	EXEC @LockResult = sp_getapplock @Resource='LPE_RESULT_LOCK_6', @LockMode = 'Exclusive'
	IF @LockResult < 0
	BEGIN
		 RAISERROR('Unable to lock LPE_RESULT_LOCK_6 resource.', 16, 1)
  		RETURN
 	END 

	INSERT INTO LPE_DISTRIBUTE_RESULT_6 (RequestBatchId, RequestNumberOfRequestsInBatch, ResultDebugInfo, ResultContent, RequestExpirationD, LpeRequestIntId)
                                                                  VALUES     (@RequestBatchId, @RequestNumberOfRequestsInBatch, @ResultDebugInfo, @ResultContent, @RequestExpirationD, @LpeRequestIntId)



COMMIT TRANSACTION

