-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[LoanModification_GetRecentModification]
	@sLId UniqueIdentifier,
	@PeriodInMinutes int,
	@ExcludeUserId UniqueIdentifier
AS
BEGIN

	SELECT DISTINCT UserName, UserType
	FROM LOAN_FILE_RECENT_MODIFICATION
	WHERE sLId = @sLId
      AND UserId <> @ExcludeUserId
	  AND ActionDate > DATEADD(minute, -1 * @PeriodInMinutes, GetDate())
END
