








CREATE PROCEDURE [dbo].[LPE_DeleteExpiredResults_2] 
AS
--RETURN

SET TRANSACTION ISOLATION LEVEL READ COMMITTED
BEGIN TRANSACTION
	DECLARE @LockResult Int
	EXEC @LockResult = sp_getapplock @Resource='LPE_RESULT_LOCK_2', @LockMode = 'Exclusive'
	IF @LockResult < 0
	BEGIN
		 RAISERROR('Unable to lock LPE_RESULT_LOCK_2 resource.', 16, 1)
  		RETURN
 	END 

DELETE FROM LPE_DISTRIBUTE_RESULT_2
             WHERE RequestExpirationD < GETDATE()

COMMIT TRANSACTION







