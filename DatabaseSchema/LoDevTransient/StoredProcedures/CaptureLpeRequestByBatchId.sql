


CREATE PROCEDURE [dbo].[CaptureLpeRequestByBatchId] 
	@LpeRequestBatchId UniqueIdentifier
AS
SELECT LpeRequestCreatedDate, LpeRequestExpiredDate, LpeRequestCurrentPartInBatch, LpeRequestNumberOfRequestsInBatch, LpeRequestType, InvestorName
FROM Lpe_Request WITH (NOLOCK) WHERE LpeRequestBatchId = @LpeRequestBatchId

