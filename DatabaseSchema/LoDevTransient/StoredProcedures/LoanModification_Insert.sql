-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[LoanModification_Insert]
	@sLId UniqueIdentifier,
	@UserId UniqueIdentifier,
	@UserName varchar(200),
	@UserType varchar(1) = '',
	@Action Int
AS
BEGIN
	INSERT INTO LOAN_FILE_RECENT_MODIFICATION (sLId, UserId, UserName, UserType, Action)
	VALUES (@sLId, @UserId, @UserName, @UserType, @Action)
END
