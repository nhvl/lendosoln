-- Delete expirations for all cache expired before the passed time
CREATE PROCEDURE [dbo].[RemoveTempCacheExpirations]
  @CacheId varchar(100) 
AS
  
  DELETE TOP (1) FROM TEXT_TEMP_CACHE
  WHERE CacheId =  @CacheId
  RETURN 0;
