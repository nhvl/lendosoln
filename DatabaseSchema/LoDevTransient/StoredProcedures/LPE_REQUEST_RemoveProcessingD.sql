-- =============================================
-- Author:		Antonio Valencia
-- Create date: 3/15/2013
-- Description:	Removes the processing date so another server picks it up 
-- =============================================
CREATE PROCEDURE [dbo].LPE_REQUEST_RemoveProcessingD
	@LpeRequestIntId bigint
AS
BEGIN
	
	UPDATE TOP(1) LPE_REQUEST
	SET LpeRequestProcessExpD = null 
	WHERE LpeRequestIntId = @LpeRequestIntId 
	
END
