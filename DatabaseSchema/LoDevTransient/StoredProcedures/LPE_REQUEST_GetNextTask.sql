-- =============================================
-- Author:		Antonio Valencia
-- Create date: 3/15/2013
-- Description:	Retrieves the next request to process on server
-- =============================================
ALTER PROCEDURE [dbo].[LPE_REQUEST_GetNextTask] 
AS
SET TRANSACTION ISOLATION LEVEL READ COMMITTED
BEGIN
	
	DECLARE @NextTask table (LpeRequestIntId int)
	DECLARE @AllowableTimeInSecondsForProcessingSeconds int 
	
	SET @AllowableTimeInSecondsForProcessingSeconds = 300
	
	-- check for single submit, preview request first 
	UPDATE TOP(1) tab
	SET LpeRequestProcessExpD = DATEADD(SECOND, @AllowableTimeInSecondsForProcessingSeconds, GETDATE())  
	OUTPUT Inserted.LpeRequestIntId INTO @NextTask 
	FROM LPE_REQUEST tab WITH (ROWLOCK, READPAST, UPDLOCK)  --this allows any other connections to skip this row instead of waiting
	WHERE (LpeRequestType = 'SingleSubmit' or LpeRequestType = 'PreviewRequest')  
		 AND LpeRequestExpiredDate > GetDate() 
		 AND  ( LpeRequestProcessExpD is null  OR LpeRequestProcessExpD < GetDate())
	
	
	IF EXISTS(SELECT * FROM @NextTask)
	BEGIN
		GOTO GETTASK	
	END	
		
	UPDATE TOP(1) tab
	SET LpeRequestProcessExpD = DATEADD(SECOND, @AllowableTimeInSecondsForProcessingSeconds, GETDATE())
	OUTPUT Inserted.LpeRequestIntId INTO @NextTask 
	FROM LPE_REQUEST tab WITH (ROWLOCK, READPAST, UPDLOCK) 
	WHERE LpeRequestType = 'PriceRequest' 
		 AND LpeRequestExpiredDate > GetDate() 
		 AND  ( LpeRequestProcessExpD is null  OR LpeRequestProcessExpD < GetDate())
	
	IF EXISTS(SELECT * FROM @NextTask)
		BEGIN
		GOTO GETTASK	
	END
	
	
	SELECT TOP(0) * FROM  LPE_Request
	
	RETURN 	
	
GETTASK: 

	SELECT LpeRequestIntId, LpeRequestBatchId, LpeRequestXmlContent, LpeRequestCreatedDate, 
		       LpeRequestExpiredDate, LpeRequestCurrentPartInBatch, LpeRequestNumberOfRequestsInBatch, LpeRequestType,
               GETDATE() AS CurrentDBTime		       
	FROM LPE_Request 
	WHERE LpeRequestIntId in (SELECT LpeRequestIntId from @NextTask)

	RETURN
END
