








CREATE PROCEDURE [dbo].[LPE_GetRequestMessages]
	@LastCutoffId BigInt
AS
BEGIN
SET TRANSACTION ISOLATION LEVEL REPEATABLE READ
BEGIN TRANSACTION
	DECLARE @LockResult Int
	EXEC @LockResult = sp_getapplock @Resource='LPE_REQUEST_LOCK', @LockMode = 'Exclusive'
	IF @LockResult < 0
	BEGIN
		 RAISERROR('Unable to lock LPE_REQUEST_LOCK resource.', 16, 1)
  		RETURN
 	END 

		SELECT TOP 400 LpeRequestIntId, LpeRequestBatchId, LpeRequestXmlContent, LpeRequestCreatedDate, 
		       LpeRequestExpiredDate, LpeRequestCurrentPartInBatch, LpeRequestNumberOfRequestsInBatch, 
               GETDATE() AS CurrentDBTime		       
		FROM LPE_Request 
		WHERE LpeRequestIntId > @LastCutoffId
		  AND LpeRequestExpiredDate > GETDATE()
		ORDER BY LpeRequestIntId
COMMIT TRANSACTION;
END









