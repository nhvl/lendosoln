
CREATE PROCEDURE RetrieveProcessingControllerStatById
	@StatId BigInt
AS
BEGIN
	SELECT StatReportD, StatContent FROM LPE_PROCESSING_CONTROLLER_STATS WHERE StatId = @StatId
END
