-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE dbo.ERROR_UpdateEmailDate
	@ErrorMessage varchar(150),
	@LastEmailedToOpmDate datetime=null,
	@LastEmailedToCriticalDate	 datetime=null
AS
BEGIN
	UPDATE RECENT_ERROR_MESSAGE
	SET LastEmailedToOpmDate = @LastEmailedToOpmDate,
	LastEmailedToCriticalDate = @LastEmailedToCriticalDate
	WHERE ErrorMessage=@ErrorMessage
END
