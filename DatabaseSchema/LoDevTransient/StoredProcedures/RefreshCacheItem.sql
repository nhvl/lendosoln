
-- =============================================
-- Author:		Antonio Valencia
-- Create date: 7/15/11
-- Description:	Updates the cache time
-- =============================================
CREATE PROCEDURE [dbo].[RefreshCacheItem]
	 @CacheId varchar(100),
	 @Minutes int

AS
BEGIN


	UPDATE TEXT_TEMP_CACHE 
	SET CacheAutoExpiredD = DATEADD(minute, @Minutes, GETDATE()) 
	WHERE CacheId = @CacheId;
	
END

