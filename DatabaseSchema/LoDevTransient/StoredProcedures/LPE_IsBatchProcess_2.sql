


CREATE PROCEDURE [dbo].[LPE_IsBatchProcess_2] 
	@RequestBatchId UniqueIdentifier
AS
	DECLARE @Cnt int
	SELECT @Cnt = COUNT(*) 	
 	FROM LPE_DISTRIBUTE_RESULT_2 WITH(NOLOCK)
	WHERE RequestBatchId = @RequestBatchId

	SELECT @Cnt AS Cnt



