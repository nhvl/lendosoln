


CREATE PROCEDURE [dbo].[CaptureLpeResultByBatchId_6] 
	@RequestBatchId UniqueIdentifier
AS
	SELECT RequestNumberOfRequestsInBatch, ResultDebugInfo, ResultDoneD
	FROM Lpe_Distribute_Result_6 WITH (NOLOCK)
	WHERE RequestBatchId = @RequestBatchId



