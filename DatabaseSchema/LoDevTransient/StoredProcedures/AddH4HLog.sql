-- =============================================
-- Author:		Antonio Valencia
-- Create date: 12/22/08
-- Description:	Logs for fha connection
-- =============================================
CREATE PROCEDURE AddH4HLog 
	-- Add the parameters for the stored procedure here
	@SubmissionId bigint, 
	@LoanId uniqueidentifier,
	@RetryDateTime datetime,
	@MismoMessage text,
	@ExceptionText varchar(1000)
AS
BEGIN
	insert into FHA_CONNECTION_LOG(SubmissionId,LoanId,  RetryDateTime, ExceptionText,MismoMessage ) 
	VALUES( @SubmissionId, @LoanId, @RetryDateTime, @ExceptionText, @MismoMessage )
END
