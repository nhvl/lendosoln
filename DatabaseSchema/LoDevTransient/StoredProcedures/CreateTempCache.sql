
CREATE        PROCEDURE [dbo].[CreateTempCache]
  @CacheId varchar(100)
, @SecondsToLive int
, @CacheLocationType varchar(10)
, @CacheContent varchar(6000) = ''
AS
  DECLARE @Expiration datetime
  SET @Expiration = DATEADD(second, @SecondsToLive, GETDATE())
  INSERT INTO TEXT_TEMP_CACHE(CacheId, CacheLocationType, CacheAutoExpiredD, CacheContent)
  VALUES (@CacheId, @CacheLocationType, @Expiration, @CacheContent)
  IF ( 0 != @@ERROR )
  BEGIN
    RAISERROR('Error in inserting into Text_Temp_Cache table in CreateTempCache', 16, 1);
    RETURN -100;
  END
  RETURN 0;