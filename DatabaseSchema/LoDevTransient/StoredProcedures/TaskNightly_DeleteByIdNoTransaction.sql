-- =============================================
-- Author:		Michael Leinweaver
-- Create date: 4/25/2017
-- Description:	Deletes from the nightly task table.
-- =============================================
CREATE PROCEDURE [dbo].[TaskNightly_DeleteByIdNoTransaction]
	@Id BigInt
AS
BEGIN
	DELETE FROM TASK_NIGHTLY_UPDATE_LOAN 
	WHERE Id = @Id
END
