-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[TaskNightly_GetTopLoanId] 
AS
BEGIN

SET TRANSACTION ISOLATION LEVEL REPEATABLE READ
BEGIN TRANSACTION

	DECLARE @LockResult Int
	EXEC @LockResult = sp_getapplock @Resource='TASK_NIGHTLY_UPDATE_LOAN', @LockMode='Exclusive'
	IF @LockResult < 0
	BEGIN
		RAISERROR('Unable to lock TASK_NIGHTLY_UPDATE_LOAN resource.', 16, 1)
		RETURN
	END

	DECLARE @Id BigInt
	DECLARE @sLId UniqueIdentifier

	SELECT TOP 1 @Id = Id, @sLId = sLId 
	FROM TASK_NIGHTLY_UPDATE_LOAN 
	WHERE (ProcessedDate IS NULL OR DATEADD(minute, 1, ProcessedDate)  < GETDATE())
	ORDER BY ID Desc
 
	IF @@ROWCOUNT > 0
	BEGIN
		UPDATE TASK_NIGHTLY_UPDATE_LOAN SET ProcessedDate = GETDATE() WHERE Id = @Id

		SELECT @Id AS Id, @sLId AS sLId
	END

COMMIT TRANSACTION;

END
