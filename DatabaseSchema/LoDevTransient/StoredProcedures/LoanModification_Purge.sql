-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE LoanModification_Purge
	@PeriodInMinutes int
AS
BEGIN
	DELETE FROM LOAN_FILE_RECENT_MODIFICATION
	WHERE
	  ActionDate < DATEADD(minute, -1 * @PeriodInMinutes, GetDate())
END
