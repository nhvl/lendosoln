
/*
Delete derive job.
*/


CREATE    PROCEDURE [dbo].[DeleteDeriveJob]
	@JobId int
	, @AllowDeleteInProgress bit = 0
AS

IF ( @AllowDeleteInProgress = 1 
	OR (@JobId IN (SELECT JobId FROM Lpe_Derive_Job WHERE JobStatus <> 'InProgress') ) )
	DELETE FROM LPE_DERIVE_JOB WHERE JobId = @JobId
    
IF 0 != @@ERROR
	BEGIN
		RAISERROR('Error deleting from LPE_DERIVE_JOB in DeleteDeriveJob sp', 16, 1);
		RETURN -100
	END
	RETURN 0;




