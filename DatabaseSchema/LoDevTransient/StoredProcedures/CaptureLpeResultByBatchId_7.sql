


CREATE PROCEDURE [dbo].[CaptureLpeResultByBatchId_7] 
	@RequestBatchId UniqueIdentifier
AS
	SELECT RequestNumberOfRequestsInBatch, ResultDebugInfo, ResultDoneD
	FROM Lpe_Distribute_Result_7 WITH (NOLOCK)
	WHERE RequestBatchId = @RequestBatchId



