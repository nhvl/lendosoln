-- =============================================
-- Author:		Antonio Valencia
-- Create date: 6/20/2013
-- Description:	Deletes given cache item
-- =============================================
CREATE PROCEDURE [dbo].DeleteCacheItem
	 @CacheId varchar(100)

AS
BEGIN
		DELETE TOP(1) TEXT_TEMP_CACHE
		where cacheid = @CacheId
END


