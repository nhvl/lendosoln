


CREATE PROCEDURE [dbo].[CaptureLpeResultByBatchId_9] 
	@RequestBatchId UniqueIdentifier
AS
	SELECT RequestNumberOfRequestsInBatch, ResultDebugInfo, ResultDoneD
	FROM Lpe_Distribute_Result_9 WITH (NOLOCK)
	WHERE RequestBatchId = @RequestBatchId



