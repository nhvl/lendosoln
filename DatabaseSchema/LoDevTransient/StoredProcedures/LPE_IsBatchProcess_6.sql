


CREATE PROCEDURE [dbo].[LPE_IsBatchProcess_6] 
	@RequestBatchId UniqueIdentifier
AS
	DECLARE @Cnt int
	SELECT @Cnt = COUNT(*) 	
 	FROM LPE_DISTRIBUTE_RESULT_6 WITH(NOLOCK)
	WHERE RequestBatchId = @RequestBatchId

	SELECT @Cnt AS Cnt



