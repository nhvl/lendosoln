/* =============================================
-- Author:		Scott Kibler
-- Create date: 8/13/2014
-- Description:	
	Checks for loan presence in the Transient DB.
	NOTE: It skips any tables that foreign key to these tables.
-- =============================================*/
CREATE PROCEDURE dbo.TRANSIENT_DB_LoanHasAnyPresence
	@BrokerID UniqueIdentifier,
	@LoanID UniqueIdentifier,
	@HasPresence bit OUTPUT,
	@TablesWhereItsPresent varchar(1000) OUTPUT	-- csl
AS
BEGIN
	DECLARE @Error int		-- to come from  @@Error
	DECLARE @RowCount int   -- to come from  @@RowCount
	DECLARE @ErrorCode int
	DECLARE @ErrorMsg varchar(1000)
	
	SET @HasPresence = 0;
	SET @TablesWhereItsPresent = '';
	SET @ErrorCode = -1;
	SET @ErrorMsg = 'Unexpected Error';
	
	--SELECT @RowCount = COUNT(*) 
	--FROM FHA_CONNECTION_LOG 
	--WHERE 
	--	LoanId = @LoanID
	--IF(0 <> @@Error)
	--BEGIN
	--	SET @ErrorCode = -53
	--	SET @ErrorMsg = 'Unable to select from FHA_CONNECTION_LOG'
	--	GOTO ErrorHandler;
	--END
	--IF(0 <> @RowCount)
	--BEGIN
	--	SET @HasPresence = 1;
	--	SET @TablesWhereItsPresent = @TablesWhereItsPresent + ', FHA_CONNECTION_LOG';
	--END
	
	
	SELECT @RowCount = COUNT(*) 
	FROM LOAN_FILE_RECENT_MODIFICATION 
	WHERE 
		sLId = @LoanID
	IF(0 <> @@Error)
	BEGIN
		SET @ErrorCode = -53
		SET @ErrorMsg = 'Unable to select from LOAN_FILE_RECENT_MODIFICATION'
		GOTO ErrorHandler;
	END
	IF(0 <> @RowCount)
	BEGIN
		SET @HasPresence = 1;
		SET @TablesWhereItsPresent = @TablesWhereItsPresent + ', LOAN_FILE_RECENT_MODIFICATION';
	END
	
	
	SELECT @RowCount = COUNT(*) 
	FROM TASK_NIGHTLY_UPDATE_LOAN 
	WHERE 
		sLId = @LoanID
	IF(0 <> @@Error)
	BEGIN
		SET @ErrorCode = -53
		SET @ErrorMsg = 'Unable to select from TASK_NIGHTLY_UPDATE_LOAN'
		GOTO ErrorHandler;
	END
	IF(0 <> @RowCount)
	BEGIN
		SET @HasPresence = 1;
		SET @TablesWhereItsPresent = @TablesWhereItsPresent + ', TASK_NIGHTLY_UPDATE_LOAN';
	END
	
	
	SELECT @RowCount = COUNT(*) 
	FROM TASK_TRIGGER_NIGHTLY_UPDATE_LOAN 
	WHERE 
		sLId = @LoanID
	IF(0 <> @@Error)
	BEGIN
		SET @ErrorCode = -53
		SET @ErrorMsg = 'Unable to select from TASK_TRIGGER_NIGHTLY_UPDATE_LOAN'
		GOTO ErrorHandler;
	END
	IF(0 <> @RowCount)
	BEGIN
		SET @HasPresence = 1;
		SET @TablesWhereItsPresent = @TablesWhereItsPresent + ', TASK_TRIGGER_NIGHTLY_UPDATE_LOAN';
	END
	
	
	RETURN 0;
	ErrorHandler:
		RAISERROR(@ErrorMsg, 16, 1)
		RETURN @ErrorCode;
END
