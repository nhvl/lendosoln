-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE dbo.TaskTriggerNightly_DeleteById
	@Id BigInt
AS
BEGIN
SET TRANSACTION ISOLATION LEVEL REPEATABLE READ
BEGIN TRANSACTION

	DECLARE @LockResult Int
	EXEC @LockResult = sp_getapplock @Resource='TASK_TRIGGER_NIGHTLY_UPDATE_LOAN', @LockMode='Exclusive'
	IF @LockResult < 0
	BEGIN
		RAISERROR('Unable to lock TASK_TRIGGER_NIGHTLY_UPDATE_LOAN resource.', 16, 1)
		RETURN
	END

	DELETE TASK_TRIGGER_NIGHTLY_UPDATE_LOAN WHERE Id = @Id

COMMIT TRANSACTION;
END
