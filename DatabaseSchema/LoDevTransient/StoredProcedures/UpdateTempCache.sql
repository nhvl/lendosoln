
-- Updates the value in temp cache if possible.
-- UpdateResult output parameter takes one of the following
--
-- 1. There is no record for that cacheId
-- 2. A DB cache record was updated
-- 3. Converted a DB cache record to FileDB
-- 4. A FiledDB cache record exists for cacheId
CREATE        PROCEDURE [dbo].[UpdateTempCache]
  @CacheId varchar(100)
, @CacheContent varchar(6000)
, @UpdateLocation varchar(10)
, @UpdateResult tinyint OUT
AS
  BEGIN TRANSACTION
  DECLARE @CurrentLocationType varchar(10)
  SET @CurrentLocationType = (SELECT CacheLocationType FROM TEXT_TEMP_CACHE WHERE CacheId = @CacheId)
  IF (0 != @@ERROR)
  BEGIN
    RAISERROR('Error in the select statement in UpdateTempCache sp', 16, 1);
    ROLLBACK TRANSACTION
    RETURN -100;
  END
  IF(@CurrentLocationType IS NOT NULL)
  BEGIN
    IF (@CurrentLocationType = 'DB')
    BEGIN
    -- The entry is already in the DB and the new content fits, so update here
      IF (@UpdateLocation = 'DB')
      BEGIN
        UPDATE TEXT_TEMP_CACHE
        SET
        CacheContent = @CacheContent
        WHERE @CacheId = CacheId
        IF (0 != @@ERROR)
        BEGIN
          RAISERROR('Error in the update statement in UpdateTempCache sp', 16, 1);
          ROLLBACK TRANSACTION
          RETURN -100;
        END
        SET @UpdateResult = 2 -- Updated DB Entry
      END
      -- The entry is already in the DB and the new content does not fit.  We convert
      -- to FileDB cache, and let the caller insert into FileDB.  If FileDB creation
      -- fails, the client rolls back this transaction.
      ELSE
      BEGIN
        UPDATE TEXT_TEMP_CACHE
        SET
          CacheContent = '',
          CacheLocationType = 'FileDB'
        WHERE @CacheId = CacheId
        IF (0 != @@ERROR)
        BEGIN
          RAISERROR('Error in the update statement in UpdateTempCache sp', 16, 1);
          ROLLBACK TRANSACTION
          RETURN -100;
        END
        SET @UpdateResult = 3 -- Updated DB entry to FileDB
      END
    END
  
    ELSE
      SET @UpdateResult = 4 -- There is already a FileDB entry
  END
  ELSE
    SET @UpdateResult = 1 -- There is no cache item
  
  COMMIT TRANSACTION
 
  RETURN 0;

