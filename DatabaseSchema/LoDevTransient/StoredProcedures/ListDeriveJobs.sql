
/*
List all derive jobs, including Errors.  Note that we include the jobXml
because we can store Error information there. -mf 04/05/07
*/
CREATE    PROCEDURE [dbo].[ListDeriveJobs]
	
AS
	SELECT
	JobID
	, JobXmlContent
	, JobStatus
	, JobPriority
	, JobSubmitD
	, JobStartD
	, JobNotifyEmailAddr
	, JobUserLabel
	, JobUserNotes
	, UserLoginNm
	FROM LPE_DERIVE_JOB
    ORDER BY 
	CASE JobStatus WHEN 'InProgress' THEN 1 ELSE 0 END DESC
	, CASE JobPriority WHEN 'Low' THEN 0 WHEN 'Normal' THEN 1 WHEN 'High' THEN 2 END DESC
	, JobSubmitD ASC
    
IF 0 != @@ERROR
	BEGIN
		RAISERROR('Error in ListDeriveJobs sp', 16, 1);
		RETURN -100
	END
	RETURN 0;









