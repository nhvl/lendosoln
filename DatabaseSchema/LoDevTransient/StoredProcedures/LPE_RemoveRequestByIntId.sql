


CREATE PROCEDURE [dbo].[LPE_RemoveRequestByIntId] 
	@LpeRequestIntId BigInt
AS
BEGIN
--RETURN 
SET TRANSACTION ISOLATION LEVEL READ COMMITTED
BEGIN TRANSACTION
	DECLARE @LockResult Int
	EXEC @LockResult = sp_getapplock @Resource='LPE_REQUEST_LOCK', @LockMode = 'Exclusive'
	IF @LockResult < 0
	BEGIN
		 RAISERROR('Unable to lock LPE_REQUEST_LOCK resource.', 16, 1)
  		RETURN
 	END 
DELETE FROM LPE_Request WHERE LpeRequestIntId = @LpeRequestIntId
COMMIT TRANSACTION
END


