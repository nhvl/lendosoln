


CREATE PROCEDURE [dbo].[LPE_IsBatchProcess_7] 
	@RequestBatchId UniqueIdentifier
AS
	DECLARE @Cnt int
	SELECT @Cnt = COUNT(*) 	
 	FROM LPE_DISTRIBUTE_RESULT_7 WITH(NOLOCK)
	WHERE RequestBatchId = @RequestBatchId

	SELECT @Cnt AS Cnt



