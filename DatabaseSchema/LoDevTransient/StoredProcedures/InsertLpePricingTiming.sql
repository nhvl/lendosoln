-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[InsertLpePricingTiming] 
	@CalcServerId varchar(100) = '',
	@DurationInMs int,
	@TimingDetail varchar(max)
AS
BEGIN
	INSERT INTO LPE_PRICING_TIMING (CalcServerId, DurationInMs, TimingDetail) VALUES (@CalcServerId, @DurationInMs, @TimingDetail)
END
