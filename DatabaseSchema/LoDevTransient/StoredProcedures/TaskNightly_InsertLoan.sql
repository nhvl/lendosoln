-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE dbo.TaskNightly_InsertLoan
	@sLId UniqueIdentifier
AS
BEGIN

SET TRANSACTION ISOLATION LEVEL REPEATABLE READ
BEGIN TRANSACTION

	DECLARE @LockResult Int
	EXEC @LockResult = sp_getapplock @Resource='TASK_NIGHTLY_UPDATE_LOAN', @LockMode='Exclusive'
	IF @LockResult < 0
	BEGIN
		RAISERROR('Unable to lock TASK_NIGHTLY_UPDATE_LOAN resource.', 16, 1)
		RETURN
	END

	-- Only insert sLId if it does not existed.
	INSERT INTO TASK_NIGHTLY_UPDATE_LOAN(sLId)
	SELECT @sLId
	WHERE NOT EXISTS(SELECT 1 FROM TASK_NIGHTLY_UPDATE_LOAN WHERE sLId=@sLId AND processeddate IS NULL)

COMMIT TRANSACTION;

END
