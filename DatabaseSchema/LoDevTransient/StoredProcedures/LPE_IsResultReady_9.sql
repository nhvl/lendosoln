



CREATE PROCEDURE [dbo].[LPE_IsResultReady_9] 
	@RequestBatchID UniqueIdentifier
AS
SET TRANSACTION ISOLATION LEVEL REPEATABLE READ
BEGIN TRANSACTION
	DECLARE @LockResult Int
	EXEC @LockResult = sp_getapplock @Resource='LPE_RESULT_LOCK_9', @LockMode = 'Exclusive'
	IF @LockResult < 0
	BEGIN
		 RAISERROR('Unable to lock LPE_RESULT_LOCK_9 resource.', 16, 1)
  		RETURN
 	END 
	SELECT COUNT(*) AS CurrentCount, MAX(RequestNumberOfRequestsInBatch) AS RequestNumberOfRequestsInBatch
 	FROM LPE_DISTRIBUTE_RESULT_9
	WHERE RequestBatchId = @RequestBatchId
                   AND GETDATE() < RequestExpirationD

COMMIT TRANSACTION



