








CREATE PROCEDURE [dbo].[LPE_DeleteExpiredResults_5] 
AS
--RETURN

SET TRANSACTION ISOLATION LEVEL READ COMMITTED
BEGIN TRANSACTION
	DECLARE @LockResult Int
	EXEC @LockResult = sp_getapplock @Resource='LPE_RESULT_LOCK_5', @LockMode = 'Exclusive'
	IF @LockResult < 0
	BEGIN
		 RAISERROR('Unable to lock LPE_RESULT_LOCK_5 resource.', 16, 1)
  		RETURN
 	END 

DELETE FROM LPE_DISTRIBUTE_RESULT_5
             WHERE RequestExpirationD < GETDATE()

COMMIT TRANSACTION







