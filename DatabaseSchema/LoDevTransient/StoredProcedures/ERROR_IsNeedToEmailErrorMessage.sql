-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ERROR_IsNeedToEmailErrorMessage]
	@ErrorMessage varchar(150),
	@IntervalInMinutes int
AS
BEGIN
	-- This stored procedure will return true or false whether the error message
    -- last send exceed the interval period.

SET TRANSACTION ISOLATION LEVEL REPEATABLE READ
BEGIN TRANSACTION

	DECLARE @LockResult Int
	
	DECLARE @LastEmailedDate datetime
	DECLARE @IsExceedTimeInterval bit
	
	DECLARE @CurrentCount int
	DECLARE @IsNewErrorMessage bit

	DECLARE @LastEmailedToOpmDate datetime
	DECLARE @LastEmailedToCriticalDate datetime
	
	EXEC @LockResult = sp_getapplock @Resource='RECENT_ERROR_MESSAGE', @LockMode = 'Exclusive'
	
	IF @LockResult < 0
	BEGIN
		RAISERROR('Unable to lock RECENT_ERROR_MESSAGE resource.', 16, 1)
		RETURN
	END
	
	SELECT @LastEmailedToOpmDate = LastEmailedToOpmDate, @LastEmailedToCriticalDate = LastEmailedToCriticalDate, @LastEmailedDate = LastEmailedDate, @CurrentCount=CountSinceLastEmail FROM RECENT_ERROR_MESSAGE WHERE ErrorMessage = @ErrorMessage

	IF @LastEmailedDate IS NULL
	BEGIN
		 -- Error Message Does not Existed

		INSERT INTO RECENT_ERROR_MESSAGE(ErrorMessage, LastEmailedDate, CountSinceLastEmail)
			   VALUES (@ErrorMessage, GETDATE(), 1)
		SET @IsExceedTimeInterval = 1
		SET @CurrentCount = 1
		SET @IsNewErrorMessage = 1
	END
	ELSE
	BEGIN
		IF DATEADD(minute, @IntervalInMinutes, @LastEmailedDate) < GETDATE()
		BEGIN
			-- Exceed Time interval 
			DECLARE @NewCount int
			
			IF DATEPART(dayofyear, @LastEmailedDate) <> DATEPART(dayofyear, GETDATE())
			BEGIN
				SET @NewCount = 1 -- Reset after new new day.
			END
			ELSE
			BEGIN
				SET @NewCount = @CurrentCount + 1
			END
			
			UPDATE RECENT_ERROR_MESSAGE
			SET LastEmailedDate = GETDATE(), 
				CountSinceLastEmail = @NewCount
			WHERE ErrorMessage = @ErrorMessage

			SET @IsExceedTimeInterval = 1
		SET @CurrentCount = @NewCount
		SET @IsNewErrorMessage = 0
			
		END
		ELSE
		BEGIN
			-- Not Exceed Time Interval

			UPDATE RECENT_ERROR_MESSAGE
			SET CountSinceLastEmail = CountSinceLastEmail + 1
			WHERE ErrorMessage = @ErrorMessage

			SET @IsExceedTimeInterval = 0
		SET @CurrentCount = @CurrentCount + 1
		SET @IsNewErrorMessage = 0
			
		END
	END

SELECT @IsExceedTimeInterval AS IsExceedTimeInterval, @CurrentCount AS CurrentCount, @IsNewErrorMessage AS IsNewErrorMessage, @LastEmailedToOpmDate AS LastEmailedToOpmDate, @LastEmailedToCriticalDate AS LastEmailedToCriticalDate

COMMIT TRANSACTION;

END
