





CREATE PROCEDURE [dbo].[LPE_InsertRequestMessage] 
	@LpeRequestBatchId UniqueIdentifier,
	@LpeRequestCurrentPartInBatch int,
	@LpeRequestNumberOfRequestsInBatch int,
	@ExpirationMinutes int,
	@LpeRequestType varchar(30),
	@LpeRequestXmlContent varchar(max),
	@InvestorName varchar(100)
AS
--RETURN 

SET TRANSACTION ISOLATION LEVEL READ COMMITTED
BEGIN TRANSACTION
	DECLARE @LockResult Int
	EXEC @LockResult = sp_getapplock @Resource='LPE_REQUEST_LOCK', @LockMode = 'Exclusive'
	IF @LockResult < 0
	BEGIN
		 RAISERROR('Unable to lock LPE_REQUEST_LOCK resource.', 16, 1)
  		RETURN
 	END 
INSERT INTO LPE_Request (LpeRequestBatchId, LpeRequestXmlContent, LpeRequestExpiredDate, LpeRequestCurrentPartInBatch, 
                                                LpeRequestNumberOfRequestsInBatch, LpeRequestType, InvestorName) 
         VALUES (@LpeRequestBatchId, @LpeRequestXmlContent, DATEADD(minute, @ExpirationMinutes, GETDATE()), @LpeRequestCurrentPartInBatch,
                       @LpeRequestNumberOfRequestsInBatch, @LpeRequestType,@InvestorName )
COMMIT TRANSACTION;





