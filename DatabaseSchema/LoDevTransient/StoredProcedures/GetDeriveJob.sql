/*
This is called when we want the latest derivation job row for derivation.
In an atomic transaction, we get the latest job, and set its status to
"InProgress".  To determine which job to take, we always take any "InProgress"
job first, then highest priority, then earliest submitted. We ignore jobs in
the "Error" status here. -mf 04/05/06
*/

ALTER    PROCEDURE [dbo].[GetDeriveJob]

AS

BEGIN TRANSACTION 

DECLARE @JobId int
SET @JobId = (SELECT TOP 1
	JobID
	FROM LPE_DERIVE_JOB
    WHERE JobStatus <> 'Error' AND (JobStartAfterD is NULL  OR JobStartAfterD <= getDate() )
	ORDER BY 
	CASE JobStatus WHEN 'InProgress' THEN 1 ELSE 0 END DESC
	, CASE JobPriority WHEN 'Low' THEN 0 WHEN 'Normal' THEN 1 WHEN 'High' THEN 2 END DESC
	, JobSubmitD ASC
	)

	IF ( 0!=@@error )
	BEGIN
		RAISERROR('Error in SELECT FROM Lpe_Derive_Job in GetDeriveJob sp', 16, 1);
		ROLLBACK TRANSACTION
		RETURN -100;
	END

UPDATE LPE_DERIVE_JOB
	SET
	JobStatus = 'InProgress'
	, JobStartD = GetDate()
	WHERE JobId = @JobId

	IF ( 0!=@@error )
	BEGIN
		RAISERROR('Error in UPDATE Lpe_Derive_Job in GetDeriveJob sp', 16, 1);
		ROLLBACK TRANSACTION
		RETURN -100;
	END

SELECT TOP 1
	JobID
	, jobXmlContent
	, JobStatus
	, JobPriority
	, JobSubmitD
	, JobStartD
	, JobNotifyEmailAddr
	, JobUserLabel
	, JobUserNotes
	, UserLoginNm
	FROM LPE_DERIVE_JOB
    WHERE JobId = @JobId
	IF ( 0!=@@error )
	BEGIN
		RAISERROR('Error in SELECT from Lpe_Derive_Job in GetDeriveJob sp', 16, 1);
		ROLLBACK TRANSACTION
		RETURN -100;
	END

COMMIT TRANSACTION
RETURN 0;