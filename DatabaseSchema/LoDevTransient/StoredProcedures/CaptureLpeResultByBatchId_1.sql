


CREATE PROCEDURE [dbo].[CaptureLpeResultByBatchId_1] 
	@RequestBatchId UniqueIdentifier
AS
	SELECT RequestNumberOfRequestsInBatch, ResultDebugInfo, ResultDoneD
	FROM Lpe_Distribute_Result_1 WITH (NOLOCK)
	WHERE RequestBatchId = @RequestBatchId



