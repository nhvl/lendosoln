


CREATE PROCEDURE [dbo].[CaptureLpeResultByBatchId_3] 
	@RequestBatchId UniqueIdentifier
AS
	SELECT RequestNumberOfRequestsInBatch, ResultDebugInfo, ResultDoneD
	FROM Lpe_Distribute_Result_3 WITH (NOLOCK)
	WHERE RequestBatchId = @RequestBatchId



