


CREATE PROCEDURE [dbo].[CaptureLpeResultByBatchId_8] 
	@RequestBatchId UniqueIdentifier
AS
	SELECT RequestNumberOfRequestsInBatch, ResultDebugInfo, ResultDoneD
	FROM Lpe_Distribute_Result_8 WITH (NOLOCK)
	WHERE RequestBatchId = @RequestBatchId



