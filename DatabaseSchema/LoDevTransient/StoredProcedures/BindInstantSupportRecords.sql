ALTER procedure dbo.BindInstantSupportRecords(@LoginId varchar(100), @FirstName varchar(50), @LastName varchar(50))
as
IF @LoginId IS NULL AND (@FirstName IS NULL) AND (@LastName IS NULL)
	SELECT TOP 300 * From INSTANT_SUPPORT_RECORD
ELSE
	SELECT TOP 300 SupportRecordId, LoginId, FirstName, LastName, SessionStartDate, SessionStartTime
	FROM INSTANT_SUPPORT_RECORD
	WHERE LoginId LIKE @LoginId+'%'
	AND (FirstName LIKE @FirstName+'%')
	AND (LastName LIKE @LastName+'%')
	ORDER BY SupportRecordId DESC
