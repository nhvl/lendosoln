-- =============================================
-- Author:		David Dao
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE dbo.LoanModification_GetUniqueUserIdModifiedByLoan 
    @sLId UniqueIdentifier
AS
BEGIN

select userid, Max(ActionDate) AS LastActionDate FROM Loan_File_Recent_Modification 
WHERE sLId= @sLId
GROUP By UserId
ORDER BY LastActionDate Desc

END
