create procedure RetrieveInstantSupportRecord(@SupportRecordId int)
as
SELECT UserID, LoginID, FirstName, LastName, SessionStartDate, SessionStartTime, LoanNum, CompanyName, CustomerCode, 
	   Email, Phone, IeVersion, Roles, PriceGroupName, PipelineQueryName, AccessLevel, ManagerName, LockDeskName,
	   LenderAeName, UnderwriterName, ProcessorName, CanModifyLoanName, CanDeleteLoan, CanViewLoanInEditor, 
	   CanWriteNonAssignedLoan, AllowEditToProcessor, AllowEditToUnderwriter, ActiveAdmins, PmlId, AdminId
FROM INSTANT_SUPPORT_RECORD
WHERE SupportRecordId = @SupportRecordId
