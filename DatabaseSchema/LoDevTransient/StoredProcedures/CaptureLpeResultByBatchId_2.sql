


CREATE PROCEDURE [dbo].[CaptureLpeResultByBatchId_2] 
	@RequestBatchId UniqueIdentifier
AS
	SELECT RequestNumberOfRequestsInBatch, ResultDebugInfo, ResultDoneD
	FROM Lpe_Distribute_Result_2 WITH (NOLOCK)
	WHERE RequestBatchId = @RequestBatchId



