


CREATE PROCEDURE [dbo].[CaptureLpeResultByBatchId_0] 
	@RequestBatchId UniqueIdentifier
AS
	SELECT RequestNumberOfRequestsInBatch, ResultDebugInfo, ResultDoneD
	FROM Lpe_Distribute_Result_0 WITH (NOLOCK)
	WHERE RequestBatchId = @RequestBatchId



