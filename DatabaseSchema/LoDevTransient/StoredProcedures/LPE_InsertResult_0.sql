




CREATE PROCEDURE [dbo].[LPE_InsertResult_0] 
	@RequestBatchId UniqueIdentifier,
	@RequestNumberOfRequestsInBatch int,
	@ResultDebugInfo varchar(500),
	@LpeRequestIntId BigInt,
	@ResultContent Text = '',
	@RequestExpirationD DateTime
AS
SET TRANSACTION ISOLATION LEVEL READ COMMITTED
BEGIN TRANSACTION
	DECLARE @LockResult Int
	EXEC @LockResult = sp_getapplock @Resource='LPE_RESULT_LOCK_0', @LockMode = 'Exclusive'
	IF @LockResult < 0
	BEGIN
		 RAISERROR('Unable to lock LPE_RESULT_LOCK_0 resource.', 16, 1)
  		RETURN
 	END 

	INSERT INTO LPE_DISTRIBUTE_RESULT_0 (RequestBatchId, RequestNumberOfRequestsInBatch, ResultContent, ResultDebugInfo, RequestExpirationD, LpeRequestIntId)
           VALUES     (@RequestBatchId, @RequestNumberOfRequestsInBatch, @ResultContent, @ResultDebugInfo, @RequestExpirationD, @LpeRequestIntId)



COMMIT TRANSACTION



