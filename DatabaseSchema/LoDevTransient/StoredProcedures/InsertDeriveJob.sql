ALTER    PROCEDURE [dbo].[InsertDeriveJob]
		@JobXmlContent varchar(5000)
		, @JobPriority varchar(100)
		, @JobNotifyEmailAddr varchar(80)
		, @JobUserLabel varchar(50)
		, @JobUserNotes varchar(1000)
		, @UserLoginNm varchar (36)
        , @JobStartAfterD DateTime = NULL
AS
	INSERT INTO LPE_DERIVE_JOB (
		JobXmlContent
		, JobStatus
		, JobPriority
		, JobSubmitD
		, JobNotifyEmailAddr
		, JobUserLabel
		, JobUserNotes
		, UserLoginNm
		, JobStartAfterD
		)
	VALUES (
		@JobXmlContent
		, 'Pending'
		, @JobPriority
		, GetDate()
		, @JobNotifyEmailAddr
		, @JobUserLabel
		, @JobUserNotes
		, @UserLoginNm
		, @JobStartAfterD
		)
	IF 0 != @@ERROR
	BEGIN
		RAISERROR('Error inserting into LPE_DERIVE_JOB in InsertDeriveJob sp', 16, 1);
		RETURN -100
	END
	RETURN 0;
