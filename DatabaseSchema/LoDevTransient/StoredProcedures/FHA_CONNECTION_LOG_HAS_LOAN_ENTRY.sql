/* =============================================
 Author:		Scott Kibler
 Create date: 8/12/2014
 Description:	
   Returns true if the fha_connection_log has any entry for the specified loan.
   Used by loan deletion for QuickPricer 2.0, just to be extra sure we can delete the loan.
  =============================================*/
CREATE PROCEDURE dbo.FHA_CONNECTION_LOG_HAS_LOAN_ENTRY
	@LoanID UniqueIdentifier,
	@HasEntry bit OUTPUT
AS
BEGIN
	DECLARE @RowCount int
	DECLARE @Error int
	DECLARE @ErrorCode int
	DECLARE @ErrorMsg varchar(1000)
	
	SET @HasEntry = 0;
	SET @ErrorCode = -1;
	SET @ErrorMsg = 'Unexpected Error';
	
	
	SELECT @RowCount = COUNT(*) 
	FROM FHA_CONNECTION_LOG
	WHERE 
		LoanId = @LoanID
	IF (0 <> @@Error)
	BEGIN
		SET @ErrorMsg = 'Something bad happened when trying to find the loan in the fha connection log.'
		SET @ErrorCode = -33;
		GOTO ErrorHandler;
	END
	IF(0 <> @RowCount)
	BEGIN
		SET @HasEntry = 1;	
	END
	
	
	RETURN 0;
	ErrorHandler:
		RAISERROR(@ErrorMsg, 16, 1)
		RETURN @ErrorCode;
END
