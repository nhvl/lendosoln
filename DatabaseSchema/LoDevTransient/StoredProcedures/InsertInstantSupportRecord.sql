

CREATE procedure [dbo].[InsertInstantSupportRecord]
					   (@UserId varchar(36), @LoginId varchar(100), @FirstName varchar(50), @LastName varchar(50), 
						@SessionStartDate varchar(10), @SessionStartTime varchar(15), @LoanNum varchar(36),
						@CompanyName varchar(100), @CustomerCode varchar(36), @Email varchar(50), @Phone varchar(15),
						@IeVersion varchar(100), @Roles varchar(600), @PriceGroupName varchar(100), 
						@PipelineQueryName varchar(100), @AccessLevel varchar(23), @ManagerName varchar(100),
						@LockDeskName varchar(100), @LenderAeName varchar(100), @UnderwriterName varchar(100), 
						@ProcessorName varchar(100), @CanModifyLoanName varchar(5), @CanDeleteLoan varchar(5), 
						@CanViewLoanInEditor varchar(5), @CanWriteNonAssignedLoan varchar(5), @AllowEditToProcessor varchar(5), 
						@AllowEditToUnderwriter varchar(5), @ActiveAdmins text, @PmlId varchar(36), @AdminId varchar(36))
as
INSERT INTO INSTANT_SUPPORT_RECORD (UserId, LoginId, FirstName, LastName, SessionStartDate, SessionStartTime, LoanNum,
			CompanyName, CustomerCode, Email, Phone, IeVersion, Roles, PriceGroupName, PipelineQueryName, AccessLevel,
			ManagerName, LockDeskName, LenderAeName, UnderwriterName, ProcessorName, CanModifyLoanName, CanDeleteLoan,
			CanViewLoanInEditor, CanWriteNonAssignedLoan, AllowEditToProcessor, AllowEditToUnderwriter, ActiveAdmins,
			PmlID, AdminID)
values (@UserId, @LoginId, @FirstName, @LastName, @SessionStartDate, @SessionStartTime, @LoanNum, @CompanyName, 
		@CustomerCode, @Email, @Phone, @IeVersion, @Roles, @PriceGroupName, @PipelineQueryName, @AccessLevel,
		@ManagerName, @LockDeskName, @LenderAeName, @UnderwriterName, @ProcessorName, @CanModifyLoanName,
		@CanDeleteLoan, @CanViewLoanInEditor, @CanWriteNonAssignedLoan, @AllowEditToProcessor, @AllowEditToUnderwriter,
		@ActiveAdmins, @PmlId, @AdminId)


