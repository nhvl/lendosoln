-- =============================================
-- Author:		Michael Leinweaver
-- Create date: 4/4/2017
-- Description:	Obtains the loans to process for 
--				the nightly task runner.
-- =============================================
ALTER PROCEDURE [dbo].[TaskNightly_GetLoansForRunner]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT Id, sLId
    FROM TASK_NIGHTLY_UPDATE_LOAN
END
