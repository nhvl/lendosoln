-- =============================================
-- Author:		Antonio Valencia
-- Create date: 1-31-08
-- Description:	Expires a cache entry by updating the time to the curretn time - 1 hour
-- =============================================
CREATE PROCEDURE [dbo].ExpireCacheItem
	 @CacheId varchar(100)

AS
BEGIN

	DECLARE @RowsAffected as int; 
	UPDATE TEXT_TEMP_CACHE 
		SET CacheAutoExpiredD = DATEADD(hour, -1, GETDATE()) 
		WHERE CacheId = @CacheId;
	
	SELECT  @RowsAffected = @@rowcount; 

	IF (@RowsAffected = 0) 
	BEGIN
		    RAISERROR('Error: ExpireCacheItem did not reset the expiration time of any item.', 16, 1);
			RETURN -100
	END
	
	RETURN 0

END
