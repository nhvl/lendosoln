/* =============================================
 Author:		Scott Kibler
 Create date: 8/12/2014
 Description:	
	Deletes the loan from transient:
		LOAN_FILE_RECENT_MODIFICATION
		TASK_NIGHTLY_UPDATE_LOAN
		TASK_TRIGGER_NIGHTLY_UPDATE_LOAN
  =============================================*/
CREATE PROCEDURE dbo.TRANSIENT_DB_DeleteLoanPresence
	@LoanID UniqueIdentifier,
	@IAlreadyDeletedFromRegularDB bit,
	@IWillDeleteFromFileDB bit
AS
BEGIN
	DECLARE @RowCount int
	DECLARE @Error int
	DECLARE @ErrorCode int
	DECLARE @ErrorMsg varchar(1000)
	
	SET @ErrorCode = -1;
	SET @ErrorMsg = 'Unexpected Error';
	
	IF @IAlreadyDeletedFromRegularDB = 0
	BEGIN
		SET @ErrorMsg = 'First you should delete from main DB.'
		SET @ErrorCode = -32;
		GOTO NonTranErrorHandler;
	END
	IF @IWillDeleteFromFileDB = 0
	BEGIN
		SET @ErrorMsg = 'You have to promise to delete from fileDB.'
		SET @ErrorCode = -38;
		GOTO NonTranErrorHandler;
	END
	
	--SELECT @RowCount = COUNT(*) 
	--FROM FHA_CONNECTION_LOG
	--WHERE 
	--	LoanId = @LoanID
	--IF (0 <> @@Error)
	--BEGIN
	--	SET @ErrorMsg = 'Something bad happened when trying to find the loan in the fha connection log.'
	--	SET @ErrorCode = -49;
	--	GOTO NonTranErrorHandler;
	--END
	--IF(0 <> @RowCount)
	--BEGIN
	--	SET @ErrorMsg = 'FHA_CONNECTION_LOG had this loan, and deletion there isnt handled yet.'
	--	SET @ErrorCode = -55;
	--	GOTO NonTranErrorHandler;
	--END
	
	BEGIN TRANSACTION -- DeletionTransaction
		DELETE 
		FROM
			LOAN_FILE_RECENT_MODIFICATION
		WHERE
			sLId = @LoanID
		IF (0 <> @@Error)
		BEGIN
			SET @ErrorMsg = 'Delete from LOAN_FILE_RECENT_MODIFICATION failed.'
			SET @ErrorCode = -68;
			GOTO ErrorHandler;
		END
		
		DELETE 
		FROM
			TASK_NIGHTLY_UPDATE_LOAN
		WHERE
			sLId = @LoanID
		IF (0 <> @@Error)
		BEGIN
			SET @ErrorMsg = 'Delete from TASK_NIGHTLY_UPDATE_LOAN failed.'
			SET @ErrorCode = -80;
			GOTO ErrorHandler;
		END
		
		DELETE 
		FROM
			TASK_TRIGGER_NIGHTLY_UPDATE_LOAN
		WHERE
			sLId = @LoanID
		IF (0 <> @@Error)
		BEGIN
			SET @ErrorMsg = 'Delete from TASK_TRIGGER_NIGHTLY_UPDATE_LOAN failed.'
			SET @ErrorCode = -92;
			GOTO ErrorHandler;
		END
		
	COMMIT TRANSACTION
	RETURN 0;
	ErrorHandler:
		ROLLBACK TRANSACTION
		RAISERROR(@ErrorMsg, 16, 1)
		RETURN @ErrorCode;
	
	RAISERROR('Should not have reached this code.', 16, 1)
	RETURN -104;
	NonTranErrorHandler:
		RAISERROR(@ErrorMsg, 16, 1)
		RETURN @ErrorCode;
END
