


CREATE PROCEDURE [dbo].[CaptureLpeResultByBatchId_4] 
	@RequestBatchId UniqueIdentifier
AS
	SELECT RequestNumberOfRequestsInBatch, ResultDebugInfo, ResultDoneD
	FROM Lpe_Distribute_Result_4 WITH (NOLOCK)
	WHERE RequestBatchId = @RequestBatchId



