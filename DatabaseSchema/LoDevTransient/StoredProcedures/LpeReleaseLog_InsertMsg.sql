
CREATE PROCEDURE [dbo].[LpeReleaseLog_InsertMsg] 
	@SnapshotId varchar(100),
	@Msg varchar(300)
AS
INSERT INTO LPE_RELEASE_LOG   (SnapshotId, Msg ) VALUES (@SnapshotId, @Msg)
