

CREATE TRIGGER [Tr_CreateLpeRequestLogEntry] on [dbo].[LPE_REQUEST]
   AFTER INSERT
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
/*
    -- Insert statements for trigger here
	insert into Lpe_request_log
			( LpeRequestId, LpeRequestBatchId, LpeRequestCreatedDate, LpeRequestExpiredDate, LpeRequestCurrentPartInBatch, LpeRequestNumberOfRequestsInBatch, LpeRequestPriority, LpeRequestType, LpeRequestProcessingExpiredD, BrokerId, InvestorName )
	select 	  LpeRequestId, LpeRequestBatchId, LpeRequestCreatedDate, LpeRequestExpiredDate, LpeRequestCurrentPartInBatch, LpeRequestNumberOfRequestsInBatch, LpeRequestPriority, LpeRequestType, LpeRequestProcessingExpiredD, BrokerId, InvestorName 
	from inserted
*/	

END

