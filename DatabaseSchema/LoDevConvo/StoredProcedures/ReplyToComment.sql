-- =============================================
-- Signature + Skeleton Author: Alan Daughton
-- Implementer: Scott Kibler
-- Create date: 1/11/17
-- Description: ReplyToComment.
-- =============================================
ALTER PROCEDURE [dbo].[ReplyToComment]
	@ParentId bigint,
	@CommenterId bigint,
	@CommentContent varbinary(MAX)
AS
BEGIN
	-- select ConversationId using parent id
	Declare @ConversationId bigint
	SET @ConversationId = -1
	SELECT @ConversationId = ConversationId
	FROM Comment
	WHERE ID = @ParentId
	--!? add error handling if it's still -1

	-- insert comment and record PK	
	DECLARE @InsertedCommentId bigint
	INSERT INTO 
		COMMENT 
		( ConversationID,  CommenterId, Content)
	VALUES
		(@ConversationID, @CommenterId, @CommentContent)
		
	SELECT @InsertedCommentId = SCOPE_IDENTITY()
	
	-- merge child into exisiting comment tree xml using a special function.
	DECLARE @ExistingCommentTreeXml XML
	SELECT 
		@ExistingCommentTreeXml = CommentTree
	FROM 
		Conversation
	WHERE 
		ID = @ConversationID
	
	DECLARE @NewCommentTreeXml XML
	SELECT @NewCommentTreeXml = [dbo].UpdateCommentTreeXmlWithChild(@ExistingCommentTreeXml, @ParentId, @InsertedCommentId)
	
	-- and reset the Conversation Table's Comment Tree to the newly built xml.
	UPDATE Conversation
	Set CommentTree = @NewCommentTreeXml
	WHERE ID = @ConversationId
	
	-- select row from Comment table
	SELECT *
	FROM COMMENT
	WHERE ID = @InsertedCommentId
END