-- =============================================
-- Signature + Skeleton Author: Alan Daughton
-- Implementer: Scott Kibler
-- Create date: 1/11/17
-- Description: InsertOrUpdateCategory.
-- =============================================
ALTER PROCEDURE [dbo].[InsertOrUpdateCategory]
	@OwnerId varchar(100),
	@CategoryName varchar(100),
	@Active bit,
	@DisplayName varchar(100) = NULL, 
	@DefaultPermissionLevelId int
AS
BEGIN
	DECLARE @CategoryId bigint
	
	SELECT 
			@CategoryId = ID
	FROM 
			Category
	WHERE 
			OwnerId = @OwnerID
		AND CategoryName = @CategoryName
	
	IF @CategoryId IS NOT NULL
		BEGIN
			EXEC UpdateCategory @CategoryName=@CategoryName, @Active=@Active, @Id=@CategoryId, @DisplayName=@DisplayName, @DefaultPermissionLevelId=@DefaultPermissionLevelId
			SELECT * FROM CATEGORY WHERE ID = @CategoryId
		END
	ELSE
		BEGIN
			EXEC InsertCategory @OwnerId=@OwnerId, @CategoryName=@CategoryName, @Active=@Active, @DisplayName=@DisplayName,@DefaultPermissionLevelId=@DefaultPermissionLevelId
		END
END