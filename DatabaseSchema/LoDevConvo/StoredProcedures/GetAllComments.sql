-- =============================================
-- Signature + Skeleton Author: Alan Daughton
-- Implementer: Scott Kibler
-- Create date: 1/11/17
-- Description: GetAllComments.
-- =============================================
ALTER PROCEDURE [dbo].[GetAllComments]
	@ResourceType char(2),
	@ResourceId varchar(100)
AS
BEGIN
--returns four tables
	DECLARE @RelevantConversations Table
	(
		ID bigint,
		CategoryId bigint
	)
	INSERT INTO @RelevantConversations
	(ID, CategoryId)
		SELECT 
				ID, CategoryId
		FROM 
				Conversation
		WHERE 
				ResourceType = @Resourcetype
			AND ResourceIdentifier = @ResourceId
	
	
	DECLARE @RelevantComments TABLE
	(
		ID bigint,
		CommenterId bigint,
		HiddenByPersonId bigint
	)
	INSERT INTO @RelevantComments
	(ID, CommenterId, HiddenByPersonId)
		SELECT 
				ID, CommenterId, HiddenByPersonId
		FROM
				Comment
		WHERE
				ConversationId
				IN
				(
					SELECT DISTINCT ID
					FROM @RelevantConversations
				)
	
	-- the first contains the relevant conversations, 
	SELECT * FROM Conversation
	WHERE ID IN
	(
		SELECT ID FROM @RelevantConversations
	)
	
	-- the second the relevant comments.	
	SELECT * FROM Comment
	WHERE ID IN
	(
		SELECT ID FROM @RelevantComments
	)

	-- the third the relevant categories.	
	SELECT 
			c.*
	FROM
			Category c
			LEFT JOIN CategoryOrder co
			ON co.CategoryId = c.ID
	WHERE
			c.ID IN
			(
				SELECT DISTINCT CategoryID
				FROM @RelevantConversations
			)
	ORDER BY
			co.Ordinal ASC
	
	-- the fourth the relevant commenters.
	SELECT 
			*
	FROM
			Commenter
	WHERE
			ID IN
			(
					SELECT DISTINCT CommenterId
					FROM @RelevantComments
				UNION
					SELECT DISTINCT HiddenByPersonId
					FROM @RelevantComments
					WHERE HiddenByPersonId IS NOT NULL
			)
END