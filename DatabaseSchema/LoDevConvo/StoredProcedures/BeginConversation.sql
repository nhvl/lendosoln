-- =============================================
-- Signature + Skeleton Author: Alan Daughton
-- Implementer: Scott Kibler
-- Create date: 1/11/17
-- Description: BeginConversation.
-- =============================================
ALTER PROCEDURE [dbo].[BeginConversation]
	@CategoryId bigint,
	@CommenterId bigint,
	@ResourceType char(2),
	@ResourceId varchar(100),
	@CommentContent varbinary(MAX),
	@PermissionLevelId int
AS
BEGIN
	IF NOT EXISTS ( 
		SELECT * 
		FROM CATEGORY 
		WHERE ID = @CategoryID
		AND Active = 1
	)
	BEGIN
		DECLARE @ErrorMsg VARCHAR(200);
		SET @ErrorMsg = ('No active category with id ' + CAST(@CategoryId AS VARCHAR(100)))
		RAISERROR(@ErrorMsg, 16, 1)
		RETURN -22;
	END
		
	-- insert conversation ('' for CommentTree) and capture PK
	DECLARE @InsertedConversationId bigint
	
	INSERT INTO 
		Conversation 
		(CategoryId, ResourceType, ResourceIdentifier, CommentTree, PermissionLevelId)
	VALUES 
		(@CategoryId, @ResourceType, @ResourceId, '', @PermissionLevelId)
	
	SELECT @InsertedConversationId = SCOPE_IDENTITY()
	
	-- insert comment and capture PK
	DECLARE @InsertedCommentId bigint
	INSERT INTO 
		COMMENT
		(ConversationId, CommenterId, Content)
	VALUES
		(@InsertedConversationId, @CommenterId, @CommentContent)
	SELECT @InsertedCommentId = SCOPE_IDENTITY()

	-- insert comment xml from above into the conversation xml.
	UPDATE Conversation
	SET CommentTree = [dbo].GetCommentXml(@InsertedCommentId)
	WHERE ID = @InsertedConversationId

	-- first return: whole comment row
	SELECT * 
	FROM COMMENT 
	WHERE ID = @InsertedCommentId

	-- second return: whole category row
	SELECT *
	FROM Category
	WHERE ID = @CategoryId
END