-- =============================================
-- Signature + Skeleton Author: Scott Kibler
-- Implementer: Scott Kibler
-- Create date: 1/11/17
-- Description: UpdateCategory.
-- =============================================
ALTER PROCEDURE [dbo].[UpdateCategory]
	@CategoryName varchar(100),
	@Active bit,
	@Id bigint,
	@DisplayName varchar(100) = NULL,
	@DefaultPermissionLevelId int
AS
BEGIN
	UPDATE Category
	SET
		Active = @Active,
		DisplayName = COALESCE(@DisplayName, @CategoryName),
		DefaultPermissionLevelid = @DefaultPermissionLevelId
	WHERE ID = @Id
END