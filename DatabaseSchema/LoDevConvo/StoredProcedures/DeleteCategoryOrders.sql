-- =============================================
-- Signature + Skeleton Author: Alan Daughton
-- Implementer: Scott Kibler
-- Create date: 1/11/17
-- Description: DeleteCategoryOrders.
-- =============================================
ALTER PROCEDURE [dbo].[DeleteCategoryOrders]
	@OwnerId varchar(100)
AS
BEGIN
	DELETE FROM CategoryOrder
	WHERE CategoryId IN
	(
		SELECT ID FROM Category
		WHERE OwnerId = @OwnerId
	)
END