-- =============================================
-- Signature + Skeleton Author: Alan Daughton
-- Implementer: Scott Kibler
-- Create date: 1/11/17
-- Description: InsertCategoryOrder.
-- =============================================
ALTER PROCEDURE [dbo].[InsertCategoryOrder]
	@CategoryId bigint,
	@Ordinal int
AS
BEGIN
	INSERT INTO 
		CategoryOrder (CategoryId, Ordinal)
	VALUES (@CategoryId, @Ordinal)
END