-- =============================================
-- Signature + Skeleton Author: Alan Daughton
-- Implementer: Scott Kibler
-- Create date: 1/11/17
-- Description: InsertOrUpdateCommenter.
-- =============================================
ALTER PROCEDURE [dbo].[InsertOrUpdateCommenter]
	@Identifier varchar(100),
	@Fullname varchar(200)
AS
BEGIN
	-- if Identifier exists update Fullname
	-- else insert
	-- select row
	IF EXISTS(
		SELECT * FROM Commenter
		WHERE Identifier = @Identifier
	)
		BEGIN
			UPDATE Commenter
			SET Fullname = @FullName
			WHERE Identifier = @Identifier
		END
	ELSE
		BEGIN
			INSERT INTO COMMENTER (Identifier, Fullname)
			VALUES (@Identifier, @Fullname)
		END
	
	SELECT * FROM Commenter
	WHERE Identifier = @Identifier
END