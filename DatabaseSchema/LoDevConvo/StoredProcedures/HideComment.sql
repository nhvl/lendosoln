-- =============================================
-- Author: Scott Kibler
-- Create date: 7/24/17
-- Description: Mark the comment as hidden by the person specified.
-- =============================================
ALTER PROCEDURE [dbo].[HideComment]
	@CommentId bigint,
	@PersonId bigint
AS
BEGIN
	UPDATE Comment
	SET HiddenByPersonId = @PersonId
	WHERE ID = @CommentId
END