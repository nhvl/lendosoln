-- =============================================
-- Author: Scott Kibler
-- Create date: 1/11/17
-- Description: GetPermissionLevelIdByCommentId.
-- =============================================
CREATE PROCEDURE [dbo].[GetPermissionLevelIdByCommentId]
	@CommentId bigint
AS
BEGIN
	SELECT conv.PermissionLevelId
	FROM Comment comm
	LEFT JOIN CONVERSATION conv
	on comm.conversationid = conv.id
	WHERE comm.ID = @CommentId
END