-- =============================================
-- Author: Scott Kibler
-- Create date: 7/24/17
-- Description: Mark the comment as not hidden.
-- =============================================
CREATE PROCEDURE [dbo].[ShowComment]
	@CommentId bigint
AS
BEGIN
	UPDATE Comment
	SET HiddenByPersonId = NULL
	WHERE ID = @CommentId
END