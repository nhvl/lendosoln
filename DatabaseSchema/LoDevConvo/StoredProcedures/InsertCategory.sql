-- =============================================
-- Signature + Skeleton Author: Scott Kibler
-- Implementer: Scott Kibler
-- Create date: 1/11/17
-- Description: InsertCategory.
-- =============================================
ALTER PROCEDURE [dbo].[InsertCategory]
	@OwnerId varchar(100),
	@CategoryName varchar(100),
	@Active bit,
	@DisplayName varchar(100) = NULL,
	@DefaultPermissionLevelId int
AS
BEGIN
	DECLARE @NewId bigint
	DECLARE @NewOrdinal INT
	SET @NewOrdinal = [dbo].GetMaxCategoryOrdinalForOwner(@OwnerId) + 1
	
	INSERT INTO 
		CATEGORY (OwnerId, CategoryName, Active, DisplayName, DefaultPermissionLevelId)
	VALUES 
		(@OwnerId, @CategoryName, @Active, COALESCE(@DisplayName, @CategoryName), @DefaultPermissionLevelId)
		
	SELECT @NewId = SCOPE_IDENTITY()
		
	EXEC InsertCategoryOrder @CategoryId = @NewId, @Ordinal= @NewOrdinal
	
	SELECT * FROM CATEGORY
	WHERE ID = @NewId
END