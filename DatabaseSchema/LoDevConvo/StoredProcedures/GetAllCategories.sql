-- =============================================
-- Signature + Skeleton Author: Alan Daughton
-- Implementer: Scott Kibler
-- Create date: 1/11/17
-- Description: GetAllCategories.
-- =============================================
ALTER PROCEDURE [dbo].[GetAllCategories]
	@OwnerId varchar(100)
AS
BEGIN
	-- select all join with CategoryOrder table and order by the Ordinal column
	SELECT 
			c.* 
	FROM
			Category c
		LEFT JOIN
			CategoryOrder co
		ON
			c.Id = co.CategoryId
	WHERE 
		c.OwnerId = @OwnerId
	ORDER BY 
		co.ordinal 
		ASC
END