-- =============================================
-- Author: Scott Kibler
-- Create date: 1/16/17
-- Description: UpdateCommentTreeXmlWithChild
-- NOTE: this function depends on the comment tree xml format.  If that changes, this needs to change.
-- Inserts the child xml node into the xml tree at the provided parent id, as the last child of the parent.
-- Allows inserting xml into xml, specifically tailored to the conversation log feature.
-- The reason is that SQL 2005 gives error when trying to insert xml into xml.
-- See https://blogs.msdn.microsoft.com/denisruc/2006/05/17/inserting-an-xml-instance-into-another-even-though-sql-type-xml-is-not-supported-in-xquery/
-- The approach to erroring comes from here: http://stackoverflow.com/a/4681815/420667
-- =============================================
ALTER FUNCTION [dbo].UpdateCommentTreeXmlWithChild
(
	@CommentTreeXml XML, 
	@ParentId bigint,
	@ChildId bigint
)
RETURNS XML
AS
BEGIN
	DECLARE @Msg varchar(3000)
	DECLARE @ErrorInt INT -- will be set on error.
	DECLARE @ChildXml XML
	
	IF @ChildId IS NULL
	BEGIN
		set @Msg = '@ChildId was null.'
		set @ErrorInt = cast(@msg as int);
	END
	
	IF @ParentId IS NULL
	BEGIN
		set @Msg = '@ParentId was null.'
		set @ErrorInt = cast(@msg as int);
	END
	
	IF (@CommentTreeXml.exist('//C[@id=sql:variable("@ParentId")]') = 0)
	BEGIN
		set @Msg = 'Could not find child with id ' + cast(@ParentId as varchar(1000))+ ' in the comment tree xml.'
		set @ErrorInt = cast(@msg as int);
	END
	
	IF (@CommentTreeXml.exist('/*[2]') = 1)
	BEGIN
		set @Msg = 'The comment tree xml has a second child, which is unexpected.'
		set @ErrorInt = cast(@msg as int);
	END
	
	IF (@CommentTreeXml.exist('//C[@id=sql:variable("@ChildId")]') = 1)
	BEGIN
		set @Msg = 'CommentTreeXml already had comment with id ' + cast(@ChildId as varchar(1000))+ ', so cant add another.'
		set @ErrorInt = cast(@msg as int);
	END
	
	SET @ChildXml = [dbo].GetCommentXml(@ChildId)
	
	IF (@ChildXml is null)
	BEGIN
		Set @Msg = 'ChildXml was null'
		set @ErrorInt = cast(@msg as int);
	END

	    
	DECLARE @x XML
	SET @x = CONVERT(XML, CONVERT(varchar(MAX), @CommentTreeXml) + (CONVERT(varchar(MAX), @ChildXml)))

	SET @x.modify('insert /*[2] as last into (//C[@id=sql:variable("@ParentId")])[1]')
	SET @x.modify('delete /*[2]')

RETURN @x
END
GO