-- =============================================
-- Author: Scott Kibler
-- Create date: 1/14/17
-- Description: GetMaxCategoryOrdinalForOwner
-- Gets the max ordinal for any new category being added, or 0 if none exists.
-- =============================================
ALTER FUNCTION [dbo].[GetMaxCategoryOrdinalForOwner](
	@OwnerId varchar(100)
)
RETURNS INT
AS
BEGIN
	DECLARE @MaxOrdinal INT
	SELECT @MaxOrdinal = ISNULL(MAX(Ordinal), 0)
	FROM CategoryOrder
	WHERE CategoryId in
	(
		SELECT ID
		FROM Category
		WHERE OwnerId = @OwnerId
	)
	RETURN @MaxOrdinal
END