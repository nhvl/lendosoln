-- =============================================
-- Author: Scott Kibler
-- Create date: 1/16/17
-- Description: GetCommentXml
-- Just a function to wrap the comment id into it's xml node.
-- =============================================
ALTER FUNCTION [dbo].[GetCommentXml](
	@CommentId bigint
)
RETURNS XML
AS
BEGIN
	DECLARE @Msg varchar(3000)
	DECLARE @ErrorInt INT -- will be set on error.
	IF @CommentId IS NULL
	BEGIN
		set @Msg = '@CommentId was null.'
		set @ErrorInt = cast(@msg as int);
	END
	
	Declare @Xml XML
	Set @XML = (
	SELECT @CommentId as "@id" -- @ creates attribute.
	FOR XML PATH('C'))
	
	RETURN @Xml
END