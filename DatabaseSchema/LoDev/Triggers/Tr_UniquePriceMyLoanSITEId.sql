
CREATE trigger Tr_UniquePriceMyLoanSITEId
on broker_user
after insert, update
as
	if( update ( PmlSiteId ) )
	begin
		declare @Cnt as integer;
		select @Cnt = count( * )
		from broker_user
		where PmlSiteId in ( select PmlSiteId from inserted where PmlSiteId is not null )
		group by PmlSiteId 
		having count( * ) > 1
		
		if( 0!=@@error )
			rollback transaction

		if( @Cnt > 0 )
		begin 
			RAISERROR('PmlSiteId in Broker_User table must be unique', 16, 1);
			rollback transaction
		end

	end



