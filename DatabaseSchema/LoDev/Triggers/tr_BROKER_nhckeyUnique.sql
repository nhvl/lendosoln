


CREATE  trigger tr_BROKER_nhckeyUnique on broker
for update, insert
as
	if( update ( nhckey ) )
	begin
		declare @maxCount int;
		select @maxCount = max(tally)
		from (select count(*)
		from broker
		where nhckey is not null
		group by nhckey ) as x( tally )
		if( @maxCount > 1 )
		begin
			RAISERROR('Broker.NHCKey must be either null or unique', 16, 1);;
			rollback transaction
		end
	end




