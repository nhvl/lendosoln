






CREATE         TRIGGER [dbo].[PopulateLoanNameChange] ON [dbo].[LOAN_FILE_E]
FOR INSERT, UPDATE 
AS

declare @bUpdate as bit

if( update( sLNm ) )
begin
	set @bUpdate = 1
end
else if( update( sOldLNm ) )
begin
	set @bUpdate = 1
end

if( 1 = @bUpdate )
begin 
	update integration_file_modified
	set sLNm = ins.sLNm, sOldLNm = ins.sOldLNm
	from inserted ins
	where integration_file_modified.sLid = ins.slid;
	
	update loan_file_cache
	set sLNm = ins.sLNm
	from inserted ins
	where loan_file_cache.sLid = ins.slid;
	
	update loan_file_cache_2
	set sOldLNm = ins.sOldLNm
	from inserted ins
	where loan_file_cache_2.sLid = ins.slid;

	update discussion_log
	set DiscRefObjNm1 = ins.sLNm
	from inserted ins
	where DiscRefObjId = ins.slid;
end






