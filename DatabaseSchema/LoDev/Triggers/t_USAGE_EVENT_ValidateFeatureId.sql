
CREATE Trigger t_USAGE_EVENT_ValidateFeatureId
on USAGE_EVENT after insert, update
as
	if( update( FeatureId ) )
	begin
		if exists( 	select usageEventId
				from inserted
				where ( FeatureId != '00000000-0000-0000-0000-000000000000' )
				and FeatureId not in ( select FeatureId from Feature ) )
		begin
			RAISERROR('Invalid FeatureId value', 16, 1);
			rollback transaction
			return;
		end	
	end



