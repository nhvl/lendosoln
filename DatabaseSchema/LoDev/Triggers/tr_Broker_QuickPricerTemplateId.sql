
CREATE trigger [tr_Broker_QuickPricerTemplateId]
on [dbo].[BROKER]
for update, insert
as
if ( update( QuickPricerTemplateId ) and exists ( 
	select ins.QuickPricerTemplateId
	from inserted ins left join loan_file_cache loanFile on
		ins.QuickPricerTemplateId = loanFile.slid AND 
		ins.BrokerId = loanFile.sBrokerId

	where ins.IsQuickPricerEnable = 1 and ins.PmlLoanTemplateId is not null and (loanFile.slid is null or loanFile.IsTemplate = 0)
) )
begin
			RAISERROR('Broker.QuickPricerTemplateId must point to a template file of the same corporate.', 16, 1);;
			rollback transaction
end
