-- =============================================
-- Author:		Geoffrey Feltman
-- Create date: 2/26/13
-- Description:	Populate INTEGRATION_FILE_MODIFIED when cache updated.
-- =============================================
CREATE TRIGGER [dbo].[PopulateInfoChangeForIntegrationFile_LFC3] 
   ON  [dbo].[LOAN_FILE_CACHE_3]
   AFTER INSERT, UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for trigger here
	if (UPDATE(sEmployeeShipperId)
		OR UPDATE(sEmployeeFunderId)
		OR UPDATE(sEmployeePostCloserId)
		OR UPDATE(sEmployeeInsuringId)
		OR UPDATE(sEmployeeCollateralAgentId)
		OR UPDATE(sEmployeeDocDrawerId)
		OR UPDATE(sEmployeeCreditAuditorId)
		OR UPDATE(sEmployeeDisclosureDeskId)
		OR UPDATE(sEmployeeJuniorProcessorId)
		OR UPDATE(sEmployeeJuniorUnderwriterId)
		OR UPDATE(sEmployeeLegalAuditorId)
		OR UPDATE(sEmployeeLoanOfficerAssistantId)
		OR UPDATE(sEmployeePurchaserId)
		OR UPDATE(sEmployeeQCComplianceId)
		OR UPDATE(sEmployeeSecondaryId)
		OR UPDATE(sEmployeeServicingId)
		)
	BEGIN
		UPDATE INTEGRATION_FILE_MODIFIED
		SET sEmployeeShipperId = ins.sEmployeeShipperId
			, sEmployeeFunderId = ins.sEmployeeFunderId
			, sEmployeePostCloserId = ins.sEmployeePostCloserId
			, sEmployeeInsuringId = ins.sEmployeeInsuringId
			, sEmployeeCollateralAgentId = ins.sEmployeeCollateralAgentId
			, sEmployeeDocDrawerId = ins.sEmployeeDocDrawerId
			, sEmployeeCreditAuditorId = ins.sEmployeeCreditAuditorId
			, sEmployeeDisclosureDeskId = ins.sEmployeeDisclosureDeskId
			, sEmployeeJuniorProcessorId = ins.sEmployeeJuniorProcessorId
			, sEmployeeJuniorUnderwriterId = ins.sEmployeeJuniorUnderwriterId
			, sEmployeeLegalAuditorId = ins.sEmployeeLegalAuditorId
			, sEmployeeLoanOfficerAssistantId = ins.sEmployeeLoanOfficerAssistantId
			, sEmployeePurchaserId = ins.sEmployeePurchaserId
			, sEmployeeQCComplianceId = ins.sEmployeeQCComplianceId
			, sEmployeeSecondaryId = ins.sEmployeeSecondaryId
			, sEmployeeServicingId = ins.sEmployeeServicingId
		FROM inserted ins
		WHERE INTEGRATION_FILE_MODIFIED.sLId = ins.sLId
	END		
END
