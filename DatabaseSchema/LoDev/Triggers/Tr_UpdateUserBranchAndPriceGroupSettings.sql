-- =============================================
-- Author:		Geoff Feltman
-- Create date: 11/11/14
-- Description:	Update the applicable Employee branches and Broker_User price 
-- groups when the PML Broker is updated.
-- =============================================
ALTER TRIGGER dbo.Tr_UpdateUserBranchAndPriceGroupSettings
   ON [dbo].[PML_BROKER]
   AFTER UPDATE
AS 
BEGIN
	-- Branch Settings
	IF UPDATE (BranchId)
	BEGIN
		UPDATE EMPLOYEE
		SET BranchId = ins.BranchId
		FROM INSERTED ins JOIN BROKER_USER bu ON ins.PmlBrokerId = bu.PmlBrokerId
			JOIN EMPLOYEE e ON bu.EmployeeId = e.EmployeeId
		WHERE e.UseOriginatingCompanyBranchId = 1
	END
	
	IF UPDATE (MiniCorrespondentBranchId)
	BEGIN
		UPDATE EMPLOYEE
		SET MiniCorrespondentBranchId = ins.MiniCorrespondentBranchId
		FROM INSERTED ins JOIN BROKER_USER bu ON ins.PmlBrokerId = bu.PmlBrokerId
			JOIN EMPLOYEE e ON bu.EmployeeId = e.EmployeeId
		WHERE e.UseOriginatingCompanyMiniCorrBranchId = 1
	END

	IF UPDATE (CorrespondentBranchId)
	BEGIN
		UPDATE EMPLOYEE
		SET CorrespondentBranchId = ins.CorrespondentBranchId
		FROM INSERTED ins JOIN BROKER_USER bu ON ins.PmlBrokerId = bu.PmlBrokerId
			JOIN EMPLOYEE e ON bu.EmployeeId = e.EmployeeId
		WHERE e.UseOriginatingCompanyCorrBranchId = 1
	END
	
	-- Price Group Settings
	IF UPDATE (LpePriceGroupId)
	BEGIN
		UPDATE BROKER_USER
		SET LpePriceGroupId = ins.LpePriceGroupId
		FROM INSERTED ins JOIN BROKER_USER bu ON ins.PmlBrokerId = bu.PmlBrokerId
		WHERE bu.UseOriginatingCompanyLpePriceGroupId = 1
	END
	
	IF UPDATE (MiniCorrespondentLpePriceGroupId)
	BEGIN
		UPDATE BROKER_USER
		SET MiniCorrespondentPriceGroupId = ins.MiniCorrespondentLpePriceGroupId
		FROM INSERTED ins JOIN BROKER_USER bu ON ins.PmlBrokerId = bu.PmlBrokerId
		WHERE bu.UseOriginatingCompanyMiniCorrPriceGroupId = 1
	END

	IF UPDATE(CorrespondentLpePriceGroupId)
	BEGIN
		UPDATE BROKER_USER
		SET CorrespondentPriceGroupId = ins.CorrespondentLpePriceGroupId
		FROM INSERTED ins JOIN BROKER_USER bu ON ins.PmlBrokerId = bu.PmlBrokerId
		WHERE bu.UseOriginatingCompanyCorrPriceGroupId = 1
	END
END
