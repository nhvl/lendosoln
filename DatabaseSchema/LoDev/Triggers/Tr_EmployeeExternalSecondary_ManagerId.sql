-- =============================================
-- Author:		Geoff Feltman
-- Create date: 11/13/14
-- Description:	Update the external secondary supervisor on file when the role
-- assignment is updated.
-- =============================================
CREATE TRIGGER [dbo].[Tr_EmployeeExternalSecondary_ManagerId]
   ON  [dbo].[LOAN_FILE_CACHE_2]
   AFTER INSERT, UPDATE
AS
IF UPDATE (sEmployeeExternalSecondaryId)
BEGIN
	UPDATE LOAN_FILE_CACHE_2
	SET LOAN_FILE_CACHE_2.PmlExternalSecondaryManagerEmployeeId = bu.PmlExternalManagerEmployeeId
	FROM INSERTED ins JOIN LOAN_FILE_CACHE_2 ON ins.sLId = LOAN_FILE_CACHE_2.sLId
		LEFT JOIN BROKER_USER bu ON LOAN_FILE_CACHE_2.sEmployeeExternalSecondaryId = bu.EmployeeId
END
