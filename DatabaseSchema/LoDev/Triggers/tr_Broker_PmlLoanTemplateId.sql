CREATE trigger tr_Broker_PmlLoanTemplateId
on broker
for update, insert
as

if ( update( PmlLoanTemplateId ) and exists ( 
	select ins.PmlLoanTemplateId
	from inserted ins left join loan_file_cache loanFile on
		ins.PmlLoanTemplateId = loanFile.slid AND 
		ins.BrokerId = loanFile.sBrokerId

	where ins.PmlLoanTemplateId is not null and loanFile.slid is null or IsTemplate = 0
) )
begin
			RAISERROR('Broker.PmlLoanTemplateId must point to a template file of the same corporate.', 16, 1);;
			rollback transaction
end
