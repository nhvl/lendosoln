-- =============================================
-- Author:		Matthew Pham
-- Create date: 9/25/2012
-- Description:	When an edoc is invalidated, all the
--     associations with conditions should be
--     deleted as well.
-- av removed insert since when is a brand new going to have any associations
-- =============================================
CREATE TRIGGER [dbo].[Tr_DeleteAssociationsOnDocumentInvalidation]
ON [dbo].[EDOCS_DOCUMENT]
AFTER UPDATE
AS
BEGIN
	IF EXISTS (SELECT COUNT(1) FROM TASK_x_EDOCS_DOCUMENT as TED
		JOIN INSERTED as I
		ON TED.DocumentId = I.DocumentId
		WHERE I.IsValid = 0)
	BEGIN
	  DELETE TASK_x_EDOCS_DOCUMENT
	  FROM TASK_x_EDOCS_DOCUMENT as TED JOIN INSERTED as I
		ON TED.DocumentId = I.DocumentId
	  WHERE I.IsValid = 0
	  RETURN
	END
END