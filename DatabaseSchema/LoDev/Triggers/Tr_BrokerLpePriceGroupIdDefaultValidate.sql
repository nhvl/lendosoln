

CREATE   trigger Tr_BrokerLpePriceGroupIdDefaultValidate on broker
after insert, update
as
if update( BrokerLpePriceGroupIdDefault )
begin
   
   if exists ( select ins.BrokerLpePriceGroupIdDefault
	--ExternalPriceGroupEnabled
   	from 
		inserted ins 
		join lpe_price_group gr on ins.BrokerLpePriceGroupIdDefault = gr.LpePriceGroupId
		--deleted del
		--join deleted del on ins.brokerId = del.brokerId 
   	where 	ins.brokerId <> gr.brokerId -- this should never be the case
		or 
		( gr.ExternalPriceGroupEnabled = 0 
/*
		  and -- it's ok if null overwrites null
		  NOT( ins.BranchLpePriceGroupIdDefault is null and del.BranchLpePriceGroupIdDefault is null )
		  and -- it's ok to overwrites the same non-null value
		  not( ins.BranchLpePriceGroupIdDefault is not null and del.BranchLpePriceGroupIdDefault is not null and ins.BranchLpePriceGroupIdDefault = del.BranchLpePriceGroupIdDefault )
*/
		) )
		--or ( del.brokerId = ins.brokerId and ( del.BrokerLpePriceGroupIdDefault is not null and ins.BrokerLpePriceGroupIdDefault is null ) ) )
   begin
	RAISERROR('Error. Disabled price group is not allowed as broker default AND price group of one broker cannot be default of another broker', 16, 1);
	rollback transaction
   end



end



