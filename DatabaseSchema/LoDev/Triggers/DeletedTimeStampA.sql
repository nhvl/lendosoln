



CREATE      trigger DeletedTimeStampA
on dbo.LOAN_FILE_A
for update
as 
IF NOT UPDATE(IsValid)
 RETURN
update loan_file_a with( rowlock )
SET DeletedD = getDate()
FROM loan_file_a s with(rowlock), inserted i, deleted d
where s.sLId = i.sLId and I.sLId = d.sLId and d.IsValid = 1 and i.IsValid = 0

update loan_file_cache with( rowlock )
set IsValid = i.IsValid
from loan_file_cache LCache with(rowlock), inserted i
where LCache.sLId = i.sLId






