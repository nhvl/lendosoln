


CREATE trigger [dbo].[Tr_ManageImageCreationQueuingAndProcessingStatistics]
on [dbo].[EDOCS_DOCUMENT]
after update
as

-- BY THINH NGUYEN-KHOA 5/19/2011
-- these dates are recorded and maintainted so we know when our distributed async 
-- model for generating eDoc images needs to be improved or more hardware/servers
-- need to be added

if( update (ImageStatus) )
begin

	-- If imageStatus becomes 1 (Image generation is enqueued), then record the date
	update edocs_document
	set TimeImageCreationEnqueued = getdate()
	where DocumentId in (
		select ins.documentId 
		from inserted ins join deleted del on ins.DocumentId = del.DocumentId
		where ins.ImageStatus = 1 and ins.ImageStatus != del.ImageStatus );

	-- If imageStatus becomes 2 (Image generation is completed), then record the date
	update edocs_document
	set TimeImageCreationCompleted = getdate()
	where DocumentId in (
		select ins.documentId 
		from inserted ins join deleted del on ins.DocumentId = del.DocumentId
		where ins.ImageStatus = 2 and ins.ImageStatus != del.ImageStatus );

	-- If imageStatus becomes 0 (No Image, Not in Queue) or 3 (Error), then erase
	-- the TimeImageCreationEnqueued, TimeImageCreationStarted and TimeImageCreationCompleted
	update edocs_document
	set TimeImageCreationEnqueued = null,TimeImageCreationStarted = null, TimeImageCreationCompleted = null
	where DocumentId in (
		select ins.documentId 
		from inserted ins join deleted del on ins.DocumentId = del.DocumentId
		where ins.ImageStatus in (0,3) and ins.ImageStatus != del.ImageStatus );
end

if( update(TimeToRequeueForImageGeneration) )
begin
    -- Record the start date when the row receives a new future requeue time
	-- this is right after the processor takes the request out of the queue.
	-- Processor in that case extends out the requeue time to avoid other code
	-- queue it again when it's being processed. 
	update edocs_document
	set TimeImageCreationStarted = getdate()
	where DocumentId in (
		select ins.documentId 
		from inserted ins join deleted del on ins.DocumentId = del.DocumentId
		where del.TimeToRequeueForImageGeneration is not null 
			and 
			ins.TimeToRequeueForImageGeneration is not null
			and 
			ins.TimeToRequeueForImageGeneration != del.TimeToRequeueForImageGeneration );
end



