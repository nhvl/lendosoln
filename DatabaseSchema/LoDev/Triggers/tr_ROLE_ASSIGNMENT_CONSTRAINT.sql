-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE TRIGGER [dbo].[tr_ROLE_ASSIGNMENT_CONSTRAINT] 
   ON  [dbo].[ROLE_ASSIGNMENT]
   FOR INSERT,UPDATE
AS 
BEGIN


SELECT 1 
FROM inserted i JOIN Employee e ON i.EmployeeId = e.EmployeeId
                JOIN All_User a ON e.EmployeeUserId = a.UserId
WHERE (a.Type = 'B' AND (i.RoleId = 'B3785253-D9E8-4A3F-B0EC-D02D6FCF8538' OR i.RoleId = '274981E4-8C18-4204-B97B-2F537922CCD9' OR i.RoleId = '89811C63-6A28-429A-BB45-3152449D854E')) -- BrokerProcessor and correspondent roles only applicable to 'P'
   OR (a.Type = 'P' AND i.RoleId NOT IN ('86C86CF3-FEB6-400F-80DC-123363A79605', '6DFC9B0E-11E3-48C1-B410-A1B0B5363B69', 'B3785253-D9E8-4A3F-B0EC-D02D6FCF8538', '274981E4-8C18-4204-B97B-2F537922CCD9', '89811C63-6A28-429A-BB45-3152449D854E')) -- P user can only have Admin, Loan Officer and Broker Process Role and correspondent roles.
   OR (a.Type = 'I')

IF @@ROWCOUNT = 1
BEGIN
    RAISERROR('Cannot UPDATE ROLE_ASSIGNMENT because Role is conflict with User Type', 16, 1);
    rollback transaction
END

END
