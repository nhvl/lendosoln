-- =============================================
-- Author:		Antonio Valencia
-- Create date: 12/20/2011
-- Description:	Updates field on change
-- =============================================
CREATE TRIGGER [dbo].[UpdatesAuditTrackedFieldChanges]
   ON  [dbo].[LOAN_FILE_CACHE]
   FOR UPDATE
AS 
BEGIN



		IF NOT UPDATE(sStatusT) 
		RETURN 
		
		SELECT sStatusT 
		FROM INSERTED 
		WHERE sStatusT NOT IN
		(
			0,  -- Loan Open
			12, -- Lead New
			15, -- Lead Canceled
			16, -- Lead Declined
			17, -- Lead Other
			18, -- Loan Other
			14 -- Web Consumer
		)
		
		IF @@rowcount = 0 
		BEGIN
		return
		END
		
		DECLARE @sLId uniqueidentifier
		SET @sLId  = (SELECT sLId FROM inserted)


		UPDATE LOAN_FILE_A
		SET sAuditTrackedFieldChanges = 1
		WHERE sLId = @sLId
    -- Insert statements for trigger here

END
