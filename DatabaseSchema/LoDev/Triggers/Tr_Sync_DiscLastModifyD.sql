
create trigger Tr_Sync_DiscLastModifyD
on discussion_log

after update
as 
	if (update(DiscLastModifyD))
	begin
		update discussion_notification
		set DiscLastModifyD = i.DiscLastModifyD
		from discussion_notification d join inserted i on d.disclogid = i.disclogid
		--where DiscLogId = inserted
		
	end



