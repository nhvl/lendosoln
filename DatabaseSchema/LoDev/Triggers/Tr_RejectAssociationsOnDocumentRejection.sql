CREATE TRIGGER Tr_RejectAssociationsOnDocumentRejection
ON EDOCS_DOCUMENT
AFTER  UPDATE
AS
BEGIN
	-- If there are any associations connected to a document that is rejected
	-- Then set the status of those associations to 'Unsatisfied'
	IF EXISTS (SELECT COUNT(1) FROM TASK_x_EDOCS_DOCUMENT as TED
		JOIN INSERTED as I
		ON TED.DocumentId = I.DocumentId
		WHERE I.status = 3) -- rejected
	BEGIN
	  UPDATE TASK_x_EDOCS_DOCUMENT
	  SET status = 2 -- Unsatisfied
	  FROM TASK_x_EDOCS_DOCUMENT as TED JOIN INSERTED as I
		ON TED.DocumentId = I.DocumentId
	  WHERE I.status=3
	  RETURN
	END
END