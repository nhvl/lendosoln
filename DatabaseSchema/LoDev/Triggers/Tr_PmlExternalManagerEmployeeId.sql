
CREATE trigger [dbo].[Tr_PmlExternalManagerEmployeeId] on [dbo].[BROKER_USER]
after insert, update
as

if update( PmlExternalManagerEmployeeId )
begin
	IF ( EXISTS( SELECT  1  from inserted ins, deleted WHERE ins.PmlExternalManagerEmployeeId <> deleted.PmlExternalManagerEmployeeId  OR (ins.PmlExternalManagerEmployeeId IS NULL AND deleted.PmlExternalManagerEmployeeId IS NOT NULL) OR (deleted.PmlExternalManagerEmployeeId IS NULL AND ins.PmlExternalManagerEmployeeId IS NOT NULL)
) )
	BEGIN
--                update loan_file_cache 
--                set loan_file_cache.PmlExternalManagerEmployeeId = CASE 
--                                 WHEN  (ins.employeeid = loan_file_cache.sEmployeeLoanRepId)
--                                  THEN ins.PmlExternalManagerEmployeeId
--                                 ELSE  loan_file_cache.PmlExternalManagerEmployeeId 
--								END
--                , loan_file_cache.PmlExternalBrokerProcessorManagerEmployeeId =  CASE
--                                 WHEN (ins.employeeid = loan_file_cache.sEmployeeBrokerProcessorId ) 
--                                 THEN ins.PmlExternalManagerEmployeeId
--								 ELSE loan_file_cache.PmlExternalBrokerProcessorManagerEmployeeId 
--								END
--				from inserted ins 
--                where 
--					
--					loan_file_cache.sEmployeeLoanRepId = ins.employeeid OR 
--					loan_file_cache.sEmployeeBrokerProcessorId = ins.employeeid

	update loan_file_cache
	set loan_file_cache.PmlExternalManagerEmployeeId = ins.PmlExternalManagerEmployeeId
	from inserted ins
	where ins.employeeid = loan_file_cache.sEmployeeLoanRepId
	
	update loan_file_cache
	set loan_file_cache.PmlExternalBrokerProcessorManagerEmployeeId = ins.PmlExternalManagerEmployeeId
	from inserted ins
	where ins.employeeid = loan_file_cache.sEmployeeBrokerProcessorId
	
	update loan_file_cache_2
	set loan_file_cache_2.PmlExternalSecondaryManagerEmployeeId = ins.PmlExternalManagerEmployeeId
	from inserted ins
	where ins.employeeid = loan_file_cache_2.sEmployeeExternalSecondaryId
	
	update loan_file_cache_2
	set loan_file_cache_2.PmlExternalPostCloserManagerEmployeeId = ins.PmlExternalManagerEmployeeId
	from inserted ins
	where ins.employeeid = loan_file_cache_2.sEmployeeExternalPostCloserId
	
	END
END

