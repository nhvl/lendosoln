
/****** Object:  Trigger dbo.tU_BROKER    Script Date: 9/30/2002 4:16:33 PM ******/
/****** Object:  Trigger dbo.tU_BROKER    Script Date: 9/27/2002 7:39:55 PM ******/
CREATE  trigger [dbo].[tU_BROKER] on [dbo].[BROKER] for UPDATE as
/* ERwin Builtin Thu Jun 06 15:31:41 2002 */
/* UPDATE trigger on BROKER */
begin
  declare  @numrows int,
           @nullcnt int,
           @validcnt int,
           @insBrokerId GUID,
           @errno   int,
           @errmsg  varchar(255)
  select @numrows = @@rowcount


  /* ERwin Builtin Thu Jun 06 15:31:41 2002 */
  /* BROKER <has> CC_TEMPLATE ON PARENT UPDATE RESTRICT */
  if
    /* %ParentPK(" or",update) */
    update(BrokerId)
  begin
    if exists (
      select * from deleted,CC_TEMPLATE
      where
        /*  %JoinFKPK(CC_TEMPLATE,deleted," = "," and") */
        CC_TEMPLATE.BrokerId = deleted.BrokerId
    )
    begin
      select @errno  = 30005,
             @errmsg = 'Cannot UPDATE BROKER because CC_TEMPLATE exists.'
      goto error
    end
  end

  /* ERwin Builtin Thu Jun 06 15:31:41 2002 */
  /* BROKER <has> BRANCH ON PARENT UPDATE RESTRICT */
  if
    /* %ParentPK(" or",update) */
    update(BrokerId)
  begin
    if exists (
      select * from deleted,BRANCH
      where
        /*  %JoinFKPK(BRANCH,deleted," = "," and") */
        BRANCH.BrokerId = deleted.BrokerId
    )
    begin
      select @errno  = 30005,
             @errmsg = 'Cannot UPDATE BROKER because BRANCH exists.'
      goto error
    end
  end
  /* ERwin Builtin Thu Jun 06 15:31:41 2002 */
  return
error:
    RAISERROR(@errmsg, 16, 1);
    rollback transaction
end
