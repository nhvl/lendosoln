CREATE TRIGGER [dbo].[ValidateUniqueMclUserId]
ON [dbo].[ALL_USER]
FOR INSERT, UPDATE
AS
begin
	if not update(MclUserId)
		return

	Declare @UserId UniqueIdentifier
	Declare @MclUserId Int
	SELECT @MclUserId = MclUserId, @UserId = UserId FROM inserted

	IF @McluserId = 0
		RETURN

	IF EXISTS (SELECT 1 FROM All_User WHERE MclUserId = @MclUserId AND UserId <> @UserId)
	BEGIN
		declare @errno   int
           		declare @errmsg  varchar(255)
	     	 select @errno  = 50000,
	            		 @errmsg = 'MclUserId is duplicate'
	     	 RAISERROR(@errmsg, 16, 1);
		rollback transaction
	END
end