


CREATE    trigger Tr_ExternalPriceGroupEnabledDefaultProtection
on lpe_price_group
after update, insert
as
if update( ExternalPriceGroupEnabled )
begin
	if exists ( 
		select LpePriceGroupId 
		from inserted ins join broker brok 
			on ins.lpePriceGroupId = brok.BrokerLpePriceGroupIdDefault
		where ins.ExternalPriceGroupEnabled = 0 )
	begin
		RAISERROR('Broker default pricing group cannot be disabled', 16, 1);
		rollback transaction
	end

	if exists ( 
		select LpePriceGroupId 
		from inserted ins join branch bran
			on ins.lpePriceGroupId = bran.BranchLpePriceGroupIdDefault
		where ins.ExternalPriceGroupEnabled = 0 )
	begin
		RAISERROR('Branch default pricing group cannot be disabled', 16, 1);
		rollback transaction
	end
end



