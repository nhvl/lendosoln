



CREATE       TRIGGER CheckEmptyLoanName ON dbo.LOAN_FILE_E
FOR INSERT, UPDATE 
AS
if( update( sLNm ) )
begin 
	if exists ( select * from inserted where sLNm = '' )
	begin 
	   declare @msg varchar(500)
	   set @msg = 'Empty loan name is not allowed'
	   RAISERROR(@msg, 16, 1);
	   rollback transaction
	end
end









