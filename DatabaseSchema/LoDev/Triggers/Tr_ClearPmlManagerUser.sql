

CREATE trigger [dbo].[Tr_ClearPmlManagerUser] on [dbo].[EMPLOYEE]
for update
as 

--if( update( IsPmlManager ) )
-- 02.21.12 mf.  Only do the update if a PML user is updated.  LO user do not have PML Supervisor.
if( update( IsPmlManager ) 
 and ( 
	exists ( 
				select userid from View_Active_PML_User 
				join inserted ins on UserId = ins.EmployeeUserId
				where PmlExternalManagerEmployeeId = ins.employeeid
			)
	OR
	exists ( 
				select userid from View_Disabled_PML_User 
				join inserted ins on UserId = ins.EmployeeUserId
				where PmlExternalManagerEmployeeId = ins.employeeid
			)
	)	
 
  -- and exists (
		--select userid from all_user 
		--join inserted ins on UserId = ins.EmployeeUserId
		--where type = 'P' ) 
		)  
begin
	update broker_user
	set PmlExternalManagerEmployeeId = null
	from inserted ins
	where ins.IsPmlManager = 0 
	and PmlExternalManagerEmployeeId is not null
	and PmlExternalManagerEmployeeId = ins.EmployeeId
		
end

