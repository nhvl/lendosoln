CREATE      TRIGGER [dbo].[VerifyLoanAndAppMatchInEDocument] ON [dbo].[EDOCS_DOCUMENT]
FOR INSERT, UPDATE
AS
if( update( aAppId ) OR update( sLId )  )
begin
    if exists (
select * from inserted i
join application_a a
on a.aAppId = i.aAppId
where a.sLId <> i.sLId
)
    begin
      declare @msg varchar(500)
      set @msg = 'LoanId does not match loan app.'
      RAISERROR(@msg, 16, 1)
      rollback transaction
    end
end 