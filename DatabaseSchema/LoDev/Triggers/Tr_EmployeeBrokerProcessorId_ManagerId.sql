
CREATE trigger [dbo].[Tr_EmployeeBrokerProcessorId_ManagerId] on [dbo].[LOAN_FILE_CACHE]
after insert, update
as
if update( sEmployeeBrokerProcessorId )
begin 

	update loan_file_cache
	set loan_file_cache.PmlExternalBrokerProcessorManagerEmployeeId = bu.PmlExternalManagerEmployeeId
	from inserted ins, broker_user bu
	where loan_file_cache.slid = ins.slid and loan_file_cache.sEmployeeBrokerProcessorId = bu.employeeId

end


