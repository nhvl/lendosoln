CREATE TRIGGER dbo.EDOCS_DOCUMENT_TYPE_CLASSIFICATION_CHECK
   ON dbo.EDOCS_DOCUMENT_TYPE
   FOR INSERT, UPDATE
AS 
BEGIN
		-- Need to prevent users from setting the same classification id
		-- for more than one doc type for each broker
		DECLARE @NewClassificationId int
	    DECLARE @NewBrokerId uniqueidentifier
		DECLARE @NewDocTypeId int
		
		SELECT top 1
			@NewClassificationId = ClassificationId,
			@NewBrokerId = brokerid,
			@NewDocTypeId = doctypeid
		FROM inserted
	    
	    IF Exists(SELECT TOP 1 1
			FROM EDOCS_DOCUMENT_TYPE
			WHERE ClassificationId = @NewClassificationId
			and ClassificationId <> 0
			and brokerid = @NewBrokerId
			and doctypeid <> @NewDocTypeId)
	    BEGIN
			RAISERROR('Duplicate Classification id.', 16, 1);
			ROLLBACK TRANSACTION
		END
END
;