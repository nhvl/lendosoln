



CREATE           TRIGGER PopulateNameChangeToTask ON [dbo].[LOAN_FILE_CACHE]
FOR INSERT, UPDATE 
AS

if( update( sPrimBorrowerFullNm ) )
begin 
	update discussion_log
	set DiscRefObjNm2 = sPrimBorrowerFullNm 
	from inserted ins
	where DiscRefObjId = ins.sLId 

end




