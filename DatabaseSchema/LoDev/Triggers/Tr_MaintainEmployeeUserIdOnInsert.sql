create trigger Tr_MaintainEmployeeUserIdOnInsert on Broker_User
after insert
as 
if update( employeeId )
begin 
	-- For update scenario: 
	-- not possible, we don't allow it currently
	
	-- For insert scenario:

	update employee
	set EmployeeUserId = ins.UserId
	from employee e join inserted ins on e.employeeId = ins.employeeId	 
end

