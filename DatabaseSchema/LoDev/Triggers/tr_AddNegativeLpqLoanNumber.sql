CREATE         TRIGGER [dbo].[tr_AddNegativeLpqLoanNumber] ON [dbo].[LOAN_FILE_E]
FOR INSERT 
AS

if( update( sLpqLoanNumber ) )
begin
	DECLARE @ins_sLpqLoanNumber int
	DECLARE @checkLoansPQ int
	DECLARE @sLId UniqueIdentifier
	SELECT @ins_sLpqLoanNumber = sLpqLoanNumber, @sLId = sLId FROM inserted

	SELECT @checkLoansPQ = 1 WHERE EXISTS (SELECT sLpqLoanNumber FROM LOAN_FILE_E where sLpqLoanNumber =0 AND sLId <> @sLId)
	IF @checkLoansPQ <> 1 OR @checkLoansPQ IS NULL
	BEGIN
	--DECLARE @msg varchar(200)
	--set @msg = 'Loan Number' + convert(varchar(20), @ins_sLpqLoanNumber)
	--RAISERROR (@msg, 0, 0)
		-- 10/6/20 - dd - Only Perform this check if we are utilize sLpqLoanNumber
	IF (@ins_sLpqLoanNumber = 0 OR @ins_sLpqLoanNumber IS NULL)
	BEGIN
		DECLARE @sLpqLoanNumber INT
		SELECT TOP 1 @sLpqLoanNumber = sLpqLoanNumber FROM LOAN_FILE_E ORDER BY sLpqLoanNumber ASC
		IF @sLpqLoanNumber > 0
		BEGIN
			SET @sLpqLoanNumber = -1
		END
		SET @sLpqLoanNumber = @sLpqLoanNumber - 1
	--DECLARE @msg varchar(200)
	--set @msg = 'Loan Number' + @sLpqLoanNumber
	--RAISERROR (@msg, 0, 0)
	UPDATE LOAN_FILE_E 
	SET sLpqLoanNumber  = @sLpqLoanNumber
	FROM inserted ins JOIN LOAN_FILE_E le ON ins.sLId = le.sLId
	END
	END
end