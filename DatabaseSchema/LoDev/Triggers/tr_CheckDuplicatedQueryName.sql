
CREATE     TRIGGER tr_CheckDuplicatedQueryName ON dbo.REPORT_QUERY
after INSERT, UPDATE 
as
if( update (QueryName) )
begin
	declare @msg varchar(500);
	if( exists ( select QueryId from inserted where 0 = LEN(queryname) ) )
	begin
	   set @msg = 'Query name cannot be blank'
	   RAISERROR(@msg, 16, 1);
	   rollback transaction
	end
	else
	begin
		declare @count int
		select @count = count(*)
		from report_query  r1 
		group by r1.queryname, r1.brokerid
		having count(*) > 1;
	
		if( @count > 0 )
		begin
		   set @msg = 'Duplicated query name is not allowed within a broker'
		   RAISERROR(@msg, 16, 1);
		   rollback transaction
		end
	end
end



