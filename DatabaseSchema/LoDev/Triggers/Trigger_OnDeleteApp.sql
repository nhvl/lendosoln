CREATE TRIGGER [Trigger_OnDeleteApp]
ON [APPLICATION_A]
FOR DELETE
AS
BEGIN
	DECLARE @loanId uniqueidentifier,
			@appId uniqueidentifier,
			@BEmail varchar(80),
			@CEmail varchar(80),
			@brokerId uniqueidentifier
	
	DECLARE cursor_acc CURSOR FOR SELECT aAppId, sLId, aBEmail, aCEmail FROM deleted
	OPEN cursor_acc
	FETCH NEXT FROM cursor_acc INTO @appId, @loanId, @BEmail, @CEmail	
	WHILE @@FETCH_STATUS = 0
	BEGIN
		SELECT @brokerId=sBrokerId FROM LOAN_FILE_CACHE WHERE sLId=@loanId
		--deletion of an app would do a cascade delete on CP_CONSUMER_X_LOANID. If no entry remains in CP_CONSUMER_X_LOANID for the email, delete the account
		IF @BEmail IS NOT NULL AND LEN(@BEmail) > 0			
			IF NOT EXISTS (SELECT * FROM CP_CONSUMER c INNER JOIN CP_CONSUMER_X_LOANID cl ON c.ConsumerId=cl.ConsumerId WHERE c.BrokerId=@brokerId AND c.Email=@BEmail)				
			BEGIN
				--select cast('as' as uniqueidentifier)
				DELETE FROM CP_CONSUMER WHERE BrokerId=@brokerId AND Email=@BEmail
			END

		IF @CEmail IS NOT NULL AND LEN(@CEmail) > 0
			IF NOT EXISTS (SELECT * FROM CP_CONSUMER c INNER JOIN CP_CONSUMER_X_LOANID cl ON c.ConsumerId=cl.ConsumerId WHERE c.BrokerId=@brokerId AND c.Email=@CEmail)				
			BEGIN
				DELETE FROM CP_CONSUMER WHERE BrokerId=@brokerId AND Email=@CEmail
			END

		/*		
		IF LEN(@BEmail) > 0
			IF EXISTS(SELECT ConsumerId FROM CP_CONSUMER WHERE Email = @BEmail)
				EXEC CP_CheckAndUpdateConsumerAccount @BEmail, '', @loanId, @appId

		IF LEN(@CEmail) > 0
			IF EXISTS(SELECT ConsumerId FROM CP_CONSUMER WHERE Email = @CEmail)
				EXEC CP_CheckAndUpdateConsumerAccount @CEmail, '', @loanId, @appId
		*/
		FETCH NEXT FROM cursor_acc INTO @appId, @loanId, @BEmail, @CEmail
	END
	CLOSE cursor_acc
	DEALLOCATE cursor_acc
END