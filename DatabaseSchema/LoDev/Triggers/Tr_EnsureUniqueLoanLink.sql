

CREATE    trigger Tr_EnsureUniqueLoanLink on Loan_Link
for update, insert
as 
	if update( sLId1 ) or update ( sLId2 )
	begin 
		if exists( select before.sLId1, before.sLId2 
			   from loan_link before, inserted ins
		 	   where ins.sLId1 = before.sLId2 or ins.sLId2 = before.sLId1 )
		begin
			RAISERROR('Error: cannot have more than one loan-link per loan. This error msg came from trigger Tr_EnsureUniqueLoanLink', 16, 1);
			rollback	
		end
	end




