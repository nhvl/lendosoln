


CREATE     trigger Tr_LoanIsTemplate
on dbo.LOAN_FILE_A
for update
as 
IF NOT UPDATE(IsTemplate)
 RETURN
update loan_file_cache with(rowlock)
set IsTemplate = i.IsTemplate
from loan_file_cache LCache, inserted i
where LCache.sLId = i.sLId





