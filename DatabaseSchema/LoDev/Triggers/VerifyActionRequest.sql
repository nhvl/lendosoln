CREATE       TRIGGER VerifyActionRequest ON CP_Consumer_Action_request
FOR INSERT, UPDATE 
AS
if( update( aAppId ) OR update( sLId )  )
begin 
	if exists ( 
select * from inserted i 
join application_a a 
on a.aAppId = i.aAppId
where a.sLId <> i.sLId 
)
	begin 
	   declare @msg varchar(500)
	   set @msg = 'LoanId does not match loan app.'
	   RAISERROR(@msg, 16, 1);
	   rollback transaction
	end
end
