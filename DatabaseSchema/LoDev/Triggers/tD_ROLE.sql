
/****** Object:  Trigger dbo.tD_ROLE    Script Date: 9/30/2002 4:16:33 PM ******/
/****** Object:  Trigger dbo.tD_ROLE    Script Date: 9/27/2002 7:39:55 PM ******/
CREATE trigger tD_ROLE on ROLE for DELETE as
/* ERwin Builtin Thu Jun 06 15:31:47 2002 */
/* DELETE trigger on ROLE */
begin
  declare  @errno   int,
           @errmsg  varchar(255)
    /* ERwin Builtin Thu Jun 06 15:31:47 2002 */
    /* ROLE <is assigned> LOAN_USER_ASSIGNMENT ON PARENT DELETE RESTRICT */
    if exists (
      select * from deleted,LOAN_USER_ASSIGNMENT
      where
        /*  %JoinFKPK(LOAN_USER_ASSIGNMENT,deleted," = "," and") */
        LOAN_USER_ASSIGNMENT.RoleId = deleted.RoleId
    )
    begin
      select @errno  = 30001,
             @errmsg = 'Cannot DELETE ROLE because LOAN_USER_ASSIGNMENT exists.'
      goto error
    end
    /* ERwin Builtin Thu Jun 06 15:31:47 2002 */
    /* ROLE <is assigned> ROLE_ASSIGNMENT ON PARENT DELETE RESTRICT */
    if exists (
      select * from deleted,ROLE_ASSIGNMENT
      where
        /*  %JoinFKPK(ROLE_ASSIGNMENT,deleted," = "," and") */
        ROLE_ASSIGNMENT.RoleId = deleted.RoleId
    )
    begin
      select @errno  = 30001,
             @errmsg = 'Cannot DELETE ROLE because ROLE_ASSIGNMENT exists.'
      goto error
    end
    /* ERwin Builtin Thu Jun 06 15:31:47 2002 */
    return
error:
    RAISERROR(@errmsg, 16, 1);
    rollback transaction
end



