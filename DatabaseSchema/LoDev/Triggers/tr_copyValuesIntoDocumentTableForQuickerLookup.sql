CREATE TRIGGER tr_copyValuesIntoDocumentTableForQuickerLookup 
on EDocs_Audit_History for INSERT, UPDATE as
update EDocs_Document 
      set EDocs_Document.Version = inserted.Version, LastModifiedDate = ModifiedDate
      from inserted 
      where EDocs_Document.DocumentId = inserted.DocumentId
