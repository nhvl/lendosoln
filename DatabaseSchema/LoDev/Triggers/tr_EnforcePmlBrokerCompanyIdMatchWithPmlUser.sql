CREATE TRIGGER [dbo].[tr_EnforcePmlBrokerCompanyIdMatchWithPmlUser]
ON [dbo].[BROKER_USER]
AFTER Update
AS
RETURN
	-- dd 6/10/2009 - I could only apply this trigger for UPDATE. The reason is during the INSERT, userid from All_User and Employee are not commit for read yet.
--	IF (UPDATE(PmlBrokerId))
--	BEGIN

--		IF EXISTS ( SELECT a.UserId FROM INSERTED i JOIN ALL_USER a ON a.UserId=i.UserId
--                                JOIN Employee e ON e.EmployeeUserId = a.UserId
--								JOIN Branch br ON e.BranchId = br.BranchId
--		                                WHERE (a.Type = 'P' AND NOT EXISTS (SELECT p.PmlBrokerId FROM Pml_Broker p WHERE p.PmlBrokerId = i.PmlBrokerId AND p.BrokerId = br.BrokerId))
--			                                    OR (a.Type = 'B' AND i.PmlBrokerId <> '00000000-0000-0000-0000-000000000000')
                                              
--			          )
--		BEGIN
--			RAISERROR('PmlBrokerId must be non empty for P-user or empty guid for B user.', 16, 1);
--			ROLLBACK TRANSACTION
--		END
--	END
