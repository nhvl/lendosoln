
CREATE Trigger t_USAGE_EVENT_ValidateUserId
on USAGE_EVENT after insert, update
as
	if( update( UserId ) )
	begin
		if exists( 	select usageEventId
				from inserted
				where ( UserId != '00000000-0000-0000-0000-000000000000' )
				and UserId not in ( select UserId from Employee ) )
		begin
			RAISERROR('Invalid UserId value', 16, 1);
			rollback transaction
			return;
		end	
	end



