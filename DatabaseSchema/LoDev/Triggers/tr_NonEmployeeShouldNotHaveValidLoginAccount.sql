CREATE trigger tr_NonEmployeeShouldNotHaveValidLoginAccount
on employee
for insert, update
as

if exists ( 
	select * 
	from inserted i join all_user us on i.employeeUserId = us.UserId
	where i.Isactive = 0 and us.IsActive = 1
)
begin
	RAISERROR('Non-employee cannot have valid login account.', 16, 1);
	rollback transaction
end