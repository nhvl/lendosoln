
ALTER TRIGGER dbo.tI_ACTION_EVENT ON dbo.ACTION_EVENT FOR INSERT AS
BEGIN
	DECLARE @UserId uniqueidentifier
	DECLARE @EventType varchar(36)
	DECLARE @EventDate datetime
	SELECT @UserId=UserID, @EventType=EventType, @EventDate=EventDate FROM inserted
	
	IF @EventType='Login'
	BEGIN
		UPDATE ALL_USER
			SET RecentLoginD=@EventDate
			WHERE UserID=@UserId
	END
END



