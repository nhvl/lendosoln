
ALTER TRIGGER [dbo].[TRG_TASK_CHECK_STATIC_COND_ROW_IDS]
ON [dbo].[TASK]
FOR INSERT, UPDATE
AS

BEGIN
	-- *** REMEMBER to test if TASK_SyncLoanCachedInfoToTaskByLoanId is still working!

	-- First, see if none of the brokers corresponding to conditions that we've inserted/updated have static conds turned on
	DECLARE @CheckStaticCondRowIds int
	SET @CheckStaticCondRowIds = (SELECT COUNT(*)
	                              FROM broker JOIN inserted ON inserted.BrokerId = broker.BrokerId
								  WHERE broker.IsUseNewTaskSystemStaticConditionIds = 1)
	
	IF @CheckStaticCondRowIds > 0 -- otherwise, there are no brokers with static conds turned on, so don't bother with this check
	BEGIN
		-- Count the number of newly added/changed conditions, belonging to a static condition id broker, that have a CondRowId of 0. These should not be 0.
		-- In other words
		-- For each condition, belonging to a broker using static condition IDs:
		--   if CondRowId == 0:
		--     then raise an error
		DECLARE @StaticZeros int
		SET @StaticZeros = (SELECT COUNT(*)
							FROM inserted JOIN broker ON inserted.BrokerId = broker.BrokerId
							WHERE inserted.TaskIsCondition = 1 AND inserted.CondRowId = 0 AND broker.IsUseNewTaskSystemStaticConditionIds = 1)
	    
	    IF @StaticZeros > 0
	    BEGIN
			RAISERROR('RowCondId is zero, and the broker has static condition IDs turned on. Conditions should be nonzero.', 16, 1);
			ROLLBACK TRANSACTION
		END
	
		-- =============================== OLD ===============================
		-- Since we don't version control stored procedures, I'm keeping this code
		-- here so that we can revert back to it if the new stuff doesn't work out.
		-- =============================== OLD ===============================
		-- Count the number of loans that have a mismatch between the number of task CondRowIds
		-- and the number of DISTINCT task CondRowIds.
		-- With this, we can tell whether a CondRowId has been duplicated.
		-- We will allow non-conditions to have CondRowIds, to allow changing a task to a condition and back.
		--DECLARE @LoanMismatchCount int
		--SET @LoanMismatchCount = (
		--	SELECT COUNT(*)
		--	FROM
		--	(   -- For each broker with static cond IDs turned on
		--		--   For each loan
		--		--     Count the number of conditions
		--		--     Count the number of distinct conditionIDs
		--		--     If the counts are different, we have a duplicate, so it's a mismatch. Add to the mismatch table.
		--		-- Count the number of entries in the mismatch table. If the count is greater than 0, raise error.
		--		SELECT COUNT(task.CondRowId) as cnt
		--		FROM task JOIN broker ON task.BrokerId = broker.BrokerId
		--		WHERE broker.IsUseNewTaskSystemStaticConditionIds = 1 AND task.CondRowId != 0
		--		GROUP BY task.LoanId
		--		HAVING COUNT(task.CondRowId) != COUNT(DISTINCT task.CondRowId)
		--	) as MismatchCount)
		--IF @LoanMismatchCount > 0 -- then there are non-unique rows
		-- =============================== OLD ===============================
		
		IF EXISTS(SELECT TOP 1 t.TaskId
		        FROM Task t JOIN inserted ins on ins.loanid = t.loanid 
		        WHERE
		        ins.taskid <> t.taskid						-- These same-loan TASKS are different
		        and ins.CondRowId = t.CondRowId					-- BUT share the same CondRowId
		        and t.brokerid = ins.brokerid					
		        and ins.TaskIsCondition = 1
		        and ins.CondIsDeleted = 0
		        and t.TaskIsCondition = 1
		        and t.CondIsDeleted = 0)
		BEGIN
			RAISERROR('RowCondId is non-unique, and the broker has static condition IDs turned on. Conditions should have a unique RowCondId.', 16, 1); 
			ROLLBACK TRANSACTION
		END
	END
END
;