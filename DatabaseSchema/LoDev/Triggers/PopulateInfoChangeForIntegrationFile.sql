ALTER TRIGGER [dbo].[PopulateInfoChangeForIntegrationFile] ON [dbo].[LOAN_FILE_CACHE]
FOR INSERT, UPDATE 
AS
if( update( aBNm ) or 
	update( sSpAddr ) or 
	update( IsValid ) or
	update( sBranchID ) or
	update( sEmployeeManagerId ) or
	update( sEmployeeProcessorId ) or
	update( sEmployeeLoanRepId ) or
	update( sEmployeeLoanOpenerId ) or
	update( sEmployeeCallCenterAgentId ) or
	update( sEmployeeRealEstateAgentId ) or
	update( sEmployeeLenderAccExecId ) or 
	update( sEmployeeLockDeskId ) or
	update( sEmployeeUnderwriterId ) or
	update( sEmployeeBrokerProcessorId) OR
	update( PmlExternalManagerEmployeeId ) OR
	update( sStatusT )
   )
begin 
	update integration_file_modified
	set aBNm =							ins.aBNm, 
		sSpAddr =						ins.sSpAddr, 
		IsValid =						ins.IsValid,
		sBranchID =						ins.sBranchID, 
		sEmployeeManagerId =			ins.sEmployeeManagerId, 
		sEmployeeProcessorId =			ins.sEmployeeProcessorId, 
		sEmployeeLoanRepId =			ins.sEmployeeLoanRepId, 
		sEmployeeLoanOpenerId =			ins.sEmployeeLoanOpenerId,
		sEmployeeCallCenterAgentId =	ins.sEmployeeCallCenterAgentId, 
		sEmployeeRealEstateAgentId =	ins.sEmployeeRealEstateAgentId, 
		sEmployeeLenderAccExecId =		ins.sEmployeeLenderAccExecId, 
		sEmployeeLockDeskId =			ins.sEmployeeLockDeskId, 
		sEmployeeUnderwriterId =		ins.sEmployeeUnderwriterId, 
		sEmployeeBrokerProcessorId = ins.sEmployeeBrokerProcessorId,
		PmlExternalManagerEmployeeId =	ins.PmlExternalManagerEmployeeId,
		sStatusT = 						ins.sStatusT
	from inserted ins
	where integration_file_modified.sLid = ins.slid
end
