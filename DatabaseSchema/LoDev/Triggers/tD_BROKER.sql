
/****** Object:  Trigger dbo.tD_BROKER    Script Date: 9/30/2002 4:16:32 PM ******/
/****** Object:  Trigger dbo.tD_BROKER    Script Date: 9/27/2002 7:39:54 PM ******/
CREATE  trigger [dbo].[tD_BROKER] on [dbo].[BROKER] for DELETE as
/* ERwin Builtin Thu Jun 06 15:31:41 2002 */
/* DELETE trigger on BROKER */
begin
  declare  @errno   int,
           @errmsg  varchar(255)
    /* ERwin Builtin Thu Jun 06 15:31:41 2002 */
    /* BROKER <has> CC_TEMPLATE ON PARENT DELETE RESTRICT */
    if exists (
      select * from deleted,CC_TEMPLATE
      where
        /*  %JoinFKPK(CC_TEMPLATE,deleted," = "," and") */
        CC_TEMPLATE.BrokerId = deleted.BrokerId
    )
    begin
      select @errno  = 30001,
             @errmsg = 'Cannot DELETE BROKER because CC_TEMPLATE exists.'
      goto error
    end

    /* ERwin Builtin Thu Jun 06 15:31:41 2002 */
    /* BROKER <has> BRANCH ON PARENT DELETE RESTRICT */
    if exists (
      select * from deleted,BRANCH
      where
        /*  %JoinFKPK(BRANCH,deleted," = "," and") */
        BRANCH.BrokerId = deleted.BrokerId
    )
    begin
      select @errno  = 30001,
             @errmsg = 'Cannot DELETE BROKER because BRANCH exists.'
      goto error
    end
    /* ERwin Builtin Thu Jun 06 15:31:41 2002 */
    return
error:
    raiserror(@errmsg, 16, 1)
    rollback transaction
end
