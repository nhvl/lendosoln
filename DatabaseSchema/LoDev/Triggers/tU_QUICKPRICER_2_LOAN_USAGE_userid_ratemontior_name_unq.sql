/*
	Author: Scott Kibler
	CreateD: 9/11/2014
	Description:
		In QUICKPRICER_2_LOAN_USAGE:
		UserID_RateMonitorName combo should be unique when rateMontorName is not null.
*/

CREATE TRIGGER [dbo].[tU_QUICKPRICER_2_LOAN_USAGE_userid_ratemontior_name_unq] 
ON [dbo].[QUICKPRICER_2_LOAN_USAGE]
FOR INSERT, UPDATE AS
BEGIN
	set nocount on;
  declare  @errno   int,
           @errmsg  varchar(1023),
           @count int

  if
    (update(UserIDOfUsingUser) OR update(RateMonitorName))-- true for inserts.
  begin
    select @count = count(*) from INSERTED I, QUICKPRICER_2_LOAN_USAGE QP
    where
			QP.UserIDOFUsingUser = I.UserIdOfUsingUser
        AND QP.RateMonitorName = I.RateMonitorName
        AND I.RateMonitorName IS NOT NULL
    if (@count > 1)
    begin
      select @errno  = 30005,
             @errmsg = 'Cannot UPDATE QUICKPRICER_2_LOAN_USAGE because that rate monitor name already exists for that user.'
      goto error
    end
  end
  
  return
error:
    RAISERROR(@errmsg, 16, 1);
    rollback transaction
end
