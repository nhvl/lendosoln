
/****** Object:  Trigger dbo.CheckBlankLoginNm    Script Date: 9/30/2002 4:16:32 PM ******/
/****** Object:  Trigger dbo.CheckBlankLoginNm    Script Date: 9/27/2002 7:39:54 PM ******/
CREATE TRIGGER CheckBlankLoginNm
ON ALL_USER
FOR INSERT, UPDATE
AS
begin
	if not update(LoginNm)
		return
	declare @blankCount int
	select @blankCount = count( LoginNm )
	from inserted
	where ltrim(rtrim(LoginNm)) = '' 
	
	if @blankCount > 0
   	 begin
		declare @errno   int
           		declare @errmsg  varchar(255)
	     	 select @errno  = 50000,
	            		 @errmsg = 'Blank login name is not allowed'
	     	 raiserror(@errmsg, 16, 1);
		rollback transaction
   	 end
end



