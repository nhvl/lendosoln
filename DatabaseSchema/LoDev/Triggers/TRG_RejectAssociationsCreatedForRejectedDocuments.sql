-- =============================================
-- Author:		Matthew Pham
-- Create date: 9/27/2012
-- Description:	If an association has been
--     created for a rejected document, that
--     association should be marked as
--     unsatisfied.
-- =============================================
CREATE TRIGGER [dbo].[TRG_RejectAssociationsCreatedForRejectedDocuments]
ON [dbo].[TASK_x_EDOCS_DOCUMENT]
AFTER INSERT, UPDATE
AS
BEGIN
	BEGIN
	  UPDATE TASK_x_EDOCS_DOCUMENT
	  SET Status = 2 -- unsatisfied
	  FROM INSERTED as I
	  JOIN EDOCS_DOCUMENT as ED
		ON I.DocumentId = ED.DocumentId
	  JOIN TASK_x_EDOCS_DOCUMENT as TED
	    ON I.DocumentId = TED.DocumentId
	    AND I.TaskId = TED.TaskId
	    AND I.sLId = TED.sLId
	  WHERE ED.Status = 3 -- rejected
	  RETURN
	END
END