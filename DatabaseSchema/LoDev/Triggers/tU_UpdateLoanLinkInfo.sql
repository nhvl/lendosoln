create trigger tU_UpdateLoanLinkInfo on loan_file_cache
for update
as 
if( update( sLNm ) )
begin 
	update loan_link
	set sLNm1 = ins.sLNm
	from inserted ins
	where sLId1 = ins.slid

	update loan_link
	set sLNm2 = ins.sLNm
	from inserted ins
	where sLid2 = ins.slid
end
