
create trigger TriggerUpdateDiscTrackReadBit
on discussion_notification
after update
as
if( update( notifisread ) )
begin
	update tr
	set tr.tracknotifisread = 1
	from track tr join inserted ins on tr.notifid = ins.notifid
	where ins.notifisread = 1
end
	



