-- =============================================
-- Author:		Geoffrey Feltman
-- Create date: 11/18/13
-- Description:	Prevents saving an invalid loan template id for a portal.
--				Based on trigger for Broker.PmlLoanTemplateId
-- =============================================
CREATE TRIGGER dbo.tr_CONSUMER_PORTAL_DefaultLoanTemplateId
   ON dbo.CONSUMER_PORTAL
   AFTER INSERT,UPDATE
AS 
BEGIN
	IF (UPDATE(DefaultLoanTemplateId) 
		AND EXISTS 
	    (SELECT ins.DefaultLoanTemplateId
		FROM INSERTED ins LEFT JOIN LOAN_FILE_CACHE loanFile on
			ins.DefaultLoanTemplateId = loanFile.sLId 
			AND ins.BrokerId = loanFile.sBrokerId
		WHERE ins.DefaultLoanTemplateId IS NOT NULL 
			AND (loanFile.slid IS NULL OR IsTemplate = 0)
		)
	)
	BEGIN
		RAISERROR('CONSUMER_PORTAL.DefaultLoanTemplateId must point to a template file of the same corporate.', 16, 1);;
		ROLLBACK TRANSACTION
	END
END
