CREATE TRIGGER [Trigger_OnUpdateApp]
ON [APPLICATION_A]
FOR UPDATE
AS
BEGIN
	-- If a consumer account exists for the borrower or the coborrower, Update the consumer account
	IF UPDATE(aBEmail) OR UPDATE(aCEmail)
	BEGIN	
		DECLARE @loanId uniqueidentifier,
				@appId uniqueidentifier,
				@oldBEmail varchar(80),
				@oldCEmail varchar(80),
				@newBEmail varchar(80),
				@newCEmail varchar(80)				

		DECLARE cursor_acc CURSOR FOR SELECT d.aAppId, d.sLId, d.aBEmail, d.aCEmail, i.aBEmail, i.aCEmail FROM inserted i inner join deleted d on i.aAppId=d.aAppId 
		OPEN cursor_acc
		FETCH NEXT FROM cursor_acc INTO @appId, @loanId, @oldBEmail, @oldCEmail, @newBEmail, @newCEmail

		WHILE @@FETCH_STATUS = 0
		BEGIN
			/*
			--SSN for a borrower cannot be cleared if the borrower has a consumer account associated with an active consumer action request
			IF EXISTS(SELECT ConsumerActionRequestId FROM CP_CONSUMER_ACTION_REQUEST WHERE sLId=@loanId AND aAppId=@appId AND (ResponseStatus=0 OR ResponseStatus=1))
			AND
			EXISTS(SELECT * FROM CP_CONSUMER c INNER JOIN CP_CONSUMER_X_LOANID cl ON c.ConsumerId=cl.ConsumerId
				WHERE cl.sLId=@loanId AND cl.aAppId=@appId 
				AND 
				(
					((c.Email = @oldBEmail AND cl.IsBorrower=1) AND (len(@oldBSsn) <> 0 AND len(@newBSsn)=0))
				OR  ((c.Email = @oldCEmail AND cl.IsBorrower=0) AND (len(@oldCSsn) <> 0 AND len(@newCSsn)=0))
				))
			RAISERROR('Constraint_DocRequestSsnDeleteOp. The borrower SSN cannot be cleared as the borrower has active document requests. Error in updating consumer SSN in Trigger_OnUpdateApp trigger', 16, 1);
			*/

			-- An email should be unique on a loan file for e-signing. Check for the uniqueness of email if there is an open e-sign consumer action request for the loan file
			IF EXISTS(SELECT ConsumerActionRequestId FROM CP_CONSUMER_ACTION_REQUEST WHERE sLId=@loanId AND IsESignAllowed=1 AND (ResponseStatus=0 OR ResponseStatus=1))
			AND(
				(len(@newBEmail)<>0 AND @newBEmail = @newCEmail) OR
				(len(@newBEmail)<>0  AND EXISTS(SELECT aBEmail, aCEmail FROM APPLICATION_A WHERE sLId=@loanId AND aAppId<>@appId AND (aBEmail = @newBEmail OR aCEmail=@newBEmail))) OR
				(len(@newCEmail)<>0  AND EXISTS(SELECT aBEmail, aCEmail FROM APPLICATION_A WHERE sLId=@loanId AND aAppId<>@appId AND (aCEmail=@newCEmail OR aCEmail=@newCEmail)))
			   )
			RAISERROR('Constraint_DocRequestUniqueBorrowerEmail. The email should not match any other borrower email in a loan file with open e-Sign requests. Error in updating consumer email in Trigger_OnUpdateApp trigger', 16, 1);

			--CONSUMER ACCOUNT HANDLING
			-- update the borrower consumer account if borrower email has changed
			IF @oldBEmail <> @newBEmail
				EXEC CP_CheckAndUpdateConsumerAccount @oldBEmail, @newBEmail, @loanId, @appId, 1, null		
			
			-- update the co-borrower consumer account if the co-borrower email has changed
			IF @oldCEmail <> @newCEmail
				EXEC CP_CheckAndUpdateConsumerAccount @oldCEmail, @newCEmail, @loanId, @appId, 0, null	
		
			FETCH NEXT FROM cursor_acc INTO @appId, @loanId, @oldBEmail, @oldCEmail, @newBEmail, @newCEmail
		END
		CLOSE cursor_acc
		DEALLOCATE cursor_acc
	END
END
