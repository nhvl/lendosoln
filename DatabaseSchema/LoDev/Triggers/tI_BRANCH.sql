
/****** Object:  Trigger dbo.tI_BRANCH    Script Date: 9/30/2002 4:16:34 PM ******/
/****** Object:  Trigger dbo.tI_BRANCH    Script Date: 9/27/2002 7:39:56 PM ******/
CREATE trigger tI_BRANCH on BRANCH for INSERT as
/* ERwin Builtin Thu Jun 06 15:31:41 2002 */
/* INSERT trigger on BRANCH */
begin
  declare  @numrows int,
           @nullcnt int,
           @validcnt int,
           @errno   int,
           @errmsg  varchar(255)
  select @numrows = @@rowcount
  /* ERwin Builtin Thu Jun 06 15:31:41 2002 */
  /* BROKER <has> BRANCH ON CHILD INSERT RESTRICT */
  if
    /* %ChildFK(" or",update) */
    update(BrokerId)
  begin
    select @nullcnt = 0
    select @validcnt = count(*)
      from inserted,BROKER
        where
          /* %JoinFKPK(inserted,BROKER) */
          inserted.BrokerId = BROKER.BrokerId
    /* %NotnullFK(inserted," is null","select @nullcnt = count(*) from inserted where"," and") */
    
    if @validcnt + @nullcnt != @numrows
    begin
      select @errno  = 30002,
             @errmsg = 'Cannot INSERT BRANCH because BROKER does not exist.'
      goto error
    end
  end
  /* ERwin Builtin Thu Jun 06 15:31:41 2002 */
  return
error:
    RAISERROR(@errmsg, 16, 1);
    rollback transaction
end



