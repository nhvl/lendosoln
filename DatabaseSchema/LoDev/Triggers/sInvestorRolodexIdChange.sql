-- =============================================
-- Author:		Antonio Valencia
-- Create date: 1/8/2013
-- Description:	Verifies a Investor Rolodex Id Is valid for given loan file
-- =============================================
CREATE TRIGGER dbo.sInvestorRolodexIdChange 
   ON  dbo.LOAN_FILE_e
   FOR INSERT, UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF (UPDATE(sInvestorRolodexId) )
	BEGIN
		DECLARE @checkCount int
		DECLARE @expectCount int 
		DECLARE @negativeCount int 
		
		SELECT @expectCount = count(*) FROM INSERTED 
		SELECT @negativeCount = count(*) FROM INSERTED  WHERE sInvestorRolodexId = -1
			

			-- if the investor doesnt exist a row will be missing from the following query
			-- note negative ids will not b e included in this query since they dont exist int he table
			SELECT @checkCount = count(*) FROM INSERTED i
							INNER JOIN INVESTOR_ROLODEX r on r.InvestorRolodexId = i.sInvestorRolodexId AND i.sBrokerId = r.BrokerId					
		
		
		IF (@expectCount) <> (@checkCount  +  @negativeCount)
		BEGIN
			RAISERROR ('sInvestorRolodexId belongs to a different broker %d %d %d', 16, 1, @expectCount, @negativeCount, @checkCount)
			ROLLBACK TRANSACTION -- do we need this? 
		END
	END
	END

