
/****** Object:  Trigger dbo.tU_ROLE    Script Date: 9/30/2002 4:16:33 PM ******/
/****** Object:  Trigger dbo.tU_ROLE    Script Date: 9/27/2002 7:39:55 PM ******/
CREATE trigger tU_ROLE on ROLE for UPDATE as
/* ERwin Builtin Thu Jun 06 15:31:47 2002 */
/* UPDATE trigger on ROLE */
begin
  declare  @numrows int,
           @nullcnt int,
           @validcnt int,
           @insRoleId GUID,
           @errno   int,
           @errmsg  varchar(255)
  select @numrows = @@rowcount
  /* ERwin Builtin Thu Jun 06 15:31:47 2002 */
  /* ROLE <is assigned> LOAN_USER_ASSIGNMENT ON PARENT UPDATE RESTRICT */
  if
    /* %ParentPK(" or",update) */
    update(RoleId)
  begin
    if exists (
      select * from deleted,LOAN_USER_ASSIGNMENT
      where
        /*  %JoinFKPK(LOAN_USER_ASSIGNMENT,deleted," = "," and") */
        LOAN_USER_ASSIGNMENT.RoleId = deleted.RoleId
    )
    begin
      select @errno  = 30005,
             @errmsg = 'Cannot UPDATE ROLE because LOAN_USER_ASSIGNMENT exists.'
      goto error
    end
  end
  /* ERwin Builtin Thu Jun 06 15:31:47 2002 */
  /* ROLE <is assigned> ROLE_ASSIGNMENT ON PARENT UPDATE RESTRICT */
  if
    /* %ParentPK(" or",update) */
    update(RoleId)
  begin
    if exists (
      select * from deleted,ROLE_ASSIGNMENT
      where
        /*  %JoinFKPK(ROLE_ASSIGNMENT,deleted," = "," and") */
        ROLE_ASSIGNMENT.RoleId = deleted.RoleId
    )
    begin
      select @errno  = 30005,
             @errmsg = 'Cannot UPDATE ROLE because ROLE_ASSIGNMENT exists.'
      goto error
    end
  end
  /* ERwin Builtin Thu Jun 06 15:31:47 2002 */
  return
error:
    RAISERROR(@errmsg, 16, 1);
    rollback transaction
end



