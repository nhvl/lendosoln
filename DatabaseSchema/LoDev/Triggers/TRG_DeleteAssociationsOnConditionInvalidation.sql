-- =============================================
-- Author:		Matthew Pham
-- Create date: 9/25/2012
-- Description:	When a condition is invalidated,
--     all the associations with edocs should be
--     deleted as well.
-- =============================================
CREATE TRIGGER [dbo].[TRG_DeleteAssociationsOnConditionInvalidation]
ON [dbo].[TASK]
AFTER INSERT, UPDATE
AS
BEGIN
	IF EXISTS (SELECT COUNT(1) FROM TASK_x_EDOCS_DOCUMENT as TED
		JOIN INSERTED as I
		ON TED.TaskId = I.TaskId
		WHERE I.CondIsDeleted = 1)
	BEGIN
	  DELETE TASK_x_EDOCS_DOCUMENT
	  FROM TASK_x_EDOCS_DOCUMENT as TED JOIN INSERTED as I
		ON TED.TaskId = I.TaskId
	  WHERE I.CondIsDeleted = 1
	  RETURN
	END
END