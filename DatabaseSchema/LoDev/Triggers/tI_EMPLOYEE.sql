
/****** Object:  Trigger dbo.tI_EMPLOYEE    Script Date: 9/30/2002 4:16:36 PM ******/
/****** Object:  Trigger dbo.tI_EMPLOYEE    Script Date: 9/27/2002 7:39:58 PM ******/
CREATE trigger tI_EMPLOYEE on dbo.EMPLOYEE for INSERT as
/* ERwin Builtin Thu Jun 06 15:31:47 2002 */
/* INSERT trigger on EMPLOYEE */
begin
  declare  @numrows int,
           @nullcnt int,
           @validcnt int,
           @errno   int,
           @errmsg  varchar(255)
  select @numrows = @@rowcount
  /* ERwin Builtin Thu Jun 06 15:31:47 2002 */
  /* BRANCH <employs> EMPLOYEE ON CHILD INSERT RESTRICT */
  if
    /* %ChildFK(" or",update) */
    update(BranchId)
  begin
    select @nullcnt = 0
    select @validcnt = count(*)
      from inserted,BRANCH
        where
          /* %JoinFKPK(inserted,BRANCH) */
          inserted.BranchId = BRANCH.BranchId
    /* %NotnullFK(inserted," is null","select @nullcnt = count(*) from inserted where"," and") */
    
    if @validcnt + @nullcnt != @numrows
    begin
      select @errno  = 30002,
             @errmsg = 'Cannot INSERT EMPLOYEE because BRANCH does not exist.'
      goto error
    end
  end
  /* ERwin Builtin Thu Jun 06 15:31:47 2002 */
  return
error:
    RAISERROR(@errmsg, 16, 1);
    rollback transaction
end



