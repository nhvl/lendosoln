


CREATE    trigger Tr_BranchLpePriceGroupIdDefaultValidate on branch
after insert, update
as
if update( BranchLpePriceGroupIdDefault )
begin
   
   if exists ( select ins.BranchLpePriceGroupIdDefault
	--ExternalPriceGroupEnabled
   	from 
		inserted ins 
		join lpe_price_group gr on ins.BranchLpePriceGroupIdDefault = gr.LpePriceGroupId
		--deleted del
		--join deleted del on ins.brokerId = del.brokerId 
   	where 	ins.brokerId <> gr.brokerId -- this should never be the case
		or 
		( gr.ExternalPriceGroupEnabled = 0 
/*
		  and -- it's ok if null overwrites null
		  NOT( ins.BranchLpePriceGroupIdDefault is null and del.BranchLpePriceGroupIdDefault is null )
		  and -- it's ok to overwrites the same non-null value
		  not( ins.BranchLpePriceGroupIdDefault is not null and del.BranchLpePriceGroupIdDefault is not null and ins.BranchLpePriceGroupIdDefault = del.BranchLpePriceGroupIdDefault )
*/
		) )
		--or ( del.brokerId = ins.brokerId and ( del.BrokerLpePriceGroupIdDefault is not null and ins.BrokerLpePriceGroupIdDefault is null ) ) )
   begin
	RAISERROR('Error. Disabled price group is not allowed as branch default AND price group of one broker cannot be default of a branch in another broker', 16, 1);
	rollback transaction
   end



end




