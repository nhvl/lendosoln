


CREATE trigger [dbo].[Tr_EmployeeLoanRepId_ManagerId] on [dbo].[LOAN_FILE_CACHE]
after insert, update
as
if update( sEmployeeLoanRepId )
begin 

	update loan_file_cache
	set loan_file_cache.PmlExternalManagerEmployeeId = bu.PmlExternalManagerEmployeeId
	from inserted ins, broker_user bu
	where loan_file_cache.slid = ins.slid and loan_file_cache.sEmployeeLoanRepId = bu.employeeId
end