ALTER TRIGGER [dbo].[tI_ACTION_EVENT_2013] ON [dbo].[ACTION_EVENT_2013] FOR INSERT AS
BEGIN
	DECLARE @UserId uniqueidentifier
	DECLARE @EventType varchar(36)
	DECLARE @EventDate datetime
	DECLARE @IpAddress varchar(46)
	SELECT @UserId=UserID, @EventType=EventType, @EventDate=EventDate, @IpAddress=IpAddress FROM inserted
	
	IF @EventType='Login'
	BEGIN
		IF @IpAddress IS NOT NULL AND @IpAddress <> ''
		BEGIN
			UPDATE ALL_USER
			SET RecentLoginD=@EventDate, LastUsedIpAddress=@IpAddress
			WHERE UserID=@UserId
		END
		ELSE
		BEGIN
			UPDATE ALL_USER
			SET RecentLoginD=@EventDate
			WHERE UserID=@UserId
		END
	END
END