CREATE       TRIGGER [dbo].[VerifyBrokerIdsMatch] ON [dbo].[EDOCS_DOCUMENT]
FOR INSERT, UPDATE 
AS
if( update( DocTypeId ) OR update( BrokerId )  )
begin 
      if exists ( 
select * from inserted i 
join EDOCS_DOCUMENT_TYPE d 
on d.DocTypeId = i.DocTypeId
where d.BrokerId <> i.BrokerId 
)
      begin 
         declare @msg varchar(500)
         set @msg = 'BrokerId does not match between Document and DocType.'
         RAISERROR(@msg, 16, 1);
         rollback transaction
      end
end
