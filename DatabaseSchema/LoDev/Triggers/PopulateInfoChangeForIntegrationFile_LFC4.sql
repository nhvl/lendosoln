-- =============================================
-- Author:		Justin Kim
-- Create date: 3/2/2018
-- Description:	Trigger that automatically updates the loan modified list from values in loan file cache 4 upon cache update
-- =============================================
CREATE TRIGGER [dbo].[PopulateInfoChangeForIntegrationFile_LFC4]
   ON  [dbo].[LOAN_FILE_CACHE_4]
   AFTER INSERT, UPDATE
AS 
IF (UPDATE(aBSsnEncrypted)
	OR UPDATE(sEncryptionKey)
	OR UPDATE(sEncryptionMigrationVersion)
	)
BEGIN
	UPDATE INTEGRATION_FILE_MODIFIED
	SET aBSsnEncrypted = ins.aBSsnEncrypted,
		sEncryptionKey = ins.sEncryptionKey,
		sEncryptionMigrationVersion = ins.sEncryptionMigrationVersion
	FROM inserted ins
	WHERE INTEGRATION_FILE_MODIFIED.sLId = ins.sLId
END