
/****** Object:  Trigger dbo.tD_EMPLOYEE    Script Date: 9/30/2002 4:16:36 PM ******/
/****** Object:  Trigger dbo.tD_EMPLOYEE    Script Date: 9/27/2002 7:39:58 PM ******/
CREATE trigger [dbo].[tD_EMPLOYEE] on [dbo].[EMPLOYEE] for DELETE as
/* ERwin Builtin Thu Jun 06 15:31:47 2002 */
/* DELETE trigger on EMPLOYEE */
begin
  declare  @errno   int,
           @errmsg  varchar(255)

    /* ERwin Builtin Thu Jun 06 15:31:47 2002 */
    /* EMPLOYEE <is> ROLE_ASSIGNMENT ON PARENT DELETE RESTRICT */
    if exists (
      select * from deleted,ROLE_ASSIGNMENT
      where
        /*  %JoinFKPK(ROLE_ASSIGNMENT,deleted," = "," and") */
        ROLE_ASSIGNMENT.EmployeeId = deleted.EmployeeId
    )
    begin
      select @errno  = 30001,
             @errmsg = 'Cannot DELETE EMPLOYEE because ROLE_ASSIGNMENT exists.'
      goto error
    end
    /* ERwin Builtin Thu Jun 06 15:31:47 2002 */
    /* EMPLOYEE <works on> LOAN_USER_ASSIGNMENT ON PARENT DELETE RESTRICT */
    if exists (
      select * from deleted,LOAN_USER_ASSIGNMENT
      where
        /*  %JoinFKPK(LOAN_USER_ASSIGNMENT,deleted," = "," and") */
        LOAN_USER_ASSIGNMENT.EmployeeId = deleted.EmployeeId
    )
    begin
      select @errno  = 30001,
             @errmsg = 'Cannot DELETE EMPLOYEE because LOAN_USER_ASSIGNMENT exists.'
      goto error
    end

    /* ERwin Builtin Thu Jun 06 15:31:47 2002 */
    return
error:
    RAISERROR(@errmsg, 16, 1);
    rollback transaction
end



