





CREATE     PROCEDURE [dbo].[ListPMLUsersByBrokerId]
	@BrokerID uniqueidentifier, 
	@NameFilter Varchar(32) = NULL,
	@LastFilter Varchar(32) = NULL, 
	@PriceGroup uniqueidentifier = NULL,
	@PmlExternalManagerEmployeeId uniqueidentifier = NULL, -- 04/11/06 mf OPM 3340
	@LenderAccExecEmployeeId uniqueidentifier = NULL, -- 05/18/06 mf OPM 5006
	@LenderAccExecEmployeeNone bit = 0,
	@UnderwriterEmployeeNone bit = 0,
	@LockDeskEmployeeNone bit = 0,
	@ManagerEmployeeNone bit = 0,
	@ProcessorEmployeeNone bit = 0,
	@PriceGroupNone bit = 0,
	@UnderwriterEmployeeId uniqueidentifier = NULL,
	@LockDeskEmployeeId uniqueidentifier = NULL,
	@ManagerEmployeeId uniqueidentifier = NULL,
	@ProcessorEmployeeId uniqueidentifier = NULL,
	@LimitResultSet bit = 1,
	@LastLoginDate datetime = NULL,
	@BeforeAfter Varchar(32) = NULL,
	@BranchId uniqueidentifier = NULL,
	@IsActive int = -1	

AS

IF @LimitResultSet = 1
BEGIN
	SET ROWCOUNT 100
END
	SELECT v.UserId, v.EmployeeId, v.LoginNm, v.UserFirstNm + ' ' + v.UserLastNm AS UserName, v.Permissions,
	               u.LenderAccExecEmployeeId, u.LockDeskEmployeeId, u.UnderwriterEmployeeId, u.ManagerEmployeeId, u.ProcessorEmployeeId,
	               v.Type, v.IsPmlManager, v.Status, u.PmlExternalManagerEmployeeId, v.IsActive, v.CanLogin, v.LpePriceGroupId,
	               v.RecentLoginD, b.BranchNm, u.PmlBrokerId
	FROM (View_Active_Pml_User_With_Broker_Info v WITH (NOLOCK) LEFT JOIN Broker_User u WITH (NOLOCK) ON v.EmployeeId = u.EmployeeId) LEFT JOIN Branch b WITH (NOLOCK) ON v.BranchId = b.BranchId
	WHERE v.BrokerId = @BrokerId
		      AND (
				v.LpePriceGroupId = COALESCE(@PriceGroup, v.LpePriceGroupId)
				OR
				(@PriceGroup IS NULL AND v.LpePriceGroupId IS NULL)
			   )
				AND (
					(@PriceGroupNone = 1 AND v.LpePriceGroupId IS NULL)
					OR
					(@PriceGroupNone = 0)	
				)
		      AND ( 
				(@BranchId IS NOT NULL AND @BranchId = b.BranchId)
				OR
				(@BranchId IS NULL)
			   )
				AND (
					((@IsActive >= 0) AND @IsActive = v.IsActive)
				OR
					(@IsActive < 0)
				)
		      AND (
				( (@LastLoginDate IS NOT NULL AND @BeforeAfter = 'before') AND ((v.RecentLoginD < @LastLoginDate) OR v.RecentLoginD IS NULL) )
				OR
				( (@LastLoginDate IS NOT NULL AND @BeforeAfter = 'after') AND v.RecentLoginD >= @LastLoginDate)
				OR
				( @LastLoginDate IS NULL)
	                  )
		      AND (
				( @LastFilter IS NULL AND ( v.UserFirstNm LIKE COALESCE(@NameFilter, v.UserFirstNm) + '%'
	                                                                                      OR  v.UserLastNm LIKE COALESCE(@NameFilter, v.UserLastNm) + '%')
	                                                                                      OR v.LoginNm LIKE COALESCE(@NameFilter, v.LoginNm) + '%')
				OR
				( @LastFilter IS NOT NULL AND ( v.UserFirstNm LIKE @NameFilter + '%' AND v.UserLastNm LIKE @LastFilter + '%') )
	                  )
		      AND (
				( @PmlExternalManagerEmployeeId IS NOT NULL AND u.PmlExternalManagerEmployeeId = @PmlExternalManagerEmployeeId)
				OR
				( @PmlExternalManagerEmployeeId IS NULL)
		          )
		AND (
			(@LenderAccExecEmployeeId IS NOT NULL AND @LenderAccExecEmployeeId = u.LenderAccExecEmployeeId)
			OR
			(@LenderAccExecEmployeeId IS NULL)
		    )
		AND (
				(@LenderAccExecEmployeeNone = 1 AND u.LenderAccExecEmployeeId IS NULL)
				OR
				(@LenderAccExecEmployeeNone = 0)	
			)
		AND (
				(@UnderwriterEmployeeNone = 1 AND u.UnderwriterEmployeeId IS NULL)
				OR
				(@UnderwriterEmployeeNone = 0)	
			)
		AND (
				(@LockDeskEmployeeNone = 1 AND u.LockDeskEmployeeId IS NULL)
				OR
				(@LockDeskEmployeeNone = 0)	
			)
		AND (
				(@ManagerEmployeeNone = 1 AND u.ManagerEmployeeId IS NULL)
				OR
				(@ManagerEmployeeNone = 0)	
			)
		AND (
				(@ProcessorEmployeeNone = 1 AND u.ProcessorEmployeeId IS NULL)
				OR
				(@ProcessorEmployeeNone = 0)	
			)
		AND (
			(@UnderwriterEmployeeId IS NOT NULL AND @UnderwriterEmployeeId = u.UnderwriterEmployeeId)
			OR
			(@UnderwriterEmployeeId IS NULL)
		    )
	
		AND (
			(@LockDeskEmployeeId IS NOT NULL AND @LockDeskEmployeeId = u.LockDeskEmployeeId)
			OR
			(@LockDeskEmployeeId IS NULL)
		    )
	
		AND (
			(@ManagerEmployeeId IS NOT NULL AND @ManagerEmployeeId = u.ManagerEmployeeId)
			OR
			(@ManagerEmployeeId IS NULL)
		    )
		AND (
			(@ProcessorEmployeeId IS NOT NULL AND @ProcessorEmployeeId = u.ProcessorEmployeeId)
			OR
			(@ProcessorEmployeeId IS NULL)
		    )

UNION 


SELECT v.UserId, v.EmployeeId, v.LoginNm, v.UserFirstNm + ' ' + v.UserLastNm AS UserName, v.Permissions,
	               u.LenderAccExecEmployeeId, u.LockDeskEmployeeId, u.UnderwriterEmployeeId, u.ManagerEmployeeId, u.ProcessorEmployeeId,
	               v.Type, v.IsPmlManager, v.Status, u.PmlExternalManagerEmployeeId, v.IsActive, v.CanLogin, v.LpePriceGroupId,
	               v.RecentLoginD, b.BranchNm,u.PmlBrokerId
	FROM (View_Disabled_Pml_User_With_Broker_Info v WITH (NOLOCK) LEFT JOIN Broker_User u WITH (NOLOCK) ON v.EmployeeId = u.EmployeeId) LEFT JOIN Branch b WITH (NOLOCK) ON v.BranchId = b.BranchId
	WHERE v.BrokerId = @BrokerId
		      AND (
				v.LpePriceGroupId = COALESCE(@PriceGroup, v.LpePriceGroupId)
				OR
				(@PriceGroup IS NULL AND v.LpePriceGroupId IS NULL)
			   )
				AND (
					(@PriceGroupNone = 1 AND v.LpePriceGroupId IS NULL)
					OR
					(@PriceGroupNone = 0)	
				)
		      AND ( 
				(@BranchId IS NOT NULL AND @BranchId = b.BranchId)
				OR
				(@BranchId IS NULL)
			   )
				AND (
					((@IsActive >= 0) AND @IsActive = v.IsActive)
				OR
					(@IsActive < 0)
				)
		      AND (
				( (@LastLoginDate IS NOT NULL AND @BeforeAfter = 'before') AND ((v.RecentLoginD < @LastLoginDate) OR v.RecentLoginD IS NULL) )
				OR
				( (@LastLoginDate IS NOT NULL AND @BeforeAfter = 'after') AND v.RecentLoginD >= @LastLoginDate)
				OR
				( @LastLoginDate IS NULL)
	                  )
		      AND (
				( @LastFilter IS NULL AND ( v.UserFirstNm LIKE COALESCE(@NameFilter, v.UserFirstNm) + '%'
	                                                                                      OR  v.UserLastNm LIKE COALESCE(@NameFilter, v.UserLastNm) + '%')
	                                                                                      OR v.LoginNm LIKE COALESCE(@NameFilter, v.LoginNm) + '%')
				OR
				( @LastFilter IS NOT NULL AND ( v.UserFirstNm LIKE @NameFilter + '%' AND v.UserLastNm LIKE @LastFilter + '%') )
	                  )
		      AND (
				( @PmlExternalManagerEmployeeId IS NOT NULL AND u.PmlExternalManagerEmployeeId = @PmlExternalManagerEmployeeId)
				OR
				( @PmlExternalManagerEmployeeId IS NULL)
		          )
		AND (
			(@LenderAccExecEmployeeId IS NOT NULL AND @LenderAccExecEmployeeId = u.LenderAccExecEmployeeId)
			OR
			(@LenderAccExecEmployeeId IS NULL)
		    )
		AND (
				(@LenderAccExecEmployeeNone = 1 AND u.LenderAccExecEmployeeId IS NULL)
				OR
				(@LenderAccExecEmployeeNone = 0)	
			)
		AND (
				(@UnderwriterEmployeeNone = 1 AND u.UnderwriterEmployeeId IS NULL)
				OR
				(@UnderwriterEmployeeNone = 0)	
			)
		AND (
				(@LockDeskEmployeeNone = 1 AND u.LockDeskEmployeeId IS NULL)
				OR
				(@LockDeskEmployeeNone = 0)	
			)
		AND (
				(@ManagerEmployeeNone = 1 AND u.ManagerEmployeeId IS NULL)
				OR
				(@ManagerEmployeeNone = 0)	
			)
		AND (
				(@ProcessorEmployeeNone = 1 AND u.ProcessorEmployeeId IS NULL)
				OR
				(@ProcessorEmployeeNone = 0)	
			)
		AND (
			(@UnderwriterEmployeeId IS NOT NULL AND @UnderwriterEmployeeId = u.UnderwriterEmployeeId)
			OR
			(@UnderwriterEmployeeId IS NULL)
		    )
	
		AND (
			(@LockDeskEmployeeId IS NOT NULL AND @LockDeskEmployeeId = u.LockDeskEmployeeId)
			OR
			(@LockDeskEmployeeId IS NULL)
		    )
	
		AND (
			(@ManagerEmployeeId IS NOT NULL AND @ManagerEmployeeId = u.ManagerEmployeeId)
			OR
			(@ManagerEmployeeId IS NULL)
		    )
		AND (
			(@ProcessorEmployeeId IS NOT NULL AND @ProcessorEmployeeId = u.ProcessorEmployeeId)
			OR
			(@ProcessorEmployeeId IS NULL)
		    )