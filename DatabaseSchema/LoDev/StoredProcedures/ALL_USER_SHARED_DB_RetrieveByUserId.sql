-- =============================================
-- Author:		David Dao
-- Create date: 4/15/2015
-- Description:	
-- =============================================
CREATE PROCEDURE dbo.ALL_USER_SHARED_DB_RetrieveByUserId 
    @UserId UniqueIdentifier
AS
BEGIN
    SELECT BrokerId, LoginNm 
    FROM ALL_USER_SHARED_DB
    WHERE UserId = @UserId

END
