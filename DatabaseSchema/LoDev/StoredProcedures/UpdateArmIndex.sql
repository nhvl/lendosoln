CREATE PROCEDURE UpdateArmIndex
	@BrokerId Guid ,
	@IndexId Guid ,
	@IndexName Varchar( 100 ) = NULL ,
	@IndexCurrentValue Decimal( 9 , 3 ) = NULL ,
	@IndexCanBeFound Varchar( 200 ) = NULL ,
	@IndexBasedOn Varchar( 200 ) = NULL ,
	@FannieMaeArmIndexT Int = NULL ,
	@FreddieArmIndexT Int = NULL ,
	@EffectiveD DateTime = NULL
AS
	UPDATE Arm_Index
		SET IndexNameVstr = COALESCE( @IndexName , IndexNameVstr )
		, IndexCurrentValueDecimal = COALESCE( @IndexCurrentValue , IndexCurrentValueDecimal )
		, IndexBasedOnVstr = COALESCE( @IndexBasedOn , IndexBasedOnVstr )
		, IndexCanBeFoundVstr = COALESCE( @IndexCanBeFound , IndexCanBeFoundVstr )
		, FannieMaeArmIndexT = COALESCE( @FannieMaeArmIndexT , FannieMaeArmIndexT )
		, FreddieArmIndexT = COALESCE( @FreddieArmIndexT , FreddieArmIndexT )
		, EffectiveD = COALESCE( @EffectiveD , EffectiveD )
	WHERE
		BrokerIdGuid = @BrokerId
		AND
		IndexIdGuid = @IndexId
	IF( @@error != 0 )
	BEGIN
		RAISERROR('error in updating table Arm_Index in UpdateArmIndex sp', 16, 1);
		RETURN -100
	END
	RETURN 0
