CREATE PROCEDURE [dbo].[Region_CreateRegion]
	@BrokerId uniqueidentifier,
	@RegionSetId int,
	@Name varchar(50),
	@Description varchar(100),
	@RegionId int out
AS
BEGIN
	INSERT INTO REGION (BrokerId, RegionSetId, Name, [Description])
	VALUES (@BrokerId, @RegionSetId, @Name, @Description)
	
	SET @RegionId = SCOPE_IDENTITY()
END