-- =============================================
-- Author:		Brian Beery
-- Create date: 09/20/2013
-- Description:	Update a data retrieval framework partner
-- =============================================
CREATE PROCEDURE [dbo].[DATA_RETRIEVAL_PARTNER_Update]
	@PartnerId UniqueIdentifier,
	@Name varchar(255),
	@ExportPath varchar(1024)
AS
BEGIN

	UPDATE DATA_RETRIEVAL_PARTNER
		SET Name = @Name,
			ExportPath = @ExportPath
	WHERE PartnerId = @PartnerId

END