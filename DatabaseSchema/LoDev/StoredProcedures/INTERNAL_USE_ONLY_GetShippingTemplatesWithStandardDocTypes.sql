CREATE PROCEDURE [dbo].[INTERNAL_USE_ONLY_GetShippingTemplatesWithStandardDocTypes]
as
select st.shippingtemplateid, st.brokerid, dtst.doctypeid, doctypename 
from EDOCS_DOCUMENT_TYPE_X_SHIPPING_TEMPLATE dtst
with (nolock)
join edocs_shipping_template st on dtst.shippingtemplateid = st.shippingtemplateid 
join edocs_document_type dt on dt.doctypeid = dtst.doctypeid 
where dtst.doctypeid in
(
	select doctypeid from edocs_document_type with (nolock) where brokerid is null
)
and st.brokerid is not null
