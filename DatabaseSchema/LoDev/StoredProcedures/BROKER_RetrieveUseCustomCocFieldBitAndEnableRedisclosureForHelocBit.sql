CREATE PROCEDURE [dbo].[BROKER_RetrieveUseCustomCocFieldBitAndEnableRedisclosureForHelocBit]
	@BrokerId uniqueidentifier
AS
BEGIN
	SELECT UseCustomCocFieldList, EnableCocRedisclosureTriggersForHelocLoans
	FROM BROKER
	WHERE BrokerId = @BrokerId
END