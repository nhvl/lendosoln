-- =============================================
-- Author:		Brian Beery
-- Create date: 02/21/13
-- Description:	Get the Originating Company's Company ID by PMLBrokerID
-- =============================================
CREATE PROCEDURE [dbo].[GetPmlBrokerCompanyIdByPmlBrokerId]
	@PmlBrokerId UniqueIdentifier
	, @BrokerId UniqueIdentifier
AS
BEGIN
	SELECT CompanyId
	FROM Pml_Broker
	WHERE PmlBrokerId = @PmlBrokerId AND BrokerId = @BrokerId
END
