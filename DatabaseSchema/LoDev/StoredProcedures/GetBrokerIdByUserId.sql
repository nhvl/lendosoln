-- =============================================
-- Author:		David Dao
-- Create date: 
-- Description:	
-- =============================================
ALTER PROCEDURE [dbo].[GetBrokerIdByUserId]
    @UserId UniqueIdentifier
AS
BEGIN
    -- Look for broker id by user id. User Id can belong to either 'P' or 'B' user.
    SELECT br.BrokerId FROM Branch br JOIN Employee e ON br.BranchId = e.BranchId
        JOIN Broker b ON br.BrokerId = b.BrokerId
    WHERE e.EmployeeUserId = @UserId
      AND b.Status=1


END
