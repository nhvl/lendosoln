﻿-- =============================================
-- Author:		Eric Mallare
-- Create date: 9/12/2016
-- Description:	Revokes the broker client certificate
-- =============================================
ALTER PROCEDURE [dbo].[RevokeBrokerClientCertificate] 
	@BrokerId uniqueidentifier,
	@CertificateId uniqueidentifier, 
	@RevokedDate smalldatetime
AS
BEGIN
	UPDATE 
		BROKER_CLIENT_CERTIFICATE
	SET 
		IsRevoked='1', RevokedDate=@RevokedDate
	WHERE
		IsRevoked='0' AND
		BrokerId=@BrokerId AND
		CertificateId=@CertificateId
END
