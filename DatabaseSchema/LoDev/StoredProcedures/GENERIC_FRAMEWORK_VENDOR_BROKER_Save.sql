-- =============================================
-- Author:		Timothy Jewell
-- Create date: 9/9/2014
-- Description:	Saves the Broker data for a Generic Framework vendor, inserting if not currently present.
-- =============================================
ALTER PROCEDURE [dbo].[GENERIC_FRAMEWORK_VENDOR_BROKER_Save]
	-- Add the parameters for the stored procedure here
	@BrokerID uniqueidentifier,
	@ProviderID varchar(7),
	@CredentialXML varbinary(MAX) = NULL,
	@EncryptionKeyId uniqueidentifier,
	@LaunchLinkConfigurationXML varchar(MAX)
AS
BEGIN
	UPDATE GENERIC_FRAMEWORK_VENDOR_BROKER_CONFIG
	SET
		CredentialXML = @CredentialXML,
		EncryptionKeyId = @EncryptionKeyId,
		LaunchLinkConfigurationXML = @LaunchLinkConfigurationXML
	WHERE BrokerID = @BrokerID AND ProviderID = @ProviderID

	IF @@ROWCOUNT = 0
		INSERT INTO GENERIC_FRAMEWORK_VENDOR_BROKER_CONFIG
			(BrokerID,   ProviderID,  CredentialXML,  EncryptionKeyId,  LaunchLinkConfigurationXML)
		VALUES
			(@BrokerID, @ProviderID, @CredentialXML, @EncryptionKeyId, @LaunchLinkConfigurationXML)
END
