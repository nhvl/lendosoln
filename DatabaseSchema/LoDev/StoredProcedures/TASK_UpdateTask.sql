ALTER PROCEDURE [dbo].[TASK_UpdateTask] 
	@BrokerId	uniqueidentifier
	, @TaskId varchar(10)
	, @TaskAssignedUserId	uniqueidentifier
	, @TaskStatus	tinyint
	, @LoanId	uniqueidentifier
	, @TaskOwnerUserId	uniqueidentifier
	, @TaskSubject	varchar(3000)
	, @TaskDueDate	smalldatetime = null
	, @TaskFollowUpDate	smalldatetime = null
	, @CondInternalNotes varchar(1000) = ''
	, @TaskIsCondition	bit
	, @TaskResolvedUserId	uniqueidentifier = null
	, @TaskToBeAssignedRoleId	uniqueidentifier = null
	, @TaskToBeOwnerRoleId	uniqueidentifier = null
	, @TaskCreatedByUserId	uniqueidentifier
	, @TaskDueDateLocked	bit = 0
	, @TaskDueDateCalcField	varchar(max) = ''
	, @TaskDueDateCalcDays	int = 0
	, @TaskHistoryXml varchar(max)  = ''
	, @TaskPermissionLevelId int
	, @TaskClosedUserId	uniqueidentifier = null
	, @TaskClosedDate datetime = null
	, @CondCategoryId int = null
	, @CondIsHidden	bit
	--, @CondRank	decimal
	, @CondRowId int = 0
	, @CondDeletedDate	datetime = null
	, @CondDeletedByUserNm	varchar(60)  = ''
	, @CondIsDeleted	bit
	, @TaskRowVersion binary(8) = null
	, @TaskAssignedUserFullName varchar(60) = '' --Remove these default
	, @TaskOwnerFullName varchar(60) = ''
	, @TaskClosedUserFullName varchar(60) = ''
	, @CondMessageId varchar(10) = ''
	, @CondRequiredDocTypeId int = null
	, @ResolutionBlockTriggerName varchar(200) = ''
	, @ResolutionDenialMessage varchar(1000) = ''
	, @ResolutionDateSetterFieldId varchar(100) = ''
	, @AssignToOnReactivationUserId uniqueidentifier = null
AS

--REVIEW
--6/9/2011: Reviewed. -ThinhNK
--7/5/2012: Added static condition row IDs. -MP
	IF @TaskIsCondition = 1  
	BEGIN
		UPDATE LOAN_FILE_F 
		SET
			sConditionLastModifiedD = GETDATE()
		WHERE 
			sLId = @LoanId 
		
		IF @CondRowId = 0
		BEGIN
			SET @CondRowId = (SELECT MAX(CondRowId) FROM Task WHERE LoanId = @LoanId AND BrokerId = @BrokerId) + 1 -- This could be null if there are no tasks at all in the loan
		END
		
		IF @CondRowId IS NULL
		BEGIN
			SET @CondRowId = 1
		END
	END

	UPDATE TASK
	SET
	  TaskAssignedUserId = @TaskAssignedUserId
	, TaskStatus = @TaskStatus
	, LoanId = @LoanId
	, TaskOwnerUserId = @TaskOwnerUserId
	, TaskSubject = @TaskSubject
	, TaskDueDate = @TaskDueDate
	, TaskFollowUpDate = @TaskFollowUpDate
	, CondInternalNotes  = @CondInternalNotes
	, TaskIsCondition = @TaskIsCondition
	, TaskResolvedUserId = @TaskResolvedUserId
	, TaskLastModifiedDate	= GetDate()
	, TaskToBeAssignedRoleId = @TaskToBeAssignedRoleId
	, TaskToBeOwnerRoleId = @TaskToBeOwnerRoleId	
	, TaskCreatedByUserId = @TaskCreatedByUserId
	, TaskDueDateLocked = @TaskDueDateLocked
	, TaskDueDateCalcField	= @TaskDueDateCalcField
	, TaskDueDateCalcDays	= @TaskDueDateCalcDays
	, TaskHistoryXml = @TaskHistoryXml
	, TaskPermissionLevelId = @TaskPermissionLevelId
	, TaskClosedUserId	= @TaskClosedUserId
	, TaskClosedDate = @TaskClosedDate
	, CondCategoryId = @CondCategoryId 
	, CondIsHidden	= @CondIsHidden 
	--, CondRank = @CondRank
	, CondRowId = @CondRowId
	, CondDeletedDate = @CondDeletedDate
	, CondDeletedByUserNm = @CondDeletedByUserNm
	, CondIsDeleted = @CondIsDeleted
	, TaskAssignedUserFullName = @TaskAssignedUserFullName
	, TaskOwnerFullName = @TaskOwnerFullName
	, TaskClosedUserFullName = @TaskClosedUserFullName
	, CondMessageId = @CondMessageId
	, CondRequiredDocTypeId = @CondRequiredDocTypeId
	, ResolutionBlockTriggerName = @ResolutionBlockTriggerName
	, ResolutionDenialMessage = @ResolutionDenialMessage
	, ResolutionDateSetterFieldId = @ResolutionDateSetterFieldId
	, AssignToOnReactivationUserId = @AssignToOnReactivationUserId
	FROM TASK WITH(ROWLOCK, READPAST, UPDLOCK)
	WHERE
	TaskId = @TaskId
	AND BrokerId = @BrokerId
	AND (@TaskRowVersion IS NULL OR @TaskRowVersion = TaskRowVersion)
	OPTION (TABLE HINT(TASK, ROWLOCK, READPAST, UPDLOCK, INDEX (Idx__BrokerIdAssignedUserId)))
	
	IF 0 != @@ERROR
	BEGIN
		RAISERROR('Error updating TASK in UpdateTask sp', 16, 1);
		RETURN -100
	END
	
	IF 0 = @@rowcount 
	BEGIN
		-- RAISERROR if can select with same condition but without lock
		IF EXISTS ( SELECT TaskRowVersion
							FROM TASK WITH(NOLOCK,INDEX (Idx__BrokerIdAssignedUserId))
							WHERE	
								TaskId = @TaskId
								AND BrokerId = @BrokerId
								AND (@TaskRowVersion IS NULL OR @TaskRowVersion = TaskRowVersion)
				   )						   
		BEGIN
			RAISERROR('Fail to update TASK in UpdateTask sp because prevent deadlock', 16, 1);
			RETURN -100
		END
	END
	

	RETURN 0;
