CREATE PROCEDURE RetrieveDiscNotifByNotifId
	@NotifId Guid,
	@MarkAsRead bit
AS 
declare @tranPoint sysname
set @tranPoint = object_name(@@procId) + CAST(@@nestlevel as varchar(10))
BEGIN TRANSACTION
SAVE TRANSACTION @tranPoint
	if( @MarkAsRead = 1 )
	begin
		update discussion_notification
		set notifisread = 1
		where notifid = @notifid;
	end
	select top 1 *
	from discussion_notification notif join Discussion_Log disc on notif.DiscLogId =  disc.DiscLogId
	WHERE NotifId = @NotifId
	if( 0!=@@error )
	begin
		RAISERROR('error in selecting from discussion_notification  in RetrieveDiscNotifByNotifId sp', 16, 1);
		goto ErrorHandler;
	end
commit transaction
return 0;
ErrorHandler:
	rollback tran @tranPoint
	rollback transaction
	return -100;
