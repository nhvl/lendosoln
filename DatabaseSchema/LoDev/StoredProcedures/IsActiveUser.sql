


CREATE   PROCEDURE [dbo].[IsActiveUser]
	@EmployeeId uniqueidentifier,
	@Type char(1)
AS

if(@Type = 'P')
BEGIN
	SELECT ''
	FROM VIEW_ACTIVE_PML_USER
	WHERE EmployeeId = @EmployeeId
END
else
BEGIN
	SELECT ''
	FROM VIEW_ACTIVE_LO_USER
	WHERE EmployeeId = @EmployeeId
END