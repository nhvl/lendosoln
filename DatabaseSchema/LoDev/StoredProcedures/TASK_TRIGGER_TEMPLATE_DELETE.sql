-- =============================================
-- Author:		Antonio Valencia
-- Create date: 12/16/11
-- Description:	Deletes template
-- =============================================
CREATE PROCEDURE [dbo].[TASK_TRIGGER_TEMPLATE_DELETE]
	-- Add the parameters for the stored procedure here
	@AutoTaskTemplateId int = null,
	@BrokerId uniqueidentifier

			
AS
BEGIN
	DELETE FROM TASK_TRIGGER_TEMPLATE WHERE AutoTaskTemplateId =  @AutoTaskTemplateId
		AND BrokerId = @BrokerId
END
