-- =============================================
-- Author:		Peter Anargirou
-- Create date: 7/16/08
-- Description:	Returns the number of deleted loans associated with a branch
-- =============================================
CREATE PROCEDURE [dbo].[GetNumDeletedLoansAssociatedWithBranchByBranchId]
	@BranchId uniqueidentifier
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		COUNT('') AS numDeletedLoans
	FROM 
		LOAN_FILE_CACHE
	WHERE
		sBranchId = @BranchId AND IsValid = 0
END
