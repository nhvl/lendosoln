-- =============================================
-- Author:		Antonio Valencia
-- Create date: 12/17/13
-- Description:	Creates new Generic EDocs
-- =============================================
ALTER PROCEDURE [dbo].[EDOCS_GENERIC_Create] 
	-- Add the parameters for the stored procedure here
	@DocumentId uniqueidentifier,
	@sLId uniqueidentifier,
	@BrokerId uniqueidentifier,
	@aAppId uniqueidentifier,
	@DocTypeId int,
	@Description varchar(200),
	@InternalComments varchar(500),
	@FileDBKey uniqueidentifier,
	@IsValid bit,
	@UserId uniqueidentifier,
	@FileName varchar(128),
	@FileType int
AS
BEGIN
	INSERT INTO EDOCS_GENERIC (
		DocumentId,
		sLId,
		BrokerId,
		aAppId,
		DocTypeId,
		Description,
		InternalComments,
		CreatedD,
		CreatedByUserId,
		FileDBKey,
		IsValid,
		FileNm,
		FileType
	)
	VALUES (
		@DocumentId,
		@sLid,
		@BrokerId, 
		@aAppId,
		@DocTypeId,
		@Description,
		@InternalComments,
		GETDATE(),
		@UserId,
		@FileDBKey,
		@IsValid,
		@FileName,
		@FileType
	)
END