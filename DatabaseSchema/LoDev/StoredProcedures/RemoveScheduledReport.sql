CREATE procedure [dbo].[RemoveScheduledReport]
@ReportId int,
@ReportType varchar (10)
as 

if(@ReportType ='Daily')
delete from DAILY_REPORTS where ReportId = @ReportId;

else if (@ReportType ='Weekly')
delete from WEEKLY_REPORTS where ReportId = @ReportId;

else if(@ReportType ='Monthly')
delete from MONTHLY_REPORTS where ReportId = @ReportId;

delete from SCHEDULED_REPORT_USER_INFO 
where ReportId = @ReportId;

