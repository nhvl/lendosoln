CREATE procedure
[dbo].[GetLastUserAgentStringAndIsActive]
as
select lastuseragentstring, isactive
from all_user WITH(NOLOCK)