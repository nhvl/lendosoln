CREATE PROCEDURE RetrieveLenderSiteIDByBrokerID 
	@BrokerID Guid
AS
SELECT BrokerPmlSiteID FROM Broker WHERE BrokerID = @BrokerID
