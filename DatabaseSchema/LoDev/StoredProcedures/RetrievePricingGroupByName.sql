
ALTER PROCEDURE [dbo].[RetrievePricingGroupByName] 
	@LpePriceGroupName varchar(100),
	@BrokerId Guid
AS
SELECT 
		BrokerId, LpePriceGroupName, ActualPriceGroupId,
		  ExternalPriceGroupEnabled
		, LpePriceGroupId
		, LenderPaidOriginatorCompensationOptionT
		, LockPolicyID
		, DisplayPmlFeeIn100Format
		, IsRoundUpLpeFee
		, RoundUpLpeFeeToInterval
		, ContentKey
		, IsAlwaysDisplayExactParRateOption
FROM LPE_Price_Group
WHERE LpePriceGroupName = @LpePriceGroupName
      AND BrokerId = @BrokerId

