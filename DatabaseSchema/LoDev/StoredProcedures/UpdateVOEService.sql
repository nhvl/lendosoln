-- =============================================
-- Author:		Eric Mallare
-- Create date: 5/22/2017
-- Description:	Updates a VOE service.
-- =============================================
ALTER PROCEDURE [dbo].[UpdateVOEService]
	@ServiceT int,
	@IsADirectVendor bit,
	@SellsViaResellers bit,
	@IsAReseller bit,
	@VoeVerificationT int,
	@VendorId int,
	@IsEnabled bit,
	@VendorServiceId int,
	@AskForSalaryKey bit
AS
BEGIN
	UPDATE VOE_SERVICE
	SET
		ServiceT=@ServiceT, IsADirectVendor=@IsADirectVendor, SellsViaResellers=@SellsViaResellers, IsAReseller=@IsAReseller, 
		VoeVerificationT=@VoeVerificationT, VendorId=@VendorId, IsEnabled=@IsEnabled, AskForSalaryKey=@AskForSalaryKey
	WHERE
		VendorServiceId=@VendorServiceId

	IF @@error!= 0 GOTO HANDLE_ERROR
END
RETURN;
	HANDLE_ERROR:
    	RAISERROR('Error in UpdateVOEService sp', 16, 1);;
    	RETURN;
