-- =============================================
-- Author:		David Dao
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE dbo.EDOCS_FetchDocsForPolling 
    @BrokerId UniqueIdentifier,
    @sLId UniqueIdentifier
AS
BEGIN

    SELECT DocumentId, ImageStatus, Status, NumPages, TimeImageCreationEnqueued
    FROM EDOCS_DOCUMENT
    WHERE BrokerId = @BrokerId AND sLId = @sLId AND IsValid=1

END
