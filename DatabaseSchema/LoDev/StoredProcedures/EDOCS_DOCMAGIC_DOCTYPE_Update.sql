-- =============================================
-- Author:		Thien Nguyen
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[EDOCS_DOCMAGIC_DOCTYPE_Update]
    @Id int,
    @DocumentClass varchar(200),
    @BarcodeClassification varchar(100)    
AS
BEGIN

	UPDATE EDOCS_DOCMAGIC_DOCTYPE
	SET    DocumentClass = @DocumentClass, BarcodeClassification = @BarcodeClassification
	WHERE Id = @Id
		  
END
