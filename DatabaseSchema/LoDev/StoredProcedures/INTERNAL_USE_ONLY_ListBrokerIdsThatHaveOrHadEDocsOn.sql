CREATE PROCEDURE [dbo].[INTERNAL_USE_ONLY_ListBrokerIdsThatHaveOrHadEDocsOn]
as
select c.BrokerId

from 
(
select distinct BrokerId from Broker with (nolock) where IsEdocsEnabled = 1

union

select distinct BrokerId from EDOCS_DOCUMENT with (nolock) 

union

select distinct BrokerId from EDOCS_DOCUMENT_TYPE  with (nolock) where brokerId is not null

union

select distinct BrokerId from EDOCS_SHIPPING_TEMPLATE  with (nolock) where brokerid is not null

) c
