-- =============================================
-- Author:		Antonio Valencia
-- Create date: July 30 2010
-- Description:	Gets the pml broker id
-- =============================================
CREATE PROCEDURE GetLoanPmlBrokerId 
	-- Add the parameters for the stored procedure here
	@sLId uniqueidentifier 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT sPmlBrokerId FROM LOAN_FILE_CACHE WHERE sLId = @sLId
END
