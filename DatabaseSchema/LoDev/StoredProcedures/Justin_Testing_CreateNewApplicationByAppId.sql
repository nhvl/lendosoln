


CREATE PROCEDURE [dbo].[Justin_Testing_CreateNewApplicationByAppId]  
	@LoanId GUID,
	@AppId GUID
AS
declare @tranPoint sysname
set @tranPoint = object_name(@@procId) + CAST(@@nestlevel as varchar(10))
BEGIN TRANSACTION
SAVE TRANSACTION @tranPoint
declare @Primary Boolean
declare @AppCount int
declare @RankIndex int

select @AppCount = count( * )
from APPLICATION_A with( updlock, rowlock )
where sLId = @LoanId
if( 0!=@@error )
begin
	RAISERROR('error in selecting from application for AppCount in CreateNewApplication sp', 16, 1);
	goto ErrorHandler;
end
if @AppCount = 0 
begin
	set @RankIndex = 0
end
else
	begin
		select @RankIndex = max(PrimaryRankIndex) + 1
		from APPLICATION_A with( updlock, rowlock )
		where sLId = @LoanId
		if( 0!=@@error )
		begin
			RAISERROR('error in selecting from application for RankIndex in CreateNewApplication sp', 16, 1);
			goto ErrorHandler;
		end
	end
-- Automatically populate Interviewer company information in 1003 page 3 from Broker information
INSERT INTO APPLICATION_A with (rowlock)
( sLId, aAppId, PrimaryRankIndex, aCAddrYrs,aBAddrYrs ,aCrN,aBusCrN,aU1DocStatN,aU2DocStatN,aCGender, aBGender,aBDecJudgment,aCDecJudgment,aBDecBankrupt,aCDecBankrupt,aBDecForeclosure,aCDecForeclosure,aBDecLawsuit,aCDecLawsuit,aBDecObligated,aCDecObligated,aBDecDelinquent,aCDecDelinquent,aBDecAlimony,aCDecAlimony,aBDecBorrowing,aCDecBorrowing,aBDecEndorser,aCDecEndorser,aCDecCitizen,aBDecCitizen,aBDecResidency,aCDecResidency,aBDecOcc,aCDecOcc,aBDecPastOwnership,aCDecPastOwnership,aCPrev2AddrYrs,aBPrev2AddrYrs,aCPrev1AddrYrs,aBPrev1AddrYrs )
SELECT @LoanId, @AppId, @RankIndex , '','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','' 
FROM 	Loan_File_cache l with( updlock, rowlock ) 
	JOIN Broker br with( updlock, rowlock ) ON br.BrokerID = l.sBrokerID 
WHERE l.sLId = @LoanID
if( 0!=@@error )
begin
	RAISERROR('error in inserting to APPLICATION_A table in CreateNewApplication sp', 16, 1);
	goto ErrorHandler;
end
INSERT INTO Application_B with (rowlock)
(sLId, aAppId) VALUES (@LoanID, @AppId)
if( 0!=@@error )
begin
	RAISERROR('error in inserting to APPLICATION_B table in CreateNewApplication sp', 16, 1);
	goto ErrorHandler;
end
commit transaction
return 0;
ErrorHandler:
	rollback tran @tranPoint
	rollback transaction
	return -100;

