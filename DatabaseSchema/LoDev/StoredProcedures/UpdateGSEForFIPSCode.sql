


-- =============================================
-- Author:		Antonio Valencia
-- Create date: 01-06-08
-- Description:	Updates An Entry in the GSE Table --- This is identical to FSH one 
-- =============================================
CREATE PROCEDURE [dbo].[UpdateGSEForFIPSCode] 
	@FIPSCode int,
	@Limit1Units money,
	@Limit2Units money,
	@Limit3Units money,
	@Limit4Units money, 
	@LastRevised datetime,
	@EffectiveDate datetime
AS
BEGIN
	DECLARE @rows_affected int 
	DECLARE @NewNumber VARCHAR(1000)

	SET @NewNumber = CAST( @FIPSCode AS VARCHAR(100) )
 
	UPDATE CONFORMING_LOAN_LIMITS 
		SET Limit1Units = @Limit1Units, 
			Limit2Units = @Limit2Units, 
			Limit3Units = @Limit3Units, 
			Limit4Units = @Limit4Units,
			LastRevised = @LastRevised
		WHERE 
			FipsCountyCode = @FIPSCode
			AND EffectiveDate = @EffectiveDate;			
	
	SELECT  @rows_affected = @@rowcount 
	
	IF @rows_affected = 0	
	BEGIN
		SELECT FipsCountyCode FROM FIPS_COUNTY WHERE FipsCountyCode = @FipsCode
		SELECT  @rows_affected = @@rowcount 
	

		IF @rows_affected = 0 
		BEGIN 
			RAISERROR ( 'The Specified Fips Code does not exist %s', 16, 1,  @NewNumber )
			return 0 
		END 
		ELSE 
		BEGIN 
			INSERT INTO CONFORMING_LOAN_LIMITS ( 
					FipsCountyCode,	Limit1UNits, Limit2Units, Limit3Units,	Limit4Units, LastRevised, EffectiveDate) 
				VALUES ( 
						@FipsCode, @Limit1Units, @Limit2Units, @Limit3Units, @Limit4Units, @LastRevised, @EffectiveDate ) 
			SELECT  @rows_affected = @@rowcount 
	
			IF @rows_affected = 0
			BEGIN 
				RAISERROR ( 'Could not insert data with fips code %s.',  16, 1, @NewNumber )
			END
		END

		
	END
	
END



