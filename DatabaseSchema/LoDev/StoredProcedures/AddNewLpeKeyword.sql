




CREATE  PROCEDURE [dbo].[AddNewLpeKeyword]
  @Keyword      varchar(100), 
  @DefaultValue varchar(100),
  @Category     varchar(50)
AS
  INSERT INTO LPE_NEW_KEYWORD ( NewKeywordName, DefaultValue, Category )
  VALUES ( @Keyword, @DefaultValue, @Category )
