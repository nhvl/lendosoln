CREATE PROCEDURE [dbo].[RetrieveLqiCreditReport] 
	@ApplicationID uniqueidentifier

AS
	SELECT TOP 1 ExternalFileID, DbFileKey, ComId, CrAccProxyId, TimeVersion
	FROM SERVICE_FILE
	WHERE	Owner = @ApplicationID
        AND IsInEffect = 1
		AND ServiceType = 'Credit'
		AND FileType = 'LQICreditReport'
	ORDER BY TimeVersion DESC
	
	if( 0!=@@error)
	begin
		RAISERROR('Error in the select statement in RetrieveLqiCreditReport sp', 16, 1);
		return -100;
	end
	return 0;
