ALTER PROCEDURE [dbo].[RetrieveCrabyMclCraCode] 
	@CraCode varchar(2) = null
AS
BEGIN
		SET NOCOUNT ON;

		SELECT MclCraCode, ComNm, ComUrl, ProtocolType, IsValid, ComId, IsInternalOnly, VoxVendorId
		FROM Service_Company WITH (NOLOCK)
		WHERE	MclCraCode = COALESCE(@CraCode, MclCraCode)
				AND LEN(MclCraCode) > 0
		ORDER BY MclCraCode
END
