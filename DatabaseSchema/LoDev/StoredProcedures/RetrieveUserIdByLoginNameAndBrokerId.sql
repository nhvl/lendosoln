-- =============================================
-- Author:		Antonio Valencia
-- Create date: 8/31/11
-- Description:	Gets user id ofuser with givenname
-- =============================================
CREATE PROCEDURE RetrieveUserIdByLoginNameAndBrokerId 
	@LoginName varchar(36),
	@BrokerId uniqueidentifier,
	@UserType varchar(1)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF @UserType = 'B' BEGIN 
			SELECT UserId
		FROM VIEW_ACTIVE_LO_USER_WITH_BROKER_AND_LICENSE_INFO
		WHERE LoginNm = @LoginName AND BrokerId = @BrokerId
		
	END 

	ELSE IF @UserType = 'P' begin 
		SELECT UserId
		FROM View_Active_Pml_User_With_Broker_Info
		WHERE LoginNm = @LoginName AND BrokerId = @BrokerId
	End
END
