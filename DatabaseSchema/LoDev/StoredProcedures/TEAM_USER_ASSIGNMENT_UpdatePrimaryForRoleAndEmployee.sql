/*
	Author: Scott Kibler
	Created Date: 12/18/2015
	Decription:
		A user may only have one team assigned as the primary team for a given role.
		This procedure sets the team specified as the primary for the role, 
		and unsets any other primary team for that user and role, at the same time (which is important).
*/
CREATE PROCEDURE [dbo].[TEAM_USER_ASSIGNMENT_UpdatePrimaryForRoleAndEmployee]
	@TargetPrimaryTeamID			uniqueidentifier,
	@EmployeeID		uniqueidentifier
AS
	DECLARE @RoleID uniqueidentifier
	DECLARE @CurrentPrimaryTeamIDForRole uniqueidentifier
	
	SELECT	@RoleID = RoleID
	FROM	TEAM
	WHERE	ID = @TargetPrimaryTeamID 
	
	-- This assumes two things:
	-- 1) that there is already a primary team for the role (which is becoming an enforced rule.)
	-- 2) that there is no more than one existing primary team for the role (which is also becoming an enforced rule.)
	SELECT @CurrentPrimaryTeamIDForRole = TeamID
	FROM TEAM_USER_ASSIGNMENT tua JOIN TEAM t ON t.ID = tua.TeamID
	WHERE
			t.RoleID = @RoleID
		AND tua.EmployeeID = @EmployeeID
		AND tua.IsPrimary = 1
		
	IF (@CurrentPrimaryTeamIDForRole != @TargetPrimaryTeamID)
	BEGIN
		UPDATE TEAM_USER_ASSIGNMENT
		SET IsPrimary = ~IsPrimary
		WHERE 
				(TeamID = @TargetPrimaryTeamID OR TeamID = @CurrentPrimaryTeamIDForRole)
			AND EmployeeID = @EmployeeID
	END
		
