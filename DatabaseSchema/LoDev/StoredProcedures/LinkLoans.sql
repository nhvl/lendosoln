CREATE PROCEDURE [dbo].[LinkLoans]
(
	@sLId1 as uniqueidentifier,
	@sLId2 as uniqueidentifier,
	@brokerID as uniqueidentifier
)
as  
BEGIN TRANSACTION
	execute UnlinkLoans @sLId1;
	IF( 0 !=@@ERROR )
	BEGIN
		RAISERROR('Error in the execute UnlinkLoans statement in LinkLoans sp', 16, 1);
		ROLLBACK TRANSACTION
		RETURN -100;
	END
	execute UnlinkLoans @sLId2;
	IF( 0 !=@@ERROR )
	BEGIN
		RAISERROR('Error in the execute UnlinkLoans statement in LinkLoans sp', 16, 1);
		ROLLBACK TRANSACTION
		RETURN -100;
	END
	declare @sLNm1 as varchar(100)
	declare @sBrokerId1 as uniqueidentifier
	select @sLNm1 = sLNm, @sBrokerId1 = sBrokerId
	from loan_file_cache
	where slid = @sLId1
	IF( 0 !=@@ERROR )
	BEGIN
		RAISERROR('Error in the select @sLNm1 statement in LinkLoans sp', 16, 1);
		ROLLBACK TRANSACTION
		RETURN -100;
	END
	declare @sLNm2 as varchar(100)	
	declare @sBrokerId2 as uniqueidentifier
	select @sLNm2 = sLNm, @sBrokerId2 = sBrokerId
	from loan_file_cache
	where slid = @sLId2 
	IF( 0 !=@@ERROR )
	BEGIN
		RAISERROR('Error in the select @sLNm2 statement in LinkLoans sp', 16, 1);
		ROLLBACK TRANSACTION
		RETURN -100;
	END
	IF( @brokerID != @sBrokerId1 )
	BEGIN
		RAISERROR('First loan does not match broker of user.', 16, 1);
		ROLLBACK TRANSACTION
		RETURN -100;
	END
	IF( @sBrokerId1 != @sBrokerId2 )
	BEGIN
		RAISERROR('First loan does not match broker of second loan.', 16, 1);
		ROLLBACK TRANSACTION
		RETURN -100;
	END
	
	insert into loan_link
	( sLId1, sLId2, sBrokerId, sLNm1, sLNm2 )
	values	
	( @sLId1, @sLId2, @brokerID, @sLNm1, @sLNm2 )
	IF( 0 !=@@ERROR )
	BEGIN
		RAISERROR('Error in the insert into loan_link statement in LinkLoans sp', 16, 1);
		ROLLBACK TRANSACTION
		RETURN -100;
	END
	COMMIT TRANSACTION
	RETURN 0;
