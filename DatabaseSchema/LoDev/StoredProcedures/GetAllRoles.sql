-- =============================================
-- Author:		Antonio Valencia
-- Create date: 4/28/11
-- Description:	Get All Roles
-- =============================================
CREATE PROCEDURE [dbo].[GetAllRoles] 

AS
BEGIN

	SELECT RoleId, RoleDesc, RoleModifiableDesc, RoleImportanceRank, RoleDefaultPipelineReportId 
	FROM ROLE order by RoleImportanceRank ASC
END
