CREATE PROCEDURE GetBranchByEmployeeID
	@EmployeeID Guid
AS
	SELECT BranchID FROM Employee WITH (NOLOCK) WHERE EmployeeID = @EmployeeID
