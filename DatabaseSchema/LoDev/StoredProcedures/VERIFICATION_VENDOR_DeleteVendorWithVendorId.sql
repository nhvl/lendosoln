-- =============================================
-- Author:		Eric Mallare
-- Create date: 5/2/2017
-- Description:	Deletes a Vendor. 
-- Remarks: YOU MUST CHECK IF ANY ROWS IN VERIFICATION_LENDER_SERVICE USE THIS SERVICE BEFORE THIS IS CALLED!
-- =============================================
CREATE PROCEDURE [dbo].[VERIFICATION_VENDOR_DeleteVendorWithVendorId]
	@VendorId int
AS
BEGIN TRANSACTION
	DECLARE @TransmissionId int
	SET @TransmissionId = (SELECT v.OverridingTransmissionId FROM VERIFICATION_VENDOR v WHERE v.VendorId=@VendorId)
	
	DELETE FROM
		VOA_SERVICE
	WHERE
		VendorId=@VendorId

	DELETE FROM
		VOE_SERVICE
	WHERE
		VendorId=@VendorId

	DELETE FROM
		SSA89_SERVICE
	WHERE
		VendorId=@VendorId

	DELETE FROM
		VERIFICATION_VENDOR
	WHERE
		VendorId=@VendorId

	DELETE FROM
		VERIFICATION_TRANSMISSION
	WHERE
		TransmissionId=@TransmissionId

	if( 0!=@@error)
	BEGIN
		ROLLBACK TRANSACTION
		RAISERROR('Error in delete in VERIFICATION_VENDOR_DeleteVendorWithVendorId sp', 16, 1);
		RETURN -100;
	END
COMMIT TRANSACTION
RETURN;
