ALTER PROCEDURE [dbo].[ListReportQuery]
	@BrokerID uniqueidentifier ,
	@QueryName Varchar( 64 ) = NULL ,
	@IncludeEmployeeId uniqueidentifier = NULL ,
	@ExcludeEmployeeId uniqueidentifier = NULL ,
	@IsPublished Bit = NULL,
	@IncludeDisabledUsers Bit = 0,
	@RequestingEmployeeId uniqueidentifier = NULL,
	@IncludeXmlContent bit = true
AS
IF( @IncludeXmlContent = 1)
begin
	SELECT
		r.QueryId, 
		r.BrokerId,
		r.EmployeeId,
		r.QueryName,
		r.IsPublished,
		r.NamePublishedAs,
		r.ShowPosition,
		r.XmlContent, 
		e.UserFirstNm + ' ' + e.UserLastNm AS EmployeeName, 
		e.IsActive as IsActiveUser,
		CASE WHEN f.EmployeeId IS NULL THEN 0 ELSE 1 END AS IsFavorite
	FROM
		Report_Query AS r INNER JOIN Employee AS e
			ON e.EmployeeId = r.EmployeeId
		INNER JOIN BROKER_USER b
			ON b.EmployeeId = e.EmployeeId
		INNER JOIN ALL_USER a
			ON a.UserId = b.UserId
		LEFT JOIN EMPLOYEE_FAVORITE_REPORTS f ON f.QueryId = r.QueryId AND f.EmployeeId = @RequestingEmployeeId
	WHERE
		r.IsPublished = COALESCE( @IsPublished , r.IsPublished )
		AND
		r.QueryName = COALESCE( @QueryName , r.QueryName )
		AND
		r.EmployeeId = COALESCE( @IncludeEmployeeId , r.EmployeeId )
		AND
		r.BrokerId = @BrokerID
		
		AND
		(
			@ExcludeEmployeeId IS NULL
			OR
			r.EmployeeId != @ExcludeEmployeeId
		)
		AND ( @IncludeDisabledUsers = 1	OR	(e.IsActive = 1 AND a.IsActive = 1))
	ORDER BY
		r.QueryName
end
ELSE
begin
	SELECT
		r.QueryId, 
		r.BrokerId,
		r.EmployeeId,
		r.QueryName,
		r.IsPublished,
		r.NamePublishedAs,
		r.ShowPosition,
		--r.XmlContent, 
		e.UserFirstNm + ' ' + e.UserLastNm AS EmployeeName, 
		e.IsActive as IsActiveUser,
		CASE WHEN f.EmployeeId IS NULL THEN 0 ELSE 1 END AS IsFavorite
		FROM
			Report_Query AS r INNER JOIN Employee AS e
				ON e.EmployeeId = r.EmployeeId
			INNER JOIN BROKER_USER b
				ON b.EmployeeId = e.EmployeeId
			INNER JOIN ALL_USER a
				ON a.UserId = b.UserId
			LEFT JOIN EMPLOYEE_FAVORITE_REPORTS f ON f.QueryId = r.QueryId AND f.EmployeeId = @RequestingEmployeeId
		WHERE
			r.IsPublished = COALESCE( @IsPublished , r.IsPublished )
			AND
			r.QueryName = COALESCE( @QueryName , r.QueryName )
			AND
			r.EmployeeId = COALESCE( @IncludeEmployeeId , r.EmployeeId )
			
			AND
			r.BrokerId = @BrokerID
			
			AND
			(
				@ExcludeEmployeeId IS NULL
				OR
				r.EmployeeId != @ExcludeEmployeeId
			)
			AND ( @IncludeDisabledUsers = 1	OR	(e.IsActive = 1 AND a.IsActive = 1))
		ORDER BY
			r.QueryName
end
	IF ( @@error != 0 )
	BEGIN
		RAISERROR('Error in the select statement in ListReportQuery sp', 16, 1);
		RETURN -100;
	END

GO

