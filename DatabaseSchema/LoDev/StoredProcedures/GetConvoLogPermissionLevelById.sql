-- ==========================================================================
-- Author: Scott Kibler
-- Create date: 5/10/2017
-- Description: 
--  Gets the convolog permission level info from the id.
-- ==========================================================================
CREATE PROCEDURE [dbo].[GetConvoLogPermissionLevelById]
	@Id int
AS
BEGIN
	SELECT BrokerId, IsActive, Name, Description
	FROM
		CONVOLOG_PERMISSION_LEVEL
	WHERE ID = @ID
END