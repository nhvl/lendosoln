







CREATE  PROCEDURE [dbo].[UpdateLoanCondition]
	@CondId uniqueidentifier,
	@LoanId uniqueidentifier,
	@CondDesc [varchar] (3000),
	@CondStatus [int],
	@CondStatusDate datetime,
	@IsLoanStillValid [bit],
	@CondCategoryDesc varchar( 100 ),
	@CondOrderedPosition int,
	@CondIsHidden bit = 0,
	@CondIsValid bit = 1,
	@CondCompletedByUserNm varchar(60) = '',
	@CondNotes varchar(1000) = '',
	@CondDeleteD datetime = null,
	@CondDeleteByUserNm varchar(60) = '',
	@IsDirty bit 
AS
UPDATE Condition
SET
	CondDesc = @CondDesc
	, CondStatus = @CondStatus
	, CondStatusDate = @CondStatusDate
	, IsLoanStillValid = @IsLoanStillValid
	, CondCategoryDesc = @CondCategoryDesc
	, CondOrderedPosition = @CondOrderedPosition
	, CondIsHidden = @CondIsHidden
    , CondIsValid = @CondIsValid
	, CondCompletedByUserNm = @CondCompletedByUserNm
	, CondNotes = @CondNotes
	, CondDeleteD = @CondDeleteD
	, CondDeleteByUserNm = @CondDeleteByUserNm
WHERE CondId = @CondId and LoanId = @LoanId
	


	if(0!=@@error)
	begin
		RAISERROR('Error in the update statement in UpdateLoanCondition sp', 16, 1);
		return -100;
	end

	IF @IsDirty = 1  
	BEGIN
	
			UPDATE LOAN_FILE_F
		SET sConditionLastModifiedD = GETDATE()
		WHERE sLId = @LoanId
	END 

	return 0;








