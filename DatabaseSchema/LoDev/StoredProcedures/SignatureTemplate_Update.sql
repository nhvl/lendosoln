-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE SignatureTemplate_Update
	@BrokerId UniqueIdentifier,
	@OwnerUserId UniqueIdentifier,
	@TemplateId UniqueIdentifier,
	@Name varchar(100),
	@FieldMetadata varchar(max)
AS
BEGIN
	UPDATE LO_SIGNATURE_TEMPLATE
	SET Name = @Name,
		FieldMetadata = @FieldMetadata
	WHERE TemplateId = @TemplateId
	  AND BrokerId = @BrokerId
	  AND OwnerUserId = @OwnerUserId
END
