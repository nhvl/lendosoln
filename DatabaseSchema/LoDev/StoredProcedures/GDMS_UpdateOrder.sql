-- =============================================
-- Author:		paoloa
-- Create date:	5/31/2013
-- Description:	Updates a GDMS order
-- =============================================
ALTER PROCEDURE [dbo].[GDMS_UpdateOrder]
	@AppraisalOrderId uniqueidentifier,
	@FileNumber int,
	@BrokerId uniqueidentifier = null,
	@UserId uniqueidentifier = null,
	@EmployeeId uniqueidentifier = null,
	@LoanId uniqueidentifier = null,
	@VendorId uniqueidentifier = null,
	@UploadedFileIdXmlContent varchar(max) = null,
	-- APPRAISAL_ORDER FIELDS
	@IsValid bit,
	@AppraisalOrderType int,
	@ValuationMethod int,
	@DeliveryMethod int,
	@AppraisalFormType int,
	@ProductName varchar(100),
	@OrderNumber varchar(50),
	@FhaDocFileId varchar(50),
	@UcdpAppraisalId varchar(50),
	@Notes varchar(max),
	@OrderedDate smalldatetime,
	@NeededDate smalldatetime,
	@ExpirationDate smalldatetime,
	@ReceivedDate smalldatetime,
	@SentToBorrowerDate smalldatetime,
	@BorrowerReceivedDate smalldatetime,
	@RevisionDate smalldatetime,
	@ValuationEffectiveDate smalldatetime,
	@SubmittedToFha smalldatetime,
	@SubmittedToUcdp smalldatetime,
	@CuRiskScore decimal(4,1),
	@OvervaluationRiskT int,
	@PropertyEligibilityRiskT int,
	@AppraisalQualityRiskT int,
	@DeletedDate smalldatetime = null,
	@DeletedByUserId uniqueidentifier = null,
	@AppIdAssociatedWithProperty uniqueidentifier = null,
	@LinkedReoId uniqueidentifier = null,
	@PropertyAddress varchar(60) = null,
	@PropertyCity varchar(72) = null,
	@PropertyState varchar(2) = null,
	@PropertyZip varchar(5) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE GDMS_ORDER
	SET 
		AppraisalOrderId = COALESCE(@AppraisalOrderId, AppraisalOrderId),
		BrokerId = COALESCE(@BrokerId, BrokerId),
		UserId = COALESCE(@UserId, UserId),
		EmployeeId = COALESCE(@EmployeeId, EmployeeId),
		sLId = COALESCE(@LoanId, sLId),
		VendorId = COALESCE(@VendorId, VendorId),
		UploadedFileIdXmlContent = COALESCE(@UploadedFileIdXmlContent, UploadedFileIdXmlContent)
	WHERE
		FileNumber = @FileNumber

	EXEC APPRAISAL_ORDER_Update
		@AppraisalOrderId,
		@LoanId,
		@BrokerId,
		@IsValid,
		@AppraisalOrderType,
		@ValuationMethod,
		@DeliveryMethod,
		@AppraisalFormType,
		@ProductName,
		@OrderNumber,
		@FhaDocFileId,
		@UcdpAppraisalId,
		@Notes,
		@OrderedDate,
		@NeededDate,
		@ExpirationDate,
		@ReceivedDate,
		@SentToBorrowerDate,
		@BorrowerReceivedDate,
		@RevisionDate,
		@ValuationEffectiveDate,
		@SubmittedToFha,
		@SubmittedToUcdp,
		@CuRiskScore,
		@OvervaluationRiskT,
		@PropertyEligibilityRiskT,
		@AppraisalQualityRiskT,
		@DeletedDate,
		@DeletedByUserId,
		@AppIdAssociatedWithProperty,
		@LinkedReoId,
		@PropertyAddress,
		@PropertyCity,
		@PropertyState,
		@PropertyZip
END