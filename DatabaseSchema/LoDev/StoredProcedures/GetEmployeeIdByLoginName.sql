


CREATE PROCEDURE [dbo].[GetEmployeeIdByLoginName]
	@LoginName varchar(36),
	@BrokerId UniqueIdentifier = NULL
AS
	SELECT EmployeeId, IsActive, UserId 
	FROM View_Active_LO_User WITH (NOLOCK)
	WHERE LoginNm = @LoginName AND Type = 'B' AND BrokerId = COALESCE(@BrokerId, BrokerId)
UNION
	SELECT EmployeeId, IsActive,UserId
	FROM View_Disabled_LO_User WITH (NOLOCK) 
	WHERE LoginNm = @LoginName AND Type = 'B' AND BrokerId = COALESCE(@BrokerId, BrokerId)




	



