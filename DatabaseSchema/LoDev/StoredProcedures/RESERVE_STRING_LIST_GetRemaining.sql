-- =============================================
-- Author:		David Dao
-- Create date: 6/17/2013
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE dbo.RESERVE_STRING_LIST_GetRemaining
	@BrokerId UniqueIdentifier,
	@Type varchar(10)
AS
BEGIN

	SELECT COUNT(*) AS RemainingCount
	FROM RESERVE_STRING_LIST
	WHERE BrokerId=@BrokerId AND Type=@Type
END
