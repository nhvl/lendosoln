ALTER PROCEDURE [dbo].[TrackIntegrationModifiedFile_AddAllFilesToModifiedList]
  @BrokerId uniqueidentifier,
  @AppCode uniqueidentifier,
  @NewestLastModified datetime,
  @MarkValidLoans bit = 1
AS
BEGIN

  DECLARE @LoansToAdd xml;
  SET @LoansToAdd = (
  SELECT sLId
  FROM LOAN_FILE_CACHE with(nolock)
  WHERE sBrokerId = @BrokerId
    AND IsValid = @MarkValidLoans
    AND IsTemplate = 0
    AND sLoanFileT = 0
  FOR XML RAW, TYPE, ROOT)  

  EXEC [dbo].[TrackIntegrationModifiedFile_AddFilesByIdList] 
    @BrokerId = @BrokerId,
    @AppCode = @AppCode,
    @LoanIdListXml = @LoansToAdd,
    @NewestLastModified = @NewestLastModified

END
