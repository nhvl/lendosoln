-- =============================================
-- Author: Scott Kibler
-- Create date: 5/9/2017
-- Description: 
--	Gets the permission levels ordinals per broker.
--  It passes the task of ordering to the datalayer to determine.
-- =============================================
CREATE PROCEDURE [dbo].[GetConvoLogPermissionLevelsOrder]
	@BrokerId uniqueidentifier
AS
BEGIN

	SELECT 
		Ordinal, 
		PermissionLevelId
	FROM 
		CONVOLOG_PERMISSION_LEVEL_ORDER
	WHERE 
		BrokerId = @BrokerId
		
END