-- =============================================
-- Author:		Timothy Jewell
-- Create date: 06/13/14
-- Description:	Saves data on Broker App Code for tracking modified files
-- =============================================
CREATE PROCEDURE [dbo].[BROKER_WEBSERVICE_APP_CODE_Save]
	@BrokerId uniqueidentifier,
	@AppCode uniqueidentifier = NULL,
	@AppName varchar(200),
	@ContactName varchar(200),
	@ContactEmail varchar(100),
	@Notes varchar(MAX),
	@Enabled bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF @AppCode IS NULL
	BEGIN
		INSERT INTO BROKER_WEBSERVICE_APP_CODE
				(BrokerId, AppName, ContactName, ContactEmail, Notes, Enabled)
		OUTPUT INSERTED.AppCode
		VALUES (@BrokerId,@AppName,@ContactName,@ContactEmail,@Notes,@Enabled)
	END
	ELSE
	BEGIN
		UPDATE BROKER_WEBSERVICE_APP_CODE
		SET
			AppName = @AppName,
			ContactName = @ContactName,
			ContactEmail = @ContactEmail,
			Notes = @Notes,
			Enabled = @Enabled
		OUTPUT @AppCode AS AppCode
		WHERE
			BrokerId = @BrokerId
			AND AppCode = @AppCode
	END
END
