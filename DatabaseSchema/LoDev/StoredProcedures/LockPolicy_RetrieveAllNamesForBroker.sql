-- =============================================
-- Author:		Scott Kibler
-- Create date: 4/24/2013
-- Description:	Get just the names and ids of the lock policies.
-- =============================================
CREATE PROCEDURE [dbo].[LockPolicy_RetrieveAllNamesForBroker]
	-- Add the parameters for the stored procedure here
	@BrokerId uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT LockPolicyId, PolicyNm FROM LENDER_LOCK_POLICY
	WHERE BrokerId = @BrokerId AND NOT PolicyNm = ''
END
