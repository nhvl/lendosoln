




ALTER PROCEDURE [dbo].[Pml_ValidateMortgageLeagueAuthentication] 
	@LoginName varchar(36),
	@BrokerPmlSiteID uniqueidentifier
AS
	SELECT a.EmployeeId, a.BrokerId, a.Permissions, a.BranchId, a.LastUsedCreditProtocolID, 
                            a.UserFirstNm, a.UserLastNm, a.Email, a.UserId, a.LoginNm, a.LoginSessionId, a.IsSharable, a.PmlLoginSessionId,
		 a.IsPmlManager, a.PmlExternalManagerEmployeeId, a.IsOthersAllowedToEditUnderwriterAssignedFile,
		a.IsLOAllowedToEditProcessorAssignedFile, a.Type, a.LpePriceGroupId,	a.IsRateLockedAtSubmission, a.IsOnlyAccountantCanModifyTrustAccount,a.HasLenderDefaultFeatures,
		a.LastUsedCreditLoginNm, a.PasswordExpirationD, a.IsQuickPricerEnable, b.IsQuickPricerEnabled AS IsUserQuickPricerEnabled
		, b.IsPricingMultipleAppsSupported, b.PmlBrokerId, b.PmlLevelAccess, b.IsNewPmlUIEnabled, b.IsUsePml2AsQuickPricer, b.PortalMode,
		a.MiniCorrespondentBranchId, a.CorrespondentBranchId
	FROM VIEW_ACTIVE_PML_USER_WITH_BROKER_INFO a WITH (NOLOCK) JOIN Broker_User b WITH(NOLOCK) ON a.UserId = b.UserId
               WHERE LoginNm = @LoginName  AND
		BrokerPmlSiteID = @BrokerPmlSiteID





