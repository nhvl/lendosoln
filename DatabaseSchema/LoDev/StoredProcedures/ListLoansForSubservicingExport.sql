-- =============================================
-- Author:		Paolo Arrastia
-- Create date: 22 Aug 2012
-- Description:	
-- 2/25/13 - Added support for new roles. GF
-- =============================================
CREATE PROCEDURE [dbo].[ListLoansForSubservicingExport] 
	-- Add the parameters for the stored procedure here
	@brokerId uniqueidentifier,
	@branchId uniqueidentifier = null,
	@employeeId uniqueidentifier = null,
	@FundD datetime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT lc.sLId, sLNm, aBFirstNm, aBLastNm, sFinalLAmt, sFundD, sLPurchaseD, sStatusT, sLT
	FROM LOAN_FILE_CACHE lc JOIN LOAN_FILE_CACHE_2 lc2 ON lc.sLId = lc2.sLId JOIN LOAN_FILE_CACHE_3 lc3 ON lc.sLId = lc3.sLId
	WHERE (sBrokerId = @brokerId) AND ( IsValid = 1 ) AND ( IsTemplate = 0 ) AND (sFundD >= @FundD)
	AND 
	(
		(@employeeId IS NULL)
		OR
		(@branchId = sBranchId)
		OR
		(
			sEmployeeLenderAccExecId = @employeeId 
			or sEmployeeLockDeskId = @employeeId 
			or sEmployeeUnderwriterId = @employeeId 
			or sEmployeeRealEstateAgentId = @employeeId 
			or sEmployeeCallCenterAgentId = @employeeId 
			or sEmployeeLoanOpenerId = @employeeId 
			or sEmployeeLoanRepId = @employeeId 
			or sEmployeeProcessorId = @employeeId 
			or sEmployeeManagerId = @employeeId 
			or sEmployeeCloserId = @employeeId 
			or lc3.sEmployeeShipperId = @employeeId 
			or lc3.sEmployeeFunderId = @employeeId 
			or lc3.sEmployeePostCloserId = @employeeId 
			or lc3.sEmployeeInsuringId = @employeeId 
			or lc3.sEmployeeCollateralAgentId = @employeeId 
			or lc3.sEmployeeDocDrawerId = @employeeId 
			or lc3.sEmployeeCreditAuditorId = @employeeId 
			or lc3.sEmployeeDisclosureDeskId = @employeeId 
			or lc3.sEmployeeJuniorProcessorId = @employeeId 
			or lc3.sEmployeeJuniorUnderwriterId = @employeeId 
			or lc3.sEmployeeLegalAuditorId = @employeeId 
			or lc3.sEmployeeLoanOfficerAssistantId = @employeeId 
			or lc3.sEmployeePurchaserId = @employeeId 
			or lc3.sEmployeeQCComplianceId = @employeeId 
			or lc3.sEmployeeSecondaryId = @employeeId 
			or lc3.sEmployeeServicingId = @employeeId
		)
	)
	ORDER BY sFundD DESC
END
