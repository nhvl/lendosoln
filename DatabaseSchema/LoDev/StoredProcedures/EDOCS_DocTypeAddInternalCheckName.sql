CREATE procedure [dbo].[EDOCS_DocTypeAddInternalCheckName]
	@DocTypeName varchar(50),
	@FolderId int
as

select 1 
	from EDOCS_DOCUMENT_TYPE with (nolock)
	where DocTypeName = @DocTypeName
		and BrokerId is null
		and IsValid = 1