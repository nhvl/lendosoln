-- =============================================
-- Author:		Michael Leinweaver
-- Create date: 8/31/2017
-- Description:	Retrieves system OCR doc types.
-- =============================================
CREATE PROCEDURE [dbo].[EDOCS_OCR_SYSTEM_DOC_TYPES_Retrieve]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT Id, Name
	FROM EDOCS_OCR_SYSTEM_DOC_TYPES
	WHERE Enabled = 1
END
