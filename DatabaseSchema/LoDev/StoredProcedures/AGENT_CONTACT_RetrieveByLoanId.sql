-- =============================================
-- Author:		Geoff Feltman
-- Create date: 12/22/17
-- Description:	Gets all agent contacts associated with a loan id.
-- =============================================
ALTER PROCEDURE [dbo].[AGENT_CONTACT_RetrieveByLoanId]
	@LoanId uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT
		AgentContactId,
		LoanId,
		FirstName,
		LastName,
		FullName,
		Title,
		CompanyName,
		BranchName,
		DepartmentName,
		StreetAddress,
		City,
		CountyFips,
		State,
		Zip,
		Phone,
		Fax,
		CellPhone,
		Email,
		IndividualNmlsId,
		ChumsId,
		WireBankName,
		WireBankCity,
		WireBankState,
		WireAbaNumber,
		WireAccountNumber,
		WireAccountName,
		WireFurtherCreditToAccountNumber,
		WireFurtherCreditToAccountName
	FROM AGENT_CONTACT
	WHERE LoanId = @LoanId
END

