ALTER proc [dbo].[DocMagic_EditAlternateLender]
	@DocMagicAlternateLenderId uniqueidentifier,
	@LenderName varchar(50) = '',
	@LenderAddress varchar(50) = '',
	@LenderCity varchar(36) = '',
	@LenderState varchar(2) = '',
	@LenderZip varchar(5) = '',
	@LenderCounty varchar(36) = '',
	@LenderNonPersonEntityIndicator bit = 0,
	@BeneficiaryName varchar(50) = '',
	@BeneficiaryAddress varchar(50) = '',
	@BeneficiaryCity varchar(36) = '',
	@BeneficiaryState varchar(2) = '',
	@BeneficiaryZip varchar(5) = '',
	@BeneficiaryCounty varchar(36) = '',
	@BeneficiaryNonPersonEntityIndicator bit = 0,
	@LossPayeeName varchar(50) = '',
	@LossPayeeAddress varchar(50) = '',
	@LossPayeeCity varchar(36) = '',
	@LossPayeeState varchar(2) = '',
	@LossPayeeZip varchar(5) = '',
	@LossPayeeCounty varchar(36) = '',
	@MakePaymentsToName varchar(50) = '',
	@MakePaymentsToAddress varchar(50) = '',
	@MakePaymentsToCity varchar(36) = '',
	@MakePaymentsToState varchar(2) = '',
	@MakePaymentsToZip varchar(5) = '',
	@MakePaymentsToCounty varchar(36) = '',
	@WhenRecodedMailToName varchar(50) = '',
	@WhenRecodedMailToAddress varchar(50) = '',
	@WhenRecodedMailToCity varchar(36) = '',
	@WhenRecodedMailToState varchar(2) = '',
	@WhenRecodedMailToZip varchar(5) = '',
	@WhenRecodedMailToCounty varchar(36) = '',
	@DocMagicAlternateLenderInternalId varchar(16) = ''
as
update DOCMAGIC_ALTERNATE_LENDER 
set	LenderName = @LenderName,
	LenderAddress = @LenderAddress,
	LenderCity = @LenderCity,
	LenderState = @LenderState,
	LenderZip = @LenderZip,
	LenderCounty = @LenderCounty,
	LenderNonPersonEntityIndicator = @LenderNonPersonEntityIndicator,
	BeneficiaryName = @BeneficiaryName,
	BeneficiaryAddress = @BeneficiaryAddress,
	BeneficiaryCity = @BeneficiaryCity,
	BeneficiaryState = @BeneficiaryState,
	BeneficiaryZip = @BeneficiaryZip,
	BeneficiaryCounty = @BeneficiaryCounty,
	BeneficiaryNonPersonEntityIndicator = @BeneficiaryNonPersonEntityIndicator,
	LossPayeeName = @LossPayeeName,
	LossPayeeAddress = @LossPayeeAddress,
	LossPayeeCity = @LossPayeeCity,
	LossPayeeState = @LossPayeeState,
	LossPayeeZip = @LossPayeeZip,
	LossPayeeCounty = @LossPayeeCounty,
	MakePaymentsToName = @MakePaymentsToName,
	MakePaymentsToAddress = @MakePaymentsToAddress,
	MakePaymentsToCity = @MakePaymentsToCity,
	MakePaymentsToState = @MakePaymentsToState,
	MakePaymentsToZip = @MakePaymentsToZip,
	MakePaymentsToCounty = @MakePaymentsToCounty,
	WhenRecodedMailToName = @WhenRecodedMailToName,
	WhenRecodedMailToAddress = @WhenRecodedMailToAddress,
	WhenRecodedMailToCity = @WhenRecodedMailToCity,
	WhenRecodedMailToState = @WhenRecodedMailToState,
	WhenRecodedMailToZip = @WhenRecodedMailToZip,
	WhenRecodedMailToCounty = @WhenRecodedMailToCounty,
	DocMagicAlternateLenderInternalId = @DocMagicAlternateLenderInternalId
where DocMagicAlternateLenderId = @DocMagicAlternateLenderId