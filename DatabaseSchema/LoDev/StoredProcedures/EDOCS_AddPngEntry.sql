CREATE PROCEDURE [dbo].[EDOCS_AddPngEntry]
	@DocumentId uniqueidentifier,
	@CommaSeparatedPngKeys varchar(MAX)
as
	BEGIN TRANSACTION
		DECLARE @PngKey varchar(100), @Pos int
		
		SET @CommaSeparatedPngKeys = LTRIM(RTRIM(@CommaSeparatedPngKeys))+ ','
		SET @Pos = CHARINDEX(',', @CommaSeparatedPngKeys, 1)
		IF REPLACE(@CommaSeparatedPngKeys, ',', '') <> ''
		BEGIN
			WHILE @Pos > 0
			BEGIN
				SET @PngKey = LTRIM(RTRIM(LEFT(@CommaSeparatedPngKeys, @Pos - 1)))
				IF @PngKey <> ''
					insert into EDOCS_PNG_ENTRY(DocumentId, PngKey)
					values (@DocumentId, @PngKey)

				SET @CommaSeparatedPngKeys = RIGHT(@CommaSeparatedPngKeys, LEN(@CommaSeparatedPngKeys) - @Pos)
				SET @Pos = CHARINDEX(',', @CommaSeparatedPngKeys, 1)
			END
		END	
	COMMIT TRANSACTION

