-- =============================================
-- Author:		Geoffrey Feltman
-- Create date: 4/9/13
-- Description:	Associate a Broker doc type id with a DocuTech doc type id.
-- =============================================
CREATE PROCEDURE dbo.EDOCS_AssociateDocuTechDocType 
	-- Add the parameters for the stored procedure here
	@BrokerId uniqueidentifier, 
	@DocTypeId int,
	@DocuTechDocTypeId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO EDOCS_DOCUTECH_DOCTYPE_X_DOCTYPE(DocuTechDocTypeId, DocTypeId, BrokerId) 
	VALUES( @DocuTechDocTypeId, @DocTypeId, @BrokerId )
END
