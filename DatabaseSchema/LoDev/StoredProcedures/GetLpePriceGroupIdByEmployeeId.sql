ALTER PROCEDURE [dbo].[GetLpePriceGroupIdByEmployeeId] 
	@EmployeeId uniqueidentifier
AS
SELECT bu.LpePriceGroupId, bu.MiniCorrespondentPriceGroupID, bu.CorrespondentPriceGroupID, br.BranchLpePriceGroupIdDefault, b.BrokerLpePriceGroupIdDefault
FROM VIEW_lendersoffice_EMPLOYEE u WITH (NOLOCK) 
	LEFT JOIN Broker_User bu WITH (NOLOCK) ON bu.employeeid = u.employeeid
	JOIN Branch br WITH (NOLOCK) ON u.BranchId = br.BranchId
     JOIN Broker b WITH (NOLOCK) ON u.BrokerId = b.BrokerId
WHERE u.EmployeeId = @EmployeeId


