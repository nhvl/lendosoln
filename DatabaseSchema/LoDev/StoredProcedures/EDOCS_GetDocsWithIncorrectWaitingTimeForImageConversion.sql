
CREATE procedure [dbo].[EDOCS_GetDocsWithIncorrectWaitingTimeForImageConversion]
	@SecondPerPage int,
	@SecondWait int
as
-- CREATED BY THINH NGUYEN-KHOA
-- to detect which edocs had longer edoc image conversion processing time than the system displays
-- to users as wait time


select brokerNm, docs.DocumentId, history.description,sLid, CreatedDate, TimeImageCreationEnqueued, TimeImageCreationStarted,  TimeImageCreationCompleted, IsUploadedByPmlUser, NumPages, @secondPerPage * NumPages + @secondWait as DisplayedEstimatedWaitTimeInSeconds, datediff( second, TimeImageCreationEnqueued, TimeImageCreationCompleted ) as ActualProcessingTimeFromEnqueuingInSeconds
from 
	edocs_document docs with(nolock) 
join 
	edocs_audit_history history with(nolock) on docs.DocumentId = history.documentId and docs.version = history.version 
join
	broker br with(nolock) on docs.brokerid = br.brokerid
	
	
where 
TimeImageCreationCompleted is not null 
and TimeImageCreationCompleted > getdate()-3
and datediff( second, TimeImageCreationEnqueued, TimeImageCreationCompleted ) > ( @secondPerPage * NumPages + @secondWait )
order by brokernm,ActualProcessingTimeFromEnqueuingInSeconds desc
