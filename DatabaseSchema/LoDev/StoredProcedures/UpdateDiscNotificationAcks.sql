CREATE PROCEDURE UpdateDiscNotificationAcks
	@UserId uniqueidentifier,
	@NotifId uniqueidentifier = NULL,
	@AckDate datetime = NULL
AS
	UPDATE Discussion_Notification
	SET
		NotifAckD = @AckDate
	WHERE
		NotifReceiverUserId = @UserId
		AND
		NotifId = COALESCE( @NotifId , NotifId )
		AND
		NotifIsValid = 1
	IF( 0!=@@error )
	BEGIN
		RAISERROR('Error in updating Discussion_Notification in UpdateDiscNotificationAcks sp', 16, 1);
		return -100;
	END
	RETURN 0;
