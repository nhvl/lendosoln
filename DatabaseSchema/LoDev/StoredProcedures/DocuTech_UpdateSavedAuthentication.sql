-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[DocuTech_UpdateSavedAuthentication]
	@UserId UniqueIdentifier,
	@DocuTechUserName varchar(50),
	@DocuTechPassword varchar(50) = null,
	@EncryptedDocuTechPassword varbinary(50)

AS
BEGIN
	UPDATE BROKER_USER
	SET
		DocuTechUserName = @DocuTechUserName,
		DocuTechPassword = COALESCE(@DocuTechPassword, DocuTechPassword),
		EncryptedDocuTechPassword = @EncryptedDocuTechPassword
	WHERE UserId = @UserId
END
