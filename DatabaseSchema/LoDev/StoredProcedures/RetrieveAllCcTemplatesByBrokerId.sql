CREATE PROCEDURE RetrieveAllCcTemplatesByBrokerId 
	@BrokerId uniqueidentifier
AS
select * 
from CC_TEMPLATE
where BrokerId = @BrokerId
