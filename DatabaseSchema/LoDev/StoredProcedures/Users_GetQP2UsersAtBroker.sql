-- =============================================
-- Author:		Scott Kibler
-- Create date: 7/23/2014
-- Description:	
--   Finds *active* users with IsUsePml2AsQuickpricer enabled 
--   Also filters by QP enabled and new PML enabled if the 
--   user needs them to enable the feature.
-- =============================================
CREATE PROCEDURE dbo.Users_GetQP2UsersAtBroker
	@BrokerID UniqueIdentifier,
	@BrokerHasQPEnabled bit,
	@BrokerHasNewPMLEnabled bit,
	@BrokerHasQP2EnabledForAll bit,
	@AllowQuickPricer2InTPOPortal bit -- from ConstStage setting.
AS
BEGIN
	SELECT 
		bu.UserID,
		au.Type,
		au.LoginNm
	FROM 
			 broker_user bu
		JOIN all_user au
		ON AU.UserID = bu.UserID
		JOIN Employee e
		ON bu.UserID = e.EmployeeUserID
		JOIN Branch br
		ON e.branchID = br.branchID
	WHERE
		     br.brokerid = @BrokerID
		AND (@BrokerHasQP2EnabledForAll = 1 OR bu.IsUsePml2AsQuickpricer = 1)
		AND (@BrokerHasQPEnabled = 1 OR bu.IsQuickPricerEnabled = 1)
		AND (@BrokerHasNewPMLEnabled = 1 OR bu.IsNewPmlUIEnabled = 1)
		AND  au.IsActive = 1
		AND (
			   (@AllowQuickPricer2InTPOPortal = 1 AND (au.Type = 'P' OR au.Type = 'B'))
			OR (@AllowQuickPricer2InTPOPortal = 0 AND au.Type = 'B'))
END
