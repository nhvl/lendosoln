ALTER PROCEDURE [dbo].[TASK_GetDefaultPermissionLevelByBrokerId] 
	@BrokerId	uniqueidentifier
	
AS

-- REVIEW
-- 6/9/2011: Reviewed. -ThinhNK	

	SELECT TOP 1
	TaskPermissionLevelId 
	FROM Task_Permission_Level
	WHERE 
	@BrokerId = BrokerId
	AND 
	(Name = 'General' OR Name='Task: LOS')
	
