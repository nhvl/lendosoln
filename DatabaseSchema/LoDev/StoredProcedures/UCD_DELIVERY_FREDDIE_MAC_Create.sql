-- =============================================
-- Author:		Eric Mallare
-- Create date: 9/6/2017
-- Description:	Adds a new UCD Freddie Mac delivery item
-- =============================================
ALTER PROCEDURE [dbo].[UCD_DELIVERY_FREDDIE_MAC_Create]
	@UcdDeliveryId int,
	@BatchId varchar(50),
	@UcdRequirementStatus int,
	@FindingsDocumentId uniqueidentifier,
	@FindingsXmlFileDbKey uniqueidentifier
AS
BEGIN
	INSERT INTO UCD_DELIVERY_FREDDIE_MAC (
		UcdDeliveryId, 
		BatchId,
		UcdRequirementStatus,
		FindingsDocumentId,
		FindingsXmlFileDbKey)
	VALUES (
		@UcdDeliveryId,
		@BatchId,
		@UcdRequirementStatus,
		@FindingsDocumentId,
		@FindingsXmlFileDbKey)
END
