CREATE PROCEDURE RetrieveLoFormById
	@FormId UniqueIdentifier,
	@BrokerId UniqueIdentifier
AS
BEGIN
	SELECT Description, IsSignable, IsStandardForm, FileName
	FROM LO_FORM
	Where FormId = @FormId AND BrokerId = @BrokerId
END
