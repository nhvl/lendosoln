-- =============================================
-- Author:		Antonio Valencia
-- Create date: 7/26/2012
-- Description:	Creates a draft from the manual templates. The manual templates have a null system id. 
-- =============================================
CREATE PROCEDURE [dbo].[CC_TEMPLATE_CreateDraftFromManualTemplates]
	@UserId uniqueidentifier ,
	@BrokerId uniqueidentifier,
	@Description varchar(200)
AS
BEGIN
	
	DECLARE @NewId int
	DECLARE @Error varchar(200)
	SET @NewId = -1 
	
	BEGIN TRANSACTION
	
	-- Delete existing draft 
	EXEC CC_TEMPLATE_DeleteDraft @BrokerId
	
	IF @@ERROR <> 0	BEGIN
		SET @Error = 'Could not delete existing draft'
		GOTO ERROR
	END
	
	INSERT INTO CC_TEMPLATE_SYSTEM( BrokerId, Description, DraftOpenedD, DraftOpenedByUserId, ReleasedD, ReleasedByUserId,RuleXml )
	VALUES( @BrokerId, @Description, GETDATE(), @UserId, NULL, NULL, '')
	
	IF @@ERROR <> 0	BEGIN
		SET @Error = 'Could not insert new CC Template System Row'
		GOTO ERROR
	END
	
	SELECT @NewId = @@IDENTITY 
	
	IF @@ERROR <> 0	BEGIN
		SET @Error = 'Could not select identity'
		GOTO ERROR
	END
	
	INSERT INTO CC_TEMPLATE(cCcTemplateId, CCTemplateSystemId, BrokerId, cCcTemplateNm, cProHazInsR, cProRealETx, cProMInsR, cLOrigFPc, cLOrigFMb, cLOrigFProps, cLDiscntPc, cLDiscntFMb, cLDiscntProps, cApprF, cApprFProps, cCrF, cInspectF, cMBrokFPc, cMBrokFMb, cTxServF, cProcF, cUwF, cWireF, c800U1fCode, c800U1FDesc, c800U1F, c800U2FCode, c800U2FDesc, c800U2F, c800U3FCode, c800U3FDesc, c800U3F, c800U4FCode, c800U4FDesc, c800U4F, c800U5FCode, c800U5FDesc, c800U5F, cIPiaDy, cMipPiaMon, cHazInsPiaMon, c904PiaDesc, c904Pia, c900U1PiaCode, c900U1PiaDesc, c900U1Pia, cHazInsRsrvMon, cMinsRsrvMon, cProSchoolTx, cRealETxRsrvMon, cFloodInsRsrvMon, cProFloodIns, c1006ProHExpDesc, c1006RsrvMon, c1006ProHExp, c1007ProHExpDesc, c1007RsrvMon, c1007ProHExp, cEscrowFTable, cEscrowFPc, cEscrowFBaseT,cEscrowFMb, cNotaryF, cTitleInsFTable, cTitleInsFPc, cTitleInsFBaseT, cTitleInsFMb, cU1TcCode, cU1TcDesc, cU1Tc, cU2TcCode, cU2TcDesc, cU2Tc, cU3TcCode, cU3TcDesc, cU3Tc, cU4TcCode, cU4TcDesc, cU4Tc, cRecFPc, cRecBaseT, cRecFMb, cCountyRTcPc, cCountyRtcBaseT, cCountyRtcMb, cStateRtcPc, cStateRtcBaseT, cStateRtcMb, cU1GovRtcCode, cU1GovRtcDesc, cU1GovRtcPc, cU1GovRtcBaseT, cU1GovRtcMb, cU2GovRtcCode, cU2GovRtcDesc, cU2GovRtcPc, cU2GovRtcBaseT, cU2GovRtcMb, cU3GovRtcCode, cU3GovRtcDesc, cU3GovRtcPc, cU3GovRtcBaseT, cU3GovRtcMb, cPestInspectF, cU1ScCode, cU1ScDesc, cU1Sc, cU2ScCode, cU2ScDesc, cU2Sc, cU3ScCode, cU3ScDesc, cU3Sc, cU4ScCode, cU4ScDesc, cU4Sc, cU5ScCode, cU5ScDesc, cU5Sc, cBrokComp1Desc, cBrokComp1, cBrokComp2Desc, cBrokComp2, cTotCcPbs, cTotCcPbsLocked, cU1FntcDesc, cU1Fntc, cGfeProvByBrok, cProcFPaid, cCrFPaid, cApprFPaid, cAttorneyF, cDocPrepF, cAggregateAdjRsrv, cDisabilityIns, cCrFProps, cInspectFProps, cMBrokFProps, cTxServFProps, cProcFProps, cUwFProps, cWireFProps, c800U1FProps, c800U2FProps, c800U3FProps, c800U4FProps, c800U5FProps, cIPiaProps, cMipPiaProps, cHazInsPiaProps, c904PiaProps, cVaFfProps, c900U1PiaProps, cHazInsRsrvProps, cMInsRsrvProps, cSchoolTxRsrvProps, cRealETxRsrvProps, cFloodInsRsrvProps, c1006RsrvProps, c1007RsrvProps, cAggregateAdjRsrvProps, cEscrowFProps, cDocPrepFProps, cNotaryFProps, cAttorneyFProps, cTitleInsFProps, cU1TcProps, cU2TcProps, cU3TcProps, cU4TcProps, cRecFProps, cCountyRtcProps, cStateRtcProps, cU1GovRtcProps, cU2GovRtcProps, cU3GovRtcProps, cPestInspectFProps, cU1ScProps, cU2ScProps, cU3ScProps, cU4ScProps, cU5ScProps, cProHazInsT, cProMInsT, cSchoolTxRsrvMon, cDaysInYr, cRecFDesc, cCountyRtcDesc, cStateRtcDesc, cApprFPaidTo, cCrFPaidTo, cTxServFPaidTo, cFloodCertificationF, cFloodCertificationFProps, cFloodCertificationFPaidTo, cInspectFPaidTo, cProcFPaidTo, cUwFPaidTo, cWireFPaidTo, c800U1FPaidTo, c800U2FPaidTo, c800U3FPaidTo, c800U4FPaidTo, c800U5FPaidTo, cProHazInsMb, cProMInsMb, cProRealETxR, cProRealETxT, cProRealETxMb, cOwnerTitleInsFPc,cOwnerTitleInsFBaseT,cOwnerTitleInsFMb, cOwnerTitleInsProps, cOwnerTitleInsPaidTo, cDocPrepFPaidTo, cNotaryFPaidTo, cU1TcPaidTo, cU2TcPaidTo, cU3TcPaidTo, cU4TcPaidTo, cU1GovRtcPaidTo, cU2GovRtcPaidTo, cU3GovRtcPaidTo, cPestInspectPaidTo, cU1ScPaidTo, cU2ScPaidTo, cU3ScPaidTo, cU4ScPaidTo, cU5ScPaidTo, GfeVersion, cAttorneyFPaidTo, cGfeUsePaidToFromOfficialContact, cHazInsPiaPaidTo, c800U1FGfeSection, c800U2FGfeSection, c800U3FGfeSection, c800U4FGfeSection, c800U5FGfeSection, cEscrowFGfeSection, cTitleInsFGfeSection, cDocPrepFGfeSection, cNotaryFGfeSection, cAttorneyFGfeSection, cU1TcGfeSection, cU2TcGfeSection, cU3TcGfeSection, cU4TcGfeSection, cU1ScGfeSection, cU2ScGfeSection, cU3ScGfeSection, cU4ScGfeSection, cU5ScGfeSection, cGfeIsTPOTransaction, c904PiaGfeSection, c900U1PiaGfeSection, cLDiscntBaseT, cMBrokFBaseT, cTitleInsurancePolicy, cPricingEngineCostT, cPricingEngineCreditT, cPricingEngineLimitCreditToT)
								       SELECT NEWID(),@NewId,  BrokerId, cCcTemplateNm, cProHazInsR, cProRealETx, cProMInsR, cLOrigFPc, cLOrigFMb, cLOrigFProps, cLDiscntPc, cLDiscntFMb, cLDiscntProps, cApprF, cApprFProps, cCrF, cInspectF, cMBrokFPc, cMBrokFMb, cTxServF, cProcF, cUwF, cWireF, c800U1fCode, c800U1FDesc, c800U1F, c800U2FCode, c800U2FDesc, c800U2F, c800U3FCode, c800U3FDesc, c800U3F, c800U4FCode, c800U4FDesc, c800U4F, c800U5FCode, c800U5FDesc, c800U5F, cIPiaDy, cMipPiaMon, cHazInsPiaMon, c904PiaDesc, c904Pia, c900U1PiaCode, c900U1PiaDesc, c900U1Pia, cHazInsRsrvMon, cMinsRsrvMon, cProSchoolTx, cRealETxRsrvMon, cFloodInsRsrvMon, cProFloodIns, c1006ProHExpDesc, c1006RsrvMon, c1006ProHExp, c1007ProHExpDesc, c1007RsrvMon, c1007ProHExp, cEscrowFTable, cEscrowFPc,
cEscrowFBaseT,cEscrowFMb, cNotaryF, cTitleInsFTable, cTitleInsFPc, cTitleInsFBaseT, cTitleInsFMb, cU1TcCode, cU1TcDesc, cU1Tc, cU2TcCode, cU2TcDesc, cU2Tc, cU3TcCode, cU3TcDesc, cU3Tc, cU4TcCode, cU4TcDesc, cU4Tc, cRecFPc, cRecBaseT, cRecFMb, cCountyRTcPc, cCountyRtcBaseT, cCountyRtcMb, cStateRtcPc, cStateRtcBaseT, cStateRtcMb, cU1GovRtcCode, cU1GovRtcDesc, cU1GovRtcPc, cU1GovRtcBaseT, cU1GovRtcMb, cU2GovRtcCode, cU2GovRtcDesc, cU2GovRtcPc, cU2GovRtcBaseT, cU2GovRtcMb, cU3GovRtcCode, cU3GovRtcDesc, cU3GovRtcPc, cU3GovRtcBaseT, cU3GovRtcMb, cPestInspectF, cU1ScCode, cU1ScDesc, cU1Sc, cU2ScCode, cU2ScDesc, cU2Sc, cU3ScCode, cU3ScDesc, cU3Sc, cU4ScCode, cU4ScDesc, cU4Sc, cU5ScCode, cU5ScDesc, cU5Sc, cBrokComp1Desc, cBrokComp1, cBrokComp2Desc, cBrokComp2, cTotCcPbs, cTotCcPbsLocked, cU1FntcDesc, cU1Fntc, cGfeProvByBrok, cProcFPaid, cCrFPaid, cApprFPaid, cAttorneyF, cDocPrepF, cAggregateAdjRsrv, cDisabilityIns, cCrFProps, cInspectFProps, cMBrokFProps, cTxServFProps, cProcFProps, cUwFProps, cWireFProps, c800U1FProps, c800U2FProps, c800U3FProps, c800U4FProps, c800U5FProps, cIPiaProps, cMipPiaProps, cHazInsPiaProps, c904PiaProps, cVaFfProps, c900U1PiaProps, cHazInsRsrvProps, cMInsRsrvProps, cSchoolTxRsrvProps, cRealETxRsrvProps, cFloodInsRsrvProps, c1006RsrvProps, c1007RsrvProps, cAggregateAdjRsrvProps, cEscrowFProps, cDocPrepFProps, cNotaryFProps, cAttorneyFProps, cTitleInsFProps, cU1TcProps, cU2TcProps, cU3TcProps, cU4TcProps, cRecFProps, cCountyRtcProps, cStateRtcProps, cU1GovRtcProps, cU2GovRtcProps, cU3GovRtcProps, cPestInspectFProps, cU1ScProps, cU2ScProps, cU3ScProps, cU4ScProps, cU5ScProps, cProHazInsT, cProMInsT, cSchoolTxRsrvMon, cDaysInYr, cRecFDesc, cCountyRtcDesc, cStateRtcDesc, cApprFPaidTo, cCrFPaidTo, cTxServFPaidTo, cFloodCertificationF, cFloodCertificationFProps, cFloodCertificationFPaidTo, cInspectFPaidTo, cProcFPaidTo, cUwFPaidTo, cWireFPaidTo, c800U1FPaidTo, c800U2FPaidTo, c800U3FPaidTo, c800U4FPaidTo, c800U5FPaidTo, cProHazInsMb, cProMInsMb, cProRealETxR, cProRealETxT, cProRealETxMb, cOwnerTitleInsFPc,cOwnerTitleInsFBaseT,cOwnerTitleInsFMb, cOwnerTitleInsProps, cOwnerTitleInsPaidTo, cDocPrepFPaidTo, cNotaryFPaidTo, cU1TcPaidTo, cU2TcPaidTo, cU3TcPaidTo, cU4TcPaidTo, cU1GovRtcPaidTo, cU2GovRtcPaidTo, cU3GovRtcPaidTo, cPestInspectPaidTo, cU1ScPaidTo, cU2ScPaidTo, cU3ScPaidTo, cU4ScPaidTo, cU5ScPaidTo, GfeVersion, cAttorneyFPaidTo, cGfeUsePaidToFromOfficialContact, cHazInsPiaPaidTo, c800U1FGfeSection, c800U2FGfeSection, c800U3FGfeSection, c800U4FGfeSection, c800U5FGfeSection, cEscrowFGfeSection, cTitleInsFGfeSection, cDocPrepFGfeSection, cNotaryFGfeSection, cAttorneyFGfeSection, cU1TcGfeSection, cU2TcGfeSection, cU3TcGfeSection, cU4TcGfeSection, cU1ScGfeSection, cU2ScGfeSection, cU3ScGfeSection, cU4ScGfeSection, cU5ScGfeSection, cGfeIsTPOTransaction, c904PiaGfeSection, c900U1PiaGfeSection, cLDiscntBaseT, cMBrokFBaseT, cTitleInsurancePolicy, cPricingEngineCostT, cPricingEngineCreditT, cPricingEngineLimitCreditToT
									   FROM CC_TEMPLATE WHERE CCTemplateSystemId IS NULL and BrokerId = @BrokerId AND GfeVersion > 0
	
	IF @@ERROR <> 0	BEGIN
		SET @Error = 'Could not copy manual templates'
		GOTO ERROR
	END
	
	COMMIT TRANSACTION
	SELECT @NewId
	RETURN

	ERROR:
		ROLLBACK TRANSACTION
		RAISERROR (@Error, 16, 1)
END