/*
	Author: 
		Scott Kibler
	Description: 
		Removes the only vendor integration request format.
	opm:
		463659
	Remarks:
		This sproc will need to be deleted at a later phase of this case.
*/
CREATE PROCEDURE [dbo].[TEMP_RemoveOnlyVendorIntegrationRequestFormat]

AS
BEGIN
	DECLARE @NumFormats INT
	SELECT @NumFormats = COUNT(*) FROM VENDOR_INTEGRATION_REQUEST_FORMAT
	IF @NumFormats > 1
	BEGIN
		DECLARE @ErrorMsg VARCHAR(255)
		
		SET @ErrorMsg = 'There is more than 1 record in VENDOR_INTEGRATION_REQUEST_FORMAT.  DELETE sproc or records manually.'
		PRINT @ErrorMsg
		RAISERROR(@ErrorMsg, 16, 1)
		RETURN -1;
	END	
	
	DELETE FROM VENDOR_INTEGRATION_REQUEST_FORMAT
END