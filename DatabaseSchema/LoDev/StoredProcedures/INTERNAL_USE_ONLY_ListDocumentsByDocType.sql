CREATE PROCEDURE [dbo].[INTERNAL_USE_ONLY_ListDocumentsByDocType]
	@DocTypeId int
	as

	select DocumentId, BrokerId
	from 
	EDOCS_DOCUMENT with (nolock)
	where
	DocTypeId = @DocTypeId
order by BrokerId
