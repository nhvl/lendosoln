-- =============================================
-- Author:		Paolo Arrastia
-- Create date: 4/11/2013
-- Description:	
-- =============================================
CREATE PROCEDURE dbo.APPRAISAL_VENDOR_DisableForBroker
	-- Add the parameters for the stored procedure here
	@VendorId uniqueidentifier,
	@BrokerId uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM APPRAISAL_VENDOR_BROKER_ENABLED
	WHERE VendorId=@VendorId AND BrokerId=@BrokerId
END
