-- =============================================
-- Author:		Scott Kibler
-- Create date: 2/4/2014
-- Description:	opm 150384
-- =============================================
CREATE PROCEDURE [dbo].TPOPortalConfig_RetrieveByID
	@ID uniqueidentifier,
	@BrokerID uniqueidentifier
AS
BEGIN
	SELECT *
	FROM LENDER_TPO_LANDINGPAGECONFIG WITH(NOLOCK)
	WHERE ID = @ID
	and BrokerID = @BrokerID
END

