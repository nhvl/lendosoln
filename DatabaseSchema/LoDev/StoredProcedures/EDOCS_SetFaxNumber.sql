ALTER procedure [dbo].[EDOCS_SetFaxNumber]
	@BrokerId uniqueidentifier,
	@FaxNumber varchar(14),
	@DocRouterLogin varchar(50),
	@EncryptedDocRouterPassword varbinary(max) = null,
	@EncryptionKeyId uniqueidentifier = null
as

BEGIN TRANSACTION

  --Clearing out means delete entry
  IF (@FaxNumber = '' AND @DocRouterLogin = '' AND @EncryptedDocRouterPassword IS NULL)
  BEGIN
	DELETE FROM EDOCS_FAX_NUMBER WHERE BrokerId = @BrokerId
	IF @@error!= 0 GOTO HANDLE_ERROR
  END
  ELSE
  BEGIN
  

--Attempt to update this fax information
	UPDATE EDOCS_FAX_NUMBER
		SET BrokerId = @BrokerId,
			DocRouterLogin = @DocRouterLogin,
			EncryptedDocRouterPassword = @EncryptedDocRouterPassword,
			EncryptionKeyId = @EncryptionKeyId
		WHERE 
			FaxNumber = @FaxNumber
	IF @@error!= 0 GOTO HANDLE_ERROR

	IF @@rowcount = 0  
	BEGIN
		-- No rows were updated -> New row, or fax# is changed.
		DELETE FROM EDOCS_FAX_NUMBER WHERE BrokerId = @BrokerId
		IF @@error!= 0 GOTO HANDLE_ERROR
		
		INSERT INTO EDOCS_FAX_NUMBER(BrokerId, FaxNumber, DocRouterLogin, EncryptedDocRouterPassword, EncryptionKeyId) 
		VALUES (@BrokerId, @FaxNumber, @DocRouterLogin, @EncryptedDocRouterPassword,  @EncryptionKeyId)
		IF @@error!= 0 GOTO HANDLE_ERROR

	END
  END
	COMMIT TRANSACTION
	RETURN;

HANDLE_ERROR:
	ROLLBACK TRANSACTION
	RAISERROR('Error in EDOCS_SetFaxNumber sp', 16, 1);;
	RETURN;