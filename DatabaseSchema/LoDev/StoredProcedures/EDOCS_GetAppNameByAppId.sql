CREATE procedure [dbo].[EDOCS_GetAppNameByAppId]
	@AppId uniqueidentifier
as
select abFirstNm, abLastNm, acFirstNm, acLastNm
from APPLICATION_A
where aAppId = @AppId