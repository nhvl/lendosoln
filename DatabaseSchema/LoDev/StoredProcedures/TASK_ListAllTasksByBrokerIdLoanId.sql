-- =============================================
-- Author:		Matthew Pham
-- Create date: 4/19/11
-- Description:	Lists all tasks by LoanId
-- =============================================
ALTER PROCEDURE [dbo].[TASK_ListAllTasksByBrokerIdLoanId] 
	-- Add the parameters for the stored procedure here
	@BrokerId uniqueidentifier,
	@LoanId uniqueidentifier,
	@TaskStatus tinyint = NULL
AS

--7/5/2012: Added CondRowId. MP

--REVIEW
--6/9/2011: Reviewed. ThinhNK

BEGIN
	SELECT
		BrokerId
		, TaskId
		, TaskAssignedUserId
		--, TaskIdentityId
		, TaskStatus
		, LoanId
		, TaskOwnerUserId
		, TaskCreatedDate
		, TaskSubject
		, TaskDueDate
		, TaskFollowUpDate
		, CondInternalNotes
		, TaskIsCondition
		, TaskResolvedUserId
		, TaskLastModifiedDate
		, BorrowerNmCached
		, LoanNumCached
		, TaskToBeAssignedRoleId
		, TaskToBeOwnerRoleId
		, TaskCreatedByUserId
		, TaskDueDateLocked
		, TaskDueDateCalcField
		, TaskDueDateCalcDays
		, TaskHistoryXml
		, TaskPermissionLevelId
		, TaskClosedUserId
		, TaskClosedDate
		, TaskRowVersion
		, CondCategoryId
		, CondIsHidden
		, CondMessageId
		--, CondRank
		, CondRowId
		, CondDeletedDate
		, CondDeletedByUserNm
		, CondIsDeleted
		--, assigned.UserFirstNm + ' ' + assigned.UserLastNm as AssignedUserFullName
		--, owned.UserFirstNm + ' ' + owned.UserLastNm as OwnerFullName
		--, CASE WHEN TaskClosedUserId IS NULL THEN '' ELSE closed.UserFirstNm + ' ' + closed.UserLastNm END as ClosingUserFullName
		, TaskAssignedUserFullName
		, TaskOwnerFullName
		, TaskClosedUserFullName
		, AssignToOnReactivationUserId

	FROM TASK t 
		--left join Employee assigned on t.TaskAssignedUserId = assigned.EmployeeUserId
		--left join Employee owned on t.TaskOwnerUserId = owned.EmployeeUserId
		--left join Employee closed on t.TaskClosedUserId = closed.EmployeeUserId

	WHERE
		BrokerId = @BrokerId
		AND
		LoanId = @LoanId
		AND
		TaskStatus = COALESCE(@TaskStatus, TaskStatus)
END
