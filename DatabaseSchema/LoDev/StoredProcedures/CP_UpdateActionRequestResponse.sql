














CREATE PROCEDURE [dbo].[CP_UpdateActionRequestResponse]
	@ConsumerActionRequestId bigint
	-- Request
	, @PdfFileDbKey uniqueidentifier = null
	, @ResponseStatus tinyint = null
	
	-- Response
	, @ArchivedEDocId uniqueidentifier = null
	, @ConsumerCompletedD datetime = null
	, @RequestorAcceptedD datetime = null
	, @ReceivingFaxNumber varchar(14) = null
	, @BorrowerSignedEventId uniqueidentifier = null
	, @CoborrowerSignedEventId uniqueidentifier = null

	-- BorrowerSignature
	, @bSignedTransactionId uniqueidentifier = null
	, @bSignature varchar(50) = null
	, @bSignedD datetime = null
	, @bIpAddress varchar(15) = null
	, @bEmailUsedToSign varchar(80) = null
	, @bPdfFileDbKey uniqueidentifier = null

	-- CoborrowerSignature
	, @cSignedTransactionId uniqueidentifier = null
	, @cSignature varchar(50) = null
	, @cSignedD datetime = null
	, @cIpAddress varchar(15) = null
	, @cEmailUsedToSign varchar(80) = null
	, @cPdfFileDbKey uniqueidentifier = null

AS


BEGIN TRANSACTION

--Update the Request
EXEC CP_UpdateActionRequest 
	@ConsumerActionRequestId = @ConsumerActionRequestId
	, @ResponseStatus = @ResponseStatus
	, @PdfFileDbKey = @PdfFileDbKey

-- Evault entries are only created, never updated or deleted
IF ( @bSignedTransactionId IS NOT NULL )
BEGIN
	EXEC CP_CreateSignEvent
	@bSignedTransactionId
	, @bSignature
	, @bSignedD
	, @bIpAddress
	, @bEmailUsedToSign
	, @bPdfFileDbKey
END

IF ( @cSignedTransactionId IS NOT NULL )
BEGIN
	EXEC CP_CreateSignEvent
	@cSignedTransactionId
	, @cSignature
	, @cSignedD
	, @cIpAddress
	, @cEmailUsedToSign
	, @cPdfFileDbKey
END

--Update the Response
IF ( @ConsumerCompletedD IS NOT NULL
	OR @ReceivingFaxNumber IS NOT NULL
	OR @BorrowerSignedEventId IS NOT NULL
	OR @CoborrowerSignedEventId IS NOT NULL )

BEGIN
	IF ( @ConsumerActionRequestId IN ( SELECT ConsumerActionRequestId FROM CP_Consumer_Action_Response ) )
	BEGIN
	  --UPDATE THIS EXISTING ROW
		EXEC CP_UpdateActionResponse
			 @ConsumerActionRequestId
			--, @ArchivedEDocId
			, @ConsumerCompletedD
			--, @RequestorAcceptedD
			, @ReceivingFaxNumber
			, @BorrowerSignedEventId
			, @CoborrowerSignedEventId
	END
	ELSE

	BEGIN
	  --CREATE THE ROW
		EXEC CP_CreateActionResponse
			 @ConsumerActionRequestId
			--, @ArchivedEDocId
			, @ConsumerCompletedD
			--, @RequestorAcceptedD
			, @ReceivingFaxNumber
			, @BorrowerSignedEventId
			, @CoborrowerSignedEventId
	END
END



COMMIT TRANSACTION
RETURN








