ALTER PROCEDURE [dbo].[GetBrokerIdByLoanId] 
	@LoanId Guid
AS
BEGIN
	SELECT sBrokerId FROM Loan_File_Cache c JOIN Broker br ON c.sBrokerId=br.BrokerId
	WHERE sLId = @LoanId
	AND br.Status=1

END
