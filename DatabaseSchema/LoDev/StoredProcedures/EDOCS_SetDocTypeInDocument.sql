CREATE PROCEDURE [dbo].[EDOCS_SetDocTypeInDocument]
	@DocumentId uniqueidentifier,
	@NewDocTypeId int
as
	update EDOCS_DOCUMENT
	set DocTypeId = @NewDocTypeId
	where DocumentId = @DocumentId

		
