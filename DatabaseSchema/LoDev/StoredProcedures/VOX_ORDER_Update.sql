-- =============================================
-- Author:		Timothy Jewell
-- Create date:	05/23/2017
-- Description:	Updates a VOX order.
-- =============================================
ALTER PROCEDURE [dbo].[VOX_ORDER_Update]
	@BrokerId uniqueidentifier,
	@LoanId uniqueidentifier,
	@OrderId int,
	@HasBeenRefreshed bit,
	@LenderServiceId int,
	@LenderServiceName varchar(200),
	@Status int,
	@StatusDescription varchar(300),
	@DateCompleted datetime,
	@ErrorMessage varchar(300)
AS
BEGIN
	UPDATE VOX_ORDER
	SET
		HasBeenRefreshed = @HasBeenRefreshed,
		LenderServiceId = @LenderServiceId,
		LenderServiceName = @LenderServiceName,
		Status = @Status,
		StatusDescription = @StatusDescription,
		DateCompleted = @DateCompleted,
		ErrorMessage = @ErrorMessage
	WHERE OrderId = @OrderId
END
