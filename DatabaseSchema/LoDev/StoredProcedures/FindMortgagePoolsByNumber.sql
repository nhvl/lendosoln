-- =============================================
-- Author:		Paolo Arrastia
-- Create date: 2 Aug 2012
-- Description:	
-- =============================================
ALTER PROCEDURE [dbo].[FindMortgagePoolsByNumber] 
	-- Add the parameters for the stored procedure here
	@poolNumber varchar(9),
	@allowPartials bit = 0,
	@BrokerId UniqueIdentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	IF(@allowPartials=1)
		SET @poolNumber = @poolNumber+'%';
    -- Insert statements for procedure here
	SELECT PoolId
	FROM MORTGAGE_POOL
	WHERE 
	(BasePoolNumber LIKE @poolNumber
		OR
	 PoolPrefix+'-'+BasePoolNumber LIKE @poolNumber
		OR
	 BasePoolNumber+IssueTypeCode+PoolTypeCode LIKE @poolNumber
	 )
	AND
	BrokerId = @BrokerId
	AND
	IsEnabled = 1
END
