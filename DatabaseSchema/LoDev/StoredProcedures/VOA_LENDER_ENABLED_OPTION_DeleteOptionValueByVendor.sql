ALTER PROCEDURE [dbo].[VOA_LENDER_ENABLED_OPTION_DeleteOptionValueByVendor]
	@VendorId int,
	@EnabledOptionType int,
	@EnabledOptionValue int
AS
BEGIN
	DELETE FROM VOA_LENDER_ENABLED_OPTION
	WHERE VendorId = @VendorId
		AND EnabledOptionType = @EnabledOptionType
		AND EnabledOptionValue = @EnabledOptionValue
END
GO
