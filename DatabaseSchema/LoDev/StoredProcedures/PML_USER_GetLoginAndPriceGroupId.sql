CREATE PROCEDURE [dbo].[PML_USER_GetLoginAndPriceGroupId]
	@BrokerId UniqueIdentifier
AS
BEGIN
	SELECT LoginNm, UserId,EmployeeId, u.LpePriceGroupId, g.LpePRiceGroupName  
	FROM VIEW_ACTIVE_PML_USER u LEFT JOIN 
	LPE_PRICE_GROUP g on g.LpePriceGroupId = u.LpePriceGroupId 
	WHERE u.BRokerId = @BrokerId
	
	UNION 
	
	SELECT LoginNm, UserId,EmployeeId, u.LpePriceGroupId, g.LpePRiceGroupName  
	FROM VIEW_DISABLED_PML_USER u LEFT JOIN 
	LPE_PRICE_GROUP g on g.LpePriceGroupId = u.LpePriceGroupId 
	WHERE u.BRokerId = @BrokerId
	
END

