
CREATE  PROCEDURE [dbo].[ListBrokerEmployeeLogin]
	@BrokerId uniqueidentifier , @BranchId uniqueidentifier = NULL , @Type Varchar( 1 ) = NULL
AS
	SELECT
		v.BranchId ,
		v.EmployeeId ,
		v.LoginNm ,
		v.UserFirstNm ,
		v.UserLastNm ,
		v.Type ,
		v.UserId
	FROM
		View_Active_LO_User AS v
	WHERE
		v.BranchId = COALESCE( @BranchId , v.BranchId )
		AND
		v.Type = COALESCE( @Type , v.Type )
		AND
		v.BrokerId = @BrokerId
	UNION
	SELECT
		v.BranchId ,
		v.EmployeeId ,
		v.LoginNm ,
		v.UserFirstNm ,
		v.UserLastNm ,
		v.Type ,
		v.UserId
	FROM
		View_Disabled_LO_User AS v
	WHERE
		v.BranchId = COALESCE( @BranchId , v.BranchId )
		AND
		v.Type = COALESCE( @Type , v.Type )
		AND
		v.BrokerId = @BrokerId

UNION
	SELECT
		v.BranchId ,
		v.EmployeeId ,
		v.LoginNm ,
		v.UserFirstNm ,
		v.UserLastNm ,
		v.Type ,
		v.UserId
	FROM
		View_Active_Pml_User AS v
	WHERE
		v.BranchId = COALESCE( @BranchId , v.BranchId )
		AND
		v.Type = COALESCE( @Type , v.Type )
		AND
		v.BrokerId = @BrokerId

UNION
	SELECT
		v.BranchId ,
		v.EmployeeId ,
		v.LoginNm ,
		v.UserFirstNm ,
		v.UserLastNm ,
		v.Type ,
		v.UserId
	FROM
		View_Disabled_Pml_User AS v
	WHERE
		v.BranchId = COALESCE( @BranchId , v.BranchId )
		AND
		v.Type = COALESCE( @Type , v.Type )
		AND
		v.BrokerId = @BrokerId

