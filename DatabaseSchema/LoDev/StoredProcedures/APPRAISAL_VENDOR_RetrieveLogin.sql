-- =============================================
-- Author:		Paolo Arrastia
-- Create date: 4/11/2013
-- Description: 
-- =============================================
ALTER PROCEDURE [dbo].[APPRAISAL_VENDOR_RetrieveLogin]
	-- Add the parameters for the stored procedure here
	@EmployeeId uniqueidentifier,
	@VendorId uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT AccountId, Username, EncryptionKeyId, EncryptedPassword
	FROM APPRAISAL_VENDOR_EMPLOYEE_LOGIN
	WHERE EmployeeId=@EmployeeId AND VendorId=@VendorId
END