-- =============================================
-- Author:		Timothy Jewell
-- Create date: 4/24/2018
-- Description:	Creates a record of a document sent to DM for appraisal delivery.
-- =============================================
ALTER PROCEDURE [dbo].[DOCMAGIC_APPRAISAL_DELIVERY_DOCUMENT_Create]
	@AppraisalDeliveryId uniqueidentifier,
	@DocumentId uniqueidentifier
AS
BEGIN
	INSERT INTO DOCMAGIC_APPRAISAL_DELIVERY_DOCUMENT
	(
		AppraisalDeliveryId,
		DocumentId
	)
	VALUES
	(
		@AppraisalDeliveryId,
		@DocumentId
	)
END