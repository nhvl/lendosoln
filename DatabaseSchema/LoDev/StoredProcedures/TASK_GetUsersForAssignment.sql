CREATE proc [dbo].[TASK_GetUsersForAssignment]
	@BrokerId as uniqueidentifier,
	@NameFilter Varchar(32) = NULL,
	@LastFilter Varchar(32) = NULL,
	@NoPML bit = NULL,
	@NoLO bit = NULL,
	@LoanId uniqueidentifier = NULL,
	@BranchId uniqueidentifier = NULL,
	@IsPipelineQuery bit = NULL,
	@PmlBrokerId uniqueidentifier = NULL
as

-- REVIEW
-- 6/9/2011: An indexed view VIEW_ACTIVE_LO_AND_PML_USER_BY_BROKERID was created to improve 
-- performance of this stored proc. -ThinhNK 
IF @PmlBrokerId IS NULL
BEGIN
	SET @PmlBrokerId = (select sPmlBrokerId from loan_file_cache where slid = @LoanId);
END

select
    EmployeeId,
    UserFirstNm + ' ' + UserLastNm AS FullName,
    Type,
    UserId,
	BranchId,
	PmlBrokerId,
	PmlLevelAccess,
	Permissions
from
    VIEW_ACTIVE_LO_AND_PML_USER_BY_BROKERID
where
	brokerid = @BrokerId

	AND ((@LastFilter IS NULL AND (UserFirstNm LIKE COALESCE(@NameFilter, '') + '%'
			OR UserLastNm LIKE COALESCE(@NameFilter, '') + '%'))
		OR (@LastFilter IS NOT NULL AND (UserFirstNm LIKE @NameFilter + '%' AND UserLastNm LIKE @LastFilter + '%')))

	AND (
		(
			Type = 'B'
			AND (
				@IsPipeLineQuery = 1 
				OR (
					(@BranchId = BranchId AND LendersOfficeLevelAccess = 1)
					OR LendersOfficeLevelAccess = 2
				)
			) AND (@NoLO = '0' OR @NoLO IS NULL)
		) OR (
			Type = 'P'
			AND (
				@IsPipeLineQuery = 1
				OR @PmlBrokerId = PmlBrokerId
			) AND (@NoPML = '0' OR @NoPML IS NULL)
		)
	)