
-- =============================================
-- Author:		<Francesc>
-- Create date: <04/29/10>
-- Description:	<Retrieve loans associated with a given consumerId>
-- =============================================
CREATE PROCEDURE [dbo].[CP_CreateConsumerId]
	@Email varchar(80),
	@BrokerId uniqueidentifier,
	@PasswordHash varchar(200),
	@LastLoggedInD datetime = null,
	@PasswordRequestD datetime = null,
	@PasswordResetCode varchar(200) = '',
	@IsTemporaryPassword bit = 0,
	@LoginFailureNumber int = 0,
	@IsLocked bit = 0
	
AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO CP_CONSUMER(Email, BrokerId, PasswordHash, LastLoggedInD, PasswordRequestD, PasswordResetCode, IsTemporaryPassword, LoginFailureNumber, IsLocked)
	VALUES(@Email, @BrokerId, @PasswordHash, @LastLoggedInD, @PasswordRequestD, @PasswordResetCode, @IsTemporaryPassword, @LoginFailureNumber, @IsLocked)

	SELECT SCOPE_IDENTITY() as consumerId

END

