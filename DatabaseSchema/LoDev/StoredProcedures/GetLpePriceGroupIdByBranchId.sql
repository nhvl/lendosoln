CREATE PROCEDURE [dbo].[GetLpePriceGroupIdByBranchId] 
	@BranchId Guid
AS
SELECT BranchLpePriceGroupIdDefault, BrokerLpePriceGroupIdDefault
FROM Branch br  WITH (NOLOCK)  JOIN Broker b WITH (NOLOCK) ON br.BrokerId = b.BrokerId
WHERE BranchId = @BranchId
