-- =============================================
-- Author:		Jhairo Erazo
-- Create date: 01/16/14
-- Description:	Retreive MBS Decription by name
-- =============================================

CREATE PROCEDURE [dbo].[MBS_RetrieveDescriptionByName]
	@DescriptionName varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	-- Returns the first description found.
	SELECT TOP 1 *
	FROM MBS_DESCRIPTION
	WHERE DescriptionName = @DescriptionName
END