-- =============================================
-- Author:		Matthew Pham
-- Create date: 9/1/11
-- Description:	Create a transaction for MBS trades
-- =============================================
CREATE PROCEDURE [dbo].[MBS_CreateTransaction] 
	-- Add the parameters for the stored procedure here
	@TransactionId uniqueidentifier,
	@TradeId uniqueidentifier,
	@Date smalldatetime,
	@Amount money,
	@Price decimal(12,6),
	@IsGainLossCalculated bit,
	@GainLossAmount money,
	@Expense money,
	@Memo varchar(500),
	@Type int,
	@IsAmountCalculated bit,
	@LinkedTransaction uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO MBS_TRANSACTION(
		TransactionId,
		TradeId,
		Date,
		Amount,
		Price,
		IsGainLossCalculated,
		GainLossAmount,
		Expense,
		Memo,
		Type,
		IsAmountCalculated,
		LinkedTransaction
	)
	VALUES (
		@TransactionId,
		@TradeId,
		@Date,
		@Amount,
		@Price,
		@IsGainLossCalculated,
		@GainLossAmount,
		@Expense,
		@Memo,
		@Type,
		@IsAmountCalculated,
		@LinkedTransaction
	)
END
