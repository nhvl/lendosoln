CREATE PROCEDURE [dbo].[AssignBranchToLoan]
	@LoanId Guid , @BranchId Guid
AS
	BEGIN TRANSACTION
	UPDATE Loan_File_F with( rowlock )
	SET
		sBranchId = @BranchId
	WHERE
		sLId = @LoanId
	IF @@ERROR != 0
	BEGIN
		RAISERROR('Error in AssignBranchToLoan', 16, 1);
		ROLLBACK TRANSACTION
		RETURN -100
	END
	COMMIT TRANSACTION
	RETURN 0
