-- =============================================
-- Author:		David Dao
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE dbo.INTERNAL_USER_ONLY_UpdateAccountOwner
    @OldAccountOwnerEmployeeId UniqueIdentifier,
    @NewAccountOwnerEmployeeId UniqueIdentifier
AS
BEGIN

    UPDATE Broker_User SET IsAccountOwner=0 WHERE EmployeeID=@OldAccountOwnerEmployeeId;
    UPDATE Broker_User SET IsAccountOwner=1 WHERE EmployeeID=@NewAccountOwnerEmployeeId

END
