ALTER  PROCEDURE [dbo].[UpdatePricingGroupById] 
	@LpePriceGroupId uniqueidentifier,
	@LpePriceGroupName varchar(100) = NULL,
	@ExternalPriceGroupEnabled bit = NULL,
	@BrokerId uniqueidentifier,
	@LenderPaidOriginatorCompensationOptionT int = NULL,
	@LockPolicyID uniqueidentifier = null,
	@DisplayPmlFeeIn100Format bit = null,
	@IsRoundUpLpeFee bit = null,
	@RoundUpLpeFeeToInterval decimal(10,3) = null,
	@IsAlwaysDisplayExactParRateOption bit = null,
	@ActualPriceGroupId UniqueIdentifier = NULL
AS
UPDATE LPE_Price_Group
       SET 
		LpePriceGroupName = COALESCE(@LpePriceGroupName, LpePriceGroupName),
        ExternalPriceGroupEnabled = COALESCE(@ExternalPriceGroupEnabled, ExternalPriceGroupEnabled),
		LenderPaidOriginatorCompensationOptionT = COALESCE(@LenderPaidOriginatorCompensationOptionT, LenderPaidOriginatorCompensationOptionT),
		LockPolicyID = COALESCE(@LockPolicyID, LockPolicyID),
		DisplayPmlFeeIn100Format = COALESCE(@DisplayPmlFeeIn100Format, DisplayPmlFeeIn100Format),
		IsRoundUpLpeFee = COALESCE(@IsRoundUpLpeFee, IsRoundUpLpeFee),
		RoundUpLpeFeeToInterval = COALESCE(@RoundUpLpeFeeToInterval, RoundUpLpeFeeToInterval),
		IsAlwaysDisplayExactParRateOption = COALESCE(@IsAlwaysDisplayExactParRateOption, IsAlwaysDisplayExactParRateOption),
		ActualPriceGroupId = COALESCE(@ActualPriceGroupId, ActualPriceGroupId)
WHERE LpePriceGroupId = @LpePriceGroupId AND BrokerId = @BrokerId

