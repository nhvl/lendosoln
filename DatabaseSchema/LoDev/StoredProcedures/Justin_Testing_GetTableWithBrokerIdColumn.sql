-- =============================================
-- Author:		Justin Jia
-- CREATE date: June 14, 2012
-- Description:	Gets all table that has column name ended by 'brokerid' and that column
-- =============================================
ALTER PROCEDURE [dbo].[Justin_Testing_GetTableWithBrokerIdColumn]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select distinct TABLE_NAME, COLUMN_NAME from information_schema.columns
	where column_name like '%brokerid'
	and table_name not like 'view_%'
	and column_name not like '%pmlBrokerId'
	and column_name <> 'actualpricingbrokerid'
END
