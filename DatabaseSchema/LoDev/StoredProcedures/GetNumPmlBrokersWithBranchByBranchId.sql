
-- =============================================
-- Author:		Eduardo Michel
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[GetNumPmlBrokersWithBranchByBranchId] 
	-- Add the parameters for the stored procedure here
	@BranchId uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT Count('') as numPmlBrokersWithBranch
	FROM PML_BROKER
	WHERE BranchId = @BranchId OR 
	CorrespondentBranchId = @BranchId OR 
	MiniCorrespondentBranchId = @BranchId
END

