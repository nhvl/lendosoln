ALTER PROCEDURE [dbo].[Billing_FirstAmericanTitle]
	@StartDate datetime,
	@EndDate datetime
AS
BEGIN
-- dd 2018-11-02 - For use in automate billing executable.

SELECT TitleVendorId, BrokerNm As LenderName, CustomerCode, sLNm As LoanNumber, 
                    SUBSTRING(TitleXml, 
                    PATINDEX('%<PolicyID>%', TitleXml) + 10, 
	                    PATINDEX('%</PolicyID>%', TitleXml) - 10 
	                    - PATINDEX('%<PolicyID>%', TitleXml)) As Policy,
                    CreatedD, sTitleInsPolicyReceivedD As PolicyReceivedDate, cache.sLId As LoanId
                    FROM Title_Vendor_Response tvr WITH (nolock)
                    JOIN Loan_File_Cache cache WITH (nolock) ON cache.sLId = tvr.sLId
                    JOIN Loan_File_Cache_3 cache3 WITH (nolock) ON cache3.sLId = tvr.sLId
                    JOIN Broker WITH (nolock) ON Broker.BrokerId = cache.sBrokerId
                    WHERE TitleXml LIKE '%<PolicyID>%</PolicyID>%'
                    AND Broker.SuiteType = 1 AND cache.sLoanFileT = 0
                    AND ((CreatedD >= @StartDate AND CreatedD < @EndDate AND sTitleInsPolicyReceivedD <> '')
                      OR (sTitleInsPolicyReceivedD >= @StartDate AND sTitleInsPolicyReceivedD < @EndDate AND CreatedD <> ''))
                    ORDER BY TitleVendorId, LenderName, LoanNumber
					
END