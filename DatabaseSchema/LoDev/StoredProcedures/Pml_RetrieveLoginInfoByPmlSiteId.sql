



-- 2015-12-01 TJ - Currently no obvious references in the project to this Stored Procedure other than a comment mentioning it in the list of SPs used to create a PML Principal
ALTER PROCEDURE [dbo].[Pml_RetrieveLoginInfoByPmlSiteId]
	@PmlSiteID uniqueidentifier
AS
	SELECT a.EmployeeId, a.BrokerId, a.Permissions, a.BranchId, a.LastUsedCreditProtocolID, 
                            a.UserFirstNm, a.UserLastNm, a.Email, a.UserId, a.LoginNm, a.LoginSessionId, a.IsSharable, a.PmlLoginSessionId,
		 a.IsPmlManager, a.PmlExternalManagerEmployeeId, a.IsOthersAllowedToEditUnderwriterAssignedFile,
		a.IsLOAllowedToEditProcessorAssignedFile, a.Type, a.LpePriceGroupId, 	a.IsRateLockedAtSubmission, a.IsOnlyAccountantCanModifyTrustAccount,a.HasLenderDefaultFeatures,
		a.LastUsedCreditLoginNm, a.PasswordExpirationD, a.IsQuickPricerEnable, b.IsQuickPricerEnabled AS IsUserQuickPricerEnabled
		, b.IsPricingMultipleAppsSupported, b.PmlBrokerId, b.PmlLevelAccess, b.IsNewPmlUIEnabled, b.IsUsePml2AsQuickPricer, b.PortalMode,
		a.MiniCorrespondentBranchId, a.CorrespondentBranchId
	FROM VIEW_ACTIVE_PML_USER_WITH_BROKER_INFO a WITH (NOLOCK) JOIN BROKER_USER b WITH(NOLOCK) ON a.UserId = b.UserId
	WHERE a.PmlSiteId = @PmlSiteID AND a.PmlSiteId IS NOT NULL





