-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE RetrieveComIdByMclCraCode 
	@MclCraCode char(2)
AS
BEGIN
	SELECT ComId FROM Service_Company WHERE MclCraCode = @MclCraCode
END
