-- =============================================
-- Author:		paoloa
-- Create date: 8/21/2013
-- Description:	
-- =============================================
ALTER PROCEDURE [dbo].[SaveDocMagicAuthenticationForNonPTM]
	@UserId UniqueIdentifier,
	@DocMagicCustomerId varchar(50),
	@DocMagicUserName varchar(50),
	@DocMagicPassword varchar(50) = null,
	@EncryptedDocMagicPassword varbinary(50)
AS
BEGIN
	UPDATE BROKER_USER
	SET
		DocMagicCustomerId = @DocMagicCustomerId,
		DocMagicUserName = @DocMagicUserName,
		DocMagicPassword = COALESCE(@DocMagicPassword, DocMagicPassword),
		EncryptedDocMagicPassword = @EncryptedDocMagicPassword
	WHERE UserId = @UserId 
END
