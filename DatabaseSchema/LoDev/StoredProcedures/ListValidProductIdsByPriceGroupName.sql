-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ListValidProductIdsByPriceGroupName]
	@BrokerId UniqueIdentifier,
	@LpePriceGroupName varchar(100)
AS
BEGIN
	SELECT lLpTemplateId, LenderProfitMargin 
	FROM Lpe_Price_Group lpg JOIN Lpe_Price_Group_Product product ON lpg.LpePriceGroupId = product.LpePriceGroupId
	WHERE lpg.BrokerId = @BrokerId AND lpg.ExternalPriceGroupEnabled = 1
	  AND product.IsValid = 1 AND LpePriceGroupName=@LpePriceGroupName
	
END