ALTER PROCEDURE [dbo].[TASK_ListTasksByBrokerIdUserIdForPipeline] 
	@BrokerId uniqueidentifier,
	@UserId uniqueidentifier = NULL,
	@OrderBy varchar(60),
	@Desc bit
AS

-- REVIEW
-- 6/9/2011: The sorting takes majority of the execution time but we have to put the pipeline in some order. Reviewed. -ThinhNK
-- 7/5/2012: Added CondRowId. -MP

BEGIN
	SELECT TOP 50
		BrokerId
		, TaskId
		, TaskAssignedUserId
		--, TaskIdentityId
		, TaskStatus
		, LoanId
		, TaskOwnerUserId
		, TaskCreatedDate
		, TaskSubject
		, TaskDueDate
		, TaskFollowUpDate
		, CondInternalNotes
		, TaskIsCondition
		, TaskResolvedUserId
		, TaskLastModifiedDate
		, BorrowerNmCached
		, LoanNumCached
		, TaskToBeAssignedRoleId
		, TaskToBeOwnerRoleId
		, TaskCreatedByUserId
		, TaskDueDateLocked
		, TaskDueDateCalcField
		, TaskDueDateCalcDays
		, TaskHistoryXml
		, TaskPermissionLevelId
		, TaskClosedUserId
		, TaskClosedDate
		, TaskRowVersion
		, CondCategoryId
		, CondIsHidden
		--, CondRank
		, CondRowId
		, CondDeletedDate
		, CondDeletedByUserNm
		, CondIsDeleted
		--, TaskAssignedUserFullName as AssignedUserFullName
		--, TaskOwnerFullName as OwnerFullName
		--, TaskClosedUserFullName as ClosingUserFullName
		, TaskAssignedUserFullName
		, TaskOwnerFullName
		, TaskClosedUserFullName
		, CondMessageId
		, AssignToOnReactivationUserId
	FROM TASK t 

	WHERE
		BrokerId = @BrokerId
		AND
		( 
			( @UserId IS NULL OR @UserId = '00000000-0000-0000-0000-000000000000' )
			OR
			 TaskAssignedUserId = COALESCE(@UserId, TaskAssignedUserId)
		)
		AND
		(TaskStatus = 0 OR TaskStatus = 1)
		AND 
		LoanId IN (select slId from Loan_File_Cache with(nolock) where IsValid <> 0 and sBrokerId = @BrokerId)
-- ORDER BY clause is a bit unusual, but
-- we want to avoid dynamic sql if possible.
ORDER BY 
CASE WHEN @Desc = 0 AND @OrderBy = 'TaskId' THEN TaskId END,
CASE WHEN @Desc = 1 AND @OrderBy = 'TaskId' THEN TaskId END DESC,
CASE WHEN @Desc = 0 AND @OrderBy = 'TaskSubject' THEN TaskSubject END,
CASE WHEN @Desc = 1 AND @OrderBy = 'TaskSubject' THEN TaskSubject END DESC,
CASE WHEN @Desc = 0 AND @OrderBy = 'TaskStatus' THEN TaskStatus END,
CASE WHEN @Desc = 1 AND @OrderBy = 'TaskStatus' THEN TaskStatus END DESC,
CASE WHEN @Desc = 0 AND @OrderBy = 'LoanNumCached' THEN LoanNumCached END,
CASE WHEN @Desc = 1 AND @OrderBy = 'LoanNumCached' THEN LoanNumCached END DESC,
CASE WHEN @Desc = 0 AND @OrderBy = 'BorrowerNmCached' THEN BorrowerNmCached END,
CASE WHEN @Desc = 1 AND @OrderBy = 'BorrowerNmCached' THEN BorrowerNmCached END DESC,
CASE WHEN @Desc = 0 AND @OrderBy = 'TaskDueDate' THEN TaskDueDate END,
CASE WHEN @Desc = 1 AND @OrderBy = 'TaskDueDate' THEN TaskDueDate END DESC,
CASE WHEN @Desc = 0 AND @OrderBy = 'TaskFollowUpDate' THEN TaskFollowUpDate END,
CASE WHEN @Desc = 1 AND @OrderBy = 'TaskFollowUpDate' THEN TaskFollowUpDate END DESC,
CASE WHEN @Desc = 0 AND @OrderBy = 'TaskLastModifiedDate' THEN TaskStatus END,
CASE WHEN @Desc = 1 AND @OrderBy = 'TaskLastModifiedDate' THEN TaskStatus END DESC,
CASE WHEN @Desc = 0 AND @OrderBy = 'TaskAssignedUserFullName' THEN LoanNumCached END,
CASE WHEN @Desc = 1 AND @OrderBy = 'TaskAssignedUserFullName' THEN LoanNumCached END DESC,
CASE WHEN @Desc = 0 AND @OrderBy = 'TaskOwnerFullName' THEN BorrowerNmCached END,
CASE WHEN @Desc = 1 AND @OrderBy = 'TaskOwnerFullName' THEN BorrowerNmCached END DESC


END










