ALTER PROCEDURE [dbo].[COMPLIANCEEAGLE_ORDER_Save] 
	@BrokerId uniqueidentifier,
	@LoanId uniqueidentifier,
	@TransactionId uniqueidentifier,
	@VendorLoanId varchar(20),
	@OrderedDate datetime,
	@OrderedBy varchar(50)
AS
BEGIN
    INSERT INTO COMPLIANCEEAGLE_ORDER
    (
		BrokerId,
		LoanId,
		TransactionId,
		VendorLoanId,
		OrderedDate,
		OrderedBy
    )
    VALUES
    (
		@BrokerId,
		@LoanId,
		@TransactionId,
		@VendorLoanId,
		@OrderedDate,
		@OrderedBy
    );
END
