ALTER procedure [dbo].[EDOCS_GetDocTypesByBroker]
	@BrokerId uniqueidentifier
as

select f.folderid, foldername, brokerid, f2.roleid, f2.ispmluser from edocs_folder f left join edocs_folder_x_role f2 on f.folderid = f2.folderid 
WHERE brokerid = @BrokerId

SELECT 
	DocTypeName, 
	DocTypeId, 
	ClassificationId,
	DefaultForPageId,
	FolderId,
    ESignTargetDocTypeId

			
from EDOCS_DOCUMENT_TYPE as doctype 
LEFT JOIN [dbo].[EDOCS_DOCUMENT_TYPE_MAPPING] doctype_map ON doctype.DocTypeId = doctype_map.SourceDocTypeId
where doctype.BrokerId = @BrokerId and IsValid = 1
