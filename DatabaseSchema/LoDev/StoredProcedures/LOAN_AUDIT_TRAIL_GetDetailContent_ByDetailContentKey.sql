-- =============================================
-- Author:		Thien Nguyen
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[LOAN_AUDIT_TRAIL_GetDetailContent_ByDetailContentKey]
    @DetailContentKey varchar(100),   
	@LoanId Guid,	-- for security reason
    @BrokerId Guid  -- for security reason
    
AS
BEGIN
    SELECT  DetailContentKey
    FROM LOAN_AUDIT_TRAIL
    WHERE BrokerId = @BrokerId AND LoanId = @LoanId AND DetailContentKey = @DetailContentKey
END    
