-- =============================================
-- Author:		Scott Kibler
-- Create date: 3/20/2017
-- Description:	
--	Allow people with the specified role to post to the Conversation Category given by CategoryName, BrokerId.
-- Implementation:
--  Insert the role into he ROLE_X_CONVERSATION_CATEGORY if it's not already there.
-- =============================================
CREATE PROCEDURE [dbo].[PermitRoleToPostToCategory]
	@CategoryName varchar(100),
	@BrokerId uniqueidentifier,
	@RoleID uniqueidentifier
AS
BEGIN
	IF NOT EXISTS
	(
		SELECT * 
		FROM ROLE_X_CONVERSATION_CATEGORY 
		WHERE 
				RoleId = @RoleId
			AND CategoryName = @CategoryName
			AND BrokerId = @BrokerId		
	)
	BEGIN
		INSERT INTO 
			ROLE_X_CONVERSATION_CATEGORY 
				( CategoryName,  BrokerId,  RoleId)
		VALUES
				(@CategoryName, @BrokerId, @RoleId)
	END
END
