CREATE PROCEDURE [dbo].[Billing_MortgageInsurance]
	@StartDate datetime,
	@EndDate datetime
AS
BEGIN
-- dd 2018-11-02 - For use in automate billing executable.

SELECT BrokerNm AS LenderName, CustomerCode, sLNm As LoanNumber, cache.sLId AS LoanId, TransactionId, OrderNumber, OrderedDate, mioi.VendorId
                            FROM Mortgage_Insurance_Order_Info mioi WITH (nolock) JOIN Loan_File_Cache cache WITH (nolock) ON cache.sLId = mioi.sLId
                                                                                  JOIN Broker WITH (nolock) ON Broker.BrokerId = cache.sBrokerId
                            WHERE OrderedDate >= @StartDate AND OrderedDate < @EndDate
                                AND Broker.SuiteType = 1 AND cache.sLoanFileT = 0
                                AND IsQuote = 0															-- policy only (not quote)
                                AND ResponseXmlContent LIKE '%MIDecisionType=""Approved%'
                                AND NOT EXISTS-- exclude re-orders based on 90 - day lookback
                                (SELECT 1 FROM Mortgage_Insurance_Order_Info mioi_past WITH(nolock)
                                WHERE mioi_past.sLId = mioi.sLId-- same loan
                                AND mioi_past.VendorId = mioi.VendorId-- same vendor
                                AND IsQuote = 0-- policy only(not quote)
                                AND mioi_past.ResponseXmlContent LIKE '%MIDecisionType=""Approved%'
                                AND mioi_past.OrderedDate > DATEADD(DAY, -90, mioi.OrderedDate)   -- occurred within prior 90 days
                                AND
                                    (
                                        (
                                            mioi_past.OrderedDate < mioi.OrderedDate   -- is actually a prior order
                                            AND NOT ( mioi_past.OrderedDate >= @StartDate AND mioi_past.OrderedDate < @EndDate ) --was not within the same invoice month
                                        )
                                    OR
                                        (
                                        mioi_past.OrderedDate >= @StartDate AND mioi_past.OrderedDate < @EndDate-- only consider one transaction in the invoice month

                                        AND mioi_past.OrderNumber > mioi.OrderNumber-- ensures that this inner select is not returning the same record as the outer
                                        )
                                    )
                                )

                                ORDER BY mioi.VendorId, CustomerCode, LoanNumber, TransactionId
								
END