-- =============================================
-- Author:		Justin Lara
-- Create date: 11/19/2018
-- Description:	Deletes all split premium options
--				associated with a vendor.
-- =============================================
ALTER PROCEDURE [dbo].[MORTGAGE_INSURANCE_VENDOR_SPLIT_PREMIUMS_DeleteByVendor]
	@VendorId uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    DELETE FROM MORTGAGE_INSURANCE_VENDOR_SPLIT_PREMIUMS
    WHERE VendorId = @VendorId
END
GO
