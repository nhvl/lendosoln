-- =============================================
-- Author:		Huy Nguyen	
-- Create date: 5/5/2015
-- Description:	
-- =============================================
CREATE PROCEDURE dbo.ALL_USER_REGISTERED_MOBILE_DEVICE_PasswordList
	@BrokerId UniqueIdentifier,
    @UserId UniqueIdentifier,
	@DeviceUniqueId varchar(50)
AS
BEGIN
    SELECT Id,PasswordHash,PasswordSalt,DeviceUniqueId,FailedAttempts
    FROM ALL_USER_REGISTERED_MOBILE_DEVICE
	WHERE BrokerId = @BrokerId
		AND UserId = @UserId
		AND IsActive = 1
		AND FailedAttempts <10
		AND ( DeviceUniqueId = @DeviceUniqueId OR(DeviceUniqueId ='' AND DATEDIFF(MINUTE, CreateD,GETDATE())<10))
END
