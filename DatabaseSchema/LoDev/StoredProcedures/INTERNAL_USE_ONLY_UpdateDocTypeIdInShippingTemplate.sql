CREATE PROCEDURE [dbo].[INTERNAL_USE_ONLY_UpdateDocTypeIdInShippingTemplate]
	@OldDocTypeId int,
	@NewDocTypeId int,
	@ShippingTemplateId int
as
	
	update EDOCS_DOCUMENT_TYPE_X_SHIPPING_TEMPLATE
	
	set DocTypeId = @NewDocTypeId
	from EDOCS_DOCUMENT_TYPE_X_SHIPPING_TEMPLATE dtst with (nolock)
where	
DocTypeId = @OldDocTypeId
	and ShippingTemplateId = @ShippingTemplateId

		
