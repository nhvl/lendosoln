-- =============================================
-- Author:		Antonio Valencia
-- Create date: March 26, 2008
-- Description:	Updates the password expiration 
-- of a certain user. [Based on BatchPasswordOptionsUpdate] 
-- =============================================
CREATE PROCEDURE [dbo].[UpdatePasswordOptionForUser] 
	@UserId uniqueidentifier,
	@PasswordExpirationD SmallDateTime = NULL, -- Default is password does not expired.
	@PasswordExpirationPeriod int = 0 -- Default is no expiration period
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE ALL_USER 
		SET PasswordExpirationD = @PasswordExpirationD, PasswordExpirationPeriod = @PasswordExpirationPeriod
	WHERE UserId = @UserId
	AND ((IsActiveDirectoryUser = 0) OR (ALL_USER.Type <> 'B'));
END
