CREATE PROCEDURE UpdateLicense 
	@LicenseId Guid,
	@ExpirationDate smalldatetime,
	@SeatCount smallint,
	@Description varchar(100)
AS
BEGIN
	UPDATE License 
      SET ExpirationDate = @ExpirationDate,
          SeatCount = @SeatCount,
          Description = @Description
    WHERE LicenseId=@LicenseId
END
