

CREATE PROCEDURE [dbo].[GetPathToDataTracByBrokerId]
	@BrokerId uniqueidentifier
AS
	SELECT PathToDataTrac, DataTracWebServiceUrl
	FROM BROKER
	WHERE BrokerId = @BrokerId


