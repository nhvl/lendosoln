ALTER PROCEDURE [dbo].[InvalidateCreditReport]
	@Owner Guid,
	@DeleteLqi bit = 1
AS
UPDATE Service_File
SET IsInEffect = 0
WHERE Owner = @Owner
AND (@DeleteLqi = 1
	OR FileType <> 'LQICreditReport')
