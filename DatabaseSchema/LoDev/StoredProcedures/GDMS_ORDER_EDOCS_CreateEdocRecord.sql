-- =============================================
-- Author:		Eric Mallare
-- Create date: 3/20/2018
-- Description:	Inserts Edoc records for a GDMS appraisal order.
-- =============================================
ALTER PROCEDURE [dbo].[GDMS_ORDER_EDOCS_CreateEdocRecord]
	@FileNumber int,
	@BrokerId uniqueidentifier,
	@LoanId uniqueidentifier,
	@GdmsFileId int,
	@EdocId uniqueidentifier,
	@FileName varchar(300)
AS
BEGIN

	If (SELECT COUNT(*) 
	    FROM GDMS_ORDER_EDOCS 
	    WHERE 
			GdmsFileId=@GdmsFileId AND
			FileNumber=@FileNumber AND
			LoanId=@LoanId AND
			BrokerId=@BrokerId) < 1
	BEGIN
		INSERT INTO GDMS_ORDER_EDOCS
		(
			FileNumber,
			BrokerId,
			LoanId,
			GdmsFileId,
			EdocId,
			[FileName]
		)
		VALUES
		(
			@FileNumber,
			@BrokerId,
			@LoanId,
			@GdmsFileId,
			@EdocId,
			@FileName
		)
	END
END