-- =============================================
-- Author:		Kibler, Scott
-- Create date: 10/31/2011
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[RetrieveUserInfoByUserId]
	-- Add the parameters for the stored procedure here
	@UserId uniqueidentifier
	AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT Type from all_user where UserId = @UserID
END
