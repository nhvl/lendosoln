-- =============================================
-- Author:		Thien Nguyen
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[FLOOD_RESELLER_Update]
	@FloodResellerId uniqueidentifier,
    @Name varchar(200),
    @Url varchar(500)
AS
BEGIN

	UPDATE FLOOD_RESELLER
		SET Name = @Name, Url = @Url
		WHERE FloodResellerId = @FloodResellerId		
		
END
