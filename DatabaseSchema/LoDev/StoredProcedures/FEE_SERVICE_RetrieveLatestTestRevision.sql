-- Get Latest Test Revision Only --
CREATE PROCEDURE [dbo].[FEE_SERVICE_RetrieveLatestTestRevision] 
	@BrokerId uniqueidentifier
AS
BEGIN
	SELECT TOP(1) f.BrokerId, f.Comments, f.FeeTemplateXmlContent, f.FileDbKey, f.UploadedByUserId, f.UploadedD,
		COALESCE(e.UserFirstNm + ' ' + e.UserLastNm, '') as UploadedByUserName, f.Id, f.IsTest
	FROM FEE_SERVICE_REVISION f LEFT JOIN EMPLOYEE e on f.UploadedByUserId = e.EmployeeUserId
	WHERE BrokerId = @BrokerId AND IsTest = 1 ORDER BY UploadedD DESC
END