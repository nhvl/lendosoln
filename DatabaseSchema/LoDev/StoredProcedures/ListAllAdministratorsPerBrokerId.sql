


CREATE PROCEDURE [dbo].[ListAllAdministratorsPerBrokerId]
	@BrokerId uniqueidentifier
AS
	SELECT 
	UserFirstNm, UserLastNm, BranchId, Email
FROM 
	View_Employee_Role
WHERE 
	RoleDesc = 'Administrator' and BrokerId = @BrokerId and IsActive = 1



