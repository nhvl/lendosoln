-- =============================================
-- Author:		Geoff Feltman
-- Create date: 2018-08-24
-- Description:	Enables the coborrower flag for a legacy app and adds an
-- association between that consumer and a ULAD application.
-- =============================================
ALTER PROCEDURE [dbo].[AddCoborrowerToLegacyAppAndUladApp]
	@LoanId UniqueIdentifier,
	@LegacyAppId UniqueIdentifier,
	@UladAppId UniqueIdentifier,
	@ConsumerId UniqueIdentifier OUT,
	@UladAppConsumerId UniqueIdentifier OUT
AS
BEGIN
	BEGIN TRANSACTION;

	BEGIN TRY
		UPDATE APPLICATION_A
		SET aHasCoborrowerData = 1
		WHERE aAppId = @LegacyAppId;

		SELECT @UladAppConsumerId = NEWID();
		SELECT @ConsumerId = aCConsumerId from APPLICATION_A WHERE aAppId = @LegacyAppId;
		DECLARE @IsPrimary Bit;
		SELECT @IsPrimary = 0;
		EXEC ULAD_APPLICATION_X_CONSUMER_Create @UladAppConsumerId, @LoanId, @UladAppId, @ConsumerId, @LegacyAppId, @IsPrimary;
	END TRY
	BEGIN CATCH
		ROLLBACK;
	END CATCH

	COMMIT;
END