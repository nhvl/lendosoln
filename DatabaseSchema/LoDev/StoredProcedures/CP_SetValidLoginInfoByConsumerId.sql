
CREATE PROCEDURE [dbo].[CP_SetValidLoginInfoByConsumerId]
	@ConsumerId bigint,
	@BrokerId uniqueidentifier
AS

UPDATE CP_CONSUMER
SET LastLoggedInD = GETDATE(), LoginFailureNumber = 0
WHERE	ConsumerId = @ConsumerId AND
		BrokerId = @BrokerId

