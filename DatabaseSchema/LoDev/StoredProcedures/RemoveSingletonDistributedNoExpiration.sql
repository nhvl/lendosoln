CREATE PROCEDURE RemoveSingletonDistributedNoExpiration
	@ObjId varchar(100)
as
-- NOTE THAT THIS STORE PROC MUST OPERATE ATOMICALLY
	update singleton_tracker
	set IsExisting = 0
	where ObjId = @ObjId and IsExisting = 1 and DateDiff( day, ExpirationTime, '12/31/2050' ) = 0 
