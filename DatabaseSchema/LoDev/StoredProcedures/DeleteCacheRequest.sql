



CREATE   PROCEDURE [dbo].[DeleteCacheRequest] 
	@ReqId uniqueidentifier
AS
	delete from LOAN_CACHE_UPDATE_REQUEST
	WHERE RequestId = @ReqId
	
	if( 0 != @@error )
	begin
		RAISERROR('Error in deleting from LOAN_CACHE_UPDATE_REQUEST table in DeleteCacheRequest sp', 16, 1);
		return -100;
	end
	return 0;


