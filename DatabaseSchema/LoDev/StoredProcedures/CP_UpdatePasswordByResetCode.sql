


alter PROCEDURE [dbo].[CP_UpdatePasswordByResetCode]
	@LoginName varchar(80),	
	@PasswordHash varchar(200),
	@PasswordResetCode varchar(200)
AS
BEGIN
	UPDATE CP_CONSUMER 
	SET		PasswordHash = @PasswordHash, PasswordResetCode = '', PasswordRequestD = NULL, IsTemporaryPassword = 0, IsLocked = 0, LoginFailureNumber = 0
	FROM CP_CONSUMER cp
	join broker b on cp.brokerid = b.brokerid
	WHERE	Email = @LoginName AND PasswordResetCode = @PasswordResetCode and b.status = 1

END



