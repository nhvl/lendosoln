ALTER PROCEDURE [dbo].[UpdateCacheRequest]
	@ReqId uniqueidentifier, 
	@Error text, 
	@sqlFields text, 
	@LastRetryD datetime = " ",
	@WhoRetriedLastLogin varchar(36) = " "
AS
	IF(@Error is NULL)
		BEGIN
			UPDATE LOAN_CACHE_UPDATE_REQUEST
			SET AffectedCachedFields = @sqlFields
			WHERE RequestId = @ReqId
		END	
	ELSE
		BEGIN
			UPDATE LOAN_CACHE_UPDATE_REQUEST
			SET ErrorInfo = @Error, 
				LastRetryD = @LastRetryD,
				WhoRetriedLastLoginNm = @WhoRetriedLastLogin,
				NumRetries = NumRetries + 1
			WHERE RequestId = @ReqId
		END
	
	
	if( 0 != @@error )
	begin
		RAISERROR('Error in updating LOAN_CACHE_UPDATE_REQUEST table in UpdateCacheRequest sp', 16, 1);
		return -100;
	end
	return 0;
