-- =============================================
-- Author:		Michael Leinweaver
-- Create date: 11/17/2016
-- Description:	Retrieves the name of the specified originating company.
-- =============================================
CREATE PROCEDURE [dbo].[RetrievePmlBrokerNameById]
	@PmlBrokerId UniqueIdentifier, 
	@BrokerId UniqueIdentifier
AS
BEGIN
	SELECT p.Name
	FROM Pml_Broker p
	WHERE p.PmlBrokerId = @PmlBrokerId AND p.BrokerId = @BrokerId
END
