CREATE PROCEDURE [dbo].[TEAM_UpdateUserAssignment] 
	-- Add the parameters for the stored procedure here

	@TeamId uniqueidentifier, 
	@EmployeeId uniqueidentifier,
	@IsPrimary bit
AS
BEGIN

	UPDATE Team_User_Assignment
	SET
	 IsPrimary = COALESCE(@IsPrimary, IsPrimary)
	WHERE
		EmployeeId = @EmployeeId AND 
		TeamId = @TeamId
	
	
END
