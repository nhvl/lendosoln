-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[RetrieveCompassData]
	@CustomerCode varchar(50)
AS
BEGIN

DECLARE @BrokerID uniqueidentifier;
SELECT @BrokerID=brokerid FROM Broker WHERE CustomerCode=@CustomerCode

DECLARE @ReportStartDate smalldatetime;
SELECT @ReportStartDate = DATEADD("yyyy", -1, GETDATE()) -- go back 1 year only
-- dd 4/6/2011 - The order of SELECT columns ARE VERY IMPORTANT. Third party tools rely on this order.
--               Therefore when adding new column always add to the end
SELECT sRLckdD As LockDate,
	sLNm As LoanNumber,
	aBLastNm AS LastName,
	aBFirstNm AS FirstName,
	sLpTemplateNm AS LoanProgram,
	sNoteIR AS NoteRate,
	sFinalLAmt AS TotalLoanAmt,
	sRLckdDays AS LockDays,
	sSpState AS State,
	sRLckdExpiredD AS LockExpiration,
	case( sStatusT ) 
		when '0' then 'Loan Open' 
		when '1' then 'Loan Pre-qual' 
		when '2' then 'Loan Pre-approve' 
		when '3' then 'Loan Registered' 
		when '4' then 'Loan Approved' 
		when '5' then 'Loan Docs' 
		when '6' then 'Loan Funded' 
		when '7' then 'Loan On hold' 
		when '8' then 'Loan Suspended' 
		when '9' then 'Loan Canceled' 
		when '10' then 'Loan Rejected' 
		when '11' then 'Loan Closed' 
		when '12' then 'Lead Open' 
		when '13' then 'Loan Underwriting' 
		when '14' then 'Web Consumer' 
		when '15' then 'Lead Canceled' 
		when '16' then 'Lead Declined' 
		when '17' then 'Lead Other' 
		when '18' then 'Loan Other' 
		when '19' then 'Loan Recorded' 
		when '20' then 'Loan Shipped' 
		when '21' then 'Loan Clear to Close' 
		when '22' then 'Loan Processing' 
		when '23' then 'Loan Final Underwriting' 
		when '24' then 'Loan Docs Back' 
		when '25' then 'Loan Funding Conditions' 
		when '26' then 'Loan Final Docs' 
		when '27' then 'Loan Purchased'  -- Loan Sold
		when '28' then 'Loan Submitted'
		when '29' then 'Loan Pre-Processing'    -- start sk 1/6/2014 opm 145251
		when '30' then 'Loan Document Check'
		when '31' then 'Loan Document Check Failed'
		when '32' then 'Loan Pre-Underwriting'
		when '33' then 'Loan Condition Review'
		when '34' then 'Loan Pre-Doc QC'
		when '35' then 'Loan Docs Ordered'
		when '36' then 'Loan Docs Drawn'
		when '37' then 'Loan Investor Conditions'       --  tied to sSuspendedByInvestorD
		when '38' then 'Loan Investor Conditions Sent'   --  tied to sCondSentToInvestorD
		when '39' then 'Loan Ready For Sale'
		when '40' then 'Loan Submitted For Purchase Review'
		when '41' then 'Loan In Purchase Review'
		when '42' then 'Loan Pre-Purchase Conditions'
		when '43' then 'Loan Submitted For Final Purchase Review'
		when '44' then 'Loan In Final Purchase Review'
		when '45' then 'Loan Clear To Purchase'
		when '46' then 'Loan Purchased'        -- don't confuse with the old { E_sStatusT.Loan_LoanPurchased, which is now Loan Sold in the UI.
		when '47' then 'Loan Counter Offer Approved'
		when '48' then 'Loan Withdrawn'
		when '49' then 'Loan Archived'    
	end as LoanStatus, 
	case( sProdImpound ) 
		when 1 then 'Yes' 
		when 0 then 'No' 
	end as Impound, 
	sLtvR AS LTV,
	sCltvR AS CLTV,
	sCreditScoreType2 AS QualifyingScore,
	case( sLPurposeT ) 
		when '3' then 'Construction' 
		when '4' then 'Construction Permanent' 
		when '5' then 'Other' 
		when '0' then 'Purchase' 
		when '1' then 'Refinance' 
		when '2' then 'Refinance Cash-out' 
		when '6' then 'FHA Streamlined Refinance'
		when '7' then 'VAIRRL'
	end as LoanPurpose, 
	case (sLPurposeT) 
		when '1' then 'Rate & Term'
		when '2' then 'Cashout'
		else '' 
	end as Refi_Reason, 
	case( sProdSpT ) 
		when '8' then 2
		when '9' then 3
		when '10' then 4
		else 1
	end as NumberofUnits,
	case( aOccT ) 
		when '2' then 'Investment' 
		when '0' then 'Primary Residence' 
		when '1' then 'Secondary Residence' 
	end as OccupancyType, 
	sTerm AS Term,
	sQualBottomR AS DTI,
	case( sProdSpT ) 
		when '0' then 'SFR / Detached' 
		when '8' then '2 Units' 
		when '9' then '3 Units' 
		when '10' then '4 Units' 
		when '1' then 'PUD' 
		when '6' then 'Commercial' 
		when '2' then 'Condo' 
		when '3' then 'Co-Op' 
		when '4' then 'Manufactured' 
		when '7' then 'Mixed Use' 
		when '5' then 'Attached PUD' 
		when '11' then 'Modular' 
		when '12' then 'Rowhouse' 
	end AS PropertyType,
	-sBrokComp1Pc AS BEPrice,	
	CASE
		WHEN (sInvestorLockLpInvestorNm IS NULL OR sInvestorLockLpInvestorNm = '') THEN sLpInvestorNm
		ELSE sInvestorLockLpInvestorNm
	END AS InvestorName,

sBrokerLockFinalBrokComp1PcPrice,-sBrokerLockTotalLenderAdj AS sBrokerLockTotalLenderAdj,
sLPurchaseD AS PurchaseDate,
sInvestorLockRLckdD AS CommitmentDate,
sRLckDN AS LockDateComment,
sInvestorLockBrokComp1PcPrice AS InvestorLockPrice,
sSubmitN AS RegisteredDateComment,
	case ( sInvestorLockCommitmentT )
		when 0 then 'Best Efforts'
		when 1 then 'Mandatory'
		when 2 then 'Hedged'
		when 3 then 'Securitized'		
	end as InvestorCommitmentType,
	case ( sInvestorLockRateLockStatusT )
		when 0 then 'Not Locked'
		when 1 then 'Locked'
	end as InvestorRateLockStatus,
sInvestorLockRateSheetId AS BaseBuyPrice,
sInvestorLockBaseBrokComp1PcPrice AS BaseSellPrice,
sInvestorLockRateSheetEffectiveTime AS SellPrice,
sCanceledN AS FalloutDate,
sInvestorLockRateSheetID AS InvestorRateSheetId,
sInvestorLockProjectedProfit AS GrossProjProfitAndLoss,
sTrNotes AS RateLockInternalNotes,
sInvestorLockLoanNum AS InvestorLoanNumber,
sInvestorLockConfNum AS InvestorLockConfirmNumber,

sFundD AS FundingDate,
case (sProdDocT)
	when '0' then 'Full Document'
	when '1' then 'Alt'
	when '2' then 'Lite'
	when '3' then 'NIV (SIVA)'
	when '4' then 'VISA'
	when '5' then 'NIV (SISA)'
	when '6' then 'No Ratio'
	when '7' then 'NINA'
	when '8' then 'NISA'
	when '9' then 'No Doc'
	when '10' then 'No Doc Verif Assets'
	when '11' then 'VINA'
	when '12' then 'Streamline'
	end as DocType
	
	FROM LOAN_FILE_CACHE lc WITH(NOLOCK) JOIN LOAN_FILE_CACHE_2 lc2 WITH(NOLOCK) ON lc.sLId = lc2.sLId

where 
	( sBrokerId = @BrokerID ) and 
	( IsValid = 1 ) and 
	( IsTemplate = 0 ) and 
	( sIsRateLocked  = 1 ) and 
	( 
		sPreApprovD >= @ReportStartDate or
		sUnderwritingD >= @ReportStartDate or
		sOpenedD >= @ReportStartDate or
		sPreQualD >= @ReportStartDate or
		sDocsD >= @ReportStartDate or
		sSubmitD >= @ReportStartDate or
		sApprovD >= @ReportStartDate or
		sFundD >= @ReportStartDate or
		sShippedToInvestorD >= @ReportStartDate or
		sRecordedD >= @ReportStartDate or
		sClosedD >= @ReportStartDate or
		sOnHoldD >= @ReportStartDate or
		sSuspendedD >= @ReportStartDate or
		sCanceledD >= @ReportStartDate or
		sRejectD >= @ReportStartDate
	)
order by  
	sLpTemplateNm asc , 
	case( sStatusT) 
		when '15' then 0 
		when '16' then 1 
		when '12' then 2 
		when '17' then 3 
		when '4' then 4 
		when '9' then 5 
		when '11' then 6 
		when '5' then 7 
		when '6' then 8 
		when '7' then 9 
		when '0' then 10 
		when '18' then 11 
		when '2' then 12 
		when '1' then 13 
		when '19' then 14 
		when '10' then 15 
		when '3' then 16 
		when '8' then 17 
		when '13' then 18 
		when '20' then 19 
		when '21' then 20 
		else 21 
	end asc




END
