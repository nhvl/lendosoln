ALTER PROCEDURE [dbo].[ListCreditProxyByBrokerID] 
	@BrokerID Guid
AS
BEGIN
	SELECT
		CrAccProxyId,
		CraUserName,
		CraPassword,
		EncryptedCraPassword,
		CraAccId,
		CraId,
		IsExperianPulled,
		IsEquifaxPulled,
		IsTransUnionPulled,
		CrAccProxyDesc,
		IsFannieMaePulled,
		CreditMornetPlusPassword,
		EncryptedCreditMornetPlusPassword,
		CreditMornetPlusUserId,
		AllowPmlUserToViewReports,
		EncryptionKeyId
	FROM Credit_Report_Account_Proxy
	WHERE BrokerID = @BrokerID
		AND IsValid = 1
	ORDER BY CrAccProxyDesc
END
