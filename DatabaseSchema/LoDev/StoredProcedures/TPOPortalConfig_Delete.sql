-- =============================================
-- Author:		Scott Kibler
-- Create date: 2/4/2014
-- Description:	opm 150384
-- =============================================
CREATE PROCEDURE [dbo].TPOPortalConfig_Delete
	@ID uniqueidentifier,
	@BrokerID uniqueidentifier
AS
BEGIN
	UPDATE LENDER_TPO_LANDINGPAGECONFIG
	SET IsDeleted = 1
	WHERE ID = @ID
	and BrokerID = @BrokerID
END

