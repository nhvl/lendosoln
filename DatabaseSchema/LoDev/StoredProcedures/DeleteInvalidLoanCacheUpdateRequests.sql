-- =============================================
-- Author:		Geoff Feltman
-- Create date: 2/27/17
-- Description:	Delete cache failures that are no longer associated
-- with a loan.
-- =============================================
CREATE PROCEDURE dbo.DeleteInvalidLoanCacheUpdateRequests
AS
BEGIN
	delete
	from loan_cache_update_request
	where not exists 
		(select a.slid 
		 from loan_file_a a
		 where a.slid = loan_cache_update_request.slid)
		 
	select @@rowcount
END
GO
