-- =============================================
-- Author:		Michael Leinweaver
-- Create date: 5/23/2018
-- Description:	Lists all originating company user pipeline settings.
-- =============================================
CREATE PROCEDURE [dbo].[LENDER_ORIGINATING_COMPANY_USER_PIPELINE_SETTINGS_ListAll] 
	@BrokerId uniqueidentifier
AS
BEGIN
	SELECT 
		ps.DisplayOrder, 
		ps.BrokerId, 
		ps.SettingId, 
		ps.QueryId, 
		r.QueryName as 'PipelineReportName',
		ps.PortalDisplayName,
		ps.OverridePipelineReportName,
		ps.AvailableForBrokerPortalMode,
		ps.AvailableForMiniCorrespondentPortalMode,
		ps.AvailableForCorrespondentPortalMode
	FROM 
		LENDER_ORIGINATING_COMPANY_USER_PIPELINE_SETTINGS ps
		JOIN REPORT_QUERY r ON ps.QueryId = r.QueryId
	WHERE ps.BrokerId = @BrokerId
END