

CREATE PROCEDURE [dbo].[QMoveToMessage]
(
	@MessageId bigint
) 
AS 

	INSERT INTO Q_Message(QueueId, Priority, Subject1, Subject2, DataLoc, InsertionTime, Data) SELECT QueueId, Priority, Subject1, Subject2, DataLoc, InsertionTime, Data FROM Q_ARCHIVE WHERE MessageId = @MessageId; 
	DELETE FROM Q_ARCHIVE WHERE MessageId = @MessageId; 


