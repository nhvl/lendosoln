CREATE procedure [dbo].[EDOCS_GetDocsByApp]
	@AppId uniqueidentifier
as
select DocumentId, FileDBKey_CurrentRevision
from EDOCS_DOCUMENT
where aAppId = @AppId