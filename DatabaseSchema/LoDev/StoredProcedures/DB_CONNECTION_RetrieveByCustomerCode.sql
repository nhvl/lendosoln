-- =============================================
-- Author:		David Dao
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE dbo.DB_CONNECTION_RetrieveByCustomerCode
    @CustomerCode varchar(36)
AS
BEGIN
    SELECT DbConnectionId FROM DB_CONNECTION_x_BROKER
    WHERE CustomerCode = @CustomerCode

END
