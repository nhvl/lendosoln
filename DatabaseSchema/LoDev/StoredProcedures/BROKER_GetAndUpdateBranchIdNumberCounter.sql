-- =============================================
-- Author:		Geoff Feltman
-- Create date: 5/5/17
-- Description:	Retrieves and increments the branch id number counter.
-- Uses a technique found here: http://stackoverflow.com/a/13996911
-- =============================================
ALTER PROCEDURE [dbo].[BROKER_GetAndUpdateBranchIdNumberCounter]
	@BrokerId uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @BranchIdNumberCounter int
    
	UPDATE [BROKER]
	SET @BranchIdNumberCounter = BranchIdNumberCounter,
		BranchIdNumberCounter = BranchIdNumberCounter + 1
	WHERE BrokerId = @BrokerId 
			
	SELECT @BranchIdNumberCounter as BranchIdNumberCounter
END