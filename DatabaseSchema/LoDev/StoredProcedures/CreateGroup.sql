CREATE PROCEDURE [CreateGroup]
	@GroupId uniqueidentifier OUT,
	@BrokerId uniqueidentifier,
	@GroupName varchar(100) ,
	@GroupType int,
	@Description varchar(500)	
AS
BEGIN
	SET NOCOUNT ON
	BEGIN TRANSACTION		
		SET @GroupId = NEWID()
		INSERT INTO [GROUP] (GroupId, BrokerId, GroupName, Description, GroupType) VALUES (@GroupId, @BrokerId, @GroupName, COALESCE(@Description, ''), @GroupType)
		if( 0!=@@error)
		BEGIN
			ROLLBACK TRANSACTION
			RAISERROR('Error in insert to Group table in CreateGroup sp', 16, 1);
			RETURN -100;
		END
	COMMIT TRANSACTION
	RETURN 0;
END
