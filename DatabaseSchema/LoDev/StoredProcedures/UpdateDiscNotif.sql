CREATE PROCEDURE UpdateDiscNotif
	@NotifId 		uniqueidentifier,
	@NotifAlertDate 	datetime,
	@NotifIsRead 		bit,
	@NotifIsValid		bit,
	@NotifLastReadDate	datetime,
	@NotifInvalidDate	datetime,
	@NotifUpdatedDate	datetime = NULL
AS
	UPDATE Discussion_Notification
	SET
		NotifAlertDate = @NotifAlertDate,
		NotifIsRead = @NotifIsRead,
		NotifUpdatedDate = COALESCE( @NotifUpdatedDate , getdate() ),
		NotifIsValid = @NotifIsValid,
		NotifLastReadD = @NotifLastReadDate,
		NotifInvalidDate = @NotifInvalidDate
	WHERE
		NotifId = @NotifId
	IF( 0!=@@error )
	BEGIN
		RAISERROR('Error in updating Discussion_Notification in UpdateDiscNotif sp', 16, 1);
		return -100;
	END
	RETURN 0;
