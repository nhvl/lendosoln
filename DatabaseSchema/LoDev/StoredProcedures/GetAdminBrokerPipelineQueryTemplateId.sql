


CREATE PROCEDURE [dbo].[GetAdminBrokerPipelineQueryTemplateId]
@IsPmlEnabled int = 1,
@AdminBrokerId UniqueIdentifier
AS
	IF (@IsPmlEnabled = 1 or @IsPmlEnabled = null)
		BEGIN
			SELECT
				q.QueryId
			FROM
				Report_Query AS q
			WHERE
				q.BrokerId = @AdminBrokerId
				AND
				q.QueryName = 'Simple pipeline'
		END
	ELSE
		BEGIN
			SELECT
				q.QueryId
			FROM
				Report_Query AS q
			WHERE
				q.BrokerId = @AdminBrokerId
				AND
				q.QueryName = 'Simple retail pipeline'
		END

	
	IF( @@error != 0 )
	BEGIN
		RAISERROR('Error in the select statement in GetAdminBrokerPipelineQueryTemplateId sp', 16, 1);
		RETURN 0;
	END



