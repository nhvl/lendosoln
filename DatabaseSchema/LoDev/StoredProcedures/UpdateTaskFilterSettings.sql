CREATE  PROCEDURE UpdateTaskFilterSettings
      @UserID Guid,
      @DiscFilterSettings char(50),
      @TaskTab char(1) -- T: Tracked Tasks, L: Tasks in Your Loans
AS
      DECLARE @Offset tinyint
      IF (@TaskTab = 'T')
            SET @Offset = 51
      ELSE
            IF (@TaskTab = 'L')
                  SET @Offset = 1
            ELSE
                  BEGIN
                        RAISERROR('Invalid TaskTab in UpdateTaskFilterSettings sp', 16, 1);
                        RETURN -100;
                  END
      UPDATE Broker_User
      SET DiscFilterSettings = STUFF(DiscFilterSettings, @Offset, 50, @DiscFilterSettings)
      WHERE UserID = @UserID
      IF(0!=@@error)
      BEGIN
            RAISERROR('Error in the update statement in UpdateTaskFilterSettings sp', 16, 1);
            RETURN -100;
      END
      RETURN 0;
