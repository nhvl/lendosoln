-- =============================================
-- Author:		Justin Lara
-- Create date: 8/22/2018
-- Description:	Saves a document framework order.
-- =============================================
ALTER PROCEDURE [dbo].[DOCUMENT_FRAMEWORK_ORDER_Create]
	@OrderId int OUTPUT,
	@BrokerId uniqueidentifier,
	@LoanId uniqueidentifier,
	@TransactionId varchar(50),
	@PackageName varchar(50),
	@OrderedBy varchar(50),
	@OrderedByUserId uniqueidentifier,
	@OrderedDate datetime,
	@VendorName varchar(50),
	@MERSRegistrationSelected bit,
	@EDiscloseSelected bit,
	@ESignSelected bit,
	@ManualFulfillmentSelected bit,
	@EmailDocumentsSelected bit,
	@IndividualFormsSelected bit
AS
BEGIN
    INSERT INTO [dbo].[DOCUMENT_FRAMEWORK_ORDER]
    (
		BrokerId,
		LoanId,
		TransactionId,
		PackageName,
		OrderedBy,
		OrderedByUserId,
		OrderedDate,
		VendorName,
		MERSRegistrationSelected,
		EDiscloseSelected,
		ESignSelected,
		ManualFulfillmentSelected,
		EmailDocumentsSelected,
		IndividualFormsSelected
	)
	VALUES
	(
		@BrokerId,
		@LoanId,
		@TransactionId,
		@PackageName,
		@OrderedBy,
		@OrderedByUserId,
		@OrderedDate,
		@VendorName,
		@MERSRegistrationSelected,
		@EDiscloseSelected,
		@ESignSelected,
		@ManualFulfillmentSelected,
		@EmailDocumentsSelected,
		@IndividualFormsSelected
	)
	
	SET @OrderId = SCOPE_IDENTITY();
END
