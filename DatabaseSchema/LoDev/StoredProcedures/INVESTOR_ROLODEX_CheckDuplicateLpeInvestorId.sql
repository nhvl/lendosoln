-- =============================================
-- Author:		David Dao
-- Create date: 
-- Description:	Check to see if LpeInvestorId is assign to other rolodex contact.
-- =============================================
CREATE PROCEDURE dbo.INVESTOR_ROLODEX_CheckDuplicateLpeInvestorId
    @BrokerId uniqueidentifier,
    @ExcludeId int = NULL,
    @LpeInvestorId int
AS
BEGIN

    SELECT 1 FROM INVESTOR_ROLODEX
    WHERE BrokerId=@BrokerId AND LpeInvestorId=@LpeInvestorId AND (@ExcludeId IS NULL OR InvestorRolodexId <> @ExcludeId)
    

END
