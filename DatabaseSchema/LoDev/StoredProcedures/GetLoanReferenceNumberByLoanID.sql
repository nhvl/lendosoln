CREATE  PROCEDURE [dbo].[GetLoanReferenceNumberByLoanID]
	@LoanID uniqueidentifier
AS
	SELECT sLRefNm 
	FROM LOAN_FILE_CACHE_4 with(nolock)
	WHERE
		sLId = @LoanID
