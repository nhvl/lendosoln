ALTER PROCEDURE [dbo].[RetrievePmlBrokerByNameSearch]
	@PmlBrokerSearch varchar(100),
	@BrokerId UniqueIdentifier,
	@IsActive bit = NULL
AS
BEGIN
	SELECT 	*
	FROM 
		Pml_Broker
	WHERE 
		Name LIKE @PmlBrokerSearch
		AND
		BrokerId = @BrokerId
		AND
		IsActive = COALESCE(@IsActive, IsActive)
	ORDER BY
		Name
END

