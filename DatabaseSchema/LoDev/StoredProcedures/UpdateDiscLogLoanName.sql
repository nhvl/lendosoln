CREATE PROCEDURE UpdateDiscLogLoanName 
	@LoanId uniqueidentifier,
	@NewLoanName varchar(100)
AS
update discussion_log
set DiscRefObjNm1 = @NewLoanName
where DiscRefObjId = @LoanId
