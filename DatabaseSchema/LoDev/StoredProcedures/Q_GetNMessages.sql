-- =============================================
-- Author:		Matthew Pham
-- Create date: 2/23/12
-- Description:	Gets the top N messages from the queue
-- =============================================
CREATE PROCEDURE Q_GetNMessages 
	-- Add the parameters for the stored procedure here
	@N int = 0, 
	@QueueId int,
	@Priority tinyint
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT TOP (@N) 
		MessageId, Subject1, Data, Subject2, Priority, InsertionTime, DataLoc
	FROM
		Q_MESSAGE
	WHERE
		QueueId = @QueueId
		AND Priority = @Priority
END
