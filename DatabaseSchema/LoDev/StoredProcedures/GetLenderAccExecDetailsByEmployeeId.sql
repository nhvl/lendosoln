CREATE PROCEDURE [dbo].[GetLenderAccExecDetailsByEmployeeId] 
	@EmployeeId uniqueidentifier
AS
-- Retrieve information of the associate lender acct exec for this employee.
SELECT e.EmployeeId, e.UserFirstNm, e.UserLastNm, e.Phone, e.Fax, e.Addr, e.City, e.State, e.Zip, e.Email, e.LicenseXmlContent
FROM broker_user u WITH (NOLOCK) JOIN Employee e WITH (NOLOCK) ON u.LenderAccExecEmployeeId = e.EmployeeId
WHERE u.EmployeeId = @EmployeeId

