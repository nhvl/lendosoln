-- =============================================
-- Author:		David Dao
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE dbo.BROKER_GLOBAL_IP_WHITELIST_Update 
		@Id int,
		@BrokerId UniqueIdentifier,
		@IPAddress varchar(50),
		@Description varchar(200),
		@UserId UniqueIdentifier
AS
BEGIN
    
		UPDATE BROKER_GLOBAL_IP_WHITELIST
		   SET IPAddress = @IPAddress,
		       Description = @Description,
			   LastModifiedDate = GETDATE(),
			   LastModifiedUserId = @UserId
		WHERE Id = @Id AND BrokerId = @BrokerId
END
