CREATE PROCEDURE [dbo].[Billing_ComplianceEagle]
	@StartDate datetime,
	@EndDate datetime
AS
BEGIN
-- dd 2018-11-02 - For use in automate billing executable.

SELECT BrokerNm As LenderName, CustomerCode, lfc.sLNm AS LoanNumber, lfc.sLId As LoanId, ptb.BillingDate AS BillingDate, lfc.aBNm AS BorrowerName
                                                FROM BILLING_PER_TRANSACTION ptb JOIN LOAN_FILE_CACHE lfc ON lfc.sLId = ptb.sLId
                                                                                 JOIN Loan_File_D lfd ON lfd.sLId = ptb.sLId
                                                                                 JOIN Broker ON Broker.BrokerId = lfc.sBrokerId
                                                WHERE BillingDate >= @StartDate AND BillingDate<@EndDate
                                                AND IsEnableComplianceEagleIntegration> 0
                                                AND BROKER.SuiteType = 1
                                                AND lfc.sLoanFileT = 0
                                                AND sComplianceEaseChecksum <> '' -- This is re-used for ComplianceEagle
                                                ORDER BY CustomerCode, LoanNumber
												
END