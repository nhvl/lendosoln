ALTER   PROCEDURE [dbo].[GetLoginFailureCount]
	@LoginNm varchar(36),
	@Type char(1),
	@BrokerPmlSiteId uniqueidentifier = '00000000-0000-0000-0000-000000000000'
AS
	SELECT LoginFailureCount
	FROM All_User AS a
	JOIN EMPLOYEE e ON a.UserId = e.EmployeeUserId
	JOIN BRANCH AS r ON r.BranchId = e.BranchId 
	JOIN BROKER AS b ON b.BrokerId = r.BrokerId 
	WHERE LoginNm = @LoginNm
		AND a.Type = @Type
		AND a.BrokerPmlSiteId = @BrokerPmlSiteId
		AND b.Status = 1