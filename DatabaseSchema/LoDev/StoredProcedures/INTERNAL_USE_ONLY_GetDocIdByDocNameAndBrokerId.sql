CREATE PROCEDURE [dbo].[INTERNAL_USE_ONLY_GetDocIdByDocNameAndBrokerId]
@BrokerId uniqueidentifier,
@DocTypeName varchar(50)
as

select docTypeId from edocs_document_type with (nolock)
where brokerid = @BrokerId
and
doctypename = @DocTypeName
