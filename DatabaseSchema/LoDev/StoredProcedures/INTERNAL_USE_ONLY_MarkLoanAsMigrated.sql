CREATE PROCEDURE INTERNAL_USE_ONLY_MarkLoanAsMigrated
	@sLId Guid
AS
	UPDATE Loan_File_Cache
	SET
		IsMarkedInvalidated = 0
	WHERE
		sLId = @sLId
