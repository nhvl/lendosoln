-- =============================================
-- Author:		Michael Leinweaver
-- Create date: 9/13/2017
-- Description:	Updates the latest status for an OCR request.
-- =============================================
CREATE PROCEDURE [dbo].[EDOCS_OCR_REQUEST_UpdateLatestStatus]
	@OcrRequestId uniqueidentifier,
	@BrokerId uniqueidentifier,
	@Status tinyint,
	@Date datetime
AS
BEGIN
	UPDATE TOP(1) EDOCS_OCR_REQUEST
	SET
		LatestStatus = @Status,
		LatestStatusDate = @Date
	WHERE OcrRequestId = @OcrRequestId AND BrokerId = @BrokerId
END
