
CREATE PROCEDURE [dbo].[INTERNAL_USE_ONLY_ListAllEDocsRoles]
AS
	SELECT 
	v.RoleDesc
	, v.RoleId
	, v.BrokerId
	, v.EmployeeId
FROM 
	View_Employee_Role v with (nolock)
WHERE 
	v.IsActive = '1' AND (v.RoleDesc = 'Underwriter' or v.RoleDesc = 'Processor' or v.RoleDesc = 'Shipper' )