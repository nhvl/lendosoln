CREATE PROCEDURE [dbo].[TASK_TRIGGER_ASSOCIATION_Create]
	@BrokerId uniqueidentifier,
	@LoanId uniqueidentifier,
	@TaskId varchar(10),
	@TriggerType int,
	@TriggerName varchar(200)
AS
BEGIN
	INSERT INTO TASK_TRIGGER_ASSOCIATION
	(
		BrokerId,
		LoanId,
		TaskId,
		TriggerType,
		TriggerName
	)
	VALUES
	(
		@BrokerId,
		@LoanId,
		@TaskId,
		@TriggerType,
		@TriggerName
	)
END