CREATE PROCEDURE UpdatePrintGroup
	@GroupID Guid,
	@BrokerID Guid,
	@GroupName varchar(200),
	@GroupDefinition text
AS
	UPDATE Print_Group
	       SET GroupName = @GroupName,
	               GroupDefinition = @GroupDefinition
             WHERE GroupID = @GroupID AND OwnerID = @BrokerID
