
CREATE PROCEDURE [dbo].[INTERNAL_USE_ONLY_ListActiveBrokers]
AS
	SELECT BrokerId, BrokerNm, CustomerCode
FROM Broker
WHERE Status = '1'
ORDER BY BrokerNm