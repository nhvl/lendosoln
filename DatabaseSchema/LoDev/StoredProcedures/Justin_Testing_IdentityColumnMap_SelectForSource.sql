-- =============================================
-- Author:		Scott Kibler
-- Description:	Gets all entries from DATA_IMPORT_IDENTITY_COLUMN_MAP pertaining to the supplied source.
-- =============================================
CREATE PROCEDURE [dbo].[Justin_Testing_IdentityColumnMap_SelectForSource]
	@StageSiteSourceName CHAR(100),
	@DatabaseSourceName CHAR(100)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT * 
	FROM 
			DATA_IMPORT_IDENTITY_COLUMN_MAP
	WHERE 
			StageSiteSourceName = @StageSiteSourceName
		AND DatabaseSourceName = @DatabaseSourceName
END
