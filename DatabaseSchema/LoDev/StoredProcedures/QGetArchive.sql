



CREATE PROCEDURE [dbo].[QGetArchive]
(  @QueueId int,
   @Size int = 25
)
AS
  
	SELECT TOP(@Size) MessageId, Priority, InsertionTime, RemovalTime, Subject1, Subject2, DataLoc FROM Q_ARCHIVE WHERE @QueueId = QueueId ORDER BY MessageId DESC;



