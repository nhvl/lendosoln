-- =============================================
-- Author:		Antonio Valencia
-- Create date: 8/7/2012
-- Description:	Deletes a CC template from a draft
-- =============================================
CREATE PROCEDURE dbo.CC_TEMPLATE_DeleteFromDraft 
	-- Add the parameters for the stored procedure here
	@BrokerId uniqueidentifier, 
	@TemplateId  uniqueidentifier
	
AS
BEGIN
	delete from CC_TEMPLATE  
	FROM CC_TEMPLATE t
	join cc_template_system as c on t.CCtemplatesystemid = c.id 
	where t.cCcTemplateId = @TemplateId AND c.ReleasedD is null  and CCtemplatesystemid IS NOT NULL
	AND t.BrokerId = @BrokerId
	SELECT @@Rowcount
END
