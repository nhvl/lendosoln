-- =============================================
-- Author:		Eric Mallare
-- Create date: 11/28/2017
-- Description:	Creates a Seamless LPA Polling job.
-- =============================================
ALTER PROCEDURE [dbo].[SEAMLESS_LPA_POLL_JOBS_Create]
	@JobId int,
	@PollAsyncId varchar(30),
	@PollingIntervalInSeconds int,
	@PollingAttemptCount int,
	@LoanId uniqueidentifier,
	@BrokerId uniqueidentifier,
	@UserId uniqueidentifier,
	@UserType char(1),
	@LpaUsername varchar(50),
	@LpaUserPassword varbinary(max),
	@LpaSellerNumber varchar(50),
	@LpaSellerPassword varbinary(max),
	@CreditReportType tinyint = null
AS
BEGIN
	INSERT INTO [dbo].[SEAMLESS_LPA_POLL_JOBS]
	(JobId, PollAsyncId, PollingAttemptCount, PollingIntervalInSeconds, LoanId, BrokerId, UserId, UserType, LpaUsername, LpaUserPassword, LpaSellerNumber, LpaSellerPassword, CreditReportType)
	VALUES
	(@JobId, @PollAsyncId, @PollingAttemptCount, @PollingIntervalInSeconds, @LoanId, @BrokerId, @UserId, @UserType, @LpaUsername, @LpaUserPassword, @LpaSellerNumber, @LpaSellerPassword, @CreditReportType)
END