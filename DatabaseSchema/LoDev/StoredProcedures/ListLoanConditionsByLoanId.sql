



CREATE PROCEDURE [dbo].[ListLoanConditionsByLoanId]
	@LoanId uniqueidentifier
	, @Deleted bit = 0
	, @Hidden bit = 0
	, @ShowAll bit = 1
AS
	SELECT LoanId
	, CondId
	, CondDesc
	, CondStatus
	, IsLoanStillValid
	, CondCategoryDesc
	, CondStatusDate
	, CondOrderedPosition
	, CreatedD
	, CondIsHidden
	, CondIsValid
	, CondCompletedByUserNm
	, CondNotes
	, CondDeleteD
	, CondDeleteByUserNm
	FROM Condition
	WHERE
		LoanId = @LoanID
	AND	( ( ( CondIsHidden = 0 OR ( CondIsHidden = 1 AND @Hidden = 1 ) )
		AND
		( CondIsValid != @Deleted ) )
		OR @ShowAll = 1 )

	ORDER BY CondOrderedPosition, CondCategoryDesc
	IF( 0!=@@error)
	BEGIN
		RAISERROR('Error in the selection statement in ListLoanConditionsByLoanId sp', 16, 1);
		RETURN -100;
	END
	RETURN 0;

