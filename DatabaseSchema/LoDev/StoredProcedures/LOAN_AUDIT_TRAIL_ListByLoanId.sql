-- =============================================
-- Author:		Thien Nguyen
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[LOAN_AUDIT_TRAIL_ListByLoanId] 
	@LoanId Guid,
    @BrokerId Guid  -- for security reason

AS
BEGIN
    SELECT  Id, EventDate, Category, ClassType, EventDescription, UserName, HasDetails, ReadPermissionsRequiredJsonContent,DetailContentKey
    FROM LOAN_AUDIT_TRAIL
    WHERE LoanId = @LoanId AND BrokerId = @BrokerId
    ORDER BY EventDate ASC
END    
