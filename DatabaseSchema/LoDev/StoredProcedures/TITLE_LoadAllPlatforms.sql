-- =============================================
-- Author:		Eric Mallare
-- Create date: 8/16/2017
-- Description:	List all title platforms.
-- =============================================
ALTER PROCEDURE [dbo].[TITLE_LoadAllPlatforms]
AS
BEGIN
	SELECT 
		t.TransmissionAssociationT,
		t.TargetUrl,
		t.TransmissionAuthenticationT,
		t.EncryptionKeyId,
		t.ResponseCertId,
		t.RequestCredentialName,
		t.RequestCredentialPassword,
		t.ResponseCredentialName,
		t.ResponseCredentialPassword,
		p.PlatformId,
		p.PlatformName,
		p.TransmissionModelT,
		p.PayloadFormatT,
		p.UsesAccountid,
		p.RequiresAccountId,
		p.TransmissionInfoId as TransmissionId
	FROM
		TITLE_FRAMEWORK_PLATFORM p JOIN
		TITLE_FRAMEWORK_TRANSMISSION_INFO t ON p.TransmissionInfoId=t.TransmissionInfoId
END
