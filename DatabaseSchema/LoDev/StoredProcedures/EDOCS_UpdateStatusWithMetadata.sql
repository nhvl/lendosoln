-- =============================================
-- Author:		David Dao
-- Create date: 6/25/2015
-- Description:	Updated the Original and Current NonDestructive metadata.
-- =============================================
ALTER PROCEDURE [dbo].[EDOCS_UpdateStatusWithMetadata] 
	@BrokerId UniqueIdentifier,
	@DocId uniqueidentifier, 
	@ImageStatus tinyint,
	@MetadataJsonContent varchar(max),
	@OriginalMetadataJsonContent varchar(max),
	@MetadataProtobufContent varbinary(max) = NULL,
	@OriginalMetadataProtobufContent varbinary(max)	= NULL
AS
BEGIN
	UPDATE EDOCS_DOCUMENT
	SET 
		ImageStatus  = @ImageStatus,
		MetadataJsonContent  = @MetadataJsonContent,
		OriginalMetadataJsonContent = @OriginalMetadataJsonContent,
		TimeToRequeueForImageGeneration  = NULL,
		MetadataProtobufContent = @MetadataProtobufContent,
		OriginalMetadataProtobufContent = @OriginalMetadataProtobufContent
	WHERE
		DocumentId = @DocId AND BrokerId=@BrokerId
		
END


