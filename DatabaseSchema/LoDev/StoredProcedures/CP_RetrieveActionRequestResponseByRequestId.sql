CREATE PROCEDURE [dbo].[CP_RetrieveActionRequestResponseByRequestId]
	@ConsumerActionRequestId bigint
AS

SELECT 

--REQUEST
sLId
, aAppId
, aBFullName
, aCFullName
, ResponseStatus
, req.DocTypeId
, Description
, PdfFileDbKey
, PdfMetaDataSnapshot
, IsApplicableToBorrower
, IsApplicableToCoborrower
, IsSignatureRequested
, IsESignAllowed
, aBInitials
, aCInitials
, CreatorEmployeeId
, CreateOnD
, sTitleBorrowerId

-- RESPONSE
, ConsumerCompletedD
, ReceivingFaxNumber
, BorrowerSignedEventId
, CoborrowerSignedEventId


--DOC TYPE
, DocTypeName
, FolderName

FROM
CP_Consumer_Action_Request req
LEFT JOIN CP_Consumer_Action_Response res on req.ConsumerActionRequestId = res.ConsumerActionRequestId
JOIN Edocs_Document_Type dt on dt.DocTypeId = req.DocTypeId
JOIN Edocs_Folder folder on dt.FolderId = folder.FolderId


WHERE req.ConsumerActionRequestId = @ConsumerActionRequestId
