-- =============================================
-- Author:		Justin Lara
-- Create date: 12/7/2016
-- Description:	Retrieve all CRA transactions
--				between the given DateTimes.
-- =============================================
ALTER PROCEDURE [dbo].[ListCraTransactionsInTimeFrame]
	@StartTime DateTime,
	@EndTime DateTime
AS
BEGIN
	SELECT DISTINCT sf.timeversion, sf.externalfileid, br.BrokerNm, loan.aBNm, sf.ComId, sf.FileType, sf.BrandedProductName
	FROM service_file sf JOIN Application_A app ON sf.owner = app.aAppId
		JOIN Loan_File_Cache loan ON app.sLId = loan.sLId
		JOIN Broker br ON loan.sBrokerId = br.BrokerId
	WHERE sf.IsInEffect = 1 AND sf.HowDidItGetHere = 'OrderFromCRA' AND sf.FileType <> 'BillingReport'
		AND TimeVersion >= @StartTime AND TimeVersion < @EndTime
END
