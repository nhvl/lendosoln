-- ==========================================================================
-- Author: Scott Kibler
-- Create date: 6/30/2017
-- Description: 
--  Gets the convolog permission levels for the broker.
-- ==========================================================================
CREATE PROCEDURE [dbo].[GetConvoLogPermissionLevelsForBroker]
	@BrokerId uniqueidentifier
AS
BEGIN
	SELECT ID, IsActive, Name, Description
	FROM
		CONVOLOG_PERMISSION_LEVEL
	WHERE BrokerId = @BrokerId
END