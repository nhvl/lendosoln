CREATE PROCEDURE RetrieveListOfOldAndNewAppId 
	@srcLoanID Guid,
	@destLoanID Guid
AS
SELECT o.aAppId AS src_aAppId,  n.aAppId AS dest_aAppId
FROM 
(select aAppId, PrimaryRankIndex
FROM Application_A WHERE sLId = @srcLoanID) o
JOIN
(select aAppId, PrimaryRankIndex
FROM Application_A WHERE sLId = @destLoanID) n
ON o.PrimaryRankIndex = n.PrimaryRankIndex
