-- =============================================
-- Author:		Eric Mallare
-- Create date: 8/17/2017
-- Description:	Creates a transmission
-- =============================================
ALTER PROCEDURE [dbo].[TITLE_CreateTransmission]
	@TargetUrl varchar(200),
	@TransmissionAuthenticationT int,
	@TransmissionAssociationT int,
	@EncryptionKeyId uniqueidentifier,
	@ResponseCertId uniqueidentifier = null,
	@RequestCredentialName varchar(50) = null,
	@RequestCredentialPassword varbinary(max) = null,
	@ResponseCredentialName varchar(50) = null,
	@ResponseCredentialPassword varbinary(max) = null,
	@TransmissionInfoId int OUTPUT
AS
BEGIN
	INSERT INTO
		TITLE_FRAMEWORK_TRANSMISSION_INFO
		(TransmissionAssociationT, TargetUrl, TransmissionAuthenticationT, EncryptionKeyId, ResponseCertId,
		 RequestCredentialName, RequestCredentialPassword, ResponseCredentialName, ResponseCredentialPassword)
	VALUES
		(@TransmissionAssociationT, @TargetUrl, @TransmissionAuthenticationT, @EncryptionKeyId, @ResponseCertId, 
		 @RequestCredentialName, @RequestCredentialPassword, @ResponseCredentialName, @ResponseCredentialPassword)
		 
	SET @TransmissionInfoId=SCOPE_IDENTITY();
END
