CREATE procedure [dbo].[EDOCS_CopyShippingTemplateCheckName]
	@ShippingTemplateId int,
	@ShippingTemplateName varchar(50),
	@BrokerId uniqueidentifier
as

select 1
	from EDOCS_SHIPPING_TEMPLATE with (nolock)
	where ShippingTemplateName = @ShippingTemplateName
		and BrokerId = @BrokerId