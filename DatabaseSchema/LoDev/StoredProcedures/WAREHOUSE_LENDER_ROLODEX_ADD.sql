-- =============================================
-- Author:		paoloa
-- Create date: 7/12/2013
-- Description:	
-- =============================================
ALTER PROCEDURE [dbo].[WAREHOUSE_LENDER_ROLODEX_ADD]
	@BrokerId uniqueidentifier, 
	@WarehouseLenderName varchar(100), 
	@Status int, 
	@FannieMaePayeeId varchar(50), 
	@FreddieMacPayeeId varchar(50),
	@WarehouseLenderFannieMaeId varchar(50) = null,
	@WarehouseLenderFreddieMacId varchar(50) = null,
	@MersOrganizationId varchar(7), 
	@PayToBankName varchar(50), 
	@PayToCity varchar(36), 
	@PayToState varchar(2), 
	@PayToABANum varchar(9), 
	@PayToAccountName varchar(50), 
	@PayToAccountNum varchar(50), 
	@FurtherCreditToAccountName varchar(50), 
	@FurtherCreditToAccountNum varchar(50), 
	@MainCompanyName  varchar(400), 
	@MainContactName varchar(150), 
	@MainAttention varchar(400), 
	@MainEmail varchar(100), 
	@MainAddress varchar(60), 
	@MainCity varchar(36), 
	@MainState varchar(2), 
	@MainZip varchar(10), 
	@MainPhone varchar(21), 
	@MainFax varchar(21), 
	@CollateralPackageToAttention varchar(400), 
	@CollateralPackageToContactName varchar(150), 
	@CollateralPackageToUseMainAddress bit, 
	@CollateralPackageToEmail varchar(100), 
	@CollateralPackageToAddress varchar(60), 
	@CollateralPackageToCity varchar(36), 
	@CollateralPackageToState varchar(2), 
	@CollateralPackageToZip varchar(10), 
	@CollateralPackageToPhone varchar(21), 
	@CollateralPackageToFax varchar(21), 
	@ClosingPackageToAttention varchar(400), 
	@ClosingPackageToContactName varchar(150), 
	@ClosingPackageToUseMainAddress bit, 
	@ClosingPackageToEmail varchar(100), 
	@ClosingPackageToAddress varchar(60), 
	@ClosingPackageToCity varchar(36), 
	@ClosingPackageToState varchar(2), 
	@ClosingPackageToZip varchar(10), 
	@ClosingPackageToPhone varchar(21), 
	@ClosingPackageToFax varchar(21),
	@AdvanceClassificationsXmlContent varchar(max)
AS
BEGIN
	INSERT INTO WAREHOUSE_LENDER_ROLODEX(BrokerId, WarehouseLenderName, [Status], FannieMaePayeeId, FreddieMacPayeeId, MersOrganizationId, PayToBankName, PayToCity, PayToState, PayToABANum, PayToAccountName, PayToAccountNum, FurtherCreditToAccountName, FurtherCreditToAccountNum, MainCompanyName, MainContactName, MainAttention, MainEmail, MainAddress, MainCity, MainState, MainZip, MainPhone, MainFax, CollateralPackageToAttention, CollateralPackageToContactName, CollateralPackageToUseMainAddress, CollateralPackageToEmail, CollateralPackageToAddress, CollateralPackageToCity, CollateralPackageToState, CollateralPackageToZip, CollateralPackageToPhone, CollateralPackageToFax, ClosingPackageToAttention, ClosingPackageToContactName, ClosingPackageToUseMainAddress, ClosingPackageToEmail, ClosingPackageToAddress, ClosingPackageToCity, ClosingPackageToState, ClosingPackageToZip, ClosingPackageToPhone, ClosingPackageToFax, AdvanceClassificationsXmlContent, WarehouseLenderFannieMaeId, WarehouseLenderFreddieMacId)
	VALUES (@BrokerId, @WarehouseLenderName, @Status, @FannieMaePayeeId, @FreddieMacPayeeId, @MersOrganizationId, @PayToBankName, @PayToCity, @PayToState, @PayToABANum, @PayToAccountName, @PayToAccountNum, @FurtherCreditToAccountName, @FurtherCreditToAccountNum, @MainCompanyName, @MainContactName, @MainAttention, @MainEmail, @MainAddress, @MainCity, @MainState, @MainZip, @MainPhone, @MainFax, @CollateralPackageToAttention, @CollateralPackageToContactName, @CollateralPackageToUseMainAddress, @CollateralPackageToEmail, @CollateralPackageToAddress, @CollateralPackageToCity, @CollateralPackageToState, @CollateralPackageToZip, @CollateralPackageToPhone, @CollateralPackageToFax, @ClosingPackageToAttention, @ClosingPackageToContactName, @ClosingPackageToUseMainAddress, @ClosingPackageToEmail, @ClosingPackageToAddress, @ClosingPackageToCity, @ClosingPackageToState, @ClosingPackageToZip, @ClosingPackageToPhone, @ClosingPackageToFax, @AdvanceClassificationsXmlContent, @WarehouseLenderFannieMaeId, @WarehouseLenderFreddieMacId )
	SELECT CAST( SCOPE_IDENTITY() as int)
END