CREATE PROCEDURE [dbo].[CP_RetrieveConsumerPasswordTemporaryByEmailBroker]
	@Email varchar(80),
	@BrokerId uniqueidentifier
AS

SELECT IsTemporaryPassword
FROM CP_CONSUMER
WHERE	BrokerId = @BrokerId	AND
		Email = @Email