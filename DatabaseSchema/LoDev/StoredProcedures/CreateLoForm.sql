-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE CreateLoForm 
	@FormId UniqueIdentifier,
	@BrokerId UniqueIdentifier,
	@Description varchar(100),
	@IsSignable bit,
	@IsStandardForm bit,
	@FileName varchar(50),
	@FieldMetaData varchar(max) = ''
AS
BEGIN
	INSERT INTO LO_FORM (FormId, Description, IsSignable, FieldMetaData, IsStandardForm, BrokerId, FileName, PaperSize)
	VALUES (@FormId, @Description, @IsSignable, @FieldMetaData, @IsStandardForm, @BrokerId, @FileName, 0)
END
