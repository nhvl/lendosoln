CREATE PROCEDURE [dbo].[UpdateMembersGroup]
	@BrokerId uniqueidentifier, 
	@MemberId uniqueidentifier,
	@GroupType int,
	@CommaSeparatedGroupIds varchar(4000),
	@IsAddOnly bit = 0
AS
BEGIN
	SET NOCOUNT ON
	BEGIN TRANSACTION
		DECLARE @GroupId varchar(100), @Pos int
		DECLARE @MembershipTableName nvarchar(100)
		
		IF @IsAddOnly=0
			BEGIN
				IF @GroupType=1 
					DELETE FROM GROUP_x_EMPLOYEE  WHERE EmployeeId = @MemberId		
				ELSE IF @GroupType=2
					DELETE FROM GROUP_x_BRANCH  WHERE BranchId = @MemberId		
				ELSE
					DELETE FROM GROUP_x_PML_BROKER  WHERE PmlBrokerId = @MemberId
			END
		
		IF( 0!=@@error) GOTO ErrorHandler

		SET @CommaSeparatedGroupIds = LTRIM(RTRIM(@CommaSeparatedGroupIds))+ ','
		SET @Pos = CHARINDEX(',', @CommaSeparatedGroupIds, 1)
		IF REPLACE(@CommaSeparatedGroupIds, ',', '') <> ''
		BEGIN
			WHILE @Pos > 0
			BEGIN
				SET @GroupId = LTRIM(RTRIM(LEFT(@CommaSeparatedGroupIds, @Pos - 1)))
				IF @GroupId <> ''
				BEGIN						
						IF @groupType=1 
							INSERT INTO GROUP_x_EMPLOYEE VALUES (@GroupId ,@MemberId)							
						ELSE IF @groupType=2
							INSERT INTO GROUP_x_BRANCH VALUES (@GroupId ,@MemberId)							
						ELSE
							INSERT INTO GROUP_x_PML_BROKER  VALUES (@GroupId ,@MemberId)
					
						IF( 0!=@@error) GOTO ErrorHandler
				END
				SET @CommaSeparatedGroupIds = RIGHT(@CommaSeparatedGroupIds, LEN(@CommaSeparatedGroupIds) - @Pos)
				SET @Pos = CHARINDEX(',', @CommaSeparatedGroupIds, 1)
			END
		END	
	COMMIT TRANSACTION
	
	ErrorHandler:
		if( 0!=@@error)
		BEGIN
			ROLLBACK TRANSACTION
			RAISERROR('Error in UpdateMembersGroup sp', 16, 1);
			RETURN -100;
		END		
END


