-- =============================================
-- Author:		Jhairo Erazo
-- Create date: 01/08/14
-- Description:	Get the trade counter from broker table and increase counter
-- =============================================
CREATE PROCEDURE [dbo].[MBS_GetTradeCounter]
	@BrokerId Guid,
    @TradeCounter BigInt = NULL OUT
AS

DECLARE @IsAutoGenerateTradeNums bit

SELECT @TradeCounter = TradeCounter, @IsAutoGenerateTradeNums = IsAutoGenerateTradeNums 
FROM Broker WHERE BrokerId = @BrokerId

IF @IsAutoGenerateTradeNums = 1
BEGIN
	--increment counter until we ge to an unsued trade number
	WHILE (1=1)
	BEGIN
		SET @TradeCounter = @TradeCounter + 1
	
		IF (SELECT COUNT(*) FROM MBS_TRADE WHERE BrokerId = @BrokerId AND TradeNum = @TradeCounter) = 0
			BREAK
	END
	
	-- Update Counter to broker table
	UPDATE Broker WITH ( ROWLOCK ) SET TradeCounter = @TradeCounter
	WHERE BrokerId = @BrokerId
END
ELSE
BEGIN
	SET @TradeCounter = NULL
END