-- =============================================
-- Author:		David Dao
-- Create date: 11/04/2009
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UpdateDocMagicSavedAuthentication]
	@UserId UniqueIdentifier,
	@DocMagicCustomerId varchar(50),
	@DocMagicUserName varchar(50),
	@DocMagicPassword varchar(50)
AS
BEGIN
	UPDATE DOCUMENT_VENDOR_USER_ENABLED_CREDENTIALS
	SET AccountId = @DocMagicCustomerId,
	    [Login] = @DocMagicUserName,
		[Password] = @DocMagicPassword
	WHERE UserId = @UserId AND VendorId = '00000000-0000-0000-0000-000000000000'
	
	IF @@ROWCOUNT = 0
	BEGIN		
		DECLARE @BrokerId uniqueidentifier;
		
		SELECT @BrokerId = BrokerId
		FROM VIEW_ACTIVE_LO_USER_WITH_BROKER_AND_LICENSE_INFO
		WHERE UserId = @UserId;
		
		INSERT INTO DOCUMENT_VENDOR_USER_ENABLED_CREDENTIALS(UserId, VendorId, BrokerId, [Login], [Password], AccountId)
		VALUES (@UserId, '00000000-0000-0000-0000-000000000000', @BrokerId, @DocMagicUserName, @DocMagicPassword, @DocMagicCustomerId)
	END
END
