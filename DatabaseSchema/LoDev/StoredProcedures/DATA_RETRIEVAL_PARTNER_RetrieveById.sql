-- =============================================
-- Author:		Brian Beery
-- Create date: 09/20/2013
-- Description:	Get a data retrieval framework partner by ID
-- =============================================
CREATE PROCEDURE [dbo].[DATA_RETRIEVAL_PARTNER_RetrieveById] 
	@PartnerId UniqueIdentifier
AS
BEGIN

	SELECT PartnerId, Name, ExportPath
	FROM DATA_RETRIEVAL_PARTNER
	WHERE PartnerId = @PartnerId
	
END