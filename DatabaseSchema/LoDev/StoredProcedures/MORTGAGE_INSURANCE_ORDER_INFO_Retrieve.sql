-- =============================================
-- Author:		Brian Beery
-- Create date: 09/19/2014
-- Description:	Retrieves an MI framework transaction from the MORTGAGE_INSURANCE_ORDER_INFO table.
-- =============================================
CREATE PROCEDURE [dbo].[MORTGAGE_INSURANCE_ORDER_INFO_Retrieve]
	-- Add the parameters for the stored procedure here
	@sLId uniqueidentifier,
	@OrderNumber varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT *
	FROM MORTGAGE_INSURANCE_ORDER_INFO
	WHERE sLId = @sLId 
	AND OrderNumber = @OrderNumber
END
