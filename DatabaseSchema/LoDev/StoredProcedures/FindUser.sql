ALTER  PROCEDURE [dbo].[FindUser]
    @UserFirstNm Varchar( 32 ) = NULL , 
    @UserLastNm Varchar( 32 ) = NULL , 
    @UserNm Varchar( 32 ) = NULL , 
    @LoginNm Varchar( 32 ) = NULL , 
    @Type Varchar( 1 ) = NULL , 
    @Email Varchar( 80 ) = NULL , 
    @BrokerNm Varchar( 128 ) = NULL , 
    @IsActive Bit = NULL,
    @CanLogin bit = NULL,
    @BrokerStatus int = NULL,
    @UserID uniqueidentifier = NULL,
    @EmployeeID uniqueidentifier = NULL
AS

   SELECT TOP 100
        e.EmployeeId ,
        e.UserFirstNm + ' ' + e.UserLastNm AS FullName ,
        e.Email ,
        a.LoginNm ,
        a.Type ,
        r.BranchNm ,
        br.BrokerNm ,
        e.BrokerId ,
        a.UserId ,
        e.IsActive ,
        a.RecentLoginD,
        a.IsActive as CanLogin,
        a.BrokerPmlSiteId,
		br.EnableRetailTpoPortalMode
    FROM
        View_lendersoffice_Employee AS e  WITH (NOLOCK) 
		JOIN Branch AS r WITH (NOLOCK) ON r.BranchId = e.BranchId
		JOIN Broker as br with (nolock) on r.brokerid = br.brokerid
		JOIN broker_user as bu with (nolock) on e.employeeid = bu.employeeid
		JOIN all_user as a with (nolock) on a.userid = bu.userid
	
	WHERE
		((@UserFirstNm IS NULL) OR (e.UserFirstNm LIKE '' + @UserFirstNm + '%'))
	AND
		((@UserLastNm IS NULL)	OR (e.UserLastNm LIKE '' + @UserLastNm + '%'))
	AND
		((@UserNm IS NULL )		OR (   (e.UserFirstNm LIKE '' + @UserNm + '%') 
									OR (e.UserLastNm  LIKE '' + @UserNm + '%')))
	AND
		((@LoginNm IS NULL)		OR (a.LoginNm LIKE '' + @LoginNm + '%'))
	AND
		((@Email IS NULL)		OR (e.Email LIKE '' + @Email + '%'))
	AND
		((@BrokerNm IS NULL)	OR (br.BrokerNm LIKE '' + @BrokerNm + '%'))
	AND
		((@TYPE IS NULL)		OR (a.TYPE = @TYPE))
	AND
		((@IsActive IS NULL)	OR (e.IsActive = @IsActive))
	AND
		((@CanLogin IS NULL)	OR (a.IsActive = @CanLogin))
	AND
		((@BrokerStatus IS NULL)OR (br.Status = @BrokerStatus))
	AND
		((@UserID IS NULL)		OR (a.UserId = @UserID))
	AND
		((@EmployeeID IS NULL)	OR (e.EmployeeId = @EmployeeID))
