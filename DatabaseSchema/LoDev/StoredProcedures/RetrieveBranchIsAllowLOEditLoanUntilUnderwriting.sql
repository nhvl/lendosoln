
CREATE PROCEDURE [dbo].[RetrieveBranchIsAllowLOEditLoanUntilUnderwriting]
	@BranchID uniqueidentifier = NULL
AS
	SELECT IsAllowLOEditLoanUntilUnderwriting
FROM
	Branch
WHERE
	BranchID = @BranchID AND IsAllowLOEditLoanUntilUnderwriting = 1

