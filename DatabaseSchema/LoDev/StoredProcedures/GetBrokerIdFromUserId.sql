-- =============================================
-- Author:		Antonio Valencia
-- Create date: July 20, 2010
-- Description:	Get Broker Id From USer Id returns whether user is active and their employee id 
-- =============================================
CREATE PROCEDURE GetBrokerIdFromUserId 
	-- Add the parameters for the stored procedure here
	@UserId uniqueidentifier 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	select BrokerId,EmployeeId, 1 as IsActive from View_active_Lo_user where UserId=@UserId
	union 
	select BrokerId, EmployeeId, 0 as IsActive from VIEW_DISABLED_LO_USER where  UserId=@UserId

END
