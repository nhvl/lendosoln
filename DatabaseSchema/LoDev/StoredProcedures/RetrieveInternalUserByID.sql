

CREATE PROCEDURE [dbo].[RetrieveInternalUserByID]
	@UserID uniqueidentifier 
AS
	SELECT a.LoginNm, a.IsActive, a.PasswordHash, a.PasswordExpirationD, i.FirstNm, i.LastNm, i.Permissions, a.IsUpdatedPassword, a.PasswordSalt
	FROM all_user a, internal_user i 
	WHERE a.UserID = @UserID 
	      AND a.UserID = i.UserID
	if( 0!=@@error)
	begin
		RAISERROR('Error in the select statement in RetrieveInternalUserByID sp', 16, 1);
		return -100;
	end
	return 0;


