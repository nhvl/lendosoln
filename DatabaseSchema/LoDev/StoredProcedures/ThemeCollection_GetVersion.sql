-- =============================================
-- Author:		Michael Leinweaver
-- Create date: 6/21/2017
-- Description:	Gets the version for a theme collection.
-- =============================================
CREATE PROCEDURE [dbo].[ThemeCollection_GetVersion]
	@BrokerId uniqueidentifier,
	@ThemeCollectionType int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT Version
	FROM BROKER_THEME_COLLECTION
	WHERE BrokerId = @BrokerId AND ThemeCollectionType = @ThemeCollectionType
END
