﻿-- =============================================
-- Author:		Geoff Feltman
-- Create date: 1/30/17
-- Description:	Retrieve multiple tasks by id in a single query.
-- =============================================
ALTER PROCEDURE [dbo].[TASK_ListTasksByBrokerIdLoanIdTaskId] 
	@BrokerId uniqueidentifier,
	@LoanId uniqueidentifier,
	@IdXml xml
AS
BEGIN

SELECT
	BrokerId
	, TaskId
	, TaskAssignedUserId
	, TaskStatus
	, LoanId
	, TaskOwnerUserId
	, TaskCreatedDate
	, TaskSubject
	, TaskDueDate
	, TaskFollowUpDate
	, CondInternalNotes
	, TaskIsCondition
	, TaskResolvedUserId
	, TaskLastModifiedDate
	, BorrowerNmCached
	, LoanNumCached
	, TaskToBeAssignedRoleId
	, TaskToBeOwnerRoleId
	, TaskCreatedByUserId
	, TaskDueDateLocked
	, TaskDueDateCalcField
	, TaskDueDateCalcDays
	, TaskHistoryXml
	, TaskPermissionLevelId
	, TaskClosedUserId
	, TaskClosedDate
	, TaskRowVersion
	, CondCategoryId
	, CondIsHidden
	, CondRowId
	, CondDeletedDate
	, CondDeletedByUserNm
	, CondIsDeleted
	, TaskAssignedUserFullName
	, TaskOwnerFullName
	, TaskClosedUserFullName
	, CondMessageId
	, CondRequiredDocTypeId
	, ResolutionBlockTriggerName
	, ResolutionDenialMessage
	, ResolutionDateSetterFieldId
	, AssignToOnReactivationUserId

FROM @IdXml.nodes('root/id') as T(Item)
	JOIN Task task on T.Item.value('.', 'varchar(10)') = task.TaskId
WHERE
	BrokerId = @BrokerId
	AND LoanId = @LoanId

END
