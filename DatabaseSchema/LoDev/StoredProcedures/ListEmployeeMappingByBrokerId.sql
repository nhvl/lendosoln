
CREATE  PROCEDURE [dbo].[ListEmployeeMappingByBrokerId]
	@BrokerId uniqueidentifier
AS
	SELECT
		v.EmployeeId ,
		a.IsActive as CanLogin,
		a.IsActive ,
		a.Type ,
		a.UserId
	FROM
		View_lendersoffice_Employee AS v WITH (NOLOCK)
left join broker_user as b on v.employeeid = b.employeeid
		left join all_user as a on a.userid = b.userid
		
	WHERE
		v.BrokerId = @BrokerId