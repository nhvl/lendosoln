CREATE PROCEDURE [dbo].[INTERNAL_USE_MoveLoan]
	@sLId uniqueidentifier,
	@branchIdDes uniqueidentifier
as
declare @brokerIdSrc uniqueidentifier
declare @branchIdSrc uniqueidentifier
declare @brokerIdDes uniqueidentifier
select @brokerIdSrc = sBrokerId, @branchIdSrc = sBranchId
from loan_file_cache
where sLId = @sLId
select @brokerIdDes = brokerId
from branch
where branchId = @branchIdDes
update loan_file_f
set sBranchId = @branchIdDes
where slid = @slid
update loan_file_e
set sBrokerId = @brokerIdDes
where slid = @slid
update loan_file_cache
set sBranchId = @branchIdDes, sBrokerId = @brokerIdDes
where slid = @slid
delete from loan_user_assignment
where sLId = @sLId
update loan_link
set sBrokerId = @brokerIdDes
where ( sLId1 = @sLId or sLId2 = @sLId ) and sBrokerId = @brokerIdSrc 
delete from track
where DiscLogId in
( 
select DiscLogId from discussion_log where DiscRefObjId = @sLId and discBrokerId = @brokerIdSrc
)
delete from discussion_notification
where discLogId in 
( 
select DiscLogId from discussion_log where DiscRefObjId = @sLId and discBrokerId = @brokerIdSrc
)
