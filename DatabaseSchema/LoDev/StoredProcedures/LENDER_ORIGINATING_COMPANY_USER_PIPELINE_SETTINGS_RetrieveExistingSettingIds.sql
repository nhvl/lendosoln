-- =============================================
-- Author:		Michael Leinweaver
-- Create date: 6/12/2018
-- Description:	Retrieves the IDs of existing settings for a lender.
-- =============================================
CREATE PROCEDURE [dbo].[LENDER_ORIGINATING_COMPANY_USER_PIPELINE_SETTINGS_RetrieveExistingSettingIds]
	@BrokerId uniqueidentifier
AS
BEGIN
	SELECT SettingId
	FROM LENDER_ORIGINATING_COMPANY_USER_PIPELINE_SETTINGS
	WHERE BrokerId = @BrokerId
END
