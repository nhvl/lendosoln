
-- =============================================
-- Author:		Antonio Valencia
-- Create date: Aug 22 2011
-- Description:	Inserts a new record into the doc magic pdf status table
-- =============================================
CREATE PROCEDURE [dbo].[EDOCS_AddDocMagicPDFStatus] 
	@Id uniqueidentifier, 
	@sLId uniqueidentifier, 
	@Data varchar(max)
	
AS
BEGIN
	INSERT INTO EDOCS_DOCMAGIC_PDF_STATUS (Id, CurrentStatus, LoanId, Data) VALUES( @Id, 0, @sLId, @Data)  
END

