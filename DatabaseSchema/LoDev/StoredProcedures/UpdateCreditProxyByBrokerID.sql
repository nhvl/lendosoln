ALTER PROCEDURE [dbo].[UpdateCreditProxyByBrokerID] 
	@BrokerID uniqueidentifier,
	@CrAccProxyId uniqueidentifier,
	@CrAccProxyDesc varchar(100),
	@CraUserName varchar(100),
	@CraPassword varchar(100) = NULL,
	@EncryptedCraPassword varbinary(120) = 0x,
	@CraAccId varchar(100),
	@CraId uniqueidentifier,
	@IsExperianPulled bit,
	@IsEquifaxPulled bit,
	@IsTransunionPulled bit,
	@IsFannieMaePulled bit,
	@CreditMornetPlusUserId varchar(100),
	@CreditMornetPlusPassword varchar(50) = NULL,
	@EncryptedCreditMornetPlusPassword varbinary(100) = 0x,
	@AllowPmlUserToViewReports bit = 0,
	@EncryptionKeyId uniqueidentifier = NULL -- This parameter is intentionally optional, and should only be used when inserting
AS
IF @CrAccProxyId IS NULL
BEGIN
	INSERT INTO Credit_Report_Account_Proxy (
		BrokerId,
		CraUserName,
		CraPassword,
		EncryptedCraPassword,
		CraAccId,
		CraId,
		IsExperianPulled,
		IsEquifaxPulled,
		IsTransUnionPulled,
		CrAccProxyDesc,
		IsFannieMaePulled,
		CreditMornetPlusUserId,
		CreditMornetPlusPassword,
		EncryptedCreditMornetPlusPassword,
		AllowPmlUserToViewReports,
		EncryptionKeyId
	) VALUES (
		@BrokerID,
		@CraUserName,
		COALESCE(@CraPassword, ''),
		@EncryptedCraPassword,
		@CraAccId,
		@CraId,
		@IsExperianPulled,
		@IsEquifaxPulled,
		@IsTransUnionPulled,
		@CrAccProxyDesc,
		@IsFannieMaePulled,
		@CreditMornetPlusUserId,
		COALESCE(@CreditMornetPlusPassword, ''),
		@EncryptedCreditMornetPlusPassword,
		@AllowPmlUserToViewReports,
		@EncryptionKeyId
	);
END
ELSE
BEGIN
	UPDATE Credit_Report_Account_Proxy
	SET
		CraUserName = @CraUserName,
		CraPassword = COALESCE(@CraPassword, CraPassword),
		EncryptedCraPassword = @EncryptedCraPassword,
		CrAccProxyDesc = @CrAccProxyDesc,
		CraAccId = @CraAccId,
		CraId = @CraId,
		IsExperianPulled = @IsExperianPulled,
		IsEquifaxPulled = @IsEquifaxPulled,
		IsTransunionPulled = @IsTransunionPulled,
		IsFannieMaePulled = @IsFannieMaePulled,
		CreditMornetPlusUserId = @CreditMornetPlusUserId,
		CreditMornetPlusPassword = COALESCE(@CreditMornetPlusPassword, ''),
		EncryptedCreditMornetPlusPassword = @EncryptedCreditMornetPlusPassword,
		AllowPmlUserToViewReports = @AllowPmlUserToViewReports
	WHERE CrAccProxyId = @CrAccProxyId AND BrokerID = @BrokerID
END
