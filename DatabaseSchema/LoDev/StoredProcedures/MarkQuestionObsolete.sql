CREATE PROCEDURE MarkQuestionObsolete @QuestionId int, @IsObsolete bit AS
BEGIN	
	UPDATE SECURITY_QUESTION SET IsObsolete = @IsObsolete WHERE QuestionId = @QuestionId
	IF @@ROWCOUNT = 0 RETURN -1	
	RETURN 1
END
