﻿-- =============================================
-- Author:		Geoffrey Feltman
-- Create date: 3/2/17
-- Description:	Selects loan cache update requests
-- for retry.
-- =============================================
ALTER PROCEDURE [dbo].[GetCacheRequestsForRetry]
	@MaxRetries int,
	@TenMinutesAgo datetime
AS
BEGIN

select 
    u.RequestId,
    u.sLId,
    u.UserId,
    u.UserLoginNm,
    u.PageName,
    u.AffectedCachedFields,
    SUBSTRING(u.ErrorInfo, 1, 50) as ErrorInfo,
    u.FirstOccurD,
    u.LastRetryD,
    u.WhoRetriedLastLoginNm,
    u.NumRetries,
    c.sLNm,
    c.sBrokerId as BrokerId,
    b.CustomerCode,
    u.NumRetries
from loan_cache_update_request u with(nolock)
    join loan_file_cache c with(nolock) on u.slid = c.slid
    join broker b with(nolock) on c.sbrokerid = b.brokerid
where NumRetries < @MaxRetries
    and
    (
        u.LastRetryD is not null -- The update request has been attempted in the past.
        or
        u.FirstOccurD < @TenMinutesAgo -- The update request is over 10 minutes old.
    )
order by u.NumRetries, u.FirstOccurD desc

END
