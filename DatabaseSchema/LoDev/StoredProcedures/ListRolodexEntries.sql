ALTER PROCEDURE [dbo].[ListRolodexEntries]
	@FullName varchar(64) = NULL, 
	@CompanyName varchar(64) = NULL, 
	@AgentType varchar(16) = NULL, 
	@BrokerId uniqueidentifier = NULL,
	@RestrictToSettlementServiceProviders bit = NULL
AS
	IF( @FullName IS NOT NULL AND @FullName != '' )
	BEGIN
		SET @CompanyName = NULL
		SET @AgentType = NULL
	END
	ELSE
	BEGIN
		SET @FullName = NULL
	END
	SELECT AgentID,
		   BrokerId,
		   AgentType,
		   AgentTypeOtherDescription,
	       AgentNm,
	       AgentTitle,
	       AgentPhone,
	       AgentAltPhone,
	       AgentFax,
	       AgentEmail,
	       AgentComNm,
	       AgentAddr,
	       AgentCity,
	       AgentState,
	       AgentZip,
	       AgentCounty,
	       AgentN,
	       AgentDepartmentName,
	       AgentPager,
	       AgentLicenseNumber,
	       CompanyLicenseNumber,
	       PhoneOfCompany,
	       FaxOfCompany,
	       IsNotifyWhenLoanStatusChange,
	       AgentWebsiteUrl,
	       CommissionPointOfLoanAmount,
	       CommissionPointOfGrossProfit,
	       CommissionMinBase,
	       LicenseXmlContent,
	       CompanyId,
	       AgentBranchName,
	       AgentPayToBankName,
	       AgentPayToBankCityState,
	       AgentPayToABANumber,
	       AgentPayToAccountName,
	       AgentPayToAccountNumber,
	       AgentFurtherCreditToAccountName,
	       AgentFurtherCreditToAccountNumber,
	       AgentIsApproved,
	       AgentIsAffiliated,
	       OverrideLicenses,
	       SystemId,
	       IsSettlementServiceProvider
	FROM Agent
	WHERE
		BrokerId = COALESCE(@BrokerId, BrokerId)
		AND
		AgentNm = COALESCE(@FullName, AgentNm)
		AND
		AgentComNm = COALESCE(@CompanyName, AgentComNm)
		AND
		AgentType = COALESCE(@AgentType, AgentType)
		AND
		(
			COALESCE(@RestrictToSettlementServiceProviders, 0) = 0 OR
			IsSettlementServiceProvider = 1
		)
	ORDER BY
		AgentComNm, AgentNm
	if (@@error != 0)
	begin
		RAISERROR('Error in the select statement in ListRolodexEntries sp', 16, 1);
		return -100;
	end
	
	return 0;
