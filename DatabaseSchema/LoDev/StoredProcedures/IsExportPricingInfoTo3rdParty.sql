


CREATE   PROCEDURE [dbo].[IsExportPricingInfoTo3rdParty]

	@BrokerId uniqueidentifier
AS

	SELECT IsExportPricingInfoTo3rdParty
	FROM BROKER
	WHERE BrokerId = @BrokerId