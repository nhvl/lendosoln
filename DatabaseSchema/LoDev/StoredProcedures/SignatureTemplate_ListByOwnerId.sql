-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SignatureTemplate_ListByOwnerId]
	@BrokerId UniqueIdentifier,
	@OwnerUserId UniqueIdentifier
AS
BEGIN
	SELECT TemplateId, Name, FieldMetadata FROM LO_SIGNATURE_TEMPLATE
	WHERE BrokerId = @BrokerId AND OwnerUserId = @OwnerUserId
	ORDER BY NAME
END
