CREATE PROCEDURE [dbo].[DYNAMIC_CLOSING_COST_FEE_HISTORY_RetrieveAll]
	@BrokerId uniqueidentifier,
	@LoanId uniqueidentifier
AS
BEGIN
	SELECT FeeHistoryId, BrokerId, LoanId, [Description], ClosingCostFeeTypeId, HudLine
	FROM DYNAMIC_CLOSING_COST_FEE_HISTORY
	WHERE BrokerId = @BrokerId
	AND LoanId = @LoanId
END