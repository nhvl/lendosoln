-- =============================================
-- Author:		Matthew Pham
-- Create date: 8/24/11
-- Description:	Deletes PNG entries in EDOCS_PNG_ENTRY by DocId
-- =============================================
CREATE PROCEDURE [dbo].[EDOCS_DeletePNGEntriesByDocId] 
	-- Add the parameters for the stored procedure here
	@DocId uniqueidentifier 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM EDOCS_PNG_ENTRY
	WHERE
		documentid=@DocId
END
