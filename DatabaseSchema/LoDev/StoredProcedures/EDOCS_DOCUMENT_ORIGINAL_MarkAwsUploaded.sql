-- =============================================
-- Author:		David Dao
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE dbo.EDOCS_DOCUMENT_ORIGINAL_MarkAwsUploaded
    @BrokerId UniqueIdentifier,
    @DocumentId UniqueIdentifier
AS
BEGIN
	UPDATE EDOCS_DOCUMENT_ORIGINAL
		SET IsUploadedAws = 1
	WHERE BrokerId = @BrokerId AND DocumentId = @DocumentId

END
