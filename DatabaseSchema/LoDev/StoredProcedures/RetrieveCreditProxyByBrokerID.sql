ALTER PROCEDURE  [dbo].[RetrieveCreditProxyByBrokerID]
	@BrokerID uniqueidentifier
AS
BEGIN
	SELECT
		CrAccProxyId,
		CraUserName,
		CraPassword,
		EncryptedCraPassword,
		CraAccId,
		CraId,
		IsExperianPulled,
		IsEquifaxPulled,
		IsTransUnionPulled,
		CrAccProxyDesc,
		IsFannieMaePulled,
		CreditMornetPlusPassword,
		EncryptedCreditMornetPlusPassword,
		CreditMornetPlusUserId,
		AllowPmlUserToViewReports,
		EncryptionKeyId
	FROM CREDIT_REPORT_ACCOUNT_PROXY
	WHERE BrokerId = @BrokerID
		AND IsValid = 1
END
