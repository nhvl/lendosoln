-- =============================================
-- Author:		Antonio Valencia
-- Create date: 10/28/2013
-- Description:	Creates Document Share Request
-- =============================================
CREATE PROCEDURE [dbo].[CP_ShareDocument_Add]
	@sLId uniqueidentifier, 
	@aAppId uniqueidentifier,
	@Description varchar(100),
	@CreatorEmployeeId uniqueidentifier,
	@LinkedEDocId uniqueidentifier = null,
	@LinkedEDocVersion int = null,
	@PdfFileDbKey varchar(100), 
	@BrokerId uniqueidentifier
	
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @CreatorBrokerId uniqueidentifier
	DECLARE @LoanBrokerId uniqueidentifier
	DECLARE @EdocLoanId uniqueidentifier
	DECLARE @aBFullName varchar(100)
	DECLARE @aCFullName varchar(100)
	
	
	-- ensure user belongs to specified broker
	SELECT top(1) @CreatorBrokerId =  brokerid FROM employee e join branch br on e.branchid = br.branchid WHERE EmployeeId = @CreatorEmployeeId 
	
	IF (@CreatorBrokerId <> @BrokerId OR @CreatorBrokerId IS NULL) BEGIN
		RAISERROR('User does not belong to specified brokerid', 16, 1);
		RETURN -1
	END 
		print 'test'
	
	
	-- ensure loan belongs to specified broker
	SELECT top(1) @LoanBrokerId = sBrokerId from loan_file_cache where slid = @slid and sbrokerid = @brokerId
	
	IF (@LoanBrokerId <> @BrokerId OR @LoanBrokerId is NULL) BEGIN
		RAISERROR('User does not belong to specified brokerid', 16, 1);
		RETURN -1
	END 

	
	-- Ensure app is linked to specified loan
	SELECT @aBFullName = aBFirstNm + ' ' + aBLastNm,  @aCFullName = aCFirstNm + ' ' + aCLastNm
	FROM APPLICATION_A where sLId = @sLId and aAppId = @aAppId
	
	IF @@rowcount <> 1 BEGIn
		RAISERROR('App Does not belong to specified loan', 16, 1);
		RETURN -1
	END 
	
	-- ensure edoc belongs to specified loan
	IF @LinkedEDocId IS NOT NULL BEGIN
		
		SELECT @EdocLoanId = sLid from edocs_document where @LinkedEDocId = DocumentId
		
		IF (@EdocLoanId <> @sLId OR @EdocLoanId is NULL) BEGIN
			RAISERROR('Edoc does not belong to specified loan', 16, 1);
			RETURN -1
		END
	END 
	INSERT INTO CP_SHARE_DOCUMENT(
		[sLId],
		[aAppId],
		[aBFullName],
		[aCFullName],
		[ShareStatusT],
		[Description],
		[PdfFileDbKey],
		[CreatorEmployeeId],
		[BrokerId],
		[EDocId],
		[EDocVersion]) VALUES(
	@sLid,
	@aAppId,
	@aBFullName,
	@aCFullName,
	0,
	@Description,
	@PdfFileDbKey,
	@CreatorEmployeeId,
	@BrokerId,
	@LinkedEDocId,
	@LinkedEDocVersion
	)
	
	SELECT CAST(scope_identity() as bigint) as Id, @aBFullName as aBFullName, @aCFullName as aCFullName
	
END
