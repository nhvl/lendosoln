

CREATE PROCEDURE [dbo].[CP_UpdateConsumerPassword]
	@ConsumerId bigint,	
	@LoginName varchar(80),	
	@BrokerId uniqueidentifier,
	@OldPasswordHash varchar(200),
	@NewPasswordHash varchar(200)
AS
BEGIN
	UPDATE CP_CONSUMER
	SET		PasswordHash = @NewPasswordHash,
			PasswordResetCode = '',
			PasswordRequestD = NULL,
			IsTemporaryPassword = 0
	
	WHERE	ConsumerId = @ConsumerId AND
			Email = @LoginName AND
			BrokerId = @BrokerId AND
			PasswordHash = @OldPasswordHash
END


