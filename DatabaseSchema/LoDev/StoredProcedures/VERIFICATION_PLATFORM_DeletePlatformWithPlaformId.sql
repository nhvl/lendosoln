-- =============================================
-- Author:		Eric Mallare
-- Create date: 4/27/2017
-- Description:	Deletes a Platform and the associated Transmission Info
-- =============================================
CREATE PROCEDURE [dbo].[VERIFICATION_PLATFORM_DeletePlatformWithPlaformId]
	@PlatformId int
AS
BEGIN TRANSACTION
	DECLARE @TransmissionId int
	SET @TransmissionId = (SELECT p.TransmissionId FROM VERIFICATION_PLATFORM p WHERE p.PlatformId=@PlatformId)
	
		DELETE FROM
			VERIFICATION_PLATFORM
		WHERE
			PlatformId=@Platformid
		
		DELETE FROM
			VERIFICATION_TRANSMISSION
		WHERE	
			TransmissionId=@TransmissionId
		
		if( 0!=@@error)
		BEGIN
			ROLLBACK TRANSACTION
			RAISERROR('Error in delete in VERIFICATION_PLATFORM_DeletePlatformWithPlaformId sp', 16, 1);
			RETURN -100;
		END
COMMIT TRANSACTION
