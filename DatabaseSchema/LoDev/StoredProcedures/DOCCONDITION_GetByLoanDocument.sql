CREATE PROCEDURE [dbo].[DOCCONDITION_GetByLoanDocument] 
	-- Add the parameters for the stored procedure here
	@LoanId uniqueidentifier,
	@DocumentId uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT sLId, TaskId, DocumentId, Status
	FROM TASK_x_EDOCS_DOCUMENT
	WHERE sLId = @LoanId
	AND DocumentId = @DocumentId
END