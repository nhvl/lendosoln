-- =============================================
-- Author:		David Dao
-- Create date: 
-- Description:	Check to see if the loan qualify to participate in nightly task process.
-- =============================================
CREATE PROCEDURE [dbo].[IsQualifyRunNightlyTask] 
    @sLId UniqueIdentifier
AS
BEGIN

    SELECT 1 FROM LOAN_FILE_CACHE cache JOIN BROKER br ON cache.sbrokerId = br.BrokerId
    WHERE sLId=@sLId
      AND cache.IsValid=1
      AND cache.IsTemplate=0
      AND br.Status=1
      AND br.IsUseNewCondition = 1
      AND cache.sStatusT != 9  -- Loan Cancelled
      AND cache.sStatusT != 10 -- Loan Denied
      AND cache.sStatusT != 15 -- Lead Cancelled
      AND cache.sStatusT != 16 -- Lead Declined
      AND cache.sStatusT != 48 -- Loan Withdrawn
      AND cache.sStatusT != 49 -- Loan Archived

END
