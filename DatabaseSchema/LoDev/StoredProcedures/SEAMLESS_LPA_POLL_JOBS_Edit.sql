-- =============================================
-- Author:		Eric Mallare
-- Create date: 11/28/2017
-- Description:	Edits an existing seamless lpa polling job.
-- =============================================
ALTER PROCEDURE [dbo].[SEAMLESS_LPA_POLL_JOBS_Edit]
	@JobId int,
	@PollingAttemptCount int,
	@PollingIntervalInSeconds int,
	@ResultFileDbKey uniqueidentifier = null
AS
BEGIN
	UPDATE
		SEAMLESS_LPA_POLL_JOBS 
	SET
		ResultFileDbKey = @ResultFileDbKey,
		PollingAttemptCount = @PollingAttemptCount,
		PollingIntervalInSeconds = @PollingIntervalInSeconds
	WHERE
		JobId=@JobId
END