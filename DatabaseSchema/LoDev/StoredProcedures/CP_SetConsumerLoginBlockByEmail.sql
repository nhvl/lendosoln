
CREATE PROCEDURE [dbo].[CP_SetConsumerLoginBlockByEmail]
	@Email varchar(80),
	@BrokerId uniqueidentifier
AS

UPDATE CP_CONSUMER
SET IsLocked = 1
WHERE	Email = @Email AND
		BrokerId = @BrokerId

