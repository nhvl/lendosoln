ALTER PROCEDURE [dbo].[SERVICE_CREDENTIAL_CreatePmlBrokerCredential]
	@Id int OUTPUT,
	@BrokerId	uniqueidentifier,
	@PmlBrokerId	uniqueidentifier,
	@ServiceProviderId	uniqueidentifier = null,
	@ServiceProviderName	varchar(100) = null,
	@UserType	int,
	@AccountId	varchar(100),
	@UserName	varchar(100),
	@UserPassword	varbinary(max),
	@EncryptionKeyId	uniqueidentifier,
	@IsForCreditReports	bit,
	@IsForVerifications	bit,
	@VoxVendorId	int = null,
	@VoxVendorName	varchar(150) = null,
	@IsEnabledForNonSeamlessDu	bit,
	@IsForAusSubmission	bit,
	@AusOption	int = null,
	@IsForTitleQuotes	bit,
	@TitleQuoteLenderServiceId	int = null,
	@TitleQuoteLenderServiceName	varchar(200) = null,
	@IsForUcdDelivery	bit,
	@UcdDeliveryTarget	int = null,
	@IsForDocumentCapture	bit,
	@IsForDigitalMortgage bit = 0,
	@DigitalMortgageProviderTarget tinyint = null

AS
	BEGIN TRANSACTION
		IF(EXISTS 
			(SELECT Id 
			 FROM SERVICE_CREDENTIAL sc 
				JOIN PMLBROKER_X_SERVICE_CREDENTIAL xsc ON sc.Id = xsc.ServiceCredentialId AND sc.BrokerId = xsc.BrokerId
			 WHERE 
				@PmlBrokerId = xsc.PmlBrokerId
				AND @BrokerId = sc.BrokerId
				AND @UserType = sc.UserType
				AND
				   ((@IsForCreditReports=1 AND sc.IsForCreditReports=1 AND @ServiceProviderId=sc.ServiceProviderId) OR
					(@IsForVerifications=1 AND sc.IsForVerifications=1 AND @VoxVendorId=sc.VoxVendorId) OR
					(@IsForAusSubmission=1 AND sc.IsForAusSubmission=1 AND @AusOption=sc.AusOption) OR
					(@IsForTitleQuotes=1 AND sc.IsForTitleQuotes=1 AND @TitleQuoteLenderServiceId=sc.TitleQuoteLenderServiceId) OR
					(@IsForUcdDelivery=1 AND sc.IsForUcdDelivery=1 AND @UcdDeliveryTarget=sc.UcdDeliveryTarget) OR
					(@IsForDocumentCapture=1 AND sc.IsForDocumentCapture=1) OR
					(@IsForDigitalMortgage=1 AND sc.IsForDigitalMortgage=1 AND @DigitalMortgageProviderTarget=sc.DigitalMortgageProviderTarget)) 
			))
			GOTO HANDLE_ERROR
			
		INSERT INTO SERVICE_CREDENTIAL(
			BrokerId,
			ServiceProviderId,
			ServiceProviderName,
			UserType,
			AccountId,
			UserName,
			UserPassword,
			EncryptionKeyId,
			IsForCreditReports,
			IsForVerifications,
			VoxVendorId,
			VoxVendorName,
			IsEnabledForNonSeamlessDu,
			IsForAusSubmission,
			AusOption,
			IsForTitleQuotes,
			TitleQuoteLenderServiceId,
			TitleQuoteLenderServiceName,
			IsForUcdDelivery,
			UcdDeliveryTarget,
			IsForDocumentCapture,
			IsForDigitalMortgage,
			DigitalMortgageProviderTarget) 
		VALUES (
			@BrokerId,
			@ServiceProviderId,
			@ServiceProviderName,
			@UserType,
			@AccountId,
			@UserName,
			@UserPassword,
			@EncryptionKeyId,
			@IsForCreditReports,
			@IsForVerifications,
			@VoxVendorId,
			@VoxVendorName,
			@IsEnabledForNonSeamlessDu,
			@IsForAusSubmission,
			@AusOption,
			@IsForTitleQuotes,
			@TitleQuoteLenderServiceId,
			@TitleQuoteLenderServiceName,
			@IsForUcdDelivery,
			@UcdDeliveryTarget,
			@IsForDocumentCapture,
			@IsForDigitalMortgage,
			@DigitalMortgageProviderTarget)
			
		IF @@error!= 0 GOTO HANDLE_ERROR
		SET @Id=SCOPE_IDENTITY();
	 
		INSERT INTO PMLBROKER_X_SERVICE_CREDENTIAL(
			PmlBrokerId,
			BrokerId,
			ServiceCredentialId) 
		VALUES (
			@PmlBrokerId,
			@BrokerId,
			@Id)
	
  
	IF( @@error != 0 )
		GOTO HANDLE_ERROR

	COMMIT TRANSACTION
	
	RETURN;

	HANDLE_ERROR:
		ROLLBACK TRANSACTION
		RAISERROR('Error in [SERVICE_CREDENTIAL_CreatePmlBrokerCredential] sp', 16, 1);;
		RETURN;
