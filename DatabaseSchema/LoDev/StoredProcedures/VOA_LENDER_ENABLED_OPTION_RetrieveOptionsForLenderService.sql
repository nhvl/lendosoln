ALTER PROCEDURE [dbo].[VOA_LENDER_ENABLED_OPTION_RetrieveOptionsForLenderService]
	@BrokerId uniqueidentifier,
	@LenderServiceId int,
	@VendorId int
AS
BEGIN
	SELECT
		VendorId,
		BrokerId,
		LenderServiceId,
		EnabledOptionType,
		EnabledOptionValue,
		UserType,
		IsDefault
	FROM VOA_LENDER_ENABLED_OPTION
	WHERE BrokerId = @BrokerId
		AND LenderServiceId = @LenderServiceId
END
