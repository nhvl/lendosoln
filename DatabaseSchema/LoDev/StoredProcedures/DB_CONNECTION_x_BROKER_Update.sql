-- =============================================
-- Author:		David Dao
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE dbo.DB_CONNECTION_x_BROKER_Update
    @BrokerId UniqueIdentifier,
    @CustomerCode varchar(36)
AS
BEGIN

    UPDATE DB_CONNECTION_x_BROKER
       SET CustomerCode = @CustomerCode
    WHERE BrokerId = @BrokerId

END
