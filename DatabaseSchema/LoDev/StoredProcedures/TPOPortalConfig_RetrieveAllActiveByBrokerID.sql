-- =============================================
-- Author:		Scott Kibler
-- Create date: 2/4/2014
-- Description:	opm 150384
-- =============================================
CREATE PROCEDURE [dbo].TPOPortalConfig_RetrieveAllActiveByBrokerID
	@BrokerID uniqueidentifier
AS
BEGIN
	SELECT *
	FROM LENDER_TPO_LANDINGPAGECONFIG WITH(NOLOCK)
	WHERE BrokerID = @BrokerID
	and IsDeleted = 0
END

