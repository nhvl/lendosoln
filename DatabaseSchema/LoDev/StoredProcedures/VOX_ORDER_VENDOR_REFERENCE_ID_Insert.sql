-- =============================================
-- Author:		Eric Mallare
-- Create date: 6/4/2018
-- Description:	Inserts a vendor reference id for a VOX order.
-- =============================================
CREATE PROCEDURE [dbo].[VOX_ORDER_VENDOR_REFERENCE_ID_Insert]
	@OrderId int,
	@VendorReferenceId varchar(50)
AS
BEGIN
	INSERT INTO
		VOX_ORDER_VENDOR_REFERENCE_ID(OrderId, VendorReferenceId)
	VALUES
		(@OrderId, @VendorReferenceId)
END