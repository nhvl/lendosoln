-- =============================================
-- Author:		Ajay Mishra
-- Create date: 07/21/11
-- Description:	Deletes the FileDB key for signature file for a Employee
-- =============================================
CREATE PROCEDURE DeleteEmployeeSignatureKey	
	 @EmployeeId uniqueidentifier	 
AS
BEGIN
	UPDATE BROKER_USER set SignatureFileDBKey = '00000000-0000-0000-0000-000000000000' WHERE EmployeeId=@EmployeeId
END
