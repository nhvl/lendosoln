CREATE PROCEDURE dbo.GetAccessControlBrokerFeatures
	@BrokerId Guid
AS
	SELECT FeatureId 
	FROM Feature_Subscription
	WHERE BrokerId = @BrokerId
	  AND IsActive=1

