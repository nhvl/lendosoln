-- =============================================
-- Author:		Geoff Feltman
-- Create date: 3/2/17
-- Description:	Gets status of broker.
-- =============================================
CREATE PROCEDURE dbo.GetBrokerStatus
	@brokerid uniqueidentifier
AS
BEGIN
	select status
	from broker
	where brokerid = @brokerid
END
GO
