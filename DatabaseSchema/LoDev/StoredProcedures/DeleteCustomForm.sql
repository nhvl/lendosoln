
CREATE PROCEDURE [dbo].[DeleteCustomForm]
	@BrokerID uniqueidentifier,
	@CustomLetterID uniqueidentifier
AS
	DELETE FROM Custom_Letter WHERE BrokerId = @BrokerID AND CustomLetterID = @CustomLetterID

