CREATE PROCEDURE NotifyDiscUsers
	@DiscLogId uniqueidentifier,
	@NotifByUserId uniqueidentifier,
	@BrokerId uniqueidentifier,
	@MemoAllReceiverLoginNms varchar(256)
AS
declare @NmListLen  int;
declare @NmStartIndex int;
declare @NmEndIndex int;
set  @NmListLen = len( @MemoAllReceiverLoginNms );
set @NmStartIndex = 1;
set @NmEndIndex = charindex(';',@MemoAllReceiverLoginNms, 1 );
declare @tranPoint sysname;
set @tranPoint = object_name(@@procId) + CAST(@@nestlevel as varchar(10))
begin transaction
save transaction @tranPoint
--LOAD UP A TABLE OF ALL USER NAMES AND IDS HERE, SEE THE OTHER MEMO STORE PROC FOR DETAILS
while @NmStartIndex > 0 and @NmEndIndex < @NmListLen
begin 
	declare @MemoReceiverLoginNm varchar (36);
	
	set @MemoReceiverLoginNm = ltrim( rtrim( substring( @MemoAllReceiverLoginNms, @NmStartIndex, @NmEndIndex - @NmStartIndex ) ) );
	
	declare @userid uniqueidentifier;
	select @userid = au.userid
	from( ( all_user au join broker_user bu on au.userid = bu.userid ) join employee em on em.employeeid = bu.employeeid ) join branch br on br.branchid = em.branchid
	where br.brokerid = @brokerid and au.loginnm = @MemoReceiverLoginNm
	if( @userid is not null )
	begin
		/*
		ALTER  PROCEDURE CreateDiscNotif 
			@NotifId uniqueidentifier,
			@NotifAlertDate datetime,
			@NotifUpdatedDate datetime,
			@NotifIsRead bit,
			@DiscLogId uniqueidentifier,
			@NotifReceiverUserId uniqueidentifier,
			@NotifReceiverLoginNm varchar(36),
			@NotifBrokerId uniqueidentifier,
			@NotifIsValid bit,
			@NotifInvalidDate datetime
		*/
		declare @notifId uniqueidentifier;
		set @notifId = newid();
		
		declare @updatedDate datetime;
		set @updatedDate = getdate();
		
		exec CreateDiscNotif @NotifId = @notifId, 
			@NotifAlertDate = null, 
			@NotifUpdatedDate = @updatedDate,
			@NotifIsRead = 0, 
			@DiscLogId = @DiscLogId,
			@NotifReceiverUserId = @userid,
			@NotifReceiverLoginNm = @MemoReceiverLoginNm,
			@NotifBrokerId = @brokerid,
			@NotifIsValid = 1,
			@NotifInvalidDate = null
		if(0!=@@error)
		begin
			RAISERROR('Error in creating new discussion notification in sp NotifyDiscUsers', 16, 1);
			goto ErrorHandler;
		end
	end
	
	
	/*
	insert into Memo
	( [MemoId], [MemoAlertDate], [MemoIsRead], [DiscLogId], [MemoCreatorUserId],[MemoReceiverUserId],[MemoCreatorLoginNm],
	[MemoReceiverLoginNm] ,MemoBrokerId,[MemoMsg],MemoAllReceiverLoginNms,MemoAllReceiverUserIds,	MemoTrackIsValid,MemoTrackInvalidDate )
	values
	( newid(), @MemoAlertDate, 0, @DiscLogId, @MemoCreatorUserId, @MemoReceiverUserId, @MemoCreatorLoginNm,
	@MemoReceiverLoginNm, @MemoBrokerId, @MemoMsg, @MemoAllReceiverLoginNms, @MemoAllReceiverUserIds, @MemoTrackIsValid, @MemoTrackInvalidDate )
	*/
	
	set @NmStartIndex = @NmStartIndex + 1; -- add one to skip over the last ';'
	set @NmEndIndex = charIndex( ';', @MemoAllReceiverLoginNms, @NmStartIndex );
end
commit transaction @tranPoint;
commit transaction
return 0;
ErrorHandler:
	rollback transaction @tranPoint
	rollback transaction
	RETURN -100;
