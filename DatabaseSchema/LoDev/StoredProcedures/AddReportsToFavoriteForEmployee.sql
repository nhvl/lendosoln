CREATE PROCEDURE [AddReportsToFavoriteForEmployee]
	@EmployeeId uniqueidentifier, 	
	@CommaSeparatedReportIds varchar(2000)
AS
BEGIN
	SET NOCOUNT ON
	BEGIN TRANSACTION
		DECLARE @ReportId varchar(100), @Pos int	
				
		SET @CommaSeparatedReportIds = LTRIM(RTRIM(@CommaSeparatedReportIds))+ ','
		SET @Pos = CHARINDEX(',', @CommaSeparatedReportIds, 1)
		IF REPLACE(@CommaSeparatedReportIds, ',', '') <> ''
		BEGIN
			WHILE @Pos > 0
			BEGIN
				SET @ReportId = LTRIM(RTRIM(LEFT(@CommaSeparatedReportIds, @Pos - 1)))
				IF @ReportId <> ''
				BEGIN						
					IF NOT EXISTS (SELECT * FROM EMPLOYEE_FAVORITE_REPORTS WHERE QueryId=@ReportId AND EmployeeId=@EmployeeId)
					INSERT INTO EMPLOYEE_FAVORITE_REPORTS (EmployeeId, QueryId) VALUES (@EmployeeId, @ReportId)					
					IF( 0!=@@error) GOTO ErrorHandler
				END
				SET @CommaSeparatedReportIds = RIGHT(@CommaSeparatedReportIds, LEN(@CommaSeparatedReportIds) - @Pos)
				SET @Pos = CHARINDEX(',', @CommaSeparatedReportIds, 1)
			END
		END
	COMMIT TRANSACTION
	
	ErrorHandler:
		if( 0!=@@error)
		BEGIN
			ROLLBACK TRANSACTION
			RAISERROR('Error in delete from Group table in AddReportsToFavoriteForEmployee sp', 16, 1);
			RETURN -100;
		END		
END
