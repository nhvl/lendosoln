-- =============================================
-- Author:		Timothy Jewell
-- Create date:	07/26/2017
-- Description:	Creates an SSA-89 order.  Be sure to wrap this in a transaction with other inserts
-- =============================================
ALTER PROCEDURE [dbo].[SSA89_ORDER_Create]
	@OrderId int,
	@IsForCoborrower bit,
	@BorrowerName varchar(100),
	@LastFourSsn char(4),
	@DateOfBirth smalldatetime = null,
	@DeathMasterStatus int = null,
	@SocialSecurityAdministrationStatus int = null,
	@OfficeOfForeignAssetsControlStatus int = null,
	@CreditHeaderStatus int = null,
	@RequestFormAccepted int = null,
	@RequestFormRejectionReason varchar(200) = null
AS
BEGIN
	INSERT INTO SSA89_ORDER
		(OrderId, IsForCoborrower, BorrowerName, LastFourSSN, DateOfBirth, DeathMasterStatus, SocialSecurityAdministrationStatus,
		OfficeOfForeignAssetsControlStatus, CreditHeaderStatus, RequestFormAccepted, RequestFormRejectionReason)
	VALUES
		(@OrderId, @IsForCoborrower, @BorrowerName, @LastFourSsn, @DateOfBirth, @DeathMasterStatus, @SocialSecurityAdministrationStatus,
		@OfficeOfForeignAssetsControlStatus, @CreditHeaderStatus, @RequestFormAccepted, @RequestFormRejectionReason)
END
