-- =============================================
-- Author:		Antonio Valencia
-- Create date: 7/27/2012
-- Description:	Checks whether there is a draft system
-- =============================================
CREATE PROCEDURE dbo.[CC_TEMPLATE_DraftCount] 
	@BrokerId uniqueidentifier 
AS
BEGIN
	SET NOCOUNT ON;

	SELECT COUNT(Id) FROM CC_TEMPLATE_SYSTEM Where BrokerId = @BrokerId AND
	ReleasedD is NULL 
END
