-- =============================================
-- Author:		Antonio Valencia
-- Create date: 5/23/2013
-- Description:	Records Title Info
-- =============================================
CREATE PROCEDURE [dbo].[TITLE_VENDOR_RESPONSE_SaveInfo]
	-- Add the parameters for the stored procedure here
	@BrokerId uniqueidentifier, 
	@sLId uniqueidentifier,
	@TitleVendorId int,
	@RequestResponseXml varchar(max)
AS
BEGIN
	DELETE FROM TITLE_VENDOR_RESPONSE 
	WHERE BrokerId = @BrokerId AND sLId = @sLId  --delete old quote
	
	INSERT INTO TITLE_VENDOR_RESPONSE(TitleVendorId, BrokerId,sLId, TitleXml)
	VALUES(@TitleVendorId,@BrokerId,@sLId,@RequestResponseXml)
END
