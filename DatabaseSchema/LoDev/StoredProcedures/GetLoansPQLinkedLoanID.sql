


-- =============================================
-- Author:		Antonio
-- Create date: March 20 2009
-- Description:	Retrieves the loan id for the given broker id, loans pq file id
-- =============================================
CREATE PROCEDURE [dbo].[GetLoansPQLinkedLoanID] 
	@BrokerID uniqueidentifier,
	@LoansPQFileID bigint
AS
BEGIN
	--select '8355ca5c-e3a4-4632-a927-c4c6334f89fe'	
	DECLARE @slid  uniqueidentifier
	DECLARE @slnm varchar(36)
	
	SELECT top 1 @slid = sLId, @slnm = sLNm from LOAN_FILE_E WHERE @BrokerID = sBrokerId and sLpqLoanNumber  = @LoansPQFileID
	if @@rowcount = 0 begin
		select  '00000000-0000-0000-0000-000000000000' as LoanId , '' as LoanName
	end
	else 
		select @slid as LoanId, @slnm as LoanName
END



