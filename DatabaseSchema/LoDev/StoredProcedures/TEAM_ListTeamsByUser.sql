

CREATE PROCEDURE [dbo].[TEAM_ListTeamsByUser] 
	@BrokerId		uniqueidentifier,
	@EmployeeId		uniqueidentifier,
	@RoleId			uniqueidentifier = null
AS
	SELECT 
		t.Id
		, t.RoleId
		, t.Name 
		, Notes
		, IsPrimary
	FROM TEAM t 
	LEFT JOIN TEAM_USER_ASSIGNMENT tua ON t.Id = tua.TeamId
	WHERE
	BrokerId = @BrokerId
	AND 
	tua.EmployeeId = @EmployeeId
	AND
	( @RoleId IS NULL
	OR @RoleId = RoleId)
	
