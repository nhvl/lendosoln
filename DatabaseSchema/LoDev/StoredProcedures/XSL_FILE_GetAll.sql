/*
	Author: 
		Scott Kibler
	Description: 
		Gets the xsl files that have been created in the system.
	opm:
		463659
*/
CREATE PROCEDURE [dbo].[XSL_FILE_GetAll]
AS
BEGIN
	SELECT Id, Name
	FROM
		XSL_FILE
END