-- ============================================================
-- Author:      Generated by application LqbEntitiesToSqlTables
-- Create date: 5/25/2018
-- Description: Updates an existing row of the CONSUMER_X_VOR_RECORD table.
-- ============================================================
ALTER PROCEDURE [dbo].[CONSUMER_X_VOR_RECORD_Update]
	@ConsumerVorRecordAssociationId UniqueIdentifier,
	@LoanId UniqueIdentifier,
	@ConsumerId UniqueIdentifier,
	@VorRecordId UniqueIdentifier,
	@AppId UniqueIdentifier,
	@IsPrimary Bit
AS
BEGIN
	UPDATE [dbo].[CONSUMER_X_VOR_RECORD]
	SET
		ConsumerId = @ConsumerId,
		VorRecordId = @VorRecordId,
		AppId = @AppId,
		IsPrimary = @IsPrimary
	WHERE
		ConsumerVorRecordAssociationId = @ConsumerVorRecordAssociationId
		AND LoanId = @LoanId
END
