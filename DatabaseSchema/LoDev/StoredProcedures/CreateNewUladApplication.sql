ALTER PROCEDURE [dbo].[CreateNewUladApplication]
	@LoanId UniqueIdentifier,
	@BrokerId UniqueIdentifier,
	@LegacyAppId UniqueIdentifier,
	@UladAppId UniqueIdentifier OUT,
	@UladAppConsumerId UniqueIdentifier OUT,
	@ConsumerId UniqueIdentifier OUT,
	@IsPrimary Bit OUT
AS
SET @UladAppId = NEWID();
SET @UladAppConsumerId = NEWID();
SELECT @ConsumerId = aBConsumerId FROM APPLICATION_A WHERE aAppId = @LegacyAppId;
SET @IsPrimary = CASE WHEN EXISTS(SELECT RowId FROM ULAD_APPLICATION WHERE BrokerId = @BrokerId AND LoanId = @LoanId) THEN 0 ELSE 1 END;

DECLARE @stringUladId varchar(36);
SET @stringUladId = CONVERT(varchar(36), @UladAppId);

DECLARE @stringConsumerId varchar(36);
SET @stringConsumerId = CONVERT(varchar(36), @ConsumerId);

DECLARE @uladOrder varchar(MAX);
SELECT @uladOrder = SortOrder FROM LOAN_COLLECTION_SORT_ORDER WHERE LoanId = @LoanId AND CollectionName = 'UladApplication';
IF @uladOrder IS NULL
	SET @uladOrder = @stringUladId;
ELSE
	SET @uladOrder = @uladOrder + ',' + @stringUladId;

EXEC ULAD_APPLICATION_Create @UladAppId, @LoanId, @BrokerId, @IsPrimary;
EXEC ULAD_APPLICATION_X_CONSUMER_Create @UladAppConsumerId, @LoanId, @UladAppId, @ConsumerId, @LegacyAppId, 1;
EXEC LOAN_COLLECTION_SORT_ORDER_InsertOrUpdate @LoanId, NULL, NULL, NULL, 'UladApplication', @uladOrder
EXEC LOAN_COLLECTION_SORT_ORDER_InsertOrUpdate @LoanId, NULL, NULL, @UladAppId, 'Consumer', @stringConsumerId
