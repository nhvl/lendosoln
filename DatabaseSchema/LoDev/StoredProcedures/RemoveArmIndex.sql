CREATE PROCEDURE RemoveArmIndex
	@IndexId Guid
AS
	DELETE FROM Arm_Index
	WHERE
		IndexIdGuid = @IndexId
	IF( @@error != 0 )
	BEGIN
		RAISERROR('error in removing from table Arm_Index in RemoveArmIndex sp', 16, 1);
		RETURN -100
	END
	RETURN 0
