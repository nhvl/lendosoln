-- =============================================
-- Author:		Peter Anargirou
-- Create date: 7/16/08
-- Description:	Returns number of active users by branch ID
-- Modified:	2/24/12, Matthew Pham
--				It used to just count B users, now it counts
--				all users.
-- =============================================
ALTER PROCEDURE [dbo].[GetNumActiveUsersAssociatedWithBranchByBranchId] 
	@BranchId uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
SELECT
		COUNT('') AS numActiveUsers
	FROM 
		--VIEW_ACTIVE_LO_USER
		VIEW_LENDERSOFFICE_EMPLOYEE
	WHERE
		BranchId = @BranchId OR MiniCorrespondentBranchId = @BranchId OR CorrespondentBranchId = @BranchId
		AND IsActive = 1
END
