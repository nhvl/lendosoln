ALTER PROCEDURE [dbo].[VOX_ORDER_STATUSES_LoadFromOrder]
    @OrderId int,
    @LoanId uniqueidentifier
AS

SELECT vo.OrderId, statuses.StatusCode, statuses.StatusDescription, statuses.StatusTime
FROM VOX_ORDER [vo] JOIN VOX_ORDER_STATUSES statuses ON vo.OrderId = statuses.OrderId
WHERE vo.OrderId = @OrderId AND vo.LoanId = @LoanId
