-- =============================================
-- Author:		David Dao
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[GetAdditionalCredentialForBrokerUserPrincipal]
    @BrokerId UniqueIdentifier,
    @UserId UniqueIdentifier,
    @EmployeeId UniqueIdentifier,
	@SessionId varchar(13)


AS
BEGIN

    -- 4/18/2014 - dd
    -- Return multiple result set record. The order must match with code in BrokerUserPrincipal.cs

    -- #1 - Retrieve Role of employee.
    SELECT RoleId FROM ROLE_ASSIGNMENT WHERE EmployeeId=@EmployeeId;
    
    -- #2 - Retrieve Broker Global IP Whitelist.
    SELECT IpAddress FROM  BROKER_GLOBAL_IP_WHITELIST WHERE BrokerId=@BrokerId;
    
    -- #3 - Get Team membership
    SELECT TeamId FROM TEAM_USER_ASSIGNMENT WHERE EmployeeId=@EmployeeId;

	-- #4 - Get Session Info
	SELECT SessionId FROM Login_Session WHERE SessionId = @SessionId

END
