-- =============================================
-- Author:		Eric Mallare
-- Create date: 7/21/2017
-- Description:	Removes all document records associated with an order.
-- =============================================
ALTER PROCEDURE [dbo].[VOX_DOCUMENT_RemoveByOrderId]
	@OrderId int
AS
BEGIN
	DELETE FROM
		VOX_DOCUMENT
	WHERE
		OrderId=@OrderId
END
