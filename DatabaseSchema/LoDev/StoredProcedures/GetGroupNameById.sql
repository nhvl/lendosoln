-- =============================================
-- Author:		Michael Leinweaver
-- Create date: 11/16/2017
-- Description:	Gets a group's name by ID.
-- =============================================
CREATE PROCEDURE [dbo].[GetGroupNameById]
    @BrokerId uniqueidentifier,
    @GroupId uniqueidentifier
AS
BEGIN
	SELECT GroupName
	FROM [GROUP]
	WHERE BrokerId = @BrokerId AND GroupId = @GroupId
END
