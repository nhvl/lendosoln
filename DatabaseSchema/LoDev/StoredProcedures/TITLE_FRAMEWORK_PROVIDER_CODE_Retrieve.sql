-- =============================================
-- Author:		Justin Lara
-- Create date: 8/24/2017
-- Description:	Retrieves a provider code.
-- =============================================
ALTER PROCEDURE [dbo].[TITLE_FRAMEWORK_PROVIDER_CODE_Retrieve]
	@BrokerId uniqueidentifier,
	@AgentId uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT ProviderCode
	FROM TITLE_FRAMEWORK_PROVIDER_CODE
	WHERE
		BrokerId = @BrokerId
		AND AgentId = @AgentId
END
