
-- =============================================
-- Author:		Isaac Ribakoff
-- Create date: 3/28/2014
-- Description:	Pulls CRMNow ID and loan status information (OPM 169478)
-- =============================================
CREATE PROCEDURE [dbo].[RetrieveCRMNowLoanStatus]
	@CustomerCode varchar(50),
	@LastTransactionTime smalldatetime
AS
BEGIN
DECLARE @BrokerID uniqueidentifier
SELECT @BrokerID=brokerid FROM Broker WHERE CustomerCode=@CustomerCode

Select lc.sLNm, sCrmNowLeadId,
		case( lc.sStatusT )
		when '0' then 'Loan Open'
		when '1' then 'Pre-qual'
		when '2' then 'Pre-approved' 
		when '3' then 'Registered' 
		when '4' then 'Approved' 
		when '5' then 'Docs Out' 
		when '6' then 'Funded' 
		when '7' then 'Loan On-hold' 
		when '8' then 'Loan Suspended' 
		when '9' then 'Loan Canceled' 
		when '10' then 'Loan Denied' 
		when '11' then 'Loan Closed' 
		when '12' then 'Lead New' 
		when '13' then 'In Underwriting' 
		when '14' then 'Loan Web Consumer'
		when '15' then 'Lead Canceled' 
		when '16' then 'Lead Declined' 
		when '17' then 'Lead Other' 
		when '18' then 'Loan Other' 
		when '19' then 'Recorded' 
		when '20' then 'Loan Shipped' 
		when '21' then 'Clear to Close' 
		when '22' then 'Processing' 
		when '23' then 'Final Underwriting' 
		when '24' then 'Docs Back' 
		when '25' then 'Funding Conditions' 
		when '26' then 'Final Docs' 
		when '27' then 'Loan Sold' 
		when '28' then 'Loan Submitted'
		when '29' then 'Pre-Processing'
		when '30' then 'Document Check'
		when '31' then 'Document Check Failed'
		when '32' then 'Pre-Underwriting'
		when '33' then 'Condition Review'
		when '34' then 'Pre-Doc QC'
		when '35' then 'Docs Ordered'
		when '36' then 'Docs Drawn'
		when '37' then 'Investor Conditions'
		when '38' then 'Investor Conditions Sent'
		when '39' then 'Ready For Sale'
		when '40' then 'Submitted For Purchase Review'
		when '41' then 'In Purchase Review'
		when '42' then 'Pre-Purchase Conditions'
		when '43' then 'Submitted For Final Purchase Review'
		when '44' then 'In Final Purchase Review'
		when '45' then 'Clear To Purchase'
		when '46' then 'Loan Purchased'
		when '47' then 'Counter Offer Approved'
		when '48' then 'Loan Withdrawn'
		when '49' then 'Loan Archived'
	END AS sStatusT_Text,
	(select Orderindex from Loan_Status where EnumValue=lc.sStatusT) as statusNumber,
	case( lc.sStatusT )
		when '0' then sOpenedD													--'Loan Open'
		when '1' then sPreQualD													--'Loan Pre-qual'
		when '2' then sPreApprovD												--'Loan Pre-approve' 
		when '3' then sSubmitD													--'Loan Registered' 
		when '4' then sApprovD													--'Loan Approved' 
		when '5' then sDocsD													--'Loan Docs' 
		when '6' then sFundD													--'Loan Funded' 
		when '7' then sOnHoldD													--'Loan On hold' 
		when '8' then sSuspendedD												--'Loan Suspended' 
		when '9' then sCanceledD												--'Loan Canceled' 
		when '10' then sRejectD													--'Loan Rejected' 
		when '11' then sClosedD													--'Loan Closed' 
		when '12' then sLeadD													--'Lead Open' 
		when '13' then sUnderwritingD											--'Loan Underwriting' 
		when '14' then sOpenedD													--'Web Consumer', default to sOpenedD
		when '15' then sCanceledD												--'Lead Canceled' 
		when '16' then sRejectD													--'Lead Declined' 
		when '17' then sOpenedD													--'Lead Other', default to sOpenedD
		when '18' then sOpenedD													--'Loan Other', default to sOpenedD 
		when '19' then sRecordedD												--'Loan Recorded' 
		when '20' then lc.sShippedToInvestorD									--'Loan Shipped' 
		when '21' then sClearToCloseD											--'Loan Clear to Close' 
		when '22' then sProcessingD												--'Loan Processing' 
		when '23' then sFinalUnderwritingD										--'Loan Final Underwriting' 
		when '24' then sDocsBackD												--'Loan Docs Back' 
		when '25' then sFundingConditionsD										--'Loan Funding Conditions' 
		when '26' then sFinalDocsD												--'Loan Final Docs' 
		when '27' then lc.sLPurchaseD											--'Loan Purchased' -- Loan Sold 
		when '28' then sLoanSubmittedD											--'Loan Submitted'
		when '29' then sPreProcessingD											--'Loan Pre-Processing'
		when '30' then sDocumentCheckD											--'Loan Document Check'
		when '31' then sDocumentCheckFailedD									--'Loan Document Check Failed'
		when '32' then sPreUnderwritingD										--'Loan Pre-Underwriting'
		when '33' then sConditionReviewD										--'Loan Condition Review'
		when '34' then sPreDocQCD												--'Loan Pre-Doc QC'
		when '35' then sDocsOrderedD											--'Loan Docs Ordered'
		when '36' then sDocsDrawnD												--'Loan Docs Drawn'
		when '37' then sSuspendedByInvestorD									--'Loan Investor Conditions'
		when '38' then sCondSentToInvestorD										--'Loan Investor Conditions Sent'
		when '39' then sReadyForSaleD											--'Loan Ready For Sale'
		when '40' then sSubmittedForPurchaseReviewD								--'Loan Submitted For Purchase Review'
		when '41' then sInPurchaseReviewD										--'Loan In Purchase Review'
		when '42' then sPrePurchaseConditionsD									--'Loan Pre-Purchase Conditions'
		when '43' then sSubmittedForFinalPurchaseD								--'Loan Submitted For Final Purchase Review'
		when '44' then sInFinalPurchaseReviewD									--'Loan In Final Purchase Review'
		when '45' then sClearToPurchaseD										--'Loan Clear To Purchase'
		when '46' then sPurchasedD												--'Loan Purchased'
		when '47' then sCounterOfferD											--'Loan Counter Offer Approved'
		when '48' then sWithdrawnD												--'Loan Withdrawn'
		when '49' then sArchivedD												--'Loan Archived'
	END AS sStatusD

	FROM LOAN_FILE_CACHE lc WITH(NOLOCK) JOIN LOAN_FILE_CACHE_2 lc2 WITH(NOLOCK) ON lc.sLId = lc2.sLId
										 JOIN LOAN_FILE_CACHE_3 lc3 WITH(NOLOCK) ON lc.sLId = lc3.sLId
										 JOIN LOAN_FILE_B lb WITH(NOLOCK) ON lc.sLId = lb.sLId
	WHERE ( sBrokerId = @BrokerID )
	and ( IsValid = 1 )
	and ( IsTemplate = 0 )
	and ( ( sCrmNowLeadId != '') or ( sCreatedD >= @LastTransactionTime ) )
	and ( sStatusD >= @LastTransactionTime )
	
	SELECT CURRENT_TIMESTAMP as TransactionCompleted
END

