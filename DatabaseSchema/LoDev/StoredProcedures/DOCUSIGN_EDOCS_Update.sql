-- =============================================
-- Author:		Eric Mallare
-- Create date: 3/5/2018
-- Description:	Updates a DocuSign Edoc record
-- =============================================
ALTER PROCEDURE [dbo].[DOCUSIGN_EDOCS_Update]
	@EnvelopeReferenceId int,
	@Id int,
	@EdocId uniqueidentifier = NULL,
	@Name varchar(200),
	@SequenceNumber int
AS
BEGIN
	UPDATE [dbo].[DOCUSIGN_EDOCS]
	SET
		EdocId=@EdocId,
		Name=@Name,
		SequenceNumber=@SequenceNumber
	WHERE
		Id=@Id AND
		EnvelopeReferenceId=@EnvelopeReferenceId
END