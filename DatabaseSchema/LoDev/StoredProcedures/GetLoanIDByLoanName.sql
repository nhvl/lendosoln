CREATE  PROCEDURE [dbo].[GetLoanIDByLoanName]
	@sLNm varchar(36),
	@sBrokerID Guid,
	@CheckInvalid bit = 1
AS
	DECLARE @tempSlid uniqueidentifier  
	
	SELECT @tempSlid = sLId FROM LOAN_FILE_CACHE with(nolock) 
	WHERE sLNm = @sLNm AND sBrokerID = @sBrokerID AND (IsValid=1 OR @CheckInvalid  = 1) 
	
	if @tempSlid is null and @sBrokerID = 'baf07293-f626-44c6-9750-706199a7c0b6'  --for remn broker only 
	begin
		SELECT top 1 @tempSlid = a.sLId FROM LOAN_FILE_CACHE_2 as a with(nolock)  
		join loan_file_cache as b on a.slid = b.slid
		WHERE sBrokerID = @sBrokerID AND a.sOldLNm = @sLNm and IsValid=1
	end
	if @tempslid is not null begin
		select @tempslid as sLId
	end 