ALTER PROCEDURE [dbo].[InsertCreditReportFile]
	@Owner GUID,
	@ExternalFileId  ShortDesc,
	@DbFileKey Guid,
	@ComId Guid,
	@UserId Guid,
	@CrAccProxyId Guid,
	@HowDidItGetHere varchar(50),
	@BrandedProductName varchar(50) = 'Credit Report'
AS
	DECLARE @tranPoint SYSNAME
	set @tranPoint = object_name(@@procId) + CAST(@@nestlevel as varchar(10))
	
	BEGIN TRANSACTION 
	SAVE TRANSACTION @tranPoint
	INSERT INTO Service_File(ComId, ServiceType, FileType, Owner, ExternalFileId, DbFileKey, UserId,  CrAccProxyId, HowDidItGetHere, BrandedProductName)
             VALUES (@ComId, 'Credit', 'CreditReport', @Owner, @ExternalFileId, @DbFileKey, @UserId,  @CrAccProxyId, @HowDidItGetHere, @BrandedProductName)
	IF ( 0!=@@error )
	BEGIN
			RAISERROR('Error in inserting into Service_File table in InsertCreditReportFile sp', 16, 1);
			goto ErrorHandler;
	END
	COMMIT TRANSACTION
	RETURN 0;
ErrorHandler:
	rollback transaction @tranPoint
	rollback transaction
	return -100
