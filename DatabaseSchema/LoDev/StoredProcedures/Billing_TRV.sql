CREATE PROCEDURE [dbo].[Billing_TRV]
	@StartDate datetime,
	@EndDate datetime
AS
BEGIN
-- dd 2018-11-02 - For use in automate billing executable.

SELECT BrokerNm As LenderName, CustomerCode, sLNm As LoanNumber, OrderNumber, OrderDate, ApplicationName, TransactionId, VendorId , cache.sLId As LoanId
                            FROM IRS_4506T_Order_Info oi WITH (nolock) JOIN Loan_File_Cache cache WITH (nolock) ON cache.sLId = oi.sLId
                                                                       JOIN Broker WITH (nolock) ON Broker.BrokerId = cache.sBrokerId
                            WHERE BROKER.SuiteType = 1
                            AND cache.sLoanFileT = 0
                            AND OrderDate >= @StartDate AND OrderDate < @EndDate
							
END