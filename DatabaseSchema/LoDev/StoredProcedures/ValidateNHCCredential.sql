
CREATE PROCEDURE [dbo].[ValidateNHCCredential] 
	@NHCKey uniqueidentifier,
	@LoginNm varchar(50)
AS
SELECT EmployeeID, BrokerID, UserID
FROM VIEW_ACTIVE_LO_USER_WITH_BROKER_AND_LICENSE_INFO
WHERE NHCKey = @NHCKey
	AND LoginNm = @LoginNm

