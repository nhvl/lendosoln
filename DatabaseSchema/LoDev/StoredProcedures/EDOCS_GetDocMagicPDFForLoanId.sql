
-- =============================================
-- Author:		Antonio Valencia
-- Create date: 8/22/11
-- Description:	List all the pdf statuses for the given loan
-- =============================================
CREATE PROCEDURE [dbo].[EDOCS_GetDocMagicPDFForLoanId]
	@LoanId uniqueidentifier, 
	@BrokerId uniqueidentifier 
AS
BEGIN
	DECLARE @tempbrokerid uniqueidentifier
	SELECT  @tempbrokerid  = sBrokerId from LOAN_FILE_CACHE where sLId = @LoanId AND sBrokerId = @BrokerId 
	
	IF ( @@rowcount <> 1 ) begin
		return
	end

	SELECT * FROM EDOCS_DOCMAGIC_PDF_STATUS WHERE LoanId = @LoanId 
END

