
CREATE PROCEDURE [dbo].[UpdateEntireProtocolType] 
	@ProtocolType int,
	@IsWarningOn bit,
	@WarningMsg varchar(500),
	@WarningStartD smalldatetime
AS
UPDATE Service_Company
SET IsWarningOn = @IsWarningOn,
	WarningMsg = @WarningMsg,
	WarningStartD = @WarningStartD
WHERE ProtocolType = @ProtocolType
