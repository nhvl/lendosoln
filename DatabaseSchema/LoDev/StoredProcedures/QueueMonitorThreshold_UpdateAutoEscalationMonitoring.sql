-- =============================================
-- Author:		Michael Leinweaver
-- Create date: 10/27/2015
-- Description:	Updates the queue monitoring threshold values stored for the queue specified
--				by @QueueName.
-- =============================================
ALTER PROCEDURE [dbo].[QueueMonitorThreshold_UpdateAutoEscalationMonitoring]
	@ID int,
	@NewName varchar(50),
	@NewDescription varchar(50),
	@NewThreshold1 int,
	@NewThreshold1DurationInMinutes int,
	@NewThreshold1OldestItemMinutes int,
	@NewThreshold2 int,
	@NewThreshold2DurationInMinutes int,
	@NewThreshold2OldestItemMinutes int
AS
BEGIN
	UPDATE QUEUE_MONITOR_THRESHOLD
	SET 
		Name = @NewName,
		Description = @NewDescription,
		Threshold1 = @NewThreshold1,
		Threshold1DurationInMinutes = @NewThreshold1DurationInMinutes,
		Threshold1OldestItemMinutes = @NewThreshold1OldestItemMinutes,
		Threshold2 = @NewThreshold2,
		Threshold2DurationInMinutes = @NewThreshold2DurationInMinutes,
		Threshold2OldestItemMinutes = @NewThreshold2OldestItemMinutes
	WHERE ID = @ID
END
