CREATE PROCEDURE ListUsersByLicenseId
	@LicenseId Guid
AS
BEGIN

SELECT b.UserId, a.UserFirstNm + ' ' + a.UserLastNm AS DisplayName, c.LoginNm, c.RecentLoginD 
FROM EMPLOYEE a, BROKER_USER b, ALL_USER c 
WHERE a.EmployeeUserId=b.UserId 
  AND b.UserId=c.UserId 
  AND b.CurrentLicenseId=@LicenseId

END
