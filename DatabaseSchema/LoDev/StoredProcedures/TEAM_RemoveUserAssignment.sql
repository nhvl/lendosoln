

CREATE PROCEDURE [dbo].[TEAM_RemoveUserAssignment] 
	@TeamId uniqueidentifier, 
	@EmployeeId uniqueidentifier

AS
BEGIN

	DELETE FROM Team_User_Assignment
	WHERE 
	@TeamId = TeamId
	AND 
	@EmployeeId = EmployeeId
	
END


