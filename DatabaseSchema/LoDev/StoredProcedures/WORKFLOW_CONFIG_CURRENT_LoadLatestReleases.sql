CREATE PROCEDURE [dbo].[WORKFLOW_CONFIG_CURRENT_LoadLatestReleases] 
	@BrokerId uniqueidentifier = null 
AS
BEGIN
	SELECT BrokerId, ConfigId, [Version], ConfigXmlKey, CompiledConfig, ModifyingUserId, ReleaseDate
	FROM WORKFLOW_CONFIG_CURRENT
	WHERE @BrokerId IS NULL
		OR BrokerId = @BrokerId
END
