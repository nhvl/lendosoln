ALTER PROCEDURE [dbo].[SCHEDULED_XSLT_EXPORTS_Update]
	@BrokerId uniqueidentifier,
    @ExportId int,
    @ExportName varchar(250),
    @MapName varchar(250) = NULL,
    @UserId uniqueidentifier,
    @ReportId uniqueidentifier,
	@Notes varchar(max),
	@Protocol varchar(10) = '',
    @HostName varchar(500) = '',
    @Login varchar(40) = '',
    @Password varchar(90) = '',
    @Port int = 21,
    @SSHFingerprint varchar(80) = '',
    @DestPath varchar(500) = '',
    @Use_DotNet bit = 1,
    @SecurityMode varchar(11) = 'None',
    @SSLHostFingerprint varchar(80) = '',
    @UsePassive bit = 0,
    @AdditionalPostUrl varchar(200) = '',
    @Comments varchar(500) = '',
	@To varchar(500) = '',
	@From varchar(500) = '',
	@FileName varchar(500) = '',
	@Subject varchar(500) = '',
	@Message varchar(500) = ''
AS
BEGIN TRANSACTION
UPDATE dbo.SCHEDULED_XSLT_EXPORT_SETTINGS 
SET
    [ExportName] = @ExportName,
    [MapName] = COALESCE(@MapName, [MapName]),
    [UserId] = @UserId,
    [ReportId] = @ReportId,
	[Notes] = @Notes
OUTPUT INSERTED.ExportId
WHERE ExportId = @ExportId AND BrokerId = @BrokerId

IF @@error!= 0 GOTO HANDLE_ERROR

UPDATE dbo.SCHEDULED_XSLT_EXPORT_SETTINGS_FTP
SET
    [Protocol] = @Protocol,
    [HostName] = @HostName,
    [Login] = COALESCE(@Login, [Login]),
    [Password] = COALESCE(@Password, [Password]),
    [Port] = @Port,
    [SSHFingerprint] = @SSHFingerprint,
    [DestPath] = COALESCE(@DestPath, DestPath),
    [Use_DotNet] = COALESCE(@Use_DotNet, Use_DotNet),
    [SecurityMode] = COALESCE(@SecurityMode, SecurityMode),
    [SSLHostFingerprint] = COALESCE(@SSLHostFingerprint, [SSLHostFingerprint]),
    [UsePassive] = COALESCE(@UsePassive, [UsePassive]),
    [AdditionalPostUrl] = COALESCE(@AdditionalPostUrl, [AdditionalPostUrl]),
    [Comments] = COALESCE(@Comments, [Comments])
WHERE [ExportId] = @ExportId

IF @@error!= 0 GOTO HANDLE_ERROR

UPDATE dbo.SCHEDULED_XSLT_EXPORT_SETTINGS_EMAIL
SET
    [To] = COALESCE(@To, [To]),
	[From] = COALESCE(@From, [From]),
	[FileName] = COALESCE(@FileName, [FileName]),
	[Subject] = COALESCE(@Subject, [Subject]),
	[Message] = COALESCE(@Message, [Message])
WHERE [ExportId] = @ExportId

IF @@error!= 0 GOTO HANDLE_ERROR
	
COMMIT TRANSACTION
   	
RETURN;
   
HANDLE_ERROR:
    ROLLBACK TRANSACTION
    RAISERROR('Error in [SCHEDULED_XSLT_EXPORTS_Update] sp', 16, 1);;
    RETURN;


