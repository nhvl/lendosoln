CREATE  PROCEDURE ListDiscAccessibleByEmployeeId
	@EmployeeID Guid , @UserId Guid , @BrokerId Guid , @IsForCondition Bit = NULL
AS
select distinct disc.DiscLogId, notif.NotifId, notif.NotifIsValid, DiscDueDate, DiscSubject, DiscRefObjId,DiscRefObjNm1, DiscRefObjNm2, sStatusT AS DiscRefObjStatus, DiscPriority, DiscStatus, DiscCreatedDate, DiscCreatorLoginNm, DiscCreatorUserId, NotifReceiverUserId, ISNULL(DiscDueDate, convert(smalldatetime, '2079-01-01')) AS SortDiscDueDate
FROM LOAN_FILE_CACHE loan WITH(NOLOCK) inner join discussion_log disc WITH(NOLOCK) on disc.discrefobjid = loan.slid 
		                       left join discussion_notification notif WITH(NOLOCK) on disc.disclogid = notif.disclogid and notif.notifreceiveruserid = @UserId
where (loan.sEmployeeLoanRepId=@EmployeeId
       OR loan.sEmployeeProcessorId = @EmployeeId
       OR loan.sEmployeeManagerId = @EmployeeId
       OR loan.sEmployeeCallCenterAgentId = @EmployeeId
       OR loan.sEmployeeLoanOpenerId = @EmployeeId
       OR loan.sEMployeeRealEstateAgentId = @EmployeeId
       OR loan.sEmployeeLenderAccExecId = @EmployeeId
       OR loan.sEmployeeUnderwriterId = @EmployeeId
       OR loan.sEmployeeLockDeskId = @EmployeeId
      )
      AND
disc.discbrokerid = @brokerId and disc.discisrefobjvalid = 1 and disc.IsForCondition = COALESCE( @IsForCondition , disc.IsForCondition ) and ISNULL( loan.IsTemplate , 0 ) = 0 and disc.IsValid = 1
order by disc.discrefobjnm1 asc, disc.discpriority asc
