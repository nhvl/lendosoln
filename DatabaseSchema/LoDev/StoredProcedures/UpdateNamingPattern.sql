CREATE PROCEDURE UpdateNamingPattern
	@BrokerID Guid,
	@NamingPattern varchar(50)
AS
UPDATE Broker
       SET NamingPattern = @NamingPattern
WHERE BrokerID = @BrokerID
