
ALTER PROCEDURE [dbo].[RetrievePricingGroupById] 
	@LpePriceGroupId Guid,
	@BrokerId Guid
AS
SELECT 
	LpePriceGroupId, ActualPriceGroupId
	, BrokerId
		, ExternalPriceGroupEnabled
		, LpePriceGroupName
		, LenderPaidOriginatorCompensationOptionT
		, LockPolicyID
		, DisplayPmlFeeIn100Format
		, IsRoundUpLpeFee
		, RoundUpLpeFeeToInterval
		, ContentKey
		, IsAlwaysDisplayExactParRateOption
		
FROM LPE_Price_Group
WHERE LpePriceGroupId = @LpePriceGroupId
      AND BrokerId = @BrokerId

