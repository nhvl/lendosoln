

-- =============================================
-- Author:		Antonio Valencia
-- Create date: may 17, 2011
-- Description:	Changes the edoc status
-- =============================================
CREATE PROCEDURE [dbo].[EDOCS_UpdateStatus] 
	@DocId uniqueidentifier, 
	@ImageStatus tinyint,
	@TimeToRequeueForImageGeneration datetime = null
	
AS
BEGIN
	UPDATE EDOCS_DOCUMENT
	SET 
		ImageStatus  = @ImageStatus,
		TimeToRequeueForImageGeneration  = @TimeToRequeueForImageGeneration
	WHERE
		DocumentId = @DocId 
		
END


