/*
	Author: Scott Kibler
	Created Date: 1/4/2015
	Decription:
		Remove all teams assigned to the given employee for the given role. 
*/
CREATE PROCEDURE [dbo].[TEAM_USER_ASSIGNMENT_RemoveForRoleAndEmployee]
	@BrokerID		uniqueidentifier,
	@RoleID			uniqueidentifier,
	@EmployeeID		uniqueidentifier
AS	
BEGIN
	DELETE 
			tua
	FROM 
			TEAM_USER_ASSIGNMENT tua
			LEFT JOIN TEAM t
			ON t.id = tua.TeamID
	WHERE 
			tua.EmployeeID = @EmployeeID
		AND t.RoleID = @RoleID
		AND t.BrokerID = @BrokerID 
END	
