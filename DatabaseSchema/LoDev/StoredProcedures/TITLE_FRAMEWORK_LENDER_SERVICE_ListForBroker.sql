ALTER PROCEDURE [dbo].[TITLE_FRAMEWORK_LENDER_SERVICE_ListForBroker]
    @BrokerId uniqueidentifier
AS BEGIN
    SELECT * FROM dbo.TITLE_FRAMEWORK_LENDER_SERVICE
    WHERE 
        BrokerId = @BrokerId
END
