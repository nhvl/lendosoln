-- =============================================
-- Author:		Isaac Ribakoff
-- Create date: 9/18/2014
-- Description:	Lists MI Vendor Credentials For Branch
-- =============================================
ALTER PROCEDURE [dbo].[MORTGAGE_INSURANCE_VENDOR_BRANCH_SETTINGS_ListVendors]
	@BranchId UniqueIdentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SELECT b.VendorId, b.UserName, b.Password, b.EncryptionKeyId, b.AccountId, b.PolicyId, b.IsDelegated
FROM MORTGAGE_INSURANCE_VENDOR_BRANCH_SETTINGS b
WHERE b.BranchId = @BranchId
  
END
