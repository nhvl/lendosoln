-- =============================================
-- Author:		Scott Kibler
-- Create date: October 6, 2016
-- Description:	Gets most recent configuration for an organization
-- =============================================
ALTER PROCEDURE [dbo].[CONFIG_RELEASED_GetLatestId] 
	@OrgId uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SELECT TOP 1 ConfigId
	FROM CONFIG_RELEASED 
	WHERE OrgId = @OrgId
	ORDER BY ReleaseDate DESC

END