-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE SignatureTemplate_CheckName
	@TemplateId UniqueIdentifier,
	@BrokerId UniqueIdentifier,
	@OwnerUserId UniqueIdentifier,
	@Name varchar(100)
AS
BEGIN
	SELECT TemplateId FROM LO_SIGNATURE_TEMPLATE
	WHERE BrokerId = @BrokerId AND OwnerUserId = @OwnerUserId AND Name = @Name AND TemplateId <> @TemplateId
END
