CREATE PROCEDURE ListDiscTracksByCreatorUserIdLoanId
	@UserId uniqueidentifier,
	@LoanId uniqueidentifier
AS
select tr.TrackId, tr.TrackCreatedDate,tr.NotifId,tr.DiscLogId, tr.TrackNotifIsRead, notif.NotifReceiverUserId, notif.NotifReceiverLoginNm, notif.NotifIsValid, notif.NotifInvalidDate, disc.DiscRefObjId, disc.DiscRefObjNm1, disc.DiscRefObjNm2, disc.DiscSubject, disc.DiscPriority, disc.DiscStatus, disc.DiscDueDate,DiscCreatorLoginNm
from ( track tr join discussion_notification notif on tr.notifid = notif.notifid ) join discussion_log disc on tr.disclogid = disc.disclogid
where tr.TrackIsValid = 1 and tr.trackcreatoruserid = @userid and disc.DiscRefObjId = @LoanId and disc.IsValid = 1
