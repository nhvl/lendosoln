CREATE PROCEDURE [dbo].[QSend] (
	@QueueId int,
	@Subject1 varchar(300),
	@Subject2 varchar(300), 
	@Data varchar(5500),
	@Priority tinyint = 2,
	@DataLoc varchar(10), 
	@EnforceUniqueSubject1 bit = 0, 
	@MessageId bigint OUTPUT
)


AS 
	IF @QueueId = 2 AND @Subject1 LIKE '%(QUICKPRICER[_]%' AND @Subject1 LIKE '%): Loan status has changed'
	BEGIN
		SELECT @MessageId = 1
		return 0
	END
	
	-- We need a better way to do this
	IF @EnforceUniqueSubject1 = 1  
	BEGIN
		SELECT TOP 1 * FROM Q_MESSAGE
		WHERE QUEUEID = @QueueId AND Subject1 = @Subject1 
		
		IF @@rowcount > 0 
		BEGIN 
			SELECT @MessageId = 1
			RETURN 0
		END		
	END
	
	DECLARE @error int 
	INSERT INTO Q_MESSAGE(QueueId, Subject1, Data, Priority, Subject2, DataLoc) 
		VALUES(@QueueId, @Subject1, @Data, @Priority, @Subject2, @DataLoc)
	SELECT @error = @@error
	if @error <> 0 
	BEGIN
		RAISERROR('Unexpected Error Sending', 16, 1);  
		IF @@TRANCOUNT > 0 
			ROLLBACK TRANSACTION 
		RETURN -1
	END
	SELECT @MessageId = SCOPE_IDENTITY();
	RETURN 0




