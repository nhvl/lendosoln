

CREATE PROCEDURE [dbo].[ListCustomFormByOwnerEmployeeId] 
	@BrokerID uniqueidentifier,
	@OwnerEmployeeId uniqueidentifier
AS
	SELECT CustomLetterID, Title, UserFirstNm + ' ' + UserLastNm AS EmployeeName
	FROM Custom_Letter c , Employee e
	WHERE c.BrokerId = @BrokerID AND c.ContentType = 'WORD' 
	AND c.OwnerEmployeeId = @OwnerEmployeeId
	AND c.OwnerEmployeeId = e.EmployeeId


