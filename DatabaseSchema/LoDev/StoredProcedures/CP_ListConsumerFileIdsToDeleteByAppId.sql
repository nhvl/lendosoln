

-- List all fileDB entries that are associated with this application,
-- but do not have an eVault entry.
CREATE PROCEDURE [dbo].[CP_ListConsumerFileIdsToDeleteByAppId]
	@aAppId uniqueidentifier
AS

SELECT 
PdfFileDbKey
FROM
CP_Consumer_Action_Request
WHERE 
@aAppId = aAppId
AND
PdfFileDbKey <> '00000000-0000-0000-0000-000000000000'
AND
PdfFileDbKey NOT IN (SELECT PdfFileDbKey FROM CP_Signed_Event)



