ALTER PROCEDURE [dbo].[RetrieveUserNamesByUserId]
	@BrokerId uniqueidentifier,
	@UserId uniqueidentifier
AS
BEGIN	
	SELECT LoginNm, UserFirstNm, UserLastNm
	FROM VIEW_EMPLOYEE_CONTACT_INFO
	WHERE UserId = @UserId and BrokerId = @BrokerId
END
