ALTER PROCEDURE [dbo].CUSTOM_FOLDER_RETRIEVE_BY_BROKERID
	@BrokerId uniqueidentifier
AS
BEGIN
	SELECT FolderId, EmployeeId, InternalName, ExternalName, LeadLoanStatus, SortOrder, ParentFolderId
	FROM CUSTOM_FOLDER
	WHERE BrokerId = @BrokerId AND EmployeeId is NULL
END
