-- =============================================
-- Author:		Eric Mallare
-- Create date: 5/8/2017
-- Description:	Deletes a lender service.
-- =============================================
ALTER PROCEDURE [dbo].[VERIFICATION_LENDER_SERVICE_Delete]
	@LenderServiceId int,
	@BrokerId uniqueidentifier
AS
BEGIN TRANSACTION
	EXEC VOA_LENDER_ENABLED_OPTION_DeleteAllOptionsByLenderService @BrokerId, @LenderServiceId
	IF @@ERROR != 0 GOTO HANDLE_ERROR

	DELETE FROM
		VERIFICATION_LENDER_SERVICE
	WHERE
		LenderServiceId=@LenderServiceId AND
		BrokerId=@BrokerId
	IF @@ERROR != 0 GOTO HANDLE_ERROR
	
	COMMIT TRANSACTION
RETURN

HANDLE_ERROR:
	ROLLBACK TRANSACTION
	RETURN
