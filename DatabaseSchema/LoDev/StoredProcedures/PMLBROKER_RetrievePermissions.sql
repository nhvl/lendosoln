-- =============================================
-- Author:		Michael Leinweaver
-- Create date: 3/8/2016
-- Description:	Retrieves permissions for an OC.
-- =============================================
ALTER PROCEDURE [dbo].[PMLBROKER_RetrievePermissions]
	@BrokerId uniqueidentifier,
	@PmlBrokerId uniqueidentifier
AS
BEGIN
	SELECT *
	FROM PML_BROKER_PERMISSIONS
	WHERE BrokerId = @BrokerId AND  PmlBrokerId = @PmlBrokerId
END
