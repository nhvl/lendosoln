CREATE PROCEDURE GetCountyCityStateByZip
	@Zipcode char(5)
AS
	SELECT County, City, State FROM CityCountyStateZip WITH (NOLOCK) WHERE Zipcode = @Zipcode
