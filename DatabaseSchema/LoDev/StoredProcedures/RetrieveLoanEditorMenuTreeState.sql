CREATE PROCEDURE RetrieveLoanEditorMenuTreeState 
	@UserID Guid
AS
	SELECT LoanEditorMenuTreeStateXmlContent FROM Broker_User WITH (NOLOCK) WHERE UserID= @UserID
