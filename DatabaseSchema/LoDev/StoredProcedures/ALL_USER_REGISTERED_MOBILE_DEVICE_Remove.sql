-- =============================================
-- Author:		Huy Nguyen	
-- Create date: 5/5/2015
-- Description:	
-- =============================================
CREATE PROCEDURE dbo.ALL_USER_REGISTERED_MOBILE_DEVICE_Remove
	@Id BigInt ,
	@BrokerId UniqueIdentifier,
    @UserId UniqueIdentifier
AS
BEGIN
	UPDATE ALL_USER_REGISTERED_MOBILE_DEVICE
	   SET IsActive = 0,
			DeletedD = GETDATE()
    WHERE Id = @Id
	AND  BrokerId = @BrokerId
	AND UserId = @UserId
END
