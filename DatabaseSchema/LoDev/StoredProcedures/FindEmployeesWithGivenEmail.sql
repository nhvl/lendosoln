CREATE PROCEDURE [dbo].[FindEmployeesWithGivenEmail]
	@Email varchar(320)
AS
	SELECT EmployeeId FROM BROKER_USER WHERE SupportEmail = @Email
