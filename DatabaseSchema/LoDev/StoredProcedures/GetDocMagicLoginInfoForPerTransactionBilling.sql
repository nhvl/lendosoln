-- =============================================
-- Author:		Scott Kibler
-- Create date: 3/29/2012
-- Description:	Given per transaction billing, (i.e. doesn't check.)
--      find the docmagic login info
--		according to 76166 (employee, then branch, then broker)
--   assumes:
--		- userid, branchid, and brokerid are corresponding.
--		- username and password are either both filled in or empty.

-- Updated: 3/30/2012

-- =============================================
CREATE PROCEDURE [dbo].[GetDocMagicLoginInfoForPerTransactionBilling] 
	-- Add the parameters for the stored procedure here
	@UserId uniqueidentifier,
	@BranchId uniqueidentifier,
	@BrokerId uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--return values
	declare @dmun varchar(50)
	declare @dmpw varchar(150)
	declare @dmcid varchar(50)
	
	--user settings, highest priority
	declare @userdmcid varchar(50)
	declare @userdmun varchar(50)
	declare @userdmpw varchar(150)

	--branch settings, middle
	declare @branchdmun varchar(50)
	declare @branchdmpw varchar(150)
	declare @branchdmcid varchar(50)
	
	--broker settings, lowest.
	declare @brokerdmcid varchar(50)
	declare @brokerdmun varchar(50)
	declare @brokerdmpw varchar(150)
	
	select @userdmun = [Login], @userdmpw = [Password], @userdmcid = AccountId
	from DOCUMENT_VENDOR_USER_ENABLED_CREDENTIALS
	where UserId=@UserId AND VendorId = '00000000-0000-0000-0000-000000000000'
	
	select @branchdmun = [Login], @branchdmpw = [Password], @branchdmcid = AccountId
	from DOCUMENT_VENDOR_BRANCH_CREDENTIALS
	where BranchId = @BranchId AND VendorId = '00000000-0000-0000-0000-000000000000' 
	
	select @brokerdmun = [Login], @brokerdmpw = [Password], @brokerdmcid = AccountId
	from DOCUMENT_VENDOR_BROKER_SETTINGS
	where BrokerId = @BrokerId AND VendorId = '00000000-0000-0000-0000-000000000000'
	
	set @dmun = @userdmun;
	set @dmpw = @userdmpw;
	set @dmcid = @userdmcid;
	if ISNULL(@userdmun, '') = ''
	begin
		set @dmun = @branchdmun;
		set @dmpw = @branchdmpw;
		set @dmcid = @branchdmcid;
		if ISNULL(@branchdmun, '') = ''
		begin
			set @dmun = @brokerdmun;
			set @dmpw = @brokerdmpw;
			set @dmcid = @brokerdmcid;
			if ISNULL(@brokerdmun, '') = '' RAISERROR ('couldnt even find docmagic login info at broker level.',16,1)
		end
	end
	
	if ISNULL(@dmcid, '') = ''
	begin
		set @dmcid = @branchdmcid;
		if ISNULL(@branchdmcid, '') = ''
		begin
			set @dmcid = @brokerdmcid;
			if ISNULL(@brokerdmcid, '') = '' RAISERROR ('couldnt even find docmagic customer id at broker level.',16,1)
		end
	end	

	select 'DocMagicUserName'=@dmun, 'DocMagicPassword'=@dmpw, 'DocMagicCustomerId'=@dmcid
END