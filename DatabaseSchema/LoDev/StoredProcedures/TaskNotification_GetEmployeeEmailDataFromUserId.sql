-- =============================================
-- Author:		Michael Leinweaver
-- Create date: 6/12/2017
-- Description:	Retrieves task email information for a user.
-- =============================================
ALTER PROCEDURE [dbo].[TaskNotification_GetEmployeeEmailDataFromUserId]
	@BrokerId uniqueidentifier,
	@UserId uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
		v.Type,
		v.TaskRelatedEmailOptionT as 'EmployeeTaskEmailOption', 
		v.PopulatePMLPermissionsT,
		p.TaskRelatedEmailOptionT as 'OriginatingCompanyTaskEmailOption',
		v.Email
	FROM 
		VIEW_EMPLOYEE_CONTACT_INFO v
		LEFT JOIN PML_BROKER_PERMISSIONS p ON v.PmlBrokerId = p.PmlBrokerId
	WHERE v.BrokerId = @BrokerId AND v.UserId = @UserId
END
