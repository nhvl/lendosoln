
CREATE PROCEDURE [dbo].[CreateCustomForm] 
	@CustomLetterID uniqueidentifier,
	@BrokerID uniqueidentifier,
	@Title varchar(36),
	@FileDBKey uniqueidentifier,
	@OwnerEmployeeId uniqueidentifier = null
AS
	INSERT INTO Custom_Letter(CustomLetterID, BrokerId, OwnerEmployeeId, Title, FileDBKey, ContentType)
	                           VALUES(@CustomLetterID, @BrokerID, @OwnerEmployeeId, @Title, @FileDBKey, 'WORD')

