-- =============================================
-- Author:		Geoffrey Feltman
-- Create date: 6/11/14
-- Description:	Create a new pending document request.
-- =============================================
CREATE PROCEDURE [dbo].[PENDING_DOCUMENT_REQUEST_Create]
    @Id int OUTPUT,
    @LoanId uniqueidentifier,
    @ApplicationId uniqueidentifier,
    @BrokerId uniqueidentifier,
    @RequestType int,
    @Description varchar(100),
    @ScheduledSendTime smalldatetime,
    @CreatorEmployeeId uniqueidentifier,
    @FileDbKey varchar(100),
    @EDocId uniqueidentifier,
    @EDocVersion int,
    @DocumentLayoutXml varchar(max),
    @DocTypeId int,
    @IsESignAllowed bit,
    @IsRequestBorrowerSignature bit,
    @IsRequestCoborrowerSignature bit,
    @CoborrowerTitleBorrowerId uniqueidentifier,
    @ErrorMessage varchar(100)
AS
BEGIN
    DECLARE @TempIdTable TABLE (Id int)
	
	INSERT INTO PENDING_DOCUMENT_REQUEST 
	(
		LoanId,
		ApplicationId,
		BrokerId,
		RequestType,
		Description,
		ScheduledSendTime,
		CreatorEmployeeId,
		FileDbKey,
		EDocId,
		EDocVersion,
		DocumentLayoutXml,
		DocTypeId,
		IsESignAllowed,
		IsRequestBorrowerSignature,
		IsRequestCoborrowerSignature,
		CoborrowerTitleBorrowerId,
		ErrorMessage
	)
	OUTPUT Inserted.Id INTO @TempIdTable
	VALUES
	(
		@LoanId,
		@ApplicationId,
		@BrokerId,
		@RequestType,
		@Description,
		@ScheduledSendTime,
		@CreatorEmployeeId,
		@FileDbKey,
		@EDocId,
		@EDocVersion,
		@DocumentLayoutXml,
		@DocTypeId,
		@IsESignAllowed,
		@IsRequestBorrowerSignature,
		@IsRequestCoborrowerSignature,
		@CoborrowerTitleBorrowerId,
		@ErrorMessage
	)

	SELECT @Id = Id FROM @TempIdTable
	
END
