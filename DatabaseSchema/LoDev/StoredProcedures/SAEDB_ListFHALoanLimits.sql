-- =============================================
-- Author:		Michael Leinweaver
-- Create date: 1/22/2016
-- Description:	Obtains the list of FHA loan limits.
-- =============================================
ALTER PROCEDURE [dbo].[SAEDB_ListFHALoanLimits] 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT FipsCountyCode,Limit1Units, Limit2Units, Limit3Units,Limit4Units,LastRevised 
	FROM FHA_LOAN_LIMITS
END