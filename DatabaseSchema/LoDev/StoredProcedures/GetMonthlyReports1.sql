
-- =============================================
-- Author:		Long Nguyen
-- Create date: 8/4/2015
-- Description:	Update schedule report recovery (iOPM 215368): Get reports at current hour
-- =============================================
CREATE procedure [dbo].[GetMonthlyReports1] 
as 
BEGIN
	select * from MONTHLY_REPORTS 
		where NextRun >= dateadd(day, -14, getdate())
			AND NextRun <= dateadd(minute, 20, getdate())
			AND Weekday IS NOT NULL
		order by ReportId
END
