CREATE PROCEDURE [dbo].[INTERNAL_USE_ONLY_RetrieveAllUnassignedDocTypes]

as
	select DocTypeId, BrokerId 
	from edocs_document_type 
	where folderid is null
	and
	BrokerId is not null