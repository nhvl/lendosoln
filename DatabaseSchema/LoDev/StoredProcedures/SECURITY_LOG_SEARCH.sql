-- =============================================
-- Author:		Justin Kim
-- Create date: 7/23/18
-- Description:	Search Security Event Log
-- =============================================
ALTER PROCEDURE [dbo].[SECURITY_LOG_SEARCH]
	@BrokerId uniqueidentifier,
	@EventType smallint = null,
	@UserId uniqueidentifier = null,
	@FromDate datetime2 = null,
	@ToDate datetime2 = null,
	@ClientIp varchar(50) = null,
	@Offset int,
	@SearchResultsPerPage int
AS
BEGIN
	WITH SearchResults(eventType, descriptionText, userFirstNm, userLastNm, loginNm, isInternalUserAction, isSystemAction, createdDate, clientIP, principalType) as (
	SELECT EventType, DescriptionText, UserFirstNm, UserLastNm, LoginNm, IsInternalUserAction, IsSystemAction, CreatedDate, ClientIP, PrincipalType
	FROM SECURITY_LOG
	WHERE 
		BrokerId = @BrokerId AND
		(EventType= @EventType OR @EventType IS NULL) AND 
		(UserId = @UserId OR @UserId IS NULL) AND 
		(CreatedDate >= @FromDate OR @FromDate IS NULL) AND
		(CreatedDate <= @ToDate OR @ToDate IS NULl) AND
		(ClientIP LIKE '%' + @ClientIp + '%' OR @ClientIp IS NULL)		
	), TempCount AS (
		SELECT COUNT(*) AS MaxRows FROM SearchResults
	)
	SELECT MaxRows, EventType, DescriptionText, UserFirstNm, UserLastNm, LoginNm, IsInternalUserAction, IsSystemAction, CreatedDate, ClientIP, PrincipalType
	FROM SearchResults, TempCount
	ORDER BY SearchResults.createdDate DESC
	OFFSET @Offset ROWS
	FETCH NEXT @SearchResultsPerPage ROWS ONLY
END