ALTER PROCEDURE [dbo].[SaveSecurityQuestionsAnswers] 
	@UserId uniqueidentifier,
	@Question1Id int,
	@Answer1 varchar(20) = null,
	@EncryptedAnswer1 varbinary(50) = 0x,
	@Question2Id int,
	@Answer2 varchar(20) = null,
	@EncryptedAnswer2 varbinary(50) = 0x,
	@Question3Id int,
	@Answer3 varchar(20) = null,
	@EncryptedAnswer3 varbinary(50) = 0x
AS
BEGIN
	UPDATE BROKER_USER
	SET
		SecurityQuestion1Id = @Question1Id,
		SecurityQuestion1Answer = COALESCE(@Answer1, SecurityQuestion1Answer),
		SecurityQuestion1EncryptedAnswer = @EncryptedAnswer1,
		SecurityQuestion2Id = @Question2Id,
		SecurityQuestion2Answer = COALESCE(@Answer2, SecurityQuestion2Answer),
		SecurityQuestion2EncryptedAnswer = @EncryptedAnswer2,
		SecurityQuestion3Id = @Question3Id,
		SecurityQuestion3Answer = COALESCE(@Answer3, SecurityQuestion3Answer),
		SecurityQuestion3EncryptedAnswer = @EncryptedAnswer3
	WHERE UserId = @UserId
	IF @@ROWCOUNT = 0 RETURN -1
	RETURN 1
END
