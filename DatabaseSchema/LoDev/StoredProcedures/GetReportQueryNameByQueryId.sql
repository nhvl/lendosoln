CREATE PROCEDURE GetReportQueryNameByQueryId
	@QueryId uniqueidentifier
AS
	SELECT QueryName
	FROM REPORT_QUERY with(nolock)
	WHERE QueryId = @QueryID
