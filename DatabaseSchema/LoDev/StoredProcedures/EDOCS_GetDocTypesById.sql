ALTER procedure [dbo].[EDOCS_GetDocTypesById]
	@DocTypeId int
as
select DocTypeName, DocTypeId, BrokerId, FolderId, ClassificationId, DefaultForPageId, ESignTargetDocTypeId
from EDOCS_DOCUMENT_TYPE doctype LEFT JOIN 
     EDOCS_DOCUMENT_TYPE_MAPPING map ON doctype.DocTypeId = map.SourceDocTypeId
where @DocTypeId = DocTypeId
