CREATE PROCEDURE UpdateLoanEditorMenuTreeState 
	@UserID Guid,
	@LoanEditorMenuTreeStateXmlContent Text
AS
	UPDATE Broker_User
	SET LoanEditorMenuTreeStateXmlContent = @LoanEditorMenuTreeStateXmlContent
	WHERE UserID = @UserID
