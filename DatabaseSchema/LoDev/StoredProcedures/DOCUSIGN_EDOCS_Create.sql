-- =============================================
-- Author:		Eric Mallare
-- Create date: 1/23/2018
-- Description:	Inserts a DocuSign Edoc record into the DB.
-- =============================================
ALTER PROCEDURE [dbo].[DOCUSIGN_EDOCS_Create]
	@EnvelopeReferenceId int,
	@SequenceNumber int,
	@FromDocuSign bit,
	@EdocId uniqueidentifier = null,
	@Name varchar(200),
	@DocumentId int = NULL,
	@IsCertificate bit = 0,
	@Id int = null OUTPUT
AS
BEGIN
	INSERT INTO DOCUSIGN_EDOCS
		(EnvelopeReferenceId,
		 SequenceNumber,
		 FromDocuSign,
		 EdocId,
		 Name,
		 DocumentId,
		 IsCertificate)
	VALUES
		(@EnvelopeReferenceId,
		 @SequenceNumber,
		 @FromDocuSign,
		 @EdocId,
		 @Name,
		 @DocumentId,
		 @IsCertificate)

	SET @Id=SCOPE_IDENTITY();
END