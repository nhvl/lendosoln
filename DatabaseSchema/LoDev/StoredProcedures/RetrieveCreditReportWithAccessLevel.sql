
CREATE PROCEDURE [dbo].[RetrieveCreditReportWithAccessLevel] 
	@ApplicationID uniqueidentifier
AS
	SELECT TOP 1 ExternalFileID, DbFileKey, ComId, crap.CrAccProxyId, AllowPmlUserToViewReports
	FROM SERVICE_FILE sf LEFT JOIN CREDIT_REPORT_ACCOUNT_PROXY crap on sf.CrAccProxyId = crap.CrAccProxyId
	WHERE Owner = @ApplicationID
      AND IsInEffect = 1
	  AND ServiceType='Credit'
	  AND FileType='CreditReport'
	ORDER BY TimeVersion DESC

	if( 0!=@@error)
	begin
		RAISERROR('Error in the select statement in RetrieveCreditReportWithAccessLevel sp', 16, 1);
		return -100;
	end
	return 0;

