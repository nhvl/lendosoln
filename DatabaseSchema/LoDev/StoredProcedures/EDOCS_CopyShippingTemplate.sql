CREATE procedure [dbo].[EDOCS_CopyShippingTemplate]
	@ShippingTemplateId int,
	@ShippingTemplateName varchar(50),
	@BrokerId uniqueidentifier
as

declare @CopyShippingTemplateId as int

if exists(select 1
			from EDOCS_SHIPPING_TEMPLATE
			where ShippingTemplateName = @ShippingTemplateName
				and BrokerId = @BrokerId)
begin 
	RAISERROR('A shipping template with the same name already exists. Please select another name.', 16, 1);
	return;
end

BEGIN TRANSACTION

	insert EDOCS_SHIPPING_TEMPLATE (BrokerId, ShippingTemplateName)
		values (@BrokerId, @ShippingTemplateName)
	IF @@error!= 0 GOTO HANDLE_ERROR

	select @CopyShippingTemplateId = ShippingTemplateId 
		from EDOCS_SHIPPING_TEMPLATE 
		where ShippingTemplateName = @ShippingTemplateName
			and BrokerId = @BrokerId
	IF @@error!= 0 GOTO HANDLE_ERROR

	insert EDOCS_DOCUMENT_TYPE_X_SHIPPING_TEMPLATE (ShippingTemplateId, DocTypeId, StackingOrder)
		select @CopyShippingTemplateId, DocTypeId, StackingOrder
			from EDOCS_DOCUMENT_TYPE_X_SHIPPING_TEMPLATE
			where ShippingTemplateId = @ShippingTemplateId
	IF @@error!= 0 GOTO HANDLE_ERROR

COMMIT TRANSACTION
RETURN

HANDLE_ERROR:
    ROLLBACK TRANSACTION
    RETURN 