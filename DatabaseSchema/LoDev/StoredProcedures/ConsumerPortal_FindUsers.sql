-- =============================================
-- Author:		paoloa
-- Create date: 6/11/2013
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[ConsumerPortal_FindUsers]
	-- Add the parameters for the stored procedure here
	@BrokerNm varchar(128) = null,
	@Email varchar(80) = null, 
	@Id varchar(16) = null,
	@Name varchar(80) = null
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT u.Id, u.FirstName, u.LastName, u.Email, u.BrokerId, u.LastLoginD, b.BrokerNm
	FROM CONSUMER_PORTAL_USER u WITH (NOLOCK)
		JOIN Broker as b WITH (NOLOCK) on u.brokerid = b.brokerid
	WHERE
		((@BrokerNm is null) OR (b.BrokerNm LIKE @BrokerNm + '%'))
		AND
		((@Email is null) OR (u.Email LIKE @Email + '%'))
		AND
		((@Id is null) OR (u.Id = @Id))
		AND
		((@Name is null) OR ((u.FirstName +' '+ u.LastName) LIKE '%'+@Name+'%'))
END
