CREATE PROCEDURE [dbo].[EDOCS_ListBrokerIdsThatHaveFolderName]
@FolderName varchar(50)
as
select BrokerId, FolderId
FROM
EDOCS_FOLDER with (nolock)
where
FolderName = @FolderName

