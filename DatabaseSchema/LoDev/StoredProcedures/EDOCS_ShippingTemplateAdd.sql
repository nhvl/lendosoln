CREATE procedure [dbo].[EDOCS_ShippingTemplateAdd]
	@ShippingTemplateName varchar(50),
	@List varchar(max),
	@BrokerId uniqueidentifier
as

declare @ShippingTemplateId as int;

if exists(select 1
			from EDOCS_SHIPPING_TEMPLATE 
			where ShippingTemplateName = @ShippingTemplateName
				and (BrokerId = @BrokerId or BrokerId is null))
begin
	RAISERROR('A shipping template with the same name already exists. Please select another name.', 16, 1);
	return;
end

BEGIN TRANSACTION

	insert EDOCS_SHIPPING_TEMPLATE (BrokerId, ShippingTemplateName)
		values (@BrokerId, @ShippingTemplateName)
	IF @@error!= 0 GOTO HANDLE_ERROR

	select @ShippingTemplateId = ShippingTemplateId 
		from EDOCS_SHIPPING_TEMPLATE 
		where ShippingTemplateName = @ShippingTemplateName
			and BrokerId = @BrokerId 
	IF @@error!= 0 GOTO HANDLE_ERROR

	declare @start	int,
			@end    int,
			@length int,
			@order	int

	select @start = 0, 
		   @end = 1, 
		   @order = 1

	while @end > 0
	begin
		select @end = charindex(',', @List, @start + 1)
		select @length = @end - @start - 1
		if (@length > 0)
		begin
			insert EDOCS_DOCUMENT_TYPE_X_SHIPPING_TEMPLATE (ShippingTemplateId, DocTypeId, StackingOrder)
				values (@ShippingTemplateId, convert(int, substring(@List, @start + 1, @length)), @order)
			IF @@error!= 0 GOTO HANDLE_ERROR
		end
		select @order = @order + 1
		select @start = @end
	end

COMMIT TRANSACTION
RETURN

HANDLE_ERROR:
    ROLLBACK TRANSACTION
    RETURN 