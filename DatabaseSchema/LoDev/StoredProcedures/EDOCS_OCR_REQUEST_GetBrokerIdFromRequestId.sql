-- =============================================
-- Author:		Michael Leinweaver
-- Create date: 9/13/2017
-- Description:	Retrieves the broker ID associated with an OCR request.
-- =============================================
CREATE PROCEDURE [dbo].[EDOCS_OCR_REQUEST_GetBrokerIdFromRequestId]
	@OcrRequestId uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT BrokerId
    FROM EDOCS_OCR_REQUEST
    WHERE OcrRequestId = @OcrRequestId
END
