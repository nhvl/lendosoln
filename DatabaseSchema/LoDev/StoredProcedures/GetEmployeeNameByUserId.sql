

-- =============================================
-- Author:		Matthew Pham
-- Create date: April 25, 2011
-- Description:	Get an employee's name by their userID
-- =============================================
CREATE PROCEDURE [dbo].[GetEmployeeNameByUserId]
	@EmployeeUserID uniqueidentifier
AS
BEGIN
	SELECT UserLastNm + ', ' + UserFirstNm AS FullName,
	UserFirstNm + ' ' + UserLastNm as FirstLast,
	Email

	
	FROM Employee WITH (NOLOCK)
	WHERE EmployeeUserID = @EmployeeUserID
END


