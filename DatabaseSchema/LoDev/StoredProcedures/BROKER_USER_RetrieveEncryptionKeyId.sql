-- =============================================
-- Author:		Timothy Jewell
-- Create date: 8/2018
-- Description:	Retrieves the EncryptionKeyId for
-- a specified user.
-- =============================================
ALTER PROCEDURE [dbo].[BROKER_USER_RetrieveEncryptionKeyId]
	@UserId uniqueidentifier
AS
BEGIN
	SET NOCOUNT ON;
	SELECT
		EncryptionKeyId
	FROM BROKER_USER
	WHERE UserId = @UserId
END
