CREATE PROCEDURE GetCustomerCodeByBrokerId
	@BrokerId Guid
AS
BEGIN
	SELECT CustomerCode FROM Broker WHERE BrokerId=@BrokerId
END
