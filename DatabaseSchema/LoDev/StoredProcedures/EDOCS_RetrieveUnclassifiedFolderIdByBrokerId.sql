CREATE PROCEDURE [dbo].[EDOCS_RetrieveUnclassifiedFolderIdByBrokerId]
@BrokerId uniqueidentifier
as
	select FolderId from edocs_folder where brokerid = @BrokerId