CREATE PROCEDURE [dbo].[GetUnObsoleteableMiQuoteEdocIdsByQuoteOrderNumber]
	@sLId uniqueidentifier,
	@BrokerId uniqueidentifier,
	@MiQuoteDocTypeId int,
	@MiQuoteOrderNumber varchar(50)
AS
BEGIN
	SELECT e.DocumentId
	FROM EDOCS_DOCUMENT e
	JOIN MORTGAGE_INSURANCE_ORDER_INFO_X_EDOCS_DOCUMENT oxe
	ON oxe.DocumentId = e.DocumentId
	JOIN MORTGAGE_INSURANCE_ORDER_INFO o
	ON oxe.OrderNumber = o.OrderNumber
	WHERE e.sLId = @sLId
	AND e.BrokerId = @BrokerId
	AND e.IsValid = 1
	AND e.DocTypeId = @MiQuoteDocTypeId
	AND (e.[Status] = 1 /*Obsolete*/ OR e.[Status] = 3 /*Rejected*/)
	AND o.IsQuote = 1
	AND o.OrderNumber = @MiQuoteOrderNumber
END