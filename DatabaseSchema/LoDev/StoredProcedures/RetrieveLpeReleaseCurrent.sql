ALTER PROCEDURE dbo.RetrieveLpeReleaseCurrent
AS
select top 1 *
from lpe_release
WHERE SnapshotId <> 'ManualImport'
order by releaseversion desc
