CREATE PROCEDURE [dbo].[RetrieveQueryIdByQueryNameAndEmployeeId]
	@EmployeeId uniqueidentifier,
	@QueryName varchar(100)
AS
BEGIN

	SELECT QueryId
	FROM Report_Query
	WHERE EmployeeId = @EmployeeId
	AND
	QueryName = @QueryName
END
