-- =============================================
-- Author:		Matthew Pham
-- Create date: 9/1/11
-- Description:	Delete a trade for MBS trades
-- =============================================
CREATE PROCEDURE MBS_DeleteTrade
	-- Add the parameters for the stored procedure here
	@TradeId uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM MBS_TRADE
	WHERE TradeId = @TradeId
END
