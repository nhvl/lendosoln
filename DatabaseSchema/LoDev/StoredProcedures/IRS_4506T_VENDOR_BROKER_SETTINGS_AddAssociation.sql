-- =============================================
-- Author:		David Dao
-- Create date: Aug 6, 2013
-- Description:	
-- =============================================
CREATE PROCEDURE dbo.IRS_4506T_VENDOR_BROKER_SETTINGS_AddAssociation
	@BrokerId UniqueIdentifier,
	@VendorId UniqueIdentifier 

AS
BEGIN

INSERT INTO IRS_4506T_VENDOR_BROKER_SETTINGS(BrokerId, VendorId) VALUES (@BrokerId, @VendorId)

END
