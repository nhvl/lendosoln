-- =============================================
-- Author:		Scott Kibler
-- Create date: 9/10/2014
-- Description:	
--   Gets the RateMonitor name (if in use) by loan id and broker id and user id.
-- =============================================
CREATE PROCEDURE dbo.QUICKPRICER_2_LOAN_USAGE_GetRateMonitorNameByLoanID
	@BrokerID UniqueIdentifier,
	@LoanID UniqueIdentifier,
	@UserIdOfUsingUser UniqueIdentifier
AS
BEGIN
	SELECT 
		RateMonitorName
	FROM 
		QUICKPRICER_2_LOAN_USAGE
	WHERE 
			BrokerID = @BrokerID
		AND IsInUse = 1
		AND UserIDofUsingUser = @UserIDofUsingUser
		AND LoanID = @LoanID
END
