ALTER PROCEDURE [dbo].[VOA_LENDER_ENABLED_OPTION_RetrieveDistinctOptionValuesForVendor]
	@VendorId int,
	@EnabledOptionType int
AS
BEGIN
	SELECT DISTINCT EnabledOptionValue
	FROM VOA_LENDER_ENABLED_OPTION
	WHERE VendorId = @VendorId
		AND EnabledOptionType = @EnabledOptionType
END
GO
