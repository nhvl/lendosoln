-- =============================================
-- Author:		Antonio Valencia
-- Create date: 7/26/2012
-- Description:	Gets draft and its templates
-- =============================================
CREATE PROCEDURE [dbo].[CC_TEMPLATE_GetDraftAndTemplates]
	@BrokerId uniqueidentifier
AS
BEGIN
	DECLARE @DraftOpenedByUserId uniqueidentifier 
	DECLARE @Id int
	SET @Id = null
	
	-- there should only be one draft 
	SELECT TOP 1 @Id = Id, @DraftOpenedByUserId = DraftOpenedByUserId FROM CC_TEMPLATE_SYSTEM where BrokerId = @BrokerId AND ReleasedD IS Null
	
	SELECT Id, BrokerId, [Description], DraftOpenedD, DraftOpenedByUserId, ReleasedD, ReleasedByUserId, RuleXml
	FROM CC_TEMPLATE_SYSTEM Where Id = @Id 
	
	SELECT '' as Name
	
	SELECT cCcTemplateId, cCcTemplateNm, GfeVersion 
	FROM CC_TEMPLATE WHERE CCTemplateSystemId =  @Id and BrokerId = @BrokerId
	
	SELECT UserFirstNm + ' ' + UserLastNm as Name From Employee where EmployeeUserId = @DraftOpenedByUserId
		
END
