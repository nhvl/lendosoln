-- =============================================
-- Author:		Michael Leinweaver
-- Create date: 12/13/2018
-- Description:	Removes a record from the ALL_USER_SHARED_DB table, 
--				intended for use in the event a user's record fails 
--				creation and is rolled back.
-- =============================================
CREATE PROCEDURE [dbo].[ALL_USER_SHARED_DB_Delete]
	@BrokerId uniqueidentifier,
	@LoginName varchar(50)
AS
BEGIN
	DELETE TOP (1)
	FROM ALL_USER_SHARED_DB
	WHERE BrokerId = @BrokerId AND LoginNm = @LoginName
END
