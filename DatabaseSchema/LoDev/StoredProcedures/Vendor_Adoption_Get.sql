CREATE PROCEDURE [dbo].[Vendor_Adoption_Get]
	@IntegrationType varchar(50),
	@VendorId guid,
	@GenericFrameworkProviderId varchar(7),
	@ServiceVendorId int
AS
BEGIN
	SELECT [BrokerId], REPLACE ( BrokerNm, ',', '' ) As Client, [CustomerCode]
	FROM [Broker]
	WHERE [Status] > 0 AND (
		(@IntegrationType = '_4506T' 
			AND [BrokerId] IN (SELECT DISTINCT [BrokerId] FROM [IRS_4506T_VENDOR_BROKER_SETTINGS] WHERE [VendorId] = @VendorId))
		OR
		(@IntegrationType = 'Appraisal' 
			AND [BrokerId] IN (SELECT DISTINCT [BrokerId] FROM [APPRAISAL_VENDOR_BROKER_ENABLED] WHERE [VendorId] = @VendorId))
		OR
		(@IntegrationType = 'AUS'
			AND [BrokerId] IN (SELECT DISTINCT sc.[BrokerId] FROM [SERVICE_CREDENTIAL] sc JOIN [BROKER_X_SERVICE_CREDENTIAL] xsc ON sc.Id = xsc.ServiceCredentialId AND sc.BrokerId = xsc.BrokerId 
								WHERE [IsForAusSubmission] = 1))
		OR
		(@IntegrationType = 'Compliance' 
			AND [IsEnableComplianceEaseIntegration] = 1)
		OR
		(@IntegrationType = 'Credit'
			AND [BrokerId] IN (SELECT DISTINCT sc.[BrokerId] FROM [SERVICE_CREDENTIAL] sc JOIN [BROKER_X_SERVICE_CREDENTIAL] xsc ON sc.Id = xsc.ServiceCredentialId AND sc.BrokerId = xsc.BrokerId 
								WHERE [IsForCreditReports] = 1 AND [ServiceProviderId] = @VendorId))
		OR
		(@IntegrationType = 'Document' 
			AND [BrokerId] IN (SELECT DISTINCT [BrokerId] FROM [DOCUMENT_VENDOR_BROKER_SETTINGS] WHERE [VendorId] = @VendorId))
		OR
		(@IntegrationType = 'FHAConnection')
		OR
		(@IntegrationType = 'Flood' 
			AND [IsEnableFloodIntegration] = 1)
		OR 
		(@IntegrationType = 'Fraud' 
			AND [IsEnableDriveIntegration] = 1)
		OR 
		(@IntegrationType = 'GenericFramework' 
			AND [BrokerId] IN (SELECT DISTINCT [BrokerId] FROM [GENERIC_FRAMEWORK_VENDOR_BROKER_CONFIG] WHERE [ProviderID] = @GenericFrameworkProviderId))
		OR
		(@IntegrationType = 'KIVA'
			AND [IsKivaIntegrationEnabled] = 1)
		OR
		(@IntegrationType = 'MortgageInsurance' 
			AND [BrokerId] IN (SELECT DISTINCT [BrokerId] FROM [MORTGAGE_INSURANCE_VENDOR_BROKER_SETTINGS] WHERE [VendorId] = @VendorId))
		OR
		(@IntegrationType = 'OCR'
			AND [OCRVendorId] = @VendorId)
		OR
		(@IntegrationType = 'QualityControl'
			AND [IsEnableFannieMaeEarlyCheck] = 1)
		OR
		(@IntegrationType = 'Symitar'
			AND [IsCreditUnion] = 1)
		OR
		(@IntegrationType = 'Title'
			AND [BrokerId] IN (SELECT DISTINCT [BrokerId] FROM [TITLE_FRAMEWORK_LENDER_SERVICE] WHERE [VendorId] = @ServiceVendorId))
		OR
		(@IntegrationType = 'VOX'
			AND [BrokerId] IN (SELECT DISTINCT [BrokerId] FROM [VERIFICATION_LENDER_SERVICE] WHERE [VendorId] = @ServiceVendorId))
	)
END
GO


