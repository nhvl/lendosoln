-- =============================================
-- Author:		Geoff Feltman
-- Create date: 11/10/17
-- Description:	Gets a loan's data schema version by loan id.
-- =============================================
ALTER PROCEDURE [dbo].[GetDataSchemaVersionByLoanId]
	@sLId uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT sDataSchemaVersion
	FROM LOAN_FILE_A
	WHERE sLId = @sLId
END
