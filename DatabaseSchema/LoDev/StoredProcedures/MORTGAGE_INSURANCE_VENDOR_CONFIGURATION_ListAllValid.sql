-- =============================================
-- Author:		Isaac Ribakoff
-- Create date: 9/19/2014
-- Description: List All Valid MI Vendors
-- =============================================
CREATE PROCEDURE [dbo].[MORTGAGE_INSURANCE_VENDOR_CONFIGURATION_ListAllValid]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT *
	FROM MORTGAGE_INSURANCE_VENDOR_CONFIGURATION
	WHERE IsValid = 1
	ORDER BY VendorType
END
