ALTER PROCEDURE [dbo].[ListRolodexUsingEmployeeByBrokerID]
	@BrokerId uniqueidentifier, @UserType char(1) = NULL, @TypeFilter varchar(64) = NULL, @NameFilter varchar(64) = NULL, @StatusFilter int = NULL, @BranchId uniqueidentifier = NULL
AS
	SELECT DISTINCT
		e.EmployeeId AS AgentId,
		0 AS AgentType,
		'' AS AgentTitle,
		e.UserFirstNm + ' ' + e.UserLastNm AS AgentNm,
		e.UserNmOnLoanDocs AS AgentNmOnLoanDocs,
		e.Phone AS AgentPhone,
		e.CellPhone AS AgentAltPhone,
		e.IsCellphoneForMultiFactorOnly AS IsAgentAltPhoneForMultiFactorOnly,
		e.Fax AS AgentFax,
		e.Pager AS AgentPager,
		CASE x.Type WHEN 'B' THEN b.BrokerNm WHEN 'P' THEN pmlBroker.Name END AS AgentComNm,
		'' AS AgentDepartmentName,
		e.Email AS AgentEmail,
		e.Addr AS AgentAddr,
		e.City AS AgentCity,
		e.State AS AgentState,
		e.Zip AS AgentZip,
		e.LicenseXmlContent as LicenseXmlContent,
		e.NmlsIdentifier as LosIdentifier,
		b.BrokerAddr AS AddrOfCompany,
		b.BrokerCity AS CityOfCompany,		
		b.BrokerState AS StateOfCompany,
		b.BrokerZip AS ZipOfCompany,
		b.NmlsIdentifier AS BrokerLosIdentifier,
		b.LicenseXmlContent as BrokerLicenseXml,
		h.BranchNm AS NameOfBranch,
		h.BranchAddr AS AddrOfBranch,
		h.BranchCity AS CityOfBranch,		
		h.BranchState AS StateOfBranch,
		h.BranchZip AS ZipOfBranch,
		e.EmployeeLicenseNumber AS AgentLicenseNumber,
		e.CommissionPointOfLoanAmount as CommissionPointOfLoanAmount,
		e.CommissionPointOfGrossProfit as CommissionPointOfGrossProfit,
		e.CommissionMinBase as CommissionMinBase,
		'' as CompanyLicenseNumber,
		b.BrokerPhone as PhoneOfCompany,
		b.BrokerFax AS FaxOfCompany,
		h.BranchPhone as PhoneOfBranch,
		h.BranchFax AS FaxOfBranch,
		h.UseBranchInfoForLoans,
		h.NmlsIdentifier AS BranchLosIdentifier,
		h.LicenseXmlContent as BranchLicenseXml,
		0 AS IsNotifyWhenLoanStatusChange,
		CASE x.Type WHEN 'B' THEN '' WHEN 'P' THEN 'PML' END AS UserType,
		h.DisplayNm AS CompanyDisplayName,
		e.EmployeeIDInCompany,
		e.ChumsID as ChumsID
	FROM
		Broker AS b WITH (NOLOCK) JOIN Branch AS h WITH (NOLOCK) ON h.BrokerId = b.BrokerId 
                                              JOIN Employee AS e WITH (NOLOCK) ON e.BranchId = h.BranchId 
											  JOIN Broker_User bu WITH(NOLOCK) ON e.EmployeeUserId = bu.UserId
                                              LEFT JOIN Pml_Broker pmlBroker WITH(NOLOCK) ON bu.PmlBrokerId = pmlBroker.PmlBrokerId
                                              JOIN All_User AS x WITH (NOLOCK) ON x.UserId = e.EmployeeUserId, Role AS r, Role_Assignment AS a
	WHERE
		b.BrokerId = @BrokerId
		AND
		r.RoleModifiableDesc = COALESCE(@TypeFilter, r.RoleModifiableDesc)
		AND
		a.RoleId = r.RoleId
		AND
		e.EmployeeId = a.EmployeeId
		AND
		(
			(@UserType IS NULL)
			OR
			((@UserType IS NOT NULL) AND (x.Type = @UserType))
		)
		AND
		(
			(@StatusFilter IS NULL)
			OR
			((@StatusFilter IS NOT NULL) AND (e.IsActive = @StatusFilter))
		)
		AND
		(
			e.UserFirstNm + ' ' +  e.UserLastNm LIKE COALESCE(@NameFilter + '%', e.UserFirstNm + ' ' + e.UserLastNm)
			OR
			e.UserLastNm LIKE COALESCE(@NameFilter + '%', e.UserLastNm)
		)
		AND
		(
			(@BranchId IS NULL)
			OR
			((@BranchId IS NOT NULL) AND (e.BranchId = @BranchId))
		)
	ORDER BY
		AgentNm
	if( 0!=@@error)
	begin
		RAISERROR('Error in the select statement in ListRolodexUsingEmployeeByBrokerID sp', 16, 1);
		return -100;
	end
	
	return 0;
