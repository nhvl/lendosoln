-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE dbo.TaskTriggerTemplate_ListDistinctBroker 
AS
BEGIN
	SELECT DISTINCT tt.BrokerId
	FROM TASK_TRIGGER_TEMPLATE tt JOIN BROKER b ON tt.BrokerId = b.BrokerId
	WHERE b.Status = 1
END
