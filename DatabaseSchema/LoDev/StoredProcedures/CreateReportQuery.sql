CREATE PROCEDURE CreateReportQuery
	@BrokerID Guid,
             @QueryID Guid , 
             @QueryName Varchar(64) , 
             @EmployeeID Guid , 
             @XmlContent Text , 
             @ShowPosition Int = 0
AS
	INSERT INTO Report_Query
	( QueryID , BrokerID , EmployeeID , QueryName , XmlContent , ShowPosition
	)
	VALUES
	( @Queryid , @BrokerID , @EmployeeID , @QueryName , @XmlContent , @ShowPosition
	)
