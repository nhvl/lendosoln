

-- =============================================
-- Author:		Antonio Valencia
-- Create date: September 16, 2010
-- Description:	Gets most recent configurations for each organization
-- =============================================
CREATE PROCEDURE [dbo].[CONFIG_LO_GenerateImportListing] 
	@IsDraft bit,
	@BrokerName varchar( 32 ) = NULL , 
	@CustomerCode varchar( 10 ) = NULL 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	IF @IsDraft = 1 
	BEGIN
		SELECT CustomerCode, BrokerNm, BrokerId 
		FROM CONFIG_DRAFT JOIN BROKER on OrgId = brokerid
		WHERE BrokerNm LIKE COALESCE( @BrokerName + '%' , BrokerNm )
		AND
		CustomerCode LIKE COALESCE( @CustomerCode + '%' , CustomerCode )
    END
	ELSE
	BEGIN

		SELECT DISTINCT  CustomerCode, BrokerNm, BrokerId
		FROM CONFIG_RELEASED join  Broker on BrokerId = OrgId
		WHERE BrokerNm LIKE COALESCE( @BrokerName + '%' , BrokerNm )
		AND
		CustomerCode LIKE COALESCE( @CustomerCode + '%' , CustomerCode )
	END

END


