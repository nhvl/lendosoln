ALTER PROCEDURE [dbo].[CUSTOM_COC_FIELD_LIST_SAVE]
	@CustomCocFieldList CustomCocFieldTableType READONLY,
	@BrokerId uniqueidentifier
AS
BEGIN	
	WITH CUSTOM_COC_FIELD_LIST_PARTIAL AS
	(
		SELECT RowId, FieldId, BrokerId, [Description], IsInstant
		FROM CUSTOM_COC_FIELD_LIST 
		WHERE BrokerId = @BrokerId
	)
	MERGE CUSTOM_COC_FIELD_LIST_PARTIAL AS TARGET
	USING @CustomCocFieldList AS SOURCE
	ON (TARGET.FieldId = SOURCE.FieldId)
		WHEN MATCHED AND 
			TARGET.[Description] != SOURCE.[Description] OR
			TARGET.IsInstant != SOURCE.IsInstant 
		THEN UPDATE SET 
			TARGET.[Description] = SOURCE.[Description],
			TARGET.IsInstant = SOURCE.IsInstant
		WHEN NOT MATCHED BY TARGET THEN
			INSERT
			(
				FieldId,
				BrokerId,
				[Description],
				IsInstant
			)
			VALUES 
			(
				SOURCE.FieldId,
				@BrokerId,
				SOURCE.[Description],
				SOURCE.IsInstant
			)
		WHEN NOT MATCHED BY SOURCE THEN
		DELETE;
END
