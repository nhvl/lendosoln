-- =============================================
-- Author:		Timothy Jewell
-- Create date: 06/13/14
-- Description:	Fetches data on Broker App Code for tracking modified files
-- =============================================
CREATE PROCEDURE [dbo].[BROKER_WEBSERVICE_APP_CODE_Fetch]
	@BrokerId uniqueidentifier,
	@AppCode uniqueidentifier = NULL	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		AppCode,
		AppName,
		ContactName,
		ContactEmail,
		Notes,
		Enabled
	FROM BROKER_WEBSERVICE_APP_CODE WITH (nolock)
	WHERE 
		BrokerId = @BrokerId
		AND (@AppCode IS NULL OR AppCode = @AppCode)
END
