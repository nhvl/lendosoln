
-- =============================================
-- Author:		Scott Gettinger
-- Create date: 
-- Description:	Retrieve a list of DU Third Party Providers approved by Fannie Mae for the DU program.
-- =============================================
CREATE PROCEDURE [dbo].[DU_THIRD_PARTY_PROVIDERS_ListAll] 

AS
BEGIN

    SELECT *
    FROM DU_THIRD_PARTY_PROVIDERS

END

