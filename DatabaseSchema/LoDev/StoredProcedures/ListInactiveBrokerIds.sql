-- =============================================
-- Author:		Geoff Feltman
-- Create date: 10/31/17
-- Description:	Retrieves all disabled broker ids.
-- =============================================
ALTER PROCEDURE [dbo].[ListInactiveBrokerIds]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT BrokerId
	FROM BROKER
	WHERE Status = 0
END
