-- =============================================
-- Author:		Eduardo Michel
-- Create date: 7/3/2017
-- Description:	Adds a Status Event to the StatusEvent table.
-- =============================================
ALTER PROCEDURE [dbo].[STATUS_EVENT_ADD] 
	-- Add the parameters for the stored procedure here
	@LoanId uniqueidentifier, 
	@StatusT int,
	@StatusD smalldatetime,
	@Timestamp smalldatetime,
	@IsExclude bit,
	@BrokerId uniqueidentifier,
	@UserName varchar(100) = NULL -- TODO Remove default before merge
AS
BEGIN
	INSERT INTO STATUS_EVENT (LoanId, StatusT, StatusD, Timestamp, IsExclude, BrokerId, UserName)
	VALUES (@LoanId, @StatusT, @StatusD, @Timestamp, @IsExclude, @BrokerId, @UserName);	
END
