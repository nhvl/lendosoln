ALTER PROCEDURE [dbo].[VOA_LENDER_ENABLED_OPTION_SaveOption]
	@VendorId int,
	@BrokerId uniqueidentifier,
	@LenderServiceId int,
	@EnabledOptionType int,
	@EnabledOptionValue int,
	@UserType char(1),
	@IsDefault bit
AS
BEGIN
	INSERT INTO VOA_LENDER_ENABLED_OPTION
	(
		VendorId,
		BrokerId,
		LenderServiceId,
		EnabledOptionType,
		EnabledOptionValue,
		UserType,
		IsDefault
	)
	VALUES
	(
		@VendorId,
		@BrokerId,
		@LenderServiceId,
		@EnabledOptionType,
		@EnabledOptionValue,
		@UserType,
		@IsDefault
	)
END
GO
