-- =============================================
-- Author:		Scott Kibler
-- Create date: 2/4/2014
-- Description:	opm 150384
-- =============================================
CREATE PROCEDURE [dbo].TPOPortalConfig_Update
	@ID uniqueidentifier,
	@BrokerID uniqueidentifier,
	@Name varchar(36),
	@Url varchar(255)
AS
BEGIN
	UPDATE LENDER_TPO_LANDINGPAGECONFIG
	SET Name = @Name, Url = @Url
	WHERE ID = @ID
	and BrokerID = @BrokerID
END

