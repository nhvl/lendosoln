--this sp was created to test transaction stuff.  Never mind its existence -Thinh.
CREATE PROCEDURE WillFail AS
	declare @tranPoint sysname
	set @tranPoint = object_name(@@procId) + CAST(@@nestlevel as varchar(10))
	--print @tranPoint
	
	BEGIN TRANSACTION
	SAVE TRANSACTION @tranPoint
	update APPLICATION_A
	set aBAddr = getdate()
	where aAppId = '46B314DD-25F8-4B0A-937E-0058DCA5366D'
	
	if( 0!=@@error)
	begin
		RAISERROR('Error in 1st updating Application table in WillFail sp', 16, 1);
		goto ErrorHandler;
	end
	
	update APPLICATION_A
	set aAppId = '46B314DD-25F8-4B0A-937E-0058DCA5366D'
	where aAppId =  'BBE7998C-689C-421C-B648-0086AB2258F9'
	
	if( 0!=@@error)
	begin
		RAISERROR('Error in 2nd updating Application table in WillFail sp', 16, 1);
		goto ErrorHandler;
	end
	
	commit transaction 
	return 0;
ErrorHandler:
	rollback transaction @tranPoint
	rollback transaction
	return -100;
