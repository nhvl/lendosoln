-- =============================================
-- Author:		Antonio Valencia
-- Create date: 1/5/10
-- Description:	Creates a new EDOCS Document
-- 1/4/12: Added support for document status - OPM 63511, AM
-- 5/12/2013 : Use a transaction.. to avoid partially created edocs OPM 124089
-- 7/12/2013: Added SQL to update sUnscreenedDocsInFile and sNewEdocsUploadedD in LOAN_FILE_CACHE
-- 8/14/2013: Added HideFromPmlUsers
-- =============================================
ALTER PROCEDURE [dbo].[EDOCS_CreateDocument] 
	-- Add the parameters for the stored procedure here
	@sLId uniqueidentifier = null, 
	@aAppId uniqueidentifier = null,
	@BrokerId uniqueidentifier = null, 
	@DocTypeId int, 
	@FaxNumber varchar(14) = '',
	@OriginalRevisionKey uniqueidentifier = null,
	@CurrentRevisionKey uniqueidentifier = null,
	@Comments varchar(500),
	@NumPages int,
	@IsUploadedByPmlUser bit,
	@DocumentId uniqueidentifier,
	@CreatedFromExistingDocId uniqueidentifier = null, 
	@ModifiedByUserId uniqueidentifier = null, 
	@UpdateReason varchar(200) = '',
	@InternalDescription varchar(200),
	@PublicDescription varchar(200),
	@IsEditable bit,
	@PdfHasOwnerPassword bit = null,
	@HasInternalAnnotation bit,
	@TimeToRequeueForImageGeneration DateTime = null,
	@Reason varchar(200) = '',
	@PdfHasUnsupportedFeatures bit = 0,
	@Status tinyint = 0,
	@StatusDescription varchar(200)= '',
	@HideFromPmlUsers bit = 0,
	@MetadataJsonContent varchar(max) = NULL,
	@OriginalMetadataJsonContent varchar(max) = NULL,
	@IsESigned bit = 0,
	@CreatedByUserId uniqueidentifier = NULL,
	@HasESignTags bit = 0,
	@MetadataProtobufContent varbinary(max) = NULL,
	@OriginalMetadataProtobufContent varbinary(max) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	
	IF @BrokerId is null AND @sLId is not null  BEGIN 
		select @BrokerId = sBrokerId from Loan_File_Cache where sLId = @sLId
	END
	
	DECLARE @Month int 
	DECLARE @Year int

	SET @Month = MONTH(GETDATE())
	SET @Year = YEAR(GETDATE())
	
	BEGIN tran
	INSERT INTO EDOCS_DOCUMENT(
		DocumentId, 
		sLId, 
		aAppId, 
		BrokerId, 
		DocTypeId, 
		FaxNumber, 
		FileDBKey_CurrentRevision, 
		FileDBKey_OriginalDocument, 
		Description, 
		Comments, 
		NumPages, 
		IsUploadedByPmlUser, 
		Version, 
		InternalDescription, 
		IsEditable,
		PdfHasOwnerPassword, 
		HasInternalAnnotation, 
		TimeToRequeueForImageGeneration, 
		PdfHasUnsupportedFeatures, 
		[Status], 
		StatusDescription, 
		HideFromPmlUsers, 
		MetadataJsonContent, 
		OriginalMetadataJsonContent, 
		IsESigned,
		CreatedByUserId,
		HasESignTags,
		MetadataProtobufContent,
		OriginalMetadataProtobufContent) 
	VALUES(
		@DocumentId, 
		@sLId, 
		@aAppId, 
		@BrokerId, 
		@DocTypeId, 
		@FaxNumber,  
		@CurrentRevisionKey,
		@OriginalRevisionKey, 
		@PublicDescription, 
		@Comments, 
		@NumPages, 
		@IsUploadedByPmlUser, 
		0, 
		@InternalDescription, 
		@IsEditable, 
		0, 
		@HasInternalAnnotation, 
		@TimeToRequeueForImageGeneration, 
		@PdfHasUnsupportedFeatures, 
		@Status, 
		@StatusDescription, 
		@HideFromPmlUsers, 
		@MetadataJsonContent, 
		@OriginalMetadataJsonContent, 
		@IsESigned,
		@CreatedByUserId,
		@HasESignTags,
		@MetadataProtobufContent,
		@OriginalMetadataProtobufContent)

	IF @CreatedFromExistingDocId IS NOT NULL BEGIN -- per av OPM 67127 Copy audit from split 
		INSERT INTO EDocs_Audit_History( DocumentId, ModifiedByUserId, ModifiedDate, Description ) 
		SELECT  @DocumentId  as  DocumentId, ModifiedByUserId, ModifiedDate, Description 
		FROM EDocs_Audit_History
		WHERE DocumentId = @CreatedFromExistingDocId
	END
	
	-- update cached stats --
	IF @sLId IS NOT NULL
	BEGIN
		UPDATE LOAN_FILE_CACHE_3
		SET sNewEdocsUploadedD = GETDATE(),
		sUnscreenedDocsInFile = CASE WHEN @Status = 0 THEN 1 ELSE sUnscreenedDocsInFile END
		WHERE sLId = @sLId
	END

	-- update the audit history
	INSERT INTO EDocs_Audit_History( DocumentId, ModifiedByUserId, ModifiedDate, Description ) 
		VALUES ( @DocumentId, @ModifiedByUserId,  GETDATE(), @Reason )

	COMMIT  
END
