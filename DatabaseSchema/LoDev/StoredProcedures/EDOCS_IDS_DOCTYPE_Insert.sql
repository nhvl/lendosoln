-- =============================================
-- Author:		Thien Nguyen
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[EDOCS_IDS_DOCTYPE_Insert] 
    @Description varchar(200),
    @BarcodeClassification varchar(5)
AS
BEGIN

	INSERT INTO EDOCS_IDS_DOCTYPE (Description,  BarcodeClassification)
					VALUES(@Description, @BarcodeClassification)
                                     
	SELECT CAST(SCOPE_IDENTITY() AS int)
END
