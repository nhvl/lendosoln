ALTER PROCEDURE [dbo].[InsertServiceCompany] 
	@ComId uniqueidentifier,
	@ComNm varchar(100),
	@ComUrl varchar(196),
	@ProtocolType int,
	@IsValid bit,
	@IsInternalOnly bit,
	@MclCraCode char(2),
	@IsWarningOn bit,
	@WarningMsg varchar(500),
	@WarningStartD smalldatetime,
	@VoxVendorId int = null
AS
INSERT INTO Service_Company(ComId, ComNm, ComPhone, ComFax, ComUrl, ProtocolType, IsValid, IsInternalOnly, MclCraCode, IsWarningOn, WarningMsg, WarningStartD, VoxVendorId)
VALUES (@ComId, @ComNm, '', '', @ComUrl, @ProtocolType, @IsValid, @IsInternalOnly, @MclCraCode, @IsWarningOn, @WarningMsg, @WarningStartD, @VoxVendorId)
