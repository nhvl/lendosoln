CREATE procedure tmp_SetUserHashPassword
	@userId uniqueidentifier,
	@hashPwd varchar(300)
as
	update all_user
	set passwordHash = @hashPwd
	where userId = @userId
