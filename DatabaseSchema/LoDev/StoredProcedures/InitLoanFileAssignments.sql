CREATE PROCEDURE [dbo].[InitLoanFileAssignments]
	@CreatorEmployeeID GUID,
	@LoanID GUID,
	@SrcLoanIdForAssignmentDuplication GUID = null,
	@PortalMode int = null,
	@SourceFilePmlBrokerId uniqueidentifier = null OUTPUT
AS
-- Assign the loan to this employee (EXCEPT for Administrator role, Accountant role).
IF( @SrcLoanIdForAssignmentDuplication is null )
BEGIN
	-- Assign all roles of this employee to loan (EXCEPT for Administrator role, Accountant role, Closer Role).
	INSERT INTO Loan_User_Assignment with(rowlock) (sLId, RoleID, EmployeeID) 
	SELECT @LoanID, RoleID, @CreatorEmployeeID 
	FROM Role_Assignment with(updlock,rowlock)
	WHERE EmployeeID = @CreatorEmployeeID 
		AND RoleID NOT IN 
		(
			'86C86CF3-FEB6-400F-80DC-123363A79605',
			'F98C17F5-27AD-4097-ACFB-88F6A754ACF3',
			'270A7689-EEED-49F9-B438-69AC322EA834'
		)
END
ELSE
BEGIN
	-- Copying assignment from source loan
	INSERT INTO Loan_User_Assignment with(rowlock)(sLId, RoleID, EmployeeID) 
	SELECT @LoanID, RoleID, EmployeeID 
	FROM Loan_User_Assignment with(updlock,rowlock)
	WHERE sLId = @SrcLoanIdForAssignmentDuplication
	
	
	SELECT @SourceFilePmlBrokerId = sPmlBrokerId
	FROM LOAN_FILE_CACHE
	WHERE sLId = @SrcLoanIdForAssignmentDuplication
END