CREATE procedure [dbo].[EDOCS_GetShippingTemplateData]
	@ShippingTemplateId int,
	@BrokerId uniqueidentifier = null
as
select doc.DocTypeId, doc.DocTypeName, doc.FolderId
from EDOCS_DOCUMENT_TYPE_X_SHIPPING_TEMPLATE as tem, 
	 EDOCS_DOCUMENT_TYPE as doc, 
	 EDOCS_SHIPPING_TEMPLATE as shi
where tem.ShippingTemplateId = @ShippingTemplateId
	and tem.ShippingTemplateId = shi.ShippingTemplateId
	and tem.DocTypeId = doc.DocTypeId
	and (shi.BrokerId = @BrokerId 
	or shi.BrokerId is null)
order by tem.StackingOrder