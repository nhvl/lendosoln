CREATE PROCEDURE [dbo].[RegionSet_Delete]
	@BrokerId uniqueidentifier,
	@RegionSetId int
AS
BEGIN
	DELETE FROM REGION_X_FIPS
	WHERE BrokerId = @BrokerId
		AND RegionSetId = @RegionSetId
		
	DELETE FROM REGION
	WHERE BrokerId = @BrokerId
		AND RegionSetId = @RegionSetId

	DELETE FROM REGION_SET
	WHERE BrokerId = @BrokerId
		AND RegionSetId = @RegionSetId
END