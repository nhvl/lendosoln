CREATE procedure EDOCS_ShippingTemplateEditCheckName
	@ShippingTemplateName varchar(50),
	@List varchar(MAX),
	@ShippingTemplateId int,
	@BrokerId uniqueidentifier
as

select 1
	from EDOCS_SHIPPING_TEMPLATE 
	where ShippingTemplateName = @ShippingTemplateName
		and ShippingTemplateId <> @ShippingTemplateId
		and (BrokerId = @BrokerId or BrokerId is null)