ALTER PROCEDURE [dbo].[TrackIntegrationModifiedFile_AddFilesByIdList]
  @BrokerId uniqueidentifier,
  @AppCode uniqueidentifier = NULL,
  @LoanIdListXml xml,
  @NewestLastModified datetime
AS
BEGIN
  -- Convert XML input into a table 
  DECLARE @LoansToAdd TABLE (loanId uniqueidentifier NOT NULL)
  
  INSERT INTO @LoansToAdd
  SELECT T.Item.value('.', 'uniqueidentifier')
  FROM @LoanIdListXml.nodes('root/row/@sLId') as T(Item)

  INSERT INTO Integration_File_Modified 
			(AppCode, sLId, sLNm, LastModifiedD, aBNm, sSpAddr, BrokerId,  IsValid, sBranchID, sEmployeeManagerId, sEmployeeProcessorId,sEmployeeLoanRepId, sEmployeeLoanOpenerId,sEmployeeCallCenterAgentId, sEmployeeRealEstateAgentId, sEmployeeLenderAccExecId, sEmployeeLockDeskId, sEmployeeUnderwriterId, PmlExternalManagerEmployeeId, sEmployeeShipperId, sEmployeeFunderId, sEmployeePostCloserId, sEmployeeInsuringId, sEmployeeCollateralAgentId, sEmployeeDocDrawerId, sStatusT, sEmployeeCreditAuditorId, sEmployeeDisclosureDeskId, sEmployeeJuniorProcessorId, sEmployeeJuniorUnderwriterId, sEmployeeLegalAuditorId, sEmployeeLoanOfficerAssistantId, sEmployeePurchaserId, sEmployeeQCComplianceId, sEmployeeSecondaryId, sEmployeeServicingId, sOldLNm, sEncryptionKey, sEncryptionMigrationVersion, aBSsnEncrypted ) 
	SELECT	 @AppCode, lc.sLId, lc.sLNm, DateAdd(ms, -3.3*row_number() OVER (ORDER BY (SELECT NULL)), @NewestLastModified), aBNm, sSpAddr, lc.sBrokerId, IsValid, sBranchID, sEmployeeManagerId, sEmployeeProcessorId,sEmployeeLoanRepId, sEmployeeLoanOpenerId,sEmployeeCallCenterAgentId, sEmployeeRealEstateAgentId, sEmployeeLenderAccExecId, sEmployeeLockDeskId, sEmployeeUnderwriterId, PmlExternalManagerEmployeeId, sEmployeeShipperId, sEmployeeFunderId, sEmployeePostCloserId, sEmployeeInsuringId, sEmployeeCollateralAgentId, sEmployeeDocDrawerId, sStatusT, sEmployeeCreditAuditorId, sEmployeeDisclosureDeskId, sEmployeeJuniorProcessorId, sEmployeeJuniorUnderwriterId, sEmployeeLegalAuditorId, sEmployeeLoanOfficerAssistantId, sEmployeePurchaserId, sEmployeeQCComplianceId, sEmployeeSecondaryId, sEmployeeServicingId /*! only doing coalesce for dev.*/, coalesce(sOldLNm,''), sEncryptionKey, sEncryptionMigrationVersion, aBSsnEncrypted
	FROM LOAN_FILE_CACHE lc
		JOIN @LoansToAdd loans ON loans.loanId = lc.sLId
		JOIN LOAN_FILE_CACHE_2 lc2 ON lc.sLId = lc2.sLId
		JOIN LOAN_FILE_CACHE_3 lc3 ON lc.sLId = lc3.sLId
		JOIN LOAN_FILE_CACHE_4 lc4 on lc.sLId = lc4.sLId
	WHERE lc.sBrokerId = @BrokerId
    AND NOT EXISTS (SELECT sLId FROM Integration_File_Modified WHERE Integration_File_Modified.sLId = lc.sLId AND AppCode = @AppCode)
END
