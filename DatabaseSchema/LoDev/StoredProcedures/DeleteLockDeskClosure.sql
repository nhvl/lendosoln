

CREATE PROCEDURE [dbo].[DeleteLockDeskClosure]
	@BrokerId uniqueidentifier,
	@ClosureDate smalldatetime
AS
BEGIN

	DELETE FROM LockDesk_Closure
	WHERE BrokerId = @BrokerId AND ClosureDate = @ClosureDate
END
