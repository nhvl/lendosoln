CREATE PROCEDURE [dbo].[Billing_ComplianceEase_AldenCU]
	@StartDate datetime,
	@EndDate datetime
AS
BEGIN
	-- dd 2018-11-02 - For use in automate billing executable.
	-- Query to generate compliance ease transaction.
SELECT ptb.BillingDate AS BillingDate, REPLACE(lfc.aBNm, ',', '') AS BorrowerName, lfc.sLNm AS LoanNumber, REPLACE(BROKER.BrokerNm, ',', '') AS Lender
                                            FROM BILLING_PER_TRANSACTION ptb
                                            JOIN LOAN_FILE_CACHE lfc ON lfc.sLId = ptb.sLId
                                            JOIN Broker ON Broker.BrokerId = lfc.sBrokerId
                                            WHERE BillingDate >= @StartDate AND BillingDate<@EndDate
                                            AND Broker.CustomerCode = 'PML0269' 
                                            AND lfc.aBLastNm != 'TESTCASE'
                                            AND lfc.aBLastNm != 'Sample' 
                                            AND lfc.aBLastNm != 'HOMEOWNER'
                                            AND lfc.sLpTemplateNm != 'Home Equity Loan with Prepayment Penalty'
                                            AND lfc.sLpTemplateNm != 'Fixed Home Equity Loan'
                                            AND lfc.sLienPosT != 1
END