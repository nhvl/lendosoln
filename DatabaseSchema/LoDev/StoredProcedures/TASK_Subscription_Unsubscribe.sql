-- =============================================
-- Author:		Antonio Valencia
-- Create date: 5/3/11
-- Description:	Unsubscribes a user from task notification
-- =============================================
CREATE PROCEDURE [dbo].[TASK_Subscription_Unsubscribe] 
	@TaskId varchar(10),
	@UserId uniqueidentifier
AS

-- REVIEW:
-- 6/9/2011: Reviewed. -ThinhNK
BEGIN
	DELETE FROM TASK_SUBSCRIPTION 
	WHERE TaskId = @TaskId AND UserId = @UserId 
END
