-- =============================================
-- Author:		Thien Nguyen
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[EDOCS_DOCUTECH_DOCTYPE_Delete] 
    @Id int
AS
BEGIN

	DELETE FROM EDOCS_DOCUTECH_DOCTYPE
		  WHERE Id = @Id
END
