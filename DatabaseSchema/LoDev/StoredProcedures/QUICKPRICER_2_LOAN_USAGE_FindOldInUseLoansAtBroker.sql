-- =============================================
-- Author:		Scott Kibler
-- Create date: 8/6/2014
-- Description:	
--	Finds "in use" pooled loans older than QuickPricer2InUseLoanExpirationDays.
-- =============================================
CREATE PROCEDURE dbo.QUICKPRICER_2_LOAN_USAGE_FindOldInUseLoansAtBroker
	@BrokerID UniqueIdentifier,
	@QuickPricer2InUseLoanExpirationDays tinyint = 2 -- must be 2 or greater.  from conststage ideally.
AS
BEGIN
	DECLARE @ErrorCode int
	DECLARE @ErrorMsg varchar(1000)
	SET @ErrorCode = -1;
	SET @ErrorMsg = 'Unexpected Error';
	
	IF @QuickPricer2InUseLoanExpirationDays < 2
	BEGIN
		SET @ErrorMsg = 'Cannot look for loans less than 2 days old.'
		GOTO ErrorHandler;
	END
	
	SELECT LoanID
	FROM QUICKPRICER_2_LOAN_USAGE
	WHERE 
		    BrokerID = @BrokerID
		AND IsInUse = 1
		AND LastUsedD < DATEADD(day, -@QuickPricer2InUseLoanExpirationDays, GetDate())
	IF 0 <> @@Error
	BEGIN
		SET @ErrorMsg = 'An issue occurred when finding the loans.'
		GOTO ErrorHandler;
	END
	
	
	RETURN 0;
	ErrorHandler:
		RAISERROR(@ErrorMsg, 16, 1)
		RETURN @ErrorCode;
END
