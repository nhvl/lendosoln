ALTER PROCEDURE [dbo].[VOA_ORDER_ASSET_VERIFIED_ClearForOrder]
	@BrokerId uniqueidentifier,
    @LoanId uniqueidentifier,
    @OrderId int
AS
BEGIN
	DELETE FROM [dbo].[VOA_ORDER_ASSET_VERIFIED]
    WHERE OrderId = @OrderId
        AND (SELECT COUNT(*) FROM dbo.VOX_ORDER o WHERE o.OrderId = @OrderId AND o.BrokerId = @BrokerId AND o.LoanId = @LoanId) = 1
END
