CREATE PROCEDURE UpdatePriceGroupProduct 
	@LpePriceGroupId Guid,
	@lLpTemplateId Guid,
	@LenderProfitMargin decimal(9, 3),
	@LenderMaxYspAdjustLckd bit,
	@LenderMaxYspAdjust decimal(9, 3),
	@LenderRateMargin decimal(9, 3),
	@LenderMarginMargin decimal(9, 3),
	@IsValid bit
AS
UPDATE LPE_Price_Group_Product
       SET LenderProfitMargin = @LenderProfitMargin,
               LenderMaxYspAdjust = @LenderMaxYspAdjust,
	 LenderMaxYspAdjustLckd = @LenderMaxYspAdjustLckd,
	LenderRateMargin = @LenderRateMargin,
	LenderMarginMargin = @LenderMarginMargin,
	IsValid = @IsValid
WHERE LpePriceGroupId = @LpePriceGroupId AND lLpTemplateId = @lLpTemplateId
IF @@ROWCOUNT = 0
BEGIN
INSERT INTO LPE_Price_Group_Product (LpePriceGroupId, lLpTemplateId, LenderProfitMargin, LenderMaxYspAdjustLckd, LenderMaxYspAdjust, LenderRateMargin, IsValid, LenderMarginMargin)
VALUES (@LpePriceGroupId, @lLpTemplateId, @LenderProfitMargin, @LenderMaxYspAdjustLckd, @LenderMaxYspAdjust, @LenderRateMargin, @IsValid, @LenderMarginMargin)
END
