-- =============================================
-- Author:		Timothy Jewell
-- Create date:	05/23/2017
-- Description:	Retrieves a single VOA order for a loan
-- =============================================
ALTER PROCEDURE [dbo].[VOA_ORDER_Retrieve]
	@BrokerId uniqueidentifier,
	@LoanId uniqueidentifier,
	@OrderId int = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	IF @OrderId IS NOT NULL
	BEGIN
		SELECT
			voxOrder.BrokerId,
			voxOrder.LoanId,
			voxOrder.ApplicationId,
			voxOrder.OrderId,
			voxOrder.TransactionId,
			voxOrder.VendorId,
			voxOrder.ProviderId,
			voxOrder.HasBeenRefreshed,
			voxOrder.RefreshedFromOrderId,
			voxOrder.IsTestOrder,
			voxOrder.LenderServiceId,
			voxOrder.LenderServiceName,
			voxOrder.OrderNumber,
			voxOrder.Status,
			voxOrder.StatusDescription,
			voxOrder.OrderedBy,
			voxOrder.DateOrdered,
			voxOrder.DateCompleted,
			voxOrder.ErrorMessage,
			voxOrder.NotificationEmail,
			voaOrder.VerificationType,
			voaOrder.RefreshPeriod,
			voaOrder.IsForCoborrower
		FROM VOA_ORDER voaOrder
			JOIN VOX_ORDER voxOrder ON voxOrder.OrderId = voaOrder.OrderId
		WHERE voxOrder.BrokerId = @BrokerId
			AND voxOrder.LoanId = @LoanId
			AND voxOrder.OrderId = @OrderId

		SELECT a.OrderId, a.Id, a.Institution, a.AccountNumber
		FROM VOA_ORDER_ASSET a
			JOIN VOA_ORDER voaOrder ON a.OrderId = voaOrder.OrderId
			JOIN VOX_ORDER voxOrder ON voaOrder.OrderId = voxOrder.OrderId
		WHERE voxOrder.BrokerId = @BrokerId
			AND voxOrder.LoanId = @LoanId
			AND voxOrder.OrderId = @OrderId
			
		SELECT 
			d.OrderId, d.DocumentId
		FROM
			VOX_DOCUMENT d
		WHERE
			d.OrderId=@OrderId

		SELECT
			vri.OrderId, vri.VendorReferenceId
		FROM
			VOX_ORDER_VENDOR_REFERENCE_ID vri
		WHERE
			vri.OrderId=@OrderId

        SELECT va.OrderId, va.RowId, va.FinancialInstitutionName, va.EncryptedAccountNumber, va.AccountBalance, va.AssetType, va.EncryptionKeyId
		FROM VOA_ORDER_ASSET_VERIFIED va
			JOIN VOA_ORDER voaOrder ON va.OrderId = voaOrder.OrderId
			JOIN VOX_ORDER voxOrder ON voaOrder.OrderId = voxOrder.OrderId
		WHERE voxOrder.BrokerId = @BrokerId
			AND voxOrder.LoanId = @LoanId
			AND voxOrder.OrderId = @OrderId

        SELECT o.OrderId, statuses.StatusCode, statuses.StatusDescription, statuses.StatusTime
        FROM     
            VOX_ORDER [o] JOIN 
            VOX_ORDER_STATUSES statuses ON o.OrderId = statuses.OrderId
        WHERE   
            o.BrokerId = @BrokerId AND
            o.LoanId = @LoanId AND
            o.OrderId = @OrderId
	END
	ELSE -- @OrderId IS NULL
	BEGIN
		SELECT
			voxOrder.BrokerId,
			voxOrder.LoanId,
			voxOrder.ApplicationId,
			voxOrder.OrderId,
			voxOrder.TransactionId,
			voxOrder.VendorId,
			voxOrder.ProviderId,
			voxOrder.HasBeenRefreshed,
			voxOrder.RefreshedFromOrderId,
			voxOrder.IsTestOrder,
			voxOrder.LenderServiceId,
			voxOrder.LenderServiceName,
			voxOrder.OrderNumber,
			voxOrder.Status,
			voxOrder.StatusDescription,
			voxOrder.OrderedBy,
			voxOrder.DateOrdered,
			voxOrder.DateCompleted,
			voxOrder.ErrorMessage,
			voxOrder.NotificationEmail,
			voaOrder.VerificationType,
			voaOrder.RefreshPeriod,
			voaOrder.IsForCoborrower
		FROM VOA_ORDER voaOrder JOIN 
			 VOX_ORDER voxOrder ON voxOrder.OrderId = voaOrder.OrderId
		WHERE voxOrder.BrokerId = @BrokerId AND 
			  voxOrder.LoanId = @LoanId

		SELECT a.OrderId, a.Id, a.Institution, a.AccountNumber
		FROM VOA_ORDER_ASSET a
			JOIN VOA_ORDER voaOrder ON a.OrderId = voaOrder.OrderId
			JOIN VOX_ORDER voxOrder ON voaOrder.OrderId = voxOrder.OrderId
		WHERE voxOrder.BrokerId = @BrokerId
			AND voxOrder.LoanId = @LoanId
			
		SELECT
			d.OrderId, d.DocumentId
		FROM	
			VOX_DOCUMENT d JOIN
			VOX_ORDER o ON d.OrderId=o.OrderId
		WHERE
			o.BrokerId=@BrokerId AND
			o.LoanId=@LoanId

		SELECT
			vri.OrderId, vri.VendorReferenceId
		FROM
			VOX_ORDER_VENDOR_REFERENCE_ID vri JOIN
			VOX_ORDER o on vri.OrderId=o.OrderId
		WHERE
			o.BrokerId=@BrokerId AND
			o.LoanId=@LoanId

        SELECT va.OrderId, va.RowId, va.FinancialInstitutionName, va.EncryptedAccountNumber, va.AccountBalance, va.AssetType, va.EncryptionKeyId
		FROM VOA_ORDER_ASSET_VERIFIED va
			JOIN VOA_ORDER voaOrder ON va.OrderId = voaOrder.OrderId
			JOIN VOX_ORDER voxOrder ON voaOrder.OrderId = voxOrder.OrderId
		WHERE voxOrder.BrokerId = @BrokerId
			AND voxOrder.LoanId = @LoanId

        SELECT o.OrderId, statuses.StatusCode, statuses.StatusDescription, statuses.StatusTime
        FROM     
            VOX_ORDER [o] JOIN 
            VOX_ORDER_STATUSES statuses ON o.OrderId = statuses.OrderId
        WHERE   
            o.BrokerId = @BrokerId AND
            o.LoanId = @LoanId 
	END
END
