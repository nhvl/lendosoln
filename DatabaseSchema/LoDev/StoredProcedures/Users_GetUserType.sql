-- =============================================
-- Author:		Antonio Valencia
-- Create date: 4/24/2012
-- Description:	Get UserType from userId
-- =============================================
CREATE PROCEDURE dbo.[Users_GetUserType] 
	-- Add the parameters for the stored procedure here
	@userid uniqueidentifier 
AS
BEGIN

	SELECT [Type] as UserType  from ALL_USER where userid = @userid
END
