CREATE   PROCEDURE GetLicense
	@LicenseId Guid
AS
	SELECT *
	FROM License with(nolock)
	WHERE LicenseId = @LicenseId
