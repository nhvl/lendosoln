-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- 07/31/2013 - OPM 118577 - Added RequestXml as selected field
-- =============================================
CREATE PROCEDURE [dbo].[XSLT_EXPORT_RESULT_STATUS_RetrieveByReportId] 
	@ReportId varchar(13),
	@UserId UniqueIdentifier
AS
BEGIN

	SELECT ReportName, CreatedDate, OutputFileName, Status, RequestXml FROM XSLT_EXPORT_RESULT_STATUS WHERE ReportId = @ReportId AND UserId=@UserId
END

