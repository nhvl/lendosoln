
CREATE  PROCEDURE [dbo].[ListRolePermissionsByBrokerId]
	@BrokerID uniqueidentifier,
	@RoleID uniqueidentifier
AS
	SELECT
		r.DefaultPermissions
	FROM
		ROLE_DEFAULT_PERMISSIONS AS r
	WHERE
		r.BrokerId = @BrokerID
		AND
		r.RoleId = @RoleID

