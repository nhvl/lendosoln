-- =============================================
-- Author:		Geoffrey Feltman
-- Create date: 9/9/13
-- Description:	Save fee revision for broker.
-- =============================================
CREATE PROCEDURE [dbo].[FEE_SERVICE_SaveRevision] 
	@BrokerId uniqueidentifier, 
	@Comments varchar(250),
	@FeeTemplateXmlContent varchar(max),
	@FileDbKey varchar(50),
	@UploadedByUserId uniqueidentifier,
	@UploadedD datetime,
	@IsTest bit
AS
BEGIN
	INSERT INTO FEE_SERVICE_REVISION (BrokerId, Comments, FeeTemplateXmlContent, FileDbKey, UploadedByUserId, UploadedD, IsTest)
	VALUES (@BrokerId, @Comments, @FeeTemplateXmlContent, @FileDbKey, @UploadedByUserId, @UploadedD, @IsTest)
	SELECT CAST( SCOPE_IDENTITY() as int)
END
