

CREATE   PROCEDURE [dbo].[SetLoginBlockExpirationDateByUserId]
	@UserId uniqueidentifier = '00000000-0000-0000-0000-000000000000',
	@NextAvailableLoginTime datetime 
AS
	UPDATE All_User
	Set LoginBlockExpirationD = @NextAvailableLoginTime
	WHERE UserId = @UserId
