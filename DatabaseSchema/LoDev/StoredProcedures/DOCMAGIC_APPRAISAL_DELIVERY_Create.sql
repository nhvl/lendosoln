-- =============================================
-- Author:		Timothy Jewell
-- Create date: 4/24/2018
-- Description:	Creates a record of a request for DM appraisal delivery.
-- =============================================
ALTER PROCEDURE [dbo].[DOCMAGIC_APPRAISAL_DELIVERY_Create]
	@AppraisalDeliveryId uniqueidentifier,
	@BrokerId uniqueidentifier,
	@LoanId uniqueidentifier,
	@AppraisalOrderId uniqueidentifier,
	@DocVendorId uniqueidentifier,
	@SentDate datetime,
	@SentByName varchar(100),
	@SentByUserId uniqueidentifier,
	@WebsheetNumber varchar(100)
AS
BEGIN
	INSERT INTO DOCMAGIC_APPRAISAL_DELIVERY
	(
		AppraisalDeliveryId,
		BrokerId,
		LoanId,
		AppraisalOrderId,
		DocVendorId,
		SentDate,
		SentByName,
		SentByUserId,
		WebsheetNumber
	)
	VALUES
	(
		@AppraisalDeliveryId,
		@BrokerId,
		@LoanId,
		@AppraisalOrderId,
		@DocVendorId,
		@SentDate,
		@SentByName,
		@SentByUserId,
		@WebsheetNumber
	)
END
