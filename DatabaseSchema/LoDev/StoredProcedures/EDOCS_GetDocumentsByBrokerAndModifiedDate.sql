CREATE PROCEDURE [dbo].[EDOCS_GetDocumentsByBrokerAndModifiedDate] 
	@BrokerId uniqueidentifier,
	@ModifiedDate datetime
AS
BEGIN
	SELECT
	doc.sLId, 
	sLNm,
	FolderName,
	DocTypeName,
	DocumentId,
	doc.DocTypeId,
	app.aBFirstNm,
	app.aBLastNm,
	Description,
	InternalDescription,
	NumPages,
	LastModifiedDate,
	HideFromPmlUsers
	
	FROM	EDOCS_DOCUMENT doc
	left join Application_A app on app.aAppId = doc.aAppId
	inner join EDOCS_DOCUMENT_TYPE docType on doc.DocTypeId = docType.DocTypeId
	inner join EDOCS_FOLDER docFolder on docType.FolderId = docFolder.FolderId
	inner join LOAN_FILE_CACHE loanFileCache on doc.sLId = loanFileCache.sLId
			
	WHERE 
    	doc.IsValid = 1	
		AND doc.BrokerId = @BrokerId
		AND doc.LastModifiedDate > @ModifiedDate 
END
