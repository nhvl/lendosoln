

-- =============================================
-- Author:		<Francesc>
-- Create date: <04/29/10>
-- Description:	<Check if a consumer is locked based on its ID>
-- =============================================
CREATE PROCEDURE [dbo].[CP_IsConsumerLocked]
	@ConsumerId bigint
	
AS
BEGIN
	SET NOCOUNT ON;

	SELECT IsLocked
	FROM CP_CONSUMER
	WHERE ConsumerId = @ConsumerId AND IsLocked = 1

END


