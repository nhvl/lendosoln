

CREATE PROCEDURE [dbo].[GetActiveUserByEmployeeId]
	@EmployeeId uniqueidentifier,
	@Type char(1)
AS
	if @Type = 'P'
		BEGIN
			SELECT CAST(UserId as varchar(40)) + ',' + CAST(BrokerPmlSiteId as varchar(40)) AS ValueField FROM View_Active_Pml_User_With_Broker_Info WITH (NOLOCK) WHERE EmployeeId = @EmployeeId and Type = @Type
		END
	ELSE
		BEGIN
			SELECT BrokerId, UserId FROM View_Active_LO_User WITH (NOLOCK) WHERE EmployeeId = @EmployeeId and Type = @Type
		END	