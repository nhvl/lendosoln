-- =============================================
-- Author:		paoloa
-- Create date: 5/31/2013
-- Description:	
-- =============================================
CREATE PROCEDURE dbo.ConsumerPortal_DuplicateNames
	-- Add the parameters for the stored procedure here
	@Id uniqueidentifier,
	@BrokerId uniqueidentifier,
	@Name varchar(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT Id FROM CONSUMER_PORTAL
	WHERE Name = @Name AND BrokerId = @BrokerId AND NOT Id = @Id
END
