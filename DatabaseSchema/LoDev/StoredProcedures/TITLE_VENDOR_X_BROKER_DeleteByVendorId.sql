-- =============================================
-- Author:		David Dao
-- Create date: 4/17/2015
-- Description:	Deletes a title vendor
-- =============================================
CREATE PROCEDURE dbo.TITLE_VENDOR_X_BROKER_DeleteByVendorId
	-- Add the parameters for the stored procedure here
	@TitleVendorId int 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	
	DELETE FROM TITLE_VENDOR_X_BROKER 
	WHERE TitleVendorId = @TitleVendorId 

	
END
