CREATE PROCEDURE [dbo].[DOCCONDITION_GetByLoanCondition] 
	-- Add the parameters for the stored procedure here
	@LoanId uniqueidentifier,
	@TaskId varchar(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT sLId, TaskId, DocumentId, Status
	FROM TASK_x_EDOCS_DOCUMENT
	WHERE sLId = @LoanId
	AND TaskId = @TaskId
END