CREATE PROCEDURE [dbo].[Billing_CondoTek]
	@StartDate datetime,
	@EndDate datetime
AS
BEGIN
-- dd 2018-11-02 - For use in automate billing executable.
SELECT DISTINCT gfb.LoanId, lfc.sLNm as LoanNumber, b.BrokerNm as LenderName, b.CustomerCode
                            FROM GENERIC_FRAMEWORK_BILLING gfb JOIN LOAN_FILE_CACHE lfc on gfb.LoanID = lfc.sLId
                                                               JOIN BROKER b on gfb.BrokerId = b.BrokerId
                            WHERE ProviderId= 'VEN0095' -- CondoTek
                            AND gfb.DateUploaded >= @StartDate AND gfb.DateUploaded < @EndDate
                            AND b.SuiteType = 1 AND lfc.sLoanFileT = 0
                            AND NOT EXISTS
	                            (
		                            Select 1 FROM GENERIC_FRAMEWORK_BILLING gfb1
		                            Where ProviderId='VEN0095' -- CondoTek
		                            AND gfb1.LoanID = gfb.LoanID
		                            AND gfb1.DateUploaded < @StartDate
	                            )
                            ORDER BY LenderName
END