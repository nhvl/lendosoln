-- =============================================
-- Author:		David Dao
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE dbo.LPE_PRICE_GROUP_PRODUCT_ListByPriceGroupId
    @LpePriceGroupId UniqueIdentifier,
    @BrokerId UniqueIdentifier
AS
BEGIN

    SELECT p.LpePriceGroupId, p.lLpTemplateId, p.LenderProfitMargin, p.LenderMaxYspAdjustLckd,
           p.LenderMaxYspAdjust, p.LenderRateMargin, p.IsValid, p.LenderMarginMargin
    FROM LPE_PRICE_GROUP_PRODUCT p JOIN LPE_PRICE_GROUP g ON p.LpePriceGroupId = g.LpePriceGroupId
    WHERE g.LpePriceGroupId = @LpePriceGroupId
      AND g.BrokerId = @BrokerId


END
