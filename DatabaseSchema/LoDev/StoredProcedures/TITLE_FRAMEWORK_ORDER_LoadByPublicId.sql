-- =============================================
-- Author:		Eric Mallare
-- Create date: 9/25/2017
-- Description:	Loads a TitleOrder by public id. 
-- =============================================
ALTER PROCEDURE [dbo].[TITLE_FRAMEWORK_ORDER_LoadByPublicId]
	@BrokerId uniqueidentifier,
	@LoanId uniqueidentifier,
	@PublicId uniqueidentifier
AS
BEGIN
	SELECT
		OrderId,
		PublicId,
		BrokerId,
		LoanId,
		TransactionId,
		OrderNumber,
		[Status],
		OrderedBy,
		OrderedDate,
		IsTestOrder,
		ResultsFileDbKey,
		FeesFileDbKey,
		ConfigFileDbKey,
		LenderServiceId,
		LenderServiceName,
		TitleProviderCode,
		TitleProviderName,
		TitleProviderRolodexId,
		ProductType,
		AppliedDate
	FROM TITLE_FRAMEWORK_ORDER
	WHERE
		LoanId = @LoanId AND 
		BrokerId = @BrokerId AND
		PublicId = @PublicId
END
