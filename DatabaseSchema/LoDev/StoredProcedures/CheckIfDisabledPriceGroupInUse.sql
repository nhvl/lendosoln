-- =============================================
-- Author:		Michael Leinweaver
-- Create date: 3/8/2017
-- Description:	Checks to see if a price group to be
--				disabled is in use at the user, branch,
--				originating company, or broker level.
-- =============================================
ALTER PROCEDURE [dbo].[CheckIfDisabledPriceGroupInUse]
	@LpePriceGroupId uniqueidentifier,
	@BrokerId uniqueidentifier
AS
BEGIN
	DECLARE 
		@IsSetForBUser bit, 
		@IsSetForPUser bit, 
		@IsSetForBranch bit, 
		@IsSetForOc bit, 
		@IsSetForCorporate bit, 
		@IsSetForTpoPortalDefault bit
	
	SELECT @IsSetForBUser = CASE WHEN EXISTS (
		SELECT 1
		FROM VIEW_EMPLOYEE_CONTACT_INFO
		WHERE 
			BrokerId = @BrokerId AND
			EmployeeIsActive = 1 AND 
			[Type] = 'B' AND (
			LpePriceGroupID = @LpePriceGroupId OR
			MiniCorrespondentPriceGroupID = @LpePriceGroupId OR
			CorrespondentPriceGroupID = @LpePriceGroupId)
	) THEN 1 ELSE 0 END
	
	SELECT @IsSetForPUser = CASE WHEN EXISTS (
		SELECT 1
		FROM VIEW_EMPLOYEE_CONTACT_INFO
		WHERE 
			BrokerId = @BrokerId AND
			EmployeeIsActive = 1 AND 
			[Type] = 'P' AND (
			LpePriceGroupID = @LpePriceGroupId OR
			MiniCorrespondentPriceGroupID = @LpePriceGroupId OR
			CorrespondentPriceGroupID = @LpePriceGroupId)
	) THEN 1 ELSE 0 END
	
	SELECT @IsSetForBranch = CASE WHEN EXISTS (
		SELECT 1 
		FROM BRANCH
		WHERE 
			BrokerId = @BrokerId AND
			BranchLpePriceGroupIdDefault = @LpePriceGroupId
	) THEN 1 ELSE 0 END
	
	SELECT @IsSetForOc = CASE WHEN EXISTS (
		SELECT 1 
		FROM PML_BROKER
		WHERE
			BrokerId = @BrokerId AND
			IsActive = 1 AND (
			LpePriceGroupID = @LpePriceGroupId OR
			MiniCorrespondentLpePriceGroupID = @LpePriceGroupId OR
			CorrespondentLpePriceGroupID = @LpePriceGroupId)
	) THEN 1 ELSE 0 END
	
	SELECT @IsSetForCorporate = CASE WHEN EXISTS (
		SELECT 1
		FROM BROKER
		WHERE 
			Status = 1 AND
			BrokerLpePriceGroupIdDefault = @LpePriceGroupID
	) THEN 1 ELSE 0 END
	
	SELECT @IsSetForTpoPortalDefault = CASE WHEN EXISTS (
		SELECT 1 
		FROM BROKER
		WHERE
			BrokerId = @BrokerId AND
			Status = 1 AND (
			WholesalePriceGroupID = @LpePriceGroupId OR
			MiniCorrPriceGroupID = @LpePriceGroupId OR
			CorrespondentPriceGroupID = @LpePriceGroupId)
	) THEN 1 ELSE 0 END
	
	SELECT 
		@IsSetForBUser as 'IsSetForBUser', 
		@IsSetForPUser as 'IsSetForPUser',
		@IsSetForBranch as 'IsSetForBranch', 
		@IsSetForOc as 'IsSetForOc', 
		@IsSetForCorporate as 'IsSetForCorporate',
		@IsSetForTpoPortalDefault as 'IsSetForTpoPortalDefault'
END