

CREATE PROCEDURE [dbo].[TEAM_DeleteTeam] 
	@TeamId			uniqueidentifier,
	@BrokerId		uniqueidentifier
AS
	BEGIN TRANSACTION
	
	DELETE FROM TEAM_LOAN_ASSIGNMENT
	WHERE @TeamId = TeamId

	IF( @@error != 0 )
   	GOTO HANDLE_ERROR
		
	DELETE FROM TEAM_USER_ASSIGNMENT
	WHERE @TeamId = TeamId
	
	IF( @@error != 0 )
   	GOTO HANDLE_ERROR
	
	DELETE FROM TEAM
	WHERE 
	@TeamId = Id
	AND
	@BrokerId = BrokerId

	IF( @@error != 0 )
   	GOTO HANDLE_ERROR

	COMMIT TRANSACTION
	
	RETURN;

   HANDLE_ERROR:
      	ROLLBACK TRANSACTION
     	RAISERROR('Error in [TEAM_DeleteTeam] sp', 16, 1);;
     	RETURN;


