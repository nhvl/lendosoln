CREATE  PROCEDURE ListDiscNotifications
	@UserId Guid = NULL, @IsValid Bit = NULL
AS
	SELECT DISTINCT n.NotifId, n.NotifIsValid, n.NotifReceiverUserId, n.NotifIsRead, n.NotifAlertDate, n.NotifUpdatedDate, n.NotifLastReadD, n.NotifAckD, n.NotifInvalidDate, n.DiscLastModifyD, n.DiscLogId, n.NotifBrokerId
	FROM 
Discussion_Notification AS n with(nolock)
JOIN Discussion_Log AS l with(nolock) ON n.DiscLogId = l.DiscLogId 
LEFT JOIN Loan_File_Cache AS c with(nolock) ON c.sLId = l.DiscRefObjId
	WHERE
		n.NotifReceiverUserId = COALESCE( @UserId , n.NotifReceiverUserId )
		AND
		n.NotifIsValid = COALESCE( @IsValid , n.NotifIsValid )
		AND
		ISNULL( c.IsTemplate , 0 ) = 0
		AND
		l.DiscIsRefObjValid = 1
		AND
		l.IsValid = 1
	if( 0!=@@error)
	begin
		RAISERROR('Error in the selection statement in ListDiscNotifications sp', 16, 1);
		return -100;
	end
	return 0;
