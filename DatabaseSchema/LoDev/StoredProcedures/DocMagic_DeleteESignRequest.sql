
-- =============================================
-- Author:		Antonio Valencia
-- Create date: 4/16/2012
-- Description:	Finds a doc magic request 
-- =============================================
CREATE PROCEDURE dbo.[DocMagic_DeleteESignRequest]
	@Id int

AS
BEGIN
	DELETE FROM DOCMAGIC_PENDING_ESIGN_REQUEST
	WHERE Id = @Id
END

