ALTER PROCEDURE [dbo].[RetrieveEnabledMultiFactorAuthenticationByEmployeeId]
	@EmployeeId uniqueidentifier
AS
BEGIN
	SELECT EnabledMultiFactorAuthentication
	FROM Employee e join Broker_User bu on e.EmployeeId = bu.EmployeeId
	WHERE e.EmployeeId = @EmployeeId
END