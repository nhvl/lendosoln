ALTER PROCEDURE dbo.LPE_RELEASE_RetrieveLatestManualImport
AS
SELECT TOP 1 ReleaseVersion, SnapshotId, DataLastModifiedD, FileKey
FROM LPE_RELEASE
WHERE SnapshotId = 'ManualImport'
ORDER BY ReleaseVersion DESC
