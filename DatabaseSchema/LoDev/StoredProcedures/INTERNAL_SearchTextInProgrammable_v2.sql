


CREATE Procedure [dbo].[INTERNAL_SearchTextInProgrammable_v2]
	@id as int,
	@type as varchar(2),
	@date as int,
	@str as varchar(1000)
as

declare @like as varchar(1002)
set @like = '%' + @str + '%'

if (@id is null)
BEGIN
	SELECT object_id AS id, object_name(object_id) AS name, len(OBJECT_DEFINITION(object_id)) AS length, OBJECT_DEFINITION(object_id) AS code
	FROM   sys.objects
	WHERE OBJECT_DEFINITION(object_id) LIKE @like
		AND type LIKE @type
		AND DATEDIFF(D, modify_date, GETDATE() )< @date
	GROUP BY object_id
END
else
BEGIN
	SELECT object_id AS id, object_name(object_id) AS name, len(OBJECT_DEFINITION(object_id)) AS length, OBJECT_DEFINITION(object_id) AS code
	FROM   sys.objects
	WHERE object_id=@id AND OBJECT_DEFINITION(object_id) LIKE @like
	GROUP BY object_id
END




