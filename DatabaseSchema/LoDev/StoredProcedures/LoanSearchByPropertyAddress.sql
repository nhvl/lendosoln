ALTER procedure [dbo].[LoanSearchByPropertyAddress]
	@BrokerID uniqueidentifier,
	@sSpAddr varchar(60) = null,
	@sSpCity varchar(36) = null, 
	@sSpState varchar(2) = null, 
	@sSpZip varchar(5) = null,
	@sStatusD smalldatetime = null
AS

select 
c.sLid, c.slnm, c.sLPurposeT, c.sStatusT, c.sStatusD, c.sLienPosT, c.sSpAddr, c.sSpCity, c.sSpState, c.sSpZip, c.sSpCounty,
c2.sGseSpT,
c4.sEncryptionKey, c4.sEncryptionMigrationVersion, c4.sPrimaryAppId,
a.aAppId, a.aBFirstNm, a.aBLastNm, a.aCFirstNm, a.aCLastNm, a.aBEmail, a.aCEmail, a.aBHPhone, a.aCHPhone, a.aBBusPhone, a.aCBusPhone, b.aBCellphone, b.aCCellphone,
a.aBSsnEncrypted, a.aCSsnEncrypted

from application_a a 
join application_b b on a.aAppId=b.aAppId
join loan_file_cache c on a.slid = c.slid
join loan_file_cache_4 c4 on a.slid = c4.slid 
join loan_file_cache_2 c2 on c4.slid=c2.slid
where 

c.sbrokerid = @BrokerID 
and c.istemplate = 0 
and c.sLoanFileT = 0 
and c.isvalid = 1 
and (@sSpAddr is null OR @sSpAddr = sSpAddr) 
and (@sSpCity is null OR @sSpCity = sSpCity)
and (@sSpState is null OR @sSpState = sSpState)
and (@sSpZip is null OR @sSpZip = sSpZip)
and (@sStatusD is null OR c.sStatusD >= @sStatusD)