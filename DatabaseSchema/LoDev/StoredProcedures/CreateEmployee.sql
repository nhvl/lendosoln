﻿ALTER PROCEDURE [dbo].[CreateEmployee] 
	@UserID uniqueidentifier OUT,
	@EmployeeID uniqueidentifier OUT,
	@BranchID uniqueidentifier,
	@FirstName Varchar( 21 ),
	@MiddleName Varchar ( 21 ) = '',
	@LastName Varchar( 21 ),
	@Suffix Varchar( 10 ) = '',
	@NmOnLoanDocs Varchar( 100 ) = '',
	@NmOnLoanDocsLckd Bit = false,
	@Address Varchar( 60 ),
	@City Varchar( 36 ),
	@State char( 2 ),
	@Zipcode char( 5 ),
	@Phone Varchar( 21 ),
	@Fax Varchar( 21 ) = '',
	@CellPhone Varchar( 21 ) = '',
	@Pager Varchar( 21 ) = '',
	@LoginName Varchar( 36 ),
	@PasswordHash Varchar( 200 ),
	@Email Varchar( 80 ),
	@IsActive Bit,
	@ManagerEmployeeID uniqueidentifier,
	@LenderAccExecEmployeeID uniqueidentifier,
	@LockDeskEmployeeID uniqueidentifier,
	@UnderwriterEmployeeID uniqueidentifier,
	@ProcessorEmployeeID uniqueidentifier,
	@PmlSiteID uniqueidentifier,
	@LpePriceGroupID uniqueidentifier,
	@PmlExternalManagerEmployeeId uniqueidentifier,
	@IsPmlManager Bit = false,
	@IsAccountOwner Bit,
	@EncryptionKeyId uniqueidentifier = NULL,
	@WhoDoneIt uniqueidentifier = NULL,
	@NewOnlineLoanEventNotifOptionT int,
	@TaskRelatedEmailOptionT int,
	@PasswordExpirationD SmallDateTime = NULL,
	@PasswordHistory Text = NULL,
	@PasswordExpirationPeriod int = 0,
	@EmployeeLicenseNumber Varchar( 30 ) = '',
	@NotesByEmployer Text = '',
	@UserType Varchar( 1 ) = 'B',
	@CommissionPointOfLoanAmount decimal(9,3) = 0,
	@CommissionPointOfGrossProfit decimal(9,3) = 0,
	@CommissionMinBase decimal(9,2) = 0,
	@IsSharable Bit = false,
	@LicenseXmlContent text = '',
	@DUAutoLoginNm Varchar( 50 ) = '',
	@DUAutoPassword Varchar( 50 ) = '',
	@EncryptedDUAutoPassword varbinary(50) = 0x,
	@DOAutoLoginNm Varchar( 50 ) = '',
	@DOAutoPassword Varchar( 50 ) = '',
	@EncryptedDOAutoPassword varbinary(50) = 0x,
	@LpeRsExpirationByPassPermission Varchar(100),
	@MustLogInFromTheseIpAddresses varchar(200) = '',
	@IsQuickPricerEnabled bit = false,
	@AnonymousQuickPricerUserId uniqueidentifier = null,
	@IsPricingMultipleAppsSupported bit = false,
	@PmlBrokerId UniqueIdentifier,
	@PipelineCustomReportSettingsXmlContent varchar(500) = '',
	@MclUserId int = 0,
	@NmLsIdentifier varchar(50) = '',
	@PmlLevelAccess int = 0,
	@BrokerProcessorEmployeeId uniqueidentifier,
	@IsUserTPO bit = 0,
    @OriginatorCompensationAuditXml varchar(max),
    @OriginatorCompensationPercent decimal(9, 3),
    @OriginatorCompensationBaseT int,
    @OriginatorCompensationMinAmount money,
    @OriginatorCompensationMaxAmount money,
    @OriginatorCompensationFixedAmount money,
    @OriginatorCompensationNotes varchar(200),
    @OriginatorCompensationLastModifiedDate datetime = null,
	@OriginatorCompensationSetLevelT int,
	@IsNewPmlUIEnabled bit = 0,
	@EmployeeIDInCompany varchar(30) = '',
	@AssignedPrintGroups varchar(max) = null,
	@EmployeeStartD smalldatetime = null,
	@EmployeeTerminationD smalldatetime = null,
	@JuniorProcessorEmployeeID uniqueidentifier,
	@JuniorUnderwriterEmployeeID uniqueidentifier,
	@ShowQMStatusInPml2 bit = 0,
	@CustomPricingPolicyField1Fixed varchar(25),
	@CustomPricingPolicyField2Fixed varchar(25),
	@CustomPricingPolicyField3Fixed varchar(25),
	@CustomPricingPolicyField4Fixed varchar(25),
	@CustomPricingPolicyField5Fixed varchar(25),
	@CustomPricingPolicyFieldSource int, 
	@PMLNavigationPermissions varchar(max),
	@Lender_TPO_LandingPageConfigId uniqueidentifier = null,
	@IsActiveDirectoryUser bit = false,
	@ChumsID varchar(8),
	@LoanOfficerAssistantEmployeeID uniqueidentifier,
	@EnabledIpRestriction bit = false,
	@EnabledMultiFactorAuthentication bit = true,
	@EnabledClientDigitalCertificateInstall bit = false,
	@CanBeMemberOfMultipleTeams bit = 0,
	@UseOriginatingCompanyBranchId bit = 0,
	@UseOriginatingCompanyLpePriceGroupId bit = 0,
	@MiniCorrManagerEmployeeId uniqueidentifier = NULL,
	@MiniCorrProcessorEmployeeId uniqueidentifier = NULL,
	@MiniCorrJuniorProcessorEmployeeId uniqueidentifier = NULL,
	@MiniCorrLenderAccExecEmployeeId uniqueidentifier = NULL,
	@MiniCorrExternalPostCloserEmployeeId uniqueidentifier = NULL,
	@MiniCorrUnderwriterEmployeeId uniqueidentifier = NULL,
	@MiniCorrJuniorUnderwriterEmployeeId uniqueidentifier = NULL,
	@MiniCorrCreditAuditorEmployeeId uniqueidentifier = NULL,
	@MiniCorrLegalAuditorEmployeeId uniqueidentifier = NULL,
	@MiniCorrLockDeskEmployeeId uniqueidentifier = NULL,
	@MiniCorrPurchaserEmployeeId uniqueidentifier = NULL,
	@MiniCorrSecondaryEmployeeId uniqueidentifier = NULL,
	@CorrManagerEmployeeId uniqueidentifier = NULL,
	@CorrProcessorEmployeeId uniqueidentifier = NULL,
	@CorrJuniorProcessorEmployeeId uniqueidentifier = NULL,
	@CorrLenderAccExecEmployeeId uniqueidentifier = NULL,
	@CorrExternalPostCloserEmployeeId uniqueidentifier = NULL,
	@CorrUnderwriterEmployeeId uniqueidentifier = NULL,
	@CorrJuniorUnderwriterEmployeeId uniqueidentifier = NULL,
	@CorrCreditAuditorEmployeeId uniqueidentifier = NULL,
	@CorrLegalAuditorEmployeeId uniqueidentifier = NULL,
	@CorrLockDeskEmployeeId uniqueidentifier = NULL,
	@CorrPurchaserEmployeeId uniqueidentifier = NULL,
	@CorrSecondaryEmployeeId uniqueidentifier = NULL,
	@CorrespondentLandingPageID uniqueidentifier = NULL,
	@MiniCorrespondentLandingPageID uniqueidentifier = NULL,
	@UseOriginatingCompanyCorrPriceGroupId bit = 0,
	@CorrespondentPriceGroupId uniqueidentifier = NULL,
	@UseOriginatingCompanyMiniCorrPriceGroupId bit = 0,
	@MiniCorrespondentPriceGroupId uniqueidentifier = NULL,
	@UseOriginatingCompanyCorrBranchId bit = 0,
	@CorrespondentBranchId uniqueidentifier = NULL,
	@UseOriginatingCompanyMiniCorrBranchId bit = 0,
	@MiniCorrespondentBranchId uniqueidentifier = NULL,
	@PortalMode int = 0,
	@SupportEmail varchar(320) = null,
	@SupportEmailVerified bit = 0,
	@CanContactSupport bit = 0,
	@FavoritePageIdsJSON varchar(max)= '',
	@IsUpdatedPassword bit,
	@PasswordSalt varchar(128),
	@OptsToUseNewLoanEditorUI bit = 1,
	@PopulateBrokerRelationshipsT int = NULL,
	@PopulateMiniCorrRelationshipsT int = NULL,
	@PopulateCorrRelationshipsT int = NULL,
	@PopulatePMLPermissionsT int = NULL,
	@UserTheme int = 0,
	@IsCellphoneForMultiFactorOnly bit = 0,
	@EnableAuthCodeViaSms bit = 1,
	@OriginatorCompensationIsOnlyPaidForFirstLienOfCombo bit = 0,
	@EncryptedMfaOtpSeed varbinary(200) = 0x

AS
	declare @tranPoint sysname
	set @tranPoint = object_name(@@procId) + CAST(@@nestlevel as varchar(10))
	BEGIN TRANSACTION
	SAVE TRANSACTION @tranPoint
	SET @EmployeeID = newid();
	
	IF @PopulateBrokerRelationshipsT IS NULL
	BEGIN
		SELECT @PopulateBrokerRelationshipsT = CASE WHEN @UserType = 'B' THEN 1 ELSE 0 END
	END
	
	IF @PopulateMiniCorrRelationshipsT IS NULL
	BEGIN
		SELECT @PopulateMiniCorrRelationshipsT = CASE WHEN @UserType = 'B' THEN 1 ELSE 0 END
	END
	
	IF @PopulateCorrRelationshipsT IS NULL
	BEGIN
		SELECT @PopulateCorrRelationshipsT = CASE WHEN @UserType = 'B' THEN 1 ELSE 0 END
	END
	
	IF @PopulatePMLPermissionsT IS NULL
	BEGIN
		SELECT @PopulatePMLPermissionsT = CASE WHEN @UserType = 'B' THEN 1 ELSE 0 END
	END
	
	INSERT INTO Employee (EmployeeID, BranchID, UserFirstNm, UserMiddleNm , UserLastNm, UserSuffix, UserNmOnLoanDocs, UserNmOnLoanDocsLckd,  Addr, City, State, Zip, Phone, Fax, Email, IsActive, IsPmlManager, NewOnlineLoanEventNotifOptionT, TaskRelatedEmailOptionT, NotesByEmployer, EmployeeLicenseNumber, CommissionPointOfLoanAmount, CommissionPointOfGrossProfit, CommissionMinBase, CellPhone, Pager, LicenseXmlContent, NmLsIdentifier, IsUserTPO, OriginatorCompensationAuditXml, OriginatorCompensationPercent, OriginatorCompensationBaseT, OriginatorCompensationMinAmount, OriginatorCompensationMaxAmount, OriginatorCompensationFixedAmount, OriginatorCompensationNotes, OriginatorCompensationLastModifiedDate, OriginatorCompensationSetLevelT, EmployeeIDInCompany, EmployeeStartD, EmployeeTerminationD, CustomPricingPolicyField1Fixed,CustomPricingPolicyField2Fixed, CustomPricingPolicyField3Fixed, CustomPricingPolicyField4Fixed,CustomPricingPolicyField5Fixed, CustomPricingPolicyFieldSource, ChumsID, CanBeMemberOfMultipleTeams, UseOriginatingCompanyBranchId, UseOriginatingCompanyCorrBranchId, CorrespondentBranchId, UseOriginatingCompanyMiniCorrBranchId, MiniCorrespondentBranchId, CanContactSupport, FavoritePageIdsJSON, PopulateBrokerRelationshipsT, PopulateMiniCorrRelationshipsT, PopulateCorrRelationshipsT, PopulatePMLPermissionsT, IsCellphoneForMultiFactorOnly, OriginatorCompensationIsOnlyPaidForFirstLienOfCombo)
	                          VALUES(@EmployeeID, @BranchID, @FirstName, @MiddleName, @LastName, @Suffix, @NmOnLoanDocs, @NmOnLoanDocsLckd, @Address, @City, @State, @Zipcode, @Phone, @Fax, @Email, @IsActive, @IsPmlManager, @NewOnlineLoanEventNotifOptionT, @TaskRelatedEmailOptionT, @NotesByEmployer, @EmployeeLicenseNumber, @CommissionPointOfLoanAmount, @CommissionPointOfGrossProfit, @CommissionMinBase, @CellPhone, @Pager, @LicenseXmlContent, @NmLsIdentifier, @IsUserTPO, @OriginatorCompensationAuditXml, @OriginatorCompensationPercent, @OriginatorCompensationBaseT, @OriginatorCompensationMinAmount, @OriginatorCompensationMaxAmount, @OriginatorCompensationFixedAmount, @OriginatorCompensationNotes, @OriginatorCompensationLastModifiedDate, @OriginatorCompensationSetLevelT, @EmployeeIDInCompany, @EmployeeStartD, @EmployeeTerminationD,  @CustomPricingPolicyField1Fixed,@CustomPricingPolicyField2Fixed, @CustomPricingPolicyField3Fixed, @CustomPricingPolicyField4Fixed,@CustomPricingPolicyField5Fixed,@CustomPricingPolicyFieldSource, @ChumsID, @CanBeMemberOfMultipleTeams, @UseOriginatingCompanyBranchId, @UseOriginatingCompanyCorrBranchId, @CorrespondentBranchId, @UseOriginatingCompanyMiniCorrBranchId, @MiniCorrespondentBranchId, @CanContactSupport, @FavoritePageIdsJSON, @PopulateBrokerRelationshipsT, @PopulateMiniCorrRelationshipsT, @PopulateCorrRelationshipsT, @PopulatePMLPermissionsT, @IsCellphoneForMultiFactorOnly, @OriginatorCompensationIsOnlyPaidForFirstLienOfCombo);
	if( 0!=@@error )
	begin
		RAISERROR('error in inserting into Employee table in CreateEmployee sp', 16, 1);;
		goto ErrorHandler;
	end
	IF @LoginName IS NOT NULL
	BEGIN
		DECLARE @vNewUserID uniqueidentifier
		DECLARE @BrokerPmlSiteId uniqueidentifier
		SET @vNewUserID = newid();
		SET @UserID     = @vNewUserID
		SET @BrokerPmlSiteId = '00000000-0000-0000-0000-000000000000'
		IF @UserType <> 'B'
			SELECT @BrokerPmlSiteId = b.BrokerPmlSiteId FROM Branch AS r WITH (NOLOCK) JOIN Broker AS b WITH (NOLOCK) ON b.BrokerId = r.BrokerId AND r.BranchId = @BranchId
			
		INSERT INTO All_User(UserID, LoginNm, PasswordHash, Type, IsActive, PasswordExpirationD, PasswordHistory, PasswordExpirationPeriod, IsSharable, BrokerPmlSiteId, MclUserId, IsActiveDirectoryUser, IsUpdatedPassword, PasswordSalt, EncryptedMfaOtpSeed)
		                      VALUES (@vNewUserID, @LoginName, @PasswordHash, @UserType, @IsActive, @PasswordExpirationD, @PasswordHistory, @PasswordExpirationPeriod, @IsSharable, @BrokerPmlSiteId, @MclUserId, @IsActiveDirectoryUser, @IsUpdatedPassword, @PasswordSalt, @EncryptedMfaOtpSeed);
		if( 0!=@@error )
		BEGIN
			RAISERROR('error in inserting into All_User table in CreateEmployee sp', 16, 1);;
			goto ErrorHandler;
		END
		IF @LpePriceGroupId IS NOT NULL AND @LpePriceGroupId = '00000000-0000-0000-0000-000000000000'
			SET @LpePriceGroupId = NULL
		INSERT INTO Broker_User(PmlBrokerId, UserID, EmployeeID, StartD, IsAccountOwner, EncryptionKeyId, PmlSiteID, LpePriceGroupID, LenderAccExecEmployeeID, LockDeskEmployeeID, UnderwriterEmployeeID, PmlExternalManagerEmployeeId, ManagerEmployeeId, ProcessorEmployeeId, DUAutoLoginNm, DUAutoPassword, EncryptedDUAutoPassword, DOAutoLoginNm, DOAutoPassword, EncryptedDOAutoPassword, LpeRsExpirationByPassPermission, MustLogInFromTheseIpAddresses, IsQuickPricerEnabled,AnonymousQuickPricerUserId,IsPricingMultipleAppsSupported, PipelineCustomReportSettingsXmlContent, PmlLevelAccess, BrokerProcessorEmployeeId, IsNewPmlUIEnabled, JuniorProcessorEmployeeID, JuniorUnderwriterEmployeeID, ShowQMStatusInPml2, Lender_TPO_LandingPageConfigId, PMLNavigationPermissions, LoanOfficerAssistantEmployeeID, 
		EnabledIpRestriction,EnabledMultiFactorAuthentication,EnabledClientDigitalCertificateInstall, UseOriginatingCompanyLpePriceGroupId,
		CorrespondentLandingPageID, MiniCorrespondentLandingPageID, UseOriginatingCompanyCorrPriceGroupId, CorrespondentPriceGroupId, UseOriginatingCompanyMiniCorrPriceGroupId, MiniCorrespondentPriceGroupId,
		MiniCorrManagerEmployeeId, MiniCorrProcessorEmployeeId, MiniCorrJuniorProcessorEmployeeId, MiniCorrLenderAccExecEmployeeId, MiniCorrExternalPostCloserEmployeeId, MiniCorrUnderwriterEmployeeId, MiniCorrJuniorUnderwriterEmployeeId, MiniCorrCreditAuditorEmployeeId, MiniCorrLegalAuditorEmployeeId, MiniCorrLockDeskEmployeeId, MiniCorrPurchaserEmployeeId, MiniCorrSecondaryEmployeeId,
		CorrManagerEmployeeId, CorrProcessorEmployeeId, CorrJuniorProcessorEmployeeId, CorrLenderAccExecEmployeeId, CorrExternalPostCloserEmployeeId, CorrUnderwriterEmployeeId, CorrJuniorUnderwriterEmployeeId, CorrCreditAuditorEmployeeId, CorrLegalAuditorEmployeeId, CorrLockDeskEmployeeId, CorrPurchaserEmployeeId, CorrSecondaryEmployeeId, PortalMode, AssignedPrintGroups, SupportEmail, SupportEmailVerified, OptsToUseNewLoanEditorUI, UserTheme, EnableAuthCodeViaSms)
	             	VALUES (@PmlBrokerId, @vNewUserID, @EmployeeID, getDate(), @IsAccountOwner, @EncryptionKeyId, @PmlSiteID, @LpePriceGroupID, @LenderAccExecEmployeeID, @LockDeskEmployeeID, @UnderwriterEmployeeID, @PmlExternalManagerEmployeeId, @ManagerEmployeeId, @ProcessorEmployeeId, @DUAutoLoginNm, @DUAutoPassword, @EncryptedDUAutoPassword, @DOAutoLoginNm, @DOAutoPassword, @EncryptedDOAutoPassword, @LpeRsExpirationByPassPermission, @MustLogInFromTheseIpAddresses, @IsQuickPricerEnabled, @AnonymousQuickPricerUserId,@IsPricingMultipleAppsSupported, @PipelineCustomReportSettingsXmlContent, @PmlLevelAccess, @BrokerProcessorEmployeeId, @IsNewPmlUIEnabled, @JuniorProcessorEmployeeID, @JuniorUnderwriterEmployeeID, @ShowQMStatusInPml2, @Lender_TPO_LandingPageConfigId, @PMLNavigationPermissions, @LoanOfficerAssistantEmployeeID, 
					@EnabledIpRestriction,@EnabledMultiFactorAuthentication,@EnabledClientDigitalCertificateInstall, @UseOriginatingCompanyLpePriceGroupId,
					@CorrespondentLandingPageID, @MiniCorrespondentLandingPageID, @UseOriginatingCompanyCorrPriceGroupId, @CorrespondentPriceGroupId, @UseOriginatingCompanyMiniCorrPriceGroupId, @MiniCorrespondentPriceGroupId,
					@MiniCorrManagerEmployeeId, @MiniCorrProcessorEmployeeId, @MiniCorrJuniorProcessorEmployeeId, @MiniCorrLenderAccExecEmployeeId, @MiniCorrExternalPostCloserEmployeeId, @MiniCorrUnderwriterEmployeeId, @MiniCorrJuniorUnderwriterEmployeeId, @MiniCorrCreditAuditorEmployeeId, @MiniCorrLegalAuditorEmployeeId, @MiniCorrLockDeskEmployeeId, @MiniCorrPurchaserEmployeeId, @MiniCorrSecondaryEmployeeId,
					@CorrManagerEmployeeId, @CorrProcessorEmployeeId, @CorrJuniorProcessorEmployeeId, @CorrLenderAccExecEmployeeId, @CorrExternalPostCloserEmployeeId, @CorrUnderwriterEmployeeId, @CorrJuniorUnderwriterEmployeeId, @CorrCreditAuditorEmployeeId, @CorrLegalAuditorEmployeeId, @CorrLockDeskEmployeeId, @CorrPurchaserEmployeeId, @CorrSecondaryEmployeeId,  @PortalMode, @AssignedPrintGroups, @SupportEmail, @SupportEmailVerified, @OptsToUseNewLoanEditorUI, @UserTheme, @EnableAuthCodeViaSms);
		if( 0!=@@error )
		BEGIN
			RAISERROR('error in inserting into Broker_User table in CreateEmployee sp', 16, 1);;
			goto ErrorHandler;
		END
		IF @WhoDoneIt IS NULL
		BEGIN	
			SET @WhoDoneIt = @vNewUserID
		END
		IF @IsActive = 1 AND @WhoDoneIt IS NOT NULL
		BEGIN
			DECLARE @BrokerID uniqueidentifier
			SELECT @BrokerID = BrokerID FROM Branch WITH (NOLOCK) WHERE BranchID = @BranchID
			INSERT INTO Usage_event(FeatureId, UserId, BrokerId,  WhoDoneIt, EventType) 
	                                     VALUES ('00000000-0000-0000-0000-000000000000', @vNewUserId, @BrokerId, @WhoDoneIt, 'o')
			IF( 0!=@@error )
			BEGIN
				RAISERROR('error in inserting into Usage_Event table in CreateEmployee sp', 16, 1);;
				goto ErrorHandler;
			END
		END
	END
	COMMIT TRANSACTION;
	return 0;
	ErrorHandler:
		rollback tran @tranPoint
		rollback transaction
		return -100;
