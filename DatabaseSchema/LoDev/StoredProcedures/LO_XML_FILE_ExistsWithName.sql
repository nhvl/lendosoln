/*
	Author: 
		Scott Kibler
	Description: 
		Checks if the file exists with the given name and 
		sets the @FileExists OUTPUT parameter with 0 = no, 1 = yes.
	opm:
		463659		
*/
CREATE PROCEDURE [dbo].[LO_XML_FILE_ExistsWithName]
	@Name varchar(255),
	@FileExists bit = NULL OUTPUT
AS
BEGIN
	SELECT @FileExists = 
		CASE WHEN 
				EXISTS (SELECT * FROM LO_XML_FILE WHERE NAME = @Name) 
			THEN 1
			ELSE 0
		END
END