-- =============================================
-- Author:		Brian Beery
-- Create date: 09/20/2013
-- Description:	Get a lender's data retrieval framework partners
-- =============================================
CREATE PROCEDURE [dbo].[DATA_RETRIEVAL_PARTNER_ListByBrokerId]
	@BrokerId UniqueIdentifier
AS
BEGIN

	SELECT PartnerId
	FROM DATA_RETRIEVAL_PARTNER_BROKER
	WHERE BrokerId = @BrokerId

END