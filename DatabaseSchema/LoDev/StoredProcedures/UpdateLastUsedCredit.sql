CREATE PROCEDURE [dbo].[UpdateLastUsedCredit] 
	@UserID Guid,
	@LastUsedCreditLoginNm varchar(36) = NULL,
	@LastUsedCreditProtocolID Guid = NULL,
	@LastUsedCreditAccountId varchar(36) = NULL
AS
UPDATE Broker_User
SET LastUsedCreditLoginNm = COALESCE(@LastUsedCreditLoginNm, LastUsedCreditLoginNm),
    LastUsedCreditProtocolID = COALESCE(@LastUsedCreditProtocolID, LastUsedCreditProtocolID),
    LastUsedCreditAccountId = COALESCE(@LastUsedCreditAccountId, LastUsedCreditAccountId)
        
WHERE UserID = @UserID
