CREATE PROCEDURE StartDiscTrack 
	@TrackCreatorUserId uniqueidentifier = NULL,
	@TrackCreatorLoginNm varchar(36) = NULL,
	@TrackBrokerId uniqueidentifier,
	@NotifId uniqueidentifier
AS
declare @TrackId uniqueidentifier
if( @TrackCreatorUserId is null and @TrackCreatorLoginNm is not null )
begin
	select @TrackCreatorUserId = UserId from All_User where lower( LoginNm ) = lower( @TrackCreatorLoginNm )
end
if( @TrackCreatorLoginNm is null and @TrackCreatorUserId is not null )
begin
	select @TrackCreatorLoginNm = LoginNm from All_User where UserId = @TrackCreatorUserId
end
select @TrackId = trackid 
from track 
where trackcreatoruserid=@TrackCreatorUserId and notifid = @NotifId
declare @tranPoint sysname
set @tranPoint = object_name(@@procId) + CAST(@@nestlevel as varchar(10))
BEGIN TRANSACTION
SAVE TRANSACTION @tranPoint
if( @TrackId is not null )
begin
	update track
	set trackisvalid = 1, trackinvaliddate = null, tracknotifisread = 0
	where trackid = @TrackId
	if( 0!=@@error )
	begin
		RAISERROR('error in updating track in sp StartDiscTrack', 16, 1);
		goto ErrorHandler;
	end
end
else
begin
	declare @DiscLogId uniqueidentifier;
	
	select @DiscLogId = DiscLogId
	from discussion_notification
	where notifid = @notifid;
	
	set @TrackId = newid();
	INSERT INTO TRACK 
	([TrackId], [TrackCreatedDate], [TrackCreatorUserId],   [TrackCreatorLoginNm], [TrackBrokerId], [NotifId], [DiscLogId] )
	VALUES
	(@TrackId,   getdate(), 	@TrackCreatorUserId, 	@TrackCreatorLoginNm, @TrackBrokerId,	@NotifId, @DiscLogId )
	if( 0!=@@error )
	begin
		RAISERROR('error in insert into track in sp StartDiscTrack', 16, 1);
		goto ErrorHandler;
	end
end
commit transaction
return 0;
ErrorHandler:
	rollback tran @tranPoint
	rollback transaction
	return -100;
