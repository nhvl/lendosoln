-- =============================================
-- Author:		Geoffrey Feltman
-- Create date: 6/11/14
-- Description:	Updates the error message of a pending document request.
-- =============================================
CREATE PROCEDURE [dbo].[PENDING_DOCUMENT_REQUEST_UpdateErrorMessageById]
	@Id int,
	@ErrorMessage varchar(100),
	@BrokerId uniqueidentifier
AS
BEGIN
	UPDATE PENDING_DOCUMENT_REQUEST
	SET ErrorMessage = @ErrorMessage
	WHERE Id = @Id
		AND BrokerId = @BrokerId
END
