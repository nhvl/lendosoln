CREATE PROCEDURE [dbo].[Billing_EarlyCheck]
	@StartDate datetime,
	@EndDate datetime
AS
BEGIN
-- dd 2018-11-02 - For use in automate billing executable.

SELECT br.BrokerNm As LenderName, br.customercode, lc.slid As LoanId, lc.slnm As LoanNumber, lc.abnm As BorrowerName, lb.sFnmaEarlyCheckFirstRunD As DateRun
                            FROM loan_file_cache lc WITH(NOLOCK) JOIN loan_file_b lb with(NOLOCK) on lc.slid=lb.slid
                            JOIN Broker br WITH(NOLOCK) on lc.sbrokerid=br.brokerid
                            WHERE br.CustomerCode='PML0176' AND lb.sFnmaEarlyCheckFirstRunD between @StartDate AND @EndDate
							
END