-- =============================================
-- Author:		Scott Gettinger
-- Create date: 7/1/2015
-- Description:	Fetch a broker's audit history   
-- later than a given version for valid EDocs.
-- =============================================
ALTER PROCEDURE [dbo].[EDOCS_FetchBrokerAuditHistoryByVersion]
	@BrokerId uniqueidentifier,
	@Version int,
  @Count int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT TOP(@Count)

    auditHistory.DocumentId,
		auditHistory.ModifiedByUserId,
		auditHistory.Version,
		auditHistory.ModifiedDate,
		auditHistory.Description,
		auditHistory.Details
		
	FROM edocs_document as doc 
	INNER JOIN EDOCS_AUDIT_HISTORY as auditHistory on auditHistory.DocumentId = doc.DocumentId
	WHERE doc.BrokerId = @BrokerId AND auditHistory.Version > @Version
	ORDER BY auditHistory.Version

END

GO


