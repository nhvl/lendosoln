CREATE  PROCEDURE RetrieveTaskFilterSettingsByID
      @UserID Guid,
      @TaskTab char(1)
AS
      DECLARE @Offset tinyint
      IF (@TaskTab = 'T') -- Tracked Tasks
            SET @Offset = 51
      ELSE
            IF (@TaskTab = 'L') -- Tasks in Your Loans
                  SET @Offset = 1
            ELSE
                  BEGIN
                        RAISERROR('Invalid TaskTab in RetrieveTaskFilterSettingsByID sp', 16, 1);
                        RETURN -100;
                  END
      SELECT SUBSTRING(DiscFilterSettings, @Offset, 50) as DiscFilterSettings
      FROM Broker_User
      WHERE UserID = @UserID
      IF(0!=@@error)
      BEGIN
            RAISERROR('Error in the select statement in RetrieveTaskFilterSettingsByID sp', 16, 1);
            RETURN -100;
      END
      RETURN 0;
