CREATE PROCEDURE RetrieveLpeRsExpirationByPassPermissionByUserId
	@UserId Guid
AS
BEGIN
	SELECT LpeRsExpirationByPassPermission FROM Broker_User WHERE UserId=@UserId
END
