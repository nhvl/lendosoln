-- =============================================
-- Author:		Michael Leinweaver
-- Create date: 6/21/2017
-- Description:	Gets the theme collection for a broker.
-- =============================================
ALTER PROCEDURE [dbo].[ThemeCollection_Retrieve]
	@BrokerId uniqueidentifier,
	@ThemeCollectionType int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DECLARE @ThemeCollectionId int
    SELECT @ThemeCollectionId = ThemeCollectionId
    FROM BROKER_THEME_COLLECTION
    WHERE BrokerId = @BrokerId AND ThemeCollectionType = @ThemeCollectionType
    
    IF @ThemeCollectionId IS NOT NULL
    BEGIN
        SELECT @ThemeCollectionId as "ThemeCollectionId", SelectedThemeId, Version
		FROM BROKER_THEME_COLLECTION
		WHERE BrokerId = @BrokerId AND ThemeCollectionType = @ThemeCollectionType
		
		SELECT 
			ColorThemeId, 
			Name, 
			PageHeaderBackground,
			PageHeaderText,
			PageHeaderAccent,
			SideNavigationBackground,
			SideNavigationSelection,
			SideNavigationCollapseExpandButton,
			SideNavigationText,
			ContentHeader,
			ContentIconButtonTab,
			ContentLinkSortedColumn,
			ContentTooltip,
			ContentTooltipText,
			ContentText,
			ContentRaisedButtonText,
			ContentFlatButtonText,
			ContentProgressBarPast,
			ContentProgressBarCurrent,
			PageHeaderSaveButtonText,
			PageHeaderSaveButtonBackground
		FROM BROKER_COLOR_THEME
		WHERE CollectionId = @ThemeCollectionId
    END
END
