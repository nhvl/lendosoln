ALTER PROCEDURE [dbo].[RetrieveServiceCompanyByID] 
	@ComId uniqueidentifier,
	@returnInvalid bit = 0
AS
SELECT ComId, ComNm, ComUrl, ProtocolType, IsInternalOnly, IsWarningOn, WarningMsg, WarningStartD, MclCraCode, VoxVendorId
FROM Service_Company
WHERE ComId = @ComId AND  ( IsValid = 1 OR @returnInvalid = 1)
