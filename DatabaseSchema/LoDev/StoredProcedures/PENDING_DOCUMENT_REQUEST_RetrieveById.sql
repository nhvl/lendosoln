-- =============================================
-- Author:		Geoffrey Feltman
-- Create date: 6/11/14
-- Description:	Retrieves a pending document request by id.
-- =============================================
CREATE PROCEDURE [dbo].[PENDING_DOCUMENT_REQUEST_RetrieveById]
	@Id int,
	@BrokerId uniqueidentifier
AS
BEGIN
    SELECT
        Id,
        LoanId,
        ApplicationId,
        BrokerId,
        RequestType,
        Description,
        ScheduledSendTime,
        CreatorEmployeeId,
        FileDbKey,
        EDocId,
        EDocVersion,
        DocumentLayoutXml,
        DocTypeId,
        IsESignAllowed,
        IsRequestBorrowerSignature,
        IsRequestCoborrowerSignature,
        CoborrowerTitleBorrowerId,
        ErrorMessage
    FROM PENDING_DOCUMENT_REQUEST 
    WHERE Id = @Id
		AND BrokerId = @BrokerId
END
