-- =============================================
-- Author:		Paolo Arrastia
-- Create date: 4/9/2013
-- Description:	
-- =============================================
ALTER PROCEDURE [dbo].[APPRAISAL_VENDOR_Save] 
	-- Add the parameters for the stored procedure here
	@VendorId uniqueidentifier,
	@VendorName varchar(255),
	@UsesAccountId bit,
	@ExportPath varchar(255),
	@IsUseGlobalDMSIntegration bit,
	@AppraisalNeededDateOption int,
	@HideNotes bit,
	@ExportToStagePath bit,
	@AllowOnlyOnePrimaryProduct bit,
	@SendAuthTicketWithOrders bit,
    @ShowAdditionalEmails bit,
    @EnableConnectionTest bit,
    @EnableBiDirectionalCommunication bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE APPRAISAL_VENDOR_CONFIGURATION
	SET
		VendorName = @VendorName,
		UsesAccountId = @UsesAccountId,
		ExportPath = @ExportPath,
		IsUseGlobalDMSIntegration = @IsUseGlobalDMSIntegration,
		AppraisalNeededDateOption = @AppraisalNeededDateOption,
		HideNotes = @HideNotes,
		ExportToStagePath = @ExportToStagePath,
		AllowOnlyOnePrimaryProduct = @AllowOnlyOnePrimaryProduct,
		SendAuthTicketWithOrders = @SendAuthTicketWithOrders,
		ShowAdditionalEmails = @ShowAdditionalEmails,
		EnableConnectionTest = @EnableConnectionTest,
		EnableBiDirectionalCommunication = @EnableBiDirectionalCommunication
	WHERE VendorId = @VendorId
	
	IF @@rowcount = 0
	BEGIN
		INSERT INTO APPRAISAL_VENDOR_CONFIGURATION 
		(
			VendorId,
			VendorName,
			ExportPath,
			UsesAccountId,
			IsUseGlobalDMSIntegration,
			AppraisalNeededDateOption,
			HideNotes,
			ExportToStagePath,
			AllowOnlyOnePrimaryProduct,
			SendAuthTicketWithOrders,
			ShowAdditionalEmails,
			EnableConnectionTest,
			EnableBiDirectionalCommunication
		)
		VALUES							
		(
			@VendorId,
			@VendorName,
			@ExportPath,
			@UsesAccountId,
			@IsUseGlobalDMSIntegration,
			@AppraisalNeededDateOption,
			@HideNotes,
			@ExportToStagePath,
			@AllowOnlyOnePrimaryProduct,
			@SendAuthTicketWithOrders,
			@ShowAdditionalEmails,
			@EnableConnectionTest,
			@EnableBiDirectionalCommunication
		)
	END
END


