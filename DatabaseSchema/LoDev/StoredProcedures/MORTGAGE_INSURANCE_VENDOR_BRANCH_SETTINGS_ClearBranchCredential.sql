-- =============================================
-- Author:		Isaac Ribakoff
-- Create date: 9/18/2014
-- Description:	Removes Branch Credentials For Specific MI Vendor
-- =============================================
ALTER PROCEDURE [dbo].[MORTGAGE_INSURANCE_VENDOR_BRANCH_SETTINGS_ClearBranchCredential]
	-- Add the parameters for the stored procedure here
	@BranchId uniqueidentifier,
	@VendorId uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DELETE FROM MORTGAGE_INSURANCE_VENDOR_BRANCH_SETTINGS
	WHERE BranchId = @BranchId
	AND VendorId = @VendorId
END
