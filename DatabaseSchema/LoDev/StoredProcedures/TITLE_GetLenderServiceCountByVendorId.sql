-- =============================================
-- Author:		Eric Mallare
-- Create date: 9/21/2017
-- Description:	Gets a count of how many Title lender services are using a title vendor.
-- =============================================
CREATE PROCEDURE [dbo].[TITLE_GetLenderServiceCountByVendorId]
	@VendorId int
AS
BEGIN
	SELECT
		COUNT(*) as ServiceCount
	FROM
		TITLE_FRAMEWORK_LENDER_SERVICE
	WHERE
		VendorId=@VendorId
END
