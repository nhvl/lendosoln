
CREATE PROCEDURE [dbo].[INTERNAL_USE_ONLY_ListActiveUsersByBrokerId]
@BrokerId uniqueidentifier
AS
	SELECT 
	EmployeeId
FROM 
	view_lendersoffice_employee
WHERE 
	BrokerId = @BrokerId and IsActive = '1'