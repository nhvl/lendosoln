-- =============================================
-- Author:		Paolo Arrastia
-- Create date: 4/9/2013
-- Description:	
-- =============================================
CREATE PROCEDURE dbo.APPRAISAL_VENDOR_GetConfig 
	-- Add the parameters for the stored procedure here
	@VendorId uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT *
	FROM APPRAISAL_VENDOR_CONFIGURATION
	WHERE VendorId = @VendorId
END
