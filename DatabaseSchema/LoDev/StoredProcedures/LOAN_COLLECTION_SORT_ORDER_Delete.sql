-- ============================================================
-- Author:      Alan Daughton
-- Create date: 10/19/2018
-- Description: Delete a row of the LOAN_COLLECTION_SORT_ORDER table.
-- ============================================================
CREATE PROCEDURE [dbo].[LOAN_COLLECTION_SORT_ORDER_Delete]
	@LoanId UniqueIdentifier,
	@AppId UniqueIdentifier = null,
	@ConsumerId UniqueIdentifier = null,
	@UladApplicationId UniqueIdentifier = null,
	@CollectionName VarChar(200)
AS
BEGIN
	IF @AppId IS NOT NULL
		DELETE FROM [dbo].[LOAN_COLLECTION_SORT_ORDER] WHERE LoanId = @LoanId AND AppId = @AppId AND CollectionName = @CollectionName;
	ELSE IF @ConsumerId IS NOT NULL
		DELETE FROM [dbo].[LOAN_COLLECTION_SORT_ORDER] WHERE LoanId = @LoanId AND ConsumerId = @ConsumerId AND CollectionName = @CollectionName;
	ELSE IF @UladApplicationId IS NOT NULL
		DELETE FROM [dbo].[LOAN_COLLECTION_SORT_ORDER] WHERE LoanId = @LoanId AND UladApplicationId = @UladApplicationId AND CollectionName = @CollectionName;
END