
CREATE  PROCEDURE [dbo].[ListEmployeeRelationshipsByBrokerId]
	@BrokerId uniqueidentifier
AS
	SELECT
		v.EmployeeId ,
		u.PmlExternalManagerEmployeeId ,
		u.LenderAccExecEmployeeId ,
		u.LockDeskEmployeeId ,
		u.ManagerEmployeeId ,
		u.UnderwriterEmployeeId ,
		u.ProcessorEmployeeId ,
		v.BrokerId, 
		u.BrokerProcessorEmployeeId,
		u.JuniorUnderwriterEmployeeId,
		u.JuniorProcessorEmployeeId,
		u.LoanOfficerAssistantEmployeeID,
		u.MiniCorrManagerEmployeeId, 
		u.MiniCorrProcessorEmployeeId, 
		u.MiniCorrJuniorProcessorEmployeeId, 
		u.MiniCorrLenderAccExecEmployeeId, 
		u.MiniCorrExternalPostCloserEmployeeId, 
		u.MiniCorrUnderwriterEmployeeId, 
		u.MiniCorrJuniorUnderwriterEmployeeId, 
		u.MiniCorrCreditAuditorEmployeeId, 
		u.MiniCorrLegalAuditorEmployeeId, 
		u.MiniCorrLockDeskEmployeeId, 
		u.MiniCorrPurchaserEmployeeId, 
		u.MiniCorrSecondaryEmployeeId, 
		u.CorrManagerEmployeeId, 
		u.CorrProcessorEmployeeId, 
		u.CorrJuniorProcessorEmployeeId, 
		u.CorrLenderAccExecEmployeeId, 
		u.CorrExternalPostCloserEmployeeId, 
		u.CorrUnderwriterEmployeeId, 
		u.CorrJuniorUnderwriterEmployeeId, 
		u.CorrCreditAuditorEmployeeId, 
		u.CorrLegalAuditorEmployeeId, 
		u.CorrLockDeskEmployeeId, 
		u.CorrPurchaserEmployeeId, 
		u.CorrSecondaryEmployeeId,
		a.Type as UserType
	FROM
		View_LendersOffice_Employee AS v LEFT JOIN Broker_User AS u ON u.EmployeeId = v.EmployeeId
			LEFT JOIN ALL_USER a on u.UserId = a.UserId
	WHERE
		v.BrokerId = @BrokerId

