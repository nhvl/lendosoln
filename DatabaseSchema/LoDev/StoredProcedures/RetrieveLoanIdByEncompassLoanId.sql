-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[RetrieveLoanIdByEncompassLoanId] 
	@sEncompassLoanId UniqueIdentifier,
	@sBrokerId UniqueIdentifier
AS
BEGIN
	SELECT top 1 lf.sLId FROM LOAN_FILE_F lf JOIN LOAN_FILE_CACHE lc ON lf.sLId = lc.sLId
    WHERE sEncompassLoanId = @sEncompassLoanId
      AND lc.sBrokerId = @sBrokerId and IsValid = 1 
	ORDER BY lf.sLCreatedOnLendersOfficeD desc
END
