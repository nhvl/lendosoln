CREATE  PROCEDURE ListDueDiscNotifByUserID
	@ReceiverUserID Guid
AS
declare @now smalldatetime;
set @now = GetDate();
SELECT DiscSubject, DiscDueDate, NotifAlertDate, DiscRefObjNm1, DiscRefObjNm2, ISNULL(NotifAlertDate, convert(smalldatetime, '2079-01-01')) AS SortAlertDate,
	 DiscPriority, dbo.GetReminderPriorityDescription(DiscPriority) AS PriorityDescription, DiscRefObjId, DiscStatus, DiscStatusDate
     FROM 
discussion_notification notif with(nolock) 
inner join discussion_log conv with(nolock) on notif.DiscLogid = conv.DiscLogId 
left join loan_file_cache cache with(nolock) on cache.sLId = conv.DiscRefObjId
     WHERE
	NotifIsValid = 1
	and
	DiscIsRefObjValid = 1
	and
	NotifReceiverUserId = @ReceiverUserID
	and
	notif.NotifAlertDate  <= @now and DiscStatus IN (0)
	and
	ISNULL( cache.IsTemplate , 0 ) = 0
     ORDER BY DiscRefObjNm2 ASC, NotifUpdatedDate DESC, SortAlertDate ASC
