-- =============================================
-- Author:		Matthew Pham
-- Create date: 6/12/12
-- Description:	
-- =============================================
ALTER PROCEDURE [dbo].[Drive_RetrieveSavedAuthentication] 
	-- Add the parameters for the stored procedure here
	@UserId UniqueIdentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT
		DriveUserName,
		DrivePassword,
		EncryptedDrivePassword,
		EncryptionKeyId
	FROM BROKER_USER
	WHERE UserId = @UserId
END
