ALTER PROCEDURE [dbo].[GetSecurityEventLogInfoForPMLByUserId]
	@UserId uniqueidentifier
AS
BEGIN
	SELECT UserFirstNm, UserLastNm, LoginNm
	FROM VIEW_ACTIVE_PML_USER
	where UserId= @UserId
END