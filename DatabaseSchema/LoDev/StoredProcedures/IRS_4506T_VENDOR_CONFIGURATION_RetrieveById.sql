-- =============================================
-- Author:		David Dao
-- Create date: 8/2/2013
-- Description:	
-- =============================================
ALTER PROCEDURE [dbo].[IRS_4506T_VENDOR_CONFIGURATION_RetrieveById] 
	@VendorId UniqueIdentifier
AS
BEGIN

	SELECT VendorId, VendorName, IsValid, ExportPath, IsUseAccountId, EnableConnectionTest, CommunicationModel
	FROM IRS_4506T_VENDOR_CONFIGURATION
	WHERE VendorId = @VendorId
	
END
