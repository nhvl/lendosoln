


CREATE    PROCEDURE [dbo].[CreateUsageEvent]
	@FeatureID uniqueidentifier,
	@UserID uniqueidentifier,	
	@BrokerID uniqueidentifier,
	@WhoDoneIt uniqueidentifier,
	@EventType char(1)
AS
	
	INSERT INTO Usage_Event(FeatureId, UserId, BrokerId, WhoDoneIt, EventType)
	VALUES (@FeatureID, @UserID, @BrokerID, @WhoDoneIt, @EventType)
	IF 0 != @@ERROR
	BEGIN
		RAISERROR('Error inserting into Usage_Event in [CreateUsageEvent] sp', 16, 1);
		RETURN -100
	END
	RETURN 0;