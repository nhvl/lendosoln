-- =============================================
-- Author:		Antonio Valencia
-- Create date: 12/16/11
-- Description:	Fetches Template
--				Added LastModifiedDate, AssignedUserId, OwnerUserId, CondIsHidden, CondInternalNotes - MP 1/11/12
-- =============================================
ALTER PROCEDURE [dbo].[TASK_TRIGGER_TEMPLATE_SAVE]
	-- Add the parameters for the stored procedure here
	@AutoTaskTemplateId int = null,
	@BrokerId uniqueidentifier,
	@Subject varchar(3000),
	@TriggerName varchar(200),
	@Frequency int,
	@LastModifiedDate smalldatetime,
	@AssignedUserId uniqueidentifier,
	@ToBeAssignedRoleId uniqueidentifier = null,
	@OwnerUserId uniqueidentifier,
	@ToBeOwnedByRoleId uniqueidentifier = null,
	@TaskPermissionLevelId int,
	@DueDateCalculatedFromField varchar(100),
	@DueDateCalculationOffset int,
	@IsConditionTemplate bit,
	@CondIsHidden bit,
	@CondInternalNotes varchar(1000),
	@ConditionCategoryId int = null,
	@Comments varchar(3000),
	@CondRequiredDocTypeId int = null,
	@ResolutionBlockTriggerName varchar(200) = '',
	@ResolutionDenialMessage varchar(1000) = '',
	@ResolutionDateSetterFieldId varchar(100) = '',
	@AutoResolveTriggerName varchar(200) = '',
	@AutoCloseTriggerName varchar(200) = ''
			
AS
BEGIN
	IF @AutoTaskTemplateId IS NULL 
	BEGIN
		INSERT INTO TASK_TRIGGER_TEMPLATE(
			BrokerId,
			Subject,
			TriggerName,
			Frequency,
			LastModifiedDate,
			AssignedUserId,
			ToBeAssignedRoleId,
			OwnerUserId,
			ToBeOwnedByRoleId,
			TaskPermissionLevelId,
			DueDateCalculatedFromField,
			DueDateCalculationOffset,
			IsConditionTemplate,
			CondIsHidden,
			CondInternalNotes,
			ConditionCategoryId,
			Comments, 
			CondRequiredDocTypeId,
			ResolutionBlockTriggerName,
			ResolutionDenialMessage,
			ResolutionDateSetterFieldId,
			AutoResolveTriggerName,
			AutoCloseTriggerName)
		VALUES(
			@BrokerId,
			@Subject,
			@TriggerName,
			@Frequency,
			@LastModifiedDate,
			@AssignedUserId,
			@ToBeAssignedRoleId,
			@OwnerUserId,
			@ToBeOwnedByRoleId,
			@TaskPermissionLevelId,
			@DueDateCalculatedFromField,
			@DueDateCalculationOffset,
			@IsConditionTemplate,
			@CondIsHidden,
			@CondInternalNotes,
			@ConditionCategoryId,
			@Comments,
			@CondRequiredDocTypeId,
			@ResolutionBlockTriggerName,
			@ResolutionDenialMessage,
			@ResolutionDateSetterFieldId,
			@AutoResolveTriggerName,
			@AutoCloseTriggerName)
			
	END
	ELSE 
	BEGIN
		UPDATE TASK_TRIGGER_TEMPLATE
		SET 
			Subject = @Subject,
			TriggerName = @TriggerName, 
			Frequency = @Frequency,
			LastModifiedDate = @LastModifiedDate,
			AssignedUserId = @AssignedUserId,
			ToBeAssignedRoleId = @ToBeAssignedRoleId,
			OwnerUserId = @OwnerUserId,
			ToBeOwnedByRoleId = @ToBeOwnedByRoleId,
			TaskPermissionLevelId = @TaskPermissionLevelId,
			DueDateCalculatedFromField = @DueDateCalculatedFromField,
			DueDateCalculationOffset = @DueDateCalculationOffset,
			IsConditionTemplate = @IsConditionTemplate,
			CondIsHidden = @CondIsHidden,
			CondInternalNotes = @CondInternalNotes,
			ConditionCategoryId = @ConditionCategoryId,
			Comments = @Comments,
			CondRequiredDocTypeId = @CondRequiredDocTypeId,
			ResolutionBlockTriggerName = @ResolutionBlockTriggerName,
			ResolutionDenialMessage = @ResolutionDenialMessage,
			ResolutionDateSetterFieldId = @ResolutionDateSetterFieldId,
			AutoResolveTriggerName = @AutoResolveTriggerName,
			AutoCloseTriggerName = @AutoCloseTriggerName
		WHERE
			AutoTaskTemplateId = @AutoTaskTemplateId AND
			BrokerId = @BrokerId
	END 
END
