-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE dbo.TASK_SyncLoanCachedInfoToTaskByLoanId
	@sLId UniqueIdentifier
AS
BEGIN
	UPDATE Task
	SET LoanNumCached = lc.sLNm,
	    BorrowerNmCached = lc.sPrimBorrowerFullNm
	FROM Task t JOIN LOAN_FILE_CACHE lc ON t.LoanId = lc.sLId
	WHERE lc.sLId = @sLId
	  AND (t.LoanNumCached <> lc.sLNm OR t.BorrowerNmCached <> lc.sPrimBorrowerFullNm)

END
