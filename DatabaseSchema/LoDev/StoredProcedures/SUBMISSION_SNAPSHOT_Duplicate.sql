ALTER PROCEDURE dbo.SUBMISSION_SNAPSHOT_Duplicate
	@SourceLoanId uniqueidentifier,
	@NewLoanId uniqueIdentifier
AS
BEGIN

INSERT INTO SUBMISSION_SNAPSHOT WITH(ROWLOCK) (LoanId, SnapshotDataLastModifiedD, BrokerId, SnapshotFileKey, PriceGroupId, PriceGroupFileKey, FeeServiceRevisionId, SubmissionType, SubmissionDate)
SELECT @NewLoanId, SnapshotDataLastModifiedD, BrokerId, SnapshotFileKey, PriceGroupId, PriceGroupFileKey, FeeServiceRevisionId, SubmissionType, SubmissionDate
FROM SUBMISSION_SNAPSHOT WITH(ROWLOCK, UPDLOCK)
WHERE LoanId = @SourceLoanId

END
