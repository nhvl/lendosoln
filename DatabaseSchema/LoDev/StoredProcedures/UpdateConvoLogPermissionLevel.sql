-- =============================================
-- Author: Scott Kibler
-- Create date: 5/9/2017
-- Description: 
--  Updates the Conversation log permission level information.
-- =============================================
CREATE PROCEDURE [dbo].[UpdateConvoLogPermissionLevel]
	@Id int,
	@IsActive bit,
	@Name varchar(40),
	@Description varchar(1000)
AS
BEGIN
	UPDATE CONVOLOG_PERMISSION_LEVEL
	SET
		IsActive = @IsActive,
		Name = @Name,
		Description =  @Description
	WHERE
		Id = @Id
END