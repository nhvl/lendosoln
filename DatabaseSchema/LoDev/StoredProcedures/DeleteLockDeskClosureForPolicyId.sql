-- =============================================
-- Author:		paoloa
-- Create date: 4/18/2013
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[DeleteLockDeskClosureForPolicyId]
	-- Add the parameters for the stored procedure here
	@PolicyId uniqueidentifier,
	@BrokerId uniqueidentifier,
	@ClosureDate smalldatetime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM LENDER_LOCK_POLICY_HOLIDAY
	WHERE LockPolicyId = @PolicyId AND BrokerId = @BrokerId and ClosureDate = @ClosureDate
END
