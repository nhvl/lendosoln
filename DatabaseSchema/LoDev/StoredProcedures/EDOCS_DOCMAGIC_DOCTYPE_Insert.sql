-- =============================================
-- Author:		Thien Nguyen
-- Create date: 
-- Description:	
-- =============================================
-- =============================================
-- Author:		Thien Nguyen
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[EDOCS_DOCMAGIC_DOCTYPE_Insert] 
    @DocumentClass varchar(200),
    @BarcodeClassification varchar(100)
AS
BEGIN

	INSERT INTO EDOCS_DOCMAGIC_DOCTYPE (DocumentClass,  BarcodeClassification)
					VALUES(@DocumentClass, @BarcodeClassification)
                                     
	SELECT CAST(SCOPE_IDENTITY() AS int)
END
