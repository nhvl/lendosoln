-- =============================================
-- Author:		Thien Nguyen
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[EDOCS_IDS_DOCTYPE_Delete] 
    @Id int
AS
BEGIN

	DELETE FROM EDOCS_IDS_DOCTYPE
		  WHERE Id = @Id
END
