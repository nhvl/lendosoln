-- =============================================
-- Author:		David Dao
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE dbo.BROKER_GLOBAL_IP_WHITELIST_ListByBrokerId 
    @BrokerId UniqueIdentifier
AS
BEGIN

		SELECT i.Id, i.IPAddress, i.Description, i.LastModifiedDate, a.LoginNm
		FROM BROKER_GLOBAL_IP_WHITELIST i JOIN ALL_USER a ON i.LastModifiedUserId=a.UserId
		WHERE i.BrokerId = @BrokerId
		ORDER BY i.IPAddress

END
