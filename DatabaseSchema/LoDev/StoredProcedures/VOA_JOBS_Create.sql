-- =============================================
-- Author:		Eric Mallare
-- Create date: 10/16/2018
-- Description:	Creates a VOA job entry.
-- =============================================
CREATE PROCEDURE [dbo].[VOA_JOBS_Create]
	@JobId int,
	@JobRequestDataFileDbGuidId uniqueidentifier = NULL,
	@RetryIntervalInSeconds int
AS
BEGIN
	INSERT INTO VOA_JOBS(
		JobId,
		JobRequestDataFileDbGuidId,
		RetryIntervalInSeconds
	)
	VALUES(
		@JobId,
		@JobRequestDataFileDbGuidId,
		@RetryIntervalInSeconds
	)
END