-- =============================================
-- Author:		Matthew Pham
-- Create date: 9/2/11
-- Description:	Update a trade for MBS trades
-- =============================================
CREATE PROCEDURE [dbo].[MBS_UpdateTrade] 
	-- Add the parameters for the stored procedure here
	@TradeId uniqueidentifier,
	@BrokerId uniqueidentifier,
	@TradeNum int,
	@DealerInvestor varchar(100),
	@AssignedTo varchar(100),
	@TypeA tinyint,
	@TypeB tinyint,
	@Month tinyint,
	@DescriptionId uniqueidentifier,
	@Coupon decimal(5,3),
	@TradeDate smalldatetime = null,
	@NotificationDate smalldatetime = null,
	@SettlementDate smalldatetime = null,
	@Price decimal(12, 6),
	@IsAllocatedLoansCalculated bit,
	@AllocatedLoans money,
	@PoolId bigint
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DECLARE @OldPoolId bigint 
	
	-- Get the old pool id in case its changing - if so we may need to update the pool table. 
	
	SELECT @OldPoolId = PoolId FROM MBS_TRADE WHERE TradeId = @TradeId 
	
    -- Insert statements for procedure here
	UPDATE MBS_TRADE
	SET
		TradeNum = @TradeNum,
		DealerInvestor = @DealerInvestor,
		AssignedTo = @AssignedTo,
		TypeA = @TypeA,
		TypeB = @TypeB,
		Month = @Month,
		DescriptionId = @DescriptionId,
		Coupon = @Coupon,
		TradeDate = @TradeDate,
		NotificationDate = @NotificationDate,
		SettlementDate = @SettlementDate,
		Price = @Price,
		IsAllocatedLoansCalculated = @IsAllocatedLoansCalculated,
		AllocatedLoans = @AllocatedLoans,
		PoolId = @PoolId
	WHERE TradeId = @TradeId
	

	
	IF (@OldPoolId <> @PoolId OR (@OldPoolId is NULL and @PoolId is not null ) OR (@PoolId is NULL and @OldPoolId is not null)) BEGIN
		EXEC [MORTGAGE_POOL_Update_PoolTradeNumbers] @OldPoolId  
		EXEC [MORTGAGE_POOL_Update_PoolTradeNumbers] @PoolId  
	END
END
