

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UpdateRSWarningMsgById]
	-- Add the parameters for the stored procedure here
	@Broker varchar(50), 
	@NormalUserExpiredMsg varchar(500),
	@LockDeskExpiredMsg varchar(500),
	@NormalUserCutOffMsg varchar(500),
	@LockDeskCutOffMsg	varchar(500),
	@OutsideNormalHrMsg varchar(500),
	@OutsideClosureDayHrMsg varchar(500),
	@OutsideNormalHrAndPassInvestorCutOffMsg varchar(500)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	update RS_Expiration_Custom_Warning_Msgs
	set NormalUserExpiredCustomMsg = @NormalUserExpiredMsg,
		LockDeskExpiredCustomMsg = @LockDeskExpiredMsg,
		NormalUserCutOffCustomMsg = @NormalUserCutOffMsg,
		LockDeskCutOffCustomMsg = @LockDeskCutOffMsg,
		OutsideNormalHrCustomMsg = @OutsideNormalHrMsg,
		OutsideClosureDayHrCustomMsg = @OutsideClosureDayHrMsg,
		OutsideNormalHrAndPassInvestorCutOffCustomMsg = @OutsideNormalHrAndPassInvestorCutOffMsg
		where brokerId = @Broker
	
END


