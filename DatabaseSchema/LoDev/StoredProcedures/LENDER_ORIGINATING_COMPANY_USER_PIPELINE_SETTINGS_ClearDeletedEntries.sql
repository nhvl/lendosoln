-- =============================================
-- Author:		Michael Leinweaver
-- Create date: 5/23/2018
-- Description:	Clears deleted pipeline settings.
-- =============================================
ALTER PROCEDURE [dbo].[LENDER_ORIGINATING_COMPANY_USER_PIPELINE_SETTINGS_ClearDeletedEntries]
	@BrokerId uniqueidentifier,
	@DeletedPipelineSettingIdXml xml
AS
BEGIN
	DELETE FROM LENDER_ORIGINATING_COMPANY_USER_PIPELINE_SETTINGS
	WHERE BrokerId = @BrokerId AND SettingId IN (
		SELECT T.item.value('.', 'uniqueidentifier')
		FROM @DeletedPipelineSettingIdXml.nodes('root/id') as T(Item)
	)
END