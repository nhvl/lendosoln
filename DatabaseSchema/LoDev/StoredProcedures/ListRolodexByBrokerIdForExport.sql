ALTER PROCEDURE [dbo].[ListRolodexByBrokerIdForExport]
	@BrokerID uniqueidentifier , @NameFilter varchar(64) = NULL , @TypeFilter int = NULL,
	@IsApprovedFilter bit = null
AS
	SELECT 
		 AgentType as [Type],
		 AgentTypeOtherDescription as 'Type Other Description',
		 AgentNm as Name,
		 AgentTitle as Title,
		 AgentPhone as Phone,
		 AgentAltPhone as 'Agent Cell Phone',
		 AgentFax as 'Agent Fax',
		 AgentComNm as 'Company Name',
		 AgentEmail as 'Email',
		 AgentAddr as [Address],
		 AgentCity as City,
		 AgentState as [State],
		 AgentZip as Zip,
		 AgentDepartmentName as 'Department Name',
		 AgentPager as Pager,
		 AgentLicenseNumber as 'Agent License #',
		 CompanyLicenseNumber as 'Company License #',
		 PhoneOfCompany as 'Company Phone',  
		 FaxOfCompany as 'Company Fax',
		 AgentWebsiteUrl as 'Company Website',
		 AgentN as 'Notes',
		 AgentCounty as County,
		 SystemId as 'System ID',
		 CompanyId as 'Company ID'

	FROM
		Agent
	WHERE
		BrokerID = @BrokerID
		AND
		(
			AgentNm LIKE COALESCE('%' +  @NameFilter + '%' ,  AgentNm )
			OR
			AgentComNm LIKE COALESCE( '%' +  @NameFilter + '%' ,  AgentComNm )
			
		)
		AND
		AgentType = COALESCE( @TypeFilter , AgentType )
		AND
		AgentIsApproved = COALESCE(@IsApprovedFilter, AgentIsApproved)
	ORDER BY
		AgentType, AgentNm
	if( 0!=@@error)
	begin
		RAISERROR('Error in the select statement in ListRolodexByBrokerID sp', 16, 1);
		return -100;
	end
	
	return 0;




