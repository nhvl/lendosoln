
CREATE  PROCEDURE [dbo].[Pml_FindPriceMyLoanLoanFile] 
	@sLNm varchar(36),
	@aBLastNm varchar(36),
	@BrokerID Guid,
	@BranchID Guid,
	@EmployeeID Guid,
	@AccessLevel int
AS
	-- dd 6/23/2005 - Pipeline in PML will no longer support branch & coporate access level.
	SELECT lc.sLId
	FROM Loan_File_Cache lc with(nolock)
             WHERE lc.sLNm = @sLNm 
                   AND lc.IsValid = 1 AND lc.sPmlSubmitStatusT = 0 AND lc.aBLastNm = @aBLastNm AND @sLNm <> '' AND @aBLastNm <> ''
                   AND lc.sBrokerID = @BrokerID
	      AND (lc.sEmployeeLoanRepId = @EmployeeID OR lc.sEmployeeLenderAccExecId = @EmployeeID OR sEmployeeManagerId = @EmployeeID)
	     AND IsTemplate = 0

