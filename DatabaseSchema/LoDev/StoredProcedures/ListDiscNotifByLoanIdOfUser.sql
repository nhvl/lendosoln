
CREATE PROCEDURE [dbo].[ListDiscNotifByLoanIdOfUser]
	@UserID uniqueidentifier,
	@LoanId uniqueidentifier
AS
	SELECT
		NotifId,
		NotifIsRead,
		DiscCreatorLoginNm,
		DiscStatus,
		DiscStatusDate,
		DiscPriority,
		DiscSubject,
		DiscDueDate,
		NotifAlertDate,
		DiscRefObjNm1,
		DiscRefObjNm2,
		ISNULL(NotifAlertDate, convert(smalldatetime, '2079-01-01')) AS SortAlertDate,
		DiscPriority,
		dbo.GetReminderPriorityDescription(DiscPriority) AS PriorityDescription,
		DiscRefObjId,
		DiscCreatedDate
			 FROM discussion_notification notif WITH (NOLOCK)
				inner join discussion_log conv WITH (NOLOCK)
					on notif.DiscLogid = conv.DiscLogId
			 WHERE notif.NotifIsValid = 1
					and conv.DiscIsRefObjValid = 1
					and notif.NotifReceiverUserId = @UserID
					and conv.DiscRefObjId = @LoanId
					and conv.IsValid = 1
