-- =============================================
-- Author:		Paolo Arrastia
-- Create date: 24 Jul 2012
-- Description:	
-- =============================================
ALTER PROCEDURE [dbo].[RateMonitorLoad]
	-- Add the parameters for the stored procedure here
	@sLId UniqueIdentifier, 
	@bpdesc varchar(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    SELECT Id, Rate, Point, EmailAddresses, CreatedByUserId, CreatedByUserType
		FROM RATE_MONITOR
		WHERE (sLId = @sLId AND BPDesc = @bpdesc AND IsEnabled = 1);
END
