-- =============================================
-- Author:		Paolo Arrastia
-- Create date: 4/11/2013
-- Description: 
-- =============================================
ALTER PROCEDURE [dbo].[APPRAISAL_VENDOR_SaveLogin]
	-- Add the parameters for the stored procedure here
	@EmployeeId uniqueidentifier,
	@VendorId uniqueidentifier,
	@AccountId varchar(50),
	@Username varchar(50),
	@EncryptedPassword varbinary(max) = null,
	@EncryptionKeyId uniqueidentifier = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    UPDATE APPRAISAL_VENDOR_EMPLOYEE_LOGIN
    SET
		AccountId = @AccountId,
		Username = @Username,
		EncryptedPassword = @EncryptedPassword,
		EncryptionKeyId = @EncryptionKeyId
	WHERE
		EmployeeId = @EmployeeId AND VendorId = @VendorId
		
	IF @@rowcount = 0
	BEGIN
		INSERT INTO APPRAISAL_VENDOR_EMPLOYEE_LOGIN
		(EmployeeId, VendorId, AccountId, Username, EncryptedPassword, EncryptionKeyId)
		VALUES
		(@EmployeeId, @VendorId, @AccountId, @Username, @EncryptedPassword, @EncryptionKeyId)
	END
	
END
