-- =============================================
-- Author:		Antonio Valencia
-- Create date: 10/29/2013
-- Description:	Delete CP share document request
-- =============================================


CREATE PROCEDURE [dbo].CP_ShareDocument_Delete 
	-- Add the parameters for the stored procedure here
	@BrokerId uniqueidentifier,
	@Id bigint
AS
BEGIN
	DELETE FROM CP_SHARE_DOCUMENT
	WHERE BRokerID = @Brokerid and Id = @Id
END
