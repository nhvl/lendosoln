-- =============================================
-- Author:		Antonio Valencia
-- Create date: 1/2/2013 11:46 AM
-- Description:	Adds Investor Rolodex Entries
-- =============================================
ALTER PROCEDURE [dbo].[INVESTOR_ROLODEX_ADD]
	@BrokerId uniqueidentifier,
	@SellerId varchar(25),
	@InvestorName varchar(100),
	@MERSOrganizationId varchar(7),
	@Status int,
	@CompanyName varchar(400),
	@MainAttention varchar(400),
	@MainAddress varchar(60),
	@MainState varchar(2),
	@MainCity varchar(36),
	@MainZip varchar(10),
	@MainPhone varchar(21),
	@MainFax varchar(21),
	@MainContactName varchar(150),
	@MainEmail varchar(100),
	@NoteShipToAttention varchar(400),
	@NoteShipToAddress varchar(60),
	@NoteShipToState varchar(2),
	@NoteShipToCity varchar(36),
	@NoteShipToZip varchar(10),
	@NoteShipToPhone varchar(21),
	@NoteShipToFax varchar(21),
	@NoteShipToContactName varchar(150),
	@NoteShipToEmail varchar(100),
	@NoteShipToUseMainAdddress bit,
	@PaymentToAttention varchar(400),
	@PaymentToAddress varchar(60),
	@PaymentToState varchar(2),
	@PaymentToCity varchar(36),
	@PaymentToZip varchar(10),
	@PaymentToPhone varchar(21),
	@PaymentToFax varchar(21),
	@PaymentToContactName varchar(150),
	@PaymentToEmail varchar(100),
	@PaymentToUseMainAdddress bit,
	@LossPayeeAttention varchar(400),
	@LossPayeeAddress varchar(60),
	@LossPayeeState varchar(2), 
	@LossPayeeCity varchar(36),
	@LossPayeeZip varchar(10),
	@LossPayeePhone varchar(21),
	@LossPayeeFax varchar(21),
	@LossPayeeContactName varchar(150),
	@LossPayeeEmail varchar(100),
	@LossPayeeUseMainAdddress bit,
	@HmdaPurchaser2004T int, 
	@InvestorRolodexType int,
	@LpeInvestorId int,
	@HmdaPurchaser2015T int
AS
BEGIN
	INSERT INTO INVESTOR_ROLODEX(BrokerId, SellerId, InvestorName, MERSOrganizationId, [Status], CompanyName, MainAttention, MainAddress, MainState, MainCity, MainZip, MainPhone, MainFax, MainContactName, MainEmail, NoteShipToAttention, NoteShipToAddress, NoteShipToState, NoteShipToCity, NoteShipToZip, NoteShipToPhone, NoteShipToFax, NoteShipToContactName, NoteShipToEmail, NoteShipToUseMainAdddress, PaymentToAttention, PaymentToAddress, PaymentToState, PaymentToCity, PaymentToZip, PaymentToPhone, PaymentToFax, PaymentToContactName, PaymentToEmail, PaymentToUseMainAdddress, LossPayeeAttention, LossPayeeAddress, LossPayeeState, LossPayeeCity, LossPayeeZip, LossPayeePhone, LossPayeeFax, LossPayeeContactName, LossPayeeEmail, LossPayeeUseMainAdddress, HmdaPurchaser2004T, InvestorRolodexType, LpeInvestorId, HmdaPurchaser2015T)
	VALUES(@BrokerId, @SellerId, @InvestorName, @MERSOrganizationId, @Status, @CompanyName, @MainAttention, @MainAddress, @MainState, @MainCity, @MainZip, @MainPhone, @MainFax, @MainContactName, @MainEmail, @NoteShipToAttention, @NoteShipToAddress, @NoteShipToState, @NoteShipToCity, @NoteShipToZip, @NoteShipToPhone, @NoteShipToFax, @NoteShipToContactName, @NoteShipToEmail, @NoteShipToUseMainAdddress, @PaymentToAttention, @PaymentToAddress, @PaymentToState, @PaymentToCity, @PaymentToZip, @PaymentToPhone, @PaymentToFax, @PaymentToContactName, @PaymentToEmail, @PaymentToUseMainAdddress, @LossPayeeAttention, @LossPayeeAddress, @LossPayeeState, @LossPayeeCity, @LossPayeeZip, @LossPayeePhone, @LossPayeeFax, @LossPayeeContactName, @LossPayeeEmail, @LossPayeeUseMainAdddress, @HmdaPurchaser2004T, @InvestorRolodexType, @LpeInvestorId, @HmdaPurchaser2015T)
	SELECT CAST( SCOPE_IDENTITY() as int)
END
