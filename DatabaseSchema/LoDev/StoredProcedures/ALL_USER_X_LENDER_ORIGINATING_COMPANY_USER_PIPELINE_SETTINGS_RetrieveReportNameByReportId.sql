-- =============================================
-- Author:		Michael Leinweaver
-- Create date: 6/5/2018
-- Description:	Retrieves pipeline settings for a TPO user.
-- =============================================
ALTER PROCEDURE [dbo].[ALL_USER_X_LENDER_ORIGINATING_COMPANY_USER_PIPELINE_SETTINGS_RetrieveReportNameByReportId]
	@UserId uniqueidentifier,
	@BrokerId uniqueidentifier,
	@ReportId uniqueidentifier,
	@PortalMode tinyint
AS
BEGIN
	SELECT 
		settings.PortalDisplayName,
		settings.OverridePipelineReportName,
		report.QueryName
	FROM 
		ALL_USER_X_LENDER_ORIGINATING_COMPANY_USER_PIPELINE_SETTINGS junction
		JOIN LENDER_ORIGINATING_COMPANY_USER_PIPELINE_SETTINGS settings ON junction.SettingId = settings.SettingId
		JOIN REPORT_QUERY report ON settings.QueryId = report.QueryId
	WHERE 
		junction.UserId = @UserId AND 
		junction.BrokerId = @BrokerId AND
		junction.PortalMode = @PortalMode AND
		settings.QueryId = @ReportId
END
