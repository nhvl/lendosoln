CREATE PROCEDURE EmployeeAddRole 
	@RoleID Guid,
             @EmployeeID Guid,
	@BrokerID Guid
AS
INSERT INTO Role_Assignment(EmployeeID, RoleID) 
SELECT @EmployeeID, @RoleID
FROM Employee e, Branch b
WHERE e.EmployeeID = @EmployeeID AND e.branchID = b.branchID AND b.BrokerID = @BrokerID
if( 0!=@@error)
begin
	RAISERROR('Error in inserting into Role_Assignment table in EmployeeAddRole sp', 16, 1);
	return -100;
end
return 0;
