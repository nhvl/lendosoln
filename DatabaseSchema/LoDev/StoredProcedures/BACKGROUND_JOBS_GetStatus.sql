CREATE PROCEDURE [dbo].[BACKGROUND_JOBS_GetStatus]
AS
BEGIN
	SELECT 
		   ProcessingStatus, JobType, Count(*) as NumJobs, min(DateCreated) as OldestJobDateCreated
	FROM 
		   BACKGROUND_JOBS b
	GROUP BY
		   ProcessingStatus, JobType
END