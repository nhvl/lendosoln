-- =============================================
-- Author:		Michael Leinweaver
-- Create date: 4/15/2017
-- Description:	Created for the migration in case 430908.
-- =============================================
ALTER PROCEDURE [dbo].[AddSystemIdsToRolodexEntriesForMigrator]
	@BrokerId uniqueidentifier,
	@RolodexContactId uniqueidentifier,
	@SystemId int
AS
BEGIN
	if exists (select agentid from agent where brokerid = @brokerid and agentid <> @rolodexcontactid and systemid = @systemid)
	begin
		raiserror('cannot add duplicate system id to agent table', 16, 1);
	end
	else 
	begin 
		update top (1) agent
		set systemid = @systemid
		where agentid = @rolodexcontactid and brokerid = @brokerid
	end
END
