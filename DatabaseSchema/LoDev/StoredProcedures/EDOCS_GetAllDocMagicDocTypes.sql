

-- =============================================
-- Author:		Antonio Valencia
-- Create date: August 22, 2011
-- Description:	Retrieves list of unused docmagic doctypes
-- =============================================
CREATE PROCEDURE [dbo].[EDOCS_GetAllDocMagicDocTypes] 
AS
BEGIN
	SELECT id, DocumentClass FROM EDOCS_DOCMAGIC_DOCTYPE 

END


