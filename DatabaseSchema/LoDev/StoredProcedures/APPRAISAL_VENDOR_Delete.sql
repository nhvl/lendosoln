-- =============================================
-- Author:		Paolo Arrastia
-- Create date: 4/10/2013
-- Description:	
-- =============================================
ALTER PROCEDURE [dbo].[APPRAISAL_VENDOR_Delete]
	-- Add the parameters for the stored procedure here
	@VendorId uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON; 
    
    DELETE FROM APPRAISAL_VENDOR_CONFIGURATION
	WHERE VendorId = @VendorId
END
