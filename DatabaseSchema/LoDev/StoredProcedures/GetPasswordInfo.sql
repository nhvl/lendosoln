
CREATE   PROCEDURE [dbo].[GetPasswordInfo]
	@LoginNm varchar(36),
	@BrokerPmlSiteId uniqueidentifier = '00000000-0000-0000-0000-000000000000'
AS
	SELECT PasswordHash, PasswordSalt, IsUpdatedPassword
	FROM ALL_USER
	WHERE LoginNm = @LoginNm AND BrokerPmlSiteId = @BrokerPmlSiteId

