-- =============================================
-- Author:		Paolo Arrastia
-- Create date: 6 Aug 2012
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[RateMonitorDelete] 
	-- Add the parameters for the stored procedure here
	@monitorid bigint
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM RATE_MONITOR
	WHERE Id = @monitorid;
END
