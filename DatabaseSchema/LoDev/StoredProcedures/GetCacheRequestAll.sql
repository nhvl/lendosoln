ALTER PROCEDURE [dbo].[GetCacheRequestAll]
	
AS
	SELECT TOP(1000)
		u.RequestId, 
		u.sLId, 
		u.UserId, 
		u.UserLoginNm, 
		u.PageName, 
		u.AffectedCachedFields, 
		u.ErrorInfo, c.sLNm, 
		u.FirstOccurD, 
		u.LastRetryD, 
		u.WhoRetriedLastLoginNm,
		c.sBrokerId as BrokerId,
		u.NumRetries,
		(SELECT CustomerCode FROM BROKER as b WHERE b.brokerid = c.sbrokerid) as CustomerCode
	FROM LOAN_CACHE_UPDATE_REQUEST u INNER JOIN LOAN_FILE_CACHE c
	
	ON u.sLId = c.sLId
	ORDER BY u.FirstOccurD desc
	if( 0 != @@error )
	begin
		RAISERROR('Error in selecting from LOAN_CACHE_UPDATE_REQUEST table in GetCacheRequest sp', 16, 1);
		return -100;
	end
	return 0;

