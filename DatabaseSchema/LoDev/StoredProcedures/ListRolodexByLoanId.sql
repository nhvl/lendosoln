-- Author:		Antonio Valencia
-- Create date: 2/3/11
-- Description:	Retrieves Assigned Users from Loan
--
-- 2/25/13: Added support for Shipper, Funder, Post-Closer,
-- Insuring, Collateral Agent, and Doc Drawer. GF
--
-- =============================================
ALTER PROCEDURE [dbo].[ListRolodexByLoanId] 
	-- Add the parameters for the stored procedure here
	@BrokerId  uniqueidentifier,
	@sLId uniqueidentifier
AS
BEGIN
		
	SELECT 
		e.EmployeeId AS AgentId,
		IDs.AgentType as AgentTypeDesc,
		0 AS AgentType,
		'' AS AgentTitle,
		e.UserFirstNm + ' ' + e.UserLastNm AS AgentNm,
		e.Phone AS AgentPhone,
		e.CellPhone AS AgentAltPhone,
		e.IsCellphoneForMultiFactorOnly AS IsAgentAltPhoneForMultiFactorOnly,
		e.Fax AS AgentFax,
		e.Pager AS AgentPager,
		CASE x.Type WHEN 'B' THEN b.BrokerNm WHEN 'P' THEN pmlBroker.Name END AS AgentComNm,
		'' AS AgentDepartmentName,
		e.Email AS AgentEmail,
		e.Addr AS AgentAddr,
		e.City AS AgentCity,
		e.State AS AgentState,
		e.Zip AS AgentZip,
		e.LicenseXmlContent as LicenseXmlContent,
		e.NmlsIdentifier as LosIdentifier,
		b.BrokerAddr AS AddrOfCompany,
		b.BrokerCity AS CityOfCompany,		
		b.BrokerState AS StateOfCompany,
		b.BrokerZip AS ZipOfCompany,
		b.NmlsIdentifier AS BrokerLosIdentifier,
		b.LicenseXmlContent as BrokerLicenseXml,
		h.BranchNm AS NameOfBranch,
		h.BranchAddr AS AddrOfBranch,
		h.BranchCity AS CityOfBranch,		
		h.BranchState AS StateOfBranch,
		h.BranchZip AS ZipOfBranch,
		e.EmployeeLicenseNumber AS AgentLicenseNumber,
		e.CommissionPointOfLoanAmount as CommissionPointOfLoanAmount,
		e.CommissionPointOfGrossProfit as CommissionPointOfGrossProfit,
		e.CommissionMinBase as CommissionMinBase,
		'' as CompanyLicenseNumber,
		b.BrokerPhone as PhoneOfCompany,
		b.BrokerFax AS FaxOfCompany,
		h.BranchPhone as PhoneOfBranch,
		h.BranchFax AS FaxOfBranch,
		h.UseBranchInfoForLoans,
		h.NmlsIdentifier AS BranchLosIdentifier,
		h.LicenseXmlContent as BranchLicenseXml,
		0 AS IsNotifyWhenLoanStatusChange,
		CASE x.Type WHEN 'B' THEN '' WHEN 'P' THEN 'PML' END AS UserType,
		h.DisplayNm AS CompanyDisplayName
	FROM
		(

		select EmployeeId, AgentType from	(
	SELECT EmployeeId = 
		CASE X.Which
		WHEN '1' THEN sEmployeeBrokerProcessorId
		WHEN '2' THEN sEmployeeCallCenterAgentId
		WHEN '3' THEN sEmployeeCloserId
		WHEN '4' THEN sEmployeeLenderAccExecId
		WHEN '5' THEN sEmployeeLoanOpenerId
		WHEN '6' THEN sEmployeeLoanRepId
		WHEN '7' THEN sEmployeeLockDeskId
		WHEN '8' THEN sEmployeeManagerId
		WHEN '9' THEN sEmployeeProcessorId
		WHEN '10' THEN sEmployeeUnderwriterId
		WHEN '11' THEN sEmployeeShipperId
		WHEN '12' THEN sEmployeeFunderId
		WHEN '13' THEN sEmployeePostCloserId
		WHEN '14' THEN sEmployeeInsuringId
		WHEN '15' THEN sEmployeeCollateralAgentId
		WHEN '16' THEN sEmployeeDocDrawerId
		WHEN '17' THEN sEmployeeCreditAuditorId
		WHEN '18' THEN sEmployeeDisclosureDeskId
		WHEN '19' THEN sEmployeeJuniorProcessorId
		WHEN '20' THEN sEmployeeJuniorUnderwriterId
		WHEN '21' THEN sEmployeeLegalAuditorId
		WHEN '22' THEN sEmployeeLoanOfficerAssistantId
		WHEN '23' THEN sEmployeePurchaserId
		WHEN '24' THEN sEmployeeQCComplianceId
		WHEN '25' THEN sEmployeeSecondaryId
		WHEN '26' THEN sEmployeeServicingId
		WHEN '27' THEN sEmployeeExternalSecondaryId
		WHEN '28' THEN sEmployeeExternalPostCloserId
		END,
			 AgentType = 
		CASE X.Which
			WHEN '1' THEN 'Processor (External)'
			WHEN '2' THEN 'Call Center Agent'
			WHEN '3' THEN 'Closer'
			WHEN '4' THEN 'Lender Account Exec'
			WHEN '5' THEN 'Loan Opener'
			WHEN '6' THEN 'Loan Officer'
			WHEN '7' THEN 'Lock Desk'
			WHEN '8' THEN 'Manager'
			WHEN '9' THEN 'Processor'
			WHEN '10' THEN 'Underwriter'
			WHEN '11' THEN 'Shipper'
			WHEN '12' THEN 'Funder'
			WHEN '13' THEN 'Post-Closer'
			WHEN '14' THEN 'Insuring'
			WHEN '15' THEN 'Collateral Agent'
			WHEN '16' THEN 'Doc Drawer'
			WHEN '17' THEN 'Credit Auditor'
			WHEN '18' THEN 'Disclosure Desk'
			WHEN '19' THEN 'Junior Processor'
			WHEN '20' THEN 'Junior Underwriter'
			WHEN '21' THEN 'Legal Auditor'
			WHEN '22' THEN 'Loan Officer Assistant'
			WHEN '23' THEN 'Purchaser'
			WHEN '24' THEN 'QC Compliance'
			WHEN '25' THEN 'Secondary'
			WHEN '26' THEN 'Servicing'
			WHEN '27' THEN 'Secondary (External)'
			WHEN '28' THEN 'Post-Closer (External)'
		END
	FROM
		LOAN_FILE_CACHE JOIN LOAN_FILE_CACHE_3 ON LOAN_FILE_CACHE.sLId = LOAN_FILE_CACHE_3.sLId
			JOIN LOAN_FILE_CACHE_2 ON LOAN_FILE_CACHE.sLId = LOAN_FILE_CACHE_2.sLId
		CROSS JOIN 
		(
			SELECT '1' UNION ALL
			SELECT '2' UNION ALL
			SELECT '3' UNION ALL
			SELECT '4' UNION ALL
			SELECT '5' UNION ALL
			SELECT '6' UNION ALL
			SELECT '7' UNION ALL
			SELECT '8' UNION ALL
			SELECT '9' UNION ALL
			SELECT '10' UNION ALL
			SELECT '11' UNION ALL
			SELECT '12' UNION ALL
			SELECT '13' UNION ALL
			SELECT '14' UNION ALL
			SELECT '15' UNION ALL
			SELECT '16' UNION ALL
			SELECT '17' UNION ALL
			SELECT '18' UNION ALL
			SELECT '19' UNION ALL
			SELECT '20' UNION ALL
			SELECT '21' UNION ALL
			SELECT '22' UNION ALL
			SELECT '23' UNION ALL
			SELECT '24' UNION ALL
			SELECT '25' UNION ALL
			SELECT '26' UNION ALL
			SELECT '27' UNION ALL
			SELECT '28'
		) as  X (Which)
	
	WHERE LOAN_FILE_CACHE.sLId = @sLId ) as P
 
WHERE EmployeeId <> '00000000-0000-0000-0000-000000000000'  and employeeid is not null

) as IDs 
	 LEFT JOIN Employee e ON e.EmployeeId = IDs.EmployeeId
	 LEFT JOIN Branch AS h ON h.branchid = e.BranchId 
	 LEFT JOIN Broker AS b   ON b.brokerid = h.brokerid
	 LEFT JOIN Broker_User bu ON e.EmployeeUserId = bu.UserId
	 LEFT JOIN Pml_Broker pmlBroker  ON bu.PmlBrokerId = pmlBroker.PmlBrokerId
	 LEFT JOIN All_User AS x  ON x.UserId = e.EmployeeUserId	

END

