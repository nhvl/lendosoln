-- =============================================
-- Author:		Eric Mallare
-- Create date: 7/16/2016
-- Description:	Lists all loans that have the given sPmlBrokerId.
-- =============================================
ALTER PROCEDURE [dbo].[ListLoansByPmlBrokerId]
	@sPmlBrokerId uniqueidentifier,
	@BrokerId uniqueidentifier
AS
BEGIN
	SELECT 
		lfc.sLId 
	FROM 
		Loan_file_cache lfc
	WHERE 
		lfc.IsValid='1' AND
		lfc.sBrokerId=@BrokerId AND
		lfc.sPmlBrokerId=@sPmlBrokerId 
END
