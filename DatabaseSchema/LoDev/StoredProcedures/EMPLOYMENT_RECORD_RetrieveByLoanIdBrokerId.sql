-- ============================================================
-- Author:      Generated by application LqbEntitiesToSqlTables
-- Create date: 3/15/2018
-- Description: Retrieve a row from the EMPLOYMENT_RECORD table associated with a given loan and broker.
-- ============================================================
ALTER PROCEDURE [dbo].[EMPLOYMENT_RECORD_RetrieveByLoanIdBrokerId]
	@LoanId UniqueIdentifier,
	@BrokerId UniqueIdentifier
AS
BEGIN
	SELECT
		EmploymentRecordId,
		EmploymentRecordVoeData,
		VerifSignatureImgId,
		VerifSigningEmployeeId,
		Attention,
		EmployerZip,
		IsCurrent,
		IsSeeAttachment,
		JobTitle,
		PrepDate,
		VerifExpiresDate,
		VerifRecvDate,
		VerifReorderedDate,
		VerifSentDate,
		EmployerAddress,
		EmployerCity,
		EmployerFax,
		EmployerName,
		NULL AS 'EmployerPhone', -- 475067 TODO: Remove this line
		EmployerPhoneData,
		EmployerState,
		EmploymentEndDate,
		EmploymentStartDate,
		PrimaryEmploymentStartDate,
		EmploymentStatusType,
		IsSelfEmployed,
		NULL AS 'MonthlyIncome',  -- 475067 TODO: Remove this line
		ProfessionStartDate,
		SelfOwnershipShareTData,
		IsSpecialBorrowerEmployerRelationshipData,
		ConsumerId,
		LegacyAppId,
		IsPrimary,
		IncomeRecords
	FROM [dbo].[EMPLOYMENT_RECORD]
	WHERE LoanId = @LoanId
		AND BrokerId = @BrokerId
END