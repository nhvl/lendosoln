CREATE PROCEDURE UpdateSelectedPipelineCustomReportId
	@UserId UniqueIdentifier , @ReportId UniqueIdentifier
AS
	UPDATE Broker_User
	SET
		SelectedPipelineCustomReportId = @ReportId
	WHERE
		UserId = @UserId
