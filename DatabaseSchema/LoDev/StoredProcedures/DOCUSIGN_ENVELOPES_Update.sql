-- =============================================
-- Author:		Eric Mallare
-- Create date: 1/23/2018
-- Description:	Update a DocuSign envelope record.
-- =============================================
ALTER PROCEDURE [dbo].[DOCUSIGN_ENVELOPES_Update]
	@Id int,
	@LoanId uniqueidentifier,
	@BrokerId uniqueidentifier,
	@CurrentStatus int,
	@CreatedDate datetime = null,
	@DeletedDate datetime = null,
	@SentDate datetime = null,
	@DeliveredDate datetime = null,
	@SignedDate datetime = null,
	@CompletedDate datetime = null,
	@DeclinedDate datetime = null,
	@TimedOutDate datetime = null,
	@VoidedDate datetime = null,
	@VoidReason varchar(200) = null,
	@VoidDoneBy varchar(200) = null

AS
BEGIN
	UPDATE DOCUSIGN_ENVELOPES
	SET
		CurrentStatus=@CurrentStatus,
		CreatedDate=@CreatedDate,
		DeletedDate=@DeletedDate,
		SentDate=@SentDate,
		DeliveredDate=@DeliveredDate,
		SignedDate=@SignedDate,
		CompletedDate=@CompletedDate,
		DeclinedDate=@DeclinedDate,
		TimedOutDate=@TimedOutDate,
		VoidedDate=@VoidedDate,
		VoidReason=@VoidReason,
		VoidDoneBy=@VoidDoneBy
	WHERE
		Id=@Id AND
		LoanId=@LoanId AND
		BrokerId=@BrokerId
END
