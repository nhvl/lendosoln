-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- 07/31/2013 - OPM 118577 - Added RequestXml as selected field
-- =============================================
CREATE PROCEDURE [dbo].[XSLT_EXPORT_RESULT_STATUS_ByUserId]
	@UserId UniqueIdentifier 
AS
BEGIN
	SELECT TOP 100 ReportId, ReportName, CreatedDate, Status, OutputFileName, RequestXml
	FROM XSLT_EXPORT_RESULT_STATUS
	WHERE UserId = @UserId
	ORDER BY CreatedDate DESC
END
