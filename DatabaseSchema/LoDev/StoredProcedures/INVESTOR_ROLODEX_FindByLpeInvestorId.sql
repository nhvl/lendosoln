-- =============================================
-- Author:		David Dao
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE dbo.INVESTOR_ROLODEX_FindByLpeInvestorId
    @BrokerId uniqueidentifier,
    @LpeInvestorId int
AS
BEGIN
    SELECT InvestorRolodexId
    FROM INVESTOR_ROLODEX
    WHERE BrokerId = @BrokerId AND Status=1 AND LpeInvestorId=@LpeInvestorId

END
