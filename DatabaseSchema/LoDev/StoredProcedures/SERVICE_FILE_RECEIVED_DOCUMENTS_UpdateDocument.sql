ALTER PROCEDURE [dbo].[SERVICE_FILE_RECEIVED_DOCUMENTS_UpdateDocument]
	@Id int,
	@BrokerId uniqueidentifier,
	@DocumentId uniqueidentifier,
	@DocumentChecksum char(64)
AS
BEGIN
	UPDATE SERVICE_FILE_RECEIVED_DOCUMENTS
	SET
		DocumentId = @DocumentId,
		DocumentChecksum = @DocumentChecksum
	WHERE
		Id = @Id
		AND BrokerId = @BrokerId;
END
