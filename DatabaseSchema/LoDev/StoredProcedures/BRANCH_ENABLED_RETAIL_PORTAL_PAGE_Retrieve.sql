-- =============================================
-- Author:		Michael Leinweaver
-- Create date: 10/20/2017
-- Description:	Retrieves the custom pages enabled
--				for a retail portal at the branch
--				level.
-- =============================================
CREATE PROCEDURE [dbo].[BRANCH_ENABLED_RETAIL_PORTAL_PAGE_Retrieve]
	@BranchId uniqueidentifier
AS
BEGIN
	SELECT PageId
	FROM BRANCH_ENABLED_RETAIL_PORTAL_PAGE
	WHERE BranchId = @BranchId
END
