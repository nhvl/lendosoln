ALTER PROCEDURE [dbo].[TrackIntegrationModifiedFile] 
	@BrokerId uniqueidentifier,
	@LoanName varchar(36),
	@LoanId uniqueidentifier
AS
declare @tranPoint sysname
set @tranPoint = object_name(@@procId) + CAST(@@nestlevel as varchar(10))

SELECT sLoanFileT FROM LOAN_FILE_B where sLId = @LoanID AND sLoanFileT = 0
IF @@ROWCOUNT <> 1
	RETURN 0;
	
BEGIN TRANSACTION
SAVE TRANSACTION @tranPoint

DECLARE @AppCode UniqueIdentifier
DECLARE AppCodeCursor CURSOR FORWARD_ONLY STATIC READ_ONLY FOR
	SELECT AppCode FROM BROKER_WEBSERVICE_APP_CODE WHERE BrokerId = @BrokerId AND Enabled=1

OPEN AppCodeCursor

FETCH NEXT FROM AppCodeCursor INTO @AppCode

WHILE @@FETCH_STATUS = 0
BEGIN
	EXEC [dbo].[TrackIntegrationModifiedFile_SpecificAppCode] @BrokerId, @AppCode, @LoanName, @LoanId
	IF( 0!=@@ERROR )
	BEGIN
		RAISERROR('error inserting/update Integration_File_Modified', 16, 1);
		GOTO ErrorHandler
	END

	FETCH NEXT FROM AppCodeCursor INTO @AppCode
END
CLOSE AppCodeCursor
DEALLOCATE AppCodeCursor



COMMIT TRANSACTION
RETURN 0
ErrorHandler:
	CLOSE AppCodeCursor
	DEALLOCATE AppCodeCursor

	rollback tran @tranPoint
	rollback transaction
	return -100
