
-- =============================================
-- Author:		Long Nguyen
-- Create date: 8/4/2015
-- Description:	Update schedule report recovery (iOPM 215368): Add RunTime and RetryUntil
-- =============================================
CREATE PROCEDURE [dbo].[CreateMonthlyReport]
	@ReportId as int, 
	@Month as varchar(9), 
	@DateOfTheMonthOrWeekNo as int,
	@Weekday as varchar(9) = NULL,
	@EffectiveDate as datetime, 
	@LastRun as datetime = NULL, 
	@NextRun as datetime, 
	@EndDate as datetime,
	@RunTime as datetime = NULL,
	@RetryUntil as datetime = NULL
as
BEGIN
	insert into MONTHLY_REPORTS 
	values( @ReportId, 
			@Month, 
			@DateOfTheMonthOrWeekNo,
			@Weekday,
			@EffectiveDate, 
			@LastRun, 
			@NextRun, 
			@EndDate,
			@RunTime,
			@RetryUntil)
END
