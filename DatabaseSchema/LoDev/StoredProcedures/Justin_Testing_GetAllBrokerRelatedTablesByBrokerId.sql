-- =============================================
-- Author:		Justin Jia
-- Create date: 8/14/14
-- Description:	Get all table entries that only related to broker but not exported explicitly in the broker export tool.
-- =============================================
ALTER PROCEDURE [dbo].[Justin_Testing_GetAllBrokerRelatedTablesByBrokerId] 
	-- Add the parameters for the stored procedure here
	@BrokerId uniqueidentifier
AS
BEGIN
	SELECT 'AGENT' as TableName,AgentId FROM AGENT WHERE BrokerId = @BrokerId
	SELECT 'APPRAISAL_VENDOR_BROKER_ENABLED' as TableName,BrokerId,VendorId FROM APPRAISAL_VENDOR_BROKER_ENABLED WHERE BrokerId = @BrokerId
	SELECT 'ARM_INDEX' as TableName,IndexIdGuid FROM ARM_INDEX WHERE BrokerIdGuid = @BrokerId
	SELECT 'BROKER_WEBSERVICE_APP_CODE' as TableName,AppCode FROM BROKER_WEBSERVICE_APP_CODE WHERE BrokerId = @BrokerId
	SELECT 'BROKER_XSLT_REPORT_MAP' as TableName,BrokerId,XsltMapId FROM BROKER_XSLT_REPORT_MAP WHERE BrokerId = @BrokerId
	SELECT 'CC_TEMPLATE' as TableName,cCcTemplateId FROM CC_TEMPLATE WHERE BrokerId = @BrokerId
	SELECT 'CONDITION_CATEGORY' as TableName,ConditionCategoryId,BrokerId,Category FROM CONDITION_CATEGORY WHERE BrokerId = @BrokerId
	SELECT 'CONDITION_CHOICE' as TableName,BrokerId,ConditionChoiceId FROM CONDITION_CHOICE WHERE BrokerId = @BrokerId
	SELECT 'CONFIG_DRAFT' as TableName,OrgID FROM CONFIG_DRAFT WHERE OrgID = @BrokerId
	SELECT 'CONSUMER_PORTAL_USER' as TableName,Id FROM CONSUMER_PORTAL_USER WHERE BrokerId = @BrokerId
	SELECT 'CP_CONSUMER' as TableName,ConsumerId FROM CP_CONSUMER WHERE BrokerId = @BrokerId
	SELECT 'CREDIT_REPORT_ACCOUNT_PROXY' as TableName,CrAccProxyId FROM CREDIT_REPORT_ACCOUNT_PROXY WHERE BrokerId = @BrokerId
	SELECT 'CreditReportProtocol' as TableName,OwnerId,ServiceComId FROM CreditReportProtocol WHERE OwnerId = @BrokerId
	SELECT 'DATA_RETRIEVAL_PARTNER_BROKER' as TableName,BrokerId,PartnerId FROM DATA_RETRIEVAL_PARTNER_BROKER WHERE BrokerId = @BrokerId
	SELECT 'DOCMAGIC_ALTERNATE_LENDER' as TableName,DocMagicAlternateLenderId FROM DOCMAGIC_ALTERNATE_LENDER WHERE BrokerId = @BrokerId
	SELECT 'DOCUMENT_VENDOR_BROKER_SETTINGS' as TableName,BrokerId,VendorId,VendorIndex FROM DOCUMENT_VENDOR_BROKER_SETTINGS WHERE BrokerId = @BrokerId
	SELECT 'EDOCS_DOCMAGIC_DOCTYPE_X_DOCTYPE' as TableName,DocMagicDocTypeId,DocTypeId FROM EDOCS_DOCMAGIC_DOCTYPE_X_DOCTYPE WHERE BrokerId = @BrokerId
	SELECT 'EDOCS_DOCUMENT_TYPE' as TableName,DocTypeId FROM EDOCS_DOCUMENT_TYPE WHERE BrokerId = @BrokerId
	SELECT 'EDOCS_DOCUTECH_DOCTYPE_X_DOCTYPE' as TableName,DocTypeId,DocuTechDocTypeId FROM EDOCS_DOCUTECH_DOCTYPE_X_DOCTYPE WHERE BrokerId = @BrokerId
	SELECT 'EDOCS_FAX_NUMBER' as TableName,FaxNumber FROM EDOCS_FAX_NUMBER WHERE BrokerId = @BrokerId
	SELECT 'EDOCS_FOLDER' as TableName,FolderId FROM EDOCS_FOLDER WHERE BrokerId = @BrokerId
	SELECT 'EDOCS_IDS_DOCTYPE_X_DOCTYPE' as TableName,BrokerId,DocTypeId,IDSDocTypeId FROM EDOCS_IDS_DOCTYPE_X_DOCTYPE WHERE BrokerId = @BrokerId
	SELECT 'EDOCS_SHIPPING_TEMPLATE' as TableName,ShippingTemplateId FROM EDOCS_SHIPPING_TEMPLATE WHERE BrokerId = @BrokerId
	SELECT 'FEATURE_SUBSCRIPTION' as TableName,BrokerId,FeatureId FROM FEATURE_SUBSCRIPTION WHERE BrokerId = @BrokerId
	SELECT 'FEE_TYPE' as TableName,FeeTypeId FROM FEE_TYPE WHERE BrokerId = @BrokerId
	SELECT 'GROUP' as TableName,BrokerId,GroupId FROM [GROUP] WHERE BrokerId = @BrokerId
	SELECT 'INVESTOR_ROLODEX' as TableName,InvestorRolodexId FROM INVESTOR_ROLODEX WHERE BrokerId = @BrokerId
	SELECT 'IRS_4506T_VENDOR_BROKER_SETTINGS' as TableName,BrokerId,VendorId FROM IRS_4506T_VENDOR_BROKER_SETTINGS WHERE BrokerId = @BrokerId
	SELECT 'LENDER_LOCK_POLICY' as TableName,LockPolicyId FROM LENDER_LOCK_POLICY WHERE BrokerId = @BrokerId
	SELECT 'LENDER_LOCK_POLICY_CUSTOM_WARNING_MSGS' as TableName,BrokerId,LockPolicyId FROM LENDER_LOCK_POLICY_CUSTOM_WARNING_MSGS WHERE BrokerId = @BrokerId
	SELECT 'LENDER_LOCK_POLICY_HOLIDAY' as TableName,BrokerId,ClosureDate,LockPolicyId FROM LENDER_LOCK_POLICY_HOLIDAY WHERE BrokerId = @BrokerId
	SELECT 'Lender_TPO_LandingPageConfig' as TableName,Id FROM Lender_TPO_LandingPageConfig WHERE BrokerId = @BrokerId
	SELECT 'LO_FORM' as TableName,FormId FROM LO_FORM WHERE BrokerId = @BrokerId
	SELECT 'LOCKDESK_CLOSURE' as TableName,BrokerId,ClosureDate FROM LOCKDESK_CLOSURE WHERE BrokerId = @BrokerId
	SELECT 'LPE_PRICE_GROUP' as TableName,LpePriceGroupId FROM LPE_PRICE_GROUP WHERE BrokerId = @BrokerId
	SELECT 'LPE_PRICE_GROUP_PRODUCT' as TableName,lpp.lLpTemplateId,lpp.LpePriceGroupId FROM LPE_PRICE_GROUP_PRODUCT AS lpp inner join LPE_PRICE_GROUP AS lpg ON lpg.LpePriceGroupId = lpp.LpePriceGroupId where lpg.BrokerId = @BrokerId
	SELECT 'MBS_TRADE' as TableName,TradeId FROM MBS_TRADE WHERE BrokerId = @BrokerId
	SELECT 'MORTGAGE_POOL' as TableName,PoolId FROM MORTGAGE_POOL WHERE BrokerId = @BrokerId
	SELECT 'PML_BROKER' as TableName,BrokerId,PmlBrokerId FROM PML_BROKER WHERE BrokerId = @BrokerId
	SELECT 'RESERVE_STRING_LIST' as TableName,BrokerId,ID,Type FROM RESERVE_STRING_LIST WHERE BrokerId = @BrokerId
	SELECT 'ROLE_DEFAULT_PERMISSIONS' as TableName,BrokerId,RoleId FROM ROLE_DEFAULT_PERMISSIONS WHERE BrokerId = @BrokerId
	SELECT 'RS_EXPIRATION_CUSTOM_WARNING_MSGS' as TableName,BrokerId FROM RS_EXPIRATION_CUSTOM_WARNING_MSGS WHERE BrokerId = @BrokerId
	SELECT 'TASK_PERMISSION_LEVEL' as TableName,TaskPermissionLevelId,BrokerId,Name FROM TASK_PERMISSION_LEVEL WHERE BrokerId = @BrokerId
	SELECT 'TEAM' as TableName,Id FROM TEAM WHERE BrokerId = @BrokerId
	SELECT 'TITLE_VENDOR_X_BROKER' as TableName,BrokerId,TitleVendorId FROM TITLE_VENDOR_X_BROKER WHERE BrokerId = @BrokerId
	SELECT 'WAREHOUSE_LENDER_ROLODEX' as TableName,WarehouseLenderRolodexId FROM WAREHOUSE_LENDER_ROLODEX WHERE BrokerId = @BrokerId
END
