-- =============================================
-- Author:		Scott Kibler
-- Create date: 3/20/2017
-- Description:	
--	Removes the role from consideration for posting to Conversation Category given by CategoryName, BrokerId.
-- Implementation:
--  Remove the role from the ROLE_X_CONVERSATION_CATEGORY table for the given category.
-- =============================================
CREATE PROCEDURE [dbo].[IgnoreRoleForPostToCategory]
	@CategoryName varchar(100),
	@BrokerId uniqueidentifier,
	@RoleID uniqueidentifier
AS
BEGIN
	DELETE FROM
			ROLE_X_CONVERSATION_CATEGORY 
	WHERE 
			RoleId = @RoleId
		AND CategoryName = @CategoryName
		AND BrokerId = @BrokerId		
END
