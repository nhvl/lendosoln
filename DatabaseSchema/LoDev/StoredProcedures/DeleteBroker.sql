CREATE PROCEDURE DeleteBroker
	@BrokerID GUID
AS
	DELETE FROM Broker WHERE BrokerID = @BrokerID
