
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Accounting_ListFundedLoansByBrokerId]
	@BrokerId UniqueIdentifier,
	@StartD smalldatetime,
	@EndD smalldatetime
AS
BEGIN
		SELECT lc.sLNm, lc.sLT, lc2.sFinalLAmt, lc2.sOriginatorCompensationAmount, lc.sEmployeeLoanRepId, lc.sEmployeeLoanRepName,
		lc2.s800U1F, lc2.s800U1FDesc, br.BranchNm AS sBranchNm, lf.sInvestorLockAdjustXmlContent, lf.sBrokerLockAdjustXmlContent, lc.aBFirstNm, lc.aBLastNm, lc.sFundD, lc.sLPurchaseD
		FROM LOAN_FILE_CACHE lc JOIN LOAN_FILE_CACHE_2 lc2 ON lc.sLId=lc2.sLId
		     JOIN Branch br ON lc.sBranchId=br.BranchId JOIN LOAN_FILE_F lf ON lc.sLId = lf.sLId
		WHERE lc.sBrokerId = @BrokerId
		  AND lc.IsValid =1
		  AND lc.sFundD BETWEEN @StartD AND @EndD
		ORDER BY br.BranchNm
END

