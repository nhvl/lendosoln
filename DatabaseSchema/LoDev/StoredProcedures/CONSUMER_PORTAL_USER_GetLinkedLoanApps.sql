-- =============================================
-- Author:		David Dao
-- Create date: 6/6/2013
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[CONSUMER_PORTAL_User_GetLinkedLoanApps] 
	@ConsumerUserId bigint
AS
BEGIN

SELECT a.sLId, sLNm, lc.sStatusT, sSpAddr, lb.sSpCity, lb.sSpCounty, lb.sSpState, lb.sSpZip, sFinalLAmt, 
       sOpenedD, lc2.sLoanSubmittedD,lb.sLPurposeT,aOcct,sProdSpT, aBFirstNm, aBMidNm, aBLastNm, 
	   aBSuffix, sLeadD, lc.sLienPosT, ld.sSubFinT, lc3.sConsumerPortalSubmittedD,
	   lb.sConsumerPortalVerbalSubmissionD, lc4.sInitialLoanEstimateCreatedD, lc2.sGfeInitialDisclosureD,
	   lc3.sConsumerPortalCreationD, lc.sAppSubmittedD
FROM CONSUMER_PORTAL_USER_X_LOANAPP a JOIN LOAN_FILE_CACHE lc ON a.sLId = lc.sLId
                                      JOIN LOAN_FILE_CACHE_2 lc2 ON lc.sLId = lc2.sLId
									  JOIN LOAN_FILE_D ld ON lc.sLId = ld.sLId
									  JOIN LOAN_FILE_B lb ON lc.sLId = lb.sLId
									  JOIN LOAN_FILE_CACHE_3 lc3 ON lc.sLId = lc3.sLId
									  JOIN LOAN_FILE_CACHE_4 lc4 ON lc.sLId = lc4.sLId
WHERE a.ConsumerUserId = @ConsumerUserId
  AND lc.IsValid = 1		
  
END
