CREATE procedure [dbo].[GetScheduledReportData]
@ReportId int,
@ReportType varchar(10)
as

if(@ReportType ='Daily')
select *
from SCHEDULED_REPORT_USER_INFO as s JOIN DAILY_REPORTS as d 
on s.ReportId = d.ReportId 
where s.ReportId = @ReportId;

if(@ReportType ='Weekly')
select *
from SCHEDULED_REPORT_USER_INFO as s JOIN WEEKLY_REPORTS as w 
on s.ReportId = w.ReportId 
where s.ReportId = @ReportId;

if(@ReportType ='Monthly')
select *
from SCHEDULED_REPORT_USER_INFO as s JOIN MONTHLY_REPORTS as m
on s.ReportId = m.ReportId 
where s.ReportId = @ReportId
order by Month, DateOfTheMonthOrWeekNo;