-- =============================================
-- Author:		Eduardo
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE dbo.ListOriginatingCompaniesAssociatedWithBranchId 
	@BranchId uniqueidentifier,
	@BrokerId uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	Select p.Name, p.CompanyId, p.PmlBrokerId
	From dbo.PML_BROKER as p
	Where 
	p.BrokerId = @BrokerId AND
	(p.BranchId = @BranchId OR p.MiniCorrespondentBranchId = @BranchId OR p.CorrespondentBranchId = @BranchId)
END
