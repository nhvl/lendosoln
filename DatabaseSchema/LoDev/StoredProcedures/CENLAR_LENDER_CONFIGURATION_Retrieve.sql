-- =============================================
-- Author:		Justin Lara
-- Create date: 3/27/2018
-- Description:	Retrieves a Cenlar configuration.
-- =============================================
ALTER PROCEDURE [dbo].[CENLAR_LENDER_CONFIGURATION_Retrieve]
	@BrokerId uniqueidentifier
AS
BEGIN
    SELECT
		BrokerId,
		EnableAutomaticTransmissions,
		DummyUserId,
		CustomReportName,
		BatchExportName,
		NotificationEmails,
		ShouldSuppressAcceptedLoanNotifications,
		RemoveProblematicPayloadCharacters
	FROM CENLAR_LENDER_CONFIGURATION
	WHERE BrokerId = @BrokerId
END
