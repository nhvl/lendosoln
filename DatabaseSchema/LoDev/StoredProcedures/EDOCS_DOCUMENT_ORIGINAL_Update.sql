-- =============================================
-- Author:		David Dao
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE dbo.EDOCS_DOCUMENT_ORIGINAL_Update 
    @BrokerId UniqueIdentifier,
    @DocumentId UniqueIdentifier,
	@SourceT int,
	@MetadataJsonContent varchar(max),
	@NumPages int,
	@PdfSizeInBytes int,
	@PngSizeInBytes int
AS
BEGIN
	UPDATE EDOCS_DOCUMENT_ORIGINAL
		SET MetadataJsonContent = @MetadataJsonContent,
			NumPages = @NumPages,
			PdfSizeInBytes = @PdfSizeInBytes,
			PngSizeInBytes = @PngSizeInBytes,
			SourceT = @SourceT,
			CompletedDate = GETDATE()
	WHERE BrokerId = @BrokerId AND DocumentId = @DocumentId

END
