CREATE PROCEDURE [dbo].[DROPBOX_CountFiles]
	@UserId uniqueidentifier,
	@BrokerId uniqueidentifier = null
AS
BEGIN
	
	SELECT SUM(NumFiles) as NumFiles
	FROM 
		(
			SELECT COUNT(*) as NumFiles from DROPBOX_FILES with(nolock) where UserId = @UserId
			UNION ALL
			SELECT COUNT(*) as NumFiles FROM EDOCS_LOST_DOCUMENT WHERE BrokerID = @BrokerID and BrokerId is not null and DocumentSource = 1
		) s
END