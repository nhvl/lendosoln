ALTER PROCEDURE [dbo].[RetrieveMCTDataXml]
	@CustomerCode varchar(50)
AS
BEGIN
DECLARE @BrokerID uniqueidentifier;
SELECT @BrokerID=brokerid FROM Broker WHERE CustomerCode=@CustomerCode

DECLARE @ReportStartDate smalldatetime;
SELECT @ReportStartDate = DATEADD("mm", -6, GETDATE()) -- go back 6 months
-- dd 4/6/2011 - The order of SELECT columns ARE VERY IMPORTANT. Third party tools rely on this order.
--               Therefore when adding new column always add to the end
SELECT sRLckdD As LockDate,
	sLNm As LoanNumber,
	aBNm AS aBNm,
	sLpTemplateNm AS LoanProgram,
	sNoteIR AS NoteRate,
	sFinalLAmt AS TotalLoanAmt,
	lc2.sRLckdDays AS LockDays,
	sSpState AS State,
	sRLckdExpiredD AS LockExpiration,
	case( sStatusT ) 
		when '0' then 'Loan Open' 
		when '1' then 'Loan Pre-qual' 
		when '2' then 'Loan Pre-approve' 
		when '3' then 'Loan Registered' 
		when '4' then 'Loan Approved' 
		when '5' then 'Loan Docs' 
		when '6' then 'Loan Funded' 
		when '7' then 'Loan On hold' 
		when '8' then 'Loan Suspended' 
		when '9' then 'Loan Canceled' 
		when '10' then 'Loan Rejected' 
		when '11' then 'Loan Closed' 
		when '12' then 'Lead Open' 
		when '13' then 'Loan Underwriting' 
		when '14' then 'Web Consumer' 
		when '15' then 'Lead Canceled' 
		when '16' then 'Lead Declined' 
		when '17' then 'Lead Other' 
		when '18' then 'Loan Other' 
		when '19' then 'Loan Recorded' 
		when '20' then 'Loan Shipped' 
		when '21' then 'Loan Clear to Close' 
		when '22' then 'Loan Processing' 
		when '23' then 'Loan Final Underwriting' 
		when '24' then 'Loan Docs Back' 
		when '25' then 'Loan Funding Conditions' 
		when '26' then 'Loan Final Docs' 
		when '27' then 'Loan Purchased' -- Loan Sold 
		when '28' then 'Loan Submitted'
		when '29' then 'Loan Pre-Processing'    -- start sk 1/6/2014 opm 145251
		when '30' then 'Loan Document Check'
		when '31' then 'Loan Document Check Failed'
		when '32' then 'Loan Pre-Underwriting'
		when '33' then 'Loan Condition Review'
		when '34' then 'Loan Pre-Doc QC'
		when '35' then 'Loan Docs Ordered'
		when '36' then 'Loan Docs Drawn'
		when '37' then 'Loan Investor Conditions'       --  tied to sSuspendedByInvestorD
		when '38' then 'Loan Investor Conditions Sent'   --  tied to sCondSentToInvestorD
		when '39' then 'Loan Ready For Sale'
		when '40' then 'Loan Submitted For Purchase Review'
		when '41' then 'Loan In Purchase Review'
		when '42' then 'Loan Pre-Purchase Conditions'
		when '43' then 'Loan Submitted For Final Purchase Review'
		when '44' then 'Loan In Final Purchase Review'
		when '45' then 'Loan Clear To Purchase'
		when '46' then 'Loan Purchased'        -- don't confuse with the old { E_sStatusT.Loan_LoanPurchased, which is now Loan Sold in the UI.
		when '47' then 'Loan Counter Offer Approved'
		when '48' then 'Loan Withdrawn'
		when '49' then 'Loan Archived'  
	end as LoanStatus, 
	case( sProdImpound ) 
		when 1 then 'Yes' 
		when 0 then 'No' 
	end as Impound, 
	sLtvR AS LTV,
	sCltvR AS CLTV,
	sCreditScoreType2 AS QualifyingScore,
	case( sLPurposeT ) 
		when '3' then 'Construction' 
		when '4' then 'Construction Permanent' 
		when '5' then 'Other' 
		when '0' then 'Purchase' 
		when '1' then 'Refinance' 
		when '2' then 'Refinance Cash-out' 
		when '6' then 'FHA Streamlined Refinance' 
		when '7' then 'VA IRRRL'
	end as LoanPurpose, 
	case (sLPurposeT) 
		when '1' then 'Rate & Term'
		when '2' then 'Cashout'
		else '' 
	end as Refi_Reason, 
	case( sProdSpT ) 
		when '8' then 2
		when '9' then 3
		when '10' then 4
		else 1
	end as NumberofUnits,
	case( aOccT ) 
		when '2' then 'Investment' 
		when '0' then 'Primary Residence' 
		when '1' then 'Secondary Residence' 
	end as OccupancyType, 
	sTerm AS Term,
	sQualBottomR AS DTI,
	case( sProdSpT ) 
		when '0' then 'SFR / Detached' 
		when '8' then '2 Units' 
		when '9' then '3 Units' 
		when '10' then '4 Units' 
		when '1' then 'PUD' 
		when '6' then 'Commercial' 
		when '2' then 'Condo' 
		when '3' then 'Co-Op' 
		when '4' then 'Manufactured' 
		when '7' then 'Mixed Use' 
		when '5' then 'Attached PUD' 
		when '11' then 'Modular' 
		when '12' then 'Rowhouse' 
	end AS PropertyType,
	-lc2.sBrokComp1Pc AS BEPrice,	
	CASE
		WHEN (lc2.sInvestorLockLpInvestorNm IS NULL OR lc2.sInvestorLockLpInvestorNm = '') THEN lc.sLpInvestorNm
		ELSE lc2.sInvestorLockLpInvestorNm
	END AS InvestorName,

sBrokerLockFinalBrokComp1PcPrice,-sBrokerLockTotalLenderAdj AS sBrokerLockTotalLenderAdj,
lc.sLPurchaseD AS PurchaseDate,
lc2.sInvestorLockRLckdD AS CommitmentDate,
sRLckDN AS LockDateComment,
sInvestorLockBrokComp1PcPrice,
sSubmitN AS RegisteredDateComment,
	case ( lc2.sInvestorLockCommitmentT )
		when 0 then 'Best Efforts'
		when 1 then 'Mandatory'
		when 2 then 'Hedged'
		when 3 then 'Securitized'		
	end as InvestorCommitmentType,
	case ( lc2.sInvestorLockRateLockStatusT )
		when 0 then 'Not Locked'
		when 1 then 'Locked'
	end as InvestorRateLockStatus,
lc2.sInvestorLockRateSheetId AS BaseBuyPrice,
sInvestorLockBaseBrokComp1PcPrice AS BaseSellPrice,
lc2.sInvestorLockRateSheetEffectiveTime AS SellPrice,
sCanceledN AS FalloutDate,
lc2.sInvestorLockRateSheetID AS InvestorRateSheetId,
sInvestorLockProjectedProfit AS GrossProjProfitAndLoss,
sTrNotes AS RateLockInternalNotes,
lc2.sInvestorLockConfNum,
sEmployeeLoanRepName,
br.BranchNm AS sBranchNm,
lc2.sInvestorLockLoanNum AS InvestorLoanNumber,
sfundd AS sFundD,
aBFirstNm AS aBFirstNm,
aBLastNm AS aBLastNm,
case (sLT)
	when '0' then 'Conventional'
	when '1' then 'FHA'
	when '2' then 'VA'
	when '3' then 'USDA Rural'
	when '4' then 'Other'
end AS LoanType,
sCustomField14Pc, -- OPM 136102
sCustomField14Notes, -- OPM 136102
sPurchaseAdviceTotalPrice_Field1, -- OPM 139768
sLAmtCalc, -- OPM 141898
CASE ( lc2.sProd3rdPartyUwResultT )
		WHEN '0' THEN 'None/Not Submitted'
		WHEN '1' THEN 'DU Approve/Eligible'
		WHEN '2' THEN 'DU Approve/Ineligible'
		WHEN '3' THEN 'DU Refer/Eligible'
		WHEN '4' THEN 'DU Refer/Ineligible'
		WHEN '5' THEN 'DU Refer with Caution/Eligible'
		WHEN '6' THEN 'DU Refer with Caution/Ineligible'
		WHEN '7' THEN 'DU EA I/Eligible'
		WHEN '8' THEN 'DU EA II/Eligible'
		WHEN '9' THEN 'DU EA III/Eligible'
		WHEN '10' THEN 'LP Accept/Eligible'
		WHEN '11' THEN 'LP Accept/Ineligible'
		WHEN '12' THEN 'LP Caution/Eligible'
		WHEN '13' THEN 'LP Caution/Ineligible'
		WHEN '14' THEN 'Out of Scope'
		WHEN '15' THEN 'LP A- Level 1'
		WHEN '16' THEN 'LP A- Level 2'
		WHEN '17' THEN 'LP A- Level 3'
		WHEN '18' THEN 'LP A- Level 4'
		WHEN '19' THEN 'LP A- Level 5'
		WHEN '20' THEN 'LP Refer'
		WHEN '21' THEN 'TOTAL Approve/Eligible'
		WHEN '22' THEN 'TOTAL Approve/Ineligible'
		WHEN '23' THEN 'TOTAL Refer/Eligible'
		WHEN '24' THEN 'TOTAL Refer/Ineligible'
		WHEN '25' THEN 'GUS Accept/Eligible'
		WHEN '26' THEN 'GUS Accept/Ineligible'
		WHEN '27' THEN 'GUS Refer/Eligible'
		WHEN '28' THEN 'GUS Refer/Ineligible'
		WHEN '29' THEN 'GUS Refer with Caution/Eligible'
		WHEN '30' THEN 'GUS Refer with Caution/Ineligible'	
END as AUResponse, -- OPM 144952
sStatusD, -- OPM 170062
sSpCounty, -- OPM 170062
CASE (lc.sProdDocT)
    WHEN '0' THEN 'Full Document'
    WHEN '1' THEN 'Alt - 12 months bank stmts'
    WHEN '2' THEN 'Lite - 6 months bank stmts'
    WHEN '3' THEN 'NIV (SIVA) - Stated Income, Verified Assets'
    WHEN '4' THEN 'VISA - Verified Income, Stated Assets'
    WHEN '5' THEN 'NIV (SISA) - Stated Income, Stated Assets'
    WHEN '6' THEN 'No Ratio - No Income, Verified Assets'
    WHEN '7' THEN 'NINA - No Income, No Assets'
    WHEN '8' THEN 'NISA - No Income, Stated Assets'
    WHEN '9' THEN 'No Doc - No Income, No Assets, No Empl'
    WHEN '10' THEN 'NoDoc Verif Assets - No Income, Verified Assets, No Empl'
    WHEN '11' THEN 'VINA - Verified Income, No Assets'
    WHEN '12' THEN 'Streamline'
END AS DocType, -- OPOM 170062
sClosedD, -- OPM 200032
--OPM 206049
sBrokerLockBaseBrokComp1PcPrice,
sBrokerLockTotHiddenAdjBrokComp1PcPriceCR as sBrokerLockTotHiddenAdjBrokComp1PcPrice,
sBrokerLockBrokerBaseBrokComp1PcPrice,
sBrokerLockTotVisibleAdjBrokComp1PcPriceCR as sBrokerLockTotVisibleAdjBrokComp1PcPrice,
sBrokerLockAdjustXmlContent,
sInvestorLockTotAdjBrokComp1PcPrice,
sInvestorLockAdjustXmlContent,
--OPM 206348
pmlbroker.Name as pmlBrokerName,
--OPM 214292
sSpAddr,
sSpCity,
sSpZip,
sApprVal,
sPurchPrice,
--OPM 215872
(lc2.sBrokComp1Pc - sBrokerLockTotalLenderAdj) as FEPointsBeforeLenderAdj,
(sBrokerLockOriginatorPriceBrokComp1PcFee - lc2.sBrokComp1Pc) as LPOCAdjustment,
--OPM 216495
division as BranchDivision,
--OPM 225743
lc2.sBranchChannelT as Channel,
case (@CustomerCode) -- IR 11/6/2015: sLendingSTaffNotes column is big. Provide only for PML0272.
  when 'PML0272' then lc2.sLendingStaffNotes
  else ''
end as LendingStaffNotes,
lc.sShippedToInvestorD as LoanShippedDate,
lc.sLeadSrcDesc as LeadSource,
sLTotI as TotalIncome,
case -- ML 6/3/2016: Provide only for PML0218. IR 10/20/2017 - Add PML0252
  when (@CustomerCode = 'PML0218') or (@CustomerCode = 'PML0252') then lc.sCustomField21Notes
  else ''
end as sCustomField21Notes,
case -- ML 6/3/2016: Provide only for PML0218. IR 10/20/2017 - Add PML0252
  when (@CustomerCode = 'PML0218') or (@CustomerCode = 'PML0252') then lc.sCustomField22Notes
  else ''
end as sCustomField22Notes,
lc2.sDuCaseId,
lc2.sFreddieLoanId,
lc2.sLpAusKey,
case (@CustomerCode) -- JL 3/30/2017: Provide only for PML0170 per OPM 452937.
  when 'PML0170' then lc.sCustomField29Money
  else ''
end as sCustomField29Money,
lc.sSchedDueD1 as sSchedDueD1,
lc4.sUniversalLoanIdentifier as sUniversalLoanIdentifier,
CASE (lc2.sMiCompanyNmT)
    WHEN '0' THEN ''
    WHEN '1' THEN 'CMG'
    WHEN '2' THEN 'Essent'
    WHEN '3' THEN 'Genworth'
    WHEN '4' THEN 'MGIC'
    WHEN '5' THEN 'Radian'
    WHEN '6' THEN 'UnitedGuaranty'
    WHEN '7' THEN 'Amerin'
    WHEN '8' THEN 'CAHLIF'
    WHEN '9' THEN 'CMGPre94'
    WHEN '10' THEN 'Commonwealth'
    WHEN '11' THEN 'MDHousing'
    WHEN '12' THEN 'MIF'
    WHEN '13' THEN 'PMI'
    WHEN '14' THEN 'RMIC'
    WHEN '15' THEN 'REMICNC'
    WHEN '16' THEN 'SONYMA'
    WHEN '17' THEN 'Triad'
    WHEN '18' THEN 'Verex'
    WHEN '19' THEN 'WiscMtgAssr'
    WHEN '20' THEN 'FHA'
    WHEN '21' THEN 'VA'
    WHEN '22' THEN 'USDA'
    WHEN '23' THEN 'NationalMI'
    WHEN '24' THEN 'Arch'
    WHEN '25' THEN 'MassHousing'
END AS sMiCompanyNmT,
CASE (lc2.sMiInsuranceT)
	WHEN '0' THEN 'None'
	WHEN '1' THEN 'BorrowerPaid'
	WHEN '2' THEN 'LenderPaid'
	WHEN '3' THEN 'InvestorPaid'
END AS sMiInsuranceT,
lc3.sU1LockFieldDesc,
lc3.sU1LockFieldD,
lc3.sU1LockFieldAmt,
lc3.sU1LockFieldPc,
lc3.sU2LockFieldDesc,
lc3.sU2LockFieldD,
lc3.sU2LockFieldAmt,
lc3.sU2LockFieldPc
	FROM LOAN_FILE_CACHE lc WITH(NOLOCK) JOIN LOAN_FILE_CACHE_2 lc2 WITH(NOLOCK) ON lc.sLId = lc2.sLId
										 JOIN LOAN_FILE_CACHE_3 lc3 WITH(NOLOCK) ON lc.SLId = lc3.sLId
										 JOIN LOAN_FILE_CACHE_4 lc4 WITH(NOLOCK) ON lc.sLId = lc4.sLId
										 JOIN BRANCH br WITH(NOLOCK) ON lc.sBranchId = br.BranchId
										 JOIN LOAN_FILE_F lf WITH(NOLOCK) ON lc.sLId = lf.sLId
										 LEFT JOIN PML_Broker pmlbroker WITH(NOLOCK) ON lc.sPmlBrokerId = pmlbroker.PmlBrokerId
where
	( sBrokerId = @BrokerID ) and
	( IsValid = 1 ) and
	( IsTemplate = 0 ) and
	( sLoanFileT = 0 ) and
	( lc2.sBranchChannelT <> 4 ) and -- OPM 239650: Exclude loans that are brokered by the client. The client in this case does not fund the loan, so no hedging is performed.
	( lc.sRateLockStatusT  = 1 or lc.sRateLockStatusT  = 3 ) and
	(
		(sRLckdExpiredD >= @ReportStartDate)
		OR (sFundD >= @ReportStartDate)
		OR (lc.sLPurchaseD >= @ReportStartDate)
		OR (sFundD is not null and lc.sLPurchaseD is null)
	)
order by  
	sLpTemplateNm asc , 
	case( sStatusT) 
		when '15' then 0 
		when '16' then 1 
		when '12' then 2 
		when '17' then 3 
		when '4' then 4 
		when '9' then 5 
		when '11' then 6 
		when '5' then 7 
		when '6' then 8 
		when '7' then 9 
		when '0' then 10 
		when '18' then 11 
		when '2' then 12 
		when '1' then 13 
		when '19' then 14 
		when '10' then 15 
		when '3' then 16 
		when '8' then 17 
		when '13' then 18 
		when '20' then 19 
		when '21' then 20 
		else 21 
	end asc

END
