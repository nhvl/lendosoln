-- =============================================
-- Author:		David Dao
-- Create date: Aug 8, 2013
-- Description:	
-- =============================================
CREATE PROCEDURE dbo.IRS_4506T_ORDER_INFO_RetrieveResponseXmlContent 
    @TransactionId UniqueIdentifier
AS
BEGIN

SELECT ResponseXmlContent
FROM IRS_4506T_ORDER_INFO
WHERE TransactionId = @TransactionId

END
