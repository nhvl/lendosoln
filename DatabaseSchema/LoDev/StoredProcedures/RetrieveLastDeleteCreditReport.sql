CREATE PROCEDURE RetrieveLastDeleteCreditReport 
	@ApplicationID Guid
AS
	SELECT TOP 1 ExternalFileID, DbFileKey, ComId, CrAccProxyId
	FROM SERVICE_FILE
	WHERE Owner = @ApplicationID
                           AND IsInEffect = 0
		AND ServiceType='Credit' AND FileType='CreditReport'
	ORDER BY TImeVersion DESC
	if( 0!=@@error)
	begin
		RAISERROR('Error in the select statement in RetrieveCreditReport sp', 16, 1);
		return -100;
	end
	return 0;
