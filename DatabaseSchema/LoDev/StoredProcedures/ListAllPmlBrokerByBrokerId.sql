-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[ListAllPmlBrokerByBrokerId] 
	@BrokerId UniqueIdentifier
AS
BEGIN
	SELECT 
		p.*,
		'' AS TaxId, -- Hack for the SQL service in Case 473461
		b.BranchNm, 
		l.LpePriceGroupName,
		perm.CanApplyForIneligibleLoanPrograms, 
		perm.CanRunPricingEngineWithoutCreditReport, 
		perm.CanSubmitWithoutCreditReport, 
		perm.CanAccessTotalScorecard, 
		perm.AllowOrder4506T, 
		perm.TaskRelatedEmailOptionT,
		dbo.GetPmlBrokerRelationshipsAsXml(p.BrokerId, p.PmlBrokerId) as 'RelationshipXml'
	FROM Pml_Broker p
	LEFT OUTER JOIN PML_BROKER_PERMISSIONS perm ON p.PmlBrokerId = perm.PmlBrokerId
	LEFT OUTER JOIN BRANCH b ON p.BranchId = b.BranchId
	LEFT OUTER JOIN LPE_PRICE_GROUP l ON p.LpePriceGroupId = l.LpePriceGroupId
	WHERE p.BrokerId = @BrokerId
	ORDER BY Name
END
