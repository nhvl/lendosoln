
CREATE PROCEDURE [dbo].[GetNumOriginatingCompaniesAssociatedWithBranch] 
	@BranchId uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
SELECT
		COUNT(*) AS numOriginatingCompanies
	FROM 
		PML_BROKER
		
	WHERE
		BranchId = @BranchId
END