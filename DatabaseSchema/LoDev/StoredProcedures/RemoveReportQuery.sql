-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- 08/07/2013 - OPM 132539 - Remove query associations with Batch Exports
-- =============================================
CREATE PROCEDURE [dbo].[RemoveReportQuery]
	@QueryID uniqueidentifier
AS
	SET NOCOUNT ON
	BEGIN TRAN
	
		DELETE FROM BROKER_XSLT_REPORT_MAP WHERE QueryId = @QueryID
		if( @@error != 0 ) GOTO ERROR

		DELETE FROM EMPLOYEE_FAVORITE_REPORTS WHERE QueryID = @QueryID
		if( @@error != 0 ) GOTO ERROR

		DELETE	FROM Report_Query	WHERE QueryID = @QueryID
		if( @@error != 0 ) GOTO ERROR

	COMMIT TRAN
	RETURN

	ERROR:		
		RAISERROR('Error in the delete statement in RemoveReportQuery sp', 16, 1);
		return -100;
		ROLLBACK TRAN
RETURN