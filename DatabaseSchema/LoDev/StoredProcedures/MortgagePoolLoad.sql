-- =============================================
-- Author:		Paolo Arrastia
-- Create date: 6 Aug 2012
-- Description:	
-- =============================================
ALTER PROCEDURE [dbo].[MortgagePoolLoad] 
	-- Add the parameters for the stored procedure here
	@poolId bigint
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    
    WITH PoolData AS 
    (
		SELECT TOP 1 *
		FROM MORTGAGE_POOL
		WHERE PoolId = @poolId
	),
	AllLoans (TotalCurrentBalance, RawBackEndBrokerBasePricing, LowR, HighR, LoanCount, ProfitabilityAmountAllocated, RawFrontEndRateLockPricing) AS
	(
		SELECT	ISNULL(SUM(sServicingUnpaidPrincipalBalance ), 0),
				ISNULL(SUM(sServicingUnpaidPrincipalBalance * CAST(REPLACE(REPLACE(sInvestorLockBaseBrokComp1PcPrice, '%', ''), ',', '') as decimal(9,5))), 0),
				ISNULL(MIN(sNoteIR), 0),
				ISNULL(MAX(sNoteIR), 0),
				COUNT(*),
				ISNULL(SUM(sServicingUnpaidPrincipalBalance * (sInvestorLockProjectedProfit / 100)), 0),
				ISNULL(SUM(sServicingUnpaidPrincipalBalance * (sBrokerLockFinalBrokComp1PcPrice / 100)), 0)
		FROM LOAN_FILE_CACHE lc JOIN LOAN_FILE_CACHE_2 lc2 ON lc.sLId = lc2.sLId
		WHERE sMortgagePoolId = @poolId AND ( IsValid = 1 ) AND ( IsTemplate = 0 )
	),
	ShippedLoans (UnpaidBalanceAmountShipped, ProfitabilityAmountShipped, ShippedLoanCount) AS
	(
		SELECT  ISNULL(SUM(sServicingUnpaidPrincipalBalance), 0),
				ISNULL(SUM(sServicingUnpaidPrincipalBalance * (sInvestorLockProjectedProfit / 100)), 0),
				COUNT(*)
		FROM LOAN_FILE_CACHE lc JOIN LOAN_FILE_CACHE_2 lc2 ON lc.sLId = lc2.sLId
		WHERE sMortgagePoolId = @poolId AND (IsValid = 1) AND (IsTemplate = 0) AND (sShippedToInvestorD IS NOT NULL)
	),
	PurchasedLoans (UnpaidBalanceAmountPurchased, ProfitabilityAmountPurchased, PurchasedLoanCount) AS
	(
		SELECT  ISNULL(SUM(sServicingUnpaidPrincipalBalance), 0),
				ISNULL(SUM(sServicingUnpaidPrincipalBalance * (sInvestorLockProjectedProfit / 100)), 0),
				COUNT(*)
		FROM LOAN_FILE_CACHE lc JOIN LOAN_FILE_CACHE_2 lc2 ON lc.sLId = lc2.sLId
		WHERE sMortgagePoolId = @poolId AND (IsValid = 1) AND (IsTemplate = 0) AND (sLPurchaseD IS NOT NULL)
	)
	SELECT TOP 1 *
	FROM AllLoans CROSS JOIN PoolData CROSS JOIN ShippedLoans CROSS JOIN PurchasedLoans;
END