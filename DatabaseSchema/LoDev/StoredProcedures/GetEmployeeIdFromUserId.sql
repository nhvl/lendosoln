-- =============================================
-- Author:		Eric Mallare
-- Create date: 5/11/2016 
-- Description:	Gets the Employee Id using a User Id. Only checks active brokers.

-- =============================================
CREATE PROCEDURE [dbo].[GetEmployeeIdFromUserId]  
	@UserId uniqueidentifier
AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		bu.EmployeeId
	FROM
		BROKER_USER bu JOIN
		EMPLOYEE e ON bu.EmployeeId=e.EmployeeId JOIN
		BRANCH br on br.BranchId=e.BranchId JOIN
		BROKER b on br.BrokerId=b.BrokerId
	WHERE
		bu.UserId=@UserId AND
		b.Status=1
    
END
GO
