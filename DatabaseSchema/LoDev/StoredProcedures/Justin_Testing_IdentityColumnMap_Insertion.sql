



-- =============================================
-- Author:		Justin Jia
-- ALTER date: 	06-13-2014
-- Description:	Insert a new identity key mapping entry
-- =============================================
CREATE PROCEDURE [dbo].[Justin_Testing_IdentityColumnMap_Insertion]
	@StageSiteSourceName char(100),
	@DatabaseSourceName char(100),
	@TableName char(100),
	@ColumnName char(100),
	@ColumnType char(50),
	@SourceDbValue varchar(200),
	@ThisDbValue varchar(200)
AS
BEGIN
	
	INSERT INTO DATA_IMPORT_IDENTITY_COLUMN_MAP (StageSiteSourceName, DatabaseSourceName, TableName, ColumnName, ColumnType, SourceDbValue, ThisDbValue)
                VALUES(@StageSiteSourceName, @DatabaseSourceName, @TableName, @ColumnName, @ColumnType, @SourceDbValue, @ThisDbValue)

END




