CREATE PROCEDURE [GetGroupMembershipsForEmployee]
	@BrokerId uniqueidentifier,
	@EmployeeId uniqueidentifier
AS
BEGIN
	SET NOCOUNT ON
	-- GroupType 1 are Employee groups
	SELECT CASE WHEN ge.EmployeeId IS NULL THEN 0 ELSE 1 END AS IsInGroup, g.* 
	FROM [GROUP] g LEFT JOIN (SELECT * FROM GROUP_x_EMPLOYEE WHERE EmployeeId=@EmployeeId) ge on g.GroupId =ge.GroupId 
	WHERE g.GroupType = 1 AND ge.EmployeeId=@EmployeeId AND g.BrokerID=@BrokerId ORDER BY g.GroupName
END
