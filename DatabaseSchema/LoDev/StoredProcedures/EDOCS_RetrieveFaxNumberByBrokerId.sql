ALTER procedure [dbo].[EDOCS_RetrieveFaxNumberByBrokerId]
	@BrokerId uniqueidentifier
as
SELECT FaxNumber, DocRouterLogin, EncryptionKeyId, EncryptedDocRouterPassword
from EDOCS_FAX_NUMBER
where BrokerId = @BrokerId