ALTER  PROCEDURE [dbo].[FindActiveLOUser]
	@UserFirstNm Varchar( 32 ) = NULL , 
    @UserLastNm Varchar( 32 ) = NULL , 
    @UserNm Varchar( 32 ) = NULL , 
    @LoginNm Varchar( 32 ) = NULL , 
    @Email Varchar( 80 ) = NULL , 
    @BrokerNm Varchar( 128 ) = NULL , 
    @IsActive Bit = NULL,
    @CanLogin bit = NULL,
    @BrokerStatus int = NULL
AS
	
	SELECT TOP 100
        e.EmployeeId ,
        e.UserFirstNm + ' ' + e.UserLastNm AS FullName ,
        e.Email ,
        e.LoginNm ,
        e.Type ,
        r.BranchNm ,
        e.BrokerNm ,
        e.BrokerId ,
        e.UserId ,
        e.IsActive ,
        e.RecentLoginD,
        e.CanLogin,
        e.BrokerPmlSiteId
    FROM
        View_Active_LO_User_WITH_BROKER_AND_LICENSE_INFO AS e  WITH (NOLOCK) 
        JOIN Branch AS r WITH (NOLOCK) ON r.BranchId = e.BranchId
    
    WHERE
		((@UserFirstNm IS NULL) OR (e.UserFirstNm LIKE '' + @UserFirstNm + '%'))
	AND
		((@UserLastNm IS NULL)	OR (e.UserLastNm LIKE '' + @UserLastNm + '%'))
	AND
		((@UserNm IS NULL )		OR (   (e.UserFirstNm LIKE '' + @UserNm + '%') 
									OR (e.UserLastNm  LIKE '' + @UserNm + '%')))
	AND
		((@LoginNm IS NULL)		OR (e.LoginNm LIKE '' + @LoginNm + '%'))
	AND
		((@Email IS NULL)		OR (e.Email LIKE '' + @Email + '%'))
	AND
		((@BrokerNm IS NULL)	OR (e.BrokerNm LIKE '' + @BrokerNm + '%'))
	AND
		((@IsActive IS NULL)	OR (e.IsActive = @IsActive))
	AND
		((@CanLogin IS NULL)	OR (e.CanLogin = @CanLogin))
	AND
		((@BrokerStatus IS NULL)OR (e.Status = @BrokerStatus))

