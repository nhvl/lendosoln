-- =============================================
-- Author:		Antonio Valencia
-- Create date: 3/7/11
-- Description:	Bumps Loan File Condition Version
-- =============================================
CREATE PROCEDURE LOAN_BumpLoanConditionVersion 
	@LoanId uniqueidentifier 
AS
BEGIN
	UPDATE LOAN_FILE_F
	SET sConditionSummaryVersion = (sConditionSummaryVersion +1 )
	WHERE sLid = @LoanId
	
	
END
