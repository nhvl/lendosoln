CREATE PROCEDURE [dbo].[RetrieveBrokerIdByCustomerCode] 
	@CustomerCode varchar(50)
AS
	SELECT BrokerID 
	FROM DB_CONNECTION_X_BROKER WITH (NOLOCK) 
	WHERE CustomerCode=@CustomerCode