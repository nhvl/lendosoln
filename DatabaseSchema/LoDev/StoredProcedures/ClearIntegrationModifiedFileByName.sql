ALTER PROCEDURE [dbo].[ClearIntegrationModifiedFileByName] 
	@AppCode uniqueidentifier,
	@BrokerId uniqueidentifier,
	@sLNm varchar(36)
AS
DELETE FROM Integration_File_Modified WITH(ROWLOCK, XLOCK)
WHERE AppCode=@AppCode AND BrokerId = @BrokerId AND sLNm = @sLNm
