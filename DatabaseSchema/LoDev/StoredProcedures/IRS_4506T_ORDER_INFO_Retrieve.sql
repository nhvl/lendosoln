CREATE PROCEDURE [dbo].[IRS_4506T_ORDER_INFO_Retrieve]
    @sLId uniqueIdentifier,
    @VendorId uniqueIdentifier,
    @OrderNumber varchar(50)
    
AS
BEGIN

    SELECT order_info.*, cache.sBrokerId FROM IRS_4506T_ORDER_INFO order_info JOIN LOAN_FILE_CACHE cache ON order_info.sLId=cache.sLId
    WHERE
        order_info.sLId = @sLId
    AND VendorId = @VendorId
    AND OrderNumber = @OrderNumber

END
