ALTER PROCEDURE [dbo].[Billing_Appraisal_RapidAMS]
	@StartDate datetime,
	@EndDate datetime
AS
BEGIN
-- dd 2018-11-02 - For use in automate billing executable.

SELECT BrokerNm As LenderName, CustomerCode, sLNm As LoanNumber, cache.sLId AS LoanId, OrderNumber, Cast(InitialOrderDate as DATE) as OrderDate, VendorId , aoi.Status, 
                                                        SUBSTRING(aoi.AppraisalOrderXMLContent, CHARINDEX('<OrderedProduct', aoi.AppraisalOrderXMLContent)
                                                        , CHARINDEX('</OrderInfo>',aoi.AppraisalOrderXMLContent) - CHARINDEX('<OrderedProduct', aoi.AppraisalOrderXMLContent)) As OrderedProducts
                                                FROM Appraisal_Order_Info aoi WITH (nolock) JOIN Loan_File_Cache cache WITH (nolock) ON cache.sLId = aoi.sLId
                                                                                            JOIN Broker WITH (nolock) ON Broker.BrokerId = cache.sBrokerId
                                                WHERE --avc.ExportToStagePath = 0
                                                    InitialOrderDate >= @StartDate AND InitialOrderDate < @EndDate
                                                    AND aoi.Status != 'Cancelled'
                                                    AND BROKER.SuiteType = 1
                                                    AND cache.sLoanFileT = 0
                                                    AND VendorId='8CF8BA79-B8B0-4AD0-A6ED-D42C792F2032' -- RapidAMS
                                                ORDER BY VendorId, CustomerCode, LoanNumber, OrderNumber
												
END