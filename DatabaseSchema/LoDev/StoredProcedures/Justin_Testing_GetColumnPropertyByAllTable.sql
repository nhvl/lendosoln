-- =============================================
-- Author:		Justin Jia
-- ALTER date: June 14, 2012
-- Description:	Gets column property such as IsForeignKey, IsPrimaryKey by property name.
-- =============================================
CREATE PROCEDURE [dbo].[Justin_Testing_GetColumnPropertyByAllTable]
	-- Add the parameters for the stored procedure here
	@Property varchar(60)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT distinct isk.column_name, isk.table_name, sc.is_identity
	FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE as isk
	inner join sys.columns as sc on sc.name = isk.column_name and sc.object_id = object_id(isk.table_name)
	WHERE OBJECTPROPERTY(OBJECT_ID(isk.constraint_name), @Property) = 1
END
