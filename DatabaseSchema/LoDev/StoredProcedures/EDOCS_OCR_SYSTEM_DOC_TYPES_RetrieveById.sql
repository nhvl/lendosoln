-- =============================================
-- Author:		Michael Leinweaver
-- Create date: 9/8/2017
-- Description:	Retrieves a classification name by ID.
-- =============================================
ALTER PROCEDURE [dbo].[EDOCS_OCR_SYSTEM_DOC_TYPES_RetrieveById]
	@Id int
AS
BEGIN
	SELECT Name
	FROM EDOCS_OCR_SYSTEM_DOC_TYPES
	WHERE Id = @Id AND Enabled = 1
END
