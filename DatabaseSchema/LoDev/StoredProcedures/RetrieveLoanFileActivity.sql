-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE dbo.RetrieveLoanFileActivity
	@sLId UniqueIdentifier
AS
BEGIN
	SELECT XmlContent FROM LOAN_FILE_ACTIVITY
	WHERE sLId = @sLId
END
