ALTER PROCEDURE [dbo].[ListIntegrationModifiedFilesByBrokerId_PmlUser] 
	@AppCode uniqueidentifier,
	@BrokerId uniqueidentifier,
	@EmployeeId uniqueidentifier,
	@IsPmlManager bit
AS

declare @last90daysDate as datetime
set @last90daysDate = dateadd(day, datediff(day, 0 ,getdate())-90, 0) 

IF(@IsPmlManager = 1)
	BEGIN
		SELECT ifm.sLId, ifm.sLNm, ifm.LastModifiedD, ifm.sOldLNm, ifm.IsValid, ifm.aBNm, ifm.sSpAddr, ifm.sStatusT, lfc.sLRefNm,
			ifm.aBSsnEncrypted, ifm.sEncryptionKey, ifm.sEncryptionMigrationVersion
		FROM Integration_File_Modified ifm JOIN LOAN_FILE_CACHE_4 lfc ON ifm.sLId = lfc.sLId
		WHERE ifm.BrokerId = @BrokerId AND ifm.AppCode = @AppCode
		AND lastModifiedD > @last90daysDate
		AND 
			ifm.sEmployeeLoanRepId = @EmployeeId OR PmlExternalManagerEmployeeId = @EmployeeId
	END
ELSE
	BEGIN
		SELECT ifm.sLId, ifm.sLNm, ifm.LastModifiedD, ifm.sOldLNm, ifm.IsValid, ifm.aBNm, ifm.sSpAddr, ifm.sStatusT, lfc.sLRefNm,
			ifm.aBSsnEncrypted, ifm.sEncryptionKey, ifm.sEncryptionMigrationVersion
		FROM Integration_File_Modified ifm JOIN LOAN_FILE_CACHE_4 lfc ON ifm.sLId = lfc.sLId
		WHERE ifm.BrokerId = @BrokerId AND ifm.AppCode=@AppCode
		AND lastModifiedD > @last90daysDate
		AND 
			ifm.sEmployeeLoanRepId = @EmployeeId
	END

