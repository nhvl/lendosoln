CREATE PROCEDURE IsLoanNumberExisting 
	@BrokerId as uniqueidentifier,
	@LoanNumber as varchar(100)
AS
SELECT top 1 sLId from LOAN_FILE_Cache with(updlock,rowlock)
WHERE sLNm = @LoanNumber AND sBrokerID = @BrokerID
