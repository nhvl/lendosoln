CREATE PROCEDURE [dbo].[Billing_PTB_AllBilledMonths]
    @BrokerId UniqueIdentifier
AS
BEGIN
--As it turns out, the best way to take an integer month part and an integer year
--and turn them into a date is by converting the whole thing into a string
--in yyyy-mm-dd format, and then casting it to a date.
select cast(cast(Year as varchar) + '-' + 
            cast(Month as varchar) + 
            '-01' as smalldatetime) --We always want to return the first day of the month
            as BilledMonth
            , LoanCount
from
(
    select MONTH(BillingDate) as Month, YEAR(BillingDate) as Year, count(*) as LoanCount
    from [BILLING_PER_TRANSACTION]
    where BrokerId = @BrokerId
    GROUP BY MONTH(BillingDate), YEAR(BillingDate)
) MonthParts
order by BilledMonth desc

END
