ALTER PROCEDURE [dbo].[DOCUSIGN_BRANCH_CONFIGURATION_Retrieve]
	@BrokerId uniqueidentifier,
	@BranchId uniqueidentifier
AS
BEGIN
	SELECT * FROM dbo.DOCUSIGN_BRANCH_CONFIGURATION
	WHERE BrokerId = @BrokerId
		AND BranchId = @BranchId
END
