CREATE PROCEDURE [dbo].[SERVICE_FILE_Insert]
	@ComId Guid,
	@ServiceType varchar(21) = 'Credit',
	@FileType varchar(21),
	@Owner GUID,
	@ExternalFileId varchar(21),
	@DbFileKey Guid,
	@UserId Guid,
	@CrAccProxyId Guid,
	@HowDidItGetHere varchar(50),
	@BrandedProductName varchar(50)
AS
	INSERT INTO Service_File (
		ComId,
		ServiceType,
		FileType,
		Owner,
		ExternalFileId,
		DbFileKey,
		UserId,
		CrAccProxyId,
		HowDidItGetHere,
		BrandedProductName)
	VALUES (
		@ComId,
		@ServiceType,
		@FileType,
		@Owner,
		@ExternalFileId,
		@DbFileKey,
		@UserId,
		@CrAccProxyId,
		@HowDidItGetHere,
		@BrandedProductName)
