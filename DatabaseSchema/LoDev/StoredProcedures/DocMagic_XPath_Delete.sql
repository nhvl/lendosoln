CREATE PROCEDURE [dbo].[DocMagic_XPath_Delete]
	@XPath varchar(150)
AS
BEGIN
	delete from DOCMAGIC_XPATH_MAPPINGS
	where XPath = Coalesce(@XPath, XPath)
END
