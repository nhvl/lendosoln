-- =============================================
-- Author:		David Dao
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE dbo.LPE_PRICE_GROUP_PRODUCT_DeleteByLpTemplateId
    @lLpTemplateId UniqueIdentifier
AS
BEGIN
    DELETE FROM LPE_PRICE_GROUP_PRODUCT
    WHERE lLpTemplateId=@lLpTemplateId

END
