
CREATE PROCEDURE [dbo].[CP_UpdateLoginFailureCount]
	@Email varchar(80),
	@BrokerId uniqueidentifier
AS

UPDATE CP_CONSUMER
SET LoginFailureNumber = LoginFailureNumber + 1
WHERE Email = @Email AND BrokerId = @BrokerId
	
SELECT LoginFailureNumber
FROM CP_CONSUMER
WHERE	Email = @Email AND
		BrokerId = @BrokerId

