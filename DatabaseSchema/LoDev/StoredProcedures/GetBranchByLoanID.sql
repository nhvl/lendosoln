CREATE  PROCEDURE [dbo].[GetBranchByLoanID]
	@LoanID Guid
AS
	SELECT sBranchId , br.BranchNm AS sBranchNm 
		FROM LOAN_FILE_CACHE lc WITH(NOLOCK) JOIN BRANCH br WITH(NOLOCK) ON lc.sBranchId = br.BranchId
	WHERE sLId = @LoanID
