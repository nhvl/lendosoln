-- =============================================
-- Author:		David Dao
-- Create date: 11/04/2009
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[RetrieveDocMagicSavedAuthentication] 
	@UserId UniqueIdentifier
AS
BEGIN
	SELECT
		DocMagicUserName,
		DocMagicPassword,
		DocMagicCustomerId,
		EncryptedDocMagicPassword,
		EncryptionKeyId
	FROM Broker_User
	WHERE UserId = @UserId
END
