-- =============================================
-- Author:		Brian Beery
-- Create date: 09/20/2013
-- Description:	Disassociate a data retrieval framework partner from a lender
-- =============================================
CREATE PROCEDURE [dbo].[DATA_RETRIEVAL_PARTNER_BROKER_RemoveAssociationByBrokerId]
	@BrokerId UniqueIdentifier 

AS
BEGIN

DELETE FROM DATA_RETRIEVAL_PARTNER_BROKER
WHERE BrokerId = @BrokerId

END