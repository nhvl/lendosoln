-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE SetBypassTicketByUserId
	@ByPassTicket UniqueIdentifier,
	@UserId UniqueIdentifier
AS
BEGIN
	UPDATE All_User
       SET ByPassTicket = @ByPassTicket
	WHERE UserId = @UserId
END
