create PROCEDURE ListAllLicense
	@BrokerId Guid
AS
	SELECT * 
	FROM LICENSE 
	WHERE LicenseOwnerBrokerId=@BrokerId
	ORDER BY ExpirationDate DESC
