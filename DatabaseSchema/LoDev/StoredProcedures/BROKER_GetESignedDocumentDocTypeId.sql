-- =============================================
-- Author:		Geoff Feltman
-- Create date: 8/25/17
-- Description:	Retrieves the doc type id for e-signed documents for a broker.
-- =============================================
ALTER PROCEDURE [dbo].[BROKER_GetESignedDocumentDocTypeId]
	@BrokerId uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT ESignedDocumentDocTypeId
	FROM BROKER
	WHERE BrokerId = @BrokerId
END
