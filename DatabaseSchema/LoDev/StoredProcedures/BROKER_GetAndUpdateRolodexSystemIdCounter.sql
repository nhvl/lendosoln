-- =============================================
-- Author:		Michael Leinweaver
-- Create date: 2/22/2017
-- Description:	Obtains the rolodex system ID 
--				counter for the broker, 
--				incrementing the counter at the 
--				same time.
-- =============================================
ALTER PROCEDURE [dbo].[BROKER_GetAndUpdateRolodexSystemIdCounter]
	@BrokerId uniqueidentifier
AS
SET TRANSACTION ISOLATION LEVEL READ COMMITTED
BEGIN TRANSACTION
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DECLARE @LockResult int
	EXEC @LockResult = sp_getapplock @Resource='BROKER_GetAndUpdateRolodexSystemIdCounter', @LockMode = 'Exclusive'
	IF @LockResult < 0
	BEGIN
		RAISERROR('Unable to lock BROKER_GetAndUpdateRolodexSystemIdCounter resource.', 16, 1)
  		RETURN
 	END 

    DECLARE @Counter int
    
    SELECT @Counter = RolodexSystemIdCounter
    FROM BROKER
    WHERE BrokerId = @BrokerId
    
    UPDATE TOP (1) BROKER
    SET RolodexSystemIdCounter = @Counter + 1
    WHERE BrokerId = @BrokerId
    
    SELECT @Counter as "Counter"
COMMIT TRANSACTION
