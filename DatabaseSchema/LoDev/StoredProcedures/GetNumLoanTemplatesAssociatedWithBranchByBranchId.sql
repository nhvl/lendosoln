


-- =============================================
-- Author:		Peter Anargirou
-- Create date: 7/23/08
-- Description:	Retrieves the number of loan file templates that use a 
-- particular branch by default by branch ID.
-- =============================================
CREATE PROCEDURE [dbo].[GetNumLoanTemplatesAssociatedWithBranchByBranchId] 
	@BranchId uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
SELECT
		COUNT('') AS numLoanTemplatesAssociatedWithBranchByBranchId
	FROM 
		LOAN_FILE_F
		
	WHERE
		sBranchIdForNewFileFromTemplate = @BranchId
END



