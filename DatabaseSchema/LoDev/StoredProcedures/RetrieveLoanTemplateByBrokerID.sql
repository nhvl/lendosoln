
CREATE   PROCEDURE [dbo].[RetrieveLoanTemplateByBrokerID]
	@BrokerId Guid , @BranchId Guid , @EmployeeId Guid , @FilterByBranch Bit
AS
	-- 2/28/13 gf - Case 108148 - add support for new roles
	--
	-- 12/04/2012 mp - Case 106694 - add sLPurpose so that we can filter by purpose
	--
	-- 5/26/2005 kb - Per case 1884, we now use a publish level to resolve
	-- template access for creating new loans.
	--
	-- 0 = Individual (must be assigned)
	-- 1 = Branch (originated within your branch)
	-- 2 = Corporate (exposed to all)
	DECLARE @FilterId Guid
	IF( @FilterByBranch = 1 )
		SET @FilterId = @BranchId
	ELSE
		SET @FilterId = NULL
	SELECT
		sLId,
		sLNm,
		sEmployeeLenderAccExecId,
		sEmployeeLockDeskId,
		sEmployeeUnderwriterId,
		sEmployeeRealEstateAgentId,
		sEmployeeCallCenterAgentId,
		sEmployeeLoanOpenerId,
		sEmployeeLoanRepId,
		sEmployeeProcessorId,
		sEmployeeManagerId,
		sTemplatePublishLevelT,
		sLPurposeT,
		sIsLineOfCredit
	FROM
		view_loan_file_template l with(nolock)
	WHERE
		(
			l.sTemplatePublishLevelT = 0 AND 
			(
				l.sEmployeeLenderAccExecId = @EmployeeId
				OR l.sEmployeeLockDeskId = @EmployeeId
				OR l.sEmployeeUnderwriterId = @EmployeeId
				OR l.sEmployeeRealEstateAgentId = @EmployeeId
				OR l.sEmployeeCallCenterAgentId = @EmployeeId
				OR l.sEmployeeLoanOpenerId = @EmployeeId
				OR l.sEmployeeLoanRepId = @EmployeeId
				OR l.sEmployeeProcessorId = @EmployeeId
				OR l.sEmployeeManagerId = @EmployeeId
				OR l.sEmployeeShipperId = @EmployeeId
				OR l.sEmployeeFunderId = @EmployeeId
				OR l.sEmployeePostCloserId = @EmployeeId
				OR l.sEmployeeInsuringId = @EmployeeId
				OR l.sEmployeeCollateralAgentId = @EmployeeId
				OR l.sEmployeeDocDrawerId = @EmployeeId
				OR l.sEmployeeCreditAuditorId = @EmployeeId
				OR l.sEmployeeDisclosureDeskId = @EmployeeId
				OR l.sEmployeeJuniorProcessorId = @EmployeeId
				OR l.sEmployeeJuniorUnderwriterId = @EmployeeId
				OR l.sEmployeeLegalAuditorId = @EmployeeId
				OR l.sEmployeeLoanOfficerAssistantId = @EmployeeId
				OR l.sEmployeePurchaserId = @EmployeeId
				OR l.sEmployeeQCComplianceId = @EmployeeId
				OR l.sEmployeeSecondaryId = @EmployeeId
				OR l.sEmployeeServicingId = @EmployeeId
			)
			OR
			l.sTemplatePublishLevelT = 1 AND l.sBranchId = @BranchId
			OR
			l.sTemplatePublishLevelT = 2
		)
		AND
		l.sBranchId = COALESCE( @FilterId , l.sBranchId )
		AND
		l.sBrokerId = @BrokerId
	if( 0!=@@error)
	begin
		RAISERROR('Error in the select statement in RetrieveLoanTemplateByBrokerID sp', 16, 1);
		return -100;
	end
	return 0;

