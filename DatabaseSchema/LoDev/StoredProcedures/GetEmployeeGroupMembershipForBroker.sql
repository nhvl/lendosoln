CREATE PROCEDURE [dbo].[GetEmployeeGroupMembershipForBroker]
	@BrokerId uniqueidentifier
AS
BEGIN
	SELECT g.GroupId, g.BrokerId, g.GroupName, g.Description, ge.EmployeeId 
	FROM [GROUP] g JOIN [GROUP_x_EMPLOYEE] ge on g.groupid = ge.groupid 
	WHERE g.BrokerId = @BrokerId  and g.GroupType = 1

END

