CREATE PROCEDURE [dbo].[RegionSet_GetAllRegionSetsByBrokerId]
	@BrokerId uniqueidentifier
AS
BEGIN
	SELECT RegionSetId, BrokerId, Name, [Description]
	FROM REGION_SET
	WHERE BrokerId = @BrokerId
	
	SELECT RegionId, RegionSetId, BrokerId, Name, [Description]
	FROM REGION
	WHERE BrokerId = @BrokerId
	
	SELECT BrokerId, RegionSetId, RegionId, FipsCountyCode
	FROM REGION_X_FIPS
	WHERE BrokerId = @BrokerId
END