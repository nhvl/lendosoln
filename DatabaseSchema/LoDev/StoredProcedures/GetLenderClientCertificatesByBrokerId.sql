-- =============================================
-- Author:		Eric Mallare
-- Create date: 9/7/2016
-- Description:	Loads the lender certificates for the specified lender.
-- =============================================
ALTER PROCEDURE [dbo].[GetLenderClientCertificatesByBrokerId] 
	@BrokerId uniqueidentifier,
	@IsRevoked bit = null
AS
BEGIN
	DECLARE @TempTable TABLE
	(
		CertificateId uniqueidentifier,
		BrokerId uniqueidentifier,
		IsRevoked bit,
		Description varchar(100),
		NotesFileDbKey uniqueidentifier,
		Level int,
		UserType int,
		CreatedDate smalldatetime,
		RevokedDate smalldatetime,
		AssociatedUserId uniqueidentifier
	);
	
	INSERT INTO
		@TempTable
	SELECT
		CertificateId, BrokerId, IsRevoked, Description, 
		NotesFileDbKey, Level, UserType, CreatedDate, RevokedDate, AssociatedUserId
	FROM
		Broker_Client_Certificate
	WHERE
		BrokerId=@BrokerId AND
		IsRevoked=COALESCE(@IsRevoked, IsRevoked)
	
	
	SELECT *
	FROM @TempTable
	
	SELECT
		gbcc.CertificateId, gbcc.GroupId, g.GroupName, g.Description, g.GroupType
	FROM 
		Group_X_Broker_Client_Certificate gbcc JOIN
		[dbo].[Group] g ON gbcc.GroupId=g.GroupId JOIN
		@TempTable tt ON gbcc.CertificateId=tt.CertificateId 		
			
END
