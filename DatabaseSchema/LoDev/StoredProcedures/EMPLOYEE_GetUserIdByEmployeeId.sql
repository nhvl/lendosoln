/*
	Author: Scott Kibler
	Created Date: 9/9/2014
	Description: Return the UserID of the Employee at the specified broker.
*/
CREATE PROCEDURE dbo.EMPLOYEE_GetUserIdByEmployeeId
	@EmployeeID UniqueIdentifier,
	@BrokerID UniqueIdentifier
AS
BEGIN
	SELECT 
		EmployeeUserID as UserID
	FROM
		EMPLOYEE
	WHERE
		EmployeeID = @EmployeeID
	AND BranchID in
	(
		SELECT BranchID
		FROM Branch
		WHERE BrokerID = @BrokerID
	)
END