-- =============================================
-- Author:		Eduardo Michel
-- Create date: 
-- Description:	
-- =============================================
ALTER PROCEDURE [dbo].[STATUS_EVENT_AddMultiple] 
	-- Add the parameters for the stored procedure here
	@loanId uniqueidentifier, 
	@brokerId uniqueidentifier,
	@newEventsXml xml
AS
BEGIN
	INSERT INTO STATUS_EVENT (LoanId, StatusT, StatusD, Timestamp, IsExclude, BrokerId, UserName)
	
		select 
		@loanId as LoanId,
		StatusEvent.query('./StatusT').value('.', 'int') as StatusT,
		StatusEvent.query('./StatusD').value('.', 'smalldatetime') as StatusD,
		StatusEvent.query('./Timestamp').value('.', 'smalldatetime') as Timestamp,
		StatusEvent.query('./IsExclude').value('.', 'bit') as IsExclude,
		@brokerId as BrokerId,
		StatusEvent.query('./UserName').value('.', 'varchar(100)') as UserName
		FROM @newEventsXml.nodes('/root/statusEvent') as StatusEvents(StatusEvent);
END
