-- =============================================
-- Author:		Antonio Valencia
-- Create date: 8/24/11
-- Description:	Remove Doc Magic PDF scan Errors
-- =============================================
CREATE PROCEDURE EDOCS_RemoveDocMagicErrorStatus 
	-- Add the parameters for the stored procedure here
	@Id uniqueidentifier 

AS
BEGIN
	DELETE FROM EDOCS_DOCMAGIC_PDF_STATUS
	WHERE CurrentStatus = 100 AND @Id = Id
END
