CREATE PROCEDURE [dbo].[DYNAMIC_CLOSING_COST_FEE_HISTORY_Create]
	@BrokerId uniqueidentifier,
	@LoanId uniqueidentifier,
	@Description varchar(200),
	@ClosingCostFeeTypeId uniqueidentifier,
	@HudLine int,
	@FeeHistoryId int out
AS
BEGIN
	INSERT INTO DYNAMIC_CLOSING_COST_FEE_HISTORY (BrokerId, LoanId, [Description], ClosingCostFeeTypeId, HudLine)
	VALUES (@BrokerId, @LoanId, @Description, @ClosingCostFeeTypeId, @HudLine)
	
	SET @FeeHistoryId = SCOPE_IDENTITY()
END