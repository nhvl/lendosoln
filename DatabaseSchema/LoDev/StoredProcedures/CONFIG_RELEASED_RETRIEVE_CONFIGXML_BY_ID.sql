-- =============================================
-- Author:		Justin Kim
-- Create date: 5/16/18
-- Description:	Retrieve workflow configuration xml by configuration ID.
-- =============================================
ALTER PROCEDURE [dbo].[CONFIG_RELEASED_RETRIEVE_CONFIGXML_BY_ID]
	@BrokerId uniqueidentifier,
	@ConfigId bigint
AS
BEGIN
	SELECT ConfigurationXmlContent
	FROM CONFIG_RELEASED
	WHERE OrgId = @BrokerID and ConfigId = @ConfigId
END

