ALTER PROCEDURE [dbo].[VOA_ORDER_ASSET_VERIFIED_Create]
	@OrderId int,
    @AccountBalance decimal(18,2),
	@FinancialInstitutionName varchar(100),
	@AssetType int,
	@EncryptedAccountNumber varbinary(max),
    @EncryptionKeyId uniqueidentifier
AS
BEGIN
	INSERT INTO [dbo].[VOA_ORDER_ASSET_Verified]
		(OrderId, AccountBalance, FinancialInstitutionName, AssetType, EncryptedAccountNumber, EncryptionKeyId)
	VALUES
		(@OrderId, @AccountBalance, @FinancialInstitutionName, @AssetType, @EncryptedAccountNumber, @EncryptionKeyId)
END
