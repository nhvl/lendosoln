-- =============================================
-- Author:		David Dao
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE dbo.EDOCS_DOCUMENT_ORIGINAL_Exists
    @BrokerId UniqueIdentifier,
    @DocumentId UniqueIdentifier
AS
BEGIN

    SELECT MetadataJsonContent FROM EDOCS_DOCUMENT_ORIGINAL WHERE BrokerId = @BrokerID AND DocumentId = @DocumentId

END
