
CREATE PROCEDURE InsertPayment 
	@PaymentId Guid,
	@LicenseId Guid,
	@Amount money,
	@Description varchar(100),
	@PaidBy varchar(50),
	@AuthCode varchar(50)
AS
BEGIN
	INSERT INTO Payment (PaymentId, LicenseId, PaymentDate, Amount, Description, PaidBy, AuthCode)
              VALUES    (@PaymentId, @LicenseId, GETDATE(), @Amount, @Description, @PaidBy, @AuthCode)
END
