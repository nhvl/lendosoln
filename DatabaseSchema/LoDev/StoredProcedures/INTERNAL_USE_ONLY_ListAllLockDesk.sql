

CREATE PROCEDURE [dbo].[INTERNAL_USE_ONLY_ListAllLockDesk]
AS
	SELECT 
	v.RoleDesc
	, v.RoleId
	, v.BrokerId
	, v.EmployeeId
FROM 
	View_Employee_Role v
WHERE 
	v.RoleDesc = 'LockDesk' 
