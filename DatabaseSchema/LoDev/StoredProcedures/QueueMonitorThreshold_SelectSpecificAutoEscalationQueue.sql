-- =============================================
-- Author:		Michael Leinweaver
-- Create date: July 14, 2015
-- Description:	Lists the name, description, and size threshold values for the queue specified by @QueueName.
-- =============================================
ALTER PROCEDURE [dbo].[QueueMonitorThreshold_SelectSpecificAutoEscalationQueue]
	@ID int
AS
BEGIN
	SELECT *
	FROM QUEUE_MONITOR_THRESHOLD qmt
	WHERE qmt.ID = @ID
END
