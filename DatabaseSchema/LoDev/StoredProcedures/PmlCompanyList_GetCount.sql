-- =============================================
-- Author:		Michael Leinweaver
-- Create date: 2/16/2016
-- Description:	Retrieves the number of PML brokers for the
--				given broker.
-- =============================================
ALTER PROCEDURE [dbo].[PmlCompanyList_GetCount]
	@BrokerId uniqueidentifier,
	@CompanyNameFilter varchar(102),
	@CompanyIDFilter varchar(38),
	@CompanyRoleFilter varchar(3),
	@UnderwritingAuthorityFilter int = null,
	@CompanyIndexFilter int = -1,
	@BrokerStatusFilter int = -1,
	@MiniCorrStatusFilter int = -1,
	@CorrStatusFilter int = -1
AS
BEGIN
	SELECT COUNT(PmlBrokerId) AS TotalBrokers 
	FROM Pml_Broker p WITH(NOLOCK) 
	WHERE 
		p.BrokerId = @BrokerId AND 
		p.Name LIKE @CompanyNameFilter AND
		p.CompanyId LIKE @CompanyIDFilter AND
		p.UnderwritingAuthority = COALESCE(@UnderwritingAuthorityFilter, p.UnderwritingAuthority) AND
		(
			@CompanyRoleFilter = 'ANY' OR -- DON'T FILTER ON COMPANY ROLE
			(@CompanyRoleFilter = 'NA' AND (p.Roles IS NULL OR p.Roles = '')) OR -- NO ROLES SET
			p.Roles LIKE @CompanyRoleFilter -- FILTER ON ROLE
		) AND
		(@CompanyIndexFilter = -1 OR p.IndexNumber = @CompanyIndexFilter) AND
		(@BrokerStatusFilter = -1 OR p.BrokerRoleStatusT = @BrokerStatusFilter) AND
		(@MiniCorrStatusFilter = -1 OR p.MiniCorrRoleStatusT = @MiniCorrStatusFilter) AND
		(@CorrStatusFilter = -1 OR p.CorrRoleStatusT = @CorrStatusFilter)
END
