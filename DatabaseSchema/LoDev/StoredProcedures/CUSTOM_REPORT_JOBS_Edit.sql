-- =============================================
-- Author:		Michael Leinweaver
-- Create date: 6/8/2018
-- Description:	Edits an existing custom report job.
-- =============================================
ALTER PROCEDURE [dbo].[CUSTOM_REPORT_JOBS_Edit]
	@JobId int,
	@ResultFileDbKey uniqueidentifier = null,
	@UserLacksReportRunPermission bit
AS
BEGIN
	UPDATE CUSTOM_REPORT_JOBS 
	SET 
		ResultFileDbKey = @ResultFileDbKey,
		UserLacksReportRunPermission = @UserLacksReportRunPermission
	WHERE JobId = @JobId
END
