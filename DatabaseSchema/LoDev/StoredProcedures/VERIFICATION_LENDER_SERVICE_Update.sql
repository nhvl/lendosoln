-- =============================================
-- Author:		Eric Mallare
-- Create date: 5/8/2017
-- Description:	Updates the lender service.
-- =============================================
ALTER PROCEDURE [dbo].[VERIFICATION_LENDER_SERVICE_Update]
	@LenderServiceId int,
	@BrokerId uniqueidentifier,
	@ServiceT int,
	@VendorId int,
	@ResellerId int,
	@DisplayName varchar(200),
	@DisplayNameLckd bit,
	@IsEnabledForLqbUsers bit,
	@EnabledEmployeeGroupId uniqueidentifier = null,
	@IsEnabledForTpoUsers bit,
	@EnabledOcGroupId uniqueidentifier = null,
	@AllowPaymentByCreditCard bit,
	@VoeUseSpecificEmployerRecordSearch bit,
	@VoeEnforceUseSpecificEmployerRecordSearch bit,
	@VoaAllowImportingAssets bit,
	@VoaAllowRequestWithoutAssets bit,
	@VoaEnforceAllowRequestWithoutAssets bit,
    @VoeAllowImportingEmployment bit,
    @VoeAllowRequestWithoutEmployment bit,
    @VoeEnforceRequestsWithoutEmployment bit,
	@CanBeUsedForCredentialsInTpo bit
AS
BEGIN
	UPDATE
		VERIFICATION_LENDER_SERVICE
	SET
		ServiceT=@ServiceT, VendorId=@VendorId, ResellerId=@ResellerId, DisplayName=@DisplayName, DisplayNameLckd=@DisplayNameLckd, 
		IsEnabledForLqbUsers=@IsEnabledForLqbUsers, EnabledEmployeeGroupId=@EnabledEmployeeGroupId, IsEnabledForTpoUsers=@IsEnabledForTpoUsers,
		EnabledOcGroupId=@EnabledOcGroupId, AllowPaymentByCreditCard=@AllowPaymentByCreditCard,
		VoeUseSpecificEmployerRecordSearch=@VoeUseSpecificEmployerRecordSearch,
		VoeEnforceUseSpecificEmployerRecordSearch=@VoeEnforceUseSpecificEmployerRecordSearch,
		VoaAllowImportingAssets=@VoaAllowImportingAssets,
		VoaAllowRequestWithoutAssets=@VoaAllowRequestWithoutAssets,
		VoaEnforceAllowRequestWithoutAssets=@VoaEnforceAllowRequestWithoutAssets,
        VoeAllowImportingEmployment = @VoeAllowImportingEmployment, 
        VoeAllowRequestWithoutEmployment = @VoeAllowRequestWithoutEmployment, 
        VoeEnforceRequestsWithoutEmployment = @VoeEnforceRequestsWithoutEmployment,
		CanBeUsedForCredentialsInTpo=@CanBeUsedForCredentialsInTpo
	WHERE
		LenderServiceId=@LenderServiceId AND
		BrokerId=@BrokerId
END