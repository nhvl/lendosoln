-- =============================================
-- Author:		David Dao
-- Create date: 
-- Description:	
-- =============================================
ALTER PROCEDURE dbo.LPE_PRICE_GROUP_ListAll

AS
BEGIN
    SELECT LpePriceGroupId, BrokerId, ExternalPriceGroupEnabled, ActualPriceGroupId,
           LpePriceGroupName, LenderPaidOriginatorCompensationOptionT, 
           LockPolicyID, DisplayPmlFeeIn100Format, IsRoundUpLpeFee,
           RoundUpLpeFeeToInterval, ContentKey, IsAlwaysDisplayExactParRateOption
    FROM LPE_PRICE_GROUP

END
