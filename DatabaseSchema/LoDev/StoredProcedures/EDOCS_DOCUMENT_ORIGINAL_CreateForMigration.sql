-- =============================================
-- Author:		David Dao
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE dbo.EDOCS_DOCUMENT_ORIGINAL_CreateForMigration
    @BrokerId UniqueIdentifier,
    @DocumentId UniqueIdentifier,
	@SourceT int,
	@MetadataJsonContent varchar(max),
	@NumPages int,
	@PdfSizeInBytes int,
	@PngSizeInBytes int

AS
BEGIN
    INSERT INTO EDOCS_DOCUMENT_ORIGINAL(BrokerId, DocumentId, MetadataJsonContent, NumPages, PdfSizeInBytes, PngSizeInBytes, SourceT, OriginalRequestJson, CompletedDate)
    VALUES (@BrokerId, @DocumentId, @MetadataJsonContent, @NumPages, @PdfSizeInBytes, @PngSizeInBytes, @SourceT, '', GetDate())

END
