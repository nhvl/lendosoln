create procedure [dbo].UpdateDailyReport
	@LastRun as DateTime,
	@NextRun as DateTime,
	@ReportId as int
as
update DAILY_REPORTS 
set LastRun = @LastRun,
	NextRun = @NextRun
where ReportId = @ReportId;