-- =============================================
-- Author:		David Dao
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE dbo.ALL_USER_CLIENT_CERTIFICATE_Delete
    @UserId UniqueIdentifier,
    @CertificateId UniqueIdentifier
AS
BEGIN
    DELETE FROM ALL_USER_CLIENT_CERTIFICATE
    WHERE CertificateId = @CertificateId AND UserId = @UserId

END
