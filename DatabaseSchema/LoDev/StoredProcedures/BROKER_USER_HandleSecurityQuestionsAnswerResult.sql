-- =============================================
-- Author:      Timothy Jewell (and old code)
-- Create date: 8/2018
-- Description: Handles the result of the user's
-- attempt to answer the security questions. This
-- is a modified version of an existing procedure
-- MatchSecurityQuestionsAnswers, but we need to
-- move the check on the answers to the C# since
-- the value in the DB will now be encrypted.
-- =============================================
ALTER PROCEDURE [dbo].[BROKER_USER_HandleSecurityQuestionsAnswerResult]
	@UserId uniqueidentifier,
	@QuestionAnswersMatch bit,
	@AttemptsCount int OUTPUT
AS
BEGIN
	SET NOCOUNT ON;
	-- Check if an active broker user exists
	IF NOT EXISTS (SELECT a.UserId FROM BROKER_USER b INNER JOIN ALL_USER a on b.UserId = a.UserId WHERE b.UserId = @UserId AND a.IsActive = 1)
		RETURN -1

	IF @QuestionAnswersMatch = 1
		BEGIN
			UPDATE ALL_USER
			SET SecurityQuestionsAttemptsMade = 0,
				@AttemptsCount = 0
			WHERE UserId = @UserId
			RETURN 1
		END
	ELSE
		BEGIN
			UPDATE ALL_USER
			SET SecurityQuestionsAttemptsMade = SecurityQuestionsAttemptsMade + 1,
				@AttemptsCount = SecurityQuestionsAttemptsMade + 1
			WHERE UserId = @UserId
			RETURN -2
		END
END
