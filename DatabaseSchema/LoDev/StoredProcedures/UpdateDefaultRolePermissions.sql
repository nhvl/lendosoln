ALTER PROCEDURE [dbo].[UpdateDefaultRolePermissions]
	@RoleID uniqueidentifier,
	@BrokerID uniqueidentifier,
	@DefaultPermissions char(255)
	
AS
	UPDATE ROLE_DEFAULT_PERMISSIONS
	       SET DefaultPermissions = @DefaultPermissions
	 WHERE RoleId = @RoleID AND BrokerId = @BrokerID
	
	if(0!=@@error)
	begin
		RAISERROR('Error in the update statement in UpdateDefaultRolePermissions sp', 16, 1);
		return -100;
	end
	return 0;
