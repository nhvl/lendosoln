-- =============================================
-- Author:		Paolo Arrastia
-- Create date: 4/11/2013
-- Description:	
-- =============================================
CREATE PROCEDURE dbo.APPRAISAL_VENDOR_EnableForBroker
	-- Add the parameters for the stored procedure here
	@VendorId uniqueidentifier,
	@BrokerId uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    IF NOT EXISTS (SELECT * FROM APPRAISAL_VENDOR_BROKER_ENABLED WHERE VendorId=@VendorId AND BrokerId=@BrokerId)
		INSERT INTO APPRAISAL_VENDOR_BROKER_ENABLED(VendorId, BrokerId)
		VALUES (@VendorId, @BrokerId)
END
