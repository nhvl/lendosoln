ALTER PROCEDURE [dbo].[ListRolodexByBrokerId]
	@BrokerID uniqueidentifier , 
	@NameFilter varchar(64) = NULL , 
	@TypeFilter int = NULL,
	@IsApprovedFilter bit = null,
	@OnlySettlementServiceProviders bit = null
AS
	SELECT a.AgentID, 
	       AgentType,
	       AgentTypeOtherDescription,
	       AgentNm,
	       AgentTitle,
	       AgentPhone,
	       AgentAltPhone,
	       AgentFax,
	       AgentComNm,
	       AgentEmail,
	       AgentAddr,
	       AgentCity,
	       AgentState,
	       AgentZip,
	       AgentCounty,
	       AgentDepartmentName,
	       AgentPager,
	       AgentLicenseNumber,
	       CompanyLicenseNumber,
	       PhoneOfCompany,
	       FaxOfCompany,
	       IsNotifyWhenLoanStatusChange,
	       AgentWebsiteUrl,
	       AgentN,
	       CommissionPointOfLoanAmount,
	       CommissionPointOfGrossProfit,
	       CommissionMinBase,
	       LicenseXmlContent,
	       CompanyId,
	       AgentBranchName,
	       AgentPayToBankName,
	       AgentPayToBankCityState,
	       AgentPayToABANumber,
	       AgentPayToAccountName,
	       AgentPayToAccountNumber,
	       AgentFurtherCreditToAccountName,
	       AgentFurtherCreditToAccountNumber,
	       AgentIsApproved,
	       AgentIsAffiliated,
	       OverrideLicenses,
		   CASE
		   	   WHEN LEN(AgentBranchName) > 0 THEN AgentComNm + ' - ' + AgentBranchName
			   ELSE AgentComNm
		   END AS AgentComNmBranchNm, SystemId, IsSettlementServiceProvider
	FROM
		Agent a
	WHERE
		a.BrokerID = @BrokerID
		AND
		(
			AgentNm LIKE COALESCE('%' +  @NameFilter + '%' ,  AgentNm )
			OR
			AgentComNm LIKE COALESCE( '%' +  @NameFilter + '%' ,  AgentComNm )
			
		)
		AND
		AgentType = COALESCE( @TypeFilter , AgentType )
		AND
		AgentIsApproved = COALESCE(@IsApprovedFilter, AgentIsApproved)
		AND
		(
			COALESCE(@OnlySettlementServiceProviders, 0) = 0
			OR
			IsSettlementServiceProvider = 1
		)
	ORDER BY
		AgentType, AgentNm
	if( 0!=@@error)
	begin
		RAISERROR('Error in the select statement in ListRolodexByBrokerID sp', 16, 1);
		return -100;
	end
	
	return 0;