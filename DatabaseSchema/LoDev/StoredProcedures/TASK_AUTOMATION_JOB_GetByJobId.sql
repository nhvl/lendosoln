ALTER PROCEDURE [dbo].[TASK_AUTOMATION_JOB_GetByJobId]
	@JobId int
AS
BEGIN
	SELECT
		j.JobId,
		j.ProcessingStatus,
		j.NextRunDate,
		j.PublicJobId,
		j.DateCreated,
		j.ExpirationDate,
		j.LastRunStartDate,
		j.MaxExceptionCount,
		j.ExceptionCount,
		j.DateCompleted,
		j.CorrelationId,
		j.UserId,
		t.BrokerId,	-- 11/2/18 - je - For backwards compatibility we get LoanId from TASK_AUTOMATION_JOB table.
		t.LoanId,	-- We should be able to change this in the future if necessary.
		t.TaskId,
		t.TaskAutomationJobType
	FROM TASK_AUTOMATION_JOB t
	JOIN BACKGROUND_JOBS j ON t.JobId = j.JobId
	WHERE j.JobId = @JobId
END