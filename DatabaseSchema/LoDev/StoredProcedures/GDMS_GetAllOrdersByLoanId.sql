ALTER PROCEDURE [dbo].[GDMS_GetAllOrdersByLoanId]
	@BrokerId uniqueidentifier,
	@LoanId uniqueidentifier
AS
BEGIN
	SELECT a.*, FileNumber, UserId, EmployeeId, VendorId, UploadedFileIdXmlContent
	FROM GDMS_ORDER g
	JOIN APPRAISAL_ORDER a ON g.AppraisalOrderId = a.AppraisalOrderId
	JOIN BROKER b ON g.brokerid = b.brokerid
	WHERE b.status = 1
	AND g.BrokerId = @BrokerId
	AND sLId = @LoanId

	SELECT
		FileNumber, GdmsFileId, EdocId, [FileName]
	FROM
		GDMS_ORDER_EDOCS
	WHERE
		BrokerId=@BrokerId AND
		LoanId=@LoanId
END