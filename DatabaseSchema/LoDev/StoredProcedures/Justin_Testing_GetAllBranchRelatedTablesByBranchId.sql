-- =============================================
-- Author:		Justin Jia
-- Create date: 8/14/14
-- Description:	Get all table entries that only related to broker but not exported explicitly in the broker export tool.
-- =============================================
CREATE PROCEDURE [dbo].[Justin_Testing_GetAllBranchRelatedTablesByBranchId] 
	-- Add the parameters for the stored procedure here
	@BranchId uniqueidentifier
AS
BEGIN
	SELECT 'DOCUMENT_VENDOR_BRANCH_CREDENTIALS' as TableName,BranchId,VendorId FROM DOCUMENT_VENDOR_BRANCH_CREDENTIALS WHERE BranchId = @BranchId
	SELECT 'GROUP_x_BRANCH' as TableName,GroupId,BranchId FROM GROUP_x_BRANCH WHERE BranchId = @BranchId
END
