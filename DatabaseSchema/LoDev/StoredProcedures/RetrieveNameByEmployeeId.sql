ALTER PROCEDURE [dbo].[RetrieveNameByEmployeeId]
	@EmployeeID UniqueIdentifier
AS
BEGIN
	SELECT UserFirstNm, UserLastNm FROM Employee WITH (NOLOCK)  WHERE EmployeeID = @EmployeeID
END

