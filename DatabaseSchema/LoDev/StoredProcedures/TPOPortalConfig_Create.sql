-- =============================================
-- Author:		Scott Kibler
-- Create date: 2/4/2014
-- Description:	opm 150384
-- =============================================
CREATE PROCEDURE [dbo].TPOPortalConfig_Create
	-- Add the parameters for the stored procedure here
	@ID uniqueidentifier,
	@BrokerID uniqueidentifier,
	@Name varchar(36),
	@Url varchar(255)
AS
BEGIN
	INSERT INTO LENDER_TPO_LANDINGPAGECONFIG (ID,BrokerID, Name, Url)
	VALUES (@ID, @BrokerID, @Name, @Url)
END

