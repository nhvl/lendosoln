-- =============================================
-- Author:		Thien Nguyen
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE UpdateAuditHistoryMigratedByLoanId
	@sLId Guid,
    @sIsAuditHistoryMigrated bit
AS
BEGIN
	UPDATE Loan_File_A
	SET
		sIsAuditHistoryMigrated = @sIsAuditHistoryMigrated
    WHERE sLId = @sLId    
END
