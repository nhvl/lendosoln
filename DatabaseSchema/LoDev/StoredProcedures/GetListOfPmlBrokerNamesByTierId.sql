-- =============================================
-- Author:		Jhairo Erazo
-- Create date: 11/21/2014
-- Description:	Get a list of PML Brokers with the assigned tier
-- =============================================
CREATE PROCEDURE [dbo].[GetListOfPmlBrokerNamesByTierId]
	@BrokerId UniqueIdentifier,
	@PmlCompanyTierId int
AS
BEGIN
	SELECT Name
	FROM Pml_Broker
	WHERE BrokerId = @BrokerId
	AND PmlCompanyTierId = @PmlCompanyTierId
	ORDER BY Name
END