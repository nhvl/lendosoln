-- =============================================
-- Author:		Justin Kim
-- Create date: 9/19/17
-- Description:	Retrieve predefined adjustments belonging to a broker
-- =============================================
ALTER PROCEDURE [dbo].[Predefined_Adjustment_Retrieve_All_For_Broker]
	@BrokerId uniqueidentifier
AS
BEGIN
	SET NOCOUNT ON;

	select [Id], [Type], [Description], [From], [To], [POC], [IncludeIn1003Details], [DFLP]
	from Predefined_Adjustment
	where BrokerId = @BrokerId
	order by [Description]
END
