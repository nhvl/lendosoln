-- =============================================
-- Author:		Eric Mallare
-- Create date: 1/23/2018
-- Description:	Inserts a DocuSign recipient record into the DB.
-- =============================================
ALTER PROCEDURE [dbo].[DOCUSIGN_RECIPIENTS_Create]
	@EnvelopeReferenceId int,
	@SigningOrder int,
	@Type int,
	@Name varchar(200),
	@Email varchar(200),
	@AccessCode varchar(50) = null,
	@ProvidedPhoneNumber varchar(20) = null,
	@MfaOption int,
	@CurrentStatus int = null,
	@SentDate datetime = null,
	@DeliveredDate datetime = null,
	@SignedDate datetime = null,
	@DeclinedDate datetime = null,
	@DeclinedReason varchar(500) = null,
	@AutoRespondedReason varchar(500) = null,
	@RecipientIdGuid varchar(36) = null,
	@CustomRecipientId int,
    @ClientUserId uniqueidentifier = null,
	@Id int OUTPUT
AS
BEGIN
	INSERT INTO DOCUSIGN_RECIPIENTS
		(EnvelopeReferenceId,
		 SigningOrder,
		 Type,
		 Name,
		 Email,
		 AccessCode,
		 ProvidedPhoneNumber,
		 MfaOption,
		 CurrentStatus,
		 SentDate,
		 DeliveredDate,
		 SignedDate,
		 DeclinedDate,
		 DeclinedReason,
		 AutoRespondedReason,
		 RecipientIdGuid,
		 CustomRecipientId,
         ClientUserId)
	VALUES
		(@EnvelopeReferenceId,
		 @SigningOrder,
		 @Type,
		 @Name,
		 @Email,
		 @AccessCode,
		 @ProvidedPhoneNumber,
		 @MfaOption,
		 @CurrentStatus,
		 @SentDate,
		 @DeliveredDate,
		 @SignedDate,
		 @DeclinedDate,
		 @DeclinedReason,
		 @AutoRespondedReason,
		 @RecipientIdGuid,
		 @CustomRecipientId,
         @ClientUserId)

	SET @Id=SCOPE_IDENTITY()
END