CREATE PROCEDURE [dbo].[EmployeeSetRoles]
	@Roles varchar(1500),
             @EmployeeID Guid,
	@BrokerID Guid
AS
declare @tranPoint sysname
set @tranPoint = object_name(@@procId) + CAST(@@nestlevel as varchar(10))
--print @tranPoint
begin transaction
save transaction @tranPoint
DECLARE @RoleID varchar(40)
DECLARE @strLen int
DECLARE @CommaIndex int
DECLARE @LastIndex int
SET @strLen = LEN(@Roles)
SET @CommaIndex = 1
SET @LastIndex = 1
-- Remove all previous roles.
DELETE FROM Role_Assignment
WHERE EmployeeID = @EmployeeID
 
IF @Roles IS NOT NULL 
BEGIN
-- Set new roles
WHILE @CommaIndex <> 0 
BEGIN
	SELECT @CommaIndex = CHARINDEX(',', @Roles, @CommaIndex)
	IF @CommaIndex <> 0 
	BEGIN	
		SELECT @RoleID = SUBSTRING(@Roles, @LastIndex, @CommaIndex - @LastIndex)
		SELECT @LastIndex = @CommaIndex + 1
		SELECT @CommaIndex = @CommaIndex + 1
	END
	ELSE
	BEGIN
		SELECT @RoleID = SUBSTRING(@Roles, @LastIndex, @strLen - @LastIndex + 1)
	END		
	
	INSERT INTO Role_Assignment(EmployeeID, RoleID) VALUES(@EmployeeID, @RoleID)
	if( 0!=@@error)
	begin
		RAISERROR('Error in inserting into Role_Assignment table in EmployeeSetRole sp', 16, 1);
		goto ErrorHandler;
	end
END
END
commit transaction
return 0;
ErrorHandler:
	rollback transaction @tranPoint
	rollback transaction
	return -100;
