CREATE PROCEDURE [dbo].UnlinkLoans
(
	@sLId as uniqueidentifier
)
as  
	declare @sLId2 uniqueidentifier 
	declare @sLId1 uniqueidentifier 
	
	SELECT @sLId1 = sLId1, @sLId2 = sLId2 FROM LOAN_LINK 
	where @sLid = slid1 or @slid = sLId2
	
	delete from loan_link 
	where sLId1 = @sLId or sLId2 = @sLId
	
	select @slid1 as sLId1, @slid2 as sLId2
