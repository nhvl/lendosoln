-- =============================================
-- Author:		Paolo Arrastia
-- Create date: 30 Jul 2012
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[FindRateMonitors]
	-- Add the parameters for the stored procedure here
	@monitorid bigint = null,
	@isenabled bit = null,
	@loanid uniqueidentifier = null,
	@bpgroup varchar(100) = null,
	@createdbefore datetime = null,
	@createdafter datetime = null
	
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT Id, sLId, Rate, Point, EmailAddresses, CcAddresses, BPDesc, CreatedDate, DisabledDate, DisabledReason, IsEnabled, LastRunDate, LastRunBatchId
	FROM RATE_MONITOR
	WHERE
		(Id LIKE @monitorid
			OR
			@monitorId IS NULL)
		AND
		(sLId LIKE @loanid
			OR
			@loanid IS NULL)
		AND
		(IsEnabled LIKE @isenabled
			OR
			@isenabled IS NULL)
		AND
		(BPDesc LIKE '%'+@bpgroup+'%'
			OR
			@bpgroup IS NULL)
		AND
			(@createdbefore IS NULL
			OR
			DATEDIFF(day, @createdbefore, CreatedDate) <= 0)
		AND
			(@createdafter IS NULL
			OR
			DATEDIFF(day, @createdafter, CreatedDate) >= 0)
	ORDER BY Id;
END
