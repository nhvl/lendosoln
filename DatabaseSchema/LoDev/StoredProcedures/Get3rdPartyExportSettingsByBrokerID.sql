
CREATE  PROCEDURE [dbo].[Get3rdPartyExportSettingsByBrokerID]
	@BrokerId uniqueidentifier
AS
	SELECT
		IsShowBuyPriceHiddenAdjIn3rdParty,
		IsExportPricingInfoTo3rdParty
	FROM
		Broker
	WHERE
		BrokerId = @BrokerId
	if( 0!=@@error)
	begin
		RAISERROR('Error in the select statement in Get3rdPartyExportSettingsByBrokerID sp', 16, 1);
		return -100;
	end
	return 0;