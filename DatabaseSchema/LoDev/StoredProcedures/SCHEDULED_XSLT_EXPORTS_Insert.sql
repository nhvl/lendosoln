ALTER PROCEDURE [dbo].[SCHEDULED_XSLT_EXPORTS_Insert]
	@BrokerId uniqueidentifier,
    @ExportName varchar(250),
    @MapName varchar(250) = '',
    @UserId uniqueidentifier,
    @ReportId uniqueidentifier,
	@Notes varchar(max)='',
	@Protocol varchar(10) = '',
    @HostName varchar(500) = '',
    @Login varchar(40) = '',
    @Password varchar(90) = '',
    @Port int = 21,
    @SSHFingerprint varchar(80) = '',
    @DestPath varchar(500) = '',
    @Use_DotNet bit = 1,
    @SecurityMode varchar(11) = 'None',
    @SSLHostFingerprint varchar(80) = '',
    @UsePassive bit = 0,
    @AdditionalPostUrl varchar(200) = '',
    @Comments varchar(500) = '',
	@To varchar(500) = '',
	@From varchar(500) = '',
	@FileName varchar(500) = '',
	@Subject varchar(500) = '',
	@Message varchar(500) = ''
AS
BEGIN transaction
	declare @Id int
INSERT INTO dbo.SCHEDULED_XSLT_EXPORT_SETTINGS (
	[BrokerId],
    [ExportName],
    [MapName],
    [UserId],
    [ReportId],
	[Notes]
	)
OUTPUT Inserted.ExportId
VALUES (
	@BrokerId,
    @ExportName,
    @MapName,
    @UserId,
    @ReportId,
	@Notes
	)

IF @@error!= 0 GOTO HANDLE_ERROR
SET @Id=SCOPE_IDENTITY();

INSERT INTO [dbo].[SCHEDULED_XSLT_EXPORT_SETTINGS_FTP] (
	[ExportId],
	[Protocol],
	[HostName],
	[Login],
	[Password],
	[Port],
	[SSHFingerprint],
	[DestPath],
	[Use_DotNet],
	[SecurityMode],
	[SSLHostFingerprint],
	[UsePassive],
	[AdditionalPostUrl],
	[Comments]
)	
VALUES (
	@Id,
	@Protocol,
	@HostName,
	@Login,
	@Password,
	@Port,
	@SSHFingerprint,
	@DestPath,
	@Use_DotNet,
	@SecurityMode,
	@SSLHostFingerprint,
	@UsePassive,
	@AdditionalPostUrl,
	@Comments
);

IF @@error!= 0 GOTO HANDLE_ERROR

INSERT INTO dbo.SCHEDULED_XSLT_EXPORT_SETTINGS_EMAIL (
	[ExportId],
	[To],
	[From],
	[FileName],
	[Subject],
	[Message]
)
VALUES (
	@Id,
	@To,
	@From,
	@FileName,
	@Subject,
	@Message
)

IF @@error!= 0 GOTO HANDLE_ERROR
	
COMMIT TRANSACTION
   	
RETURN;
   
HANDLE_ERROR:
    ROLLBACK TRANSACTION
    RAISERROR('Error in [SCHEDULED_XSLT_EXPORTS_Insert] sp', 16, 1);;
    RETURN;


