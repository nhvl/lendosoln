CREATE PROCEDURE ListUsersHasDiscNotifByNotifIdSrc
	@NotifIdSrc uniqueidentifier
AS
/*
select au.UserId, au.LoginNm
from ( ( ( broker brok join branch br on brok.brokerid = br.brokerid ) 
	join employee em on br.branchid = em.branchid ) 
	join broker_user bu on bu.employeeid = em.employeeid )
	join all_user au on au.userid = bu.userid
where brok.brokerid = @brokerid and em.isactive = 1 and au.isactive = 1
*/
select notif.NotifId, notif.NotifReceiverUserId, notif.NotifReceiverLoginNm
from discussion_notification notifSrc WITH (NOLOCK) join discussion_notification notif WITH (NOLOCK) on notifSrc.disclogid = notif.disclogid
where notifSrc.notifId = @notifIdSrc
