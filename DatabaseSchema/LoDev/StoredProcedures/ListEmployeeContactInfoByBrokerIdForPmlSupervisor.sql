﻿ALTER PROCEDURE [dbo].[ListEmployeeContactInfoByBrokerIdForPmlSupervisor]
	@BrokerId uniqueidentifier,
	@Type Varchar( 1 ) = NULL,
	@SupervisorEmployeeId uniqueidentifier,
	@PmlBrokerId uniqueidentifier = NULL
AS
	SELECT
		e.EmployeeID AS EmployeeID,
		l.EncryptionKeyId,
		a.LoginNm AS LoginName,
		a.Type AS UserType,
		e.BranchId AS BranchID,
		e.UserFirstNm AS FirstName,
		e.UserMiddleNm AS MiddleName,
		e.UserLastNm AS LastName,
		e.UserSuffix AS Suffix,
		e.UserNmOnLoanDocs AS NmOnLoanDocs,
		e.UserNmOnLoanDocsLckd AS NmOnLoanDocsLckd,
		e.Addr AS Address,
		e.City AS City,
		e.State AS State,
		e.Zip AS Zipcode,
		e.Phone AS Phone,
		e.Fax AS Fax,
		e.CellPhone AS CellPhone,
		e.Pager AS Pager,
		e.Email AS Email,
		e.IsActive AS IsActive,
		e.EmployeeLicenseNumber AS EmployeeLicenseNumber,
		e.CommissionPointOfLoanAmount,
		e.CommissionPointOfGrossProfit,
		e.CommissionMinBase,
		e.LicenseXmlContent,
		e.NmLsIdentifier,
		a.IsActive AS HasLogin,
		e.NewOnlineLoanEventNotifOptionT AS NewOnlineLoanEventNotifOptionT,
		e.TaskRelatedEmailOptionT AS TaskRelatedEmailOptionT,
		l.IsAccountOwner AS IsAccountOwner,
		l.UserId AS UserID,
		l.PmlSiteID AS PmlSiteID,
		l.LpePriceGroupID AS LpePriceGroupID,
		l.PmlExternalManagerEmployeeID,
		l.ManagerEmployeeID AS ManagerEmployeeID,
		l.LenderAccExecEmployeeID AS LenderAccExecEmployeeID,
		l.LockDeskEmployeeID AS LockDeskEmployeeID,
		l.UnderwriterEmployeeID AS UnderwriterEmployeeID,
		l.JuniorUnderwriterEmployeeID AS JuniorUnderwriterEmployeeID,
		l.ProcessorEmployeeID AS ProcessorEmployeeID,
		l.JuniorProcessorEmployeeID AS JuniorProcessorEmployeeID,
		l.LoanOfficerAssistantEmployeeID AS LoanOfficerAssistantEmployeeID,
		l.MiniCorrManagerEmployeeId, 
		l.MiniCorrProcessorEmployeeId, 
		l.MiniCorrJuniorProcessorEmployeeId, 
		l.MiniCorrLenderAccExecEmployeeId, 
		l.MiniCorrExternalPostCloserEmployeeId, 
		l.MiniCorrUnderwriterEmployeeId, 
		l.MiniCorrJuniorUnderwriterEmployeeId, 
		l.MiniCorrCreditAuditorEmployeeId, 
		l.MiniCorrLegalAuditorEmployeeId, 
		l.MiniCorrLockDeskEmployeeId, 
		l.MiniCorrPurchaserEmployeeId, 
		l.MiniCorrSecondaryEmployeeId, 
		l.CorrManagerEmployeeId, 
		l.CorrProcessorEmployeeId, 
		l.CorrJuniorProcessorEmployeeId, 
		l.CorrLenderAccExecEmployeeId, 
		l.CorrExternalPostCloserEmployeeId, 
		l.CorrUnderwriterEmployeeId, 
		l.CorrJuniorUnderwriterEmployeeId, 
		l.CorrCreditAuditorEmployeeId, 
		l.CorrLegalAuditorEmployeeId, 
		l.CorrLockDeskEmployeeId, 
		l.CorrPurchaserEmployeeId, 
		l.CorrSecondaryEmployeeId,
		l.DOAutoLoginNm AS DOAutoLoginNm,
		l.DOAutoPassword AS DOAutoPassword,
		l.EncryptedDOAutoPassword,
		l.DUAutoLoginNm AS DUAutoLoginNm,
		l.DUAutoPassword AS DUAutoPassword,
		l.EncryptedDUAutoPassword,
		l.LpeRsExpirationByPassPermission AS LpeRsExpirationByPassPermission,
		l.MustLogInFromTheseIpAddresses, 
		l.PmlBrokerId,
		l.PipelineCustomReportSettingsXmlContent ,
		e.IsPmlManager,
		a.PasswordHash AS PasswordHash,
		a.PasswordExpirationD,
		a.PasswordExpirationPeriod,
		a.PasswordHistory,
		e.NotesByEmployer,
		a.IsSharable,
		dbo.GetEmployeeNameFromId(l.ManagerEmployeeID) AS ManagerFullName,
		dbo.GetEmployeeNameFromId(l.LenderAccExecEmployeeID) AS LenderAccExecFullName,
		dbo.GetEmployeeNameFromId(l.LockDeskEmployeeID) AS LockDeskFullName,
		dbo.GetEmployeeNameFromId(l.UnderwriterEmployeeID) AS UnderwriterFullName,
		dbo.GetEmployeeNameFromId(l.JuniorUnderwriterEmployeeID) AS JuniorUnderwriterFullName,
		dbo.GetEmployeeNameFromId(l.ProcessorEmployeeID) AS ProcessorFullName,
		dbo.GetEmployeeNameFromId(l.JuniorProcessorEmployeeID) AS JuniorProcessorFullName,
		dbo.GetEmployeeNameFromId(l.PmlExternalManagerEmployeeID) AS PmlExternalManagerFullName,
		dbo.GetEmployeeNameFromId(l.BrokerProcessorEmployeeId) as BrokerProcessorFullName,
		dbo.GetEmployeeNameFromId(l.MiniCorrManagerEmployeeId) AS MiniCorrManagerFullName, 
		dbo.GetEmployeeNameFromId(l.MiniCorrProcessorEmployeeId) AS MiniCorrProcessorFullName, 
		dbo.GetEmployeeNameFromId(l.MiniCorrJuniorProcessorEmployeeId) AS MiniCorrJuniorProcessorFullName, 
		dbo.GetEmployeeNameFromId(l.MiniCorrLenderAccExecEmployeeId) AS MiniCorrLenderAccExecFullName, 
		dbo.GetEmployeeNameFromId(l.MiniCorrExternalPostCloserEmployeeId) AS MiniCorrExternalPostCloserFullName, 
		dbo.GetEmployeeNameFromId(l.MiniCorrUnderwriterEmployeeId) AS MiniCorrUnderwriterFullName, 
		dbo.GetEmployeeNameFromId(l.MiniCorrJuniorUnderwriterEmployeeId) AS MiniCorrJuniorUnderwriterFullName, 
		dbo.GetEmployeeNameFromId(l.MiniCorrCreditAuditorEmployeeId) AS MiniCorrCreditAuditorFullName, 
		dbo.GetEmployeeNameFromId(l.MiniCorrLegalAuditorEmployeeId) AS MiniCorrLegalAuditorFullName, 
		dbo.GetEmployeeNameFromId(l.MiniCorrLockDeskEmployeeId) AS MiniCorrLockDeskFullName, 
		dbo.GetEmployeeNameFromId(l.MiniCorrPurchaserEmployeeId) AS MiniCorrPurchaserFullName, 
		dbo.GetEmployeeNameFromId(l.MiniCorrSecondaryEmployeeId) AS MiniCorrSecondaryFullName, 
		dbo.GetEmployeeNameFromId(l.CorrManagerEmployeeId) AS CorrManagerFullName, 
		dbo.GetEmployeeNameFromId(l.CorrProcessorEmployeeId) AS CorrProcessorFullName, 
		dbo.GetEmployeeNameFromId(l.CorrJuniorProcessorEmployeeId) AS CorrJuniorProcessorFullName, 
		dbo.GetEmployeeNameFromId(l.CorrLenderAccExecEmployeeId) AS CorrLenderAccExecFullName, 
		dbo.GetEmployeeNameFromId(l.CorrExternalPostCloserEmployeeId) AS CorrExternalPostCloserFullName, 
		dbo.GetEmployeeNameFromId(l.CorrUnderwriterEmployeeId) AS CorrUnderwriterFullName, 
		dbo.GetEmployeeNameFromId(l.CorrJuniorUnderwriterEmployeeId) AS CorrJuniorUnderwriterFullName, 
		dbo.GetEmployeeNameFromId(l.CorrCreditAuditorEmployeeId) AS CorrCreditAuditorFullName, 
		dbo.GetEmployeeNameFromId(l.CorrLegalAuditorEmployeeId) AS CorrLegalAuditorFullName, 
		dbo.GetEmployeeNameFromId(l.CorrLockDeskEmployeeId) AS CorrLockDeskFullName, 
		dbo.GetEmployeeNameFromId(l.CorrPurchaserEmployeeId) AS CorrPurchaserFullName, 
		dbo.GetEmployeeNameFromId(l.CorrSecondaryEmployeeId) AS CorrSecondaryFullName, 
		l.IsQuickPricerEnabled,
		l.AnonymousQuickPricerUserId,
		l.IsPricingMultipleAppsSupported,
		a.MclUserId,
		l.PmlLevelAccess,
		l.BrokerProcessorEmployeeId,
		l.SignatureFileDBKey,
		e.IsUserTPO,
		e.OriginatorCompensationAuditXml,
		e.OriginatorCompensationPercent,
		e.OriginatorCompensationBaseT,
		e.OriginatorCompensationMinAmount,
		e.OriginatorCompensationMaxAmount,
		e.OriginatorCompensationFixedAmount,
		e.OriginatorCompensationNotes,
		e.OriginatorCompensationLastModifiedDate,
		e.OriginatorCompensationSetLevelT,
		COALESCE(p.Name, '') as PmlBrokerName,
		COALESCE(p.CompanyId, '') AS PmlCompanyId,
		a.RecentLoginD,
		l.StartD,
		l.IsNewPmlUIEnabled,
		'' AS GlobalDmsCompanyId,
		'' AS GlobalDmsUsername,
		'' AS GlobalDmsPassword,
		e.EmployeeIDInCompany,
		l.AssignedPrintGroups,	
		l.ShowQMStatusInPml2,	
		l.Lender_TPO_LandingPageConfigId,
		e.CustomPricingPolicyField1Fixed,
		e.CustomPricingPolicyField2Fixed,
		e.CustomPricingPolicyField3Fixed,
		e.CustomPricingPolicyField4Fixed,
		e.CustomPricingPolicyField5Fixed,
		e.CustomPricingPolicyFieldSource,
		l.PMLNavigationPermissions,
		e.ChumsID,
		e.CanBeMemberOfMultipleTeams,
		l.SupportEmail,
		l.SupportEmailVerified,
		
		CAST(CASE 
                 WHEN (select count(*) from View_Employee_Role where employeeid = e.EmployeeId and roleid = '6DFC9B0E-11E3-48C1-B410-A1B0B5363B69') = 1
                     THEN 'true' 
                 ELSE 'false' 
             END AS bit) as IsLoanOfficer,
		CAST(CASE 
                 WHEN (select count(*) from View_Employee_Role where employeeid = e.EmployeeId and roleid = 'B3785253-D9E8-4A3F-B0EC-D02D6FCF8538') = 1
                     THEN 'true' 
                 ELSE 'false'
             END AS bit) as IsBrokerProcessor,
        CAST(CASE 
                 WHEN (select count(*) from View_Employee_Role where employeeid = e.EmployeeId and roleid = '274981E4-8C18-4204-B97B-2F537922CCD9') = 1
                     THEN 'true' 
                 ELSE 'false'
             END AS bit) as IsExternalSecondary,
        CAST(CASE 
                 WHEN (select count(*) from View_Employee_Role where employeeid = e.EmployeeId and roleid = '89811C63-6A28-429A-BB45-3152449D854E') = 1
                     THEN 'true' 
                 ELSE 'false'
             END AS bit) as IsExternalPostCloser,
        e.EmployeeStartD,
        e.EmployeeTerminationD,
        a.IsActiveDirectoryUser,
		e.UseOriginatingCompanyBranchId,
		l.UseOriginatingCompanyLpePriceGroupId,
		l.CorrespondentLandingPageID, 
		l.MiniCorrespondentLandingPageID, 
		l.UseOriginatingCompanyCorrPriceGroupId, 
		l.CorrespondentPriceGroupId, 
		l.UseOriginatingCompanyMiniCorrPriceGroupId, 
		l.MiniCorrespondentPriceGroupId,
		e.UseOriginatingCompanyCorrBranchId, 
		e.CorrespondentBranchId, 
		e.UseOriginatingCompanyMiniCorrBranchId, 
		e.MiniCorrespondentBranchId,
		l.PortalMode,
		e.FavoritePageIdsJSON,
		e.CanContactSupport,
		a.IsUpdatedPassword,
		a.PasswordSalt,
		l.OptsToUseNewLoanEditorUI,
		e.PopulateBrokerRelationshipsT,
		e.PopulateMiniCorrRelationshipsT,
		e.PopulateCorrRelationshipsT,
		e.PopulatePMLPermissionsT,
		l.UserTheme,
		e.IsCellphoneForMultiFactorOnly,
		l.EnableAuthCodeViaSms,
		l.LastModifiedDate,
		l.[Permissions],
		l.EnabledIpRestriction,
		l.EnabledMultiFactorAuthentication,
		l.EnabledClientDigitalCertificateInstall,
		e.OriginatorCompensationIsOnlyPaidForFirstLienOfCombo,
		  a.EncryptedMfaOtpSeed,
		a.AUEncryptionKeyId
	FROM
			Employee AS e JOIN Branch AS r ON r.BranchId = e.BranchId AND r.BrokerId = @BrokerId 
                                                   JOIN Broker_User AS l ON l.EmployeeId = e.EmployeeId 
                                                   JOIN All_User AS a ON a.UserId = l.UserId AND a.Type = COALESCE( @Type , a.Type )
												   LEFT JOIN PML_BROKER as p ON p.PmlBrokerId = l.PmlBrokerId
WHERE PmlExternalManagerEmployeeId = @SupervisorEmployeeId	and (@PmlBrokerId is null OR p.PmlBrokerId = @PmlBrokerId)

ORDER BY
		e.UserLastNm , e.UserFirstNm
