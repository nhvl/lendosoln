
-- =============================================
-- Author:		Antonio Valencia
-- Create date: 06 26 2008
-- Description:	Updates individual role permission for all brokers. Use this when we there's a new permission added and you want to set the default values. 
-- =============================================
ALTER PROCEDURE [dbo].[UpdateIndividualRolePermission] 
	
	@RoleId uniqueidentifier,
	@PermissionValue char(1),
	@PermissionLocation tinyint,     
	@DefaultPermissionSet char(255)  -- for brokers who did not set a permission for this specific role. 
	
AS
BEGIN
	BEGIN TRY 
		BEGIN TRAN
			UPDATE ROLE_DEFAULT_PERMISSIONS SET DefaultPermissions = STUFF( DefaultPermissions, @PermissionLocation + 1, 1, @PermissionValue) WHERE  RoleId = @RoleId ;  

			INSERT INTO ROLE_DEFAULT_PERMISSIONS(RoleId, BrokerId, DefaultPermissions) 
				SELECT @RoleId, BrokerId, @DefaultPermissionSet FROM Broker WHERE BrokerId NOT IN ( SELECT BrokerId FROM ROLE_DEFAULT_PERMISSIONS WHERE RoleId = @roleId ) ;
		COMMIT TRAN 
	END TRY 
	BEGIN CATCH 
		ROLLBACK TRAN
		DECLARE @ErrorMsg NVARCHAR(4000); 
		SELECT @ErrorMsg = ERROR_MESSAGE(); 
		RAISERROR( @ErrorMsg, 16, 1 );
	END CATCH 
END

