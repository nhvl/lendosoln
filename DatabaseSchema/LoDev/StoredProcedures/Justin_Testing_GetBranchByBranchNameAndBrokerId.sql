-- =============================================
-- Author:		Justin Jia
-- ALTER date: June 14, 2012
-- Description:	Gets all column property of one table by the table name
-- =============================================
CREATE PROCEDURE [dbo].[Justin_Testing_GetBranchByBranchNameAndBrokerId]
	-- Add the parameters for the stored procedure here
	@BranchNm varchar(100),
	@BrokerId uniqueidentifier
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM BRANCH 
	WHERE BranchNm = @BranchNm
	and BrokerId = @BrokerId

END
