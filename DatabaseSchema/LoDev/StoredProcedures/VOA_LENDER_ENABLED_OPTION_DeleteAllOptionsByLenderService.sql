ALTER PROCEDURE [dbo].[VOA_LENDER_ENABLED_OPTION_DeleteAllOptionsByLenderService]
	@BrokerId uniqueidentifier,
	@LenderServiceId int
AS
BEGIN
	DELETE FROM VOA_LENDER_ENABLED_OPTION
	WHERE BrokerId = @BrokerId
		AND LenderServiceId = @LenderServiceId
END
GO
