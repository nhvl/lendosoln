CREATE PROCEDURE [dbo].[Billing_TitleErnst]
	@StartDate datetime,
	@EndDate datetime
AS
BEGIN
-- dd 2018-11-02 - For use in automate billing executable.
SELECT tfo.OrderNumber, tfo.OrderedDate, b.BrokerNm As LenderName, b.CustomerCode, cache.slnm as LoanNumber, cache.slid AS LoanId, tfo.OrderedBy, tfo.LenderServiceName
                                                FROM TITLE_FRAMEWORK_ORDER tfo with(nolock) JOIN Broker b with(nolock)on b.brokerid = tfo.BrokerId
                                                                                            JOIN LOAN_FILE_CACHE cache on cache.sLId = tfo.LoanId
                                                WHERE OrderedDate >= @StartDate AND OrderedDate < @EndDate
                                                  AND tfo.IsTestOrder = 0
                                                  AND b.SuiteType = 1 AND cache.sLoanFileT = 0
                                                Order BY LenderServiceName, LenderName, OrderedDate
END