-- =============================================
-- Author:		Jhairo Erazo
-- Create date: 14 Jan 2014
-- Description:	Unified Find Mortgage Pools SP, searches based on all criteria
-- =============================================
ALTER PROCEDURE [dbo].[FindMortgagePools] 
	-- Add the parameters for the stored procedure here
	@PoolStatus bit = NULL,
	@AgencyT int = NULL,
	@AmortT int = NULL,
	@poolNumber varchar(9) = NULL,
	@InternalId varchar(36) = NULL,
	@CommitmentNum varchar(50) = NULL,
	@allowPartialPoolNum bit = 0,
	@allowPartialPoolId bit = 0,
	@allowPartialCommitmentNumber bit = 0,
	@BrokerId UniqueIdentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	IF(@poolNumber IS NOT NULL AND @allowPartialPoolNum=1)
		SET @poolNumber = @poolNumber+'%';
		
	IF(@InternalId IS NOT NULL AND @allowPartialPoolId=1)
		SET @InternalId = '%'+@InternalId+'%';
		
	IF(@CommitmentNum IS NOT NULL AND @allowPartialCommitmentNumber=1)
		SET @CommitmentNum = '%'+@CommitmentNum+'%';

    -- Insert statements for procedure here
	SELECT PoolId
	FROM MORTGAGE_POOL
	WHERE
		-- BY FILTERS
		(	
			((ClosedD IS NULL) AND (@PoolStatus = 1))
			OR
			((ClosedD IS NOT NULL) AND (@PoolStatus = 0))
			OR
			@PoolStatus IS NULL
		)
		AND
		(
			AgencyT = @AgencyT
			OR
			@AgencyT IS NULL
		)
		AND
		(
			AmortizationT = @AmortT
			OR
			@AmortT IS NULL
		)
		AND --BY POOL NUMBER
		(
			BasePoolNumber LIKE @poolNumber
			OR
			PoolPrefix+'-'+BasePoolNumber LIKE @poolNumber
			OR
			BasePoolNumber+IssueTypeCode+PoolTypeCode LIKE @poolNumber
			OR
			@poolNumber IS NULL
		)
		AND -- BY INTERNAL ID
		(
			InternalId LIKE @InternalId
			OR
			@InternalId IS NULL
		)
		AND -- BY COMMITMENT NUMBER
		(
			CommitmentNum LIKE @CommitmentNum
			OR
			@CommitmentNum IS NULL
		)
		AND
			BrokerId = @BrokerId
		AND
		IsEnabled = 1
	--Make new pools (empty pool number) appear first
	ORDER BY (CASE WHEN BasePoolNumber LIKE '' THEN 0 ELSE 1 END), PoolId ASC;
END
