﻿-- =============================================
-- Author:		Eduardo Michel
-- Create date: 
-- Description:	
-- =============================================
ALTER PROCEDURE [dbo].[STATUS_EVENT_ToggleExclude] 
	-- Add the parameters for the stored procedure here
	@loanId uniqueidentifier, 
	@brokerId uniqueidentifier,
	@dirtyIdXml xml
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Update sE
	SET sE.IsExclude = ~sE.IsExclude
	FROM (@dirtyIdXml.nodes('root/id') as T(Item) JOIN 
		STATUS_EVENT sE ON T.Item.value('.', 'int') = sE.Id)
	WHERE se.LoanId = @loanId AND se.BrokerId = @brokerId;
		
	
END
