CREATE PROCEDURE IsPrintGroupNameExisting
	@BrokerId Guid,
	@GroupName varchar(50),
	@GroupId Guid
AS
	SELECT TOP 1 GroupName FROM Print_Group WHERE GroupName = @GroupName AND OwnerId = @BrokerId AND GroupId <> @GroupId
