


CREATE PROCEDURE [dbo].[ListDiscLogsByLoanId]
	@LoanID uniqueidentifier ,	@StatusFilter Int = NULL , @IsValid Bit = NULL
AS
	SELECT l.*
	FROM Discussion_Log AS l
	WHERE
		l.DiscRefObjId = @LoanID
		AND
		l.DiscStatus = COALESCE( @StatusFilter , l.DiscStatus )
		AND
		l.IsValid = COALESCE( @IsValid , l.IsValid )
		AND
		l.DiscIsRefObjValid = 1
	ORDER BY DiscCreatedDate
	IF( 0!=@@error)
	BEGIN
		RAISERROR('Error in the selection statement in ListDiscLogsByLoanId sp', 16, 1);
		RETURN -100;
	END
	RETURN 0;



