-- =============================================
-- Author:		Justin Lara
-- Create date: 2/26/2018
-- Description:	Retrieves the loan number and
--				subservicer loan name by loan ID.
-- =============================================
ALTER PROCEDURE [dbo].[GetLoanIdentifiersForSubservicerTransmission] 
	@BrokerId uniqueidentifier,
	@LoanId uniqueidentifier
AS
BEGIN
	SELECT lc.sLNm, lc3.sSubservicerLoanNm
	FROM LOAN_FILE_CACHE lc
		JOIN LOAN_FILE_CACHE_3 lc3 ON lc.sLId = lc3.sLId
	WHERE lc.sLId = @LoanID
		AND lc.sBrokerId = @BrokerId
END
