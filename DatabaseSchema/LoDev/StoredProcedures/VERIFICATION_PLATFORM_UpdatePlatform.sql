-- =============================================
-- Author:		Eric Mallare
-- Create date: 4/27/2017
-- Description:	Updates a platform and its associated transmission info.
-- =============================================
CREATE PROCEDURE [dbo].[VERIFICATION_PLATFORM_UpdatePlatform]
	@PlatformId int,
	@PlatformName varchar(150),
	@TransmissionModelT int,
	@PayloadFormatT int,
	@TransmissionId int,
	@UsesAccountId bit,
	@RequiresAccountId bit,
	@TargetUrl varchar(200),
	@TransmissionAuthenticationT int,
	@EncryptionKeyId uniqueidentifier,
	@TransmissionAssociationT int = null,
	@RequestCertLocation varchar(200) = null,
	@ResponseCertId uniqueidentifier = null,
	@RequestCredentialName varchar(50) = null,
	@RequestCredentialPassword varbinary(max) = null,
	@ResponseCredentialName varchar(50) = null,
	@ResponseCredentialPassword varbinary(max) = null
AS
BEGIN TRANSACTION
	
	UPDATE
		VERIFICATION_TRANSMISSION
	SET
		TargetUrl=@TargetUrl, TransmissionAuthenticationT=@TransmissionAuthenticationT,
		EncryptionKeyId = @EncryptionKeyId, RequestCertLocation=@RequestCertLocation,
		ResponseCertId=@ResponseCertId, RequestCredentialName=@RequestCredentialName, ResponseCredentialName=@ResponseCredentialName, 
		RequestCredentialPassword=@RequestCredentialPassword, ResponseCredentialPassword=@ResponseCredentialPassword
	WHERE
		TransmissionId=@TransmissionId

	IF( @@error != 0 )
		GOTO HANDLE_ERROR
	
	UPDATE
		VERIFICATION_PLATFORM
	SET
		PlatformName=@PlatformName, TransmissionModelT=@TransmissionModelT, PayloadFormatT=@PayloadFormatT,
		UsesAccountId=@UsesAccountId, RequiresAccountId=@RequiresAccountId
	WHERE 
		PlatformId=@PlatformId
		
	IF( @@error != 0 )
		GOTO HANDLE_ERROR

COMMIT TRANSACTION
RETURN;
	HANDLE_ERROR:
		ROLLBACK TRANSACTION
		RAISERROR('Error in VERIFICATION_PLATFORM_UpdatePlatform sp', 16, 1);
		RETURN;
