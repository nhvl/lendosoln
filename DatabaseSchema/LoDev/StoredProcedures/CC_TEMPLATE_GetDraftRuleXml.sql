-- =============================================
-- Author:		Antonio Valencia
-- Create date: 8/1/2012
-- Description:	Gets the id and draft rule xml
-- =============================================
CREATE PROCEDURE [dbo].[CC_TEMPLATE_GetDraftRuleXml]
	@BrokerId uniqueidentifier
AS
BEGIN

	-- there should only be one draft 
	SELECT TOP 1 Id, RuleXml FROM CC_TEMPLATE_SYSTEM where BrokerId = @BrokerId AND ReleasedD IS NULL
		
END
