ALTER PROCEDURE [dbo].[Billing_PTB_BillLoan]
	@sLId UniqueIdentifier,
	@BrokerId UniqueIdentifier,
	@BillingDate smalldatetime,
	@BillingType int,
	@DocumentVendor int
AS
BEGIN
	insert [BILLING_PER_TRANSACTION] 
		(sLId,  BrokerId,  BillingDate,  BillingType, DocumentVendor)
	values						
		(@sLId, @BrokerId, @BillingDate, @BillingType, @DocumentVendor)
END