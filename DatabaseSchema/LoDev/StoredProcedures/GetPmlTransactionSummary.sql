

CREATE PROCEDURE [dbo].[GetPmlTransactionSummary] 
	@StartDate smalldatetime,
	@EndDate smalldatetime
	
AS
	SELECT T1.CustomerCode, Step1, Step2, Step3, Step4, Submit FROM
(
    SELECT x.CustomerCode, COALESCE(Step,0) AS Step1 FROM Broker x
    LEFT OUTER JOIN
        (SELECT COUNT(sPml1stVisitCreditStepD) AS Step, CustomerCode
            FROM loan_file_cache cache with(nolock), loan_file_f f with(nolock), broker b with(nolock)
            WHERE cache.slid = f.slid AND cache.sBrokerId = b.BrokerId 
                AND ( sPml1stVisitCreditStepD >= @StartDate AND sPml1stVisitCreditStepD < @EndDate )
            GROUP BY CustomerCode
        ) AS y
    ON x.CustomerCode=y.CustomerCode
    WHERE x.CustomerCode LIKE 'PML0%'
) T1,
(
    SELECT x.CustomerCode, COALESCE(Step,0) AS Step2 FROM Broker x
    LEFT OUTER JOIN
        (SELECT COUNT(sPml1stVisitApplicantStepD) AS Step, CustomerCode
            FROM loan_file_cache cache with(nolock), loan_file_f f with(nolock), broker b with(nolock)
            WHERE cache.slid = f.slid AND cache.sBrokerId = b.BrokerId 
                AND ( sPml1stVisitApplicantStepD >= @StartDate AND sPml1stVisitApplicantStepD < @EndDate )
            GROUP BY CustomerCode
        ) AS y
    ON x.CustomerCode=y.CustomerCode
    WHERE x.CustomerCode LIKE 'PML0%'
) T2,
(
    SELECT x.CustomerCode, COALESCE(Step,0) AS Step3 FROM Broker x
    LEFT OUTER JOIN
        (SELECT COUNT(sPml1stVisitPropLoanStepD) AS Step, CustomerCode
            FROM loan_file_cache cache with(nolock), loan_file_f f with(nolock), broker b with(nolock)
            WHERE cache.slid = f.slid AND cache.sBrokerId = b.BrokerId 
                AND ( sPml1stVisitPropLoanStepD >= @StartDate AND sPml1stVisitPropLoanStepD < @EndDate )
            GROUP BY CustomerCode
        ) AS y
    ON x.CustomerCode=y.CustomerCode
    WHERE x.CustomerCode LIKE 'PML0%'
) T3,
(
    SELECT x.CustomerCode, COALESCE(Step, 0) AS Step4 FROM Broker x
    LEFT OUTER JOIN
        (SELECT COUNT(sPml1stVisitResultStepD) AS Step, CustomerCode
            FROM loan_file_cache cache with(nolock), loan_file_f f with(nolock), broker b with(nolock)
            WHERE cache.slid = f.slid AND cache.sBrokerId = b.BrokerId 
                AND ( sPml1stVisitResultStepD >= @StartDate AND sPml1stVisitResultStepD < @EndDate )
            GROUP BY CustomerCode
        ) AS y
    ON x.CustomerCode=y.CustomerCode
    WHERE x.CustomerCode LIKE 'PML0%'
) T4,
(
    SELECT x.CustomerCode, COALESCE(Submit,0) AS Submit FROM Broker x
    LEFT OUTER JOIN
        (SELECT COUNT(sPml1stSubmitD) AS Submit, CustomerCode
            FROM loan_file_cache cache with(nolock), loan_file_f f with(nolock), broker b with(nolock)
            WHERE cache.slid = f.slid AND cache.sBrokerId = b.BrokerId 
                AND ( sPml1stSubmitD >= @StartDate AND sPml1stSubmitD < @EndDate )
            GROUP BY CustomerCode
        ) AS y
    ON x.CustomerCode=y.CustomerCode
    WHERE x.CustomerCode LIKE 'PML0%'
) T5
WHERE T1.CustomerCode=T2.CustomerCode AND T1.CustomerCode=T3.CustomerCode AND T1.CustomerCode=T4.CustomerCode AND T1.CustomerCode=T5.CustomerCode
ORDER BY T1.CustomerCode
