-- =============================================
-- Author:		Scott Kibler
-- Create date: Friday 4/6/2012
-- Description:	Get LPQ Login info (username and password) by UserId.
-- =============================================
ALTER PROCEDURE  [dbo].[LPQ_RetrieveSavedAuthenticationByUserId]
	-- Add the parameters for the stored procedure here
	@UserId uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT
		LPQUsername,
		LPQPassword,
		EncryptedLPQPassword,
		EncryptionKeyId
	from Broker_User
	where UserId = @UserId
END
