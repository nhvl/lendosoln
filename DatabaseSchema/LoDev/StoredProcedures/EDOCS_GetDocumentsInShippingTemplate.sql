-- =============================================
-- Author:		Antonio Valencia
-- Create date: 1/7/10
-- Description:	Gets the EDocuments in a shipping template
-- =============================================
ALTER PROCEDURE [dbo].[EDOCS_GetDocumentsInShippingTemplate] 
	@ShippingTemplateId int,
	@BrokerId uniqueidentifier,
	@LoanId uniqueidentifier
AS
BEGIN
	SELECT  aappid, aBFirstNm, aCFirstNm, aBLastNm, aCLastNm
	FROM APPLICATION_A WHERE sLId = @LoanId
	
	SELECT  
			docType.FolderId, 
			ImageStatus, 
			TimeToRequeueForImageGeneration , 
			HasInternalAnnotation, 
			doc.DocumentId, 
			doc.sLId, 
			doc.aAppId, 
			doc.IsValid,  
			DocTypeName,   
			docType.IsValid as DocTypeIsValid,
			doc.BrokerId, 
			doc.DocTypeId, 
			FaxNumber, 
			FileDBKey_CurrentRevision, 
			FileDBKey_OriginalDocument,
			doc.[Description] as PublicDescription, 
			InternalDescription, 
			Comments, 
			NumPages, 
			IsUploadedByPmlUser, 
			[Version], 
			LastModifiedDate, 
			CreatedDate, 
			IsEditable,
			PdfHasOwnerPassword, 
			TimeImageCreationEnqueued, 
			PdfHasUnsupportedFeatures, 
			[Status], 
			StatusDescription,
			PdfPageInfo,
			HideFromPmlUsers,
			MetadataJsonContent,
			OriginalMetadataJsonContent,
			doc.CreatedByUserId,
			IsESigned,
			doc.HasESignTags,
			doc.MetadataProtobufContent,
			doc.OriginalMetadataProtobufContent,
            gen.FileType as GenericFileType
	FROM	EDOCS_DOCUMENT_TYPE_X_SHIPPING_TEMPLATE as tem, 
			EDOCS_DOCUMENT_TYPE as docType, 
			EDOCS_DOCUMENT as doc
            LEFT JOIN EDOCS_GENERIC as gen ON doc.DocumentId = gen.DocumentId, 
			EDOCS_SHIPPING_TEMPLATE as shi
			
	WHERE 
			doc.IsValid = 1											-- only retrieve valid docs 
		AND shi.ShippingTemplateId = @ShippingTemplateId			-- join with shipping template for brokerid
		AND ( shi.BrokerId = @BrokerId OR shi.BrokerId IS NULL )	-- make sure the broker has access to the shipping template should probably raise error if not met.. but i think empty list is fine 
		AND tem.ShippingTemplateId = @ShippingTemplateId			-- select the correct shipping template
		AND tem.DocTypeId = docType.DocTypeId						-- join up with the doc  table for the docs
		AND doc.DocTypeId = tem.DocTypeId							-- join up with the doc type table for the names 
		AND doc.BrokerId = @BrokerId								-- ensure the documents for the given broker are only returned
		AND doc.sLId = @LoanId										-- and documents for the givne loan are only returned.
END
