CREATE PROCEDURE dbo.EDOCS_ShippingTemplateEditInternalCheckName
	@ShippingTemplateName varchar(50),
	@List varchar(1000),
	@ShippingTemplateId int
as

select 1
	from EDOCS_SHIPPING_TEMPLATE 
	where ShippingTemplateName = @ShippingTemplateName
		and ShippingTemplateId <> @ShippingTemplateId
		and BrokerId is null