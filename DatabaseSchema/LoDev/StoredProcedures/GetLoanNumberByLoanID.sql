CREATE  PROCEDURE GetLoanNumberByLoanID
	@LoanID Guid
AS
	SELECT sLNm , sPrimBorrowerFullNm 
	FROM LOAN_FILE_CACHE with(nolock)
	WHERE
		sLId = @LoanID
