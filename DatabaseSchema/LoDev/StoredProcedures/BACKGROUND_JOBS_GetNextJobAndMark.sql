-- =============================================
-- Author:		Eric Mallare
-- Create date: 12/1/2017
-- Description:	Gets the first job that is due and marks it as 'Processing'. Also sets the LastRunStartDate for the job to the current date.
-- =============================================
ALTER PROCEDURE [dbo].[BACKGROUND_JOBS_GetNextJobAndMark]
	@CurrentDate datetime,
	@JobTypeMask int = null,
	@Filter varchar(10) = null
AS
BEGIN
	Update
		BACKGROUND_JOBS WITH(ROWLOCK, READPAST, UPDLOCK)
	SET
		ProcessingStatus=2,
		LastRunStartDate=@CurrentDate
	OUTPUT
		inserted.JobId, inserted.JobType
	WHERE
		JobId in (SELECT TOP(1)
						JobId
					FROM
						BACKGROUND_JOBS WITH(ROWLOCK, READPAST, UPDLOCK)
					WHERE
						(@JobTypeMask IS NULL OR POWER(2, JobType) & @JobTypeMask <> 0) AND
						((@Filter IS NULL AND Filter IS NULL) OR Filter = @Filter) AND
						(
							(ProcessingStatus=1 /*ProcessingStatus.Incomplete*/ AND @CurrentDate>NextRunDate) OR
							(
								ProcessingStatus=2 /*ProcessingStatus.Processing*/ AND 
								CanRetryIfInLimbo=1 AND
								ExpectedRunTimeInSeconds IS NOT NULL AND
								DateCreated IS NOT NULL AND
								DATEADD(second, ExpectedRunTimeInSeconds, LastRunStartDate) < @CurrentDate AND
								ExpirationDate > @CurrentDate
							)
						)
					ORDER BY
						NextRunDate)
END