CREATE PROCEDURE [dbo].[Billing_DPPValueInsured]
	@StartDate datetime,
	@EndDate datetime
AS
BEGIN
-- dd 2018-11-02 - For use in automate billing executable.

SELECT b.BrokerNm As LenderName, b.CustomerCode As CustomerCode, lfc.sLId As LoanId, lfc.sLNm As LoanNumber
                                                FROM LOAN_FILE_D lfd with(nolock)
                                                JOIN LOAN_FILE_CACHE lfc with(nolock)ON lfd.sLId = lfc.sLId
                                                JOIN Broker B with(nolock)ON lfc.sBrokerId = B.BrokerId

                                                WHERE
                                                (
                                                    (B.CustomerCode = 'PML0034' AND lfd.sCustomPMLField2 ='0;1')
                                                    OR
                                                    (B.CustomerCode = 'PML0251' AND lfd.sCustomPMLField1 ='0;1')
                                                )
                                                AND B.SuiteType = 1
                                                AND lfc.sLoanFileT = 0

                                                Order BY B.BrokerNm, lfc.sLNm

END												
