-- =============================================
-- Author:		Eric Mallare
-- Create date: 10/16/2018
-- Description:	Retrieves a VOA job entry.
-- =============================================
ALTER PROCEDURE [dbo].[VOA_JOBS_Retrieve]
	@JobId int = NULL,
	@PublicJobId uniqueidentifier = NULL
AS
BEGIN
	SELECT
		j.JobId,
		j.ProcessingStatus,
		j.NextRunDate,
		j.PublicJobId,
		j.DateCreated,
		j.ExpirationDate,
		j.LastRunStartDate,
		j.MaxExceptionCount,
		j.ExceptionCount,
		j.DateCompleted,
		j.BrokerId,
		j.LoanId,
		j.CorrelationId,
		j.UserId,
		v.ResponseFileDbGuidId,
		v.JobRequestDataFileDbGuidId,
		v.RetryIntervalInSeconds
	FROM
		VOA_JOBS v JOIN
		BACKGROUND_JOBS j ON v.JobId=j.JobId
	WHERE
		j.JobId=@JobId OR
		j.PublicJobId=@PublicJobId
END