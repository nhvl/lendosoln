-- =============================================
-- Author:		Geoff Feltman
-- Create date: 2018-08-23
-- Description:	Adds an association between the primary borrower
-- of a legacy application and a ULAD application.
-- =============================================
CREATE PROCEDURE dbo.ULAD_APPLICATION_X_CONSUMER_AddForPrimaryBorrowerOnLegacyApp
@LoanId UniqueIdentifier,
@LegacyAppId UniqueIdentifier,
@UladAppId UniqueIdentifier,
@UladAppConsumerId UniqueIdentifier OUT,
@ConsumerId UniqueIdentifier OUT
AS
BEGIN
	SET @UladAppConsumerId = NEWID();
	SELECT @ConsumerId = aBConsumerId FROM APPLICATION_A WHERE aAppId = @LegacyAppId;
	DECLARE @IsPrimary Bit;
	SET @IsPrimary = 0;

	EXEC ULAD_APPLICATION_X_CONSUMER_Create @UladAppConsumerId, @LoanId, @UladAppId, @ConsumerId, @LegacyAppId, @IsPrimary;
END
GO
