CREATE PROCEDURE UpdateUsageEvents
	@BrokerID Guid,
	@EndDate DateTime
AS
SET NOCOUNT ON
DECLARE @Tranpoint SYSNAME
SET @Tranpoint = OBJECT_NAME(@@PROCID) + CAST(@@NESTLEVEL AS VARCHAR(10))
BEGIN TRANSACTION 
SAVE TRANSACTION @Tranpoint
-- Pair off all matching subscribe/unsubscribe features before @EndDate.
-- Pair off user_on/user_off event for individual users.
UPDATE Usage_Event
SET AccountedDate = GETDATE()
FROM Usage_Event e JOIN (SELECT FeatureID, MAX(EventDate) AS EventDate
                         FROM Usage_Event
                         WHERE BrokerID = @BrokerID AND EventType = 'u' AND EventDate <= @EndDate AND AccountedDate IS NULL
                         GROUP BY FeatureID) m
                   ON e.FeatureID = m.FeatureID AND e.EventDate <= m.EventDate
WHERE BrokerID = @BrokerID
IF 0 != @@ERROR
BEGIN
	RAISERROR('Error in matching subscribe/unsubscribe event in UpdateUsageEvents sp', 16, 1);
	GOTO ErrorHandler;
END
UPDATE Usage_Event
SET AccountedDate = GETDATE()
FROM Usage_Event e JOIN (SELECT UserID, MAX(EventDate) AS EventDate
		         FROM Usage_Event
			 WHERE BrokerID = @BrokerID AND EventType = 'f' AND EventDate <= @EndDate AND AccountedDate IS NULL
			 GROUP BY UserID) m
		   ON e.UserID = m.UserID AND e.EventDate <= m.EventDate
WHERE e.BrokerID = @BrokerID
IF 0 != @@ERROR
BEGIN
	RAISERROR('Error in matching user_on/user_off event in UpdateUsageEvents sp', 16, 1);
	GOTO ErrorHandler;
END
COMMIT TRANSACTION
RETURN 0
ErrorHandler:
	ROLLBACK TRANSACTION @Tranpoint
	ROLLBACK TRANSACTION
	RETURN -100;
