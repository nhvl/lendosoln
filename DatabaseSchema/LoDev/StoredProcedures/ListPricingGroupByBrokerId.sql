CREATE   PROCEDURE ListPricingGroupByBrokerId 
	@BrokerId Guid , @ExternalPriceGroupEnabled Bit = NULL
AS
	SELECT
		g.LpePriceGroupId,
		g.LpePriceGroupName,
		g.ExternalPriceGroupEnabled,
		CASE WHEN( b.BrokerLpePriceGroupIdDefault IS NOT NULL ) THEN CAST( 1 AS Bit ) ELSE CAST( 0 AS Bit ) END AS IsCurrentDefault
	FROM
		LPE_Price_Group AS g LEFT JOIN Broker AS b ON b.BrokerId = g.BrokerId AND b.BrokerLpePriceGroupIdDefault = g.LpePriceGroupId
	WHERE
		g.BrokerId = @BrokerId
		AND
		g.ExternalPriceGroupEnabled = COALESCE( @ExternalPriceGroupEnabled , g.ExternalPriceGroupEnabled )
	ORDER BY
		g.ExternalPriceGroupEnabled DESC, g.LpePriceGroupName
