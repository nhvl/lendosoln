-- =============================================
-- Author:		Scott Kibler
-- Create date: 8/6/2014
-- Description:	
--	Finds unused loans who were created from outdated quickpricer templates
--  (checks the template and file version.)
-- =============================================
CREATE PROCEDURE dbo.QUICKPRICER_2_LOAN_USAGE_FindUnusedLoansNotMatchingQPTemplateAtBroker
	@BrokerID UniqueIdentifier,
	@QuickPricerTemplateID uniqueidentifier,
	@QuickPricerTemplateFileVersion int = null -- if not null, use it.
AS
BEGIN
	DECLARE @ErrorCode int
	DECLARE @ErrorMsg varchar(1000)
	SET @ErrorCode = -1;
	SET @ErrorMsg = 'Unexpected Error';
	
	IF @QuickPricerTemplateFileVersion IS NULL
	BEGIN
		SELECT @QuickPricerTemplateFileVersion=sFileVersion
		FROM LOAN_FILE_A
		WHERE
			  sLId = @QuickPricerTemplateID
		IF 1 <> @@RowCount
		BEGIN
			SET @ErrorMsg = 'Could not find the specified quickpricer template.'
			GOTO ErrorHandler;
		END
	END
	
	SELECT LoanID
	FROM QUICKPRICER_2_LOAN_USAGE
	WHERE 
		    BrokerID = @BrokerID
		AND IsInUse = 0
		AND (
				QuickPricerTemplateID != @QuickPricerTemplateID 
			 OR QuickPricerTemplateFileVersion <> @QuickPricerTemplateFileVersion
			)
	IF 0 <> @@Error
	BEGIN
		SET @ErrorMsg = 'An issue occurred when finding the loans.'
		GOTO ErrorHandler;
	END
	
	
	RETURN 0;
	ErrorHandler:
		RAISERROR(@ErrorMsg, 16, 1)
		RETURN @ErrorCode;
END
