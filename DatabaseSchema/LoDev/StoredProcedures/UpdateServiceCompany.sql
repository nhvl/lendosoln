ALTER PROCEDURE [dbo].[UpdateServiceCompany] 
	@ComId uniqueidentifier,
	@ComNm varchar(100),
	@ComUrl varchar(196),
	@ProtocolType int,
	@IsValid bit,
	@IsInternalOnly bit,
	@MclCraCode char(2),
	@IsWarningOn bit,
	@WarningMsg varchar(500),
	@WarningStartD smalldatetime,
	@VoxVendorId int = null
AS
UPDATE Service_Company
SET 
	ComNm = @ComNm,
	ComUrl = @ComUrl,
	ProtocolType = @ProtocolType,
	isValid = @IsValid,
	IsInternalOnly = @IsInternalOnly,
	MclCraCode = @MclCraCode,
	IsWarningOn = @IsWarningOn,
	WarningMsg = @WarningMsg,
	WarningStartD = @WarningStartD,
	VoxVendorId = @VoxVendorId
WHERE ComId = @ComId
