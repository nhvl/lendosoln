CREATE proc [dbo].[DocMagic_DeleteAlternateLender]
	@DocMagicAlternateLenderId uniqueidentifier
as
update DOCMAGIC_ALTERNATE_LENDER
set IsValid = 0
where DocMagicAlternateLenderId = @DocMagicAlternateLenderId