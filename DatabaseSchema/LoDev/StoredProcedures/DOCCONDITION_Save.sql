-- =============================================
-- Author:		Matthew Pham
-- Create date: 9/19/2012
-- Description:	Create a new document-condition
--				association.
-- =============================================
CREATE PROCEDURE dbo.DOCCONDITION_Save 
	-- Add the parameters for the stored procedure here
	@LoanId uniqueidentifier,
	@TaskId varchar(10),
	@DocId uniqueIdentifier,
	@Status int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	BEGIN TRAN
		-- Insert if the row doesn't exist, otherwise update
		IF NOT EXISTS
			( SELECT 1 FROM TASK_x_EDOCS_DOCUMENT TD WITH (updlock, rowlock, holdlock)
			WHERE TD.sLId = @LoanId
			AND TD.TaskId = @TaskId
			AND TD.DocumentId = @DocId)
			INSERT INTO TASK_x_EDOCS_DOCUMENT
			VALUES ( @LoanId, @TaskId, @DocId, @Status )
			
		ELSE
			UPDATE TASK_x_EDOCS_DOCUMENT
			SET sLId = @LoanId,
				TaskId = @TaskId,
				DocumentId = @DocId,
				Status = @Status
			WHERE sLId = @LoanId
			AND TaskId = @TaskId
			AND DocumentId = @DocId
	COMMIT
END
