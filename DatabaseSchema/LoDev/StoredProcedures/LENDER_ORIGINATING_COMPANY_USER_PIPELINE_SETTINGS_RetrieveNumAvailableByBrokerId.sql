-- =============================================
-- Author:		Eduardo Michel
-- Create date: 8/31/2018
-- Description:	Retrieves all the Custom Pipeline Reports the user can choose from.
-- =============================================
ALTER PROCEDURE [dbo].[LENDER_ORIGINATING_COMPANY_USER_PIPELINE_SETTINGS_RetrieveNumAvailableByBrokerId]
	-- Add the parameters for the stored procedure here
	@BrokerId uniqueidentifier, 
	@PortalMode int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT 
		COUNT(*)
	FROM 
		LENDER_ORIGINATING_COMPANY_USER_PIPELINE_SETTINGS ps
	WHERE ps.BrokerId = @BrokerId AND 
	(
		(ps.AvailableForBrokerPortalMode = 1 AND @PortalMode = 1) OR
		(ps.AvailableForMiniCorrespondentPortalMode = 1 AND @PortalMode = 2) OR
		(ps.AvailableForCorrespondentPortalMode = 1 AND @PortalMode = 3)OR
		(@PortalMode = 4)
	)
	
END
