CREATE  PROCEDURE [dbo].[GetRoles] 
AS
	SELECT
		r.RoleId ,
		r.RoleDesc ,
		r.RoleImportanceRank ,
		r.RoleModifiableDesc
	FROM
		Role AS r WITH (NOLOCK)
	/* OPM 108148 gf 1/23/13 - Funder is now a supported role
	WHERE
		r.RoleDesc NOT IN ( 'Funder' )
	*/
	ORDER BY
		RoleImportanceRank
	if(0!=@@error)
	begin
		RAISERROR('Error in the select statement in GetRoles sp', 16, 1);
		return -100;
	end
	return 0
