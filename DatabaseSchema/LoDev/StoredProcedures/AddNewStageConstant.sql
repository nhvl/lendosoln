-- =============================================
-- Author:		Peter Anargirou
-- Create date: 8/12/08
-- Description:	Adds a new stage constant
-- =============================================
ALTER PROCEDURE dbo.AddNewStageConstant
	-- Add the parameters for the stored procedure here
	@KeyIdStr varchar(100),
	@OptionContentStr varchar(1500),
	@OptionContentInt int,
	@Owner varchar(100),
	@CreatedDate smalldatetime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO STAGE_CONFIG ( KeyIdStr, OptionContentStr, OptionContentInt, Owner, CreatedDate )
	VALUES ( @KeyIdStr, @OptionContentStr, @OptionContentInt, @Owner, @CreatedDate )
END
