-- =============================================
-- Author:		Michael Leinweaver
-- Create date: 5/6/2016
-- Description:	Updates the default branch and price group
-- 				values for a broker and propagates that change
--				to the originating companies that utilize broker
--				level defaults.
-- =============================================
ALTER PROCEDURE [dbo].[Broker_UpdateDefaultBranchPGValues]
	@BrokerId uniqueidentifier,
	@DefaultWholesaleBranchId uniqueidentifier = null,
	@DefaultMiniCorrBranchId uniqueidentifier = null,
	@DefaultCorrBranchId uniqueidentifier = null,
	@DefaultWholesalePriceGroupId uniqueidentifier = null,
	@DefaultMiniCorrPriceGroupId uniqueidentifier = null,
	@DefaultCorrPriceGroupId uniqueidentifier = null
AS
BEGIN
	-- Update broker settings
	UPDATE BROKER
	SET
		WholesaleBranchID = COALESCE(@DefaultWholesaleBranchID,WholesaleBranchID),
		MiniCorrBranchID = COALESCE(@DefaultMiniCorrBranchID,MiniCorrBranchID),
		CorrespondentBranchID = COALESCE(@DefaultCorrBranchId,CorrespondentBranchID),
		WholesalePriceGroupID = COALESCE(@DefaultWholesalePriceGroupID,WholesalePriceGroupID),
		MiniCorrPriceGroupID = COALESCE(@DefaultMiniCorrPriceGroupID,MiniCorrPriceGroupID),
		CorrespondentPriceGroupID = COALESCE(@DefaultCorrPriceGroupId,CorrespondentPriceGroupID)
	WHERE BrokerId = @BrokerId

	-- Update OC branch settings
	IF @DefaultWholesaleBranchId IS NOT NULL
	BEGIN
		UPDATE PML_BROKER
		SET BranchId = @DefaultWholesaleBranchId
		WHERE BrokerId = @BrokerId AND UseDefaultWholesaleBranchId = 1
	END
	
	IF @DefaultMiniCorrBranchId IS NOT NULL
	BEGIN
		UPDATE PML_BROKER
		SET MiniCorrespondentBranchId = @DefaultMiniCorrBranchId
		WHERE BrokerId = @BrokerId AND UseDefaultMiniCorrBranchId = 1
	END
	
	IF @DefaultCorrBranchId IS NOT NULL
	BEGIN
		UPDATE PML_BROKER
		SET CorrespondentBranchId = @DefaultCorrBranchId
		WHERE BrokerId = @BrokerId AND UseDefaultCorrBranchId = 1
	END
	
	-- Update OC price group settings
	IF @DefaultWholesalePriceGroupId IS NOT NULL
	BEGIN
		UPDATE PML_BROKER
		SET LpePriceGroupId = @DefaultWholesalePriceGroupId
		WHERE BrokerId = @BrokerId AND UseDefaultWholesalePriceGroupId = 1
	END
	
	IF @DefaultMiniCorrPriceGroupId IS NOT NULL
	BEGIN
		UPDATE PML_BROKER
		SET MiniCorrespondentLpePriceGroupId = @DefaultMiniCorrPriceGroupId
		WHERE BrokerId = @BrokerId AND UseDefaultMiniCorrPriceGroupId = 1
	END
	
	IF @DefaultCorrPriceGroupId IS NOT NULL
	BEGIN
		UPDATE PML_BROKER
		SET CorrespondentLpePriceGroupId = @DefaultCorrPriceGroupId
		WHERE BrokerId = @BrokerId AND UseDefaultCorrPriceGroupId = 1
	END
END
	