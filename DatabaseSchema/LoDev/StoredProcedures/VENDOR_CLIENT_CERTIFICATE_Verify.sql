-- =============================================
-- Author:		Michael Leinweaver
-- Create date: 9/21/2017
-- Description:	Verifies a vendor client certificate.
-- =============================================
CREATE PROCEDURE [dbo].[VENDOR_CLIENT_CERTIFICATE_Verify]
	@VendorId uniqueidentifier,
	@CertificateId uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT 1
	FROM VENDOR_CLIENT_CERTIFICATE
	WHERE VendorId = @VendorId AND CertificateId = @CertificateId
END
