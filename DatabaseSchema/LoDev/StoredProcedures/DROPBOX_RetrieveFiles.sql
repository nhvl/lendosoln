CREATE PROCEDURE [dbo].[DROPBOX_RetrieveFiles]
	@UserId uniqueidentifier,
	@FileId uniqueidentifier = null
AS
BEGIN
	select * from DROPBOX_FILES where UserId = @UserId and FileId = coalesce(@FileId, FileId)
END