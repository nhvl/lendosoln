


-- =============================================
-- Author:		Antonio Valencia
-- Create date: 1/7/10
-- Description:	Retrieves the audit events for a given doc
-- =============================================
CREATE PROCEDURE [dbo].[EDOCS_FetchAuditEvents] 
	@DocumentId uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT ModifiedByUserId, Version, ModifiedDate, Description, userfirstnm + ' ' + userlastnm as Name, Details
	FROM EDOCS_AUDIT_HISTORY 
	left outer join broker_user as a on UserId = ModifiedByUserid 
	left outer join  Employee as e on a.employeeid = e.employeeid
	WHERE DocumentId = @DocumentId order by version asc
END



