

CREATE PROCEDURE [dbo].[RetrieveBrokerLicenseNumByBrokerId]
	@BrokerId uniqueidentifier
AS
	SELECT
		BrokerLicNum 
	FROM 
		Broker
	WHERE
		BrokerId = @BrokerId