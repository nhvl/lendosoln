
CREATE PROCEDURE [dbo].[UpdateDefaultNonBoolRolePermissions]
	@RoleID uniqueidentifier,
	@BrokerID uniqueidentifier,
	@DefaultNonBoolPermissions char(100)
	
AS
	UPDATE ROLE_DEFAULT_PERMISSIONS
	       SET DefaultNonBoolPermissions = @DefaultNonBoolPermissions
	 WHERE RoleId = @RoleID AND BrokerId = @BrokerID
	
	if(0!=@@error)
	begin
		RAISERROR('Error in the update statement in UpdateDefaultNonBoolRolePermissions sp', 16, 1);
		return -100;
	end
	return 0;

