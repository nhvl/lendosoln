-- =============================================
-- Author:		Justin Lara
-- Create date: 12/7/2016
-- Description:	Retrieves the Service Companies
--				using the given protocol.
-- =============================================
ALTER PROCEDURE [dbo].[SERVICE_COMPANY_ListByProtocolType]
	@ProtocolType int
AS
BEGIN
	SELECT ComId, ComNm
	FROM SERVICE_COMPANY
	WHERE ProtocolType = @ProtocolType
END
