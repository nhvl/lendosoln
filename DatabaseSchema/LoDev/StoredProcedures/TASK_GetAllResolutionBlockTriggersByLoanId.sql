CREATE PROCEDURE [dbo].[TASK_GetAllResolutionBlockTriggersByLoanId]
	@BrokerId uniqueidentifier,
	@LoanId uniqueidentifier
AS
BEGIN
	SELECT
		TaskId, ResolutionBlockTriggerName
	FROM TASK
	WHERE BrokerId = @BrokerId
		AND LoanId = @LoanId
		AND ResolutionBlockTriggerName IS NOT NULL
		AND ResolutionBlockTriggerName <> ''
END