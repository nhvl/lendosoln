-- =============================================
-- Author:		Geoffrey Feltman
-- Create date: 6/11/14
-- Description:	Deletes a pending document request from the database.
-- =============================================
CREATE PROCEDURE [dbo].[PENDING_DOCUMENT_REQUEST_Delete]
	@Id int,
	@BrokerId uniqueidentifier
AS
BEGIN
	DELETE FROM PENDING_DOCUMENT_REQUEST
	WHERE Id = @Id 
		AND BrokerId = @BrokerId
END
