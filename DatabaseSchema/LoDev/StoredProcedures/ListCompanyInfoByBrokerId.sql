CREATE  PROCEDURE [dbo].[ListCompanyInfoByBrokerId]
	@BrokerId Guid
AS
	SELECT
		v.Addr ,
		v.City ,
		v.State ,
		v.Zip ,
		v.Phone ,
		v.Fax
	FROM
		View_LendersOffice_Employee AS v
	WHERE
		v.BrokerId = @BrokerId
