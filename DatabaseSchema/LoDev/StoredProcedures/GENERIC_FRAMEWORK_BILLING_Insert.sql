-- =============================================
-- Author:		Timothy Jewell
-- Create date:	2/20/2015
-- Description:	Inserts a billing record for a Generic Framework transaction.
-- =============================================
CREATE PROCEDURE [dbo].[GENERIC_FRAMEWORK_BILLING_Insert]
	-- Add the parameters for the stored procedure here
	@LoanId uniqueidentifier,
	@BrokerId uniqueidentifier,
	@ServiceType int,
	@ProviderId varchar(7),
	@DocumentId uniqueidentifier
	
AS
BEGIN

	INSERT INTO GENERIC_FRAMEWORK_BILLING
		( LoanId,  BrokerId,  ServiceType,  ProviderId,  DocumentId)
	VALUES
		(@LoanId, @BrokerId, @ServiceType, @ProviderId, @DocumentId)
END
