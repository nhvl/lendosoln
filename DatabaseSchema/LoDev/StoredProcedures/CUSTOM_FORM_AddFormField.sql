CREATE PROCEDURE [dbo].[CUSTOM_FORM_AddFormField]
	@FieldId varchar(100),
	@FieldFriendlyName varchar(200),
	@CategoryId int,
	@IsReviewNeeded bit,
	@IsBoolean bit,
	@Description varchar(200),
	@IsHidden bit
AS
BEGIN

	INSERT INTO CUSTOM_FORM_FIELD_LIST(FieldId, FieldFriendlyName,CategoryId, IsReviewNeeded, IsBoolean, Description, IsHidden)
	VALUES (@FieldId, @FieldFriendlyName, @CategoryId, @IsReviewNeeded, @IsBoolean, @Description, @IsHidden)
END
