
CREATE PROCEDURE ListLockDeskClosureByBrokerId
	@BrokerId Guid
AS
BEGIN

	SELECT ClosureDate, IsClosedAllDay, LockDeskOpenTime, LockDeskCloseTime
	FROM LockDesk_Closure
	WHERE BrokerId = @BrokerId
	ORDER BY ClosureDate ASC
END
