-- =============================================
-- Author:		Antonio Valencia
-- Create date: 1/6/10 - updated 2/14/2011
-- Description:	Retrieves the matching electronic documents from the database
-- 1/4/12: Added support for document status - OPM 63511, AM
-- 8/22/12: Added First Close Title doc type and sent date
-- 8/14/2013: Added HideFromPmlUsers
-- 6/23/2015: dd - Add MetadaJsonContent
-- =============================================
ALTER PROCEDURE [dbo].[EDOCS_FetchDocsOptimized] 
	@DocumentId uniqueidentifier = null, 
	@LoanId uniqueidentifier = null,
	@BrokerId uniqueidentifier, -- I have added BrokerId Every Where  
	@IsValid bit = null
AS
BEGIN
	IF (@LoanId IS NULL AND @DocumentId IS NOT NULL)
	BEGIN
		SELECT @LoanId = sLId FROM EDOCS_DOCUMENT WHERE BrokerId = @BrokerId and DocumentId = @DocumentId
	END

	-- RESULT SET 1 
	SELECT  aappid, aBFirstNm, aCFirstNm, aBLastNm, aCLastNm
	FROM APPLICATION_A WHERE sLId = @LoanId

	-- IF SEARCHING BY DOCUMENT ID  Continue to query folder roles in sql. 
	IF (@DocumentId IS NOT NULL) BEGIN
		print 'first query';
		SELECT  
			doctype.FolderName,
			COALESCE((SELECT CAST(f.roleid as varchar(MAX)) +':' + CAST(f.ispmluser as varchar(MAX)) + ',' 		FROM edocs_folder_x_role as f WHERE f.folderid = doctype.folderid 	FOR XML PATH ('')	),'')  as FolderRoles,
			doctype.FolderId, 
			doc.ImageStatus, 
			doc.TimeToRequeueForImageGeneration , 
			doc.HasInternalAnnotation, 
			doc.DocumentId, 
			doc.sLId, 
			doc.aAppId, 
			doc.IsValid,  
			doctype.DocTypeName,   
			doctype.IsValid as DocTypeIsValid,
			doc.BrokerId, 
			doc.DocTypeId, 
			doc.FaxNumber, 
			doc.FileDBKey_CurrentRevision, 
			doc.FileDBKey_OriginalDocument,
			doc.Description as PublicDescription, 
			doc.InternalDescription, 
			doc.Comments, 
			doc.NumPages, 
			doc.IsUploadedByPmlUser, 
			doc.[Version], 
			doc.LastModifiedDate, 
			doc.CreatedDate, 
			doc.IsEditable,
			doc.PdfHasOwnerPassword, 
			doc.TimeImageCreationEnqueued, 
			doc.PdfHasUnsupportedFeatures, 
			doc.[Status], 
			doc.StatusDescription,
			doc.PdfPageInfo,
			doc.HideFromPmlUsers,
			doc.MetadataJsonContent,
			doc.OriginalMetadataJsonContent,
			doc.IsESigned,
			doc.CreatedByUserId,
			doc.HasESignTags,
			doc.MetadataProtobufContent,
			doc.OriginalMetadataProtobufContent,
            gen.FileType as GenericFileType
		FROM EDOCS_DOCUMENT as doc
		JOIN VIEW_EDOCS_DOCTYPE_FOLDER doctype on doctype.doctypeid = doc.doctypeid
        LEFT JOIN EDOCS_GENERIC as gen ON doc.DocumentId = gen.DocumentId AND doc.sLId = gen.sLId AND doc.BrokerId = gen.BrokerId
		WHERE (@IsValid IS NULL OR doc.IsValid = @IsValid) AND doc.BrokerId = @BrokerId and doc.DocumentId = @DocumentId
	END
	
	
	-- SEARCHING BY LOAN ID AND BROKERID 
	ELSE IF (@DocumentId IS NULL AND @LoanId IS NOT NULL ) BEGIN
		print 'second query';
		SELECT  
			doctype.FolderName,
			doctype.FolderId, 
			doc.ImageStatus, 
			doc.TimeToRequeueForImageGeneration , 
			doc.HasInternalAnnotation, 
			doc.DocumentId, 
			doc.sLId, 
			doc.aAppId, 
			doc.IsValid,  
			doctype.DocTypeName,   
			doctype.IsValid as DocTypeIsValid,
			doc.BrokerId, 
			doc.DocTypeId, 
			doc.FaxNumber, 
			doc.FileDBKey_CurrentRevision, 
			doc.FileDBKey_OriginalDocument,
			doc.Description as PublicDescription, 
			doc.InternalDescription, 
			doc.Comments, 
			doc.NumPages, 
			doc.IsUploadedByPmlUser, 
			doc.[Version], 
			doc.LastModifiedDate, 
			doc.CreatedDate, 
			doc.IsEditable,
			doc.PdfHasOwnerPassword, 
			doc.TimeImageCreationEnqueued, 
			doc.PdfHasUnsupportedFeatures, 
			doc.[Status], 
			doc.StatusDescription,
			doc.PdfPageInfo,
			doc.HideFromPmlUsers,
			doc.MetadataJsonContent,
			doc.OriginalMetadataJsonContent,
			doc.IsESigned,
			doc.CreatedByUserId,
			doc.HasESignTags,
			doc.MetadataProtobufContent,
			doc.OriginalMetadataProtobufContent,
            gen.FileType as GenericFileType		
		FROM EDOCS_DOCUMENT as doc
		JOIN VIEW_EDOCS_DOCTYPE_FOLDER doctype on doctype.doctypeid = doc.doctypeid
        LEFT JOIN EDOCS_GENERIC as gen ON doc.DocumentId = gen.DocumentId AND doc.sLId = gen.sLId AND doc.BrokerId = gen.BrokerId
		WHERE (@IsValid IS NULL OR doc.IsValid = @IsValid) AND doc.sLId =   @LoanId AND doc.BrokerId = @BrokerId
		
		-- Result SET 3
		select f.folderid, f.brokerid, fr.roleid, fr.IsPmlUser, f.folderName  
		from  edocs_folder f  left join edocs_folder_x_role fr   on f.folderid = fr.folderid 
		where brokerid = @BrokerId
	END
END
