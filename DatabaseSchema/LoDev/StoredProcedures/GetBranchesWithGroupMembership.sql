CREATE PROCEDURE [GetBranchesWithGroupMembership]
	@BrokerId uniqueidentifier,
	@GroupId uniqueidentifier
AS
BEGIN
	SET NOCOUNT ON	
	SELECT CASE WHEN gb.GroupId IS NULL THEN 0 ELSE 1 END AS IsInGroup, b.BranchId, b.BranchCode, b.BranchNm, b.BranchAddr, b.BranchCity, b.BranchState, b.BranchZip
	FROM Branch b LEFT JOIN (SELECT * FROM GROUP_x_BRANCH WHERE GroupId=@GroupId) gb on b.BranchId = gb.BranchId 
	WHERE b.BrokerId = @BrokerId ORDER BY b.BranchNm
END
