CREATE PROCEDURE ListInvoiceUsageEvents
	@BrokerId Guid = NULL , @StartDate datetime = NULL , @FinalDate datetime = NULL
AS
	SELECT u.UserId , u.EventType , MAX( u.EventDate ) AS EventDate , b.BrokerId , b.BrokerNm
	FROM
		Usage_Event AS u LEFT JOIN Broker AS b ON b.BrokerId = u.BrokerId JOIN All_User AS z ON z.UserId = u.UserId
	WHERE
		u.BrokerId = @BrokerId
		AND
		DATEDIFF( day , COALESCE( @StartDate , u.EventDate ) , u.EventDate ) < 0
		AND
		(
			u.UserId = '00000000-0000-0000-0000-000000000000'
			OR
			z.Type = 'B'
		)
	GROUP BY
		u.UserId , u.EventType , b.BrokerId , b.BrokerNm
	SELECT u.UserId , u.EventType , u.EventDate , b.BrokerId , b.BrokerNm
	FROM
		Usage_Event AS u LEFT JOIN Broker AS b ON b.BrokerId = u.BrokerId JOIN All_User AS z ON z.UserId = u.UserId
	WHERE
		u.BrokerId = @BrokerId
		AND
		DATEDIFF( day , COALESCE( @StartDate , u.EventDate ) , u.EventDate ) >= 0
		AND
		DATEDIFF( day , COALESCE( @FinalDate , u.EventDate ) , u.EventDate ) <= 0
		AND
		(
			u.UserId = '00000000-0000-0000-0000-000000000000'
			OR
			z.Type = 'B'
		)
	SELECT a.UserId , 'c' AS EventType , e.StartD AS EventDate , b.BrokerId , b.BrokerNm
	FROM
		Broker AS b JOIN Branch AS r ON r.BrokerId = b.BrokerId JOIN Employee AS e ON e.BranchId = r.BranchId JOIN Broker_User AS x ON x.EmployeeId = e.EmployeeId JOIN All_User AS a ON a.UserId = x.UserId LEFT JOIN Usage_Event AS u ON u.UserId = a.UserId AND DATEDIFF( day , @FinalDate , u.EventDate ) <= 0 AND u.EventType = 'o'
	WHERE
		b.BrokerId = @BrokerId
		AND
		DATEDIFF( day , COALESCE( @FinalDate , e.StartD ) , e.StartD ) <= 0
		AND
		e.IsActive = 1
		AND
		a.IsActive = 1
		AND
		u.UserId IS NULL
	if( 0 != @@error )
	begin
		RAISERROR('Error querying ListInvoiceUsageEvents sp', 16, 1);
		return -100;
	end
	return 0;
