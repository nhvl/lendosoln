-- =============================================
-- Author:		Eric Mallare
-- Create date: 9/5/2018
-- Description:	Creates an MI Framework Request job entry
-- =============================================
ALTER PROCEDURE [dbo].[MI_FRAMEWORK_REQUEST_JOBS_Create]
	@JobId int,
    @LoanId uniqueidentifier,
    @ApplicationId uniqueidentifier,
    @BrokerId uniqueidentifier,
    @UserId uniqueidentifier,
    @UserType char(1),
    @BranchId uniqueidentifier,
    @VendorId uniqueidentifier,
    @PremiumType tinyint,
    @Refundability tinyint,
    @UFMIPFinanced bit,
    @MasterPolicyNumber varchar(100),
    @CoveragePercent decimal(9,4),
    @RenewalType tinyint,
    @PremiumAtClosing tinyint,
    @IsRelocationLoan bit,
    @IsQuote bit,
    @QuoteNumber varchar(20) = null,
    @OriginalQuoteNumber varchar(50) = null,
    @PollingIntervalInSeconds int,
	@DelegationType tinyint,
	@RequestType int = 0,
	@UpfrontPremiumMismoValue varchar(14) = NULL
AS
BEGIN
	INSERT INTO MI_FRAMEWORK_REQUEST_JOBS 
	(
		JobId,
		LoanId,
		ApplicationId,
		BrokerId,
		UserId,
		UserType,
		BranchId,
		VendorId,
		PremiumType,
		Refundability,
		UFMIPFinanced,
		MasterPolicyNumber,
		CoveragePercent,
		RenewalType,
		PremiumAtClosing,
		IsRelocationLoan,
		IsQuote,
		QuoteNumber,
		OriginalQuoteNumber,
		PollingIntervalInSeconds,
		DelegationType,
		RequestType,
		UpfrontPremiumMismoValue
	)
	VALUES 
	(
		@JobId,
		@LoanId,
		@ApplicationId,
		@BrokerId,
		@UserId,
		@UserType,
		@BranchId,
		@VendorId,
		@PremiumType,
		@Refundability,
		@UFMIPFinanced,
		@MasterPolicyNumber,
		@CoveragePercent,
		@RenewalType,
		@PremiumAtClosing,
		@IsRelocationLoan,
		@IsQuote,
		@QuoteNumber,
		@OriginalQuoteNumber,
		@PollingIntervalInSeconds,
		@DelegationType,
		@RequestType,
		@UpfrontPremiumMismoValue
	)
	
END