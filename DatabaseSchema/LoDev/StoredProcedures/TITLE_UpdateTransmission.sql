-- =============================================
-- Author:		Eric Mallare
-- Create date: 8/17/2017
-- Description:	Updates a transmission
-- =============================================
ALTER PROCEDURE [dbo].[TITLE_UpdateTransmission]
	@TargetUrl varchar(200),
	@TransmissionAuthenticationT int,
	@TransmissionAssociationT int,
	@EncryptionKeyId uniqueidentifier,
	@ResponseCertId uniqueidentifier = null,
	@RequestCredentialName varchar(50) = null,
	@RequestCredentialPassword varbinary(max) = null,
	@ResponseCredentialName varchar(50) = null,
	@ResponseCredentialPassword varbinary(max) = null,
	@TransmissionInfoId int
AS
BEGIN
	UPDATE
		TITLE_FRAMEWORK_TRANSMISSION_INFO
	SET
		TargetUrl=@TargetUrl,
		TransmissionAuthenticationT=@TransmissionAuthenticationT,
		EncryptionKeyId = @EncryptionKeyId,
		ResponseCertId=@ResponseCertId,
		RequestCredentialName=@RequestCredentialName,
		ResponseCredentialName=@ResponseCredentialName,
		RequestCredentialPassword=@RequestCredentialPassword,
		ResponseCredentialPassword=@ResponseCredentialPassword
	WHERE
		TransmissionInfoId=@TransmissionInfoId
END
