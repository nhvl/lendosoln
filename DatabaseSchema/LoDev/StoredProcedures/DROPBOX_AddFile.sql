ALTER PROCEDURE [dbo].[DROPBOX_AddFile]
	@UserId uniqueidentifier,
	@FileId uniqueidentifier,
	@FileName varchar(50),
	@SizeBytes int,
	@UploadedOn datetime,
	@IsESigned bit
AS
BEGIN
	insert DROPBOX_FILES
	values (@UserId,
			@FileId,
			@FileName,
			@SizeBytes,
			@UploadedOn,
			@IsESigned)
END