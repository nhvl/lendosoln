ALTER procedure [dbo].[EDOCS_RetrieveSharedFaxNumber]
	
as
SELECT FaxNumber, DocRouterLogin, EncryptionKeyId, EncryptedDocRouterPassword
from EDOCS_FAX_NUMBER
where BrokerId IS NULL