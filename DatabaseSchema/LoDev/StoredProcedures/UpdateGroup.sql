CREATE PROCEDURE [UpdateGroup]
	@BrokerId uniqueidentifier, 
	@GroupId uniqueidentifier,
	@GroupName varchar(100) ,	
	@Description varchar(500)	
AS
BEGIN
	SET NOCOUNT ON
	BEGIN TRANSACTION
		UPDATE [GROUP] SET GroupName=@GroupName, Description=@Description WHERE GroupId = @GroupId AND BrokerID=@BrokerId
		if( 0!=@@error)
		BEGIN
			ROLLBACK TRANSACTION
			RAISERROR('Error in updating the Group table in UpdateGroup sp', 16, 1);
			RETURN -100;
		END
	COMMIT TRANSACTION
	RETURN 0;
END
