CREATE PROCEDURE GetBorrowerName
	@ApplicationID Guid
AS
	SELECT aBLastNm + ', ' + aBFirstNm FROM APPLICATION_A WHERE aAppId = @ApplicationID
