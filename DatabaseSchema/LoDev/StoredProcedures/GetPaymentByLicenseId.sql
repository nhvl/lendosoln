CREATE   PROCEDURE GetPaymentByLicenseId
	@LicenseId Guid
AS
	SELECT *
	FROM Payment with(nolock)
	WHERE LicenseId = @LicenseId
