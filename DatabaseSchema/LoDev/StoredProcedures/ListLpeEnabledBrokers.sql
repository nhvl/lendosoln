CREATE PROCEDURE [dbo].ListLpeEnabledBrokers
as
select distinct fea.BrokerId, br.brokerNm 
from Feature_Subscription fea join broker br on fea.brokerid = br.brokerid
where fea.IsActive = 1 and br.Status = 1 and FeatureId in ('5B542BB7-872A-4A4E-B1A1-228FDFC2988A', '0FAB7C8C-B3C4-49E5-92BF-2DCDD45B701F')

