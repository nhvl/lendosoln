/*
	Author: Scott Kibler
	Created Date: 12/15/2015
	Decription:
		List the teams assigned to the user(s) given the supplied criteria.
*/
CREATE PROCEDURE [dbo].[TEAM_USER_ASSIGNMENT_List]
	@TeamID			uniqueidentifier = null,
	@EmployeeId		uniqueidentifier = null,
	@IsPrimary		bit = null
AS
	SELECT 
		TeamId,
		EmployeeId,
		IsPrimary
	FROM TEAM_USER_ASSIGNMENT tua
	WHERE
		((@TeamID is null) OR (TeamID = @TeamID))
	AND
		((@EmployeeID is null) OR (EmployeeID = @EmployeeID))
	AND
		((@IsPrimary is null) OR (IsPrimary = @IsPrimary))
		
