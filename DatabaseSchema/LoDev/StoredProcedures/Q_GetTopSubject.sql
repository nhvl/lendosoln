

-- =============================================
-- Author:		Antonio Valencia
-- Create date: 7/14/11
-- Description:	Get By Top Subject
-- =============================================
CREATE PROCEDURE [dbo].[Q_GetTopSubject] 
	@QueueId int,
	@Priority tinyint
AS
BEGIN
	DECLARE @Subject1 varchar(300); 
	DECLARE @RowCount int

	SELECT TOP 1 @Subject1 = Subject1 
	FROM Q_MESSAGE 
	WHERE QueueId = @QueueId AND Priority = @Priority
	
	
	SELECT @RowCount = @@rowcount
	
	IF @RowCount > 0 
	BEGIN	
		SELECT MessageId, Subject1, Data, Subject2, Priority, InsertionTime, DataLoc
		FROM Q_MESSAGE WHERE QueueId = @QueueId AND Subject1 = @Subject1 AND Priority = @Priority
	
	END

END


