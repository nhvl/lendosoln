-- =============================================
-- Author:		David Dao
-- Create date: 
-- Description:	Returns either OK or ERROR. There are multiple reasons for ERROR
--       1) Code does not exists
--       2) Code does not match
--       3) Too many guesses on code
--       4) Code is expired.
--       However for security purpose, I am not going to return which one cause error.
-- =============================================
CREATE PROCEDURE dbo.ALL_USER_MULTI_FACTOR_AUTH_CODE_Verify
    @UserId UniqueIdentifier,
    @AuthCode varchar(10)

AS
BEGIN

    DECLARE @ActualAuthCode varchar(10)
    
    SELECT @ActualAuthCode=AuthCode
    FROM ALL_USER_MULTI_FACTOR_AUTH_CODE
    WHERE UserId = @UserId
      AND ExpiredDate > GETDATE()
      AND NumberOfFailedAttempts < MaxAllowNumberOfFailedAttempts
      
    IF @@ROWCOUNT = 1
    BEGIN
        IF @ActualAuthCode = @AuthCode
        BEGIN
            SELECT 'OK' AS Status
        END
        ELSE
        BEGIN
            UPDATE ALL_USER_MULTI_FACTOR_AUTH_CODE
               SET NumberOfFailedAttempts = NumberOfFailedAttempts + 1
            WHERE UserId = @UserId
            
            SELECT 'ERROR' AS Status
        END
    END
    ELSE
    BEGIN
        SELECT 'ERROR' AS Status
    END
    

END
