
-- =============================================
-- Author:		Antonio Valencia
-- Create date: 4/20/11
-- Description:	Retrieves brokerid and email aliases
-- =============================================
CREATE PROCEDURE [dbo].[TASK_Broker_GetIdsAndAliases] 

AS

-- REVIEW NOTES/QUESTIONS:
-- 6/9/2011: Should this one be conditioned on Broker.Status = 1? -thinhNK 
-- 6/14/2011: Antonio av 
BEGIN
	SELECT brokerid, TaskAliasEmailAddress FROM BROKER where len(TaskAliasEmailAddress) > 0 and Status = 1
END

