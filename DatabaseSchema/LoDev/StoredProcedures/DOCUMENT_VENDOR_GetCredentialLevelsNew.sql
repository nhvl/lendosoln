-- =============================================
-- Author:		paoloa
-- Create date: 7/2013
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[DOCUMENT_VENDOR_GetCredentialLevelsNew]
	-- Add the parameters for the stored procedure here
	@UserId uniqueidentifier,
	@BranchId uniqueidentifier,
	@BrokerId uniqueidentifier,
	@VendorId uniqueidentifier
AS
BEGIN
		select	[Login] as DocVendorUserName, 
				[Password] as DocVendorPassword, 
				AccountId as DocVendorCustomerId
		from DOCUMENT_VENDOR_USER_ENABLED_CREDENTIALS
		where UserId = @UserId AND VendorId = @VendorId And BrokerID = @BrokerId
		
		select [Login] as DocVendorUserName, 
			   [Password] as DocVendorPassword, 
			   AccountId as DocVendorCustomerId
		from DOCUMENT_VENDOR_BRANCH_CREDENTIALS
		where BranchId = @BranchId AND VendorId = @VendorId
		
		select [Login] as DocVendorUserName, 
			   [Password] as DocVendorPassword, 
			   AccountId as DocVendorCustomerId
		from DOCUMENT_VENDOR_BROKER_SETTINGS
		where BrokerId = @BrokerId AND VendorId = @VendorId
END
