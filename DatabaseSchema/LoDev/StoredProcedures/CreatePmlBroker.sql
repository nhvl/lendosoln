-- =============================================
-- Author:		David Dao
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[CreatePmlBroker] 
	@PmlBrokerId UniqueIdentifier
	, @BrokerId UniqueIdentifier
	, @IsActive bit
	, @Name varchar(100)
	, @Addr varchar(60)
	, @City varchar(36)
	, @State char(2)
	, @Zip char(5)
	, @Phone varchar(21)
	, @Fax varchar(21)
	, @LicenseXmlContent text
	, @CompanyId varchar(36)
	, @PrincipalName1 varchar(100)
	, @PrincipalName2 varchar(100)	
	, @NmLsIdentifier varchar (50) = ''
	, @OriginatorCompensationAuditXml varchar(max)
	, @OriginatorCompensationPercent decimal(9, 3)
	, @OriginatorCompensationBaseT int
	, @OriginatorCompensationMinAmount money
	, @OriginatorCompensationMaxAmount money
	, @OriginatorCompensationFixedAmount money
	, @OriginatorCompensationNotes varchar(200)
	, @OriginatorCompensationLastModifiedDate datetime = null
	, @CustomPricingPolicyField1 varchar(25)
	, @CustomPricingPolicyField2 varchar(25)
	, @CustomPricingPolicyField3 varchar(25)
	, @CustomPricingPolicyField4 varchar(25)
	, @CustomPricingPolicyField5 varchar(25)
	, @Lender_TPO_LandingPageConfigId uniqueidentifier = null
	, @NmlsName varchar(100)
	, @NmlsNameLckd bit
	, @LpePriceGroupId UniqueIdentifier = null
	, @BranchId UniqueIdentifier = null
	, @PmlCompanyTierId int = 0
	, @CorrespondentTPOPortalConfigID  UniqueIdentifier = null
	, @CorrespondentLpePriceGroupId  UniqueIdentifier = null
	, @CorrespondentBranchId  UniqueIdentifier = null
	, @MiniCorrespondentTPOPortalConfigID  UniqueIdentifier = null
	, @MiniCorrespondentLpePriceGroupId  UniqueIdentifier = null
	, @MiniCorrespondentBranchId  UniqueIdentifier = null
	, @Roles  varchar(50) = ''
	, @UnderwritingAuthority  int = 0
	, @PMLNavigationPermissions varchar(max) = ''
	, @BrokerRoleStatusT int = 0
	, @MiniCorrRoleStatusT int = 0
	, @CorrRoleStatusT int = 0
	, @UseDefaultWholesalePriceGroupId bit = 0
	, @UseDefaultWholesaleBranchId bit = 0
	, @UseDefaultMiniCorrPriceGroupId bit = 0
	, @UseDefaultMiniCorrBranchId bit = 0
	, @UseDefaultCorrPriceGroupId bit = 0
	, @UseDefaultCorrBranchId bit = 0
	, @OriginatorCompensationIsOnlyPaidForFirstLienOfCombo bit = 0
	, @TpoColorThemeId uniqueidentifier = null
	, @TaxIdEncrypted varbinary(max) = null
	, @EncryptionKeyId uniqueidentifier = null
	, @IndexNumber int = NULL
AS
BEGIN
	DECLARE @UseBranchLpePriceGroupId  bit
	IF @LpePriceGroupId IS NOT NULL AND @LpePriceGroupId = '00000000-0000-0000-0000-000000000000'
	BEGIN
		SET @UseBranchLpePriceGroupId = 1
		SET @LpePriceGroupId = NULL
	END
	ELSE
	BEGIN
		SET @UseBranchLpePriceGroupId = 0
	END
	
	DECLARE @UseCorrespondentBranchLpePriceGroupId  bit
	IF @CorrespondentLpePriceGroupId IS NOT NULL AND @CorrespondentLpePriceGroupId = '00000000-0000-0000-0000-000000000000'
	BEGIN
		SET @UseCorrespondentBranchLpePriceGroupId = 1
		SET @CorrespondentLpePriceGroupId = NULL
	END
	ELSE
	BEGIN
		SET @UseCorrespondentBranchLpePriceGroupId = 0
	END
	
	DECLARE @UseMiniCorrespondentBranchLpePriceGroupId  bit
	IF @MiniCorrespondentLpePriceGroupId IS NOT NULL AND @MiniCorrespondentLpePriceGroupId = '00000000-0000-0000-0000-000000000000'
	BEGIN
		SET @UseMiniCorrespondentBranchLpePriceGroupId = 1
		SET @MiniCorrespondentLpePriceGroupId = NULL
	END
	ELSE
	BEGIN
		SET @UseMiniCorrespondentBranchLpePriceGroupId = 0
	END
	
	IF @TpoColorThemeId IS NULL
	BEGIN
		SET @TpoColorThemeId = '00000000-0000-0000-0000-000000000000'
	END

	INSERT INTO Pml_Broker (
	PmlBrokerId
	,BrokerId
	,IsActive
	,NAME
	,Addr
	,City
	,STATE
	,Zip
	,Phone
	,Fax
	,LicenseXmlContent
	,CompanyId
	,PrincipalName1
	,PrincipalName2
	,NmLsIdentifier
	,OriginatorCompensationAuditXml
	,OriginatorCompensationPercent
	,OriginatorCompensationBaseT
	,OriginatorCompensationMinAmount
	,OriginatorCompensationMaxAmount
	,OriginatorCompensationFixedAmount
	,OriginatorCompensationNotes
	,OriginatorCompensationLastModifiedDate
	,CustomPricingPolicyField1
	,CustomPricingPolicyField2
	,CustomPricingPolicyField3
	,CustomPricingPolicyField4
	,CustomPricingPolicyField5
	,Lender_TPO_LandingPageConfigId
	,NmlsNameLckd
	,NmlsName
	,UseBranchLpePriceGroupId
	,LpePriceGroupId
	,BranchId
	,PmlCompanyTierId
	,CorrespondentTPOPortalConfigID
	,CorrespondentLpePriceGroupId
	,CorrespondentBranchId
	,MiniCorrespondentTPOPortalConfigID
	,MiniCorrespondentLpePriceGroupId
	,MiniCorrespondentBranchId
	,Roles
	,UnderwritingAuthority
	,UseCorrespondentBranchLpePriceGroupId
	,UseMiniCorrespondentBranchLpePriceGroupId
	,PMLNavigationPermissions
	,BrokerRoleStatusT
	,MiniCorrRoleStatusT
	,CorrRoleStatusT
	,UseDefaultWholesalePriceGroupId
	,UseDefaultWholesaleBranchId
	,UseDefaultMiniCorrPriceGroupId
	,UseDefaultMiniCorrBranchId
	,UseDefaultCorrPriceGroupId
	,UseDefaultCorrBranchId
	,OriginatorCompensationIsOnlyPaidForFirstLienOfCombo
	,TpoColorThemeId
	,TaxIdEncrypted
	,EncryptionKeyId
	,IndexNumber
	)
VALUES (
	@PmlBrokerId
	,@BrokerId
	,@IsActive
	,@Name
	,@Addr
	,@City
	,@State
	,@Zip
	,@Phone
	,@Fax
	,@LicenseXmlContent
	,@CompanyId
	,@PrincipalName1
	,@PrincipalName2
	,@NmLsIdentifier
	,@OriginatorCompensationAuditXml
	,@OriginatorCompensationPercent
	,@OriginatorCompensationBaseT
	,@OriginatorCompensationMinAmount
	,@OriginatorCompensationMaxAmount
	,@OriginatorCompensationFixedAmount
	,@OriginatorCompensationNotes
	,@OriginatorCompensationLastModifiedDate
	,@CustomPricingPolicyField1
	,@CustomPricingPolicyField2
	,@CustomPricingPolicyField3
	,@CustomPricingPolicyField4
	,@CustomPricingPolicyField5
	,@Lender_TPO_LandingPageConfigId
	,@NmlsNameLckd
	,@NmlsName
	,@UseBranchLpePriceGroupId
	,@LpePriceGroupId
	,@BranchId
	,@PmlCompanyTierId
	,@CorrespondentTPOPortalConfigID
	,@CorrespondentLpePriceGroupId
	,@CorrespondentBranchId
	,@MiniCorrespondentTPOPortalConfigID
	,@MiniCorrespondentLpePriceGroupId
	,@MiniCorrespondentBranchId
	,@Roles
	,@UnderwritingAuthority
	,@UseCorrespondentBranchLpePriceGroupId
	,@UseMiniCorrespondentBranchLpePriceGroupId
	,@PMLNavigationPermissions
	,@BrokerRoleStatusT
	,@MiniCorrRoleStatusT
	,@CorrRoleStatusT
	,@UseDefaultWholesalePriceGroupId
	,@UseDefaultWholesaleBranchId
	,@UseDefaultMiniCorrPriceGroupId
	,@UseDefaultMiniCorrBranchId
	,@UseDefaultCorrPriceGroupId
	,@UseDefaultCorrBranchId
	,@OriginatorCompensationIsOnlyPaidForFirstLienOfCombo
	,@TpoColorThemeId
	,@TaxIdEncrypted
	,@EncryptionKeyId
	,@IndexNumber
	)
END