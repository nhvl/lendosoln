-- =============================================
-- Author:		Michael Leinweaver
-- Create date: 5/7/2018
-- Description:	Gets the version of an edoc.
-- =============================================
CREATE PROCEDURE [dbo].[EDOCS_GetDocumentVersionById]
	@DocumentId uniqueidentifier, 
	@BrokerId uniqueidentifier
AS
BEGIN
	SELECT Version
	FROM EDOCS_DOCUMENT 
	WHERE DocumentId = @DocumentId AND BrokerId = @BrokerId
END
