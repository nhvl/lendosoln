ALTER PROCEDURE [dbo].[TITLE_FRAMEWORK_LENDER_SERVICE_Insert]
    @LenderServiceId int OUTPUT,
    @PublicId uniqueidentifier,
    @BrokerId uniqueidentifier,
    @VendorId int,
    @VendorName varchar(200),
    @IsNonInteractiveQuoteEnabled bit,
    @IsInteractiveQuoteEnabled bit,
    @IsPolicyEnabled bit,
    @ConfigurationFileDbKey uniqueidentifier = null,
    @IsEnabledForLqbUsers bit,
    @EnabledEmployeeGroupId uniqueidentifier = null,
    @IsEnabledForTpoUsers bit,
    @EnabledOcGroupId uniqueidentifier = null,
    @UsesProviderCodes bit
AS BEGIN
    INSERT INTO dbo.TITLE_FRAMEWORK_LENDER_SERVICE
        (PublicId,
        BrokerId,
        VendorId,
        VendorName,
        IsNonInteractiveQuoteEnabled,
        IsInteractiveQuoteEnabled,
        IsPolicyEnabled,
        ConfigurationFileDbKey,
        IsEnabledForLqbUsers,
        EnabledEmployeeGroupId,
        IsEnabledForTpoUsers,
        EnabledOcGroupId,
        UsesProviderCodes)
    VALUES
        (@PublicId,
        @BrokerId,
        @VendorId,
        @VendorName,
        @IsNonInteractiveQuoteEnabled,
        @IsInteractiveQuoteEnabled,
        @IsPolicyEnabled,
        @ConfigurationFileDbKey,
        @IsEnabledForLqbUsers,
        @EnabledEmployeeGroupId,
        @IsEnabledForTpoUsers,
        @EnabledOcGroupId,
        @UsesProviderCodes)
    SET @LenderServiceId = SCOPE_IDENTITY();
    RETURN;
END

