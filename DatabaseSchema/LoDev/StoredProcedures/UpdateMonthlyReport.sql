create procedure [dbo].UpdateMonthlyReport
	@LastRun as DateTime,
	@NextRun as DateTime,
	@ReportId as int
as
update MONTHLY_REPORTS 
set LastRun = @LastRun,
	NextRun = @NextRun
where ReportId = @ReportId;