ALTER PROCEDURE [dbo].[DOCCONDITION_GetByLoanConditionHeavy] 
	-- Add the parameters for the stored procedure here
	@LoanId uniqueidentifier,
	@TaskId varchar(10)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
 
    -- Insert statements for procedure here
	SELECT TED.sLId, TED.TaskId, TED.DocumentId, TED.Status as Status, VVED.Status as DocumentStatus, doctype.FolderName, doctype.DocTypeName, T.TaskSubject, t.condrowid,  VVED.DocTypeId
	FROM TASK_x_EDOCS_DOCUMENT TED
	
	JOIN EDOCS_DOCUMENT VVED  ON TED.DocumentId = VVED.DocumentID
	
		join VIEW_EDOCS_DOCTYPE_FOLDER doctype on doctype.doctypeid = VVED.doctypeid
		
	JOIN TASK T 
		ON VVED.BrokerId = T.BrokerId
		AND TED.sLId = T.LoanId
		AND TED.TaskId = T.TaskId
		AND T.CondIsDeleted = 0
	WHERE TED.sLId = @LoanId
	AND TED.TaskId = @TaskId
	
END