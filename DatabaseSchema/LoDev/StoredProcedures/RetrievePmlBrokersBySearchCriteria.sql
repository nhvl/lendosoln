-- =============================================
-- Author:		Isaac Ribakoff
-- Create date: 1/19/2015
-- Description:	Obtains a collection of Originating Companies with matching filter criteria
-- =============================================
ALTER PROCEDURE [dbo].[RetrievePmlBrokersBySearchCriteria] 
	@BrokerId UniqueIdentifier = null,
	@AllowPartialMatching bit = 0,	
	@Name varchar(100) = null,
	@CompanyId varchar(36) = null,
	@Addr varchar(60) = null,
	@PrincipalName1 varchar(100) = null,
	@PrincipalName2 varchar(100) = null,
	@NmlsIdentifier varchar(50) = null
AS
BEGIN
	SELECT top(25) 
		p.*, 
		b.BranchNm, 
		l.LpePriceGroupName,
		perm.CanApplyForIneligibleLoanPrograms, 
		perm.CanRunPricingEngineWithoutCreditReport, 
		perm.CanSubmitWithoutCreditReport, 
		perm.CanAccessTotalScorecard, 
		perm.AllowOrder4506T, 
		perm.TaskRelatedEmailOptionT,
		dbo.GetPmlBrokerRelationshipsAsXml(p.BrokerId, p.PmlBrokerId) as 'RelationshipXml'
	FROM Pml_Broker p WITH(NOLOCK) 
	LEFT OUTER JOIN PML_BROKER_PERMISSIONS perm WITH(NOLOCK) ON p.PmlBrokerId = perm.PmlBrokerId
	LEFT OUTER JOIN BRANCH b WITH(NOLOCK) ON p.BranchId = b.BranchId
	LEFT OUTER JOIN LPE_PRICE_GROUP l WITH(NOLOCK) ON p.LpePriceGroupId = l.LpePriceGroupId
	WHERE p.BrokerId = @BrokerId
	AND 
	(	
		(
			@AllowPartialMatching = 0
			AND ((@Name is null) OR (@Name = Name))
			AND ((@CompanyId is null) OR (@CompanyId = CompanyId))
			AND ((@Addr is null) OR (@Addr = Addr))
			AND ((@PrincipalName1 is null) OR (@PrincipalName1 = PrincipalName1))
			AND ((@PrincipalName2 is null) OR (@PrincipalName2 = PrincipalName2))
			AND ((@NmlsIdentifier is null) OR (@NmlsIdentifier = p.NmlsIdentifier))
		)
		OR
		(
			@AllowPartialMatching = 1
			AND ((@Name is null) OR (Name like '%'+@Name+'%'))
			AND ((@CompanyId is null) OR (CompanyId like '%'+@CompanyId+'%'))
			AND ((@Addr is null) OR (Addr like '%'+@Addr+'%'))
			AND ((@PrincipalName1 is null) OR (PrincipalName1 like '%'+@PrincipalName1+'%'))
			AND ((@PrincipalName2 is null) OR (PrincipalName2 like '%'+@PrincipalName2+'%'))
			AND ((@NmlsIdentifier is null) OR (p.NmlsIdentifier like '%'+@NmlsIdentifier+'%'))
		)
	)
END
