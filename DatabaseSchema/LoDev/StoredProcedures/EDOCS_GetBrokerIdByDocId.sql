-- =============================================
-- Author:		David Dao
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE dbo.EDOCS_GetBrokerIdByDocId 
    @DocumentId UniqueIdentifier
AS
BEGIN

    SELECT BrokerId FROM EDOCS_DOCUMENT
    WHERE DocumentId = @DocumentId


END
