CREATE PROCEDURE [dbo].[INTERNAL_USE_ONLY_DeleteLostDocsWithStandardDocTypes]
as

delete from EDOCS_LOST_DOCUMENT

where
doctypeid in
(
	select doctypeid from edocs_document_type where brokerid is null
)


