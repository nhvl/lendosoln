-- =============================================
-- Author:		Michael Leinweaver
-- Create date: 10/27/2015
-- Description:	Lists the name, description, and size threshold values for all queues that
--				have threshold monitoring.
-- =============================================
ALTER PROCEDURE [dbo].[QueueMonitorThreshold_ListAutoEscalationQueues]
AS
BEGIN
	SELECT *
	FROM QUEUE_MONITOR_THRESHOLD qmt
END
