-- =============================================
-- Author:		Brian Beery
-- Create date: 08/08/2014
-- Description:	Use the lender's BrokerId to lookup their ComplianceEagle Company ID.
-- =============================================
CREATE PROCEDURE [dbo].[GetComplianceEagleCompanyIdByBrokerId]
	-- Add the parameters for the stored procedure here
	@BrokerId Guid
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT ComplianceEagleCompanyID FROM Broker WITH(nolock)
	WHERE BrokerId=@BrokerId
END
