-- =============================================
-- Author:		Michael Leinweaver
-- Create date: 9/21/2018
-- Description:	Retrieves a PDF generation job by public job ID.
-- =============================================
ALTER PROCEDURE [dbo].[PDF_GENERATION_JOBS_GetJobByPublicId]
	@PublicJobId uniqueidentifier
AS
BEGIN
	SELECT 
		j.JobId,
		j.ProcessingStatus,
		j.NextRunDate,
		j.PublicJobId,
		j.DateCreated,
		j.ExpirationDate,
		j.LastRunStartDate,
		j.MaxExceptionCount,
		j.ExceptionCount,
		j.DateCompleted,
		j.LoanId,
		j.CorrelationId,
		p.BrokerId,	-- 11/2/18 - je - For backwards compatibility we get BrokerId and UserId from PDF_GENERATION_JOBS table.
		p.UserId, 	-- We should be able to change this in the future if necessary.
		p.UserType, 
		p.PdfGenerationJobType, 
		p.PdfName, 
		p.SerializedData, 
		p.EncryptionKeyId, 
		p.EncryptedPassword, 
		p.ResultFileDbKey, 
		p.PollingIntervalInSeconds
	FROM 
		PDF_GENERATION_JOBS p JOIN
		BACKGROUND_JOBS j ON p.JobId = j.JobId
	WHERE j.PublicJobId = @PublicJobId
END
