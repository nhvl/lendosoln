-- =============================================
-- Author:		Eric Mallare
-- Create date: 5/2/2017
-- Description:	Updates a Vendor and its linked transmission info.
-- =============================================
CREATE PROCEDURE [dbo].[VERIFICATION_VENDOR_UpdateVendor]
	@CompanyName varchar(150),
	@IsEnabled bit,
	@IsTestVendor bit,
	@CanPayWithCreditCard bit,
	@DuValidationServiceProviderId int = null,
	@PlatformId int = null,
	@IsOverridingPlatform bit,
	@MclCraId char(2),
	@TargetUrl varchar(200),
	@TransmissionAuthenticationT int,
	@TransmissionAssociationT int= null,
	@EncryptionKeyId uniqueidentifier,
	@RequestCertLocation varchar(200) = null,
	@ResponseCertId uniqueidentifier = null,
	@RequestCredentialName varchar(50) = null,
	@RequestCredentialPassword varbinary(max) = null,
	@ResponseCredentialName varchar(50) = null,
	@ResponseCredentialPassword varbinary(max) = null,
	@TransmissionId int,
	@VendorId int,
	@PreserveRequestPass bit = null,
	@PreserveResponsePass bit = null,
	@OutputRequestPass varbinary(max) = null OUTPUT,
	@OutputResponsePass varbinary(max) = null OUTPUT
AS
BEGIN TRANSACTION
	UPDATE
		VERIFICATION_TRANSMISSION
	SET
		TargetUrl=@TargetUrl, TransmissionAuthenticationT=@TransmissionAuthenticationT, RequestCertLocation=@RequestCertLocation,
		ResponseCertId=@ResponseCertId, RequestCredentialName=@RequestCredentialName, ResponseCredentialName=@ResponseCredentialName,
		EncryptionKeyId = @EncryptionKeyId,
		RequestCredentialPassword=CASE WHEN @PreserveRequestPass IS NOT NULL AND @PreserveRequestPass='1' THEN COALESCE(@RequestCredentialPassword, RequestCredentialPassword) ELSE @RequestCredentialPassword END,
		ResponseCredentialPassword=CASE WHEN @PreserveResponsePass IS NOT NULL AND @PreserveResponsePass='1' THEN COALESCE(@ResponseCredentialPassword, ResponseCredentialPassword) ELSE @ResponseCredentialPassword END
	WHERE
		TransmissionId=@TransmissionId

	IF @@error!= 0 GOTO HANDLE_ERROR
	
	UPDATE
		VERIFICATION_VENDOR
	SET
		CompanyName=@CompanyName, IsEnabled=@IsEnabled, IsTestVendor=@IsTestVendor, CanPayWithCreditCard=@CanPayWithCreditCard,
		DuValidationServiceProviderId=@DuValidationServiceProviderId, PlatformId=@PlatformId, IsOverridingPlatform=@IsOverridingPlatform,
		MclCraId=@MclCraId
	WHERE
		VendorId=@VendorId
		
	IF @@error!= 0 GOTO HANDLE_ERROR

	IF @PreserveRequestPass IS NOT NULL
	BEGIN
		SET @OutputRequestPass = (SELECT RequestCredentialPassword FROM VERIFICATION_TRANSMISSION WHERE TransmissionId=@TransmissionId);
	END

	IF @PreserveResponsePass IS NOT NULL
	BEGIN
		SET @OutputResponsePass = (SELECT ResponseCredentialPassword FROM VERIFICATION_TRANSMISSION WHERE TransmissionId=@TransmissionId);
	END
COMMIT TRANSACTION
RETURN;

	HANDLE_ERROR:
		ROLLBACK TRANSACTION
		RAISERROR('Error in VERIFICATION_VENDOR_UpdateVendor sp', 16, 1);;
		RETURN;
