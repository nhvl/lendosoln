CREATE PROCEDURE DeleteAllBrokerCRA 
	@BrokerID Guid
AS
DELETE FROM CreditReportProtocol WHERE OwnerID = @BrokerID
