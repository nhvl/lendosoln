-- =============================================
-- Author:		David Dao
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE dbo.ALL_USER_CLIENT_CERTIFICATE_List
    @UserId UniqueIdentifier
AS
BEGIN

    SELECT CertificateId, Description, CreatedDate, CreatedBy, a.LoginNm AS CreatedByUserName 
    FROM ALL_USER_CLIENT_CERTIFICATE c JOIN ALL_USER a ON c.CreatedBy = a.UserId
    WHERE c.UserId = @UserId 
    ORDER BY CreatedDate DESC

END
