-- =============================================
-- Author:		Paolo Arrastia
-- Create date: 24 Jul 2012
-- Description:	
-- =============================================
ALTER PROCEDURE [dbo].[RateMonitorNew] 
	-- Add the parameters for the stored procedure here
	@sLId UniqueIdentifier,
	@rate decimal(9,3),
	@point decimal(9,3),
	@email varchar(200),
	@bpdesc varchar(100),
	@currDate DateTime,
	@createdBy UniqueIdentifier,
	@createdByType char(1),
	@doQP2FileUpdate bit,
	@rateMonitorScenarioName varchar(1024),
	@brokerID uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @ErrorCode int
	DECLARE @ErrorMsg varchar(1024)
	DECLARE @RowCount int
	
	SET @ErrorCode = -1
	SET @ErrorMsg = 'unknown'

	IF (@doQP2FileUpdate = 1)
	BEGIN
		BEGIN transaction
			UPDATE
				QUICKPRICER_2_LOAN_USAGE
			SET 
				RateMonitorName = @rateMonitorScenarioName
			WHERE 
					Loanid = @slid
				and Isinuse = 1
				and Brokerid = @BrokerID
				and Useridofusinguser = @createdBy
			SELECT @ErrorCode = @@Error, @RowCount = @@RowCount
			IF 0 <> @ErrorCode 
			BEGIN
				SET @ErrorMsg = 'An error occurred when updating the rate monitor name.';
				GOTO ErrorHandler;
			END
			IF 0 = @RowCount
			BEGIN
				SET @ErrorCode = -2
				SET @ErrorMsg = 'Could not find specified loan.'
				GOTO ErrorHandler;
			END	
			
			INSERT RATE_MONITOR(sLId, Rate, Point, EmailAddresses, BPDesc, CreatedDate, DisabledReason, IsEnabled, CreatedByUserId, CreatedByUserType)
			VALUES (@sLId, @rate, @point, @email, @bpdesc, @currDate, '', 1, @createdBy, @createdByType);
			IF 0 <> @@Error
			BEGIN
				SET @ErrorMsg = 'An error occurred when inserting the rate monitor.';
				GOTO ErrorHandler;
			END
			
		COMMIT transaction
		RETURN 0;
		ErrorHandler:
			ROLLBACK transaction
			RAISERROR(@ErrorMsg, 16, 1);
			RETURN @ErrorCode
	END
	ELSE
	BEGIN
		INSERT RATE_MONITOR(sLId, Rate, Point, EmailAddresses, BPDesc, CreatedDate, DisabledReason, IsEnabled, CreatedByUserId, CreatedByUserType)
		VALUES (@sLId, @rate, @point, @email, @bpdesc, @currDate, '', 1, @createdBy, @createdByType);
    END
END
