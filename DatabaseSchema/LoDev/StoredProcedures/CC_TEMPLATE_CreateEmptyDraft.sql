
CREATE PROCEDURE [dbo].[CC_TEMPLATE_CreateEmptyDraft] 
	@BrokerId uniqueidentifier,
	@UserId uniqueidentifier,
	@Description varchar(200)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @Error varchar(200)
	
	BEGIN TRANSACTION 
		
		EXEC CC_TEMPLATE_DeleteDraft @BrokerId
	
		IF @@ERROR <> 0	BEGIN
			SET @Error = 'Could not delete existing draft'
			GOTO ERROR
		END
	
	 	INSERT INTO CC_TEMPLATE_SYSTEM( BrokerId, Description, DraftOpenedD, DraftOpenedByUserId, ReleasedD, ReleasedByUserId,RuleXml )
			VALUES( @BrokerId, @Description, GETDATE(), @UserId, NULL, NULL, '')
		
		IF @@ERROR <> 0	BEGIN
			SET @Error = 'Could not create new cc system'
			GOTO ERROR
		END
	
	COMMIT TRANSACTION 
	RETURN
	
		
	ERROR:
		ROLLBACK TRANSACTION
		RAISERROR (@Error, 16, 1)
	
END
