-- =============================================
-- Author:		Justin Jia
-- ALTER date: June 14, 2012
-- Description:	Get all parameters of the stored procedure by its name.
-- =============================================
CREATE PROCEDURE [dbo].[Justin_Testing_GetParameterNamesByStoredProcedureName]
	-- Add the parameters for the stored procedure here
	@spName varchar(200) = ''
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select name from sys.parameters
	WHERE object_id = OBJECT_ID(@spName)
END
