CREATE PROCEDURE SubscribeFeature
	@BrokerID Guid,
	@FeatureID Guid,
	@UserID Guid 
AS
DECLARE @tranPoint SYSNAME
SET @tranPoint = OBJECT_NAME(@@procId) + CAST(@@NESTLEVEL AS VARCHAR(10))
BEGIN TRANSACTION
SAVE TRANSACTION @tranPoint
	INSERT INTO Feature_Subscription(BrokerID, FeatureID) VALUES(@BrokerID, @FeatureID)
	IF (0!=@@error)
	BEGIN
		RAISERROR('Error in inserting into table Feature_Subscription  in SubscribeFeature sp', 16, 1);
		GOTO ErrorHandler;
	END
	INSERT INTO Usage_Event (FeatureID, BrokerId, UserID, EventType, WhoDoneIt) VALUES (@FeatureID, @BrokerID, '00000000-0000-0000-0000-000000000000', 's', @UserID)
	IF (0!=@@error)
	BEGIN
		RAISERROR('Error in inserting into table Usage_Event  in SubscribeFeature sp', 16, 1);
		GOTO ErrorHandler;
	END
COMMIT TRANSACTION
RETURN 0
ErrorHandler:
	ROLLBACK TRANSACTION @tranPoint
	rollback TRANSACTION 
	RETURN -100
