CREATE PROCEDURE [dbo].[CP_RetrieveConsumerLastLoggedInByEmailBroker]
	@Email varchar(80),
	@BrokerId uniqueidentifier
AS

SELECT LastLoggedInD
FROM CP_CONSUMER
WHERE	BrokerId = @BrokerId	AND
		Email = @Email