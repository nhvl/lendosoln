-- =============================================
-- Author:		Justin Lara
-- Create date: 3/27/2018
-- Description:	Saves a Cenlar configuration.
-- =============================================
ALTER PROCEDURE [dbo].[CENLAR_LENDER_CONFIGURATION_Save]
	@BrokerId uniqueidentifier,
	@EnableAutomaticTransmissions bit,
	@DummyUserId uniqueidentifier,
	@CustomReportName varchar(250),
	@BatchExportName varchar(250),
	@NotificationEmails varchar(500),
	@ShouldSuppressAcceptedLoanNotifications bit,
	@RemoveProblematicPayloadCharacters bit
AS
BEGIN
	UPDATE CENLAR_LENDER_CONFIGURATION
	SET
		EnableAutomaticTransmissions = @EnableAutomaticTransmissions,
		DummyUserId = @DummyUserId,
		CustomReportName = @CustomReportName,
		BatchExportName = @BatchExportName,
		NotificationEmails = @NotificationEmails,
		ShouldSuppressAcceptedLoanNotifications = @ShouldSuppressAcceptedLoanNotifications,
		RemoveProblematicPayloadCharacters = @RemoveProblematicPayloadCharacters
	WHERE BrokerId = @BrokerId
	
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO CENLAR_LENDER_CONFIGURATION
		(
			BrokerId,
			EnableAutomaticTransmissions,
			DummyUserId,
			CustomReportName,
			BatchExportName,
			NotificationEmails,
			ShouldSuppressAcceptedLoanNotifications,
			RemoveProblematicPayloadCharacters
		)
		VALUES
		(
			@BrokerId,
			@EnableAutomaticTransmissions,
			@DummyUserId,
			@CustomReportName,
			@BatchExportName,
			@NotificationEmails,
			@ShouldSuppressAcceptedLoanNotifications,
			@RemoveProblematicPayloadCharacters
		)
	END
END
