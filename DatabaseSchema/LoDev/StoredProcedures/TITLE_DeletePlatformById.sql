-- =============================================
-- Author:		Eric Mallare
-- Create date: 8/16/2017
-- Description:	Deletes a title platform
-- =============================================
ALTER PROCEDURE [dbo].[TITLE_DeletePlatformById]
	@PlatformId int
AS
BEGIN TRANSACTION
	DECLARE @TransmissionInfoId int
	SET @TransmissionInfoId = (SELECT p.TransmissionInfoId FROM TITLE_FRAMEWORK_PLATFORM p WHERE p.PlatformId=@PlatformId)
	
		DELETE FROM
			TITLE_FRAMEWORK_PLATFORM
		WHERE
			PlatformId=@Platformid
		
		DELETE FROM
			TITLE_FRAMEWORK_TRANSMISSION_INFO
		WHERE	
			TransmissionInfoId=@TransmissionInfoId
		
		if( 0!=@@error)
		BEGIN
			ROLLBACK TRANSACTION
			RAISERROR('Error in delete in TITLE_DeletePlatformById sp', 16, 1);
			RETURN -100;
		END
COMMIT TRANSACTION
