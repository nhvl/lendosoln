/****** 
Object:  StoredProcedure [dbo].[Feature_Subscription_ListByBrokerId]    
Author: Scott Kibler
Script Date: 10/21/2016 13:31:51 
Description: Finds all feature_subscription for given broker.  Used for export.
Notes: Feature is shared, Feature_Subscription is not.  Do not join the two.
******/
ALTER PROCEDURE [dbo].[Feature_Subscription_ListByBrokerId]
	@BrokerID uniqueidentifier
AS
	SELECT 
		FeatureId, IsActive
	FROM 
		FEATURE_SUBSCRIPTION
	WHERE 
		BrokerId = @BrokerID



