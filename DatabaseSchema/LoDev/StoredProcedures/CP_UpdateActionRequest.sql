






CREATE  PROCEDURE [dbo].[CP_UpdateActionRequest]
@ConsumerActionRequestId bigint
, @sLId uniqueidentifier = NULL
, @aAppId uniqueidentifier = NULL
, @aBFullName varchar(50) = NULL
, @aCFullName varchar(50) = NULL
, @ResponseStatus tinyint = NULL
, @DocTypeId int = NULL
, @Description varchar(100) = NULL
, @PdfFileDbKey uniqueidentifier = NULL
, @PdfMetaDataSnapshot varchar(max) = NULL
, @IsApplicableToBorrower bit = NULL
, @IsApplicableToCoborrower bit = NULL
, @IsSignatureRequested bit = NULL
, @IsESignAllowed bit = NULL

AS
UPDATE CP_CONSUMER_ACTION_REQUEST 
SET
sLId = COALESCE(@sLId, sLId)
, aAppId = COALESCE(@aAppId, aAppId )
, aBFullName = COALESCE(@aBFullName, aBFullName )
, aCFullName = COALESCE(@aCFullName, aCFullName )
, ResponseStatus  = COALESCE(@ResponseStatus, ResponseStatus )
, DocTypeId  = COALESCE(@DocTypeId, DocTypeId )
, Description  = COALESCE(@Description, Description )
, PdfFileDbKey  = COALESCE(@PdfFileDbKey, PdfFileDbKey )
, PdfMetaDataSnapshot  = COALESCE(@PdfMetaDataSnapshot, PdfMetaDataSnapshot )
, IsApplicableToBorrower  = COALESCE(@IsApplicableToBorrower, IsApplicableToBorrower )
, IsApplicableToCoborrower = COALESCE(@IsApplicableToCoborrower, IsApplicableToCoborrower )
, IsSignatureRequested = COALESCE(@IsSignatureRequested, IsSignatureRequested )
, IsESignAllowed = COALESCE(@IsESignAllowed, IsESignAllowed )

WHERE ConsumerActionRequestId = @ConsumerActionRequestId

IF ( 0 != @@ERROR )
BEGIN
	RAISERROR('Error updating CP_CONSUMER_ACTION_REQUEST  table in CP_UpdateActionRequest sp', 16, 1);
	RETURN -100;
END
RETURN 0;







