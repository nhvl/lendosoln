CREATE PROCEDURE RetrieveAgreementAcceptanceHistoryByUserID 
	@UserID Guid
AS
SELECT AgreementAcceptanceHistory FROM All_User WHERE UserID = @UserID
