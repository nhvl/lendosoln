-- =============================================
-- Author:		Timothy Jewell
-- Create date: 03/27/13
-- Description:	Selects sLNm, AppId to be used in UDN Order service based on order status and loan status dates.
-- =============================================
ALTER PROCEDURE [dbo].[GetAppsForUDNActivationDeactivation]
	-- Add the parameters for the stored procedure here
	@sBrokerId uniqueidentifier,
	@sBranchId uniqueidentifier,
	@sBranchChannelT int,
	@IsActivation bit,
	--@Include_sStatusD refers to whether sStatusD is included in orders for UDN Activation.
	--1 means sStatusD is a C_Status (all true triggering Activation)
	--0 means sStatusD is a D_Status (one or more true triggering Deactivation)
	--NULL means sStatusD is ignored
	@Include_sLeadD bit = NULL,
	@Include_sOpenedD bit = NULL,
	@Include_sPreQualD bit = NULL,
	@Include_sSubmitD bit = NULL,
	@Include_sPreProcessingD bit = NULL,
	@Include_sProcessingD bit = NULL,
	@Include_sDocumentCheckD bit = NULL,
	@Include_sDocumentCheckFailedD bit = NULL,
	@Include_sLoanSubmittedD bit = NULL,
	@Include_sPreUnderwritingD bit = NULL,
	@Include_sUnderwritingD bit = NULL,
	@Include_sPreApprovD bit = NULL,
	@Include_sEstCloseD bit = NULL,
	@Include_sApprovD bit = NULL,
	@Include_sConditionReviewD bit = NULL,
	@Include_sFinalUnderwritingD bit = NULL,
	@Include_sPreDocQCD bit = NULL,
	@Include_sClearToCloseD bit = NULL,
	@Include_sDocsOrderedD bit = NULL,
	@Include_sDocsDrawnD bit = NULL,
	@Include_sDocsD bit = NULL,
	@Include_sDocsBackD bit = NULL,
	@Include_sFundingConditionsD bit = NULL,
	@Include_sFundD bit = NULL,
	@Include_sRecordedD bit = NULL,
	@Include_sFinalDocsD bit = NULL,
	@Include_sClosedD bit = NULL,
	@Include_sSubmittedForPurchaseReviewD bit = NULL,
	@Include_sInPurchaseReviewD bit = NULL,
	@Include_sPrePurchaseConditionsD bit = NULL,
	@Include_sSubmittedForFinalPurchaseD bit = NULL,
	@Include_sInFinalPurchaseReviewD bit = NULL,
	@Include_sClearToPurchaseD bit = NULL,
	@Include_sPurchasedD bit = NULL,
	@Include_sReadyForSaleD bit = NULL,
	@Include_sShippedToInvestorD bit = NULL,
	@Include_sSuspendedByInvestorD bit = NULL,
	@Include_sCondSentToInvestorD bit = NULL,
	@Include_sAdditionalCondSentD bit = NULL,
	@Include_sLPurchaseD bit = NULL,
	@Include_sCounterOfferD bit = NULL,
	@Include_sOnHoldD bit = NULL,
	@Include_sCanceledD bit = NULL,
	@Include_sSuspendedD bit = NULL,
	@Include_sRejectD bit = NULL,
	@Include_sWithdrawnD bit = NULL,
	@Include_sArchivedD bit = NULL,
	@AcceptableLoanTypeXml xml = NULL,
	@AcceptableLoanPurposeXml xml = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DECLARE @AcceptableLoanTypes TABLE(sLT int)
	
	IF @IsActivation = 1 AND @AcceptableLoanTypeXml IS NOT NULL
	BEGIN
		INSERT INTO @AcceptableLoanTypes(sLT)
		SELECT T.Item.value('.', 'int')
		FROM @AcceptableLoanTypeXml.nodes('root/item') as T(Item)
	END
	
	DECLARE @AcceptableLoanPurposes TABLE(sLPurposeT int)
	
	IF @IsActivation = 1 AND @AcceptableLoanPurposeXml IS NOT NULL
	BEGIN
		INSERT INTO @AcceptableLoanPurposes(sLPurposeT)
		SELECT T.Item.value('.', 'int')
		FROM @AcceptableLoanPurposeXml.nodes('root/item') as T(Item)
	END
 
    -- Insert statements for procedure here
	SELECT aAppId, sLNm
	FROM APPLICATION_B b WITH (NOLOCK)
		INNER JOIN LOAN_FILE_CACHE lfc WITH (NOLOCK) ON lfc.sLId = b.sLId
		INNER JOIN LOAN_FILE_CACHE_2 lfc2 WITH (NOLOCK) ON lfc.sLId = lfc2.sLId
		INNER JOIN LOAN_FILE_CACHE_3 lfc3 WITH (NOLOCK) ON lfc.sLId = lfc3.sLId
	WHERE	lfc.IsValid = 1
		AND lfc.IsTemplate = 0
		AND (lfc.sLoanFileT = 0 OR lfc.sLoanFileT IS NULL)
		AND lfc.sBrokerId = @sBrokerId
		AND lfc.sBranchId = @sBranchId
		AND lfc2.sBranchChannelT = @sBranchChannelT
		AND aUDNDeactivatedD IS NULL
		AND
	( --UDN Activation
		(@IsActivation = 1 AND aUDNOrderedD IS NULL
			AND (@AcceptableLoanTypeXml IS NULL OR lfc.sLT IN (SELECT sLT FROM @AcceptableLoanTypes))
			AND (@AcceptableLoanPurposeXml IS NULL OR lfc.sLPurposeT IN (SELECT sLPurposeT FROM @AcceptableLoanPurposes))
			AND (lfc.sLienPosT = 0 /* First Lien */ OR lfc3.sLinkedLoanName IS NULL OR lfc3.sLinkedLoanName = '') -- activation only considers first liens or stand-alone seconds
			AND (@Include_sLeadD IS NULL OR (@Include_sLeadD = 1 AND sLeadD IS NOT NULL) OR (@Include_sLeadD = 0 AND sLeadD IS NULL))
			AND (@Include_sOpenedD IS NULL OR (@Include_sOpenedD = 1 AND sOpenedD IS NOT NULL) OR (@Include_sOpenedD = 0 AND sOpenedD IS NULL))
			AND (@Include_sPreQualD IS NULL OR (@Include_sPreQualD = 1 AND sPreQualD IS NOT NULL) OR (@Include_sPreQualD = 0 AND sPreQualD IS NULL))
			AND (@Include_sSubmitD IS NULL OR (@Include_sSubmitD = 1 AND sSubmitD IS NOT NULL) OR (@Include_sSubmitD = 0 AND sSubmitD IS NULL))
			AND (@Include_sPreProcessingD IS NULL OR (@Include_sPreProcessingD = 1 AND sPreProcessingD IS NOT NULL) OR (@Include_sPreProcessingD = 0 AND sPreProcessingD IS NULL))
			AND (@Include_sProcessingD IS NULL OR (@Include_sProcessingD = 1 AND sProcessingD IS NOT NULL) OR (@Include_sProcessingD = 0 AND sProcessingD IS NULL))
			AND (@Include_sDocumentCheckD IS NULL OR (@Include_sDocumentCheckD = 1 AND sDocumentCheckD IS NOT NULL) OR (@Include_sDocumentCheckD = 0 AND sDocumentCheckD IS NULL))
			AND (@Include_sDocumentCheckFailedD IS NULL OR (@Include_sDocumentCheckFailedD = 1 AND sDocumentCheckFailedD IS NOT NULL) OR (@Include_sDocumentCheckFailedD = 0 AND sDocumentCheckFailedD IS NULL))
			AND (@Include_sLoanSubmittedD IS NULL OR (@Include_sLoanSubmittedD = 1 AND sLoanSubmittedD IS NOT NULL) OR (@Include_sLoanSubmittedD = 0 AND sLoanSubmittedD IS NULL))
			AND (@Include_sPreUnderwritingD IS NULL OR (@Include_sPreUnderwritingD = 1 AND sPreUnderwritingD IS NOT NULL) OR (@Include_sPreUnderwritingD = 0 AND sPreUnderwritingD IS NULL))
			AND (@Include_sUnderwritingD IS NULL OR (@Include_sUnderwritingD = 1 AND sUnderwritingD IS NOT NULL) OR (@Include_sUnderwritingD = 0 AND sUnderwritingD IS NULL))
			AND (@Include_sPreApprovD IS NULL OR (@Include_sPreApprovD = 1 AND sPreApprovD IS NOT NULL) OR (@Include_sPreApprovD = 0 AND sPreApprovD IS NULL))
			AND (@Include_sEstCloseD IS NULL OR (@Include_sEstCloseD = 1 AND sEstCloseD IS NOT NULL) OR (@Include_sEstCloseD = 0 AND sEstCloseD IS NULL))
			AND (@Include_sApprovD IS NULL OR (@Include_sApprovD = 1 AND sApprovD IS NOT NULL) OR (@Include_sApprovD = 0 AND sApprovD IS NULL))
			AND (@Include_sConditionReviewD IS NULL OR (@Include_sConditionReviewD = 1 AND sConditionReviewD IS NOT NULL) OR (@Include_sConditionReviewD = 0 AND sConditionReviewD IS NULL))
			AND (@Include_sFinalUnderwritingD IS NULL OR (@Include_sFinalUnderwritingD = 1 AND sFinalUnderwritingD IS NOT NULL) OR (@Include_sFinalUnderwritingD = 0 AND sFinalUnderwritingD IS NULL))
			AND (@Include_sPreDocQCD IS NULL OR (@Include_sPreDocQCD = 1 AND sPreDocQCD IS NOT NULL) OR (@Include_sPreDocQCD = 0 AND sPreDocQCD IS NULL))
			AND (@Include_sClearToCloseD IS NULL OR (@Include_sClearToCloseD = 1 AND sClearToCloseD IS NOT NULL) OR (@Include_sClearToCloseD = 0 AND sClearToCloseD IS NULL))
			AND (@Include_sDocsOrderedD IS NULL OR (@Include_sDocsOrderedD = 1 AND sDocsOrderedD IS NOT NULL) OR (@Include_sDocsOrderedD = 0 AND sDocsOrderedD IS NULL))
			AND (@Include_sDocsDrawnD IS NULL OR (@Include_sDocsDrawnD = 1 AND sDocsDrawnD IS NOT NULL) OR (@Include_sDocsDrawnD = 0 AND sDocsDrawnD IS NULL))
			AND (@Include_sDocsD IS NULL OR (@Include_sDocsD = 1 AND sDocsD IS NOT NULL) OR (@Include_sDocsD = 0 AND sDocsD IS NULL))
			AND (@Include_sDocsBackD IS NULL OR (@Include_sDocsBackD = 1 AND sDocsBackD IS NOT NULL) OR (@Include_sDocsBackD = 0 AND sDocsBackD IS NULL))
			AND (@Include_sFundingConditionsD IS NULL OR (@Include_sFundingConditionsD = 1 AND sFundingConditionsD IS NOT NULL) OR (@Include_sFundingConditionsD = 0 AND sFundingConditionsD IS NULL))
			AND (@Include_sFundD IS NULL OR (@Include_sFundD = 1 AND sFundD IS NOT NULL) OR (@Include_sFundD = 0 AND sFundD IS NULL))
			AND (@Include_sRecordedD IS NULL OR (@Include_sRecordedD = 1 AND sRecordedD IS NOT NULL) OR (@Include_sRecordedD = 0 AND sRecordedD IS NULL))
			AND (@Include_sFinalDocsD IS NULL OR (@Include_sFinalDocsD = 1 AND sFinalDocsD IS NOT NULL) OR (@Include_sFinalDocsD = 0 AND sFinalDocsD IS NULL))
			AND (@Include_sClosedD IS NULL OR (@Include_sClosedD = 1 AND sClosedD IS NOT NULL) OR (@Include_sClosedD = 0 AND sClosedD IS NULL))
			AND (@Include_sSubmittedForPurchaseReviewD IS NULL OR (@Include_sSubmittedForPurchaseReviewD = 1 AND sSubmittedForPurchaseReviewD IS NOT NULL) OR (@Include_sSubmittedForPurchaseReviewD = 0 AND sSubmittedForPurchaseReviewD IS NULL))
			AND (@Include_sInPurchaseReviewD IS NULL OR (@Include_sInPurchaseReviewD = 1 AND sInPurchaseReviewD IS NOT NULL) OR (@Include_sInPurchaseReviewD = 0 AND sInPurchaseReviewD IS NULL))
			AND (@Include_sPrePurchaseConditionsD IS NULL OR (@Include_sPrePurchaseConditionsD = 1 AND sPrePurchaseConditionsD IS NOT NULL) OR (@Include_sPrePurchaseConditionsD = 0 AND sPrePurchaseConditionsD IS NULL))
			AND (@Include_sSubmittedForFinalPurchaseD IS NULL OR (@Include_sSubmittedForFinalPurchaseD = 1 AND sSubmittedForFinalPurchaseD IS NOT NULL) OR (@Include_sSubmittedForFinalPurchaseD = 0 AND sSubmittedForFinalPurchaseD IS NULL))
			AND (@Include_sInFinalPurchaseReviewD IS NULL OR (@Include_sInFinalPurchaseReviewD = 1 AND sInFinalPurchaseReviewD IS NOT NULL) OR (@Include_sInFinalPurchaseReviewD = 0 AND sInFinalPurchaseReviewD IS NULL))
			AND (@Include_sClearToPurchaseD IS NULL OR (@Include_sClearToPurchaseD = 1 AND sClearToPurchaseD IS NOT NULL) OR (@Include_sClearToPurchaseD = 0 AND sClearToPurchaseD IS NULL))
			AND (@Include_sPurchasedD IS NULL OR (@Include_sPurchasedD = 1 AND sPurchasedD IS NOT NULL) OR (@Include_sPurchasedD = 0 AND sPurchasedD IS NULL))
			AND (@Include_sReadyForSaleD IS NULL OR (@Include_sReadyForSaleD = 1 AND sReadyForSaleD IS NOT NULL) OR (@Include_sReadyForSaleD = 0 AND sReadyForSaleD IS NULL))
			AND (@Include_sShippedToInvestorD IS NULL OR (@Include_sShippedToInvestorD = 1 AND sShippedToInvestorD IS NOT NULL) OR (@Include_sShippedToInvestorD = 0 AND sShippedToInvestorD IS NULL))
			AND (@Include_sSuspendedByInvestorD IS NULL OR (@Include_sSuspendedByInvestorD = 1 AND sSuspendedByInvestorD IS NOT NULL) OR (@Include_sSuspendedByInvestorD = 0 AND sSuspendedByInvestorD IS NULL))
			AND (@Include_sCondSentToInvestorD IS NULL OR (@Include_sCondSentToInvestorD = 1 AND sCondSentToInvestorD IS NOT NULL) OR (@Include_sCondSentToInvestorD = 0 AND sCondSentToInvestorD IS NULL))
			AND (@Include_sAdditionalCondSentD IS NULL OR (@Include_sAdditionalCondSentD = 1 AND sAdditionalCondSentD IS NOT NULL) OR (@Include_sAdditionalCondSentD = 0 AND sAdditionalCondSentD IS NULL))
			AND (@Include_sLPurchaseD IS NULL OR (@Include_sLPurchaseD = 1 AND sLPurchaseD IS NOT NULL) OR (@Include_sLPurchaseD = 0 AND sLPurchaseD IS NULL))
			AND (@Include_sCounterOfferD IS NULL OR (@Include_sCounterOfferD = 1 AND sCounterOfferD IS NOT NULL) OR (@Include_sCounterOfferD = 0 AND sCounterOfferD IS NULL))
			AND (@Include_sOnHoldD IS NULL OR (@Include_sOnHoldD = 1 AND sOnHoldD IS NOT NULL) OR (@Include_sOnHoldD = 0 AND sOnHoldD IS NULL))
			AND (@Include_sCanceledD IS NULL OR (@Include_sCanceledD = 1 AND sCanceledD IS NOT NULL) OR (@Include_sCanceledD = 0 AND sCanceledD IS NULL))
			AND (@Include_sSuspendedD IS NULL OR (@Include_sSuspendedD = 1 AND sSuspendedD IS NOT NULL) OR (@Include_sSuspendedD = 0 AND sSuspendedD IS NULL))
			AND (@Include_sRejectD IS NULL OR (@Include_sRejectD = 1 AND sRejectD IS NOT NULL) OR (@Include_sRejectD = 0 AND sRejectD IS NULL))
			AND (@Include_sWithdrawnD IS NULL OR (@Include_sWithdrawnD = 1 AND sWithdrawnD IS NOT NULL) OR (@Include_sWithdrawnD = 0 AND sWithdrawnD IS NULL))
			AND (@Include_sArchivedD IS NULL OR (@Include_sArchivedD = 1 AND sArchivedD IS NOT NULL) OR (@Include_sArchivedD = 0 AND sArchivedD IS NULL))
		)
	OR --UDN Deactivation
		(@IsActivation = 0 AND aUDNOrderedD IS NOT NULL AND (
			   (@Include_sLeadD = 0 AND sLeadD IS NOT NULL)
			OR (@Include_sOpenedD = 0 AND sOpenedD IS NOT NULL)
			OR (@Include_sPreQualD = 0 AND sPreQualD IS NOT NULL)
			OR (@Include_sSubmitD = 0 AND sSubmitD IS NOT NULL)
			OR (@Include_sPreProcessingD = 0 AND sPreProcessingD IS NOT NULL)
			OR (@Include_sProcessingD = 0 AND sProcessingD IS NOT NULL)
			OR (@Include_sDocumentCheckD = 0 AND sDocumentCheckD IS NOT NULL)
			OR (@Include_sDocumentCheckFailedD = 0 AND sDocumentCheckFailedD IS NOT NULL)
			OR (@Include_sLoanSubmittedD = 0 AND sLoanSubmittedD IS NOT NULL)
			OR (@Include_sPreUnderwritingD = 0 AND sPreUnderwritingD IS NOT NULL)
			OR (@Include_sUnderwritingD = 0 AND sUnderwritingD IS NOT NULL)
			OR (@Include_sPreApprovD = 0 AND sPreApprovD IS NOT NULL)
			OR (@Include_sEstCloseD = 0 AND sEstCloseD IS NOT NULL)
			OR (@Include_sApprovD = 0 AND sApprovD IS NOT NULL)
			OR (@Include_sConditionReviewD = 0 AND sConditionReviewD IS NOT NULL)
			OR (@Include_sFinalUnderwritingD = 0 AND sFinalUnderwritingD IS NOT NULL)
			OR (@Include_sPreDocQCD = 0 AND sPreDocQCD IS NOT NULL)
			OR (@Include_sClearToCloseD = 0 AND sClearToCloseD IS NOT NULL)
			OR (@Include_sDocsOrderedD = 0 AND sDocsOrderedD IS NOT NULL)
			OR (@Include_sDocsDrawnD = 0 AND sDocsDrawnD IS NOT NULL)
			OR (@Include_sDocsD = 0 AND sDocsD IS NOT NULL)
			OR (@Include_sDocsBackD = 0 AND sDocsBackD IS NOT NULL)
			OR (@Include_sFundingConditionsD = 0 AND sFundingConditionsD IS NOT NULL)
			OR (@Include_sFundD = 0 AND sFundD IS NOT NULL)
			OR (@Include_sRecordedD = 0 AND sRecordedD IS NOT NULL)
			OR (@Include_sFinalDocsD = 0 AND sFinalDocsD IS NOT NULL)
			OR (@Include_sClosedD = 0 AND sClosedD IS NOT NULL)
			OR (@Include_sSubmittedForPurchaseReviewD = 0 AND sSubmittedForPurchaseReviewD IS NOT NULL)
			OR (@Include_sInPurchaseReviewD = 0 AND sInPurchaseReviewD IS NOT NULL)
			OR (@Include_sPrePurchaseConditionsD = 0 AND sPrePurchaseConditionsD IS NOT NULL)
			OR (@Include_sSubmittedForFinalPurchaseD = 0 AND sSubmittedForFinalPurchaseD IS NOT NULL)
			OR (@Include_sInFinalPurchaseReviewD = 0 AND sInFinalPurchaseReviewD IS NOT NULL)
			OR (@Include_sClearToPurchaseD = 0 AND sClearToPurchaseD IS NOT NULL)
			OR (@Include_sPurchasedD = 0 AND sPurchasedD IS NOT NULL)
			OR (@Include_sReadyForSaleD = 0 AND sReadyForSaleD IS NOT NULL)
			OR (@Include_sShippedToInvestorD = 0 AND sShippedToInvestorD IS NOT NULL)
			OR (@Include_sSuspendedByInvestorD = 0 AND sSuspendedByInvestorD IS NOT NULL)
			OR (@Include_sCondSentToInvestorD = 0 AND sCondSentToInvestorD IS NOT NULL)
			OR (@Include_sAdditionalCondSentD = 0 AND sAdditionalCondSentD IS NOT NULL)
			OR (@Include_sLPurchaseD = 0 AND sLPurchaseD IS NOT NULL)
			OR (@Include_sCounterOfferD = 0 AND sCounterOfferD IS NOT NULL)
			OR (@Include_sOnHoldD = 0 AND sOnHoldD IS NOT NULL)
			OR (@Include_sCanceledD = 0 AND sCanceledD IS NOT NULL)
			OR (@Include_sSuspendedD = 0 AND sSuspendedD IS NOT NULL)
			OR (@Include_sRejectD = 0 AND sRejectD IS NOT NULL)
			OR (@Include_sWithdrawnD = 0 AND sWithdrawnD IS NOT NULL)
			OR (@Include_sArchivedD = 0 AND sArchivedD IS NOT NULL)
		))
	)
END