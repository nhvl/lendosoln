-- =============================================
-- Author:		David Dao
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE dbo.EDOCS_IDS_DOCTYPE_ListAll 

AS
BEGIN

    SELECT Id, Description, BarcodeClassification
    FROM EDOCS_IDS_DOCTYPE

END
