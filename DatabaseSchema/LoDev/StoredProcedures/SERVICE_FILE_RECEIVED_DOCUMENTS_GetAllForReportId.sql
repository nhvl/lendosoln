ALTER PROCEDURE [dbo].[SERVICE_FILE_RECEIVED_DOCUMENTS_GetAllForReportId]
	@BrokerId uniqueidentifier,
	@AppId uniqueidentifier,
	@VendorId uniqueidentifier,
	@ReportId varchar(21)
AS
BEGIN
	SELECT *
	FROM SERVICE_FILE_RECEIVED_DOCUMENTS
	WHERE BrokerId = @BrokerId
		AND AppId = @AppId
		AND VendorId = @VendorId
		AND ReportId = @ReportId;
END
