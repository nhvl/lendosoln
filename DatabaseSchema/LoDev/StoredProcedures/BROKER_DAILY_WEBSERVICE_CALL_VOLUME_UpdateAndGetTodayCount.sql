-- =============================================
-- Author:		Thien Nguyen
-- Create date: 3/24/2016
-- Description:	Update today call volume and return its new value.
-- =============================================
ALTER PROCEDURE [dbo].[BROKER_DAILY_WEBSERVICE_CALL_VOLUME_UpdateAndGetTodayCount]
    @BrokerId UniqueIdentifier,
	@PendingCount int
	
AS
SET TRANSACTION ISOLATION LEVEL READ COMMITTED
BEGIN TRANSACTION
	DECLARE @LockResult Int
    DECLARE @Today DATETIME

	EXEC @LockResult = sp_getapplock @Resource='BROKER_DAILY_WEBSERVICE_CALL_VOLUME_LOCK', @LockMode = 'Exclusive'
	IF @LockResult < 0
	BEGIN
		 RAISERROR('Unable to lock BROKER_DAILY_WEBSERVICE_CALL_VOLUME_LOCK resource.', 16, 1)
  		RETURN
 	END 
   
    
    SELECT @Today = DATEDIFF(DAY, 0, GETDATE())
	
	IF EXISTS (SELECT NumberOfCalls
					FROM BROKER_DAILY_WEBSERVICE_CALL_VOLUME 
					WHERE BrokerId = @BrokerId AND RecordDate = @Today)
	BEGIN
		UPDATE BROKER_DAILY_WEBSERVICE_CALL_VOLUME
			SET NumberOfCalls = NumberOfCalls + @PendingCount
		WHERE (BrokerId = @BrokerId) AND (RecordDate = @Today)
	END
	ELSE
	BEGIN
		INSERT INTO BROKER_DAILY_WEBSERVICE_CALL_VOLUME( BrokerId, RecordDate, NumberOfCalls)
			VALUES ( @BrokerId, @Today, @PendingCount)		
	END
	
	SELECT NumberOfCalls
		FROM BROKER_DAILY_WEBSERVICE_CALL_VOLUME 
		WHERE (BrokerId = @BrokerId) AND (RecordDate = @Today)

COMMIT TRANSACTION
