-- =============================================
-- Author:		Eric Mallare
-- Create date: 8/17/2017
-- Description:	Deletes a title vendor
-- =============================================
ALTER PROCEDURE [dbo].[TITLE_DeleteVendorById]
	@VendorId int
AS
BEGIN TRANSACTION
	DECLARE @QuotingTransmissionId int
	SET @QuotingTransmissionId = (SELECT v.QuotingTransmissionId FROM TITLE_FRAMEWORK_VENDOR v WHERE v.VendorId=@VendorId);
	
	DECLARE @PolicyOrderingTransmissionId int
	SET @PolicyOrderingTransmissionId = (SELECT v.PolicyOrderingTransmissionId FROM TITLE_FRAMEWORK_VENDOR v WHERE v.VendorId=@VendorId)
	
	DELETE FROM
		TITLE_FRAMEWORK_VENDOR
	WHERE
		VendorId=@VendorId

	DELETE FROM
		TITLE_FRAMEWORK_TRANSMISSION_INFO
	WHERE
		TransmissionInfoId=@QuotingTransmissionId OR
		TransmissionInfoId=@PolicyOrderingTransmissionId

	if( 0!=@@error)
	BEGIN
		ROLLBACK TRANSACTION
		RAISERROR('Error in delete in TITLE_DeleteVendorById sp', 16, 1);
		RETURN -100;
	END
COMMIT TRANSACTION
RETURN;
