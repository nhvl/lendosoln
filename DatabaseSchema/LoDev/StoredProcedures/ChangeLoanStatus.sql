CREATE PROCEDURE ChangeLoanStatus
	@LoanID Guid,
	@NewStatus int,
	@Reason varchar(1000) = NULL
AS
UPDATE Loan_File_B
       SET sStatusT = @NewStatus
WHERE sLId = @LoanID and sStatusT <> @NewStatus
UPDATE Loan_File_A
      SET  sStatusLckd = 1
WHERE sLId = @LoanID
