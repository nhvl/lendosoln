-- =============================================
-- Author:		Brian Beery
-- Create date: 12/31/12
-- Description:	Stores the user's flood credentials
-- =============================================
ALTER PROCEDURE [dbo].[Flood_UpdateSavedAuthentication]
	@UserId UniqueIdentifier,
	@FloodLoginNm varchar(50),
	@FloodPassword varchar(50) = null,
	@EncryptedFloodPassword varbinary(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	UPDATE BROKER_USER
	SET
		FloodLoginNm = @FloodLoginNm,
		FloodPassword = COALESCE(@FloodPassword, FloodPassword),
		EncryptedFloodPassword = @EncryptedFloodPassword
	WHERE UserId= @UserId
END
