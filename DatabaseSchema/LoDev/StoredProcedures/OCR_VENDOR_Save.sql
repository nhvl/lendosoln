CREATE PROCEDURE [dbo].[OCR_VENDOR_Save]
	@VendorId uniqueidentifier = null,
	@IsValid bit,
	@DisplayName varchar(50), 
	@PortalUrl varchar(512),
	@UploadURL varchar(512),
	@EnableConnectionTest bit
AS
	IF @VendorId is null 
	begin 
	INSERT OCR_VENDOR_CONFIGURATION (IsValid, DisplayName, PortalUrl, VendorId, UploadURL, EnableConnectionTest)
	VALUES (@IsValid, @DisplayName, @PortalUrl, newid(), @UploadURL, @EnableConnectionTest) 
	end
	ELSE
	begin
		UPDATE TOP(1) OCR_VENDOR_CONFIGURATION SET 
		IsValid = @IsValid,
		DisplayName = @DisplayName,
		PortalUrl = @PortalUrl,
		UploadUrl = @UploadUrl,
		EnableConnectionTest = @EnableConnectionTest
		WHERE VendorId = @VendorId 
	
		IF @@Rowcount = 0 
		BEGIN
			RAISERROR('Vendor Not Found', 16, 1);
		END
	
	END

