



ALTER PROCEDURE [dbo].[RetrieveUserCredentialByUserId] 
	@UserId uniqueidentifier,
	@BrokerId UniqueIdentifier = NULL
AS
SELECT v.BrokerId, v.UserId, v.EmployeeId, v.Permissions, v.BrokerNm, v.UserFirstNm, v.UserLastNm, 
               v.NeedToAcceptLatestAgreement, v.LoginNm, v.IsSharable, v.PasswordExpirationD, v.LoginSessionID, v.BranchId,
               v.IsOthersAllowedToEditUnderwriterAssignedFile, v.IsLOAllowedToEditProcessorAssignedFile,
               v.HasLONIntegration, v.NHCKey, v.SelectedPipelineCustomReportId, v.LastUsedCreditLoginNm, v.LastUsedCreditAccountId,
               v.ByPassBgCalcForGfeAsDefault, v.LastUsedCreditProtocolId, v.Type, v.LpePriceGroupId,
	v.IsRateLockedAtSubmission,v.IsOnlyAccountantCanModifyTrustAccount,v.HasLenderDefaultFeatures
, v.IsQuickPricerEnable,b.IsQuickPricerEnabled AS IsUserQuickPricerEnabled, b.IsPricingMultipleAppsSupported, v.BillingVersion, b.PmlBrokerId, b.PmlLevelAccess
, b.IsNewPmlUIEnabled, b.IsUsePml2AsQuickPricer, b.PortalMode, NULL as MiniCorrespondentBranchId, NULL as CorrespondentBranchId
, b.OptsToUseNewLoanEditorUI
FROM VIEW_ACTIVE_LO_USER_WITH_BROKER_AND_LICENSE_INFO v WITH (NOLOCK) JOIN Broker_User b WITH (NOLOCK) ON v.UserId=b.UserId
WHERE v.UserId = @UserId

UNION

SELECT v.BrokerId, v.UserId, v.EmployeeId, v.Permissions, v.BrokerNm, v.UserFirstNm, v.UserLastNm, 
               v.NeedToAcceptLatestAgreement, v.LoginNm, v.IsSharable, v.PasswordExpirationD, v.LoginSessionID, v.BranchId,
               v.IsOthersAllowedToEditUnderwriterAssignedFile, v.IsLOAllowedToEditProcessorAssignedFile,
               v.HasLONIntegration, v.NHCKey, v.SelectedPipelineCustomReportId, v.LastUsedCreditLoginNm, '', --LastUsedCreditAccountId not defined for PML Users, OPM 118517
               v.ByPassBgCalcForGfeAsDefault, v.LastUsedCreditProtocolId, v.Type, v.LpePriceGroupId,
	v.IsRateLockedAtSubmission,v.IsOnlyAccountantCanModifyTrustAccount,v.HasLenderDefaultFeatures
, v.IsQuickPricerEnable,b.IsQuickPricerEnabled AS IsUserQuickPricerEnabled, b.IsPricingMultipleAppsSupported, v.BillingVersion, b.PmlBrokerId, b.PmlLevelAccess
, b.IsNewPmlUIEnabled, b.IsUsePml2AsQuickPricer, b.PortalMode, v.MiniCorrespondentBranchId, v.CorrespondentBranchId
, b.OptsToUseNewLoanEditorUI
FROM VIEW_ACTIVE_PML_USER_WITH_BROKER_INFO v WITH (NOLOCK) JOIN Broker_User b WITH (NOLOCK) ON v.UserId = b.UserId
WHERE v.UserId = @UserId




