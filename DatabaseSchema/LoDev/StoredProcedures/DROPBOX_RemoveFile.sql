CREATE PROCEDURE [dbo].DROPBOX_RemoveFile
	@UserId uniqueidentifier,
	@FileId uniqueidentifier
AS
BEGIN
	delete from DROPBOX_FILES where UserId = @UserId and FileId = @FileId
END