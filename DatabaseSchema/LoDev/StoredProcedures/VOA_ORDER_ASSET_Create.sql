-- =============================================
-- Author:		Timothy Jewell
-- Create date:	05/23/2017
-- Description:	Creates an Asset on a VOA order.  Be sure to wrap this in a transaction with the other inserts.
-- =============================================
ALTER PROCEDURE [dbo].[VOA_ORDER_ASSET_Create]
	@OrderId int,
	@Id uniqueidentifier,
	@Institution varchar(100),
	@AccountNumber varchar(50)
AS
BEGIN
	INSERT INTO [dbo].[VOA_ORDER_ASSET]
		(OrderId, Id, Institution, AccountNumber)
	VALUES
		(@OrderId, @Id, @Institution, @AccountNumber)
END
GO
