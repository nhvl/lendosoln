-- =============================================
-- Author:		Justin Lara
-- Create date: 3/29/2017
-- Description:	Deletes an AUS order from the table.
-- =============================================
CREATE PROCEDURE [dbo].[AUS_ORDERS_Delete] 
	@AusOrderId int,
	@BrokerId uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    DELETE FROM AUS_ORDERS
    WHERE AusOrderId = @AusOrderId AND BrokerId = @BrokerId
END
