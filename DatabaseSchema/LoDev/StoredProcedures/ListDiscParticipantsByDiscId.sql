CREATE PROCEDURE ListDiscParticipantsByDiscId 
	@DiscLogId Guid , @IsValid Bit = NULL
AS
SELECT n.NotifId , n.NotifReceiverUserId , n.NotifIsValid , n.NotifReceiverLoginNm , e.UserFirstNm , e.UserLastNm
FROM Discussion_Notification AS n WITH (NOLOCK)  JOIN Employee AS e ON e.EmployeeUserId = n.NotifReceiverUserId
WHERE
	n.DiscLogId = @DiscLogId
	AND
	n.NotifIsValid = COALESCE( @IsValid , n.NotifIsValid )
	if( 0!=@@error)
	begin
		RAISERROR('Error in the selection statement in ListDiscParticipantsByDiscId sp', 16, 1);
		return -100;
	end
	return 0;
