-- =============================================
-- Author:		paoloa
-- Create date: 4/18/2013
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[UpdateLockDeskClosureForPolicyId]
	-- Add the parameters for the stored procedure here
	@PolicyId uniqueidentifier,
	@BrokerId uniqueidentifier,
	@ClosureDate smalldatetime,
	@LockDeskOpenTime smalldatetime,
	@LockDeskCloseTime smalldatetime,
	@IsClosedAllDay bit,
	@IsClosedForBusiness bit
	
AS
BEGIN
	UPDATE LENDER_LOCK_POLICY_HOLIDAY
	SET
		LockDeskOpenTime = @LockDeskOpenTime
		, LockDeskCloseTime = @LockDeskCloseTime
		, IsClosedAllDay = @IsClosedAllDay
		, IsClosedForBusiness  = @IsClosedForBusiness  
		
	WHERE LockPolicyId = @PolicyId and BrokerId = @BrokerId and ClosureDate = @ClosureDate
END
