CREATE PROCEDURE ListUsers
	@Type UserType = NULL , @IsActive Bit = NULL
AS
	SELECT
		a.UserId , a.LoginNm , a.RecentLoginD , a.IsSharable , a.Type , a.IsActive
	FROM
		All_User AS a
	WHERE
		a.Type = COALESCE( @Type , a.Type )
		AND
		a.IsActive = COALESCE( @IsActive , a.IsActive )
