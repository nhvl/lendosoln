-- =============================================
-- Author:		Justin Lara
-- Create date: 9/13/2018
-- Description:	Retrieves all OCR-enabled branch
--              groups for a specified broker.
-- =============================================
ALTER PROCEDURE [dbo].[EDOCS_OCR_ENABLED_BRANCH_GROUPS_RetrieveByBroker]
	@BrokerId uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT BrokerId, GroupId
    FROM EDOCS_OCR_ENABLED_BRANCH_GROUPS
    WHERE BrokerId = @BrokerId
END
