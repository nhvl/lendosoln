-- =============================================
-- Author:		Antonio Valencia
-- Create date: 2/9/2013
-- Description:	Migrate Categories for task so we can delete
-- =============================================
ALTER PROCEDURE [dbo].[TASK_MigrateCategory] 
	-- Add the parameters for the stored procedure here
	@BrokerId uniqueidentifier,
	@CondCategorySrc int,
	@CondCategoryDst int,
	@AffectedTaskRows int OUT,
	@AffectedConditionChoiceRows int OUT
AS
BEGIN
	update task
	set condcategoryid = @CondCategoryDst
	where condcategoryid = @CondCategorySrc and brokerid = @brokerid
	
	select @AffectedTaskRows = @@rowcount
	
	update CONDITION_CHOICE
	SET ConditionCategoryId = @CondCategoryDst
	WHERE ConditionCategoryId = @CondCategorySrc and brokerid = @brokerid
	
	select @AffectedConditionChoiceRows = @@rowcount
END
