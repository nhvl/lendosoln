


CREATE PROCEDURE [dbo].[RetrieveBrokerPmlSiteIdByEmployeeID] 
	@EmployeeID uniqueidentifier
AS
	SELECT BrokerPmlSiteID FROM VIEW_ACTIVE_PML_USER_WITH_BROKER_INFO WITH (NOLOCK) WHERE EmployeeID=@EmployeeID



