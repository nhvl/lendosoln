ALTER  PROCEDURE [dbo].[RetrieveBrokerByID]
	@BrokerId Guid
AS
BEGIN
	SELECT
		*
	FROM
		Broker
	WHERE
		BrokerId = @BrokerId
	if( 0!=@@error)
	begin
		RAISERROR('Error in the select statement in RetrieveBrokerById sp', 16, 1);
		return -100;
	end
	return 0;
END
