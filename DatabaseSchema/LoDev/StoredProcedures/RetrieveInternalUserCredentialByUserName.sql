
CREATE PROCEDURE [dbo].[RetrieveInternalUserCredentialByUserName]
	@LoginName LoginName
AS
SELECT UserId, LoginNm, LoginSessionId, Permissions, FirstNm, LastNm, PasswordHash, PasswordSalt, IsUpdatedPassword
FROM VIEW_ACTIVE_INTERNAL_USER
WHERE LoginNm = @LoginName

