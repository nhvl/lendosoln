﻿ALTER PROCEDURE [dbo].[InsertActionEvent]
	@UserId Guid, @TypeDesc varchar(36), @LoginName varchar(36), @FirstName varchar(36), @LastName varchar(36), @NewActivationCap Int = 0, @BrokerId Guid, @IpAddress varchar(46) = ''
AS
	INSERT INTO Action_Event_2013
		( UserId
		, EventType
		, UserLoginNm
		, UserFirstNm
		, UserLastNm
		, NewActivationCap
		, BrokerId
		, IpAddress
		)
	VALUES 
		( @UserId
		, @TypeDesc
		, @LoginName
		, @FirstName
		, @LastName
		, @NewActivationCap
		, @BrokerId
		, @IpAddress
		)
	if( 0 != @@error )
	begin
		RAISERROR('Error in updating Action_Event table in InsertActionEvent sp', 16, 1);
		return -100;
	end
	return 0;
