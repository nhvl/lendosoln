



ALTER PROCEDURE [dbo].[GetNamingPatternAndCounter]
	@BrokerId Guid,
	@BranchId Guid,
	@EnableMersGeneration bit = 1, -- dd - Provide option to stop increase Mers counter, ie: Generate template and lead.
	@UpdateCounters bit = 1,
	@NamingPattern varchar(50) OUT,
	@NamingCounter BigInt OUT,
	@BranchLNmPrefix varchar(10) = NULL  OUT,
    @MersOrganizationId varchar(7) = NULL OUT,
    @MersCounter BigInt = NULL OUT,
    @ReferenceCounter BigInt = NULL OUT,
    @ReferenceNamingPattern varchar(50) OUT
AS




-- dd 9/29/05 - Get the counter from broker table and increase counter. We will apply the naming pattern to actual name in C# code.
DECLARE @CurrentDate SmallDateTime
DECLARE @CounterDate SmallDateTime
DECLARE @ReferenceCounterDate SmallDateTime

DECLARE @IsAutoGenerateMersMin bit
SET @CurrentDate = GETDATE()

SELECT @NamingPattern = NamingPattern, @NamingCounter = NamingCounter, @CounterDate = NamingCounterDate,
       @MersOrganizationId = MersOrganizationId, @MersCounter = MersCounter, @IsAutoGenerateMersMin = IsAutoGenerateMersMin,
       @ReferenceNamingPattern = NamingPattern, @ReferenceCounter = ReferenceCounter, @ReferenceCounterDate = ReferenceCounterDate
FROM Broker WHERE BrokerId = @BrokerId

SELECT @BranchLNmPrefix = BranchLNmPrefix FROM Branch WHERE BrokerId = @BrokerId AND BranchId = @BranchId

-- Check if counter need to reset by day or month or year or increment counter by one.
IF CHARINDEX('d}', @NamingPattern) > 0 AND ( DAY(@CounterDate) <> DAY(@CurrentDate) OR MONTH(@CounterDate) <> MONTH(@CurrentDate) OR YEAR(@CounterDate) <> YEAR(@CurrentDate) )
BEGIN
	SET @NamingCounter = 0
END
ELSE IF CHARINDEX('m}', @NamingPattern) > 0 AND ( MONTH(@CounterDate) <> MONTH(@CurrentDate) OR YEAR(@CounterDate) <> YEAR(@CurrentDate) )
BEGIN
	SET @NamingCounter = 0
END
ELSE IF CHARINDEX('y}', @NamingPattern) > 0 AND (  YEAR(@CounterDate) <> YEAR(@CurrentDate) ) 
BEGIN
	SET @NamingCounter = 0
END
ELSE
BEGIN
	SET @NamingCounter = @NamingCounter + 1
END

-- Check if reference counter need to reset by day or month or year or increment counter by one.
IF CHARINDEX('d}', @ReferenceNamingPattern) > 0 AND ( DAY(@ReferenceCounterDate) <> DAY(@CurrentDate) OR MONTH(@ReferenceCounterDate) <> MONTH(@CurrentDate) OR YEAR(@ReferenceCounterDate) <> YEAR(@CurrentDate) )
BEGIN
	SET @ReferenceCounter = 0
END
ELSE IF CHARINDEX('m}', @ReferenceNamingPattern) > 0 AND ( MONTH(@ReferenceCounterDate) <> MONTH(@CurrentDate) OR YEAR(@ReferenceCounterDate) <> YEAR(@CurrentDate) )
BEGIN
	SET @ReferenceCounter = 0
END
ELSE IF CHARINDEX('y}', @ReferenceNamingPattern) > 0 AND (  YEAR(@ReferenceCounterDate) <> YEAR(@CurrentDate) ) 
BEGIN
	SET @ReferenceCounter = 0
END
ELSE
BEGIN
	SET @ReferenceCounter = @ReferenceCounter + 1
END

IF @IsAutoGenerateMersMin = 1 AND @EnableMersGeneration = 1
BEGIN
	SET @MersCounter = @MersCounter + 1
	
END
ELSE
BEGIN
	SET @MersCounter = NULL
	SET @MersOrganizationId = NULL
END
-- Update Counter to broker table
IF @UpdateCounters = 1
BEGIN
	UPDATE Broker WITH ( ROWLOCK ) SET NamingCounter = @NamingCounter, NamingCounterDate = GETDATE(), MersCounter =COALESCE(@MersCounter, MersCounter),
	ReferenceCounter = @ReferenceCounter, ReferenceCounterDate = GETDATE()
	WHERE BrokerId = @BrokerId
END
--SELECT @NamingPattern AS NamingPattern, @NamingCounter AS NamingCounter


