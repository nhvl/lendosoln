-- =============================================
-- Author:		Timothy Jewell
-- Create date: 2/9/2015
-- Description:	Fetch summary of information on
-- all EDocs, valid and invalid, on a specific
-- loan.
-- 9/28/17 - av - Add BrokerId switch it to use the edoc table and not obsolete view.
-- =============================================
ALTER PROCEDURE [dbo].[EDOCS_FetchAllDocsForLoan]
	@LoanId uniqueidentifier,
	@BrokerId uniqueidentifier = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT
		DocumentId,
		sLId,
		aAppId,
		doc.IsValid,
		FolderId,
		FolderName,
		doc.DocTypeId,
		DocTypeName,
		doctype.IsValid as DocTypeIsValid,
		doc.description as PublicDescription,
		InternalDescription,
		NumPages,
		CreatedDate,
		LastModifiedDate,
		[Status],
		StatusDescription
		
	FROM EDOCS_DOCUMENT as doc 
	JOIN VIEW_EDOCS_DOCTYPE_FOLDER as doctype on doc.doctypeid = doctype.doctypeid
	WHERE doc.sLId = @LoanId and doc.BrokerId =  COALESCE(@BrokerId, doc.BrokerId)

END
