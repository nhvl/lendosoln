ALTER PROCEDURE [dbo].[RetrieveBrokerByIDWithVersion]
	@BrokerID UniqueIdentifier,
	@Version timestamp = NULL
AS
BEGIN
	IF EXISTS (SELECT 1 FROM Broker Where BrokerID = @BrokerID AND Version <> @Version)
	BEGIN
		SELECT *
		FROM Broker
		WHERE BrokerId = @BrokerId
	END
END
