
CREATE PROCEDURE [dbo].EDOCS_LostDocumentDelete
	@Id uniqueidentifier,
	@BrokerId uniqueidentifier

 
AS
BEGIN
	DELETE TOP(1) FROM EDOCS_LOST_DOCUMENT
	WHERE DocumentId = @Id and BrokerId = @BrokerId 
END
