CREATE proc [dbo].[DocMagic_ListAlternateLenderByBroker]
	@BrokerId uniqueidentifier
as
select LenderName, DocMagicAlternateLenderId 
from DOCMAGIC_ALTERNATE_LENDER
where BrokerId = @BrokerId
and IsValid = 1
order by LenderName

