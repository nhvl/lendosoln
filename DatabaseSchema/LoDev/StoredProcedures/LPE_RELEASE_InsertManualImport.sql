ALTER PROCEDURE dbo.LPE_RELEASE_InsertManualImport
	@ReleaseVersion datetime,
	@SnapshotId varchar(100),
	@DataLastModifiedD datetime,
	@FileKey varchar(100)
AS
BEGIN

	INSERT INTO LPE_RELEASE(ReleaseVersion, SnapshotId, DataLastModifiedD, FileKey)
	                                      VALUES (@ReleaseVersion, @SnapshotId, @DataLastModifiedD, @FileKey)
                               

END