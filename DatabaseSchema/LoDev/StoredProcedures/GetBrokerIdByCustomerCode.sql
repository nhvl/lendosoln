CREATE PROCEDURE [dbo].[GetBrokerIdByCustomerCode] 
	@CustomerCode varchar(36)
AS
BEGIN
	SELECT BrokerId FROM Broker WHERE CustomerCode = @CustomerCode
END
