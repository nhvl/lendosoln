-- =============================================
-- Author:		Antonio Valencia
-- Create date: 4/23/2013
-- Description:	Creates a Title Vendor Record
-- =============================================
ALTER PROCEDURE [dbo].[TITLE_VENDOR_Get]
	@Id int = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SELECT
		TitleVendorId,
		TitleVendorName,
		TitleVendorExportPath,
		TitleVendorRequiresAccountId,
		LQBServiceUserName,
		LQBServicePassword,
		EncryptedLQBServicePassword,
		LQBServiceSourceName,
		TitleVendorExportPathTitle,
		TitleVendorIntegrationType,
		EnableConnectionTest,
		EncryptionKeyId
	FROM TITLE_VENDOR
	WHERE (@Id is null OR @Id = TitleVendorId)
END
