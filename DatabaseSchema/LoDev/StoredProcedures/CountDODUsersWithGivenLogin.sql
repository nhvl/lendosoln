
-- =============================================
-- Author:		Antonio Valencia
-- Create date: 09-25-08
-- Description:	Retrieves user count who have given do du logins 
-- =============================================
CREATE PROCEDURE [dbo].[CountDODUsersWithGivenLogin]
	@BrokerId uniqueidentifier = null,
	@PmlSiteId uniqueidentifier = null,
	@DOLogin varchar(200) = null, 
	@DULogin varchar(200) = null, 
	@DuplicateDULogin int output,
	@DuplicateDOLogin int output,
	@UserId uniqueidentifier = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
		SET NOCOUNT ON;
	
------ NEed to clean up
		IF @DoLogin = null and @DuLogin = null
		begin 
			select @DuplicateDOLogin = 0, @DuplicateDOLogin = 0;
			return
		end

		IF @DuLogin = null 
		BEGIN 
			SELECT @DuplicateDULogin = 0, @DuplicateDOLogin = count(*) 
			From Broker_User as bu JOIN Employee e ON bu.EmployeeId = e.EmployeeId
                                   JOIN Branch br ON e.BranchId = br.BranchId
			WHERE br.BrokerId = @BrokerId
              AND bu.UserId <> @UserId
              AND bu.DOAutoLoginNm = @DoLogin
			return;
		end

		if  @DoLogin = null 
		begin 
			select @DuplicateDOLogin = 0, 	@DuplicateDULogin = count(*) 
			From Broker_User as bu JOIN Employee e ON bu.EmployeeId = e.EmployeeId
                                   JOIN Branch br ON e.BranchId = br.BranchId
			WHERE br.BrokerId = @BrokerId
              AND bu.UserId <> @UserId
              AND bu.DUAutoLoginNm = @DuLogin
			return
		end

			SELECT @DuplicateDOLogin = count(*) 
			From Broker_User as bu JOIN Employee e ON bu.EmployeeId = e.EmployeeId
                                   JOIN Branch br ON e.BranchId = br.BranchId
			WHERE br.BrokerId = @BrokerId
              AND bu.UserId <> @UserId
              AND bu.DOAutoLoginNm = @DoLogin

			select @DuplicateDULogin = count(*) 
			From Broker_User as bu JOIN Employee e ON bu.EmployeeId = e.EmployeeId
                                   JOIN Branch br ON e.BranchId = br.BranchId
			WHERE br.BrokerId = @BrokerId
              AND bu.UserId <> @UserId
              AND bu.DUAutoLoginNm = @DuLogin
		

END

