-- =============================================
-- Author:		Eric Mallare
-- Create date: 1/23/2018
-- Description:	Update a DocuSign recipient record.
-- =============================================
ALTER PROCEDURE [dbo].[DOCUSIGN_RECIPIENTS_Update]
	@Id int,
	@CurrentStatus int = null,
	@SentDate datetime = null,
	@DeliveredDate datetime = null,
	@SignedDate datetime = null,
	@DeclinedDate datetime = null,
	@DeclinedReason varchar(500) = null,
	@AutoRespondedReason varchar(500) = null,
	@RecipientIdGuid varchar(36) = null,
	@SigningOrder int
AS
BEGIN
	UPDATE DOCUSIGN_RECIPIENTS
	SET
		CurrentStatus=@CurrentStatus,
		SentDate=@SentDate,
		DeliveredDate=@DeliveredDate,
		SignedDate=@SignedDate,
		DeclinedDate=@DeclinedDate,
		DeclinedReason=@DeclinedReason,
		AutoRespondedReason=@AutoRespondedReason,
		RecipientIdGuid=@RecipientIdGuid,
		SigningOrder=@SigningOrder
	WHERE
		Id=@Id 
END