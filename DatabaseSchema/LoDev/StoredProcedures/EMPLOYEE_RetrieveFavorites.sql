-- =============================================
-- Author:		Michael Leinweaver
-- Create date: 11/28/2017
-- Description:	Retrieves an employee's favorites JSON.
-- =============================================
CREATE PROCEDURE [dbo].[EMPLOYEE_RetrieveFavorites]
	@EmployeeId uniqueidentifier
AS
BEGIN
	SELECT FavoritePageIdsJSON
	FROM EMPLOYEE
	WHERE EmployeeId = @EmployeeId
END
