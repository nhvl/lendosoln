ALTER PROCEDURE [dbo].[BROKER_UCD_CONFIG_Save]
    @BrokerId uniqueidentifier,
    @AllowLqbDeliveryToFannieMae bit,
    @IsFannieMaeTesting bit,
    @AllowLqbDeliveryToFreddieMac bit,
    @IsFreddieMacTesting bit,
    @AllowDocMagicDeliveryToFannieMae bit,
    @AllowDocMagicDeliveryToFreddieMac bit
AS
BEGIN
    BEGIN TRANSACTION [SaveTrans]
    IF EXISTS(SELECT 1 FROM dbo.BROKER WHERE BrokerId = @BrokerId)
    BEGIN

        UPDATE dbo.BROKER_UCD_CONFIG
        SET 
            AllowLqbDeliveryToFannieMae = @AllowLqbDeliveryToFannieMae,
            IsFannieMaeTesting = @IsFannieMaeTesting,
            AllowLqbDeliveryToFreddieMac = @AllowLqbDeliveryToFreddieMac,
            IsFreddieMacTesting = @IsFreddieMacTesting,
            AllowDocMagicDeliveryToFannieMae = @AllowDocMagicDeliveryToFannieMae,
            AllowDocMagicDeliveryToFreddieMac = @AllowDocMagicDeliveryToFreddieMac
        WHERE BrokerId = @BrokerId

        IF @@ROWCOUNT = 0
        BEGIN
            INSERT INTO BROKER_UCD_CONFIG
                (BrokerId,
                AllowLqbDeliveryToFannieMae,
                IsFannieMaeTesting,
                AllowLqbDeliveryToFreddieMac,
                IsFreddieMacTesting,
                AllowDocMagicDeliveryToFannieMae,
                AllowDocMagicDeliveryToFreddieMac)
            VALUES 
                (@BrokerId,
                @AllowLqbDeliveryToFannieMae,
                @IsFannieMaeTesting,
                @AllowLqbDeliveryToFreddieMac,
                @IsFreddieMacTesting,
                @AllowDocMagicDeliveryToFannieMae,
                @AllowDocMagicDeliveryToFreddieMac)
        END
    END
    COMMIT TRANSACTION
END
