-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE QuickPricerLoanPool_InsertUsedLoan 
	@BrokerId UniqueIdentifier,
	@sLId UniqueIdentifier
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL REPEATABLE READ
	BEGIN TRANSACTION

		DECLARE @LockResult Int
		EXEC @LockResult = sp_getapplock @Resource='QUICK_PRICER_LOAN_POOL', @LockMode = 'Exclusive'
		IF @LockResult < 0
		BEGIN
			RAISERROR('Unable to lock QUICK_PRICER_LOAN_POOL resource.', 16, 1)
  			RETURN
 		END 
	
		INSERT INTO Quick_Pricer_Loan_Pool (BrokerId, sLId, IsInUsed) VALUES(@BrokerId, @sLId, 1)

	COMMIT TRANSACTION;
END
