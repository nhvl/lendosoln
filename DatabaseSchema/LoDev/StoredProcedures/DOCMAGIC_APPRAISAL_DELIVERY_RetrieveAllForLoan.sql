-- =============================================
-- Author:		Timothy Jewell
-- Create date: 4/24/2018
-- Description:	Retrieves the DM appraisal delivery records for a specified loan.
-- =============================================
ALTER PROCEDURE [dbo].[DOCMAGIC_APPRAISAL_DELIVERY_RetrieveAllForLoan]
	@BrokerId uniqueidentifier,
	@LoanId uniqueidentifier
AS
BEGIN
	SELECT
		AppraisalDeliveryId,
		BrokerId,
		LoanId,
		AppraisalOrderId,
		DocVendorId,
		SentDate,
		SentByName,
		SentByUserId,
		WebsheetNumber
	FROM DOCMAGIC_APPRAISAL_DELIVERY
	WHERE	BrokerId = @BrokerId
		AND	LoanId = @LoanId

	SELECT
		doc.AppraisalDeliveryId,
		doc.DocumentId
	FROM DOCMAGIC_APPRAISAL_DELIVERY_DOCUMENT doc
	JOIN DOCMAGIC_APPRAISAL_DELIVERY delivery ON delivery.AppraisalDeliveryId = doc.AppraisalDeliveryId
	WHERE	delivery.BrokerId = @BrokerId
		AND	delivery.LoanId = @LoanId
END
