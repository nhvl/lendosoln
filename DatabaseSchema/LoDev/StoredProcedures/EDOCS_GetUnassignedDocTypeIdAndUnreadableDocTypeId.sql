-- =============================================
-- Author:		Geoffrey Feltman
-- Create date: 4/10/13
-- Description:	Get the doc type ids that are used when there is a missing mapping or 
--				unreadable barcode.
--				This was taken from EDOCS_GetDocTypesWithDocMagicMappingsForBroker,
--				which was written by Antonio.
-- =============================================
CREATE PROCEDURE dbo.EDOCS_GetUnassignedDocTypeIdAndUnreadableDocTypeId
	-- Add the parameters for the stored procedure here
	@BrokerId uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    DECLARE @UnassignedDocTypeId int
	DECLARE @UnclassifiedFolderId int 
	DECLARE @UnreadableBarCodeDocTypeId int
	
	-- GET The Special DocTypes 
	SELECT @UnassignedDocTypeId = docTypeId FROM EDOCS_DOCUMENT_TYPE where BrokerId = @BrokerID AND DocTypeName = 'UNASSIGNED DOCUMENT TYPE';
	SELECT @UnreadableBarCodeDocTypeId = docTypeId FROM EDOCS_DOCUMENT_TYPE where BrokerId = @BrokerID AND DocTypeName = 'UNREADABLE BARCODE';
	
	-- If we cant find them then create them 
	IF @UnassignedDocTypeId IS NULL OR @UnreadableBarCodeDocTypeId  IS NULL BEGIN
		--to create them we need the folder id 
		SELECT @UnclassifiedFolderId = FolderId FROM EDOCS_FOLDER WHERE BrokerId = @BrokerId AND FolderName = 'UNCLASSIFIED'
		
		--cant create them if there is no unclassified folder 
		IF  @UnclassifiedFolderId IS NULL BEGIN
		RAISERROR('Could not get unclassified folder id', 16, 1);
		return 
		END
		
		IF @UnassignedDocTypeId IS NULL BEGIN 
			INSERT INTO EDOCS_DOCUMENT_TYPE(BrokerId, DocTypeName, IsValid, FolderId) 
			VALUES( @BrokerId, 'UNASSIGNED DOCUMENT TYPE', 1, @UnclassifiedFolderId)
			SELECT @UnassignedDocTypeId = SCOPE_IDENTITY()
		END 
		
		IF @UnreadableBarCodeDocTypeId IS NULL BEGIN 
			INSERT INTO EDOCS_DOCUMENT_TYPE(BrokerId, DocTypeName, IsValid, FolderId) 
			VALUES( @BrokerId, 'UNREADABLE BARCODE', 1, @UnclassifiedFolderId)
			SELECT @UnreadableBarCodeDocTypeId = SCOPE_IDENTITY()
		END 
			
	END
	
	--finally get the special doctypes 
	SELECT @UnassignedDocTypeId as UnassignedDocTypeId, @UnreadableBarCodeDocTypeId as UnreadableBarCodeDocTypeId
END
