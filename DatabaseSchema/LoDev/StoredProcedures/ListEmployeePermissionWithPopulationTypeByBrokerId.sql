﻿ALTER PROCEDURE [dbo].[ListEmployeePermissionWithPopulationTypeByBrokerId]
	@BrokerId uniqueidentifier
AS
	SELECT 
		a.Type,
		e.EmployeeId, 
		e.PopulatePmlPermissionsT,
		u.PmlBrokerId,
		u.Permissions
	FROM 
		dbo.All_User AS a 
		JOIN dbo.Broker_User AS u ON u.UserId = a.UserId 
		JOIN dbo.Employee AS e ON e.EmployeeId = u.EmployeeId 
		JOIN dbo.Branch AS r ON r.BranchId = e.BranchId 
		JOIN dbo.Broker AS b ON b.BrokerId = r.BrokerId 
	WHERE b.BrokerId = @BrokerId
