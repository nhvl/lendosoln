CREATE PROCEDURE [dbo].[FEE_TYPE_Delete] 
	@FeeTypeId		uniqueidentifier
AS
	DELETE FROM FEE_TYPE
	WHERE FeeTypeId = @FeeTypeId