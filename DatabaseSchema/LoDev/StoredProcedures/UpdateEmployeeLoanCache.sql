
-- 07/20/06 mf - This is repeatedly called after an employee's name,
-- phone, or email is changed in the employee editor. OPM 6441.
--
-- 2/25/13 gf - Updated to include Shipper, Funder, Post-Closer,
-- Insuring, Collateral Agent, and Doc Drawer roles.

CREATE    PROCEDURE [dbo].[UpdateEmployeeLoanCache]
  @pEmployeeId uniqueidentifier
  , @pBrokerId uniqueidentifier
  , @RoleName varchar(50)
  , @pFullName varchar(60)
  , @pPhone varchar(21)
  , @pEmail varchar(80)
  , @pEmployeeStartD smalldatetime = null
  , @pEmployeeTerminationD smalldatetime = null
AS
  IF @RoleName = 'Loan Officer'
  BEGIN
    UPDATE LOAN_FILE_CACHE
    SET sEmployeeLoanRepName = @pFullName
      , sEmployeeLoanRepPhone = @pPhone
      , sEmployeeLoanRepEmail = @pEmail
    WHERE sBrokerId = @pBrokerId AND sEmployeeLoanRepId = @pEmployeeId
    UPDATE lc3
    SET
        sEmployeeLoanRepStartDate = @pEmployeeStartD
      , sEmployeeLoanRepTermination = @pEmployeeTerminationD
    from loan_file_cache lc1
    inner join loan_file_cache_3 lc3
    on lc1.slid = lc3.slid
    WHERE sBrokerId = @pBrokerId AND sEmployeeLoanRepId = @pEmployeeId
  END
  ELSE IF @RoleName = 'Processor'
  BEGIN
    UPDATE LOAN_FILE_CACHE 
    SET sEmployeeProcessorName = @pFullName
      , sEmployeeProcessorPhone = @pPhone
      , sEmployeeProcessorEmail = @pEmail
    WHERE sBrokerId = @pBrokerId AND sEmployeeProcessorId = @pEmployeeId
  END
  ELSE IF @RoleName = 'Manager'
  BEGIN
    UPDATE LOAN_FILE_CACHE 
    SET sEmployeeManagerName = @pFullName
      , sEmployeeManagerPhone = @pPhone
      , sEmployeeManagerEmail = @pEmail
    WHERE sBrokerId = @pBrokerId AND sEmployeeManagerId = @pEmployeeId
  END
  ELSE IF @RoleName = 'Loan Opener'
  BEGIN
    UPDATE LOAN_FILE_CACHE 
    SET sEmployeeLoanOpenerName = @pFullName
      , sEmployeeLoanOpenerPhone = @pPhone
      , sEmployeeLoanOpenerEmail = @pEmail
    WHERE sBrokerId = @pBrokerId AND sEmployeeLoanOpenerId = @pEmployeeId
  END
  ELSE IF @RoleName = 'Lender Acct Exec'
  BEGIN
    UPDATE LOAN_FILE_CACHE
    SET sEmployeeLenderAccExecName = @pFullName
      , sEmployeeLenderAccExecPhone = @pPhone
      , sEmployeeLenderAccExecEmail = @pEmail
    WHERE sBrokerId = @pBrokerId AND sEmployeeLenderAccExecId = @pEmployeeId
  END
  ELSE IF @RoleName = 'Underwriter'
  BEGIN
    UPDATE LOAN_FILE_CACHE 
    SET sEmployeeUnderwriterName = @pFullName
      , sEmployeeUnderwriterPhone = @pPhone
      , sEmployeeUnderwriterEmail = @pEmail
    WHERE sBrokerId = @pBrokerId AND sEmployeeUnderwriterId = @pEmployeeId
  END
  ELSE IF @RoleName = 'Call Center Agent'
  BEGIN
    UPDATE LOAN_FILE_CACHE 
    SET sEmployeeCallCenterAgentName = @pFullName
      , sEmployeeCallCenterPhone = @pPhone
      , sEmployeeCallCenterAgentEmail = @pEmail
    WHERE sBrokerId = @pBrokerId AND sEmployeeCallCenterAgentId = @pEmployeeId
  END
  ELSE IF @RoleName = 'Real Estate Agent'
  BEGIN
    UPDATE LOAN_FILE_CACHE
    SET sEmployeeRealEstateAgentName = @pFullName
      , sEmployeeRealEstateAgentPhone = @pPhone
      , sEmployeeRealEstateAgentEmail = @pEmail 
    WHERE sBrokerId = @pBrokerId AND sEmployeeRealEstateAgentId = @pEmployeeId
  END
  ELSE IF @RoleName = 'Lock Desk'
  BEGIN
    UPDATE LOAN_FILE_CACHE
    SET sEmployeeLockDeskName = @pFullName
      , sEmployeeLockDeskPhone = @pPhone
      , sEmployeeLockDeskEmail = @pEmail
    WHERE sBrokerId = @pBrokerId AND sEmployeeLockDeskId = @pEmployeeId
  END
 ELSE IF @RoleName = 'Closer'
  BEGIN
    UPDATE LOAN_FILE_CACHE
    SET sEmployeeCloserName = @pFullName
      , sEmployeeCloserPhone = @pPhone
      , sEmployeeCloserEmail = @pEmail
    WHERE sBrokerId = @pBrokerId AND sEmployeeCloserId = @pEmployeeId
  END
   ELSE IF @RoleName = 'Broker Processor'
  BEGIN
    UPDATE LOAN_FILE_CACHE
    SET sEmployeeBrokerProcessorName = @pFullName
      , sEmployeeBrokerProcessorPhone = @pPhone
      , sEmployeeBrokerProcessorEmail = @pEmail
    WHERE sBrokerId = @pBrokerId AND sEmployeeBrokerProcessorId = @pEmployeeId
  END
  ELSE IF @RoleName = 'Funder'
  BEGIN
    UPDATE l3
    SET sEmployeeFunderName = @pFullName
      , sEmployeeFunderPhone = @pPhone
      , sEmployeeFunderEmail = @pEmail
    FROM LOAN_FILE_CACHE l JOIN LOAN_FILE_CACHE_3 l3 ON l.sLId = l3.sLId
    WHERE l.sBrokerId = @pBrokerId AND l3.sEmployeeFunderId = @pEmployeeId
  END
  ELSE IF @RoleName = 'Shipper'
  BEGIN
    UPDATE l3
    SET sEmployeeShipperName = @pFullName
      , sEmployeeShipperPhone = @pPhone
      , sEmployeeShipperEmail = @pEmail
    FROM LOAN_FILE_CACHE l JOIN LOAN_FILE_CACHE_3 l3 ON l.sLId = l3.sLId
    WHERE l.sBrokerId = @pBrokerId AND l3.sEmployeeShipperId = @pEmployeeId
  END
  ELSE IF @RoleName = 'Post-Closer'
  BEGIN
    UPDATE l3
    SET sEmployeePostCloserName = @pFullName
      , sEmployeePostCloserPhone = @pPhone
      , sEmployeePostCloserEmail = @pEmail
    FROM LOAN_FILE_CACHE l JOIN LOAN_FILE_CACHE_3 l3 ON l.sLId = l3.sLId
    WHERE l.sBrokerId = @pBrokerId AND l3.sEmployeePostCloserId = @pEmployeeId
  END
  ELSE IF @RoleName = 'Insuring'
  BEGIN
    UPDATE l3
    SET sEmployeeInsuringName = @pFullName
      , sEmployeeInsuringPhone = @pPhone
      , sEmployeeInsuringEmail = @pEmail
    FROM LOAN_FILE_CACHE l JOIN LOAN_FILE_CACHE_3 l3 ON l.sLId = l3.sLId
    WHERE l.sBrokerId = @pBrokerId AND l3.sEmployeeInsuringId = @pEmployeeId
  END
  ELSE IF @RoleName = 'Collateral Agent'
  BEGIN
    UPDATE l3
    SET sEmployeeCollateralAgentName = @pFullName
      , sEmployeeCollateralAgentPhone = @pPhone
      , sEmployeeCollateralAgentEmail = @pEmail
    FROM LOAN_FILE_CACHE l JOIN LOAN_FILE_CACHE_3 l3 ON l.sLId = l3.sLId
    WHERE l.sBrokerId = @pBrokerId AND l3.sEmployeeCollateralAgentId = @pEmployeeId
  END
  ELSE IF @RoleName = 'Doc Drawer'
  BEGIN
    UPDATE l3
    SET sEmployeeDocDrawerName = @pFullName
      , sEmployeeDocDrawerPhone = @pPhone
      , sEmployeeDocDrawerEmail = @pEmail
    FROM LOAN_FILE_CACHE l JOIN LOAN_FILE_CACHE_3 l3 ON l.sLId = l3.sLId
    WHERE l.sBrokerId = @pBrokerId AND l3.sEmployeeDocDrawerId = @pEmployeeId
  END
  ELSE IF @RoleName = 'Credit Auditor'
  BEGIN
      UPDATE l3
      SET sEmployeeCreditAuditorName = @pFullName
        , sEmployeeCreditAuditorPhone = @pPhone
        , sEmployeeCreditAuditorEmail = @pEmail
      FROM LOAN_FILE_CACHE l JOIN LOAN_FILE_CACHE_3 l3 ON l.sLId = l3.sLId
      WHERE l.sBrokerId = @pBrokerId AND l3.sEmployeeCreditAuditorId = @pEmployeeId
  END
  ELSE IF @RoleName = 'Disclosure Desk'
  BEGIN
      UPDATE l3
      SET sEmployeeDisclosureDeskName = @pFullName
        , sEmployeeDisclosureDeskPhone = @pPhone
        , sEmployeeDisclosureDeskEmail = @pEmail
      FROM LOAN_FILE_CACHE l JOIN LOAN_FILE_CACHE_3 l3 ON l.sLId = l3.sLId
      WHERE l.sBrokerId = @pBrokerId AND l3.sEmployeeDisclosureDeskId = @pEmployeeId
  END
  ELSE IF @RoleName = 'Junior Processor'
  BEGIN
      UPDATE l3
      SET sEmployeeJuniorProcessorName = @pFullName
        , sEmployeeJuniorProcessorPhone = @pPhone
        , sEmployeeJuniorProcessorEmail = @pEmail
      FROM LOAN_FILE_CACHE l JOIN LOAN_FILE_CACHE_3 l3 ON l.sLId = l3.sLId
      WHERE l.sBrokerId = @pBrokerId AND l3.sEmployeeJuniorProcessorId = @pEmployeeId
  END
  ELSE IF @RoleName = 'Junior Underwriter'
  BEGIN
      UPDATE l3
      SET sEmployeeJuniorUnderwriterName = @pFullName
        , sEmployeeJuniorUnderwriterPhone = @pPhone
        , sEmployeeJuniorUnderwriterEmail = @pEmail
      FROM LOAN_FILE_CACHE l JOIN LOAN_FILE_CACHE_3 l3 ON l.sLId = l3.sLId
      WHERE l.sBrokerId = @pBrokerId AND l3.sEmployeeJuniorUnderwriterId = @pEmployeeId
  END
  ELSE IF @RoleName = 'Legal Auditor'
  BEGIN
      UPDATE l3
      SET sEmployeeLegalAuditorName = @pFullName
        , sEmployeeLegalAuditorPhone = @pPhone
        , sEmployeeLegalAuditorEmail = @pEmail
      FROM LOAN_FILE_CACHE l JOIN LOAN_FILE_CACHE_3 l3 ON l.sLId = l3.sLId
      WHERE l.sBrokerId = @pBrokerId AND l3.sEmployeeLegalAuditorId = @pEmployeeId
  END
  ELSE IF @RoleName = 'Loan Officer Assistant'
  BEGIN
      UPDATE l3
      SET sEmployeeLoanOfficerAssistantName = @pFullName
        , sEmployeeLoanOfficerAssistantPhone = @pPhone
        , sEmployeeLoanOfficerAssistantEmail = @pEmail
      FROM LOAN_FILE_CACHE l JOIN LOAN_FILE_CACHE_3 l3 ON l.sLId = l3.sLId
      WHERE l.sBrokerId = @pBrokerId AND l3.sEmployeeLoanOfficerAssistantId = @pEmployeeId
  END
  ELSE IF @RoleName = 'Purchaser'
  BEGIN
      UPDATE l3
      SET sEmployeePurchaserName = @pFullName
        , sEmployeePurchaserPhone = @pPhone
        , sEmployeePurchaserEmail = @pEmail
      FROM LOAN_FILE_CACHE l JOIN LOAN_FILE_CACHE_3 l3 ON l.sLId = l3.sLId
      WHERE l.sBrokerId = @pBrokerId AND l3.sEmployeePurchaserId = @pEmployeeId
  END
  ELSE IF @RoleName = 'QC Compliance'
  BEGIN
      UPDATE l3
      SET sEmployeeQCComplianceName = @pFullName
        , sEmployeeQCCompliancePhone = @pPhone
        , sEmployeeQCComplianceEmail = @pEmail
      FROM LOAN_FILE_CACHE l JOIN LOAN_FILE_CACHE_3 l3 ON l.sLId = l3.sLId
      WHERE l.sBrokerId = @pBrokerId AND l3.sEmployeeQCComplianceId = @pEmployeeId
  END
  ELSE IF @RoleName = 'Secondary'
  BEGIN
      UPDATE l3
      SET sEmployeeSecondaryName = @pFullName
        , sEmployeeSecondaryPhone = @pPhone
        , sEmployeeSecondaryEmail = @pEmail
      FROM LOAN_FILE_CACHE l JOIN LOAN_FILE_CACHE_3 l3 ON l.sLId = l3.sLId
      WHERE l.sBrokerId = @pBrokerId AND l3.sEmployeeSecondaryId = @pEmployeeId
  END
  ELSE IF @RoleName = 'Servicing'
  BEGIN
      UPDATE l3
      SET sEmployeeServicingName = @pFullName
        , sEmployeeServicingPhone = @pPhone
        , sEmployeeServicingEmail = @pEmail
      FROM LOAN_FILE_CACHE l JOIN LOAN_FILE_CACHE_3 l3 ON l.sLId = l3.sLId
      WHERE l.sBrokerId = @pBrokerId AND l3.sEmployeeServicingId = @pEmployeeId
  END
  ELSE IF @RoleName = 'Secondary (External)'
  BEGIN
    UPDATE LOAN_FILE_CACHE_2
    SET sEmployeeExternalSecondaryName = @pFullName
      , sEmployeeExternalSecondaryPhone = @pPhone
      , sEmployeeExternalSecondaryEmail = @pEmail
    FROM LOAN_FILE_CACHE l JOIN LOAN_FILE_CACHE_2 l2 on l.sLId = l2.sLId
    WHERE sBrokerId = @pBrokerId AND sEmployeeExternalSecondaryId = @pEmployeeId
  END
  ELSE IF @RoleName = 'Post-Closer (External)'
  BEGIN
    UPDATE LOAN_FILE_CACHE_2
    SET sEmployeeExternalPostCloserName = @pFullName
      , sEmployeeExternalPostCloserPhone = @pPhone
      , sEmployeeExternalPostCloserEmail = @pEmail
    FROM LOAN_FILE_CACHE l JOIN LOAN_FILE_CACHE_2 l2 on l.sLId = l2.sLId
    WHERE sBrokerId = @pBrokerId AND sEmployeeExternalPostCloserId = @pEmployeeId
  END
      IF(0!=@@error)
      BEGIN
            RAISERROR('Error in the update statement in UpdateEmployeeLoanCache sp', 16, 1);
            RETURN -100;
      END
      RETURN 0;
	


