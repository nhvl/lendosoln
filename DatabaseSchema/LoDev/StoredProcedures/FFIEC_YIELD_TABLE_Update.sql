-- =============================================
-- Author:		David Dao
-- Create date: 
-- Description:	
-- =============================================
ALTER PROCEDURE dbo.FFIEC_YIELD_TABLE_Update
	@DataSource int,
    @Type varchar(5),
    @MD5Checksum varchar(50),
    @Content varchar(max)

AS
BEGIN


    UPDATE FFIEC_YIELD_TABLE 
       SET LastModifiedDate = GETDATE(),
           MD5Checksum = @MD5Checksum, 
           Content = @Content
    WHERE DataSource = @DataSource
		AND [Type] = @Type
    
END
