

CREATE PROCEDURE [dbo].[GetCustomFormOwnerByEmployeeId] 
	@CustomLetterID uniqueidentifier,
	@BrokerID uniqueidentifier,
	@EmployeeID uniqueidentifier
AS
	SELECT ''
	FROM Custom_Letter
	WHERE 
		BrokerId = @BrokerID 
		AND OwnerEmployeeId = @EmployeeID
		AND ContentType = 'WORD'
		AND CustomLetterID = @CustomLetterID


