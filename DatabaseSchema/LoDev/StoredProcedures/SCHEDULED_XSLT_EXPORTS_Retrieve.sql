ALTER PROCEDURE [dbo].[SCHEDULED_XSLT_EXPORTS_Retrieve]
    @BrokerId UniqueIdentifier,
    @ExportId int

AS
BEGIN
select 
		a.BrokerId, a.Notes, a.exportid, a.exportname, a.mapname, a.userid, a.reportid,
		b.protocol, b.hostname, b.login, b.password, b.port, b.sshfingerprint, b.destpath,
		b.use_dotnet, b.securitymode, b.sslhostfingerprint, b.usepassive, b.additionalposturl, b.comments, 
		c.[to], c.[from], c.filename, c.subject, c.message,
		br.CustomerCode, br.BrokerNm, au.LoginNm As UserLoginName, r.QueryName
	from 
		dbo.Broker AS br JOIN
		dbo.scheduled_xslt_Export_settings as a  ON br.BrokerId = a.BrokerId JOIN
		dbo.All_User AS au ON a.UserId = au.UserId JOIN
		dbo.Report_Query AS r ON a.reportid = r.QueryId left outer join
		dbo.scheduled_xslt_export_settings_ftp as b on a.exportid = b.exportid left outer join
		dbo.scheduled_xslt_export_settings_email as c on a.exportid = c.exportid
WHERE
    a.ExportId = @ExportId AND a.BrokerId = @BrokerId
END


