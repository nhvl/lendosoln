-- =============================================
-- Author:		Eric Mallare
-- Create date: 7/21/2017
-- Description:	Inserts an EDoc associated with a VOX order.
-- =============================================
ALTER PROCEDURE [dbo].[VOX_DOCUMENT_Insert]
	@OrderId int,
	@DocumentId uniqueidentifier
AS
BEGIN
	INSERT INTO
		VOX_DOCUMENT(OrderId, DocumentId)
	VALUES
		(@OrderId, @DocumentId)
END
