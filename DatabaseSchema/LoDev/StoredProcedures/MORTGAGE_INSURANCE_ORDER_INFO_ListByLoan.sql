
-- =============================================
-- Author:		Brian Beery
-- Create date: 09/19/2014
-- Description:	Retrieves all MI framework transactions in the MORTGAGE_INSURANCE_ORDER_INFO table for the given loan file.
-- =============================================
CREATE PROCEDURE [dbo].[MORTGAGE_INSURANCE_ORDER_INFO_ListByLoan]
	-- Add the parameters for the stored procedure here
	@sLId uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT *
	FROM MORTGAGE_INSURANCE_ORDER_INFO
	WHERE sLId = @sLId
	ORDER BY OrderedDate DESC
END

