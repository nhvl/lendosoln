ALTER PROCEDURE [dbo].[CUSTOM_FOLDER_UPDATE_OR_DELETE]
	@CustomFolder CustomFolderTableType READONLY,
	@FolderIds IntTableType READONLY
AS
BEGIN
	WITH 
		CUSTOM_FOLDER_PARTIAL AS
		(
			SELECT p.FolderId, p.BrokerId, P.EmployeeId, p.InternalName, p.ExternalName, p.LeadLoanStatus, p.SortOrder, p.ParentFolderId
			FROM CUSTOM_FOLDER as p
			JOIN @FolderIds as c ON p.FolderId = c.IntValue
		)
	MERGE CUSTOM_FOLDER_PARTIAL AS TARGET
	USING @CustomFolder AS SOURCE
	ON (TARGET.FolderId = SOURCE.FolderId)
		WHEN MATCHED AND 
			TARGET.InternalName != SOURCE.InternalName OR
			TARGET.ExternalName != SOURCE.ExternalName OR 
			TARGET.LeadLoanStatus != SOURCE.LeadLoanStatus OR
			TARGET.SortOrder != SOURCE.SortOrder OR 
			TARGET.ParentFolderId != SOURCE.ParentFolderId
		THEN UPDATE SET 
			TARGET.InternalName = SOURCE.InternalName,
			TARGET.ExternalName = SOURCE.ExternalName,
			TARGET.LeadLoanStatus = SOURCE.LeadLoanStatus,
			TARGET.SortOrder = SOURCE.SortOrder,
			TARGET.ParentFolderId = SOURCE.ParentFolderId

		WHEN NOT MATCHED BY SOURCE THEN
		DELETE;
END
