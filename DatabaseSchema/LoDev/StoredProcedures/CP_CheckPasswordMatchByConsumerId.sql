




CREATE PROCEDURE [dbo].[CP_CheckPasswordMatchByConsumerId]
	@ConsumerId bigint,
	@PasswordHash varchar(200)
AS

SELECT ConsumerId, Email, BrokerId 
FROM CP_CONSUMER
WHERE	ConsumerId = @ConsumerId	AND
		PasswordHash = @PasswordHash


