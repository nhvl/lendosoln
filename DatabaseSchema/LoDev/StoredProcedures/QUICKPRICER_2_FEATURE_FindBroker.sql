/*
	Author: Scott Kibler
	Created Date: 8/18/2014
	Description: 
*/
CREATE PROCEDURE dbo.QUICKPRICER_2_FEATURE_FindBroker
	@Pml2AsQuickPricerModeIncluded INT = NULL, -- Filter in of this type
	@Pml2AsQuickPricerModeExcluded INT = NULL, -- Filter out of this type.
	@Status BIT = 1
AS
BEGIN
	SELECT BrokerID
	FROM BROKER
	WHERE 
			Status = @Status
		AND (@Pml2AsQuickPricerModeIncluded IS NULL OR Pml2AsQuickPricerMode = @Pml2AsQuickPricerModeIncluded)
		AND (@Pml2AsQuickPricerModeExcluded IS NULL OR Pml2AsQuickPricerMode <> @Pml2AsQuickPricerModeExcluded)
END