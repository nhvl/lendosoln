-- =============================================
-- Author:		Long Nguyen
-- Create date: 8/4/2015
-- Description:	Update schedule report recovery (iOPM 215368): Get detail at current hour
-- =============================================
ALTER procedure [dbo].[GetQueryDetailDaily]
as 
BEGIN
	select r.QueryName, r.BrokerId, s.*, a.IsActive 
	from 
		SCHEDULED_REPORT_USER_INFO s 
		JOIN REPORT_QUERY r ON s.QueryId = r.QueryId 
		JOIN ALL_USER a ON s.UserId = a.UserId 
		JOIN Broker b ON b.BrokerId = r.BrokerId
	where 
		b.Status = 1 AND
		ReportId in(select ReportId 
					from DAILY_REPORTS 
					where NextRun >= dateadd(day, -14, getdate()) and NextRun <= dateadd(minute, 20, getdate()))
END
