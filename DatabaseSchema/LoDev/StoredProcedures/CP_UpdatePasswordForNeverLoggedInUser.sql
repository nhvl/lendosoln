--Per OPM 61505, we want to be able to resend a ConsumerPortal invitiation to users who have never successfully logged in before.
CREATE PROCEDURE [dbo].[CP_UpdatePasswordForNeverLoggedInUser]
	@Email varchar(80),	
	@BrokerId uniqueidentifier, 
	@PasswordHash varchar(200)
AS
BEGIN
	UPDATE CP_CONSUMER
	SET		PasswordHash = @PasswordHash, IsTemporaryPassword = 1, IsLocked = 0, LoginFailureNumber = 0, LastLoggedInD = GETDATE()
	WHERE	Email = @Email AND BrokerId = @BrokerId
END



