-- =============================================
-- Author:		Michael Leinweaver
-- Create date: 10/27/2015
-- Description:	Adds queue threshold monitoring to a queue.
-- =============================================
ALTER PROCEDURE [dbo].[QueueMonitorThreshold_AddAutoEscalationMonitoringToQueue]
	@NewName varchar(50),
	@NewDescription varchar(50),
	@NewThreshold1 int,
	@NewThreshold1DurationInMinutes int,
	@NewThreshold1OldestItemMinutes int,
	@NewThreshold2 int,
	@NewThreshold2DurationInMinutes int,
	@NewThreshold2OldestItemMinutes int
AS
BEGIN
	INSERT INTO QUEUE_MONITOR_THRESHOLD(Name, Description, Threshold1, Threshold1DurationInMinutes, Threshold1OldestItemMinutes, Threshold2, Threshold2DurationInMinutes, Threshold2OldestItemMinutes)
	VALUES(@NewName, @NewDescription, @NewThreshold1, @NewThreshold1DurationInMinutes, @NewThreshold1OldestItemMinutes, @NewThreshold2, @NewThreshold2DurationInMinutes, @NewThreshold2OldestItemMinutes)
END
