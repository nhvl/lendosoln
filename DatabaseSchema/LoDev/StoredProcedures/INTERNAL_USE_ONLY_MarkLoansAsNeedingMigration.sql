

CREATE PROCEDURE [dbo].[INTERNAL_USE_ONLY_MarkLoansAsNeedingMigration]
AS
DECLARE @Cnt Int
DECLARE @Total Int
SET @Total = 0
SET @Cnt = 1
WHILE @Cnt > 0
BEGIN
BEGIN TRANSACTION

update top(1000) loan_file_cache 
set  IsMarkedInvalidated=1
from loan_file_cache lc with(nolock) join broker br with(nolock) on lc.sbrokerid=br.brokerid
where br.status=1 and lc.isvalid=1 and lc.IsMarkedInvalidated=0

SET @Cnt = @@ROWCOUNT
SET @Total = @Total + @Cnt
COMMIT TRANSACTION
END
