CREATE PROCEDURE [dbo].[TASK_TRIGGER_ASSOCIATION_Delete]
	@BrokerId uniqueidentifier,
	@LoanId uniqueidentifier,
	@TaskId varchar(10),
	@TriggerType int
AS
BEGIN
	DELETE FROM TASK_TRIGGER_ASSOCIATION
	WHERE BrokerId = @BrokerId
		AND LoanId = @LoanId
		AND TaskId = @TaskId
		AND TriggerType = @TriggerType
END