CREATE  PROCEDURE [dbo].UpdateBrokerConditionChoices
	@BrokerId Guid , @ConditionChoicesXmlContent Text
AS
	UPDATE
		Broker
	SET
		ConditionChoicesXmlContent = @ConditionChoicesXmlContent
	WHERE
		BrokerId = @BrokerId
	IF( 0!=@@error)
	BEGIN
		RAISERROR('Error in updating Broker table in UpdateBrokerConditionChoices sp', 16, 1);
		RETURN -100;
	END
	RETURN 0;
	
