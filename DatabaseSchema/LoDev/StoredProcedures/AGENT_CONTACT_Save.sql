-- =============================================
-- Author:		Geoff Feltman
-- Create date: 12/22/17
-- Description:	Updates or inserts an agent contact
-- =============================================
ALTER PROCEDURE [dbo].[AGENT_CONTACT_Save]
    @AgentContactId uniqueidentifier,
    @LoanId uniqueidentifier,
    @FirstName varchar(50),
    @LastName varchar(50),
    @FullName varchar(100),
    @Title varchar(21),
    @CompanyName varchar(100),
    @BranchName varchar(100),
    @DepartmentName varchar(100),
    @StreetAddress varchar(36),
    @City varchar(36),
    @CountyFips int,
    @State varchar(2),
    @Zip varchar(9),
    @Phone varchar(21),
    @Fax varchar(21),
    @CellPhone varchar(21),
    @Email varchar(80),
    @IndividualNmlsId varchar(50),
    @ChumsId varchar(10),
    @WireBankName varchar(50),
    @WireBankCity varchar(36),
    @WireBankState varchar(2),
    @WireAbaNumber varchar(9),
    @WireAccountNumber varchar(20),
    @WireAccountName varchar(50),
    @WireFurtherCreditToAccountNumber varchar(20),
    @WireFurtherCreditToAccountName varchar(50)
AS
BEGIN

    UPDATE AGENT_CONTACT
    SET
        FirstName = @FirstName,
        LastName = @LastName,
        FullName = @FullName,
        Title = @Title,
        CompanyName = @CompanyName,
        BranchName = @BranchName,
        DepartmentName = @DepartmentName,
        StreetAddress = @StreetAddress,
        City = @City,
        CountyFips = @CountyFips,
        State = @State,
        Zip = @Zip,
        Phone = @Phone,
        Fax = @Fax,
        CellPhone = @CellPhone,
        Email = @Email,
        IndividualNmlsId = @IndividualNmlsId,
        ChumsId = @ChumsId,
        WireBankName = @WireBankName,
        WireBankCity = @WireBankCity,
        WireBankState = @WireBankState,
        WireAbaNumber = @WireAbaNumber,
        WireAccountNumber = @WireAccountNumber,
        WireAccountName = @WireAccountName,
        WireFurtherCreditToAccountNumber = @WireFurtherCreditToAccountNumber,
        WireFurtherCreditToAccountName = @WireFurtherCreditToAccountName
    FROM AGENT_CONTACT
    WHERE AgentContactId = @AgentContactId 
		AND LoanId = @LoanId

    IF @@rowcount = 0
    BEGIN
        INSERT INTO AGENT_CONTACT
        (
            AgentContactId,
            LoanId,
            FirstName,
            LastName,
            FullName,
            Title,
            CompanyName,
            BranchName,
            DepartmentName,
            StreetAddress,
            City,
            CountyFips,
            State,
            Zip,
            Phone,
            Fax,
            CellPhone,
            Email,
            IndividualNmlsId,
            ChumsId,
            WireBankName,
            WireBankCity,
            WireBankState,
            WireAbaNumber,
            WireAccountNumber,
            WireAccountName,
            WireFurtherCreditToAccountNumber,
            WireFurtherCreditToAccountName
        )
        VALUES
        (
            @AgentContactId,
            @LoanId,
            @FirstName,
            @LastName,
            @FullName,
            @Title,
            @CompanyName,
            @BranchName,
            @DepartmentName,
            @StreetAddress,
            @City,
            @CountyFips,
            @State,
            @Zip,
            @Phone,
            @Fax,
            @CellPhone,
            @Email,
            @IndividualNmlsId,
            @ChumsId,
            @WireBankName,
            @WireBankCity,
            @WireBankState,
            @WireAbaNumber,
            @WireAccountNumber,
            @WireAccountName,
            @WireFurtherCreditToAccountNumber,
            @WireFurtherCreditToAccountName
        )
    END
END
