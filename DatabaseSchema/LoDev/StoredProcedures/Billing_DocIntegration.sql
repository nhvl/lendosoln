CREATE PROCEDURE [dbo].[Billing_DocIntegration]
	@StartDate datetime,
	@EndDate datetime
AS
BEGIN
-- dd 2018-11-02 - For use in automate billing executable.

SELECT DocVendor = 
	                            CASE DocumentVendor
	                            WHEN 1 THEN 'DocuTech'
	                            WHEN 2 THEN 'DocsOnDemand'
	                            WHEN 3 THEN 'IDS'
	                            WHEN 4 THEN 'DocMagic'
	                            WHEN 6 THEN 'ClosingXpress'
	                            WHEN 0 THEN 'UnknownOtherNone'
	                            ELSE 'Unhandled DocumentVendor type'
	                            END, 
                                BrokerNm As LenderName, CustomerCode, sLNm As LoanNumber, cache.sLId As LoanId, BillingEvent =
	                            CASE BillingType
	                            WHEN 0 THEN 'ClosingDocs'
	                            WHEN 1 THEN 'LoanPutInBillableStatus'
	                            ELSE 'Unhandled BillingType'
	                            END, BillingDate
                            FROM BILLING_PER_TRANSACTION bpt WITH (nolock) JOIN Loan_File_Cache cache WITH (nolock) ON cache.sLId = bpt.sLId
                                                                           JOIN Broker WITH (nolock) ON Broker.BrokerId = bpt.BrokerId
                            WHERE BROKER.SuiteType = 1 AND cache.sLoanFileT = 0
                              AND BillingDate >= @StartDate AND BillingDate < @EndDate
                            ORDER BY DocVendor, CustomerCode, BillingDate
							
END							