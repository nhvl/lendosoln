-- =============================================
-- Author:		Timothy Jewell
-- Create date: 9/9/2014
-- Description:	Saves a Generic Framework Vendor, inserting if not currently present.
-- =============================================
ALTER PROCEDURE [dbo].[GENERIC_FRAMEWORK_VENDOR_Save]
	@ProviderID varchar(7),
	@Name varchar(255),
	@LaunchURL varchar(255),
	@CredentialXML varchar(MAX) = NULL,
	@EncryptedCredentialXML varbinary(max) = 0x,
	@EncryptionKeyId uniqueidentifier = NULL, -- This parameter is intentionally optional, and should only be used when inserting
	@IncludeUsername bit,
	@ServiceType int,
	@LoanIdentifierTypeToSend int
AS
BEGIN
	UPDATE GENERIC_FRAMEWORK_VENDOR_CONFIG
	SET
		Name = @Name,
		LaunchURL = @LaunchURL,
		CredentialXML = COALESCE(@CredentialXML, CredentialXML),
		EncryptedCredentialXML = @EncryptedCredentialXML,
		IncludeUsername = @IncludeUsername,
		ServiceType = @ServiceType,
		LoanIdentifierTypeToSend = @LoanIdentifierTypeToSend
	WHERE ProviderID = @ProviderID

	IF @@ROWCOUNT = 0
		INSERT INTO GENERIC_FRAMEWORK_VENDOR_CONFIG
			( ProviderID,  Name,  LaunchURL,  CredentialXML,  EncryptedCredentialXML,  IncludeUsername,  ServiceType,  LoanIdentifierTypeToSend,  EncryptionKeyId)
		VALUES
			(@ProviderID, @Name, @LaunchURL, COALESCE(@CredentialXML, ''), @EncryptedCredentialXML, @IncludeUsername, @ServiceType, @LoanIdentifierTypeToSend, @EncryptionKeyId)
END
