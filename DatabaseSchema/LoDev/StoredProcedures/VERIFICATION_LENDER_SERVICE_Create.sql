-- =============================================
-- Author:		Eric Mallare
-- Create date: 5/8/2017
-- Description:	Creates a new Lender Service and saves it.
-- =============================================
ALTER PROCEDURE [dbo].[VERIFICATION_LENDER_SERVICE_Create]
	@LenderServiceId int OUTPUT,
	@BrokerId uniqueidentifier,
	@ServiceT int,
	@VendorId int,
	@ResellerId int,
	@DisplayName varchar(200),
	@DisplayNameLckd bit,
	@IsEnabledForLqbUsers bit,
	@EnabledEmployeeGroupId uniqueidentifier = null,
	@IsEnabledForTpoUsers bit,
	@EnabledOcGroupId uniqueidentifier = null,
	@AllowPaymentByCreditCard bit,
	@VoeUseSpecificEmployerRecordSearch bit,
	@VoeEnforceUseSpecificEmployerRecordSearch bit,
	@VoaAllowImportingAssets bit,
	@VoaAllowRequestWithoutAssets bit,
	@VoaEnforceAllowRequestWithoutAssets bit,
    @VoeAllowImportingEmployment bit,
    @VoeAllowRequestWithoutEmployment bit,
    @VoeEnforceRequestsWithoutEmployment bit,
	@CanBeUsedForCredentialsInTpo bit

AS
BEGIN
	INSERT INTO VERIFICATION_LENDER_SERVICE
		(BrokerId, ServiceT, VendorId, ResellerId, DisplayName, DisplayNameLckd, IsEnabledForLqbUsers, EnabledEmployeeGroupId,
		 IsEnabledForTpoUsers, EnabledOcGroupId, AllowPaymentByCreditCard, VoeUseSpecificEmployerRecordSearch, VoeEnforceUseSpecificEmployerRecordSearch,
		 VoaAllowImportingAssets,     VoaAllowRequestWithoutAssets,     VoaEnforceAllowRequestWithoutAssets, 
         VoeAllowImportingEmployment, VoeAllowRequestWithoutEmployment, VoeEnforceRequestsWithoutEmployment, 
         CanBeUsedForCredentialsInTpo)
	VALUES
		(@BrokerId, @ServiceT, @VendorId, @ResellerId, @DisplayName, @DisplayNameLckd, @IsEnabledForLqbUsers, @EnabledEmployeeGroupId,
		 @IsEnabledForTpoUsers, @EnabledOcGroupId, @AllowPaymentByCreditCard, @VoeUseSpecificEmployerRecordSearch, @VoeEnforceUseSpecificEmployerRecordSearch,
		 @VoaAllowImportingAssets,     @VoaAllowRequestWithoutAssets,     @VoaEnforceAllowRequestWithoutAssets, 
         @VoeAllowImportingEmployment, @VoeAllowRequestWithoutEmployment, @VoeEnforceRequestsWithoutEmployment,
         @CanBeUsedForCredentialsInTpo)

	SET @LenderServiceId=SCOPE_IDENTITY();
	RETURN;
END