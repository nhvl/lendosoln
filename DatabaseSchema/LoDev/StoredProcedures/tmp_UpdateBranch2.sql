CREATE   PROCEDURE tmp_UpdateBranch2
	 @BranchID GUID,
	 @Name CompanyName,
	 @Address StreetAddress,
	 @City City,
	 @State	 State,
	 @Zipcode Zip,
	 @Phone PhoneNumber,
	 @Fax PhoneNumber
AS 
	UPDATE BRANCH
	SET  BranchNm	 = @Name
		 --BranchAddr  = @Address,
		 --BranchCity  = @City,
		 --BranchState = @State,
		 --BranchZip = @Zipcode,
		 --BranchPhone = @Phone,
		 --BranchFax = @Fax 
	
	WHERE BranchId = @BranchID
	
	
	if( 0!=@@error)
	begin
		RAISERROR('Error in updating Branch table in UpdateBranch sp', 16, 1);
		return -100;
	end
	return 0;
