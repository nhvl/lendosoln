CREATE  PROCEDURE CheckLoginUniqueness
	@LoginName LoginName , @BrokerPmlSiteID Guid = '00000000-0000-0000-0000-000000000000' , @ExcludeUserID Guid = '00000000-0000-0000-0000-000000000000'
AS
	-- 7/18/2005 kb - We now check only within a broker for all 'P' users.  Regular
	-- broker users ('B') must be unique across all non-'P' users.  See case 2331 for
	-- more details.
	SELECT COUNT( LoginNm ) from All_User where LoginNm = @LoginName AND BrokerPmlSiteId = @BrokerPmlSiteId AND UserID <> @ExcludeUserID
	if(0!=@@error)
	begin
		RAISERROR('Error in the select statement in CheckLoginUniqueness sp', 16, 1);
		return -100;
	end
	return 0
