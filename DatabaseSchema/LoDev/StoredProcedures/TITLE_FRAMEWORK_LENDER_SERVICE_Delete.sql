ALTER PROCEDURE [dbo].[TITLE_FRAMEWORK_LENDER_SERVICE_Delete]
    @LenderServiceId int,
    @BrokerId uniqueidentifier
AS BEGIN
DELETE FROM dbo.TITLE_FRAMEWORK_LENDER_SERVICE
WHERE 
    LenderServiceId = @LenderServiceId
    AND BrokerId = @BrokerId
END
