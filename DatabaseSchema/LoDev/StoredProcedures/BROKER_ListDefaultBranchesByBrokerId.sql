CREATE PROCEDURE [dbo].[BROKER_ListDefaultBranchesByBrokerId] @BrokerID uniqueidentifier
AS
	SELECT
		WholesaleBranchID,
		MiniCorrBranchID,
		CorrespondentBranchID
	FROM Broker
	WHERE BrokerID = @BrokerID
	if(0!=@@error)
	begin
		RAISERROR('Error in the selection statement in BROKER_ListDefaultBranchesByBrokerId sp', 16, 1);
		return -100;
	end
	return 0;