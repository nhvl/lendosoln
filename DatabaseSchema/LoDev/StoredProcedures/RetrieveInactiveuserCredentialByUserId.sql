ALTER PROCEDURE [dbo].[RetrieveInactiveUserCredentialByUserId] 
	@UserId uniqueidentifier,
	@BrokerId UniqueIdentifier = NULL
AS

SELECT v.BrokerId, v.UserId, v.EmployeeId, v.Permissions, b.BrokerNm, v.UserFirstNm, v.UserLastNm, 
               v.NeedToAcceptLatestAgreement, v.LoginNm, v.IsSharable, v.PasswordExpirationD, v.LoginSessionID, v.BranchId,
               b.IsOthersAllowedToEditUnderwriterAssignedFile, b.IsLOAllowedToEditProcessorAssignedFile,
               b.HasLONIntegration, b.NHCKey, v.SelectedPipelineCustomReportId, v.LastUsedCreditLoginNm, bu.LastUsedCreditAccountId,
               v.ByPassBgCalcForGfeAsDefault, v.LastUsedCreditProtocolId, v.Type, v.LpePriceGroupId,
	b.IsRateLockedAtSubmission,b.IsOnlyAccountantCanModifyTrustAccount,b.HasLenderDefaultFeatures
, b.IsQuickPricerEnable,bu.IsQuickPricerEnabled AS IsUserQuickPricerEnabled, bu.IsPricingMultipleAppsSupported, b.BillingVersion, bu.PmlBrokerId, bu.PmlLevelAccess
, bu.IsNewPmlUIEnabled, bu.IsUsePml2AsQuickPricer, bu.PortalMode, NULL as MiniCorrespondentBranchId, NULL as CorrespondentBranchId
, bu.OptsToUseNewLoanEditorUI
FROM VIEW_DISABLED_LO_USER v WITH (NOLOCK) JOIN Broker_User bu WITH (NOLOCK) ON v.UserId=bu.UserId JOIN Broker b on v.brokerId = b.brokerId
WHERE v.UserId = @UserId AND (@BrokerId IS NULL OR b.BrokerId = @BrokerId)

UNION

SELECT v.BrokerId, v.UserId, v.EmployeeId, v.Permissions, b.BrokerNm, v.UserFirstNm, v.UserLastNm, 
               v.NeedToAcceptLatestAgreement, v.LoginNm, v.IsSharable, v.PasswordExpirationD, v.LoginSessionID, v.BranchId,
               b.IsOthersAllowedToEditUnderwriterAssignedFile, b.IsLOAllowedToEditProcessorAssignedFile,
               b.HasLONIntegration, b.NHCKey, v.SelectedPipelineCustomReportId, v.LastUsedCreditLoginNm, '', --LastUsedCreditAccountId not defined for PML Users, OPM 118517
               v.ByPassBgCalcForGfeAsDefault, v.LastUsedCreditProtocolId, v.Type, v.LpePriceGroupId,
	b.IsRateLockedAtSubmission,b.IsOnlyAccountantCanModifyTrustAccount,b.HasLenderDefaultFeatures
, b.IsQuickPricerEnable,bu.IsQuickPricerEnabled AS IsUserQuickPricerEnabled, bu.IsPricingMultipleAppsSupported, b.BillingVersion, bu.PmlBrokerId, bu.PmlLevelAccess
, bu.IsNewPmlUIEnabled, bu.IsUsePml2AsQuickPricer, bu.PortalMode, v.MiniCorrespondentBranchId, v.CorrespondentBranchId
, bu.OptsToUseNewLoanEditorUI
FROM VIEW_DISABLED_PML_USER v WITH (NOLOCK) JOIN Broker_User bu WITH (NOLOCK) ON v.UserId = bu.UserId JOIN Broker b on v.brokerId = b.brokerId
WHERE v.UserId = @UserId AND (@BrokerId IS NULL OR b.BrokerId = @BrokerId)

