
CREATE    PROCEDURE [dbo].[GetAccessControlEmployeeRoles] 
	@EmployeeId Guid
AS
	SELECT
		v.RoleId ,
		v.RoleDesc ,
		v.RoleModifiableDesc ,
		v.RoleImportanceRank
	FROM
		View_Employee_Role AS v  WITH (NOLOCK)
	WHERE
		v.EmployeeId = @EmployeeId

