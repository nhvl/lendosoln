-- =============================================
-- Author: Scott Kibler
-- Create date: 5/9/2017
-- Description: 
--	Sets the new conversation log permission level order based on the passed in xml.
--  Does this by first deleting from the xml then adding the levels back one by one.
--  Does not delete all first otherwise someone else could add a permission level and have it deleted.
--  The expected PermissionLevelOrderXml format is:
--    <p id="..." o="..." /><p ...
-- =============================================
CREATE PROCEDURE [dbo].[SetConvoLogPermissionLevelsOrder]
	@BrokerId uniqueidentifier,
	@PermissionLevelOrderXml xml -- format: <p id="..." o="..." /><p ...
AS
BEGIN

	DELETE FROM CONVOLOG_PERMISSION_LEVEL_ORDER
	WHERE
		BrokerId = @BrokerId
	AND PermissionLevelId IN
	(
		SELECT
			tbl.col.value('@id', 'int') as permissionLevelId
		FROM 
			@PermissionLevelOrderXml.nodes('//p') tbl(col)
	)
		
	INSERT INTO 
		CONVOLOG_PERMISSION_LEVEL_ORDER
		(BrokerId, Ordinal, PermissionLevelId)
	SELECT
		@BrokerId as brokerid,
		tbl.col.value('@o', 'int') as ordinal,
		tbl.col.value('@id', 'int') as permissionLevelId
	FROM 
		@PermissionLevelOrderXml.nodes('//p') tbl(col)
	
END