-- =============================================
-- Author:		Scott Kibler
-- Create date: 7/11/2014
-- Description:	
--   Adds a loan to the pool marked as in use (and who's using and when)
--   The loan should already have been created.
--   If it can't find the quickpricer template id passed in, it fails.
-- =============================================
CREATE PROCEDURE dbo.QUICKPRICER_2_LOAN_USAGE_AddInUseSandboxedLoanToPool
	@LoanID UniqueIdentifier,
	@BrokerID UniqueIdentifier,
	@QuickPricerTemplateID UniqueIdentifier,
	@UserIDofUsingUser UniqueIdentifier
AS
BEGIN
	DECLARE @ErrorCode INT
	SET @ErrorCode = 0;
	DECLARE @ErrorMsg varchar(1000)
	
	DECLARE @QuickPricerTemplateFileVersion int
	SELECT @QuickPricerTemplateFileVersion = sFileVersion
	FROM LOAN_FILE_A
	WHERE sLId = @QuickPricerTemplateID
	IF( @@ROWCOUNT <> 1 )
	BEGIN
		 SET @ErrorMsg = 'No loan with the given qp template id found'
		 SET @ErrorCode = -30;
		  
		 GOTO ErrorHandler;
	END 
	  
    INSERT INTO QUICKPRICER_2_LOAN_USAGE
      ( BrokerID,  LoanID, IsInUse, LastUsedD,  UserIDofUsingUser,  QuickPricerTemplateFileVersion,  QuickPricerTemplateID)
    VALUES
      (@BrokerID, @LoanID,		 1, GetDate(), @UserIDofUsingUser, @QuickPricerTemplateFileVersion, @QuickPricerTemplateID)
     
     IF(@@ROWCOUNT <> 1)
     BEGIN
		  SET @ErrorMsg = 'Did not insert loan to pool'
		  SET @ErrorCode = -43;
		  
		  GOTO ErrorHandler;
	 END 
	
	
	RETURN 0
	ErrorHandler:
		Raiserror(@ErrorMsg, 16, 1);
		RETURN @ErrorCode;
END
