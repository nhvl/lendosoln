-- =============================================
-- Author:		Michael Leinweaver
-- Create date: 3/8/2016
-- Description:	Updates relationship information for P-Users
--				that populate their relationships from their
--				OC.
-- =============================================
ALTER PROCEDURE [dbo].[PMLBROKER_UpdateUserRelationships]
	@PmlBrokerId uniqueidentifier,
	@OCRoleT int,
	@ManagerId uniqueidentifier,
	@ProcessorId uniqueidentifier,
	@JuniorProcessor uniqueidentifier,
	@LenderAccountExecId uniqueidentifier,
	@UnderwriterId uniqueidentifier,
	@JuniorUnderwriterId uniqueidentifier,
	@LockDeskId uniqueidentifier,
	@CreditAuditorId uniqueidentifier,
	@LegalAuditorId uniqueidentifier,
	@PurchaserId uniqueidentifier,
	@SecondaryId uniqueidentifier
AS
BEGIN
	DECLARE @UPDATEEMPLOYEES TABLE (EmployeeId uniqueidentifier)
	
	INSERT INTO @UPDATEEMPLOYEES(EmployeeId)
	SELECT e.EmployeeId
	FROM EMPLOYEE e JOIN Broker_User bu ON e.EmployeeId = bu.EmployeeId
	WHERE 
		bu.PmlBrokerId = @PmlBrokerId AND
		((e.PopulateBrokerRelationshipsT = 0 AND @OCRoleT = 0) OR 
		(e.PopulateMiniCorrRelationshipsT = 0 and @OCRoleT = 1) OR 
		(e.PopulateCorrRelationshipsT = 0 AND @OCRoleT = 2))
	
	-- Broker relationships
	IF @OCRoleT = 0
	BEGIN
		UPDATE Broker_User
		SET
			ManagerEmployeeId = @ManagerId,
			ProcessorEmployeeId = @ProcessorId,
			JuniorProcessorEmployeeID = @JuniorProcessor,
			LenderAccExecEmployeeId = @LenderAccountExecId,
			UnderwriterEmployeeId = @UnderwriterId,
			JuniorUnderwriterEmployeeID = @JuniorUnderwriterId,
			LockDeskEmployeeId = @LockDeskId
		WHERE EmployeeId IN (SELECT * FROM @UPDATEEMPLOYEES)
	END
	
	-- Mini-Corr relationships
	IF @OCRoleT = 1
	BEGIN
		UPDATE Broker_User
		SET
			MiniCorrManagerEmployeeId = @ManagerId,
			MiniCorrProcessorEmployeeId = @ProcessorId,
			MiniCorrJuniorProcessorEmployeeId = @JuniorProcessor,
			MiniCorrLenderAccExecEmployeeId = @LenderAccountExecId,
			MiniCorrUnderwriterEmployeeId = @UnderwriterId,
			MiniCorrJuniorUnderwriterEmployeeId = @JuniorUnderwriterId,
			MiniCorrCreditAuditorEmployeeId = @CreditAuditorId,
			MiniCorrLegalAuditorEmployeeId = @LegalAuditorId,
			MiniCorrLockDeskEmployeeId = @LockDeskId,
			MiniCorrPurchaserEmployeeId = @PurchaserId,
			MiniCorrSecondaryEmployeeId = @SecondaryId
		WHERE EmployeeId IN (SELECT * FROM @UPDATEEMPLOYEES)
	END
	
	-- Corr relationships
	IF @OCRoleT = 2
	BEGIN
		UPDATE Broker_User
		SET
			CorrManagerEmployeeId = @ManagerId,
			CorrProcessorEmployeeId = @ProcessorId,
			CorrJuniorProcessorEmployeeId = @JuniorProcessor,
			CorrLenderAccExecEmployeeId = @LenderAccountExecId,
			CorrUnderwriterEmployeeId = @UnderwriterId,
			CorrJuniorUnderwriterEmployeeId = @JuniorUnderwriterId,
			CorrCreditAuditorEmployeeId = @CreditAuditorId,
			CorrLegalAuditorEmployeeId = @LegalAuditorId,
			CorrLockDeskEmployeeId = @LockDeskId,
			CorrPurchaserEmployeeId = @PurchaserId,
			CorrSecondaryEmployeeId = @SecondaryId
		WHERE EmployeeId IN (SELECT * FROM @UPDATEEMPLOYEES)
	END
END
GO
