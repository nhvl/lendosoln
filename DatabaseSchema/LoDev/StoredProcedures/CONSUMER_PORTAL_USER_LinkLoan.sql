-- =============================================
-- Author:		Antonio Valencia
-- Create date: 6/2/2013
-- Description:	Gets loans consumer user has access to. Ensures only shows up once per loan
-- =============================================
ALTER PROCEDURE [dbo].[CONSUMER_PORTAL_User_LinkLoan]
	@ConsumerUserId bigint,
	@sLid uniqueidentifier
AS
BEGIN
	DELETE FROM CONSUMER_PORTAL_USER_X_LOANAPP WHERE sLId = @sLid and ConsumerUserId = @ConsumerUserId
	
	INSERT INTO CONSUMER_PORTAL_USER_X_LOANAPP(ConsumerUserId, sLid)
	VALUES(@ConsumerUserId, @sLid)
END
