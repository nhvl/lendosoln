
CREATE procedure [dbo].[QCreate]  
(
	@Name varchar(50), 
	@RequiresArchive bit 
)

AS 

	INSERT INTO Q_TYPE(QueueName, RequiresArchive) VALUES(@Name, @RequiresArchive); 
	return SCOPE_IDENTITY(); 



