ALTER PROCEDURE [dbo].[SERVICE_FILE_RECEIVED_DOCUMENTS_SaveNewDocument]
	@BrokerId uniqueidentifier,
	@LoanId uniqueidentifier,
	@AppId uniqueidentifier,
	@VendorId uniqueidentifier,
	@ReportId varchar(21),
	@FileName varchar(50),
	@DocumentId uniqueidentifier,
	@DocumentChecksum char(64)
AS
BEGIN
	INSERT INTO SERVICE_FILE_RECEIVED_DOCUMENTS (
		BrokerId,
		LoanId,
		AppId,
		VendorId,
		ReportId,
		FileName,
		DocumentId,
		DocumentChecksum
	) VALUES (
		@BrokerId,
		@LoanId,
		@AppId,
		@VendorId,
		@ReportId,
		@FileName,
		@DocumentId,
		@DocumentChecksum
	);
END
