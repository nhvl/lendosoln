



alter PROCEDURE [dbo].[CP_RetrieveConsumerIdByPasswordResetToken]
	@LoginName varchar(80),
	@PasswordResetCode varchar(200)
AS

SELECT ConsumerId, Email, cp.BrokerId, PasswordRequestD, GETDATE() AS CurrentTimeD 
FROM CP_CONSUMER cp
join broker br on cp.brokerid = br.brokerid
WHERE	Email = @LoginName		AND
		PasswordResetCode = @PasswordResetCode and 
		br.status = 1

