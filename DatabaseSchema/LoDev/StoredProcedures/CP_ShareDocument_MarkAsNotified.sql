-- =============================================
-- Author:		Antonio Valencia
-- Create date: 10/28/2013
-- Description:	Marks a shared document as notified
-- =============================================
CREATE PROCEDURE [dbo].[CP_ShareDocument_MarkAsNotified]
	@BrokerId uniqueidentifier,
	@Id bigint
AS
BEGIN
	SET NOCOUNT ON;
	
	UPDATE CP_Share_Document 
	SET [ShareStatusT] = 1
	WHERE @BrokerId = BrokerId   AND @Id = Id
	
	SELECT @@rowcount
END
