-- =============================================
-- Author:		Geoffrey Feltman
-- Create date: 6/11/14
-- Description:	Retrieves all of the pending document requests for a loan.
--              Gets the friendly descriptions of the EDoc info and borrower names.
-- =============================================
CREATE PROCEDURE [dbo].[PENDING_DOCUMENT_REQUEST_RetrieveByLoanId]
	@LoanId uniqueidentifier,
	@BrokerId uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT
		Id,
		LoanId,
		ApplicationId,
		RequestType,
		Description,
		ScheduledSendTime,
		DocTypeName AS DocTypeDescription,
		FolderName AS FolderDescription,
		CoborrowerTitleBorrowerId,
		ErrorMessage,
		aBFirstNm + ' ' + aBLastNm AS BorrowerFullName,
		aCFirstNm + ' ' + aCLastNm AS CoborrowerFullName,
		aBEmail AS BorrowerEmail,
		aCEmail AS CoborrowerEmail
	FROM PENDING_DOCUMENT_REQUEST p
		LEFT OUTER JOIN EDOCS_DOCUMENT_TYPE dt on p.DocTypeId = dt.DocTypeId
		LEFT OUTER JOIN EDOCS_FOLDER f on dt.FolderId = f.FolderId
		JOIN APPLICATION_A a on p.ApplicationId = a.aAppId
	WHERE LoanId = @LoanId
		AND p.BrokerId = @BrokerId
END
