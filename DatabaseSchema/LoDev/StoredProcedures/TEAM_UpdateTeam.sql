CREATE PROCEDURE [dbo].[TEAM_UpdateTeam]
	@Id		uniqueidentifier,
	@BrokerId uniqueidentifier,
	@RoleId uniqueidentifier = null,
	@Name varchar(300) = null,
	@Notes varchar(max) = null
AS

	IF @RoleId IS NOT NULL
	AND EXISTS (SELECT TOP 1 * FROM TEAM_USER_ASSIGNMENT WHERE TeamId = @Id)
	BEGIN
		RAISERROR('Team Role cannot be changed when users are assigned to it.', 16, 1);
		return -100	
	END

	UPDATE TEAM
	SET
		RoleId = COALESCE(@RoleId, RoleId),
		Name = COALESCE(@Name, Name),
		Notes = COALESCE(@Notes, Notes)
		
	WHERE
		@Id = Id
		AND
		@BrokerId = BrokerId


	IF 0 = @@ROWCOUNT
	BEGIN
		RAISERROR('Team does not exist.', 16, 1);
		return -100
	END

	RETURN 0;