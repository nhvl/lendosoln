ALTER PROCEDURE [dbo].[SERVICE_CREDENTIAL_ListByBrokerId] 
	@BrokerID uniqueidentifier,
	@IsForCreditReports bit = null,
	@IsForVerifications bit = null,
	@IsForAusSubmission bit = null,
	@IsForTitleQuotes bit = NULL,
	@IsForDocumentCapture bit = null,
	@IsForUcdDelivery bit = null,
	@IsForDigitalMortgage bit = null
AS

	SELECT
	Id,
	ServiceProviderId,
	ServiceProviderName,
	UserType,
	AccountId,
	UserName,
	UserPassword,
	EncryptionKeyId,
	IsForCreditReports,
	IsForVerifications,
	VoxVendorId,
	VoxVendorName,
	IsForAusSubmission, 
	AusOption,
	IsEnabledForNonSeamlessDu,
	IsForDocumentCapture,
	IsForTitleQuotes,
	TitleQuoteLenderServiceId,
	TitleQuoteLenderServiceName,
	sc.IsForUcdDelivery,
	sc.UcdDeliveryTarget,
	IsForDigitalMortgage,
	DigitalMortgageProviderTarget,
	null as BranchId,
	null as PmlBrokerId,
	null as EmployeeId
	FROM
		SERVICE_CREDENTIAL sc
		JOIN BROKER_X_SERVICE_CREDENTIAL xsc
			ON sc.Id = xsc.ServiceCredentialId
			AND sc.BrokerId = xsc.BrokerId
	WHERE 
		@BrokerId = xsc.BrokerId AND
		(@IsForCreditReports IS NULL OR @IsForCreditReports=sc.IsForCreditReports) AND
		(@IsForVerifications IS NULL OR @IsForVerifications=sc.IsForVerifications) AND
		(@IsForAusSubmission IS NULL OR @IsForAusSubmission=sc.IsForAusSubmission) AND 
		(@IsForTitleQuotes IS NULL OR @IsForTitleQuotes=sc.IsForTitleQuotes) AND
		(@IsForDocumentCapture IS NULL OR @IsForDocumentCapture=sc.IsForDocumentCapture) AND
		(@IsForUcdDelivery IS NULL OR @IsForUcdDelivery = sc.IsForUcdDelivery) AND
		(@IsForDigitalMortgage IS NULL OR @IsForDigitalMortgage = sc.IsForDigitalMortgage)
