
-- =============================================
-- Author:		Justin Jia
-- ALTER date: June 14, 2012
-- Description:	Gets all unique indexes and its corresponding tables and columns.
-- =============================================
CREATE PROCEDURE [dbo].[Justin_Testing_GetAllUniqueIndexColumnNames]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT       distinct tableName = obj.name , indexName = idx.name , columnName = col.name
	FROM         sys.objects AS obj INNER JOIN
				 sys.columns AS col ON col.object_id = obj.object_id INNER JOIN
				 sys.index_columns AS idx_cols ON idx_cols.column_id = col.column_id AND idx_cols.object_id = col.object_id INNER JOIN
				 sys.indexes AS idx ON idx_cols.index_id = idx.index_id AND idx.object_id = col.object_id
	WHERE        idx.is_unique = 1 and idx.is_primary_key = 0 and obj.type = 'u'

END

