CREATE PROCEDURE [dbo].[ListLoFormByBrokerId] 
	@BrokerId UniqueIdentifier
AS
BEGIN
	SELECT FormId, Description, IsSignable, IsStandardForm, FileName
	FROM LO_FORM
	Where BrokerId = @BrokerId
	ORDER BY Description
END
