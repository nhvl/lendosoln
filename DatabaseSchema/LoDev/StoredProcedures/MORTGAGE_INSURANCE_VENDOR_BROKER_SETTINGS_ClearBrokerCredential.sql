-- =============================================
-- Author:		Isaac Ribakoff
-- Create date: 9/18/2014
-- Description:	Removes Broker Credentials For Specific MI Vendor
-- =============================================
ALTER PROCEDURE [dbo].[MORTGAGE_INSURANCE_VENDOR_BROKER_SETTINGS_ClearBrokerCredential]
	-- Add the parameters for the stored procedure here
	@BrokerId uniqueidentifier,
	@VendorId uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DELETE FROM MORTGAGE_INSURANCE_VENDOR_BROKER_SETTINGS
	WHERE BrokerId = @BrokerId
	AND VendorId = @VendorId

END
