CREATE PROCEDURE [dbo].[DocMagic_XPath_Save]
	@XPath varchar(150),
	@DefaultURL varchar(100),
	@AltURL varchar(100),
	@URLDependsOnFieldId varchar(100),
	@FieldId varchar(100)
AS
BEGIN
	UPDATE DOCMAGIC_XPATH_MAPPINGS
	SET XPath = @XPath,
		DefaultURL = @DefaultURL,
		AltURL = @AltURL,
		URLDependsOnFieldId = @URLDependsOnFieldId,
		FieldId = @FieldId
	WHERE XPath = @XPath;
		
	IF @@rowcount = 0
	BEGIN
		insert DOCMAGIC_XPATH_MAPPINGS 
		(
			XPath,
			DefaultURL,
			AltURL,
			URLDependsOnFieldId,
			FieldId
		)
		values							
		(
			@XPath,
			@DefaultURL,
			@AltURL,
			@URLDependsOnFieldId,
			@FieldId
		)
	END
END
