-- =============================================
-- Author:		David Dao
-- Create date: 4/4/2015
-- Description:	List lLpTemplateId in the price group.
-- =============================================


CREATE  PROCEDURE [dbo].[LPE_PRICE_GROUP_ListLpTemplateIdByPriceGroupId] 
	@LpePriceGroupId uniqueidentifier
AS

SELECT lLpTemplateId FROM LPE_PRICE_GROUP_PRODUCT WHERE LpePriceGroupId = @LpePriceGroupId AND IsValid = 1