CREATE PROCEDURE [dbo].[UpdateLender]
             @LenderID guid,
	@Name CompanyName, 
              @Address varchar(60), 
              @City City, 
              @State State, 
              @Zipcode Zip, 
              @Phone PhoneNumber, 
              @Fax PhoneNumber, 
              @LicenseNumber LongId,
              @BillingFirstName PartName,
              @BillingLastName PartName,
              @BillingEmail EmailAddress80,
              @BillingPhone PhoneNumber,
              @BillingFax PhoneNumber,
              @BillingAddress StreetAddress,
              @BillingCity  City,
              @BillingState State,
              @BillingZipcode Zip,
	@CreditReportURL URL
AS
	UPDATE BROKER
	SET        BrokerNm =@Name, 
	               BrokerAddr =@Address, 
	               BrokerCity =@City, 
	               BrokerState =@State, 
	               BrokerZip =@Zipcode, 
	               BrokerPhone =@Phone, 
	               BrokerFax=@Fax, 
	               BrokerLicNum =@LicenseNumber,
	               BillFirstNm =@BillingFirstName,
	               BillLastNm =@BillingLastName,
	               BillEmail =@BillingEmail,
	               BillPhone =@BillingPhone,
	               BillFax =@BillingFax,
	               BillAddr =@BillingAddress,
	               BillCity =@BillingCity,
	               BillState =@BillingState,
	               BillZip =@BillingZipcode,
	               CreditReportURL = @CreditReportURL
	FROM Broker
	WHERE BrokerID = @LenderID
	if( 0!=@@error)
	begin
		RAISERROR('Error in updating Broker table in UpdateBroker sp', 16, 1);
		return -100;
	end
	return 0;
