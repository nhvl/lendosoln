-- =============================================
-- Author:		Michael Leinweaver
-- Create date: 9/13/2017
-- Description:	Saves OCR request data.
-- =============================================
ALTER PROCEDURE [dbo].[EDOCS_OCR_REQUEST_Save]
	@OcrRequestId uniqueidentifier,
	@BrokerId uniqueidentifier,
	@LoanId uniqueidentifier,
	@AppId uniqueidentifier,
	@Origin tinyint,
	@DocumentSource tinyint,
	@UploadingUserId uniqueidentifier = NULL,
	@UploadingUserType char(1)
AS
BEGIN
	INSERT INTO EDOCS_OCR_REQUEST(
		OcrRequestId, 
		BrokerId, 
		LoanId, 
		AppId, 
		Origin, 
		DocumentSource, 
		UploadingUserId, 
		UploadingUserType)
    VALUES(
		@OcrRequestId, 
		@BrokerId, 
		@LoanId, 
		@AppId, 
		@Origin, 
		@DocumentSource, 
		@UploadingUserId, 
		@UploadingUserType)
END
