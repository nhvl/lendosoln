-- =============================================
-- Author:		Matthew Pham
-- Create date: 8/31/11
-- Description:	Fetch trades for tracking in the MBS-TBA market
-- =============================================
CREATE PROCEDURE [dbo].[MBS_FetchTrades] 
	-- Add the parameters for the stored procedure here
	@BrokerId uniqueidentifier = null, 
	@TradeId uniqueidentifier = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	IF (@BrokerId IS NOT NULL AND @TradeId IS NOT NULL) BEGIN
		SELECT *
		FROM MBS_TRADE
		WHERE BrokerId = @BrokerId AND
			  TradeId = @TradeId
	END

    IF (@BrokerId IS NOT NULL AND @TradeId IS NULL) BEGIN
		SELECT *
		FROM MBS_TRADE
		WHERE BrokerId = @BrokerId
	END

	IF (@BrokerId IS NULL AND @TradeId IS NOT NULL) BEGIN
		SELECT *
		FROM MBS_TRADE
		WHERE TradeId = @TradeId
	END
END
