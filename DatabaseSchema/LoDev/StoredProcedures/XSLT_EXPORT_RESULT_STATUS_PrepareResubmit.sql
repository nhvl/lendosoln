-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE dbo.XSLT_EXPORT_RESULT_STATUS_PrepareResubmit 
	@ReportId varchar(13),
	@UserId UniqueIdentifier
AS
BEGIN
	UPDATE XSLT_EXPORT_RESULT_STATUS
		SET CreatedDate =GetDate(),
		     Status=0,
		     ExecutionInMs =0
	WHERE ReportId=@ReportId AND UserId=@UserId
	
	SELECT RequestXml FROM XSLT_EXPORT_RESULT_STATUS WHERE ReportId=@ReportId AND UserId=@UserId
END
