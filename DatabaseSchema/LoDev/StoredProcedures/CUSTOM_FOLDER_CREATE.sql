ALTER PROCEDURE [dbo].CUSTOM_FOLDER_CREATE
	@BrokerId uniqueidentifier,
	@EmployeeId uniqueidentifier = null,
	@InternalName varchar(50),
	@ExternalName varchar(50),
	@LeadLoanStatus tinyint,
	@SortOrder smallint,
	@ParentFolderId int = null
AS
BEGIN
	INSERT INTO CUSTOM_FOLDER
	(
		BrokerId,
		EmployeeId,
		InternalName,
		Externalname,
		LeadLoanStatus,
		SortOrder,
		ParentFolderId
	)
	OUTPUT INSERTED.FolderId
	VALUES
	(
		@BrokerId,
		@EmployeeId,
		@InternalName,
		@Externalname,
		@LeadLoanStatus,
		@SortOrder,
		@ParentFolderId
	)
END
