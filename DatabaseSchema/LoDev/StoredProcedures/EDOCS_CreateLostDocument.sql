-- =============================================
-- Author:		Antonio Valencia
-- Create date: January 18, 2009
-- Description:	Creates a new Lost Document
-- =============================================
ALTER PROCEDURE [dbo].[EDOCS_CreateLostDocument] 
	-- Add the parameters for the stored procedure here
	@DocumentId uniqueidentifier,
 	@BrokerId uniqueidentifier = null,
	@LoanId uniqueidentifier = null, 
	@DocTypeId int = null,
	@FaxNumber varchar(14),
	@Description varchar(200),
	@NumPages int,
	@CreatedDate Datetime,
	@FileDbKey uniqueidentifier,
	@DocumentSource int,
	@IsESigned bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO EDOCS_LOST_DOCUMENT(DocumentId, BrokerId, sLId, DocTypeId,  FaxNumber, FileDBKey_OriginalDocument, Description, NumPages, CreatedDate, DocumentSource, IsESigned)
	VALUES(@DocumentId, @BrokerId, @LoanId, @DocTypeId, @FaxNumber, @FileDbKey, @Description, @NumPages, @CreatedDate, @DocumentSource, @IsESigned)

END
