-- =============================================
-- Author:		Justin Lara
-- Create date: 3/7/2017
-- Description:	Retrieves the specified AUS order.
-- =============================================
ALTER PROCEDURE [dbo].[AUS_ORDERS_RetrieveById]
	@AusOrderId int,
	@BrokerId uniqueIdentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT
		AusOrderId,
		LoanId,
		BrokerId,
		IsManual,
		CustomPosition,
		UnderwritingService,
		UnderwritingServiceOtherDescription,
		DuRecommendation,
		LpRiskClass,
		LpPurchaseEligibility,
		LpStatus,
		LpFeeLevel,
		TotalRiskClass,
		TotalEligibilityAssessment,
		GusRecommendation,
		GusRiskEvaluation,
		ResultOtherDescription,
		CaseId,
		FindingsDocument,
		OrderPlacedBy,
		Timestamp,
		Notes,
        LoanIsFha
	FROM AUS_ORDERS
	WHERE AusOrderId = @AusOrderId AND BrokerId = @BrokerId
END
