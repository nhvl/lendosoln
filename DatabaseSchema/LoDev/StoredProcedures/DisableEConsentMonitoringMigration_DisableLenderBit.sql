-- =============================================
-- Author:		Michael Leinweaver
-- Create date: 11/29/2018
-- Description:	Disables e-consent monitoring automation
--				for a lender.
-- =============================================
CREATE PROCEDURE [dbo].[DisableEConsentMonitoringMigration_DisableLenderBit]
	@BrokerId uniqueidentifier
AS
BEGIN
	UPDATE BROKER
	SET EnableEConsentMonitoringAutomation = 0
	WHERE BrokerId = @BrokerId
END
