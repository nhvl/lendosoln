

CREATE PROCEDURE [dbo].[RetrieveUserIdByPmlLoginNameAndBrokerId]
	@LoginName varchar(36),
	@BrokerId uniqueidentifier
AS
SELECT UserId
FROM View_Active_Pml_User_With_Broker_Info
WHERE LoginNm = @LoginName AND BrokerId = @BrokerId


