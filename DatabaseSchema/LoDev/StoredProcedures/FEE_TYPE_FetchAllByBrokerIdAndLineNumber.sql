CREATE PROCEDURE [dbo].[FEE_TYPE_FetchAllByBrokerIdAndLineNumber]
	@BrokerId		uniqueidentifier,
	@GfeLineNumber	varchar(5) = NULL
AS
	SELECT *
	FROM FEE_TYPE
	WHERE BrokerId = @BrokerId
	AND (
			@GfeLineNumber IS NULL
			OR
			@GfeLineNumber = ''
			OR
			GfeLineNumber = @GfeLineNumber
		)