CREATE procedure [dbo].[UserCustomReports] 
@EmployeeId varchar(50)=NULL
as 
select QueryName,r.EmployeeId ,QueryId , Email
from 
REPORT_QUERY r JOIN 
EMPLOYEE e
on e.EmployeeId = r.EmployeeId
where r.EmployeeId =@EmployeeId