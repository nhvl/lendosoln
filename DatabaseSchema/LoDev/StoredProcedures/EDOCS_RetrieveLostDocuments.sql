-- =============================================
-- Author:		Antonio Valencia
-- Create date: 1/25/10
-- Description:	Retrieves Lost Documents
-- =============================================
ALTER PROCEDURE [dbo].[EDOCS_RetrieveLostDocuments] 
	@StartingRecord int,
	@EndingRecord int,
	@TotalCount int output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;



	SELECT @TotalCount = count(*) FROM EDOCS_LOST_DOCUMENT
	SELECT DocumentId, sLId, BrokerId, DocTypeId, FaxNumber, FileDBKey_OriginalDocument, Description, NumPages, CreatedDate, BrokerNm, DocTypeName, DocumentSource, IsESigned
	FROM 
		(SELECT  Row_Number() OVER(ORDER BY CreatedDate DESC) as RowID,  
			DocumentId, sLId, b.BrokerId , e.DocTypeId, FaxNumber, FileDBKey_OriginalDocument, Description, NumPages, CreatedDate, b.BrokerNm, d.DocTypeName, DocumentSource, IsESigned
			FROM EDOCS_LOST_DOCUMENT as e 
			LEFT OUTER JOIN broker as b on b.BrokerId = e.BrokerId 
			LEFT OUTER JOIN EDOCS_DOCUMENT_TYPE as D ON d.DocTypeId = e.DocTypeId
		)

 AS RowResults

			WHERE RowID BETWEEN @StartingRecord AND @EndingRecord
	
	
END
