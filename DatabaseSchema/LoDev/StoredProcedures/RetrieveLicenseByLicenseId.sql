
CREATE PROCEDURE [dbo].[RetrieveLicenseByLicenseId]
	@LicenseId Guid
AS
BEGIN
	SELECT Description, SeatCount, ExpirationDate, LicenseNumber FROM License WHERE LicenseId=@LicenseId
END

