-- =============================================
-- Author:		Matthew Pham
-- Create date: 10/19/12
-- Description:	Retrieves PNG entries in EDOCS_PNG_ENTRY by DocId
-- =============================================
CREATE PROCEDURE [dbo].[EDOCS_RetrievePNGEntriesByDocId]
	-- Add the parameters for the stored procedure here
	@DocumentId uniqueidentifier 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT PngKey
	FROM EDOCS_PNG_ENTRY
	WHERE documentid=@DocumentId
END
