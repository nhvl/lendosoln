CREATE PROCEDURE [dbo].[FEE_TYPE_Update]
	@FeeTypeId		uniqueidentifier,
	@BrokerId		uniqueidentifier = NULL,
	@Description	varchar(max) = NULL,
	@GfeLineNumber	varchar(5) = NULL,
	@GfeSection		int = NULL,
	@Props			int = NULL
AS
	UPDATE FEE_TYPE
	SET
		[Description] = COALESCE(@Description, [Description]),
		GfeLineNumber = COALESCE(@GfeLineNumber, GfeLineNumber),
		GfeSection = COALESCE(@GfeSection, GfeSection),
		Props = COALESCE(@Props, Props)
	WHERE
		FeeTypeId = @FeeTypeId
		AND (
			@BrokerId IS NULL
			OR
			BrokerId = @BrokerId
		)
	
	IF 0 != @@ERROR
	BEGIN
		RAISERROR('Error updating FEE_TYPE in Update sp', 16, 1);
		rollback transaction
		return -100
	END

	RETURN 0;