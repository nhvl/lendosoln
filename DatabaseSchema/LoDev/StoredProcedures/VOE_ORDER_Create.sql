-- =============================================
-- Author:		Timothy Jewell
-- Create date:	05/23/2017
-- Description:	Creates a VOE order.  Be sure to wrap this in a transaction with other inserts.
-- =============================================
ALTER PROCEDURE [dbo].[VOE_ORDER_Create]
	@OrderId int,
	@VerificationType int,
	@BorrowerName varchar(60),
	@ParentOrderNumber varchar(50),
	@Last4Ssn varchar(4)
AS
BEGIN
	INSERT INTO VOE_ORDER
		(OrderId, VerificationType, BorrowerName, ParentOrderNumber, Last4Ssn)
	VALUES
		(@OrderId, @VerificationType, @BorrowerName, @ParentOrderNumber, @Last4Ssn)
END