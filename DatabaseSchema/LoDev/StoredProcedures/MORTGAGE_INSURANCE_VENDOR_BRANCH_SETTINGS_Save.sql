-- =============================================
-- Author:		Isaac Ribakoff
-- Create date: 9/19/2014
-- Description:	Saves MI Vendor Branch Settings
-- =============================================
ALTER PROCEDURE [dbo].[MORTGAGE_INSURANCE_VENDOR_BRANCH_SETTINGS_Save]
	@VendorId uniqueidentifier,
	@BranchId uniqueidentifier,
	@UserName varchar(50),
	@Password varbinary(max) = 0x, -- Not null column, but the code uses null
	@EncryptionKeyId uniqueidentifier,
	@AccountId varchar(50),
	@PolicyId varchar(50),
	@IsDelegated bit
AS
BEGIN
	UPDATE MORTGAGE_INSURANCE_VENDOR_BRANCH_SETTINGS
	SET UserName = @UserName,
		Password = @Password,
		EncryptionKeyId = @EncryptionKeyId,
		AccountId = @AccountId,
		PolicyId = @PolicyId,
		IsDelegated = @IsDelegated
	WHERE VendorId = @VendorId
		AND BranchId = @BranchId
	
	IF @@rowcount = 0
	BEGIN
		INSERT INTO MORTGAGE_INSURANCE_VENDOR_BRANCH_SETTINGS
		(
			VendorId,
			BranchId,
			UserName,
			Password,
			EncryptionKeyId,
			AccountId,
			PolicyId,
			IsDelegated
		)
		Values
		(
			@VendorId,
			@BranchId,
			@UserName,
			@Password,
			@EncryptionKeyId,
			@AccountId,
			@PolicyId,
			@IsDelegated
		)
	END
END
