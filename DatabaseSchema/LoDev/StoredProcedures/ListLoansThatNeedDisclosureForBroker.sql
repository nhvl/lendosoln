-- =============================================
-- Author:		Geoffrey Feltman
-- Create date: 4/19/13
-- Description:	Get all of the loans that need disclosure today for the given broker.
-- =============================================
CREATE PROCEDURE [dbo].[ListLoansThatNeedDisclosureForBroker]
	-- Add the parameters for the stored procedure here
	@BrokerId uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    -- Get the DATE portion of today & tomorrow without the time.
    DECLARE @Today DATETIME
    SELECT @Today = DATEDIFF(DAY, 0, GETDATE())
    DECLARE @Tomorrow DATETIME
    SELECT @Tomorrow = DATEADD(DAY, 1, @Today)
    
	SELECT lfc.sLId
	FROM LOAN_FILE_CACHE lfc 
	JOIN LOAN_FILE_CACHE_3 lfc3 
	ON lfc.sLId = lfc3.sLId
	join loan_file_cache_2 lfc2
	on lfc.slid = lfc2.slid
	WHERE lfc.sBrokerId = @BrokerId
		AND sLoanFileT = 0
		AND (sNeedInitialDisc = 1 OR sNeedRedisc = 1)
		AND sDisclosuresDueD < @Tomorrow
		AND lfc.IsValid = 1
		AND lfc.sStatusT NOT IN 
		(
			6,  /* Loan Funded */
			7,  /* Loan On-Hold */
			9,  /* Loan Canceled */
			10, /* Loan Denied */
			11, /* Loan Closed */
			15, /* Lead Canceled */
			16, /* Lead Declined */
			17, /* Lead Other */
			18, /* Loan Other */
			19, /* Recorded */
			20, /* Loan Shipped */
			24, /* Docs Back */
			25, /* Funding Conditions */
			26, /* Final Docs */
			27, /* Loan Sold */
			37, /* Investor Conditions */
			38, /* Investor Conditions Sent */
			39, /* Ready for Sale*/
			40, /* Submitted for Purchase Review */
			41, /* In Purchase Review */
			42, /* Pre-Purchase Conditions */
			43, /* Submitted for Final Purchase Review */
			44, /* In Final Purchase Review */
			45, /* Clear to Purchase */
			46, /* Loan Purchased */
			47, /* Counter Offer Approved */
			48, /* Loan Withdrawn */
			49  /* Loan Archived */
		)
		and lfc2.sBranchChannelT != 3 -- correspondent
		/* OPM 149050 - FirstTech wants HELOCs to be included in this again. */
		/* Assume loan isn't a HELOC if sIsLineOfCredit is NULL in cache. */
		--AND (lfc3.sIsLineOfCredit = 0 OR lfc3.sIsLineOfCredit IS NULL)
END
