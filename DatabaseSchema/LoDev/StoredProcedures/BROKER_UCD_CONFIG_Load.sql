ALTER PROCEDURE [dbo].[BROKER_UCD_CONFIG_Load]
    @BrokerId uniqueidentifier
AS
SELECT ucd.*, b.RelationshipWithFreddieMac
FROM dbo.BROKER_UCD_CONFIG ucd 
    INNER JOIN dbo.[BROKER] b on ucd.BrokerId = b.BrokerId
WHERE ucd.BrokerId = @BrokerId
