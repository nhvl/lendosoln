
CREATE procedure [dbo].[EDOCS_GetDocTypeName]
	@BrokerId uniqueidentifier,
	@DocTypeId int
as
select (
	CASE WHEN ISValid = 1 THEN DocTypeName ELSE DocTypeName + ' (Deleted)' END) as DocTypeName
from EDOCS_DOCUMENT_TYPE 
where BrokerId = @BrokerId AND DocTypeId = @DocTypeId
