-- =============================================
-- Author:		Brian Beery
-- Create date: 09/20/2013
-- Description:	Get the list of all data retrieval framework partners
-- =============================================
CREATE PROCEDURE [dbo].[DATA_RETRIEVAL_PARTNER_List]
AS
BEGIN

	SELECT PartnerId, Name, ExportPath
	FROM DATA_RETRIEVAL_PARTNER
	ORDER BY Name

END