
-- =============================================
-- Author:		Isaac Ribakoff
-- Create date: 9/19/2014
-- Description: Returns Brokers that are associated with a specific LenderId.
-- =============================================
CREATE PROCEDURE [dbo].[MORTGAGE_INSURANCE_VENDOR_CONFIGURATION_ListBrokerAssociations]
	@VendorId UniqueIdentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT CustomerCode
	FROM MORTGAGE_INSURANCE_VENDOR_BROKER_SETTINGS bs
		JOIN BROKER b ON bs.BrokerId = b.BrokerId
	WHERE ( VendorId = @VendorId )
END

