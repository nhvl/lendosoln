CREATE PROCEDURE [dbo].[CreatePasswordResetRequest] @Login varchar(36), @Email varchar(80), @Type char(1), @PmlSiteId uniqueidentifier, @RequestID uniqueidentifier OUTPUT, @UserFullName varchar(42) OUTPUT AS
BEGIN	
	DECLARE @Now datetime, @AllowPasswordChange bit, @UserId uniqueidentifier
	
	BEGIN TRANSACTION
	
	SELECT top 1 @AllowPasswordChange = AllowPasswordChange, @UserId = au.UserId, @UserFullName = ee.UserFirstNm + ' ' + ee.UserLastNm FROM ALL_USER au INNER JOIN BROKER_USER bu ON au.UserId=bu.UserId INNER JOIN EMPLOYEE ee on bu.EmployeeId=ee.EmployeeId WHERE au.LoginNm = @Login AND ee.Email = @Email AND au.Type = @Type AND ( @Type<>'P' OR au.BrokerPmlSiteId=@PmlSiteId ) AND au.LoginBlockExpirationD <> '2078-01-01 00:00:00.000'
		
	IF @@ROWCOUNT !=1
	BEGIN		
		GOTO ErrorHandler;	
	END

	IF 0!=@@error
	BEGIN
		RAISERROR('Error in CreatePasswordResetRequest sp', 16, 1);
		GOTO ErrorHandler;
	END	
	
	
	IF @AllowPasswordChange = 0
	BEGIN
		RAISERROR('User is not permitted to reset the password', 16, 1);
		GOTO ErrorHandler;
	END

	SET @RequestID = NEWID()
	SET @Now = GETDATE()

	UPDATE ALL_USER SET PasswordChangeRequestId = @RequestID, TimeOfPasswordChangeRequest = @Now WHERE UserId = @UserId	
	
	IF( 0!=@@error)
	BEGIN
		RAISERROR('Error updating All_User table in CreatePasswordResetRequest sp', 16, 1);
		RETURN -100;
	END
	
	COMMIT TRANSACTION;
	RETURN 0

ErrorHandler:	
	ROLLBACK TRANSACTION
	RETURN -100;

END

