-- =============================================
-- Author:		Eric Mallare
-- Create date: 8/17/2017
-- Description:	Loads a title vendor by id.
-- =============================================
ALTER PROCEDURE [dbo].[TITLE_LoadVendorById]
	@VendorId int
AS
BEGIN
	SELECT
		v.VendorId,
		v.CompanyName,
		v.IsEnabled,
		v.IsTestVendor,
		v.IsQuickQuotingEnabled,
		v.IsDetailedQuotingEnabled,
		v.IsPolicyOrderingEnabled,
		v.QuotingPlatformId,
		v.IsOverridingQuotingPlatform,
		v.QuotingTransmissionId,
		v.PolicyOrderingPlatformId,
		v.IsOverridingPolicyOrderingPlatform,
		v.PolicyOrderingTransmissionId,
		v.UsesProviderCodes
	FROM
		TITLE_FRAMEWORK_VENDOR v
	WHERE
		v.VendorId=@VendorId

	SELECT
		qt.TransmissionAssociationT,
		qt.TargetUrl,
		qt.TransmissionAuthenticationT,
		qt.EncryptionKeyId,
		qt.ResponseCertId,
		qt.RequestCredentialName,
		qt.RequestCredentialPassword,
		qt.ResponseCredentialName,
		qt.ResponseCredentialPassword
	FROM 
		TITLE_FRAMEWORK_TRANSMISSION_INFO qt JOIN
		TITLE_FRAMEWORK_VENDOR v ON qt.TransmissionInfoId=v.QuotingTransmissionId
	WHERE
		v.VendorId=@VendorId

	SELECT
		pot.TransmissionAssociationT,
		pot.TargetUrl,
		pot.TransmissionAuthenticationT,
		pot.EncryptionKeyId,
		pot.ResponseCertId,
		pot.RequestCredentialName,
		pot.RequestCredentialPassword,
		pot.ResponseCredentialName,
		pot.ResponseCredentialPassword
	FROM 
		TITLE_FRAMEWORK_TRANSMISSION_INFO pot JOIN
		TITLE_FRAMEWORK_VENDOR v ON pot.TransmissionInfoId=v.PolicyOrderingTransmissionId
	WHERE
		v.VendorId=@VendorId
END
