-- =============================================
-- Author:		Timothy Jewell
-- Create date: 1/23/2018
-- Description:	Retrieves a single DocuSign envelope record using envelope id or id
-- =============================================
ALTER PROCEDURE [dbo].[DOCUSIGN_ENVELOPES_RetrieveByOnlyEnvelopeId]
	@EnvelopeId varchar(36)
AS
BEGIN
	DECLARE @FoundEnvelopeId int;
	SELECT @FoundEnvelopeId = Id FROM DOCUSIGN_ENVELOPES WHERE EnvelopeId = @EnvelopeId;
	IF @FoundEnvelopeId IS NULL
		RETURN -- We don't want an empty reader for anything


	-- Grab the Edocs for the found envelope
	SELECT
		ded.Id, ded.EnvelopeReferenceId, ded.SequenceNumber, ded.FromDocuSign, ded.EdocId, ded.Name, ded.DocumentId, ded.IsCertificate
	FROM 
		DOCUSIGN_EDOCS ded
	WHERE 
		ded.EnvelopeReferenceId=@FoundEnvelopeId


	-- Grab the auth results
	SELECT
		dra.Id, dra.RecipientReferenceId, dra.EnvelopeReferenceId, dra.Type, dra.Status, dra.EventTime, dra.FailureDescription
	FROM
		DOCUSIGN_RECIPIENTS_AUTHRESULTS dra 
	WHERE
		dra.EnvelopeReferenceId=@FoundEnvelopeId


	-- Grab the recipients
	SELECT
		dr.Id, dr.EnvelopeReferenceId, dr.SigningOrder, dr.Type, dr.Name, dr.Email, dr.MfaOption, dr.CurrentStatus, dr.SentDate, dr.DeliveredDate, dr.SignedDate, 
		dr.DeclinedDate, dr.DeclinedReason, dr.AutoRespondedReason, dr.RecipientIdGuid, dr.CustomRecipientId, dr.AccessCode, dr.ProvidedPhoneNumber, dr.ClientUserId
	FROM
		DOCUSIGN_RECIPIENTS dr
	WHERE
		dr.EnvelopeReferenceId=@FoundEnvelopeId


	-- Finally, grab the envelopes themselves
	SELECT
		de.Id, de.EnvelopeId, de.Name, de.RequestedDate, de.SenderName, de.SenderEmail, de.SenderUserId, de.LoanId, de.BrokerId, de.CurrentStatus, de.CreatedDate, de.DeletedDate, 
		de.SentDate, de.DeliveredDate, de.SignedDate, de.CompletedDate, de.DeclinedDate, de.TimedOutDate, de.VoidedDate, de.VoidReason, de.VoidDoneBy
	FROM
		DOCUSIGN_ENVELOPES de 
	WHERE
		de.Id=@FoundEnvelopeId
END
