CREATE PROCEDURE ListTaskEventsByUserId
	@UserId Guid
AS
	SELECT
		d.DiscCreatorUserId ,
		d.DiscCreatedDate ,
		d.DiscDueDate ,
		n.NotifAlertDate ,
		n.NotifLastReadD ,
		n.NotifAckD ,
		n.NotifIsRead ,
		d.DiscLastModifyD ,
		d.DiscLogId
	FROM
		Discussion_Notification AS n WITH (NOLOCK) JOIN Discussion_Log AS d WITH (NOLOCK) ON d.DiscLogId = n.DiscLogId
	WHERE
		n.NotifReceiverUserId = @UserId
		AND
		n.NotifIsValid = 1
		AND
		d.IsValid = 1
		AND
		d.DiscIsRefObjValid = 1
