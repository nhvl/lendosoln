CREATE PROCEDURE [GetEmployeesWithGroupMembership]
	@GroupId uniqueidentifier,
	@BrokerId uniqueidentifier
AS
BEGIN
	SET NOCOUNT ON	
	SELECT CASE WHEN ge.GroupId IS NULL THEN 0 ELSE 1 END AS IsInGroup, e.EmployeeId, e.UserFirstNm, e.UserLastNm, u.LoginNm, b.BranchNm 
	FROM EMPLOYEE e INNER JOIN Branch b on e.BranchId = b.BranchId  INNER JOIN ALL_USER u on e.EmployeeUserId= u.UserId LEFT JOIN (SELECT * FROM GROUP_x_EMPLOYEE WHERE GroupId=@GroupId) ge on e.EmployeeId = ge.EmployeeId 
	WHERE b.BrokerId = @BrokerId AND e.IsActive=1 AND u.[Type]='B' ORDER BY e.UserLastNm, e.UserFirstNm
END
