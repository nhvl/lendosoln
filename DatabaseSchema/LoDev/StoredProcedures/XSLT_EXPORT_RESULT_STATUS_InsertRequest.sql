-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[XSLT_EXPORT_RESULT_STATUS_InsertRequest]
	@ReportId varchar(13),
	@UserId UniqueIdentifier,
	@ReportName varchar(100),
	@RequestXml varchar(max),
	@OutputFileName varchar(100)
AS
BEGIN

	INSERT INTO XSLT_EXPORT_RESULT_STATUS(ReportId, UserId, ReportName, RequestXml, OutputFileName)
	VALUES(@ReportId, @UserId, @ReportName, @RequestXml, @OutputFileName)
END
