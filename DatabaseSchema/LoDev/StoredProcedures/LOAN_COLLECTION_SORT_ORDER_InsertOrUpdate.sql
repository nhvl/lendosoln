-- ============================================================
-- Author:      Alan Daughton
-- Create date: 4/18/2018
-- Description: Updates (or inserts) a row of the LOAN_COLLECTION_SORT_ORDER table.
-- ============================================================
ALTER PROCEDURE [dbo].[LOAN_COLLECTION_SORT_ORDER_InsertOrUpdate]
	@LoanId UniqueIdentifier,
	@AppId UniqueIdentifier = null,
	@ConsumerId UniqueIdentifier = null,
	@UladApplicationId UniqueIdentifier = null,
	@CollectionName VarChar(200),
	@SortOrder VarChar(MAX)
AS
BEGIN
	IF @SortOrder IS NULL OR @SortOrder = ''
	BEGIN
		MERGE [dbo].[LOAN_COLLECTION_SORT_ORDER] AS target
		USING (VALUES(@SortOrder))
			AS source (SortOrder)
			ON 
			  target.LoanId = @LoanId
			  AND target.CollectionName = @CollectionName
			  AND (target.AppId = @AppId OR (@AppId IS NULL AND target.AppId IS NULL))
			  AND (target.ConsumerId = @ConsumerId OR (@ConsumerId IS NULL AND target.ConsumerId IS NULL))
			  AND (target.UladApplicationId = @UladApplicationId OR (@UladApplicationId IS NULL AND target.UladApplicationId IS NULL))
		WHEN MATCHED THEN
			DELETE
		WHEN NOT MATCHED THEN
			INSERT (LoanId, CollectionName, SortOrder, AppId, ConsumerId, UladApplicationId)
			VALUES (@LoanId, @CollectionName, @SortOrder, @AppId, @ConsumerId, @UladApplicationId);
	END
	ELSE
	BEGIN
		MERGE [dbo].[LOAN_COLLECTION_SORT_ORDER] AS target
		USING (VALUES(@SortOrder))
			AS source (SortOrder)
			ON 
			  target.LoanId = @LoanId
			  AND target.CollectionName = @CollectionName
			  AND (target.AppId = @AppId OR (@AppId IS NULL AND target.AppId IS NULL))
			  AND (target.ConsumerId = @ConsumerId OR (@ConsumerId IS NULL AND target.ConsumerId IS NULL))
			  AND (target.UladApplicationId = @UladApplicationId OR (@UladApplicationId IS NULL AND target.UladApplicationId IS NULL))
		WHEN MATCHED THEN
			UPDATE
			SET SortOrder = source.SortOrder
		WHEN NOT MATCHED THEN
			INSERT (LoanId, CollectionName, SortOrder, AppId, ConsumerId, UladApplicationId)
			VALUES (@LoanId, @CollectionName, @SortOrder, @AppId, @ConsumerId, @UladApplicationId);
	END
END