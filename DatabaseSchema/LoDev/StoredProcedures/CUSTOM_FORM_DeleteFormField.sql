-- =============================================
-- Author:		Matthew Pham
-- Create date: 4/23/12
-- Description:	Delete form fields
-- =============================================
CREATE PROCEDURE dbo.[CUSTOM_FORM_DeleteFormField] 
	-- Add the parameters for the stored procedure here
	@FieldId varchar(100) = '' 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM CUSTOM_FORM_FIELD_LIST
	WHERE FieldId = @FieldId
END
