

CREATE  PROCEDURE [dbo].[SetLoanAsSecond]
 @LoanID uniqueidentifier,
 @sLienPosT Int = 0

AS
 UPDATE LOAN_FILE_B
Set sLienPosT = @sLienPosT
WHERE sLId = @LoanID
