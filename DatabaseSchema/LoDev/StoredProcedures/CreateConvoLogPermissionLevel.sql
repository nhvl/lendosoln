-- =============================================
-- Author: Scott Kibler
-- Create date: 5/9/2017
-- Description: 
--  Creates the Permission Level for Conversation Categories.
--  Inserts it into the ordering table in the last position.
--  Returns the id.
-- =============================================
ALTER PROCEDURE [dbo].[CreateConvoLogPermissionLevel]
	@BrokerId uniqueidentifier,
	@IsActive bit,
	@Name varchar(40),
	@Description varchar(1000)
AS
BEGIN
	DECLARE @InsertedPermissionLevelId int
	
	INSERT INTO 
		CONVOLOG_PERMISSION_LEVEL
		( BrokerId,  IsActive,  Name,  Description)
	VALUES 
		(@BrokerId, @IsActive, @Name, @Description)
		
	SELECT @InsertedPermissionLevelId = SCOPE_IDENTITY()
		
	EXEC InsertConvoLogPermissionLevelLast @BrokerId=@BrokerId, @PermissionLevelId=@InsertedPermissionLevelId
	
	SELECT @InsertedPermissionLevelId as Id
END