-- =============================================
-- Author:		Antonio Valencia
-- Create date: 4/12/11
-- Description:	Deletes given condition
-- =============================================
CREATE PROCEDURE [dbo].[TASK_Cateogries_Delete] 
	@BrokerId uniqueidentifier, 
	@Id int
AS 

-- REVIEW NOTES:
-- 6/9/2011: The stored procedure name is misspelled. :) -ThinhNK

BEGIN
	DELETE FROM CONDITION_CATEGORY
	WHERE BrokerId = @BrokerId AND ConditionCategoryId = @Id
END
