CREATE PROCEDURE [dbo].[Region_UpdateRegionById]
	@BrokerId uniqueidentifier,
	@RegionId int,
	@Name varchar(50),
	@Description varchar(100)
AS
BEGIN
	UPDATE REGION
	SET Name = @Name,
		[Description] = @Description
	WHERE BrokerId = @BrokerId
		AND RegionId = @RegionId
END