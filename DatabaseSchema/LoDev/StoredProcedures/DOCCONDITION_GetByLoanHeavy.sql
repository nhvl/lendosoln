-- =============================================
-- Author:		Matthew Pham
-- Create date: 9/19/2012
-- Description:	Get document-condition associations
--				by loan. Includes document folder
--				name and document type
-- =============================================
ALTER PROCEDURE [dbo].[DOCCONDITION_GetByLoanHeavy] 
	-- Add the parameters for the stored procedure here
	@LoanId uniqueidentifier, 
	@ExcludeHiddenConditions bit,
	@BrokerId uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
 
    -- Insert statements for procedure here
	SELECT TED.sLId, TED.TaskId, TED.DocumentId, TED.Status as Status, doc.Status as DocumentStatus, doctype.FolderName, doctype.DocTypeName, T.TaskSubject, t.condrowid, doc.doctypeid
	FROM TASK_x_EDOCS_DOCUMENT TED
	JOIN edocs_document doc
		ON TED.DocumentId = doc.DocumentID
	JOIN TASK T  WITH(INDEX(idx_BrokerId_LoanId_TaskIsCondition_CondIsDeleted)) -- must use index otherwise it'll be SLOW
		ON doc.BrokerId = T.BrokerId
		AND TED.sLId = T.LoanId
		AND TED.TaskId = T.TaskId
		AND T.CondIsDeleted = 0
	JOIN VIEW_EDOCS_DOCTYPE_FOLDER doctype on doctype.doctypeid = doc.doctypeid
	WHERE TED.sLId = @LoanId
	and T.LOANId = @LoanId
	and ted.slid = @LoanId
	and doc.slid = @loanId
	and doc.BrokerId = @BrokerId
	and ( @ExcludeHiddenConditions = 0 OR (@ExcludeHiddenConditions = 1 AND t.CondIsHidden = 0))
END