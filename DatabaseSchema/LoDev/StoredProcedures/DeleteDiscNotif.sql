CREATE PROCEDURE [dbo].DeleteDiscNotif
	@NotifId Guid , @DiscLogId Guid OUT ,  @NotifReceiverUserId Guid OUT , @InvalidDate DateTime = NULL
AS
	SELECT @DiscLogId = n.DiscLogId , @NotifReceiverUserId = n.NotifReceiverUserId FROM Discussion_Notification AS n WHERE n.NotifId = @NotifId
	UPDATE Discussion_Notification
	SET
		NotifIsValid = 0,
		NotifInvalidDate = COALESCE( @InvalidDate , getdate() )
	WHERE
		NotifId = @NotifId
	IF( @@error != 0 )
	BEGIN
		RAISERROR('Error in the update statement in DeleteDiscNotif sp', 16, 1);
		RETURN -100;
	END
