
-- =============================================
-- Author:		Antonio Valencia
-- Create date: 5/2/11
-- Description:	Add Subscriber to task
-- =============================================
CREATE PROCEDURE [dbo].[TASK_Subscription_Subscribe] 
	-- Add the parameters for the stored procedure here
	@UserId uniqueidentifier,
	@TaskId  varchar(10) 
AS

-- REVIEW:
-- 6/9/2011: Reviewed. -ThinhNK
BEGIN
	DECLARE @UserCount int 
	SELECT @UserCount =  Count(*) FROM TASK_SUBSCRIPTION WHERE UserId = @UserId AND TaskId = @TaskId
	IF @UserCount = 0 BEGIN
		
		INSERT INTO TASK_SUBSCRIPTION( UserId, TaskId) VALUES( @UserId, @TaskId) 
	END

	
END

