-- =============================================
-- Author:		Jhairo Erazo
-- Create date: 13 Jan 2014
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[FindMortgagePoolsByInternalId] 
	-- Add the parameters for the stored procedure here
	@InternalId varchar(36),
	@allowPartials bit = 0,
	@BrokerId UniqueIdentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	IF(@allowPartials=1)
		SET @InternalId = '%'+@InternalId+'%';
    -- Insert statements for procedure here
	SELECT PoolId
	FROM MORTGAGE_POOL
	WHERE
	InternalId LIKE @InternalId
	AND
	BrokerId = @BrokerId
	AND
	IsEnabled = 1
END
