CREATE  PROCEDURE [dbo].[RequestSingletonDistributedWithHolderConstrain]
	@ObjId varchar(100),
	@SecondsToExpire int,
	@HolderId varchar(100)
as 
-- NOTE THAT THIS STORE PROC MUST OPERATE ATOMICALLY
declare @now DateTime;
set @now = getdate();
if( @SecondsToExpire < 0 )
begin
	begin
		update singleton_tracker
		set IsExisting = 1, StartTime = getdate(), HolderId = @HolderId, ExpirationTime = '12/31/2050'
		where ObjId = @ObjId and ( IsExisting = 0 or ( DateDiff( second, ExpirationTime, @now ) > 0 ) )
              and HolderId = @HolderId
	end 
end
else
	begin
		update singleton_tracker
		set IsExisting = 1, StartTime = getdate(), HolderId = @HolderId, ExpirationTime = DateAdd( second, @SecondsToExpire, @now )
		where ObjId = @ObjId and ( IsExisting = 0 or ( DateDiff( second, ExpirationTime, @now ) > 0 ) )
              and HolderId = @HolderId
	end
