-- =============================================
-- Author:		Eric Mallare
-- Create date: 2/13/2018
-- Description:	Deletes the recipient. Will fail if the auth results have not been deleted beforehand.
-- =============================================
CREATE PROCEDURE [dbo].[DOCUSIGN_RECIPIENTS_Delete]
	@EnvelopeReferenceId int,
	@Id int
AS
BEGIN
	DELETE FROM 
		DOCUSIGN_RECIPIENTS
	WHERE
		Id=@Id AND
		EnvelopeReferenceId=@EnvelopeReferenceId
END