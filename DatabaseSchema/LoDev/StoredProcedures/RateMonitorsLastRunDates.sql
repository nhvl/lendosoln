-- =============================================
-- Author:		Paolo Arrastia
-- Create date: 3 Aug 2012
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[RateMonitorsLastRunDates] 
	-- Add the parameters for the stored procedure here
	@olderThan DateTime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT Id, LastRunDate, LastRunBatchId
	FROM RATE_MONITOR
	WHERE IsEnabled = 1 AND 
	(LastRunDate IS NULL OR LastRunDate <= @olderThan)
END
