CREATE PROCEDURE dbo.TITLE_VENDOR_X_BROKER_Get
	@BrokerId uniqueidentifier, 
	@Id int = null
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT TitleVendorId, [Login], [ClientCode] as AccountId, [Password], BrokerId,
	LoginTitle, ClientCodeTitle as AccountIdTitle, PasswordTitle
	FROM TITLE_VENDOR_X_BROKER
	where brokerid = @brokerid and (@id is null or @id = TitleVendorId)
	
	
END