-- =============================================
-- Author:		Paolo Arrastia
-- Create date: 3/26/2013
-- Description:	
-- =============================================
CREATE PROCEDURE dbo.SaveQueryForBatchExport 
	-- Add the parameters for the stored procedure here
	@BrokerId uniqueidentifier, 
	@QueryId uniqueidentifier,
	@MapId varchar(50)
AS
BEGIN
	UPDATE BROKER_XSLT_REPORT_MAP 
	SET QueryId = @QueryId
	WHERE (BrokerId = @BrokerId and XsltMapId = @MapId)
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO BROKER_XSLT_REPORT_MAP
		(BrokerId, XsltMapId, QueryId)
		VALUES
		(@BrokerId, @MapId, @QueryId)
	END
END
