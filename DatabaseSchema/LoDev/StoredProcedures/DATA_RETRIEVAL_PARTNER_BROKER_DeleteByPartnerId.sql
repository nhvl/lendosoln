-- =============================================
-- Author:		Justin Lara
-- Create date: 12/6/2016
-- Description:	Remove all broker associations
--				with a data retrieval partner.
-- =============================================
ALTER PROCEDURE [dbo].[DATA_RETRIEVAL_PARTNER_BROKER_DeleteByPartnerId]
	@PartnerId uniqueidentifier
AS
BEGIN
    DELETE FROM DATA_RETRIEVAL_PARTNER_BROKER
	WHERE PartnerId = @PartnerId
END
