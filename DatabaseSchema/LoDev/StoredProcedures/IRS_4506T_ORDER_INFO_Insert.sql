-- =============================================
-- Author:		David Dao
-- Create date: Aug 7, 2013
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[IRS_4506T_ORDER_INFO_Insert]
    @TransactionId UniqueIdentifier,
    @sLId UniqueIdentifier,
    @VendorId UniqueIdentifier,
    @aAppId UniqueIdentifier,
    @ApplicationName varchar(200),
    @OrderNumber varchar(50),
    @Status varchar(1000),
    @StatusT int
    
AS
BEGIN

    INSERT INTO IRS_4506T_ORDER_INFO (TransactionId, sLId, VendorId, aAppId, ApplicationName, 
                                     OrderNumber, OrderDate, Status, StatusDate, ResponseXmlContent, UploadedFilesXmlContent, StatusT)
                              VALUES (@TransactionId, @sLId, @VendorId, @aAppId, @ApplicationName,
                                      @OrderNumber, GETDATE(), @Status, GETDATE(), '', '', @StatusT)

END
