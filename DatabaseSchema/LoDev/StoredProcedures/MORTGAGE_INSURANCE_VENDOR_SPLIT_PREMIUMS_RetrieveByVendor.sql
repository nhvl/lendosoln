-- =============================================
-- Author:		Justin Lara
-- Create date: 11/19/2018
-- Description:	Retrieves all split premium
--				options offered by a vendor.
-- =============================================
ALTER PROCEDURE [dbo].[MORTGAGE_INSURANCE_VENDOR_SPLIT_PREMIUMS_RetrieveByVendor]
	@VendorId uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT RowId, VendorId, UpfrontPremiumPercentage, UpfrontPremiumMismoValue
    FROM MORTGAGE_INSURANCE_VENDOR_SPLIT_PREMIUMS
    WHERE VendorId = @VendorId
END
GO
