CREATE PROCEDURE [dbo].[Billing_Credit]
	@StartDate datetime,
	@EndDate datetime
AS
BEGIN
-- dd 2018-11-02 - For use in automate billing executable.
SELECT DISTINCT br.BrokerNm AS LenderName, br.CustomerCode AS CustomerCode, loan.sLId AS LoanId, loan.sLNm As LoanNumber, sf.ExternalFileId AS ReportId, sf.ComId
                           FROM service_file sf JOIN Application_A app with(nolock) ON sf.owner = app.aAppId
                                                JOIN Loan_File_Cache loan with(nolock) ON app.sLId = loan.sLId
                                                JOIN Broker br with(nolock) ON loan.sBrokerId = br.BrokerId
                           WHERE sf.IsInEffect=1 AND sf.HowDidItGetHere='OrderFromCRA'
                             AND br.SuiteType = 1
                             AND TimeVersion >= @StartDate AND TimeVersion < @EndDate

END							 