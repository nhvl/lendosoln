ALTER     PROCEDURE [dbo].[TASK_RetrieveTaskByBrokerIdTaskId] 
	@BrokerId	uniqueidentifier
	, @TaskId	varchar(10)

AS

--REVIEW
--6/9/2011: Reviewed. ThinhNK 
--7/5/2012: Added CondRowId. -MP

	SELECT
		t.BrokerId
		, TaskId
		, TaskAssignedUserId
		--, TaskIdentityId
		, TaskStatus
		, LoanId
		, TaskOwnerUserId
		, TaskCreatedDate
		, TaskSubject
		, TaskDueDate
		, TaskFollowUpDate
		, CondInternalNotes
		, TaskIsCondition
		, TaskResolvedUserId
		, TaskLastModifiedDate
		, BorrowerNmCached
		, LoanNumCached
		, TaskToBeAssignedRoleId
		, TaskToBeOwnerRoleId
		, TaskCreatedByUserId
		, TaskDueDateLocked
		, TaskDueDateCalcField
		, TaskDueDateCalcDays
		, TaskHistoryXml
		, TaskPermissionLevelId
		, TaskClosedUserId
		, TaskClosedDate
		, TaskRowVersion
		, CondCategoryId
		, CondIsHidden
		--, CondRank
		, CondRowId
		, CondDeletedDate
		, CondDeletedByUserNm
		, CondIsDeleted
		--, assigned.UserFirstNm + ' ' + assigned.UserLastNm as AssignedUserFullName
		--, owned.UserFirstNm + ' ' + owned.UserLastNm as OwnerFullName
		--, CASE WHEN TaskClosedUserId IS NULL THEN '' ELSE closed.UserFirstNm + ' ' + closed.UserLastNm END as ClosingUserFullName
		, TaskAssignedUserFullName
		, TaskOwnerFullName
		, TaskClosedUserFullName
		, CondMessageId
		, CondRequiredDocTypeId
		, ResolutionBlockTriggerName
		, ResolutionDenialMessage
		, ResolutionDateSetterFieldId
		, AssignToOnReactivationUserId

	FROM TASK t WITH(ROWLOCK, INDEX(Idx__BrokerIdAssignedUserId))
		--left join Employee assigned on t.TaskAssignedUserId = assigned.EmployeeUserId
		--left join Employee owned on t.TaskOwnerUserId = owned.EmployeeUserId
		--left join Employee closed on t.TaskClosedUserId = closed.EmployeeUserId
	WHERE 
	t.BrokerId = @BrokerId
	AND TaskId = @TaskId





