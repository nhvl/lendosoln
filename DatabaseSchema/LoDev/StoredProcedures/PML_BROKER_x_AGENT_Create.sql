CREATE PROCEDURE [dbo].[PML_BROKER_x_AGENT_Create]
	@BrokerId		uniqueidentifier,
	@PmlBrokerId	uniqueidentifier,
	@AgentId		uniqueidentifier
AS
	INSERT INTO PML_BROKER_x_AGENT (
		BrokerId,
		PmlBrokerId,
		AgentId
	)
	VALUES (
		@BrokerId,
		@PmlBrokerId,
		@AgentId
	)