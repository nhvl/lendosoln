CREATE PROCEDURE ListDiscTracks
	@DiscLogId Guid , @CreatorUserId Guid = NULL , @CreatorLoginNm Varchar( 64 ) = NULL , @IsValid Bit = NULL
AS
	IF( @CreatorUserId IS NULL )
	BEGIN
		SELECT @CreatorUserId = UserId FROM All_User WHERE LOWER( LoginNm ) = LOWER( @CreatorLoginNm )
	END
	SELECT t.TrackId , t.TrackCreatedDate , t.TrackNotifIsRead , t.TrackIsValid , t.NotifId , n.NotifIsValid , n.NotifReceiverUserId , n.NotifReceiverLoginNm , e.UserFirstNm , e.UserLastNm
	FROM Track AS t WITH (NOLOCK) JOIN Discussion_Notification AS n WITH (NOLOCK) ON n.NotifId = t.NotifId 
                                          JOIN Employee AS e WITH (NOLOCK) ON e.EmployeeUserId = n.NotifReceiverUserId
	WHERE
		t.DiscLogId = @DiscLogId
		AND
		t.TrackCreatorUserId = @CreatorUserId
		AND
		t.TrackIsValid = COALESCE( @IsValid , t.TrackIsValid )
	if( 0!=@@error)
	begin
		RAISERROR('Error in the selection statement in ListDiscTracks sp', 16, 1);
		return -100;
	end
	return 0;
