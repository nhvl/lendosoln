-- =============================================
-- Author:		Paolo Arrastia
-- Create date: 24 Jul 2012
-- Description:	
-- =============================================
ALTER PROCEDURE [dbo].[RateMonitorsForNextLoan] 
	-- Add the parameters for the stored procedure here
	@batchId int,
	@currDate DateTime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @topId UniqueIdentifier;
	SET @topId = (
		SELECT top 1 sLId
			FROM RATE_MONITOR
			WHERE ( 
					IsEnabled = 1 AND
					( (LastRunBatchId < @batchId) OR (LastRunBatchID IS NULL) )
				)
		);
	IF (@@ROWCOUNT > 0)
	BEGIN
		UPDATE RATE_MONITOR
			SET LastRunBatchId = @batchId, LastRunDate = @currDate
			WHERE (sLId = @topId AND IsEnabled = 1);
		SELECT Id, sLId, Rate, Point, EmailAddresses, BPDesc, CreatedDate, CreatedByUserId, CreatedByUserType
			FROM RATE_MONITOR
			WHERE (sLId = @topId AND IsEnabled = 1);
	END
END
