-- =============================================
-- Author:		Antonio Valencia
-- Create date: 9/16/2014
-- Description:	Moves pending document request from one loan to another
-- =============================================
CREATE PROCEDURE [dbo].[PENDING_DOCUMENT_REQUEST_MoveExistingToNewLoan]
	-- Add the parameters for the stored procedure here
	@BrokerId uniqueidentifier,
	@src_sLId uniqueidentifier,
	@dst_sLId uniqueidentifier,
	@src_aAppId uniqueidentifier,
	@dst_aAppId uniqueidentifier
AS
BEGIN
	DECLARE @dst_BrokerId uniqueidentifier
	
	SELECT @dst_BrokerId = sBrokerId from loan_file_cache where slid = @dst_slid 
	
	IF (@dst_BrokerId <> @BrokerId) BEGIN
	RAISERROR('Cross broker check failed', 16, 1);
		RETURN -100
	END
	
	UPDATE PENDING_DOCUMENT_REQUEST 
	SET LoanId = @dst_slid,
		ApplicationId = @dst_aAppId
	WHERE @BrokerID = BrokerId AND LoanId = @src_slid AND applicationid = @src_aAppId
END
