CREATE PROCEDURE GetReportQuery
	@BrokerId Guid , @QueryId Guid
AS
	SELECT
		*
	FROM
		Report_Query AS r
	WHERE
		r.BrokerId = @BrokerId
		AND
		r.QueryId = @QueryId
	IF ( @@error != 0 )
	BEGIN
		RAISERROR('Error in the select statement in GetReportQuery sp', 16, 1);
		RETURN -100;
	END
