-- =============================================
-- Author:		Timothy Jewell
-- Create date:	05/23/2017
-- Description:	Creates a VOA order.  Be sure to wrap this in a transaction with other inserts
-- =============================================
ALTER PROCEDURE [dbo].[VOA_ORDER_Create]
	@OrderId int,
	@VerificationType int,
	@RefreshPeriod int,
	@IsForCoborrower bit
AS
BEGIN
	INSERT INTO VOA_ORDER
		(OrderId, VerificationType, RefreshPeriod, IsForCoborrower)
	VALUES
		(@OrderId, @VerificationType, @RefreshPeriod, @IsForCoborrower)
END
