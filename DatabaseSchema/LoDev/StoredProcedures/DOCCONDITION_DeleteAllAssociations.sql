-- =============================================
-- Author:		Antonio Valencia
-- Create date: 3/26/2014
-- Description:	Delete All Existing Associations 
--				for given task
-- =============================================
CREATE PROCEDURE [dbo].[DOCCONDITION_DeleteAllAssociations] 
	-- Add the parameters for the stored procedure here
	@LoanId uniqueidentifier,
	@TaskId varchar(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DELETE TD 
	FROM TASK_x_EDOCS_DOCUMENT TD
	WHERE TD.sLId = @LoanId
	AND TD.TaskId = @TaskId


END
