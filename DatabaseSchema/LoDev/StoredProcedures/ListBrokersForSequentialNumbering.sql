-- =============================================
-- Author:		Geoffrey Feltman
-- Create date: 5/31/13
-- Description:	Get list of broker ids that need sequential numbering.
-- =============================================
ALTER PROCEDURE dbo.ListBrokersForSequentialNumbering 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT BrokerId
	FROM BROKER
	WHERE EnableSequentialNumbering = 1 and status = 1 
END
