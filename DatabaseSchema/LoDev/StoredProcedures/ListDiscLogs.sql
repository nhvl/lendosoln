


CREATE  PROCEDURE [dbo].[ListDiscLogs]
	@UserId uniqueidentifier = NULL , @IsValid Bit = NULL
AS
	SELECT DISTINCT l.*
	FROM Discussion_Log AS l with(nolock) LEFT JOIN Discussion_Notification  AS n with(nolock) ON l.DiscLogId = n.DiscLogId 
		LEFT JOIN Loan_File_Cache AS c with(nolock) ON c.sLId = l.DiscRefObjId
	WHERE
		n.NotifReceiverUserId = COALESCE( @UserId , n.NotifReceiverUserId )
		AND
		n.NotifIsValid = COALESCE( @IsValid , n.NotifIsValid )
		AND
		ISNULL( c.IsTemplate , 0 ) = 0
		AND
		l.DiscIsRefObjValid = 1
		AND
		l.IsValid = 1
	if( 0!=@@error)
	begin
		RAISERROR('Error in the selection statement in ListDiscLogs sp', 16, 1);
		return -100;
	end
	return 0;



