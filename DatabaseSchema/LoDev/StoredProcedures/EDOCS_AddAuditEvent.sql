

-- =============================================
-- Author:		Antonio Valencia
-- Create date: July 11, 2011
-- Description:	Add an audit event to edocs
-- =============================================
CREATE PROCEDURE [dbo].[EDOCS_AddAuditEvent] 
	-- Add the parameters for the stored procedure here
	@ModifiedByUserId uniqueidentifier = null, 
	@ModifiedDate datetime = null,
	@Description varchar(200),
	@DocumentId uniqueidentifier,
	@Details varchar(max) = ''
AS
BEGIN
	IF @ModifiedDate is null BEGIN
		SELECT @ModifiedDate = GETDATE() 
	END
	INSERT INTO EDocs_Audit_History (
		DocumentId, 
		ModifiedByUserId,  
		ModifiedDate, 
		Description,
		Details 
	) 
	VALUES (
		@DocumentId, 
		@ModifiedByUserId, 
		@ModifiedDate,	
		@Description,
		@Details
	)
END


