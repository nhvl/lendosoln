
CREATE PROCEDURE [dbo].[InsertPerFieldSecurityEntry] 
            @FieldId varchar(100),
			@Description varchar(200),
			@LockDeskPerm bit, 
			@CloserPerm bit, 
			@AccountantPerm bit,
			@InvestorInfoPerm bit
AS
	INSERT INTO LOAN_FIELD_SECURITY
	VALUES(	@FieldId, @Description, getdate(), @LockDeskPerm, @CloserPerm, @AccountantPerm, @InvestorInfoPerm)
	if( 0!=@@error)
	begin
		RAISERROR('Error in updating LOAN_FIELD_SECURITY table in InsertPerFieldSecurityEntry sp', 16, 1);
		return -100;
	end
	return 0;

