-- =============================================
-- Author:		paoloa
-- Create date: 4/26/2013
-- Description:	
-- =============================================
CREATE PROCEDURE dbo.CreateRSWarningMsgByBrokerPolicyId
	-- Add the parameters for the stored procedure here
	@Broker uniqueidentifier, 
	@NormalUserExpiredMsg varchar(500),
	@LockDeskExpiredMsg varchar(500),
	@NormalUserCutOffMsg varchar(500),
	@LockDeskCutOffMsg	varchar(500),
	@OutsideNormalHrMsg varchar(500),
	@OutsideClosureDayHrMsg varchar(500),
	@OutsideNormalHrAndPassInvestorCutOffMsg varchar(500),
	@PolicyId uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	insert into LENDER_LOCK_POLICY_CUSTOM_WARNING_MSGS(BrokerId, NormalUserExpiredCustomMsg, LockDeskExpiredCustomMsg, NormalUserCutOffCustomMsg, LockDeskCutOffCustomMsg, OutsideNormalHrCustomMsg, OutsideClosureDayHrCustomMsg, OutsideNormalHrAndPassInvestorCutOffCustomMsg, LockPolicyId )
	values(@Broker, @NormalUserExpiredMsg, @LockDeskExpiredMsg, @NormalUserCutOffMsg, @LockDeskCutOffMsg, @OutsideNormalHrMsg, @OutsideClosureDayHrMsg, @OutsideNormalHrAndPassInvestorCutOffMsg, @PolicyId);
END
