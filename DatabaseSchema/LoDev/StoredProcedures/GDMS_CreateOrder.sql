-- =============================================
-- Author:		Matthew Pham
-- Create date:	12/18/12
-- Description:	Create a GDMS order
-- =============================================
ALTER PROCEDURE [dbo].[GDMS_CreateOrder]
	@AppraisalOrderId uniqueidentifier,
	@FileNumber int,
	@BrokerId uniqueidentifier,
	@UserId uniqueidentifier,
	@EmployeeId uniqueidentifier,
	@LoanId uniqueidentifier,
	@VendorId uniqueidentifier,
	@UploadedFileIdXmlContent varchar(max),
	-- APPRAISAL_ORDER FIELDS
	@IsValid bit,
	@AppraisalOrderType int,
	@ValuationMethod int,
	@DeliveryMethod int,
	@AppraisalFormType int,
	@ProductName varchar(100),
	@OrderNumber varchar(50),
	@FhaDocFileId varchar(50),
	@UcdpAppraisalId varchar(50),
	@Notes varchar(max),
	@OrderedDate smalldatetime,
	@NeededDate smalldatetime,
	@ExpirationDate smalldatetime,
	@ReceivedDate smalldatetime,
	@SentToBorrowerDate smalldatetime,
	@BorrowerReceivedDate smalldatetime,
	@RevisionDate smalldatetime,
	@ValuationEffectiveDate smalldatetime,
	@SubmittedToFha smalldatetime,
	@SubmittedToUcdp smalldatetime,
	@CuRiskScore decimal(4,1),
	@OvervaluationRiskT int,
	@PropertyEligibilityRiskT int,
	@AppraisalQualityRiskT int,
	@DeletedDate smalldatetime = null,
	@DeletedByUserId uniqueidentifier = null,
	@AppIdAssociatedWithProperty uniqueidentifier = null,
	@LinkedReoId uniqueidentifier = null,
	@PropertyAddress varchar(60) = null,
	@PropertyCity varchar(72) = null,
	@PropertyState varchar(2) = null,
	@PropertyZip varchar(5) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	EXEC APPRAISAL_ORDER_Create
		@AppraisalOrderId,
		@LoanId,
		@BrokerId,
		@IsValid,
		@AppraisalOrderType,
		@ValuationMethod,
		@DeliveryMethod,
		@AppraisalFormType,
		@ProductName,
		@OrderNumber,
		@FhaDocFileId,
		@UcdpAppraisalId,
		@Notes,
		@OrderedDate,
		@NeededDate,
		@ExpirationDate,
		@ReceivedDate,
		@SentToBorrowerDate,
		@BorrowerReceivedDate,
		@RevisionDate,
		@ValuationEffectiveDate,
		@SubmittedToFha,
		@SubmittedToUcdp,
		@CuRiskScore,
		@OvervaluationRiskT,
		@PropertyEligibilityRiskT,
		@AppraisalQualityRiskT,
		@DeletedDate,
		@DeletedByUserId,
		@AppIdAssociatedWithProperty,
		@LinkedReoId,
		@PropertyAddress,
		@PropertyCity,
		@PropertyState,
		@PropertyZip

	INSERT INTO GDMS_ORDER
	(
		AppraisalOrderId,
		FileNumber,
		BrokerId,
		UserId,
		EmployeeId,
		sLId,
		VendorId,
		UploadedFileIdXmlContent
	)
	VALUES
	(
		@AppraisalOrderId,
		@FileNumber,
		@BrokerId,
		@UserId,
		@EmployeeId,
		@LoanId,
		@VendorId,
		@UploadedFileIdXmlContent
	)
END