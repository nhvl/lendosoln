-- =============================================
-- Author:		Michael Leinweaver
-- Create date: 7/24/2018
-- Description:	Adds an index number to an originating company
--			    as part of the migration for case 470266.
-- =============================================
CREATE PROCEDURE [dbo].[PML_BROKER_AddIndexNumberForMigration]
	@BrokerId uniqueidentifier,
	@PmlBrokerId uniqueidentifier,
	@IndexNumber int
AS
BEGIN
	UPDATE PML_BROKER
	SET IndexNumber = @IndexNumber
	WHERE BrokerId = @BrokerId AND PmlBrokerId = @PmlBrokerId
END
