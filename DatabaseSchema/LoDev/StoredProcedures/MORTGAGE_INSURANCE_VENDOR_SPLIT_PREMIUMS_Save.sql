-- =============================================
-- Author:		Justin Lara
-- Create date: 11/19/2018
-- Description:	Saves a split premium option.
-- =============================================
ALTER PROCEDURE [dbo].[MORTGAGE_INSURANCE_VENDOR_SPLIT_PREMIUMS_Save]
	@VendorId uniqueidentifier,
	@UpfrontPremiumPercentage decimal(3,2),
	@UpfrontPremiumMismoValue varchar(14)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    INSERT INTO MORTGAGE_INSURANCE_VENDOR_SPLIT_PREMIUMS
    (
		VendorId,
		UpfrontPremiumPercentage,
		UpfrontPremiumMismoValue
	)
	VALUES
	(
		@VendorId,
		@UpfrontPremiumPercentage,
		@UpfrontPremiumMismoValue
	)
END
GO
