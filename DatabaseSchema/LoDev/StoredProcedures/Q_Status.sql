
-- =============================================
-- Author:		Antonio Valencia
-- Create date: 11/26/2013
-- Description: Returns count and all queues that exist
-- =============================================
ALTER PROCEDURE [dbo].[Q_STATUS] 

AS
BEGIN
	SELECT QueueName, t.QueueId, count(*) as cnt, min(insertiontime) as OldestMessageInsertionTime  from Q_message m with (nolock) right join q_type t with (Nolock) on m.queueid = t.queueid  
	Group by t.QueueId, QueueName
	order by t.queueid 
END

