-- =============================================
-- Author:		Matthew Pham
-- Create date: 11/22/11
-- Description:	Gets a deduped list of all condition category names
--              Used by the workflow config for condition-related parameters
--              in ParameterReporting.cs.
-- =============================================
CREATE PROCEDURE [dbo].[TASK_Categories_FetchAll] 

AS
BEGIN
	SELECT Category
	FROM CONDITION_CATEGORY
	GROUP BY Category
	ORDER BY Category ASC
END
