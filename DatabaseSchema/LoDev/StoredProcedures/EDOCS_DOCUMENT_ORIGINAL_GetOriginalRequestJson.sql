-- =============================================
-- Author:		David Dao
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE dbo.EDOCS_DOCUMENT_ORIGINAL_GetOriginalRequestJson
    @BrokerId UniqueIdentifier,
    @DocumentId UniqueIdentifier
AS
BEGIN
    SELECT OriginalRequestJson
    FROM EDOCS_DOCUMENT_ORIGINAL
    WHERE BrokerId = @BrokerId AND DocumentId = @DocumentId

END
