ALTER PROCEDURE [dbo].[TITLE_FRAMEWORK_LENDER_SERVICE_Load]
    @LenderServiceId int,
    @BrokerId uniqueidentifier
AS BEGIN
SELECT * FROM dbo.TITLE_FRAMEWORK_LENDER_SERVICE
WHERE 
    LenderServiceId = @LenderServiceId
    AND BrokerId = @BrokerId
END
