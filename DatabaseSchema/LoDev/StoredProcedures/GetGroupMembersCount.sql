CREATE PROCEDURE [GetGroupMembersCount]
	@GroupId uniqueidentifier,
	@BrokerId uniqueidentifier
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @groupType int, @membersCount int
	SELECT @groupType=GroupType	FROM [GROUP] WHERE GroupId=@GroupId	

	
	IF @groupType=1 
		SELECT  @membersCount = COUNT(*) FROM GROUP_x_EMPLOYEE  WHERE GroupId = @GroupId		
	ELSE IF @groupType=2
		SELECT  @membersCount = COUNT(*) FROM GROUP_x_BRANCH  WHERE GroupId = @GroupId		
	ELSE
		SELECT  @membersCount = COUNT(*) FROM GROUP_x_PML_BROKER  WHERE GroupId = @GroupId

	return @membersCount
	
END