

CREATE   PROCEDURE [dbo].[GetAccessControlLoanAssignments]
	@LoanId uniqueidentifier
AS
	SELECT
		l.sEmployeeManagerId ,
		l.sEmployeeUnderwriterId ,
		l.sEmployeeLockDeskId ,
		l.sEmployeeProcessorId ,
		l.sEmployeeLoanOpenerId ,
		l.sEmployeeLoanRepId ,
		l.sEmployeeLenderAccExecId ,
		l.sEmployeeRealEstateAgentId ,
		l.sEmployeeCallCenterAgentId,
		l.sEmployeeCloserId,
		l.sEmployeeBrokerProcessorId,
		l3.sEmployeeShipperId,
		l3.sEmployeeFunderId,
		l3.sEmployeePostCloserId,
		l3.sEmployeeInsuringId,
		l3.sEmployeeCollateralAgentId,
		l3.sEmployeeDocDrawerId,
		l3.sEmployeeCreditAuditorId,
		l3.sEmployeeDisclosureDeskId,
		l3.sEmployeeJuniorProcessorId,
		l3.sEmployeeJuniorUnderwriterId,
		l3.sEmployeeLegalAuditorId,
		l3.sEmployeeLoanOfficerAssistantId,
		l3.sEmployeePurchaserId,
		l3.sEmployeeQCComplianceId,
		l3.sEmployeeSecondaryId,
		l3.sEmployeeServicingId,
		l2.sEmployeeExternalSecondaryId,
		l2.sEmployeeExternalPostCloserId

	FROM
		Loan_File_Cache AS l with(nolock) JOIN Loan_File_Cache_3 AS l3 with (nolock) ON l.sLId = l3.sLId
			JOIN LOAN_FILE_CACHE_2 l2 with(nolock) on l.sLId = l2.sLId
	WHERE
		l.sLId = @LoanId