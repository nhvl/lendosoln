-- =============================================
-- Author:		Michael Leinweaver
-- Create date: 5/16/2018
-- Description:	Retrieves the description for a custom PDF form.
-- =============================================
CREATE PROCEDURE [dbo].[LO_FORM_GetDescriptionById]
	@BrokerId uniqueidentifier,
	@FormId uniqueidentifier
AS
BEGIN
	SELECT Description
	FROM LO_FORM
	WHERE BrokerId = @BrokerId AND FormId = @FormId
END
