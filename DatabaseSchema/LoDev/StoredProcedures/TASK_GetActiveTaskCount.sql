-- =============================================
-- Author:		Michael Leinweaver
-- Create date: 10/27/2016
-- Description:	Obtains the number of active tasks and conditions
--				assigned to a user for a given broker and loan file.
-- =============================================
ALTER PROCEDURE [dbo].[TASK_GetActiveTaskCount]
	@BrokerId uniqueidentifier,
	@LoanId uniqueidentifier,
	@AssignedUserId uniqueidentifier
AS
BEGIN
	SELECT 
		COALESCE(SUM(CASE WHEN TaskIsCondition = 0 THEN 1 ELSE 0 END), 0) as TaskCount,
		COALESCE(SUM(CASE WHEN TaskIsCondition = 1 THEN 1 ELSE 0 END), 0) as ConditionCount
	FROM TASK WITH (NOLOCK) 
	WHERE 
		BrokerId = @BrokerId AND
		LoanId = @LoanId AND
		TaskAssignedUserId = @AssignedUserId AND
		TaskStatus = 0 AND
		CondIsDeleted = 0
END
