-- =============================================
-- Author:		Timothy Jewell
-- Create date:	05/23/2017
-- Description:	Creates a VOX order.  Be sure to wrap this in a transaction with the other inserts.
-- =============================================
ALTER PROCEDURE [dbo].[VOX_ORDER_Create]
	@BrokerId uniqueidentifier,
	@LoanId uniqueidentifier,
	@ApplicationId uniqueidentifier,
	@TransactionId uniqueidentifier,
	@VendorId int,
	@ProviderId int,
	@HasBeenRefreshed bit,
	@RefreshedFromOrderId int,
	@IsTestOrder bit,
	@LenderServiceId int,
	@LenderServiceName varchar(200),
	@OrderNumber varchar(50),
	@Status int,
	@StatusDescription varchar(300),
	@OrderedBy varchar(100),
	@DateOrdered datetime,
	@DateCompleted datetime,
	@ErrorMessage varchar(300),
	@NotificationEmail varchar(80),
	@OrderId int OUTPUT
AS
BEGIN
	INSERT INTO [dbo].[VOX_Order]
		(BrokerId, LoanId, ApplicationId, TransactionId, VendorId, ProviderId, HasBeenRefreshed, RefreshedFromOrderId,
		IsTestOrder, LenderServiceId, LenderServiceName, OrderNumber, Status, StatusDescription, OrderedBy,
		DateOrdered, DateCompleted, ErrorMessage, NotificationEmail)
	VALUES
		(@BrokerId, @LoanId, @ApplicationId, @TransactionId, @VendorId, @ProviderId, @HasBeenRefreshed, @RefreshedFromOrderId,
		@IsTestOrder, @LenderServiceId, @LenderServiceName, @OrderNumber, @Status, @StatusDescription, @OrderedBy,
		@DateOrdered, @DateCompleted, @ErrorMessage, @NotificationEmail)
	
	SET @OrderId = SCOPE_IDENTITY();
END
