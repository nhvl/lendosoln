ALTER PROCEDURE [dbo].[WORKFLOW_CONFIG_CURRENT_SaveRelease] 
	@BrokerId uniqueidentifier,
	@ConfigXmlKey uniqueidentifier,
	@CompiledConfig varbinary(max),
	@ModifyingUserId uniqueidentifier,
	@ReleaseDate datetime,
	@ConfigurationXmlContent varchar(max)
AS
BEGIN
BEGIN TRANSACTION trans
BEGIN TRY	
	DECLARE	@ConfigId bigint

	INSERT INTO CONFIG_RELEASED(ConfigurationXmlContent, OrgId, ModifyingUserId, ReleaseDate, CompiledConfiguration) 
	OUTPUT INSERTED.ConfigId
	VALUES( @ConfigurationXmlContent, @BrokerId, @ModifyingUserId, @ReleaseDate, @CompiledConfig)
	
	SET @ConfigId = SCOPE_IDENTITY()
	
	IF EXISTS(SELECT 1 FROM WORKFLOW_CONFIG_CURRENT WHERE BrokerId = @BrokerId)
	BEGIN
		UPDATE WORKFLOW_CONFIG_CURRENT
		SET ConfigId = @ConfigId,
			[Version] = [Version] + 1,
			ConfigXmlKey = @ConfigXmlKey,
			CompiledConfig = @CompiledConfig,
			ModifyingUserId = @ModifyingUserId,
			ReleaseDate = @ReleaseDate
		WHERE BrokerId = @BrokerId
	END
	ELSE
	BEGIN
		INSERT INTO WORKFLOW_CONFIG_CURRENT(BrokerId, ConfigId, ConfigXmlKey, CompiledConfig, ModifyingUserId, ReleaseDate) 
		VALUES(@BrokerId, @ConfigId, @ConfigXmlKey, @CompiledConfig, @ModifyingUserId, @ReleaseDate)
	END
COMMIT TRANSACTION trans
END TRY
BEGIN CATCH
  ROLLBACK TRANSACTION trans
END CATCH  
END