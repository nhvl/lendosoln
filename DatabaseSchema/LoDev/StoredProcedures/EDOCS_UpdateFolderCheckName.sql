

-- =============================================
-- Author:		Diana Baird
-- Create date: 5/13/10
-- Description:	Checks if an EDoc Folder exists with the same name
-- =============================================
CREATE PROCEDURE [dbo].[EDOCS_UpdateFolderCheckName] 
	@BrokerId uniqueidentifier,
	@FolderName varchar(50)
AS
BEGIN
select 1 from EDOCS_FOLDER

where FolderName = @FolderName
AND
BrokerId = @BrokerId
	END








