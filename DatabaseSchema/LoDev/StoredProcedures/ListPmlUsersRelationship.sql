


CREATE PROCEDURE [dbo].[ListPmlUsersRelationship] 
	@BrokerId uniqueidentifier
AS
-- dd 10/5/2005 I don't know what to call this stored procedure. Basicly it only use in PMLUserList.aspx page.
-- The reason I did not list all employee from this view is to prevent a large result set for broker with many 'P' user. i.e: SCME
SELECT v.EmployeeId, UserFirstNm + ' ' + UserLastNm AS UserName
FROM View_LendersOffice_Employee v WITH (NOLOCK)
left join broker_user b on v.employeeid = b.employeeid
left join all_user a on a.userid = b.userid
WHERE BrokerId = @BrokerId
AND ((Type ='P' AND IsPmlManager=1)  OR (Type <> 'P')  OR (Type IS NULL))


