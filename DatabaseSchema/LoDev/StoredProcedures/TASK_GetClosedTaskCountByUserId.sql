CREATE procedure [dbo].[TASK_GetClosedTaskCountByUserId]
	@BrokerId uniqueidentifier,
	@UserId uniqueidentifier,
	@SinceDate datetime,
	@IsCondition bit = null
AS
BEGIN
	SELECT count(*) as UserClosedTasks FROM TASK
	WHERE
		--Since TaskStatus and BrokerId are part of an index, 
		--specifying them really helps speed.
		BrokerId = @BrokerId and
		TaskStatus = 2 and
		TaskClosedUserId = @UserId and
		TaskClosedDate >= @SinceDate and (
	(@IsCondition  is not null and TaskIsCondition = @IsCondition ) or 
		(@IsCondition is null))
END