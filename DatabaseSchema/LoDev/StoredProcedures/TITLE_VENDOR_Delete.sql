-- =============================================
-- Author:		Antonio Valencia
-- Create date: 4/23/2013
-- Description:	Deletes a title vendor
-- =============================================
ALTER PROCEDURE [dbo].[TITLE_VENDOR_Delete]
	-- Add the parameters for the stored procedure here
	@Id int 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DELETE FROM TITLE_VENDOR
	WHERE TitleVendorId = @Id
	
END
