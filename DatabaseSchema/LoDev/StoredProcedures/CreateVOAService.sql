-- =============================================
-- Author:		Eric Mallare
-- Create date: 5/2/2017
-- Description:	Creates a VOA service
-- =============================================
ALTER PROCEDURE [dbo].[CreateVOAService]
	@ServiceT int,
	@IsADirectVendor bit,
	@SellsViaResellers bit,
	@IsAReseller bit,
	@AskForNotificationEmail bit,
	@VoaVerificationT int,
	@VendorId int,
	@OptionsFileDbKey uniqueidentifier,
	@IsEnabled bit,
	@VendorServiceId int OUTPUT
AS
BEGIN
	INSERT INTO VOA_SERVICE
		(ServiceT, IsADirectVendor, SellsViaResellers, IsAReseller, AskForNotificationEmail, VoaVerificationT, VendorId, 
		 OptionsFileDbKey, IsEnabled)
	VALUES
		(@ServiceT, @IsADirectVendor, @SellsViaResellers, @IsAReseller, @AskForNotificationEmail, @VoaVerificationT, @VendorId,
		 @OptionsFileDbKey, @IsEnabled)
	
	IF @@error!= 0 GOTO HANDLE_ERROR
	SET @VendorServiceId=SCOPE_IDENTITY();
	
	RETURN;
END

HANDLE_ERROR:
	RAISERROR('Error in CreateVOAService sp', 16, 1);;
	RETURN;
