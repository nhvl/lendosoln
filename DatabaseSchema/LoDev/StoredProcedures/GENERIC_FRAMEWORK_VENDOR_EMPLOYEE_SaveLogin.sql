-- =============================================
-- Author:		Justin Lara
-- Create date: 3/9/2015
-- Description:	Saves a set of generic framework
--              credentials for the given employee.
-- =============================================
ALTER PROCEDURE [dbo].[GENERIC_FRAMEWORK_VENDOR_EMPLOYEE_SaveLogin] 
	-- Add the parameters for the stored procedure here
	@EmployeeId uniqueidentifier,
	@ProviderId varchar(7),
	@Username varchar(100),
	@BrokerId uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE GENERIC_FRAMEWORK_VENDOR_EMPLOYEE_LOGIN
    SET
		Username = @Username,
		BrokerId = @BrokerId
	WHERE
		EmployeeId = @EmployeeId AND ProviderId = @ProviderId
		
	IF @@rowcount = 0
	BEGIN
		INSERT INTO GENERIC_FRAMEWORK_VENDOR_EMPLOYEE_LOGIN
		( EmployeeId,  ProviderId,  Username,  BrokerId)
		VALUES
		(@EmployeeId, @ProviderId, @Username, @BrokerId)
	END
END
