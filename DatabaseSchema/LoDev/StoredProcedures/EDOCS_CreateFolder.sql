CREATE procedure [dbo].[EDOCS_CreateFolder]
	@BrokerId uniqueidentifier,
	@FolderName varchar(50),
	@FolderId int OUT
as

if exists(select 1 
			from EDOCS_FOLDER 
			where FolderName = @FolderName
				and BrokerId = @BrokerId)
begin
	RAISERROR('A folder with the same name already exists. Please select another name.', 16, 1);
	return;
end
begin

insert EDOCS_FOLDER (BrokerId, FolderName)
		values (@BrokerId, @FolderName)
		SET @FolderId = SCOPE_IDENTITY()
end