CREATE  PROCEDURE CanReminderHasThisLoanName 
	@sLNm varchar(100),
	@BrokerId  uniqueidentifier
AS
if exists ( select top 1 sLId from Loan_File_cache with( nolock )
	WHERE sLNm = @sLNm and sBrokerID = COALESCE(@BrokerID, sBrokerID) )
	select 1;
select 0;
