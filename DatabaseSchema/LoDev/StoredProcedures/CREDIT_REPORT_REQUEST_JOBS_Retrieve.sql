-- =============================================
-- Author:		Eric Mallare
-- Create date: 9/13/2018
-- Description:	Retrieves an Credit Report Request job entry
-- =============================================
ALTER PROCEDURE [dbo].[CREDIT_REPORT_REQUEST_JOBS_Retrieve]
	@JobId int = null,
	@PublicJobId uniqueidentifier = null
AS
BEGIN
	SELECT TOP(1)
		j.JobId,
		j.ProcessingStatus,
		j.NextRunDate,
		j.PublicJobId,
		j.DateCreated,
		j.ExpirationDate,
		j.LastRunStartDate,
		j.MaxExceptionCount,
		j.ExceptionCount,
		j.DateCompleted,
		j.LoanId,
		j.CorrelationId,
		crr.ShouldReissueOnResponseNotReady,
		crr.RetryIntervalInSeconds,
		crr.MaxRetryCount,
		crr.AttemptCount,
		crr.BrokerId,	-- 11/2/18 - je - For backwards compatibility we get Broker and UserId from crr table.
		crr.UserId,		-- We should be able to change this in the future if necessary.
		crr.UserType,
		crr.CreditRequestDataFileDbGuidId,
		crr.CreditReportResponseFileDbGuidId
	FROM
		CREDIT_REPORT_REQUEST_JOBS crr WITH(ROWLOCK, READPAST) JOIN
		BACKGROUND_JOBS j WITH(ROWLOCK, READPAST) ON crr.JobId=j.JobId
	WHERE
		crr.JobId=@JobId OR
		j.PublicJobId=@PublicJobId
END