-- =============================================
-- Author:		Michael Leinweaver
-- Create date: 12/16/2016
-- Description:	Obtains data necessary to determine if
--				a loan should have rate monitoring disabled.
-- =============================================
ALTER PROCEDURE [dbo].[GetLoanDataForRateMonitorDisableCheck]
	@LoanId uniqueidentifier,
	@BrokerId uniqueidentifier
AS
BEGIN
	SELECT IsValid, sStatusT, sRateLockStatusT
	FROM LOAN_FILE_CACHE
	WHERE sLId = @LoanId AND sBrokerId = @BrokerId
END
