-- =============================================
-- Author:		Eric Mallare
-- Create date: 11/28/2017
-- Description:	Creates a new job for background processing
-- =============================================
ALTER PROCEDURE [dbo].[BACKGROUND_JOBS_Create]
	@JobId int OUTPUT,
	@ProcessingStatus int,
	@NextRunDate datetime,
	@JobType int,
	@PublicJobId uniqueidentifier,
	@ExpirationDate datetime,
	@DateCreated datetime,
	@ExpectedRunTimeInSeconds int = null,
	@CanRetryIfInLimbo bit,
	@LastRunStartDate datetime = null,
	@MaxExceptionCount int = null,
	@ExceptionCount int = 0,
	@BrokerId uniqueidentifier = null,
	@LoanId uniqueidentifier = null,
	@CorrelationId uniqueidentifier = null,
	@UserId uniqueidentifier = null,
	@Filter varchar(10) = null
AS
BEGIN
	INSERT INTO [dbo].[BACKGROUND_JOBS]
	(
		ProcessingStatus,
		NextRunDate,
		JobType,
		PublicJobId,
		ExpirationDate,
		DateCreated,
		ExpectedRunTimeInSeconds,
		CanRetryIfInLimbo,
		LastRunStartDate,
		MaxExceptionCount,
		ExceptionCount,
		BrokerId,
		LoanId,
		CorrelationId,
		UserId,
		Filter
	)
	VALUES
	(
		@ProcessingStatus,
		@NextRunDate,
		@JobType,
		@PublicJobId,
		@ExpirationDate,
		@DateCreated,
		@ExpectedRunTimeInSeconds,
		@CanRetryIfInLimbo,
		@LastRunStartDate,
		@MaxExceptionCount,
		@ExceptionCount,
		@BrokerId,
		@LoanId,
		@CorrelationId,
		@UserId,
		@Filter
	)
	
	SET @JobId = SCOPE_IDENTITY();
END