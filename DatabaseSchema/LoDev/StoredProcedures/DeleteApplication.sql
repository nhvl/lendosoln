ALTER PROCEDURE [dbo].[DeleteApplication]
	@AppId uniqueidentifier,
	@UserId uniqueidentifier
AS
declare @tranPoint sysname
set @tranPoint = object_name(@@procId) + CAST(@@nestlevel as varchar(10))
begin transaction
save transaction @tranPoint

declare @slid uniqueidentifier 
declare @AppCount int
declare @aBSsnEncrypted varbinary(max)
declare @aCSsnEncrypted varbinary(max)

select @sLId = slid, @aBSsnEncrypted = aBSsnEncrypted, @aCSsnEncrypted = aCSsnEncrypted from APPLICATION_A where aAppId = @AppId 

select @AppCount = count( * )
from APPLICATION_A
where sLId = @slid  
if(0!=@@error)
begin
	RAISERROR('Error in selecting AppCount from Application table in DeleteApplication sp', 16, 1);
	goto ErrorHandler;
end
if( @AppCount <= 1 )
begin
	RAISERROR('Cannot delete the only application in a loan file', 16, 1);
	goto ErrorHandler;
end	
else
begin

	DELETE FROM BORROWER_CREDIT_AUTHORIZATION_DOCUMENT_ASSOCIATION
	WHERE AppId = @AppId
	if(0!=@@error)
	begin
		RAISERROR('Error in delete from BORROWER_CREDIT_AUTHORIZATION_DOCUMENT_ASSOCIATION table in DeleteApplication sp', 16, 1);
		goto ErrorHandler;
	end

	EXEC EDOCS_InvalidateDocByAppId @AppId, @slid, @UserId, 'Delete Application'
	if(0!=@@error)
	begin
		RAISERROR('Error invalidating EDocs in DeleteApplication sp', 16, 1);
		goto ErrorHandler;
	end


	DELETE FROM CP_Consumer_Action_Response
	WHERE ConsumerActionRequestId in (select ConsumerActionRequestId from CP_Consumer_Action_Request where aAppId = @AppId )
	if(0!=@@error)
	begin
		RAISERROR('Error in delete from CP_Consumer_Action_Response table in DeleteApplication sp', 16, 1);
		goto ErrorHandler;
	end

	DELETE FROM CP_Consumer_Action_Request
	WHERE aAppId = @AppId
	if(0!=@@error)
	begin
		RAISERROR('Error in delete from CP_Consumer_Action_Request table in DeleteApplication sp', 16, 1);
		goto ErrorHandler;
	end
	
	DELETE FROM PENDING_DOCUMENT_REQUEST
	WHERE ApplicationId = @AppId
	if(0!=@@error)
	begin
		RAISERROR('Error in delete from PENDING_DOCUMENT_REQUEST table in DeleteApplication sp', 16, 1);
		goto ErrorHandler;
	end
	delete from IRS_4506T_ORDER_INFO
	where aAppId = @AppId;
	if(0!=@@error)
	begin
		RAISERROR('Error in deleting from IRS_4506T_ORDER_INFO table in DeleteApplication sp', 16, 1);
		goto ErrorHandler;
	end	
	
	-- ML, OPM 311311: unlock HMDA reporting coapplicant source SSN when removing that applicant.
	if exists (SELECT 1 FROM LOAN_FILE_B as b join LOAN_FILE_A as a on b.slid = a.slid WHERE b.sLId = @slid AND sHmdaCoApplicantSourceSsnLckd = 1 AND 
		(a.sHmdaCoApplicantSourceSsnEncrypted = @aBSsnEncrypted OR a.sHmdaCoApplicantSourceSsnEncrypted = @aCSsnEncrypted) )
	begin
		UPDATE LOAN_FILE_B
		SET sHmdaCoApplicantSourceSsnLckd = 0
		WHERE sLId = @slid
		if(0!=@@error)
		begin
			RAISERROR('Error in updating sHmdaCoApplicantSourceSsnLckd in DeleteApplication sp', 16, 1);
			goto ErrorHandler;
		end	
		UPDATE LOAN_FILE_A
		SET sHmdaCoApplicantSourceSsnEncrypted = null
		WHERE sLId = @slid
		if(0!=@@error)
		begin
			RAISERROR('Error in updating sHmdaCoApplicantSourceSsnEncrypted in DeleteApplication sp', 16, 1);
			goto ErrorHandler;
		end	
	end
	
	delete from APPLICATION_B
	where aAppId = @AppId;
	if(0!=@@error)
	begin
		RAISERROR('Error in deleting from Application_B table in DeleteApplication sp', 16, 1);
		goto ErrorHandler;
	end		
	delete from APPLICATION_A
	where aAppId = @AppId;
	if(0!=@@error)
	begin
		RAISERROR('Error in deleting from Application_A table in DeleteApplication sp', 16, 1);
		goto ErrorHandler;
	end	
end	

commit transaction
return 0;
ErrorHandler:
	rollback transaction @tranPoint
	rollback transaction
	return -100;

