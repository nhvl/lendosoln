CREATE PROCEDURE [dbo].[UpdateSystemArmIndex]
	@IndexName Varchar( 100 ),
	@IndexCurrentValue Decimal( 9 , 3 ),
	@MasterPmlBrokerId UniqueIdentifier
AS
	BEGIN TRANSACTION
	
	UPDATE Arm_Index
		SET 
		 IndexCurrentValueDecimal = @IndexCurrentValue
		, EffectiveD = getdate()
	WHERE
		BrokerIdGuid = @MasterPmlBrokerId -- SYSTEM
		AND
		IndexNameVstr = @IndexName
		
	--IF( @@error != 0 )
	--GOTO HANDLE_ERROR

		
	IF @@rowcount = 0
	BEGIN
		INSERT INTO Arm_Index ( BrokerIdGuid , IndexIdGuid , IndexNameVstr , IndexCurrentValueDecimal , EffectiveD )
			VALUES
		( @MasterPmlBrokerId
		, newid()
		, @IndexName
		, @IndexCurrentValue
		, getdate()
		)
		IF( @@error != 0 )
		GOTO HANDLE_ERROR
	END
	
	COMMIT TRANSACTION
	RETURN;
		
HANDLE_ERROR:
   	ROLLBACK TRANSACTION
  	RAISERROR('Error in UpdateSystemArmIndex sp', 16, 1);;
  	RETURN;