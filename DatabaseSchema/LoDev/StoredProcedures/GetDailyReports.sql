
-- =============================================
-- Author:		Long Nguyen
-- Create date: 8/4/2015
-- Description:	Update schedule report recovery (iOPM 215368): Get reports at current hour
-- =============================================
CREATE procedure [dbo].[GetDailyReports] 
as 
BEGIN
	select * from DAILY_REPORTS 
		where NextRun >= dateadd(day, -14, getdate())
			and NextRun <= dateadd(minute, 20, getdate())
		order by ReportId
END
