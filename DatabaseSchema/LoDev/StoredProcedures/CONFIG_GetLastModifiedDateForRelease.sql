-- =============================================
-- Author:		Antonio Valencia
-- Create date: 9/29/10	
-- Description:	Gets the most recent time
-- =============================================
CREATE PROCEDURE CONFIG_GetLastModifiedDateForRelease
	@OrgId uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

		SELECT TOP 1 ReleaseDate 
		FROM CONFIG_RELEASED 
		WHERE OrgId = @OrgId
		ORDER BY ReleaseDate DESC
END
