ALTER procedure [dbo].[EDOCS_SetSharedFaxNumber]
	@FaxNumber varchar(14),
	@DocRouterLogin varchar(50),
	@EncryptedDocRouterPassword varbinary(max) = null,
	@EncryptionKeyId uniqueidentifier = null
as

BEGIN TRANSACTION

  DELETE FROM EDOCS_FAX_NUMBER WHERE BrokerId IS NULL
  IF @@error!= 0 GOTO HANDLE_ERROR

  IF ( @FaxNumber <> '' OR @DocRouterLogin <> '' OR @EncryptedDocRouterPassword IS NOT NULL)
  BEGIN
	INSERT INTO EDOCS_FAX_NUMBER(FaxNumber, DocRouterLogin, EncryptedDocRouterPassword, EncryptionKeyId) 
	VALUES (@FaxNumber, @DocRouterLogin, @EncryptedDocRouterPassword, @EncryptionKeyId)
	IF @@error!= 0 GOTO HANDLE_ERROR
  END
	COMMIT TRANSACTION
	RETURN;

HANDLE_ERROR:
	ROLLBACK TRANSACTION
	RAISERROR('Error in EDOCS_SetSharedFaxNumber sp', 16, 1);;
	RETURN;