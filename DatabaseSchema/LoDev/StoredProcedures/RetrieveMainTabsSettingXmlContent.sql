-- =============================================
-- Author:		Eric Mallare
-- Create date: 11/4/2014
-- Description:	Retrieve MainTabsXMLContent
-- =============================================
CREATE PROCEDURE [dbo].[RetrieveMainTabsSettingXmlContent]
	@UserId uniqueidentifier
AS
	SELECT
		MainTabsSettingXmlContent
	FROM
		Broker_User
	WHERE
		UserId = @UserId
