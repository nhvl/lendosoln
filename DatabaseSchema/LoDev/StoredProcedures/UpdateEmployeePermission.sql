ALTER PROCEDURE [dbo].[UpdateEmployeePermission]
	@EmployeeID Guid,
	@Permission char(255),
	@BrokerID Guid
AS
	UPDATE Broker_User WITH(ROWLOCK, UPDLOCK)
	       SET Permissions = @Permission
	 WHERE EmployeeID = @EmployeeID
	
	if(0!=@@error)
	begin
		RAISERROR('Error in the update statement in UpdateEmployeePermission sp', 16, 1);
		return -100;
	end
	return 0;
