-- =============================================
-- Author:		Michael Leinweaver
-- Create date: 6/21/2017
-- Description:	Deletes a color theme and resets PML
--				brokers using the theme to use the default.
-- =============================================
ALTER PROCEDURE [dbo].[ColorTheme_Delete]
	@CollectionId int,
	@ColorThemeId uniqueidentifier,
	@BrokerId uniqueidentifier
AS
BEGIN
	UPDATE PML_BROKER
	SET TpoColorThemeId = '00000000-0000-0000-0000-000000000000'
	WHERE BrokerId = @BrokerId AND TpoColorThemeId = @ColorThemeId

	DELETE FROM BROKER_COLOR_THEME
	WHERE CollectionId = @CollectionId AND ColorThemeId = @ColorThemeId	
END