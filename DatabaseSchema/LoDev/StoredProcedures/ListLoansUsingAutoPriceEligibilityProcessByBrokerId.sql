CREATE PROCEDURE [dbo].[ListLoansUsingAutoPriceEligibilityProcessByBrokerId]
	@BrokerId uniqueidentifier
AS
BEGIN
	SELECT slid FROM Loan_File_Cache 
	WHERE
	sBrokerId = @BrokerId
	AND sIsIncludeInAutoPriceEligibilityProcess = 1
END