CREATE  PROCEDURE RefreshDiscussionLogs
	@BrokerId Guid , @LoanId Guid
AS
	DECLARE @Nm1 Varchar( 64 )
	DECLARE @Nm2 Varchar( 64 )
	DECLARE @IsValid Bit
	SELECT @Nm1 = c.sLNm , @Nm2 = c.sPrimBorrowerFullNm , @IsValid = c.IsValid FROM Loan_File_Cache AS c with(nolock)
	WHERE
		c.sBrokerID = @BrokerId
		AND
		c.sLId = @LoanId
	IF( @@ROWCOUNT != 1 )
	BEGIN
		RAISERROR('Specified loan not found in cache for RefreshDiscussionLogs sp', 16, 1);
		RETURN -100;
	END
	IF( 0 != @@ERROR )
	BEGIN
		RAISERROR('Error in the select statement in RefreshDiscussionLogs sp', 16, 1);
		RETURN -100;
	END
	UPDATE Discussion_Log
	SET
		DiscRefObjNm1 = @Nm1,
		DiscRefObjNm2 = @Nm2,
		DiscIsRefObjValid = @IsValid
	WHERE
		DiscRefObjId = @LoanId
		AND
		IsValid = 1
	IF( 0 != @@ERROR )
	BEGIN
		RAISERROR('Error in the update statement in RefreshDiscussionLogs sp', 16, 1);
		RETURN -100;
	END
	RETURN 0
