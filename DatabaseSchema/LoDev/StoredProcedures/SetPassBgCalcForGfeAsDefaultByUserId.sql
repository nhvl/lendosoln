-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE SetPassBgCalcForGfeAsDefaultByUserId
	@ByPassBgCalcForGfeAsDefault bit,
	@UserId UniqueIdentifier
AS
BEGIN
	UPDATE Broker_User
       SET ByPassBgCalcForGfeAsDefault = @ByPassBgCalcForGfeAsDefault
    WHERE  UserId = @UserId

END
