CREATE PROCEDURE GetAllSecurityQuestions  @IncludeObsolete bit AS
BEGIN
	SET NOCOUNT ON
	IF @IncludeObsolete = 0
		SELECT QuestionId, Question, IsObsolete FROM SECURITY_QUESTION WHERE IsObsolete = 0 ORDER BY IsObsolete, QuestionId
	ELSE
		SELECT QuestionId, Question, IsObsolete FROM SECURITY_QUESTION ORDER BY IsObsolete, QuestionId
END
