CREATE PROCEDURE [dbo].[UpdateScheduledReportUserInfo]
	@Email AS VARCHAR(200), 
	@CcIds AS VARCHAR(400) = NULL, 
	@ScheduleName AS VARCHAR(30), 
	@QueryId AS UNIQUEIDENTIFIER, 
	@ReportType AS VARCHAR(10),
	@ReportId AS INT
AS
UPDATE SCHEDULED_REPORT_USER_INFO
SET Email = @Email, 
	CcIds = @CcIds, 
	ScheduleName = @ScheduleName, 
	QueryId = @QueryId, 
	ReportType = @ReportType
WHERE ReportId = @ReportId