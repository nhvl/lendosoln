ALTER PROCEDURE [dbo].[ListIntegrationModifiedFilesByBrokerId_BrokerUser] 
	@AppCode uniqueidentifier,
	@BrokerId uniqueidentifier,
	@BranchId uniqueidentifier,
	@EmployeeId uniqueidentifier,
	@AccessLevel varchar(10)
AS

declare @last90daysDate as datetime
set @last90daysDate = dateadd(day, datediff(day, 0 ,getdate())-90, 0) 


IF( @AccessLevel = 'individual' )
	BEGIN
		SELECT ifm.sLId, sLNm, LastModifiedD, sOldLNm, IsValid, aBNm, sSpAddr, sStatusT, sLRefNm,
			ifm.aBSsnEncrypted, ifm.sEncryptionKey, ifm.sEncryptionMigrationVersion
		FROM Integration_File_Modified ifm JOIN LOAN_FILE_CACHE_4 lfc ON ifm.sLId = lfc.sLId
		WHERE BrokerId = @BrokerId AND AppCode=@AppCode
		AND lastModifiedD > @last90daysDate
		AND
		(
			sEmployeeManagerId = @EmployeeId 
			OR
			sEmployeeProcessorId = @EmployeeId
			OR
			sEmployeeLoanRepId = @EmployeeId
			OR
			sEmployeeLoanOpenerId = @EmployeeId
			OR
			sEmployeeCallCenterAgentId = @EmployeeId
			OR
			sEmployeeRealEstateAgentId = @EmployeeId
			OR
			sEmployeeLenderAccExecId = @EmployeeId
			OR
			sEmployeeLockDeskId = @EmployeeId
			OR
			sEmployeeUnderwriterId = @EmployeeId
			OR
			sEmployeeFunderId = @EmployeeId
			OR
			sEmployeeShipperId = @EmployeeId
			OR
			sEmployeePostCloserId = @EmployeeId
			OR
			sEmployeeInsuringId = @EmployeeId
			OR
			sEmployeeCollateralAgentId = @EmployeeId
			OR
			sEmployeeDocDrawerId = @EmployeeId
			OR
			sEmployeeCreditAuditorId = @EmployeeId
            OR
            sEmployeeDisclosureDeskId = @EmployeeId
            OR
            sEmployeeJuniorProcessorId = @EmployeeId
            OR
            sEmployeeJuniorUnderwriterId = @EmployeeId
            OR
            sEmployeeLegalAuditorId = @EmployeeId
            OR
            sEmployeeLoanOfficerAssistantId = @EmployeeId
            OR
            sEmployeePurchaserId = @EmployeeId
            OR
            sEmployeeQCComplianceId = @EmployeeId
            OR
            sEmployeeSecondaryId = @EmployeeId
            OR
            sEmployeeServicingId = @EmployeeId

		)
	END
ELSE IF( @AccessLevel = 'branch' )
	BEGIN
		SELECT ifm.sLId, sLNm, LastModifiedD, sOldLNm, IsValid, aBNm, sSpAddr, sStatusT, sLRefNm,
			ifm.aBSsnEncrypted, ifm.sEncryptionKey, ifm.sEncryptionMigrationVersion
		FROM Integration_File_Modified ifm JOIN LOAN_FILE_CACHE_4 lfc ON ifm.sLId = lfc.sLId
		WHERE BrokerId = @BrokerId AND AppCode=@AppCode
		AND lastModifiedD > @last90daysDate
		AND
		(
			sBranchID = @BranchId
			OR
			sEmployeeManagerId = @EmployeeId 
			OR
			sEmployeeProcessorId = @EmployeeId
			OR
			sEmployeeLoanRepId = @EmployeeId
			OR
			sEmployeeLoanOpenerId = @EmployeeId
			OR
			sEmployeeCallCenterAgentId = @EmployeeId
			OR
			sEmployeeRealEstateAgentId = @EmployeeId
			OR
			sEmployeeLenderAccExecId = @EmployeeId
			OR
			sEmployeeLockDeskId = @EmployeeId
			OR
			sEmployeeUnderwriterId = @EmployeeId
			OR
			sEmployeeFunderId = @EmployeeId
			OR
			sEmployeeShipperId = @EmployeeId
			OR
			sEmployeePostCloserId = @EmployeeId
			OR
			sEmployeeInsuringId = @EmployeeId
			OR
			sEmployeeCollateralAgentId = @EmployeeId
			OR
			sEmployeeDocDrawerId = @EmployeeId
			OR
			sEmployeeCreditAuditorId = @EmployeeId
            OR
            sEmployeeDisclosureDeskId = @EmployeeId
            OR
            sEmployeeJuniorProcessorId = @EmployeeId
            OR
            sEmployeeJuniorUnderwriterId = @EmployeeId
            OR
            sEmployeeLegalAuditorId = @EmployeeId
            OR
            sEmployeeLoanOfficerAssistantId = @EmployeeId
            OR
            sEmployeePurchaserId = @EmployeeId
            OR
            sEmployeeQCComplianceId = @EmployeeId
            OR
            sEmployeeSecondaryId = @EmployeeId
            OR
            sEmployeeServicingId = @EmployeeId
		)
	END
ELSE
	BEGIN
		SELECT ifm.sLId, ifm.sLNm, ifm.LastModifiedD, ifm.sOldLNm, ifm.IsValid, ifm.aBNm, ifm.sSpAddr,  ifm.sStatusT, lfc.sLRefNm,
			ifm.aBSsnEncrypted, ifm.sEncryptionKey, ifm.sEncryptionMigrationVersion
        FROM Integration_File_Modified ifm WITH(ROWLOCK, READCOMMITTED) JOIN LOAN_FILE_CACHE_4 lfc ON ifm.sLId = lfc.sLId
		WHERE ifm.BrokerId = @BrokerId AND AppCode=@AppCode
		and lastModifiedD > @last90daysDate 
	END



