
-- =============================================
-- Author:		Antonio Valencia
-- Create date: 5/2/11
-- Description:	GetSubscription
-- =============================================
CREATE PROCEDURE [dbo].[TASK_Subscription_GetSubscribed] 

	@TaskId varchar(10),
	@UserId uniqueidentifier = null
AS

-- REVIEW
-- 6/9/2011: Reviewed. -ThinhNK
BEGIN
	SELECT TaskId, UserId FROM
	TASK_SUBSCRIPTION 
	WHERE TaskId = @TaskId AND UserId = COALESCE(@UserId, UserId)
END
