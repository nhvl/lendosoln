CREATE PROCEDURE [dbo].[Billing_SSA89]
	@StartDate datetime,
	@EndDate datetime
AS
BEGIN
-- dd 2018-11-02 - For use in automate billing executable.

SELECT BrokerNm As LenderName, CustomerCode, sLNm As LoanNumber, cache.sLId As LoanId, oi.OrderId, OrderNumber, 
                                                       BorrowerName, DateOrdered, DateCompleted, vo.VendorId 
                                                FROM SSA89_Order oi WITH (nolock) JOIN VOX_Order vo WITH (nolock) ON oi.OrderId = vo.OrderId
                                                JOIN Application_A a WITH (nolock) ON a.aAppId = vo.ApplicationId
                                                JOIN Loan_File_Cache cache WITH (nolock) ON cache.sLId = a.sLId
                                                JOIN Broker WITH (nolock) ON Broker.BrokerId = cache.sBrokerId
                                                WHERE DateOrdered >= @StartDate AND DateOrdered < @EndDate
                                                AND Broker.SuiteType = 1 AND cache.sLoanFileT = 0
                                                AND IsTestOrder = 0
                                                ORDER BY CustomerCode, LoanNumber, OrderId
												
END