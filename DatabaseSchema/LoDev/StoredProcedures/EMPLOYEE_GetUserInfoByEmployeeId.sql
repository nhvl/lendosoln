/*
	Author: Scott Kibler
	Created Date: 9/9/2014
	Description: 
		Return some user info of the employee at the specified broker.
*/
CREATE PROCEDURE dbo.EMPLOYEE_GetUserInfoByEmployeeId
	@EmployeeID UniqueIdentifier,
	@BrokerID UniqueIdentifier
AS
BEGIN
	SELECT 
		Type as UserType,
		UserId
	FROM ALL_USER
	WHERE UserID in
	(
		SELECT 
			EmployeeUserID as UserID
		FROM
			EMPLOYEE
		WHERE
			EmployeeID = @EmployeeID
		AND BranchID in
		(
			SELECT BranchID
			FROM Branch
			WHERE BrokerID = @BrokerID
		)
	)
END