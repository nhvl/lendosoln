-- =============================================
-- Author:		David Dao
-- Create date: 7/8/2014
-- Description:	List all records from DB_CONNECTION.
-- =============================================
CREATE PROCEDURE [dbo].[DB_CONNECTION_ListAll]
AS
BEGIN
    SELECT Id, Code, Description, DataSource, InitialCatalog, FailoverPartner, 
	       ReadOnlyLogin, ReadOnlyEncryptedPassword, FullAccessLogin, FullAccessEncryptedPassword
    FROM DB_CONNECTION

END
