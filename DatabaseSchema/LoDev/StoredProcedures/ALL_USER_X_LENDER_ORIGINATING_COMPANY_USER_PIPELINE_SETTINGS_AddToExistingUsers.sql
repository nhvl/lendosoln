-- =============================================
-- Author:		Michael Leinweaver
-- Create date: 6/8/2018
-- Description:	Adds a new pipeline setting to all users for a lender.
-- =============================================
ALTER PROCEDURE [dbo].[ALL_USER_X_LENDER_ORIGINATING_COMPANY_USER_PIPELINE_SETTINGS_AddToExistingUsers]
	@BrokerId uniqueidentifier,
	@AddedPipelineSettingIdXml xml,
	@PortalMode tinyint
AS
BEGIN
	WITH 
		AFFECTED_USERS (UserId) AS (SELECT UserId FROM VIEW_ACTIVE_PML_USER WHERE BrokerId = @BrokerId),
		NEW_SETTINGS (SettingId) AS (SELECT T.Item.value('.', 'uniqueidentifier') FROM @AddedPipelineSettingIdXml.nodes('root/id') as T(Item))
		
	INSERT INTO ALL_USER_X_LENDER_ORIGINATING_COMPANY_USER_PIPELINE_SETTINGS(BrokerId, UserId, PortalMode, SettingId) 
	SELECT @BrokerId, UserId, @PortalMode, SettingId
	FROM AFFECTED_USERS CROSS JOIN NEW_SETTINGS
END
