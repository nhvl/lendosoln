
CREATE  PROCEDURE [dbo].[ListNonBoolRolePermissionsByBrokerId]
	@BrokerID uniqueidentifier,
	@RoleID uniqueidentifier
AS
	SELECT
		r.DefaultNonBoolPermissions
	FROM
		ROLE_DEFAULT_PERMISSIONS AS r
	WHERE
		r.BrokerId = @BrokerID
		AND
		r.RoleId = @RoleID

