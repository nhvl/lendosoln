











-- 7/23/08 Created for use with LOAdmin FindLoan page -AH-
CREATE PROCEDURE [dbo].[FindLoanAssignmentInfoByLoanId]
	-- Add the parameters for the stored procedure here
@loanId varchar(36)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT     (e.UserFirstNm + ' ' + e.UserLastNm) AS fullName,
				r.RoleModifiableDesc,
				a.LoginNm,	 
				a.UserId,
				a.Type, 
				a.IsActive as canLogin,  
				a.BrokerPmlSiteId,
				e.isActive,
				b.Status

	FROM        LOAN_USER_ASSIGNMENT AS lua WITH (NOLOCK) INNER JOIN
                ROLE AS r WITH (NOLOCK) ON lua.RoleId = r.RoleId RIGHT OUTER JOIN
                View_lendersoffice_Employee AS e WITH (NOLOCK) ON lua.EmployeeId = e.EmployeeId
				left join broker_user as bu on bu.employeeid = e.employeeid
				left join all_user as a on a.userid = bu.userid
				JOIN Branch AS br ON br.BranchId = e.BranchId 
				JOIN Broker AS b ON b.BrokerId = br.BrokerId 

	WHERE     lua.sLId = @loanId
END












