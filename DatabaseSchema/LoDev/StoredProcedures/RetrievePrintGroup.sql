CREATE PROCEDURE RetrievePrintGroup 
	@GroupID Guid,
	@BrokerID Guid
AS
	SELECT GroupName, GroupDefinition 
	FROM Print_Group
	WHERE GroupID = @GroupID AND OwnerID = @BrokerID
