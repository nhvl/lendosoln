-- =============================================
-- Author:		Eric Mallare
-- Create date: 4/27/2017
-- Description:	Adds a new platform and its associated transmission into the database.
-- =============================================
CREATE PROCEDURE [dbo].[VERIFICATION_PLATFORM_CreatePlatform]
	@PlatformName varchar(150),
	@TransmissionModelT int,
	@PayloadFormatT int,
	@UsesAccountId bit,
	@RequiresAccountId bit,
	@TargetUrl varchar(200),
	@TransmissionAuthenticationT int,
	@TransmissionAssociationT int = null,
	@EncryptionKeyId uniqueidentifier,
	@RequestCertLocation varchar(200) = null,
	@ResponseCertId uniqueidentifier = null,
	@RequestCredentialName varchar(50) = null,
	@RequestCredentialPassword varbinary(max) = null,
	@ResponseCredentialName varchar(50) = null,
	@ResponseCredentialPassword varbinary(max) = null,
	@TransmissionId int OUTPUT,
	@PlatformId int OUTPUT
AS
BEGIN TRANSACTION
	INSERT INTO VERIFICATION_TRANSMISSION
		(TransmissionAssociationT, TargetUrl, TransmissionAuthenticationT, EncryptionKeyId, RequestCertLocation, ResponseCertId,
		 RequestCredentialName, RequestCredentialPassword, ResponseCredentialName, ResponseCredentialPassword)
	VALUES
		(0, @TargetUrl, @TransmissionAuthenticationT, @EncryptionKeyId, @RequestCertLocation, @ResponseCertId, 
		 @RequestCredentialName, @RequestCredentialPassword, @ResponseCredentialName, @ResponseCredentialPassword)
	
	IF( @@error != 0 )
		GOTO HANDLE_ERROR
	SET @TransmissionId=SCOPE_IDENTITY();
		 
	INSERT INTO VERIFICATION_PLATFORM
		(PlatformName, TransmissionModelT, PayloadFormatT, TransmissionId, UsesAccountId, RequiresAccountId)
	VALUES
		(@PlatformName, @TransmissionModelT, @PayloadFormatT, @TransmissionId, @UsesAccountId, @RequiresAccountId)   
	
	IF( @@error != 0 )
		GOTO HANDLE_ERROR
	SET @PlatformId=SCOPE_IDENTITY();

COMMIT TRANSACTION

RETURN;

	HANDLE_ERROR:
		ROLLBACK TRANSACTION
		RAISERROR('Error in VERIFICATION_PLATFORM_CreatePlatform sp', 16, 1);
		RETURN;
