CREATE PROCEDURE [dbo].[Billing_GenericFramework]
	@StartDate datetime,
	@EndDate datetime
AS
BEGIN
-- dd 2018-11-02 - For use in automate billing executable.

SELECT BrokerNm As LenderName, CustomerCode, sLNm As LoanNumber, cache.sLId as LoanId, DateUploaded, DocumentId,TransactionId, gfb.ProviderId, gfb.ServiceType
                                                FROM Generic_Framework_Billing gfb WITH (nolock) JOIN Broker WITH (nolock) ON Broker.BrokerId = gfb.BrokerId
                                                                                                 JOIN Loan_File_Cache cache WITH (nolock) ON cache.sLId = gfb.LoanId
                                                WHERE DateUploaded >= @StartDate AND DateUploaded < @EndDate
                                                AND Broker.SuiteType = 1
                                                AND cache.sLoanFileT = 0
                                                ORDER BY ProviderId, CustomerCode, TransactionId
												
END