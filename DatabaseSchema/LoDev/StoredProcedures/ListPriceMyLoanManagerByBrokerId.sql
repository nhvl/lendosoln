CREATE PROCEDURE ListPriceMyLoanManagerByBrokerId
	@BrokerId Guid
AS
	SELECT
		e.EmployeeId ,
		e.UserFirstNm + ' ' + e.UserLastNm AS UserFullName
	FROM
		Employee AS e WITH (NOLOCK) JOIN Branch AS r WITH (NOLOCK) ON r.BranchId = e.BranchId
	WHERE
		e.IsPmlManager = 1
		AND
		r.BrokerId = @BrokerId
	ORDER BY
		e.UserLastNm , e.UserFirstNm
	if( 0!=@@error)
	begin
		RAISERROR('Error in the selection statement in ListPriceMyLoanManagerByBrokerId sp', 16, 1);
		return -100;
	end
	
	return 0;
