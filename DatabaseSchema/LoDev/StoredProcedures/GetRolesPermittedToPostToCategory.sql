-- =============================================
-- Author:		Scott Kibler
-- Create date: 3/20/2017
-- Description:	
--	Gets the ids of the roles that allow a user to post to the specified 
--  Conversation Category given by CategoryName, BrokerId.
-- =============================================
CREATE PROCEDURE [dbo].[GetRolesPermittedToPostToCategory]
	@CategoryName varchar(100),
	@BrokerId uniqueidentifier
AS
BEGIN
	SELECT
		   RoleId
	FROM
			ROLE_X_CONVERSATION_CATEGORY
	WHERE
				CategoryName = @CategoryName
			AND	BrokerId = @BrokerId
END
