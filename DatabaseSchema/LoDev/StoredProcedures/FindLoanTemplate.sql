CREATE PROCEDURE FindLoanTemplate 
	-- Add the parameters for the stored procedure here
	@BrokerId uniqueidentifier,
	@sLNm varchar(36)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT TOP 1 sLId FROM VIEW_LOAN_FILE_TEMPLATE WHERE sBrokerId = @BrokerId AND sLNm = @sLNm
END