CREATE PROCEDURE dbo.EDOCS_ShippingTemplateEditCheckAccess
	@ShippingTemplateName varchar(50),
	@List varchar(MAX),
	@ShippingTemplateId int,
	@BrokerId uniqueidentifier
as

select 1
	from EDOCS_SHIPPING_TEMPLATE 
	where ShippingTemplateId = @ShippingTemplateId
		and (BrokerId <> @BrokerId or BrokerId is null)