ALTER PROCEDURE [dbo].[DeleteCreditProxy] 
	@BrokerID Guid,
	@CrAccProxyId Guid
AS
	UPDATE Credit_Report_Account_Proxy
	SET IsValid = 0
	WHERE BrokerID = @BrokerID AND CrAccProxyId = @CrAccProxyId
