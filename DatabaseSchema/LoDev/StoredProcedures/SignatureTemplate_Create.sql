-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SignatureTemplate_Create]
	@TemplateId UniqueIdentifier,
	@BrokerId UniqueIdentifier,
	@OwnerUserId UniqueIdentifier,
	@Name varchar(100),
	@FieldMetadata varchar(max)
AS
BEGIN
	INSERT INTO LO_SIGNATURE_TEMPLATE(TemplateId, BrokerId, OwnerUserId, Name, FIeldMetadata)
		VALUES (@TemplateId, @BrokerId, @OwnerUserId, @Name, @FieldMetadata)
END
