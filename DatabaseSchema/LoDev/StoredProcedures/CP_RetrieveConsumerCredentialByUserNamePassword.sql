



CREATE PROCEDURE [dbo].[CP_RetrieveConsumerCredentialByUserNamePassword]
	@LoginName varchar(80),
	@Password varchar(50),
	@BrokerId uniqueidentifier
AS

SELECT ConsumerId, Email, BrokerId 
FROM CP_CONSUMER
WHERE	BrokerId = @BrokerId	AND
		Email = @LoginName		AND
		PasswordHash = @Password AND
		IsLocked = 0

