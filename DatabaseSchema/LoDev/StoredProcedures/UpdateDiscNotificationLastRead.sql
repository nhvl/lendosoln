CREATE PROCEDURE UpdateDiscNotificationLastRead
	@NotifId uniqueidentifier,
	@LastReadDate datetime
AS
	UPDATE Discussion_Notification
	SET
		NotifLastReadD = @LastReadDate
	WHERE
		NotifId = @NotifId
	IF( 0!=@@error )
	BEGIN
		RAISERROR('Error in updating Discussion_Notification in UpdateDiscNotificationLastRead sp', 16, 1);
		return -100;
	END
	RETURN 0;
