-- =============================================
-- Author:		Justin Lara
-- Create date: 2/20/2018
-- Description:	Retrieves a credential set for
--				the given lender.
-- =============================================
ALTER PROCEDURE [dbo].[CENLAR_LENDER_CREDENTIAL_Retrieve]
	@BrokerId uniqueidentifier,
	@EnvironmentT int
AS
BEGIN
	SELECT
		BrokerId,
		EnvironmentT,
		UserId,
		ApiKey,
		LastApiKeyReset,
		CustomerId
	FROM CENLAR_LENDER_CREDENTIAL 
	WHERE BrokerId = @BrokerId
		AND EnvironmentT = @EnvironmentT
END
GO
