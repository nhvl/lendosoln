ALTER procedure [dbo].[EDOCS_DocTypeAddCheckName]
	@BrokerId uniqueidentifier,
	@FolderId int,
	@DocTypeName varchar(50),
	@ClassificationId int = 0,
    @EsignTargetDocTypeId int = null
as

select 1 
	from EDOCS_DOCUMENT_TYPE with (nolock)
	where DocTypeName = @DocTypeName
		and BrokerId = @BrokerId
		and IsValid = 1
