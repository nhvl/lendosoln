﻿-- =============================================
-- Author:		David Dao
-- Create date: 
-- Description:	
-- =============================================
ALTER PROCEDURE [dbo].[ListSandboxLoans] 
    @sBrokerId uniqueidentifier,
    @sSrcsLId uniqueidentifier
AS
BEGIN

SELECT cache.sLId, cache.sCreatedD,cache.sLNm 
FROM loan_file_b lb join loan_file_cache cache on lb.slid=cache.slid 
WHERE ssrcslid=@sSrcsLId
and cache.isvalid=1
and cache.sBrokerId = @sBrokerId
and lb.sloanfilet = 1

END
