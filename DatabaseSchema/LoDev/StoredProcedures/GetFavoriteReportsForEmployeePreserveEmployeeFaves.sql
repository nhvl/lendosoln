-- =============================================
-- Author:		Eric Mallare
-- Create date: 10/29/2015
-- Description:	Grabs employee custom report favorites. Preserves all items from the Employee Favorites table
-- =============================================
ALTER PROCEDURE [dbo].[GetFavoriteReportsForEmployeePreserveEmployeeFaves]	
	@EmployeeId uniqueidentifier
AS
BEGIN
	SET NOCOUNT ON
	SELECT
		r.QueryId, 
		r.BrokerId,
		r.EmployeeId,
		r.QueryName,
		r.IsPublished,
		r.NamePublishedAs,
		r.ShowPosition,
		r.XmlContent,
		e.UserFirstNm + ' ' + e.UserLastNm AS EmployeeName, 
		CASE f.EmployeeId WHEN NULL THEN 0 ELSE 1 END AS IsFavorite, 
		f.QueryId as FavQId,
		e.IsActive as IsActiveUser
	FROM
		Report_Query AS r 
		INNER JOIN Employee AS e
			ON e.EmployeeId = r.EmployeeId
		INNER JOIN BROKER_USER b
			ON b.EmployeeId = e.EmployeeId
		INNER JOIN ALL_USER a
			ON a.UserId = b.UserId
		RIGHT JOIN EMPLOYEE_FAVORITE_REPORTS f 
			ON f.QueryId = r.QueryId 
	WHERE f.EmployeeId = @EmployeeId
	ORDER BY r.QueryName
END
