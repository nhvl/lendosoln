-- =============================================
-- Author:		Michael Leinweaver
-- Create date: 9/13/2017
-- Description:	Retrieves OCR request data.
-- =============================================
ALTER PROCEDURE [dbo].[EDOCS_OCR_REQUEST_Retrieve]
	@OcrRequestId uniqueidentifier,
	@BrokerId uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT 
		ocr.LoanId, 
		ocr.AppId, 
		ocr.Origin, 
		ocr.DocumentSource,
		ocr.UploadingUserId,
		ocr.UploadingUserType, 
		ocr.LatestStatus, 
		ocr.LatestStatusDate,
		lfc.sLNm
    FROM 
		EDOCS_OCR_REQUEST ocr
		JOIN LOAN_FILE_CACHE lfc ON ocr.LoanId = lfc.sLId
    WHERE 
		OcrRequestId = @OcrRequestId AND 
		BrokerId = @BrokerId
END
