ALTER PROCEDURE [dbo].CUSTOM_COC_FIELD_LIST_RETRIEVE
	@BrokerId uniqueidentifier
AS
BEGIN
	SELECT FieldId, [Description], IsInstant
	FROM CUSTOM_COC_FIELD_LIST
	WHERE BrokerId = @BrokerId
END
