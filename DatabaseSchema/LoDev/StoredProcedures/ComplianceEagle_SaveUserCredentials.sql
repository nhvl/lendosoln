-- =============================================
-- Author:		Timothy Jewell
-- Create date: 7/31/2014
-- Description:	Saves User-level ComplianceEagle Credentials
-- =============================================
CREATE PROCEDURE [dbo].[ComplianceEagle_SaveUserCredentials]
	@UserId uniqueidentifier,
	@ComplianceEagleUserName varchar(50),
	@ComplianceEaglePassword varbinary(MAX)
AS
BEGIN
	SET NOCOUNT ON;

	UPDATE BROKER_USER
	SET
		ComplianceEagleUserName = @ComplianceEagleUserName,
		ComplianceEaglePassword = @ComplianceEaglePassword
	WHERE UserID = @UserID
END
