-- =============================================
-- Author:		Michael Leinweaver
-- Create date: 1/24/2017
-- Description:	Determines whether the universal 
-- loan identifier for a loan is unique for a given broker.
-- =============================================
ALTER PROCEDURE [dbo].[CheckForUniqueUniversalLoanIdentifier]
	@BrokerId uniqueidentifier,
	@ThisLoanId uniqueidentifier,
	@UniversalLoanIdentifier varchar(45)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT TOP 1 lfc.sLId
	FROM LOAN_FILE_CACHE lfc JOIN LOAN_FILE_CACHE_4 lfc4 ON lfc.sLId = lfc4.sLId
	WHERE 
		lfc.IsValid = 1 AND
		lfc.sBrokerId = @BrokerId AND
		lfc.sLId <> @ThisLoanId AND
		lfc4.sUniversalLoanIdentifier = @UniversalLoanIdentifier
END
