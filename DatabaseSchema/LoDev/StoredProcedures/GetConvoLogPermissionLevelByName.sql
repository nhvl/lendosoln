-- ==========================================================================
-- Author: Scott Kibler
-- Create date: 5/10/2017
-- Description: 
--  Gets the convolog permission level info for the broker and name.
-- ==========================================================================
ALTER PROCEDURE [dbo].[GetConvoLogPermissionLevelByName]
	@BrokerId uniqueidentifier,
	@Name varchar(40)
AS
BEGIN
	SELECT
			Id, IsActive, Name, Description
	FROM
			CONVOLOG_PERMISSION_LEVEL
	WHERE 
			BrokerId = @BrokerId
		AND Name = @Name
END