-- =============================================
-- Author:		David Dao
-- Create date: 8/2/2013
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[IRS_4506T_VENDOR_CONFIGURATION_Update]
	@VendorId UniqueIdentifier,
	@VendorName varchar(200),
	@IsValid bit,
	@ExportPath varchar(1024),
	@IsUseAccountId bit,
    @EnableConnectionTest bit,
    @CommunicationModel tinyint
AS
BEGIN

	UPDATE IRS_4506T_VENDOR_CONFIGURATION
		SET VendorName=@VendorName,
			IsValid= @IsValid,
			ExportPath = @ExportPath,
			IsUseAccountId=@IsUseAccountId,
            EnableConnectionTest = @EnableConnectionTest,
            CommunicationModel = @CommunicationModel
	WHERE VendorId = @VendorId

END
