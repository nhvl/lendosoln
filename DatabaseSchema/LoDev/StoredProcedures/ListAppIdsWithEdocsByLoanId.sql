-- =============================================
-- Author:		Eduardo Michel
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[ListAppIdsWithEdocsByLoanId]
	-- Add the parameters for the stored procedure here
	@sLId uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
		Select Distinct aAppId
	From [dbo].[EDOCS_DOCUMENT]
	Where sLId = @sLId AND [IsValid] = 1;

    -- Insert statements for procedure here
END
