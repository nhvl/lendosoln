
CREATE PROCEDURE [dbo].[MBS_FetchTradesInPool] 
	@BrokerId uniqueidentifier, 
	@PoolId bigint
AS
BEGIN
	SET NOCOUNT ON;
	SELECT * FROM MBS_TRADE
	WHERE PoolId = @PoolId 
	  AND BrokerId = @BrokerId
END
