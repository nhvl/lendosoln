ALTER PROCEDURE [dbo].[TrackIntegrationModifiedFile_SpecificAppCode]
	@BrokerId uniqueidentifier,
	@AppCode uniqueidentifier,
	@LoanName varchar(36),
	@LoanId uniqueidentifier
AS

DECLARE @aBSsnEncrypted varbinary(max)
DECLARE @sEncryptionKey uniqueidentifier
DECLARE @sEncryptionMigrationVersion tinyint

SELECT @sEncryptionMigrationVersion = sEncryptionMigrationVersion,  @aBSsnEncrypted = aBSsnEncrypted, @sEncryptionKey = sEncryptionKey
FROM LOAN_FILE_CACHE_4 as lc4 JOIN LOAN_FILE_CACHE as lc ON lc4.sLId = lc.sLId
WHERE lc4.sLId = @LoanId


BEGIN
	SELECT sLId FROM Integration_File_Modified WHERE sLId = @LoanID AND AppCode=@AppCode
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO Integration_File_Modified 
				(AppCode, sLId, sLNm, aBNm, sSpAddr, lc.BrokerId,  IsValid, sBranchID, sEmployeeManagerId, sEmployeeProcessorId,sEmployeeLoanRepId, sEmployeeLoanOpenerId,sEmployeeCallCenterAgentId, sEmployeeRealEstateAgentId, sEmployeeLenderAccExecId, sEmployeeLockDeskId, sEmployeeUnderwriterId, PmlExternalManagerEmployeeId, sEmployeeShipperId, sEmployeeFunderId, sEmployeePostCloserId, sEmployeeInsuringId, sEmployeeCollateralAgentId, sEmployeeDocDrawerId, sStatusT, sEmployeeCreditAuditorId, sEmployeeDisclosureDeskId, sEmployeeJuniorProcessorId, sEmployeeJuniorUnderwriterId, sEmployeeLegalAuditorId, sEmployeeLoanOfficerAssistantId, sEmployeePurchaserId, sEmployeeQCComplianceId, sEmployeeSecondaryId, sEmployeeServicingId, sOldLNm, sEncryptionKey, sEncryptionMigrationVersion, aBSsnEncrypted ) 
		SELECT	 @AppCode, lc.sLId, @LoanName, aBNm, sSpAddr, @BrokerId, IsValid, sBranchID, sEmployeeManagerId, sEmployeeProcessorId,sEmployeeLoanRepId, sEmployeeLoanOpenerId,sEmployeeCallCenterAgentId, sEmployeeRealEstateAgentId, sEmployeeLenderAccExecId, sEmployeeLockDeskId, sEmployeeUnderwriterId, PmlExternalManagerEmployeeId, sEmployeeShipperId, sEmployeeFunderId, sEmployeePostCloserId, sEmployeeInsuringId, sEmployeeCollateralAgentId, sEmployeeDocDrawerId, sStatusT, sEmployeeCreditAuditorId, sEmployeeDisclosureDeskId, sEmployeeJuniorProcessorId, sEmployeeJuniorUnderwriterId, sEmployeeLegalAuditorId, sEmployeeLoanOfficerAssistantId, sEmployeePurchaserId, sEmployeeQCComplianceId, sEmployeeSecondaryId, sEmployeeServicingId /*! only doing coalesce for dev.*/, coalesce(sOldLNm,''), @sEncryptionKey, @sEncryptionMigrationVersion, @aBSsnEncrypted
		FROM LOAN_FILE_CACHE lc
			JOIN LOAN_FILE_CACHE_2 lc2 on lc.sLId = lc2.sLId
			JOIN LOAN_FILE_CACHE_3 lc3 ON lc.sLId = lc3.sLId
		WHERE lc.sLId = @LoanID
		OPTION (FORCE ORDER)
	END
	ELSE
	BEGIN
		UPDATE Integration_File_Modified WITH(ROWLOCK, UPDLOCK)
		SET LastModifiedD = GetDate(),
			sLNm = @LoanName,
			aBNm = lc.aBNm,
			sSpAddr = lc.sSpAddr,
			IsValid = lc.IsValid,
			sBranchID = lc.sBranchID,
			sEmployeeManagerId = lc.sEmployeeManagerId,
			sEmployeeProcessorId = lc.sEmployeeProcessorId,
			sEmployeeLoanRepId = lc.sEmployeeLoanRepId,
			sEmployeeLoanOpenerId = lc.sEmployeeLoanOpenerId,
			sEmployeeCallCenterAgentId = lc.sEmployeeCallCenterAgentId,
			sEmployeeRealEstateAgentId = lc.sEmployeeRealEstateAgentId,
			sEmployeeLenderAccExecId = lc.sEmployeeLenderAccExecId,
			sEmployeeLockDeskId = lc.sEmployeeLockDeskId,
			sEmployeeUnderwriterId = lc.sEmployeeUnderwriterId,
			PmlExternalManagerEmployeeId = lc.PmlExternalManagerEmployeeId,
			sEmployeeShipperId = lc3.sEmployeeShipperId,
			sEmployeeFunderId = lc3.sEmployeeFunderId,
			sEmployeePostCloserId = lc3.sEmployeePostCloserId,
			sEmployeeInsuringId = lc3.sEmployeeInsuringId,
			sEmployeeCollateralAgentId = lc3.sEmployeeCollateralAgentId,
			sEmployeeDocDrawerId = lc3.sEmployeeDocDrawerId,
			sStatusT = lc.sStatusT,
			sEmployeeCreditAuditorId = lc3.sEmployeeCreditAuditorId,
			sEmployeeDisclosureDeskId	= lc3.sEmployeeDisclosureDeskId,
			sEmployeeJuniorProcessorId = lc3.sEmployeeJuniorProcessorId,
		    sEmployeeJuniorUnderwriterId = lc3.sEmployeeJuniorUnderwriterId,
			sEmployeeLegalAuditorId = lc3.sEmployeeLegalAuditorId,
			sEmployeeLoanOfficerAssistantId = lc3.sEmployeeLoanOfficerAssistantId,
			sEmployeePurchaserId = lc3.sEmployeePurchaserId, 
			sEmployeeQCComplianceId = lc3.sEmployeeQCComplianceId, 
			sEmployeeSecondaryId = lc3.sEmployeeSecondaryId,	 
			sEmployeeServicingId = lc3.sEmployeeServicingId,
			sOldLNm = coalesce(lc2.sOldLNm,''),
			sEncryptionKey = @sEncryptionKey,
			sEncryptionMigrationVersion = @sEncryptionMigrationVersion,
			aBSsnEncrypted = @aBSsnEncrypted
		FROM Integration_File_Modified I
			JOIN LOAN_FILE_CACHE lc on I.sLId = lc.sLId
			JOIN LOAN_FILE_CACHE_2 lc2 on I.sLId = lc2.sLId
			JOIN LOAN_FILE_CACHE_3 lc3 on I.sLId = lc3.sLId		
		WHERE AppCode=@AppCode AND I.sLId = @LoanID
		OPTION	(FORCE ORDER)
		
		
	END
END
