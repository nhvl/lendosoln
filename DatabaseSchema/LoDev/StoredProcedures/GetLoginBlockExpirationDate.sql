
CREATE   PROCEDURE [dbo].[GetLoginBlockExpirationDate]
	@LoginNm varchar(36),
	@Type char(1),
	@BrokerPmlSiteId uniqueidentifier = '00000000-0000-0000-0000-000000000000'
AS
	SELECT LoginBlockExpirationD
	FROM ALL_USER 
	WHERE LoginNm = @LoginNm AND Type = @Type AND BrokerPmlSiteId = @BrokerPmlSiteId

