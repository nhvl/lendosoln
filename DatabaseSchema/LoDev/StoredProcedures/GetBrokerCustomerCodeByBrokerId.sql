CREATE PROCEDURE GetBrokerCustomerCodeByBrokerId
	@BrokerId Guid
AS
BEGIN
	SELECT CustomerCode FROM Broker WHERE BrokerId=@BrokerId
END
