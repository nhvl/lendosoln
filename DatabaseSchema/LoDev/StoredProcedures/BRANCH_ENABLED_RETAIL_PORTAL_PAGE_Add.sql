-- =============================================
-- Author:		Michael Leinweaver
-- Create date: 10/20/2017
-- Description:	Adds a list of custom page IDs set
--				at the branch level.
-- =============================================
CREATE PROCEDURE [dbo].[BRANCH_ENABLED_RETAIL_PORTAL_PAGE_Add]
	@BranchId uniqueidentifier,
	@AddedPageXml xml
AS
BEGIN
	INSERT INTO BRANCH_ENABLED_RETAIL_PORTAL_PAGE(BranchId, PageId)
	SELECT @BranchId, T.Item.value('.', 'uniqueidentifier')
	FROM @AddedPageXml.nodes('r/p') as T(Item)
END
