CREATE PROCEDURE [dbo].[UCD_DELIVERY_create]
    @LoanId uniqueidentifier,
    @BrokerId uniqueidentifier,
    @UcdDeliveryType int,
    @DeliveredTo int,
    @FileDeliveredDocumentId uniqueidentifier = null, -- TODO: REMOVE THIS ONCE I FIX JS ON UI.
    @CaseFileId varchar(50),
    @DateOfDelivery smalldatetime,
    @Note varchar(500),
    @IncludeInUldd bit,
    @UcdDeliveryId int out
AS
BEGIN
	INSERT INTO UCD_DELIVERY
	(
		LoanId,
		BrokerId,
		UcdDeliveryType,
		DeliveredTo,
		FileDeliveredDocumentId,
		CaseFileId,
		DateOfDelivery,
		Note,
		IncludeInUldd
	)
	VALUES
	(
		@LoanId,
		@BrokerId,
		@UcdDeliveryType,
		@DeliveredTo,
		@FileDeliveredDocumentId,
		@CaseFileId,
		@DateOfDelivery,
		@Note,
		@IncludeInUldd
	)
	
	SET @UcdDeliveryId = SCOPE_IDENTITY()
END