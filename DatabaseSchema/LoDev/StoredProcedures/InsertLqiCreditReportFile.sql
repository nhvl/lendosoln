ALTER PROCEDURE [dbo].[InsertLqiCreditReportFile]
	@Owner GUID,
	@ExternalFileId  ShortDesc,
	@DbFileKey Guid,
	@ComId Guid,
	@UserId Guid,
	@CrAccProxyId Guid,
	@HowDidItGetHere varchar(50),
	@BrandedProductName varchar(50)
AS
	DECLARE @tranPoint SYSNAME
	set @tranPoint = object_name(@@procId) + CAST(@@nestlevel as varchar(10))
	
	BEGIN TRANSACTION 
	SAVE TRANSACTION @tranPoint
	INSERT INTO Service_File(ComId, ServiceType, FileType, Owner, ExternalFileId, DbFileKey, UserId,  CrAccProxyId, HowDidItGetHere, BrandedProductName)
             VALUES (@ComId, 'Credit', 'LQICreditReport', @Owner, @ExternalFileId, @DbFileKey, @UserId,  @CrAccProxyId, @HowDidItGetHere, @BrandedProductName)
	IF ( 0!=@@error )
	BEGIN
			RAISERROR('Error in inserting into Service_File table in InsertLqiCreditReportFile sp', 16, 1);
			goto ErrorHandler;
	END
	COMMIT TRANSACTION
	RETURN 0;
ErrorHandler:
	rollback transaction @tranPoint
	rollback transaction
	return -100
