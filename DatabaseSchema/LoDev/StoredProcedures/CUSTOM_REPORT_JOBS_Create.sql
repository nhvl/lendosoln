-- =============================================
-- Author:		Michael Leinweaver
-- Create date: 6/8/2018
-- Description:	Creates a custom report job.
-- =============================================
ALTER PROCEDURE [dbo].[CUSTOM_REPORT_JOBS_Create]
	@JobId int,
	@QueryId uniqueidentifier,
	@BrokerId uniqueidentifier,
	@UserId uniqueidentifier,
	@UserType char(1),
	@PortalMode tinyint,
	@AlternateSorting varchar(200) = NULL,
	@ReportExtent tinyint,
	@ReportRunType tinyint,
	@PollingIntervalInSeconds int
AS
BEGIN
	INSERT INTO CUSTOM_REPORT_JOBS(
		JobId,
		QueryId,
		BrokerId,
		UserId,
		UserType,
		PortalMode,
		AlternateSorting,
		ReportExtent,
		ReportRunType,
		UserLacksReportRunPermission,
		PollingIntervalInSeconds)
	VALUES (
		@JobId,
		@QueryId,
		@BrokerId,
		@UserId,
		@UserType,
		@PortalMode,
		@AlternateSorting,
		@ReportExtent,
		@ReportRunType,
		0, -- UserLacksReportRunPermission
		@PollingIntervalInSeconds)
END