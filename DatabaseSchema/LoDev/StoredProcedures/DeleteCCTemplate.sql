ALTER PROCEDURE [dbo].[DeleteCCTemplate]
	@cCCTemplateId GUID
AS

delete from CC_TEMPLATE
where cCCTemplateId = @cCCTemplateId 

if(0!=@@error)
	begin
		RAISERROR('Error in deleting from CC_Template table in DeleteCCTemplate sp', 16, 1);
		goto ErrorHandler;
	end	

	return 0;
ErrorHandler:

	return -100;
