CREATE PROCEDURE [dbo].[Billing_EDocs]
	@StartDate datetime,
	@EndDate datetime
AS
BEGIN
-- dd 2018-11-02 - For use in automate billing executable.

-- 10/2/2018 - dd - Per Api, we only generate summary for PML0171.
SELECT DISTINCT br.BrokerNm As LenderName, br.CustomerCode, lc.sLId AS LoanId, lc.sLNm AS LoanNumber
                           FROM EDOCS_DOCUMENT doc WITH(NOLOCK) JOIN Broker br WITH(NOLOCK) ON doc.BrokerId = br.BrokerID
                                                                JOIN LOAN_FILE_CACHE lc WITH(NOLOCK) ON doc.sLId = lc.sLId
                           WHERE doc.IsValid=1 AND doc.LastModifiedDate >= @StartDate AND doc.LastModifiedDate < @EndDate
                             AND br.SuiteType = 1
                             AND br.CustomerCode IN ('PML0171')
END