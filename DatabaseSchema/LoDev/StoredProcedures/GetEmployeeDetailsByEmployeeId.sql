




CREATE PROCEDURE [dbo].[GetEmployeeDetailsByEmployeeId] 
	@EmployeeId uniqueidentifier
AS
	SELECT 
		e.Phone, 
		e.Fax, 
		e.CellPhone,	
		e.Pager, 
		e.Addr, 
		e.City, 
		e.State, 
		e.Zip, 
		e.Email,
		e.UserFirstNm + ' '  + e.UserLastNm AS FullName, 
		e.LicenseXmlContent, 
		COALESCE(pmlBroker.Name, '') AS PmlBrokerCompany, 
		e.BranchId,
		bu.PmlBrokerId,
		COALESCE(pmlBroker.LicenseXmlContent, '') as PmlBrokerLicenseXmlContent
	
FROM Employee e WITH (NOLOCK) LEFT JOIN Broker_User bu WITH(NOLOCK) ON e.EmployeeUserId = bu.UserId
         LEFT JOIN Pml_Broker pmlBroker WITH(NOLOCK) ON bu.PmlBrokerId = pmlBroker.PmlBrokerId
    WHERE e.EmployeeId = @EmployeeId




