-- =============================================
-- Author:		Timothy Jewell
-- Create date: 6/28/2017
-- Description:	Creates an SSA-89 service.
-- =============================================
CREATE PROCEDURE [dbo].[SSA89_SERVICE_Create]
	@VendorId int,
	@IsEnabled bit,
	@IsADirectVendor bit,
	@SellsViaResellers bit,
	@IsAReseller bit,
	@VendorServiceId int OUTPUT
AS
BEGIN
	INSERT INTO SSA89_SERVICE (
		VendorId,
		IsEnabled,
		IsADirectVendor,
		SellsViaResellers,
		IsAReseller)
	VALUES (
		@VendorId,
		@IsEnabled,
		@IsADirectVendor,
		@SellsViaResellers,
		@IsAReseller)
	
	IF @@error!= 0 GOTO HANDLE_ERROR
	SET @VendorServiceId=SCOPE_IDENTITY();
	RETURN;
END

HANDLE_ERROR:
	RAISERROR('Error in SSA89_SERVICE_Create sp', 16, 1);;
	RETURN;
GO
