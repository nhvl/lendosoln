-- =============================================
-- Author:		Justin Lara
-- Create date: 3/8/2018
-- Description:	Retrieves data on all lenders with
--				the Cenlar integration enabled.
-- =============================================
ALTER PROCEDURE [dbo].[GetLendersUsingCenlarIntegration]
AS
BEGIN
	SET NOCOUNT ON;

    SELECT
		b.BrokerId,
		b.CustomerCode,
		b.BrokerNm,
		b.IsCenlarEnabled,
		c.EnableAutomaticTransmissions
	FROM BROKER b
		JOIN CENLAR_LENDER_CONFIGURATION c ON b.BrokerId = c.BrokerId
	WHERE b.Status = 1
		AND b.IsCenlarEnabled = 1
END
