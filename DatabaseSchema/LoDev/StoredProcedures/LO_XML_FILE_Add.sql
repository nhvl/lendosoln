/*
	Author: 
		Scott Kibler
	Description: 
		Adds the lo xml file, we can find it in filedb in filedb: Normal, key: loxml_<Name>
	opm:
		463659
	Remarks:
		Step 1 confirm if name exists here using ExistsLoXmlFileWithName @Name
		Step 2 if not existing, add to filedb
		Step 3 add to this table using this sproc
		Step 4 if fails rollback filedb update.
		The reason for the order is that if we insert here before saving to filedb, people can mistakenly pull a nonexistant file from the db.
	Datalayer responsibility:
		The datalayer should ensure that the Name does not contain any DEL or other control characters, as that could mean clobbering other filedb entries e.g. DELx6<someOtherFileDBKey>.
	Returns:
		The id of the inserted record.
*/
CREATE PROCEDURE [dbo].[LO_XML_FILE_Add]
	@Name varchar(255)
AS
BEGIN
	INSERT INTO 
		LO_XML_FILE 
			( Name)
	OUTPUT
		INSERTED.Id As Id
	VALUES 
			(@Name)
END