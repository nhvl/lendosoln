-- =============================================
-- Author:		Timothy Jewell
-- Create date: 9/9/2014
-- Description:	Deletes the Broker data for a Generic Framework vendor.
-- =============================================
ALTER PROCEDURE [dbo].[GENERIC_FRAMEWORK_VENDOR_BROKER_Delete]
	-- Add the parameters for the stored procedure here
	@BrokerID uniqueidentifier,
	@ProviderID varchar(7)
AS
BEGIN
	DELETE FROM GENERIC_FRAMEWORK_VENDOR_BROKER_CONFIG
	WHERE BrokerID = @BrokerID AND ProviderID = @ProviderID
END
