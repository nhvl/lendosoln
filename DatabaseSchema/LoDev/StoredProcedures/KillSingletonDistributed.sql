CREATE PROCEDURE KillSingletonDistributed
	@ObjId varchar(100)
as
update singleton_tracker
set IsExisting = 0
where ObjId = @ObjId
