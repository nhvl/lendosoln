-- =============================================
-- Author:		Timothy Jewell
-- Create date: 10/31/2016
-- Description:	Retrieves all of the Originating
-- Company data for a given broker id.  Created 
-- for use with the SQL Transmission Service.
-- =============================================
CREATE PROCEDURE [dbo].[PmlBrokerRelationships_RetrieveAllByBrokerId]
	@BrokerId uniqueidentifier
AS
BEGIN
	SET NOCOUNT ON;

	SELECT PmlBrokerId, OCRoleT, EmployeeId, EmployeeName, EmployeeRoleT
	FROM PML_BROKER_RELATIONSHIPS
	WHERE BrokerId = @BrokerId
END
GO
