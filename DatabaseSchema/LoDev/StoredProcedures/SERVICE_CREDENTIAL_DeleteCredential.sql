ALTER  PROCEDURE [dbo].[SERVICE_CREDENTIAL_DeleteCredential]
	@Id int,
	@BrokerId uniqueidentifier  
AS
    BEGIN TRANSACTION  
      
    DELETE FROM BROKER_X_SERVICE_CREDENTIAL
    WHERE
    @Id = ServiceCredentialId
    AND @BrokerId = BrokerId
    
    IF @@error!= 0 GOTO HANDLE_ERROR
    
    DELETE FROM BRANCH_X_SERVICE_CREDENTIAL
    WHERE
    @Id = ServiceCredentialId
    AND @BrokerId = BrokerId
   
    IF( @@error != 0 ) GOTO HANDLE_ERROR

	DELETE FROM PMLBROKER_X_SERVICE_CREDENTIAL
	WHERE @Id = ServiceCredentialId AND @BrokerId = BrokerId;
	IF (@@error != 0) GOTO HANDLE_ERROR;

    DELETE FROM EMPLOYEE_X_SERVICE_CREDENTIAL
    WHERE
    @Id = ServiceCredentialId
    AND @BrokerId = BrokerId
   
    IF( @@error != 0 ) GOTO HANDLE_ERROR        
      
	DELETE FROM SERVICE_CREDENTIAL
	WHERE 
	@Id = Id
	AND @BrokerId = BrokerId
	
    IF @@error!= 0 GOTO HANDLE_ERROR  
     
    COMMIT TRANSACTION  
      
    RETURN;  
     
      HANDLE_ERROR:  
          ROLLBACK TRANSACTION  
         RAISERROR('Error in [SERVICE_CREDENTIAL_DeleteCredential] sp', 16, 1);;  
         RETURN;
