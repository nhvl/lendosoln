-- =============================================
-- Author:		Antonio Valencia
-- Create date: 1/2/2013 11:46 AM
-- Description:	Selects Investor Contacts. @Id is not h
-- =============================================
ALTER PROCEDURE [dbo].[INVESTOR_ROLODEX_GET]
	-- Add the parameters for the stored procedure here
	@BrokerId uniqueidentifier,
	@Id int  = null,
	@Status int = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	IF @Id IS NULL
	BEGIN
	SELECT InvestorRolodexId, BrokerId, SellerId, InvestorName,
		MERSOrganizationId, [Status], CompanyName, MainAttention,
		MainAddress, MainState, MainCity, MainZip, MainPhone, MainFax, MainContactName, 
		MainEmail, NoteShipToAttention, NoteShipToAddress, 
		NoteShipToState, NoteShipToCity, NoteShipToZip, NoteShipToPhone, NoteShipToFax,
		NoteShipToContactName, NoteShipToEmail, NoteShipToUseMainAdddress,
		PaymentToAttention, PaymentToAddress, PaymentToState, PaymentToCity, PaymentToZip, PaymentToPhone,
		PaymentToFax,  PaymentToContactName, PaymentToEmail, PaymentToUseMainAdddress,
		LossPayeeAttention, LossPayeeAddress, LossPayeeState, LossPayeeCity, LossPayeeZip,
		LossPayeePhone, LossPayeeFax, LossPayeeContactName, LossPayeeEmail, LossPayeeUseMainAdddress, HmdaPurchaser2004T, InvestorRolodexType, LpeInvestorId,
		HmdaPurchaser2015T
		
	FROM INVESTOR_ROLODEX
	WHERE BrokerId = @BrokerId AND @Status is null OR @Status = [Status]
	END
	
	ELSE BEGIN
	SELECT InvestorRolodexId, BrokerId, SellerId, InvestorName,
		MERSOrganizationId, [Status], CompanyName, MainAttention,
		MainAddress, MainState, MainCity, MainZip, MainPhone, MainFax, MainContactName, 
		MainEmail, NoteShipToAttention, NoteShipToAddress, 
		NoteShipToState, NoteShipToCity, NoteShipToZip, NoteShipToPhone, NoteShipToFax,
		NoteShipToContactName, NoteShipToEmail, NoteShipToUseMainAdddress,
		PaymentToAttention, PaymentToAddress, PaymentToState, PaymentToCity, PaymentToZip, PaymentToPhone,
		PaymentToFax,  PaymentToContactName, PaymentToEmail, PaymentToUseMainAdddress,
		LossPayeeAttention, LossPayeeAddress, LossPayeeState, LossPayeeCity, LossPayeeZip,
		LossPayeePhone, LossPayeeFax, LossPayeeContactName, LossPayeeEmail, LossPayeeUseMainAdddress, HmdaPurchaser2004T, InvestorRolodexType, LpeInvestorId,
		HmdaPurchaser2015T
		
	FROM INVESTOR_ROLODEX
	WHERE BrokerId = @BrokerId AND InvestorRolodexId = @Id
	END
		
END
