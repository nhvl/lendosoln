
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[List_InternalAdminAccounts]
	@ShowInactive AS BIT
AS
BEGIN
	SELECT a.UserId, a.LoginNm, b.FirstNm + ' ' + b.LastNm AS AdminNm, a.IsActive
	FROM All_User a JOIN Internal_User b ON a.UserId = b.UserId
	WHERE @ShowInactive=1 OR a.IsActive=1
	ORDER BY b.FirstNm + ' ' + b.LastNm
END

