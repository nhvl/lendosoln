
-- =============================================
-- Author:		Justin Jia
-- ALTER date: June 24, 2012
-- Description:	Update loan_file_a~f for loan-overwrite-tool, which don't touch any foreign key
-- =============================================

CREATE PROCEDURE [dbo].[Justin_Testing_UpdateLoanFileA] 

@sLId uniqueidentifier,
@IsValid bit = 0,
@IsTemplate bit = 0,
@DeletedD datetime = '2011-01-01 00:00:00',
@sLpTemplateNm varchar(100) = '',
@sCcTemplateNm varchar(36) = '',
@sLeadD smalldatetime = NULL,
@sLeadN varchar(21) = '',
@sPreApprovD smalldatetime = NULL,
@sPreApprovN varchar(21) = '',
@sEstCloseN varchar(21) = '',
@sOpenedD smalldatetime = '2011-01-01 00:00:00',
@sOpenedN text = '',
@sCanceledD smalldatetime = NULL,
@sCanceledN text = '',
@sSuspendedD smalldatetime = NULL,
@sSuspendedN text = '',
@sClosedN text = '',
@sU1LStatDesc varchar(21) = '',
@sU1LStatD smalldatetime = NULL,
@sU1LStatN text = '',
@sU2LStatDesc varchar(21) = '',
@sU2LStatD smalldatetime = NULL,
@sU2LStatN text = '',
@sU3LStatDesc varchar(21) = '',
@sU3LStatD smalldatetime = NULL,
@sU3LStatN text = '',
@sU4LStatDesc varchar(21) = '',
@sU4LStatD smalldatetime = NULL,
@sU4LStatN text = '',
@sPrelimRprtOd smalldatetime = NULL,
@sPrelimRprtRd smalldatetime = NULL,
@sPrelimRprtN varchar(36) = '',
@sApprRprtOd smalldatetime = NULL,
@sApprRprtRd smalldatetime = NULL,
@sApprRprtN varchar(36) = '',
@sTilGfeOd smalldatetime = NULL,
@sTilGfeRd smalldatetime = NULL,
@sTilGfeN varchar(36) = '',
@sU1DocStatDesc varchar(21) = '',
@sU1DocStatOd smalldatetime = NULL,
@sU1DocStatRd smalldatetime = NULL,
@sU1DocStatN varchar(36) = '',
@sU2DocStatDesc varchar(21) = '',
@sU2DocStatOd smalldatetime = NULL,
@sU2DocStatRd smalldatetime = NULL,
@sU2DocStatN varchar(36) = '',
@sU3DocStatDesc varchar(21) = '',
@sU3DocStatOd smalldatetime = NULL,
@sU3DocStatRd smalldatetime = NULL,
@sU3DocStatN varchar(36) = '',
@sU4DocStatDesc varchar(21) = '',
@sU4DocStatOd smalldatetime = NULL,
@sU4DocStatRd smalldatetime = NULL,
@sU4DocStatN varchar(36) = '',
@sU5DocStatDesc varchar(21) = '',
@sU5DocStatOd smalldatetime = NULL,
@sU5DocStatRd smalldatetime = NULL,
@sU5DocStatN varchar(36) = '',
@sRLckdD smalldatetime = NULL,
@sRLckdN varchar(36) = '',
@sRLckdExpiredD smalldatetime = NULL,
@sRLckdExpiredN varchar(36) = '',
@sPreQualD smalldatetime = NULL,
@sPreQualN varchar(36) = '',
@sSubmitD smalldatetime = NULL,
@sSubmitN text = '',
@sApprovD smalldatetime = NULL,
@sApprovN text = '',
@sDocsD smalldatetime = NULL,
@sDocsN text = '',
@sFundD smalldatetime = NULL,
@sFundN text = '',
@sOnHoldD smalldatetime = NULL,
@sOnHoldN varchar(36) = '',
@sRejectD smalldatetime = NULL,
@sRejectN text = '',
@sClosedD smalldatetime = NULL,
@sProcFPaid bit = 0,
@sCrFPaid bit = 0,
@sApprFPaid bit = 0,
@sAttorneyF money = 0,
@sDocPrepF money = 0,
@sDocPrepFProps int = 0,
@sRemain1stMBal money = 0,
@sSubFinIR decimal(9, 3) = 0,
@sSubFinTerm int = 0,
@sRemain1stMPmt money = 0,
@sSubFinMb money = 0,
@sRAdjStat bit = 0,
@sFinMethT int = 0,
@sYrBuilt char(4) = '',
@sInsReqDesc varchar(21) = '',
@sReqCreditLifeIns bit = 0,
@sReqCreditDisabilityIns bit = 0,
@sReqPropIns bit = 0,
@sReqFloodIns bit = 0,
@sIfPurchInsFrCreditor bit = 0,
@sIfPurchPropInsFrCreditor bit = 0,
@sIfPurchFloodInsFrCreditor bit = 0,
@sInsFrCreditorAmt money = 0,
@sMldsPpmtMonMax int = 0,
@sRAdjWorstIndex bit = 1,
@sAgentXmlContent text = '',
@sDwnPmtSrcExplain varchar(36) = '',
@sFannieDocT int = 0,
@sIsBalloon bit = 0,
@sArmIndexT int = 0,
@sIsSellerProvidedBelowMktFin char(1) = '',
@sFannieSpT int = 0,
@sUnderwritingD smalldatetime = NULL,
@sWillEscrowBeWaived bit = 0,
@sLpTemplateId uniqueidentifier = '00000000-0000-0000-0000-000000000000',
@sCcTemplateId uniqueidentifier = '00000000-0000-0000-0000-000000000000',
@sProdIT int = 0,
@sProdAssetT int = 0,
@sProdPpmtPenaltyMon int = 0,
@sProdCashoutAmt money = 0,
@sProdImpound bit = 1,
@sProdSpT int = 0,
@sProdSpUnits int = 1,
@sProdCondoStories int = 1,
@sProdGiftPc decimal(9, 3) = 0,
@sProd4506Sign bit = 1,
@sProdAmortFixed bit = 1,
@sProdAmortArm bit = 1,
@sProdAmortGpm bit = 1,
@sProdAmortIOnly bit = 1,
@sProHazInsMb money = 0,
@sProMInsMb money = 0,
@sConsumerCanEditStartTime datetime = NULL,
@sConsumerCanEditEndTime datetime = NULL,
@sConsumerCanEditStatuses int = 0,
@sIsLicensedLender bit = 0,
@sIsCorrespondentLender bit = 0,
@sIsOtherTypeLender bit = 0,
@sOtherTypeLenderDesc varchar(50) = '',
@sRefundabilityOfFeeToLenderT int = 0,
@sCommitmentEstimateDays varchar(20) = '',
@sRecordedD smalldatetime = NULL,
@sRecordedN text = '',
@sRecFDesc varchar(100) = '',
@sCountyRtcDesc varchar(100) = '',
@sStateRtcDesc varchar(100) = '',
@sIntegrationT int = 0,
@sCondXmlContent uniqueidentifier = '00000000-0000-0000-0000-000000000000',
@sFinMethDesc varchar(50) = '',
@sSpImprovDesc varchar(60) = '',
@sTitleInsFTable varchar(200) = '',
@sProjNm varchar(72) = '',
@sU1TcDesc text = '',
@sU2TcDesc text = '',
@sU3TcDesc text = '',
@sU4TcDesc text = '',
@sU1ScDesc text = '',
@sU2ScDesc text = '',
@sU3ScDesc text = '',
@sU4ScDesc text = '',
@sU5ScDesc text = '',
@sIsSection32 bit = 0,
@sIsSelfEmployed bit = 0,
@sLAmtLckd bit = 0,
@sSpAddr varchar(60) = '',
@sStatusLckd bit = 0,
@sRefPdOffAmtGfe money = 0,
@sRefPdOffAmtGfeLckd bit = 0,
@sFannieARMPlanNum varchar(36) = '',
@sUseObsoleteGfeForm bit = 0,
@sLeadCanceledD smalldatetime = NULL,
@sLeadDeclinedD smalldatetime = NULL,
@sLeadOtherD smalldatetime = NULL,
@sLeadCanceledN smalldatetime = NULL,
@sLeadDeclinedN smalldatetime = NULL,
@sHcltvR decimal(9, 3) = 0,
@sHcltvRLckd bit = 0,
@sDebtToHousingGapR decimal(9, 3) = 0,
@sDebtToHousingGapRLckd bit = 0,
@sIsSpReviewNoAppr bit = 0,
@sSpReviewFormNum varchar(25) = '',
@sTransmFntc money = 0,
@sTransmFntcLckd bit = 0,
@sVerifAssetAmt money = 0,
@sFntcSrc varchar(40) = '',
@sRsrvMonNumDesc varchar(10) = '',
@sInterestedPartyContribR decimal(9, 3) = 0,
@sIsManualUw bit = 0,
@sIsDuUw bit = 0,
@sIsLpUw bit = 0,
@sIsOtherUw bit = 0,
@sOtherUwDesc varchar(40) = '',
@sAusRecommendation varchar(60) = '',
@sLpAusKey varchar(30) = '',
@sDuCaseId varchar(30) = '',
@sLpDocClass varchar(40) = '',
@sRepCrScore varchar(30) = '',
@sIsCommLen bit = 0,
@sIsHOwnershipEdCertInFile bit = 0,
@sIsMOrigBroker bit = 0,
@sIsMOrigCorrespondent bit = 0,
@sTransmBuydwnTermDesc varchar(30) = '',
@sCreatedD smalldatetime = '2011-01-01 00:00:00',
@sVaBuildingStatusT int = 0,
@sVaBuildingStatusTLckd bit = 0,
@sVaBuildingT int = 0,
@sVaIsFactoryFabricatedTri tinyint = 0,
@sVaNumOfBuildings varchar(36) = '',
@sVaNumOfLivingUnits varchar(36) = '',
@sVaStreetAccessPrivateTri tinyint = 0,
@sVaStreetMaintenancePrivateTri tinyint = 0,
@sVaConstructWarrantyTri tinyint = 0,
@sVaConstructWarrantyProgramNm varchar(100) = '',
@sVaConstructExpiredD smalldatetime = NULL,
@sVaConstructCompleteD smalldatetime = NULL,
@sVaOwnerNm varchar(100) = '',
@sVaSpOccT int = 0,
@sVaSpRentalMonthly money = 0,
@sVaSpOccupantNm varchar(100) = '',
@sVaSpOccupantPhone varchar(50) = '',
@sVaSpAvailableForInspectDesc varchar(60) = '',
@sVaSpAvailableForInspectTimeT int = 0,
@sVaConstructComplianceInspectionMadeByT int = 0,
@sVaConstructPlansFirstSubmitTri tinyint = 0,
@sVaConstructPrevPlansCaseNum varchar(50) = '',
@sVaSpecialAssessmentsComments text = '',
@sSpMineralRightsReservedTri tinyint = 0,
@sSpMineralRightsReservedExplain varchar(100) = '',
@sSpLeaseIs99Yrs bit = 0,
@sSpLeaseIsRenewable bit = 0,
@sSpLeaseAnnualGroundRent money = 0,
@sLotPurchaseSeparatelyTri tinyint = 0,
@sVaRefiAmt money = 0,
@sVaSaleContractAttachedTri tinyint = 0,
@sVaPrevApprovedContractNum varchar(50) = '',
@sLAmtCalcPelckd bit = 0,
@sLAmtCalcPeval money = 0,
@sProOFinBalPelckd bit = 0,
@sProOFinBalPeval money = 0,
@sHouseValPelckd bit = 0,
@sHouseValPeval money = 0,
@sLPurposeTPelckd bit = 0,
@sLPurposeTPeval int = 0,
@sLTotIPelckd bit = 0,
@sLTotIPeval money = 0,
@sSpStatePelckd bit = 0,
@sSpStatePeval char(2) = '',
@sProMInsLckd bit = 0,
@sProMIns money = 0,
@sFannieProdDesc varchar(30) = '',
@sClearToCloseD smalldatetime = NULL,
@sClearToCloseN varchar(36) = '',
@sFileVersion int = 0,
@sProcessingD smalldatetime = NULL,
@sProcessingN varchar(36) = '',
@sFinalUnderwritingD smalldatetime = NULL,
@sFinalUnderwritingN varchar(36) = '',
@sDocsBackD smalldatetime = NULL,
@sDocsBackN varchar(36) = '',
@sFundingConditionsD smalldatetime = NULL,
@sFundingConditionsN varchar(36) = '',
@sFinalDocsD smalldatetime = NULL,
@sFinalDocsN varchar(36) = '',
@sLPurchasedN varchar(36) = '',
@sAppSubmittedD smalldatetime = NULL,
@sAuditTrackedFieldChanges bit = 0
	
AS
	
	UPDATE loan_file_a SET 
		IsValid = @IsValid,
		IsTemplate = @IsTemplate,
		DeletedD = @DeletedD,
		sLpTemplateNm = @sLpTemplateNm,
		sCcTemplateNm = @sCcTemplateNm,
		sLeadD = @sLeadD,
		sLeadN = @sLeadN,
		sPreApprovD = @sPreApprovD,
		sPreApprovN = @sPreApprovN,
		sEstCloseN = @sEstCloseN,
		sOpenedD = @sOpenedD,
		sOpenedN = @sOpenedN,
		sCanceledD = @sCanceledD,
		sCanceledN = @sCanceledN,
		sSuspendedD = @sSuspendedD,
		sSuspendedN = @sSuspendedN,
		sClosedN = @sClosedN,
		sU1LStatDesc = @sU1LStatDesc,
		sU1LStatD = @sU1LStatD,
		sU1LStatN = @sU1LStatN,
		sU2LStatDesc = @sU2LStatDesc,
		sU2LStatD = @sU2LStatD,
		sU2LStatN = @sU2LStatN,
		sU3LStatDesc = @sU3LStatDesc,
		sU3LStatD = @sU3LStatD,
		sU3LStatN = @sU3LStatN,
		sU4LStatDesc = @sU4LStatDesc,
		sU4LStatD = @sU4LStatD,
		sU4LStatN = @sU4LStatN,
		sPrelimRprtOd = @sPrelimRprtOd,
		sPrelimRprtRd = @sPrelimRprtRd,
		sPrelimRprtN = @sPrelimRprtN,
		sApprRprtOd = @sApprRprtOd,
		sApprRprtRd = @sApprRprtRd,
		sApprRprtN = @sApprRprtN,
		sTilGfeOd = @sTilGfeOd,
		sTilGfeRd = @sTilGfeRd,
		sTilGfeN = @sTilGfeN,
		sU1DocStatDesc = @sU1DocStatDesc,
		sU1DocStatOd = @sU1DocStatOd,
		sU1DocStatRd = @sU1DocStatRd,
		sU1DocStatN = @sU1DocStatN,
		sU2DocStatDesc = @sU2DocStatDesc,
		sU2DocStatOd = @sU2DocStatOd,
		sU2DocStatRd = @sU2DocStatRd,
		sU2DocStatN = @sU2DocStatN,
		sU3DocStatDesc = @sU3DocStatDesc,
		sU3DocStatOd = @sU3DocStatOd,
		sU3DocStatRd = @sU3DocStatRd,
		sU3DocStatN = @sU3DocStatN,
		sU4DocStatDesc = @sU4DocStatDesc,
		sU4DocStatOd = @sU4DocStatOd,
		sU4DocStatRd = @sU4DocStatRd,
		sU4DocStatN = @sU4DocStatN,
		sU5DocStatDesc = @sU5DocStatDesc,
		sU5DocStatOd = @sU5DocStatOd,
		sU5DocStatRd = @sU5DocStatRd,
		sU5DocStatN = @sU5DocStatN,
		sRLckdD = @sRLckdD,
		sRLckdN = @sRLckdN,
		sRLckdExpiredD = @sRLckdExpiredD,
		sRLckdExpiredN = @sRLckdExpiredN,
		sPreQualD = @sPreQualD,
		sPreQualN = @sPreQualN,
		sSubmitD = @sSubmitD,
		sSubmitN = @sSubmitN,
		sApprovD = @sApprovD,
		sApprovN = @sApprovN,
		sDocsD = @sDocsD,
		sDocsN = @sDocsN,
		sFundD = @sFundD,
		sFundN = @sFundN,
		sOnHoldD = @sOnHoldD,
		sOnHoldN = @sOnHoldN,
		sRejectD = @sRejectD,
		sRejectN = @sRejectN,
		sClosedD = @sClosedD,
		sProcFPaid = @sProcFPaid,
		sCrFPaid = @sCrFPaid,
		sApprFPaid = @sApprFPaid,
		sAttorneyF = @sAttorneyF,
		sDocPrepF = @sDocPrepF,
		sDocPrepFProps = @sDocPrepFProps,
		sRemain1stMBal = @sRemain1stMBal,
		sSubFinIR = @sSubFinIR,
		sSubFinTerm = @sSubFinTerm,
		sRemain1stMPmt = @sRemain1stMPmt,
		sSubFinMb = @sSubFinMb,
		sRAdjStat = @sRAdjStat,
		sFinMethT = @sFinMethT,
		sYrBuilt = @sYrBuilt,
		sInsReqDesc = @sInsReqDesc,
		sReqCreditLifeIns = @sReqCreditLifeIns,
		sReqCreditDisabilityIns = @sReqCreditDisabilityIns,
		sReqPropIns = @sReqPropIns,
		sReqFloodIns = @sReqFloodIns,
		sIfPurchInsFrCreditor = @sIfPurchInsFrCreditor,
		sIfPurchPropInsFrCreditor = @sIfPurchPropInsFrCreditor,
		sIfPurchFloodInsFrCreditor = @sIfPurchFloodInsFrCreditor,
		sInsFrCreditorAmt = @sInsFrCreditorAmt,
		sMldsPpmtMonMax = @sMldsPpmtMonMax,
		sRAdjWorstIndex = @sRAdjWorstIndex,
		--sAgentXmlContent = @sAgentXmlContent,
		sDwnPmtSrcExplain = @sDwnPmtSrcExplain,
		sFannieDocT = @sFannieDocT,
		sIsBalloon = @sIsBalloon,
		sArmIndexT = @sArmIndexT,
		sIsSellerProvidedBelowMktFin = @sIsSellerProvidedBelowMktFin,
		sFannieSpT = @sFannieSpT,
		sUnderwritingD = @sUnderwritingD,
		sWillEscrowBeWaived = @sWillEscrowBeWaived,
		sLpTemplateId = @sLpTemplateId,
		sCcTemplateId = @sCcTemplateId,
		sProdIT = @sProdIT,
		sProdAssetT = @sProdAssetT,
		sProdPpmtPenaltyMon = @sProdPpmtPenaltyMon,
		sProdCashoutAmt = @sProdCashoutAmt,
		sProdImpound = @sProdImpound,
		sProdSpT = @sProdSpT,
		sProdSpUnits = @sProdSpUnits,
		sProdCondoStories = @sProdCondoStories,
		sProdGiftPc = @sProdGiftPc,
		sProd4506Sign = @sProd4506Sign,
		sProdAmortFixed = @sProdAmortFixed,
		sProdAmortArm = @sProdAmortArm,
		sProdAmortGpm = @sProdAmortGpm,
		sProdAmortIOnly = @sProdAmortIOnly,
		sProHazInsMb = @sProHazInsMb,
		sProMInsMb = @sProMInsMb,
		sConsumerCanEditStartTime = @sConsumerCanEditStartTime,
		sConsumerCanEditEndTime = @sConsumerCanEditEndTime,
		sConsumerCanEditStatuses = @sConsumerCanEditStatuses,
		sIsLicensedLender = @sIsLicensedLender,
		sIsCorrespondentLender = @sIsCorrespondentLender,
		sIsOtherTypeLender = @sIsOtherTypeLender,
		sOtherTypeLenderDesc = @sOtherTypeLenderDesc,
		sRefundabilityOfFeeToLenderT = @sRefundabilityOfFeeToLenderT,
		sCommitmentEstimateDays = @sCommitmentEstimateDays,
		sRecordedD = @sRecordedD,
		sRecordedN = @sRecordedN,
		sRecFDesc = @sRecFDesc,
		sCountyRtcDesc = @sCountyRtcDesc,
		sStateRtcDesc = @sStateRtcDesc,
		sIntegrationT = @sIntegrationT,
		sCondXmlContent = @sCondXmlContent,
		sFinMethDesc = @sFinMethDesc,
		sSpImprovDesc = @sSpImprovDesc,
		sTitleInsFTable = @sTitleInsFTable,
		sProjNm = @sProjNm,
		sU1TcDesc = @sU1TcDesc,
		sU2TcDesc = @sU2TcDesc,
		sU3TcDesc = @sU3TcDesc,
		sU4TcDesc = @sU4TcDesc,
		sU1ScDesc = @sU1ScDesc,
		sU2ScDesc = @sU2ScDesc,
		sU3ScDesc = @sU3ScDesc,
		sU4ScDesc = @sU4ScDesc,
		sU5ScDesc = @sU5ScDesc,
		sIsSection32 = @sIsSection32,
		sIsSelfEmployed = @sIsSelfEmployed,
		sLAmtLckd = @sLAmtLckd,
		sSpAddr = @sSpAddr,
		sStatusLckd = @sStatusLckd,
		sRefPdOffAmtGfe = @sRefPdOffAmtGfe,
		sRefPdOffAmtGfeLckd = @sRefPdOffAmtGfeLckd,
		sFannieARMPlanNum = @sFannieARMPlanNum,
		sUseObsoleteGfeForm = @sUseObsoleteGfeForm,
		sLeadCanceledD = @sLeadCanceledD,
		sLeadDeclinedD = @sLeadDeclinedD,
		sLeadOtherD = @sLeadOtherD,
		sLeadCanceledN = @sLeadCanceledN,
		sLeadDeclinedN = @sLeadDeclinedN,
		sHcltvR = @sHcltvR,
		sHcltvRLckd = @sHcltvRLckd,
		sDebtToHousingGapR = @sDebtToHousingGapR,
		sDebtToHousingGapRLckd = @sDebtToHousingGapRLckd,
		sIsSpReviewNoAppr = @sIsSpReviewNoAppr,
		sSpReviewFormNum = @sSpReviewFormNum,
		sTransmFntc = @sTransmFntc,
		sTransmFntcLckd = @sTransmFntcLckd,
		sVerifAssetAmt = @sVerifAssetAmt,
		sFntcSrc = @sFntcSrc,
		sRsrvMonNumDesc = @sRsrvMonNumDesc,
		sInterestedPartyContribR = @sInterestedPartyContribR,
		sIsManualUw = @sIsManualUw,
		sIsDuUw = @sIsDuUw,
		sIsLpUw = @sIsLpUw,
		sIsOtherUw = @sIsOtherUw,
		sOtherUwDesc = @sOtherUwDesc,
		sAusRecommendation = @sAusRecommendation,
		sLpAusKey = @sLpAusKey,
		sDuCaseId = @sDuCaseId,
		sLpDocClass = @sLpDocClass,
		sRepCrScore = @sRepCrScore,
		sIsCommLen = @sIsCommLen,
		sIsHOwnershipEdCertInFile = @sIsHOwnershipEdCertInFile,
		sIsMOrigBroker = @sIsMOrigBroker,
		sIsMOrigCorrespondent = @sIsMOrigCorrespondent,
		sTransmBuydwnTermDesc = @sTransmBuydwnTermDesc,
		sCreatedD = @sCreatedD,
		sVaBuildingStatusT = @sVaBuildingStatusT,
		sVaBuildingStatusTLckd = @sVaBuildingStatusTLckd,
		sVaBuildingT = @sVaBuildingT,
		sVaIsFactoryFabricatedTri = @sVaIsFactoryFabricatedTri,
		sVaNumOfBuildings = @sVaNumOfBuildings,
		sVaNumOfLivingUnits = @sVaNumOfLivingUnits,
		sVaStreetAccessPrivateTri = @sVaStreetAccessPrivateTri,
		sVaStreetMaintenancePrivateTri = @sVaStreetMaintenancePrivateTri,
		sVaConstructWarrantyTri = @sVaConstructWarrantyTri,
		sVaConstructWarrantyProgramNm = @sVaConstructWarrantyProgramNm,
		sVaConstructExpiredD = @sVaConstructExpiredD,
		sVaConstructCompleteD = @sVaConstructCompleteD,
		sVaOwnerNm = @sVaOwnerNm,
		sVaSpOccT = @sVaSpOccT,
		sVaSpRentalMonthly = @sVaSpRentalMonthly,
		sVaSpOccupantNm = @sVaSpOccupantNm,
		sVaSpOccupantPhone = @sVaSpOccupantPhone,
		sVaSpAvailableForInspectDesc = @sVaSpAvailableForInspectDesc,
		sVaSpAvailableForInspectTimeT = @sVaSpAvailableForInspectTimeT,
		sVaConstructComplianceInspectionMadeByT = @sVaConstructComplianceInspectionMadeByT,
		sVaConstructPlansFirstSubmitTri = @sVaConstructPlansFirstSubmitTri,
		sVaConstructPrevPlansCaseNum = @sVaConstructPrevPlansCaseNum,
		sVaSpecialAssessmentsComments = @sVaSpecialAssessmentsComments,
		sSpMineralRightsReservedTri = @sSpMineralRightsReservedTri,
		sSpMineralRightsReservedExplain = @sSpMineralRightsReservedExplain,
		sSpLeaseIs99Yrs = @sSpLeaseIs99Yrs,
		sSpLeaseIsRenewable = @sSpLeaseIsRenewable,
		sSpLeaseAnnualGroundRent = @sSpLeaseAnnualGroundRent,
		sLotPurchaseSeparatelyTri = @sLotPurchaseSeparatelyTri,
		sVaRefiAmt = @sVaRefiAmt,
		sVaSaleContractAttachedTri = @sVaSaleContractAttachedTri,
		sVaPrevApprovedContractNum = @sVaPrevApprovedContractNum,
		sLAmtCalcPelckd = @sLAmtCalcPelckd,
		sLAmtCalcPeval = @sLAmtCalcPeval,
		sProOFinBalPelckd = @sProOFinBalPelckd,
		sProOFinBalPeval = @sProOFinBalPeval,
		sHouseValPelckd = @sHouseValPelckd,
		sHouseValPeval = @sHouseValPeval,
		sLPurposeTPelckd = @sLPurposeTPelckd,
		sLPurposeTPeval = @sLPurposeTPeval,
		sLTotIPelckd = @sLTotIPelckd,
		sLTotIPeval = @sLTotIPeval,
		sSpStatePelckd = @sSpStatePelckd,
		sSpStatePeval = @sSpStatePeval,
		sProMInsLckd = @sProMInsLckd,
		sProMIns = @sProMIns,
		sFannieProdDesc = @sFannieProdDesc,
		sClearToCloseD = @sClearToCloseD,
		sClearToCloseN = @sClearToCloseN,
		sFileVersion = @sFileVersion,
		sProcessingD = @sProcessingD,
		sProcessingN = @sProcessingN,
		sFinalUnderwritingD = @sFinalUnderwritingD,
		sFinalUnderwritingN = @sFinalUnderwritingN,
		sDocsBackD = @sDocsBackD,
		sDocsBackN = @sDocsBackN,
		sFundingConditionsD = @sFundingConditionsD,
		sFundingConditionsN = @sFundingConditionsN,
		sFinalDocsD = @sFinalDocsD,
		sFinalDocsN = @sFinalDocsN,
		sLPurchasedN = @sLPurchasedN,
		sAppSubmittedD = @sAppSubmittedD,
		sAuditTrackedFieldChanges = @sAuditTrackedFieldChanges
	WHERE sLId = @sLId	
	
	if( 0!=@@error)
	begin
		RAISERROR('Error in updating Branch table in UpdateLoan sp', 16, 1);
		return -100;
	end
	return 0;
