-- =============================================
-- Author:		Michael Leinweaver
-- Create date: 5/23/2018
-- Description:	Saves an originating company user pipeline setting.
-- =============================================
CREATE PROCEDURE [dbo].[LENDER_ORIGINATING_COMPANY_USER_PIPELINE_SETTINGS_Save] 
	@DisplayOrder int,
	@SettingId uniqueidentifier,
	@BrokerId uniqueidentifier,
	@QueryId uniqueidentifier,
	@PortalDisplayName varchar(100),
	@OverridePipelineReportName bit,
	@AvailableForBrokerPortalMode bit,
	@AvailableForMiniCorrespondentPortalMode bit,
	@AvailableForCorrespondentPortalMode bit
AS
BEGIN
	UPDATE LENDER_ORIGINATING_COMPANY_USER_PIPELINE_SETTINGS
	SET
		DisplayOrder = @DisplayOrder,
		PortalDisplayName = @PortalDisplayName,
		OverridePipelineReportName = @OverridePipelineReportName,
		AvailableForBrokerPortalMode = @AvailableForBrokerPortalMode,
		AvailableForMiniCorrespondentPortalMode = @AvailableForMiniCorrespondentPortalMode,
		AvailableForCorrespondentPortalMode = @AvailableForCorrespondentPortalMode
	WHERE BrokerId = @BrokerId AND SettingId = @SettingId
	
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO LENDER_ORIGINATING_COMPANY_USER_PIPELINE_SETTINGS(
			DisplayOrder, 
			SettingId, 
			BrokerId, 
			QueryId, 
			PortalDisplayName, 
			OverridePipelineReportName, 
			AvailableForBrokerPortalMode, 
			AvailableForMiniCorrespondentPortalMode, 
			AvailableForCorrespondentPortalMode)
		VALUES(
			@DisplayOrder,
			@SettingId,
			@BrokerId,
			@QueryId,
			@PortalDisplayName,
			@OverridePipelineReportName,
			@AvailableForBrokerPortalMode,
			@AvailableForMiniCorrespondentPortalMode,
			@AvailableForCorrespondentPortalMode
		)
	END
END