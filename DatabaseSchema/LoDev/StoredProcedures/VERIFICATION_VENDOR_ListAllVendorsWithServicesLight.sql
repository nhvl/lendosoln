-- =============================================
-- Author:		Eric Mallare
-- Create date: 5/5/2017
-- Description:	Gets basic vendor info along with their services.
-- =============================================
CREATE PROCEDURE [dbo].[VERIFICATION_VENDOR_ListAllVendorsWithServicesLight]
AS
BEGIN
	DECLARE @VendorData TABLE(
		VendorId int, 
		CompanyName varchar(150), 
		IsEnabled bit, 
		IsTestVendor bit,
		CanPayWithCreditCard bit,
		PlatformId int,
		RequiresAccountId bit,
		UsesAccountId bit
	);

	INSERT INTO @VendorData
	SELECT
		v.VendorId, v.CompanyName, v.IsEnabled, v.IsTestVendor, v.CanPayWithCreditCard, v.PlatformId, p.RequiresAccountId, p.UsesAccountId
	FROM
		VERIFICATION_VENDOR v LEFT JOIN
		VERIFICATION_PLATFORM p on v.PlatformId=p.PlatformId

	SELECT *
	FROM @VendorData
		
	SELECT 
		voa.VendorServiceId, voa.IsADirectVendor, voa.SellsViaResellers, voa.IsAReseller, voa.VendorId, voa.IsEnabled, voa.ServiceT
	FROM
		@VendorData v JOIN
		VOA_SERVICE voa ON v.VendorId=voa.VendorId

	SELECT 
		voe.VendorServiceId, voe.IsADirectVendor, voe.SellsViaResellers, voe.IsAReseller, voe.VendorId, voe.IsEnabled, voe.ServiceT
	FROM
		@VendorData v JOIN
		VOE_SERVICE voe ON v.VendorId=voe.VendorId

	SELECT 
		ssa89.VendorServiceId, ssa89.VendorId, 2 AS ServiceT, ssa89.IsEnabled, ssa89.IsADirectVendor, ssa89.SellsViaResellers, ssa89.IsAReseller
	FROM
		@VendorData v
		JOIN SSA89_SERVICE ssa89 ON v.VendorId = ssa89.VendorId
END
