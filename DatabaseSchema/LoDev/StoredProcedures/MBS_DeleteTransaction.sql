-- =============================================
-- Author:		Matthew Pham
-- Create date: 9/1/11
-- Description:	Delete a transaction for MBS trades
-- =============================================
CREATE PROCEDURE [dbo].[MBS_DeleteTransaction]
	-- Add the parameters for the stored procedure here
	@TransactionId uniqueidentifier,
	@LinkedTransactionId uniqueidentifier = null,
	@ForceScrubAllLinked bit = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    IF (@LinkedTransactionId IS NOT NULL) BEGIN
		UPDATE MBS_TRANSACTION
		SET
			LinkedTransaction = null
		WHERE TransactionId = @LinkedTransactionId
    END
    
    --Note that this can be slow since there's no index; don't use it unless something goes wrong.
    IF (@ForceScrubAllLinked = 1) BEGIN
		UPDATE MBS_TRANSACTION
		SET
			LinkedTransaction = null
		WHERE LinkedTransaction = @TransactionId
	END
    
	DELETE FROM MBS_TRANSACTION
	WHERE TransactionId = @TransactionId
END
