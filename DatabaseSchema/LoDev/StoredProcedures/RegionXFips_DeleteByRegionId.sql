CREATE PROCEDURE [dbo].[RegionXFips_DeleteByRegionId]
	@BrokerId uniqueidentifier,
	@RegionId int
AS
BEGIN
	DELETE FROM REGION_X_FIPS
	WHERE BrokerId = @BrokerId
		AND RegionId = @RegionId
END