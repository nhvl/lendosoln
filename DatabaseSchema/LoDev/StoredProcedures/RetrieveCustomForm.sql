
CREATE PROCEDURE [dbo].[RetrieveCustomForm] 
	@CustomLetterID uniqueidentifier,
	@BrokerID uniqueidentifier
AS
	SELECT Title, FileDBKey
	FROM Custom_Letter
	WHERE CustomLetterID = @CustomLetterID AND BrokerId = @BrokerID

