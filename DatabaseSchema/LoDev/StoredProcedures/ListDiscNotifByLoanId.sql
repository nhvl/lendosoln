
CREATE PROCEDURE [dbo].[ListDiscNotifByLoanId]
	@UserID uniqueidentifier,
	@LoanId uniqueidentifier
AS
	SELECT NotifId, NotifIsRead, DiscCreatorLoginNm, DiscStatus, DiscStatusDate, DiscPriority, DiscSubject, DiscDueDate, NotifAlertDate, DiscRefObjNm1, DiscRefObjNm2, ISNULL(NotifAlertDate, convert(smalldatetime, '2079-01-01')) AS SortAlertDate,
		 DiscPriority, dbo.GetReminderPriorityDescription(DiscPriority) AS PriorityDescription, DiscRefObjId, DiscCreatedDate
             FROM discussion_notification notif inner join discussion_log conv on notif.DiscLogid = conv.DiscLogId
             WHERE NotifIsValid = 1 and DiscIsRefObjValid = 1 and DiscRefObjId = @LoanId
             ORDER BY SortAlertDate ASC, DiscCreatedDate DESC

