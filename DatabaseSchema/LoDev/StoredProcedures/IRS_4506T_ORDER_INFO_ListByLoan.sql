-- =============================================
-- Author:		David Dao
-- Create date: Aug 7, 2013
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[IRS_4506T_ORDER_INFO_ListByLoan]
    @sLId UniqueIdentifier
    
AS
BEGIN

    SELECT Order_Info.TransactionId, 
           Order_Info.sLId, 
           Order_Info.VendorId, 
           Order_Info.aAppId, 
           Order_Info.ApplicationName,
           Order_Info.OrderNumber, 
           Order_Info.OrderDate, 
           Order_Info.Status, 
           Order_Info.StatusT, 
           Order_Info.StatusDate,
           Order_Info.UploadedFilesXmlContent,
           Cache.sBrokerId
    FROM IRS_4506T_ORDER_INFO order_info JOIN LOAN_FILE_CACHE cache ON order_info.slid=cache.slid
    WHERE order_info.sLId = @sLId
    ORDER BY OrderDate

END
