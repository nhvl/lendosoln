-- =============================================
-- Author:		Thien Nguyen
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[LOAN_AUDIT_TRAIL_Insert] 
	@BrokerId Guid,
	@LoanId Guid,
    @EventDate DateTime,
    @Category int,
    @ClassType varchar(1000),
    @EventDescription varchar(1000),
    @UserName varchar(100),
    @HasDetails bit,
    @ReadPermissionsRequiredJsonContent varchar(max),
    @DetailContentKey varchar(100)   
    
AS
BEGIN
    INSERT INTO LOAN_AUDIT_TRAIL
               (
                    BrokerId
                   ,LoanId
                   ,EventDate
                   ,Category
                   ,ClassType
                   ,EventDescription
                   ,UserName
                   ,HasDetails
                   ,ReadPermissionsRequiredJsonContent
                   ,DetailContentKey
               )
         VALUES
               (
                    @BrokerId
                   ,@LoanId
                   ,@EventDate
                   ,@Category
                   ,@ClassType
                   ,@EventDescription
                   ,@UserName
                   ,@HasDetails
                   ,@ReadPermissionsRequiredJsonContent
                   ,@DetailContentKey
               )
END
