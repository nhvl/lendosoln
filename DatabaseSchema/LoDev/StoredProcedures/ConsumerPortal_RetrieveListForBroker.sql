-- =============================================
-- Author:		paoloa
-- Create date: 5/30/2013
-- Description:	
-- =============================================
CREATE PROCEDURE dbo.ConsumerPortal_RetrieveListForBroker
	-- Add the parameters for the stored procedure here
	@BrokerId uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT Id, Name, ReferralUrl
	FROM CONSUMER_PORTAL
	WHERE BrokerId = @BrokerId AND IsEnabled = 1
END
