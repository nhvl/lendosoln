CREATE PROCEDURE [dbo].[INTERNAL_USE_ONLY_DeleteConsumerActionRequestWithStandardDocTypes]
as

delete from cp_consumer_action_request

where
doctypeid in
(
	select doctypeid from edocs_document_type where brokerid is null
)


