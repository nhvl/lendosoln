-- =============================================
-- Author:		Brian Beery
-- Create date: 02/22/13
-- Description:	Select all apps for loan file L by loan ID
-- =============================================
CREATE PROCEDURE [dbo].[ListAppsByLoanId] 
	-- Add the parameters for the stored procedure here
	@LoanId Guid = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM Application_A WITH (nolock)
	JOIN Application_B WITH (nolock) ON Application_A.aAppId = Application_B.aAppId
	WHERE Application_A.sLId = @LoanId
	ORDER BY primaryrankindex
END
