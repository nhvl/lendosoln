-- =============================================
-- Author:		Thien Nguyen
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[BROKER_IIS_LOG_DATA_Insert] 
	@BrokerId uniqueidentifier,
    @ReportedDate datetime,
    @FileDbKey varchar(200),
    @GeneratedDate datetime
AS
BEGIN

    INSERT INTO BROKER_IIS_LOG_DATA(BrokerId, ReportedDate, FileDbKey, GeneratedDate)
                      VALUES(@BrokerId, @ReportedDate, @FileDbKey, @GeneratedDate)
END
