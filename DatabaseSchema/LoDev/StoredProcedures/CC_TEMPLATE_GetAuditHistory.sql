-- =============================================
-- Author:		Antonio Valencia
-- Create date: 8/9/2012
-- Description:	Gets the audit history
-- =============================================
CREATE PROCEDURE [dbo].[CC_TEMPLATE_GetAuditHistory]
	@BrokerId uniqueidentifier
AS
BEGIN
	SELECT  [Description], DraftOpenedD, ReleasedD, d.UserFirstNm + ' ' + d.UserLastNm as DraftUser, 
		r.UserFirstNm + ' ' + r.UserLastNm as ReleaseUser
	FROM CC_TEMPLATE_SYSTEM_RELEASE_AUDIT a
		JOIN EMPLOYEE d ON DraftOpenedByUserId = d.EmployeeUserId
		JOIN EMPLOYEE r ON ReleasedByUserId = r.EmployeeUserId
		Where brokerid = @BrokerId order by ReleasedD desc
END
