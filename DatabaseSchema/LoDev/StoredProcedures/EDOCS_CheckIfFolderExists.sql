CREATE procedure [dbo].[EDOCS_CheckIfFolderExists]
	@BrokerId uniqueidentifier,
	@FolderName varchar(50)
as

select 1 
			from EDOCS_FOLDER 
			where FolderName = @FolderName
				and BrokerId = @BrokerId 

