CREATE procedure [dbo].[EDOCS_HasDeletedDoctype]
	@BrokerId uniqueidentifier,
	@DocTypeName varchar(50)
as

select DocTypeName, DocTypeId, BrokerId, FolderId
from EDOCS_DOCUMENT_TYPE
where BrokerId = @BrokerId and IsValid = 0 AND DOcTypeName = @DocTypeName