ALTER PROCEDURE [dbo].[ALL_USER_UPDATE_RECENT_LOGIN]
	@UserId uniqueidentifier,
	@EventDate datetime,
	@IpAddress varchar(46)
AS
BEGIN
	UPDATE ALL_USER
	SET 
		RecentLoginD = @EventDate, 
		LastUsedIpAddress = COALESCE(@IpAddress, LastUsedIpAddress)
	WHERE UserID=@UserId
END