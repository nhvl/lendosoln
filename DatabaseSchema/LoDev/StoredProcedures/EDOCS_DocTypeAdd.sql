ALTER procedure [dbo].[EDOCS_DocTypeAdd]
	@BrokerId uniqueidentifier,
	@DocTypeName varchar(50),
	@FolderId int,
	@ClassificationId int = 0,
    @EsignTargetDocTypeId int = null
as

if exists(select 1 
			from EDOCS_DOCUMENT_TYPE 
			where DocTypeName = @DocTypeName
				and BrokerId = @BrokerId
				and IsValid = 1)
begin
	RAISERROR('A doc type with the same name already exists. Please select another name.', 16, 1);
	return;
end

DECLARE @DocTypeId int;
if exists(select 1 
			from EDOCS_DOCUMENT_TYPE 
			where DocTypeName = @DocTypeName
				and BrokerId = @BrokerId
				and IsValid = 0)
begin
	update EDOCS_DOCUMENT_TYPE
		set @DocTypeId = DocTypeId,
    IsValid = 1,
		FolderId = @FolderId,
		ClassificationId = @ClassificationId
		where DocTypeName = @DocTypeName
			and BrokerId = @BrokerId
end
else
begin
	insert EDOCS_DOCUMENT_TYPE (BrokerId, DocTypeName, IsValid, FolderId, ClassificationId)
		values (@BrokerId, @DocTypeName, 1, @FolderId, @ClassificationId)
    SET @DocTypeId = SCOPE_IDENTITY()
end
EXEC dbo.EDOCS_DOCUMENT_TYPE_MAPPING_AddOrUpdate
    @SourceDocTypeId = @DocTypeId,
    @EsignDocTypeId = @ESignTargetDocTypeId
