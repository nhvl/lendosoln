
-- =============================================
-- Author:		Antonio Valencia
-- Create date: 7/26/2012
-- Description:	Pull Release Version CC Template Info
-- =============================================
CREATE PROCEDURE [dbo].[CC_TEMPLATE_GetReleaseVersion] 
	-- Add the parameters for the stored procedure here
	@BrokerId uniqueidentifier,
	@IncludeIndividualTemplateInfo bit 
AS
BEGIN
	DECLARE @Id int
	DECLARE @ReleasedByUserId uniqueidentifier
	
	SET @Id = null
	
	SELECT TOP 1 @Id = Id, @ReleasedByUserId = ReleasedByUserId FROM CC_TEMPLATE_SYSTEM where BrokerId = @BrokerId AND ReleasedD IS Not Null Order By ReleasedD Desc 
	
	SELECT Id, BrokerId, [Description], DraftOpenedD, DraftOpenedByUserId, ReleasedD, ReleasedByUserId, RuleXml
	FROM CC_TEMPLATE_SYSTEM 
	Where Id = @Id 
	
	IF @IncludeIndividualTemplateInfo = 1 AND @Id is Not Null BEGIN
	
		SELECT UserFirstNm + ' ' + UserLastNm as Name From Employee where EmployeeUserId = @ReleasedByUserId
		
		SELECT cCcTemplateId, cCcTemplateNm, GfeVersion 
		FROM CC_TEMPLATE WHERE CCTemplateSystemId =  @Id					
	END
	
END

