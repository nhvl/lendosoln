-- =============================================
-- Author:		Antonio Valencia
-- Create date: 4/12/11
-- Description:	Delete Condition Choices
-- =============================================
CREATE PROCEDURE [dbo].[TASK_ConditionChoice_Delete] 
	@BrokerId uniqueidentifier, 
	@Id int 
AS
-- REVIEW NOTES:
-- 6/9/2011. Reviewed. -ThinhNK

BEGIN
	DELETE FROM Condition_Choice
	WHERE BrokerId = @BrokerId AND ConditionChoiceId = @Id
END
