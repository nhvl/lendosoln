
CREATE   PROCEDURE [dbo].[ListDiscNotifByUserID]
	@UserID uniqueidentifier
AS
SELECT top 500
		NotifId,
		CASE WHEN( NotifIsRead = 0 ) THEN 0 WHEN( conv.DiscLastModifyD > ISNULL( NotifLastReadD , '1/1/1900' ) AND conv.DiscLastModifyD > ISNULL( NotifAckD , '1/1/1900' ) ) THEN 0 ELSE 1 END AS NotifIsRead,
		DiscCreatorLoginNm,
		DiscStatus,
		DiscStatusDate,
		DiscPriority,
		DiscSubject,
		DiscDueDate,
		ISDATE( DiscDueDate ) AS HasDiscDueDate,
		NotifAlertDate,
		ISDATE( NotifAlertDate ) AS HasNotifAlertDate,
		DiscRefObjNm1,
		DiscRefObjNm2,
		ISNULL(NotifAlertDate, convert(smalldatetime, '2079-01-01')) AS SortAlertDate,
		DiscPriority,
		dbo.GetReminderPriorityDescription(DiscPriority) AS PriorityDescription,
		DiscRefObjId,
		conv.DiscLastModifyD,
		UserFirstNm,
		UserLastNm
             FROM
Discussion_Notification notif 
INNER JOIN Discussion_Log conv with(nolock) ON notif.DiscLogid = conv.DiscLogId 
LEFT JOIN Loan_File_Cache cache with(nolock) ON cache.sLId = conv.DiscRefObjId 
left join view_active_lo_user u with(nolock) on u.userId = conv.DiscCreatorUserId 
/*
LEFT JOIN Broker_User u ON u.UserId = conv.DiscCreatorUserId 
LEFT JOIN Employee AS e ON e.EmployeeId = u.EmployeeId
*/
             WHERE
		NotifIsValid = 1
		AND
		DiscIsRefObjValid = 1
		AND
		conv.IsValid = 1
		AND
		ISNULL( cache.IsTemplate , 0 ) = 0
		AND
		NotifReceiverUserId = @UserID
             ORDER BY
		SortAlertDate ASC, DiscCreatedDate DESC
