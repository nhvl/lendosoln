

CREATE  PROCEDURE [dbo].[UpdateDiscussionLog]
	@DiscLogId uniqueidentifier,
	@DiscRefObjId uniqueidentifier='00000000-0000-0000-0000-000000000000',
	@DiscRefObjType [varchar] (36),
	@DiscRefObjNm1 [varchar] (36),
	@DiscRefObjNm2 [varchar] (100),
	@DiscDueDate [smalldatetime] ,
	@DiscSubject [varchar] (500),
	@DiscPriority [int],
	@DiscStatus [int],
	@DiscStatusDate datetime,
	@DiscIsRefObjValid [bit],
	@DiscLastModifierLoginNm varchar(36),
	@DiscLastModifierUserId uniqueidentifier,
	@DiscLastModifyDate datetime = NULL,
	@IsValid bit = NULL
AS
IF @DiscRefObjID = '00000000-0000-0000-0000-000000000000' 
BEGIN
	declare @currentDiscRefObjID uniqueidentifier;
	declare @brokerId uniqueidentifier;
	select @currentDiscRefObjID = DiscRefObjID, @brokerId  = DiscBrokerId
	from Discussion_Log
	where DiscLogId = @DiscLogId
	if( @DiscRefObjNm1 = '' )
	begin
		if(  @currentDiscRefObjID != '00000000-0000-0000-0000-000000000000' )
		begin
			RAISERROR('Detaching a conversation log from loan file is not allowed.', 16, 1);
			return -100;
		end
	end	
	else
	begin 
		SELECT @DiscRefObjID = sLId, @DiscRefObjNm2 = sPrimBorrowerFullNm, @DiscIsRefObjValid = IsValid
		FROM LOAN_FILE_cache with(nolock)
		WHERE sBrokerID = @brokerID AND sLNm = @DiscRefObjNm1
	end
END
update Discussion_Log
set 	
	DiscLastModifyD = COALESCE( @DiscLastModifyDate , getdate() ),
	DiscLastModifierLoginNm = @DiscLastModifierLoginNm, DiscLastModifierUserId = @DiscLastModifierUserId,
	DiscRefObjId = @DiscRefObjId, DiscRefObjType = @DiscRefObjType, DiscRefObjNm1 = @DiscRefObjNm1, 
	DiscDueDate = @DiscDueDate, DiscSubject = @DiscSubject, DiscPriority = @DiscPriority, DiscStatus = @DiscStatus, DiscStatusDate = @DiscStatusDate, DiscIsRefObjValid = @DiscIsRefObjValid, DiscRefObjNm2 = @DiscRefObjNm2,
	IsValid = COALESCE( @IsValid , IsValid )
where DiscLogId = @DiscLogId


