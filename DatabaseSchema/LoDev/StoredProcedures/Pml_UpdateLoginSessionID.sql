CREATE PROCEDURE Pml_UpdateLoginSessionID 
	@UserID Guid,
	@PmlLoginSessionID Guid
AS
	UPDATE All_User SET PmlLoginSessionID = @PmlLoginSessionID WHERE UserID = @UserID
