-- =============================================
-- Author:		Brian Beery
-- Create date: 03/31/14
-- Description:	Get the IsActiveDirectoryUser bit
-- =============================================
CREATE PROCEDURE [dbo].[IsActiveDirectoryUser] 
	@LoginName varchar(36),
	@Type char(1)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT IsActiveDirectoryUser
	FROM ALL_USER WITH (nolock)
	WHERE LoginNm = @LoginName 
	AND Type = @Type
END