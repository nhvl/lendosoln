CREATE procedure [dbo].[EDOCS_DocTypeAddInternal]
	@DocTypeName varchar(50),
	@FolderId int
as

if exists(select 1 
			from EDOCS_DOCUMENT_TYPE 
			where DocTypeName = @DocTypeName
				and BrokerId is null
				and IsValid = 1)
begin
	RAISERROR('A doc type with the same name already exists. Please select another name.', 16, 1);
	return;
end

if exists(select 1 
			from EDOCS_DOCUMENT_TYPE 
			where DocTypeName = @DocTypeName
				and BrokerId is null
				and IsValid = 0)
begin
	update EDOCS_DOCUMENT_TYPE
		set IsValid = 1,
		FolderId = @FolderId
		where DocTypeName = @DocTypeName
			and BrokerId is null
	return;
end

BEGIN TRANSACTION

	insert EDOCS_DOCUMENT_TYPE (BrokerId, DocTypeName, IsValid, FolderId)
		values (null, @DocTypeName, 1, @FolderId)
	IF @@error!= 0 GOTO HANDLE_ERROR

	declare @DocTypeId as int
	select @DocTypeId = DocTypeId
		from EDOCS_DOCUMENT_TYPE 
		where DocTypeName = @DocTypeName
			and BrokerId is null
	IF @@error!= 0 GOTO HANDLE_ERROR

	if exists(select 1
				from EDOCS_DOCUMENT_TYPE 
				where DocTypeName = @DocTypeName
					and BrokerId is not null)
	begin
		update EDOCS_DOCUMENT
			set	DocTypeId = @DocTypeId
			where exists
			(select 1
				from EDOCS_DOCUMENT_TYPE d
				where d.DocTypeId = EDOCS_DOCUMENT.DocTypeId
					and d.DocTypeName = @DocTypeName)
		IF @@error!= 0 GOTO HANDLE_ERROR

		update EDOCS_DOCUMENT_TYPE_X_SHIPPING_TEMPLATE
			set	DocTypeId = @DocTypeId
			where exists
			(select 1
				from EDOCS_DOCUMENT_TYPE d
				where d.DocTypeId = EDOCS_DOCUMENT_TYPE_X_SHIPPING_TEMPLATE.DocTypeId
					and d.DocTypeName = @DocTypeName)
		IF @@error!= 0 GOTO HANDLE_ERROR

		delete from EDOCS_DOCUMENT_TYPE
			where DocTypeName = @DocTypeName
				and DocTypeId <> @DocTypeId
		IF @@error!= 0 GOTO HANDLE_ERROR
	end

COMMIT TRANSACTION
RETURN

HANDLE_ERROR:
    ROLLBACK TRANSACTION
    RETURN