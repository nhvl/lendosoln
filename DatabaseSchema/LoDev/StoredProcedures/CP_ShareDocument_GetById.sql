-- =============================================
-- Author:		Antonio Valencia
-- Create date: 10/28/2013
-- Description:	Creates Document Share Request
-- =============================================
CREATE PROCEDURE [dbo].[CP_ShareDocument_GetById]
	@Id bigint, 
	@BrokerId uniqueidentifier 
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT * FROM [CP_Share_Document] WHERE BrokerId = @BrokerId AND
	@Id = Id
	
END
