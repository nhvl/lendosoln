ALTER  PROCEDURE [dbo].[CreateBlankLoanFile] 
	@EmployeeID uniqueidentifier,
	@BranchID uniqueidentifier,
	@sLNm Varchar(36),
	@InLoanID uniqueidentifier = NULL, -- If this value is NULL then new ID will generate randomly, else use this value instead.
	@IsTemplate Bit = 0,
	@sLienPosT int = 0,
	@sTotalScoreUniqueLoanId Varchar(20) = '',
	@sLpqLoanNumber bigint = 0,
	@sMersMin varchar(18) = '',
	@sUseGFEDataForSCFields bit = NULL,
	@sIsRequireFeesFromDropDown bit = 0,
	@sClosingCostFeeVersionT int = 0,
	@sIsHousingExpenseMigrated bit = 0,
	@sLoanFileT int = 0,
    @LoanID uniqueidentifier OUT,
    @sCFPBSetsVersionNumT int = 0, -- TODO REMOVE ID-CHECK
    @sLoanVersionT int = 0,
    @sUse2016ComboAndProrationUpdatesTo1003Details bit = 0,
    @sAprCalculationT int = 0,
    @sLRefNm varchar(100) = '',
	@sCalculateInitialLoanEstimate bit = 1,
	@sCalculateInitialClosingDisclosure bit = 1,
	@sLegalEntityIdentifier varchar(20) = '',
	@sDataSchemaVersion int = NULL, -- TODO: Make this required.
	@sEncryptionKey uniqueidentifier = null,
	@sEncryptionMigrationVersion tinyint = 0,
	@sBorrowerApplicationCollectionT int = 0,
	@sAlwaysRequireAllBorrowersToReceiveClosingDisclosure bit = NULL
AS
--declare @tranPoint sysname
--set @tranPoint = object_name(@@procId) + CAST(@@nestlevel as varchar(10))
--print @tranPoint
	
--BEGIN TRANSACTION
--SAVE TRANSACTION @tranPoint
-- WILL CHECK FOR DB CONSISTENCY HERE FOR THE PARAMS
DECLARE @BranchNm varchar(100)
DECLARE @BranchCode varchar(36)
DECLARE @AppId GUID
DECLARE @BrokerID uniqueidentifier

SELECT @BrokerID = b.BrokerID FROM Branch b WHERE b.BranchID = @BranchID
if @@error != 0
begin
	RAISERROR('error in selecting from employee in CreateBlankLoanFile sp', 16, 1);
	goto ErrorHandler;
end
IF @InLoanID IS NULL
BEGIN
	
	--set @LoanID = newid()
	--TRYING THIS OUT TO AVOID PERFORMANCE ISSUE. -thinh 3/24/2014
	set @LoanId = cast( cast(NewID() as binary(10)) + cast(GetDate() as binary(6)) as uniqueidentifier)

END
ELSE
BEGIN
	SET @LoanID = @InLoanID
END
-- dd 06/08/2005 - If broker has LON Integration then any new loan create will set sIntegrationT correctly.
DECLARE @sIntegrationT int
SET @sIntegrationT = 0 -- default to None.
IF @IsTemplate = 0
BEGIN
   SELECT @sIntegrationT = 1 FROM Broker WHERE BrokerID = @BrokerID AND HasLONIntegration = 1
END
INSERT INTO LOAN_FILE_A with( rowlock )
( sLId, 	  IsTemplate, sIntegrationT, sDataSchemaVersion, sIsSellerProvidedBelowMktFin,sYrBuilt,sArmIndexT,sFannieSpT,sFannieDocT,sMldsPpmtMonMax,sFinMethT,sSubFinTerm,sSubFinIR,sDocPrepFProps,sU5DocStatN,sU4DocStatN,sU3DocStatN,sU2DocStatN,sU1DocStatN,sTilGfeN,sApprRprtN,sPrelimRprtN,sCcTemplateNm,sLpTemplateNm,DeletedD,sRAdjWorstIndex, sEncryptionKey, sEncryptionMigrationVersion, sBorrowerApplicationCollectionT)
VALUES(  @LoanID, @IsTemplate, @sIntegrationT, @sDataSchemaVersion, '','',0,0,0,0,0,0,0,0,'','','','','','','','','','',0,0, @sEncryptionKey, @sEncryptionMigrationVersion, @sBorrowerApplicationCollectionT)
if @@error != 0
begin
	RAISERROR('error in inserting into Loan_File_A in CreateBlankLoanFile sp', 16, 1);
	goto ErrorHandler;
end
INSERT INTO LOAN_FILE_B with( rowlock )
( sLId,sBiweeklyPmt,sLienPosT,sIsRequireFeesFromDropDown,sClosingCostFeeVersionT,sIsHousingExpenseMigrated,sLoanFileT,sCFPBSetsVersionNumT,sLoanVersionT,sUse2016ComboAndProrationUpdatesTo1003Details, sAprCalculationT, sCalculateInitialLoanEstimate, sCalculateInitialClosingDisclosure, sLegalEntityIdentifier)
VALUES( @LoanID,0, @sLienPosT, @sIsRequireFeesFromDropDown, @sClosingCostFeeVersionT, @sIsHousingExpenseMigrated, @sLoanFileT, @sCFPBSetsVersionNumT, @sLoanVersionT, @sUse2016ComboAndProrationUpdatesTo1003Details, @sAprCalculationT, @sCalculateInitialLoanEstimate, @sCalculateInitialClosingDisclosure, @sLegalEntityIdentifier)
if @@error != 0
begin
	RAISERROR('error in inserting into Loan_File_B in CreateBlankLoanFile sp', 16, 1);;
	goto ErrorHandler;
end
INSERT INTO LOAN_FILE_C with( rowlock )
( sLId, sUseGFEDataForSCFields )
VALUES( @LoanID, @sUseGFEDataForSCFields )
if @@error != 0
begin
	RAISERROR('error in inserting into Loan_File_C in CreateBlankLoanFile sp', 16, 1);;
	goto ErrorHandler;
end
INSERT INTO LOAN_FILE_D with( rowlock )
( sLId,sRLckdNumOfDays )
VALUES( @LoanID,0 )
if @@error != 0
begin
	RAISERROR('error in inserting into Loan_File_D in CreateBlankLoanFile sp', 16, 1);;
	goto ErrorHandler;
end
INSERT INTO LOAN_FILE_E with( rowlock )
( sLId, sLNm, sBrokerId, sLpqLoanNumber, sLRefNm )
VALUES( @LoanID, @sLNm, @BrokerId, @sLpqLoanNumber, @sLRefNm )
if @@error != 0
begin
	RAISERROR('error in inserting into Loan_File_E in CreateBlankLoanFile sp', 16, 1);;
	goto ErrorHandler;
end
-- OPM 72500 fhalenderid code updates from loan, branch, or broker.
DECLARE @fhalenderid varchar(10)
exec FhaLenderIDFromLoanOrBranchOrBroker @LoanID, @BranchID, @fhalenderid OUTPUT
if @@error != 0
begin
	RAISERROR('error in executing FhaLenderIDFromLoanOrBranchOrBroker sp', 16, 1);;
	goto ErrorHandler;
end
UPDATE LOAN_FILE_E with( rowlock )
SET sFhaLenderIdCode = @fhalenderid
WHERE slid = @LoanID
if @@error != 0
begin
	RAISERROR('error in updating fhalenderid in Loan_File_E in CreateBlankLoanFile sp', 16, 1);;
	goto ErrorHandler;
end

-- OPM 64339 :  For blank loans created after the effective date, default to lender paid instead. -AM
declare @OrgCompMigrationDt datetime
declare @sOriginatorCompensationPaymentSourceT int
set @sOriginatorCompensationPaymentSourceT = 0
select @OrgCompMigrationDt=OriginatorCompensationMigrationDate from BROKER where BrokerID=@BrokerID
if @OrgCompMigrationDt is null or GETDATE() > @OrgCompMigrationDt  
begin
	set @sOriginatorCompensationPaymentSourceT = 2 -- Lender Paid
end

--147124 Default Originator Comp as an APR Field for Files Created off of Blank Template
INSERT INTO LOAN_FILE_F with( rowlock )
( sLId, 	 sBranchId, sTotalScoreUniqueLoanId, sMersMin, sOriginatorCompensationPaymentSourceT, sGfeOriginatorCompFProps, sAlwaysRequireAllBorrowersToReceiveClosingDisclosure)
VALUES( @LoanID, @BranchID,@sTotalScoreUniqueLoanId, @sMersMin, @sOriginatorCompensationPaymentSourceT, 1, @sAlwaysRequireAllBorrowersToReceiveClosingDisclosure)
if @@error != 0
begin
	RAISERROR('error in inserting into Loan_File_F in CreateBlankLoanFile sp', 16, 1);;
	goto ErrorHandler;
end
INSERT INTO TRUST_ACCOUNT with( rowlock )
( sLId )
VALUES( @LoanID )
if @@error != 0
begin
	RAISERROR('error in inserting into TRUST_ACCOUNT in CreateBlankLoanFile sp', 16, 1);;
	goto ErrorHandler;
end
INSERT INTO LOAN_FILE_CACHE with( rowlock )
(sLId, sBrokerId, sBranchId, sLNm, IsTemplate, IsValid, sLoanFileT)
values
(@LoanID, @BrokerID, @BranchID, @sLNm, @IsTemplate, 0, @sLoanFileT)
if @@error != 0
begin
	RAISERROR('error in inserting into loan_file_cache in CreateBlankLoanFile sp', 16, 1);;
	goto ErrorHandler;
end
INSERT INTO LOAN_FILE_CACHE_2 with (rowlock)
(sLId)
values
(@LoanID)
if @@error != 0
begin
	RAISERROR('error in inserting into loan_file_cache_2 in CreateBlankLoanFile sp', 16, 1);;
	goto ErrorHandler;
end
INSERT INTO LOAN_FILE_CACHE_3 with (rowlock)
(sLId)
values
(@LoanID)
if @@error != 0
begin
	RAISERROR('error in inserting into loan_file_cache_3 in CreateBlankLoanFile sp', 16, 1);;
	goto ErrorHandler;
end

INSERT INTO LOAN_FILE_CACHE_4 with (rowlock)
(sLId, sLRefNm)
values
(@LoanID, @sLRefNm)
if @@error != 0
begin
	RAISERROR('error in inserting into LOAN_FILE_CACHE_4 in CreateBlankLoanFile sp', 16, 1);;
	goto ErrorHandler;
end

declare @retVal int
Exec @retVal  = CreateNewApplication @LoanID, @AppId output
if( 0!=@@error or 0!=@retVal )
begin
	RAISERROR('error in executing CreateNewApplication sp in CreateBlankLoanFile sp', 16, 1);;
	goto ErrorHandler;
end

IF @sBorrowerApplicationCollectionT <> 0
BEGIN
	DECLARE 
		-- All used as OUT parameters.
		@UladAppId UniqueIdentifier,
		@UladAppConsumerId UniqueIdentifier,
		@ConsumerId UniqueIdentifier,
		@IsPrimary Bit;
	EXEC CreateNewUladApplication @LoanId, @BrokerId, @AppId, @UladAppId, @UladAppConsumerId, @ConsumerId, @IsPrimary;
END
IF @@error != 0
BEGIN
	RAISERROR('Error executing CreateNewUladApplication sp in CreateBlankLoanFile sp', 16, 1);
	GOTO ErrorHandler;
END

--commit transaction;
return 0;
ErrorHandler:
	--rollback tran @tranPoint
	--rollback transaction
	return -100;
