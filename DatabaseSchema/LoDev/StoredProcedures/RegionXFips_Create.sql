CREATE PROCEDURE [dbo].[RegionXFips_Create]
	@BrokerId uniqueidentifier,
	@RegionSetId int,
	@RegionId int,
	@FipsCountyCode int
AS
BEGIN
	INSERT INTO REGION_X_FIPS (BrokerId, RegionSetId, RegionId, FipsCountyCode)
	VALUES (@BrokerId, @RegionSetId, @RegionId, @FipsCountyCode)
END