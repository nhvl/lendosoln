


CREATE PROCEDURE [dbo].[CP_SetPasswordResetCodeByConsumerId]
	@ConsumerId bigint,
	@BrokerId uniqueidentifier,
	@ResetCode varchar(200)
AS

UPDATE CP_CONSUMER
SET		PasswordResetCode = @ResetCode, PasswordRequestD = GETDATE()
WHERE	ConsumerId = @ConsumerId AND BrokerId = @BrokerId