-- =============================================
-- Author:		Geoffrey Feltman
-- Create date: 5/31/13
-- Description:	Get a list of loans that need 
-- numbering for the given broker. A loan is 
-- needs numbering when the target field is blank.
-- =============================================
ALTER PROCEDURE [dbo].[ListLoansThatNeedNumberingForBroker] 
	-- Add the parameters for the stored procedure here
	@BrokerId uniqueidentifier = null,
	@FieldId varchar(35) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT sLId
	FROM LOAN_FILE_CACHE
	WHERE sBrokerId = @BrokerId AND
		IsTemplate = 0 AND
		IsValid = 1 AND
		sStatusT NOT IN (12, 15, 16, 17) AND -- don't return leads
		CASE @FieldId
			WHEN 'sCustomField1Notes' THEN sCustomField1Notes 
			WHEN 'sCustomField2Notes' THEN sCustomField2Notes 
			WHEN 'sCustomField3Notes' THEN sCustomField3Notes 
			WHEN 'sCustomField4Notes' THEN sCustomField4Notes 
			WHEN 'sCustomField5Notes' THEN sCustomField5Notes 
			WHEN 'sCustomField6Notes' THEN sCustomField6Notes 
			WHEN 'sCustomField7Notes' THEN sCustomField7Notes 
			WHEN 'sCustomField8Notes' THEN sCustomField8Notes 
			WHEN 'sCustomField9Notes' THEN sCustomField9Notes 
			WHEN 'sCustomField10Notes' THEN sCustomField10Notes 
			WHEN 'sCustomField11Notes' THEN sCustomField11Notes 
			WHEN 'sCustomField12Notes' THEN sCustomField12Notes 
			WHEN 'sCustomField13Notes' THEN sCustomField13Notes 
			WHEN 'sCustomField14Notes' THEN sCustomField14Notes 
			WHEN 'sCustomField15Notes' THEN sCustomField15Notes 
			WHEN 'sCustomField16Notes' THEN sCustomField16Notes 
			WHEN 'sCustomField17Notes' THEN sCustomField17Notes 
			WHEN 'sCustomField18Notes' THEN sCustomField18Notes 
			WHEN 'sCustomField19Notes' THEN sCustomField19Notes 
			WHEN 'sCustomField20Notes' THEN sCustomField20Notes
			WHEN 'sCustomField21Notes' THEN sCustomField21Notes
			WHEN 'sCustomField22Notes' THEN sCustomField22Notes
			WHEN 'sCustomField23Notes' THEN sCustomField23Notes
			WHEN 'sCustomField24Notes' THEN sCustomField24Notes
			WHEN 'sCustomField25Notes' THEN sCustomField25Notes
			WHEN 'sCustomField26Notes' THEN sCustomField26Notes
			WHEN 'sCustomField27Notes' THEN sCustomField27Notes
			WHEN 'sCustomField28Notes' THEN sCustomField28Notes
			WHEN 'sCustomField29Notes' THEN sCustomField29Notes
			WHEN 'sCustomField30Notes' THEN sCustomField30Notes
			WHEN 'sCustomField31Notes' THEN sCustomField31Notes
			WHEN 'sCustomField32Notes' THEN sCustomField32Notes
			WHEN 'sCustomField33Notes' THEN sCustomField33Notes
			WHEN 'sCustomField34Notes' THEN sCustomField34Notes
			WHEN 'sCustomField35Notes' THEN sCustomField35Notes
			WHEN 'sCustomField36Notes' THEN sCustomField36Notes
			WHEN 'sCustomField37Notes' THEN sCustomField37Notes
			WHEN 'sCustomField38Notes' THEN sCustomField38Notes
			WHEN 'sCustomField39Notes' THEN sCustomField39Notes
			WHEN 'sCustomField40Notes' THEN sCustomField40Notes
			WHEN 'sCustomField41Notes' THEN sCustomField41Notes
			WHEN 'sCustomField42Notes' THEN sCustomField42Notes
			WHEN 'sCustomField43Notes' THEN sCustomField43Notes
			WHEN 'sCustomField44Notes' THEN sCustomField44Notes
			WHEN 'sCustomField45Notes' THEN sCustomField45Notes
			WHEN 'sCustomField46Notes' THEN sCustomField46Notes
			WHEN 'sCustomField47Notes' THEN sCustomField47Notes
			WHEN 'sCustomField48Notes' THEN sCustomField48Notes
			WHEN 'sCustomField49Notes' THEN sCustomField49Notes
			WHEN 'sCustomField50Notes' THEN sCustomField50Notes
			WHEN 'sCustomField51Notes' THEN sCustomField51Notes
			WHEN 'sCustomField52Notes' THEN sCustomField52Notes
			WHEN 'sCustomField53Notes' THEN sCustomField53Notes
			WHEN 'sCustomField54Notes' THEN sCustomField54Notes
			WHEN 'sCustomField55Notes' THEN sCustomField55Notes
			WHEN 'sCustomField56Notes' THEN sCustomField56Notes
			WHEN 'sCustomField57Notes' THEN sCustomField57Notes
			WHEN 'sCustomField58Notes' THEN sCustomField58Notes
			WHEN 'sCustomField59Notes' THEN sCustomField59Notes
			WHEN 'sCustomField60Notes' THEN sCustomField60Notes
		END = ''
	ORDER BY sCreatedD
END
