-- =============================================
-- Author:		Michael Leinweaver
-- Create date: 6/21/2017
-- Description:	Saves a theme collection.
-- =============================================
CREATE PROCEDURE [dbo].[ThemeCollection_Save]
	@BrokerId uniqueidentifier,
	@ThemeCollectionType int,
	@SelectedThemeId uniqueidentifier,
	@Version bigint,
	@ThemeCollectionId int OUT
AS
BEGIN
	UPDATE BROKER_THEME_COLLECTION
	SET
		SelectedThemeId = @SelectedThemeId,
		Version = Version + 1,
		@ThemeCollectionId = ThemeCollectionId
	WHERE BrokerId = @BrokerId AND ThemeCollectionType = @ThemeCollectionType
	
	IF @@rowcount = 0
	BEGIN
		INSERT INTO BROKER_THEME_COLLECTION(BrokerId, ThemeCollectionType, SelectedThemeId, Version)
		VALUES(@BrokerId, @ThemeCollectionType, @SelectedThemeId, 0)
		
		SELECT @ThemeCollectionId = SCOPE_IDENTITY()
	END	
END
GO
