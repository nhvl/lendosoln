ALTER PROCEDURE [dbo].[TASK_ListTasksByBrokerId] 
	@BrokerId uniqueidentifier,
	@UserId uniqueidentifier = NULL,
	@IncludeOwnedTasks bit = 0,
	@LoanId uniqueidentifier = NULL,
	@IncludeClosed bit = 0,
	@IncludeOpen bit = 1,
	--@OnlyDeleted bit = 0,
	@IncludeDeleted bit = 0,
	@IncludeActive bit = 1,
	@OnlyConditions bit = 0,
	@OnlyNonConditions bit = 0,
	@OnlyOpen bit = 0,
	@IncludeInvalidLoans bit = 0,
	@OnlyOpenedBefore smalldatetime = null,
	@LoanOpenedMinDate smalldatetime = null,
	@LoanOpenedMaxDate smalldatetime = null,
	@ConditionCategoryId int = null
AS

-- REVIEW
-- 6/9/2011: Joints have been eliminated. Reviewed. -ThinhNK
-- 6/20/2011: Needs Review -- modified by antonio to check is valid so we can log into PML
-- 7/5/2012: Added CondRowId. MP
-- 7/6/2012: Split out into 2 scenarios so we can use index more efficiently by avoid coalesce on loanID. -ThinhNK
-- 1/2/2013: Adding parameters OnlyNonConditions, OnlyOpen. SK
-- 1/7/2013: Adding OnlyOpenedBefore parameter. SK
-- 1/11/2013: Adding LoanOpenedMinDate and LoanOpenedMaxDate parameters. SK
-- 1/17/2013: Adding ConditionCategoryId

IF @LoanId is null
	BEGIN

				SELECT
					BrokerId
					, TaskId
					, TaskAssignedUserId
					--, TaskIdentityId
					, TaskStatus
					, LoanId
					, TaskOwnerUserId
					, TaskCreatedDate
					, TaskSubject
					, TaskDueDate
					, TaskFollowUpDate
					, CondInternalNotes
					, TaskIsCondition
					, TaskResolvedUserId
					, TaskLastModifiedDate
					, BorrowerNmCached
					, LoanNumCached
					, TaskToBeAssignedRoleId
					, TaskToBeOwnerRoleId
					, TaskCreatedByUserId
					, TaskDueDateLocked
					, TaskDueDateCalcField
					, TaskDueDateCalcDays
					, TaskHistoryXml
					, TaskPermissionLevelId
					, TaskClosedUserId
					, TaskClosedDate
					, TaskRowVersion
					, CondCategoryId
					, CondIsHidden
					--, CondRank
					, CondRowId
					, CondDeletedDate
					, CondDeletedByUserNm
					, CondIsDeleted
					--, assigned.UserFirstNm + ' ' + assigned.UserLastNm as AssignedUserFullName
					--, owned.UserFirstNm + ' ' + owned.UserLastNm as OwnerFullName
					--, CASE WHEN TaskClosedUserId IS NULL THEN '' ELSE closed.UserFirstNm + ' ' + closed.UserLastNm END as ClosingUserFullName
					, TaskAssignedUserFullName
					, TaskOwnerFullName
					, TaskClosedUserFullName
					, CondMessageId
					, CondRequiredDocTypeId
					, ResolutionBlockTriggerName
					, ResolutionDenialMessage
					, ResolutionDateSetterFieldId
					, AssignToOnReactivationUserId

				FROM TASK t WITH(ROWLOCK)
					LEFT JOIN LOAN_FILE_CACHE loan_file on loan_file.sLId = t.LoanId 
					--left join Employee assigned on t.TaskAssignedUserId = assigned.EmployeeUserId
					--left join Employee owned on t.TaskOwnerUserId = owned.EmployeeUserId
					--left join Employee closed on t.TaskClosedUserId = closed.EmployeeUserId

				WHERE
					BrokerId = @BrokerId
					--AND
					--LoanId = COALESCE(@LoanId, LoanId)
					--AND		@OnlyDeleted = CondIsDeleted 
					AND
					(
						( @IncludeDeleted = 1 AND CondIsDeleted = 1 )
						OR
						( @IncludeActive = 1 AND CondIsDeleted = 0 )
					)
					AND
					(
						( @OnlyOpenedBefore is null )
						OR
						( DateDiff(day, @OnlyOpenedBefore , TaskCreatedDate ) < 0 )
					)

					AND
					(	
						@OnlyConditions = 0 
						OR
						TaskIsCondition  = 1
					)
					AND
					(	
						@OnlyNonConditions = 0 
						OR
						TaskIsCondition  = 0
					)
					AND
					( 
						@UserId IS NULL
						OR
						( 
							TaskAssignedUserId = COALESCE(@UserId, TaskAssignedUserId)
							OR
							(@IncludeOwnedTasks = 1 AND TaskOwnerUserId = COALESCE(@UserId, TaskOwnerUserId) )
						)
					)
					AND
					(
						( @IncludeClosed = 1 AND TaskStatus = 2 )
						OR
						( @IncludeOpen = 1 AND TaskStatus <> 2 )
					)
					AND
					(
						@OnlyOpen = 0
						OR
						TaskStatus = 0
					)
					
					AND loan_file.IsValid = 1 
					AND
					(
						@LoanOpenedMinDate is null
						OR
						(
							loan_file.sOpenedD is not null 
							and 
							DateDiff(day, @LoanOpenedMinDate , loan_file.sOpenedD) >= 0
						)
					)
					AND
					(
						@LoanOpenedMaxDate is null
						OR
						(
							loan_file.sOpenedD is not null 
							and 
							DateDiff(day, @LoanOpenedMaxDate , loan_file.sOpenedD ) <= 0
						)
					)
					AND
					(
						@ConditionCategoryId is null
						OR
						@ConditionCategoryId = CondCategoryId
					)


	END
ELSE 
	BEGIN
				SELECT
					BrokerId
					, TaskId
					, TaskAssignedUserId
					--, TaskIdentityId
					, TaskStatus
					, LoanId
					, TaskOwnerUserId
					, TaskCreatedDate
					, TaskSubject
					, TaskDueDate
					, TaskFollowUpDate
					, CondInternalNotes
					, TaskIsCondition
					, TaskResolvedUserId
					, TaskLastModifiedDate
					, BorrowerNmCached
					, LoanNumCached
					, TaskToBeAssignedRoleId
					, TaskToBeOwnerRoleId
					, TaskCreatedByUserId
					, TaskDueDateLocked
					, TaskDueDateCalcField
					, TaskDueDateCalcDays
					, TaskHistoryXml
					, TaskPermissionLevelId
					, TaskClosedUserId
					, TaskClosedDate
					, TaskRowVersion
					, CondCategoryId
					, CondIsHidden
					--, CondRank
					, CondRowId
					, CondDeletedDate
					, CondDeletedByUserNm
					, CondIsDeleted
					--, assigned.UserFirstNm + ' ' + assigned.UserLastNm as AssignedUserFullName
					--, owned.UserFirstNm + ' ' + owned.UserLastNm as OwnerFullName
					--, CASE WHEN TaskClosedUserId IS NULL THEN '' ELSE closed.UserFirstNm + ' ' + closed.UserLastNm END as ClosingUserFullName
					, TaskAssignedUserFullName
					, TaskOwnerFullName
					, TaskClosedUserFullName
					, CondMessageId
					, CondRequiredDocTypeId
					, ResolutionBlockTriggerName
					, ResolutionDenialMessage
					, ResolutionDateSetterFieldId
					, AssignToOnReactivationUserId

				FROM TASK t WITH(ROWLOCK, INDEX(idx_BrokerId_LoanId_TaskIsCondition_CondIsDeleted))
					LEFT JOIN LOAN_FILE_CACHE loan_file WITH(INDEX(PK__LOAN_FILE_CACHE__12B3B8EF)) on loan_file.sLId = t.LoanId 
					--left join Employee assigned on t.TaskAssignedUserId = assigned.EmployeeUserId
					--left join Employee owned on t.TaskOwnerUserId = owned.EmployeeUserId
					--left join Employee closed on t.TaskClosedUserId = closed.EmployeeUserId

				WHERE
					BrokerId = @BrokerId
					AND
					LoanId = @LoanId
					--AND		@OnlyDeleted = CondIsDeleted 
					AND
					(
						( @IncludeDeleted = 1 AND CondIsDeleted = 1 )
						OR
						( @IncludeActive = 1 AND CondIsDeleted = 0 )
					)
					AND
					(
						( @OnlyOpenedBefore is null )
						OR
						( DateDiff(day, @OnlyOpenedBefore , TaskCreatedDate ) < 0 )
					)

					AND
					(	
						@OnlyConditions = 0 
						OR
						TaskIsCondition  = 1
					)
					AND
					(	
						@OnlyNonConditions = 0 
						OR
						TaskIsCondition  = 0
					)
					AND
					( 
						@UserId IS NULL
						OR
						( 
							TaskAssignedUserId = COALESCE(@UserId, TaskAssignedUserId)
							OR
							(@IncludeOwnedTasks = 1 AND TaskOwnerUserId = COALESCE(@UserId, TaskOwnerUserId) )
						)
					)
					AND
					(
						( @IncludeClosed = 1 AND TaskStatus = 2 )
						OR
						( @IncludeOpen = 1 AND TaskStatus <> 2 )
					)
					AND
					(
						@OnlyOpen = 0
						OR
						TaskStatus = 0
					)
					AND
					(
						@IncludeInvalidLoans = 1
						OR
						loan_file.IsValid = 1
					)
					AND
					(
						@LoanOpenedMinDate is null
						OR
						(
							loan_file.sOpenedD is not null 
							and 
							DateDiff(day, @LoanOpenedMinDate , loan_file.sOpenedD) >= 0
						)
					)
					AND
					(
						@LoanOpenedMaxDate is null
						OR
						(
							loan_file.sOpenedD is not null 
							and 
							DateDiff(day, @LoanOpenedMaxDate , loan_file.sOpenedD ) <= 0
						)
					)
					AND
					(
						@ConditionCategoryId is null
						OR
						@ConditionCategoryId = CondCategoryId
					)
	END