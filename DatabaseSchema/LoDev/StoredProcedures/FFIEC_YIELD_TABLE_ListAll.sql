-- =============================================
-- Author:		David Dao
-- Create date: 
-- Description:	
-- =============================================
ALTER PROCEDURE dbo.FFIEC_YIELD_TABLE_ListAll
    @IncludeContent bit,
    @DataSource int = null
AS
BEGIN

    IF @IncludeContent = 1
    BEGIN
        SELECT DataSource, [Type], LastModifiedDate,MD5Checksum, Content
        FROM FFIEC_YIELD_TABLE
        WHERE @DataSource IS NULL OR DataSource = @DataSource
    END
    ELSE
    BEGIN
        SELECT DataSource, [Type], LastModifiedDate,MD5Checksum
        FROM FFIEC_YIELD_TABLE
        WHERE @DataSource IS NULL OR DataSource = @DataSource
    END

END
