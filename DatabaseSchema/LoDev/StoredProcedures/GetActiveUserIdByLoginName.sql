-- =============================================
-- Author:		David Dao
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE dbo.GetActiveUserIdByLoginName
    @LoginNm varchar(36),
    @BrokerId UniqueIdentifier
AS
BEGIN
    SELECT UserId
    FROM View_Active_LO_User WITH(NOLOCK)
    WHERE LoginNm=@LoginNm AND Type='B' AND BrokerId=@BrokerId

END
