create PROCEDURE ListActiveLicense
	@BrokerId Guid
AS
	SELECT * 
	FROM LICENSE 
	WHERE LicenseOwnerBrokerId=@BrokerId AND ExpirationDate > GETDATE()
