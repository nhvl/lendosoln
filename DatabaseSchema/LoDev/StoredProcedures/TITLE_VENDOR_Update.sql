-- =============================================
-- Author:		Antonio Valencia
-- Create date: 4/23/2013
-- Description:	Creates a Title Vendor Record
-- =============================================
ALTER PROCEDURE [dbo].[TITLE_VENDOR_Update]
	@Name varchar(255),
	@ExportPath varchar(1000),
	@Id int = null, 
	@AccountIdRequired bit,
	@LQBServiceUserName varchar(20),
	@LQBServicePassword varchar(200) = NULL,
	@EncryptedLQBServicePassword varbinary(250),
	@LQBServiceSourceName varchar(20),
	@TitleVendorExportPathTitle varchar(1000),
	@TitleVendorIntegrationType int,
	@EnableConnectionTest bit,
	@EncryptionKeyId uniqueidentifier = NULL -- This parameter is intentionally optional, and should only be used when inserting
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	IF @Id IS NULL BEGIN
		INSERT TITLE_VENDOR(
			TitleVendorName,
			TitleVendorExportPath,
			TitleVendorRequiresAccountId,
			LQBServiceUserName,
			LQBServicePassword,
			EncryptedLQBServicePassword,
			LQBServiceSourceName,
			TitleVendorExportPathTitle,
			TitleVendorIntegrationType,
			EnableConnectionTest,
			EncryptionKeyId
		) VALUES (
			@Name,
			@ExportPath,
			@AccountIdRequired,
			@LQBServiceUserName,
			COALESCE(@LQBServicePassword,''),
			@EncryptedLQBServicePassword,
			@LQBServiceSourceName,
			@TitleVendorExportPathTitle,
			@TitleVendorIntegrationType,
			@EnableConnectionTest,
			@EncryptionKeyId
		);

		SELECT @@Identity 
	END
	ELSE BEGIN
		UPDATE TOP(1) TITLE_VENDOR
		SET
			TitleVendorName = @Name, 
			TitleVendorExportPath = @ExportPath,
			TitleVendorRequiresAccountId = @AccountIdRequired,
			LQBServiceUserName = @LQBServiceUserName,
			LQBServicePassword = COALESCE(@LQBServicePassword, LQBServicePassword),
			EncryptedLQBServicePassword = @EncryptedLQBServicePassword,
			LQBServiceSourceName = @LQBServiceSourceName,
			TitleVendorExportPathTitle = @TitleVendorExportPathTitle,
			TitleVendorIntegrationType = @TitleVendorIntegrationType,
			EnableConnectionTest = @EnableConnectionTest
		WHERE TitleVendorId = @Id

		SELECT @@rowcount
	END
END
