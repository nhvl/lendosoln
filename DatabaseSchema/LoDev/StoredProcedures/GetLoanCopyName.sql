CREATE  PROCEDURE GetLoanCopyName
	@BrokerID Guid , @sLId Guid , @Name Varchar( 50 ) OUTPUT
AS 
	SELECT
		@Name = f.sLNm
	FROM
		Loan_File_Cache AS f with( nolock )
	WHERE
		f.sLId = @sLId
