-- =============================================
-- Author:		Eric Mallare
-- Create date: 1/23/2018
-- Description:	Inserts a DocuSign envelope record into the DB.
-- =============================================
ALTER PROCEDURE [dbo].[DOCUSIGN_ENVELOPES_Create]
	@LoanId uniqueidentifier,
	@BrokerId uniqueidentifier,
	@CurrentStatus int,
	@EnvelopeId varchar(36),
	@Name varchar(200),
	@RequestedDate datetime,
	@SenderName varchar(200),
	@SenderEmail varchar(200) = null,
	@SenderUserId uniqueidentifier,
	@CreatedDate datetime = null,
	@DeletedDate datetime = null,
	@SentDate datetime = null,
	@DeliveredDate datetime = null,
	@SignedDate datetime = null,
	@CompletedDate datetime = null,
	@DeclinedDate datetime = null,
	@TimedOutDate datetime = null,
	@VoidedDate datetime = null,
	@Id int OUTPUT

AS
BEGIN
	INSERT INTO DOCUSIGN_ENVELOPES
		(CurrentStatus,
		 LoanId,
		 BrokerId,
		 EnvelopeId,
		 Name,
		 RequestedDate,
		 SenderName,
		 SenderEmail,
		 SenderUserId,
		 CompletedDate,
		 CreatedDate,
		 DeclinedDate,
		 DeletedDate,
		 DeliveredDate,
		 SentDate,
		 SignedDate,
		 TimedOutDate,
		 VoidedDate)
	VALUES
		(@CurrentStatus,
		 @LoanId,
		 @BrokerId,
		 @EnvelopeId,
		 @Name,
		 @RequestedDate,
		 @SenderName,
		 @SenderEmail,
		 @SenderUserId,
		 @CompletedDate,
		 @CreatedDate,
		 @DeclinedDate,
		 @DeletedDate,
		 @DeliveredDate,
		 @SentDate,
		 @SignedDate,
		 @TimedOutDate,
		 @VoidedDate)

	SET @Id=SCOPE_IDENTITY();
END
