


CREATE   PROCEDURE [dbo].[IsActiveInternalUser]
	@LoginName varchar(36)
AS
SELECT '' -- For now, we just need to know if this user is active
FROM VIEW_ACTIVE_INTERNAL_USER
WHERE LoginNm = @LoginName