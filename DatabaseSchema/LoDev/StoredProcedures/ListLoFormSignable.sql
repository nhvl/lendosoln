CREATE PROCEDURE ListLoFormSignable 
	@BrokerId UniqueIdentifier
AS
BEGIN
	SELECT FormId, Description, IsSignable, IsStandardForm, FileName
	FROM LO_FORM
	Where (BrokerId = @BrokerId OR BrokerId IS NULL)
      AND IsSignable = 1
	ORDER BY FileName
END
