
-- =============================================
-- Author:		Matthew Pham
-- Create date: 01/29/13
-- Description:	Retrieves List of loans by branchid
-- =============================================

CREATE     PROCEDURE [dbo].[ListLoansByBranchId]
	@BrokerId uniqueidentifier, 
	@BranchId uniqueidentifier,
	@IsValid bit = NULL,
	@IsTemplate bit = NULL
AS
	SELECT
		sLId AS LoanId,
		sBrokerId AS BrokerId,
		sBranchId AS BranchId,
		sLNm AS LoanNm,
		sPrimBorrowerFullNm AS BorrowerNm,
		sStatusT AS Status
	FROM Loan_File_Cache WITH (NOLOCK)
	WHERE
		sBrokerID = @BrokerId
		AND sBranchID = @BranchId
		AND IsTemplate = COALESCE( @IsTemplate , IsTemplate )
		AND IsValid = COALESCE( @IsValid , IsValid )

