CREATE PROCEDURE UnsubscribeFeature
	@BrokerID Guid,
	@FeatureID Guid,
	@UserID Guid 
AS
DECLARE @tranPoint SYSNAME
SET @tranPoint = OBJECT_NAME(@@procId) + CAST(@@NESTLEVEL AS VARCHAR(10))
BEGIN TRANSACTION
SAVE TRANSACTION @tranPoint
	DELETE FROM Feature_Subscription WHERE BrokerID = @BrokerID AND FeatureID = @FeatureID
	IF (0!=@@error)
	BEGIN
		RAISERROR('Error in delete record from table Feature_Subscription  in UnsubscribeFeature sp', 16, 1);
		GOTO ErrorHandler;
	END
	INSERT INTO Usage_Event (FeatureID, BrokerId, UserID, EventType, WhoDoneIt) VALUES (@FeatureID, @BrokerID, '00000000-0000-0000-0000-000000000000', 'u', @UserID)
	IF (0!=@@error)
	BEGIN
		RAISERROR('Error in inserting into table Usage_Event  in UnsubscribeFeature sp', 16, 1);
		GOTO ErrorHandler;
	END
COMMIT TRANSACTION
RETURN 0
ErrorHandler:
	ROLLBACK TRANSACTION @tranPoint
	rollback TRANSACTION 
	RETURN -100
