
CREATE PROCEDURE [dbo].[RetrieveSingletonDistributedStatusActiveOnly]
	@ObjId varchar(100)
as
	select *
	from singleton_tracker
	where ObjId = @ObjId and IsExisting = 1 and ( DateDiff( second, getdate(), ExpirationTime ) > 0 )

