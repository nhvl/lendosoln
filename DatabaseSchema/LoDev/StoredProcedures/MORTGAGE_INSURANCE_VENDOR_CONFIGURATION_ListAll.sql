ALTER PROCEDURE [dbo].[MORTGAGE_INSURANCE_VENDOR_CONFIGURATION_ListAll]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT 
	VendorId,
	VendorType,
	IsValid,
	ExportPath_Test,
	ExportPath_Production,
	IsUseAccountId,
	SendAuthTicketWithOrders,
	EnableConnectionTest,
	IncludesEligibilityEvalInQuote,
	UsesSplitPremiumPlans
	FROM MORTGAGE_INSURANCE_VENDOR_CONFIGURATION
	ORDER BY VendorType
END