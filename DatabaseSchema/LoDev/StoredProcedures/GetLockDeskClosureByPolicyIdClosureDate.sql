-- =============================================
-- Author:		paoloa
-- Create date: 4/18/2013
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[GetLockDeskClosureByPolicyIdClosureDate]
	-- Add the parameters for the stored procedure here
	@PolicyId uniqueidentifier,
	@BrokerId uniqueidentifier,
	@ClosureDate Datetime
AS
BEGIN
	SELECT ClosureDate, IsClosedAllDay, LockDeskOpenTime, LockDeskCloseTime, IsClosedForBusiness
	FROM LENDER_LOCK_POLICY_HOLIDAY
	WHERE LockPolicyId = @PolicyId and BrokerId = @BrokerId and ClosureDate = @ClosureDate
END
