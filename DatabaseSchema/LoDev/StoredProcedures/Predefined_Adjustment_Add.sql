-- =============================================
-- Author:		Justin Kim
-- Create date: 9/19/17
-- Description:	Add predefined adjustment to the setup page
-- =============================================
ALTER PROCEDURE [dbo].[Predefined_Adjustment_Add]
	@BrokerId uniqueidentifier,
	@Type int,
	@Description varchar(100),
	@From int,
	@To int,
	@POC bit,
	@IncludeIn1003Details int,
	@DFLP bit
AS
BEGIN
	insert into predefined_adjustment(
		[BrokerId],
		[Type],
		[Description],
		[From],
		[To],
		[POC],
		[IncludeIn1003Details],
		[DFLP]
	)
	values(
		@BrokerId,
		@Type,
		@Description,
		@From,
		@To,
		@POC,
		@IncludeIn1003Details,
		@DFLP
	)
END
