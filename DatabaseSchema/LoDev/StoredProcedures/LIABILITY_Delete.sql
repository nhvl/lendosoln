-- ============================================================
-- Author:      Generated by application LqbEntitiesToSqlTables
-- Create date: 3/19/2018
-- Description: Deletes a row from the LIABILITY table.
-- ============================================================
CREATE PROCEDURE [dbo].[LIABILITY_Delete]
	@LiabilityId UniqueIdentifier,
	@LoanId UniqueIdentifier,
	@BrokerId UniqueIdentifier
AS
BEGIN
	DELETE FROM [dbo].[LIABILITY]
	WHERE
		LiabilityId = @LiabilityId
		AND LoanId = @LoanId
		AND BrokerId = @BrokerId
END
