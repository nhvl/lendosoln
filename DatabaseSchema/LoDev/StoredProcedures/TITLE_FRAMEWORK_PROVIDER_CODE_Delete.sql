-- =============================================
-- Author:		Justin Lara
-- Create date: 8/24/2017
-- Description:	Deletes a provider code.
-- =============================================
ALTER PROCEDURE [dbo].[TITLE_FRAMEWORK_PROVIDER_CODE_Delete]
	@BrokerId uniqueidentifier,
	@AgentId uniqueidentifier
AS
BEGIN
	DELETE FROM
		TITLE_FRAMEWORK_PROVIDER_CODE
	WHERE
		BrokerId = @BrokerId
		AND AgentId = @AgentId
END
