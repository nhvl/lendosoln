



CREATE PROCEDURE [dbo].[UpdateNewLpeKeyword]
	@Keyword varchar(100), 
	@DefaultValue varchar(100),
    @Category     varchar(50)
AS
UPDATE LPE_NEW_KEYWORD
    SET Category = @Category,
	    DefaultValue = @DefaultValue
WHERE NewKeywordName = @Keyword
