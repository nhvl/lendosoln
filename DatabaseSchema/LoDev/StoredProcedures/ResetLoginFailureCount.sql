

CREATE   PROCEDURE [dbo].[ResetLoginFailureCount]
	@LoginNm varchar(36),
	@Type char(1),
	@LoginBlockExpirationD DateTime,
	@BrokerPmlSiteId uniqueidentifier = '00000000-0000-0000-0000-000000000000',
	@LastUserAgentString varchar (500) = NULL
AS
	UPDATE All_User
	SET LoginFailureCount = 0, LoginBlockExpirationD = @LoginBlockExpirationD,
		PasswordResetFailureCount = 0,
		LastUserAgentString = COALESCE(@LastUserAgentString, LastUserAgentString)
	WHERE LoginNm = @LoginNm AND Type = @Type AND BrokerPmlSiteId = @BrokerPmlSiteId
