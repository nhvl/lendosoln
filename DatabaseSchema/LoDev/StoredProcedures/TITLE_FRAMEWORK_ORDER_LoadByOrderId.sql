-- =============================================
-- Author:		Eric Mallare
-- Create date: 9/18/2017
-- Description:	Loads a single TitleOrder by order id.
-- =============================================
ALTER PROCEDURE [dbo].[TITLE_FRAMEWORK_ORDER_LoadByOrderId]
	@BrokerId uniqueidentifier,
	@LoanId uniqueidentifier,
	@OrderId int
AS
BEGIN
	SELECT
		OrderId,
		PublicId,
		BrokerId,
		LoanId,
		TransactionId,
		OrderNumber,
		[Status],
		OrderedBy,
		OrderedDate,
		IsTestOrder,
		ResultsFileDbKey,
		FeesFileDbKey,
		ConfigFileDbKey,
		LenderServiceId,
		LenderServiceName,
		TitleProviderCode,
		TitleProviderName,
		TitleProviderRolodexId,
		ProductType,
		AppliedDate
	FROM TITLE_FRAMEWORK_ORDER
	WHERE
		LoanId = @LoanId AND 
		BrokerId = @BrokerId AND
		OrderId = @orderId
END
