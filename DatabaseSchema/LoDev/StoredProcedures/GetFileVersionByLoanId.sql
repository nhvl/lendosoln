CREATE PROCEDURE [dbo].[GetFileVersionByLoanId] 
	@LoanId Guid
AS
BEGIN
	SELECT sFileVersion from Loan_File_A where sLId = @LoanId
END
