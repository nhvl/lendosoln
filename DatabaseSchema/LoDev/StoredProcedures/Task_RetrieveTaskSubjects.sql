-- =============================================
-- Author:		Antonio Valencia
-- Create date: November 18, 2011
-- Description:	Retrieves all task subjects for loanid
-- =============================================

CREATE PROCEDURE dbo.[Task_RetrieveTaskSubjects] 
	-- Add the parameters for the stored procedure here
	@BrokerId uniqueidentifier,
	@sLId uniqueidentifier,
	@ConditionsOnly bit = 1
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT TaskSubject 
	from TASK 
	where BrokerId = @BrokerId and LoanId = @sLId and TaskIsCondition = @ConditionsOnly
END
