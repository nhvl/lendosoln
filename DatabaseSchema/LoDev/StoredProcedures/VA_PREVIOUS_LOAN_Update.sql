-- ============================================================
-- Author:      Generated by application LqbEntitiesToSqlTables
-- Create date: 11/27/2018
-- Description: Updates an existing row of the VA_PREVIOUS_LOAN table.
-- ============================================================
ALTER PROCEDURE [dbo].[VA_PREVIOUS_LOAN_Update]
	@VaPreviousLoanId UniqueIdentifier,
	@LoanId UniqueIdentifier,
	@BrokerId UniqueIdentifier,
	@City VarChar(256),
	@CityState VarChar(100),
	@DateOfLoan VarChar(512),
	@IsStillOwned TinyInt,
	@LoanTypeDesc VarChar(512),
	@State Char(2),
	@Zip VarChar(10),
	@LoanDate DateTime,
	@PropertySoldDate DateTime,
	@StreetAddress VarChar(256),
	@VaLoanNumber VarChar(20)
AS
BEGIN
	UPDATE [dbo].[VA_PREVIOUS_LOAN]
	SET
		City = @City,
		CityState = @CityState,
		DateOfLoan = @DateOfLoan,
		IsStillOwned = @IsStillOwned,
		LoanTypeDesc = @LoanTypeDesc,
		State = @State,
		Zip = @Zip,
		LoanDate = @LoanDate,
		PropertySoldDate = @PropertySoldDate,
		StreetAddress = @StreetAddress,
		VaLoanNumber = @VaLoanNumber
	WHERE
		VaPreviousLoanId = @VaPreviousLoanId
		AND LoanId = @LoanId
		AND BrokerId = @BrokerId
END
