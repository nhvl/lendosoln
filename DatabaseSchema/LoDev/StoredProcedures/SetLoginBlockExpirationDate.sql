
CREATE   PROCEDURE [dbo].[SetLoginBlockExpirationDate]
	@LoginNm varchar(36),
	@Type char(1),
	@BrokerPmlSiteId uniqueidentifier = '00000000-0000-0000-0000-000000000000',
	@NextAvailableLoginTime datetime 
AS
	UPDATE All_User
	Set LoginBlockExpirationD = @NextAvailableLoginTime
	WHERE LoginNm = @LoginNm AND Type = @Type AND BrokerPmlSiteId = @BrokerPmlSiteId