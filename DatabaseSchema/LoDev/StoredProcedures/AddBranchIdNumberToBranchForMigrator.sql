-- =============================================
-- Author:		Geoff Feltman
-- Create date: 5/5/17
-- Description:	Sets the BranchIdNumber for a Branch
-- =============================================
CREATE PROCEDURE dbo.AddBranchIdNumberToBranchForMigrator
	@BrokerId uniqueidentifier,
	@BranchId uniqueidentifier,
	@BranchIdNumber int
AS
BEGIN
	UPDATE TOP(1) BRANCH
	SET BranchIdNumber = @BranchIdNumber
	WHERE BrokerId = @BrokerId and BranchId = @BranchId
END
GO
