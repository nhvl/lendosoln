-- =============================================
-- Author:		Michael Leinweaver
-- Create date: 3/8/2016
-- Description:	Saves permissions for an OC.
-- =============================================
ALTER PROCEDURE [dbo].[PMLBROKER_SavePermissions]
	@BrokerId uniqueidentifier,
	@PmlBrokerId uniqueidentifier,
	@CanApplyForIneligibleLoanPrograms bit,
	@CanRunPricingEngineWithoutCreditReport bit,
	@CanSubmitWithoutCreditReport bit,
	@CanAccessTotalScorecard bit,
	@AllowOrder4506T bit,
	@TaskRelatedEmailOptionT int
AS
BEGIN
	IF EXISTS (SELECT TOP 1 PmlBrokerId FROM PML_BROKER_PERMISSIONS WHERE BrokerId = @BrokerId AND  PmlBrokerId = @PmlBrokerId)
	BEGIN
		UPDATE PML_BROKER_PERMISSIONS
		SET
			CanApplyForIneligibleLoanPrograms = @CanApplyForIneligibleLoanPrograms,
			CanRunPricingEngineWithoutCreditReport = @CanRunPricingEngineWithoutCreditReport,
			CanSubmitWithoutCreditReport = @CanSubmitWithoutCreditReport,
			CanAccessTotalScorecard = @CanAccessTotalScorecard,
			AllowOrder4506T = @AllowOrder4506T,
			TaskRelatedEmailOptionT = @TaskRelatedEmailOptionT
		WHERE BrokerId = @BrokerId AND  PmlBrokerId = @PmlBrokerId
	END
	ELSE
	BEGIN
		INSERT INTO PML_BROKER_PERMISSIONS(BrokerId, PmlBrokerId, CanApplyForIneligibleLoanPrograms, CanRunPricingEngineWithoutCreditReport, CanSubmitWithoutCreditReport, CanAccessTotalScorecard, AllowOrder4506T, TaskRelatedEmailOptionT)
		VALUES(@BrokerId, @PmlBrokerId, @CanApplyForIneligibleLoanPrograms, @CanRunPricingEngineWithoutCreditReport, @CanSubmitWithoutCreditReport, @CanAccessTotalScorecard, @AllowOrder4506T, @TaskRelatedEmailOptionT)
	END
END
