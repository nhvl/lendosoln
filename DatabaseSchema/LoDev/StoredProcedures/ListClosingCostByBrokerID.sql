----obsolete dont use this anymore.
CREATE PROCEDURE [dbo].[ListClosingCostByBrokerID]
	@BrokerID Guid,
	@GfeVersion int = NULL
AS
	SELECT cCcTemplateId, cCcTemplateNm, GfeVersion
	FROM CC_Template
	WHERE BrokerID = @BrokerID
      AND GfeVersion = COALESCE(@GfeVersion, GfeVersion)
      AND CCTemplateSystemId IS NULL
	ORDER BY cCcTemplateNm
