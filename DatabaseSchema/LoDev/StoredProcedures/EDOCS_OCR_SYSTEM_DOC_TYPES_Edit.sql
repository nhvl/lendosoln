-- =============================================
-- Author:		Michael Leinweaver
-- Create date: 9/8/2017
-- Description:	Edits a system edoc classification.
-- =============================================
CREATE PROCEDURE [dbo].[EDOCS_OCR_SYSTEM_DOC_TYPES_Edit]
	@Id int,
	@NewName varchar(100)
AS
BEGIN
	UPDATE TOP(1) EDOCS_OCR_SYSTEM_DOC_TYPES
	SET Name = @NewName
	WHERE Id = @Id
END
