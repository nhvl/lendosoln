CREATE PROCEDURE INTERNAL_USE_ONLY_RemoveOldInvalidLoans
	@cutOffDate smalldatetime
as
	delete from loan_file_a
	where IsValid = 0 and DeletedD < @cutOffDate
