CREATE PROCEDURE INTERNAL_USE_ONLY_ListAllLoanId
AS
	SELECT
		c.sLId ,
		c.sLNm ,
		c.IsMarkedInvalidated
	FROM
		Loan_File_Cache AS c
	WHERE
		c.IsValid = 1
