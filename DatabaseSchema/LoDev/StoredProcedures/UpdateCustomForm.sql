

CREATE PROCEDURE [dbo].[UpdateCustomForm] 
	@CustomLetterID uniqueidentifier,
	@BrokerID uniqueidentifier,
	@Title varchar(36) = NULL,
	@FileDBKey uniqueidentifier = NULL,
	@OwnerEmployeeId uniqueidentifier = NULL
AS
IF(@OwnerEmployeeId = '00000000-0000-0000-0000-000000000000')
	BEGIN	
		UPDATE Custom_Letter
			SET Title = COALESCE(@Title, title),
			FileDBKey = COALESCE(@FileDBKey, FileDBKey),
			OwnerEmployeeId = NULL
			WHERE CustomLetterID = @CustomLetterID AND BrokerId = @BrokerID
		END
	ELSE
		BEGIN
			UPDATE Custom_Letter
			SET Title = COALESCE(@Title, title),
			FileDBKey = COALESCE(@FileDBKey, FileDBKey),
			OwnerEmployeeId = COALESCE(@OwnerEmployeeId, OwnerEmployeeId)
			WHERE CustomLetterID = @CustomLetterID AND BrokerId = @BrokerID
		END
	


