
CREATE PROCEDURE [dbo].[InitLoanFileTeamAssignments]
	@LoanID GUID,
	@SrcLoanIdForAssignmentDuplication uniqueidentifier = null,
	@EmployeeID uniqueidentifier = null 
AS
	IF @SrcLoanIdForAssignmentDuplication is not null
	BEGIN
		-- Copying assignment from source loan
		INSERT INTO Team_Loan_Assignment with(rowlock)(LoanId, RoleId, TeamId) 
		SELECT @LoanID, RoleID, TeamId 
		FROM Team_Loan_Assignment with(updlock,rowlock)
		WHERE LoanId = @SrcLoanIdForAssignmentDuplication
	END
	ELSE 
	BEGIN
		-- copy initial assignments from user 
		INSERT Team_Loan_Assignment (LoanId, RoleId, TeamId) 
		SELECT @LoanID, RoleID, TeamId
		FROM team_user_assignment t join TEAM t1 on t.TeamId = t1.id 
		WHERE @EmployeeID = EmployeeId  and IsPrimary = 1 
	END

