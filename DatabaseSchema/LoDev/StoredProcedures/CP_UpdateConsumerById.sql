

-- =============================================
-- Author:		<Francesc>
-- Create date: <04/29/10>
-- Description:	<Retrieve loans associated with a given consumerId>
-- =============================================
CREATE PROCEDURE [dbo].[CP_UpdateConsumerById]
	@ConsumerId bigint,
	@PasswordHash varchar(200) = null,
	@LastLoggedInD datetime = null,
	@PasswordRequestD datetime = null,
	@PasswordResetCode varchar(200) = null,
	@IsTemporaryPassword bit = null,
	@LoginFailureNumber int = null,
	@IsLocked bit = null
AS
BEGIN
SET NOCOUNT ON
	UPDATE CP_CONSUMER
	SET
		PasswordHash =		 COALESCE(@PasswordHash, PasswordHash),
		LastLoggedInD =		 COALESCE(@LastLoggedInD, LastLoggedInD),
		PasswordRequestD =	 COALESCE(@PasswordRequestD, PasswordRequestD),
		PasswordResetCode =  COALESCE(@PasswordResetCode, PasswordResetCode),
		IsTemporaryPassword =COALESCE(@IsTemporaryPassword, IsTemporaryPassword),
		LoginFailureNumber = COALESCE(@LoginFailureNumber,LoginFailureNumber),
		IsLocked =			 COALESCE(@IsLocked ,IsLocked)
	WHERE ConsumerId = @ConsumerId
END