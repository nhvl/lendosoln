-- =============================================
-- Author:		Eduardo Michel
-- Create date: 11/10/14
-- Description:	Returns the task with the corresponding task id only if its in the broker and the loan has the correct branch channel and correspondentprocess.
-- =============================================
ALTER PROCEDURE [dbo].[TASK_RetrieveTaskByBrokerIdTaskIdPortalMode] 

	@BrokerId	uniqueidentifier, 
	@TaskId	varchar(10),
	@sBranchChannelT int,
	@sCorrespondentProcessT int,
	@AllowBlankChannel bit = 0,
	@IsCorrespondent bit = 0,
	@DelegatedProcessT int,
	@PriorProcessT int 

AS

--REVIEW
--6/9/2011: Reviewed. ThinhNK 
--7/5/2012: Added CondRowId. -MP

	SELECT
		t.BrokerId
		, TaskId
		, TaskAssignedUserId
		--, TaskIdentityId
		, TaskStatus
		, LoanId
		, TaskOwnerUserId
		, TaskCreatedDate
		, TaskSubject
		, TaskDueDate
		, TaskFollowUpDate
		, CondInternalNotes
		, TaskIsCondition
		, TaskResolvedUserId
		, TaskLastModifiedDate
		, BorrowerNmCached
		, LoanNumCached
		, TaskToBeAssignedRoleId
		, TaskToBeOwnerRoleId
		, TaskCreatedByUserId
		, TaskDueDateLocked
		, TaskDueDateCalcField
		, TaskDueDateCalcDays
		, TaskHistoryXml
		, TaskPermissionLevelId
		, TaskClosedUserId
		, TaskClosedDate
		, TaskRowVersion
		, CondCategoryId
		, CondIsHidden
		--, CondRank
		, CondRowId
		, CondDeletedDate
		, CondDeletedByUserNm
		, CondIsDeleted
		--, assigned.UserFirstNm + ' ' + assigned.UserLastNm as AssignedUserFullName
		--, owned.UserFirstNm + ' ' + owned.UserLastNm as OwnerFullName
		--, CASE WHEN TaskClosedUserId IS NULL THEN '' ELSE closed.UserFirstNm + ' ' + closed.UserLastNm END as ClosingUserFullName
		, TaskAssignedUserFullName
		, TaskOwnerFullName
		, TaskClosedUserFullName
		, CondMessageId
		, CondRequiredDocTypeId
		, ResolutionBlockTriggerName
		, ResolutionDenialMessage
		, ResolutionDateSetterFieldId
		, AssignToOnReactivationUserId

	FROM TASK t
		join loan_file_cache_2 as lc2 on (t.LoanId = lc2.sLId)
		join loan_file_cache_3 as lc3 on (t.LoanId = lc3.sLId)
	WHERE 
	t.BrokerId = @BrokerId
	AND TaskId = @TaskId
	AND ((lc2.sBranchChannelT = @sBranchChannelT) OR (lc2.sBranchChannelT = 0 AND @AllowBlankChannel = 1))
	AND (@sCorrespondentProcessT = -1 OR lc3.sCorrespondentProcessT = @sCorrespondentProcessT OR (@Iscorrespondent = 1 AND (lc3.sCorrespondentProcessT = @DelegatedProcessT OR lc3.sCorrespondentProcessT = @PriorProcessT)))
