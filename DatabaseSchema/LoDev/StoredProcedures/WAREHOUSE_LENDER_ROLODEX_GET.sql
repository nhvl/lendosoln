-- =============================================
-- Author:		paoloa
-- Create date: 7/12/2013
-- Description:	
-- =============================================
ALTER PROCEDURE [dbo].[WAREHOUSE_LENDER_ROLODEX_GET]
	-- Add the parameters for the stored procedure here
	@BrokerId uniqueidentifier,
	@Id int  = null,
	@Status int = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	IF @Id IS NULL
	BEGIN
	SELECT WarehouseLenderRolodexId, BrokerId, WarehouseLenderName, [Status], FannieMaePayeeId, FreddieMacPayeeId, MersOrganizationId, PayToBankName, PayToCity, PayToState, PayToABANum, PayToAccountName, PayToAccountNum, FurtherCreditToAccountName, FurtherCreditToAccountNum, MainCompanyName, MainContactName, MainAttention, MainEmail, MainAddress, MainCity, MainState, MainZip, MainPhone, MainFax, CollateralPackageToAttention, CollateralPackageToContactName, CollateralPackageToUseMainAddress, CollateralPackageToEmail, CollateralPackageToAddress, CollateralPackageToCity, CollateralPackageToState, CollateralPackageToZip, CollateralPackageToPhone, CollateralPackageToFax, ClosingPackageToAttention, ClosingPackageToContactName, ClosingPackageToUseMainAddress, ClosingPackageToEmail, ClosingPackageToAddress, ClosingPackageToCity, ClosingPackageToState, ClosingPackageToZip, ClosingPackageToPhone, ClosingPackageToFax, AdvanceClassificationsXmlContent, WarehouseLenderFannieMaeId, WarehouseLenderFreddieMacId
	FROM WAREHOUSE_LENDER_ROLODEX
	WHERE BrokerId = @BrokerId AND @Status is null OR @Status = [Status]
	END
	
	ELSE BEGIN
	SELECT WarehouseLenderRolodexId, BrokerId, WarehouseLenderName, [Status], FannieMaePayeeId, FreddieMacPayeeId, MersOrganizationId, PayToBankName, PayToCity, PayToState, PayToABANum, PayToAccountName, PayToAccountNum, FurtherCreditToAccountName, FurtherCreditToAccountNum, MainCompanyName, MainContactName, MainAttention, MainEmail, MainAddress, MainCity, MainState, MainZip, MainPhone, MainFax, CollateralPackageToAttention, CollateralPackageToContactName, CollateralPackageToUseMainAddress, CollateralPackageToEmail, CollateralPackageToAddress, CollateralPackageToCity, CollateralPackageToState, CollateralPackageToZip, CollateralPackageToPhone, CollateralPackageToFax, ClosingPackageToAttention, ClosingPackageToContactName, ClosingPackageToUseMainAddress, ClosingPackageToEmail, ClosingPackageToAddress, ClosingPackageToCity, ClosingPackageToState, ClosingPackageToZip, ClosingPackageToPhone, ClosingPackageToFax, AdvanceClassificationsXmlContent, WarehouseLenderFannieMaeId, WarehouseLenderFreddieMacId
	FROM WAREHOUSE_LENDER_ROLODEX
	WHERE BrokerId = @BrokerId AND WarehouseLenderRolodexId = @Id
	END
		
END