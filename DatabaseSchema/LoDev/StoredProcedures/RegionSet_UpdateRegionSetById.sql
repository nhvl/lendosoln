CREATE PROCEDURE [dbo].[RegionSet_UpdateRegionSetById]
	@BrokerId uniqueidentifier,
	@RegionSetId int,
	@Name varchar(50),
	@Description varchar(100)
AS
BEGIN
	UPDATE REGION_SET
	SET Name = @Name,
		[Description] = @Description
	WHERE BrokerId = @BrokerId
		AND RegionSetId = @RegionSetId
END