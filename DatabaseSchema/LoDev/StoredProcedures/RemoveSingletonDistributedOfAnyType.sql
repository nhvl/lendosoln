CREATE PROCEDURE RemoveSingletonDistributedOfAnyType
	@ObjId varchar(100)
as
-- NOTE THAT THIS STORE PROC MUST OPERATE ATOMICALLY
declare @now datetime
set @now = getdate()
	update singleton_tracker
	set IsExisting = 0
	where ObjId = @ObjId and IsExisting = 1 and DateDiff( second, @now, ExpirationTime  ) > 0 
