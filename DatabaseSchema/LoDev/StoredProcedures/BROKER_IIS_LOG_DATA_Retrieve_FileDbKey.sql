-- =============================================
-- Author:		Thien Nguyen
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[BROKER_IIS_LOG_DATA_Retrieve_FileDbKey]
	@BrokerId UniqueIdentifier,
	@ReportedDate datetime

AS
BEGIN

	SELECT top 1 FileDbKey
	FROM BROKER_IIS_LOG_DATA
	WHERE BrokerId = @BrokerId AND ReportedDate=@ReportedDate 
	ORDER BY GeneratedDate desc
	
END
