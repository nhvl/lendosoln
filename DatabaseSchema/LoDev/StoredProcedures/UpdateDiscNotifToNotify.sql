CREATE PROCEDURE UpdateDiscNotifToNotify
	@NotifId Guid , @NotifUpdatedDate DateTime = NULL
AS
	UPDATE Discussion_Notification
	SET
		NotifIsRead = 0,
		NotifUpdatedDate = COALESCE( @NotifUpdatedDate , getdate() ),
		NotifIsValid = 1,
		NotifInvalidDate = null
	WHERE
		NotifId = @NotifId
