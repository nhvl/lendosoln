




-- =============================================
-- Author:		Andrew Hatfield
-- Create date: July 30, 2008
-- Description:	Takes in a LoanId and returns information about it's current status/owner
-- =============================================
CREATE PROCEDURE [dbo].[FindLoanInfoByLoanId] 
	@LoanId varchar(36)
	
AS
BEGIN
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
			cache.sLNm,
			b.BrokerNm,
			b.BrokerId,
			b.CustomerCode,
			
			lfa.DeletedD,
			lfa.IsValid,
			lfe.sOldLNm,
		
			-- Loan Status
			dbo.GetLoanStatusName(cache.sStatusT) as LoanStatus,

			-- Property Address
			cache.sspAddr + ', ' + cache.sspCity + ', ' + cache.sspState + ', ' + cache.sspZip as sSpFullAddr,

			-- Name of the borrower
			cache.sPrimBorrowerFullNm, 

			br.BranchNm,
			b.Status
			
	FROM
			loan_file_cache as cache,
			loan_file_a as lfa,
			loan_file_e as lfe,
			broker as b,
			branch as br

	WHERE

			cache.slid = @LoanId and
			cache.slid = lfa.slid and
			cache.slid = lfe.slid and
			cache.sBrokerId = b.BrokerId and
			cache.sBranchId = br.BranchId
END





