-- =============================================
-- Author:		Antonio Valencia
-- Create date: 4/23/2013
-- Description:	Creates a Title Vendor broker association Record
-- =============================================
CREATE PROCEDURE [dbo].[TITLE_VENDOR_X_BROKER_Update] 
	-- Add the parameters for the stored procedure here
	@BrokerId uniqueidentifier,
	@TitleVendorId int,
	@Login varchar(255),
	@Password varchar(255) = null,
	@ClientCode varchar(255),
	@LoginTitle varchar(255),
	@ClientCodeTitle varchar(255), 
	@PasswordTitle varchar(255)	 = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	
	SET NOCOUNT ON;
	

		UPDATE TOP(1) TITLE_VENDOR_X_BROKER SET 
		[Login] = @Login, 
		[ClientCode] = @ClientCode,
		[Password] = COALESCE(@Password,Password),
		PasswordTitle = COALESCE(@PasswordTitle,PasswordTitle),
		ClientCodeTitle = @ClientCodeTitle,
		LoginTitle = @LoginTitle
		WHERE TitleVendorId = @TitleVendorId AND BrokerId = @BrokerID
		
		IF @@rowcount = 0 begin
			IF @Password is null or @PasswordTitle is null 
			begin
			RAISERROR('Passwords cannot be empty or null', 16, 1); 
			end
		INSERT TITLE_VENDOR_X_BROKER([BrokerId], [TitleVendorId], [Login], [Password],[ClientCode],  LoginTitle, ClientCodeTitle, PasswordTitle	)
		VALUES(@BrokerId, @TitleVendorId, @Login, @Password, @ClientCode,  @LoginTitle, @ClientCodeTitle, @PasswordTitle	)

		end
	
	
END
