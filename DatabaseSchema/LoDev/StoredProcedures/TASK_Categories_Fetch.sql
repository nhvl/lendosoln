-- =============================================
-- Author:		Antonio Valencia
-- Create date: 4/12/11
-- Description:	Retrieves List oF conditions for broker
-- =============================================
CREATE PROCEDURE [dbo].[TASK_Categories_Fetch] 
	-- Add the parameters for the stored procedure here
	@BrokerId uniqueidentifier, 
	@Id int = null
AS
BEGIN
	SELECT ConditionCategoryId,BrokerId,Category,DefaultTaskPermissionLevelId,IsDisplayAtTop 
	FROM CONDITION_CATEGORY
	WHERE BrokerId = @BrokerId AND ConditionCategoryId = COALESCE(@Id, ConditionCategoryId)
END
