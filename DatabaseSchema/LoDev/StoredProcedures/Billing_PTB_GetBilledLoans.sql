
ALTER PROCEDURE [dbo].[Billing_PTB_GetBilledLoans]
	@StartDate datetime,
@EndDate datetime,
@BrokerId uniqueidentifier = null
AS
BEGIN


select broker.CustomerCode as CustomerCode, ptb.sLId as LoanId, lfc.sLNm as LoanNumber, lfc.aBNm as BorrowerName, lfc.sAgentLoanOfficerName as LoanOfficer,
	   branch.BranchNm as BranchName, branch.BranchCode as BranchCode, ptb.BillingType as BillingType, ptb.BillingDate as BillingDate
	   
from   BILLING_PER_TRANSACTION ptb join LOAN_FILE_CACHE lfc on lfc.sLId = ptb.sLId
								   join BRANCH branch on branch.BranchId = lfc.sBranchId
								   join Broker broker on broker.BrokerId = ptb.BrokerId
								 
where  ptb.BrokerId = COALESCE(@BrokerId, ptb.BrokerId) AND 
	broker.SuiteType = 1 AND
	   (@StartDate <= BillingDate AND BillingDate < @EndDate)

END
