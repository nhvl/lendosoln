




CREATE     PROCEDURE [dbo].[ListUsersAssociatedWithBranch]
	@BrokerId uniqueidentifier, 
	@BranchId uniqueidentifier
AS
	SELECT TOP 100
		v.UserLastNm + ', ' + v.UserFirstNm AS UserName ,
		a.LoginNm ,
		CASE a.Type WHEN 'P' THEN 'PML User' ELSE 'Employee' END AS Type,
		CASE v.IsActive WHEN 0 THEN 'No' WHEN 1 THEN 'Yes' END  AS IsActive,
		v.EmployeeId ,
		l.LicenseNumber
	FROM View_lendersoffice_Employee AS v 
	join broker_user as u on v.employeeid = u.employeeid
	join all_user as a on u.userid = a.userid
	join license as l on l.licenseid = u.currentlicenseid


	WHERE v.BrokerId = @BrokerId AND 
	(v.BranchId = @BranchId OR v.MiniCorrespondentBranchId = @BranchId OR v.CorrespondentBranchId = @BranchId)





