-- =============================================
-- Author:		<Jason Kudlinski>
-- Create date: <12-4-08>
-- =============================================
CREATE PROCEDURE GetRSWarningMessageById
	-- Add the parameters for the stored procedure here
	@Broker varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT BrokerId, NormalUserExpiredCustomMsg, LockDeskExpiredCustomMsg, NormalUserCutOffCustomMsg, LockDeskCutOffCustomMsg, OutsideNormalHrCustomMsg, OutsideClosureDayHrCustomMsg, OutsideNormalHrAndPassInvestorCutOffCustomMsg
		from RS_Expiration_Custom_Warning_Msgs
		where BrokerId = @Broker;
END
