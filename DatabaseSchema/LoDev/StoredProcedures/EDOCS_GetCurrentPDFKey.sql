CREATE PROCEDURE [dbo].[EDOCS_GetCurrentPDFKey]
	@DocId uniqueidentifier,
	@BrokerId uniqueidentifier
as
	SELECT FileDBKey_CurrentRevision 
	FROM EDOCS_DOCUMENT where documentid = @DocId AND @BrokerId = @BrokerId