-- =============================================
-- Author:		Isaac Ribakoff
-- Create date: 9/19/2014
-- Description:	Saves MI Vendor-Broker Entry,
--              Sets whether to use production or test environment
-- =============================================
ALTER PROCEDURE [dbo].[MORTGAGE_INSURANCE_VENDOR_BROKER_SETTINGS_SetEntry]
	@VendorId uniqueidentifier,
	@BrokerId uniqueidentifier,
	@IsProduction bit
AS
BEGIN
	UPDATE MORTGAGE_INSURANCE_VENDOR_BROKER_SETTINGS
	SET IsProduction = @IsProduction
	WHERE VendorId = @VendorId
		AND BrokerId = @BrokerId
	
	IF @@rowcount = 0
	BEGIN
		INSERT INTO MORTGAGE_INSURANCE_VENDOR_BROKER_SETTINGS
		(
			VendorId,
			BrokerId,
			IsProduction,
			EncryptionKeyId
		)
		VALUES
		(
			@VendorId,
			@BrokerId,
			@IsProduction,
			'00000000-0000-0000-0000-000000000000' -- nothing to encrypt yet, so we won't make the key yet
		)
	END
END
