-- =============================================
-- Author:		Justin Kim
-- Create date: 7/11/17
-- Description:	Get all tracked feature featureIds.
-- =============================================
ALTER PROCEDURE [dbo].[GetAllTrackedFeatureIds]

AS
BEGIN
	SET NOCOUNT ON;

	SELECT FeatureId
	FROM TRACKED_FEATURE
END
