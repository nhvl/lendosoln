CREATE  PROCEDURE [dbo].[LoanStatisticsByBroker] 
	@BrokerID GUID,
	@sStatusD DateTime = NULL,
	@LimitD DateTime = NULL
AS
	SELECT l.sStatusT,
	        count(l.sStatusT ) AS count
	FROM LOAN_FILE_CACHE l with(nolock)
	WHERE sBrokerID = @BrokerID 
	     AND l.IsValid = 1
	     AND l.IsTemplate = 0
	    AND (sStatusD >= COALESCE(@sStatusD, sStatusD) AND sStatusD <= COALESCE(@LimitD, sStatusD) OR (@sStatusD IS NULL AND sStatusD IS NULL))
	GROUP BY sStatusT
	if( 0!=@@error)
	begin
		RAISERROR('Error in the select statement in LoanStatisticsByBroker sp', 16, 1);
		return -100;
	end
	
	return 0;
