-- =============================================
-- Author:		Thien Nguyen
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[GetAuditHistoryMigratedByLoanId]
	@LoanId Guid
AS
BEGIN
	SELECT sIsAuditHistoryMigrated
    FROM Loan_File_A
    WHERE sLId = @LoanId
END
