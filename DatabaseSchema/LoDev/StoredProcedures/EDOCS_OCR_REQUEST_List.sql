-- =============================================
-- Author:		Michael Leinweaver
-- Create date: 9/13/2017
-- Description:	Retrieves OCR requests.
-- =============================================
ALTER PROCEDURE [dbo].[EDOCS_OCR_REQUEST_List]
	@BrokerId uniqueidentifier,
	@LoanId uniqueidentifier = NULL
AS
BEGIN
	SELECT 
		ocr.OcrRequestId, 
		ocr.LoanId, 
		ocr.AppId, 
		ocr.Origin, 
		ocr.DocumentSource,
		ocr.UploadingUserId,
		ocr.UploadingUserType, 
		ocr.LatestStatusDate, 
		ocr.LatestStatus,
		lfc.sLNm
	FROM 
		EDOCS_OCR_REQUEST ocr
		JOIN LOAN_FILE_CACHE lfc ON ocr.LoanId = lfc.sLId
	WHERE 
		BrokerId = @BrokerId AND 
		(@LoanId IS NULL OR LoanId = @LoanId)
END
