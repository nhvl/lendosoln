CREATE PROCEDURE RetrieveBranchNameById
	@BranchID uniqueidentifier, @BrokerID uniqueidentifier
AS
	SELECT
	BranchNm
FROM
	Branch
WHERE
	BranchID = @BranchID AND BrokerID = @BrokerID
