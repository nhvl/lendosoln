-- =============================================
-- Author:		Michael Leinweaver
-- Create date: 10/25/2017
-- Description:	Retrieves a TPO portal configuration
--				by branch ID.
-- =============================================
ALTER PROCEDURE [dbo].[TPOPortalConfig_RetrieveByBranchId]
	@BrokerId uniqueidentifier,
	@BranchId uniqueidentifier
AS
BEGIN
	DECLARE @RetailTpoLandingPageId uniqueidentifier
	
	SELECT @RetailTpoLandingPageId = RetailTpoLandingPageId
	FROM BRANCH
	WHERE BrokerId = @BrokerId AND BranchId = @BranchId
	
	IF @@rowcount <> 0 AND @RetailTpoLandingPageId IS NOT NULL
	BEGIN
		SELECT Id, BrokerId, Name, Url, IsDeleted
		FROM Lender_TPO_LandingPageConfig
		WHERE 
			ID = @RetailTpoLandingPageId AND 
			IsDeleted = 0 AND
			BrokerID = @BrokerId
	END
END
