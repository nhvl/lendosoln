-- =============================================
-- Author:		Thien Nguyen
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[EDOCS_IDS_DOCTYPE_Update] 
    @Id int,
    @Description varchar(200),
    @BarcodeClassification varchar(5)
AS
BEGIN

	UPDATE EDOCS_IDS_DOCTYPE
	SET	  Description = @Description, BarcodeClassification = @BarcodeClassification
    WHERE Id = @Id
		  
END
