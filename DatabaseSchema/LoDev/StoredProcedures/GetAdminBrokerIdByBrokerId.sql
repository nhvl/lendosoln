CREATE PROCEDURE GetAdminBrokerIdByBrokerId
	@BrokerId uniqueidentifier
AS
	SELECT UserId
	FROM VIEW_ACTIVE_LO_USER
	WHERE IsAccountOwner = 1 AND BrokerId = @BrokerId
