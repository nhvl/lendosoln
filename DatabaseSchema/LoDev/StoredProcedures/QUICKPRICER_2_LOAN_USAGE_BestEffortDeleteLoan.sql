-- =============================================
-- Author:		Scott Kibler
-- Create date: 8/6/2014
-- Description:	
--   Deletes the loan *iff it is a qp2.0 loan*, and tables associated with that loan.
--   ERRORS IF: sLoanFileT != quickpricer2sandboxed, and other reasons..
--		 see QUICKPRICER_2_LOAN_USAGE_CanDeleteLoan logic.
--   RETURNS: a list of fileDBGuids associated with the loan
--		 see QUICKPRICER_2_LOAN_USAGE_CanDeleteLoan
--   DOES NOT: delete stuff from file db associated with the loan.
--   TODO: make it so only certain DB users can execute this sproc.
--   TODO: possibly add a table of which user deleted which loan.
-- =============================================
ALTER PROCEDURE [dbo].[QUICKPRICER_2_LOAN_USAGE_BestEffortDeleteLoan]
	@BrokerID UniqueIdentifier,
	@LoanID UniqueIdentifier,
	--@UserIDofDeletingUser UniqueIdentifier, -- // TODO: possibly log the user who deletes.
	@BrokerPMLTemplateID UniqueIdentifier, -- shouldn't match the loanid.
	@BrokerQuickPricerTemplateID UniqueIdentifier, -- shouldn't match the loanid.
	@IWillDeleteFromTransient bit,
	@IWillDeleteAllHardCodedFileDBFiles bit, -- like slid_approval.pdf, etc.
	@IWillDeleteAllConditionsFromFileDB bit,
	--@TransientFhaConnectionLogHasNoEntry bit,	-- no longer in use
	@QuickPricer2InUseLoanExpirationDays tinyint = 2 -- must be 2 or greater.  from conststage ideally.
AS
BEGIN
	DECLARE @Error int		-- to come from  @@Error
	DECLARE @RowCount int   -- to come from  @@RowCount
	DECLARE @ErrorCode int
	DECLARE @ErrorMsg varchar(1000)
	DECLARE @ReasonCantDeleteLoan varchar(1000)
	DECLARE @CanDeleteLoan bit
	SET @CanDeleteLoan = 0;
	SET @ReasonCantDeleteLoan = 'Reason not determined.';
	SET @ErrorCode = -1;
	SET @ErrorMsg = 'Unexpected Error';
	
	print 'entering deletion'
	
	EXEC 
	QUICKPRICER_2_LOAN_USAGE_CanDeleteLoan 
	@BrokerID=@BrokerID,
	@LoanID=@LoanID, 
	@BrokerPMLTemplateID=@BrokerPMLTemplateID,
	@BrokerQuickPricerTemplateID=@BrokerQuickPricerTemplateID,
	@QuickPricer2InUseLoanExpirationDays = @QuickPricer2InUseLoanExpirationDays,
	--@TransientFhaConnectionLogHasNoEntry = @TransientFhaConnectionLogHasNoEntry,
	@IWillDeleteFromTransient = @IWillDeleteFromTransient,
	@IWillDeleteAllConditionsFromFileDB = @IWillDeleteAllConditionsFromFileDB,
	@IWillDeleteAllHardCodedFileDBFiles = @IWillDeleteAllHardCodedFileDBFiles, -- e.g. pin states, gfe archive, etc.
	@CanDeleteLoan=@CanDeleteLoan OUTPUT,
	@ReasonCantDeleteLoan = @ReasonCantDeleteLoan OUTPUT
	
	PRINT 'EXECUTED CAN DELETE LOAN.'
	
	IF @@Error <> 0
	BEGIN
		print 'Failed executing CanDeleteLoan.'
		SET @ErrorMsg = 'Failed executing CanDeleteLoan'
		GOTO NonTranErrorHandler;
	END	
	IF @ReasonCantDeleteLoan <> ''
	BEGIN
		SET @ErrorMsg = @ReasonCantDeleteLoan
		GOTO NonTranErrorHandler;
	END
	IF @CanDeleteLoan = 0
	BEGIN
		SET @ErrorMsg = 'Cant delete loan, unknown reason.';
		GOTO NonTranErrorHandler;
	END
	
	print 'starting the deletion transaction'
	
	BEGIN TRANSACTION -- Deletion Transaction
		DELETE FROM STATUS_EVENT
		WHERE LoanId = @LoanId AND BrokerId = @BrokerId
		SELECT @RowCount=@@RowCount, @Error = @@Error
		IF(0 <> @Error) -- OR 0 = @RowCount)
		BEGIN
			SET @ErrorCode = -53
			SET @ErrorMsg = 'Unable to delete from StatusEvents'
			GOTO ErrorHandler;
		END
		
		DELETE 
		FROM APPLICATION_B
		WHERE 
			sLId = @LoanID
		SELECT @RowCount=@@RowCount, @Error = @@Error
		IF(0 <> @Error) -- OR 0 = @RowCount)
		BEGIN
			SET @ErrorCode = -53
			SET @ErrorMsg = 'Unable to delete from application_b'
			GOTO ErrorHandler;
		END
		
		DELETE 
		FROM APPLICATION_A
		WHERE 
			sLId = @LoanID
		SELECT @RowCount=@@RowCount, @Error = @@Error
		IF(0 <> @Error) --OR 0 = @RowCount)
		BEGIN
			SET @ErrorCode = -53
			SET @ErrorMsg = 'Unable to delete from application_a'
			GOTO ErrorHandler;
		END
		
		DELETE
		FROM RATE_MONITOR
		WHERE
			sLId = @LoanID
		SELECT @RowCount=@@RowCount, @Error = @@Error
		IF(0 <> @Error) -- OR 1 <> @RowCount)
		BEGIN
			SET @ErrorCode = -53
			SET @ErrorMsg = 'Unable to delete from RATE_MONITOR'
			GOTO ErrorHandler;
		END		

		
		DELETE 
		FROM AUDIT_TRAIL_DAILY
		WHERE
			    LoanID = @LoanID
			AND FileDBKey = ''
		IF(0 <> @@Error)
		BEGIN
			SET @ErrorCode = -53
			SET @ErrorMsg = 'Unable to delete from audit_trail'
			GOTO ErrorHandler;
		END
		
		-- TODO: eventually delete from audit_trail_daily where there *is* a filedbkey.
		
		--DELETE TOP (1)
		--FROM AUDIT_TRAIL_DAILY
		--WHERE
		--	    LoanID = @LoanID
		--  AND FileDBKey <> ''
		--IF(0 <> @@Error)
		--BEGIN
		--	SET @ErrorCode = -53
		--	SET @ErrorMsg = 'Unable to delete from audit_trail_daily'
		--	GOTO ErrorHandler;
		--END
		
		DELETE TOP (1)
		FROM LOAN_FILE_B
		WHERE
			sLId = @LoanID
		SELECT @RowCount=@@RowCount, @Error = @@Error
		IF(0 <> @Error) --OR 1 <> @RowCount)
		BEGIN
			SET @ErrorCode = -53
			SET @ErrorMsg = 'Unable to delete from loan_file_b'
			GOTO ErrorHandler;
		END
		
		DELETE TOP (1)
		FROM LOAN_FILE_C
		WHERE
			sLId = @LoanID
		SELECT @RowCount=@@RowCount, @Error = @@Error
		IF(0 <> @Error) -- OR 1 <> @RowCount)
		BEGIN
			SET @ErrorCode = -53
			SET @ErrorMsg = 'Unable to delete from loan_file_c'
			GOTO ErrorHandler;
		END
		
		DELETE
		FROM CONDITION
		WHERE
			LoanID = @LoanID
		IF(0 <> @@Error)
		BEGIN
			SET @ErrorCode = -53
			SET @ErrorMsg = 'Unable to delete from condition'
			GOTO ErrorHandler;
		END
		
		DELETE TOP (1)
		FROM LOAN_FILE_ACTIVITY
		WHERE
			sLId = @LoanID
		SELECT @RowCount=@@RowCount, @Error = @@Error
		IF(0 <> @Error)
		BEGIN
			SET @ErrorCode = -53
			SET @ErrorMsg = 'Unable to delete from LOAN_FILE_ACTIVITY'
			GOTO ErrorHandler;
		END
		
		DELETE TOP (1)
		FROM QUICKPRICER_2_LOAN_USAGE
		WHERE
			LoanID = @LoanID
		SELECT @RowCount=@@RowCount, @Error = @@Error
		IF(0 <> @Error) -- allow skipping deletion if its sCreatedD is sufficiently old.
		BEGIN
			SET @ErrorCode = -53
			SET @ErrorMsg = 'Unable to delete from QUICKPRICER_2_LOAN_USAGE'
			GOTO ErrorHandler;
		END
		
		DELETE
		FROM TASK
		WHERE
			LoanID = @LoanID
		IF(0 <> @@Error)
		BEGIN
			SET @ErrorCode = -53
			SET @ErrorMsg = 'Unable to delete from Task'
			GOTO ErrorHandler;
		END
		
		DELETE TOP (1)
		FROM LOAN_FILE_CACHE
		WHERE
			sLId = @LoanID
		SELECT @RowCount=@@RowCount, @Error = @@Error
		IF(0 <> @Error) -- OR 1 <> @RowCount)
		BEGIN
			SET @ErrorCode = -53
			SET @ErrorMsg = 'Unable to delete from LOAN_FILE_CACHE'
			GOTO ErrorHandler;
		END
		
		DELETE TOP (1)
		FROM LOAN_FILE_CACHE_2
		WHERE
			sLId = @LoanID
		SELECT @RowCount=@@RowCount, @Error = @@Error
		IF(0 <> @Error) -- OR 1 <> @RowCount)
		BEGIN
			SET @ErrorCode = -53
			SET @ErrorMsg = 'Unable to delete from LOAN_FILE_CACHE_2'
			GOTO ErrorHandler;
		END
		
		DELETE TOP (1)
		FROM LOAN_FILE_D
		WHERE
			sLId = @LoanID
		SELECT @RowCount=@@RowCount, @Error = @@Error
		IF(0 <> @Error) -- OR 1 <> @RowCount)
		BEGIN
			SET @ErrorCode = -53
			SET @ErrorMsg = 'Unable to delete from LOAN_FILE_D'
			GOTO ErrorHandler;
		END
		
		DELETE TOP (1)
		FROM LOAN_FILE_E
		WHERE
			sLId = @LoanID
		SELECT @RowCount=@@RowCount, @Error = @@Error
		IF(0 <> @Error) -- OR 1 <> @RowCount)
		BEGIN
			SET @ErrorCode = -53
			SET @ErrorMsg = 'Unable to delete from LOAN_FILE_E'
			GOTO ErrorHandler;
		END
		
		DELETE 
		FROM INTEGRATION_FILE_MODIFIED
		WHERE
			sLId = @LoanID
		SELECT @RowCount=@@RowCount, @Error = @@Error
		IF(0 <> @Error) -- OR 1 <> @RowCount)
		BEGIN
			SET @ErrorCode = -53
			SET @ErrorMsg = 'Unable to delete from INTEGRATION_FILE_MODIFIED'
			GOTO ErrorHandler;
		END
		
		DELETE TOP (1)
		FROM LOAN_FILE_F
		WHERE
			sLId = @LoanID
		SELECT @RowCount=@@RowCount, @Error = @@Error
		IF(0 <> @Error) -- OR 1 <> @RowCount)
		BEGIN
			SET @ErrorCode = -53
			SET @ErrorMsg = 'Unable to delete from LOAN_FILE_F'
			GOTO ErrorHandler;
		END
		
		-- loan_file_g doesn't exist on production, so re-enable this when it is added to production.
		--DELETE TOP (1)
		--FROM LOAN_FILE_G
		--WHERE
		--	sLId = @LoanID
		--SELECT @RowCount=@@RowCount, @Error = @@Error
		--IF(0 <> @Error) -- supposedly we don't yet use loan_file_g.
		--BEGIN
		--	SET @ErrorCode = -53
		--	SET @ErrorMsg = 'Unable to delete from loan_file_g'
		--	GOTO ErrorHandler;
		--END
		
		DELETE
		FROM LOAN_USER_ASSIGNMENT
		WHERE
			sLId = @LoanID
		IF(0 <> @@Error)
		BEGIN
			SET @ErrorCode = -53
			SET @ErrorMsg = 'Unable to delete from loan_user_assignment'
			GOTO ErrorHandler;
		END
		
		DELETE
		FROM TASK_x_TASK_TRIGGER_TEMPLATE
		WHERE
			LoanID = @LoanID
		IF(0 <> @@Error)
		BEGIN
			SET @ErrorCode = -53
			SET @ErrorMsg = 'Unable to delete from TASK_x_TASK_TRIGGER_TEMPLATE'
			GOTO ErrorHandler;
		END
		
		DELETE TOP (1)
		FROM TRUST_ACCOUNT
		WHERE
			sLId = @LoanID
		SELECT @RowCount=@@RowCount, @Error = @@Error
		IF(0 <> @Error) -- OR 1 <> @RowCount)
		BEGIN
			SET @ErrorCode = -53
			SET @ErrorMsg = 'Unable to delete from TRUST_ACCOUNT'
			GOTO ErrorHandler;
		END
		
		DELETE
		FROM TEAM_LOAN_ASSIGNMENT
		WHERE
			LoanID = @LoanID
		SELECT @RowCount=@@RowCount, @Error = @@Error
		IF(0 <> @Error) -- OR 1 <> @RowCount)
		BEGIN
			SET @ErrorCode = -53
			SET @ErrorMsg = 'Unable to delete from TEAM_LOAN_ASSIGNMENT'
			GOTO ErrorHandler;
		END
		
		DELETE TOP (1)
		FROM LOAN_FILE_CACHE_3
		WHERE
			sLId = @LoanID
		SELECT @RowCount=@@RowCount, @Error = @@Error
		IF(0 <> @Error) -- OR 1 <> @RowCount)
		BEGIN
			SET @ErrorCode = -53
			SET @ErrorMsg = 'Unable to delete from loan_file_cache_3'
			GOTO ErrorHandler;
		END
		
		DELETE TOP (1)
		FROM LOAN_FILE_CACHE_4
		WHERE
			sLId = @LoanID
		SELECT @RowCount=@@RowCount, @Error = @@Error
		IF(0 <> @Error) -- OR 1 <> @RowCount)
		BEGIN
			SET @ErrorCode = -53
			SET @ErrorMsg = 'Unable to delete from loan_file_cache_4'
			GOTO ErrorHandler;
		END
		
		DELETE
		FROM DISCUSSION_LOG
		WHERE
			DiscRefObjId = @LoanID
		IF(0 <> @@Error)
		BEGIN
			SET @ErrorCode = -53
			SET @ErrorMsg = 'Unable to delete from discussion_log'
			GOTO ErrorHandler;
		END
		
		DELETE
		FROM LOAN_CACHE_UPDATE_REQUEST
		WHERE
			sLId = @LoanID
		IF(0 <> @@Error)
		BEGIN
			SET @ErrorCode = -53
			SET @ErrorMsg = 'Unable to delete from loan_cache_update_request'
			GOTO ErrorHandler;
		END
		
		
		DELETE TOP (1)
		FROM LOAN_FILE_A
		WHERE
			sLId = @LoanID
		SELECT @RowCount=@@RowCount, @Error = @@Error
		IF(0 <> @Error) -- OR 1 <> @RowCount)
		BEGIN
			SET @ErrorCode = -53
			SET @ErrorMsg = 'Unable to delete from loan_file_a'
			GOTO ErrorHandler;
		END
		
	
	COMMIT TRANSACTION -- DeletionTransaction
	print 'transaction committed'
	RETURN 0;
	ErrorHandler:
		ROLLBACK TRANSACTION -- DeletionTransaction
		print 'transaction rolled back.'
		RAISERROR(@ErrorMsg, 16, 1)
		RETURN @ErrorCode;
	
	
	RAISERROR('Should not have reached this code.', 16, 1)
	print 'unobtainable code obtained.'
	RETURN -377;
	NonTranErrorHandler:
		print 'NonTranErrorHandler'
		RAISERROR(@ErrorMsg, 16, 1)
		RETURN @ErrorCode;
END
