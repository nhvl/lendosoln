-- =============================================
-- Author:		Scott Kibler
-- Create date: June 14, 2012
-- Description:	Gets the primary app data given a loan id
-- =============================================
CREATE PROCEDURE [dbo].[App_GetPrimaryAppIdFromLoanId]
	-- Add the parameters for the stored procedure here
	@LoanId Guid = null
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT top(1) aAppId from Application_A 
	where sLId = @LoanId 
	order by primaryrankindex
END
