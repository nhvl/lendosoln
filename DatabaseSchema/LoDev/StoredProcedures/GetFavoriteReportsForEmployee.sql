CREATE PROCEDURE [GetFavoriteReportsForEmployee]	
	@EmployeeId uniqueidentifier
AS
BEGIN
	SET NOCOUNT ON
	SELECT
		r.* , e.UserFirstNm + ' ' + e.UserLastNm AS EmployeeName, CASE f.EmployeeId WHEN NULL THEN 0 ELSE 1 END AS IsFavorite
	FROM
		Report_Query AS r INNER JOIN Employee AS e
			ON e.EmployeeId = r.EmployeeId
		INNER JOIN BROKER_USER b
			ON b.EmployeeId = e.EmployeeId
		INNER JOIN ALL_USER a
			ON a.UserId = b.UserId
		INNER JOIN EMPLOYEE_FAVORITE_REPORTS f ON f.QueryId = r.QueryId 
		WHERE f.EmployeeId = @EmployeeId
		ORDER BY r.QueryName
END
