CREATE PROCEDURE [dbo].[UpdateGroupMembership]
	@BrokerId uniqueidentifier, 
	@GroupId uniqueidentifier,	
	@CommaSeparatedMemberIds varchar(4000),
	@IsAddOnly bit = 0
AS
BEGIN
	SET NOCOUNT ON
	BEGIN TRANSACTION
		DECLARE @EntityId varchar(100), @Pos int
		DECLARE @MembershipTableName nvarchar(100)
		
		DECLARE @groupType int
		SELECT @groupType=GroupType	FROM [GROUP] WHERE GroupId=@GroupId	

		IF @IsAddOnly=0
			BEGIN
				IF @groupType=1 
					DELETE FROM GROUP_x_EMPLOYEE  WHERE GroupId = @GroupId		
				ELSE IF @groupType=2
					DELETE FROM GROUP_x_BRANCH  WHERE GroupId = @GroupId		
				ELSE
					DELETE FROM GROUP_x_PML_BROKER  WHERE GroupId = @GroupId		
				IF( 0!=@@error) GOTO ErrorHandler
			END

		SET @CommaSeparatedMemberIds = LTRIM(RTRIM(@CommaSeparatedMemberIds))+ ','
		SET @Pos = CHARINDEX(',', @CommaSeparatedMemberIds, 1)
		IF REPLACE(@CommaSeparatedMemberIds, ',', '') <> ''
		BEGIN
			WHILE @Pos > 0
			BEGIN
				SET @EntityId = LTRIM(RTRIM(LEFT(@CommaSeparatedMemberIds, @Pos - 1)))
				IF @EntityId <> ''
				BEGIN						
						IF @groupType=1 
							INSERT INTO GROUP_x_EMPLOYEE VALUES (@GroupId ,@EntityId)							
						ELSE IF @groupType=2
							INSERT INTO GROUP_x_BRANCH VALUES (@GroupId ,@EntityId)							
						ELSE
							INSERT INTO GROUP_x_PML_BROKER  VALUES (@GroupId ,@EntityId)
					
					IF( 0!=@@error) GOTO ErrorHandler
				END
				SET @CommaSeparatedMemberIds = RIGHT(@CommaSeparatedMemberIds, LEN(@CommaSeparatedMemberIds) - @Pos)
				SET @Pos = CHARINDEX(',', @CommaSeparatedMemberIds, 1)
			END
		END	
	COMMIT TRANSACTION
	
	ErrorHandler:
		if( 0!=@@error)
		BEGIN
			ROLLBACK TRANSACTION
			RAISERROR('Error in delete from Group table in DeleteGroup sp', 16, 1);
			RETURN -100;
		END		
END

