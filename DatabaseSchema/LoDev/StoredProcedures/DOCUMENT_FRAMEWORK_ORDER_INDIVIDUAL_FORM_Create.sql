-- =============================================
-- Author:		Justin Lara
-- Create date: 8/22/2018
-- Description:	Saves an individual form ordered
--				as part of a custom document request.
-- =============================================
ALTER PROCEDURE [dbo].[DOCUMENT_FRAMEWORK_ORDER_INDIVIDUAL_FORM_Create]
	@OrderId int,
	@FormId varchar(50)
AS
BEGIN
	INSERT INTO DOCUMENT_FRAMEWORK_ORDER_INDIVIDUAL_FORM
	(
		OrderId,
		FormId
	)
	VALUES
	(
		@OrderId,
		@FormId
	)
END
GO
