ALTER PROCEDURE [dbo].[OCR_Document_List]
	@BrokerId uniqueidentifier = null,
	@TransactionId uniqueidentifier
AS

IF @BrokerId is not null 
begin
SELECT 
	o.BrokerId,
	FileName,
	SizeBytes,
	o.Status,
	StatusUpdatedOn,
	TransactionId,
	UploadedOn,
	UserId,
	VendorId, 
	Source,
	UploadAppId,
	UploadLoanId,
	UploadDocTypeId,
	UploadInternalDescription
FROM OCR_DOCUMENT o
join broker b on b.brokerid = o.brokerid
WHERE o.BrokerId = @BrokerId and TransactionId = @TransactionId  and b.status = 1

end
else 
begin
SELECT 
	o.BrokerId,
	FileName,
	SizeBytes,
	o.Status,
	StatusUpdatedOn,
	TransactionId,
	UploadedOn,
	UserId,
	VendorId
	Source,
	UploadAppId,
	UploadLoanId,
	UploadDocTypeId,
	UploadInternalDescription
FROM OCR_DOCUMENT o
join broker b on b.brokerid = o.brokerid
WHERE TransactionId = @TransactionId and b.status = 1
END