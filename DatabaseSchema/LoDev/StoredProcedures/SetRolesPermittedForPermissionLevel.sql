-- =============================================
-- Author: Scott Kibler
-- Create date: 5/9/2017
-- Description: 
--	Deletes the roles for the given permission type then adds them based on the xml.
--  The format of the xml should be:
--	  <r id="..."/><r ...
-- =============================================
ALTER PROCEDURE [dbo].[SetRolesPermittedForPermissionLevel]
	@BrokerId uniqueidentifier,
	@PermissionLevelId int,
	@PermissionType int,
	@XmlOfRoleIdsPermittedForPermissionLevelAndType xml -- format: <r id="..."/><r ...
AS
BEGIN

	DELETE FROM CONVOLOG_PERMISSION_ROLE
	WHERE
		BrokerId = @BrokerId
	AND PermissionLevelId = @PermissionLevelId
	AND PermissionType = @PermissionType 
		
	INSERT INTO 
		CONVOLOG_PERMISSION_ROLE
		(PermissionLevelId, BrokerId, RoleId, PermissionType)
	SELECT
		@PermissionLevelId as PermissionLevelId,
		@BrokerId as brokerid,
		tbl.col.value('@id', 'uniqueidentifier') as RoleId,
		@PermissionType as PermissionType
	FROM 
		@XmlOfRoleIdsPermittedForPermissionLevelAndType.nodes('//r') tbl(col)
	
END