ALTER PROCEDURE [dbo].[ListBrokerCRA] 
	@BrokerID Guid
AS
	-- dd 5/11/04 - Since UI only support maximum of 10 CRA. Only return the first 10 records.
	SELECT TOP 10 c.ServiceComId, IsPdfViewNeeded
	FROM CreditReportProtocol c
	WHERE c.OwnerId = @BrokerID
	ORDER BY c.CreatedD