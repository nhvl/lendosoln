CREATE PROCEDURE [dbo].[UCD_DELIVERY_RetrieveAll]
	@LoanId uniqueidentifier,
	@BrokerId uniqueidentifier
AS
BEGIN
	-- Retrieve Manual Deliveries --
	SELECT
		UcdDeliveryId,
		LoanId,
		BrokerId,
		UcdDeliveryType,
		DeliveredTo,
		FileDeliveredDocumentId,
		CaseFileId,
		DateOfDelivery,
		Note,
		IncludeInUldd
	FROM
		UCD_DELIVERY
	WHERE
		LoanId = @LoanId AND 
		BrokerId = @BrokerId AND
		UcdDeliveryType = 0
		
	-- Retrieve Fannie Mae Deliveries --
	SELECT
		u.UcdDeliveryId,
		u.LoanId,
		u.BrokerId,
		u.UcdDeliveryType,
		u.DeliveredTo,
		u.FileDeliveredDocumentId,
		u.CaseFileId,
		u.DateOfDelivery,
		u.Note,
		u.IncludeInUldd,
		ufan.BatchId,
		ufan.CasefileStatus,
		ufan.FindingsDocumentId,
		ufan.FindingsXmlFileDbKey
	FROM
		UCD_DELIVERY u JOIN 
		UCD_DELIVERY_FANNIE_MAE ufan ON ufan.UcdDeliveryId=u.UcdDeliveryId
	WHERE
		u.LoanId = @LoanId AND 
		u.BrokerId = @BrokerId
		
	-- Retrieve Fannie Mae Deliveries --
	SELECT
		u.UcdDeliveryId,
		u.LoanId,
		u.BrokerId,
		u.UcdDeliveryType,
		u.DeliveredTo,
		u.FileDeliveredDocumentId,
		u.CaseFileId,
		u.DateOfDelivery,
		u.Note,
		u.IncludeInUldd,
		ufred.BatchId,
		ufred.UcdRequirementStatus,
		ufred.FindingsDocumentId,
		ufred.FindingsXmlFileDbKey
	FROM
		UCD_DELIVERY u JOIN 
		UCD_DELIVERY_FREDDIE_MAC ufred ON ufred.UcdDeliveryId=u.UcdDeliveryId
	WHERE
		u.LoanId = @LoanId AND 
		u.BrokerId = @BrokerId
END