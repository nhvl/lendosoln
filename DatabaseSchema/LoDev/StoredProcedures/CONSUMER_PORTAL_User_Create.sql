-- =============================================
-- Author:		Antonio Valencia
-- Create date: 6/2/2013
-- Description:	Creates a new consumer portal user
-- =============================================
ALTER PROCEDURE [dbo].[CONSUMER_PORTAL_User_Create]
	@BrokerId uniqueidentifier,
	@CreateD datetime = null, 
	@FirstName varchar(50),
	@LastName varchar(50),
	@Phone varchar(21),
	@Email varchar(80),
	@PasswordHash varchar(128),
	@PasswordSalt varchar(128),
	@LastLoginD datetime = null, 
	@PasswordResetCode varchar(10) = '',
	@PasswordResetCodeRequestD datetime = null,
	@DisclosureAcceptedD datetime = null,
	@IsTemporaryPassword bit,
	@Id bigint = null,
	@LoginAttempts int = 0,
	@IsNotifyOnStatusChanges  bit = 0,
	@IsNotifyOnNewDocRequests bit = 1,
	@TempPasswordSetD datetime = null,
	@NotificationEmail varchar(80), 
	@SignatureFontType int,
	@SignatureName varchar(150),
	@ReferralSource varchar(300),
	@SecurityPin varchar(4) = NULL,
	@EncryptedSecurityPin varbinary(20) = 0x,
	@EncryptionKeyId uniqueidentifier
AS
BEGIN
	INSERT CONSUMER_PORTAL_USER(BrokerId, CreateD, FirstName, LastName, Phone, Email, PasswordHash, PasswordSalt, LastLoginD, PasswordResetCode, PasswordResetCodeRequestD, DisclosureAcceptedD, IsTemporaryPassword, LoginAttempts,IsNotifyOnStatusChanges,IsNotifyOnNewDocRequests,TempPasswordSetD,NotificationEmail, SignatureFontType,SignatureName,ReferralSource,SecurityPin,
	EncryptedSecurityPin,
	EncryptionKeyId
	) VALUES (
	@BrokerId,@CreateD,@FirstName,@LastName,@Phone,@Email,@PasswordHash,@PasswordSalt,@LastLoginD,@PasswordResetCode,@PasswordResetCodeRequestD,@DisclosureAcceptedD,@IsTemporaryPassword, @LoginAttempts,@IsNotifyOnStatusChanges,@IsNotifyOnNewDocRequests,@TempPasswordSetD,@NotificationEmail,@SignatureFontType,@SignatureName,@ReferralSource,COALESCE(@SecurityPin,''),
	@EncryptedSecurityPin,
	@EncryptionKeyId
	)

SELECT CAST(SCOPE_IDENTITY() AS BIGINT) AS LAST_IDENTITY
END
