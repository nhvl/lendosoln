-- =============================================
-- Author:		Geoff Feltman
-- Create date: 9/16/16
-- Description:	Retrieve multiple specified condition choices in a single db call.
-- =============================================
ALTER PROCEDURE [dbo].[TASK_ConditionChoice_FetchMultiple]
	@BrokerId uniqueidentifier, 
	@IdXml xml
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT 	
		ConditionChoiceId,
		CONDITION_CHOICE.BrokerId,
		CONDITION_CHOICE.ConditionCategoryId,
		ConditionType,
		sLT,
		ConditionSubject,
		ToBeAssignedRole,
		ToBeOwnedByRole,
		DueDateFieldName,
		DueDateAddition,
		Rank,
		cc.Category,
		cc.DefaultTaskPermissionLevelId,
		cc.IsDisplayAtTop,
		arole.RoleModifiableDesc as ToBeAssignedRoleName, 
		brole.RoleModifiableDesc  as ToBeOwnedByRoleName,
		RequiredDocTypeId,
		IsHidden
	FROM @IdXml.nodes('root/id') as T(Item)
		JOIN CONDITION_CHOICE ON T.Item.value('.', 'int') = ConditionChoiceId 
		LEFT JOIN ROLE as arole on CONDITION_CHOICE.ToBeAssignedRole = arole.RoleId 
		LEFT JOIN   ROLE as brole on CONDITION_CHOICE.ToBeOwnedByRole = brole.RoleId
		LEFT JOIN Condition_Category as cc on cc.ConditionCategoryId = CONDITION_CHOICE.ConditionCategoryId 
	WHERE CONDITION_CHOICE.BrokerId = @BrokerId
	ORDER BY Rank, cc.Category
END
