ALTER PROCEDURE [dbo].[TITLE_FRAMEWORK_LENDER_SERVICE_Update]
    @LenderServiceId int,
    @PublicId uniqueidentifier = null, -- Not using this but here due to shared parameter lists with create SP.
    @BrokerId uniqueidentifier,
    @VendorId int,
    @VendorName varchar(200),
    @IsNonInteractiveQuoteEnabled bit,
    @IsInteractiveQuoteEnabled bit,
    @IsPolicyEnabled bit,
    @ConfigurationFileDbKey uniqueidentifier = null,
    @IsEnabledForLqbUsers bit,
    @EnabledEmployeeGroupId uniqueidentifier = null,
    @IsEnabledForTpoUsers bit,
    @EnabledOcGroupId uniqueidentifier = null,
    @UsesProviderCodes bit
AS BEGIN
UPDATE dbo.TITLE_FRAMEWORK_LENDER_SERVICE
SET
    VendorId = @VendorId,
    VendorName = @VendorName,
    IsNonInteractiveQuoteEnabled = @IsNonInteractiveQuoteEnabled,
    IsInteractiveQuoteEnabled = @IsInteractiveQuoteEnabled,
    IsPolicyEnabled = @IsPolicyEnabled,
    ConfigurationFileDbKey = @ConfigurationFileDbKey,
    IsEnabledForLqbUsers = @IsEnabledForLqbUsers,
    EnabledEmployeeGroupId = @EnabledEmployeeGroupId,
    IsEnabledForTpoUsers = @IsEnabledForTpoUsers,
    EnabledOcGroupId = @EnabledOcGroupId,
    UsesProviderCodes = @UsesProviderCodes
WHERE
    LenderServiceId = @LenderServiceId AND
    BrokerId = @BrokerId
END

