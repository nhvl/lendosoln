-- =============================================
-- Author:		paoloa
-- Create date: 5/1/2013
-- Description:	
-- =============================================
ALTER PROCEDURE [dbo].[GetAppraisalOrders]
	-- Add the parameters for the stored procedure here
	@sLId uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT ao.*,
		VendorId,
		[Status],
		StatusDate,
		AppraisalOrderXmlContent,
		OrderDataUpdateXmlContent,
		UploadedFilesXmlContent,
		OrderHistoryXmlContent,
		EmbeddedDocumentIdsXmlContent,
		InitialOrderDate
	FROM APPRAISAL_ORDER_INFO aoi
	JOIN APPRAISAL_ORDER ao ON aoi.AppraisalOrderId = ao.AppraisalOrderId
	WHERE sLId = @sLId
END
