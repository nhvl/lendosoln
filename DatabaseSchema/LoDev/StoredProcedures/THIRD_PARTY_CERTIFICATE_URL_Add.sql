CREATE PROCEDURE [dbo].[THIRD_PARTY_CERTIFICATE_URL_Add]
	@ThirdPartyCertificateId int, 
	@ApplicableUrl varchar(300)
AS
BEGIN
	
	DECLARE @ExistingEntryId int
	
	SELECT @ExistingEntryId = Id FROM THIRD_PARTY_CERTIFICATE_URL WHERE @ApplicableUrl = ApplicableUrl 
	
	
	IF @@ROWCOUNT = 1 
	BEGIN
		UPDATE THIRD_PARTY_CERTIFICATE_URL
		SET ThirdPartyCertificateId = @ThirdPartyCertificateId 
		WHERE @ExistingEntryId = Id 
	END
	ELSE 
	BEGIN
		INSERT INTO [dbo].[THIRD_PARTY_CERTIFICATE_URL] (ThirdPartyCertificateId, ApplicableUrl)
		VALUES (@ThirdPartyCertificateId, @ApplicableUrl)
	END
END

