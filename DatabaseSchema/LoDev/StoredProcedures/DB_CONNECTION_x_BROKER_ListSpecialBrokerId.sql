-- =============================================
-- Author:		David Dao
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE dbo.DB_CONNECTION_x_BROKER_ListSpecialBrokerId

AS
BEGIN

    SELECT BrokerId, CustomerCode
    FROM DB_CONNECTION_x_BROKER
    WHERE CustomerCode IN ('PMLMASTER', 'ADMINBR')
END
