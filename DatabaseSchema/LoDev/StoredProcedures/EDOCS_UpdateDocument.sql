/****** Object:  StoredProcedure [dbo].[EDOCS_UpdateDocument]    Script Date: 01/23/2018 10:31:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Antonio Valencia
-- Create date: 1/5/10
-- Description:	Updates a EDoc Document
-- 7/11/11 -- Removed the audit stuff from here. EDoc object will need to add it itself in order to support OPM 67127
-- 1/4/12: Added support for document status - OPM 63511, AM
-- 08/24/12: Added First Close doc type and sent date
-- 7/12/2013: Added SQL to update sUnscreenedDocsInFile in LOAN_FILE_CACHE
-- 8/14/2013: Added HideFromPmlUsers
-- =============================================
ALTER PROCEDURE [dbo].[EDOCS_UpdateDocument] 
	@sLId uniqueidentifier = null, 
	@aAppId uniqueidentifier = null,
	@BrokerId uniqueidentifier, 
	@DocTypeId int, 
	@FaxNumber varchar(14),
	@InternalDescription varchar(200),
	@PublicDescription varchar(200),
	@Comments varchar(500),
	@IsUploadedByPmlUser bit,
	@DocumentId uniqueidentifier,
	@FileDBKey uniqueidentifier, 
	@NumPages int,
	@ModifiedByUserId uniqueidentifier = null, 
	@UpdateReason varchar(200) = '',
	@Version int,
	@IsEditable bit,
	@HasInternalAnnotation bit,
	@PdfHasUnsupportedFeatures bit = 0,
	@Status tinyint = 0,
	@StatusDescription varchar(200)= '',
	@PdfPageInfo varchar(max) = null, --todo remove,
	@HideFromPmlUsers bit = 0,
	@MetadataJsonContent varchar(max) = null,
	@IsESigned bit = 0,
	@HasESignTags bit = 0,
	@MetadataProtobufContent varbinary(max) = NULL,
	@OriginalMetadataProtobufContent varbinary(max) = NULL
AS
BEGIN
	DECLARE @CurrentVersion int  
	DECLARE @CurrentRevisionKey uniqueidentifier 
	DECLARE @CurrentPageCount int
	DECLARE @CurrentStatus As tinyint

	
	BEGIN TRANSACTION	
	
	SELECT @CurrentVersion = Version, @CurrentPageCount = NumPages, @CurrentStatus = [Status]  FROM  EDOCS_Document  WITH(ROWLOCK, UPDLOCK) WHERE DocumentId = @DocumentId 
	IF @@error!= 0 GOTO HANDLE_ERROR
	

	IF @CurrentVersion <> @Version
	BEGIN  
		GOTO INVALID_ERROR
	END 

	UPDATE EDOCS_Document WITH(ROWLOCK, UPDLOCK)
	SET 
		sLId = @sLId,
		aAppId = @aAppId,
		BrokerId = @BrokerId,
		DocTypeId = @DocTypeId,
		FaxNumber = @FaxNumber,
		InternalDescription = @InternalDescription,
		Comments = @Comments,
		IsUploadedByPmlUser = @IsUploadedByPmlUser,
		FileDBKey_CurrentRevision = @FileDBKey,
		NumPages = @NumPages,
		Description = @PublicDescription,
		IsEditable = @IsEditable,
		HasInternalAnnotation = @HasInternalAnnotation,
		PdfHasUnsupportedFeatures = @PdfHasUnsupportedFeatures,
		[Status] = @Status,
		StatusDescription = @StatusDescription,
		PdfPageInfo =   coalesce(@PdfPageInfo, PdfPageInfo),
		HideFromPmlUsers = @HideFromPmlUsers,
		MetadataJsonContent = COALESCE(@MetadataJsonContent, MetadataJsonContent),
		IsESigned = @IsESigned,
		HasESignTags = @HasESignTags,
		MetadataProtobufContent = COALESCE(@MetadataProtobufContent, MetadataProtobufContent),
		OriginalMetadataProtobufContent = COALESCE(@OriginalMetadataProtobufContent, OriginalMetadataProtobufContent)
	WHERE 
		DocumentId = @DocumentId; 
	IF @@error!= 0 GOTO HANDLE_ERROR

	-- update cached stats --
	-- Only need to update if Current Status and New Status are not the same
	IF @sLId IS NOT NULL AND @CurrentStatus <> @Status
	BEGIN
		IF @Status = 0
		BEGIN
			-- We know we're setting doc status to unscreened, so just set to 1 --
			UPDATE LOAN_FILE_CACHE_3
			SET sUnscreenedDocsInFile = 1
			WHERE sLId = @sLId
		END
		ELSE IF @CurrentStatus = 0
		BEGIN
			-- Changed Status from blank, so we need to check for other unscreened docs --
			UPDATE LOAN_FILE_CACHE_3
			SET sUnscreenedDocsInFile = CASE WHEN EXISTS(SELECT TOP(1) 1 FROM EDOCS_DOCUMENT
				WHERE sLId = @sLid AND IsValid = 1 AND [Status] = 0) THEN 1 ELSE 0 END
			WHERE sLId = @sLId
		END
		-- ELSE DO NOTHING. NO NEED TO UPDATE IF STATUS CHANGED FROM ONE SCREENED STATUS TO ANOTHER --
	END
	
	
	COMMIT
	
	SELECT VERSION from EDOCS_Document where DocumentId = @DocumentId
	
	RETURN

INVALID_ERROR: 
	ROLLBACK TRANSACTION 
	RAISERROR  ('Versions do not match.', 16, 1 );   --update EDocument.cs iF you modify the message
	RETURN; 

HANDLE_ERROR:
    ROLLBACK TRANSACTION
	RAISERROR ('Failure updating electronic document.', 16, 1)
    RETURN 
		
END