ALTER PROCEDURE [dbo].[OCR_DOCUMENT_Delete]
	@TransactionId uniqueidentifier
AS

DELETE TOP(1) ocr_document
where TransactionId = @TransactionId 
select @@rowcount 
