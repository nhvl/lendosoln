-- =============================================
-- Author:		Michael Leinweaver
-- Create date: 5/17/2018
-- Description:	Gets a document's description by ID.
-- =============================================
CREATE PROCEDURE [dbo].[EDOCS_DOCUMENT_GetDescriptionById]
	@DocumentId uniqueidentifier,
	@BrokerId uniqueidentifier
AS
BEGIN
	SELECT Description
	FROM EDOCS_DOCUMENT
	WHERE DocumentId = @DocumentId AND BrokerId = @BrokerId
END