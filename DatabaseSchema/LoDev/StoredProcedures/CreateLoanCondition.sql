






CREATE  PROCEDURE [dbo].[CreateLoanCondition]
	@CondId uniqueidentifier out,
	@LoanId uniqueidentifier,
	@CondDesc varchar (3000),
	@CondStatus int,
	@CondStatusDate datetime,
	@CondCategoryDesc varchar(100) = '',
	@CondOrderedPosition int = 0,
	@CondIsHidden bit = 0,
	@CondIsValid bit = 1,
	@CondCompletedByUserNm varchar(60) = '',
	@CondNotes varchar(1000) = '',
	@CondDeleteD datetime = null,
	@CondDeleteByUserNm varchar(60) = ''
 AS
DECLARE @IsLoanStillValid bit;
SET @CondId = newid();

SELECT @IsLoanStillValid = IsValid
FROM Loan_File_Cache WITH(NOLOCK)
WHERE sLId = @LoanId


INSERT INTO Condition 
([CondId], [LoanId], [CondDesc], [CondStatus], [CondStatusDate], [IsLoanStillValid], [CondCategoryDesc], [CondOrderedPosition], [CondIsHidden], [CondIsValid], [CondCompletedByUserNm], [CondNotes], [CondDeleteD], [CondDeleteByUserNm])
VALUES
(@CondId, @LoanId, @CondDesc, @CondStatus, @CondStatusDate, @IsLoanStillValid, @CondCategoryDesc, @CondOrderedPosition, @CondIsHidden, @CondIsValid, @CondCompletedByUserNm, @CondNotes, @CondDeleteD, @CondDeleteByUserNm)
IF ( 0 != @@ERROR )
BEGIN
	RAISERROR('Error in inserting into Condition table in CreateLoanCondition sp', 16, 1);
	RETURN -100;
END

UPDATE LOAN_FILE_F
SET sConditionLastModifiedD = GETDATE()
WHERE sLId = @LoanId

RETURN 0;






