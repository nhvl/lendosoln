

-- =============================================
-- Author:        Antonio Valencia
-- Create date: 4/8/11
-- Description:   RetrievesTaskPermissionLevels
--
-- Edited by Matthew Pham 5/15/12
-- : Added the Close permission level. OPM 81483
--
-- Edited by Geoffrey Feltman 2/4/13
-- : Added support for the Post-Closer, Insuring, Collateral Agent, and 
--   Doc Drawer roles. OPM 108148
-- =============================================
CREATE PROCEDURE [dbo].[TASK_PermissionLevelFetch] 
      -- Add the parameters for the stored procedure here
      @BrokerId uniqueidentifier, 
      @Id int = null 
AS

--REVIEW
-- 6/9/2011: Reviewed. -ThinhNK

BEGIN
      SET NOCOUNT ON;

      SELECT 
            TaskPermissionLevelId,
            BrokerId ,
            Name,
            Description,
            Rank,
            IsAppliesToConditions,
            IsActive,
            IsRequiredWorkOnRoleAdministrator ,
            IsRequiredWorkOnRoleManager ,
            IsRequiredWorkOnRoleAccountant ,
            IsRequiredWorkOnRoleUnderwriter ,
            IsRequiredWorkOnRoleLockDesk ,
            IsRequiredWorkOnRoleFunder ,
            IsRequiredWorkOnRoleProcessor ,
            IsRequiredWorkOnRoleLoanOpener ,
            IsRequiredWorkOnRoleLoanOfficer ,
            IsRequiredWorkOnRoleLenderAccountExecutive ,
            IsRequiredWorkOnRoleRealEstateAgent ,
            IsRequiredWorkOnRoleCallCenterAgent ,
            IsRequiredWorkOnRoleShipper ,
            IsRequiredWorkOnRoleCloser ,
            IsRequiredWorkOnRolePmlLoanOfficer ,
            IsRequiredWorkOnRolePmlBrokerProcessor ,
            IsRequiredWorkOnRolePmlSupervisor ,
            IsRequiredWorkOnRolePostCloser ,
            IsRequiredWorkOnRoleInsuring ,
            IsRequiredWorkOnRoleCollateralAgent ,
            IsRequiredWorkOnRoleDocDrawer ,
            IsRequiredWorkOnRoleCreditAuditor,
			IsRequiredWorkOnRoleDisclosureDesk,
			IsRequiredWorkOnRoleJuniorProcessor,
			IsRequiredWorkOnRoleJuniorUnderwriter,
			IsRequiredWorkOnRoleLegalAuditor,
			IsRequiredWorkOnRoleLoanOfficerAssistant,
			IsRequiredWorkOnRolePurchaser,
			IsRequiredWorkOnRoleQCCompliance,
			IsRequiredWorkOnRoleSecondary,
			IsRequiredWorkOnRoleServicing,
            IsRequiredWorkOnRoleExternalSecondary,
            IsRequiredWorkOnRoleExternalPostCloser,

            IsRequiredCloseRoleAdministrator ,
            IsRequiredCloseRoleManager ,
            IsRequiredCloseRoleAccountant ,
            IsRequiredCloseRoleUnderwriter ,
            IsRequiredCloseRoleLockDesk ,
            IsRequiredCloseRoleFunder ,
            IsRequiredCloseRoleProcessor ,
            IsRequiredCloseRoleLoanOpener ,
            IsRequiredCloseRoleLoanOfficer ,
            IsRequiredCloseRoleLenderAccountExecutive ,
            IsRequiredCloseRoleRealEstateAgent ,
            IsRequiredCloseRoleCallCenterAgent ,
            IsRequiredCloseRoleShipper ,
            IsRequiredCloseRoleCloser ,
            IsRequiredCloseRolePmlLoanOfficer ,
            IsRequiredCloseRolePmlBrokerProcessor ,
            IsRequiredCloseRolePmlSupervisor ,
            IsRequiredCloseRolePostCloser ,
            IsRequiredCloseRoleInsuring ,
            IsRequiredCloseRoleCollateralAgent ,
            IsRequiredCloseRoleDocDrawer ,
            IsRequiredCloseRoleCreditAuditor,
			IsRequiredCloseRoleDisclosureDesk,
			IsRequiredCloseRoleJuniorProcessor,
			IsRequiredCloseRoleJuniorUnderwriter,
			IsRequiredCloseRoleLegalAuditor,
			IsRequiredCloseRoleLoanOfficerAssistant,
			IsRequiredCloseRolePurchaser,
			IsRequiredCloseRoleQCCompliance,
			IsRequiredCloseRoleSecondary,
			IsRequiredCloseRoleServicing,
            IsRequiredCloseRoleExternalSecondary,
            IsRequiredCloseRoleExternalPostCloser,

            IsRequiredManageRoleAdministrator ,
            IsRequiredManageRoleManager ,
            IsRequiredManageRoleAccountant ,
            IsRequiredManageRoleUnderwriter ,
            IsRequiredManageRoleLockDesk ,
            IsRequiredManageRoleFunder ,
            IsRequiredManageRoleProcessor ,
            IsRequiredManageRoleLoanOpener ,
            IsRequiredManageRoleLoanOfficer ,
            IsRequiredManageRoleLenderAccountExecutive ,
            IsRequiredManageRoleRealEstateAgent ,
            IsRequiredManageRoleCallCenterAgent ,
            IsRequiredManageRoleShipper ,
            IsRequiredManageRoleCloser ,
            IsRequiredManageRolePmlLoanOfficer ,
            IsRequiredManageRolePmlBrokerProcessor ,
            IsRequiredManageRolePmlSupervisor ,
            IsRequiredManageRolePostCloser ,
            IsRequiredManageRoleInsuring ,
            IsRequiredManageRoleCollateralAgent ,
            IsRequiredManageRoleDocDrawer ,
            IsRequiredManageRoleCreditAuditor,
			IsRequiredManageRoleDisclosureDesk,
			IsRequiredManageRoleJuniorProcessor,
			IsRequiredManageRoleJuniorUnderwriter,
			IsRequiredManageRoleLegalAuditor,
			IsRequiredManageRoleLoanOfficerAssistant,
			IsRequiredManageRolePurchaser,
			IsRequiredManageRoleQCCompliance,
			IsRequiredManageRoleSecondary,
			IsRequiredManageRoleServicing,
            IsRequiredManageRoleExternalSecondary,
            IsRequiredManageRoleExternalPostCloser,
            
            RequiredWorkOnEmployeeGroup ,
            RequiredCloseEmployeeGroup ,
            RequiredManageEmployeeGroup 

      FROM
            TASK_PERMISSION_LEVEL
      WHERE BrokerId = @BrokerId AND TaskPermissionLevelId = COALESCE(@Id, TaskPermissionLevelId)
      ORDER BY Rank ASC, Name ASC

            
END



