-- =============================================
-- Author:		Antonio Valencia
-- Create date: 8/7/2012
-- Description:	List all templates for a given broker only returns gfe2010.
-- =============================================
CREATE PROCEDURE [dbo].[CC_TEMPLATE_GetTemplateDetails]
	@BrokerId uniqueidentifier
AS
BEGIN
	DECLARE @Id int

	SET @Id = null
		

	SELECT TOP 1 @Id = Id FROM CC_TEMPLATE_SYSTEM where BrokerId = @BrokerId AND ReleasedD IS Not Null Order By ReleasedD Desc 
	


	-- we should probably limit
	SELECT cCcTemplateNm, cCcTemplateId, CCTemplateSystemId,GfeVersion 
	FROM CC_TEMPLATE  
	WHERE BrokerId = @BrokerId AND GfeVersion = 1 AND CCTemplateSystemId is NULL or (@Id is not null and CCTemplateSystemId = @id)
END
