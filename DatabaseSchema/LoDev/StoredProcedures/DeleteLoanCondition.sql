

CREATE PROCEDURE [dbo].[DeleteLoanCondition]
	@CondId uniqueidentifier,
	@LoanId uniqueidentifier

AS
DELETE
FROM Condition 
WHERE CondId = @CondId 
  AND LoanId = @LoanId
