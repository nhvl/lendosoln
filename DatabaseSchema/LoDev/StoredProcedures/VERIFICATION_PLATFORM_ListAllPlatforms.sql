-- =============================================
-- Author:		Eric Mallare
-- Create date: 4/25/2017
-- Description:	Loads all the Platforms and associated TransmissionInfo data
-- =============================================
CREATE PROCEDURE [dbo].[VERIFICATION_PLATFORM_ListAllPlatforms]
AS
BEGIN
	SELECT 
		t.TransmissionAssociationT, t.TargetUrl, t.TransmissionAuthenticationT, t.EncryptionKeyId,
		t.RequestCertLocation, t.ResponseCertId, t.RequestCredentialName, t.RequestCredentialPassword, t.ResponseCredentialName, t.ResponseCredentialPassword,
		p.PlatformId, p.PlatformName, p.TransmissionModelT, p.PayloadFormatT, p.UsesAccountid, p.RequiresAccountId, p.TransmissionId
	FROM
		VERIFICATION_PLATFORM p JOIN
		VERIFICATION_TRANSMISSION t ON p.TransmissionId=t.TransmissionId
END
