-- =============================================
-- Author:		Eric Mallare
-- Create date: 8/16/2017
-- Description:	Updates a Title platform
-- =============================================
ALTER PROCEDURE [dbo].[TITLE_UpdatePlatform]
	@PlatformId int,
	@PlatformName varchar(150),
	@TransmissionModelT int,
	@PayloadFormatT int,
	@TransmissionId int,
	@UsesAccountId bit,
	@RequiresAccountId bit,
	@TargetUrl varchar(200),
	@TransmissionAuthenticationT int,
	@TransmissionAssociationT int,
	@EncryptionKeyId uniqueidentifier,
	@ResponseCertId uniqueidentifier = null,
	@RequestCredentialName varchar(50) = null,
	@RequestCredentialPassword varbinary(max) = null,
	@ResponseCredentialName varchar(50) = null,
	@ResponseCredentialPassword varbinary(max) = null
AS
BEGIN TRANSACTION
	
	UPDATE
		TITLE_FRAMEWORK_TRANSMISSION_INFO
	SET
		TargetUrl=@TargetUrl,
		TransmissionAuthenticationT=@TransmissionAuthenticationT,
		EncryptionKeyId = @EncryptionKeyId,
		ResponseCertId=@ResponseCertId,
		RequestCredentialName=@RequestCredentialName,
		ResponseCredentialName=@ResponseCredentialName,
		RequestCredentialPassword=@RequestCredentialPassword,
		ResponseCredentialPassword=@ResponseCredentialPassword
	WHERE
		TransmissionInfoId=@TransmissionId

	IF( @@error != 0 )
      	GOTO HANDLE_ERROR
	
	UPDATE
		TITLE_FRAMEWORK_PLATFORM
	SET
		PlatformName=@PlatformName, 
		TransmissionModelT=@TransmissionModelT, 
		PayloadFormatT=@PayloadFormatT,
		UsesAccountId=@UsesAccountId, 
		RequiresAccountId=@RequiresAccountId
	WHERE 
		PlatformId=@PlatformId
		
	IF( @@error != 0 )
      	GOTO HANDLE_ERROR

COMMIT TRANSACTION
RETURN;
	HANDLE_ERROR:
     	ROLLBACK TRANSACTION
    	RAISERROR('Error in TITLE_UpdatePlatform sp', 16, 1);;
    	RETURN;
