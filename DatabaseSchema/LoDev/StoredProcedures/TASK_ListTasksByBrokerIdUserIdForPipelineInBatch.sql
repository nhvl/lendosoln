











ALTER PROCEDURE [dbo].[TASK_ListTasksByBrokerIdUserIdForPipelineInBatch] 
	@BrokerId uniqueidentifier,
	@UserId uniqueidentifier = NULL,
	@OrderBy varchar(60),
	@Desc bit, 
	@StartRow int
	
AS

-- REVIEW
-- 6/9/2011: The sorting takes majority of the execution time but we have to put the pipeline in some order. Reviewed. -ThinhNK
-- 7/5/2012: Added CondRowId. -MP
BEGIN

	DECLARE @EndRow int
	SET @EndRow = @StartRow + 100
	
	SELECT TOP 100
		BrokerId
		, TaskId
		, TaskAssignedUserId
		, TaskStatus
		, LoanId
		, TaskOwnerUserId
		, TaskCreatedDate
		, TaskSubject
		, TaskDueDate
		, TaskFollowUpDate
		, CondInternalNotes
		, TaskIsCondition
		, TaskResolvedUserId
		, TaskLastModifiedDate
		, BorrowerNmCached
		, LoanNumCached
		, TaskToBeAssignedRoleId
		, TaskToBeOwnerRoleId
		, TaskCreatedByUserId
		, TaskDueDateLocked
		, TaskDueDateCalcField
		, TaskDueDateCalcDays
		, TaskHistoryXml
		, TaskPermissionLevelId
		, TaskClosedUserId
		, TaskClosedDate
		, TaskRowVersion
		, CondRowId
		, CondCategoryId
		, CondIsHidden
		, CondDeletedDate
		, CondDeletedByUserNm
		, CondIsDeleted
		, TaskAssignedUserFullName
		, TaskOwnerFullName
		, TaskClosedUserFullName
		, CondMessageId
		, AssignToOnReactivationUserId

	FROM (
	
		select BrokerId
		, ta.TaskId
		, ta.TaskAssignedUserId
		, ta.TaskStatus
		, ta.LoanId
		, ta.TaskOwnerUserId
		, ta.TaskCreatedDate
		, ta.TaskSubject
		, ta.TaskDueDate
		, ta.TaskFollowUpDate
		, ta.CondInternalNotes
		, ta.TaskIsCondition
		, ta.TaskResolvedUserId
		, ta.TaskLastModifiedDate
		, ta.BorrowerNmCached
		, ta.LoanNumCached
		, ta.TaskToBeAssignedRoleId
		, ta.TaskToBeOwnerRoleId
		, ta.TaskCreatedByUserId
		, ta.TaskDueDateLocked
		, ta.TaskDueDateCalcField
		, ta.TaskDueDateCalcDays
		, ta.TaskHistoryXml
		, ta.TaskPermissionLevelId
		, ta.TaskClosedUserId
		, ta.TaskClosedDate
		, ta.TaskRowVersion
		, ta.CondRowId
		, ta.CondCategoryId
		, ta.CondIsHidden
		, ta.CondDeletedDate
		, ta.CondDeletedByUserNm
		, ta.CondIsDeleted
		, ta.TaskAssignedUserFullName
		, ta.TaskOwnerFullName
		, ta.TaskClosedUserFullName
		, ta.CondMessageId
		, ta.AssignToOnReactivationUserId
		, ROW_NUMBER() OVER(
		ORDER BY 
CASE WHEN @Desc = 0 AND @OrderBy = 'TaskId' THEN ta.TaskId END,
CASE WHEN @Desc = 1 AND @OrderBy = 'TaskId' THEN ta.TaskId END DESC,
CASE WHEN @Desc = 0 AND @OrderBy = 'TaskSubject' THEN ta.TaskSubject END,
CASE WHEN @Desc = 1 AND @OrderBy = 'TaskSubject' THEN ta.TaskSubject END DESC,
CASE WHEN @Desc = 0 AND @OrderBy = 'TaskStatus' THEN ta.TaskStatus END,
CASE WHEN @Desc = 1 AND @OrderBy = 'TaskStatus' THEN ta.TaskStatus END DESC,
CASE WHEN @Desc = 0 AND @OrderBy = 'LoanNumCached' THEN ta.LoanNumCached END,
CASE WHEN @Desc = 1 AND @OrderBy = 'LoanNumCached' THEN ta.LoanNumCached END DESC,
CASE WHEN @Desc = 0 AND @OrderBy = 'BorrowerNmCached' THEN ta.BorrowerNmCached END,
CASE WHEN @Desc = 1 AND @OrderBy = 'BorrowerNmCached' THEN ta.BorrowerNmCached END DESC,
CASE WHEN @Desc = 0 AND @OrderBy = 'TaskDueDate' THEN ta.TaskDueDate END,
CASE WHEN @Desc = 1 AND @OrderBy = 'TaskDueDate' THEN ta.TaskDueDate END DESC,
CASE WHEN @Desc = 0 AND @OrderBy = 'TaskFollowUpDate' THEN ta.TaskFollowUpDate END,
CASE WHEN @Desc = 1 AND @OrderBy = 'TaskFollowUpDate' THEN ta.TaskFollowUpDate END DESC,
CASE WHEN @Desc = 0 AND @OrderBy = 'TaskLastModifiedDate' THEN ta.TaskStatus END,
CASE WHEN @Desc = 1 AND @OrderBy = 'TaskLastModifiedDate' THEN ta.TaskStatus END DESC,
CASE WHEN @Desc = 0 AND @OrderBy = 'TaskAssignedUserFullName' THEN ta.LoanNumCached END,
CASE WHEN @Desc = 1 AND @OrderBy = 'TaskAssignedUserFullName' THEN ta.LoanNumCached END DESC,
CASE WHEN @Desc = 0 AND @OrderBy = 'TaskOwnerFullName' THEN ta.BorrowerNmCached END,
CASE WHEN @Desc = 1 AND @OrderBy = 'TaskOwnerFullName' THEN ta.BorrowerNmCached END DESC

) as RowNumber FROM task ta where
		ta.BrokerId = @BrokerId
		AND
		( 
			( @UserId IS NULL OR @UserId = '00000000-0000-0000-0000-000000000000' )
			OR
			 ta.TaskAssignedUserId = COALESCE(@UserId, ta.TaskAssignedUserId)
		)
		AND
		(ta.TaskStatus = 0 OR ta.TaskStatus = 1)
		AND 
		ta.LoanId IN (select slId from Loan_File_Cache with(nolock) where IsValid <> 0 and sBrokerId = @BrokerId)
) Task

	WHERE
		RowNumber > @StartRow
		AND 
		RowNumber < @EndRow

-- ORDER BY clause is a bit unusual, but
-- we want to avoid dynamic sql if possible.
ORDER BY 
CASE WHEN @Desc = 0 AND @OrderBy = 'TaskId' THEN TaskId END,
CASE WHEN @Desc = 1 AND @OrderBy = 'TaskId' THEN TaskId END DESC,
CASE WHEN @Desc = 0 AND @OrderBy = 'TaskSubject' THEN TaskSubject END,
CASE WHEN @Desc = 1 AND @OrderBy = 'TaskSubject' THEN TaskSubject END DESC,
CASE WHEN @Desc = 0 AND @OrderBy = 'TaskStatus' THEN TaskStatus END,
CASE WHEN @Desc = 1 AND @OrderBy = 'TaskStatus' THEN TaskStatus END DESC,
CASE WHEN @Desc = 0 AND @OrderBy = 'LoanNumCached' THEN LoanNumCached END,
CASE WHEN @Desc = 1 AND @OrderBy = 'LoanNumCached' THEN LoanNumCached END DESC,
CASE WHEN @Desc = 0 AND @OrderBy = 'BorrowerNmCached' THEN BorrowerNmCached END,
CASE WHEN @Desc = 1 AND @OrderBy = 'BorrowerNmCached' THEN BorrowerNmCached END DESC,
CASE WHEN @Desc = 0 AND @OrderBy = 'TaskDueDate' THEN TaskDueDate END,
CASE WHEN @Desc = 1 AND @OrderBy = 'TaskDueDate' THEN TaskDueDate END DESC,
CASE WHEN @Desc = 0 AND @OrderBy = 'TaskFollowUpDate' THEN TaskFollowUpDate END,
CASE WHEN @Desc = 1 AND @OrderBy = 'TaskFollowUpDate' THEN TaskFollowUpDate END DESC,
CASE WHEN @Desc = 0 AND @OrderBy = 'TaskLastModifiedDate' THEN TaskStatus END,
CASE WHEN @Desc = 1 AND @OrderBy = 'TaskLastModifiedDate' THEN TaskStatus END DESC,
CASE WHEN @Desc = 0 AND @OrderBy = 'TaskAssignedUserFullName' THEN LoanNumCached END,
CASE WHEN @Desc = 1 AND @OrderBy = 'TaskAssignedUserFullName' THEN LoanNumCached END DESC,
CASE WHEN @Desc = 0 AND @OrderBy = 'TaskOwnerFullName' THEN BorrowerNmCached END,
CASE WHEN @Desc = 1 AND @OrderBy = 'TaskOwnerFullName' THEN BorrowerNmCached END DESC


END












