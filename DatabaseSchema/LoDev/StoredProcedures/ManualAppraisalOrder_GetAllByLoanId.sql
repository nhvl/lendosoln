ALTER PROCEDURE [dbo].[ManualAppraisalOrder_GetAllByLoanId]
	@LoanId uniqueidentifier,
	@BrokerId uniqueidentifier,
	@IsValid bit = null
AS
BEGIN
	SELECT a.*
	FROM APPRAISAL_ORDER a
	JOIN broker b on a.brokerid = b.brokerid
	WHERE b.status = 1
	AND a.LoanId = @LoanId
	AND a.BrokerId = @BrokerId
	AND (@IsValid IS NULL OR IsValid = @IsValid)
	AND AppraisalOrderType = 0	-- manual appraisals.

	SELECT
		aoe.EDocId, aoe.AppraisalOrderId
	FROM
		APPRAISAL_ORDER_EDOCS aoe JOIN
		APPRAISAL_ORDER ao ON aoe.AppraisalOrderId=ao.AppraisalOrderId
	WHERE
		ao.LoanId=@LoanId AND
		ao.BrokerId=@BrokerId AND
		(@IsValid IS NULL OR ao.IsValid=@IsValid) AND 
		ao.AppraisalOrderType = 0
	
END