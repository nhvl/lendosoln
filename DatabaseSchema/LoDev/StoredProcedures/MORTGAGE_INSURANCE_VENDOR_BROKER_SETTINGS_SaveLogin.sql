-- =============================================
-- Author:		Isaac Ribakoff
-- Create date: 9/19/2014
-- Description:	Saves MI Vendor Broker Settings
-- =============================================
ALTER PROCEDURE [dbo].[MORTGAGE_INSURANCE_VENDOR_BROKER_SETTINGS_SaveLogin]
	@VendorId uniqueidentifier,
	@BrokerId uniqueidentifier,
	@UserName varchar(50),
	@Password varbinary(max) = 0x, -- Not null column, but the code uses null
	@EncryptionKeyId uniqueidentifier,
	@AccountId varchar(50),
	@PolicyId varchar(50),
	@IsDelegated bit
AS
BEGIN
	UPDATE MORTGAGE_INSURANCE_VENDOR_BROKER_SETTINGS
	SET UserName = @UserName,
		Password = @Password,
		EncryptionKeyId = @EncryptionKeyId,
		AccountId = @AccountId,
		PolicyId = @PolicyId,
		IsDelegated = @IsDelegated
	WHERE VendorId = @VendorId
		AND BrokerId = @BrokerId
END
