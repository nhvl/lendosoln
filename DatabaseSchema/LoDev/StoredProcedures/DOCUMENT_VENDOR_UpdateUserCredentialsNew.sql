-- =============================================
-- Author:		paoloa
-- Create date: 7/2013
-- Description:	
-- =============================================
ALTER PROCEDURE [dbo].[DOCUMENT_VENDOR_UpdateUserCredentialsNew]
	@UserId UniqueIdentifier,
	@VendorId uniqueidentifier,
	@BrokerId uniqueidentifier,
	@Login varchar(50) = '',
	@Password varchar(150) = '',
	@AccountId varchar(50) = ''
AS
BEGIN
	UPDATE DOCUMENT_VENDOR_USER_ENABLED_CREDENTIALS
	SET AccountId = @AccountId,
		[Login] = @Login,
		[Password] = @Password
	WHERE UserId = @UserId and VendorId = @VendorId
	
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO DOCUMENT_VENDOR_USER_ENABLED_CREDENTIALS(UserId, VendorId, BrokerId, [Login], [Password], AccountId)
		VALUES (@UserId, @VendorId, @BrokerId, @Login, @Password, @AccountId)
	END
	
END
