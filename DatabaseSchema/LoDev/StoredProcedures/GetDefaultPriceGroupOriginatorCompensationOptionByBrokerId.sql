
create procedure [dbo].[GetDefaultPriceGroupOriginatorCompensationOptionByBrokerId]
@BrokerId uniqueidentifier
as
begin

declare @LenderPaidOriginatorCompensationOptionT int

set @LenderPaidOriginatorCompensationOptionT = 
( 
	select LenderPaidOriginatorCompensationOptionT 
	from 
	lpe_price_group pg join broker b on b.BrokerLpePriceGroupIdDefault = pg.LpePriceGroupId
	where b.brokerId = @brokerid
)

select COALESCE(@LenderPaidOriginatorCompensationOptionT, 1) as LenderPaidOriginatorCompensationOptionT

end