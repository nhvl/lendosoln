-- =============================================
-- Author:		Justin Lara
-- Create date: 3/9/2015
-- Description:	Loads a set of generic framework
--              credentials for the given employee.
-- =============================================
CREATE PROCEDURE [dbo].[GENERIC_FRAMEWORK_VENDOR_EMPLOYEE_RetrieveLogin] 
	-- Add the parameters for the stored procedure here
	@EmployeeId uniqueidentifier,
	@ProviderId varchar(7),
	@BrokerId uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT Username
	FROM GENERIC_FRAMEWORK_VENDOR_EMPLOYEE_LOGIN
	WHERE EmployeeId = @EmployeeId 
		AND ProviderId = @ProviderId 
		AND BrokerId = @BrokerId
END
