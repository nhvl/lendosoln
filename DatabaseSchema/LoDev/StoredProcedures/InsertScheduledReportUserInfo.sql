CREATE PROCEDURE [dbo].[InsertScheduledReportUserInfo]
	@UserId AS UNIQUEIDENTIFIER, 
	@Email AS VARCHAR(200), 
	@CcIds AS VARCHAR(400) = NULL, 
	@ScheduleName AS VARCHAR(30), 
	@QueryId AS UNIQUEIDENTIFIER, 
	@ReportType AS VARCHAR(10)
AS
INSERT INTO SCHEDULED_REPORT_USER_INFO (UserId,
										Email, 
										CcIds, 
										ScheduleName, 
										QueryId, 
										ReportType)
VALUES( @UserId, 
		@Email, 
		@CcIds, 
		@ScheduleName, 
		@QueryId, 
		@ReportType);