/*
	Author: 
		Scott Kibler
	Description: 
		Sets the files to be used for the specified integration request for the specified vendor.
		Currently this feature only allows 0-1 vendors + integration request types being enabled
		so this sproc also deletes any other records in the table.
	opm:
		463659
	Remarks:
		This sproc will need to be deleted at a later phase of this case.
*/
CREATE PROCEDURE [dbo].[TEMP_SetOnlyVendorIntegrationRequestFormat]
	@VendorId uniqueidentifier,
	@IntegrationRequestType int,
	@LoXmlFileId int,
	@XslFileId int
AS
BEGIN

	DECLARE @ErrorMsg VARCHAR(255)
	DECLARE @ErrorCode VARCHAR(255)
	DECLARE @NumFormats INT
	SELECT @NumFormats = COUNT(*) FROM VENDOR_INTEGRATION_REQUEST_FORMAT
	IF @NumFormats > 1
	BEGIN
		SET @ErrorMsg = 'There is more than 1 record in VENDOR_INTEGRATION_REQUEST_FORMAT.  DELETE sproc or records manually.'
		SET @ErrorCode = -1
		PRINT @ErrorMsg
		RAISERROR(@ErrorMsg, 16, 1)
		RETURN @ErrorCode;
	END	
	
	BEGIN TRANSACTION
		DELETE FROM VENDOR_INTEGRATION_REQUEST_FORMAT
		IF(0 <> @@Error)
		BEGIN
			SET @ErrorCode = -38
			SET @ErrorMsg = 'Unable to delete from VENDOR_INTEGRATION_REQUEST_FORMAT'
			GOTO TransactionalErrorHandler;
		END
		
		INSERT INTO 
			VENDOR_INTEGRATION_REQUEST_FORMAT 
				( VendorId,  IntegrationRequestType,  LoXmlFileId,  XslFileId) 
			VALUES
				(@VendorId, @IntegrationRequestType, @LoXmlFileId, @XslFileId)
		IF(0 <> @@Error)
		BEGIN
			SET @ErrorCode = -50
			SET @ErrorMsg = 'Unable to INSERT format into VENDOR_INTEGRATION_REQUEST_FORMAT'
			GOTO TransactionalErrorHandler
		END
	COMMIT TRANSACTION
	RETURN 0;
	
	TransactionalErrorHandler:
		PRINT @ErrorMsg
		RAISERROR(@ErrorMsg, 16, 1)
		RETURN @ErrorCode;
END