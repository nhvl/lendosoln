
-- =============================================
-- Author:		Antonio Valencia
-- Create date: September 14, 2010
-- Description:	Gets most recent configurations for each organization
-- Modified-by: Matthew Pham
-- Mod. date:   April 25, 2011
-- =============================================
ALTER PROCEDURE [dbo].[CONFIG_LoadLatestReleases] 
	@OrgId uniqueidentifier = null 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	IF @OrgId IS NOT NULL
	BEGIN
		SELECT TOP 1 ConfigId, OrgId, ConfigurationXmlContent, ModifyingUserId, ReleaseDate, CompiledConfiguration 
		FROM CONFIG_RELEASED 
		WHERE OrgId = @OrgId
		ORDER BY ReleaseDate DESC
    END
	ELSE
	BEGIN
		SELECT c.OrgId as OrgId, c.ConfigId, c.ConfigurationXmlContent as ConfigurationXmlContent, c.CompiledConfiguration AS CompiledConfiguration, c.ModifyingUserId as ModifyingUserId, c.ReleaseDate as ReleaseDate  FROM 
			CONFIG_RELEASED AS c 
				JOIN ( 
					SELECT DISTINCT OrgId, max(ConfigID) as Id
					FROM CONFIG_RELEASED
					GROUP BY orgid 
				) AS t ON  c.ConfigId = t.Id
	END

END

