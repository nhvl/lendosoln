CREATE PROCEDURE [dbo].[BrokerUser_FindEmployeesWithGivenSupportEmail]
	@Email varchar(320)
AS
	SELECT EmployeeId FROM BROKER_USER WHERE SupportEmail = @Email
