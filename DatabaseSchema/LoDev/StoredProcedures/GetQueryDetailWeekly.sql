-- =============================================
-- Author:		Long Nguyen
-- Create date: 8/4/2015
-- Description:	Update schedule report recovery (iOPM 215368): Get detail at current hour
-- =============================================
ALTER procedure [dbo].[GetQueryDetailWeekly]
as 
BEGIN
	select q.QueryName, q.BrokerId, s.*, a.IsActive 
	from 
		SCHEDULED_REPORT_USER_INFO s 
		JOIN REPORT_QUERY q ON s.QueryId = q.QueryId 
		JOIN ALL_USER a on a.userid = s.userid 
		JOIN Broker b ON q.BrokerId = b.BrokerId
	where 
		b.Status = 1 AND
		ReportId in(select ReportId 
					from WEEKLY_REPORTS 
					where NextRun >= dateadd(day, -14, getdate()) and NextRun <= dateadd(minute, 20, getdate()))
END
