-- =============================================
-- Author:		paoloa
-- Create date: 4/19/2013
-- Description:	
-- =============================================
CREATE PROCEDURE dbo.LockPolicy_DuplicateHolidays
	-- Add the parameters for the stored procedure here
	@ToPolicy uniqueidentifier,
	@FromPolicy uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    INSERT INTO LENDER_LOCK_POLICY_HOLIDAY(LockPolicyId, BrokerId, ClosureDate, IsClosedAllDay, LockDeskOpenTime, LockDeskCloseTime)
	SELECT @ToPolicy, BrokerId, ClosureDate, IsClosedAllDay, LockDeskOpenTime, LockDeskCloseTime
	FROM LENDER_LOCK_POLICY_HOLIDAY
	WHERE LockPolicyId = @FromPolicy
END
