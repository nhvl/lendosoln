-- =============================================
-- Author:		Matthew Pham
-- Create date: 6/20/12
-- Description:	
-- =============================================
CREATE PROCEDURE dbo.INTERNAL_USE_ONLY_ListAllTitleServicesRoles 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
	v.RoleDesc
	, v.RoleId
	, v.BrokerId
	, v.EmployeeId
	FROM 
	View_Employee_Role v with (nolock)
	WHERE 
	v.IsActive = '1' AND (v.RoleDesc = 'Processor' or v.RoleDesc = 'Closer' or v.RoleDesc = 'Manager' )
END
