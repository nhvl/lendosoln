-----------------------------------------------------------------
-- sIsApprSentToBorr is set to 1 when there is:                --
-- * At least 1 valid Appraisal Order AND                      --
-- * All valid appraisal orders have thier SentToBorrowerDate  --
--   filled out.                                               --
-----------------------------------------------------------------
ALTER PROCEDURE [dbo].[APPRAISAL_ORDER_Update_sIsApprSentToBorr]
	@BrokerId uniqueidentifier,
	@LoanId uniqueidentifier
AS
BEGIN
	DECLARE @TotalValidOrders int
	DECLARE @UnsetValidOrders int
	DECLARE @UpdateValue bit

	SELECT @TotalValidOrders = COUNT(*),
		@UnsetValidOrders = SUM(CASE WHEN a.SentToBorrowerDate IS NULL THEN 1 ELSE 0 END)
	FROM APPRAISAL_ORDER a
		JOIN BROKER b ON a.brokerid = b.brokerid
		WHERE b.status = 1
		AND a.BrokerId = @BrokerId
		AND LoanId = @LoanId
		AND IsValid = 1

	SET @UpdateValue = CASE WHEN @TotalValidOrders > 0 AND @UnsetValidOrders = 0 THEN 1 ELSE 0 END
	
	UPDATE LOAN_FILE_CACHE_4
	SET sIsApprSentToBorr = @UpdateValue
	WHERE sLId = @LoanId
END
