-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[TaskTriggerTemplate_ListLoanToProcessNightlyByBrokerId]
	@sBrokerId UniqueIdentifier
	
AS
BEGIN
	-- List loans that are not in Loan Canceled, Loan Rejected, Lead Canceled and Lead Rejected.
	SELECT sLId, sBrokerId
	FROM LOAN_FILE_CACHE lc
	WHERE lc.IsValid=1 AND lc.IsTemplate=0
		AND lc.sStatusD BETWEEN dateadd(yy, -1, GETDATE()) AND GETDATE()
		AND sBrokerId = @sBrokerId
		AND lc.sLoanFileT = 0

END
