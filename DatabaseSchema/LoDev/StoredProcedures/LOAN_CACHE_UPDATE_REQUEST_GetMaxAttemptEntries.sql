ALTER PROCEDURE [dbo].[LOAN_CACHE_UPDATE_REQUEST_GetMaxAttemptEntries] 
AS
  SELECT COUNT(*) as [Count], min(FirstOccurD) as FirstOccurD FROM LOAN_CACHE_UPDATE_REQUEST
  WHERE NumRetries >= 5 

