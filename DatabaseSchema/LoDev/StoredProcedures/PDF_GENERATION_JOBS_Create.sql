-- =============================================
-- Author:		Michael Leinweaver
-- Create date: 9/21/2018
-- Description:	Creates a PDF background job.
-- =============================================
CREATE PROCEDURE [dbo].[PDF_GENERATION_JOBS_Create]
	@JobId int,
	@BrokerId uniqueidentifier,
	@UserId uniqueidentifier,
	@UserType char(1),
	@PdfGenerationJobType tinyint,
	@PdfName varchar(100) = NULL,
	@SerializedData varchar(max),
	@EncryptionKeyId uniqueidentifier,
	@EncryptedPassword varbinary(max) = NULL,
	@PollingIntervalInSeconds int
AS
BEGIN
	INSERT INTO PDF_GENERATION_JOBS (
		JobId, 
		BrokerId, 
		UserId, 
		UserType, 
		PdfGenerationJobType, 
		PdfName, 
		SerializedData, 
		EncryptionKeyId, 
		EncryptedPassword, 
		PollingIntervalInSeconds
	)
	VALUES (
		@JobId, 
		@BrokerId, 
		@UserId, 
		@UserType, 
		@PdfGenerationJobType, 
		@PdfName, 
		@SerializedData, 
		@EncryptionKeyId, 
		@EncryptedPassword, 
		@PollingIntervalInSeconds
	)
END
