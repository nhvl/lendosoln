CREATE procedure [dbo].[EDOCS_ShippingTemplateAddInternalCheckName]
	@ShippingTemplateName varchar(50),
	@List varchar(max)
as

select 1
	from EDOCS_SHIPPING_TEMPLATE 
	where ShippingTemplateName = @ShippingTemplateName
		and BrokerId is null