CREATE PROCEDURE [dbo].[RetrieveQueryIdByQueryNameAndBrokerId]
	@BrokerId uniqueidentifier,
	@QueryName varchar(100)
AS
BEGIN

	SELECT QueryId
	FROM Report_Query
	WHERE BrokerId = @BrokerId
	AND
	QueryName = @QueryName
END
