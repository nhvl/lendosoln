-- =============================================
-- Author:		Eric Mallare
-- Create date: 8/17/2017
-- Description:	Updates a title vendor
-- =============================================
ALTER PROCEDURE [dbo].[TITLE_UpdateVendor]
	@VendorId int,
	@CompanyName varchar(150),
	@IsEnabled bit,
	@IsTestVendor bit,
	@QuotingPlatformId int = null,
	@IsOverridingQuotingPlatform bit = null,
	@PolicyOrderingPlatformId int = null,
	@QuotingTransmissionId int = null,
	@IsOverridingPolicyOrderingPlatform bit = null,
	@PolicyOrderingTransmissionId int = null,
	@IsQuickQuotingEnabled bit,
	@IsDetailedQuotingEnabled bit,
	@IsPolicyOrderingEnabled bit,
	@UsesProviderCodes bit
AS
BEGIN
	UPDATE
		TITLE_FRAMEWORK_VENDOR
	SET
		CompanyName=@CompanyName, 
		IsEnabled=@IsEnabled, 
		IsTestVendor=@IsTestVendor, 
		QuotingPlatformId=@QuotingPlatformId,
		IsOverridingQuotingPlatform=@IsOverridingQuotingPlatform,
		PolicyOrderingPlatformId=@PolicyOrderingPlatformId,
		QuotingTransmissionId=@QuotingTransmissionId,
		IsOverridingPolicyOrderingPlatform=@IsOverridingPolicyOrderingPlatform,
		PolicyOrderingTransmissionId=@PolicyOrderingTransmissionId,
		IsQuickQuotingEnabled=@IsQuickQuotingEnabled,
		IsDetailedQuotingEnabled=@IsDetailedQuotingEnabled,
		IsPolicyOrderingEnabled=@IsPolicyOrderingEnabled,
		UsesProviderCodes=@UsesProviderCodes
	WHERE 
		VendorId=@VendorId
END
