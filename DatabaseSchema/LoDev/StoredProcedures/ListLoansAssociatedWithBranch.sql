






CREATE     PROCEDURE [dbo].[ListLoansAssociatedWithBranch]
	@BrokerId uniqueidentifier, 
	@BranchId uniqueidentifier,
	@Validity bit
AS
	SELECT TOP 100
		sLId,
		sLNm ,
		dbo.GetLoanStatusName(sStatusT) AS LoanStatus,
		aBLastNm,
		aBFirstNm,
		sStatusD
	FROM Loan_File_Cache WITH (NOLOCK)
	WHERE sBrokerID = @BrokerId AND sBranchID = @BranchId AND IsValid = @Validity

