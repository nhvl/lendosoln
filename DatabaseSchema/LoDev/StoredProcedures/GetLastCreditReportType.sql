CREATE PROCEDURE [dbo].[GetLastCreditReportType] 
	@ApplicationID uniqueidentifier

AS
	SELECT TOP 1 FileType
	FROM SERVICE_FILE
	WHERE	Owner = @ApplicationID
        AND IsInEffect = 1
		AND ServiceType = 'Credit'
		AND (FileType = 'CreditReport'
			OR FileType = 'LQICreditReport')
	ORDER BY TimeVersion DESC
	
	if( 0!=@@error)
	begin
		RAISERROR('Error in the select statement in RetrieveCreditReport sp', 16, 1);
		return -100;
	end
	return 0;