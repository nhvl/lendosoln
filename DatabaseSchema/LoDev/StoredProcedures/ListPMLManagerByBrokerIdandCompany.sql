ALTER procedure [dbo].[ListPMLManagerByBrokerIdandCompany]
	@BrokerId uniqueidentifier,
	@IsActive bit = NULL,
	@PmlBrokerId uniqueidentifier = NULL,
	@NameFilter varchar(32) = NULL, 
	@LastFilter varchar(32) = NULL,
	@ExSupervisorEmployeeIdsToIgnore xml = NULL
as
	DECLARE @IgnoreManagerIds TABLE(EmployeeId uniqueidentifier)
	
	IF @ExSupervisorEmployeeIdsToIgnore IS NOT NULL
	BEGIN
		INSERT INTO @IgnoreManagerIds(EmployeeId)
		SELECT T.Item.value('.', 'uniqueidentifier') 
		FROM @ExSupervisorEmployeeIdsToIgnore.nodes('root/id') as T(Item)
	END

	SELECT
		e.EmployeeId,
		e.UserFirstNm + ' ' + e.UserLastNm AS UserFullName,
		COALESCE(pb.Name, '') AS CompanyName,
		r.BranchNm 
	FROM
		Employee AS e WITH (NOLOCK) JOIN Branch AS r WITH (NOLOCK) ON r.BranchId = e.BranchId
		LEFT JOIN Broker_User bu WITH(NOLOCK) ON e.EmployeeUserId = bu.UserId
		LEFT JOIN PML_Broker pb WITH (NOLOCK) ON pb.BrokerId = r.BrokerId AND bu.PmlBrokerId = pb.PmlBrokerId
	WHERE
		e.EmployeeId NOT IN (SELECT EmployeeId FROM @IgnoreManagerIds)
		AND
		e.IsPmlManager = 1
		AND
		r.BrokerId = @BrokerId
		AND
		pb.PmlBrokerId = COALESCE(@PmlBrokerId, pb.PmlBrokerId)
		AND
		e.IsActive = COALESCE(@IsActive, e.IsActive)
		AND
		(
			(
				e.UserFirstNm LIKE @NameFilter + '%'
				AND
				e.UserLastNm LIKE @LastFilter + '%'
				AND
				@NameFilter IS NOT NULL
				AND
				@LastFilter IS NOT NULL
			)
			OR
			(
				(
					e.UserFirstNm LIKE @NameFilter + '%'
					OR
					e.UserLastNm LIKE @NameFilter + '%'
				)
				AND
				@NameFilter IS NOT NULL
				AND
				@LastFilter IS NULL
			)
			OR
			(
				@NameFilter IS NULL
				AND
				@LastFilter IS NULL
			)
		)
	ORDER BY
		e.UserLastNm , e.UserFirstNm
	if( 0!=@@error)
	begin
		RAISERROR('Error in the selection statement in ListPriceMyLoanManagerByBrokerId sp', 16, 1);
		return -100;
	end
	
	return 0;