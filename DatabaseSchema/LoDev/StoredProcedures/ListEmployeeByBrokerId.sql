ALTER PROCEDURE [dbo].[ListEmployeeByBrokerId] 
	@BrokerId UniqueIdentifier,
	@IsActiveFilter bit
AS
BEGIN

SELECT 
	e.EmployeeId, 
	e.UserFirstNm, 
	e.UserLastNm, 
	e.IsActive, 
	e.EmployeeUserId, 
	e.Email,
	br.BranchNm,
	bu.IsAccountOwner,
	bu.Permissions,
	au.LoginNm,
	au.IsActive AS CanLogin, 
	au.Type,
	l.LicenseNumber,
	e.UseOriginatingCompanyBranchId,
	bu.PmlBrokerId,
	e.PopulatePmlPermissionsT
FROM Employee e JOIN Branch br ON e.BranchId = br.BranchId		
                LEFT JOIN Broker_User bu ON e.EmployeeUserId = bu.UserId
                LEFT JOIN All_User au ON e.EmployeeUserId = au.UserId
                LEFT JOIN License l ON bu.CurrentLicenseId = l.LicenseId
WHERE BrokerId=@BrokerId
  AND e.IsActive=@IsActiveFilter
  AND ((au.Type IS NULL) OR (au.Type = 'B'))
END
