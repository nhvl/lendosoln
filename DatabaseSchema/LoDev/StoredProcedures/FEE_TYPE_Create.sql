CREATE PROCEDURE [dbo].[FEE_TYPE_Create]
	@BrokerId		uniqueidentifier,
	@FeeTypeId		uniqueidentifier OUTPUT,
	@Description	varchar(max) = '',
	@GfeLineNumber	varchar(5) = '',
	@GfeSection		int = 0,
	@Props			int = 0
AS
	DECLARE @TempTable TABLE (FeeTypeId uniqueidentifier)

	INSERT INTO FEE_TYPE (
		BrokerId,
		[Description],
		GfeLineNumber,
		GfeSection,
		Props
	)
	OUTPUT INSERTED.FeeTypeId INTO @TempTable
	VALUES (
		@BrokerId,
		@Description,
		@GfeLineNumber,
		@GfeSection,
		@Props
	)
	
	SELECT @FeeTypeId = FeeTypeId FROM @TempTable
	
	IF 0 != @@ERROR
	BEGIN
		RAISERROR('Error inserting into FEE_TYPE in Create sp', 16, 1);
		rollback transaction
		return -100
	END

	RETURN 0;