create PROCEDURE ListOpenActiveLicense
	@BrokerId Guid
AS
	SELECT * 
	FROM LICENSE 
	WHERE LicenseOwnerBrokerId=@BrokerId AND OpenSeatCount > 0 AND ExpirationDate > GETDATE()
