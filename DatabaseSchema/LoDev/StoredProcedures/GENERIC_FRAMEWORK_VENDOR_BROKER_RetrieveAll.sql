-- =============================================
-- Author:		Timothy Jewell
-- Create date: 9/9/2014
-- Description:	Retrieves all Broker-level data for all Generic Framework vendors on a specified Broker.
-- Due to the DB split, global-level configuration will have to be retrieved separately.
-- =============================================
ALTER PROCEDURE [dbo].[GENERIC_FRAMEWORK_VENDOR_BROKER_RetrieveAll]
	-- Add the parameters for the stored procedure here
	@BrokerID uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Insert statements for procedure here
	SELECT
		ProviderID,
		CredentialXML,
		EncryptionKeyId,
		LaunchLinkConfigurationXML
	FROM GENERIC_FRAMEWORK_VENDOR_BROKER_CONFIG
	WHERE BrokerID = @BrokerID
END
