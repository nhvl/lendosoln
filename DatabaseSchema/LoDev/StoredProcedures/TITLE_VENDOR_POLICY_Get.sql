-- =============================================
-- Author:		Antonio Valencia
-- Create date: 6/18/2013
-- Description:	Get Policies
-- =============================================
CREATE PROCEDURE dbo.TITLE_VENDOR_POLICY_Get
	-- Add the parameters for the stored procedure here --
	@State varchar(2),
	@TitleVendorId  int 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT  State, TitleVendorId, PolicyId, PolicyName, PolicyType FROM TITLE_VENDOR_POLICY where TItlevendorId = @TitleVendorId and State = @State
END
