-- =============================================
-- Author:		paoloa
-- Create date: 5/1/2013
-- Description:	
-- =============================================
ALTER PROCEDURE [dbo].[GetAppraisalOrdersByVendorId]
	-- Add the parameters for the stored procedure here
	@VendorId uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT *
	FROM APPRAISAL_ORDER_INFO
	WHERE VendorId = @VendorId
END
