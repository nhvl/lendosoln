ALTER PROCEDURE [dbo].[CUSTOM_FOLDER_X_LOAN_TYPE_SAVE]
	@CustomFolderXLoanType CustomFolderXLoanTypeTableType READONLY,
	@FolderIds IntTableType READONLY
AS
BEGIN
	WITH 
		CUSTOM_FOLDER_X_LOAN_TYPE_PARTIAL AS
		(
			SELECT p.FolderId, p.LoanType
			FROM CUSTOM_FOLDER_X_LOAN_TYPE as p
			JOIN @FolderIds as c ON p.FolderId = c.IntValue
		)
	MERGE CUSTOM_FOLDER_X_LOAN_TYPE_PARTIAL AS TARGET
	USING @CustomFolderXLoanType AS SOURCE
	ON (TARGET.FolderId = SOURCE.FolderId AND TARGET.LoanType = SOURCE.LoanType)		
		WHEN NOT MATCHED BY TARGET THEN 
			INSERT
			(
				FolderId,
				LoanType
			)
			VALUES
			(
				SOURCE.FolderId,
				SOURCE.LoanType
			)		

		WHEN NOT MATCHED BY SOURCE THEN
		DELETE;
END
