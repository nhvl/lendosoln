-- =============================================
-- Author:		Thien Nguyen
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[FLOOD_RESELLER_Insert]
	@FloodResellerId uniqueidentifier,
    @Name varchar(200),
    @Url varchar(500)

AS
BEGIN

	INSERT INTO FLOOD_RESELLER (FloodResellerId, Name, Url)
					   VALUES(@FloodResellerId, @Name, @Url)

END
