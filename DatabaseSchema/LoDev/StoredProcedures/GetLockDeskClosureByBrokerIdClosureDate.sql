

CREATE PROCEDURE [dbo].[GetLockDeskClosureByBrokerIdClosureDate]
	@BrokerId uniqueidentifier,
	@ClosureDate Datetime
AS
BEGIN
	SELECT ClosureDate, IsClosedAllDay, LockDeskOpenTime, LockDeskCloseTime
	FROM LockDesk_Closure
	WHERE BrokerId = @BrokerId and ClosureDate = @ClosureDate
END
