

CREATE PROCEDURE [dbo].[RemovePerFieldSecurityEntry] 
            @FieldId varchar(100)
AS
	DELETE FROM LOAN_FIELD_SECURITY
	WHERE sFieldId = @FieldId
	
	if( 0!=@@error)
	begin
		RAISERROR('Error in updating LOAN_FIELD_SECURITY table in RemovePerFieldSecurityEntry sp', 16, 1);
		return -100;
	end
	return 0;


