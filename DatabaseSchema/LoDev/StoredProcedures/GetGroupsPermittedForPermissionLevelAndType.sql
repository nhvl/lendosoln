-- =============================================
-- Author: Scott Kibler
-- Create date: 5/9/2017
-- Description: 
--	Gets the group ids for a particular permission level and type.
-- =============================================
CREATE PROCEDURE [dbo].[GetGroupsPermittedForPermissionLevelAndType]
	@BrokerId uniqueidentifier,
	@PermissionLevelId INT,
	@PermissionType INT
AS
BEGIN
	SELECT GroupId
	FROM CONVOLOG_PERMISSION_GROUP
	WHERE 
			BrokerId = @BrokerId
		AND PermissionLevelId = @PermissionLevelId
		AND PermissionType = @PermissionType	
END