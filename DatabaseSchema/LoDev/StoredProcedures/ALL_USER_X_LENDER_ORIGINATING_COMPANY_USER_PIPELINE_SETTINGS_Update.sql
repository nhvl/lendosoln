-- =============================================
-- Author:		Michael Leinweaver
-- Create date: 6/5/2018
-- Description:	Updates pipeline settings for a TPO user.
-- =============================================
ALTER PROCEDURE [dbo].[ALL_USER_X_LENDER_ORIGINATING_COMPANY_USER_PIPELINE_SETTINGS_Update]
	@UserId uniqueidentifier,
	@BrokerId uniqueidentifier,
	@PortalMode tinyint,
	@NewReportIdXml xml
AS
BEGIN
	DELETE FROM ALL_USER_X_LENDER_ORIGINATING_COMPANY_USER_PIPELINE_SETTINGS
	WHERE 
		UserId = @UserId AND 
		BrokerId = @BrokerId AND 
		PortalMode = @PortalMode
	
	;WITH NEW_REPORT_IDS AS (
		SELECT T.item.value('.', 'uniqueidentifier') as ReportId
		FROM @NewReportIdXml.nodes('root/id') as T(Item)
	)
	INSERT INTO ALL_USER_X_LENDER_ORIGINATING_COMPANY_USER_PIPELINE_SETTINGS(UserId, BrokerId, PortalMode, SettingId)
	SELECT @UserId, @BrokerId, @PortalMode, settings.SettingId
	FROM 
		LENDER_ORIGINATING_COMPANY_USER_PIPELINE_SETTINGS settings 
		JOIN NEW_REPORT_IDS newIds ON settings.QueryId = newIds.ReportId
	WHERE settings.BrokerId = @BrokerId
END
