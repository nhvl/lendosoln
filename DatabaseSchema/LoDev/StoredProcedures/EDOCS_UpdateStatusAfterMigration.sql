-- =============================================
-- Author:		David Dao
-- Create date: 
-- Description:	
-- =============================================
ALTER PROCEDURE dbo.EDOCS_UpdateStatusAfterMigration 
	@BrokerId UniqueIdentifier,
	@DocId uniqueidentifier, 
	@ImageStatus tinyint,
	@MetadataJsonContent varchar(max),
	@MetadataProtobufContent varbinary(max) = NULL

AS
BEGIN

    UPDATE EDOCS_DOCUMENT WITH(ROWLOCK, UPDLOCK)
    SET FileDBKey_CurrentRevision = '00000000-0000-0000-0000-000000000000', 
    ImageStatus = @ImageStatus,
    MetadataJsonContent = @MetadataJsonContent,
	TimeToRequeueForImageGeneration  = NULL,
	MetadataProtobufContent = @MetadataProtobufContent
    WHERE BrokerId = @BrokerId AND DocumentId = @DocId 

END
