

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[QuickPricerLoanPool_ReleaseLoan]
	@sLId UniqueIdentifier,
	@BrokerId UniqueIdentifier 
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL REPEATABLE READ
	BEGIN TRANSACTION
		EXEC DeclareLoanFileValid @sLId = @sLId, @IsValid = 0

		DECLARE @LockResult Int
		EXEC @LockResult = sp_getapplock @Resource='QUICK_PRICER_LOAN_POOL', @LockMode = 'Exclusive'
		IF @LockResult < 0
		BEGIN
			RAISERROR('Unable to lock QUICK_PRICER_LOAN_POOL resource.', 16, 1)
  			RETURN
 		END 

		UPDATE QUICK_PRICER_LOAN_POOL
		SET IsInUsed = 0
		WHERE BrokerId = @BrokerId AND sLId = @sLId
	COMMIT TRANSACTION;
END


