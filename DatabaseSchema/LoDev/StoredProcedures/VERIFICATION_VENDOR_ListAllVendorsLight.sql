-- =============================================
-- Author:		Eric Mallare
-- Create date: 4/28/2017
-- Description:	Gets a small set of vendor info
-- =============================================
CREATE PROCEDURE [dbo].[VERIFICATION_VENDOR_ListAllVendorsLight]
AS
BEGIN
	SELECT
		v.VendorId, v.CompanyName, v.IsEnabled, v.IsTestVendor, v.CanPayWithCreditCard, v.PlatformId, p.RequiresAccountId, p.UsesAccountId
	FROM
		VERIFICATION_VENDOR v LEFT JOIN
		VERIFICATION_PLATFORM p ON v.PlatformId=p.PlatformId
END
