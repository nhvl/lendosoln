
CREATE PROCEDURE GetLicenseIdFromRenewLicenseId
	@RenewLicenseId Guid
AS
BEGIN
	SELECT LicenseId FROM License WHERE RenewLicenseId=@RenewLicenseId
END
