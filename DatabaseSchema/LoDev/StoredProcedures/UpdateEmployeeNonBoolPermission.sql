CREATE PROCEDURE [dbo].[UpdateEmployeeNonBoolPermission]
	@EmployeeID uniqueidentifier,
	@NonBoolPermission char(100),
	@BrokerID uniqueidentifier
AS
	UPDATE Broker_User
	       SET NonBoolPermissions = @NonBoolPermission
	 WHERE EmployeeID = @EmployeeID
	
	if(0!=@@error)
	begin
		RAISERROR('Error in the update statement in UpdateEmployeePermission sp', 16, 1);
		return -100;
	end
	return 0;
