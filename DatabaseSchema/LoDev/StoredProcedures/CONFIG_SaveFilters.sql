-- =============================================
-- Author:		Matthew Pham
-- Create date: 01/17/2011
-- Description:	Saves new filters
-- =============================================
CREATE PROCEDURE [dbo].[CONFIG_SaveFilters]
	@OrgId uniqueidentifier,
	@ConfigurationFilterXmlContent varchar(max)

AS
BEGIN
	SET NOCOUNT ON;

	-- Here we always assume that the row for the @OrgId has already been created
	UPDATE CONFIG_DRAFT 
	SET ConfigurationFilterXmlContent = @ConfigurationFilterXmlContent
	WHERE OrgId = @OrgId 
		
END