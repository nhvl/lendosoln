-- =============================================
-- Author:		David Dao
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE dbo.DB_CONNECTION_RetrieveByBrokerId
    @BrokerId UniqueIdentifier
AS
BEGIN
    SELECT DbConnectionId FROM DB_CONNECTION_x_BROKER
    WHERE BrokerId = @BrokerId

END
