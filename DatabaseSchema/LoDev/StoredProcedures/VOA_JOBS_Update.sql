-- =============================================
-- Author:		Eric Mallare
-- Create date: 10/16/2018
-- Description:	Updates a VOA job entry.
-- =============================================
ALTER PROCEDURE [dbo].[VOA_JOBS_Update]
	@JobId int,
	@ResponseFileDbGuidId uniqueidentifier = NULL,
	@JobRequestDataFileDbGuidId uniqueidentifier = NULL
AS
BEGIN
	UPDATE 
		VOA_JOBS
	SET
		ResponseFileDbGuidId=@ResponseFileDbGuidId,
		JobRequestDataFileDbGuidId=@JobRequestDataFileDbGuidId
	WHERE
		JobId=@JobId
END