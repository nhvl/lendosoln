-- =============================================
-- Author:		Brian Beery
-- Create date: 12/31/12
-- Description:	Pulls the user's flood credentials
-- =============================================
ALTER PROCEDURE [dbo].[Flood_RetrieveSavedAuthentication]
	@UserId UniqueIdentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT
		FloodLoginNm,
		FloodPassword,
		EncryptedFloodPassword,
		EncryptionKeyId
	FROM BROKER_USER
	WHERE UserId = @UserId
END
