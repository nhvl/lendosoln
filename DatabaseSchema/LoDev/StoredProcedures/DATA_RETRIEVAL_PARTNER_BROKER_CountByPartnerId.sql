-- =============================================
-- Author:		Brian Beery
-- Create date: 09/24/2013
-- Description:	Returns the number of lenders associated with a Data Retrieval Framework partner
-- =============================================
CREATE PROCEDURE [dbo].[DATA_RETRIEVAL_PARTNER_BROKER_CountByPartnerId] 
	@PartnerId UniqueIdentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT COUNT(BrokerId) AS NumLenders
	FROM DATA_RETRIEVAL_PARTNER_BROKER
	WHERE PartnerId = @PartnerId
END