-- =============================================
-- Author:		David Dao
-- Create date: 
-- Description:	Retrieve a list of all brokers. Only return most basic information: Name, Customer Code, Status
-- =============================================
CREATE PROCEDURE dbo.BROKER_ListAll 

AS
BEGIN

    SELECT BrokerId, BrokerNm, CustomerCode, Status
    FROM BROKER

END
