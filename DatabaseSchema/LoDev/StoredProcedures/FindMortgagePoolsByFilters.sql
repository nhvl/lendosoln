-- =============================================
-- Author:		Paolo Arrastia
-- Create date: 1 Aug 2012
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[FindMortgagePoolsByFilters] 
	-- Add the parameters for the stored procedure here
	@PoolStatus bit = null,
	@AgencyT int = NULL,
	@AmortT int = NULL,
	@BrokerId UniqueIdentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

    -- Insert statements for procedure here
	SELECT PoolId
	FROM MORTGAGE_POOL
	WHERE
		(	
			((ClosedD IS NULL) AND (@PoolStatus = 1))
			OR
			((ClosedD IS NOT NULL) AND (@PoolStatus = 0))
			OR
			@PoolStatus IS NULL
		)
		AND
		(
			AgencyT = @AgencyT
			OR
			@AgencyT IS NULL
		)
		AND
		(
			AmortizationT = @AmortT
			OR
			@AmortT IS NULL
		)
		AND
			BrokerId = @BrokerId
		AND
		IsEnabled = 1
	--Make new pools (empty pool number) appear first
	ORDER BY (CASE WHEN BasePoolNumber LIKE '' THEN 0 ELSE 1 END), PoolId ASC;
END
