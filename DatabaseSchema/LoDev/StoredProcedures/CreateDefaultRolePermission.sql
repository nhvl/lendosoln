
ALTER     PROCEDURE [dbo].[CreateDefaultRolePermission]
	@RoleID uniqueidentifier,
	@BrokerID uniqueidentifier,
	@DefaultPermissions char(255),
	@DefaultNonBoolPermissions char(100) = ''
AS
	IF @RoleID != '00000000-0000-0000-0000-000000000000'
	BEGIN
		INSERT INTO ROLE_DEFAULT_PERMISSIONS(RoleId, BrokerId, DefaultPermissions, DefaultNonBoolPermissions)
		VALUES (@RoleID, @BrokerID, @DefaultPermissions, @DefaultNonBoolPermissions)
	END
	IF 0 != @@ERROR
	BEGIN
		RAISERROR('Error inserting into ROLE_DEFAULT_PERMISSIONS in CreateDefaultRolePermission sp', 16, 1);
		RETURN -100
	END
	RETURN 0;

