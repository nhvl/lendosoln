






CREATE  PROCEDURE [dbo].[CP_CreateSignEvent]

	@SignedTransactionId uniqueidentifier
	, @Signature varchar(50)
	, @SignedD datetime
	, @IpAddress varchar(15)
	, @EmailUsedToSign varchar(80)
	, @PdfFileDbKey uniqueidentifier


AS
INSERT INTO CP_SIGNED_EVENT
(
	SignedTransactionId
	, Signature
	, SignedD
	, IpAddress
	, EmailUsedToSign
	, PdfFileDbKey
)
           
VALUES
(
	@SignedTransactionId
	, @Signature
	, @SignedD
	, @IpAddress
	, @EmailUsedToSign
	, @PdfFileDbKey
)

IF ( 0 != @@ERROR )
BEGIN
	RAISERROR('Error in inserting into CP_SIGNED_EVENT  table in CP_CreateSignEvent sp', 16, 1);
	RETURN -100;
END
RETURN 0;







