-- =============================================
-- Author:		Huy Nguyen	
-- Create date: 5/5/2015
-- Description:	
-- =============================================
CREATE PROCEDURE dbo.ALL_USER_REGISTERED_MOBILE_DEVICE_ListByUser 
	@BrokerId UniqueIdentifier,
    @UserId UniqueIdentifier
AS
BEGIN
    SELECT Id,DeviceName,CreateD,LastUsedD
    FROM ALL_USER_REGISTERED_MOBILE_DEVICE
	WHERE BrokerId = @BrokerId
		AND UserId = @UserId
		AND IsActive = 1
END
