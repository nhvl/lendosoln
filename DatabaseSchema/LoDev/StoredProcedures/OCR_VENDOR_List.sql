ALTER PROCEDURE [dbo].[OCR_VENDOR_List]
	@VendorId uniqueidentifier = null
AS

	select VendorId, IsValid, DisplayName, PortalUrl, UploadURL, EnableConnectionTest
	from OCR_VENDOR_CONFIGURATION 
	where @VendorId is null OR @vendorId = vendorid
