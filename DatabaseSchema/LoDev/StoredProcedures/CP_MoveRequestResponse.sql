-- =============================================
-- Author:		Antonio Valencia
-- Create date: 9/17/2014
-- Description:	Moves Document Request Between Loans
-- =============================================
CREATE PROCEDURE [dbo].[CP_MoveRequestResponse] 
	@BrokerId uniqueidentifier,
	@src_sLId uniqueidentifier,
	@src_aAppId uniqueidentifier,
	@dst_sLId uniqueidentifier,
	@dst_aAppId uniqueidentifier
AS
BEGIN
	DECLARE @dst_BrokerId uniqueidentifier
	
	SELECT @dst_BrokerId = sBrokerId from loan_file_cache where slid = @dst_slid 
	
	IF (@dst_BrokerId <> @BrokerId) BEGIN
	RAISERROR('Cross broker check failed', 16, 1);
		RETURN -100
	END
	
	
	UPDATE CP_CONSUMER_ACTION_REQUEST
	SET 
		aAppId = @dst_aAppId,
		sLId = @dst_sLId
	WHERE sLId = @src_sLId AND aAppId = @src_aAppId  
	
	
END
