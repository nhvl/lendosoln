


CREATE PROCEDURE [dbo].[TEAM_ListUsersByTeam] 
	@TeamId		uniqueidentifier
AS
	SELECT
		e.EmployeeId,
		e.UserFirstNm,
		e.UserLastNm
		
	FROM Employee e
	LEFT JOIN TEAM_USER_ASSIGNMENT tua ON e.EmployeeId = tua.EmployeeId
	WHERE
	tua.TeamId = @TeamId

	

