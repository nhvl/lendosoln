-- =============================================
-- Author:		Antonio Valencia
-- Create date: 1/8/10
-- Description:	Invalidates a EDOcument
-- 7/12/2013: Added SQL to update sUnscreenedDocsInFile in LOAN_FILE_CACHE
-- =============================================
ALTER PROCEDURE [dbo].[EDOCS_InvalidateDoc] 
	-- Add the parameters for the stored procedure here
	@DocumentId uniqueidentifier, 
	@UserId	uniqueidentifier = null,
	@Reason varchar(200)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @BrokerId uniqueidentifier
	DECLARE @sLId uniqueidentifier 
	DECLARE @TotalNumPages int
	DECLARE @IsValid bit 
	DECLARE @Status tinyint
	

	
	BEGIN TRANSACTION	

	SELECT @BrokerId = BrokerId, @sLId = sLId, @TotalNumPages = NumPages, @IsValid = IsValid, @Status = [Status] FROM EDOCS_DOCUMENT  
	WHERE DocumentId = @DocumentId 
	IF @@error!= 0 GOTO HANDLE_ERROR
	
	UPDATE EDOCS_DOCUMENT 
	SET 
		IsValid = 0 
	WHERE DocumentId = @DocumentId
	IF @@error!= 0 GOTO HANDLE_ERROR
	
	-- update cached stats --
	-- Only need to update if we are deleting a document with Status = 0 --
	IF @sLId IS NOT NULL AND @Status = 0
	BEGIN
		UPDATE LOAN_FILE_CACHE_3
		SET sUnscreenedDocsInFile = CASE WHEN EXISTS(SELECT TOP(1) 1 FROM EDOCS_DOCUMENT
			WHERE sLId = @sLid AND IsValid = 1 AND [Status] = 0) THEN 1 ELSE 0 END
		WHERE sLId = @sLId
	END


	INSERT INTO EDocs_Audit_History( DocumentId, ModifiedByUserId, ModifiedDate, Description ) 
	VALUES ( @DocumentId, @UserId,  GETDATE(),  @Reason )
	IF @@error!= 0 GOTO HANDLE_ERROR

	COMMIT 
	RETURN

HANDLE_ERROR:
    ROLLBACK TRANSACTION
	RAISERROR('Failure invalidating electronic document.', 16, 1);
    RETURN 

END



