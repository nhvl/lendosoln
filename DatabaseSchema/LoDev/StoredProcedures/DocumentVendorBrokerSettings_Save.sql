-- =============================================
-- Author:		paoloa
-- Create date: 7/24/2013
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[DocumentVendorBrokerSettings_Save]
	-- Add the parameters for the stored procedure here
	@BrokerId uniqueidentifier,
	@VendorId uniqueidentifier,
	@Login varchar(50),
	@Password varchar(150),
	@AccountId varchar(50),
	@ServicesLinkNm varchar(50),
	@DisclosureInTest bit,
	@DisclosureBilling bit,
	@DisclosureOnlineInterface bit,
	@VendorIndex int,
	@IsEnableForProductionLoans bit,
	@IsEnableForTestLoans bit,
	@BlockManualFulfillment bit,
	@HasCustomPackageData bit = 0,
	@CustomPackageDataJSON varchar(max) = '',
	@EnableAppraisalDelivery bit = null
AS
BEGIN
    -- Insert statements for procedure here
	INSERT INTO DOCUMENT_VENDOR_BROKER_SETTINGS(BrokerId, VendorId, [Login], [Password], AccountId, ServicesLinkNm, DisclosureInTest, DisclosureBilling, DisclosureOnlineInterface, VendorIndex, IsEnableForProductionLoans, IsEnableForTestLoans, BlockManualFulfillment, HasCustomPackageData, CustomPackageDataJSON, EnableAppraisalDelivery)
	VALUES (@BrokerId, @VendorId, @Login, @Password, @AccountId, @ServicesLinkNm, @DisclosureInTest, @DisclosureBilling, @DisclosureOnlineInterface, @VendorIndex, @IsEnableForProductionLoans, @IsEnableForTestLoans, @BlockManualFulfillment, @HasCustomPackageData, @CustomPackageDataJSON, @EnableAppraisalDelivery)
END