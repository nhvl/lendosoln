ALTER PROCEDURE [dbo].[ManualAppraisalOrder_Invalidate]
	@AppraisalOrderId uniqueidentifier,
	@BrokerId uniqueidentifier,
	@LoanId uniqueidentifier,
	@DeletedByUserId uniqueidentifier,
	@DeletedDate smalldatetime
AS
BEGIN
	UPDATE APPRAISAL_ORDER
	SET IsValid = 0,
	DeletedByUserId = @DeletedByUserId,
	DeletedDate = @DeletedDate
	FROM APPRAISAL_ORDER a
	JOIN broker b on a.brokerid = b.brokerid
	WHERE b.status = 1
	AND a.AppraisalOrderId = @AppraisalOrderId
	AND AppraisalOrderType = 0	-- We should only allow this for manual appraisals.
	
	EXEC APPRAISAL_ORDER_Update_sIsApprSentToBorr @BrokerId, @LoanId
END
