-- =============================================
-- Author:		Justin Kim
-- Create date: 7/5/17
-- Description:	Update an entry in tracked_feature
-- =============================================
ALTER PROCEDURE [dbo].UpdateTrackedFeature
	@featureId int,
	@priority decimal(6,3),
	@isHidden bit
AS
BEGIN	
	SET NOCOUNT ON;

	update tracked_feature
	set
		Priority = @priority,
		IsHidden = @isHidden
	where 
		featureId = @featureId
END
