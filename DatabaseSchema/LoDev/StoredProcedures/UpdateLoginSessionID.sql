CREATE PROCEDURE UpdateLoginSessionID 
	@UserID Guid,
	@LoginSessionID Guid
AS
	UPDATE All_User SET LoginSessionID = @LoginSessionID WHERE UserID = @UserID
