-- =============================================
-- Author:		Ajay Mishra
-- Creation date: 5/20/2010
-- Description:	Handles Consumer Accounts creation/update on borrower email updates. Called when a document request is created or when a borrower email is updated on a loan application.
-- =============================================

CREATE PROCEDURE [dbo].[CP_CheckAndUpdateConsumerAccount]
(
	@oldEmail varchar(80),
	@newEmail varchar(80),
	@loanId uniqueidentifier,
	@appId uniqueidentifier,
	@isBorrower bit,
	@hashPasswordForNewAcc varchar(200),
	@forceUpdate bit = 0
)
AS
BEGIN
	DECLARE @brokerId uniqueidentifier,			
			@passwordHash varchar(200),			
			@appHasOpenRequest bit,
			@isTemporaryPassword bit,
			@retVal bit -- return 1 if a new account is created
	
	SELECT @appHasOpenRequest = 0,
		@retVal = 0,
		@isTemporaryPassword = 0

BEGIN TRAN

	--flag for open requests in app
	IF EXISTS(SELECT * FROM CP_CONSUMER_ACTION_REQUEST WHERE sLId=@loanId AND aAppId=@appId AND (ResponseStatus=0 OR ResponseStatus=1))
		SET @appHasOpenRequest = 1

	SELECT @brokerId=sBrokerId FROM LOAN_FILE_CACHE WHERE sLId=@loanId

	-- old email
	IF @oldEmail IS NOT NULL AND LEN(@oldEmail) > 0
	BEGIN		
		-- If the old email doesn't exist for other app in the loan AND for the other borrower in the app
		IF NOT EXISTS (SELECT * FROM CP_CONSUMER c INNER JOIN CP_CONSUMER_X_LOANID cl ON c.ConsumerId=cl.ConsumerId WHERE c.BrokerId=@brokerId AND c.Email=@oldEmail AND cl.sLId=@loanId AND cl.aAppId<>@appId)
			AND NOT EXISTS (SELECT * FROM CP_CONSUMER c INNER JOIN CP_CONSUMER_X_LOANID cl ON c.ConsumerId=cl.ConsumerId WHERE c.BrokerId=@brokerId AND c.Email=@oldEmail AND cl.sLId=@loanId AND cl.aAppId=@appId AND cl.IsBorrower <> @isBorrower)
		BEGIN
			--delete loan entry from CP_CONSUMER_X_LOANID				
			DELETE FROM CP_CONSUMER_X_LOANID WHERE sLId=@loanId AND ConsumerId IN (select ConsumerId FROM CP_CONSUMER WHERE Email=@oldEmail AND BrokerId=@brokerId)
			IF( 0!=@@ERROR)
			BEGIN			
				GOTO ERR_HANDLER;
			END

			-- if the email doesn't exist for any other loan for the given broker, the account should be deleted
			IF NOT EXISTS (SELECT * FROM CP_CONSUMER c INNER JOIN CP_CONSUMER_X_LOANID cl ON c.ConsumerId=cl.ConsumerId WHERE c.BrokerId=@brokerId AND c.Email=@oldEmail AND cl.sLId<>@loanId)				
			BEGIN
				-- copy the necessary info from the old account which can be used by the new account	
				IF LEN(@newEmail) > 0
					SELECT @passwordHash=PasswordHash FROM CP_CONSUMER c WHERE c.Email = @oldEmail AND c.BrokerId=@brokerId
				
				--delete the account					
				DELETE FROM CP_CONSUMER WHERE Email = @oldEmail AND BrokerId=@brokerId
				IF( 0!=@@ERROR)
				BEGIN
					GOTO ERR_HANDLER;
				END
			END
		END
		ELSE
		BEGIN
			--delete entry from CP_CONSUMER_X_LOANID for the loan/app
			DELETE FROM CP_CONSUMER_X_LOANID WHERE sLId=@loanId	AND aAppId=@appId AND IsBorrower = @isBorrower
			IF( 0!=@@ERROR)
			BEGIN
				GOTO ERR_HANDLER;
			END
		END
	END

	-- new email. This encapsulates email updates and new account creation requests
	IF @appHasOpenRequest = 1 OR @forceUpdate = 1
	IF @newEmail IS NOT NULL AND LEN(@newEmail) > 0
	BEGIN
		DECLARE @consumerId bigint
		IF NOT EXISTS (SELECT * FROM CP_CONSUMER WHERE BrokerId=@brokerId AND Email=@newEmail)
		-- Create new account
		BEGIN
			IF @passwordHash is null OR LEN(@passwordHash) = 0 -- For email update. If it is an email update, use old account info else create new password
			BEGIN				
				IF @hashPasswordForNewAcc IS NULL OR LEN(@hashPasswordForNewAcc) = 0
					SET @hashPasswordForNewAcc = 'invalid_hash' -- if @hashPasswordForNewAcc is null or empty. The table doesn't allow null or empty to be inserted
									
				SET @passwordHash = @hashPasswordForNewAcc
				SET @isTemporaryPassword = 1
			END

			-- Create new account
			-- Set last login date when an account is created. OPM 52015				
			INSERT INTO CP_CONSUMER (Email, BrokerId, PasswordHash, LastLoggedInD, IsTemporaryPassword) VALUES(@newEmail, @brokerId, @passwordHash, GETDATE(), @isTemporaryPassword)
			IF( 0!=@@ERROR)
			BEGIN
				GOTO ERR_HANDLER;
			END
			SET @consumerId = SCOPE_IDENTITY()
			SET @retVal  = 1 -- new account
		END
		ELSE
		BEGIN
			--Use the existing consumer account for the loan
			SELECT @consumerId=ConsumerId FROM CP_CONSUMER WHERE BrokerId=@brokerId AND Email=@newEmail
		END

		IF NOT EXISTS (SELECT * FROM CP_CONSUMER_X_LOANID WHERE ConsumerId=@consumerId AND sLId =@loanId AND aAppId=@appId AND IsBorrower=@isBorrower) AND @consumerId IS NOT NULL
		BEGIN
				INSERT INTO CP_CONSUMER_X_LOANID(ConsumerId, sLId, aAppId, IsBorrower) VALUES (@consumerId, @loanId, @appId, @isBorrower)
				IF( 0!=@@ERROR)
				BEGIN
					GOTO ERR_HANDLER;
				END
		END
	END

COMMIT TRAN

RETURN @retVal;

ERR_HANDLER:
	RAISERROR('Error in updating consumer account in CP_CheckAndUpdateConsumerAccount sp', 16, 1);		
	ROLLBACK TRAN
	return -100;

END