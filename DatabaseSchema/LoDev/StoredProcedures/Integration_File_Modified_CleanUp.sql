-- =============================================
-- Author:		David Dao
-- Create date: 
-- Description:	Delete record of files that modified over 90 days.
-- =============================================
ALTER PROCEDURE [dbo].[Integration_File_Modified_CleanUp]

AS
BEGIN
    DELETE FROM Integration_File_Modified WITH(ROWLOCK, READPAST, XLOCK)
    WHERE LastModifiedD <  getdate() -90

END
