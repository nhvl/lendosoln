-- =============================================
-- Author:		Michael Leinweaver
-- Create date: 3/8/2016
-- Description:	Adds a relationship between an employee and an OC
--				for a specific OC role.
-- =============================================
ALTER PROCEDURE [dbo].[PMLBROKER_SaveRelationship]
	@BrokerId uniqueidentifier,
	@PmlBrokerId uniqueidentifier,
	@OCRoleT int,
	@EmployeeId uniqueidentifier,
	@EmployeeName varchar(45),
	@EmployeeRoleT int
AS
BEGIN
	INSERT INTO PML_BROKER_RELATIONSHIPS (BrokerId, PmlBrokerId, OCRoleT, EmployeeId, EmployeeName, EmployeeRoleT)
	VALUES (@BrokerId, @PmlBrokerId, @OCRoleT, @EmployeeId, @EmployeeName, @EmployeeRoleT)
END
GO
