
CREATE PROCEDURE [dbo].[GetFthbCustomDefinitionByBrokerId] 
	@BrokerID uniqueidentifier
AS
	SELECT FthbCustomDefinition FROM Broker WITH(NOLOCK) WHERE BrokerID = @BrokerID

