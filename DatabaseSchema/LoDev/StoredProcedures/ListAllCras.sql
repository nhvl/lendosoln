ALTER PROCEDURE [dbo].[ListAllCras] 
AS
SELECT ComId, ComNm,  ComUrl, ProtocolType, IsInternalOnly, MclCraCode, VoxVendorId
	FROM Service_Company
	WHERE IsValid = 1
	ORDER BY ComNm
