CREATE PROCEDURE [dbo].GetAViewOfEmployeesWithTooManyPrimaryTeamsForARole
AS
BEGIN
	SELECT er.EmployeeId, er.RoleID, v.BrokerID, v.UserFirstNm, v.UserLastNm
	FROM
		[dbo].FindEmployeesWithTooManyPrimaryTeamsForARole() er
	LEFT OUTER JOIN 
		View_Lendersoffice_Employee v
	ON
		v.employeeid = er.employeeid
END