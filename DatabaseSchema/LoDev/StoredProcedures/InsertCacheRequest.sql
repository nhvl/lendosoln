ALTER  PROCEDURE [dbo].[InsertCacheRequest]
	@ReqId uniqueidentifier, 
	@LoanId uniqueidentifier, 
	@UserId uniqueidentifier, 
	@LoginName varchar(100), 
	@PageName varchar(100), 
	@DirtyFields text, 
	@WhoRetriedLastLogin varchar(36) = " ",
	@Error text = ''
AS
    -- 1/26/2017 - dd - Will try to update data if request exists before insert new record.
    UPDATE LOAN_CACHE_UPDATE_REQUEST
    SET
        sLid = @LoanId,
        UserId = @UserId,
        UserLoginNm = @LoginName,
        PageName = @PageName,
        AffectedCachedFields = @DirtyFields,
        ErrorInfo = @Error
    WHERE RequestId = @ReqId
    
    IF @@ROWCOUNT = 0
    BEGIN
	    INSERT INTO LOAN_CACHE_UPDATE_REQUEST
		    ( RequestId
		    , sLId
		    , UserId
		    , UserLoginNm
		    , PageName
		    , AffectedCachedFields
		    , FirstOccurD
		    , WhoRetriedLastLoginNm
		    , ErrorInfo
		    )
	    VALUES 
		    ( @ReqId
		    , @LoanId
		    , @UserId
		    , @LoginName
		    , @PageName
		    , @DirtyFields
		    , getdate()
		    , @WhoRetriedLastLogin
	        , @Error
		    )
    END
    
	if( 0 != @@error )
	begin
		RAISERROR('Error in updating LOAN_CACHE_UPDATE_REQUEST table in InsertCacheRequest sp', 16, 1);
		return -100;
	end
	return 0;



