
-- Modified 2/24/12 by Matthew Pham
-- Now left joining on license to get users without a license

-- Modified 3/13/12 by Matthew Pham
-- Now left joining on broker_user and all_user to get employees
--     without a userid.




CREATE     PROCEDURE [dbo].[ListUsersAllAssociatedWithBranch]
	@BrokerId uniqueidentifier, 
	@BranchId uniqueidentifier
AS
	SELECT TOP 100
		v.UserLastNm + ', ' + v.UserFirstNm AS UserName ,
		a.LoginNm ,
		CASE a.Type WHEN 'P' THEN 'PML User' ELSE 'Employee' END AS Type,
		CASE a.IsActive WHEN 0 THEN 'No' WHEN 1 THEN 'Yes' END  AS IsActive,
		v.EmployeeId ,
		l.LicenseNumber,
		a.UserId
	FROM View_lendersoffice_Employee AS v 
	left join broker_user as u on v.employeeid = u.employeeid
	left join all_user as a on u.userid = a.userid
	left join license as l on l.licenseid = u.currentlicenseid


	WHERE v.BrokerId = @BrokerId AND v.BranchId = @BranchId








