ALTER PROCEDURE [dbo].[ListBrokersUsingAutoPriceEligibilityProcess]
AS
BEGIN
	SELECT BrokerId FROM Broker
	WHERE
	Status = 1 -- Active
	AND EnableAutoPriceEligibilityProcess = 1
END
