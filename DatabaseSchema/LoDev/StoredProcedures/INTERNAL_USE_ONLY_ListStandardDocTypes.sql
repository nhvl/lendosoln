CREATE PROCEDURE [dbo].[INTERNAL_USE_ONLY_ListStandardDocTypes]
as
select DocTypeName, DocTypeId
from 
EDOCS_DOCUMENT_TYPE with (nolock)
where
brokerid is null
