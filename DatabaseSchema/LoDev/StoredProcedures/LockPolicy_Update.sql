-- =============================================
-- Author:		paoloa
-- Create date: 4/15/2013
-- Description:	
-- =============================================
ALTER PROCEDURE [dbo].[LockPolicy_Update]
	-- Add the parameters for the stored procedure here
	@LockPolicyId uniqueidentifier,
    @PolicyNm varchar(100),
    @BrokerId uniqueidentifier,
    @IsUsingRateSheetExpirationFeature bit,
    @LpeIsEnforceLockDeskHourForNormalUser bit,
    @TimezoneForRsExpiration char(3),
    @LpeMinutesNeededToLockLoan int,
    @EnableAutoLockExtensions bit,
    @MaxLockExtensions int,
    @MaxTotalLockExtensionDays int,
    @LockExtensionOptionTableXmlContent varchar(max),
    @LockExtensionMarketWorsenPoints decimal(9,3),
    @EnableAutoFloatDowns bit,
    @MaxFloatDowns int,
    @FloatDownPointFee decimal(9,3),
    @FloatDownPercentFee decimal(9,3),
    @FloatDownFeeIsPercent bit,
    @FloatDownMinRateImprovement decimal(9,3),
    @FloatDownMinPointImprovement decimal(9,3),
    @FloatDownAllowRateOptionsRequiringAdditionalPoints bit,
    @EnableAutoReLocks bit,
    @MaxReLocks int,
    @ReLockWorstCasePricingMaxDays int,
    @ReLockFeeTableXmlContent varchar(max),
    @ReLockMarketPriceReLockFee decimal(9,3),
    @ReLockRequireSameInvestor bit,
    @DefaultLockDeskID uniqueidentifier,
    @FloatDownAllowedAfterLockExtension bit,
    @FloatDownAllowedAfterReLock bit,
    @DaySettingsXml varchar(max),
    @IsLockDeskEmailDefaultFromForRateLockConfirmation bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
    UPDATE LENDER_LOCK_POLICY
    SET
        PolicyNm = @PolicyNm,
        BrokerId = @BrokerId,
        IsUsingRateSheetExpirationFeature = @IsUsingRateSheetExpirationFeature,
        LpeIsEnforceLockDeskHourForNormalUser = @LpeIsEnforceLockDeskHourForNormalUser,
        TimezoneForRsExpiration = @TimezoneForRsExpiration,
        LpeMinutesNeededToLockLoan = @LpeMinutesNeededToLockLoan,
        EnableAutoLockExtensions = @EnableAutoLockExtensions,
        MaxLockExtensions = @MaxLockExtensions,
        MaxTotalLockExtensionDays = @MaxTotalLockExtensionDays,
        LockExtensionOptionTableXmlContent = @LockExtensionOptionTableXmlContent,
        LockExtensionMarketWorsenPoints = @LockExtensionMarketWorsenPoints,
        EnableAutoFloatDowns = @EnableAutoFloatDowns,
        MaxFloatDowns = @MaxFloatDowns,
        FloatDownPointFee = @FloatDownPointFee,
        FloatDownPercentFee = @FloatDownPercentFee,
        FloatDownFeeIsPercent = @FloatDownFeeIsPercent,
        FloatDownMinRateImprovement = @FloatDownMinRateImprovement,
        FloatDownMinPointImprovement = @FloatDownMinPointImprovement,
        FloatDownAllowRateOptionsRequiringAdditionalPoints = @FloatDownAllowRateOptionsRequiringAdditionalPoints,
        EnableAutoReLocks = @EnableAutoReLocks,
        MaxReLocks = @MaxReLocks,
        ReLockWorstCasePricingMaxDays = @ReLockWorstCasePricingMaxDays,
        ReLockFeeTableXmlContent = @ReLockFeeTableXmlContent,
        ReLockMarketPriceReLockFee = @ReLockMarketPriceReLockFee,
        ReLockRequireSameInvestor = @ReLockRequireSameInvestor,
        DefaultLockDeskID = @DefaultLockDeskID,
		FloatDownAllowedAfterLockExtension = @FloatDownAllowedAfterLockExtension,
		FloatDownAllowedAfterReLock = @FloatDownAllowedAfterReLock,
		DaySettingsXml = @DaySettingsXml,
		IsLockDeskEmailDefaultFromForRateLockConfirmation = @IsLockDeskEmailDefaultFromForRateLockConfirmation
		
	WHERE LockPolicyId = @LockPolicyId
END
