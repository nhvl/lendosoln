



-- =============================================
-- Author:		Antonio Valencia
-- Create date: 4/14/2011
-- Description:	Saves Condition Choices
--
-- 03/27/2014 - Added RequiredDocTypeId - OPM 170314
-- =============================================
CREATE PROCEDURE [dbo].[TASK_ConditionChoice_Save] 
	@BrokerId uniqueidentifier,
	@Id int = null,
	@ConditionCategoryId int,
	@ConditionType int,
	@sLT int,
	@ConditionSubject varchar(max),
	@ToBeAssignedRole uniqueidentifier,
	@ToBeOwnedByRole uniqueidentifier,
	@DueDateFieldName varchar(50),
	@DueDateAddition int,
	@Rank int,
	@RequiredDocTypeId int = null,
	@IsHidden bit
AS

--REVIEWS
--6/9/2011: Reviewed. -ThinhNK

BEGIN
	IF @Id is null 
	BEGIN
		INSERT INTO Condition_Choice(
			BrokerId,
			ConditionCategoryId,
			ConditionType,
			sLT, 
			ConditionSubject,
			ToBeAssignedRole,
			ToBeOwnedByRole,
			DueDateFieldName,
			DueDateAddition,
			Rank,
			RequiredDocTypeId,
			IsHidden)
		VALUES( 
			@BrokerId,
			@ConditionCategoryId,
			@ConditionType,
			@sLT, 
			@ConditionSubject,
			@ToBeAssignedRole,
			@ToBeOwnedByRole,
			@DueDateFieldName,
			@DueDateAddition,
			@Rank,
			@RequiredDocTypeId,
			@IsHidden)
	
		SET @Id = scope_identity()
		SELECT @Id 
	END
	ELSE 
	BEGIN
		UPDATE Condition_Choice SET
			ConditionCategoryId = @ConditionCategoryId,
			ConditionType = @ConditionType,
			sLT = @sLT,
			ConditionSubject = @ConditionSubject,
			ToBeAssignedRole = @ToBeAssignedRole,
			ToBeOwnedByRole = @ToBeOwnedByRole,
			DueDateFieldName =  @DueDateFieldName,
			DueDateAddition = @DueDateAddition,
			Rank = @Rank,
			RequiredDocTypeId = @RequiredDocTypeId,
			IsHidden = @IsHidden
		WHERE BrokerId = @BrokerId AND @Id = ConditionChoiceId

		SELECT @Id
	END
END




