-- =============================================
-- Author:		Justin Lara
-- Create date: 1/11/2017
-- Description:	Retrieves all messages associated
--				with a loan and order that have
--				not been read by the given user.
-- =============================================
ALTER PROCEDURE dbo.APPRAISAL_VENDOR_MESSAGES_GetUnreadMessages
	@BrokerId uniqueidentifier,
	@LoanId uniqueidentifier,
	@UserId uniqueidentifier
AS
BEGIN
	SET NOCOUNT ON;

	SELECT MessageId
	FROM APPRAISAL_VENDOR_MESSAGES
	WHERE LoanId = @LoanId
		AND BrokerId = @BrokerId
		AND LoanId = @LoanId
		AND MessageId NOT IN
		(
			SELECT MessageId
			FROM APPRAISAL_VENDOR_MESSAGES_READ
			WHERE UserId = @UserId
				AND BrokerId = @BrokerId
				AND LoanId = @LoanId
		)
END
