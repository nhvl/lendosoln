-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE RetrieveAnonymousQuickPricerUserId
	@UserId UniqueIdentifier
AS
BEGIN
	SELECT AnonymousQuickPricerUserId FROM BROKER_USER 
	WHERE UserId = @UserId AND AnonymousQuickPricerUserId IS NOT NULL
END
