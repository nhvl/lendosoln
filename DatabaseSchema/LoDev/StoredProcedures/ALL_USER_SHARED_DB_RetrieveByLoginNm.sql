-- =============================================
-- Author:		David Dao
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE dbo.ALL_USER_SHARED_DB_RetrieveByLoginNm 
    @LoginNm varchar(50)
AS
BEGIN

    SELECT BrokerId, UserId
    FROM ALL_USER_SHARED_DB
    WHERE LoginNm = @LoginNm
END
