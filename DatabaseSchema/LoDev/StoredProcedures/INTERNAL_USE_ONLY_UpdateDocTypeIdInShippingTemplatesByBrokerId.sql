CREATE PROCEDURE [dbo].[INTERNAL_USE_ONLY_UpdateDocTypeIdInShippingTemplatesByBrokerId]
	@BrokerId uniqueidentifier,
	@OldDocTypeId int,
	@NewDocTypeId int
as
	
	update EDOCS_DOCUMENT_TYPE_X_SHIPPING_TEMPLATE
	
	set DocTypeId = @NewDocTypeId
	from EDOCS_DOCUMENT_TYPE_X_SHIPPING_TEMPLATE dtst 
	join EDOCS_DOCUMENT_TYPE edt on dtst.DocTypeId = edt.DocTypeId
	join EDOCS_SHIPPING_TEMPLATE st on dtst.shippingtemplateid = st.shippingtemplateid

		where dtst.DocTypeId = @OldDocTypeId
			and edt.BrokerId is null
			and st.BrokerId = @BrokerId

		
