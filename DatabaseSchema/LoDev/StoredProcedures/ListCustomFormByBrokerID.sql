


CREATE PROCEDURE [dbo].[ListCustomFormByBrokerID] 
	@BrokerID uniqueidentifier
AS
	SELECT CustomLetterID, Title, UserFirstNm + ' ' + UserLastNm AS EmployeeName
	FROM Custom_Letter c , Employee e
	WHERE c.BrokerId = @BrokerID AND c.ContentType = 'WORD' 
	AND 
		c.OwnerEmployeeId = e.EmployeeId
	UNION
	SELECT CustomLetterID, Title, 'None' AS EmployeeName
	FROM Custom_Letter
	WHERE BrokerId = @BrokerID AND ContentType = 'WORD' AND OwnerEmployeeId IS NULL
	ORDER BY Title


