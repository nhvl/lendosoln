

CREATE PROCEDURE [dbo].[RetrieveEmployeeIdByPmlLoginNameAndSiteId]
	@LoginName varchar(36),
	@BrokerPmlSiteId uniqueidentifier
AS
SELECT EmployeeId
FROM View_Active_Pml_User_With_Broker_Info
WHERE LoginNm = @LoginName AND BrokerPmlSiteId = @BrokerPmlSiteId


