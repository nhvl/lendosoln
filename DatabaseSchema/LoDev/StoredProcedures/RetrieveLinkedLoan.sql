CREATE PROCEDURE RetrieveLinkedLoan
(
	@sLId uniqueidentifier
)
as
	select *
	from loan_link
	where slid1 = @sLId or slid2 = @sLId
