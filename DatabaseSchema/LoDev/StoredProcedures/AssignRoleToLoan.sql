CREATE PROCEDURE dbo.AssignRoleToLoan
	@LoanID UniqueIdentifier,
	@RoleID UniqueIdentifier,
	@EmployeeID UniqueIdentifier
AS
	declare @tranPoint sysname
	set @tranPoint = object_name(@@procId) + CAST(@@nestlevel as varchar(10))
	--print @tranPoint
	
	BEGIN TRANSACTION
	SAVE TRANSACTION @tranPoint
	IF @EmployeeID = '00000000-0000-0000-0000-000000000000'
	BEGIN
		DELETE Loan_User_Assignment with( rowlock ) WHERE sLId = @LoanID AND RoleID = @RoleID
	END
	ELSE
	BEGIN
		UPDATE Loan_User_Assignment with( rowlock )
		      SET EmployeeID = @EmployeeID
		WHERE sLId = @LoanID AND RoleID = @RoleID
	
		IF @@ROWCOUNT = 0
		BEGIN
			INSERT INTO Loan_User_Assignment with( rowlock ) (sLId, RoleID, EmployeeID) 
		                      VALUES (@LoanID,  @RoleID, @EmployeeID)
				
			IF (0!=@@error)
			BEGIN
				RAISERROR('Error in inserting into table Loan_User_Assignment in AssignRoleToLoan sp', 16, 1);
				GOTO ErrorHandler;
			
			END
		END
	END
	commit transaction
	return 0;
ErrorHandler:
	rollback transaction @tranPoint
	rollback transaction
	return -100;
