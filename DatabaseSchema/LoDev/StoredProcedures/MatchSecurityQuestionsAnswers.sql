-- =============================================
-- EncryptionTODO
-- This stored procedure will be retired when the
-- unencrypted DB question answer columns are retired
-- or when all code has been switched to the replacement
-- BROKER_USER_HandleSecurityQuestionsAnswerResult.
-- =============================================
ALTER PROCEDURE [dbo].[MatchSecurityQuestionsAnswers]
	@UserId uniqueidentifier,
	@Answer1 varchar(20),
	@Answer2 varchar(20),
	@Answer3 varchar(20),
	@AttemptsCount int OUTPUT
AS
BEGIN
	SET NOCOUNT ON
	-- Check if an active broker user exists
	IF NOT EXISTS (SELECT a.UserId FROM BROKER_USER b INNER JOIN ALL_USER a on b.UserId = a.UserId WHERE b.UserId = @UserId AND a.IsActive = 1)
		RETURN -1

	IF EXISTS (SELECT UserId FROM BROKER_USER WHERE UserId = @UserId AND SecurityQuestion1Answer = @Answer1 AND SecurityQuestion2Answer = @Answer2 AND SecurityQuestion3Answer = @Answer3) 
		BEGIN -- Answers match
			SET @AttemptsCount = 0
			UPDATE ALL_USER SET SecurityQuestionsAttemptsMade = 0 WHERE UserId = @UserId
			RETURN 1
		END
	ELSE
		BEGIN -- Answers do not match
			UPDATE ALL_USER SET SecurityQuestionsAttemptsMade = SecurityQuestionsAttemptsMade + 1 WHERE UserId = @UserId
			SELECT TOP 1 @AttemptsCount = SecurityQuestionsAttemptsMade FROM ALL_USER WHERE UserId = @UserId			
			RETURN -2
		END
END
