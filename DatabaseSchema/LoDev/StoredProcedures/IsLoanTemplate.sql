CREATE PROCEDURE [dbo].[IsLoanTemplate]
	@LoanID uniqueidentifier
AS
	SELECT la.IsTemplate,sbranchid,sfileversion FROM Loan_File_Cache with(nolock) join loan_file_a as la with(nolock) on loan_file_cache.slid=la.slid WHERE loan_file_cache.sLId = @LoanID
