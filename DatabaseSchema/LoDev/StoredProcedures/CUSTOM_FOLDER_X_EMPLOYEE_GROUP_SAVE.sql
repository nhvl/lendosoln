ALTER PROCEDURE [dbo].[CUSTOM_FOLDER_X_EMPLOYEE_GROUP_SAVE]
	@CustomFolderXEmployeeGroup CustomFolderXEmployeeGroupTableType READONLY,
	@FolderIds IntTableType READONLY
AS
BEGIN
	WITH 
		CUSTOM_FOLDER_X_EMPLOYEE_GROUP_PARTIAL AS
		(
			SELECT p.FolderId, p.GroupId
			FROM CUSTOM_FOLDER_X_EMPLOYEE_GROUP as p
			JOIN @FolderIds as c ON p.FolderId = c.IntValue
		)
	MERGE CUSTOM_FOLDER_X_EMPLOYEE_GROUP_PARTIAL AS TARGET
	USING @CustomFolderXEmployeeGroup AS SOURCE
	ON (TARGET.FolderId = SOURCE.FolderId AND TARGET.GroupId = SOURCE.GroupId)		
		WHEN NOT MATCHED BY TARGET THEN 
			INSERT
			(
				FolderId,
				GroupId
			)
			VALUES
			(
				SOURCE.FolderId,
				SOURCE.GroupId
			)		

		WHEN NOT MATCHED BY SOURCE THEN
		DELETE;
END
