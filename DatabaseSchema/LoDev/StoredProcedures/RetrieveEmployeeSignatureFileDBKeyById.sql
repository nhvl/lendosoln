ALTER PROCEDURE [dbo].[RetrieveEmployeeSignatureFileDBKeyById]
	@EmployeeId uniqueidentifier
AS
BEGIN
	SELECT SignatureFileDBKey
	FROM Broker_User
	WHERE EmployeeId = @EmployeeId
END
