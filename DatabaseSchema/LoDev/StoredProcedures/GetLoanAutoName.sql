CREATE PROCEDURE GetLoanAutoName
	@BrokerID Guid , @Name Varchar( 50 ) OUTPUT
AS 
	-- 12/28/2004 kb - Copied from dd's auto naming implementation to reuse in
	-- new strategy that doesn't care about collisions or gaps (we defer handling
	-- collisions to part of a wider loop).
	--
	-- 12/29/2004 kb - We must return a unique name with each call.  To limit
	-- concurrent reading of the same value, and in lieu of an atomic test and
	-- set operation, we should update the broker's counter right after we read
	-- it.  The select and update should lock the row so that other threads see
	-- what was written, not what we read.
	
	DECLARE @Collision int
	-- Hardcode input
	--SET @Pattern = '{yy}{mm}-{r:16}'
	--SET @Counter = 7
	-- Actual function here
	DECLARE @CurrentDate smalldatetime
	DECLARE @mm char(2)
	DECLARE @yyyy char(4)
	DECLARE @yy char(2)
	DECLARE @y char
	DECLARE @m char
	DECLARE @dd char(2)
	DECLARE @c varchar(10)
	DECLARE @month int
	DECLARE @day int
	DECLARE @r varchar(10)
	SET @CurrentDate = GETDATE()
	SET @yyyy = RIGHT(YEAR(@CurrentDate), 4)
	SET @yy = RIGHT(YEAR(@CurrentDate), 2)
	SET @y = RIGHT(YEAR(@CurrentDate), 1)
	SET @month = MONTH(@CurrentDate)
	IF @month < 10 
	BEGIN
		SET @mm = '0' + CONVERT(char, @month)
		SET @m = @month
	END
	ELSE
	BEGIN
		SET @mm = @month
		-- Convert to hex value
		SET @m = CHAR(55 + @month)
	END
	SET @day = DAY(@CurrentDate)
	IF @day < 10
	BEGIN
	    	SET @dd = '0' + CONVERT(char, @day)
	END
	ELSE
	BEGIN
	    	SET @dd = @day
	END
	DECLARE @Pattern varchar(50)
	DECLARE @Counter BigInt
	DECLARE @CounterDate smalldatetime
	SELECT @Pattern = NamingPattern, @Counter = NamingCounter, @CounterDate = NamingCounterDate
	FROM Broker WHERE BrokerID = @BrokerID
	
	-- Check if counter need to reset by day ormonth or year or increment counter by one.
	IF CHARINDEX('d}', @Pattern) > 0 AND (DAY(@CounterDate) <> DAY(@CurrentDate) OR MONTH(@CounterDate) <> MONTH(@CurrentDate) OR YEAR(@CounterDate) <> YEAR(@CurrentDate))
	BEGIN
		SET @Counter = 0
	END
	ELSE IF CHARINDEX('m}', @Pattern) > 0 AND (MONTH(@CounterDate) <> MONTH(@CurrentDate) OR YEAR(@CounterDate) <> YEAR(@CurrentDate))
	BEGIN
		SET @Counter = 0
	END
	ELSE IF CHARINDEX('y}', @Pattern) > 0 AND YEAR(@CounterDate) <> YEAR(@CurrentDate)
	BEGIN
		SET @Counter = 0
	END
	ELSE
	BEGIN
		SET @Counter = @Counter + 1
	END
	-- Update counter to broker table.
	UPDATE Broker with( rowlock ) SET NamingCounter = @Counter, NamingCounterDate = GETDATE() WHERE BrokerID = @BrokerID
	-- Find counter pattern
             -- Replace {c:XX} where XX is integer from 1 - 10 with '000....@counter'
	DECLARE @StartIndex int
	DECLARE @EndIndex int
	DECLARE @Length int
	DECLARE @CounterPattern varchar(10)
	SET @StartIndex = CHARINDEX('{c:', @Pattern)
	IF @StartIndex <> 0 OR CONVERT(varchar(3), @Pattern) = '{c:'
	BEGIN
		SET @EndIndex = CHARINDEX('}', @Pattern, @StartIndex)
		SET @Length = CONVERT(int, SUBSTRING(@Pattern, @StartIndex + 3, @EndIndex - @StartIndex - 3))
		SET @CounterPattern = SUBSTRING(@Pattern, @StartIndex, @EndIndex - @StartIndex + 1)
                           -- Counter is overflow (larger than indicate length) then reset counter to 0.
		IF (LEN(CONVERT(varchar(10), @Counter)) > @Length)
		BEGIN
			-- 1/13/2005 kb - Updated counter reset so that we preserve a counter value
			-- that may already be too long.  We don't update the db after here, so we'll
			-- always be cropping off the left hand side if the counter grows.  This should
			-- work for all the brokers that have a high counter value and lower their naming
			-- pattern digit count.
			SET @Counter = CONVERT(BigInt, RIGHT(CONVERT(varchar(10), @Counter), @Length))
		END
		SET @c = REPLICATE('0', @Length - LEN(CONVERT(varchar(10), @Counter))) + CONVERT(varchar(10), @Counter)
	END
	-- Find random pattern
	DECLARE @RandomPattern varchar(10)
	SET @StartIndex = CHARINDEX('{r:', @Pattern)
	IF @StartIndex <> 0
	BEGIN
		SET @EndIndex = CHARINDEX('}', @Pattern, @StartIndex)
		SET @Length = CONVERT(int, SUBSTRING(@Pattern, @StartIndex + 3, @EndIndex - @StartIndex - 3))
		SET @RandomPattern = SUBSTRING(@Pattern, @StartIndex, @EndIndex - @StartIndex + 1)
		SET @r = RIGHT(NEWID(), @Length)
	END
	SET @Name =
		REPLACE(
		REPLACE(
		REPLACE(
		REPLACE(
		REPLACE(
		REPLACE(
		REPLACE(
		REPLACE(
			@Pattern, '{yy}', @yy),
			'{yyyy}', @yyyy),
			'{dd}', @dd), 
			'{mm}', @mm), 
			ISNULL(@CounterPattern,''), ISNULL(@c, '')),
			'{m}', @m),
			'{y}', @y),
			ISNULL(@RandomPattern,''), ISNULL(@r, ''))
