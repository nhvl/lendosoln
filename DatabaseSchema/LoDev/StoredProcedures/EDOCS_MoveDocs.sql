-- =============================================
-- Author:		Antonio Valencia
-- Create date: 1/2/2014
-- Description: Migrates EDocs Between Loan Files
-- =============================================
ALTER PROCEDURE [dbo].[EDOCS_MoveDocs] 
	@src_sLId uniqueidentifier,
	@src_aAppId uniqueidentifier,
	@BrokerId uniqueidentifier,
	@dst_sLId uniqueidentifier,
	@dst_aAppId uniqueidentifier

 
AS
BEGIN
	UPDATE EDOCS_DOCUMENT
	SET 
		sLID = @dst_sLId,
		aAppId = @dst_aAppId
	WHERE sLID  = @src_sLId and aAppId = @src_aAppId and BrokerId = @BrokerId
	
	UPDATE EDOCS_GENERIC
	SET 
		sLID = @dst_sLId,
		aAppId = @dst_aAppId
	WHERE  sLID  = @src_sLId and aAppId = @src_aAppId and BrokerId = @BrokerId
	
	UPDATE BORROWER_CREDIT_AUTHORIZATION_DOCUMENT_ASSOCIATION
	SET
		LoanId = @dst_sLId,
		AppId = @dst_aAppId
	WHERE LoanId = @src_sLId and AppId = @src_aAppId and BrokerId = @BrokerId
END