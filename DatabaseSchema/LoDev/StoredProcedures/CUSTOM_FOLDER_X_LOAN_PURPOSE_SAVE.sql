ALTER PROCEDURE [dbo].[CUSTOM_FOLDER_X_LOAN_PURPOSE_SAVE]
	@CustomFolderXLoanPurpose CustomFolderXLoanPurposeTableType READONLY,
	@FolderIds IntTableType READONLY
AS
BEGIN
	WITH 
		CUSTOM_FOLDER_X_LOAN_PURPOSE_PARTIAL AS
		(
			SELECT p.FolderId, p.LoanPurpose
			FROM CUSTOM_FOLDER_X_LOAN_PURPOSE as p
			JOIN @FolderIds as c ON p.FolderId = c.IntValue
		)
	MERGE CUSTOM_FOLDER_X_LOAN_PURPOSE_PARTIAL AS TARGET
	USING @CustomFolderXLoanPurpose AS SOURCE
	ON (TARGET.FolderId = SOURCE.FolderId AND TARGET.LoanPurpose = SOURCE.LoanPurpose)		
		WHEN NOT MATCHED BY TARGET THEN 
			INSERT
			(
				FolderId,
				LoanPurpose
			)
			VALUES
			(
				SOURCE.FolderId,
				SOURCE.LoanPurpose
			)		

		WHEN NOT MATCHED BY SOURCE THEN
		DELETE;
END
