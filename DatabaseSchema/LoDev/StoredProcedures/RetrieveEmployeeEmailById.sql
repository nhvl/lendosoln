CREATE PROCEDURE [dbo].[RetrieveEmployeeEmailById]
	@EmployeeId uniqueidentifier
AS
BEGIN	
	SELECT Email
	FROM EMPLOYEE
	WHERE EmployeeId = @EmployeeID
END
