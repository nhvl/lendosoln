
-- =============================================
-- Author:		Antonio Valencia
-- Create date: Aug 22 11
-- Description:	Updates the status of a doc magic doc
-- =============================================
CREATE PROCEDURE [dbo].[EDOCS_UpdateDocMagicPDFStatus] 
	-- Add the parameters for the stored procedure here
	@Id uniqueidentifier, 
	@CurrentStatus  tinyint,  
	@PublicNotes varchar(1000) = null,
	@DevNotes varchar(max) = null
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	IF @CurrentStatus = 4 BEGIN
		DELETE FROM EDOCS_DOCMAGIC_PDF_STATUS  
		WHERE Id = @Id 
	RETURN
	END

	UPDATE EDOCS_DOCMAGIC_PDF_STATUS
	SET 
		CurrentStatus = @CurrentStatus,
		PublicNotes = COALESCE(@PublicNotes, PublicNotes), 
		DevNotes = COALESCE(@DevNotes, DevNotes)
	WHERE Id = @Id 

END

