-- =============================================
-- Author:		Matthew Pham
-- Create date: 8/23/11
-- Description:	Select documents that have
--				images older than X months.
--				For case 66755.
-- =============================================
ALTER PROCEDURE [dbo].[EDOCS_GetDocumentsWithImagesOlderThanXMonths] 
	-- Add the parameters for the stored procedure here
	@months int = 4
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT top 1000 doctype.FolderId,
		app.aBFirstNm, app.aCFirstNm, app.aBLastNm, app.aCLastNm,
		ImageStatus,
		TimeToRequeueForImageGeneration,
		HasInternalAnnotation,
		doc.DocumentId,
		doc.sLId,
		doc.aAppId,
		doc.IsValid, 
		(CASE WHEN doctype.ISValid = 1 THEN DocTypeName ELSE DocTypeName + ' (Deleted)' END) as DocTypeName,
		doctype.IsValid as DocTypeIsValid,
		doc.BrokerId,
		doc.DocTypeId,
		FaxNumber,
		FileDBKey_CurrentRevision,
		FileDBKey_OriginalDocument,
		doc.Description as PublicDescription,
		InternalDescription,
		Comments,
		NumPages,
		IsUploadedByPmlUser,
		doc.Version,
		LastModifiedDate,
		CreatedDate,
		IsEditable,
		PdfHasOwnerPassword,
		TimeImageCreationEnqueued,
		PdfHasUnsupportedFeatures,
		doc.Status,
		StatusDescription,
		PdfPageInfo,
		HideFromPmlUsers,
		doc.CreatedByUserId,
		doc.HasESignTags,
		doc.MetadataProtobufContent,
		doc.OriginalMetadataProtobufContent,
        gen.FileType as GenericFileType
		FROM EDOCS_DOCUMENT as doc
			JOIN EDOCS_DOCUMENT_TYPE as doctype ON doc.DocTypeId = doctype.DocTypeId
			JOIN APPLICATION_A as app ON app.aAppId = doc.aAppId   
			join broker b on b.brokerid = doc.brokerid
            LEFT JOIN EDOCS_GENERIC as gen ON doc.DocumentId = gen.DocumentId AND doc.sLId = gen.sLId AND doc.BrokerId = gen.BrokerId
		WHERE
			TimeImageCreationCompleted < DATEADD(month, @months*-1, GETDATE())
			AND ImageStatus=2 and b.status = 1
END
