-- =============================================
-- Author:		Jhairo Erazo
-- Create date: 01/13/14
-- Description:	Get the pool counter from broker table and increase counter
-- =============================================
CREATE PROCEDURE [dbo].[MortgagePool_GetPoolCounter]
	@BrokerId Guid,
    @PoolCounter BigInt = NULL OUT
AS

-- Get Mortgage Pool Counter (OPM 145924)
SELECT @PoolCounter = PoolCounter
FROM Broker WHERE BrokerId = @BrokerId

BEGIN
	--increment counter until we ge to an unsued pool number
	WHILE (1=1)
	BEGIN
		SET @PoolCounter = @PoolCounter + 1
	
		IF (SELECT COUNT(*) FROM MORTGAGE_POOL WHERE BrokerId = @BrokerId AND InternalId = CONVERT(varchar, @PoolCounter)) = 0
			BREAK
	END
	
	-- Update Counter to broker table
	UPDATE Broker WITH ( ROWLOCK ) SET PoolCounter = @PoolCounter
	WHERE BrokerId = @BrokerId
END