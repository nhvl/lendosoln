CREATE PROCEDURE [dbo].[LockPolicyObservedHoliday_Create]
	@LockPolicyId uniqueidentifier,
	@BrokerId uniqueidentifier,
	@HolidayName varchar(50)
AS
BEGIN
	INSERT INTO dbo.[LENDER_LOCK_POLICY_OBSERVED_HOLIDAY]
	( LockPolicyId,  BrokerId,  HolidayName) VALUES
	(@LockPolicyId, @BrokerId, @HolidayName)
END
