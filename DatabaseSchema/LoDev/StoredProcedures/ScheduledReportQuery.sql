CREATE procedure [dbo].[ScheduledReportQuery]
@UserId varchar(50) = NULL,
@BrokerId uniqueidentifier
as 
select r.ReportId,r.ScheduleName,d.LastRun,d.NextRun,d.RunTime,d.RetryUntil,q.QueryName, q.QueryId
from SCHEDULED_REPORT_USER_INFO as r JOIN DAILY_REPORTS as d 
on r.ReportId = d.ReportId join REPORT_QUERY q
on q.QueryId = r.QueryId join VIEW_ACTIVE_LO_USER a
on a.UserId = r.UserId
AND a.BrokerId = @BrokerId
AND (r.UserId = @UserId
OR @UserId IS NULL)
where r.ReportType ='Daily'
union 
select r.ReportId,r.ScheduleName,d.LastRun,d.NextRun,d.RunTime,d.RetryUntil,q.QueryName ,q.QueryId
from SCHEDULED_REPORT_USER_INFO as r JOIN WEEKLY_REPORTS as d
on r.ReportId = d.ReportId join REPORT_QUERY q
on q.QueryId = r.QueryId join VIEW_ACTIVE_LO_USER a
on a.UserId = r.UserId
AND a.BrokerId = @BrokerId
AND (r.UserId = @UserId
OR @UserId IS NULL)
where r.ReportType ='Weekly'
union
select r.ReportId,r.ScheduleName,d.LastRun,d.NextRun,d.RunTime,d.RetryUntil,q.QueryName ,q.QueryId
from SCHEDULED_REPORT_USER_INFO as r JOIN MONTHLY_REPORTS as d
on r.ReportId = d.ReportId join REPORT_QUERY q
on q.QueryId = r.QueryId join VIEW_ACTIVE_LO_USER a
on a.UserId = r.UserId
AND a.BrokerId = @BrokerId
AND (r.UserId = @UserId
OR @UserId IS NULL)
where r.ReportType ='Monthly'
union 
select r.ReportId,r.ScheduleName,null,null,null,null,q.QueryName,q.QueryId
from SCHEDULED_REPORT_USER_INFO as r JOIN REPORT_QUERY q
on  q.QueryId = r.QueryId join VIEW_ACTIVE_LO_USER a
on a.UserId = r.UserId
AND a.BrokerId = @BrokerId
AND (r.UserId = @UserId
OR @UserId IS NULL)
where r.ReportType ='Never'