-- =============================================
-- Author:		Scott Gettinger
-- Create date: 12/21/2017
-- Description:	Gets a list of document ids on the same loan as the specified document. The loan ID is also available as an output parameter.
-- =============================================
ALTER PROCEDURE [dbo].[EDOCS_GetDocumentsOnSameLoan]
	@DocumentId uniqueidentifier, 
	@LoanId uniqueidentifier OUTPUT
AS
BEGIN
	SET @LoanId = (SELECT TOP 1 sLId FROM EDOCS_DOCUMENT WHERE DocumentId = @DocumentId)
    SELECT DocumentId FROM EDOCS_DOCUMENT WHERE sLId = @LoanId
END
