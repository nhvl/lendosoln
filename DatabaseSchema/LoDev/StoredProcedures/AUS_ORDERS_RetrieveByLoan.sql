-- =============================================
-- Author:		Justin Lara
-- Create date: 3/7/2017
-- Description:	Retrieves a list of AUS orders
--				associated with the given loan.
-- =============================================
ALTER PROCEDURE [dbo].[AUS_ORDERS_RetrieveByLoan]
	@LoanId uniqueidentifier,
	@BrokerId uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT
		AusOrderId,
		LoanId,
		BrokerId,
		IsManual,
		CustomPosition,
		UnderwritingService,
		UnderwritingServiceOtherDescription,
		DuRecommendation,
		LpRiskClass,
		LpPurchaseEligibility,
		LpStatus,
		LpFeeLevel,
		TotalRiskClass,
		TotalEligibilityAssessment,
		GusRecommendation,
		GusRiskEvaluation,
		ResultOtherDescription,
		CaseId,
		FindingsDocument,
		OrderPlacedBy,
		Timestamp,
		Notes,
        LoanIsFha
	FROM AUS_ORDERS
	WHERE LoanId = @LoanId AND BrokerId = @BrokerId
END
