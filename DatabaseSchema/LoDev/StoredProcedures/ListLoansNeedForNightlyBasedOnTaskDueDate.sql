-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE dbo.ListLoansNeedForNightlyBasedOnTaskDueDate
AS
BEGIN

SELECT DISTINCT LoanId
FROM TASK t JOIN LOAN_FILE_CACHE cache ON t.LoanId = cache.sLId
            JOIN BROKER br ON br.BrokerId = cache.sBrokerId
WHERE
	cache.IsValid = 1 
    AND br.Status = 1 
    AND br.IsUseNewCondition = 1 
    AND cache.IsTemplate = 0
	AND t.TaskDueDate IS NOT NULL 
    AND t.TaskDueDate BETWEEN GETDATE() - 2 AND GETDATE() + 1

END

