-- =============================================
-- Author:		David Dao
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE dbo.EDOCS_DOCMAGIC_DOCTYPE_X_DOCTYPE_GetByBrokerId 
    @BrokerId UniqueIdentifier,
    @DocTypeId int = NULL
AS
BEGIN

    SELECT DocTypeId, DocMagicDocTypeId
    FROM EDOCS_DOCMAGIC_DOCTYPE_X_DOCTYPE
    WHERE DocTypeId = COALESCE(@DocTypeId, DocTypeId) AND BrokerId = @BrokerId
    
END
