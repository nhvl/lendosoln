-- =============================================
-- Author:		Justin Lara
-- Create date: 12/7/2016
-- Description:	Retrieves the Service Company
--				with the given ComId.
-- =============================================
ALTER PROCEDURE [dbo].[SERVICE_COMPANY_Retrieve]
	@ComId uniqueidentifier
AS
BEGIN
	SELECT ComId, ComNm
	FROM SERVICE_COMPANY
	WHERE ComId = @ComId
END

