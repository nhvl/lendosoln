-- =============================================
-- Author:		Eric Mallare
-- Create date: 1/23/2018
-- Description:	Update a DocuSign recipient record.
-- =============================================
ALTER PROCEDURE [dbo].[DOCUSIGN_RECIPIENTS_AUTHRESULTS_Update]
	@Id int,
	@Status int,
	@EventTime datetime = null,
	@FailureDescription varchar(500) = null
AS
BEGIN
	UPDATE DOCUSIGN_RECIPIENTS_AUTHRESULTS
	SET
		Status=@Status,
		EventTime=@EventTime,
		FailureDescription=@FailureDescription
	WHERE
		Id=@Id 
END