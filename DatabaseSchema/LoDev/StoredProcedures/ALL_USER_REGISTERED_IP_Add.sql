-- =============================================
-- Author:		David Dao
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE dbo.ALL_USER_REGISTERED_IP_Add
    @BrokerId UniqueIdentifier,
    @UserId UniqueIdentifier,
    @IpAddress varchar(50),
    @Description varchar(200),
    @IsRegistered bit
AS
BEGIN

	UPDATE ALL_USER_REGISTERED_IP
	   SET IsRegistered = @IsRegistered,
	       Description = @Description,
	       CreatedDate = GETDATE()
	WHERE BrokerId = @BrokerId
	  AND UserId = @UserId
	  AND IpAddress = @IPAddress
	  
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO ALL_USER_REGISTERED_IP (BrokerId, UserId, IPAddress, Description, IsRegistered, CreatedDate)
								   VALUES  (@BrokerId, @UserId, @IPAddress, @Description, @IsRegistered, GETDATE())
	END
                               

END
