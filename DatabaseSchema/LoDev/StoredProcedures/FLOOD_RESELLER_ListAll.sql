-- =============================================
-- Author:		Thien Nguyen
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[FLOOD_RESELLER_ListAll]

AS
BEGIN

    SELECT FloodResellerId, Name, Url
    FROM FLOOD_RESELLER

END
