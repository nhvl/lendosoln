CREATE PROCEDURE [dbo].SaveLandingPageId
	@EmployeeId uniqueidentifier,
	@LandingPageId varchar(50)
AS
BEGIN
	UPDATE Employee
	SET LandingPageId = @LandingPageId
	WHERE EmployeeId = @EmployeeId
END
