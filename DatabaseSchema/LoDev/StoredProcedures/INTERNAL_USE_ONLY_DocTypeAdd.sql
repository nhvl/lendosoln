ALTER procedure [dbo].[INTERNAL_USE_ONLY_DocTypeAdd]
	@BrokerId uniqueidentifier,
	@DocTypeName varchar(50),
	@DocumentId int OUT
as

if exists(select 1 from EDOCS_DOCUMENT_TYPE where DocTypeName = @DocTypeName and BrokerId = @BrokerId and IsValid = 1)
begin
	RAISERROR('A doc type with the same name already exists. Please select another name.', 16, 1);
	return;
end

if exists(select 1 from EDOCS_DOCUMENT_TYPE where DocTypeName = @DocTypeName and BrokerId = @BrokerId and IsValid = 0)
begin
	update EDOCS_DOCUMENT_TYPE
	set 
		IsValid = 1,
		@DocumentId = DocTypeId
	where DocTypeName = @DocTypeName and BrokerId = @BrokerId
end
else
begin
	insert into EDOCS_DOCUMENT_TYPE (BrokerId, DocTypeName, IsValid)
	values (@BrokerId, @DocTypeName, 1)

	SET @DocumentId = SCOPE_IDENTITY()
end