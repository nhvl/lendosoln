-- =============================================
-- Author:		Timothy Jewell
-- Create date:	07/27/2017
-- Description:	Retrieves SSA-89 order(s) for a loan.
-- =============================================
ALTER PROCEDURE [dbo].[SSA89_ORDER_Retrieve]
	@BrokerId uniqueidentifier,
	@LoanId uniqueidentifier,
	@OrderId int = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	IF @OrderId IS NOT NULL
	BEGIN
		SELECT
			voxOrder.BrokerId,
			voxOrder.LoanId,
			voxOrder.ApplicationId,
			voxOrder.OrderId,
			voxOrder.TransactionId,
			voxOrder.VendorId,
			voxOrder.ProviderId,
			voxOrder.HasBeenRefreshed,
			voxOrder.RefreshedFromOrderId,
			voxOrder.IsTestOrder,
			voxOrder.LenderServiceId,
			voxOrder.LenderServiceName,
			voxOrder.OrderNumber,
			voxOrder.Status,
			voxOrder.StatusDescription,
			voxOrder.OrderedBy,
			voxOrder.DateOrdered,
			voxOrder.DateCompleted,
			voxOrder.ErrorMessage,
			voxOrder.NotificationEmail,
			ssa89Order.IsForCoborrower,
			ssa89Order.BorrowerName,
			ssa89Order.LastFourSSN,
			ssa89Order.DateOfBirth,
			ssa89Order.DeathMasterStatus,
			ssa89Order.SocialSecurityAdministrationStatus,
			ssa89Order.OfficeOfForeignAssetsControlStatus,
			ssa89Order.CreditHeaderStatus,
			ssa89Order.RequestFormAccepted,
			ssa89Order.RequestFormRejectionReason
		FROM SSA89_ORDER ssa89Order
		JOIN VOX_ORDER voxOrder ON voxOrder.OrderId = ssa89Order.OrderId
		WHERE voxOrder.BrokerId = @BrokerId
			AND voxOrder.LoanId = @LoanId
			AND voxOrder.OrderId = @OrderId

		SELECT
			d.OrderId,
			d.DocumentId
		FROM VOX_DOCUMENT d
		WHERE d.OrderId = @OrderId
	END
	ELSE -- @OrderId IS NULL
	BEGIN
		SELECT
			voxOrder.BrokerId,
			voxOrder.LoanId,
			voxOrder.ApplicationId,
			voxOrder.OrderId,
			voxOrder.TransactionId,
			voxOrder.VendorId,
			voxOrder.ProviderId,
			voxOrder.HasBeenRefreshed,
			voxOrder.RefreshedFromOrderId,
			voxOrder.IsTestOrder,
			voxOrder.LenderServiceId,
			voxOrder.LenderServiceName,
			voxOrder.OrderNumber,
			voxOrder.Status,
			voxOrder.StatusDescription,
			voxOrder.OrderedBy,
			voxOrder.DateOrdered,
			voxOrder.DateCompleted,
			voxOrder.ErrorMessage,
			voxOrder.NotificationEmail,
			ssa89Order.IsForCoborrower,
			ssa89Order.BorrowerName,
			ssa89Order.LastFourSSN,
			ssa89Order.DateOfBirth,
			ssa89Order.DeathMasterStatus,
			ssa89Order.SocialSecurityAdministrationStatus,
			ssa89Order.OfficeOfForeignAssetsControlStatus,
			ssa89Order.CreditHeaderStatus,
			ssa89Order.RequestFormAccepted,
			ssa89Order.RequestFormRejectionReason
		FROM SSA89_ORDER ssa89Order
		JOIN VOX_ORDER voxOrder ON voxOrder.OrderId = ssa89Order.OrderId
		WHERE voxOrder.BrokerId = @BrokerId
			AND voxOrder.LoanId = @LoanId

		SELECT
			d.OrderId,
			d.DocumentId
		FROM VOX_DOCUMENT d
		JOIN VOX_ORDER o ON d.OrderId = o.OrderId
		WHERE o.BrokerId = @BrokerId
			AND o.LoanId = @LoanId
	END
END
