CREATE PROCEDURE CreateArmIndex
	@BrokerId Guid ,
	@IndexName Varchar( 100 ) ,
	@IndexCurrentValue Decimal ,
	@EffectiveD DateTime ,
	@IndexId Guid OUT
AS
	SET @IndexId = newid()
	INSERT INTO Arm_Index ( BrokerIdGuid , IndexIdGuid , IndexNameVstr , IndexCurrentValueDecimal , EffectiveD )
	VALUES
		( @BrokerId
		, @IndexId
		, @IndexName
		, @IndexCurrentValue
		, @EffectiveD
		)
	IF( @@error != 0 )
	BEGIN
		RAISERROR('error in inserting into table Arm_Index in CreateArmIndex sp', 16, 1);
		RETURN -100
	END
	RETURN 0
