-- =============================================
-- Author:		Justin Lara
-- Create date: 2/20/2018
-- Description:	Saves a credential set or updates
--				if it already exists
-- =============================================
ALTER PROCEDURE [dbo].[CENLAR_LENDER_CREDENTIAL_Save]
	@BrokerId uniqueidentifier,
	@EnvironmentT int,
	@UserId varchar(50),
	@ApiKey varchar(50),
	@LastApiKeyReset datetime,
	@CustomerId varchar(5)
AS
BEGIN
	UPDATE CENLAR_LENDER_CREDENTIAL
	SET
		UserId = @UserId,
		ApiKey = @ApiKey,
		LastApiKeyReset = @LastApiKeyReset,
		CustomerId = @CustomerId
	WHERE BrokerId = @BrokerId
		AND EnvironmentT = @EnvironmentT
		
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO CENLAR_LENDER_CREDENTIAL
		(
			BrokerId,
			EnvironmentT,
			UserId,
			ApiKey,
			LastApiKeyReset,
			CustomerId
		)
		VALUES
		(
			@BrokerId,
			@EnvironmentT,
			@UserId,
			@ApiKey,
			@LastApiKeyReset,
			@CustomerId
		)
	END
END
GO