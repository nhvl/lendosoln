
CREATE PROCEDURE [dbo].[QUICKPRICER_2_FIND_ABANDONED_LOANS] 
	@BrokerId uniqueidentifier, 
	@DeleteBeforeDate datetime
AS
BEGIN

   SELECT b.slid, sbrokerid FROM LOAN_FILE_A a WITH(NOLOCK) 
	LEFT JOIN LOAN_FILE_E e WITH(NOLOCK) on a.sLId = e.slid 
	LEFT JOIN LOAN_FILE_B b with(nolock) on b.slid = a.slid 
  WHERE sloanfilet = 2 AND IsValid = 0 AND sCreatedD < @DeleteBeforeDate AND sBrokerId = @BrokerID;

END
