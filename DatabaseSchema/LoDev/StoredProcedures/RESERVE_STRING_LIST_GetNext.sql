-- =============================================
-- Author:		David Dao
-- Create date: 6/17/2013
-- Description:	Return the next available string in RESERVE_STRING_LIST
-- =============================================
CREATE PROCEDURE [dbo].[RESERVE_STRING_LIST_GetNext]
	@BrokerId UniqueIdentifier,
	@Type varchar(10)
AS
BEGIN

	DELETE FROM RESERVE_STRING_LIST WITH(ROWLOCK, UPDLOCK)
	OUTPUT deleted.id, deleted.StringValue
	WHERE ID IN (SELECT TOP 1 ID
	             FROM RESERVE_STRING_LIST WITH(ROWLOCK, UPDLOCK) 
				 WHERE BrokerId=@BrokerId AND Type=@Type
				 ORDER BY ID)

END
