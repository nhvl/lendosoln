
-- =============================================
-- Author:		Eric Mallare
-- Create date: 9/6/2016
-- Description:	Inserts a new broker client certificate into Broker_Client_Certificate
-- =============================================
CREATE PROCEDURE [dbo].[AddNewBrokerClientCertificate] 
	@CertificateId uniqueidentifier,
	@BrokerId uniqueidentifier,
	@IsRevoked bit = 0,
	@Description varchar(100) = '',
	@NotesFileDbKey uniqueidentifier = null,
	@Level int = '4',
	@UserType int = '0',
	@CreatedDate smalldatetime,
	@RevokedDate smalldatetime = null,
	@AssociatedUserId uniqueidentifier = null,
	@Groups xml = null,
	@GroupType int = null
AS
BEGIN
	DECLARE @TempGroups TABLE
	(
		Id uniqueidentifier NOT NULL,
		Type int NOT NULL
	)
	
	INSERT INTO 
		Broker_Client_Certificate ( 
			CertificateId, 
			BrokerId, 
			IsRevoked, 
			Description, 
			NotesFileDbKey,
			Level,
			UserType,
			CreatedDate,
			RevokedDate,
			AssociatedUserId)
	VALUES 
		   (@CertificateId, 
			@BrokerId, 
			@IsRevoked, 
			@Description, 
			@NotesFileDbKey,
			@Level,
			@UserType,
			@CreatedDate,
			@RevokedDate,
			@AssociatedUserId)
			
	IF @Groups IS NOT NULL AND @GroupType IS NOT NULL
	BEGIN
		INSERT INTO 
			@TempGroups (Id, Type)
		SELECT 
			T.Item.value('.', 'uniqueidentifier'),
			@GroupType 
		FROM 
			@Groups.nodes('root/group/@id') as T(Item)
			
		INSERT INTO
			Group_X_Broker_Client_Certificate(GroupId, CertificateId, GroupType)
		SELECT
			Id, @CertificateId, Type
		FROM
			@TempGroups 
	END			
END

