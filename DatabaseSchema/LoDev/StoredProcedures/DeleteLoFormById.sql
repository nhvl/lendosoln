CREATE PROCEDURE DeleteLoFormById
	@FormId UniqueIdentifier,
	@BrokerId UniqueIdentifier
AS
BEGIN
	DELETE FROM LO_FORM WHERE FormId = @FormId AND BrokerId = @BrokerId
END
