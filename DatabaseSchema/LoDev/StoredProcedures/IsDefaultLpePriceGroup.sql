

CREATE PROCEDURE [dbo].[IsDefaultLpePriceGroup] 
	@BrokerID uniqueidentifier,
	@LpePriceGroupID uniqueidentifier 
AS
	SELECT TOP 1 'Broker' AS DefaultFor 
	FROM BROKER 
	WHERE BrokerId = @BrokerID AND BrokerLpePriceGroupIdDefault = @LpePriceGroupID

	IF (@@ROWCOUNT = 0)
		BEGIN
			SELECT TOP 1 'Branch' AS DefaultFor 
			FROM BRANCH 
			WHERE BrokerId = @BrokerID AND BranchLpePriceGroupIdDefault = @LpePriceGroupID
		END
	



