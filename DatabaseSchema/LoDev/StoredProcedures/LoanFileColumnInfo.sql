

CREATE PROCEDURE [dbo].[LoanFileColumnInfo]
@TableName varchar(30)
AS
	BEGIN
		SELECT column_name ,
		data_type,
		CHARacter_maximum_length,
		IS_NULLABLE
		FROM information_schema.columns
		WHERE table_name = @TableName
		
		SELECT
		c.NAME 'ForeignKey'
		FROM 
		sys.foreign_key_columns fkc 
		INNER JOIN 
		sys.columns c 
		   ON fkc.parent_column_id = c.column_id 
			  AND fkc.parent_object_id = c.object_id
		where   OBJECT_NAME(parent_object_id) = @TableName
	END