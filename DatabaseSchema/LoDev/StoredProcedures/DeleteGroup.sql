ALTER PROCEDURE [dbo].[DeleteGroup]
	@BrokerId uniqueidentifier, 
	@GroupId uniqueidentifier
AS
BEGIN
	SET NOCOUNT ON
	BEGIN TRANSACTION
		DELETE FROM
			Group_X_Broker_Client_Certificate
		WHERE
			GroupId=@GroupId
	
		DELETE FROM 
			[GROUP] 
		WHERE 
			GroupId = @GroupId AND BrokerID=@BrokerId
		
		if( 0!=@@error)
		BEGIN
			ROLLBACK TRANSACTION
			RAISERROR('Error in delete from Group table in DeleteGroup sp', 16, 1);
			RETURN -100;
		END
	COMMIT TRANSACTION
	RETURN 0;
END
