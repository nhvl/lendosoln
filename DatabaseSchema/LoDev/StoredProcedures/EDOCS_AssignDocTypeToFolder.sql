CREATE PROCEDURE [dbo].[EDOCS_AssignDocTypeToFolder]
@DocTypeId int,
@FolderId int
as
update EDOCS_DOCUMENT_TYPE
set FolderId = @FolderId
where 
DocTypeId = @DocTypeId
