CREATE PROCEDURE ChangeBrokerStatus 
	@BrokerId Guid,
	@Status int,
	@UserId Guid
AS
DECLARE @tranPoint SYSNAME
SET @tranPoint = object_name(@@procId) + CAST(@@nestlevel as varchar(10))
BEGIN TRANSACTION
SAVE TRANSACTION @tranPoint
UPDATE Broker SET Status = @Status WHERE BrokerId = @BrokerId
IF( 0!=@@error )
BEGIN
	RAISERROR('error  update broker status in ChangeBrokerStatus  sp', 16, 1);;
	goto ErrorHandler;
END
DECLARE @EventType char
IF @Status = 1 
BEGIN
	SET @EventType= 'e'
END
ELSE
BEGIN
	SET @EventType = 'd'
END
INSERT INTO Usage_Event(FeatureId, UserId, BrokerId, WhoDoneIt, EventType)
VALUES ('00000000-0000-0000-0000-000000000000', '00000000-0000-0000-0000-000000000000', @BrokerId, @UserId, @EventType)
IF( 0!=@@error )
BEGIN
	RAISERROR('error in inserting into Usage_Event table in ChangeBrokerStatus sp', 16, 1);;
	goto ErrorHandler;
END
COMMIT TRANSACTION;
RETURN 0;
ErrorHandler:
	rollback tran @tranPoint
	rollback transaction
	return -100;	
