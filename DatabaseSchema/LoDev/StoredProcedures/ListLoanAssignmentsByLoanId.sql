

CREATE     PROCEDURE [dbo].[ListLoanAssignmentsByLoanId]
	@LoanId uniqueidentifier
AS

SELECT
  a.RoleId,
  e.EmployeeId ,
  e.BranchId,
  e.EmployeeUserId AS UserId,
  e.UserFirstNm ,
  e.UserLastNm ,
  e.Email ,
  b.PmlBrokerId
FROM
  Loan_User_Assignment AS a WITH (NOLOCK) JOIN Employee AS  e  WITH (NOLOCK)  ON e.EmployeeId = a.EmployeeId 
										  LEFT JOIN Broker_User b WITH(NOLOCK) ON e.EmployeeId = b.EmployeeId
WHERE
	a.sLId = @LoanId
if( 0!=@@error )
begin
	RAISERROR('Error in the select statement in ListLoanAssignmentsByLoanId sp', 16, 1);
	return -100;
end

return 0;



