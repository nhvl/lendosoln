/*
	Author: 
		Scott Kibler
	Description: 
		Gets the lo xml files that have been created in the system.
	opm:
		463659
*/
CREATE PROCEDURE [dbo].[LO_XML_FILE_GetAll]
AS
BEGIN
	SELECT Id, Name
	FROM
		LO_XML_FILE
END