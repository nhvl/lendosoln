-- =============================================
-- Author:		Isaac Ribakoff
-- Create date: 9/19/2014
-- Description: Gets Valid MI Vendors
-- =============================================
ALTER PROCEDURE [dbo].[MORTGAGE_INSURANCE_VENDOR_CONFIGURATION_Get]
	@VendorId UniqueIdentifier,
	@AllowInvalid bit = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT *
	FROM MORTGAGE_INSURANCE_VENDOR_CONFIGURATION
	WHERE (IsValid = 1 OR @AllowInvalid = 1)
		AND ( VendorId = @VendorId )
END