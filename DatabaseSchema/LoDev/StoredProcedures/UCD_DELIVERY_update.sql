ALTER PROCEDURE [dbo].[UCD_DELIVERY_update]
	@UcdDeliveryId int,
    @LoanId uniqueidentifier,	-- Passed in, but not used.
    @BrokerId uniqueidentifier,	-- Passed in, but not used.
    @UcdDeliveryType int = null,
    @DeliveredTo int = null,
    @FileDeliveredDocumentId uniqueidentifier = null,
    @CaseFileId varchar(50) = null,
    @DateOfDelivery smalldatetime = null,
    @Note varchar(500),
    @IncludeInUldd bit
AS
BEGIN
	IF @UcdDeliveryType IS NOT NULL AND @UcdDeliveryType=0 -- Not Manual delivery type.
		BEGIN
			UPDATE UCD_DELIVERY
			SET
				UcdDeliveryType = @UcdDeliveryType,
				DeliveredTo = @DeliveredTo,
				FileDeliveredDocumentId = @FileDeliveredDocumentId,
				CaseFileId = @CaseFileId,
				DateOfDelivery = @DateOfDelivery,
				Note = @Note,
				IncludeInUldd = @IncludeInUldd
			WHERE UcdDeliveryId = @UcdDeliveryId
				--AND LoanId = @LoanId
				--AND BrokerId = @BrokerId
		END
	ELSE
		BEGIN
			UPDATE UCD_DELIVERY
			SET
				UcdDeliveryType = COALESCE(@UcdDeliveryType, UcdDeliveryType),
				DeliveredTo = COALESCE(@DeliveredTo, DeliveredTo),
				FileDeliveredDocumentId = COALESCE(@FileDeliveredDocumentId, FileDeliveredDocumentId),
				CaseFileId = COALESCE(@CaseFileId, CaseFileId),
				DateOfDelivery = COALESCE(@DateOfDelivery, DateOfDelivery),
				Note = @Note,
				IncludeInUldd = @IncludeInUldd
			WHERE UcdDeliveryId = @UcdDeliveryId
				--AND LoanId = @LoanId
				--AND BrokerId = @BrokerId
		END
END
