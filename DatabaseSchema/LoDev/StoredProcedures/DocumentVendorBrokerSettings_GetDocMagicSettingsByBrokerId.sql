-- =============================================
-- Author: Scott Kibler
-- Create date: 6/30/2014
-- Description: Use this instead of DocumentVendorBrokerSettings_ListByBrokerId 
-- for docmagic vendor settings.
-- =============================================
CREATE PROCEDURE [dbo].[DocumentVendorBrokerSettings_GetDocMagicSettingsByBrokerId]
	@BrokerId uniqueidentifier
AS
BEGIN
    SELECT * FROM DOCUMENT_VENDOR_BROKER_SETTINGS
	WHERE BrokerId = @BrokerId
	AND VendorId='00000000-0000-0000-0000-000000000000' --DocMagic conventionally.
	ORDER BY VendorIndex -- adding this because there is no unique constraint on VendorId.
END
