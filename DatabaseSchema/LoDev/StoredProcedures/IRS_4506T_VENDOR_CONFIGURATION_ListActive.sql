-- =============================================
-- Author:		David Dao
-- Create date: 8/2/2013
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[IRS_4506T_VENDOR_CONFIGURATION_ListActive]
AS
BEGIN

	SELECT VendorId, VendorName, IsValid, ExportPath, IsUseAccountId, EnableConnectionTest, CommunicationModel
	FROM IRS_4506T_VENDOR_CONFIGURATION
	WHERE IsValid=1
	ORDER BY VendorName

END
