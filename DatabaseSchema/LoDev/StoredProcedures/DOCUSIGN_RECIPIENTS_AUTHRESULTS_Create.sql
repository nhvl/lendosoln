-- =============================================
-- Author:		Eric Mallare
-- Create date: 1/23/2018
-- Description:	Inserts a DocuSign recipient auth result record into the DB.
-- =============================================
ALTER PROCEDURE [dbo].[DOCUSIGN_RECIPIENTS_AUTHRESULTS_Create]
	@Status int,
	@Type int,
	@EventTime datetime = null,
	@FailureDescription varchar(500) = null,
	@RecipientReferenceId int,
	@EnvelopeReferenceId int,
	@Id int OUTPUT
AS
BEGIN
	INSERT INTO DOCUSIGN_RECIPIENTS_AUTHRESULTS
		(Status,
		 Type,
		 EventTime,
		 FailureDescription,
		 RecipientReferenceId,
		 EnvelopeReferenceId)
	VALUES
		(@Status,
		 @Type,
		 @EventTime,
		 @FailureDescription,
		 @RecipientReferenceId,
		 @EnvelopeReferenceId)

	SET @Id=SCOPE_IDENTITY()
END