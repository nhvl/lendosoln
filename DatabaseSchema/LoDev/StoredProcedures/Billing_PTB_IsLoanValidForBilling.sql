ALTER PROCEDURE [dbo].[Billing_PTB_IsLoanValidForBilling]
	@sLId UniqueIdentifier,
	@OnDate smalldatetime,
	@BillingType int
AS
BEGIN
	--If this loan was billed in the last 90 days, we won't bill for it again.
	select cast(case when count(*) > 0 then DocumentVendor end as int) as DocVendor, BillingDate 
	from [BILLING_PER_TRANSACTION] 
	where 
		(
		   (@BillingType = 0 and BillingDate > dateadd(day, -90, @OnDate))
		OR (@BillingType = 1)
		)
	  and @sLId = sLId
	group by DocumentVendor, BillingDate
	order by BillingDate DESC
END
