
CREATE PROCEDURE [dbo].[GetBranchIdForNewFileByTemplateId]
	@TemplateId uniqueidentifier
AS
BEGIN
	SELECT sBranchIdForNewFileFromTemplate
	FROM LOAN_FILE_F with(nolock)
	WHERE sLId = @TemplateId
END
