

CREATE PROCEDURE [dbo].[TEAM_ListTeamsByLoan] 
	@LoanId		uniqueidentifier,
	@BrokerId	uniqueidentifier
AS
	SELECT
		t.Id,
		t.Name,
		t.RoleId,
		t.Notes,
		CASE WHEN t.RoleId = tla.RoleId THEN 1 ELSE 0 END AS TeamHasAssignedRole
		
	FROM TEAM t
	LEFT JOIN TEAM_LOAN_ASSIGNMENT tla ON t.Id = tla.TeamId
	WHERE
	tla.LoanId = @LoanId
	and
	t.BrokerId = @BrokerId

	
