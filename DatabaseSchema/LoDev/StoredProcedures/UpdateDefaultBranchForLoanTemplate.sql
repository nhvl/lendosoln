
-- =============================================
-- Author:		Peter Anargirou
-- Create date: August 5, 2008
-- Description:	Updates a loan template to use a different default branch
-- =============================================
CREATE PROCEDURE [dbo].[UpdateDefaultBranchForLoanTemplate]
	@LoanId uniqueidentifier,
	@BranchId uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE LOAN_FILE_F
		SET sBranchIdForNewFileFromTemplate = @BranchId
		WHERE sLId = @LoanId
END

