-- =============================================
-- Author:		Antonio Valencia
-- Create date: August 15, 2011
-- Description:	Restore document type
-- =============================================
CREATE PROCEDURE EDOCS_RestoreDocType 
	-- Add the parameters for the stored procedure here
	@DocTypeId int,
	@BrokerId uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

UPDATE EDOCS_DOCUMENT_TYPE
    SET IsValid = 1
    WHERE DocTypeId = @DocTypeId 
		and BrokerId = @BrokerId
	
END
