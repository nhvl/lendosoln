-- =============================================
-- Author:		Justin Jia
-- ALTER date: June 14, 2012
-- Description:	Gets all unique indexes and its corresponding tables and columns.
-- =============================================
CREATE PROCEDURE [dbo].[Justin_Testing_GetDBConnectionByBrokerId]
	-- Add the parameters for the stored procedure here
	@BrokerId UniqueIdentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT   	DbConnectionId
	FROM		DB_CONNECTION_x_BROKER	
	WHERE		BrokerId = @BrokerId 

END
