


-- =============================================
-- Author:		Antonio Valencia
-- Create date: 9-30-08
-- Description:	Retrieves a list of users with the given do du logins
-- =============================================
CREATE PROCEDURE [dbo].[GetDUDOWithNames] 
	@BrokerID uniqueidentifier, 
	@DOLogin	varchar(50),
	@DULogin	varchar(50) 
AS
BEGIN
	SET NOCOUNT ON;
	

		select 'Active' as isActive,
				pml.LoginNm as login,
				CONVERT(VARCHAR, RecentLoginD, 101) as lastLogin,  
				(pml.UserFirstNm + ' ' + pml.UserLastNm) as username, 	
				bu.EmployeeID as employeeID,
				bu.DOAutoLoginNm as doLogin, 
				bu.DUAutoLoginNm as duLogin
		from VIEW_ACTIVE_PML_USER_WITH_BROKER_INFO as pml, BROKER_USER as bu
		WHERE bu.EmployeeId = pml.EmployeeId 
			AND  pml.BrokerID = @BrokerID 
			AND (( @DOLogin <> '' AND bu.DOAutoLoginNm = @DOLogin ) OR (@DULogin <> '' AND bu.DUAutoLoginNm = @DULogin ))
			
	UNION 

		select 'Inactive' as isActive,
				pml.LoginNm as login,
				CONVERT(VARCHAR, RecentLoginD, 101) as lastLogin,  
				(pml.UserFirstNm + ' ' + pml.UserLastNm) as username, 	
				bu.EmployeeID as employeeID,
				bu.DOAutoLoginNm as doLogin, 
				bu.DUAutoLoginNm as duLogin
		from VIEW_DISABLED_PML_USER_WITH_BROKER_INFO as pml, BROKER_USER as bu
		WHERE bu.EmployeeId = pml.EmployeeId 
			AND  pml.BrokerID = @BrokerID 
			AND (( @DOLogin <> '' AND bu.DOAutoLoginNm = @DOLogin ) OR (@DULogin <> '' AND bu.DUAutoLoginNm = @DULogin ))

	UNION 
		select 'Active' as isActive,
				pml.LoginNm as login,
				CONVERT(VARCHAR, RecentLoginD, 101) as lastLogin,  
				(pml.UserFirstNm + ' ' + pml.UserLastNm) as username, 	
				bu.EmployeeID as employeeID,
				bu.DOAutoLoginNm as doLogin, 
				bu.DUAutoLoginNm as duLogin
		from VIEW_ACTIVE_LO_USER as pml, BROKER_USER as bu
		WHERE bu.EmployeeId = pml.EmployeeId 
			AND  pml.BrokerID = @BrokerID 
			AND (( @DOLogin <> '' AND bu.DOAutoLoginNm = @DOLogin ) OR (@DULogin <> '' AND bu.DUAutoLoginNm = @DULogin ))

	UNION 

		select 'Inactive' as isActive,
				pml.LoginNm as login,
				CONVERT(VARCHAR, RecentLoginD, 101) as lastLogin,  
				(pml.UserFirstNm + ' ' + pml.UserLastNm) as username, 	
				bu.EmployeeID as employeeID,
				bu.DOAutoLoginNm as doLogin, 
				bu.DUAutoLoginNm as duLogin
		from VIEW_DISABLED_LO_USER as pml, BROKER_USER as bu
		WHERE bu.EmployeeId = pml.EmployeeId 
			AND  pml.BrokerID = @BrokerID 
			AND (( @DOLogin <> '' AND bu.DOAutoLoginNm = @DOLogin ) OR (@DULogin <> '' AND bu.DUAutoLoginNm = @DULogin ))

END



