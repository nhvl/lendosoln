-- =============================================
-- Author:		David Dao
-- Create date: <Create Date,,>
-- Description:	Create a clean up stored procedure for Unit Test
-- =============================================
ALTER PROCEDURE [dbo].[INTERNAL_DeletePmlBroker]
	@PmlBrokerId UniqueIdentifier
	, @BrokerId UniqueIdentifier
AS
BEGIN
	DELETE FROM PML_BROKER_PERMISSIONS
	WHERE BrokerId = @BrokerId AND PmlBrokerId = @PmlBrokerId
	
	DELETE FROM PML_BROKER_RELATIONSHIPS
	WHERE BrokerId = @BrokerId AND PmlBrokerId = @PmlBrokerId

	DELETE FROM Pml_Broker
	WHERE BrokerId = @BrokerId AND PmlBrokerId = @PmlBrokerId
END
