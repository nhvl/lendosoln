ALTER PROCEDURE [dbo].[RetrieveAutoDoDuLoginByUserId]
	@UserId Guid
AS
BEGIN

	SELECT
		DOAutoLoginNm,
		DOAutoPassword,
		EncryptedDOAutoPassword,
		DUAutoLoginNm,
		DUAutoPassword,
		EncryptedDUAutoPassword,
		EncryptionKeyId
	FROM Broker_User
	WHERE UserId = @UserId
END
