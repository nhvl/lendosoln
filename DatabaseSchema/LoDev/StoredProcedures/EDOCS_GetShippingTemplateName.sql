CREATE procedure [dbo].EDOCS_GetShippingTemplateName
	@ShippingTemplateId int,
	@BrokerId uniqueidentifier = null
as

select ShippingTemplateName
	from EDOCS_SHIPPING_TEMPLATE
	where ShippingTemplateId = @ShippingTemplateId
	and (BrokerId = @BrokerId 
		or BrokerId is null)