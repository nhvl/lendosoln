-- =============================================
-- Author:		Justin Kim
-- Create date: 6/29/17
-- Description:	Add a new tracked feature to the tracked_feature table
-- =============================================
ALTER PROCEDURE [dbo].[AddNewTrackedFeature]	
	@FeatureId int
AS
BEGIN
	SET NOCOUNT ON;
	IF NOT EXISTS
	(
		SELECT 1
		FROM TRACKED_FEATURE
		WHERE FeatureId = @FeatureId
	)
	BEGIN
		INSERT INTO TRACKED_FEATURE(FeatureId)
		VALUES(@FeatureId)
	END;
END
