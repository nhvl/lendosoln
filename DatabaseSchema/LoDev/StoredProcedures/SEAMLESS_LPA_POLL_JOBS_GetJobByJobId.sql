-- =============================================
-- Author:		Eric Mallare
-- Create date: 11/28/2017
-- Description:	Gets a Seamless LPA Poll job
-- =============================================
ALTER PROCEDURE [dbo].[SEAMLESS_LPA_POLL_JOBS_GetJobByJobId]
	@JobId int
AS
BEGIN
	SELECT
		j.JobId,
		j.ProcessingStatus,
		j.NextRunDate,
		j.PublicJobId,
		j.DateCreated,
		j.ExpirationDate,
		j.LastRunStartDate,
		j.MaxExceptionCount,
		j.ExceptionCount,
		j.DateCompleted,
		j.CorrelationId,
		lpa.PollAsyncId,
		lpa.PollingIntervalInSeconds,
		lpa.PollingAttemptCount,
		lpa.LoanId,		-- 11/2/18 - je - For backwards compatibility we get LoanId, BrokerId, and UserID
		lpa.BrokerId,	-- from the SEAMLESS_LPA_POLL_JOBS table.
		lpa.UserId,		-- We should be able to change this in the future if necessary.
		lpa.UserType,
		lpa.LpaUsername,
		lpa.LpaUserPassword,
		lpa.LpaSellerNumber,
		lpa.LpaSellerPassword,
		lpa.ResultFileDbKey,
		lpa.CreditReportType
	FROM
		SEAMLESS_LPA_POLL_JOBS lpa JOIN
		BACKGROUND_JOBS j ON lpa.JobId=j.JobId
	WHERE
		lpa.JobId=@JobId
END