/*
	Author: Scott Kibler
	Created Date: 8/18/2014
	Description: 
*/
CREATE PROCEDURE dbo.QUICKPRICER_2_FEATURE_FindAbandonedLoans
	@IsValid BIT = NULL,
	@BrokerID UniqueIdentifier = NULL
AS
BEGIN
	SELECT 
		lfb.slid AS LoanID,
		lfc.sLNm As LoanName,
		lfc.IsValid,
		lfc.sBrokerId as BrokerID
	FROM 
				  LOAN_FILE_B as lfb
		LEFT JOIN LOAN_FILE_CACHE as lfc
		ON lfb.slid = lfc.slid
	WHERE
		  lfb.sLoanFileT = 2
	  AND lfb.sLId NOT IN
			(
				SELECT loanid
				FROM quickpricer_2_loan_usage
			)
	  AND (@BrokerID IS NULL OR sBrokerID = @BrokerID)
	  AND (@IsValid IS NULL OR IsValid = @IsValid)
END