-- =============================================
-- Author:		paoloa
-- Create date: 5/30/2013
-- Description:	
-- =============================================
ALTER PROCEDURE dbo.ConsumerPortal_Retrieve
	-- Add the parameters for the stored procedure here
	@Id uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT c.* FROM CONSUMER_PORTAL c
	join broker b on b.brokerid = c.brokerid
	where b.status = 1 and Id = @Id
END
