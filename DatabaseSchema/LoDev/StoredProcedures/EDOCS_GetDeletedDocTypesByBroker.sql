ALTER procedure [dbo].[EDOCS_GetDeletedDocTypesByBroker]
	@BrokerId uniqueidentifier,
	@DocTypeName varchar(50) = null
AS 
	SELECT 
        DocTypeName, 
        DocTypeId, 
        ClassificationId, 
        DefaultForPageId, 
        doc.BrokerId, 	
        folder.FolderId, 
        folder.foldername,
	    COALESCE((SELECT CAST(roleid as varchar(MAX)) +':' + CAST(ispmluser as varchar(MAX)) + ','		
            FROM edocs_folder_x_role as f WHERE f.folderid = folder.folderid 	FOR XML PATH ('')	), '') as FolderRoles,
        ESignTargetDocTypeId 
	FROM EDOCS_DOCUMENT_TYPE as doc left join 
        Edocs_Folder as folder ON folder.folderid = doc.folderid LEFT JOIN 
        EDOCS_DOCUMENT_TYPE_MAPPING map ON doc.DocTypeId = map.SourceDocTypeId
	WHERE doc.BrokerId = @BrokerId AND IsValid = 0 AND DocTypeName = COALESCE(@DocTypeName, DocTypeName)
	ORDER BY DocTypeName
