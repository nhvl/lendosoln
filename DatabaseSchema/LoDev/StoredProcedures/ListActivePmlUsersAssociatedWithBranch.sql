

CREATE     PROCEDURE [dbo].[ListActivePmlUsersAssociatedWithBranch]
	@BrokerId uniqueidentifier, 
	@BranchId uniqueidentifier
AS
	SELECT TOP 100
		v.UserLastNm + ', ' + v.UserFirstNm AS UserName ,
		v.LoginNm ,
		CASE v.IsActive WHEN 0 THEN 'No' WHEN 1 THEN 'Yes' END  AS IsActive,
		'PML User' AS Type,
		v.EmployeeId ,
		v.LicenseNumber
	FROM View_Active_Pml_User_With_Broker_Info AS v
	WHERE v.BrokerId = @BrokerId AND v.BranchId = @BranchId
