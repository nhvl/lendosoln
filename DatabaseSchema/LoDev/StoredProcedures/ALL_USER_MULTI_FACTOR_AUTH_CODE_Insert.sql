-- =============================================
-- Author:		David Dao
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE dbo.ALL_USER_MULTI_FACTOR_AUTH_CODE_Insert
    @UserId UniqueIdentifier,
    @AuthCode varchar(10),
    @MaxAllowNumberOfFailedAttempts int,
    @ExpiresInMinutes int
AS
BEGIN

    UPDATE ALL_USER_MULTI_FACTOR_AUTH_CODE
       SET AuthCode=@AuthCode,
           CreatedDate = GETDATE(),
           ExpiredDate = DATEADD(minute, @ExpiresInMinutes, getdate()),
           NumberOfFailedAttempts = 0,
           MaxAllowNumberOfFailedAttempts = @MaxAllowNumberOfFailedAttempts
    WHERE UserId = @UserId
                                           
    IF @@ROWCOUNT = 0
    BEGIN
        -- If record does not exists then create new one.
        INSERT INTO ALL_USER_MULTI_FACTOR_AUTH_CODE(UserId, AuthCode, ExpiredDate, NumberOfFailedAttempts, MaxAllowNumberOfFailedAttempts)
                                              VALUES (@UserId, @AuthCode, DATEADD(minute, @ExpiresInMinutes, getdate()), 0, @MaxAllowNumberOfFailedAttempts)
    
    END

END
