-- =============================================
-- Author:		Antonio Valencia
-- Create date: 8/1/2012
-- Description:	Saves Draft XML 
-- =============================================
CREATE PROCEDURE [dbo].[CC_TEMPLATE_SaveDraftXML]
	@BrokerId uniqueidentifier,
	@Id int, 
	@Xml varchar(max)
AS
BEGIN

	-- there should only be one draft 
	UPDATE TOP(1) CC_TEMPLATE_SYSTEM
	SET RuleXml = @Xml 
	WHERE BrokerId = @BrokerId AND ReleasedD IS NULL And id = @Id  
	select @@rowcount
		
END
