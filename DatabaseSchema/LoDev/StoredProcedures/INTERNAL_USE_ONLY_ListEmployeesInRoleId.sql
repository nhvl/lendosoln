ALTER PROCEDURE [dbo].[INTERNAL_USE_ONLY_ListEmployeesInRoleId]
	@RoleId UniqueIdentifier = null,
	@BrokerID uniqueidentifier = null
AS
	SELECT e.EmployeeId, b.BrokerId
	FROM Employee e JOIN Branch b ON e.BranchId = b.BranchId   
					join broker br on b.brokerid = br.brokerid
					JOIN Role_Assignment ra ON e.EmployeeId = ra.EmployeeId
	WHERE (@RoleId IS NULL OR ra.RoleId = @RoleId)
	  AND (@BrokerId IS NULL OR b.BrokerId = @BrokerId)
	  and (br.status = 1)