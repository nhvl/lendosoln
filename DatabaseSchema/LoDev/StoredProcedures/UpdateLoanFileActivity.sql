-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE dbo.UpdateLoanFileActivity
	@sLId UniqueIdentifier,
	@XmlContent varchar(max),
	@sLatestActivityDateForMobileApp smalldatetime,
	@sLatestActivityType int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	UPDATE LOAN_FILE_ACTIVITY 
	   SET XmlContent = @XmlContent, 
	       sLatestActivityType=@sLatestActivityType, 
		   sLatestActivityDateForMobileApp = @sLatestActivityDateForMobileApp
   WHERE sLId = @sLId
	
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO LOAN_FILE_ACTIVITY(sLId, XmlContent,sLatestActivityType,sLatestActivityDateForMobileApp) VALUES(@sLId, @XmlContent, @sLatestActivityType,@sLatestActivityDateForMobileApp)
	END
	
	UPDATE LOAN_FILE_CACHE_2 SET sLatestActivityDateForMobileApp=@sLatestActivityDateForMobileApp,sLatestActivityType=@sLatestActivityType  WHERE sLId = @sLId
END
