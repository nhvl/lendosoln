CREATE PROCEDURE [dbo].[Region_Delete]
	@BrokerId uniqueidentifier,
	@RegionId int
AS
BEGIN
	DELETE FROM REGION_X_FIPS
	WHERE BrokerId = @BrokerId
		AND RegionId = @RegionId
		
	DELETE FROM REGION
	WHERE BrokerId = @BrokerId
		AND RegionId = @RegionId
END