ALTER PROCEDURE [dbo].[RetrievePmlBrokerByCompanyId]
	@CompanyId varchar(36)
	, @BrokerId UniqueIdentifier
AS
BEGIN
	SELECT 
		p.*, 
		b.BranchNm, 
		l.LpePriceGroupName,
		perm.CanApplyForIneligibleLoanPrograms, 
		perm.CanRunPricingEngineWithoutCreditReport, 
		perm.CanSubmitWithoutCreditReport, 
		perm.CanAccessTotalScorecard, 
		perm.AllowOrder4506T, 
		perm.TaskRelatedEmailOptionT,
		dbo.GetPmlBrokerRelationshipsAsXml(p.BrokerId, p.PmlBrokerId) as 'RelationshipXml'
	FROM Pml_Broker p
	LEFT OUTER JOIN PML_BROKER_PERMISSIONS perm ON p.PmlBrokerId = perm.PmlBrokerId
	LEFT OUTER JOIN BRANCH b ON p.BranchId = b.BranchId
	LEFT OUTER JOIN LPE_PRICE_GROUP l ON p.LpePriceGroupId = l.LpePriceGroupId
	WHERE p.CompanyId = @CompanyId AND p.BrokerId = @BrokerId
END


