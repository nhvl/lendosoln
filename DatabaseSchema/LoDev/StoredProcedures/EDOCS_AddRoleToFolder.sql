CREATE procedure [dbo].[EDOCS_AddRoleToFolder]
	@FolderId int,	
	@RoleId uniqueidentifier,
	@IsPmlUser bit
	
as

begin
	insert EDOCS_FOLDER_X_ROLE (FolderId, RoleId, IsPmlUser)
		values (@FolderId, @RoleId, @IsPmlUser)
end