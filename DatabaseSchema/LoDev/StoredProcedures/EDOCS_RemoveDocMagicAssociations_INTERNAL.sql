
-- =============================================
-- Author:		Antonio Valencia
-- Create date: 8/22/11
-- Description:	Remove all associations for given doctype
-- =============================================
CREATE PROCEDURE [dbo].[EDOCS_RemoveDocMagicAssociations_INTERNAL] 
	-- Add the parameters for the stored procedure here
	@BrokerId uniqueidentifier


AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DELETE FROM EDOCS_DOCMAGIC_DOCTYPE_X_DOCTYPE 
	WHERE BrokerID = @BrokerId 
END

