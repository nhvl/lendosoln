CREATE PROCEDURE GetBrokerActivationCap
	@BrokerId Guid = NULL
AS
	SELECT b.BrokerId , b.BrokerNm , b.ActivationCap , b.ActivationCapLocked , b.Status
	FROM
		Broker AS b WITH (NOLOCK)
	WHERE
		BrokerId = COALESCE( @BrokerId , b.BrokerId )
	if( 0!=@@error)
	begin
		RAISERROR('Error in the selection statement in GetBrokerActivationCap sp', 16, 1);
		return -100;
	end
	return 0;
