-- =============================================
-- Author:		paoloa
-- Create date: 8/2013
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[DOCUMENT_VENDOR_SaveBranchCredentials]
	-- Add the parameters for the stored procedure here
	@BranchId uniqueidentifier,
	@VendorId uniqueidentifier,
	@Login varchar(50),
	@Password varchar(150),
	@AccountId varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	UPDATE DOCUMENT_VENDOR_BRANCH_CREDENTIALS
	SET
		[Login] = @Login,
		[Password] = @Password,
		AccountId = @AccountId
	WHERE 
		BranchId = @BranchId AND VendorId = @VendorId
		
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO DOCUMENT_VENDOR_BRANCH_CREDENTIALS(BranchId, Vendorid, [Login], [Password], AccountId)
		VALUES (@BranchId, @VendorId, @Login, @Password, @AccountId)
	END
END
