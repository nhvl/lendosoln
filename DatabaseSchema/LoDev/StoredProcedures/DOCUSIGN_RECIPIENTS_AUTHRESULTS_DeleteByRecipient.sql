-- =============================================
-- Author:		Eric Mallare
-- Create date: 2/13/2018
-- Description:	Deletes the auth results associated with a recipient and envelope
-- =============================================
CREATE PROCEDURE [dbo].[DOCUSIGN_RECIPIENTS_AUTHRESULTS_DeleteByRecipient]
	@EnvelopeReferenceId int,
	@RecipientReferenceId int
AS
BEGIN
	DELETE FROM 
		DOCUSIGN_RECIPIENTS_AUTHRESULTS
	WHERE
		RecipientReferenceId=@RecipientReferenceId AND
		EnvelopeReferenceId=@EnvelopeReferenceId
END