
CREATE PROCEDURE [dbo].[BatchPasswordOptionsUpdate] 
	@BrokerID uniqueidentifier,
	@BranchID uniqueidentifier = NULL, -- Default is apply to all branch
	@PasswordExpirationD SmallDateTime = NULL, -- Default is password does not expired.
	@PasswordExpirationPeriod int = 0 -- Default is no expiration period
AS
	UPDATE All_User
	       SET PasswordExpirationD = @PasswordExpirationD, PasswordExpirationPeriod = @PasswordExpirationPeriod
	FROM Branch br JOIN Employee e ON br.BranchID = e.BranchID
                                        JOIN Broker_User bu ON e.EmployeeID = bu.EmployeeID
                                        JOIN All_user a ON bu.UserID = a.UserID
	WHERE br.BrokerID = @BrokerID
                    AND br.BranchID = COALESCE(@BranchID, br.BranchID)
                    AND ((a.IsActiveDirectoryUser = 0) OR (a.Type <> 'B'))

