-- =============================================
-- Author:		Eric Mallare
-- Create date: 5/1/2017
-- Description:	Loads a verification vendor using a vendor id.
-- =============================================
CREATE PROCEDURE [dbo].[VERIFICATION_VENDOR_GetVendorWithVendorId]
	@VendorId int
AS
BEGIN
	SELECT
		v.VendorId, v.CompanyName, v.IsEnabled, v.IsTestVendor, v.CanPayWithCreditCard, v.DuValidationServiceProviderId, v.PlatformId,
		v.IsOverridingPlatform, v.OverridingTransmissionId, v.MclCraId,
		t.TransmissionId, t.TransmissionAssociationT, t.TargetUrl, t.TransmissionAuthenticationT, t.EncryptionKeyId, t.RequestCertLocation, t.ResponseCertId, t.RequestCredentialName, t.RequestCredentialPassword, t.ResponseCredentialName, t.ResponseCredentialPassword
	FROM
		VERIFICATION_VENDOR v JOIN
		VERIFICATION_TRANSMISSION t ON v.OverridingTransmissionId=t.TransmissionId
	WHERE
		v.VendorId=@VendorId

	SELECT
		voa.VendorServiceId, voa.ServiceT, voa.IsADirectVendor, voa.SellsViaResellers, voa.IsAReseller, voa.AskForNotificationEmail,
		voa.VoaVerificationT, voa.VendorId, voa.OptionsFileDbKey, voa.IsEnabled
	FROM 
		VOA_SERVICE voa
	WHERE
		voa.VendorId=@VendorId

	SELECT
		voe.VendorServiceId, voe.ServiceT, voe.IsADirectVendor, voe.SellsViaResellers, voe.IsAReseller, 
		voe.VoeVerificationT, voe.VendorId, voe.IsEnabled, voe.AskForSalaryKey
	FROM
		VOE_SERVICE voe
	WHERE
		voe.VendorId=@VendorId

	SELECT
		ssa89.VendorServiceId, 2 AS ServiceT, ssa89.VendorId, ssa89.IsEnabled, ssa89.IsADirectVendor, ssa89.SellsViaResellers, ssa89.IsAReseller
	FROM
		SSA89_SERVICE ssa89
	WHERE
		ssa89.VendorId=@VendorId
END
