-- =============================================
-- Author:		Eric Mallare
-- Create date: 11/4/2014
-- Description:	Update MainTabsSettingXmlContent
-- =============================================
CREATE PROCEDURE [dbo].[UpdateMainTabsSettingXmlContent] 
	@UserId uniqueidentifier,
	@XmlContent varchar(max)  
AS
	UPDATE Broker_User
	SET
		MainTabsSettingXmlContent = @XmlContent
	WHERE
		UserId = @UserId