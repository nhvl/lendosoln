-- =============================================
-- Author:		Antonio Valencia
-- Create date: 4/21/11
-- Description:	Update Condition Choice Rank Order
-- =============================================
ALTER PROCEDURE [dbo].[TASK_ConditionChoice_SaveOrder] 
	-- Add the parameters for the stored procedure here
	@Id int,
	@BrokerId uniqueidentifier ,
	@Rank int
AS

-- REVIEW
-- 6/9/2011: Reviewed. -ThinhNK
BEGIN
	UPDATE CONDITION_CHOICE WITH(ROWLOCK, READPAST, UPDLOCK)
	SET Rank = @Rank
	WHERE BrokerId = @BrokerId and ConditionChoiceId = @Id

	IF @@rowcount = 0 BEGIN
		RAISERROR('No record updated', 16, 1);
	END
END
