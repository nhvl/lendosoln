-- =============================================
-- Author:		Eric Mallare
-- Create date: 8/16/2017
-- Description:	Creates a Title platform.
-- =============================================
ALTER PROCEDURE [dbo].[TITLE_CreatePlatform]
	@PlatformName varchar(150),
	@TransmissionModelT int,
	@PayloadFormatT int,
	@UsesAccountId bit,
	@RequiresAccountId bit,
	@TargetUrl varchar(200),
	@TransmissionAuthenticationT int,
	@TransmissionAssociationT int,
	@EncryptionKeyId uniqueidentifier,
	@ResponseCertId uniqueidentifier = null,
	@RequestCredentialName varchar(50) = null,
	@RequestCredentialPassword varbinary(max) = null,
	@ResponseCredentialName varchar(50) = null,
	@ResponseCredentialPassword varbinary(max) = null,
	@TransmissionId int OUTPUT,
	@PlatformId int OUTPUT
AS
BEGIN TRANSACTION
	INSERT INTO TITLE_FRAMEWORK_TRANSMISSION_INFO
		(TransmissionAssociationT, TargetUrl, TransmissionAuthenticationT, EncryptionKeyId, ResponseCertId,
		 RequestCredentialName, RequestCredentialPassword, ResponseCredentialName, ResponseCredentialPassword)
	VALUES
		(0, @TargetUrl, @TransmissionAuthenticationT, @EncryptionKeyId, @ResponseCertId, 
		 @RequestCredentialName, @RequestCredentialPassword, @ResponseCredentialName, @ResponseCredentialPassword)
	
	IF( @@error != 0 )
      	GOTO HANDLE_ERROR
      	
	SET @TransmissionId=SCOPE_IDENTITY();
		 
	INSERT INTO TITLE_FRAMEWORK_PLATFORM
		(PlatformName, TransmissionModelT, PayloadFormatT, TransmissionInfoId, UsesAccountId, RequiresAccountId)
	VALUES
		(@PlatformName, @TransmissionModelT, @PayloadFormatT, @TransmissionId, @UsesAccountId, @RequiresAccountId)   
	
	IF( @@error != 0 )
      	GOTO HANDLE_ERROR
	SET @PlatformId=SCOPE_IDENTITY();
	   				
COMMIT TRANSACTION

RETURN;

	HANDLE_ERROR:
     	ROLLBACK TRANSACTION
    	RAISERROR('Error in TITLE_CreatePlatform sp', 16, 1);;
    	RETURN;
