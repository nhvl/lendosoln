-- =============================================
-- Author:		Antonio Valencia
-- Create date: 9-23-08
-- Description:	Returns the location of the given ip in the users ip string. 
-- If the ip is not part of the string 0 is returned otherwise it is part of the string. 
-- If the ip string is empty -1 is returned
-- =============================================
CREATE PROCEDURE [dbo].[CheckIpForUser]
	@UserId uniqueidentifier,
	@IpAddress varchar(15)
AS
BEGIN
	SET NOCOUNT ON;
	
	-- dd 12/1/2011 - The ip 172.16.99.18 is from us and we must ignore this ip in order for edocs.lendersoffice.com
	--                to work with user has IP restriction turn on.
	IF @IpAddress = '172.16.99.18'
	BEGIN
		SELECT -1 as result
		return;
	END
	DECLARE @iplist varchar(200);
	select @iplist = MustLogInFromTheseIpAddresses from Broker_user where UserId = @UserId
	IF @iplist = '' 
	begin 
		SELECT -1 as result 
		return;	
	end 
	print charindex( @IpAddress, @iplist)
	select charindex( @IpAddress, @iplist) as result
END
