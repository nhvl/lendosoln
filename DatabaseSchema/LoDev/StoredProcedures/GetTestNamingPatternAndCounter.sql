
-- =============================================
-- Author:		Jhairo Erazo
-- Create date: 7/31/2015
-- Description:	Generate Alternate Loan Number
-- =============================================
CREATE PROCEDURE [dbo].[GetTestNamingPatternAndCounter] 
	@BrokerId uniqueidentifier,
	@BranchId uniqueidentifier,
	@UpdateCounters bit = 1,
	@NamingCounter BigInt OUT,
	@BranchLNmPrefix varchar(10) = NULL  OUT
AS
-- dd 9/29/05 - Get the counter from broker table and increase counter. We will apply the naming pattern to actual name in C# code.
DECLARE @CurrentDate SmallDateTime
DECLARE @CounterDate SmallDateTime
SET @CurrentDate = GETDATE()
SELECT @NamingCounter = TestNamingCounter , @CounterDate = TestNamingCounterDate FROM Broker WHERE BrokerId = @BrokerId
SELECT @BranchLNmPrefix = BranchLNmPrefix FROM Branch WHERE BrokerId = @BrokerId AND BranchId = @BranchId
-- Check if counter needs to reset by month or increment counter by one.
IF MONTH(@CounterDate) <> MONTH(@CurrentDate) OR YEAR(@CounterDate) <> YEAR(@CurrentDate)
BEGIN
	SET @NamingCounter = 0
END
ELSE
BEGIN
	SET @NamingCounter = @NamingCounter + 1
END
-- Update Counter to broker table
IF @UpdateCounters = 1
BEGIN
	UPDATE Broker WITH ( ROWLOCK ) SET TestNamingCounter  = @NamingCounter, TestNamingCounterDate = GETDATE() WHERE BrokerId = @BrokerId
END

