-- =============================================
-- Author: GF
-- Create date: 9/11/12
-- Description:	Retrieves company id by company name and broker id
-- =============================================
CREATE PROCEDURE [dbo].[RetrieveComIdByBrokerIdComNm]
	@BrokerId uniqueidentifier,
	@AgentComNm varchar(25)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT CompanyId
	FROM   Agent
	WHERE BrokerId = @BrokerId AND AgentComNm = @AgentComNm
END