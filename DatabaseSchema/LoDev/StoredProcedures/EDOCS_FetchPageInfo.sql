-- =============================================
-- Author:		Antonio Valencia
-- Create date: 10/8/2012
-- Description:	Loads Page Information XML
-- =============================================
CREATE PROCEDURE [dbo].[EDOCS_FetchPageInfo] 
	-- Add the parameters for the stored procedure here
	@DocumentId uniqueidentifier, 
	@BrokerId uniqueidentifier,
	@sLId uniqueidentifier
AS
BEGIN
	select pdfpageinfo from edocs_document 
	where documentid = @documentid and brokerid = @brokerid and slid = @slid 
END
