-- =============================================
-- Author:		David Dao
-- Create date: Aug 9, 2013
-- Description:	
-- =============================================
ALTER PROCEDURE [dbo].[IRS_4506T_VENDOR_EMPLOYEE_INFO_Save] 
    @UserId UniqueIdentifier,
    @VendorId UniqueIdentifier,
    @AccountId varchar(50),
    @UserName varchar(50),
    @Password varbinary(max) = null,
    @EncryptionKeyId uniqueidentifier
AS
BEGIN

	-- If userName is empty then delete the record.
	IF @UserName = ''
	BEGIN
		DELETE IRS_4506T_VENDOR_EMPLOYEE_INFO
		WHERE UserId=@UserId AND VendorId=@VendorId
		
		RETURN
	END
	
    UPDATE IRS_4506T_VENDOR_EMPLOYEE_INFO
      SET AccountId = @AccountId,
          UserName = @UserName,
          Password = COALESCE(@Password, Password),
          EncryptionKeyId = @EncryptionKeyId
    WHERE UserId=@UserId AND VendorId = @VendorId
    
	IF @@rowcount = 0
	BEGIN
		INSERT INTO IRS_4506T_VENDOR_EMPLOYEE_INFO
		(UserId, VendorId, AccountId, Username, Password, EncryptionKeyId)
		VALUES
		(@UserId, @VendorId, @AccountId, @Username, COALESCE(@Password, 0x00000000), @EncryptionKeyId)
	END    
	
END
