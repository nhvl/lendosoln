ALTER Procedure [dbo].[TASK_TrackTaskCreationFromTemplate]
	@brokerId uniqueidentifier,
	@loanId uniqueidentifier,
	@taskId varchar(10),
	@templateId int,
	@appId uniqueidentifier,
	@recordId uniqueidentifier,
	@IsBlocked bit = 0,
	@IsResolved bit = 0,
	@IsClosed bit = 0

AS

INSERT INTO TASK_x_TASK_TRIGGER_TEMPLATE VALUES
(@brokerId, @loanId, @taskId, @templateID, @appId, 
COALESCE(@recordId, '00000000-0000-0000-0000-000000000000')
)


GO


