-- =============================================
-- Author:		David Dao
-- Create date: 4/26/2015
-- Description:	Find active user id with the given ByPassKey
-- =============================================
ALTER PROCEDURE [dbo].[ALL_USER_GetUserIdByByPassTicket]
    @ByPassTicket UniqueIdentifier
AS
BEGIN
    SELECT TOP(1) 
		au.UserId 
    FROM 
		ALL_USER AS au JOIN
		Broker_User AS bu ON bu.UserId=au.UserId JOIN
		Employee AS e ON e.EmployeeId=bu.EmployeeId JOIN
		Branch AS br ON br.BranchId=e.BranchId JOIN
		Broker AS b ON b.BrokerId=br.BrokerId
    WHERE 
		au.ByPassTicket=@ByPassTicket AND 
		au.IsActive=1 AND
		b.Status=1
END
