-- =============================================
-- Author:		Eric Mallare
-- Create date: 9/12/2016
-- Description:	Revokes the broker client certificate
-- =============================================
ALTER PROCEDURE [dbo].[RestoreBrokerClientCertificate] 
	@BrokerId uniqueidentifier,
	@CertificateId uniqueidentifier
AS
BEGIN
	UPDATE 
		BROKER_CLIENT_CERTIFICATE
	SET 
		IsRevoked='0'
	WHERE
		BrokerId=@BrokerId AND
		CertificateId=@CertificateId
END
