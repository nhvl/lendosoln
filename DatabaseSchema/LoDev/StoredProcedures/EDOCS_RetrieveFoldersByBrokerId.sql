CREATE PROCEDURE [dbo].[EDOCS_RetrieveFoldersByBrokerId]
@BrokerId uniqueidentifier
as
	select * from edocs_folder f
	join EDOCS_FOLDER_X_ROLE r
	on f.folderid = r.folderid
	and f.brokerid = @brokerid