



CREATE   PROCEDURE [dbo].[RetrieveDisabledPmlUserByLoginNm]
	@LoginName varchar(36),
	@BrokerPmlSiteID uniqueidentifier
AS
SELECT '' -- For now, we just need to know if this user is disabled
FROM VIEW_DISABLED_PML_USER_WITH_BROKER_INFO
WHERE LoginNm = @LoginName and BrokerPmlSiteId = @BrokerPmlSiteID

