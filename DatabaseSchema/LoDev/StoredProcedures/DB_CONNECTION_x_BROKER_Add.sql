-- =============================================
-- Author:		David Dao
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[DB_CONNECTION_x_BROKER_Add]
    @DbConnectionId UniqueIdentifier,
    @BrokerId UniqueIdentifier,
    @CustomerCode varchar(36) 

AS
BEGIN

    INSERT INTO DB_CONNECTION_x_BROKER(DbConnectionId, BrokerId, CustomerCode)
                                VALUES(@DbConnectionId, @BrokerId, @CustomerCode)

END
