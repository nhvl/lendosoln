CREATE PROCEDURE EmployeeRemoveRole 
	@RoleID Guid,
             @EmployeeID Guid,
	@BrokerID Guid
AS
DELETE FROM Role_Assignment 
WHERE RoleID = @RoleID 
      AND EmployeeID = @EmployeeID
if(0!=@@error)
begin
	RAISERROR('Error in deleting from Role_Assignment table in EmployeeRemoveRole sp', 16, 1);
	return -100;
end
return 0
