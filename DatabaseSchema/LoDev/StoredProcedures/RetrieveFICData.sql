
CREATE proc [dbo].[RetrieveFICData]
      @CustomerCode varchar(50)
as
begin

DECLARE @BrokerID uniqueidentifier;
SELECT @BrokerID=brokerid FROM Broker WHERE CustomerCode=@CustomerCode

DECLARE @ReportStartDate smalldatetime;
SELECT @ReportStartDate = DATEADD("yyyy", -1, GETDATE()) -- go back 1 year only

select sLNm as Filename,
      aBLastNm as Borrower,
      sFinalLAmt as 'Loan Amt',
      sNoteIR as 'Int Rate',
      sTerm as Term, 
      sLpTemplateNm as Program, 
      sRLckdD as 'Lock Date', 
      sLPurchaseD as Purchased, -- sold.
      sRLckdExpiredD as Expires, 
      sAgentLoanOfficerName as 'Loan Rep', 
      sDocsBackD as 'Docs Received', 
      dbo.GetLoanStatusName(sStatusT) as Status, 
      sRecordedD as Recorded, 
      sRLckdDays as Days, 
      sInvestorLockRLckExpiredD as 'Investor Expiration',
      case (sLT)
            when '0' then 'Conventional' 
            when '1' then 'FHA' 
            when '2' then 'VA' 
            when '3' then 'UsdaRural' 
            when '4' then 'Other' 
      end as TYPE,
      case (sFinMethT)
            when '0' then 'Fixed'
            when '1' then 'ARM'
            when '2' then 'Graduated'
      end as AMORT, 
      case( sLPurposeT ) 
            when '3' then 'Construction' 
            when '4' then 'Construction Permanent' 
            when '5' then 'Other' 
            when '0' then 'Purchase' 
            when '1' then 'Refinance' 
            when '2' then 'Refinance Cash-out' 
            when '6' then 'FHA Streamlined Refinance' 
      end as Purpose,
      case (sLienPosT)
            when '0' then 'First'
            when '1' then 'Second'
      end as 'FIRST & Sec', 
      case( aOccT ) 
            when '2' then 'Investment' 
            when '0' then 'Primary Residence' 
            when '1' then 'Secondary Residence' 
      end as Occupancy,
      sSpState as State, 
      sSpCounty as County, 
      sLtvR as LTV, 
      sCltvR as CLTV, 
      sFHAPurposeIsHudReo as 'HUD RE', 
      sFHAPurposeIs203k as '203K', 
      case (sProdImpound)
            when '0' then 'Waived'
            when '1' then 'Not Waived'
      end as 'ESC WA', 
      sInvestorLockBrokComp1PcPrice as 'Inv Price', 
      -- sInvestorLockTotNonSRPAdjBrokComp1PcPrice as Fee, 
      sPurchaseAdviceTotalPrice_Field1 as FinalSalePrice, 
      -- sPurchaseAdviceSRP_Field3 as FinalSaleSRP, 
      sCanceledD as DownCancelDate, 
      DATEADD("mm", -1, sMonthlyPaymentFirstAdjStartD) as ArmChngDate,
      CASE
            WHEN (sPurchaseAdviceSummaryInvNm IS NULL OR sPurchaseAdviceSummaryInvNm = '') THEN sInvestorLockLpInvestorNm
            ELSE sPurchaseAdviceSummaryInvNm
      END AS DesignInvest,
      dbo.GetLoanStatusName(sStatusT) as ClientStatus,
      aBExperianScore as 'Fico B',
      aCExperianScore as 'Fico C',
      aBTransUnionScore as 'TU B',
      aCTransUnionScore as 'TU C',
      aBEquifaxScore as 'Eq B',
      aCEquifaxScore as 'Eq C',
      sBrokerLockInvestorPrice as 'Investor Price', 
sBrokerLockTotalLenderAdj - sBrokerLockTotHiddenAdjBrokComp1PcFee - sBrokerLockTotVisibleAdjBrokComp1PcFee as 'Ln ADJ',
      sRLckdD as Locked,
      sUnderwritingD as 'U/W Received',
      sApprovD as 'U/W Approved',
      sDocsD as 'Docs Drawn',
      sDocsBackD as 'Docs Back',
      sFundD as Funded,
      sRLckdExpiredD as Expired,
      sCanceledD as Cancelled,
      --sBrokComp1Pc as LoanLevelAdjustments,
      sInvestorLockRateSheetEffectiveTime AS 'Ratesheet Effective Time',
      case (sInvestorLockCommitmentT)
            when '0' then 'Best Efforts'
            when '1' then 'Mandatory'
		when 2 then 'Hedged'
		when 3 then 'Securitized'			
      end as 'Commitment Type',
      sInvestorLockProjectedProfit AS 'Gross Projected Profit',
      sBrokerLockFinalBrokComp1PcPrice AS 'Front End Final Price',
      sWarehouseFunderDesc as 'Warehouse Lender'
      
	FROM LOAN_FILE_CACHE lc WITH(NOLOCK) JOIN LOAN_FILE_CACHE_2 lc2 WITH(NOLOCK) ON lc.sLId = lc2.sLId

where 
      ( sBrokerId = @BrokerID ) and 
      ( IsValid = 1 ) and 
      ( IsTemplate = 0 ) and 
      -- ( sIsRateLocked  = 1 ) and  -- Send all loans per request from Binh and John @ Flatirons Capital. dd 4/17/2013
      ( 
            sPreApprovD >= @ReportStartDate or
            sUnderwritingD >= @ReportStartDate or
            sOpenedD >= @ReportStartDate or
            sPreQualD >= @ReportStartDate or
            sDocsD >= @ReportStartDate or
            sSubmitD >= @ReportStartDate or
            sApprovD >= @ReportStartDate or
            sFundD >= @ReportStartDate or
            sShippedToInvestorD >= @ReportStartDate or
            sRecordedD >= @ReportStartDate or
            sClosedD >= @ReportStartDate or
            sOnHoldD >= @ReportStartDate or
            sSuspendedD >= @ReportStartDate or
            sCanceledD >= @ReportStartDate or
            sRejectD >= @ReportStartDate
      )
order by  
      sLpTemplateNm asc , 
      case( sStatusT) 
            when '15' then 0 
            when '16' then 1 
            when '12' then 2 
            when '17' then 3 
            when '4' then 4 
            when '9' then 5 
            when '11' then 6 
            when '5' then 7 
            when '6' then 8 
            when '7' then 9 
            when '0' then 10 
            when '18' then 11 
            when '2' then 12 
            when '1' then 13 
            when '19' then 14 
            when '10' then 15 
            when '3' then 16 
            when '8' then 17 
            when '13' then 18 
            when '20' then 19 
            when '21' then 20 
            else 21 
      end asc
end
