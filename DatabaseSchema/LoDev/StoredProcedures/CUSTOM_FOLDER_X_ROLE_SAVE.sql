ALTER PROCEDURE [dbo].[CUSTOM_FOLDER_X_ROLE_SAVE]
	@CustomFolderXRole CustomFolderXRoleTableType READONLY,
	@FolderIds IntTableType READONLY
AS
BEGIN
	WITH 
		CUSTOM_FOLDER_X_ROLE_PARTIAL AS
		(
			SELECT p.FolderId, p.RoleId
			FROM CUSTOM_FOLDER_X_ROLE as p
			JOIN @FolderIds as c ON p.FolderId = c.IntValue
		)
	MERGE CUSTOM_FOLDER_X_ROLE_PARTIAL AS TARGET
	USING @CustomFolderXRole AS SOURCE
	ON (TARGET.FolderId = SOURCE.FolderId)
		WHEN NOT MATCHED BY TARGET THEN 
			INSERT
			(
				FolderId,
				RoleId
			)
			VALUES
			(
				SOURCE.FolderId,
				SOURCE.RoleId
			)		

		WHEN NOT MATCHED BY SOURCE THEN
		DELETE;
END
