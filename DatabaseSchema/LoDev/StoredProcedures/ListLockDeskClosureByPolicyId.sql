-- =============================================
-- Author:		paoloa
-- Create date: 4/18/2013
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[ListLockDeskClosureByPolicyId]
	-- Add the parameters for the stored procedure here
	@PolicyId uniqueidentifier,
	@BrokerId uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT ClosureDate, IsClosedAllDay, IsClosedForBusiness, LockDeskOpenTime, LockDeskCloseTime
	FROM LENDER_LOCK_POLICY_HOLIDAY
	WHERE LockPolicyId = @PolicyId and BrokerId = @BrokerId
	ORDER BY ClosureDate ASC
END
