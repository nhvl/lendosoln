







CREATE  PROCEDURE [dbo].[CP_CreateActionResponse]
@ConsumerActionRequestId bigint
--, @ArchivedEDocId uniqueidentifier = NULL
, @ConsumerCompletedD datetime = NULL
--, @RequestorAcceptedD datetime = NULL
, @ReceivingFaxNumber varchar(14) = NULL
, @BorrowerSignedEventId uniqueidentifier = NULL
, @CoborrowerSignedEventId uniqueidentifier = NULL

AS
INSERT INTO CP_CONSUMER_ACTION_RESPONSE
(
ConsumerActionRequestId
, ConsumerCompletedD
, ReceivingFaxNumber
, BorrowerSignedEventId
, CoborrowerSignedEventId
)
           
VALUES
(
@ConsumerActionRequestId 
, @ConsumerCompletedD
, COALESCE(@ReceivingFaxNumber, '')
, @BorrowerSignedEventId
, @CoborrowerSignedEventId
)


IF ( 0 != @@ERROR )
BEGIN
	RAISERROR('Error in inserting into CP_CONSUMER_ACTION_RESPONSE table in CP_CreateActionResponse sp', 16, 1);
	RETURN -100;
END
RETURN 0;








