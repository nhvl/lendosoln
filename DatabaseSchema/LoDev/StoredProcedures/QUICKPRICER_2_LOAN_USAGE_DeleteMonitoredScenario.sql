-- =============================================
-- Author:		Scott Kibler
-- Create date: 9/16/2014
-- Description:	
--	 Deletes associated rate monitors.
--   Nullifies rate monitor name.
--   Fails if there's no entry in the quickpricer_2_loan_usage table.
-- =============================================
CREATE PROCEDURE dbo.QUICKPRICER_2_LOAN_USAGE_DeleteMonitoredScenario
	@BrokerID UniqueIdentifier,
	@LoanID UniqueIdentifier,
	@UserID UniqueIdentifier-- // TODO: possibly log the user who deletes.
	AS
BEGIN
	DECLARE @RowCount int   -- to come from  @@RowCount
	DECLARE @ErrorCode int
	DECLARE @ErrorMsg varchar(1000)
	SET @ErrorCode = -1;
	SET @ErrorMsg = 'Unexpected Error';
	
	SELECT 
		@RowCount = COUNT(*)
	FROM
		QUICKPRICER_2_LOAN_USAGE
	WHERE
		    LoanID = @LoanID
		AND BrokerID = @BrokerID
		AND UserIDOfUsingUser = @UserID
		AND IsInUse = 1 -- there should be no rate monitors on non-used loans.
	IF @@Error <> 0 
	BEGIN
		SET @ErrorMsg = 'Errored finding loan.'
		GOTO NonTranErrorHandler;
	END	
	IF @RowCount = 0
	BEGIN
		SET @ErrorMsg = 'Failed finding loan.'
		GOTO NonTranErrorHandler;
	END
	
	BEGIN TRANSACTION -- Deletion Transaction
		DELETE 
		FROM 
			RATE_MONITOR
		WHERE 
			sLId = @LoanID
			AND CreatedByUserId = @UserID
		IF(0 <> @@Error)
		BEGIN
			SET @ErrorCode = -53
			SET @ErrorMsg = 'Unable to delete from RATE_MONITOR'
			GOTO ErrorHandler;
		END
		
		UPDATE
			QUICKPRICER_2_LOAN_USAGE
		SET
			RateMonitorName = NULL
		WHERE
				LoanID = @LoanID
			AND BrokerID = @BrokerID
			AND UserIDofUsingUser = @UserID
		IF(0 <> @@Error) -- OR 0 = @RowCount)
		BEGIN
			SET @ErrorCode = -55
			SET @ErrorMsg = 'Unable to nullify ratemonitorname'
			GOTO ErrorHandler;
		END
		
	
	COMMIT TRANSACTION -- DeletionTransaction
	RETURN 0;
	ErrorHandler:
		ROLLBACK TRANSACTION -- DeletionTransaction
		print 'transaction rolled back.'
		RAISERROR(@ErrorMsg, 16, 1)
		RETURN @ErrorCode;
	
	
	RAISERROR('Should not have reached this code.', 16, 1)
	print 'unobtainable code obtained.'
	RETURN -377;
	NonTranErrorHandler:
		print 'NonTranErrorHandler'
		RAISERROR(@ErrorMsg, 16, 1)
		RETURN @ErrorCode;
END
