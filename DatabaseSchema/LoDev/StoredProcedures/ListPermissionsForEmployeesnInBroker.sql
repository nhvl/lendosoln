
-- =============================================
-- Author:		Antonio Valencia
-- Create date: 3/22/2012
-- Description:	Retrieves all the permissions for the broker
-- =============================================
CREATE PROCEDURE dbo.[ListPermissionsForEmployeesnInBroker] 
	-- Add the parameters for the stored procedure here
	@BrokerId uniqueidentifier 
AS
BEGIN

	SET NOCOUNT ON;


	SELECT EmployeeId, Permissions From View_Active_Lo_User
	Where BrokerId = @BrokerId 
	
END

