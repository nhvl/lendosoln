ALTER PROCEDURE [dbo].[RemoveLoginSessionsByDate]
	@DateCutoff datetime
AS
	DELETE FROM Login_Session
	WHERE LogoutDate < @DateCutoff
