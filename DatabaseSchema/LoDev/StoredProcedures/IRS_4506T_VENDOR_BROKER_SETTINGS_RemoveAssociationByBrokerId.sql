-- =============================================
-- Author:		David Dao
-- Create date: Aug 6, 2013
-- Description:	
-- =============================================
CREATE PROCEDURE dbo.IRS_4506T_VENDOR_BROKER_SETTINGS_RemoveAssociationByBrokerId
	@BrokerId UniqueIdentifier 

AS
BEGIN

DELETE FROM IRS_4506T_VENDOR_BROKER_SETTINGS
WHERE BrokerId = @BrokerId

END
