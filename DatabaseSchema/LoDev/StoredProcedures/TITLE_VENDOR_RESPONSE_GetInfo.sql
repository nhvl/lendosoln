-- =============================================
-- Author:		Antonio Valencia
-- Create date: 5/23/2013
-- Description:	Gets Quote log along with the provider info in order to 
-- =============================================
CREATE PROCEDURE [dbo].[TITLE_VENDOR_RESPONSE_GetInfo]
	-- Add the parameters for the stored procedure here
	@BrokerId uniqueidentifier, 
	@sLId uniqueidentifier
AS
BEGIN
	SELECT r.TitleXml, r.TitleVendorId, r.BrokerId,
			[Login], [ClientCode] as AccountId, [Password],  LoginTitle, ClientCodeTitle as AccountIdTitle, PasswordTitle
	FROM TITLE_VENDOR_RESPONSE r JOIN TITLE_VENDOR_X_BROKER b ON r.TitleVendorId = b.TitleVendorId AND b.BrokerId = @BrokerId
	WHERE r.BrokerId = @BrokerId AND sLId = @sLId

	
	
END