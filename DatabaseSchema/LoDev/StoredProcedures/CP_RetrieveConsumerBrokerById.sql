


-- =============================================
-- Author:		<Francesc>
-- Create date: <04/29/10>
-- Description:	<Retrieve loans associated with a given consumerId>
-- =============================================
CREATE PROCEDURE [dbo].[CP_RetrieveConsumerBrokerById]
	@ConsumerId bigint
	
AS
BEGIN
	SET NOCOUNT ON;

	SELECT ConsumerId, Email, cp.BrokerId, PasswordHash, LastLoggedInD, PasswordRequestD, PasswordResetCode, IsTemporaryPassword, LoginFailureNumber, IsLocked, BrokerNm
	FROM CP_CONSUMER cp WITH (NOLOCK)
	JOIN BROKER b WITH (NOLOCK)
		ON b.brokerid = cp.brokerid
	WHERE ConsumerId = @ConsumerId

END



