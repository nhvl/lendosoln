-- =============================================
-- Author:		Geoff Feltman
-- Create date: 12/16/15
-- Description:	Loads all of the user registered ips for a given broker id.
-- =============================================
ALTER PROCEDURE dbo.ALL_USER_REGISTERED_IP_ListByBrokerId
	@BrokerId uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT Id, BrokerId, UserId, IPAddress, Description, CreatedDate, IsRegistered
	FROM ALL_USER_REGISTERED_IP
	WHERE BrokerId = @BrokerId
END
