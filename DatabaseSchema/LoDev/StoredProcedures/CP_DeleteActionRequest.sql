






CREATE PROCEDURE [dbo].[CP_DeleteActionRequest]
	@ConsumerActionRequestId bigint
	,@ShouldDeleteFromFileDB bit OUT
AS

BEGIN TRANSACTION

-- Basically, if this pdf is used by any signing event,
-- Consider it part of the vault and do not allow deletion
IF EXISTS ( SELECT * FROM CP_Consumer_Action_Request req
			join CP_Signed_Event se 
			on req.PdfFileDbKey = se.PdfFileDbKey
			WHERE req.ConsumerActionRequestId = @ConsumerActionRequestId
			)
	SET @ShouldDeleteFromFileDB = 0
ELSE
	SET @ShouldDeleteFromFileDB = 1

DELETE FROM CP_Consumer_Action_Response
WHERE ConsumerActionRequestId = @ConsumerActionRequestId

DELETE FROM CP_Consumer_Action_Request
WHERE ConsumerActionRequestId = @ConsumerActionRequestId

COMMIT TRANSACTION






