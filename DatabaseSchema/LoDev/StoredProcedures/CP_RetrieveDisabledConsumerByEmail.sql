
CREATE PROCEDURE [dbo].[CP_RetrieveDisabledConsumerByEmail]
	@Email varchar(80),
	@BrokerId uniqueidentifier
AS

SELECT ConsumerId
FROM CP_CONSUMER
WHERE	Email = @Email AND
		BrokerId = @BrokerId AND
		IsLocked = 1

