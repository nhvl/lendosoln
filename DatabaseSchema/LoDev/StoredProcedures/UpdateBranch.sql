ALTER PROCEDURE [dbo].[UpdateBranch]
	@BranchID uniqueidentifier,
	@BrokerID uniqueidentifier,
	@Name varchar(100),
	@Address varchar(60),
	@City varchar(36),
	@State	 char(2),
	@Zipcode char(5),
	@Phone varchar(21),
	@Fax varchar(21),
	@BranchLpePriceGroupIdDefault uniqueidentifier,
	@DisplayBranchNameInLoanNotificationEmails bit,
	@BranchCode varchar(36),
	@IsAllowLOEditLoanUntilUnderwriting bit = 0,
	@IsUseBranchInfoForLoans bit = 0,
	@NmlsIdentifier varchar(50) = '',
	@LicenseXmlContent text = '',
	@DisplayNm varchar(100)= '',
	@DisplayNmLckd bit = 0,
	@IsBranchTPO bit = 0,
	@BranchChannelT int = 0,
	@DocMagicUserName varchar(50) = null,
	@DocMagicPassword varchar(150) = null,
	@DocMagicCustomerId varchar(50) = null,
	@DisclosureDocUserName varchar(50) = null,
	@DisclosureDocPassword varchar(150) = null,
	@DisclosureDocCustomerId varchar(50) = null,
	@ConsumerPortalId uniqueidentifier = null, 
	@FhaLenderId varchar(10) = null,
	@IsFhaLenderIdModified bit = null,
	@Division varchar(100) = null,
	@PopulateFhaAddendumLines bit = null,
	@CustomPricingPolicyField1 varchar(25) = null,
    @CustomPricingPolicyField2 varchar(25) = null,
    @CustomPricingPolicyField3 varchar(25) = null,
    @CustomPricingPolicyField4 varchar(25) = null,
    @CustomPricingPolicyField5 varchar(25) = null,
    @RetailTpoLandingPageId uniqueidentifier = null,
    @LegalEntityIdentifier varchar(20) = null,
	@IsLegalEntityIdentifierModified bit = null,
	@Status int = null
AS
	IF @BranchLpePriceGroupIdDefault IS NOT NULL AND @BranchLpePriceGroupIdDefault = '00000000-0000-0000-0000-000000000000'
		SET @BranchLpePriceGroupIdDefault = NULL
	UPDATE BRANCH WITH(ROWLOCK, READPAST, UPDLOCK)
	SET
		BranchNm = @Name,
		BranchAddr = @Address,
		BranchCity = @City,
		BranchState = @State,
		BranchZip = @Zipcode,
		BranchPhone = @Phone,
		BranchFax = @Fax,
		BranchLpePriceGroupIdDefault = @BranchLpePriceGroupIdDefault,
		DisplayBranchNameInLoanNotificationEmails = @DisplayBranchNameInLoanNotificationEmails,
		BranchCode = @BranchCode,
		IsAllowLOEditLoanUntilUnderwriting = @IsAllowLOEditLoanUntilUnderwriting,
		UseBranchInfoForLoans  = @IsUseBranchInfoForLoans,
		NmlsIdentifier = @NmlsIdentifier, 
		LicenseXmlContent = @LicenseXmlContent,
		DisplayNm = @DisplayNm,
		DisplayNmLckd = @DisplayNmLckd,
		IsBranchTPO = @IsBranchTPO,
		BranchChannelT = @BranchChannelT,
		DocMagicUserName = COALESCE(@DocMagicUserName, DocMagicUserName),
		DocMagicPassword = COALESCE(@DocMagicPassword, DocMagicPassword),
		DocMagicCustomerId = COALESCE(@DocMagicCustomerId, DocMagicCustomerId),
		DisclosureDocUserName = COALESCE(@DisclosureDocUserName, DisclosureDocUserName),
		DisclosureDocPassword = COALESCE(@DisclosureDocPassword, DisclosureDocPassword),
		DisclosureDocCustomerId = COALESCE(@DisclosureDocCustomerId, DisclosureDocCustomerId),
		ConsumerPortalId = COALESCE(@ConsumerPortalId, ConsumerPortalid),
		FhaLenderId = COALESCE(@FhaLenderId, FhaLenderId),
		IsFhaLenderIdModified = COALESCE(@IsFhaLenderIdModified, IsFhaLenderIdModified),
		Division = COALESCE(@Division, Division),
		PopulateFhaAddendumLines = COALESCE(@PopulateFhaAddendumLines, PopulateFhaAddendumLines),
		CustomPricingPolicyField1 = COALESCE(@CustomPricingPolicyField1, CustomPricingPolicyField1),
        CustomPricingPolicyField2 = COALESCE(@CustomPricingPolicyField2, CustomPricingPolicyField2),
        CustomPricingPolicyField3 = COALESCE(@CustomPricingPolicyField3, CustomPricingPolicyField3),
        CustomPricingPolicyField4 = COALESCE(@CustomPricingPolicyField4, CustomPricingPolicyField4),
        CustomPricingPolicyField5 = COALESCE(@CustomPricingPolicyField5, CustomPricingPolicyField5),
        RetailTpoLandingPageId = COALESCE(@RetailTpoLandingPageId, RetailTpoLandingPageId),
        LegalEntityIdentifier = COALESCE(@LegalEntityIdentifier, LegalEntityIdentifier),
        IsLegalEntityIdentifierModified = COALESCE(@IsLegalEntityIdentifierModified, IsLegalEntityIdentifierModified),
        [Status] = COALESCE(@Status, [Status])

	WHERE
		BranchId = @BranchID
		AND
		BrokerID = @BrokerID
	
	
	if( 0!=@@error OR @@rowcount = 0)
	begin
		RAISERROR('Error in updating Branch table in UpdateBranch sp', 16, 1);
		return -100;
	end
	return 0;
