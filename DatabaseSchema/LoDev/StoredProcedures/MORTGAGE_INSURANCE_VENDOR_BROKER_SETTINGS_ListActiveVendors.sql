-- =============================================
-- Author:		Isaac Ribakoff
-- Create date: 9/18/2014
-- Description:	Lists MI Vendors That Are Active In Broker Account
-- =============================================
ALTER PROCEDURE [dbo].[MORTGAGE_INSURANCE_VENDOR_BROKER_SETTINGS_ListActiveVendors]
	@BrokerId UniqueIdentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SELECT b.VendorId, b.IsProduction, --For LOAdmin
  b.UserName, b.Password, b.EncryptionKeyId, b.AccountId, b.PolicyId, b.IsDelegated --For General Settings
FROM MORTGAGE_INSURANCE_VENDOR_BROKER_SETTINGS b
WHERE b.BrokerId = @BrokerId
  
END
