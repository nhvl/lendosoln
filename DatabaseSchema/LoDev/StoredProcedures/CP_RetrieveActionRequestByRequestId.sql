CREATE PROCEDURE [dbo].[CP_RetrieveActionRequestByRequestId]
	@ConsumerActionRequestId bigint
AS

SELECT 
sLId
, aAppId
, aBFullName
, aCFullName
, ResponseStatus
, DocTypeId
, Description
, PdfFileDbKey
, PdfMetaDataSnapshot
, IsApplicableToBorrower
, IsApplicableToCoborrower
, IsSignatureRequested
, IsESignAllowed
, CreateOnD
, sTitleBorrowerId 

FROM
CP_Consumer_Action_Request

WHERE ConsumerActionRequestId = @ConsumerActionRequestId
