CREATE PROCEDURE [dbo].[AUTHORIZED_EMAIL_DOMAIN_Delete]
	@BrokerId uniqueidentifier,
	@DomainName varchar(50)
AS
BEGIN
	DELETE FROM AUTHORIZED_EMAIL_DOMAIN
	WHERE BrokerId = @BrokerId
		AND DomainName = @DomainName
END