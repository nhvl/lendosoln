-- =============================================
-- Author:		David Dao
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[UpdatePmlBroker] 
	@PmlBrokerId UniqueIdentifier
	, @BrokerId UniqueIdentifier
	, @IsActive bit
	, @Name varchar(100)
	, @Addr varchar(60)
	, @City varchar(36)
	, @State char(2)
	, @Zip char(5)
	, @Phone varchar(21)
	, @Fax varchar(21)
	, @LicenseXmlContent text
	, @CompanyId varchar(36)
	, @TaxId varchar(36) = ''
	, @PrincipalName1 varchar(100)
	, @PrincipalName2 varchar(100)
	, @NmLsIdentifier varchar(50) = ''
	, @OriginatorCompensationAuditXml varchar(max)
	, @OriginatorCompensationPercent decimal(9, 3)
	, @OriginatorCompensationBaseT int
	, @OriginatorCompensationMinAmount money
	, @OriginatorCompensationMaxAmount money
	, @OriginatorCompensationFixedAmount money
	, @OriginatorCompensationNotes varchar(200)
	, @OriginatorCompensationLastModifiedDate datetime = null
	, @CustomPricingPolicyField1 varchar(25)
	, @CustomPricingPolicyField2 varchar(25)
	, @CustomPricingPolicyField3 varchar(25)
	, @CustomPricingPolicyField4 varchar(25)
	, @CustomPricingPolicyField5 varchar(25)
	, @Lender_TPO_LandingPageConfigId uniqueidentifier = null
	, @NmlsName varchar(100)
	, @NmlsNameLckd bit
	, @LpePriceGroupId UniqueIdentifier = null
	, @BranchId UniqueIdentifier = null
	, @PmlCompanyTierId int = 0
	, @CorrespondentTPOPortalConfigID  UniqueIdentifier = null
	, @CorrespondentLpePriceGroupId  UniqueIdentifier = null
	, @CorrespondentBranchId  UniqueIdentifier = null
	, @MiniCorrespondentTPOPortalConfigID  UniqueIdentifier = null
	, @MiniCorrespondentLpePriceGroupId  UniqueIdentifier = null
	, @MiniCorrespondentBranchId  UniqueIdentifier = null
	, @Roles  varchar(50) = ''
	, @UnderwritingAuthority  int = 0
	, @PMLNavigationPermissions varchar(max) = ''
	, @BrokerRoleStatusT int = 0
	, @MiniCorrRoleStatusT int = 0
	, @CorrRoleStatusT int = 0
	, @UseDefaultWholesalePriceGroupId bit = null
	, @UseDefaultWholesaleBranchId bit = null
	, @UseDefaultMiniCorrPriceGroupId bit = null
	, @UseDefaultMiniCorrBranchId bit = null
	, @UseDefaultCorrPriceGroupId bit = null
	, @UseDefaultCorrBranchId bit = null
	, @OriginatorCompensationIsOnlyPaidForFirstLienOfCombo bit = null
	, @TpoColorThemeId uniqueidentifier = null
	, @TaxIdEncrypted varbinary(max) = null
AS
BEGIN
	DECLARE @UseBranchLpePriceGroupId  bit
	IF @LpePriceGroupId IS NOT NULL AND @LpePriceGroupId = '00000000-0000-0000-0000-000000000000'
	BEGIN
		SET @UseBranchLpePriceGroupId = 1
		SET @LpePriceGroupId = NULL
	END
	ELSE
	BEGIN
		SET @UseBranchLpePriceGroupId = 0
	END
	
	DECLARE @UseCorrespondentBranchLpePriceGroupId  bit
	IF @CorrespondentLpePriceGroupId IS NOT NULL AND @CorrespondentLpePriceGroupId = '00000000-0000-0000-0000-000000000000'
	BEGIN
		SET @UseCorrespondentBranchLpePriceGroupId = 1
		SET @CorrespondentLpePriceGroupId = NULL
	END
	ELSE
	BEGIN
		SET @UseCorrespondentBranchLpePriceGroupId = 0
	END
	
	DECLARE @UseMiniCorrespondentBranchLpePriceGroupId  bit
	IF @MiniCorrespondentLpePriceGroupId IS NOT NULL AND @MiniCorrespondentLpePriceGroupId = '00000000-0000-0000-0000-000000000000'
	BEGIN
		SET @UseMiniCorrespondentBranchLpePriceGroupId = 1
		SET @MiniCorrespondentLpePriceGroupId = NULL
	END
	ELSE
	BEGIN
		SET @UseMiniCorrespondentBranchLpePriceGroupId = 0
	END
	
	UPDATE Pml_Broker
	   SET IsActive = @IsActive
		   , Name = @Name
			, Addr = @Addr
			, City = @City
			, State = @State
			, Zip = @Zip
			, Phone = @Phone
			, Fax = @Fax
			, LicenseXmlContent = @LicenseXmlContent
			, CompanyId = @CompanyId
			, PrincipalName1 = @PrincipalName1
			, PrincipalName2 = @PrincipalName2
			, NmLsIdentifier = @NmLsIdentifier
			, OriginatorCompensationAuditXml = @OriginatorCompensationAuditXml
			, OriginatorCompensationPercent = @OriginatorCompensationPercent
			, OriginatorCompensationBaseT = @OriginatorCompensationBaseT
	 		, OriginatorCompensationMinAmount = @OriginatorCompensationMinAmount
	 		, OriginatorCompensationMaxAmount = @OriginatorCompensationMaxAmount
			, OriginatorCompensationFixedAmount = @OriginatorCompensationFixedAmount
			, OriginatorCompensationNotes = @OriginatorCompensationNotes
			, OriginatorCompensationLastModifiedDate = @OriginatorCompensationLastModifiedDate
			, CustomPricingPolicyField1 = @CustomPricingPolicyField1
			, CustomPricingPolicyField2 = @CustomPricingPolicyField2
			, CustomPricingPolicyField3 = @CustomPricingPolicyField3
			, CustomPricingPolicyField4 = @CustomPricingPolicyField4
			, CustomPricingPolicyField5 = @CustomPricingPolicyField5
			, Lender_TPO_LandingPageConfigId = @Lender_TPO_LandingPageConfigId
			, NmlsNameLckd = @NmlsNameLckd
			, NmlsName = @NmlsName
			, UseBranchLpePriceGroupId = @UseBranchLpePriceGroupId
			, LpePriceGroupId = @LpePriceGroupId
			, BranchId = @BranchId
			, PmlCompanyTierId = @PmlCompanyTierId
			, CorrespondentTPOPortalConfigID  = @CorrespondentTPOPortalConfigID
			, CorrespondentLpePriceGroupId  = @CorrespondentLpePriceGroupId
			, CorrespondentBranchId  = @CorrespondentBranchId
			, MiniCorrespondentTPOPortalConfigID  = @MiniCorrespondentTPOPortalConfigID
			, MiniCorrespondentLpePriceGroupId  = @MiniCorrespondentLpePriceGroupId
			, MiniCorrespondentBranchId  = @MiniCorrespondentBranchId
			, Roles  = @Roles
			, UnderwritingAuthority = @UnderwritingAuthority
			, UseCorrespondentBranchLpePriceGroupId = @UseCorrespondentBranchLpePriceGroupId
			, UseMiniCorrespondentBranchLpePriceGroupId = @UseMiniCorrespondentBranchLpePriceGroupId
			, PMLNavigationPermissions = @PMLNavigationPermissions
			, BrokerRoleStatusT = @BrokerRoleStatusT
			, MiniCorrRoleStatusT = @MiniCorrRoleStatusT
			, CorrRoleStatusT = @CorrRoleStatusT
			, UseDefaultWholesalePriceGroupId = COALESCE(@UseDefaultWholesalePriceGroupId, UseDefaultWholesalePriceGroupId)
			, UseDefaultWholesaleBranchId = COALESCE(@UseDefaultWholesaleBranchId, UseDefaultWholesaleBranchId)
			, UseDefaultMiniCorrPriceGroupId = COALESCE(@UseDefaultMiniCorrPriceGroupId, UseDefaultMiniCorrPriceGroupId)
			, UseDefaultMiniCorrBranchId = COALESCE(@UseDefaultMiniCorrBranchId, UseDefaultMiniCorrBranchId)
			, UseDefaultCorrPriceGroupId = COALESCE(@UseDefaultCorrPriceGroupId, UseDefaultCorrPriceGroupId)
			, UseDefaultCorrBranchId = COALESCE(@UseDefaultCorrBranchId, UseDefaultCorrBranchId)
			, OriginatorCompensationIsOnlyPaidForFirstLienOfCombo = COALESCE(@OriginatorCompensationIsOnlyPaidForFirstLienOfCombo, OriginatorCompensationIsOnlyPaidForFirstLienOfCombo)
			, TpoColorThemeId = COALESCE(@TpoColorThemeId, TpoColorThemeId)
			, TaxIdEncrypted = COALESCE(@TaxIdEncrypted, TaxIdEncrypted)
	WHERE PmlBrokerId = @PmlBrokerId AND BrokerId = @BrokerId
END