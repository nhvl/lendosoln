CREATE PROCEDURE [dbo].RetrieveLandingPageIdByEmployeeId
	@EmployeeId uniqueidentifier
AS
BEGIN	
	SELECT LandingPageId
	FROM Employee
	WHERE @EmployeeId = @EmployeeId
END
