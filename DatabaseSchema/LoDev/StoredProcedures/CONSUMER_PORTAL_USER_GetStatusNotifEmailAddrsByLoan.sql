-- =============================================
-- Author:		Geoffrey Feltman
-- Create date: 6/10/13
-- Description:	Get all of the email addresses for 
-- consumer status change notifications for a loan.
-- =============================================
ALTER PROCEDURE [dbo].[CONSUMER_PORTAL_USER_GetStatusNotifEmailAddrsByLoan] 
	-- Add the parameters for the stored procedure here
	@sLId uniqueidentifier
AS
BEGIN
	SELECT u.Id, Email
	FROM CONSUMER_PORTAL_USER_X_LOANAPP x JOIN 
		 CONSUMER_PORTAL_USER u on u.id = x.ConsumerUserId
	WHERE x.sLId = @sLId AND IsNotifyOnStatusChanges = 1
END
