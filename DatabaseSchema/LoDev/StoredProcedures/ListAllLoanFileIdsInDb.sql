CREATE  PROCEDURE ListAllLoanFileIdsInDb
	@IsValid bit
AS
	select sLId , sLNm
	from loan_file_cache with(nolock)
	where IsValid = @IsValid
