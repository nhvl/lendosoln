
CREATE Procedure [dbo].[GetPerFieldSecurityEntries]
AS
	SELECT sFieldId, sDescription, LastEditedD, bLockDeskPerm, bCloserPerm, bAccountantPerm, bInvestorInfoPerm
	FROM LOAN_FIELD_SECURITY

