CREATE procedure [dbo].[GetScheduledReportDetails]
@ReportId int,
@BrokerId uniqueidentifier
as
select r.UserId, r.ReportType, r.QueryId, r.ScheduleName, a.BrokerId
from 
SCHEDULED_REPORT_USER_INFO r join VIEW_ACTIVE_LO_USER a
on a.UserId = r.UserId
where r.ReportId = @ReportId
and (@BrokerId = a.BrokerId or @BrokerId is null)