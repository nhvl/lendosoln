CREATE PROCEDURE [GetGroupMembershipsForPmlBroker]
	@BrokerId uniqueidentifier,
	@PmlBrokerId uniqueidentifier
AS
BEGIN
	SET NOCOUNT ON
	-- GroupType 3 are PmlBroker groups
	SELECT CASE WHEN gp.PmlBrokerId IS NULL THEN 0 ELSE 1 END AS IsInGroup, g.* 
	FROM [GROUP] g LEFT JOIN (SELECT * FROM GROUP_x_PML_BROKER WHERE PmlBrokerId = @PmlBrokerId) gp on g.GroupId =gp.GroupId 
	WHERE g.GroupType = 3 AND g.BrokerID=@BrokerId ORDER BY g.GroupName
END
