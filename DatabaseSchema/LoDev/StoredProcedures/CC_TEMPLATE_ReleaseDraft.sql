-- =============================================
-- Author:		Antonio Valencia
-- Create date: 7/26/2012
-- Description:	Releases a draft
-- =============================================
CREATE PROCEDURE [dbo].[CC_TEMPLATE_ReleaseDraft]
	@BrokerId uniqueidentifier,
	@DraftId int,
	@UserId uniqueidentifier  
AS
BEGIN
	DECLARE @Rows int
	DECLARE @ResultCount int 
	DECLARE @IdToDelete int 
	

	
	UPDATE CC_TEMPLATE_SYSTEM SET 
		ReleasedD = GETDATE(),
		ReleasedByUserId = @UserId 
	WHERE Id = @DraftId AND ReleasedD IS NULL  
	

	select @Rows = @@rowcount 
	
	IF @Rows = 1 BEGIN	
		INSERT INTO CC_TEMPLATE_SYSTEM_RELEASE_AUDIT(BrokerId,[Description], DraftOpenedD,DraftOpenedByUserId,  ReleasedD, ReleasedByUserId)
		SELECT BrokerId, [Description], DraftOpenedD,DraftOpenedByUserId,  ReleasedD, ReleasedByUserId FROM CC_TEMPLATE_SYSTEM Where Id = @DraftId and BrokerId = @BrokerId
	END
	
	--how many released templates ? 
	SELECT @ResultCount = count(*) FROM CC_TEMPLATE_SYSTEM where BrokerId = @BrokerId and ReleasedD Is Not null 
	
	IF @ResultCount > = 5 BEGIN --we have too many release templates 
		--get the id of the top oldest template
		SELECT TOP 1 @IdToDelete = Id from CC_TEMPLATE_SYSTEM WHERE BrokerId = @BrokerId and releasedd is not null order by releasedd asc 
		
		-- now delete it 
		DELETE FROM CC_TEMPLATE where CCTemplateSystemId = @IdToDelete  and BrokerId = @BrokerId
		DELETE FROM CC_TEMPLATE_SYSTEM WHERE ID = @IdToDelete and BrokerId = @BrokerId
	END
	
	SELECT @Rows 
END
