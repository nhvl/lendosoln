-- =============================================
-- Author:		Michael Leinweaver
-- CREATE date: August 12, 2015
-- Description:	Gets branch ID using the branch's name and the broker's ID.
-- =============================================
CREATE PROCEDURE [dbo].[BRANCH_GetBranchIdByBranchNameAndBrokerId]
	-- Add the parameters for the stored procedure here
	@BranchNm varchar(100),
	@BrokerId uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT BranchId 
	FROM BRANCH 
	WHERE BranchNm = @BranchNm and BrokerId = @BrokerId

END
