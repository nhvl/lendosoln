CREATE PROCEDURE [dbo].[RetrievePMLBrokerIDByCustomerCode]
	@CustomerCode varchar(36)
AS
	SELECT
		BrokerPmlSiteId
	FROM
		Broker with (nolock)
	WHERE
		CustomerCode = @CustomerCode
	if( 0!=@@error)
	begin
		RAISERROR('Error in the select statement in [RetrievePMLBrokerIDByCustomerCode] sp', 16, 1);
		return -100;
	end
	return 0;
