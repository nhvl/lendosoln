

CREATE PROCEDURE [dbo].[TEAM_SetLoanAssignment] 
	@TeamId uniqueidentifier, 
	@LoanId uniqueidentifier,
	@RoleId uniqueidentifier = null
AS
BEGIN
	
	IF @TeamID = '00000000-0000-0000-0000-000000000000'  
	BEGIN  
		DELETE FROM Team_Loan_Assignment WHERE LoanId = @LoanID AND RoleID = @RoleID  
		RETURN;
	END  

	BEGIN TRANSACTION 
	
	--If not provided, assume assigning in current role of team. (Very rare to change)
	IF @RoleId IS NULL
	BEGIN
		SET @RoleId = (SELECT TOP 1 RoleId from TEAM WHERE Id = @TeamId)
		
		IF @RoleId IS NULL
		   	GOTO HANDLE_ERROR
	END

	--Clear any existing team.  Only one allowed per loan per role.
	DELETE FROM Team_Loan_Assignment WHERE LoanId = @LoanId AND RoleId = @RoleId


	INSERT INTO Team_Loan_Assignment(TeamId, LoanId, RoleId)
	VALUES(@TeamId, @LoanId, @RoleId) 
	
	COMMIT TRANSACTION
	
	RETURN;

   HANDLE_ERROR:
      	ROLLBACK TRANSACTION
     	RAISERROR('Error in TEAM_SetLoanAssignment sp', 16, 1);;
     	RETURN;

	
END
