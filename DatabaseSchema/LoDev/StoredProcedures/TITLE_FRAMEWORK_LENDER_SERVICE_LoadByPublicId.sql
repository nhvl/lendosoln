-- =============================================
-- Author:		Eric Mallare
-- Create date: 9/25/2017
-- Description:	Gets a LenderService by the public id.
-- =============================================
ALTER PROCEDURE [dbo].[TITLE_FRAMEWORK_LENDER_SERVICE_LoadByPublicId]
	@PublicId uniqueidentifier,
    @BrokerId uniqueidentifier
AS
BEGIN
	SELECT * 
	FROM 
		dbo.TITLE_FRAMEWORK_LENDER_SERVICE
	WHERE 
		PublicId = @PublicId
		AND BrokerId = @BrokerId
END
