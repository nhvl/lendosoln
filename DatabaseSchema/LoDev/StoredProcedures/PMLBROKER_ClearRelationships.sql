-- =============================================
-- Author:		Michael Leinweaver
-- Create date: 3/8/2016
-- Description:	Clears role relationship information for an OC.
-- =============================================
ALTER PROCEDURE [dbo].[PMLBROKER_ClearRelationships]
	@BrokerId uniqueidentifier,
	@PmlBrokerId uniqueidentifier,
	@OCRoleT int
AS
BEGIN
	DELETE FROM PML_BROKER_RELATIONSHIPS
	WHERE 
		BrokerId = @BrokerId AND 
		PmlBrokerId = @PmlBrokerId AND
		OCRoleT = @OCRoleT
END
GO
