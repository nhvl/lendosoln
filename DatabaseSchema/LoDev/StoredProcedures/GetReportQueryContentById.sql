
ALTER PROCEDURE [dbo].[GetReportQueryContentById] 
	@QueryId Guid,
	@BrokerId Guid = null
AS
SELECT XmlContent 
FROM Report_Query
JOIN Broker ON Broker.BrokerId = Report_Query.BrokerId
WHERE 
	QueryId = @QueryId AND
	(
		@BrokerId is null 
		OR
		Report_Query.BrokerId = @BrokerId
	)
	AND Broker.Status = 1

