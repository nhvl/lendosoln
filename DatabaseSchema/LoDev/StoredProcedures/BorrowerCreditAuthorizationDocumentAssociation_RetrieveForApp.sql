ALTER PROCEDURE [dbo].[BorrowerCreditAuthorizationDocumentAssociation_RetrieveForApp]
	@AppId uniqueidentifier
AS
BEGIN
	SELECT bcada.AppId, bcada.IsCoborrower, bcada.DocumentId, doc.[Description] as DocumentDescription
	FROM BORROWER_CREDIT_AUTHORIZATION_DOCUMENT_ASSOCIATION bcada
		JOIN EDOCS_Document doc ON bcada.DocumentId = doc.DocumentId
	WHERE bcada.AppId = @AppId AND doc.IsValid = 1
END
