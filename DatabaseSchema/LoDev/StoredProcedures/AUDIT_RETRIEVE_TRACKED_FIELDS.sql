-- =============================================
-- Author:		Antonio Valencia
-- Create date: 12/16/11
-- Description:	Retrieves tracked fields
-- =============================================
CREATE PROCEDURE [dbo].[AUDIT_RETRIEVE_TRACKED_FIELDS]

AS
BEGIN
	SELECT 
		t.sFieldId, 
		Description, 
		TrackedSince, 
		COALESCE(s.bLockDeskPerm, 0) as RequiresLockDeskPermission,
		COALESCE(s.bCloserPerm, 0) as RequiresCloserPermission,
		COALESCE(s.bAccountantPerm, 0) as  RequiresAccountantPermision,
		COALESCE(s.bInvestorInfoPerm, 0) as RequiresInvestorInfoPermission
		 
	FROM LOAN_FIELD_TRACKED as t
	LEFT JOIN LOAN_FIELD_SECURITY as s ON t.sFieldId = s.sFieldId
END
