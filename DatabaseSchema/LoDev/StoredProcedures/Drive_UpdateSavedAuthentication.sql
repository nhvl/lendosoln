-- =============================================
-- Author:		Matthew Pham
-- Create date: 6/12/12
-- Description:	Stores the user's authentication
--              info for CBC DRIVE.
-- =============================================
ALTER PROCEDURE [dbo].[Drive_UpdateSavedAuthentication]
	@UserId UniqueIdentifier,
	@DriveUserName varchar(50),
	@DrivePassword varchar(50) = null,
	@EncryptedDrivePassword varbinary(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	UPDATE BROKER_USER
	SET
		DriveUserName = @DriveUserName,
		DrivePassword = COALESCE(@DrivePassword, DrivePassword),
		EncryptedDrivePassword = @EncryptedDrivePassword
	WHERE UserId = @UserId
END
