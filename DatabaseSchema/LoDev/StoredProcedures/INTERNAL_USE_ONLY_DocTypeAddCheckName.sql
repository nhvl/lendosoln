CREATE procedure [dbo].[INTERNAL_USE_ONLY_DocTypeAddCheckName]
	@BrokerId uniqueidentifier,
	@DocTypeName varchar(50)
as

select 1 
	from EDOCS_DOCUMENT_TYPE with (nolock)
	where DocTypeName = @DocTypeName
		and BrokerId = @BrokerId
		and IsValid = 1