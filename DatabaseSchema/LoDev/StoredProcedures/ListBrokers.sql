


ALTER  PROCEDURE [dbo].[ListBrokers]
	@BrokerName varchar( 32 ) = NULL , 
	@AccountManager varchar( 10 ) = NULL , 
	@CustomerCode varchar( 10 ) = NULL , 
	@HasLenderDefaultFeatures bit = NULL , 
	@BrokerStatus int = NULL,
	@DuUserId varchar(100) = NULL,
	@BrokerID uniqueidentifier = NULL,
	@SuiteType tinyint = NULL
AS
	SELECT
		BrokerID,
		BrokerNm,
		BrokerAddr + ' ' + BrokerCity + ' ' + BrokerState + ' ' + BrokerZip AS BrokerAddress,
		BrokerPhone,
		BrokerFax,
		CreditReportURL,
		CustomerCode,
		CreditMornetPlusUserId,
		AccountManager,
		PricePerSeat,
		EmailAddressesForSystemChangeNotif,
		CASE [Status] WHEN 0 THEN 'No' WHEN 1 THEN 'Yes' END  AS IsActive,
		[Status],
		Notes,
		SuiteType
	FROM Broker
	WHERE
		BrokerNm LIKE COALESCE( @BrokerName + '%' , BrokerNm )
		AND
		CreditMornetPlusUserId LIKE COALESCE(@DuUserId + '%', CreditMornetPlusUserId)
		AND
		AccountManager LIKE COALESCE( @AccountManager + '%' , AccountManager )
		AND
		CustomerCode LIKE COALESCE( @CustomerCode + '%' , CustomerCode )
		AND
		HasLenderDefaultFeatures = COALESCE( @HasLenderDefaultFeatures , HasLenderDefaultFeatures )
		AND
		Status = COALESCE( @BrokerStatus , Status )
		AND
		BrokerID = COALESCE( @BrokerID, BrokerID)
		AND
		SuiteType = COALESCE(@SuiteType, SuiteType)

	ORDER BY BrokerNm
	if( 0!=@@error)
	begin
		RAISERROR('Error in the selection statement in ListBrokers sp', 16, 1);
		return -100;
	end
	return 0;



