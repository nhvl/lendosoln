/*
	Author: 
		Scott Kibler
	Description: 
		Gets the vendor id, integration type (e.g. irs4506t) + request type (e.g. order),
		and xml and xsl file ids used for that vendor for that integration + request type.
		There should only be 0-1 records in the table at a time during the first phase.
	opm:
		463659
	Remarks:
		This sproc will need to be deleted at a later phase of this case.
*/
CREATE PROCEDURE [dbo].[TEMP_GetOnlyVendorIntegrationRequestFormat]
AS
BEGIN
	SELECT TOP 1 
		f.VendorId, f.IntegrationRequestType, f.LoXmlFileId, f.XslFileId, xmlf.Name as LoXmlFileName, xslf.Name as XslFileName
	FROM
		VENDOR_INTEGRATION_REQUEST_FORMAT f
	LEFT JOIN 
		LO_XML_FILE xmlf
	ON 	xmlf.Id = f.LoXmlFileId
	LEFT JOIN 
		XSL_FILE xslf
	ON xslf.Id = f.XslFileId
END
GO


