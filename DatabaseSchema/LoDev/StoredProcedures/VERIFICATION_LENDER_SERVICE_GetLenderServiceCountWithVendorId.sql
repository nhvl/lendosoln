-- =============================================
-- Author:		Eric Mallare
-- Create date: 5/2/2017
-- Description:	Retrieves the records in VERIFICATION_LENDER_SERVICE that use this vendor.
-- =============================================
ALTER PROCEDURE [dbo].[VERIFICATION_LENDER_SERVICE_GetLenderServiceCountWithVendorId]
	@VendorId int
AS
BEGIN
	SELECT
		COUNT(*) as ServiceCount
	FROM
		VERIFICATION_LENDER_SERVICE
	WHERE
		VendorId=@VendorId OR
		ResellerId=@VendorId
END
