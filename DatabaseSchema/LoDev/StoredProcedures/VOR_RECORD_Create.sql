-- ============================================================
-- Author:      Generated by application LqbEntitiesToSqlTables
-- Create date: 11/27/2018
-- Description: Inserts a new row into the VOR_RECORD table.
-- ============================================================
ALTER PROCEDURE [dbo].[VOR_RECORD_Create]
	@VorRecordId UniqueIdentifier,
	@LoanId UniqueIdentifier,
	@BrokerId UniqueIdentifier,
	@VerifSignatureImgId UniqueIdentifier,
	@VerifSigningEmployeeId UniqueIdentifier,
	@AccountName VarChar(512),
	@StreetAddressFor VarChar(256),
	@StreetAddressTo VarChar(256),
	@Attention VarChar(512),
	@CityFor VarChar(256),
	@CityTo VarChar(256),
	@Description VarChar(512),
	@FaxTo VarChar(64),
	@IsForBorrower Bit = NULL, -- TODO: Remove this once the column is dropped after 2019.R1
	@IsSeeAttachment Bit,
	@LandlordCreditorName VarChar(512),
	@PhoneTo VarChar(64),
	@PrepDate DateTime,
	@StateFor Char(2),
	@StateTo Char(2),
	@VerifExpDate DateTime,
	@VerifRecvDate DateTime,
	@VerifReorderedDate DateTime,
	@VerifSentDate DateTime,
	@VerifType TinyInt,
	@ZipFor VarChar(10),
	@ZipTo VarChar(10),
    @LegacyRecordSpecialType TinyInt
AS
BEGIN
	INSERT INTO [dbo].[VOR_RECORD]
	(
		VorRecordId,
		LoanId,
		BrokerId,
		VerifSignatureImgId,
		VerifSigningEmployeeId,
		AccountName,
		StreetAddressFor,
		StreetAddressTo,
		Attention,
		CityFor,
		CityTo,
		Description,
		FaxTo,
		IsSeeAttachment,
		LandlordCreditorName,
		PhoneTo,
		PrepDate,
		StateFor,
		StateTo,
		VerifExpDate,
		VerifRecvDate,
		VerifReorderedDate,
		VerifSentDate,
		VerifType,
		ZipFor,
		ZipTo,
        LegacyRecordSpecialType
	)
	VALUES
	(
		@VorRecordId,
		@LoanId,
		@BrokerId,
		@VerifSignatureImgId,
		@VerifSigningEmployeeId,
		@AccountName,
		@StreetAddressFor,
		@StreetAddressTo,
		@Attention,
		@CityFor,
		@CityTo,
		@Description,
		@FaxTo,
		@IsSeeAttachment,
		@LandlordCreditorName,
		@PhoneTo,
		@PrepDate,
		@StateFor,
		@StateTo,
		@VerifExpDate,
		@VerifRecvDate,
		@VerifReorderedDate,
		@VerifSentDate,
		@VerifType,
		@ZipFor,
		@ZipTo,
        @LegacyRecordSpecialType
	)
END
