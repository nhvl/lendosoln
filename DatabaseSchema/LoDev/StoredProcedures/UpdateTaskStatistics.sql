-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE dbo.UpdateTaskStatistics
	@sLId UniqueIdentifier,
	@sNumOfActiveTasks int,
	@sNumOfActiveDueTodayTasks int, 
	@sNumOfActivePastDueTasks int, 

	@sNumOfOutstandingTasks int,
	@sNumOfOutStandingDueTodayTasks int,
	@sNumOfOutStandingPastDueTasks int,

	@sNumOfClosedTasks int, 
	
	@sNumOfActiveTasksOnly int,
	@sNumOfActiveConditionOnly int,
	@sNumOfActivePastDueConditionOnly int,
	@sNumOfActiveDueTodayTasksOnly int,
	@sNumOfActiveDueTodayConditionOnly int, 
	@sNumOfActivePastDueTasksOnly int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	UPDATE TOP(1) LOAN_FILE_CACHE_2 
	SET 
		sNumOfActiveTasks = @sNumOfActiveTasks,
		sNumOfActiveDueTodayTasks =@sNumOfActiveDueTodayTasks, 
		sNumOfActivePastDueTasks =@sNumOfActivePastDueTasks, 

		sNumOfOutstandingTasks =@sNumOfOutstandingTasks,
		sNumOfOutStandingDueTodayTasks =@sNumOfOutStandingDueTodayTasks,
		sNumOfOutStandingPastDueTasks =@sNumOfOutStandingPastDueTasks,

		sNumOfClosedTasks =@sNumOfClosedTasks
	WHERE sLId = @sLId
	
	UPDATE TOP(1) LOAN_FILE_CACHE_3
	SET
		sNumOfActiveTasksOnly = @sNumOfActiveTasksOnly,
		sNumOfActiveConditionOnly = @sNumOfActiveConditionOnly,
		sNumOfActivePastDueConditionOnly = @sNumOfActivePastDueConditionOnly,
		sNumOfActiveDueTodayTasksOnly = @sNumOfActiveDueTodayTasksOnly,
		sNumOfActiveDueTodayConditionOnly =  @sNumOfActiveDueTodayConditionOnly,
		sNumOfActivePastDueTasksOnly = @sNumOfActivePastDueTasksOnly
		
	WHERE sLId = @sLId
		
END		
