-- =============================================
-- Author:		Budi Sulayman
-- Create date: 8/28/2015
-- Description:	Retrieve list of loans by branchId and closing cost fee type
-- =============================================
CREATE PROCEDURE [dbo].[ListLegacyLoansByBranchId]
	@BrokerId uniqueidentifier, 
	@BranchId uniqueidentifier,
	@IsValid bit = NULL,
	@IsTemplate bit = NULL
AS
BEGIN
	SELECT
		Loan_File_Cache.sLId AS LoanId,
		sBrokerId AS BrokerId,
		sBranchId AS BranchId,
		sLNm AS LoanNm
	FROM Loan_File_Cache JOIN Loan_File_B
	ON Loan_File_Cache.sLId = Loan_File_B.sLId
	WHERE
		sBrokerID = @BrokerId
		AND sBranchID = @BranchId
		AND IsTemplate = COALESCE( @IsTemplate , IsTemplate )
		AND IsValid = COALESCE( @IsValid , IsValid )
		AND sDisclosureRegulationT = 1
		AND sClosingCostFeeVersionT = 0
END


