-- =============================================
-- Author:		Eric Mallare
-- Create date: 12/5/2017
-- Description:	Sets the background job back to incomplete and gives it a new next run date.
-- =============================================
ALTER PROCEDURE [dbo].[BACKGROUND_JOBS_ResetOnError]
	@JobId int,
	@NextRunDate datetime,
	@ExceptionCount int = 0
AS
BEGIN
	UPDATE 
		BACKGROUND_JOBS
	SET
		ProcessingStatus=1, --ProcessingStatus.Incomplete
		NextRunDate=@NextRunDate,
		ExceptionCount=@ExceptionCount
	WHERE
		JobId=@JobId
END