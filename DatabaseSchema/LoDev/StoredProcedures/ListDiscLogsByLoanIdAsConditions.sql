CREATE PROCEDURE ListDiscLogsByLoanIdAsConditions
	@LoanID Guid ,	@StatusFilter Int = NULL , @IsForCondition Bit = NULL , @IsValid Bit = NULL
AS
	SELECT CASE WHEN l.DiscPriority = 0 THEN 'True' ELSE 'False' END AS IsRequired ,  CASE WHEN l.DiscStatus = 1 THEN 'True' ELSE 'False' END AS IsDone , CASE WHEN l.DiscStatus = 1 THEN l.DiscStatusDate ELSE NULL END AS DoneDate , l.CondCategoryDesc AS PriorToEventDesc , l.DiscSubject AS CondDesc
	FROM Discussion_Log AS l
	WHERE
		l.DiscRefObjId = @LoanID
		AND
		l.DiscStatus = COALESCE( @StatusFilter , l.DiscStatus )
		AND
		l.IsForCondition = COALESCE( @IsForCondition , l.IsForCondition )
		AND
		l.IsValid = COALESCE( @IsValid , l.IsValid )
		AND
		l.DiscIsRefObjValid = 1
	ORDER BY CondOrderedPosition , DiscCreatedDate
	IF( 0!=@@error)
	BEGIN
		RAISERROR('Error in the selection statement in ListDiscLogsByLoanIdAsConditions sp', 16, 1);
		RETURN -100;
	END
	RETURN 0;
