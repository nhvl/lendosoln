-- =============================================
-- Author:		Scott Kibler
-- Create date: 7/23/2014
-- Description:	
--   Gets an the number of unsused loans 
--   at the broker if it matches the qp template, and the userid.
-- =============================================
CREATE PROCEDURE [dbo].[QUICKPRICER_2_LOAN_USAGE_GetNumUnusedLoans]
	
	@BrokerID UniqueIdentifier,
	@QuickPricerTemplateID UniqueIdentifier,
	@UserIDofUsingUser UniqueIdentifier,
	@BranchChannelT int = 0,
	@CorrespondentProcessT int = 0,
	@IsWholesale bit = 0,
	
	-- optional parameters:
	@QuickPricerTemplateFileVersion INT = NULL -- supplying the value makes it faster, but allows out of sync.
AS
BEGIN
    -- Grab the matching QPTemplate file version, or error if none exists.
	IF @QuickPricerTemplateFileVersion IS NULL
	BEGIN
		SELECT @QuickPricerTemplateFileVersion = sFileVersion
		FROM LOAN_FILE_A
		WHERE sLId = @QuickPricerTemplateID
		IF( @@ROWCOUNT <> 1 )
		BEGIN
			RAISERROR ('No loan with the given qp template id found',
						 16,
						 1)

			select 0
		END 
	END
	
	IF(@IsWholesale = 1) 
	BEGIN
		SELECT 
			count(LoanID)
		FROM 
			QUICKPRICER_2_LOAN_USAGE as q JOIN LOAN_FILE_CACHE_2 as lc2 on (q.LoanID = lc2.sLId) JOIN LOAN_FILE_CACHE_3 as lc3 on (q.LoanID = lc3.sLId)
		WHERE 
				BrokerID = @BrokerID
			AND IsInUse = 0
			AND QuickPricerTemplateID = @QuickPricerTemplateID
			AND QuickPricerTemplateFileVersion = @QuickPricerTemplateFileVersion
			AND UserIDofUsingUser = @UserIDofUsingUser
			AND sBranchChannelT = @BranchChannelT
	END
	ELSE IF (@BranchChannelT > 0)
	BEGIN 
		SELECT 
			count(LoanID)
		FROM 
			QUICKPRICER_2_LOAN_USAGE as q JOIN LOAN_FILE_CACHE_2 as lc2 on (q.LoanID = lc2.sLId) JOIN LOAN_FILE_CACHE_3 as lc3 on (q.LoanID = lc3.sLId)
		WHERE 
				BrokerID = @BrokerID
			AND IsInUse = 0
			AND QuickPricerTemplateID = @QuickPricerTemplateID
			AND QuickPricerTemplateFileVersion = @QuickPricerTemplateFileVersion
			AND UserIDofUsingUser = @UserIDofUsingUser
			AND sBranchChannelT = @BranchChannelT
			AND sCorrespondentProcessT = @CorrespondentProcessT
	END		
	ELSE
	BEGIN
		SELECT 
			count(LoanID)
		FROM 
			QUICKPRICER_2_LOAN_USAGE
		WHERE 
				BrokerID = @BrokerID
			AND IsInUse = 0
			AND QuickPricerTemplateID = @QuickPricerTemplateID
			AND QuickPricerTemplateFileVersion = @QuickPricerTemplateFileVersion
			AND UserIDofUsingUser = @UserIDofUsingUser
	END	
END
