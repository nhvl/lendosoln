-- =============================================
-- Author:		David Dao
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE dbo.BROKER_WEBSERVICE_APP_CODE_HasCode
    @BrokerId uniqueidentifier
AS
BEGIN
    SELECT TOP 1 AppCode FROM BROKER_WEBSERVICE_APP_CODE WHERE BrokerId=@BrokerId AND Enabled=1

END
