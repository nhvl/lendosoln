-- =============================================
-- Author:		Geoff Feltman
-- Create date: 1/13/17
-- Description:	Inserts a record into the SUBMISSION_SNAPSHOT table.
-- =============================================
ALTER PROCEDURE [dbo].[SUBMISSION_SNAPSHOT_Insert]
	@LoanId uniqueidentifier,
	@BrokerId uniqueidentifier,
	@SnapshotDataLastModifiedD datetime,
	@SnapshotFileKey varchar(100),
	@PriceGroupId uniqueidentifier,
	@PriceGroupFileKey varchar(100),
	@FeeServiceRevisionId int,
	@SubmissionType int,
	@SubmissionDate datetime,
	@Id int output
AS
BEGIN
	INSERT INTO SUBMISSION_SNAPSHOT WITH(ROWLOCK) (LoanId, BrokerId, SnapshotDataLastModifiedD, SnapshotFileKey, PriceGroupId, PriceGroupFileKey, FeeServiceRevisionId, SubmissionType, SubmissionDate)
	VALUES (@LoanId, @BrokerId, @SnapshotDataLastModifiedD, @SnapshotFileKey, @PriceGroupId, @PriceGroupFileKey, @FeeServiceRevisionId, @SubmissionType, @SubmissionDate)
	
	SELECT @Id = SCOPE_IDENTITY()
END
