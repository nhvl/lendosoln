-- =============================================
-- Author:		David Dao
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE dbo.ALL_USER_REGISTERED_IP_Delete
    @BrokerId UniqueIdentifier,
    @UserId UniqueIdentifier,
    @Id int

AS
BEGIN
    DELETE FROM ALL_USER_REGISTERED_IP
    WHERE BrokerId=@BrokerId AND UserId=@UserId AND Id=@Id

                               

END
