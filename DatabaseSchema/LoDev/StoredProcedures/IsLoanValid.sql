CREATE PROCEDURE IsLoanValid
	@LoanID Guid
AS
	SELECT IsValid FROM Loan_File_Cache with(updlock,rowlock) WHERE sLId = @LoanID
