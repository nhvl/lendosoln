CREATE PROCEDURE [dbo].[Employee_GetContactSupportBitFromEmployeeId]
	@EmployeeId GUID
AS
	SELECT e.CanContactSupport FROM EMPLOYEE e WHERE e.EmployeeId = @EmployeeId
