
ALTER     PROCEDURE [dbo].[CreateDefaultNonBoolRolePermission]
	@RoleID uniqueidentifier,
	@BrokerID uniqueidentifier,
	@DefaultNonBoolPermissions char(100),
	@DefaultPermissions char(255) = ''
AS
	IF @RoleID != '00000000-0000-0000-0000-000000000000'
	BEGIN
		INSERT INTO ROLE_DEFAULT_PERMISSIONS(RoleId, BrokerId, DefaultNonBoolPermissions, DefaultPermissions)
		VALUES (@RoleID, @BrokerID, @DefaultNonBoolPermissions, @DefaultPermissions)
	END
	IF 0 != @@ERROR
	BEGIN
		RAISERROR('Error inserting into ROLE_DEFAULT_PERMISSIONS in CreateDefaultNonBoolRolePermission sp', 16, 1);
		RETURN -100
	END
	RETURN 0;

