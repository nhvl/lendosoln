-- =============================================
-- Author:		Justin Jia
-- ALTER date: June 14, 2012
-- Description:	Gets all column property of one table by the table name
-- =============================================
CREATE PROCEDURE [dbo].[Justin_Testing_ListUsersBelongingToBranch] 
	@BranchID uniqueidentifier
AS
	SELECT temp1.employeeid, temp1.fullname, temp1.isactive, temp1.branchnm, temp2.userid, temp2.loginnm, temp2.type, temp2.IsAccountOwner, temp2.CanLogin, temp1.email
	FROM 
		( SELECT e.employeeid, e.userfirstnm + ' ' + e.userlastnm AS fullname, e.isactive, b.branchnm, e.email
		  FROM employee e LEFT OUTER JOIN branch b 
		  ON e.branchid = b.branchid 
		  WHERE b.branchid = @BranchID 
	    ) temp1
	LEFT OUTER JOIN
	( SELECT b.userid, b.employeeid, a.loginnm, a.isactive as canlogin, cast( a.type as varchar( 1 ) ) as type, b.IsAccountOwner 
	FROM broker_user b, all_user a where b.userid = a.userid) temp2
	ON temp1.employeeid = temp2.employeeid
	ORDER BY fullname;
