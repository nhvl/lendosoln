-- =============================================
-- Author:		Timothy Jewell
-- Create date: 6/28/2017
-- Description:	Updates an SSA-89 service.
-- =============================================
CREATE PROCEDURE [dbo].[SSA89_SERVICE_Update]
	@VendorServiceId int,
	@VendorId int,
	@IsEnabled bit,
	@IsADirectVendor bit,
	@SellsViaResellers bit,
	@IsAReseller bit
AS
BEGIN
	UPDATE SSA89_SERVICE
	SET
		VendorId=@VendorId,
		IsEnabled=@IsEnabled,
		IsADirectVendor=@IsADirectVendor,
		SellsViaResellers=@SellsViaResellers,
		IsAReseller=@IsAReseller
	WHERE
		VendorServiceId=@VendorServiceId

	IF @@error!= 0 GOTO HANDLE_ERROR
END
RETURN;

HANDLE_ERROR:
	RAISERROR('Error in SSA89_SERVICE_Update sp', 16, 1);;
	RETURN;
GO
