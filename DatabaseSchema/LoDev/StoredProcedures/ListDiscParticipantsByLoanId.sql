CREATE PROCEDURE ListDiscParticipantsByLoanId
	@LoanID Guid,
	@StatusFilter int = NULL
AS
	SELECT  n.NotifId, n.DiscLogId, n.NotifReceiverUserId, n.NotifReceiverLoginNm, n.NotifIsRead, n.NotifIsValid
	FROM Discussion_Log d WITH (NOLOCK) JOIN Discussion_Notification n WITH (NOLOCK) ON d.DiscLogId = n.DiscLogId
	WHERE d.DiscRefObjId = @LoanID AND d.DiscStatus = COALESCE(@StatusFilter, d.DiscStatus)
	if( 0!=@@error)
	begin
		RAISERROR('Error in the selection statement in ListDiscParticipantsByLoanId sp', 16, 1);
		return -100;
	end
	return 0;
