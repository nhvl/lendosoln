-- =============================================
-- Author:		Matthew Pham
-- Create date: 12/18/12
-- Description:	Select a GDMS order by file number ID
-- =============================================
ALTER PROCEDURE [dbo].[GDMS_GetOrderByFileNumber]
	-- Add the parameters for the stored procedure here
	@FileNumber int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT a.*, FileNumber, UserId, EmployeeId, VendorId, UploadedFileIdXmlContent
	FROM GDMS_ORDER g
	join APPRAISAL_ORDER a on g.AppraisalOrderId = a.AppraisalOrderId
	join broker b on g.brokerid = b.brokerid
	WHERE FileNumber=@FileNumber and b.status = 1

	SELECT
		GdmsFileId, EdocId, [FileName]
	FROM
		GDMS_ORDER_EDOCS
	WHERE
		FileNumber=@FileNumber
END