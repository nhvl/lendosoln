ALTER PROCEDURE [dbo].[DuplicateRolodex]
	@RolodexID Guid,
	@NewRolodexID Guid
AS
	DECLARE @AgentID Guid,
	@BrokerID Guid,
	@AgentType TypeName,
	@AgentTypeOtherDescription varchar(50),
	@AgentNm varchar(100),
	@AgentTitle ShortDesc,
	@AgentPhone PhoneNumber,
	@AgentAltPhone PhoneNumber,
	@AgentFax PhoneNumber,
	@AgentComNm varchar(100),
	@AgentAddr StreetAddress,
	@AgentCity City,
	@AgentState State,
	@AgentZip Zip,
	@AgentN varchar(500),
	@AgentDepartmentName varchar(100),
	@AgentPager PhoneNumber,
	@AgentLicenseNumber varchar(30),
	@CompanyLicenseNumber varchar(30),
	@PhoneOfCompany PhoneNumber,
	@FaxOfCompany PhoneNumber,
	@AgentEmail EmailAddress80,
	@IsNotifyWhenLoanStatusChange bit,
	@AgentWebsiteUrl varchar(200),
	@CompanyId varchar(25),
	@AgentBranchName varchar(100),
	@AgentPayToBankName varchar(50),
	@AgentPayToBankCityState varchar(50),
	@AgentPayToABANumber varchar(9),
	@AgentPayToAccountName varchar(50),
	@AgentPayToAccountNumber varchar(50),
	@AgentFurtherCreditToAccountName varchar(50),
	@AgentFurtherCreditToAccountNumber varchar(50),
	@AgentIsApproved bit,
	@AgentIsAffiliated bit,
	@OverrideLicenses bit
 
	SELECT @BrokerID = BrokerID,
	@AgentType = AgentType,
	@AgentTypeOtherDescription = AgentTypeOtherDescription,
	@AgentNm = AgentNm,
	@AgentTitle = AgentTitle,
	@AgentPhone = AgentPhone,
	@AgentAltPhone = AgentAltPhone,
	@AgentFax = AgentFax,
	@AgentComNm = AgentComNm,
	@AgentAddr = AgentAddr,
	@AgentCity = AgentCity,
	@AgentState = AgentState,
	@AgentZip = AgentZip,
	@AgentN = AgentN,
	@AgentDepartmentName = AgentDepartmentName,
	@AgentPager = AgentPager,
	@AgentLicenseNumber = AgentLicenseNumber,
	@CompanyLicenseNumber = CompanyLicenseNumber,
	@PhoneOfCompany = PhoneOfCompany,
	@FaxOfCompany = FaxOfCompany,
	@AgentEmail = AgentEmail,
	@IsNotifyWhenLoanStatusChange = IsNotifyWhenLoanStatusChange,
	@AgentWebsiteUrl = AgentWebsiteUrl,
	@CompanyId = CompanyId,
	@AgentBranchName = AgentBranchName,
	@AgentPayToBankName = AgentPayToBankName,
	@AgentPayToBankCityState = AgentPayToBankCityState,
	@AgentPayToABANumber = AgentPayToABANumber,
	@AgentPayToAccountName = AgentPayToAccountName,
	@AgentPayToAccountNumber = AgentPayToAccountNumber,
	@AgentFurtherCreditToAccountName = AgentFurtherCreditToAccountName,
	@AgentFurtherCreditToAccountNumber = AgentFurtherCreditToAccountNumber,
	@AgentIsApproved = AgentIsApproved,
	@AgentIsAffiliated = AgentIsAffiliated,
	@OverrideLicenses = OverrideLicenses
	
	FROM Agent
	WHERE AgentID = @RolodexID
	SET @AgentID = @NewRolodexID
	INSERT INTO Agent(AgentID, 	BrokerID, AgentType, AgentTypeOtherDescription, AgentNm, AgentTitle, AgentPhone, AgentAltPhone, AgentFax, AgentEmail, AgentComNm, AgentAddr, AgentCity, AgentState, AgentZip, AgentN, AgentDepartmentName, AgentPager, AgentLicenseNumber, CompanyLicenseNumber, PhoneOfCompany, FaxOfCompany, IsNotifyWhenLoanStatusChange, AgentWebsiteUrl, CompanyId, AgentBranchName, AgentPayToBankName, AgentPayToBankCityState, AgentPayToABANumber, AgentPayToAccountName, AgentPayToAccountNumber, AgentFurtherCreditToAccountName, AgentFurtherCreditToAccountNumber, AgentIsApproved, AgentIsAffiliated, OverrideLicenses)
        	   VALUES(@AgentID, 	@BrokerID, @AgentType, @AgentTypeOtherDescription, @AgentNm, @AgentTitle, @AgentPhone, @AgentAltPhone, @AgentFax, @AgentEmail, @AgentComNm, @AgentAddr, @AgentCity, @AgentState, @AgentZip, @AgentN, @AgentDepartmentName, @AgentPager, @AgentLicenseNumber, @CompanyLicenseNumber, @PhoneOfCompany, @FaxOfCompany, @IsNotifyWhenLoanStatusChange, @AgentWebsiteUrl, @CompanyId, @AgentBranchName, @AgentPayToBankName, @AgentPayToBankCityState, @AgentPayToABANumber, @AgentPayToAccountName, @AgentPayToAccountNumber, @AgentFurtherCreditToAccountName, @AgentFurtherCreditToAccountNumber, @AgentIsApproved, @AgentIsAffiliated, @OverrideLicenses)
	IF ( 0 != @@ERROR )
	BEGIN
		RAISERROR('Error in inserting into Agent table in DuplicateRolodex SP', 16, 1);
		RETURN -100;
	END
	RETURN 0;	

