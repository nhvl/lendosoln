

CREATE PROCEDURE [dbo].[CreateLockDeskClosure]
	@BrokerId uniqueidentifier,
	@ClosureDate smalldatetime,
	@LockDeskOpenTime smalldatetime,
	@LockDeskCloseTime smalldatetime,
	@IsClosedAllDay bit
AS
	INSERT INTO LockDesk_Closure (ClosureDate, BrokerId, IsClosedAllDay, LockDeskOpenTime, LockDeskCloseTime)
	VALUES (@ClosureDate, @BrokerId, @IsClosedAllDay, @LockDeskOpenTime, @LockDeskCloseTime);

if( 0!=@@error )
begin
	RAISERROR('Error in inserting into table LockDesk_Closure in CreateLockDeskClosure sp', 16, 1);
	return -100
end
return 0