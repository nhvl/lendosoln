-- =============================================
-- Author:		Justin Kim
-- Create date: 6/29/17
-- Description:	Get all tracked feature information
-- =============================================
ALTER PROCEDURE [dbo].[GetAllTrackedFeatures]

AS
BEGIN
	SET NOCOUNT ON;

	SELECT FeatureId, Priority, IsHidden
	From TRACKED_FEATURE
END
