
CREATE PROCEDURE [dbo].[ListPriceMyLoanBrokers]
	@BrokerStatus int = NULL
AS
SELECT BrokerNm, BrokerPmlSiteId, CustomerCode
FROM Broker br JOIN Feature_Subscription f ON br.BrokerID = f.BrokerID
WHERE f.FeatureId='0FAB7C8C-B3C4-49E5-92BF-2DCDD45B701F'
	AND f.IsActive=1
	AND br.Status = COALESCE ( @BrokerStatus , br.Status )
ORDER BY BrokerNm

