

CREATE PROCEDURE [dbo].[OCR_DOCUMENT_Add]
	@BrokerID uniqueidentifier,
	@UserID uniqueidentifier,
	@TransactionID uniqueidentifier,
	@FileName varchar(50),
	@SizeBytes int,
	@UploadedOn datetime,
	@Status int,
	@StatusUpdatedOn datetime,
	@VendorID uniqueidentifier,
	@Source int,
	@UploadAppID uniqueidentifier = null,
	@UploadLoanID uniqueidentifier = null,
	@UploadDocTypeID int = null,
	@UploadInternalDescription varchar(200) = ''
AS

INSERT INTO [OCR_DOCUMENT]
           ([BrokerID]
           ,[UserID]
           ,[TransactionID]
           ,[FileName]
           ,[SizeBytes]
           ,[UploadedOn]
           ,[Status]
           ,[StatusUpdatedOn]
           ,[VendorID]
           ,[Source]
           ,[UploadAppID]
           ,[UploadLoanID]
           ,[UploadDocTypeID]
           ,[UploadInternalDescription])
     VALUES
           (@BrokerID ,
@UserID ,
@TransactionID ,
@FileName ,
@SizeBytes ,
@UploadedOn ,
@Status ,
@StatusUpdatedOn ,
@VendorID ,
@Source ,
@UploadAppID ,
@UploadLoanID ,
@UploadDocTypeID ,
@UploadInternalDescription)

