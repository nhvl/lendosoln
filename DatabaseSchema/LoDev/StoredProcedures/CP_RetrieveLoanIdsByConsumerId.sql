-- =============================================
-- Author:		<Francesc>
-- Create date: <04/29/10>
-- Description:	<Retrieve loans associated with a given consumerId>
-- =============================================
CREATE PROCEDURE [dbo].[CP_RetrieveLoanIdsByConsumerId]
	@ConsumerId bigint
AS
BEGIN
	SET NOCOUNT ON;

	SELECT sLId
	FROM CP_CONSUMER_X_LOANID
	WHERE ConsumerId = @ConsumerId
	if( 0!=@@error)
		begin
			RAISERROR('Error in the select statement in CP_RetrieveLoanIdsByConsumerId sp', 16, 1);
			return -100;
		end
		return 0;
END
