-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE dbo.Accounting_RetrieveEmployeeLOSplitByBrokerId
	@BrokerId UniqueIdentifier
AS
BEGIN
			SELECT EmployeeId, CommissionPointOfGrossProfit FROM Employee e JOIN Branch br ON e.BranchId = br.BranchId
			WHERE br.BrokerId = @BrokerId
END
