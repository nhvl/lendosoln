-- =============================================
-- Author:		David Dao
-- Create date: Aug 8, 2013
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[IRS_4506T_ORDER_INFO_UpdateResponse]
    @TransactionId UniqueIdentifier,
    @Status varchar(1000),
    @StatusT int,
    @ResponseXmlContent varchar(max),
    @UploadedFilesXmlContent varchar(max)
AS
BEGIN

    UPDATE IRS_4506T_ORDER_INFO
       SET [Status] = @Status,
           StatusT = @StatusT,
           StatusDate = GETDATE(),
           ResponseXmlContent = @ResponseXmlContent,
           UploadedFilesXmlContent = @UploadedFilesXmlContent
    WHERE 
        TransactionId = @TransactionId

END
