






CREATE  PROCEDURE [dbo].[CP_UpdateActionResponse]
@ConsumerActionRequestId bigint
--, @ArchivedEDocId uniqueidentifier = NULL
, @ConsumerCompletedD datetime = NULL
--, @RequestorAcceptedD datetime = NULL
, @ReceivingFaxNumber varchar(14) = NULL
, @BorrowerSignedEventId uniqueidentifier = NULL
, @CoborrowerSignedEventId uniqueidentifier = NULL

AS
UPDATE CP_CONSUMER_ACTION_RESPONSE
SET
 ConsumerCompletedD = COALESCE(@ConsumerCompletedD, ConsumerCompletedD )
, ReceivingFaxNumber = COALESCE(@ReceivingFaxNumber, ReceivingFaxNumber )
, BorrowerSignedEventId = COALESCE(@BorrowerSignedEventId, BorrowerSignedEventId )
, CoborrowerSignedEventId = COALESCE(@CoborrowerSignedEventId, CoborrowerSignedEventId )

WHERE ConsumerActionRequestId = @ConsumerActionRequestId

IF ( 0 != @@ERROR )
BEGIN
	RAISERROR('Error in updating CP_CONSUMER_ACTION_RESPONSE table in CP_UpdateActionResponse sp', 16, 1);
	RETURN -100;
END
RETURN 0;







