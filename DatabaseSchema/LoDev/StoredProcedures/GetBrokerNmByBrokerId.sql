ALTER  PROCEDURE [dbo].[GetBrokerNmByBrokerId]
@BrokerId uniqueidentifier

AS

select 
	BrokerNm, CustomerCode 
from 
	Broker 
where BrokerId = @BrokerId
