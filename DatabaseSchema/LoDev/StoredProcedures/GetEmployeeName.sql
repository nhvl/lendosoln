ALTER PROCEDURE [dbo].[GetEmployeeName]
	@EmployeeID uniqueidentifier
AS
	SELECT UserLastNm + ', ' + UserFirstNm AS FullName
	FROM Employee WITH (NOLOCK)
	WHERE EmployeeID = @EmployeeID
