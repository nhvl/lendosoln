-- =============================================
-- Author:		Geoff Feltman
-- Create date: 2018-09-04
-- Description:	Gets the application collection type for a loan file.
-- =============================================
CREATE PROCEDURE [dbo].[GetBorrowerApplicationCollectionT]
	@sLId UniqueIdentifier
AS
BEGIN
	SELECT sBorrowerApplicationCollectionT
	FROM LOAN_FILE_A
	WHERE sLId = @sLId
END
