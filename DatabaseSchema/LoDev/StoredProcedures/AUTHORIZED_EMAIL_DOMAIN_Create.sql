CREATE PROCEDURE [dbo].[AUTHORIZED_EMAIL_DOMAIN_Create]
	@BrokerId uniqueidentifier,
	@DomainName varchar(50)
AS
BEGIN
	INSERT INTO AUTHORIZED_EMAIL_DOMAIN (BrokerId, DomainName)
	VALUES (@BrokerId, @DomainName)
END