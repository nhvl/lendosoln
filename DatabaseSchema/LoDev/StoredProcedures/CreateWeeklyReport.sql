
-- =============================================
-- Author:		Long Nguyen
-- Create date: 8/4/2015
-- Description:	Update schedule report recovery (iOPM 215368): Add RunTime and RetryUntil
-- =============================================
CREATE PROCEDURE [dbo].[CreateWeeklyReport]
	@ReportId as int, 
	@Frequency as int, 
	@Weekday as varchar(9),
	@EffectiveDate as datetime, 
	@LastRun as datetime = NULL, 
	@NextRun as datetime, 
	@EndDate as datetime,
	@RunTime as datetime = NULL,
	@RetryUntil as datetime = NULL

as
BEGIN
	insert into WEEKLY_REPORTS 
	values( @ReportId, 
			@Frequency, 
			@Weekday,
			@EffectiveDate, 
			@LastRun, 
			@NextRun, 
			@EndDate,
			@RunTime,
			@RetryUntil)
END
