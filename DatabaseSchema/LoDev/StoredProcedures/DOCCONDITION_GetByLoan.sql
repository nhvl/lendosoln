-- =============================================
-- Author:		Matthew Pham
-- Create date: 9/19/2012
-- Description:	Get document-condition associations
--				by loan.
-- =============================================
CREATE PROCEDURE dbo.DOCCONDITION_GetByLoan 
	-- Add the parameters for the stored procedure here
	@LoanId uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT sLId, TaskId, DocumentId, Status
	FROM TASK_x_EDOCS_DOCUMENT
	WHERE sLId = @LoanId
END
