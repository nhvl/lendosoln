

CREATE PROCEDURE [dbo].[RetrieveBrokerPmlSiteIdByUserID] 
	@UserID uniqueidentifier
AS
	SELECT BrokerPmlSiteID FROM VIEW_ACTIVE_PML_USER_WITH_BROKER_INFO WITH (NOLOCK) WHERE UserID=@UserID


