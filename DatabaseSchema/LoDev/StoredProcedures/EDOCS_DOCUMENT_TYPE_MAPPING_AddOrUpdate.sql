ALTER PROCEDURE [dbo].[EDOCS_DOCUMENT_TYPE_MAPPING_AddOrUpdate]
    @SourceDocTypeId int,
    @EsignDocTypeId int = null
AS
BEGIN
UPDATE dbo.EDOCS_DOCUMENT_TYPE_MAPPING
    SET ESignTargetDocTypeId = @EsignDocTypeId
    WHERE SourceDocTypeId = @SourceDocTypeId

IF @@ROWCOUNT = 0 AND @EsignDocTypeId IS NOT NULL
BEGIN
    INSERT INTO dbo.EDOCS_DOCUMENT_TYPE_MAPPING
    (
        SourceDocTypeId,
        ESignTargetDocTypeId
    )
    VALUES
    (
        @SourceDocTypeId,
        @EsignDocTypeId
    )
END
END
