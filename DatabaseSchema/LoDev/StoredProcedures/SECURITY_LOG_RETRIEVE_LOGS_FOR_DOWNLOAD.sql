ALTER PROCEDURE [dbo].[SECURITY_LOG_RETRIEVE_LOGS_FOR_DOWNLOAD]
	@BrokerId uniqueidentifier,
	@EventType smallint = null,
	@UserId uniqueidentifier = null,
	@FromDate datetime2 = null,
	@ToDate datetime2 = null,
	@ClientIp varchar(50) = null,
	@MaxDownloadResults int
AS
BEGIN
	SELECT TOP (@MaxDownloadResults)
		CreatedDate,
		ClientIp,
		EventType,	
		UserId,
		DescriptionText,
		UserFirstNm,
		UserLastNm,
		LoginNm,
		IsInternalUserAction,
		IsSystemAction,
		PrincipalType
	FROM 
		SECURITY_LOG
	WHERE 
		BrokerId = @BrokerId AND
		(EventType= @EventType OR @EventType IS NULL) AND 
		(UserId = @UserId OR @UserId IS NULL) AND 
		(CreatedDate >= @FromDate OR @FromDate IS NULL) AND
		(CreatedDate <= @ToDate OR @ToDate IS NULl) AND
		(ClientIP LIKE '%' + @ClientIp + '%' OR @ClientIp IS NULL)		
END