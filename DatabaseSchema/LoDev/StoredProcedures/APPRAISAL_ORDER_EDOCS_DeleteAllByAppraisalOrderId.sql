-- =============================================
-- Author:		Eric Mallare
-- Create date: 3/30/2018
-- Description:	Deletes all Edoc assocations for an appraisal order.
-- =============================================
ALTER PROCEDURE [dbo].[APPRAISAL_ORDER_EDOCS_DeleteAllByAppraisalOrderId]
	@AppraisalOrderId uniqueidentifier,
	@LoanId uniqueidentifier,
	@BrokerId uniqueidentifier
AS
BEGIN
	DELETE FROM	
		APPRAISAL_ORDER_EDOCS
	WHERE
		AppraisalOrderId=@AppraisalOrderId AND
		LoanId=@LoanId AND
		BrokerId=@BrokerId
END