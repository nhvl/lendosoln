-- =============================================
-- Author:		Antonio Valencia
-- Create date: 7/8/11
-- Description:	Sets the consent date
-- =============================================
CREATE PROCEDURE CP_UpdateDisclosureStatus 
	-- Add the parameters for the stored procedure here
	@ConsumerId bigint 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	UPDATE CP_CONSUMER
	SET DisclosureAcceptedD = GETDATE()
	WHERE ConsumerId = @ConsumerId
END
