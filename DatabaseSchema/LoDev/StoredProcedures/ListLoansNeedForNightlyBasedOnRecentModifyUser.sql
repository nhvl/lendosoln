-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ListLoansNeedForNightlyBasedOnRecentModifyUser]
	@StartDate datetime,
	@EndDate DateTime
AS
BEGIN

SELECT LoanId
FROM
(
SELECT DISTINCT LoanId
FROM TASK t
WHERE t.TaskAssignedUserId IN
      (SELECT bu.UserId 
       FROM BROKER_USER bu 
       WHERE bu.LastModifiedDate BETWEEN @StartDate AND @EndDate)
      OR t.TaskOwnerUserId IN
      (SELECT bu.UserId 
       FROM BROKER_USER bu 
       WHERE bu.LastModifiedDate BETWEEN @StartDate AND @EndDate)
) temp JOIN LOAN_FILE_CACHE cache ON temp.LoanId = cache.sLId
       JOIN BROKER br ON br.BrokerId = cache.sBrokerId
WHERE
    cache.IsValid = 1 AND br.Status=1 AND br.IsUseNewCondition = 1 AND cache.IsTemplate=0




END
