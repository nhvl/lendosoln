CREATE PROCEDURE [dbo].[INTERNAL_USE_ONLY_ListEDocumentsWithStandardDocType]
as
select DocumentId, doc.BrokerId, doctype.DocTypeName, doctype.DocTypeId
from EDOCS_DOCUMENT doc join EDOCS_DOCUMENT_TYPE doctype with (nolock)
on
doc.DocTypeId = doctype.DocTypeId
and
doctype.brokerid is null
