-- =============================================
-- Author:		Michael Leinweaver
-- Create date: 6/16/2017
-- Description:	Retrieves the color theme ID of a PML broker.
-- =============================================
CREATE PROCEDURE [dbo].[RetrievePmlBrokerTpoColorThemeId]
	@BrokerId uniqueidentifier,
	@PmlBrokerId uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT TpoColorThemeId
	FROM PML_BROKER
	WHERE BrokerId = @BrokerId AND PmlBrokerId = @PmlBrokerId
END
