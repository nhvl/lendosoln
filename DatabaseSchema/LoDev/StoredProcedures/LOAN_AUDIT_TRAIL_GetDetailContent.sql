-- =============================================
-- Author:		Thien Nguyen
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[LOAN_AUDIT_TRAIL_GetDetailContent]
    @Id BigInt,
	@LoanId Guid,	-- for security reason
    @BrokerId Guid  -- for security reason
    
AS
BEGIN
    SELECT  DetailContentKey
    FROM LOAN_AUDIT_TRAIL
    WHERE Id = @Id AND BrokerId = @BrokerId AND LoanId = @LoanId 
END    
