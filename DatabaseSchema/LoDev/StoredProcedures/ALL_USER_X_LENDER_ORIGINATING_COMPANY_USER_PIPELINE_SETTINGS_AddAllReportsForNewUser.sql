-- =============================================
-- Author:		Michael Leinweaver
-- Create date: 6/8/2018
-- Description:	Adds all report settings for a new user.
-- =============================================
ALTER PROCEDURE [dbo].[ALL_USER_X_LENDER_ORIGINATING_COMPANY_USER_PIPELINE_SETTINGS_AddAllReportsForNewUser]
	@BrokerId uniqueidentifier,
	@UserId uniqueidentifier,
	@PortalMode tinyint
AS
BEGIN
	INSERT INTO ALL_USER_X_LENDER_ORIGINATING_COMPANY_USER_PIPELINE_SETTINGS(BrokerId, UserId, PortalMode, SettingId)
	SELECT @BrokerId, @UserId, @PortalMode, SettingId
	FROM LENDER_ORIGINATING_COMPANY_USER_PIPELINE_SETTINGS settings
	WHERE settings.BrokerId = @BrokerId
	ORDER BY settings.DisplayOrder
END
