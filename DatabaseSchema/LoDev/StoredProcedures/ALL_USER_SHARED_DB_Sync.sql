-- =============================================
-- Author:		David Dao
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE dbo.ALL_USER_SHARED_DB_Sync 
    @LoginNm varchar(50),
    @UserId UniqueIdentifier,
    @BrokerId UniqueIdentifier
AS
BEGIN

    UPDATE ALL_USER_SHARED_DB 
    SET LoginNm =@LoginNm
    WHERE UserId=@UserId AND BrokerId=@BrokerId
    
    IF @@ROWCOUNT=0
    BEGIN
    
        INSERT INTO ALL_USER_SHARED_DB (LoginNm, UserId, BrokerId)
        VALUES (@LoginNm, @UserId, @BrokerId)
    END

END
