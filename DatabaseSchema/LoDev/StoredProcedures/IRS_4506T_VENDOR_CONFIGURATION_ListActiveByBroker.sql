-- =============================================
-- Author:		David Dao
-- Create date: Aug 6, 2013
-- Description:	
-- =============================================
CREATE PROCEDURE dbo.IRS_4506T_VENDOR_CONFIGURATION_ListActiveByBroker
	@BrokerId UniqueIdentifier
AS
BEGIN

SELECT s.VendorID
FROM IRS_4506T_VENDOR_BROKER_SETTINGS s
WHERE s.BrokerId = @BrokerId

END
