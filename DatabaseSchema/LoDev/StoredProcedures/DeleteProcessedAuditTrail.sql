CREATE PROCEDURE DeleteProcessedAuditTrail 
	@LoanId Guid,
	@LastLogTime datetime
AS
DELETE FROM Audit_Trail_Daily
WHERE LoanId = @LoanId AND LogStartTime <= @LastLogTime
