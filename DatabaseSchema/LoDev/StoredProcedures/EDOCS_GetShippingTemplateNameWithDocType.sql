CREATE procedure [dbo].[EDOCS_GetShippingTemplateNameWithDocType]
	@BrokerId uniqueidentifier,
	@DocTypeId int
as
select shi.ShippingTemplateName, shi.ShippingTemplateId
from EDOCS_SHIPPING_TEMPLATE as shi 
	 JOIN EDOCS_DOCUMENT_TYPE_X_SHIPPING_TEMPLATE as tem on tem.ShippingTemplateId = shi.ShippingTemplateId
	 
where tem.DocTypeId = @DocTypeId
	and shi.BrokerId = @BrokerId
	 
order by shi.ShippingTemplateName



select * from EDOCS_SHIPPING_TEMPLATE