ALTER PROCEDURE [dbo].[GetUserTypeByByPassTicket]
	@ByPassTicket uniqueidentifier
AS
SELECT a.UserId, a.Type, br.BrokerId
from ALL_USER a
JOIN EMPLOYEE e ON a.UserId = e.EmployeeUserId
JOIN Branch br ON e.BranchId = br.BranchId
JOIN BROKER AS b ON b.BrokerId = br.BrokerId 
WHERE a.ByPassTicket = 	@ByPassTicket
AND b.status = 1