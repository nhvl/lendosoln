-- =============================================
-- Author:		Timothy Jewell
-- Create date:	05/23/2017
-- Description:	Creates an Employment record on a VOE order.  Be sure to wrap this in a transaction with the other inserts.
-- =============================================
ALTER PROCEDURE [dbo].[VOE_ORDER_EMPLOYMENT_Create]
	@OrderId int,
	@Id uniqueidentifier,
	@EmployerName varchar(100),
	@IsSelfEmployed bit
AS
BEGIN
	INSERT INTO [dbo].[VOE_ORDER_EMPLOYMENT]
		(OrderId, Id, EmployerName, IsSelfEmployed)
	VALUES
		(@OrderId, @Id, @EmployerName, @IsSelfEmployed)
END
GO
