

  CREATE PROCEDURE [dbo].[GetNumberBadPwAllowedLO]
	@BrokerId uniqueidentifier = '00000000-0000-0000-0000-000000000000'
AS
	SELECT NumberBadPasswordsAllowed
	FROM Broker
	WHERE BrokerId = @BrokerId
