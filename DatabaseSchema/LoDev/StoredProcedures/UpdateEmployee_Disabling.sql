﻿ALTER PROCEDURE [dbo].[UpdateEmployee_Disabling]
	@EmployeeID uniqueidentifier,
	@BrokerID uniqueidentifier,
	@BranchID uniqueidentifier,
	@FirstName Varchar( 21 ),
	@MiddleName Varchar ( 21 ) = '',
	@LastName Varchar( 21 ),
	@Suffix Varchar( 10 ) = '',
	@NmOnLoanDocs Varchar( 100 ) = '',
	@NmOnLoanDocsLckd Bit = false,
	@Address Varchar( 60 ),
	@City Varchar( 36 ),
	@State char( 2 ),
	@Zipcode char( 5 ),
	@Phone Varchar( 21 ),
	@Fax Varchar( 21 ),
	@CellPhone Varchar( 21 ) = '',
	@Pager Varchar( 21 ) = '',
	@LoginName Varchar( 36 ),
	@PasswordHash Varchar( 200 ),
	@Email Varchar( 80 ),
	@IsActive Bit,
	@EncryptionKeyId uniqueidentifier = NULL,
	@ManagerEmployeeID uniqueidentifier,
	@LenderAccExecEmployeeID uniqueidentifier,
	@LockDeskEmployeeID uniqueidentifier,
	@UnderwriterEmployeeID uniqueidentifier,
	@ProcessorEmployeeID uniqueidentifier,
	@PmlExternalManagerEmployeeId uniqueidentifier,
	@PmlSiteID uniqueidentifier,
	@LpePriceGroupID uniqueidentifier,
	@IsPmlManager Bit,
	@NewOnlineLoanEventNotifOptionT int,
	@TaskRelatedEmailOptionT int,
	@WhoDoneIt uniqueidentifier = NULL,
	@PasswordExpirationD SmallDateTime = NULL,
	@PasswordHistory Text = NULL,
	@AllowLogin Bit,
	@PasswordExpirationPeriod int = 0,
	@EmployeeLicenseNumber Varchar( 30 ),
	@NotesByEmployer Text = '',
	@UserType Varchar( 1 ) = "B",
	@CommissionPointOfLoanAmount decimal(9,3) = 0,
	@CommissionPointOfGrossProfit decimal(9,3) = 0,
	@CommissionMinBase decimal(9,2) = 0,
	@IsSharable bit = false,
	@LicenseXmlContent text = '',
	@DUAutoLoginNm Varchar( 50 ),
	@DUAutoPassword Varchar( 50 ) = null,
	@EncryptedDUAutoPassword varbinary(50) = null,
	@DOAutoLoginNm Varchar( 50 ),
	@DOAutoPassword Varchar( 50 ) = null,
	@EncryptedDOAutoPassword varbinary(50) = null,
	@LpeRsExpirationByPassPermission Varchar(100),
	@MustLogInFromTheseIpAddresses varchar(200) = null,
	@IsQuickPricerEnabled bit = NULL,
	@AnonymousQuickPricerUserId uniqueidentifier = null,
	@IsPricingMultipleAppsSupported bit = null,
	@PmlBrokerId UniqueIdentifier,
	@PipelineCustomReportSettingsXmlContent varchar(500) = null,
	@MclUserId int = null,
	@NmLsIdentifier Varchar( 50 ) = '',
	@PmlLevelAccess int,
	@BrokerProcessorEmployeeId uniqueidentifier,
	@IsUserTPO bit = 0,
    @OriginatorCompensationAuditXml varchar(max),
    @OriginatorCompensationPercent decimal(9, 3),
    @OriginatorCompensationBaseT int,
    @OriginatorCompensationMinAmount money,
    @OriginatorCompensationMaxAmount money,
    @OriginatorCompensationFixedAmount money,
    @OriginatorCompensationNotes varchar(200),
    @OriginatorCompensationLastModifiedDate datetime = null,
	@OriginatorCompensationSetLevelT int,
	@IsNewPmlUIEnabled bit = null,
	@EmployeeIDInCompany varchar(30) = null,
	@AssignedPrintGroups varchar(max) = null,
	@EmployeeStartD smalldatetime = null,
	@EmployeeTerminationD smalldatetime = null,
	@JuniorProcessorEmployeeID uniqueidentifier,
	@JuniorUnderwriterEmployeeID uniqueidentifier,
	@ShowQMStatusInPml2 bit = null,
	@CustomPricingPolicyField1Fixed varchar(25),
	@CustomPricingPolicyField2Fixed varchar(25),
	@CustomPricingPolicyField3Fixed varchar(25),
	@CustomPricingPolicyField4Fixed varchar(25),
	@CustomPricingPolicyField5Fixed varchar(25),
	@CustomPricingPolicyFieldSource int,
	@Lender_TPO_LandingPageConfigId uniqueidentifier = null,
	@PMLNavigationPermissions varchar(max),
	@IsActiveDirectoryUser bit = null,
	@LoanOfficerAssistantEmployeeID uniqueidentifier,
	@ChumsID varchar(8),
	@EnabledIpRestriction bit = false,
	@EnabledMultiFactorAuthentication bit = true,
	@EnabledClientDigitalCertificateInstall bit = false,
	@CanBeMemberOfMultipleTeams bit = 0,
	@UseOriginatingCompanyBranchId bit = 0,
	@UseOriginatingCompanyLpePriceGroupId bit = 0,
	@MiniCorrManagerEmployeeId uniqueidentifier = NULL,
	@MiniCorrProcessorEmployeeId uniqueidentifier = NULL,
	@MiniCorrJuniorProcessorEmployeeId uniqueidentifier = NULL,
	@MiniCorrLenderAccExecEmployeeId uniqueidentifier = NULL,
	@MiniCorrExternalPostCloserEmployeeId uniqueidentifier = NULL,
	@MiniCorrUnderwriterEmployeeId uniqueidentifier = NULL,
	@MiniCorrJuniorUnderwriterEmployeeId uniqueidentifier = NULL,
	@MiniCorrCreditAuditorEmployeeId uniqueidentifier = NULL,
	@MiniCorrLegalAuditorEmployeeId uniqueidentifier = NULL,
	@MiniCorrLockDeskEmployeeId uniqueidentifier = NULL,
	@MiniCorrPurchaserEmployeeId uniqueidentifier = NULL,
	@MiniCorrSecondaryEmployeeId uniqueidentifier = NULL,
	@CorrManagerEmployeeId uniqueidentifier = NULL,
	@CorrProcessorEmployeeId uniqueidentifier = NULL,
	@CorrJuniorProcessorEmployeeId uniqueidentifier = NULL,
	@CorrLenderAccExecEmployeeId uniqueidentifier = NULL,
	@CorrExternalPostCloserEmployeeId uniqueidentifier = NULL,
	@CorrUnderwriterEmployeeId uniqueidentifier = NULL,
	@CorrJuniorUnderwriterEmployeeId uniqueidentifier = NULL,
	@CorrCreditAuditorEmployeeId uniqueidentifier = NULL,
	@CorrLegalAuditorEmployeeId uniqueidentifier = NULL,
	@CorrLockDeskEmployeeId uniqueidentifier = NULL,
	@CorrPurchaserEmployeeId uniqueidentifier = NULL,
	@CorrSecondaryEmployeeId uniqueidentifier = NULL,
	@CorrespondentLandingPageID uniqueidentifier = NULL,
	@MiniCorrespondentLandingPageID uniqueidentifier = NULL,
	@UseOriginatingCompanyCorrPriceGroupId bit = 0,
	@CorrespondentPriceGroupId uniqueidentifier = NULL,
	@UseOriginatingCompanyMiniCorrPriceGroupId bit = 0,
	@MiniCorrespondentPriceGroupId uniqueidentifier = NULL,
	@UseOriginatingCompanyCorrBranchId bit = 0,
	@CorrespondentBranchId uniqueidentifier = NULL,
	@UseOriginatingCompanyMiniCorrBranchId bit = 0,
	@MiniCorrespondentBranchId uniqueidentifier = NULL,
	@PortalMode int = 0,
	@SupportEmail varchar(320) = null,
	@SupportEmailVerified bit = null,
	@CanContactSupport bit = 0,
	@FavoritePageIdsJSON varchar(max) = '',
	@IsUpdatedPassword bit,
	@PasswordSalt varchar(128),
	@OptsToUseNewLoanEditorUI bit = null,
	@PopulateBrokerRelationshipsT int = NULL,
	@PopulateMiniCorrRelationshipsT int = NULL,
	@PopulateCorrRelationshipsT int = NULL,
	@PopulatePMLPermissionsT int = NULL,
	@UserTheme int = NULL,
	@IsCellphoneForMultiFactorOnly bit = NULL,
	@EnableAuthCodeViaSms bit = NULL,
	@OriginatorCompensationIsOnlyPaidForFirstLienOfCombo bit = NULL,
	@EncryptedMfaOtpSeed varbinary(200) = NULL,
	@AUEncryptionKeyId uniqueidentifier = null
	
AS
declare @tranPoint sysname
	set @tranPoint = object_name(@@procId) + CAST(@@nestlevel as varchar(10))
	--print @tranPoint
	
	BEGIN TRANSACTION
	SAVE TRANSACTION @tranPoint
	-- 9/12/2002 - IsActive is present in both All_User and Employee table, therefore need to update in two places. dd.
	-- 7/30/2004 kb - We now maintain a "Price My Loan" Site Id for LO users.
	-- 8/10/2004 kb - We added a lender account executive employee relationship.
	-- 10/6/2004 kb - We added actual broker name for PML/'P' employees.
	-- 10/21/2004 kb - We added lock desk relationship.
	-- 6/1/2005 kb - Added manager relationship too.
	-- 7/1/2005 kb - We now support "Price My Loan" managers.
	-- 8/5/2005 kb - Adding lpe's price group setting (empty -> null).


IF @LpePriceGroupId IS NOT NULL AND @LpePriceGroupId = '00000000-0000-0000-0000-000000000000'
		SET @LpePriceGroupId = NULL
	UPDATE
		Broker_User
	SET
		PmlSiteID = @PmlSiteID,
		LpePriceGroupID = @LpePriceGroupID,
		PmlExternalManagerEmployeeID = @PmlExternalManagerEmployeeID,
		ManagerEmployeeID = @ManagerEmployeeID,
		LenderAccExecEmployeeID = @LenderAccExecEmployeeID,
		LockDeskEmployeeID = @LockDeskEmployeeID,
		UnderwriterEmployeeID = @UnderwriterEmployeeID,
		ProcessorEmployeeId = @ProcessorEmployeeID,
		DUAutoLoginNm = @DUAutoLoginNm,
		DUAutoPassword = COALESCE(@DUAutoPassword, DUAutoPassword),
		EncryptedDUAutoPassword = COALESCE(@EncryptedDUAutoPassword, EncryptedDUAutoPassword),
		DOAutoLoginNm = @DOAutoLoginNm,
		DOAutoPassword = COALESCE(@DOAutoPassword, DOAutoPassword),
		EncryptedDOAutoPassword = COALESCE(@EncryptedDOAutoPassword, EncryptedDOAutoPassword),
		LpeRsExpirationByPassPermission = @LpeRsExpirationByPassPermission,
		MustLogInFromTheseIpAddresses= COALESCE( @MustLogInFromTheseIpAddresses , MustLogInFromTheseIpAddresses ),
		IsQuickPricerEnabled = COALESCE(@IsQuickPricerEnabled, IsQuickPricerEnabled),
		AnonymousQuickPricerUserId = NULLIF( COALESCE(@AnonymousQuickPricerUserId, AnonymousQuickPricerUserId), '00000000-0000-0000-0000-000000000000' ),
		IsPricingMultipleAppsSupported = COALESCE(@IsPricingMultipleAppsSupported, IsPricingMultipleAppsSupported),
		PipelineCustomReportSettingsXmlContent = COALESCE(@PipelineCustomReportSettingsXmlContent, PipelineCustomReportSettingsXmlContent),
		PmlBrokerId = @PmlBrokerId,
		PmlLevelAccess = @PmlLevelAccess,
		BrokerProcessorEmployeeId = @BrokerProcessorEmployeeId,
		LastModifiedDate = GETDATE(),
		IsNewPmlUIEnabled = COALESCE(@IsNewPmlUIEnabled, IsNewPmlUIEnabled),
		AssignedPrintGroups = COALESCE(@AssignedPrintGroups, AssignedPrintGroups),
		JuniorProcessorEmployeeId = @JuniorProcessorEmployeeID,
		JuniorUnderwriterEmployeeID = @JuniorUnderwriterEmployeeID,
		ShowQMStatusInPml2 = COALESCE(@ShowQMStatusInPml2, ShowQMStatusInPml2),
		Lender_TPO_LandingPageConfigId = @Lender_TPO_LandingPageConfigId,
		PMLNavigationPermissions  = @PMLNavigationPermissions,
		LoanOfficerAssistantEmployeeID = @LoanOfficerAssistantEmployeeID,
		EnabledIpRestriction = @EnabledIpRestriction,
		EnabledMultiFactorAuthentication = @EnabledMultiFactorAuthentication,
		EnabledClientDigitalCertificateInstall = @EnabledClientDigitalCertificateInstall,
		UseOriginatingCompanyLpePriceGroupId = @UseOriginatingCompanyLpePriceGroupId,
		CorrespondentLandingPageID = @CorrespondentLandingPageID, 
		MiniCorrespondentLandingPageID = @MiniCorrespondentLandingPageID, 
		UseOriginatingCompanyCorrPriceGroupId = @UseOriginatingCompanyCorrPriceGroupId, 
		CorrespondentPriceGroupId = @CorrespondentPriceGroupId, 
		UseOriginatingCompanyMiniCorrPriceGroupId = @UseOriginatingCompanyMiniCorrPriceGroupId, 
		MiniCorrespondentPriceGroupId = @MiniCorrespondentPriceGroupId, 
		MiniCorrManagerEmployeeId = @MiniCorrManagerEmployeeId, 
		MiniCorrProcessorEmployeeId = @MiniCorrProcessorEmployeeId, 
		MiniCorrJuniorProcessorEmployeeId = @MiniCorrJuniorProcessorEmployeeId, 
		MiniCorrLenderAccExecEmployeeId = @MiniCorrLenderAccExecEmployeeId, 
		MiniCorrExternalPostCloserEmployeeId = @MiniCorrExternalPostCloserEmployeeId, 
		MiniCorrUnderwriterEmployeeId = @MiniCorrUnderwriterEmployeeId, 
		MiniCorrJuniorUnderwriterEmployeeId = @MiniCorrJuniorUnderwriterEmployeeId, 
		MiniCorrCreditAuditorEmployeeId = @MiniCorrCreditAuditorEmployeeId, 
		MiniCorrLegalAuditorEmployeeId = @MiniCorrLegalAuditorEmployeeId, 
		MiniCorrLockDeskEmployeeId = @MiniCorrLockDeskEmployeeId, 
		MiniCorrPurchaserEmployeeId = @MiniCorrPurchaserEmployeeId, 
		MiniCorrSecondaryEmployeeId = @MiniCorrSecondaryEmployeeId,
		CorrManagerEmployeeId = @CorrManagerEmployeeId, 
		CorrProcessorEmployeeId = @CorrProcessorEmployeeId, 
		CorrJuniorProcessorEmployeeId = @CorrJuniorProcessorEmployeeId, 
		CorrLenderAccExecEmployeeId = @CorrLenderAccExecEmployeeId, 
		CorrExternalPostCloserEmployeeId = @CorrExternalPostCloserEmployeeId, 
		CorrUnderwriterEmployeeId = @CorrUnderwriterEmployeeId, 
		CorrJuniorUnderwriterEmployeeId = @CorrJuniorUnderwriterEmployeeId, 
		CorrCreditAuditorEmployeeId = @CorrCreditAuditorEmployeeId, 
		CorrLegalAuditorEmployeeId = @CorrLegalAuditorEmployeeId, 
		CorrLockDeskEmployeeId = @CorrLockDeskEmployeeId,
		CorrPurchaserEmployeeId = @CorrPurchaserEmployeeId, 
		CorrSecondaryEmployeeId = @CorrSecondaryEmployeeId,
		PortalMode = @PortalMode,
		OptsToUseNewLoanEditorUI = COALESCE(@OptsToUseNewLoanEditorUI, OptsToUseNewLoanEditorUI),
		UserTheme = COALESCE(@UserTheme, UserTheme),
		EnableAuthCodeViaSms = COALESCE(@EnableAuthCodeViaSms, EnableAuthCodeViaSms)
	WHERE
		EmployeeID = @EmployeeID
	if(0!=@@error)
	begin
		RAISERROR('Error in updating Broker_User table in UpdateEmployee_Disabling sp', 16, 1);
		goto ErrorHandler;
	end
	DECLARE @PreviousAllowLogin Boolean
	DECLARE @vUserID uniqueidentifier
	DECLARE @vNewUserID uniqueidentifier
	SELECT @vUserID = b.UserID, @PreviousAllowLogin = IsActive 
	FROM Broker_User b JOIN All_User a ON b.UserID = a.UserID 
	WHERE b.EmployeeID = @EmployeeID
	IF @vUserID IS NULL
	BEGIN
		-- There is no broker employee record for this employee.
		SET @vNewUserID = NEWID()
	END
	ELSE 
	BEGIN
		SET @vNewUserID = @vUserID
	END
	IF @PreviousAllowLogin <> @AllowLogin 
	BEGIN
		-- This login account has change since last update. If @AllowLogin is true then record USER SUBSCRIBE event.
		DECLARE @EventType CHAR
		IF @AllowLogin = 1
		BEGIN
			SET @EventType = 'o'
		END
		ELSE 
		BEGIN
			SET @EventType = 'f'
		END
		INSERT INTO Usage_event(FeatureId, UserId, BrokerId,  WhoDoneIt, EventType) 
                           VALUES ('00000000-0000-0000-0000-000000000000', @vNewUserID, @BrokerId, @WhoDoneIt, @EventType)		
		IF( 0!=@@error )
		BEGIN
			RAISERROR('error in inserting into Usage_Event table in UpdateEmployee_Disabling sp', 16, 1);;
			goto ErrorHandler;
		END
	END

	IF @vUserID IS NULL AND @AllowLogin = 1
	BEGIN
		DECLARE @BrokerPmlSiteId uniqueidentifier
		SET @BrokerPmlSiteId = '00000000-0000-0000-0000-000000000000'
		IF @UserType <> 'B'
			SELECT @BrokerPmlSiteId = b.BrokerPmlSiteId FROM Broker AS b WHERE b.BrokerId = @BrokerId
		INSERT INTO All_User
			( UserID
			, LoginNm
			, PasswordHash
			, Type
			, IsActive
			, PasswordExpirationD
			, PasswordHistory
			, PasswordExpirationPeriod
			, IsSharable
			, BrokerPmlSiteId
			, IsUpdatedPassword
			, PasswordSalt
			, EncryptedMfaOtpSeed
			, AUEncryptionKeyId
			)
		VALUES
			( @vNewUserID
			, @LoginName
			, @PasswordHash
			, @UserType
			, @AllowLogin
			, @PasswordExpirationD
			, @PasswordHistory
			, @PasswordExpirationPeriod
			, @IsSharable
			, @BrokerPmlSiteId
			, @IsUpdatedPassword
			, @PasswordSalt
			, COALESCE(@EncryptedMfaOtpSeed, 0x)
			, @AUEncryptionKeyId
			);
			
		IF (0!=@@error) 
		BEGIN
			RAISERROR('Error in inserting into All_User table in UpdateEmployee_Disabling sp', 16, 1);
			goto ErrorHandler;
		END

		INSERT INTO Broker_User(PmlBrokerId,UserID, EmployeeID, LpePriceGroupId, StartD, UseOriginatingCompanyLpePriceGroupId, EncryptionKeyId) VALUES (@PmlBrokerId, @vNewUserID, @EmployeeID, @LpePriceGroupId, getDate(), @UseOriginatingCompanyLpePriceGroupId, @EncryptionKeyId);
		IF (0!=@@error) 
		BEGIN
			RAISERROR('Error in inserting into Broker_User table in UpdateEmployee_Disabling sp', 16, 1);
			goto ErrorHandler;
		END
	END
	ELSE 
	BEGIN
		UPDATE All_User 
            SET PasswordHash = COALESCE(@PasswordHash, PasswordHash),
            IsActive = @AllowLogin, 
            LoginNm = COALESCE(@LoginName, LoginNm),
			--02/16/12 mf. 78765. Cannot update user type.
			--Type = @UserType,
            PasswordExpirationD = @PasswordExpirationD, 
            PasswordHistory = @PasswordHistory,
			PasswordExpirationPeriod = @PasswordExpirationPeriod ,
		    IsSharable = @IsSharable,
			MclUserId = COALESCE(@MclUserId, MclUserId),
			SecurityQuestionsAttemptsMade = CASE @PasswordHash WHEN null THEN SecurityQuestionsAttemptsMade ELSE 0 END,
			IsActiveDirectoryUser = COALESCE(@IsActiveDirectoryUser, IsActiveDirectoryUser),
			IsUpdatedPassword = COALESCE(@IsUpdatedPassword, IsUpdatedPassword),
			PasswordSalt = COALESCE(@PasswordSalt, PasswordSalt), 
			EncryptedMfaOtpSeed = COALESCE(@EncryptedMfaOtpSeed, EncryptedMfaOtpSeed)
        WHERE UserID = @vNewUserID;
	END

UPDATE
		Employee
	SET
		BranchID = @BranchID,
		UserFirstNm = @FirstName,
		UserMiddleNm = @MiddleName,
		UserLastNm = @LastName,
		UserSuffix = @Suffix,
		UserNmOnLoanDocs = @NmOnLoanDocs,
		UserNmOnLoanDocsLckd = @NmOnLoanDocsLckd,
		Addr = @Address,
		City = @City,
		State = @State,
		Zip = @Zipcode,
		Phone = @Phone,
		Fax = @Fax,
		Email = @Email,
		IsActive = @IsActive,
		IsPmlManager = @IsPmlManager,
		NewOnlineLoanEventNotifOptionT = @NewOnlineLoanEventNotifOptionT,
		TaskRelatedEmailOptionT = @TaskRelatedEmailOptionT,
		NotesByEmployer = @NotesByEmployer,  
		EmployeeLicenseNumber = @EmployeeLicenseNumber,
		CommissionPointOfLoanAmount = @CommissionPointOfLoanAmount,
		CommissionPointOfGrossProfit = @CommissionPointOfGrossProfit,
		CommissionMinBase = @CommissionMinBase,
		CellPhone = @CellPhone,
		Pager = @Pager,
		LicenseXmlContent = @LicenseXmlContent,
		NmLsIdentifier = @NmLsIdentifier,
		IsUserTPO = @IsUserTPO,
		OriginatorCompensationAuditXml = @OriginatorCompensationAuditXml,
		OriginatorCompensationPercent = @OriginatorCompensationPercent,
		OriginatorCompensationBaseT = @OriginatorCompensationBaseT,
	 	OriginatorCompensationMinAmount = @OriginatorCompensationMinAmount,
	 	OriginatorCompensationMaxAmount = @OriginatorCompensationMaxAmount,
		OriginatorCompensationFixedAmount = @OriginatorCompensationFixedAmount,
		OriginatorCompensationNotes = @OriginatorCompensationNotes,
		OriginatorCompensationLastModifiedDate = @OriginatorCompensationLastModifiedDate,
		OriginatorCompensationSetLevelT = @OriginatorCompensationSetLevelT,
		EmployeeIDInCompany = COALESCE(@EmployeeIDInCompany, EmployeeIDInCompany),
		EmployeeStartD = @EmployeeStartD,
		EmployeeTerminationD = @EmployeeTerminationD,
		CustomPricingPolicyField1Fixed = @CustomPricingPolicyField1Fixed,
		CustomPricingPolicyField2Fixed = @CustomPricingPolicyField2Fixed,
		CustomPricingPolicyField3Fixed = @CustomPricingPolicyField3Fixed,
		CustomPricingPolicyField4Fixed = @CustomPricingPolicyField4Fixed,
		CustomPricingPolicyField5Fixed = @CustomPricingPolicyField5Fixed,
		CustomPricingPolicyFieldSource = @CustomPricingPolicyFieldSource,
		ChumsID = @ChumsID,
		CanBeMemberOfMultipleTeams = @CanBeMemberOfMultipleTeams,
		UseOriginatingCompanyBranchId = @UseOriginatingCompanyBranchId,
		UseOriginatingCompanyCorrBranchId = @UseOriginatingCompanyCorrBranchId, 
		CorrespondentBranchId = @CorrespondentBranchId, 
		UseOriginatingCompanyMiniCorrBranchId = @UseOriginatingCompanyMiniCorrBranchId, 
		MiniCorrespondentBranchId = @MiniCorrespondentBranchId,
		CanContactSupport = @CanContactSupport,
		FavoritePageIdsJSON = @FavoritePageIdsJSON,
		PopulateBrokerRelationshipsT = COALESCE(@PopulateBrokerRelationshipsT, PopulateBrokerRelationshipsT),
		PopulateMiniCorrRelationshipsT = COALESCE(@PopulateMiniCorrRelationshipsT, PopulateMiniCorrRelationshipsT),
		PopulateCorrRelationshipsT = COALESCE(@PopulateCorrRelationshipsT, PopulateCorrRelationshipsT),
		PopulatePMLPermissionsT = COALESCE(@PopulatePMLPermissionsT, PopulatePMLPermissionsT),
		IsCellphoneForMultiFactorOnly = COALESCE(@IsCellphoneForMultiFactorOnly, IsCellphoneForMultiFactorOnly),
		OriginatorCompensationIsOnlyPaidForFirstLienOfCombo = COALESCE(@OriginatorCompensationIsOnlyPaidForFirstLienOfCombo,OriginatorCompensationIsOnlyPaidForFirstLienOfCombo)
		
	WHERE
		EmployeeID = @EmployeeID AND EXISTS (SELECT 1 FROM Employee e, Branch b WHERE e.EmployeeID = @EmployeeID AND e.BranchID = b.BranchID AND b.BrokerID=@BrokerID)	

	if(0!=@@error)
	begin
		RAISERROR('Error in updating Employee table in UpdateEmployee_Disabling sp', 16, 1);
		goto ErrorHandler;
	end
	
	DELETE FROM PML_BROKER_RELATIONSHIPS
	WHERE EmployeeId = @EmployeeID
	if(0!=@@error)
	begin
		RAISERROR('Error in deleting from PML_BROKER_RELATIONSHIPS in UpdateEmployee_Disabling sp', 16, 1);
		goto ErrorHandler;
	end

COMMIT TRANSACTION
	RETURN 0;
ErrorHandler:
	ROLLBACK TRANSACTION @tranPoint
	ROLLBACK TRANSACTION
	RETURN -100;
