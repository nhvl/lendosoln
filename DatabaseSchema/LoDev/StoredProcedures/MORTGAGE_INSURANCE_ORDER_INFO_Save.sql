-- =============================================
-- Author:		Brian Beery
-- Create date: 09/19/2014
-- Description:	Inserts/updates an MI framework transaction record in the MORTGAGE_INSURANCE_ORDER_INFO table.
-- =============================================
ALTER PROCEDURE [dbo].[MORTGAGE_INSURANCE_ORDER_INFO_Save]
	-- Add the parameters for the stored procedure here
	@TransactionId uniqueidentifier,
	@sLId uniqueidentifier,
	@VendorId uniqueidentifier,
	@OrderNumber varchar(50),
	@OrderedDate smalldatetime,
	@ResponseXmlContent varchar(max),
	@MIPremiumType int,
	@PremiumRefundability int,
	@UFMIPFinanced bit,
	@LenderIdentifier varchar(20),
	@QuoteNumber varchar(20),
	@IsQuote bit,
	@OriginalQuoteOrderNumber varchar(50),
	@DelegationType tinyint,
	@UpfrontPremiumMismoValue varchar(14) = NULL
AS
BEGIN	
	UPDATE MORTGAGE_INSURANCE_ORDER_INFO
	SET
		TransactionId = @TransactionId,
		OrderedDate = @OrderedDate,
		ResponseXmlContent = @ResponseXmlContent,
		MIPremiumType = @MIPremiumType,
		PremiumRefundability = @PremiumRefundability,
		UFMIPFinanced = @UFMIPFinanced,
		LenderIdentifier = @LenderIdentifier,
		QuoteNumber = @QuoteNumber,
		IsQuote = @IsQuote,
		OriginalQuoteOrderNumber = @OriginalQuoteOrderNumber,
		DelegationType = @DelegationType,
		UpfrontPremiumMismoValue = @UpfrontPremiumMismoValue
	WHERE 
		sLId = @sLId 
		AND VendorId = @VendorId 
		AND OrderNumber = @OrderNumber
	
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO MORTGAGE_INSURANCE_ORDER_INFO
			(TransactionId, sLId, VendorId, OrderNumber, OrderedDate, ResponseXmlContent, MIPremiumType, PremiumRefundability, UFMIPFinanced, LenderIdentifier, QuoteNumber, IsQuote, OriginalQuoteOrderNumber, DelegationType, UpfrontPremiumMismoValue)
		VALUES
			(@TransactionId, @sLId, @VendorId, @OrderNumber, @OrderedDate, @ResponseXmlContent, @MIPremiumType, @PremiumRefundability, @UFMIPFinanced, @LenderIdentifier, @QuoteNumber, @IsQuote, @OriginalQuoteOrderNumber, @DelegationType, @UpfrontPremiumMismoValue)
	END
END