-- =============================================
-- Author:		Antonio Valencia
-- Create date: 2/28/11
-- Description:	Updates Empty Broker Origination Date
-- =============================================
CREATE PROCEDURE LOADMIN_UpdateEmptyOriginationMigrationDates 
	-- Add the parameters for the stored procedure here
	@MigrationDate datetime
AS
BEGIN
	UPDATE BROKER
	SET OriginatorCompensationMigrationDate = @MigrationDate
	WHERE OriginatorCompensationMigrationDate IS NULL
END
