-- =============================================
-- Author:		Eric Mallare
-- Create date: 9/6/2017
-- Description:	Adds a new UCD Fannie Mae delivery item
-- =============================================
ALTER PROCEDURE [dbo].[UCD_DELIVERY_FANNIE_MAE_Create]
	@UcdDeliveryId int,
	@BatchId varchar(50),
	@CasefileStatus int,
	@FindingsDocumentId uniqueidentifier,
	@FindingsXmlFileDbKey uniqueidentifier
AS
BEGIN
	INSERT INTO UCD_DELIVERY_FANNIE_MAE (
		UcdDeliveryId, 
		BatchId,
		CasefileStatus,
		FindingsDocumentId,
		FindingsXmlFileDbKey)
	VALUES (
		@UcdDeliveryId,
		@BatchId,
		@CasefileStatus,
		@FindingsDocumentId,
		@FindingsXmlFileDbKey)
END
