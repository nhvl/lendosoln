-- =============================================
-- Author:		Antonio Valencia
-- Create date: 9/3/2010
-- Description:	Saves configuration data for release
-- =============================================
ALTER PROCEDURE [dbo].[CONFIG_SaveRelease] 
		@ConfigurationXmlContent varchar(max),
		@OrgId uniqueidentifier,	
		@ModifyingUserId uniqueidentifier,
		@CompiledConfiguration varbinary(max),
	    @ReleaseDate datetime

AS
BEGIN
	SET NOCOUNT ON;
	
--	BEGIN TRAN 
--
--		IF @LatestReleaseDate IS NOT NULL 
--		BEGIN
--				IF ( SELECT TOP 1 ReleaseDate FROM CONFIG_RELEASED WHERE OrgId = @OrgId ) <> @LatestReleaseDate 
--				BEGIN
--					ROLLBACK TRAN
--					
--					RAISERROR('A release config was added since last load.', 16, 1); 
--
--					RETURN 
--				END
--		END
--		ELSE
--		BEGIN 
--			IF EXISTS(SELECT TOP 1 ReleaseDate FROM CONFIG_RELEASED WHERE OrgId = @OrgId )
--			BEGIN
--				ROLLBACK TRAN
--				RAISERROR('A release config was added since last load', 16, 1); 
--				RETURN 
--			END
--		END
--
--		BEGIN TRY
--				DECLARE @currentTime datetime
--				SET @CurrentTime = GETDATE() 
--				INSERT INTO CONFIG_RELEASED(ConfigurationXmlContent, OrgId, ModifyingUserId, ReleaseDate) 
--				VALUES( @ConfigurationXmlContent, @OrgId, @ModifyingUserId, @currentTime)
--				COMMIT TRAN
--				
--				SELECT @currentTime as LatestReleaseDate
--				
--		END TRY
--		BEGIN CATCH
--			RAISERROR('Failed to insert new config', 16, 1); 
--			Rollback TRAN
--		END CATCH
--			


--		DECLARE @currentTime datetime
--		SET @CurrentTime = GETDATE() 
		INSERT INTO CONFIG_RELEASED(ConfigurationXmlContent, OrgId, ModifyingUserId, ReleaseDate, CompiledConfiguration) 
		OUTPUT INSERTED.ConfigID
		VALUES( @ConfigurationXmlContent, @OrgId, @ModifyingUserId, @ReleaseDate, @CompiledConfiguration)		
			
END


