-- =============================================
-- Author:		paoloa
-- Create date: 4/26/2013
-- Description:	
-- =============================================
CREATE PROCEDURE dbo.getRSWarningMessageByBrokerPolicyId
	-- Add the parameters for the stored procedure here
	@Broker uniqueidentifier,
	@PolicyId uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT BrokerId, NormalUserExpiredCustomMsg, LockDeskExpiredCustomMsg, NormalUserCutOffCustomMsg, LockDeskCutOffCustomMsg, OutsideNormalHrCustomMsg, OutsideClosureDayHrCustomMsg, OutsideNormalHrAndPassInvestorCutOffCustomMsg
		from LENDER_LOCK_POLICY_CUSTOM_WARNING_MSGS
		where BrokerId = @Broker AND LockPolicyId = @PolicyId;
END
