ALTER PROCEDURE [dbo].[ASSET_RetrieveByLoanIdBrokerId]
	@LoanId UniqueIdentifier,
	@BrokerId UniqueIdentifier
AS
BEGIN
	SELECT
		AssetId,
		AccountName,
		AccountNum,
		AssetCashDepositType,
		AssetType,
		Attention,
		City,
		CompanyName,
		DepartmentName,
		[Desc],
		FaceValue,
		GiftSourceData,
		IsEmptyCreated,
		IsSeeAttachment,
		OtherTypeDesc,
		Phone,
		PrepDate,
		State,
		StreetAddress,
		Value,
		VerifExpiresDate,
		VerifRecvDate,
		VerifReorderedDate,
		VerifSentDate,
		VerifSignatureImgId,
		VerifSigningEmployeeId,
		Zip,
		IsCreditAtClosingData
	FROM [dbo].[ASSET]
	WHERE LoanId = @LoanId
		AND BrokerId = @BrokerId
END
