

ALTER PROCEDURE [dbo].[UpdateStageConstant] 
	@KeyIdStr varchar(100),
	@OptionContentInt int,
	@OptionContentStr varchar(1500),
	@Owner varchar(100)
AS
	UPDATE STAGE_CONFIG
	SET OptionContentInt = @OptionContentInt,
                     OptionContentStr = @OptionContentStr,
                     Owner = @Owner
             WHERE KeyIdStr = @KeyIdStr


