

CREATE PROCEDURE [dbo].[RetrieveUserIdByPmlLoginNameAndPmlBrokerId]
	@LoginName varchar(36),
	@PmlBrokerId uniqueidentifier
AS
SELECT UserId
FROM View_Active_Pml_User_With_Broker_Info
WHERE LoginNm = @LoginName AND PmlBrokerId = @PmlBrokerId


