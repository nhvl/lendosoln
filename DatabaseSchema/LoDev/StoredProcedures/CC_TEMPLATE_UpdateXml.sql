-- =============================================
-- Author:		Antonio Valencia
-- Create date: 8/8/2012
-- Description:	Updates the xml.. this is used by copy code.
-- =============================================
CREATE PROCEDURE [dbo].[CC_TEMPLATE_UpdateXml]
	@BrokerId uniqueidentifier,
	@DraftId int,
	@RuleXml varchar(max)
AS
BEGIN
	UPDATE TOP(1) CC_TEMPLATE_SYSTEM
	SET RuleXml = @RuleXml 
	WHERE BrokerId = @BrokerId AND Id = @DraftId 
END
