CREATE PROCEDURE Tmp_AdjustSCMEUser
	@brokerid uniqueidentifier
as
select bu.*
from broker_user bu join employee emp on bu.employeeId = emp.employeeId
	join branch bra on bra.branchid = emp.branchId
where bra.brokerid = @brokerId
update broker_user
set Permissions = substring( Permissions, 1, 11 ) + '1' + substring( permissions, 13, 88 ) 
from broker_user bu join employee emp on bu.employeeId = emp.employeeId
	join branch bra on bra.branchid = emp.branchId
where bra.brokerid = @brokerId
select bu.*
from broker_user bu join employee emp on bu.employeeId = emp.employeeId
	join branch bra on bra.branchid = emp.branchId
where bra.brokerid = @brokerId
