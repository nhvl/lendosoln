-- =============================================
-- Author:		paoloa
-- Create date: 4/16/2013
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[LockPolicy_Delete]
	-- Add the parameters for the stored procedure here
	@LockPolicyId uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM LENDER_LOCK_POLICY
	WHERE LockPolicyId = @LockPolicyId
END
