CREATE PROCEDURE CreateDiscNotif 
@NotifId uniqueidentifier,
@NotifAlertDate datetime = null,
@NotifUpdatedDate datetime = null,
@NotifLastReadD datetime = null,
@NotifIsRead bit,
@NotifIdSrc uniqueidentifier = null,
@DiscLogId uniqueidentifier = null,
@NotifReceiverUserId uniqueidentifier = null,
@NotifReceiverLoginNm varchar(36) = null,
@NotifBrokerId uniqueidentifier,
@NotifIsValid bit,
@NotifInvalidDate datetime = null
AS
if( @NotifUpdatedDate is null )
set @NotifUpdatedDate = getdate();
if( @DiscLogId is null )
begin
select @DiscLogId = DiscLogId
from Discussion_Notification
where notifId = @notifIdSrc;
if( @DiscLogId is null )
begin
RAISERROR('cannot find DiscLogId in Discussion_Notification table in CreateDiscNotif sp', 16, 1);
return;
end
end
if( @NotifReceiverLoginNm is null and @NotifReceiverUserId is not null )
begin
select @NotifReceiverLoginNm = LoginNm
from All_User WITH(NOLOCK)
where UserId = @NotifReceiverUserId
if( @NotifReceiverLoginNm is null )
begin
RAISERROR('cannot find LoginNm in All_User table in CreateDiscNotif sp', 16, 1);
return;
end
end
if( @NotifReceiverUserId is null and @NotifReceiverLoginNm is not null )
begin
select @NotifReceiverUserId = UserId
from All_User WITH(NOLOCK)
where LoginNm = @NotifReceiverLoginNm
if( @NotifReceiverUserId is null )
begin
RAISERROR('cannot find UserId in All_User table in CreateDiscNotif sp', 16, 1);
return;
end
end
if( @NotifReceiverLoginNm is null and @NotifReceiverUserId is null )
begin
RAISERROR('invalid parameters -- specify at least LoginNm or UserId in CreateDiscNotif sp', 16, 1);
return;
end
declare @DiscLastModifyD datetime
select @DiscLastModifyD = DiscLastModifyD
from Discussion_log
where DiscLogId = @DiscLogId
INSERT INTO DISCUSSION_NOTIFICATION
([NotifId], DiscLastModifyD, [NotifAlertDate], [NotifUpdatedDate], [NotifLastReadD], [NotifIsRead], [DiscLogId], [NotifReceiverUserId], [NotifReceiverLoginNm], [NotifBrokerId], [NotifIsValid], [NotifInvalidDate])
VALUES
(@NotifId, @DiscLastModifyD, @NotifAlertDate, @NotifUpdatedDate, @NotifLastReadD, @NotifIsRead, @DiscLogId, @NotifReceiverUserId, @NotifReceiverLoginNm, @NotifBrokerId, @NotifIsValid,@NotifInvalidDate)
