

---If MessageId is given then the message id is dropped from the database only if the queue id is algo given. The dataloc is returned in order to let the dev
--- remove the file also. If no message id all them are dropped. The Dataloc is returned of all the messages also if the message is not there nothing happens
CREATE PROCEDURE [dbo].[QDropArchiveMessage](
	@QueueId int, 
	@MessageId bigint = -1
)
AS




	IF @MessageId <> -1 
	BEGIN 
		SELECT DataLoc,Data FROM Q_ARCHIVE Where  MessageId = @MessageId  AND QueueId = @QueueId AND DataLoc = 'filedb';
		DELETE FROM Q_Archive WHERE MessageId = @MessageId AND QueueId = @QueueId;
	END
	ELSE
	BEGIN

		 SELECT DataLoc,Data FROM Q_ARCHIVE WHERE  QueueId = @QueueId AND DataLoc = 'filedb';
		DELETE FROM Q_ARCHIVE WHERE @QueueId = QueueId;
	END
	



