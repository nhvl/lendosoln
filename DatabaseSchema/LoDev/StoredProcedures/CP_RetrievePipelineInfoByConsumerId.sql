

-- =============================================
-- Author:		<Francesc>
-- Create date: <04/29/10>
-- Description:	<Retrieve loans associated with a given consumerId>
-- =============================================
CREATE PROCEDURE [dbo].[CP_RetrievePipelineInfoByConsumerId]
	@ConsumerId bigint
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		fc.sLId ,
		-- Keep the following set in sync with loan access info loading
		fc.sBrokerId ,
		fc.sBranchId ,
		fc.sLNm ,
		fc.sPrimBorrowerFullNm ,
		fc.sStatusT ,
		fc.sLienPosT ,
		fc.sOpenedD ,
		fc.sNoteIRSubmitted ,
		fc.IsTemplate ,
		fc.IsValid ,
		fc.sEmployeeManagerId ,
		fc.sEmployeeUnderwriterId ,
		fc.sEmployeeLockDeskId ,
		fc.sEmployeeProcessorId ,
		fc.sEmployeeLoanOpenerId ,
		fc.sEmployeeLoanRepId ,
		fc.sEmployeeLenderAccExecId ,
		fc.sEmployeeRealEstateAgentId ,
		fc.sEmployeeCallCenterAgentId ,
		fc.PmlExternalManagerEmployeeId ,
		fc.sEmployeeCloserId,
		fc.sEmployeeBrokerProcessorId,
		fc.sPmlBrokerId,
		fc.PmlExternalBrokerProcessorManagerEmployeeId,
		fc.sRateLockStatusT,
		-- end sync set
		fc.sSpAddr, fc.sSpCity, fc.sSpState, fc.sSpZip, sLAmtCalc
	FROM Loan_File_Cache fc INNER JOIN Loan_File_Cache_2 fc2
		ON fc.sLId = fc2.sLId
	INNER JOIN CP_CONSUMER_X_LOANID cid
		ON fc.sLId = cid.sLId
	WHERE cid.ConsumerId = @ConsumerId
		AND fc.IsValid = 1
		AND fc.IsTemplate = 0
	ORDER BY sOpenedD DESC
	if( 0!=@@error)
		begin
			RAISERROR('Error in the select statement in CP_RetrievePipelineInfoByConsumerId sp', 16, 1);
			return -100;
		end
		return 0;
END


