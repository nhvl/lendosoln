-- =============================================
-- Author:		Michael Leinweaver
-- Create date: 6/8/2018
-- Description:	Gets a custom report job by its internal job ID.
-- =============================================
ALTER PROCEDURE [dbo].[CUSTOM_REPORT_JOBS_GetJobByJobId]
	@JobId int
AS
BEGIN
	SELECT
		j.JobId,
		j.ProcessingStatus,
		j.NextRunDate,
		j.PublicJobId,
		j.DateCreated,
		j.ExpirationDate,
		j.LastRunStartDate,
		j.MaxExceptionCount,
		j.ExceptionCount,
		j.DateCompleted,
		j.LoanId,
		j.CorrelationId,
		c.QueryId,
		c.BrokerId,	-- 11/2/18 - je - For backwards compatibility we get BrokerId and UserId from CUSTOM_REPORT_JOBS table.
		c.UserId,	-- We should be able to change this in the future if necessary.
		c.UserType,
		c.PortalMode,
		c.AlternateSorting,
		c.ReportExtent,
		c.ReportRunType,
		c.UserLacksReportRunPermission,
		c.PollingIntervalInSeconds,
		c.ResultFileDbKey
	FROM
		CUSTOM_REPORT_JOBS c JOIN
		BACKGROUND_JOBS j ON c.JobId = j.JobId
	WHERE
		j.JobId = @JobId
END