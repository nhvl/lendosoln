

CREATE    PROCEDURE [dbo].[GetAccessControlBrokerInfo]
	@BrokerId uniqueidentifier
AS
	SELECT
		b.BrokerId ,
		b.BrokerNm ,
		b.CustomerCode ,
		b.BrokerPhone ,
		b.EmailAddrSendFromForNotif ,
		b.NameSendFromForNotif ,
		b.OptionTelemarketerCanOnlyAssignToManager ,
		b.OptionTelemarketerCanRunPrequal ,
		b.HasLenderDefaultFeatures ,
		b.IsLOAllowedToEditProcessorAssignedFile ,
		b.IsOnlyAccountantCanModifyTrustAccount ,
		b.IsOthersAllowedToEditUnderwriterAssignedFile ,
		b.IsAllowViewLoanInEditorByDefault ,
		b.NewUserDefault_CanRunLpeWOCreditReport ,
		b.NewUserDefault_RestrictWritingRolodex ,
		b.NewUserDefault_CannotDeleteLoan ,
		b.NewUserDefault_RestrictReadingRolodex ,
		b.NewUserDefault_CannotCreateLoanTemplate ,
		b.IsBlankTemplateInvisibleForFileCreation ,
		b.IsRoundUpLpeRateTo1Eighth ,
		b.IsRateLockedAtSubmission ,
		b.HasManyUsers ,
		b.LpeLockPeriodAdj ,
		b.BrokerPmlSiteId ,
		b.NotifRuleXmlContent ,
		b.Notes,
		b.BillingVersion
		
	FROM
		Broker AS b
	WHERE
		b.BrokerId = @BrokerId


