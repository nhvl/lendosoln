-- =============================================
-- Author:		Antonio Valencia
-- Create date: 9/3/2010
-- Description:	Saves new draft config
-- =============================================
CREATE PROCEDURE CONFIG_SaveDraft
	@OrgId uniqueidentifier,
	@ConfigurationXmlContent varchar(max)

AS
BEGIN
	SET NOCOUNT ON;

	IF ( EXISTS( SELECT * FROM CONFIG_DRAFT WHERE OrgId = @OrgId )  ) 
		BEGIN 
			UPDATE CONFIG_DRAFT 
			SET ConfigurationXmlContent = @ConfigurationXmlContent
			WHERE OrgId = @OrgId 
		END 
	ELSE
		BEGIN
			INSERT INTO CONFIG_DRAFT(OrgId, ConfigurationXmlContent) 
			VALUES( @OrgID,  @ConfigurationXmlContent )
		END 
		
END
