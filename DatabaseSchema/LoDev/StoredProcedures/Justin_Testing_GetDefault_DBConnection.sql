-- =============================================
-- Author:		Justin Jia
-- ALTER date: June 14, 2012
-- Description:	Gets all unique indexes and its corresponding tables and columns.
-- =============================================
CREATE PROCEDURE [dbo].[Justin_Testing_GetDefault_DBConnection]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT   	id
	FROM		DB_CONNECTION	
	WHERE		Description like '%main%'
	
	SELECT   	top 1 id
	FROM		DB_CONNECTION	
END
