-- =============================================
-- Author:		Matthew Pham
-- Create date: 9/7/11
-- Description:	Updates transactions for MBS trades
-- =============================================
CREATE PROCEDURE [dbo].[MBS_UpdateTransaction] 
	-- Add the parameters for the stored procedure here
	@TransactionId uniqueidentifier,
	@TradeId uniqueidentifier,
	@Date smalldatetime,
	@Amount money,
	@Price decimal(12,6),
	@IsGainLossCalculated bit,
	@GainLossAmount money,
	@Expense money,
	@Memo varchar(500),
	@Type int,
	@IsAmountCalculated bit,
	@LinkedTransaction uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE MBS_TRANSACTION
	SET
        TransactionId = @TransactionId,
        TradeId = @TradeId,
        Date = @Date,
        Amount = @Amount,
        Price = @Price,
        IsGainLossCalculated = @IsGainLossCalculated,
        GainLossAmount = @GainLossAmount,
        Expense = @Expense,
        Memo = @Memo,
		Type = @Type,
		IsAmountCalculated = @IsAmountCalculated,
		LinkedTransaction = @LinkedTransaction
	WHERE TransactionId = @TransactionId
END
