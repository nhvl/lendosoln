


CREATE PROCEDURE [dbo].[SetDataTracLoginNmByUserId]
	@UserId uniqueidentifier = '00000000-0000-0000-0000-000000000000',
	@DataTracLoginNm varchar(36) 
AS
	UPDATE Broker_User
	Set DataTracLoginNm = @DataTracLoginNm
	WHERE UserId = @UserId

