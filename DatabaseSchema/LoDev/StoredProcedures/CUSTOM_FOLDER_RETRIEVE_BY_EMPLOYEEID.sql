CREATE PROCEDURE [dbo].CUSTOM_FOLDER_RETRIEVE_BY_EMPLOYEEID
	@BrokerId uniqueidentifier,
	@EmployeeId uniqueidentifier
AS
BEGIN
	SELECT FolderId, EmployeeId, InternalName, ExternalName, LeadLoanStatus, SortOrder, ParentFolderId
	FROM CUSTOM_FOLDER
	WHERE BrokerId = @BrokerId AND EmployeeId = @EmployeeId
END
