-- =============================================
-- Author:		Antonio Valencia
-- Create date: 1/17/2012
-- Description:	Gets Q Size
-- =============================================
CREATE PROCEDURE Q_Count 
	-- Add the parameters for the stored procedure here
	@QueueId int
AS
BEGIN

	SET NOCOUNT ON;

    
	SELECT count(*) from Q_Message Where Queueid = @QueueId
END
