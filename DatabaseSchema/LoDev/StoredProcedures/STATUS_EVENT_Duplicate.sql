-- =============================================
-- Author:		Eduardo Michel
-- Create date: 
-- Description:	
-- =============================================
ALTER PROCEDURE [dbo].[STATUS_EVENT_Duplicate] 
	-- Add the parameters for the stored procedure here
	@sourceId uniqueidentifier, 
	@destinationId uniqueidentifier,
	@brokerId uniqueidentifier
AS
BEGIN
	INSERT INTO STATUS_EVENT (LoanId, StatusT, StatusD, Timestamp, IsExclude, BrokerId, UserName)
	SELECT @destinationId, StatusT, StatusD, Timestamp, IsExclude, BrokerId, UserName 
	FROM STATUS_EVENT
	WHERE LoanId = @sourceId AND BrokerId = @brokerId;
END
