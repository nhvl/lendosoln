CREATE PROCEDURE [dbo].[ToggleApprovalStatusRolodex]
	@RolodexID Guid
AS
UPDATE Agent 
SET AgentIsApproved=~AgentIsApproved
WHERE AgentID = @RolodexID

if(0!=@@error)
begin
	RAISERROR('Error in toggling approval status in Agent table in ToggleApprovalStatusRolodex', 16, 1);
	return -100;
end
return 0;
