-- =============================================
-- Author:		Antonio Valencia
-- Create date: 7/26/2012
-- Description: Creates the oldest release D 
-- =============================================
CREATE PROCEDURE dbo.CC_TEMPLATE_ManualTemplateCount
	@BrokerId uniqueidentifier
AS
BEGIN 
	SELECT COUNT(*)
	FROM CC_TEMPLATE Where BrokerId = @BrokerId AND CCTemplateSystemId is null 

	
END