-- =============================================
-- Author:		Geoff Feltman
-- Create date: 12/16/15
-- Description:	Loads all of the client certificates in a lender.
-- =============================================
ALTER PROCEDURE [dbo].[ALL_USER_CLIENT_CERTIFICATE_ListAllForBroker]
	@BrokerId uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT cc.CertificateId, cc.UserId, cc.Description, cc.CreatedDate, cc.CreatedBy, au.LoginNm AS CreatedByUserName
	FROM ALL_USER_CLIENT_CERTIFICATE cc
		JOIN EMPLOYEE emp on cc.UserId = emp.EmployeeUserId
		JOIN BRANCH bra on emp.BranchId = bra.BranchId
		JOIN BROKER bro on bro.BrokerId = bra.BrokerId
		JOIN ALL_USER au on cc.CreatedBy = au.UserId
	WHERE bro.BrokerId = @BrokerId
END
