



-- =============================================
-- Author:		Antonio Valencia
-- Create date: 02-06-08
-- Description:	Retrieves the loan limits for the 
-- given fips code. If it doesnt exist standard is returned.
-- =============================================
CREATE PROCEDURE [dbo].[GetFHALimitsForFips] 
	@FIPSCode int
AS
BEGIN
	DECLARE @rows int
	DECLARE @Limit1 money
	DECLARE @Limit2 money 
	DECLARE @Limit3 money 
	DECLARE @Limit4 money 
	DECLARE @FipsCountyCode int
	DECLARE @LastRevised datetime 
	DECLARE @EffectiveDate datetime

	SELECT @Limit1 = Limit1Units, @Limit2 = Limit2Units, @Limit3 = Limit3Units, @Limit4 = Limit4Units, @FipsCountyCode = FipsCountyCode, @LastRevised = LastRevised, @EffectiveDate = EffectiveDate from FHA_LOAN_LIMITS WHERE FipsCountyCode = @FIPSCode
	SELECT @rows = @@rowcount 
	IF @rows = 0 
	BEGIN 
		SELECT @Limit1 = Limit1Units, @Limit2 = Limit2Units, @Limit3 = Limit3Units, @Limit4 = Limit4Units, @FipsCountyCode = FipsCountyCode, @LastRevised = LastRevised, @EffectiveDate = EffectiveDate from FHA_LOAN_LIMITS WHERE FipsCountyCode = 0 
	END

	SELECT @Limit1 as Limit1Units, @Limit2 as Limit2Units, @Limit3 as Limit3Units, @Limit4 as Limit4Units, @FipsCountyCode as FipsCountyCode, @LastRevised as LastRevised, @EffectiveDate as EffectiveDate  
END




