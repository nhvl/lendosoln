-- Meant to retrieve information for revision history listing in UI.
-- Does not retrieve FeeTemplateXmlContent. This saves on bandwidth
-- when retrieving full history, as the XML data can be very large.
CREATE PROCEDURE [dbo].[FEE_SERVICE_RetrieveRevisionsForListing]
	@BrokerId uniqueidentifier
AS
BEGIN
	SELECT f.BrokerId, f.Comments, f.FileDbKey, f.UploadedByUserId, f.UploadedD,
		COALESCE(e.UserFirstNm + ' ' + e.UserLastNm, '') as UploadedByUserName, f.Id, f.IsTest
	FROM FEE_SERVICE_REVISION f LEFT JOIN EMPLOYEE e on f.UploadedByUserId = e.EmployeeUserId
	WHERE BrokerId = @BrokerId
END