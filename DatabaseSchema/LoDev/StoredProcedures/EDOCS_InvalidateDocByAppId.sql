

-- =============================================
-- Author:		Antonio Valencia
-- Create date: 1/8/10
-- Description:	Invalidates a EDOcument
-- =============================================
CREATE PROCEDURE [dbo].[EDOCS_InvalidateDocByAppId] 
	-- Add the parameters for the stored procedure here
	@oldAppId uniqueidentifier, 
	@slid uniqueidentifier,
	@UserId	uniqueidentifier = null,
	@Reason varchar(200)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	
	BEGIN TRANSACTION	
	
	-- For 137104 we want to reassign to the new primary app. av 8 29 2013
	DECLARE @newAppId uniqueidentifier 
	SET @newAppId = null
		
	-- Get the new primary app id
	SELECT top(1) @newAppId = aAppId from Application_A  where sLId = @slid and @oldAppId <> aAppId order by primaryrankindex

		
	--Update the audit history for invalid docs 
	INSERT INTO EDocs_Audit_History( DocumentId, ModifiedByUserId, ModifiedDate, Description )  
	SELECT DocumentId, @UserId, GETDATE(), @Reason FROM EDOCS_DOCUMENT 
	WHERE  aAppId = @oldAppId  and IsValid = 0 and @slid = slid
	IF @@error!= 0 GOTO HANDLE_ERROR
		
	
	-- Unlink invalid docs 
	UPDATE EDOCS_DOCUMENT SET 
	aAppId = @newAppId
	WHERE aAppId = @oldAppId  and IsValid = 0  and @slid = slid
	IF @@error!= 0 GOTO HANDLE_ERROR
	
	
	--copy all the valid  documents assigned to the  app being deleted into a temporary table  
	SELECT BrokerId, sLId, DocumentId, NumPages INTO #AppDocuments FROM EDOCS_DOCUMENT WHERE @oldAppId = aAppId AND BrokerId IS NOT NULL and @slid = slid and IsValid = 1
	IF @@error!= 0 GOTO HANDLE_ERROR
		
		
	
	--Update the audit history for the valid docs 
	INSERT INTO EDocs_Audit_History( DocumentId, ModifiedByUserId, ModifiedDate, Description )  
	SELECT DocumentId, @UserId, GETDATE(), @Reason FROM #AppDocuments 
	IF @@error!= 0 GOTO HANDLE_ERROR
	
	-- Finally update the valid docs 
	UPDATE EDOCS_DOCUMENT 
	SET aAppId = @newAppId
	WHERE aAppId = @oldAppId and @slid = slid
	IF @@error!= 0 GOTO HANDLE_ERROR
	
	
	-- And update any xml docs
	UPDATE EDOCS_GENERIC SET 
	aAppId = @newAppId
	WHERE aAppId = @oldAppId and @slid = slid

	COMMIT 
	RETURN

HANDLE_ERROR:
    ROLLBACK TRANSACTION
	RAISERROR ('Failure invalidating electronic document by appid.', 16, 1)
    RETURN 

END
