CREATE PROCEDURE [FetchAllGroups]
	@BrokerId uniqueidentifier,
	@GroupType int
AS
BEGIN
	SET NOCOUNT ON
	SELECT * FROM [GROUP] WHERE GroupType = @GroupType AND BrokerID=@BrokerId ORDER BY GroupName
END
