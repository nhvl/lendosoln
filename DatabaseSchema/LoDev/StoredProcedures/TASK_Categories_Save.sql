
-- =============================================
-- Author:		Antonio Valencia
-- Create date: 4/12/11
-- Description:	Saves Condition categories
-- =============================================
CREATE PROCEDURE [dbo].[TASK_Categories_Save] 
	-- Add the parameters for the stored procedure here
	@BrokerId uniqueidentifier, 
	@Id int, 
	@Category varchar(50),
	@IsDisplayAtTop bit = 0,
	@DefaultTaskPermissionLevelId  int
	
AS

-- REVIEW NOTES:
-- 6/9/2011: Reviewed. -ThinhNK

BEGIN
	DECLARE @nid  int
	SET @nid = @id 
	IF @Id  = -1 
	BEGIN
		INSERT  INTO CONDITION_CATEGORY( BrokerId, Category, DefaultTaskPermissionLevelId, IsDisplayAtTop )
		VALUES( @BrokerId, @Category, @DefaultTaskPermissionLevelId, @IsDisplayAtTop )
		SET @nid = SCOPE_IDENTITY(); 
	END
		

	ELSE
	BEGIN
		UPDATE CONDITION_CATEGORY 
		SET
			Category = @Category,
			DefaultTaskPermissionLevelId  = @DefaultTaskPermissionLevelId,
			IsDisplayAtTop = @IsDisplayAtTop
		WHERE
			BrokerId = @BrokerId AND
			ConditionCategoryId = @Id
	END

	select @nid
	
END

