-- =============================================
-- Author:		Eric Mallare
-- Create date: 5/22/2017
-- Description:	Creates a VOE service.
-- =============================================
ALTER PROCEDURE [dbo].[CreateVOEService]
	@ServiceT int,
	@IsADirectVendor bit,
	@SellsViaResellers bit,
	@IsAReseller bit,
	@VoeVerificationT int,
	@VendorId int,
	@IsEnabled bit,
	@AskForSalaryKey bit,
	@VendorServiceId int OUTPUT
AS
BEGIN
	INSERT INTO VOE_SERVICE
		(ServiceT, IsADirectVendor, SellsViaResellers, IsAReseller, VoeVerificationT, VendorId, 
		 IsEnabled, AskForSalaryKey)
	VALUES
		(@ServiceT, @IsADirectVendor, @SellsViaResellers, @IsAReseller, @VoeVerificationT, @VendorId,
		 @IsEnabled, @AskForSalaryKey)
	
	IF @@error!= 0 GOTO HANDLE_ERROR
	SET @VendorServiceId=SCOPE_IDENTITY();
	
	RETURN;
END

HANDLE_ERROR:
	RAISERROR('Error in CreateVOEService sp', 16, 1);;
	RETURN;

