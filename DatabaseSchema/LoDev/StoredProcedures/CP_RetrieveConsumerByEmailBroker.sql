



CREATE PROCEDURE [dbo].[CP_RetrieveConsumerByEmailBroker]
	@LoginName varchar(80),
	@BrokerId uniqueidentifier
AS

SELECT ConsumerId, Email, BrokerId 
FROM CP_CONSUMER
WHERE	BrokerId = @BrokerId	AND
		Email = @LoginName

