-- =============================================
-- Author:		Paolo Arrastia
-- Create date: 9 Aug 2012
-- Description:	
-- =============================================
ALTER PROCEDURE [dbo].[MortgagePoolNew] 
	-- Add the parameters for the stored procedure here
	@BrokerId UniqueIdentifier,
	@InternalId varchar(36),
	@EncryptionKeyId uniqueidentifier = null,
    @PublicId uniqueidentifier = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO MORTGAGE_POOL (
	BrokerId
	,AgencyT
	,BasePoolNumber
	,IssueTypeCode
	,PoolTypeCode
	,IssuerId
	,CustodianId
	,SecurityR
	,PoolTerm
	,AmortizationMethodT
	,AmortizationT
	,SecurityRMargin
	,PoolIndexT
	,IsBondFinancePool
	,CertAndAgreementT
	,IsFormHUD11711ASendToDocumentCustodian
	,IsEnabled
	,SearchMinimumStatusT
	,InternalId
	,EncryptionKeyId
	,TaxIdEncrypted
    ,PublicId
	)
VALUES (
	@BrokerId
	,0 --AgencyT
	,'' --BasePoolNumber
	,'' --IssueTypeCode
	,'' --PoolTypeCode
	,'' --IssuerId
	,'' --CustodianId
	,0 --SecurityR
	,0 --PoolTerm
	,0 --AmortizationMethodT
	,-1 --AmortizationT
	,0 --SecurityRMargin
	,0 --PoolIndexT
	,0 --IsBondFinancePool
	,0 --CertAndAgreementT
	,0 --IsFormHUD11711ASendToDocumentCustodian
	,1 --IsEnabled
	,4 --SearchMinimumStatusT
	,@InternalId
	,@EncryptionKeyId
	,null --TaxIdEncrypted
    ,COALESCE(@PublicId, NEWID())
	)

	SELECT SCOPE_IDENTITY() AS InsertedId
END
