-- =============================================
-- Author:		Justin Lara
-- Create date: 9/20/2018
-- Description:	Disables OCR for all branch
--				groups associated with a broker.
-- =============================================
CREATE PROCEDURE [dbo].[EDOCS_OCR_ENABLED_BRANCH_GROUPS_DeleteByBroker]
	@BrokerId uniqueidentifier
AS
BEGIN
	DELETE FROM EDOCS_OCR_ENABLED_BRANCH_GROUPS
	WHERE BrokerId = @BrokerId
END
