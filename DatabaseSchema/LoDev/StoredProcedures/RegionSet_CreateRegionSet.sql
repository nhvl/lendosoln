CREATE PROCEDURE [dbo].[RegionSet_CreateRegionSet]
	@BrokerId uniqueidentifier,
	@Name varchar(50),
	@Description varchar(100)
AS
BEGIN
	INSERT INTO REGION_SET (BrokerId, Name, [Description])
	VALUES (@BrokerId, @Name, @Description)
END