CREATE PROCEDURE [dbo].[CreateBlankCCTemplate]
	@BrokerId uniqueidentifier,
	@UseThisId uniqueidentifier = NULL,
	@CCTemplateSystemId int = null, --added for new closing automation feature. manual tempaltes dont ahve a cc_template_system
	@GfeVersion int,
              @cCCTemplateId uniqueidentifier OUT
AS
declare @tranPoint sysname
set @tranPoint = object_name(@@procId) + CAST(@@nestlevel as varchar(10))
--print @tranPoint
	
BEGIN TRANSACTION
SAVE TRANSACTION @tranPoint
IF( @UseThisId IS NOT NULL )
	SET @cCCTemplateId = @UseThisId;
ELSE
	SET @cCCTemplateId = newid();
INSERT INTO CC_TEMPLATE
( cCCTemplateId, BrokerId, GfeVersion, CCTemplateSystemId )
values
( @cCCTemplateId, @BrokerId, @GfeVersion,@CCTemplateSystemId )
if @@error != 0
begin
	RAISERROR('Error insert blank CC_TEMPLATE ', 16, 1);
	goto ErrorHandler;
end
commit transaction;
return 0;
ErrorHandler:
	rollback tran @tranPoint
	rollback transaction
	return -100;
