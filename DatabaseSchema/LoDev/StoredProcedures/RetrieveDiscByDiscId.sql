CREATE PROCEDURE RetrieveDiscByDiscId
	@DiscLogId uniqueidentifier
AS
select top 1 *
FROM discussion_log 
where disclogid = @DiscLogId and discisrefobjvalid = 1
order by discrefobjnm1 asc, discpriority asc
