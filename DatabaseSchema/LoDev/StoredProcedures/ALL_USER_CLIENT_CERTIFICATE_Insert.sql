-- =============================================
-- Author:		David Dao
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE dbo.ALL_USER_CLIENT_CERTIFICATE_Insert 
    @CertificateId UniqueIdentifier,
    @UserId UniqueIdentifier,
    @Description varchar(200),
    @CreatedBy UniqueIdentifier
AS
BEGIN

    INSERT INTO ALL_USER_CLIENT_CERTIFICATE (CertificateId, UserId, Description, CreatedBy)
                                     VALUES (@CertificateId, @UserId, @Description, @CreatedBy)

END
