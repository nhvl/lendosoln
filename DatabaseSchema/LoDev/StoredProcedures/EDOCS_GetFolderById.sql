CREATE PROCEDURE [dbo].[EDOCS_GetFolderById]
@FolderId int
as
	select * from edocs_folder f
	join EDOCS_FOLDER_X_ROLE r
	on f.folderid = r.folderid
	where f.FolderId = @FolderId