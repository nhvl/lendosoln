ALTER PROCEDURE [dbo].[CUSTOM_FOLDER_X_PAGE_SAVE]
	@CustomFolderXPage CustomFolderXPageTableType READONLY,
	@FolderIds IntTableType READONLY
AS
BEGIN
	WITH 
		CUSTOM_FOLDER_X_PAGE_PARTIAL AS
		(
			SELECT P.FolderId, P.PageId, P.SortOrder
			FROM CUSTOM_FOLDER_X_PAGE as p
			JOIN @FolderIds as c ON p.FolderId = c.IntValue
		)
	MERGE CUSTOM_FOLDER_X_PAGE_PARTIAL AS TARGET
	USING @CustomFolderXPage AS SOURCE
	ON (TARGET.FolderId = SOURCE.FolderId AND TARGET.PageId = SOURCE.PageId)		
		WHEN MATCHED AND TARGET.SortOrder != SOURCE.SortOrder
			THEN UPDATE SET 
				TARGET.SortOrder = SOURCE.SortOrder
		WHEN NOT MATCHED BY TARGET THEN 
			INSERT
			(
				FolderId,
				PageId,
				SortOrder
			)
			VALUES
			(
				SOURCE.FolderId,
				SOURCE.PageId,
				SOURCE.SortOrder
			)		
		WHEN NOT MATCHED BY SOURCE THEN
		DELETE;
END
