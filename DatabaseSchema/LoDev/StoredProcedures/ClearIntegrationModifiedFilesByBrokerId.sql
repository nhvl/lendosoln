ALTER PROCEDURE [dbo].[ClearIntegrationModifiedFilesByBrokerId] 
	@AppCode uniqueidentifier,
	@BrokerId uniqueidentifier,
	@LastModifiedD DateTime
AS
DELETE FROM Integration_File_Modified  WITH(ROWLOCK, READPAST, XLOCK) WHERE AppCode=@AppCode AND BrokerId = @BrokerId AND LastModifiedD <= @LastModifiedD
