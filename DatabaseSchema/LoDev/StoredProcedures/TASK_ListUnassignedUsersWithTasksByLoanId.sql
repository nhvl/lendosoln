ALTER PROCEDURE [dbo].[TASK_ListUnassignedUsersWithTasksByLoanId] 
	@LoanId uniqueidentifier,
	@BrokerId uniqueidentifier
AS

--REVIEW:
--6/9/2011: with(nohint)'s were removed to make sure the return set is consistent. 
--  Matt P is checking to see if we can filter out the inactive employees as they 
--  might fall into the orphanage scenarios and will be taken care of nightly. -ThinhNK

BEGIN
	SELECT 
		EmployeeUserId as UserId
		, UserFirstNm + ' ' + UserLastNm as UserName
	FROM Employee 
	WHERE 
		-- Assigned to a task in this loan
		EmployeeUserId IN (SELECT DISTINCT TaskAssignedUserId FROM Task WITH(INDEX(ix_TASK_LoanId_includes)) WHERE LoanId = @LoanId AND BrokerId = @BrokerId and CondIsDeleted = 0) 
	AND 
		-- Not assigned to this loan file directly
		EmployeeId NOT IN (SELECT DISTINCT employeeid FROM Loan_User_Assignment WHERE slid = @LoanId)
	AND
		-- Not a special user.
		EmployeeUserId NOT IN ('00000000-0000-0000-0000-000000000000', '11111111-1111-1111-1111-111111111111')

END
