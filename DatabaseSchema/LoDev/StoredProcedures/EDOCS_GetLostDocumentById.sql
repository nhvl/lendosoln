-- =============================================
-- Author:		Antonio Valencia
-- Create date: 1/25/10
-- Description:	Retrieve lost document by id
-- =============================================
ALTER PROCEDURE [dbo].[EDOCS_GetLostDocumentById] 
	-- Add the parameters for the stored procedure here
	@DocumentId uniqueidentifier 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SELECT  top 1    
			DocumentId, DocumentSource, sLId, b.BrokerId , e.DocTypeId, FaxNumber, FileDBKey_OriginalDocument, Description, NumPages, CreatedDate, b.BrokerNm, d.DocTypeName, IsESigned
			FROM EDOCS_LOST_DOCUMENT as e 
			LEFT OUTER JOIN broker as b on b.BrokerId = e.BrokerId 
			LEFT OUTER JOIN EDOCS_DOCUMENT_TYPE as D ON d.DocTypeId = e.DocTypeId
			where DocumentId = @DocumentId
END
