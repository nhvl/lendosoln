ALTER PROCEDURE  [dbo].[RetrieveCreditProxyByProxyID]
	@CrAccProxyId uniqueidentifier
AS
BEGIN
	SELECT
		CrAccProxyId,
		CraUserName,
		CraPassword,
		EncryptedCraPassword,
		CraAccId,
		CraId,
		IsExperianPulled,
		IsEquifaxPulled,
		IsTransUnionPulled,
		CrAccProxyDesc,
		IsFannieMaePulled,
		CreditMornetPlusPassword,
		EncryptedCreditMornetPlusPassword,
		CreditMornetPlusUserId,
		AllowPmlUserToViewReports,
		EncryptionKeyId
	FROM CREDIT_REPORT_ACCOUNT_PROXY
	WHERE CrAccProxyId = @CrAccProxyId
END
