-- =============================================
-- Author:		Geoffrey Feltman
-- Create date: 4/24/13
-- Description:	Retrieve the loans with e-consent that expired today.
-- 11/18/13 - The due date is now 3 days in the future, as opposed to 2. 
-- The migration is complete, so we can switch back to LOAN_FILE_CACHE.
-- =============================================
ALTER PROCEDURE [dbo].[ListLoansAwaitingEConsentDueToday]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    -- Get the DATE portion of today & tomorrow without the time.
    DECLARE @Today DATETIME
    SELECT @Today = DATEDIFF(DAY, 0, GETDATE())
    DECLARE @Tomorrow DATETIME
    SELECT @Tomorrow = DATEADD(DAY, 1, @Today)
    DECLARE @DayAfterTomorrow DATETIME
    SELECT @DayAfterTomorrow = DATEADD(DAY, 1, @Tomorrow)
    
    SELECT lfc.sLId
	FROM LOAN_FILE_CACHE lfc JOIN LOAN_FILE_CACHE_3 lfc3 ON lfc.sLId = lfc3.sLId
		JOIN BROKER b on lfc.sBrokerId = b.BrokerId
    WHERE sDisclosureNeededT = 15
		AND sDisclosuresDueD < @DayAfterTomorrow
		AND lfc.IsValid = 1
		AND b.Status = 1
		AND b.EnableEConsentMonitoringAutomation = 1
END
