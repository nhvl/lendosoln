-- =============================================
-- Author:		Scott Kibler
-- Create date: 7/11/2014
-- Description:	
--   Removes the loan from the pool
-- =============================================
CREATE PROCEDURE dbo.QUICKPRICER_2_LOAN_USAGE_RemoveFromPool
	@BrokerID UniqueIdentifier,
	@LoanID UniqueIdentifier,
	@UserIDofUsingUser UniqueIdentifier
AS
BEGIN
	DELETE FROM QUICKPRICER_2_LOAN_USAGE
    WHERE
         LoanID = @LoanID
     AND BrokerID = @BrokerID
     AND UserIDofUsingUser = @UserIDofUsingUser
    IF(1 <> @@RowCount)
	BEGIN
		RAISERROR('Could not remove loan from pool',16,1);
		RETURN -24;
	END
    
    RETURN 0;
END
