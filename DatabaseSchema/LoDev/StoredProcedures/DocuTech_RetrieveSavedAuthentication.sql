-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[DocuTech_RetrieveSavedAuthentication]
	@UserId UniqueIdentifier 
AS
BEGIN
	SELECT
		DocuTechUserName,
		DocuTechPassword,
		EncryptedDocuTechPassword,
		EncryptionKeyId
	FROM BROKER_USER
	WHERE UserId = @UserId
END
