CREATE procedure [dbo].[EDOCS_DeleteFolder]
	@FolderIdToDelete int,
	@FolderIdToAssignDocTypesTo int
as

BEGIN TRANSACTION

EXEC EDOCS_DeleteAllRoles @FolderIdToDelete
IF @@error!= 0 GOTO HANDLE_ERROR

UPDATE EDOCS_DOCUMENT_TYPE
    SET FolderId = @FolderIdToAssignDocTypesTo
    WHERE FolderId = @FolderIdToDelete
IF @@error!= 0 GOTO HANDLE_ERROR

delete from edocs_folder where folderId = @FolderIdToDelete
IF @@error!= 0 GOTO HANDLE_ERROR

COMMIT TRANSACTION
RETURN

HANDLE_ERROR:
    ROLLBACK TRANSACTION
    RETURN
