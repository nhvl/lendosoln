CREATE PROCEDURE [dbo].[ListArmIndexByBrokerId]
	@BrokerId uniqueidentifier

AS
	SELECT
		IndexIdGuid
		, BrokerIdGuid
		, IndexNameVstr
		, IndexCurrentValueDecimal
		, EffectiveD 
		, FreddieArmIndexT
		, FannieMaeArmIndexT
		, IndexBasedOnVstr
		, IndexCanBeFoundVstr
	FROM
		Arm_Index AS a
	WHERE
		a.BrokerIdGuid = @BrokerId
	ORDER BY
		a.IndexNameVstr
