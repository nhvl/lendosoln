-- =============================================
-- Author:		David Dao
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE dbo.ALL_USER_REGISTERED_IP_ListByUserId
    @BrokerId UniqueIdentifier,
    @UserId UniqueIdentifier
AS
BEGIN
    SELECT Id, BrokerId, UserId, IPAddress, Description, CreatedDate, IsRegistered
    FROM ALL_USER_REGISTERED_IP
    WHERE BrokerId=@BrokerId AND UserId=@UserId

END
