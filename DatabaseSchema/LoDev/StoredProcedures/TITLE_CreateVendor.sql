-- =============================================
-- Author:		Eric Mallare
-- Create date: 8/17/2017
-- Description:	Creates a new title vendor
-- =============================================
ALTER PROCEDURE [dbo].[TITLE_CreateVendor]
	@VendorId int OUTPUT,
	@CompanyName varchar(150),
	@IsEnabled bit,
	@IsTestVendor bit,
	@QuotingPlatformId int = null,
	@IsOverridingQuotingPlatform bit = null,
	@PolicyOrderingPlatformId int = null,
	@QuotingTransmissionId int = null,
	@IsOverridingPolicyOrderingPlatform bit = null,
	@PolicyOrderingTransmissionId int = null,
	@IsQuickQuotingEnabled bit,
	@IsDetailedQuotingEnabled bit,
	@IsPolicyOrderingEnabled bit,
	@UsesProviderCodes bit
AS
BEGIN
	INSERT INTO TITLE_FRAMEWORK_VENDOR
		(CompanyName, IsEnabled, IsTestVendor, QuotingPlatformId, IsOverridingQuotingPlatform,
		 PolicyOrderingPlatformId, QuotingTransmissionId, IsOverridingPolicyOrderingPlatform, PolicyOrderingTransmissionId,
		 IsQuickQuotingEnabled, IsDetailedQuotingEnabled, IsPolicyOrderingEnabled, UsesProviderCodes)
	VALUES
		(@CompanyName, @IsEnabled, @IsTestVendor, @QuotingPlatformId, @IsOverridingQuotingPlatform,
		 @PolicyOrderingPlatformId, @QuotingTransmissionId, @IsOverridingPolicyOrderingPlatform, @PolicyOrderingTransmissionId,
		 @IsQuickQuotingEnabled, @IsDetailedQuotingEnabled, @IsPolicyOrderingEnabled, @UsesProviderCodes)
		
	SET @VendorId=SCOPE_IDENTITY();
END
