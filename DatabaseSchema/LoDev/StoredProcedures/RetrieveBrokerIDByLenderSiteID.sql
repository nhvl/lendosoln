
CREATE PROCEDURE [dbo].[RetrieveBrokerIDByLenderSiteID] 
	@BrokerPmlSiteID uniqueidentifier
AS
SELECT BrokerID FROM Broker WITH (NOLOCK) WHERE BrokerPmlSiteID = @BrokerPmlSiteID