ALTER PROCEDURE  [dbo].[CreateBrokerCRA]
	@BrokerID Guid,
	@CreatedD datetime,
	@ModifiedD datetime,
	@ServiceComId Guid,
	@IsPdfViewNeeded bit = false
AS
	INSERT INTO CreditReportProtocol(OwnerID, CreatedD, ModifiedD, ServiceComId, IsPdfViewNeeded)
	VALUES (@BrokerID,  @CreatedD, @ModifiedD, @ServiceComId, @IsPdfViewNeeded)
