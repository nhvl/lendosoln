CREATE PROCEDURE RetrieveEmailByPmlLoginNameAndSiteId
	@LoginName varchar(36),
	@BrokerPmlSiteId uniqueidentifier,
	@CanLogin bit
AS
SELECT Email
FROM View_Active_Pml_User_With_Broker_Info
WHERE LoginNm = @LoginName 
	AND BrokerPmlSiteId = @BrokerPmlSiteId
	AND CanLogin = @CanLogin