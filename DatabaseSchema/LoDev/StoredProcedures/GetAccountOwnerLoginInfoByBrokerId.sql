



CREATE PROCEDURE [dbo].[GetAccountOwnerLoginInfoByBrokerId] 
	-- Add the parameters for the stored procedure here
	@BrokerId varchar(36)
AS
BEGIN
	SET NOCOUNT ON;

 	SELECT UserId, LoginNm, Type, PmlSiteId, UserFirstNm + ' ' + UserLastNm as UserFullNm, Email
	From VIEW_ACTIVE_LO_USER
	Where CanLogin = 1 AND IsActive = 1 AND IsAccountOwner = 1 AND BrokerId=@BrokerId
END




