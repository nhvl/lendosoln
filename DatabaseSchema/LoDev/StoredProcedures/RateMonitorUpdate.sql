-- =============================================
-- Author:		Paolo Arrastia
-- Create date: 24 Jul 2012
-- Description:	
-- =============================================
ALTER PROCEDURE [dbo].[RateMonitorUpdate] 
	-- Add the parameters for the stored procedure here
	@monitorid bigint,
	@rate decimal(9,3),
	@point decimal(9,3),
	@email varchar(200),
	@currDate DateTime,
	@doQP2FileUpdate bit,
	@rateMonitorScenarioName varchar(1024),
	@brokerID uniqueidentifier,
	@createdBy UniqueIdentifier,
	@sLId UniqueIdentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @ErrorCode int
	DECLARE @ErrorMsg varchar(1024)
	DECLARE @RowCount int
	
	SET @ErrorCode = -1
	SET @ErrorMsg = 'unknown'

	IF (@doQP2FileUpdate = 1)
	BEGIN
		BEGIN transaction
			UPDATE
				QUICKPRICER_2_LOAN_USAGE
			SET 
				RateMonitorName = @rateMonitorScenarioName
			WHERE 
					Loanid = @slid
				and Isinuse = 1
				and Brokerid = @BrokerID
				and Useridofusinguser = @createdBy
			SELECT @ErrorCode = @@Error, @RowCount = @@RowCount
			IF 0 <> @ErrorCode 
			BEGIN
				SET @ErrorMsg = 'An error occurred when updating the rate monitor name.';
				GOTO ErrorHandler;
			END
			IF 0 = @RowCount
			BEGIN
				SET @ErrorCode = -2
				SET @ErrorMsg = 'Could not find specified loan.'
				GOTO ErrorHandler;
			END	
			
			UPDATE RATE_MONITOR
			SET Rate = @rate, Point = @point, EmailAddresses = @email, CreatedDate = @currDate
			WHERE Id = @monitorid
			IF 0 <> @@Error
			BEGIN
				SET @ErrorMsg = 'An error occurred when updating the rate monitor.';
				GOTO ErrorHandler;
			END
			
		COMMIT transaction
		RETURN 0;
		ErrorHandler:
			ROLLBACK transaction
			RAISERROR(@ErrorMsg, 16, 1);
			RETURN @ErrorCode
	END
	ELSE
	BEGIN
		UPDATE RATE_MONITOR
		SET Rate = @rate, Point = @point, EmailAddresses = @email, CreatedDate = @currDate
		WHERE Id = @monitorid
    END	
END
