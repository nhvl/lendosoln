ALTER PROCEDURE [dbo].[TASK_CreateTask] 
	@BrokerId	uniqueidentifier
	, @TaskId	varchar(10) OUTPUT
	, @TaskAssignedUserId	uniqueidentifier = NULL
	, @TaskStatus	tinyint = 0
	, @LoanId	uniqueidentifier
	, @TaskOwnerUserId	uniqueidentifier = NULL
	, @TaskSubject	varchar(3000)
	, @TaskDueDate	smalldatetime = NULL
	, @TaskFollowUpDate	smalldatetime = NULL
	, @CondInternalNotes varchar(1000) = ''
	, @TaskIsCondition	bit
	, @TaskResolvedUserId	uniqueidentifier = NULL
	, @BorrowerNmCached	varchar(60) OUTPUT
	, @LoanNumCached varchar(36) OUTPUT

	, @TaskToBeAssignedRoleId	uniqueidentifier = NULL
	, @TaskToBeOwnerRoleId	uniqueidentifier = NULL
	, @TaskCreatedByUserId	uniqueidentifier
	, @TaskDueDateLocked	bit = 0
	, @TaskDueDateCalcField	varchar(max) = ''
	, @TaskDueDateCalcDays	int = 0
	, @TaskHistoryXml varchar(max)
	, @TaskPermissionLevelId int
	, @TaskClosedUserId	uniqueidentifier = NULL
	, @TaskClosedDate datetime = NULL
	, @CondCategoryId int = NULL
	, @CondIsHidden	bit = 0
	, @CondDeletedDate	datetime = NULL
	, @CondDeletedByUserNm	varchar(60) = ''
	, @CondIsDeleted	bit = 0
	, @TaskAssignedUserFullName varchar(60) = ''
	, @TaskOwnerFullName varchar(60) = ''
	, @TaskClosedUserFullName varchar(60) = ''
	, @CondRowId int = 0
	, @CondMessageId varchar(10) = ''
	, @CondRequiredDocTypeId int = null
	, @ResolutionBlockTriggerName varchar(200) = ''
	, @ResolutionDenialMessage varchar(1000) = ''
	, @ResolutionDateSetterFieldId varchar(100) = ''
	, @AssignToOnReactivationUserId uniqueidentifier = NULL

AS

-- REVIEW
-- 6/9/2011: Reviewed last week and improvements made to eliminate known deadlock issues and performance drag. -ThinhNK
-- 6/9/2011: I removed all transactions, they are not needed. -ThinhNK
-- 7/5/2012: Added static condition row IDs. -MP


	IF @TaskIsCondition = 1 
	BEGIN
		UPDATE LOAN_FILE_F 
		SET
			sConditionLastModifiedD = GETDATE()
		WHERE 
			sLId = @LoanId;
		
		IF @CondRowId = 0
		BEGIN
			SET @CondRowId = (SELECT MAX(CondRowId) FROM Task WHERE LoanId = @LoanId AND BrokerId = @BrokerId) + 1 -- This could be null if there are no tasks at all in the loan
		END
		
		IF @CondRowId IS NULL
		BEGIN
			SET @CondRowId = 1
		END
	END

	DECLARE @IsTemplate bit
	SELECT @LoanNumCached = sLNm, @BorrowerNmCached = sPrimBorrowerFullNm, @IsTemplate = IsTemplate from loan_file_cache with(nolock)
	WHERE slId = @LoanId
	
	INSERT INTO TASK_IDENTITY (DummyContent) values ('b')
	DECLARE @TaskIdentityId bigint
	SET @TaskIdentityId = SCOPE_IDENTITY()

	IF @IsTemplate = 0
		SET @TaskId = dbo.IntToNewBase( @TaskIdentityId, 22)
	ELSE 
		SET @TaskId = 'T' + dbo.IntToNewBase( @TaskIdentityId, 22)

 
	INSERT INTO TASK (
		BrokerId
		, TaskId
		, TaskAssignedUserId
		, TaskStatus
		, LoanId
		, TaskOwnerUserId
		, TaskSubject
		, TaskDueDate
		, TaskFollowUpDate
		, CondInternalNotes
		, TaskIsCondition
		, TaskResolvedUserId
		, BorrowerNmCached
		, LoanNumCached
		, TaskToBeAssignedRoleId
		, TaskToBeOwnerRoleId
		, TaskCreatedByUserId
		, TaskDueDateLocked
		, TaskDueDateCalcField
		, TaskDueDateCalcDays
		, TaskHistoryXml
		, TaskPermissionLevelId
		, TaskClosedUserId
		, TaskClosedDate
		, CondCategoryId
		, CondIsHidden
		, CondDeletedDate
		, CondDeletedByUserNm
		, CondIsDeleted
		, TaskAssignedUserFullName
		, TaskOwnerFullName
		, TaskClosedUserFullName
		, CondRowId
		, CondMessageId
		, CondRequiredDocTypeId
		, ResolutionBlockTriggerName
		, ResolutionDenialMessage
		, ResolutionDateSetterFieldId
		, AssignToOnReactivationUserId

		)
	VALUES (
		@BrokerId
		, @TaskId
		, @TaskAssignedUserId
		, @TaskStatus
		, @LoanId
		, @TaskOwnerUserId
		, @TaskSubject
		, @TaskDueDate
		, @TaskFollowUpDate
		, @CondInternalNotes
		, @TaskIsCondition
		, @TaskResolvedUserId
		, @BorrowerNmCached
		, @LoanNumCached
		, @TaskToBeAssignedRoleId
		, @TaskToBeOwnerRoleId
		, @TaskCreatedByUserId
		, @TaskDueDateLocked
		, @TaskDueDateCalcField
		, @TaskDueDateCalcDays
		, @TaskHistoryXml
		, @TaskPermissionLevelId
		, @TaskClosedUserId
		, @TaskClosedDate
		, @CondCategoryId
		, @CondIsHidden
		, @CondDeletedDate
		, @CondDeletedByUserNm
		, @CondIsDeleted
		, @TaskAssignedUserFullName
		, @TaskOwnerFullName
		, @TaskClosedUserFullName
		, @CondRowId
		, @CondMessageId
		, @CondRequiredDocTypeId
		, @ResolutionBlockTriggerName
		, @ResolutionDenialMessage
		, @ResolutionDateSetterFieldId
		, @AssignToOnReactivationUserId

	)

	IF 0 != @@ERROR
	BEGIN
		RAISERROR('Error inserting into TASK in CreateTask sp', 16, 1);
		rollback transaction
		return -100
	END

	RETURN 0;












