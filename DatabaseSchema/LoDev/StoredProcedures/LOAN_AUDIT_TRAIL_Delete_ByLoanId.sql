-- =============================================
-- Author:		Thien Nguyen
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[LOAN_AUDIT_TRAIL_Delete_ByLoanId]
	@LoanId Guid,
    @BrokerId Guid  -- for security reason
AS
BEGIN
	DELETE FROM LOAN_AUDIT_TRAIL
		  WHERE LoanId = @LoanId AND BrokerId = @BrokerId
END
