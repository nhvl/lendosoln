
ALTER  PROCEDURE [dbo].[DeclareNewLpeVersion] 
@SnapshotId varchar(100),
@SnapshotName varchar(100),
@DataLastModifiedD datetime,
@FileKey varchar(100)=''

AS
set transaction isolation level SERIALIZABLE 
begin transaction

insert into lpe_release
( SnapshotId, DataLastModifiedD, FileKey )
values
( @SnapshotId, @DataLastModifiedD, @FileKey )
commit transaction
return 0;



