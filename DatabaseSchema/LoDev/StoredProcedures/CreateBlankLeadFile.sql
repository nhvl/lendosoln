ALTER  PROCEDURE [dbo].[CreateBlankLeadFile] 
	@EmployeeID UniqueIdentifier,
	@BranchID UniqueIdentifier,
	@sLNm Varchar(36),
	@BrokerID UniqueIdentifier,
	@sTotalScoreUniqueLoanId varchar(20)='',
	@sUseGFEDataForSCFields bit = NULL,
	@sIsRequireFeesFromDropDown bit = 0,
	@sClosingCostFeeVersionT int = 0,
	@sIsHousingExpenseMigrated bit = 0,
	@LoanID UniqueIdentifier OUT,
	@sCFPBSetsVersionNumT int = 0,
	@sLoanVersionT int = 0,
	@sUse2016ComboAndProrationUpdatesTo1003Details bit = 0,
	@sAprCalculationT int = 0,
    @sLRefNm varchar(100) = '',
    @sLegalEntityIdentifier varchar(20) = '',
    @sCalculateInitialLoanEstimate bit = 1,
	@sCalculateInitialClosingDisclosure bit = 1,
	@sDataSchemaVersion int = NULL, -- TODO: Make this required.
	@sEncryptionKey uniqueidentifier = null,
	@sEncryptionMigrationVersion tinyint = 0,
	@sBorrowerApplicationCollectionT int = 0,
	@sAlwaysRequireAllBorrowersToReceiveClosingDisclosure bit = NULL
AS
--declare @tranPoint sysname
--set @tranPoint = object_name(@@procId) + CAST(@@nestlevel as varchar(10))
--print @tranPoint
	
--BEGIN TRANSACTION
--SAVE TRANSACTION @tranPoint
-- WILL CHECK FOR DB CONSISTENCY HERE FOR THE PARAMS
DECLARE @AppId UniqueIdentifier
if @@error != 0
begin
	RAISERROR('error in selecting from employee in CreateBlankLeadFile sp', 16, 1);
	goto ErrorHandler;
end

--set @LoanID = newid()	
--TRYING THIS OUT TO AVOID PERFORMANCE ISSUE. -thinh 3/24/2014
set @LoanId = cast( cast(NewID() as binary(10)) + cast(GetDate() as binary(6)) as uniqueidentifier)

INSERT INTO LOAN_FILE_A with (rowlock)
( sLId, IsTemplate, sLeadD, sStatusLckd, sDataSchemaVersion, sIsSellerProvidedBelowMktFin,sYrBuilt,sArmIndexT,sFannieSpT,sFannieDocT,sMldsPpmtMonMax,sFinMethT,sSubFinTerm,sSubFinIR,sDocPrepFProps,sU5DocStatN,sU4DocStatN,sU3DocStatN,sU2DocStatN,sU1DocStatN,sTilGfeN,sApprRprtN,sPrelimRprtN,sCcTemplateNm,sLpTemplateNm,DeletedD,sRAdjWorstIndex, sEncryptionKey, sEncryptionMigrationVersion, sBorrowerApplicationCollectionT)
VALUES( @LoanID, 0 , GETDATE(), 1, @sDataSchemaVersion, '' ,'',0,0,0,0,0,0,0,0,'','','','','','','','','','',0,0, @sEncryptionKey, @sEncryptionMigrationVersion, @sBorrowerApplicationCollectionT)
if @@error != 0
begin
	RAISERROR('error in inserting into Loan_File_A in CreateBlankLeadFile sp', 16, 1);
	goto ErrorHandler;
end
INSERT INTO LOAN_FILE_B with (rowlock)
( sLId, sStatusT,sBiweeklyPmt, sIsRequireFeesFromDropDown, sClosingCostFeeVersionT, sIsHousingExpenseMigrated,sCFPBSetsVersionNumT,sLoanVersionT,sUse2016ComboAndProrationUpdatesTo1003Details, sAprCalculationT, sLegalEntityIdentifier, sCalculateInitialLoanEstimate, sCalculateInitialClosingDisclosure) VALUES( @LoanID, 12,0, @sIsRequireFeesFromDropDown, @sClosingCostFeeVersionT, @sIsHousingExpenseMigrated,@sCFPBSetsVersionNumT,@sLoanVersionT,@sUse2016ComboAndProrationUpdatesTo1003Details, @sAprCalculationT, @sLegalEntityIdentifier, @sCalculateInitialLoanEstimate, @sCalculateInitialClosingDisclosure)
if @@error != 0
begin
	RAISERROR('error in inserting into Loan_File_B in CreateBlankLeadFile sp', 16, 1);;
	goto ErrorHandler;
end
INSERT INTO LOAN_FILE_C with (rowlock)
( sLId, sUseGFEDataForSCFields ) VALUES( @LoanID, @sUseGFEDataForSCFields )
if @@error != 0
begin
	RAISERROR('error in inserting into Loan_File_C in CreateBlankLeadFile sp', 16, 1);;
	goto ErrorHandler;
end
INSERT INTO LOAN_FILE_D with (rowlock)
( sLId,sRLckdNumOfDays ) VALUES( @LoanID,0 )
if @@error != 0
begin
	RAISERROR('error in inserting into Loan_File_D in CreateBlankLeadFile sp', 16, 1);;
	goto ErrorHandler;
end
INSERT INTO LOAN_FILE_E with (rowlock)
( sLId, sLNm, sBrokerId, sLRefNm ) VALUES( @LoanID,@sLNm,@BrokerID,@sLRefNm )
if @@error != 0
begin
	RAISERROR('error in inserting into Loan_File_E in CreateBlankLeadFile sp', 16, 1);;
	goto ErrorHandler;
end
-- OPM 72500 fhalenderid code updates from loan, branch, or broker.
DECLARE @fhalenderid varchar(10)
exec FhaLenderIDFromLoanOrBranchOrBroker @LoanID, @BranchID, @fhalenderid OUTPUT
if @@error != 0
begin
	RAISERROR('error in executing FhaLenderIDFromLoanOrBranchOrBroker sp', 16, 1);;
	goto ErrorHandler;
end
UPDATE LOAN_FILE_E with( rowlock )
SET sFhaLenderIdCode = @fhalenderid
WHERE slid = @LoanID
if @@error != 0
begin
	RAISERROR('error in updating fhalenderid in Loan_File_E in CreateBlankLeadFile sp', 16, 1);;
	goto ErrorHandler;
end
INSERT INTO LOAN_FILE_F with (rowlock)
( sLId, sBranchId,sTotalScoreUniqueLoanId,sAlwaysRequireAllBorrowersToReceiveClosingDisclosure ) 
VALUES( @LoanID,@BranchID,@sTotalScoreUniqueLoanId,@sAlwaysRequireAllBorrowersToReceiveClosingDisclosure )
if @@error != 0
begin
	RAISERROR('error in inserting into Loan_File_F in CreateBlankLeadFile sp', 16, 1);;
	goto ErrorHandler;
end
INSERT INTO TRUST_ACCOUNT with( rowlock )
( sLId )
VALUES( @LoanID )
if @@error != 0
begin
	RAISERROR('error in inserting into TRUST_ACCOUNT in CreateBlankLeadFile sp', 16, 1);;
	goto ErrorHandler;
end
INSERT INTO LOAN_FILE_CACHE with (rowlock)
(sLId, sBrokerId, sBranchId, sLNm, IsTemplate, IsValid)
values
(@LoanID, @BrokerID, @BranchID, @sLNm, 0,0)
if @@error != 0
begin
	RAISERROR('error in inserting into loan_file_cache in CreateBlankLeadFile sp', 16, 1);;
	goto ErrorHandler;
end
INSERT INTO LOAN_FILE_CACHE_2 with (rowlock)
(sLId)
values
(@LoanID)
if @@error != 0
begin
	RAISERROR('error in inserting into loan_file_cache_2 in CreateBlankLeadFile sp', 16, 1);;
	goto ErrorHandler;
end

INSERT INTO LOAN_FILE_CACHE_3 with (rowlock)
(sLId)
values
(@LoanID)
if @@error != 0
begin
	RAISERROR('error in inserting into loan_file_cache_3 in CreateBlankLeadFile sp', 16, 1);;
	goto ErrorHandler;
end

INSERT INTO LOAN_FILE_CACHE_4 with (rowlock)
(sLId, sLRefNm)
values
(@LoanID, @sLRefNm)
if @@error != 0
begin
	RAISERROR('error in inserting into loan_file_cache_4 in CreateBlankLeadFile sp', 16, 1);;
	goto ErrorHandler;
end

declare @retVal int
Exec @retVal  = CreateNewApplication @LoanID, @AppId output
if( 0!=@@error or 0!=@retVal )
begin
	RAISERROR('error in executing CreateNewApplication sp in CreateBlankLeadFile sp', 16, 1);;
	goto ErrorHandler;
end

IF @sBorrowerApplicationCollectionT <> 0
BEGIN
	DECLARE 
		-- All used as OUT parameters.
		@UladAppId UniqueIdentifier,
		@UladAppConsumerId UniqueIdentifier,
		@ConsumerId UniqueIdentifier,
		@IsPrimary Bit;
	EXEC CreateNewUladApplication @LoanId, @BrokerId, @AppId, @UladAppId, @UladAppConsumerId, @ConsumerId, @IsPrimary;
END
IF @@error != 0
BEGIN
	RAISERROR('Error executing CreateNewUladApplication sp in CreateBlankLeadFile sp', 16, 1);
	GOTO ErrorHandler;
END

--commit transaction;
return 0;
ErrorHandler:
	--rollback tran @tranPoint
	--rollback transaction
	return -100;
