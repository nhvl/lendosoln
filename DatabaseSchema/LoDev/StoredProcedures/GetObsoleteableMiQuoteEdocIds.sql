CREATE PROCEDURE [dbo].[GetObsoleteableMiQuoteEdocIds]
	@sLId uniqueidentifier,
	@BrokerId uniqueidentifier,
	@MiQuoteDocTypeId int
AS
BEGIN
	SELECT e.DocumentId
	FROM EDOCS_DOCUMENT e
	JOIN MORTGAGE_INSURANCE_ORDER_INFO_X_EDOCS_DOCUMENT oxe
	ON oxe.DocumentId = e.DocumentId
	JOIN MORTGAGE_INSURANCE_ORDER_INFO o
	ON oxe.OrderNumber = o.OrderNumber
	WHERE e.sLId = @sLId
	AND e.BrokerId = @BrokerId
	AND e.IsValid = 1
	AND e.DocTypeId = @MiQuoteDocTypeId
	AND (e.[Status] = 0 /*Blank*/ OR e.[Status] = 4 /*Screened*/)
	AND o.IsQuote = 1
	AND NOT EXISTS (
		SELECT 1
		FROM MORTGAGE_INSURANCE_ORDER_INFO o2
		WHERE o2.OriginalQuoteOrderNumber = o.OrderNumber
	)
END