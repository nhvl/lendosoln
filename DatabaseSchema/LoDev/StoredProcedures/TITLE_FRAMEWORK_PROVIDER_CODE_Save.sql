-- =============================================
-- Author:		Justin Lara
-- Create date: 8/24/2017
-- Description:	Saves a title provider code.
-- =============================================
ALTER PROCEDURE [dbo].[TITLE_FRAMEWORK_PROVIDER_CODE_Save]
	@BrokerId uniqueidentifier,
	@AgentId uniqueidentifier,
	@ProviderCode varchar(20)
AS
BEGIN
	UPDATE TITLE_FRAMEWORK_PROVIDER_CODE
	SET
		ProviderCode = @ProviderCode
	WHERE
		BrokerId = @BrokerId
		AND AgentId = @AgentId
	
	IF @@rowcount = 0
	BEGIN
		INSERT INTO TITLE_FRAMEWORK_PROVIDER_CODE (
			BrokerId,
			AgentId,
			ProviderCode
		)
		VALUES (
			@BrokerId,
			@AgentId,
			@ProviderCode
		)
	END
END
