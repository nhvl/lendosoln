-- =============================================
-- Author:		Jhairo Erazo
-- Create date: 1/17/14
-- Description:	Fetch MBS transactions by transaction number
-- =============================================
CREATE PROCEDURE [dbo].[MBS_FetchTransactionsByTransactionNum] 
	-- Add the parameters for the stored procedure here
	@TransactionNum int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SELECT *
	FROM MBS_TRANSACTION
	WHERE TransactionNum=@TransactionNum
	
END