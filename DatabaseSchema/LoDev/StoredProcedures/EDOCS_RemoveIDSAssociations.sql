-- =============================================
-- Author:		Geoffrey Feltman
-- Create date: 11/14/13
-- Description:	Remove all of the IDS associations for a given Broker and doc type.
-- =============================================
CREATE PROCEDURE dbo.EDOCS_RemoveIDSAssociations 
	-- Add the parameters for the stored procedure here
	@BrokerId uniqueidentifier, 
	@DocTypeId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    DELETE FROM EDOCS_IDS_DOCTYPE_X_DOCTYPE
	WHERE BrokerId = @BrokerId AND @DocTypeId = DocTypeId
END
