

CREATE PROCEDURE [dbo].[QReceiveById] (
	@QueueId int, 
	@MessageId bigint, 
	@Archive bit
)

AS 
	DECLARE @Rownum int; 
	DECLARE @Errors int; 
	DECLARE @Returnval int; 
	DECLARE @Priority tinyint;
	DECLARE @Subject1 varchar(300);
	DECLARE @Subject2 varchar(300);
	DECLARE @InsertionTime datetime; 
	DECLARE @DataLoc varchar(10); 
	DECLARE @Data varchar(5500); 

	SELECT  @Priority = Priority, @Subject1 = Subject1, @Subject2 = Subject2, @DataLoc = DataLoc, @InsertionTime = InsertionTime, @Data = Data 
		FROM Q_MESSAGE WHERE MessageId = @MessageId AND @QueueId = QueueId
	SELECT @rownum = @@rowcount, @errors = @@error
	if @errors <> 0 
	BEGIN 
		RAISERROR('There was a sql error in QReceiveById', 16, 1); 
		SELECT @ReturnVal = -1; 
		GOTO ERROR; 
	END 
	if @rownum = 0 
	BEGIN
		RAISERROR('The message does not exist!', 16, 1); 
		SELECT @ReturnVal = -2; 
		GOTO ERROR; 
	END
	DELETE FROM Q_MESSAGE WHERE MessageId = @MessageId; 
	SELECT @rownum = @@rowcount, @errors = @@error;
		if @errors <> 0 
	BEGIN 
		RAISERROR('There was a sql error in QReceiveById', 16, 1); 
		SELECT @ReturnVal = -1; 
		GOTO ERROR; 
	END 
	if @rownum = 0 
	BEGIN
		RAISERROR('The message has been processed!', 16, 1); 
		SELECT @ReturnVal = -3; 
		GOTO ERROR; 
	END

	IF @Archive = 1 
	BEGIN 
		INSERT INTO Q_ARCHIVE( InsertionTime, QueueId, Subject1, Data,  MessageId, Subject2, Priority, DataLoc) 
			VALUES( @InsertionTime, @QueueId, @Subject1, @Data, @MessageId, @Subject2, @Priority, @DataLoc );
		SELECT @rownum = @@rowcount, @errors = @@error;
		if @errors <> 0 
		BEGIN 
			RAISERROR('There was a sql error in QReceiveById', 16, 1); 
			SELECT @ReturnVal = -1; 
			GOTO ERROR; 
		END 
		if @rownum = 0 
		BEGIN
			RAISERROR('The message has been processed!', 16, 1); 
			SELECT @ReturnVal = -3; 
			GOTO ERROR; 
		END
	END		

	
	SELECT @Priority as Priority, @Subject1 as Subject1, @Subject2 as Subject2, @DataLoc as DataLoc, @InsertionTime as InsertionTime, @Data as Data; 

	GOTO FINISH; 

	ERROR: 
		if @@TRANCOUNT > 0 
			ROLLBACK TRANSACTION; 
	FINISH:
		return @ReturnVal;
	



