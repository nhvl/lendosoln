ALTER PROCEDURE [dbo].[APPRAISAL_ORDER_GetById]
	@AppraisalOrderId uniqueidentifier
AS
BEGIN
	SELECT a.*
	FROM APPRAISAL_ORDER a
	JOIN broker b on a.brokerid = b.brokerid
	WHERE b.status = 1
	AND a.AppraisalOrderId = @AppraisalOrderId

	SELECT 
		EDocId, AppraisalOrderId
	FROM
		APPRAISAL_ORDER_EDOCS
	WHERE
		AppraisalOrderId=@AppraisalOrderId
END