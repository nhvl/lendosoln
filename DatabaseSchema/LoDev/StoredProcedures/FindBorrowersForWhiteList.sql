ALTER PROCEDURE [dbo].[FindBorrowersForWhiteList]
	@slid uniqueidentifier
	
AS
BEGIN

	select a.aAppId, a.aBFirstNm, a.aBLastNm,  a.aBSsnEncrypted,
		   a.aCFirstNm, a.aCLastNm,  a.aCSsnEncrypted,
		   lfc4.sEncryptionKey, lfc4.sEncryptionMigrationVersion
		   
	from application_a a
	join loan_file_cache_4 lfc4 on lfc4.sLId = a.sLId
	where a.slid = @slid
	
END