-- =========================================
-- Author:		Scott Kibler
-- Create date: 2/7/2014
-- Description:	opm 150384
--		Retrieves the (non-deleted) tpo portal config associated with the user
--		, or its originating company, if it doesn't have one.
-- =============================================
CREATE PROCEDURE [dbo].[TPOPortalConfig_RetrieveByUserID] 
	@UserID uniqueidentifier,
	@BrokerID uniqueidentifier,
	@PortalMode int = 1
AS
BEGIN
	declare @userConfigID uniqueidentifier
	declare @pmlBrokerID uniqueidentifier
	
	if @PortalMode = 1 /*Broker Mode*/
	BEGIN
		SELECT @userConfigID = Lender_TPO_LandingPageConfigId, @pmlBrokerID = pmlBrokerId
		from broker_user with(nolock)
		where userid= @UserID
	END
	ELSE if @PortalMode = 2 /*Mini-Correspondent Mode*/
	BEGIN
		SELECT @userConfigID = MiniCorrespondentLandingPageID, @pmlBrokerID = pmlBrokerId
		from broker_user with(nolock)
		where userid= @UserID
	END
	ELSE if @PortalMode = 3 /*Correspondent Mode*/
	BEGIN
		SELECT @userConfigID = CorrespondentLandingPageID, @pmlBrokerID = pmlBrokerId
		from broker_user with(nolock)
		where userid= @UserID
	END
	
	if @userConfigID is not null
	BEGIN
		SELECT *
		from Lender_TPO_LandingPageConfig
		where ID = @userConfigID
		and IsDeleted = 0
		and BrokerID = @BrokerID
	END
	
	if @userConfigID is null OR @@RowCount = 0 -- if the user level didn't match, try the broker company level.
	BEGIN
		DECLARE @ocConfigID uniqueidentifier
		
		if @PortalMode = 1 /*Broker Mode*/
		BEGIN
			SELECT @ocConfigID = Lender_TPO_LandingPageConfigId
			from pml_broker with(nolock)
			where pmlbrokerid = @pmlBrokerID
			and BrokerID = @BrokerID
		END
		ELSE if @PortalMode = 2 /*Mini-Correspondent Mode*/
		BEGIN
			SELECT @ocConfigID = MiniCorrespondentTPOPortalConfigID
			from pml_broker with(nolock)
			where pmlbrokerid = @pmlBrokerID
			and BrokerID = @BrokerID
		END
		ELSE if @PortalMode = 3 /*Correspondent Mode*/
		BEGIN
			SELECT @ocConfigID = CorrespondentTPOPortalConfigID
			from pml_broker with(nolock)
			where pmlbrokerid = @pmlBrokerID
			and BrokerID = @BrokerID
		END
		
		if @ocConfigID is not null
		BEGIN
			SELECT *
			from Lender_TPO_LandingPageConfig
			where ID = @ocConfigID
			and IsDeleted = 0
			and BrokerID = @BrokerID
		END
	END
END

