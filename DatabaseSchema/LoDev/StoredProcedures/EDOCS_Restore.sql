-- =============================================
-- Author:		Antonio Valencia
-- Create date: July 7, 11
-- Description:	Restore Deleted Docs
-- 7/12/2013: Added SQL to update sUnscreenedDocsInFile in LOAN_FILE_CACHE
-- =============================================
ALTER PROCEDURE [dbo].[EDOCS_Restore] 
	-- Add the parameters for the stored procedure here
	@DocumentId uniqueidentifier, 
	@UserId uniqueidentifier,
	@Reason varchar(200)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @sLId uniqueidentifier 
	DECLARE @TotalNumPages int
	DECLARE @IsValid bit 
	DECLARE @BrokerId uniqueidentifier 
	DECLARE @Status tinyint
	
	BEGIN TRANSACTION	

	SELECT @BrokerId = BrokerId, @sLId = sLId, @TotalNumPages = NumPages, @IsValid = IsValid, @Status = [Status] FROM EDOCS_DOCUMENT  
	WHERE DocumentId = @DocumentId 
	IF @@error!= 0 GOTO HANDLE_ERROR
	
	IF @IsValid = 1 GOTO FINISH
		
	UPDATE EDOCS_DOCUMENT 
	SET 
		IsValid = 1
	WHERE DocumentId = @DocumentId
	IF @@error!= 0 GOTO HANDLE_ERROR
	
	-- update cached stats --
	-- Only need to update if we are restoring a document with Status = 0 --
	IF @sLId IS NOT NULL AND @Status = 0
	BEGIN
		-- We know we're adding back an unscreened doc, so just set to 1 --
		UPDATE LOAN_FILE_CACHE_3
		SET sUnscreenedDocsInFile = 1
		WHERE sLId = @sLId
	END

	INSERT INTO EDocs_Audit_History( DocumentId, ModifiedByUserId, ModifiedDate, Description ) 
	VALUES ( @DocumentId, @UserId,  GETDATE(),  @Reason )
	IF @@error!= 0 GOTO HANDLE_ERROR

	
FINISH: 
	COMMIT 
	RETURN

HANDLE_ERROR:
    ROLLBACK TRANSACTION
	RAISERROR ('Failure invalidating electronic document.', 16, 1)
    RETURN 

END


