-- =============================================
-- Author:		Justin Kim
-- Create date: 6/5/17
-- Description:	Return PmlBrokerId based on originating company name
-- =============================================
ALTER PROCEDURE [dbo].[GetPmlBrokerIdByName] 
	-- Add the parameters for the stored procedure here
	@PmlBrokerNm varchar(100), @BrokerId UniqueIdentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT p.PmlBrokerId
	FROM Pml_Broker p
	WHERE p.Name = @PmlBrokerNm AND p.BrokerId = @BrokerId
END
