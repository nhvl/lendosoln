ALTER PROCEDURE [dbo].[THIRD_PARTY_CERTIFICATE_Insert]
	@Description varchar(300),
	@FileDBKey uniqueidentifier,
	@EncryptionKeyId uniqueidentifier,
	@CreatedDate datetime,
	@EncryptedPassword varbinary(max),
	@ValidOn datetime,
	@ExpiresOn datetime, 
	@Issuer varchar(300),
	@Subject varchar(300)
AS
BEGIN
			INSERT INTO THIRD_PARTY_CERTIFICATE(Description, FileDBKey, EncryptionKeyId, CreatedDate, EncryptedPassword, ValidOn, ExpiresOn, Issuer, Subject)
			VALUES (@Description, @FileDBKey, @EncryptionKeyId, @CreatedDate, @EncryptedPassword, @ValidOn, @ExpiresOn, @Issuer, @Subject)
			
			SELECT SCOPE_IDENTITY()
END
