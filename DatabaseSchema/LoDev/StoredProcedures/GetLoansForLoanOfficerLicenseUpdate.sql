-- =============================================
-- Author:		Eric Mallare
-- Create date: 12/1/2016
-- Description:	Grabs the loans for Loan Officer License update when the employee's license info is updated.
-- =============================================
ALTER PROCEDURE [dbo].[GetLoansForLoanOfficerLicenseUpdate] 
	@EmployeeId uniqueidentifier,
	@BrokerId uniqueidentifier
AS
BEGIN
	SELECT
		lfc.sLId, lfc.sStatusT
	FROM
		LOAN_FILE_CACHE lfc
	WHERE
		lfc.sBrokerId=@BrokerId AND
		lfc.IsValid=1 AND
		lfc.sEmployeeLoanRepId=@EmployeeId
END
