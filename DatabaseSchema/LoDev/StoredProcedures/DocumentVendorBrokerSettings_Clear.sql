-- =============================================
-- Author:		paoloa
-- Create date: 7/24/2013
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE dbo.DocumentVendorBrokerSettings_Clear
	-- Add the parameters for the stored procedure here
	@BrokerId uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM DOCUMENT_VENDOR_BROKER_SETTINGS
	WHERE BrokerId = @BrokerId
END
