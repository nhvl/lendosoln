-- =============================================
-- Author:		Eric Mallare
-- Create date: 9/14/2018
-- Description:	Creates a Credit Report Request job.
-- =============================================
ALTER PROCEDURE [dbo].[CREDIT_REPORT_REQUEST_JOBS_Create]
	@JobId int,
	@RetryIntervalInSeconds int,
	@ShouldReissueOnResponseNotReady bit,
	@MaxRetryCount int = null,
	@CreditRequestDataFileDbGuidId uniqueidentifier = null,
	@BrokerId uniqueidentifier = null,
	@UserId uniqueidentifier = null,
	@UserType char(1) = null,
	@AttemptCount int
AS
BEGIN
	INSERT INTO [dbo].[CREDIT_REPORT_REQUEST_JOBS]
	(
		JobId,
		RetryIntervalInSeconds,
		ShouldReissueOnResponseNotReady,
		MaxRetryCount,
		CreditRequestDataFileDbGuidId,
		BrokerId,
		UserId,
		UserType,
		AttemptCount
	)
	VALUES
	(
		@JobId,
		@RetryIntervalInSeconds,
		@ShouldReissueOnResponseNotReady,
		@MaxRetryCount,
		@CreditRequestDataFileDbGuidId,
		@BrokerId,
		@UserId,
		@UserType,
		@AttemptCount
	)
END
