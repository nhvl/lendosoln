-- =============================================
-- Author:		Justin Lara
-- Create date: 1/11/2017
-- Description:	Saves an association indicating
--				that a message has been read by
--				a user.
-- =============================================
ALTER PROCEDURE [dbo].[APPRAISAL_VENDOR_MESSAGES_SetMessageRead]
	@MessageId int,
	@UserId uniqueidentifier,
	@BrokerId uniqueidentifier,
	@LoanId uniqueidentifier
AS
BEGIN
	SET NOCOUNT ON;
	
	IF NOT EXISTS
	(
		SELECT 1
		FROM APPRAISAL_VENDOR_MESSAGES_READ
		WHERE MessageId = @MessageId
			AND UserId = @UserId
			AND BrokerId = @BrokerId
			AND LoanId = @LoanId
	)
	BEGIN
		INSERT INTO APPRAISAL_VENDOR_MESSAGES_READ
		(
			MessageId,
			UserId,
			BrokerId,
			LoanId
		)
		VALUES
		(
			@MessageId,
			@UserId,
			@BrokerId,
			@LoanId
		)
	END
END
