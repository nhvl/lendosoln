CREATE PROCEDURE dbo.GetAdminBrokerReportQueryTemplateId
	@BrokerId UniqueIdentifier
AS
	SELECT
		q.QueryId
	FROM
		Report_Query AS q
	WHERE
		q.BrokerId = @BrokerId
		AND
		q.QueryName = 'Blank report'
	IF( @@error != 0 )
	BEGIN
		RAISERROR('Error in the select statement in GetAdminBrokerReportQueryTemplateId sp', 16, 1);
		RETURN 0;
	END
