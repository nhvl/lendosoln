/* =============================================
-- Author:		Scott Kibler
-- Create date: 8/13/2014
-- Description:	
	Checks for loan presence in the Main DB.
	NOTE: It skips any tables that foreign key to these tables.
	NOTE: Also need to check transient DB and fileDB.
-- =============================================*/
ALTER PROCEDURE [dbo].[QUICKPRICER_2_LOAN_USAGE_LoanHasAnyPresence]
	@BrokerID UniqueIdentifier,
	@LoanID UniqueIdentifier,
	@HasPresence bit OUTPUT,
	@TablesWhereItsPresent varchar(1000) OUTPUT	-- csl
AS
BEGIN
	DECLARE @Error int		-- to come from  @@Error
	DECLARE @RowCount int   -- to come from  @@RowCount
	DECLARE @ErrorCode int
	DECLARE @ErrorMsg varchar(1000)
	
	SET @HasPresence = 0;
	SET @TablesWhereItsPresent = '';
	SET @ErrorCode = -1;
	SET @ErrorMsg = 'Unexpected Error';
	
	
	SELECT @RowCount = COUNT(*) 
	FROM STATUS_EVENT 
	WHERE 
		LoanId = @LoanID
	IF(0 <> @@Error)
	BEGIN
		SET @ErrorCode = -53
		SET @ErrorMsg = 'Unable to select from Status_Event'
		GOTO ErrorHandler;
	END
	
	SELECT @RowCount = COUNT(*) 
	FROM APPLICATION_B 
	WHERE 
		sLId = @LoanID
	IF(0 <> @@Error)
	BEGIN
		SET @ErrorCode = -53
		SET @ErrorMsg = 'Unable to select from application_b'
		GOTO ErrorHandler;
	END
	IF(0 <> @RowCount)
	BEGIN
		SET @HasPresence = 1;
		SET @TablesWhereItsPresent = @TablesWhereItsPresent + ', APPLICATION_B';
	END
	
	SELECT @RowCount = COUNT(*) 
	FROM APPLICATION_A 
	WHERE 
		sLId = @LoanID
	IF(0 <> @@Error)
	BEGIN
		SET @ErrorCode = -53
		SET @ErrorMsg = 'Unable to select from application_a'
		GOTO ErrorHandler;
	END
	IF(0 <> @RowCount)
	BEGIN
		SET @HasPresence = 1;
		SET @TablesWhereItsPresent = @TablesWhereItsPresent + ', APPLICATION_A';
	END

	
	SELECT @RowCount = COUNT(*) 
	FROM AUDIT_TRAIL_DAILY 
	WHERE
		    LoanID = @LoanID
	IF(0 <> @@Error)
	BEGIN
		SET @ErrorCode = -53
		SET @ErrorMsg = 'Unable to select from audit_trail'
		GOTO ErrorHandler;
	END
	IF(0 <> @RowCount)
	BEGIN
		SET @HasPresence = 1;
		SET @TablesWhereItsPresent = @TablesWhereItsPresent + ', AUDIT_TRAIL_DAILY';
	END
	
	SELECT @RowCount = COUNT(*)
	FROM LOAN_FILE_B 
	WHERE
		sLId = @LoanID
	IF(0 <> @@Error)
	BEGIN
		SET @ErrorCode = -53
		SET @ErrorMsg = 'Unable to select from LOAN_FILE_B'
		GOTO ErrorHandler;
	END
	IF(0 <> @RowCount)
	BEGIN
		SET @HasPresence = 1;
		SET @TablesWhereItsPresent = @TablesWhereItsPresent + ', LOAN_FILE_B';
	END
	
	SELECT @RowCount = COUNT(*)
	FROM LOAN_FILE_C 
	WHERE
		sLId = @LoanID
	IF(0 <> @@Error )
	BEGIN
		SET @ErrorCode = -53
		SET @ErrorMsg = 'Unable to select from LOAN_FILE_C'
		GOTO ErrorHandler;
	END
	IF(0 <> @RowCount)
	BEGIN
		SET @HasPresence = 1;
		SET @TablesWhereItsPresent = @TablesWhereItsPresent + ', LOAN_FILE_C';
	END
	
	SELECT @RowCount = COUNT(*)
	FROM CONDITION 
	WHERE
		LoanID = @LoanID
	IF(0 <> @@Error)
	BEGIN
		SET @ErrorCode = -53
		SET @ErrorMsg = 'Unable to select from CONDITION'
		GOTO ErrorHandler;
	END
	IF(0 <> @RowCount)
	BEGIN
		SET @HasPresence = 1;
		SET @TablesWhereItsPresent = @TablesWhereItsPresent + ', CONDITION';
	END
	
	SELECT @RowCount = COUNT(*)
	FROM LOAN_FILE_ACTIVITY 
	WHERE
		sLId = @LoanID
	IF(0 <> @@Error)
	BEGIN
		SET @ErrorCode = -53
		SET @ErrorMsg = 'Unable to select from LOAN_FILE_ACTIVITY'
		GOTO ErrorHandler;
	END
	IF(0 <> @RowCount)
	BEGIN
		SET @HasPresence = 1;
		SET @TablesWhereItsPresent = @TablesWhereItsPresent + ', LOAN_FILE_ACTIVITY';
	END
	
	SELECT @RowCount = COUNT(*)
	FROM RATE_MONITOR 
	WHERE
		sLId = @LoanID
	IF(0 <> @@Error)
	BEGIN
		SET @ErrorCode = -53
		SET @ErrorMsg = 'Unable to select from RATE_MONITOR'
		GOTO ErrorHandler;
	END
	IF(0 <> @RowCount)
	BEGIN
		SET @HasPresence = 1;
		SET @TablesWhereItsPresent = @TablesWhereItsPresent + ', RATE_MONITOR';
	END
	
	SELECT @RowCount = COUNT(*)
	FROM QUICKPRICER_2_LOAN_USAGE 
	WHERE
		LoanID = @LoanID
	IF(0 <> @@Error) -- allow skipping deletion if its sCreatedD is sufficiently old.
	BEGIN
		SET @ErrorCode = -53
		SET @ErrorMsg = 'Unable to select from QUICKPRICER_2_LOAN_USAGE'
		GOTO ErrorHandler;
	END
	IF(0 <> @RowCount)
	BEGIN
		SET @HasPresence = 1;
		SET @TablesWhereItsPresent = @TablesWhereItsPresent + ', QUICKPRICER_2_LOAN_USAGE';
	END
	
	SELECT @RowCount = COUNT(*)
	FROM TASK 
	WHERE
		LoanID = @LoanID
	IF(0 <> @@Error)
	BEGIN
		SET @ErrorCode = -53
		SET @ErrorMsg = 'Unable to select from Task'
		GOTO ErrorHandler;
	END
	IF(0 <> @RowCount)
	BEGIN
		SET @HasPresence = 1;
		SET @TablesWhereItsPresent = @TablesWhereItsPresent + ', TASK';
	END
	
	SELECT @RowCount = COUNT(*)
	FROM LOAN_FILE_CACHE 
	WHERE
		sLId = @LoanID
	IF(0 <> @@Error)
	BEGIN
		SET @ErrorCode = -53
		SET @ErrorMsg = 'Unable to select from LOAN_FILE_CACHE'
		GOTO ErrorHandler;
	END
	IF(0 <> @RowCount)
	BEGIN
		SET @HasPresence = 1;
		SET @TablesWhereItsPresent = @TablesWhereItsPresent + ', LOAN_FILE_CACHE';
	END
	
	SELECT @RowCount = COUNT(*)
	FROM LOAN_FILE_CACHE_2 
	WHERE
		sLId = @LoanID
	IF(0 <> @@Error )
	BEGIN
		SET @ErrorCode = -53
		SET @ErrorMsg = 'Unable to select from LOAN_FILE_CACHE_2'
		GOTO ErrorHandler;
	END
	IF(0 <> @RowCount)
	BEGIN
		SET @HasPresence = 1;
		SET @TablesWhereItsPresent = @TablesWhereItsPresent + ', LOAN_FILE_CACHE_2';
	END
	
	SELECT @RowCount = COUNT(*)
	FROM LOAN_FILE_D 
	WHERE
		sLId = @LoanID
	IF(0 <> @@Error )
	BEGIN
		SET @ErrorCode = -53
		SET @ErrorMsg = 'Unable to select from LOAN_FILE_D'
		GOTO ErrorHandler;
	END
	IF(0 <> @RowCount)
	BEGIN
		SET @HasPresence = 1;
		SET @TablesWhereItsPresent = @TablesWhereItsPresent + ', LOAN_FILE_D';
	END
	
	SELECT @RowCount = COUNT(*)
	FROM LOAN_FILE_E 
	WHERE
		sLId = @LoanID
	IF(0 <> @@Error )
	BEGIN
		SET @ErrorCode = -53
		SET @ErrorMsg = 'Unable to select from LOAN_FILE_E'
		GOTO ErrorHandler;
	END
	IF(0 <> @RowCount)
	BEGIN
		SET @HasPresence = 1;
		SET @TablesWhereItsPresent = @TablesWhereItsPresent + ', LOAN_FILE_E';
	END
	
	SELECT @RowCount = COUNT(*)
	FROM LOAN_FILE_F 
	WHERE
		sLId = @LoanID
	IF(0 <> @@Error )
	BEGIN
		SET @ErrorCode = -53
		SET @ErrorMsg = 'Unable to select from LOAN_FILE_F'
		GOTO ErrorHandler;
	END
	IF(0 <> @RowCount)
	BEGIN
		SET @HasPresence = 1;
		SET @TablesWhereItsPresent = @TablesWhereItsPresent + ', LOAN_FILE_F';
	END
	
	SELECT @RowCount = COUNT(*)
	FROM INTEGRATION_FILE_MODIFIED 
	WHERE
		sLId = @LoanID
	IF(0 <> @@Error )
	BEGIN
		SET @ErrorCode = -53
		SET @ErrorMsg = 'Unable to select from INTEGRATION_FILE_MODIFIED'
		GOTO ErrorHandler;
	END
	IF(0 <> @RowCount)
	BEGIN
		SET @HasPresence = 1;
		SET @TablesWhereItsPresent = @TablesWhereItsPresent + ', INTEGRATION_FILE_MODIFIED';
	END	
	
	
	
	SELECT @RowCount = COUNT(*)
	FROM LOAN_USER_ASSIGNMENT 
	WHERE
		sLId = @LoanID
	IF(0 <> @@Error)
	BEGIN
		SET @ErrorCode = -53
		SET @ErrorMsg = 'Unable to select from LOAN_USER_ASSIGNMENT'
		GOTO ErrorHandler;
	END
	IF(0 <> @RowCount)
	BEGIN
		SET @HasPresence = 1;
		SET @TablesWhereItsPresent = @TablesWhereItsPresent + ', LOAN_USER_ASSIGNMENT';
	END
	
	SELECT @RowCount = COUNT(*)
	FROM TASK_x_TASK_TRIGGER_TEMPLATE 
	WHERE
		LoanID = @LoanID
	IF(0 <> @@Error)
	BEGIN
		SET @ErrorCode = -53
		SET @ErrorMsg = 'Unable to select from TASK_x_TASK_TRIGGER_TEMPLATE'
		GOTO ErrorHandler;
	END
	IF(0 <> @RowCount)
	BEGIN
		SET @HasPresence = 1;
		SET @TablesWhereItsPresent = @TablesWhereItsPresent + ', TASK_x_TASK_TRIGGER_TEMPLATE';
	END
	
	SELECT @RowCount = COUNT(*)
	FROM TRUST_ACCOUNT 
	WHERE
		sLId = @LoanID
	IF(0 <> @@Error )
	BEGIN
		SET @ErrorCode = -53
		SET @ErrorMsg = 'Unable to select from TRUST_ACCOUNT'
		GOTO ErrorHandler;
	END
	IF(0 <> @RowCount)
	BEGIN
		SET @HasPresence = 1;
		SET @TablesWhereItsPresent = @TablesWhereItsPresent + ', TRUST_ACCOUNT';
	END
	
	SELECT @RowCount = COUNT(*)
	FROM LOAN_FILE_CACHE_3 
	WHERE
		sLId = @LoanID
	IF(0 <> @@Error )
	BEGIN
		SET @ErrorCode = -53
		SET @ErrorMsg = 'Unable to select from LOAN_FILE_CACHE_3'
		GOTO ErrorHandler;
	END
	IF(0 <> @RowCount)
	BEGIN
		SET @HasPresence = 1;
		SET @TablesWhereItsPresent = @TablesWhereItsPresent + ', LOAN_FILE_CACHE_3';
	END
	
	
	SELECT @RowCount = COUNT(*)
	FROM DISCUSSION_LOG 
	WHERE
		DiscRefObjId = @LoanID
	IF(0 <> @@Error)
	BEGIN
		SET @ErrorCode = -53
		SET @ErrorMsg = 'Unable to select from DISCUSSION_LOG'
		GOTO ErrorHandler;
	END
	IF(0 <> @RowCount)
	BEGIN
		SET @HasPresence = 1;
		SET @TablesWhereItsPresent = @TablesWhereItsPresent + ', DISCUSSION_LOG';
	END
	
	SELECT @RowCount = COUNT(*)
	FROM LOAN_CACHE_UPDATE_REQUEST 
	WHERE
		sLId = @LoanID
	IF(0 <> @@Error)
	BEGIN
		SET @ErrorCode = -53
		SET @ErrorMsg = 'Unable to select from LOAN_CACHE_UPDATE_REQUEST'
		GOTO ErrorHandler;
	END
	IF(0 <> @RowCount)
	BEGIN
		SET @HasPresence = 1;
		SET @TablesWhereItsPresent = @TablesWhereItsPresent + ', LOAN_CACHE_UPDATE_REQUEST';
	END
	
	
	SELECT @RowCount = COUNT(*)
	FROM LOAN_FILE_A 
	WHERE
		sLId = @LoanID
	IF(0 <> @@Error )
	BEGIN
		SET @ErrorCode = -53
		SET @ErrorMsg = 'Unable to select from LOAN_FILE_A'
		GOTO ErrorHandler;
	END
	IF(0 <> @RowCount)
	BEGIN
		SET @HasPresence = 1;
		SET @TablesWhereItsPresent = @TablesWhereItsPresent + ', LOAN_FILE_A';
	END
	
	RETURN 0;
	ErrorHandler:
		RAISERROR(@ErrorMsg, 16, 1)
		RETURN @ErrorCode;
END
