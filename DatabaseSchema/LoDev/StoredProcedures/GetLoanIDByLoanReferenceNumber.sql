CREATE  PROCEDURE [dbo].GetLoanIdByLoanReferenceNumber
	@sLRefNm varchar(100),
	@sBrokerID uniqueidentifier,
	@IncludeInactive bit = 1
AS
	SELECT d.sLId
	FROM LOAN_FILE_CACHE a with(nolock)
	JOIN LOAN_FILE_CACHE_4 d with(nolock)
	ON a.sLId = d.sLId
	WHERE sLRefNm = @sLRefNm AND sBrokerID = @sBrokerID AND (IsValid=1 OR @IncludeInactive  = 1)