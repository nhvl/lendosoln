

CREATE PROCEDURE [dbo].[GetNumLoansAssociatedWithBranchByBranchId]
	@BranchId uniqueidentifier
AS
	SELECT
		COUNT('') AS numLoans
	FROM 
		LOAN_FILE_CACHE
	WHERE
		sBranchId = @BranchId
