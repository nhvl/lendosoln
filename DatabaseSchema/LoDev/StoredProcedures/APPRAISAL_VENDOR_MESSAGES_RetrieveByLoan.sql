-- =============================================
-- Author:		Justin Lara
-- Create date: 1/11/2017
-- Description:	Retrieves all appraisal vendor
--				messages for a given loan.
-- =============================================
ALTER PROCEDURE dbo.APPRAISAL_VENDOR_MESSAGES_RetrieveByLoan
	@BrokerId uniqueidentifier,
	@LoanId uniqueidentifier
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT MessageId,
		VendorId,
		BrokerId,
		LoanId,
		OrderNumber,
		SendingParty,
		ReceivingParty,
		Subject,
		Message,
		Date
	FROM APPRAISAL_VENDOR_MESSAGES
	WHERE BrokerId = @BrokerId AND LoanId = @LoanId
END
