CREATE PROCEDURE [dbo].[LockPolicyObservedHoliday_List]
	@LockPolicyId uniqueidentifier,
	@BrokerId uniqueidentifier
AS
BEGIN
	SELECT HolidayName
	FROM dbo.LENDER_LOCK_POLICY_OBSERVED_HOLIDAY
	WHERE LockPolicyId = @LockPolicyId AND BrokerId = @BrokerId
END
