
CREATE   PROCEDURE [dbo].[ListBrokersAccountOwners]
	@BrokerId uniqueidentifier = NULL
AS
	SELECT
		v.BrokerId ,
		v.UserId ,
		v.EmployeeId ,
		v.UserFirstNm ,
		v.UserLastNm ,
		v.Email
	FROM
		View_Active_LO_User AS v
	WHERE
		v.BrokerId = COALESCE( @BrokerId , v.BrokerId )
		AND
		v.IsAccountOwner = 1

