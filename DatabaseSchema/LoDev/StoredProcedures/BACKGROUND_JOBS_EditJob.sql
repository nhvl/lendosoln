-- =============================================
-- Author:		Eric Mallare
-- Create date: 11/28/2017
-- Description:	Edits an existing background job.
-- =============================================
ALTER PROCEDURE [dbo].[BACKGROUND_JOBS_EditJob]
	@JobId int,
	@ProcessingStatus int,
	@NextRunDate datetime,
	@DateCompleted datetime = null
AS
BEGIN
	UPDATE
		BACKGROUND_JOBS
	SET
		ProcessingStatus=@ProcessingStatus,
		NextRunDate=@NextRunDate,
		DateCompleted=@DateCompleted
	WHERE
		JobId=@JobId
END