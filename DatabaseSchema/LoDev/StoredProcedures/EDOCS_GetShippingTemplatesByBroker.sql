ALTER procedure [dbo].[EDOCS_GetShippingTemplatesByBroker]
	@BrokerId uniqueidentifier 
as

	select 
		st.ShippingTemplateName, 
		st.ShippingTemplateId, 
		st.BrokerId
		from EDOCS_SHIPPING_TEMPLATE st 
		where st.BrokerId = @BrokerId
		order by st.ShippingTemplateName

