-- =============================================
-- Author:		Matthew Pham
-- Create date: 2/23/12
-- Description:	Deletes a range of messages
-- =============================================
CREATE PROCEDURE Q_DeleteRange 
	-- Add the parameters for the stored procedure here
	@QueueId int ,
	@Archive bit,
	@StartMsgId bigint,
	@EndMsgId bigint,
	@Priority tinyint
AS
BEGIN
	
	SET NOCOUNT ON;


	IF @Archive = 1	 
	BEGIN
		INSERT INTO Q_ARCHIVE( InsertionTime, QueueId, Subject1, Data,  MessageId, Subject2, Priority, DataLoc) 
			SELECT  InsertionTime, QueueId, Subject1, Data, MessageId, Subject2, Priority, DataLoc 
			FROM Q_MESSAGE
			WHERE QueueId = @QueueId
				AND MessageId >= @StartMsgId
				AND MessageId <= @EndMsgId
				AND Priority = @Priority
	END	

	DELETE FROM Q_MESSAGE 
	WHERE QueueId = @QueueId
		AND MessageId >= @StartMsgId
		AND MessageId <= @EndMsgId
		AND Priority = @Priority


	
END
