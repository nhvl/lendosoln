

-- =============================================
-- Author:		<Mallare, Eric>
-- Create date: <6/2/2015>
-- Description:	<Gets all loan ids for files that are migrated to the new CFPB data layer.>
-- =============================================
CREATE PROCEDURE [dbo].[ListAllCFPBMigratedLoans]
AS
BEGIN
	SELECT
		b.sLId as sLId
	FROM
		BROKER br JOIN
		LOAN_FILE_CACHE lfc on br.BrokerId=lfc.sBrokerId JOIN
		LOAN_FILE_B b on lfc.sLId=b.sLId
	WHERE
		br.Status='1' AND
		lfc.IsValid=1 AND
		b.sIsHousingExpenseMigrated=1 AND
		b.sClosingCostFeeVersionT<>0
END


