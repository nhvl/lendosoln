-- =============================================
-- Author:		Geoffrey Feltman
-- Create date: 6/11/14
-- Description:	Retrieves the pending document requests that have reached
--              their scheduled send time.
-- =============================================
CREATE PROCEDURE [dbo].[PENDING_DOCUMENT_REQUEST_RetrieveScheduledRequests]
AS
BEGIN
SELECT 
    Id,
    LoanId,
    ApplicationId,
    p.BrokerId,
    RequestType,
    Description,
    ScheduledSendTime,
    CreatorEmployeeId,
    FileDbKey,
    EDocId,
    EDocVersion,
    DocumentLayoutXml,
	DocTypeId,
    IsESignAllowed,
    IsRequestBorrowerSignature,
    IsRequestCoborrowerSignature,
    CoborrowerTitleBorrowerId,
    ErrorMessage
FROM PENDING_DOCUMENT_REQUEST p 
	JOIN LOAN_FILE_CACHE l ON p.LoanId = l.sLId
	JOIN LOAN_FILE_CACHE_3 l3 on p.loanid = l3.sLId 
	JOIN APPLICATION_A a ON p.ApplicationId = a.aAppId
	JOIN BROKER b ON p.BrokerId = b.BrokerId
WHERE 
	b.Status = 1
	AND l.IsValid = 1
	AND l.IsTemplate = 0
	AND (l3.sConsumerPortalSubmittedD   is not null OR sStatusT not in (12, 15, 16,17))
	AND p.ErrorMessage IS NULL
	AND 
	(
		SELECT COUNT(*)
		FROM PENDING_DOCUMENT_REQUEST p2
		WHERE p2.LoanId = p.LoanId
			AND p2.ScheduledSendTime > GETDATE() 
	) = 0
	AND
	(
		(
			b.IsEnableNewConsumerPortal = 0
			AND (a.aBEmail <> '' OR a.aCEmail <> '')
		)
		OR
		(
			b.IsEnableNewConsumerPortal = 1
			AND EXISTS
			(
				SELECT *
				FROM CONSUMER_PORTAL_USER c JOIN CONSUMER_PORTAL_USER_X_LOANAPP c_x_l ON c.Id = c_x_l.ConsumerUserId
				WHERE c_x_l.sLId = p.LoanId
					AND (c.Email = a.aBEmail OR c.Email = a.aCEmail)
			)
		)
	)
END
