-- =============================================
-- Author:		Brian Beery
-- Create date: 03/11/13
-- Description:	Pulls data to send to Secondary Interactive (OPM 110121)
-- =============================================
ALTER PROCEDURE [dbo].[RetrieveSIData]
	@CustomerCode varchar(50)
AS
BEGIN

DECLARE @BrokerID uniqueidentifier;
SELECT @BrokerID=brokerid FROM Broker WHERE CustomerCode=@CustomerCode

DECLARE @ReportStartDate smalldatetime;
SELECT @ReportStartDate = DATEADD("mm", -6, GETDATE()) -- go back 6 months only

SELECT sCreditScoreLpeQual AS QualifyingScore,
	aBFirstNm AS BorrFirstName,
	aBLastNm AS BorrLastName,
	aBTotI AS BorrTotalMonthlyIncome,
	aCTotI AS CoBorrTotalMonthlyIncome,
	sFinalLAmt AS TotalLoanAmount,
	sLAmtCalc AS LoanAmount,
	sLtvR AS LTV,
	sCltvR AS CLTV,
	CASE ( sProdDocT )
		WHEN '0' THEN 'Full Document'
		WHEN '1' THEN 'Alt'
		WHEN '2' THEN 'Lite'
		WHEN '3' THEN 'NIV (SIVA)'
		WHEN '4' THEN 'VISA'
		WHEN '5' THEN 'NIV (SISA)'
		WHEN '6' THEN 'No Ratio'
		WHEN '7' THEN 'NINA'
		WHEN '8' THEN 'NISA'
		WHEN '9' THEN 'No Doc'
		WHEN '10' THEN 'No Doc Verif Assets'
		WHEN '11' THEN 'VINA'
		WHEN '12' THEN 'Streamline'
	END	AS DocType,
	CASE ( sProdImpound )
		WHEN '0' THEN 'No'
		WHEN '1' THEN 'Yes'
	END AS IsImpound,
	sNoteIR AS NoteRate,
	CASE ( sLT )
		WHEN '0' THEN 'Conventional'
		WHEN '1' THEN 'FHA'
		WHEN '2' THEN 'VA'
		WHEN '3' THEN 'USDA Rural'
		WHEN '4' THEN 'Other'
	END	AS LoanType,
	sLNm AS LoanNumber,
	sTerm AS Term,
	CASE ( sStatusT )
		WHEN '0' THEN 'Loan Open'
		WHEN '1' THEN 'Pre-qual'
		WHEN '2' THEN 'Pre-approved'
		WHEN '3' THEN 'Registered'
		WHEN '4' THEN 'Approved'
		WHEN '5' THEN 'Docs Out'
		WHEN '6' THEN 'Funded'
		WHEN '7' THEN 'Loan On-hold'
		WHEN '8' THEN 'Loan Suspended'
		WHEN '9' THEN 'Loan Canceled'
		WHEN '10' THEN 'Loan Rejected'
		WHEN '11' THEN 'Loan Closed'
		WHEN '12' THEN 'Lead New'
		WHEN '13' THEN 'In Underwriting'
		WHEN '14' THEN 'Loan Web Consumer '
		WHEN '15' THEN 'Lead Canceled'
		WHEN '16' THEN 'Lead Declined'
		WHEN '17' THEN 'Lead Other'
		WHEN '18' THEN 'Loan Other'
		WHEN '19' THEN 'Recorded'
		WHEN '20' THEN 'Shipped To Investor'
		WHEN '21' THEN 'Clear to Close'
		WHEN '22' THEN 'Processing'
		WHEN '23' THEN 'Final Underwriting'
		WHEN '24' THEN 'Docs Back'
		WHEN '25' THEN 'Funding Conditions'
		WHEN '26' THEN 'Final Docs'
		WHEN '27' THEN 'Loan Purchased' -- Loan Sold
		WHEN '28' THEN 'Loan Submitted'
	END AS LoanStatus,
	CASE ( aOccT )
		WHEN '0' THEN 'Primary Residence'
		WHEN '1' THEN 'Secondary Residence'
		WHEN '2' THEN 'Investment'
	END	AS SubjPropPurpose,
	sLpTemplateNm AS LoanProgramName,
	sSubmitN AS RegisteredComments,
	sSpAddr AS SubjPropAddress,
	sSpCity AS SubjPropCity,
	sSpState AS SubjPropState,
	sSpZip AS SubjPropZip,
	sSpCounty AS SubjPropCounty,
	CASE ( sGseSpT )
		WHEN '0' THEN ''
		WHEN '1' THEN 'Attached'
		WHEN '2' THEN 'Condominium'
		WHEN '3' THEN 'Cooperative'
		WHEN '4' THEN 'Detached'
		WHEN '5' THEN 'Detached Condominium'
		WHEN '6' THEN 'HighRise Condominium'
		WHEN '7' THEN 'Manufactured Home Condominium'
		WHEN '8' THEN 'Manufactured Home Multiwide'
		WHEN '9' THEN 'Manufactured Housing'
		WHEN '10' THEN 'Manufactured Housing Single Wide'
		WHEN '11' THEN 'Modular'
		WHEN '12' THEN 'PUD'
	END AS PropertyType,
	sLpProductCode AS ProductCode,
	sPurchPrice AS PurchasePrice,
	CASE ( sLPurposeT )
		WHEN '0' THEN 'Purchase'
		WHEN '1' THEN 'Refinance'
		WHEN '2' THEN 'Refinance Cash-out'
		WHEN '3' THEN 'Construction'
		WHEN '4' THEN 'Construction Permanent'
		WHEN '5' THEN 'Other'
		WHEN '6' THEN 'FHA Streamlined Refinance'
		WHEN '7' THEN 'VA IRRRL'
	END AS LoanPurpose,
	sRefPurpose AS PurposeofRefinance,
	sUnitsNum AS SubjPropNoOfUnits,
	sMipPia AS '902MortgageInsurancePremium',
	sWarehouseFunderDesc AS WarehouseFunder,
	sRLckdD AS RateLockedDate,
	sRLckdExpiredD AS LockExpirationDate,
	sBrokerLockFinalBrokComp1PcPrice AS BrokerRateLockPrice,
	sInvestorLockRateSheetID AS InvestorRateSheetID,
	BranchNm AS BranchName,
	sEmployeeLoanRepName AS AssignedLoanOfficerName,
	sCanceledD AS CanceledDate,
	sEstCloseD AS EstimatedCloseDate,
	sInvestorLockBaseBrokComp1PcPrice AS InvestorBaseRateLockPrice,
	sInvestorLockBrokComp1PcPrice AS InvestorRateLockPrice,
	sInvestorLockLpInvestorNm AS InvestorRateLockInvestorName,
	sInvestorLockRLckdD AS InvestorRateLockDate,
	sInvestorLockConfNum AS InvestorRateLockConfirmation#,
	sInvestorLockLoanNum AS InvestorRateLockLoan#,
	sPurchaseAdviceTotalPrice_Field3 AS PurchaseAdviceTotalPrice,
	sFundD AS FundedDate,
	sLPurchaseD AS LoanPurchasedDate,
	CASE ( sInvestorLockCommitmentT )
		WHEN '0' THEN 'Best Effort'
		WHEN '1' THEN 'Mandatory'
		when 2 then 'Hedged'
		when 3 then 'Securitized'		
	END AS InvestorCommitmentType,
	sQualBottomR AS DTI,
	CASE ( sAusFindingsPull )
		WHEN '0' THEN 'No'
		WHEN '1' THEN 'Yes'
	END	AS AUSfindingspulled,
	CASE ( sProd3rdPartyUwResultT )
		WHEN '0' THEN 'None/Not Submitted'
		WHEN '1' THEN 'DU Approve/Eligible'
		WHEN '2' THEN 'DU Approve/Ineligible'
		WHEN '3' THEN 'DU Refer/Eligible'
		WHEN '4' THEN 'DU Refer/Ineligible'
		WHEN '5' THEN 'DU Refer with Caution/Eligible'
		WHEN '6' THEN 'DU Refer with Caution/Ineligible'
		WHEN '7' THEN 'DU EA I/Eligible'
		WHEN '8' THEN 'DU EA II/Eligible'
		WHEN '9' THEN 'DU EA III/Eligible'
		WHEN '10' THEN 'LP Accept/Eligible'
		WHEN '11' THEN 'LP Accept/Ineligible'
		WHEN '12' THEN 'LP Caution/Eligible'
		WHEN '13' THEN 'LP Caution/Ineligible'
		WHEN '14' THEN 'Out of Scope'
		WHEN '15' THEN 'LP A- Level 1'
		WHEN '16' THEN 'LP A- Level 2'
		WHEN '17' THEN 'LP A- Level 3'
		WHEN '18' THEN 'LP A- Level 4'
		WHEN '19' THEN 'LP A- Level 5'
		WHEN '20' THEN 'LP Refer'
		WHEN '21' THEN 'TOTAL Approve/Eligible'
		WHEN '22' THEN 'TOTAL Approve/Ineligible'
		WHEN '23' THEN 'TOTAL Refer/Eligible'
		WHEN '24' THEN 'TOTAL Refer/Ineligible'
		WHEN '25' THEN 'GUS Accept/Eligible'
		WHEN '26' THEN 'GUS Accept/Ineligible'
		WHEN '27' THEN 'GUS Refer/Eligible'
		WHEN '28' THEN 'GUS Refer/Ineligible'
		WHEN '29' THEN 'GUS Refer with Caution/Eligible'
		WHEN '30' THEN 'GUS Refer with Caution/Ineligible'
	END AS AUResponse,
	CASE ( aBIsSelfEmplmt )
		WHEN '0' THEN 'No'
		WHEN '1' THEN 'Yes'	
	END AS 'IsBorrSelf-Employed',
	CASE ( sMiCompanyNmT )
		WHEN '0' THEN ''
		WHEN '1' THEN 'CMG'
		WHEN '2' THEN 'Essent'
		WHEN '3' THEN 'Genworth'
		WHEN '4' THEN 'MGIC'
		WHEN '5' THEN 'Radian'
		WHEN '6' THEN 'UGI'
		WHEN '7' THEN 'Amerin'
		WHEN '8' THEN 'CAHLIF'
		WHEN '9' THEN 'CMG Pre-Sep 94'
		WHEN '10' THEN 'Commonwealth'
		WHEN '11' THEN 'MD Housing'
		WHEN '12' THEN 'MIF'
		WHEN '13' THEN 'PMI'
		WHEN '14' THEN 'RMIC'
		WHEN '15' THEN 'RMIC-NC'
		WHEN '16' THEN 'SONYMA'
		WHEN '17' THEN 'Triad'
		WHEN '18' THEN 'Verex'
		WHEN '19' THEN 'Wisc Mtg Assr'
		WHEN '20' THEN 'FHA'
		WHEN '21' THEN 'VA'
		WHEN '22' THEN 'USDA'
	END AS MortgageInsuranceProvider,
	sRejectD AS RejectedDate,
	sBrokerLockInvestorPrice AS BrokerRateLockInvestorPrice,
	sClosedD AS ClosedDate

FROM Loan_File_Cache WITH (nolock)
JOIN Loan_File_Cache_2 WITH (nolock) ON Loan_File_Cache_2.sLId = Loan_File_Cache.sLId
JOIN Branch WITH (nolock) ON Branch.BranchId = Loan_File_Cache.sBranchId
WHERE
	( sBrokerId = @BrokerID ) and 
	( IsValid = 1 ) and 
	( IsTemplate = 0 ) and 
	( sIsRateLocked = 1 ) and 
	( sRLckdD >= @ReportStartDate )
END

