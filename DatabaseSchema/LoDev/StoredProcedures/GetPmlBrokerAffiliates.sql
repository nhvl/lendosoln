CREATE  PROCEDURE [dbo].[GetPmlBrokerAffiliates]
	@BrokerID uniqueidentifier,
	@PmlBrokerId uniqueidentifier
AS
	SELECT p.BrokerId, p.PmlBrokerId, a.*
	FROM PML_BROKER_x_AGENT p
	INNER JOIN AGENT a
	ON p.AgentID = a.AgentID
	WHERE p.BrokerID = @BrokerID
	AND p.PmlBrokerId = @PmlBrokerId
	
	if( 0!=@@error)
	begin
		RAISERROR('Error in the select statement in GetPmlBrokerAffiliates sp', 16, 1);
		return -100;
	end
	return 0;