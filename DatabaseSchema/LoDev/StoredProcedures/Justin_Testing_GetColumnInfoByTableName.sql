-- =============================================
-- Author:		Justin Jia
-- ALTER date: June 14, 2012
-- Description:
--	Gets all column information by the table name, such as column_size, is_nullable.
--  Also gets any columns that are blocked from being null via a check constraint (and that are otherwise nullable.)
-- =============================================
ALTER PROCEDURE [dbo].[Justin_Testing_GetColumnInfoByTableName]
	
	@TableName varchar(200)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @tableId int
	set @tableId = object_id(@tableName)

	SELECT DISTINCT 
		isk.column_name, 
		isk.table_name, 
		isk.data_type, 
		sc.is_nullable,
		sc.is_identity, 
		sc.max_length, 
		sc.is_computed
	FROM 
			INFORMATION_SCHEMA.COLUMNS as isk
		INNER JOIN 
			sys.columns as sc 
		ON	
			sc.name = isk.column_name 
	WHERE 
			isk.table_name = @TableName
		AND isk.table_schema = 'dbo'
		AND sc.object_id = @tableId		
	
	SELECT 
			sco.name as columnName 
	FROM 
			SYS.COLUMNS as sco
		INNER JOIN 
			SYS.check_constraints as sch 
		ON 
			sch.parent_column_id = sco.column_id
	WHERE 
			sco.object_id = @tableId
		AND sch.parent_object_id = @tableid
		AND	sco.is_nullable = 1
		AND sch.[definition] LIKE '%IS NOT NULL)'
END
