-- =============================================
-- Author:		Paolo Arrastia
-- Create date: 1 Aug 2012
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[FindLoansByMortgagePool] 
	-- Add the parameters for the stored procedure here
	@poolid bigint = -1,
	@brokerid uniqueidentifier = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if(@brokerid = null)
	begin
		SELECT lc.sLId
		FROM LOAN_FILE_CACHE lc JOIN LOAN_FILE_CACHE_2 lc2 ON lc.sLId = lc2.sLId
		WHERE sMortgagePoolId = @poolid AND ( IsValid = 1 ) AND ( IsTemplate = 0 )
	end
	else
	begin
		SELECT lc.sLId
		FROM LOAN_FILE_CACHE lc JOIN LOAN_FILE_CACHE_2 lc2 ON lc.sLId = lc2.sLId
		WHERE @brokerid = sBrokerId and sMortgagePoolId = @poolid AND ( IsValid = 1 ) AND ( IsTemplate = 0 )
	end
END
