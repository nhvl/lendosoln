-- =============================================
-- Author:		Antonio Valencia
-- Create date: 10-1-08
-- Description:	Clears a do/du auto login for the given user
-- =============================================
CREATE PROCEDURE [dbo].[ClearDODUAutoLoginFor] 
	@Field varchar(10), 
	@BrokerID uniqueidentifier, 
	@EmployeeID uniqueidentifier
AS
BEGIN

	IF @Field = 'DOLogin' 
	BEGIN  
		UPDATE BROKER_USER 
			SET DoAutoLoginNm = '' 
			WHERE ( EXISTS( SELECT EmployeeID FROM VIEW_ACTIVE_PML_USER_WITH_BROKER_INFO WHERE @BrokerID = BrokerId AND @EmployeeID =EmployeeId ) OR 
				   EXISTS ( SELECT EmployeeID  FROM VIEW_DISABLED_PML_USER_WITH_BROKER_INFO WHERE @BrokerID = BrokerID and @EmployeeID = EmployeeID ) OR
					EXISTS( SELECT EmployeeID FROM VIEW_ACTIVE_LO_USER WHERE @BrokerID = BrokerId AND @EmployeeID =EmployeeId ) OR	
					EXISTS( SELECT EmployeeID FROM VIEW_DISABLED_LO_USER WHERE @BrokerID = BrokerId AND @EmployeeID =EmployeeId )

				  ) 
					AND @EmployeeID = EmployeeID
	END 
	IF  @Field = 'DULogin'
	BEGIN
		
    		UPDATE BROKER_USER 
			SET DuAutoLoginNm = '' 
			WHERE ( EXISTS( SELECT EmployeeID FROM VIEW_ACTIVE_PML_USER_WITH_BROKER_INFO WHERE @BrokerID = BrokerId AND @EmployeeID =EmployeeId ) OR 
				   EXISTS ( SELECT EmployeeID  FROM VIEW_DISABLED_PML_USER_WITH_BROKER_INFO WHERE @BrokerID = BrokerID and @EmployeeID = EmployeeID ) OR
					EXISTS( SELECT EmployeeID FROM VIEW_ACTIVE_LO_USER WHERE @BrokerID = BrokerId AND @EmployeeID =EmployeeId ) OR	
					EXISTS( SELECT EmployeeID FROM VIEW_DISABLED_LO_USER WHERE @BrokerID = BrokerId AND @EmployeeID =EmployeeId )

				  ) 
					AND @EmployeeID = EmployeeID
	END
END
