

-- =============================================
-- Author:		<Francesc>
-- Create date: <06/10/10>
-- Description:	<Retrieve loans associated with a given consumerId>
-- =============================================
CREATE PROCEDURE [dbo].[CP_RetrieveConsumerSsnInfoById]
	@ConsumerId bigint
	
AS
BEGIN
	SET NOCOUNT ON;

	SELECT ConsumerId, Last4SsnDigits, SsnAttemptFailureNumber, LastSsnAttemptFailDate
	FROM CP_CONSUMER
	WHERE ConsumerId = @ConsumerId

END


