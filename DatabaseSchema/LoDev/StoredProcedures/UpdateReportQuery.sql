CREATE PROCEDURE UpdateReportQuery
	@BrokerId Guid , @QueryId Guid , @QueryName Varchar(64) = NULL , @XmlContent Text = NULL , @IsPublished Bit = NULL , @NamePublishedAs Varchar(100) = NULL , @ShowPosition Int = NULL
AS
	UPDATE Report_Query
	SET
		QueryName = COALESCE( @QueryName , QueryName ) ,
		XmlContent = COALESCE( @XmlContent , XmlContent ) ,
		IsPublished = COALESCE( @IsPublished , IsPublished ) ,
		NamePublishedAs = COALESCE( @NamePublishedAs , NamePublishedAs ) ,
		ShowPosition = COALESCE( @ShowPosition , ShowPosition )
	WHERE
		QueryID = @QueryID
		AND
		BrokerID = @BrokerID
	IF( @@error != 0 )
	BEGIN
		RAISERROR('Error in the update statement in UpdateReportQuery sp', 16, 1);
		RETURN -100;
	END
