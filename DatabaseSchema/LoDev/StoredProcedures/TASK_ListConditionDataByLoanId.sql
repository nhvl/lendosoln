-- =============================================
-- Author:		Antonio Valencia
-- Last Modified by: Scott Kibler
-- Create date: March 07, 2013
-- Last Modified date: May 28, 2014
-- Description:
--	 - Generates the condition summary (counts of statuses per category) for the loan.
--   - Includes all 0's for conditions not on the loan.
-- =============================================
CREATE PROCEDURE [dbo].[TASK_ListConditionDataByLoanId] 
	@BrokerId uniqueidentifier,
	@LoanId uniqueidentifier
AS

BEGIN
	SELECT 
		  Category
		, count(CASE WHEN TaskStatus = 0 THEN 1 ELSE NULL END) AS OpenCount
		, count(CASE WHEN TaskStatus = 1 THEN 1 ELSE NULL END) AS ResolvedCount
		, count(CASE WHEN TaskStatus = 2 THEN 1 ELSE NULL END) AS ClosedCount
	FROM 
		Condition_Category cc
		LEFT JOIN Task t ON cc.ConditionCategoryId = t.CondCategoryId
	WHERE 
			cc.BrokerId = @BrokerId AND t.BrokerId = @BrokerId
		AND t.LoanId = @LoanId
		AND t.TaskIsCondition = 1 AND t.CondIsDeleted = 0
	GROUP BY Category
UNION ALL
	SELECT 
		  Category
		, 0 AS OpenCount
		, 0 AS ResolvedCount
		, 0 AS ClosedCount
	FROM 
		Condition_Category cc3
	WHERE 
		cc3.BrokerId = @brokerId
		AND Category NOT IN 
		(
			SELECT Category
			FROM 
				Condition_Category cc2
				LEFT JOIN Task t2 ON cc2.ConditionCategoryId = t2.CondCategoryId
			WHERE 
					cc2.BrokerId = @BrokerId AND t2.BrokerId = @BrokerId
				AND t2.LoanId = @LoanId
				AND t2.TaskIsCondition = 1 AND t2.CondIsDeleted = 0
			GROUP BY Category
		)
END