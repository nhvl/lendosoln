-- =============================================
-- Author:		Paolo Arrastia
-- Create date: 2/25/2013
-- Description:	
-- =============================================
CREATE PROCEDURE dbo.GetQueryForBatchExport
	-- Add the parameters for the stored procedure here
	@BrokerId UniqueIdentifier, 
	@MapId varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT QueryId
	FROM dbo.BROKER_XSLT_REPORT_MAP
	WHERE BrokerId = @BrokerId AND XsltMapId = @MapId
END
