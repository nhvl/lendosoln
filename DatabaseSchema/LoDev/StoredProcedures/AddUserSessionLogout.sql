ALTER PROCEDURE [dbo].[AddUserSessionLogout] 
	@SessionId varchar(13),
	@LogoutDate datetime
AS
BEGIN
	IF NOT EXISTS
			( SELECT SessionId from Login_Session where SessionId = @SessionId)
	BEGIN
		INSERT INTO Login_Session (SessionId, LogoutDate)
		VALUES ( @SessionId, @LogoutDate )
	END
END