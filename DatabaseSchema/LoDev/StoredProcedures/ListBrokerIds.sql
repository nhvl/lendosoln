-- =============================================
-- Author:		???
-- Create date: ???
-- Description:	Retrieves a list of brokerIds
-- Modified:    3/8/12, Matthew Pham
--              Now only retrieves active brokers.
-- =============================================
ALTER PROCEDURE [dbo].[ListBrokerIds]
	@SuiteType tinyint = NULL
AS
	SELECT BrokerId
	FROM Broker
	WHERE Status = 1 AND (@SuiteType IS NULL OR SuiteType = @SuiteType)
