CREATE PROCEDURE ListDiscTracksByCreatorUserId
	@UserId Guid
AS
	SELECT
		tr.TrackId,
		tr.TrackCreatedDate,
		tr.NotifId,
		tr.DiscLogId,
		tr.TrackNotifIsRead,
		notif.NotifReceiverUserId,
		notif.NotifReceiverLoginNm,
		notif.NotifIsValid,
		notif.NotifInvalidDate,
		disc.DiscRefObjId,
		disc.DiscRefObjNm1,
		disc.DiscRefObjNm2,
		disc.DiscSubject,
		disc.DiscPriority,
		disc.DiscStatus,
		disc.DiscDueDate,
		e.UserFirstNm,
		e.UserLastNm
	FROM
		( track tr WITH (NOLOCK) JOIN Discussion_Notification notif WITH (NOLOCK) ON tr.notifid = notif.notifid 
                                         JOIN Discussion_Notification AS ex WITH (NOLOCK) ON tr.DiscLogId = ex.DiscLogId AND ex.NotifReceiverUserId = @UserId AND ex.NotifIsValid = 1 
                                         LEFT JOIN Employee AS e WITH (NOLOCK) ON e.EmployeeUserId =notif.NotifReceiverUserId) JOIN Discussion_Log disc ON tr.DiscLogId = disc.DiscLogId AND disc.DiscIsRefObjValid = 1 AND disc.IsValid = 1
	WHERE
		tr.TrackIsValid = 1
		AND
		tr.TrackCreatorUserId = @UserId
