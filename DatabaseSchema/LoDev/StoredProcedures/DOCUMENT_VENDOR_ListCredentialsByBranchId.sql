-- =============================================
-- Author:		paoloa
-- Create date: 8/2013
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE dbo.DOCUMENT_VENDOR_ListCredentialsByBranchId
	-- Add the parameters for the stored procedure here
	@BranchId uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM DOCUMENT_VENDOR_BRANCH_CREDENTIALS
	WHERE BranchId = @BranchId
END
