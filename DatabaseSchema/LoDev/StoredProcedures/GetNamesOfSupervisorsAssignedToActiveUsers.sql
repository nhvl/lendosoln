-- =============================================
-- Author:		Michael Leinweaver
-- Create date: 8/26/2016
-- Description:	Given a set of potential PML supervisor IDs, 
--				determines which IDs correspond to PML supervisors 
--				that are currently assigned to active PML users.
-- =============================================
ALTER PROCEDURE [dbo].[GetNamesOfSupervisorsAssignedToActiveUsers]
	@BrokerId uniqueidentifier,
	@SupervisorIdXml xml
AS
BEGIN
	SELECT v.EmployeeId, v.UserFirstNm + ' ' + v.UserLastNm as "Name"
	FROM 
		@SupervisorIdXml.nodes('root/id') as T(Item) JOIN 
		VIEW_ACTIVE_PML_USER v ON T.Item.value('.', 'uniqueidentifier') = v.EmployeeId
	WHERE 
		v.BrokerId = @BrokerId AND 
		v.IsPmlManager = 1 AND
		EXISTS(SELECT TOP 1 v2.EmployeeId FROM VIEW_ACTIVE_PML_USER v2 WHERE v2.BrokerId = @BrokerId AND v2.PmlExternalManagerEmployeeId = v.EmployeeId)
END
