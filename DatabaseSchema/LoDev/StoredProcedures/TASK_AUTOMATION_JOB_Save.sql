CREATE PROCEDURE [dbo].[TASK_AUTOMATION_JOB_Save]
	@JobId int,
	@BrokerId uniqueidentifier,
	@LoanId uniqueidentifier,
	@TaskId varchar(10),
	@TaskAutomationJobType int
AS
BEGIN
	INSERT INTO TASK_AUTOMATION_JOB
	(
		JobId,
		BrokerId,
		LoanId,
		TaskId,
		TaskAutomationJobType
	)
	VALUES
	(
		@JobId,
		@BrokerId,
		@LoanId,
		@TaskId,
		@TaskAutomationJobType
	)
END