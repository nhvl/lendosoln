-- 07/14/06 mf OPM 6364.  We now restrict result to 100,
-- to cut down on timeouts.

-- 08/21/06 mf OPM 7053. We now provide a site constant for
-- the results limit.




ALTER PROCEDURE [dbo].[ListActivePMLUsersByBrokerId]
	@BrokerID uniqueidentifier, 
	@NameFilter Varchar(32) = NULL,
	@LastFilter Varchar(32) = NULL, 
	@PriceGroup uniqueidentifier = NULL,
	@PmlExternalManagerEmployeeId uniqueidentifier = NULL, -- 04/11/06 mf OPM 3340
	@LenderAccExecEmployeeId uniqueidentifier = NULL, -- 05/18/06 mf OPM 5006
	@LenderAccExecEmployeeNone bit = 0,
	@UnderwriterEmployeeNone bit = 0,
	@LockDeskEmployeeNone bit = 0,
	@ManagerEmployeeNone bit = 0,
	@ProcessorEmployeeNone bit = 0,
	@PriceGroupNone bit = 0,
	@PmlExternalManagerEmployeeNone bit = 0,
	@UnderwriterEmployeeId uniqueidentifier = NULL,
	@LockDeskEmployeeId uniqueidentifier = NULL,
	@ManagerEmployeeId uniqueidentifier = NULL,
	@ProcessorEmployeeId uniqueidentifier = NULL,
	@LimitResultSet bit = 1,
	@LastLoginDate datetime = NULL,
	@BeforeAfter Varchar(32) = NULL,
	@BranchId uniqueidentifier = NULL,
	@PmlBrokerId UniqueIdentifier = NULL,
	@JuniorUnderwriterEmployeeNone bit = 0,
	@JuniorProcessorEmployeeNone bit = 0,
	@JuniorUnderwriterEmployeeId uniqueidentifier = NULL,
	@JuniorProcessorEmployeeId uniqueidentifier = NULL,
	@PriceGroupUseOc bit = 0,
	@BranchUseOc bit = 0,
	@MiniCorrManagerEmployeeNone bit = 0,
	@MiniCorrManagerEmployeeId uniqueidentifier = null,
	@MiniCorrProcessorEmployeeNone bit = 0,
	@MiniCorrProcessorEmployeeId uniqueidentifier = null,
	@MiniCorrJuniorProcessorEmployeeNone bit = 0,
	@MiniCorrJuniorProcessorEmployeeId uniqueidentifier = null,
	@MiniCorrLenderAccExecEmployeeNone bit = 0,
	@MiniCorrLenderAccExecEmployeeId uniqueidentifier = null,
	@MiniCorrUnderwriterEmployeeNone bit = 0,
	@MiniCorrUnderwriterEmployeeId uniqueidentifier = null,
	@MiniCorrJuniorUnderwriterEmployeeNone bit = 0,
	@MiniCorrJuniorUnderwriterEmployeeId uniqueidentifier = null,
	@MiniCorrCreditAuditorEmployeeNone bit = 0,
	@MiniCorrCreditAuditorEmployeeId uniqueidentifier = null,
	@MiniCorrLegalAuditorEmployeeNone bit = 0,
	@MiniCorrLegalAuditorEmployeeId uniqueidentifier = null,
	@MiniCorrLockDeskEmployeeNone bit = 0,
	@MiniCorrLockDeskEmployeeId uniqueidentifier = null,
	@MiniCorrPurchaserEmployeeNone bit = 0,
	@MiniCorrPurchaserEmployeeId uniqueidentifier = null,
	@MiniCorrSecondaryEmployeeNone bit = 0,
	@MiniCorrSecondaryEmployeeId uniqueidentifier = null,
	@CorrManagerEmployeeNone bit = 0,
	@CorrManagerEmployeeId uniqueidentifier = null,
	@CorrProcessorEmployeeNone bit = 0,
	@CorrProcessorEmployeeId uniqueidentifier = null,
	@CorrJuniorProcessorEmployeeNone bit = 0,
	@CorrJuniorProcessorEmployeeId uniqueidentifier = null,
	@CorrLenderAccExecEmployeeNone bit = 0,
	@CorrLenderAccExecEmployeeId uniqueidentifier = null,
	@CorrUnderwriterEmployeeNone bit = 0,
	@CorrUnderwriterEmployeeId uniqueidentifier = null,
	@CorrJuniorUnderwriterEmployeeNone bit = 0,
	@CorrJuniorUnderwriterEmployeeId uniqueidentifier = null,
	@CorrCreditAuditorEmployeeNone bit = 0,
	@CorrCreditAuditorEmployeeId uniqueidentifier = null,
	@CorrLegalAuditorEmployeeNone bit = 0,
	@CorrLegalAuditorEmployeeId uniqueidentifier = null,
	@CorrLockDeskEmployeeNone bit = 0,
	@CorrLockDeskEmployeeId uniqueidentifier = null,
	@CorrPurchaserEmployeeNone bit = 0,
	@CorrPurchaserEmployeeId uniqueidentifier = null,
	@CorrSecondaryEmployeeNone bit = 0,
	@CorrSecondaryEmployeeId uniqueidentifier = null,
	@MiniCorrBranchId uniqueidentifier = null,
	@MiniCorrBranchUseOc bit = 0,
	@MiniCorrPriceGroupId uniqueidentifier = null,
	@MiniCorrPriceGroupUseOc bit = 0,
	@MiniCorrPriceGroupNone bit = 0,
	@CorrBranchId uniqueidentifier = null,
	@CorrBranchUseOc bit = 0,
	@CorrPriceGroupId uniqueidentifier = null,
	@CorrPriceGroupUseOc bit = 0,
	@CorrPriceGroupNone bit = 0
AS

IF @LimitResultSet = 1
BEGIN
	SELECT TOP 100 v.UserId, v.EmployeeId, v.LoginNm, v.UserFirstNm + ' ' + v.UserLastNm AS UserName, v.Permissions,
	               v.LenderAccExecEmployeeId, v.LockDeskEmployeeId, v.UnderwriterEmployeeId, v.ManagerEmployeeId, v.ProcessorEmployeeId,
	               v.Type, v.IsPmlManager, v.IsActive as Status, v.PmlExternalManagerEmployeeId, v.IsActive, v.CanLogin, v.LpePriceGroupId,
	               v.RecentLoginD, v.BranchNm, v.PmlBrokerName, v.UseOriginatingCompanyLpePriceGroupId, v.UseOriginatingCompanyBranchId,
	               v.PmlBrokerId,

MAX(CASE WHEN RoleID = 'B3785253-D9E8-4A3F-B0EC-D02D6FCF8538' THEN 'Processor (External), ' ELSE '' END) + 
MAX(CASE WHEN RoleID = '6DFC9B0E-11E3-48C1-B410-A1B0B5363B69' THEN 'Loan Officer, ' ELSE '' END) + 
MAX(CASE WHEN RoleID = '86C86CF3-FEB6-400F-80DC-123363A79605' THEN 'Supervisor, ' ELSE '' END) +
MAX(CASE WHEN RoleID = '274981E4-8C18-4204-B97B-2F537922CCD9' THEN 'Secondary (External), ' ELSE '' END) +
MAX(CASE WHEN RoleID = '89811C63-6A28-429A-BB45-3152449D854E' THEN 'Post-Closer (External), ' ELSE '' END)

 as role, v.JuniorUnderwriterEmployeeId, v.JuniorProcessorEmployeeId,
	v.MiniCorrManagerEmployeeId, v.MiniCorrProcessorEmployeeId, v.MiniCorrJuniorProcessorEmployeeId,
	v.MiniCorrLenderAccExecEmployeeId, v.MiniCorrUnderwriterEmployeeId, v.MiniCorrJuniorUnderwriterEmployeeId,
	v.MiniCorrCreditAuditorEmployeeId, v.MiniCorrLegalAuditorEmployeeId, v.MiniCorrLockDeskEmployeeId,
	v.MiniCorrPurchaserEmployeeId, v.MiniCorrSecondaryEmployeeId, v.MiniCorrespondentPriceGroupId,
	v.MiniCorrespondentBranchId,
	v.CorrManagerEmployeeId, v.CorrProcessorEmployeeId, v.CorrJuniorProcessorEmployeeId,
	v.CorrLenderAccExecEmployeeId, v.CorrUnderwriterEmployeeId, v.CorrJuniorUnderwriterEmployeeId,
	v.CorrCreditAuditorEmployeeId, v.CorrLegalAuditorEmployeeId, v.CorrLockDeskEmployeeId,
	v.CorrPurchaserEmployeeId, v.CorrSecondaryEmployeeId, v.CorrespondentPriceGroupId,
	v.CorrespondentBranchId, v.PopulatePmlPermissionsT, v.BranchId

	FROM View_Active_Pml_User v WITH (NOLOCK) 
	left join role_assignment ra on ra.EmployeeId = v.EmployeeId 
	WHERE v.BrokerId = @BrokerId
	AND Type = 'P'
	AND
	(
		(@PriceGroupUseOc = 1 AND v.UseOriginatingCompanyLpePriceGroupId = 1)
		OR
		(
			@PriceGroupUseOc = 0
			AND
			(
				v.LpePriceGroupId = COALESCE(@PriceGroup, v.LpePriceGroupId)
				OR
				(@PriceGroup IS NULL AND v.LpePriceGroupId IS NULL)
			)
		)
	)
	AND
	(
		(@PriceGroupNone = 1 AND v.LpePriceGroupId IS NULL)
		OR
		(@PriceGroupNone = 0)	
	)
	AND
	( 
		(@BranchUseOc = 1 AND v.UseOriginatingCompanyBranchId = 1)
		OR
		(
			@BranchUseOc = 0
			AND
			(
				(@BranchId IS NOT NULL AND @BranchId = v.BranchId)
				OR
				(@BranchId IS NULL)
			)
		)
	)
		      AND (
				( (@LastLoginDate IS NOT NULL AND @BeforeAfter = 'before') AND ((v.RecentLoginD < @LastLoginDate) OR v.RecentLoginD IS NULL) )
				OR
				( (@LastLoginDate IS NOT NULL AND @BeforeAfter = 'after') AND v.RecentLoginD >= @LastLoginDate)
				OR
				( @LastLoginDate IS NULL)
	                  )
		      AND (
				( @LastFilter IS NULL AND ( v.UserFirstNm LIKE COALESCE(@NameFilter, '') + '%'
	                                                                                      OR  v.UserLastNm LIKE COALESCE(@NameFilter, '') + '%')
	                                                                                      OR v.LoginNm LIKE COALESCE(@NameFilter, '') + '%')
				OR
				( @LastFilter IS NOT NULL AND ( v.UserFirstNm LIKE @NameFilter + '%' AND v.UserLastNm LIKE @LastFilter + '%') )
	                  )
		      AND (
				( @PmlExternalManagerEmployeeId IS NOT NULL AND v.PmlExternalManagerEmployeeId = @PmlExternalManagerEmployeeId)
				OR
				( @PmlExternalManagerEmployeeId IS NULL)
		          )
			AND (
				(@PmlExternalManagerEmployeeNone = 1 AND v.PmlExternalManagerEmployeeId IS NULL)
				OR
				(@PmlExternalManagerEmployeeNone = 0)	
				)
		AND (
			(@LenderAccExecEmployeeId IS NOT NULL AND @LenderAccExecEmployeeId = v.LenderAccExecEmployeeId)
			OR
			(@LenderAccExecEmployeeId IS NULL)
		    )
		AND (
				(@LenderAccExecEmployeeNone = 1 AND v.LenderAccExecEmployeeId IS NULL)
				OR
				(@LenderAccExecEmployeeNone = 0)	
			)
		AND (
				(@UnderwriterEmployeeNone = 1 AND v.UnderwriterEmployeeId IS NULL)
				OR
				(@UnderwriterEmployeeNone = 0)	
			)
		AND (
				(@LockDeskEmployeeNone = 1 AND v.LockDeskEmployeeId IS NULL)
				OR
				(@LockDeskEmployeeNone = 0)	
			)
		AND (
				(@ManagerEmployeeNone = 1 AND v.ManagerEmployeeId IS NULL)
				OR
				(@ManagerEmployeeNone = 0)	
			)
		AND (
				(@ProcessorEmployeeNone = 1 AND v.ProcessorEmployeeId IS NULL)
				OR
				(@ProcessorEmployeeNone = 0)	
			)
		AND (
				(@JuniorUnderwriterEmployeeNone = 1 AND v.JuniorUnderwriterEmployeeId IS NULL)
				OR
				(@JuniorUnderwriterEmployeeNone = 0)	
			)
		AND (
				(@JuniorProcessorEmployeeNone = 1 AND v.JuniorProcessorEmployeeId IS NULL)
				OR
				(@JuniorProcessorEmployeeNone = 0)	
			)
		AND (
			(@UnderwriterEmployeeId IS NOT NULL AND @UnderwriterEmployeeId = v.UnderwriterEmployeeId)
			OR
			(@UnderwriterEmployeeId IS NULL)
		    )
	
		AND (
			(@LockDeskEmployeeId IS NOT NULL AND @LockDeskEmployeeId = v.LockDeskEmployeeId)
			OR
			(@LockDeskEmployeeId IS NULL)
		    )
	
		AND ((@ManagerEmployeeId IS NOT NULL AND v.ManagerEmployeeId = @ManagerEmployeeId) 
			OR (@ManagerEmployeeId IS NULL))
		AND (
			(@ProcessorEmployeeId IS NOT NULL AND @ProcessorEmployeeId = v.ProcessorEmployeeId)
			OR
			(@ProcessorEmployeeId IS NULL)
		    )
		AND (
			(@JuniorProcessorEmployeeId IS NOT NULL AND @JuniorProcessorEmployeeId = v.JuniorProcessorEmployeeId)
			OR
			(@JuniorProcessorEmployeeId IS NULL)
			)
		AND (
			(@JuniorUnderwriterEmployeeId IS NOT NULL AND @JuniorUnderwriterEmployeeId = v.JuniorUnderwriterEmployeeId)
			OR
			(@JuniorUnderwriterEmployeeId IS NULL)
			)
		AND (
				(@MiniCorrManagerEmployeeNone = 1 AND v.MiniCorrManagerEmployeeId IS NULL)
				OR
				(@MiniCorrManagerEmployeeNone = 0)
			)
		AND (
				(@MiniCorrManagerEmployeeId IS NOT NULL AND @MiniCorrManagerEmployeeId = v.MiniCorrManagerEmployeeId)
				OR
				(@MiniCorrManagerEmployeeId IS NULL)
			)
		AND (
				(@MiniCorrProcessorEmployeeNone = 1 AND v.MiniCorrProcessorEmployeeId IS NULL)
				OR
				(@MiniCorrProcessorEmployeeNone = 0)
			)
		AND (
				(@MiniCorrProcessorEmployeeId IS NOT NULL AND @MiniCorrProcessorEmployeeId = v.MiniCorrProcessorEmployeeId)
				OR
				(@MiniCorrProcessorEmployeeId IS NULL)
			)
		AND (
				(@MiniCorrJuniorProcessorEmployeeNone = 1 AND v.MiniCorrJuniorProcessorEmployeeId IS NULL)
				OR
				(@MiniCorrJuniorProcessorEmployeeNone = 0)
			)
		AND (
				(@MiniCorrJuniorProcessorEmployeeId IS NOT NULL AND @MiniCorrJuniorProcessorEmployeeId = v.MiniCorrJuniorProcessorEmployeeId)
				OR
				(@MiniCorrJuniorProcessorEmployeeId IS NULL)
			)
		AND (
				(@MiniCorrLenderAccExecEmployeeNone = 1 AND v.MiniCorrLenderAccExecEmployeeId IS NULL)
				OR
				(@MiniCorrLenderAccExecEmployeeNone = 0)
			)
		AND (
				(@MiniCorrLenderAccExecEmployeeId IS NOT NULL AND @MiniCorrLenderAccExecEmployeeId = v.MiniCorrLenderAccExecEmployeeId)
				OR
				(@MiniCorrLenderAccExecEmployeeId IS NULL)
			)
		AND (
				(@MiniCorrUnderwriterEmployeeNone = 1 AND v.MiniCorrUnderwriterEmployeeId IS NULL)
				OR
				(@MiniCorrUnderwriterEmployeeNone = 0)
			)
		AND (
				(@MiniCorrUnderwriterEmployeeId IS NOT NULL AND @MiniCorrUnderwriterEmployeeId = v.MiniCorrUnderwriterEmployeeId)
				OR
				(@MiniCorrUnderwriterEmployeeId IS NULL)
			)
		AND (
				(@MiniCorrJuniorUnderwriterEmployeeNone = 1 AND v.MiniCorrJuniorUnderwriterEmployeeId IS NULL)
				OR
				(@MiniCorrJuniorUnderwriterEmployeeNone = 0)
			)
		AND (
				(@MiniCorrJuniorUnderwriterEmployeeId IS NOT NULL AND @MiniCorrJuniorUnderwriterEmployeeId = v.MiniCorrJuniorUnderwriterEmployeeId)
				OR
				(@MiniCorrJuniorUnderwriterEmployeeId IS NULL)
			)
		AND (
				(@MiniCorrCreditAuditorEmployeeNone = 1 AND v.MiniCorrCreditAuditorEmployeeId IS NULL)
				OR
				(@MiniCorrCreditAuditorEmployeeNone = 0)
			)
		AND (
				(@MiniCorrCreditAuditorEmployeeId IS NOT NULL AND @MiniCorrCreditAuditorEmployeeId = v.MiniCorrCreditAuditorEmployeeId)
				OR
				(@MiniCorrCreditAuditorEmployeeId IS NULL)
			)
		AND (
				(@MiniCorrLegalAuditorEmployeeNone = 1 AND v.MiniCorrLegalAuditorEmployeeId IS NULL)
				OR
				(@MiniCorrLegalAuditorEmployeeNone = 0)
			)
		AND (
				(@MiniCorrLegalAuditorEmployeeId IS NOT NULL AND @MiniCorrLegalAuditorEmployeeId = v.MiniCorrLegalAuditorEmployeeId)
				OR
				(@MiniCorrLegalAuditorEmployeeId IS NULL)
			)
		AND (
				(@MiniCorrLockDeskEmployeeNone = 1 AND v.MiniCorrLockDeskEmployeeId IS NULL)
				OR
				(@MiniCorrLockDeskEmployeeNone = 0)
			)
		AND (
				(@MiniCorrLockDeskEmployeeId IS NOT NULL AND @MiniCorrLockDeskEmployeeId = v.MiniCorrLockDeskEmployeeId)
				OR
				(@MiniCorrLockDeskEmployeeId IS NULL)
			)
		AND (
				(@MiniCorrPurchaserEmployeeNone = 1 AND v.MiniCorrPurchaserEmployeeId IS NULL)
				OR
				(@MiniCorrPurchaserEmployeeNone = 0)
			)
		AND (
				(@MiniCorrPurchaserEmployeeId IS NOT NULL AND @MiniCorrPurchaserEmployeeId = v.MiniCorrPurchaserEmployeeId)
				OR
				(@MiniCorrPurchaserEmployeeId IS NULL)
			)
		AND (
				(@MiniCorrSecondaryEmployeeNone = 1 AND v.MiniCorrSecondaryEmployeeId IS NULL)
				OR
				(@MiniCorrSecondaryEmployeeNone = 0)
			)
		AND (
				(@MiniCorrSecondaryEmployeeId IS NOT NULL AND @MiniCorrSecondaryEmployeeId = v.MiniCorrSecondaryEmployeeId)
				OR
				(@MiniCorrSecondaryEmployeeId IS NULL)
			)
		AND (
				(@CorrManagerEmployeeNone = 1 AND v.CorrManagerEmployeeId IS NULL)
				OR
				(@CorrManagerEmployeeNone = 0)
			)
		AND (
				(@CorrManagerEmployeeId IS NOT NULL AND @CorrManagerEmployeeId = v.CorrManagerEmployeeId)
				OR
				(@CorrManagerEmployeeId IS NULL)
			)
		AND (
				(@CorrProcessorEmployeeNone = 1 AND v.CorrProcessorEmployeeId IS NULL)
				OR
				(@CorrProcessorEmployeeNone = 0)
			)
		AND (
				(@CorrProcessorEmployeeId IS NOT NULL AND @CorrProcessorEmployeeId = v.CorrProcessorEmployeeId)
				OR
				(@CorrProcessorEmployeeId IS NULL)
			)
		AND (
				(@CorrJuniorProcessorEmployeeNone = 1 AND v.CorrJuniorProcessorEmployeeId IS NULL)
				OR
				(@CorrJuniorProcessorEmployeeNone = 0)
			)
		AND (
				(@CorrJuniorProcessorEmployeeId IS NOT NULL AND @CorrJuniorProcessorEmployeeId = v.CorrJuniorProcessorEmployeeId)
				OR
				(@CorrJuniorProcessorEmployeeId IS NULL)
			)
		AND (
				(@CorrLenderAccExecEmployeeNone = 1 AND v.CorrLenderAccExecEmployeeId IS NULL)
				OR
				(@CorrLenderAccExecEmployeeNone = 0)
			)
		AND (
				(@CorrLenderAccExecEmployeeId IS NOT NULL AND @CorrLenderAccExecEmployeeId = v.CorrLenderAccExecEmployeeId)
				OR
				(@CorrLenderAccExecEmployeeId IS NULL)
			)
		AND (
				(@CorrUnderwriterEmployeeNone = 1 AND v.CorrUnderwriterEmployeeId IS NULL)
				OR
				(@CorrUnderwriterEmployeeNone = 0)
			)
		AND (
				(@CorrUnderwriterEmployeeId IS NOT NULL AND @CorrUnderwriterEmployeeId = v.CorrUnderwriterEmployeeId)
				OR
				(@CorrUnderwriterEmployeeId IS NULL)
			)
		AND (
				(@CorrJuniorUnderwriterEmployeeNone = 1 AND v.CorrJuniorUnderwriterEmployeeId IS NULL)
				OR
				(@CorrJuniorUnderwriterEmployeeNone = 0)
			)
		AND (
				(@CorrJuniorUnderwriterEmployeeId IS NOT NULL AND @CorrJuniorUnderwriterEmployeeId = v.CorrJuniorUnderwriterEmployeeId)
				OR
				(@CorrJuniorUnderwriterEmployeeId IS NULL)
			)
		AND (
				(@CorrCreditAuditorEmployeeNone = 1 AND v.CorrCreditAuditorEmployeeId IS NULL)
				OR
				(@CorrCreditAuditorEmployeeNone = 0)
			)
		AND (
				(@CorrCreditAuditorEmployeeId IS NOT NULL AND @CorrCreditAuditorEmployeeId = v.CorrCreditAuditorEmployeeId)
				OR
				(@CorrCreditAuditorEmployeeId IS NULL)
			)
		AND (
				(@CorrLegalAuditorEmployeeNone = 1 AND v.CorrLegalAuditorEmployeeId IS NULL)
				OR
				(@CorrLegalAuditorEmployeeNone = 0)
			)
		AND (
				(@CorrLegalAuditorEmployeeId IS NOT NULL AND @CorrLegalAuditorEmployeeId = v.CorrLegalAuditorEmployeeId)
				OR
				(@CorrLegalAuditorEmployeeId IS NULL)
			)
		AND (
				(@CorrLockDeskEmployeeNone = 1 AND v.CorrLockDeskEmployeeId IS NULL)
				OR
				(@CorrLockDeskEmployeeNone = 0)
			)
		AND (
				(@CorrLockDeskEmployeeId IS NOT NULL AND @CorrLockDeskEmployeeId = v.CorrLockDeskEmployeeId)
				OR
				(@CorrLockDeskEmployeeId IS NULL)
			)
		AND (
				(@CorrPurchaserEmployeeNone = 1 AND v.CorrPurchaserEmployeeId IS NULL)
				OR
				(@CorrPurchaserEmployeeNone = 0)
			)
		AND (
				(@CorrPurchaserEmployeeId IS NOT NULL AND @CorrPurchaserEmployeeId = v.CorrPurchaserEmployeeId)
				OR
				(@CorrPurchaserEmployeeId IS NULL)
			)
		AND (
				(@CorrSecondaryEmployeeNone = 1 AND v.CorrSecondaryEmployeeId IS NULL)
				OR
				(@CorrSecondaryEmployeeNone = 0)
			)
		AND (
				(@CorrSecondaryEmployeeId IS NOT NULL AND @CorrSecondaryEmployeeId = v.CorrSecondaryEmployeeId)
				OR
				(@CorrSecondaryEmployeeId IS NULL)
			)
		AND
		(
			(@MiniCorrPriceGroupUseOc = 1 AND v.UseOriginatingCompanyMiniCorrPriceGroupId = 1)
			OR
			(
				(@MiniCorrPriceGroupUseOc = 0)
				AND
				(
					(@MiniCorrPriceGroupId IS NOT NULL AND @MiniCorrPriceGroupId = v.MiniCorrespondentPriceGroupId)
					OR
					(@MiniCorrPriceGroupId IS NULL)
				)
			)
		)
		AND
		(
			(@MiniCorrPriceGroupNone = 1 AND v.MiniCorrespondentPriceGroupId IS NULL)
			OR
			(@MiniCorrPriceGroupNone = 0)	
		)
		AND
		( 
			(@MiniCorrBranchUseOc = 1 AND v.UseOriginatingCompanyMiniCorrBranchId = 1)
			OR
			(
				(@MiniCorrBranchUseOc = 0)
				AND
				(
					(@MiniCorrBranchId IS NOT NULL AND @MiniCorrBranchId = v.MiniCorrespondentBranchId)
					OR
					(@MiniCorrBranchId IS NULL)
				)
			)
		)
		AND
		(
			(@CorrPriceGroupUseOc = 1 AND v.UseOriginatingCompanyCorrPriceGroupId = 1)
			OR
			(
				(@CorrPriceGroupUseOc = 0)
				AND
				(
					(@CorrPriceGroupId IS NOT NULL AND @CorrPriceGroupId = v.CorrespondentPriceGroupId)
					OR
					(@CorrPriceGroupId IS NULL)
				)
			)
		)
		AND
		(
			(@CorrPriceGroupNone = 1 AND v.CorrespondentPriceGroupId IS NULL)
			OR
			(@CorrPriceGroupNone = 0)	
		)
		AND
		( 
			(@CorrBranchUseOc = 1 AND v.UseOriginatingCompanyCorrBranchId = 1)
			OR
			(
				(@CorrBranchUseOc = 0)
				AND
				(
					(@CorrBranchId IS NOT NULL AND @CorrBranchId = v.CorrespondentBranchId)
					OR
					(@CorrBranchId IS NULL)
				)
			)
		)
		AND v.PmlBrokerId = COALESCE(@PmlBrokerId, PmlBrokerId)
	group by v.employeeid,
v.UserId, v.LoginNm, v.UserFirstNm,v.UserLastNm , v.Permissions,
	               v.LenderAccExecEmployeeId, v.LockDeskEmployeeId, v.UnderwriterEmployeeId, v.ManagerEmployeeId, v.ProcessorEmployeeId,
	               v.Type, v.IsPmlManager, v.IsActive, v.PmlExternalManagerEmployeeId, v.IsActive, v.CanLogin, v.LpePriceGroupId,
	               v.RecentLoginD, v.BranchNm, v.PmlBrokerName, v.JuniorUnderwriterEmployeeId, v.JuniorProcessorEmployeeId,
	               v.UseOriginatingCompanyLpePriceGroupId, v.UseOriginatingCompanyBranchId, PmlBrokerId,
	               v.MiniCorrManagerEmployeeId, v.MiniCorrProcessorEmployeeId, v.MiniCorrJuniorProcessorEmployeeId,
				   v.MiniCorrLenderAccExecEmployeeId, v.MiniCorrUnderwriterEmployeeId, v.MiniCorrJuniorUnderwriterEmployeeId,
				   v.MiniCorrCreditAuditorEmployeeId, v.MiniCorrLegalAuditorEmployeeId, v.MiniCorrLockDeskEmployeeId,
				   v.MiniCorrPurchaserEmployeeId, v.MiniCorrSecondaryEmployeeId, v.MiniCorrespondentPriceGroupId,
				   v.MiniCorrespondentBranchId, -- This should probably pull the name.
				   v.CorrManagerEmployeeId, v.CorrProcessorEmployeeId, v.CorrJuniorProcessorEmployeeId,
				   v.CorrLenderAccExecEmployeeId, v.CorrUnderwriterEmployeeId, v.CorrJuniorUnderwriterEmployeeId,
				   v.CorrCreditAuditorEmployeeId, v.CorrLegalAuditorEmployeeId, v.CorrLockDeskEmployeeId,
				   v.CorrPurchaserEmployeeId, v.CorrSecondaryEmployeeId, v.CorrespondentPriceGroupId,
				   v.CorrespondentBranchId, -- This should probably pull the name. 
				   v.PopulatePmlPermissionsT, v.BranchId
END
ELSE
BEGIN
	SELECT v.UserId, v.EmployeeId, v.LoginNm, v.UserFirstNm + ' ' + v.UserLastNm AS UserName, v.Permissions,
	               v.LenderAccExecEmployeeId, v.LockDeskEmployeeId, v.UnderwriterEmployeeId, v.ManagerEmployeeId, v.ProcessorEmployeeId,
	               v.Type, v.IsPmlManager, v.IsActive as Status, v.PmlExternalManagerEmployeeId, v.IsActive, v.CanLogin, v.LpePriceGroupId,
	               v.RecentLoginD, v.BranchNm, v.PmlBrokerName, v.UseOriginatingCompanyLpePriceGroupId, v.UseOriginatingCompanyBranchId,
	               v.PmlBrokerId,
MAX(CASE WHEN RoleID = 'B3785253-D9E8-4A3F-B0EC-D02D6FCF8538' THEN 'Processor (External), ' ELSE '' END) + 
MAX(CASE WHEN RoleID = '6DFC9B0E-11E3-48C1-B410-A1B0B5363B69' THEN 'Loan Officer, ' ELSE '' END) + 
MAX(CASE WHEN RoleID = '86C86CF3-FEB6-400F-80DC-123363A79605' THEN 'Supervisor, ' ELSE '' END) +
MAX(CASE WHEN RoleID = '274981E4-8C18-4204-B97B-2F537922CCD9' THEN 'Secondary (External), ' ELSE '' END) +
MAX(CASE WHEN RoleID = '89811C63-6A28-429A-BB45-3152449D854E' THEN 'Post-Closer (External), ' ELSE '' END)

 as role, v.JuniorUnderwriterEmployeeId, v.JuniorProcessorEmployeeId,
 	v.MiniCorrManagerEmployeeId, v.MiniCorrProcessorEmployeeId, v.MiniCorrJuniorProcessorEmployeeId,
	v.MiniCorrLenderAccExecEmployeeId, v.MiniCorrUnderwriterEmployeeId, v.MiniCorrJuniorUnderwriterEmployeeId,
	v.MiniCorrCreditAuditorEmployeeId, v.MiniCorrLegalAuditorEmployeeId, v.MiniCorrLockDeskEmployeeId,
	v.MiniCorrPurchaserEmployeeId, v.MiniCorrSecondaryEmployeeId, v.MiniCorrespondentPriceGroupId,
	v.MiniCorrespondentBranchId, -- This should probably pull the name.
	v.CorrManagerEmployeeId, v.CorrProcessorEmployeeId, v.CorrJuniorProcessorEmployeeId,
	v.CorrLenderAccExecEmployeeId, v.CorrUnderwriterEmployeeId, v.CorrJuniorUnderwriterEmployeeId,
	v.CorrCreditAuditorEmployeeId, v.CorrLegalAuditorEmployeeId, v.CorrLockDeskEmployeeId,
	v.CorrPurchaserEmployeeId, v.CorrSecondaryEmployeeId, v.CorrespondentPriceGroupId,
	v.CorrespondentBranchId, -- This should probably pull the name. 
	v.PopulatePmlPermissionsT, v.BranchId
	FROM View_Active_Pml_User as v 
	left join role_assignment as ra on ra.EmployeeId = v.EmployeeId 
	WHERE v.BrokerId = @BrokerId
	AND Type = 'P'
		      AND (
				(@PriceGroupUseOc = 1 AND v.UseOriginatingCompanyLpePriceGroupId = 1)
				OR
				v.LpePriceGroupId = COALESCE(@PriceGroup, v.LpePriceGroupId)
				OR
				(@PriceGroup IS NULL AND v.LpePriceGroupId IS NULL)
			   )
				AND (
					(@PriceGroupNone = 1 AND v.LpePriceGroupId IS NULL)
					OR
					(@PriceGroupNone = 0)	
				)
		      AND ( 
				(@BranchUseOc = 1 AND v.UseOriginatingCompanyBranchId = 1)
				OR
				(@BranchId IS NOT NULL AND @BranchId = v.BranchId)
				OR
				(@BranchId IS NULL)
			   )
		      AND (
				( (@LastLoginDate IS NOT NULL AND @BeforeAfter = 'before') AND ((v.RecentLoginD < @LastLoginDate) OR v.RecentLoginD IS NULL) )
				OR
				( (@LastLoginDate IS NOT NULL AND @BeforeAfter = 'after') AND v.RecentLoginD >= @LastLoginDate)
				OR
				( @LastLoginDate IS NULL)
	                  )
		      AND (
				( @LastFilter IS NULL AND ( v.UserFirstNm LIKE COALESCE(@NameFilter, '') + '%'
	                                                                                      OR  v.UserLastNm LIKE COALESCE(@NameFilter, '') + '%')
	                                                                                      OR v.LoginNm LIKE COALESCE(@NameFilter, '') + '%')
				OR
				( @LastFilter IS NOT NULL AND ( v.UserFirstNm LIKE @NameFilter + '%' AND v.UserLastNm LIKE @LastFilter + '%') )
	                  )
		      AND (
				( @PmlExternalManagerEmployeeId IS NOT NULL AND v.PmlExternalManagerEmployeeId = @PmlExternalManagerEmployeeId)
				OR
				( @PmlExternalManagerEmployeeId IS NULL)
		          )
			AND (
				(@PmlExternalManagerEmployeeNone = 1 AND v.PmlExternalManagerEmployeeId IS NULL)
				OR
				(@PmlExternalManagerEmployeeNone = 0)	
			)
		AND (
			(@LenderAccExecEmployeeId IS NOT NULL AND @LenderAccExecEmployeeId = v.LenderAccExecEmployeeId)
			OR
			(@LenderAccExecEmployeeId IS NULL)
		    )
		AND (
				(@LenderAccExecEmployeeNone = 1 AND v.LenderAccExecEmployeeId IS NULL)
				OR
				(@LenderAccExecEmployeeNone = 0)	
			)
		AND (
				(@UnderwriterEmployeeNone = 1 AND v.UnderwriterEmployeeId IS NULL)
				OR
				(@UnderwriterEmployeeNone = 0)	
			)
		AND (
				(@LockDeskEmployeeNone = 1 AND v.LockDeskEmployeeId IS NULL)
				OR
				(@LockDeskEmployeeNone = 0)	
			)
		AND (
				(@ManagerEmployeeNone = 1 AND v.ManagerEmployeeId IS NULL)
				OR
				(@ManagerEmployeeNone = 0)	
			)
		AND (
				(@ProcessorEmployeeNone = 1 AND v.ProcessorEmployeeId IS NULL)
				OR
				(@ProcessorEmployeeNone = 0)	
			)
		AND (
				(@JuniorUnderwriterEmployeeNone = 1 AND v.JuniorUnderwriterEmployeeId IS NULL)
				OR
				(@JuniorUnderwriterEmployeeNone = 0)	
			)
		AND (
				(@JuniorProcessorEmployeeNone = 1 AND v.JuniorProcessorEmployeeId IS NULL)
				OR
				(@JuniorProcessorEmployeeNone = 0)	
			)
		AND (
			(@UnderwriterEmployeeId IS NOT NULL AND @UnderwriterEmployeeId = v.UnderwriterEmployeeId)
			OR
			(@UnderwriterEmployeeId IS NULL)
		    )
	
		AND (
			(@LockDeskEmployeeId IS NOT NULL AND @LockDeskEmployeeId = v.LockDeskEmployeeId)
			OR
			(@LockDeskEmployeeId IS NULL)
		    )
				AND ((@ManagerEmployeeId IS NOT NULL AND v.ManagerEmployeeId = @ManagerEmployeeId) 
			OR (@ManagerEmployeeId IS NULL))

		AND (
			(@ProcessorEmployeeId IS NOT NULL AND @ProcessorEmployeeId = v.ProcessorEmployeeId)
			OR
			(@ProcessorEmployeeId IS NULL)
		    )
		AND (
			(@JuniorProcessorEmployeeId IS NOT NULL AND @JuniorProcessorEmployeeId = v.JuniorProcessorEmployeeId)
			OR
			(@JuniorProcessorEmployeeId IS NULL)
			)
		AND (
			(@JuniorUnderwriterEmployeeId IS NOT NULL AND @JuniorUnderwriterEmployeeId = v.JuniorUnderwriterEmployeeId)
			OR
			(@JuniorUnderwriterEmployeeId IS NULL)
			)
		AND (
				(@MiniCorrManagerEmployeeNone = 1 AND v.MiniCorrManagerEmployeeId IS NULL)
				OR
				(@MiniCorrManagerEmployeeNone = 0)
			)
		AND (
				(@MiniCorrManagerEmployeeId IS NOT NULL AND @MiniCorrManagerEmployeeId = v.MiniCorrManagerEmployeeId)
				OR
				(@MiniCorrManagerEmployeeId IS NULL)
			)
		AND (
				(@MiniCorrProcessorEmployeeNone = 1 AND v.MiniCorrProcessorEmployeeId IS NULL)
				OR
				(@MiniCorrProcessorEmployeeNone = 0)
			)
		AND (
				(@MiniCorrProcessorEmployeeId IS NOT NULL AND @MiniCorrProcessorEmployeeId = v.MiniCorrProcessorEmployeeId)
				OR
				(@MiniCorrProcessorEmployeeId IS NULL)
			)
		AND (
				(@MiniCorrJuniorProcessorEmployeeNone = 1 AND v.MiniCorrJuniorProcessorEmployeeId IS NULL)
				OR
				(@MiniCorrJuniorProcessorEmployeeNone = 0)
			)
		AND (
				(@MiniCorrJuniorProcessorEmployeeId IS NOT NULL AND @MiniCorrJuniorProcessorEmployeeId = v.MiniCorrJuniorProcessorEmployeeId)
				OR
				(@MiniCorrJuniorProcessorEmployeeId IS NULL)
			)
		AND (
				(@MiniCorrLenderAccExecEmployeeNone = 1 AND v.MiniCorrLenderAccExecEmployeeId IS NULL)
				OR
				(@MiniCorrLenderAccExecEmployeeNone = 0)
			)
		AND (
				(@MiniCorrLenderAccExecEmployeeId IS NOT NULL AND @MiniCorrLenderAccExecEmployeeId = v.MiniCorrLenderAccExecEmployeeId)
				OR
				(@MiniCorrLenderAccExecEmployeeId IS NULL)
			)
		AND (
				(@MiniCorrUnderwriterEmployeeNone = 1 AND v.MiniCorrUnderwriterEmployeeId IS NULL)
				OR
				(@MiniCorrUnderwriterEmployeeNone = 0)
			)
		AND (
				(@MiniCorrUnderwriterEmployeeId IS NOT NULL AND @MiniCorrUnderwriterEmployeeId = v.MiniCorrUnderwriterEmployeeId)
				OR
				(@MiniCorrUnderwriterEmployeeId IS NULL)
			)
		AND (
				(@MiniCorrJuniorUnderwriterEmployeeNone = 1 AND v.MiniCorrJuniorUnderwriterEmployeeId IS NULL)
				OR
				(@MiniCorrJuniorUnderwriterEmployeeNone = 0)
			)
		AND (
				(@MiniCorrJuniorUnderwriterEmployeeId IS NOT NULL AND @MiniCorrJuniorUnderwriterEmployeeId = v.MiniCorrJuniorUnderwriterEmployeeId)
				OR
				(@MiniCorrJuniorUnderwriterEmployeeId IS NULL)
			)
		AND (
				(@MiniCorrCreditAuditorEmployeeNone = 1 AND v.MiniCorrCreditAuditorEmployeeId IS NULL)
				OR
				(@MiniCorrCreditAuditorEmployeeNone = 0)
			)
		AND (
				(@MiniCorrCreditAuditorEmployeeId IS NOT NULL AND @MiniCorrCreditAuditorEmployeeId = v.MiniCorrCreditAuditorEmployeeId)
				OR
				(@MiniCorrCreditAuditorEmployeeId IS NULL)
			)
		AND (
				(@MiniCorrLegalAuditorEmployeeNone = 1 AND v.MiniCorrLegalAuditorEmployeeId IS NULL)
				OR
				(@MiniCorrLegalAuditorEmployeeNone = 0)
			)
		AND (
				(@MiniCorrLegalAuditorEmployeeId IS NOT NULL AND @MiniCorrLegalAuditorEmployeeId = v.MiniCorrLegalAuditorEmployeeId)
				OR
				(@MiniCorrLegalAuditorEmployeeId IS NULL)
			)
		AND (
				(@MiniCorrLockDeskEmployeeNone = 1 AND v.MiniCorrLockDeskEmployeeId IS NULL)
				OR
				(@MiniCorrLockDeskEmployeeNone = 0)
			)
		AND (
				(@MiniCorrLockDeskEmployeeId IS NOT NULL AND @MiniCorrLockDeskEmployeeId = v.MiniCorrLockDeskEmployeeId)
				OR
				(@MiniCorrLockDeskEmployeeId IS NULL)
			)
		AND (
				(@MiniCorrPurchaserEmployeeNone = 1 AND v.MiniCorrPurchaserEmployeeId IS NULL)
				OR
				(@MiniCorrPurchaserEmployeeNone = 0)
			)
		AND (
				(@MiniCorrPurchaserEmployeeId IS NOT NULL AND @MiniCorrPurchaserEmployeeId = v.MiniCorrPurchaserEmployeeId)
				OR
				(@MiniCorrPurchaserEmployeeId IS NULL)
			)
		AND (
				(@MiniCorrSecondaryEmployeeNone = 1 AND v.MiniCorrSecondaryEmployeeId IS NULL)
				OR
				(@MiniCorrSecondaryEmployeeNone = 0)
			)
		AND (
				(@MiniCorrSecondaryEmployeeId IS NOT NULL AND @MiniCorrSecondaryEmployeeId = v.MiniCorrSecondaryEmployeeId)
				OR
				(@MiniCorrSecondaryEmployeeId IS NULL)
			)
		AND (
				(@CorrManagerEmployeeNone = 1 AND v.CorrManagerEmployeeId IS NULL)
				OR
				(@CorrManagerEmployeeNone = 0)
			)
		AND (
				(@CorrManagerEmployeeId IS NOT NULL AND @CorrManagerEmployeeId = v.CorrManagerEmployeeId)
				OR
				(@CorrManagerEmployeeId IS NULL)
			)
		AND (
				(@CorrProcessorEmployeeNone = 1 AND v.CorrProcessorEmployeeId IS NULL)
				OR
				(@CorrProcessorEmployeeNone = 0)
			)
		AND (
				(@CorrProcessorEmployeeId IS NOT NULL AND @CorrProcessorEmployeeId = v.CorrProcessorEmployeeId)
				OR
				(@CorrProcessorEmployeeId IS NULL)
			)
		AND (
				(@CorrJuniorProcessorEmployeeNone = 1 AND v.CorrJuniorProcessorEmployeeId IS NULL)
				OR
				(@CorrJuniorProcessorEmployeeNone = 0)
			)
		AND (
				(@CorrJuniorProcessorEmployeeId IS NOT NULL AND @CorrJuniorProcessorEmployeeId = v.CorrJuniorProcessorEmployeeId)
				OR
				(@CorrJuniorProcessorEmployeeId IS NULL)
			)
		AND (
				(@CorrLenderAccExecEmployeeNone = 1 AND v.CorrLenderAccExecEmployeeId IS NULL)
				OR
				(@CorrLenderAccExecEmployeeNone = 0)
			)
		AND (
				(@CorrLenderAccExecEmployeeId IS NOT NULL AND @CorrLenderAccExecEmployeeId = v.CorrLenderAccExecEmployeeId)
				OR
				(@CorrLenderAccExecEmployeeId IS NULL)
			)
		AND (
				(@CorrUnderwriterEmployeeNone = 1 AND v.CorrUnderwriterEmployeeId IS NULL)
				OR
				(@CorrUnderwriterEmployeeNone = 0)
			)
		AND (
				(@CorrUnderwriterEmployeeId IS NOT NULL AND @CorrUnderwriterEmployeeId = v.CorrUnderwriterEmployeeId)
				OR
				(@CorrUnderwriterEmployeeId IS NULL)
			)
		AND (
				(@CorrJuniorUnderwriterEmployeeNone = 1 AND v.CorrJuniorUnderwriterEmployeeId IS NULL)
				OR
				(@CorrJuniorUnderwriterEmployeeNone = 0)
			)
		AND (
				(@CorrJuniorUnderwriterEmployeeId IS NOT NULL AND @CorrJuniorUnderwriterEmployeeId = v.CorrJuniorUnderwriterEmployeeId)
				OR
				(@CorrJuniorUnderwriterEmployeeId IS NULL)
			)
		AND (
				(@CorrCreditAuditorEmployeeNone = 1 AND v.CorrCreditAuditorEmployeeId IS NULL)
				OR
				(@CorrCreditAuditorEmployeeNone = 0)
			)
		AND (
				(@CorrCreditAuditorEmployeeId IS NOT NULL AND @CorrCreditAuditorEmployeeId = v.CorrCreditAuditorEmployeeId)
				OR
				(@CorrCreditAuditorEmployeeId IS NULL)
			)
		AND (
				(@CorrLegalAuditorEmployeeNone = 1 AND v.CorrLegalAuditorEmployeeId IS NULL)
				OR
				(@CorrLegalAuditorEmployeeNone = 0)
			)
		AND (
				(@CorrLegalAuditorEmployeeId IS NOT NULL AND @CorrLegalAuditorEmployeeId = v.CorrLegalAuditorEmployeeId)
				OR
				(@CorrLegalAuditorEmployeeId IS NULL)
			)
		AND (
				(@CorrLockDeskEmployeeNone = 1 AND v.CorrLockDeskEmployeeId IS NULL)
				OR
				(@CorrLockDeskEmployeeNone = 0)
			)
		AND (
				(@CorrLockDeskEmployeeId IS NOT NULL AND @CorrLockDeskEmployeeId = v.CorrLockDeskEmployeeId)
				OR
				(@CorrLockDeskEmployeeId IS NULL)
			)
		AND (
				(@CorrPurchaserEmployeeNone = 1 AND v.CorrPurchaserEmployeeId IS NULL)
				OR
				(@CorrPurchaserEmployeeNone = 0)
			)
		AND (
				(@CorrPurchaserEmployeeId IS NOT NULL AND @CorrPurchaserEmployeeId = v.CorrPurchaserEmployeeId)
				OR
				(@CorrPurchaserEmployeeId IS NULL)
			)
		AND (
				(@CorrSecondaryEmployeeNone = 1 AND v.CorrSecondaryEmployeeId IS NULL)
				OR
				(@CorrSecondaryEmployeeNone = 0)
			)
		AND (
				(@CorrSecondaryEmployeeId IS NOT NULL AND @CorrSecondaryEmployeeId = v.CorrSecondaryEmployeeId)
				OR
				(@CorrSecondaryEmployeeId IS NULL)
			)
		AND
		(
			(@MiniCorrPriceGroupUseOc = 1 AND v.UseOriginatingCompanyMiniCorrPriceGroupId = 1)
			OR
			(
				(@MiniCorrPriceGroupUseOc = 0)
				AND
				(
					(@MiniCorrPriceGroupId IS NOT NULL AND @MiniCorrPriceGroupId = v.MiniCorrespondentPriceGroupId)
					OR
					(@MiniCorrPriceGroupId IS NULL)
				)
			)
		)
		AND
		(
			(@MiniCorrPriceGroupNone = 1 AND v.MiniCorrespondentPriceGroupId IS NULL)
			OR
			(@MiniCorrPriceGroupNone = 0)	
		)
		AND
		( 
			(@MiniCorrBranchUseOc = 1 AND v.UseOriginatingCompanyMiniCorrBranchId = 1)
			OR
			(
				(@MiniCorrBranchUseOc = 0)
				AND
				(
					(@MiniCorrBranchId IS NOT NULL AND @MiniCorrBranchId = v.MiniCorrespondentBranchId)
					OR
					(@MiniCorrBranchId IS NULL)
				)
			)
		)
		AND
		(
			(@CorrPriceGroupUseOc = 1 AND v.UseOriginatingCompanyCorrPriceGroupId = 1)
			OR
			(
				(@CorrPriceGroupUseOc = 0)
				AND
				(
					(@CorrPriceGroupId IS NOT NULL AND @CorrPriceGroupId = v.CorrespondentPriceGroupId)
					OR
					(@CorrPriceGroupId IS NULL)
				)
			)
		)
		AND
		(
			(@CorrPriceGroupNone = 1 AND v.CorrespondentPriceGroupId IS NULL)
			OR
			(@CorrPriceGroupNone = 0)	
		)
		AND
		( 
			(@CorrBranchUseOc = 1 AND v.UseOriginatingCompanyCorrBranchId = 1)
			OR
			(
				(@CorrBranchUseOc = 0)
				AND
				(
					(@CorrBranchId IS NOT NULL AND @CorrBranchId = v.CorrespondentBranchId)
					OR
					(@CorrBranchId IS NULL)
				)
			)
		)
		AND v.PmlBrokerId = COALESCE(@PmlBrokerId, PmlBrokerId)
group by v.employeeid,
v.UserId,  v.LoginNm, v.UserFirstNm , v.UserLastNm , v.Permissions,
	               v.LenderAccExecEmployeeId, v.LockDeskEmployeeId, v.UnderwriterEmployeeId, v.ManagerEmployeeId, v.ProcessorEmployeeId,
	               v.Type, v.IsPmlManager, v.IsActive, v.PmlExternalManagerEmployeeId, v.IsActive, v.CanLogin, v.LpePriceGroupId,
	               v.RecentLoginD, v.BranchNm, v.PmlBrokerName, v.JuniorUnderwriterEmployeeId, v.JuniorProcessorEmployeeId,
	               v.UseOriginatingCompanyLpePriceGroupId, v.UseOriginatingCompanyBranchId, PmlBrokerId,
	               v.MiniCorrManagerEmployeeId, v.MiniCorrProcessorEmployeeId, v.MiniCorrJuniorProcessorEmployeeId,
				   v.MiniCorrLenderAccExecEmployeeId, v.MiniCorrUnderwriterEmployeeId, v.MiniCorrJuniorUnderwriterEmployeeId,
				   v.MiniCorrCreditAuditorEmployeeId, v.MiniCorrLegalAuditorEmployeeId, v.MiniCorrLockDeskEmployeeId,
				   v.MiniCorrPurchaserEmployeeId, v.MiniCorrSecondaryEmployeeId, v.MiniCorrespondentPriceGroupId,
				   v.MiniCorrespondentBranchId, -- This should probably pull the name.
				   v.CorrManagerEmployeeId, v.CorrProcessorEmployeeId, v.CorrJuniorProcessorEmployeeId,
				   v.CorrLenderAccExecEmployeeId, v.CorrUnderwriterEmployeeId, v.CorrJuniorUnderwriterEmployeeId,
				   v.CorrCreditAuditorEmployeeId, v.CorrLegalAuditorEmployeeId, v.CorrLockDeskEmployeeId,
				   v.CorrPurchaserEmployeeId, v.CorrSecondaryEmployeeId, v.CorrespondentPriceGroupId,
				   v.CorrespondentBranchId, -- This should probably pull the name. 
				   v.PopulatePmlPermissionsT, v.BranchId
END












