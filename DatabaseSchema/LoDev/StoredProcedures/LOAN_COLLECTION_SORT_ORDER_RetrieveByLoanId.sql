-- ============================================================
-- Author:      Alan Daughton
-- Create date: 4/17/2018
-- Description: Retrieve all rows from the LOAN_COLLECTION_SORT_ORDER table associated with a given loan.
-- ============================================================
ALTER PROCEDURE [dbo].[LOAN_COLLECTION_SORT_ORDER_RetrieveByLoanId]
	@LoanId UniqueIdentifier
AS
BEGIN
	SELECT
		CollectionName,
		SortOrder,
		AppId,
		ConsumerId,
		UladApplicationId
	FROM [dbo].[LOAN_COLLECTION_SORT_ORDER]
	WHERE LoanId = @LoanId
END
