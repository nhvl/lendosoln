-- =============================================
-- Author:		David Dao
-- Create date: 
-- Description:	Retrieve all the roles assign to employee
-- =============================================
CREATE PROCEDURE dbo.ROLE_ASSIGNMENT_ListByEmployeeId
    @EmployeeId UniqueIdentifier
AS
BEGIN
    SELECT RoleId 
    FROM ROLE_ASSIGNMENT
    WHERE EmployeeId = @EmployeeId
    
END
