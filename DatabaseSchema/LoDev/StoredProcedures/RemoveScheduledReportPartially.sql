CREATE procedure [dbo].[RemoveScheduledReportPartially]
@ReportId int
as 

delete from DAILY_REPORTS where ReportId = @ReportId;
delete from WEEKLY_REPORTS where ReportId = @ReportId;
delete from MONTHLY_REPORTS where ReportId = @ReportId;