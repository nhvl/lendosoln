-- =============================================
-- Author:		Michael Leinweaver
-- Create date: 2/5/2016
-- Description:	Lists all of the brokers with the "Always Display Exact Par Rate Option"
--				enabled.
-- =============================================
ALTER PROCEDURE [dbo].[SAEDB_ListBrokersWithExactParRateOption] 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT b.CustomerCode as 'CustomerCode', b.BrokerNm as 'CompanyName'
	FROM Broker b
	WHERE b.Status = 1 and b.IsAlwaysDisplayExactParRateOption = 1
	ORDER BY b.CustomerCode
END
