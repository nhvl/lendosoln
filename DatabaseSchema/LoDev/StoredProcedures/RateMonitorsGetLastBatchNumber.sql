-- =============================================
-- Author:		Paolo Arrastia
-- Create date: 25 Jul 2012
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[RateMonitorsGetLastBatchNumber] 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT MAX(LastRunBatchId) AS StartBatch
	FROM RATE_MONITOR
END
