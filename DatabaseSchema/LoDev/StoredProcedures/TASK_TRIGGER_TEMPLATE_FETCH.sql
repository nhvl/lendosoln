-- =============================================
-- Author:		Antonio Valencia
-- Create date: 12/16/11
-- Description:	Fetches Template
--				Added LastModifiedDate, AssignedUserId, OwnerUserId, CondIsHidden, CondInternalNotes - MP 1/11/12
-- =============================================
ALTER PROCEDURE [dbo].[TASK_TRIGGER_TEMPLATE_FETCH] 
	-- Add the parameters for the stored procedure here
	@BrokerId uniqueidentifier,
	@AutoTaskTemplateId int = null
AS
BEGIN
	IF @AutoTaskTemplateId IS NULL 
	BEGIN 
		SELECT 
			AutoTaskTemplateId,
			BrokerId,
			Subject,
			TriggerName,
			Frequency,
			LastModifiedDate,
			AssignedUserId,
			ToBeAssignedRoleId,
			OwnerUserId,
			ToBeOwnedByRoleId,
			TaskPermissionLevelId,
			DueDateCalculatedFromField,
			DueDateCalculationOffset,
			IsConditionTemplate,
			CondIsHidden,
			CondInternalNotes,
			ConditionCategoryId,
			Comments,
			CondRequiredDocTypeId,
			ResolutionBlockTriggerName,
			ResolutionDenialMessage,
			ResolutionDateSetterFieldId,
			AutoResolveTriggerName,
			AutoCloseTriggerName
			
		FROM TASK_TRIGGER_TEMPLATE
		WHERE BrokerId = @BrokerId 
		
	END
	ELSE 
	BEGIN
		SELECT 
			AutoTaskTemplateId,
			BrokerId,
			Subject,
			TriggerName,
			Frequency,
			LastModifiedDate,
			AssignedUserId,
			ToBeAssignedRoleId,
			OwnerUserId,
			ToBeOwnedByRoleId,
			TaskPermissionLevelId,
			DueDateCalculatedFromField,
			DueDateCalculationOffset,
			IsConditionTemplate,
			CondIsHidden,
			CondInternalNotes,
			ConditionCategoryId,
			Comments,
			CondRequiredDocTypeId,
			ResolutionBlockTriggerName,
			ResolutionDenialMessage,
			ResolutionDateSetterFieldId,
			AutoResolveTriggerName,
			AutoCloseTriggerName
			
		FROM TASK_TRIGGER_TEMPLATE
		WHERE BrokerId = @BrokerId 
		AND AutoTaskTemplateId = @AutoTaskTemplateId
	END
END
