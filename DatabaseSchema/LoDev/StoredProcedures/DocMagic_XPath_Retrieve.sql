CREATE PROCEDURE [dbo].[DocMagic_XPath_Retrieve]
	@XPath varchar(150)
AS
BEGIN
	select * from DOCMAGIC_XPATH_MAPPINGS where XPath = coalesce(@XPath, XPath)
END
