-- =============================================
-- Author:		Justin Kim
-- Create date: 5/15/18
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[WORKFLOW_CHANGE_AUDIT_CREATE]	
	@BrokerId uniqueidentifier,
	@UserId uniqueidentifier,
	@WorkflowAuditType tinyint,
	@AuditDescription varchar(100),
	@AuditDate Datetime,
	@IsInternalUser bit,
	@ReleaseConfigId bigint
AS
BEGIN
	INSERT INTO WORKFLOW_CHANGE_AUDIT (
		BrokerId,
		UserId,
		WorkflowAuditType,
		AuditDescription,
		AuditDate,
		IsInternalUser,
		ReleaseConfigId
	)
	VALUES (
		@BrokerId,
		@UserId,
		@WorkflowAuditType,
		@AuditDescription,
		@AuditDate,
		@IsInternalUser,
		@ReleaseConfigId
	)
END