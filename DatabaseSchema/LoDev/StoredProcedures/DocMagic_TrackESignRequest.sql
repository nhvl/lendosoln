-- =============================================
-- Author:		Antonio Valencia
-- Create date: April 16, 2012
-- Description:	Add ESign Request Info
-- =============================================
CREATE PROCEDURE [dbo].[DocMagic_TrackESignRequest] 
	-- Add the parameters for the stored procedure here
	@DocMagicAccountNumber varchar(20), 
	@sLId uniqueidentifier,
	@sLNm varchar(36),
	@EmployeeId uniqueidentifier,
	@BrokerId uniqueidentifier,
	@Username varchar(50),
	@Password varchar(150)
AS
BEGIN
	INSERT INTO DOCMAGIC_PENDING_ESIGN_REQUEST
	(DocMagicAccountNumber ,sLId, sLNm, EmployeeId, BrokerId, RequestedD, Username, Password)
	VALUES
	(@DocMagicAccountNumber ,@sLId, @sLNm, @EmployeeId, @BrokerId, GETDATE(), @Username, @Password)
END
