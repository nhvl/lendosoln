ALTER PROCEDURE [dbo].[GetPmldojoBrokerReportQueryTemplateId]
	@SampleCustomReportLoginNm varchar(50) = ''
AS
	SELECT 	
		q.QueryId
	FROM 
		View_lendersoffice_Employee AS e  WITH (NOLOCK) 
		JOIN Branch AS r WITH (NOLOCK) ON r.BranchId = e.BranchId
		JOIN Broker as br with (nolock) on r.brokerid = br.brokerid
		JOIN broker_user as bu with (nolock) on e.employeeid = bu.employeeid
		JOIN all_user as a with (nolock) on a.userid = bu.userid	
		JOIN Report_Query AS q with (nolock) on q.BrokerId = br.brokerid
	WHERE a.loginNm = @SampleCustomReportLoginNm
	
	IF( @@error != 0 )
	BEGIN
		RAISERROR('Error in the select statement in GetAdminBrokerReportQueryTemplateId sp', 16, 1);
		RETURN 0;
	END
	

