


CREATE PROCEDURE [dbo].[RetrieveBrokerIdByLendersOfficeLoginNm] 
	@LoginNm varchar(36)
AS
	SELECT BrokerID FROM VIEW_ACTIVE_LO_USER_WITH_BROKER_AND_LICENSE_INFO WITH (NOLOCK) WHERE LoginNm=@LoginNm