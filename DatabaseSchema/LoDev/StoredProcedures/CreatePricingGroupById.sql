ALTER PROCEDURE [dbo].[CreatePricingGroupById] 
	@LpePriceGroupId uniqueidentifier,
	@BrokerId uniqueidentifier,
	@ExternalPriceGroupEnabled bit,
	@LpePriceGroupName varchar(100),
	@LenderPaidOriginatorCompensationOptionT int = NULL,
	@LockPolicyID uniqueidentifier = null,
	@DisplayPmlFeeIn100Format bit = null,
	@IsRoundUpLpeFee bit = null,
	@RoundUpLpeFeeToInterval decimal(10,3) = null,
	@IsAlwaysDisplayExactParRateOption bit = null,
	@ActualPriceGroupId UniqueIdentifier = '00000000-0000-0000-0000-000000000000'
AS
BEGIN
	IF(	 -- if any of the broker fields are null, we can load them from the broker table as defaults.
	 	   @LockPolicyID is null
		OR @DisplayPmlFeeIn100Format is null
		OR @IsRoundUpLpeFee is null
		OR @RoundUpLpeFeeToInterval is null
		OR @IsAlwaysDisplayExactParRateOption is null
	)
	BEGIN
		SELECT 
				  @LockPolicyID = COALESCE(@LockPolicyID, DefaultLockPolicyID)
				, @DisplayPmlFeeIn100Format = COALESCE(@DisplayPmlFeeIn100Format, DisplayPmlFeeIn100Format)
				, @IsRoundUpLpeFee = COALESCE(@IsRoundUpLpeFee, IsRoundUpLpeFee)
				, @RoundUpLpeFeeToInterval = COALESCE(@RoundUpLpeFeeToInterval, RoundUpLpeFeeToInterval)
				, @IsAlwaysDisplayExactParRateOption = COALESCE(@IsAlwaysDisplayExactParRateOption, IsAlwaysDisplayExactParRateOption)
		FROM broker
		WHERE brokerid = @BrokerId
	END
	
	
	INSERT INTO LPE_Price_Group(LpePriceGroupId,   BrokerId,  ActualPriceGroupId, ExternalPriceGroupEnabled,  LpePriceGroupName,           LenderPaidOriginatorCompensationOptionT,		 LockPolicyID,  DisplayPmlFeeIn100Format,  IsRoundUpLpeFee,  RoundUpLpeFeeToInterval, IsAlwaysDisplayExactParRateOption)
						VALUES (@LpePriceGroupId, @BrokerId, @ActualPriceGroupId, @ExternalPriceGroupEnabled, @LpePriceGroupName, COALESCE(@LenderPaidOriginatorCompensationOptionT, 1), @LockPolicyID, @DisplayPmlFeeIn100Format, @IsRoundUpLpeFee, @RoundUpLpeFeeToInterval, @IsAlwaysDisplayExactParRateOption)
END


