-- =============================================
-- Author:		Scott Kibler
-- Create date: 7/11/2014
-- Description:	
--   Gets an unused loan (at the broker if it matches the qp template)
--   and marks it as in use
-- =============================================
ALTER PROCEDURE [dbo].[QUICKPRICER_2_LOAN_USAGE_GetSandboxedUnusedLoanAndMarkAsInUse]
	
	@BrokerID UniqueIdentifier,
	@QuickPricerTemplateID UniqueIdentifier,
	@UserIDofUsingUser UniqueIdentifier,
	
	@BranchChannelT int = 0,
	@CorrespondentProcessT int = 0,
	@IsWholesale bit = 0,
	
	-- optional parameters:
	@QuickPricerTemplateFileVersion INT = NULL, -- supplying the value makes it faster, but allows out of sync.
	
	-- output parameters:
	@LoanID UniqueIdentifier = NULL OUTPUT
AS
BEGIN
    -- Grab the matching QPTemplate file version, or error if none exists.
	IF @QuickPricerTemplateFileVersion IS NULL
	BEGIN
		SELECT @QuickPricerTemplateFileVersion = sFileVersion
		FROM LOAN_FILE_A
		WHERE sLId = @QuickPricerTemplateID
		IF( @@ROWCOUNT <> 1 )
		BEGIN
			RAISERROR ('No loan with the given qp template id found',
						 16,
						 1)

			RETURN
		END 
	END
	
	
	-- Find a pooled loan that is not in use if one exists and lock and update it.
	BEGIN TRAN TRAN1 -- default transaction isolation is READ COMMITED
		-- instead of using updlck, may want to use OUTPUT clause, but moving on for time.
		-- http://stackoverflow.com/questions/497464/is-there-a-way-to-select-and-update-rows-at-the-same-time
		
		IF(@IsWholesale = 1) 
		BEGIN
			SELECT TOP 1
				@LoanID = LoanID
			FROM 
				QUICKPRICER_2_LOAN_USAGE as q WITH(UPDLOCK) JOIN LOAN_FILE_CACHE_2 as lc2 on (q.LoanID = lc2.sLId) JOIN LOAN_FILE_CACHE_3 as lc3 on (q.LoanID = lc3.sLId) -- holds updlock until tran1 complete.
			WHERE 
					BrokerID = @BrokerID
				AND IsInUse = 0
				AND QuickPricerTemplateID = @QuickPricerTemplateID
				AND QuickPricerTemplateFileVersion = @QuickPricerTemplateFileVersion
				AND UserIDofUsingUser = @UserIDofUsingUser
				AND sBranchChannelT = @BranchChannelT
			IF( @@ROWCOUNT <> 1 )
			BEGIN
				-- LoanID is NULL
				ROLLBACK TRAN TRAN1
				RETURN
			END
		END
		ELSE IF (@BranchChannelT > 0)
		BEGIN
			SELECT TOP 1
				@LoanID = LoanID
			FROM 
				QUICKPRICER_2_LOAN_USAGE as q WITH(UPDLOCK) JOIN LOAN_FILE_CACHE_2 as lc2 on (q.LoanID = lc2.sLId) JOIN LOAN_FILE_CACHE_3 as lc3 on (q.LoanID = lc3.sLId) -- holds updlock until tran1 complete.
			WHERE 
					BrokerID = @BrokerID
				AND IsInUse = 0
				AND QuickPricerTemplateID = @QuickPricerTemplateID
				AND QuickPricerTemplateFileVersion = @QuickPricerTemplateFileVersion
				AND UserIDofUsingUser = @UserIDofUsingUser
				AND sBranchChannelT = @BranchChannelT
				AND sCorrespondentProcessT = @CorrespondentProcessT
			IF( @@ROWCOUNT <> 1 )
			BEGIN
				-- LoanID is NULL
				ROLLBACK TRAN TRAN1
				RETURN
			END
		END
		ELSE
		BEGIN
			SELECT TOP 1
			@LoanID = LoanID
			FROM 
				QUICKPRICER_2_LOAN_USAGE WITH(UPDLOCK) -- holds updlock until tran1 complete.
			WHERE 
					BrokerID = @BrokerID
				AND IsInUse = 0
				AND QuickPricerTemplateID = @QuickPricerTemplateID
				AND QuickPricerTemplateFileVersion = @QuickPricerTemplateFileVersion
				AND UserIDofUsingUser = @UserIDofUsingUser
			IF( @@ROWCOUNT <> 1 )
			BEGIN
				-- LoanID is NULL
				ROLLBACK TRAN TRAN1
				RETURN
			END 
		END
		
		UPDATE 
			QUICKPRICER_2_LOAN_USAGE
		SET 
			IsInUse = 1,
			LastUsedD = GetDate(),
			UserIDofUsingUser = @UserIDofUsingUser
		WHERE 
			LoanID = @LoanID
	COMMIT	
END
