-- =============================================
-- Author:		Eric Mallare
-- Create date: 9/12/2016
-- Description:	Edits an existing Broker Client Certificate
-- =============================================
ALTER PROCEDURE [dbo].[EditBrokerClientCertificate] 
	@CertificateId uniqueidentifier,
	@BrokerId uniqueidentifier,
	@Description varchar(100),
	@NotesFileDbKey uniqueidentifier,
	@GroupIdsXml xml = null,
	@GroupType int = null 
AS
BEGIN
	UPDATE 
		Broker_Client_Certificate
	SET
		Description=@Description,
		NotesFileDbKey=@NotesFileDbKey
	WHERE
		CertificateId=@CertificateId AND
		BrokerId=@BrokerId
		
	IF @GroupIdsXml IS NOT NULL AND @GroupType IS NOT NULL
	BEGIN
		DELETE FROM 
			Group_X_Broker_Client_Certificate
		WHERE
			CertificateId=@CertificateId
			
		INSERT INTO
			Group_X_Broker_Client_Certificate(GroupId, CertificateId, GroupType)
		SELECT
			T.Item.value('.', 'uniqueidentifier'), @CertificateId, @GroupType
		FROM
			@GroupIdsXml.nodes('root/group/@id') as T(Item) 
	END
END
