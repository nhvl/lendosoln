ALTER PROCEDURE [dbo].[CP_FindConsumer]
    @Email Varchar( 80 ) = NULL , 
    @BrokerNm Varchar( 128 ) = NULL , 
    @IsLocked Bit = NULL
AS

    SELECT TOP 100
		cp.ConsumerId,
		cp.Email,
		cp.IsLocked,
		cp.BrokerId ,
        cp.LastLoggedInD,
        br.BrokerNm ,
        br.BrokerPmlSiteId
    FROM
		CP_CONSUMER AS cp WITH (NOLOCK)
		JOIN Broker as br with (nolock) on cp.brokerid = br.brokerid
	WHERE
		((@Email IS NULL)		OR (cp.Email Like '' + @Email + '%'))
	AND
		((@BrokerNm IS NULL)	OR (br.BrokerNm Like '' + @BrokerNm + '%'))
	AND
		((@IsLocked IS NULL)	OR (cp.IsLocked = @IsLocked))
