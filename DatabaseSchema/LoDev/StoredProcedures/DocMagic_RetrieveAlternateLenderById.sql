ALTER proc [dbo].[DocMagic_RetrieveAlternateLenderById]
	@DocMagicAlternateLenderId uniqueidentifier
as
select LenderName, LenderAddress, LenderCity, LenderState, LenderZip, LenderCounty, LenderNonPersonEntityIndicator, 
BeneficiaryName, BeneficiaryAddress, BeneficiaryCity, BeneficiaryState, BeneficiaryZip, BeneficiaryCounty,
BeneficiaryNonPersonEntityIndicator, LossPayeeName, LossPayeeAddress, LossPayeeCity, LossPayeeState, 
LossPayeeZip, LossPayeeCounty, MakePaymentsToName, MakePaymentsToAddress, MakePaymentsToCity, MakePaymentsToState, 
MakePaymentsToZip, MakePaymentsToCounty, WhenRecodedMailToName, WhenRecodedMailToAddress, WhenRecodedMailToCity, 
WhenRecodedMailToState, WhenRecodedMailToZip, WhenRecodedMailToCounty, DocMagicAlternateLenderInternalId
from DOCMAGIC_ALTERNATE_LENDER
where DocMagicAlternateLenderId = @DocMagicAlternateLenderId