-- ============================================================
-- Author:      Generated by application LqbEntitiesToSqlTables
-- Create date: 5/21/2018
-- Description: Deletes a row from the CONSUMER_X_ASSET table.
-- ============================================================
CREATE PROCEDURE [dbo].[CONSUMER_X_ASSET_Delete]
	@ConsumerAssetAssociationId UniqueIdentifier,
	@LoanId UniqueIdentifier
AS
BEGIN
	DELETE FROM [dbo].[CONSUMER_X_ASSET]
	WHERE
		ConsumerAssetAssociationId = @ConsumerAssetAssociationId
		AND LoanId = @LoanId
END
