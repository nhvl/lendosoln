-- =============================================
-- Author:		Geoff Feltman
-- Create date: 9/9/14
-- Description:	Gets the loans for auto-disclosure.
-- =============================================
CREATE PROCEDURE [dbo].[ListLoansForAutoDisclosure]
	@BrokerId uniqueidentifier,
	@BranchId uniqueidentifier = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT lfc.sLId
	FROM LOAN_FILE_CACHE lfc 
		JOIN LOAN_FILE_CACHE_3 lfc3 ON lfc.sLId = lfc3.sLId
		JOIN loan_file_cache_2 lfc2 ON lfc.slid = lfc2.slid
	WHERE lfc.sBrokerId = @BrokerId
		AND (@BranchId IS NULL OR sBranchId = @BranchId)
		AND sLoanFileT = 0
		AND sNeedInitialDisc = 1
		AND lfc.IsValid = 1
		AND lfc.sStatusT NOT IN 
		(
			-- If you are updating this list of statuses, please also update
			-- ConstApp.StatusesExcludedFromAutoDisclosure
			6,  /* Loan Funded */
			7,  /* Loan On-Hold */
			9,  /* Loan Canceled */
			10, /* Loan Denied */
			11, /* Loan Closed */
			15, /* Lead Canceled */
			16, /* Lead Declined */
			17, /* Lead Other */
			18, /* Loan Other */
			19, /* Recorded */
			20, /* Loan Shipped */
			24, /* Docs Back */
			25, /* Funding Conditions */
			26, /* Final Docs */
			27, /* Loan Sold */
			37, /* Investor Conditions */
			38, /* Investor Conditions Sent */
			39, /* Ready for Sale*/
			40, /* Submitted for Purchase Review */
			41, /* In Purchase Review */
			42, /* Pre-Purchase Conditions */
			43, /* Submitted for Final Purchase Review */
			44, /* In Final Purchase Review */
			45, /* Clear to Purchase */
			46, /* Loan Purchased */
			47, /* Counter Offer Approved */
			48, /* Loan Withdrawn */
			49  /* Loan Archived */
		)
		AND lfc2.sBranchChannelT != 3 -- correspondent
		AND lfc3.sDisclosureNeededT != 6 -- E-Consent Expired
		AND lfc3.sDisclosureNeededT != 3 -- E-Consent Declined
END
