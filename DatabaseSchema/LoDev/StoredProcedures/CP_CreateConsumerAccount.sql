CREATE PROCEDURE CP_CreateConsumerAccount	
	@Email varchar(80),
	@BrokerId uniqueidentifier,
	@passwordHash varchar(200)
AS
BEGIN	
	SET NOCOUNT ON;
	
	IF NOT EXISTS (SELECT * FROM CP_CONSUMER where Email=@Email AND BrokerId=@BrokerId)
	BEGIN
		INSERT INTO CP_CONSUMER(Email, BrokerId, PasswordHash) VALUES(@Email, @BrokerId, @passwordHash)
	END
END
