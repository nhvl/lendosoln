ALTER PROCEDURE [dbo].[SecurityAnswersSavedForUser]
	@UserId uniqueidentifier
AS
BEGIN
	IF EXISTS (
		SELECT UserId
		FROM BROKER_USER
		WHERE UserId = @UserId
			AND (SecurityQuestion1Id IS NULL OR SecurityQuestion2Id IS NULL OR SecurityQuestion3Id IS NULL
				OR (SecurityQuestion1Answer = '' AND SecurityQuestion1EncryptedAnswer = 0x)
				OR (SecurityQuestion2Answer = '' AND SecurityQuestion2EncryptedAnswer = 0x)
				OR (SecurityQuestion3Answer='' AND SecurityQuestion3EncryptedAnswer = 0x))
	)
		RETURN -1
	
RETURN 1
END
