CREATE PROCEDURE [dbo].[DeleteRolodex] 
	@RolodexID Guid
AS

-- Remove agent from PML Broker Affiliates lists first --

DELETE FROM PML_BROKER_x_AGENT WHERE AgentID = @RolodexID

DELETE FROM Agent WHERE AgentID = @RolodexID
if(0!=@@error)
begin
	RAISERROR('Error in deleting from Agent table in DeleteRolodex', 16, 1);
	return -100;
end
return 0;
