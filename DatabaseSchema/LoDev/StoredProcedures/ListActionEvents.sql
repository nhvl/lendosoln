
CREATE PROCEDURE [dbo].[ListActionEvents]
	@BrokerName varchar(32) = NULL, @UserId Guid = NULL, @BrokerId Guid = NULL, @TypeDesc varchar(36) = NULL, @StartDate datetime = NULL, @FinalDate datetime = NULL
AS
	SELECT a.UserId AS UserId , a.UserFirstNm + ' ' + a.UserLastNm AS UserName, a.EventType AS EventType , a.NewActivationCap AS EventArg , a.EventDate AS EventDate , a.NewActivationCap AS NewActivationCap , a.BrokerId AS BrokerId , b.BrokerNm AS BrokerName
	FROM
		Action_Event AS a LEFT JOIN Broker AS b ON b.BrokerId = a.BrokerId LEFT JOIN All_User AS u ON u.UserId = a.UserId
	WHERE
		a.BrokerId = COALESCE( @BrokerId , a.BrokerId ) 
		AND
		DATEDIFF( day , COALESCE( @StartDate , a.EventDate ) , a.EventDate ) >= 0
		AND
		DATEDIFF( day , COALESCE( @FinalDate , a.EventDate ) , a.EventDate ) <= 0
		AND
		a.EventType = COALESCE( @TypeDesc , a.EventType )
		AND
		a.UserId = COALESCE( @UserId , a.UserId )
		AND
		b.BrokerNm LIKE COALESCE( @BrokerName + '%' , b.BrokerNm )
		AND
		(
			a.UserId = '00000000-0000-0000-0000-000000000000'
			OR
			u.Type = 'B'
		)
	ORDER BY
		BrokerName , UserName , a.EventType
	if( 0 != @@error )
	begin
		RAISERROR('Error querying Action_Event table in ListActionEvents sp', 16, 1);
		return -100;
	end
	return 0;

