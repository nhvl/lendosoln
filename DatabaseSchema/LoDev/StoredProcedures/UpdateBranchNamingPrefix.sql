CREATE  PROCEDURE [dbo].UpdateBranchNamingPrefix
	@BrokerID Guid,
	@BranchID Guid,
	@NamingPrefix varchar(10)
	
AS
	UPDATE Branch
	SET BranchLNmPrefix = @NamingPrefix
	WHERE BrokerId = @BrokerID AND BranchId = @BranchID
      IF(0!=@@error)
      BEGIN
            RAISERROR('Error in the update statement in UpdateBranchNamingPrefix sp', 16, 1);
            RETURN -100;
      END
      RETURN 0;
	
