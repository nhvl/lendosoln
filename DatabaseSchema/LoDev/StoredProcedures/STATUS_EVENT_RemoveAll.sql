﻿-- =============================================
-- Author:		Eduardo Michel
-- Create date: 
-- Description:	
-- =============================================
ALTER PROCEDURE [dbo].[STATUS_EVENT_RemoveAll] 
	-- Add the parameters for the stored procedure here
	@LoanId uniqueidentifier, 
	@BrokerId uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM STATUS_EVENT
	WHERE LoanId = @LoanId AND BrokerId = @BrokerId
END
