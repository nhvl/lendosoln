
-- =============================================
-- Author:		Peter Anargirou
-- Create date: 08/08/08
-- Description:	Returns a list of all stage constants
-- =============================================
CREATE PROCEDURE [dbo].[ListStageConstants]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT KeyIdStr, OptionContentInt, OptionContentStr, Owner, CreatedDate
	FROM STAGE_CONFIG
END

