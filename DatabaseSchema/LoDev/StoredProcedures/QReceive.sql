


/* Retrieves a message from the message queue with the given id and the message will be at the same or lower priority. 
if this method fails it returns a negative number. 
	-1 for sql error,
	-2 for No messages
	-3 for Message Has been Processed 
*/
ALTER PROCEDURE [dbo].[QReceive] (
	@QueueId int ,
	@Priority tinyint,
	@Archive bit
)
AS 
	DECLARE @Rowcount int;
	DECLARE @Error int;
	DECLARE @ReturnValue int;
	

    -- declare table variable
    DECLARE @tmpTable TABLE (
        MessageId bigint,
        QueueId int NOT NULL,
        Priority tinyint NOT NULL,
        Subject1 varchar(300) NOT NULL,
        Subject2 varchar(300) NULL,
        DataLoc varchar(10) NOT NULL,
        InsertionTime datetime NULL,
        Data varchar(5500) NOT NULL
    );    
	
    --Try to find a record that has the priority wanted or a lower ones. 
	DELETE FROM Q_MESSAGE WITH(ROWLOCK, READPAST, UPDLOCK)
        OUTPUT deleted.MessageId, deleted.QueueId, deleted.Priority, deleted.Subject1, deleted.Subject2, 
               deleted.DataLoc, deleted.InsertionTime, deleted.Data 
               INTO @tmpTable	
        WHERE MessageId in ( SELECT TOP 1 MessageId
                                FROM Q_MESSAGE WITH(ROWLOCK, READPAST, UPDLOCK)
                                WHERE QueueId = @QueueId AND Priority >= @Priority  
                                ORDER BY Priority ASC, MessageId ASC       
                           )         
        
    SELECT @Error = @@error, @Rowcount = @@rowcount; 
    IF @Error <> 0 
    BEGIN
            RAISERROR('Sql Error', 16, 1); ;
            SELECT @ReturnValue = -1;
            GOTO ERROR;
    END
    
	-- If there are none then get the top one from the queury. I believe this is more efficient then failing to reduce the database load
	IF @Rowcount = 0 
	BEGIN 
    
        DELETE FROM Q_MESSAGE WITH(ROWLOCK, READPAST, UPDLOCK)
            OUTPUT deleted.MessageId, deleted.QueueId, deleted.Priority, deleted.Subject1, deleted.Subject2, 
                   deleted.DataLoc, deleted.InsertionTime, deleted.Data 
                   INTO @tmpTable	
            WHERE MessageId in ( SELECT TOP 1 MessageId
                                    FROM Q_MESSAGE WITH(ROWLOCK, READPAST, UPDLOCK)
                                    WHERE QueueId = @QueueId
                                    ORDER BY MessageId ASC       
                               )             
    
		SELECT @Error = @@error, @Rowcount = @@rowcount; 
		IF @Error <> 0 
		BEGIN
				RAISERROR('Sql Error', 16, 1); ;
				SELECT @ReturnValue = -1;
				GOTO ERROR;
		END
		IF @Rowcount = 0 
		BEGIN
			RAISERROR('The queue with id %i is empty or does not exist.',   16, 1, @QueueId  );
			SELECT @ReturnValue = -2;
			GOTO ERROR;
		END 
	END
	
	--Copies the message into Q_ARCHIVE
	IF @Archive = 1	
	BEGIN
		INSERT INTO Q_ARCHIVE( InsertionTime, QueueId, Subject1, Data,  MessageId, Subject2, Priority, DataLoc) 
            SELECT InsertionTime, QueueId, Subject1, Data,  MessageId, Subject2, Priority, DataLoc
            FROM @tmpTable

        SELECT @Error = @@error, @Rowcount = @@rowcount; 
		IF @error <> 0
		BEGIN 
			RAISERROR('Unexpected sql error', 16, 1);
			SELECT @ReturnValue = -1
			GOTO ERROR	
		END
		IF @rowcount = 0 
		BEGIN
			RAISERROR('The message has been processed.', 16, 1);
			SELECT @ReturnValue = -3
			GOTO ERROR
		END
	END	
	

	SELECT @ReturnValue = 0 
	SELECT MessageId, Priority, Data, Subject2, Subject1, InsertionTime, DataLoc 
        FROM @tmpTable
	GOTO FINISH
	
	ERROR: 
		if @@TRANCOUNT > 0 
			ROLLBACK TRANSACTION; 
	FINISH:
		return @ReturnValue;
