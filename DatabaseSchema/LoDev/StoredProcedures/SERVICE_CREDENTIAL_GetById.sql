ALTER  PROCEDURE [dbo].[SERVICE_CREDENTIAL_GetById]
	@Id int,
	@BrokerId uniqueidentifier  
AS
	SELECT 
		sc.Id,
		sc.ServiceProviderId,
		sc.ServiceProviderName,
		sc.UserType,
		sc.AccountId,
		sc.UserName,
		sc.UserPassword,
		sc.EncryptionKeyId,
		sc.IsForCreditReports,
		sc.IsForVerifications,
		sc.VoxVendorId,
		sc.VoxVendorName,
		sc.IsForAusSubmission,
		sc.AusOption,
		sc.IsEnabledForNonSeamlessDu,
		sc.IsForDocumentCapture,
		sc.IsForTitleQuotes,
		sc.TitleQuoteLenderServiceId,
		sc.TitleQuoteLenderServiceName,
		sc.IsForUcdDelivery,
		sc.UcdDeliveryTarget,
		sc.IsForDigitalMortgage,
		sc.DigitalMortgageProviderTarget,
		ec.EmployeeId,
		pbsc.PmlBrokerId,
		brsc.BranchId
	FROM SERVICE_CREDENTIAL sc
		LEFT JOIN EMPLOYEE_X_SERVICE_CREDENTIAL ec ON sc.Id=ec.ServiceCredentialId
		LEFT JOIN PMLBROKER_X_SERVICE_CREDENTIAL pbsc ON sc.Id=pbsc.ServiceCredentialId
		LEFT JOIN BRANCH_X_SERVICE_CREDENTIAL brsc ON sc.Id=brsc.ServiceCredentialId
	WHERE
		@Id = id
		AND @BrokerId = sc.BrokerId
