-- =============================================
-- Author:		Michael Leinweaver
-- Create date: 7/24/2018
-- Description:	Retrieves the lender's OC index number
--				counter and then increments the value.
-- =============================================
CREATE PROCEDURE [dbo].[BROKER_GetAndUpdateOriginatingCompanyIndexNumberCounter]
	@BrokerId uniqueidentifier
AS
BEGIN
	DECLARE @OriginatingCompanyIndexNumberCounter int
	
	UPDATE BROKER
	SET
		@OriginatingCompanyIndexNumberCounter = OriginatingCompanyIndexNumberCounter,
		OriginatingCompanyIndexNumberCounter = OriginatingCompanyIndexNumberCounter + 1
	WHERE BrokerId = @BrokerId
	
	SELECT @OriginatingCompanyIndexNumberCounter AS 'OriginatingCompanyIndexNumberCounter'
END
