-- =============================================
-- Author:		Timothy Jewell
-- Create date: 9/9/2014
-- Description:	Retrieves the Generic Framework vendors, optionally restricted to a ProviderID.
-- =============================================
ALTER PROCEDURE [dbo].[GENERIC_FRAMEWORK_VENDOR_Retrieve]
	-- Add the parameters for the stored procedure here
	@ProviderID varchar(7) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Insert statements for procedure here
	IF @ProviderID IS NULL
		SELECT
			ProviderID,
			Name,
			LaunchURL,
			CredentialXML,
			EncryptedCredentialXML,
			EncryptionKeyId,
			IncludeUsername,
			ServiceType,
			LoanIdentifierTypeToSend
		FROM GENERIC_FRAMEWORK_VENDOR_CONFIG
	ELSE
		SELECT
			ProviderID,
			Name,
			LaunchURL,
			CredentialXML,
			EncryptedCredentialXML,
			EncryptionKeyId,
			IncludeUsername,
			ServiceType,
			LoanIdentifierTypeToSend
		FROM GENERIC_FRAMEWORK_VENDOR_CONFIG
		WHERE ProviderID = @ProviderID
END
