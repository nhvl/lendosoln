


-- =============================================
-- Author:		<Francesc>
-- Create date: <04/29/10>
-- Description:	<Retrieve loans associated with a given consumerId>
-- =============================================
CREATE PROCEDURE [dbo].[CP_RetrieveConsumerById]
	@ConsumerId bigint
	
AS
BEGIN
	SET NOCOUNT ON;

	SELECT DisclosureAcceptedD, ConsumerId, Email, BrokerId, PasswordHash, LastLoggedInD, PasswordRequestD, PasswordResetCode, IsTemporaryPassword, LoginFailureNumber, IsLocked, Last4SsnDigits, SsnAttemptFailureNumber, LastSsnAttemptFailDate
	FROM CP_CONSUMER
	WHERE ConsumerId = @ConsumerId

END



