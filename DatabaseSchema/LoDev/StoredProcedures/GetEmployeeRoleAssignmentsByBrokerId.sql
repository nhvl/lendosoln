ALTER PROCEDURE [dbo].[GetEmployeeRoleAssignmentsByBrokerId]
    @BrokerId uniqueidentifier,
    @Include_B_Users bit = 0,
    @Include_P_Users bit = 0    
AS
SELECT
    ra.EmployeeId,
    ra.RoleId
FROM
    dbo.ROLE_ASSIGNMENT ra 
    JOIN dbo.EMPLOYEE e on ra.EmployeeId = e.EmployeeId 
    JOIN dbo.ALL_USER au on au.UserId = e.EmployeeUserId
    JOIN dbo.BRANCH br on e.BranchId = br.BranchId
WHERE br.BrokerId = @BrokerId
AND ((au.Type = 'B' AND @Include_B_Users = 1) OR (au.Type = 'P' AND @Include_P_Users = 1))
