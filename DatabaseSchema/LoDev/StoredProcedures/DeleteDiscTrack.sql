CREATE PROCEDURE DeleteDiscTrack
	@TrackId Guid = NULL , @NotifId Guid = NULL , @CreatorUserId Guid = NULL , @CreatorLoginNm Varchar( 64 ) = NULL , @InvalidDate DateTime = NULL
AS
	IF( @CreatorUserId IS NULL )
	BEGIN
		SELECT @CreatorUserId = UserId FROM All_User WITH (NOLOCK) WHERE LOWER( LoginNm ) = LOWER( @CreatorLoginNm )
	END
	UPDATE Track
	SET
		TrackIsValid = 0,
		TrackInvalidDate = COALESCE( @InvalidDate , getdate() )
	WHERE
		TrackId = COALESCE( @TrackId , ( SELECT TrackId FROM Track WHERE NotifId = @NotifId AND TrackCreatorUserId = @CreatorUserId ) )
	IF( @@error != 0 )
	BEGIN
		RAISERROR('Error in the update statement in DeleteDiscTrack sp', 16, 1);
		RETURN -100;
	END
