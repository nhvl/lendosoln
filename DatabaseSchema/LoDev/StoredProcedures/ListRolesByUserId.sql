-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE dbo.ListRolesByUserId 
	@UserId UniqueIdentifier
AS
BEGIN
	SELECT RoleId
	FROM ROLE_ASSIGNMENT role JOIN Broker_User bu ON role.EmployeeId = bu.EmployeeId
	WHERE bu.UserId = @UserId
END
