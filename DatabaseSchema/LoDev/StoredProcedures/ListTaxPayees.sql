-- =============================================
-- Author:		Paolo Arrastia
-- Create date: 13 Aug 2012
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[ListTaxPayees] 
	-- Add the parameters for the stored procedure here
	@state varchar(2),
	@filter varchar(50) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT PayeeId, PayeeName, City, State
	FROM TAX_PAYEE
	WHERE 
		State LIKE @state
		AND
		(PayeeName LIKE '%'+@filter+'%'
		OR
		City LIKE '%'+@filter+'%');
END
