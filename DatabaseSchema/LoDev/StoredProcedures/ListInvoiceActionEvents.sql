CREATE PROCEDURE ListInvoiceActionEvents
	@BrokerId Guid = NULL , @StartDate datetime = NULL , @FinalDate datetime = NULL
AS
	SELECT a.UserId , a.EventType , a.NewActivationCap , MAX( a.EventDate ) AS EventDate , b.BrokerId , b.BrokerNm
	FROM
		Action_Event AS a LEFT JOIN Broker AS b ON b.BrokerId = a.BrokerId JOIN All_User AS z ON z.UserId = a.UserId
	WHERE
		a.BrokerId = @BrokerId
		AND
		DATEDIFF( day , COALESCE( @StartDate , a.EventDate ) , a.EventDate ) < 0
		AND
		(
			a.UserId = '00000000-0000-0000-0000-000000000000'
			OR
			z.Type = 'B'
		)
	GROUP BY
		a.UserId , a.EventType , a.NewActivationCap , b.BrokerId , b.BrokerNm
	SELECT a.UserId , a.EventType , a.NewActivationCap , a.EventDate , b.BrokerId , b.BrokerNm
	FROM
		Action_Event AS a LEFT JOIN Broker AS b ON b.BrokerId = a.BrokerId JOIN All_User AS z ON z.UserId = a.UserId
	WHERE
		a.BrokerId = @BrokerId
		AND
		DATEDIFF( day , COALESCE( @StartDate , a.EventDate ) , a.EventDate ) >= 0
		AND
		DATEDIFF( day , COALESCE( @FinalDate , a.EventDate ) , a.EventDate ) <= 0
		AND
		(
			a.UserId = '00000000-0000-0000-0000-000000000000'
			OR
			z.Type = 'B'
		)
	if( 0 != @@error )
	begin
		RAISERROR('Error querying ListInvoiceActionEvents sp', 16, 1);
		return -100;
	end
	return 0;
