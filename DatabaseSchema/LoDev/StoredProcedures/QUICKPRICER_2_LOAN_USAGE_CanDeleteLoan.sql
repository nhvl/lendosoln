/* =============================================
 Author:		Scott Kibler
 Create date: 8/6/2014
 Description:	
   Determines if the loan should be deleted (from the qp2.0 pool and all associated tables.)
   Returns false if:
	 - sLoanFileT (loan_file_a) != quickpricer2sandboxed
	 - The loan is in use and it's not sufficiently old.
	 - The loan is not in use but the quickpricertemplate matches the broker's.
   Does not check tables that foreign key to the rows to be deleted, as the key itself would cause deletion to fail.
   Returns a list of conditions associated.  If other fileDBKeys need to be added,
   they should be unioned with the condition select statement.
   E.g.
   UNION
   select '<tablename>' as TableName, <dbkeyColumn> as FileDBGuid where loanid, brokerid, etc.
  =============================================*/
ALTER PROCEDURE dbo.QUICKPRICER_2_LOAN_USAGE_CanDeleteLoan
	@BrokerID UniqueIdentifier,
	@LoanID UniqueIdentifier,
	--@UserIDofDeletingUser UniqueIdentifier, -- TODO: possibly keep a record of who deleted.
	@BrokerPMLTemplateID UniqueIdentifier, -- shouldn't match the loanid.
	@BrokerQuickPricerTemplateID UniqueIdentifier, -- shouldn't match the loanid.
	@QuickPricer2InUseLoanExpirationDays tinyint = 2, -- must be 2 or greater.  from conststage ideally.
	--// TODO: assert will delete from AuditTrailDaily file, DISCUSSION LOG FILES.
	--@TransientFhaConnectionLogHasNoEntry bit, -- no longer used.
	@IWillDeleteAllConditionsFromFileDB bit,
	@IWillDeleteFromTransient bit,
	@IWillDeleteAllHardCodedFileDBFiles bit, -- e.g. pin states, gfe archive, etc.
	@QuickPricer2NonPooledExpirationMinutes int = 30, -- This should not be less than how long it takes to commit a file and put it in the pool
	@CanDeleteLoan bit OUTPUT,
	@ReasonCantDeleteLoan varchar(1000) OUTPUT
AS
BEGIN
    
	DECLARE @RowCount int
	DECLARE @Error int
	DECLARE @sLoanFileT int
	DECLARE @IsInUse bit
	DECLARE @LastUsedD smalldatetime
	DECLARE @BrokerQuickPricerTemplateFileVersion int
	DECLARE @FilesQuickPricerTemplateID UniqueIdentifier
	DECLARE @FilesQuickPricerTemplateFileVersion int
	DECLARE @sCreatedD smalldatetime
	DECLARE @isValid bit
    
	print 'entering candelete loan.'
	
	SET @CanDeleteLoan = 0; -- I don't care what you pass in.  By default you cannot delete.
	SET @ReasonCantDeleteLoan = 'Not Determined.';
	
	--IF @TransientFhaConnectionLogHasNoEntry = 0
	--BEGIN
	--	SET @ReasonCantDeleteLoan = 'Check the Transient Fha_connection_log first.'
	--	RETURN 0;
	--END
	
	IF @IWillDeleteFromTransient <> 1
	BEGIN
		SET @ReasonCantDeleteLoan = 'You have to promise to delete from transient.'
		RETURN 0;
	END
	
	IF @IWillDeleteAllHardCodedFileDBFiles <> 1
	BEGIN
		SET @ReasonCantDeleteLoan = 'You have to promise to delete the hardcoded fileDB files.'
		RETURN 0;
	END
	IF @IWillDeleteAllConditionsFromFileDB <> 1
	BEGIN
		SET @ReasonCantDeleteLoan = 'You have to promise to delete the conditions from fileDB.'
		RETURN 0;
	END
	
	-- first stage is just blocking certain deletions.	
	IF @QuickPricer2InUseLoanExpirationDays IS NULL OR @QuickPricer2InUseLoanExpirationDays < 2
	BEGIN
		SET @ReasonCantDeleteLoan = 'Cannot delete loans less than 2 days old.'
		RETURN 0;
	END
	
	IF @LoanID = @BrokerPMLTemplateID
	BEGIN
		SET @ReasonCantDeleteLoan = 'Somehow this loan is the brokers pml template.  cant delete.'
		RETURN 0;
	END
	
	IF @LoanID = @BrokerQuickPricerTemplateID
	BEGIN
		SET @ReasonCantDeleteLoan = 'Somehow this loan is the brokers quickpricer template.  cant delete.'
		RETURN 0;
	END
	
	print 'almost there...'
	
	-- Ensure the loan is a quickpricer 2 loan.
	SELECT @sLoanFileT=sLoanFileT
	FROM LOAN_FILE_B
	WHERE
		    sLId = @LoanID
	SELECT @Error = @@Error, @RowCount = @@Rowcount
	IF (0 <> @Error OR 1 <> @RowCount)
	BEGIN
		SET @ReasonCantDeleteLoan = 'Was unable to find loan with specified id.'
		RETURN 0;
	END
		
	IF @sLoanFileT <> 2 -- quickpricer 2 sandboxed.
	BEGIN
		SET @ReasonCantDeleteLoan = 'The file you attempted to delete is not a quickpricer 2 sandboxed file'
		RETURN 0;
	END
	
	print 'just ran sloanFileT check.'
	
	SELECT 
		@BrokerQuickPricerTemplateFileVersion = sFileVersion
	FROM LOAN_FILE_A
	WHERE
		sLId = @BrokerQuickPricerTemplateID
	SELECT @Error = @@Error, @RowCount = @@Rowcount
	IF (0 <> @Error OR 1 <> @RowCount)
	BEGIN
		SET @ReasonCantDeleteLoan = 'Could not find the specified quickpricer template.'
		RETURN 0;
	END
	
	SELECT
		@sCreatedD = sCreatedD,
		@isValid = isValid
	FROM
		LOAN_FILE_A
	WHERE
		 sLId = @LoanID
	SELECT @Error = @@Error, @RowCount = @@Rowcount
	IF (0 <> @Error OR 1 <> @RowCount)
	BEGIN
		SET @ReasonCantDeleteLoan = 'Could not find the specified loan in loan_file_a.'
		RETURN 0;
	END
	
	print 'just ran loan_file_a check.'
	
	-- For now, only allowing deletion if it is in the pool.
	SELECT 
		  @IsInUse=IsInUse
		, @LastUsedD=LastUsedD
		, @FilesQuickPricerTemplateID = QuickPricerTemplateId
		, @FilesQuickPricerTemplateFileVersion = QuickPricerTemplateFileVersion
	FROM
		QUICKPRICER_2_LOAN_USAGE
	WHERE
		    BrokerID = @BrokerID
		AND LoanID = @LoanID
	SELECT @Error = @@Error, @RowCount = @@Rowcount
	IF (0 <> @Error)
	BEGIN
		SET @ReasonCantDeleteLoan = 'Error occurred determining if the loan is pooled.'
		RETURN 0;
	END    
    IF (@RowCount = 0 AND @sCreatedD > DATEADD(minute, -@QuickPricer2NonPooledExpirationMinutes, GETDATE()))
    BEGIN
		SET @ReasonCantDeleteLoan = 'Loan is not in pool and it is too young to delete (it could be simply in the middle of being fully created).'
		RETURN 0;
    END
	
	print 'just ran quickpricer_2_loan_usage check'
		
	IF @IsInUse = 1 AND @LastUsedD > DATEADD(day, -@QuickPricer2InUseLoanExpirationDays, GETDATE())
	BEGIN
		SET @ReasonCantDeleteLoan = 'File is in use and is too young to delete (it could be kept in the users UI).'
		RETURN 0;
	END
	
	print 'just ran is_in_use check'
	
	IF    (@IsInUse = 0 
	  AND @FilesQuickPricerTemplateFileVersion = @BrokerQuickPricerTemplateFileVersion
	  AND @FilesQuickPricerTemplateID = @BrokerQuickPricerTemplateID)
	BEGIN
		SET @ReasonCantDeleteLoan = 'File matches brokers quickpricer template and isnt in use, so why delete it?'
		RETURN 0;
	END		
	
	print 'just ran qptemplate check.'
	
	SELECT @RowCount = COUNT(*) 
	FROM RATE_MONITOR
	WHERE 
		sLId = @LoanID
	IF(0 <> @@Error OR (0 <> @RowCount AND @isValid = 1))
	BEGIN
		SET @ReasonCantDeleteLoan = 'File has a rate_monitor and is a valid loan, so will not delete.'
		RETURN 0;
	END
	
	print 'just ran RATE_MONITOR check'
	
	-- TODO: handle AUDIT_TRAIL_DAILY log deletion when there *is* a fileDB key.
	SELECT @RowCount = COUNT(*)
	FROM 
		AUDIT_TRAIL_DAILY
	WHERE
	 	    LoanID = @LoanID
		AND FileDBKey <> ''
	IF(0 <> @@Error OR 0 <> @RowCount)
	BEGIN
		SET @ReasonCantDeleteLoan = 'File has an AUDIT_TRAIL_DAILY log with a filedb key, which is not yet handled in the deletion routine.'
		RETURN 0;
	END
	
	print 'just ran AUDIT_TRAIL_DAILY check'
		
		SELECT 'Condition' as TableName, condid as FileDBGuid
		FROM CONDITION
		WHERE loanid = @LoanID
	UNION
		SELECT 'Discussion_Log' as TableName, disclogid as FileDBGuid
		FROM DISCUSSION_LOG
		WHERE discrefobjid = @LoanID
	IF(0 <> @@Error)
	BEGIN
		SET @ReasonCantDeleteLoan = 'Error accessing Condition or Discussion_Log table.'
		RETURN 0;
	END
	
	PRINT 'exiting candeleteloan with true.'
	
	SET @CanDeleteLoan = 1
	SET @ReasonCantDeleteLoan = '';
	RETURN 0;
END