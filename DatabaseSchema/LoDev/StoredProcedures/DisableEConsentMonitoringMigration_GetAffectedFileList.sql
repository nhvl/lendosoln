-- =============================================
-- Author:		Michael Leinweaver
-- Create date: 11/29/2018
-- Description:	Retrieves the list of loans that must be
--				migrated before e-consent monitoring automation
--				can be disabled for a lender.
-- =============================================
CREATE PROCEDURE [dbo].[DisableEConsentMonitoringMigration_GetAffectedFileList]
	@BrokerId uniqueidentifier
AS
BEGIN
	SELECT lfa.sLId, sLNm, sNeedInitialDisc, sNeedRedisc, sDisclosureNeededT, sDisclosuresDueD, sDisclosuresDueD_New
	FROM 
		LOAN_FILE_A lfa
		JOIN LOAN_FILE_B lfb ON lfa.sLId = lfb.sLId
		JOIN LOAN_FILE_C lfc ON lfa.sLId = lfc.sLId
		JOIN LOAN_FILE_E lfe ON lfa.sLId = lfe.sLId
	WHERE
		IsValid = 1 AND
		IsTemplate = 0 AND
		sBrokerId = @BrokerId AND
		sLoanFileT = 0 AND -- Ignore test, sandbox, and QP2 files
		sStatusT NOT IN (12, 15, 16, 17) AND -- Ignore lead statuses
		(
			sDisclosureNeededT = 15 -- Awaiting e-consent
			OR (sNeedInitialDisc = 1 AND sDisclosureNeededT = 3) -- Need initial disclosure with e-consent declined
			OR (sNeedInitialDisc = 1 AND sDisclosureNeededT = 4) -- Need initial disclosure with e-consent expired
		)
END
