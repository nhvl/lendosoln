
CREATE PROCEDURE [dbo].[INTERNAL_USER_ONLY_ListLoansNeedMigration] 
AS
    -- dd 9/25/2008 - Only migrate loan of active broker account.
	SELECT c.sLId , c.sLNm , c.IsMarkedInvalidated
	FROM Loan_File_Cache AS c JOIN Broker b ON c.sBrokerId=b.brokerId 
	WHERE b.Status=1 AND c.IsValid = 1 AND c.IsMarkedInvalidated=1




                       

