-- =============================================
-- Author:		Brian Beery
-- Create date: 09/20/2013
-- Description:	Remove a data retrieval framework partner
-- =============================================
ALTER PROCEDURE [dbo].[DATA_RETRIEVAL_PARTNER_Delete]
	@PartnerId uniqueidentifier
	
AS
BEGIN

	DELETE FROM DATA_RETRIEVAL_PARTNER
	WHERE PartnerId = @PartnerId

END