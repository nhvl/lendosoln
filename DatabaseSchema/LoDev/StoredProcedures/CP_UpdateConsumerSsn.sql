


CREATE PROCEDURE [dbo].[CP_UpdateConsumerSsn]
	@ConsumerId bigint,	
	@BrokerId uniqueidentifier,
	@Last4SsnDigits varchar(4) = null,
	@SsnAttemptFailureNumber int = null,
	@LastSsnAttemptFailDate datetime = null

AS
BEGIN
	UPDATE CP_CONSUMER
	SET		Last4SsnDigits = COALESCE(@Last4SsnDigits , Last4SsnDigits),
			SsnAttemptFailureNumber = COALESCE(@SsnAttemptFailureNumber , SsnAttemptFailureNumber),
			LastSsnAttemptFailDate = COALESCE(@LastSsnAttemptFailDate , LastSsnAttemptFailDate)
	
	WHERE	ConsumerId = @ConsumerId AND
			BrokerId = @BrokerId
END



