-- =============================================
-- Author:		Antonio Valencia
-- Create date: 4/23/2013
-- Description:	Deletes a title vendor association with broker
-- =============================================
CREATE PROCEDURE dbo.TITLE_VENDOR_X_BROKER_Delete
	-- Add the parameters for the stored procedure here
	@VendorId int ,
	@BrokerId uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	
	DELETE FROM TITLE_VENDOR_X_BROKER 
	WHERE TitleVendorId = @VendorId and BrokerId = @BrokerId

	
END
