-- =============================================
-- Author:		David Dao
-- Create date: 9/16/11
-- Description:	Stored procedure to pull CapitalMarkets::Trades data for MCM.
-- This stored procedure is called by external program.
-- =============================================
ALTER PROCEDURE [dbo].[RetrieveMCMTrades] 
	-- Add the parameters for the stored procedure here
	@CustomerCode varchar(36) = ''
AS
BEGIN
	DECLARE @BrokerID uniqueidentifier;
	SELECT @BrokerID=brokerid FROM Broker WHERE CustomerCode=@CustomerCode

	SELECT * FROM (
		SELECT
			t.TradeNum,
			d.DescriptionName,
			case( t.TypeB )
				when '0' then 'B'
				when '1' then 'S'
			end
			+
			case( t.TypeA )
				when '0' then ''
				when '1' then 'C'
				when '2' then 'P'
			end as TradeType,
			t.DealerInvestor,
			t.TradeDate,
			(SELECT SUM(tr.Amount)
			FROM MBS_TRANSACTION AS tr
			WHERE t.TradeId = tr.TradeId
			) - t.AllocatedLoans
			AS Amount,
			t.SettlementDate,
			t.NotificationDate,
			t.Coupon,
			t.Price,
			'' AS Blank0,
			'' AS Blank1,
			'' AS Blank2,
			'' AS Blank3,
			'' AS Blank4
		FROM
			MBS_TRADE AS t,
			MBS_DESCRIPTION AS d
		WHERE
			t.DescriptionId = d.DescriptionId
			AND t.BrokerId = @BrokerID
	) AS vtable WHERE Amount > 0
END