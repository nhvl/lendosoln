


-- =============================================
-- Author:		Peter Anargirou
-- Create date: July 25, 2008
-- Description:	Returns the loan templates associated with a branch.
-- =============================================
CREATE PROCEDURE [dbo].[ListLoanTemplatesAssociatedWithBranch]
	@BranchId uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT LOAN_FILE_F.sLId as ID, LOAN_FILE_E.sLNm as loanName
	FROM LOAN_FILE_F
		INNER JOIN LOAN_FILE_E
		ON LOAN_FILE_F.sLId = LOAN_FILE_E.sLId
	WHERE sBranchIdForNewFileFromTemplate = @BranchId

END



