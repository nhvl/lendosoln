
CREATE procedure [dbo].[EDOCS_DeleteDocType]
	@DocTypeId int,
	@BrokerId uniqueidentifier = null
as
UPDATE EDOCS_DOCUMENT_TYPE
    SET IsValid = 0
    WHERE DocTypeId = @DocTypeId 
		and (BrokerId = @BrokerId
			or (BrokerId is null and @BrokerId is null))


	DELETE FROM EDOCS_DOCMAGIC_DOCTYPE_X_DOCTYPE 
	WHERE BrokerID = @BrokerId AND @DocTypeId = DocTypeId
	
	DELETE FROM EDOCS_DOCUTECH_DOCTYPE_X_DOCTYPE
	WHERE BrokerID = @BrokerId AND @DocTypeId = DocTypeId
	
	DELETE FROM EDOCS_IDS_DOCTYPE_X_DOCTYPE
	WHERE BrokerID = @BrokerId AND @DocTypeId = DocTypeId