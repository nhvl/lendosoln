-- =============================================
-- Author:		Scott Kibler
-- Create date: 4/3/2013
-- Description:	Find the applicants that are possible duplicates per 105723
-- only comparing against loans at same lender
-- with rate lock status equal to "locked" or "lock requested"
-- loan purpose must match
-- ignoring sandboxed loans.										-- 190071
-- possible duplicate is returned if all of the following match:
--   any borrower's last name
--	 lien position
--   property address (county, state, zip, ignoring city because it could be misspelled.)
--
-- INPUTS:
--  Some of the arguments are optional.  If they aren't passed in, they are loaded from the loan.			
--
-- 03/02/2018 - Jhairo Erazo
-- This procedure has been modified to return app information instead of loan information.
-- This is due to the switch to encrypting SSNs in the DB, making SSN comparisons in SQL impossible. 
-- =============================================
ALTER PROCEDURE [dbo].[FindPossibleDuplicatesPer105723]
	@slid uniqueidentifier,
	@loanZip char(5) = null,
	@loanCounty varchar(36) = null,
	@loanState char(2) = null,
	@loanLienPosT int = null,
	@loanPurposeT int = null
AS
BEGIN
	-- get some loan info from the loan that we will need for filtering.
	declare @brokerID uniqueidentifier

	select
		   @loanCounty = Coalesce(@loanCounty, sSpCounty)
		,  @loanZip = Coalesce(@loanZip, sSpZip)
		,  @loanState = Coalesce(@loanState, sSpState)
		,  @loanLienPosT = Coalesce(@loanLienPosT, sLienPosT)
		,  @loanPurposeT = Coalesce(@loanPurposeT, sLPurposeT)
		,  @brokerID = sBrokerId
	from loan_file_cache where slid=@slid and sloanfilet = 0
	if(0 = @@RowCount)
	begin
		return
	end

	-- look for the duplicates.
	select a.aAppId, a.aBFirstNm, a.aBLastNm,  a.aCFirstNm, a.aCLastNm, a.aBSsnEncrypted, a.aCSsnEncrypted,
	lfc.slnm, lfc.sSpAddr, lfc.slid, lfc.sStatusT, lfc2.sBranchChannelT, lfc4.sEncryptionKey, lfc4.sEncryptionMigrationVersion
	from application_a a 
	join loan_file_cache lfc on a.slid = lfc.slid
	join loan_file_cache_2 lfc2 on a.slid = lfc2.slid
	join loan_file_cache_4 lfc4 on a.slid = lfc4.slid
	where 
		-- filter loan fields --
		a.slid != @slid
		and (lfc.sRateLockStatusT =1 OR  lfc.sRateLockStatusT = 2) -- include locked or lock requested loans.
		and lfc.IsValid = 1									   -- exclude deleted loans
		and lfc.IsTemplate = 0								   -- exclude templates
		and lfc.sSpCounty = @loanCounty						   -- exclude County
		and lfc.sSpZip = @loanZip							   -- match zip
		and lfc.sSpState = @loanState						   -- match state.
		and lfc.sLienPosT = @loanLienPosT					   -- match lien position.
		and lfc.sBrokerId = @brokerID						   -- only consider loans in same lender.
		and lfc.sLPurposeT = @loanPurposeT					   -- must have the same purpose.
		and lfc.sloanfilet = 0
	
		-- filter app fields --
		and
		(
			(
				a.aBLastNm is not null
				and a.aBLastNm <> ''
				and a.aBLastNm in 
				(
					select aBLastNm from application_a where slid=@slid
					UNION
					select aCLastNm from application_a where slid=@slid
				)
			)
			OR (
				a.aCLastNm is not null
				and a.aCLastNm <> ''
				and a.aCLastNm in
				(
					select aBLastNm from application_a where slid=@slid
					UNION
					select aCLastNm from application_a where slid=@slid
				)				
			)
		)
END
