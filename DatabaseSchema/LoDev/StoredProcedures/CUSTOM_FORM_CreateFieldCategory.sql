CREATE PROCEDURE CUSTOM_FORM_CreateFieldCategory
	@CategoryId int OUTPUT,
	@CategoryName varchar(100)
AS
BEGIN
	INSERT INTO CUSTOM_FORM_FIELD_CATEGORY (CategoryName) VALUES( @CategoryName)

	SET @CategoryId = @@IDENTITY
END
