-- ============================================================
-- Author:      Generated by application LqbEntitiesToSqlTables
-- Create date: 12/13/2018
-- Description: Deletes a row from the HOUSING_HISTORY_ENTRY table.
-- ============================================================
CREATE PROCEDURE [dbo].[HOUSING_HISTORY_ENTRY_Delete]
	@HousingHistoryEntryId UniqueIdentifier,
	@LoanId UniqueIdentifier,
	@BrokerId UniqueIdentifier
AS
BEGIN
	DELETE FROM [dbo].[HOUSING_HISTORY_ENTRY]
	WHERE
		HousingHistoryEntryId = @HousingHistoryEntryId
		AND LoanId = @LoanId
		AND BrokerId = @BrokerId
END
