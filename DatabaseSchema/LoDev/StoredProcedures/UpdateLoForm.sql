CREATE PROCEDURE UpdateLoForm 
	@FormId UniqueIdentifier,
	@BrokerId UniqueIdentifier,
	@Description varchar(100),
	@IsSignable bit,
	@IsStandardForm bit,
	@FileName varchar(50),
	@FieldMetaData varchar(max) = NULL
AS
BEGIN
	UPDATE LO_FORM
	SET Description = @Description,
	    IsSignable = @IsSignable,
		IsStandardForm = @IsStandardForm,
		FileName = @FileName,
		FieldMetaData = COALESCE(@FieldMetaData, FieldMetaData)
	WHERE FormId = @FormId AND BrokerId = @BrokerId
END
