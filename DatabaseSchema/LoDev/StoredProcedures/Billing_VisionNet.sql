CREATE PROCEDURE [dbo].[Billing_VisionNet]
	@StartDate datetime,
	@EndDate datetime
AS
BEGIN
-- dd 2018-11-02 - For use in automate billing executable.
-- dd 2018-11-02 - For use in automate billing executable.
select distinct lfc.sLNm as LoanNumber, doc.slid as LoanId
                                                from EDOCS_DOCUMENT doc with(nolock)
                                                join edocs_audit_history aud with(nolock) on doc.documentid=aud.documentid
                                                join LOAN_FILE_CACHE lfc with(nolock) on lfc.slid=doc.slid
                                                where modifiedbyuserid='98c3a7f3-1b37-496f-8b2f-2ee2c90c0be6'
                                                AND aud.ModifiedDate >= @StartDate AND aud.ModifiedDate < @EndDate
END