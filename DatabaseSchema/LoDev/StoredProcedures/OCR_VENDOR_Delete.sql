CREATE PROCEDURE [dbo].[OCR_VENDOR_Delete]
	@VendorId uniqueidentifier 
AS

	DELETE TOP(1) OCR_VENDOR_CONFIGURATION
	WHERE VendorId = @VendorId