-- =============================================
-- Author:		Eric Mallare
-- Create date: 5/3/2017
-- Description:	Gets the passwords from the transmission linked to a vendor.
-- =============================================
ALTER PROCEDURE [dbo].[GetVendorPasswords] 
	@NewPlatformId int
AS
BEGIN
	SELECT
		t.RequestCredentialPassword as NewPlatformReqPass, 
		t.ResponseCredentialPassword as NewPlatformRespPass
	FROM
		VERIFICATION_PLATFORM p JOIN
		VERIFICATION_TRANSMISSION t ON p.TransmissionId=t.TransmissionId
	WHERE
		p.PlatformId=@NewPlatformId
END
