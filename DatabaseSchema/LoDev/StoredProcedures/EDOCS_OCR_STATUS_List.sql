-- =============================================
-- Author:		Michael Leinweaver
-- Create date: 9/13/2017
-- Description:	Retrieves OCR request statuses.
-- =============================================
CREATE PROCEDURE [dbo].[EDOCS_OCR_STATUS_List]
	@BrokerId uniqueidentifier,
	@LoanId uniqueidentifier = NULL
AS
BEGIN
	SELECT OcrRequestId, Status, Date, SupplementalData
	FROM EDOCS_OCR_STATUS
	WHERE BrokerId = @BrokerId AND OcrRequestId IN (
		SELECT OcrRequestId
		FROM EDOCS_OCR_REQUEST
		WHERE BrokerId = @BrokerID AND (@LoanId IS NULL OR LoanId = @LoanId)
	)
END
