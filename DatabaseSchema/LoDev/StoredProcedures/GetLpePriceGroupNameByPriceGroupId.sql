CREATE PROCEDURE GetLpePriceGroupNameByPriceGroupId
	@LpePriceGroupId uniqueidentifier
AS
	SELECT LpePriceGroupName
	FROM LPE_PRICE_GROUP with(nolock)
	WHERE LpePriceGroupId = @LpePriceGroupId
