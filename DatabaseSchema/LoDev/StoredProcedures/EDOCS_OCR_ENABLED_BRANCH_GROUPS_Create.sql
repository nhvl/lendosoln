-- =============================================
-- Author:		Justin Lara
-- Create date: 9/13/2018
-- Description:	Enables a branch group for OCR
--              by adding it to the table.
-- =============================================
ALTER PROCEDURE [dbo].[EDOCS_OCR_ENABLED_BRANCH_GROUPS_Create] 
	@BrokerId uniqueidentifier,
	@GroupId uniqueidentifier
AS
BEGIN
	IF NOT EXISTS
	(
		SELECT 1
		FROM EDOCS_OCR_ENABLED_BRANCH_GROUPS
		WHERE BrokerId = @BrokerId
			AND GroupId = @GroupId
	)
	BEGIN
		INSERT INTO EDOCS_OCR_ENABLED_BRANCH_GROUPS
		(
			BrokerId,
			GroupId
		)
		VALUES
		(
			@BrokerId,
			@GroupId
		)
	END
END
