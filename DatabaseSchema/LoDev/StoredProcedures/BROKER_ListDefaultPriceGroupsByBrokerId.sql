CREATE PROCEDURE [dbo].[BROKER_ListDefaultPriceGroupsByBrokerId] @BrokerID uniqueidentifier
AS
	SELECT
		WholesalePriceGroupID,
		MiniCorrPriceGroupID,
		CorrespondentPriceGroupID
	FROM Broker
	WHERE BrokerID = @BrokerID
	if(0!=@@error)
	begin
		RAISERROR('Error in the selection statement in BROKER_ListDefaultPriceGroupsByBrokerId sp', 16, 1);
		return -100;
	end
	return 0;