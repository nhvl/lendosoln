
-- =============================================
-- Author:		Peter Anargirou
-- Create date: 8/12/08
-- Description:	Deletes a stage constant given the key
-- =============================================
CREATE PROCEDURE [dbo].[DeleteStageConstant]
	-- Add the parameters for the stored procedure here
	@KeyIdStr varchar(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM STAGE_CONFIG
	WHERE KeyIdStr = @KeyIdStr
END

