ALTER PROCEDURE [dbo].[ListEmployeePermissionByEmployeeId]
	@EmployeeId uniqueidentifier
AS
	SELECT
		v.EmployeeId,
		v.PopulatePMLPermissionsT,
		bu.Permissions,
		bu.PmlBrokerId
	FROM
		View_lendersoffice_Employee AS v
		LEFT JOIN Broker_User as bu on v.employeeid = bu.employeeid
	WHERE
		v.EmployeeId = @EmployeeId