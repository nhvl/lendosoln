ALTER PROCEDURE [dbo].[BorrowerCreditAuthorizationDocumentAssociation_Clear]
	@AppId uniqueidentifier,
	@IsCoborrower bit
AS
BEGIN
	DELETE FROM BORROWER_CREDIT_AUTHORIZATION_DOCUMENT_ASSOCIATION
	WHERE AppId = @AppId AND IsCoborrower = @IsCoborrower
END
