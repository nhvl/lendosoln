-- =============================================
-- Author:		David Dao
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE dbo.BROKER_GLOBAL_IP_WHITELIST_Create
		@Id int OUTPUT,
		@BrokerId UniqueIdentifier,
		@IPAddress varchar(50),
		@Description varchar(200),
		@UserId UniqueIdentifier
AS
BEGIN

		INSERT INTO BROKER_GLOBAL_IP_WHITELIST (BrokerId, IPAddress, Description, LastModifiedDate, LastModifiedUserId)
		VALUES (@BrokerId, @IPAddress, @Description, GETDATE(), @UserId);
		
		SET @Id=SCOPE_IDENTITY();
END
