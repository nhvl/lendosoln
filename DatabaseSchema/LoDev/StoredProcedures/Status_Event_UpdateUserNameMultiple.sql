CREATE PROCEDURE [dbo].Status_Event_UpdateUserNameMultiple
	@loanId uniqueidentifier, 
	@brokerId uniqueidentifier,
	@StatusEventUserNames StatusEventUserNameUpdateTableType READONLY
AS
BEGIN	
	UPDATE STATUS_EVENT
	SET UserName = statusEventUserNames.UserName
	FROM STATUS_EVENT statusEvent INNER JOIN @StatusEventUsernames statusEventUserNames ON statusEventUserNames.Id = statusEvent.Id
END
