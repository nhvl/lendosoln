-- =============================================
-- Author:		Eric Mallare
-- Create date: 5/5/2017
-- Description:	Retrieves a specific lender service.
-- =============================================
ALTER PROCEDURE [dbo].[VERIFICATION_LENDER_SERVICE_Get]
	@BrokerId uniqueidentifier,
	@LenderServiceId int
AS
BEGIN
	SELECT
		ls.LenderServiceId, ls.ServiceT, ls.VendorId, ls.ResellerId, ls.DisplayName, ls.DisplayNameLckd,
		ls.IsEnabledForLqbUsers, ls.EnabledEmployeeGroupId, ls.IsEnabledForTpoUsers, ls.EnabledOcGroupId,
		ls.AllowPaymentByCreditCard, ls.VoeUseSpecificEmployerRecordSearch, ls.VoeEnforceUseSpecificEmployerRecordSearch,
		ls.VoaAllowImportingAssets,     ls.VoaAllowRequestWithoutAssets,     ls.VoaEnforceAllowRequestWithoutAssets,
        ls.VoeAllowImportingEmployment, ls.VoeAllowRequestWithoutEmployment, ls.VoeEnforceRequestsWithoutEmployment,
        ls.CanBeUsedForCredentialsInTpo
	FROM
		VERIFICATION_LENDER_SERVICE ls
	WHERE
		ls.BrokerId=@BrokerId AND
		ls.LenderServiceId=@LenderServiceId
END
