

CREATE PROCEDURE [dbo].[TEAM_SetUserAssignment] 
	@TeamId uniqueidentifier, 
	@EmployeeId uniqueidentifier,
	@IsPrimary bit
AS
BEGIN

	INSERT INTO Team_User_Assignment(TeamId, EmployeeId, IsPrimary)
	VALUES(@TeamId, @EmployeeId, @IsPrimary) 
	
	
END


