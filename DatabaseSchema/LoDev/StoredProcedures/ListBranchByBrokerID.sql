ALTER PROCEDURE [dbo].[ListBranchByBrokerID] @BrokerID uniqueidentifier
AS
	SELECT
		BranchID AS BranchID,
		BranchNm AS Name,
		BranchAddr + ', ' + BranchCity + ', ' + BranchState + ' ' + BranchZip AS Address,
		BranchAddr AS Street,
		BranchCity AS City,
		BranchState AS State,
		BranchZip AS Zipcode,
		BranchPhone AS Phone,
		BranchFax AS Fax,
		BranchLpePriceGroupIdDefault,
		BranchLNmPrefix,
		DisplayBranchNameInLoanNotificationEmails,
		BranchCode,
		IsAllowLOEditLoanUntilUnderwriting,
		UseBranchInfoForLoans,		
		NmlsIdentifier,
		LicenseXmlContent,
		DisplayNm,
		DisplayNmLckd as DisplayNmModified,
		IsBranchTPO,
		BranchChannelT,
		DocMagicUserName,
		DocMagicPassword,
		DocMagicCustomerId,
		DisclosureDocUserName,
		DisclosureDocPassword,
		DisclosureDocCustomerId,
		ConsumerPortalId,
		IsFhaLenderIDModified,
		FhaLenderID,
		Division,
		PopulateFhaAddendumLines,
		CustomPricingPolicyField1,
		CustomPricingPolicyField2,
		CustomPricingPolicyField3,
		CustomPricingPolicyField4,
		CustomPricingPolicyField5,
		BranchIdNumber,
		RetailTpoLandingPageId,
		IsLegalEntityIdentifierModified,
		LegalEntityIdentifier,
		[Status]
	FROM Branch
	WHERE BrokerID = @BrokerID
	ORDER BY BranchNm
	if(0!=@@error)
	begin
		RAISERROR('Error in the selection statement in ListBranchByBrokerID sp', 16, 1);
		return -100;
	end
	return 0;
