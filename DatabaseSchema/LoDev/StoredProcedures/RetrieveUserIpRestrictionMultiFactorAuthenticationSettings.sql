-- =============================================
-- Author:		David Dao
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE dbo.RetrieveUserIpRestrictionMultiFactorAuthenticationSettings 
    @UserId UniqueIdentifier
AS
BEGIN
    SELECT MustLogInFromTheseIpAddresses, EnabledIpRestriction
    FROM BROKER_USER
    WHERE UserId = @UserId

END
