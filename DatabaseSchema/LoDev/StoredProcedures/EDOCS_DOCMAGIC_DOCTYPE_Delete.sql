-- =============================================
-- Author:		Thien Nguyen
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[EDOCS_DOCMAGIC_DOCTYPE_Delete]
    @Id int
AS
BEGIN

	DELETE FROM EDOCS_DOCMAGIC_DOCTYPE
		  WHERE Id = @Id
END
