


CREATE  PROCEDURE [dbo].[ListBrokersByFeature]
	@BrokerName varchar( 32 ) = NULL , 
	@AccountManager varchar( 10 ) = NULL , 
	@CustomerCode varchar( 10 ) = NULL , 
	@HasLenderDefaultFeatures bit = NULL , 
	@BrokerStatus int = NULL,
	@DuUserId varchar(100) = NULL, 
	@HasFeatureId uniqueidentifier, --This is required. If i set it to null here this store procedure will return duplicate rows. av 1-02-08
	@BrokerID uniqueidentifier = NULL
AS
	SELECT b.BrokerID, b.BrokerNm, b.BrokerAddr + ' ' + b.BrokerCity + ' ' + b.BrokerState + ' ' + b.BrokerZip AS BrokerAddress, BrokerPhone, BrokerFax, CreditReportURL, CustomerCode, CreditMornetPlusUserId, AccountManager, PricePerSeat, EmailAddressesForSystemChangeNotif, CASE Status WHEN 0 THEN 'No' WHEN 1 THEN 'Yes' END  AS IsActive, Notes
	FROM 
		Broker as b WITH (NOLOCK) 
			JOIN 
		Feature_Subscription as s  WITH (NOLOCK) ON s.BrokerId = b.BrokerId 
	WHERE
		b.BrokerNm LIKE COALESCE( @BrokerName + '%' , b.BrokerNm )
		AND
		b.CreditMornetPlusUserId LIKE COALESCE(@DuUserId + '%', b.CreditMornetPlusUserId)
		AND
		b.AccountManager LIKE COALESCE( @AccountManager + '%' , b.AccountManager )
		AND
		b.CustomerCode LIKE COALESCE( @CustomerCode + '%' , b.CustomerCode )
		AND
		b.HasLenderDefaultFeatures = COALESCE( @HasLenderDefaultFeatures , b.HasLenderDefaultFeatures )
		AND
		b.Status = COALESCE( @BrokerStatus , b.Status )
		AND 
		s.FeatureId = @HasFeatureId
		AND 
		s.IsActive = 1
		AND
		s.BrokerID = COALESCE(@BrokerID, s.BrokerID)
		
	ORDER BY BrokerNm
	if( 0!=@@error)
	begin
		RAISERROR('Error in the selection statement in ListBrokers sp', 16, 1);
		return -100;
	end
	return 0;






