CREATE  PROCEDURE [dbo].[CP_CreateActionRequest]
@ConsumerActionRequestId bigint OUT
, @sLId uniqueidentifier
, @aAppId uniqueidentifier
, @aBFullName varchar(50)
, @aCFullName varchar(50)
, @ResponseStatus tinyint
, @DocTypeId int
, @Description varchar(100)
, @PdfFileDbKey uniqueidentifier
, @PdfMetaDataSnapshot varchar(max)
, @IsApplicableToBorrower bit
, @IsApplicableToCoborrower bit
, @IsSignatureRequested bit
, @IsESignAllowed bit
, @aBInitials char(2) 
, @aCInitials char(2)
, @CreatorEmployeeId uniqueidentifier = null -- Temp Default to avoid breaking the feature
, @sTitleBorrowerId uniqueidentifier = null
AS
INSERT INTO CP_CONSUMER_ACTION_REQUEST 
(
sLId 
, aAppId 
, aBFullName 
, aCFullName 
, ResponseStatus 
, DocTypeId 
, Description 
, PdfFileDbKey 
, PdfMetaDataSnapshot 
, IsApplicableToBorrower 
, IsApplicableToCoborrower
, IsSignatureRequested
, IsESignAllowed
, aBInitials
, aCInitials
, CreatorEmployeeId
, sTitleBorrowerId
)
           
VALUES
(
@sLId 
, @aAppId 
, @aBFullName 
, @aCFullName 
, @ResponseStatus 
, @DocTypeId 
, @Description 
, @PdfFileDbKey 
, @PdfMetaDataSnapshot 
, @IsApplicableToBorrower 
, @IsApplicableToCoborrower
, @IsSignatureRequested
, @IsESignAllowed
, @aBInitials
, @aCInitials
, @CreatorEmployeeId
, @sTitleBorrowerId
)


SELECT  @ConsumerActionRequestId = SCOPE_IDENTITY()


IF ( 0 != @@ERROR )
BEGIN
	RAISERROR('Error in inserting into CP_CONSUMER_ACTION_REQUEST  table in CP_CreateActionRequest sp', 16, 1);
	RETURN -100;
END
RETURN 0;
