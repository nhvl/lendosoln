-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[FindPoolIdForBrokerIdAgencyPoolNum]
	-- Add the parameters for the stored procedure here
	@BrokerId Guid,
	@PoolTypeCode varchar(2),
	@BasePoolNumber varchar(6),
	@IssueTypeCode varchar(1),
	@PendingPoolNum varchar(9), -- Will be at most 9 characters
	@AgencyT int,
	@PoolPrefix varchar(3)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT PoolId
	FROM MORTGAGE_POOL
	WHERE BrokerId = @BrokerId AND
		  IsEnabled = 1 AND (
			(@AgencyT = 3 AND (BasePoolNumber+IssueTypeCode+PoolTypeCode) = @PendingPoolNum) OR -- GINNIE
			(@AgencyT IN (1, 2) AND PoolPrefix = @PoolPrefix AND BasePoolNumber = @BasePoolNumber) -- FANNIE, FREDDIE
		  )
END
