CREATE procedure [dbo].[UpdateZipcodes]
@Zipcode char(5), @County varchar(100), @State char(2), @City varchar(50), @FipsCountyCode int
as

if (select count(*) 
	from citycountystatezip 
	where zipcode = @Zipcode
	group by zipcode) > 0
begin
update CityCountyStateZip
set County = @County, State = @State, City = @City, FipsCountyCode = @FipsCountyCode
where zipcode = @Zipcode;
end

else
begin
insert into CityCountyStateZip( Zipcode, County, State, City, EntryId, FipsCountyCode )
values (@Zipcode, @County, @State, @City, newid(), @FipsCountyCode);
end