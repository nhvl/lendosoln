

-- =============================================
-- Author:		Diana Baird
-- Create date: 5/13/10
-- Description:	Updates an EDoc Folder
-- =============================================
CREATE PROCEDURE [dbo].[EDOCS_UpdateFolder] 
	@FolderId int,
	@FolderName varchar(50)
AS
BEGIN
update EDOCS_FOLDER
set FolderName = @FolderName
where FolderId = @FolderId

	END








