-- =============================================
-- Author:		Justin Kim
-- Create date: 6/29/18
-- Description:	Create a security log item.
-- =============================================
ALTER PROCEDURE [dbo].[SECURITY_LOG_CREATE]
	@CreatedDate datetime2,
	@ClientIp varchar(50),
	@EventType smallint,
	@BrokerId uniqueidentifier,
	@UserId uniqueidentifier = null,
	@DescriptionText varchar(500),
	@UserFirstNm varchar(21),
	@UserLastNm varchar(21),
	@LoginNm varchar(36),
	@IsInternalUserAction bit,
	@IsSystemAction bit,
	@PrincipalType tinyint
AS
BEGIN
	INSERT INTO SECURITY_LOG
	(
		CreatedDate,
		ClientIp,
		EventType,
		BrokerId,
		UserId,
		DescriptionText,
		UserFirstNm,
		UserLastNm,
		LoginNm,
		IsInternalUserAction,
		IsSystemAction,
		PrincipalType
	)
	VALUES
	(
		@CreatedDate,
		@ClientIp,
		@EventType,
		@BrokerId,
		@UserId,
		@DescriptionText,
		@UserFirstNm,
		@UserLastNm,
		@LoginNm,
		@IsInternalUserAction,
		@IsSystemAction,
		@PrincipalType
	)
END
