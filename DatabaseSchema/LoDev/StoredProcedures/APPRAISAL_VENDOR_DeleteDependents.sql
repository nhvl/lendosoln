-- =============================================
-- Author:		Jhairo Erazo
-- Create date: 7/28/2016
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[APPRAISAL_VENDOR_DeleteDependents]
	-- Add the parameters for the stored procedure here
	@VendorId uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    DELETE FROM APPRAISAL_VENDOR_EMPLOYEE_LOGIN
	WHERE VendorId = @VendorId
	
	DELETE FROM APPRAISAL_VENDOR_BROKER_ENABLED
	WHERE VendorId = @VendorId
END
