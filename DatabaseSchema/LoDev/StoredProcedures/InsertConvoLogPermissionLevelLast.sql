-- =============================================
-- Author: Scott Kibler
-- Create date: 5/9/2017
-- Description: 
--	Adds the permission level to the last position in the order.
-- =============================================
CREATE PROCEDURE [dbo].[InsertConvoLogPermissionLevelLast]
	@BrokerId uniqueidentifier,
	@PermissionLevelId int
AS
BEGIN
	DECLARE @NewOrdinal INT
	
	SELECT @NewOrdinal = ISNULL(MAX(Ordinal) + 1, 1)
	FROM CONVOLOG_PERMISSION_LEVEL_ORDER
	WHERE BrokerId = @BrokerId
	
	INSERT INTO 
		CONVOLOG_PERMISSION_LEVEL_ORDER
		(BrokerId, Ordinal, PermissionLevelId)
	VALUES 
		(@BrokerId, @NewOrdinal, @PermissionLevelId)	
END