-- =============================================
-- Author:		Brian Beery
-- Create date: 09/20/2013
-- Description:	Associate a data retrieval framework partner with a lender
-- =============================================
CREATE PROCEDURE [dbo].[DATA_RETRIEVAL_PARTNER_BROKER_AddAssociation]
	@BrokerId UniqueIdentifier,
	@PartnerId UniqueIdentifier 

AS
BEGIN

INSERT INTO DATA_RETRIEVAL_PARTNER_BROKER(BrokerId, PartnerId) VALUES (@BrokerId, @PartnerId)

END