CREATE PROCEDURE UpdateCreditReportStatusDate
	@LoanID Guid,
	@ApplicationID Guid,
	@OrderDate Datetime = NULL,
	@ReceiveDate Datetime = NULL
AS
	-- 11/9/04 dd- Is this OBSOLETE? If yes then delete.
	UPDATE APPLICATION_A 
	SET aCrOd = COALESCE(@OrderDate, aCrOd),
	        aCrRd = COALESCE(@ReceiveDate, aCrRd)
	WHERE sLId = @LoanID
	      AND aAppID = @ApplicationID
	if( 0!=@@error)
	begin
		RAISERROR('Error in updating Application table in UpdateCreditReportStatusDate sp', 16, 1);
		return -100;
	end
	return 0;
