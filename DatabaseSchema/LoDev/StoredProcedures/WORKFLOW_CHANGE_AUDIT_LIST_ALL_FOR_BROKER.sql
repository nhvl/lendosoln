-- =============================================
-- Author:		Justin Kim
-- Create date: 5/15/18
-- Description:	List all workflow change audits belonging to a broker.
-- =============================================
ALTER PROCEDURE [dbo].[WORKFLOW_CHANGE_AUDIT_LIST_ALL_FOR_BROKER]
	@BrokerId uniqueidentifier
AS
BEGIN
	SELECT UserId, WorkflowAuditType, AuditDescription, AuditDate, IsInternalUser, ReleaseConfigId
	FROM WORKFLOW_CHANGE_AUDIT
	WHERE BrokerId = @BrokerId
END
