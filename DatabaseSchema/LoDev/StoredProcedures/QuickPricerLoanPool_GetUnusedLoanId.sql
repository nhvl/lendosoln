-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE QuickPricerLoanPool_GetUnusedLoanId
	@BrokerId UniqueIdentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

	DECLARE @sLId UniqueIdentifier

	SET TRANSACTION ISOLATION LEVEL REPEATABLE READ
	BEGIN TRANSACTION

		DECLARE @LockResult Int
		EXEC @LockResult = sp_getapplock @Resource='QUICK_PRICER_LOAN_POOL', @LockMode = 'Exclusive'
		IF @LockResult < 0
		BEGIN
			RAISERROR('Unable to lock QUICK_PRICER_LOAN_POOL resource.', 16, 1)
  			RETURN
 		END 
	
		SELECT TOP 1 @sLId = sLId FROM QUICK_PRICER_LOAN_POOL 
		WHERE BrokerId = @BrokerId AND IsInUsed = 0


		IF @@ROWCOUNT = 1
		BEGIN
			UPDATE Quick_Pricer_Loan_Pool SET IsInUsed = 1
			WHERE BrokerId = @BrokerId AND sLId = @sLId



		END
	COMMIT TRANSACTION;
	SELECT @sLId AS sLId WHERE @sLId IS NOT NULL

END
