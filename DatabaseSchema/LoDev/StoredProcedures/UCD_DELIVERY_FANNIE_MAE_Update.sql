-- =============================================
-- Author:		Eric Mallare
-- Create date: 10/9/2017
-- Description:	Updates a Fannie Mae UCD delivery item
-- =============================================
ALTER PROCEDURE [dbo].[UCD_DELIVERY_FANNIE_MAE_Update]
	@UcdDeliveryId int,
	@BatchId varchar(50),
	@CasefileStatus int,
	@FindingsDocumentId uniqueidentifier,
	@FindingsXmlFileDbKey uniqueidentifier
AS
BEGIN
	UPDATE 
		UCD_DELIVERY_FANNIE_MAE
	SET
		BatchId=@BatchId,
		CasefileStatus=@CasefileStatus,
		FindingsDocumentId=@FindingsDocumentId,
		FindingsXmlFileDbKey=@FindingsXmlFileDbKey
	WHERE
		UcdDeliveryId=@UcdDeliveryId
END
