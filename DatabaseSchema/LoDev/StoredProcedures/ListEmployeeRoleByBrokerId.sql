ALTER PROCEDURE [dbo].[ListEmployeeRoleByBrokerId]
	@BrokerId uniqueidentifier ,  --av 04/02/08 changed from GUID 
	@BranchId uniqueidentifier = NULL ,  --av 04/02/08 changed from GUID  
	@NameFilter Varchar(32) = NULL , 
	@LastFilter Varchar(32) = NULL , 
	@RoleDesc Varchar( 32 ) = NULL,
	@IsActive bit = NULL,
	@BranchChannelT int = NULL,
	@CorrespondentProcessT int = NULL
AS
	SELECT
		au.LoginNm,
		au.Type,
		au.UserId,
		v.EmployeeId ,
		v.UserFirstNm ,
		v.UserLastNm ,
		v.Email ,
		v.RoleModifiableDesc ,
		v.RoleImportanceRank ,
		v.RoleDesc ,
		v.RoleId ,
		v.IsActive,
		e.Addr,
		e.City,
		e.State,
		e.Zip,
		e.Phone,
		e.Fax, 
		e.BranchId, --OPM 21236 av 04/02/08
		e.LicenseXmlContent,
		e.Pager,
		e.Cellphone,
		e.UserMiddleNm,
		e.UserSuffix,
	    COALESCE(pb.Name, '') AS CompanyName ,
		bu.PmlBrokerId,
		pb.LicenseXmlContent as CompanyLicenseXml,
		branchnm,
		e.IsCellphoneForMultiFactorOnly
	FROM
		View_Employee_Role AS v WITH(NOLOCK) 
		JOIN Employee e WITH(NOLOCK) ON v.employeeId = e.EmployeeId
		LEFT JOIN Broker_User bu WITH(NOLOCK) ON e.EmployeeUserId = bu.UserId
		LEFT JOIN All_User au  WITH(NOLOCK) ON e.EmployeeUserId = au.UserId
		LEFT JOIN PML_Broker pb WITH(NOLOCK) ON pb.BrokerId=@BrokerId AND bu.PmlBrokerId = pb.PmlBrokerId
		LEFT JOIN BRANCH b on e.branchid = b.branchid
	WHERE
		(
			(
				(
					v.UserFirstNm LIKE @NameFilter + '%'
					AND
					v.UserLastNm LIKE @LastFilter + '%'
				)
				AND
				@NameFilter IS NOT NULL
				AND
				@LastFilter IS NOT NULL
			)
			OR
			(
				(
					v.UserFirstNm LIKE @NameFilter + '%'
					OR
					v.UserLastNm LIKE @NameFilter + '%'
				)
				AND
				@NameFilter IS NOT NULL
				AND
				@LastFilter IS NULL
			)
			OR
			(
				@NameFilter IS NULL
				AND
				@LastFilter IS NULL
			)
		)
		AND v.RoleDesc = COALESCE( @RoleDesc , v.RoleDesc )
		AND v.IsActive = COALESCE(@IsActive, v.IsActive)
		AND v.BrokerId = @BrokerId
		AND
		(
			@BranchId IS NULL
			OR
			(au.Type IS NULL OR au.Type = 'B' AND v.BranchId = @BranchId)
			OR
			(au.Type = 'P' AND v.BranchId = @BranchId AND @BranchChannelT IN (1, 2, 4))
			OR
			(au.Type = 'P' AND e.CorrespondentBranchId = @BranchId AND @BranchChannelT = 3 AND @CorrespondentProcessT IN (0, 1, 2, 4, 5))
			OR
			(au.Type = 'P' AND e.MiniCorrespondentBranchId = @BranchId AND @BranchChannelT = 3 AND @CorrespondentProcessT = 3)
			OR
			(au.Type = 'P' AND (@BranchChannelT = 0 OR @BranchChannelT IS NULL) AND (v.BranchId = @BranchID OR e.CorrespondentBranchId = @BranchId OR e.MiniCorrespondentBranchId = @BranchId))
		)
	ORDER BY
		v.IsActive DESC , v.UserLastNm , v.UserFirstNm

		
		OPTION (OPTIMIZE FOR (@BrokerId UNKNOWN, @BranchId UNKNOWN, @NameFilter UNKNOWN, @LastFilter UNKNOWN, @RoleDesc UNKNOWN,
		   @IsActive UNKNOWN, @BranchChannelT UNKNOWN, @CorrespondentProcessT UNKNOWN ))
		   		

	if( 0!=@@error)
	begin
		RAISERROR('Error in the selection statement in ListEmployeeRoleByBrokerId sp', 16, 1);
		return -100;
	end
	
	return 0;