

-- =============================================
-- Author:		Brian Beery
-- Create date: 02/21/13
-- Description:	Selects the 15 loans least recently modified for the given lender by BrokerID
-- =============================================
CREATE PROCEDURE [dbo].[ListModifiedLoansByBrokerIDwithCache]
	-- Add the parameters for the stored procedure here
	@BrokerId uniqueidentifier,
	@AppCode uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Insert statements for procedure here
	SELECT TOP(15) WITH TIES Integration_File_Modified.*
	FROM Integration_File_Modified WITH (nolock)
		JOIN Loan_File_Cache WITH (nolock) ON Loan_File_Cache.sLId = Integration_File_Modified.sLId
		JOIN Loan_File_B WITH (nolock) ON Loan_File_B.sLId = Loan_File_Cache.sLId
	WHERE Integration_File_Modified.BrokerId = @BrokerId AND Integration_File_Modified.AppCode=@AppCode
		AND Loan_File_Cache.IsTemplate = 0
		AND Loan_File_B.sLoanFileT = 0
	ORDER BY Integration_File_Modified.LastModifiedD
END


