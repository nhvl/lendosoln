CREATE PROCEDURE [dbo].[FEE_TYPE_Fetch]
	@FeeTypeId		uniqueidentifier
AS
	SELECT *
	FROM FEE_TYPE
	WHERE FeeTypeId = @FeeTypeId