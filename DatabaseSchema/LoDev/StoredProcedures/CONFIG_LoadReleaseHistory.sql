


-- =============================================
-- Author:		Antonio Valencia
-- Create date: September 14, 2010
-- Description:	Gets most recent configurations for each organization
-- =============================================
ALTER PROCEDURE [dbo].[CONFIG_LoadReleaseHistory] 
	@OrgId uniqueidentifier 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	
		SELECT ConfigId, OrgId, ConfigurationXmlContent, ModifyingUserId, ReleaseDate, CompiledConfiguration 
		FROM CONFIG_RELEASED 
		WHERE OrgId = @OrgId 
		ORDER BY ReleaseDate DESC
  

END



