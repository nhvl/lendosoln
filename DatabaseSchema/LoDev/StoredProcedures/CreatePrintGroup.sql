CREATE PROCEDURE CreatePrintGroup
	@GroupID Guid,
	@BrokerID Guid,
	@GroupName varchar(200),
	@GroupDefinition text
AS
	INSERT INTO Print_Group(GroupID, OwnerID, GroupName, GroupDefinition)
	                      VALUES (@GroupID, @BrokerID, @GroupName, @GroupDefinition)
