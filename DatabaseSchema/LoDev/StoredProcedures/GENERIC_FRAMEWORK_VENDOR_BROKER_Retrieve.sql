-- =============================================
-- Author:		Timothy Jewell
-- Create date: 9/9/2014
-- Description:	Retrieves Broker's data for a specific Generic Framework Vendor.
-- =============================================
ALTER PROCEDURE [dbo].[GENERIC_FRAMEWORK_VENDOR_BROKER_Retrieve]
	-- Add the parameters for the stored procedure here
	@BrokerID uniqueidentifier,
	@ProviderID varchar(7)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Insert statements for procedure here
	SELECT brokerConfig.CredentialXML, brokerConfig.EncryptionKeyId, brokerConfig.LaunchLinkConfigurationXML
	FROM GENERIC_FRAMEWORK_VENDOR_BROKER_CONFIG brokerConfig WITH(NOLOCK)
	WHERE brokerConfig.BrokerID = @BrokerID AND brokerConfig.ProviderID = @ProviderID
END
