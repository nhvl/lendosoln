

CREATE PROCEDURE [dbo].[INTERNAL_USER_ONLY_ListUsersBelongingToBroker] 
	@BrokerID uniqueidentifier
AS
	SELECT temp1.employeeid, temp1.fullname, temp1.isactive, temp1.branchnm, temp2.userid, temp2.loginnm, temp2.type, temp2.IsAccountOwner, temp2.CanLogin, temp1.email
	FROM 
		( SELECT e.employeeid, e.userfirstnm + ' ' + e.userlastnm AS fullname, e.isactive, b.branchnm, e.email
		  FROM employee e LEFT OUTER JOIN branch b 
		  ON e.branchid = b.branchid 
		  WHERE b.brokerid = @BrokerID 
	    ) temp1
	LEFT OUTER JOIN
	( SELECT b.userid, b.employeeid, a.loginnm, a.isactive as canlogin, cast( a.type as varchar( 1 ) ) as type, b.IsAccountOwner 
	FROM broker_user b, all_user a where b.userid = a.userid) temp2
	ON temp1.employeeid = temp2.employeeid
	ORDER BY fullname;


