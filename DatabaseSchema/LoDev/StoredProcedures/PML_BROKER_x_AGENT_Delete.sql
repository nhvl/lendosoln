CREATE PROCEDURE [dbo].[PML_BROKER_x_AGENT_Delete] 
	@BrokerId		uniqueidentifier,
	@PmlBrokerId	uniqueidentifier,
	@AgentId		uniqueidentifier
AS
	DELETE FROM PML_BROKER_x_AGENT
	WHERE BrokerId = @BrokerId
	AND PmlBrokerId = @PmlBrokerId
	AND AgentId = @AgentId