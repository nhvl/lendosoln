-- =============================================
-- Author:      David Dao
-- Create date: 
-- Description: 
-- =============================================
ALTER PROCEDURE [dbo].[BROKER_USER_GetSecurityQuestionAnswer] 
    @UserId UniqueIdentifier
AS
BEGIN
    SELECT
        EncryptionKeyId,
        SecurityQuestion1Id,
        SecurityQuestion1Answer,
        SecurityQuestion1EncryptedAnswer,
        SecurityQuestion2Id,
        SecurityQuestion2Answer,
        SecurityQuestion2EncryptedAnswer,
        SecurityQuestion3Id,
        SecurityQuestion3Answer,
        SecurityQuestion3EncryptedAnswer
    FROM BROKER_USER
    WHERE UserId=@UserId

END
