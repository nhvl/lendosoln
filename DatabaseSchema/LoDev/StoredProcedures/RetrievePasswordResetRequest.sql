CREATE PROCEDURE [dbo].[RetrievePasswordResetRequest] @RequestID uniqueidentifier AS
BEGIN
	SET NOCOUNT ON
	-- SELECT u.EmployeeId, a.UserId, a.LoginNm, a.TimeOfPasswordChangeRequest, a.SecurityQuestionsAttemptsMade, a.AllowPasswordChange FROM ALL_USER  a INNER JOIN
    --                  BROKER_USER AS u ON u.UserId = a.UserId WHERE PasswordChangeRequestId = @RequestID

SELECT r.BrokerId, u.EmployeeId, a.UserId, a.LoginNm, a.TimeOfPasswordChangeRequest, a.SecurityQuestionsAttemptsMade, a.AllowPasswordChange
FROM         dbo.ALL_USER AS a INNER JOIN
                      dbo.BROKER_USER AS u ON u.UserId = a.UserId INNER JOIN
                      dbo.EMPLOYEE AS e ON e.EmployeeId = u.EmployeeId INNER JOIN
                      dbo.BRANCH AS r ON r.BranchId = e.BranchId INNER JOIN
                      dbo.BROKER AS b ON b.BrokerId = r.BrokerId
WHERE     (b.Status = 1) AND (e.IsActive = 1) AND (a.IsActive = 1) AND (a.PasswordChangeRequestId = @RequestID)

END
