CREATE PROCEDURE [dbo].[CP_ListActionRequestResponseByLoanId]
	@sLId uniqueidentifier
AS

SELECT 
  req.ConsumerActionRequestId

--REQUEST
, sLId
, aAppId
, aBFullName
, aCFullName
, ResponseStatus
, Description
, PdfFileDbKey
, PdfMetaDataSnapshot
, IsApplicableToBorrower
, IsApplicableToCoborrower
, IsSignatureRequested
, IsESignAllowed
, CreateOnD 
, CreatorEmployeeId
, sTitleBorrowerId

-- RESPONSE
--, ArchivedEDocId
, ConsumerCompletedD
--, RequestorAcceptedD
, ReceivingFaxNumber
, BorrowerSignedEventId
, CoborrowerSignedEventId


-- DOC TYPE
, DocTypeName
, FolderName

FROM
CP_Consumer_Action_Request req
-- This will change to VIEW
LEFT JOIN CP_Consumer_Action_Response res on req.ConsumerActionRequestId = res.ConsumerActionRequestId
JOIN Edocs_Document_Type dt on dt.DocTypeId = req.DocTypeId
JOIN Edocs_Folder folder on dt.FolderId = folder.FolderId

WHERE 
@sLId = sLId
