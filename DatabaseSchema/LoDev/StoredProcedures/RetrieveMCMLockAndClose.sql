



CREATE PROCEDURE [dbo].[RetrieveMCMLockAndClose]
	@CustomerCode varchar(50),
	@Funded bit
AS
BEGIN

DECLARE @BrokerID uniqueidentifier;
SELECT @BrokerID=brokerid FROM Broker WHERE CustomerCode=@CustomerCode

DECLARE @ReportStartDate smalldatetime;
SELECT @ReportStartDate = DATEADD("yyyy", -1, GETDATE()) -- go back 1 year only

SELECT
	sLNm as LoanNumber,
	aBLastNm as LastName,
	sLpTemplateNm as LoanProgram,
	sFinalLAmt as LoanAmount,
	sNoteIR as NoteRate,
	case( @CustomerCode )
		when 'PML0170' then	sBrokComp1Pc - sBrokerLockTotalLenderAdj
		else sBrokComp1Pc
	end as Discount,
	case( sStatusT )
		when '4' then 'Approved'
		when '21' then 'Clear to Close' 
		when '24' then 'Docs Back' 
		when '5' then 'Docs Out' 
		when '26' then 'Final Docs' 
		when '23' then 'Final Underwriting' 
		when '6' then 'Funded' 
		when '25' then 'Funding Conditions' 
		when '28' then 'Loan Submitted'
		when '13' then 'In Underwriting' 
		when '15' then 'Lead Canceled' 
		when '16' then 'Lead Declined' 
		when '12' then 'Lead New' 
		when '17' then 'Lead Other' 
		when '9' then 'Loan Canceled' 
		when '11' then 'Loan Closed' 
		when '7' then 'Loan On-hold' 
		when '0' then 'Loan Open' 
		when '18' then 'Loan Other' 
		when '27' then 'Loan Purchased' -- Loan Sold
		when '10' then 'Loan Rejected' 
		when '8' then 'Loan Suspended' 
		when '14' then 'Loan Web Consumer' 
		when '2' then 'Pre-approved' 
		when '1' then 'Pre-qual' 
		when '22' then 'Processing' 
		when '19' then 'Recorded' 
		when '3' then 'Registered' 
		when '20' then 'Shipped To Investor' 
		when '29' then 'Pre-Processing'    -- start sk 1/6/2014 opm 145251
		when '30' then 'Document Check'
		when '31' then 'Document Check Failed'
		when '32' then 'Pre-Underwriting'
		when '33' then 'Condition Review'
		when '34' then 'Pre-Doc QC'
		when '35' then 'Docs Ordered'
		when '36' then 'Docs Drawn'
		when '37' then 'Investor Conditions'       --  tied to sSuspendedByInvestorD
		when '38' then 'Investor Conditions Sent'   --  tied to sCondSentToInvestorD
		when '39' then 'Ready For Sale'
		when '40' then 'Submitted For Purchase Review'
		when '41' then 'In Purchase Review'
		when '42' then 'Pre-Purchase Conditions'
		when '43' then 'Submitted For Final Purchase Review'
		when '44' then 'In Final Purchase Review'
		when '45' then 'Clear To Purchase'
		when '46' then 'Loan Purchased'        -- don't confuse with the old { E_sStatusT.Loan_LoanPurchased, which is now Loan Sold in the UI.
		when '47' then 'Counter Offer Approved'
		when '48' then 'Loan Withdrawn'
		when '49' then 'Loan Archived'   
	end as LoanStatus, 
	br.BranchCode as Source, 
	sRAdjMarginR as Margin, 
	sRAdjLifeCapR as LifeAdjCap, 
	case( sLPurposeT ) 
		when '3' then 'Construction' 
		when '4' then 'Construction Permanent' 
		when '5' then 'Other' 
		when '0' then 'Purchase' 
		when '1' then 'Refinance' 
		when '2' then 'Refinance Cash-out' 
		when '6' then 'FHA Streamlined Refinance' 
		when '7' then 'VA IRRRL' 
	end as LoanPurpose, 
	sLtvR as LTV, 
	sRLckdD as RateLockDate, 
	case (@Funded)
		when 0 then sRLckdExpiredD			
		when 1 then sFundD
	end as StatusDate,
	case( aOccT ) 
		when '2' then 'Investment' 
		when '0' then 'Primary Residence' 
		when '1' then 'Secondary Residence' 
	end as PropertyPurpose, 
	case( sProdSpT ) 
		when '0' then 'SFR / Detached' 
		when '8' then '2 Units' 
		when '9' then '3 Units' 
		when '10' then '4 Units' 
		when '1' then 'PUD' 
		when '6' then 'Commercial' 
		when '2' then 'Condo' 
		when '3' then 'Co-Op' 
		when '4' then 'Manufactured' 
		when '7' then 'Mixed Use' 
		when '5' then 'Attached PUD' 
		when '11' then 'Modular' 
		when '12' then 'Rowhouse' 
	end as PropertyType,
	sSpState as PropertyState,
	case( sProdDocT )
		when '0' then 'Full Document'
		when '1' then 'Alt'
		when '2' then 'Lite'
		when '7' then 'NINA'
		when '6' then 'No Ratio'
		when '8' then 'NISA'
		when '5' then 'NIV (SISA)'
		when '3' then 'NIV (SIVA)'
		when '4' then 'VISA'
		when '9' then 'No Doc'
		when '10' then 'No Doc Verif Assets'
		when '11' then 'VINA'
		when '12' then 'Streamline'
	end as DocType,
	sCreditScoreType2 as QualifyingScore,
	'' as BuyDown,
	sCltvR as CLTV,
	case( aProdBCitizenT )
		when '0' then 'US Citizen'
		when '1' then 'Permanent Resident'
		when '2' then 'Non-permanent Resident'
		when '3' then 'Non-Resident Alien (Foreign National)'
	end as BorrowerCitizenship,
	case( sProdImpound )
		when 1 then 'Yes'
		when 0 then 'No'
	end as Impound,
	case( @CustomerCode ) -- dd 1/3/2013 OPM 108499
		when 'PML0034' then	sOpenedD
		else sApp1003InterviewerPrepareDate
	end as AppSubmitDate,
	sQualTopR as HousingRatio,
	sQualBottomR as DTI,
	case when (sProMIns>0)
		then 'Y'
		else 'N'
	end as HasMI, 
	br.BranchCode as BranchCode, 
	sEmployeeLoanRepName as EmployeeLoanRepName, 
	sApp1003InterviewerCompanyName as InteviewCompany,
	case( @CustomerCode )
		when 'PML0255' then	sOriginatorCompensationPercent
		else 0
	end as sOriginatorCompensationPercent
	FROM LOAN_FILE_CACHE lc WITH(NOLOCK) JOIN LOAN_FILE_CACHE_2 lc2 WITH(NOLOCK) ON lc.sLId = lc2.sLId
	                                     JOIN BRANCH br WITH(NOLOCK) ON lc.sBranchId = br.BranchId
WHERE 
	( sBrokerId = @BrokerId ) and 
	( IsValid = 1 ) and 
	( IsTemplate = 0 ) and 
	(sRateLockStatusT  = 1) and 
	(sInvestorLockRateLockStatusT  = 0) and 
	(sInvestorLockConfNum  = '' OR sInvestorLockConfNum IS NULL) and 
	(sClosedD is null) and 
	(sCanceledD is null) and 
	(sRejectD is null) and 
	(sStatusT <> 48) and -- 6/15/2015 per John Place @ MCM do not include Loan Withdrawn
	( @Funded = 1 or (datediff( dd , GETDATE() , sRLckdExpiredD ) >= 0) ) and
	( ( @Funded = 1 and not (sFundD is null) ) or ( @Funded = 0 and (sFundD is null) ) ) and
	( 
		sPreApprovD >= @ReportStartDate or
		sUnderwritingD >= @ReportStartDate or
		sOpenedD >= @ReportStartDate or
		sPreQualD >= @ReportStartDate or
		sDocsD >= @ReportStartDate or
		sSubmitD >= @ReportStartDate or
		sApprovD >= @ReportStartDate or
		sFundD >= @ReportStartDate or
		sShippedToInvestorD >= @ReportStartDate or
		sRecordedD >= @ReportStartDate or
		sClosedD >= @ReportStartDate or
		sOnHoldD >= @ReportStartDate or
		sSuspendedD >= @ReportStartDate or
		sCanceledD >= @ReportStartDate or
		sRejectD >= @ReportStartDate
	) and -- OPM 219995: Exclude MCF/PML0245 OPEN30 105% and PRIME7/1
	(@CustomerCode <> 'PML0245' or (sLpTemplateNm <> 'OPEN30 105% LTV' and sLpTemplateNm <> 'PRIME7/1'))
END



