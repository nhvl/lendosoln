-- =============================================
-- Author:		Matthew Pham
-- Create date: 01/17/2011
-- Description:	Loads filter if OrgId is specified
-- =============================================
CREATE PROCEDURE [dbo].[CONFIG_LoadFilters]
	@OrgId uniqueidentifier = null

AS
BEGIN
	SET NOCOUNT ON;

	SELECT OrgId, ConfigurationFilterXmlContent
	FROM CONFIG_DRAFT
	WHERE OrgId = COALESCE(@OrgId, OrgId)
	
END
