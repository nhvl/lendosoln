alter procedure [dbo].[EDOCS_DeleteShippingTemplate]
	@ShippingTemplateId int,
	@BrokerId uniqueidentifier = null
as

delete from EDOCS_DOCUMENT_TYPE_X_SHIPPING_TEMPLATE
	where exists
		(select *
			from EDOCS_SHIPPING_TEMPLATE s
			where EDOCS_DOCUMENT_TYPE_X_SHIPPING_TEMPLATE.ShippingTemplateId = s.ShippingTemplateId
				and s.ShippingTemplateId = @ShippingTemplateId
				and (s.BrokerId = @BrokerId
					or (s.BrokerId is null and @BrokerId is null)))

delete from EDOCS_SHIPPING_TEMPLATE
	where ShippingTemplateId = @ShippingTemplateId
		and (BrokerId = @BrokerId 
		or (BrokerId is null and @BrokerId is null))