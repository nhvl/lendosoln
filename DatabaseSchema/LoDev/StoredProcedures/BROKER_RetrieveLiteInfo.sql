-- =============================================
-- Author:		David Dao
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE dbo.BROKER_RetrieveLiteInfo
    @BrokerId UniqueIdentifier
AS
BEGIN
    
    SELECT BrokerNm, CustomerCode, Status
    FROM BROKER
    WHERE BrokerId = @BrokerId
    
END
