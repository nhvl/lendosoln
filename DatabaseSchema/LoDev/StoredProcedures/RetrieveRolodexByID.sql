ALTER  PROCEDURE [dbo].[RetrieveRolodexByID]
	@AgentID uniqueidentifier,
	@BrokerID uniqueidentifier
AS
	SELECT AgentType,
		   AgentTypeOtherDescription
	       AgentNm,
	       AgentTitle,
	       AgentPhone,
	       AgentAltPhone,
	       AgentFax,
	       AgentEmail,
	       AgentComNm,
	       AgentAddr,
	       AgentCity,
	       AgentState,
	       AgentZip,
	       AgentCounty,
	       AgentN,
	       AgentDepartmentName,
	       AgentPager,
	       AgentLicenseNumber,
	       CompanyLicenseNumber,
	       PhoneOfCompany,
	       FaxOfCompany,
	       IsNotifyWhenLoanStatusChange,
	       AgentWebsiteUrl,
	       CommissionPointOfLoanAmount,
	       CommissionPointOfGrossProfit,
	       CommissionMinBase,
	       LicenseXmlContent,
	       CompanyId,
	       AgentBranchName,
	       AgentPayToBankName,
	       AgentPayToBankCityState,
	       AgentPayToABANumber,
	       AgentPayToAccountName,
	       AgentPayToAccountNumber,
	       AgentFurtherCreditToAccountName,
	       AgentFurtherCreditToAccountNumber,
	       AgentIsApproved,
	       AgentIsAffiliated,
	       OverrideLicenses,
	       SystemId,
	       IsSettlementServiceProvider
	FROM Agent
	WHERE AgentID = @AgentID
	   AND BrokerID = @BrokerID
	if( 0!=@@error)
	begin
		RAISERROR('Error in the select statement in RetrieveRolodexByID sp', 16, 1);
		return -100;
	end
	return 0;
