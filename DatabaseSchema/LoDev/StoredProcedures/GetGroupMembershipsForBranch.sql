CREATE PROCEDURE [GetGroupMembershipsForBranch]
	@BrokerId uniqueidentifier,
	@BranchId uniqueidentifier
AS
BEGIN
	SET NOCOUNT ON
	-- GroupType 2 are Branch groups
	SELECT CASE WHEN gb.BranchId IS NULL THEN 0 ELSE 1 END AS IsInGroup, g.* 
	FROM [GROUP] g LEFT JOIN (SELECT * FROM GROUP_x_BRANCH WHERE BranchId=@BranchId) gb on g.GroupId =gb.GroupId 
	WHERE g.GroupType = 2 AND g.BrokerID=@BrokerId ORDER BY g.GroupName
END
