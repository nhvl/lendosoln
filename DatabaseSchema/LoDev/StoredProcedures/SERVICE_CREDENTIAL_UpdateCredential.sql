ALTER PROCEDURE [dbo].[SERVICE_CREDENTIAL_UpdateCredential]
	@Id int,
	@BrokerId uniqueidentifier,
	@ServiceProviderId uniqueidentifier = null,
	@ServiceProviderName varchar(100) = null,
	@UserType int = null,
	@AccountId varchar(100) = '',
	@UserName varchar(100) = null,
	@UserPassword varbinary(max) = null,
	@EncryptionKeyId uniqueidentifier,
	@IsForCreditReports bit = null,
	@IsForVerifications bit = null,
	@IsForAusSubmission bit = null,
	@IsForTitleQuotes bit = NULL,
    @IsForUcdDelivery bit = null,
	@AusOption int = null,
	@IsEnabledForNonSeamlessDu bit = null,
	@VoxVendorId int = null,
	@VoxVendorName varchar(150) = null,
	@ServiceProviderType int = null, -- TODO VOX USER: REMOVE THIS
	@IsForDocumentCapture bit = null,
	@TitleQuoteLenderServiceId int = NULL,
	@TitleQuoteLenderServiceName varchar(200) = NULL,
    @UcdDeliveryTarget int = null,
	@IsForDigitalMortgage bit = null,
	@DigitalMortgageProviderTarget tinyint = null
AS
	UPDATE SERVICE_CREDENTIAL
	SET
		ServiceProviderId = @ServiceProviderId,
		ServiceProviderName = @ServiceProviderName,
		UserType = COALESCE(@UserType, UserType),
		AccountId = @AccountId,
		UserName = COALESCE(@UserName, UserName),
		UserPassword = COALESCE(@UserPassword, UserPassword),
		EncryptionKeyId = COALESCE(@EncryptionKeyId, EncryptionKeyId),
		IsForCreditReports = COALESCE(@IsForCreditReports, IsForCreditReports),
		IsForVerifications = COALESCE(@IsForVerifications, IsForVerifications),
		IsForAusSubmission = COALESCE(@IsForAusSubmission, IsForAusSubmission),
		IsForTitleQuotes = COALESCE(@IsForTitleQuotes, IsForTitleQuotes),
        IsForUcdDelivery = COALESCE(@IsForUcdDelivery, IsForUcdDelivery),
		VoxVendorId = @VoxVendorId,
		VoxVendorName = @VoxVendorName,
		IsEnabledForNonSeamlessDu = @IsEnabledForNonSeamlessDu,
		AusOption = @AusOption,
		TitleQuoteLenderServiceId = @TitleQuoteLenderServiceId,
		TitleQuoteLenderServiceName = @TitleQuoteLenderServiceName,
		IsForDocumentCapture = COALESCE(@IsForDocumentCapture, IsForDocumentCapture),
        UcdDeliveryTarget = @UcdDeliveryTarget,
		IsForDigitalMortgage = COALESCE(@IsForDigitalMortgage, IsForDigitalMortgage),
		DigitalMortgageProviderTarget = COALESCE(@DigitalMortgageProviderTarget, DigitalMortgageProviderTarget)
	WHERE
		@Id = Id
		AND @BrokerId = BrokerId
