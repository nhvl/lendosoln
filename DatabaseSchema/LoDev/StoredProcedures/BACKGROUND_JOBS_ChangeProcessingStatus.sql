-- =============================================
-- Author:		Eric Mallare
-- Create date: 12/1/2017
-- Description:	Changes the processing status of a job.
-- =============================================
ALTER PROCEDURE [dbo].[BACKGROUND_JOBS_ChangeProcessingStatus]
	@JobId int,
	@ProcessingStatus int
AS
BEGIN
	UPDATE 
		BACKGROUND_JOBS
	SET
		ProcessingStatus=@ProcessingStatus
	WHERE
		JobId=@JobId
END