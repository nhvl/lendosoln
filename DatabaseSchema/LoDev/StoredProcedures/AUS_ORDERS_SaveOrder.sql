-- =============================================
-- Author:		Justin Lara
-- Create date: 3/7/2017
-- Description:	Saves an AUS order record.
-- =============================================
ALTER PROCEDURE [dbo].[AUS_ORDERS_SaveOrder]
	@AusOrderId int = NULL,
	@LoanId uniqueidentifier,
	@BrokerId uniqueidentifier,
	@IsManual bit,
	@CustomPosition int,
	@UnderwritingService int,
	@UnderwritingServiceOtherDescription varchar(50) = '',
	@DuRecommendation int = 0,
	@LpRiskClass int = 0,
	@LpPurchaseEligibility int = 0,
	@LpStatus int = 0,
	@LpFeeLevel char(2) = '',
	@TotalRiskClass int = 0,
	@TotalEligibilityAssessment int = 0,
	@GusRecommendation int = 0,
	@GusRiskEvaluation int = 0,
	@ResultOtherDescription varchar(50) = '',
	@CaseId varchar(30),
	@FindingsDocument uniqueidentifier,
	@OrderPlacedBy varchar(50),
	@Timestamp datetime,
	@Notes varchar(500) = '',
    @LoanIsFha bit = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE AUS_ORDERS
    SET
		LoanId = @LoanId,
		BrokerId = @BrokerId,
		IsManual = @IsManual,
		CustomPosition = @CustomPosition,
		UnderwritingService = @UnderwritingService,
		UnderwritingServiceOtherDescription = @UnderwritingServiceOtherDescription,
		DuRecommendation = @DuRecommendation,
		LpRiskClass = @LpRiskClass,
		LpPurchaseEligibility = @LpPurchaseEligibility,
		LpStatus = @LpStatus,
		LpFeeLevel = @LpFeeLevel,
		TotalRiskClass = @TotalRiskClass,
		TotalEligibilityAssessment = @TotalEligibilityAssessment,
		GusRecommendation = @GusRecommendation,
		GusRiskEvaluation = @GusRiskEvaluation,
		ResultOtherDescription = @ResultOtherDescription,
		CaseId = @CaseId,
		FindingsDocument = @FindingsDocument,
		OrderPlacedBy = @OrderPlacedBy,
		Timestamp = @Timestamp,
		Notes = @Notes,
        LoanIsFha = @LoanIsFha
	WHERE AusOrderId = @AusOrderId
	
	IF @@rowcount = 0
	BEGIN
		INSERT INTO AUS_ORDERS
		(
			LoanId,
			BrokerId,
			IsManual,
			CustomPosition,
			UnderwritingService,
			UnderwritingServiceOtherDescription,
			DuRecommendation,
			LpRiskClass,
			LpPurchaseEligibility,
			LpStatus,
			LpFeeLevel,
			TotalRiskClass,
			TotalEligibilityAssessment,
			GusRecommendation,
			GusRiskEvaluation,
			ResultOtherDescription,
			CaseId,
			FindingsDocument,
			OrderPlacedBy,
			Timestamp,
			Notes,
            LoanIsFha
		)
		VALUES
		(
			@LoanId,
			@BrokerId,
			@IsManual,
			@CustomPosition,
			@UnderwritingService,
			@UnderwritingServiceOtherDescription,
			@DuRecommendation,
			@LpRiskClass,
			@LpPurchaseEligibility,
			@LpStatus,
			@LpFeeLevel,
			@TotalRiskClass,
			@TotalEligibilityAssessment,
			@GusRecommendation,
			@GusRiskEvaluation,
			@ResultOtherDescription,
			@CaseId,
			@FindingsDocument,
			@OrderPlacedBy,
			@Timestamp,
			@Notes,
            @LoanIsFha
		)
	END
END
