
CREATE PROCEDURE [dbo].[UpdateInternalUser]
	@UserID uniqueidentifier,
	@LoginName varchar(36),
	@PasswordHash varchar(50) = NULL,		
	@FirstNm varchar(21),
	@LastNm varchar(21),
	@Permissions char(100),
	@IsActive bit,
	@PasswordExpirationD SmallDateTime = NULL,
	@IsUpdatedPassword bit = NULL,
	@PasswordSalt varchar(128) = NULL
AS
UPDATE All_User
      SET  LoginNm = @LoginName,
               PasswordHash = COALESCE(@PasswordHash, PasswordHash),
               IsActive = @IsActive,
			   PasswordExpirationD = @PasswordExpirationD,
			   IsUpdatedPassword = COALESCE(@IsUpdatedPassword, IsUpdatedPassword),
			   PasswordSalt = COALESCE(@PasswordSalt, PasswordSalt)
WHERE UserID = @UserID AND Type = 'I'
UPDATE Internal_User
       SET FirstNm = @FirstNm,
               LastNm = @LastNm,
               Permissions = @Permissions
WHERE UserID = @UserID

