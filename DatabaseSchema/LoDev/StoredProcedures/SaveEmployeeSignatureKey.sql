-- =============================================
-- Author:		Ajay Mishra
-- Create date: 07/21/11
-- Description:	Saves the FileDB key for signature file for a Employee
-- =============================================
CREATE PROCEDURE SaveEmployeeSignatureKey	
	 @EmployeeId uniqueidentifier,
	 @SignatureKey uniqueidentifier
AS
BEGIN
	UPDATE BROKER_USER set SignatureFileDBKey = @SignatureKey WHERE EmployeeId=@EmployeeId
END
