CREATE procedure [dbo].[TASK_GetResolvedTasksByUserId]
	@BrokerId uniqueidentifier,
	@UserId uniqueidentifier,
	@SinceDate datetime,
	@IsCondition bit = null 
	
AS
BEGIN
	--In order to actually count the number of tasks the user has resolved
	--(and not just tasks they resolved and then someone else edited recently)
	--you have to examine the task history xml. This could be faster if we didn't care about that.
	SELECT TaskHistoryXml FROM TASK
	WHERE
		--Since TaskStatus and BrokerId are part of an index, 
		--specifying them really helps speed.
		BrokerId = @BrokerId and
		TaskStatus = 1 /*Resolved*/ and
		TaskResolvedUserId = @UserId and
		TaskLastModifiedDate >= @SinceDate and (
		(@IsCondition  is not null and TaskIsCondition = @IsCondition ) or 
		(@IsCondition is null))
END