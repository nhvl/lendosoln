


CREATE   PROCEDURE [dbo].[RetrieveDisabledLendersOfficeUserByLoginNm]
	@LoginName varchar(36),
	@Type char(1)
AS
SELECT '' -- For now, we just need to know if this user is disabled
FROM VIEW_DISABLED_LO_USER
WHERE LoginNm = @LoginName and Type = @Type


