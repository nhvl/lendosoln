-- =============================================
-- Author:		Justin Lara
-- Create date: 8/21/2017
-- Description:	Creates a new Title order in the database.
-- =============================================
ALTER PROCEDURE [dbo].[TITLE_FRAMEWORK_ORDER_CreateOrder]
	@OrderId int OUTPUT,
	@PublicId uniqueidentifier,
	@BrokerId uniqueidentifier,
	@LoanId uniqueidentifier,
	@TransactionId uniqueidentifier,
	@OrderNumber varchar(50),
	@OrderedDate datetime,
	@OrderedBy varchar(50),
	@IsTestOrder bit,
	@ResultsFileDbKey uniqueidentifier,
	@FeesFileDbKey uniqueidentifier,
	@ConfigFileDbKey uniqueidentifier,
	@LenderServiceId int,
	@LenderServiceName varchar(200),
	@TitleProviderCode varchar(20) = NULL,
	@TitleProviderName varchar(200) = NULL,
	@TitleProviderRolodexId uniqueidentifier = NULL,
	@Status int,
	@ProductType int,
	@AppliedDate datetime = NULL
AS
BEGIN
	INSERT INTO TITLE_FRAMEWORK_ORDER (
		PublicId,
		BrokerId,
		LoanId,
		TransactionId,
		OrderNumber,
		OrderedDate,
		OrderedBy,
		IsTestOrder,
		ResultsFileDbKey,
		FeesFileDbKey,
		ConfigFileDbKey,
		LenderServiceId,
		LenderServiceName,
		TitleProviderCode,
		TitleProviderName,
		TitleProviderRolodexId,
		[Status],
		ProductType,
		AppliedDate
	)
	VALUES (
		@PublicId,
		@BrokerId,
		@LoanId,
		@TransactionId,
		@OrderNumber,
		@OrderedDate,
		@OrderedBy,
		@IsTestOrder,
		@ResultsfileDbKey,
		@FeesFileDbKey,
		@ConfigFileDbKey,
		@LenderServiceId,
		@LenderServiceName,
		@TitleProviderCode,
		@TitleProviderName,
		@TitleProviderRolodexId,
		@Status,
		@ProductType,
		@AppliedDate
	)

	SET @OrderId = SCOPE_IDENTITY();
END
