CREATE PROCEDURE GetReportQueryDetails
	@BrokerId Guid , @QueryId Guid = NULL , @QueryName Varchar( 64 ) = NULL
AS
	IF @QueryId IS NOT NULL
	BEGIN
		SELECT
			QueryId , BrokerId , EmployeeId , QueryName
		FROM
			Report_Query AS r
		WHERE
			r.QueryId = r.QueryId
			AND
			r.BrokerId = @BrokerId
	END
	ELSE
	BEGIN
		SELECT
			QueryId , BrokerId , EmployeeId , QueryName
		FROM
			Report_Query AS r
		WHERE
			r.QueryName = COALESCE( @QueryName , r.QueryName )
			AND
			r.BrokerId = @BrokerId
	END
	IF ( @@error != 0 )
	BEGIN
		RAISERROR('Error in the select statement in GetReportQueryDetails sp', 16, 1);
		RETURN -100;
	END
