-- =============================================
-- Author:		Justin Kim
-- Create date: 9/19/17
-- Description:	Delete a predefined adjustment.
-- =============================================
ALTER PROCEDURE [dbo].[Predefined_Adjustment_Delete]
	@BrokerId uniqueidentifier,
	@Id int
AS
BEGIN
	Delete From [dbo].[Predefined_Adjustment]
	where Id = @Id AND BrokerId = @BrokerId
END
