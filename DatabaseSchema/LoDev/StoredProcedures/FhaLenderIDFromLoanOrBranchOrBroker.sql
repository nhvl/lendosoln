
-- =============================================
-- Author:		Scott Kibler
-- Create date: September 3, 2013
-- Description: Get the loan, branch level, or broker level fhalenderid (in that order)
-- or return blank.
-- For opm 72500
-- =============================================
CREATE PROCEDURE [dbo].[FhaLenderIDFromLoanOrBranchOrBroker]
	 @loanid uniqueidentifier
	,@branchid uniqueidentifier
	,@fhalenderidcode varchar(10) OUTPUT --! OUT
AS
BEGIN
	----- region: declarations -----------------------
	declare @isValidAndNonBlank bit
	
	declare @loanfhalenderidcode varchar(10)
	
	declare @branchfhalenderidcode varchar(10)
	
	declare @brokerid uniqueidentifier
	declare @brokerFhaLenderId varchar(10)
	declare @UseFHATOTALProductionAccount bit
	declare @branchFhaIsModified bit
	
	
	---- region: get the field at every level ----------	
	select
		@loanfhalenderidcode = sfhalenderidcode
	from loan_file_e with(nolock)
	where slid=@loanid

	select 
		  @brokerid = brokerid
		, @branchfhalenderidcode=fhalenderid
		, @branchFhaIsModified = isFhaLenderIDModified
	from branch with(nolock)
	where branchid = @branchid

	select 
		 @UseFHATOTALProductionAccount = UseFHATOTALProductionAccount
		,@brokerFhaLenderId = fhalenderid
	from broker with(nolock)
	where brokerid=@brokerid	
	
	
	----- region: get the first field that is valid --------	
	exec FhaLenderIdIsValidAndNonBlank @loanfhalenderidcode, @UseFHATOTALProductionAccount, @isValidAndNonBlank OUTPUT
	if @isValidAndNonBlank = 1
	begin
		select @fhalenderidcode = @loanfhalenderidcode
		return
	end
	
	if @branchFhaIsModified=1
	begin
		exec FhaLenderIdIsValidAndNonBlank @branchfhalenderidcode, @UseFHATOTALProductionAccount, @isValidAndNonBlank OUTPUT

		if @isValidAndNonBlank = 1
		begin
			select @fhalenderidcode = @branchfhalenderidcode
			return
		end
	end
	
	exec FhaLenderIdIsValidAndNonBlank @brokerFhaLenderId, @UseFHATOTALProductionAccount, @isValidAndNonBlank OUTPUT
	if @isValidAndNonBlank = 1
	begin
		select @fhalenderidcode = @brokerFhaLenderId
		return
	end
	
	----- default to empty -----------------
	select @fhalenderidcode = ''

END
