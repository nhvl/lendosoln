CREATE PROCEDURE dbo.EDOCS_ShippingTemplateAddCheckName
	@ShippingTemplateName varchar(50),
	@List varchar(max),
	@BrokerId uniqueidentifier
as

select 1
	from EDOCS_SHIPPING_TEMPLATE 
	where ShippingTemplateName = @ShippingTemplateName
		and (BrokerId = @BrokerId or BrokerId is null)