-- =============================================
-- Author:		Eric Mallare
-- Create date: 9/23/2016
-- Description:	Retrieves the login name 
-- =============================================
ALTER PROCEDURE [dbo].[RetrieveUserLoginByUserId]
	@UserId uniqueidentifier
AS
BEGIN
	SELECT
		LoginNm
	FROM
		ALL_USER
	WHERE
		UserId=@UserId
END
