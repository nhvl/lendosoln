ALTER PROCEDURE [dbo].[GetUserByLoginName]
	@LoginName varchar(36),
	@Type char(1),
	@BrokerId uniqueidentifier = null
AS
	if @Type = 'P'
		BEGIN
			SELECT CAST(UserId as varchar(40)) + ',' + CAST(BrokerPmlSiteId as varchar(40)) AS ValueField, 
				   BrokerNm,
				   UserFirstNm as FirstNm, 
				   UserLastNm as LastNm,
				   UserId
			FROM 
				View_Active_Pml_User_With_Broker_Info WITH (NOLOCK) 
			WHERE 
				LoginNm = @LoginName AND 
				Type = @Type AND
				COALESCE(@BrokerId, BrokerId)=BrokerId
		END
	ELSE
		BEGIN
			SELECT 
				BrokerId, 
				UserId, 
				UserFirstNm as FirstNm, 
				UserLastNm as LastNm
			FROM 
				View_Active_LO_User WITH (NOLOCK) 
			WHERE 
				LoginNm = @LoginName AND 
				Type = @Type AND
				COALESCE(@BrokerId, BrokerId)=BrokerId
		END
