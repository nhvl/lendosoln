CREATE PROCEDURE ListRolesForDiscNotif 
AS
select RoleId, RoleModifiableDesc
from role
union
select '00000000-0000-0000-0000-000000000000', 'Assigned Employees'
union
select '00000000-0000-0000-0000-000000000002', 'Active Participants'
union
select '00000000-0000-0000-0000-000000000003', 'Past Participants'
union
select '00000000-0000-0000-0000-000000000001', 'Everyone'
