CREATE PROCEDURE RetrieveLastDoDuLpLoginNmByUserId 
	@UserId Guid
AS
BEGIN
	SELECT DOLastLoginNm, DULastLoginNm, LpLastLoginNm 
	FROM Broker_User
	WHERE UserId = @UserId
END
