-- =============================================
-- Author:		paoloa
-- Create date: 5/1/2013
-- Description:	
-- =============================================
ALTER PROCEDURE [dbo].[SaveAppraisalOrder]
	-- Add the parameters for the stored procedure here
	@AppraisalOrderId uniqueidentifier,
	@LoanId uniqueidentifier,
	@VendorId uniqueidentifier,
	@OrderNumber varchar(50),
	@OrderedDate smalldatetime,
	@Status varchar(20),
	@StatusDate smalldatetime,
	@AppraisalOrderXmlContent varchar(max),
	@OrderDataUpdateXmlContent varchar(max),
	@UploadedFilesXmlContent varchar(max),
	@OrderHistoryXmlContent varchar(max),
	@EmbeddedDocumentIdsXmlContent varchar(max),
	-- APPRAISAL_ORDER FIELDS--
	@BrokerId uniqueidentifier,
	@IsValid bit,
	@AppraisalOrderType int,
	@ValuationMethod int,
	@DeliveryMethod int,
	@AppraisalFormType int,
	@ProductName varchar(100),
	@FhaDocFileId varchar(50),
	@UcdpAppraisalId varchar(50),
	@Notes varchar(max),
	@NeededDate smalldatetime,
	@ExpirationDate smalldatetime,
	@ReceivedDate smalldatetime,
	@SentToBorrowerDate smalldatetime,
	@BorrowerReceivedDate smalldatetime,
	@RevisionDate smalldatetime,
	@ValuationEffectiveDate smalldatetime,
	@SubmittedToFha smalldatetime,
	@SubmittedToUcdp smalldatetime,
	@CuRiskScore decimal(4,1),
	@OvervaluationRiskT int,
	@PropertyEligibilityRiskT int,
	@AppraisalQualityRiskT int,
	@DeletedDate smalldatetime = null,
	@DeletedByUserId uniqueidentifier = null,
	@AppIdAssociatedWithProperty uniqueidentifier = null,
	@LinkedReoId uniqueidentifier = null,
	@PropertyAddress varchar(60) = null,
	@PropertyCity varchar(72) = null,
	@PropertyState varchar(2) = null,
	@PropertyZip varchar(5) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	UPDATE APPRAISAL_ORDER_INFO
	SET
		AppraisalOrderId = @AppraisalOrderId,
		OrderedDate = @OrderedDate,
		Status = @Status,
		StatusDate = @StatusDate,
		AppraisalOrderXmlContent = @AppraisalOrderXmlContent,
		OrderDataUpdateXmlContent = @OrderDataUpdateXmlContent,
		UploadedFilesXmlContent = @UploadedFilesXmlContent,
		OrderHistoryXmlContent = @OrderHistoryXmlContent,
		EmbeddedDocumentIdsXmlContent = @EmbeddedDocumentIdsXmlContent
	WHERE
		sLId = @LoanId AND VendorId = @VendorId AND OrderNumber = @OrderNumber

	IF @@ROWCOUNT = 0
		BEGIN
			EXEC APPRAISAL_ORDER_Create
				@AppraisalOrderId,
				@LoanId,
				@BrokerId,
				@IsValid,
				@AppraisalOrderType,
				@ValuationMethod,
				@DeliveryMethod,
				@AppraisalFormType,
				@ProductName,
				@OrderNumber,
				@FhaDocFileId,
				@UcdpAppraisalId,
				@Notes,
				@OrderedDate,
				@NeededDate,
				@ExpirationDate,
				@ReceivedDate,
				@SentToBorrowerDate,
				@BorrowerReceivedDate,
				@RevisionDate,
				@ValuationEffectiveDate,
				@SubmittedToFha,
				@SubmittedToUcdp,
				@CuRiskScore,
				@OvervaluationRiskT,
				@PropertyEligibilityRiskT,
				@AppraisalQualityRiskT,
				@DeletedDate,
				@DeletedByUserId,
				@AppIdAssociatedWithProperty,
				@LinkedReoId,
				@PropertyAddress,
				@PropertyCity,
				@PropertyState,
				@PropertyZip
		
			INSERT INTO APPRAISAL_ORDER_INFO
				(sLId, VendorId, OrderNumber, OrderedDate, Status, StatusDate, AppraisalOrderXmlContent, OrderDataUpdateXmlContent, UploadedFilesXmlContent, OrderHistoryXmlContent, EmbeddedDocumentIdsXmlContent, AppraisalOrderId)
			VALUES
				(@LoanId, @VendorId, @OrderNumber, @OrderedDate, @Status, @StatusDate, @AppraisalOrderXmlContent, @OrderDataUpdateXmlContent, @UploadedFilesXmlContent, @OrderHistoryXmlContent, @EmbeddedDocumentIdsXmlContent, @AppraisalOrderId)
		END
	ELSE
		BEGIN
			EXEC APPRAISAL_ORDER_Update
				@AppraisalOrderId,
				@LoanId,
				@BrokerId,
				@IsValid,
				@AppraisalOrderType,
				@ValuationMethod,
				@DeliveryMethod,
				@AppraisalFormType,
				@ProductName,
				@OrderNumber,
				@FhaDocFileId,
				@UcdpAppraisalId,
				@Notes,
				@OrderedDate,
				@NeededDate,
				@ExpirationDate,
				@ReceivedDate,
				@SentToBorrowerDate,
				@BorrowerReceivedDate,
				@RevisionDate,
				@ValuationEffectiveDate,
				@SubmittedToFha,
				@SubmittedToUcdp,
				@CuRiskScore,
				@OvervaluationRiskT,
				@PropertyEligibilityRiskT,
				@AppraisalQualityRiskT,
				@DeletedDate,
				@DeletedByUserId,
				@AppIdAssociatedWithProperty,
				@LinkedReoId,
				@PropertyAddress,
				@PropertyCity,
				@PropertyState,
				@PropertyZip
		END
END