CREATE procedure [dbo].[EDOCS_UpdateDocTypeCheckName]
	@BrokerId uniqueidentifier,	
	@DocTypeName varchar(50),
	@ClassificationId int = 0
as

select 1 
	from EDOCS_DOCUMENT_TYPE with (nolock)
	where DocTypeName = @DocTypeName
		and BrokerId = @BrokerId
		and IsValid = 1