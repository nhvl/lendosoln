-- ============================================================
-- Author:      Generated by application LqbEntitiesToSqlTables
-- Create date: 5/21/2018
-- Description: Retrieve a row from the CONSUMER_X_REAL_PROPERTY table associated with a given loan.
-- ============================================================
ALTER PROCEDURE [dbo].[CONSUMER_X_REAL_PROPERTY_RetrieveByLoanId]
	@LoanId UniqueIdentifier
AS
BEGIN
	SELECT
		ConsumerRealPropertyAssociationId,
		ConsumerId,
		RealPropertyId,
		AppId,
		IsPrimary
	FROM [dbo].[CONSUMER_X_REAL_PROPERTY]
	WHERE LoanId = @LoanId
END
