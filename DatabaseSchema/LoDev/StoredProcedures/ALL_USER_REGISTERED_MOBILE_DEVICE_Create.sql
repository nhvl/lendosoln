-- =============================================
-- Author:		Huy Nguyen	
-- Create date: 5/5/2015
-- Description:	
-- =============================================
CREATE PROCEDURE dbo.ALL_USER_REGISTERED_MOBILE_DEVICE_Create
	@BrokerId UniqueIdentifier,
    @UserId UniqueIdentifier,
	@DeviceName varchar(100),
	@PasswordHash varchar(128) ,
	@PasswordSalt varchar(128)
AS
BEGIN
    --check value exist
	--insert value if not exist
	BEGIN
		INSERT INTO ALL_USER_REGISTERED_MOBILE_DEVICE (BrokerId, UserId, DeviceName, PasswordHash, PasswordSalt, CreateD,FailedAttempts,IsActive,DeviceUniqueId)
								   VALUES  (@BrokerId, @UserId, @DeviceName, @PasswordHash,@PasswordSalt, GETDATE(),0,1,'')
	END
END
