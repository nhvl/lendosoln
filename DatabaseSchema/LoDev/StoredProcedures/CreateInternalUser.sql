
CREATE  PROCEDURE [dbo].[CreateInternalUser]
	@UserID uniqueidentifier,
	@LoginName varchar(36),
	@PasswordHash varchar(50),		
	@FirstNm varchar(21),
	@LastNm varchar(21),
	@Permissions char(100),
	@IsActive bit,
	@PasswordExpirationD SmallDateTime = NULL,
	@IsUpdatedPassword bit,
	@PasswordSalt varchar(128)
AS
declare @tranPoint sysname
set @tranPoint = object_name(@@procId) + CAST(@@nestlevel as varchar(10))
	
BEGIN TRANSACTION
SAVE TRANSACTION @tranPoint
	
INSERT INTO all_user(UserId, LoginNm, PasswordHash, IsActive, BrokerPmlSiteId, Type, PasswordExpirationD, IsUpdatedPassword, PasswordSalt)
                     VALUES ( @userId, @LoginName, @PasswordHash, @IsActive, '00000000-0000-0000-0000-000000000000', 'I', @PasswordExpirationD, @IsUpdatedPassword, @PasswordSalt )
if( 0!=@@error )
begin
	RAISERROR('error inserting into All_User table in CreateInternalUser sp', 16, 1);
	goto ErrorHandler
end
--print 'done all_user'
INSERT INTO internal_user(UserId, FirstNm, LastNm, Permissions  )
                            VALUES ( @userId, @FirstNm, @LastNm, @Permissions );
if( 0!=@@error )
begin
	RAISERROR('error inserting into internal_user table in CreateInternalUser sp', 16, 1);
	goto ErrorHandler
end
COMMIT TRANSACTION
RETURN 0
ErrorHandler:
	rollback tran @tranPoint
	rollback transaction
	return -100

