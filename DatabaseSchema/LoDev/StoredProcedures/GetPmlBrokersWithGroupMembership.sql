CREATE PROCEDURE [GetPmlBrokersWithGroupMembership]
	@BrokerId uniqueidentifier,
	@GroupId uniqueidentifier
AS
BEGIN
	SET NOCOUNT ON
	SELECT CASE WHEN gp.GroupId IS NULL THEN 0 ELSE 1 END AS IsInGroup, p.[Name], p.PmlBrokerId, p.Addr, p.City, p.State, p.Zip
	FROM PML_BROKER p LEFT JOIN (SELECT * FROM GROUP_x_PML_BROKER WHERE GroupId=@GroupId) gp on p.PmlBrokerId = gp.PmlBrokerId 
	WHERE p.BrokerId = @BrokerId ORDER BY p.[Name]
END
