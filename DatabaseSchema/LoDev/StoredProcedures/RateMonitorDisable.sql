-- =============================================
-- Author:		Paolo Arrastia
-- Create date: 24 Jul 2012
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[RateMonitorDisable]
	-- Add the parameters for the stored procedure here
	@monitorId bigint, 
	@disableReason varchar(100),
	@currDate DateTime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE RATE_MONITOR
		SET DisabledReason = @disableReason, DisabledDate = @currDate, IsEnabled = 0
		WHERE Id = @monitorId;
END
