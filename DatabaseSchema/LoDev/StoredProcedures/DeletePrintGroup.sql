CREATE PROCEDURE DeletePrintGroup 
	@GroupID Guid,
	@BrokerID Guid
AS
	DELETE FROM Print_Group WHERE GroupID = @GroupID AND OwnerID = @BrokerID
