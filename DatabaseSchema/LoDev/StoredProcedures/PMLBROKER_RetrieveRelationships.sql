-- =============================================
-- Author:		Michael Leinweaver
-- Create date: 3/8/2016
-- Description:	Retrieves relationships for an OC
--				with a specific OC role.
-- =============================================
ALTER PROCEDURE [dbo].[PMLBROKER_RetrieveRelationships]
	@BrokerId uniqueidentifier,
	@PmlBrokerId uniqueidentifier,
	@OCRoleT int
AS
BEGIN
	SELECT EmployeeId, EmployeeName, EmployeeRoleT
	FROM PML_BROKER_RELATIONSHIPS
	WHERE 
		BrokerId = @BrokerId AND 
		PmlBrokerId = @PmlBrokerId AND
		OCRoleT = @OCRoleT
END
GO
