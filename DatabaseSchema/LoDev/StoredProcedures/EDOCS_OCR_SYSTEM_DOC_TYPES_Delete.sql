-- =============================================
-- Author:		Michael Leinweaver
-- Create date: 9/8/2017
-- Description:	Marks a system edoc classification as disabled.
-- =============================================
ALTER PROCEDURE [dbo].[EDOCS_OCR_SYSTEM_DOC_TYPES_Delete]
	@Id int
AS
BEGIN
	IF EXISTS(SELECT 1 FROM EDOCS_DOCUMENT_TYPE WHERE ClassificationId = @Id)
	BEGIN
		RAISERROR('Cannot delete system doc type assigned to a lender doc type.', 16, 1)
		RETURN
	END

	UPDATE TOP(1) EDOCS_OCR_SYSTEM_DOC_TYPES
	SET Enabled = 0
	WHERE Id = @Id
END
