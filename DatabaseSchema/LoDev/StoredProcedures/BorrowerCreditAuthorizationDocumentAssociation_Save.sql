ALTER PROCEDURE [dbo].[BorrowerCreditAuthorizationDocumentAssociation_Save]
	@AppId uniqueidentifier,
	@IsCoborrower bit,
	@DocumentId uniqueidentifier
AS
BEGIN
	UPDATE BORROWER_CREDIT_AUTHORIZATION_DOCUMENT_ASSOCIATION
	SET DocumentId = @DocumentId
	WHERE AppId = @AppId AND IsCoborrower = @IsCoborrower
	
	IF @@ROWCOUNT = 0
		INSERT INTO BORROWER_CREDIT_AUTHORIZATION_DOCUMENT_ASSOCIATION
			(AppId, IsCoborrower, DocumentId, LoanId, BrokerId)
		SELECT
			@AppId,@IsCoborrower,@DocumentId,a.sLId,lfc.sBrokerId
		FROM APPLICATION_A a
		JOIN LOAN_FILE_CACHE lfc ON a.sLId = lfc.sLId
		WHERE a.aAppId = @AppId
END
