-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE RetrieveByPassBgCalcForGfeAsDefaultByUserId
	@UserId UniqueIdentifier
AS
BEGIN
	SELECT ByPassBgCalcForGfeAsDefault FROM Broker_User WHERE UserID = @UserId
END
