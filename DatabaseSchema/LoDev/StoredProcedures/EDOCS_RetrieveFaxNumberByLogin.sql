ALTER PROCEDURE [dbo].[EDOCS_RetrieveFaxNumberByLogin]
	@DocRouterLogin varchar(50)
AS
SELECT 
	edoc.FaxNumber, edoc.BrokerId, edoc.EncryptionKeyId, edoc.EncryptedDocRouterPassword
FROM 
	EDOCS_FAX_NUMBER edoc JOIN
	[BROKER] br on edoc.brokerid = br.brokerid
WHERE 
	@DocRouterLogin = DocRouterLogin AND
	br.status = 1