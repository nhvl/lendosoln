ALTER PROCEDURE [dbo].CUSTOM_FOLDER_RETRIEVE_BY_BROKERID_AND_EMPLOYEEID
	@BrokerId uniqueidentifier,
	@EmployeeId uniqueidentifier
AS
BEGIN
	SELECT FolderId, EmployeeId, InternalName, ExternalName, LeadLoanStatus, SortOrder, ParentFolderId
	FROM CUSTOM_FOLDER
	WHERE BrokerId = @BrokerId AND (EmployeeId = @EmployeeId OR EmployeeId is NULL)
END
