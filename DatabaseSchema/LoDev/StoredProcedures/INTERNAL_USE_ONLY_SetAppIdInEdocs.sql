CREATE PROCEDURE [dbo].[INTERNAL_USE_ONLY_SetAppIdInEdocs]
as
	
update edocs_document
set aAppId = s.aappid
from edocs_document ed 
join
(
	select aAppId, a.slid from application_a  as  a 
	join (select sLId,  min(PrimaryRankIndex) as PrimaryRankIndex  
	from application_a where slid in (select slid from edocs_document where slid is not null) group by sLId ) as p 
	on a.sLId = p.sLId and  a.PrimaryRankIndex = p.PrimaryRankIndex
	
)  as s
on ed.slid = s.slid

where
ed.IsValid = 1 -- don't stomp on anything that had a null appId because the app was deleted
AND
ed.aappid is null -- don't stomp on anything that was already migrated and perhaps changed from the primary app





		
