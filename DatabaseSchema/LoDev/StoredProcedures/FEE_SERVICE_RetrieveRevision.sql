-- Get Specific Revision--
CREATE PROCEDURE [dbo].[FEE_SERVICE_RetrieveRevision] 
     @BrokerId uniqueidentifier,
     @Id int
AS
BEGIN
     SELECT f.BrokerId, f.Comments, f.FeeTemplateXmlContent, f.FileDbKey, f.UploadedByUserId, f.UploadedD,
           COALESCE(e.UserFirstNm + ' ' + e.UserLastNm, '') as UploadedByUserName, f.Id, f.IsTest
     FROM FEE_SERVICE_REVISION f LEFT JOIN EMPLOYEE e on f.UploadedByUserId = e.EmployeeUserId
     WHERE BrokerId = @BrokerId AND Id = @Id AND FileDbKey <> ''
END
