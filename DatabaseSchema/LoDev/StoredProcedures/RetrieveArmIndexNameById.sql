-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE dbo.RetrieveArmIndexNameById
	@IndexIdGuid UniqueIdentifier
AS
BEGIN
	SELECT IndexNameVstr FROM ARM_INDEX WHERE IndexIdGuid = @IndexIdGuid
END
