-- =============================================
-- Author:		Eric Mallare
-- Create date: 9/7/2016
-- Description:	Loads a specific lender client certificate.
-- =============================================
ALTER PROCEDURE [dbo].[GetLenderClientCertificate] 
	@BrokerId uniqueidentifier,
	@CertificateId uniqueidentifier
AS
BEGIN
	SELECT
		CertificateId, BrokerId, IsRevoked, Description, 
		NotesFileDbKey, Level, UserType, CreatedDate, RevokedDate, AssociatedUserId
	FROM
		Broker_Client_Certificate
	WHERE
		BrokerId=@BrokerId AND
		CertificateId=@CertificateId
	
	SELECT 
		gbcc.GroupId, g.GroupName, g.Description, g.GroupType
	FROM 
		Group_X_Broker_Client_Certificate gbcc JOIN
		[dbo].[Group] g ON gbcc.GroupId=g.GroupId
	WHERE
		gbcc.CertificateId=@CertificateId
END
