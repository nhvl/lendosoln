-- =============================================
-- Author:		David Dao
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE dbo.BROKER_GLOBAL_IP_WHITELIST_Delete
    @Id int,
    @BrokerId UniqueIdentifier
AS
BEGIN

		DELETE FROM BROKER_GLOBAL_IP_WHITELIST 
		WHERE Id = @Id AND BrokerId = @BrokerId
END
