-- =============================================
-- Author:		Rodrigo Noronha
-- Create date: 9/21/2012
-- Description:	Delete an existing document-condition
--				association.
-- =============================================
CREATE PROCEDURE [dbo].[DOCCONDITION_Delete] 
	-- Add the parameters for the stored procedure here
	@LoanId uniqueidentifier,
	@TaskId varchar(10),
	@DocId uniqueIdentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	BEGIN TRAN
			DELETE TD 
			FROM TASK_x_EDOCS_DOCUMENT TD
			WHERE TD.sLId = @LoanId
			AND TD.TaskId = @TaskId
			AND TD.DocumentId = @DocId
	COMMIT
END
