ALTER PROCEDURE [dbo].[COMPLIANCEEAGLE_ORDERED_PRODUCT_Save]
	@BrokerId uniqueidentifier,
	@LoanId uniqueidentifier,
	@TransactionId uniqueidentifier,
	@ReportId varchar(20),
	@ProductType int,
	@ProductDesc varchar(20)
AS
BEGIN
	INSERT INTO COMPLIANCEEAGLE_ORDERED_PRODUCT
	(
		BrokerId,
		LoanId,
		TransactionId,
		ReportId,
		ProductType,
		ProductDesc
	)
	VALUES
	(
		@BrokerId,
		@LoanId,
		@TransactionId,
		@ReportId,
		@ProductType,
		@ProductDesc
	);
END
