


CREATE PROCEDURE [dbo].[RetrieveLoanConditionByCondId]
	@CondId uniqueidentifier
AS
	SELECT LoanId, CondId, CondDesc, CondStatus, IsLoanStillValid, CondCategoryDesc, CondStatusDate,
           CondOrderedPosition, CreatedD, CondIsHidden, CondIsValid, CondCompletedByUserNm, CondNotes, CondDeleteD, CondDeleteByUserNm
FROM Condition 
where CondId = @CondId

