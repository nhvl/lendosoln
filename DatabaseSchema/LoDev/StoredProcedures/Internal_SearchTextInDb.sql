









CREATE PROCEDURE [dbo].[Internal_SearchTextInDb]
	-- Add the parameters for the stored procedure here
	@str varchar(1000), 
	@type varchar(2)
AS

declare @like as varchar(1002)
set @like = '%' + @str + '%'
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;




    -- Insert statements for procedure here
	If @type = 'C'
		Begin
			Select column_name as name, table_name, modify_date, type
			from Information_Schema.columns, sys.objects
			where column_name like @str and table_name=name
		End

	If @type = 'U'
		Begin
			Select name, modify_date
			from sys.tables
			where name like @str
		End
	
	If @type = 'V' or @type = 'T' or @type = 'P' or @type = 'TR' or @type = 'FN'
		Begin
			Select name, modify_date, type, OBJECT_DEFINITION(object_id) AS code
			from sys.objects
			where object_definition(object_id) like @str and (type = @type)
		End
	
	If @type = 'CC'
		Begin
			Select name, modify_date, object_definition(object_id) as code, object_name(parent_object_id) as parent
			from sys.objects
			where object_definition(object_id) like @str and (type = 'C')
		End
	
END









