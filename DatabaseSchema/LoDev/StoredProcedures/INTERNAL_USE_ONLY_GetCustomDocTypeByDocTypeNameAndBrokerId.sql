CREATE PROCEDURE [dbo].[INTERNAL_USE_ONLY_GetCustomDocTypeByDocTypeNameAndBrokerId]
	@DocTypeName varchar(50),
	@BrokerId uniqueidentifier
as
	select docTypeId from edocs_document_type with (nolock)
	where
	docTypeName = @DocTypeName
	and
	BrokerId = @BrokerId
	and
	BrokerId is not null
	