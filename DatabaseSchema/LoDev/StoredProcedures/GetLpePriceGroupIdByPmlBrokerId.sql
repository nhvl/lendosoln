-- =============================================
-- Author:		Eduardo Michel
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[GetLpePriceGroupIdByPmlBrokerId] 
	-- Add the parameters for the stored procedure here
	@PmlBrokerId uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT MiniCorrespondentLpePriceGroupId, CorrespondentLpePriceGroupId
	FROM dbo.PML_BROKER as p
	Where @PmlBrokerId = PmlBrokerId
END