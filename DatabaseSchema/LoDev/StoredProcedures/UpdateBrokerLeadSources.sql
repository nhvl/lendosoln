CREATE PROCEDURE UpdateBrokerLeadSources
	@BrokerId Guid , @LeadSourcesXmlContent Text
AS
	UPDATE
		Broker
	SET
		LeadSourcesXmlContent = @LeadSourcesXmlContent
	WHERE
		BrokerId = @BrokerId
	IF( 0!=@@error)
	BEGIN
		RAISERROR('Error in updating Broker table in UpdateBrokerLeadSources sp', 16, 1);
		RETURN -100;
	END
	RETURN 0;
	
