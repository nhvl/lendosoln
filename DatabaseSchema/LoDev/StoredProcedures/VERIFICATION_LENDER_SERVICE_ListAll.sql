-- =============================================
-- Author:		EricMallare
-- Create date: 5/4/2017
-- Description:	Loads all lender services.
-- =============================================
ALTER PROCEDURE [dbo].[VERIFICATION_LENDER_SERVICE_ListAll]
	@BrokerId uniqueidentifier
AS
BEGIN
	SELECT
		ls.LenderServiceId, ls.ServiceT, ls.VendorId, ls.ResellerId, ls.DisplayName, ls.DisplayNameLckd,
		ls.IsEnabledForLqbUsers, ls.EnabledEmployeeGroupId, ls.IsEnabledForTpoUsers, ls.EnabledOcGroupId,
		ls.AllowPaymentByCreditCard, ls.VoeUseSpecificEmployerRecordSearch, ls.VoeEnforceUseSpecificEmployerRecordSearch,
		ls.VoaAllowImportingAssets,     ls.VoaAllowRequestWithoutAssets,     ls.VoaEnforceAllowRequestWithoutAssets,
        ls.VoeAllowImportingEmployment, ls.VoeAllowRequestWithoutEmployment, ls.VoeEnforceRequestsWithoutEmployment,
        ls.CanBeUsedForCredentialsInTpo
	FROM
		VERIFICATION_LENDER_SERVICE ls 
	WHERE
		ls.BrokerId=@BrokerId
END
