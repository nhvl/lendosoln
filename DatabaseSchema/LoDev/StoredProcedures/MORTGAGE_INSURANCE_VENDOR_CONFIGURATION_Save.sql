-- =============================================
-- Author:		Isaac Ribakoff
-- Create date: 9/19/2014
-- Description:	Saves MI Vendor Config
-- =============================================
ALTER PROCEDURE [dbo].[MORTGAGE_INSURANCE_VENDOR_CONFIGURATION_Save]
	@VendorId uniqueidentifier,
	@VendorType int,
	@IsValid bit,
	@ExportPath_Test varchar(1024),
	@ExportPath_Production varchar(1024),
	@IsUseAccountId bit,
	@SendAuthTicketWithOrders bit,
	@EnableConnectionTest bit,
	@UsesSplitPremiumPlans bit,
	@IncludesEligibilityEvalInQuote bit = 0
	
AS
BEGIN
	UPDATE MORTGAGE_INSURANCE_VENDOR_CONFIGURATION
	SET VendorType = @VendorType,
		IsValid = @IsValid,
		ExportPath_Test = @ExportPath_Test,
		ExportPath_Production = @ExportPath_Production,
		IsUseAccountId = @IsUseAccountId,
		SendAuthTicketWithOrders = @SendAuthTicketWithOrders,
		EnableConnectionTest = @EnableConnectionTest,
		IncludesEligibilityEvalInQuote = @IncludesEligibilityEvalInQuote,
		UsesSplitPremiumPlans = @UsesSplitPremiumPlans
	WHERE VendorId = @VendorId
	
	IF @@rowcount = 0
	BEGIN
		INSERT INTO MORTGAGE_INSURANCE_VENDOR_CONFIGURATION
		(
			VendorType,
			IsValid,
			ExportPath_Test,
			ExportPath_Production,
			IsUseAccountId,
			SendAuthTicketWithOrders,
			EnableConnectionTest,
			IncludesEligibilityEvalInQuote,
			UsesSplitPremiumPlans
		)
		Values
		(
			@VendorType,
			@IsValid,
			@ExportPath_Test,
			@ExportPath_Production,
			@IsUseAccountId,
			@SendAuthTicketWithOrders,
			@EnableConnectionTest, 
			@IncludesEligibilityEvalInQuote,
			@UsesSplitPremiumPlans
		)
	END
END