﻿-- =============================================
-- Author:		Eduardo Michel
-- Create date: 
-- Description:	
-- =============================================
ALTER PROCEDURE [dbo].[STATUS_EVENT_GET] 
	-- Add the parameters for the stored procedure here
	@BrokerId uniqueIdentifier, 
	@LoanId uniqueIdentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT *
	FROM STATUS_EVENT
	WHERE BrokerId = @BrokerId AND LoanId = @LoanId;
END
