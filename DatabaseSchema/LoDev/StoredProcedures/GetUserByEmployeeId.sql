CREATE PROCEDURE GetUserByEmployeeId
	@EmployeeId Guid
AS
	SELECT UserId FROM Broker_User WITH (NOLOCK) WHERE EmployeeId = @EmployeeId
