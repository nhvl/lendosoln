ALTER PROCEDURE [dbo].[CreateNewBroker]
	(
		@LoginName LoginName,
		@PasswordHash varchar(50),
		@BrokerId GUID OUTPUT,
		@BranchId GUID OUTPUT
		
	)
AS
	declare @tranPoint sysname
	set @tranPoint = object_name(@@procId) + CAST(@@nestlevel as varchar(10))
	
	BEGIN TRANSACTION
	SAVE TRANSACTION @tranPoint
	set @BrokerId = newid();
	
	insert into broker  (BrokerId) values( @BrokerId )
	if 0!=@@error
	begin
		RAISERROR('Error in inserting into Broker table in CreateNewBroker sp', 16, 1);
		goto ErrorHandler;
	end
	
	
	set @BranchId = newid();
	
	insert into branch (BranchId, brokerId) 	values( @BranchId,@brokerId )
	if 0!=@@error
	begin
		RAISERROR('Error in inserting into Branch table in CreateNewBroker sp', 16, 1);
		goto ErrorHandler;
	end
	
	declare @employeeId GUID;
	set @employeeId = newid();
	
	insert into employee (EmployeeId,BranchId)	values(@EmployeeId,@BranchId)
	if 0!=@@error
	begin
		RAISERROR('Error in inserting into Employee table in CreateNewBroker sp', 16, 1);
		goto ErrorHandler;
	end
	
	declare @userId GUID;
	set @userId = newid();
	
	insert into all_user(UserId, LoginNm, PasswordHash, IsActive, Type)
	values( @userId, @LoginName, @PasswordHash, 1, 'B' )
	if 0!=@@error
	begin
		RAISERROR('Error in inserting into All_User table in CreateNewBroker sp', 16, 1);
		goto ErrorHandler;
	end
	
	declare @AdminPermission char(100);
	set @AdminPermission = '100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000';
	insert into broker_user(UserId, EmployeeId, Permissions, StartD )
	values( @userId, @employeeId, @AdminPermission, getDate() );
	if 0!=@@error
	begin
		RAISERROR('Error in inserting into Broker_User table in CreateNewBroker sp', 16, 1);
		goto ErrorHandler;
	end
/*
	declare @retVal int;
	declare @newTemplateId GUID;
	execute @retVal = CreateBlankLoanFile @employeeId, 1, @newTemplateId output
	if( 0!=@@error or 0!=@retVal )
	begin
		RAISERROR('Error in executing CreateBlankLoanFile sp in CreateNewBroker sp', 16, 1);
		goto ErrorHandler;
	end
	
	execute @retVal = DeclareLoanFileValid @newTemplateID, 1
	if( 0!=@@error or 0!=@retVal )
	begin
		RAISERROR('Error in executing DeclareLoanFileValid sp in CreateNewBroker sp', 16, 1);
		goto ErrorHandler;
	end
*/
	commit transaction;
	return 0;
ErrorHandler:
	rollback tran @tranPoint
	rollback transaction
	return -100;
