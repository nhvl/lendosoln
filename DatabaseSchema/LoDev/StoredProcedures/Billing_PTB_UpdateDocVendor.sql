-- =============================================
-- Author:		Budi Sulayman
-- Create date: January 14, 2016
-- Description:	Update Document Vendor on Billing Per Transaction
-- =============================================
ALTER PROCEDURE dbo.Billing_PTB_UpdateDocVendor 
	@sLId uniqueidentifier,
	@BrokerId uniqueidentifier,
	@BillingDate smalldatetime,
	@BillingType int,
	@DocumentVendor int
AS
BEGIN
    UPDATE BILLING_PER_TRANSACTION
    SET DocumentVendor = @DocumentVendor
	WHERE sLId = @sLId AND BrokerId = @BrokerId AND BillingType = @BillingType AND BillingDate = @BillingDate
END
