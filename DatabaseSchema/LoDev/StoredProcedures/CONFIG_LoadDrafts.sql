



-- =============================================
-- Author:		Antonio Valencia
-- Create date: 9/3/2010
-- Description:	Loads drafts or single draft if orgid is specified
-- =============================================
CREATE PROCEDURE [dbo].[CONFIG_LoadDrafts]
	@OrgId uniqueidentifier = null

AS
BEGIN
	SET NOCOUNT ON;


	SELECT OrgId, ConfigurationXmlContent
	FROM CONFIG_DRAFT
	WHERE OrgId = COALESCE(@OrgId, OrgId)


	
END




