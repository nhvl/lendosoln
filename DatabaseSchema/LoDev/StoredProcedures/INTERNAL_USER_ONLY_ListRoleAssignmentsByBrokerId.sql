

CREATE PROCEDURE [dbo].[INTERNAL_USER_ONLY_ListRoleAssignmentsByBrokerId] 
	@BrokerID uniqueidentifier
AS
	SELECT r.EmployeeId, r.RoleId
	FROM ROLE_ASSIGNMENT AS r JOIN EMPLOYEE e ON r.EmployeeId = e.EmployeeId
	                          JOIN Branch b ON b.BranchId = e.BranchId
	WHERE b.BrokerId = @BrokerID


