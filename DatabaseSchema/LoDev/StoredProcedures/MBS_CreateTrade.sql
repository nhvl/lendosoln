-- =============================================
-- Author:		Matthew Pham
-- Create date: 8/31/11
-- Description:	Create a new trade for MBS-TBA tracking
-- =============================================
CREATE PROCEDURE [dbo].[MBS_CreateTrade] 
	-- Add the parameters for the stored procedure here
	@TradeId uniqueidentifier,
	@BrokerId uniqueidentifier,
	@TradeNum int,
	@DealerInvestor varchar(100),
	@AssignedTo varchar(100),
	@TypeA tinyint,
	@TypeB tinyint,
	@Month tinyint,
	@DescriptionId uniqueidentifier,
	@Coupon decimal(5,3),
	@TradeDate smalldatetime = null,
	@NotificationDate smalldatetime = null,
	@SettlementDate smalldatetime = null,
	@Price decimal(12,6),
	@IsAllocatedLoansCalculated bit,
	@AllocatedLoans money,
	@PoolId bigint
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO MBS_TRADE(
		TradeId,
		BrokerId,
		TradeNum,
		DealerInvestor,
		AssignedTo,
		TypeA,
		TypeB,
		Month,
		DescriptionId,
		Coupon,
		TradeDate,
		NotificationDate,
		SettlementDate,
		Price,
		IsAllocatedLoansCalculated,
		AllocatedLoans,
		PoolId
	)
	VALUES (
		@TradeId,
		@BrokerId,
		@TradeNum,
		@DealerInvestor,
		@AssignedTo,
		@TypeA,
		@TypeB,
		@Month,
		@DescriptionId,
		@Coupon,
		@TradeDate,
		@NotificationDate,
		@SettlementDate,
		@Price,
		@IsAllocatedLoansCalculated,
		@AllocatedLoans,
		@PoolId
	)
	
	EXEC [MORTGAGE_POOL_Update_PoolTradeNumbers] @PoolId  
END
