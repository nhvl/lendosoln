ALTER PROCEDURE [dbo].[ListEmployeeAddressByBrokerId]
	@BrokerId uniqueidentifier
AS
	SELECT
		v.UserFirstNm ,
		v.UserLastNm ,
		v.Addr ,
		v.City ,
		v.State ,
		v.Zip ,
		v.Phone ,
		v.Fax ,
		v.Email ,
		v.EmployeeStartD ,
		v.EmployeeId ,
		e.CellPhone ,
		e.Pager,
		e.IsCellphoneForMultiFactorOnly
	FROM
		View_LendersOffice_Employee AS v JOIN Employee e ON v.EmployeeId = e.EmployeeId
	WHERE
		v.BrokerId = @BrokerId
