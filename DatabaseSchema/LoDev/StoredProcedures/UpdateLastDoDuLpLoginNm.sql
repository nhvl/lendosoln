CREATE PROCEDURE UpdateLastDoDuLpLoginNm 
	@UserId Guid,
	@DOLastLoginNm varchar(50) = NULL,
	@DULastLoginNm varchar(50) = NULL,
	@LPLastLoginNm varchar(50) = NULL
AS
BEGIN
	UPDATE Broker_User
	   SET DOLastLoginNm = COALESCE(@DOLastLoginNm, DOLastLoginNm),
           DULastLoginNm = COALESCE(@DULastLoginNm, DULastLoginNm),
		   LPLastLoginNm = COALESCE(@LPLastLoginNm, LPLastLoginNm)
    WHERE UserId = @UserId
END
