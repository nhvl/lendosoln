ALTER PROCEDURE [dbo].[ComplianceEase_UpdateSavedAuthentication]
	@UserId UniqueIdentifier,
	@ComplianceEaseUserName varchar(50),
	@ComplianceEasePassword varchar(50) = null,
	@EncryptedComplianceEasePassword varbinary(50)
AS
BEGIN
	UPDATE BROKER_USER
	SET ComplianceEaseUserName = @ComplianceEaseUserName,
		ComplianceEasePassword = COALESCE(@ComplianceEasePassword, ComplianceEasePassword),
		EncryptedComplianceEasePassword = @EncryptedComplianceEasePassword
	WHERE UserId = @UserId
END
