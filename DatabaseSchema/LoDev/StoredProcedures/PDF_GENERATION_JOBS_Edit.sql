-- =============================================
-- Author:		Michael Leinweaver
-- Create date: 9/21/2018
-- Description:	Edits a PDF generation job.
-- =============================================
CREATE PROCEDURE [dbo].[PDF_GENERATION_JOBS_Edit]
	@JobId int,
	@ResultFileDbKey uniqueidentifier
AS
BEGIN
	UPDATE PDF_GENERATION_JOBS
	SET ResultFileDbKey = @ResultFileDbKey
	WHERE JobId = @JobId
END
