-- =============================================
-- Author:		Eric Mallare
-- Create date: 9/14/2018
-- Description:	Edits a Credit Report Request job.
-- =============================================
ALTER PROCEDURE [dbo].[CREDIT_REPORT_REQUEST_JOBS_Edit]
	@JobId int,
	@CreditRequestDataFileDbGuidId uniqueidentifier = null,
	@CreditReportResponseFileDbGuidId uniqueidentifier = null,
	@AttemptCount int
AS
BEGIN
	UPDATE
		CREDIT_REPORT_REQUEST_JOBS
	SET
		CreditRequestDataFileDbGuidId=@CreditRequestDataFileDbGuidId,
		CreditReportResponseFileDbGuidId=@CreditReportResponseFileDbGuidId,
		AttemptCount=@AttemptCount
	WHERE
		JobId=@JobId
END
