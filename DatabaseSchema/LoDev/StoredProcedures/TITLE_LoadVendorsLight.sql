-- =============================================
-- Author:		Eric Mallare
-- Create date: 8/21/2017
-- Description:	Loads a light version of all TitleVendors
-- =============================================
ALTER PROCEDURE [dbo].[TITLE_LoadVendorsLight]
AS
BEGIN
	SELECT
		v.CompanyName, v.IsEnabled, v.IsTestVendor, v.VendorId, 
		v.IsQuickQuotingEnabled, v.IsDetailedQuotingEnabled, v.IsPolicyOrderingEnabled,
		pq.UsesAccountId as QuotingPlatformUsesAccountId, pq.RequiresAccountId as QuotingPlatformRequiresAccountId,
		pp.UsesAccountId as PolicyPlatformUsesAccountId, pp.RequiresAccountId as PolicyPlatformRequiresAccountId
	FROM
		TITLE_FRAMEWORK_VENDOR v LEFT JOIN
		TITLE_FRAMEWORK_PLATFORM pq ON v.QuotingPlatformId=pq.PlatformId LEFT JOIN
		TITLE_FRAMEWORK_PLATFORM pp ON v.PolicyOrderingPlatformId=pp.PlatformId
END
