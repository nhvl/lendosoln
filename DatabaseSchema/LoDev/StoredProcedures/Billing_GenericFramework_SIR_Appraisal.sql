CREATE PROCEDURE [dbo].[Billing_GenericFramework_SIR_Appraisal]
	@StartDate datetime,
	@EndDate datetime
AS
BEGIN
-- dd 2018-11-02 - For use in automate billing executable.
SELECT DISTINCT BrokerNm As LenderName, CustomerCode, sLNm As LoanNumber, gfb.LoanId --,DateUploaded, DocumentId
                                                FROM Generic_Framework_Billing gfb WITH (nolock)
                                                    JOIN [LendersOffice].[dbo].Generic_Framework_Vendor_Config gfvc WITH (nolock) ON gfvc.ProviderId = gfb.ProviderId
                                                    JOIN Broker WITH (nolock) ON Broker.BrokerId = gfb.BrokerId
                                                    JOIN Loan_File_Cache cache WITH (nolock) ON cache.sLId = gfb.LoanId
                                                WHERE DateUploaded >= @StartDate AND DateUploaded < @EndDate
                                                    AND Broker.SuiteType = 1
                                                    AND cache.sLoanFileT = 0
                                                    AND gfb.ProviderId= 'VEN0034'
                                                ORDER BY CustomerCode, LoanNumber
END