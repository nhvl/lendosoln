-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE internal_RetrieveUserIdByUserName
	@BrokerId UniqueIdentifier,
	@LoginNm varchar(100),
	@UserType varchar(1)
AS
BEGIN

	IF @UserType = 'B'
	BEGIN
		SELECT UserId FROM VIEW_ACTIVE_LO_USER WHERE BrokerId = @BrokerId AND LoginNm = @LoginNm
	END
	ELSE
	BEGIN
		SELECT UserId FROM VIEW_ACTIVE_PML_USER WHERE BrokerId = @BrokerId AND LoginNm = @LoginNm
	END
END
