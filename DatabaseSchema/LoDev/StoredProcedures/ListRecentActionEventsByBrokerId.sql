CREATE PROCEDURE ListRecentActionEventsByBrokerId
	@BrokerId Guid, @UserId Guid = NULL, @TypeDesc varchar(36) = NULL
AS
	SELECT a.UserId , a.UserFirstNm + ' ' + a.UserLastNm AS UserName , a.EventType AS EventType ,  a.NewActivationCap AS EventArg , MAX( a.EventDate ) AS EventDate , a.BrokerId AS BrokerId , b.BrokerNm AS BrokerName FROM Action_Event AS a LEFT JOIN Broker AS b ON b.BrokerId = a.BrokerId
	WHERE
		a.UserId = COALESCE( @UserId , a.UserId )
		AND
		a.EventType = COALESCE( @TypeDesc , a.EventType )
		AND
		a.BrokerId = @BrokerId
	GROUP BY
		a.UserId , a.BrokerId , a.UserFirstNm , a.UserLastNm , a.EventType , a.NewActivationCap , b.BrokerNm
	ORDER BY
		BrokerName , UserName
	if( 0 != @@error )
	begin
		RAISERROR('Error querying Action_Event table in ListRecentActionEventsByBrokerId sp', 16, 1);
		return -100;
	end
	return 0;
