CREATE PROCEDURE ListAuditTrailByLoanId 
	@LoanId Guid
AS
SELECT AuditTrailXmlContent, LogStartTime, FileDbKey FROM AUDIT_TRAIL_DAILY
WHERE LoanId = @LoanId
ORDER BY LogStartTime ASC
