CREATE PROCEDURE RetrieveLinkedLoanId
(
	@srcLoanId as uniqueidentifier
)
as
	select sLId1, sLId2, sLNm1, sLNm2
	from loan_link
	where sLId1 = @SrcLoanId or sLId2 = @SrcLoanId
