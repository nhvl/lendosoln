-- =============================================
-- Author:		Scott Kibler
-- Create date: September 3, 2013
-- Description:	Valid = 10 digits, and doesn't start with a 9 if usefhatotalproductionaccount
-- =============================================
CREATE PROCEDURE [dbo].[FhaLenderIDIsValidAndNonBlank] 
	 @proposedfhalenderid varchar(10),
	 @usefhatotalproductionaccount bit, -- controls if the first one has to be 9
     @isValidAndNonBlank bit OUTPUT --! OUT
AS
SELECT @isValidAndNonBlank = 0
if
  (@proposedfhalenderid like '[0-8][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]' and @usefhatotalproductionaccount=1 )-- 10 digits, first isn't 9)
OR(@proposedfhalenderid like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]' and @usefhatotalproductionaccount=0 )-- 10 digits, first isn't 9)
BEGIN
	SELECT @isValidAndNonBlank = 1
END

    
