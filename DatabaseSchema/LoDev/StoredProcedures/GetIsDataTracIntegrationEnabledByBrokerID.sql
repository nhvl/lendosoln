


CREATE PROCEDURE [dbo].[GetIsDataTracIntegrationEnabledByBrokerID]
	@BrokerID uniqueidentifier

AS
	select IsDataTracIntegrationEnabled from Broker with(nolock) where BrokerID = @BrokerID
