CREATE PROCEDURE [dbo].[GetBrokerIdAndEmpIdByLoginNm] 
	@LoginNm varchar(50)
AS
BEGIN
	SELECT 
		br.brokerid BrId, e.employeeid EmpId, rq.QueryName, rq.XmlContent, rq.IsPublished, rq.NamePublishedAs, rq.ShowPosition
	FROM 
		Employee AS e  WITH (NOLOCK) 
		JOIN Branch AS r WITH (NOLOCK) ON r.BranchId = e.BranchId
		JOIN Broker as br with (nolock) on r.brokerid = br.brokerid
		JOIN broker_user as bu with (nolock) on e.employeeid = bu.employeeid
		JOIN all_user as a with (nolock) on a.userid = bu.userid
		JOIN Report_Query as rq with (nolock) on rq.employeeid = e.employeeid AND rq.brokerid = br.brokerid
	WHERE 
		a.loginNm = @LoginNm
END		