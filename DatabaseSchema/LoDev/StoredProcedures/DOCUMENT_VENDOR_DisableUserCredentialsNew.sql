-- =============================================
-- Author:		paoloa
-- Create date: 7/29/2013
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[DOCUMENT_VENDOR_DisableUserCredentialsNew]
	-- Add the parameters for the stored procedure here
	@UserId uniqueidentifier,
	@VendorId uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	DELETE FROM DOCUMENT_VENDOR_USER_ENABLED_CREDENTIALS
	WHERE UserId = @UserId AND VendorId = @VendorId
END
