-- =============================================
-- Author:		Eric Mallare
-- Create date: 9/5/2018
-- Description:	Retrieves an MI Framework Request job entry
-- =============================================
ALTER PROCEDURE [dbo].[MI_FRAMEWORK_REQUEST_JOBS_Retrieve]
	@JobId int = null,
	@PublicJobId uniqueidentifier = null
AS
BEGIN
	SELECT
		j.JobId,
		j.ProcessingStatus,
		j.NextRunDate,
		j.PublicJobId,
		j.DateCreated,
		j.ExpirationDate,
		j.LastRunStartDate,
		j.MaxExceptionCount,
		j.ExceptionCount,
		j.DateCompleted,
		j.CorrelationId,
		mi.ResultsFileDbId,
		mi.LoanId,		-- 11/2/18 - je - For backwards compatibility we get LoanId from mi table. We should be able to change this in the future if necessary.
		mi.ApplicationId,
		mi.BrokerId,	-- 11/2/18 - je - BrokerId and UserId are also retrieved from mi table for the same reasons as LoanId.
		mi.UserId,
		mi.UserType,
		mi.BranchId,
		mi.VendorId,
		mi.PremiumType,
		mi.Refundability,
		mi.UFMIPFinanced,
		mi.MasterPolicyNumber,
		mi.CoveragePercent,
		mi.RenewalType,
		mi.PremiumAtClosing,
		mi.IsRelocationLoan,
		mi.IsQuote,
		mi.QuoteNumber,
		mi.OriginalQuoteNumber,
		mi.PollingIntervalInSeconds,
		mi.DelegationType,
		mi.RequestType,
		mi.UpfrontPremiumMismoValue
	FROM
		MI_FRAMEWORK_REQUEST_JOBS mi JOIN
		BACKGROUND_JOBS j ON mi.JobId=j.JobId
	WHERE
		mi.JobId=@JobId OR
		j.PublicJobId=@PublicJobId
END