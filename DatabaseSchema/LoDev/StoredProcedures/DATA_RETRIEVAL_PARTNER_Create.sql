-- =============================================
-- Author:		Brian Beery
-- Create date: 09/20/2013
-- Description:	Add a data retrieval framework partner
-- =============================================
CREATE PROCEDURE [dbo].[DATA_RETRIEVAL_PARTNER_Create]
	@Name varchar(255),
	@ExportPath varchar(1024)
	
AS
BEGIN

	INSERT INTO DATA_RETRIEVAL_PARTNER(Name, ExportPath)
	VALUES (@Name, @ExportPath)

END