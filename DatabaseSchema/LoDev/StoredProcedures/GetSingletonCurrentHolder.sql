-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE GetSingletonCurrentHolder 
AS
BEGIN
	SELECT HolderId 
	FROM SINGLETON_TRACKER
	WHERE ObjId = 'LPE_DATA_ACCESS_TICKET'  and  
		IsExisting = 1 and  ExpirationTime >= getdate()
END
