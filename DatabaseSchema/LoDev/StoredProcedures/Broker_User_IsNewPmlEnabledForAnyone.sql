-- =============================================
-- Author:		Antonio Valencia
-- Create date: 12/13/2013
-- Description:	Check If PML Is Enabled for Any Employee
-- =============================================
CREATE PROCEDURE dbo.Broker_User_IsNewPmlEnabledForAnyone 
	-- Add the parameters for the stored procedure here
	@BrokerId uniqueidentifier
AS
BEGIN
	
	SELECT count(*) FROM BROKER_USER bu join Employee e on bu.employeeid = e.employeeid  join branch br on br.branchid = e.branchid Where IsNewPmlUIEnabled = 1 and BrokerId = @BrokerId and IsActive = 1

END
