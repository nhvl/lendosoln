-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE CC_TEMPLATE_RetrieveIdByName
	@BrokerID UniqueIdentifier,
	@cCcTemplateNm varchar(36)
AS
BEGIN
	SELECT cCcTemplateId
	FROM CC_TEMPLATE
	WHERE BrokerId = @BrokerID
      AND cCCTemplateNm = @cCcTemplateNm
END
