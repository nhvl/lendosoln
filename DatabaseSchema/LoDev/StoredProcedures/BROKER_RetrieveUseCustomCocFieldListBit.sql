ALTER PROCEDURE [dbo].[BROKER_RetrieveUseCustomCocFieldListBit]
	@BrokerId uniqueidentifier
AS
BEGIN
	SELECT UseCustomCocFieldList
	FROM Broker 
	WHERE BrokerId = @BrokerId
END