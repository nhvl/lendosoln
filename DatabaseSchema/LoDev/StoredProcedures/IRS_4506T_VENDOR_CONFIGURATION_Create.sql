-- =============================================
-- Author:		David Dao
-- Create date: 8/2/2013
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[IRS_4506T_VENDOR_CONFIGURATION_Create]
	@VendorName varchar(200),
	@IsValid bit,
	@ExportPath varchar(1024),
	@IsUseAccountId bit,
    @EnableConnectionTest bit,
    @CommunicationModel tinyint
AS
BEGIN

	INSERT INTO IRS_4506T_VENDOR_CONFIGURATION(VendorName, IsValid, ExportPath, IsUseAccountId, EnableConnectionTest, CommunicationModel)
	VALUES (@VendorName, @IsValid, @ExportPath, @IsUseAccountId, @EnableConnectionTest, @CommunicationModel)

END
