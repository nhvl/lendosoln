

  CREATE PROCEDURE [dbo].[PmlGetNumberBadPwAllowed]
	@BrokerPmlSiteId uniqueidentifier = '00000000-0000-0000-0000-000000000000'
AS
	SELECT NumberBadPasswordsAllowed
	FROM Broker
	WHERE BrokerPmlSiteId = @BrokerPmlSiteId
