-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE SignatureTemplate_DeleteById
	@TemplateId UniqueIdentifier,
	@BrokerId UniqueIdentifier,
	@OwnerUserId UniqueIdentifier
AS
BEGIN
	DELETE FROM LO_SIGNATURE_TEMPLATE
	WHERE TemplateId = @TemplateId AND BrokerId = @BrokerId AND OwnerUserId = @OwnerUserId
END
