ALTER PROCEDURE [dbo].[RetrieveBranchByID] 
	@BranchID uniqueidentifier , @BrokerID uniqueidentifier
AS
	SELECT
		BrokerID AS BrokerID,
		BranchNm AS Name,
		BranchAddr AS Address,
		BranchCity AS City,
		BranchState AS State,
		BranchZip AS Zipcode,
		BranchPhone AS Phone,
		BranchFax AS Fax,
		BranchLpePriceGroupIdDefault,
		DisplayBranchNameInLoanNotificationEmails,
		BranchCode,
		IsAllowLOEditLoanUntilUnderwriting,
		UseBranchInfoForLoans,
		NmlsIdentifier,
		LicenseXmlContent,
		DisplayNm,
		DisplayNmLckd,
		IsBranchTPO,
		BranchChannelT,
		DocMagicUserName,
		DocMagicPassword,
		DocMagicCustomerId,
		DisclosureDocUserName,
		DisclosureDocPassword,
		DisclosureDocCustomerId,
		ConsumerPortalId,
		FhaLenderId,
		IsFhaLenderIdModified,
		Division,
		PopulateFhaAddendumLines,
		CustomPricingPolicyField1,
		CustomPricingPolicyField2,
		CustomPricingPolicyField3,
		CustomPricingPolicyField4,
		CustomPricingPolicyField5,
		BranchIdNumber,
		RetailTpoLandingPageId,
		LegalEntityIdentifier,
		IsLegalEntityIdentifierModified,
		[Status]
	FROM
		Branch
	WHERE
		BranchID = @BranchID
		AND
		BrokerID = @BrokerID
	if( 0!=@@error)
	begin
		RAISERROR('Error in the select statement in RetrieveBranchByID sp', 16, 1);
		return -100;
	end
	return 0;