-- =============================================
-- Author:		David Dao
-- Create date: Aug 9, 2013
-- Description:	
-- =============================================
ALTER PROCEDURE [dbo].[IRS_4506T_VENDOR_EMPLOYEE_INFO_Retrieve] 
    @UserId UniqueIdentifier,
    @VendorId UniqueIdentifier
AS
BEGIN

    SELECT UserId, VendorId, AccountId, UserName, Password, EncryptionKeyId
    FROM IRS_4506T_VENDOR_EMPLOYEE_INFO
    WHERE UserId = @UserId AND VendorId = @VendorId
END
