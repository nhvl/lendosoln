-- =============================================
-- Author:		Michael Leinweaver
-- Create date: 10/27/2015
-- Description:	Removes threshold monitoring for the queue specified by QueueName.
-- =============================================
ALTER PROCEDURE [dbo].[QueueMonitorThreshold_DeleteAutoEscalationMonitoringFromQueue]
	@ID int
AS
BEGIN
	DELETE FROM QUEUE_MONITOR_THRESHOLD
	WHERE ID = @ID
END
