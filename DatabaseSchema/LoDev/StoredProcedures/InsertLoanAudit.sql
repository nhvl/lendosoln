CREATE PROCEDURE InsertLoanAudit 
	@LoanId Guid,
	@AuditTrailXmlContent Text,
	@FileDBKey varchar(25)
AS
INSERT INTO AUDIT_TRAIL_DAILY (LoanId, AuditTrailXmlContent, FileDBKey)
VALUES (@LoanId, @AuditTrailXmlContent, @FileDBKey)
