create proc [dbo].MBS_RetrieveDescriptionById
	@DescriptionId uniqueidentifier
as
select *
from MBS_DESCRIPTION
where DescriptionId = @DescriptionId