CREATE PROCEDURE [dbo].[UCD_DELIVERY_DeleteManual]
	@UcdDeliveryId int
AS
BEGIN
	DELETE FROM UCD_DELIVERY
	WHERE UcdDeliveryId = @UcdDeliveryId
		AND UcdDeliveryType = 0 -- Ensure only manual deliveries are deleted.
END