-- =============================================
-- Author:		Michael Leinweaver
-- Create date: 9/8/2017
-- Description:	Adds a new edoc classification.
-- =============================================
ALTER PROCEDURE [dbo].[EDOCS_OCR_SYSTEM_DOC_TYPES_AddNew]
	@Name varchar(100)
AS
BEGIN
	BEGIN TRANSACTION
    
    DECLARE @Id int
    
    SELECT @Id = MAX(Id) + 1
    FROM EDOCS_OCR_SYSTEM_DOC_TYPES
    
    INSERT INTO EDOCS_OCR_SYSTEM_DOC_TYPES(Id, Name, Enabled)
    VALUES(@Id, @Name, 1)
    
    IF (@@error != 0)
	BEGIN
		ROLLBACK TRANSACTION
		RAISERROR('Error in EDOCS_OCR_SYSTEM_DOC_TYPES_AddNew sp', 16, 1);
	END
	ELSE
	BEGIN
		COMMIT TRANSACTION
		SELECT @Id as "Id"
	END
END
