
CREATE   PROCEDURE [dbo].[ListEmployeePermissionByBrokerId]
	@BrokerId uniqueidentifier
AS
	SELECT
		v.EmployeeId ,
		v.Permissions
	FROM
		View_Active_LO_User AS v
	WHERE
		v.BrokerId = @BrokerId
	UNION
	SELECT
		v.EmployeeId ,
		v.Permissions
	FROM
		View_Disabled_LO_User AS v
	WHERE
		v.BrokerId = @BrokerId

UNION
	SELECT
		v.EmployeeId ,
		v.Permissions
	FROM
		View_Active_Pml_User AS v
	WHERE
		v.BrokerId = @BrokerId


UNION
	SELECT
		v.EmployeeId ,
		v.Permissions
	FROM
		View_Disabled_Pml_User AS v
	WHERE
		v.BrokerId = @BrokerId


