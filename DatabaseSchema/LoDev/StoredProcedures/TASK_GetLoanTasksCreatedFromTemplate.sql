CREATE Procedure [dbo].[TASK_GetLoanTasksCreatedFromTemplate]
	@brokerId uniqueidentifier,
	@loanId uniqueidentifier,	
	@templateId int,
	@recordId uniqueidentifier
		
AS
	select * from TASK_x_TASK_TRIGGER_TEMPLATE where BrokerId=@brokerId and LoanId=@loanId and TaskTemplateId=@templateId and RecordId=@recordId
