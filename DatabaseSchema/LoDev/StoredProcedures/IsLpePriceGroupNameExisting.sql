CREATE PROCEDURE IsLpePriceGroupNameExisting 
	@BrokerId Guid,
	@LpePriceGroupName varchar(100),
	@LpePriceGroupId Guid
AS
SELECT top 1 LpePriceGroupName  FROM LPE_Price_Group with(updlock,rowlock)
WHERE BrokerId = @BrokerId AND LpePriceGroupName = @LpePriceGroupName AND LpePriceGroupId <> @LpePriceGroupId
