-- =============================================
-- Author:		Antonio Valencia
-- Create date: 7/25/2013
-- Description:	Updates Primary Trade Number on mortgage pool
-- =============================================
CREATE PROCEDURE [dbo].[MORTGAGE_POOL_Update_PoolTradeNumbers]
	@PoolId bigint = null 
AS
BEGIN
	IF @PoolId is not null
	BEGIN
		DECLARE @TradeNumberList varchar(1000)
		SELECT @TradeNumberList = SUBSTRING( COALESCE((select ' / ' + CAST(TradeNum as varchar(10))  from mbs_trade where poolid = @PoolId order by tradenum asc for xml path(''), ELEMENTS),''), 4,500) 
	
		
		UPDATE TOP(1) MORTGAGE_POOL
		SET PoolTradeNumbers = @TradeNumberList
		WHERE PoolId = @PoolId
	END	
END	
