ALTER PROCEDURE dbo.[UpdateComplianceEaseData]
	@sLId uniqueidentifier,
	@sFileVersion int,
	@sComplianceEaseErrorMessage varchar(max),
	@sComplianceEaseChecksum varchar(100),
	@sComplianceEaseStatus varchar(20),
	@sComplianceEaseId varchar(10) = null
as
UPDATE LOAN_FILE_D
SET sComplianceEaseErrorMessage = @sComplianceEaseErrorMessage,
	sComplianceEaseChecksum = @sComplianceEaseChecksum,
	sComplianceEaseStatus = @sComplianceEaseStatus
WHERE sLId = @sLId

IF @sComplianceEaseId IS NOT NULL
BEGIN
UPDATE LOAN_FILE_A
SET sComplianceEaseId = @sComplianceEaseId
WHERE sLId = @sLId
END