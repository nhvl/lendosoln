-- =============================================
-- Author:		Antonio Valencia
-- Create date: 6/2/2013
-- Description:	Retrieves a consumer portal user
-- =============================================
ALTER PROCEDURE [dbo].[CONSUMER_PORTAL_User_Retrieve] 
	@Id bigint = null,
	@Email varchar(80) = null,
	@BrokerId uniqueidentifier
AS
BEGIN
	IF @Id is null and  @Email is null
	BEGIN
		RAISERROR('Not all parameters can be null', 16, 1); 
	END
	
	IF @Id is not null
	BEGIN
		SELECT Id, BrokerId, TempPasswordSetD, CreateD, FirstName, LastName, Phone, Email, PasswordHash, PasswordSalt, LoginAttempts, LastLoginD, PasswordResetCode, PasswordResetCodeRequestD, DisclosureAcceptedD, IsTemporaryPassword, IsNotifyOnStatusChanges ,IsNotifyOnNewDocRequests, NotificationEmail, SignatureFontType,SignatureName,ReferralSource, SecurityPin, EncryptedSecurityPin, EncryptionKeyId
		FROM CONSUMER_PORTAL_USER
		WHERE id = @id 	and BRokeriD = @Brokerid
		RETURN
	END
	
	SELECT Id, BrokerId, TempPasswordSetD, CreateD, FirstName, LastName, Phone, Email, PasswordHash, PasswordSalt, LoginAttempts, LastLoginD, PasswordResetCode, PasswordResetCodeRequestD, DisclosureAcceptedD, IsTemporaryPassword, IsNotifyOnStatusChanges ,IsNotifyOnNewDocRequests, NotificationEmail, SignatureFontType,SignatureName, ReferralSource, SecurityPin, EncryptedSecurityPin, EncryptionKeyId
		FROM CONSUMER_PORTAL_USER
		WHERE @BrokerId = BrokerId and @Email = Email
END
