-- =============================================
-- Author:		Antonio Valencia
-- Create date: 10/8/2012
-- Description:	Saves Page Info Xml
-- =============================================
CREATE PROCEDURE [dbo].[EDOCS_UpdatePageInfo]
	
	@DocumentId uniqueidentifier, 
	@PageInfoXml varchar(max),
	@RevisionKey uniqueidentifier
AS
BEGIN
	update  edocs_document 
	set PdfPageInfo  = @PageInfoXml   
	where documentid = @documentid and fileDbKey_CurrentRevision = @RevisionKey
	
	select @@rowcount
END
