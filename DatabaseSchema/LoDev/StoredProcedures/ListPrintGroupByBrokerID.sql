CREATE PROCEDURE ListPrintGroupByBrokerID 
	@BrokerID Guid
AS
	SELECT GroupID, GroupName 
	FROM Print_Group
	WHERE OwnerID = @BrokerID
	ORDER BY GroupName
