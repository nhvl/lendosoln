ALTER PROCEDURE [dbo].[EDOCS_RetrieveAllLostDocuments] 
	@BrokerId uniqueidentifier = null,
	@DocumentSource int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;



	SELECT DocumentId, sLId, e.BrokerId, e.DocTypeId, FaxNumber, FileDBKey_OriginalDocument, Description, NumPages, CreatedDate, BrokerNm, DocTypeName, DocumentSource, IsESigned
	FROM EDOCS_LOST_DOCUMENT as e 
			LEFT OUTER JOIN broker as b on b.BrokerId = e.BrokerId 
			LEFT OUTER JOIN EDOCS_DOCUMENT_TYPE as D ON d.DocTypeId = e.DocTypeId
	WHERE (@BrokerId is null AND e.brokerid is null) OR (@brokerid = e.BrokerId AND e.brokerId is not null) AND @DocumentSOurce = DocumentSource  
	
END
