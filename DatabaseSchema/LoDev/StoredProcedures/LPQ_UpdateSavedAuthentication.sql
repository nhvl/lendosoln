-- =============================================
-- Author:		Scott Kibler
-- Create date: Friday 4/6/2012
-- Description:	Update LPQ LoginInfo for user with this userid.
-- =============================================
ALTER PROCEDURE [dbo].[LPQ_UpdateSavedAuthentication]
	@UserId UniqueIdentifier,
	@LPQUsername varchar(50),
	@LPQPassword varchar(50) = null,
	@EncryptedLPQPassword varbinary(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	UPDATE BROKER_USER
	SET
		LPQUsername = @LPQUsername,
		LPQPassword = COALESCE(@LPQPassword, LPQPassword),
		EncryptedLPQPassword = @EncryptedLPQPassword
	WHERE UserId= @UserId
END
