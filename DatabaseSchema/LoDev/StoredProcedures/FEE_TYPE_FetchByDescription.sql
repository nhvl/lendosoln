CREATE PROCEDURE [dbo].[FEE_TYPE_FetchByDescription]
	@BrokerId		uniqueidentifier,
	@GfeLineNumber	varchar(5),
	@Description	varchar(max)
AS
	SELECT *
	FROM FEE_TYPE
	WHERE BrokerId = @BrokerId
	AND GfeLineNumber = @GfeLineNumber
	AND [Description] = @Description