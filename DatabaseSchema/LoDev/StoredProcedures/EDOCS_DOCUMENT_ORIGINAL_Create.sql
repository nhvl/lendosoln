-- =============================================
-- Author:		David Dao
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE dbo.EDOCS_DOCUMENT_ORIGINAL_Create 
    @BrokerId UniqueIdentifier,
    @DocumentId UniqueIdentifier,
	@SourceT int,
	@OriginalRequestJson varchar(max)
AS
BEGIN
    INSERT INTO EDOCS_DOCUMENT_ORIGINAL(BrokerId, DocumentId, MetadataJsonContent, NumPages, PdfSizeInBytes, PngSizeInBytes, SourceT, OriginalRequestJson)
    VALUES (@BrokerId, @DocumentId, '', 0, 0, 0, @SourceT, @OriginalRequestJson)

END
