
CREATE procedure [dbo].[GetPriceGroupDefaultsByBrokerId]
@BrokerId uniqueidentifier
as
begin

-- sometimes the BrokerLpePriceGroupIdDefault is null, but we still want 
-- the other broker entries, SO use LEFT JOIN
select COALESCE(LenderPaidOriginatorCompensationOptionT, 1) as LenderPaidOriginatorCompensationOptionT
	, b.DefaultLockPolicyID
	, b.DisplayPmlFeeIn100Format
	, b.IsRoundUpLpeFee
	, b.RoundUpLpeFeeToInterval
	, b.IsAlwaysDisplayExactParRateOption
  from broker b
  LEFT JOIN lpe_price_group pg
  on  b.BrokerLpePriceGroupIdDefault = pg.LpePriceGroupId 
where b.brokerid=@brokerid



end