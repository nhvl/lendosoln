-- =============================================
-- Author:		Eric Mallare
-- Create date: 5/2/2017
-- Description:	Creates a vendor an a linked transmission info
-- =============================================
CREATE PROCEDURE [dbo].[VERIFICATION_VENDOR_CreateVendor]
	@CompanyName varchar(150),
	@IsEnabled bit,
	@IsTestVendor bit,
	@CanPayWithCreditCard bit,
	@DuValidationServiceProviderId int = null,
	@PlatformId int = null,
	@IsOverridingPlatform bit,
	@MclCraId char(2),
	@TargetUrl varchar(200),
	@TransmissionAuthenticationT int,
	@TransmissionAssociationT int = null,
	@EncryptionKeyId uniqueidentifier,
	@RequestCertLocation varchar(200) = null,
	@ResponseCertId uniqueidentifier = null,
	@RequestCredentialName varchar(50) = null,
	@RequestCredentialPassword varbinary(max) = null,
	@ResponseCredentialName varchar(50) = null,
	@ResponseCredentialPassword varbinary(max) = null,
	@TransmissionId int OUTPUT,
	@VendorId int OUTPUT
AS
BEGIN TRANSACTION
	INSERT INTO VERIFICATION_TRANSMISSION
		(TransmissionAssociationT, TargetUrl, TransmissionAuthenticationT, EncryptionKeyId, RequestCertLocation, ResponseCertId,
		 RequestCredentialName, RequestCredentialPassword, ResponseCredentialName, ResponseCredentialPassword)
	VALUES
		(1, @TargetUrl, @TransmissionAuthenticationT, @EncryptionKeyId, @RequestCertLocation, @ResponseCertId, 
		 @RequestCredentialName, @RequestCredentialPassword, @ResponseCredentialName, @ResponseCredentialPassword)
	
	IF @@error!= 0 GOTO HANDLE_ERROR
	SET @TransmissionId=SCOPE_IDENTITY();
	
	INSERT INTO VERIFICATION_VENDOR
		(CompanyName, IsEnabled, IsTestVendor, CanPayWithCreditCard, DuValidationServiceProviderId, PlatformId, 
		 IsOverridingPlatform, OverridingTransmissionId, MclCraId)
	VALUES
		(@CompanyName, @IsEnabled, @IsTestVendor, @CanPayWithCreditCard, @DuValidationServiceProviderId, @PlatformId,
		 @IsOverridingPlatform, @TransmissionId, @MclCraId)
	
	IF @@error!= 0 GOTO HANDLE_ERROR
	SET @VendorId=SCOPE_IDENTITY();
COMMIT TRANSACTION

RETURN;

	HANDLE_ERROR:
		ROLLBACK TRANSACTION
		RAISERROR('Error in VERIFICATION_VENDOR_CreateVendor sp', 16, 1);
		RETURN;
