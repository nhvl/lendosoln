ALTER   PROCEDURE [dbo].[UpdatePasswordResetFailureCount]    
 @LoginNm varchar(36),    
 @Type char(1),    
 @BrokerPmlSiteId uniqueidentifier = '00000000-0000-0000-0000-000000000000',  
 @Email varchar(80)  
AS   
  
 -- Bump the login account  
 UPDATE All_User    
 SET   
 PasswordResetFailureCount = PasswordResetFailureCount + 1    
 WHERE   
 LoginNm = @LoginNm   
 AND Type = @Type   
 AND BrokerPmlSiteId = @BrokerPmlSiteId   
   
 -- Bump all accounts that use this email address.  
UPDATE   
 au  
SET  
    au.PasswordResetFailureCount = au.PasswordResetFailureCount + 1  
FROM  
    ALL_USER AS au  
    JOIN Employee AS emp  
        ON au.UserId = emp.EmployeeUserId  
WHERE  
    emp.Email = @Email  
    AND au.Type = @Type AND au.BrokerPmlSiteId = @BrokerPmlSiteId   
   
 -- Our application only tries to get affected accounts for lock them.
 -- In case application doesn't lock some affected accounts at this time, it can do "at next time" if that user fail for log-in again.
 -- Therefore it is OK if don't return 100% affected accounts.
 SELECT   
 br.BrokerId,  
 au.UserId,  
 emp.EmployeeId,
 emp.UserFirstNm,
 emp.UserLastNm,
 au.LoginNm,  
 au.PasswordResetFailureCount,  
 au.LoginBlockExpirationD  
 FROM   
 ALL_USER au   
 JOIN EMPLOYEE emp WITH(ROWLOCK, READPAST) on emp.EmployeeUserId = au.UserId  
 JOIN BRANCH br WITH(ROWLOCK, READPAST) on br.BranchId = emp.BranchId  
 where emp.email = @Email or au.LoginNm = @LoginNm  
 AND Type = @Type AND BrokerPmlSiteId = @BrokerPmlSiteId   
   