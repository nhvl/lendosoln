-- =============================================
-- Author:		Paolo Arrastia
-- Create date: 9 Aug 2012
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[MortgagePoolDelete] 
	-- Add the parameters for the stored procedure here
	@PoolId bigint
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE MORTGAGE_POOL
	SET IsEnabled = 0
	WHERE PoolId = @PoolId 
END
