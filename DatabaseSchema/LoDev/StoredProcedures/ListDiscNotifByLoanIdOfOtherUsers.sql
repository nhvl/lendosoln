
CREATE PROCEDURE [dbo].[ListDiscNotifByLoanIdOfOtherUsers]
	@ThisUserID uniqueidentifier,
	@LoanId uniqueidentifier
AS
	SELECT  NotifId, DiscCreatorLoginNm, DiscStatus, DiscStatusDate, DiscPriority, DiscSubject, DiscDueDate, NotifAlertDate, DiscRefObjNm1, DiscRefObjNm2, ISNULL(NotifAlertDate, convert(smalldatetime, '2079-01-01')) AS SortAlertDate,
		 DiscPriority, dbo.GetReminderPriorityDescription(DiscPriority) AS PriorityDescription, DiscRefObjId, DiscCreatedDate,NotifIsRead,disc.DiscLogId,NotifIsValid
/*
             FROM discussion_notification notif join discussion_log conv on notif.DiscLogid = conv.DiscLogId
             WHERE DiscRefObjId = @LoanId 
and
			conv.DiscLogId not in
			(
				select c.DiscLogId
				from discussion_log c join discussion_notification n on c.DiscLogId = n.DiscLogId
				where c.DiscRefObjId = @LoanId and n.notifreceiveruserid = @thisuserid and n.NotifIsValid = 1
				
			)
and
			notif.notifid in
			(
				select top 1 ntop.notifid
				from discussion_notification ntop
				where ntop.disclogid = conv.disclogid
				
			)
             ORDER BY SortAlertDate ASC, DiscCreatedDate DESC
*/
FROM discussion_log disc left join discussion_notification notif on disc.disclogid = notif.disclogid and notif.notifreceiveruserid = @ThisUserId
where disc.discrefobjid = @loanid and disc.IsValid = 1
and disc.DiscLogId not in
			(
				select c.DiscLogId
				from discussion_log c join discussion_notification n on c.DiscLogId = n.DiscLogId
				where c.DiscRefObjId = @LoanId and n.notifreceiveruserid = @thisuserid and n.NotifIsValid = 1
				
			)
order by disc.discrefobjnm1 asc, disc.discpriority asc

