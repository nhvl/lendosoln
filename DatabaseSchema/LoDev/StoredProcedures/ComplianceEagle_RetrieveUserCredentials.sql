-- =============================================
-- Author:		Timothy Jewell
-- Create date: 7/31/2014
-- Description:	Loads User-level ComplianceEagle Credentials
-- =============================================
ALTER PROCEDURE [dbo].[ComplianceEagle_RetrieveUserCredentials]
	@BrokerId uniqueidentifier,
	@UserId uniqueidentifier
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @ComplianceEagleCompanyID varchar(50)
	
	SELECT @ComplianceEagleCompanyID = ComplianceEagleCompanyID
	FROM BROKER
	WHERE BrokerId = @BrokerId

	SELECT
		ComplianceEagleUserName,
		ComplianceEaglePassword,
		EncryptionKeyId,
		@ComplianceEagleCompanyID AS ComplianceEagleCompanyID
	FROM BROKER_USER
	WHERE UserId = @UserId
END
