-- =============================================
-- Author:		Justin Lara
-- Create date: 4/17/2017
-- Description:	Adds a county value to a specified agent.
-- =============================================
ALTER PROCEDURE [dbo].[PopulateAgentCountyForMigrator]
	@BrokerId uniqueidentifier,
	@AgentId uniqueidentifier,
	@AgentCounty varchar(36)
AS
BEGIN
	UPDATE AGENT
	SET AgentCounty = @AgentCounty
	WHERE BrokerId = @BrokerId AND AgentId = @AgentId
END
