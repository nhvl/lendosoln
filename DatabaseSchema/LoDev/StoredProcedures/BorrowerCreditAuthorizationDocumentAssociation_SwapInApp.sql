ALTER PROCEDURE [dbo].[BorrowerCreditAuthorizationDocumentAssociation_SwapInApp]
	@AppId uniqueidentifier
AS
BEGIN
	UPDATE BORROWER_CREDIT_AUTHORIZATION_DOCUMENT_ASSOCIATION
	SET IsCoborrower = ~IsCoborrower
	WHERE AppId = @AppId
END
