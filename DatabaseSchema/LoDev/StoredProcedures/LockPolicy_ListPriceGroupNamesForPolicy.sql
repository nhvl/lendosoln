-- =============================================
-- Author:		paoloa
-- Create date: 5/6/2013
-- Description:	
-- =============================================
CREATE PROCEDURE dbo.LockPolicy_ListPriceGroupNamesForPolicy
	-- Add the parameters for the stored procedure here
	@PolicyId uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT LpePriceGroupName
	FROM LPE_PRICE_GROUP
	WHERE LockPolicyId = @PolicyId
	
END
