CREATE PROCEDURE [dbo].[DYNAMIC_CLOSING_COST_FEE_HISTORY_Update]
	@BrokerId uniqueidentifier,
	@FeeHistoryId int,
	@HudLine int
AS
BEGIN
	UPDATE DYNAMIC_CLOSING_COST_FEE_HISTORY
	SET HudLine = @HudLine
	WHERE BrokerId = @BrokerId
	AND FeeHistoryId = @FeeHistoryId
END