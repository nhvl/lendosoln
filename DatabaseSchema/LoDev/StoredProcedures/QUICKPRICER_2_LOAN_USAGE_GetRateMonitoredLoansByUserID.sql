-- =============================================
-- Author:		Scott Kibler
-- Create date: 9/15/2014
-- Description:	
--   Gets the "in use" rate monitored loans for a particular user.
-- =============================================
CREATE PROCEDURE [dbo].[QUICKPRICER_2_LOAN_USAGE_GetRateMonitoredLoansByUserID]
	@BrokerID UniqueIdentifier,
	@UserIdOfUsingUser UniqueIdentifier
AS
BEGIN
	SELECT 
		LoanID,
		RateMonitorName
		, sBranchChannelT
        , sCorrespondentProcessT
  FROM [dbo].[QUICKPRICER_2_LOAN_USAGE] JOIN dbo.Loan_File_Cache_2 as b on (LoanID = b.sLId) JOIN dbo.Loan_File_Cache_3 as c on (LoanID = c.sLId)
	WHERE 
			BrokerID = @BrokerID
		AND IsInUse = 1
		AND UserIDofUsingUser = @UserIDofUsingUser
		AND RateMonitorName IS NOT NULL
END
