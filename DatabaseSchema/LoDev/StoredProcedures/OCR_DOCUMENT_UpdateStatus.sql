ALTER PROCEDURE [dbo].[OCR_DOCUMENT_UpdateStatus]
	@Status int, 
	@StatusUpdatedOn datetime,
	@BrokerId uniqueidentifier,
	@TransactionId uniqueidentifier
AS

UPDATE top(1) ocr_document
set Status = @status,
	StatusUpdatedOn =  @statusupdatedon
where TransactionId = @TransactionId AND BrokerId = @BrokerID
