-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[XSLT_EXPORT_RESULT_STATUS_UpdateResponse] 
	@ReportId varchar(13),
	@UserId UniqueIdentifier,
	@Status int,
	@ExecutionInMs int
AS
BEGIN

	UPDATE XSLT_EXPORT_RESULT_STATUS
	  SET Status = @Status,
		ExecutionInMs = @ExecutionInMs
	WHERE ReportId = @ReportId AND UserId = @UserId
END
