-- =============================================
-- Author:		Antonio Valencia
-- Create date: 6/2/2013
-- Description:	Updates a consumer portal user.
-- =============================================
ALTER PROCEDURE [dbo].[CONSUMER_PORTAL_User_Update]
	@Id bigint, 
	@BrokerId uniqueidentifier,
	@CreateD datetime = null, 
	@FirstName varchar(50),
	@LastName varchar(50),
	@Phone varchar(21),
	@Email varchar(80), 
	@PasswordHash varchar(128),
	@PasswordSalt varchar(128),
	@LoginAttempts int,
	@LastLoginD datetime = null, 
	@PasswordResetCode varchar(10),
	@PasswordResetCodeRequestD datetime = null,
	@DisclosureAcceptedD datetime = null,
	@IsTemporaryPassword bit,
	@TempPasswordSetD datetime = null, 
	@IsNotifyOnStatusChanges bit,
	@IsNotifyOnNewDocRequests bit,
	@NotificationEmail varchar(80),
	@SignatureFontType int, 
	@SignatureName  varchar(150),
	@ReferralSource varchar(300),
	@SecurityPin varchar(4) = NULL,
	@EncryptedSecurityPin varbinary(20) = 0x
AS
BEGIN
	UPDATE TOP(1) CONSUMER_PORTAL_USER 
	SET 
		CreateD = @CreateD, 
		FirstName = @FirstName,
		LastName = @LastName, 
		Phone = @Phone,
		PasswordHash = @PasswordHash, 
		PasswordSalt = @PasswordSalt, 
		LoginAttempts  = @LoginAttempts, 
		LastLoginD  = @LastLoginD, 
		PasswordResetCode = @PasswordResetCode, 
		PasswordResetCodeRequestD = @PasswordResetCodeRequestD,
		DisclosureAcceptedD = @DisclosureAcceptedD,
		IsTemporaryPassword = @IsTemporaryPassword,
		TempPasswordSetD = @TempPasswordSetD,
		 IsNotifyOnStatusChanges = @IsNotifyOnStatusChanges,
		 IsNotifyOnNewDocRequests = @IsNotifyOnNewDocRequests,
		NotificationEmail  = @NotificationEmail,
		SignatureFontType = @SignatureFontType,
		SignatureName = @SignatureName,
		ReferralSource = @ReferralSource,
		SecurityPin = COALESCE(@SecurityPin, SecurityPin),
		EncryptedSecurityPin = @EncryptedSecurityPin
	WHERE Id = @Id and BrokerId = @BrokerId 
END
