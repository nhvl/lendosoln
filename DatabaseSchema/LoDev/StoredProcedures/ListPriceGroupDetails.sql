CREATE PROCEDURE ListPriceGroupDetails 
	@LpePriceGroupId Guid
AS
SELECT lLpTemplateId, LpePriceGroupName, LenderProfitMargin, LenderMaxYspAdjust, LenderRateMargin, LenderMarginMargin
FROM LPE_Price_Group_Product pgp JOIN LPE_Price_Group pg ON pgp.LpePriceGroupId = pg.LpePriceGroupId
WHERE pg.LpePriceGroupId = @LpePriceGroupId
     AND pg.ExternalPriceGroupEnabled = 1
     AND pgp.IsValid = 1
