CREATE PROCEDURE CUSTOM_FORM_UpdateFieldCategory
	@CategoryId int,
	@CategoryName varchar(100)
AS
BEGIN
	UPDATE CUSTOM_FORM_FIELD_CATEGORY
	Set 
	CategoryName = @CategoryName
	WHERE CategoryId = @CategoryId
END
