ALTER PROCEDURE [dbo].[INTERNAL_USER_ONLY_ListAllCras] AS
SELECT ComId, ComNm, ComUrl, ProtocolType, IsValid, IsInternalOnly, MclCraCode, IsWarningOn, WarningMsg, WarningStartD, VoxVendorId 
FROM Service_Company
ORDER BY ComNm
