-- =============================================
-- Author:		Thien Nguyen
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[EDOCS_DOCUTECH_DOCTYPE_Update] 
    @Id int,
    @Description varchar(200),
    @BarcodeClassification varchar(100)
AS
BEGIN

	UPDATE EDOCS_DOCUTECH_DOCTYPE
	SET	  Description = @Description, BarcodeClassification = @BarcodeClassification
    WHERE Id = @Id
		  
END
