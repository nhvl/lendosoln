-- =============================================
-- Author:		Michael Leinweaver
-- Create date: 10/20/2017
-- Description:	Removes a list of custom page IDs set
--				at the branch level.
-- =============================================
CREATE PROCEDURE [dbo].[BRANCH_ENABLED_RETAIL_PORTAL_PAGE_Remove]
	@BranchId uniqueidentifier,
	@RemovedPageXml xml
AS
BEGIN
	DECLARE @RemovedPages TABLE(PageId uniqueidentifier)
	
	INSERT INTO @RemovedPages(PageId)
	SELECT T.Item.value('.', 'uniqueidentifier')
	FROM @RemovedPageXml.nodes('r/p') as T(Item)

	DELETE FROM BRANCH_ENABLED_RETAIL_PORTAL_PAGE
	WHERE 
		BranchId = @BranchId AND 
		PageId in (SELECT PageId FROM @RemovedPages)
END
