

CREATE PROCEDURE [dbo].[CP_RetrieveAccessibleLoansByConsumerId]
	@ConsumerId bigint
AS

SELECT DISTINCT sLId, aAppId
FROM cp_consumer_x_loanid
WHERE	ConsumerId = @ConsumerId


