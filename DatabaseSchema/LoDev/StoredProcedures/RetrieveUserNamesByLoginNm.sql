CREATE PROCEDURE [dbo].[RetrieveUserNamesByLoginNm]
	@BrokerId uniqueidentifier,
	@LoginNm varchar(36)
AS
BEGIN
	SELECT UserId, UserFirstNm, UserLastNm
	FROM VIEW_EMPLOYEE_CONTACT_INFO
	WHERE BrokerId = @BrokerId and LoginNm = @LoginNm
END
