-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE CUSTOM_FORM_ListFieldCategory
AS
BEGIN
	SELECT CategoryId, CategoryName FROM CUSTOM_FORM_FIELD_CATEGORY
	ORDER BY CategoryName
END
