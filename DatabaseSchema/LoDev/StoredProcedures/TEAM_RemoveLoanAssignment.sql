

CREATE PROCEDURE [dbo].[TEAM_RemoveLoanAssignment] 
	@TeamId uniqueidentifier, 
	@LoanId uniqueidentifier

AS
BEGIN

	DELETE FROM Team_Loan_Assignment
	WHERE 
	@TeamId = TeamId
	AND 
	@LoanId = LoanId
	
END


