-- =============================================
-- Author:		Geoffrey Feltman
-- Create date: 11/14/13
-- Description:	Associate a Broker doc type id with a IDS doc type id.
-- =============================================
CREATE PROCEDURE dbo.EDOCS_AssociateIDSDocType 
	-- Add the parameters for the stored procedure here
	@BrokerId uniqueidentifier, 
	@DocTypeId int,
	@IDSDocTypeId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO EDOCS_IDS_DOCTYPE_X_DOCTYPE(IDSDocTypeId, DocTypeId, BrokerId) 
	VALUES( @IDSDocTypeId, @DocTypeId, @BrokerId )
END
