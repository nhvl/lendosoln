-- =============================================
-- Author:		Eric Mallare
-- Create date: 5/16/2017
-- Description:	Gets the lender services a user can choose for an order.
-- =============================================
ALTER PROCEDURE [dbo].[VERIFICATION_LENDER_SERVICE_GetLight]
	@BrokerId uniqueidentifier,
	@ServiceT int = null
AS
BEGIN
	SELECT
		LenderServiceId, DisplayName,
		IsEnabledForLqbUsers, EnabledEmployeeGroupId,
		IsEnabledForTpoUsers, EnabledOcGroupId, CanBeUsedForCredentialsInTpo,
		VendorId, ResellerId
	FROM
		VERIFICATION_LENDER_SERVICE
	WHERE	
		BrokerId=@BrokerId AND
		(@ServiceT IS NULL OR ServiceT=@ServiceT)
END
