-- =============================================
-- Author:		Eric Mallare
-- Create date: 6/2/2017
-- Description:	Gets the counts of VOX Lender services and orders.
-- =============================================
ALTER PROCEDURE [dbo].[GetVOXLenderServiceAndOrderCounts]
	@LoanId uniqueidentifier,
	@BrokerId uniqueidentifier
AS
BEGIN
	DECLARE @OrderCount int
	SET @OrderCount = (SELECT COUNT(*) FROM VOX_ORDER o WHERE o.LoanId=@LoanId AND o.BrokerId=@BrokerId)
	
	DECLARE @LenderServiceCount int
	SET @LenderServiceCount = (SELECT COUNT(*) FROM VERIFICATION_LENDER_SERVICE ls WHERE ls.BrokerId=@BrokerId)

	SELECT 
	@OrderCount as OrderCount,
	@LenderServiceCount as LenderServiceCount
END
