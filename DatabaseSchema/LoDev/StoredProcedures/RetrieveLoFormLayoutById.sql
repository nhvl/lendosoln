CREATE PROCEDURE RetrieveLoFormLayoutById 
	@FormId UniqueIdentifier,
	@BrokerId UniqueIdentifier
AS
BEGIN
	SELECT FieldMetadata FROM LO_FORM
	WHERE FormId = @FormId AND BrokerId = @BrokerId
END
