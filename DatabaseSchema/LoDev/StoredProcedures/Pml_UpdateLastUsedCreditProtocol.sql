CREATE PROCEDURE Pml_UpdateLastUsedCreditProtocol 
	@EmployeeID Guid,
	@LastUsedCreditProtocolID Guid
AS
	UPDATE BROKER_USER
	       SET LastUsedCreditProtocolID = @LastUsedCreditProtocolID
	WHERE EmployeeID = @EmployeeID
