-- =============================================
-- Author:		Isaac Ribakoff
-- Create date: 6/3/2014
-- Description:	Obtains files with lead new or loan open dates after a specified value.
--              Returns loan number, official loan officer and assigned loan officer. (OPM 181856)
-- =============================================
CREATE PROCEDURE [dbo].[RetrieveNewLoansForFNMA32Export]
	@CustomerCode varchar(50),
	@LastTransactionTime smalldatetime 
AS
BEGIN
	DECLARE @BrokerID uniqueidentifier
	SELECT @BrokerID=brokerid FROM Broker WHERE CustomerCode=@CustomerCode

	SELECT aBLastNm, sLNm, sAgentLoanOfficerEmail, sEmployeeLoanRepEmail
	FROM LOAN_FILE_CACHE
	WHERE ( sBrokerId = @BrokerID )
	and ( IsValid = 1 )
	and ( IsTemplate = 0 )
	and ( ( sLeadD >= @LastTransactionTime ) OR ( sOpenedD >= @LastTransactionTime ) )
	
	SELECT CURRENT_TIMESTAMP as TransactionCompleted
END
