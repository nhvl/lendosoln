-- =============================================
-- Author:		Antonio Valencia
-- Create date: 6/4/2013
-- Description:	Gets all CPs associated with a loan
-- =============================================
ALTER PROCEDURE [dbo].[CONSUMER_PORTAL_USER_GetByLoan] 
	-- Add the parameters for the stored procedure here
	@sLId uniqueidentifier 
AS
BEGIN
	SELECT u.Id, u.BrokerId, TempPasswordSetD, CreateD, FirstName, LastName, Phone, Email, PasswordHash, PasswordSalt, LoginAttempts, LastLoginD, PasswordResetCode, PasswordResetCodeRequestD, DisclosureAcceptedD, IsTemporaryPassword, sLId,IsNotifyOnStatusChanges ,IsNotifyOnNewDocRequests, NotificationEmail, SignatureFontType,SignatureName, ReferralSource, SecurityPin, EncryptedSecurityPin, EncryptionKeyId
	FROM CONSUMER_PORTAL_USER_X_LOANAPP x JOIN 
		 CONSUMER_PORTAL_USER u on u.id = x.ConsumerUserId 
	WHERE x.sLId = @sLId
END
