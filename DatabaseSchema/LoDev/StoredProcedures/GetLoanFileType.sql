-- =============================================
-- Author:		Justin Lara
-- Create date: 9/30/2016
-- Description:	Retrieves the loan file type for
--				a given loan.
-- =============================================
ALTER PROCEDURE [dbo].[GetLoanFileType] 
	@LoanId uniqueidentifier
AS
BEGIN
	SELECT COALESCE(lfc.sLoanFileT, lfb.sLoanFileT) AS sLoanFileT
	FROM LOAN_FILE_CACHE lfc
		LEFT JOIN LOAN_FILE_B lfb ON lfb.sLId = lfc.sLId
	WHERE lfc.sLId = @LoanId
END
