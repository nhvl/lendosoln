-- =============================================
-- Author:		Eric Mallare
-- Create date: 3/29/2018
-- Description:	Inserts an appraisal order edoc association record.
-- =============================================
ALTER PROCEDURE [dbo].[APPRAISAL_ORDER_EDOCS_Create]
	@AppraisalOrderId uniqueidentifier,
	@LoanId uniqueidentifier,
	@BrokerId uniqueidentifier,
	@EDocId uniqueidentifier
AS
BEGIN
	INSERT INTO APPRAISAL_ORDER_EDOCS
	(
		AppraisalOrderId,
		LoanId,
		BrokerId,
		EDocId
	)
	VALUES
	(
		@AppraisalOrderId,
		@LoanId,
		@BrokerId,
		@EDocId
	)


END