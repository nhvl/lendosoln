CREATE PROCEDURE [dbo].[RetrieveEmployeeFavoritePageJSONById]
	@EmployeeId uniqueidentifier
AS
BEGIN
	SELECT FavoritePageIdsJSON
	FROM EMPLOYEE
	WHERE EmployeeId = @EmployeeID
END
