
CREATE PROCEDURE [dbo].[TEAM_ListTeams] 
	@BrokerId		uniqueidentifier,
	@NameFilter		varchar(300) = null
AS
	SELECT 
		Id
		, RoleId
		, Name 
		, Notes
	FROM TEAM
	WHERE
	BrokerId = @BrokerId
	AND
	( ( @NameFilter IS NULL OR @NameFilter = '' )
	OR
	Name LIKE '%' + @NameFilter + '%' )