-- =============================================
-- Author:		paoloa
-- Create date: 4/15/2013
-- Description:	
-- =============================================
ALTER PROCEDURE [dbo].[LockPolicy_Create]
	-- Add the parameters for the stored procedure here
	@LockPolicyId uniqueidentifier,
    @PolicyNm varchar(100),
    @BrokerId uniqueidentifier,
    @IsUsingRateSheetExpirationFeature bit,
    @LpeIsEnforceLockDeskHourForNormalUser bit,
    @TimezoneForRsExpiration char(3),
    --@LpeLockDeskWorkHourStartTime smalldatetime,
    --@LpeLockDeskWorkHourEndTime smalldatetime,
    @LpeMinutesNeededToLockLoan int,
    @EnableAutoLockExtensions bit,
    @MaxLockExtensions int = null,
    @MaxTotalLockExtensionDays int = null,
    @LockExtensionOptionTableXmlContent varchar(max),
    @LockExtensionMarketWorsenPoints decimal(9,3) = null,
    @EnableAutoFloatDowns bit,
    @MaxFloatDowns int = null,
    @FloatDownPointFee decimal(9,3) = null,
    @FloatDownPercentFee decimal(9,3) = null,
    @FloatDownFeeIsPercent bit,
    @FloatDownMinRateImprovement decimal(9,3) = null,
    @FloatDownMinPointImprovement decimal(9,3) = null,
    @FloatDownAllowRateOptionsRequiringAdditionalPoints bit,
    @EnableAutoReLocks bit,
    @MaxReLocks int = null,
    @ReLockWorstCasePricingMaxDays int = null,
    @ReLockFeeTableXmlContent varchar(max),
    @ReLockMarketPriceReLockFee decimal(9,3) = null,
    @ReLockRequireSameInvestor bit,
    @DefaultLockDeskID uniqueidentifier,
    @FloatDownAllowedAfterLockExtension bit,
    @FloatDownAllowedAfterReLock bit,
    @DaySettingsXml varchar(max),
    @IsLockDeskEmailDefaultFromForRateLockConfirmation bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    INSERT INTO LENDER_LOCK_POLICY
		(
			LockPolicyId,
            PolicyNm,
            BrokerId,
            IsUsingRateSheetExpirationFeature,
            LpeIsEnforceLockDeskHourForNormalUser,
            TimezoneForRsExpiration,
         --   LpeLockDeskWorkHourStartTime,
          --  LpeLockDeskWorkHourEndTime,
            LpeMinutesNeededToLockLoan,
            EnableAutoLockExtensions,
            MaxLockExtensions,
            MaxTotalLockExtensionDays,
            LockExtensionOptionTableXmlContent,
            LockExtensionMarketWorsenPoints,
            EnableAutoFloatDowns,
            MaxFloatDowns,
            FloatDownPointFee,
            FloatDownPercentFee,
            FloatDownFeeIsPercent,
            FloatDownMinRateImprovement,
            FloatDownMinPointImprovement,
            FloatDownAllowRateOptionsRequiringAdditionalPoints,
            EnableAutoReLocks,
            MaxReLocks,
            ReLockWorstCasePricingMaxDays,
            ReLockFeeTableXmlContent,
            ReLockMarketPriceReLockFee,
            ReLockRequireSameInvestor,
            DefaultLockDeskID,
            FloatDownAllowedAfterLockExtension,
            FloatDownAllowedAfterReLock,
            DaySettingsXml,
            IsLockDeskEmailDefaultFromForRateLockConfirmation
		)
    VALUES
		(
			@LockPolicyId,
            @PolicyNm,
            @BrokerId,
            @IsUsingRateSheetExpirationFeature,
            @LpeIsEnforceLockDeskHourForNormalUser,
            @TimezoneForRsExpiration,
            --@LpeLockDeskWorkHourStartTime,
            --@LpeLockDeskWorkHourEndTime,
            @LpeMinutesNeededToLockLoan,
            @EnableAutoLockExtensions,
            @MaxLockExtensions,
            @MaxTotalLockExtensionDays,
            @LockExtensionOptionTableXmlContent,
            @LockExtensionMarketWorsenPoints,
            @EnableAutoFloatDowns,
            @MaxFloatDowns,
            @FloatDownPointFee,
            @FloatDownPercentFee,
            @FloatDownFeeIsPercent,
            @FloatDownMinRateImprovement,
            @FloatDownMinPointImprovement,
            @FloatDownAllowRateOptionsRequiringAdditionalPoints,
            @EnableAutoReLocks,
            @MaxReLocks,
            @ReLockWorstCasePricingMaxDays,
            @ReLockFeeTableXmlContent,
            @ReLockMarketPriceReLockFee,
            @ReLockRequireSameInvestor,
            @DefaultLockDeskID,
            @FloatDownAllowedAfterLockExtension,
            @FloatDownAllowedAfterReLock,
            @DaySettingsXml,
            @IsLockDeskEmailDefaultFromForRateLockConfirmation
		)
END
