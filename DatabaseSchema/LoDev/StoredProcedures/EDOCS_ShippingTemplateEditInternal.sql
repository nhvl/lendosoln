CREATE procedure [dbo].[EDOCS_ShippingTemplateEditInternal]
	@ShippingTemplateName varchar(50),
	@List varchar(MAX),
	@ShippingTemplateId int
as

if exists(select 1
			from EDOCS_SHIPPING_TEMPLATE 
			where ShippingTemplateName = @ShippingTemplateName
				and ShippingTemplateId <> @ShippingTemplateId
				and BrokerId is null)
begin
	RAISERROR('A shipping template with the same name already exists. Please select another name.', 16, 1);
	return;
end

BEGIN TRANSACTION

	delete from EDOCS_DOCUMENT_TYPE_X_SHIPPING_TEMPLATE
		where exists
			(select *
				from EDOCS_SHIPPING_TEMPLATE s
				where EDOCS_DOCUMENT_TYPE_X_SHIPPING_TEMPLATE.ShippingTemplateId = s.ShippingTemplateId
					and s.ShippingTemplateName = @ShippingTemplateName
					and s.ShippingTemplateId <> @ShippingTemplateId)
	IF @@error!= 0 GOTO HANDLE_ERROR

	delete from EDOCS_SHIPPING_TEMPLATE
		where ShippingTemplateName = @ShippingTemplateName
			and ShippingTemplateId <> @ShippingTemplateId
	IF @@error!= 0 GOTO HANDLE_ERROR

	update EDOCS_SHIPPING_TEMPLATE
		set ShippingTemplateName = @ShippingTemplateName
		where ShippingTemplateId = @ShippingTemplateId
			and BrokerId is null
	IF @@error!= 0 GOTO HANDLE_ERROR

	delete from EDOCS_DOCUMENT_TYPE_X_SHIPPING_TEMPLATE
		where exists
			(select *
				from EDOCS_SHIPPING_TEMPLATE s
				where EDOCS_DOCUMENT_TYPE_X_SHIPPING_TEMPLATE.ShippingTemplateId = s.ShippingTemplateId
					and s.ShippingTemplateId = @ShippingTemplateId
					and BrokerId is null)
	IF @@error!= 0 GOTO HANDLE_ERROR

	declare @start	int,
			@end    int,
			@length int,
			@order	int

	select @start = 0, 
		   @end = 1, 
		   @order = 1

	while @end > 0
	begin
		select @end = charindex(',', @List, @start + 1)
		select @length = @end - @start - 1
		if (@length > 0)
		begin
			insert EDOCS_DOCUMENT_TYPE_X_SHIPPING_TEMPLATE (ShippingTemplateId, DocTypeId, StackingOrder)
				values (@ShippingTemplateId, convert(int, substring(@List, @start + 1, @length)), @order)
			IF @@error!= 0 GOTO HANDLE_ERROR
		end
		select @order = @order + 1
		select @start = @end
	end

COMMIT TRANSACTION
RETURN

HANDLE_ERROR:
    ROLLBACK TRANSACTION
    RETURN