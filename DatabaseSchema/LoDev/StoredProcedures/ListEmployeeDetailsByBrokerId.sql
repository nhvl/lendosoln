ALTER PROCEDURE [dbo].[ListEmployeeDetailsByBrokerId]
	@BrokerId uniqueidentifier, 
	@NameFilter Varchar(32) = NULL,
	@LastFilter Varchar(32) = NULL, 
	@CompanyName Varchar(64) = NULL,
	@EarliestLastModifiedDate datetime = NULL,
	@EmployeeType varchar(1) = NULL
AS
	SELECT
		v.UserFirstNm ,
		v.UserLastNm ,
		v.Phone ,
		v.Email ,
		a.LoginNm ,
		a.Type ,
		CAST( CASE WHEN v.NewOnlineLoanEventNotifOptionT = 0 THEN 1 ELSE 0 END AS Bit ) AS DoNotify ,
		bu.LpePriceGroupId ,
		v.IsPmlManager ,
		bu.IsAccountOwner ,
		a.IsActive as CanLogin,
		v.IsActive ,
		v.EmployeeId ,
		a.UserId ,
		v.BranchId ,
		v.BrokerId ,
		l.LicenseNumber,
		e.NotesByEmployer,
		e.CommissionPointOfLoanAmount,
		e.CommissionPointOfGrossProfit,
		e.CommissionMinBase,
		e.EmployeeLicenseNumber,
		e.CellPhone,
		e.Pager,
		e.NmLsIdentifier,
		e.EmployeeIDInCompany,
        pmlBroker.Name AS PmlBrokerCompany,
        e.OriginatorCompensationPercent,
        e.OriginatorCompensationMinAmount,
        e.OriginatorCompensationMaxAmount,
        e.OriginatorCompensationFixedAmount,
        e.OriginatorCompensationNotes,
        e.OriginatorCompensationBaseT,
        e.OriginatorCompensationSetLevelT,
        e.OriginatorCompensationLastModifiedDate,
		e.CustomPricingPolicyField1Fixed,
		e.CustomPricingPolicyField2Fixed,
		e.CustomPricingPolicyField3Fixed,
		e.CustomPricingPolicyField4Fixed,
		e.CustomPricingPolicyField5Fixed,
		e.CustomPricingPolicyFieldSource,
		bu.PMLNavigationPermissions,
		e.ChumsID,
		e.UseOriginatingCompanyBranchId,
		bu.UseOriginatingCompanyLpePriceGroupId,
        pmlBroker.PmlBrokerId,
        e.MiniCorrespondentBranchId,
        e.CorrespondentBranchId,
        a.RecentLoginD,
        bu.EnabledIpRestriction,
        bu.MustLogInFromTheseIpAddresses,
        bu.EnabledMultiFactorAuthentication,
        bu.EnabledClientDigitalCertificateInstall,
        a.LastUsedIpAddress,
        e.LicenseXmlContent,
        a.LastUsedIpAddress,
		e.PopulateBrokerRelationshipsT,
		e.PopulateMiniCorrRelationshipsT,
		e.PopulateCorrRelationshipsT,
		e.PopulatePMLPermissionsT,
		bu.LastModifiedDate,
		e.IsCellphoneForMultiFactorOnly,
		bu.EnableAuthCodeViaSms,
		e.OriginatorCompensationIsOnlyPaidForFirstLienOfCombo
	FROM View_lendersoffice_employee AS v JOIN Employee e ON v.EmployeeId = e.EmployeeId
		 LEFT JOIN Broker_User bu ON e.EmployeeUserId = bu.UserId
		 LEFT JOIN All_User as a on bu.userid = a.userid
		 LEFT JOIN License as l on bu.currentlicenseid = l.licenseid
         LEFT JOIN Pml_Broker pmlBroker ON bu.PmlBrokerId = pmlBroker.PmlBrokerId
	WHERE v.BrokerId = @BrokerId
	      AND (
			( @LastFilter IS NULL AND ( v.UserFirstNm LIKE COALESCE(@NameFilter, v.UserFirstNm) + '%' OR  v.UserLastNm 

LIKE COALESCE(@NameFilter, v.UserLastNm) + '%'))
			OR
			( @LastFilter IS NOT NULL AND ( v.UserFirstNm LIKE @NameFilter + '%' AND v.UserLastNm LIKE @LastFilter + '%') )
          )
		  AND ( @EarliestLastModifiedDate IS NULL OR ( @EarliestLastModifiedDate <= bu.LastModifiedDate ) )
		  AND ( @EmployeeType IS NULL OR ( @EmployeeType = a.Type) )

