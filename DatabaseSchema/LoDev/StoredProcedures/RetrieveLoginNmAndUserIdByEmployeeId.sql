ALTER PROCEDURE [dbo].[RetrieveLoginNmAndUserIdByEmployeeId]
	@BrokerId uniqueidentifier,
	@EmployeeId uniqueidentifier
AS
BEGIN
	SELECT LoginNm, UserId
	FROM View_Employee_Contact_Info
	WHERE BrokerId = @BrokerId and EmployeeId = @EmployeeId
END
