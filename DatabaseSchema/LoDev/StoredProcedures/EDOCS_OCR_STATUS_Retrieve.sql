-- =============================================
-- Author:		Michael Leinweaver
-- Create date: 9/13/2017
-- Description:	Retrieves OCR request status data.
-- =============================================
ALTER PROCEDURE [dbo].[EDOCS_OCR_STATUS_Retrieve]
	@OcrRequestId uniqueidentifier,
	@BrokerId uniqueidentifier
AS
BEGIN
	SELECT Status, Date, SupplementalData
	FROM EDOCS_OCR_STATUS
	WHERE OcrRequestId = @OcrRequestId AND BrokerId = @BrokerId
	ORDER BY Date DESC
END
