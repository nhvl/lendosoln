





-- =============================================
-- Author:		Andrew Hatfield
-- Create date: July 30, 2008
-- Description:	Takes in a LoanId and returns information about it's current status/owner
-- =============================================
CREATE PROCEDURE [dbo].[CP_FindLoanListByConsumerId] 
	@ConsumerId bigint
	
AS
BEGIN
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT distinct
			cache.sLId,
			cache.sLNm,
			b.BrokerNm,
			b.BrokerId,
			
			lfa.DeletedD,
			lfa.IsValid,
			lfe.sOldLNm,
		
			-- Loan Status
			dbo.GetLoanStatusName(cache.sStatusT) as LoanStatus,

			-- Property Address
			cache.sspAddr + ', ' + cache.sspCity + ', ' + cache.sspState + ', ' + cache.sspZip as sSpFullAddr,

			-- Name of the borrower
			cache.sPrimBorrowerFullNm, 

			br.BranchNm,
			b.Status
			
	FROM
			loan_file_cache as cache JOIN
			loan_file_a as lfa
				on cache.slid = lfa.slid
			JOIN loan_file_e as lfe
				on lfe.slid = cache.slid
			JOIN broker as b
				on cache.sbrokerid = b.brokerid
			JOIN branch as br
				on cache.sBranchId = br.BranchId
			JOIN cp_consumer_x_loanid as cp
				on cache.slid = cp.slid
	WHERE
			cp.consumerid = @ConsumerId
			
END






