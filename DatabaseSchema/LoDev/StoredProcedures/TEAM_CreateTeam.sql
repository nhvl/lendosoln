
CREATE PROCEDURE [dbo].[TEAM_CreateTeam] 
	@BrokerId uniqueidentifier, 
	@RoleId uniqueidentifier,
	@Name varchar(300),
	@Notes varchar(max),
	@Id uniqueidentifier OUT
AS
BEGIN
	DECLARE @TempIdTable TABLE (Id uniqueidentifier)

	INSERT INTO Team(BrokerId, RoleId, Name, Notes )
	OUTPUT Inserted.Id INTO @TempIdTable
	VALUES(@BrokerId, @RoleId, @Name, @Notes) 
	
	SELECT @Id = Id FROM @TempIdTable
	
END

