CREATE PROCEDURE RetrieveStageConfigByKey
	@KeyIdStr varchar(100)
AS
SELECT OptionContentInt, OptionContentStr, Owner
FROM Stage_Config 
WHERE KeyIdStr = @KeyIdStr
