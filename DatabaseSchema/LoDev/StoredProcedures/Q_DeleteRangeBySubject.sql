-- =============================================
-- Author:		Antonio Valencia
-- Create date: 7/14/11
-- Description:	Deletes the messages between the given ids in the specified q with the specified subject
-- Inclusive
-- =============================================
CREATE PROCEDURE [dbo].[Q_DeleteRangeBySubject] 
		@QueueId int ,
		@Archive bit,
		@StartMsgId bigint,
		@EndMsgId bigint,
		@Subject1 varchar(300),
		@Priority tinyint
AS
BEGIN
	
	SET NOCOUNT ON;


	IF @Archive = 1	 
	BEGIN
		INSERT INTO Q_ARCHIVE( InsertionTime, QueueId, Subject1, Data,  MessageId, Subject2, Priority, DataLoc) 
			SELECT  InsertionTime, QueueId, Subject1, Data, MessageId, Subject2, Priority, DataLoc 
				FROM Q_MESSAGE WHERE QueueId = @QueueId AND MessageId >= @StartMsgId AND MessageId <= @EndMsgId AND Subject1 = @Subject1 AND Priority = @Priority
	END	

	DELETE FROM Q_MESSAGE 
	WHERE QueueId = @QueueId AND MessageId >= @StartMsgId AND MessageId <= @EndMsgId AND Subject1 = @Subject1 AND Priority = @Priority


	
END
