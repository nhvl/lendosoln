


-- =============================================
-- Author:		Antonio Valencia
-- Create date: 4/02/2012
-- Description:	Generate Alternate Loan Number
-- =============================================
ALTER PROCEDURE [dbo].[GetLeadNamingPatternAndCounter] 
	@BrokerId uniqueidentifier,
	@BranchId uniqueidentifier,
	@UpdateCounters bit = 1,
	@NamingPattern varchar(50) OUT,
	@NamingCounter BigInt OUT,
	@BranchLNmPrefix varchar(10) = NULL  OUT,
	@ReferenceCounter BigInt = NULL OUT,
	@ReferenceNamingPattern varchar(50) OUT
AS
-- dd 9/29/05 - Get the counter from broker table and increase counter. We will apply the naming pattern to actual name in C# code.
DECLARE @CurrentDate SmallDateTime
DECLARE @CounterDate SmallDateTime
DECLARE @ReferenceCounterDate SmallDateTime

SET @CurrentDate = GETDATE()

SELECT @NamingPattern = LeadNamingPattern, @NamingCounter = LeadNamingCounter , @CounterDate = LeadNamingCounterDate,
	   @ReferenceCounter = ReferenceCounter, @ReferenceCounterDate = ReferenceCounterDate, @ReferenceNamingPattern = NamingPattern
FROM Broker WHERE BrokerId = @BrokerId

SELECT @BranchLNmPrefix = BranchLNmPrefix FROM Branch WHERE BrokerId = @BrokerId AND BranchId = @BranchId

-- Check if counter need to reset by day or month or year or increment counter by one.
IF CHARINDEX('d}', @NamingPattern) > 0 AND ( DAY(@CounterDate) <> DAY(@CurrentDate) OR MONTH(@CounterDate) <> MONTH(@CurrentDate) OR YEAR(@CounterDate) <> YEAR(@CurrentDate) )
BEGIN
	SET @NamingCounter = 0
END
ELSE IF CHARINDEX('m}', @NamingPattern) > 0 AND ( MONTH(@CounterDate) <> MONTH(@CurrentDate) OR YEAR(@CounterDate) <> YEAR(@CurrentDate) )
BEGIN
	SET @NamingCounter = 0
END
ELSE IF CHARINDEX('y}', @NamingPattern) > 0 AND (  YEAR(@CounterDate) <> YEAR(@CurrentDate) ) 
BEGIN
	SET @NamingCounter = 0
END
ELSE
BEGIN
	SET @NamingCounter = @NamingCounter + 1
END

-- Check if reference counter need to reset by day or month or year or increment counter by one.
IF CHARINDEX('d}', @ReferenceNamingPattern) > 0 AND ( DAY(@ReferenceCounterDate) <> DAY(@CurrentDate) OR MONTH(@ReferenceCounterDate) <> MONTH(@CurrentDate) OR YEAR(@ReferenceCounterDate) <> YEAR(@CurrentDate) )
BEGIN
	SET @ReferenceCounter = 0
END
ELSE IF CHARINDEX('m}', @ReferenceNamingPattern) > 0 AND ( MONTH(@ReferenceCounterDate) <> MONTH(@CurrentDate) OR YEAR(@ReferenceCounterDate) <> YEAR(@CurrentDate) )
BEGIN
	SET @ReferenceCounter = 0
END
ELSE IF CHARINDEX('y}', @ReferenceNamingPattern) > 0 AND (  YEAR(@ReferenceCounterDate) <> YEAR(@CurrentDate) ) 
BEGIN
	SET @ReferenceCounter = 0
END
ELSE
BEGIN
	SET @ReferenceCounter = @ReferenceCounter + 1
END

-- Update Counter to broker table
IF @UpdateCounters = 1
BEGIN
	UPDATE Broker WITH ( ROWLOCK ) SET LeadNamingCounter  = @NamingCounter, LeadNamingCounterDate  = GETDATE(),
		ReferenceCounter = @ReferenceCounter, ReferenceCounterDate  = GETDATE()
	WHERE BrokerId = @BrokerId
END
--SELECT @NamingPattern AS NamingPattern, @NamingCounter AS NamingCounter

