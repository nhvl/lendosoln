
CREATE PROCEDURE [dbo].[VerifyAppCode] 
	@BrokerId uniqueidentifier, 
	@AppCode uniqueidentifier
AS

BEGIN
	SET NOCOUNT ON;
	SELECT 1 FROM dbo.BROKER_WEBSERVICE_APP_CODE 
	WHERE AppCode=@AppCode AND BrokerId=@BrokerId AND Enabled=1
END
