
CREATE PROCEDURE [dbo].[RetrieveDataTracLoginNmByUserId] 
	@UserId uniqueidentifier
AS
BEGIN
	SELECT DataTracLoginNm 
	FROM Broker_User
	WHERE UserId = @UserId
END

