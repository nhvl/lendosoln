
CREATE  PROCEDURE [dbo].GetIsShowBuyPriceHiddenAdjIn3rdPartyByBrokerId
	@BrokerId uniqueidentifier
AS
	SELECT
		IsShowBuyPriceHiddenAdjIn3rdParty
	FROM
		Broker
	WHERE
		BrokerId = @BrokerId
	if( 0!=@@error)
	begin
		RAISERROR('Error in the select statement in GetIsShowBuyPriceProfitMarginIn3rdPartyByBrokerID sp', 16, 1);
		return -100;
	end
	return 0;