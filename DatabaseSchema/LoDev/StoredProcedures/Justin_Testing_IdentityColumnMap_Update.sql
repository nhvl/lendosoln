



-- =============================================
-- Author:		Justin Jia
-- Create date: 	06-13-2014
-- Description:	Update the new identity key value for the identity key mapping
-- =============================================
CREATE PROCEDURE [dbo].[Justin_Testing_IdentityColumnMap_Update]
	@StageSiteSourceName char(100),
	@DatabaseSourceName char(100),
	@TableName char(100),
	@ColumnName char(100),
	@ColumnType char(50),
	@SourceDbValue varchar(200),
	@ThisDbValue varchar(200)
AS
BEGIN
	
	UPDATE DATA_IMPORT_IDENTITY_COLUMN_MAP SET 
	ThisDbValue=@ThisDbValue
	WHERE
	StageSiteSourceName=@StageSiteSourceName and
	DatabaseSourceName=@DatabaseSourceName and
	TableName=@TableName and
	ColumnName=@ColumnName and
	SourceDbValue=@SourceDbValue

END




