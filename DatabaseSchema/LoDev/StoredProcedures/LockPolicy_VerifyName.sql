-- =============================================
-- Author:		paoloa
-- Create date: 4/18/2013
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[LockPolicy_VerifyName]
	-- Add the parameters for the stored procedure here
	@LockPolicyId uniqueidentifier,
	@BrokerId uniqueidentifier,
	@PolicyNm varchar(100),
	@IsDuplicateId bit output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    IF EXISTS (SELECT * FROM LENDER_LOCK_POLICY WHERE PolicyNm = @PolicyNm AND BrokerId = @BrokerId AND NOT LockPolicyId = @LockPolicyId)
		SELECT @IsDuplicateId = 0
	ELSE
		SELECT @IsDuplicateId = 1
		
END
