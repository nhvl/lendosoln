ALTER PROCEDURE [dbo].[DeclareLoanFileValid]
	(
		@sLId uniqueidentifier,
		@IsValid bit,
		@IsLeadToLoan bit = 0
	)
AS
	declare @tranPoint sysname
	set @tranPoint = object_name(@@procId) + CAST(@@nestlevel as varchar(10))
	BEGIN TRANSACTION
	SAVE TRANSACTION @tranPoint
	declare @retCode  int;
	declare @errMsg char( 500 );
--************* main code ***********
	if( 0 = @IsValid )	
	begin
	     exec UnlinkLoans @sLId;
	end
	declare @currentState Boolean;
	select @currentState = ( select top 1 IsValid from loan_file_cache with( updlock, rowlock ) where sLId = @sLId );
	if( @IsValid <> @currentState )
	begin
		if( 0 = @IsValid )
		begin
			update Loan_file_A with(rowlock)
			set IsValid = 0
			where sLId = @sLId 
			if(0!=@@error)
			begin
				RAISERROR('Error during setting IsValid bit for future undo deletion process', 16, 1);
				goto ErrorHandler;
			end
			update loan_file_e with(rowlock)
			set sOldLNm = sLNm,
				sOldLRefNm = sLRefNm,
				sLNm = CAST( sLId as varchar(36)),
				SLRefNm = 
					CASE WHEN @IsLeadToLoan = 1 THEN 
						'L:' + CAST( sLId as varchar(36))
					ELSE 
						sLRefNm
					END
			where sLId = @sLId
			if(0!=@@error)
			begin
				RAISERROR('Error during storing old name for future undo deletion process', 16, 1);
				goto ErrorHandler;
			end
		end
		else
		begin
			update Loan_file_A with(rowlock)
			set IsValid =1
			where sLId = @sLId 
			if(0!=@@error)
			begin
				RAISERROR('Error while setting IsValid bit to 1', 16, 1);
				goto ErrorHandler;
			end
		end
		update discussion_log with(rowlock)
		set DiscIsRefObjValid = @IsValid
		where DiscRefObjId = @sLId
		
		update Condition with(rowlock)
		set IsLoanStillValid = @IsValid
		where LoanId = @sLId
	end
--***************************************
	commit transaction;
	return 0;
ErrorHandler:
	rollback tran @tranPoint
	rollback transaction
	return -100;

