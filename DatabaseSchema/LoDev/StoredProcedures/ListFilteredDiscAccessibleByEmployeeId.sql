
CREATE        PROCEDURE [dbo].[ListFilteredDiscAccessibleByEmployeeId]
	@EmployeeID uniqueidentifier
	, @UserId uniqueidentifier 
	, @BrokerId uniqueidentifier
	, @Filter char(50)
AS
select distinct top 500 
	disc.DiscLogId
	, notif.NotifId
	, notif.NotifIsValid
	, DiscDueDate
	, DiscSubject
	, DiscRefObjId
	, DiscRefObjNm1
	, DiscRefObjNm2
	, DiscPriority
	, DiscStatus
	, DiscCreatedDate
	, DiscCreatorLoginNm

FROM LOAN_FILE_CACHE loan WITH(NOLOCK)
JOIN LOAN_FILE_CACHE_3 loan3 WITH (NOLOCK) on loan.sLId = loan3.sLId
inner join discussion_log disc WITH(NOLOCK) on disc.discrefobjid = loan.slid 
left join discussion_notification notif WITH(NOLOCK) on disc.disclogid = notif.disclogid and notif.notifreceiveruserid = @UserId

where (loan.sEmployeeLoanRepId=@EmployeeId
       OR loan.sEmployeeProcessorId = @EmployeeId
       OR loan.sEmployeeManagerId = @EmployeeId
       OR loan.sEmployeeCallCenterAgentId = @EmployeeId
       OR loan.sEmployeeLoanOpenerId = @EmployeeId
       OR loan.sEMployeeRealEstateAgentId = @EmployeeId
       OR loan.sEmployeeLenderAccExecId = @EmployeeId
       OR loan.sEmployeeUnderwriterId = @EmployeeId
       OR loan.sEmployeeLockDeskId = @EmployeeId
       OR loan3.sEmployeeFunderId = @EmployeeId
       OR loan3.sEmployeeShipperId = @EmployeeId
       OR loan3.sEmployeePostCloserId = @EmployeeId
       OR loan3.sEmployeeInsuringId = @EmployeeId
       OR loan3.sEmployeeCollateralAgentId = @EmployeeId
       OR loan3.sEmployeeDocDrawerId = @EmployeeId
       OR loan3.sEmployeeCreditAuditorId = @EmployeeId
       OR loan3.sEmployeeDisclosureDeskId = @EmployeeId
       OR loan3.sEmployeeJuniorProcessorId = @EmployeeId
       OR loan3.sEmployeeJuniorUnderwriterId = @EmployeeId
       OR loan3.sEmployeeLegalAuditorId = @EmployeeId
       OR loan3.sEmployeeLoanOfficerAssistantId = @EmployeeId
       OR loan3.sEmployeePurchaserId = @EmployeeId
       OR loan3.sEmployeeQCComplianceId = @EmployeeId
       OR loan3.sEmployeeSecondaryId = @EmployeeId
       OR loan3.sEmployeeServicingId = @EmployeeId
      )
      AND
loan.sBrokerId = @BrokerId
AND
disc.discbrokerid = @brokerId and disc.discisrefobjvalid = 1 and loan.IsTemplate = 0 and disc.IsValid = 1

      AND -- Task Status
        (  (SUBSTRING(@Filter, 1, 1) = '1' AND disc.DiscStatus = 0)  -- Active
        OR (SUBSTRING(@Filter, 2, 1) = '1' AND disc.DiscStatus = 1)  -- Done
        OR (SUBSTRING(@Filter, 3, 1) = '1' AND disc.DiscStatus = 2)  -- Suspended
        OR (SUBSTRING(@Filter, 4, 1) = '1' AND disc.DiscStatus = 3)) -- Cancelled
      AND -- Participation
        (  (SUBSTRING(@Filter, 5, 1) = '1' AND (@UserID = notif.NotifReceiverUserId AND notif.NotifIsValid = 1) ) -- Active
        OR (SUBSTRING(@Filter, 6, 1) = '1' AND (@UserID = notif.NotifReceiverUserId AND notif.NotifIsValid = 0) ) -- Inactive
        OR (SUBSTRING(@Filter, 7, 1) = '1' AND notif.NotifIsValid is NULL ) ) -- Other
      AND -- Priority
        (  (SUBSTRING(@Filter, 8, 1) = '1' AND disc.DiscPriority = 2)   -- Low
        OR (SUBSTRING(@Filter, 9, 1) = '1' AND disc.DiscPriority = 1)   -- Normal
        OR (SUBSTRING(@Filter, 10, 1) = '1' AND disc.DiscPriority = 0)) -- High
      AND -- Loan Status
	(  (SUBSTRING(@Filter, 11, 1) = '1' AND loan.sStatusT = 0)  -- Open
	OR (SUBSTRING(@Filter, 12, 1) = '1' AND loan.sStatusT = 1)  -- Pre-Qual
	OR (SUBSTRING(@Filter, 13, 1) = '1' AND loan.sStatusT = 2)  -- Pre-Approve
	OR (SUBSTRING(@Filter, 14, 1) = '1' AND loan.sStatusT = 3)  -- Submitted
	OR (SUBSTRING(@Filter, 15, 1) = '1' AND loan.sStatusT = 4)  -- Approved
	OR (SUBSTRING(@Filter, 16, 1) = '1' AND loan.sStatusT = 5)  -- Docs
	OR (SUBSTRING(@Filter, 17, 1) = '1' AND loan.sStatusT = 6)  -- Funded
	OR (SUBSTRING(@Filter, 18, 1) = '1' AND loan.sStatusT = 7)  -- On Hold
	OR (SUBSTRING(@Filter, 19, 1) = '1' AND loan.sStatusT = 13) -- Underwritten
	OR (SUBSTRING(@Filter, 20, 1) = '1' AND loan.sStatusT = 19) -- Recorded
	OR (SUBSTRING(@Filter, 21, 1) = '1' AND loan.sStatusT = 8)  -- Suspended
	OR (SUBSTRING(@Filter, 22, 1) = '1' AND loan.sStatusT = 10) -- Denied
	OR (SUBSTRING(@Filter, 23, 1) = '1' AND loan.sStatusT = 9)  -- Cancelled
	OR (SUBSTRING(@Filter, 24, 1) = '1' AND loan.sStatusT = 11) -- Closed
	OR (SUBSTRING(@Filter, 25, 1) = '1' AND loan.sStatusT NOT IN (0,1,2,3,4,5,6,7,13,19,8,10,9,11))) -- Other
      AND -- Task Author
        (  (SUBSTRING(@Filter, 26, 1) = '1' AND @UserId = disc.DiscCreatorUserId)   -- Mine
        OR (SUBSTRING(@Filter, 27, 1) = '1' AND @UserId != disc.DiscCreatorUserId)) -- Others
