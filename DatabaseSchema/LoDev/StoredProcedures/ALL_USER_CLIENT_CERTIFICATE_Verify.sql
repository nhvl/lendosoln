-- =============================================
-- Author:		David Dao
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE dbo.ALL_USER_CLIENT_CERTIFICATE_Verify 
    @CertificateId UniqueIdentifier,
    @UserId UniqueIdentifier
AS
BEGIN

    SELECT 1 FROM ALL_USER_CLIENT_CERTIFICATE 
    WHERE CertificateId = @CertificateId AND UserId = @UserId 

END
