-- =============================================
-- Author:		Thien Nguyen
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[EDOCS_DOCUTECH_DOCTYPE_Insert] 
    @Description varchar(200),
    @BarcodeClassification varchar(100)
AS
BEGIN

	INSERT INTO EDOCS_DOCUTECH_DOCTYPE (Description,  BarcodeClassification)
					VALUES(@Description, @BarcodeClassification)
                                     
	SELECT CAST(SCOPE_IDENTITY() AS int)
END
