CREATE PROCEDURE [dbo].[UpdateBrokerPmlCompanyTiers]
	@BrokerId Guid , @PmlCompanyTiersXmlContent Text
AS
	UPDATE
		Broker
	SET
		PmlCompanyTiersXmlContent = @PmlCompanyTiersXmlContent
	WHERE
		BrokerId = @BrokerId
	IF( 0!=@@error)
	BEGIN
		RAISERROR('Error in updating Broker table in UpdateBrokerPmlCompanyTiers sp', 16, 1);
		RETURN -100;
	END
	RETURN 0;