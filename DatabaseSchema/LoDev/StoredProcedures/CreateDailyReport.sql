
-- =============================================
-- Author:		Long Nguyen
-- Create date: 8/4/2015
-- Description:	Update schedule report recovery (iOPM 215368): Add RunTime and RetryUntil
-- =============================================
CREATE PROCEDURE [dbo].[CreateDailyReport]
	@ReportId as int, 
	@Frequency as int, 
	@EffectiveDate as datetime, 
	@LastRun as datetime = NULL, 
	@NextRun as datetime, 
	@EndDate as datetime, 
	@IncludeSatSun as bit,
	@RunTime as datetime = NULL,
	@RetryUntil as datetime = NULL
as
BEGIN
	insert into DAILY_REPORTS 
	values( @ReportId, 
			@Frequency, 
			@EffectiveDate, 
			@LastRun, 
			@NextRun, 
			@EndDate, 
			@IncludeSatSun,
			@RunTime,
			@RetryUntil)
END
