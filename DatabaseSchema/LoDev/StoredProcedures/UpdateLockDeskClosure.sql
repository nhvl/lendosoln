




CREATE  PROCEDURE [dbo].[UpdateLockDeskClosure]
	@BrokerId uniqueidentifier,
	@ClosureDate smalldatetime,
	@LockDeskOpenTime smalldatetime,
	@LockDeskCloseTime smalldatetime,
	@IsClosedAllDay bit
AS
UPDATE LockDesk_Closure
SET
	LockDeskOpenTime = @LockDeskOpenTime
	, LockDeskCloseTime = @LockDeskCloseTime
	, IsClosedAllDay = @IsClosedAllDay
WHERE BrokerId = @BrokerId and ClosureDate = @ClosureDate
	if(0!=@@error)
	begin
		RAISERROR('Error in the update statement in UpdateLockDeskClosure sp', 16, 1);
		return -100;
	end
	return 0;