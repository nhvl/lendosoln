-- =============================================
-- Author:		Antonio Valencia
-- Create date: 4/16/2012
-- Description:	Finds a doc magic request 
-- =============================================
ALTER PROCEDURE [dbo].[DocMagic_FindESignRequest]
	@DocMagicAccountNumber varchar(20), 
	@sLNm varchar(36),
	@Username varchar(50)
AS
BEGIN
	SELECT ID, DocMagicAccountNumber ,sLId, sLNm, EmployeeId, doc.BrokerId, RequestedD, Username, Password 
	FROM DOCMAGIC_PENDING_ESIGN_REQUEST doc
	join broker br on br.brokerid = doc.brokerid
	WHERE sLNm = @sLNm 
	AND DocMagicAccountNumber = @DocMagicAccountNumber  
	AND Username = @Username
	and br.status = 1
END
