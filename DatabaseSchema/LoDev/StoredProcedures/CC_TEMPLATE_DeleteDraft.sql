-- =============================================
-- Author:		Antonio Valencia
-- Create date: 7/26/2012
-- Description:	Deletes a draft template system
-- =============================================
CREATE PROCEDURE dbo.CC_TEMPLATE_DeleteDraft
	@BrokerId uniqueidentifier
AS
BEGIN
	DECLARE @Id int 
	SET @Id = null 
	SELECT TOP 1 @Id = Id FROM CC_TEMPLATE_SYSTEM WHERE BrokerId = @BrokerId AND ReleasedD is NULL 
	IF @Id is NOT NULL BEGIN
		DELETE FROM CC_TEMPLATE WHERE CCTemplateSystemId = @Id AND BrokerID = @BrokerId 
		DELETE FROM CC_TEMPLATE_SYSTEM WHERE Id = @Id  AND Brokerid = @BrokerId 
	END
END
