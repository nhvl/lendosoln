-- =============================================
-- Author:		Justin Kim
-- Create date: 9/19/17
-- Description:	Update a predefined adjustment.
-- =============================================
ALTER PROCEDURE [dbo].[Predefined_Adjustment_Update]
	@Id int,
	@BrokerId uniqueidentifier,
	@Type int,
	@Description varchar(100),
	@From int,
	@To int,
	@POC bit,
	@IncludeIn1003Details int,
	@DFLP bit
AS
BEGIN
	update [dbo].[Predefined_Adjustment]
	set 
		[Type] = @Type,
		[Description] = @Description,
		[From] = @From,
		[To] = @To,
		[POC] = @POC,
		[IncludeIn1003Details] = @IncludeIn1003Details,
		[DFLP] = @DFLP
	where Id = @Id AND BrokerId = @BrokerId
END
