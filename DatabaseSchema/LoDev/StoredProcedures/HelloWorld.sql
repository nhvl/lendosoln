
CREATE PROCEDURE [dbo].[HelloWorld]
AS
   DECLARE 
   @mynvarchar NVARCHAR(50),
   @myfloat FLOAT
   SET @mynvarchar = @@VERSION
   SET @mynvarchar  = 'HELLO, WORLD'
   SET @myfloat = 1.6180

   PRINT @mynvarchar
   RETURN (0)

