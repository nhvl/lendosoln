CREATE PROCEDURE [dbo].[LockPolicyObservedHoliday_Delete]
	@LockPolicyId uniqueidentifier,
	@BrokerId uniqueidentifier,
	@HolidayName varchar(50)
AS
BEGIN
	DELETE FROM [dbo].[LENDER_LOCK_POLICY_OBSERVED_HOLIDAY]
	WHERE LockPolicyId = @LockPolicyId
		AND BrokerId = @BrokerId
		AND HolidayName = @HolidayName
END
