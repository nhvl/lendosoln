
-- =============================================
-- Author:		Brian Beery
-- Create date: 09/24/2013
-- Description:	Returns the number of Data Retrieval Framework partners that are enabled for the lender
-- =============================================
CREATE PROCEDURE [dbo].[DATA_RETRIEVAL_PARTNER_BROKER_CountByBrokerId] 
	@BrokerId UniqueIdentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT COUNT(PartnerId) AS NumPartners
	FROM DATA_RETRIEVAL_PARTNER_BROKER
	WHERE BrokerId = @BrokerId
END

