CREATE PROCEDURE dbo.LPE_PRICE_GROUP_InsertRevision
    @LpePriceGroupId UniqueIdentifier,
    @BrokerId UniqueIdentifier,
	@ContentKey varchar(100),
	@ModifiedUserId UniqueIdentifier,
	@ModifiedDate datetime
AS
BEGIN

	UPDATE LPE_PRICE_GROUP
	   SET ContentKey = @ContentKey
	WHERE LpePriceGroupId = @LpePriceGroupId AND BrokerId=@BrokerId
	
	INSERT INTO LPE_PRICE_GROUP_REVISION(BrokerId, LpePriceGroupId, ContentKey, ModifiedUserId, ModifiedDate)
	    VALUES (@BrokerId, @LpePriceGroupId, @ContentKey, @ModifiedUserId, @ModifiedDate)
END