ALTER procedure [dbo].[EDOCS_UpdateDocType]
	@DocTypeId int,	
	@FolderId int,
	@DocTypeName varchar(50),
	@ClassificationId int = 0,
	@DefaultForPageId int,
	@brokerid uniqueidentifier,
    @ESignTargetDocTypeId int = null
as

update EDOCS_DOCUMENT_TYPE
	set FolderId = @FolderId,
	DocTypeName = @DocTypeName,
	ClassificationId = @ClassificationId,
	DefaultForPageId = @DefaultForPageId
	where DocTypeId = @DocTypeId and @brokerid =brokerid 

IF @ESignTargetDocTypeId IS NOT NULL
BEGIN
EXEC dbo.EDOCS_DOCUMENT_TYPE_MAPPING_AddOrUpdate
    @SourceDocTypeId = @DocTypeId,
    @EsignDocTypeId = @ESignTargetDocTypeId
END
