CREATE PROCEDURE [dbo].[ImportCustomReportByLoginNm] 
	@LoginNm varchar(50),
	@QueryId varchar(100),
	@QueryNm varchar(100),
	@XmlContent text,
	@IsPublished bit,
	@NamePublishedAs varchar(100),
	@ShowPosition INT
AS
BEGIN
	DECLARE @brId nvarchar(100);
	DECLARE @EmpId nvarchar(100);
	Select Top 1
		@brId = br.brokerid ,  @EmpId = e.employeeid 
	FROM 
		Employee AS e  WITH (NOLOCK) 
		JOIN Branch AS r WITH (NOLOCK) ON r.BranchId = e.BranchId
		JOIN Broker as br with (nolock) on r.brokerid = br.brokerid
		JOIN broker_user as bu with (nolock) on e.employeeid = bu.employeeid
		JOIN all_user as a with (nolock) on a.userid = bu.userid
		JOIN Report_Query as rq with (nolock) on rq.employeeid = e.employeeid AND rq.brokerid = br.brokerid
	WHERE 
		a.loginNm = @LoginNm

	Insert into report_query
	Values(@QueryId, @brId, @EmpId, @QueryNm, @XmlContent, @IsPublished, @NamePublishedAs, @ShowPosition)

END		