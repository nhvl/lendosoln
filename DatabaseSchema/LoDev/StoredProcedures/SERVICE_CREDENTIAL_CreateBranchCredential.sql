ALTER PROCEDURE [dbo].[SERVICE_CREDENTIAL_CreateBranchCredential]
	@Id int OUTPUT,
	@BrokerId	uniqueidentifier,
	@BranchId uniqueidentifier,
	@ServiceProviderId	uniqueidentifier = null,
	@ServiceProviderName	varchar(100) = null,
	@UserType	int,
	@AccountId	varchar(100),
	@UserName	varchar(100),
	@UserPassword	varbinary(max),
	@EncryptionKeyId uniqueidentifier,
	@IsForCreditReports bit,
	@IsForVerifications bit,
	@IsForAusSubmission bit,
	@IsForTitleQuotes bit = 0,
    @IsForUcdDelivery bit = 0, -- TODO TITLE: Temp for testing
	@AusOption int = null,
	@IsEnabledForNonSeamlessDu bit = null,
	@VoxVendorId int = null,
	@VoxVendorName varchar(150) = null,
	@TitleQuoteLenderServiceId int = NULL,
	@TitleQuoteLenderServiceName varchar(200) = NULL,
	@ServiceProviderType int = null, -- TODO VOX USER: REMOVE THIS
	@IsForDocumentCapture bit = 0,
    @UcdDeliveryTarget int = null,
	@IsForDigitalMortgage bit = 0,
	@DigitalMortgageProviderTarget tinyint = null 

AS
   	BEGIN TRANSACTION
		IF(EXISTS 
			(SELECT Id 
			 FROM SERVICE_CREDENTIAL sc 
				JOIN BRANCH_X_SERVICE_CREDENTIAL bxsc ON sc.Id = bxsc.ServiceCredentialId AND sc.BrokerId = bxsc.BrokerId
			 WHERE 
				@BranchId = bxsc.BranchId
				AND @BrokerId = sc.BrokerId
				AND @UserType = sc.UserType
				AND 
				 ((@IsForCreditReports=1 AND sc.IsForCreditReports=1 AND @ServiceProviderId=sc.ServiceProviderId) OR
				  (@IsForVerifications=1 AND sc.IsForVerifications=1 AND @VoxVendorId=sc.VoxVendorId) OR
				  (@IsForAusSubmission=1 AND sc.IsForAusSubmission=1 AND @AusOption=sc.AusOption) OR
				  (@IsForTitleQuotes=1 AND sc.IsForTitleQuotes=1 AND @TitleQuoteLenderServiceId=sc.TitleQuoteLenderServiceId)OR
                  (@IsForUcdDelivery=1 AND sc.IsForUcdDelivery=1 AND @UcdDeliveryTarget=sc.UcdDeliveryTarget) OR
				  (@IsForDigitalMortgage=1 AND sc.IsForDigitalMortgage=1 AND @DigitalMortgageProviderTarget=sc.DigitalMortgageProviderTarget)) 
			))
			GOTO HANDLE_ERROR
		   	
   	  	INSERT INTO SERVICE_CREDENTIAL(
   	  		BrokerId,
			ServiceProviderId,
			ServiceProviderName,
			UserType,
			AccountId,
			UserName,
			UserPassword,
			EncryptionKeyId,
			IsForCreditReports,
			IsForVerifications,
			IsForAusSubmission,
			IsForTitleQuotes,
            IsForUcdDelivery,
			AusOption,
			IsEnabledForNonSeamlessDu,
			VoxVendorId,
			VoxVendorName,
			TitleQuoteLenderServiceId,
			TitleQuoteLenderServiceName,
			IsForDocumentCapture,
            UcdDeliveryTarget,
			IsForDigitalMortgage,
			DigitalMortgageProviderTarget) 
   		VALUES (
   			@BrokerId,
			@ServiceProviderId,
			@ServiceProviderName,
			@UserType,
			@AccountId,
			@UserName,
			@UserPassword,
			@EncryptionKeyId,
			@IsForCreditReports,
			@IsForVerifications,
			@IsForAusSubmission,
			@IsForTitleQuotes,
            @IsForUcdDelivery,
			@AusOption,
			@IsEnabledForNonSeamlessDu,
			@VoxVendorId,
			@VoxVendorName,
			@TitleQuoteLenderServiceId,
			@TitleQuoteLenderServiceName,
			@IsForDocumentCapture,
            @UcdDeliveryTarget,
			@IsForDigitalMortgage,
			@DigitalMortgageProviderTarget)
			
  		IF @@error!= 0 GOTO HANDLE_ERROR
   		SET @Id=SCOPE_IDENTITY();
   	 
   	  	INSERT INTO BRANCH_X_SERVICE_CREDENTIAL(
   	  		BranchId,
   	  		BrokerId,
			ServiceCredentialId) 
   		VALUES (
   			@BranchId,
   			@BrokerId,
			@Id)
   	
  
   	IF( @@error != 0 )
      	GOTO HANDLE_ERROR
   
   	COMMIT TRANSACTION
   	
   	RETURN;
   
      HANDLE_ERROR:
         	ROLLBACK TRANSACTION
        	RAISERROR('Error in [SERVICE_CREDENTIAL_CreateBranchCredential] sp', 16, 1);;
        	RETURN;
