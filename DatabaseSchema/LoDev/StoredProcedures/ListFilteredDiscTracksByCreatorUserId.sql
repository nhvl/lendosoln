CREATE   PROCEDURE ListFilteredDiscTracksByCreatorUserId
	@UserId Guid,
	@Filter char(50)
AS
	SELECT
		tr.TrackId,
		tr.TrackCreatedDate,
		tr.NotifId,
		tr.DiscLogId,
		tr.TrackNotifIsRead,
		notif.NotifReceiverUserId,
		notif.NotifReceiverLoginNm,
		notif.NotifIsValid,
		notif.NotifInvalidDate,
		disc.DiscRefObjId,
		disc.DiscRefObjNm1,
		disc.DiscRefObjNm2,
		disc.DiscSubject,
		disc.DiscPriority,
		disc.DiscStatus,
		disc.DiscDueDate,
		e.UserFirstNm,
		e.UserLastNm
	FROM
		( track tr WITH (NOLOCK) JOIN Discussion_Notification notif  WITH (NOLOCK) ON tr.notifid = notif.notifid 
                                         JOIN Discussion_Notification AS ex  WITH (NOLOCK) ON tr.DiscLogId = ex.DiscLogId AND ex.NotifReceiverUserId = @UserId AND ex.NotifIsValid = 1 
                                         LEFT JOIN Employee AS e  WITH (NOLOCK) ON e.EmployeeUserId = notif.NotifReceiverUserId) JOIN Discussion_Log disc  WITH (NOLOCK) ON tr.DiscLogId = disc.DiscLogId AND disc.DiscIsRefObjValid = 1 AND disc.IsValid = 1
	WHERE
		tr.TrackIsValid = 1
		AND
		tr.TrackCreatorUserId = @UserId
		AND -- Read Status
		(  (SUBSTRING(@Filter, 1, 1) = '1' AND tr.TrackNotifIsRead = 1)  -- Read
		OR (SUBSTRING(@Filter, 2, 1) = '1' AND tr.TrackNotifIsRead = 0)) -- Not Read
		AND -- Task Status
		(  (SUBSTRING(@Filter, 3, 1) = '1' AND disc.DiscStatus = 0)  -- Active
		OR (SUBSTRING(@Filter, 4, 1) = '1' AND disc.DiscStatus = 1)  -- Done
		OR (SUBSTRING(@Filter, 5, 1) = '1' AND disc.DiscStatus = 2)  -- Suspended
		OR (SUBSTRING(@Filter, 6, 1) = '1' AND disc.DiscStatus = 3)) -- Cancelled
		AND -- Priority
		(  (SUBSTRING(@Filter, 7, 1) = '1' AND disc.DiscPriority = 2)  -- Low
		OR (SUBSTRING(@Filter, 8, 1) = '1' AND disc.DiscPriority = 1)  -- Normal
		OR (SUBSTRING(@Filter, 9, 1) = '1' AND disc.DiscPriority = 0)) -- High
