CREATE PROCEDURE [dbo].[RetrieveEmployeeIdByPmlLoginName]
	@LoginName LoginName,
	@BrokerId Guid
AS
SELECT e.EmployeeId, e.IsActive
FROM All_User a JOIN Broker_User u ON a.UserId = u.UserId
   JOIN Employee e ON e.EmployeeId = u.EmployeeId
   JOIN Branch br ON e.BranchId = br.BranchId
WHERE a.LoginNm = @LoginName  AND br.BrokerId = @BrokerId AND Type = 'P'
