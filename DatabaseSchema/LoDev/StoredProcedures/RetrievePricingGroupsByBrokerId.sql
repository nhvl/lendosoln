
ALTER PROCEDURE [dbo].[RetrievePricingGroupsByBrokerId] 
	@BrokerId Guid
AS
SELECT BrokerId
		, LpePriceGroupId, ActualPriceGroupId
		,  ExternalPriceGroupEnabled
		, LpePriceGroupName
		, LenderPaidOriginatorCompensationOptionT
		, LockPolicyID
		, DisplayPmlFeeIn100Format
		, IsRoundUpLpeFee
		, RoundUpLpeFeeToInterval
		, ContentKey
		, IsAlwaysDisplayExactParRateOption
FROM LPE_Price_Group
WHERE BrokerId = @BrokerId

