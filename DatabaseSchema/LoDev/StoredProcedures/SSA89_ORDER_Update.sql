-- =============================================
-- Author:		Timothy Jewell
-- Create date:	07/26/2017
-- Description:	Updates an SSA-89 order.
-- =============================================
CREATE PROCEDURE [dbo].[SSA89_ORDER_Update]
	@OrderId int,
	@DeathMasterStatus int, -- Statuses could be null
	@SocialSecurityAdministrationStatus int,
	@OfficeOfForeignAssetsControlStatus int,
	@CreditHeaderStatus int,
	@RequestFormAccepted bit, -- RequestFormAccepted will be null until they tell us
	@RequestFormRejectionReason varchar(200) -- should be null unless Accepted = 0
AS
BEGIN
	UPDATE SSA89_ORDER
	SET
		DeathMasterStatus = @DeathMasterStatus,
		SocialSecurityAdministrationStatus = @SocialSecurityAdministrationStatus,
		OfficeOfForeignAssetsControlStatus = @OfficeOfForeignAssetsControlStatus,
		CreditHeaderStatus = @CreditHeaderStatus,
		RequestFormAccepted = @RequestFormAccepted,
		RequestFormRejectionReason = @RequestFormRejectionReason
	WHERE OrderId = @OrderId
END
