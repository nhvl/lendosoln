create procedure [dbo].UpdateWeeklyReport
	@LastRun as DateTime,
	@NextRun as DateTime,
	@ReportId as int
as
update WEEKLY_REPORTS 
set LastRun = @LastRun,
	NextRun = @NextRun
where ReportId = @ReportId;