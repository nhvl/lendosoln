-- =============================================
-- Author:		David Dao
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE dbo.EDOCS_DOCMAGIC_DOCTYPE_ListAll 

AS
BEGIN

    SELECT Id, DocumentClass, BarcodeClassification
    FROM EDOCS_DOCMAGIC_DOCTYPE
END
