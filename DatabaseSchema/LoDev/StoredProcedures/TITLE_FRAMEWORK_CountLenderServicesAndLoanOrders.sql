-- =============================================
-- Author:		Timothy Jewell
-- Create date: 10/5/2017
-- Description:	Gets the counts of Title Framework Lender services and orders.
-- =============================================
CREATE PROCEDURE [dbo].[TITLE_FRAMEWORK_CountLenderServicesAndLoanOrders]
	@LoanId uniqueidentifier,
	@BrokerId uniqueidentifier
AS
BEGIN
    SELECT
        (SELECT COUNT(*)
            FROM TITLE_FRAMEWORK_ORDER
            WHERE LoanId = @LoanId) AS 'OrderCount',
        (SELECT COUNT(*)
            FROM TITLE_FRAMEWORK_LENDER_SERVICE
            WHERE BrokerId = @BrokerId) AS 'LenderServiceCount'
END
