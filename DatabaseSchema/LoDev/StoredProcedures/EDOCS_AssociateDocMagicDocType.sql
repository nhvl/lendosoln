-- =============================================
-- Author:		Antonio Valencia
-- Create date: Aug 22, 2011
-- Description:	Update DocMAgicDocType Association
-- =============================================
CREATE PROCEDURE EDOCS_AssociateDocMagicDocType 
	-- Add the parameters for the stored procedure here
	@BrokerId uniqueidentifier,
	@DocTypeId int,
	@DocMagicDocTypeId int
AS
BEGIN
	
	INSERT INTO EDOCS_DOCMAGIC_DOCTYPE_X_DOCTYPE(DocMagicDocTypeId, DocTypeId, BrokerId) 
	VALUES( @DocMagicDocTypeId, @DocTypeId, @BrokerId )
END
