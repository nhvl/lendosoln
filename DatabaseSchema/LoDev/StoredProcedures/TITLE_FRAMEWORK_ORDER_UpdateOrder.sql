-- =============================================
-- Author:		Justin Lara
-- Create date: 8/21/2017
-- Description:	Updates a Title order in the database.
-- =============================================
ALTER PROCEDURE [dbo].[TITLE_FRAMEWORK_ORDER_UpdateOrder]
	@OrderId int,
	@Status int,
	@AppliedDate datetime = NULL,
	@LoanId uniqueidentifier,
	@BrokerId uniqueidentifier
AS
BEGIN
	UPDATE
		TITLE_FRAMEWORK_ORDER
	SET
		[Status] = @Status,
		AppliedDate = @AppliedDate
	WHERE
		OrderId = @OrderId AND
		BrokerId = @BrokerId AND
		LoanId = @LoanId
END
