-- ============================================================
-- Author:      Generated by application LqbEntitiesToSqlTables
-- Create date: 11/6/2018
-- Description: Retrieve a row from the CONSUMER_X_COUNSELING_EVENT table associated with a given loan.
-- ============================================================
CREATE PROCEDURE [dbo].[CONSUMER_X_COUNSELING_EVENT_RetrieveByLoanId]
	@LoanId UniqueIdentifier
AS
BEGIN
	SELECT
		CounselingEventAttendanceId,
		ConsumerId,
		CounselingEventId
	FROM [dbo].[CONSUMER_X_COUNSELING_EVENT]
	WHERE LoanId = @LoanId
END
