-- =============================================
-- Author:		Justin Lara
-- Create date: 1/11/2017
-- Description:	Saves an appraisal vendor message.
-- =============================================
ALTER PROCEDURE [dbo].[APPRAISAL_VENDOR_MESSAGES_SaveMessage]
	@VendorId uniqueidentifier,
	@BrokerId uniqueidentifier,
	@LoanId uniqueidentifier,
	@OrderNumber varchar(50),
	@SendingParty varchar(50),
	@ReceivingParty varchar(50),
	@Subject varchar(200),
	@Message varchar(5000),
	@Date datetime
AS
BEGIN
	SET NOCOUNT ON;
	
	-- Prevent saving duplicate messages
	IF NOT EXISTS
	(
		SELECT 1
		FROM APPRAISAL_VENDOR_MESSAGES
		WHERE VendorId = @VendorId
			AND BrokerId = @BrokerId
			AND LoanId = @LoanId
			AND OrderNumber = @OrderNumber
			AND SendingParty = @SendingParty
			AND ReceivingParty = @ReceivingParty
			AND Subject = @Subject
			AND Message = @Message
			
			-- Exclude Date from equivalence comparison, a
			-- duplicate message may come at a different second.
	)
	BEGIN
		INSERT INTO APPRAISAL_VENDOR_MESSAGES
		(
			VendorId,
			BrokerId,
			LoanId,
			OrderNumber,
			SendingParty,
			ReceivingParty,
			Subject,
			Message,
			Date
		)
		VALUES
		(
			@VendorId,
			@BrokerId,
			@LoanId,
			@OrderNumber,
			@SendingParty,
			@ReceivingParty,
			@Subject,
			@Message,
			@Date
		)
	END
END
