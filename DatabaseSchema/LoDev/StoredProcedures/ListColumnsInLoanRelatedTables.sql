-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ListColumnsInLoanRelatedTables]
AS
BEGIN

SELECT column_name, data_type, table_name, character_maximum_length, numeric_precision, numeric_scale 
FROM INFORMATION_SCHEMA.COLUMNS 
WHERE table_name IN
('LOAN_FILE_A', 'LOAN_FILE_B', 'LOAN_FILE_C', 'LOAN_FILE_D', 'LOAN_FILE_E', 'LOAN_FILE_F',
'APPLICATION_A', 'APPLICATION_B', 'TRUST_ACCOUNT', 'BRANCH')

END
