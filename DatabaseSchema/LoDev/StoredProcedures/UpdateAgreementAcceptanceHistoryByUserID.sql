CREATE PROCEDURE UpdateAgreementAcceptanceHistoryByUserID 
	@UserID Guid,
	@NeedToAcceptLatestAgreement bit,
	@AgreementAcceptanceHistory Text
AS
	UPDATE All_User
	     SET NeedToAcceptLatestAgreement = @NeedToAcceptLatestAgreement,
		AgreementAcceptanceHistory = @AgreementAcceptanceHistory
	WHERE UserID = @UserID
