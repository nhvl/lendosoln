CREATE PROCEDURE [dbo].[EDOCS_LostDocumentCount]
	@BrokerId uniqueidentifier = null,
	@DocumentSource int = 0
AS
BEGIN
	
	select count(*) FROM EDOCS_LOST_DOCUMENT where 
	DocumentSource = @DocumentSource AND 
	( BrokerId is null and @BrokerId is null OR BrokerId = @BrokerId) 

END