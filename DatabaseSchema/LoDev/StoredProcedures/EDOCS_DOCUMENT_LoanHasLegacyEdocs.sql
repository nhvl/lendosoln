CREATE PROCEDURE dbo.EDOCS_DOCUMENT_LoanHasLegacyEdocs 
    @BrokerId UniqueIdentifier,
    @sLId UniqueIdentifier
AS
BEGIN

SELECT count(*)
FROM EDOCS_DOCUMENT
WHERE BrokerId = @BrokerId
    AND sLId = @sLId
    AND FileDBKey_CurrentRevision <> '00000000-0000-0000-0000-0000000000000'
	AND ImageStatus <> 1

END
