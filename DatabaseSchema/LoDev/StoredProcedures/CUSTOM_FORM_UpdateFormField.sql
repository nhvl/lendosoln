CREATE PROCEDURE [dbo].CUSTOM_FORM_UpdateFormField
	@FieldId varchar(100),
	@FieldFriendlyName varchar(200),
	@CategoryId int,
	@IsReviewNeeded bit,
	@IsBoolean bit,
	@Description varchar(200),
	@IsHidden bit
AS
BEGIN
	UPDATE CUSTOM_FORM_FIELD_LIST
	Set FieldFriendlyName = @FieldFriendlyName,
	CategoryId = @CategoryId,
	IsReviewNeeded = @IsReviewNeeded,
	IsBoolean = @IsBoolean,
	Description = @Description,
	IsHidden = @IsHidden
	WHERE FieldId = @FieldId
END
