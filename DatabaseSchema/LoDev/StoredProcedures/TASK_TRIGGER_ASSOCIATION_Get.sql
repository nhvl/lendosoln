CREATE PROCEDURE [dbo].[TASK_TRIGGER_ASSOCIATION_Get]
	@BrokerId uniqueidentifier,
	@LoanId uniqueidentifier,
	@TaskId varchar(10) = null,
	@TriggerType int = null
AS
BEGIN
	SELECT
		Id,
		BrokerId,
		LoanId,
		TaskId,
		TriggerType,
		TriggerName
	FROM TASK_TRIGGER_ASSOCIATION
	WHERE BrokerId = @BrokerId
		AND LoanId = @LoanId
		AND (@TaskId IS NULL
			OR TaskId = @TaskId)
		AND (@TriggerType IS NULL
			OR TriggerType = @TriggerType)
END