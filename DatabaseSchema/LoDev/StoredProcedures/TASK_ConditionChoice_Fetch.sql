



-- =============================================
-- Author:		Antonio Valencia
-- Create date: 4/12/11
-- Description:	Fetch new Condition Choices
--
-- 03/27/2014 - Added RequiredDocTypeId - OPM 170314
-- =============================================
ALTER PROCEDURE [dbo].[TASK_ConditionChoice_Fetch] 
	@BrokerId uniqueidentifier, 
	@Id int = null
AS

-- REVIEWS
-- 6/9/2011: Reviewed. -ThinhNK

BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT 	
		ConditionChoiceId,
		CONDITION_CHOICE.BrokerId,
		CONDITION_CHOICE.ConditionCategoryId,
		ConditionType,
		sLT,
		ConditionSubject,
		ToBeAssignedRole,
		ToBeOwnedByRole,
		DueDateFieldName,
		DueDateAddition,
		Rank,
		cc.Category,
		cc.DefaultTaskPermissionLevelId,
		cc.IsDisplayAtTop,
		arole.RoleModifiableDesc as ToBeAssignedRoleName, 
		brole.RoleModifiableDesc  as ToBeOwnedByRoleName,
		RequiredDocTypeId,
		IsHidden
	FROM CONDITION_CHOICE WITH(ROWLOCK)
		LEFT JOIN ROLE as arole on CONDITION_CHOICE.ToBeAssignedRole = arole.RoleId 
		LEFT JOIN   ROLE as brole on CONDITION_CHOICE.ToBeOwnedByRole = brole.RoleId
		LEFT JOIN Condition_Category as cc on cc.BrokerId = CONDITION_CHOICE.BrokerId  AND
                                              cc.ConditionCategoryId = CONDITION_CHOICE.ConditionCategoryId 

	WHERE CONDITION_CHOICE.BrokerId = @BrokerId AND ConditionChoiceId= COALESCE( @Id, ConditionChoiceId) 
	ORDER BY Rank, cc.Category
		
END




