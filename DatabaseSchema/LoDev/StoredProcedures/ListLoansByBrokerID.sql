ALTER  PROCEDURE [dbo].[ListLoansByBrokerID]
	@BrokerID Guid , @IsTemplate Bit = NULL , @IsValid Bit = NULL, @ExcludeQp Bit = NULL
AS
	SELECT
		c.sLId AS LoanId , c.sBrokerId AS BrokerId , c.sBranchId AS BranchId , c.sLNm AS LoanNm , c.sPrimBorrowerFullNm AS BorrowerNm , c.sStatusT AS Status
	FROM
		Loan_File_Cache AS c with(nolock) 
	WHERE
		c.sBrokerId = @BrokerID
		AND
		c.IsTemplate = COALESCE( @IsTemplate , c.IsTemplate )
		AND
		c.IsValid = COALESCE( @IsValid , c.IsValid )
		AND
		(COALESCE(@ExcludeQp, 0) = 0 OR (c.sLoanFileT <> 2 AND c.sLNm NOT LIKE 'QUICKPRICER_%' ))
	IF ( @@error != 0 )
	BEGIN
		RAISERROR('Error in the select statement in ListLoansByBrokerID sp', 16, 1);
		RETURN -100;
	END
