


CREATE  PROCEDURE [dbo].[CreateDiscussionLog]
	@DiscLogId uniqueidentifier out,
	@DiscCreatorUserId uniqueidentifier,
	@DiscRefObjId uniqueidentifier,
	@DiscRefObjType varchar(36),
	@DiscRefObjNm1 varchar(36),
	@DiscRefObjNm2 varchar(100),
	@DiscDueDate datetime,
	@DiscSubject varchar (500),
	@DiscPriority int,
	@DiscStatus int,
	@DiscStatusDate datetime,
	@DiscCreatorLoginNm varchar(36),
	@DiscCreationDate datetime,
	@IsValid bit = 0,
	@BrokerId uniqueidentifier
 AS
declare @DiscIsRefObjValid bit;
set @DiscLogId = newid();
if @DiscRefObjId = '00000000-0000-0000-0000-000000000000'
begin 
	if @DiscRefObjNm1 <> ''
	begin
		select @DiscRefObjId = sLId, @DiscIsRefObjValid = IsValid, @DiscRefObjNm2 = sPrimBorrowerFullNm
		from Loan_File_Cache with(nolock)
		where sBrokerID = @BrokerID and sLNm = @DiscRefObjNm1
	end
	else
		set @DiscIsRefObjValid  = 1;
end
else
begin
	select @DiscIsRefObjValid = IsValid, @DiscRefObjNm1 = sLNm, @DiscRefObjNm2 = sPrimBorrowerFullNm
	from Loan_File_Cache WITH(NOLOCK)
	where sBrokerID = @BrokerID and sLId = @DiscRefObjId
end
if( @DiscRefObjId <> '00000000-0000-0000-0000-000000000000' and @DiscRefObjNm2 = '' )
begin
	-- old loans which had been created prior to sPrimBorrowerNm was introduced doesn't have the 
	-- field sPrimBorrowerNm correctly updated.
	declare @firstNm varchar(36);
	declare @lastNm varchar(36);
	select top 1 @firstNm = aBFirstNm, @lastNm = aBLastNm
	from Application_A
	where sLId = @DiscRefObjId
	order by PrimaryRankIndex asc
	if( @firstNm = '' )
		set @DiscRefObjNm2 = @lastNm;
	else if( @lastNm = '' )
		set @DiscRefObjNm2 = @firstNm;
	else
		set @DiscRefObjNm2 = @firstNm + ' ' + @lastNm;
end
insert into discussion_log 
([DiscLogId], [DiscCreatedDate], [DiscLastModifierLoginNm], [DiscLastModifierUserId], [DiscLastModifyD], [DiscCreatorUserId], [DiscRefObjId], [DiscRefObjType], [DiscRefObjNm1], [DiscRefObjNm2], [DiscDueDate], [DiscSubject], [DiscPriority], [DiscStatus], [DiscStatusDate], [DiscIsRefObjValid], [DiscCreatorLoginNm], [IsValid], [DiscBrokerId])
values
(@DiscLogId, @DiscCreationDate, @DiscCreatorLoginNm, @DiscCreatorUserId, @DiscCreationDate, @DiscCreatorUserId, @DiscRefObjId, @DiscRefObjType, @DiscRefObjNm1, @DiscRefObjNm2, @DiscDueDate, @DiscSubject, @DiscPriority, @DiscStatus, @DiscStatusDate, @DiscIsRefObjValid, @DiscCreatorLoginNm, @IsValid, @BrokerId)



