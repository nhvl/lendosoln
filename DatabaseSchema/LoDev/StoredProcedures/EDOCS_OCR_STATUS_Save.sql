-- =============================================
-- Author:		Michael Leinweaver
-- Create date: 9/13/2017
-- Description:	Saves OCR request status data.
-- =============================================
CREATE PROCEDURE [dbo].[EDOCS_OCR_STATUS_Save]
	@OcrRequestId uniqueidentifier,
	@BrokerId uniqueidentifier,
	@Status tinyint,
	@Date datetime,
	@SupplementalData varchar(4000) = null
AS
BEGIN
	INSERT INTO EDOCS_OCR_STATUS(OcrRequestId, BrokerId, Status, Date, SupplementalData)
	VALUES(@OcrRequestId, @BrokerId, @Status, @Date, @SupplementalData)
END
