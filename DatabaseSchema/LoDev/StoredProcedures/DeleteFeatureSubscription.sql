


CREATE    PROCEDURE [dbo].[DeleteFeatureSubscription]
	@BrokerID uniqueidentifier,
	@FeatureID uniqueidentifier
AS
	
	DELETE FROM Feature_Subscription 
	WHERE BrokerId = @BrokerID AND FeatureId = @FeatureID
	
	IF 0 != @@ERROR
	BEGIN
		RAISERROR('Error deleting from Feature_Subscription in [DeleteFeatureSubscription] sp', 16, 1);
		RETURN -100
	END
	RETURN 0;