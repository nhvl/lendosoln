CREATE PROCEDURE [dbo].[Billing_VOX]
	@StartDate datetime,
	@EndDate datetime
AS
BEGIN
-- dd 2018-11-02 - For use in automate billing executable.
select b.BrokerNm AS LenderName, lfc.sLNm As LoanNumber, lfc.sLId AS LoanID, vo.VendorId, vo.OrderNumber, 
                                                    vo.OrderedBy, vo.DateOrdered, vo.DateCompleted
                                                FROM VOX_ORDER vo
                                                JOIN Broker b on vo.BrokerId = b.BrokerId
                                                JOIN LOAN_FILE_CACHE lfc on lfc.sLId = vo.LoanId
                                                WHERE DateOrdered >= @StartDate AND DateOrdered < @EndDate
                                                AND b.SuiteType = 1
                                                AND lfc.sLoanFileT = 0
                                                ORDER BY LenderName
END