ALTER PROCEDURE [dbo].[ComplianceEase_RetrieveSavedAuthentication]
	@UserId UniqueIdentifier
AS
BEGIN
	SELECT
		ComplianceEaseUserName,
		ComplianceEasePassword,
		EncryptedComplianceEasePassword,
		EncryptionKeyId
	FROM BROKER_USER
	WHERE UserId = @UserId
END
