-- =============================================
-- Author: Scott Kibler
-- Create date: 5/9/2017
-- Description: 
--	Deletes the groups for the given permission type then adds them based on the xml.
--  The format of the xml should be:
--	  <g id="..."/><g ...
-- =============================================
ALTER PROCEDURE [dbo].[SetGroupsPermittedForPermissionLevel]
	@BrokerId uniqueidentifier,
	@PermissionLevelId int,
	@PermissionType int,
	@XmlOfGroupIdsPermittedForPermissionLevelAndType xml -- format: <g id="..."/><g ...
AS
BEGIN

	DELETE FROM CONVOLOG_PERMISSION_GROUP
	WHERE
		BrokerId = @BrokerId
	AND PermissionLevelId = @PermissionLevelId
	AND PermissionType = @PermissionType 
		
	INSERT INTO 
		CONVOLOG_PERMISSION_GROUP
		(PermissionLevelId, BrokerId, GroupId, PermissionType)
	SELECT
		@PermissionLevelId as PermissionLevelId,
		@BrokerId as brokerid,
		tbl.col.value('@id', 'uniqueidentifier') as GroupId,
		@PermissionType as PermissionType
	FROM 
		@XmlOfGroupIdsPermittedForPermissionLevelAndType.nodes('//g') tbl(col)
	
END