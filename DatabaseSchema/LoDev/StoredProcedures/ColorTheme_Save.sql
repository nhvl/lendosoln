-- =============================================
-- Author:		Michael Leinweaver
-- Create date: 6/21/2017
-- Description:	Saves a color theme.
-- =============================================
ALTER PROCEDURE [dbo].[ColorTheme_Save]
	@CollectionId int,
	@ColorThemeId uniqueidentifier,
	@Name varchar(50),
	@PageHeaderBackground varchar(7) = null,
	@PageHeaderText varchar(7) = null,
	@PageHeaderAccent varchar(7) = null,
	@SideNavigationBackground varchar(7) = null,
	@SideNavigationSelection varchar(7) = null,
	@SideNavigationCollapseExpandButton varchar(7) = null,
	@SideNavigationText varchar(7) = null,
	@ContentHeader varchar(7) = null,
	@ContentIconButtonTab varchar(7) = null,
	@ContentLinkSortedColumn varchar(7) = null,
	@ContentTooltip varchar(7) = null,
	@ContentTooltipText varchar(7) = null,
	@ContentText varchar(7) = null,
	@ContentRaisedButtonText varchar(7) = null,
	@ContentFlatButtonText varchar(7) = null,
	@ContentProgressBarPast varchar(7) = null,
	@ContentProgressBarCurrent varchar(7) = null,
	@PageHeaderSaveButtonText varchar(7) = null,
	@PageHeaderSaveButtonBackground varchar(7) = null
AS
BEGIN
	UPDATE BROKER_COLOR_THEME
	SET
		Name = @Name,
		PageHeaderBackground = COALESCE(@PageHeaderBackground, PageHeaderBackground),
		PageHeaderText = COALESCE(@PageHeaderText, PageHeaderText),
		PageHeaderAccent = COALESCE(@PageHeaderAccent, PageHeaderAccent),
		SideNavigationBackground = COALESCE(@SideNavigationBackground, SideNavigationBackground),
		SideNavigationSelection = COALESCE(@SideNavigationSelection, SideNavigationSelection),
		SideNavigationCollapseExpandButton = COALESCE(@SideNavigationCollapseExpandButton, SideNavigationCollapseExpandButton),
		SideNavigationText = COALESCE(@SideNavigationText, SideNavigationText),
		ContentHeader = COALESCE(@ContentHeader, ContentHeader),
		ContentIconButtonTab = COALESCE(@ContentIconButtonTab, ContentIconButtonTab),
		ContentLinkSortedColumn = COALESCE(@ContentLinkSortedColumn, ContentLinkSortedColumn),
		ContentTooltip = COALESCE(@ContentTooltip, ContentTooltip),
		ContentTooltipText = COALESCE(@ContentTooltipText, ContentTooltipText),
		ContentText = COALESCE(@ContentText, ContentText),
		ContentRaisedButtonText = COALESCE(@ContentRaisedButtonText, ContentRaisedButtonText),
		ContentFlatButtonText = COALESCE(@ContentFlatButtonText, ContentFlatButtonText),
		ContentProgressBarPast = COALESCE(@ContentProgressBarPast, ContentProgressBarPast),
		ContentProgressBarCurrent = COALESCE(@ContentProgressBarCurrent, ContentProgressBarCurrent),
		PageHeaderSaveButtonText = COALESCE(@PageHeaderSaveButtonText, PageHeaderSaveButtonText),
		PageHeaderSaveButtonBackground = COALESCE(@PageHeaderSaveButtonBackground, PageHeaderSaveButtonBackground)
	WHERE CollectionId = @CollectionId AND ColorThemeId = @ColorThemeId
	
	IF @@rowcount = 0
	BEGIN
		INSERT INTO BROKER_COLOR_THEME(
			ColorThemeId, 
			CollectionId, 
			Name, 
			PageHeaderBackground,
			PageHeaderText,
			PageHeaderAccent,
			SideNavigationBackground,
			SideNavigationSelection,
			SideNavigationCollapseExpandButton,
			SideNavigationText,
			ContentHeader,
			ContentIconButtonTab,
			ContentLinkSortedColumn,
			ContentTooltip,
			ContentTooltipText,
			ContentText,
			ContentRaisedButtonText,
			ContentFlatButtonText,
			ContentProgressBarPast,
			ContentProgressBarCurrent,
			PageHeaderSaveButtonText,
			PageHeaderSaveButtonBackground)
		VALUES(
			@ColorThemeId, 
			@CollectionId, 
			@Name, 
			@PageHeaderBackground,
			@PageHeaderText,
			@PageHeaderAccent,
			@SideNavigationBackground,
			@SideNavigationSelection,
			@SideNavigationCollapseExpandButton,
			@SideNavigationText,
			@ContentHeader,
			@ContentIconButtonTab,
			@ContentLinkSortedColumn,
			@ContentTooltip,
			@ContentTooltipText,
			@ContentText,
			@ContentRaisedButtonText,
			@ContentFlatButtonText,
			@ContentProgressBarPast,
			@ContentProgressBarCurrent,
			@PageHeaderSaveButtonText,
			@PageHeaderSaveButtonBackground)
	END
END
