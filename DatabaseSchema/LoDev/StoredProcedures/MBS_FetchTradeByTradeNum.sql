-- =============================================
-- Author:		Jhairo Erazo
-- Create date: 1/16/14
-- Description:	Fetch trades by trade num
-- =============================================
CREATE PROCEDURE [dbo].[MBS_FetchTradeByTradeNum] 
	-- Add the parameters for the stored procedure here
	@BrokerId uniqueidentifier = null, 
	@TradeNum int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	IF (@BrokerId IS NOT NULL) BEGIN
		SELECT *
		FROM MBS_TRADE
		WHERE BrokerId = @BrokerId AND
			  TradeNum = @TradeNum
	END ELSE BEGIN
		SELECT *
		FROM MBS_TRADE
		WHERE TradeNum = @TradeNum
	END
END
