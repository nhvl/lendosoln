ALTER PROCEDURE [dbo].[ListReportQueryDetails]
	@BrokerID uniqueidentifier 

AS

	SELECT
		r.QueryId, 
		r.BrokerId,
		r.EmployeeId,
		r.QueryName 
	FROM
		Report_Query AS r 
		INNER JOIN VIEW_ACTIVE_LO_USER v on r.employeeid = v.employeeid and v.brokerid = @BrokerId

	WHERE
		r.BrokerId = @BrokerID
	