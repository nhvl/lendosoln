
CREATE PROCEDURE [dbo].[DeleteBranch]
	@BranchID uniqueidentifier
AS
	declare @tranPoint sysname
	set @tranPoint = object_name(@@procId) + CAST(@@nestlevel as varchar(10))
	begin transaction
	save transaction @tranPoint
	
	DELETE FROM DOCUMENT_VENDOR_BRANCH_CREDENTIALS WHERE BRANCHID = @BranchID
	DELETE FROM MORTGAGE_INSURANCE_VENDOR_BRANCH_SETTINGS WHERE BRANCHID = @BranchID

	DELETE FROM GROUP_x_BRANCH WHERE BranchID = @BranchID
	if(0!=@@error)
	begin
		RAISERROR('Error delete from group_x_branch in DeleteBranch sp', 16, 1);
		goto ErrorHandler;
	end

	DELETE FROM Branch WHERE BranchID = @BranchID
	if(0!=@@error)
	begin
		RAISERROR('Error deleting branch in DeleteBranch sp', 16, 1);
		goto ErrorHandler;
	end

commit transaction
return 0;

ErrorHandler:
	rollback transaction @tranPoint
	rollback transaction
	return -100;