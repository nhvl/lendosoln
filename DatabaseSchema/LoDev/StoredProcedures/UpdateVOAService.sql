-- =============================================
-- Author:		Eric Mallare
-- Create date: 5/2/2017
-- Description:	Updates a VOA Service
-- =============================================
ALTER PROCEDURE [dbo].[UpdateVOAService]
	@ServiceT int,
	@IsADirectVendor bit,
	@SellsViaResellers bit,
	@IsAReseller bit,
	@AskForNotificationEmail bit,
	@VoaVerificationT int,
	@VendorId int,
	@OptionsFileDbKey uniqueidentifier,
	@IsEnabled bit,
	@VendorServiceId int
AS
BEGIN
	UPDATE VOA_SERVICE
	SET
		ServiceT=@ServiceT, IsADirectVendor=@IsADirectVendor, SellsViaResellers=@SellsViaResellers, IsAReseller=@IsAReseller, 
		AskForNotificationEmail=@AskForNotificationEmail, VoaVerificationT=@VoaVerificationT, VendorId=@VendorId,
		OptionsFileDbKey=@OptionsFileDbKey, IsEnabled=@IsEnabled
	WHERE
		VendorServiceId=@VendorServiceId

	IF @@error!= 0 GOTO HANDLE_ERROR
END
RETURN;
	HANDLE_ERROR:
    	RAISERROR('Error in UpdateVOAService sp', 16, 1);;
    	RETURN;

