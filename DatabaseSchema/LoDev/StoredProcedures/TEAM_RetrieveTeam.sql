

CREATE PROCEDURE [dbo].[TEAM_RetrieveTeam] 
	@BrokerId		uniqueidentifier,
	@TeamId			uniqueidentifier
AS
	SELECT 
		Id
		, RoleId
		, Name 
		, Notes
	FROM TEAM
	WHERE
	BrokerId = @BrokerId
	AND
	Id = @TeamId
	
