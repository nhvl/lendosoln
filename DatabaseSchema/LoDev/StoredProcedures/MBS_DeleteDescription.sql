create proc [dbo].MBS_DeleteDescription
	@DescriptionId uniqueidentifier
as
delete
from MBS_DESCRIPTION
where DescriptionId = @DescriptionId