ALTER  PROCEDURE [dbo].[CreateRolodex]
    @AgentID uniqueidentifier OUT,
	@BrokerID uniqueidentifier,
	@AgentType int,
	@AgentTypeOtherDescription varchar(50) = '',
	@AgentNm varchar(100),
	@AgentTitle varchar(21),
	@AgentPhone varchar(21),
	@AgentAltPhone varchar(21),
	@AgentFax varchar(21),
	@AgentEmail varchar(80),
	@AgentComNm varchar(100),
	@AgentAddr varchar(36),
	@AgentCity varchar(36),
	@AgentState varchar(2),
	@AgentZip char(5),
	@AgentCounty varchar(36) = '',
	@AgentN varchar(500),
	@AgentDepartmentName varchar(100),
	@AgentPager varchar(21),
	@AgentLicenseNumber varchar(30),
	@CompanyLicenseNumber varchar(30),
	@PhoneOfCompany varchar(21),
	@FaxOfCompany varchar(21),
	@IsNotifyWhenLoanStatusChange bit,
	@AgentWebsiteUrl varchar(200),
	@CommissionPointOfLoanAmount decimal(9,3),
	@CommissionPointOfGrossProfit decimal(9,3),
	@CommissionMinBase decimal(9,2),
	@LicenseXmlContent text = '',
	@CompanyId varchar(25),
	@AgentBranchName varchar(100),
	@AgentPayToBankName varchar(50),
	@AgentPayToBankCityState varchar(50),
	@AgentPayToABANumber varchar(9),
	@AgentPayToAccountName varchar(50),
	@AgentPayToAccountNumber varchar(50),
	@AgentFurtherCreditToAccountName varchar(50),
	@AgentFurtherCreditToAccountNumber varchar(50),
	@AgentIsApproved bit = 1,
	@AgentIsAffiliated bit = 0,
	@OverrideLicenses bit = 0,
	@SystemId int = 0,
	@IsSettlementServiceProvider bit = 0
AS

IF EXISTS (SELECT AgentId FROM AGENT WHERE BrokerId = @BrokerId AND SystemId = @SystemId)
BEGIN
	RAISERROR('Cannot insert duplicate system ID into Agent table', 16, 1);
	RETURN -100;
END

SET @AgentID = newid()
INSERT INTO Agent(CompanyId, AgentID, 	BrokerID, AgentType, AgentTypeOtherDescription, AgentNm, AgentTitle, AgentPhone, AgentAltPhone, AgentFax, AgentEmail, AgentComNm, AgentAddr, AgentCity, AgentState, AgentZip, AgentCounty, AgentN, AgentDepartmentName, AgentPager, AgentLicenseNumber, CompanyLicenseNumber, PhoneOfCompany, FaxOfCompany, IsNotifyWhenLoanStatusChange, AgentWebsiteUrl, CommissionPointOfLoanAmount, CommissionPointOfGrossProfit, CommissionMinBase, LicenseXmlContent, AgentBranchName, AgentPayToBankName, AgentPayToBankCityState, AgentPayToABANumber, AgentPayToAccountName, AgentPayToAccountNumber, AgentFurtherCreditToAccountName, AgentFurtherCreditToAccountNumber, AgentIsApproved, AgentIsAffiliated, OverrideLicenses, SystemId, IsSettlementServiceProvider)
           VALUES(@CompanyId, @AgentID, @BrokerID, @AgentType, @AgentTypeOtherDescription, @AgentNm, @AgentTitle, @AgentPhone, @AgentAltPhone, @AgentFax, @AgentEmail, @AgentComNm, @AgentAddr, @AgentCity, @AgentState, @AgentZip, @AgentCounty, @AgentN, @AgentDepartmentName, @AgentPager, @AgentLicenseNumber, @CompanyLicenseNumber, @PhoneOfCompany, @FaxOfCompany, @IsNotifyWhenLoanStatusChange, @AgentWebsiteUrl, @CommissionPointOfLoanAmount, @CommissionPointOfGrossProfit, @CommissionMinBase, @LicenseXmlContent, @AgentBranchName, @AgentPayToBankName, @AgentPayToBankCityState, @AgentPayToABANumber, @AgentPayToAccountName, @AgentPayToAccountNumber, @AgentFurtherCreditToAccountName, @AgentFurtherCreditToAccountNumber, @AgentIsApproved, @AgentIsAffiliated, @OverrideLicenses, @SystemId, @IsSettlementServiceProvider)
IF ( 0 != @@ERROR )
BEGIN
	RAISERROR('Error in inserting into Agent table in CreateRolodex sp', 16, 1);
	RETURN -100;
END
RETURN 0;
