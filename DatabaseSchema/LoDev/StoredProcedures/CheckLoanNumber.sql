CREATE  PROCEDURE CheckLoanNumber
	@BrokerID Guid = NULL,
	@LoanID Guid,
	@LoanNumber varchar(36)
AS
-- TODO: Remove COALESCE once the code on demo and production is update with the latest version. The
-- COALESCE allow older code to work as before. 10/21/02 dd
SELECT count(sLNm) 
FROM Loan_File_cache with(nolock)
WHERE sBrokerID = COALESCE(@BrokerID, sBrokerID) 
     AND sLNm = @LoanNumber
     AND sLId <> @LoanID
if(0!=@@error)
begin
	RAISERROR('Error in the select statement in CheckLoanNumber sp', 16, 1);
	return -100;
end
return 0
