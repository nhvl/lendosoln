-- =============================================
-- Author:		Antonio Valencia
-- Create date: 7/26/2012
-- Description:	Gets the existing released automation templates.
-- =============================================
CREATE PROCEDURE [dbo].[CC_TEMPLATE_GetPreviousReleasesListingInfo]
	@BrokerId uniqueidentifier
AS
BEGIN
	SELECT Id, [Description], DraftOpenedD, ReleasedD 
	FROM CC_TEMPLATE_SYSTEM Where BrokerId = @BrokerId and ReleasedD IS NOT NULL Order BY ReleasedD DESC
END