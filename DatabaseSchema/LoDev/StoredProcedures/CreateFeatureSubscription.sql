


CREATE    PROCEDURE [dbo].[CreateFeatureSubscription]
	@BrokerID uniqueidentifier,
	@FeatureID uniqueidentifier,
	@IsActive bit
AS
	
	INSERT INTO Feature_Subscription(BrokerId, FeatureId, IsActive)
	VALUES (@BrokerID, @FeatureID, @IsActive)
	IF 0 != @@ERROR
	BEGIN
		RAISERROR('Error inserting into Feature_Subscription in CreateFeatureSubscription sp', 16, 1);
		RETURN -100
	END
	RETURN 0;