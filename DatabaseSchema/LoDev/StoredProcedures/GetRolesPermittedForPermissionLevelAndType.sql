-- =============================================
-- Author: Scott Kibler
-- Create date: 5/9/2017
-- Description: 
--	Gets the role ids for a particular permission level and type.
-- =============================================
ALTER PROCEDURE [dbo].[GetRolesPermittedForPermissionLevelAndType]
	@BrokerId uniqueidentifier,
	@PermissionLevelId INT,
	@PermissionType INT
AS
BEGIN

	SELECT RoleId
	FROM CONVOLOG_PERMISSION_ROLE
	WHERE 
			BrokerId = @BrokerId
		AND PermissionLevelId = @PermissionLevelId
		AND PermissionType = @PermissionType	
END