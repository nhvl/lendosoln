

CREATE   PROCEDURE [dbo].[UpdateLoginFailureCount]
	@LoginNm varchar(36),
	@Type char(1),
	@BrokerPmlSiteId uniqueidentifier = '00000000-0000-0000-0000-000000000000'
AS
	UPDATE All_User
	SET LoginFailureCount = LoginFailureCount + 1
	WHERE LoginNm = @LoginNm AND Type = @Type AND BrokerPmlSiteId = @BrokerPmlSiteId
	
	SELECT LoginFailureCount FROM All_User
	WHERE LoginNm = @LoginNm AND Type = @Type AND BrokerPmlSiteId = @BrokerPmlSiteId
