-- =============================================
-- Author:		David Dao
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE dbo.ALL_USER_SHARED_DB_CheckUniqueness 
    @LoginNm varchar(50),
    @ExcludeUserId UniqueIdentifier
AS
BEGIN

    SELECT COUNT(LoginNm) AS Hits FROM ALL_USER_SHARED_DB 
    WHERE LoginNm=@LoginNm AND UserId <> @ExcludeUserId
   

END
