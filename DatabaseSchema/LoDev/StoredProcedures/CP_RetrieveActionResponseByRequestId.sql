





CREATE PROCEDURE [dbo].[CP_RetrieveActionResponseByRequestId]
	@ConsumerActionRequestId bigint
AS

SELECT 
ConsumerActionRequestId
--, ArchivedEDocId
, ConsumerCompletedD
--, RequestorAcceptedD
, ReceivingFaxNumber
, BorrowerSignedEventId
, CoborrowerSignedEventId

FROM
CP_Consumer_Action_Response

WHERE ConsumerActionRequestId = @ConsumerActionRequestId




