-- =============================================
-- Author:		Justin Jia
-- ALTER date: June 14, 2012
-- Description:	Check if user has nuke the loan file to ready it for update(For loan-replicate tool)
-- =============================================
CREATE PROCEDURE [dbo].[Justin_Testing_CheckIfTableEligibleForImport]
	-- Add the parameters for the stored procedure here
	@LoanId Guid 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select aBFirstNm, aBLastNm from APPLICATION_A WHERE aBFirstNm = 'nuke' and aBLastNm = 'nuke' and slid = @LoanId
	
	select sLNm from LOAN_FILE_E where sLNm like '@_@ NUKE THIS LOAN%' and slid = @LoanId
END
