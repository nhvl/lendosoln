CREATE PROCEDURE [dbo].[Billing_GenericFramework_CoreLogic_LoanSafe]
	@StartDate datetime,
	@EndDate datetime
AS
BEGIN
-- dd 2018-11-02 - For use in automate billing executable.

SELECT Distinct gfb.ProviderId, gfb.ServiceType, BrokerNm AS LenderName, CustomerCode, sLNm As LoanNumber, gfb.LoanId
                                                FROM Generic_Framework_Billing gfb WITH (nolock)
                                                JOIN Broker WITH (nolock) ON Broker.BrokerId = gfb.BrokerId
                                                JOIN Loan_File_Cache cache WITH (nolock) ON cache.sLId = gfb.LoanId
                                                WHERE DateUploaded >= @StartDate AND DateUploaded < @EndDate
                                                AND Broker.SuiteType = 1
                                                AND cache.sLoanFileT = 0
                                                AND gfb.ProviderId = 'VEN0105'
                                                AND NOT EXISTS															-- exclude re-orders based on 90-day lookback
                                                (SELECT 1 FROM GENERIC_FRAMEWORK_BILLING gfb_past WITH (nolock)
                                                  WHERE gfb_past.LoanId = gfb.LoanId													-- same loan
                                                  AND gfb_past.ProviderId = gfb.ProviderId											-- same vendor
                                                  AND gfb_past.DateUploaded > DATEADD(DAY, -90, gfb.DateUploaded)					-- occurred within prior 90 days
                                                  AND 
                                                  (
		                                                gfb_past.DateUploaded < gfb.DateUploaded									-- is actually a prior order
		                                                AND NOT (																		-- was not within the same invoice month
		                                                    gfb_past.DateUploaded >= @StartDate AND gfb_past.DateUploaded < @EndDate
		                                                )
                                                  )
                                                )

                                                ORDER BY ProviderId, ServiceType
												
END												