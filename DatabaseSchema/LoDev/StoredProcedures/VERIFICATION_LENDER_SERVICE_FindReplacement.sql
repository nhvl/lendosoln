-- =============================================
-- Author:		Eric Mallare
-- Create date: 5/18/2017
-- Description:	Finds a lender service that is using the specified vendor and reseller combo
-- =============================================
ALTER PROCEDURE [dbo].[VERIFICATION_LENDER_SERVICE_FindReplacement]
	@BrokerId uniqueidentifier,
	@VendorId int,
	@ResellerId int
AS
BEGIN
	SELECT
		LenderServiceId, DisplayName
	FROM
		VERIFICATION_LENDER_SERVICE
	WHERE	
		BrokerId=@BrokerId AND
		VendorId=@VendorId AND
		ResellerId=@ResellerId
END
