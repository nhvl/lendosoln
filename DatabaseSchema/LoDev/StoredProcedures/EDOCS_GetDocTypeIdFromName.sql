
-- =============================================
-- Author:		Antonio Valencia
-- Create date: August 16, 2011
-- Description:	Gets Doc Type By Name
-- =============================================
CREATE PROCEDURE [dbo].[EDOCS_GetDocTypeIdFromName] 
	-- Add the parameters for the stored procedure here
	@BrokerId uniqueidentifier, 
	@Name varchar(50)
AS
BEGIN
	select DocTypeId, IsValid from edocs_document_type
	where brokerid = @BrokerId and
	@Name =DocTypeName
END

