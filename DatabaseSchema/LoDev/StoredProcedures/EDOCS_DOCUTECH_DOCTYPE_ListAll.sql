-- =============================================
-- Author:		David Dao
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE dbo.EDOCS_DOCUTECH_DOCTYPE_ListAll

AS
BEGIN

    SELECT Id, Description, BarcodeClassification
    FROM EDOCS_DOCUTECH_DOCTYPE

END
