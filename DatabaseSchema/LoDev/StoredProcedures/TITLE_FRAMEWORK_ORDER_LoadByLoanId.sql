-- =============================================
-- Author:		Justin Lara
-- Create date: 8/21/2017
-- Description:	Loads all Title orders associated
--				with the given loan.
-- =============================================
ALTER PROCEDURE [dbo].[TITLE_FRAMEWORK_ORDER_LoadByLoanId]
	@BrokerId uniqueidentifier,
	@LoanId uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT
		PublicId,
		OrderId,
		BrokerId,
		LoanId,
		TransactionId,
		OrderNumber,
		[Status],
		OrderedBy,
		OrderedDate,
		IsTestOrder,
		ResultsFileDbKey,
		FeesFileDbKey,
		ConfigFileDbKey,
		LenderServiceId,
		LenderServiceName,
		TitleProviderCode,
		TitleProviderName,
		TitleProviderRolodexId,
		ProductType,
		AppliedDate
	FROM TITLE_FRAMEWORK_ORDER
	WHERE
		LoanId = @LoanId
		AND BrokerId = @BrokerId
END
