ALTER PROCEDURE [dbo].[ListColumnsInLoanFileCacheTables]
AS
BEGIN

SELECT column_name, data_type, table_name, character_maximum_length, numeric_precision, numeric_scale 
FROM INFORMATION_SCHEMA.COLUMNS 
WHERE table_name IN ('LOAN_FILE_CACHE', 'LOAN_FILE_CACHE_2', 'LOAN_FILE_CACHE_3', 'LOAN_FILE_CACHE_4')
and column_name not in ('aBSsn','aCSsn')
END