﻿-- =============================================
-- Author:		Geoff Feltman
-- Create date: 9/16/16
-- Description:	Retrieve multiple specified condition categories with a single query.
-- =============================================
ALTER PROCEDURE [dbo].[TASK_Categories_FetchMultiple]
	@BrokerId uniqueidentifier, 
	@IdXml xml
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT ConditionCategoryId,BrokerId,Category,DefaultTaskPermissionLevelId,IsDisplayAtTop 
	FROM @IdXml.nodes('root/id') as T(Item)
		JOIN CONDITION_CATEGORY ON T.Item.value('.', 'int') = ConditionCategoryId 
	WHERE BrokerId = @BrokerId
END
