-- =============================================
-- Author:		Antonio Valencia
-- Create date: 12/17/13
-- Description: Fetches generic edocs
-- =============================================
ALTER PROCEDURE [dbo].[EDOCS_Generic_Fetch] 
	-- Add the parameters for the stored procedure here
	@DocumentId uniqueidentifier = null,
	@sLId uniqueidentifier = null,
	@BrokerId uniqueidentifier,
	@IsValid bit = 1

AS
BEGIN
	SELECT
		p.folderid,
		p.doctypename, 
		a.aBFirstNm,
		a.aBLastNm,
		a.aCFirstNm,
		a.aCLastNm,
		e.FileNm,
		e.DocumentId,
		e.sLId,
		e.BrokerId,
		e.aAppId,
		e.DocTypeId,
		Description,
		e.InternalComments,
		e.CreatedD,
		e.CreatedByUserId,
		e.FileDBKey,
		e.IsValid,
		f.FolderName,
		f.FolderId, 
		e.FileType,
		COALESCE((SELECT CAST(roleid as varchar(MAX)) +':' + CAST(ispmluser as varchar(MAX)) + ',' 		FROM edocs_folder_x_role as f WHERE f.folderid = p.folderid 	FOR XML PATH ('')	),'')  as FolderRoles
	FROM edocs_generic as e
		JOIN EDOCS_DOCUMENT_TYPE as p on e.doctypeid = p.doctypeid
		JOIN EDOCS_FOLDER f on f.folderid = p.folderid
		JOIN application_a as a on a.aAppId = e.aappid
	WHERE e.slid = coalesce(@slid, e.slid) 
		AND e.brokerid = @brokerid
		AND Documentid = COALESCE(@DocumentId, DOcumentId) 
		AND e.IsValid = @IsValid
END