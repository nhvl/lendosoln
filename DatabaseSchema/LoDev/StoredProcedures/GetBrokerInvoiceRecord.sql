CREATE PROCEDURE GetBrokerInvoiceRecord
	@BrokerId Guid
AS
	SELECT b.BrokerId , b.BrokerNm AS BrokerNm , b.PricePerSeat , b.InvoicesXmlContent AS InvoiceRecord FROM Broker AS b
	WHERE
		BrokerId = @BrokerId
	if( 0!=@@error)
	begin
		RAISERROR('Error in the selection statement in GetBrokerInvoiceRecord sp', 16, 1);
		return -100;
	end
	return 0;
