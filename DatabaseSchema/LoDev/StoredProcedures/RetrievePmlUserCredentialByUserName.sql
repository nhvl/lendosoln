



CREATE   PROCEDURE [dbo].[RetrievePmlUserCredentialByUserName]
	@LoginName varchar(36),
	@CustomerCode varchar(50)
AS
SELECT v.BrokerId, v.UserId, v.EmployeeId, v.Permissions, v.BrokerNm, v.UserFirstNm, v.UserLastNm, 
               v.NeedToAcceptLatestAgreement, v.LoginNm, v.IsSharable, v.PasswordExpirationD, v.LoginSessionID, v.BranchId,
               v.IsOthersAllowedToEditUnderwriterAssignedFile, v.IsLOAllowedToEditProcessorAssignedFile,
               v.HasLONIntegration, v.NHCKey, v.SelectedPipelineCustomReportId, v.LastUsedCreditLoginNm, '' as LastUsedCreditAccountId, v.ByPassBgCalcForGfeAsDefault,v.LastUsedCreditProtocolId, v.Type,v.LpePriceGroupId,
	v.IsRateLockedAtSubmission,v.IsOnlyAccountantCanModifyTrustAccount,v.HasLenderDefaultFeatures
, v.IsQuickPricerEnable,b.IsQuickPricerEnabled AS IsUserQuickPricerEnabled, b.IsPricingMultipleAppsSupported , v.BillingVersion, b.PmlBrokerId, b.PmlLevelAccess
, b.IsNewPmlUIEnabled, b.IsUsePml2AsQuickPricer, b.PortalMode, v.MiniCorrespondentBranchId, v.CorrespondentBranchId
, PasswordHash, PasswordSalt, IsUpdatedPassword
, b.OptsToUseNewLoanEditorUI
FROM VIEW_ACTIVE_PML_USER_WITH_BROKER_INFO v JOIN Broker_User b WITH(NOLOCK) ON v.UserId = b.UserId
WHERE v.LoginNm = @LoginName
      AND v.CustomerCode = @CustomerCode