-- =============================================
-- Author:		Antonio Valencia
-- Create date: 10/28/2013
-- Description:	Creates Document Share Request
-- =============================================
CREATE PROCEDURE [dbo].[CP_ShareDocument_MarkAsDownloaded]
	@BrokerId uniqueidentifier,
	@Id bigint
AS
BEGIN
	SET NOCOUNT ON;
	
	UPDATE CP_Share_Document 
	SET [DownloadedOn] = GETDATE(),
		[ShareStatusT] = 2
	WHERE @BrokerId = BrokerId   AND @Id = Id
	
	SELECT @@rowcount
END
