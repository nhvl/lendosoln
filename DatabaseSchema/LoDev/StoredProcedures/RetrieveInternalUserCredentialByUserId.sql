CREATE PROCEDURE RetrieveInternalUserCredentialByUserId 
	@UserId Guid
AS
SELECT UserId, LoginNm, LoginSessionId, Permissions, FirstNm, LastNm 
FROM VIEW_ACTIVE_INTERNAL_USER
WHERE UserId = @UserId
