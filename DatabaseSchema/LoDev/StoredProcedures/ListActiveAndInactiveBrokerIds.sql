-- =============================================
-- Author:		Scott Kibler
-- Create date: 7/21/2017
-- Description:	Retrieves a list of brokerIds.  
--   For only active ones, see ListBrokerIds.
--  Be careful with this.  Since there are multiple db's, you want a hashset of the results.
--  For each brokerid you get, you want to grab the connection from the shared db connection table.
-- =============================================

ALTER PROCEDURE [dbo].[ListActiveAndInactiveBrokerIds]
AS
	SELECT BrokerId
	FROM Broker

