CREATE PROCEDURE RetrieveEmployeeRole
	@EmployeeID Guid
AS
	SELECT RoleDesc 
	FROM Role r JOIN Role_Assignment ra ON r.roleid = ra.roleid
	WHERE ra.EmployeeID = @EmployeeID
