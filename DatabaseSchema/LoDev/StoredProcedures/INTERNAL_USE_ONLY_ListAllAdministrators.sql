
CREATE PROCEDURE [dbo].[INTERNAL_USE_ONLY_ListAllAdministrators]
AS
	SELECT 
	v.RoleDesc
	, v.RoleId
	, v.BrokerId
	, v.EmployeeId
FROM 
	View_Employee_Role v
WHERE 
	v.RoleDesc = 'Administrator' 