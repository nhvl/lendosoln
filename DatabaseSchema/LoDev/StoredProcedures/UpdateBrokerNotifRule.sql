CREATE PROCEDURE UpdateBrokerNotifRule
	@BrokerId Guid , @NotifRuleXmlContent Text
AS
	UPDATE
		Broker
	SET
		NotifRuleXmlContent = @NotifRuleXmlContent
	WHERE
		BrokerId = @BrokerId
	IF( 0!=@@error)
	BEGIN
		RAISERROR('Error in updating Broker table in UpdateBrokerNotifRule sp', 16, 1);
		RETURN -100;
	END
	RETURN 0;
	
