-- =============================================
-- Author:		Matthew Pham
-- Create date: 9/1/11
-- Description:	Delete all transactions for a trade for MBS trades
-- =============================================
CREATE PROCEDURE MBS_DeleteTransactionsByTradeId
	-- Add the parameters for the stored procedure here
	@TradeId uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM MBS_TRANSACTION
	WHERE TradeId = @TradeId
END
