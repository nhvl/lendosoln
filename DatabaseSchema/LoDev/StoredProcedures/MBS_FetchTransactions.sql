-- =============================================
-- Author:		Matthew Pham
-- Create date: 9/1/11
-- Description:	Fetch MBS transactions
-- =============================================
CREATE PROCEDURE [dbo].[MBS_FetchTransactions] 
	-- Add the parameters for the stored procedure here
	@TradeId uniqueidentifier = null,
	@TransactionId uniqueidentifier = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	IF (@TradeId IS NOT NULL) BEGIN
		SELECT *
		FROM MBS_TRANSACTION
		WHERE TradeId=@TradeId AND
			  TransactionId=COALESCE(@TransactionId, TransactionId)
	END ELSE BEGIN
		SELECT *
		FROM MBS_TRANSACTION
		WHERE TransactionId=@TransactionId
	END
	
END