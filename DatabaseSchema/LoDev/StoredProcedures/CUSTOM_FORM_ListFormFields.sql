CREATE PROCEDURE [dbo].CUSTOM_FORM_ListFormFields 
AS
BEGIN
	SELECT FieldId, FieldFriendlyName, CategoryId, IsReviewNeeded, IsBoolean, Description, IsHidden
	FROM CUSTOM_FORM_FIELD_LIST
	ORDER BY FieldFriendlyName
END
