CREATE PROCEDURE FeedSingletonDistributedWithHolderConstrain
	@ObjId varchar(100),
	@SecondsToExpire int,
	@HolderId varchar(100)
as 
-- NOTE THAT THIS STORE PROC MUST OPERATE ATOMICALLY
declare @now DateTime;
set @now = getdate();
--There is one row
update singleton_tracker
set ExpirationTime = DateAdd( second, @SecondsToExpire, @now )
where ObjId = @ObjId and IsExisting = 1 and ( DateDiff( second, @now, ExpirationTime ) >= 0 )
      and  HolderId = @HolderId
