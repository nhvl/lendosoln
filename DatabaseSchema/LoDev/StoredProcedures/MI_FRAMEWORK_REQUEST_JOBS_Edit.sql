-- =============================================
-- Author:		Eric Mallare
-- Create date: 9/5/2018
-- Description:	Updates an MI Framework Request job entry
-- =============================================
ALTER PROCEDURE [dbo].[MI_FRAMEWORK_REQUEST_JOBS_Edit]
	@JobId int,
    @ResultsFileDbId uniqueidentifier = null
AS
BEGIN
	UPDATE 
		MI_FRAMEWORK_REQUEST_JOBS
	SET
		ResultsFileDbId=@ResultsFileDbId
	WHERE
		JobId=@JobId
END