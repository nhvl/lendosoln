ALTER PROCEDURE [dbo].[VOX_ORDER_STATUSES_Insert]
    @OrderId int,
    @StatusCode int,
    @StatusDescription varchar(300) = '',
    @StatusTime datetime = NULL
AS

INSERT INTO VOX_ORDER_STATUSES 
(
    OrderId,
    StatusCode,
    StatusDescription,
    StatusTime
)
VALUES (
    @OrderId,
    @StatusCode,
    @StatusDescription,
    COALESCE(@StatusTime, CAST('1753-1-1' as datetime))
)
