-- =============================================
-- Author:		Antonio Valencia
-- Create date: 12/6/13
-- Description:	Creates new generic docs
-- =============================================
ALTER PROCEDURE [dbo].[EDOCS_GENERIC_Update]
	-- Add the parameters for the stored procedure here
	@DocumentId uniqueidentifier,
	@BrokerId uniqueidentifier,
	@aAppId uniqueidentifier,
	@DocTypeId int,
	@Description varchar(200),
	@InternalComments varchar(500),
	@FileDBKey uniqueidentifier,
	@IsValid bit,
	@UserId uniqueidentifier,
	@FileName varchar(128),
	@FileType int
AS
BEGIN

	UPDATE TOP(1) EDOCS_GENERIC 
	SET 
		DocTypeId = @DocTypeId,
		Description = @Description,
		InternalComments = @InternalComments,
		FileDBKey = COALESCE(@FileDBKey, FileDBKey),
		aAppId = @aAppId, 
		IsValid = @IsValid, 
		FileNm = @FileName,
		FileType = @FileType
	WHERE 
		DocumentId = @DocumentId AND BrokerId = @BrokerId 
END