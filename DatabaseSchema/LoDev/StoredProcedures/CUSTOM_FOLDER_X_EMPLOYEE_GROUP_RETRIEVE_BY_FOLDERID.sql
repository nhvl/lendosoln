CREATE PROCEDURE [dbo].CUSTOM_FOLDER_X_EMPLOYEE_GROUP_RETRIEVE_BY_FOLDERID
	@FolderId int
AS
BEGIN
	SELECT GroupId
	FROM CUSTOM_FOLDER_X_EMPLOYEE_GROUP
	WHERE FolderId = @FolderId
END
