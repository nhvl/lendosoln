-- =============================================
-- Author:		Justin Jia
-- Create date: 8/14/14
-- Description:	Get all table entries for broker export tool.
-- IsEndOfGroup = 2 means to separate current query into one group per row. 0 means there is next query in the same group. 
-- 1 means it is the last query of current group. when n-1>0, it means n-1 rows per group for current query. Only apply when there is no next query in the same group.
-- See Custom export portion for ordering there.
-- =============================================
ALTER PROCEDURE [dbo].[Justin_Testing_Another_GetBrokerBranchUserRelatedTablesAndOthers] 
	-- Add the parameters for the stored procedure here
	@IsByTableName int,
	@UserOption varchar(40),	--0: no user	1: user by branchNm		2: all user in this broker		
	@BrokerId uniqueidentifier = NULL,
	@BranchName	varchar(40) = NULL,
	@InputTableName varchar(100) = NULL,
	@pkey1 uniqueidentifier = NULL,
	@pkey2 uniqueidentifier = NULL,
	@pkey3 uniqueidentifier = NULL,
	@variantKey1 sql_variant = NULL,
	@variantKey2 sql_variant = NULL,
	@variantKey3 sql_variant = NULL,
	@identityKey bigint = 0,
	@intKey1 int = 0,
	@intKey2 int = 0,
	@intKey3 int = 0,
	@bigintKey1 bigint = 0,
	@bigintKey2 bigint = 0,
	@bigintKey3 bigint = 0,
	@datetimeKey1 datetime = NULL,
	@datetimeKey2 datetime = NULL,
	@datetimeKey3 datetime = NULL,
	@varcharKey1 varchar(MAX) = NULL,
	@varcharKey2 varchar(MAX) = NULL,
	@varcharKey3 varchar(MAX) = NULL,
	@varcharKey4 varchar(MAX) = NULL,
	@varcharKey5 varchar(MAX) = NULL
	
AS
BEGIN
	IF ( @IsByTableName = 0 and ( @UserOption  = 'NO_USER' or @UserOption = 'ONE_BRANCH_USERS' or @UserOption = 'ALL_BRANCH_USERS' ) )
	BEGIN
			
		SELECT 'BROKER' as TableName, 0 as IsEndOfGroup, NULL as ClosingCostTitleVendorId, NULL as PmlLoanTemplateId, NULL as QuickPricerTemplateId, NULL as IsQuickPricerEnable, *
		FROM [BROKER] 
		WHERE BrokerId = @BrokerId

		SELECT 'EDOCS_FOLDER' as TableName, 0 as IsEndOfGroup, T0.* FROM [EDOCS_FOLDER] as T0
		WHERE T0.BrokerId = @BrokerId
		
		SELECT 'EDOCS_DOCUMENT_TYPE' as TableName, 0 as IsEndOfGroup, T0.* FROM [EDOCS_DOCUMENT_TYPE] as T0
		WHERE T0.BrokerId = @BrokerId

		SELECT 'TASK_PERMISSION_LEVEL' as TableName, 0 as IsEndOfGroup, T0.* FROM [TASK_PERMISSION_LEVEL] as T0
		WHERE T0.BrokerId = @BrokerId

		SELECT 'LENDER_LOCK_POLICY' as TableName, 0 as IsEndOfGroup, T0.* FROM [LENDER_LOCK_POLICY] as T0
		WHERE T0.BrokerId = @BrokerId

		SELECT 'LPE_PRICE_GROUP' as TableName, 0 as IsEndOfGroup, T0.* FROM [LPE_PRICE_GROUP] as T0
		WHERE T0.BrokerId = @BrokerId
		
		SELECT 'CONDITION_CATEGORY' as TableName, 0 as IsEndOfGroup, T0.* FROM [CONDITION_CATEGORY] as T0
		WHERE T0.BrokerId = @BrokerId

		SELECT 'DOCUMENT_VENDOR_CONFIGURATION' as TableName, 0 as IsEndOfGroup, T0.* FROM [DOCUMENT_VENDOR_CONFIGURATION] as T0
		INNER JOIN [BROKER] as T1 on T1.DocumentVendorId = T0.VendorId
		WHERE T1.BrokerId = @BrokerId

		SELECT 'GROUP_END' as TableName, 1 as IsEndOfGroup
		
		-- Edocs_folder_x_role group
		SELECT 'EDOCS_FOLDER_X_ROLE' as TableName, 0 as IsEndOfGroup, T0.* FROM [EDOCS_FOLDER_X_ROLE] as T0
		INNER JOIN EDOCS_FOLDER AS T1
		ON T1.FOLDERID = T0.FOLDERID
		WHERE T1.BrokerId = @BrokerId
		
		SELECT 'GROUP_END' as TableName, 1 as IsEndOfGroup

		SELECT 'AGENT' as TableName, 0 as IsEndOfGroup, T0.* FROM [AGENT] as T0
		WHERE T0.BrokerId = @BrokerId

		SELECT 'GROUP_END' as TableName, 1 as IsEndOfGroup


		SELECT 'ARM_INDEX' as TableName, 0 as IsEndOfGroup, T0.* FROM [ARM_INDEX] as T0
		INNER JOIN [BROKER] as T1 on T1.BrokerId = T0.BrokerIdGuid
		WHERE T1.BrokerId = @BrokerId

		SELECT 'GROUP_END' as TableName, 1 as IsEndOfGroup

		---Group
		SELECT 'LO_FORM' as TableName, 0 as IsEndOfGroup, T0.* FROM [LO_FORM] as T0
		WHERE T0.BrokerId = @BrokerId
		
		SELECT 'GROUP_END' as TableName, 1 as IsEndOfGroup

		SELECT 'LICENSE' as TableName, 0 as IsEndOfGroup, T0.* FROM [LICENSE] as T0
		WHERE LicenseOwnerBrokerId = @BrokerId

		SELECT 'SECURITY_QUESTION' as TableName, 0 as IsEndOfGroup, T0.* FROM [SECURITY_QUESTION] as T0
		INNER JOIN BRANCH as b on @UserOption<>'NO_USER' and b.BrokerId = @BrokerId AND ( (@UserOption = 'ONE_BRANCH_USERS' and BranchNm = @BranchName) or @UserOption = 'ALL_BRANCH_USERS')
		INNER JOIN EMPLOYEE AS e on e.BranchId = b.BranchId 
		INNER JOIN [BROKER_USER] as T1 on T1.EmployeeId = e.EmployeeId and T1.SecurityQuestion1Id = T0.QuestionId
		
		SELECT 'GROUP_END' as TableName, 1 as IsEndOfGroup
		


		SELECT 'BRANCH' as TableName, 2 as IsEndOfGroup, NULL as ConsumerPortalId, T0.* FROM [BRANCH] as T0
		WHERE T0.BrokerId = @BrokerId

		SELECT 'GROUP_END' as TableName, 1 as IsEndOfGroup


		---Group
		SELECT 'BROKER_WEBSERVICE_APP_CODE' as TableName, 0 as IsEndOfGroup, T0.* FROM [BROKER_WEBSERVICE_APP_CODE] as T0
		WHERE T0.BrokerId = @BrokerId
		
		SELECT 'GROUP_END' as TableName, 1 as IsEndOfGroup


		---Group
		SELECT 'Lender_TPO_LandingPageConfig' as TableName, 0 as IsEndOfGroup, T0.* FROM [Lender_TPO_LandingPageConfig] as T0
		WHERE T0.BrokerId = @BrokerId
		
		SELECT 'GROUP_END' as TableName, 1 as IsEndOfGroup
		

		---Group
		SELECT 'ALL_USER' as TableName, 5 as IsEndOfGroup, T0.* FROM [ALL_USER] as T0
		INNER JOIN BRANCH as b on @UserOption<>'NO_USER' and b.BrokerId = @BrokerId AND ( (@UserOption = 'ONE_BRANCH_USERS' and BranchNm = @BranchName) or @UserOption = 'ALL_BRANCH_USERS')
		INNER JOIN EMPLOYEE AS e on e.BranchId = b.BranchId 
		INNER JOIN [BROKER_USER] as T1 on T1.EmployeeId = e.EmployeeId and T1.UserId = T0.UserId
		
		SELECT 'GROUP_END' as TableName, 1 as IsEndOfGroup
		
		
		---Group
		SELECT 'EMPLOYEE' as TableName, 2 as IsEndOfGroup, NULL as EmployeeUserId, T0.* FROM [EMPLOYEE] as T0
		INNER JOIN BRANCH as b on @UserOption<>'NO_USER' and b.BrokerId = @BrokerId AND ( (@UserOption = 'ONE_BRANCH_USERS' and BranchNm = @BranchName) or @UserOption = 'ALL_BRANCH_USERS')
		AND T0.BranchId = b.BranchId 
		
		SELECT 'GROUP_END' as TableName, 1 as IsEndOfGroup


		---Group
		SELECT 'BROKER_USER' as TableName, 2 as IsEndOfGroup,
		NULL as BlitzDocTicketId, NULL as BlitzDocTicketIdExpiredD,
		NULL as BrokerProcessorEmployeeId,	NULL as JuniorProcessorEmployeeID,	NULL as JuniorUnderwriterEmployeeID,	
		NULL as LenderAccExecEmployeeId,	NULL as LoanOfficerAssistantEmployeeID,	NULL as LockDeskEmployeeId,	
		NULL as ManagerEmployeeId,	NULL as PmlExternalManagerEmployeeId,	NULL as ProcessorEmployeeId,	
		NULL as UnderwriterEmployeeId, T0.* 
		FROM [BROKER_USER] as T0
		INNER JOIN BRANCH as b on @UserOption<>'NO_USER' and b.BrokerId = @BrokerId AND ( (@UserOption = 'ONE_BRANCH_USERS' and BranchNm = @BranchName) or @UserOption = 'ALL_BRANCH_USERS')
		INNER JOIN EMPLOYEE AS e on e.BranchId = b.BranchId and e.EmployeeId = T0.EmployeeId
		
		SELECT 'GROUP_END' as TableName, 1 as IsEndOfGroup
		
		---Group
		SELECT 'BROKER_USER' as TableName, 2 as IsEndOfGroup,
		BlitzDocTicketId, BlitzDocTicketIdExpiredD, T0.UserId
		FROM [BROKER_USER] as T0
		INNER JOIN BRANCH as b on @UserOption<>'NO_USER' and b.BrokerId = @BrokerId AND ( (@UserOption = 'ONE_BRANCH_USERS' and BranchNm = @BranchName) or @UserOption = 'ALL_BRANCH_USERS')
		INNER JOIN EMPLOYEE AS e on e.BranchId = b.BranchId and e.EmployeeId = T0.EmployeeId
		WHERE BlitzDocTicketId IS NOT NULL OR BlitzDocTicketIdExpiredD IS NOT NULL -- to avoid only user id there.
		
		SELECT 'GROUP_END' as TableName, 1 as IsEndOfGroup
		
		
		---Group
		SELECT 'EMPLOYEE' as TableName, 51 as IsEndOfGroup, T0.EmployeeId, T0.EmployeeUserId  FROM [EMPLOYEE] as T0
		INNER JOIN BRANCH as b on @UserOption<>'NO_USER' and b.BrokerId = @BrokerId AND ( (@UserOption = 'ONE_BRANCH_USERS' and BranchNm = @BranchName) or @UserOption = 'ALL_BRANCH_USERS')
		AND T0.BranchId = b.BranchId 
		
		SELECT 'GROUP_END' as TableName, 1 as IsEndOfGroup


		---Group
		SELECT 'REPORT_QUERY' as TableName, 0 as IsEndOfGroup, T0.* FROM [REPORT_QUERY] as T0
		INNER JOIN BRANCH as b on @UserOption<>'NO_USER' and b.BrokerId = @BrokerId AND ( (@UserOption = 'ONE_BRANCH_USERS' and BranchNm = @BranchName) or @UserOption = 'ALL_BRANCH_USERS')
		INNER JOIN EMPLOYEE AS e on e.BranchId = b.BranchId and e.EmployeeId = T0.EmployeeId

		SELECT 'BROKER_XSLT_REPORT_MAP' as TableName, 0 as IsEndOfGroup, T0.* FROM [BROKER_XSLT_REPORT_MAP] as T0
		INNER JOIN BRANCH as b on @UserOption<>'NO_USER' and b.BrokerId = @BrokerId AND ( (@UserOption = 'ONE_BRANCH_USERS' and BranchNm = @BranchName) or @UserOption = 'ALL_BRANCH_USERS')
		INNER JOIN EMPLOYEE AS e on e.BranchId = b.BranchId 
		INNER JOIN [REPORT_QUERY] as T1 on e.EmployeeId = T1.EmployeeId and T1.QueryId = T0.QueryId
		
		SELECT 'GROUP_END' as TableName, 1 as IsEndOfGroup




		---Group
		SELECT 'CC_TEMPLATE' as TableName, 0 as IsEndOfGroup, T0.* FROM [CC_TEMPLATE] as T0
		WHERE T0.BrokerId = @BrokerId
		
		SELECT 'CC_TEMPLATE_SYSTEM' as TableName, 0 as IsEndOfGroup, T0.*,
		'00000000-0000-0000-0000-000000000000' as DraftOpenedByUserId,
		'00000000-0000-0000-0000-000000000000' as ReleasedByUserId
		FROM [CC_TEMPLATE_SYSTEM] as T0
		WHERE T0.BrokerId = @BrokerId
		
		
		SELECT 'GROUP_END' as TableName, 1 as IsEndOfGroup
		
		-- Group
		SELECT 'BROKER_THEME_COLLECTION' as TableName, 0 as IsEndOfGroup, T0.*
		FROM [BROKER_THEME_COLLECTION] AS T0
		WHERE T0.BROKERID = @BrokerId
		SELECT 'GROUP_END' as TableName, 1 as IsEndOfGroup

		-- Group
		SELECT 'BROKER_COLOR_THEME' as TableName, 0 as IsEndOfGroup, T0.* 
		FROM [BROKER_COLOR_THEME] AS T0
			INNER JOIN
				[BROKER_THEME_COLLECTION] AS T1
			ON T0.CollectionId = T1.ThemeCollectionId
		WHERE T1.BROKERID = @BrokerId
		SELECT 'GROUP_END' as TableName, 1 as IsEndOfGroup
			
		
		SELECT 'CONDITION_CHOICE' as TableName, 0 as IsEndOfGroup, T0.* FROM [CONDITION_CHOICE] as T0
		WHERE T0.BrokerId = @BrokerId
		
		
		SELECT 'GROUP_END' as TableName, 1 as IsEndOfGroup



		---Group
		SELECT 'CREDIT_REPORT_ACCOUNT_PROXY' as TableName, 0 as IsEndOfGroup, T0.* FROM [CREDIT_REPORT_ACCOUNT_PROXY] as T0
		WHERE T0.BrokerId = @BrokerId
		
		
		SELECT 'GROUP_END' as TableName, 1 as IsEndOfGroup





		---Group
		SELECT 'DATA_RETRIEVAL_PARTNER' as TableName, 0 as IsEndOfGroup, T0.* FROM [DATA_RETRIEVAL_PARTNER] as T0
		INNER JOIN [DATA_RETRIEVAL_PARTNER_BROKER] as T1 on T1.PartnerId = T0.PartnerId
		WHERE T1.BrokerId = @BrokerId
		
		SELECT 'DATA_RETRIEVAL_PARTNER_BROKER' as TableName, 0 as IsEndOfGroup, T0.* FROM [DATA_RETRIEVAL_PARTNER_BROKER] as T0
		WHERE T0.BrokerId = @BrokerId
		
		
		SELECT 'GROUP_END' as TableName, 1 as IsEndOfGroup


		---Group
		SELECT 'FEATURE' as TableName, 0 as IsEndOfGroup, T0.* FROM [FEATURE] as T0
		INNER JOIN [FEATURE_SUBSCRIPTION] as T1 on T1.FeatureId = T0.FeatureId
		WHERE T1.BrokerId = @BrokerId

		SELECT 'FEATURE_SUBSCRIPTION' as TableName, 0 as IsEndOfGroup, T0.* FROM [FEATURE_SUBSCRIPTION] as T0
		WHERE T0.BrokerId = @BrokerId
		
		
		SELECT 'GROUP_END' as TableName, 1 as IsEndOfGroup




		---Group
		SELECT 'FEE_TYPE' as TableName, 0 as IsEndOfGroup, T0.* FROM [FEE_TYPE] as T0
		WHERE T0.BrokerId = @BrokerId

		
		
		SELECT 'GROUP_END' as TableName, 1 as IsEndOfGroup

		---Group
		SELECT 'GROUP' as TableName, 0 as IsEndOfGroup, T0.* FROM [GROUP] as T0
		WHERE T0.BrokerId = @BrokerId
		
		
		SELECT 'GROUP_END' as TableName, 1 as IsEndOfGroup



		---Group
		SELECT 'INVESTOR_ROLODEX' as TableName, 0 as IsEndOfGroup, T0.* FROM [INVESTOR_ROLODEX] as T0
		WHERE T0.BrokerId = @BrokerId
		
		
		
		
		SELECT 'GROUP_END' as TableName, 1 as IsEndOfGroup


		---Group
		SELECT 'LENDER_LOCK_POLICY_CUSTOM_WARNING_MSGS' as TableName, 0 as IsEndOfGroup, T0.* FROM [LENDER_LOCK_POLICY_CUSTOM_WARNING_MSGS] as T0
		WHERE T0.BrokerId = @BrokerId
		
		
		
		
		SELECT 'GROUP_END' as TableName, 1 as IsEndOfGroup




		---Group
		SELECT 'LENDER_LOCK_POLICY_HOLIDAY' as TableName, 0 as IsEndOfGroup, T0.* FROM [LENDER_LOCK_POLICY_HOLIDAY] as T0
		WHERE T0.BrokerId = @BrokerId
		
		
		SELECT 'GROUP_END' as TableName, 1 as IsEndOfGroup


		---Group
		SELECT 'LOCKDESK_CLOSURE' as TableName, 0 as IsEndOfGroup, T0.* FROM [LOCKDESK_CLOSURE] as T0
		WHERE T0.BrokerId = @BrokerId
		
		
		
		
		SELECT 'GROUP_END' as TableName, 1 as IsEndOfGroup



		---Group
		SELECT 'MBS_TRADE' as TableName, 0 as IsEndOfGroup, T0.* FROM [MBS_TRADE] as T0
		WHERE T0.BrokerId = @BrokerId
		
		SELECT 'MORTGAGE_POOL' as TableName, 0 as IsEndOfGroup, T0.* FROM [MORTGAGE_POOL] as T0
		WHERE T0.BrokerId = @BrokerId
		
		
		SELECT 'GROUP_END' as TableName, 1 as IsEndOfGroup




		---Group
		SELECT 'PML_BROKER' as TableName, 6 as IsEndOfGroup, T0.* FROM [PML_BROKER] as T0
		WHERE T0.BrokerId = @BrokerId
		
		SELECT 'GROUP_END' as TableName, 1 as IsEndOfGroup




		---Group
		SELECT 'RESERVE_STRING_LIST' as TableName, 0 as IsEndOfGroup, T0.* FROM [RESERVE_STRING_LIST] as T0
		WHERE T0.BrokerId = @BrokerId
		
		
		
		
		SELECT 'GROUP_END' as TableName, 1 as IsEndOfGroup




		---Group
		SELECT 'ROLE_DEFAULT_PERMISSIONS' as TableName, 0 as IsEndOfGroup, T0.* FROM [ROLE_DEFAULT_PERMISSIONS] as T0
		WHERE T0.BrokerId = @BrokerId
		
		
		SELECT 'GROUP_END' as TableName, 1 as IsEndOfGroup




		---Group
		SELECT 'RS_EXPIRATION_CUSTOM_WARNING_MSGS' as TableName, 0 as IsEndOfGroup, T0.* FROM [RS_EXPIRATION_CUSTOM_WARNING_MSGS] as T0
		WHERE T0.BrokerId = @BrokerId
		
		
		
		
		SELECT 'GROUP_END' as TableName, 1 as IsEndOfGroup




		---Group
		SELECT 'TEAM' as TableName, 0 as IsEndOfGroup, T0.* FROM [TEAM] as T0
		WHERE T0.BrokerId = @BrokerId
		
		
		
		
		SELECT 'GROUP_END' as TableName, 1 as IsEndOfGroup




		---Group
		SELECT 'WAREHOUSE_LENDER_ROLODEX' as TableName, 0 as IsEndOfGroup, T0.* FROM [WAREHOUSE_LENDER_ROLODEX] as T0
		WHERE T0.BrokerId = @BrokerId
		
		
		
		
		SELECT 'GROUP_END' as TableName, 1 as IsEndOfGroup


		---Group
		SELECT 'GROUP_x_BRANCH' as TableName, 0 as IsEndOfGroup, T0.* FROM [GROUP_x_BRANCH] as T0
		INNER JOIN BRANCH as T1 on T1.BranchId = T0.BranchId
		WHERE T1.BrokerId = @BrokerId
		
		
		---User Related
		SELECT 'GROUP_END' as TableName, 1 as IsEndOfGroup



		---Group
		SELECT 'CONFIG_RELEASED' as TableName, 0 as IsEndOfGroup, '00000000-0000-0000-0000-000000000000' as ModifyingUserId, T0.*
		FROM [CONFIG_RELEASED] as T0
		WHERE T0.OrgId = @BrokerId
		order by T0.releaseDate desc
		
		SELECT 'GROUP_END' as TableName, 1 as IsEndOfGroup


		---Group
		SELECT 'CUSTOM_LETTER' as TableName, 0 as IsEndOfGroup, NULL as OwnerEmployeeId, T0.* FROM [CUSTOM_LETTER] as T0
		WHERE T0.BrokerId = @BrokerId
		
		SELECT 'GROUP_END' as TableName, 1 as IsEndOfGroup
		

		--Group
		SELECT 'PRINT_GROUP' as TableName, 0 as IsEndOfGroup, T0.* FROM [PRINT_GROUP] as T0
		WHERE T0.OwnerId = @BrokerId
		
		SELECT 'GROUP_END' as TableName, 1 as IsEndOfGroup


		---Group
		SELECT 'EMPLOYEE_FAVORITE_REPORTS' as TableName, 0 as IsEndOfGroup, T0.* FROM [EMPLOYEE_FAVORITE_REPORTS] as T0
		INNER JOIN BRANCH as b on @UserOption<>'NO_USER' and b.BrokerId = @BrokerId AND ( (@UserOption = 'ONE_BRANCH_USERS' and BranchNm = @BranchName) or @UserOption = 'ALL_BRANCH_USERS')
		INNER JOIN EMPLOYEE AS e on e.BranchId = b.BranchId and e.EmployeeId = T0.EmployeeId

		SELECT 'GROUP_END' as TableName, 1 as IsEndOfGroup




		---Group
		SELECT 'GROUP_x_EMPLOYEE' as TableName, 0 as IsEndOfGroup, T0.* FROM [GROUP_x_EMPLOYEE] as T0
		INNER JOIN BRANCH as b on @UserOption<>'NO_USER' and b.BrokerId = @BrokerId AND ( (@UserOption = 'ONE_BRANCH_USERS' and BranchNm = @BranchName) or @UserOption = 'ALL_BRANCH_USERS')
		INNER JOIN EMPLOYEE AS e on e.BranchId = b.BranchId and e.EmployeeId = T0.EmployeeId
		
		
		SELECT 'GROUP_END' as TableName, 1 as IsEndOfGroup
		
		--Group
		SELECT 'GROUP_x_PML_BROKER' as TableName, 0 as IsEndOfGroup, T0.* FROM [GROUP_x_PML_BROKER] as T0
		INNER JOIN [Group] AS g on g.GroupId = T0.GroupId and g.brokerid = @BrokerId
		
		
		SELECT 'GROUP_END' as TableName, 1 as IsEndOfGroup




		---Group
		SELECT 'LO_SIGNATURE_TEMPLATE' as TableName, 0 as IsEndOfGroup, T0.* FROM [LO_SIGNATURE_TEMPLATE] as T0
		INNER JOIN BRANCH as b on @UserOption<>'NO_USER' and b.BrokerId = @BrokerId AND ( (@UserOption = 'ONE_BRANCH_USERS' and BranchNm = @BranchName) or @UserOption = 'ALL_BRANCH_USERS')
		INNER JOIN EMPLOYEE AS e on e.BranchId = b.BranchId
		INNER JOIN BROKER_USER AS bu on e.EmployeeId = bu.EmployeeId and bu.UserId = T0.OwnerUserId
		
		SELECT 'GROUP_END' as TableName, 1 as IsEndOfGroup




		---Group
		SELECT 'LOAN_CACHE_UPDATE_REQUEST' as TableName, 0 as IsEndOfGroup, T0.* FROM [LOAN_CACHE_UPDATE_REQUEST] as T0
		INNER JOIN BRANCH as b on @UserOption<>'NO_USER' and b.BrokerId = @BrokerId AND ( (@UserOption = 'ONE_BRANCH_USERS' and BranchNm = @BranchName) or @UserOption = 'ALL_BRANCH_USERS')
		INNER JOIN EMPLOYEE AS e on e.BranchId = b.BranchId
		INNER JOIN BROKER_USER AS bu on e.EmployeeId = bu.EmployeeId and bu.UserId = T0.UserId
		
		SELECT 'GROUP_END' as TableName, 1 as IsEndOfGroup




		---Group
		SELECT 'ROLE_ASSIGNMENT' as TableName, 55 as IsEndOfGroup, T0.* FROM [ROLE_ASSIGNMENT] as T0
		INNER JOIN BRANCH as b on @UserOption<>'NO_USER' and b.BrokerId = @BrokerId AND ( (@UserOption = 'ONE_BRANCH_USERS' and BranchNm = @BranchName) or @UserOption = 'ALL_BRANCH_USERS')
		INNER JOIN EMPLOYEE AS e on e.BranchId = b.BranchId and e.EmployeeId = T0.EmployeeId
		INNER JOIN ALL_USER a on e.EmployeeUserid = a.UserId 
		and  (a.Type <> 'B' or (T0.RoleId <> 'B3785253-D9E8-4A3F-B0EC-D02D6FCF8538' and T0.RoleId <> '274981E4-8C18-4204-B97B-2F537922CCD9' and T0.RoleId <> '89811C63-6A28-429A-BB45-3152449D854E')) -- BrokerProcessor and correspondent roles only applicable to 'P'
		and (a.Type <> 'P' or T0.RoleId IN ('86C86CF3-FEB6-400F-80DC-123363A79605', '6DFC9B0E-11E3-48C1-B410-A1B0B5363B69', 'B3785253-D9E8-4A3F-B0EC-D02D6FCF8538', '274981E4-8C18-4204-B97B-2F537922CCD9', '89811C63-6A28-429A-BB45-3152449D854E')) 
		and (a.Type <> 'I')
		
		SELECT 'GROUP_END' as TableName, 1 as IsEndOfGroup

		---Group
		SELECT 'TASK_TRIGGER_TEMPLATE' as TableName, 0 as IsEndOfGroup, 0 as IsConditionTemplate, NULL as ConditionCategoryId, '00000000-0000-0000-0000-000000000000' as OwnerUserId, T0.* FROM [TASK_TRIGGER_TEMPLATE] as T0
		WHERE T0.BrokerId = @BrokerId
		
		SELECT 'GROUP_END' as TableName, 1 as IsEndOfGroup


		---Group
		SELECT 'TASK_TRIGGER_TEMPLATE' as TableName, 0 as IsEndOfGroup, T0.AutoTaskTemplateId, T0.BrokerId, T0.IsConditionTemplate, T0.ConditionCategoryId, T0.OwnerUserId FROM [TASK_TRIGGER_TEMPLATE] as T0
		WHERE T0.BrokerId = @BrokerId and (@UserOption = 'ALL_BRANCH_USERS' or exists(
			SELECT * FROM BROKER_USER bu2 
			INNER JOIN EMPLOYEE e2 on @UserOption = 'ONE_BRANCH_USERS' and e2.EmployeeId = bu2.EmployeeId and bu2.UserId = T0.OwnerUserId
			INNER JOIN BRANCH b2 on b2.branchId = e2.branchId and b2.BranchNm = @BranchName
		))
		
		SELECT 'GROUP_END' as TableName, 1 as IsEndOfGroup



		---Group
		SELECT 'TEAM_USER_ASSIGNMENT' as TableName, 0 as IsEndOfGroup, T0.* FROM [TEAM_USER_ASSIGNMENT] as T0
		INNER JOIN BRANCH as b on @UserOption<>'NO_USER' and b.BrokerId = @BrokerId AND ( (@UserOption = 'ONE_BRANCH_USERS' and BranchNm = @BranchName) or @UserOption = 'ALL_BRANCH_USERS')
		INNER JOIN EMPLOYEE AS e on e.BranchId = b.BranchId and e.EmployeeId = T0.EmployeeId

		
		SELECT 'GROUP_END' as TableName, 1 as IsEndOfGroup
		---Group
		SELECT 'XSLT_EXPORT_RESULT_STATUS' as TableName, 0 as IsEndOfGroup, T0.* FROM [XSLT_EXPORT_RESULT_STATUS] as T0
		INNER JOIN BRANCH as b on @UserOption<>'NO_USER' and b.BrokerId = @BrokerId AND ( (@UserOption = 'ONE_BRANCH_USERS' and BranchNm = @BranchName) or @UserOption = 'ALL_BRANCH_USERS')
		INNER JOIN EMPLOYEE AS e on e.BranchId = b.BranchId
		INNER JOIN BROKER_USER AS bu on e.EmployeeId = bu.EmployeeId and bu.UserId = T0.UserId

		SELECT 'GROUP_END' as TableName, 1 as IsEndOfGroup


		---Group 
		SELECT 'SERVICE_COMPANY' as TableName, 6 as IsEndOfGroup, T0.* FROM [SERVICE_COMPANY] as T0
		INNER JOIN CreditReportProtocol cp on cp.ServiceComId = T0.ComId and cp.OwnerId = @BrokerId
		
		SELECT 'CreditReportProtocol' as TableName, 0 as IsEndOfGroup, T0.* FROM [CreditReportProtocol] as T0
		WHERE T0.OwnerId = @BrokerId

		SELECT 'GROUP_END' as TableName, 1 as IsEndOfGroup
		
		---Group
		
		SELECT 'LOAN_FILE_A' as TableName, 0 as IsEndOfGroup, T0.* FROM [LOAN_FILE_A] as T0
		WHERE 
		EXISTS (SELECT distinct T2.PmlLoanTemplateId as sLId FROM BROKER as T2
		WHERE T2.BrokerId = @BrokerId AND T0.sLId = T2.PmlLoanTemplateId)
		OR
		EXISTS (SELECT distinct T2.QuickPricerTemplateId FROM BROKER as T2
		WHERE T2.BrokerId = @BrokerId AND T0.sLId = T2.QuickPricerTemplateId)
		OR
		EXISTS (SELECT distinct T2.DefaultLoanTemplateId FROM CONSUMER_PORTAL as T2
		where T2.BrokerId = @BrokerId AND T0.sLId = T2.DefaultLoanTemplateId)

		SELECT 'LOAN_FILE_B' as TableName, 0 as IsEndOfGroup, T0.* FROM [LOAN_FILE_B] as T0
		WHERE 
		EXISTS (SELECT distinct T2.PmlLoanTemplateId as sLId FROM BROKER as T2
		WHERE T2.BrokerId = @BrokerId AND T0.sLId = T2.PmlLoanTemplateId)
		OR
		EXISTS (SELECT distinct T2.QuickPricerTemplateId FROM BROKER as T2
		WHERE T2.BrokerId = @BrokerId AND T0.sLId = T2.QuickPricerTemplateId)
		OR
		EXISTS (SELECT distinct T2.DefaultLoanTemplateId FROM CONSUMER_PORTAL as T2
		where T2.BrokerId = @BrokerId AND T0.sLId = T2.DefaultLoanTemplateId)

		SELECT 'LOAN_FILE_C' as TableName, 0 as IsEndOfGroup, T0.* FROM [LOAN_FILE_C] as T0
		WHERE 
		EXISTS (SELECT distinct T2.PmlLoanTemplateId as sLId FROM BROKER as T2
		WHERE T2.BrokerId = @BrokerId AND T0.sLId = T2.PmlLoanTemplateId)
		OR
		EXISTS (SELECT distinct T2.QuickPricerTemplateId FROM BROKER as T2
		WHERE T2.BrokerId = @BrokerId AND T0.sLId = T2.QuickPricerTemplateId)
		OR
		EXISTS (SELECT distinct T2.DefaultLoanTemplateId FROM CONSUMER_PORTAL as T2
		where T2.BrokerId = @BrokerId AND T0.sLId = T2.DefaultLoanTemplateId)

		SELECT 'LOAN_FILE_D' as TableName, 0 as IsEndOfGroup, T0.* FROM [LOAN_FILE_D] as T0
		WHERE 
		EXISTS (SELECT distinct T2.PmlLoanTemplateId as sLId FROM BROKER as T2
		WHERE T2.BrokerId = @BrokerId AND T0.sLId = T2.PmlLoanTemplateId)
		OR
		EXISTS (SELECT distinct T2.QuickPricerTemplateId FROM BROKER as T2
		WHERE T2.BrokerId = @BrokerId AND T0.sLId = T2.QuickPricerTemplateId)
		OR
		EXISTS (SELECT distinct T2.DefaultLoanTemplateId FROM CONSUMER_PORTAL as T2
		where T2.BrokerId = @BrokerId AND T0.sLId = T2.DefaultLoanTemplateId)

		SELECT 'LOAN_FILE_E' as TableName, 0 as IsEndOfGroup, T0.* FROM [LOAN_FILE_E] as T0
		WHERE 
		EXISTS (SELECT distinct T2.PmlLoanTemplateId as sLId FROM BROKER as T2
		WHERE T2.BrokerId = @BrokerId AND T0.sLId = T2.PmlLoanTemplateId)
		OR
		EXISTS (SELECT distinct T2.QuickPricerTemplateId FROM BROKER as T2
		WHERE T2.BrokerId = @BrokerId AND T0.sLId = T2.QuickPricerTemplateId)
		OR
		EXISTS (SELECT distinct T2.DefaultLoanTemplateId FROM CONSUMER_PORTAL as T2
		where T2.BrokerId = @BrokerId AND T0.sLId = T2.DefaultLoanTemplateId)

		SELECT 'LOAN_FILE_F' as TableName, 0 as IsEndOfGroup, T0.* FROM [LOAN_FILE_F] as T0
		WHERE 
		EXISTS (SELECT distinct T2.PmlLoanTemplateId as sLId FROM BROKER as T2
		WHERE T2.BrokerId = @BrokerId AND T0.sLId = T2.PmlLoanTemplateId)
		OR
		EXISTS (SELECT distinct T2.QuickPricerTemplateId FROM BROKER as T2
		WHERE T2.BrokerId = @BrokerId AND T0.sLId = T2.QuickPricerTemplateId)
		OR
		EXISTS (SELECT distinct T2.DefaultLoanTemplateId FROM CONSUMER_PORTAL as T2
		where T2.BrokerId = @BrokerId AND T0.sLId = T2.DefaultLoanTemplateId)

		SELECT 'LOAN_FILE_CACHE' as TableName, 0 as IsEndOfGroup, T0.* FROM [LOAN_FILE_CACHE] as T0
		WHERE 
		EXISTS (SELECT distinct T2.PmlLoanTemplateId as sLId FROM BROKER as T2
		WHERE T2.BrokerId = @BrokerId AND T0.sLId = T2.PmlLoanTemplateId)
		OR
		EXISTS (SELECT distinct T2.QuickPricerTemplateId FROM BROKER as T2
		WHERE T2.BrokerId = @BrokerId AND T0.sLId = T2.QuickPricerTemplateId)
		OR
		EXISTS (SELECT distinct T2.DefaultLoanTemplateId FROM CONSUMER_PORTAL as T2
		where T2.BrokerId = @BrokerId AND T0.sLId = T2.DefaultLoanTemplateId)

		SELECT 'LOAN_FILE_CACHE_2' as TableName, 0 as IsEndOfGroup, T0.* FROM [LOAN_FILE_CACHE_2] as T0
		WHERE 
		EXISTS (SELECT distinct T2.PmlLoanTemplateId as sLId FROM BROKER as T2
		WHERE T2.BrokerId = @BrokerId AND T0.sLId = T2.PmlLoanTemplateId)
		OR
		EXISTS (SELECT distinct T2.QuickPricerTemplateId FROM BROKER as T2
		WHERE T2.BrokerId = @BrokerId AND T0.sLId = T2.QuickPricerTemplateId)
		OR
		EXISTS (SELECT distinct T2.DefaultLoanTemplateId FROM CONSUMER_PORTAL as T2
		where T2.BrokerId = @BrokerId AND T0.sLId = T2.DefaultLoanTemplateId)

		SELECT 'LOAN_FILE_CACHE_3' as TableName, 0 as IsEndOfGroup, T0.* FROM [LOAN_FILE_CACHE_3] as T0
		WHERE 
		EXISTS (SELECT distinct T2.PmlLoanTemplateId as sLId FROM BROKER as T2
		WHERE T2.BrokerId = @BrokerId AND T0.sLId = T2.PmlLoanTemplateId)
		OR
		EXISTS (SELECT distinct T2.QuickPricerTemplateId FROM BROKER as T2
		WHERE T2.BrokerId = @BrokerId AND T0.sLId = T2.QuickPricerTemplateId)
		OR
		EXISTS (SELECT distinct T2.DefaultLoanTemplateId FROM CONSUMER_PORTAL as T2
		where T2.BrokerId = @BrokerId AND T0.sLId = T2.DefaultLoanTemplateId)
		
		SELECT 'LOAN_FILE_CACHE_4' as TableName, 0 as IsEndOfGroup, T0.* FROM [LOAN_FILE_CACHE_4] as T0
		WHERE 
		EXISTS (SELECT distinct T2.PmlLoanTemplateId as sLId FROM BROKER as T2
		WHERE T2.BrokerId = @BrokerId AND T0.sLId = T2.PmlLoanTemplateId)
		OR
		EXISTS (SELECT distinct T2.QuickPricerTemplateId FROM BROKER as T2
		WHERE T2.BrokerId = @BrokerId AND T0.sLId = T2.QuickPricerTemplateId)
		OR
		EXISTS (SELECT distinct T2.DefaultLoanTemplateId FROM CONSUMER_PORTAL as T2
		where T2.BrokerId = @BrokerId AND T0.sLId = T2.DefaultLoanTemplateId)

		SELECT 'APPLICATION_A' as TableName, 0 as IsEndOfGroup, T0.* FROM [APPLICATION_A] as T0
		WHERE 
		EXISTS (SELECT distinct T2.PmlLoanTemplateId as sLId FROM BROKER as T2
		WHERE T2.BrokerId = @BrokerId AND T0.sLId = T2.PmlLoanTemplateId)
		OR
		EXISTS (SELECT distinct T2.QuickPricerTemplateId FROM BROKER as T2
		WHERE T2.BrokerId = @BrokerId AND T0.sLId = T2.QuickPricerTemplateId)
		OR
		EXISTS (SELECT distinct T2.DefaultLoanTemplateId FROM CONSUMER_PORTAL as T2
		where T2.BrokerId = @BrokerId AND T0.sLId = T2.DefaultLoanTemplateId)

		SELECT 'APPLICATION_B' as TableName, 0 as IsEndOfGroup, T0.* FROM [APPLICATION_B] as T0
		WHERE 
		EXISTS (SELECT distinct T2.PmlLoanTemplateId as sLId FROM BROKER as T2
		WHERE T2.BrokerId = @BrokerId AND T0.sLId = T2.PmlLoanTemplateId)
		OR
		EXISTS (SELECT distinct T2.QuickPricerTemplateId FROM BROKER as T2
		WHERE T2.BrokerId = @BrokerId AND T0.sLId = T2.QuickPricerTemplateId)
		OR
		EXISTS (SELECT distinct T2.DefaultLoanTemplateId FROM CONSUMER_PORTAL as T2
		where T2.BrokerId = @BrokerId AND T0.sLId = T2.DefaultLoanTemplateId)
		
		SELECT 'TRUST_ACCOUNT' as TableName, 0 as IsEndOfGroup, T0.* FROM [TRUST_ACCOUNT] as T0
		WHERE 
		EXISTS (SELECT distinct T2.PmlLoanTemplateId as sLId FROM BROKER as T2
		WHERE T2.BrokerId = @BrokerId AND T0.sLId = T2.PmlLoanTemplateId)
		OR
		EXISTS (SELECT distinct T2.QuickPricerTemplateId FROM BROKER as T2
		WHERE T2.BrokerId = @BrokerId AND T0.sLId = T2.QuickPricerTemplateId)
		OR
		EXISTS (SELECT distinct T2.DefaultLoanTemplateId FROM CONSUMER_PORTAL as T2
		where T2.BrokerId = @BrokerId AND T0.sLId = T2.DefaultLoanTemplateId)

		
		SELECT 'GROUP_END' as TableName, 1 as IsEndOfGroup

		
		--	START GROUP
		SELECT 'REGION_SET' as TableName, 0 as IsEndOfGroup, T0.* FROM [REGION_SET] AS T0
		WHERE T0.BrokerId = @BrokerId		
		
		SELECT 'GROUP_END' as TableName, 1 as IsEndOfGroup
		
		--	START GROUP
		SELECT 'REGION' as TableName, 0 as IsEndOfGroup, T0.* FROM [REGION] AS T0
		WHERE T0.BrokerId = @BrokerId		
		
		SELECT 'GROUP_END' as TableName, 1 as IsEndOfGroup

		--	START GROUP - NOTE: REGION_X_FIPS implicitly DEPENDS ON CityCountyStateZip by FipsCountyCode, but that's not broker specific.
		SELECT 'REGION_X_FIPS' as TableName, 0 as IsEndOfGroup, T0.* FROM [REGION_X_FIPS] AS T0
		WHERE T0.BrokerId = @BrokerId		
		
		SELECT 'GROUP_END' as TableName, 1 as IsEndOfGroup
		
		
		--you must include primary keys in any such query
		SELECT 'BROKER' as TableName, 0 as IsEndOfGroup, BrokerId, CustomerCode, PmlLoanTemplateId, QuickPricerTemplateId, IsQuickPricerEnable, BrokerPmlSiteId FROM [BROKER] 
		WHERE BrokerId = @BrokerId
		
		
		SELECT 'GROUP_END' as TableName, 1 as IsEndOfGroup		
	END
	
	else IF(@IsByTableName = 0 and @UserOption = 'LOAN_TEMPLATES') --Loan Template only to get the id
	BEGIN

		SELECT 'APPLICATION_A' as TableName, 0 as IsEndOfGroup, T0.sLId, T0.aAppId FROM APPLICATION_A as T0
		INNER JOIN LOAN_FILE_E as e on e.sBrokerId = @BrokerId and e.sLId = T0.sLId
		INNER JOIN LOAN_FILE_A as a on a.isTemplate = 1 and a.sLId = T0.sLId
		

	END
	
	else IF(@IsByTableName = 0 and @UserOption = 'MAIN_DB_PRICING') --Pricing Main database
	BEGIN	
		SELECT 'FEE_SERVICE_REVISION' AS TableName, 0 as IsEndOfGroup, * FROM FEE_SERVICE_REVISION
		WHERE BrokerId = @BrokerId
		
		SELECT 'LENDER_LOCK_POLICY' AS TableName, 0 as IsEndOfGroup, * FROM LENDER_LOCK_POLICY
		WHERE BrokerId = @BrokerId
		
		SELECT 'LOCKDESK_CLOSURE' AS TableName, 0 as IsEndOfGroup, * FROM LOCKDESK_CLOSURE
		WHERE BrokerId = @BrokerId
		
		SELECT 'LPE_PRICE_GROUP' AS TableName, 0 as IsEndOfGroup, * FROM LPE_PRICE_GROUP
		WHERE BrokerId = @BrokerId
		
		SELECT 'LPE_PRICE_GROUP_PRODUCT' AS TableName, 0 as IsEndOfGroup, T0.* FROM LPE_PRICE_GROUP_PRODUCT as T0
		INNER JOIN LPE_PRICE_GROUP as T1 ON T1.LpePriceGroupId = T0.LpePriceGroupId and T1.BrokerId = @BrokerId

	END
	
	else IF(@IsByTableName = 0 and @UserOption = 'SYS_TABLES')
	BEGIN

		

		---Group
		

		SELECT 'FEATURE' as TableName, 0 as IsEndOfGroup, T0.* FROM [FEATURE] as T0
		
		SELECT 'SERVICE_COMPANY' as TableName, 6 as IsEndOfGroup, T0.* FROM [SERVICE_COMPANY] as T0
		
		SELECT 'GROUP_END' as TableName, 1 as IsEndOfGroup



	END
	
	else IF ( @IsByTableName = 1 and @UserOption = 'MAIN_DB' )
	BEGIN
		/* CUSTOM EXPORT PORTION
		 
		 The order of the varcharkeys is determined by 
			exec Justin_Testing_GetColumnPropertyByAllTable @Property="IsPrimaryKey"
		 or simply:
		 
		 SELECT distinct isk.column_name, isk.table_name, sc.is_identity
		 FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE as isk
		 inner join sys.columns as sc on sc.name = isk.column_name and sc.object_id = object_id(isk.table_name)
		 WHERE OBJECTPROPERTY(OBJECT_ID(isk.constraint_name), 'isprimarykey') = 1
		 and isk.TABLE_NAME = yourTableName
		
		 TODO: At some point we should replace this with named parameters so we do not need to rely on the ordering being consistent across stages
		*/
		
		IF ( @InputTableName = 'BROKER' )
        BEGIN
			SELECT 'BROKER' as TableName, 0 as IsEndOfGroup, * FROM [BROKER]
			WHERE BrokerId = @varcharKey1
			RETURN 1;
        END
		
        IF ( @InputTableName = 'EDOCS_FOLDER' )
        BEGIN
			SELECT 'EDOCS_FOLDER' as TableName, 1 as IsEndOfGroup, * FROM [EDOCS_FOLDER]
			WHERE FolderId = @varcharKey1
			RETURN 1;
        END

        IF ( @InputTableName = 'EDOCS_DOCUMENT_TYPE' )
        BEGIN
			SELECT 'EDOCS_DOCUMENT_TYPE' as TableName, 0 as IsEndOfGroup, * FROM [EDOCS_DOCUMENT_TYPE]
			WHERE DocTypeId = @varcharKey1
			RETURN 1;
        END
		
		IF ( @InputTableName = 'TASK_PERMISSION_LEVEL' )
        BEGIN
			SELECT 'TASK_PERMISSION_LEVEL' as TableName, 0 as IsEndOfGroup, * FROM [TASK_PERMISSION_LEVEL]
			WHERE TaskPermissionLevelId = @varcharKey1
			RETURN 1;
        END

        IF ( @InputTableName = 'TITLE_VENDOR' )
        BEGIN
			SELECT 'TITLE_VENDOR' as TableName, 0 as IsEndOfGroup, T1.BrokerId, T0.* FROM [TITLE_VENDOR] AS T0
		    INNER JOIN TITLE_VENDOR_X_BROKER AS T1 ON T1.TitleVendorId = T0.TitleVendorId and T0.TitleVendorId = @varcharKey1
			RETURN 1;
        END

		IF ( @InputTableName = 'TITLE_VENDOR_X_BROKER' )
        BEGIN
			SELECT 'TITLE_VENDOR_X_BROKER' as TableName, 0 as IsEndOfGroup, * FROM [TITLE_VENDOR_X_BROKER]
			WHERE BrokerId = @varcharKey1 and TitleVendorId = @varcharKey2
			RETURN 1;
        END

		IF ( @InputTableName = 'TITLE_VENDOR_POLICY' )
        BEGIN
			SELECT 'TITLE_VENDOR_POLICY' as TableName, 0 as IsEndOfGroup, T1.BrokerId, T0.* FROM [TITLE_VENDOR_POLICY] AS T0
		    INNER JOIN TITLE_VENDOR_X_BROKER AS T1 ON T1.TitleVendorId = T0.TitleVendorId 
		    and T0.TitleVendorId = @varcharKey1 and T0.State = @varcharKey2 and T0.PolicyId = @varcharKey3
			RETURN 1;
        END
		
        IF ( @InputTableName = 'LENDER_LOCK_POLICY' )
        BEGIN
			SELECT 'LENDER_LOCK_POLICY' as TableName, 0 as IsEndOfGroup, * FROM [LENDER_LOCK_POLICY]
			WHERE LockPolicyId = @varcharKey1
			RETURN 1;
        END

        IF ( @InputTableName = 'LPE_PRICE_GROUP' )
        BEGIN
			SELECT 'LPE_PRICE_GROUP' as TableName, 0 as IsEndOfGroup, * FROM [LPE_PRICE_GROUP]
			WHERE LpePriceGroupId = @varcharKey1
			RETURN 1;
        END
		
		IF ( @InputTableName = 'CONDITION_CATEGORY' )
        BEGIN
			SELECT 'CONDITION_CATEGORY' as TableName, 0 as IsEndOfGroup, * FROM [CONDITION_CATEGORY]
			WHERE ConditionCategoryId = @varcharKey1
			RETURN 1;
        END
		
		IF ( @InputTableName = 'DOCUMENT_VENDOR_CONFIGURATION' )
        BEGIN
			SELECT 'DOCUMENT_VENDOR_CONFIGURATION' as TableName, 0 as IsEndOfGroup, * FROM [DOCUMENT_VENDOR_CONFIGURATION]
			WHERE VendorId = @varcharKey1
			RETURN 1;
        END
		
		IF ( @InputTableName = 'AGENT' )
        BEGIN
			SELECT 'AGENT' as TableName, 1 as IsEndOfGroup, * FROM [AGENT]
			WHERE AgentId = @varcharKey1
			RETURN 1;
        END
		
		IF ( @InputTableName = 'APPRAISAL_VENDOR_CONFIGURATION' )
        BEGIN
			SELECT 'APPRAISAL_VENDOR_CONFIGURATION' as TableName, 0 as IsEndOfGroup, * FROM [APPRAISAL_VENDOR_CONFIGURATION]
			WHERE VendorId = @varcharKey1
			RETURN 1;
        END

        IF ( @InputTableName = 'APPRAISAL_VENDOR_BROKER_ENABLED' )
        BEGIN
			SELECT 'APPRAISAL_VENDOR_BROKER_ENABLED' as TableName, 0 as IsEndOfGroup, * FROM [APPRAISAL_VENDOR_BROKER_ENABLED]
			WHERE BrokerId = @varcharKey1 and VendorId = @varcharKey2
			RETURN 1;
        END

		IF ( @InputTableName = 'ARM_INDEX' )
        BEGIN
			SELECT 'ARM_INDEX' as TableName, 1 as IsEndOfGroup, BrokerIdGuid as BrokerId, * FROM [ARM_INDEX]
			WHERE IndexIdGuid = @varcharKey1
			RETURN 1;
        END
		

		--
		IF ( @InputTableName = 'SERVICE_COMPANY' )
        BEGIN
			SELECT 'SERVICE_COMPANY' as TableName, 0 as IsEndOfGroup, '00000000-0000-0000-0000-000000000000' as BrokerId, * FROM [SERVICE_COMPANY]
			WHERE ComId = @varcharKey1
			RETURN 1;
        END

        IF ( @InputTableName = 'BRANCH' )
        BEGIN
			SELECT 'BRANCH' as TableName, 0 as IsEndOfGroup, * FROM [BRANCH]
			WHERE BranchId = @varcharKey1
			RETURN 1;
        END
        
        IF ( @InputTableName = 'FEE_SERVICE_REVISION' )
        BEGIN
			SELECT 'FEE_SERVICE_REVISION' as TableName, 0 as IsEndOfGroup, * FROM [FEE_SERVICE_REVISION]
			WHERE Id = @varcharKey1
			RETURN 1;
        END

        IF ( @InputTableName = 'LO_FORM' )
        BEGIN
			SELECT 'LO_FORM' as TableName, 0 as IsEndOfGroup, * FROM [LO_FORM]
			WHERE FormId = @varcharKey1
			RETURN 1;
        END

        IF ( @InputTableName = 'CONSUMER_PORTAL' )
        BEGIN
			SELECT 'CONSUMER_PORTAL' as TableName, 1 as IsEndOfGroup, * FROM [CONSUMER_PORTAL]
			WHERE Id = @varcharKey1
			RETURN 1;
        END

		--
		IF ( @InputTableName = 'BROKER_WEBSERVICE_APP_CODE' )
        BEGIN
			SELECT 'BROKER_WEBSERVICE_APP_CODE' as TableName, 1 as IsEndOfGroup, * FROM [BROKER_WEBSERVICE_APP_CODE]
			WHERE AppCode = @varcharKey1
			RETURN 1;
        END
		
		--
        IF ( @InputTableName = 'REPORT_QUERY' )
        BEGIN
			SELECT 'REPORT_QUERY' as TableName, 0 as IsEndOfGroup, * FROM [REPORT_QUERY]
			WHERE QueryId = @varcharKey1
			RETURN 1;
        END

        IF ( @InputTableName = 'BROKER_XSLT_REPORT_MAP' )
        BEGIN
			SELECT 'BROKER_XSLT_REPORT_MAP' as TableName, 0 as IsEndOfGroup, * FROM [BROKER_XSLT_REPORT_MAP]
			WHERE BrokerId = @varcharKey1 and XsltMapId = @varcharKey2
			RETURN 1;
        END

		--
		IF ( @InputTableName = 'CC_TEMPLATE' )
        BEGIN
			SELECT 'CC_TEMPLATE' as TableName, 0 as IsEndOfGroup, * FROM [CC_TEMPLATE]
			WHERE cCcTemplateId = @varcharKey1
			RETURN 1;
        END

        IF ( @InputTableName = 'CC_TEMPLATE_SYSTEM' )
        BEGIN
			SELECT 'CC_TEMPLATE_SYSTEM' as TableName, 0 as IsEndOfGroup, * FROM [CC_TEMPLATE_SYSTEM]
			WHERE Id = @varcharKey1
			RETURN 1;
        END
		
		--
		IF ( @InputTableName = 'ROLE' )
        BEGIN
			SELECT 'ROLE' as TableName, 0 as IsEndOfGroup, '00000000-0000-0000-0000-000000000000' as BrokerId, * FROM [ROLE]
			WHERE RoleId = @varcharKey1
			RETURN 1;
        END

        IF ( @InputTableName = 'CONDITION_CHOICE' )
        BEGIN
			SELECT 'CONDITION_CHOICE' as TableName, 0 as IsEndOfGroup, * FROM [CONDITION_CHOICE]
			WHERE BrokerId = @varcharKey1 and ConditionChoiceId = @varcharKey2
			RETURN 1;
        END
		
		--
		IF ( @InputTableName = 'CONFIG_DRAFT' )
        BEGIN
			SELECT 'CONFIG_DRAFT' as TableName, 1 as IsEndOfGroup, OrgID as BrokerId, * FROM [CONFIG_DRAFT]
			WHERE OrgID = @varcharKey1
			RETURN 1;
        END
		
		--
        IF ( @InputTableName = 'CONSUMER_PORTAL_USER' )
        BEGIN
			SELECT 'CONSUMER_PORTAL_USER' as TableName, 1 as IsEndOfGroup, * FROM [CONSUMER_PORTAL_USER]
			WHERE Id = @varcharKey1
			RETURN 1;
        END
		
		--
		IF ( @InputTableName = 'CP_CONSUMER' )
        BEGIN
			SELECT 'CP_CONSUMER' as TableName, 1 as IsEndOfGroup, * FROM [CP_CONSUMER]
			WHERE ConsumerId = @varcharKey1
			RETURN 1;
        END
		
		--		
        IF ( @InputTableName = 'CREDIT_REPORT_ACCOUNT_PROXY' )
        BEGIN
			SELECT 'CREDIT_REPORT_ACCOUNT_PROXY' as TableName, 1 as IsEndOfGroup, * FROM [CREDIT_REPORT_ACCOUNT_PROXY]
			WHERE CrAccProxyId = @varcharKey1
			RETURN 1;
        END




		--
        IF ( @InputTableName = 'CreditReportProtocol' )
        BEGIN
			SELECT 'CreditReportProtocol' as TableName, 1 as IsEndOfGroup, OwnerId as BrokerId, * FROM [CreditReportProtocol]
			WHERE OwnerId = @varcharKey1 and ServiceComId = @varcharKey2
			RETURN 1;
        END




		--
        IF ( @InputTableName = 'DATA_RETRIEVAL_PARTNER' )
        BEGIN
			SELECT 'DATA_RETRIEVAL_PARTNER' as TableName, 0 as IsEndOfGroup, * FROM [DATA_RETRIEVAL_PARTNER]
			WHERE PartnerId = @varcharKey1
			RETURN 1;
        END

        IF ( @InputTableName = 'DATA_RETRIEVAL_PARTNER_BROKER' )
        BEGIN
			SELECT 'DATA_RETRIEVAL_PARTNER_BROKER' as TableName, 1 as IsEndOfGroup, * FROM [DATA_RETRIEVAL_PARTNER_BROKER]
			WHERE BrokerId = @varcharKey1 and PartnerId = @varcharKey2
			RETURN 1;
        END



		--
        IF ( @InputTableName = 'DOCMAGIC_ALTERNATE_LENDER' )
        BEGIN
			SELECT 'DOCMAGIC_ALTERNATE_LENDER' as TableName, 1 as IsEndOfGroup, * FROM [DOCMAGIC_ALTERNATE_LENDER]
			WHERE DocMagicAlternateLenderId = @varcharKey1
			RETURN 1;
        END




		--
        IF ( @InputTableName = 'DOCUMENT_VENDOR_BROKER_SETTINGS' )
        BEGIN
			SELECT 'DOCUMENT_VENDOR_BROKER_SETTINGS' as TableName, 1 as IsEndOfGroup, * FROM [DOCUMENT_VENDOR_BROKER_SETTINGS]
			WHERE BrokerId = @varcharKey1 and VendorId = @varcharKey2 and VendorIndex = @varcharKey3
			RETURN 1;
        END




		--
        IF ( @InputTableName = 'EDOCS_SHIPPING_TEMPLATE' )
        BEGIN
			SELECT 'EDOCS_SHIPPING_TEMPLATE' as TableName, 0 as IsEndOfGroup, * FROM [EDOCS_SHIPPING_TEMPLATE]
			WHERE ShippingTemplateId = @varcharKey1
			RETURN 1;
        END


        IF ( @InputTableName = 'EDOCS_DOCMAGIC_DOCTYPE' )
        BEGIN
			SELECT 'EDOCS_DOCMAGIC_DOCTYPE' as TableName, 0 as IsEndOfGroup, * FROM [EDOCS_DOCMAGIC_DOCTYPE]
			WHERE Id = @varcharKey1
			RETURN 1;
        END

        IF ( @InputTableName = 'EDOCS_DOCMAGIC_DOCTYPE_X_DOCTYPE' )
        BEGIN
			SELECT 'EDOCS_DOCMAGIC_DOCTYPE_X_DOCTYPE' as TableName, 1 as IsEndOfGroup, * FROM [EDOCS_DOCMAGIC_DOCTYPE_X_DOCTYPE]
			WHERE DocMagicDocTypeId = @varcharKey1 and DocTypeId = @varcharKey2
			RETURN 1;
        END





        IF ( @InputTableName = 'EDOCS_DOCUTECH_DOCTYPE' )
        BEGIN
			SELECT 'EDOCS_DOCUTECH_DOCTYPE' as TableName, 0 as IsEndOfGroup, * FROM [EDOCS_DOCUTECH_DOCTYPE]
			WHERE Id = @varcharKey1
			RETURN 1;
        END

        IF ( @InputTableName = 'EDOCS_DOCUTECH_DOCTYPE_X_DOCTYPE' )
        BEGIN
			SELECT 'EDOCS_DOCUTECH_DOCTYPE_X_DOCTYPE' as TableName, 1 as IsEndOfGroup, * FROM [EDOCS_DOCUTECH_DOCTYPE_X_DOCTYPE]
			WHERE DocTypeId = @varcharKey1 and DocuTechDocTypeId = @varcharKey2
			RETURN 1;
        END





        IF ( @InputTableName = 'EDOCS_FAX_NUMBER' )
        BEGIN
			SELECT 'EDOCS_FAX_NUMBER' as TableName, 1 as IsEndOfGroup, * FROM [EDOCS_FAX_NUMBER]
			WHERE FaxNumber = @varcharKey1
			RETURN 1;
        END





        IF ( @InputTableName = 'EDOCS_IDS_DOCTYPE' )
        BEGIN
			SELECT 'EDOCS_IDS_DOCTYPE' as TableName, 0 as IsEndOfGroup, * FROM [EDOCS_IDS_DOCTYPE]
			WHERE Id = @varcharKey1
			RETURN 1;
        END

        IF ( @InputTableName = 'EDOCS_IDS_DOCTYPE_X_DOCTYPE' )
        BEGIN
			SELECT 'EDOCS_IDS_DOCTYPE_X_DOCTYPE' as TableName, 1 as IsEndOfGroup, * FROM [EDOCS_IDS_DOCTYPE_X_DOCTYPE]
			WHERE BrokerId = @varcharKey1 and DocTypeId = @varcharKey2 and IDSDocTypeId = @varcharKey3
			RETURN 1;
        END





        IF ( @InputTableName = 'FEATURE' )
        BEGIN
			SELECT 'FEATURE' as TableName, 0 as IsEndOfGroup, * FROM [FEATURE]
			WHERE FeatureId = @varcharKey1
			RETURN 1;
        END

        IF ( @InputTableName = 'FEATURE_SUBSCRIPTION' )
        BEGIN
			SELECT 'FEATURE_SUBSCRIPTION' as TableName, 1 as IsEndOfGroup, * FROM [FEATURE_SUBSCRIPTION]
			WHERE BrokerId = @varcharKey1 and FeatureId = @varcharKey2
			RETURN 1;
        END





        IF ( @InputTableName = 'FEE_TYPE' )
        BEGIN
			SELECT 'FEE_TYPE' as TableName, 1 as IsEndOfGroup, * FROM [FEE_TYPE]
			WHERE FeeTypeId = @varcharKey1
			RETURN 1;
        END


        IF ( @InputTableName = 'GROUP' )
        BEGIN
			SELECT 'GROUP' as TableName, 1 as IsEndOfGroup, * FROM [GROUP]
			WHERE BrokerId = @varcharKey1 and GroupId = @varcharKey2
			RETURN 1;
        END
        

        IF ( @InputTableName = 'INVESTOR_ROLODEX' )
        BEGIN
			SELECT 'INVESTOR_ROLODEX' as TableName, 1 as IsEndOfGroup, * FROM [INVESTOR_ROLODEX]
			WHERE InvestorRolodexId = @varcharKey1
			RETURN 1;
        END





        IF ( @InputTableName = 'IRS_4506T_VENDOR_CONFIGURATION' )
        BEGIN
			SELECT 'IRS_4506T_VENDOR_CONFIGURATION' as TableName, 0 as IsEndOfGroup, * FROM [IRS_4506T_VENDOR_CONFIGURATION]
			WHERE VendorId = @varcharKey1
			RETURN 1;
        END

        IF ( @InputTableName = 'IRS_4506T_VENDOR_BROKER_SETTINGS' )
        BEGIN
			SELECT 'IRS_4506T_VENDOR_BROKER_SETTINGS' as TableName, 1 as IsEndOfGroup, * FROM [IRS_4506T_VENDOR_BROKER_SETTINGS]
			WHERE BrokerId = @varcharKey1 and VendorId = @varcharKey2
			RETURN 1;
        END




        IF ( @InputTableName = 'LENDER_LOCK_POLICY_CUSTOM_WARNING_MSGS' )
        BEGIN
			SELECT 'LENDER_LOCK_POLICY_CUSTOM_WARNING_MSGS' as TableName, 1 as IsEndOfGroup, * FROM [LENDER_LOCK_POLICY_CUSTOM_WARNING_MSGS]
			WHERE BrokerId = @varcharKey1 and LockPolicyId = @varcharKey2
			RETURN 1;
        END





        IF ( @InputTableName = 'LENDER_LOCK_POLICY_HOLIDAY' )
        BEGIN
			SELECT 'LENDER_LOCK_POLICY_HOLIDAY' as TableName, 1 as IsEndOfGroup, * FROM [LENDER_LOCK_POLICY_HOLIDAY]
			WHERE BrokerId = @varcharKey1 and ClosureDate = @varcharKey2 and LockPolicyId = @varcharKey3
			RETURN 1;
        END





        IF ( @InputTableName = 'Lender_TPO_LandingPageConfig' )
        BEGIN
			SELECT 'Lender_TPO_LandingPageConfig' as TableName, 1 as IsEndOfGroup, * FROM [Lender_TPO_LandingPageConfig]
			WHERE Id = @varcharKey1
			RETURN 1;
        END





        IF ( @InputTableName = 'LOCKDESK_CLOSURE' )
        BEGIN
			SELECT 'LOCKDESK_CLOSURE' as TableName, 1 as IsEndOfGroup, * FROM [LOCKDESK_CLOSURE]
			WHERE BrokerId = @varcharKey1 and ClosureDate = @varcharKey2
			RETURN 1;
        END





        IF ( @InputTableName = 'MBS_TRADE' )
        BEGIN
			SELECT 'MBS_TRADE' as TableName, 0 as IsEndOfGroup, * FROM [MBS_TRADE]
			WHERE TradeId = @varcharKey1
			RETURN 1;
        END

        IF ( @InputTableName = 'MORTGAGE_POOL' )
        BEGIN
			SELECT 'MORTGAGE_POOL' as TableName, 1 as IsEndOfGroup, * FROM [MORTGAGE_POOL]
			WHERE PoolId = @varcharKey1
			RETURN 1;
        END






        IF ( @InputTableName = 'PML_BROKER' )
        BEGIN
			SELECT 'PML_BROKER' as TableName, 1 as IsEndOfGroup, * FROM [PML_BROKER]
			WHERE BrokerId = @varcharKey1 and PmlBrokerId = @varcharKey2
			RETURN 1;
        END
		
		
		IF ( @InputTableName = 'BROKER_COLOR_THEME' )
        BEGIN
			SELECT 'BROKER_COLOR_THEME' as TableName, 1 as IsEndOfGroup, * FROM [BROKER_COLOR_THEME]
			WHERE ColorThemeId = @varcharKey1
			RETURN 1;
        END

		IF ( @InputTableName = 'BROKER_THEME_COLLECTION' )
        BEGIN
			SELECT 'BROKER_THEME_COLLECTION' as TableName, 1 as IsEndOfGroup, * FROM [BROKER_THEME_COLLECTION]
			WHERE ThemeCollectionId = @varcharKey1
			RETURN 1;
        END

		IF ( @InputTableName = 'STATUS_EVENT' )
        BEGIN
			SELECT 'STATUS_EVENT' as TableName, 1 as IsEndOfGroup, * FROM [STATUS_EVENT]
			WHERE Id = @varcharKey1
			RETURN 1;
        END


        IF ( @InputTableName = 'RESERVE_STRING_LIST' )
        BEGIN
			SELECT 'RESERVE_STRING_LIST' as TableName, 1 as IsEndOfGroup, * FROM [RESERVE_STRING_LIST]
			WHERE BrokerId = @varcharKey1 and ID = @varcharKey2 and Type = @varcharKey3
			RETURN 1;
        END





        IF ( @InputTableName = 'ROLE_DEFAULT_PERMISSIONS' )
        BEGIN
			SELECT 'ROLE_DEFAULT_PERMISSIONS' as TableName, 1 as IsEndOfGroup, * FROM [ROLE_DEFAULT_PERMISSIONS]
			WHERE BrokerId = @varcharKey1 and RoleId = @varcharKey2
			RETURN 1;
        END





        IF ( @InputTableName = 'RS_EXPIRATION_CUSTOM_WARNING_MSGS' )
        BEGIN
			SELECT 'RS_EXPIRATION_CUSTOM_WARNING_MSGS' as TableName, 1 as IsEndOfGroup, * FROM [RS_EXPIRATION_CUSTOM_WARNING_MSGS]
			WHERE BrokerId = @varcharKey1
			RETURN 1;
        END





        IF ( @InputTableName = 'TEAM' )
        BEGIN
			SELECT 'TEAM' as TableName, 1 as IsEndOfGroup, * FROM [TEAM]
			WHERE Id = @varcharKey1
			RETURN 1;
        END






        IF ( @InputTableName = 'WAREHOUSE_LENDER_ROLODEX' )
        BEGIN
			SELECT 'WAREHOUSE_LENDER_ROLODEX' as TableName, 1 as IsEndOfGroup, * FROM [WAREHOUSE_LENDER_ROLODEX]
			WHERE WarehouseLenderRolodexId = @varcharKey1
			RETURN 1;
        END





        IF ( @InputTableName = 'DOCUMENT_VENDOR_BRANCH_CREDENTIALS' )
        BEGIN
			SELECT 'DOCUMENT_VENDOR_BRANCH_CREDENTIALS' as TableName, 1 as IsEndOfGroup, T1.BrokerId, T0.* FROM [DOCUMENT_VENDOR_BRANCH_CREDENTIALS] T0
		   INNER JOIN BRANCH T1 ON T1.BranchId = T0.BranchId and T0.BranchId = @varcharKey1 and T0.VendorId = @varcharKey2
			RETURN 1;
        END


        IF ( @InputTableName = 'GROUP_x_BRANCH' )
        BEGIN
			SELECT 'GROUP_x_BRANCH' as TableName, 0 as IsEndOfGroup, T1.BrokerId, T0.* FROM [GROUP_x_BRANCH] T0
			INNER JOIN BRANCH T1 ON T1.BranchId = T0.BranchId and T0.BranchId = @varcharKey1 and T0.GroupId = @varcharKey2
			RETURN 1;
        END
        
        IF ( @InputTableName = 'GROUP_x_PML_BROKER' )
        BEGIN
			SELECT 'GROUP_x_PML_BROKER' as TableName, 0 as IsEndOfGroup, T1.BrokerId, T0.* FROM [GROUP_x_PML_BROKER] T0
			INNER JOIN [Group] T1 ON T1.GroupId = T0.GroupId and T0.GroupId = @varcharKey1 and T0.PmlBrokerId = @varcharKey2
			RETURN 1;
        END


		IF ( @InputTableName = 'EMPLOYEE' )
		BEGIN
			SELECT 'EMPLOYEE' as TableName, 0 as IsEndOfGroup, T1.BrokerId, T0.* FROM EMPLOYEE T0
			INNER JOIN BRANCH T1 ON T1.BranchId = T0.BranchId and T0.EmployeeId = @varcharKey1 
			RETURN 1;
		END
		
		IF ( @InputTableName = 'BROKER_USER' )
		BEGIN
			SELECT 'BROKER_USER' as TableName, 0 as IsEndOfGroup, T3.BrokerId, T1.* FROM BROKER_USER T1
			INNER JOIN EMPLOYEE AS T2 ON T2.EmployeeId = T1.EmployeeId and T1.UserId = @varcharKey1
			INNER JOIN BRANCH AS T3 on T3.BranchId = T2.BranchId
			RETURN 1;
		END
		
		IF ( @InputTableName = 'ALL_USER' )
		BEGIN
			SELECT 'ALL_USER' as TableName, 0 as IsEndOfGroup, T3.BrokerId, T0.* FROM ALL_USER T0
			INNER JOIN BROKER_USER AS T1 ON T1.UserId = T0.UserId AND T0.UserId = @varcharKey1
			INNER JOIN EMPLOYEE AS T2 ON T2.EmployeeId = T1.EmployeeId
			INNER JOIN BRANCH AS T3 on T3.BranchId = T2.BranchId
			RETURN 1;
		END
		
		
        IF ( @InputTableName = 'LICENSE' )
        BEGIN
			SELECT 'LICENSE' as TableName, 0 as IsEndOfGroup, LicenseOwnerBrokerId as BrokerId, * FROM [LICENSE]
			WHERE LicenseId = @varcharKey1 and LicenseOwnerBrokerId = @varcharKey2
			RETURN 1;
        END

        IF ( @InputTableName = 'SECURITY_QUESTION' )
        BEGIN
			SELECT 'SECURITY_QUESTION' as TableName, 1 as IsEndOfGroup, '00000000-0000-0000-0000-000000000000' as BrokerId, * FROM [SECURITY_QUESTION]
			WHERE QuestionId = @varcharKey1
			RETURN 1;
        END
		
		IF ( @InputTableName = 'ACTION_EVENT' )
        BEGIN
			SELECT 'ACTION_EVENT' as TableName, 1 as IsEndOfGroup, T3.BrokerId, T0.* FROM [ACTION_EVENT] T0
			INNER JOIN BROKER_USER AS T1 ON T1.UserId = T0.UserId AND T0.UserId = @varcharKey1 AND T0.ActionEventId = @varcharKey2
			INNER JOIN EMPLOYEE AS T2 ON T2.EmployeeId = T1.EmployeeId
			INNER JOIN BRANCH AS T3 on T3.BranchId = T2.BranchId
			RETURN 1;
        END


        IF ( @InputTableName = 'ALL_USER_CLIENT_CERTIFICATE' )
        BEGIN
			SELECT 'ALL_USER_CLIENT_CERTIFICATE' as TableName, 1 as IsEndOfGroup, T3.BrokerId, T0.* FROM [ALL_USER_CLIENT_CERTIFICATE] T0
			INNER JOIN BROKER_USER AS T1 ON T1.UserId = T0.UserId AND T0.UserId = @varcharKey1 AND T0.CertificateId = @varcharKey2
			INNER JOIN EMPLOYEE AS T2 ON T2.EmployeeId = T1.EmployeeId
			INNER JOIN BRANCH AS T3 on T3.BranchId = T2.BranchId
			RETURN 1;
        END
		
		IF ( @InputTableName = 'ALL_USER_MULTI_FACTOR_AUTH_CODE' )
        BEGIN
			SELECT 'ALL_USER_MULTI_FACTOR_AUTH_CODE' as TableName, 1 as IsEndOfGroup, T3.BrokerId, T0.* FROM [ALL_USER_MULTI_FACTOR_AUTH_CODE] T0
			INNER JOIN BROKER_USER AS T1 ON T1.UserId = T0.UserId AND T0.UserId = @varcharKey1 AND T0.UserId = @varcharKey2
			INNER JOIN EMPLOYEE AS T2 ON T2.EmployeeId = T1.EmployeeId
			INNER JOIN BRANCH AS T3 on T3.BranchId = T2.BranchId
			RETURN 1;
        END

		IF ( @InputTableName = 'ALL_USER_REGISTERED_IP' )
        BEGIN
			SELECT 'ALL_USER_REGISTERED_IP' as TableName, 1 as IsEndOfGroup, * FROM [ALL_USER_REGISTERED_IP]
			WHERE Id = @varcharKey1
			RETURN 1;
        END
		
		IF ( @InputTableName = 'APPRAISAL_VENDOR_EMPLOYEE_LOGIN' )
        BEGIN
			SELECT 'APPRAISAL_VENDOR_EMPLOYEE_LOGIN' as TableName, 1 as IsEndOfGroup, T3.BrokerId, T0.* FROM [APPRAISAL_VENDOR_EMPLOYEE_LOGIN] T0
			INNER JOIN EMPLOYEE AS T2 ON T2.EmployeeId = T0.EmployeeId and T0.EmployeeId = @varcharKey1 and T0.VendorId = @varcharKey2
			INNER JOIN BRANCH AS T3 on T3.BranchId = T2.BranchId
			RETURN 1;
        END		
						
		IF ( @InputTableName = 'BROKER_GLOBAL_IP_WHITELIST' )
        BEGIN
			SELECT 'BROKER_GLOBAL_IP_WHITELIST' as TableName, 1 as IsEndOfGroup, * FROM [BROKER_GLOBAL_IP_WHITELIST]
			WHERE Id = @varcharKey1
			RETURN 1;
        END
		
		IF ( @InputTableName = 'CONFIG_RELEASED' )
        BEGIN
			SELECT 'CONFIG_RELEASED' as TableName, 1 as IsEndOfGroup, OrgId as BrokerId, * FROM [CONFIG_RELEASED]
			WHERE ConfigId = @varcharKey1
			RETURN 1;
        END
		
		IF ( @InputTableName = 'CUSTOM_LETTER' )
        BEGIN
			SELECT 'CUSTOM_LETTER' as TableName, 1 as IsEndOfGroup, * FROM [CUSTOM_LETTER]
			WHERE CustomLetterID = @varcharKey1
			RETURN 1;
        END
        
        IF ( @InputTableName = 'PRINT_GROUP' )
        BEGIN
			SELECT 'PRINT_GROUP' as TableName, 1 as IsEndOfGroup, * FROM [PRINT_GROUP]
			WHERE GroupId = @varcharKey1
			RETURN 1;
        END
		
		IF ( @InputTableName = 'DISCUSSION_LOG' )
        BEGIN
			SELECT 'DISCUSSION_LOG' as TableName, 1 as IsEndOfGroup, * FROM [DISCUSSION_LOG]
			WHERE DiscLogId = @varcharKey1
			RETURN 1;
        END
		
		IF ( @InputTableName = 'DISCUSSION_NOTIFICATION' )
        BEGIN
			SELECT 'DISCUSSION_NOTIFICATION' as TableName, 1 as IsEndOfGroup, NotifBrokerId as BrokerId, * FROM [DISCUSSION_NOTIFICATION]
			WHERE NotifId = @varcharKey1
			RETURN 1;
        END
		
		IF ( @InputTableName = 'DOCUMENT_VENDOR_USER_ENABLED_CREDENTIALS' )
        BEGIN
			SELECT 'DOCUMENT_VENDOR_USER_ENABLED_CREDENTIALS' as TableName, 1 as IsEndOfGroup, * FROM [DOCUMENT_VENDOR_USER_ENABLED_CREDENTIALS]
			WHERE UserId = @varcharKey1 and VendorId = @varcharKey2
			RETURN 1;
        END
		
		IF ( @InputTableName = 'DROPBOX_FILES' )
        BEGIN
			SELECT 'DROPBOX_FILES' as TableName, 1 as IsEndOfGroup, T3.BrokerId, T0.* FROM [DROPBOX_FILES] T0
			INNER JOIN BROKER_USER AS T1 ON T1.UserId = T0.UserId and T0.FileId = @varcharKey1 and T0.UserId = @varcharKey2
			INNER JOIN EMPLOYEE AS T2 ON T2.EmployeeId = T1.EmployeeId
			INNER JOIN BRANCH AS T3 on T3.BranchId = T2.BranchId
			RETURN 1;
        END
		
		IF ( @InputTableName = 'EMPLOYEE_FAVORITE_REPORTS' )
        BEGIN
			SELECT 'EMPLOYEE_FAVORITE_REPORTS' as TableName, 1 as IsEndOfGroup, T1.BrokerId, T0.* FROM [EMPLOYEE_FAVORITE_REPORTS] T0
			INNER JOIN REPORT_QUERY T1 ON T1.QueryId = T0.QueryId AND T0.EmployeeId = @varcharKey1 and T0.QueryId = @varcharKey2
			RETURN 1;
        END
		
		IF ( @InputTableName = 'GROUP_x_EMPLOYEE' )
        BEGIN
			SELECT 'GROUP_x_EMPLOYEE' as TableName, 1 as IsEndOfGroup, T1.BrokerId, T0.* FROM [GROUP_x_EMPLOYEE] T0
			INNER JOIN [GROUP] T1 ON T1.GroupId = T0.GroupId AND T0.EmployeeId = @varcharKey1 and T0.GroupId = @varcharKey2
			RETURN 1;
        END		
		
		IF ( @InputTableName = 'INTERNAL_USER' )
        BEGIN
			SELECT 'INTERNAL_USER' as TableName, 1 as IsEndOfGroup, T3.BrokerId, T0.* FROM [INTERNAL_USER] T0
			INNER JOIN BROKER_USER AS T1 ON T1.UserId = T0.UserId and T0.UserId = @varcharKey1
			INNER JOIN EMPLOYEE AS T2 ON T2.EmployeeId = T1.EmployeeId
			INNER JOIN BRANCH AS T3 on T3.BranchId = T2.BranchId
			RETURN 1;
        END
		
		IF ( @InputTableName = 'IRS_4506T_VENDOR_EMPLOYEE_INFO' )
        BEGIN
			SELECT 'IRS_4506T_VENDOR_EMPLOYEE_INFO' as TableName, 1 as IsEndOfGroup, T3.BrokerId, T0.* FROM [IRS_4506T_VENDOR_EMPLOYEE_INFO] T0
			INNER JOIN BROKER_USER AS T1 ON T1.UserId = T0.UserId and T0.UserId = @varcharKey1 and T0.VendorId = @varcharKey2
			INNER JOIN EMPLOYEE AS T2 ON T2.EmployeeId = T1.EmployeeId
			INNER JOIN BRANCH AS T3 on T3.BranchId = T2.BranchId
			RETURN 1;
        END
		
		
		IF ( @InputTableName = 'LO_SIGNATURE_TEMPLATE' )
        BEGIN
			SELECT 'LO_SIGNATURE_TEMPLATE' as TableName, 1 as IsEndOfGroup, * FROM [LO_SIGNATURE_TEMPLATE]
			WHERE TemplateId = @varcharKey1
			RETURN 1;
        END

		IF ( @InputTableName = 'LOAN_CACHE_UPDATE_REQUEST' )
        BEGIN
			SELECT 'LOAN_CACHE_UPDATE_REQUEST' as TableName, 1 as IsEndOfGroup, T3.BrokerId, T0.* FROM [LOAN_CACHE_UPDATE_REQUEST] T0
			INNER JOIN BROKER_USER AS T1 ON T1.UserId = T0.UserId and T0.RequestId = @varcharKey1
			INNER JOIN EMPLOYEE AS T2 ON T2.EmployeeId = T1.EmployeeId
			INNER JOIN BRANCH AS T3 on T3.BranchId = T2.BranchId
			RETURN 1;
        END
		
		IF ( @InputTableName = 'ROLE_ASSIGNMENT' )
        BEGIN
			SELECT 'ROLE_ASSIGNMENT' as TableName, 1 as IsEndOfGroup, * FROM [ROLE_ASSIGNMENT] T1
			INNER JOIN EMPLOYEE AS T2 ON T2.EmployeeId = T1.EmployeeId and T1.EmployeeId = @varcharKey1 and T1.RoleId = @varcharKey2
			INNER JOIN BRANCH AS T3 on T3.BranchId = T2.BranchId
			RETURN 1;
        END
		
		
		
		
		
		IF ( @InputTableName = 'LOAN_FILE_A' )
        BEGIN
			SELECT 'LOAN_FILE_A' as TableName, 0 as IsEndOfGroup, e.sBrokerId as BrokerId, t.* FROM [LOAN_FILE_A] t
			INNER JOIN LOAN_FILE_E e on e.sLId = t.sLId and t.sLId = @varcharKey1
			RETURN 1;
        END
		IF ( @InputTableName = 'LOAN_FILE_B' )
        BEGIN
			SELECT 'LOAN_FILE_B' as TableName, 0 as IsEndOfGroup, e.sBrokerId as BrokerId, t.* FROM [LOAN_FILE_B] t
			INNER JOIN LOAN_FILE_E e on e.sLId = t.sLId and t.sLId = @varcharKey1
			RETURN 1;
        END
		IF ( @InputTableName = 'LOAN_FILE_C' )
        BEGIN
			SELECT 'LOAN_FILE_C' as TableName, 0 as IsEndOfGroup, e.sBrokerId as BrokerId, t.* FROM [LOAN_FILE_C] t
			INNER JOIN LOAN_FILE_E e on e.sLId = t.sLId and t.sLId = @varcharKey1
			RETURN 1;
        END
		IF ( @InputTableName = 'LOAN_FILE_D' )
        BEGIN
			SELECT 'LOAN_FILE_D' as TableName, 0 as IsEndOfGroup, e.sBrokerId as BrokerId, t.* FROM [LOAN_FILE_D] t
			INNER JOIN LOAN_FILE_E e on e.sLId = t.sLId and t.sLId = @varcharKey1
			RETURN 1;
        END
		IF ( @InputTableName = 'LOAN_FILE_E' )
        BEGIN
			SELECT 'LOAN_FILE_E' as TableName, 0 as IsEndOfGroup, sBrokerId as BrokerId, * FROM [LOAN_FILE_E]
			WHERE sLId = @varcharKey1
			RETURN 1;
        END
		IF ( @InputTableName = 'LOAN_FILE_F' )
        BEGIN
			SELECT 'LOAN_FILE_F' as TableName, 0 as IsEndOfGroup, e.sBrokerId as BrokerId, t.* FROM [LOAN_FILE_F] t
			INNER JOIN LOAN_FILE_E e on e.sLId = t.sLId and t.sLId = @varcharKey1
			RETURN 1;
        END
		IF ( @InputTableName = 'LOAN_FILE_CACHE' )
        BEGIN
			SELECT 'LOAN_FILE_CACHE' as TableName, 0 as IsEndOfGroup, e.sBrokerId as BrokerId, t.* FROM [LOAN_FILE_CACHE] t
			INNER JOIN LOAN_FILE_E e on e.sLId = t.sLId and t.sLId = @varcharKey1
			RETURN 1;
        END
		IF ( @InputTableName = 'LOAN_FILE_CACHE_2' )
        BEGIN
			SELECT 'LOAN_FILE_CACHE_2' as TableName, 0 as IsEndOfGroup, e.sBrokerId as BrokerId, t.* FROM [LOAN_FILE_CACHE_2] t
			INNER JOIN LOAN_FILE_E e on e.sLId = t.sLId and t.sLId = @varcharKey1
			RETURN 1;
        END
		IF ( @InputTableName = 'LOAN_FILE_CACHE_3' )
        BEGIN
			SELECT 'LOAN_FILE_CACHE_3' as TableName, 0 as IsEndOfGroup, e.sBrokerId as BrokerId, t.* FROM [LOAN_FILE_CACHE_3] t
			INNER JOIN LOAN_FILE_E e on e.sLId = t.sLId and t.sLId = @varcharKey1
			RETURN 1;
        END
        IF ( @InputTableName = 'LOAN_FILE_CACHE_4' )
        BEGIN
			SELECT 'LOAN_FILE_CACHE_4' as TableName, 0 as IsEndOfGroup, e.sBrokerId as BrokerId, t.* FROM [LOAN_FILE_CACHE_4] t
			INNER JOIN LOAN_FILE_E e on e.sLId = t.sLId and t.sLId = @varcharKey1
			RETURN 1;
        END
        
        IF ( @InputTableName = 'TRUST_ACCOUNT' )
        BEGIN
			SELECT 'TRUST_ACCOUNT' as TableName, 0 as IsEndOfGroup, e.sBrokerId as BrokerId, t.* FROM [TRUST_ACCOUNT] t
			INNER JOIN LOAN_FILE_E e on e.sLId = t.sLId and t.sLId = @varcharKey1
			RETURN 1;
        END
		
		
        IF ( @InputTableName = 'LOAN_USER_ASSIGNMENT' )
        BEGIN
			SELECT 'LOAN_USER_ASSIGNMENT' as TableName, 0 as IsEndOfGroup, e.sBrokerId as BrokerId, t.* FROM [LOAN_USER_ASSIGNMENT] t
			INNER JOIN LOAN_FILE_E e on e.sLId = t.sLId and t.RoleId = @varcharKey2 and t.sLId = @varcharKey3
			RETURN 1;
        END
		
		IF ( @InputTableName = 'AUDIT_TRAIL_DAILY' )
        BEGIN
			SELECT 'AUDIT_TRAIL_DAILY' as TableName, 0 as IsEndOfGroup, e.sBrokerId as BrokerId, t.* FROM [AUDIT_TRAIL_DAILY] t
			INNER JOIN LOAN_FILE_E e on e.sLId = t.LoanId and t.LoanId = @varcharKey1
			RETURN 1;
        END
		
		IF ( @InputTableName = 'APPLICATION_A' )
        BEGIN
			SELECT 'APPLICATION_A' as TableName, 0 as IsEndOfGroup, e.sBrokerId as BrokerId, t.* FROM [APPLICATION_A] t
			INNER JOIN LOAN_FILE_E e on e.sLId = t.sLId and t.aAppId = @varcharKey1
			RETURN 1;
        END

        IF ( @InputTableName = 'APPLICATION_B' )
        BEGIN
			SELECT 'APPLICATION_B' as TableName, 1 as IsEndOfGroup, e.sBrokerId as BrokerId, t.* FROM [APPLICATION_B] t
			INNER JOIN LOAN_FILE_E e on e.sLId = t.sLId and t.aAppId = @varcharKey1
			RETURN 1;
        END
		
		
		IF ( @InputTableName = 'SERVICE_FILE' )
        BEGIN
			SELECT 'SERVICE_FILE' as TableName, 1 as IsEndOfGroup, e.sBrokerId as BrokerId, t.* FROM [SERVICE_FILE] t
			INNER JOIN APPLICATION_B ap on ap.aAppId = t.Owner and t.ServiceFileId = @varcharKey1
			INNER JOIN LOAN_FILE_E e on e.sLId = ap.sLId 
			RETURN 1;
        END
		
        IF ( @InputTableName = 'TASK_SUBSCRIPTION' )
        BEGIN
			SELECT 'TASK_SUBSCRIPTION' as TableName, 1 as IsEndOfGroup, T3.BrokerId, T0.* FROM [TASK_SUBSCRIPTION] T0
			INNER JOIN BROKER_USER AS T1 ON T1.UserId = T0.UserId and T0.TaskId = @varcharKey1 and T0.UserId = @varcharKey2
			INNER JOIN EMPLOYEE AS T2 ON T2.EmployeeId = T1.EmployeeId
			INNER JOIN BRANCH AS T3 on T3.BranchId = T2.BranchId
			RETURN 1;
        END
		
		IF ( @InputTableName = 'TASK_TRIGGER_TEMPLATE' )
        BEGIN
			SELECT 'TASK_TRIGGER_TEMPLATE' as TableName, 1 as IsEndOfGroup, * FROM [TASK_TRIGGER_TEMPLATE]
			WHERE AutoTaskTemplateId = @varcharKey1
			RETURN 1;
        END
		
		IF ( @InputTableName = 'TEAM_USER_ASSIGNMENT' )
        BEGIN
			SELECT 'TEAM_USER_ASSIGNMENT' as TableName, 1 as IsEndOfGroup, T3.BrokerId, T0.* FROM [TEAM_USER_ASSIGNMENT] T0
			INNER JOIN EMPLOYEE AS T2 ON T2.EmployeeId = T0.EmployeeId AND T0.EmployeeId = @varcharKey1 and T0.TeamId = @varcharKey2
			INNER JOIN BRANCH AS T3 on T3.BranchId = T2.BranchId
			RETURN 1;
        END
        
        IF ( @InputTableName = 'TEAM_LOAN_ASSIGNMENT' )
        BEGIN
			SELECT 'TEAM_LOAN_ASSIGNMENT' as TableName, 1 as IsEndOfGroup, T2.BrokerId, T0.* FROM [TEAM_LOAN_ASSIGNMENT] T0
			INNER JOIN TEAM AS T2 ON T2.Id = T0.TeamId AND T0.TeamId = @varcharKey2
			INNER JOIN LOAN_FILE_CACHE AS T3 on T3.sLId = T0.LoanID AND T0.LoanId = @varcharKey1
			RETURN 1;
        END
		
		IF ( @InputTableName = 'TRACK' )
        BEGIN
			SELECT 'TRACK' as TableName, 1 as IsEndOfGroup, TrackBrokerId as BrokerId, * FROM [TRACK]
			WHERE TrackId = @varcharKey1
			RETURN 1;
        END
		
		IF ( @InputTableName = 'USAGE_EVENT' )
        BEGIN
			SELECT 'USAGE_EVENT' as TableName, 1 as IsEndOfGroup, * FROM [USAGE_EVENT]
			WHERE UsageEventId = @varcharKey1
			RETURN 1;
        END
		
		IF ( @InputTableName = 'XSLT_EXPORT_RESULT_STATUS' )
        BEGIN
			SELECT 'XSLT_EXPORT_RESULT_STATUS' as TableName, 1 as IsEndOfGroup, T3.BrokerId, T0.* FROM [XSLT_EXPORT_RESULT_STATUS] T0
			INNER JOIN BROKER_USER AS T1 ON T1.UserId = T0.UserId and T0.ReportId = @varcharKey1 
			INNER JOIN EMPLOYEE AS T2 ON T2.EmployeeId = T1.EmployeeId
			INNER JOIN BRANCH AS T3 on T3.BranchId = T2.BranchId
			RETURN 1;
        END
		
		IF ( @InputTableName = 'EDOCS_FOLDER_X_ROLE' )
        BEGIN
			SELECT 'EDOCS_FOLDER_X_ROLE' as TableName, 1 as IsEndOfGroup, T1.BrokerId, T0.* FROM [EDOCS_FOLDER_X_ROLE] T0
			INNER JOIN EDOCS_FOLDER T1 on T1.FolderId = T0.FolderId and T0.FolderId = @varcharKey1 and T0.RoleId = @varcharKey2 and T0.IsPmlUser = @varcharkey3
			RETURN 1;
        END
		
		IF ( @InputTableName = 'CP_CONSUMER_X_LOANID' )
        BEGIN
			SELECT 'CP_CONSUMER_X_LOANID' as TableName, 1 as IsEndOfGroup, T1.sBrokerId as BrokerId, T0.* FROM [CP_CONSUMER_X_LOANID] T0
			INNER JOIN LOAN_FILE_E T1 on T1.sLId = T0.sLId and T0.ConsumerId = @varcharKey1 
			and T0.sLId = @varcharKey2 and T0.aAppId = @varcharKey3 and T0.IsBorrower = @varcharKey4
			RETURN 1;
        END
		
		IF ( @InputTableName = 'RATE_MONITOR' )
        BEGIN
			SELECT 'RATE_MONITOR' as TableName, 1 as IsEndOfGroup, e.sBrokerId as BrokerId, T0.* FROM [RATE_MONITOR] T0
			INNER JOIN LOAN_FILE_E e on e.sLId = T0.sLId and T0.id = @varcharKey1
			
			--WHERE Rate = CAST(@varcharKey1 AS decimal(9,3)) and Point = CAST(@varcharKey2 AS decimal(9,3))
		    --WHERE id = @varcharKey1
			RETURN 1;
        END
		
		IF ( @InputTableName = 'TASK' )
        BEGIN
			SELECT 'TASK' as TableName, 1 as IsEndOfGroup, * FROM [TASK]
		    WHERE TaskId = @varcharKey1
			RETURN 1;
        END
		
		--Pricing at loMain		
		IF ( @InputTableName = 'FEE_SERVICE_REVISION' )
        BEGIN
			SELECT 'FEE_SERVICE_REVISION' AS TableName, 0 as IsEndOfGroup, * FROM FEE_SERVICE_REVISION
			WHERE Id = @varcharKey1
			RETURN 1;
		END
		
		IF ( @InputTableName = 'LOCKDESK_CLOSURE' )
        BEGIN
			SELECT 'LOCKDESK_CLOSURE' AS TableName, 0 as IsEndOfGroup, * FROM LOCKDESK_CLOSURE
			WHERE ClosureDate like @varcharKey1 and BrokerId = @varcharKey2
			RETURN 1;
		END
		
		IF ( @InputTableName = 'LENDER_LOCK_POLICY' )
        BEGIN
			SELECT 'LENDER_LOCK_POLICY' AS TableName, 0 as IsEndOfGroup, * FROM LENDER_LOCK_POLICY
			WHERE LockPolicyId = @varcharKey1
			RETURN 1;
		END
		
		IF ( @InputTableName = 'LPE_PRICE_GROUP' )
        BEGIN
			SELECT 'LPE_PRICE_GROUP' AS TableName, 0 as IsEndOfGroup, * FROM LPE_PRICE_GROUP
			WHERE LpePriceGroupId = @varcharKey1
			RETURN 1;
		END
		
		IF ( @InputTableName = 'LPE_PRICE_GROUP_PRODUCT' )
        BEGIN
			SELECT 'LPE_PRICE_GROUP_PRODUCT' AS TableName, 0 as IsEndOfGroup, g.BrokerId, p.* FROM LPE_PRICE_GROUP_PRODUCT as p
			INNER JOIN LPE_PRICE_GROUP g on g.LpePriceGroupId = p.LpePriceGroupId 
			WHERE p.lLpTemplateId = @varcharKey1 and p.LpePriceGroupId = @varcharKey2
			RETURN 1;
		END
		
		IF ( @InputTableName = 'REGION' )
        BEGIN
			SELECT 'REGION' as TableName, 0 as IsEndOfGroup, * FROM [REGION]
			WHERE RegionId = @varcharKey1
			RETURN 1;
		END
        
        IF ( @InputTableName = 'REGION_SET' )
        BEGIN
			SELECT 'REGION_SET' as TableName, 0 as IsEndOfGroup, * FROM [REGION_SET]
			WHERE RegionSetId = @varcharKey1
			RETURN 1;
        END
        
        IF ( @InputTableName = 'REGION_X_FIPS' )
        BEGIN
			SELECT 'REGION_X_FIPS' as TableName, 0 as IsEndOfGroup, * FROM [REGION_X_FIPS]
			WHERE FipsCountyCode = @varcharKey1 AND RegionId = @varcharKey2 AND RegionSetId = @varcharKey3
			RETURN 1;
        END
		
		BEGIN
			DECLARE @ErrorMessage varchar(1000)
			DECLARE @ErrorCode int
			
			SET @ErrorMessage = 'The table ' + @InputTableName + ' is not present in the sproc Justin_Testing_Another_GetBrokerBranchUserRelatedTablesAndOthers when @IsByTableName=1.'
			SET @ErrorCode = -53
			RAISERROR(@ErrorMessage, 16, 1)
			RETURN @ErrorCode
		END
	END
	
	else IF ( @IsByTableName = 1 and @UserOption = 'CheckIdentityEntryExist' )
	BEGIN
		-- by unique index
		-- Main Database
		IF ( @InputTableName = 'LICENSE' )
        BEGIN
			SELECT 'LicenseNumber', 'LicenseId' 
			
			SELECT LicenseNumber FROM LICENSE WHERE LicenseId = @varcharKey1
        END				
		
		IF ( @InputTableName = 'CONDITION_CATEGORY' )
        BEGIN
			SELECT 'ConditionCategoryId', 'BrokerId', 'Category' 
			
			SELECT ConditionCategoryId FROM CONDITION_CATEGORY WHERE BrokerId = @varcharKey1 and Category = @varcharKey2
        END
		
		IF ( @InputTableName = 'CONSUMER_PORTAL_USER' )
        BEGIN
			SELECT 'Id', 'BrokerId', 'Email' 
			
			SELECT Id FROM CONSUMER_PORTAL_USER WHERE BrokerId = @varcharKey1 and Email = @varcharKey2
        END
		
		IF ( @InputTableName = 'CUSTOM_FORM_FIELD_CATEGORY' )
        BEGIN
			SELECT 'CategoryId', 'CategoryName'
			
			SELECT CategoryId FROM CUSTOM_FORM_FIELD_CATEGORY WHERE CategoryName = @varcharKey1
        END
		
		IF ( @InputTableName = 'EDOCS_DOCMAGIC_DOCTYPE' )
        BEGIN
			SELECT 'Id', 'BarcodeClassification'
			
			SELECT Id FROM EDOCS_DOCMAGIC_DOCTYPE WHERE BarcodeClassification = @varcharKey1
        END
		
		IF ( @InputTableName = 'EDOCS_IDS_DOCTYPE' )
        BEGIN
			SELECT 'Id', 'BarcodeClassification' 
			
			SELECT Id FROM EDOCS_IDS_DOCTYPE WHERE BarcodeClassification = @varcharKey1
        END
		
		IF ( @InputTableName = 'EDOCS_PNG_ENTRY' )
        BEGIN
			SELECT 'Id', 'DocumentId', 'PngKey' 
			
			SELECT Id FROM EDOCS_PNG_ENTRY WHERE DocumentId = @varcharKey1 and PngKey = @varcharKey2
        END
		
		IF ( @InputTableName = 'INVESTOR_ROLODEX' )
        BEGIN
			SELECT 'InvestorRolodexId', 'BrokerId', 'InvestorName' 
			
			SELECT InvestorRolodexId FROM INVESTOR_ROLODEX WHERE BrokerId = @varcharKey1 and InvestorName = @varcharKey2
        END

		
		IF ( @InputTableName = 'TASK_PERMISSION_LEVEL' )
        BEGIN
			SELECT 'TaskPermissionLevelId', 'BrokerId', 'Name' 
			
			SELECT TaskPermissionLevelId FROM TASK_PERMISSION_LEVEL WHERE BrokerId = @varcharKey1 and Name = @varcharKey2
        END
		
		IF ( @InputTableName = 'WAREHOUSE_LENDER_ROLODEX' )
        BEGIN
			SELECT 'WarehouseLenderRolodexId', 'BrokerId', 'WarehouseLenderName' 
			
			SELECT WarehouseLenderRolodexId FROM WAREHOUSE_LENDER_ROLODEX WHERE BrokerId = @varcharKey1 and WarehouseLenderName = @varcharKey2
        END		
	END
END

GO


