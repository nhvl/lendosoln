CREATE procedure [dbo].[TEST_ThinhTest]
@BrokerId uniqueidentifier 
as
begin
SELECT
		BrokerId
		, TaskId
		, TaskAssignedUserId
		--, TaskIdentityId
		, TaskStatus
		, LoanId
		, TaskOwnerUserId
		, TaskCreatedDate
		, TaskSubject
		, TaskDueDate
		, TaskFollowUpDate
		, CondInternalNotes
		, TaskIsCondition
		, TaskResolvedUserId
		, TaskLastModifiedDate
		, BorrowerNmCached
		, LoanNumCached
		, TaskToBeAssignedRoleId
		, TaskToBeOwnerRoleId
		, TaskCreatedByUserId
		, TaskDueDateLocked
		, TaskDueDateCalcField
		, TaskDueDateCalcDays
		, TaskHistoryXml
		, TaskPermissionLevelId
		, TaskClosedUserId
		, TaskClosedDate
		, TaskRowVersion
		, CondCategoryId
		, CondIsHidden
		--, CondRank
		, CondDeletedDate
		, CondDeletedByUserNm
		, CondIsDeleted
		, assigned.UserFirstNm + ' ' + assigned.UserLastNm as AssignedUserFullName
		, owned.UserFirstNm + ' ' + owned.UserLastNm as OwnerFullName
		, CASE WHEN TaskClosedUserId IS NULL THEN '' ELSE closed.UserFirstNm + ' ' + closed.UserLastNm END as ClosingUserFullName
	FROM TASK t 
		left join Employee assigned on t.TaskAssignedUserId = assigned.EmployeeUserId
		left join Employee owned on t.TaskOwnerUserId = owned.EmployeeUserId
		left join Employee closed on t.TaskClosedUserId = closed.EmployeeUserId

	WHERE
		BrokerId = @brokerId
		and
		TaskStatus in (0,1)
		AND
		CondIsDeleted = 0 

end