-- =============================================
-- Author:		Michael Leinweaver
-- Create date: 2/5/2016
-- Description:	Lists all of the TPO portal users
--				for the specified broker.
-- =============================================
ALTER PROCEDURE [dbo].[SAEDB_ListBrokerTPOUsers] 
	@BrokerId uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT v.LoginNm as 'UserName', v.UserFirstNm as 'FirstName', v.UserLastNm as 'LastName', v.Email, g.LpePriceGroupName as 'PriceGroup',
		 v.PmlBrokerName as 'CompanyName', v.BranchNm as 'BranchName', p.Addr as 'StreetAddress', p.City, p.State,
		 'IsSupervisor' = CASE WHEN v.IsPmlManager = 1 THEN 'Yes' ELSE 'No' END
	FROM VIEW_ACTIVE_PML_USER v join PML_BROKER p on p.PmlBrokerId = v.PmlBrokerId join LPE_PRICE_GROUP g on g.LpePriceGroupId = v.LpePriceGroupId 
	WHERE v.BrokerId = @BrokerId
END
GO