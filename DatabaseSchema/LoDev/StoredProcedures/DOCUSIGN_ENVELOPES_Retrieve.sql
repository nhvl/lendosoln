-- =============================================
-- Author:		Eric Mallare
-- Create date: 1/23/2018
-- Description:	Loads DocuSign envelope records and their various components based on loan id, envelope id, or id.
-- =============================================
ALTER PROCEDURE [dbo].[DOCUSIGN_ENVELOPES_Retrieve] 
	@BrokerId uniqueidentifier,
	@LoanId uniqueidentifier,
	@EnvelopeId varchar(36) = null,
	@Id int = null
AS
BEGIN
	-- Grab the IDs first. We need that to look in all other tables
	DECLARE @FoundEnvelopeIds TABLE ( Id int NOT NULL )
	INSERT INTO
		@FoundEnvelopeIds(Id)
	SELECT
		de.Id
	FROM
		DOCUSIGN_ENVELOPES de
	WHERE
		de.LoanId=@LoanId AND de.BrokerId=@BrokerId AND
		de.EnvelopeId=COALESCE(@EnvelopeId, de.EnvelopeId) AND de.Id=COALESCE(@Id, de.Id)


	-- Grab the Edocs for the found envelopes
	SELECT
		ded.Id, ded.EnvelopeReferenceId, ded.SequenceNumber, ded.FromDocuSign, ded.EdocId, ded.Name, ded.DocumentId, ded.IsCertificate
	FROM 
		DOCUSIGN_EDOCS ded JOIN 
		@FoundEnvelopeIds fe ON ded.EnvelopeReferenceId=fe.Id

	-- Find Ids for found Recipients
	DECLARE @FoundRecipientIds TABLE
	(
		Id int NOT NULL
	)
	INSERT INTO
		@FoundRecipientIds(Id)
	SELECT
		dr.Id
	FROM
		DOCUSIGN_RECIPIENTS dr JOIN
		@FoundEnvelopeIds fe ON dr.EnvelopeReferenceId=fe.Id

	-- Grab the auth results
	SELECT
		dra.Id, dra.RecipientReferenceId, dra.EnvelopeReferenceId, dra.Type, dra.Status, dra.EventTime, dra.FailureDescription
	FROM
		DOCUSIGN_RECIPIENTS_AUTHRESULTS dra JOIN
		@FoundRecipientIds fr ON dra.RecipientReferenceId=fr.Id


	-- Grab the recipients
	SELECT
		dr.Id, dr.EnvelopeReferenceId, dr.SigningOrder, dr.Type, dr.Name, dr.Email, dr.MfaOption, dr.CurrentStatus, dr.SentDate, dr.DeliveredDate, dr.SignedDate, 
		dr.DeclinedDate, dr.DeclinedReason, dr.AutoRespondedReason, dr.RecipientIdGuid, dr.CustomRecipientId, dr.AccessCode, dr.ProvidedPhoneNumber, dr.ClientUserId
	FROM
		DOCUSIGN_RECIPIENTS dr JOIN 
		@FoundRecipientIds fr ON dr.Id=fr.Id

	-- Finally, grab the envelopes themselves
	SELECT
		de.Id, de.EnvelopeId, de.Name, de.RequestedDate, de.SenderName, de.SenderUserId, de.SenderEmail, de.LoanId, de.BrokerId, de.CurrentStatus, de.CreatedDate, de.DeletedDate, 
		de.SentDate, de.DeliveredDate, de.SignedDate, de.CompletedDate, de.DeclinedDate, de.TimedOutDate, de.VoidedDate, de.VoidReason, de.VoidDoneby
	FROM
		DOCUSIGN_ENVELOPES de JOIN
		@FoundEnvelopeIds fe ON de.Id=fe.Id
END
