-- =============================================
-- Author:		paoloa
-- Create date: 4/18/2013
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[CreateLockDeskClosureForPolicyId]
	-- Add the parameters for the stored procedure here
	@PolicyId uniqueidentifier,
	@BrokerId uniqueidentifier,
	@ClosureDate smalldatetime,
	@LockDeskOpenTime smalldatetime,
	@LockDeskCloseTime smalldatetime,
	@IsClosedAllDay bit,
	@IsClosedForBusiness bit = null
AS
BEGIN
	
	

	INSERT INTO LENDER_LOCK_POLICY_HOLIDAY (ClosureDate, LockPolicyId, BrokerId, IsClosedAllDay, LockDeskOpenTime, LockDeskCloseTime, IsClosedForBusiness)
	VALUES (@ClosureDate, @PolicyId, @BrokerId, @IsClosedAllDay, @LockDeskOpenTime, @LockDeskCloseTime, COALESCE(@IsClosedForBusiness,@IsClosedAllDay ))
END
