ALTER PROCEDURE [dbo].[DOCUSIGN_BRANCH_CONFIGURATION_Save]
	@BrokerId uniqueidentifier,
	@BranchId uniqueidentifier,
	@BrandIdForBlank uniqueidentifier = null,
	@BrandIdForRetail uniqueidentifier = null,
	@BrandIdForWholesale uniqueidentifier = null,
	@BrandIdForCorrespondent uniqueidentifier = null,
	@BrandIdForBroker uniqueidentifier = null,
	@BrandNameForBlank varchar(100) = '',
	@BrandNameForRetail varchar(100) = '',
	@BrandNameForWholesale varchar(100) = '',
	@BrandNameForCorrespondent varchar(100) = '',
	@BrandNameForBroker varchar(100) = ''
AS
BEGIN
	IF (@BrandIdForBlank IS NULL
		AND @BrandIdForRetail IS NULL
		AND @BrandIdForWholesale IS NULL
		AND @BrandIdForCorrespondent IS NULL
		AND @BrandIdForBroker IS NULL
		AND @BrandNameForBlank = ''
		AND @BrandNameForRetail = ''
		AND @BrandNameForWholesale = ''
		AND @BrandNameForCorrespondent = ''
		AND @BrandNameForBroker = '')
	BEGIN
		DELETE FROM dbo.DOCUSIGN_BRANCH_CONFIGURATION
		WHERE BrokerId = @BrokerId
			AND BranchId = @BranchId;
		RETURN;
	END

	UPDATE dbo.DOCUSIGN_BRANCH_CONFIGURATION
	SET BrandIdForBlank = @BrandIdForBlank,
		BrandIdForRetail = @BrandIdForRetail,
		BrandIdForWholesale = @BrandIdForWholesale,
		BrandIdForCorrespondent = @BrandIdForCorrespondent,
		BrandIdForBroker = @BrandIdForBroker,
		BrandNameForBlank = @BrandNameForBlank,
		BrandNameForRetail = @BrandNameForRetail,
		BrandNameForWholesale = @BrandNameForWholesale,
		BrandNameForCorrespondent = @BrandNameForCorrespondent,
		BrandNameForBroker = @BrandNameForBroker
	WHERE BrokerId = @BrokerId
		AND BranchId = @BranchId

	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO dbo.DOCUSIGN_BRANCH_CONFIGURATION
		(
			BrokerId,
			BranchId,
			BrandIdForBlank,
			BrandIdForRetail,
			BrandIdForWholesale,
			BrandIdForCorrespondent,
			BrandIdForBroker,
			BrandNameForBlank,
			BrandNameForRetail,
			BrandNameForWholesale,
			BrandNameForCorrespondent,
			BrandNameForBroker
		)
		VALUES
		(
			@BrokerId,
			@BranchId,
			@BrandIdForBlank,
			@BrandIdForRetail,
			@BrandIdForWholesale,
			@BrandIdForCorrespondent,
			@BrandIdForBroker,
			@BrandNameForBlank,
			@BrandNameForRetail,
			@BrandNameForWholesale,
			@BrandNameForCorrespondent,
			@BrandNameForBroker
		)
	END
END
