-- =============================================
-- Author:		Antonio Valencia
-- Create date: 11/4/2013
-- Description:	Gets List of Task Created From Templates for specified loan
-- =============================================
CREATE PROCEDURE [dbo].[TASK_GetTaskTriggerAssociations] 
	@loanId uniqueidentifier,
	@brokerid uniqueidentifier
	
	
	
AS
BEGIN
	SELECT * FROM TASK_x_TASK_TRIGGER_TEMPLATE 
	WHERE @brokerId = brokerid AND @loanid = loanid 
END
