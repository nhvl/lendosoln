CREATE  PROCEDURE [dbo].UpdateBrokerFieldChoices
	@BrokerId Guid , @FieldChoicesXmlContent Text
AS
	UPDATE
		Broker
	SET
		FieldChoicesXmlContent = @FieldChoicesXmlContent
	WHERE
		BrokerId = @BrokerId
	IF( 0!=@@error)
	BEGIN
		RAISERROR('Error in updating Broker table in UpdateBrokerFieldChoices sp', 16, 1);
		RETURN -100;
	END
	RETURN 0;
	
