
CREATE   FUNCTION [dbo].[GetEmployeeNameFromUserId] (@UserID uniqueidentifier )  
RETURNS varchar(43) AS  
BEGIN 
  DECLARE @FullName varchar(43)
  SET @FullName = ( SELECT UserFirstNm + ' ' + UserLastNm FROM Employee WITH (NOLOCK)  WHERE EmployeeUserId = @UserId )
  RETURN @FullName
END