-- =============================================
-- Author:		Scott Kibler
-- Create date: 3/20/2014
-- Description:	Checks if an employee is at a loan's broker
-- =============================================
CREATE FUNCTION [dbo].[EmployeeBrokerMatchesLoanBroker] 
(
	@EmployeeID uniqueidentifier,
	@LoanID uniqueidentifier
)
RETURNS bit
AS
BEGIN
	DECLARE @Matches bit
	DECLARE @lBrokerID uniqueidentifier

	SELECT @lBrokerID=sBrokerId
	FROM loan_file_e
	WHERE slid = @LoanID
	
	IF(
		EXISTS(
			SELECT *
			FROM VIEW_LENDERSOFFICE_EMPLOYEE v -- faster than join.
			WHERE 
					v.brokerid=@lBrokerID
				AND v.employeeid=@EmployeeID
			)
		)
		BEGIN
			SET @Matches = 1
		END
	ELSE
		BEGIN
			SET @Matches = 0
		END
	
	
	RETURN @Matches
END
GO