CREATE FUNCTION [dbo].[GetLoanTypeName] (@t int )  
RETURNS varchar(20) AS  
BEGIN 
DECLARE @Name varchar(20)
SELECT @Name =        CASE @t WHEN 0 THEN 'Conv.' 
                        WHEN 1 THEN 'FHA'
                        WHEN 2 THEN 'VA'
                        WHEN 3 THEN 'USDA/Rural Housing'
             END
RETURN @Name
END