-- =============================================
-- Author:		Scott Kibler
-- Create date: 12/11/2015
-- Description:	Checks if the investor rolodex table contains the investorid and brokerid provided
-- =============================================
CREATE FUNCTION [dbo].[InvestorRolodexTableContains] 
(
	@InvestorRolodexID int,
	@BrokerID uniqueidentifier
)
RETURNS bit
AS
BEGIN
	DECLARE @Matches bit
	
	IF(
		EXISTS(
			SELECT *
			FROM INVESTOR_ROLODEX
			WHERE 
				InvestorRolodexId = @InvestorRolodexID
			AND BrokerID = @BrokerID
			)
		)
		BEGIN
			SET @Matches = 1
		END
	ELSE
		BEGIN
			SET @Matches = 0
		END
	
	
	RETURN @Matches
END
