-- =============================================
-- Author:		Scott Kibler
-- Create date: 11/6/2015
-- Description:	Checks if a team is at the same broker as an employee
-- =============================================
CREATE FUNCTION [dbo].[TeamBrokerMatchesEmployeeBroker] 
(
	@TeamID uniqueidentifier,
	@EmployeeID uniqueidentifier
)
RETURNS bit
AS
BEGIN
	DECLARE @Matches bit
	DECLARE @tBrokerID uniqueidentifier

	SELECT @tBrokerID=BrokerId
	FROM TEAM
	WHERE id = @TeamID
	
	IF(
		EXISTS(
			SELECT *
			FROM VIEW_LENDERSOFFICE_EMPLOYEE v -- faster than join.
			WHERE 
					v.brokerid=@tBrokerID
				AND v.employeeid=@EmployeeID
			)
		)
		BEGIN
			SET @Matches = 1
		END
	ELSE
		BEGIN
			SET @Matches = 0
		END
	
	
	RETURN @Matches
END
GO