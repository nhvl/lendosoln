-- =============================================
-- Author:		Scott Kibler
-- Create date: 12/11/2015
-- Description:	Checks if an investor rolodex matches the loans broker.
-- =============================================
CREATE FUNCTION [dbo].[InvestorRolodexBrokerMatchesLoanBroker] 
(
	@InvestorRolodexID int,
	@LoanID uniqueidentifier
)
RETURNS bit
AS
BEGIN
	DECLARE @Matches bit
	DECLARE @iBrokerID uniqueidentifier

	SELECT @iBrokerID=BrokerId
	FROM INVESTOR_ROLODEX
	WHERE InvestorRolodexId = @InvestorRolodexID
	
	IF(
		EXISTS(
			SELECT *
			FROM LOAN_FILE_E e
			WHERE 
					e.sbrokerid=@iBrokerID
				AND e.slid=@LoanID
			)
		)
		BEGIN
			SET @Matches = 1
		END
	ELSE
		BEGIN
			SET @Matches = 0
		END
	
	
	RETURN @Matches
END
