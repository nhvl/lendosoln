

CREATE FUNCTION [dbo].[IntToNewBase] (@num bigint, @base tinyint)

RETURNS varchar(10) AS 
BEGIN 
  --This will move to C# once we have it in.

  -- Declarations
  DECLARE @string varchar(255)
  DECLARE @return varchar(10)
  DECLARE @finished bit
  DECLARE @div int
  DECLARE @rem int
  DECLARE @char char(1)

  -- Initialise
  SELECT @string   = '679CDFGHJKLMNPRWX' --'34679ACDEFGHJKLMNPRWXY'
  SELECT @return   = CASE WHEN @num <= 0 THEN '0' ELSE '' END
  SELECT @finished = CASE WHEN @num <= 0 THEN 1 ELSE 0 END
  SELECT @base     = CASE WHEN @base < 2 OR @base IS NULL THEN 2 WHEN @base > 17 THEN 17 ELSE @base END

  -- Loop
  WHILE @finished = 0
  BEGIN

    -- Do the math
    SELECT @div = @num / @base
    SELECT @rem = @num - (@div * @base)
    SELECT @char = SUBSTRING(@string, @rem + 1, 1)
    SELECT @return = @char + @return
    SELECT @num = @div

    -- Nothing left?
    IF @num = 0 SELECT @finished = 1

  END
  -- Done
  RETURN @return
  
END



