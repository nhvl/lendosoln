CREATE FUNCTION GetRoleName (@ID Guid )  
RETURNS varchar(21) AS  
BEGIN 
	DECLARE @RoleName varchar(21)
	SELECT @RoleName = RoleDesc FROM Role WHERE RoleID = @ID
	RETURN @RoleName
END
