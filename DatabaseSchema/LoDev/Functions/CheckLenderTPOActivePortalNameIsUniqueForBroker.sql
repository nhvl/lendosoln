-- =============================================
-- Author:		Scott Kibler
-- Create date: 2/7/2014
-- Description:	Check that name across broker tpo configs is unique
-- for non-deleted configs.
-- =============================================
CREATE FUNCTION [dbo].[CheckLenderTPOActivePortalNameIsUniqueForBroker]
(
	@BrokerID uniqueidentifier,
	@Name varchar(36)
)
RETURNS INT
AS
BEGIN
	DECLARE @RET INT

	
	SELECT @RET = COUNT(*)
	FROM Lender_TPO_LandingPageConfig WITH(NOLOCK)
	WHERE
	 
		BrokerID = @BrokerID
	AND IsDeleted = 0
	And Name = @Name
	
	RETURN @RET
END

