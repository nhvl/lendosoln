CREATE FUNCTION [dbo].[GetLoanStatusName] (@t int )  
RETURNS varchar(50) AS  
BEGIN 
DECLARE @Name varchar(50)
SELECT @Name =        
    CASE @t  WHEN 0 THEN 'Loan Open'
             WHEN 1 THEN 'Pre-qual'
             WHEN 2 THEN 'Pre-approved'
             WHEN 3 THEN 'Registered'
             WHEN 4 THEN 'Approved'
             WHEN 5 THEN 'Docs Out'
             WHEN 6 THEN 'Funded'
             WHEN 7 THEN 'Loan On-hold'
             WHEN 8 THEN 'Loan Suspended'
             WHEN 9 THEN 'Loan Canceled'
             WHEN 10 THEN 'Loan Denied'
             WHEN 11 THEN 'Loan Closed'
             WHEN 12 THEN 'Lead New'
             WHEN 13 THEN 'In Underwriting'
             WHEN 14 THEN 'Loan Web consumer'
	         WHEN 15 THEN 'Lead Canceled'
             WHEN 16 THEN 'Lead Declined'
	         WHEN 17 THEN 'Lead Other'
             WHEN 18 THEN 'Loan Other'
             WHEN 19 THEN 'Recorded'
	         WHEN 20 THEN 'Loan Shipped'
			 WHEN 21 THEN 'Clear to Close'
			 WHEN 22 THEN 'Processing'
			 WHEN 23 THEN 'Final Underwriting'
			 WHEN 24 THEN 'Docs Back'
			 WHEN 25 THEN 'Funding Conditions'
			 WHEN 26 THEN 'Final Docs'
			 WHEN 27 THEN 'Loan Sold'
			 WHEN 28 THEN 'Loan Submitted'
			 WHEN 29 THEN'Pre-Processing'    -- start sk 1/6/2014 opm 145251
			WHEN 30 THEN 'Document Check'
			WHEN 31 THEN 'Document Check Failed'
			WHEN 32 THEN 'Pre-Underwriting'
			WHEN 33 THEN 'Condition Review'
			WHEN 34 THEN 'Pre-Doc QC'
			WHEN 35 THEN 'Docs Ordered'
			WHEN 36 THEN 'Docs Drawn'
			WHEN 37 THEN 'Investor Conditions'       --  tied to sSuspendedByInvestorD
			WHEN 38 THEN 'Investor Conditions Sent'   --  tied to sCondSentToInvestorD
			WHEN 39 THEN 'Ready For Sale'
			WHEN 40 THEN 'Submitted For Purchase Review'
			WHEN 41 THEN 'In Purchase Review'
			WHEN 42 THEN 'Pre-Purchase Conditions'
			WHEN 43 THEN 'Submitted For Final Purchase Review'
			WHEN 44 THEN 'In Final Purchase Review'
			WHEN 45 THEN 'Clear To Purchase'
			WHEN 46 THEN 'Loan Purchased'        -- don't confuse with the old { E_sStatusT.Loan_LoanPurchased, which is now Loan Sold in the UI.
			WHEN 47 THEN 'Counter Offer Approved'
			WHEN 48 THEN 'Loan Withdrawn'
			WHEN 49 THEN 'Loan Archived'   
             END
RETURN @Name
END
