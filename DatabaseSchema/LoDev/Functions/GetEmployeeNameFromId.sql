CREATE   FUNCTION GetEmployeeNameFromId (@EmployeeID uniqueidentifier )  
RETURNS varchar(43) AS  
BEGIN 
  DECLARE @FullName varchar(43)
  SET @FullName = ( SELECT UserFirstNm + ' ' + UserLastNm FROM Employee WITH (NOLOCK)  WHERE EmployeeID = @EmployeeID )
  RETURN @FullName
END
