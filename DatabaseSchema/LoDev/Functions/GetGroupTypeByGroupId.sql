-- =============================================
-- Author:		Scott Kibler
-- Create date: 2/27/2017
-- Description:	
-- Gets the grouptype of the group with the given id, or throws an error if no such group.
-- The approach to erroring comes from here: http://stackoverflow.com/a/4681815/420667
-- =============================================
ALTER FUNCTION [dbo].[GetGroupTypeByGroupId]
(
	@GroupID uniqueidentifier
)
RETURNS INT
AS
BEGIN
	
	DECLARE @Msg varchar(3000)
	DECLARE @ErrorInt INT -- will be set on error.
	DECLARE @Ret INT
	
	SELECT @Ret=GroupType
	FROM [dbo].[Group]
	WHERE
		GroupId=@GroupId
	
	IF @RET IS NULL
	BEGIN
		-- The approach to erroring comes from here: http://stackoverflow.com/a/4681815/420667
		-- The message will fail to cast to an int, and thus throw an error, which is desirable.
		set @Msg = 'No group found with id ' + cast(@GroupId as varchar(100))
		set @ErrorInt = cast(@msg as int);
	END
	
	RETURN @Ret
END
