-- =============================================
-- Author:		Scott Kibler
-- Create date: 12/11/2015
-- Description:	Checks if a warehouse rolodex matches the loans broker.
-- =============================================
CREATE FUNCTION [dbo].[WarehouseRolodexBrokerMatchesLoanBroker] 
(
	@WarehouseRolodexID int,
	@LoanID uniqueidentifier
)
RETURNS bit
AS
BEGIN
	DECLARE @Matches bit
	DECLARE @wBrokerID uniqueidentifier

	SELECT @wBrokerID=BrokerId
	FROM WAREHOUSE_LENDER_ROLODEX
	WHERE WarehouseLenderRolodexId = @WarehouseRolodexID
	
	IF(
		EXISTS(
			SELECT *
			FROM LOAN_FILE_E e
			WHERE 
					e.sbrokerid=@wBrokerID
				AND e.slid=@LoanID
			)
		)
		BEGIN
			SET @Matches = 1
		END
	ELSE
		BEGIN
			SET @Matches = 0
		END
	
	
	RETURN @Matches
END
