-- =============================================
-- Author:		Scott Kibler
-- Create date: 11/6/2015
-- Description:	Checks if a team is at a loan's broker
-- =============================================
CREATE FUNCTION [dbo].[TeamBrokerMatchesLoanBroker] 
(
	@TeamID uniqueidentifier,
	@LoanID uniqueidentifier
)
RETURNS bit
AS
BEGIN
	DECLARE @Matches bit
	DECLARE @tBrokerID uniqueidentifier

	SELECT @tBrokerID=BrokerId
	FROM TEAM
	WHERE id = @TeamID
	
	IF(
		EXISTS(
			SELECT *
			FROM LOAN_FILE_E e -- faster than join.
			WHERE 
					e.sbrokerid=@tBrokerID
				AND e.slid=@LoanID
			)
		)
		BEGIN
			SET @Matches = 1
		END
	ELSE
		BEGIN
			SET @Matches = 0
		END
	
	
	RETURN @Matches
END
GO