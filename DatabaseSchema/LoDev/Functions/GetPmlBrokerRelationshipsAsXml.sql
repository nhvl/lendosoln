-- =============================================
-- Author:		Michael Leinweaver
-- Create date: 2/1/2017
-- Description:	Returns an OC's relationships in XML format.
-- =============================================
CREATE FUNCTION [dbo].[GetPmlBrokerRelationshipsAsXml]
(
	@BrokerId uniqueidentifier,
	@PmlBrokerId uniqueidentifier
)
RETURNS xml
AS
BEGIN
	DECLARE @ret xml

	SELECT @ret = (SELECT OCRoleT, EmployeeId, EmployeeName, EmployeeRoleT
	FROM PML_BROKER_RELATIONSHIPS
	WHERE BrokerId = @BrokerId AND PmlBrokerId = @PmlBrokerId
	FOR XML RAW, TYPE, ROOT)
	
	RETURN @ret
END
GO

