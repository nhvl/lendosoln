CREATE FUNCTION GetRoleIDByName (@Name varchar(21) )  
RETURNS UniqueIdentifier AS  
BEGIN 
	DECLARE @RoleID UniqueIdentifier
	SELECT @RoleID = RoleID FROM Role WHERE RoleDesc = @Name
	RETURN @RoleID
END
