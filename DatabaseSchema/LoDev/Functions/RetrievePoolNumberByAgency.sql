-- =============================================
-- Author:		Antonio Valencia
-- Create date: 4/16/2013
-- Description:	Retrieves the PoolNumberByAgency -- keep in sync with code 
-- =============================================
ALTER FUNCTION [dbo].[RetrievePoolNumberByAgency]
(
	@AgencyT int,
	@PoolTypeCode varchar(2),
	@BasePoolNumber  varchar(6),
	@IssueTypeCode varchar(1),
	@PoolPrefix varchar(3)
)
RETURNS varchar(10)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Result varchar(10)
	SET @Result = ''
	
	IF @AgencyT is Null BEGIN
		GOTO FINISH 
	END 
	
	IF @AgencyT IN (1, 2) BEGIN
		SET @Result = @PoolPrefix + '-' + @BasePoolNumber
	END
	ELSE IF @AgencyT = 3 BEGIN
		SET @Result = @BasePoolNumber + @IssueTypeCode + @PoolTypeCode
	END

FINISH: 
	-- Return the result of the function
	RETURN @Result

END
