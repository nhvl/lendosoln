CREATE FUNCTION GetReminderPriorityDescription (@t int )  
RETURNS varchar(15) AS  
BEGIN 
DECLARE @Name varchar(15)
SELECT @Name =        CASE @t WHEN 0 THEN 'High' 
                        WHEN 1 THEN 'Normal'
                        WHEN 2 THEN 'Low'
             END
RETURN @Name
END
