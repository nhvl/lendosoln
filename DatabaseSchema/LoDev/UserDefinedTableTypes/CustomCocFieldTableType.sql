CREATE TYPE [dbo].CustomCocFieldTableType AS TABLE 
(
	FieldId varchar(100),
	[Description] varchar(100),
	IsInstant bit
)
