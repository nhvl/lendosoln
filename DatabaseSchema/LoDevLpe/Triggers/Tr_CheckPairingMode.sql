CREATE trigger Tr_CheckPairingMode on product
for insert, update
as

if( update( PairingModeFor2ndLienProduct ) )
begin 
	if exists ( select * 
					from inserted I
					where PairingModeFor2ndLienProduct = '' and
						exists (	select *
									from product_pairing Pairing
									where I.InvestorName = pairing.InvestorName2ndLien
										and I.ProductCode = pairing.ProductCode2ndLien ) )
	begin
		RAISERROR('Cannot set 2nd lien product pairing mode to blank when there is at least one pairing rule', 16, 1);;
		rollback transaction;
		return;
	end
									
end
