-- =============================================
-- ALTER  trigger contained If UPDATE(column)
-- =============================================
CREATE   TRIGGER [dbo].[Tr_ProgTemplate_MaintainLpeCache]
ON [dbo].[LOAN_PROGRAM_TEMPLATE]
FOR INSERT, UPDATE , DELETE
AS 

if( 
    ( exists ( 
	select ins.lLpTemplateId
	from inserted ins 			
	where ins.IsLpe = 1 ) )
     OR
    ( exists ( 
	select del.lLpTemplateId 
	from deleted del 
			
	where del.IsLpe = 1 ) )
)
begin
	exec UpdatePriceEngineVersionDate
end
