CREATE trigger [Tr_CheckLpeInvestorProductCode]
on [dbo].[LOAN_PROGRAM_TEMPLATE]
for insert,update

as

	if( update( lLpInvestorNm ) or update( productCode ) )
	begin
		if exists( 
					select * 
					from inserted as I
					where IsLpe = 1 and isMasterPriceGroup = 0 and 
						not exists( 
						select * from product as prod
						where i.lLpInvestorNm = prod.InvestorName and i.ProductCode = prod.ProductCode ) )
		begin
			RAISERROR('Error during insert or update operation to loan_program_template table. Investor and ProductCode combination does not exist in Product table.', 16, 1);
			rollback transaction
		end
	end
