CREATE trigger [Tr_SrcRateOptionProgId] 
on [dbo].[LOAN_PROGRAM_TEMPLATE]
for insert, update
as
begin

-- this is how SrcRateOptionsProgId is defined.
--	[SrcRateOptionsProgId]  
-- AS (case when [lRateSheetxmlContentOverrideBit]=(1) 
-- then [lLpTemplateId] 
-- else [SrcRateOptionsProgIdInherit] end),

	if( update( lLpTemplateId ) or 
		update( lRateSheetXmlContentOverrideBit ) )
	begin
		-- Just to make sure the programs with overriden ratesheet has 
		-- a row in rate_options table.
		insert into Rate_Options
		( SrcProgId,lRateSheetXmlContent )
		select lLpTemplateId, ''
		from inserted I
		where lRateSheetXmlContentOverrideBit = 1 
			and not exists
				( select SrcProgId from Rate_Options ro where ro.SrcProgId = I.lLpTemplateId ) 	
	end	

end
