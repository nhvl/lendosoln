-- =============================================
-- Author:		paoloa
-- Create date: 5/7/2013
-- Description:	
-- =============================================
CREATE TRIGGER dbo.tr_InvestorAndProductCodeMustBeValidDisabledByLenderLockPolicy
   ON  dbo.DISABLED_BY_LENDER_LOCK_POLICY
   FOR insert, update
AS 
BEGIN
	if( update( investorName ) or update( productCode ) )
	begin
		if ( exists ( 
					select * 
					from inserted i left join product p on 
						i.investorName = p.investorName and i.productCode = p.productCode
					where p.investorName is null ) )
		begin
				RAISERROR('Error during insert or update operation to DISABLED_BY_LENDER_LOCK_POLICY table. Investor and ProductCode combination does not exist in Product table.', 16, 1);
				rollback transaction
		end
	end
END
