-- =============================================
-- ALTER  trigger contained If UPDATE(column)
-- =============================================
CREATE   TRIGGER Tr_ProdFolder_MaintainLpeCache
ON Loan_Product_Folder
FOR INSERT, UPDATE , DELETE
AS 

if( 
    ( exists ( 
	select * 
	from inserted ins 			
	where ins.IsLpe = 1 ) )
     OR
    ( exists ( 
	select * 
	from deleted del 
	where del.IsLpe = 1 ) )
)
begin
	exec UpdatePriceEngineVersionDate
end
