CREATE  trigger [Tr_NotAllowingInvalidInvestorName]
on [dbo].[LOAN_PROGRAM_TEMPLATE]
for insert,update
as
if( update ( lLpInvestorNm ) )
begin
	if exists ( select top 1 lLpTemplateId from inserted I where IsLpe = 1 and IsMasterPriceGroup = 0 and
		/* I replace this line with the next line to improve performance. -tn 7/19/2007
				lLpInvestorNm in ( select InvestorName from Investor_Name where IsValid = 0 ) ) 
		*/
		exists( select * from investor_name invName where IsValid = 0 and invName.InvestorName = I.lLpInvestorNm ) )
	begin
		RAISERROR('Error: cannot create new loan program with investor name marked as invalid', 16, 1);
		rollback	
	end
end
