CREATE trigger [Tr_ValidateData_For_PRICEGROUP_PROG_POLICY_ASSOC] 
on [dbo].[PRICEGROUP_PROG_POLICY_ASSOC]
after insert, update
as
    /* progId must be not master */
    if( update(ProgId)  and
        exists ( 
                   select * 
    	                from inserted ins, LOAN_PROGRAM_TEMPLATE pg
    	                where ins.ProgId =  pg.lLpTemplateId and pg.isMaster = 1 )
    	 )
    begin
    	RAISERROR('Error in inserting or updating PRICEGROUP_PROG_POLICY_ASSOC table: progId must be not master.', 16, 1);
    	rollback transaction
    end
