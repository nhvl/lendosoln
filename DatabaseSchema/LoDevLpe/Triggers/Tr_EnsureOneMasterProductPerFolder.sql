CREATE    trigger [Tr_EnsureOneMasterProductPerFolder] on [dbo].[LOAN_PROGRAM_TEMPLATE]
for update, insert
as 
	if update( IsMaster )
	begin 
		declare @count int
		select @count = count(*)
		from loan_program_template
		where IsMaster = 1 and IsMasterPriceGroup = 0 and isLpe = 1 
		group by FolderId, brokerid
		order by count(*)

		if( @count >= 2 )
		begin
			RAISERROR('Error: cannot have more than one master product for a folder.', 16, 1);
			rollback	
		end
	end
