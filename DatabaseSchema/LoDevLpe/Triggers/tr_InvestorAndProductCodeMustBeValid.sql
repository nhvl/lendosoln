CREATE trigger tr_InvestorAndProductCodeMustBeValid
on disabled_by_lender
for insert, update
as

if( update( investorName ) or update( productCode ) )
begin
	if ( exists ( 
				select * 
				from inserted i left join product p on 
					i.investorName = p.investorName and i.productCode = p.productCode
				where p.investorName is null ) )
	begin
			RAISERROR('Error during insert or update operation to DISABLED_BY_LENDER table. Investor and ProductCode combination does not exist in Product table.', 16, 1);
			rollback transaction
	end

end
