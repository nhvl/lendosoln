CREATE trigger [dbo].[Tr_RemoveRateOptionsRowUponDelete]
on [dbo].[LOAN_PROGRAM_TEMPLATE]
for delete
as
	delete from Rate_Options
	where SrcProgId in 
	(
		select lLpTemplateId
		from deleted 
	)
