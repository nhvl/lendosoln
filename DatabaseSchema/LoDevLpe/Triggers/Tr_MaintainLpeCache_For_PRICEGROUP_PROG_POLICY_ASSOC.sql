CREATE  TRIGGER [Tr_MaintainLpeCache_For_PRICEGROUP_PROG_POLICY_ASSOC]
on [dbo].[PRICEGROUP_PROG_POLICY_ASSOC]
FOR INSERT, UPDATE , DELETE
AS 
begin
	exec UpdatePriceEngineVersionDate

    /************* new code for PRICEGROUP_PROG_POLICY_ASSOC_VERSION **********/
    
    /* for insert and update PRICEGROUP_PROG_POLICY_ASSOC*/

    /* update versionId for existed record PRICEGROUP_PROG_POLICY_ASSOC_VERSION(LpePriceGroupId, ProgId) */
	  update PRICEGROUP_PROG_POLICY_ASSOC_VERSION
	   set  VersionId = newid()
       from inserted ins join PRICEGROUP_PROG_POLICY_ASSOC_VERSION ver
            on ins.LpePriceGroupId = ver.LpePriceGroupId and ins.ProgId = ver.ProgId

    /* insert a new record into PRICEGROUP_PROG_POLICY_ASSOC_VERSION if (LpePriceGroupId, ProgId) doesn't exist */
    insert into PRICEGROUP_PROG_POLICY_ASSOC_VERSION(LpePriceGroupId, ProgId)
       select ins.LpePriceGroupId, ins.ProgId 
       from inserted ins left join PRICEGROUP_PROG_POLICY_ASSOC_VERSION ver
            on ins.LpePriceGroupId = ver.LpePriceGroupId and ins.ProgId = ver.ProgId
       where ver.LpePriceGroupId is null


    /* for delete PRICEGROUP_PROG_POLICY_ASSOC */

    /* update versionId for existed record PRICEGROUP_PROG_POLICY_ASSOC_VERSION(LpePriceGroupId, ProgId) */
	  update PRICEGROUP_PROG_POLICY_ASSOC_VERSION
	     set  VersionId = newid()
       from deleted del join PRICEGROUP_PROG_POLICY_ASSOC_VERSION ver
            on del.LpePriceGroupId = ver.LpePriceGroupId and del.ProgId = ver.ProgId

    /* insert a new record into PRICEGROUP_PROG_POLICY_ASSOC_VERSION if (LpePriceGroupId, ProgId) doesn't exist */
    insert into PRICEGROUP_PROG_POLICY_ASSOC_VERSION(LpePriceGroupId, ProgId)
       select del.LpePriceGroupId, del.ProgId 
       from deleted del left join PRICEGROUP_PROG_POLICY_ASSOC_VERSION ver
            on del.LpePriceGroupId = ver.LpePriceGroupId and del.ProgId = ver.ProgId
       where ver.LpePriceGroupId is null

end
