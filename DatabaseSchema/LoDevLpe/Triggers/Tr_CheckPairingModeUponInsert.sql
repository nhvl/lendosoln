CREATE trigger Tr_CheckPairingModeUponInsert
on Product_Pairing
for insert, update
as
if( update( investorName2ndLien ) or update( productCode2ndLien ) )
begin 
	if exists ( 
			select * 
			from inserted I 
			where not exists 
				(	select * 
					from product Prod 
					where	Prod.InvestorName = I.InvestorName2ndLien
						and Prod.ProductCode = I.ProductCode2ndLien
						and PairingModeFor2ndLienProduct <> '' ) )
	begin
		RAISERROR('Cannot have product level pairing rule when the pairing mode is blank', 16, 1);;
		rollback transaction
		return
	end


end
