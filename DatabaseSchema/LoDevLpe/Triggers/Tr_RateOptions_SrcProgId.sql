CREATE trigger [dbo].[Tr_RateOptions_SrcProgId] on [dbo].[RATE_OPTIONS]
for update, delete
as
begin 
	if( update ( SrcProgId ) )
		begin
		if( exists( select del.SrcProgId 
					from deleted del join loan_program_template lp on del.SrcProgId = lp.SrcRateOptionsProgId ) )
		begin
			RAISERROR('cannot delete these rows from Rate_Options table, at least one row is still referenced by computed column loan_program_template.SrcRateOptionsProgId', 16, 1); 
			rollback transaction
		end
	end
	
end
