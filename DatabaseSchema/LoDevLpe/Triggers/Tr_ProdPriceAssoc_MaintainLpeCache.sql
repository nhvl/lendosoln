-- =============================================
-- ALTER  trigger contained If UPDATE(column)
-- =============================================

CREATE  TRIGGER Tr_ProdPriceAssoc_MaintainLpeCache
ON Product_Price_Association
FOR INSERT, UPDATE , DELETE
AS 


begin
	exec UpdatePriceEngineVersionDate
end
