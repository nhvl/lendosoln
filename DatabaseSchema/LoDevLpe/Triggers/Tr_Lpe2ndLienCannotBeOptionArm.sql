CREATE trigger Tr_Lpe2ndLienCannotBeOptionArm on loan_program_template
for update, insert
as
if( update( IsOptionArm ) or update( lLienPosT ) or update( isLPe ) )
begin
	if exists (
		select top 1 lLpTemplateId 
		from inserted 
		where isLPe = 1 and lLienPosT = 1 and IsOptionARm = 1
	)
	begin
		RAISERROR('Error: cannot have 2nd-lien lpe program with IsOptionArm = true', 16, 1);
		rollback	
	end
end
