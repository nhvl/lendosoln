-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[CreateDescendantStr_Wriring]
(
	@policyId uniqueidentifier
)
RETURNS varchar(MAX)
AS
BEGIN

    DECLARE @DescendantsStr varchar(MAX)
    
    SELECT  @DescendantsStr = COALESCE(@DescendantsStr + ';', '') +       
            CAST([PolicyId] AS varchar(50))
      FROM PRICE_POLICY_TO_ANCESTOR
      Where AncestorPolicyId = @policyId;

    Set @DescendantsStr = IsNull( @DescendantsStr, '' );
    
    	-- Return the result of the function
    RETURN @DescendantsStr

END
