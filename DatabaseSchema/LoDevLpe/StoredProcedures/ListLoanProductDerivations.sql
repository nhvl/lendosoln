CREATE     PROCEDURE [dbo].[ListLoanProductDerivations]
	@ProductId uniqueidentifier
AS
	SELECT
		p.lLpTemplateId ,
		p.lLpTemplateNm ,
		p.lBaseLpId ,
		p.BrokerId
	FROM
		Loan_Program_Template AS p
	WHERE
		p.lBaseLpId = @ProductId
