ALTER PROCEDURE dbo.SavePricePolicy
	@PricePolicyId uniqueidentifier,
	@ParentPolicyId uniqueidentifier = NULL,
	@PricePolicyXmlText text = NULL, 
	@PricePolicyDescription varchar(100) = NULL,
	@IsPublished bit = NULL,
	@PricePolicyNotes varchar(200) = NULL,
	@MutualExclusiveExecSortedId varchar(36) = null,
	@ContentKey varchar(100) = NULL,
	@RulesProtobufContent varbinary(max) = NULL,
	@HasQualifiedRule bit
	
AS
	UPDATE Price_Policy
	SET       
		ParentPolicyId = COALESCE( @ParentPolicyId , ParentPolicyId ),
		PricePolicyDescription = COALESCE( @PricePolicyDescription , PricePolicyDescription ),
		PricePolicyXmlText = COALESCE( @PricePolicyXmlText , PricePolicyXmlText ),
		IsPublished = COALESCE( @IsPublished , IsPublished ),
		Notes = COALESCE( @PricePolicyNotes , Notes ),
		MutualExclusiveExecSortedId = COALESCE( @MutualExclusiveExecSortedId, MutualExclusiveExecSortedId ),
		HasQualifiedRule = @HasQualifiedRule,
		ContentKey = COALESCE(@ContentKey, ContentKey),
		RulesProtobufContent = COALESCE(@RulesProtobufContent, RulesProtobufContent)
	FROM Price_Policy
	WHERE
		PricePolicyId =@PricePolicyId
