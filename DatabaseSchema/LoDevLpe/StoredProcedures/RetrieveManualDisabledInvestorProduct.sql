-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[RetrieveManualDisabledInvestorProduct] 
	@BrokerId UniqueIdentifier,
	@InvestorName varchar(100),
	@ProductCode varchar(100)
AS
BEGIN
	SELECT InvestorName, ProductCode,DisableStatus, MessagesToSubmittingUsers,DisableEntryModifiedDate,
           DisableEntryModifiedByEmployeeId, DisableEntryModifiedByUserName, DisableDateTime, IsHiddenFromResult, Notes
    FROM Disabled_By_Lender 
	WHERE BrokerId = @BrokerId AND InvestorName = @InvestorName AND ProductCode = @ProductCode
END
