CREATE PROCEDURE [dbo].[DoesLoanProductFolderHaveChildren]
	@BrokerID uniqueidentifier , @FolderId uniqueidentifier
AS
	SELECT
		Top 1 ' '
	FROM
		Loan_Product_Folder f
	WHERE
		f.BrokerID = @BrokerID
		AND
		( f.ParentFolderId = @FolderID 
			OR 
		( @FolderID = '00000000-0000-0000-0000-000000000000' AND f.IsLpe = 1 AND f.ParentFolderId is null ) )
