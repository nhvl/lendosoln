CREATE proc [dbo].[SavePMIProgramTemplate]
	@ProductId uniqueidentifier,
	@FolderId uniqueidentifier,
	@MICompany int,
	@MIType int,
	@IsEnabled bit,
	@UfmipIsRefundableOnProRataBasis bit
as
IF (SELECT count(*) FROM PMI_PRODUCT WHERE ProductId = @ProductId) > 0
BEGIN
	-- Update the template
	UPDATE PMI_PRODUCT
	SET FolderId = @FolderId,
		MICompany = @MICompany,
		MIType = @MIType,
		IsEnabled = @IsEnabled,
		UfmipIsRefundableOnProRataBasis = @UfmipIsRefundableOnProRataBasis
	WHERE ProductId = @ProductId
END
ELSE
BEGIN
	-- Or just add it in case it is new
    INSERT INTO PMI_PRODUCT (ProductId, FolderId, MICompany, MIType, IsEnabled, CreatedD,UfmipIsRefundableOnProRataBasis)
    VALUES (@ProductId, @FolderId, @MICompany, @MIType, @IsEnabled, GETDATE(), @UfmipIsRefundableOnProRataBasis)


SELECT PricePolicyId 
INTO #Temp
FROM PMI_POLICY_ASSOC 
WHERE PmiProductId = (SELECT top(1) ProductId FROM PMI_PRODUCT WHERE MIType = 0 AND FolderId = @FolderId)
and AssocOverrideTri = 1

Declare @PricePolicyId uniqueidentifier
While EXISTS(SELECT * From #Temp)
Begin
    SELECT Top 1 @PricePolicyId = PricePolicyId From #Temp
	INSERT INTO dbo.PMI_POLICY_ASSOC (PmiProductId, PricePolicyId, AssocOverrideTri, InheritVal)
    VALUES (@ProductId, @PricePolicyId, 0, 1)
    Delete #Temp Where PricePolicyId = @PricePolicyId
End
END
-- return the created date so that we can set it in the object for new templates
SELECT CreatedD FROM PMI_PRODUCT WHERE ProductId = @ProductId
