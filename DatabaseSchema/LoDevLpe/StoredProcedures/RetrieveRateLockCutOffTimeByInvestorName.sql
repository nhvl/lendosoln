CREATE PROCEDURE [dbo].[RetrieveRateLockCutOffTimeByInvestorName] 
	@InvestorName varchar(100)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT RateLockCutOffTime, UseLenderTimezoneForRsExpiration FROM Investor_Name WHERE InvestorName = @InvestorName;
END
