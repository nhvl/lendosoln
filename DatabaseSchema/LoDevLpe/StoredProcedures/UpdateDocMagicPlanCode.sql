
CREATE PROCEDURE [dbo].[UpdateDocMagicPlanCode]
		@PlanCodeId UniqueIdentifier
		, @PlanCodeNm varchar(100)
		, @PlanCodeDesc varchar(300)
		, @InvestorNm varchar(100)
		, @RuleXmlContent varchar(max)

AS
UPDATE DOC_MAGIC_PLAN_CODE
       SET 
		PlanCodeNm = @PlanCodeNm
		, PlanCodeDesc = @PlanCodeDesc 
		, InvestorNm = CASE WHEN @InvestorNm = 'GENERIC' THEN NULL ELSE @InvestorNm END
		, RuleXmlContent = @RuleXmlContent
WHERE PlanCodeId = @PlanCodeId
