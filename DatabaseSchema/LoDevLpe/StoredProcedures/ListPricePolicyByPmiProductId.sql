
create proc dbo.ListPricePolicyByPmiProductId
	@PmiProductId uniqueidentifier
as
	SELECT p.PricePolicyId, p.PricePolicyDescription, p.ParentPolicyID, p.IsPublished, pa.AssocOverrideTri, pa.InheritVal, p.IsExpandedNode
	FROM Price_Policy p LEFT JOIN PMI_POLICY_ASSOC pa ON pa.PricePolicyID = p.PricePolicyId AND pa.PmiProductId = @PmiProductId
	ORDER BY p.MutualExclusiveExecSortedId, p.PricePolicyDescription
