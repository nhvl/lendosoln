-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[DeleteManualDisabledInvestorProduct] 
	@BrokerId UniqueIdentifier,
	@InvestorName varchar(100),
	@ProductCode varchar(100)
AS
BEGIN
	DELETE Disabled_By_Lender 
	WHERE BrokerId = @BrokerId AND InvestorName = @InvestorName AND ProductCode = @ProductCode
END
