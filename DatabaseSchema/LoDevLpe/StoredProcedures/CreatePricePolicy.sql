ALTER PROCEDURE dbo.CreatePricePolicy
	@BrokerId Guid, 
	@ParentPolicyId Guid,
	@PricePolicyXmlText text,
	@PricePolicyDescription varchar(100),
	@IsPublished bit,
	@Notes varchar(200),
	@UseThisId Guid = NULL,
	@MutualExclusiveExecSortedId varchar(36),
	@HasQualifiedRule bit,
	@ContentKey varchar(100) = '',
	@RulesProtobufContent varbinary(max) = NULL,
	@PricePolicyId GUID OUT
	
AS
IF( @UseThisId IS NOT NULL )
	SET @PricePolicyId = @UseThisId
ELSE
	SET @PricePolicyId = newid()
INSERT INTO Price_Policy
( BrokerId, PricePolicyId, ParentPolicyId, PricePolicyXmlText,  PricePolicyDescription, IsPublished, Notes, MutualExclusiveExecSortedId, HasQualifiedRule, ContentKey, RulesProtobufContent )
values
( @BrokerId, @PricePolicyId, @ParentPolicyId, @PricePolicyXmlText, @PricePolicyDescription, @IsPublished, @Notes, @MutualExclusiveExecSortedId, @HasQualifiedRule, @ContentKey, @RulesProtobufContent  )
