CREATE  PROCEDURE [dbo].[ListLoanProgramsUsingBrokerID]
	@BrokerID uniqueidentifier = NULL , @IsLpe Bit , @IsMaster Bit = NULL
AS
SELECT
		l.IsMaster,
		l.lLpTemplateId,
		l.lLpTemplateNm,
		l.lRateOptionBaseId,
		l.lLockedDays,
		l.lLendNm,
		l.lTerm,
		l.lDue,
		l.FolderId,
		l.lRateSheetEffectiveD,
		l.lRateSheetExpirationD,
		COALESCE(r.lRateSheetXmlContent,'') lRateSheetXmlContent,
		l.lRateSheetXmlContentOverrideBit,
		l.BrokerId
	FROM
		Loan_Program_Template l LEFT JOIN Rate_Options r ON l.SrcRateOptionsProgId = r.SrcProgId  
	WHERE
		l.BrokerID = COALESCE( @BrokerID , l.BrokerId )
		AND
		l.IsMaster = COALESCE( @IsMaster , l.IsMaster )
		AND
		l.IsLpe = @IsLpe
		AND
		IsMasterPriceGroup = 0
	ORDER BY
		IsMaster DESC, lLpTemplateNm
