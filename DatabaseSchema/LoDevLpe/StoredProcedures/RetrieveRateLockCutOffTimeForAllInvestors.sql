-- =============================================
-- Author:		Eric Mallare
-- Create date: 1/27/2017
-- Description:	Gets the investor rate lock cut off time for all investors.
-- =============================================
ALTER PROCEDURE [dbo].[RetrieveRateLockCutOffTimeForAllInvestors]
	@InvestorNames xml = null
AS
BEGIN
	SET NOCOUNT ON;
	
	IF @InvestorNames IS NOT NULL
	BEGIN
		SELECT 
			inv.InvestorName, inv.RateLockCutOffTime, inv.UseLenderTimezoneForRsExpiration 
		FROM 
			Investor_Name inv JOIN
			@InvestorNames.nodes('root/investor/@name') as T(Item) ON inv.InvestorName=T.Item.value('.', 'varchar(100)')
	END
	ELSE
	BEGIN
		SELECT 
			InvestorName, RateLockCutOffTime, UseLenderTimezoneForRsExpiration 
		FROM 
			Investor_Name
	END
END
