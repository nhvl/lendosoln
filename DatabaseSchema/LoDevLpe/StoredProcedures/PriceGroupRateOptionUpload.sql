-- =============================================
-- Author:		Antonio Valencia
-- Create date: 2/7/2013
-- Description:	Upload Rates from Feed Email bot
-- =============================================
CREATE PROCEDURE dbo.[PriceGroupRateOptionUpload] 
	-- Add the parameters for the stored procedure here
	@LpePriceGroupId uniqueidentifier, 
	@lLpTemplateId uniqueidentifier,
	@lRateSheetXmlContent varchar(max),
	@LpeAcceptableRsFileId varchar(100),
	@LpeAcceptableRsFileVersionNumber bigint,
	@lRateSheetDownloadStartD datetime,
	@lRateSheetDownloadEndD datetime
AS
BEGIN
	
	UPDATE TOP(1) RATE_OPTION_PRICEGROUP_PROGRAM SET 
		lRateSheetXmlContent = @lRateSheetXmlContent,
		LpeAcceptableRsFileVersionNumber = @LpeAcceptableRsFileVersionNumber,
		LpeAcceptableRsFileId = @LpeAcceptableRsFileId
	WHERE LpePriceGroupId = @LpePriceGroupId and lLpTemplateId = @lLpTemplateId
	
	IF @@rowcount = 0 and @@error = 0 
	Begin
		INSERT RATE_OPTION_PRICEGROUP_PROGRAM(LpePriceGroupId,lLpTemplateId,
			lRateSheetXmlContent, LpeAcceptableRsFileId, LpeAcceptableRsFileVersionNumber)
		VALUES(
			@LpePriceGroupId, @lLpTemplateId, @lRateSheetXmlContent, @LpeAcceptableRsFileId, 
			@LpeAcceptableRsFileVersionNumber
		)
	END
	
	UPDATE TOP(1) RATE_OPTION_PRICEGROUP_PROGRAM_DOWNLOAD_TIME SET
		lRateSheetDownloadStartD = @lRateSheetDownloadStartD,
		lRateSheetDownloadEndD = @lRateSheetDownloadEndD
	WHERE LpePriceGroupId = @LpePriceGroupId AND lLpTemplateId = @lLpTemplateId
	
	IF @@rowcount = 0 and @@error = 0 
	BEGIN
		INSERT RATE_OPTION_PRICEGROUP_PROGRAM_DOWNLOAD_TIME(
			LpePriceGroupId, lLpTemplateId, lRateSheetDownloadStartD,
			lRateSheetDownloadEndD)
		VALUES(@LpePriceGroupId,@lLpTemplateId,@lRateSheetDownloadStartD,@lRateSheetDownloadEndD)
	END
		
		
END
