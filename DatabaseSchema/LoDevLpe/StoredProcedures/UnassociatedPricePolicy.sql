CREATE PROCEDURE [dbo].[UnassociatedPricePolicy]
	@LoanProductID Guid,
	@Ids varchar(2084)
AS
DECLARE @PolicyID varchar(40)
DECLARE @strLen int
DECLARE @CommaIndex int
DECLARE @LastIndex int
SET @strLen = LEN(@IDs)
SET @CommaIndex = 1
SET @LastIndex = 1
WHILE @CommaIndex <> 0 
BEGIN
	SELECT @CommaIndex = CHARINDEX(',', @IDs, @CommaIndex)
	IF @CommaIndex <> 0 
	BEGIN	
		SELECT @PolicyID = SUBSTRING(@IDs, @LastIndex, @CommaIndex - @LastIndex)
		SELECT @LastIndex = @CommaIndex + 1
		SELECT @CommaIndex = @CommaIndex + 1
	END
	ELSE
	BEGIN
		SELECT @PolicyID = SUBSTRING(@IDs, @LastIndex, @strLen - @LastIndex + 1)
	END		
	DELETE FROM Product_Price_Association WHERE PricePolicyID = @PolicyID AND ProductID = @LoanProductID
END
