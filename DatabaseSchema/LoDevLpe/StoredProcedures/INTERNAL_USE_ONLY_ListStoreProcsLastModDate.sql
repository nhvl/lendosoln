Create procedure [dbo].[INTERNAL_USE_ONLY_ListStoreProcsLastModDate]
as
SELECT ROUTINE_NAME , last_altered
   FROM INFORMATION_SCHEMA.ROUTINES 
   WHERE ROUTINE_NAME not like '%dt_%'
   order by last_altered DESC
