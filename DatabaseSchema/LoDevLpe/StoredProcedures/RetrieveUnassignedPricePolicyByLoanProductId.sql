CREATE PROCEDURE [dbo].[RetrieveUnassignedPricePolicyByLoanProductId]
	@LoanProductID Guid,
	@BrokerID Guid
AS
	SELECT PricePolicyId, PricePolicyDescription
	FROM Price_Policy ppMain
	WHERE BrokerID = @BrokerID 
	      AND IsPublished = 1 AND 
			
			/* 7/19/2007: I replaced this line with the exists block below to improve performance. -tn.
			PricePolicyId NOT IN (SELECT PricePolicyID FROM Product_Price_Association WHERE ProductID = @LoanProductID)
			*/
			exists ( select * 
					 from Product_Price_Association ppaSub 
					 where ppaSub.ProductID = @LoanProductID and ppMain.PricePolicyId = ppaSub.PricePolicyID )

    	ORDER BY MutualExclusiveExecSortedId,PricePolicyDescription
