





CREATE PROCEDURE [dbo].[GetProductInfoByBrokerId] 
	@BrokerId uniqueidentifier
	
AS
	SELECT lp.lLpTemplateId
	, lp.ProductCode
	, lp.lLpInvestorNm
	, ro.LpeAcceptableRsFileId
	, ro.LpeAcceptableRsFileVersionNumber
	FROM loan_program_template lp WITH (NOLOCK) 
	LEFT JOIN Rate_Options ro ON ro.SrcProgId = lp.SrcRateOptionsProgId
	WHERE brokerId = @BrokerId AND IsLpe = '1' AND IsMaster = '0' AND IsEnabled = '1'