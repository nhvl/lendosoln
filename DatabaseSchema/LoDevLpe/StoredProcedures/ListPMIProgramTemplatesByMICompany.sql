
CREATE proc [dbo].[ListPMIProgramTemplatesByMICompany]
	@MICompany int
as
select ProductId, MIType
from PMI_PRODUCT
where MICompany = @MICompany
and IsEnabled = 1
