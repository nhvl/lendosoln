CREATE  PROCEDURE [dbo].[CreateBlankLoanProduct]
	@BrokerId uniqueidentifier 
, @IsLpe Bit 
, @IsMaster Bit 
, @sLienPosT Int 
, @LoanProductId uniqueidentifier OUT
, @lBaseLpId uniqueidentifier = NULL 
, @FolderID uniqueidentifier = NULL 
, @UseThisId uniqueidentifier = NULL
, @SrcRateOptionsProgIdInherit uniqueidentifier = NULL
, @lLpInvestorNm varchar(100) = ''

AS
	DECLARE @PairIdFor1stLienProdGuid uniqueidentifier
	DECLARE @lLpeFeeMax int
	IF @UseThisId IS NOT NULL
		SET @LoanProductId = @UseThisId
	ELSE
		SET @LoanProductId = newid()
	IF @sLienPosT = 0
	BEGIN
		SET @PairIdFor1stLienProdGuid = @LoanProductId
		SET @lLpeFeeMax = 2
	END
	ELSE
	BEGIN
		SET @PairIdFor1stLienProdGuid = '{00000000-0000-0000-0000-000000000000}'
		SET @lLpeFeeMax = 5
	END
	INSERT INTO LOAN_PROGRAM_TEMPLATE
		( lLpTemplateId
		, BrokerId
		, IsLpe
		, IsMaster
		, lBaseLpId
		, lLienPosT
		, lRadj1stCapMon
		, lRAdjCapMon
		, lLateDays
		, lLateChargePc
		, lAprIncludesReqDeposit
		, lIOnlyMon
		, PairIdFor1stLienProdGuid
		, PairingProductIdsOverrideBit
		, FolderID
        , SrcRateOptionsProgIdInherit
		, lLpInvestorNm
		, lLpeFeeMax 
		)
		VALUES
		( @LoanProductId
		, @BrokerId
		, @IsLpe
		, @IsMaster
		, @lBaseLpId
		, @sLienPosT
		, 0
		, 0
		, 0
		, ''
		, 0
		, 0
		, @PairIdFor1stLienProdGuid
		, 0
		, @FolderID
        , @SrcRateOptionsProgIdInherit
		, @lLpInvestorNm
		, @lLpeFeeMax 
		)
	IF @@error != 0
	BEGIN
		RAISERROR('Error inserting blank loan program template.', 16, 1);
		RETURN -100
	END
