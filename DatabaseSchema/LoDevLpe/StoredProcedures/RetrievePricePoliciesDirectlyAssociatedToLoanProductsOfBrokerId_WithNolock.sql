create  PROCEDURE [dbo].[RetrievePricePoliciesDirectlyAssociatedToLoanProductsOfBrokerId_WithNolock]
	@BrokerId GUID,
	@IsEnabledProducts bit
AS
	-- This only return the policies associated with the loan products directly
	
	SELECT pa.ProductId, p.PricePolicyId, p.PricePolicyDescription, p.MutualExclusiveExecSortedId, lLpTemplateNm
	FROM ( Product_Price_Association pa with (nolock) JOIN Price_Policy p with (nolock) ON pa.PricePolicyID = p.PricePolicyId ) 
		join loan_program_template product with (nolock) on pa.productId = product.lLpTemplateId
	WHERE 
			product.BrokerId = @BrokerId 
		and 	product.IsMaster = 0 
		and 	product.IsEnabled = @IsEnabledProducts 
		and 	product.IsLpe = 1 
		and 	pa.AssocOverrideTri != 2
		and	( pa.InheritVal = 1 or pa.AssocOverrideTri = 1 or pa.InheritValFromBaseProd = 1 )
	ORDER BY pa.ProductId, MutualExclusiveExecSortedId
