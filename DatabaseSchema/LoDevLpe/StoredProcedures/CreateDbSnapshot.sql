ALTER PROCEDURE [dbo].[CreateDbSnapshot]
	@SnapshotName as varchar(100)
AS
IF( @SnapshotName not like 'ss%' )
BEGIN
	RAISERROR('Error: To avoid dropping the source database by mistake because of naming mix-up, snapshot name must have ''ss'' prefix. Msg from CreateDbSnapshot sp', 16, 1);
	RETURN -100;
END

IF( @SnapshotName LIKE '%[^a-zA-Z0-9_]%' ) -- adapted from http://stackoverflow.com/a/2558794/420667
BEGIN
	RAISERROR('Error: To avoid SQL injection, snapshot name may contain only alphanumeric characters and ''_''. Msg from CreateDbSnapshot sp', 16, 1);
	RETURN -100;
END

declare @dbName as varchar(100);
declare @logicalDbName as varchar(100);

select @dbname = db_name(), @logicalDbName = file_name(1);

declare @filename as varchar(600)
select @filename = location from view_DbLocationforSnapshot
set @filename =  @filename + @SnapshotName;


declare @sql nvarchar(2000)

set @sql = 'CREATE DATABASE ' + @SnapshotName + ' ON ( NAME = ' + @logicalDbName + ' , FILENAME = ''' + @filename  + '.ss'' ) AS SNAPSHOT OF ' + @dbname + ';'
print @sql
exec DropDbSnapshot @SnapshotName;

execute (@sql)

declare @errorStr as varchar(1000);
if( 0!=@@error )
begin
	set @errorStr = 'Error inside stored proc CreateDbSnapshot when execute this statement:' + @sql
	RAISERROR(@errorStr, 16, 1);
end

if( not exists ( SELECT [name] FROM master.dbo.[sysdatabases] WHERE ([name] = @SnapshotName ) ) )
begin
	set @errorStr = 'Error inside stored proc CreateDbSnapshot, snapshot was created after this statement was executed: ' + @sql
	RAISERROR(@errorStr, 16, 1);
end
