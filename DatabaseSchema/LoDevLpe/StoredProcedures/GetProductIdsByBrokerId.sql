CREATE PROCEDURE [dbo].[GetProductIdsByBrokerId] 
	@BrokerId uniqueidentifier
	
AS
	SELECT lLpTemplateId
	FROM loan_program_template WITH (NOLOCK) 
	WHERE brokerId = @BrokerId AND IsLpe = '1' AND IsMaster = '0' AND IsEnabled = '1'