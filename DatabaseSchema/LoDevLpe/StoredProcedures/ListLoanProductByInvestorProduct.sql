-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ListLoanProductByInvestorProduct]
	@sBrokerId UniqueIdentifier,
	@lLpInvestorNm varchar(100),
	@ProductCode varchar(65)
AS
BEGIN
	SELECT lLpTemplateNm, ro.LpeAcceptableRsFileId, ro.LpeAcceptableRsFileVersionNumber
	FROM Loan_Program_Template lp LEFT JOIN Rate_Options ro ON lp.SrcRateOptionsProgId = ro.SrcProgId
    WHERE lp.BrokerId = @sBrokerId
	  AND lLpInvestorNm = @lLpInvestorNm
      AND ProductCode = @ProductCode
      AND lp.IsLpe = 1 AND lp.IsEnabled = 1 AND lp.IsMaster = 0
    ORDER BY lLpTemplateNm


END
