
CREATE PROCEDURE [dbo].[DeleteDocMagicPlanCode]
	@PlanCodeId uniqueidentifier 
AS
	DELETE FROM Doc_Magic_Plan_Code
	WHERE @PlanCodeId = PlanCodeId
