CREATE PROCEDURE [dbo].[CreateLoanProductFolder]
	@FolderId Guid OUT, @FolderName varchar(36) , @IsLpe Bit , @BrokerId Guid , @ParentFolderId Guid = NULL , @UseThisId Guid = NULL
AS
	IF @UseThisId IS NOT NULL
		SET @FolderId = @UseThisId
	ELSE
		SET @FolderID = NEWID()
	INSERT INTO
		Loan_Product_Folder
		( FolderId
		, FolderName
		, IsLpe
		, BrokerId
		, ParentFolderId
		) VALUES
		( @FolderId
		, @FolderName
		, @IsLpe
		, @BrokerId
		, @ParentFolderId
		)
