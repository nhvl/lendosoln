-- =============================================
-- Author:		Michael Leinweaver
-- Create date: 1/22/2016
-- Description:	Obtains information about a folder and its parent.
-- =============================================
ALTER PROCEDURE [dbo].[SAEDB_ListInvalidInvestorProgramsFolderLookup] 
	@FolderId uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT FolderId, FolderName, ParentFolderId 
	FROM LOAN_PRODUCT_FOLDER 
	WHERE FolderId = @FolderId
END