-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Internal_ListLoanProgramTemplate]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
select lLpTemplateId, SrcRateOptionsProgId, lRateDelta,lRateDeltaInherit,lFeeDelta,lFeeDeltaInherit,lRateSheetXmlContentOverrideBit
FROM LOAN_PROGRAM_TEMPLATE
WHERE IsLpe=1 AND IsMaster=0
END
