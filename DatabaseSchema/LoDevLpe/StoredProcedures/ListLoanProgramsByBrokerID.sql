CREATE  PROCEDURE [dbo].[ListLoanProgramsByBrokerID]
	@BrokerID uniqueidentifier , @IsLpe Bit , @FolderID uniqueidentifier = NULL
AS
SELECT
		IsMaster,
		IsEnabled,
		lLpTemplateId,
		lLpTemplateNm,
		lRateOptionBaseId,
		lLockedDays,
		lLendNm,
		lTerm,
		lDue,
		lLockedDays,
		lLockedDaysLowerSearch,
		lPpmtPenaltyMon,
		lPpmtPenaltyMonLowerSearch,
		lRAdj1stCapMon,
		lFinMethT,
		lRateSheetEffectiveD,
		lRateSheetExpirationD,
		COALESCE(lRateSheetXmlContent,'') lRateSheetXmlContent,
		CASE WHEN IsMaster = 1 AND lBaseLpId IS NULL THEN 'Master' WHEN IsMaster = 0 AND lBaseLpId IS NOT NULL THEN 'Derived' WHEN IsMaster = 0 AND lBaseLpId IS NULL THEN '' WHEN IsMaster = 1 AND lBaseLpId IS NOT NULL THEN 'Invalid' END AS Type,
		Enabled = 
			case IsEnabled 
				when 1 then 'Yes' 
				else 'No'
			end
	FROM
		Loan_Program_Template l LEFT JOIN Rate_Options     r ON l.SrcRateOptionsProgId = r.SrcProgId  
	WHERE
		l.BrokerID = @BrokerID
		AND
		l.IsLpe = @IsLpe
		AND
		ISNULL(FolderID, '00000000-0000-0000-0000-000000000000') = ISNULL(@FolderId, '00000000-0000-0000-0000-000000000000')
		AND l.IsMasterPriceGroup = 0
	ORDER BY
		IsMaster desc, lLpTemplateNm
