CREATE PROCEDURE [dbo].[RetrieveAllLoanProductsByBrokerID_WithNolock]
	@BrokerID GUID,
	@IsEnabled bit,
	@IsLpe bit
AS
SELECT *
FROM Loan_Program_Template with (nolock)
WHERE BrokerID = @BrokerID and IsMaster = 0 and IsEnabled = @IsEnabled and IsLpe = @IsLpe
