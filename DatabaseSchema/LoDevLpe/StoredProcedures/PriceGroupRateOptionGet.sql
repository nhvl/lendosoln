-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE dbo.PriceGroupRateOptionGet
	@PriceGroupId uniqueidentifier,
	@TemplateId uniqueidentifier = null
	
AS
BEGIN

	if @TemplateId is null
		BEGIN
			select lLpTemplateId, lRateSheetXmlContent, LpeAcceptableRsFileId, LpeAcceptableRsFileVersionNumber  from RATE_OPTION_PRICEGROUP_PROGRAM 
			where lpepricegroupId = @PriceGroupId 
		END
	ELSE  BEGIN
	select lLpTemplateId, lRateSheetXmlContent, LpeAcceptableRsFileId, LpeAcceptableRsFileVersionNumber  from RATE_OPTION_PRICEGROUP_PROGRAM 
			where lpepricegroupId = @PriceGroupId and lLpTemplateId = @TemplateId
	END
	
	
END
