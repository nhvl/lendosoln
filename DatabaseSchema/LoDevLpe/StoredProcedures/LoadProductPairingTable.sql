CREATE PROCEDURE [dbo].[LoadProductPairingTable]
AS
BEGIN

SELECT InvestorName1stLien, ProductCode1stLien,InvestorName2ndLien, ProductCode2ndLien, PairingModeFor2ndLienProduct
FROM Product_Pairing pp JOIN Product p ON pp.InvestorName2ndLien=p.InvestorName AND pp.ProductCode2ndLien= p.ProductCode AND p.PairingModeFor2ndLienProduct != ''

END
