-- =============================================
-- Author:		paoloa
-- Create date: 5/16/2013
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[LockPolicy_DuplicateDisabledPricing]
	-- Add the parameters for the stored procedure here
	@ToPolicy uniqueidentifier,
	@FromPolicy uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    INSERT INTO DISABLED_BY_LENDER_LOCK_POLICY(LockPolicyId, BrokerId, InvestorName, ProductCode, DisableStatus, MessagesToSubmittingUsers, DisableEntryModifiedDate, DisableEntryModifiedByEmployeeId, DisableEntryModifiedByUserName, DisableDateTime, IsHiddenFromResult, Notes)
	SELECT @ToPolicy, BrokerId, InvestorName, ProductCode, DisableStatus, MessagesToSubmittingUsers, DisableEntryModifiedDate, DisableEntryModifiedByEmployeeId, DisableEntryModifiedByUserName, DisableDateTime, IsHiddenFromResult, Notes
	FROM DISABLED_BY_LENDER_LOCK_POLICY
	WHERE LockPolicyId = @FromPolicy
END
