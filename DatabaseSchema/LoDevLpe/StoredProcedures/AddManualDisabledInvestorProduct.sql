-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[AddManualDisabledInvestorProduct] 
	@BrokerId UniqueIdentifier,
	@InvestorName varchar(100),
	@ProductCode varchar(100),
	@DisableStatus varchar(50),
	@MessagesToSubmittingUsers varchar(5000),
	@DisableEntryModifiedByEmployeeId UniqueIdentifier,
	@DisableEntryModifiedByUserName varchar(100),
	@IsHiddenFromResult bit,
	@Notes varchar(5000)
AS
BEGIN
	INSERT INTO Disabled_By_Lender (BrokerId, InvestorName, ProductCode, DisableStatus, MessagesToSubmittingUsers,DisableEntryModifiedDate, DisableEntryModifiedByEmployeeId, DisableEntryModifiedByUserName, DisableDateTime, IsHiddenFromResult, Notes)
                         VALUES (@BrokerId, @InvestorName, @ProductCode, @DisableStatus, @MessagesToSubmittingUsers, GETDATE(), @DisableEntryModifiedByEmployeeId, @DisableEntryModifiedByUserName, GETDATE(), @IsHiddenFromResult, @Notes)
END
