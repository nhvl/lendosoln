-- =============================================
-- Author:		David Dao
-- Create date: 
-- Description:	
-- =============================================
ALTER PROCEDURE [dbo].[LpeUpload_UploadRateSheet] 
    @lLpTemplateId UniqueIdentifier,
    @lRateSheetDownloadStartD datetime,
    @lRateSheetXmlContent varchar(max),
	@RatesProtobufContent varbinary(max) = NULL,
    @ContentKey varchar(100),
    @LpeAcceptableRsFileId varchar(100) = NULL,
    @LpeAcceptableRsFileVersionNumber bigint = NULL,
    @new_lLockedDays int = NULL,
    @new_lLockedDaysLowerSearch int = NULL,
    @new_lPpmtPenaltyMon int = NULL,
    @new_lPpmtPenaltyMonLowerSearch int = NULL,
    @new_lIOnlyMonLowerSearch int = NULL,
    @new_lIOnlyMonUpperSearch int = NULL,
    @isAllowUpdateLockRange bit

AS
BEGIN

    -- 1/20/2017 - dd - This stored procedure is used by the LpeUpload process to upload ratesheet directly to database
    --                  without going through the CLoanProductData.InitSave() / Save(). During the peak hour, the code path CLoanProductData.InitSave()
    --                  is not fast enough to handle large volume. This stored procedure was extract the absolute necessary logic that perform by CLoanProductData.Save()
    
DECLARE @old_lLockedDays int
DECLARE @old_lLockedDaysLowerSearch int
DECLARE @old_lPpmtPenaltyMon int
DECLARE @old_lPpmtPenaltyMonLowerSearch int
DECLARE @old_lLockedDaysOverrideBit bit
DECLARE @old_lPpmtPenaltyMonOverrideBit bit
DECLARE @old_lIOnlyMonLowerSearch int
DECLARE @old_lIOnlyMonUpperSearch int
DECLARE @old_lIOnlyMonLowerSearchOverrideBit bit
DECLARE @old_lIOnlyMonUpperSearchOverrideBit bit
DECLARE @lBaseLpId UniqueIdentifier
DECLARE @SrcRateOptionsProgId UniqueIdentifier
DECLARE @lRateSheetXmlContentOverrideBit bit

-- First retrieve current data from LOAN_PROGRAM_TEMPLATE
SELECT @old_lLockedDays=lLockedDays, @old_lLockedDaysLowerSearch=lLockedDaysLowerSearch, 
       @old_lPpmtPenaltyMon=lPpmtPenaltyMon, @old_lPpmtPenaltyMonLowerSearch=lPpmtPenaltyMonLowerSearch,
	   @old_lLockedDaysOverrideBit=lLockedDaysOverrideBit,@old_lPpmtPenaltyMonOverrideBit=lPpmtPenaltyMonOverrideBit,
	   @old_lIOnlyMonLowerSearch=lIOnlyMonLowerSearch, @old_lIOnlyMonUpperSearch=lIOnlyMonUpperSearch,
	   @old_lIOnlyMonLowerSearchOverrideBit=lIOnlyMonLowerSearchOverrideBit,@old_lIOnlyMonUpperSearchOverrideBit=lIOnlyMonUpperSearchOverrideBit,
	   @lBaseLpId=lBaseLpId, @SrcRateOptionsProgId=SrcRateOptionsProgId,@lRateSheetXmlContentOverrideBit=lRateSheetXmlContentOverrideBit
FROM loan_program_template 
WHERE 
lLpTemplateId=@lLpTemplateId


DECLARE @new_lLockedDaysOverrideBit bit
DECLARE @new_lPpmtPenaltyMonOverrideBit bit
DECLARE @new_lIOnlyMonLowerSearchOverrideBit bit
DECLARE @new_lIOnlyMonUpperSearchOverrideBit bit

IF (@lBaseLpId IS NULL) OR (@lBaseLpId = '00000000-0000-0000-0000-000000000000')
BEGIN
    SET @new_lLockedDaysOverrideBit = @old_lLockedDaysOverrideBit
	SET @new_lPpmtPenaltyMonOverrideBit = @old_lPpmtPenaltyMonOverrideBit
	SET @new_lIOnlyMonLowerSearchOverrideBit = @old_lIOnlyMonLowerSearchOverrideBit
	SET @new_lIOnlyMonUpperSearchOverrideBit = @old_lIOnlyMonUpperSearchOverrideBit
END
ELSE
BEGIN
    SET @new_lLockedDaysOverrideBit = 1
	SET @new_lPpmtPenaltyMonOverrideBit = 1
	SET @new_lIOnlyMonLowerSearchOverrideBit = 1
	SET @new_lIOnlyMonUpperSearchOverrideBit = 1
END



    BEGIN TRANSACTION

    IF (@isAllowUpdateLockRange = 1) AND (
       (@new_lLockedDays <> @old_lLockedDays) OR
       (@new_lLockedDaysLowerSearch <> @old_lLockedDaysLowerSearch) OR
       (@new_lPpmtPenaltyMon <> @old_lPpmtPenaltyMon) OR 
       (@new_lIOnlyMonLowerSearch <> @old_lIOnlyMonLowerSearch) OR
       (@new_lIOnlyMonLowerSearchOverrideBit <> @old_lIOnlyMonLowerSearchOverrideBit) OR
       (@new_lIOnlyMonUpperSearch <> @old_lIOnlyMonUpperSearch) OR
       (@new_lIOnlyMonUpperSearchOverrideBit <> @old_lIOnlyMonUpperSearchOverrideBit) OR
       (@new_lLockedDaysOverrideBit <> @old_lLockedDaysOverrideBit) OR
       (@new_lPpmtPenaltyMonOverrideBit <> @old_lPpmtPenaltyMonOverrideBit))
    BEGIN
	    UPDATE LOAN_PROGRAM_TEMPLATE
	       SET
	       lLockedDays = @new_lLockedDays,
	       lLockedDaysLowerSearch = @new_lLockedDaysLowerSearch,
	       lPpmtPenaltyMon = @new_lPpmtPenaltyMon,
	       lPpmtPenaltyMonLowerSearch = @new_lPpmtPenaltyMonLowerSearch,
	       lIOnlyMonLowerSearch = @new_lIOnlyMonLowerSearch,
	       lIOnlyMonLowerSearchOverrideBit = @new_lIOnlyMonLowerSearchOverrideBit,
		   lIOnlyMonUpperSearch = @new_lIOnlyMonUpperSearch,
		   lIOnlyMonUpperSearchOverrideBit = @new_lIOnlyMonUpperSearchOverrideBit,
	       lLockedDaysOverrideBit = @new_lLockedDaysOverrideBit,
		   lPpmtPenaltyMonOverrideBit = @new_lPpmtPenaltyMonOverrideBit,
		   IsSpreadingNeededForProductData = 1
	    WHERE lLpTemplateId= @lLpTemplateId
    END

    IF @lRateSheetXmlContentOverrideBit = 1
    BEGIN	
	    UPDATE Rate_Options 
	       SET lRateSheetXmlContent = @lRateSheetXmlContent, 
				RatesProtobufContent = COALESCE(@RatesProtobufContent, RatesProtobufContent),
		       LpeAcceptableRsFileId=COALESCE(@LpeAcceptableRsFileId,LpeAcceptableRsFileId),
		       LpeAcceptableRsFileVersionNumber = COALESCE(@LpeAcceptableRsFileVersionNumber,LpeAcceptableRsFileVersionNumber),
		       ContentKey = @ContentKey
	    WHERE SrcProgId=@SrcRateOptionsProgId		

	    UPDATE RATE_OPTIONS_DOWNLOAD_TIME
		    SET lRateSheetDownloadStartD = @lRateSheetDownloadStartD,
			    lRateSheetDownloadEndD = @lRateSheetDownloadStartD
	    WHERE SrcProgId=@SrcRateOptionsProgId	
    END		

    COMMIT TRANSACTION

END
