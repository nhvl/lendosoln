CREATE proc [dbo].[DeletePMIProductFolder]
	@FolderId uniqueidentifier
as

while (select count(*) from PMI_PRODUCT_FOLDER where ParentFolderId = @FolderId) > 0
begin
DECLARE @TempId uniqueidentifier
SET @TempId = (SELECT top 1 FolderId FROM PMI_PRODUCT_FOLDER where ParentFolderId = @FolderId)
exec DeletePMIProductFolder @FolderId = @TempId
end

DELETE FROM PMI_POLICY_ASSOC WHERE PmiProductId in
(SELECT ProductId FROM PMI_PRODUCT WHERE FolderId = @FolderId)

delete from PMI_PRODUCT
where FolderId = @FolderId

delete from PMI_PRODUCT_FOLDER
where FolderId = @FolderId
