
CREATE PROCEDURE [dbo].[RetrieveDocMagicPlanCodeById]
	@PlanCodeId uniqueidentifier
AS
BEGIN
	SELECT 
	PlanCodeId
	, PlanCodeNm
	, PlanCodeDesc
	, CASE WHEN InvestorNm IS NULL THEN 'GENERIC' ELSE InvestorNm END as InvestorNm
	, RuleXmlContent
			
	FROM DOC_MAGIC_PLAN_CODE
	WHERE PlanCodeId = @PlanCodeId
END
