CREATE proc [dbo].[ListPmiProductsInFolderByMasterId]
	@PmiProductId uniqueidentifier
as

-- Returns all product id if the input product is master
-- Returns only the input if the input product is not a master

if (select top(1) MIType from PMI_PRODUCT where ProductId = @PmiProductId) = 0
begin
	Declare @FolderId uniqueidentifier
	set @FolderId = (select top(1) FolderId from PMI_PRODUCT where ProductId = @PmiProductId)
	SELECT ProductId FROM PMI_PRODUCT WHERE FolderId = @FolderId
end
else
	SELECT ProductId FROM PMI_PRODUCT WHERE ProductId = @PmiProductId
