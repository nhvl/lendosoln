CREATE PROCEDURE [dbo].[ListPriceGroupAssociationForDisplayByPolicyId]
	@PricePolicyId uniqueidentifier
AS

SELECT ProductId, AssocOverrideTri
FROM Product_Price_Association
WHERE PricePolicyId = @PricePolicyId AND AssocOverrideTri <> 0
