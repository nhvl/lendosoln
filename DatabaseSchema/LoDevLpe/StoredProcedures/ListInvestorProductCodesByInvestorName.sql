CREATE PROCEDURE [dbo].[ListInvestorProductCodesByInvestorName]
	@InvestorName VarChar( 100 ),
	@IsValid bit = NULL
AS
	SELECT ProductCode, IsValid, IsRateSheetExpirationFeatureDeployed
	FROM PRODUCT with(nolock) 
	WHERE IsValid = COALESCE( @IsValid, IsValid )
		AND InvestorName = @InvestorName
	ORDER BY ProductCode ASC
