CREATE PROCEDURE [dbo].[RetrievePricePolicyRelation]
	
AS
select PricePolicyId, ParentPolicyId, MutualExclusiveExecSortedId, IsPublished 
from price_policy
order by MutualExclusiveExecSortedId
