CREATE PROCEDURE [dbo].[RetrievePricePolicyByLoanProductId]
	@LoanProductId GUID
AS
	SELECT p.PricePolicyId, p.PricePolicyDescription
	FROM Product_Price_Association pa JOIN Price_Policy p ON pa.PricePolicyID = p.PricePolicyId
	WHERE pa.ProductID = @LoanProductID
	ORDER BY p.MutualExclusiveExecSortedId asc, p.PricePolicyDescription
