CREATE  PROCEDURE [dbo].[UpdateDisabledLoanProgramInfo]
	@InvestorName varchar(100),
	@ProductCode varchar(100),
	@DisableStatus varchar(50),
	@SaeName varchar(100),
	@Notes varchar(5000) = ''
AS
UPDATE DISABLED_BY_SAE 
 SET

DisableStatus = @DisableStatus,
SaeName = @SaeName,
Notes = @Notes
where 
InvestorName = @InvestorName and ProductCode = @ProductCode
