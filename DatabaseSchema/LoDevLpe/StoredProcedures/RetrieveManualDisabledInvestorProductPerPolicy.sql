
-- =============================================
-- Author:		paoloa
-- Create date: 4/29/2013
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[RetrieveManualDisabledInvestorProductPerPolicy]
	@BrokerId UniqueIdentifier,
	@PolicyId UniqueIdentifier,
	@InvestorName varchar(100),
	@ProductCode varchar(100)
AS
BEGIN
	SELECT InvestorName, ProductCode,DisableStatus, MessagesToSubmittingUsers,DisableEntryModifiedDate,
           DisableEntryModifiedByEmployeeId, DisableEntryModifiedByUserName, DisableDateTime, IsHiddenFromResult, Notes, LockPolicyId
    FROM  DISABLED_BY_LENDER_LOCK_POLICY
	WHERE BrokerId = @BrokerId AND InvestorName = @InvestorName AND ProductCode = @ProductCode AND LockPolicyId = @PolicyId
END
