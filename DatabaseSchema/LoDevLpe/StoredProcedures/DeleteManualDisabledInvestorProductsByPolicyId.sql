-- =============================================
-- Author:		paoloa
-- Create date: 5/14/2013
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[DeleteManualDisabledInvestorProductsByPolicyId]
	-- Add the parameters for the stored procedure here
	@LockPolicyId uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM DISABLED_BY_LENDER_LOCK_POLICY
	WHERE LockPolicyId = @LockPolicyId
END
