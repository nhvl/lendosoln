CREATE PROCEDURE [dbo].[ListPricePolicy]
AS
	SELECT PricePolicyId, CreateDate, PricePolicyDescription, ParentPolicyId, IsPublished, IsExpandedNode
 	FROM Price_Policy
	ORDER BY MutualExclusiveExecSortedId,PricePolicyDescription
