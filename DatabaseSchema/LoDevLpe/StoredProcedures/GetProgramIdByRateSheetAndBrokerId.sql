CREATE PROCEDURE [dbo].[GetProgramIdByRateSheetAndBrokerId] 
	@RatesheetId varchar(60),
	@BrokerId uniqueidentifier
	
AS
	SELECT lLpTemplateId
	FROM loan_program_template WITH (NOLOCK) 
	WHERE lRateOptionBaseId = @RatesheetId AND brokerId = @BrokerId
