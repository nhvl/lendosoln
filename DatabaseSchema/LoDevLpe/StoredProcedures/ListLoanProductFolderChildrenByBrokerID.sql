CREATE PROCEDURE [dbo].[ListLoanProductFolderChildrenByBrokerID]
	@BrokerID uniqueidentifier , @IsLpe Bit , @FolderId uniqueidentifier
AS
	SELECT
		f.FolderId , f.FolderName , f.ParentFolderID
	FROM
		Loan_Product_Folder f
	WHERE
		f.BrokerID = @BrokerID
		AND
		f.IsLpe = @IsLpe
		AND
		ISNULL(f.ParentFolderId, '00000000-0000-0000-0000-000000000000') = ISNULL(@FolderId, '00000000-0000-0000-0000-000000000000')
	ORDER BY
		FolderName
