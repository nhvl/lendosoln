CREATE PROCEDURE [dbo].[ListIdsOfProductsHaveDerivedProds]
as
	select distinct lBaseLpId
	from loan_program_template
	where lBaseLpId is not null and isLpe = 1
