-- =============================================
-- Author:		Michael Leinweaver
-- Create date: 12/29/2016
-- Description:	Loads loan product template data.
-- =============================================
ALTER PROCEDURE [dbo].[LoadLoanProductData]
	@fileid uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT LOAN_PROGRAM_TEMPLATE.*, r.lRateSheetXmlContent, r.LpeAcceptableRsFileId, r.LpeAcceptableRsFileVersionNumber , download.lRateSheetDownloadStartD, download.lRateSheetDownloadEndD, COALESCE(inherit.lRateSheetXmlContent, '') as lRateSheetXmlContentInherit, r.ContentKey AS RateOptionsContentKey, inherit.ContentKey AS RateOptionsContentKeyInherit, r.RatesProtobufContent
    FROM LOAN_PROGRAM_TEMPLATE
		join Rate_Options as r on LOAN_PROGRAM_TEMPLATE.SrcRateOptionsProgId = r.SrcProgId
		join RATE_OPTIONS_DOWNLOAD_TIME as download on LOAN_PROGRAM_TEMPLATE.SrcRateOptionsProgId = download.SrcProgId 
		left join Rate_Options as inherit on LOAN_PROGRAM_TEMPLATE.SrcRateOptionsProgIdInherit = inherit.SrcProgId
	WHERE lLpTemplateId = @fileid
END
GO
