-- =============================================
-- Author:		Michael Leinweaver
-- Create date: 12/29/2016
-- Description:	Loads loan product folder data.
-- =============================================
CREATE PROCEDURE [dbo].[LoadLoanProductFolderData]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SELECT * 
	FROM LOAN_PRODUCT_FOLDER
END
