CREATE proc [dbo].[LoadPMIProgramTemplate]
	@ProductId uniqueidentifier
as
select ProductId, FolderId, MICompany, MIType, IsEnabled, CreatedD, UfmipIsRefundableOnProRataBasis
from PMI_PRODUCT
where ProductId = @ProductId
