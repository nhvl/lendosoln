CREATE PROCEDURE [dbo].[ListInvestorProductWithValidProductCode]
AS
	SELECT InvestorName, ProductCode
	FROM PRODUCT
	WHERE IsValid = 1;
