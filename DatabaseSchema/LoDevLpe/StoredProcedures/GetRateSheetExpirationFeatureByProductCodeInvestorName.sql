CREATE PROCEDURE [dbo].[GetRateSheetExpirationFeatureByProductCodeInvestorName]
	@ProductCode varchar(65),
	@InvestorName varchar(100)
AS
BEGIN
	SELECT IsRateSheetExpirationFeatureDeployed 
    FROM Product
	WHERE ProductCode = @ProductCode AND InvestorName = @InvestorName
END
