CREATE  PROCEDURE [dbo].[ListInvestorNames]
  @IsValid bit = NULL
AS
SELECT InvestorName, IsValid, RateLockCutOffTime, UseLenderTimezoneForRsExpiration, InvestorId 
  FROM Investor_Name with(nolock) WHERE IsValid = COALESCE( @IsValid, IsValid )
  ORDER BY InvestorName ASC
