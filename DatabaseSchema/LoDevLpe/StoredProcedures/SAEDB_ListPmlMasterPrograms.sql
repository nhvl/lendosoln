-- =============================================
-- Author:		Michael Leinweaver
-- Create date: 1/22/2016
-- Description:	Obtains a list of master PML programs.
-- =============================================
ALTER PROCEDURE [dbo].[SAEDB_ListPmlMasterPrograms] 
	@PmlMasterBrokerId uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT DISTINCT LPE.lLpInvestorNm, LPE.ProductCode, 'Lien' = CASE WHEN LPE.lLienPosT = 0 THEN 'First' ELSE 'Second' END, LPE.lLpTemplateNm
	FROM LOAN_PROGRAM_TEMPLATE LPE
	WHERE LPE.BrokerId = @PmlMasterBrokerId
		AND LPE.isMaster = 0            --not a master
		AND LPE.lBaseLpId IS NULL        --not derived

	EXCEPT

	SELECT DISTINCT LPE.lLpInvestorNm, LPE.ProductCode, 'Lien' = CASE WHEN LPE.lLienPosT = 0 THEN 'First' ELSE 'Second' END, LPE.lLpTemplateNm
	FROM LOAN_PROGRAM_TEMPLATE LPE
	JOIN PRODUCT_PRICE_ASSOCIATION PPA
		ON LPE.lLpTemplateId = PPA.ProductId
	WHERE LPE.BrokerId = @PmlMasterBrokerId
		AND PPA.PricePolicyId = 'bce6e620-d267-4b06-845b-073b85366226' --GENERIC::DISCONTINUE policy
		AND LPE.isMaster = 0            --not a master
		AND LPE.lBaseLpId IS NULL        --not derived

	ORDER BY 1,2,3,4
END