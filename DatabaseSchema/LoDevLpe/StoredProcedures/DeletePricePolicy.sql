CREATE PROCEDURE [dbo].[DeletePricePolicy]
	@BrokerId Guid , @PricePolicyId Guid
AS
	DECLARE @tranPoint sysname
	SET @tranPoint = object_name( @@procId ) + CAST( @@nestlevel as varchar(10) )
	BEGIN TRANSACTION
	SAVE TRANSACTION @tranPoint
	--MAKING SURE THAT WE ARE NOT DELETING A POLICY WITH VALID OR FORCE NO ASSOCIATION
	--IF MASTER, 
	declare @count int
	select @count = count(*)
	from product_price_association
	where PricePolicyId = @PricePolicyId and AssocOverrideTri = 1
	
	if( @count > 0 )
	begin
		RAISERROR('Error in deleting from Product_Price_Association table in DeletePricePolicy sp, at least one product or master is associated with the target policy', 16, 1);
		goto ErrorHandler;
	end
	
	DELETE FROM Product_Price_Association
	WHERE PricePolicyId = @PricePolicyId
	IF( 0!=@@error )
	BEGIN
		RAISERROR('Error in deleting from Product_Price_Association table in DeletePricePolicy sp', 16, 1);
		goto ErrorHandler;
	END	
	DELETE FROM Price_Policy
	WHERE BrokerId = @BrokerId
		AND
		PricePolicyId = @PricePolicyId
	IF( 0!=@@error )
	BEGIN
		RAISERROR('Error in deleting from Price_Policy table in DeletePricePolicy sp', 16, 1);
		goto ErrorHandler;
	END	
	COMMIT TRANSACTION
	RETURN 0;
ErrorHandler:
	ROLLBACK TRANSACTION @tranPoint
	ROLLBACK TRANSACTION
	RETURN -100;
