ALTER PROCEDURE [dbo].[GetPriceEngineVersionDate]
as 
select LastUpdatedD, getdate() as CurrentDateTime
from price_engine_version with(nolock)