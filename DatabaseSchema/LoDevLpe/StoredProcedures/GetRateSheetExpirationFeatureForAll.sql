-- =============================================
-- Author:		Eric Mallare
-- Create date: 1/27/2017
-- Description:	Gets the IsRateSheetExpirationFeatureDeployed field for all product codes
-- =============================================
ALTER PROCEDURE [dbo].[GetRateSheetExpirationFeatureForAll]
	@InvestorCodePairXml xml = null
AS
BEGIN
	IF @InvestorCodePairXml IS NULL
	BEGIN
		SELECT 
			p.IsRateSheetExpirationFeatureDeployed, p.ProductCode, p.InvestorName 
		FROM 
			Product p
	END
	ELSE
	BEGIN
		SELECT 
			p.IsRateSheetExpirationFeatureDeployed, p.ProductCode, p.InvestorName 
		FROM 
			Product p JOIN 
			@InvestorCodePairXml.nodes('root/pair') as T(Item) ON 
				p.InvestorName=T.Item.value('@investorName', 'varchar(100)') AND
				p.ProductCode=T.item.value('@code', 'varchar(65)')
	END
END