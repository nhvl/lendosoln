-- =============================================
-- Author:		David Dao
-- Create date: 
-- Description:	
-- =============================================
ALTER PROCEDURE [dbo].[LOAN_PROGRAM_TEMPLATE_ListForPriceGroupAssociationByBrokerId]
    @BrokerId UniqueIdentifier
AS
BEGIN
	SELECT lp.lLpTemplateId, lp.lLpTemplateNm, lp.lLienPosT, lp.ProductCode, lp.lLT, lp.lLpInvestorNm, lp.lDataTracLpId 
            , ro.LpeAcceptableRsFileId, ro.LpeAcceptableRsFileVersionNumber
    FROM LOAN_PROGRAM_TEMPLATE lp LEFT JOIN RATE_OPTIONS ro ON ro.SrcProgId = lp.SrcRateOptionsProgId
    WHERE lp.BrokerId = @BrokerId
        AND lp.IsLpe = 1 AND lp.IsEnabled=1 AND lp.IsMaster=0
	ORDER BY lLpInvestorNm, ProductCode, lLienPosT, lLpTemplateNm

END