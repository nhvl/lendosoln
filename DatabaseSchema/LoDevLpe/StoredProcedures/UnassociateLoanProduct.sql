CREATE PROCEDURE [dbo].[UnassociateLoanProduct]
	@LoanProductId Guid
AS
	DELETE
	FROM Product_Price_Association
	WHERE
		ProductId = @LoanProductId
