-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ListInvestorProductHasRSE] 
AS
BEGIN
	SELECT InvestorName, ProductCode
	FROM Product
	WHERE IsValid = 1 AND IsRateSheetExpirationFeatureDeployed = 1
	ORDER By InvestorName, ProductCode
END
