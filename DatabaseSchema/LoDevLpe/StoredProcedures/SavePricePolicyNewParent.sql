CREATE PROCEDURE [dbo].[SavePricePolicyNewParent]
	@PricePolicyId Guid , @ParentPolicyId Guid
AS
	UPDATE Price_Policy
	SET       
		ParentPolicyId = @ParentPolicyId
	FROM Price_Policy
	WHERE
		PricePolicyId =@PricePolicyId
