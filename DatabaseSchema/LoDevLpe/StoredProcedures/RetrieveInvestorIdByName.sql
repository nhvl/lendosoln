
-- =============================================
-- Author:		David Dao
-- Create date: 3/5/2012
-- =============================================
CREATE PROCEDURE dbo.RetrieveInvestorIdByName
	@InvestorName varchar(100)
AS
BEGIN
	SELECT InvestorId FROM INVESTOR_NAME WHERE InvestorName = @InvestorName
END
