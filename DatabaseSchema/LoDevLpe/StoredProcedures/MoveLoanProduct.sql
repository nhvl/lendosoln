CREATE PROCEDURE [dbo].[MoveLoanProduct]
             @loanProductID uniqueidentifier,
	@newFolderId uniqueidentifier
AS
update LOAN_PROGRAM_TEMPLATE
set FolderId = @newFolderId
where lLpTemplateId = @loanProductID
