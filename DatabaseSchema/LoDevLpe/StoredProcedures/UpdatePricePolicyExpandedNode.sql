CREATE PROCEDURE [dbo].[UpdatePricePolicyExpandedNode]
	@PricePolicyID Guid,
	@IsExpandedNode bit
AS
UPDATE Price_Policy
       SET IsExpandedNode = @IsExpandedNode
WHERE PricePolicyID = @PricePolicyID and IsExpandedNode != @IsExpandedNode
