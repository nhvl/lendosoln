CREATE PROCEDURE dbo.ListPricePolicyChildren
	@PricePolicyId Guid
AS	SELECT p.*
 	FROM Price_Policy AS p
	WHERE
		p.ParentPolicyId = @PricePolicyId
	ORDER BY MutualExclusiveExecSortedId, PricePolicyDescription
	IF ( @@error != 0 )
	BEGIN
		RAISERROR('Error in the select statement in ListPricePolicyChildren sp', 16, 1);
		RETURN -100;
	END
