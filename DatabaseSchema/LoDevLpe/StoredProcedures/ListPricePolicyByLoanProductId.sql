CREATE PROCEDURE [dbo].[ListPricePolicyByLoanProductId]
	@LoanProductId uniqueidentifier
AS
	SELECT p.PricePolicyId, p.PricePolicyDescription, p.ParentPolicyID, p.IsPublished, pa.AssocOverrideTri, pa.InheritValFromBaseProd, pa.InheritVal, pa.IsForcedNo, p.IsExpandedNode
	FROM Price_Policy p LEFT JOIN Product_Price_Association pa ON pa.PricePolicyID = p.PricePolicyId AND pa.ProductID = @LoanProductID
	ORDER BY p.MutualExclusiveExecSortedId, p.PricePolicyDescription
