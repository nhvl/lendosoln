CREATE PROCEDURE dbo.LPE_FILE_KEY_LIST_Insert
	@FileKey varchar(100),
	@FileType varchar(25),
	@CreatedDate datetime
AS

	INSERT INTO LPE_FILE_KEY_LIST(FileKey, FileType, CreatedDate) VALUES (@FileKey, @FileType, @CreatedDate)