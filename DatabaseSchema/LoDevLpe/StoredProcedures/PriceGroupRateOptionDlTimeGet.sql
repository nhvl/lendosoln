-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE dbo.PriceGroupRateOptionDlTimeGet
	 @PriceGroupId uniqueidentifier
	
AS
BEGIN
	select lLpTemplateId, lRateSheetDownloadStartD,lRateSheetDownloadEndD  from RATE_OPTION_PRICEGROUP_PROGRAM_DOWNLOAD_TIME 
	where LpePriceGroupId = @PriceGroupId
END
