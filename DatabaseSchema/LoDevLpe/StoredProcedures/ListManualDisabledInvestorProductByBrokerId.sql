-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ListManualDisabledInvestorProductByBrokerId]
	@BrokerId UniqueIdentifier
AS
BEGIN
	SELECT InvestorName, ProductCode, DisableStatus, MessagesToSubmittingUsers, 
           DisableEntryModifiedDate, DisableEntryModifiedByEmployeeId, DisableEntryModifiedByUserName,
			DisableDateTime, IsHiddenFromResult, Notes
    FROM Disabled_By_Lender
    WHERE BrokerId = @BrokerId
    ORDER BY InvestorName, ProductCode
END
