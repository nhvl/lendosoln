CREATE PROCEDURE [dbo].[ClearAllAssocInheritanceCachedVal]
as
update product_price_association
set InheritValFromBaseProd = 0, InheritVal = 0
where InheritValFromBaseProd != 0 or InheritVal != 0
