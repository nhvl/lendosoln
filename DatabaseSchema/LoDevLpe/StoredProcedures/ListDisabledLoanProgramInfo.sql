CREATE  PROCEDURE [dbo].[ListDisabledLoanProgramInfo]
	@InvestorName varchar(100)
AS
	SELECT * FROM DISABLED_BY_SAE 
	WHERE InvestorName = @InvestorName
