CREATE PROCEDURE [dbo].[RetrieveLoanProgramInfoById] 
	@lLpTemplateId UniqueIdentifier
AS
BEGIN
SELECT lLpTemplateNm, lLpInvestorNm, lTerm, lDue, lLienPosT, ProductCode, lFinMethT, lRAdj1stCapMon ,lArmIndexGuid
FROM loan_program_template
WHERE lLpTemplateId = @lLpTemplateId
END
