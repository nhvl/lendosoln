
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ListInvestorNamesByBrokerId]
	@sBrokerId UniqueIdentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT DISTINCT lLpInvestorNm
	FROM Loan_Program_Template
	WHERE BrokerId=@sBrokerId AND IsMaster=0 AND IsEnabled=1 AND IsLpe =1  --and lLpInvestorNm <> 'aurora'
END
