ALTER PROCEDURE [dbo].[ListNonMasterLoanProgramsByBrokerIDToRunLpe]
	@BrokerID uniqueidentifier,
	@LienPosTIncluded1 int,
	@LienPosTIncluded2 int,
	@LoanIdWithMustMatchProductId uniqueidentifier = null,
	@PriceGroupId uniqueidentifier,
	@FilterDue10Yrs bit = 0,
	@FilterDue15Yrs bit = 0,
	@FilterDue20Yrs bit = 0,
	@FilterDue25Yrs bit = 0,
	@FilterDue30Yrs bit = 0,
	@FilterDueOther bit = 0,
	@FilterFinMethFixed bit = 0,
	@FilterFinMeth3YrsArm bit = 0,
	@FilterFinMeth5YrsArm bit = 0,	
	@FilterFinMeth7YrsArm bit = 0,
	@FilterFinMeth10YrsArm bit = 0,	
	@FilterFinMethOther bit = 0,
	@FilterOnlyCanBeStandAlone2nd bit = 0,
	@FilterLpProductType varchar(30) = NULL,
	@FirstLienLpId UniqueIdentifier = NULL,
	@Include10yrAnd25yrInOtherDueFilter bit = 0,
	@FilterLpInvestorName varchar(100) = NULL,
	@FilterPaymentTypePI bit = 1, -- TEMP : to unbreak dev pricing.
	@FilterPaymentTypeIOnly bit = 1,
	@IsNonQm bit = 0
	
AS
-- 2/12/05 dd - Hack such that lLockedDaysRequested = 999 is return everything. PpmtPenaltyMonRequested = 999 is return everything
if( @FirstLienLpId is null )
begin
	SELECT distinct prod.lLpTemplateId, lLpTemplateNm, FolderID, lLpInvestorNm, ProductCode, lLpProductType, lLT
	FROM Loan_Program_Template prod
	
	WHERE prod.BrokerID = @BrokerID 
	      AND IsLpe = 1 and IsMaster = 0 and IsEnabled = 1 
		  AND (lLienPosT = @LienPosTIncluded1 or lLienPosT = @LienPosTIncluded2 )
		  AND (@FilterOnlyCanBeStandAlone2nd = 0 OR CanBeStandAlone2nd = 1)
		  AND (IsLpeDummyProgram = 1 OR (
			   (
					 (@FilterDue10Yrs = 1 AND lTerm = 120)
				  OR (@FilterDue15Yrs = 1 AND lTerm = 180)
				  OR (@FilterDue20Yrs = 1 AND lTerm = 240)
				  OR (@FilterDue25Yrs = 1 AND lTerm = 300)
				  OR (@FilterDue30Yrs = 1 AND lTerm = 360)
				  OR (@FilterDueOther = 1 AND @Include10yrAnd25yrInOtherDueFilter=1 AND lTerm NOT IN (120, 180, 240, 300, 360))
				  OR (@FilterDueOther = 1 AND @Include10yrAnd25yrInOtherDueFilter=0 AND lTerm NOT IN (180, 240, 360))
				  )
			  AND (lLpProductType = COALESCE(@FilterLpProductType, lLpProductType))
			  AND (lLpInvestorNm=COALESCE(@FilterLpInvestorName, lLpInvestorNm))
			  AND (
				  (@FilterFinMethFixed = 1 AND lFinMethT = 0 AND (IsOptionArm = 0 AND lPmtAdjCapR = 0 AND lPmtAdjCapMon=0 AND lPmtAdjRecastPeriodMon=0 AND lPmtAdjRecastStop=0 AND lPmtAdjMaxBalPc=0))
				  OR (@FilterFinMeth3YrsArm = 1 AND (lFinMethT = 1 AND lRAdj1stCapMon = 36) AND (IsOptionArm = 0 AND lPmtAdjCapR = 0 AND lPmtAdjCapMon=0 AND lPmtAdjRecastPeriodMon=0 AND lPmtAdjRecastStop=0 AND lPmtAdjMaxBalPc=0))
				  OR (@FilterFinMeth5YrsArm = 1 AND (lFinMethT = 1 AND lRAdj1stCapMon = 60) AND (IsOptionArm = 0 AND lPmtAdjCapR = 0 AND lPmtAdjCapMon=0 AND lPmtAdjRecastPeriodMon=0 AND lPmtAdjRecastStop=0 AND lPmtAdjMaxBalPc=0))
				  OR (@FilterFinMeth7YrsArm = 1 AND (lFinMethT = 1 AND lRAdj1stCapMon = 84) AND (IsOptionArm = 0 AND lPmtAdjCapR = 0 AND lPmtAdjCapMon=0 AND lPmtAdjRecastPeriodMon=0 AND lPmtAdjRecastStop=0 AND lPmtAdjMaxBalPc=0))
				  OR (@FilterFinMeth10YrsArm = 1 AND (lFinMethT = 1 AND lRAdj1stCapMon = 120) AND (IsOptionArm = 0 AND lPmtAdjCapR = 0 AND lPmtAdjCapMon=0 AND lPmtAdjRecastPeriodMon=0 AND lPmtAdjRecastStop=0 AND lPmtAdjMaxBalPc=0))		
  			      OR (@FilterFinMethOther = 1 AND ((lFinMethT = 1 AND lRAdj1stCapMon NOT IN (36, 60, 84, 120)) OR lFinMethT > 1 OR IsOptionArm = 1 OR lPmtAdjCapR <> 0 OR lPmtAdjCapMon<>0 OR lPmtAdjRecastPeriodMon<>0 OR lPmtAdjRecastStop<>0 OR lPmtAdjMaxBalPc<>0))
			  )
			  AND (
					 (@FilterPaymentTypePI = 1 AND lIOnlyMon <= 0)
			      OR (@FilterPaymentTypeIOnly = 1 AND lIOnlyMon > 0)
			  )
			  AND (
					(@IsNonQm = 0)
				OR	(@IsNonQm = 1 AND IsDisplayInNonQmQuickPricer = 1)
			  )
          ))

	ORDER BY lLpTemplateNm
end
else
begin 

	if( @FirstLienLpId != '00000000-0000-0000-0000-000000000000' )
	begin 
		declare @PaidId1stLienStr varchar(50)
		DECLARE @IsPairedOnlyWithSameInvestor varchar(5)
		DECLARE @FirstInvestorNm varchar(100)
		DECLARE @FirstProductCode varchar(100)

		
		SELECT @PaidId1stLienStr = PairIdFor1stLienProdGuid, @IsPairedOnlyWithSameInvestor=IsPairedOnlyWithSameInvestor,
               @FirstInvestorNm = lLpInvestorNm, @FirstProductCode=ProductCode
		FROM loan_program_template
		WHERE lLpTemplateId = @FirstLienLpId;


		SELECT DISTINCT prod.lLpTemplateId, lLpTemplateNm, FolderID, lLpInvestorNm, ProductCode, lLT,
			            @FirstInvestorNm AS FirstInvestorName, @FirstProductCode AS FirstProductCode, lLpProductType,
			            DATALENGTH( PairingProductIds ) AS PairingProductIdsLength 
		FROM Loan_Program_Template prod 
		WHERE prod.BrokerID = @BrokerId
	      AND IsLpe = 1 and IsMaster = 0 and IsEnabled = 1 
		  AND (lLienPosT = @LienPosTIncluded1 or lLienPosT = @LienPosTIncluded2 )
		  AND (@FilterOnlyCanBeStandAlone2nd = 0 OR CanBeStandAlone2nd = 1)
		  AND (IsLpeDummyProgram = 1 OR (
			  (
					 (@FilterDue10Yrs = 1 AND lTerm = 120)
				  OR (@FilterDue15Yrs = 1 AND lTerm = 180)
				  OR (@FilterDue20Yrs = 1 AND lTerm = 240)
				  OR (@FilterDue25Yrs = 1 AND lTerm = 300)
				  OR (@FilterDue30Yrs = 1 AND lTerm = 360)
				  OR (@FilterDueOther = 1 AND @Include10yrAnd25yrInOtherDueFilter=1 AND lTerm NOT IN (120, 180, 240, 300, 360))
				  OR (@FilterDueOther = 1 AND @Include10yrAnd25yrInOtherDueFilter=0 AND lTerm NOT IN (180, 240, 360))
				  )
			  AND (lLpProductType = COALESCE(@FilterLpProductType, lLpProductType))
			  AND (lLpInvestorNm=COALESCE(@FilterLpInvestorName, lLpInvestorNm))
			  AND (
				  (@FilterFinMethFixed = 1 AND lFinMethT = 0 AND (IsOptionArm = 0 AND lPmtAdjCapR = 0 AND lPmtAdjCapMon=0 AND lPmtAdjRecastPeriodMon=0 AND lPmtAdjRecastStop=0 AND lPmtAdjMaxBalPc=0))
				  OR (@FilterFinMeth3YrsArm = 1 AND (lFinMethT = 1 AND lRAdj1stCapMon = 36) AND (IsOptionArm = 0 AND lPmtAdjCapR = 0 AND lPmtAdjCapMon=0 AND lPmtAdjRecastPeriodMon=0 AND lPmtAdjRecastStop=0 AND lPmtAdjMaxBalPc=0))
				  OR (@FilterFinMeth5YrsArm = 1 AND (lFinMethT = 1 AND lRAdj1stCapMon = 60) AND (IsOptionArm = 0 AND lPmtAdjCapR = 0 AND lPmtAdjCapMon=0 AND lPmtAdjRecastPeriodMon=0 AND lPmtAdjRecastStop=0 AND lPmtAdjMaxBalPc=0))
				  OR (@FilterFinMeth7YrsArm = 1 AND (lFinMethT = 1 AND lRAdj1stCapMon = 84) AND (IsOptionArm = 0 AND lPmtAdjCapR = 0 AND lPmtAdjCapMon=0 AND lPmtAdjRecastPeriodMon=0 AND lPmtAdjRecastStop=0 AND lPmtAdjMaxBalPc=0))
				  OR (@FilterFinMeth10YrsArm = 1 AND (lFinMethT = 1 AND lRAdj1stCapMon = 120) AND (IsOptionArm = 0 AND lPmtAdjCapR = 0 AND lPmtAdjCapMon=0 AND lPmtAdjRecastPeriodMon=0 AND lPmtAdjRecastStop=0 AND lPmtAdjMaxBalPc=0))
  			      OR (@FilterFinMethOther = 1 AND ((lFinMethT = 1 AND lRAdj1stCapMon NOT IN (36, 60, 84, 120)) OR lFinMethT > 1 OR IsOptionArm = 1 OR lPmtAdjCapR <> 0 OR lPmtAdjCapMon<>0 OR lPmtAdjRecastPeriodMon<>0 OR lPmtAdjRecastStop<>0 OR lPmtAdjMaxBalPc<>0))
			  )
			  AND (
					 (@FilterPaymentTypePI = 1 AND lIOnlyMon <= 0)
				  OR (@FilterPaymentTypeIOnly = 1 AND lIOnlyMon > 0)
			  )
			  AND ( 
                    ( (@IsPairedOnlyWithSameInvestor = 0) AND (( DATALENGTH(PairingProductIds) = 0) OR (PairingProductIds LIKE '%' + @PaidId1stLienStr + '%')))
                    OR ( (@IsPairedOnlyWithSameInvestor = 1) AND (lLpInvestorNm = @FirstInvestorNm) AND ( (DATALENGTH(PairingProductIds) = 0) OR (PairingProductIds LIKE '%' + @PaidId1stLienStr + '%')))
                  )
			  AND (
					 (@IsNonQm = 0)
				  OR (@IsNonQm = 1 AND IsDisplayInNonQmQuickPricer = 1)
			  )
          ))

	ORDER BY lLpTemplateNm



	end
end