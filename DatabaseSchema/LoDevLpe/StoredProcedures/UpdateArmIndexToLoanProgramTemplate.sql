
-- =============================================
-- Author:		David Dao
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE dbo.UpdateArmIndexToLoanProgramTemplate
    @IndexIdGuid UniqueIdentifier,
    @IndexNameVstr varchar(100),
    @IndexCurrentValueDecimal decimal(9,3),
    @EffectiveD smalldatetime,
    @FreddieArmIndexT int,
    @IndexBasedOnVstr varchar(200),
    @IndexCanBeFoundVstr varchar(200)
    
AS
BEGIN

    UPDATE LOAN_PROGRAM_TEMPLATE
    SET
        lArmIndexNameVstr = @IndexNameVstr,
	    lRAdjIndexR = @IndexCurrentValueDecimal,
		lArmIndexEffectiveD = @EffectiveD,
		lFreddieArmIndexT = @FreddieArmIndexT,
		lArmIndexBasedOnVstr = @IndexBasedOnVstr,
		lArmIndexCanBeFoundVstr = @IndexCanBeFoundVstr
	WHERE lArmIndexGuid = @IndexIdGuid
    
END
