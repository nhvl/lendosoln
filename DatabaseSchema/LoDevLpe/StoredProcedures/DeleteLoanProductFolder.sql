CREATE PROCEDURE [dbo].[DeleteLoanProductFolder]
	@BrokerId Guid , @FolderId Guid
AS
	DECLARE @tranPoint sysname
	SET @tranPoint = object_name( @@procId ) + CAST( @@nestlevel as varchar(10) )
	BEGIN TRANSACTION
	SAVE TRANSACTION @tranPoint
	DELETE FROM Loan_Product_Folder
	WHERE
		BrokerId = @BrokerId
		AND
		FolderId = @FolderId
	IF( 0!=@@error )
	BEGIN
		RAISERROR('Error in deleting from Loan_Product_Folder table in DeleteLoanProductFolder sp', 16, 1);
		goto ErrorHandler;
	END	
	COMMIT TRANSACTION
	RETURN 0;
ErrorHandler:
	ROLLBACK TRANSACTION @tranPoint
	ROLLBACK TRANSACTION
	RETURN -100;
