CREATE  PROCEDURE [dbo].[CreateDisabledLoanProgramInfo]
	@InvestorName varchar(100),
	@ProductCode varchar(100),
	@DisableStatus varchar(50),
	@SaeName varchar(100),
	@Notes varchar(5000) = ''
AS
  INSERT INTO DISABLED_BY_SAE ( InvestorName, ProductCode, DisableStatus, SaeName, Notes )
  VALUES ( @InvestorName, @ProductCode, @DisableStatus, @SaeName, @Notes )
