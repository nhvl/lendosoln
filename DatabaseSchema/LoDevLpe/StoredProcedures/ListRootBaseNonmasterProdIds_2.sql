CREATE PROCEDURE [dbo].[ListRootBaseNonmasterProdIds_2]
as
-- Only select products that pass the following 3 criteria:
-- 1) Not a master
-- 2) Must have derived child
-- 3) Doesn't derive from anybody
select lLpTemplateId, lBaseLpId
from loan_program_template progMain where lBaseLpId is null and IsLpe = 1 and IsMaster = 0  
/* 7/19/07: I replaced this block with the following block to improve performance. -tn.
	and lLpTemplateId in 
		( select lBaseLpId 
		  from loan_program_template 
		  where lBaseLpId is not null and IsLpe = 1 )
*/
and exists (	select * 
				from loan_program_template progSub 
				where progSub.lBaseLpId is not null 
						and progSub.IsLpe = 1 
						and progSub.lBaseLpId = progMain.lLpTemplateId )
