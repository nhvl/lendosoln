
CREATE PROCEDURE [dbo].[ListDocMagicPlanCodes]
	@InvestorNm varchar(100) = null
	, @ListAll bit = 0
AS
BEGIN
	SELECT 
	PlanCodeId
	, PlanCodeNm
	, PlanCodeDesc
	, CASE WHEN InvestorNm IS NULL THEN 'GENERIC' ELSE InvestorNm END as InvestorNm
	, RuleXmlContent
			
	FROM DOC_MAGIC_PLAN_CODE
	WHERE 
	InvestorNm = @InvestorNm 
	OR (@InvestorNm IS NULL AND InvestorNm IS NULL)
	OR @ListAll = 1
END
