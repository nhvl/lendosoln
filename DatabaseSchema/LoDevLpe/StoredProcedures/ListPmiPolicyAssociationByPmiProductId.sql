
CREATE proc dbo.ListPmiPolicyAssociationByPmiProductId
	@PmiProductId uniqueidentifier
as

if (select top(1) MIType from PMI_PRODUCT where ProductId = @PmiProductId) = 0
begin
	Declare @FolderId uniqueidentifier
	set @FolderId = (select top(1) FolderId from PMI_PRODUCT where ProductId = @PmiProductId)
	SELECT * FROM PMI_POLICY_ASSOC WHERE PmiProductId in
	(SELECT ProductId FROM PMI_PRODUCT WHERE FolderId = @FolderId)
end
else
	SELECT * FROM PMI_POLICY_ASSOC WHERE PmiProductId = @PmiProductId
