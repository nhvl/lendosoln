CREATE  PROCEDURE [dbo].[RetrieveLpInvestorNm] 
	@lLpTemplateId Guid
AS

SELECT lLpTemplateId, lLpInvestorNm, ProductCode FROM Loan_Program_Template WHERE lLpTemplateId = @lLpTemplateId
