CREATE  PROCEDURE [dbo].[GetDisabledLoanProgramInfo]
	@ProductCode varchar(100),
	@InvestorName varchar(100)
AS
	SELECT * FROM DISABLED_BY_SAE 
	WHERE ProductCode = @ProductCode AND InvestorName = @InvestorName
