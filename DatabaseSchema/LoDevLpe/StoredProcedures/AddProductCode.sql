CREATE PROCEDURE [dbo].[AddProductCode]
	@InvestorName VarChar( 100 ),
	@ProductCode VarChar( 65 ),
	@IsValid bit = 1,
	@IsRateSheetExpirationFeatureDeployed bit = 0
AS
	INSERT INTO PRODUCT ( InvestorName, ProductCode, IsValid, IsRateSheetExpirationFeatureDeployed )
	VALUES ( @InvestorName, @ProductCode, @IsValid, @IsRateSheetExpirationFeatureDeployed  )
