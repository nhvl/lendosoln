CREATE PROCEDURE [dbo].[CreateDocMagicPlanCode]
		@PlanCodeId UniqueIdentifier OUT
		, @PlanCodeNm varchar(100)
		, @PlanCodeDesc varchar(300)
		, @InvestorNm varchar(100) = null
		, @RuleXmlContent varchar(max)

AS
	SET @PlanCodeId = newid()
	
INSERT INTO DOC_MAGIC_PLAN_CODE
( 
	PlanCodeId
	, PlanCodeNm
	, PlanCodeDesc
	, InvestorNm
	, RuleXmlContent 
)
values
( 
	@PlanCodeId
	, @PlanCodeNm
	, @PlanCodeDesc
	, CASE WHEN @InvestorNm = 'GENERIC' THEN NULL ELSE @InvestorNm END
	, @RuleXmlContent 
)
