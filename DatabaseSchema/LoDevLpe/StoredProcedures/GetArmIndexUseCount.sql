CREATE PROCEDURE [dbo].[GetArmIndexUseCount]
	@IndexId Guid
AS
	SELECT count( l.lLpTemplateNm )
	FROM
		Loan_Program_Template AS l
	WHERE
		lArmIndexGuid = @IndexId
	IF( @@error != 0 )
	BEGIN
		RAISERROR('Error in the select statement in GetARMIndexUseCount sp', 16, 1);
		RETURN -100;
	END
	RETURN 0
