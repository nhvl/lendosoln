CREATE  PROCEDURE [dbo].[AddInvestorName]
  @InvestorName varchar(100)
  , @IsValid bit = 1
AS
  INSERT INTO Investor_Name ( InvestorName, IsValid )
  VALUES ( @InvestorName, @IsValid )
