-- =============================================
-- Author:		Michael Leinweaver
-- Create date: 1/22/2016
-- Description:	Obtains a list of invalid investor programs.
-- =============================================
ALTER PROCEDURE [dbo].[SAEDB_ListInvalidInvestorPrograms] 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT lpe.BrokerId, lLpInvestorNm, folderid, lLpTemplateNm, 'Master' = CASE WHEN isMaster = 1 THEN 'Yes' ELSE 'No' END, ProductCode
	FROM LOAN_PROGRAM_TEMPLATE lpe
	WHERE 
		lBaseLpId IS NULL AND        --not derived
		IsLpe = 1 AND				 --created by SAE
		IsMasterPriceGroup = 0 AND
		lLpInvestorNm IN 
			(
			SELECT InvestorName
			FROM INVESTOR_NAME
			WHERE IsValid = 0
			)
END