
-- =============================================
-- Author:		paoloa
-- Create date: 4/29/2013
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[ListManualDisabledInvestorProductByBrokerIdAllPolicies]
	@BrokerId UniqueIdentifier
AS
BEGIN
	SELECT InvestorName, ProductCode, DisableStatus, MessagesToSubmittingUsers, 
           DisableEntryModifiedDate, DisableEntryModifiedByEmployeeId, DisableEntryModifiedByUserName,
			DisableDateTime, IsHiddenFromResult, Notes, LockPolicyId
    FROM DISABLED_BY_LENDER_LOCK_POLICY
    WHERE BrokerId = @BrokerId
    ORDER BY InvestorName, ProductCode
END
