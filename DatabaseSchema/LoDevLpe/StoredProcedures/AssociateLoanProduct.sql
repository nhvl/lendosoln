CREATE PROCEDURE [dbo].[AssociateLoanProduct]
	@LoanProductID Guid , @PricePolicyId Guid , @AssocOverrideTri TinyInt
AS
	INSERT INTO Product_Price_Association( PricePolicyId , ProductId , AssocOverrideTri )
	VALUES ( @PricePolicyId , @LoanProductId , @AssocOverrideTri )
