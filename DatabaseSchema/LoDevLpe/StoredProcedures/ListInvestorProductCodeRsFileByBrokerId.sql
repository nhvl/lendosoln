-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ListInvestorProductCodeRsFileByBrokerId] 
	@BrokerId UniqueIdentifier
AS
BEGIN
select distinct lLpInvestorNm, ProductCode, LpeAcceptableRsFileId, LpeAcceptableRsFileVersionNumber
from loan_program_template lp left join rate_options ro on ro.srcprogid=lp.srcrateoptionsprogid
where
lp.islpe=1 and lp.isenabled=1 and lp.ismaster=0
and BrokerId = @BrokerId
order by lLpInvestorNm, productcode
END
