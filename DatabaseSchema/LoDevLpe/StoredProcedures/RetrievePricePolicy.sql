ALTER PROCEDURE [dbo].[RetrievePricePolicy]
	@PricePolicyId GUID
AS
	SELECT PricePolicyId, BrokerId, CreateDate, PricePolicyDescription, Notes, PricePolicyXmlText, IsPublished, ParentPolicyId, IsExpandedNode, MutualExclusiveExecSortedId, HasQualifiedRule, VersionTimestamp, ContentKey
	FROM PRICE_POLICY
	WHERE PricePolicyId = @PricePolicyId
