CREATE    PROCEDURE [dbo].[CreateLpeSsIfDataOldEnough]
    @SnapshotName as varchar(100) 
as

declare @secondsAgeRequired int;
declare @lastUpdateD as datetime;
declare @secondsDiff as int;

declare @lastUpdateD_again as datetime;

set @secondsAgeRequired = 60;

--NOTE: We cannot create a database within a transaction with other statements. -tn 9/6/06

select @lastUpdateD = LastUpdatedD from price_engine_version
set @secondsDiff = DATEDIFF( second, @lastUpdateD, getdate() );

if( @secondsDiff >= @secondsAgeRequired )
begin 
 	exec CreateDbSnapshot @SnapshotName
	if( 0!=@@error )
	begin
		RAISERROR('Error in executing stored proc CreateDbSnapshot', 16, 1);
		select -1;
		return;
	end

    -- ThienNguyen  5/29/2008 - opm 22458 : make sure lpe data is not modified during executing stored proc CreateDbSnapsho
    select @lastUpdateD_again = LastUpdatedD from price_engine_version

    -- select @lastUpdateD_again = getdate() -- make bug for testing.

    if( @lastUpdateD != @lastUpdateD_again )
    begin
        exec DropDbSnapshot @SnapshotName  -- LpeRelease also remove this snapshot soon if it exists by some reason.
		RAISERROR('LPE data was modified during executing stored proc CreateDbSnapshot', 16, 1);
		select -1;
		return;
	end


	select 0;
	return;
end
-- returning the # of seconds left to reach the required age
select @secondsAgeRequired - @secondsDiff;
return;
