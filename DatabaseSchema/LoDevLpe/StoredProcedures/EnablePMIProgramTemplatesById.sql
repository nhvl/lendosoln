create proc [dbo].[EnablePMIProgramTemplatesById]
	@ProductId uniqueidentifier
as
update dbo.PMI_PRODUCT
set IsEnabled = 1
where ProductId = @ProductId
