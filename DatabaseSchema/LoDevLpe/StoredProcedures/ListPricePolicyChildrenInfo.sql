CREATE PROCEDURE dbo.ListPricePolicyChildrenInfo
	@PricePolicyId Guid
AS	SELECT p.PricePolicyId , p.PricePolicyDescription , p.ParentPolicyId
 	FROM Price_Policy AS p
	WHERE
		p.ParentPolicyId = @PricePolicyId
	ORDER BY MutualExclusiveExecSortedId,PricePolicyDescription
	IF ( @@error != 0 )
	BEGIN
		RAISERROR('Error in the select statement in ListPricePolicyChildrenInfo sp', 16, 1);
		RETURN -100;
	END
