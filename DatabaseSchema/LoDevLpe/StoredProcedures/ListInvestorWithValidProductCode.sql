CREATE PROCEDURE [dbo].[ListInvestorWithValidProductCode]
AS
   SELECT COUNT(DISTINCT InvestorName) 
	FROM PRODUCT
	WHERE IsValid = 1;
