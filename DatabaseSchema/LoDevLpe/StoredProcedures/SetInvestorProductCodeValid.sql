CREATE PROCEDURE [dbo].[SetInvestorProductCodeValid]
	@InvestorName VarChar( 100 ),
	@ProductCode VarChar( 65 ),
	@IsValid bit,
	@IsRateSheetExpirationFeatureDeployed bit
AS
	UPDATE PRODUCT
	SET IsValid = @IsValid,
	IsRateSheetExpirationFeatureDeployed = @IsRateSheetExpirationFeatureDeployed
	WHERE InvestorName = @InvestorName
		AND ProductCode = @ProductCode
