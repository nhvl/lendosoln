CREATE PROCEDURE [dbo].[RetrieveAllPricePolicies]
AS
	SELECT *
	FROM PRICE_POLICY
	order by ParentPolicyId, MutualExclusiveExecSortedId asc
