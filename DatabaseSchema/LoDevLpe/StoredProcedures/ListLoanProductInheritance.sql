CREATE  PROCEDURE [dbo].[ListLoanProductInheritance]

	@ProductId Guid

AS

	-- We gather a complex dataset by looping through the rows connected
	-- by base id chaining.  Only a data set fill will be able to handle this.  Using
	-- a reader will just give you the first table (which is not the chain).  Expect
	-- the chain to be returned in order from given to most-base with each link
	-- residing in a separate table.

	DECLARE @CurrentId Guid
	SET @CurrentId = @ProductId
	DECLARE @Count Int
	SET @Count = 1

	WHILE( @Count < 1000 )
	BEGIN

		-- For the current product id, initialize some containers and build
		-- up the folder path.  We do this here so we don't need the whole
		-- tree in the application with every view of the inheritance page.
		-- After we gather the path, we load up our result set with exactly
		-- one table that has one row, which contains the current product's
		-- details (including the path we just assembled).

		DECLARE @BaseId Guid
		DECLARE @FolderId Guid
		DECLARE @LpPath Varchar( 2048 )
		SET @LpPath = ''

		SELECT @FolderId = p.FolderId FROM Loan_Program_Template AS p
		WHERE
			p.lLpTemplateId = @CurrentId

		WHILE( @FolderId IS NOT NULL )
		BEGIN

			DECLARE @Folder Varchar( 128 )
			SELECT @Folder = f.FolderName , @FolderId = f.ParentFolderId FROM Loan_Product_Folder AS f
			WHERE
				f.FolderId = @FolderId
			SET @LpPath = @Folder + '\' + @LpPath

		END

		SELECT @BaseId = p.lBaseLpId	FROM Loan_Program_Template AS p
		WHERE
			p.lLpTemplateId = @CurrentId

		SELECT
			p.lLpTemplateId ,
			p.lLpTemplateNm ,
			@LpPath AS lLpPath ,
			p.lBaseLpId ,
			p.BrokerId
		FROM
			Loan_Program_Template AS p
		WHERE
			p.lLpTemplateId = @CurrentId

		IF( @BaseId IS NULL ) RETURN 0

		SET @CurrentId = @BaseId
		SET @Count = @Count + 1

	END
