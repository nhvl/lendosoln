-- =============================================
-- Author:		Michael Leinweaver
-- Create date: 1/22/2016
-- Description:	Obtains a list of loan programs with no loan program type.
-- =============================================
ALTER PROCEDURE [dbo].[SAEDB_NoLoanProductType] AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT l.BrokerId, ISNULL(f.folderName,'Root Folder') AS 'FolderName', l.lLpTemplateNm as 'TemplateName', l.CreatedD
	FROM loan_program_template as l LEFT JOIN loan_product_folder as f ON l.folderId = f.folderId
	WHERE isMaster = 0            --not a master
		AND lBaseLpId IS NULL        --not derived
		AND lLpProductType LIKE ''    --empty product type
END