CREATE PROCEDURE [dbo].[IsCCTemplateAssociatedWithLoanProgram]
@BrokerID uniqueidentifier,	
@lCcTemplateId uniqueidentifier
AS
	SELECT TOP 1 ' '
	FROM LOAN_PROGRAM_TEMPLATE
	WHERE BrokerId = @BrokerID AND lCcTemplateId = @lCcTemplateId
