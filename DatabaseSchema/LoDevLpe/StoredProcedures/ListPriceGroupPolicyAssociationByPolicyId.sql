CREATE  PROCEDURE [dbo].[ListPriceGroupPolicyAssociationByPolicyId] 
	@PricePolicyId uniqueidentifier
AS
SELECT AssocOverrideTri
	, ProductId AS LpePriceGroupId
FROM Product_Price_Association
WHERE PricePolicyId = @PricePolicyId AND AssocOverrideTri <> 0
  AND ProductId IN (SELECT lLpTemplateId FROM Loan_Program_Template WHERE IsMasterPriceGroup = 1)
