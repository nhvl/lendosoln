
CREATE PROCEDURE [dbo].ListPMIProductFolders
AS
	SELECT
		FolderId, 
		FolderName, 
		ParentFolderId
	FROM
		PMI_PRODUCT_FOLDER
	ORDER BY
		FolderName
