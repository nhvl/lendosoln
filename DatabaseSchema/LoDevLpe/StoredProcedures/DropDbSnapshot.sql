ALTER procedure [dbo].[DropDbSnapshot]
	@SnapshotName AS VARCHAR(100)
AS
IF( @SnapshotName NOT LIKE 'ss%' )
BEGIN
	RAISERROR('Error: To avoid dropping the source database by mistake because of naming mix-up, snapshot name must have ''ss'' prefix. Msg from CreateDbSnapshot sp', 16, 1);
	RETURN -100;
END

IF( @SnapshotName LIKE '%[^a-zA-Z0-9_]%' ) -- adapted from http://stackoverflow.com/a/2558794/420667
BEGIN
	RAISERROR('Error: To avoid SQL injection, snapshot name may contain only alphanumeric characters and ''_''. Msg from CreateDbSnapshot sp', 16, 1);
	RETURN -100;
END

-- do not change this condition without considering the effects on sql injection.
IF( EXISTS ( SELECT [name] FROM master.dbo.[sysdatabases] WHERE ([name] = @SnapshotName ) ) )
BEGIN
	DECLARE @sql NVARCHAR(2000)
	SET @sql = 'drop database ' + @SnapshotName  + ';'
	EXECUTE (@sql);
END
