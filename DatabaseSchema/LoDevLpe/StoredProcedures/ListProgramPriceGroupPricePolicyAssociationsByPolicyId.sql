CREATE PROCEDURE [dbo].[ListProgramPriceGroupPricePolicyAssociationsByPolicyId]
	@PricePolicyId uniqueidentifier
AS

SELECT 
  lpt.BrokerId
  , pppa.LpePriceGroupId
  , pppa.ProgId
  , pppa.AssociateType
  , lpt.lLpTemplateNm

FROM PRICEGROUP_PROG_POLICY_ASSOC pppa
  LEFT JOIN LOAN_PROGRAM_TEMPLATE lpt WITH(NOLOCK) on lpt.lLpTemplateId = pppa.ProgId 
WHERE pppa.PricePolicyId = @PricePolicyId 
   AND pppa.AssociateType <> 0
