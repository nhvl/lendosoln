
-- =============================================
-- Author:		paoloa
-- Create date: 4/29/2013
-- Description:	
-- =============================================
CREATE PROCEDURE dbo.DeleteManualDisabledInvestorProductPerBrokerAllPolicies
	@BrokerId UniqueIdentifier,
	@InvestorName varchar(100),
	@ProductCode varchar(100)
AS
BEGIN
	DELETE FROM DISABLED_BY_LENDER_LOCK_POLICY 
	WHERE BrokerId = @BrokerId AND InvestorName = @InvestorName AND ProductCode = @ProductCode
END
