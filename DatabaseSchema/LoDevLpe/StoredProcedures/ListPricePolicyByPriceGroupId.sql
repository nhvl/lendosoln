CREATE PROCEDURE [dbo].[ListPricePolicyByPriceGroupId]
	@PriceGroupId uniqueidentifier
	
AS
	SELECT 
      p.PricePolicyId
      , p.PricePolicyDescription
      , p.ParentPolicyID
      , p.IsPublished
      , pa.AssocOverrideTri as PriceGroupAssociateType
      , p.IsExpandedNode
	FROM Price_Policy p LEFT JOIN Product_Price_Association pa
      ON pa.PricePolicyId = p.PricePolicyId
        AND pa.ProductId = @PriceGroupId
        AND pa.AssocOverrideTri <> 0
	ORDER BY p.MutualExclusiveExecSortedId, p.PricePolicyDescription
