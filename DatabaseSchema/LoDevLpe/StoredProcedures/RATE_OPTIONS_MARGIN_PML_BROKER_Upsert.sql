CREATE PROCEDURE dbo.RATE_OPTIONS_MARGIN_PML_BROKER_Upsert
    @BrokerId UniqueIdentifier,
    @PmlBrokerIndexNumber int,
    @RatesMarginProtobufContent varbinary(max),
    @LastModifiedDate datetime
AS
BEGIN

    UPDATE RATE_OPTIONS_MARGIN_PML_BROKER
      SET RatesMarginProtobufContent = @RatesMarginProtobufContent,
          LastModifiedDate = @LastModifiedDate
    WHERE BrokerId = @BrokerId AND PmlBrokerIndexNumber = @PmlBrokerIndexNumber
    
    IF @@ROWCOUNT = 0 
    BEGIN
        INSERT INTO RATE_OPTIONS_MARGIN_PML_BROKER(BrokerId, PmlBrokerIndexNumber, RatesMarginProtobufContent, LastModifiedDate)
                                           VALUES (@BrokerId, @PmlBrokerIndexNumber, @RatesMarginProtobufContent, @LastModifiedDate)
    END

END