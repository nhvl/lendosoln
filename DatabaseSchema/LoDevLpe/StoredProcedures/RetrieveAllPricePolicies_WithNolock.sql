CREATE PROCEDURE [dbo].[RetrieveAllPricePolicies_WithNolock]
AS
	SELECT *
	FROM PRICE_POLICY with (nolock)
	order by ParentPolicyId, MutualExclusiveExecSortedId asc
