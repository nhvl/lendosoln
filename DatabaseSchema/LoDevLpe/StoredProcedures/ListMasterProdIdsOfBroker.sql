CREATE PROCEDURE [dbo].[ListMasterProdIdsOfBroker]
	@brokerid uniqueidentifier
as
	select lLpTemplateId
	from loan_program_template
	where brokerid = @brokerid and ismaster = 1 and ismasterpricegroup = 0 and IsLpe =1
