-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Internal_RetrieveRateOptionsBySrcProgId]
	@SrcProgId UniqueIdentifier
AS
BEGIN
	SELECT lRateSheetXmlContent 
	FROM RATE_OPTIONS
	WHERE SrcProgId = @SrcProgId
END
