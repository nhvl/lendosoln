-- =============================================
-- Author:		Eric Mallare
-- Create date: 1/30/2017
-- Description:	Gets the migrated manually disabled investor products for the broker.
-- =============================================
ALTER PROCEDURE [dbo].[RetrieveMigratedManualDisabledInvestorProductForBroker]
	@BrokerId uniqueidentifier
AS
BEGIN
	SELECT DISTINCT InvestorName, ProductCode,DisableStatus, MessagesToSubmittingUsers,DisableEntryModifiedDate,
           DisableEntryModifiedByEmployeeId, DisableEntryModifiedByUserName, DisableDateTime, IsHiddenFromResult, Notes, LockPolicyId
    FROM DISABLED_BY_LENDER_LOCK_POLICY
	WHERE BrokerId = @BrokerId
END
