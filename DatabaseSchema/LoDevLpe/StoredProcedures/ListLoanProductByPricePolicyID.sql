CREATE PROCEDURE [dbo].[ListLoanProductByPricePolicyID]
	@BrokerId Guid , @PricePolicyId Guid = NULL
AS
	SELECT a.PricePolicyId , a.ProductId , a.AssocOverrideTri
	FROM Product_Price_Association AS a JOIN Price_Policy AS p ON p.PricePolicyId = a.PricePolicyId
	WHERE
		p.BrokerId = @BrokerId
		AND
		a.PricePolicyId = COALESCE( @PricePolicyId , a.PricePolicyId )
