-- =============================================
-- Author:		Thien Nguyen
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[LOAN_PROGRAM_TEMPLATE_Reset_lBaseLpId]
	@lLpTemplateId Guid
AS
BEGIN
    UPDATE LOAN_PROGRAM_TEMPLATE
    SET	lBaseLpId = null
    WHERE lLpTemplateId = @lLpTemplateId    
END
