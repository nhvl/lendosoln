-- =============================================
-- Author:		Eric Mallare
-- Create date: 1/30/2017
-- Description:	Gets the unmigrated manually disabled investor products for the broker.
-- =============================================
ALTER PROCEDURE [dbo].[RetrieveUnmigratedManualDisabledInvestorProductForBroker]
	@BrokerId uniqueidentifier
AS
BEGIN
	SELECT InvestorName, ProductCode,DisableStatus, MessagesToSubmittingUsers,DisableEntryModifiedDate,
           DisableEntryModifiedByEmployeeId, DisableEntryModifiedByUserName, DisableDateTime, IsHiddenFromResult, Notes
    FROM Disabled_By_Lender 
	WHERE BrokerId = @BrokerId
END
