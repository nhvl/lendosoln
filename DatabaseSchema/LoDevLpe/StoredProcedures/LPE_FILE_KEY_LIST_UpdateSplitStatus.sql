CREATE PROCEDURE [dbo].[LPE_FILE_KEY_LIST_UpdateSplitStatus]
	@Id int,
	@FileKey varchar(100)
AS

BEGIN
	UPDATE LPE_FILE_KEY_LIST
	SET	IsProcessedSplitPerLender = 1
	WHERE Id = @Id AND FileKey = @FileKey AND
  	      FileType='PRICING_SNAPSHOT' AND IsProcessedSplitPerLender = 0 
END
