CREATE  PROCEDURE [dbo].[ListLoanProductByBrokerID]
	@BrokerId uniqueidentifier , @IsLpe Bit
AS
	SELECT
		t.lLpTemplateId , t.lLpTemplateNm , t.lBaseLpId , t.IsEnabled , t.IsMaster , t.FolderId, t.lLpInvestorNm
	FROM
		Loan_Program_Template AS t
	WHERE
		t.BrokerID = @BrokerID
		AND
		t.IsLpe = @IsLpe
		AND
		t.IsMasterPriceGroup = 0
	ORDER BY
		t.lLpTemplateNm
