CREATE  PROCEDURE [dbo].[InvestorUpdate]
  @InvestorName varchar(100),
  @IsValid bit,
  @RateLockCutOffTime DateTime,
  @UseLenderTimezoneForRsExpiration bit

AS
  UPDATE Investor_Name
	SET IsValid = @IsValid,
        RateLockCutOffTime = @RateLockCutOffTime,
		UseLenderTimezoneForRsExpiration = @UseLenderTimezoneForRsExpiration
	WHERE InvestorName = @InvestorName
