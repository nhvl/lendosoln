CREATE PROCEDURE [dbo].[ListLoanProductFolderByBrokerID]
	@BrokerId Guid , @IsLpe Bit , @FolderId Guid = NULL
AS
	SELECT
		f.FolderId , f.FolderName , f.ParentFolderId
	FROM
		Loan_Product_Folder AS f
	WHERE
		f.BrokerID = @BrokerID
		AND
		f.IsLpe = @IsLpe
		AND
		f.FolderId = COALESCE( @FolderId , f.FolderId )
	ORDER BY
		FolderName
