CREATE    PROCEDURE [dbo].[DataOldEnough]
as

declare @secondsAgeRequired int;
declare @lastUpdateD as datetime;
declare @secondsDiff as int;

set @secondsAgeRequired = 60; -- relate with CreateLpeSsIfDataOldEnough


select @lastUpdateD = LastUpdatedD from price_engine_version
set @secondsDiff = DATEDIFF( second, @lastUpdateD, getdate() );

if( @secondsDiff >= @secondsAgeRequired )
begin 
	select 0;
	return;
end
-- returning the # of seconds left to reach the required age
select @secondsAgeRequired - @secondsDiff;
return;
