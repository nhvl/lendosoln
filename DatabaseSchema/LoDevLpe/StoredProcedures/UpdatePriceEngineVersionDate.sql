CREATE      PROCEDURE [dbo].[UpdatePriceEngineVersionDate]
as 


/************
Thien decided to turn off this feature because
  a. Now PB shows dbAge
  b. Improve performance for lpeUpdate

declare @now  datetime;
Set @now = getdate()


SET NOCOUNT ON

-- save idle interval 
Insert into price_engine_version_history(StartTime, EndTime)
    select LastUpdatedD StartTime, @now EndTime
    from  price_engine_version
    where LastUpdatedD <= DateAdd(second, -60, @now)

SET NOCOUNT OFF
update price_engine_version
   set LastUpdatedD = @now

***************/

update price_engine_version
set LastUpdatedD = getdate()
