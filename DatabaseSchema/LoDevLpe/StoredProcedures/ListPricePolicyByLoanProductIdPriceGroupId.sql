CREATE PROCEDURE [dbo].[ListPricePolicyByLoanProductIdPriceGroupId]
	@LoanProductId uniqueidentifier
	, @PriceGroupId uniqueidentifier
	
AS
	SELECT 
      p.PricePolicyId
      , p.PricePolicyDescription
      , p.ParentPolicyID
      , p.IsPublished
      , pa.AssocOverrideTri
      , pa.InheritValFromBaseProd
      , pa.InheritVal
      , p.IsExpandedNode
      , pga.AssociateType as PriceGroupProgramAssociateType
      , papg.AssocOverrideTri as PriceGroupAssociateType
	FROM Price_Policy p LEFT JOIN Product_Price_Association pa
      ON pa.PricePolicyID = p.PricePolicyId AND pa.ProductID = @LoanProductId
      LEFT JOIN PriceGroup_Prog_Policy_Assoc pga
      ON pga.LpePriceGroupId = @PriceGroupId 
        AND pga.ProgId = @LoanProductId
        AND pga.PricePolicyId = p.PricePolicyId
      LEFT JOIN Product_Price_Association papg
      on papg.ProductID = @PriceGroupId and p.PricePolicyId = papg.PricePolicyId
	ORDER BY p.MutualExclusiveExecSortedId, p.PricePolicyDescription
