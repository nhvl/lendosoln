create     PROCEDURE [dbo].[ClearIsSpreadingNeededForProductDataBit]
as 
update loan_program_template 
set IsSpreadingNeededForProductData = 0
where IsSpreadingNeededForProductData = 1
