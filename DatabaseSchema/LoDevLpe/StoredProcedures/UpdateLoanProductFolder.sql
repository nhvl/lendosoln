CREATE PROCEDURE [dbo].[UpdateLoanProductFolder]
	@FolderID Guid,
	@FolderName varchar(36),
	@BrokerID Guid
AS
	UPDATE Loan_Product_Folder
	       SET FolderName = @FolderName
          	WHERE FolderID = @FolderID AND BrokerID = @BrokerID
