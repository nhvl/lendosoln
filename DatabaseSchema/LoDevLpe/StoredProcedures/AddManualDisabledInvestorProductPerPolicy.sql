
-- =============================================
-- Author:		paoloa
-- Create date: 4/29/2013
-- Description:	
-- =============================================
CREATE PROCEDURE dbo.AddManualDisabledInvestorProductPerPolicy
	@BrokerId UniqueIdentifier,
	@PolicyId UniqueIdentifier,
	@InvestorName varchar(100),
	@ProductCode varchar(100),
	@DisableStatus varchar(50),
	@MessagesToSubmittingUsers varchar(5000),
	@DisableEntryModifiedByEmployeeId UniqueIdentifier,
	@DisableEntryModifiedByUserName varchar(100),
	@IsHiddenFromResult bit,
	@Notes varchar(5000)
AS
BEGIN
	INSERT INTO DISABLED_BY_LENDER_LOCK_POLICY (BrokerId, InvestorName, ProductCode, DisableStatus, MessagesToSubmittingUsers,DisableEntryModifiedDate, DisableEntryModifiedByEmployeeId, DisableEntryModifiedByUserName, DisableDateTime, IsHiddenFromResult, Notes, LockPolicyId)
                         VALUES (@BrokerId, @InvestorName, @ProductCode, @DisableStatus, @MessagesToSubmittingUsers, GETDATE(), @DisableEntryModifiedByEmployeeId, @DisableEntryModifiedByUserName, GETDATE(), @IsHiddenFromResult, @Notes, @PolicyId)
END
