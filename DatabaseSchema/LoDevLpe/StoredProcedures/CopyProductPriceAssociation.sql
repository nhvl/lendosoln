CREATE PROCEDURE [dbo].[CopyProductPriceAssociation] 
	@SrcProductId Guid,
	@DestProductId Guid
AS
INSERT INTO Product_Price_Association (PricePolicyId, ProductId, AssocOverrideTri, InheritVal, InheritValFromBaseProd) 
SELECT PricePolicyId, @DestProductId, AssocOverrideTri, InheritVal, InheritValFromBaseProd FROM Product_Price_Association WHERE ProductId = @SrcProductId
