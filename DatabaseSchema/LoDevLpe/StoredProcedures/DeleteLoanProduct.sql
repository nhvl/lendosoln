CREATE   PROCEDURE [dbo].[DeleteLoanProduct]
	@LoanProdId Guid
AS
DECLARE @FolderId UniqueIdentifier
	DECLARE @BrokerId UniqueIdentifier
	DECLARE @IsLpe Bit
	DECLARE @IsMaster Bit
	SELECT @IsMaster = IsMaster , @IsLpe = IsLpe , @FolderId = FolderId , @BrokerId = BrokerId FROM Loan_Program_Template WHERE lLpTemplateId = @LoanProdId
	IF( @IsMaster = 1 )
	BEGIN
		SELECT FolderId FROM Loan_Product_Folder WHERE ParentFolderId = @FolderId AND IsLpe = @IsLpe AND BrokerId = @BrokerId
		IF( @@rowcount > 0 )
		BEGIN
			RAISERROR('Error in deleting from LOAN_PROGRAM_TEMPLATE table in DeleteLoanProduct sp, cannot delete master program when there are subfolders.', 16, 1);
			RETURN -2 -- Master's folder has subfolders...  Denied.
		END
		
		SELECT lLpTemplateId FROM Loan_Program_Template WHERE FolderId = @FolderId AND IsLpe = @IsLpe AND BrokerId = @BrokerId AND lLpTemplateId != @LoanProdId
		IF( @@rowcount > 0 )
		BEGIN
			RAISERROR('Error in deleting from LOAN_PROGRAM_TEMPLATE table in DeleteLoanProduct sp, cannot delete master program when there are other programs in the folder.', 16, 1);
			RETURN -1 -- Master's folder has children...  Denied.
		END
	END
	
	BEGIN TRANSACTION
	
	DELETE FROM Product_Price_Association
	WHERE
		ProductId = @LoanProdId

	DELETE FROM Rate_Options
	WHERE
		SrcProgId = @LoanProdId

  /************ begin of new code **************/ 
  delete from PRICEGROUP_PROG_POLICY_ASSOC
      where ProgId = @LoanProdId

	DELETE FROM PRICEGROUP_PROG_POLICY_ASSOC_VERSION
		WHERE ProgId = @LoanProdId
  /************ end of new code **************/ 



	
	DELETE FROM Loan_Program_Template
	WHERE
		lLpTemplateId = @LoanProdId
	
	IF( @@error != 0 )
	BEGIN
		RAISERROR('Error in deleting from LOAN_PROGRAM_TEMPLATE table in DeleteLoanProduct sp', 16, 1);
		ROLLBACK TRANSACTION
		RETURN -100
	END
	
	COMMIT TRANSACTION
	
	RETURN 0
