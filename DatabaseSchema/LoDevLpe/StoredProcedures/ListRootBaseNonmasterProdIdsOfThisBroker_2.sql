CREATE PROCEDURE [dbo].[ListRootBaseNonmasterProdIdsOfThisBroker_2]
	@BrokerId uniqueidentifier 
as
-- Only select products that pass the following 3 criteria:
-- 1) Not a master
-- 2) Must have derived child
-- 3) Doesn't derive from anybody
select lLpTemplateId, lLpTemplateNm
from loan_program_template as prog1
where brokerId = @BrokerId and lBaseLpId is null and IsMaster = 0 and islpe = 1
/* 7/19/07: I replaced this block with the block below to improve performance.-tn.	
and lLpTemplateId in 
		( select lBaseLpId 
		  from loan_program_template 
		  where lBaseLpId is not null and islpe = 1 )
*/
and exists 
( 
	select * 
	from loan_program_template as prog2
	where prog2.lBaseLpId is not null 
			and prog2.isLpe = 1 
			and prog2.lBaseLpId = prog1.lLpTemplateId )
