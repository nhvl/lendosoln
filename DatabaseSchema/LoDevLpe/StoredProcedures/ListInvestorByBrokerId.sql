-- =============================================
-- Author:		David Dao
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE dbo.ListInvestorByBrokerId
    @BrokerId UniqueIdentifier
AS
BEGIN

SELECT DISTINCT inv.InvestorName,inv.InvestorId
FROM LOAN_PROGRAM_TEMPLATE lp JOIN INVESTOR_NAME inv ON lp.lLpInvestorNm=inv.InvestorName
WHERE BrokerId=@BrokerId
 AND IsLpe=1 AND IsMaster=0 AND IsEnabled=1
END

