CREATE PROCEDURE [dbo].[ListNonlpeLoanProgramsByBrokerId]
	@BrokerId uniqueidentifier
as 
	SELECT lLpTemplateId, lLpTemplateNm, FolderID
	from loan_program_template
	where brokerId = @brokerid and IsLpe = 0
	ORDER BY lLpTemplateNm
