CREATE  PROCEDURE [dbo].[ListLoanProgramNamesByBrokerID]
	@BrokerID uniqueidentifier ,
	@IsLpe Bit 
AS
SELECT
		lLpTemplateId,
		lLpTemplateNm,
		FolderId
	FROM
		Loan_Program_Template 
	WHERE
		BrokerID = @BrokerID AND
		IsLpe = @IsLpe AND 
		IsMasterPriceGroup = 0
	ORDER BY
		IsMaster desc, lLpTemplateNm
