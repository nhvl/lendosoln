CREATE proc [dbo].[RetrievePMIProgramTemplatesByFolder]
	@FolderId uniqueidentifier
as
select ProductId, FolderId, MICompany, MIType, IsEnabled, CreatedD,UfmipIsRefundableOnProRataBasis 
from PMI_PRODUCT
where FolderId = @FolderId
order by MIType
