ALTER procedure [dbo].[CreatePMIProductFolder]
	@FolderName varchar(36),
	@FolderId uniqueidentifier,
	@ParentFolderId Guid = null
AS
	INSERT INTO
		PMI_PRODUCT_FOLDER
		( FolderId
		, FolderName
		, ParentFolderId
		) VALUES
		( @FolderId
		, @FolderName
		, @ParentFolderId
		)
		
	DECLARE @Id uniqueidentifier
	SET @Id = NEWID()
	
	EXEC SavePMIProgramTemplate
	@ProductId = @Id,
	@FolderId = @FolderId,
	@MICompany = 0,
	@MIType = 0,
	@IsEnabled = 0,
	@UfmipIsRefundableOnProRataBasis = 0;
