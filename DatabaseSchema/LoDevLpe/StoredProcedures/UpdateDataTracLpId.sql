CREATE PROCEDURE [dbo].[UpdateDataTracLpId] 
	@lLpTemplateId uniqueidentifier,
	@lDataTracLpId varchar(100) = NULL
AS
UPDATE LOAN_PROGRAM_TEMPLATE
       SET lDataTracLpId = COALESCE(@lDataTracLpId, '')
WHERE lLpTemplateId = @lLpTemplateId
