CREATE  PROCEDURE [dbo].[SetInvestorValid]
  @InvestorName varchar(100)
  , @IsValid bit
AS
  UPDATE Investor_Name
  SET IsValid = @IsValid
  WHERE InvestorName = @InvestorName
