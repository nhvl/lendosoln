CREATE      PROCEDURE [dbo].[ListLoanProgramsNeedProductDataSpreading]
as
select lLpTemplateId
from loan_program_template progMain
where IsSpreadingNeededForProductData = 1 and IsMaster = 0 and IsLpe = 1 and 
/* 7/19/07: I repalce this block with the exists block below to improve performance. -tn.
lLpTemplateId in 
	( select lBaseLpId from loan_program_template )
*/
exists (	select * 
			from loan_program_template progSub 
			where progSub.lBaseLpId = progMain.lLpTemplateId )
