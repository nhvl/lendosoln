CREATE PROCEDURE [dbo].[ListLoanProgramsByCCTemplateID]
@BrokerID uniqueidentifier,	
@lCcTemplateId uniqueidentifier
AS
	SELECT IsMaster,IsEnabled,IsEnabledOverrideBit,lLpTemplateId,lLpTemplateNm,lBaseLpId, 
	CASE 
		WHEN IsMaster = 1 AND lBaseLpId IS NULL
			THEN 'Master'
		WHEN IsMaster = 0 AND lBaseLpId IS NOT NULL
			THEN 'Derived'
		WHEN IsMaster = 0 AND lBaseLpId IS NULL
			THEN ''
		WHEN IsMaster = 1 AND lBaseLpId IS NOT NULL
			THEN 'Invalid'
	END AS Type,
	Enabled = case IsEnabled when 1 then 'Yes' else 'No' end
	
	FROM LOAN_PROGRAM_TEMPLATE
	WHERE BrokerId = @BrokerID AND lCcTemplateId = @lCcTemplateId AND IsMasterPriceGroup = 0
