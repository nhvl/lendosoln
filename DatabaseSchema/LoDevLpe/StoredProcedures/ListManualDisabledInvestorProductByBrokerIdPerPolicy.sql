
-- =============================================
-- Author:		paoloa
-- Create date: 4/29/2013
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[ListManualDisabledInvestorProductByBrokerIdPerPolicy]
	@BrokerId UniqueIdentifier,
	@PolicyId UniqueIdentifier
AS
BEGIN
	SELECT InvestorName, ProductCode, DisableStatus, MessagesToSubmittingUsers, 
           DisableEntryModifiedDate, DisableEntryModifiedByEmployeeId, DisableEntryModifiedByUserName,
			DisableDateTime, IsHiddenFromResult, Notes, LockPolicyId
    FROM DISABLED_BY_LENDER_LOCK_POLICY
    WHERE BrokerId = @BrokerId AND LockPolicyId = @PolicyId
    ORDER BY InvestorName, ProductCode
END
