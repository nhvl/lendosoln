CREATE PROCEDURE [dbo].[RetrieveDataTracLpId] 
	@lLpTemplateId uniqueidentifier
AS
SELECT lDataTracLpId from LOAN_PROGRAM_TEMPLATE
WHERE lLpTemplateId = @lLpTemplateId
