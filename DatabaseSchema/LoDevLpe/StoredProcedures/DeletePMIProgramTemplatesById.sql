CREATE proc [dbo].[DeletePMIProgramTemplatesById]
	@ProductId uniqueidentifier
as
delete
from dbo.PMI_POLICY_ASSOC
where PmiProductId = @ProductId

delete
from dbo.PMI_PRODUCT
where ProductId = @ProductId
