CREATE  PROCEDURE [dbo].[DeleteDisabledLoanProgramInfo]
	@ProductCode varchar(100),
	@InvestorName varchar(100)
AS
	DELETE FROM DISABLED_BY_SAE 
	WHERE ProductCode = @ProductCode AND InvestorName = @InvestorName
