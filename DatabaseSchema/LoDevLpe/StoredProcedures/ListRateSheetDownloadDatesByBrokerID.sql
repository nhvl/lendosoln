CREATE  PROCEDURE [dbo].[ListRateSheetDownloadDatesByBrokerID]
	@BrokerId uniqueidentifier
AS

SELECT
  lLpTemplateId
  , lRateSheetDownloadStartD
  , lRateSheetDownloadEndD
FROM Loan_Program_Template LEFT JOIN RATE_OPTIONS_DOWNLOAD_TIME ON SrcProgId = SrcRateOptionsProgId
WHERE BrokerId = @BrokerId AND IsLpe = 1 AND IsEnabled = 1
