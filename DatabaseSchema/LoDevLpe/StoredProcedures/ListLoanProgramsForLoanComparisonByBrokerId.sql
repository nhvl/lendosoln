-- =============================================
-- Author:		David Dao
-- Create date: 4/4/2015
-- Description:	List Loan Programs to Run LPE for Loan Comparison
-- =============================================


ALTER  PROCEDURE [dbo].[ListLoanProgramsForLoanComparisonByBrokerId] 
	@BrokerId uniqueidentifier, 
	@Ids xml -- '<r><id>1</id><id>2</id><id>3</id></r>'
AS
SELECT llptemplateid, llptemplatenm, ProductCode, lTerm, lDue
       ,lRAdj1stCapMon, lRAdj1stCapR, lRAdjCapR, lRAdjLifeCapR, lRAdjCapMon, lArmIndexNameVstr
       ,lBuydwnR1, lBuydwnR2, lBuydwnR3, lBuydwnR4, lBuydwnR5
       ,lFinMethT, llpProductType, lHardPrepmtPeriodMonths + lSoftPrepmtPeriodMonths as lPrepmtPeriod
	   ,lLpInvestorNm

FROM LOAN_PROGRAM_TEMPLATE
WHERE BrokerId = @BrokerId
      AND IsMaster = 0
      AND IsEnabled = 1
      AND IsLpe = 1
	  AND llptemplateid IN (select  node.value('(.)[1]', 'uniqueidentifier')  from @ids.nodes('/r/id') as result(node))
