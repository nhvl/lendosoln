CREATE PROCEDURE [dbo].[RetrieveLpIsMaster] 
	@lLpTemplateId Guid
AS
	SELECT IsMaster FROM Loan_Program_Template WHERE lLpTemplateId = @lLpTemplateId AND IsMasterPriceGroup = 0
