Import the files:
1) Shut down the instance of Visual Studio that has the project open.
2) Navigate to the project's folder on the file system, right click and do TortoiseSVN -> Import
3) Create a new folder in whatever location in the repository browser that you want to put the files in
4) Select that newly created folder for importing when doing the Import command

Check the files back out:
1) To be safe, rename the existing folder on the fileserver so that it can be a backup - you might need to do an iisreset -stop command first.
2) Create a new folder with the old name of the one you backed up
3) Right click and do SVN Update
4) Navigate to the folder in the repository browser that has the files you just added
5) Re-start iis if you stopped it at stop 1
6) Re-start visual studio