2009-06-01 - dd
	- Use this query to return number of columns in each table and the max column len.
	    
		select object_name(object_id) AS TableName, count(name) AS ColumnCount, sum(max_length) AS TotalSize from sys.columns group by object_id ORDER BY TableName
		
2009-07-27 - dd
	- Use this query to return the indexes usage on the database
	
	select object_name(a.object_id, a.database_id) as objectname, b.name as indexname,b.type_desc, 
a.user_seeks, a.user_scans,a.user_lookups, a.user_updates, a.last_user_seek, a.last_user_scan,a.last_user_lookup,a.last_user_update,
a.system_seeks,a.system_scans,a.system_lookups,a.system_updates,a.last_system_seek,a.last_system_scan,a.last_system_lookup,a.last_system_update
from sys.dm_db_index_usage_stats a right outer JOIN sys.indexes b ON a.object_id = b.object_id AND a.index_id = b.index_id
where a.object_id > 100
order by objectname, indexname


		
		