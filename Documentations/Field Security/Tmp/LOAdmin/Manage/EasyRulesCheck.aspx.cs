using System;
using System.IO;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;
using LendersOffice.Security.EasyAccessControl;
using LendersOffice.Security.EngineBuilder;
using RuleEngineFactory = LendersOffice.Security.EngineBuilder.RuleEngineFactory;

namespace LendersOfficeApp.LOAdmin.Manage
{
    internal class AccessControlLab
    {

        private AccessControlLab()
        {            
            string xmlRuleFile   = "../../EasyLoanRules.xml";
            if ( HttpContext.Current != null) 
            {
                //string ruleBasename = SiteConfigUtilities.GetStringValue("LoanRuleFile", "");
                string ruleBasename  = "~/EasyLoanRules.xml";
                xmlRuleFile = HttpContext.Current.Server.MapPath(ruleBasename);
            }

            

            TextReader txtReader = null;
            try
            {
                txtReader          = File.OpenText(xmlRuleFile);
                m_xmlRulesContent  = txtReader.ReadToEnd();

                BinaryRuleEngine   binRuleEngine;
                string sourceCode  = RuleEngineFactory.XmlToCSharpEngine(m_xmlRulesContent, false /*IsFile*/, out binRuleEngine );
                m_AccessControlInf = (IAccessControlEngine) CompilerHelper.CreateDllAndObj( sourceCode, 
                                                                typeof(IAccessControlEngine), 
                                                                "AccessControlEngineImp") ;
                    
                m_vars             = m_AccessControlInf.GetVars();
                m_txtPatternRules  = BinaryRuleEngineUtility.LinearToText(binRuleEngine);

                m_slipRulesErrorMsg = null;
                try
                {
                    PatternRule[] patternRules = BinaryRuleEngineUtility.LinearToPatternRules(binRuleEngine);
                    m_splitRules               = new SplitPatternRules(m_vars, patternRules, OptimizeRulesLevel.e_high);
                }
                catch( Exception exc)
                {
                    m_slipRulesErrorMsg = exc.Message;
                }
                

                

            }
            catch( Exception exc)
            {
                m_errorMsg = exc.Message;
            }
            finally
            {
                if( txtReader != null )
                    txtReader.Close();
            }
        }

        public int[] CreateBinaryTicket()
        {
            return new int[m_vars.Length];
        }

        public bool EasyTicketToBinaryTicket(string easyTicket, int[] binaryTicket)
        {
            return Var.EasyTicketToBinaryTicket(m_vars, easyTicket, binaryTicket);
        }

        public string BinaryTicketToEasyTicket(int[] binaryTicket)
        {
            return Var.BinaryTicketToEasyTicket(m_vars, binaryTicket);
            
        }

        public string toNotation(int varIdx, int value)
        {
            return m_vars[varIdx].toNotation(value);
        }

        public PermissionEnumType GetPermission(int[] binaryTicket, System.Collections.ArrayList reasons)
        {
            return m_AccessControlInf.GetPermission(binaryTicket, reasons);
        }

        public String ErrorMessage
        {
            get { return m_errorMsg; }
        }

        public String SplitRulesErrorMessage
        {
            get { return m_slipRulesErrorMsg; }
        }

        public string ConvertKirkTicketToEasyTicket(string kirkTicket)
        {
            return m_AccessControlInf.KirkSimpleTicket2EasyTicket( kirkTicket);
        }



        public string XML
        {
            get { return m_xmlRulesContent; }
        }

        public string TxtPatternRules
        {
            get { return m_txtPatternRules; }
        }

        public string GetPartition(string easyTicket, int filter, bool isPartitionTicket)
        {
            return m_splitRules.GetTxtDisjoinRules(easyTicket, filter, isPartitionTicket);
        }

        public bool HasMultipleGroups()
        {
            return Var.Find(m_vars, "CanModifyLoanName") != null;
        }


        string               m_txtPatternRules;
        string               m_xmlRulesContent;
        string               m_errorMsg;

        IAccessControlEngine m_AccessControlInf;
        Var[]                m_vars;

        string               m_slipRulesErrorMsg;
        SplitPatternRules    m_splitRules;

        static AccessControlLab m_instance;
        static Object This      = new Object();

        static public AccessControlLab GetInstance()
        {
            if( m_instance == null )
            {
                lock( This )
                {   
                    m_instance = new AccessControlLab();
                }
            }

            return m_instance;
        }

        static public void Reload()
        {
            lock( This )
            {   
                m_instance = new AccessControlLab();
            }
        }


    }

	/// <summary>
	/// Summary description for EasyRulesCheck.
	/// </summary>
	public class EasyRulesCheck : System.Web.UI.Page
	{
        protected System.Web.UI.WebControls.TextBox txtResult;
        protected System.Web.UI.WebControls.Button btnSubmit;
        protected System.Web.UI.WebControls.Button btnClear;
        protected System.Web.UI.WebControls.DropDownList ddlFilter;
        
        protected System.Web.UI.WebControls.DropDownList ddlAppCode;
        protected System.Web.UI.WebControls.DropDownList ddlUserType;
        protected System.Web.UI.WebControls.DropDownList ddlScope;
        protected System.Web.UI.WebControls.CheckBoxList cblRoleId;
        protected System.Web.UI.WebControls.DropDownList ddlIsAssigned;
        protected System.Web.UI.WebControls.DropDownList ddlBop2PriceMyLoan;

        protected System.Web.UI.WebControls.DropDownList ddlBop4CanEditUwAssigned;
        protected System.Web.UI.WebControls.DropDownList ddlBop5EditProcessorAssigned;
        
        protected System.Web.UI.WebControls.DropDownList ddlUop1AccessToTemplatesOfAnyBranch;
        protected System.Web.UI.WebControls.DropDownList ddlUop2MayAccessClosedLoan;
        protected System.Web.UI.WebControls.DropDownList ddlLop1ValidLoan;
        protected System.Web.UI.WebControls.DropDownList ddlLop2LoanIsTemplate;
        protected System.Web.UI.WebControls.DropDownList ddlLop3LoanHasUnderwriter;        
        protected System.Web.UI.WebControls.DropDownList ddlLop4LoanHasProcessor;
        protected System.Web.UI.WebControls.DropDownList ddlLop5LoanIsManagedByUser;
        protected System.Web.UI.WebControls.DropDownList ddlLop6LienPosition;
        protected System.Web.UI.WebControls.CheckBoxList cblLoanStatus;
        
        
        protected System.Web.UI.WebControls.Label lblParserError;
        protected System.Web.UI.WebControls.Button btnParser;
        protected System.Web.UI.WebControls.TextBox txtTicket;
        protected System.Web.UI.WebControls.Label lblTitile;
        protected System.Web.UI.WebControls.Button btnDisplayXmlContent;

        protected System.Web.UI.WebControls.ListControl [] m_listControls;
        protected System.Web.UI.WebControls.Button btnPatternRules;
        protected System.Web.UI.WebControls.Button btnCategory;
        protected System.Web.UI.WebControls.Button btnReloadRule;
        protected System.Web.UI.WebControls.DropDownList ddlCanWriteNonassingedLoan;
        protected System.Web.UI.WebControls.DropDownList ddlActAsRateLocked;

        protected System.Web.UI.WebControls.DropDownList ddlOnlyAccountantCanModifyTrustAccount;
        protected System.Web.UI.WebControls.DropDownList dllCanModifyLoanName;
        protected System.Web.UI.WebControls.DropDownList dllIsRateLocked;
        protected System.Web.UI.WebControls.DropDownList dllIsAccountExecutiveOnly;
        protected System.Web.UI.WebControls.DropDownList ddlSep = new DropDownList();
        protected System.Web.UI.WebControls.DropDownList ddlGroups;
        protected System.Web.UI.WebControls.Button btnParserOldTicket;
        protected System.Web.UI.WebControls.Button btnPartitionTicket;
        protected System.Web.UI.WebControls.DropDownList ddlCanCreateLoanTemplate;
        protected System.Web.UI.WebControls.DropDownList ddlHasLenderDefaultFeatures;

        private   AccessControlLab accessControl;

        private bool HasMultipleGroups()
        {
            LoadAccessControl();
            return (  accessControl != null  && accessControl.HasMultipleGroups() );
        }
    
		private void Page_Load(object sender, System.EventArgs e)
		{
            //HasMultipleGroups()

            ddlSep.Items.Add( new ListItem("$", "$") );
            ddlSep.SelectedIndex = 0;

            m_listControls = new ListControl []
                    {
                        ddlAppCode,
                        ddlUserType,
                        ddlScope,
                        ddlIsAssigned,

                        ddlLop3LoanHasUnderwriter,          // 4
                        ddlBop4CanEditUwAssigned,
                        ddlLop4LoanHasProcessor,
                        ddlBop5EditProcessorAssigned,

                        ddlUop2MayAccessClosedLoan,         //8
                        ddlLop1ValidLoan,
                        ddlLop2LoanIsTemplate,

                        ddlUop1AccessToTemplatesOfAnyBranch, // 11

                        ddlBop2PriceMyLoan,
                        ddlLop5LoanIsManagedByUser,
                        ddlLop6LienPosition,

                        cblRoleId,
                        cblLoanStatus,                      // 16
                        ddlCanWriteNonassingedLoan,
                        ddlActAsRateLocked,

                        /*
                        ddlOnlyAccountantCanModifyTrustAccount, // 19 
                        dllCanModifyLoanName,              
                        dllIsRateLocked,
                        dllIsAccountExecutiveOnly,
                        
                        ddlCanCreateLoanTemplate,
                        ddlHasLenderDefaultFeatures,                        

                        ddlSep,                             // 23
                        ddlGroups,
                        */

            };

            ListControl [] newControls = new ListControl []
                    {
                        ddlOnlyAccountantCanModifyTrustAccount, // 19 
                        dllCanModifyLoanName,              
                        dllIsRateLocked,
                        dllIsAccountExecutiveOnly,
                        ddlCanCreateLoanTemplate,
                        ddlHasLenderDefaultFeatures,                        

                    };


            ArrayList arrControls = new ArrayList(m_listControls);

            bool hasGroups = HasMultipleGroups();
            if( hasGroups )
                arrControls.AddRange( newControls );

            arrControls.Add( ddlSep );
            arrControls.Add( ddlGroups );

            m_listControls = (ListControl[]) arrControls.ToArray(typeof(ListControl));

            foreach( ListControl listControl in newControls )
                listControl.Enabled = hasGroups;


            // append value to list item's text
            if( ddlAppCode.Items[0].Text.IndexOf('-') < 0 )
                foreach( ListControl listControl in m_listControls )
                {
                    DropDownList ddl = listControl as DropDownList;
                    if( ddl == null )
                        continue;

                    for(int i=0; i< ddl.Items.Count; i++)
                    {
                        ListItem listItem = ddl.Items[i];
                        if( listItem.Value.IndexOf('*') < 0)
                            listItem.Text = String.Format("{1} - {0}", listItem.Value, listItem.Text);
                    }
                }


			// Put user code to initialize the page here
            if( IsPostBack == false )
            {
                string[] arrRoles = new string[] 
                {
                    "aCountant",
                    "Administrator",
                    "lender accounteXec",
                    "Loan officer",
                    "loan Opener",
                    "lock Desk",
                    "Manager",
                    "Processor",
                    "Real estate agent",
                    "Telemarketer",
                    "Underwriter",
                    "Other ?"
                };
                //cblRoleId.DataSource = arrRoles;
                //cblRoleId.DataBind();

                btnClear_Click(this, null);

                string oldFormatTicket = Request.QueryString["oldticket"];
                if( oldFormatTicket != null && oldFormatTicket.Length > 0 )
                {
                    txtTicket.Text = oldFormatTicket;
                    btnParserOldTicket_Click(this, null);
                    if( lblParserError.Text == String.Empty )
                        btnSubmit_Click(this, null);
                }

            }
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.btnParser.Click += new System.EventHandler(this.btnParser_Click);
            this.btnParserOldTicket.Click += new System.EventHandler(this.btnParserOldTicket_Click);
            this.btnPatternRules.Click += new System.EventHandler(this.btnPatternRules_Click);
            this.btnDisplayXmlContent.Click += new System.EventHandler(this.btnDisplayXmlContent_Click);
            this.btnReloadRule.Click += new System.EventHandler(this.btnReloadRule_Click);
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            this.btnCategory.Click += new System.EventHandler(this.btnCategory_Click);
            this.btnPartitionTicket.Click += new System.EventHandler(this.btnPartitionTicket_Click);
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            this.Load += new System.EventHandler(this.Page_Load);

        }
		#endregion

        private void btnClear_Click(object sender, System.EventArgs e)
        {
            foreach( ListControl listControl in m_listControls )
            {
                DropDownList ddl = listControl as DropDownList;
                CheckBoxList cbl = listControl as CheckBoxList;

                if( ddl != null )
                {
                    ListItem item = ddl.Items.FindByValue("*");
                    if( item != null )
                        ddl.SelectedIndex = ddl.Items.IndexOf( item );
                }
                if( cbl != null)
                {
                    foreach(ListItem item in cbl.Items)
                        item.Selected = true;
                }
            }

            ddlUserType.SelectedIndex = 3;
        
        }

        private int[] GetTickets(out string easyTicket)
        {
            easyTicket = null;

            StringBuilder sb = new StringBuilder();

            foreach( ListControl listControl in m_listControls )
            {
                DropDownList ddl = listControl as DropDownList;
                CheckBoxList cbl = listControl as CheckBoxList;

                if( ddl != null )
                {
                    sb.Append( ddl.SelectedItem.Value);
                }
                if( cbl != null)
                {
                    int count = 0;
                    foreach(ListItem item in cbl.Items)
                        if( item.Selected )
                            count++;
                    if( count == cbl.Items.Count )
                        sb.Append("*");
                    else
                    {

                        if( count != 1 )
                            sb.Append("(");

                        foreach(ListItem item in cbl.Items)
                            if( item.Selected )
                                sb.Append(item.Value);
                        if( count != 1 )
                            sb.Append(")");
                    }

                    if( count == 0 )
                    {
                        txtResult.Text = "Invalid ticket error : you need to select at least one option for " 
                            + cbl.ID.Substring(3);
                        return null;
                    }
                }
            }

            if( LoadAccessControl() == false )
                return null;

            int[] binaryTicket  = accessControl.CreateBinaryTicket();
            if( accessControl.EasyTicketToBinaryTicket(sb.ToString(), binaryTicket) == false )
            {
                txtResult.Text = "Internal Error : can not parser the ticket " + sb.ToString() +
                    "\nPlease, contact with the developer for fixing this bug";
                return null;
            }

            easyTicket = sb.ToString();
            return binaryTicket;

        }

        private void btnSubmit_Click(object sender, System.EventArgs e)
        {
            string easyTicket;
            int[] binaryTicket  = GetTickets(out easyTicket);
            if( binaryTicket == null )
                return;

            StringBuilder sb = new StringBuilder("Ticket =  ");
            sb.Append( easyTicket ).Append( "\n" ) ;

            ArrayList reasons = new ArrayList();
            PermissionEnumType result = accessControl.GetPermission(binaryTicket, reasons);

            sb.Append("\n");

            int ruleIdx = 0;
            foreach( string reason in reasons )
                if( reason != null )
                {
                    sb.AppendFormat( "\n{0,2}. {1}", ruleIdx, reason);
                    ruleIdx++;
                }

            sb.Append("\n\n").Append( "   result = ").Append(PermissionEnumTypeUtil.ToString(result));

            txtResult.Text = sb.ToString();

        }  

        private bool LoadAccessControl()
        {
            accessControl = AccessControlLab.GetInstance();
            if( accessControl.ErrorMessage != null )
            {
                txtResult.Text = "Can not create AccessControl interface because :\n" + accessControl.ErrorMessage;
                return false;
            }
            return true;
        }


        private void btnParserOldTicket_Click(object sender, System.EventArgs e)
        {
            if( LoadAccessControl() == false )
                return;

            lblParserError.Text = "";
            txtTicket.Text = txtTicket.Text.Trim();

            string easyTicket = "";

            try
            {
                easyTicket = accessControl.ConvertKirkTicketToEasyTicket(txtTicket.Text);
            }
            catch( Exception exc )
            {
                lblParserError.Text = "Invalid ticket : can not convert to new format. " + exc.ToString();
                return;
            }

            if( easyTicket == String.Empty )
            {
                lblParserError.Text = "Invalid ticket : can not convert to new format.";
                return;
            }

            //txtTicket.Text = easyTicket;
            ParserTicket(easyTicket);
        
        }

        private void ParserTicket(string easyTicket)
        {
            if( LoadAccessControl() == false )
                return;

            if( easyTicket.IndexOf("$") < 0 )
                easyTicket += "$A";

            int[] binaryTicket  = accessControl.CreateBinaryTicket();
            if( accessControl.EasyTicketToBinaryTicket( easyTicket, binaryTicket) == false )
            {
                lblParserError.Text = "Invalid ticket";
                try
                {
                    String correctPart = accessControl.BinaryTicketToEasyTicket(binaryTicket);
                    lblParserError.Text = String.Format("Invalid ticket. Correct part : [{0}]", correctPart );

                }
                finally
                {
                }
                return;
            }

            for(int i=0; i < m_listControls.Length; i++)
            {
                DropDownList ddl = m_listControls[i] as DropDownList;
                CheckBoxList cbl = m_listControls[i]  as CheckBoxList;

                if( ddl != null )
                {
                    string option = accessControl.toNotation(i, binaryTicket[i]);
                    ListItem item = ddl.Items.FindByValue(option);
                    if( item != null)
                        ddl.SelectedIndex = ddl.Items.IndexOf( item );
                    else if( ddl == this.ddlGroups)
                    {
                        ddl.Items[ddl.Items.Count-1].Value = option;
                        ddl.Items[ddl.Items.Count-1].Text  = "Combination - " + option;
                        ddl.SelectedIndex = ddl.Items.Count-1;

                    }
                    else
                    {                        
                        lblParserError.Text = ddl.ID + " has some problem";
                        return;
                    }
                }
                else
                {
                    int value = binaryTicket[i];
                    for( int k = 0; k < cbl.Items.Count; k++)
                        cbl.Items[k].Selected = ( value & (1 << k) ) != 0 ;

                }

            }
        }

        private void btnParser_Click(object sender, System.EventArgs e)
        {
            lblParserError.Text = "";
            txtTicket.Text = txtTicket.Text.Trim();

            ParserTicket(txtTicket.Text);        
        }

        private void btnDisplayXmlContent_Click(object sender, System.EventArgs e)
        {
            LoadAccessControl();
            if( accessControl.XML  != null )
            {
                txtResult.Text = accessControl.XML + 
                                 (accessControl.ErrorMessage != null ? "\n" + accessControl.ErrorMessage : "");


            }
        }

        private void btnPatternRules_Click(object sender, System.EventArgs e)
        {
            if( LoadAccessControl() == false )
                return;

            txtResult.Text = accessControl.TxtPatternRules;
        }

        private void btnCategory_Click(object sender, System.EventArgs e)
        {
            Category( false /* isPartitionTicket */);
        }

        private void Category(bool isPartitionTicket)
        {
            string easyTicket;
            int[] binaryTicket  = GetTickets(out easyTicket);
            if( binaryTicket == null )
                return;

            if( accessControl.SplitRulesErrorMessage != null )
            {
                txtResult.Text = accessControl.SplitRulesErrorMessage;
                return;
            }
            int filter = 0;
            string filterString = ddlFilter.SelectedItem.Value;
                                  
            if( filterString.IndexOf("None")  >= 0 )
                filter |= (int)PermissionEnumType.e_none;
            if( filterString.IndexOf("Read")  >= 0 )
                filter |= (int)PermissionEnumType.e_read;
            if( filterString.IndexOf("Full")  >= 0 )
                filter |= (int)PermissionEnumType.e_write;

            txtResult.Text = "Ticket =  " + easyTicket + "\n" + accessControl.GetPartition(easyTicket, filter, isPartitionTicket);                                 
        }


        private void btnReloadRule_Click(object sender, System.EventArgs e)
        {
            AccessControlLab.Reload();        
        }

        private void btnPartitionTicket_Click(object sender, System.EventArgs e)
        {
            Category( true /* isPartitionTicket */);
        
        }



	}
}
