<%@ Page language="c#" Codebehind="EasyRulesCheck.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.LOAdmin.Manage.EasyRulesCheck" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>EasyRulesCheck</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<body MS_POSITIONING="FlowLayout" onload="onInit();">
		<script language="jscript">
	
		<!--
		function resize(w, h) {
		w = w < screen.width ? w : screen.width - 10;
		h = h < screen.height ? h : screen.height - 60;
		var l = (screen.width - w) / 2;
		var t = (screen.height - h - 50) / 2;
		self.dialogWidth = w + 'px'; 
		self.dialogHeight = h + 'px';
		self.dialogLeft = l + 'px';
		self.dialogTop = t + 'px';
		}
		
		function onInit()
		{

			if( window.dialogArguments != null )
			{
				<% if( IsPostBack == false ) { %>
				
				resize( screen.width - 10 , screen.height - 60 );
				
				
				<% } %>
			}
		}
		
		// -->
		
		</script>
		<FORM id="LoanRulesCheck" onsubmit="document.all.txtResult.value = '';" method="post" runat="server">
			<TABLE id="Table1" style="WIDTH: 1110px; HEIGHT: 850px" cellSpacing="1" cellPadding="1" width="1110" border="1">
				<TR>
					<TD align="center" colSpan="4"><STRONG><asp:label id="lblTitile" runat="server" ForeColor="Black">Easy Rules Check</asp:label></STRONG></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 284px" colSpan="4"></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 314px"><asp:textbox id="txtTicket" runat="server" Width="300px"></asp:textbox></TD>
					<TD style="WIDTH: 800px" colSpan="3"><asp:button id="btnParser" runat="server" Width="46px" Text="Parser Ticket"></asp:button><asp:button id="btnParserOldTicket" runat="server" Width="75px" Text="Old Parser"></asp:button><asp:label id="lblParserError" runat="server" ForeColor="Red" Width="574px"></asp:label></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 314px">0. Application</TD>
					<TD style="WIDTH: 286px"><asp:dropdownlist id="ddlAppCode" runat="server" Width="186px">
							<asp:ListItem Value="L" Selected="True">Lender Office</asp:ListItem>
							<asp:ListItem Value="P">Price My Loan</asp:ListItem>
							<asp:ListItem Value="*">*</asp:ListItem>
						</asp:dropdownlist></TD>
					<TD style="WIDTH: 323px">08. Allow&nbsp;managers&nbsp;to&nbsp;edit closed 
						loans,&nbsp;others&nbsp;can read</TD>
					<TD style="WIDTH: 197px"><asp:dropdownlist id="ddlUop2MayAccessClosedLoan" runat="server" Width="122px">
							<asp:ListItem Value="Y">Yes</asp:ListItem>
							<asp:ListItem Value="N">No</asp:ListItem>
							<asp:ListItem Value="*">*</asp:ListItem>
						</asp:dropdownlist></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 314px">1. User Type</TD>
					<TD style="WIDTH: 286px"><asp:dropdownlist id="ddlUserType" runat="server" Width="189px">
							<asp:ListItem Value="B">Broker</asp:ListItem>
							<asp:ListItem Value="P">Price My Loan</asp:ListItem>
							<asp:ListItem Value="I">Internal</asp:ListItem>
							<asp:ListItem Value="(BP)">Broker, PML</asp:ListItem>
							<asp:ListItem Value="(BI)">Broker,  Internal</asp:ListItem>
							<asp:ListItem Value="(PI)">PML,  Internal</asp:ListItem>
							<asp:ListItem Value="*">*</asp:ListItem>
						</asp:dropdownlist></TD>
					<TD style="WIDTH: 323px">09. Loan&nbsp; Is Valid ?</TD>
					<TD><asp:dropdownlist id="ddlLop1ValidLoan" runat="server" Width="122px">
							<asp:ListItem Value="V">Valid</asp:ListItem>
							<asp:ListItem Value="D">Delete</asp:ListItem>
							<asp:ListItem Value="*">*</asp:ListItem>
						</asp:dropdownlist></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 314px">2. Scope</TD>
					<TD style="WIDTH: 286px"><asp:dropdownlist id="ddlScope" runat="server" Width="189px">
							<asp:ListItem Value="C">CrossCorporate</asp:ListItem>
							<asp:ListItem Value="I">InScope</asp:ListItem>
							<asp:ListItem Value="N">Nonduty</asp:ListItem>
							<asp:ListItem Value="(CI)">Cross. , InScope</asp:ListItem>
							<asp:ListItem Value="(CN)">Cross.,  NonDuty</asp:ListItem>
							<asp:ListItem Value="(IN)">InScope, Nonduty</asp:ListItem>
							<asp:ListItem Value="*">*</asp:ListItem>
						</asp:dropdownlist></TD>
					<TD style="WIDTH: 323px">10. Is Real Loan</TD>
					<TD style="WIDTH: 197px"><asp:dropdownlist id="ddlLop2LoanIsTemplate" runat="server" Width="122px">
							<asp:ListItem Value="T">Template</asp:ListItem>
							<asp:ListItem Value="L">Loan</asp:ListItem>
							<asp:ListItem Value="*">*</asp:ListItem>
						</asp:dropdownlist></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 314px; HEIGHT: 17px">3. Is Assigned</TD>
					<TD style="WIDTH: 286px; HEIGHT: 17px"><asp:dropdownlist id="ddlIsAssigned" runat="server" Width="189px">
							<asp:ListItem Value="Y">Yes</asp:ListItem>
							<asp:ListItem Value="N">No</asp:ListItem>
							<asp:ListItem Value="*">*</asp:ListItem>
						</asp:dropdownlist></TD>
					<TD style="WIDTH: 323px; HEIGHT: 17px">11.&nbsp;Limit Templates&nbsp;To Branch</TD>
					<TD style="WIDTH: 197px; HEIGHT: 17px"><asp:dropdownlist id="ddlUop1AccessToTemplatesOfAnyBranch" runat="server" Width="122px">
							<asp:ListItem Value="L">Limit</asp:ListItem>
							<asp:ListItem Value="l">No Limit</asp:ListItem>
							<asp:ListItem Value="*">*</asp:ListItem>
						</asp:dropdownlist></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 314px">4.Loan's underwriter is&nbsp;other user.</TD>
					<TD style="WIDTH: 286px"><asp:dropdownlist id="ddlLop3LoanHasUnderwriter" runat="server" Width="189px">
							<asp:ListItem Value="U">Yes</asp:ListItem>
							<asp:ListItem Value="u">No</asp:ListItem>
							<asp:ListItem Value="*">*</asp:ListItem>
						</asp:dropdownlist></TD>
					<TD style="WIDTH: 323px">12. Price My Loan</TD>
					<TD style="WIDTH: 197px"><asp:dropdownlist id="ddlBop2PriceMyLoan" runat="server" Width="121px">
							<asp:ListItem Value="Y">Yes</asp:ListItem>
							<asp:ListItem Value="N">No</asp:ListItem>
							<asp:ListItem Value="*">*</asp:ListItem>
						</asp:dropdownlist></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 314px">5.&nbsp;Maybe, you&nbsp;can edit&nbsp;UW&nbsp;Assigned</TD>
					<TD style="WIDTH: 286px"><asp:dropdownlist id="ddlBop4CanEditUwAssigned" runat="server" Width="189px">
							<asp:ListItem Value="Y">Yes</asp:ListItem>
							<asp:ListItem Value="N">No</asp:ListItem>
							<asp:ListItem Value="*">*</asp:ListItem>
						</asp:dropdownlist></TD>
					<TD style="WIDTH: 323px">13. Loan Is Managed By User</TD>
					<TD style="WIDTH: 197px"><asp:dropdownlist id="ddlLop5LoanIsManagedByUser" runat="server" Width="122px">
							<asp:ListItem Value="Y">Yes</asp:ListItem>
							<asp:ListItem Value="N">No</asp:ListItem>
							<asp:ListItem Value="*">*</asp:ListItem>
						</asp:dropdownlist></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 314px">6. Loan's Processor is&nbsp;other user.</TD>
					<TD style="WIDTH: 286px"><asp:dropdownlist id="ddlLop4LoanHasProcessor" runat="server" Width="186px">
							<asp:ListItem Value="P">Yes</asp:ListItem>
							<asp:ListItem Value="p">No</asp:ListItem>
							<asp:ListItem Value="*">*</asp:ListItem>
						</asp:dropdownlist></TD>
					<TD style="WIDTH: 323px">14.&nbsp;Lien position</TD>
					<TD style="WIDTH: 197px"><asp:dropdownlist id="ddlLop6LienPosition" runat="server" Width="122px">
							<asp:ListItem Value="F">First</asp:ListItem>
							<asp:ListItem Value="S">Second</asp:ListItem>
							<asp:ListItem Value="O">Other</asp:ListItem>
							<asp:ListItem Value="(FS)">First,Second</asp:ListItem>
							<asp:ListItem Value="(FO)">First, Other</asp:ListItem>
							<asp:ListItem Value="(SO)">Second, Other</asp:ListItem>
							<asp:ListItem Value="*">*</asp:ListItem>
						</asp:dropdownlist></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 314px">7. Maybe, you&nbsp;can edit&nbsp; Processor Assigned</TD>
					<TD style="WIDTH: 286px"><asp:dropdownlist id="ddlBop5EditProcessorAssigned" runat="server" Width="188px">
							<asp:ListItem Value="Y">Yes</asp:ListItem>
							<asp:ListItem Value="N">No</asp:ListItem>
							<asp:ListItem Value="*">*</asp:ListItem>
						</asp:dropdownlist></TD>
					<TD style="WIDTH: 323px"></TD>
					<TD style="WIDTH: 197px"></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 314px"></TD>
					<TD style="WIDTH: 286px"></TD>
					<TD style="WIDTH: 323px"></TD>
					<TD style="WIDTH: 197px"></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 314px">15.&nbsp;Role Id</TD>
					<TD colSpan="3"><asp:checkboxlist id="cblRoleId" runat="server" RepeatDirection="Horizontal" RepeatColumns="6">
							<asp:ListItem Value="C">aCountant</asp:ListItem>
							<asp:ListItem Value="A">Administrator&#160;&#160;&#160;</asp:ListItem>
							<asp:ListItem Value="X">lender accounteXec&#160;&#160;&#160;</asp:ListItem>
							<asp:ListItem Value="L">Loan officer</asp:ListItem>
							<asp:ListItem Value="O">loan Opener</asp:ListItem>
							<asp:ListItem Value="D">lock Desk</asp:ListItem>
							<asp:ListItem Value="M">Manager</asp:ListItem>
							<asp:ListItem Value="P">Processor</asp:ListItem>
							<asp:ListItem Value="R">Real estate agent&#160;&#160;&#160;</asp:ListItem>
							<asp:ListItem Value="T">Telemarketer</asp:ListItem>
							<asp:ListItem Value="U">Underwriter</asp:ListItem>
							<asp:ListItem Value="?">Other</asp:ListItem>
						</asp:checkboxlist></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 314px; HEIGHT: 30px">16. Loan Status</TD>
					<TD style="HEIGHT: 30px" colSpan="3"><asp:checkboxlist id="cblLoanStatus" runat="server" Width="655px" RepeatDirection="Horizontal" RepeatColumns="6">
							<asp:ListItem Value="L">Lead&#160;&#160;&#160;</asp:ListItem>
							<asp:ListItem Value="O">Open&#160;&#160;&#160;</asp:ListItem>
							<asp:ListItem Value="B">Active But Before  Approve</asp:ListItem>
							<asp:ListItem Value="A">Active&#160;&#160;&#160;</asp:ListItem>
							<asp:ListItem Value="I">Inactive&#160;&#160;&#160;</asp:ListItem>
							<asp:ListItem Value="C">Closed</asp:ListItem>
						</asp:checkboxlist></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 314px; HEIGHT: 17px">17. Can write non-assigned loan
						<asp:dropdownlist id="ddlCanWriteNonassingedLoan" runat="server" Width="89px">
							<asp:ListItem Value="W">Yes</asp:ListItem>
							<asp:ListItem Value="w">No</asp:ListItem>
							<asp:ListItem Value="*">*</asp:ListItem>
						</asp:dropdownlist></TD>
					<TD style="WIDTH: 286px; HEIGHT: 17px">18.&nbsp;RateLockedAtSubmit&nbsp;&nbsp;
						<asp:dropdownlist id="ddlActAsRateLocked" runat="server" Width="111px">
							<asp:ListItem Value="N">No Affect</asp:ListItem>
							<asp:ListItem Value="R">allow Read</asp:ListItem>
							<asp:ListItem Value="W">allow Write</asp:ListItem>
							<asp:ListItem Value="(NR)">NoAffect, AllowRead</asp:ListItem>
							<asp:ListItem Value="(NR)">NoAffect, AllowWrite</asp:ListItem>
							<asp:ListItem Value="(RW)">Allow Read, Write</asp:ListItem>
							<asp:ListItem Value="*">*</asp:ListItem>
						</asp:dropdownlist></TD>
					<TD style="WIDTH: 323px; HEIGHT: 17px">19.OnlyAccountantCanModifyTrustAccount</TD>
					<TD style="WIDTH: 197px; HEIGHT: 17px"><asp:dropdownlist id="ddlOnlyAccountantCanModifyTrustAccount" runat="server" Width="88px" Enabled="False">
							<asp:ListItem Value="Y">Yes</asp:ListItem>
							<asp:ListItem Value="N">No</asp:ListItem>
							<asp:ListItem Value="*">*</asp:ListItem>
						</asp:dropdownlist></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 314px; HEIGHT: 14px">20.&nbsp;Can Modiy Loan 
						Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<asp:dropdownlist id="dllCanModifyLoanName" runat="server" Width="86px" Enabled="False">
							<asp:ListItem Value="M">Yes</asp:ListItem>
							<asp:ListItem Value="m">No</asp:ListItem>
							<asp:ListItem Value="*">*</asp:ListItem>
						</asp:dropdownlist></TD>
					<TD style="WIDTH: 286px; HEIGHT: 14px">21. 
						IsRateLocked&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;<asp:dropdownlist id="dllIsRateLocked" runat="server" Width="89px" Enabled="False">
							<asp:ListItem Value="L">Yes</asp:ListItem>
							<asp:ListItem Value="U">No</asp:ListItem>
							<asp:ListItem Value="*">*</asp:ListItem>
						</asp:dropdownlist></TD>
					<TD style="WIDTH: 323px; HEIGHT: 14px">22. Is AE 
						Only&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</TD>
					<TD style="WIDTH: 208px; HEIGHT: 14px"><asp:dropdownlist id="dllIsAccountExecutiveOnly" runat="server" Width="85px" Enabled="False">
							<asp:ListItem Value="O">Yes</asp:ListItem>
							<asp:ListItem Value="o">No</asp:ListItem>
							<asp:ListItem Value="*">*</asp:ListItem>
						</asp:dropdownlist></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 314px">23.Can Create Loan Template&nbsp;&nbsp;&nbsp;&nbsp;
						<asp:dropdownlist id="ddlCanCreateLoanTemplate" runat="server" Width="86px" Enabled="False">
							<asp:ListItem Value="C">Yes</asp:ListItem>
							<asp:ListItem Value="c">No</asp:ListItem>
							<asp:ListItem Value="*">*</asp:ListItem>
						</asp:dropdownlist></TD>
					<TD style="WIDTH: 286px">24. HasLenderDefaultFeatures
						<asp:dropdownlist id="ddlHasLenderDefaultFeatures" runat="server" Width="87px" Enabled="False">
							<asp:ListItem Value="H">Yes</asp:ListItem>
							<asp:ListItem Value="h">No</asp:ListItem>
							<asp:ListItem Value="*">*</asp:ListItem>
						</asp:dropdownlist></TD>
					<TD style="WIDTH: 323px">$&nbsp;&nbsp;&nbsp;&nbsp; 26.Groups</TD>
					<TD style="WIDTH: 208px; HEIGHT: 14px"><asp:dropdownlist id="ddlGroups" runat="server" Width="155px">
							<asp:ListItem Value="A">Default</asp:ListItem>
							<asp:ListItem Value="B">LoanName</asp:ListItem>
							<asp:ListItem Value="C">RateLocked</asp:ListItem>
							<asp:ListItem Value="D">LiabilityCollect.</asp:ListItem>
							<asp:ListItem Value="E">Underwriter</asp:ListItem>
							<asp:ListItem Value="F">TrustAccount</asp:ListItem>
							<asp:ListItem Value="G">AuditHistory</asp:ListItem>
							<asp:ListItem Value="H">CreateTemplateLink</asp:ListItem>
							<asp:ListItem Value="I">LinkForLenderDefaultFeature</asp:ListItem>
							<asp:ListItem Value="J">Link For PML</asp:ListItem>
							<asp:ListItem Value="K">Old Condition Page</asp:ListItem>
							<asp:ListItem Value="L">Modify Conditions Page</asp:ListItem>
							<asp:ListItem Value="M">Break RateLock</asp:ListItem>
							<asp:ListItem Value="O">PMLDefaultValuesGroup</asp:ListItem>
							<asp:ListItem Value="*">*</asp:ListItem>
							<asp:ListItem Value="(AB)">Combination</asp:ListItem>
							<asp:ListItem Value="TestingNewRuleGroup">TestingNewRuleGroup</asp:ListItem>
						</asp:dropdownlist></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 314px"></TD>
					<TD style="WIDTH: 286px"></TD>
					<TD style="WIDTH: 323px"></TD>
					<TD style="WIDTH: 197px"></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 314px">&nbsp;Result 
						Filter&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:dropdownlist id="ddlFilter" runat="server" Width="92px">
							<asp:ListItem Value="None,Read, Full" Selected="True">No Filter</asp:ListItem>
							<asp:ListItem Value="1">None</asp:ListItem>
							<asp:ListItem Value="Read">Read</asp:ListItem>
							<asp:ListItem Value="Full">Full</asp:ListItem>
							<asp:ListItem Value="Read, Full">Read, Full</asp:ListItem>
						</asp:dropdownlist></TD>
					<TD style="WIDTH: 286px"><asp:button id="btnPatternRules" runat="server" Text="Rules"></asp:button><asp:button id="btnDisplayXmlContent" runat="server" Width="51px" Text="XML"></asp:button><asp:button id="btnReloadRule" runat="server" Text="Reload"></asp:button></TD>
					<TD style="WIDTH: 500px" align="right" colSpan="2"><asp:button id="btnClear" runat="server" Width="61px" Text="Clear"></asp:button><asp:button id="btnCategory" runat="server" Text="Category"></asp:button><asp:button id="btnPartitionTicket" runat="server" Text="Partition Ticket"></asp:button><asp:button id="btnSubmit" runat="server" Width="54px" Text="Submit"></asp:button></TD>
				</TR>
				<TR>
					<TD colSpan="4"><asp:textbox id="txtResult" runat="server" Width="1093px" Wrap="False" ReadOnly="True" Height="221px" TextMode="MultiLine" EnableViewState="False" BorderStyle="Ridge"></asp:textbox></TD>
				</TR>
			</TABLE>
		</FORM>
	</body>
</HTML>
