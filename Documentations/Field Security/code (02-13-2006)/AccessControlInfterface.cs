using System;
using System.Collections;
using System.Text.RegularExpressions;

using System.Xml;
using LendersOffice.Constants;
using DataAccess;
using LendersOffice.Common;
namespace LendersOffice.Security.EasyAccessControl
{
    public enum PermissionEnumType
    {
        e_none    =  1,  
        e_read    =  2,
        e_write   =  4
    }

    public interface IFieldMapping
    {
        string[] Mapping(string fieldName); 
    }


    // David will implement this class
    public class MockFieldMapping : IFieldMapping
    {
        private static Hashtable s_hash = new Hashtable();

        static MockFieldMapping()
        {
            System.Web.HttpContext currentContext = System.Web.HttpContext.Current;

            if (null == currentContext) 
            {
                throw new CBaseException(ErrorMessages.Generic, "Need to run in HttpContext."); 
            }
            XmlDocument doc = new XmlDocument();

            doc.Load(currentContext.Server.MapPath(ConstAppDavid.RuleGroupsXmlFile));

            XmlNodeList groupNodeList = doc.SelectNodes("//group");
            foreach (XmlElement groupEl in groupNodeList) 
            {
                string groupName = groupEl.GetAttribute("name");
                foreach (XmlElement fieldEl in groupEl.ChildNodes) 
                {
                    string fieldName = fieldEl.GetAttribute("name").ToLower();
                    ArrayList list = (ArrayList) s_hash[fieldName];
                    if (null == list) 
                    {
                        list = new ArrayList();
                        s_hash[fieldName] = list;
                    }
                    list.Add(groupName);
                }
            }

        }

        virtual public string[] Mapping(string fieldName)
        {
            // 2/13/2006 dd - If this method return ArrayList instead of string[] then we save ourself from casting.
            ArrayList list = (ArrayList) s_hash[fieldName.ToLower()];

            if (null == list)
                return null;

            return (string[]) list.ToArray(typeof(string));

        }

    }

    public interface IAccessControlEngine
    {
        string             GetFileVersion();
        string             GetCreateTime(); 

        PermissionEnumType GetPermission(string kirkTicket, ArrayList reasons);
        PermissionEnumType GetPermission(int[] binaryTicket, ArrayList reasons);

        // Hashtable implements IDictionary
        PermissionEnumType GetPermission(string kirkTicket, ArrayList reasons, IDictionary permissionGroups);
        PermissionEnumType GetPermission(int[] binaryTicket, ArrayList reasons, IDictionary permissionGroups);

        String KirkSimpleTicket2EasyTicket(string kirkTicket); // don't support wildcard characters
        Var[] GetVars();
    }

    public class BuilderException : ApplicationException
    {
        public BuilderException(string message) : base(message)
        {
        }
    }

    public class ArgumentBuilderException : BuilderException
    {
        public ArgumentBuilderException(string message) : base(message)
        {
        }
    }

}
