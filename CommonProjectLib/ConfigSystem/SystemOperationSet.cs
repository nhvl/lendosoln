﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Collections;
using CommonProjectLib.Common;
using CommonProjectLib.ConfigSystem;

namespace ConfigSystem
{
    /// <summary>
    /// Represents the list of application system operations that are associated with the conditions.
    /// Some examples are:
    /// Run PML
    /// Write Loan
    /// Read Loan
    /// </summary>
    public class SystemOperationSet : IEnumerable<SystemOperation>
    {
        private List<SystemOperation> m_systemOperations = null;

        /// <summary>
        /// A simple string list of the operation names contained in this particular set
        /// </summary>
        public IEnumerable<string> SystemOperationNames
        {
            get 
            {
                return this.m_systemOperations.Select(sysop => sysop.Name);
            }
        }

        public void Add(SystemOperation sysOp)
        {
            if (Get(sysOp.Name) != null)
            {
                throw CBaseException.CreateGeneric("Cannot add system operation " + sysOp.Name + " to the system operation set because one already exists with the same name.");
            }
            
            this.m_systemOperations.Add(sysOp);
        }

        public void Remove(string operationName)
        {
            this.m_systemOperations.RemoveAll(sysop => sysop.Name.EqualsIgnoreCase(operationName));
        }

        public void Replace(ISystemOperationIdentifier operation, SystemOperation sysOp)
        {
            this.Replace(operation.Id, sysOp);
        }

        public void Replace(string operationName, SystemOperation sysOp)
        {
            int index = this.m_systemOperations.FindIndex(op => op.Name.EqualsIgnoreCase(operationName));
            if (index >= 0)
                this.m_systemOperations[index] = sysOp;
            else
                this.m_systemOperations.Add(sysOp);
        }

        public SystemOperation Get(ISystemOperationIdentifier operation)
        {
            return this.Get(operation.Id);
        }

        public SystemOperation Get(string operationName)
        {
            return this.m_systemOperations.Find(sysop => sysop.Name.EqualsIgnoreCase(operationName));
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="systemOperationSetRoot">The "ApplicableSystemOperationSet" element from the main SystemConfig XML</param>
        public SystemOperationSet(XElement systemOperationSetRoot)
        {
            this.m_systemOperations = new List<SystemOperation>();
            Parse(systemOperationSetRoot);
        }

        public SystemOperationSet()
        {
            this.m_systemOperations = new List<SystemOperation>();
        }

        /// <summary>
        /// Parses the ApplicableSystemOperationSet element for the system operations
        /// </summary>
        /// <param name="systemOperationSetRoot">The "ApplicableSystemOperationSet" element from the main SystemConfig XML</param>
        private void Parse(XElement systemOperationSetRoot)
        {
            foreach (XElement element in systemOperationSetRoot.Elements("SystemOperation"))
            {
                SystemOperation sysOp = new SystemOperation(element);
                this.m_systemOperations.Add(sysOp);
            }
        }

        /// <summary>
        /// Generates the XML representing this object.
        /// </summary>
        /// <returns>The XElement that is the root of this object's XML tree</returns>
        public XElement GetXml()
        {
            XElement root = new XElement("ApplicableSystemOperationSet");
            foreach (SystemOperation op in this.m_systemOperations)
            {
                root.Add(op.GetXml());
            }

            return root;
        }

        #region IEnumerable<SystemOperation> Members

        public IEnumerator<SystemOperation> GetEnumerator()
        {
            return this.m_systemOperations.GetEnumerator();
        }

        #endregion

        #region IEnumerable Members

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.m_systemOperations.GetEnumerator();
        }

        #endregion
    }
}
