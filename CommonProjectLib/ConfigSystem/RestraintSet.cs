﻿namespace ConfigSystem
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Xml.Linq;
    using CommonProjectLib.Common;

    public class RestraintSet : AbstractSecurityPermissionSet
    {
        /// <summary>
        /// XML element name used when this object is serialized.
        /// </summary>
        protected override string RootName
        {
            get { return "RestraintSet"; }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RestraintSet"/> class.
        /// </summary>
        /// <param name="restraintSetRoot">XElement to parse restraint set from.</param>
        public RestraintSet(XElement restraintSetRoot)
            : base(restraintSetRoot)
        {
            
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RestraintSet"/> class.
        /// </summary>
        public RestraintSet()
            : base()
        {

        }

        /// <summary>
        /// Add restraint to set.
        /// </summary>
        /// <param name="restraint">The restraint to add.</param>
        public void AddRestraint(Restraint restraint)
        {
            abstractConditionList.Add(restraint);
        }

        /// <summary>
        /// Parsed restraint set from XML elelment.
        /// </summary>
        /// <param name="restraintSetRoot">XElement to parse restraint set from.</param>
        protected override void Parse(XElement restraintSetRoot)
        {
            foreach (XElement element in restraintSetRoot.Elements("Restraint"))
            {
                abstractConditionList.Add(new Restraint(element));
            }
        }

        /// <summary>
        /// Gets restraint from set by ID.
        /// </summary>
        /// <param name="id">Restraint ID.</param>
        /// <returns>Restraing object mathcing given ID, or null if match not found.</returns>
        public override AbstractConditionGroup Get(Guid id)
        {
            var ret = base.Get(id);

            return ret == null ? null : (Restraint)ret;
        }

        /// <summary>
        /// Copies restraint by ID and adds it to the set.
        /// </summary>
        /// <param name="id">ID of restraint to copy.</param>
        public override void Copy(Guid id)
        {
            AbstractConditionGroup group = Get(id);
            if (group != null)
            {
                XElement element = group.GetXml();
                AbstractConditionGroup copy = new Restraint(element, Guid.NewGuid());
                abstractConditionList.Add(copy);
            }
            else
            {
                throw CBaseException.CreateGeneric("RestraintSet:: Could not find restraint with id " + id + " to copy.");
            }
        }
    }
}
