﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using CommonProjectLib.Common;

namespace ConfigSystem
{
    public class ConstraintSet : AbstractSecurityPermissionSet
    {
        protected override string RootName
        {
            get { return "ConstraintSet"; }
        }

        public ConstraintSet(XElement constraintSetRoot)
            : base(constraintSetRoot)
        {
            
        }

        public ConstraintSet()
            : base()
        {

        }

        public void AddConstraint(Constraint constraint)
        {
            abstractConditionList.Add(constraint);
        }

        protected override void Parse(XElement conditionSetRoot)
        {
            foreach (XElement element in conditionSetRoot.Elements("Constraint"))
            {
                abstractConditionList.Add(new Constraint(element));
            }
        }

        public override AbstractConditionGroup Get(Guid id)
        {
            var ret = base.Get(id);

            return ret == null ? null : (Constraint)ret;
        }

        public override void Copy(Guid id)
        {
            AbstractConditionGroup group = Get(id);
            if (group != null)
            {
                XElement element = group.GetXml();
                AbstractConditionGroup copy = new Constraint(element, Guid.NewGuid());
                abstractConditionList.Add(copy);
            }
            else
            {
                throw CBaseException.CreateGeneric("ConstraintSet:: Could not find constraint with id " + id + " to copy.");
            }
        }
    }
}
