﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.IO;
using CommonProjectLib.Logging;
using System.Collections;
using CommonProjectLib.Common;

namespace ConfigSystem
{
    public class CustomVariableSet : IEnumerable<CustomVariable>
    {
        private List<CustomVariable> m_customVariables = null;

        private static readonly Func<CustomVariable, string, bool> SearchFilter = (custVar, searchText) =>
                 (custVar.Id.ToString().ContainsIgnoreCase(searchText) ||
                  custVar.Notes.ContainsIgnoreCase(searchText) ||
                  custVar.Name.ContainsIgnoreCase(searchText) ||
                  custVar.Parameters.Any(param => param.Name.ContainsIgnoreCase(searchText)));

        internal List<CustomVariable> CustomVariables
        {
            get { return this.m_customVariables; }
        }

        public List<CustomVariable> GetCustomVariablesByName(string customVariableName)
        {
            return this.m_customVariables.Where(cvar => cvar.Name.EqualsIgnoreCase(customVariableName)).ToList();
        }
        
        public void Add(CustomVariable cv)
        {
            this.m_customVariables.Add(cv);
        }

        public void Remove(Guid id)
        {
            this.m_customVariables.RemoveAll(custVar => custVar.Id == id);
        }

        public void Replace(Guid id, CustomVariable cv)
        {
            var index = this.m_customVariables.FindIndex(acg => acg.Id == id);

            if (index >= 0)
            {
                this.m_customVariables[index] = cv;
            }
        }

        public void Copy(Guid id)
        {
            CustomVariable cvToCopy = Get(id);
            if (cvToCopy != null)
            {
                XElement element = cvToCopy.GetXml();
                CustomVariable copy = new CustomVariable(element, Guid.NewGuid());
                this.m_customVariables.Add(copy);
            }
            else
            {
                throw CBaseException.CreateGeneric("CustomVariableSet:: Could not find custom variable with id " + id + " to copy.");
            }
        }

        public CustomVariable Get(Guid id)
        {
            return this.m_customVariables.Find(cv => cv.Id == id);
        }
        
        public IEnumerable<string> CustomVariableNames
        {
            get
            {
                return this.m_customVariables.Select(cvar => cvar.Name);
            }
        }

        public CustomVariableSet(XElement customVariableSetRoot)
        {
            this.m_customVariables = new List<CustomVariable>();
            Parse(customVariableSetRoot);
        }

        private void Parse(XElement customVariableSetRoot)
        {
            foreach (XElement element in customVariableSetRoot.Elements("CustomVariable"))
            {
                this.m_customVariables.Add(new CustomVariable(element));
            }
        }

        /// <summary>
        /// Generates the XML representing this object.
        /// </summary>
        /// <returns>The XElement that is the root of this object's XML tree</returns>
        public XElement GetXml()
        {
            XElement root = new XElement("CustomVariableSet");
            foreach (CustomVariable customVar in this.m_customVariables)
            {
                root.Add(customVar.GetXml());
            }

            return root;
        }

        public void Filter(string searchText)
        {
            this.m_customVariables.RemoveAll(custVar => CustomVariableSet.SearchFilter(custVar, searchText) == false);
        }
        
        #region IEnumerable<CustomVariable> Members

        public IEnumerator<CustomVariable> GetEnumerator()
        {
            return this.m_customVariables.GetEnumerator();
        }

        #endregion

        #region IEnumerable Members

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.m_customVariables.GetEnumerator();
        }

        #endregion
    }
}
