﻿namespace CommonProjectLib.ConfigSystem
{
    /// <summary>
    /// An interface for identifying system operations.
    /// </summary>
    public interface ISystemOperationIdentifier
    {
        /// <summary>
        /// Gets the identifier of the system operation.
        /// </summary>
        /// <value>The identifier of the system operation.</value>
        string Id { get; }
    }
}
