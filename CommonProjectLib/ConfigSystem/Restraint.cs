﻿namespace ConfigSystem
{
    using System;
    using System.Xml.Linq;

    /// <summary>
    /// Represents workflow restraints.
    /// </summary>
    public class Restraint : AbstractConditionGroup
    {
        /// <summary>
        /// XML element name used when this object is serialized.
        /// </summary>
        protected override string RootName
        {
            get { return "Restraint"; }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Restraint"/> class.
        /// </summary>
        /// <param name="restraintRoot">XElement to parse restraint from.</param>
        public Restraint(XElement restraintRoot)
            : base(restraintRoot)
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Restraint"/> class.
        /// </summary>
        /// <param name="restraintRoot">XElement to parse restraint from.</param>
        /// <param name="newId">Restraint ID.</param>
        public Restraint(XElement restraintRoot, Guid newId)
            : base(restraintRoot, newId)
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Restraint"/> class.
        /// </summary>
        public Restraint() : base()
        {

        }
    }
}
