﻿namespace ConfigSystem
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using CommonProjectLib.Common;

    /// <summary>
    /// Finds the difference between 2 condition sets.
    /// </summary>
    public static class ConditionSetDiff
    {
        /// <summary>
        /// Return broken down set combinations in set1 that are not in set2
        /// </summary>
        /// <param name="set1">Condition set.</param>
        /// <param name="set2">Condition set to compare against.</param>
        /// <returns>A List of conditions from the 1st set that differed from the 2nd set.</returns>
        public static List<AbstractConditionGroup> DetectExternalCombinations(AbstractSecurityPermissionSet set1, AbstractSecurityPermissionSet set2)
        {
            List<AbstractConditionGroup> result = new List<AbstractConditionGroup>();

            Dictionary<Guid, AbstractConditionGroup> set2Hash = new Dictionary<Guid, AbstractConditionGroup>();
            foreach (AbstractConditionGroup c in set2)
            {
                set2Hash.Add(c.Id, c);
            }

            foreach (AbstractConditionGroup c1 in set1)
            {
                if (!set2Hash.ContainsKey(c1.Id))
                    goto FOUND_DIFF;
                else // see if there is configuration/structure difference 
                {
                    AbstractConditionGroup c2 = set2Hash[c1.Id];

                    // Do condition1 and condition2 have the same count of operations?
                    if (c1.SystemOperationNames.Count<string>() != c2.SystemOperationNames.Count<string>())
                        goto FOUND_DIFF;

                    // Do condition1 and condition2 apply to the same set operations?
                    foreach (SystemOperation op1 in c1.SysOpSet)
                    {
                        SystemOperation op2 = c2.SysOpSet.Get(op1.Name);
                        if (op2 == null)
                        {
                            goto FOUND_DIFF;
                        }

                        // Matching op found, check op fields.
                        IEnumerable<string> difference = op1.Fields.Keys.Except(op2.Fields.Keys).Union(op2.Fields.Keys.Except(op1.Fields.Keys));
                        if (difference.Any())
                        {
                            goto FOUND_DIFF;
                        }
                    }
                    
                    foreach (Parameter p1 in c1.ParameterSet)
                    {
                        // Does parameter1 in condition1 exists in condition2?
                        Parameter p2 = c2.ParameterSet.GetParameter(p1.Name);
                        if (p2 == null)
                            goto FOUND_DIFF;

                        // Do parameter1 and parameter2 have the same function type?
                        if (p1.FunctionT != p2.FunctionT)
                            goto FOUND_DIFF;

                        // Do parameter1 and parameter2 have the same number of values?
                        if (p1.Values.Count != p2.Values.Count)
                            goto FOUND_DIFF;

                        // Do parameter1 and parameter2 have the same number of exceptions?
                        if (p1.Exceptions.Count != p2.Exceptions.Count)
                            goto FOUND_DIFF;

                        // Do parameter1 and parameter2 have the same set of values?
                        foreach (string v1 in p1.Values)
                        {
                            if (!p2.Values.Contains(v1))
                                goto FOUND_DIFF;
                        }

                        // Do parameter1 and parameter2 have the same set of exceptions?
                        foreach (string v1 in p1.Exceptions)
                        {
                            if (!p2.Exceptions.Contains(v1))
                                goto FOUND_DIFF;
                        }
                    }
                }

                continue;

                FOUND_DIFF:
                result.Add(c1);
            }

            return result;
        }
    }
}