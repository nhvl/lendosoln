﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using CommonProjectLib.Logging;
using System.Collections;
using CommonProjectLib.Common;

namespace ConfigSystem
{
    public class ConditionSet : AbstractSecurityPermissionSet
    {
        protected override string RootName
        {
            get { return "ConditionSet"; }
        }

        public ConditionSet(XElement conditionSetRoot)
            : base(conditionSetRoot)
        {
            
        }

        public ConditionSet() : base()
        {

        }

        public void AddCondition(Condition condition)
        {
            abstractConditionList.Add(condition);
        }

        public List<Condition> GetConditionsByCustomVariableName(string customVariableName)
        {
            return this.abstractConditionList
                        .Where(condition => condition.ParameterSet.Any(param => param.Name.EqualsIgnoreCase(customVariableName)))
                        .Cast<Condition>()
                        .ToList();
        }

        protected override void Parse(XElement conditionSetRoot)
        {
            foreach (XElement element in conditionSetRoot.Elements("Condition"))
            {
                abstractConditionList.Add(new Condition(element));
            }
        }

        public override AbstractConditionGroup Get(Guid id)
        {
            var ret = base.Get(id);

            return ret == null ? null : (Condition)ret;
        }

        public override void Copy(Guid id)
        {
            AbstractConditionGroup group = Get(id);
            if (group != null)
            {
                XElement element = group.GetXml();
                AbstractConditionGroup copy = new Condition(element, Guid.NewGuid());
                abstractConditionList.Add(copy);
            }
            else
            {
                throw CBaseException.CreateGeneric("ConditionSet:: Could not find Condition with id " + id + " to copy.");
            }
        }
    }
}
