﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace ConfigSystem
{
    public class Condition : AbstractConditionGroup
    {
        protected override string RootName
        {
            get { return "Condition"; }
        }

        public Condition(XElement conditionRoot)
            : base(conditionRoot)
        {

        }

        public Condition(XElement conditionRoot, Guid newId)
            : base(conditionRoot, newId)
        {

        }

        public Condition() : base()
        {

        }
    }
}
