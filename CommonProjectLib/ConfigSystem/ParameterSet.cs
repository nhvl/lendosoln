﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Collections;
using CommonProjectLib.Common;

namespace ConfigSystem
{
    public class ParameterSet : IEnumerable<Parameter>
    {
        private List<Parameter> m_parameters;

        public int ParameterCount
        {
            get 
            {
                return this.m_parameters == null ? 0 : this.m_parameters.Count;
            }
        }

        /// <summary>
        /// Determines whether this parameter set contains a parameter
        /// with the specified <paramref name="varName"/> that is required
        /// for a condition to be included in a user-friendly checklist.
        /// </summary>
        /// <param name="varName">
        /// The name of the parameter.
        /// </param>
        /// <returns>
        /// True if there is a parameter in the set with the name that is required,
        /// false otherwise.
        /// </returns>
        public bool IsRequiredParameterForChecklist(string varName)
        {
            return this.m_parameters.Any(parameter =>
                parameter.RequiredForUserFriendlyChecklistInclusion &&
                string.Equals(varName, parameter.Name, StringComparison.OrdinalIgnoreCase));
        }

        public void AddParameter(Parameter p)
        {
            if (p != null)
            {
                this.m_parameters.Add(p);
            }
        }

        public void RemoveParameter(string parameterName)
        {
            this.m_parameters.RemoveAll(param => param.Name.EqualsIgnoreCase(parameterName));
        }

        public void ReplaceParameter(string parameterName, Parameter p)
        {
            var index = this.m_parameters.FindIndex(param => param.Name.EqualsIgnoreCase(parameterName));
            
            if (index >= 0 && p != null)
            {
                this.m_parameters[index] = p;
            }
        }
        
        public ParameterSet(XElement parameterSetElement)
        {
            this.m_parameters = new List<Parameter>();
            Parse(parameterSetElement);
        }

        public Parameter GetParameter(string parameterName)
        {
            return this.m_parameters.Find(param => param.Name.EqualsIgnoreCase(parameterName));
        }

        public ParameterSet()
        {
            this.m_parameters = new List<Parameter>();
        }

        private void Parse(XElement parameterSetElement)
        {
            foreach (XElement element in parameterSetElement.Elements("Parameter"))
            {
                this.m_parameters.Add(new Parameter(element));
            }
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("PARAMETERS: " + Environment.NewLine);
            foreach (Parameter param in this.m_parameters)
            {
                sb.Append(param + Environment.NewLine);
            }

            return sb.ToString();
        }

        /// <summary>
        /// Generates the XML representing this object.
        /// </summary>
        /// <returns>The XElement that is the root of this object's XML tree</returns>
        public XElement GetXml()
        {
            XElement root = new XElement("ParameterSet");
            foreach (Parameter param in this.m_parameters)
            {
                root.Add(param.GetXml());
            }

            return root;
        }

        #region IEnumerable Members



        public IEnumerator<Parameter> GetEnumerator()
        {
            return this.m_parameters.GetEnumerator();
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.m_parameters.GetEnumerator();
        }

        #endregion

    }
}
