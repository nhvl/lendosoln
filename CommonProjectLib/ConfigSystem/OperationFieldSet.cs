﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Xml.Linq;

namespace ConfigSystem
{
    public class OperationFieldSet : IDictionary<string, OperationField>
    {
        private Dictionary<string, OperationField> m_fields;

        public List<string> FieldNames
        {
            get
            {
                return (from OperationField field in this.m_fields.Values
                        select field.FriendlyName).ToList();
            }
        }

        public OperationFieldSet()
        {
            this.m_fields = new Dictionary<string, OperationField>();
        }

        public OperationFieldSet(XElement loanFieldSetRoot)
        {
            Parse(loanFieldSetRoot);
        }

        private void Parse(XElement fieldSetElement)
        {
            foreach (XElement element in fieldSetElement.Elements("Field"))
            {
                var field = new OperationField(element);
                this.Add(field);
            }
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("FIELDS: " + Environment.NewLine);
            foreach (OperationField field in this.m_fields.Values)
            {
                sb.Append(field.Id + Environment.NewLine);
            }

            return sb.ToString();
        }

        /// <summary>
        /// Generates the XML representing this object.
        /// </summary>
        /// <returns>The XElement that is the root of this object's XML tree</returns>
        public XElement GetXml()
        {
            XElement root = new XElement("FieldSet");
            foreach (OperationField field in this.m_fields.Values)
            {
                root.Add(field.GetXml());
            }

            return root;
        }

        public void Add(OperationField field)
        {
            // TODO: Pre-validation if necessary
            this.m_fields.Add(field.Id, field);
        }

        public bool Remove(string key)
        {
            return this.m_fields.Remove(key);
        }

        #region IDictionary<string,OperationField> Members

        public void Add(string key, OperationField value)
        {
            this.m_fields.Add(key, value);
        }

        public bool ContainsKey(string key)
        {
            return this.m_fields.ContainsKey(key);
        }

        public ICollection<string> Keys
        {
            get { return this.m_fields.Keys; }
        }

        bool IDictionary<string, OperationField>.Remove(string key)
        {
            return this.m_fields.Remove(key);
        }

        public bool TryGetValue(string key, out OperationField value)
        {
            return this.m_fields.TryGetValue(key, out value);
        }

        public ICollection<OperationField> Values
        {
            get { return this.m_fields.Values; }
        }

        public OperationField this[string key]
        {
            get
            {
                return this.m_fields[key];
            }
            set
            {
                this.m_fields[key] = value;
            }
        }
        #endregion

        #region ICollection<KeyValuePair<string,OperationField>> Members

        public void Add(KeyValuePair<string, OperationField> item)
        {
            this.m_fields.Add(item.Key, item.Value);
        }

        public void Clear()
        {
            this.m_fields.Clear();
        }

        public bool Contains(KeyValuePair<string, OperationField> item)
        {
            return this.m_fields.Contains(item);
        }

        public void CopyTo(KeyValuePair<string, OperationField>[] array, int arrayIndex)
        {
            throw new NotImplementedException();
        }

        int ICollection<KeyValuePair<string, OperationField>>.Count
        {
            get { return this.m_fields.Count; }
        }

        public bool IsReadOnly
        {
            get { return false; }
        }

        public bool Remove(KeyValuePair<string, OperationField> item)
        {
            return this.m_fields.Remove(item.Key);
        }

        #endregion

        #region IEnumerable<KeyValuePair<string,OperationField>> Members

        public IEnumerator<KeyValuePair<string, OperationField>> GetEnumerator()
        {
            return this.m_fields.GetEnumerator();
        }

        #endregion

        #region IEnumerable Members

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.m_fields.GetEnumerator();
        }

        #endregion
    }
}
