﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommonProjectLib.Common;
using System.Diagnostics;
using CommonProjectLib.Logging;
    
namespace ConfigSystem
{
    public class ConflictDetector
    {
        public ConflictDetector()
        {
        }

        /// <summary>
        /// case-insensitive comparision
        /// </summary>
        /// <param name="strSet1">a set of literals or integers</param>
        /// <param name="strSet2">a set of literals or integers</param>
        /// <returns></returns>
        static private bool AreIntersectingSets(IEnumerable<string> strSet1, IEnumerable<string> strSet2)
        {
            foreach( string str1 in strSet1 )
            {
                string s1 = str1.ToLower();

                foreach (string str2 in strSet2)
                {
                    if (s1.CompareTo(str2.ToLower()) == 0)
                        return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Compares two operation sets to see if they intersect.
        /// </summary>
        /// <param name="opSet1">a set of system operations</param>
        /// <param name="opSet2">a set of system operations</param>
        /// <remarks>
        /// Operations are considered a match only if they have the same Name
        /// and if their field sets are either both empty or intersect.
        /// </remarks>
        /// <returns>True if the operation sets intersect. False otherwise.</returns>
        static private bool AreIntersectingOps(SystemOperationSet opSet1, SystemOperationSet opSet2)
        {
            foreach (SystemOperation op1 in opSet1)
            {
                SystemOperation op2 = opSet2.Get(op1.Name);

                if (op2 != null)
                {
                    if (!op1.Fields.Any() && !op2.Fields.Any())
                    {
                        return true;
                    }

                    return AreIntersectingSets(op1.Fields.Keys, op2.Fields.Keys);
                }
            }

            return false;
        }

        static private bool DoesContainBlankString(ICollection<string> constraintValues)
        {
            foreach (string str in constraintValues)
            {
                if (str == "")
                    return true;
            }
            return false;
        }
        static private string ParameterDesc( Parameter p )
        {
            StringBuilder sb = new StringBuilder();
            
            sb.AppendFormat( "Parameter: Name={0}, FunctionType={1}, ValueType={2}" + Environment.NewLine, p.Name, p.FunctionT, p.ValueType );

            sb.AppendLine("Values =");          
            foreach (string v in p.Values)
            {
                sb.AppendFormat( "val:{0}({1});", v, p.ValueDesc[v]);
            }

            if (p.Exceptions.Count > 0)
            {
                sb.AppendLine();
                sb.AppendLine("Exceptions =");
                foreach (string e in p.Exceptions)
                {
                    sb.AppendFormat("val:{0}({1});", e, p.ExceptionDesc[e]);
                }
            }

            return sb.ToString();
        }

        static private string InvalidScenarioMsg(Parameter p)
        {
            return string.Format("Invalid scenario." + ParameterDesc( p ) );
        }

        static private bool AreIntersectedParamSets(ParameterSet ruleParams, ParameterSet constraintParams)
        {
            foreach (Parameter constraintP in constraintParams)
            {
                Parameter ruleP = ruleParams.GetParameter(constraintP.Name);
                
                if( ruleP == null ) // it's a don't care parameter in rule, that's considered a parameter intersection
                    continue;

                if( !ConflictDetector.AreIntersectedParams( ruleP, constraintP ) )
                {
                    return false;
                }
            }
            return true; // if all parameters intersect
        }

        static private decimal[] StrCollectionToDecimalArray(ICollection<string> collection)
        {
            decimal[] r = new decimal[collection.Count];

            int i = 0;
            foreach (string s in collection)
            {
                r[i++] = decimal.Parse(s);
            }
            return r;
        }

        static private DateTime[] StrCollectionToDateTimeArray(ICollection<string> collection)
        {
            DateTime[] r = new DateTime[collection.Count];

            int i = 0;
            foreach (string s in collection)
            {
                r[i++] = DateTime.Parse(s);
            }
            return r;
        }

        static private bool StrCollectionToBool(ICollection<string> collection)
        {
            if (collection.Count != 1)
               throw CBaseException.CreateGeneric( "ConflictDetector:StrCollectionToBool() expects collection to have 1 value. Actual#:" + collection.Count);

            foreach (string s in collection)
            {
                return bool.Parse(s);
            }
            throw CBaseException.CreateGeneric( "Bug: Unreachable code has been reached in ConflictDetector:StrCollectionToBool!");
        }

        private enum E_CompareValueType
        {
            Float,
            DateTime
        }
        static private bool AreIntersectingClosedRanges(E_CompareValueType compareValueT, ICollection<string> closeR1, ICollection<string> closeR2)
        {
            if (closeR1.Count != 2 || closeR2.Count != 2)
                throw CBaseException.CreateGeneric(
                    string.Format("ConflictDetector:AreIntersectingClosedRanges expecting both input collections to have exact 2 strings each but here are what we get:", closeR1.Count, closeR2.Count));
           
            switch( compareValueT )
            {
                case E_CompareValueType.Float:
                    {
                        decimal[] c1 = StrCollectionToDecimalArray(closeR1);
                        decimal[] c2 = StrCollectionToDecimalArray(closeR2);

                    if (c1[0] > c2[1] || c2[0] > c1[1])
                        return false;
                    return true;
                    }
                case E_CompareValueType.DateTime:
                    {
                        DateTime[] c1 = StrCollectionToDateTimeArray(closeR1);
                        DateTime[] c2 = StrCollectionToDateTimeArray(closeR2);

                    if (c1[0] > c2[1] || c2[0] > c1[1])
                        return false;
                    return true;
                    }
                default:
                    throw CBaseException.CreateUnexpectedEnumException(compareValueT);
            }
            
        }

        static private bool AreIntersectingCloseAndGreaterRange(E_CompareValueType compareValueT, ICollection<string> closeR, ICollection<string> greaterR)
        {
            if (closeR.Count != 2)
                throw CBaseException.CreateGeneric("ConflictDetector:AreIntersectingCloseAndGreaterRange expect CloseR.Count to be 2. Actual count:" + closeR.Count);

            if (greaterR.Count != 1)
                throw CBaseException.CreateGeneric( "ConflictDetector:AreIntersectingCloseAndGreaterRange expects greaterR.Count to be 1. Actual count:" + greaterR.Count);

            switch (compareValueT)
            {
                case E_CompareValueType.Float:
                    {
                        decimal[] c = StrCollectionToDecimalArray(closeR);
                        decimal[] g = StrCollectionToDecimalArray(greaterR);

                        if (g[0] >= c[1])
                            return false;
                        return true;
                    }
                case E_CompareValueType.DateTime:
                    {
                        DateTime[] c = StrCollectionToDateTimeArray(closeR);
                        DateTime[] g = StrCollectionToDateTimeArray(greaterR);

                        if (g[0] >= c[1])
                            return false;
                        return true;

                    }
                default:
                    throw CBaseException.CreateUnexpectedEnumException(compareValueT);
            }
        }


        static private bool AreIntersectingCloseAndGreaterEqualRange(E_CompareValueType compareValueT, ICollection<string> closeR, ICollection<string> greaterEqualR)
        {
            if (closeR.Count != 2)
                throw CBaseException.CreateGeneric( "ConflictDetector:AreIntersectingCloseAndGreaterEqualRange expect CloseR.Count to be 2. Actual count:" + closeR.Count);
            if (greaterEqualR.Count != 1)
                throw CBaseException.CreateGeneric( "ConflictDetector:AreIntersectingCloseAndGreaterEqualRange expects greaterEqualR.Count to be 1. Actual count:" + greaterEqualR.Count);

            switch (compareValueT)
            {
                case E_CompareValueType.Float:
                    {
                        decimal[] c = StrCollectionToDecimalArray(closeR);
                        decimal[] ge = StrCollectionToDecimalArray(greaterEqualR);

                        if (ge[0] > c[1])
                            return false;
                        return true;
                    }
                case E_CompareValueType.DateTime:
                    {
                        DateTime[] c = StrCollectionToDateTimeArray(closeR);
                        DateTime[] ge = StrCollectionToDateTimeArray(greaterEqualR);

                        if (ge[0] > c[1])
                            return false;
                        return true;

                    }
                default:
                    throw CBaseException.CreateUnexpectedEnumException( compareValueT );
            }

        }

        static private bool AreIntersectingCloseAndLesserRange(E_CompareValueType compareValueT, ICollection<string> closeR, ICollection<string> lesserR)
        {
            if (closeR.Count != 2)
                throw CBaseException.CreateGeneric( "AreIntersectingCloseAndLesserRange expect CloseR.Count to be 2");

            if (lesserR.Count != 1)
                throw CBaseException.CreateGeneric( "AreIntersectingCloseAndLesserRange expects lesserR.Count to be 1");

            switch (compareValueT)
            {
                case E_CompareValueType.Float:
                    {
                        decimal[] c = StrCollectionToDecimalArray(closeR);
                        decimal[] l = StrCollectionToDecimalArray(lesserR);

                        if (l[0] <= c[0])
                            return false;
                        return true;
                    }
                case E_CompareValueType.DateTime:
                    {
                        DateTime[] c = StrCollectionToDateTimeArray(closeR);
                        DateTime[] l = StrCollectionToDateTimeArray(lesserR);

                        if (l[0] <= c[0])
                            return false;
                        return true;

                    }
                default:
                    throw CBaseException.CreateUnexpectedEnumException(compareValueT);
            }
        }


        static private bool AreIntersectingCloseAndLesserEqualRange(E_CompareValueType compareValueT, ICollection<string> closeR, ICollection<string> lesserEqualR)
        {
            if (closeR.Count != 2)
                throw CBaseException.CreateGeneric( "AreIntersectingCloseAndLesserEqualRange expect closeR.Count to be 2");

            if (lesserEqualR.Count != 1)
                throw CBaseException.CreateGeneric( "AreIntersectingCloseAndLesserEqualRange expects lesserEqualR.Count to be 1");

            switch (compareValueT)
            {
                case E_CompareValueType.Float:
                    {
                        decimal[] c = StrCollectionToDecimalArray(closeR);
                        decimal[] le = StrCollectionToDecimalArray(lesserEqualR);

                        if (le[0] < c[0])
                            return false;
                        return true;
                    }
                case E_CompareValueType.DateTime:
                    {
                        DateTime[] c = StrCollectionToDateTimeArray(closeR);
                        DateTime[] le = StrCollectionToDateTimeArray(lesserEqualR);

                        if (le[0] < c[0])
                            return false;
                        return true;

                    }
                default:
                    throw CBaseException.CreateUnexpectedEnumException(compareValueT);
            }
        }

        static private bool AreIntersectingCloseAndIsNonzero(ICollection<string> closeR, ICollection<string> isNonzeroValues)
        {
            if (closeR.Count != 2)
                throw CBaseException.CreateGeneric( "AreIntersectingCloseAndIsNonzero expect closeR.Count to be 2");

            if (isNonzeroValues.Count != 1)
                throw CBaseException.CreateGeneric( "AreIntersectingCloseAndIsNonzero expects isNonzeroValues.Count to be 1");


            decimal[] c = StrCollectionToDecimalArray(closeR);
            bool isNonzeroValueOnly = StrCollectionToBool(isNonzeroValues);

            if (isNonzeroValueOnly)
            {
                if (c[0] != 0 || c[1] != 0)
                    return true; // as long as the closed range is not [0,0] then they intersect
            }
            else // the constraint param is looking for 0 or undefined value only
            {
                // Note: undefined value is not possible in closed range.

                if (c[0] <= 0 && c[1] >= 0)
                    return true;
            }
            
            return false;
        }

        static private bool AreIntersectingCloseAndAnyValueAcceptance(E_CompareValueType compareValueT, ICollection<string> closeR, ICollection<string> anyValueAcceptanceValues)
        {
            if (closeR.Count != 2)
                throw CBaseException.CreateGeneric( "AreIntersectingCloseAndAnyValueAcceptance expect closeR.Count to be 2");

            if (anyValueAcceptanceValues.Count != 1)
                throw CBaseException.CreateGeneric( "AreIntersectingCloseAndAnyValueAcceptance expects anyValueAcceptanceValues.Count to be 1");

            if( StrCollectionToBool(anyValueAcceptanceValues) )
                return true; // closed range cannot contain undefined value, so it's always true
            return false;
        }

        static private bool AreIntersectingGreaterAndGreaterRange(E_CompareValueType compareValueT, ICollection<string> greaterR1, ICollection<string> greaterR2 )
        {
            if( greaterR1.Count != 1)
                throw CBaseException.CreateGeneric( "AreIntersectingGreaterAndGreaterRange expect greaterR1.Count to be 1");
            if (greaterR2.Count != 1)
                throw CBaseException.CreateGeneric( "AreIntersectingGreaterAndGreaterRange expect greaterR2.Count to be 1");

            return true; // Greater ranges always intersect
        }

        static private bool AreIntersectingGreaterAndLesserRange(E_CompareValueType compareValueT, ICollection<string> greaterR, ICollection<string> lesserR)
        {
            if (greaterR.Count != 1)
                throw CBaseException.CreateGeneric( "AreIntersectingGreaterAndLesserRange expect greaterR.Count to be 1");
            if (lesserR.Count != 1)
               throw CBaseException.CreateGeneric( "AreIntersectingGreaterAndLesserRange expects lesserR.Count to be 1");

            switch (compareValueT)
            {
                case E_CompareValueType.Float:
                    {
                        decimal[] g = StrCollectionToDecimalArray(greaterR);
                        decimal[] l = StrCollectionToDecimalArray(lesserR);
                        if (l[0] <= g[0])
                            return false;
                        return true;
                    }
                case E_CompareValueType.DateTime:
                    {
                        DateTime[] g = StrCollectionToDateTimeArray(greaterR);
                        DateTime[] l = StrCollectionToDateTimeArray(lesserR);
                        if (l[0] <= g[0])
                            return false;
                        return true;
                    }
                default:
                    throw CBaseException.CreateUnexpectedEnumException(compareValueT);
            }
        }

        static private bool AreIntersectingGreaterAndGreaterEqualRange(E_CompareValueType compareValueT, ICollection<string> greaterR, ICollection<string> greaterEqualR)
        {
            if (greaterR.Count != 1)
                throw CBaseException.CreateGeneric( "AreIntersectingGreaterAndGreaterEqualRange expect greaterR.Count to be 1");
            if (greaterEqualR.Count != 1)
                throw CBaseException.CreateGeneric( "AreIntersectingGreaterAndGreaterEqualRange expect greaterEqualR.Count to be 1");

            return true; // Greater ranges always intersect

        }

        static private bool AreIntersectingGreaterAndLesserEqualRange(E_CompareValueType compareValueT, ICollection<string> greaterR, ICollection<string> lesserEqualR)
        {
            if (greaterR.Count != 1)
                throw CBaseException.CreateGeneric( "AreIntersectingGreaterAndLesserEqualRange expect greaterR.Count to be 1");
            if (lesserEqualR.Count != 1)
                throw CBaseException.CreateGeneric( "AreIntersectingGreaterAndLesserEqualRange expects lesserEqualR.Count to be 1");

            switch (compareValueT)
            {
                case E_CompareValueType.Float:
                    {
                        decimal[] g = StrCollectionToDecimalArray(greaterR);
                        decimal[] le = StrCollectionToDecimalArray(lesserEqualR);
                        if (le[0] <= g[0])
                            return false;
                        return true;
                    }
                case E_CompareValueType.DateTime:
                    {
                        DateTime[] g = StrCollectionToDateTimeArray(greaterR);
                        DateTime[] le = StrCollectionToDateTimeArray(lesserEqualR);
                        if (le[0] <= g[0])
                            return false;
                        return true;
                    }
                default:
                    throw CBaseException.CreateUnexpectedEnumException(compareValueT);
            }
        }

        static private bool AreIntersectingGreaterAndIsNonzero(ICollection<string> greaterR, ICollection<string>  isNonzeroValues)
        {
            if (greaterR.Count != 1)
                throw CBaseException.CreateGeneric( "AreIntersectingGreaterAndIsNonzero expect greaterR.Count to be 1");
            if (isNonzeroValues.Count != 1)
                throw CBaseException.CreateGeneric( "AreIntersectingGreaterAndIsNonzero expects isNonzeroValues.Count to be 1");
            
            decimal[] g = StrCollectionToDecimalArray(greaterR);
            bool isNonzero = StrCollectionToBool(isNonzeroValues);

            if (isNonzero)
            {
                return true; // greater range always contains a non-zero value
            }
            else // looking for only zero or underfined. (note: Undefined doesn't exist in a greater range)
            {
                if (g[0] < 0)
                    return true;
                return false;
            }
        }

        static private bool AreIntersectingGreaterAndAnyValueAcceptance(E_CompareValueType compareValueT, ICollection<string> greaterR, ICollection<string> anyValueAcceptanceValues)
        {
            if (greaterR.Count != 1)
                throw CBaseException.CreateGeneric( "AreIntersectingGreaterAndAnyValueAcceptance expect greaterR.Count to be 1");
            if (anyValueAcceptanceValues.Count != 1)
                throw CBaseException.CreateGeneric( "AreIntersectingGreaterAndAnyValueAcceptance expects anyValueAcceptanceValues.Count to be 1");

            if (StrCollectionToBool(anyValueAcceptanceValues))
                return true; // greaterR range cannot contain undefined value, so it's always true
            return false;
        }

        static private bool AreIntersectingLesserAndLesserRange(E_CompareValueType compareValueT, ICollection<string> lesserR1, ICollection<string> lesserR2)
        {
            if (lesserR1.Count != 1)
                throw CBaseException.CreateGeneric("AreIntersectingLesserAndLesserRange expect lesserR1.Count to be 1");
            if (lesserR2.Count != 1)
                throw CBaseException.CreateGeneric("AreIntersectingLesserAndLesserRange expects lesserR2.Count to be 1");

            return true; // lesser ranges always intersect
        }

        static private bool AreIntersectingGreaterEqualAndLesserRange(E_CompareValueType compareValueT, ICollection<string> greaterEqualR, ICollection<string> lesserR)
        {
            if (greaterEqualR.Count != 1)
                throw CBaseException.CreateGeneric( "AreIntersectingGreaterEqualAndLesserRange expect greaterR.Count to be 1");
            if (lesserR.Count != 1)
                throw CBaseException.CreateGeneric( "AreIntersectingGreaterEqualAndLesserRange expects lesserR.Count to be 1");

            switch (compareValueT)
            {
                case E_CompareValueType.Float:
                    {
                        decimal[] ge = StrCollectionToDecimalArray(greaterEqualR);
                        decimal[] l = StrCollectionToDecimalArray(lesserR);
                        if (l[0] <= ge[0])
                            return false;
                        return true;
                    }
                case E_CompareValueType.DateTime:
                    {
                        DateTime[] ge = StrCollectionToDateTimeArray(greaterEqualR);
                        DateTime[] l = StrCollectionToDateTimeArray(lesserR);
                        if (ge[0] <= l[0])
                            return false;
                        return true;
                    }
                default:
                    throw CBaseException.CreateUnexpectedEnumException(compareValueT);
            }
        }

        static private bool AreIntersectingLesserAndLessEqualRange(E_CompareValueType compareValueT, ICollection<string> lesserR, ICollection<string> lesserEqualR)
        {
            if (lesserR.Count != 1)
                throw CBaseException.CreateGeneric( "AreIntersectingLesserAndLessEqualRange expect lesserR.Count to be 1");
            if (lesserEqualR.Count != 1)
                throw CBaseException.CreateGeneric( "AreIntersectingLesserAndLessEqualRange expects lesserEqualR.Count to be 1");

            return true; // lesser ranges always intersect
        }
        static private bool AreIntersectingLesserAndIsNonzero(ICollection<string> lesserR, ICollection<string> isNonzeroValues)
        {
            if (lesserR.Count != 1)
                throw CBaseException.CreateGeneric( "AreIntersectingLesserAndIsNonzero expect lesserR.Count to be 1");
            if (isNonzeroValues.Count != 1)
                throw CBaseException.CreateGeneric( "AreIntersectingLesserAndIsNonzero expects isNonzeroValues.Count to be 1");

            decimal[] l = StrCollectionToDecimalArray(lesserR);
            bool isNonzero = StrCollectionToBool(isNonzeroValues);

            if (isNonzero)
            {
                return true; // lesser range always contains a non-zero value
            }
            else // looking for only zero or underfined. (note: Undefined doesn't exist in a greater range)
            {
                if (l[0] > 0)
                    return true;
                return false;
            }
        }
        static private bool AreIntersectingLesserAndAnyValueAcceptance(E_CompareValueType compareValueT, ICollection<string> lesserR, ICollection<string> anyValueAcceptanceValues)
        {
            if (lesserR.Count != 1)
                throw CBaseException.CreateGeneric( "AreIntersectingGreaterAndAnyValueAcceptance expect lesserR.Count to be 1");
            if (anyValueAcceptanceValues.Count != 1)
                throw CBaseException.CreateGeneric( "AreIntersectingGreaterAndAnyValueAcceptance expects anyValueAcceptanceValues.Count to be 1");

            if (StrCollectionToBool(anyValueAcceptanceValues))
                return true; // greaterR range always contains valid value(s), so it's true here
            return false;
 
        }

        static private bool AreIntersectingGreaterEqualAndGreaterEqualRange(E_CompareValueType compareValueT, ICollection<string> greaterEqualR1, ICollection<string> greaterEqualR2)
        {
            if (greaterEqualR1.Count != 1)
                throw CBaseException.CreateGeneric( "AreIntersectingGreaterEqualAndGreaterEqualRange expect greaterEqualR1.Count to be 1");
            if (greaterEqualR2.Count != 1)
                throw CBaseException.CreateGeneric( "AreIntersectingGreaterEqualAndGreaterEqualRange expect greaterEqualR2.Count to be 1");

            return true; // Greater ranges always intersect
        }

        static private bool AreIntersectingGreaterEqualAndLesserEqualRange(E_CompareValueType compareValueT, ICollection<string> greaterEqualR, ICollection<string> lesserEqualR)
        {
            if (greaterEqualR.Count != 1)
                throw CBaseException.CreateGeneric( "AreIntersectingGreaterEqualAndLesserEqualRange expects greaterR.Count to be 1");
            if (lesserEqualR.Count != 1)
                throw CBaseException.CreateGeneric( "AreIntersectingGreaterEqualAndLesserEqualRange expects lesserEqualR.Count to be 1");

            switch (compareValueT)
            {
                case E_CompareValueType.Float:
                    {
                        decimal[] ge = StrCollectionToDecimalArray(greaterEqualR);
                        decimal[] le = StrCollectionToDecimalArray(lesserEqualR);
                        if (le[0] < ge[0])
                            return false;
                        return true;
                    }
                case E_CompareValueType.DateTime:
                    {
                        DateTime[] ge = StrCollectionToDateTimeArray(greaterEqualR);
                        DateTime[] l = StrCollectionToDateTimeArray(lesserEqualR);
                        if (ge[0] < l[0])
                            return false;
                        return true;
                    }
                default:
                    throw CBaseException.CreateUnexpectedEnumException(compareValueT);
            }
        }

        static private bool AreIntersectingGreaterEqualAndIsNonzero(ICollection<string> greaterEqualR, ICollection<string> isNonzeroValues)
        {
            if (greaterEqualR.Count != 1)
                throw CBaseException.CreateGeneric( "AreIntersectingGreaterEqualAndIsNonzero expect AreIntersectingGreaterEqualAndIsNonzero.Count to be 1");
            if (isNonzeroValues.Count != 1)
                throw CBaseException.CreateGeneric( "AreIntersectingGreaterEqualAndIsNonzero expects isNonzeroValues.Count to be 1");

            decimal[] ge = StrCollectionToDecimalArray(greaterEqualR);
            bool isNonzero = StrCollectionToBool(isNonzeroValues);

            if (isNonzero)
            {
                return true; // greater range always contains a non-zero value
            }
            else // looking for only zero or underfined. (note: Undefined doesn't exist in a greater range)
            {
                if (ge[0] <= 0)
                    return true;
                return false;
            }
        }

        static private bool AreIntersectingGreaterEqualAndAnyValueAcceptance(E_CompareValueType compareValueT, ICollection<string> greaterEqualR, ICollection<string> anyValueAcceptanceValues)
        {
            if (greaterEqualR.Count != 1)
                throw CBaseException.CreateGeneric( "AreIntersectingGreaterEqualAndAnyValueAcceptance expect greaterEqualR.Count to be 1");
            if (anyValueAcceptanceValues.Count != 1)
                throw CBaseException.CreateGeneric( "AreIntersectingGreaterEqualAndAnyValueAcceptance expects anyValueAcceptanceValues.Count to be 1");

            if (StrCollectionToBool(anyValueAcceptanceValues))
                return true; // greaterEqualR range always contains valid value(s), so it's true here
            return false;
        }

        static private bool AreIntersectingLesserEqualAndLesserEqualRange(E_CompareValueType compareValueT, ICollection<string> lesserEqualR1, ICollection<string> lesserEqualR2)
        {
            if (lesserEqualR1.Count != 1)
                throw CBaseException.CreateGeneric( "AreIntersectingLesserEqualAndLesserEqualRange expects lesserEqualR1.Count to be 1");
            if (lesserEqualR2.Count != 1)
                throw CBaseException.CreateGeneric( "AreIntersectingLesserEqualAndLesserEqualRange expects lesserEqualR1.Count to be 1");

            return true; // lesser ranges always intersect

        }

        static private bool AreIntersectingLesserEqualAndIsNonzero(ICollection<string> lesserEqualR, ICollection<string> isNonzeroValues)
        {
            if (lesserEqualR.Count != 1)
                throw CBaseException.CreateGeneric("AreIntersectingLesserEqualAndIsNonzero expect lesserR.Count to be 1");
            if (isNonzeroValues.Count != 1)
                throw CBaseException.CreateGeneric( "AreIntersectingLesserEqualAndIsNonzero expects isNonzeroValues.Count to be 1");

            decimal[] ge = StrCollectionToDecimalArray(lesserEqualR);
            bool isNonzero = StrCollectionToBool(isNonzeroValues);

            if (isNonzero)
            {
                return true; // lesser range always contains a non-zero value
            }
            else // looking for only zero or underfined. (note: Undefined doesn't exist in a greater range)
            {
                if (ge[0] >= 0)
                    return true;
                return false;
            }
        }

        static private bool AreIntersectingLesserEqualAndAnyValueAcceptance(E_CompareValueType compareValueT, ICollection<string> lesserEqualR, ICollection<string> anyValueAcceptanceValues)
        {
            if (lesserEqualR.Count != 1)
                throw CBaseException.CreateGeneric( "AreIntersectingLesserEqualAndAnyValueAcceptance expect lesserEqualR.Count to be 1");
            if (anyValueAcceptanceValues.Count != 1)
                throw CBaseException.CreateGeneric( "AreIntersectingLesserEqualAndAnyValueAcceptance expects anyValueAcceptanceValues.Count to be 1");

            if (StrCollectionToBool(anyValueAcceptanceValues))
                return true; // lesserEqualR range always contains valid value(s), so it's true here
            return false;

        }

        static private bool AreIntersectingIsNonzeroAndIsNonzero(ICollection<string> isNonzeroValues1, ICollection<string> isNonzeroValues2)
        {
            if (isNonzeroValues1.Count != 1)
                throw CBaseException.CreateGeneric( "AreIntersectingIsNonzeroAndIsNonzero expects isNonzeroValues1.Count to be 1");
            if (isNonzeroValues2.Count != 1)
                throw CBaseException.CreateGeneric( "AreIntersectingIsNonzeroAndIsNonzero expects isNonzeroValues2.Count to be 1");

            return StrCollectionToBool(isNonzeroValues1) == StrCollectionToBool(isNonzeroValues2);
        }

        static private bool AreIntersectingIsNonblankStrAndIsNonblankStr(ICollection<string> isNonblankStrValues1, ICollection<string> isNonblankStrValues2)
        {
            if (isNonblankStrValues1.Count != 1)
                throw CBaseException.CreateGeneric( "AreIntersectingIsNonblankStrAndIsNonblankStr expects isNonblankStrValues1.Count to be 1");
            if (isNonblankStrValues2.Count != 1)
                throw CBaseException.CreateGeneric( "AreIntersectingIsNonblankStrAndIsNonblankStr expects isNonblankStrValues2.Count to be 1");

            return StrCollectionToBool(isNonblankStrValues1) == StrCollectionToBool(isNonblankStrValues2);
        }

        static private bool AreIntersectingIsNonzeroAndAnyValueAcceptance(ICollection<string> isNonzeroValues, ICollection<string> anyValueAcceptanceValues)
        {
            if (isNonzeroValues.Count != 1)
                throw CBaseException.CreateGeneric( "AreIntersectingIsNonzeroAndAnyValueAcceptance expects isNonzeroValues.Count to be 1");
            if (anyValueAcceptanceValues.Count != 1)
                throw CBaseException.CreateGeneric( "AreIntersectingIsNonzeroAndAnyValueAcceptance expects anyValueAcceptanceValues.Count to be 1");

            bool isNonzero = StrCollectionToBool(isNonzeroValues); // excludes only 0 and undefined
            bool isAnyValue = StrCollectionToBool(anyValueAcceptanceValues); // excludes only undefined

            if (isAnyValue) // includes all values including 0, but excludes undefined
            {
                return true; // because isNonzero = false; it would include 0 which is a valid value
                             // if isNonzero = true; it would include many normal numbers which are also valid values
            }
            else // includes only undefined
            {
                if (isNonzero)
                    return false;
                else // includes 0 and undefined
                    return true;
            }
        }

        static private bool AreIntersectingAnyValueAcceptanceAndAnyValueAcceptance(E_CompareValueType compareValueT, ICollection<string> anyValueAcceptanceValues1, ICollection<string> anyValueAcceptanceValues2)
        {
            if (anyValueAcceptanceValues1.Count != 1)
                throw CBaseException.CreateGeneric( "AreIntersectingAnyValueAcceptanceAndAnyValueAcceptance expects anyValueAcceptanceValues1.Count to be 1");
            if (anyValueAcceptanceValues2.Count != 1)
                throw CBaseException.CreateGeneric( "AreIntersectingAnyValueAcceptanceAndAnyValueAcceptance expects anyValueAcceptanceValues2.Count to be 1");

            return StrCollectionToBool(anyValueAcceptanceValues1) == StrCollectionToBool(anyValueAcceptanceValues2);

        }
 
        static private bool AreIntersectedParams(Parameter ruleParam, Parameter constraintParam )
        {
            ICollection<string> ruleValues = ruleParam.Values;
            ICollection<string> constraintValues = constraintParam.Values;

            E_FunctionT ruleFT = ruleParam.FunctionT;
            E_FunctionT constraintFT = constraintParam.FunctionT;

            E_ParameterValueType valType = ruleParam.ValueType;

            #region VALIDATING INPUTS
            if (ruleParam.Name.ToLower() != constraintParam.Name.ToLower())
                throw CBaseException.CreateGeneric(string.Format("ruleParam and constraintParam names are not matched in AreIntersected().",
                    ParameterDesc(ruleParam), ParameterDesc(constraintParam)));

            if (ruleParam.ValueType != constraintParam.ValueType)
                throw CBaseException.CreateGeneric( 
                    string.Format( "param1 and param2 value types are not matched in AreIntersected(). \r\nPARAMETER1:{0}\r\nPARAMETER2:{1}"
                    , ParameterDesc(ruleParam), ParameterDesc(constraintParam)));


            Debug.Assert(valType == constraintParam.ValueType, "Parameter of the same name must have the same value type.");

            switch (ruleFT)
            {
                case E_FunctionT.In:
                case E_FunctionT.Between:
                case E_FunctionT.Equal:
                case E_FunctionT.IsNonblankStr:
                case E_FunctionT.IsGreaterThan:
                case E_FunctionT.IsLessThan:
                case E_FunctionT.IsGreaterThanOrEqualTo:
                case E_FunctionT.IsLessThanOrEqualTo:
                case E_FunctionT.IsNonzeroValue:
                case E_FunctionT.IsAnyValue:
                    break;
                
                case E_FunctionT.IsLessThanVar:
                case E_FunctionT.IsGreaterThanVar:
                case E_FunctionT.EqualVar:
                case E_FunctionT.IsGreaterThanOrEqualToVar:
                case E_FunctionT.IsLessThanOrEqualToVar:
                    throw CBaseException.CreateGeneric( string.Format( "Comparing to another variable (operation={0})is not supported.", ruleFT) );
                default:
                    throw CBaseException.CreateUnexpectedEnumException(ruleFT);
            }

            switch (constraintFT)
            {
                case E_FunctionT.In:
                case E_FunctionT.Between:
                case E_FunctionT.Equal:
                case E_FunctionT.IsNonblankStr:
                case E_FunctionT.IsGreaterThan:
                case E_FunctionT.IsLessThan:
                case E_FunctionT.IsGreaterThanOrEqualTo:
                case E_FunctionT.IsLessThanOrEqualTo:
                case E_FunctionT.IsNonzeroValue:
                case E_FunctionT.IsAnyValue:
                    break;
                // These don't make sense for constraint because there is no loan data to do such comparison
                case E_FunctionT.IsLessThanVar:
                case E_FunctionT.IsGreaterThanVar:
                case E_FunctionT.EqualVar:
                case E_FunctionT.IsGreaterThanOrEqualToVar:
                case E_FunctionT.IsLessThanOrEqualToVar:
                    throw CBaseException.CreateGeneric(string.Format("Comparing to another variable (operation:{0}) is not supported in constraint.", constraintFT));
                default:
                    throw CBaseException.CreateUnexpectedEnumException(constraintFT);
            }
            #endregion // VALIDATING_INPUTS

            // FUTURE: Need to "not-match" the following scenario: condition param: sTotalLAmt >= sLAmt, constraint:sTotalLAmt < sLAmt
                 
            switch (valType)
            {
                
                    
                case E_ParameterValueType.Int: // Int share with Float all Function Type except In(InSet) operations
                case E_ParameterValueType.Float:
                    {
                        #region VALUETYPE_INTEGER_AND_FLOAT
                        switch (ruleFT)
                        {
                            case E_FunctionT.In:
                                {
                                    if( valType == E_ParameterValueType.Float )
                                        throw CBaseException.CreateGeneric( "Expecting Int value type (enum) for function Type IN. Float is not allowed.");

                                    if (constraintFT != E_FunctionT.In)
                                        throw CBaseException.CreateGeneric( "Both params must have In function type");

                                    return ConflictDetector.AreIntersectingSets(ruleValues, constraintValues)
                                        || ConflictDetector.AreIntersectingSets(ruleParam.Exceptions, constraintParam.Exceptions); // Only need to test exceptions for IN operation.
                                }

                             case E_FunctionT.Between:
                                {
                                    switch (constraintFT) // value:float, rule:between, cconstraint:see below
                                    {
                                        case E_FunctionT.Between:
                                            return ConflictDetector.AreIntersectingClosedRanges(E_CompareValueType.Float, ruleValues, constraintValues);
                                        case E_FunctionT.IsGreaterThan:
                                            return ConflictDetector.AreIntersectingCloseAndGreaterRange(E_CompareValueType.Float, ruleValues, constraintValues);
                                        case E_FunctionT.IsLessThan:
                                            return ConflictDetector.AreIntersectingCloseAndLesserRange(E_CompareValueType.Float, ruleValues, constraintValues);
                                        case E_FunctionT.IsGreaterThanOrEqualTo:
                                            return ConflictDetector.AreIntersectingCloseAndGreaterEqualRange(E_CompareValueType.Float, ruleValues, constraintValues);
                                        case E_FunctionT.IsLessThanOrEqualTo:
                                            return ConflictDetector.AreIntersectingCloseAndLesserEqualRange(E_CompareValueType.Float, ruleValues, constraintValues);
                                        case E_FunctionT.IsNonzeroValue: // vals2 must contain either "True" or "False"
                                            return ConflictDetector.AreIntersectingCloseAndIsNonzero(ruleValues, constraintValues);
                                        case E_FunctionT.IsAnyValue: // vals2 must contain either "True" or "False";
                                            return ConflictDetector.AreIntersectingCloseAndAnyValueAcceptance(E_CompareValueType.Float, ruleValues, constraintValues);
                                        default:
                                            throw CBaseException.CreateUnexpectedEnumException( constraintFT );

                                    }
                                }
                            case E_FunctionT.IsGreaterThan:
                                {
                                    switch (constraintFT) // v:float, r:greater, c:see below
                                    {
                                        case E_FunctionT.Between:
                                            return ConflictDetector.AreIntersectingCloseAndGreaterRange(E_CompareValueType.Float, constraintValues, ruleValues);
                                        case E_FunctionT.IsGreaterThan:
                                            return ConflictDetector.AreIntersectingGreaterAndGreaterRange(E_CompareValueType.Float, ruleValues, constraintValues);
                                        case E_FunctionT.IsLessThan:
                                            return ConflictDetector.AreIntersectingGreaterAndLesserRange(E_CompareValueType.Float, ruleValues, constraintValues);
                                        case E_FunctionT.IsGreaterThanOrEqualTo:
                                            return ConflictDetector.AreIntersectingGreaterAndGreaterEqualRange(E_CompareValueType.Float, ruleValues, constraintValues);
                                        case E_FunctionT.IsLessThanOrEqualTo:
                                            return ConflictDetector.AreIntersectingGreaterAndLesserEqualRange(E_CompareValueType.Float, ruleValues, constraintValues);
                                        case E_FunctionT.IsNonzeroValue:
                                            return ConflictDetector.AreIntersectingGreaterAndIsNonzero(ruleValues, constraintValues);
                                        case E_FunctionT.IsAnyValue:
                                            return ConflictDetector.AreIntersectingGreaterAndAnyValueAcceptance(E_CompareValueType.Float, ruleValues, constraintValues);
                                        default:
                                            throw CBaseException.CreateUnexpectedEnumException(constraintFT);
                                    }
                                }
 
                            case E_FunctionT.IsLessThan:
                                {
                                    switch (constraintFT) // v:float, r:lesser, c:see below
                                    {
                                        case E_FunctionT.Between:
                                            return ConflictDetector.AreIntersectingCloseAndLesserRange(E_CompareValueType.Float, constraintValues, ruleValues);
                                        case E_FunctionT.IsGreaterThan:
                                            return ConflictDetector.AreIntersectingGreaterAndLesserRange(E_CompareValueType.Float, constraintValues, ruleValues);
                                        case E_FunctionT.IsLessThan:
                                            return ConflictDetector.AreIntersectingLesserAndLesserRange(E_CompareValueType.Float, ruleValues, constraintValues);
                                        case E_FunctionT.IsGreaterThanOrEqualTo:
                                            return ConflictDetector.AreIntersectingGreaterEqualAndLesserRange(E_CompareValueType.Float, constraintValues, ruleValues);
                                        case E_FunctionT.IsLessThanOrEqualTo:
                                            return ConflictDetector.AreIntersectingLesserAndLessEqualRange(E_CompareValueType.Float, ruleValues, constraintValues);
                                        case E_FunctionT.IsNonzeroValue:
                                            return ConflictDetector.AreIntersectingLesserAndIsNonzero(ruleValues, constraintValues);
                                        case E_FunctionT.IsAnyValue:
                                            return ConflictDetector.AreIntersectingLesserAndAnyValueAcceptance(E_CompareValueType.Float, ruleValues, constraintValues);
                                        default:
                                            throw CBaseException.CreateUnexpectedEnumException(constraintFT);
                                    }
                                }
                            case E_FunctionT.IsGreaterThanOrEqualTo:
                                {
                                    switch (constraintFT) // v:float, r:greaterEqual, c:see below
                                    {
                                        case E_FunctionT.Between:
                                            return ConflictDetector.AreIntersectingCloseAndGreaterEqualRange(E_CompareValueType.Float, constraintValues, ruleValues);
                                        case E_FunctionT.IsGreaterThan:
                                            return ConflictDetector.AreIntersectingGreaterAndGreaterEqualRange(E_CompareValueType.Float, constraintValues, ruleValues);
                                        case E_FunctionT.IsLessThan:
                                            return ConflictDetector.AreIntersectingGreaterEqualAndLesserRange(E_CompareValueType.Float, ruleValues, constraintValues);
                                        case E_FunctionT.IsGreaterThanOrEqualTo:
                                            return ConflictDetector.AreIntersectingGreaterEqualAndGreaterEqualRange(E_CompareValueType.Float, ruleValues, constraintValues);
                                        case E_FunctionT.IsLessThanOrEqualTo:
                                            return ConflictDetector.AreIntersectingGreaterEqualAndLesserEqualRange(E_CompareValueType.Float, ruleValues, constraintValues);
                                        case E_FunctionT.IsNonzeroValue:
                                            return ConflictDetector.AreIntersectingGreaterEqualAndIsNonzero(ruleValues, constraintValues);
                                        case E_FunctionT.IsAnyValue:
                                            return ConflictDetector.AreIntersectingGreaterEqualAndAnyValueAcceptance(E_CompareValueType.Float, ruleValues, constraintValues);
                                        default:
                                            throw CBaseException.CreateUnexpectedEnumException(constraintFT);
                                    }
                                }
                            case E_FunctionT.IsLessThanOrEqualTo:
                                {
                                    switch (constraintFT) // v:float, r:lessEqual, c:see below
                                    {
                                        case E_FunctionT.Between:
                                            return ConflictDetector.AreIntersectingCloseAndLesserEqualRange(E_CompareValueType.Float, constraintValues, ruleValues);
                                        case E_FunctionT.IsGreaterThan:
                                            return ConflictDetector.AreIntersectingGreaterAndLesserEqualRange(E_CompareValueType.Float, constraintValues, ruleValues);
                                        case E_FunctionT.IsLessThan:
                                            return ConflictDetector.AreIntersectingLesserAndLessEqualRange(E_CompareValueType.Float, constraintValues, ruleValues);
                                        case E_FunctionT.IsGreaterThanOrEqualTo:
                                            return ConflictDetector.AreIntersectingGreaterEqualAndLesserEqualRange(E_CompareValueType.Float, constraintValues, ruleValues);
                                        case E_FunctionT.IsLessThanOrEqualTo:
                                            return ConflictDetector.AreIntersectingLesserEqualAndLesserEqualRange(E_CompareValueType.Float, ruleValues, constraintValues);
                                        case E_FunctionT.IsNonzeroValue:
                                            return ConflictDetector.AreIntersectingLesserEqualAndIsNonzero(ruleValues, constraintValues);
                                        case E_FunctionT.IsAnyValue:
                                            return ConflictDetector.AreIntersectingLesserEqualAndAnyValueAcceptance(E_CompareValueType.Float, ruleValues, constraintValues);
                                        default:
                                            throw CBaseException.CreateUnexpectedEnumException(constraintFT);
                                    }
                                }

                            case E_FunctionT.IsNonzeroValue:
                                {
                                    switch (constraintFT) // v:float, r:nonzero, c:see below
                                    {
                                        case E_FunctionT.Between:
                                            return ConflictDetector.AreIntersectingCloseAndIsNonzero(constraintValues, ruleValues);
                                        case E_FunctionT.IsGreaterThan:
                                            return ConflictDetector.AreIntersectingGreaterEqualAndIsNonzero(constraintValues, ruleValues);
                                        case E_FunctionT.IsLessThan:
                                            return ConflictDetector.AreIntersectingLesserAndIsNonzero(constraintValues, ruleValues);
                                        case E_FunctionT.IsGreaterThanOrEqualTo:
                                            return ConflictDetector.AreIntersectingGreaterEqualAndIsNonzero(constraintValues, ruleValues);
                                        case E_FunctionT.IsLessThanOrEqualTo:
                                            return ConflictDetector.AreIntersectingLesserEqualAndIsNonzero(constraintValues, ruleValues);
                                        case E_FunctionT.IsNonzeroValue:
                                            return ConflictDetector.AreIntersectingIsNonzeroAndIsNonzero(ruleValues, constraintValues);
                                        case E_FunctionT.IsAnyValue:
                                            return ConflictDetector.AreIntersectingIsNonzeroAndAnyValueAcceptance(ruleValues, constraintValues);
                                        default:
                                            throw CBaseException.CreateUnexpectedEnumException(constraintFT);
                                    }
                                }
                            case E_FunctionT.IsAnyValue: // v:float, r:anyvalue, c:see below
                                {
                                    switch (constraintFT)
                                    {
                                        case E_FunctionT.Between:
                                            return ConflictDetector.AreIntersectingCloseAndAnyValueAcceptance(E_CompareValueType.Float, constraintValues, ruleValues);
                                        case E_FunctionT.IsGreaterThan:
                                            return ConflictDetector.AreIntersectingGreaterAndAnyValueAcceptance(E_CompareValueType.Float, constraintValues, ruleValues);
                                        case E_FunctionT.IsLessThan:
                                            return ConflictDetector.AreIntersectingLesserAndAnyValueAcceptance(E_CompareValueType.Float, constraintValues, ruleValues);
                                        case E_FunctionT.IsGreaterThanOrEqualTo:
                                            return ConflictDetector.AreIntersectingGreaterEqualAndAnyValueAcceptance(E_CompareValueType.Float, constraintValues, ruleValues);
                                        case E_FunctionT.IsLessThanOrEqualTo:
                                            return ConflictDetector.AreIntersectingLesserEqualAndAnyValueAcceptance(E_CompareValueType.Float, constraintValues, ruleValues);
                                        case E_FunctionT.IsNonzeroValue:
                                            return ConflictDetector.AreIntersectingIsNonzeroAndAnyValueAcceptance(constraintValues, ruleValues);
                                        case E_FunctionT.IsAnyValue:
                                            return ConflictDetector.AreIntersectingAnyValueAcceptanceAndAnyValueAcceptance(E_CompareValueType.Float, ruleValues, constraintValues);
                                        default:
                                            throw CBaseException.CreateUnexpectedEnumException(constraintFT);
                                    }
                                }
                            default:
                                throw CBaseException.CreateUnexpectedEnumException(ruleFT);

                        }
                        #endregion // #region VALUETYPE_FLOAT
                    }
                case E_ParameterValueType.String:
                    {
                        #region VALUETYPE_STRING
                        switch (ruleFT)
                        {
                            case E_FunctionT.In:
                                {
                                    if (constraintFT == E_FunctionT.In)
                                        return ConflictDetector.AreIntersectingSets(ruleValues, constraintValues);

                                    throw CBaseException.CreateUnexpectedEnumException(constraintFT);
                                }
                            case E_FunctionT.IsNonblankStr:
                                {
                                    switch (constraintFT) // v:string, r:IsNonblankStr, c:see below
                                    {
                                        case E_FunctionT.In:
                                            return ConflictDetector.DoesContainBlankString(constraintValues); 
                                        case E_FunctionT.IsNonblankStr:
                                            return ConflictDetector.AreIntersectingIsNonblankStrAndIsNonblankStr(ruleValues,constraintValues);
                                        default:
                                            throw CBaseException.CreateUnexpectedEnumException(constraintFT);
                                    }
                                }
                            default:
                                throw CBaseException.CreateUnexpectedEnumException(ruleFT);
                        }
                        #endregion //VALUETYPE_STRING
                    }

                case E_ParameterValueType.DateTime:
                    {
                        #region VALUETYPE_DATETIME
                        switch (ruleFT)
                        {
                            case E_FunctionT.Between:
                                {
                                    switch (constraintFT)
                                    {
                                        case E_FunctionT.Between:
                                            return ConflictDetector.AreIntersectingClosedRanges(E_CompareValueType.DateTime, ruleValues, constraintValues);
                                        case E_FunctionT.IsGreaterThan:
                                            return ConflictDetector.AreIntersectingCloseAndGreaterRange(E_CompareValueType.DateTime, ruleValues, constraintValues); ;
                                        case E_FunctionT.IsLessThan:
                                            return ConflictDetector.AreIntersectingCloseAndLesserRange(E_CompareValueType.DateTime, ruleValues, constraintValues);
                                        case E_FunctionT.IsGreaterThanOrEqualTo:
                                            return ConflictDetector.AreIntersectingCloseAndGreaterEqualRange(E_CompareValueType.DateTime, ruleValues, constraintValues);
                                        case E_FunctionT.IsLessThanOrEqualTo:
                                            return ConflictDetector.AreIntersectingCloseAndLesserEqualRange(E_CompareValueType.DateTime, ruleValues, constraintValues);                                            
                                        case E_FunctionT.IsAnyValue:
                                            return ConflictDetector.AreIntersectingCloseAndAnyValueAcceptance(E_CompareValueType.DateTime, ruleValues, constraintValues);
                                        default:
                                            throw CBaseException.CreateUnexpectedEnumException(constraintFT);
                                    }
                                }
                            case E_FunctionT.IsGreaterThan:
                                {
                                    switch (constraintFT)
                                    {
                                        case E_FunctionT.Between:
                                            return ConflictDetector.AreIntersectingCloseAndGreaterRange(E_CompareValueType.DateTime, constraintValues, ruleValues);
                                        case E_FunctionT.IsGreaterThan:
                                            return ConflictDetector.AreIntersectingGreaterAndGreaterRange(E_CompareValueType.DateTime, ruleValues, constraintValues);
                                        case E_FunctionT.IsLessThan:
                                            return ConflictDetector.AreIntersectingGreaterAndLesserRange(E_CompareValueType.DateTime, ruleValues, constraintValues);
                                        case E_FunctionT.IsGreaterThanOrEqualTo:
                                            return ConflictDetector.AreIntersectingGreaterAndGreaterEqualRange(E_CompareValueType.DateTime, ruleValues, constraintValues);
                                        case E_FunctionT.IsLessThanOrEqualTo:
                                            return ConflictDetector.AreIntersectingGreaterAndLesserEqualRange(E_CompareValueType.DateTime, ruleValues, constraintValues);
                                        case E_FunctionT.IsAnyValue:
                                            return ConflictDetector.AreIntersectingGreaterAndAnyValueAcceptance(E_CompareValueType.DateTime, ruleValues, constraintValues);
                                        default:
                                            throw CBaseException.CreateUnexpectedEnumException(constraintFT);
                                    }
                                }

                            case E_FunctionT.IsLessThan:
                                {
                                    switch (constraintFT)
                                    {
                                        case E_FunctionT.Between:
                                            return ConflictDetector.AreIntersectingCloseAndLesserRange(E_CompareValueType.DateTime, constraintValues, ruleValues);
                                        case E_FunctionT.IsGreaterThan:
                                            return ConflictDetector.AreIntersectingGreaterAndLesserRange(E_CompareValueType.DateTime, constraintValues, ruleValues);
                                        case E_FunctionT.IsLessThan:
                                            return ConflictDetector.AreIntersectingLesserAndLesserRange(E_CompareValueType.DateTime, ruleValues, constraintValues);
                                        case E_FunctionT.IsGreaterThanOrEqualTo:
                                            return ConflictDetector.AreIntersectingGreaterEqualAndLesserRange(E_CompareValueType.DateTime,constraintValues, ruleValues);
                                        case E_FunctionT.IsLessThanOrEqualTo:
                                            return ConflictDetector.AreIntersectingLesserAndLessEqualRange(E_CompareValueType.DateTime, ruleValues, constraintValues);
                                        case E_FunctionT.IsAnyValue:
                                            return ConflictDetector.AreIntersectingLesserAndAnyValueAcceptance(E_CompareValueType.DateTime, ruleValues, constraintValues);
                                        default:
                                            throw CBaseException.CreateUnexpectedEnumException(constraintFT);
                                    }
                                }
                            case E_FunctionT.IsGreaterThanOrEqualTo:
                                {
                                    switch (constraintFT)
                                    {
                                        case E_FunctionT.Between:
                                            return ConflictDetector.AreIntersectingCloseAndGreaterEqualRange(E_CompareValueType.DateTime, constraintValues, ruleValues);
                                        case E_FunctionT.IsGreaterThan:
                                            return ConflictDetector.AreIntersectingGreaterAndGreaterEqualRange(E_CompareValueType.DateTime, constraintValues, ruleValues);
                                        case E_FunctionT.IsLessThan:
                                            return ConflictDetector.AreIntersectingGreaterEqualAndLesserRange(E_CompareValueType.DateTime, ruleValues, constraintValues);
                                        case E_FunctionT.IsGreaterThanOrEqualTo:
                                            return ConflictDetector.AreIntersectingGreaterEqualAndGreaterEqualRange(E_CompareValueType.DateTime, ruleValues, constraintValues);
                                        case E_FunctionT.IsLessThanOrEqualTo:
                                            return ConflictDetector.AreIntersectingGreaterEqualAndLesserEqualRange(E_CompareValueType.DateTime, ruleValues, constraintValues);
                                        case E_FunctionT.IsAnyValue:
                                            return ConflictDetector.AreIntersectingGreaterEqualAndAnyValueAcceptance(E_CompareValueType.DateTime, ruleValues, constraintValues);
                                        default:
                                            throw CBaseException.CreateUnexpectedEnumException(constraintFT);
                                    }
                                }
                            case E_FunctionT.IsLessThanOrEqualTo:
                                {
                                    switch (constraintFT)
                                    {
                                        case E_FunctionT.Between:
                                            return ConflictDetector.AreIntersectingCloseAndLesserEqualRange(E_CompareValueType.DateTime, constraintValues, ruleValues);
                                        case E_FunctionT.IsGreaterThan:
                                            return ConflictDetector.AreIntersectingGreaterAndLesserEqualRange(E_CompareValueType.DateTime, constraintValues, ruleValues);
                                        case E_FunctionT.IsLessThan:
                                            return ConflictDetector.AreIntersectingLesserAndLessEqualRange(E_CompareValueType.DateTime, constraintValues, ruleValues);
                                        case E_FunctionT.IsGreaterThanOrEqualTo:
                                            return ConflictDetector.AreIntersectingGreaterEqualAndLesserEqualRange(E_CompareValueType.DateTime, constraintValues, ruleValues);
                                        case E_FunctionT.IsLessThanOrEqualTo:
                                            return ConflictDetector.AreIntersectingLesserEqualAndLesserEqualRange(E_CompareValueType.DateTime, ruleValues, constraintValues);
                                        case E_FunctionT.IsAnyValue:
                                            return ConflictDetector.AreIntersectingLesserEqualAndAnyValueAcceptance(E_CompareValueType.DateTime, ruleValues, constraintValues);
                                        default:
                                            throw CBaseException.CreateUnexpectedEnumException(constraintFT);
                                    }
                                }
                            case E_FunctionT.IsAnyValue:
                                {
                                    switch (constraintFT)
                                    {
                                        case E_FunctionT.Between:
                                            return ConflictDetector.AreIntersectingCloseAndAnyValueAcceptance(E_CompareValueType.DateTime, constraintValues, ruleValues);
                                        case E_FunctionT.IsGreaterThan:
                                            return ConflictDetector.AreIntersectingGreaterAndAnyValueAcceptance(E_CompareValueType.DateTime, constraintValues, ruleValues);
                                        case E_FunctionT.IsLessThan:
                                            return ConflictDetector.AreIntersectingLesserAndAnyValueAcceptance(E_CompareValueType.DateTime, constraintValues, ruleValues);
                                        case E_FunctionT.IsGreaterThanOrEqualTo:
                                            return ConflictDetector.AreIntersectingGreaterEqualAndAnyValueAcceptance(E_CompareValueType.DateTime, constraintValues, ruleValues);
                                        case E_FunctionT.IsLessThanOrEqualTo:
                                            return ConflictDetector.AreIntersectingLesserEqualAndAnyValueAcceptance(E_CompareValueType.DateTime, constraintValues, ruleValues);
                                        case E_FunctionT.IsAnyValue:
                                            return ConflictDetector.AreIntersectingAnyValueAcceptanceAndAnyValueAcceptance(E_CompareValueType.DateTime, ruleValues, constraintValues);
                                        default:
                                            throw CBaseException.CreateUnexpectedEnumException(constraintFT);
                                    }
                                }
                            default:
                                throw CBaseException.CreateUnexpectedEnumException(ruleFT);                               
                        }

                        #endregion // VALUETYPE_DATETIME
                    }
                case E_ParameterValueType.Bool:
                case E_ParameterValueType.CustomVariable: // this one is also Bool type
                    {
                        #region VALUETYPE_BOOL_&_CUSTOMVARIABLE
                        switch (ruleFT)
                        {
                            case E_FunctionT.Equal:
                                if( constraintFT != E_FunctionT.Equal )
                                    throw CBaseException.CreateUnexpectedEnumException(constraintFT);
                                return ConflictDetector.AreIntersectingSets(ruleValues, constraintValues);
                            default:
                                throw CBaseException.CreateUnexpectedEnumException(ruleFT);
                        }
                        #endregion // VALUETYPE_BOOL_&_CUSTOMVARIABLE
                    }
                default:
                    throw CBaseException.CreateUnexpectedEnumException(valType);
                    
            }
    }
    

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rules"></param>
        /// <param name="constraint"></param>
        /// <returns>Conditions that are in conflict with the input constraint</returns>
        public List<Condition> DetectConflictConditions(ConditionSet rules, Constraint constraint, CustomVariableSet cvSet)
        {

            List<Condition> resultSet = new List<Condition>();

            foreach (Condition rule in rules)
            {
                if (!AreIntersectingOps(rule.SysOpSet, constraint.SysOpSet))
                    continue;

                if (AreIntersectedParamSets(rule.ParameterSet, constraint.ParameterSet))
                    resultSet.Add(rule);
                    
            }
            //DetectDuplicateConditions(rules, true); 

            return resultSet;

        }

 
        /// <summary>
        /// 
        /// </summary>
        /// <param name="rule"></param>
        /// <param name="constraints"></param>
        /// <returns>Constraints that are in conflict with the input rule</returns>
        public List<Constraint> DetectConflictConstraints(Condition rule, ConstraintSet constraints, CustomVariableSet cvSet )
        {
            
            List<Constraint> resultSet = new List<Constraint>();

            foreach (Constraint constraint in constraints)
            {

                if (!AreIntersectingOps(rule.SysOpSet, constraint.SysOpSet))
                    continue;

                if (AreIntersectedParamSets(rule.ParameterSet, constraint.ParameterSet))
                    resultSet.Add(constraint);

            }
            return resultSet;

        }

        public bool HasConflict( ConditionSet rules, ConstraintSet constraints)
        {
            foreach (Constraint constraint in constraints)
            {
                if (DetectConflictConditions(rules, constraint, null).Count > 0)
                    return true;
            }
            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rules"></param>
        /// <returns>List of list of conditions. The first condition in each row is the one being duplicated by the subsequent ones</returns>
        public List<List<Condition>> DetectDuplicateConditions(ConditionSet rules, bool outPutToPb )
        {
            #region DETECT_IDENTICALS
            List<List<Condition>> identicals = new List<List<Condition>>();

            foreach (Condition rule1 in rules)
            {
                List<Condition> row = new List<Condition>();
                foreach (Condition rule2 in rules)
                {
                    if (rule1.Id == rule2.Id)
                        continue;
                    if (AreIdenticalParams(rule1.ParameterSet, rule2.ParameterSet))
                    {
                        if (row.Count == 0)
                            row.Add(rule1);
                        row.Add(rule2);
                    }
                }
                if (row.Count > 0)
                    identicals.Add(row);
            }

            if (outPutToPb)
            {
                StringBuilder sb = new StringBuilder();

                sb.Append(string.Format("{0} conditions with identical Parameter Sets found:", identicals.Count));

                for (int i = 0; i < identicals.Count; ++i)
                {
                    List<Condition> row = identicals[i];
                    sb.Append("\r\n");
                    sb.Append(i);
                    sb.Append(": ");
                    foreach (Condition  condition in row)
                    {
                        sb.Append(condition.Id);
                        sb.Append(", ");
                    }
                }
                LogManager.Info(sb.ToString());
            }
            #endregion // DETECT_IDENTICALS


            return identicals;
        }

        static private bool AreIdenticalParams(ParameterSet rule1Params, ParameterSet rule2Params )
        {

            if (rule1Params.ParameterCount != rule2Params.ParameterCount)
                return false;

            foreach (Parameter p1 in rule1Params )
            {
                Parameter p2 = rule2Params.GetParameter(p1.Name);

                if (p2 == null) 
                    return false; // We are looking for exact match
                                
                if (p1.FunctionT != p2.FunctionT )
                    return false;

                if (p1.ValueType != p2.ValueType)
                    return false;

                if (p1.Values.Count != p2.Values.Count)
                    return false;

                if (p1.Exceptions.Count != p2.Exceptions.Count)
                    return false;

                foreach (string val in p1.Values)
                {
                    if (!p2.Values.Contains(val, StringComparer.OrdinalIgnoreCase))
                        return false;
                }

                foreach (string ex in p1.Exceptions)
                {
                    if (!p2.Exceptions.Contains(ex, StringComparer.OrdinalIgnoreCase))
                        return false;
                }
            }
            return true; // if all parameters are the same
        }
    }

  

}
