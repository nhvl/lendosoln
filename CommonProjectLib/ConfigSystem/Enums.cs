﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConfigSystem
{
    /// <summary>
    /// All available operations/functions available in custom variables/conditions/constraints.
    /// Some of them are not applicable to some variables, that will be up to the implementing projects to enforce.
    /// The engine will know what to do with which. Some of these do not make sense in certain sitatuons, for example, 
    /// using GreaterThanVar with a CustomVariable since CustomVariable is true/false.
    /// </summary>
    public enum E_FunctionT
    {
        In,
        Between,
        Equal,
        IsNonblankStr,
        IsGreaterThan,
        IsGreaterThanVar,
        IsLessThan,
        IsLessThanVar,
        IsGreaterThanOrEqualTo,
        IsLessThanOrEqualTo,
        EqualVar,
        IsGreaterThanOrEqualToVar,
        IsLessThanOrEqualToVar,
        IsNonzeroValue,
        IsAnyValue

    }
}
