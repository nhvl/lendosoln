﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using CommonProjectLib.Common;

namespace ConfigSystem
{
    public class CustomVariable
    {
        public enum E_Scope { System, Lender }

        protected ParameterSet m_paramSet = null;
        private string m_name = "";
        private Guid m_id = Guid.Empty;
        private E_Scope m_scope;
        private string m_notes = "";

        public string Name
        {
            get { return m_name; }
            private set { m_name = value; }
        }

        public string Notes
        {
            get { return m_notes; }
            set { m_notes = value; }
        }

        public Guid Id
        {
            get { return m_id; }
        }

        public E_Scope Scope
        {
            get { return m_scope; }
            set { m_scope = value; }
        }

        public ParameterSet ParameterSet
        {
            get { return m_paramSet; }
            set { m_paramSet = value; }
        }

        public IEnumerable<Parameter> Parameters
        {
            get { return m_paramSet.ToList(); }
        }

        public CustomVariable(XElement customVariableRoot)
        {
            Parse(customVariableRoot);
        }

        public CustomVariable(XElement customVariableRoot, Guid newId)
        {
            Parse(customVariableRoot);
            m_id = newId;
        }

        public CustomVariable(string name, E_Scope scope)
        {
            m_paramSet = new ParameterSet();
            m_id = Guid.NewGuid();
            m_scope = scope;
            m_name = name;
        }

        private void Parse(XElement customVariableRoot)
        {
            m_id = new Guid(customVariableRoot.Attribute("id").Value.Trim());
            m_name = customVariableRoot.Attribute("name").Value;
            var xmlScope = customVariableRoot.Attribute("scope");
            if (xmlScope != null)
            {
                m_scope = (E_Scope) Enum.Parse(typeof(E_Scope), xmlScope.Value, true);
            }
            else
            {
                m_scope = E_Scope.Lender;
            }
            XElement parameterSetElement = customVariableRoot.Element("ParameterSet");
            m_paramSet = new ParameterSet(parameterSetElement);

            XElement notesElement = customVariableRoot.Element("Notes");
            if (notesElement != null)
            {
                m_notes = notesElement.Value.Trim();
            }
        }

        /// <summary>
        /// Generates the XML representing this object.
        /// </summary>
        /// <returns>The XElement that is the root of this object's XML tree</returns>
        public XElement GetXml()
        {
            XElement root = new XElement("CustomVariable");
            root.Add(new XAttribute("id", m_id.ToString()));
            root.Add(new XAttribute("name", Name));
            root.Add(new XAttribute("scope", Scope.ToString()));
            root.Add(new XElement(m_paramSet.GetXml()));
            if (!string.IsNullOrEmpty(m_notes.Trim()))
            {
                root.Add(new XElement("Notes", new XCData(m_notes)));
            }

            return root;
        }
    }
}
