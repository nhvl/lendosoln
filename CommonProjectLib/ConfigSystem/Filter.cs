﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace ConfigSystem
{
    public class Filter
    {
        public const string ROLESALL = "RolesAll";
        public const string OPERATIONSALL = "OperationsAll";
        public const string TYPE_ROLE = "role";
        public const string TYPE_OPERATION = "operation";
        private string m_name = "";
        private string m_filterType = ""; // roles or operations. should be an enum
        private string m_value = ""; // usually true or false, but may be more complicated
        private E_FunctionT m_function; // usually ==, but may be other in conjunction with value

        public string Name
        {
            get { return m_name; }
        }

        public string FilterType
        {
            get { return m_filterType; }
        }

        public string Value
        {
            get { return m_value; }
        }

        public E_FunctionT Function
        {
            get { return m_function; }
        }

        public Filter(XElement filterRoot, string newName)
        {
            Parse(filterRoot);
            m_name = newName;
        }

        public Filter(XElement filterRoot)
        {
            Parse(filterRoot);
        }

        /// <summary>
        /// Creates a filter that filters out the given name.
        /// </summary>
        public Filter(string name, string filterType, string value, E_FunctionT function)
        {
            m_name = name;
            m_filterType = filterType;
            m_value = value;
            m_function = function;
        }

        public Filter(string name, string filterType, string value)
        {
            m_name = name;
            m_filterType = filterType;
            m_value = value;
            m_function = E_FunctionT.Equal;
        }

        public Filter(string name, string filterType)
        {
            m_name = name;
            m_filterType = filterType;
            m_value = "true";
            m_function = E_FunctionT.Equal;
        }

        /// <summary>
        /// Interpret the XML from filterRoot as a filter
        /// </summary>
        private void Parse(XElement filterRoot)
        {
            m_name = filterRoot.Attribute("name").Value;
            m_filterType = filterRoot.Attribute("filterType").Value;
            m_function = (E_FunctionT)Enum.Parse(typeof(E_FunctionT), filterRoot.Attribute("function").Value, true);
            m_value = filterRoot.Attribute("value").Value;
        }

        /// <summary>
        /// Generates the XML representing this object.
        /// </summary>
        /// <returns>The XElement that is the root of this object's XML tree</returns>
        public XElement GetXml()
        {
            XElement root = new XElement("Filter");
            root.Add(new XAttribute("name", m_name));
            root.Add(new XAttribute("filterType", m_filterType));
            root.Add(new XAttribute("function", m_function));
            root.Add(new XAttribute("value", m_value));

            return root;
        }
    }
}
