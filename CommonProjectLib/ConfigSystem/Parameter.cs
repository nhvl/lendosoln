﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using CommonProjectLib.Logging;
using System.Collections;
using CommonProjectLib.Common;

namespace ConfigSystem
{
    // Any changes to this enum need to also be made in SystemConfig.xsd in the VarType simple group.
    public enum E_ParameterValueType
    {
        Int,
        Float,
        String,
        DateTime,
        Bool,
        CustomVariable
    };
    public class Parameter
    {
        private string                      m_name = "";
        private E_FunctionT                 m_functionT;
        private E_ParameterValueType        m_valueType;
        private Dictionary<string, string>  m_valueDescriptions;
        private Dictionary<string, string>  m_exceptionDescriptions;
        private bool                        m_isSensitive = false;
        private bool                        m_isHidden = false;
        private CustomVariable.E_Scope?     m_scope;

        public string Name
        {
            get { return m_name; }
            set { m_name = value; }
        }
        public string ExecutingEngineName
        {
            get
            {
                if (IsLookAtNewData)
                {
                    return "m:" + Name;
                }
                else
                {
                    return Name;
                }
            }
        }
        public E_FunctionT FunctionT
        {
            get { return m_functionT; }
            set { m_functionT = value; }
        }

        public ICollection<string> Values
        {
            get { return m_valueDescriptions.Keys; }
        }

        public ICollection<string> Exceptions
        {
            get { return m_exceptionDescriptions.Keys; }
        }

        public E_ParameterValueType ValueType
        {
            get { return m_valueType; }
        }

        public bool IsSensitive
        {
            get { return m_isSensitive; }
            set { m_isSensitive = value; }
        }
        public bool IsLookAtNewData { get; set; } // 3/21/2014 dd - Add to support OPM 144112
        public bool IsHidden
        {
            get { return m_isHidden; }
            set { m_isHidden = value; }
        }

        public CustomVariable.E_Scope Scope
        {
            get
            {
                if (m_scope.HasValue)
                {
                    return m_scope.Value;
                }
                else
                {
                    return CustomVariable.E_Scope.Lender; // Would really prefer to return null
                }
            }
            set { m_scope = value; }
        }

        public bool RequiredForUserFriendlyChecklistInclusion { get; set; }

        public void RenameValueDesc(string oldValue, string newValue)
        {
            List<string> matchedKeys = new List<string>();
            
            foreach (string key in m_valueDescriptions.Keys)
            {
                if (m_valueDescriptions[key].ToLower().Equals(oldValue.ToLower()))
                {
                    matchedKeys.Add(key);
                }
            }

            foreach (string matchedKey in matchedKeys)
            {
                m_valueDescriptions[matchedKey] = newValue;
            }
        }

        public void RenameValue(string oldValue, string newValue)
        {
            if (m_valueDescriptions.ContainsKey(oldValue))
            {
                string desc = m_valueDescriptions[oldValue];
                m_valueDescriptions.Remove(oldValue);
                m_valueDescriptions.Add(newValue, desc);
            }
        }

        public void ClearValues()
        {
            m_valueDescriptions.Clear();
        }

        /// <summary>
        /// Returns a Dictionary of <string, string> where the first string is the value, and the second is the value's description
        /// </summary>
        public IDictionary<string, string> ValueDesc
        {
            get { return m_valueDescriptions; }
        }

        public IDictionary<string, string> ExceptionDesc
        {
            get { return m_exceptionDescriptions; }
        }

        public Parameter(XElement parameterRoot)
        {
            m_valueDescriptions = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
            m_exceptionDescriptions = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
            Parse(parameterRoot);
        }

        public void AddValue(string value, string valueDesc)
        {
            if (value == null)
            {
                return;
            }

            if (valueDesc == null)
            {
                valueDesc = "";
            }

            m_valueDescriptions.Add(value, valueDesc);
        }

        public void AddExcption(string exception, string exceptionDesc)
        {
            if (exception == null)
            {
                return;
            }

            if (exceptionDesc == null)
            {
                exceptionDesc = "";
            }

            m_exceptionDescriptions.Add(exception, exceptionDesc);
        }

        public Parameter(string name, E_FunctionT functionT, E_ParameterValueType valueType, bool isHidden, bool isSensitive)
        {
            m_valueDescriptions = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
            m_exceptionDescriptions = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
            m_name = name;
            m_functionT = functionT;
            m_valueType = valueType;
            m_isHidden = isHidden;
            m_isSensitive = isSensitive;

            IsLookAtNewData = false;
        }

        public Parameter(string name, E_FunctionT functionT, E_ParameterValueType valueType, bool isHidden, bool isSensitive, CustomVariable.E_Scope? scope)
            : this(name, functionT, valueType, isHidden, isSensitive)
        {
            m_scope = scope;
        }

        private void Parse(XElement parameterRoot)
        {
            m_name = parameterRoot.Attribute("name").Value;
            m_functionT = (E_FunctionT)Enum.Parse(typeof(E_FunctionT), parameterRoot.Attribute("function").Value, true);

            m_valueType = (E_ParameterValueType)Enum.Parse(typeof(E_ParameterValueType), parameterRoot.Attribute("type").Value);

            if (parameterRoot.Attribute("sensitive") != null)
            {
                m_isSensitive = bool.Parse(parameterRoot.Attribute("sensitive").Value);
            }

            if (parameterRoot.Attribute("hidden") != null)
            {
                m_isHidden = bool.Parse(parameterRoot.Attribute("hidden").Value);
            }

            if (parameterRoot.Attribute("IsLookAtNewData") != null)
            {
                IsLookAtNewData = bool.Parse(parameterRoot.Attribute("IsLookAtNewData").Value);
            }

            if (parameterRoot.Attribute("RequiredForUserFriendlyChecklistInclusion") != null)
            {
                this.RequiredForUserFriendlyChecklistInclusion = bool.Parse(parameterRoot.Attribute("RequiredForUserFriendlyChecklistInclusion").Value);
            }

            foreach (XElement element in parameterRoot.Elements("Value"))
            {
                string description = "";
                if (element.Attribute("desc") != null)
                {
                    description = element.Attribute("desc").Value;
                }

                try
                {
                    m_valueDescriptions.Add(element.Value, description);
                }
                catch (ArgumentException ae)
                {
                    throw new CBaseException("Cannot add " + element.Value + " to Parameter values because it already exists.  Parameter name = " + m_name, ae);
                }
            }

            foreach (XElement element in parameterRoot.Elements("Exception"))
            {
                string description = "";
                if (element.Attribute("desc") != null)
                {
                    description = element.Attribute("desc").Value;
                }

                try
                {
                    m_exceptionDescriptions.Add(element.Value, description);
                }
                catch (ArgumentException ae)
                {
                    throw new CBaseException("Cannot add " + element.Value + " to Parameter exceptions because it already exists.  Parameter name = " + m_name, ae);
                }
            }

            if (parameterRoot.Attribute("scope") != null && m_valueType == E_ParameterValueType.CustomVariable) // Custom variable scope
            {
                m_scope = (CustomVariable.E_Scope) Enum.Parse(typeof(CustomVariable.E_Scope), parameterRoot.Attribute("scope").Value, true);
            }
        }

        /// <summary>
        /// Generates the XML representing this object.
        /// </summary>
        /// <returns>The XElement that is the root of this object's XML tree</returns>
        public XElement GetXml()
        {
            XElement root = new XElement("Parameter");
            root.Add(new XAttribute("name", Name));

            if (IsHidden)
            {
                root.Add(new XAttribute("hidden", "true"));
            }
            if (IsSensitive)
            {
                root.Add(new XAttribute("sensitive", "true"));
            }
            if (IsLookAtNewData)
            {
                root.Add(new XAttribute("IsLookAtNewData", "true"));
            }
            if (this.RequiredForUserFriendlyChecklistInclusion)
            {
                root.Add(new XAttribute("RequiredForUserFriendlyChecklistInclusion", "true"));
            }

            root.Add(new XAttribute("function", FunctionT.ToString()));
            root.Add(new XAttribute("type", ValueType.ToString()));

            foreach (string val in m_valueDescriptions.Keys)
            {
                XElement value = new XElement("Value", val);
                if (m_valueDescriptions[val] != "")
                {
                    value.Add(new XAttribute("desc", m_valueDescriptions[val]));
                }

                root.Add(value);
            }

            foreach (string ex in m_exceptionDescriptions.Keys)
            {
                XElement exception = new XElement("Exception", ex);
                if (m_exceptionDescriptions[ex] != "")
                {
                    exception.Add(new XAttribute("desc", m_exceptionDescriptions[ex]));
                }

                root.Add(exception);
            }

            if (m_scope.HasValue && m_valueType == E_ParameterValueType.CustomVariable) // Custom variable scope
            {
                root.Add(new XAttribute("scope", m_scope.ToString()));
            }

            return root;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Name: " + Name + " ");
            sb.Append("Function: " + FunctionT + " ");
            sb.Append("IsSensitive: " + IsSensitive + " ");
            sb.Append("IsHidden: " + IsHidden + Environment.NewLine);
            sb.Append("Values: " + Environment.NewLine);
            foreach (string val in m_valueDescriptions.Keys)
            {
                sb.Append("\t" + val + " - " + m_valueDescriptions[val]);
            }

            sb.Append(Environment.NewLine);
            sb.Append("Exceptions: " + Environment.NewLine);
            foreach (string ex in m_exceptionDescriptions.Keys)
            {
                sb.Append("\t" + ex + " - " + m_exceptionDescriptions[ex]);
            }

            return sb.ToString();
        }
    }
}
