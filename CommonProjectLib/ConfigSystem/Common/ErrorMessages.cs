﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConfigSystem.Common
{
    public static class ErrorMessages
    {
        public static readonly string ReleaseChangedSinceLastLoad = "The system configuration changed since the last time you loaded it. Reload the configuration."; 
    }
}
