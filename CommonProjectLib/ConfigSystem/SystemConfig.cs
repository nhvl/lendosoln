﻿namespace ConfigSystem
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Linq;
    using System.Xml.Schema;

    using CommonProjectLib.Common;
    using LqbGrammar.DataTypes.PathDispatch;

    public enum E_TypeToRename
    {
        Value = 0,
        ValueDesc = 1
    }
    public sealed class SystemConfig
    {
        public const string fileDbKeySuffix = "WORKFLOW_CONFIG";

        private ConditionSet _conditions;
        private ConstraintSet _constraints;
        private RestraintSet _restraints;
        private CustomVariableSet _customVariables;

        private static XmlSchemaSet x_schemas;

        static SystemConfig()
        {
            x_schemas = new XmlSchemaSet();

            Assembly asm = Assembly.GetExecutingAssembly();
            using (XmlReader reader = XmlReader.Create(asm.GetManifestResourceStream("CommonProjectLib.ConfigSystem.SystemConfig.xsd")))
            {
                x_schemas.Add("", reader);
            }
        }
        public ConditionSet Conditions
        {
            get { return _conditions; }
        }

        public ConstraintSet Constraints
        {
            get { return _constraints; }
        }

        public RestraintSet Restraints
        {
            get { return _restraints; }
        }

        public CustomVariableSet CustomVariables
        {
            get { return _customVariables; }
        }

        public SystemConfig(string configXml)
        {
            ParseConfigFile(configXml);
        }

        public SystemConfig()
        {
            ParseConfigFile(null);
        }

        /// <summary>
        /// Checks for all constraints that fail each condition in the current condition set
        /// </summary>
        /// <returns>A dictionary whose key is the condition ID and the value is the set of
        /// constraint IDs that fail that condition.</returns>
        public Dictionary<Guid, List<Guid>> CheckForConflictsByConditions()
        {
            Dictionary<Guid, List<Guid>> failures = new Dictionary<Guid, List<Guid>>();
            
            ConflictDetector cd = new ConflictDetector();

            foreach (Condition c in _conditions)
            {
                List<Constraint> cSet = cd.DetectConflictConstraints(c, _constraints, _customVariables);

                List<Guid> failedConstraintIds = new List<Guid>();
                foreach (Constraint constraint in cSet)
                {
                    failedConstraintIds.Add(constraint.Id);
                }

                if (cSet.Count > 0)
                {
                    failures.Add(c.Id, failedConstraintIds);
                }
            }

            return failures;
        }

        /// <summary>
        /// Checks for all conditions that fail each constraint in the current constraint set
        /// </summary>
        /// <returns>A dictionary whose key is the constraint ID and the value is the set of
        /// condition IDs that fail that constraint.</returns>
        public Dictionary<Guid, List<Guid>> CheckForConflictsByConstraints()
        {
            Dictionary<Guid, List<Guid>> failures = new Dictionary<Guid, List<Guid>>();
            ConflictDetector cd = new ConflictDetector();

            foreach (Constraint c in _constraints)
            {
                List<Condition> cSet = cd.DetectConflictConditions(_conditions, c, _customVariables);
                
                List<Guid> failedConditionIds = new List<Guid>();
                foreach (Condition condition in cSet)
                {
                    failedConditionIds.Add(condition.Id);
                }

                if (cSet.Count > 0)
                {
                    failures.Add(c.Id, failedConditionIds);
                }
            }
            return failures;
        }

        public void RenameParameter(string oldVal, string newVal)
        {
            foreach (Condition condition in _conditions)
            {
                foreach (Parameter parameter in condition.ParameterSet)
                {
                    if (parameter.Name.ToLower().Equals(oldVal.ToLower()))
                    {
                        parameter.Name = newVal;
                    }
                }
            }

            foreach (Constraint constraint in _constraints)
            {
                foreach (Parameter parameter in constraint.ParameterSet)
                {
                    if (parameter.Name.ToLower().Equals(oldVal.ToLower()))
                    {
                        parameter.Name = newVal;
                    }
                }
            }

            foreach (Restraint restraint in _restraints)
            {
                foreach (Parameter parameter in restraint.ParameterSet)
                {
                    if (parameter.Name.ToLower().Equals(oldVal.ToLower()))
                    {
                        parameter.Name = newVal;
                    }
                }
            }

            foreach (CustomVariable cv in _customVariables)
            {
                foreach (Parameter parameter in cv.ParameterSet)
                {
                    if (parameter.Name.ToLower().Equals(oldVal.ToLower()))
                    {
                        parameter.Name = newVal;
                    }
                }
            }
        }

        /// <summary>
        /// Used to check if this config has any workflow rules with a path parameter
        /// that navigates along the provided path.
        /// </summary>
        /// <param name="path">Path string to check against.</param>
        /// <remarks>
        /// Parameters are considered a match if thier Name starts with the given path.
        /// String check is case insensitive.
        /// </remarks>
        public bool HasMatchingPathParameter(string path)
        {
            if (!path.StartsWith(DataPath.PathHeader, StringComparison.OrdinalIgnoreCase))
            {
                path = DataPath.PathHeader + path;
            }

            return _conditions.Any(c => c.ParameterSet.Any(p => p.Name.StartsWith(path, StringComparison.OrdinalIgnoreCase)))
                || _constraints.Any(c => c.ParameterSet.Any(p => p.Name.StartsWith(path, StringComparison.OrdinalIgnoreCase)))
                || _restraints.Any(c => c.ParameterSet.Any(p => p.Name.StartsWith(path, StringComparison.OrdinalIgnoreCase)))
                || _customVariables.Any(c => c.ParameterSet.Any(p => p.Name.StartsWith(path, StringComparison.OrdinalIgnoreCase)));
        }

        public bool DoesExistNonParameter(E_TypeToRename typeToFind, string parameterName, string valueToFind)
        {
            return this.FindValuesInUse(typeToFind, parameterName).Contains(valueToFind, StringComparer.OrdinalIgnoreCase);
        }

        public IEnumerable<string> FindValuesInUse(E_TypeToRename typeToFind, string parameterName)
        {
            List<string> foundValues = new List<string>();

            Func<Parameter, IEnumerable<string>> getter = parameter => {
                if (typeToFind == E_TypeToRename.Value)
                {
                    return parameter.Values;
                }
                else
                {
                    return parameter.ValueDesc.Values;
                }
            };

            foreach (AbstractConditionGroup condition in _conditions.Concat(_constraints).Concat(_restraints))
            {
                foreach (Parameter parameter in condition.ParameterSet)
                {
                    if (parameterName.ToLower().Equals(parameter.Name.ToLower()))
                    {
                        foundValues.AddRange(getter(parameter));
                    }
                }
            }

            foreach (CustomVariable cv in _customVariables)
            {
                foreach (Parameter parameter in cv.ParameterSet)
                {
                    if (parameterName.ToLower().Equals(parameter.Name.ToLower()))
                    {
                        if (parameterName.ToLower().Equals(parameter.Name.ToLower()))
                        {
                            foundValues.AddRange(getter(parameter));
                        }
                    }
                }
            }

            return foundValues;
        }

        public void RenameNonParameter(E_TypeToRename typeToRename, string parameterName, string oldVal, string newVal)
        {
            switch (typeToRename)
            {
                case E_TypeToRename.Value:
                    foreach (Condition condition in _conditions)
                    {
                        foreach (Parameter parameter in condition.ParameterSet)
                        {
                            if (parameterName.ToLower().Equals(parameter.Name.ToLower()))
                            {
                                parameter.RenameValue(oldVal, newVal);
                            }
                        }
                    }

                    foreach (Constraint constraint in _constraints)
                    {
                        foreach (Parameter parameter in constraint.ParameterSet)
                        {
                            if (parameterName.ToLower().Equals(parameter.Name.ToLower()))
                            {
                                parameter.RenameValue(oldVal, newVal);
                            }
                        }
                    }

                    foreach (Restraint restraint in _restraints)
                    {
                        foreach (Parameter parameter in restraint.ParameterSet)
                        {
                            if (parameterName.ToLower().Equals(parameter.Name.ToLower()))
                            {
                                parameter.RenameValue(oldVal, newVal);
                            }
                        }
                    }

                    foreach (CustomVariable cv in _customVariables)
                    {
                        foreach (Parameter parameter in cv.ParameterSet)
                        {
                            if (parameterName.ToLower().Equals(parameter.Name.ToLower()))
                            {
                                parameter.RenameValue(oldVal, newVal);
                            }
                        }
                    }
                    break;
                case E_TypeToRename.ValueDesc:
                    foreach (Condition condition in _conditions)
                    {
                        foreach (Parameter parameter in condition.ParameterSet)
                        {
                            if (parameterName.ToLower().Equals(parameter.Name.ToLower()))
                            {
                                parameter.RenameValueDesc(oldVal, newVal);
                            }
                        }
                    }

                    foreach (Constraint constraint in _constraints)
                    {
                        foreach (Parameter parameter in constraint.ParameterSet)
                        {
                            if (parameterName.ToLower().Equals(parameter.Name.ToLower()))
                            {
                                parameter.RenameValueDesc(oldVal, newVal);
                            }
                        }
                    }

                    foreach (Restraint restraint in _restraints)
                    {
                        foreach (Parameter parameter in restraint.ParameterSet)
                        {
                            if (parameterName.ToLower().Equals(parameter.Name.ToLower()))
                            {
                                parameter.RenameValueDesc(oldVal, newVal);
                            }
                        }
                    }

                    foreach (CustomVariable cv in _customVariables)
                    {
                        foreach (Parameter parameter in cv.ParameterSet)
                        {
                            if (parameterName.ToLower().Equals(parameter.Name.ToLower()))
                            {
                                parameter.RenameValueDesc(oldVal, newVal);
                            }
                        }
                    }
                    break;
                default:
                    throw CBaseException.CreateUnexpectedEnumException(typeToRename);
            }
        }

        private void ParseConfigFile(string configXml)
        {
            XDocument config;
            if (string.IsNullOrEmpty(configXml))
            {
                config = new XDocument(
                    new XDeclaration("1.0", "utf8", "yes"),
                    new XElement("SystemConfig",
                        new XElement("ConditionSet"),
                        new XElement("ConstraintSet"),
                        new XElement("CustomVariableSet")
                        )
                );
            }
            else
            {
                config = XDocument.Parse(configXml);
            }

            config.Validate(x_schemas, (o, e) =>
            {
                throw new XmlException(e.Message);
            });

            XElement root = config.Element("SystemConfig");
            _conditions = new ConditionSet(root.Element("ConditionSet"));
            _constraints = new ConstraintSet(root.Element("ConstraintSet"));
            _restraints = new RestraintSet(root.Element("RestraintSet"));
            _customVariables = new CustomVariableSet(root.Element("CustomVariableSet"));
        }

        public override string ToString()
        {
            return GetXml().ToString();
        }

        public XDocument GetXml()
        {
            XDocument config = new XDocument(
                new XDeclaration("1.0", "utf-8", "yes"),
                new XElement("SystemConfig",
                 _conditions.GetXml(),
                 _constraints.GetXml(),
                 _restraints.GetXml(),
                 _customVariables.GetXml()
                 )
             );

            config.Validate(x_schemas, (o, e) =>
            {
                throw new XmlException(e.Message);
            });
            return config;
        }

        public bool IsFieldInUse(params string[]  fieldNames)
        {
            Func<ParameterSet, bool> fieldSearch = p => p.Any(c => fieldNames.Contains(c.Name, StringComparer.CurrentCultureIgnoreCase ));

            if (_conditions.Any( p => fieldSearch(p.ParameterSet)))
            {
                return true;
            }

            if( _constraints.Any(p => fieldSearch(p.ParameterSet)))
            {
                return true;
            }

            if (_restraints.Any(p => fieldSearch(p.ParameterSet)))
            {
                return true;
            }

            if ( _customVariables.Any(p=>fieldSearch(p.ParameterSet)))
            {
                return true;
            }
            return false;
        }

        public void Filter(string searchText)
        {
            this.Conditions.Filter(searchText);
            this.Constraints.Filter(searchText);
            this.Restraints.Filter(searchText);
            this.CustomVariables.Filter(searchText);
        }
    }
}
