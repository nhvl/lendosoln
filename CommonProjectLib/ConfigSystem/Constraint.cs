﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace ConfigSystem
{
    public class Constraint : AbstractConditionGroup
    {
        protected override string RootName
        {
            get { return "Constraint"; }
        }

        public Constraint(XElement constraintRoot)
            : base(constraintRoot)
        {

        }

        public Constraint(XElement constraintRoot, Guid newId)
            : base(constraintRoot, newId)
        {

        }

        public Constraint() : base()
        {

        }
    }
}
