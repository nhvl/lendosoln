﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using CommonProjectLib.ConfigSystem;

namespace ConfigSystem
{
    /// <summary>
    /// Represents a System Operation for use in conditions.  A System Operation is something like "Run PML" or "Write Loan"
    /// The available system operations for this instance are stored on a system config XML file so that any
    /// group utilizing this project can add whatever system operations apply to their product.  The UI is in charge
    /// of knowing what system operations refer to which parts of the UI, and hiding those UI elements accordingly
    /// if permission is not granted for the System Operation.
    /// </summary>
    public class SystemOperation
    {
        private string m_opName = "";

        public string Name
        {
            get { return m_opName; }
        }

        private OperationFieldSet m_fields;

        public OperationFieldSet Fields
        {
            get { return m_fields; }
            set { m_fields = value; }
        }

        public SystemOperation(ISystemOperationIdentifier operation)
            : this(operation.Id)
        {
        }

        public SystemOperation(string name)
        {
            m_fields = new OperationFieldSet();
            m_opName = name;
            Validate();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="systemOperationRoot">One of the "SystemOperation" elements from the main SystemConfig XML</param>
        public SystemOperation(XElement systemOperationRoot)
        {
            m_fields = new OperationFieldSet();
            Parse(systemOperationRoot);
        }

        /// <summary>
        /// Parses the SystemOperation element for the system operation name
        /// </summary>
        /// <param name="systemOperationRoot">One of the "SystemOperation" elements from the main SystemConfig XML</param>
        private void Parse(XElement systemOperationRoot)
        {
            m_opName = systemOperationRoot.Value;
            var fieldSet = systemOperationRoot.Element("FieldSet");
            if (fieldSet != null)
            {
                var fields = fieldSet.Elements("Field");
                foreach (var field in fields)
                {
                    m_fields.Add(new OperationField(field));
                }
            }
            Validate();
        }

        /// <summary>
        /// Provides internal validation that is not expected to be handled outside this project.  For example,
        /// validating that the System Operation name set in this object is actually one of the available system
        /// operations for the product will be handled outside.  Validating that the FriendlyName value is contained in
        /// the XML will be handled here internally.
        /// </summary>
        private void Validate()
        {
            if (string.IsNullOrEmpty(m_opName))
            {
                throw new ValidationException("System Error.", "System operations must contain a name");
            }
        }

        /// <summary>
        /// Generates the XML representing this object.
        /// </summary>
        /// <returns>The XElement that is the root of this object's XML tree</returns>
        public XElement GetXml()
        {
            var root = new XElement("SystemOperation", Name);
            if (m_fields.Count() > 0)
            {
                root.Add(m_fields.GetXml());
            }
            return root;
        }

        public void AddField(OperationField f)
        {
            m_fields.Add(f);
        }

        public void RemoveField(string fieldName)
        {
            m_fields.Remove(fieldName);
        }
    }
}
