﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConfigSystem.Engine
{
    public enum E_TableEntryT
    {
        ActionTable,
        VariableTable,
        StringTable,
        ConditionGuidTable,
        CustomVariableTable,
        RestraintGuidTable
    }
}
