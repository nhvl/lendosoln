﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConfigSystem.Engine
{
    class VariableEntryType
    {
        public int VarIndex { get; private set; }
        public SupportedType Type { get; private set; }
        public int StringIndex { get; private set; }

        private bool m_IsLookAtNewData;
        public bool IsLookAtNewData
        {
            get
            {
                // 3/21/2014 dd - Implement for OPM 144112.
                // I need a way to indicate the engine that this variable is only interest
                // about field that is modify.
                if (m_rawVariableName == null)
                {
                    ParseVariableName();
                }

                return m_IsLookAtNewData;
            }
        }

        private bool m_IsNotOperation;
        public bool IsNotOperation
        {
            get
            {
                if (m_rawVariableName == null)
                {
                    ParseVariableName();
                }

                return m_IsNotOperation;
            }
        }

        private string m_rawVariableName = null;

        private string m_displayVariableName = null;

        private void ParseVariableName()
        {
            m_rawVariableName = m_compileCode.GetStringByIdx(StringIndex);
            m_displayVariableName = m_rawVariableName;

            // OPM 241675 - Check for not prefix.
            if (m_displayVariableName.StartsWith(ExecutingEngineCompiler.NotPrefix, StringComparison.OrdinalIgnoreCase))
            {
                m_IsNotOperation = true;
                m_displayVariableName = m_rawVariableName.Substring(ExecutingEngineCompiler.NotPrefix.Length);
            }

            // Check for is look at new data prefix.
            if (m_displayVariableName.StartsWith("m:"))
            {
                m_IsLookAtNewData = true;
                m_displayVariableName = m_rawVariableName.Substring(2);
            }
        }
        public string VariableName
        {
            get 
            {
                if (null == m_rawVariableName)
                {
                    ParseVariableName();
                }
                return m_displayVariableName;
            }
        }

        
        private ExecutingEngineCompileCode m_compileCode;
        
        public VariableEntryType(int varIndex, SupportedType type, int stringIndex, ExecutingEngineCompileCode compileCode)
        {
            VarIndex = varIndex;
            Type = type;
            StringIndex = stringIndex;
            m_compileCode = compileCode;
        }
    }
}
