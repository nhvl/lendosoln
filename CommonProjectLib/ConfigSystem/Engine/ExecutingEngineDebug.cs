﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConfigSystem.Engine
{
    public class ExecutingEngineDebug
    {
        private List<ExecutingEngineConditionDebug> m_conditionList = new List<ExecutingEngineConditionDebug>();

        private List<ExecutingEngineConditionDebug> m_restraintList = new List<ExecutingEngineConditionDebug>();

        private List<TriggerDebug> m_restraintTriggerList = new List<TriggerDebug>();
        private List<TriggerDebug> m_conditionTriggerList = new List<TriggerDebug>();

        public ExecutingEngineDebug(string operation)
        {
            this.Operation = operation;
        }

        public IReadOnlyCollection<ExecutingEngineConditionDebug> ConditionList
        {
            get { return m_conditionList; }
        }

        public IReadOnlyCollection<ExecutingEngineConditionDebug> RestraintList
        {
            get { return m_restraintList; }
        }

        public IReadOnlyCollection<TriggerDebug> RestraintTriggerList
        {
            get { return m_restraintTriggerList; }
        }

        public IReadOnlyCollection<TriggerDebug> ConditionTriggerList
        {
            get { return m_conditionTriggerList; }
        }

        public void AddConditions(IEnumerable<ExecutingEngineConditionDebug> condition)
        {
            m_conditionList.AddRange(condition);
        }

        public void AddRestraints(IEnumerable<ExecutingEngineConditionDebug> condition)
        {
            m_restraintList.AddRange(condition);
        }

        public void SetTriggers(Dictionary<string, TriggerDebug> restraintTriggerDict, Dictionary<string, TriggerDebug> conditionTriggerDict)
        {
            m_restraintTriggerList = restraintTriggerDict != null ? restraintTriggerDict.Values.OrderBy(o => o.TriggerName).ToList() : new List<TriggerDebug>();
            m_conditionTriggerList = conditionTriggerDict != null ? conditionTriggerDict.Values.OrderBy(o => o.TriggerName).ToList() : new List<TriggerDebug>();
        }

        public bool CanPerform { get; set; }

        public string Operation { get; private set; }
    }
}
