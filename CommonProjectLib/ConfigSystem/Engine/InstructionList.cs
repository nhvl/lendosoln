﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConfigSystem.Engine.Instructions;

namespace ConfigSystem.Engine
{
    public class InstructionList : IEnumerable<IInstruction>
    {
        private List<IInstruction> m_list = new List<IInstruction>();
        public int Offset { get; private set; }
        public void Add(IInstruction instruction)
        {
            m_list.Add(instruction);
            Offset += instruction.GetBytes().Length;
        }
        public void Add(IEnumerable<IInstruction> instructions)
        {
            foreach (IInstruction instruction in instructions)
            {
                this.Add(instruction);
            }
        }

        public IEnumerator<IInstruction> GetEnumerator()
        {
            return m_list.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
