﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConfigSystem.Engine
{
    public class ExecutingEngineParameterDebug
    {
        internal ExecutingEngineParameterDebug(string name, E_FunctionT function)
        {
            this.VarName = name;
            this.Function = function;
        }
        private List<string> m_expectedValueList = new List<string>();
        private List<string> m_actualValueList = new List<string>();

        public string VarName { get; private set; }
        public E_FunctionT Function { get; private set; }

        internal void AddExpectedValue<T>(T value)
        {
            m_expectedValueList.Add(value.ToString());
        }
        internal void AddActualValue<T>(IEnumerable<T> value)
        {
            foreach (var o in value)
            {
                m_actualValueList.Add(o.ToString());
            }
        }

        public IEnumerable<string> ExpectedValueList
        {
            get { return m_expectedValueList; }
        }
        public IEnumerable<string> ActualValueList
        {
            get { return m_actualValueList; }
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("VarName=[" + VarName + "] Function=" + Function + " ExpectedValue=[");
            sb.Append(string.Join(",", m_expectedValueList.ToArray()));
            sb.Append("], ActualValue=[");
            sb.Append(string.Join(",", m_actualValueList.ToArray()));
            sb.Append("]");
            return sb.ToString();
        }
    }
}
