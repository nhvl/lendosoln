﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConfigSystem.Engine
{
    class RuleListEntryType
    {
        public int StringIndex { get; private set; }

        public string Name
        {
            get { return m_compileCode.GetStringByIdx(StringIndex); }
        }
        private ExecutingEngineCompileCode m_compileCode;
        private int[] m_ruleList;

        public IEnumerable<int> RuleList
        {
            get
            {
                return m_ruleList;
            }
        }

        public RuleListEntryType(int stringIndex, int[] ruleList, ExecutingEngineCompileCode compileCode)
        {
            m_ruleList = ruleList;
            StringIndex = stringIndex;
            m_compileCode = compileCode;
        }
    }
}
