﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace ConfigSystem.Engine
{
    public class UnionTypeIntComparer : IComparer<UnionType>
    {
        public int Compare(UnionType x, UnionType y)
        {
            return x.IntValue.CompareTo(y.IntValue);
        }
    }
    public class UnionTypeFloatComparer : IComparer<UnionType>
    {
        public int Compare(UnionType x, UnionType y)
        {
            return x.FloatValue.CompareTo(y.FloatValue);
        }
    }

    public class UnionTypeIntEqualityComparer : IEqualityComparer<UnionType>
    {

        public bool Equals(UnionType x, UnionType y)
        {
            return x.IntValue.Equals(y.IntValue);
        }

        public int GetHashCode(UnionType obj)
        {
            return obj.IntValue.GetHashCode();
        }
    }

    public class UnionTypeFloatEqualityComparer : IEqualityComparer<UnionType>
    {

        public bool Equals(UnionType x, UnionType y)
        {
            return x.FloatValue.Equals(y.FloatValue);
        }

        public int GetHashCode(UnionType obj)
        {
            return obj.FloatValue.GetHashCode();
        }
    }

    [StructLayout(LayoutKind.Explicit)]
    public struct UnionType
    {
        [FieldOffset(0)]
        public int IntValue;
        [FieldOffset(0)]
        public float FloatValue;
        [FieldOffset(0)]
        public ushort UShortValue;

        [FieldOffset(0)]
        public byte Byte0;
        [FieldOffset(1)]
        public byte Byte1;
        [FieldOffset(2)]
        public byte Byte2;
        [FieldOffset(3)]
        public byte Byte3;

    }
}
