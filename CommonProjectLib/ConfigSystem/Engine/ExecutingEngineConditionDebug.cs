﻿namespace ConfigSystem.Engine
{
    using System;
    using System.Collections.Generic;

    public class ExecutingEngineConditionDebug
    {
        private List<ExecutingEngineParameterDebug> m_failedParameterList = new List<ExecutingEngineParameterDebug>();
        private List<ExecutingEngineParameterDebug> m_successfulParameterList = new List<ExecutingEngineParameterDebug>();

        public Guid ConditionId { get; private set;}

        public ExecutingEngineConditionDebug(Guid id)
        {
            ConditionId = id;
        }
        public int FailedParameterCount
        {
            get { return m_failedParameterList.Count; }
        }

        public int SuccessfulParameterCount
        {
            get { return m_successfulParameterList.Count; }
        }

        public IEnumerable<ExecutingEngineParameterDebug> FailedParameterList
        {
            get { return m_failedParameterList; }
        }
        public IEnumerable<ExecutingEngineParameterDebug> SuccessfulParameterList
        {
            get { return m_successfulParameterList; }
        }

        public void Add<T>(bool bResult, string parameterName, E_FunctionT function, IList<T> expectedValues, IList<T> actualValues)
        {
            ExecutingEngineParameterDebug parameter = new ExecutingEngineParameterDebug(parameterName, function);
            parameter.AddActualValue(actualValues);
            foreach (var o in expectedValues)
            {
                parameter.AddExpectedValue(o);
            }
            if (bResult)
            {
                m_successfulParameterList.Add(parameter);
            }
            else
            {
                m_failedParameterList.Add(parameter);
            }
        }
    }

    public class ExecutingEngineConditionDebugComparer : IEqualityComparer<ExecutingEngineConditionDebug>
    {
        public bool Equals(ExecutingEngineConditionDebug first, ExecutingEngineConditionDebug second)
        {
            if (object.ReferenceEquals(first, second))
            {
                return true;
            }

            return first?.ConditionId == second?.ConditionId;
        }

        public int GetHashCode(ExecutingEngineConditionDebug obj)
        {
            return obj.ConditionId.GetHashCode();
        }
    }
}
