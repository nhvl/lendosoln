﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConfigSystem.Engine
{
    public enum ComparisonOpCode
    {
        equal = 0,
        not_equal = 1,
        greater = 2,
        greater_or_equal = 3,
        less_than = 4,
        less_than_or_equal = 5,
        is_non_default_value = 6, // default value for  a) str:null or empty.  b) int: 0 c) float:0
        is_valid_value = 7 // Is Valid value mean the value getter does not throw exception.

    }
}
