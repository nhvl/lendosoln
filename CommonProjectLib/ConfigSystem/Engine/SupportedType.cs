﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConfigSystem.Engine
{
    public enum SupportedType : byte
    {
        Bool = 0,
        Byte = 1,
        UShort = 2,
        Int = 4,
        Float = 5,
        String = 6,
        DateTime = 7

    }
}
