﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConfigSystem.Engine
{
    public class ExecutingEngineVersionMismatchException : Exception
    {
        public ExecutingEngineVersionMismatchException()
            : base("Version does not match. Recompile again.")
        {
        }
    }
}
