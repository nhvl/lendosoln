﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConfigSystem.Engine
{
    public enum OpCode
    {
        eq_0_15 = 0,
        eq_16_31 = 1,
        between = 2,
        in_byte = 3,
        in_ushort = 4,
        in_int = 5,
        in_float = 6,
        in_string = 7,
        in_datetime = 8,
        comparison_op = 9, // Handle >=, >, <=, <, !=, =
        comparison_var = 10, // Similar with comparison_op except the right hand side value is var id.
        custom_var = 11,
        label = 14,
        ret_true = 15,

    }
}
