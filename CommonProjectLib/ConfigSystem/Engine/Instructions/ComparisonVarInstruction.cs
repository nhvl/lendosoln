﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConfigSystem.Engine.Instructions
{
    class ComparisonVarInstruction<T> : AbstractOpCodesInstruction<T>
    {
        protected override IEnumerable<SupportedType> AllowSupportedTypeList
        {
            get { return new SupportedType[] { SupportedType.Bool, SupportedType.Int, SupportedType.Float, SupportedType.String, SupportedType.DateTime }; }
        }
        private ComparisonOpCode m_comparisonOpCode;
        private ushort m_rhsVarIndex;

        internal ComparisonVarInstruction(string varId, SymbolTable symbolTable, string rhsVarId, E_FunctionT function)
            : base(varId, symbolTable)
        {
            m_rhsVarIndex = symbolTable.GetVarIndex(rhsVarId, ArgumentType);
            switch (function)
            {
                case E_FunctionT.EqualVar:
                    m_comparisonOpCode = ComparisonOpCode.equal;
                    break;
                //case E_FunctionT.NotEqualVar:
                //    m_comparisonOpCode = ComparisonOpCode.not_equal;
                //    break;
                case E_FunctionT.IsGreaterThanVar:
                    m_comparisonOpCode = ComparisonOpCode.greater;
                    break;
                case E_FunctionT.IsLessThanVar:
                    m_comparisonOpCode = ComparisonOpCode.less_than;
                    break;
                //case E_FunctionT.BeginsWithVar:
                //    m_comparisonOpCode = ComparisonOpCode.begins_with;
                //    break;
                //case E_FunctionT.DoesNotBeginWithVar:
                //    m_comparisonOpCode = ComparisonOpCode.not_begins_with;
                //    break;
                case E_FunctionT.IsGreaterThanOrEqualToVar:
                    m_comparisonOpCode = ComparisonOpCode.greater_or_equal;
                    break;
                case E_FunctionT.IsLessThanOrEqualToVar:
                    m_comparisonOpCode = ComparisonOpCode.less_than_or_equal;
                    break;
                default:
                    throw new Exception("Unhandle function=" + function + " in ComparisonInstruction.");
            }

        }
        protected override void CreateOpCodes()
        {
            Append(OpCode.comparison_var, (int)m_comparisonOpCode);
            Append(VarIndex);
            Append(m_rhsVarIndex);
        }

        public override string ToString()
        {
            return ".comparison_var " + m_comparisonOpCode + " .lhs_var_idx:" + VarIndex + " .rhs_var_idx:" + m_rhsVarIndex;
        }
    }
}
