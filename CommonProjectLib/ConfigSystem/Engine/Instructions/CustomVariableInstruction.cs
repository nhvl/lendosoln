﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommonProjectLib.Common;

namespace ConfigSystem.Engine.Instructions
{
    public class CustomVariableInstruction : AbstractInstruction
    {
        private ushort m_customVariableIndex;
        private bool m_rhsValue;
        private bool m_isSystemCustomVariable = false;
        public CustomVariableInstruction(string variableName, CustomVariable.E_Scope scope, SymbolTable symbolTable, string rhsValue)
        {
            // 11/7/2011 dd - TODO Have better way to handle system custom variable.
            switch (scope)
            {
                case CustomVariable.E_Scope.System:
                    m_isSystemCustomVariable = true;
                    break;
                case CustomVariable.E_Scope.Lender:
                    m_isSystemCustomVariable = false;
                    break;
                default:
                    throw CBaseException.CreateUnexpectedEnumException(scope);
            }

            m_rhsValue = bool.Parse(rhsValue);
            if (m_isSystemCustomVariable)
            {
                // 11/7/2011 dd - For system custom variable, we are storing string index.
                m_customVariableIndex = symbolTable.GetStringIndex(variableName);
            }
            else
            {
                m_customVariableIndex = symbolTable.GetCustomVariableIndex(variableName);
            }
        }
        public override string ToString()
        {
            return ".custom_var custom_var_idx:" + m_customVariableIndex + " eq " + m_rhsValue;
        }
        protected override void CreateOpCodes()
        {
            int opParameter = 0;

            if (m_isSystemCustomVariable)
            {
                opParameter = m_rhsValue ? 3 : 2;
            }
            else
            {
                opParameter = m_rhsValue ? 1 : 0;
            }
            Append(OpCode.custom_var, opParameter);
            Append(m_customVariableIndex);
        }
    }
}
