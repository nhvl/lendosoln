﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConfigSystem.Engine.Instructions
{
    public class InInstruction<T> : AbstractOpCodesInstruction<T> where T : IComparable<T>
    {
 
        protected override IEnumerable<SupportedType> AllowSupportedTypeList
        {
            get
            {
                return new SupportedType[] { SupportedType.Bool, SupportedType.Int, SupportedType.Float, SupportedType.String, SupportedType.DateTime };
            }
        }
        private List<UnionType> m_list;
        private OpCode m_opCode;
        private int m_opParameter = 0;
        private bool m_hasSpecialValue = false;
        private SupportedType m_bestType = SupportedType.String;

        private List<T> m_args;
        private Func<T, byte[]> m_converter;
        public InInstruction(string varId, SymbolTable symbolTable, List<T> args, Func<T, byte[]> converter)
            : base(varId, symbolTable)
        {
            if (null == args || args.Count == 0)
            {
                throw new Exception("Arguments list cannot be empty.");
            }
            if (args.Count >= byte.MaxValue)
            {
                throw new Exception("Arguments list cannot exceed " + byte.MaxValue + " items.");
            }
            m_args = args;
            m_converter = converter;
            m_opParameter = m_args.Count < 16 ? m_args.Count : 0;
            switch (ArgumentType)
            {
                case SupportedType.Bool:
                    break;
                case SupportedType.Byte:
                    break;
                case SupportedType.UShort:
                    break;
                case SupportedType.Int:
                    break;
                case SupportedType.Float:
                    m_opCode = OpCode.in_float;
                    break;
                case SupportedType.String:
                    m_opCode = OpCode.in_string;
                    break;
                case SupportedType.DateTime:
                    m_opCode = OpCode.in_datetime;
                    break;
                default:
                    throw new Exception("Unhandle TYpe=" + ArgumentType);
            }

        }
        public InInstruction(string varId, SymbolTable symbolTable, List<T> args) : base(varId, symbolTable)
        {
            if (null == args || args.Count == 0)
            {
                throw new Exception("Arguments list cannot be empty.");
            }
            if (args.Count >= byte.MaxValue)
            {
                throw new Exception("Arguments list cannot exceed " + byte.MaxValue + " items.");
            }

            m_args = args;

            m_list = new List<UnionType>();
            IComparer<UnionType> comparer = null;
            IEqualityComparer<UnionType> equalityComparer = null;
            switch (ArgumentType)
            {
                case SupportedType.Bool:
                case SupportedType.Int:
                    foreach (var o in args)
                    {
                        m_list.Add(new UnionType() { IntValue = Convert.ToInt32(o) });
                    }
                    comparer = new UnionTypeIntComparer();
                    equalityComparer = new UnionTypeIntEqualityComparer();
                    break;
                case SupportedType.Float:
                    m_list = new List<UnionType>();
                    foreach (var o in args)
                    {
                        m_list.Add(new UnionType() { FloatValue = Convert.ToSingle(o) });

                    }
                    comparer = new UnionTypeFloatComparer();
                    equalityComparer = new UnionTypeFloatEqualityComparer();
                    break;
                case SupportedType.String:
                    List<string> orderStringList = new List<string>();
                    foreach (var o in args)
                    {
                        orderStringList.Add(o.ToString());
                    }
                    orderStringList.Sort(StringComparer.OrdinalIgnoreCase);

                    foreach (var o in orderStringList)
                    {
                        m_list.Add(new UnionType() { UShortValue = symbolTable.GetStringIndex(o) });
                    }
                    comparer = null;
                    //comparer = new UnionTypeIntComparer();
                    //equalityComparer = new UnionTypeIntEqualityComparer();
                    break;
                case SupportedType.DateTime:
                    comparer = null;
                    m_args.Sort();
                    m_args = m_args.Distinct().ToList();
                    break;
                default:
                    throw new Exception("Unsupported type=" + ArgumentType + " in IN instruction");

            }
            if (null != comparer)
            {
                m_list.Sort(comparer);
                m_list = m_list.Distinct(equalityComparer).ToList();
            }

            if (ArgumentType == SupportedType.String)
            {
                m_bestType = SupportedType.UShort;
                m_hasSpecialValue = false;
                m_opCode = OpCode.in_string;
                m_opParameter = m_list.Count < 16 ? m_list.Count : 0;
            }
            else if (ArgumentType == SupportedType.Float)
            {
                m_bestType = SupportedType.Float;
                m_hasSpecialValue = false;
                m_opCode = OpCode.in_float;
                m_opParameter = m_list.Count < 16 ? m_list.Count : 0;
            }
            else if (ArgumentType == SupportedType.DateTime)
            {
                m_bestType = SupportedType.DateTime;
                m_hasSpecialValue = false;
                m_opCode = OpCode.in_datetime;
                m_opParameter = m_list.Count < 16 ? m_list.Count : 0;
            }
            else
            {
                m_bestType = GetBestIntegerType(m_list);
                if (m_list.Count == 1)
                {
                    int v = m_list[0].IntValue;
                    if (v >= 0 && v < 16)
                    {
                        m_hasSpecialValue = true;
                        m_opCode = OpCode.eq_0_15;
                        m_opParameter = v;
                    }
                    else if (v >= 16 && v < 32)
                    {
                        m_hasSpecialValue = true;
                        m_opCode = OpCode.eq_16_31;
                        m_opParameter = v - 16;
                    }
                }

                if (m_hasSpecialValue == false)
                {
                    if (m_bestType == SupportedType.Byte)
                    {
                        m_opCode = OpCode.in_byte;
                    }
                    else if (m_bestType == SupportedType.UShort)
                    {
                        m_opCode = OpCode.in_ushort;
                    }
                    else if (m_bestType == SupportedType.Int)
                    {
                        m_opCode = OpCode.in_int;
                    }
                    else
                    {
                        throw new Exception("Unsupported type=" + m_bestType);
                    }

                    m_opParameter = m_list.Count < 16 ? m_list.Count : 0;

                }
            }

        }

        protected override void CreateOpCodes()
        {
            Append(m_opCode, m_opParameter);
            if (m_args.Count >= 16)
            {
                Append((byte)m_args.Count);
            }
            Append(VarIndex);

            if (m_hasSpecialValue == false)
            {
                if (ArgumentType == SupportedType.DateTime)
                {
                    Append(m_args, m_converter);
                }
                else
                {
                    Append(m_list, m_bestType);
                }
            }
        }

        public override string ToString()
        {
            string args = "";
            foreach (var o in m_args)
            {
                args += o + ",";
            }
            return m_opCode + " var_idx:" + VarIndex + " args:" + args ;
        }

    }
}
