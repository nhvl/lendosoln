﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConfigSystem.Engine.Instructions
{
    public class BetweenInstruction<T> : AbstractOpCodesInstruction<T> where T : IComparable<T>
    {
        protected override IEnumerable<SupportedType> AllowSupportedTypeList
        {
            get
            {
                return new SupportedType[] {SupportedType.Int, SupportedType.Float, SupportedType.DateTime };
            }
        }
        private SupportedType m_type;

        private List<T> m_args;

        public BetweenInstruction(string varId, SymbolTable symbolTable, T minValue, T maxValue)
            : base (varId, symbolTable)
        {
            if (minValue.CompareTo(maxValue) > 0)
            {
                throw new Exception("maxValue is less than minValue. minValue=" + minValue + ", maxValue=" + maxValue);
            }

            m_args = new List<T>();
            m_args.Add(minValue);
            m_args.Add(maxValue);

            switch (ArgumentType)
            {
                case SupportedType.Int:
                    m_type = GetBestIntegerType(m_args);
                    break;
                case SupportedType.Float:
                case SupportedType.DateTime:
                    m_type = ArgumentType;
                    break;
                default:
                    throw new Exception("Unhandle type " + ArgumentType);
            }

        }

        public override string ToString()
        {
            return ".between type:" + m_type + ". var_idx:" + VarIndex + ". range:[" + m_args[0] + "," + m_args[1] + "]";
        }


        protected override void CreateOpCodes()
        {
            Append(OpCode.between, (int)m_type);
            Append(VarIndex);
            Append(m_args, m_type);
        }

    }
}
