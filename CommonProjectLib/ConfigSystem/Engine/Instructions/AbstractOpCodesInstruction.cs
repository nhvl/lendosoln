﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConfigSystem.Engine.Instructions
{
    public abstract class AbstractOpCodesInstruction<T> : AbstractInstruction
    {
        private ushort m_varIndex;
        protected ushort VarIndex
        {
            get { return m_varIndex; }
        }

        private SymbolTable m_symbolTable;
        protected SymbolTable SymbolTable
        {
            get { return m_symbolTable; }
        }

        protected abstract IEnumerable<SupportedType> AllowSupportedTypeList { get; }
        protected AbstractOpCodesInstruction(string varId, SymbolTable symbolTable)
        {
            if (AllowSupportedTypeList.Contains(ArgumentType) == false)
            {
                throw new Exception("Unsupport type=" + ArgumentType);
            }
            m_varIndex = symbolTable.GetVarIndex(varId, ArgumentType);
            m_symbolTable = symbolTable;
        }

        protected SupportedType ArgumentType
        {
            get
            {
                Type t = typeof(T);
                if (t == typeof(bool))
                {
                    return SupportedType.Bool;
                }
                else if (t == typeof(Int32) || t.IsEnum || t == typeof(Enum))
                {
                    return SupportedType.Int;
                }
                else if (t == typeof(Single))
                {
                    return SupportedType.Float;
                }
                else if (t == typeof(string))
                {
                    return SupportedType.String;
                }
                else if (t == typeof(DateTime)) {
                    return SupportedType.DateTime;
                }
                throw new Exception("Unsupport type " + t.FullName);
            }
        }
    }
}
