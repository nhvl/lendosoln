﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConfigSystem.Engine.Instructions
{
    public class StringDataInstruction : AbstractInstruction
    {
        private string m_string;

        public StringDataInstruction(string str)
        {
            if (string.IsNullOrEmpty(str))
            {
                str = string.Empty;
            }
            if (str.Length >= ushort.MaxValue)
            {
                throw new Exception("String too long");
            }
            m_string = str.ToLower();
        }

        protected override void CreateOpCodes()
        {
            Append((ushort)m_string.Length);
            Append(m_string);
        }

        public override string ToString()
        {
            return ".string [" +m_string + "]. Length=" + m_string.Length;
        }


    }
}
