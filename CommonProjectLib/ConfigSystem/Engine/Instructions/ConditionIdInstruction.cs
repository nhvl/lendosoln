﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConfigSystem.Engine.Instructions
{
    public class ConditionIdInstruction : AbstractInstruction
    {
        private ushort m_idx;
        public ConditionIdInstruction(ushort idx)
        {
            m_idx = idx;
        }
        protected override void CreateOpCodes()
        {
            Append(OpCode.label, 0);
            Append(m_idx);
        }
    }
}
