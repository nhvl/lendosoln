﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommonProjectLib.Common;

namespace ConfigSystem.Engine.Instructions
{
    class ComparisonInstruction<T> : AbstractOpCodesInstruction<T>
    {
        protected override IEnumerable<SupportedType> AllowSupportedTypeList
        {
            get 
            {
                return new SupportedType[] { SupportedType.Bool, SupportedType.Int, SupportedType.Float, SupportedType.String, SupportedType.DateTime };
            }
        }
        private ComparisonOpCode m_comparisonOpCode;
        private T m_arg;

        public ComparisonInstruction(string varId, SymbolTable symbolTable, ComparisonOpCode op, T arg)
            : base (varId, symbolTable)
        {
            m_arg = arg;
            m_comparisonOpCode = op;
        }
        public ComparisonInstruction(string varId, SymbolTable symbolTable, E_FunctionT function, T arg)
            : base(varId, symbolTable)
        {

            m_arg = arg;
            switch (function)
            {
                case E_FunctionT.Equal:
                    m_comparisonOpCode = ComparisonOpCode.equal;
                    break;
                //case E_FunctionT.NotEqual:
                //    m_comparisonOpCode = ComparisonOpCode.not_equal;
                //    break;
                case E_FunctionT.IsGreaterThan:
                    m_comparisonOpCode = ComparisonOpCode.greater;
                    break;
                case E_FunctionT.IsLessThan:
                    m_comparisonOpCode = ComparisonOpCode.less_than;
                    break;
                //case E_FunctionT.BeginsWith:
                //    m_comparisonOpCode = ComparisonOpCode.begins_with;
                //    break;
                //case E_FunctionT.DoesNotBeginWith:
                //    m_comparisonOpCode = ComparisonOpCode.not_begins_with;
                //    break;
                case E_FunctionT.IsGreaterThanOrEqualTo:
                    m_comparisonOpCode = ComparisonOpCode.greater_or_equal;
                    break;
                case E_FunctionT.IsLessThanOrEqualTo:
                    m_comparisonOpCode = ComparisonOpCode.less_than_or_equal;
                    break;
                default:
                    throw new Exception("Unhandle function=" + function + " in ComparisonInstruction.");
            }
        }

        protected override void CreateOpCodes()
        {
            if (m_comparisonOpCode == ComparisonOpCode.equal && ArgumentType == SupportedType.Bool)
            {
                bool b = Convert.ToBoolean(m_arg);
                Append(OpCode.eq_0_15, b ? 1 : 0);
                Append(VarIndex);
            }
            else
            {
                Append(OpCode.comparison_op, (int)m_comparisonOpCode);
                Append(VarIndex);
                switch (ArgumentType)
                {
                    case SupportedType.Bool:
                    case SupportedType.Int:
                        int intValue = Convert.ToInt32(m_arg);
                        Append(intValue);
                        break;
                    case SupportedType.Float:
                        float floatValue = Convert.ToSingle(m_arg);
                        Append(floatValue);
                        break;
                    case SupportedType.String:
                        ushort stringIndex = SymbolTable.GetStringIndex(m_arg.ToString());
                        Append(stringIndex);
                        break;
                    case SupportedType.DateTime:
                        DateTime dt = Convert.ToDateTime(m_arg);
                        Append(dt.Ticks);
                        break;
                    default:
                        throw CBaseException.CreateUnexpectedEnumException(ArgumentType);
                }
            }

        }

        public override string ToString()
        {
            return ".comparison " + m_comparisonOpCode + " .var_idx:" + VarIndex + ". arg:" + m_arg;
        }

    }
}
