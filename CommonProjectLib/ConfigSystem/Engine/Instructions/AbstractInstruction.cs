﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConfigSystem.Engine.Instructions
{
    public abstract class AbstractInstruction : IInstruction
    {
        private List<byte> m_bytes = null;

        public byte[] GetBytes()
        {
            if (null == m_bytes)
            {
                m_bytes = new List<byte>();
                CreateOpCodes();
            }
            return m_bytes.ToArray();
        }

        protected abstract void CreateOpCodes();

        protected void Append(OpCode opCode, int v)
        {
            if ((v < 0 && v > 15) || ((int)opCode > 15))
            {
                throw new Exception("Opcode is greater than 15 or v is out of range (0-15)");
            }
            byte b = (byte)(((int)opCode << 4) | v);
            Append(b);

        }
        protected void Append(byte value)
        {
            m_bytes.Add(value);
        }
        protected void Append(ushort value)
        {
            m_bytes.AddRange(BitConverter.GetBytes(value));
        }

        protected void Append(int value)
        {
            m_bytes.AddRange(BitConverter.GetBytes(value));
        }
        protected void Append(float value)
        {
            m_bytes.AddRange(BitConverter.GetBytes(value));
        }

        protected void Append(string value)
        {
            m_bytes.AddRange(ASCIIEncoding.ASCII.GetBytes(value));
        }
        protected void Append(long value)
        {
            m_bytes.AddRange(BitConverter.GetBytes(value));
        }
        protected void Append<T>(IEnumerable<T> args, Func<T, byte[]> convert)
        {
            foreach (var o in args)
            {
                m_bytes.AddRange(convert(o));
            }
        }
        protected void Append<T>(IEnumerable<T> args, SupportedType type)
        {
            foreach (var o in args)
            {
                switch (type)
                {
                    case SupportedType.Bool:
                    case SupportedType.Byte:
                        Append(Convert.ToByte(o));
                        break;
                    case SupportedType.UShort:
                        Append(Convert.ToUInt16(o));
                        break;
                    case SupportedType.Int:
                        Append(Convert.ToInt32(o));
                        break;
                    case SupportedType.Float:
                        Append(Convert.ToSingle(o));
                        break;
                    case SupportedType.DateTime:
                        DateTime dt = Convert.ToDateTime(o);
                        m_bytes.AddRange(BitConverter.GetBytes(dt.Ticks));
                        break;
                    case SupportedType.String:
                    default:
                        throw new Exception("Unhandle type=" + type);
                }
            }
        }

        protected SupportedType GetBestIntegerType<T>(IEnumerable<T> list)
        {
            if (typeof(T) != typeof(int))
            {
                throw new Exception("Only invoke GetBestIntegerType on integer type.");
            }
            SupportedType type = SupportedType.Byte;
            foreach (var o in list)
            {
                int v = Convert.ToInt32(o);
                if (v >= byte.MinValue && v <= byte.MaxValue) 
                {
                }
                else if (v >= ushort.MinValue && v <= ushort.MaxValue)
                {
                    type = SupportedType.UShort;
                }
                else
                {
                    type = SupportedType.Int;
                    break;
                }
            }
            return type;
        }

        protected void Append(IEnumerable<UnionType> args, SupportedType type)
        {
            foreach (var o in args)
            {
                switch (type)
                {
                    case SupportedType.Bool:
                    case SupportedType.Byte:
                        Append(o.Byte0);
                        break;
                    case SupportedType.Int:
                        Append(o.IntValue);
                        break;
                    case SupportedType.Float:
                        Append(o.FloatValue);
                        break;
                    case SupportedType.UShort:
                        Append(o.UShortValue);
                        break;
                    default:
                        throw new Exception("Unsupport type " + type + " in Append");
                }
            }
        }

        protected SupportedType GetBestIntegerType(IEnumerable<UnionType> list)
        {
            SupportedType type = SupportedType.Byte;

            foreach (var v in list)
            {
                int o = v.IntValue;
                if (o >= byte.MinValue && o <= byte.MaxValue)
                {
                }
                else if (o >= ushort.MinValue && o <= ushort.MaxValue)
                {
                    type = SupportedType.UShort;
                }
                else
                {
                    type = SupportedType.Int;
                    break;
                }
            }
            return type;

        }

    }
}
