﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConfigSystem.Engine.Instructions
{
    class GuidDataInstruction : AbstractInstruction
    {
        private Guid m_guid;

        internal GuidDataInstruction(Guid g)
        {
            m_guid = g;
        }

        protected override void CreateOpCodes()
        {
            Append(m_guid.ToByteArray(), SupportedType.Byte);
        }
        public override string ToString()
        {
            return ".guid [" + m_guid.ToString() + "]";
        }
    }
}
