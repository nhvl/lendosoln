﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConfigSystem.Engine.Instructions
{
    public class VarInstruction : AbstractInstruction
    {
        private ushort m_stringIdx;
        private SupportedType m_type;

        private static SupportedType[] s_supportedTypes = { SupportedType.Bool, SupportedType.Int, SupportedType.Float, SupportedType.String, SupportedType.DateTime };

        public VarInstruction(ushort stringIdx, SupportedType type)
        {
            if (s_supportedTypes.Contains(type) == false)
            {
                throw new Exception("Unsupported Type " + type);
            }

            m_stringIdx = stringIdx;
            m_type = type;


        }
        
        public override string ToString()
        {
            return ".var type:" + m_type + ". string_idx:" + m_stringIdx;
        }

        protected override void CreateOpCodes()
        {
            Append((byte)m_type);
            Append(m_stringIdx);
        }
    }
}
