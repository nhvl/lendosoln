﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConfigSystem.Engine.Instructions
{
    public class RuleVersionInstruction : AbstractInstruction
    {
        private DateTime m_version;

        public RuleVersionInstruction(DateTime version)
        {
            m_version = version;
        }

        protected override void CreateOpCodes()
        {
            Append(m_version.Ticks);
        }

        public override string ToString()
        {
            return ".rule_version " + m_version;
        }

    }
}
