﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConfigSystem.Engine.Instructions
{
    public class LabelInstruction : AbstractInstruction
    {
        private string m_label;

        public int Offset { get; set; }
        
        public LabelInstruction(string label)
        {
            m_label = label;
        }

        protected override void CreateOpCodes()
        {
            // There is no opcode instruction for Label instruction.

        }

        public override string ToString()
        {
            return ".label " + m_label;
        }

    }
}
