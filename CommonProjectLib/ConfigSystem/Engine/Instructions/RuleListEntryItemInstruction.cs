﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConfigSystem.Engine.Instructions
{
    public class RuleListEntryItemInstruction : AbstractInstruction
    {
        private string m_description;
        private ushort m_stringIndex;
        private IList<LabelInstruction> m_ruleList;

        public RuleListEntryItemInstruction(string description, ushort stringIndex, IList<LabelInstruction> ruleList)
        {
            m_stringIndex = stringIndex;
            m_ruleList = ruleList;
            m_description = description;
        }

        public override string ToString()
        {
            string s = m_description + " string_idx:" + m_stringIndex + ". rule_count:" + m_ruleList.Count + ". rule_offets:";

            foreach (var o in m_ruleList)
            {
                s += o.Offset + ",";
            }
            return s;
        }
        protected override void CreateOpCodes()
        {
            Append(m_stringIndex);
            Append((ushort)m_ruleList.Count);
            foreach (var o in m_ruleList)
            {
                Append(o.Offset);
            }
        }
    }
}
