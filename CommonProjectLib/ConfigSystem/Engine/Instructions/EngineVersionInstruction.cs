﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConfigSystem.Engine.Instructions
{
    public class EngineVersionInstruction : AbstractInstruction
    {
        private byte m_version;

        public EngineVersionInstruction(byte version)
        {
            m_version = version;
        }

        protected override void CreateOpCodes()
        {
            Append(m_version);
        }

        public override string ToString()
        {
            return ".engine_version " + m_version;
        }

    }
}
