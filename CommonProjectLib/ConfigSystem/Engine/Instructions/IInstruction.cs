﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConfigSystem.Engine.Instructions
{
    public interface IInstruction
    {
        byte[] GetBytes();
    }
}
