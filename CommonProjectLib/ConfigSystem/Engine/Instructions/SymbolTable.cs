﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConfigSystem.Engine.Instructions
{
    public class SymbolTable
    {
        private List<string> m_stringList = new List<string>();
        private List<KeyValuePair<ushort, SupportedType>> m_varList = new List<KeyValuePair<ushort, SupportedType>>();
        private Dictionary<ushort, List<LabelInstruction>> m_actionTableHash = new Dictionary<ushort, List<LabelInstruction>>();
        private Dictionary<ushort, List<LabelInstruction>> m_restraintTableHash = new Dictionary<ushort, List<LabelInstruction>>();
        private List<KeyValuePair<ushort, List<LabelInstruction>>> m_customVariableList = new List<KeyValuePair<ushort, List<LabelInstruction>>>();

        private List<Guid> m_conditionGuidList = new List<Guid>();

        public ushort GetStringIndex(string s)
        {
            s = s.ToLower();
            for (int i = 0; i < m_stringList.Count; i++)
            {
                if (m_stringList[i] == s)
                {
                    return (ushort) i;
                }
            }

            if (m_stringList.Count == ushort.MaxValue)
            {
                throw new Exception("Compiler only support " + ushort.MaxValue + " unique strings.");
            }
            m_stringList.Add(s);
            return (ushort)(m_stringList.Count - 1);

        }
        public ushort GetGuidIndex(Guid id)
        {
            for (int i = 0; i < m_conditionGuidList.Count; i++)
            {
                if (m_conditionGuidList[i] == id)
                {
                    return (ushort)i;
                }
            }
            if (m_conditionGuidList.Count == ushort.MaxValue)
            {
                throw new Exception("Compiler only support " + ushort.MaxValue + " unique Guid.");
            }
            m_conditionGuidList.Add(id);
            return (ushort)(m_conditionGuidList.Count - 1);
        }
        public ushort GetVarIndex(string varId, SupportedType type)
        {
            ushort stringIdx = GetStringIndex(varId);
            for (int i = 0; i < m_varList.Count; i++)
            {
                if (m_varList[i].Key == stringIdx)
                {
                    if (m_varList[i].Value == type)
                    {
                        return (ushort)i;
                    }
                    else
                    {
                        throw new Exception(varId + " has multiple types.");
                    }
                }
            }
            if (m_varList.Count == ushort.MaxValue)
            {
                throw new Exception("Compiler only support " + ushort.MaxValue + " unique variables.");
            }
            m_varList.Add(new KeyValuePair<ushort, SupportedType>(stringIdx, type));
            return (ushort)(m_varList.Count - 1);
        }
        public ushort GetCustomVariableIndex(string variableName)
        {
            ushort stringIdx = GetStringIndex(variableName);
            for (int i = 0; i < m_customVariableList.Count; i++)
            {
                if (m_customVariableList[i].Key == stringIdx)
                {
                    return (ushort)i;
                }
            }
            if (m_customVariableList.Count == ushort.MaxValue)
            {
                throw new Exception("Compiler only support " + ushort.MaxValue + " unique custom variables");
            }
            m_customVariableList.Add(new KeyValuePair<ushort, List<LabelInstruction>>(stringIdx, new List<LabelInstruction>()));
            return (ushort)(m_customVariableList.Count - 1);
        }
        public void AddCustomVariable(string variableName, LabelInstruction ruleLabel)
        {
            int customVariableIndex = GetCustomVariableIndex(variableName);
            m_customVariableList[customVariableIndex].Value.Add(ruleLabel);
        }
        public void AddActionToken(string actionToken, LabelInstruction ruleLabel)
        {
            ushort stringIndex = GetStringIndex(actionToken);
            List<LabelInstruction> list = null;

            if (m_actionTableHash.TryGetValue(stringIndex, out list) == false)
            {
                list = new List<LabelInstruction>();
                m_actionTableHash.Add(stringIndex, list);
            }
            list.Add(ruleLabel);
        }

        public void AddRestraintToken(string restraintToken, LabelInstruction ruleLabel)
        {
            ushort stringIndex = GetStringIndex(restraintToken);
            List<LabelInstruction> list = null;

            if (m_restraintTableHash.TryGetValue(stringIndex, out list) == false)
            {
                list = new List<LabelInstruction>();
                m_restraintTableHash.Add(stringIndex, list);
            }
            list.Add(ruleLabel);
        }


        public void Export(InstructionList results, TableEntryInstruction tableEntry)
        {
            IList<IInstruction> instructionList = null;
            switch (tableEntry.Key)
            {
                case E_TableEntryT.ActionTable:
                    instructionList = ExportActionTableEntryItem();
                    break;
                case E_TableEntryT.VariableTable:
                    instructionList = ExportVariableData();
                    break;
                case E_TableEntryT.StringTable:
                    instructionList = ExportStringData();
                    break;
                case E_TableEntryT.CustomVariableTable:
                    instructionList = ExportCustomVariableTableEntryItem();
                    break;
                case E_TableEntryT.ConditionGuidTable:
                    instructionList = ExportConditionGuidList();
                    break;
                case E_TableEntryT.RestraintGuidTable:
                    instructionList = ExportRestraintTableEntryItem();
                    break;
                default:
                    throw new NotSupportedException("Unhandle E_TableEntryT." + tableEntry.Key);
            }

            results.Add(new LabelInstruction(tableEntry.Key.ToString()));
            tableEntry.Set(results.Offset, (ushort)instructionList.Count);

            foreach (var o in instructionList)
            {
                results.Add(o);
            }

        }

        private IList<IInstruction> ExportVariableData()
        {
            List<IInstruction> list = new List<IInstruction>(m_varList.Count);
            foreach (var o in m_varList)
            {
                list.Add(new VarInstruction(o.Key, o.Value));
            }
            return list;
        }
        private IList<IInstruction> ExportStringData()
        {
            List<IInstruction> list = new List<IInstruction>(m_stringList.Count);

            foreach (string s in m_stringList)
            {
                list.Add(new StringDataInstruction(s));
            }
            return list;
        }
        private IList<IInstruction> ExportConditionGuidList()
        {
            List<IInstruction> list = new List<IInstruction>(m_conditionGuidList.Count);
            foreach (var o in m_conditionGuidList)
            {
                list.Add(new GuidDataInstruction(o));
            }
            return list;
        }
        private IList<IInstruction> ExportCustomVariableTableEntryItem()
        {
            List<IInstruction> list = new List<IInstruction>();
            foreach (var item in m_customVariableList)
            {
                list.Add(new RuleListEntryItemInstruction(".custom_var_table_entry", item.Key, item.Value));
            }
            return list;
        }
        private IList<IInstruction> ExportActionTableEntryItem()
        {
            List<IInstruction> instructionList = new List<IInstruction>();

            foreach (var item in m_actionTableHash)
            {
                instructionList.Add(new RuleListEntryItemInstruction(".action_table_entry", item.Key, item.Value));
            }

            return instructionList;
        }

        private IList<IInstruction> ExportRestraintTableEntryItem()
        {
            List<IInstruction> instructionList = new List<IInstruction>();

            foreach (var item in m_restraintTableHash)
            {
                instructionList.Add(new RuleListEntryItemInstruction(".restraint_table_entry", item.Key, item.Value));
            }

            return instructionList;
        }
    }
}
