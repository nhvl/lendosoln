﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConfigSystem.Engine.Instructions
{
    public class ReturnInstruction : AbstractInstruction
    {

        protected override void CreateOpCodes()
        {
            Append(OpCode.ret_true, 0);
        }

        public override string ToString()
        {
            return ".ret_true";
        }

    }
}
