﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConfigSystem.Engine.Instructions
{
    public class TableEntryInstruction : IInstruction
    {
        private byte[] m_bytes;
        public E_TableEntryT Key { get; private set; }
        public TableEntryInstruction(E_TableEntryT key)
        {
            m_bytes = new byte[6];
            Key = key;
        }
        public byte[] GetBytes()
        {
            return m_bytes;
        }

        public void Set(int offset, ushort count)
        {
            Array.Copy(BitConverter.GetBytes(offset), 0, m_bytes, 0, 4);
            Array.Copy(BitConverter.GetBytes(count), 0, m_bytes, 4, 2);
        }

        public override string ToString()
        {
            return ".table_entry offset:" + BitConverter.ToInt32(m_bytes, 0) + ". Count=" + BitConverter.ToUInt16(m_bytes, 4);
        }
    }
}
