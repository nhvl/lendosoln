﻿namespace ConfigSystem.Engine
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Debug information for Custom Variable.
    /// </summary>
    public class TriggerDebug
    {
        public TriggerDebug(string triggerName)
        {
            this.TriggerName = triggerName;
        }

        public IReadOnlyCollection<ExecutingEngineConditionDebug> ConditionList
        {
            get { return m_conditionList; }
        }

        public void AddConditions(IEnumerable<ExecutingEngineConditionDebug> condition)
        {
            m_conditionList.AddRange(condition);
        }

        public bool Value { get; set; }

        public string TriggerName { get; private set; }

        private List<ExecutingEngineConditionDebug> m_conditionList = new List<ExecutingEngineConditionDebug>();
    }
}


