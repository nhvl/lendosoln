﻿using System;
using System.Collections.Generic;
using System.Linq;
using CommonProjectLib.Caching;
using CommonProjectLib.Common;
using CommonProjectLib.ConfigSystem;
using ConfigSystem.DataAccess;

namespace ConfigSystem.Engine
{
    public class ExecutingEngine : IVersionable
    {
        public const string CacheKeyPrefix = "__EXECUTINGENGINE__";

        public static readonly TimeSpan EngineCacheDuration = TimeSpan.FromMinutes(5);

        public static ExecutingEngine LoadFromConfigSystemXml(string xml)
        {
            SystemConfig config = new SystemConfig(xml);
            byte[] content = ExecutingEngineCompiler.Compile(config, DateTime.Now);
            return LoadFromContent(content);
        }
        public static ExecutingEngine LoadFromContent(byte[] content)
        {
            return new ExecutingEngine(new ExecutingEngineCompileCode(content));
        }
        public static ExecutingEngine GetEngineByBrokerId(IConfigRepository repository, Guid organizationId)
        {
            // 9/27/2010 dd - This method need to be thread safe because we could be switching executing engine when there is new version
            // available.
            var cacheKey = CacheKeyPrefix + organizationId.ToString("N");
            return MemoryCacheUtilities.GetOrAddExistingWithVersioning(
                cacheKey,
                () => repository.GetConfigVersion(organizationId),
                () => LoadExecutingEngineFromDatabase(repository, organizationId),
                EngineCacheDuration);
        }
        private static ExecutingEngine LoadExecutingEngineFromDatabase(IConfigRepository repository, Guid organizationId)
        {
            ExecutingEngine engine = null;

            ReleaseConfigData config = repository.LoadActiveRelease(organizationId);
            if (null != config)
            {
                if (config.CompiledConfiguration != null)
                {
                    try
                    {
                        ExecutingEngineCompileCode compileCode = new ExecutingEngineCompileCode(config.CompiledConfiguration);
                        engine = new ExecutingEngine(compileCode);
                    }
                    catch (ExecutingEngineVersionMismatchException)
                    {
                        // 10/26/2011 dd - We introduce a new compiler version. Therefore need to recompile the code again.
                        repository.SaveReleaseConfig(organizationId, config.Configuration, config.UserId.Value);

                        config = repository.LoadActiveRelease(organizationId); // Reload
                        engine = new ExecutingEngine(new ExecutingEngineCompileCode(config.CompiledConfiguration));
                    }
                }
            }
            if (engine != null)
            {
                engine.x_configRepository = repository;
                engine.x_organizationId = organizationId;
                engine.Version = config.Version;
            }
            return engine;
        }
        private ExecutingEngineCompileCode m_compileCode = null;
        private ExecutingEngineBoolValueGetter m_boolGetter = null;
        private ExecutingEngineIntValueGetter m_intByteGetter = null;
        private ExecutingEngineIntValueGetter m_intUShortGetter = null;
        private ExecutingEngineIntValueGetter m_intGetter = null;
        private ExecutingEngineFloatValueGetter m_floatGetter = null;
        private ExecutingEngineStringValueGetter m_stringGetter = null;
        private ExecutingEngineDateTimeValueGetter m_datetimeGetter = null;

        public DateTime ReleasedDate
        {
            get { return m_compileCode.ReleasedDate; }
        }
        public int Size { get { return m_compileCode.Size; } }

        public long Version { get; private set; } = ReleaseConfigData.DefaultDummyVersion;

        private ExecutingEngine(ExecutingEngineCompileCode compileCode)
        {
            m_compileCode = compileCode;
            m_intByteGetter = new ExecutingEngineIntValueGetter(1, compileCode);
            m_intUShortGetter = new ExecutingEngineIntValueGetter(2, compileCode);
            m_intGetter = new ExecutingEngineIntValueGetter(4, compileCode);
            m_floatGetter = new ExecutingEngineFloatValueGetter(compileCode);
            m_stringGetter = new ExecutingEngineStringValueGetter(compileCode);
            m_datetimeGetter = new ExecutingEngineDateTimeValueGetter(compileCode);
            m_boolGetter = new ExecutingEngineBoolValueGetter(compileCode);
        }

        #region Public Methods

        private IConfigRepository x_configRepository; // 11/18/2011 dd - Hold this reference to load system config later. DON'T USE THIS VARIABLE DIRECTLY.
        private Guid x_organizationId = Guid.Empty;
        public static readonly Guid SystemOrganizationId = new Guid("11111111-1111-1111-1111-111111111111"); // 11/18/2011 dd - Should move ConstAppDavid.SystemBrokerGuid here.

        private ExecutingEngine x_systemExecutingEngineForNUnit = null;
        private ExecutingEngine x_systemExecutingEngine = null;
        public ExecutingEngine SystemExecutingEngine
        {
            get 
            {
                if (x_systemExecutingEngineForNUnit == null)
                {
                    if (x_organizationId != SystemOrganizationId)
                    {
                        if (x_configRepository != null)
                        {
                            if (x_systemExecutingEngine == null)
                            {
                                // 8/25/2015 - dd - Cache The System Engine.
                                x_systemExecutingEngine = ExecutingEngine.LoadExecutingEngineFromDatabase(x_configRepository, SystemOrganizationId);
                            }
                            return x_systemExecutingEngine;
                        }
                    }
                    return null;
                }
                else
                {
                    return x_systemExecutingEngineForNUnit;
                }
                
            }
        }
        public void SetSystemExecutingEngineForNUnit(ExecutingEngine executingEngine)
        {
            x_systemExecutingEngineForNUnit = executingEngine;
        }

        public string DisplayDebugCompiledCode()
        {
            return m_compileCode.GenerateDebug();
        }
        public IEnumerable<string> GetOperationList()
        {
            HashSet<string> list = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
            foreach (var o in m_compileCode.ActionEntryList)
            {
                list.Add(o.Name);
            }

            foreach (var o in m_compileCode.RestraintEntryList)
            {
                list.Add(o.Name);
            }

            return list;
        }
        public IEnumerable<KeyValuePair<string, string>> GetVariableList()
        {
            List<KeyValuePair<string, string>> list = new List<KeyValuePair<string, string>>();

            foreach (var o in m_compileCode.VariableList)
            {
                list.Add(new KeyValuePair<string, string>(o.VariableName, o.Type.ToString()));
            }
            return list;
        }
        public bool HasOperation(ISystemOperationIdentifier operation)
        {
            return this.HasPrivilege(operation) || this.HasRestraint(operation);
        }

        public bool HasPrivilege(ISystemOperationIdentifier operation)
        {
            return m_compileCode.HasAction(operation.Id);
        }

        public bool HasRestraint(ISystemOperationIdentifier operation)
        {
            return m_compileCode.HasRestraint(operation.Id);
        }

        public bool HasCustomVariable(string customVariableName)
        {
            var ruleListEntry = m_compileCode.GetCustomVariableByName(customVariableName);
            return ruleListEntry != null;
        }

        public bool HasPassingPrivilege(ISystemOperationIdentifier operation, IEngineValueEvaluator ticket, ExecutingEngineDebug debug = null, 
                                        Dictionary<string, TriggerDebug> triggerDebugDict = null)
        {
            bool isDebugMode = debug != null;
            ExecutingEngineCache engineCache = new ExecutingEngineCache();

            bool result = isDebugMode ? debug.CanPerform : true;
            IEnumerable<int> privilegeRules = m_compileCode.GetRulesByAction(operation.Id);
            List<ExecutingEngineConditionDebug> privilegeDebug = isDebugMode ? new List<ExecutingEngineConditionDebug>() : null;
            result &= Evaluate(ticket, privilegeRules, privilegeDebug, engineCache, triggerDebugDict);

            if (isDebugMode)
            {
                debug.AddConditions(privilegeDebug);
            }

            return result;
        }

        public bool HasPassingRestraint(ISystemOperationIdentifier operation, IEngineValueEvaluator ticket, ExecutingEngineDebug debug = null,
                                        Dictionary<string, TriggerDebug> triggerDebugDict = null)
        {
            bool result = false;
            bool isDebugMode = debug != null;
            ExecutingEngineCache engineCache = new ExecutingEngineCache();

            IEnumerable<int> restraintRules = m_compileCode.GetRulesByRestraint(operation.Id);
            List<ExecutingEngineConditionDebug> restraintDebug = isDebugMode ? new List<ExecutingEngineConditionDebug>() : null;
            if (Evaluate(ticket, restraintRules, restraintDebug, engineCache, triggerDebugDict))
            {
                if (!isDebugMode)
                {
                    return true;
                }

                result = true;
            }

            if (isDebugMode)
            {
                debug.AddRestraints(restraintDebug);
            }

            return result;
        }

        public IEnumerable<string> GetDependencyVariableList(IEnumerable<ISystemOperationIdentifier> operationList)
        {
            return this.GetDependencyVariableList(operationList.Select(o => o.Id));
        }

        public IEnumerable<string> GetDependencyVariableList(IEnumerable<string> operationList)
        {
            SortedList<int, string> variableSortedList = new SortedList<int, string>();
            SortedList<int, bool> processedRuleList = new SortedList<int, bool>();
            List<string> systemVariableList = new List<string>();
            foreach (string operation in operationList)
            {
                IEnumerable<int> ruleList = m_compileCode.GetRulesByOperation(operation);

                foreach (var varIdx in GetVarIdxList(ruleList, processedRuleList, systemVariableList))
                {
                    VariableEntryType variable = m_compileCode.GetVariableByIndex(varIdx);

                    if (variableSortedList.ContainsKey(varIdx) == false)
                    {
                        variableSortedList.Add(varIdx, variable.VariableName);
                    }
                }
            }
            foreach (var o in variableSortedList.Values)
            {
                if (systemVariableList.Contains(o, StringComparer.OrdinalIgnoreCase) == false)
                {
                    systemVariableList.Add(o);
                }
            }
            return systemVariableList;
            //return variableSortedList.Values;
        }

        /// <summary>
        /// Return null if trigger is not found.
        /// </summary>
        /// <param name="triggerName">The name of the trigger.</param>
        /// <remarks>
        /// This method simply returns the list of variables from the byte code associated with the given trigger name.
        /// While it will recurse through nested Custom Variables, it does not attempt to parse dependencies for special
        /// fields, such as Data Paths, which are returned as they appear in the byte code.
        /// To get the full dependency set, including Path dependencies use LendingQBExecutingEngine.GetDependencyFieldsByTrigger instead.
        /// </remarks>
        /// <returns></returns>
        public IEnumerable<string> GetDependencyVariableListForTrigger(string triggerName)
        {
            RuleListEntryType ruleEntry = m_compileCode.GetCustomVariableByName(triggerName);

            if (ruleEntry == null)
            {
                return null;
            }
            SortedList<int, string> variableSortedList = new SortedList<int, string>();
            SortedList<int, bool> processedRuleList = new SortedList<int, bool>();
            List<string> systemVariableList = new List<string>();

            IEnumerable<int> ruleList = ruleEntry.RuleList;

            foreach (var varIdx in GetVarIdxList(ruleList, processedRuleList, systemVariableList))
            {
                VariableEntryType variable = m_compileCode.GetVariableByIndex(varIdx);

                if (variableSortedList.ContainsKey(varIdx) == false)
                {
                    variableSortedList.Add(varIdx, variable.VariableName);
                }
            }
            foreach (var o in variableSortedList.Values)
            {
                if (systemVariableList.Contains(o, StringComparer.OrdinalIgnoreCase) == false)
                {
                    if (o.Equals("branchgroup", StringComparison.OrdinalIgnoreCase))
                    {
                        systemVariableList.Add("sbranchid");
                    }
                    else if (o.Equals("ocgroup", StringComparison.OrdinalIgnoreCase))
                    {
                        systemVariableList.Add("spmlbrokerid");
                    }
                    else if (o.Equals("DoesConditionExist", StringComparison.OrdinalIgnoreCase))
                    {
                        systemVariableList.Add("sfGetDistinctConditionCategory");
                    }
                    else if (o.Equals("AreAllConditionsClosed", StringComparison.OrdinalIgnoreCase))
                    {
                        systemVariableList.Add("sfGetDistinctConditionCategoryWhereAllConditionsAreClosed");
                    }
                    else if (o.Equals("DoesActiveConditionExist", StringComparison.OrdinalIgnoreCase))
                    {
                        systemVariableList.Add("sfGetDistinctConditionCategoryWithActiveCondition");
                    }
                    else if (o.Equals("sLeadSrcIdEnum", StringComparison.OrdinalIgnoreCase))
                    {
                        systemVariableList.Add("sLeadSrcId");
                    }
                    else if (o.Equals("sStatusProgressT", StringComparison.OrdinalIgnoreCase))
                    {
                        systemVariableList.Add("sStatusT");
                    }
                    else
                    {
                        systemVariableList.Add(o);
                    }
                }
            }
            return systemVariableList;
        }
        public bool IsTriggerContainDateParameters(string triggerName)
        {
            // 1/4/2012 dd - Return true if at least one parameter in trigger name is DateTime.
            var variableList = GetDependencyVariableListForTrigger(triggerName);
            if (variableList != null)
            {
                var systemEngine = this.SystemExecutingEngine;

                foreach (var varName in variableList)
                {
                    var variable = m_compileCode.GetVariableByName(varName);

                    if (variable == null && systemEngine != null) 
                    {
                        variable = systemEngine.m_compileCode.GetVariableByName(varName);
                    }
                    if (variable != null)
                    {
                        if (variable.Type == SupportedType.DateTime)
                        {
                            return true;
                        }
                    }

                }
            }
            return false;
        }

        /// <summary>
        /// Gets the condition IDs (Guids) for all passing Privilege rules for the given operation.
        /// </summary>
        /// <remarks>Does NOT take Restraints into account.</remarks>
        /// <param name="operation">The operation being evaluated.</param>
        /// <param name="ticket">An engine value evaluator.</param>
        /// <returns>A list of Guid condition IDs.</returns>
        public IEnumerable<Guid> GetAllPassingPrivilegeConditionIds(ISystemOperationIdentifier operation, IEngineValueEvaluator ticket)
        {
            return this.GetAllPassingPrivilegeConditionIds(operation.Id, ticket);
        }

        /// <summary>
        /// Gets the condition IDs (Guids) for all passing Privilege rules for the given operation.
        /// </summary>
        /// <remarks>Does NOT take Restraints into account.</remarks>
        /// <param name="operation">The operation being evaluated.</param>
        /// <param name="ticket">An engine value evaluator.</param>
        /// <returns>A list of Guid condition IDs.</returns>
        public IEnumerable<Guid> GetAllPassingPrivilegeConditionIds(string operation, IEngineValueEvaluator ticket)
        {
            List<Guid> list = new List<Guid>();
            ExecutingEngineCache engineCache = new ExecutingEngineCache();
            foreach (var rule in m_compileCode.GetRulesByAction(operation))
            {
                if (EvaluateSingleCondition(rule, ticket, null, engineCache) == true)
                {
                    Guid conditionId = m_compileCode.GetConditionId(rule);
                    list.Add(conditionId);
                }
            }
            return list;
        }

        /// <summary>
        /// Gets the condition IDs (Guids) for all passing Restraint rules for the given operation.
        /// </summary>
        /// <remarks>NOTE: If there are any passing Restraints then the operation should be blocked.</remarks>
        /// <param name="operation">The operation being evaluated.</param>
        /// <param name="ticket">An engine value evaluator.</param>
        /// <returns>A list of Guid condition IDs.</returns>
        public IEnumerable<Guid> GetAllPassingRestraintConditionIds(ISystemOperationIdentifier operation, IEngineValueEvaluator ticket)
        {
            return this.GetAllPassingRestraintConditionIds(operation.Id, ticket);
        }

        /// <summary>
        /// Gets the condition IDs (Guids) for all passing Restraint rules for the given operation.
        /// </summary>
        /// <remarks>NOTE: If there are any passing Restraints then the operation should be blocked.</remarks>
        /// <param name="operation">The operation being evaluated.</param>
        /// <param name="ticket">An engine value evaluator.</param>
        /// <returns>A list of Guid condition IDs.</returns>
        public IEnumerable<Guid> GetAllPassingRestraintConditionIds(string operation, IEngineValueEvaluator ticket)
        {
            List<Guid> list = new List<Guid>();
            ExecutingEngineCache engineCache = new ExecutingEngineCache();
            foreach (var rule in m_compileCode.GetRulesByRestraint(operation))
            {
                if (EvaluateSingleCondition(rule, ticket, null, engineCache))
                {
                    Guid conditionId = m_compileCode.GetConditionId(rule);
                    list.Add(conditionId);
                }
            }
            return list;
        }
        #endregion

        private IEnumerable<int> GetVarIdxList(IEnumerable<int> ruleList, SortedList<int, bool> processedRuleList, List<string> systemVariableList)
        {
            List<int> list = new List<int>();

            foreach (var o in ruleList)
            {

                int offset = o;
                if (processedRuleList.ContainsKey(offset))
                {
                    continue;
                }
                else
                {
                    processedRuleList.Add(offset, true);
                }
                bool isDone = false;
                while (isDone == false)
                {
                    int varIdx = -1;
                    OpCodeType opCode = m_compileCode.GetOpCode(ref offset);

                    switch (opCode.OpCode)
                    {
                        case OpCode.eq_0_15:
                        case OpCode.eq_16_31:
                            varIdx = m_compileCode.GetInt(ref offset, 2);
                            break;
                        case OpCode.between:
                            varIdx = GetVarIdxForBetweenOpCode(ref offset, opCode.OpCodeSupportedType);
                            break;
                        case OpCode.in_byte:
                            varIdx = GetVarIdxForInOpCode(ref offset, opCode.OpParameter, 1);
                            break;
                        case OpCode.in_ushort:
                        case OpCode.in_string:
                            varIdx = GetVarIdxForInOpCode(ref offset, opCode.OpParameter, 2);
                            break;
                        case OpCode.in_int:
                        case OpCode.in_float:
                            varIdx = GetVarIdxForInOpCode(ref offset, opCode.OpParameter, 4);
                            break;
                        case OpCode.in_datetime:
                            varIdx = GetVarIdxForInOpCode(ref offset, opCode.OpParameter, 8);
                            break;
                        case OpCode.comparison_op:
                            varIdx = GetVarIdxForComparisonOpCode(ref offset);
                            break;
                        case OpCode.comparison_var:
                            GetVarIndexList(ref offset, list);
                            break;
                        case OpCode.custom_var:
                            GetCustomVarIndexList(ref offset, opCode.OpParameter, list, processedRuleList, systemVariableList);
                            break;
                        case OpCode.label:
                            offset += 2;
                            break;
                        case OpCode.ret_true:
                            isDone = true;
                            break;
                        default:
                            throw CBaseException.CreateUnexpectedEnumException(opCode.OpCode);
                    }
                    if (isDone == false && varIdx >= 0)
                    {
                        list.Add(varIdx);
                    }
                }
            }
            return list;
        }
        private void GetVarIndexList(ref int offset, List<int> list)
        {
            int lhsVarIdx = m_compileCode.GetVarIndex(ref offset);
            int rhsVarIdx = m_compileCode.GetVarIndex(ref offset);
            list.Add(lhsVarIdx);
            list.Add(rhsVarIdx);

        }

        private void GetCustomVarIndexListByName(string varName, List<string> variableNameList)
        {
            foreach (var customVariable in m_compileCode.CustomVariableEntryList)
            {
                if (customVariable.Name.Equals(varName, StringComparison.OrdinalIgnoreCase))
                {
                    IEnumerable<int> tempVariableList = GetVarIdxList(customVariable.RuleList, new SortedList<int, bool>(), new List<string>());

                    foreach (var o in tempVariableList)
                    {
                        var v = m_compileCode.GetVariableByIndex(o);
                        if (variableNameList.Contains(v.VariableName, StringComparer.OrdinalIgnoreCase) == false)
                        {
                            variableNameList.Add(m_compileCode.GetVariableByIndex(o).VariableName);
                        }
                    }
                    //IEnumerable<
                    
                    //ExecutingEngineCache engineCache = new ExecutingEngineCache();
                    //return Evaluate(ticket, customVariable.RuleList, null, engineCache);
                }
            }
        }
        private void GetCustomVarIndexList(ref int offset, int opParameter, List<int> list, SortedList<int, bool> processedRuleList, List<string> systemVariableNameList)
        {
            int customVariableIndex = m_compileCode.GetVarIndex(ref offset);
            string customVariableName = "";


            if (opParameter == 1 || opParameter == 0)
            {
                IEnumerable<int> ruleList = m_compileCode.GetCustomVariableByIndex(customVariableIndex).RuleList;

                IEnumerable<int> tempVariableList = GetVarIdxList(ruleList, processedRuleList, systemVariableNameList);
                foreach (var o in tempVariableList)
                {
                    list.Add(o);
                }

            }
            else if (opParameter == 2 || opParameter == 3)
            {
                customVariableName = m_compileCode.GetStringByIdx(customVariableIndex);
                // Need to evaluate system custom variables.

                if (SystemExecutingEngine != null)
                {
                    SystemExecutingEngine.GetCustomVarIndexListByName(customVariableName, systemVariableNameList);
                }
                else
                {
                    
                    IEnumerable<int> ruleList = m_compileCode.GetCustomVariableByName(customVariableName).RuleList;

                    IEnumerable<int> tempVariableList = GetVarIdxList(ruleList, processedRuleList, systemVariableNameList);
                    foreach (var o in tempVariableList)
                    {
                        list.Add(o);
                    }

                }
            }
            else
            {
                throw new NotImplementedException("OpParameter=" + opParameter + " is not valid for CustomVariable.");
            }

        }
        private int GetVarIdxForBetweenOpCode( ref int offset, SupportedType type)
        {
            int varIdx = m_compileCode.GetVarIndex(ref offset);

            switch (type)
            {
                case SupportedType.Byte:
                    offset += 2;
                    break;
                case SupportedType.UShort:
                    offset += 4;
                    break;
                case SupportedType.Int:
                case SupportedType.Float:
                    offset += 8;
                    break;
                case SupportedType.DateTime:
                    offset += 16;
                    break;
                default:
                    throw CBaseException.CreateUnexpectedEnumException(type);
            }
            return varIdx;
        }

        private int GetVarIdxForInOpCode(ref int offset, int opParameter, int size)
        {
            int length = opParameter == 0 ? m_compileCode.GetInt(ref offset, 1) : opParameter;

            int varIdx = m_compileCode.GetVarIndex(ref offset);

            offset += (size * length);
            return varIdx;
        }
        private int GetVarIdxForComparisonOpCode(ref int offset)
        {
            VariableEntryType variable = m_compileCode.GetVariable(ref offset);

            SupportedType type = variable.Type;
            switch (type)
            {
                case SupportedType.Bool:
                case SupportedType.Int:
                case SupportedType.Float:
                    offset += 4;
                    break;
                case SupportedType.String:
                    offset += 2;
                    break;
                case SupportedType.DateTime:
                    offset += 8;
                    break;
                default:
                    throw CBaseException.CreateUnexpectedEnumException(type);
            }
            return variable.VarIndex;
        }

        private bool CompareOp<T>(IList<T> lhsValueList, ComparisonOpCode op, T rhsValue, AbstractExecutingEngineValueGetter<T> getter, ref E_FunctionT function) where T : IComparable<T>
        {
            function = E_FunctionT.In;
            bool bExpected;

            switch (op)
            {
                case ComparisonOpCode.equal:
                    function = E_FunctionT.Equal;
                    return lhsValueList.Any(o => o.Equals(rhsValue));
                case ComparisonOpCode.not_equal:
                    // 9/28/2010 dd - Not sure what function to pick here. Because the not equal of is not support in System Config.
                    // The reason for this op.not_equal is due to IsNonblankStr.
                    function = E_FunctionT.Equal;
                    return lhsValueList.Any(o => o.Equals(rhsValue) == false);
                case ComparisonOpCode.greater:
                    function = E_FunctionT.IsGreaterThan;
                    return lhsValueList.Any(o => o.CompareTo(rhsValue) > 0);
                case ComparisonOpCode.greater_or_equal:
                    function = E_FunctionT.IsGreaterThanOrEqualTo;
                    return lhsValueList.Any(o => o.CompareTo(rhsValue) >= 0);
                case ComparisonOpCode.less_than:
                    function = E_FunctionT.IsLessThan;
                    return lhsValueList.Any(o => o.CompareTo(rhsValue) < 0);
                case ComparisonOpCode.less_than_or_equal:
                    function = E_FunctionT.IsLessThanOrEqualTo;
                    return lhsValueList.Any(o => o.CompareTo(rhsValue) <= 0);
                case ComparisonOpCode.is_non_default_value:
                    bExpected = false;
                    if (typeof(T) == typeof(string))
                    {
                        function = E_FunctionT.IsNonblankStr;
                        string s = rhsValue.ToString();
                        bExpected = bool.Parse(s);

                    }
                    else if (typeof(T) == typeof(int) || typeof(T) == typeof(float))
                    {
                        function = E_FunctionT.IsNonzeroValue;
                        string s = rhsValue.ToString();
                        bExpected = s == "1";
                    }
                    else if (typeof(T) == typeof(DateTime))
                    {
                        function = E_FunctionT.IsNonzeroValue;
                        bExpected = Convert.ToDateTime(rhsValue).Ticks == 1;
                    }
                    else
                    {
                        throw CBaseException.CreateGeneric("Unhandled type:" + typeof(T).FullName + " for is_non_default_value op");
                    }
                    if (lhsValueList.Count == 0)
                    {
                        return bExpected == false;
                    }
                    else
                    {
                        return lhsValueList.Any(o => getter.IsNonDefaultValue(o) == bExpected);
                    }
                case ComparisonOpCode.is_valid_value:
                    bExpected = false;

                    if (typeof(T) == typeof(int) || typeof(T) == typeof(float))
                    {
                        function = E_FunctionT.IsAnyValue;
                        string s = rhsValue.ToString();
                        bExpected = s == "1";
                    }
                    else if (typeof(T) == typeof(DateTime))
                    {
                        function = E_FunctionT.IsNonzeroValue;
                        bExpected = Convert.ToDateTime(rhsValue).Ticks == 1;
                    }
                    else
                    {
                        throw CBaseException.CreateGeneric("Unhandled type:" + typeof(T).FullName + " for is_non_default_value op");
                    }
                    // 9/30/2013 dd - Check to see if the actual values list contains any valid value.
                    return lhsValueList.Any(o => getter.IsValidValue(o)) == bExpected;
                default:
                    throw CBaseException.CreateUnexpectedEnumException(op);
            }

            throw new Exception("Unhandled " + op);
        }

        /// <summary>
        /// Compares to variable fields based on operation.
        /// </summary>
        /// <typeparam name="T">Type of fields being compared.</typeparam>
        /// <param name="lhsValueList">List of field value the RHS value is being compared against.</param>
        /// <param name="op">Comparison operation.</param>
        /// <param name="rhsValue">RHS value of comparison. Value being compared against.</param>
        /// <param name="function">Reference to function type. Set during call.</param>
        /// <returns>True if one of the comparisons made against the LHS values list is true. False otherwise.</returns>
        private bool CompareVarOp<T>(IList<T> lhsValueList, ComparisonOpCode op, T rhsValue, ref E_FunctionT function) where T : IComparable<T>
        {
            function = E_FunctionT.In;
            switch (op)
            {
                case ComparisonOpCode.equal:
                    function = E_FunctionT.Equal;
                    return lhsValueList.Any(o => this.EqualsWithDateTimeConsiderations(o, rhsValue));
                case ComparisonOpCode.not_equal:
                    // 9/28/2010 dd - Not sure what function to pick here. Because the not equal of is not support in System Config.
                    // The reason for this op.not_equal is due to IsNonblankStr.
                    function = E_FunctionT.Equal;
                    return lhsValueList.Any(o => !this.EqualsWithDateTimeConsiderations(o, rhsValue));
                case ComparisonOpCode.greater:
                    function = E_FunctionT.IsGreaterThan;
                    return lhsValueList.Any(o => { int res = this.CompareToWithDateTimeConsiderations(o, rhsValue); return res != int.MinValue && res > 0; });
                case ComparisonOpCode.greater_or_equal:
                    function = E_FunctionT.IsGreaterThanOrEqualTo;
                    return lhsValueList.Any(o => { int res = this.CompareToWithDateTimeConsiderations(o, rhsValue); return res != int.MinValue && res >= 0; });
                case ComparisonOpCode.less_than:
                    function = E_FunctionT.IsLessThan;
                    return lhsValueList.Any(o => { int res = this.CompareToWithDateTimeConsiderations(o, rhsValue); return res != int.MinValue && res < 0; });
                case ComparisonOpCode.less_than_or_equal:
                    function = E_FunctionT.IsLessThanOrEqualTo;
                    return lhsValueList.Any(o => { int res = this.CompareToWithDateTimeConsiderations(o, rhsValue); return res != int.MinValue && res <= 0; });
                default:
                    throw CBaseException.CreateUnexpectedEnumException(op);
            }

             throw new Exception("Unhandled " + op);
        }

        /// <summary>
        /// Checks if lhsValue and rhsValue are blank values. Adjust blank values to DateTime.MaxValue.
        /// </summary>
        /// <typeparam name="T">Assumed to be DateTime.</typeparam>
        /// <param name="lhsValue">Left hand side value.</param>
        /// <param name="rhsValue">Right hand side value.</param>
        /// <returns>True if both lhs and rhs are blank. False otherwise.</returns>
        /// <remarks>Does not adjust blanks to DateTime.MaxValue if both values are blank (just returns true in that case).</remarks>
        private bool CheckAndFixBlankDateTimes<T>(ref T lhsValue, ref T rhsValue)
        {
            DateTime convertedLhsValue = Convert.ToDateTime(lhsValue);
            DateTime convertedRhsValue = Convert.ToDateTime(rhsValue);

            // Return true if both lhs and rhs are blank.
            if (convertedLhsValue == DateTime.MinValue && convertedRhsValue == DateTime.MinValue)
            {
                return true;
            }

            // If we are here then at most only one side is blank. Blank should evaluate as greater than non-blank for DateTimes.
            // To make things easier for us, just convert DateTime.MinValue to DateTime.MaxValue.
            lhsValue = convertedLhsValue == DateTime.MinValue ? (T)(object)DateTime.MaxValue : lhsValue;
            rhsValue = convertedRhsValue == DateTime.MinValue ? (T)(object)DateTime.MaxValue : rhsValue;

            return false;
        }

        /// <summary>
        /// Special Equals function that handles blank DateTimes in accordance with OPM 238673.
        /// </summary>
        /// <typeparam name="T">A generic type.</typeparam>
        /// <param name="lhsValue">Left hand side value.</param>
        /// <param name="rhsValue">Right hand side value.</param>
        /// <returns>
        /// True if lhs is equal to rhs. False otherwise.
        /// However, returns false if both parameters are of type DateTime and are both blank (DateTime.MinValue).
        /// </returns>
        private bool EqualsWithDateTimeConsiderations<T>(T lhsValue, T rhsValue) where T : IComparable<T>
        {
            // OPM 238673 - Return false if both lhs and rhs are blank DateTimes.
            if (typeof(T) == typeof(DateTime) && this.CheckAndFixBlankDateTimes(ref lhsValue, ref rhsValue))
            {
                return false;
            }

            return lhsValue.Equals(rhsValue);
        }

        /// <summary>
        /// Special CompareTo function that handles blank DateTimes in accordance with OPM 238673.
        /// </summary>
        /// <typeparam name="T">A generic type.</typeparam>
        /// <param name="lhsValue">Left hand side value.</param>
        /// <param name="rhsValue">Right hand side value.</param>
        /// <returns>
        /// -1, 0, or 1 depending on whether the lhs is less than, equal to, or greater than rhs.
        /// Or int.MinValue to indicate that the parameters are of type DateTime and both parameters are blank (DateTime.MinValue).
        /// </returns>
        private int CompareToWithDateTimeConsiderations<T>(T lhsValue, T rhsValue) where T : IComparable<T>
        {
            // OPM 238673 - Return false if both lhs and rhs are blank DateTimes.
            if (typeof(T) == typeof(DateTime) && this.CheckAndFixBlankDateTimes(ref lhsValue, ref rhsValue))
            {
                return int.MinValue;
            }


            // Since CompareTo can technically return any int, explicitly return -1,0,1
            // so that there is no chance collision with our int.MinValue special value.
            int comp = lhsValue.CompareTo(rhsValue);
            if (comp > 0)
            {
                return 1;
            }
            else if (comp < 0)
            {
                return -1;
            }
            else
            {
                return 0;
            }
        }

        private bool Evaluate(IEngineValueEvaluator ticket, IEnumerable<int> ruleList, ICollection<ExecutingEngineConditionDebug> debugList, 
                              ExecutingEngineCache engineCache, Dictionary<string, TriggerDebug> triggerDebugDict = null)
        {
            bool isDebugMode = debugList != null;
            bool result = false;

            foreach (var rule in ruleList)
            {
                if (EvaluateSingleCondition(rule, ticket, debugList, engineCache, triggerDebugDict) == true)
                {
                    if (isDebugMode)
                    {
                        result = true;
                    }
                    else
                    {
                        // 10/28/2011 dd - Return true as long as one condition match.
                        return true;
                    }
                }
            }

            return result;
        }

        private bool EvaluateSingleCondition(int rule, IEngineValueEvaluator ticket, ICollection<ExecutingEngineConditionDebug> debugList, 
                                             ExecutingEngineCache engineCache, Dictionary<string, TriggerDebug> triggerDebugDict = null)
        {
            bool isDebugMode = debugList != null;

            int offset = rule; // Need to copy to local variable in order for 'ref' to work.

            bool isDone = false;

            bool bIsAllow = true; // Assume this rule will pass.
            ExecutingEngineConditionDebug conditionDebug = null;

            if (isDebugMode)
            {
                Guid conditionId = m_compileCode.GetConditionId(offset);

                conditionDebug = new ExecutingEngineConditionDebug(conditionId);
            }
            else
            {
                // Only cache rule result when not run in debug mode.
                bool b;
                if (engineCache.RuleResultHash.TryGetValue(offset, out b))
                {
                    bIsAllow = b;
                    isDone = true; // There is result in cache move to next rule.
                }

            }
            while (isDone == false)
            {
                OpCodeType opCode = m_compileCode.GetOpCode(ref offset);

                bool bResult = false;
                #region Main Evaluation.
                switch (opCode.OpCode)
                {
                    case OpCode.eq_0_15:
                        bResult = EvaluateEq0_31(ref offset, opCode.OpParameter, ticket, conditionDebug, engineCache);
                        break;
                    case OpCode.eq_16_31:
                        bResult = EvaluateEq0_31(ref offset, opCode.OpParameter + 16, ticket, conditionDebug, engineCache);
                        break;
                    case OpCode.between:
                        bResult = EvaluateBetween(ref offset, opCode.OpCodeSupportedType, ticket, conditionDebug, engineCache);
                        break;
                    case OpCode.in_byte:
                        bResult = EvaluateIn(ref offset, opCode.OpParameter, ticket, m_intByteGetter, conditionDebug, engineCache.IntValueHash);
                        break;
                    case OpCode.in_ushort:
                        bResult = EvaluateIn(ref offset, opCode.OpParameter, ticket, m_intUShortGetter, conditionDebug, engineCache.IntValueHash);
                        break;
                    case OpCode.in_int:
                        bResult = EvaluateIn(ref offset, opCode.OpParameter, ticket, m_intGetter, conditionDebug, engineCache.IntValueHash);
                        break;
                    case OpCode.in_string:
                        bResult = EvaluateIn(ref offset, opCode.OpParameter, ticket, m_stringGetter, conditionDebug, engineCache.StringValueHash);
                        break;
                    case OpCode.in_float:
                        bResult = EvaluateIn(ref offset, opCode.OpParameter, ticket, m_floatGetter, conditionDebug, engineCache.FloatValueHash);
                        break;
                    case OpCode.in_datetime:
                        bResult = EvaluateIn(ref offset, opCode.OpParameter, ticket, m_datetimeGetter, conditionDebug, engineCache.DateTimeValueHash);
                        break;
                    case OpCode.comparison_op:
                        bResult = EvaluateComparison(ref offset, opCode.ComparisonOpCode, ticket, conditionDebug, engineCache);
                        break;
                    case OpCode.comparison_var:
                        bResult = EvaluateComparisonVar(ref offset, opCode.ComparisonOpCode, ticket, conditionDebug, engineCache);
                        break;
                    case OpCode.custom_var:
                        bResult = EvaluateCustomVar(ref offset, opCode.OpParameter, ticket, conditionDebug, engineCache, triggerDebugDict);
                        break;
                    case OpCode.label:
                        bResult = true;
                        offset += 2; // Next two bytes are GUID index.
                        break;
                    case OpCode.ret_true:
                        bResult = true;
                        isDone = true;
                        break;
                    default:
                        throw CBaseException.CreateUnexpectedEnumException(opCode.OpCode);
                }
                #endregion

                if (bResult == false)
                {
                    bIsAllow = false;

                    if (isDebugMode == false)
                    {
                        // 9/16/2010 dd - Break executing here when we are not running debug mode.
                        isDone = true;
                    }
                }
            }
            if (isDebugMode)
            {
                debugList.Add(conditionDebug);
            }
            else
            {
                if (engineCache.RuleResultHash.ContainsKey(rule) == false)
                {
                    engineCache.RuleResultHash.Add(rule, bIsAllow);
                }
            }

            return bIsAllow;

        }
        private bool EvaluateComparisonOp<T>(ref int offset, VariableEntryType var, ComparisonOpCode op, 
            IEngineValueEvaluator ticket, AbstractExecutingEngineValueGetter<T> getter, 
            ExecutingEngineConditionDebug conditionDebug, Dictionary<int, IList<T>> cache) where T : IComparable<T>
        {
            if (var.IsLookAtNewData)
            {
                if (ticket.IsDataModified(var.VariableName) == false)
                {
                    // 3/21/2014 dd - Data is not modify therefore return false.
                    // We only want to perform comparision operation if data is modify.
                    return false;
                }
            }
            T rhsValue = getter.GetExpectedValue(offset);

            offset += getter.Size;

            IList<T> actualValueList = getter.GetActualValue(ticket, var.VarIndex, cache);

            E_FunctionT function = E_FunctionT.In;

            bool bRet = CompareOp(actualValueList, op, rhsValue, getter, ref function);

            if (conditionDebug != null)
            {
                conditionDebug.Add(bRet, var.VariableName, function, new T[] { rhsValue }, actualValueList);
            }
            return bRet;
        }

        private bool EvaluateComparisonVarOp<T>(VariableEntryType lhsVar, ComparisonOpCode op, VariableEntryType rhsVar,
            IEngineValueEvaluator ticket, AbstractExecutingEngineValueGetter<T> getter,
            ExecutingEngineConditionDebug conditionDebug, Dictionary<int, IList<T>> cache) where T : IComparable<T>
        {
            bool canApplyDateTimeLogic = lhsVar.Type == SupportedType.DateTime && op != ComparisonOpCode.is_non_default_value && op != ComparisonOpCode.is_valid_value;

            IList<T> rhsValueList = getter.GetActualValue(ticket, rhsVar.VarIndex, cache);

            if (rhsValueList.Count != 1 && (!canApplyDateTimeLogic || rhsValueList.Count != 0)) // OPM 238673 - Don't throw for blank DateTime Objects.
            {
                throw new Exception("Unable to evaluate " + op + " for lhsVar=" + lhsVar.VariableName + " and rhsVar=" + rhsVar.VariableName);
            }

            // OPM 238673 - if rhsValueList is empty, add DateTime.MinValue (same as blank date).
            if (canApplyDateTimeLogic && rhsValueList.Count == 0)
            {
                rhsValueList.Add((T)(object)DateTime.MinValue);
            }

            T rhsValue;
            rhsValue = rhsValueList[0];

            if (lhsVar.IsLookAtNewData)
            {
                if (ticket.IsDataModified(lhsVar.VariableName) == false)
                {
                    // 3/21/2014 dd - Data is not modify therefore return false.
                    // We only want to perform comparision operation if data is modify.
                    return false;
                }
            }

            IList<T> lhsValueList = getter.GetActualValue(ticket, lhsVar.VarIndex, cache);

            // OPM 238673 - if lhsValueList is empty, add DateTime.MinValue (same as blank date).
            if (canApplyDateTimeLogic && lhsValueList.Count == 0)
            {
                lhsValueList.Add((T)(object)DateTime.MinValue);
            }

            E_FunctionT function = E_FunctionT.In;
            bool bRet = CompareVarOp(lhsValueList, op, rhsValue, ref function);

            return bRet;
        }

        public bool EvaluateCustomVar(string varName, IEngineValueEvaluator ticket)
        {
            foreach (var customVariable in m_compileCode.CustomVariableEntryList)
            {
                if (customVariable.Name.Equals(varName, StringComparison.OrdinalIgnoreCase))
                {
                    ExecutingEngineCache engineCache = new ExecutingEngineCache();
                    return Evaluate(ticket, customVariable.RuleList, null, engineCache);
                }
            }
            return false;
        }
        private bool EvaluateCustomVar(ref int offset, int opParameter, IEngineValueEvaluator ticket, ExecutingEngineConditionDebug conditionDebug, ExecutingEngineCache engineCache,
                                       Dictionary<string, TriggerDebug> triggerDebugDict = null)
        {
            List<ExecutingEngineConditionDebug> customVarDebug = null;
            Func<string, List<ExecutingEngineConditionDebug>> initDebugTrigger = (string triggerName) =>
            {
                if(conditionDebug != null && triggerDebugDict != null && !triggerDebugDict.ContainsKey(triggerName))
                {
                    customVarDebug = new List<ExecutingEngineConditionDebug>();
                    triggerDebugDict.Add(triggerName, new TriggerDebug(triggerName));
                }
                return customVarDebug;
            };

            int customVariableIndex = m_compileCode.GetVarIndex(ref offset);
            string customVariableName = "";

            bool rhsValue = false;
            bool lhsValue = false;

            if (opParameter == 1 || opParameter == 0)
            {
                RuleListEntryType customVariable = m_compileCode.GetCustomVariableByIndex(customVariableIndex);
                customVariableName = customVariable.Name;
                IEnumerable<int> ruleList = customVariable.RuleList;

                rhsValue = opParameter == 1;
                lhsValue = Evaluate(ticket, ruleList, initDebugTrigger(customVariableName), engineCache);
            }
            else if (opParameter == 2 || opParameter == 3)
            {
                rhsValue = opParameter == 3;
                customVariableName = m_compileCode.GetStringByIdx(customVariableIndex);
                // Need to evaluate system custom variables.

                if (SystemExecutingEngine != null)
                {
                    lhsValue = SystemExecutingEngine.EvaluateCustomVar(customVariableName, ticket);
                }
                else
                {
                    // 11/18/2011 dd - If system executing engine is not define then try to execute as normal.
                    // custom variable. This could happen if we try to execute system custom variable in system 
                    // executing engine.
                    RuleListEntryType customVariable = m_compileCode.GetCustomVariableByName(customVariableName);
                    customVariableName = customVariable.Name;
                    IEnumerable<int> ruleList = customVariable.RuleList;

                    lhsValue = Evaluate(ticket, ruleList, initDebugTrigger(customVariableName), engineCache);
                }
            }
            else
            {
                throw new NotImplementedException("OpParameter=" + opParameter + " is not valid for CustomVariable.");
            }
            bool bRet = lhsValue == rhsValue;

            if (conditionDebug != null)
            {
                conditionDebug.Add(bRet, customVariableName, E_FunctionT.Equal, new bool[] { rhsValue }, new bool[] { lhsValue });
                if(customVarDebug != null)
                {
                    triggerDebugDict[customVariableName].AddConditions(customVarDebug);
                    triggerDebugDict[customVariableName].Value = lhsValue;
                }
            }
            return bRet;
        }

        private bool EvaluateComparison(ref int offset, ComparisonOpCode op, IEngineValueEvaluator ticket, 
            ExecutingEngineConditionDebug conditionDebug, ExecutingEngineCache engineCache)
        {
            VariableEntryType var = m_compileCode.GetVariable(ref offset);

            SupportedType type = var.Type;

            switch (type)
            {
                case SupportedType.Bool:
                    return EvaluateComparisonOp(ref offset, var, op, ticket, m_boolGetter, conditionDebug, engineCache.BoolValueHash);
                case SupportedType.Int:
                    return EvaluateComparisonOp(ref offset, var, op, ticket, m_intGetter, conditionDebug, engineCache.IntValueHash);
                case SupportedType.String:
                    return EvaluateComparisonOp(ref offset, var, op, ticket, m_stringGetter, conditionDebug, engineCache.StringValueHash);
                case SupportedType.Float:
                    return EvaluateComparisonOp(ref offset, var, op, ticket, m_floatGetter, conditionDebug, engineCache.FloatValueHash);
                case SupportedType.DateTime:
                    return EvaluateComparisonOp(ref offset, var, op, ticket, m_datetimeGetter, conditionDebug, engineCache.DateTimeValueHash);
                default:
                    throw CBaseException.CreateUnexpectedEnumException(type);
            }
        }
        private bool EvaluateComparisonVar(ref int offset, ComparisonOpCode op, IEngineValueEvaluator ticket, ExecutingEngineConditionDebug conditionDebug, ExecutingEngineCache engineCache)
        {
            VariableEntryType lhsVar = m_compileCode.GetVariable(ref offset);
            VariableEntryType rhsVar = m_compileCode.GetVariable(ref offset);

            if (lhsVar.Type != rhsVar.Type)
            {
                throw new Exception("lhs Type does not match with rhs type in Comparison Var. lhsType=" + lhsVar.Type + ", rhsType=" + rhsVar.Type);
            }

            bool ret = false; 
            
            switch (lhsVar.Type)
            {
                case SupportedType.Bool:
                    ret = EvaluateComparisonVarOp(lhsVar, op, rhsVar, ticket, m_boolGetter, conditionDebug, engineCache.BoolValueHash);
                    break;
                case SupportedType.Int:
                    ret = EvaluateComparisonVarOp(lhsVar, op, rhsVar, ticket, m_intGetter, conditionDebug, engineCache.IntValueHash);
                    break;
                case SupportedType.String:
                    ret = EvaluateComparisonVarOp(lhsVar, op, rhsVar, ticket, m_stringGetter, conditionDebug, engineCache.StringValueHash);
                    break;
                case SupportedType.Float:
                    ret = EvaluateComparisonVarOp(lhsVar, op, rhsVar, ticket, m_floatGetter, conditionDebug, engineCache.FloatValueHash);
                    break;
                case SupportedType.DateTime:
                    ret = EvaluateComparisonVarOp(lhsVar, op, rhsVar, ticket, m_datetimeGetter, conditionDebug, engineCache.DateTimeValueHash);
                    break;
                default:
                    throw CBaseException.CreateUnexpectedEnumException(lhsVar.Type);
            }

            //todo fix this to actually return the actual/expected values;
            if (conditionDebug != null)
            {
                conditionDebug.Add(ret, lhsVar.VariableName, E_FunctionT.EqualVar, new List<string> { rhsVar.VariableName }, new List<string> { lhsVar.VariableName });
            }

                return ret;

        }
        private bool EvaluateEq0_31(ref int offset, int opParameter, IEngineValueEvaluator ticket, ExecutingEngineConditionDebug conditionDebug, ExecutingEngineCache engineCache)
        {

            VariableEntryType var = m_compileCode.GetVariable(ref offset);
            int varIdx = var.VarIndex;

            IList<int> actualValueList = null;
            switch (var.Type)
            {
                case SupportedType.Bool:
                    IList<bool> boolList = m_boolGetter.GetActualValue(ticket, varIdx, engineCache.BoolValueHash);

                    actualValueList = new List<int>(boolList.Count);
                    foreach (var b in boolList)
                    {
                        actualValueList.Add(b ? 1 : 0);
                    }
                    break;
                case SupportedType.Int:
                    actualValueList = m_intGetter.GetActualValue(ticket, varIdx, engineCache.IntValueHash);
                    break;
                default:
                    throw CBaseException.CreateUnexpectedEnumException(var.Type);
            }

            bool bRet = actualValueList.Any(o => o == opParameter);

            if (var.IsNotOperation)
            {
                bRet = !bRet;
            }

            if (conditionDebug != null)
            {
                conditionDebug.Add(bRet, var.VariableName, E_FunctionT.Equal, new int[] { opParameter }, actualValueList);
            }

            return bRet;
        }

        private bool EvaluateBetween(ref int offset, SupportedType type, IEngineValueEvaluator ticket, ExecutingEngineConditionDebug conditionDebug, ExecutingEngineCache engineCache)
        {
            switch (type)
            {
                case SupportedType.Byte:
                    return EvaluateBetween(ref offset, ticket, m_intByteGetter, conditionDebug, engineCache.IntValueHash);
                case SupportedType.UShort:
                    return EvaluateBetween(ref offset, ticket, m_intUShortGetter, conditionDebug, engineCache.IntValueHash);
                case SupportedType.String:
                    return EvaluateBetween(ref offset, ticket, m_stringGetter, conditionDebug, engineCache.StringValueHash);
                case SupportedType.Int:
                    return EvaluateBetween(ref offset, ticket, m_intGetter, conditionDebug, engineCache.IntValueHash);
                case SupportedType.Float:
                    return EvaluateBetween(ref offset, ticket, m_floatGetter, conditionDebug, engineCache.FloatValueHash);
                case SupportedType.DateTime:
                    return EvaluateBetween(ref offset, ticket, m_datetimeGetter, conditionDebug, engineCache.DateTimeValueHash);
                default:
                    throw CBaseException.CreateUnexpectedEnumException(type);
            }

        }
        private bool EvaluateBetween<T>(ref int offset, IEngineValueEvaluator ticket, 
            AbstractExecutingEngineValueGetter<T> getter, 
            ExecutingEngineConditionDebug conditionDebug, Dictionary<int, IList<T>> cache) where T : IComparable<T>
        {
            VariableEntryType var = m_compileCode.GetVariable(ref offset);

            int varIndex = var.VarIndex;

            IList<T> actualValueList = getter.GetActualValue(ticket, varIndex, cache);

            bool bRet = false;
            int size = getter.Size;

            T minValue = getter.GetExpectedValue(offset);
            T maxValue = getter.GetExpectedValue(offset + size);

            T[] expectedValueList = { minValue, maxValue };

            foreach (var actualValue in actualValueList)
            {

                bRet = actualValue.CompareTo(minValue) >= 0 && actualValue.CompareTo(maxValue) <= 0;
                if (bRet == true)
                {
                    break;
                }
            }

            if (conditionDebug != null)
            {
                conditionDebug.Add(bRet, var.VariableName, E_FunctionT.Between, expectedValueList, actualValueList);
            }

            offset += size * 2;
            return bRet;
        }
        private bool EvaluateIn<T>(ref int offset, int opParameter, IEngineValueEvaluator ticket, 
            AbstractExecutingEngineValueGetter<T> getter, 
            ExecutingEngineConditionDebug conditionDebug, Dictionary<int, IList<T>> cache) where T : IComparable<T>
        {
            int length = opParameter == 0 ? m_compileCode.GetInt(ref offset, 1) : opParameter;

            VariableEntryType variable = m_compileCode.GetVariable(ref offset);
            int varIndex = variable.VarIndex;

            IList<T> actualValueList = getter.GetActualValue(ticket, varIndex, cache);
            bool bRet = false;
            int size = getter.Size;
            int end = offset + length * size;

            foreach (var actualValue in actualValueList)
            {
                for (int i = offset; i < end; i += size)
                {
                    T expectedValue = getter.GetExpectedValue(i);
                    int compare = actualValue.CompareTo(expectedValue);
                    if (compare <= 0)
                    {
                        bRet = compare == 0;
                        break;
                    }
                }

                if (bRet == true)
                {
                    break;
                }
            }

            if (variable.IsNotOperation)
            {
                bRet = !bRet;
            }

            if (conditionDebug != null)
            {
                List<T> expectedValueList = new List<T>();
                for (int i = offset; i < end; i += size)
                {
                    expectedValueList.Add(getter.GetExpectedValue(i));
                }
                conditionDebug.Add(bRet, variable.VariableName, E_FunctionT.In, expectedValueList, actualValueList);
            }

            offset = end;
            return bRet;

        }

    }

}
