﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConfigSystem.Engine
{

    public class OpCodeType
    {
        public OpCodeType(int v)
        {
            OpCode = (OpCode)((v >> 4) & 0x0F);
            OpParameter = v & 0x0F;
        }

        public OpCode OpCode { get; private set; }
        public int OpParameter { get; private set; }
        public SupportedType OpCodeSupportedType
        {
            get { return (SupportedType)OpParameter; }
        }

        public ComparisonOpCode ComparisonOpCode
        {
            get { return (ComparisonOpCode)OpParameter; }
        }
    }
}
