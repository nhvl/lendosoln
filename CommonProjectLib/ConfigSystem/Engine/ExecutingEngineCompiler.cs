﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConfigSystem.Engine.Instructions;
using CommonProjectLib.Common;

namespace ConfigSystem.Engine
{
    public class ExecutingEngineCompiler
    {
        // 9/17/2010 dd - Everytime we change the file layout significantly then we need to pump up the compiler version so the executing engine
        // will throw exception if it encounter the whole file.
        public const byte CompilerVersion = 5; // Original value=2 

        /// <summary>
        /// OPM 241675 - 03/27/2017 - je - Implement NOT IN operation by adding a prefix to parameter name that tells executing engine to do NOT comparison.
        /// </summary>
        public static string NotPrefix = "[not]_";


        public static byte[] Compile(SystemConfig systemConfig, DateTime version)
        {
            SymbolTable symbolTable = new SymbolTable();
            InstructionList list = new InstructionList();

            list.Add(new LabelInstruction(".header_section"));
            list.Add(new EngineVersionInstruction(CompilerVersion));
            list.Add(new RuleVersionInstruction(version));

            TableEntryInstruction stringEntryInstruction = new TableEntryInstruction(E_TableEntryT.StringTable);
            TableEntryInstruction varEntryInstruction = new TableEntryInstruction(E_TableEntryT.VariableTable);
            TableEntryInstruction actionEntryInstruction = new TableEntryInstruction(E_TableEntryT.ActionTable);
            TableEntryInstruction restraintGuidInstruction = new TableEntryInstruction(E_TableEntryT.RestraintGuidTable);
            TableEntryInstruction customVariableInstruction = new TableEntryInstruction(E_TableEntryT.CustomVariableTable);
            TableEntryInstruction conditionGuidInstruction = new TableEntryInstruction(E_TableEntryT.ConditionGuidTable);

            list.Add(stringEntryInstruction);
            list.Add(varEntryInstruction);
            list.Add(actionEntryInstruction);
            list.Add(restraintGuidInstruction);
            list.Add(customVariableInstruction);
            list.Add(conditionGuidInstruction);
            

            int ruleId = 0;
            foreach (var condition in systemConfig.Conditions)
            {
                ushort guidIdx = symbolTable.GetGuidIndex(condition.Id);
                LabelInstruction conditionLabel = new LabelInstruction("Condition_" + ruleId);

                conditionLabel.Offset = list.Offset;
                ruleId++;
                list.Add(conditionLabel);
                list.Add(new ConditionIdInstruction(guidIdx));
                foreach (var parameter in condition.ParameterSet)
                {
                    IEnumerable<IInstruction> instructionSet = CreateInstruction(parameter, symbolTable);
                    list.Add(instructionSet);
                }
                list.Add(new ReturnInstruction());

                foreach (var operation in condition.SystemOperationNames)
                {
                    symbolTable.AddActionToken(operation, conditionLabel);
                }
            }

            ruleId = 0;
            foreach (var restraint in systemConfig.Restraints)
            {
                ushort guidIdx = symbolTable.GetGuidIndex(restraint.Id);
                LabelInstruction restraintLabel = new LabelInstruction("Restraint_" + ruleId);

                restraintLabel.Offset = list.Offset;
                ruleId++;
                list.Add(restraintLabel);
                list.Add(new ConditionIdInstruction(guidIdx));
                foreach (var parameter in restraint.ParameterSet)
                {
                    IEnumerable<IInstruction> instructionSet = CreateInstruction(parameter, symbolTable);
                    list.Add(instructionSet);
                }
                list.Add(new ReturnInstruction());

                foreach (var operation in restraint.SystemOperationNames)
                {
                    symbolTable.AddRestraintToken(operation, restraintLabel);
                }
            }

            ruleId = 0;
            foreach (var customVariable in systemConfig.CustomVariables)
            {
                var guidIdx = symbolTable.GetGuidIndex(customVariable.Id);
                LabelInstruction customVariableLabel = new LabelInstruction("CustomVariable_" + ruleId);
                customVariableLabel.Offset = list.Offset;
                ruleId++;
                list.Add(customVariableLabel);
                list.Add(new ConditionIdInstruction(guidIdx));
                
                foreach (var parameter in customVariable.ParameterSet)
                {
                    IEnumerable<IInstruction> instructionSet = CreateInstruction(parameter, symbolTable);
                    list.Add(instructionSet);
                }
                list.Add(new ReturnInstruction());
                symbolTable.AddCustomVariable(customVariable.Name, customVariableLabel);
            }

            symbolTable.Export(list, actionEntryInstruction);
            symbolTable.Export(list, restraintGuidInstruction);
            symbolTable.Export(list, customVariableInstruction);
            symbolTable.Export(list, varEntryInstruction);
            symbolTable.Export(list, stringEntryInstruction);
            symbolTable.Export(list, conditionGuidInstruction);
            return GenerateCode(list);
        }

        private static IEnumerable<IInstruction> CreateInstruction(Parameter parameter, SymbolTable symbolTable)
        {
            List<IInstruction> instructionSet = new List<IInstruction>();
            //switch (parameter.FunctionT)
            //{
            //    case E_FunctionT.IsNonzeroValue:
            //        break;
            //    case E_FunctionT.IsAnyValue:
            //        break;
            //    default:
            //        break;
            //}
            switch (parameter.FunctionT)
            {
                case E_FunctionT.IsAnyValue:
                    instructionSet.Add(CreateSpecialOpInstruction(parameter, symbolTable, ComparisonOpCode.is_valid_value));
                    break;
                case E_FunctionT.IsNonzeroValue:
                    instructionSet.Add(CreateSpecialOpInstruction(parameter, symbolTable, ComparisonOpCode.is_non_default_value));
                    break;
                case E_FunctionT.IsNonblankStr:
                    instructionSet.Add(CreateIsNonblankStrInstruction(parameter, symbolTable));
                    break;
                case E_FunctionT.In:
                    instructionSet.AddRange(CreateInInstruction(parameter, symbolTable));
                    break;
                case E_FunctionT.Between:
                    instructionSet.Add(CreateBetweenInstruction(parameter, symbolTable));
                    break;
                case E_FunctionT.Equal:
                //case E_FunctionT.NotEqual:
                case E_FunctionT.IsGreaterThan:
                case E_FunctionT.IsLessThan:
                //case E_FunctionT.BeginsWith:
                //case E_FunctionT.DoesNotBeginWith:
                case E_FunctionT.IsGreaterThanOrEqualTo:
                case E_FunctionT.IsLessThanOrEqualTo:
                    instructionSet.Add(CreateComparisonInstruction(parameter, symbolTable));
                    break;
                case E_FunctionT.IsGreaterThanVar:
                case E_FunctionT.IsLessThanVar:
                case E_FunctionT.EqualVar:
                //case E_FunctionT.NotEqualVar:
                //case E_FunctionT.BeginsWithVar:
                //case E_FunctionT.DoesNotBeginWithVar:
                case E_FunctionT.IsGreaterThanOrEqualToVar:
                case E_FunctionT.IsLessThanOrEqualToVar:
                    instructionSet.Add(CreateComparisonVarInstruction(parameter, symbolTable));
                    break;
                default:
                    throw new Exception("Unhandle function=" + parameter.FunctionT);
            }

            return instructionSet;
        }
        private static IInstruction CreateComparisonVarInstruction(Parameter parameter, SymbolTable symbolTable)
        {
            if (parameter.Values.Count != 1)
            {
                throw new Exception("Only expect one value on the right hand side.");
            }
            string rhsVarName = null;
            foreach (var s in parameter.Values)
            {
                rhsVarName = s;
            }
            switch (parameter.ValueType)
            {
                case E_ParameterValueType.Int:
                    return new ComparisonVarInstruction<int>(parameter.ExecutingEngineName, symbolTable, rhsVarName, parameter.FunctionT);
                case E_ParameterValueType.Float:
                    return new ComparisonVarInstruction<float>(parameter.ExecutingEngineName, symbolTable, rhsVarName, parameter.FunctionT);
                case E_ParameterValueType.String:
                    return new ComparisonVarInstruction<string>(parameter.ExecutingEngineName, symbolTable, rhsVarName, parameter.FunctionT);
                case E_ParameterValueType.DateTime:
                    return new ComparisonVarInstruction<DateTime>(parameter.ExecutingEngineName, symbolTable, rhsVarName, parameter.FunctionT);
                case E_ParameterValueType.Bool:
                    return new ComparisonVarInstruction<bool>(parameter.ExecutingEngineName, symbolTable, rhsVarName, parameter.FunctionT);
                default:
                    throw new Exception();
            }
        }
        private static IInstruction CreateComparisonInstructionImpl<T>(Parameter parameter, SymbolTable symbolTable, Func<string, T> convert) where T : IComparable<T>
        {
            if (parameter.Values.Count != 1)
            {
                throw new Exception("COMPARISON OPERATION requires exactly 1 value");
            }
            T arg = default(T);
            foreach (var o in parameter.Values)
            {
                arg = convert(o);
            }
            return new ComparisonInstruction<T>(parameter.ExecutingEngineName, symbolTable, parameter.FunctionT, arg);
        }
        private static IInstruction CreateComparisonInstruction(Parameter parameter, SymbolTable symbolTable)
        {
            switch (parameter.ValueType)
            {
                case E_ParameterValueType.Int:
                    return CreateComparisonInstructionImpl<int>(parameter, symbolTable, o => int.Parse(o));
                case E_ParameterValueType.Float:
                    return CreateComparisonInstructionImpl<float>(parameter, symbolTable, o => float.Parse(o));
                case E_ParameterValueType.String:
                    return CreateComparisonInstructionImpl<string>(parameter, symbolTable, o => o);
                case E_ParameterValueType.Bool:
                    return CreateComparisonInstructionImpl<bool>(parameter, symbolTable, o => bool.Parse(o));
                case E_ParameterValueType.DateTime:
                    DateTime dt;
                    // 12/20/2011 dd - Per OPM 75432 - where are now support N days from today in today+/-{N} format.
                    // Therefore if value is not actual date then we will create ComparisonVar instruction.
                    bool hasValidDate = true;
                    string paramValue = string.Empty;
                    foreach (var o in parameter.Values)
                    {
                        if (DateTime.TryParse(o, out dt) == false)
                        {
                            paramValue = o;
                            hasValidDate = false;
                            break;
                        }
                    }
                    if (hasValidDate)
                    {
                        return CreateComparisonInstructionImpl<DateTime>(parameter, symbolTable, o => DateTime.Parse(o));
                    }
                    else
                    {
                        E_FunctionT func = E_FunctionT.IsLessThanVar;
                        switch (parameter.FunctionT)
                        {
                            case E_FunctionT.Equal:
                                func = E_FunctionT.EqualVar;
                                break;
                            case E_FunctionT.IsGreaterThan:
                                func = E_FunctionT.IsGreaterThanVar;
                                break;
                            case E_FunctionT.IsLessThan:
                                func = E_FunctionT.IsLessThanVar;
                                break;
                            case E_FunctionT.IsGreaterThanOrEqualTo:
                                func = E_FunctionT.IsGreaterThanOrEqualToVar;
                                break;
                            case E_FunctionT.IsLessThanOrEqualTo:
                                func = E_FunctionT.IsLessThanOrEqualToVar;
                                break;

                            default:
                                throw new Exception("Unhandle function=" + parameter.FunctionT + " in CreateComparisonInstruction for date.");
                        }
                        return new ComparisonVarInstruction<DateTime>(parameter.ExecutingEngineName, symbolTable, paramValue, func);

                    }
                case E_ParameterValueType.CustomVariable:
                    string rhsValue = string.Empty ;
                    foreach (var o in parameter.Values)
                    {
                        rhsValue = o;
                    }
                    return new CustomVariableInstruction(parameter.Name, parameter.Scope, symbolTable, rhsValue);
                default:
                    throw new Exception("Unhandle " + parameter.ValueType);
            }
        }

        private static byte[] ConvertFromDateTime(DateTime dt)
        {
            long ticks = dt.Ticks;
            return BitConverter.GetBytes(ticks);
        }
        private static IInstruction CreateSpecialOpInstruction(Parameter parameter, SymbolTable symbolTable, ComparisonOpCode opcode)
        {
            if (parameter.Values.Count != 1)
            {
                throw new Exception("Invalid value count for IsNonblankStr. Expected 1");
            }

            string value = parameter.Values.ToArray()[0];

            bool bExpectedResult = bool.Parse(value);
            switch (parameter.ValueType)
            {
                case E_ParameterValueType.Int:
                    return new ComparisonInstruction<int>(parameter.Name, symbolTable, opcode, bExpectedResult ? 1 : 0);
                case E_ParameterValueType.Float:
                    return new ComparisonInstruction<float>(parameter.Name, symbolTable, opcode, bExpectedResult ? 1 : 0);
                case E_ParameterValueType.DateTime:
                    return new ComparisonInstruction<DateTime>(parameter.Name, symbolTable, opcode, new DateTime(bExpectedResult ? 1 : 0));
                default:
                    throw CBaseException.CreateUnexpectedEnumException(parameter.ValueType);
            }
        }
        private static IInstruction CreateIsNonblankStrInstruction(Parameter parameter, SymbolTable symbolTable)
        {
            switch (parameter.ValueType)
            {
                case E_ParameterValueType.String:
                    if (parameter.Values.Count != 1)
                    {
                        throw new Exception("Invalid value count for IsNonblankStr. Expected 1");
                    }

                    string value = parameter.Values.ToArray()[0];

                    return new ComparisonInstruction<string>(parameter.Name, symbolTable, ComparisonOpCode.is_non_default_value, value);

                default:
                    throw CBaseException.CreateUnexpectedEnumException(parameter.ValueType);
            }
        }
        private static IInstruction CreateBetweenInstruction(Parameter parameter, SymbolTable symbolTable)
        {
            switch (parameter.ValueType)
            {
                case E_ParameterValueType.Int:
                    return CreateBetweenInstructionImpl<int>(parameter, symbolTable, o => int.Parse(o));
                case E_ParameterValueType.Float:
                    return CreateBetweenInstructionImpl<float>(parameter, symbolTable, o => float.Parse(o));
                case E_ParameterValueType.DateTime:
                    return CreateBetweenInstructionImpl<DateTime>(parameter, symbolTable, o => DateTime.Parse(o));
                default:
                    throw CBaseException.CreateUnexpectedEnumException(parameter.ValueType);
            }
        }
        private static IInstruction CreateBetweenInstructionImpl<T>(Parameter parameter, SymbolTable symbolTable, Func<string, T> convert) where T : IComparable<T>
        {
            if (parameter.Values.Count != 2)
            {
                throw new Exception("BETWEEN OPERATION requires exactly 2 values.");
            }

            string[] args = parameter.Values.ToArray();
            T minValue = convert(args[0]);
            T maxValue = convert(args[1]);

            return new BetweenInstruction<T>(parameter.Name, symbolTable, minValue, maxValue);
            
        }

        private static IEnumerable<IInstruction> CreateInInstructionImpl<T>(Parameter parameter, SymbolTable symbolTable, Func<string, T> convert) where T : IComparable<T>
        {
            List<IInstruction> instructions = new List<IInstruction>();

            if (parameter.Values.Count > 0)
            {
                instructions.Add(new InInstruction<T>(parameter.Name, symbolTable, parameter.Values.Select(v => convert(v)).ToList()));
            }

            if (parameter.Exceptions.Count > 0)
            {
                instructions.Add(new InInstruction<T>(NotPrefix + parameter.Name, symbolTable, parameter.Exceptions.Select(e => convert(e)).ToList()));
            }

            return instructions;
        }
        private static IEnumerable<IInstruction> CreateInInstructionImpl<T>(Parameter parameter, SymbolTable symbolTable, Func<string, T> convert, Func<T, byte[]> tobytesConvert) where T : IComparable<T>
        {
            List<IInstruction> instructions = new List<IInstruction>();

            if (parameter.Values.Count > 0)
            {
                instructions.Add(new InInstruction<T>(parameter.Name, symbolTable, parameter.Values.Select(v => convert(v)).ToList(), tobytesConvert));
            }

            if (parameter.Exceptions.Count > 0)
            {
                instructions.Add(new InInstruction<T>(NotPrefix + parameter.Name, symbolTable, parameter.Exceptions.Select(e => convert(e)).ToList(), tobytesConvert));
            }

            return instructions;
        }

        private static IEnumerable<IInstruction> CreateInInstruction(Parameter parameter, SymbolTable symbolTable)
        {
            switch (parameter.ValueType)
            {
                case E_ParameterValueType.Int:
                    return CreateInInstructionImpl<int>(parameter, symbolTable, o => int.Parse(o));
                case E_ParameterValueType.Float:
                    return CreateInInstructionImpl<float>(parameter, symbolTable, o => float.Parse(o));
                case E_ParameterValueType.String:
                    return CreateInInstructionImpl<string>(parameter, symbolTable, o => o);
                case E_ParameterValueType.DateTime:
                    return CreateInInstructionImpl<DateTime>(parameter, symbolTable, o => DateTime.Parse(o), new Func<DateTime, byte[]>(ConvertFromDateTime));
                case E_ParameterValueType.Bool:
                    return CreateInInstructionImpl<bool>(parameter, symbolTable, o => bool.Parse(o));

                case E_ParameterValueType.CustomVariable: // 9/17/2010 dd - We do not support Custom Variable in IN.
                default:
                    throw new Exception("Unhandle type=" + parameter.ValueType + " in CreateInInstruction");
            }
        }

        private static byte[] GenerateCode(IEnumerable<IInstruction> instructionSet)
        {
            List<byte> buffer = new List<byte>();

            foreach (var o in instructionSet)
            {
                buffer.AddRange(o.GetBytes());
            }
            return buffer.ToArray();
        }
    }
}
