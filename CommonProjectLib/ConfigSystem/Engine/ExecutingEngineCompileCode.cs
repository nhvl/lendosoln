﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommonProjectLib.Common;

namespace ConfigSystem.Engine
{
    class ExecutingEngineCompileCode
    {
        private const int StringEntryIndex = 9;
        private const int VarEntryIndex = 15;
        private const int ActionEntryIndex = 21;
        private const int RestraintEntryIndex = 27;
        private const int CustomVarEntryIndex = 33;
        private const int ConditionGuidEntryIndex = 39;

        private byte[] m_content;

        private string[] m_stringList = null;
        private Guid[] m_conditionGuidList = null;
        private VariableEntryType[] m_variableEntryList = null;

        private RuleListEntryType[] m_actionEntryList = null;
        private RuleListEntryType[] m_restraintEntryList = null;
        private RuleListEntryType[] m_customVariableEntryList = null;

        public DateTime ReleasedDate { get; private set; }
        public int Size { get { return m_content.Length; } }
        public ExecutingEngineCompileCode(byte[] content)
        {
            m_content = content;
            Initialize();
        }
        private void Initialize()
        {
            byte engineVersion = m_content[0];
            if (engineVersion != ExecutingEngineCompiler.CompilerVersion)
            {
                throw new ExecutingEngineVersionMismatchException();
            }
            long versionTicks = GetLong(1);
            ReleasedDate = new DateTime(versionTicks);

            InitializeStringList();
            InitializeVariableEntryList();
            InitializeConditionGuidList();
            m_actionEntryList = InitializeRuleListEntryItem(ActionEntryIndex);
            m_restraintEntryList = InitializeRuleListEntryItem(RestraintEntryIndex);
            m_customVariableEntryList = InitializeRuleListEntryItem(CustomVarEntryIndex);

        }

        private void InitializeConditionGuidList()
        {
            int offset = GetInt(ConditionGuidEntryIndex, 4);
            int count = GetCount(ConditionGuidEntryIndex + 4);

            m_conditionGuidList = new Guid[count];

            byte[] bytes = new byte[16];
            for (int i = 0; i < count; i++)
            {
                Array.Copy(m_content, offset, bytes, 0, 16);
                m_conditionGuidList[i] = new Guid(bytes);
                offset += 16;
            }
        }

        private void InitializeVariableEntryList()
        {
            int offset = GetInt(VarEntryIndex, 4);
            int count = GetCount(VarEntryIndex + 4);

            m_variableEntryList = new VariableEntryType[count];
            for (int i = 0; i < count; i++)
            {
                SupportedType type = (SupportedType)GetInt(offset, 1);
                int stringIdx = GetStringIndex(offset + 1);
                m_variableEntryList[i] = new VariableEntryType(i, type, stringIdx, this);
                offset += 3;
            }
        }
        private void InitializeStringList()
        {
            int offset = GetInt(StringEntryIndex, 4);
            int count = GetCount(StringEntryIndex + 4);

            m_stringList = new string[count];

            for (int i = 0; i < count; i++)
            {
                int length = GetCount(offset);
                string str = ASCIIEncoding.ASCII.GetString(m_content, offset + 2, length);
                m_stringList[i] = string.Intern(str);
                offset += (2 + length);
            }
        }
        private RuleListEntryType[] InitializeRuleListEntryItem(int startOffset)
        {
            int offset = GetInt(startOffset, 4);
            int count = GetCount(startOffset + 4);

            RuleListEntryType[] list = new RuleListEntryType[count];

            for (int i = 0; i < count; i++)
            {
                int stringIndex = GetStringIndex(offset);
                int ruleCount = GetCount(offset + 2);
                int[] ruleList = new int[ruleCount];
                for (int j = 0; j < ruleCount; j++)
                {
                    ruleList[j] = GetOffset(offset + 4 + j * 4);
                }
                list[i] = new RuleListEntryType(stringIndex, ruleList, this);
                offset += (4 + 4 * ruleCount);
            }
            return list;
        }


        public OpCodeType GetOpCode(ref int startIndex)
        {
            int v = GetInt(ref startIndex, 1);
            return new OpCodeType(v);
        }
        public int GetInt(ref int startIndex, int size)
        {
            int v = GetInt(startIndex, size);
            startIndex += size;
            return v;
        }

        public int GetInt(ref int startIndex, SupportedType type)
        {
            int size = 0;
            switch (type)
            {
                case SupportedType.Byte:
                    size = 1;
                    break;
                case SupportedType.UShort:
                    size = 2;
                    break;
                case SupportedType.Int:
                    size = 4;
                    break;
                default:
                    throw new Exception("Unhandle " + type);
            }
            return GetInt(ref startIndex, size);
        }

        public int GetInt(int startIndex, int size)
        {
            if (size == 1)
            {
                return m_content[startIndex];
            }
            else if (size == 2)
            {
                return BitConverter.ToUInt16(m_content, startIndex);
            }
            else if (size == 4)
            {
                return BitConverter.ToInt32(m_content, startIndex);
            }
            else
            {
                throw new Exception("Unsupport size=" + size + " in GetInt");
            }
        }
        public long GetLong(int startIndex)
        {
            return BitConverter.ToInt64(m_content, startIndex);
        }

        public float GetFloat(int startIndex)
        {
            return BitConverter.ToSingle(m_content, startIndex);
        }
        public float GetFloat(ref int startIndex)
        {
            float v = GetFloat(startIndex);
            startIndex += 4;
            return v;
        }

        public int GetCount(int startIndex)
        {
            return GetInt(startIndex, 2);
        }
        public int GetStringIndex(int startIndex)
        {
            return GetInt(startIndex, 2);
        }
        public int GetVarIndex(ref int offset)
        {
            return GetInt(ref offset, 2);
        }
        public int GetOffset(int startIndex)
        {
            return GetInt(startIndex, 4);
        }

        public int FindString(string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                s = string.Empty;
            }
            else
            {
                s = s.ToLower();
            }
            for (int i = 0; i < m_stringList.Length; i++)
            {
                if (s == m_stringList[i])
                {
                    return i;
                }
            }
            return -1;
        }
        public string GetStringByIdx(int stringIdx)
        {
            return m_stringList[stringIdx];

        }

        public Guid GetConditionId(int ruleOffset)
        {
            // First byte contain opcode
            // Next 2 bytes contains guid index.
            int idx = GetCount(ruleOffset + 1);
            return m_conditionGuidList[idx];
        }

        public IEnumerable<RuleListEntryType> ActionEntryList
        {
            get { return m_actionEntryList; }
        }

        public IEnumerable<RuleListEntryType> RestraintEntryList
        {
            get { return m_restraintEntryList; }
        }

        public IEnumerable<VariableEntryType> VariableList
        {
            get { return m_variableEntryList; }
        }
        public IEnumerable<RuleListEntryType> CustomVariableEntryList
        {
            get { return m_customVariableEntryList; }
        }

        public VariableEntryType GetVariableByIndex(int varIndex)
        {
            return m_variableEntryList[varIndex];
        }
        public VariableEntryType GetVariableByName(string variableName)
        {
            foreach (var o in m_variableEntryList)
            {
                if (o.VariableName.Equals(variableName, StringComparison.OrdinalIgnoreCase))
                {
                    return o;
                }
            }
            return null;
        }
        public VariableEntryType GetVariable(ref int offset)
        {
            return m_variableEntryList[GetInt(ref offset, 2)];
        }

        public RuleListEntryType GetActionByIndex(int actionIndex)
        {
            return m_actionEntryList[actionIndex];
        }

        public RuleListEntryType GetRestraintByIndex(int restraintIndex)
        {
            return m_restraintEntryList[restraintIndex];
        }

        public RuleListEntryType GetCustomVariableByName(string name)
        {
            foreach (var o in m_customVariableEntryList)
            {
                if (o.Name.Equals(name, StringComparison.OrdinalIgnoreCase))
                {
                    return o;
                }
            }
            return null;
        }
        public RuleListEntryType GetCustomVariableByIndex(int varIndex)
        {
            return m_customVariableEntryList[varIndex];
        }

        private RuleListEntryType GetAction(string action)
        {
            int stringIndex = FindString(action);
            if (stringIndex >= 0)
            {
                foreach (var o in ActionEntryList)
                {
                    if (o.StringIndex == stringIndex)
                    {
                        return o;
                    }
                }
            }
            return null;
        }

        private RuleListEntryType GetRestraint(string restraint)
        {
            int stringIndex = FindString(restraint);
            if (stringIndex >= 0)
            {
                foreach (var o in RestraintEntryList)
                {
                    if (o.StringIndex == stringIndex)
                    {
                        return o;
                    }
                }
            }
            return null;
        }

        public IEnumerable<int> GetRulesByAction(string action)
        {
            RuleListEntryType actionDefinition = GetAction(action);
            if (null != actionDefinition)
            {
                return actionDefinition.RuleList;
            }

            return Enumerable.Empty<int>();
        }

        public IEnumerable<int> GetRulesByRestraint(string restraint)
        {
            RuleListEntryType restraintDefinition = GetRestraint(restraint);
            if (null != restraintDefinition)
            {
                return restraintDefinition.RuleList;
            }

            return Enumerable.Empty<int>();
        }

        public IEnumerable<int> GetRulesByOperation(string operation)
        {
            return GetRulesByAction(operation).Union(GetRulesByRestraint(operation));
        }

        public bool HasAction(string action)
        {
            return GetAction(action) != null;
        }

        public bool HasRestraint(string restraint)
        {
            return GetRestraint(restraint) != null;
        }

        private void Append(StringBuilder sb, int indentLevel, int offSet, string msg)
        {
            string indentSpaces = "  ";
            string spaces = indentSpaces;
            for (int i = 0; i < indentLevel; i++)
            {
                spaces += indentSpaces;
            }
            if (offSet < 0)
            {
                sb.AppendLine("      " + spaces + msg);
            }
            else
            {
                sb.AppendLine("0x" + offSet.ToString("X4") + spaces + msg);
            }
        }
        private void Append(StringBuilder sb, int indentLevel, string msg) 
        {
            Append(sb, indentLevel, -1, msg);
        }

        public string GenerateDebug()
        {
            StringBuilder sb = new StringBuilder();
            Append(sb, 0, "ExecutingEngineCompileCode");
            Append(sb, 0, "Content Length = " + m_content.Length + " bytes.");

            Append(sb, 0, 0, "Engine Version: " + m_content[0] + " // 1 byte.");
            Append(sb, 0, 1, "Released Date = " + ReleasedDate + " // 8 bytes.");
            sb.AppendLine();

            int offset = 9;

            #region Table Entry
            // 10/27/2011 dd - Order is important for EntryKeyList
            string[] entryKeyList = { "StringEntryIndex", "VarEntryIndex", "ActionEntryIndex",
                                        "RestraintEntryIndex", "CustomVarEntryIndex", "ConditionGuidEntryIndex" };
            AppendBlock(sb, "TableEntry", ref offset, () =>
                {
                    foreach (var key in entryKeyList)
                    {
                        int startOffset = GetOffset(offset);
                        int count = GetCount(offset + 4);

                        Append(sb, 1, offset, string.Format("{0,-30}: StartOffset:0x{1} Count:{2, -4} // 6 bytes", key, startOffset.ToString("X4"), count));
                        offset += 6;
                    }

                });
            #endregion

            #region Rule Definition

            int endRuleDefitionOffset = GetOffset(ActionEntryIndex);
            AppendBlock(sb, "Rule Definition", ref offset, () =>
                {
                    while (offset < endRuleDefitionOffset)
                    {
                        GenerateRuleDebug(sb, ref offset);
                        sb.AppendLine();
                    }

                });

            #endregion

            #region Action/Privilege List
            AppendBlock(sb, "Action/Privilege", ref offset, () =>
                {
                    foreach (var o in ActionEntryList)
                    {
                        Append(sb, 1, offset, "Action/Privilege: [" + o.Name + "]. Count=" + o.RuleList.Count() + " // 4 bytes");
                        int i = 0;
                        offset += 4;
                        foreach (var rule in o.RuleList)
                        {
                            Append(sb, 2, offset, "Condition #" + i + ". Condition Rule Offset: " + Hex(rule) + " // 4 bytes");
                            offset += 4;
                            i++;
                        }
                        sb.AppendLine();
                    }

                });

            #endregion

            #region Restraint List
            AppendBlock(sb, "Restraint", ref offset, () =>
            {
                foreach (var o in RestraintEntryList)
                {
                    Append(sb, 1, offset, "Restraint: [" + o.Name + "]. Count=" + o.RuleList.Count() + " // 4 bytes");
                    int i = 0;
                    offset += 4;
                    foreach (var rule in o.RuleList)
                    {
                        Append(sb, 2, offset, "Condition #" + i + ". Condition Rule Offset: " + Hex(rule) + " // 4 bytes");
                        offset += 4;
                        i++;
                    }
                    sb.AppendLine();
                }
            });

            #endregion

            #region Custom Variable List

            AppendBlock(sb, "Custom Variable", ref offset, () =>
                {
                    foreach (var o in CustomVariableEntryList)
                    {
                        Append(sb, 1, offset, "Name: [" + o.Name + "]. Count=" + o.RuleList.Count() + " // 4 bytes");
                        int i = 0;
                        offset += 4;
                        foreach (var rule in o.RuleList)
                        {
                            Append(sb, 2, offset, "Condition #" + i + ". Condition Rule Offset: " + Hex(rule) + " // 4 bytes");
                            offset += 4;
                            i++;
                        }
                        sb.AppendLine();
                    }

                });

            #endregion

            #region Variable List
            AppendBlock(sb, "Variable", ref offset, () =>
                {
                    foreach (var o in VariableList)
                    {
                        Append(sb, 1, offset, string.Format("{0,5}. Name=[{1}] Type={2} // 3 bytes", o.VarIndex, o.VariableName,  o.Type));
                        offset += 3;
                    }
                });

            #endregion

            #region String List
            AppendBlock(sb, "String", ref offset, () =>
                {
                    for (int i = 0; i < m_stringList.Length; i++)
                    {
                        int bytes = 2 + m_stringList[i].Length;
                        Append(sb, 1, offset, string.Format("{0,5}. Length={1,5}, [{2}] // {3} bytes", i ,m_stringList[i].Length, m_stringList[i], bytes));
                        offset += bytes;
                    }

                });

            #endregion

            #region Condition Guid List
            AppendBlock(sb, "Guid", ref offset, () =>
                {
                    for (int i = 0; i < m_conditionGuidList.Length; i++)
                    {
                        Append(sb, 1, offset, string.Format("{0, 5}. {1} // 16 bytes", i, m_conditionGuidList[i]));
                        offset += 16;
                    }
                });

            #endregion

            return sb.ToString();
        }
        private void AppendBlock(StringBuilder sb, string desc, ref int offset, Action action)
        {
            int startBlockOffset = offset;
            Append(sb, 0, "BEGIN " + desc + " List");

            action();

            Append(sb, 0, "END " + desc + " List (Total " + (offset - startBlockOffset) + " bytes)");
            sb.AppendLine();
        }
        private string Hex(int v)
        {
            return "0x" + v.ToString("X4");
        }
        private void GenerateRuleDebug(StringBuilder sb, ref int offset)
        {
            bool isDone = false;
            VariableEntryType var = null;
            int varIdx = 0;
            int argLength = 0;
            while (isDone == false)
            {
                int ruleOffset = offset;
                OpCodeType opCode = GetOpCode(ref offset);

                StringBuilder instruction = new StringBuilder();
                instruction.AppendFormat("OpCode.{0,-20} ", opCode.OpCode);
                switch (opCode.OpCode)
                {
                    case OpCode.eq_0_15:
                        varIdx = GetVarIndex(ref offset);
                        var = GetVariableByIndex(varIdx);
                        instruction.Append("Var[" + var.VariableName + "] == " + opCode.OpParameter);
                        break;
                    case OpCode.eq_16_31:
                        varIdx = GetVarIndex(ref offset);
                        var = GetVariableByIndex(varIdx);
                        instruction.Append("Var[" + var.VariableName + "] == " + (opCode.OpParameter + 16));
                        break;
                    case OpCode.between:
                        varIdx = GetVarIndex(ref offset);
                        var = GetVariableByIndex(varIdx);
                        instruction.Append("Var[" + var.VariableName + "] ");
                        switch ((SupportedType)opCode.OpParameter)
                        {
                            case SupportedType.Byte:
                                instruction.Append(". Arg_0=" + m_content[offset] + ", Arg_1=" + m_content[offset + 1]);
                                offset += 2;
                                break;
                            case SupportedType.UShort:
                                instruction.Append(". Arg_0=" + GetCount(offset) + ", Arg_1=" + GetCount(offset + 2));
                                offset += 4;
                                break;
                            case SupportedType.String:
                                instruction.Append(". Arg_0=string_idx" + GetCount(offset) + ", Arg_1" + GetCount(offset + 2));
                                offset += 4;
                                break;
                            case SupportedType.Int:
                                instruction.Append(". Arg_0=" + GetInt(offset, 4) + ", Arg_1=" + GetInt(offset + 4, 4));
                                offset += 8;
                                break;
                            case SupportedType.Float:
                                instruction.Append(". Arg_0=" + GetFloat(offset) + ", Arg_1=" + GetFloat(offset + 4));
                                offset += 8;
                                break;
                            case SupportedType.DateTime:
                                instruction.Append(". Arg_0=" + new DateTime(GetLong(offset)) + ", Arg_1=" + new DateTime(GetLong(offset + 8)));
                                offset += 16;
                                break;
                            default:
                                throw CBaseException.CreateUnexpectedEnumException((SupportedType)opCode.OpParameter);
                        }
                        break;
                    case OpCode.in_byte:

                        argLength = opCode.OpParameter == 0 ? GetInt(ref offset, 1) : opCode.OpParameter;
                        varIdx = GetVarIndex(ref offset);
                        var = GetVariableByIndex(varIdx);
                        instruction.Append("Var[" + var.VariableName + "] Args: [");
                        for (int i = 0; i < argLength; i++)
                        {
                            instruction.Append(GetInt(ref offset, 1).ToString() + ", ");
                        }
                        instruction.Append("]");

                        break;
                    case OpCode.in_ushort:

                        argLength = opCode.OpParameter == 0 ? GetInt(ref offset, 1) : opCode.OpParameter;
                        varIdx = GetVarIndex(ref offset);
                        var = GetVariableByIndex(varIdx);
                        instruction.Append("Var[" + var.VariableName + "] Args: [");
                        for (int i = 0; i < argLength; i++)
                        {
                            instruction.Append(GetInt(ref offset, 2).ToString() + ", ");
                        }
                        instruction.Append("]");
                        break;
                    case OpCode.in_int:

                        argLength = opCode.OpParameter == 0 ? GetInt(ref offset, 1) : opCode.OpParameter;
                        varIdx = GetVarIndex(ref offset);
                        var = GetVariableByIndex(varIdx);
                        instruction.Append("Var[" + var.VariableName + "] Args: [");
                        for (int i = 0; i < argLength; i++)
                        {
                            instruction.Append(GetInt(ref offset, 4).ToString() + ", ");
                        }
                        instruction.Append("]");

                        break;
                    case OpCode.in_string:
                        argLength = opCode.OpParameter == 0 ? GetInt(ref offset, 1) : opCode.OpParameter;
                        varIdx = GetVarIndex(ref offset);
                        var = GetVariableByIndex(varIdx);
                        instruction.Append("Var[" + var.VariableName + "] Args: [");
                        for (int i = 0; i < argLength; i++)
                        {
                            instruction.Append(GetStringIndex(offset).ToString() + ", ");
                            offset += 2;
                        }
                        instruction.Append("]");

                        break;
                    case OpCode.in_float:
                        argLength = opCode.OpParameter == 0 ? GetInt(ref offset, 1) : opCode.OpParameter;
                        varIdx = GetVarIndex(ref offset);
                        var = GetVariableByIndex(varIdx);
                        instruction.Append("Var[" + var.VariableName + "] Args: [");
                        for (int i = 0; i < argLength; i++)
                        {
                            instruction.Append(GetFloat(ref offset).ToString() + ", ");
                        }
                        instruction.Append("]");
                        break;
                    case OpCode.in_datetime:
                        argLength = opCode.OpParameter == 0 ? GetInt(ref offset, 1) : opCode.OpParameter;
                        varIdx = GetVarIndex(ref offset);
                        var = GetVariableByIndex(varIdx);
                        instruction.Append("Var[" + var.VariableName + "] Args: [");
                        for (int i = 0; i < argLength; i++)
                        {
                            instruction.Append(new DateTime(GetLong(offset)).ToString() + ", ");
                            offset += 8;
                        }
                        instruction.Append("]");
                        break;
                    case OpCode.comparison_op:
                        varIdx = GetVarIndex(ref offset);
                        var = GetVariableByIndex(varIdx);
                        instruction.Append("Var[" + var.VariableName + "] " + opCode.ComparisonOpCode);
                        switch (var.Type)
                        {
                            case SupportedType.Bool:
                            case SupportedType.Byte:
                                instruction.Append(". Arg_0=" + m_content[offset]);
                                offset++;
                                break;
                            case SupportedType.Int:
                                instruction.Append(". Arg_0=" + GetInt(offset, 4));
                                offset += 4;
                                break;
                            case SupportedType.Float:
                                instruction.Append(". Arg_0=" + GetFloat(offset));
                                offset += 4;

                                break;
                            case SupportedType.String:
                                instruction.Append(". Arg_0=" + GetStringIndex(offset));
                                offset += 2;
                                break;
                            case SupportedType.DateTime:
                                instruction.Append(". Arg_0=" + new DateTime(GetLong(offset)) +  "(" + GetLong(offset) + "L)");
                                offset += 8;
                                break;
                            default:
                                throw CBaseException.CreateUnexpectedEnumException(var.Type);
                        }
                        break;
                    case OpCode.comparison_var:
                        varIdx = GetVarIndex(ref offset);
                        var = GetVariableByIndex(varIdx);
                        int rhsVarIdx = GetVarIndex(ref offset);
                        VariableEntryType rhsVar = GetVariableByIndex(rhsVarIdx);
                        instruction.Append("Var[" + var.VariableName + "] " + opCode.ComparisonOpCode + " Var:[" + rhsVar.VariableName + "]");
                        break;
                    case OpCode.custom_var:
                        varIdx = GetVarIndex(ref offset);
                        if (opCode.OpParameter == 0 || opCode.OpParameter == 1)
                        {
                            instruction.Append("CustomVar[" + GetCustomVariableByIndex(varIdx).Name + "] == " + opCode.OpParameter);
                        }
                        else if (opCode.OpParameter == 2 || opCode.OpParameter == 3)
                        {
                            instruction.Append("SystemCustomVar[" + m_stringList[varIdx]+ "] == " + (opCode.OpParameter - 2));

                        }
                        break;
                    case OpCode.label:
                        Guid guid = m_conditionGuidList[GetCount(offset)];
                        offset += 2;
                        instruction.Append("ConditionId=" + guid);
                        break;
                    case OpCode.ret_true:
                        isDone = true;
                        break;
                    default:
                        throw CBaseException.CreateUnexpectedEnumException(opCode.OpCode);

                }
                int bytes = offset - ruleOffset;
                Append(sb, 2, ruleOffset, instruction + " // " + bytes + " bytes");
            }

        }
    }

}
