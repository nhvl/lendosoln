﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConfigSystem.Engine
{
    // 9/17/2010 dd - All the classes in this file are intented to be use by ExecutingEngine. It does not make sense
    // to use it outside. These classes may change as ExecutingEngine see fit.

    abstract class AbstractExecutingEngineValueGetter<T> where T : IComparable<T>
    {
        protected ExecutingEngineCompileCode CompileCode { get; private set; }

        private int m_sizeOfValue;

        protected AbstractExecutingEngineValueGetter(ExecutingEngineCompileCode compileCode, int sizeOfValue)
        {
            CompileCode = compileCode;
            m_sizeOfValue = sizeOfValue;
        }

        internal abstract T GetExpectedValue(int offset);
        protected abstract IList<T> GetActualValueImpl(IEngineValueEvaluator ticket, string name);
        internal int Size { get { return m_sizeOfValue; } }

        internal virtual bool IsNonDefaultValue(T value)
        {
            throw new NotImplementedException();
        }
        internal IList<T> GetActualValue(IEngineValueEvaluator ticket, int varIndex, Dictionary<int, IList<T>> cache)
        {
            IList<T> result;

            if (cache.TryGetValue(varIndex, out result) == false)
            {
                try
                {
                    result = GetActualValueImpl(ticket, CompileCode.GetVariableByIndex(varIndex).VariableName);
                }
                catch
                {
                    // 9/30/2010 dd - If there are any exception while getting the variable value, then I will
                    // return empty list to engine.
                    result = new List<T>();
                }
                cache.Add(varIndex, result);
            }
            return result;
        }

        internal virtual bool IsValidValue(T value)
        {
            // 9/30/2013 dd - Check to see if the value is a valid value or not.
            // By default consider all values are valid.
            return true;
        }
    }
    class ExecutingEngineBoolValueGetter : AbstractExecutingEngineValueGetter<bool>
    {
        public ExecutingEngineBoolValueGetter(ExecutingEngineCompileCode compileCode)
            : base(compileCode, 4)
        {
        }
        internal override bool GetExpectedValue(int offset)
        {
            return CompileCode.GetInt(offset, 4) == 1;
        }

        protected override IList<bool> GetActualValueImpl(IEngineValueEvaluator ticket, string name)
        {
            return ticket.GetBoolValues(name);
        }

    }

    class ExecutingEngineIntValueGetter : AbstractExecutingEngineValueGetter<int>
    {
        public ExecutingEngineIntValueGetter(int size, ExecutingEngineCompileCode compileCode)
            : base(compileCode, size)
        {
        }

        internal override int GetExpectedValue(int offset)
        {
            return CompileCode.GetInt(offset, Size);
        }

        protected override IList<int> GetActualValueImpl(IEngineValueEvaluator ticket, string name)
        {
            return ticket.GetIntValues(name);
        }

        internal override bool IsNonDefaultValue(int value)
        {
            return value != 0;
        }

    }

    class ExecutingEngineFloatValueGetter : AbstractExecutingEngineValueGetter<float>
    {
        public ExecutingEngineFloatValueGetter(ExecutingEngineCompileCode compileCode)
            : base(compileCode, 4)
        {
        }
        internal override float GetExpectedValue(int offset)
        {
            return CompileCode.GetFloat(offset);
        }

        protected override IList<float> GetActualValueImpl(IEngineValueEvaluator ticket, string name)
        {
            return ticket.GetFloatValues(name);
        }

        internal override bool IsNonDefaultValue(float value)
        {
            return value != 0;
        }
    }

    class ExecutingEngineStringValueGetter : AbstractExecutingEngineValueGetter<string>
    {
        public ExecutingEngineStringValueGetter(ExecutingEngineCompileCode compileCode)
            : base(compileCode, 2)
        {
        }
        internal override string GetExpectedValue(int offset)
        {
            return CompileCode.GetStringByIdx(CompileCode.GetStringIndex(offset));
        }

        protected override IList<string> GetActualValueImpl(IEngineValueEvaluator ticket, string name)
        {
            IList<string> list = ticket.GetStringValues(name);

            List<string> newList = new List<string>();
            foreach (var o in list)
            {
                if (null != o)
                {
                    newList.Add(o.ToLower());
                }
                else
                {
                    newList.Add(string.Empty);
                }
            }
            return newList;
        }
        internal override bool IsNonDefaultValue(string value)
        {
            if (value == null)
            {
                return false;
            }
            value = value.Trim();
            return string.IsNullOrEmpty(value) == false;
        }

    }
    class ExecutingEngineDateTimeValueGetter : AbstractExecutingEngineValueGetter<DateTime>
    {
        public ExecutingEngineDateTimeValueGetter(ExecutingEngineCompileCode compileCode) :
            base(compileCode, 8)
        {
        }
        internal override DateTime GetExpectedValue(int offset)
        {
            long ticks = CompileCode.GetLong(offset);
            return new DateTime(ticks);
        }

        protected override IList<DateTime> GetActualValueImpl(IEngineValueEvaluator ticket, string name)
        {
            return ticket.GetDateTimeValues(name);
        }

        internal override bool IsValidValue(DateTime value)
        {
            // 9/30/2013 dd - Do not consider DateTime.MinValue as valid value.
            // Stem from case 139691.
            return value != DateTime.MinValue;
        }

        internal override bool IsNonDefaultValue(DateTime value)
        {
            return value != DateTime.MinValue;
        }
    }
}
