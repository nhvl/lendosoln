﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConfigSystem.Engine
{
    public enum ExecutingEngineResultType
    {
        OperationNotFound,
        Allow,
        NotAllow

    }
    public class ExecutingEngineResult
    {
        public string OperationName { get; private set; }
        public ExecutingEngineResultType Result { get; private set; }

        public ExecutingEngineResult(string operation, ExecutingEngineResultType result)
        {
            this.OperationName = operation;
            this.Result = result;
        }
    }
}
