﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConfigSystem.Engine
{
    public interface IEngineValueEvaluator
    {
        IList<bool> GetBoolValues(string name);
        IList<int> GetIntValues(string name);
        IList<float> GetFloatValues(string name);
        IList<string> GetStringValues(string name);
        IList<DateTime> GetDateTimeValues(string name);

        bool IsDataModified(string name);
    }
}
