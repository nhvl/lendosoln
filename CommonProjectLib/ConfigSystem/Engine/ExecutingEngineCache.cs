﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConfigSystem.Engine
{
    internal enum E_ExecutingEngineModeT
    {
        Normal,
        Debug
    }
    internal class ExecutingEngineCache
    {
        public Dictionary<int, IList<int>> IntValueHash { get; private set; }

        public Dictionary<int, IList<bool>> BoolValueHash { get; private set; }

        public Dictionary<int, IList<float>> FloatValueHash { get; private set; }

        public Dictionary<int, IList<string>> StringValueHash { get; private set; }

        public Dictionary<int, IList<DateTime>> DateTimeValueHash { get; private set; }

        public Dictionary<int, bool> RuleResultHash { get; private set; }

        internal ExecutingEngineCache()
        {
            IntValueHash = new Dictionary<int, IList<int>>();
            BoolValueHash = new Dictionary<int, IList<bool>>();
            FloatValueHash = new Dictionary<int, IList<float>>();
            StringValueHash = new Dictionary<int, IList<string>>();
            DateTimeValueHash = new Dictionary<int, IList<DateTime>>();
            RuleResultHash = new Dictionary<int, bool>();

        }
    }
}
