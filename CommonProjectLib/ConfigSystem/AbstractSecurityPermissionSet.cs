﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Collections;
using CommonProjectLib.Common;

namespace ConfigSystem
{
    public abstract class AbstractSecurityPermissionSet : IEnumerable<AbstractConditionGroup>
    {
        protected List<AbstractConditionGroup> abstractConditionList = null;
        protected abstract string RootName { get; }
        protected abstract void Parse(XElement conditionConstraintSetRoot);
        public abstract void Copy(Guid id);

        protected static readonly Func<AbstractConditionGroup, string, bool> SearchFilter = (acg, searchText) =>
            acg.Id.ToString().ContainsIgnoreCase(searchText) ||
            acg.Notes.ContainsIgnoreCase(searchText) ||
            acg.FailureMessage.ContainsIgnoreCase(searchText) ||
            acg.ParameterSet.Any(param => param.Name.ContainsIgnoreCase(searchText)) ||
            acg.SysOpSet.Any(sysop => sysop.Fields.Any(opField => OperationFieldFilter(opField.Value, searchText)));

        protected static readonly Func<OperationField, string, bool> OperationFieldFilter = (opField, searchText) =>
            opField.Id.ContainsIgnoreCase(searchText) ||
            opField.FriendlyName.ContainsIgnoreCase(searchText) ||
            opField.Categories_rep.ContainsIgnoreCase(searchText);
        
        public void Remove(Guid id)
        {
            this.abstractConditionList.RemoveAll(acg => acg.Id == id);
        }

        public void Replace(Guid id, AbstractConditionGroup group)
        {
            var index = this.abstractConditionList.FindIndex(acg => acg.Id == id);

            if (index >= 0)
            {
                this.abstractConditionList[index] = group;
            }
        }

        public virtual AbstractConditionGroup Get(Guid id)
        {
            return this.abstractConditionList.Find(acg => acg.Id == id);
        }
        
        public AbstractSecurityPermissionSet(XElement conditionSetRoot)
        {
            this.abstractConditionList = new List<AbstractConditionGroup>();

            if (conditionSetRoot == null)
            {
                return;
            }

            Parse(conditionSetRoot);
        }

        public AbstractSecurityPermissionSet()
        {
            this.abstractConditionList = new List<AbstractConditionGroup>();
        }

        /// <summary>
        /// Generates the XML representing this object.
        /// </summary>
        /// <returns>The XElement that is the root of this object's XML tree</returns>
        public XElement GetXml()
        {
            // For backwards compatibility, i.e., so the XML version between release and dev is by
            // default identical (unless someone adds restraints to dev)
            if (abstractConditionList.Count == 0 && this is RestraintSet)
            {
                return null;
            }

            XElement root = new XElement(RootName);

            foreach (AbstractConditionGroup pg in abstractConditionList)
            {
                root.Add(pg.GetXml());
            }

            return root;
        }

        public void Filter(string searchText)
        {
            this.abstractConditionList.RemoveAll(acg => AbstractSecurityPermissionSet.SearchFilter(acg, searchText) == false);
        }

        #region IEnumerable Members

        public IEnumerator<AbstractConditionGroup> GetEnumerator()
        {
            return this.abstractConditionList.GetEnumerator();
        }

        #endregion

        #region IEnumerable<AbstractConditionGroup> Members

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.abstractConditionList.GetEnumerator();
        }

        #endregion
    }
}
