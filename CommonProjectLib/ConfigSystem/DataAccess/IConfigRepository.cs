﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommonProjectLib.Database;

namespace ConfigSystem.DataAccess
{
    /// <summary>
    /// Needs to be thread safe for multiple threads.
    /// </summary>
    public interface IConfigRepository 
    {
        BasicConfigData LoadDraft(Guid organizationId);
        ReleaseConfigData LoadActiveRelease(Guid organizationId);
        FilterSet LoadFilters(Guid organizationId);

        Nullable<DateTime> GetLastModifiedDateForActiveRelease(Guid organizationId);

        long GetConfigVersion(Guid organizationId);
        long? GetActiveReleaseId(Guid brokerId);

        long SaveReleaseConfig(Guid organizationId, SystemConfig config, Guid userId);
        void SaveDraftConfig(Guid organizationId, SystemConfig config);
        void SaveFilters(Guid organizationId, FilterSet filters);

        IEnumerable<ReleaseConfigData> GetActiveReleases();

        IEnumerable<BasicConfigData> GetDrafts();

        IEnumerable<ReleaseConfigData> GetAllReleasesFor(Guid organizationId);
    }
}
