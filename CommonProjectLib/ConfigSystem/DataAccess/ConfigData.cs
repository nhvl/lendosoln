﻿namespace ConfigSystem.DataAccess
{
    using System;
    using CommonProjectLib.Caching;

    public class BasicConfigData
    {

        public Guid OrganizationId { get; set; }

        public SystemConfig Configuration
        {
            get;
            set;
        }

        public BasicConfigData()
        {
            Configuration = new SystemConfig();
        }

    }

    public class ReleaseConfigData : BasicConfigData, IVersionable
    {
        public const long DefaultDummyVersion = long.MinValue;

        public DateTime? ReleaseDate { get; set; }
        public Guid? UserId { get; set; }
        public byte[] CompiledConfiguration { get; set; }
        public long Id { get; internal set; }
        public long Version { get; internal set; } = DefaultDummyVersion;
    }
}
