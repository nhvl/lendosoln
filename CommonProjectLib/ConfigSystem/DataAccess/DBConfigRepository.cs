﻿namespace ConfigSystem.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
    using System.Data.SqlClient;

    using CommonProjectLib.Common;
    using CommonProjectLib.Database;
    using ConfigSystem.Engine;

    /// <summary>
    /// Implements a sql config repository. Uses store procedures to do the loading and the saving. 
    /// </summary>
    public sealed class DBConfigRepository : IConfigRepository
    {
        private ISqlConnectionManager m_connectionManager;
        private Func<Guid, SystemConfig, Guid> WriteConfigToFileDb;
        private Func<Guid, Guid, SystemConfig> ReadConfigFromFileDb;

        private const string SP_LOAD_DRAFT= "CONFIG_LoadDrafts";
        private const string SP_LOAD_FILTERS = "CONFIG_LoadFilters";
        private const string SP_LOAD_RELEASE_OLD = "CONFIG_LoadLatestReleases";
        private const string SP_LOAD_RELEASE = "WORKFLOW_CONFIG_CURRENT_LoadLatestReleases";
        private const string SP_SAVE_DRAFT = "CONFIG_SaveDraft";
        private const string SP_SAVE_RELEASE = "WORKFLOW_CONFIG_CURRENT_SaveRelease";
        private const string SP_SAVE_FILTERS = "CONFIG_SaveFilters";
        private const string SP_LOAD_RELEASES = "CONFIG_LoadReleaseHistory";
        private const string SP_GET_LASTDATE = "CONFIG_GetLastModifiedDateForRelease";
        private const string SP_GET_VERSION = "WORKFLOW_CONFIG_CURRENT_GetVersion";
        private const string SP_GET_LASTCONFIGID = "WORKFLOW_CONFIG_CURRENT_GetConfigId";

        public DBConfigRepository(ISqlConnectionManager manager,
            Func<Guid, SystemConfig, Guid> writeConfigToFileDb,
            Func<Guid, Guid, SystemConfig> readConfigFromFileDb)
        {
            m_connectionManager = manager;
            this.WriteConfigToFileDb = writeConfigToFileDb;
            this.ReadConfigFromFileDb = readConfigFromFileDb;
        }


        #region IConfigRepository Members

        public BasicConfigData LoadDraft(Guid organizationId)
        {
            var connManager = GetConnectionManager(organizationId);

            using (DbDataReader reader = DBUtils.ExecuteReader(connManager, SP_LOAD_DRAFT, new SqlParameter("@OrgId", organizationId)))
            {
                BasicConfigData orgData = reader.Read() ? ReadBasicDataFrom(reader) : new BasicConfigData() { OrganizationId = organizationId };
                return orgData;
            }
        }

        public FilterSet LoadFilters(Guid organizationId)
        {
            var connManager = GetConnectionManager(organizationId);

            using (DbDataReader reader = DBUtils.ExecuteReader(connManager, SP_LOAD_FILTERS, new SqlParameter("@OrgId", organizationId)))
            {
                FilterSet filters;
                if (reader.Read())
                    filters = ReadFilterDataFrom(reader);
                else
                    filters = new FilterSet();

                return filters;
            }
        }

        public ReleaseConfigData LoadActiveRelease(Guid organizationId)
        {
            var connManager = GetConnectionManager(organizationId);

            using (DbDataReader reader = DBUtils.ExecuteReader(connManager, SP_LOAD_RELEASE, new SqlParameter("@BrokerId", organizationId)))
            {
                ReleaseConfigData orgData = reader.Read() ? ReadReleaseConfigData(reader) : new ReleaseConfigData() { OrganizationId = organizationId } ;
                return orgData;
            }
        }

        public long SaveReleaseConfig(Guid organizationId, SystemConfig config, Guid userId)
        {
            var connManager = GetConnectionManager(organizationId);

            DateTime versionDate = DateTime.Now;
            byte[] content = ExecutingEngineCompiler.Compile(config, versionDate);

            Guid fileDbKey = WriteConfigToFileDb(organizationId, config);

            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@BrokerId", organizationId),
                new SqlParameter("@ConfigXmlKey", fileDbKey),
                new SqlParameter("@CompiledConfig", content),
                new SqlParameter("@ModifyingUserId", userId),
                new SqlParameter("@ReleaseDate", versionDate),
                new SqlParameter("@ConfigurationXmlContent", config.ToString())
            };

            return (long)DBUtils.ExecuteScalar(connManager, SP_SAVE_RELEASE, parameters);
        }

        public void SaveFilters(Guid organizationId, FilterSet filters)
        {
            var connManager = GetConnectionManager(organizationId);

            DBUtils.ExecuteNonQuery(connManager, SP_SAVE_FILTERS, 0,
                new SqlParameter("@ConfigurationFilterXmlContent", filters.ToString()),
                new SqlParameter("@OrgId", organizationId));
        }

        public void SaveDraftConfig(Guid organizationId, SystemConfig config)
        {
            var connManager = GetConnectionManager(organizationId);

            DBUtils.ExecuteNonQuery(connManager, SP_SAVE_DRAFT, 0,
                new SqlParameter("@ConfigurationXmlContent", config.ToString()),
                new SqlParameter("@OrgId", organizationId));
        }

        public IEnumerable<ReleaseConfigData> GetActiveReleases()
        {
            List<ReleaseConfigData> releases = new List<ReleaseConfigData>();
            using (DbDataReader reader = DBUtils.ExecuteReader(m_connectionManager, SP_LOAD_RELEASE))
            {
                while (reader.Read())
                {
                    releases.Add(ReadReleaseConfigData(reader));
                }
            }

            return releases;
        }

        /// <summary>
        /// OBSOLETE. DO NOT USE.
        /// Gets all active releases. from the CONFIG_RELEASE table.
        /// </summary>
        /// <remarks>
        /// Needed for migration to fill new WORKFLOW_CONFIG_CURRENT table.
        /// Delete after 06/16/2018 release.
        /// </remarks>
        /// <returns>An Enumerable of releases.</returns>
        public IEnumerable<ReleaseConfigData> GetActiveReleases_OLD()
        {
            List<ReleaseConfigData> releases = new List<ReleaseConfigData>();
            using (DbDataReader reader = DBUtils.ExecuteReader(m_connectionManager, SP_LOAD_RELEASE_OLD))
            {
                while (reader.Read())
                {
                    releases.Add(ReadDataFromConfigReleaseTable(reader));
                }
            }

            return releases;
        }

        public IEnumerable<BasicConfigData> GetDrafts()
        {
            List<BasicConfigData> drafts = new List<BasicConfigData>();
            using (DbDataReader reader = DBUtils.ExecuteReader(m_connectionManager, SP_LOAD_DRAFT))
            {
                while (reader.Read())
                {
                    drafts.Add(ReadBasicDataFrom(reader));
                }
            }

            return drafts;
        }

       public IEnumerable<ReleaseConfigData> GetAllReleasesFor(Guid organizationId)
       {
           var connManager = GetConnectionManager(organizationId);

        List<ReleaseConfigData> releases = new List<ReleaseConfigData>();
           using( DbDataReader reader = DBUtils.ExecuteReader(connManager, SP_LOAD_RELEASES, new SqlParameter("@OrgId", organizationId)))
           {
                while( reader.Read())
                {
                    releases.Add(ReadDataFromConfigReleaseTable(reader));
                }
           }
           return releases;
        }

       public Nullable<DateTime> GetLastModifiedDateForActiveRelease(Guid organizationId)
       {
           var connManager = GetConnectionManager(organizationId);

           using (DbDataReader reader = DBUtils.ExecuteReader(connManager, SP_GET_LASTDATE, new SqlParameter("@OrgId", organizationId)))
           {
               if( reader.Read() )
               {
                   return (DateTime) reader["ReleaseDate"];
               }
           }

           return null;
       }


        #endregion

        #region helper methods
       private ISqlConnectionManager GetConnectionManager(Guid organizationId)
       {
           if (organizationId == ExecutingEngine.SystemOrganizationId)
           {
               return m_connectionManager.GetDefaultConnectionManager();
           }
           else
           {
               return m_connectionManager;
           }
       }

        private void FillBasicDataFromReader(BasicConfigData data, DbDataReader reader)
        {
            data.Configuration = new SystemConfig((string)reader["ConfigurationXmlContent"]);
            data.OrganizationId = (Guid)reader["OrgId"];
        }

        private FilterSet ReadFilterDataFrom(DbDataReader reader)
        {
            return new FilterSet((string) reader["ConfigurationFilterXmlContent"]);
        }

        private BasicConfigData ReadBasicDataFrom(DbDataReader reader)
        {

            BasicConfigData data = new BasicConfigData();
            FillBasicDataFromReader(data, reader);
            return data;
        }

        /// <summary>
        /// Creates <see cref="ReleaseConfigData"/> from data read from the CONFIG_RELEASE table in the DB.
        /// Unless you are looking for older configs, you likely want to use ReadReleaseConfigData instead.
        /// </summary>
        /// <param name="reader">DB data reader.</param>
        /// <returns>A <see cref="ReleaseConfigData"/> object.</returns>
        private ReleaseConfigData ReadDataFromConfigReleaseTable(DbDataReader reader)
        {
            ReleaseConfigData data = new ReleaseConfigData();
            FillBasicDataFromReader(data, reader);

            data.Id = (long)reader["ConfigId"];
            data.UserId = (Guid)reader["ModifyingUserId"];
            data.ReleaseDate = reader["ReleaseDate"] as DateTime?;

            if (reader["CompiledConfiguration"] != DBNull.Value)
            {
                data.CompiledConfiguration = (byte[])reader["CompiledConfiguration"];
            }
            return data;
        }

        /// <summary>
        /// Creates <see cref="ReleaseConfigData"/> from data read from DB.
        /// </summary>
        /// <param name="reader">DB data reader.</param>
        /// <returns>A <see cref="ReleaseConfigData"/> object.</returns>
        /// <remarks>Used to read active config data from the WORKFLOW_CONFIG_CURRENT table.</remarks>
        private ReleaseConfigData ReadReleaseConfigData(DbDataReader reader)
        {
            ReleaseConfigData data = new ReleaseConfigData();
            data.OrganizationId = (Guid)reader["BrokerId"];
            data.Id = (long)reader["ConfigId"];
            data.Version = (long)reader["Version"];
            data.UserId = (Guid)reader["ModifyingUserId"];
            data.ReleaseDate = reader["ReleaseDate"] as DateTime?;

            if (reader["CompiledConfig"] != DBNull.Value)
            {
                data.CompiledConfiguration = (byte[])reader["CompiledConfig"];
            }

            Guid configXmlKey = (Guid)reader["ConfigXmlKey"];
            data.Configuration = ReadConfigFromFileDb(data.OrganizationId, configXmlKey);

            return data;
        }

        public long GetConfigVersion(Guid organizationId)
        {
            var connManager = GetConnectionManager(organizationId);

            using (DbDataReader reader = DBUtils.ExecuteReader(connManager, SP_GET_VERSION, new SqlParameter("@BrokerId", organizationId)))
            {
                if (reader.Read())
                {
                    return (long)reader["Version"];
                }
                else
                {
                    // Return sentinel value if broker has not saved a config yet.
                    return ReleaseConfigData.DefaultDummyVersion;
                }
            }
        }

        public long? GetActiveReleaseId(Guid brokerId)
        {
            var connManager = GetConnectionManager(brokerId);

            using (DbDataReader reader = DBUtils.ExecuteReader(connManager, SP_GET_LASTCONFIGID, new SqlParameter("@BrokerId", brokerId)))
            {
                if (reader.Read())
                {
                    return (long)reader["ConfigId"];
                }
            }

            return null;
        }
        #endregion 
    }
}
