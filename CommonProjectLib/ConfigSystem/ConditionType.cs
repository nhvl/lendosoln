﻿namespace ConfigSystem
{
    /// <summary>
    /// Workflow condition type.
    /// </summary>
    public enum ConditionType
    {
        /// <summary>
        /// Invalid condition type.
        /// </summary>
        Invalid = -1,

        /// <summary>
        /// Workflow privilege.
        /// </summary>
        Privilege = 0,

        /// <summary>
        /// Workflow constraint.
        /// </summary>
        Constraint = 1,

        /// <summary>
        /// Workflow restraint.
        /// </summary>
        Restraint = 2
    }
}
