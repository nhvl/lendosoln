﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.IO;
using CommonProjectLib.Logging;
using System.Collections;
using CommonProjectLib.Common;

namespace ConfigSystem
{
    public class FilterSet : IEnumerable<Filter>
    {
        private List<Filter> m_filters = null;

        internal List<Filter> Filters
        {
            get { return this.m_filters; }
        }

        public FilterSet()
        {
            this.m_filters = new List<Filter>();
        }

        public FilterSet(XElement filterSetRoot)
        {
            this.m_filters = new List<Filter>();
            if (filterSetRoot != null)
            {
                Parse(filterSetRoot);
            }
        }

        public FilterSet(string xmlString)
        {
            this.m_filters = new List<Filter>();
            if (!string.IsNullOrEmpty(xmlString))
            {                
                XDocument doc = XDocument.Parse(xmlString);
                Parse(doc.Element("FilterSet"));
            }
        }

        public void Add(Filter f)
        {
            this.m_filters.Add(f);
        }

        public void Remove(string name)
        {
            this.m_filters.RemoveAll(filter => filter.Name.EqualsIgnoreCase(name));
        }

        public void RemoveAll()
        {
            this.m_filters.Clear();
        }

        public void Replace(string name, Filter replacementFilter)
        {
            var index = this.m_filters.FindIndex(filter => filter.Name.EqualsIgnoreCase(name));

            if (index >= 0)
            {
                this.m_filters[index] = replacementFilter;
            }
        }

        public void Copy(string name)
        {
            Filter filterToCopy = Get(name);
            if (filterToCopy != null)
            {
                XElement element = filterToCopy.GetXml();
                Filter copy = new Filter(element, name);
                this.m_filters.Add(copy);
            }
            else
            {
                throw CBaseException.CreateGeneric("CustomVariableSet:: Could not find filter with name " + name + " to copy.");
            }
        }

        public Filter Get(string name)
        {
            return this.m_filters.Find(filter => filter.Name.EqualsIgnoreCase(name));
        }

        private void Parse(XElement filterSetRoot)
        {
            foreach (XElement element in filterSetRoot.Elements("Filter"))
            {
                this.m_filters.Add(new Filter(element));
            }
        }

        /// <summary>
        /// Generates the XML representing this object.
        /// </summary>
        /// <returns>The XElement that is the root of this object's XML tree</returns>
        public XElement GetXml()
        {
            XElement root = new XElement("FilterSet");
            foreach (Filter f in this.m_filters)
            {
                root.Add(f.GetXml());
            }

            return root;
        }

        public override string ToString()
        {
            return GetXml().ToString();
        }

        #region IEnumerable<Filter> Members

        public IEnumerator<Filter> GetEnumerator()
        {
            return this.m_filters.GetEnumerator();
        }

        #endregion

        #region IEnumerable Members

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.m_filters.GetEnumerator();
        }

        #endregion
    }
}
