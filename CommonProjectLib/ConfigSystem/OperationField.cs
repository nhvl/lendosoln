﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace ConfigSystem
{
    /// <summary>
    /// Represents a field for use in conditions. Fields will be placed in
    /// fieldsets for use with field-level write and field-level protection
    /// privileges.
    /// </summary>
    public class OperationField
    {
        private char CATEGORY_SEPARATOR = ';';

        private string m_friendlyName = "";
        /// <summary>
        /// The name of the field. "{FormField.FriendlyName} ({FormField.FieldName})"
        /// </summary>
        public string FriendlyName
        {
            get
            {
                return m_friendlyName;
            }
        }

        private string m_id = "";
        /// <summary>
        /// If the field is an enum, then this refers to the specific enum that is allowed.
        /// There may be multiple fields with the same name, but different enums.
        /// </summary>
        public string Id
        {
            get
            {
                return m_id;
            }
            set
            {
                m_id = value;
            }
        }

        private HashSet<string> m_categories;
        public HashSet<string> Categories
        {
            get
            {
                return m_categories;
            }
        }

        public string Categories_rep
        {
            get
            {
                return string.Join(CATEGORY_SEPARATOR.ToString(), Categories.ToArray());
            }
        }

        private bool m_clearable = false;
        /// <summary>
        /// Bool stating whether a writable date or text field can also be cleared.
        /// Refer to 4.2.1.3.1.2 and 4.2.2.5.1 of the Task Backend Business Rules spec.
        /// </summary>
        public bool Clearable
        {
            get
            {
                return m_clearable;
            }
            set
            {
                m_clearable = value;
            }
        }

        private bool m_updateable = false;
        /// <summary>
        /// Bool stating whether a date or text field can be updated (but not cleared.
        /// Refer to 4.3.1.2.6 and 4.3.3.1 of the Task Backend Business Rules spec.
        /// </summary>
        public bool Updateable
        {
            get
            {
                return m_updateable;
            }
            set
            {
                m_updateable = value;
            }
        }

        private bool m_canWrite = false;
        /// <summary>
        /// Bool that says whether a field is writable or not. This is used when
        /// populating the writable fields list.
        /// This is not stored in the XML, it's only used when generating the
        /// operations list from the FieldEnumerator in EditWorkflowConfig.cs.
        /// </summary>
        public bool CanWrite
        {
            get
            {
                return m_canWrite;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="friendlyName"></param>
        /// <param name="id">The field id used internally.</param>
        /// <param name="category">A string containing a list of CATEGORY_SEPARATOR-separated values.</param>
        /// <param name="clearable">Applies to writable fields.</param>
        /// <param name="updateable">Applies to protected fields.</param>
        /// <param name="canWrite">Only needed when populating the writable fields list</param>
        public OperationField(string friendlyName, string id, string category, bool clearable, bool updateable, bool canWrite)
        {
            m_friendlyName = friendlyName;
            m_id = id;
            m_categories = new HashSet<string>();
            if (!string.IsNullOrEmpty(category))
            {
                var categories = category.Split(CATEGORY_SEPARATOR);
                foreach (var item in categories)
                {
                    m_categories.Add(item);
                }
            }
            m_clearable = clearable;
            m_updateable = updateable;
            m_canWrite = canWrite; // NOT in the XML!
        }

        public OperationField(string friendlyName, string id, string category, bool clearable, bool updateable)
            : this(friendlyName, id, category, clearable, updateable, true)
        {

        }

        public OperationField(XElement fieldRoot)
        {
            Parse(fieldRoot);
        }

        public void Parse(XElement fieldRoot)
        {
            var friendlyName = fieldRoot.Attribute("friendlyName");
            m_friendlyName = friendlyName != null ? friendlyName.Value : "";

            var id = fieldRoot.Attribute("id");
            m_id = id != null ? id.Value : "";

            m_categories = new HashSet<string>();
            var categoryAttr = fieldRoot.Attribute("category"); // category contains a list of CATEGORY_SEPARATOR-separated fields
            if (categoryAttr != null && !string.IsNullOrEmpty(categoryAttr.Value))
            {
                var categories = categoryAttr.Value.Split(CATEGORY_SEPARATOR);
                foreach (var category in categories)
                {
                    m_categories.Add(category);
                }
            }

            var clearable = fieldRoot.Attribute("clearable");
            m_clearable = clearable != null ? bool.Parse(clearable.Value) : false; // TODO: what's the default value here?

            var updateable = fieldRoot.Attribute("updateable");
            m_updateable = updateable != null ? bool.Parse(updateable.Value) : false; // TODO: what's the default value here?

            m_canWrite = true;

            Validate();
        }

        private void Validate()
        {
            // MPTODO
        }

        /// <summary>
        /// Generates the XML representing this object.
        /// </summary>
        /// <returns>The XElement that is the root of this object's XML tree</returns>
        public XElement GetXml()
        {
            XElement root = new XElement("Field");
            root.Add(new XAttribute("friendlyName", this.FriendlyName));
            root.Add(new XAttribute("id", this.Id));
            root.Add(new XAttribute("category", string.Join(CATEGORY_SEPARATOR.ToString(), this.Categories.ToArray())));
            root.Add(new XAttribute("clearable", this.Clearable));
            root.Add(new XAttribute("updateable", this.Updateable));
            return root;
        }
    }
}
