﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using CommonProjectLib.Logging;
using CommonProjectLib.Common;

namespace ConfigSystem
{
    public abstract class AbstractConditionGroup
    {
        private SystemOperationSet m_sysOpSet = null;
        private ParameterSet m_paramSet = null;
        private string m_failureMessage = "";
        private string m_notes = "";
        private Guid m_id = Guid.Empty;
        private bool m_excludeFromRWExport = false;

        protected abstract string RootName { get; }

        public SystemOperationSet SysOpSet
        {
            get { return m_sysOpSet; }
            set { m_sysOpSet = value; }
        }

        public ParameterSet ParameterSet
        {
            get { return m_paramSet; }
            set { m_paramSet = value; }
        }

        public string FailureMessage
        {
            get { return m_failureMessage; }
            set { m_failureMessage = value; }
        }

        public string Notes
        {
            get { return m_notes; }
            set { m_notes = value; }
        }

        public bool ExcludeFromRWExport
        {
            get { return m_excludeFromRWExport; }
            set { m_excludeFromRWExport = value; }
        }

        public virtual Guid Id
        {
            get { return m_id; }
        }

        public IEnumerable<string> SystemOperationNames
        {
            get 
            {
                if (m_sysOpSet == null)
                {
                    throw new CBaseException("System Error.", "AbstractConditionGroup:: 'OperationNames' has not been instantiated yet.");
                }
                return m_sysOpSet.SystemOperationNames; 
            }
        }

        public AbstractConditionGroup(XElement conditionRoot)
        {
            Parse(conditionRoot);
        }

        public AbstractConditionGroup(XElement conditionRoot, Guid newId)
        {
            Parse(conditionRoot);
            m_id = newId;
        }

        public AbstractConditionGroup()
        {
            m_sysOpSet = new SystemOperationSet();
            m_paramSet = new ParameterSet();
            m_id = Guid.NewGuid();
        }

        private void Parse(XElement conditionRoot)
        {
            XAttribute idAtt = conditionRoot.Attribute("id");
            m_id = new Guid(idAtt.Value.Trim());

            XAttribute excludeAtt = RootName == "Condition" ? conditionRoot.Attribute("excludeFromRWExport") : null;
            m_excludeFromRWExport = excludeAtt != null && bool.Parse(excludeAtt.Value);

            XElement systemOperationSetElement = conditionRoot.Element("ApplicableSystemOperationSet");
            m_sysOpSet = new SystemOperationSet(systemOperationSetElement);

            XElement parameterSetElement = conditionRoot.Element("ParameterSet");
            m_paramSet = new ParameterSet(parameterSetElement);

            XElement failureMessageElement = conditionRoot.Element("ConditionFailureMessage");
            if (failureMessageElement != null)
                m_failureMessage = failureMessageElement.Value.Trim();

            XElement notesElement = conditionRoot.Element("Notes");
            m_notes = notesElement.Value.Trim();
        }

        /// <summary>
        /// Generates the XML representing this object.
        /// </summary>
        /// <returns>The XElement that is the root of this object's XML tree</returns>
        public XElement GetXml()
        {
            XElement root = new XElement(RootName);
            root.Add(new XAttribute("id", m_id.ToString()));
            if (RootName == "Condition" && m_excludeFromRWExport)
            {
                root.Add(new XAttribute("excludeFromRWExport", "true"));
            }

            root.Add(new XElement(m_paramSet.GetXml()));
            root.Add(new XElement(m_sysOpSet.GetXml()));
            if (!string.IsNullOrEmpty(m_failureMessage))
            {
                root.Add(new XElement("ConditionFailureMessage", new XCData(m_failureMessage)));
            }
            root.Add(new XElement("Notes", new XCData(m_notes)));

            return root;
        }
    }
}
