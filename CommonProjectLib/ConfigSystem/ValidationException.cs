﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommonProjectLib.Common;

namespace ConfigSystem
{
    public class ValidationException : CBaseException
    {
        public ValidationException(string userMsg, string devMsg)
            : base(userMsg, devMsg)
        {

        }
    }
}
