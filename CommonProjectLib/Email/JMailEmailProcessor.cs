﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendingQBPop3Mail;
using CommonProjectLib.Logging;

namespace CommonProjectLib.Email
{
    /// <summary>
    /// Requires Log Manager setup
    /// </summary>
    public class JMailEmailProcessor : IDisposable
    {

        private Pop3Class pop3;

        public JMailEmailProcessor(string emailAddress, string password, string server, int port, bool useSsl)
        {
            Pop3Class pop3Server = new Pop3Class();
            pop3Server.Connect(emailAddress, password, server, port, useSsl);
            pop3 = pop3Server;
        }

        public int EmailCount
        {
            get
            {
                return pop3.Count;
            }
        }



        public IEnumerable<Pop3Message> GetEmailMessages()
        {
            for (var i = 1; i <= pop3.Count; i++)
            {
                yield return pop3.GetMessage(i);
            }
        }


        public IEnumerable<Pop3Message> GetEmailMessages(string stopAtUniqueId)
        {
            for (var i = 1; i <= pop3.Count; i++)
            {
                if (pop3.GetMessageUID(i) == stopAtUniqueId)
                {
                    break;
                }
                yield return pop3.GetMessage(i);
            }
        }


        /// <summary>
        /// 0 based index of GetEmailmessages
        /// </summary>
        /// <param name="index"></param>
        public void DeleteMessage(int index)
        {
            pop3.DeleteSingleMessage(index + 1);
        }

        public string GetMessageUniqueId(int index)
        {
            return pop3.GetMessageUID(index + 1);
        }

        #region IDisposable Members

        public void Dispose()
        {
            if (pop3 != null)
            {
                pop3.Disconnect();
            }
        }

        #endregion
    }
}
