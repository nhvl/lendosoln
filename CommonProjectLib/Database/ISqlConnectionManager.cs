﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;

namespace CommonProjectLib.Database
{
    /// <summary>
    /// In case we need to add events and logging to these connections.
    /// </summary>
    public interface ISqlConnectionManager
    {
        DbConnection GetConnection();
        string ConnectionName { get;  }
        ISqlConnectionManager GetDefaultConnectionManager();
    }
}
