﻿using System;
using System.Data;
using System.Data.Common;
using System.Text;
using CommonProjectLib.Common;
using CommonProjectLib.Logging;
using Adapter;
using System.Data.SqlClient;

namespace CommonProjectLib.Database
{
    public class CStoredProcedureExec : IDisposable
    {
        private DbConnection m_conn = null;
        private DbTransaction m_tx = null;
        private ISqlConnectionManager m_connectionManager; 

        public String CurrentConnectionState
        {
            // Check the associated connection state.

            get
            {
                // We are really checking the transaction instance to see if it will
                // endure a commit or rollback call.

                if (m_tx != null && m_tx.Connection != null)
                {
                    return m_tx.Connection.State.ToString();
                }

                return "Null";
            }
        }



        public CStoredProcedureExec(ISqlConnectionManager connection)
        {
            m_connectionManager = connection; 
        }

        public void BeginTransactionForWrite()
        {
            OpenConnection();
            // Changing from Serializable to RepeatableRead 10/15/04
            m_tx = m_conn.BeginTransaction(IsolationLevel.RepeatableRead);
        }

        private void OpenConnection()
        {
            m_conn = m_connectionManager.GetConnection();
            m_conn.OpenWithRetry();
        }

        public void CommitTransaction()
        {
            if (null != m_tx)
            {
                m_tx.Commit();
                m_tx = null;
            }
        }
        public void RollbackTransaction()
        {
            if (null != m_tx)
            {
                m_tx.Rollback();
                m_tx = null;
            }
        }

        public int ExecuteNonQuery(string procedureName, int nRetry, bool bSendOnError, params DbParameter[] parameters)
        {
            return ExecuteNonQueryWithCustomTimeout(procedureName, nRetry, 0, bSendOnError, parameters);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="procedureName"></param>
        /// <param name="nRetry"></param>
        /// <param name="nTimeoutSeconds">Passing <= 0 to accept defaults which is less than 30 seconds </param>
        /// <param name="bSendOnError"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        private int ExecuteNonQueryWithCustomTimeout(string procedureName, int nRetry, int nTimeoutSeconds, bool bSendOnError, params DbParameter[] parameters)
        {
            // Initialize new command using the given parameters and
            // our embedded transaction.

            DbCommand command = this.m_conn.CreateCommand();
            command.CommandText = procedureName;
            command.Transaction = m_tx;

            if (nTimeoutSeconds > 0)
                command.CommandTimeout = nTimeoutSeconds; // NOTE: passing 0 means waiting for ever, we don't allow that.

            command.CommandType = CommandType.StoredProcedure;

            if (parameters != null)
            {
                foreach (DbParameter p in parameters)
                {
                    command.Parameters.Add(p);
                }
            }

            // We attempt retry + 1 executions to get this command to
            // work.  If we exceed the execution count, we throw the
            // last offending exception for logging.

            try
            {
                int i = 0;

                while (i++ <= nRetry)
                {
                    // Attempt this stored procedure invocation.  On
                    // failure, we pass over the return statement and
                    // log the error and try again.

                    try
                    {
                        int ret = command.ExecuteNonQuery();
                        return ret;
                    }
                    catch (DbException exc)
                    {
                        LogManager.Warn
                            (String.Format
                            ("Failed to executing stored procedure {0} , attempt #{1} ({2})."
                            , procedureName
                            , i
                            , exc.Message
                            )
                            , exc
                            );

                        if (i <= nRetry)
                        {
                            Tools.SleepAndWakeupRandomlyWithin(1000, 4500);
                        }
                        else
                        {
                            // We have exceeded our retries.  Log this
                            // error and move on.

                            throw;
                        }
                    }
                }

                // We should have never reached this point -- the
                // exception catch should always be executed, otherwise
                // we would have returned.

                throw CBaseException.CreateGeneric(
                    "Failed to execute stored procedure, but didn't catch errors !?!"
                    );
            }
            catch (DbException exc)
            {

                RollbackTransaction();

                StringBuilder sb = new StringBuilder();
                sb.Append(Environment.NewLine);
                sb.Append("<!---- Stored Procedure Debug Info (ExecuteNonQuery) ---->").Append(Environment.NewLine);
                sb.AppendFormat("Procedure name = {0}{1}", procedureName, Environment.NewLine);
                if (parameters != null)
                {
                    foreach (DbParameter p in parameters)
                    {
                        sb.AppendFormat("  {0} = {1}{2}", p.ParameterName, p.Value, Environment.NewLine);
                    }
                }
                sb.Append("<!----- End Stored Procedure Debug Info ---->").Append(Environment.NewLine);


                LogManager.Error(sb.ToString(), exc);

                throw;
            }
        }
        public int ExecuteNonQuery(string procedureName, int nRetry, params DbParameter[] parameters)
        {
            // Delegate to method and assume we don't send on error.

            return ExecuteNonQuery(procedureName, nRetry, false, parameters);
        }

        public DbDataReader ExecuteReader(string procedureName, params DbParameter[] parameters)
        {
            DbCommand command = this.m_conn.CreateCommand();
            command.CommandText = procedureName;
            command.Transaction = m_tx;
            command.CommandType = CommandType.StoredProcedure;

            if (parameters != null)
            {
                foreach (DbParameter p in parameters)
                {
                    command.Parameters.Add(p);
                }
            }

            try
            {
                return command.ExecuteReader();
            }
            catch (DbException exc)
            {
                RollbackTransaction();

                StringBuilder sb = new StringBuilder();
                sb.Append(Environment.NewLine);
                sb.Append("<!---- Sql Text Debug Info (ExecuteReader) ---->").Append(Environment.NewLine);
                sb.AppendFormat("Procedure = {0}{1}", procedureName, Environment.NewLine);
                sb.Append("<!----- End Sql Text Debug Info ---->").Append(Environment.NewLine);


                LogManager.Error(sb.ToString(), exc);
                throw;
            }
        }

        public void Dispose()
        {
            if (null != m_conn )
            {
                m_conn.Dispose();
            }
        }

        public object ExecuteScalar(string procedureName, params DbParameter[] parameters)
        {
            DbCommand command = this.m_conn.CreateCommand();
            command.CommandText = procedureName;
            command.Transaction = m_tx;           

            command.CommandType = CommandType.StoredProcedure;

            if (parameters != null)
            {
                foreach (DbParameter p in parameters)
                {
                    command.Parameters.Add(p);
                }
            }

            if (m_conn.State != ConnectionState.Open)
            {
                m_conn.Open();
            }

            try
            {
                return command.ExecuteScalar();
            }
            catch (SqlException exc)
            {
                LogManager.Error("ExecuteScalar" + procedureName, exc);
                throw;
            }
        }
    }
}
