﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Text;
using Adapter;
using CommonProjectLib.Common;
using CommonProjectLib.Logging;

namespace CommonProjectLib.Database
{
    public static class DBUtils
    {
        #region ExecuteNonQuery methods


        /// <summary>
        /// Executes an update store procedure returning the number of rows affected. If the store procedure name fails to execute the thread will sleep and try again nRetry + 1 times. 
        /// </summary>
        /// <param name="connectionManager"></param>
        /// <param name="procedureName">name of store procedure to execute</param>
        /// <param name="nRetry">0 or greater</param>
        /// <param name="parameters">Sql Parameters to pass to the store procedure</param>
        /// <returns>Number of rows affected</returns>
        public static int ExecuteNonQuery(ISqlConnectionManager connectionManager, string procedureName, int nRetry, params DbParameter[] parameters)
        {
            if (nRetry < 0)
            {
                throw CBaseException.CreateGeneric("Invalid nRetry cannot be less than 0");
            }

            Exception exc = null;
            int nExec = nRetry + 1;
            for (int i = 0; i < nExec; ++i)
            {
                try
                {
                    return ExecuteNonQueryHelper(connectionManager, procedureName, parameters);
                }
                catch (Exception e)
                {
                    exc = e;
                    LogManager.Warn(string.Format("Failed to executing store procedure {0}, exec count ={1}, out of {2} total # of tries.",
                        procedureName, i.ToString(), nRetry.ToString()), exc);
                    if ((i + 1) < nExec) // not the last time.
                        Tools.SleepAndWakeupRandomlyWithin(1000, 4500);
                }
            }

            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("Failed to executing store procedure {0}, after {1} tries.", procedureName, nRetry.ToString());
            sb.Append(Environment.NewLine);
            sb.AppendFormat("Data Source={0}", connectionManager.ConnectionName);
            sb.Append("<!---- Stored Procedure Debug Info (ExecuteNonQuery) ---->").Append(Environment.NewLine);
            sb.AppendFormat("Procedure name = {0}{1}", procedureName, Environment.NewLine);
            if (parameters != null)
            {
                foreach (SqlParameter p in parameters)
                {
                    sb.AppendFormat("  {0} = {1}{2}", p.ParameterName, p.Value, Environment.NewLine);
                }
            }
            sb.Append("<!----- End Stored Procedure Debug Info ---->").Append(Environment.NewLine);

            if (exc is SqlException)
            {
                SqlException sqlExc = (SqlException)exc;
                sb.Append("SqlException.Number = " + sqlExc.Number).Append(Environment.NewLine);
                LogManager.Error(sb.ToString(), sqlExc);

            }
            else
            {
                LogManager.Error(sb.ToString(), exc);
            }

            throw exc;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="procedureName"></param>
        /// <param name="nRetry"></param>
        /// <param name="parameters"></param>
        /// <returns>number of rows affected</returns>
        private static int ExecuteNonQueryHelper(ISqlConnectionManager connectionManager, string procedureName, params DbParameter[] parameters)
        {

            using (var conn = connectionManager.GetConnection())
            using (DbCommand command = conn.CreateCommand())
            {
                command.CommandText = procedureName;
                command.CommandType = CommandType.StoredProcedure;

                if (parameters != null)
                {
                    foreach (DbParameter p in parameters)
                    {
                        command.Parameters.Add((DbParameter)((ICloneable)p).Clone());
                    }
                }

                conn.OpenWithRetry();

                int ret = command.ExecuteNonQuery();
                if (parameters != null)
                {
                    // 9/7/2006 dd - Since we are cloning the SqlParameters, we need to set the value of the new SqlParamter to the original argument.
                    foreach (DbParameter p in parameters)
                    {
                        p.Value = command.Parameters[p.ParameterName].Value;
                    }
                }
                return ret;
            }

        }
        #endregion


        #region ExecuteScalar methods

        public static object ExecuteScalar(ISqlConnectionManager connectionManager, string procedureName, params DbParameter[] parameters)
        {
            using (DbConnection conn = connectionManager.GetConnection())
            using (DbCommand command = conn.CreateCommand())
            {
                command.CommandText = procedureName;
                command.CommandType = CommandType.StoredProcedure;
                if (parameters != null)
                {
                    foreach (DbParameter p in parameters)
                    {
                        command.Parameters.Add(p);
                    }
                }
                conn.OpenWithRetry();
                try
                {
                    return command.ExecuteScalar();
                }
                catch (SqlException exc)
                {
                    LogDetailException(connectionManager, "ExecuteScalar", procedureName, exc, parameters);
                    throw;
                }
            }
        }
        #endregion

        #region ExecuteReader methods




        public static DbDataReader ExecuteReader(ISqlConnectionManager connectionManager, string procedureName, params DbParameter[] parameters)
        {
            DbConnection conn = connectionManager.GetConnection();
            DbCommand command = conn.CreateCommand();
            command.CommandText = procedureName;
            command.CommandType = CommandType.StoredProcedure;

            if (parameters != null)
            {
                foreach (DbParameter p in parameters)
                {
                    command.Parameters.Add(p);
                }
            }
            conn.OpenWithRetry();
            DbDataReader reader = null;
            try
            {
                reader = command.ExecuteReader(CommandBehavior.CloseConnection);
                return reader;
            }
            catch (Exception exc)
            {
                if (exc is SqlException)
                {
                    LogDetailException(connectionManager, "ExecuteReader", procedureName, (SqlException) exc, parameters);
                }
                if (reader != null)
                {
                    reader.Dispose();
                }
                if (conn != null)
                {
                    conn.Close();
                }
                throw;
            }

     
        }

        #endregion

        #region ExecuteReaderWithRetries


        public static DbDataReader ExecuteReaderWithRetries(ISqlConnectionManager connectionManager, string procedureName, int nRetries, params SqlParameter[] parameters)
        {

            if (nRetries < 0)
            {
                throw CBaseException.CreateGeneric("ExecuteReaderWithRetries executed with invalid nRetries"); 
            }

            Exception lastException = null;

            int nExec = nRetries + 1;

            for (int i = 0; i < nExec; i++)
            {
                try
                {
                    return ExecuteReaderWithRetriesHelper(connectionManager, procedureName, parameters);
                }
                catch (Exception exc)
                {
                    lastException = exc;
                }
                Tools.SleepAndWakeupRandomlyWithin(1000, 4500);
            }

            SqlException sqlException = lastException as SqlException;
            if (null != sqlException)
                LogDetailException(connectionManager, "ExecuteReaderWithRetries", procedureName, sqlException, parameters);

            throw lastException;
        }

        private static DbDataReader ExecuteReaderWithRetriesHelper(ISqlConnectionManager connectionManager, string procedureName, params DbParameter[] parameters)
        {

            DbConnection conn = connectionManager.GetConnection();
            DbCommand command = conn.CreateCommand();
            command.CommandText = procedureName;
            command.CommandType = CommandType.StoredProcedure;

            if (parameters != null)
            {
                foreach (DbParameter p in parameters)
                {
                    command.Parameters.Add((DbParameter)((ICloneable)p).Clone());
                }
            }
            DbDataReader reader = null;
            try
            {
                conn.OpenWithRetry();
                reader = command.ExecuteReader(CommandBehavior.CloseConnection);
                return reader;
            }
            catch( Exception )
            {
                if (conn != null)
                {
                    conn.Dispose();
                }

                if (reader != null)
                {
                    reader.Dispose();
                }
                throw;
            }
           
        }

        #endregion
        private static void LogDetailException(ISqlConnectionManager connectionManager, string sqlMethod, string procedureName, SqlException exc, params DbParameter[] parameters)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("{1}<!---- Stored Procedure Debug Info ({0}) ---->{1}", sqlMethod, Environment.NewLine);
            sb.AppendFormat("DataSrc = {0}{1}", connectionManager.ConnectionName, Environment.NewLine);
            sb.AppendFormat("Procedure name = {0}{1}", procedureName, Environment.NewLine);
            if (parameters != null)
            {
                foreach (DbParameter p in parameters)
                {
                    sb.AppendFormat("  {0} = {1}{2}", p.ParameterName, p.Value, Environment.NewLine);
                }
            }
            sb.AppendFormat("<!----- End Stored Procedure Debug Info ---->{0}", Environment.NewLine);
            sb.AppendFormat("SqlException.Number = {0}{1}", exc.Number, Environment.NewLine);

            LogManager.Error(sb.ToString(), exc);
        }
    }
}
