﻿namespace CommonProjectLib.Caching
{
    /// <summary>
    /// A type that can be versioned.
    /// </summary>
    public interface IVersionable
    {
        /// <summary>
        /// Gets the version.
        /// </summary>
        /// <value>
        /// The version.
        /// </value>
        long Version { get; }
    }
}
