﻿// <copyright file="MemoryCacheUtilities.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: David Dao
//    Date:   10/22/2015 
// </summary>

namespace CommonProjectLib.Caching
{
    using System;
    using System.Runtime.Caching;

    /// <summary>
    /// An helper that will store item in memory cache. The idea is based on http://blog.falafel.com/working-system-runtime-caching-memorycache/.
    /// </summary>
    public static class MemoryCacheUtilities
    {
        /// <summary>
        /// A memory cache instance.
        /// </summary>
        private static MemoryCache cache = new MemoryCache("LendingQBCache");

        /// <summary>
        /// Get the item if it is already existed in cache. Otherwise invoke the value factory to create the object.
        /// This method is thread safe.
        /// </summary>
        /// <typeparam name="T">Type of the object.</typeparam>
        /// <param name="key">A unique identifier for the cache entry to add.</param>
        /// <param name="valueFactory">The factory for create the object if key does not existed.</param>
        /// <param name="absoluteExpiration">The fixed date and time at which the cache entry will expire.</param>
        /// <returns>The data for the cache entry.</returns>
        public static T GetOrAddExisting<T>(string key, Func<T> valueFactory, DateTimeOffset absoluteExpiration)
        {
            var newValue = new Lazy<T>(valueFactory);
            var oldValue = cache.AddOrGetExisting(key, newValue, absoluteExpiration) as Lazy<T>;

            try
            {
                return (oldValue ?? newValue).Value;
            }
            catch
            {
                // Handle cached lazy exception by evicting from cache.
                cache.Remove(key);
                throw;
            }
        }

        /// <summary>
        /// Get the item if it is already existed in cache. Otherwise invoke the value factory to create the object.
        /// This method is thread safe.
        /// </summary>
        /// <typeparam name="T">Type of the object.</typeparam>
        /// <param name="key">A unique identifier for the cache entry to add.</param>
        /// <param name="valueFactory">The factory for create the object if key does not existed.</param>
        /// <param name="slidingExpiration">Cache entry should be evict if it has not accessed in the given time span.</param>
        /// <returns>The data for the cache entry.</returns>
        public static T GetOrAddExisting<T>(string key, Func<T> valueFactory, TimeSpan slidingExpiration)
        {
            CacheItemPolicy policy = new CacheItemPolicy();
            policy.SlidingExpiration = slidingExpiration;

            var newValue = new Lazy<T>(valueFactory);
            var oldValue = cache.AddOrGetExisting(key, newValue, policy) as Lazy<T>;

            try
            {
                return (oldValue ?? newValue).Value;
            }
            catch
            {
                // Handle cached lazy exception by evicting from cache.
                cache.Remove(key);
                throw;
            }
        }

        /// <summary>
        /// Get the item if it is already existed in cache and has an up-to-date version. 
        /// Otherwise invoke the value factory to create the object.
        /// This method is thread safe.
        /// </summary>
        /// <typeparam name="T">Type of the object.</typeparam>
        /// <param name="key">A unique identifier for the cache entry to add.</param>
        /// <param name="currentVersionGetter">The getter for the current version of <typeparamref name="T"/>.</param>
        /// <param name="valueFactory">The factory for create the object if key does not existed.</param>
        /// <param name="slidingExpiration">Cache entry should be evict if it has not accessed in the given time span.</param>
        /// <returns>The data for the cache entry.</returns>
        public static T GetOrAddExistingWithVersioning<T>(string key, Func<long> currentVersionGetter, Func<T> valueFactory, TimeSpan slidingExpiration) where T : IVersionable
        {
            var cacheValue = GetOrAddExisting(key, valueFactory, slidingExpiration);
            var currentVersion = currentVersionGetter();

            if (cacheValue == null || cacheValue.Version < currentVersion)
            {
                cache.Remove(key);
                return GetOrAddExisting(key, valueFactory, slidingExpiration);
            }

            return cacheValue;
        }
    }
}