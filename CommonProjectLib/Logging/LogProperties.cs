﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonProjectLib.Logging
{
    public class LogProperties
    {
        private Dictionary<string, string> m_calleeProperties;
        private string m_message;
        private LocationInfo m_locationInfo; 

        public string Message { get { return m_message; } }
        public LocationInfo LocationInfo { get { return m_locationInfo; } }
        public Dictionary<string, string> CalleeProperties { get { return m_calleeProperties; } }




        public LogProperties(string message, Dictionary<string, string> calleeProperties)
        {
            m_message = message;
            m_calleeProperties = calleeProperties ?? new Dictionary<string,string>();
        }

        public LogProperties(string message, Dictionary<string, string> calleeProperties, LocationInfo info) : this(message,calleeProperties)
        {
            m_locationInfo = info;
        }
          

    }
}
