﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Reflection;

namespace CommonProjectLib.Logging
{
    public class LocationInfo
    {
        private string m_className = "";
        private string m_fileName = "";
        private int m_lineNumber = 0;
	    private string m_methodName = "";
        private StringBuilder m_stackTraceFromUserCode = new StringBuilder();

        public string ClassName { get { return m_className; } }
        public string FileName { get { return m_fileName; } }
        public string MethodName { get { return m_methodName; } }
        public int LineNumber { get { return m_lineNumber; } }
        public String UserStackTrace { get { return m_stackTraceFromUserCode.ToString(); } }


        public LocationInfo(Type boundaryDeclaringType)
        {
            if (boundaryDeclaringType == null)
            {
                return;
            }

            try
            {
                int frameIndex = 0;
                StackTrace fullStackTrace = new StackTrace(true);

                //go down to the boundary type. 
                while (frameIndex < fullStackTrace.FrameCount)
                {
                    StackFrame frame = fullStackTrace.GetFrame(frameIndex);
                    if (frame != null && frame.GetMethod().DeclaringType == boundaryDeclaringType)
                    {
                        break;
                    }
                    frameIndex++;
                }
                //skip over the boundary type
                while (frameIndex < fullStackTrace.FrameCount)
                {
                    StackFrame frame = fullStackTrace.GetFrame(frameIndex);
                    if (frame != null && frame.GetMethod().DeclaringType != boundaryDeclaringType)
                    {
                        break;
                    }
                    frameIndex++;
                }

                //now we are at the user call 
                //if we are out of range then bail.
                if (frameIndex >= fullStackTrace.FrameCount)
                {
                    return;
                }
                for (int i = frameIndex; i < fullStackTrace.FrameCount; i++)
                {
                    StackFrame frame = fullStackTrace.GetFrame(i);
                    if( frame == null ) 
                    { 
                        break; 
                    }
                    m_stackTraceFromUserCode.Append(frame.ToString());
                }
                //This is the first call before boundary 
                StackFrame userLocationFrame = fullStackTrace.GetFrame(frameIndex);
                
                if (userLocationFrame == null)
                {
                    return;
                }

                m_lineNumber = userLocationFrame.GetFileLineNumber();
                m_fileName = userLocationFrame.GetFileName() ?? "";

                MethodBase callee = userLocationFrame.GetMethod();

                if (callee == null)
                {
                    return;
                }

                m_methodName = callee.Name;
                if (callee.DeclaringType != null)
                {
                    m_className = callee.DeclaringType.FullName;
                }


            }

            catch (System.Security.SecurityException)
            {
                //swallow we could not get the stack trace
            }

        }
    }
}
