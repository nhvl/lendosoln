﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommonProjectLib.Common;
using System.Diagnostics;

namespace CommonProjectLib.Logging
{
    /// <summary>
    /// Central point for all logging. Calling add atleast once with a non null logger is REQUIRED
    /// </summary>
    public static class LogManager
    {
        private static readonly Type DeclaringType = typeof(LogManager);
        private static List<ILogger> s_loggers = new List<ILogger>();
        private static object s_lock = new object(); 

        public static void Add(ILogger logger)
        {
            if (logger == null)
            {
                throw CBaseException.CreateGeneric("logger cannot be null in LogManager.Add(ILogger)");
            }

            lock (s_lock)
            {
                s_loggers.Add(logger);
            }
        }


        public static void ClearAll()
        {
            s_loggers.Clear();
        }

        public static int LoggerCount
        {
            get { return s_loggers.Count; }
        }

        public static void Trace(string message)
        {
            Log(E_LoggingLevel.Trace, CreateLogProperties(message, null)); 
        }

        public static void Trace(string message, Dictionary<string, string> loggerUserProperties)
        {
            Log(E_LoggingLevel.Trace, CreateLogProperties(message, loggerUserProperties));
        }

        public static void Bug(string message)
        {
            Log(E_LoggingLevel.Bug, CreateLogProperties(message, null));
        }

        public static void Bug(string message, Dictionary<string, string> loggerUserProperties)
        {
            Log(E_LoggingLevel.Bug, CreateLogProperties(message, loggerUserProperties));
        }

        public static void Info(string message)
        {
            Log(E_LoggingLevel.Info, CreateLogProperties(message, null));
        }

        public static void Info(string message, Dictionary<string, string> loggerUserProperties)
        {
            Log(E_LoggingLevel.Info, CreateLogProperties(message, loggerUserProperties));
        }


        public static void Debug(string message)
        {
            Log(E_LoggingLevel.Debug, CreateLogProperties(message, null));
        }

        public static void Debug(string message, Dictionary<string, string> loggerUserProperties)
        {
            Log(E_LoggingLevel.Debug, CreateLogProperties(message, loggerUserProperties));
        }

        public static void Error(string message)
        {
            Log(E_LoggingLevel.Error, CreateLogPropertiesWithStackTrace(message, null));
        }

        public static void Error(string message, Exception e)
        {
            Log(E_LoggingLevel.Error, CreateLogPropertiesWithStackTrace(string.Format("{0}{1}{2}", message, System.Environment.NewLine, e.ToString()), null));
        }

        public static void Error(string message, Exception e, Dictionary<string, string> loggerUserProperties)
        {
            Log(E_LoggingLevel.Error, CreateLogPropertiesWithStackTrace(string.Format("{0}{1}{2}", message, System.Environment.NewLine, e.ToString()), loggerUserProperties));
        }

        public static void Error(string message, Dictionary<string, string> loggerUserProperties)
        {
            Log(E_LoggingLevel.Error, CreateLogPropertiesWithStackTrace(message, loggerUserProperties));
        }

        public static void Warn(string message)
        {
            Log(E_LoggingLevel.Warn, CreateLogProperties(message, null));
        }

        public static void Warn(string message, Exception e)
        {
            Log(E_LoggingLevel.Warn, CreateLogProperties(string.Format("{0}{1}{2}", message, System.Environment.NewLine, e.ToString()), null));
        }

        private static void Log(E_LoggingLevel level, LogProperties properties)
        {
            if (s_loggers.Count == 0)
            {
                return;
                //throw CBaseException.CreateGeneric("No loggers defined. Call LogManager.Add(ILogger)");
            }
            lock (s_lock)
            {
                s_loggers.ForEach(p => 
                    
                    p.Log(level, properties
                    
                    )); 
            }
        }

        private static LogProperties CreateLogProperties(string message, Dictionary<string,string> calleeProperties)
        {
            return new LogProperties(message,  calleeProperties);
        }

        private static LogProperties CreateLogPropertiesWithStackTrace(string message, Dictionary<string, string> calleeProperties)
        {
            return new LogProperties(message, calleeProperties, new LocationInfo(DeclaringType));
        }
    }
}
