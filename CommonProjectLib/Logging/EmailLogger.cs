﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;

namespace CommonProjectLib.Logging
{
    public sealed class EmailLogger : ILogger
    {
        public static bool useNewEmailer = false; // hard-coded for now because this module cannot access the ConstStage switch

        private string m_server;
        private int m_portNumber;
        private string m_toEmailAddress;
        private string m_fromEmailAddress;


        public EmailLogger(string server, int portNumber, string toEmailAddress, string fromEmailAddress)
        {
            m_server = server;
            m_toEmailAddress = toEmailAddress;
            m_fromEmailAddress = fromEmailAddress;
            m_portNumber = portNumber;
        }

        #region ILogger Members

        public void Log(E_LoggingLevel level, LogProperties properties)
        {
            // 10/3/2014 dd - If ConstStage.SmtpServer is not defined then do not email out.
            if (string.IsNullOrEmpty(m_server))
            {
                return;
            }

            if (level != E_LoggingLevel.Error)
            {
                return; //dont email non errors.
            }

            var subjectAndBody = GenerateEmailSubjectAndBody(level, properties);

            if (useNewEmailer)
            {
                SendEmailNEW(this.m_server, this.m_portNumber, this.m_fromEmailAddress, this.m_toEmailAddress, subjectAndBody.Item1, subjectAndBody.Item2);
            }
            else
            {
                SendEmailOLD(this.m_server, this.m_portNumber, this.m_fromEmailAddress, this.m_toEmailAddress, subjectAndBody.Item1, subjectAndBody.Item2);
            }
        }

        public static Tuple<string, string> GenerateEmailSubjectAndBody(E_LoggingLevel level, LogProperties properties)
        {
            string subject = properties.Message ?? "";
            if (subject.Length > 78)
            {
                subject = subject.Substring(0, 77);
            }

            subject = subject.Replace('\n', ' ');
            subject = subject.Replace('\r', ' ');
            StringBuilder body = new StringBuilder();

            body.AppendFormat("Message : {0}\n", properties.Message);
            body.AppendLine();

            body.AppendFormat("Class        :{0}\n", properties.LocationInfo.ClassName);
            body.AppendFormat("FileName     :{0}\n", properties.LocationInfo.FileName);
            body.AppendFormat("LineNumber   :{0}\n", properties.LocationInfo.LineNumber);
            body.AppendFormat("MethodName   :{0}\n", properties.LocationInfo.MethodName);

            if (properties.CalleeProperties != null)
            {
                body.AppendLine();

                foreach (var item in properties.CalleeProperties)
                {
                    body.AppendFormat("{0} : {1}\n", item.Key, item.Value);
                }
            }

            body.AppendLine();
            body.AppendLine("Stack Trace: ");
            body.AppendLine(properties.LocationInfo.UserStackTrace);

            return new Tuple<string, string>(subject, body.ToString());
        }

        public static void SendEmailOLD(string emailServer, int portNumber, string from, string to, string subject, string body)
        {
            SmtpClient client = new SmtpClient(emailServer, portNumber);
            client.Send(from, from, subject, body);
        }

        public static void SendEmailNEW(string emailServer, int portNumber, string fromEmail, string toEmail, string subject, string body)
        {
            LqbGrammar.DataTypes.EmailServerName? server = LqbGrammar.DataTypes.EmailServerName.Create(emailServer);
            if (server == null) return;

            LqbGrammar.DataTypes.PortNumber? port = LqbGrammar.DataTypes.PortNumber.Create(portNumber);
            if (port == null) return;

            LqbGrammar.DataTypes.EmailAddress? from = LqbGrammar.DataTypes.EmailAddress.Create(fromEmail);
            if (from == null) return;

            var listTo = LqbGrammar.DataTypes.EmailAddress.ParsePotentialList(toEmail);

            LqbGrammar.DataTypes.EmailSubject? emailSubject = LqbGrammar.DataTypes.EmailSubject.Create(subject);
            if (emailSubject == null) return;

            var emailBody = new LqbGrammar.Drivers.Emailer.EmailTextBody();
            emailBody.AppendText(body.ToString());

            var emailPackage = new LqbGrammar.Drivers.Emailer.EmailPackage(new Guid("11111111-1111-1111-1111-111111111111") /* SystemBrokerGuid */, from.Value, emailSubject.Value, emailBody, listTo.ToArray());

            var factory = LqbGrammar.GenericLocator<LqbGrammar.Drivers.Emailer.IEmailDriverFactory>.Factory;
            var emailAgent = factory.Create(server.Value, port.Value);
            emailAgent.SendEmail(emailPackage);
        }

        #endregion
    }
}
