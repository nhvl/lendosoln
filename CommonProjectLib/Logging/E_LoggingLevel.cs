﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonProjectLib.Logging
{
    public enum E_LoggingLevel
    {
        Trace = 0,
        Info = 1,
        Debug = 2,
        Warn = 3,
        Bug = 4,
        Error = 5
    }
}
