﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonProjectLib.Logging
{
    public interface ILogger
    {
        void Log(E_LoggingLevel level, LogProperties properties);
    }

}
