﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;

namespace CommonProjectLib.Common
{
    /// <summary>
    /// Used to generate friendly ids. Copied over from CPmlFIdGenerator with slight modifications.
    /// </summary>
    public sealed class FriendlyIDGenerator
    {
        private RNGCryptoServiceProvider m_rng;	// MSDN: Any public static members of this type are safe for multithreaded operations. Any instance members are not guaranteed to be thread safe.
        static private char[] s_convTable = new char[] { '3', '4', '6', '7', '9', 'A', 'C', 'D', 'E', 'F', 'G', 'H', 'J', 'K', 'L', 'M', 'N', 'P', 'R', 'T', 'W', 'X', 'Y' };


        public FriendlyIDGenerator()
        {
            m_rng = new RNGCryptoServiceProvider();
        }
        private static char FromByteToFriendlyChar(byte Byte)
        {
            return (char)s_convTable[Byte % s_convTable.Length]; // This won't make an even distribution but should be good enough.
        }
        public string GenerateNewFriendlyId()
        {
            byte[] random = new Byte[11];
            //RNGCryptoServiceProvider is an implementation of a random number generator.
            //System.Security.Cryptography.PasswordDeriveBytes pdb;

            m_rng.GetBytes(random); // The array is now filled with cryptographically strong random bytes.

            //char[] result = new char[11];
            StringBuilder strBuild = new StringBuilder(11);
            for (int i = 0; i < 11; ++i)
            {
                if (3 == i || 7 == i)
                    strBuild.Append('-');
                strBuild.Append(FromByteToFriendlyChar(random[i]));
            }
            return strBuild.ToString();
        }

        /// <summary>
        /// If you only need one friendly id call this. If you need more then create your own FriendlyIDGenerator and call GenerateNewFriendlyId.
        /// </summary>
        /// <returns></returns>
        public static string GenerateFriendlyId()
        {
            FriendlyIDGenerator idgenerator = new FriendlyIDGenerator();
            return idgenerator.GenerateNewFriendlyId();
        }
    }
}
