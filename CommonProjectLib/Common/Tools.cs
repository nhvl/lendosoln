﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommonProjectLib.Logging;

namespace CommonProjectLib.Common
{
    public static class Tools
    {
        public static void Assert(bool bAssert, string msg)
        {
            if (!bAssert)
                LogManager.Bug("Assert:" + msg);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="msMinSleep">in miliseconds</param>
        /// <param name="msMaxSleep">in miliseconds</param>
        /// <returns>sleep duration in miliseconds</returns>
        public static int SleepAndWakeupRandomlyWithin(int msMinSleep, int msMaxSleep)
        {
            int duration = 0;
            try
            {
                Tools.Assert(msMinSleep < msMaxSleep, "msMinSleep is expected to be smaller than msMaxSleep");
                Random rdm = new Random(unchecked((int)DateTime.Now.Ticks));
                duration = rdm.Next(msMinSleep, msMaxSleep);
                LogManager.Warn(string.Format("Sleeping for {0} msec", duration.ToString()));
                System.Threading.Thread.Sleep(duration);
            }
            catch { }
            return duration;
        }

        /// <summary>
        /// Returns a value indicating whether the specified System.String object occurs
        /// within this string. A parameter specifies the type of search to use for the 
        /// specified string.
        /// </summary>
        /// <param name="str">The System.String object to seek within.</param>
        /// <param name="value">The System.String object to seek.</param>
        /// <param name="comparisonType">One of the System.StringComparison values.</param>
        /// <returns>true if the value parameter occurs within this string, or if value is the empty string (""); otherwise, false.</returns>
        /// <exception cref="System.ArgumentNullException"><paramref name="value"/> is null.</exception>
        /// <exception cref="System.ArgumentException"><paramref name="comparisonType"/> is not a valid System.StringComparison value.</exception>
        public static bool Contains(this string str, string value, StringComparison comparisonType)
        {
            return str.IndexOf(value, comparisonType) >= 0;
        }

        /// <summary>
        /// Returns a value indicating whether the specified System.String object occurs
        /// within this string. The search is case-insensitive.
        /// </summary>
        /// <param name="str">The System.String object to seek within.</param>
        /// <param name="value">The System.String object to seek.</param>
        /// <returns>true if the value parameter occurs within this string, or if value is the empty string (""); otherwise, false.</returns>
        /// <exception cref="System.ArgumentNullException"><paramref name="value"/> is null.</exception>
        public static bool ContainsIgnoreCase(this string str, string value)
        {
            return Tools.Contains(str, value, StringComparison.OrdinalIgnoreCase);
        }

        /// <summary>
        /// Determines whether this string and a specified System.String object have the
        /// same value. The comparison is case-insensitive.
        /// </summary>
        /// <param name="str">The System.String object to check against.</param>
        /// <param name="value">The string to compare to this instance.</param>
        /// <returns>true if the value parameter is equal to this string, or if value is the empty string (""); otherwise, false.</returns>
        public static bool EqualsIgnoreCase(this string str, string value)
        {
            return str.Equals(value, StringComparison.OrdinalIgnoreCase);
        }
    }
}
