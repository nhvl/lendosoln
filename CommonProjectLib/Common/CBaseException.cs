﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonProjectLib.Common
{
    public class CBaseException : Exception
    {

        public const string UserMsg_Generic = "System error. Please contact us if this happens again.";
        
        private string m_userMessage;
        private string m_errorReferenceNumber = "";
        private string m_developerMessage = "";


        public CBaseException(string userMessage, string devMessage)  : base ( userMessage + " " + devMessage )
        {
            Initialize(userMessage, devMessage);
        }

        public CBaseException(string userMessage, Exception innerException) : base(userMessage, innerException)
        {
            Initialize(userMessage, innerException.ToString());
        }

        public CBaseException(string userMessage, string extraDevMessage, Exception innerException)  : base (userMessage + " " + extraDevMessage, innerException)
        {
            string devMessage = string.Format("{1}{0}{2}", System.Environment.NewLine, innerException.ToString()); 
            Initialize(userMessage, devMessage );
        }

        public static CBaseException CreateGeneric(string devMessage)
        {
            return new CBaseException(UserMsg_Generic, devMessage);
        }

        public static CBaseException CreateGeneric(string devMessage, Exception e)
        {
            return new CBaseException(UserMsg_Generic, devMessage, e);
        }

        public static CBaseException CreateUnexpectedEnumException(Enum enumVal)
        {
            return new CBaseException(UserMsg_Generic,
                string.Format("Unexpected enum value: {0}.{1}", enumVal.GetType().ToString(), enumVal.ToString()));
        }
        public string Subject
        {
            get;
            set;
        }

        //initializes all the variables
        private void Initialize(string userMessage, string developerMessage)
        {
            m_userMessage = userMessage;
            m_developerMessage = developerMessage;
            m_errorReferenceNumber = FriendlyIDGenerator.GenerateFriendlyId();
        }

        public override string ToString()
        {
            return string.Format("Reference #:{1}{0}{0}UserMessage: {2}{0}{0}DeveloperMessage: {3}", Environment.NewLine, m_errorReferenceNumber, m_userMessage, m_developerMessage);
        }

        public string UserMessage()
        {
            return this.m_userMessage;
        }
    }
}
