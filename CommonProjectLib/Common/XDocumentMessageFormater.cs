﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Messaging;
using System.Xml;
using System.Xml.Linq;
using System.IO;

namespace CommonProjectLib.Common
{
    // NOTE: This will be obsolete as an independent PR moved this code to
    //       Adapters.XDocumentMessageFormatter.
    //       Once both this PR and the other are merged into the main
    //       code base then this should be deleted and all code in this 
    //       library that still references System.Messaging should be
    //       removed as well.
    public class XDocumentMessageFormater : IMessageFormatter
    {
        #region IMessageFormatter Members

        public bool CanRead(Message message)
        {
            if (message == null)
            {
                return false;
            }
            return message.Formatter.GetType() == typeof(XDocumentMessageFormater);
        }

        public object Read(Message message)
        {
            if (null == message)
            {
                return null;
            }

            if (message.Formatter.GetType() != typeof(XDocumentMessageFormater))
            {
                throw new Exception("Unable to parse message with " + message.Formatter.GetType() + ". Expected " + this.GetType());
            }
            using (XmlReader reader = XmlReader.Create(message.BodyStream))
            {
                return XDocument.Load(reader);
            }
        }

        public void Write(Message message, object obj)
        {
            if (null == message || obj == null)
            {
                return;
            }
            XDocument doc = obj as XDocument;
            if (doc == null)
            {
                throw new Exception("object type must be " + typeof(XDocument).GetType());
            }
            MemoryStream stream = new MemoryStream();

            using (XmlWriter writer = XmlWriter.Create(stream))
            {
                doc.WriteTo(writer);
            }

            message.BodyStream = stream;

        }

        #endregion

        #region ICloneable Members

        public object Clone()
        {
            return new XDocumentMessageFormater();
        }

        #endregion
    }

}
