﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonProjectLib.Common.Lib
{
    /// <summary>
    /// A variant for KeyNotFoundException. This one contains the key that 
    /// caused it. 
    /// </summary>
    public sealed class KnownKeyNotFoundException<TKey> : KeyNotFoundException
    {
        public TKey Key { get; private set; }

        public KnownKeyNotFoundException(TKey key, string message)
            : base(message, null)
        {
            this.Key = key;
        }

        public KnownKeyNotFoundException(TKey key, string message, Exception innerException)
            : base(message, innerException)
        {
            this.Key = key;
        }
    }
}
