﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonProjectLib.Common.Lib
{
    /// <summary>
    /// A variant of a regular dictionary. The only difference is it throws a 
    /// KnownKeyNotFoundException instead of KeyNotFoundException. The key is known to the exception.
    /// </summary>
    public sealed class FriendlyDictionary<TKey, TVal> : Dictionary<TKey, TVal>
    {
        public FriendlyDictionary()
            : base()
        {
        }

        public FriendlyDictionary(IEqualityComparer<TKey> comparable)
            : base(comparable)
        {
        }

        public FriendlyDictionary(int initialCapacity, IEqualityComparer<TKey> comparer)
            : base(initialCapacity, comparer)
        {
        }

        new public TVal this[TKey key]
        {
            get
            {
                TVal value;

                if (TryGetValue(key, out value))
                {
                    return value;
                }

                throw new KnownKeyNotFoundException<TKey>(key,
                    string.Format("The given key ({0}) was not present in the dictionary.", key));
            }
        }
    }
}
