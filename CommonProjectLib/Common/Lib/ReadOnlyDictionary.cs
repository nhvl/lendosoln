﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonProjectLib.Common.Lib
{
    // NOTE, the readonly-ness is only at a shallow level.  modifying the keys/values is allowed but
    // goes against the spirit of the dictionary.
    public class ReadOnlyDictionary<TKey, TValue> : IDictionary<TKey, TValue>
    {
        protected Dictionary<TKey, TValue> m_dict;

        public ReadOnlyDictionary() { } // this is so it can be serialized.  Deserialization makes no sense.

        public ReadOnlyDictionary(Dictionary<TKey, TValue> dictIn)
        {
            m_dict = new Dictionary<TKey, TValue>(dictIn.Count, dictIn.Comparer);
            foreach (var kvp in dictIn)
            {
                m_dict.Add(kvp.Key, kvp.Value);
            }
        }
        #region IDictionary<TKey,TValue> Members

        /// <summary>
        /// Not Supported
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void Add(TKey key, TValue value)
        {
            throw new NotSupportedException();
        }

        public bool ContainsKey(TKey key)
        {
            return m_dict.ContainsKey(key);
        }

        public ICollection<TKey> Keys
        {
            get { return m_dict.Keys; }
        }

        /// <summary>
        /// Not Supported
        /// </summary>
        public bool Remove(TKey key)
        {
            throw new NotSupportedException();
        }

        public bool TryGetValue(TKey key, out TValue value)
        {
            return m_dict.TryGetValue(key, out value);
        }

        public ICollection<TValue> Values
        {
            get { return m_dict.Values; }
        }

        public TValue this[TKey key]
        {
            get
            {
                return m_dict[key];
            }
            /// <summary>
            /// Not Supported
            /// </summary>
            set
            {
                throw new NotSupportedException();
            }
        }

        #endregion

        #region ICollection<KeyValuePair<TKey,TValue>> Members

        /// <summary>
        /// Not Supported
        /// </summary>
        public void Add(KeyValuePair<TKey, TValue> item)
        {
            throw new NotSupportedException();
        }

        /// <summary>
        /// Not Supported
        /// </summary>
        public void Clear()
        {
            throw new NotSupportedException();
        }

        public bool Contains(KeyValuePair<TKey, TValue> item)
        {
            return m_dict.Contains(item);
        }

        public void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex)
        {
            var ic = (ICollection<KeyValuePair<TKey, TValue>>)m_dict;
            ic.CopyTo(array, arrayIndex);
        }

        public int Count
        {
            get { return m_dict.Count; }
        }

        public bool IsReadOnly
        {
            get { return true; }
        }

        /// <summary>
        /// Not Supported
        /// </summary>
        public bool Remove(KeyValuePair<TKey, TValue> item)
        {
            throw new NotSupportedException();
        }

        #endregion

        #region IEnumerable<KeyValuePair<TKey,TValue>> Members

        public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
        {
            return m_dict.GetEnumerator();
        }

        #endregion

        #region IEnumerable Members

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            var ie = (System.Collections.IEnumerable)m_dict;
            return ie.GetEnumerator();
        }

        #endregion
    }
}
