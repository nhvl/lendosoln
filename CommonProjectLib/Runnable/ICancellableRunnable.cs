﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonProjectLib.Runnable
{
    /// <summary>
    /// An interface for a job that will be ran automatically by LendersOfficeContinuousService.
    /// This interface supports CancellationToken.
    /// </summary>
    public interface ICancellableRunnable
    {
        /// <summary>
        /// Gets a description of the runnable.
        /// </summary>
        /// <value>Gets the friendly description of the class.</value>
        string Description { get; }

        /// <summary>
        /// The main entry point for the class.
        /// </summary>
        /// <param name="cancellationToken">A cancellation token that can be used by other objects to receive notice of cancellation.</param>
        void Run(System.Threading.CancellationToken cancellationToken);
    }
}
