﻿// <copyright file="IRunnable.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: David Dao
//  Date:   1/4/2011 4:11:53 PM
// </summary>
namespace CommonProjectLib.Runnable
{
    /// <summary>
    /// An interface for a job that will be ran automatically by LendersOfficeContinuousService.
    /// </summary>
    public interface IRunnable
    {
        /// <summary>
        /// Gets a description of the runnable.
        /// </summary>
        /// <value>Gets the friendly description of the class.</value>
        string Description { get; }

        /// <summary>
        /// The main entry point for the class.
        /// </summary>
        void Run();
    }
}
