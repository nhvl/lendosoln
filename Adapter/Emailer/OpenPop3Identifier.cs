﻿namespace Adapter.Emailer
{
    using LqbGrammar;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;
    using LqbGrammar.Drivers.Emailer;

    /// <summary>
    /// Identifier returned from the OpenPop library.
    /// </summary>
    public sealed class OpenPop3Identifier : EmailIdentifier
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="OpenPop3Identifier"/> class.
        /// </summary>
        /// <param name="identifier">The email identifier.</param>
        private OpenPop3Identifier(string identifier)
            : base(identifier)
        {
        }

        /// <summary>
        /// Create a validated email identifier.
        /// </summary>
        /// <param name="identifier">The email identifier.</param>
        /// <returns>A validated identifier, or null.</returns>
        public static EmailIdentifier CreateOpenPopIdentifer(string identifier)
        {
            return IsValid(identifier) ? new OpenPop3Identifier(identifier) : null;
        }

        /// <summary>
        /// Validate an email identifier.
        /// </summary>
        /// <param name="identifier">The email identifier.</param>
        /// <returns>True if the identifier is valid, false otherwise.</returns>
        private static bool IsValid(string identifier)
        {
            var factory = GenericLocator<IRegularExpressionDriverFactory>.Factory;
            var driver = factory.Create();
            return driver.IsMatch(RegularExpressionString.Pop3Uid, identifier);
        }
    }
}
