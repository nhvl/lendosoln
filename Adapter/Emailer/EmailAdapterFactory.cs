﻿namespace Adapter.Emailer
{
    using LqbGrammar.Adapters;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Factory for creating instances of the EmailAdapter class.
    /// </summary>
    public sealed class EmailAdapterFactory : IEmailAdapterFactory
    {
        /// <summary>
        /// Create an instance of the EmailAdapter class.
        /// </summary>
        /// <param name="server">The email server that will be used for sending emails.</param>
        /// <param name="portNumber">The port number of the server.</param>
        /// <returns>An instance of the EmailAdapter class.</returns>
        public IEmailAdapter Create(EmailServerName server, PortNumber portNumber)
        {
            return new EmailAdapter(server, portNumber);
        }
    }
}
