﻿namespace Adapter.Emailer
{
    using System;
    using System.Collections.Generic;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers.Emailer;
    using LqbGrammar.Exceptions;
    using OpenPop.Mime;

    /// <summary>
    /// Deliver OpenPop library message as LqbGrammar data structures.
    /// </summary>
    public sealed class OpenPop3Message : IReceivedEmailData
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="OpenPop3Message"/> class.
        /// </summary>
        /// <param name="message">The message from the OpenPop library.</param>
        public OpenPop3Message(Message message)
        {
            this.ContainedMessage = message;
        }

        /// <summary>
        /// Gets the contents of the email at the time it was sent.
        /// </summary>
        /// <value>The contents of the email at the time it was sent.</value>
        public EmailPackage Contents
        {
            get
            {
                return this.ReadContents();
            }
        }

        /// <summary>
        /// Gets the timestamp of when the email was sent.
        /// </summary>
        /// <value>The timestamp of when the email was sent.</value>
        public LqbEventDate DateSent
        {
            get
            {
                var goodFormat = this.ContainedMessage.Headers.DateSent;
                if (goodFormat.Kind != DateTimeKind.Utc)
                {
                    goodFormat = goodFormat.ToUniversalTime();
                }

                var date = LqbEventDate.Create(goodFormat);
                if (date == null)
                {
                    throw new ServerException(ErrorMessage.SystemError);
                }

                return date.Value;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the X-Auto-Response-Suppress header was set.
        /// </summary>
        /// <value>A value indicating whether the X-Auto-Response-Suppress header was set.</value>
        public bool IsAutoResponseSuppressed
        {
            get
            {
                if (this.ContainedMessage.Headers.UnknownHeaders != null && this.ContainedMessage.Headers.UnknownHeaders.Keys != null)
                {
                    foreach (string key in this.ContainedMessage.Headers.UnknownHeaders.Keys)
                    {
                        if (key.ToLower().Contains("x-auto-response-suppress"))
                        {
                            return true;
                        }
                    }
                }

                return false;
            }
        }

        /// <summary>
        /// Gets the full raw message that was received.
        /// </summary>
        /// <value>The full raw message that was received.</value>
        public byte[] RawMessage
        {
            get
            {
                return this.ContainedMessage.RawMessage;
            }
        }

        /// <summary>
        /// Gets or sets the message from the OpenPop library.
        /// </summary>
        private Message ContainedMessage { get; set; }

        /// <summary>
        /// Parse the contents of the OpenPop message into an EmailPackage structure.
        /// </summary>
        /// <returns>The contents of the OpenPop message.</returns>
        private EmailPackage ReadContents()
        {
            var from = this.ReadFrom();
            var subject = this.ReadSubject();
            var body = this.ReadBody();
            var to = this.ReadTo();

            var package = new EmailPackage(Guid.Empty, from, subject, body, to);

            this.ReadCc(package);
            this.ReadBcc(package);
            this.ReadAttachments(package);

            return package;
        }

        /// <summary>
        /// Convert an OpenPop email address to an LQB email address structure.
        /// </summary>
        /// <param name="openPop">The OpenPop email address.</param>
        /// <returns>The LQB email address, or null.</returns>
        private EmailAddress? ConvertAddress(OpenPop.Mime.Header.RfcMailAddress openPop)
        {
            var address = EmailAddress.Create(this.ContainedMessage.Headers.From.Address, this.ContainedMessage.Headers.From.DisplayName);
            if (address == null)
            {
                address = EmailAddress.Create(this.ContainedMessage.Headers.From.Address);
            }

            return address;
        }

        /// <summary>
        /// Read the from address from the OpenPop message.
        /// </summary>
        /// <returns>The from address.</returns>
        private EmailAddress ReadFrom()
        {
            EmailAddress? from = null;
            if (this.ContainedMessage.Headers.From != null && this.ContainedMessage.Headers.From.HasValidMailAddress)
            {
                from = this.ConvertAddress(this.ContainedMessage.Headers.From);
            }

            if (from != null)
            {
                return from.Value;
            }
            else
            {
                throw new ServerException(ErrorMessage.SystemError);
            }
        }

        /// <summary>
        /// Read the to addresses from the OpenPop message.
        /// </summary>
        /// <returns>The to addresses.</returns>
        private EmailAddress[] ReadTo()
        {
            EmailAddress[] to = null;

            int countTo = this.ContainedMessage.Headers.To.Count;
            if (countTo > 0)
            {
                var list = new List<EmailAddress>();
                for (int i = 0; i < countTo; ++i)
                {
                    var address = this.ConvertAddress(this.ContainedMessage.Headers.To[i]);
                    if (address != null)
                    {
                        list.Add(address.Value);
                    }
                }

                if (list.Count > 0)
                {
                    to = list.ToArray();
                }
            }

            return to;
        }

        /// <summary>
        /// Read the subject from the OpenPop message.
        /// </summary>
        /// <returns>The email subject, with a standard message if there is no subject.</returns>
        private EmailSubject ReadSubject()
        {
            var subject = EmailSubject.Create(this.ContainedMessage.Headers.Subject);
            return (subject == null) ? EmailSubject.Create("NO SUBJECT IN MESSAGE").Value : subject.Value;
        }

        /// <summary>
        /// Read the body of the OpenPop message.
        /// </summary>
        /// <returns>The body of the message.</returns>
        private EmailBody ReadBody()
        {
            var mailMsg = this.ContainedMessage.ToMailMessage();

            if (mailMsg.IsBodyHtml)
            {
                var body = new EmailHtmlBody();
                body.AppendText(mailMsg.Body);
                return body;
            }
            else
            {
                var body = new EmailTextBody();
                body.AppendText(mailMsg.Body);
                return body;
            }
        }

        /// <summary>
        /// Read the CC items from the OpenPop message.
        /// </summary>
        /// <param name="package">The CC items.</param>
        private void ReadCc(EmailPackage package)
        {
            if (this.ContainedMessage.Headers.Cc != null)
            {
                foreach (var cc in this.ContainedMessage.Headers.Cc)
                {
                    var address = this.ConvertAddress(cc);
                    if (address != null)
                    {
                        package.AddCc(address.Value);
                    }
                }
            }
        }

        /// <summary>
        /// Read the BCC items from the OpenPop message.
        /// </summary>
        /// <param name="package">The BCC items.</param>
        private void ReadBcc(EmailPackage package)
        {
            if (this.ContainedMessage.Headers.Bcc != null)
            {
                foreach (var bcc in this.ContainedMessage.Headers.Bcc)
                {
                    var address = this.ConvertAddress(bcc);
                    if (address != null)
                    {
                        package.AddBcc(address.Value);
                    }
                }
            }
        }

        /// <summary>
        /// Read the attachments from the OpenPop message.
        /// </summary>
        /// <param name="package">The attachments.</param>
        private void ReadAttachments(EmailPackage package)
        {
            var attachments = this.ContainedMessage.FindAllAttachments();
            if (attachments != null)
            {
                foreach (var item in attachments)
                {
                    if (item.IsText)
                    {
                        string text = System.Text.Encoding.UTF8.GetString(item.Body);
                        var resource = TypedResource<string>.CreateText(text, item.ContentType.MediaType, item.FileName);
                        package.AddAttachment(resource);
                    }
                    else
                    {
                        var resource = TypedResource<byte[]>.CreateBinary(item.Body, item.ContentType.MediaType, item.FileName);
                        package.AddAttachment(resource);
                    }
                }
            }
        }
    }
}
