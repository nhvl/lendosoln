﻿namespace Adapter.Emailer
{
    using System;
    using LqbGrammar.Adapters;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Factory for creating OpenPop adapter instances.
    /// </summary>
    public sealed class OpenPop3AdapterFactory : IReceiveEmailAdapterFactory
    {
        /// <summary>
        /// Gets or sets a value indicating whether a test message should be used rather than connecting to the live email server.
        /// This value only has affect on the subsequent call to Create, wherein it is set to false.
        /// </summary>
        /// <value>A value indicating whether a test message should be used.</value>
        public bool UseTestMessage { private get; set; }

        /// <summary>
        /// Create an email adapter.
        /// </summary>
        /// <param name="server">The email server name that will be used for sending the email.</param>
        /// <param name="userName">The user name for logging into the email server.</param>
        /// <param name="password">The password for logging into the email server.</param>
        /// <param name="port">The port on which the email server is listening for requests.</param>
        /// <returns>An email adapter.</returns>
        public IReceiveEmailAdapter Create(EmailServerName server, string userName, string password, int port)
        {
            var isTest = this.UseTestMessage;
            this.UseTestMessage = false;
            return new OpenPop3Adapter(server, userName, password, port, isTest);
        }
    }
}
