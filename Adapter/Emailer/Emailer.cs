﻿namespace Adapter.Emailer
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Net.Mail;
    using System.Net.Mime;
    using System.Text;
    using LqbGrammar.Adapters;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers.Emailer;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// An implementation of the IEmailAdapter class, this class wraps the .net email utilities.
    /// </summary>
    internal sealed class EmailAdapter : IEmailAdapter
    {
        /// <summary>
        /// The email server that will be used for sending emails.
        /// </summary>
        private EmailServerName server;

        /// <summary>
        /// The email server's port number will be used for sending emails.
        /// </summary>
        private PortNumber portNumber;

        /// <summary>
        /// Hold all streams for attachments so we can ensure they get closed.
        /// </summary>
        private List<Stream> streams;

        /// <summary>
        /// Initializes a new instance of the <see cref="EmailAdapter"/> class.
        /// </summary>
        /// <param name="server">The name of the email server that will be used to send emails.</param>
        /// <param name="portNumber">The port number of the email server.</param>
        public EmailAdapter(EmailServerName server, PortNumber portNumber)
        {
            this.server = server;
            this.portNumber = portNumber;
        }

        /// <summary>
        /// Send an email.
        /// </summary>
        /// <param name="email">The data that will be used to compose the email.</param>
        public void SendEmail(EmailPackage email)
        {
            try
            {
                SmtpClient smtpClient = new SmtpClient(this.server.ToString(), this.portNumber.Value);
                smtpClient.EnableSsl = false;
                smtpClient.UseDefaultCredentials = false; // Per Rajya (CMG) in Exchange 2013, this is required.
                smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network; // OPM 461108 - Delivery Method MUST be Network for sender address to be used as SMTP FROM (return-path) address.

                using (MailMessage mailMessage = this.ConvertToMailMessage(email))
                {
                    smtpClient.Send(mailMessage);
                }
            }
            catch (LqbException)
            {
                throw;
            }
            catch (Exception ex)
            {
                StringBuilder sb = new StringBuilder(100 * email.To.Length);
                foreach (EmailAddress add in email.To)
                {
                    if (sb.Length > 0)
                    {
                        sb.Append(";");
                    }

                    sb.Append(add.ToString());
                }

                SimpleContext context = new SimpleContext(sb.ToString());

                throw new DeveloperException(ErrorMessage.SystemError, context, ex);
            }
            finally
            {
                this.CloseStreams();
            }
        }

        /// <summary>
        /// Convert the LQB grammar's EmailPackage to the .net framework's MailMessage.
        /// </summary>
        /// <param name="email">Email data in the LQB grammar.</param>
        /// <returns>Email data in the .net framework's format.</returns>
        private MailMessage ConvertToMailMessage(EmailPackage email)
        {
            var mailMsg = new MailMessage();
            mailMsg.Headers.Add("X-Auto-Response-Suppress", "DR, NDR, RN, NRN, OOF, AutoReply");
            mailMsg.Subject = email.Subject.ToString();

            if (!string.IsNullOrEmpty(email.From.DisplayName))
            {
                mailMsg.From = new MailAddress(email.From.ToString(), email.From.DisplayName);
            }
            else
            {
                mailMsg.From = new MailAddress(email.From.ToString());
            }

            if (!string.IsNullOrWhiteSpace(email.Sender.ToString()))
            {
                mailMsg.Sender = new System.Net.Mail.MailAddress(email.Sender.ToString());
            }

            foreach (var to in email.To)
            {
                if (!string.IsNullOrEmpty(to.DisplayName))
                {
                    mailMsg.To.Add(new MailAddress(to.ToString(), to.DisplayName));
                }
                else
                {
                    mailMsg.To.Add(new MailAddress(to.ToString()));
                }
            }

            if (email.Cc != null)
            {
                foreach (EmailAddress cc in email.Cc)
                {
                    mailMsg.CC.Add(new MailAddress(cc.ToString()));
                }
            }

            if (email.Bcc != null)
            {
                foreach (EmailAddress bcc in email.Bcc)
                {
                    mailMsg.Bcc.Add(new MailAddress(bcc.ToString()));
                }
            }

            if (email.Body is EmailTextBody)
            {
                this.AddTextBody(mailMsg, email.Body as EmailTextBody);
            }
            else if (email.Body is EmailHtmlBody)
            {
                this.AddHtmlBody(mailMsg, email.Body as EmailHtmlBody);
            }

            this.AddBinaryAttachments(mailMsg, email.BinaryAttachments);
            this.AddTextAttachments(mailMsg, email.TextAttachments);
            this.AddFileAttachments(mailMsg, email.FileAttachments);

            return mailMsg;
        }

        /// <summary>
        /// Add binary attachements.
        /// </summary>
        /// <param name="mailMsg">The mail message that will take the attachments.</param>
        /// <param name="list">The binary attachments.</param>
        private void AddBinaryAttachments(MailMessage mailMsg, List<TypedResource<byte[]>> list)
        {
            if (list == null)
            {
                return;
            }

            foreach (TypedResource<byte[]> att in list)
            {
                var ms = new MemoryStream(att.Data);
                this.CacheStream(ms);

                Attachment attachment = new Attachment(ms, att.Name, att.MimeType.ToString());
                mailMsg.Attachments.Add(attachment);
            }
        }

        /// <summary>
        /// Add text attachments.
        /// </summary>
        /// <param name="mailMsg">The mail message that will take the attachements.</param>
        /// <param name="list">The text attachments.</param>
        private void AddTextAttachments(MailMessage mailMsg, List<TypedResource<string>> list)
        {
            if (list == null)
            {
                return;
            }

            foreach (TypedResource<string> att in list)
            {
                var encoding = new System.Text.UTF8Encoding(false);
                var data = encoding.GetBytes(att.Data);

                var ms = new MemoryStream(data);
                this.CacheStream(ms);

                Attachment attachment = new Attachment(ms, att.Name, att.MimeType.ToString());
                mailMsg.Attachments.Add(attachment);
            }
        }

        /// <summary>
        /// Add file attachments.
        /// </summary>
        /// <param name="mailMsg">The mail message that will take the attachments.</param>
        /// <param name="list">The file attachments.</param>
        private void AddFileAttachments(MailMessage mailMsg, List<TypedResource<FileInfo>> list)
        {
            if (list == null)
            {
                return;
            }

            foreach (TypedResource<FileInfo> att in list)
            {
                Attachment attachment = new Attachment(att.Data.FullName, att.MimeType.ToString());
                if (!string.IsNullOrEmpty(att.Name))
                {
                    attachment.Name = att.Name;
                }

                mailMsg.Attachments.Add(attachment);
            }
        }

        /// <summary>
        /// Add a plain text body to an email message.
        /// </summary>
        /// <param name="mailMsg">The email message.</param>
        /// <param name="body">The email body to be added.</param>
        private void AddTextBody(MailMessage mailMsg, EmailTextBody body)
        {
            mailMsg.Body = body.ToString();
            mailMsg.IsBodyHtml = false;
        }

        /// <summary>
        /// Add an html body to an email message.
        /// </summary>
        /// <param name="mailMsg">The email message.</param>
        /// <param name="body">The email body to be added.</param>
        private void AddHtmlBody(MailMessage mailMsg, EmailHtmlBody body)
        {
            mailMsg.Body = string.Empty;
            mailMsg.IsBodyHtml = true;

            AlternateView av = AlternateView.CreateAlternateViewFromString(body.ToString(), UTF8Encoding.Default, MediaTypeNames.Text.Html);
            mailMsg.AlternateViews.Add(av);

            foreach (TypedResource<byte[]> link in body.LinkedResources)
            {
                using (var ms = new MemoryStream(link.Data))
                {
                    var resource = new LinkedResource(ms, link.MimeType.ToString());
                    if (!string.IsNullOrEmpty(link.Name))
                    {
                        resource.ContentId = link.Name;
                    }

                    av.LinkedResources.Add(resource);
                }
            }
        }

        /// <summary>
        /// Add a stream to the cached collection.
        /// </summary>
        /// <param name="stream">The stream to be added to the collection.</param>
        private void CacheStream(Stream stream)
        {
            if (this.streams == null)
            {
                this.streams = new List<Stream>();
            }

            this.streams.Add(stream);
        }

        /// <summary>
        /// Close all the streams that were added to the cached collection.
        /// </summary>
        private void CloseStreams()
        {
            if (this.streams != null)
            {
                foreach (var stream in this.streams)
                {
                    stream.Close();
                }
            }
        }
    }
}
