﻿namespace Adapter.Emailer
{
    using System;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers.Emailer;

    /// <summary>
    /// There is no good way of testing against a live email server.  Instead we will
    /// test against fake data using this test shim.
    /// </summary>
    internal sealed class TestMessage : IReceivedEmailData
    {
        /// <summary>
        /// The test email data is an instaton.
        /// </summary>
        private static readonly EmailPackage Package;

        /// <summary>
        /// Initializes static members of the <see cref="TestMessage" /> class.
        /// </summary>
        static TestMessage()
        {
            var from = EmailAddress.Create("from@meridianlink.com").Value;
            var to = EmailAddress.Create("to@meridianlink.com").Value;
            var cc = EmailAddress.Create("cc@meridianlink.com").Value;
            var bcc = EmailAddress.Create("bcc@meridianlink.com").Value;
            var subject = EmailSubject.Create("FAKE SUBJECT").Value;
            var body = new EmailTextBody();
            body.AppendText("FAKE EMAIL BODY");

            Package = new EmailPackage(new Guid("11111111-1111-1111-1111-111111111111") /* SystemBrokerGuid */, from, subject, body, to);
            Package.AddCc(cc);
            Package.AddBcc(bcc);

            var resource = TypedResource<byte[]>.CreateBinary(System.Text.Encoding.ASCII.GetBytes("FAKE FILE CONTENT"), "application/pdf", "FakeFile.pdf");
            Package.AddAttachment(resource);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TestMessage"/> class.
        /// </summary>
        public TestMessage()
        {
        }

        /// <summary>
        /// Gets the contents of the email at the time it was sent.
        /// </summary>
        /// <value>The contents of the email at the time it was sent.</value>
        public EmailPackage Contents
        {
            get
            {
                return Package;
            }
        }

        /// <summary>
        /// Gets the timestamp of when the email was sent.
        /// </summary>
        /// <value>The timestamp of when the email was sent.</value>
        public LqbEventDate DateSent
        {
            get
            {
                var sentUtc = DateTime.UtcNow.AddMinutes(-10);
                return LqbEventDate.Create(sentUtc).Value;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the X-Auto-Response-Suppress header was set.
        /// </summary>
        /// <value>A value indicating whether the X-Auto-Response-Suppress header was set.</value>
        public bool IsAutoResponseSuppressed
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Gets the full raw message that was received.
        /// </summary>
        /// <value>The full raw message that was received.</value>
        public byte[] RawMessage
        {
            get
            {
                return System.Text.Encoding.ASCII.GetBytes("FAKE EMAIL RAW MESSAGE");
            }
        }
    }
}
