﻿namespace Adapter.Emailer
{
    using System;
    using System.Diagnostics.CodeAnalysis;
    using LqbGrammar.Adapters;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers.Emailer;
    using LqbGrammar.Exceptions;
    using OpenPop.Pop3;

    /// <summary>
    /// Adapter that uses the OpenPop library for receiving emails.
    /// </summary>
    internal sealed class OpenPop3Adapter : IReceiveEmailAdapter
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="OpenPop3Adapter"/> class.
        /// </summary>
        /// <param name="server">The server from which emails are received.</param>
        /// <param name="userName">The username for logging into the server.</param>
        /// <param name="password">The password for logging into the server.</param>
        /// <param name="port">The post on which the server is listening.</param>
        /// <param name="isTest">True if running in test mode, false otherwise.</param>
        public OpenPop3Adapter(EmailServerName server, string userName, string password, int port, bool isTest)
        {
            try
            {
                this.IsTest = isTest;

                if (!isTest)
                {
                    var pop3 = new Pop3Client();
                    pop3.Connect(server.ToString(), port, false);
                    pop3.Authenticate(userName, password, AuthenticationMethod.UsernameAndPassword);
                    this.Pop3 = pop3;
                }
            }
            catch (Exception ex)
            {
                throw new DeveloperException(ErrorMessage.BadConfiguration, ex);
            }
        }

        /// <summary>
        /// Finalizes an instance of the <see cref="OpenPop3Adapter" /> class.
        /// </summary>
        [SuppressMessage(category: "LendingQBStyleCop.LendingQBCustomRules", checkId: "LB1003:CannotSwallowGeneralException", Justification = "Cannot throw exceptions on the finalizer thread.")]
        ~OpenPop3Adapter()
        {
            try
            {
                this.Disconnect();
            }
            catch
            {
                // Cannot throw exceptions on the finalizer thread!
            }
        }

        /// <summary>
        /// Gets the number of email messages available on the server.
        /// </summary>
        /// <value>The number of email messages available on the server.</value>
        public int Count
        {
            get
            {
                if (this.IsDisposed)
                {
                    throw new DeveloperException(ErrorMessage.SystemError);
                }

                try
                {
                    return this.IsTest ? 1 : this.Pop3.GetMessageCount();
                }
                catch (LqbException)
                {
                    throw;
                }
                catch (Exception ex)
                {
                    throw new DeveloperException(ErrorMessage.SystemError, ex);
                }
            }
        }

        /// <summary>
        /// Gets or sets the OpenPop utility.
        /// </summary>
        /// <value>The OpenPop utility.</value>
        private Pop3Client Pop3 { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the adapter is being called in test mode.
        /// </summary>
        /// <value>A value indicating whether the adapter is being called in test mode.</value>
        private bool IsTest { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the Dispose method has been called.
        /// </summary>
        private bool IsDisposed { get; set; }

        /// <summary>
        /// Disconnect from the email server.
        /// </summary>
        public void Dispose()
        {
            if (this.IsDisposed)
            {
                return;
            }

            try
            {
                this.Disconnect();
                this.IsDisposed = true;
            }
            catch (LqbException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new DeveloperException(ErrorMessage.SystemError, ex);
            }
        }

        /// <summary>
        /// Retrieve the email data for the input index.
        /// </summary>
        /// <param name="index">A value in the range [0, Count).</param>
        /// <returns>The data for the specified email.</returns>
        public IReceivedEmailData GetEmail(int index)
        {
            if (this.IsDisposed)
            {
                throw new DeveloperException(ErrorMessage.SystemError);
            }

            try
            {
                if (this.IsTest)
                {
                    return new TestMessage();
                }
                else
                {
                    var message = this.Pop3.GetMessage(index + 1);
                    return new OpenPop3Message(message);
                }
            }
            catch (LqbException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new DeveloperException(ErrorMessage.SystemError, ex);
            }
        }

        /// <summary>
        /// Delete the email data for the input index.
        /// </summary>
        /// <param name="index">A value in the range [0, Count).</param>
        public void DeleteEmail(int index)
        {
            if (this.IsDisposed)
            {
                throw new DeveloperException(ErrorMessage.SystemError);
            }

            try
            {
                if (!this.IsTest)
                {
                    this.Pop3.DeleteMessage(index + 1);
                }
            }
            catch (LqbException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new DeveloperException(ErrorMessage.SystemError, ex);
            }
        }

        /// <summary>
        /// Retrieve an identifer for the email with the input index.
        /// </summary>
        /// <param name="index">A value in the range [0, Count).</param>
        /// <returns>An identifier for the specified email.</returns>
        public EmailIdentifier GetIdentifier(int index)
        {
            if (this.IsDisposed)
            {
                throw new DeveloperException(ErrorMessage.SystemError);
            }

            EmailIdentifier validated = null;
            try
            {
                var identifier = this.IsTest ? "123456789" : this.Pop3.GetMessageUid(index + 1);
                validated = OpenPop3Identifier.CreateOpenPopIdentifer(identifier);
            }
            catch (LqbException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new DeveloperException(ErrorMessage.SystemError, ex);
            }

            if (validated != null)
            {
                return validated;
            }
            else
            {
                throw new DeveloperException(ErrorMessage.SystemError);
            }
        }

        /// <summary>
        /// Disconnect from the email server.
        /// </summary>
        private void Disconnect()
        {
            if (this.Pop3 != null)
            {
                if (!this.IsTest)
                {
                    this.Pop3.Disconnect();
                }

                this.Pop3 = null;
            }
        }
    }
}
