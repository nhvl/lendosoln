﻿namespace Adapter.SecurityEventLogging
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using LqbGrammar;
    using LqbGrammar.Adapters.SecurityEventLogging;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;
    using LqbGrammar.Drivers.SecurityEventLogging;

    /// <summary>
    /// Security Event log adapter.
    /// </summary>
    public class SecurityEventLogAdapter : ISecurityEventLogAdapter
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SecurityEventLogAdapter" /> class.
        /// </summary>        
        public SecurityEventLogAdapter()
        {
        }

        /// <summary>
        /// Creates a new security event log.
        /// </summary>
        /// <param name="principal">IUserPrincipal object.</param>
        /// <param name="clientIp">Client IP.</param>
        /// <param name="eventType">Security event type.</param>
        /// <param name="description">Log description.</param>
        /// <param name="conn">IDbConnection object.</param>
        /// <param name="trans">IDbTransaction object.</param>
        /// <remarks>Connection being passed in must be wrapped in a using statement.</remarks>
        public void CreateSecurityEventLog(IUserPrincipal principal, string clientIp, SecurityEventType eventType, string description, IDbConnection conn, IDbTransaction trans)
        {
            PrincipalTypeT principalType = this.GetPrincipalType(principal.Type);
            
            bool isSystem = principal.InitialPrincipalType == InitialPrincipalTypeT.System;

            Guid systemBrokerGuid = new Guid("11111111-1111-1111-1111-111111111111");

            if (clientIp == "::1")
            {
                // loopback address.
                clientIp = "127.0.0.1";
            }

            List<SqlParameter> parameters = new List<SqlParameter>()
            {
                new SqlParameter("@CreatedDate", DateTime.Now),
                new SqlParameter("@ClientIp", clientIp),
                new SqlParameter("@EventType", eventType),
                new SqlParameter("@BrokerId", isSystem ? systemBrokerGuid : principal.BrokerId),
                new SqlParameter("@UserId", principal.UserId),
                new SqlParameter("@DescriptionText", description),
                new SqlParameter("@UserFirstNm", principal.FirstName),
                new SqlParameter("@UserLastNm", principal.LastName),
                new SqlParameter("@LoginNm", principal.LoginNm),
                new SqlParameter("@IsInternalUserAction", principal.InitialPrincipalType == InitialPrincipalTypeT.Internal),
                new SqlParameter("@IsSystemAction", isSystem),
                new SqlParameter("@PrincipalType", principalType)
            };

            var factory = GenericLocator<IStoredProcedureDriverFactory>.Factory;
            var driver = factory.Create(TimeoutInSeconds.Thirty);

            StoredProcedureName spName = StoredProcedureName.Create("SECURITY_LOG_CREATE").Value;

            driver.ExecuteNonQuery(conn, trans, spName, parameters);            
        }

        /// <summary>
        /// Gets security event logs for download.
        /// </summary>
        /// <param name="filter">Security event logs filter object.</param>
        /// <param name="conn">IDbConnection object.</param>
        /// <param name="maxDownloadResults">Maximum number allowed for download.</param>
        /// <returns>Security event logs.</returns>
        /// <remarks>Connection being passed in must be wrapped in a using statement.</remarks>
        public IEnumerable<SecurityEventLog> GetSecurityEventLogsForDownload(SecurityEventLogsFilter filter, IDbConnection conn, int maxDownloadResults)
        {
            List<SqlParameter> parameters = this.CreateSqlParametersForSearchAndDownload(filter);
            parameters.Add(new SqlParameter("@MaxDownloadResults", maxDownloadResults));

            var factory = GenericLocator<IStoredProcedureDriverFactory>.Factory;
            var driver = factory.Create(TimeoutInSeconds.Thirty);

            StoredProcedureName spName = StoredProcedureName.Create("SECURITY_LOG_RETRIEVE_LOGS_FOR_DOWNLOAD").Value;

            LinkedList<SecurityEventLog> logs = new LinkedList<SecurityEventLog>();
            
            using (var reader = driver.ExecuteReader(conn, null, spName, parameters))
            {
                while (reader.Read())
                {
                    SecurityEventLog log = new SecurityEventLog(reader);                        
                    logs.AddLast(log);
                }
            }               

            return logs;            
        }

        /// <summary>
        /// Search security event logs.
        /// </summary>
        /// <param name="filter">Security Events Log filter object.</param>
        /// <param name="currentPage">Current page count in the search.</param>
        /// <param name="searchResultsPerPage">Number of search results per page.</param>
        /// <param name="conn">IDbConnection object.</param>
        /// <param name="maxSearchResults">Maximum number allowed for search.</param>
        /// <returns>Search results view model.</returns>
        /// <remarks>Connection being passed in must be wrapped in a using statement.</remarks>
        public SecurityEventLogSearchResults SearchSecurityEventLog(SecurityEventLogsFilter filter, int currentPage, int searchResultsPerPage, IDbConnection conn, int maxSearchResults)
        {
            List<SqlParameter> parameters = this.CreateSqlParametersForSearchAndDownload(filter);
            parameters.Add(new SqlParameter("@Offset", currentPage * searchResultsPerPage));
            parameters.Add(new SqlParameter("@SearchResultsPerPage", searchResultsPerPage));                                    

            SecurityEventLogSearchResults results = new SecurityEventLogSearchResults();
            results.TotalSecurityEventLogsCount = -1;
            LinkedList<SecurityEventLog> logs = new LinkedList<SecurityEventLog>();

            var storedProcedure = StoredProcedureName.Create("SECURITY_LOG_SEARCH").Value;

            var factory = GenericLocator<IStoredProcedureDriverFactory>.Factory;
            var driver = factory.Create(TimeoutInSeconds.Thirty);

            using (var reader = driver.ExecuteReader(conn, null, storedProcedure, parameters))
            {
                while (reader.Read())
                {
                    if (results.TotalSecurityEventLogsCount == -1)
                    {
                        results.TotalSecurityEventLogsCount = (int)reader["MaxRows"];

                        if (results.TotalSecurityEventLogsCount > maxSearchResults)
                        {
                            throw new Exception("Too many search results");
                        }
                    }

                    SecurityEventLog log = new SecurityEventLog(reader);                        
                    logs.AddLast(log);
                }
            }
            
            results.SecurityEventLogs = logs;

            return results;
        }

        /// <summary>
        /// Creates sql parameter for searching and downloading security event logs.
        /// </summary>
        /// <param name="filter">Security event logs filter object.</param>
        /// <returns>List of sql parameter.</returns>
        private List<SqlParameter> CreateSqlParametersForSearchAndDownload(SecurityEventLogsFilter filter)
        {
            var parameters = new List<SqlParameter>();

            parameters.Add(new SqlParameter("@BrokerId", filter.BrokerId));

            if (filter.EventType != SecurityEventType.AllEvents)
            {
                parameters.Add(new SqlParameter("@EventType", filter.EventType));
            }

            if (filter.EmployeeUserId != Guid.Empty)
            {
                parameters.Add(new SqlParameter("@UserId", filter.EmployeeUserId));
            }

            if (filter.FromDate != null)
            {
                if (filter.FilterToDate == DateTime.MaxValue)
                {
                    parameters.Add(new SqlParameter("@FromDate", filter.FromDate.Value));
                }
                else if (filter.FilterToDate != null)
                {
                    parameters.Add(new SqlParameter("@FromDate", filter.FromDate.Value));
                    parameters.Add(new SqlParameter("@ToDate", filter.FilterToDate.Value));
                }
            }

            if (filter.IpAddr != string.Empty)
            {
                parameters.Add(new SqlParameter("@ClientIP", filter.IpAddr));
            }

            return parameters;
        }

        /// <summary>
        /// Get principal type.
        /// </summary>
        /// <param name="type">Principal type, in form of a string.</param>
        /// <returns>Principal type enum.</returns>
        private PrincipalTypeT GetPrincipalType(string type)
        {
            switch (type)
            {
                case "B":
                    return PrincipalTypeT.LendersOffice;
                case "P":
                    return PrincipalTypeT.OriginatorPortal;
                case "C":
                case "D":
                    return PrincipalTypeT.ConsumerPortal;
                case "I":
                    return PrincipalTypeT.Internal;
                default:
                    throw new NotImplementedException($"Principal type {type} is not allowed.");
            }
        }
    }
}
