﻿namespace Adapter.SecurityEventLogging
{
    using System.Data;
    using LqbGrammar.Adapters.SecurityEventLogging;

    /// <summary>
    /// Security Event log adapter factory.
    /// </summary>
    public class SecurityEventLogAdapterFactory : ISecurityEventLogAdapterFactory
    {
        /// <summary>
        /// Creates new security event log adapter.
        /// </summary>
        /// <returns>Security event log adapter.</returns>
        public ISecurityEventLogAdapter Create()
        {
            return new SecurityEventLogAdapter();
        }
    }
}
