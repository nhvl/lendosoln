﻿namespace Adapter
{
    using System;
    using System.IO;
    using LqbGrammar.Adapters;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers.FileSystem;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Implementation of the IBinaryFileAdapter interface.
    /// </summary>
    internal sealed class BinaryFileAdapter : IBinaryFileAdapter
    {
        /// <summary>
        /// Create or overwrite a binary file.
        /// </summary>
        /// <param name="path">The file to create or overwrite.</param>
        /// <param name="writeHandler">Delegate that writes data to the file.</param>
        public void OpenNew(LocalFilePath path, Action<LqbBinaryStream> writeHandler)
        {
            try
            {
                using (FileStream stream = File.Create(path.Value))
                {
                    writeHandler(new LqbBinaryStream(stream));
                }
            }
            catch (LqbException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new DeveloperException(ErrorMessage.SystemError, ex);
            }
        }

        /// <summary>
        /// Open an existing binary file for reading.
        /// </summary>
        /// <param name="path">The file to open.</param>
        /// <param name="readHandler">Delegate that reads data from the file.</param>
        public void OpenRead(LocalFilePath path, Action<LqbBinaryStream> readHandler)
        {
            try
            {
                if (!File.Exists(path.Value))
                {
                    throw new DeveloperException(ErrorMessage.SystemError);
                }

                using (FileStream stream = File.OpenRead(path.Value))
                {
                    readHandler(new LqbBinaryStream(stream));
                }
            }
            catch (LqbException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new DeveloperException(ErrorMessage.SystemError, ex);
            }
        }

        /// <summary>
        /// Read all the data within a binary file.
        /// </summary>
        /// <param name="path">The binary file from which the data will be read.</param>
        /// <returns>The data read from the binary file.</returns>
        public byte[] ReadAllBytes(LocalFilePath path)
        {
            try
            {
                if (!File.Exists(path.Value))
                {
                    throw new DeveloperException(ErrorMessage.SystemError);
                }

                return File.ReadAllBytes(path.Value);
            }
            catch (LqbException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new DeveloperException(ErrorMessage.SystemError, ex);
            }
        }

        /// <summary>
        /// Creates a new file, writes the specified byte array to the file,
        /// and then closes the file.  If the target file already exists, it is overwritten.
        /// </summary>
        /// <param name="path">The file to which the data is written.</param>
        /// <param name="bytes">The data that is written to the file.</param>
        public void WriteAllBytes(LocalFilePath path, byte[] bytes)
        {
            try
            {
                File.WriteAllBytes(path.Value, bytes);
            }
            catch (Exception ex)
            {
                throw new DeveloperException(ErrorMessage.SystemError, ex);
            }
        }
    }
}
