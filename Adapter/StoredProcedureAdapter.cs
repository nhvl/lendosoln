﻿namespace Adapter
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using LqbGrammar.Adapters;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// An implementation of IStoredProcedureAdapter that communicates with stored procedures in a SQL Server database.
    /// </summary>
    internal class StoredProcedureAdapter : IStoredProcedureAdapter
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="StoredProcedureAdapter"/> class.
        /// </summary>
        /// <param name="factory">The database provider factory.</param>
        /// <param name="timeout">The timeout value.</param>
        public StoredProcedureAdapter(DbProviderFactory factory, TimeoutInSeconds timeout)
        {
            this.DbFactory = factory;
            this.Timeout = timeout;
        }

        /// <summary>
        /// Gets or sets the database factory provider.
        /// </summary>
        private DbProviderFactory DbFactory { get; set; }

        /// <summary>
        /// Gets or sets the timeout value.
        /// </summary>
        private TimeoutInSeconds Timeout { get; set; }

        /// <summary>
        /// Call a stored procedure that returns data, with the full data stored in a DataSet.
        /// </summary>
        /// <param name="conn">The connection to the DB.</param>
        /// <param name="transaction">If using a transaction, pass it in here, else pass in null.</param>
        /// <param name="procedureName">The name of the stored procedure.</param>
        /// <param name="parameters">The procedure's parameters.</param>
        /// <returns>The data set containing the full returned data.</returns>
        public DataSet ExecuteDataSet(IDbConnection conn, IDbTransaction transaction, StoredProcedureName procedureName, IEnumerable<DbParameter> parameters)
        {
            IDbCommand command = this.CreateCommand(conn, transaction, procedureName, parameters);

            if (conn.State != ConnectionState.Open)
            {
                conn.OpenWithRetry();
            }

            try
            {
                using (var adapter = this.DbFactory.CreateDataAdapter())
                {
                    ((IDbDataAdapter)adapter).SelectCommand = command;
                    DataSet dataset = new DataSet();
                    adapter.Fill(dataset);
                    this.UpdateInputParameters(command, parameters);
                    return dataset;
                }
            }
            finally
            {
                command.Parameters.Clear();
            }
        }

        /// <summary>
        /// Execute a stored procedure that modifies data.
        /// </summary>
        /// <param name="conn">The connection to the DB.</param>
        /// <param name="transaction">If using a transaction, pass it in here, else pass in null.</param>
        /// <param name="procedureName">The name of the stored procedure.</param>
        /// <param name="parameters">The procedure's parameters.</param>
        /// <returns>The number of rows modified by the stored procedure.</returns>
        public ModifiedRowCount ExecuteNonQuery(IDbConnection conn, IDbTransaction transaction, StoredProcedureName procedureName, IEnumerable<DbParameter> parameters)
        {
            IDbCommand command = this.CreateCommand(conn, transaction, procedureName, parameters);

            if (conn.State != ConnectionState.Open)
            {
                conn.OpenWithRetry();
            }

            try
            {
                int count = command.ExecuteNonQuery();
                this.UpdateInputParameters(command, parameters);

                ModifiedRowCount? rowCount = ModifiedRowCount.Create(count);
                return rowCount.Value;
            }
            finally
            {
                command.Parameters.Clear();
            }
        }

        /// <summary>
        /// Call a stored procedure that returns a row set.
        /// </summary>
        /// <param name="conn">The connection to the DB.</param>
        /// <param name="transaction">If using a transaction, pass it in here, else pass in null.</param>
        /// <param name="procedureName">The name of the stored procedure.</param>
        /// <param name="parameters">The procedure's parameters.</param>
        /// <returns>An interface used to iterate through the returned row set.</returns>
        public IDataReader ExecuteReader(IDbConnection conn, IDbTransaction transaction, StoredProcedureName procedureName, IEnumerable<DbParameter> parameters)
        {
            IDbCommand command = this.CreateCommand(conn, transaction, procedureName, parameters);

            if (conn.State != ConnectionState.Open)
            {
                conn.OpenWithRetry();
            }

            try
            {
                var reader = command.ExecuteReader();
                this.UpdateInputParameters(command, parameters);
                return reader;
            }
            finally
            {
                command.Parameters.Clear();
            }
        }

        /// <summary>
        /// Call a stored procedure that returns a single value.
        /// </summary>
        /// <param name="conn">The connection to the DB.</param>
        /// <param name="transaction">If using a transaction, pass it in here, else pass in null.</param>
        /// <param name="procedureName">The name of the stored procedure.</param>
        /// <param name="parameters">The procedure's parameters.</param>
        /// <returns>The single valued return data.</returns>
        public object ExecuteScalar(IDbConnection conn, IDbTransaction transaction, StoredProcedureName procedureName, IEnumerable<DbParameter> parameters)
        {
            IDbCommand command = this.CreateCommand(conn, transaction, procedureName, parameters);

            if (conn.State != ConnectionState.Open)
            {
                conn.OpenWithRetry();
            }

            try
            {
                object obj = command.ExecuteScalar();
                this.UpdateInputParameters(command, parameters);
                return obj;
            }
            finally
            {
                command.Parameters.Clear();
            }
        }

        /// <summary>
        /// Create and populate a DbCommand object.
        /// </summary>
        /// <param name="conn">The connection to the DB.</param>
        /// <param name="transaction">If using a transaction, pass it in here, else pass in null.</param>
        /// <param name="procedureName">The name of the stored procedure.</param>
        /// <param name="parameters">The procedure's parameters.</param>
        /// <returns>An instance of DbCommand that represents the full stored procedure call.</returns>
        private IDbCommand CreateCommand(IDbConnection conn, IDbTransaction transaction, StoredProcedureName procedureName, IEnumerable<DbParameter> parameters)
        {
            IDbCommand command = conn.CreateCommand();
            command.CommandText = procedureName.ToString();
            command.CommandType = CommandType.StoredProcedure;
            command.CommandTimeout = this.Timeout.Value;
            if (transaction != null)
            {
                command.Transaction = transaction;
            }

            if (parameters != null)
            {
                foreach (var p in parameters)
                {
                    ICloneable iClone = (ICloneable)p;
                    command.Parameters.Add(iClone.Clone()); // Code may reuse parameters, so defend against that here
                }
            }

            return command;
        }

        /// <summary>
        /// Transfer the values returned from the stored procedure back to the input parameters,
        /// which is necessary since we used clones of the input parameters for the procedure call.
        /// </summary>
        /// <param name="command">The object used to make the stored procedure call.</param>
        /// <param name="parameters">The parameters passed in from the client code.</param>
        private void UpdateInputParameters(IDbCommand command, IEnumerable<DbParameter> parameters)
        {
            if (parameters != null)
            {
                foreach (var p in parameters)
                {
                    p.Value = ((IDataParameter)command.Parameters[p.ParameterName]).Value;
                }
            }
        }
    }
}
