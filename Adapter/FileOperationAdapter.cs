﻿namespace Adapter
{
    using System;
    using System.IO;
    using LqbGrammar.Adapters;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Implement file operations.
    /// </summary>
    internal sealed class FileOperationAdapter : IFileOperationAdapter
    {
        /// <summary>
        /// Copy the source file to the target file.
        /// </summary>
        /// <param name="source">The source file.</param>
        /// <param name="target">The target file.</param>
        /// <param name="allowOverwrite">True if overwriting an existing file is allowed, false otherwise.</param>
        public void Copy(LocalFilePath source, LocalFilePath target, bool allowOverwrite)
        {
            try
            {
                if (!File.Exists(source.Value))
                {
                    throw new DeveloperException(ErrorMessage.SystemError);
                }

                File.Copy(source.Value, target.Value, allowOverwrite);
            }
            catch (LqbException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new DeveloperException(ErrorMessage.SystemError, ex);
            }
        }

        /// <summary>
        /// Deletes the specified file.
        /// </summary>
        /// <param name="path">The file to delete.</param>
        public void Delete(LocalFilePath path)
        {
            try
            {
                File.Delete(path.Value);
            }
            catch (Exception ex)
            {
                throw new DeveloperException(ErrorMessage.SystemError, ex);
            }
        }

        /// <summary>
        /// Detects whether or not a specified file exists.
        /// </summary>
        /// <param name="path">The file to check.</param>
        /// <returns>True if the file exists, false otherwise.</returns>
        public bool Exists(LocalFilePath path)
        {
            try
            {
                return File.Exists(path.Value);
            }
            catch (Exception ex)
            {
                throw new DeveloperException(ErrorMessage.SystemError, ex);
            }
        }

        /// <summary>
        /// Retrieve the timestamp when the file was last modified.
        /// </summary>
        /// <param name="path">The specified file.</param>
        /// <returns>The timestamp when the file was last modified.</returns>
        public LqbEventDate GetLastWriteTime(LocalFilePath path)
        {
            try
            {
                if (!File.Exists(path.Value))
                {
                    throw new DeveloperException(ErrorMessage.SystemError);
                }

                var dt = File.GetLastWriteTime(path.Value);
                var utc = dt.ToUniversalTime();
                var ts = LqbEventDate.Create(utc);
                if (ts != null)
                {
                    return ts.Value;
                }
                else
                {
                    throw new ServerException(ErrorMessage.SystemError);
                }
            }
            catch (LqbException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new DeveloperException(ErrorMessage.SystemError, ex);
            }
        }

        /// <summary>
        /// Move the source file to the target location if the target doesn't already exist.
        /// If the target exists an exception will be thrown, the Copy and Delete methods 
        /// must be used instead.
        /// </summary>
        /// <param name="source">The file that will be moved.</param>
        /// <param name="target">The location to which the file will be moved.</param>
        public void Move(LocalFilePath source, LocalFilePath target)
        {
            try
            {
                if (!File.Exists(source.Value))
                {
                    throw new DeveloperException(ErrorMessage.SystemError);
                }

                File.Move(source.Value, target.Value);
            }
            catch (LqbException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new DeveloperException(ErrorMessage.SystemError, ex);
            }
        }

        /// <summary>
        /// Set the last write time propertry for the specified file.
        /// </summary>
        /// <param name="path">The specified file.</param>
        /// <param name="timestamp">The value that is to be set as the last write time.</param>
        public void SetLastWriteTime(LocalFilePath path, LqbEventDate timestamp)
        {
            try
            {
                if (File.Exists(path.Value))
                {
                    string ts = timestamp.ToString("O");
                    var dt = DateTime.Parse(ts, null, System.Globalization.DateTimeStyles.RoundtripKind);
                    var local = dt.ToLocalTime();

                    File.SetLastWriteTime(path.Value, local);
                }
            }
            catch (Exception ex)
            {
                throw new DeveloperException(ErrorMessage.SystemError, ex);
            }
        }

        /// <summary>
        /// Ensure the specified file is not read-only.
        /// </summary>
        /// <param name="path">The specified file.</param>
        public void TurnOffReadOnly(LocalFilePath path)
        {
            try
            {
                if (File.Exists(path.Value))
                {
                    var attributes = File.GetAttributes(path.Value);
                    if ((attributes & FileAttributes.ReadOnly) > 0)
                    {
                        attributes ^= FileAttributes.ReadOnly; // Turn off read-only bit
                        File.SetAttributes(path.Value, attributes);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new DeveloperException(ErrorMessage.SystemError, ex);
            }
        }
    }
}
