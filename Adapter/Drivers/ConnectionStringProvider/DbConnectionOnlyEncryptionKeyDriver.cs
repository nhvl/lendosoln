﻿namespace Adapter.Drivers
{
    using System.Collections.Generic;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// An implementation of <see cref="IEncryptionKeyDriver"/> that can only handle <see cref="EncryptionKeyIdentifier.DbConnectionString"/>.
    /// Any other usage will result in exceptions being thrown.
    /// </summary>
    /// <remarks>
    /// This has to live in the <c>Adapter.dll</c> rather than <c>LendersOfficeLib.dll</c>, since this is used to
    /// connect integration executables to the database.
    /// </remarks>
    public class DbConnectionOnlyEncryptionKeyDriver : IEncryptionKeyDriver
    {
        /// <summary>
        /// Prevents a default instance of the <see cref="DbConnectionOnlyEncryptionKeyDriver"/> class from being created.
        /// </summary>
        private DbConnectionOnlyEncryptionKeyDriver()
        {
        }

        /// <summary>
        /// Throws a <see cref="DeveloperException"/>.
        /// </summary>
        /// <returns>The new key identifier and the key.</returns>
        public KeyValuePair<EncryptionKeyIdentifier, EncryptionKey> GenerateKey()
        {
            throw new DeveloperException(ErrorMessage.BadConfiguration);
        }

        /// <summary>
        /// Gets the <see cref="EncryptionKey"/> associated with the <see cref="EncryptionKeyIdentifier.DbConnectionString"/>.
        /// Any other usage will throw.
        /// </summary>
        /// <param name="identifier">The identifier.</param>
        /// <returns>The key associated with the identifier.</returns>
        public EncryptionKey GetKey(EncryptionKeyIdentifier identifier)
        {
            if (identifier == EncryptionKeyIdentifier.DbConnectionString)
            {
                return this.GetDbConnectionStringKey();
            }

            throw new DeveloperException(ErrorMessage.BadConfiguration);
        }

        /// <summary>
        /// Gets the key for DB connection strings.
        /// </summary>
        /// <returns>The key for DB connection strings.</returns>
        private EncryptionKey GetDbConnectionStringKey()
        {
            // NOTE: The secure database that will hold these encryption keys in the future also holds the database connection strings.  That means that this encryption
            //       key cannot be moved to that database.  For the time being we will continue using this hard-coded value even when the other values are moved and the
            //       associated code in this class gets deleted.  In the future we will want a more flexible way to store this value, but that design is deferred for now.
            byte[] key = { 69, 191, 164, 14, 92, 188, 129, 53, 75, 205, 246, 200, 195, 239, 125, 251, 182, 70, 215, 46, 191, 27, 137, 112, 202, 164, 232, 85, 33, 173, 23, 123 };
            byte[] iv = { 171, 109, 222, 46, 9, 22, 78, 208, 210, 237, 51, 179, 48, 74, 1, 151 };

            return LqbGrammar.GenericLocator<LqbGrammar.Adapters.IDataEncryptionAdapterFactory>.Factory.CreateAesKey(201704, key, iv, null);
        }

        /// <summary>
        /// A factory for <see cref="DbConnectionOnlyEncryptionKeyDriver"/>.
        /// </summary>
        public class Factory : IEncryptionKeyDriverFactory
        {
            /// <summary>
            /// Creates an instance of <see cref="DbConnectionOnlyEncryptionKeyDriver"/>.
            /// </summary>
            /// <returns>An instance of <see cref="DbConnectionOnlyEncryptionKeyDriver"/>.</returns>
            public IEncryptionKeyDriver Create()
            {
                return new DbConnectionOnlyEncryptionKeyDriver();
            }
        }
    }
}
