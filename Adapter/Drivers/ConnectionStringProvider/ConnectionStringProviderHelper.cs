﻿namespace Adapter.Drivers
{
    using System;
    using System.Collections.Generic;
    using Adapter.Drivers.ConnectionStringProvider;
    using LqbGrammar;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Helper to allow access to secured connection strings from external 
    /// applications.  One must call Register() before accessing other methods.
    /// It is also required to have a secure key file and these config keys
    /// defined: SecureDBConnectionString, SecureConfigFilePath.
    /// </summary>
    public static class ConnectionStringProviderHelper
    {
        /// <summary>
        /// Get a connection string defined in the secure database by its Name.
        /// For example, "LOShareROnly", "RateSheet", "Hangfire", etc.
        /// </summary>
        /// <param name="databaseSource">The database source, such as "LOShareROnly".</param>
        /// <returns>The connection string.</returns>
        public static string GetConnectionStringBySourceName(string databaseSource)
        {
            var driver = GetDriver();
            return driver.GetConnectionStringBySourceName(databaseSource);
        }

        /// <summary>
        /// Get a connection string by hard coded connection Id.
        /// </summary>
        /// <remarks>
        /// Consider using GetConnectionStringBySourceName if possible.
        /// This alternative is offered because our source list can get out
        /// of sync with the database.
        /// </remarks>
        /// <param name="connectionStringId">The connection string id.</param>
        /// <returns>The corresponding connection string.</returns>
        public static string GetConnectionStringById(Guid connectionStringId)
        {
            var driver = GetDriver();
            return driver.GetConnectionStringById(connectionStringId);
        }

        /// <summary>
        /// Get a main database connection string by database name.  For 
        /// example, "LendersOffice_2".  For safety, only request write access 
        /// if it is truly needed.
        /// </summary>
        /// <param name="databaseName">Name of the database.</param>
        /// <param name="needWriteAccess">If write access is needed.</param>
        /// <returns>The requested connection string.</returns>
        public static string GetMainConnectionStringByDatabaseName(string databaseName, bool needWriteAccess)
        {
            var driver = GetDriver();
            return driver.GetMainConnectionStringByDatabaseName(databaseName, needWriteAccess);
        }

        /// <summary>
        /// Get list of all main database connection strings.  Use when action 
        /// is required across all databases.
        /// </summary>
        /// <param name="needWriteAccess">If write access is required.</param>
        /// <returns>IEnumerable containing all the connection strings.</returns>
        public static IEnumerable<string> ListAllMainConnectionStrings(bool needWriteAccess)
        {
            var driver = GetDriver();
            return driver.ListAllMainConnectionStrings(needWriteAccess);
        }

        /// <summary>
        /// Registers all applicable adapters, drivers and driver factories to 
        /// be used by ConnectionStringProviderDriver.
        /// </summary>
        /// <param name="applicationName">The name of the calling application.</param>
        public static void Register(string applicationName)
        {
            IExceptionHandlerFactory[] arrHandlers = ExceptionHandlerUtil.GetCoreExceptionHandlerFactories();
            using (IApplicationInitialize iAppInit = LqbApplication.CreateInitializer(arrHandlers))
            {
                iAppInit.SetName(applicationName);
                Adapter.ApplicationInitializer.RegisterFactories(applicationName, iAppInit);
                iAppInit.Register<IConnectionStringProviderDriverFactory>(new ConnectionStringProviderDriverFactory());
                iAppInit.Register<LqbGrammar.Drivers.IEncryptionKeyDriverFactory>(new DbConnectionOnlyEncryptionKeyDriver.Factory());
            }
        }

        /// <summary>
        /// Get the driver to use.
        /// </summary>
        /// <returns>The driver to be used.</returns>
        private static IConnectionStringProviderDriver GetDriver()
        {
            var factory = GenericLocator<IConnectionStringProviderDriverFactory>.Factory;
            return factory.Create();
        }
    }
}
