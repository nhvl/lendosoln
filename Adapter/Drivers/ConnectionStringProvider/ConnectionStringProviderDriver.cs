﻿namespace Adapter.Drivers
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Text;
    using System.Xml.Linq;
    using LqbGrammar;
    using LqbGrammar.Adapters;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// A driver to support loading connnection strings from outside the
    /// lqb app.
    /// </summary>
    internal interface IConnectionStringProviderDriver : ILqbDriver
    {
        // NOTE: If we want to encourage better design of our external executibles,
        // we would probably want to add the methods here that would calculate the
        // connection string by customercode, brokerid, and userid, so the caller
        // would not need know where the lender of interest is.  There are stored
        // procedures in the main database that will provide this and can be 
        // called directly, but it might make sense to put those here.

        /// <summary>
        /// Get a main database connection string by database name.  For 
        /// example, "LendersOffice_2".  For safety, only request write access 
        /// if it is truly needed.
        /// </summary>
        /// <param name="databaseName">Name of the database.</param>
        /// <param name="needWriteAccess">If write access is needed.</param>
        /// <returns>The requested connection string.</returns>
        string GetMainConnectionStringByDatabaseName(string databaseName, bool needWriteAccess);

        /// <summary>
        /// Get a connection string defined in the secure database by its Name.
        /// For example, "LOShareROnly", "RateSheet", "Hangfire", etc.
        /// </summary>
        /// <param name="databaseSource">The database source, such as "LOShareROnly".</param>
        /// <returns>The connection string.</returns>
        string GetConnectionStringBySourceName(string databaseSource);

        /// <summary>
        /// Get a connection string by hard coded connection Id.
        /// </summary>
        /// <remarks>
        /// Consider using GetConnectionStringBySourceName if possible.
        /// This alternative is offered because our source list can get out
        /// of sync with the database.
        /// </remarks>
        /// <param name="connectionStringId">The connection string id.</param>
        /// <returns>The corresponding connection string.</returns>
        string GetConnectionStringById(Guid connectionStringId);

        /// <summary>
        /// Get list of all main database connection strings.  Use when action 
        /// is required across all databases.
        /// </summary>
        /// <param name="needWriteAccess">If write access is required.</param>
        /// <returns>IEnumerable containing all the connection strings.</returns>
        IEnumerable<string> ListAllMainConnectionStrings(bool needWriteAccess);
    }

    /// <summary>
    /// <para>A driver that will allow access of the encrypted connection 
    /// strings.</para>
    /// <para>So we have two tables where database connection strings can be 
    /// found: Secure.Connection_String, and Main.DB_Connection.  If an app
    /// needs a standard connection string, they are pulled directly from the 
    /// secure database.  If a specific main database connection string is 
    /// needed, it becomes a two-step process: (1) get the secure connection 
    /// string to make a query to get the main shared connection string. (2)
    /// use the shared connection string to query for the specific needed main 
    /// connection string.
    /// </para>
    /// <para> A note on exception handling here: The errors are intentially 
    /// generic, so as not to introduce information leakage.  Ideally the 
    /// caller has access to this code and can use the stack trace to identify 
    /// the problem.</para>
    /// </summary>
    internal class ConnectionStringProviderDriver : IConnectionStringProviderDriver
    {
        /// <summary>
        /// This is a mapping between a connection string name and connection 
        /// string id.  These are stored in the secure database's 
        /// Connection_String table and should be the same at all stages. This 
        /// should be kept in sync with the main library's 
        /// DbConnectionStringManager.ConnectionStringMappings.
        /// </summary>
        private static readonly Dictionary<string, Guid> ConnectionStringMappings
            = new Dictionary<string, Guid>(StringComparer.OrdinalIgnoreCase)
            {
                ["LOShare"] = new Guid("cd3ee90d-ae4a-40a6-8b72-5aa22fbaec68"),
                ["LOShareROnly"] = new Guid("4ac184e8-1e3f-4bc8-be5d-5506a36ca108"),
                ["LOTransient"] = new Guid("f7087a23-2ef0-4ba4-9736-8c1a24b0ba8e"),
                ["LpeRelease1"] = new Guid("6f41e284-8a78-4f95-970f-7106d6971a4b"),
                ["LpeRelease2"] = new Guid("eca34252-38e6-4aec-927e-0a1e74173f1c"),
                ["LpeServerSnapshot"] = new Guid("c70a15d1-4def-406d-bbed-dad954e9c014"),
                ["LpeSrc"] = new Guid("4a8c0f29-7fdf-4dea-a3d9-27fc1a6f3452"),
                ["QueryReport"] = new Guid("4868163c-703c-4c1d-8f4c-b03e25d08255"),
                ["RateSheet"] = new Guid("415f331a-51a1-485b-8b55-5affd41ba9b8"),
                ["Hangfire"] = new Guid("f3820290-9b57-49e9-9e15-50836e6ddc52")
            };

        /// <summary>
        /// The connection id of our read-only connection that will be used 
        /// to further load connections from our shared database.
        /// </summary>
        private static readonly Guid ReadOnlyConnectionId = new Guid("4ac184e8-1e3f-4bc8-be5d-5506a36ca108");

        /// <summary>
        /// The config key where the secure connection string resides.
        /// </summary>
        private static readonly string SecureDBConnectionStringKey = "SecureDBConnStrReadOnly";

        /// <summary>
        /// The config key where the xml secure key file path resides.
        /// </summary>
        private static readonly string SecureConfigFilePathKey = "SecureConfigFilePath";

        /// <summary>
        /// Lazy-loaded list of the connection strings from the secure database.
        /// Current implementation has these in the secure.Connection_String table.
        /// </summary>
        private Lazy<Dictionary<Guid, DBConnectionString>> secureDefinedConnectionStrings =
            new Lazy<Dictionary<Guid, DBConnectionString>>(() => { return LoadSecureDefinedConnectionStrings(); });

        /// <summary>
        /// Lazy-loaded list of the connection strings from the main Main database.
        /// Current implementation has these in the main.DB_Connection table.
        /// </summary>
        private Lazy<Dictionary<string, Tuple<DBConnectionString, DBConnectionString>>> mainConnectionStrings =
            new Lazy<Dictionary<string, Tuple<DBConnectionString, DBConnectionString>>>(() => { return LoadMainConnectionStrings(); });

        /// <summary>
        /// Connection strings from the secure database.
        /// </summary>
        private Dictionary<Guid, DBConnectionString> SecureDefinedConnectionStrings => this.secureDefinedConnectionStrings.Value;

        /// <summary>
        /// Connection strings from the main database.
        /// </summary>
        private Dictionary<string, Tuple<DBConnectionString, DBConnectionString>> MainConnectionStrings => this.mainConnectionStrings.Value;

        /// <summary>
        /// Get a main database connection string by database name.  For 
        /// example, "LendersOffice_2".  For safety, only request write access 
        /// if it is truly needed.
        /// </summary>
        /// <param name="databaseName">Name of the database.</param>
        /// <param name="needWriteAccess">If write access is needed.</param>
        /// <returns>The requested connection string.</returns>
        public string GetMainConnectionStringByDatabaseName(string databaseName, bool needWriteAccess)
        {
            Tuple<DBConnectionString, DBConnectionString> connectionStringPair;
            if (!this.MainConnectionStrings.TryGetValue(databaseName, out connectionStringPair))
            {
                // Requesting a database name that does exist in our table
                // in the main database.
                throw new DeveloperException(ErrorMessage.SystemError);
            }

            var connectionString = needWriteAccess ? connectionStringPair.Item2 : connectionStringPair.Item1;

            return connectionString.Value;
        }

        /// <summary>
        /// Get a connection string defined in the secure database by its Name.
        /// For example, "LOShareROnly", "RateSheet", "Hangfire", etc.
        /// </summary>
        /// <param name="databaseSource">The database source, such as "LOShareROnly".</param>
        /// <returns>The connection string.</returns>
        public string GetConnectionStringBySourceName(string databaseSource)
        {
            Guid connectionStringId;
            if (!ConnectionStringMappings.TryGetValue(databaseSource, out connectionStringId))
            {
                // Requesting a database source that does exist in our 
                // hard-coded list.
                throw new DeveloperException(ErrorMessage.SystemError);
            }

            return this.GetConnectionStringById(connectionStringId);
        }

        /// <summary>
        /// Get a connection string by hard coded connection Id.
        /// </summary>
        /// <remarks>
        /// Consider using GetConnectionStringBySourceName if possible.
        /// This alternative is offered because our source list can get out
        /// of sync with the database.
        /// </remarks>
        /// <param name="connectionStringId">The connection string id.</param>
        /// <returns>The corresponding connection string.</returns>
        public string GetConnectionStringById(Guid connectionStringId)
        {
            DBConnectionString connectionString;
            if (!this.SecureDefinedConnectionStrings.TryGetValue(connectionStringId, out connectionString))
            {
                // Requesting a database id that does exist in our secure
                // database table.
                throw new DeveloperException(ErrorMessage.SystemError);
            }

            return connectionString.Value;
        }

        /// <summary>
        /// Get list of all main database connection strings.  Use when action 
        /// is required across all databases.
        /// </summary>
        /// <param name="needWriteAccess">If write access is required.</param>
        /// <returns>IEnumerable containing all the connection strings.</returns>
        public IEnumerable<string> ListAllMainConnectionStrings(bool needWriteAccess)
        {
            List<string> connectionStrings = new List<string>();
            foreach (var databaseName in this.MainConnectionStrings.Keys)
            {
                connectionStrings.Add(this.GetMainConnectionStringByDatabaseName(databaseName, needWriteAccess));
            }

            return connectionStrings;
        }
        
        /// <summary>
        /// Loads the database connection strings that are defined in the 
        /// secure database.  Decrypts the keys using the defined key file.
        /// </summary>
        /// <returns>The database connection strings that are defined in the 
        /// secure database.</returns>
        private static Dictionary<Guid, DBConnectionString> LoadSecureDefinedConnectionStrings()
        {
            var secureDefinedConnectionStrings = new Dictionary<Guid, DBConnectionString>();
            var rawConnectionStrings = ReadConnectionStringsFromSecureDatabase();
            var encryptionKeys = GetEncryptionKeysFromFile();

            foreach (var connectionString in rawConnectionStrings)
            {
                var decryptedConnectionString = DecryptConnectionString(encryptionKeys, connectionString.Value);
                secureDefinedConnectionStrings.Add(connectionString.Key, decryptedConnectionString);
            }

            return secureDefinedConnectionStrings;
        }

        /// <summary>
        /// Decrypt a value that was read from the XML file.
        /// </summary>
        /// <param name="encrypted">The value that was read from the XML file.</param>
        /// <returns>The decrypted value.</returns>
        private static byte[] DecryptXmlValue(byte[] encrypted)
        {
            try
            {
                return Adapter.LqbEncryptor.Decrypt(EncryptionKeyIdentifier.DbConnectionString, encrypted);
            }
            catch (LqbException)
            {
                // Assume the value was in plain text.
                return encrypted;
            }
            catch (System.Security.Cryptography.CryptographicException)
            {
                // Assume the value was in plain text.
                return encrypted;
            }
        }

        /// <summary>
        /// Load the connection strings from the main database.
        /// </summary>
        /// <returns>A tuple containing both the read-only and read-write 
        /// connection strings from the main database.</returns>
        private static Dictionary<string, Tuple<DBConnectionString, DBConnectionString>> LoadMainConnectionStrings()
        {
            var rawMainConnectionStrings = new Dictionary<string, Tuple<DBConnectionString, DBConnectionString>>(StringComparer.OrdinalIgnoreCase);
            var encryptionKeys = GetEncryptionKeysFromFile();
            var secureConnectionString = GetSecureDBConnectionString();
            var adapter = GetStoredProcedureAdapter();
            var listAllStoredProcedure = StoredProcedureName.Create("CONNECTION_STRING_ListAll").Value;

            using (var conn = new SqlConnection(secureConnectionString.Value))
            {
                using (var reader = adapter.ExecuteReader(conn, null, listAllStoredProcedure, null))
                {
                    while (reader.Read())
                    {
                        string connectionType = reader["ConnectionType"].ToString();
                        if (connectionType != "Split")
                        {
                            continue;
                        }

                        string initialCatalog = reader["InitialCatalog"].ToString();
                        var connectionStrings = ReadDbConnectionStringRow(reader);
                        rawMainConnectionStrings.Add(initialCatalog, connectionStrings);
                    }
                }
            }

            var mainConnectionStrings = new Dictionary<string, Tuple<DBConnectionString, DBConnectionString>>(StringComparer.OrdinalIgnoreCase);
            foreach (var connectionName in rawMainConnectionStrings.Keys)
            {
                var decryptedReadOnlyConnectionString = DecryptConnectionString(encryptionKeys, rawMainConnectionStrings[connectionName].Item1);
                var decryptedReadWriteConnectionString = DecryptConnectionString(encryptionKeys, rawMainConnectionStrings[connectionName].Item2);

                mainConnectionStrings.Add(connectionName, Tuple.Create(decryptedReadOnlyConnectionString, decryptedReadWriteConnectionString));
            }

            return mainConnectionStrings;
        }

        /// <summary>
        /// Reads a database connection string row from a reader.
        /// </summary>
        /// <param name="reader">The reader to use.</param>
        /// <returns>The connection strings pulled from the database row.</returns>
        private static Tuple<DBConnectionString, DBConnectionString> ReadDbConnectionStringRow(IDataReader reader)
        {
            string dataSource = reader["DataSource"].ToString();
            string initialCatalog = reader["InitialCatalog"].ToString();
            string failOverPartner = reader["FailoverPartner"].ToString();
            string readOnlyLogin = reader["ReadOnlyUserId"].ToString();
            string readOnlyEncryptedPassword = reader["ReadOnlyPassword"].ToString();
            string fullAccessLogin = reader["UserId"].ToString();
            string fullAccessEncryptedPassword = reader["Password"].ToString();

            var readOnlyBuilder = new SqlConnectionStringBuilder();
            readOnlyBuilder.DataSource = dataSource;
            readOnlyBuilder.InitialCatalog = initialCatalog;
            readOnlyBuilder.UserID = readOnlyLogin;
            readOnlyBuilder.Password = readOnlyEncryptedPassword;
            readOnlyBuilder.FailoverPartner = failOverPartner;

            var readWriteBuilder = new SqlConnectionStringBuilder();
            readWriteBuilder.DataSource = dataSource;
            readWriteBuilder.InitialCatalog = initialCatalog;
            readWriteBuilder.UserID = fullAccessLogin;
            readWriteBuilder.Password = fullAccessEncryptedPassword;
            readWriteBuilder.FailoverPartner = failOverPartner;

            var readOnlyConnectionString = DBConnectionString.Create(readOnlyBuilder.ConnectionString);
            if (!readOnlyConnectionString.HasValue)
            {
                // The readonly connection string is not valid.
                throw new DeveloperException(ErrorMessage.BadConfiguration);
            }

            var readWriteConnectionString = DBConnectionString.Create(readWriteBuilder.ConnectionString);
            if (!readWriteConnectionString.HasValue)
            {
                // The readwrite connection string is not valid.
                throw new DeveloperException(ErrorMessage.BadConfiguration);
            }

            return Tuple.Create(readOnlyConnectionString.Value, readWriteConnectionString.Value);
        }

        /// <summary>
        /// Read the connection strings from the secure database.
        /// </summary>
        /// <returns>Connection strings from the secure database.</returns>
        private static Dictionary<Guid, DBConnectionString> ReadConnectionStringsFromSecureDatabase()
        {
            var connectionStrings = new Dictionary<Guid, DBConnectionString>();
            var secureConnectionString = GetSecureDBConnectionString();
            var adapter = GetStoredProcedureAdapter();
            var listAllStoredProcedure = StoredProcedureName.Create("CONNECTION_STRING_ListAll").Value;

            using (var conn = new SqlConnection(secureConnectionString.Value))
            {
                using (var reader = adapter.ExecuteReader(conn, null, listAllStoredProcedure, null))
                {
                    while (reader.Read())
                    {
                        string connectionType = reader["ConnectionType"].ToString();
                        if (connectionType == "Split")
                        {
                            continue;
                        }

                        Guid stringId = (Guid)reader["Id"];

                        var builder = new SqlConnectionStringBuilder();
                        builder.DataSource = reader["DataSource"].ToString();
                        builder.InitialCatalog = reader["InitialCatalog"].ToString();
                        builder.UserID = reader["UserId"].ToString();
                        builder.Password = reader["Password"].ToString();
                        builder.FailoverPartner = reader["FailoverPartner"].ToString();
                        builder.Encrypt = (bool)reader["Encrypt"];

                        connectionStrings.Add(stringId, DBConnectionString.Create(builder.ConnectionString).Value);
                    }
                }
            }

            return connectionStrings;
        }

        /// <summary>
        /// Get the secure database connection string from the local file.
        /// </summary>
        /// <returns>The secure database connection string.</returns>
        private static DBConnectionString GetSecureDBConnectionString()
        {
            var configSecureConnectionString = System.Configuration.ConfigurationManager.AppSettings[SecureDBConnectionStringKey];

            if (string.IsNullOrEmpty(configSecureConnectionString))
            {
                // Need to define SecureDBConnectionString in config file.
                throw new DeveloperException(ErrorMessage.BadConfiguration);
            }

            var secureConnectionString = DBConnectionString.Create(configSecureConnectionString);
            if (!secureConnectionString.HasValue)
            {
                // The secure connection string in the config file is not a 
                // valid connection string.
                throw new DeveloperException(ErrorMessage.BadConfiguration);
            }

            var decryptedSecureConnectionString = DecryptConnectionStringUsingDefaultKey(secureConnectionString.Value);
            return decryptedSecureConnectionString;
        }

        /// <summary>
        /// Decrypt the connection string using default key.
        /// </summary>
        /// <param name="encryptedConnectionString">The encrypted connection string.</param>
        /// <returns>The decrypted connection string.</returns>
        private static DBConnectionString DecryptConnectionStringUsingDefaultKey(DBConnectionString encryptedConnectionString)
        {
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(encryptedConnectionString.Value);
            var encryptedUserId = Convert.FromBase64String(builder.UserID);
            var encryptedPassword = Convert.FromBase64String(builder.Password);

            builder.UserID = Encoding.UTF8.GetString(Adapter.LqbEncryptor.Decrypt(EncryptionKeyIdentifier.DbConnectionString, encryptedUserId));
            builder.Password = Encoding.UTF8.GetString(Adapter.LqbEncryptor.Decrypt(EncryptionKeyIdentifier.DbConnectionString, encryptedPassword));

            var connectionString = DBConnectionString.Create(builder.ConnectionString);

            if (!connectionString.HasValue)
            {
                // Decryption did not result in a valid connection string.
                throw new DeveloperException(ErrorMessage.SystemError);
            }

            return connectionString.Value;
        }

        /// <summary>
        /// Get a StoredProcedureAdapter.
        /// </summary>
        /// <returns>A StoredProcedureAdapter.</returns>
        private static IStoredProcedureAdapter GetStoredProcedureAdapter()
        {
            var databaseProviderFactory = System.Data.Common.DbProviderFactories.GetFactory("System.Data.SqlClient");
            var factory = GenericLocator<IStoredProcedureAdapterFactory>.Factory;
            return factory.Create(databaseProviderFactory, TimeoutInSeconds.Default);
        }

        /// <summary>
        /// Get a Base64EncodingAdapter.
        /// </summary>
        /// <returns>A Base64EncodingAdapter.</returns>
        private static IBase64EncodingAdapter GetBase64EncodingAdapter()
        {
            var factory = GenericLocator<IBase64EncodingAdapterFactory>.Factory;
            return factory.Create();
        }

        /// <summary>
        /// Get a XmlFileAdapter.
        /// </summary>
        /// <returns>A XmlFileAdapter.</returns>
        private static IXmlFileAdapter GetXmlFileAdapter()
        {
            var factory = GenericLocator<IFileSystemAdapterFactory>.Factory;
            return factory.CreateXmlFileAdapter();
        }

        /// <summary>
        /// Get a DataEncryptionAdapter.
        /// </summary>
        /// <returns>A DataEncryptionAdapter.</returns>
        private static IDataEncryptionAdapter GetDataEncryptionAdapter()
        {
            var factory = GenericLocator<IDataEncryptionAdapterFactory>.Factory;
            return factory.Create();
        }

        /// <summary>
        /// Create an encryption key.
        /// </summary>
        /// <param name="version">Key version.</param>
        /// <param name="key">The Key to be used.</param>
        /// <param name="iv">The initialization vector.</param>
        /// <returns>The encryption key.</returns>
        private static EncryptionKey CreateEncryptionKey(int version, byte[] key, byte[] iv)
        {
            var factory = GenericLocator<IDataEncryptionAdapterFactory>.Factory;
            return factory.CreateAesKey(version, key, iv, null);
        }

        /// <summary>
        /// Read the encryption keys from the local file.
        /// </summary>
        /// <returns>The encryption keys from the local file.</returns>
        private static List<EncryptionKey> GetEncryptionKeysFromFile()
        {
            var secureConfigFilePathString = System.Configuration.ConfigurationManager.AppSettings[SecureConfigFilePathKey];

            if (string.IsNullOrEmpty(secureConfigFilePathString))
            {
                // Need to define SecureConfigFilePath in config file.
                throw new DeveloperException(ErrorMessage.BadConfiguration);
            }

            var secureConfigFilePath = LocalFilePath.Create(secureConfigFilePathString);

            if (string.IsNullOrEmpty(secureConfigFilePathString))
            {
                // Invalid file path defined.  
                throw new DeveloperException(ErrorMessage.BadConfiguration);
            }

            var xmlFileAdapter = GetXmlFileAdapter();

            var lqbXmlDoc = xmlFileAdapter.Load(secureConfigFilePath.Value);

            var encryptionKeys = new List<EncryptionKey>();
            var currentKey = ParseKeyFromXml(lqbXmlDoc, "Current");
            encryptionKeys.Add(currentKey);

            var previousKey = ParseKeyFromXml(lqbXmlDoc, "Previous");
            encryptionKeys.Add(previousKey);
            
            return encryptionKeys;
        }

        /// <summary>
        /// Parse a key from the xml.
        /// </summary>
        /// <param name="lqbXmlDoc">The xml doc.</param>
        /// <param name="node">The node name to be loaded.</param>
        /// <returns>The parsed encryption key data.</returns>
        private static EncryptionKey ParseKeyFromXml(LqbXmlElement lqbXmlDoc, string node)
        {
            var xmlDoc = lqbXmlDoc.Contained;

            var keyElement = xmlDoc.Descendants().Where(p => p.Name == node).First();
            var key = LoadKeyFromXmlElement(keyElement);

            return key;
        }

        /// <summary>
        /// Load the encryption key from the xml element describing it.
        /// </summary>
        /// <param name="keyElement">Xml element describing the key.</param>
        /// <returns>EncryptionKey built from the xml element.</returns>
        private static EncryptionKey LoadKeyFromXmlElement(XElement keyElement)
        {
            var keyString = keyElement.Elements().First(p => p.Name == "Key").Value;

            if (keyString == string.Empty)
            {
                // A blank key. Should be treated as not decrypting.
                return null;
            }

            var iVString = keyElement.Elements().First(p => p.Name == "IV").Value;
            var versionString = keyElement.Attribute("version").Value;

            var iVBase64 = Base64EncodedData.Create(iVString);
            var keyBase64 = Base64EncodedData.Create(keyString);

            var decoder = GetBase64EncodingAdapter();
            var key = decoder.Decode(keyBase64.Value);
            var iv = decoder.Decode(iVBase64.Value);

            key = DecryptXmlValue(key);
            iv = DecryptXmlValue(iv);

            var version = Convert.ToInt32(versionString);

            return CreateEncryptionKey(version, key, iv);
        }

        /// <summary>
        /// Decrypt a connection string using the provided keys.
        /// </summary>
        /// <param name="keys">The list of keys to try.</param>
        /// <param name="connectionString">The connection string to decrypt.</param>
        /// <returns>The decrypted connection string.</returns>
        private static DBConnectionString DecryptConnectionString(List<EncryptionKey> keys, DBConnectionString connectionString)
        {
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(connectionString.Value);
            var userId = builder.UserID;
            var password = builder.Password;
            var encodedUserId = Base64EncodedData.Create(userId);
            var encodedPassword = Base64EncodedData.Create(password);
            bool validBase64 = encodedUserId.HasValue && encodedPassword.HasValue;

            var decoder = GetBase64EncodingAdapter();
            var encryptedUserId = validBase64 ? decoder.Decode(encodedUserId.Value) : null;
            var encryptedPassword = validBase64 ? decoder.Decode(encodedPassword.Value) : null;

            var decrypter = GetDataEncryptionAdapter();
            byte[] decryptedUserId = null;
            byte[] decryptedPassword = null;
            bool success = false;

            bool plainTextValid = false;
            foreach (var key in keys)
            {
                if (key == null)
                {
                    // A blank key indicates plain text.
                    plainTextValid = true;
                    continue;
                }

                if (validBase64)
                {
                    try
                    {
                        decryptedUserId = decrypter.Decrypt(encryptedUserId, key);
                        decryptedPassword = decrypter.Decrypt(encryptedPassword, key);
                        success = true;
                        break;
                    }
                    catch (Exception exc) 
                    {
                        var baseException = exc.GetBaseException();
                        if (baseException is System.Security.Cryptography.CryptographicException
                              || baseException is FormatException)
                        {
                            // Decryption fails, but another key might work.
                            success = false;
                            continue;
                        }

                        throw;
                    }
                }
            }

            if (success)
            {
                var plainTextUserId = Encoding.UTF8.GetString(decryptedUserId);
                var plainTextPassword = Encoding.UTF8.GetString(decryptedPassword);

                builder.UserID = plainTextUserId;
                builder.Password = plainTextPassword;

                var plainDbConnectionString = DBConnectionString.Create(builder.ConnectionString);

                if (!plainDbConnectionString.HasValue)
                {
                    // Did not decrypt to valid connection string.
                    throw new DeveloperException(ErrorMessage.SystemError);
                }

                return plainDbConnectionString.Value;
            }
            else
            {
                if (plainTextValid)
                {
                    return connectionString;
                }

                // Failed to sucessfully decrypt using any key.
                throw new DeveloperException(ErrorMessage.SystemError);
            }
        }
    }
}
