﻿namespace Adapter.Drivers.ConnectionStringProvider
{
    using LqbGrammar;

    /// <summary>
    /// Factory for IConnectionStringProviderDriver.
    /// </summary>
    internal interface IConnectionStringProviderDriverFactory : ILqbFactory
    {
        /// <summary>
        /// Create ConnectionStringProviderDriver.
        /// </summary>
        /// <returns>An instance of IConnectionStringProviderDriver.</returns>
        IConnectionStringProviderDriver Create();
    }

    /// <summary>
    /// Factory for ConnectionStringProviderDriver.
    /// </summary>
    internal class ConnectionStringProviderDriverFactory : IConnectionStringProviderDriverFactory
    {
        /// <summary>
        /// Create ConnectionStringProviderDriver.
        /// </summary>
        /// <returns>An instance of ConnectionStringProviderDriver.</returns>
        public IConnectionStringProviderDriver Create()
        {
            return new ConnectionStringProviderDriver();
        }
    }
}
