﻿namespace Adapter
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Runtime.Serialization.Json;
    using System.Web.Script.Serialization;
    using LqbGrammar.Adapters;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;
    using LqbGrammar.Exceptions;
    using LqbGrammar.Utils;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;
    using Newtonsoft.Json.Serialization;

    /// <summary>
    /// Implementation of the IJsonAdapter interface.
    /// </summary>
    public class JsonAdapter : IJsonAdapter
    {
        /// <summary>
        /// The maximum length of a serialized JSON string. Keep this value in sync
        /// with the <code>jsonSerialization</code> element in the web.config files
        /// for LendersOfficeApp and PML.
        /// </summary>
        public const int MaxJsonLength = 5000000;

        /// <summary>
        /// The serializer to use.
        /// </summary>
        private LqbGrammar.Drivers.JsonSerializer serializer;

        /// <summary>
        /// The options to control the serialization process.
        /// </summary>
        private JsonSerializerOptions options;

        /// <summary>
        /// Initializes a new instance of the <see cref="JsonAdapter"/> class.
        /// </summary>
        /// <param name="serializer">The serializer to use.</param>
        /// <param name="options">The options to control the serialization.</param>
        internal JsonAdapter(LqbGrammar.Drivers.JsonSerializer serializer, JsonSerializerOptions options)
        {
            this.serializer = serializer;
            this.options = options;
        }

        /// <summary>
        /// Deserialize a JSON string back to an instance of the indicated type.
        /// </summary>
        /// <typeparam name="T">The type of the object to be deserialize.</typeparam>
        /// <param name="serialized">The JSON serialization of an instance of the target type.</param>
        /// <returns>The deserialized instance of the target type.</returns>
        public T Deserialize<T>(string serialized) where T : class, new()
        {
            if (string.IsNullOrEmpty(serialized))
            {
                return null;
            }

            switch (this.serializer)
            {
                case LqbGrammar.Drivers.JsonSerializer.Default:
                    return this.HandleDefaultDeserialize<T>(serialized);

                case LqbGrammar.Drivers.JsonSerializer.DataContract:
                    return this.HandleDataContractDeserialize<T>(serialized);

                case LqbGrammar.Drivers.JsonSerializer.Javascript:
                    return this.HandleJavascriptDeserialize<T>(serialized);

                case LqbGrammar.Drivers.JsonSerializer.JsonConvert:
                    return this.HandleJsonConvertDeserialize<T>(serialized);

                default:
                    throw new DeveloperException(ErrorMessage.SystemError);
            }
        }

        /// <summary>
        /// Deserialize a JSON string back to an instance of the indicated type.
        /// </summary>
        /// <typeparam name="T">The type of the object to be deserialize.</typeparam>
        /// <typeparam name="R">The type used as the converter.</typeparam>
        /// <param name="streamReader">The JSON serialization of an instance of the target type.</param>
        /// <param name="converter">The converter used during deserialization.</param>
        /// <returns>The deserialized instance of the target type.</returns>
        public T Deserialize<T, R>(StreamReader streamReader, R converter)
            where T : class, new()
            where R : class
        {
            if (streamReader == null || streamReader.EndOfStream)
            {
                return null;
            }

            if (this.serializer != LqbGrammar.Drivers.JsonSerializer.Default)
            {
                throw new DeveloperException(ErrorMessage.SystemError);
            }

            if (this.options != JsonSerializerOptions.None)
            {
                throw new DeveloperException(ErrorMessage.SystemError);
            }

            // Q: Why use a typeparam when it is forced to be an instance of JsonConverter?
            // A: Primariy for symmetry with the Serialization method with a converter.
            //    The JsonConverter class is abstract and subclasses are passed in here, and 
            //    we might pass in LQB classes or Newtonsoft classes.  In the latter case 
            //    we would still like to keep the interface itself independent from the 
            //    underlying library.
            if (converter is JsonConverter)
            {
                using (JsonReader reader = new JsonTextReader(streamReader))
                {
                    JsonSerializerSettings settings = new JsonSerializerSettings
                    {
                        Converters = new List<JsonConverter> { converter as JsonConverter },
                        StringEscapeHandling = StringEscapeHandling.EscapeHtml
                    };

                    var serializer = Newtonsoft.Json.JsonSerializer.Create(settings);
                    return serializer.Deserialize<T>(reader);
                }
            }
            else
            {
                throw new DeveloperException(ErrorMessage.SystemError);
            }
        }

        /// <summary>
        /// Serialize an object of type T to a JSON string.
        /// </summary>
        /// <typeparam name="T">The type of the object to be serialized.</typeparam>
        /// <param name="forSerialization">An instance of the type to serialize.</param>
        /// <returns>The serialization of the instance to a JSON string.</returns>
        public string Serialize<T>(T forSerialization) where T : class, new()
        {
            if (forSerialization == null)
            {
                return string.Empty;
            }

            switch (this.serializer)
            {
                case LqbGrammar.Drivers.JsonSerializer.Default:
                    return this.HandleDefaultSerialize<T>(forSerialization);

                case LqbGrammar.Drivers.JsonSerializer.DataContract:
                    return this.HandleDataContractSerialize<T>(forSerialization);

                case LqbGrammar.Drivers.JsonSerializer.Javascript:
                    return this.HandleJavascriptSerialize<T>(forSerialization);

                case LqbGrammar.Drivers.JsonSerializer.JsonConvert:
                    return this.HandlJsonConvertSerialize<T>(forSerialization);

                default:
                    throw new DeveloperException(ErrorMessage.SystemError);
            }
        }

        /// <summary>
        /// Serialize an object of type T to a JSON string.
        /// </summary>
        /// <typeparam name="T">The type of the object to be serialized.</typeparam>
        /// <typeparam name="R">The type of the resolver object.</typeparam>
        /// <param name="forSerialization">An instance of the type to serialize.</param>
        /// <param name="resolver">A class that is used for type resolution during serialization.</param>
        /// <returns>The serialization of the instance to a JSON string.</returns>
        public string Serialize<T, R>(T forSerialization, R resolver)
            where T : class, new()
            where R : class
        {
            if (forSerialization == null)
            {
                return string.Empty;
            }

            if (this.serializer != LqbGrammar.Drivers.JsonSerializer.JsonConvert)
            {
                throw new DeveloperException(ErrorMessage.SystemError);
            }

            if (this.options != JsonSerializerOptions.None)
            {
                throw new DeveloperException(ErrorMessage.SystemError);
            }

            // Q: Why use a typeparam when it is forced to be an instance of JsonConverter?
            // A: LQB has a class that inherits from this and passes it in.  Other code 
            //    might pass instead other sub-classes in the future.  We could just use 
            //    a method argument of type DefaultContractResolver but that would bind the 
            //    interface the Newtonsoft library.
            if (resolver is DefaultContractResolver)
            {
                JsonSerializerSettings settings = new JsonSerializerSettings();
                settings.ContractResolver = resolver as DefaultContractResolver;
                settings.StringEscapeHandling = StringEscapeHandling.EscapeHtml;
                return JsonConvert.SerializeObject(forSerialization, settings);
            }
            else
            {
                throw new DeveloperException(ErrorMessage.SystemError);
            }
        }

        /// <summary>
        /// Sanitizes the json blob directly before it gets passed on to whatever is going to use it.
        /// Will go through each token in the json blob and sanitize each string token. 
        /// </summary>
        /// <param name="json">The json to sanitize.</param>
        /// <returns>The sanitized json blob.</returns>
        public string SanitizeJsonString(string json)
        {
            var parsedJson = JToken.Parse(json);
            var serializer = new Newtonsoft.Json.JsonSerializer();

            using (var reader = new StringReader(json))
            {
                var jsonReader = new JsonTextReader(reader);
                while (jsonReader.Read())
                {
                    if (jsonReader.TokenType == JsonToken.String && jsonReader.ValueType == typeof(string))
                    {
                        string newValue = XmlDataUtil.SantizeXmlString(serializer.Deserialize<string>(jsonReader));
                        var value = parsedJson.SelectToken(jsonReader.Path) as JValue;
                        value.Replace(JToken.FromObject(newValue));
                    }
                }
            }

            return parsedJson.ToString();
        }

        /// <summary>
        /// Default serialization method.
        /// </summary>
        /// <typeparam name="T">The type of the object to be serialized.</typeparam>
        /// <param name="forSerialization">An instance of the type to serialize.</param>
        /// <returns>The serialization of the instance to a JSON string.</returns>
        private string HandleDefaultSerialize<T>(T forSerialization)
        {
            throw new DeveloperException(ErrorMessage.SystemError);
        }

        /// <summary>
        /// Serialize with the data contract serializer.
        /// </summary>
        /// <typeparam name="T">The type of the object to be serialized.</typeparam>
        /// <param name="forSerialization">An instance of the type to serialize.</param>
        /// <returns>The serialization of the instance to a JSON string.</returns>
        private string HandleDataContractSerialize<T>(T forSerialization)
        {
            if (this.options != JsonSerializerOptions.None)
            {
                throw new DeveloperException(ErrorMessage.SystemError);
            }

            DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(T));
            string ret = string.Empty;
            using (MemoryStream stream = new MemoryStream())
            {
                serializer.WriteObject(stream, forSerialization);
                ret = System.Text.Encoding.UTF8.GetString(stream.GetBuffer(), 0, (int)stream.Position);
            }

            return ret;
        }

        /// <summary>
        /// Serializer with the javascript serializer.
        /// </summary>
        /// <typeparam name="T">The type of the object to be serialized.</typeparam>
        /// <param name="forSerialization">An instance of the type to serialize.</param>
        /// <returns>The serialization of the instance to a JSON string.</returns>
        private string HandleJavascriptSerialize<T>(T forSerialization)
        {
            if (this.options != JsonSerializerOptions.None)
            {
                throw new DeveloperException(ErrorMessage.SystemError);
            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            serializer.MaxJsonLength = MaxJsonLength;
            return serializer.Serialize(forSerialization);
        }

        /// <summary>
        /// Serialize with the JsonConvert class.
        /// </summary>
        /// <typeparam name="T">The type of the object to be serialized.</typeparam>
        /// <param name="forSerialization">An instance of the type to serialize.</param>
        /// <returns>The serialization of the instance to a JSON string.</returns>
        private string HandlJsonConvertSerialize<T>(T forSerialization)
        {
            switch (this.options)
            {
                case JsonSerializerOptions.None:
                    {
                        return JsonConvert.SerializeObject(forSerialization);
                    }

                case JsonSerializerOptions.TypeNameHandling_All:
                    {
                        var settings = new JsonSerializerSettings() { TypeNameHandling = TypeNameHandling.All };
                        return JsonConvert.SerializeObject(forSerialization, settings);
                    }

                case JsonSerializerOptions.IndentedPlusStringEnumConverter:
                    {
                        return JsonConvert.SerializeObject(forSerialization, Formatting.Indented, new Newtonsoft.Json.Converters.StringEnumConverter());
                    }

                default:
                    throw new DeveloperException(ErrorMessage.SystemError);
            }
        }

        /// <summary>
        /// Deserializer using the default serializer.
        /// </summary>
        /// <typeparam name="T">The type of the object to be serialized.</typeparam>
        /// <param name="serialized">An instance of the type to serialize.</param>
        /// <returns>The serialization of the instance to a JSON string.</returns>
        private T HandleDefaultDeserialize<T>(string serialized)
        {
            throw new DeveloperException(ErrorMessage.SystemError);
        }

        /// <summary>
        /// Deserializer using the data contract serializer.
        /// </summary>
        /// <typeparam name="T">The type of the object to be serialized.</typeparam>
        /// <param name="serialized">An instance of the type to serialize.</param>
        /// <returns>The serialization of the instance to a JSON string.</returns>
        private T HandleDataContractDeserialize<T>(string serialized)
        {
            DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(T));
            using (MemoryStream stream = new MemoryStream(System.Text.Encoding.UTF8.GetBytes(serialized)))
            {
                return (T)serializer.ReadObject(stream);
            }
        }

        /// <summary>
        /// Deserializer using the javascript serializer.
        /// </summary>
        /// <typeparam name="T">The type of the object to be serialized.</typeparam>
        /// <param name="serialized">An instance of the type to serialize.</param>
        /// <returns>The serialization of the instance to a JSON string.</returns>
        private T HandleJavascriptDeserialize<T>(string serialized)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            return serializer.Deserialize<T>(serialized);
        }

        /// <summary>
        /// Deserializer using the JsonConvert serializer.
        /// </summary>
        /// <typeparam name="T">The type of the object to be serialized.</typeparam>
        /// <param name="serialized">An instance of the type to serialize.</param>
        /// <returns>The serialization of the instance to a JSON string.</returns>
        private T HandleJsonConvertDeserialize<T>(string serialized)
        {
            switch (this.options)
            {
                case JsonSerializerOptions.None:
                    {
                        return JsonConvert.DeserializeObject<T>(serialized);
                    }

                case JsonSerializerOptions.TypeNameHandling_All:
                    {
                        var settings = new Newtonsoft.Json.JsonSerializerSettings() { TypeNameHandling = Newtonsoft.Json.TypeNameHandling.All };
                        return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(serialized, settings);
                    }

                case JsonSerializerOptions.IndentedPlusStringEnumConverter:
                    {
                        return JsonConvert.DeserializeObject<T>(serialized);
                    }

                default:
                    throw new DeveloperException(ErrorMessage.SystemError);
            }
        }
    }
}
