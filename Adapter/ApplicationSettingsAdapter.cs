﻿namespace Adapter
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using LqbGrammar.Adapters;

    /// <summary>
    /// Class used to retrieve all the appliction settings in an application's configuration file.
    /// </summary>
    internal sealed class ApplicationSettingsAdapter : IApplicationSettingsAdapter
    {
        /// <summary>
        /// Retrieve the application settings.
        /// Note that the name and value of each setting is not encapsulated into a data type for
        /// verification.  We will defer that to the drivers to simplify the implementation
        /// since we are dealing with a collection class rather than individual values.
        /// </summary>
        /// <returns>A dictionary with the name/value pairs from the application settings.</returns>
        public Dictionary<string, string> ReadAllSettings()
        {
            var settings = new Dictionary<string, string>(ConfigurationManager.AppSettings.Keys.Count, StringComparer.OrdinalIgnoreCase);
            foreach (string key in ConfigurationManager.AppSettings.Keys)
            {
                settings.Add(key, ConfigurationManager.AppSettings[key]);
            }

            return settings;
        }
    }
}
