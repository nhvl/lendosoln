﻿namespace Adapter
{
    using System;
    using System.Text.RegularExpressions;
    using System.Xml.Linq;
    using LqbGrammar;
    using LqbGrammar.Adapters;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;
    using LqbGrammar.Drivers.HttpRequest;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Adapter that sends SMS messages using the Banana API.
    /// </summary>
    internal sealed class BananaSmsAdapter : ISmsAdapter
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BananaSmsAdapter"/> class.
        /// </summary>
        /// <param name="uri">The URL for the Banana Api web service.</param>
        /// <param name="source">The phone number that will be the source of the message.</param>
        /// <param name="securityToken">Security token required for Banana Api.</param>
        /// <param name="password">Encrypted password required for Banana Api.</param>
        public BananaSmsAdapter(LqbAbsoluteUri uri, PhoneNumber source, string securityToken, string password)
        {
            this.BananaURL = uri;
            this.SourceNumber = source;
            this.BananaSecurityToken = securityToken;
            this.BananaPassword = password;
        }

        /// <summary>
        /// Gets or sets the URL to the Banana API web service.
        /// </summary>
        /// <value>The URL to the Banana API web service.</value>
        private LqbAbsoluteUri BananaURL { get; set; }

        /// <summary>
        /// Gets or sets the password sent to the Banana API web service.
        /// </summary>
        /// <value>The password sent to the Banana API web service.</value>
        private string BananaPassword { get; set; }

        /// <summary>
        /// Gets or sets the security token sent to the Banana API web service.
        /// </summary>
        /// <value>The security token sent to the Banana API web service.</value>
        private string BananaSecurityToken { get; set; }

        /// <summary>
        /// Gets or sets the phone number that will be the source of the message.
        /// </summary>
        /// <value>The phone number that will be the source of the message.</value>
        private PhoneNumber SourceNumber { get; set; }

        /// <summary>
        /// Send an authentication code message via SMS.
        /// </summary>
        /// <param name="number">The phone number to which the SMS message will be sent.</param>
        /// <param name="message">A message containing an LQB authentication code.</param>
        /// <param name="monitor">Optional monitor that an adapter may use to get provider-specific details about the SMS communication.</param>
        /// <returns>True if OK response recieved, false otherwise.</returns>
        public bool SendAuthenticationCode(PhoneNumber number, SmsAuthCodeMessage message, ISmsMonitor monitor)
        {
            try
            {
                XDocument requestDoc = this.GenerateSmsRequestXml(number.ToString(), message.ToString());

                var requestXml = LqbXmlElement.Create(requestDoc.Root);
                if (requestXml == null)
                {
                    throw new DeveloperException(ErrorMessage.SystemError);
                }

                if (monitor != null)
                {
                    monitor.BeforeSend(requestDoc.ToString());
                }

                var factory = GenericLocator<IHttpRequestDriverFactory>.Factory;
                var driver = factory.Create();

                var options = new HttpRequestOptions();
                options.Method = HttpMethod.Post;
                options.PostData = new XmlContent(requestXml.Value);
                if (monitor == null)
                {
                    options.IgnoreResponse = true;
                }

                bool good = driver.ExecuteCommunication(this.BananaURL, options);
                if (good)
                {
                    if (monitor != null)
                    {
                        monitor.AfterSend(null, options.ResponseBody);
                    }
                }
                else
                {
                    if (monitor != null)
                    {
                        monitor.AfterSend(options.ResponseStatusCode.ToString(), options.ResponseBody);
                    }
                }

                return good;
            }
            catch (LqbException)
            {
                throw;
            }
            catch (SystemException ex)
            {
                if (monitor != null)
                {
                    monitor.OnException(ex);
                }

                return false;
            }
            catch (Exception ex)
            {
                throw new ServerException(ErrorMessage.SystemError, ex);
            }
        }

        /// <summary>
        /// Generates the request XML for SMS.
        /// </summary>
        /// <param name="phoneNumber">The phone number.</param>
        /// <param name="message">The message to send.</param>
        /// <returns>Xml document for the request.</returns>
        private XDocument GenerateSmsRequestXml(string phoneNumber, string message)
        {
            if (string.IsNullOrEmpty(phoneNumber) || string.IsNullOrEmpty(message))
            {
                throw new DeveloperException(ErrorMessage.SystemError);
            }

            var sourceNumber = this.SourceNumber.ToString();
            var phoneNumberToUse = this.ConstructPhoneNumberToSend(phoneNumber);

            XDocument request = new XDocument();
            XElement rootElement = new XElement(
                                    "REQUEST",
                                    new XAttribute("type", "SMS"),
                                    new XAttribute("phone_number", phoneNumberToUse),
                                    new XAttribute("source_number", sourceNumber));

            var token = this.BananaSecurityToken;

            var factory = GenericLocator<IEncryptionDriverFactory>.Factory;
            var driver = factory.Create();
            var password = driver.DecodeAndDecrypt(EncryptionKeyIdentifier.Default, this.BananaPassword);

            XElement userElement = new XElement(
                                    "USER",
                                    new XAttribute("token", token),
                                    new XAttribute("password", password));
            rootElement.Add(userElement);

            XElement templateElement = new XElement("TEMPLATE", message);
            rootElement.Add(templateElement);

            request.Add(rootElement);

            return request;
        }

        /// <summary>
        /// Constructs the phone number. The API requires the country code in order for it to send.
        /// </summary>
        /// <param name="phoneNumber">The phone number to send to.</param>
        /// <returns>The modified phone number.</returns>
        private string ConstructPhoneNumberToSend(string phoneNumber)
        {
            var phoneNumberModified = Regex.Replace(phoneNumber, "[^0-9]", string.Empty, RegexOptions.Compiled);

            // We need to prepend the international calling code to the phone number. For US numbers, this is 1.
            // We want to prepend it to phone numbers that don't start with 1 or for those that have 10 characters or less. (111)111-1111.
            if (!phoneNumberModified.StartsWith("1") || phoneNumberModified.Length <= 10)
            {
                var countryCode = "1";
                phoneNumberModified = $"{countryCode}{phoneNumberModified}";
            }

            return phoneNumberModified;
        }
    }
}
