﻿namespace Adapter
{
    using LqbGrammar.Adapters;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Factory for creating implementations of the IDataEncryptionAdapter interface.
    /// </summary>
    public sealed class DataEncryptionAdapterFactory : IDataEncryptionAdapterFactory
    {
        /// <summary>
        /// Create an instance of a class that implements the IDataEncryptionAdapter class.
        /// </summary>
        /// <returns>An implementation of IDataEncryptionAdapter.</returns>
        public IDataEncryptionAdapter Create()
        {
            return new DataEncryptionAdapter();
        }

        /// <summary>
        /// Create an encryption key from its component parts.
        /// </summary>
        /// <param name="version">The version number for the encryption key.</param>
        /// <param name="key">The encryption key.</param>
        /// <param name="iv">The optional initialization vector, or null.</param>
        /// <param name="blockSize">Optional parameter to override the default block size (128).</param>
        /// <returns>The initialized encryption key.</returns>
        public EncryptionKey CreateAesKey(int version, byte[] key, byte[] iv, int? blockSize)
        {
            return AesEncryptor.CreateKey(version, key, iv, blockSize);
        }

        /// <summary>
        /// Generates a new encryption key.
        /// </summary>
        /// <returns>An encryption key.</returns>
        public EncryptionKey GenerateKey()
        {
            return AesEncryptor.GenerateKey();
        }
    }
}