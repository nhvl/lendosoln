﻿namespace Adapter
{
    using System;
    using System.IO;
    using System.Messaging;
    using System.Text;
    using System.Xml.Linq;
    using LqbGrammar;
    using LqbGrammar.Adapters;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;

    /// <summary>
    /// An adapter that implements the IMessageQueueAdapter interface.
    /// </summary>
    internal class MessageQueueAdapter : IMessageQueueAdapter
    {
        /// <summary>
        /// Initialize a message queue for reading JSON messages.
        /// </summary>
        /// <param name="queue">The path to the message queue.</param>
        /// <returns>The message queue.</returns>
        public LqbMessageQueue PrepareSourceQueueForJSON(MessageQueuePath queue)
        {
            var theQueue = new MessageQueue(queue.ToString());
            theQueue.MessageReadPropertyFilter.ArrivedTime = true;
            return new LqbMessageQueue(theQueue);
        }

        /// <summary>
        /// Initialize a message queue for reading XML messages.
        /// </summary>
        /// <param name="queue">The path to the message queue.</param>
        /// <returns>The message queue.</returns>
        public LqbMessageQueue PrepareSourceQueueForXML(MessageQueuePath queue)
        {
            var theQueue = new MessageQueue(queue.ToString());
            theQueue.MessageReadPropertyFilter.ArrivedTime = true;
            theQueue.Formatter = new XDocumentMessageFormatter();
            return new LqbMessageQueue(theQueue);
        }

        /// <summary>
        /// Read a JSON message from the message queue and deserialize to the specified type, waiting for the timeout.
        /// </summary>
        /// <typeparam name="T">The type that expected for the JSON data.</typeparam>
        /// <param name="queue">The message queue.</param>
        /// <param name="timeout">The time to wait for a message prior to returning.  If null is passed in then the wait will be very short (1/10 millisecond).</param>
        /// <param name="arrivalTime">The time the message arrived in the queue.</param>
        /// <returns>A deserialized instance of the expected type, or null if there are no messages within the timeout waiting period.</returns>
        public T ReceiveJSON<T>(LqbMessageQueue queue, TimeoutInSeconds? timeout, out DateTime arrivalTime) where T : class, new()
        {
            arrivalTime = DateTime.MaxValue;

            var theTimeout = this.GetTimeout(timeout);
            var theQueue = queue.Value;

            try
            {
                using (var message = theQueue.Receive(theTimeout))
                {
                    arrivalTime = message.ArrivedTime;

                    long length = message.BodyStream.Length;
                    byte[] buffer = new byte[length];
                    message.BodyStream.Read(buffer, 0, Convert.ToInt32(length));

                    string json = System.Text.Encoding.UTF8.GetString(buffer);
                    var jsonAdapter = this.GetJsonAdapter();
                    return jsonAdapter.Deserialize<T>(json);
                }
            }
            catch (MessageQueueException ex)
            {
                if (ex.MessageQueueErrorCode == MessageQueueErrorCode.IOTimeout)
                {
                    return null;
                }
                else
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Read an XML message from the message queue, waiting for the timeout.
        /// </summary>
        /// <param name="queue">The message queue.</param>
        /// <param name="timeout">The time to wait for a message prior to returning.  If null is passed in then the wait will be very short (1/10 millisecond).</param>
        /// <param name="arrivalTime">The time the message arrived in the queue.</param>
        /// <returns>The received message, or null if there are no messages within the timeout waiting period.</returns>
        public LqbXmlElement? ReceiveXML(LqbMessageQueue queue, TimeoutInSeconds? timeout, out DateTime arrivalTime)
        {
            arrivalTime = DateTime.MaxValue;

            var theTimeout = this.GetTimeout(timeout);
            var theQueue = queue.Value;

            try
            {
                using (var message = theQueue.Receive(theTimeout))
                {
                    arrivalTime = message.ArrivedTime;
                    XDocument xdoc = message.Body as XDocument;
                    return LqbXmlElement.Create(xdoc.Root);
                }
            }
            catch (MessageQueueException ex)
            {
                if (ex.MessageQueueErrorCode == MessageQueueErrorCode.IOTimeout)
                {
                    return null;
                }
                else
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Send a JSON representation of an object to a message queue.
        /// </summary>
        /// <param name="queue">The path to the message queue.</param>
        /// <param name="label">An optional label to attach to the message.</param>
        /// <param name="obj">The object to send.</param>
        public void SendJSON(MessageQueuePath queue, MessageLabel? label, object obj)
        {
            var jsonAdapter = this.GetJsonAdapter();
            string json = jsonAdapter.Serialize(obj);

            using (var theQueue = new MessageQueue(queue.ToString()))
            {
                using (var ms = new Message())
                {
                    if (label != null)
                    {
                        ms.Label = label.Value.ToString();
                    }

                    using (var stream = new MemoryStream())
                    {
                        byte[] bytes = Encoding.UTF8.GetBytes(json);
                        stream.Write(bytes, 0, bytes.Length);
                        stream.Flush();
                        ms.BodyStream = stream;
                        theQueue.Send(ms);
                    }
                }
            }
        }

        /// <summary>
        /// Send XML to a message queue.
        /// </summary>
        /// <param name="queue">The path to the message queue.</param>
        /// <param name="label">An optional label to attach to the message.</param>
        /// <param name="xml">The XML to be sent.</param>
        public void SendXML(MessageQueuePath queue, MessageLabel? label, LqbXmlElement xml)
        {
            using (var theQueue = new MessageQueue(queue.ToString()))
            {
                using (var ms = new Message())
                {
                    if (label != null)
                    {
                        ms.Label = label.Value.ToString();
                    }

                    ms.Body = xml.Contained;
                    theQueue.Send(ms);
                }
            }
        }

        /// <summary>
        /// Convert the TimeoutInSeconds to a TimeSpan.
        /// </summary>
        /// <param name="timeout">The timeout value passed as a TimeoutInSeconds instance.</param>
        /// <returns>The timeout as a TimeSpan instance.</returns>
        private TimeSpan GetTimeout(TimeoutInSeconds? timeout)
        {
            if (timeout == null)
            {
                return new TimeSpan(1000);
            }
            else if (timeout.Value == TimeoutInSeconds.Sixty)
            {
                return new TimeSpan(0, 1, 0);
            }
            else
            {
                // other values are < a minute or >= a second
                return new TimeSpan(0, 0, timeout.Value.Value);
            }
        }

        /// <summary>
        /// Retrieve the appropriate Json adapter for dealing with message queue messages.
        /// </summary>
        /// <returns>A Json adapter instance.</returns>
        private IJsonAdapter GetJsonAdapter()
        {
            var factory = GenericLocator<IJsonAdapterFactory>.Factory;
            return factory.Create(JsonSerializer.JsonConvert, JsonSerializerOptions.None);
        }
    }
}
