﻿namespace Adapter.MethodInvoke
{
    using System.IO;
    using System.Linq.Expressions;

    /// <summary>
    /// Utility for inlining invocation expression, which is used by our method invoke adapters.
    /// </summary>
    public sealed class LinqExpressionAnalyzer : ExpressionVisitor
    {
        /// <summary>
        /// Track nested calls via the tab level.
        /// </summary>
        private int tabLevel = 0;

        /// <summary>
        /// Used to write details of the visitation.
        /// </summary>
        private StreamWriter writer;

        /// <summary>
        /// Cache the argument of the invoke.
        /// </summary>
        private Expression invokeArgument = null;

        /// <summary>
        /// Cache the expression of the invoke.
        /// </summary>
        private Expression invokeMember = null;

        /// <summary>
        /// Document the structure of the expression and write that to the file path.
        /// </summary>
        /// <param name="expression">The expression to document.</param>
        /// <param name="path">The path where the documented structure is written.</param>
        public static void DocumentExpression(Expression expression, string path)
        {
            using (StreamWriter writer = File.CreateText(path))
            {
                var analyzer = new LinqExpressionAnalyzer();
                analyzer.DocumentExpressionTree(expression, writer);
            }
        }

        /// <summary>
        /// Inline invocation expressions.
        /// </summary>
        /// <param name="expression">The expression to inline.</param>
        /// <param name="writer">Stream writer to record the structure, or null.</param>
        public void DocumentExpressionTree(Expression expression, StreamWriter writer)
        {
            this.writer = writer;

            this.LogMessage("ROOT");
            this.Visit(expression);
            this.LogMessage("DONE");
        }

        /// <summary>
        /// Identify the part of the expression that is of interest.
        /// </summary>
        /// <param name="e">Method call expression.</param>
        /// <returns>The result of visiting the method call expression.</returns>
        protected override Expression VisitBinary(BinaryExpression e)
        {
            ++this.tabLevel;
            this.LogMessage("BinaryExpression");
            var ret = base.VisitBinary(e);
            --this.tabLevel;
            return ret;
        }

        /// <summary>
        /// Identify the part of the expression that is of interest.
        /// </summary>
        /// <param name="e">Method call expression.</param>
        /// <returns>The result of visiting the method call expression.</returns>
        protected override Expression VisitBlock(BlockExpression e)
        {
            ++this.tabLevel;
            this.LogMessage("BlockExpression");
            var ret = base.VisitBlock(e);
            --this.tabLevel;
            return ret;
        }

        /// <summary>
        /// Identify the part of the expression that is of interest.
        /// </summary>
        /// <param name="e">Method call expression.</param>
        /// <returns>The result of visiting the method call expression.</returns>
        protected override CatchBlock VisitCatchBlock(CatchBlock e)
        {
            ++this.tabLevel;
            this.LogMessage("CatchBlock");
            var ret = base.VisitCatchBlock(e);
            --this.tabLevel;
            return ret;
        }

        /// <summary>
        /// Identify the part of the expression that is of interest.
        /// </summary>
        /// <param name="e">Method call expression.</param>
        /// <returns>The result of visiting the method call expression.</returns>
        protected override Expression VisitConditional(ConditionalExpression e)
        {
            ++this.tabLevel;
            this.LogMessage("ConditionalExpression");
            var ret = base.VisitConditional(e);
            --this.tabLevel;
            return ret;
        }

        /// <summary>
        /// Identify the part of the expression that is of interest.
        /// </summary>
        /// <param name="e">Method call expression.</param>
        /// <returns>The result of visiting the method call expression.</returns>
        protected override Expression VisitConstant(ConstantExpression e)
        {
            ++this.tabLevel;
            this.LogMessage("ConstantExpression: " + e.ToString());
            var ret = base.VisitConstant(e);
            --this.tabLevel;
            return ret;
        }

        /// <summary>
        /// Identify the part of the expression that is of interest.
        /// </summary>
        /// <param name="e">Method call expression.</param>
        /// <returns>The result of visiting the method call expression.</returns>
        protected override Expression VisitDebugInfo(DebugInfoExpression e)
        {
            ++this.tabLevel;
            this.LogMessage("DebugInfoExpression");
            var ret = base.VisitDebugInfo(e);
            --this.tabLevel;
            return ret;
        }

        /// <summary>
        /// Identify the part of the expression that is of interest.
        /// </summary>
        /// <param name="e">Method call expression.</param>
        /// <returns>The result of visiting the method call expression.</returns>
        protected override Expression VisitDefault(DefaultExpression e)
        {
            ++this.tabLevel;
            this.LogMessage("DefaultExpression");
            var ret = base.VisitDefault(e);
            --this.tabLevel;
            return ret;
        }

        /// <summary>
        /// Identify the part of the expression that is of interest.
        /// </summary>
        /// <param name="e">Method call expression.</param>
        /// <returns>The result of visiting the method call expression.</returns>
        protected override Expression VisitDynamic(DynamicExpression e)
        {
            ++this.tabLevel;
            this.LogMessage("DynamicExpression");
            var ret = base.VisitDynamic(e);
            --this.tabLevel;
            return ret;
        }

        /// <summary>
        /// Identify the part of the expression that is of interest.
        /// </summary>
        /// <param name="e">Method call expression.</param>
        /// <returns>The result of visiting the method call expression.</returns>
        protected override ElementInit VisitElementInit(ElementInit e)
        {
            ++this.tabLevel;
            this.LogMessage("ElementInit");
            var ret = base.VisitElementInit(e);
            --this.tabLevel;
            return ret;
        }

        /// <summary>
        /// Identify the part of the expression that is of interest.
        /// </summary>
        /// <param name="e">Method call expression.</param>
        /// <returns>The result of visiting the method call expression.</returns>
        protected override Expression VisitExtension(Expression e)
        {
            ++this.tabLevel;
            this.LogMessage("Expression (extension)");
            var ret = base.VisitExtension(e);
            --this.tabLevel;
            return ret;
        }

        /// <summary>
        /// Identify the part of the expression that is of interest.
        /// </summary>
        /// <param name="e">Method call expression.</param>
        /// <returns>The result of visiting the method call expression.</returns>
        protected override Expression VisitGoto(GotoExpression e)
        {
            ++this.tabLevel;
            this.LogMessage("GotoExpression");
            var ret = base.VisitGoto(e);
            --this.tabLevel;
            return ret;
        }

        /// <summary>
        /// Identify the part of the expression that is of interest.
        /// </summary>
        /// <param name="e">Method call expression.</param>
        /// <returns>The result of visiting the method call expression.</returns>
        protected override Expression VisitIndex(IndexExpression e)
        {
            ++this.tabLevel;
            this.LogMessage("IndexExpression");
            var ret = base.VisitIndex(e);
            --this.tabLevel;
            return ret;
        }

        /// <summary>
        /// Should drop the invocation wrapper around the desired expression.
        /// </summary>
        /// <param name="e">An expression that is an invocation.</param>
        /// <returns>The result of extracting the contained expression.</returns>
        protected override Expression VisitInvocation(InvocationExpression e)
        {
            ++this.tabLevel;
            this.invokeArgument = e.Arguments[0];
            this.invokeMember = e.Expression;
            this.LogMessage("InvocationExpression: " + e.ToString());
            var ret = base.VisitInvocation(e);
            this.invokeArgument = null;
            this.invokeMember = null;
            --this.tabLevel;
            return ret;
        }

        /// <summary>
        /// Identify the part of the expression that is of interest.
        /// </summary>
        /// <param name="e">Method call expression.</param>
        /// <returns>The result of visiting the method call expression.</returns>
        protected override Expression VisitLabel(LabelExpression e)
        {
            ++this.tabLevel;
            this.LogMessage("LabelExpression");
            var ret = base.VisitLabel(e);
            --this.tabLevel;
            return ret;
        }

        /// <summary>
        /// Identify the part of the expression that is of interest.
        /// </summary>
        /// <param name="e">Method call expression.</param>
        /// <returns>The result of visiting the method call expression.</returns>
        protected override LabelTarget VisitLabelTarget(LabelTarget e)
        {
            ++this.tabLevel;
            this.LogMessage("LabelTarget");
            var ret = base.VisitLabelTarget(e);
            --this.tabLevel;
            return ret;
        }

        /// <summary>
        /// Identify the part of the expression that is of interest.
        /// </summary>
        /// <typeparam name="T">Type parameter.</typeparam>
        /// <param name="e">Method call expression.</param>
        /// <returns>The result of visiting the method call expression.</returns>
        protected override Expression VisitLambda<T>(Expression<T> e)
        {
            ++this.tabLevel;
            this.LogMessage("Expression<T>: " + e.ToString());
            var ret = base.VisitLambda<T>(e);
            --this.tabLevel;
            return ret;
        }

        /// <summary>
        /// Identify the part of the expression that is of interest.
        /// </summary>
        /// <param name="e">Method call expression.</param>
        /// <returns>The result of visiting the method call expression.</returns>
        protected override Expression VisitListInit(ListInitExpression e)
        {
            ++this.tabLevel;
            this.LogMessage("ListInitExpression");
            var ret = base.VisitListInit(e);
            --this.tabLevel;
            return ret;
        }

        /// <summary>
        /// Identify the part of the expression that is of interest.
        /// </summary>
        /// <param name="e">Method call expression.</param>
        /// <returns>The result of visiting the method call expression.</returns>
        protected override Expression VisitLoop(LoopExpression e)
        {
            ++this.tabLevel;
            this.LogMessage("LoopExpression");
            var ret = base.VisitLoop(e);
            --this.tabLevel;
            return ret;
        }

        /// <summary>
        /// Identify the part of the expression that is of interest.
        /// </summary>
        /// <param name="e">Method call expression.</param>
        /// <returns>The result of visiting the method call expression.</returns>
        protected override Expression VisitMember(MemberExpression e)
        {
            string label = string.Empty;
            if (e == this.invokeArgument)
            {
                label = " (argument)";
            }
            else if (e == this.invokeMember)
            {
                label = " (expression)";
            }

            ++this.tabLevel;
            this.LogMessage("MemberExpression" + label + " : " + e.ToString());
            var ret = base.VisitMember(e);
            --this.tabLevel;
            return ret;
        }

        /// <summary>
        /// Identify the part of the expression that is of interest.
        /// </summary>
        /// <param name="e">Method call expression.</param>
        /// <returns>The result of visiting the method call expression.</returns>
        protected override MemberAssignment VisitMemberAssignment(MemberAssignment e)
        {
            ++this.tabLevel;
            this.LogMessage("MemberAssignment");
            var ret = base.VisitMemberAssignment(e);
            --this.tabLevel;
            return ret;
        }

        /// <summary>
        /// Identify the part of the expression that is of interest.
        /// </summary>
        /// <param name="e">Method call expression.</param>
        /// <returns>The result of visiting the method call expression.</returns>
        protected override MemberBinding VisitMemberBinding(MemberBinding e)
        {
            ++this.tabLevel;
            this.LogMessage("MemberBinding");
            var ret = base.VisitMemberBinding(e);
            --this.tabLevel;
            return ret;
        }

        /// <summary>
        /// Identify the part of the expression that is of interest.
        /// </summary>
        /// <param name="e">Method call expression.</param>
        /// <returns>The result of visiting the method call expression.</returns>
        protected override Expression VisitMemberInit(MemberInitExpression e)
        {
            ++this.tabLevel;
            this.LogMessage("MemberInitExpression");
            var ret = base.VisitMemberInit(e);
            --this.tabLevel;
            return ret;
        }

        /// <summary>
        /// Identify the part of the expression that is of interest.
        /// </summary>
        /// <param name="e">Method call expression.</param>
        /// <returns>The result of visiting the method call expression.</returns>
        protected override MemberListBinding VisitMemberListBinding(MemberListBinding e)
        {
            ++this.tabLevel;
            this.LogMessage("MemberListBinding");
            var ret = base.VisitMemberListBinding(e);
            --this.tabLevel;
            return ret;
        }

        /// <summary>
        /// Identify the part of the expression that is of interest.
        /// </summary>
        /// <param name="e">Method call expression.</param>
        /// <returns>The result of visiting the method call expression.</returns>
        protected override MemberMemberBinding VisitMemberMemberBinding(MemberMemberBinding e)
        {
            ++this.tabLevel;
            this.LogMessage("MemberMemberBinding");
            var ret = base.VisitMemberMemberBinding(e);
            --this.tabLevel;
            return ret;
        }

        /// <summary>
        /// Identify the part of the expression that is of interest.
        /// </summary>
        /// <param name="e">Method call expression.</param>
        /// <returns>The result of visiting the method call expression.</returns>
        protected override Expression VisitMethodCall(MethodCallExpression e)
        {
            ++this.tabLevel;
            this.LogMessage("MethodCallExpression");
            var ret = base.VisitMethodCall(e);
            --this.tabLevel;
            return ret;
        }

        /// <summary>
        /// Identify the part of the expression that is of interest.
        /// </summary>
        /// <param name="e">Method call expression.</param>
        /// <returns>The result of visiting the method call expression.</returns>
        protected override Expression VisitNew(NewExpression e)
        {
            ++this.tabLevel;
            this.LogMessage("NewExpression");
            var ret = base.VisitNew(e);
            --this.tabLevel;
            return ret;
        }

        /// <summary>
        /// Identify the part of the expression that is of interest.
        /// </summary>
        /// <param name="e">Method call expression.</param>
        /// <returns>The result of visiting the method call expression.</returns>
        protected override Expression VisitNewArray(NewArrayExpression e)
        {
            ++this.tabLevel;
            this.LogMessage("NewArrayExpression");
            var ret = base.VisitNewArray(e);
            --this.tabLevel;
            return ret;
        }

        /// <summary>
        /// Identify the part of the expression that is of interest.
        /// </summary>
        /// <param name="e">Method call expression.</param>
        /// <returns>The result of visiting the method call expression.</returns>
        protected override Expression VisitParameter(ParameterExpression e)
        {
            ++this.tabLevel;
            this.LogMessage("ParameterExpression");
            var ret = base.VisitParameter(e);
            --this.tabLevel;
            return ret;
        }

        /// <summary>
        /// Identify the part of the expression that is of interest.
        /// </summary>
        /// <param name="e">Method call expression.</param>
        /// <returns>The result of visiting the method call expression.</returns>
        protected override Expression VisitRuntimeVariables(RuntimeVariablesExpression e)
        {
            ++this.tabLevel;
            this.LogMessage("RuntimeVariablesExpression");
            var ret = base.VisitRuntimeVariables(e);
            --this.tabLevel;
            return ret;
        }

        /// <summary>
        /// Identify the part of the expression that is of interest.
        /// </summary>
        /// <param name="e">Method call expression.</param>
        /// <returns>The result of visiting the method call expression.</returns>
        protected override Expression VisitSwitch(SwitchExpression e)
        {
            ++this.tabLevel;
            this.LogMessage("SwitchExpression");
            var ret = base.VisitSwitch(e);
            --this.tabLevel;
            return ret;
        }

        /// <summary>
        /// Identify the part of the expression that is of interest.
        /// </summary>
        /// <param name="e">Method call expression.</param>
        /// <returns>The result of visiting the method call expression.</returns>
        protected override SwitchCase VisitSwitchCase(SwitchCase e)
        {
            ++this.tabLevel;
            this.LogMessage("SwitchCase");
            var ret = base.VisitSwitchCase(e);
            --this.tabLevel;
            return ret;
        }

        /// <summary>
        /// Identify the part of the expression that is of interest.
        /// </summary>
        /// <param name="e">Method call expression.</param>
        /// <returns>The result of visiting the method call expression.</returns>
        protected override Expression VisitTry(TryExpression e)
        {
            ++this.tabLevel;
            this.LogMessage("TryExpression");
            var ret = base.VisitTry(e);
            --this.tabLevel;
            return ret;
        }

        /// <summary>
        /// Identify the part of the expression that is of interest.
        /// </summary>
        /// <param name="e">Method call expression.</param>
        /// <returns>The result of visiting the method call expression.</returns>
        protected override Expression VisitTypeBinary(TypeBinaryExpression e)
        {
            ++this.tabLevel;
            this.LogMessage("TypeBinaryExpression");
            var ret = base.VisitTypeBinary(e);
            --this.tabLevel;
            return ret;
        }

        /// <summary>
        /// Identify the part of the expression that is of interest.
        /// </summary>
        /// <param name="e">Method call expression.</param>
        /// <returns>The result of visiting the method call expression.</returns>
        protected override Expression VisitUnary(UnaryExpression e)
        {
            ++this.tabLevel;
            this.LogMessage("UnaryExpression");
            var ret = base.VisitUnary(e);
            --this.tabLevel;
            return ret;
        }

        /// <summary>
        /// Log a message to the stream writer.
        /// </summary>
        /// <param name="message">Message to log.</param>
        private void LogMessage(string message)
        {
            if (this.writer != null)
            {
                this.writer.WriteLine(new string('\t', this.tabLevel) + message);
            }
        }
    }
}
