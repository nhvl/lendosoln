﻿namespace Adapter.MethodInvoke
{
    using LqbGrammar.Adapters;

    /// <summary>
    /// Factory for creation of a DirectMethodCallAdapter implemenation of the IMethodInvokeAdapter interface.
    /// </summary>
    public sealed class DirectMethodCallAdapterFactory : IMethodInvokeAdapterFactory
    {
        /// <summary>
        /// Create an implementation of the IMethodInvokeAdapter interface.
        /// </summary>
        /// <returns>An implementation of the IMethodInvokeAdapter interface.</returns>
        public IMethodInvokeAdapter Create()
        {
            return new DirectMethodCallAdapter();
        }
    }
}
