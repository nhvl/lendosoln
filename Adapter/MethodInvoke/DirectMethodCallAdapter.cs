﻿namespace Adapter.MethodInvoke
{
    using System;
    using LqbGrammar.Adapters;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Simplest implementatation of the IMethodInvokeAdapter is to just call the method.
    /// </summary>
    internal sealed class DirectMethodCallAdapter : IMethodInvokeAdapter
    {
        /// <summary>
        /// Gets a key that a driver can use to retrieve the control information.  If null then no control information will be set.
        /// </summary>
        /// <value>A key that a driver can use to retrieve the control information.</value>
        public string ControlInformationKey
        {
            get
            {
                return null;
            }
        }

        /// <summary>
        /// Determine whether or not the call will be made directly or done on a different thread.
        /// This will allow the client code to make efficient calls when a direct call is made.
        /// </summary>
        /// <returns>True if the method will be called using a different thread, rather than directly.</returns>
        public bool InvokeIsOffThread()
        {
            return false;
        }

        /// <summary>
        /// Different adapters may require additional data to route the method calls, or other configuration information.
        /// </summary>
        /// <param name="control">The parameter is not used.</param>
        public void SetControlInformation(string control)
        {
            // not needed
        }

        /// <summary>
        /// Execute a method that accepts an argument of type S and returns void.
        /// </summary>
        /// <typeparam name="T">Type of the class that contains the method.</typeparam>
        /// <typeparam name="S">Type of the argument to the method.</typeparam>
        /// <param name="call">The method that will invoked.</param>
        /// <param name="previousCallId">The parameter is not used.</param>
        /// <returns>Always null.</returns>
        public MethodInvokeIdentifier Run<T, S>(SingleArgumentMethod<T, S> call, MethodInvokeIdentifier previousCallId) where T : new()
        {
            try
            {
                SingleArgumentMethod<T, S>.Execute(call);
                return null;
            }
            catch (LqbException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new DeveloperException(ErrorMessage.SystemError, ex);
            }
        }

        /// <summary>
        /// After waiting for a delay time, execute a method that accepts an argument of type S and returns void.
        /// </summary>
        /// <typeparam name="T">Type of the class that contains the method.</typeparam>
        /// <typeparam name="S">Type of the argument to the method.</typeparam>
        /// <param name="call">The method that will get invoked.</param>
        /// <param name="delay">The delay time to wait prior to executing the method.</param>
        /// <returns>A job identifier, or null if there is no identifier.</returns>
        public MethodInvokeIdentifier RunAfterDelay<T, S>(SingleArgumentMethod<T, S> call, TimeoutInSeconds delay) where T : new()
        {
            try
            {
                System.Threading.Thread.Sleep(delay.Value * 1000);
                SingleArgumentMethod<T, S>.Execute(call);
                return null;
            }
            catch (LqbException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new DeveloperException(ErrorMessage.SystemError, ex);
            }
        }
    }
}
