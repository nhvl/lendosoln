﻿namespace Adapter
{
    using System;
    using LqbGrammar.Adapters;

    /// <summary>
    /// Factory to create instances of RegularExpressionAdapter.
    /// </summary>
    public sealed class RegularExpressionAdapterFactory : IRegularExpressionAdapterFactory
    {
        /// <summary>
        /// Create an instance of RegularExpressionAdapter.
        /// </summary>
        /// <returns>An instance of RegularExpressionAdapter.</returns>
        public IRegularExpressionAdapter Create()
        {
            return new RegularExpressionAdapter();
        }
    }
}
