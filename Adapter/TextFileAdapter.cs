﻿namespace Adapter
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;
    using LqbGrammar.Adapters;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers.FileSystem;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Implementation of the ITextFileAdapter interface.
    /// </summary>
    internal sealed class TextFileAdapter : ITextFileAdapter
    {
        /// <summary>
        /// Opens the file, appends the string to the file, and then closes the file.
        /// If the file does not exist, this method creates a file, writes the specified
        /// string to the file, then closes the file.
        /// </summary>
        /// <param name="path">The file to which the text is appended.</param>
        /// <param name="text">The text that is appended to the file.</param>
        public void AppendString(LocalFilePath path, string text)
        {
            try
            {
                File.AppendAllText(path.Value, text);
            }
            catch (Exception ex)
            {
                throw new DeveloperException(ErrorMessage.SystemError, ex);
            }
        }

        /// <summary>
        /// Open an existing text file in append mode, or create a new file if it
        /// does not exist, for writing.
        /// </summary>
        /// <param name="path">The file to which text can be appended.</param>
        /// <param name="writeHandler">Delegate that handles writing text to the file.</param>
        public void OpenForAppend(LocalFilePath path, Action<LqbTextFileWriter> writeHandler)
        {
            try
            {
                using (StreamWriter writer = File.AppendText(path.Value))
                {
                    writeHandler(new LqbTextFileWriter(writer));
                }
            }
            catch (LqbException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new DeveloperException(ErrorMessage.SystemError, ex);
            }
        }

        /// <summary>
        /// Create a new text file if it doesn't exist, or overwrite an existing text file.
        /// </summary>
        /// <param name="path">The file to which text can be written.</param>
        /// <param name="writeHandler">Delegate that handles writing text to the file.</param>
        public void OpenNew(LocalFilePath path, Action<LqbTextFileWriter> writeHandler)
        {
            try
            {
                using (StreamWriter writer = File.CreateText(path.Value))
                {
                    writeHandler(new LqbTextFileWriter(writer));
                }
            }
            catch (LqbException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new DeveloperException(ErrorMessage.SystemError, ex);
            }
        }

        /// <summary>
        /// Open a text file for reading.
        /// </summary>
        /// <param name="path">The file that is available for reading.</param>
        /// <param name="readHandler">Delegate the reads the file.</param>
        public void OpenRead(LocalFilePath path, Action<LqbTextFileReader> readHandler)
        {
            try
            {
                if (!File.Exists(path.Value))
                {
                    throw new DeveloperException(ErrorMessage.SystemError);
                }

                using (StreamReader reader = File.OpenText(path.Value))
                {
                    readHandler(new LqbTextFileReader(reader));
                }
            }
            catch (LqbException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new DeveloperException(ErrorMessage.SystemError, ex);
            }
        }

        /// <summary>
        /// Open, read all the contents, then close a text file.
        /// </summary>
        /// <param name="path">The file that is to be read.</param>
        /// <returns>The file contents.</returns>
        public string ReadFile(LocalFilePath path)
        {
            try
            {
                if (!File.Exists(path.Value))
                {
                    throw new DeveloperException(ErrorMessage.SystemError);
                }

                return File.ReadAllText(path.Value);
            }
            catch (LqbException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new DeveloperException(ErrorMessage.SystemError, ex);
            }
        }

        /// <summary>
        /// Open, read all contents as a set of lines, then close a text file.
        /// Note that this method yields lines before they are all in memory
        /// so it is efficient for large files.
        /// </summary>
        /// <param name="path">The file that is to be read.</param>
        /// <returns>The file contents as a set of lines.</returns>
        public IEnumerable<string> ReadLines(LocalFilePath path)
        {
            try
            {
                if (!File.Exists(path.Value))
                {
                    throw new DeveloperException(ErrorMessage.SystemError);
                }

                return File.ReadLines(path.Value);
            }
            catch (LqbException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new DeveloperException(ErrorMessage.SystemError, ex);
            }
        }

        /// <summary>
        /// Creates a new text file if it doesn't exist, or overwrite an existing file,
        /// with the specified text and then closes the file.
        /// </summary>
        /// <param name="path">The file to which the text is written.</param>
        /// <param name="text">The text that is written to the file.</param>
        /// <param name="writeBOM">True if the a byte order mark should be included at the beginning of the file.</param>
        public void WriteString(LocalFilePath path, string text, bool writeBOM)
        {
            try
            {
                if (writeBOM)
                {
                    var encoding = new UTF8Encoding(true);
                    File.WriteAllText(path.Value, text, encoding);
                }
                else
                {
                    File.WriteAllText(path.Value, text);
                }
            }
            catch (Exception ex)
            {
                throw new DeveloperException(ErrorMessage.SystemError, ex);
            }
        }
    }
}
