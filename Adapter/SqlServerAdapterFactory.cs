﻿namespace Adapter
{
    using System.Data.Common;
    using LqbGrammar.Adapters;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Factory for creating instances of SqlServerAdapter.
    /// </summary>
    public sealed class SqlServerAdapterFactory : ISqlAdapterFactory
    {
        /// <summary>
        /// Create and return in instance of SqlServerAdapter.
        /// </summary>
        /// <param name="factory">The database provider factory.</param>
        /// <param name="timeout">The timeout to use when communicating with the database.</param>
        /// <returns>The interface implemented by SqlServerAdapter.</returns>
        public ISqlAdapter Create(DbProviderFactory factory, TimeoutInSeconds timeout)
        {
            return new SqlServerAdapter(factory, timeout);
        }
    }
}
