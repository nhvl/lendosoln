﻿namespace Adapter
{
    using System;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Threading;

    /// <summary>
    /// A list of extensions method for DbConnection class.
    /// </summary>
    public static class DbConnectionExtensions
    {
        /// <summary>
        /// Open a connection. If there is an intermittent network issue then it will retry 3 times before throw exception.
        /// </summary>
        /// <param name="conn">A connection to open.</param>
        public static void OpenWithRetry(this IDbConnection conn)
        {
            int maxRetries = 2;
            int currentRetry = 0;
            while (true)
            {
                try
                {
                    conn.Open();
                    return;
                }
                catch (DbException exc)
                {
                    if (currentRetry < maxRetries && IsAllowRetry(exc))
                    {
                        ////SleepExponentialBackoff(currentRetry);
                        SleepSmall(currentRetry);
                    }
                    else
                    {
                        if (exc.Data != null)
                        {
                            exc.Data["DbName"] = conn.Database; // Set information about the database that connection has trouble connect.
                        }

                        throw;
                    }

                    currentRetry++;
                }
            }
        }

        /// <summary>
        /// Test to see if the exception can be retry.
        /// </summary>
        /// <param name="exc">Exception to test.</param>
        /// <returns>Whether the exception can be retry.</returns>
        private static bool IsAllowRetry(DbException exc)
        {
            SqlException sqlException = exc as SqlException;

            if (sqlException != null)
            {
                if (sqlException.Number == 53)
                {
                    // dd 5/18/2018 - For this error message bellow we allow retry in our system.
                    // An error has occurred while establishing a connection to the server.When connecting to SQL Server, 
                    // this failure may be caused by the fact that under the default settings SQL Server does not allow remote connections. 
                    // (provider: Named Pipes Provider, error: 40 - Could not open a connection to SQL Server)(.Net SqlClient Data Provider).
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Sleeps for a small, random amount of time.
        /// 1st time between .3 and 1 second.
        /// 2nd time between 1 and 2 seconds.
        /// </summary>
        /// <param name="retry">The retry count.</param>
        private static void SleepSmall(int retry)
        {
            double sleepTimeInSeconds;
            Random random = new Random(Guid.NewGuid().GetHashCode());
            switch (retry)
            {
                case 0:
                    sleepTimeInSeconds = (double)random.Next(300, 1000) / 1000.0;
                    break;
                case 1:
                    sleepTimeInSeconds = (double)random.Next(1000, 2000) / 1000.0;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            Thread.Sleep(TimeSpan.FromSeconds(sleepTimeInSeconds));
        }

        /// <summary>
        /// Perform sleep with random time using exponential backoff algorithm.
        /// First Retry - Random between 1.5 - 3 seconds
        /// Second Retry - (Random between 1.5 - 3 seconds) ^ 2.
        /// </summary>
        /// <param name="retry">Number of retries to generate the sleep time.</param>
        private static void SleepExponentialBackoff(int retry)
        {
            if (retry < 0 || retry > 5)
            {
                throw new ArgumentOutOfRangeException();
            }

            // dd 5/18/2018 - Why not using new Random()?
            //    The reason is when the exception occurred it tend to occur
            //    within same seconde. Since new Random() is time-dependent therefore
            //    all new Random() will yield same result.
            Random random = new Random(Guid.NewGuid().GetHashCode());
            double waitBetweenRetryInSeconds = (double)random.Next(300, 1000) / 1000.0;
            double sleepTimeInSeconds = Math.Pow(waitBetweenRetryInSeconds, retry + 1);
            Thread.Sleep(TimeSpan.FromSeconds(sleepTimeInSeconds));
        }
    }
}