﻿namespace Adapter
{
    using System.Data.Common;
    using LqbGrammar.Adapters;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Factory for creating instances of StoredProcedureAdapter.
    /// </summary>
    public class StoredProcedureAdapterFactory : IStoredProcedureAdapterFactory
    {
        /// <summary>
        /// Create and return in instance of StoredProcedureAdapter.
        /// </summary>
        /// <param name="factory">The database provider factory.</param>
        /// <param name="timeout">The timeout to use when communicating with the database.</param>
        /// <returns>The interface implemented by StoredProcedureAdapter.</returns>
        public IStoredProcedureAdapter Create(DbProviderFactory factory, TimeoutInSeconds timeout)
        {
            return new StoredProcedureAdapter(factory, timeout);
        }
    }
}
