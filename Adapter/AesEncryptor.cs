﻿namespace Adapter
{
    using System;
    using System.IO;
    using System.Security.Cryptography;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Utility class for working with the AES encryption algorithm.
    /// </summary>
    internal static class AesEncryptor
    {
        /// <summary>
        /// Encrypt the input data with the input encryption key.
        /// </summary>
        /// <param name="key">The encryption key.</param>
        /// <param name="data">The data to encrypt.</param>
        /// <returns>The encrypted data.</returns>
        public static byte[] Encrypt(EncryptionKey key, byte[] data)
        {
            var aesKey = ConvertKey(key);

            using (var algorithm = GetCrypto(aesKey))
            {
                using (var encryptor = algorithm.CreateEncryptor())
                {
                    using (var memoryStream = new MemoryStream())
                    {
                        using (var cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
                        {
                            cryptoStream.Write(data, 0, data.Length);
                            cryptoStream.FlushFinalBlock();
                            cryptoStream.Flush();

                            return memoryStream.ToArray();
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Decrypt the input with the encryption key.
        /// </summary>
        /// <param name="key">The encryption key.</param>
        /// <param name="encrypted">The encrypted data.</param>
        /// <returns>The decrypted data.</returns>
        public static byte[] Decrypt(EncryptionKey key, byte[] encrypted)
        {
            var aesKey = ConvertKey(key);

            using (var algorithm = GetCrypto(aesKey))
            {
                using (var encryptor = algorithm.CreateDecryptor())
                {
                    using (var memoryStream = new MemoryStream(encrypted))
                    {
                        using (var cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Read))
                        {
                            using (var resStream = new MemoryStream())
                            {
                                cryptoStream.CopyTo(resStream);
                                return resStream.ToArray();
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Create an encryption key from its component parts.
        /// </summary>
        /// <param name="version">The version number for the encryption key.</param>
        /// <param name="key">The encryption key.</param>
        /// <param name="iv">The optional initialization vector, or null.</param>
        /// <param name="blockSize">Optional parameter to override the default block size (128).</param>
        /// <returns>The initialized encryption key.</returns>
        public static EncryptionKey CreateKey(int version, byte[] key, byte[] iv, int? blockSize)
        {
            return new RijndaelKey(version, key, iv, blockSize);
        }

        /// <summary>
        /// Generates a new encryption key.
        /// </summary>
        /// <returns>An encryption key.</returns>
        public static EncryptionKey GenerateKey()
        {
            using (var algorithm = new RijndaelManaged())
            {
                algorithm.GenerateIV();
                algorithm.GenerateKey();

                return CreateKey(201704, algorithm.Key, algorithm.IV, algorithm.BlockSize);
            }
        }

        /// <summary>
        /// Downcast the input encryption key to the AES specific key type.
        /// </summary>
        /// <param name="key">The input encryption key.</param>
        /// <returns>The AES specific key.</returns>
        private static RijndaelKey ConvertKey(EncryptionKey key)
        {
            var aesKey = key as RijndaelKey;
            if (aesKey == null)
            {
                throw new DeveloperException(ErrorMessage.SystemError);
            }

            return aesKey;
        }

        /// <summary>
        /// Create the encryptor with the encryption key data.
        /// </summary>
        /// <param name="key">The encryption key data.</param>
        /// <returns>The initialized encryptor.</returns>
        private static SymmetricAlgorithm GetCrypto(RijndaelKey key)
        {
            SymmetricAlgorithm algorithm = new RijndaelManaged();
            if (key.BlockSize != null)
            {
                algorithm.BlockSize = key.BlockSize.Value;
            }

            algorithm.Key = key.Key;
            algorithm.IV = key.IV;

            return algorithm;
        }

        /// <summary>
        /// Encryption key used for AES encryption.
        /// </summary>
        private class RijndaelKey : EncryptionKey
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="RijndaelKey"/> class.
            /// </summary>
            /// <param name="version">The version number for the encryption key.</param>
            /// <param name="key">The encryption key.</param>
            /// <param name="iv">The optional initialization vector, or null.</param>
            /// <param name="blockSize">Optional parameter to override the default block size (128).</param>
            public RijndaelKey(int version, byte[] key, byte[] iv, int? blockSize)
            {
                Validate(version, key, iv);

                this.Version = version;
                this.Key = key;
                this.IV = iv;
                this.BlockSize = blockSize;
            }

            /// <summary>
            /// Gets the version number for the encryption key.
            /// </summary>
            /// <value>The version number for the encryption key.</value>
            public int Version { get; private set; }

            /// <summary>
            /// Gets the encryption key value.
            /// </summary>
            /// <value>The encryption key value.</value>
            public byte[] Key { get; private set; }

            /// <summary>
            /// Gets the optional initialization vector.
            /// </summary>
            /// <value>The optional initialization vector.</value>
            public byte[] IV { get; private set; }

            /// <summary>
            /// Gets the optional override for the block size.
            /// </summary>
            /// <value>The optional override for the block size.</value>
            public int? BlockSize { get; private set; }

            /// <summary>
            /// Use the appropriate encryption algorithm to handle an encrypt/decrypt operation.
            /// </summary>
            /// <param name="decrypt">True if decryption is desired, false if encryption is desired.</param>
            /// <param name="data">The data to encrypt/decrypt.</param>
            /// <returns>The decrypted/encrypted data.</returns>
            public override byte[] InvokeEncryption(bool decrypt, byte[] data)
            {
                return decrypt ? AesEncryptor.Decrypt(this, data) : AesEncryptor.Encrypt(this, data);
            }

            /// <summary>
            /// Gets the backing fields required to persist the encryption key.
            /// </summary>
            /// <returns>The backing fields required to persist the encryption key.</returns>
            public override EncryptionKeyBackingFields GetBackingFields()
            {
                return new EncryptionKeyBackingFields()
                {
                    Version = this.Version,
                    Field1 = this.Key,
                    Field2 = this.IV
                };
            }

            /// <summary>
            /// Validate the input data.
            /// </summary>
            /// <param name="version">The version number for the encryption key.</param>
            /// <param name="key">The encryption key.</param>
            /// <param name="iv">The optional initialization vector, or null.</param>
            private static void Validate(int version, byte[] key, byte[] iv)
            {
                if (version >= 201704)
                {
                    Validate_201704(key, iv);
                }
                else if (version == 201702)
                {
                    Validate_201702(key, iv);
                }
                else if (version == 201701)
                {
                    Validate_201701(key, iv);
                }
                else
                {
                    throw new DeveloperException(ErrorMessage.SystemError);
                }
            }

            /// <summary>
            /// Validate the input data.
            /// </summary>
            /// <param name="key">The encryption key.</param>
            /// <param name="iv">The optional initialization vector, or null.</param>
            private static void Validate_201701(byte[] key, byte[] iv)
            {
                if (key.Length != 32)
                {
                    throw new DeveloperException(ErrorMessage.SystemError);
                }

                if (iv != null && iv.Length != 32)
                {
                    throw new DeveloperException(ErrorMessage.SystemError);
                }
            }

            /// <summary>
            /// Validate the input data.
            /// </summary>
            /// <param name="key">The encryption key.</param>
            /// <param name="iv">The optional initialization vector, or null.</param>
            private static void Validate_201702(byte[] key, byte[] iv)
            {
                if (key.Length != 24)
                {
                    throw new DeveloperException(ErrorMessage.SystemError);
                }

                if (iv != null && iv.Length != 16)
                {
                    throw new DeveloperException(ErrorMessage.SystemError);
                }
            }

            /// <summary>
            /// Validate the input data.
            /// </summary>
            /// <param name="key">The encryption key.</param>
            /// <param name="iv">The optional initialization vector, or null.</param>
            private static void Validate_201704(byte[] key, byte[] iv)
            {
                if (key.Length != 32)
                {
                    throw new DeveloperException(ErrorMessage.SystemError);
                }

                if (iv != null && iv.Length != 16)
                {
                    throw new DeveloperException(ErrorMessage.SystemError);
                }
            }
        }
    }
}
