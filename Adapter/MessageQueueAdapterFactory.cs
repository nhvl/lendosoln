﻿namespace Adapter
{
    using System;
    using LqbGrammar.Adapters;

    /// <summary>
    /// Factory for creating message queue adapters.
    /// </summary>
    public class MessageQueueAdapterFactory : IMessageQueueAdapterFactory
    {
        /// <summary>
        /// Create a message queue adapter.
        /// </summary>
        /// <returns>A message queue adapter.</returns>
        public IMessageQueueAdapter Create()
        {
            return new MessageQueueAdapter();
        }
    }
}
