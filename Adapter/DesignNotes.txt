﻿Implementations of ILqbAdapter will be located within this module.

The interfaces themselves will be defined in the LqbGrammar module.  Those
interfaces will participate in the LQB grammar, i.e., the interface
method parameters will consist of semantic data types.  The implementations
of those interfaces must reduce these down to the data types relevant to
each particular library that is being wrapped.

Also, the interfaces will be designed for stability.  They won't be simple
one-to-one translations of the 3rd party APIs.  The goal is to permit the
swap of 3rd party libraries without changing the interfaces, thus leaving
the business logic untouched.  This will result in non-trivial
implementations.  There is a talk on good design by Josh Bloch (one of
the creators of Java) where he says that to get a generic API correct you
need to successfully wrap three independent libraries that provide the
same functionality.  We won't have that luxury, but that is the target that
we want to keep in mind.

[
talk reference:
http://www.youtube.com/watch?v=aAb7hSCtvGw&feature=channel

and for discussions of the points raised there:
http://skeletonschema.info/Blog/GoodDesignTalk.php
]
