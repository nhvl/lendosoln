﻿namespace Adapter
{
    using System;
    using LqbGrammar.Adapters;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Adapter that wraps the static System.Environment class.
    /// </summary>
    internal sealed class EnvironmentAdapter : IEnvironmentAdapter
    {
        /// <summary>
        /// Gets the current working directory of the application in which the calling code is running.
        /// </summary>
        /// <value>The current working directory of the application in which the calling code is running.</value>
        public string CurrentDirectory
        {
            get
            {
                try
                {
                    return Environment.CurrentDirectory;
                }
                catch (LqbException)
                {
                    throw;
                }
                catch (Exception ex)
                {
                    throw new ServerException(ErrorMessage.SystemError, ex);
                }
            }
        }

        /// <summary>
        /// Gets the name of the machine on which the calling code is running.
        /// </summary>
        /// <value>The name of the machine on which the calling code is running.</value>
        public MachineName MachineName
        {
            get
            {
                try
                {
                    string name = Environment.MachineName;
                    var ret = MachineName.Create(name);
                    if (ret == null)
                    {
                        throw new ServerException(ErrorMessage.SystemError);
                    }

                    return ret.Value;
                }
                catch (LqbException)
                {
                    throw;
                }
                catch (Exception ex)
                {
                    throw new ServerException(ErrorMessage.SystemError, ex);
                }
            }
        }

        /// <summary>
        /// Gets the preferred newline string for the current environment.
        /// </summary>
        /// <value>The preferred newline string for the current environment.</value>
        public string NewLine
        {
            get
            {
                try
                {
                    return Environment.NewLine;
                }
                catch (LqbException)
                {
                    throw;
                }
                catch (Exception ex)
                {
                    throw new ServerException(ErrorMessage.SystemError, ex);
                }
            }
        }

        /// <summary>
        /// Gets the number of processors hosted by the machine on which the calling code is running.
        /// </summary>
        /// <value>The number of processors hosted by the machine on which the calling code is running.</value>
        public int ProcessorCount
        {
            get
            {
                try
                {
                    return Environment.ProcessorCount;
                }
                catch (LqbException)
                {
                    throw;
                }
                catch (Exception ex)
                {
                    throw new ServerException(ErrorMessage.SystemError, ex);
                }
            }
        }

        /// <summary>
        /// Gets the current stack trace information.
        /// </summary>
        /// <value>The current stack trace information.</value>
        public string StackTrace
        {
            get
            {
                try
                {
                    return Environment.StackTrace;
                }
                catch (LqbException)
                {
                    throw;
                }
                catch (Exception ex)
                {
                    throw new ServerException(ErrorMessage.SystemError, ex);
                }
            }
        }

        /// <summary>
        /// Terminates the current process and returns the exit code to the operating system.
        /// </summary>
        /// <param name="code">The exit code returned to the operating system.</param>
        public void Exit(int code)
        {
            try
            {
                Environment.Exit(code);
            }
            catch (LqbException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ServerException(ErrorMessage.SystemError, ex);
            }
        }
    }
}
