﻿namespace Adapter
{
    using FileDB3;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Implementation of the IFileToken interface for the FileDB file storage system.
    /// </summary>
    internal sealed class FileDbToken : IFileToken, IMeasureFileToken
    {
        /// <summary>
        /// The handle for a FileDB file.
        /// Note, the lifetime of the file handle is controlled by an adapter, not here.
        /// </summary>
        private FileHandle handle;

        /// <summary>
        /// The location from which this file was pulled.
        /// </summary>
        private FileStorageIdentifier storageId;

        /// <summary>
        /// The identifier of the pulled file.
        /// </summary>
        private FileIdentifier fileId;

        /// <summary>
        /// The encryption key associated with this file.
        /// </summary>
        private byte[] encryptionKey;

        /// <summary>
        /// Initializes a new instance of the <see cref="FileDbToken"/> class.
        /// </summary>
        /// <param name="handle">The handle for a FileDB file.</param>
        /// <param name="storageId">The storage identifier.</param>
        /// <param name="fileId">The file identifier.</param>
        /// <param name="encryptionKey">The encryption key associated with this file.</param>
        internal FileDbToken(FileHandle handle, FileStorageIdentifier storageId, FileIdentifier fileId, byte[] encryptionKey)
        {
            this.handle = handle;
            this.storageId = storageId;
            this.fileId = fileId;
            this.encryptionKey = encryptionKey;
        }

        /// <summary>
        /// Gets the identifier for the FileDB storage location.
        /// </summary>
        /// <value>The identifier for the FileDB storage location.</value>
        FileStorageIdentifier IMeasureFileToken.StorageIdentifier
        {
            get { return this.storageId; }
        }

        /// <summary>
        /// Gets the identifier for the FileDB file.
        /// </summary>
        /// <value>The identifier for the FileDB file.</value>
        FileIdentifier IMeasureFileToken.FileIdentifier
        {
            get { return this.fileId; }
        }

        /// <summary>
        /// Gets the encryption key for the FileDB file.
        /// </summary>
        /// <value>The encryption key for the FileDB file.</value>
        byte[] IMeasureFileToken.EncryptionKey
        {
            get { return this.encryptionKey; }
        }

        /// <summary>
        /// Gets the local file name that the file token represents.
        /// </summary>
        /// <value>The path on the local machine that contains the file.</value>
        LocalFilePath IFileToken.LocalFileName
        {
            get
            {
                if (this.handle == null)
                {
                    throw new DeveloperException(ErrorMessage.SystemError);
                }

                LocalFilePath? localPath = LocalFilePath.Create(this.handle.LocalFileName);
                if (localPath != null)
                {
                    return localPath.Value;
                }
                else
                {
                    throw new DeveloperException(ErrorMessage.SystemError);
                }
            }
        }

        /// <summary>
        /// When the encapsulated handle is disposed, this method is called
        /// so the client won't be able to continue to use this instance.
        /// </summary>
        internal void MakeInvalid()
        {
            this.handle = null;
        }

        /// <summary>
        /// Retrieve the encapsulated file handle.
        /// </summary>
        /// <returns>The encapsulated file handle.</returns>
        internal FileHandle GetHandle()
        {
            return this.handle;
        }
    }
}
