﻿namespace Adapter
{
    using System;
    using System.Security.Cryptography;
    using LqbGrammar.Adapters;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Adapter class implementation for DataEncryptionAdapter.
    /// </summary>
    internal sealed class DataEncryptionAdapter : IDataEncryptionAdapter
    {
        /// <summary>
        /// Decrypt the provided encrypted data.
        /// </summary>
        /// <param name="encryptedData">The encrypted data.</param>
        /// <param name="encryptionKey">The key to use in decryption.</param>
        /// <returns>The decrypted data.</returns>
        public byte[] Decrypt(byte[] encryptedData, EncryptionKey encryptionKey)
        {
            try
            {
                return encryptionKey.InvokeEncryption(decrypt: true, data: encryptedData);
            }
            catch (Exception ex) when (!(ex is LqbException))
            {
                // Only LQBExceptions should ever come out of Adapters.
                throw new DeveloperException(ErrorMessage.SystemError, ex);
            }
        }

        /// <summary>
        /// Encrypt the data with the key.
        /// </summary>
        /// <param name="unencryptedData">The data to be encrypted.</param>
        /// <param name="encryptionKey">The key to use in the encryption.</param>
        /// <returns>The encrypted data.</returns>
        public byte[] Encrypt(byte[] unencryptedData, EncryptionKey encryptionKey)
        {
            try
            {
                return encryptionKey.InvokeEncryption(decrypt: false, data: unencryptedData);
            }
            catch (Exception ex) when (!(ex is LqbException))
            {
                // Only LQBExceptions should ever come out of Adapters.
                throw new DeveloperException(ErrorMessage.SystemError, ex);
            }
        }
    }
}
