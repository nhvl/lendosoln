﻿namespace Adapter
{
    using System;
    using System.Xml.Linq;
    using LqbGrammar.Adapters;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Implementation of the IXmlFileAdapter interface.
    /// </summary>
    internal sealed class XmlFileAdapter : IXmlFileAdapter
    {
        /// <summary>
        /// Read a file containing XML.
        /// </summary>
        /// <param name="path">The file to read.</param>
        /// <returns>The XML data.</returns>
        public LqbXmlElement Load(LocalFilePath path)
        {
            try
            {
                var doc = XDocument.Load(path.Value);
                var elem = LqbXmlElement.Create(doc.Root);
                if (elem != null)
                {
                    return elem.Value;
                }
                else
                {
                    throw new DeveloperException(ErrorMessage.SystemError);
                }
            }
            catch (LqbException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new DeveloperException(ErrorMessage.SystemError, ex);
            }
        }

        /// <summary>
        /// Write XML data to a file.
        /// </summary>
        /// <param name="path">The file to which the XML is to be written.</param>
        /// <param name="xml">The XML data.</param>
        public void Save(LocalFilePath path, LqbXmlElement xml)
        {
            try
            {
                xml.Contained.Save(path.Value);
            }
            catch (Exception ex)
            {
                throw new DeveloperException(ErrorMessage.SystemError, ex);
            }
        }
    }
}
