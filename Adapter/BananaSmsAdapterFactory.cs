﻿namespace Adapter
{
    using System;
    using LqbGrammar.Adapters;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Factory for creating an SMS adapter that uses the Banana Api to send SMS messages.
    /// </summary>
    public sealed class BananaSmsAdapterFactory : ISmsAdapterFactory
    {
        /// <summary>
        /// Create an implemenation of the ISmsAdapter interface.
        /// </summary>
        /// <param name="source">The phone number uses as the source of the SMS messages.</param>
        /// <param name="controlInformation">Banana Api web service URL, Banana Api security token, Banana Api encrypted password.</param>
        /// <returns>An implementation of the ISmsAdapter interface.</returns>
        public ISmsAdapter Create(PhoneNumber source, params string[] controlInformation)
        {
            if (controlInformation == null || controlInformation.Length != 3)
            {
                throw new DeveloperException(ErrorMessage.SystemError);
            }

            var uri = LqbAbsoluteUri.Create(controlInformation[0]);
            if (uri == null)
            {
                throw new DeveloperException(ErrorMessage.BadConfiguration);
            }

            return new BananaSmsAdapter(uri.Value, source, controlInformation[1], controlInformation[2]);
        }
    }
}
