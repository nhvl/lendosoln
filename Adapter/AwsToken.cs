﻿namespace Adapter
{
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Implementation of the IFileToken interface for the AWS file storage system.
    /// </summary>
    internal sealed class AwsToken : IFileToken
    {
        /// <summary>
        /// The handle for an AWS file.
        /// Note, the lifetime of the file handle is controlled by an adapter, not here.
        /// </summary>
        private AwsS3FileHandle handle;

        /// <summary>
        /// Initializes a new instance of the <see cref="AwsToken"/> class.
        /// </summary>
        /// <param name="handle">The handle for an AWS file.</param>
        internal AwsToken(AwsS3FileHandle handle)
        {
            this.handle = handle;
        }

        /// <summary>
        /// Gets the local file name that the file token represents.
        /// </summary>
        /// <value>The path on the local machine that contains the file.</value>
        LocalFilePath IFileToken.LocalFileName
        {
            get
            {
                if (this.handle == null)
                {
                    throw new DeveloperException(ErrorMessage.SystemError);
                }

                LocalFilePath? localPath = LocalFilePath.Create(this.handle.LocalFileName);
                if (localPath != null)
                {
                    return localPath.Value;
                }
                else
                {
                    throw new DeveloperException(ErrorMessage.SystemError);
                }
            }
        }

        /// <summary>
        /// When the encapsulated handle is disposed, this method is called
        /// so the client won't be able to continue to use this instance.
        /// </summary>
        internal void MakeInvalid()
        {
            this.handle = null;
        }

        /// <summary>
        /// Retrieve the encapsulated file handle.
        /// </summary>
        /// <returns>The encapsulated file handle.</returns>
        internal AwsS3FileHandle GetHandle()
        {
            return this.handle;
        }
    }
}
