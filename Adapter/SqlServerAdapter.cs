﻿namespace Adapter
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using LqbGrammar.Adapters;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// An implementation of ISqlAdapter that communicates with a SQL Server database.
    /// </summary>
    internal sealed class SqlServerAdapter : ISqlAdapter
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SqlServerAdapter"/> class.
        /// </summary>
        /// <param name="factory">The database provider factory.</param>
        /// <param name="timeout">The timeout value.</param>
        public SqlServerAdapter(DbProviderFactory factory, TimeoutInSeconds timeout)
        {
            this.Factory = factory;
            this.Timeout = timeout;
        }

        /// <summary>
        /// Gets or sets the database provider factory.
        /// </summary>
        private DbProviderFactory Factory { get; set; }

        /// <summary>
        /// Gets or sets the timeout value.
        /// </summary>
        private TimeoutInSeconds Timeout { get; set; }

        /// <summary>
        /// Delete data from a SQL database.
        /// </summary>
        /// <param name="connection">The connection to the DB.</param>
        /// <param name="transaction">If using a transaction, pass it in here, else pass in null.</param>
        /// <param name="query">The query string.</param>
        /// <param name="theParams">The query parameters.</param>
        /// <returns>The number of rows that were deleted.</returns>
        public ModifiedRowCount Delete(DbConnection connection, DbTransaction transaction, SQLQueryString query, IEnumerable<DbParameter> theParams)
        {
            return this.HandleNonQuery(connection, transaction, query, theParams);
        }

        /// <summary>
        /// Insert a record into a table but do not retrieve the primary key.
        /// This is usually used for tables where the primary key is a set
        /// of columns rather than a generated key.
        /// </summary>
        /// <param name="connection">The connection to the DB.</param>
        /// <param name="transaction">If using a transaction, pass it in here, else pass in null.</param>
        /// <param name="query">The query string.</param>
        /// <param name="theParams">The query parameters.</param>
        public void InsertNoKey(DbConnection connection, DbTransaction transaction, SQLQueryString query, IEnumerable<DbParameter> theParams)
        {
            using (DbCommand cmd = connection.CreateCommand())  
            {
                cmd.CommandText = query.Value;
                this.PrepareCommand(cmd, transaction, theParams);

                bool hasError = false;
                try
                {
                    if (cmd.Connection.State != ConnectionState.Open)
                    {
                        cmd.Connection.Open();
                    }

                    cmd.ExecuteScalar();
                }
                catch (DbException exc)
                {
                    hasError = true;
                    throw new ServerException(ErrorMessage.SystemError, exc);
                }
                catch (Exception ex)
                {
                    hasError = true;
                    throw new ServerException(ErrorMessage.SystemError, ex);
                }
                finally
                {
                    if (hasError && (transaction != null))
                    {
                        transaction.Rollback();
                    }
                }
            }
        }

        /// <summary>
        /// Insert a record into a table that has a uniqueidentifier-valued primary key.
        /// </summary>
        /// <param name="connection">The connection to the DB.</param>
        /// <param name="transaction">If using a transaction, pass it in here, else pass in null.</param>
        /// <param name="query">The query string.</param>
        /// <param name="theParams">The query parameters.</param>
        /// <returns>The primary key of the inserted record.</returns>
        public PrimaryKeyGuid InsertGetGuid(DbConnection connection, DbTransaction transaction, SQLQueryString query, IEnumerable<DbParameter> theParams)
        {
            using (DbCommand cmd = connection.CreateCommand())
            {
                cmd.CommandText = query.Value;
                this.PrepareCommand(cmd, transaction, theParams);

                bool hasError = false;
                try
                {
                    if (cmd.Connection.State != ConnectionState.Open)
                    {
                        cmd.Connection.Open();
                    }

                    var pk = cmd.ExecuteScalar();

                    if (Convert.IsDBNull(pk))
                    {
                        throw new ServerException(ErrorMessage.SystemError);
                    }

                    var stringValue = pk.ToString();

                    PrimaryKeyGuid? keyValue = PrimaryKeyGuid.Create(stringValue);
                    if (keyValue != null)
                    {
                        return keyValue.Value;
                    }
                    else
                    {
                        hasError = true;
                        throw new ServerException(ErrorMessage.SystemError);
                    }
                }
                catch (LqbException)
                {
                    hasError = true;
                    throw;
                }
                catch (DbException exc)
                {
                    hasError = true;
                    throw new ServerException(ErrorMessage.SystemError, exc);
                }
                catch (Exception ex)
                {
                    hasError = true;
                    throw new ServerException(ErrorMessage.SystemError, ex);
                }
                finally
                {
                    if (hasError && (transaction != null))
                    {
                        transaction.Rollback();
                    }
                }
            }
        }

        /// <summary>
        /// Insert a record into a table that has an integer-valued primary key.
        /// </summary>
        /// <param name="connection">The connection to the DB.</param>
        /// <param name="transaction">If using a transaction, pass it in here, else pass in null.</param>
        /// <param name="query">The query string.</param>
        /// <param name="theParams">The query parameters.</param>
        /// <returns>The primary key of the inserted record.</returns>
        public PrimaryKeyInt32 InsertGetInt32(DbConnection connection, DbTransaction transaction, SQLQueryString query, IEnumerable<DbParameter> theParams)
        {
            using (DbCommand cmd = connection.CreateCommand())
            {
                cmd.CommandText = query.Value;
                this.PrepareCommand(cmd, transaction, theParams);

                bool hasError = false;
                try
                {
                    if (cmd.Connection.State != ConnectionState.Open)
                    {
                        cmd.Connection.Open();
                    }

                    var pk = cmd.ExecuteScalar();

                    if (Convert.IsDBNull(pk))
                    {
                        throw new ServerException(ErrorMessage.SystemError);
                    }

                    var intValue = Convert.ToInt32(pk);

                    PrimaryKeyInt32? keyValue = PrimaryKeyInt32.Create(intValue);
                    if (keyValue != null)
                    {
                        return keyValue.Value;
                    }
                    else
                    {
                        hasError = true;
                        throw new ServerException(ErrorMessage.SystemError);
                    }
                }
                catch (LqbException)
                {
                    hasError = true;
                    throw;
                }
                catch (DbException exc)
                {
                    hasError = true;
                    throw new ServerException(ErrorMessage.SystemError, exc);
                }
                catch (Exception ex)
                {
                    hasError = true;
                    throw new ServerException(ErrorMessage.SystemError, ex);
                }
                finally
                {
                    if (hasError && (transaction != null))
                    {
                        transaction.Rollback();
                    }
                }
            }
        }

        /// <summary>
        /// Retrieve data from a SQL database.
        /// </summary>
        /// <param name="connection">The connection to the DB.</param>
        /// <param name="transaction">If using a transaction, pass it in here, else pass in null.</param>
        /// <param name="query">The query string.</param>
        /// <param name="theParams">The query parameters.</param>
        /// <returns>The retrieved data.</returns>
        public DbDataReader Select(DbConnection connection, DbTransaction transaction, SQLQueryString query, IEnumerable<DbParameter> theParams)
        {
            using (DbCommand cmd = connection.CreateCommand())
            {
                cmd.CommandText = query.Value;
                this.PrepareCommand(cmd, transaction, theParams);

                bool hasError = false;
                try
                {
                    if (cmd.Connection.State != ConnectionState.Open)
                    {
                        cmd.Connection.Open();
                    }

                    return cmd.ExecuteReader(); // DO NOT CLOSE CONNECTION WHEN THE READER IS CLOSED
                }
                catch (DbException exc)
                {
                    hasError = true;
                    throw new ServerException(ErrorMessage.SystemError, exc);
                }
                catch (Exception ex)
                {
                    hasError = true;
                    throw new ServerException(ErrorMessage.SystemError, ex);
                }
                finally
                {
                    if (hasError && (transaction != null))
                    {
                        transaction.Rollback();
                    }
                }
            }
        }

        /// <summary>
        /// Retrieve data from a SQL database and populate the input data set.
        /// </summary>
        /// <param name="connection">The connection to the DB.</param>
        /// <param name="transaction">If using a transaction, pass it in here, else pass in null.</param>
        /// <param name="fillTarget">The data set that will be filled.</param>
        /// <param name="query">The query string.</param>
        /// <param name="theParams">The query parameters.</param>
        public void FillDataSet(DbConnection connection, DbTransaction transaction, DataSet fillTarget, SQLQueryString query, IEnumerable<DbParameter> theParams)
        {
            using (DbCommand cmd = connection.CreateCommand())
            {
                cmd.CommandText = query.Value;
                this.PrepareCommand(cmd, transaction, theParams);

                bool hasError = false;
                try
                {
                    var dataAdapter = this.Factory.CreateDataAdapter();
                    dataAdapter.SelectCommand = cmd;
                    dataAdapter.SelectCommand.CommandType = CommandType.Text;

                    if (cmd.Connection.State != ConnectionState.Open)
                    {
                        cmd.Connection.Open();
                    }

                    if (string.IsNullOrEmpty(fillTarget.DataSetName))
                    {
                        dataAdapter.Fill(fillTarget);
                    }
                    else
                    {
                        dataAdapter.Fill(fillTarget, fillTarget.DataSetName);
                    }
                }
                catch (DbException exc)
                {
                    hasError = true;
                    throw new ServerException(ErrorMessage.SystemError, exc);
                }
                catch (Exception ex)
                {
                    hasError = true;
                    throw new ServerException(ErrorMessage.SystemError, ex);
                }
                finally
                {
                    if (hasError && (transaction != null))
                    {
                        transaction.Rollback();
                    }
                }
            }
        }

        /// <summary>
        /// Retrieve data from a SQL database and populate the input data set using the pre-prepared adapter.
        /// </summary>
        /// <remarks>
        /// This will open the SQL connection without closing it.  Caller
        /// should be sure to either close or dispose it or dispose the adapter.
        /// </remarks>
        /// <param name="adapter">The adapter used to fill the data set.</param>
        /// <param name="fillTarget">The data set that will be filled.</param>
        /// <param name="tableName">Optional value that is the table name for updating.</param>
        public void FillDataSet(DbDataAdapter adapter, DataSet fillTarget, DBTableName? tableName)
        {
            if (adapter.SelectCommand.Connection.State != ConnectionState.Open)
            {
                adapter.SelectCommand.Connection.Open();
            }

            if ((tableName == null) || (tableName == DBTableName.Invalid))
            {
                adapter.Fill(fillTarget);
            }
            else
            {
                adapter.Fill(fillTarget, tableName.Value.ToString());
            }
        }

        /// <summary>
        /// Update data to a SQL database and with changes in the data set using the pre-prepared adapter.
        /// </summary>
        /// <remarks>
        /// This will open the SQL connection without closing it.  Caller
        /// should be sure to either close or dispose it or dispose the adapter.
        /// </remarks>
        /// <param name="adapter">The adapter used to update the data set.</param>
        /// <param name="withChanges">The data set that will be updated to the SQL database.</param>
        /// <param name="tableName">Optional value that is the table name for updating.</param>
        /// <returns>The number of rows that were modified.</returns>
        public ModifiedRowCount UpdateDataSet(DbDataAdapter adapter, DataSet withChanges, DBTableName? tableName)
        {
            if ((adapter.UpdateCommand != null) && (adapter.UpdateCommand.Connection != null) && (adapter.UpdateCommand.Connection.State != ConnectionState.Open))
            {
                adapter.UpdateCommand.Connection.Open();
            }

            if ((adapter.InsertCommand != null) && (adapter.InsertCommand.Connection != null) && (adapter.InsertCommand.Connection.State != ConnectionState.Open))
            {
                adapter.InsertCommand.Connection.Open();
            }

            if ((adapter.DeleteCommand != null) && (adapter.DeleteCommand.Connection != null) && (adapter.DeleteCommand.Connection.State != ConnectionState.Open))
            {
                adapter.DeleteCommand.Connection.Open();
            }

            int rowsModified;
            if ((tableName == null) || (tableName == DBTableName.Invalid))
            {
                rowsModified = adapter.Update(withChanges);
            }
            else
            {
                rowsModified = adapter.Update(withChanges, tableName.Value.ToString());
            }

            ModifiedRowCount? rowCount = ModifiedRowCount.Create(rowsModified);
            if (rowCount != null)
            {
                return rowCount.Value;
            }
            else
            {
                throw new ServerException(ErrorMessage.SystemError);
            }
        }

        /// <summary>
        /// Update data held in a SQL database.
        /// </summary>
        /// <param name="connection">The connection to the DB.</param>
        /// <param name="transaction">If using a transaction, pass it in here, else pass in null.</param>
        /// <param name="query">The query string.</param>
        /// <param name="theParams">The query parameters.</param>
        /// <returns>The number of rows that were modified.</returns>
        public ModifiedRowCount Update(DbConnection connection, DbTransaction transaction, SQLQueryString query, IEnumerable<DbParameter> theParams)
        {
            return this.HandleNonQuery(connection, transaction, query, theParams);
        }

        /// <summary>
        /// Handle a non-insert data modification for a SQL database.
        /// </summary>
        /// <param name="connection">The connection to the DB.</param>
        /// <param name="transaction">If using a transaction, pass it in here, else pass in null.</param>
        /// <param name="query">The query string.</param>
        /// <param name="theParams">The query parameters.</param>
        /// <returns>The number of rows that were modified.</returns>
        private ModifiedRowCount HandleNonQuery(DbConnection connection, DbTransaction transaction, SQLQueryString query, IEnumerable<DbParameter> theParams)
        {
            using (DbCommand cmd = connection.CreateCommand())
            {
                cmd.CommandText = query.Value;
                this.PrepareCommand(cmd, transaction, theParams);

                bool hasError = false;
                try
                {
                    if (cmd.Connection.State != ConnectionState.Open)
                    {
                        cmd.Connection.Open();
                    }

                    int rowsModified = cmd.ExecuteNonQuery();

                    ModifiedRowCount? rowCount = ModifiedRowCount.Create(rowsModified);
                    if (rowCount != null)
                    {
                        return rowCount.Value;
                    }
                    else
                    {
                        throw new ServerException(ErrorMessage.SystemError);
                    }
                }
                catch (LqbException)
                {
                    hasError = true;
                    throw;
                }
                catch (DbException exc)
                {
                    hasError = true;
                    throw new ServerException(ErrorMessage.SystemError, exc);
                }
                catch (Exception ex)
                {
                    hasError = true;
                    throw new ServerException(ErrorMessage.SystemError, ex);
                }
                finally
                {
                    if (hasError && (transaction != null))
                    {
                        transaction.Rollback();
                    }
                }
            }
        }

        /// <summary>
        /// Add the timeout and parameters to the command.
        /// </summary>
        /// <param name="cmd">The sql command.</param>
        /// <param name="transaction">If using a transaction, pass it in here, else pass in null.</param>
        /// <param name="theParams">The command parameters.</param>
        private void PrepareCommand(DbCommand cmd, DbTransaction transaction, IEnumerable<DbParameter> theParams)
        {
            int timeout = this.Timeout.Value;
            if (timeout > 0)
            {
                cmd.CommandTimeout = timeout;
            }

            if (transaction != null)
            {
                cmd.Transaction = transaction;
            }

            if (theParams != null)
            {
                foreach (DbParameter p in theParams)
                {
                    cmd.Parameters.Add(p);
                }
            }
        }
    }
}
