﻿namespace Adapter
{
    using LqbGrammar.Adapters;

    /// <summary>
    /// Factory to create implementations of the IHttpRequestAdapter interface.
    /// </summary>
    public sealed class HttpRequestAdapterFactory : IHttpRequestAdapterFactory
    {
        /// <summary>
        /// Create an implementation of the IHttpRequestAdapter interface.
        /// </summary>
        /// <returns>An implementation of the IHttpRequestAdapter interface.</returns>
        public IHttpRequestAdapter Create()
        {
            return new HttpRequestAdapter();
        }
    }
}
