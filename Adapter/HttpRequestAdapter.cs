﻿namespace Adapter
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.IO;
    using System.Net;
    using System.Security.Cryptography.X509Certificates;
    using System.Text;
    using System.Web;
    using System.Xml;
    using LqbGrammar.Adapters;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers.HttpRequest;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Implementation of the IHttpRequestAdapter interface.
    /// </summary>
    internal sealed class HttpRequestAdapter : IHttpRequestAdapter
    {
        /// <summary>
        /// Carry out the communication with the indicated URL.
        /// </summary>
        /// <param name="url">The target URL.</param>
        /// <param name="options">The options that control the request and response parsing.</param>
        /// <returns>True if the communication was successful, false otherwise.</returns>
        public bool ExecuteCommunication(LqbAbsoluteUri url, HttpRequestOptions options)
        {
            try
            {
                var webRequest = this.InitializeRequest(url.ToString(), options);
                this.InjectPostData(webRequest, options);

                using (var webResponse = (HttpWebResponse)webRequest.GetResponse())
                {
                    return this.ReadResponse(webResponse, options);
                }
            }
            catch (LqbException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ServerException(ErrorMessage.SystemError, null, ex);
            }
        }

        /// <summary>
        /// Create and initialize an instance of the HttpWebRequest class.
        /// </summary>
        /// <param name="url">The url to which the request is sent.</param>
        /// <param name="options">The options that control the request.</param>
        /// <returns>An instance of the HttpWebRequest class.</returns>
        private HttpWebRequest InitializeRequest(string url, HttpRequestOptions options)
        {
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);
            webRequest.KeepAlive = options.KeepAlive;
            webRequest.Method = options.Method.ToString();
            webRequest.AllowAutoRedirect = options.AllowAutoRedirect;
            webRequest.MaximumAutomaticRedirections = options.MaximumAutomaticRedirections;
            webRequest.PreAuthenticate = options.PreAuthenticate;
            webRequest.Timeout = options.Timeout.Value * 1000; // HttpWebRequest uses milliseconds as the unit of time

            if (options.AcceptOptions != null)
            {
                this.AddAcceptOptions(webRequest, options.AcceptOptions);
            }

            if (options.SetDecompressionMethods)
            {
                webRequest.AutomaticDecompression = DecompressionMethods.Deflate | DecompressionMethods.GZip;
            }

            if (options.ContentLength != null)
            {
                webRequest.ContentLength = options.ContentLength.Value;
            }

            if (options.Encoding != null || options.MimeType != null)
            {
                this.AddContentType(webRequest, options.Encoding, options.MimeType);
            }

            if (options.Credentials != null)
            {
                webRequest.Credentials = options.Credentials;
            }

            if (options.SetAcceptDefaultLanguage)
            {
                webRequest.Headers.Add(HttpRequestHeader.AcceptLanguage, "en-US");
            }

            if (options.RequestHeaders != null)
            {
                this.AddHeaders(webRequest, options.RequestHeaders);
            }

            if (options.MaximumResponseHeadersLength != null)
            {
                webRequest.MaximumResponseHeadersLength = options.MaximumResponseHeadersLength.Value;
            }

            if (options.UseHTTPProtocol10)
            {
                webRequest.ProtocolVersion = HttpVersion.Version10;
            }

            if (options.Referrer != null)
            {
                webRequest.Referer = options.Referrer.Value.ToString();
            }

            if (options.UserAgent != null)
            {
                webRequest.UserAgent = options.UserAgent.Value.ToString();
            }

            if (options.RequestCookieContainer != null)
            {
                webRequest.CookieContainer = options.RequestCookieContainer;
            }

            if (options.ClientCertificates != null)
            {
                webRequest.ClientCertificates = options.ClientCertificates;
            }

            return webRequest;
        }

        /// <summary>
        /// If there is post data, add the data to the web request.
        /// </summary>
        /// <param name="webRequest">The web request.</param>
        /// <param name="options">The options structure that holds the post data.</param>
        private void InjectPostData(HttpWebRequest webRequest, HttpRequestOptions options)
        {
            if ((options.Method != HttpMethod.Post && options.Method != HttpMethod.Put)
                || options.PostData == null)
            {
                return;
            }

            options.PostData.WriteToStream(webRequest.GetRequestStream());
        }

        /// <summary>
        /// Read all the response data into the structure that delivers the data to the client.
        /// </summary>
        /// <param name="response">The web response.</param>
        /// <param name="options">The structure that delivers the data to the client.</param>
        /// <returns>True if successful, false otherwise.</returns>
        private bool ReadResponse(HttpWebResponse response, HttpRequestOptions options)
        {
            options.ResponseStatusCode = response.StatusCode;
            options.ResponseStatusDescription = response.StatusDescription;

            if (options.IgnoreResponse)
            {
                return true;
            }

            if (options.ResponseHeaders != null)
            {
                foreach (var key in response.Headers.AllKeys)
                {
                    options.ResponseHeaders.Add(key, response.Headers.Get(key));
                }
            }

            if (options.ResponseCookieContainer != null)
            {
                options.ResponseCookieContainer.Add(response.Cookies);
            }

            if (response.ContentLength > HttpRequestOptions.MaximumResponseLength)
            {
                if (options.ResponseDumpPath != null)
                {
                    using (Stream stream = response.GetResponseStream())
                    {
                        this.WriteStreamToFile(stream, options.ResponseDumpPath.Value);
                    }
                }

                return false;
            }

            using (Stream stream = response.GetResponseStream())
            {
                try
                {
                    this.ReadResponseBody(stream, options);
                    return true;
                }
                catch
                {
                    if (stream.CanSeek && options.ResponseDumpPath != null)
                    {
                        stream.Seek(0, SeekOrigin.Begin);
                        this.WriteStreamToFile(stream, options.ResponseDumpPath.Value);
                        return false;
                    }

                    throw;
                }
            }
        }

        /// <summary>
        /// Read the response body into the appropriate data structure.
        /// </summary>
        /// <param name="stream">The response body.</param>
        /// <param name="options">The appropriate data structures are held here.</param>
        private void ReadResponseBody(Stream stream, HttpRequestOptions options)
        {
            if (options.ResponseFileName != null)
            {
                using (var fileStream = File.Open(options.ResponseFileName.Value.Value, FileMode.Create))
                {
                    stream.CopyTo(fileStream);
                }
            }
            else if (options.XmlResponseDelegate != null)
            {
                // XmlReaderSettings doesn't have an Encoding property,
                // the xml stream itself is used to detect the encoding.
                using (var reader = XmlReader.Create(stream, options.XmlReaderSettings))
                {
                    options.XmlResponseDelegate(reader);
                }
            }
            else
            {
                using (var reader = new StreamReader(stream, Encoding.UTF8))
                {
                    string response = reader.ReadToEnd();
                    options.ResponseBody = response;
                }
            }
        }

        /// <summary>
        /// Write the contents of the stream to a file.
        /// </summary>
        /// <param name="stream">The stream.</param>
        /// <param name="path">The path to the file where the stream data will be written.</param>
        private void WriteStreamToFile(Stream stream, LocalFilePath path)
        {
            using (var fileStream = File.Open(path.Value, FileMode.Create))
            {
                stream.CopyTo(fileStream);
            }
        }

        /// <summary>
        /// Add the mime-types to the Accept header.
        /// </summary>
        /// <param name="webRequest">An instance of the HttpWebRequest class.</param>
        /// <param name="mimeTypes">The list of mime-types.</param>
        private void AddAcceptOptions(HttpWebRequest webRequest, List<MimeType> mimeTypes)
        {
            var sb = new StringBuilder();
            foreach (var mimeType in mimeTypes)
            {
                if (sb.Length > 0)
                {
                    sb.Append(", ");
                }

                sb.Append(mimeType.ToString());
            }

            webRequest.Accept = sb.ToString();
        }

        /// <summary>
        /// Add the mime-type and encoding to the Content-Type header.
        /// </summary>
        /// <param name="webRequest">An instance of the HttpWebRequest class.</param>
        /// <param name="encoding">The character encoding.</param>
        /// <param name="mimeType">The mime-type.</param>
        private void AddContentType(HttpWebRequest webRequest, CharacterEncoding? encoding, MimeType mimeType)
        {
            var sb = new StringBuilder();
            if (mimeType != null)
            {
                sb.Append(mimeType.ToString());
            }
            
            if (encoding != null)
            {
                if (sb.Length > 0)
                {
                    sb.Append("; ");
                }

                sb.Append("charset=" + encoding.Value.ToString());
            }

            webRequest.ContentType = sb.ToString();
        }

        /// <summary>
        /// Add the http headers.
        /// </summary>
        /// <param name="webRequest">An instance of the HttpWebRequest class.</param>
        /// <param name="headers">The http headers.</param>
        private void AddHeaders(HttpWebRequest webRequest, NameValueCollection headers)
        {
            foreach (var key in headers.AllKeys)
            {
                webRequest.Headers.Add(key, headers[key]);
            }
        }
    }
}
