﻿namespace Adapter
{
    using System.Collections.Generic;
    using System.Text.RegularExpressions;
    using DocumentFormat.OpenXml.Packaging;
    using DocumentFormat.OpenXml.Wordprocessing;
    using LqbGrammar.Adapters;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Defines the interface to open XML documents.
    /// </summary>
    public class OpenXmlDocumentAdapter : IOpenXmlDocumentAdapter
    {
        /// <summary>
        /// Sets values for the list of fields in the open XML document.
        /// </summary>
        /// <param name="localFilePath">
        /// The path to the document.
        /// </param>
        /// <param name="fieldNameValuePairs">
        /// The list of field name - value pairs.
        /// </param>
        public void SetTextFieldsOnDocument(LocalFilePath localFilePath, IEnumerable<KeyValuePair<string, string>> fieldNameValuePairs)
        {
            using (var document = WordprocessingDocument.Open(localFilePath.Value, isEditable: true))
            {
                var descendants = document.MainDocumentPart.Document.Descendants<Text>();
                foreach (var descendant in descendants)
                {
                    // Use a regex instead of successive string.Replace calls per https://stackoverflow.com/a/11900932.
                    var descendantText = Regex.Replace(descendant.Text, "[«|»]", " ", RegexOptions.Compiled);

                    foreach (var fieldNameValuePair in fieldNameValuePairs)
                    {
                        descendantText = Regex.Replace(descendantText, $@"\b{fieldNameValuePair.Key}\b", fieldNameValuePair.Value ?? string.Empty);
                    }

                    descendant.Text = descendantText;
                }
            }
        }
    }
}
