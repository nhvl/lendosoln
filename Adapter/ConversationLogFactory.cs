﻿namespace Adapter
{
    using System;
    using LqbGrammar;
    using LqbGrammar.Adapters;
    using LqbGrammar.Drivers;

    /// <summary>
    /// Factory for creating adapter instances for the conversation log.
    /// </summary>
    public sealed class ConversationLogAdapterFactory : ICommentLogAdapterFactory
    {
        /// <summary>
        /// Create an implementation of the category management interface.
        /// </summary>
        /// <param name="location">The location of the conversation log service.</param>
        /// <param name="port">The port for the conversation log service.</param>
        /// <returns>An implementation of the category management interface.</returns>
        public ICategoryAdapter CreateCategoryAdapter(string location, int port)
        {
            return this.CreateImplementation(location, port) as ICategoryAdapter;
        }

        /// <summary>
        /// Create an implementation of the comment management interface.
        /// </summary>
        /// <param name="location">The location of the conversation log service.</param>
        /// <param name="port">The port for the conversation log service.</param>
        /// <returns>An implementation of the comment management interface.</returns>
        public ICommentAdapter CreateCommentAdapter(string location, int port)
        {
            return this.CreateImplementation(location, port) as ICommentAdapter;
        }

        /// <summary>
        /// Create an instance of the class that implements the conversation log adapter.
        /// </summary>
        /// <param name="location">The location of the conversation log service.</param>
        /// <param name="port">The port for the conversation log service.</param>
        /// <returns>An instance of the class that implements the conversation log adapter.</returns>
        private ILqbAdapter CreateImplementation(string location, int port)
        {
            return new ConversationLog(location, port);
        }
    }
}
