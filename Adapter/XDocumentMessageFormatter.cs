﻿namespace Adapter
{
    using System;
    using System.IO;
    using System.Messaging;
    using System.Xml;
    using System.Xml.Linq;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Copied from CommonProjectLib.Common.XDocumentMessageFormater.
    /// </summary>
    public sealed class XDocumentMessageFormatter : IMessageFormatter
    {
        /// <summary>
        /// Test whether the message can be read.
        /// </summary>
        /// <param name="message">The message under consideration.</param>
        /// <returns>True if the message can be read, false otherwise.</returns>
        public bool CanRead(Message message)
        {
            if (message == null)
            {
                return false;
            }

            return message.Formatter.GetType() == typeof(XDocumentMessageFormatter);
        }

        /// <summary>
        /// Read the contents of the message.
        /// </summary>
        /// <param name="message">The message that is being read.</param>
        /// <returns>The contents of the message.</returns>
        public object Read(Message message)
        {
            if (message == null)
            {
                return null;
            }

            if (message.Formatter.GetType() != typeof(XDocumentMessageFormatter))
            {
                throw new DeveloperException(ErrorMessage.SystemError);
            }

            using (XmlReader reader = XmlReader.Create(message.BodyStream))
            {
                return XDocument.Load(reader);
            }
        }

        /// <summary>
        /// Write an object into the message.
        /// </summary>
        /// <param name="message">The message that will be written to.</param>
        /// <param name="obj">The object that will get written into the message.</param>
        public void Write(Message message, object obj)
        {
            if (message == null || obj == null)
            {
                return;
            }

            XDocument doc = obj as XDocument;
            if (doc == null)
            {
                throw new DeveloperException(ErrorMessage.SystemError);
            }

            MemoryStream stream = new MemoryStream();
            using (XmlWriter writer = XmlWriter.Create(stream))
            {
                doc.WriteTo(writer);
            }

            message.BodyStream = stream;
        }

        /// <summary>
        /// Create a clone of this formatter.
        /// </summary>
        /// <returns>A clone of this formatter.</returns>
        public object Clone()
        {
            return new XDocumentMessageFormatter();
        }
    }
}
