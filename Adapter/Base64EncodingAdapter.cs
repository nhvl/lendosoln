﻿namespace Adapter
{
    using System;
    using LqbGrammar.Adapters;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Adapter for encoding and decoding base64 values.
    /// </summary>
    internal sealed class Base64EncodingAdapter : IBase64EncodingAdapter
    {
        /// <summary>
        /// Encode the value in base64.
        /// </summary>
        /// <param name="unencodedData">The value to be encoded.</param>
        /// <returns>The base64 encoded value.</returns>
        public Base64EncodedData Encode(byte[] unencodedData)
        {
            return Base64EncodedData.Create(Convert.ToBase64String(unencodedData)).Value;
        }

        /// <summary>
        /// Decode the value in base64.
        /// </summary>
        /// <param name="base64EncodedData">The encoded value.</param>
        /// <returns>The decoded value.</returns>
        public byte[] Decode(Base64EncodedData base64EncodedData)
        {
            return Convert.FromBase64String(base64EncodedData.Value);
        }
    }
}
