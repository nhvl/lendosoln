﻿namespace Adapter
{
    using Amazon.S3;
    using Amazon.S3.Model;

    /// <summary>
    /// Class used to communicate with AWS via an interface that mimics FileDB.
    /// This code was copied from a test project written by David Dao.
    /// </summary>
    internal static class AwsS3FileStorage
    {
        /// <summary>
        /// Determine whether the file currently exists at AWS.
        /// </summary>
        /// <param name="databaseName">The AWS bucket.</param>
        /// <param name="key">The AWS file name.</param>
        /// <returns>True if the file exists at AWS, false otherwise.</returns>
        public static bool FileExists(string databaseName, string key)
        {
            using (IAmazonS3 s3 = new AmazonS3Client())
            {
                GetObjectMetadataRequest request = new GetObjectMetadataRequest();
                request.BucketName = databaseName;
                request.Key = key;

                try
                {
                    GetObjectMetadataResponse response = s3.GetObjectMetadata(request);
                    return true;
                }
                catch (AmazonS3Exception exc)
                {
                    if (exc.StatusCode == System.Net.HttpStatusCode.NotFound)
                    {
                        return false;
                    }
                    else if (exc.StatusCode == System.Net.HttpStatusCode.Forbidden)
                    {
                        // 2015-12-29 - dd - S3 could throw access denied on non-existing key and user does not have permission to list the keys.
                        return false;
                    }
                    else
                    {
                        throw; // TODO: Need to handle these status code.
                    }
                }
            }
        }

        /// <summary>
        /// Retrieve the file from AWS.
        /// </summary>
        /// <param name="databaseName">The AWS bucket.</param>
        /// <param name="key">The AWS file name.</param>
        /// <returns>File token that references a local copy of the retrieved file.</returns>
        public static AwsS3FileHandle GetFile(string databaseName, string key)
        {
            var fh = new AwsS3FileHandle();

            try
            {
                using (IAmazonS3 s3 = new AmazonS3Client())
                {
                    GetObjectRequest request = new GetObjectRequest();
                    request.BucketName = databaseName;
                    request.Key = key;

                    GetObjectResponse response = s3.GetObject(request);

                    response.WriteResponseStreamToFile(fh.LocalFileName);

                    return fh;
                }
            }
            catch (AmazonS3Exception exc)
            {
                if (exc.StatusCode == System.Net.HttpStatusCode.NotFound)
                {
                    var notFoundException = new System.IO.FileNotFoundException();
                    notFoundException.Data.Add("Bucket", databaseName);
                    notFoundException.Data.Add("Key", key);
                    throw notFoundException;
                }
                else if (exc.StatusCode == System.Net.HttpStatusCode.Forbidden)
                {
                    // 2015-12-29 - dd - S3 could throw access denied on non-existing key and user does not have permission to list the keys.
                    var notFoundException = new System.IO.FileNotFoundException();
                    notFoundException.Data.Add("Bucket", databaseName);
                    notFoundException.Data.Add("Key", key);
                    throw notFoundException;
                }
                else
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Create and return an instance of AwsS3FileHandle.
        /// </summary>
        /// <returns>An instance of AwsS3FileHandle.</returns>
        public static AwsS3FileHandle NewFileHandle()
        {
            return new AwsS3FileHandle();
        }

        /// <summary>
        /// Upload the local file to the AWS file storage.
        /// </summary>
        /// <param name="databaseName">The AWS bucket.</param>
        /// <param name="key">The AWS file name.</param>
        /// <param name="fileHandle">Handle to the local file.</param>
        public static void PutFile(string databaseName, string key, AwsS3FileHandle fileHandle)
        {
            using (IAmazonS3 s3 = new AmazonS3Client())
            {
                PutObjectRequest request = new PutObjectRequest();
                request.BucketName = databaseName;
                request.Key = key;
                request.FilePath = fileHandle.LocalFileName;

                request.ServerSideEncryptionMethod = ServerSideEncryptionMethod.AES256;

                s3.PutObject(request);
            }
        }

        /// <summary>
        /// Delete the file from the AWS file storage.
        /// </summary>
        /// <param name="databaseName">The AWS bucket.</param>
        /// <param name="key">The file identifier used to delete.</param>
        public static void DeleteFile(string databaseName, string key)
        {
            using (IAmazonS3 s3 = new AmazonS3Client())
            {
                s3.Delete(databaseName, key, null);
            }
        }
    }
}
