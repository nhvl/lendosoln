﻿namespace Adapter
{
    using System;
    using System.Collections.Generic;
    using LqbGrammar.Adapters;

    /// <summary>
    /// Factory for creating instances of the ApplicationSettingsAdapter class.
    /// </summary>
    public sealed class ApplicationSettingsAdapterFactory : IApplicationSettingsAdapterFactory
    {
        /// <summary>
        /// Create an instance of the ApplicationSettingsAdapter class.
        /// </summary>
        /// <returns>An instance of the ApplicationSettingsAdapter class.</returns>
        public IApplicationSettingsAdapter Create()
        {
            return new ApplicationSettingsAdapter();
        }
    }
}
