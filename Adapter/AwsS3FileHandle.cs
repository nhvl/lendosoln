﻿namespace Adapter
{
    using System;
    using System.IO;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Exceptions;
    using LqbGrammar.Utils;

    /// <summary>
    /// File token that represents a local copy of an AWS file.
    /// </summary>
    internal sealed class AwsS3FileHandle : IDisposable
    {
        /// <summary>
        /// Flag set immediately when Dispose is called so methods can error out immediately.
        /// </summary>
        private bool dead;

        /// <summary>
        /// Flag set after Dispose is executed so the code is only executed once.
        /// </summary>
        private bool disposed;

        /// <summary>
        /// The path to the locak copy of an AWS file.
        /// </summary>
        private string path;

        /// <summary>
        /// Initializes a new instance of the <see cref="AwsS3FileHandle"/> class.
        /// </summary>
        public AwsS3FileHandle()
        {
            this.path = TempFileUtils.NewTempFilePath().Value;
        }

        /// <summary>
        /// Finalizes an instance of the <see cref="AwsS3FileHandle" /> class.
        /// </summary>
        ~AwsS3FileHandle()
        {
            this.DisposeImpl();
        }

        /// <summary>
        /// Gets the path to the local copy of the AWS file.
        /// </summary>
        /// <value>The path to the local copy of the AWS file.</value>
        public string LocalFileName
        {
            get
            {
                if (this.dead)
                {
                    throw new DeveloperException(ErrorMessage.SystemError);
                }

                return this.path;
            }
        }

        /// <summary>
        /// Clean up, i.e., delete the local copy of the AWS file.
        /// </summary>
        public void Dispose()
        {
            this.dead = true;
            if (!this.disposed)
            {
                this.DisposeImpl();
                GC.SuppressFinalize(this);
                this.disposed = true;
            }
        }

        /// <summary>
        /// Delete the local copy of the AWS file.
        /// </summary>
        private void DisposeImpl()
        {
            if (File.Exists(this.path))
            {
                File.Delete(this.path);
            }
        }
    }
}
