﻿namespace Adapter
{
    using LqbGrammar.Adapters;

    /// <summary>
    /// Factory class for creating IBase64EncodingAdapter interface.
    /// </summary>
    public sealed class Base64EncodingAdapterFactory : IBase64EncodingAdapterFactory
    {
        /// <summary>
        /// Create an IBase64EncodingAdapter instance.
        /// </summary>
        /// <returns>IBase64EncodingAdapter instance.</returns>
        public IBase64EncodingAdapter Create()
        {
            return new Base64EncodingAdapter();
        }
    }
}