﻿namespace Adapter
{
    using LqbGrammar;
    using LqbGrammar.Adapters;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;

    /// <summary>
    /// Utility encryption class that understands how the
    /// LQB encryption key system works.
    /// </summary>
    public static class LqbEncryptor
    {
        /// <summary>
        /// Encrypt the input data with the input encryption key.
        /// </summary>
        /// <param name="keyId">The identifier for the encryption key.</param>
        /// <param name="data">The data to encrypt.</param>
        /// <returns>The encrypted data.</returns>
        public static byte[] Encrypt(EncryptionKeyIdentifier keyId, byte[] data)
        {
            var key = RetrieveKey(keyId);
            return key.InvokeEncryption(false, data);
        }

        /// <summary>
        /// Decrypt the input with the encryption key.
        /// </summary>
        /// <param name="keyId">The identifier for the encryption key.</param>
        /// <param name="encrypted">The encrypted data.</param>
        /// <returns>The decrypted data.</returns>
        public static byte[] Decrypt(EncryptionKeyIdentifier keyId, byte[] encrypted)
        {
            var key = RetrieveKey(keyId);
            return key.InvokeEncryption(true, encrypted);
        }

        /// <summary>
        /// Use key storage to retrieve the encryption key.
        /// </summary>
        /// <param name="keyId">The identifier for the encryption key.</param>
        /// <returns>The encryption key.</returns>
        private static EncryptionKey RetrieveKey(EncryptionKeyIdentifier keyId)
        {
            var factory = GenericLocator<IEncryptionKeyDriverFactory>.Factory;
            IEncryptionKeyDriver keyDriver = factory.Create();
            return keyDriver.GetKey(keyId);
        }
    }
}
