﻿namespace Adapter
{
    using LqbGrammar.Adapters;

    /// <summary>
    /// Factory class for creating implementations of the IAwsFileStorageAdapter interface.
    /// </summary>
    public class AwsAdapterFactory : IAwsFileStorageAdapterFactory
    {
        /// <summary>
        /// Create an implementation of the IAwsFileStorageAdapter interface.
        /// </summary>
        /// <returns>An implementation of the IAwsFileStorageAdapter interface.</returns>
        public IAwsFileStorageAdapter Create()
        {
            return new AwsAdapter();
        }
    }
}
