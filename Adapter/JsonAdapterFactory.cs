﻿namespace Adapter
{
    using System;
    using LqbGrammar.Adapters;
    using LqbGrammar.Drivers;

    /// <summary>
    /// Factory for creating implementations of the IJsonAdapter interface.
    /// </summary>
    public class JsonAdapterFactory : IJsonAdapterFactory
    {
        /// <summary>
        /// Create an instance of a class that implements the IJsonAdapter interface.
        /// </summary>
        /// <param name="serializer">The type of serializer to use.</param>
        /// <param name="options">Options that control the serializer's behavior.</param>
        /// <returns>An implementation of IJsonAdapter.</returns>
        public IJsonAdapter Create(JsonSerializer serializer, JsonSerializerOptions options)
        {
            return new JsonAdapter(serializer, options);
        }
    }
}
