﻿namespace Adapter
{
    using Adapter.Emailer;
    using Adapter.MethodInvoke;
    using LqbGrammar;
    using LqbGrammar.Adapters;
    using LqbGrammar.Adapters.SecurityEventLogging;
    using SecurityEventLogging;

    /// <summary>
    /// This is a utility class to make it easier for applications to register
    /// all the standard adapters with this one call.  Since the factories have
    /// public access, applications are free to just register them themselves.
    /// An alternative is to use this to get all the standard factories and
    /// then overwrite any that the application desires to substitute with its
    /// own implementations.
    /// </summary>
    public static class ApplicationInitializer
    {
        /// <summary>
        /// Register factories with the location service.
        /// </summary>
        /// <param name="appName">The name of the application, which may be used to customize the initialization.</param>
        /// <param name="initializer">The application initializer.</param>
        public static void RegisterFactories(string appName, IApplicationInitialize initializer)
        {
            initializer.Register<IApplicationSettingsAdapterFactory>(new ApplicationSettingsAdapterFactory());
            initializer.Register<IEmailAdapterFactory>(new EmailAdapterFactory());
            initializer.Register<IReceiveEmailAdapterFactory>(new OpenPop3AdapterFactory());
            initializer.Register<IRegularExpressionAdapterFactory>(new RegularExpressionAdapterFactory());
            initializer.Register<ISqlAdapterFactory>(new SqlServerAdapterFactory());
            initializer.Register<IStoredProcedureAdapterFactory>(new StoredProcedureAdapterFactory());
            initializer.Register<IJsonAdapterFactory>(new JsonAdapterFactory());
            initializer.Register<IMessageQueueAdapterFactory>(new MessageQueueAdapterFactory());
            initializer.Register<IFileDbAdapterFactory>(new FileDbAdapterFactory());
            initializer.Register<IAwsFileStorageAdapterFactory>(new AwsAdapterFactory());
            initializer.Register<ICommentLogAdapterFactory>(new ConversationLogAdapterFactory());
            initializer.Register<IHttpRequestAdapterFactory>(new HttpRequestAdapterFactory());
            initializer.Register<IFileSystemAdapterFactory>(new FileSystemAdapterFactory());
            initializer.Register<ISmsAdapterFactory>(new BananaSmsAdapterFactory());
            initializer.Register<IMethodInvokeAdapterFactory>(new DirectMethodCallAdapterFactory());
            initializer.Register<IEnvironmentAdapterFactory>(new EnvironmentAdapterFactory());
            initializer.Register<IDataEncryptionAdapterFactory>(new DataEncryptionAdapterFactory());
            initializer.Register<IBase64EncodingAdapterFactory>(new Base64EncodingAdapterFactory());
            initializer.Register<IOpenXmlDocumentAdapterFactory>(new OpenXmlDocumentAdapterFactory());
            initializer.Register<ISecurityEventLogAdapterFactory>(new SecurityEventLogAdapterFactory());
        }
    }
}
