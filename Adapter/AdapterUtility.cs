﻿namespace Adapter
{
    using LqbGrammar;
    using LqbGrammar.Queries;

    /// <summary>
    /// Sometimes adapters would like to access functionality that
    /// has been placed in higher libraries.  As an alternative to
    /// moving such code which may result in massive changes, the
    /// functionality is implemented here using code appropriate to
    /// this level.  Since a lot of the desired functionality is
    /// implemented in drivers which are available via a service
    /// locator, we can make use of drivers.  This is an advantage
    /// of the loose coupling via the LqbGrammar interfaces.
    /// </summary>
    internal static class AdapterUtility
    {
        /// <summary>
        /// Look up a value from the stage configuration.
        /// </summary>
        /// <param name="name">The name of the value being looked up.</param>
        /// <returns>The stage configuration value matching the look-up name.</returns>
        public static string GetStageConfigValue(string name)
        {
            var factory = GenericLocator<IConfigurationQueryFactory>.Factory;
            var driver = factory.CreateStageConfiguration();

            var configuration = driver.ReadAllValues();

            var tuple = configuration.ContainsKey(name) ? configuration[name] : null;
            return tuple?.Item2;
        }

        /// <summary>
        /// Look up an integer value from the stage configuration.
        /// </summary>
        /// <param name="name">The name of the value being looked up.</param>
        /// <returns>The stage configuration value matching the look-up name, or null.</returns>
        public static int? GetStageConfigIntegerValue(string name)
        {
            var factory = GenericLocator<IConfigurationQueryFactory>.Factory;
            var driver = factory.CreateStageConfiguration();

            var configuration = driver.ReadAllValues();

            var tuple = configuration.ContainsKey(name) ? configuration[name] : null;
            return tuple?.Item1;
        }

        /// <summary>
        /// Look up a feature flag's value from the stage configuration.
        /// </summary>
        /// <param name="name">The name of the value being looked up.</param>
        /// <returns>The stage configuration value matching the look-up name.</returns>
        public static bool GetStageConfigFeatureFlag(string name)
        {
            var flag = GetStageConfigIntegerValue(name);
            return flag == null ? false : flag.Value == 1;
        }
    }
}
