﻿namespace Adapter
{
    using System;
    using LqbGrammar.Adapters;

    /// <summary>
    /// Create adapters that are used to work with local files.
    /// </summary>
    public sealed class FileSystemAdapterFactory : IFileSystemAdapterFactory
    {
        /// <summary>
        /// Create a adapter used to work with binary files.
        /// </summary>
        /// <returns>A binary file adapter.</returns>
        public IBinaryFileAdapter CreateBinaryFileAdapter()
        {
            return new BinaryFileAdapter();
        }

        /// <summary>
        /// Create a adapter used to carry out file operations.
        /// </summary>
        /// <returns>A file operation adapter.</returns>
        public IFileOperationAdapter CreateFileOperationAdapter()
        {
            return new FileOperationAdapter();
        }

        /// <summary>
        /// Create a adapter used to work with text files.
        /// </summary>
        /// <returns>A text file adapter.</returns>
        public ITextFileAdapter CreateTextFileAdapter()
        {
            return new TextFileAdapter();
        }

        /// <summary>
        /// Create a adapter used to work with XML files.
        /// </summary>
        /// <returns>An XML file adapter.</returns>
        public IXmlFileAdapter CreateXmlFileAdapter()
        {
            return new XmlFileAdapter();
        }
    }
}
