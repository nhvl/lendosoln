﻿namespace Adapter
{
    using System;
    using LqbGrammar.Adapters;

    /// <summary>
    /// Factory for creating adapter for the System.Environment class.
    /// </summary>
    public sealed class EnvironmentAdapterFactory : IEnvironmentAdapterFactory
    {
        /// <summary>
        /// Keep a singleton as the adapter is wrapping a static class.
        /// </summary>
        private static readonly IEnvironmentAdapter Adapter;

        /// <summary>
        /// Initializes static members of the <see cref="EnvironmentAdapterFactory" /> class.
        /// </summary>
        static EnvironmentAdapterFactory()
        {
            Adapter = new EnvironmentAdapter();
        }

        /// <summary>
        /// Create an implementation of the IEnvironmentAdapter interface.
        /// </summary>
        /// <returns>An implementation of the IEnvironmentAdapter interface.</returns>
        public IEnvironmentAdapter Create()
        {
            return Adapter;
        }
    }
}
