﻿namespace Adapter
{
    using System;
    using FileDB3;
    using LqbGrammar.Adapters;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Adapter that encapsulates the FileDB system.
    /// </summary>
    internal sealed class FileDbAdapter : IFileDbAdapter
    {
        /// <summary>
        /// Save a file that was previously retrieved from the file storage using the UseFile method.
        /// </summary>
        /// <param name="location">The location of the file storage repository.</param>
        /// <param name="key">The file identifier used to retrieve the file later, this can be the same as or different from the key passed into the UseFile method.</param>
        /// <param name="encryptionKey">Encryption key to use, or null.</param>
        /// <param name="token">A file token that holds the location of the file in the local environment.</param>
        public void ReSaveFile(FileStorageIdentifier location, FileIdentifier key, byte[] encryptionKey, IFileToken token)
        {
            try
            {
                if (token is FileDbToken)
                {
                    var fileToken = token as FileDbToken;
                    FileHandle handle = fileToken.GetHandle();
                    if (handle == null)
                    {
                        throw new DeveloperException(ErrorMessage.SystemError);
                    }

                    var filedb = new FileDB();
                    filedb.PutFile(location.ToString(), key.ToString(), handle, encryptionKey);
                }
                else
                {
                    throw new DeveloperException(ErrorMessage.SystemError);
                }
            }
            catch (LqbException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new DeveloperException(ErrorMessage.SystemError, ex);
            }
        }

        /// <summary>
        /// Retrieve a file from the file storage.
        /// </summary>
        /// <param name="location">The location of the file storage repository.</param>
        /// <param name="key">The file identifier previously used to save the file.</param>
        /// <param name="encryptionKey">Encryption key to use, or null.</param>
        /// <param name="readFileHandler">A delegate which will read the file from the location provided in the LocalFilePath.</param>
        public void RetrieveFile(FileStorageIdentifier location, FileIdentifier key, byte[] encryptionKey, Action<LocalFilePath> readFileHandler)
        {
            try
            {
                var filedb = new FileDB();
                using (var handle = filedb.GetFile(location.ToString(), key.ToString(), encryptionKey))
                {
                    if (handle == null)
                    {
                        throw new System.IO.FileNotFoundException(string.Format("(location, string) = ({0}, {1}) not found", location.ToString(), key.ToString()));
                    }

                    LocalFilePath localPath = this.GetLocalPath(handle);

                    readFileHandler(localPath);
                }
            }
            catch (LqbException)
            {
                throw;
            }
            catch (System.IO.FileNotFoundException)
            {
                // Our system expect FileNotFoundException for non-existing file.
                throw;
            }
            catch (Exception ex)
            {
                throw new DeveloperException(ErrorMessage.SystemError, ex);
            }
        }

        /// <summary>
        /// Save a new file into the file storage.
        /// </summary>
        /// <param name="location">The location of the file storage repository.</param>
        /// <param name="key">The file identifier used to retrieve the file later.</param>
        /// <param name="encryptionKey">Encryption key to use, or null.</param>
        /// <param name="saveFileHandler">A delegate which will save the file to the location provided in the LocalFilePath.</param>
        public void SaveNewFile(FileStorageIdentifier location, FileIdentifier key, byte[] encryptionKey, Action<LocalFilePath> saveFileHandler)
        {
            try
            {
                var filedb = new FileDB();
                using (var handle = filedb.NewFileHandle())
                {
                    var localPath = this.GetLocalPath(handle);

                    saveFileHandler(localPath);
                    
                    filedb.PutFile(location.ToString(), key.ToString(), handle, encryptionKey);
                }
            }
            catch (LqbException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new DeveloperException(ErrorMessage.SystemError, ex);
            }
        }

        /// <summary>
        /// Retrieve a file from the file storage and return a token that can be used to re-store the file.
        /// </summary>
        /// <param name="location">The location of the file storage repository.</param>
        /// <param name="key">The file identifier previously used to save the file.</param>
        /// <param name="encryptionKey">Encryption key to use, or null.</param>
        /// <param name="useFileHandler">A delegate which will read the file from the location provided in the IFileToken, and can re-store the file with this token.</param>
        public void UseFile(FileStorageIdentifier location, FileIdentifier key, byte[] encryptionKey, Action<IFileToken> useFileHandler)
        {
            try
            {
                var filedb = new FileDB();
                using (var handle = filedb.GetFile(location.ToString(), key.ToString(), encryptionKey))
                {
                    if (handle == null)
                    {
                        throw new System.IO.FileNotFoundException(string.Format("(location, string) = ({0}, {1}) not found", location.ToString(), key.ToString()));
                    }

                    var token = new FileDbToken(handle, location, key, encryptionKey);
                    try
                    {
                        useFileHandler(token);
                    }
                    finally
                    {
                        token.MakeInvalid();
                    }
                }
            }
            catch (LqbException)
            {
                throw;
            }
            catch (System.IO.FileNotFoundException)
            {
                // Our system expect FileNotFoundException for non-existing file.
                throw;
            }
            catch (Exception ex)
            {
                throw new DeveloperException(ErrorMessage.SystemError, ex);
            }
        }

        /// <summary>
        /// Delete the file from the file storage.
        /// </summary>
        /// <param name="location">The location of the file storage repository.</param>
        /// <param name="key">The file identifier used to delete.</param>
        public void DeleteFile(FileStorageIdentifier location, FileIdentifier key)
        {
            var filedb = new FileDB();
            filedb.DeleteFile(location.ToString(), key.ToString());
        }

        /// <summary>
        /// Check whether the given file exist in the file storage.
        /// </summary>
        /// <param name="location">The location of the file storage repository.</param>
        /// <param name="key">The file identifier.</param>
        /// <returns>True if the file exist false otherwise.</returns>
        public bool FileExists(FileStorageIdentifier location, FileIdentifier key)
        {
            var filedb = new FileDB();
            return filedb.FileExists(location.ToString(), key.ToString());
        }

        /// <summary>
        /// Extract the local file name from the handle and return a validated path.
        /// </summary>
        /// <param name="handle">A file handle from the FileDB system.</param>
        /// <returns>A validated local file path.</returns>
        private LocalFilePath GetLocalPath(FileHandle handle)
        {
            LocalFilePath? localPath = LocalFilePath.Create(handle.LocalFileName);
            if (localPath != null)
            {
                return localPath.Value;
            }
            else
            {
                throw new DeveloperException(ErrorMessage.SystemError);
            }
        }
    }
}
