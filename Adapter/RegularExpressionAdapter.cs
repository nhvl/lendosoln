﻿namespace Adapter
{
    using LqbGrammar.Adapters;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Adapter to the regular expression library in the .net framework.
    /// </summary>
    internal sealed class RegularExpressionAdapter : IRegularExpressionAdapter
    {
        /// <summary>
        /// Check a string against a regular expression.
        /// </summary>
        /// <param name="regex">The regular expression.</param>
        /// <param name="validationTarget">The string to be checked.</param>
        /// <returns>True if string matches the regular expression, false otherwise.</returns>
        public bool IsMatch(RegularExpressionString regex, string validationTarget)
        {
            var regex2 = RegularExpressionStore.RetrieveForMatch(regex);
            return regex2.IsMatch(validationTarget);
        }

        /// <summary>
        /// In a specified input string, replaces all strings that match a specified regular expression with a specified replacement string.
        /// </summary>
        /// <param name="regex">The regular expression.</param>
        /// <param name="input">The string to search for a match.</param>
        /// <param name="replacement">The replacement string.</param>
        /// <returns>
        /// A new string that is identical to the input string, except that the replacement string takes the place of each matched string.
        /// If pattern is not matched in the current instance, the method returns the current instance unchanged.
        /// </returns>
        public string Replace(RegularExpressionString regex, string input, string replacement)
        {
            var regex2 = RegularExpressionStore.RetrieveForSearch(regex);
            return regex2.Replace(input, replacement);
        }
    }
}
