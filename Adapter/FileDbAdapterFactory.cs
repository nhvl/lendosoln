﻿namespace Adapter
{
    using System;
    using LqbGrammar.Adapters;

    /// <summary>
    /// Factory class for creating implementations of the IFileDbAdapter interface.
    /// </summary>
    public class FileDbAdapterFactory : IFileDbAdapterFactory
    {
        /// <summary>
        /// Create an implementation of the IFileDbAdapter interface.
        /// </summary>
        /// <returns>An implementation of the IFileDbAdapter interface.</returns>
        public IFileDbAdapter Create()
        {
            return new FileDbAdapter();
        }
    }
}
