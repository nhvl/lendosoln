﻿namespace Adapter
{
    using System;
    using System.Collections.Generic;
    using ConversationLogLib.Client;
    using ConversationLogLib.Interface;
    using LqbGrammar.Adapters;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers.ConversationLog;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Adapter to interface with the conversation log service.
    /// Note - Any interface method should be wrapped to prevent non LqbExceptions from escaping.
    /// </summary>
    internal sealed class ConversationLog : ICategoryAdapter, ICommentAdapter
    {
        /// <summary>
        /// Use the transport class to implement the methods.
        /// </summary>
        private ConversationLogClient client;

        /// <summary>
        /// Initializes a new instance of the <see cref="ConversationLog"/> class.
        /// </summary>
        /// <param name="location">The location of the service.</param>
        /// <param name="port">The port on which the service is hosted.</param>
        public ConversationLog(string location, int port)
        {
            this.client = new ConversationLogClient(location, port);
        }

        /// <summary>
        /// Retrieve all the categories relevant to the logged in user, in the current order.
        /// </summary>
        /// <param name="secToken">Security token for the authenticated principal making this method call.</param>
        /// <returns>The list of categories.</returns>
        public List<Category> GetAllCategories(SecurityToken secToken)
        {
            return this.CallMethodAndWrapAnyNonLqbException<List<Category>>(() =>
            {
                var data = this.client.GetAllCategories(secToken, secToken.BrokerId.ToString());
                if (data == null)
                {
                    throw new ServerException(ErrorMessage.SystemError);
                }

                var list = new List<Category>(data.Count);
                foreach (var item in data)
                {
                    Category cat = new Category();
                    cat.Identity = new CategoryReference();
                    cat.Identity.Id = item.Id;
                    cat.Identity.Name = CommentCategoryName.Create(item.Name).Value;
                    cat.Identity.OwnerId = CommentCategoryNamespace.Create(item.OwnerId).Value;
                    cat.IsActive = item.Active;
                    cat.DisplayName = CommentCategoryName.Create(item.DisplayName).Value;
                    cat.DefaultPermissionLevelId = item.DefaultPermissionLevelId;
                    list.Add(cat);
                }

                return list;
            });
        }

        /// <summary>
        /// Retrieve all conversations that have been attached to an LQB resource.
        /// </summary>
        /// <param name="secToken">Security token for the authenticated principal making this method call.</param>
        /// <param name="resource">The LQB resource.</param>
        /// <returns>All conversations.  The order is determined by the category order, followed by the creation dates of the conversations.</returns>
        public IEnumerable<Conversation> GetAllConversations(SecurityToken secToken, ResourceId resource)
        {
            return this.CallMethodAndWrapAnyNonLqbException<IEnumerable<Conversation>>(() =>
            {
                var resData = this.ConvertResource(resource);

                var data = this.client.GetAllConversations(secToken, resData);
                if (data == null)
                {
                    throw new ServerException(ErrorMessage.SystemError);
                }

                var list = new List<Conversation>(data.Count);
                foreach (var conv in data)
                {
                    var conversation = new Conversation();
                    conversation.Category = new CategoryReference();
                    conversation.Category.Id = conv.CategoryId;
                    conversation.Category.Name = CommentCategoryName.Create(conv.CategoryName).Value;
                    conversation.Category.OwnerId = CommentCategoryNamespace.Create(conv.OwnerId).Value;
                    conversation.PermissionLevelId = conv.PermissionLevelId;

                    var commentDictionary = new Dictionary<long, CommentData>();
                    foreach (var inputComm in conv.Comments)
                    {
                        commentDictionary[inputComm.Id] = inputComm;
                    }

                    var comList = new List<Comment>(conv.Comments.Count);
                    this.ProcessTreeNode(comList, conv.FirstPost, commentDictionary, 0);

                    conversation.Comments = comList;
                    list.Add(conversation);
                }

                return list;
            });
        }

        /// <summary>
        /// Gets the permission level id for the specified comment.
        /// </summary>
        /// <param name="securityToken">Security token for the calling principal.</param>
        /// <param name="commentId">The id of the comment we want the permission level of.</param>
        /// <returns>The permission level id of the conversation that the comment belongs to, or null if it hasn't been set to the default by migration.</returns>
        public long? GetPermissionLevelIdByCommentReference(SecurityToken securityToken, long commentId)
        {
            return this.CallMethodAndWrapAnyNonLqbException<long?>(() =>
            {
                return this.client.GetPermissionLevelIdByCommentReference(securityToken, commentId);
            });
        }

        /// <summary>
        /// Post a new comment to a category, beginning a new conversation.
        /// </summary>
        /// <param name="secToken">Security token for the authenticated principal making this method call.</param>
        /// <param name="resource">The LQB resource.</param>
        /// <param name="category">The category under which this comment is posted.</param>
        /// <param name="comment">The comment data.</param>
        /// <param name="permissionLevelId">The id of the permission level associated with the conversation, or null if not yet migrated.</param>
        public void Post(SecurityToken secToken, ResourceId resource, CategoryReference category, Comment comment, long? permissionLevelId)
        {
            this.CallMethodAndWrapAnyNonLqbException(() =>
            {
                var resData = this.ConvertResource(resource);
                var content = this.ConvertComment(comment);

                var conversation = this.client.BeginConveration(secToken, resData, category.Id, content, permissionLevelId);
                if (conversation == null)
                {
                    throw new ServerException(ErrorMessage.SystemError);
                }

                this.PopulateComment(comment, conversation.Comments[0], 0);
            });
        }

        /// <summary>
        /// Reply to an existing comment in the conversation log system. <para/>
        /// Watchout, the depth of the reply is not accurate.  For that use GetAllComments.
        /// </summary>
        /// <param name="secToken">Security token for the authenticated principal making this method call.</param>
        /// <param name="parent">The comment toward which this reply is directed.</param>
        /// <param name="comment">The comment data that is the reply.</param>
        public void Reply(SecurityToken secToken, CommentReference parent, Comment comment)
        {
            this.CallMethodAndWrapAnyNonLqbException(() =>
            {
                var content = this.ConvertComment(comment);
                var data = this.client.MakeReply(secToken, parent.Id, content);
                if (data == null)
                {
                    throw new ServerException(ErrorMessage.SystemError);
                }

                this.PopulateComment(comment, data, 0);
            });
        }

        /// <summary>
        /// Hides an existing comment.
        /// </summary>
        /// <param name="securityToken">Security token for the calling principal.</param>
        /// <param name="commentId">An identifier of the comment to be hidden.</param>
        /// <returns>True iff it succeeded.</returns>
        public bool HideComment(SecurityToken securityToken, CommentReference commentId)
        {
            return this.CallMethodAndWrapAnyNonLqbException(() =>
            {
                return this.client.HideComment(securityToken, commentId.Id);
            });
        }

        /// <summary>
        /// Unhides an existing comment.
        /// </summary>
        /// <param name="securityToken">Security token for the calling principal.</param>
        /// <param name="commentId">An identifier of the comment to be unhidden.</param>
        /// <returns>True iff it succeeded.</returns>
        public bool ShowComment(SecurityToken securityToken, CommentReference commentId)
        {
            return this.CallMethodAndWrapAnyNonLqbException(() =>
            {
                return this.client.ShowComment(securityToken, commentId.Id);
            });
        }

        /// <summary>
        /// Either create a new category or modify and existing category.
        /// </summary>
        /// <param name="secToken">Security token for the authenticated principal making this method call.</param>
        /// <param name="category">A category in the comment log system.</param>
        /// <returns>The current state of the category in the log system after the method has completed.</returns>
        public Category SetCategory(SecurityToken secToken, Category category)
        {
            return this.CallMethodAndWrapAnyNonLqbException<Category>(() =>
            {
                var data = this.client.InsertOrUpdateCategory(secToken, category.Identity.OwnerId.ToString(), category.Identity.Name.ToString(), category.IsActive, category.DisplayName.ToString(), category.DefaultPermissionLevelId);
                if (data == null)
                {
                    throw new ServerException(ErrorMessage.SystemError);
                }

                return this.ConvertCategory(data);
            });
        }

        /// <summary>
        /// Set the order in which categories are to be presented to users of the comment system.
        /// </summary>
        /// <param name="secToken">Security token for the authenticated principal making this method call.</param>
        /// <param name="categoryOrder">An ordered list of category references, the order will be reflected in subsequent GUI views.</param>
        public void SetCategoryOrder(SecurityToken secToken, List<CategoryReference> categoryOrder)
        {
            this.CallMethodAndWrapAnyNonLqbException(() =>
            {
                string ownerId = null;
                var listIDs = new List<long>();
                foreach (var item in categoryOrder)
                {
                    if (ownerId == null)
                    {
                        ownerId = item.OwnerId.ToString();
                    }

                    listIDs.Add(item.Id);
                }

                bool good = this.client.SetCategoryOrder(secToken, ownerId, listIDs);
                if (!good)
                {
                    throw new ServerException(ErrorMessage.SystemError);
                }
            });
        }

        /// <summary>
        /// Recursive method that takes the conversation tree and generates a depth-first ordered set of comments.
        /// </summary>
        /// <param name="comList">The list that will receive the comments as they are created.</param>
        /// <param name="node">The current node of the conversation tree.</param>
        /// <param name="dictionary">A map from comment identifier to comment.</param>
        /// <param name="depth">The depth of the node relative to the top.</param>
        private void ProcessTreeNode(List<Comment> comList, CommentNode node, Dictionary<long, CommentData> dictionary, int depth)
        {
            long id = node.CommentId;
            var data = dictionary[id];
            var comment = new Comment();
            this.PopulateComment(comment, data, depth);
            comList.Add(comment);

            if (node.ChildNodes != null)
            {
                foreach (var child in node.ChildNodes)
                {
                    this.ProcessTreeNode(comList, child, dictionary, depth + 1);
                }
            }
        }

        /// <summary>
        /// Transfer data from inComment to the comment.
        /// Note that this method can throw exceptions from System namespace.
        /// </summary>
        /// <param name="comment">The comment to be populated.</param>
        /// <param name="inputComment">The source of the comment data.</param>
        /// <param name="depth">The depth of the node relative to the top.</param>
        private void PopulateComment(Comment comment, CommentData inputComment, int depth)
        {
            if (comment == null)
            {
                throw new ArgumentNullException(nameof(comment));
            }

            comment.Identity = new CommentReference();

            if (inputComment == null)
            {
                throw new ArgumentNullException(nameof(inputComment));
            }

            comment.Identity.Id = inputComment.Id;

            comment.Created = this.ConvertCreatedUTC(inputComment.CreatedUTC);

            // at some point it might be better to have a separate Create method that throws.
            var personName = PersonName.CreateWithValidation(inputComment.Fullname);
            if (personName == null)
            {
                throw new InvalidOperationException($"The fullname {inputComment.Fullname} could not be converted to a valid PersonName.");
            } 

            comment.Commenter = personName.Value;

            var isHidden = inputComment.HiddenByPersonId.HasValue;
            comment.IsHidden = isHidden;
            if (isHidden)
            {
                var hiderName = PersonName.CreateWithValidation(inputComment.HiderFullname);
                if (!hiderName.HasValue)
                {
                    throw new InvalidOperationException($"The fullname {inputComment.HiderFullname} could not be converted to a valid PersonName.");
                }

                comment.HiderFullName = hiderName;
            }

            var userId = UserAccountIdentifier.Create(inputComment.CommenterId);
            if (userId == null)
            {
                throw new InvalidOperationException($"The id {inputComment.CommenterId} could not be converted to a valid user account identifier.");
            }

            comment.CommenterId = userId.Value;

            if (inputComment.Content == null)
            {
                throw new ArgumentNullException(nameof(inputComment) + "." + nameof(inputComment.Content));
            }

            string text = EncryptionManager.Decrypt(inputComment.Content.EncryptedText);
            
            var commentLogEntry = CommentLogEntry.Create(text);
            if (commentLogEntry == null)
            {
                throw new InvalidOperationException($"The text {text} could not be converted to a valid commont log entry.");
            }

            comment.Value = commentLogEntry.Value;

            var calculatedDepth = Depth.Create(depth);
            if (calculatedDepth == null)
            {
                throw new InvalidOperationException($"The int depth {depth} could not be converted to a valid depth.");
            }

            comment.Depth = calculatedDepth.Value;
        }

        /// <summary>
        /// Convert a ResourceId to the data transport class ResourceData.
        /// </summary>
        /// <param name="resource">The ResourceId instance.</param>
        /// <returns>A ResourceData instance.</returns>
        private ResourceData ConvertResource(ResourceId resource)
        {
            var resData = new ResourceData();
            resData.Identifier = resource.Identifier.ToString();
            resData.ResourceType = resource.Type.ToString();
            return resData;
        }

        /// <summary>
        /// Convert a creation date stored in UTC format to an LqbEventDate instance.
        /// </summary>
        /// <param name="utc">A datetime in UTC format.</param>
        /// <returns>An instance of LqbEventDate.</returns>
        private LqbEventDate ConvertCreatedUTC(DateTime utc)
        {
            var lqbEventDate = LqbEventDate.Create(utc);

            if (lqbEventDate == null)
            {
                throw new InvalidOperationException($"The utc time {utc} could not be converted to a lqb event date.");
            }

            return lqbEventDate.Value;
        }
        
        /// <summary>
        /// Convert the contained data in a Comment and place it into a CommentContent instance.
        /// </summary>
        /// <param name="comment">An instance of the Comment class.</param>
        /// <returns>An instance of the data transport class CommentContent.</returns>
        private CommentContent ConvertComment(Comment comment)
        {
            var content = new CommentContent();
            content.EncryptedText = EncryptionManager.Encrypt(comment.Value.ToString());
            return content;
        }

        /// <summary>
        /// Convert an instance of the data transport class CategoryData to a Category instance.
        /// </summary>
        /// <param name="data">An instance of CategoryData.</param>
        /// <returns>An instance of Category.</returns>
        private Category ConvertCategory(CategoryData data)
        {
            var cat = new Category();
            cat.Identity = new CategoryReference();
            cat.Identity.Id = data.Id;
            cat.Identity.Name = CommentCategoryName.Create(data.Name).Value;
            cat.Identity.OwnerId = CommentCategoryNamespace.Create(data.OwnerId).Value;
            cat.IsActive = data.Active;
            cat.DisplayName = CommentCategoryName.Create(data.DisplayName).Value;
            cat.DefaultPermissionLevelId = data.DefaultPermissionLevelId;
            return cat;
        }

        /// <summary>
        /// Calls the method and retuns it's result, wrapping any non <see cref="LqbException"/> as a <see cref="DeveloperException"/>.
        /// </summary>
        /// <typeparam name="TResult">The result type returned.</typeparam>
        /// <param name="methodToCall">The method to be called.</param>
        /// <returns>The returned value of the method that gets called.</returns>
        private TResult CallMethodAndWrapAnyNonLqbException<TResult>(Func<TResult> methodToCall)
        {
            try
            {
                return methodToCall();
            }
            catch (Exception ex) when(!(ex is LqbException))
            {
                throw new DeveloperException(ErrorMessage.SystemError, ex);
            }
        }

        /// <summary>
        /// Calls the method and wraps any non <see cref="LqbException"/> as a <see cref="DeveloperException"/>.
        /// </summary>
        /// <param name="action">The action to call.</param>
        private void CallMethodAndWrapAnyNonLqbException(Action action)
        {
            try
            {
                action();
            }
            catch (Exception ex) when (!(ex is LqbException))
            {
                throw new DeveloperException(ErrorMessage.SystemError, ex);
            }
        }
    }
}
