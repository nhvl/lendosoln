﻿namespace Adapter.Logger
{
    using System.Collections.Generic;
    using LqbGrammar.Adapters;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;

    /// <summary>
    /// Utility class for creating target info structures for the logging system.
    /// Although this is exposed as public, client code should use the LoggingHelper class,
    /// which calls this class.  However, code in a module lower than LendersOfficeLib
    /// (e.g., CommonProjectLib) can call these methods directly.  This may require adding
    /// a reference to the Adapter project to the low-level project, which is ok as the
    /// adapter library is intended to be a low-level wrapper around external libraries.
    /// Alternatively, we may instead move the LoggingTargetInfo classes to LqbGrammar, but
    /// that means polluting LqbGrammar with implementation details of how logging works.
    /// </summary>
    public static class LoggingTargetInfoFactory
    {
        /// <summary>
        /// Create data structure for the email logger.
        /// </summary>
        /// <param name="server">The email server to use when sending the email.</param>
        /// <param name="portNumber">The port number to use when connecting to the server.</param>
        /// <param name="emailFrom">The address from which the email is sent.</param>
        /// <param name="emailTo">List of addresses to which the email is sent.</param>
        /// <returns>Data structure for the email logger.</returns>
        public static LoggingTargetInfo CreateEmailInfo(EmailServerName server, PortNumber portNumber, EmailAddress emailFrom, List<EmailAddress> emailTo)
        {
            return new EmailTargetData(server, portNumber, emailFrom, emailTo);
        }

        /// <summary>
        /// Create data structure for the msmq logger.
        /// </summary>
        /// <param name="path">The path to the message queue.</param>
        /// <returns>Data structure for the msmq logger.</returns>
        public static LoggingTargetInfo CreateMsmqInfo(MessageQueuePath path)
        {
            return new MsmqTargetData(path);
        }

        /// <summary>
        /// Retrieve a logging adapter appropriate to the logging target.
        /// </summary>
        /// <param name="target">The logging target.</param>
        /// <returns>An appropriate logging adapter.</returns>
        public static ILoggingAdapter GetAdapter(LoggingTargetInfo target)
        {
            if (target is EmailTargetData)
            {
                var item = (EmailTargetData)target;
                return item.CreateAssociatedAdapter();
            }
            else if (target is MsmqTargetData)
            {
                var item = (MsmqTargetData)target;
                return item.CreateAssociatedAdapter();
            }
            else
            {
                return null;
            }
        }
    }
}
