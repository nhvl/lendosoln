﻿namespace Adapter.Logger
{
    using LqbGrammar.Adapters;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;

    /// <summary>
    /// Data for sending logging message to queue.
    /// </summary>
    internal sealed class MsmqTargetData : LoggingTargetInfo
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MsmqTargetData"/> class.
        /// </summary>
        /// <param name="path">The path to the message queue.</param>
        public MsmqTargetData(MessageQueuePath path)
        {
            this.Path = path;
        }

        /// <summary>
        /// Gets or sets the path to the message queue.
        /// </summary>
        /// <value>The path to the message queue.</value>
        private MessageQueuePath Path { get; set; }

        /// <summary>
        /// Create and return an adapter that is associated with the particular log service.
        /// </summary>
        /// <returns>An implementation of the ILoggingAdapter interface.</returns>
        public ILoggingAdapter CreateAssociatedAdapter()
        {
            return new MsmqLoggingAdapter(this.Path);
        }
    }
}
