﻿namespace Adapter.Logger
{
    using System.Collections.Generic;
    using LqbGrammar.Adapters;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;

    /// <summary>
    /// Data for sending emails.
    /// </summary>
    internal sealed class EmailTargetData : LoggingTargetInfo
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EmailTargetData"/> class.
        /// </summary>
        /// <param name="server">The email server to use when sending the email.</param>
        /// <param name="portNumber">The port number the server is listening on for request.</param>
        /// <param name="emailFrom">The address from which the email is sent.</param>
        /// <param name="emailTo">List of addresses to which the email is sent.</param>
        public EmailTargetData(EmailServerName server, PortNumber portNumber, EmailAddress emailFrom, List<EmailAddress> emailTo)
        {
            this.Server = server;
            this.Sender = emailFrom;
            this.Recipients = emailTo;
            this.PortNumber = portNumber;
        }

        /// <summary>
        /// Gets or sets the email server's port number that it is listening to.
        /// </summary>
        /// <value>The port number to use with the email server.</value>
        private PortNumber PortNumber { get; set; }

        /// <summary>
        /// Gets or sets the server used to send the email.
        /// </summary>
        /// <value>The server used to send the email.</value>
        private EmailServerName Server { get; set; }

        /// <summary>
        /// Gets or sets the address(es) to which the email will be sent.
        /// </summary>
        /// <value>The address to which the email will be sent.</value>
        private List<EmailAddress> Recipients { get; set; }

        /// <summary>
        /// Gets or sets the address from which the email will be sent.
        /// </summary>
        /// <value>The address from which the email will be sent.</value>
        private EmailAddress Sender { get; set; }

        /// <summary>
        /// Create and return an adapter that is associated with the particular log service.
        /// </summary>
        /// <returns>An implementation of the ILoggingAdapter interface.</returns>
        public ILoggingAdapter CreateAssociatedAdapter()
        {
            return new EmailLoggingAdapter(this.Server, this.PortNumber, this.Sender, this.Recipients);
        }
    }
}
