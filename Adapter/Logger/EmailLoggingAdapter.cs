﻿namespace Adapter.Logger
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using LqbGrammar.Adapters;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Utils;

    /// <summary>
    /// Send the log messages to an email recipient.
    /// </summary>
    internal sealed class EmailLoggingAdapter : ILoggingAdapter
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EmailLoggingAdapter"/> class.
        /// </summary>
        /// <param name="server">The email server to use when sending the email.</param>
        /// <param name="portNumber">The port number to connect to.</param>
        /// <param name="emailFrom">The address from which the email is sent.</param>
        /// <param name="emailTo">List of addresses to which the email is sent.</param>
        public EmailLoggingAdapter(EmailServerName server, PortNumber portNumber, EmailAddress emailFrom, List<EmailAddress> emailTo)
        {
            this.Server = server;
            this.Sender = emailFrom;
            this.Recipients = emailTo;
            this.PortNumber = portNumber;
        }

        /// <summary>
        /// Gets or sets the port number to connect to.
        /// </summary>
        private PortNumber PortNumber { get; set; }

        /// <summary>
        /// Gets or sets the server used to send the email.
        /// </summary>
        /// <value>The server used to send the email.</value>
        private EmailServerName Server { get; set; }

        /// <summary>
        /// Gets or sets the address(es) to which the email will be sent.
        /// </summary>
        /// <value>The address to which the email will be sent.</value>
        private List<EmailAddress> Recipients { get; set; }

        /// <summary>
        /// Gets or sets the address from which the email will be sent.
        /// </summary>
        /// <value>The address from which the email will be sent.</value>
        private EmailAddress Sender { get; set; }

        /// <summary>
        /// Log a bug report.
        /// </summary>
        /// <param name="message">The parameter is not used..</param>
        /// <param name="boundaryType">The parameter is not used.</param>
        /// <param name="properties">The parameter is not used.</param>
        public void LogBug(LogMessage message, Type boundaryType, Dictionary<LogPropertyName, LogPropertyValue> properties)
        {
            // Only email errors.
            return;
        }

        /// <summary>
        /// Log debug information.
        /// </summary>
        /// <param name="message">The parameter is not used..</param>
        /// <param name="boundaryType">The parameter is not used.</param>
        /// <param name="properties">The parameter is not used.</param>
        public void LogDebug(LogMessage message, Type boundaryType, Dictionary<LogPropertyName, LogPropertyValue> properties)
        {
            // Only email errors.
            return;
        }

        /// <summary>
        /// Log an error message.
        /// </summary>
        /// <param name="message">The message to log.</param>
        /// <param name="boundaryType">Type of class that is used as the starting point for logging messages.</param>
        /// <param name="properties">Optional set of properties that will be included in the log.</param>
        public void LogError(LogMessage message, Type boundaryType, Dictionary<LogPropertyName, LogPropertyValue> properties)
        {
            var stack = new CallStack(boundaryType);
            var subjectAndBody = GenerateEmailSubjectAndBody(message, stack, stack.StackTrace, properties);

            this.LogError(subjectAndBody.Item1, subjectAndBody.Item2);
        }

        /// <summary>
        /// Log an error message using the input stack trace rather than pulling it from the call stack.
        /// </summary>
        /// <param name="message">The message to log.</param>
        /// <param name="stackTrace">Stack trace to use in the log.</param>
        /// <param name="properties">Optional set of properties that will be included in the log.</param>
        public void LogError(LogMessage message, string stackTrace, Dictionary<LogPropertyName, LogPropertyValue> properties)
        {
            var subjectAndBody = GenerateEmailSubjectAndBody(message, null, stackTrace, properties);
            this.LogError(subjectAndBody.Item1, subjectAndBody.Item2);
        }

        /// <summary>
        /// Log an informational message.
        /// </summary>
        /// <param name="message">The parameter is not used..</param>
        /// <param name="boundaryType">The parameter is not used.</param>
        /// <param name="properties">The parameter is not used.</param>
        public void LogInfo(LogMessage message, Type boundaryType, Dictionary<LogPropertyName, LogPropertyValue> properties)
        {
            // Only email errors.
            return;
        }

        /// <summary>
        /// Log a trace message.
        /// </summary>
        /// <param name="message">The parameter is not used..</param>
        /// <param name="boundaryType">The parameter is not used.</param>
        /// <param name="properties">The parameter is not used.</param>
        public void LogTrace(LogMessage message, Type boundaryType, Dictionary<LogPropertyName, LogPropertyValue> properties)
        {
            // Only email errors.
            return;
        }

        /// <summary>
        /// Log a warning message.
        /// </summary>
        /// <param name="message">The parameter is not used..</param>
        /// <param name="boundaryType">The parameter is not used.</param>
        /// <param name="properties">The parameter is not used.</param>
        public void LogWarning(LogMessage message, Type boundaryType, Dictionary<LogPropertyName, LogPropertyValue> properties)
        {
            // Only email errors.
            return;
        }

        /// <summary>
        /// Generate the body and subject line of the email.
        /// </summary>
        /// <param name="message">The message to log.</param>
        /// <param name="stack">Call stack from which source information can be extracted, or null.</param>
        /// <param name="stackTrace">Stack trace as a string.</param>
        /// <param name="properties">Optional set of properties that will be included in the log.</param>
        /// <returns>The body and subject line of the email.</returns>
        private static Tuple<string, string> GenerateEmailSubjectAndBody(LogMessage message, CallStack stack, string stackTrace, Dictionary<LogPropertyName, LogPropertyValue> properties)
        {
            string content = message.ToString();

            string subject = content;
            if (subject.Length > 78)
            {
                subject = content.Substring(0, 77);
            }

            subject = content.Replace('\n', ' ');
            subject = content.Replace('\r', ' ');

            StringBuilder body = new StringBuilder();
            body.AppendFormat("Message : {0}{1}", content, System.Environment.NewLine);
            body.AppendLine();

            if (stack != null)
            {
                body.AppendFormat("Class        :{0}{1}", stack.ClassName, System.Environment.NewLine);
                body.AppendFormat("FileName     :{0}{1}", stack.FileName, System.Environment.NewLine);
                body.AppendFormat("LineNumber   :{0}{1}", stack.LineNumber.ToString(), System.Environment.NewLine);
                body.AppendFormat("MethodName   :{0}{1}", stack.MethodName, System.Environment.NewLine);
            }

            if (properties != null)
            {
                body.AppendLine();

                foreach (var key in properties.Keys)
                {
                    body.AppendFormat("{0} : {1}{2}", key.ToString(), properties[key].ToString(), System.Environment.NewLine);
                }
            }

            if (!string.IsNullOrEmpty(stackTrace))
            {
                body.AppendLine();
                body.AppendLine("Stack Trace: ");
                body.AppendLine(stackTrace);
            }

            return new Tuple<string, string>(subject, body.ToString());
        }

        /// <summary>
        /// Log an error message.
        /// </summary>
        /// <param name="subject">The subject of the email.</param>
        /// <param name="body">The body of the email.</param>
        private void LogError(string subject, string body)
        {
            LqbGrammar.DataTypes.EmailSubject? emailSubject = LqbGrammar.DataTypes.EmailSubject.Create(subject);
            if (emailSubject == null)
            {
                return;
            }

            var emailBody = new LqbGrammar.Drivers.Emailer.EmailTextBody();
            emailBody.AppendText(body);

            var emailPackage = new LqbGrammar.Drivers.Emailer.EmailPackage(new Guid("11111111-1111-1111-1111-111111111111") /* SystemBrokerGuid */, this.Sender, emailSubject.Value, emailBody, this.Recipients.ToArray());

            var factory = LqbGrammar.GenericLocator<LqbGrammar.Drivers.Emailer.IEmailDriverFactory>.Factory;
            var emailAgent = factory.Create(this.Server, this.PortNumber);
            emailAgent.SendEmail(emailPackage);
        }
    }
}
