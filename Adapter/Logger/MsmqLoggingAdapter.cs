﻿namespace Adapter.Logger
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Reflection;
    using System.Security.Principal;
    using System.Text;
    using System.Web;
    using System.Xml.Linq;
    using LqbGrammar;
    using LqbGrammar.Adapters;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;
    using LqbGrammar.Exceptions;
    using LqbGrammar.Utils;

    /// <summary>
    /// Logging adapter that sends the log message to a service via a message queue.
    /// </summary>
    internal sealed class MsmqLoggingAdapter : ILoggingAdapter
    {
        /// <summary>
        /// Cached name of the containing process.
        /// </summary>
        private static string currentProcessName = System.Diagnostics.Process.GetCurrentProcess().ProcessName;

        /// <summary>
        /// Initializes a new instance of the <see cref="MsmqLoggingAdapter"/> class.
        /// </summary>
        /// <param name="queue">The path to the queue where the logging data is sent.</param>
        public MsmqLoggingAdapter(MessageQueuePath queue)
        {
            this.Queue = queue;
        }

        /// <summary>
        /// Gets or sets the path to the queue where the logging data is sent.
        /// </summary>
        /// <value>The path to the queue where the logging data is sent.</value>
        private MessageQueuePath Queue { get; set; }

        /// <summary>
        /// Log a bug report.
        /// </summary>
        /// <param name="message">The message to log.</param>
        /// <param name="boundaryType">Type of class that is used as the starting point for logging messages.</param>
        /// <param name="properties">Optional set of properties that will be included in the log.</param>
        public void LogBug(LogMessage message, Type boundaryType, Dictionary<LogPropertyName, LogPropertyValue> properties)
        {
            this.SendLoggingMessage("Bug", message, boundaryType, properties);
        }

        /// <summary>
        /// Log debug information.
        /// </summary>
        /// <param name="message">The message to log.</param>
        /// <param name="boundaryType">Type of class that is used as the starting point for logging messages.</param>
        /// <param name="properties">Optional set of properties that will be included in the log.</param>
        public void LogDebug(LogMessage message, Type boundaryType, Dictionary<LogPropertyName, LogPropertyValue> properties)
        {
            this.SendLoggingMessage("Debug", message, boundaryType, properties);
        }

        /// <summary>
        /// Log an error message.
        /// </summary>
        /// <param name="message">The message to log.</param>
        /// <param name="boundaryType">Type of class that is used as the starting point for logging messages.</param>
        /// <param name="properties">Optional set of properties that will be included in the log.</param>
        public void LogError(LogMessage message, Type boundaryType, Dictionary<LogPropertyName, LogPropertyValue> properties)
        {
            this.SendLoggingMessage("Error", message, boundaryType, properties);
        }

        /// <summary>
        /// Log an error message using the input stack trace rather than pulling it from the call stack.
        /// </summary>
        /// <param name="message">The message to log.</param>
        /// <param name="stackTrace">Stack trace to use in the log.</param>
        /// <param name="properties">Optional set of properties that will be included in the log.</param>
        public void LogError(LogMessage message, string stackTrace, Dictionary<LogPropertyName, LogPropertyValue> properties)
        {
            // the stack trace isn't sent to msmq!
            this.SendLoggingMessage("Error", message, null, properties);
        }

        /// <summary>
        /// Log an informational message.
        /// </summary>
        /// <param name="message">The message to log.</param>
        /// <param name="boundaryType">Type of class that is used as the starting point for logging messages.</param>
        /// <param name="properties">Optional set of properties that will be included in the log.</param>
        public void LogInfo(LogMessage message, Type boundaryType, Dictionary<LogPropertyName, LogPropertyValue> properties)
        {
            this.SendLoggingMessage("Info", message, boundaryType, properties);
        }

        /// <summary>
        /// Log a trace message.
        /// </summary>
        /// <param name="message">The message to log.</param>
        /// <param name="boundaryType">Type of class that is used as the starting point for logging messages.</param>
        /// <param name="properties">Optional set of properties that will be included in the log.</param>
        public void LogTrace(LogMessage message, Type boundaryType, Dictionary<LogPropertyName, LogPropertyValue> properties)
        {
            this.SendLoggingMessage("Trace", message, boundaryType, properties);
        }

        /// <summary>
        /// Log a warning message.
        /// </summary>
        /// <param name="message">The message to log.</param>
        /// <param name="boundaryType">Type of class that is used as the starting point for logging messages.</param>
        /// <param name="properties">Optional set of properties that will be included in the log.</param>
        public void LogWarning(LogMessage message, Type boundaryType, Dictionary<LogPropertyName, LogPropertyValue> properties)
        {
            this.SendLoggingMessage("Warn", message, boundaryType, properties);
        }

        /// <summary>
        /// Create the logging message and send it to the queue.
        /// </summary>
        /// <param name="level">Severity level of the message.</param>
        /// <param name="message">The message to log.</param>
        /// <param name="boundaryType">Type of class that is used as the starting point for logging messages.</param>
        /// <param name="properties">Optional set of properties that will be included in the log.</param>
        private void SendLoggingMessage(string level, LogMessage message, Type boundaryType, Dictionary<LogPropertyName, LogPropertyValue> properties)
        {
            Guid requestUniqueId = Guid.Empty;
            string category = string.Empty;
            string slid = string.Empty;
            string correlationId = string.Empty;
            string clientIp = string.Empty;
            string uniqueClientIdCookie = string.Empty;
            string errorUniqueId = string.Empty;
            string lqbContext = string.Empty;
            string timingProcessing = string.Empty;

            foreach (var key in properties.Keys)
            {
                try
                {
                    switch (key.ToString())
                    {
                        case "RequestUniqueId":
                            requestUniqueId = new Guid(properties[key].ToString());
                            break;
                        case "Category":
                            category = properties[key].ToString();
                            break;
                        case "sLId":
                            slid = properties[key].ToString();
                            break;
                        case "CorrelationId":
                            correlationId = properties[key].ToString();
                            break;
                        case "ClientIp":
                            clientIp = properties[key].ToString();
                            break;
                        case "UniqueClientIdCookie":
                            uniqueClientIdCookie = properties[key].ToString();
                            break;
                        case "error_unique_id":
                            errorUniqueId = properties[key].ToString();
                            break;
                        case "LqbContext":
                            lqbContext = properties[key].ToString();
                            break;
                        case "timing_processing":
                            timingProcessing = properties[key].ToString();
                            break;
                        default:
                            throw new ServerException(ErrorMessage.SystemError);
                    }
                }
                catch (ServerException)
                {
                    // dummy throw/catch to make StyleCop happy :(
                }
            }

            string machine = string.Empty;
            try
            {
                machine = Environment.MachineName;
            }
            catch (InvalidOperationException)
            {
                machine = "Environment.MachineName is undefined.";
            }

            string threadName = System.Threading.Thread.CurrentThread.Name;
            if (threadName == null)
            {
                threadName = string.Empty;
            }

            string context = string.Empty;
            IPrincipal principal = System.Threading.Thread.CurrentPrincipal;
            if (principal != null)
            {
                context = principal.Identity.Name;
            }

            string module = AppDomain.CurrentDomain.FriendlyName;
            string file = string.Empty;
            if (HttpContext.Current != null)
            {
                try
                {
                    file = HttpContext.Current.Request.Path;
                    module += " (" + HttpContext.Current.Request.Url.Host + ")";
                }
                catch (HttpException)
                {
                    // 9/23/2011 dd - HttpException can be throw if the Request object is not available or ready.
                }
            }

            var stack = new CallStack(boundaryType);
            string component = string.Format("{0}::{1}", stack.ClassName, stack.MethodName);

            string text = message.ToString();
            if (!string.IsNullOrEmpty(lqbContext))
            {
                var sb = new StringBuilder();
                sb.AppendLine("Context: " + lqbContext);
                sb.AppendLine();
                sb.Append(text);
                text = sb.ToString();
            }

            XDocument msg = this.CreateMessage(
                level,
                component,
                module,
                context,
                category,
                file,
                machine,
                threadName,
                text,
                requestUniqueId,
                slid,
                correlationId,
                clientIp,
                uniqueClientIdCookie,
                errorUniqueId,
                timingProcessing);

            var element = LqbXmlElement.Create(msg.Root).Value;

            var factory = GenericLocator<IMessageQueueDriverFactory>.Factory;
            var queue = factory.Create();
            queue.SendXML(this.Queue, null, element);
        }

        /// <summary>
        /// Create and xml message representing the message that is being logged.
        /// </summary>
        /// <param name="level">Severity level of the message.</param>
        /// <param name="component">Class and method name that is logging the message.</param>
        /// <param name="module">Friendly name of the current application domain.</param>
        /// <param name="context">Name of the logged in principal.</param>
        /// <param name="category">Category property.</param>
        /// <param name="file">File containing the code that is logging the message.</param>
        /// <param name="machine">The name of the machine where the code is running.</param>
        /// <param name="threadName">The name of the thread that is executing the code.</param>
        /// <param name="message">The message that will be logged.</param>
        /// <param name="requestUniqueId">RequestUniqueId property.</param>
        /// <param name="slid">Property called 'sLId'.</param>
        /// <param name="correlationId">CorrelationId property.</param>
        /// <param name="clientIp">The client IP when the logging is from a web application.</param>
        /// <param name="uniqueClientIdCookie">UniqueClientIdCookie property.</param>
        /// <param name="errorUniqueId">Property called 'error_unique_id'.</param>
        /// <param name="timingProcessing">The amount of time processing for the event in the log, if supplied.</param>
        /// <returns>An xml document with the log message data.</returns>
        private XDocument CreateMessage(
            string level,
            string component,
            string module,
            string context,
            string category,
            string file,
            string machine,
            string threadName,
            string message,
            Guid requestUniqueId,
            string slid,
            string correlationId,
            string clientIp,
            string uniqueClientIdCookie,
            string errorUniqueId,
            string timingProcessing)
        {
            XDocument doc = new XDocument();

            XElement el = new XElement(
                "log",
                new XAttribute("Level", level),
                new XAttribute("Component", component),
                new XAttribute("Module", module),
                new XAttribute("Context", context),
                new XAttribute("Category", category),
                new XAttribute("File", file),
                new XAttribute("Machine", machine),
                new XAttribute("ProcessName", currentProcessName),
                new XAttribute("ThreadName", threadName),
                new XAttribute("sLId", slid),
                new XAttribute("RequestUniqueId", requestUniqueId.ToString()),
                new XAttribute("CorrelationId", correlationId),
                new XAttribute("ClientIp", clientIp),
                new XAttribute("UniqueClientIdCookie", uniqueClientIdCookie),
                new XAttribute("ErrorUniqueId", errorUniqueId),
                new XAttribute("TimingProcessing", timingProcessing));

            el.Value = this.SantizeXmlString(message);
            doc.Add(el);
            return doc;
        }

        /// <summary>
        /// Strip xml of illegal characters.
        /// </summary>
        /// <param name="xml">The xml under examination.</param>
        /// <returns>Sanitized xml with illegal characters removed.</returns>
        private string SantizeXmlString(string xml)
        {
            StringBuilder sb = new StringBuilder(xml.Length);
            foreach (char ch in xml.ToCharArray())
            {
                if (this.IsLegalXmlChar(ch))
                {
                    sb.Append(ch);
                }
            }

            return sb.ToString();
        }

        /// <summary>
        /// Determine whether the input character is legal for an xml document.
        /// </summary>
        /// <param name="ch">The character under examination.</param>
        /// <returns>True if the character is legal, false otherwise.</returns>
        private bool IsLegalXmlChar(int ch)
        {
            // The list of allowable characters http://www.w3.org/TR/REC-xml/#charsets
            return ch == 0x9 || ch == 0xA || ch == 0xD || (ch >= 0x20 && ch <= 0xD7FF) || (ch >= 0xE000 && ch <= 0xFFFD) || (ch >= 0x10000 && ch <= 0x10FFFF);
        }
    }
}
