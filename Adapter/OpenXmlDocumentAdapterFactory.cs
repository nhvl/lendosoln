﻿namespace Adapter
{
    using System;
    using LqbGrammar.Adapters;

    /// <summary>
    /// Factory for creating implementations of open XML document adapters.
    /// </summary>
    public class OpenXmlDocumentAdapterFactory : IOpenXmlDocumentAdapterFactory
    {
        /// <summary>
        /// Create and return an implementation of the open XML document adapter.
        /// </summary>
        /// <returns>
        /// Implementation of the open XML document adapter.
        /// </returns>
        public IOpenXmlDocumentAdapter Create() => new OpenXmlDocumentAdapter();
    }
}
