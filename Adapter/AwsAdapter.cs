﻿namespace Adapter
{
    using System;
    using LqbGrammar.Adapters;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Adapter that encapsulates AWS as a file db storage repository.
    /// </summary>
    internal sealed class AwsAdapter : IAwsFileStorageAdapter
    {
        /// <summary>
        /// Save a file that was previously retrieved from the file storage using the UseFile method.
        /// </summary>
        /// <param name="location">The location of the file storage repository.</param>
        /// <param name="key">The file identifier used to retrieve the file later, this can be the same as or different from the key passed into the UseFile method.</param>
        /// <param name="token">A file token that holds the location of the file in the local environment.</param>
        public void ReSaveFile(FileStorageIdentifier location, FileIdentifier key, IFileToken token)
        {
            try
            {
                if (token is AwsToken)
                {
                    var fileToken = token as AwsToken;
                    AwsS3FileHandle handle = fileToken.GetHandle();
                    if (handle == null)
                    {
                        throw new DeveloperException(ErrorMessage.SystemError);
                    }

                    AwsS3FileStorage.PutFile(location.ToString(), key.ToString(), handle);
                }
                else
                {
                    throw new DeveloperException(ErrorMessage.SystemError);
                }
            }
            catch (LqbException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new DeveloperException(ErrorMessage.SystemError, ex);
            }
        }

        /// <summary>
        /// Retrieve a file from the file storage.
        /// </summary>
        /// <param name="location">The location of the file storage repository.</param>
        /// <param name="key">The file identifier previously used to save the file.</param>
        /// <param name="readFileHandler">A delegate which will read the file from the location provided in the LocalFilePath.</param>
        public void RetrieveFile(FileStorageIdentifier location, FileIdentifier key, Action<LocalFilePath> readFileHandler)
        {
            try
            {
                using (var handle = AwsS3FileStorage.GetFile(location.ToString(), key.ToString()))
                {
                    if (handle == null)
                    {
                        throw new System.IO.FileNotFoundException(string.Format("(location, string) = ({0}, {1}) not found", location.ToString(), key.ToString()));
                    }

                    LocalFilePath localPath = this.GetLocalPath(handle);

                    readFileHandler(localPath);
                }
            }
            catch (System.IO.FileNotFoundException)
            {
                // Code in FileDBTools expect FileNotFoundException when file is not in storage.
                throw;
            }
            catch (LqbException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new DeveloperException(ErrorMessage.SystemError, ex);
            }
        }

        /// <summary>
        /// Save a new file into the file storage.
        /// </summary>
        /// <param name="location">The location of the file storage repository.</param>
        /// <param name="key">The file identifier used to retrieve the file later.</param>
        /// <param name="saveFileHandler">A delegate which will save the file to the location provided in the LocalFilePath.</param>
        public void SaveNewFile(FileStorageIdentifier location, FileIdentifier key, Action<LocalFilePath> saveFileHandler)
        {
            try
            {
                using (var handle = AwsS3FileStorage.NewFileHandle())
                {
                    var localPath = this.GetLocalPath(handle);

                    saveFileHandler(localPath);
                    AwsS3FileStorage.PutFile(location.ToString(), key.ToString(), handle);
                }
            }
            catch (LqbException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new DeveloperException(ErrorMessage.SystemError, ex);
            }
        }

        /// <summary>
        /// Retrieve a file from the file storage and return a token that can be used to re-store the file.
        /// </summary>
        /// <param name="location">The location of the file storage repository.</param>
        /// <param name="key">The file identifier previously used to save the file.</param>
        /// <param name="useFileHandler">A delegate which will read the file from the location provided in the IFileToken, and can re-store the file with this token.</param>
        public void UseFile(FileStorageIdentifier location, FileIdentifier key, Action<IFileToken> useFileHandler)
        {
            try
            {
                using (var handle = AwsS3FileStorage.GetFile(location.ToString(), key.ToString()))
                {
                    if (handle == null)
                    {
                        throw new System.IO.FileNotFoundException(string.Format("(location, string) = ({0}, {1}) not found", location.ToString(), key.ToString()));
                    }

                    var token = new AwsToken(handle);
                    try
                    {
                        useFileHandler(token);
                    }
                    finally
                    {
                        token.MakeInvalid();
                    }
                }
            }
            catch (System.IO.FileNotFoundException)
            {
                // Code in FileDBTools expect FileNotFoundException when file is not in storage.
                throw;
            }
            catch (LqbException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new DeveloperException(ErrorMessage.SystemError, ex);
            }
        }

        /// <summary>
        /// Delete the file from the file storage.
        /// </summary>
        /// <param name="location">The location of the file storage repository.</param>
        /// <param name="key">The file identifier used to delete.</param>
        public void DeleteFile(FileStorageIdentifier location, FileIdentifier key)
        {
            AwsS3FileStorage.DeleteFile(location.ToString(), key.ToString());
        }

        /// <summary>
        /// Check whether the given file exist in the file storage.
        /// </summary>
        /// <param name="location">The location of the file storage repository.</param>
        /// <param name="key">The file identifier.</param>
        /// <returns>True if the file exist false otherwise.</returns>
        public bool FileExists(FileStorageIdentifier location, FileIdentifier key)
        {
            return AwsS3FileStorage.FileExists(location.ToString(), key.ToString());
        }

        /// <summary>
        /// Extract the local file name from the handle and return a validated path.
        /// </summary>
        /// <param name="handle">A file handle from the AWS system.</param>
        /// <returns>A validated local file path.</returns>
        private LocalFilePath GetLocalPath(AwsS3FileHandle handle)
        {
            LocalFilePath? localPath = LocalFilePath.Create(handle.LocalFileName);
            if (localPath != null)
            {
                return localPath.Value;
            }
            else
            {
                throw new DeveloperException(ErrorMessage.SystemError);
            }
        }
    }
}
