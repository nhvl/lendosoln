﻿namespace Adapter
{
    using System.Collections.Generic;
    using System.Text.RegularExpressions;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Regular expressions are expensive objects so we will
    /// cache them for future use.
    /// </summary>
    public static class RegularExpressionStore
    {
        /// <summary>
        /// When a regular expression is used to match a full
        /// string, it is stored here.
        /// </summary>
        private static Dictionary<string, Regex> fullStorage;

        /// <summary>
        /// When a regular expression is used to match a
        /// sub-string, it is stored here.
        /// </summary>
        private static Dictionary<string, Regex> searchStorage;

        /// <summary>
        /// Object lock used for updating the fullStorage variable.
        /// </summary>
        private static object fullLock;

        /// <summary>
        /// Object lock used for updating the searchStorage variable.
        /// </summary>
        private static object searchLock;

        /// <summary>
        /// Initializes static members of the <see cref="RegularExpressionStore" /> class.
        /// </summary>
        static RegularExpressionStore()
        {
            fullStorage = new Dictionary<string, Regex>();
            searchStorage = new Dictionary<string, Regex>();
            fullLock = new object();
            searchLock = new object();
        }

        /// <summary>
        /// Retrieve a cached regular expression used for full string matching.
        /// </summary>
        /// <param name="cacheTarget">The regular expression string that will be cached.</param>
        /// <returns>The cached regular expression object.</returns>
        public static Regex RetrieveForMatch(RegularExpressionString cacheTarget)
        {
            // Since we are matching the whole string, we need to encapsulate the
            // regular expression within boundary markers.
            string regexValue = string.Format("^({0})$", cacheTarget.ToString());

            // first grab a local ptr to the storage object
            // so if another thread alters the storage it
            // won't interfere with the instance used here
            var storage = fullStorage;
            if (storage.ContainsKey(regexValue))
            {
                return storage[regexValue];
            }

            return SafeRetrieve(regexValue, fullLock);
        }

        /// <summary>
        /// Retrieve a cached regular expression used for sub-string searches.
        /// </summary>
        /// <param name="cacheTarget">The regular expression string that will be cached.</param>
        /// <returns>The cached regular expression object.</returns>
        public static Regex RetrieveForSearch(RegularExpressionString cacheTarget)
        {
            var regexValue = cacheTarget.ToString();

            // first grab a local ptr to the storage object
            // so if another thread alters the storage it
            // won't interfere with the instance used here
            var storage = searchStorage;
            if (storage.ContainsKey(regexValue))
            {
                return storage[regexValue];
            }

            return SafeRetrieve(regexValue, searchLock);
        }

        /// <summary>
        /// Utility method that handles the multi-threaded logic.
        /// </summary>
        /// <param name="cacheTarget">The regular expression string.</param>
        /// <param name="theLock">The lock object to use.</param>
        /// <returns>The cached regular expression.</returns>
        private static Regex SafeRetrieve(string cacheTarget, object theLock)
        {
            // we may have to alter the storage object so
            // lock the other threads out until this is accomplished
            lock (theLock)
            {
                // another thread may have added the value we want
                // so we'll check again in case that has been done
                var storage = (theLock == fullLock) ? fullStorage : searchStorage;
                if (storage.ContainsKey(cacheTarget))
                {
                    return storage[cacheTarget];
                }

                // we must modify the storage.  we cannot just
                // alter the one that's in place as other threads
                // may be reading from it.  instead copy that one
                // to a new storage object and add to the new
                // storage object
                var newStorage = new Dictionary<string, Regex>(storage);

                var newRegex = new Regex(cacheTarget, RegexOptions.Compiled);
                newStorage[cacheTarget] = newRegex;

                // we have a storage object with everything we need in
                // place, so make an atomic assignment to the cached
                // variable so other threads can now get the new version.
                if (theLock == fullLock)
                {
                    fullStorage = newStorage;
                }
                else
                {
                    searchStorage = newStorage;
                }

                return newRegex;
            }
        }
    }
}
