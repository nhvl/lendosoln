/// Author: David Dao

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using LpeUpdateBotLib.Common;
namespace LpeBotConsole
{
	public class BotInfoFromDB
	{

        public static ArrayList RetrieveAllActiveBots() 
        {
            string sqlQuery = "SELECT * FROM DownloadList WHERE Active='T'";
            return ExecuteQuery(sqlQuery);
        }
        public static ArrayList RetrieveBotById(int id) 
        {
            string sqlQuery = "SELECT * FROM DownloadList WHERE Id=" + id;
            return ExecuteQuery(sqlQuery);
        }

        public static ArrayList RetrieveBotByType(string botType) 
        {
            string sqlQuery = "SELECT * FROM DownloadList WHERE BotType='" + botType.Replace("'" , "''") + "'";
            return ExecuteQuery(sqlQuery);
        }

        
        private static ArrayList ExecuteQuery(string sqlQuery) 
        {
            ArrayList list = new ArrayList();

            // 3/28/2008 dd - OPM 20818 - We are no longer use binh_db database. Therefore accessing production database is prohibit on local machine.
            /*
            using (SqlConnection con = new SqlConnection(ParseConnection(DbCfg.DSN))) 
            {
                con.Open();
                using (SqlCommand command = new SqlCommand(sqlQuery, con)) 
                {
                    using (SqlDataReader reader = command.ExecuteReader(CommandBehavior.CloseConnection)) 
                    {
                        while(reader.Read()) 
                        {
                            TestDownloadInfo info = new TestDownloadInfo();
                            info.BotType = SafeDBValue(reader["BotType"]);
                            info.Description = SafeDBValue(reader["Description"]);
                            info.Url = SafeDBValue(reader["Url"]);
                            info.FileName = SafeDBValue(reader["FileName"]);
                            info.Login = SafeDBValue(reader["Login"]);
                            info.Password = SafeDBValue(reader["Password"]);
                            list.Add(info);
                        }
                    }
                }
            }
            */
            return list;
        }

        private static string SafeDBValue(object o) 
        {
            if (null == o || Convert.IsDBNull(o))
                return "";
            return o.ToString();
        }
        private static string ParseConnection(string sConnection)
        {
            int nPos = sConnection.IndexOf("B.1;") ;
            if (nPos >= 0)
            {
                // this is here for backward compatibility with Legacy connection string
                // containing the Provider/Data Provider string.
                return sConnection.Substring(nPos + 4) ;
            }
            else
                return sConnection ;
        }
	}
}
