using System;
using System.Collections;

using System.IO;
using LpeUpdateBotLib;
using LpeUpdateBotLib.Common;
using LpeUpdateBotLib.WebBots;

namespace LpeBotConsole
{
	class LpeBotConsole
	{
		[STAThread]
		static void Main(string[] args)
		{
            switch (Config.LpeBotConsoleTestMode.ToLower()) 
            {
                case "bybottype":
                    System.Console.Out.WriteLine(Config.LpeBotConsoleTestMode + " is no longer supported. Use ByManualData mode.");
//                    TestByBotType(Config.LpeBotConsoleTestModeValue);
                    break;
                case "allactivebots":
                    System.Console.Out.WriteLine(Config.LpeBotConsoleTestMode + " is no longer supported. Use ByManualData mode.");
//
//                    TestAllActiveBots();
                    break;
                case "bybotid":
//                    TestByBotId(Config.LpeBotConsoleTestModeValue);
                    System.Console.Out.WriteLine(Config.LpeBotConsoleTestMode + " is no longer supported. Use ByManualData mode.");

                    break;
                case "bymanualdata":
                    TestByManualData();
                    break;

                default:
                    System.Console.Out.WriteLine(Config.LpeBotConsoleTestMode + " is invalid LpeBotConsoleTestMode.");
                    break;
            }
		}

        /// <summary>
        /// This method will look for given bot type in the database and download the ratesheet.
        /// </summary>
        /// <param name="botType"></param>
        private static void TestByBotType(string botType) 
        {
            ArrayList list = BotInfoFromDB.RetrieveBotByType(botType);

            TestBotList(list);
        }

        /// <summary>
        /// This method will retrieve all active bots in the database and download their ratesheet.
        /// </summary>
        private static void TestAllActiveBots() 
        {
            ArrayList list = BotInfoFromDB.RetrieveAllActiveBots();
            TestBotList(list);
        }

        /// <summary>
        /// This method will retrieve bot from db base on the id and download its ratesheet.
        /// </summary>
        /// <param name="id"></param>
        private static void TestByBotId(string id) 
        {
            ArrayList list = BotInfoFromDB.RetrieveBotById(int.Parse(id));
            TestBotList(list);
        }

        /// <summary>
        /// Individual test function that create bot with information set in config file.
        /// </summary>
        private static void TestByManualData() 
        {
            TestDownloadInfo info = new TestDownloadInfo();
            info.BotType = Config.ManualBotInfo_BotType;
            info.Description = Config.ManualBotInfo_Description;
            info.MainFileName = Config.ManualBotInfo_FileName;
            info.Url = Config.ManualBotInfo_Url;
            info.Login = Config.ManualBotInfo_Login;
            info.Password = Config.ManualBotInfo_Password;
            info.LastRatesheetTimestamp = Config.ManualBotInfo_LastRatesheetTimestamp;
            info.Token = Config.ManualBotInfo_Token;

			TestBot(info);
        }

        private static void TestBotList(ArrayList list) 
        {
            foreach (IDownloadInfo info in list) 
            {
                TestBot(info);
            }
        }
        private static void TestBot(IDownloadInfo info) 
        {
            AbstractWebBot bot = WebBotFactory.CreateBot(info);

            if (null != bot) 
            {
                E_BotDownloadStatusT downloadStatus = bot.Download();
                if (downloadStatus == E_BotDownloadStatusT.SuccessfulWithRatesheet)
                {
                    string fileName = bot.RatesheetFileName;
                    if (null != fileName)
                        File.Copy(fileName, Config.FILES_PATH + info.MainFileName, true);   

                }
            }
        }
	}
}
