/// Author: David Dao

using System;
using LpeUpdateBotLib.Common;

namespace LpeBotConsole
{
	public class TestDownloadInfo : IDownloadInfo
	{
        private string m_botType;
        private string m_description;
        private string m_url;
        private string[] m_fileNames = null;
        private string m_mainFileName = "";
        private string m_conversion;
        private string m_login;
        private string m_password;
        private DateTime m_lastRatesheetTimestamp;
        private string m_token;

        public TestDownloadInfo()
        {
        }

        public string BotType 
        {
            get { return m_botType; }
            set { m_botType = value; }
        }
        public string Description 
        {
            get { return m_description; }
            set { m_description = value; }
        }
        public string Url 
        {
            get { return m_url; }
            set { m_url = value; }
        }
        public string[] FileNames 
        {
            get { return m_fileNames; }
            set
            {
                m_fileNames = value ?? new string[]{""};
                if (m_fileNames.Length > 0)
                {
                    m_mainFileName = m_fileNames[0];
                }
            }
        }
        public string MainFileName
        {
            get { return m_mainFileName; }
            set
            {
                m_mainFileName = value;
                if (m_fileNames != null)
                {
                    if (m_fileNames.Length > 0)
                    {
                        m_fileNames[0] = m_mainFileName;
                    }
                    else
                    {
                        m_fileNames = new string[] { m_mainFileName };
                    }
                }
            }
        }
        public string Conversion 
        {
            get { return m_conversion; }
            set { m_conversion = value; }
        }
        public string Login 
        {
            get { return m_login; }
            set { m_login = value; }
        }
        public string Password 
        {
            get { return m_password; }
            set { m_password = value; }
        }
        public DateTime LastRatesheetTimestamp
        {
            get { return m_lastRatesheetTimestamp; }
            set { m_lastRatesheetTimestamp = value; }
        }

        public string Token
        {
            get { return m_token; }
            set { m_token = value; }
        }
        public int Id 
        {
            get { return 0; }
        }

        public void RecordLastSuccessfulDLAndLastUpdated() 
        {
            // NO-OP
        }
        public void RecordLastUpdated() 
        {
            // NO-OP
        }
        public void RecordLastSuccessfulDL() 
        {
            // NO-OP
        }
        public void RecordLastRatesheetTimestamp(DateTime timestamp)
        {
            // NO-OP
        }
        public void TurnOffCWDL() 
        {
            // NO-OP
        }
        public void RecordErrorMessage(Exception exc) 
        {
            // NO-OP
        }

		public void Deactivate() 
		{
			// NO-OP
		}

        // OPM 21897 - allow bots to expire pricing for a particular ratesheet
        public void ExpireRatesheet()
        {
            // NO-OP
        }
        public void ExpireRatesheet(string ratesheetName)
        {
            // NO-OP
        }
	}
}
