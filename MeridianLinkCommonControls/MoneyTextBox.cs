using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;
using System.Text;

namespace MeridianLink.CommonControls
{
	/// <summary>
	/// Summary description for MoneyTextBox.
	/// </summary>
    [DefaultProperty("Text"), 
    ToolboxData("<{0}:MoneyTextBox runat=server preset='money' width='90'/>")]
    public class MoneyTextBox : GenericMaskTextBox
    {
        public MoneyTextBox()
        {
            this.Attributes.Add("preset", "money");
            this.Width = new Unit(90);
        }
    }
}
