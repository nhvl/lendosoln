using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;

namespace MeridianLink.CommonControls
{
	/// <summary>
	/// Summary description for DateTextBox.
	/// </summary>
    [DefaultProperty("Text"), 
    ToolboxData("<{0}:DateTextBox runat=server preset='date' width='75'/>")]
    public class DateTextBox : GenericMaskTextBox 
    {
        public DateTextBox()
        {
            this.Attributes.Add("preset", "date");
            this.Width = new Unit(75);
        }
        private bool m_isDisplayCalendarHelper = true;

        [Bindable(true), Category("Appearance"), DefaultValue(true)] 
        public bool IsDisplayCalendarHelper 
        {
            get { return m_isDisplayCalendarHelper; }
            set { m_isDisplayCalendarHelper = value; }
        }

        private string m_HelperAlign = "AbsMiddle";

        [Bindable(true), Category("Appearance"), DefaultValue("AbsMiddle")] 
        public string HelperAlign 
        {
            get { return m_HelperAlign; }
            set { m_HelperAlign = value; }
        }

        protected override void OnInit(EventArgs e) 
        {
            base.OnInit(e);
        }
        protected override void Render(System.Web.UI.HtmlTextWriter writer) 
        {
            base.Render(writer);
            if (m_isDisplayCalendarHelper) 
            {
                writer.Write(string.Format(@"<a href=""#"" onclick=""return displayCalendar('{0}');"" tabindex=-1><img src=""{1}/images/pdate.gif"" border=0 title='Open calendar' align='{2}'></a>", this.ClientID, VirtualRoot, m_HelperAlign));
            }
        }
    }

}
