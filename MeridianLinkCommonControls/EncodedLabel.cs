﻿using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MeridianLink.CommonControls
{
    public class EncodedLabel : Label
    {
        private bool IsRendering = false;

        public override string Text
        {
            get
            {
                if (this.IsRendering)
                {
                    return HttpContext.Current.Server.HtmlEncode(base.Text);
                }
                return base.Text;
            }
            set
            {
                base.Text = value;
            }
        }


        public override void RenderBeginTag(HtmlTextWriter writer)
        {
            base.RenderBeginTag(writer);
            this.IsRendering = true;
        }

        public override void RenderEndTag(HtmlTextWriter writer)
        {
            this.IsRendering = false;
            base.RenderEndTag(writer);
        }
    }
}
