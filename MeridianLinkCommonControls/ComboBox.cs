using System;
using System.Collections;
using System.Collections.Specialized;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.ComponentModel;
using System.Text;
using System.Collections.Generic;

namespace MeridianLink.CommonControls
{
	/// <summary>
	/// Summary description for ComboBox.
	/// 
	/// NOTE: In order for this control to work. It need to place in page inherited from LendersOffice.Common.BasePage.
	/// 
	/// If you are not inherit from BasePage then you need to have the following javascript.
    /// document.onreadystatechange = _main;
    /// function _main() 
    /// {
    ///   if (document.readyState == 'complete') 
    ///   {
    ///     if (typeof(__initCombobox) == 'function') __initCombobox();
    ///   }
    /// }
    ///   
    ///   TODO: Revise this control so this requirement is not necessary.
	/// </summary>
    [DefaultProperty("Text"), 
    ToolboxData("<{0}:ComboBox runat='server' />")]
    [ParseChildrenAttribute(false)]
    public class ComboBox : TextBox
	{
        private static Guid JavascriptVersion = Guid.NewGuid();
        // 2/6/2012 dd - When set this to true, you are expected to have jquery include in your page.
        public bool UseJqueryScript { get; set; }

        private StringCollection m_items = new StringCollection();

        public string VirtualRoot 
        {
            get 
            {
                if (Context != null) 
                {
                    return Page.Request.ApplicationPath == "/" ? "" :Page.Request.ApplicationPath;
                } 
                else 
                {
                    return "";
                }
            }

        }
        [Bindable(true), Category("Data")] 
        public StringCollection Items 
        {
            get { return m_items; }
        }
		public ComboBox()
		{
            
		}

        private void ControlLoad(object sender, System.EventArgs e) 
        {
            Dictionary<string, List<string>> list = (Dictionary<string, List<string>>) Context.Items["ML_CBL"];
            //StringCollection list = (StringCollection) Context.Items["ML_CBL"];
            if (list == null) 
            {
                list = new Dictionary<string, List<string>>();
                Context.Items["ML_CBL"] = list;
            }
            if (list.ContainsKey(this.ClientID) == false)
            {
                list.Add(this.ClientID, new List<string>());
            }

        }
        private void ControlPreRender(object sender, System.EventArgs e) 
        {
            if (this.UseJqueryScript == false)
            {
                this.Attributes.Add("onkeydown", "onComboboxKeypress(this, event);");
                StringBuilder sb = new StringBuilder();
                bool isFirst = true;
                string c;
                foreach (string s in m_items)
                {
                    c = isFirst ? "" : ",";

                    sb.AppendFormat("{0}'{1}'", c, CommonControlsAspxTools.JsStringUnquoted(s));

                    isFirst = false;
                }
                Page.ClientScript.RegisterArrayDeclaration(this.ClientID + "_items", sb.ToString());

            }
            else
            {
                Dictionary<string, List<string>> contextItem = (Dictionary<string, List<string>>)Context.Items["ML_CBL"];
                foreach (string s in m_items)
                {
                    contextItem[this.ClientID].Add(s);
                }
            }


        }


        private void PagePreRender(object sender, System.EventArgs e) 
        {
            Dictionary<string, List<string>> list = (Dictionary<string, List<string>>)Context.Items["ML_CBL"];

            if (list == null) return;

            StringBuilder sb = new StringBuilder();
            sb.AppendLine(@"<script type=""text/javascript"">");
            if (this.UseJqueryScript)
            {

                sb.Append(@"jQuery(function() {");
                foreach (var o in list)
                {
                    sb.AppendFormat("jQuery('#{0}').combobox({1});", o.Key, BuildJsonArray(o.Value));
                }
                sb.Append("});");
            }
            else
            {
                // 2/6/2012 dd - Render using old format.
                sb.Append(@"function __initCombobox() {");
                foreach (string id in list.Keys)
                {
                    sb.AppendFormat("if (typeof({0}_items) != 'undefined') createCombobox('{0}', {0}_items);", id).Append(Environment.NewLine);
                }
                sb.Append(@"}");
            }
            sb.AppendLine("</script>");

            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "CBL_List", sb.ToString());
        }

        private string BuildJsonArray(IEnumerable<string> list)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("[");
            bool isFirst = true;
            string c;
            foreach (string s in m_items)
            {
                c = isFirst ? "" : ",";

                sb.AppendFormat("{0}'{1}'", c, CommonControlsAspxTools.JsStringUnquoted(s));

                isFirst = false;
            }
            sb.Append("]");
            return sb.ToString();
        }
        protected override void Render(System.Web.UI.HtmlTextWriter writer) 
        {
            base.Render(writer);
            if (this.UseJqueryScript == false)
            {
                writer.Write(string.Format(@"<img src='{0}/images/dropdown_arrow.gif' onclick=""onSelect('{1}');"" class='combobox_img' align=absbottom>", VirtualRoot, this.ClientID));
            }
        }
        /// <summary>
        /// Register to include js file on this page.
        /// </summary>
        /// <param name="js"></param>
        protected void RegisterJsScript(string js) 
        {
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), js, string.Format("<script src='{0}/inc/{1}'></script>", VirtualRoot, js));
        }
        protected override void OnInit(EventArgs e) 
        {


            RegisterJsScript("combobox.js?v=" + JavascriptVersion);

            this.PreRender += new System.EventHandler(this.ControlPreRender);
            this.Load += new System.EventHandler(this.ControlLoad);
            this.Page.PreRender += new System.EventHandler(PagePreRender);
            base.OnInit(e);
        }
	}
}
