using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;
using System.Text;

namespace MeridianLink.CommonControls
{
	/// <summary>
	/// Summary description for GenericMaskTextBox.
	/// 09/23/03 - Chuong changed VirtualRoot property (made it 'writable')
	/// </summary>
    public class GenericMaskTextBox : System.Web.UI.WebControls.TextBox {
        private static Guid JavascriptVersion = Guid.NewGuid();

        private string m_sVirtualRoot = null ;

        public string VirtualRoot 
        {
			set 
			{ 
				m_sVirtualRoot = value ;
			}
            get 
            {
                if (null != m_sVirtualRoot) 
					return m_sVirtualRoot ;
                
                if (Context != null) 
                    return Page.Request.ApplicationPath == "/" ? "" :Page.Request.ApplicationPath;
                else 
                    return "" ;
            }
        }
        /// <summary>
        /// Register to include js file on this page.
        /// </summary>
        /// <param name="js"></param>
        protected void RegisterJsScript(string js) {
            Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), js, string.Format("<script src=\"{0}/inc/{1}\" type=\"text/javascript\"></script>", VirtualRoot, js));
        }
        protected override void OnInit(EventArgs e) {
            RegisterJsScript("mask.js?v=" + JavascriptVersion);
            base.OnInit(e);
        }
    }
}
