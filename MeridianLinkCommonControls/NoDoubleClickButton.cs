using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;

namespace MeridianLink.CommonControls
{
	/// <summary>
	/// Summary description for NoDoubleClickButton.
	/// </summary>
    [DefaultProperty("Text"),
    ToolboxData("<{0}:NoDoubleClickButton runat=server />")]	
	public class NoDoubleClickButton : System.Web.UI.WebControls.Button
	{
        #region Javascript to perform no double click
        private const string NoDoubleClickScript = @"
<script language='javascript'>
<!--
function nodoubleclick() {
  if (typeof(Page_ClientValidate) == 'function' && !Page_ClientValidate()) return;
  var o = event.srcElement;
  o.disabled = true;
  if (o.waitmsg) o.value = o.waitmsg;
  if (!o.disableone) {
    var coll = document.getElementsByTagName('INPUT');
    var length = coll.length;
    if (length > 0) {
      for (i = 0; i < length; i++) {
        var o1 = coll[i];
        if (o1.type.toLowerCase() == 'submit' || o1.type.toLowerCase() == 'button') o1.disabled = true;
      }
    }
  }
  __doPostBack(o.name,'');
}
// -->
</script>
";
        #endregion
        private string m_waitingMessage;
        private bool m_disableOneButton;

        /// <summary>
        /// Message to display on button when clicked.
        /// </summary>
        [Bindable(true), Category("Appearance"), DefaultValue("Wait ...")] 
        public string WaitingMessage 
        {
            get { return m_waitingMessage; }
            set { m_waitingMessage = value; }
        }

        /// <summary>
        /// Determined whether to disable all buttons on the page, or just this button when click.
        /// Default is all buttons will be disable.
        /// </summary>
        [Bindable(true), Category("Appearance")]
        public bool DisableOneButton 
        {
            get { return m_disableOneButton; }
            set { m_disableOneButton = value; }
        }

		public NoDoubleClickButton()
		{
		}
        protected override void OnInit(EventArgs e) 
        {
            string initScript = String.Format(@"
<script language='javascript'>
<!--
try
{{
	var btn = document.getElementById('{0}');
	if ( btn != null )
		btn.attachEvent('onclick', nodoubleclick);
}}
catch (e) {{}}
// -->
</script>
", this.ClientID);

            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "NoDoubleClickButtonScript", NoDoubleClickScript);
            Page.ClientScript.RegisterStartupScript(this.GetType(), this.ClientID + "_Init", initScript);

            Page.ClientScript.GetPostBackEventReference(this, ""); // This will add __doPostBack
            base.OnInit(e);
        }
        protected override void OnPreRender(EventArgs e) 
        {
            if (m_waitingMessage != null && m_waitingMessage.Trim() != "") 
            {
                this.Attributes.Add("waitmsg", m_waitingMessage);
            }
            if (m_disableOneButton) 
            {
                this.Attributes.Add("disableone", "true");
            }
            base.OnPreRender(e);

        }
	}
}
