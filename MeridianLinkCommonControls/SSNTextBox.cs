using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;
using System.Text.RegularExpressions;

namespace MeridianLink.CommonControls
{
	/// <summary>
	/// Summary description for SSNTextBox.
	/// </summary>
    [DefaultProperty("Text"), 
    ToolboxData("<{0}:SSNTextBox runat=server preset='ssn' width='75px'/>")]
    public class SSNTextBox : GenericMaskTextBox 
    {
        private bool m_isMaskEnable;
        private bool m_isNotUpdated;

        public SSNTextBox() : base()
        {
            //We default to FALSE
            init(false);
        }

        public SSNTextBox(bool bMask) : base()
        {
            init(bMask);
        }

        private void init(bool bMask)
        {
            m_isMaskEnable = bMask;
            m_isNotUpdated = false;
        }
        
        public bool IsMaskEnabled
        {
            set{ m_isMaskEnable = value;   }
            get{ return m_isMaskEnable;    }
        }

        public override string Text
        {
            set
            {
                if (m_isMaskEnable)
                {
                    if (!isMasked(value))
                    {
                        base.Text = value;
                        m_isNotUpdated = false;
                    }
                    else
                        m_isNotUpdated = true;
                }
                else
                {
                    base.Text = value;
                }
            }
            get
            {
                return (m_isMaskEnable) ? mask(base.Text) : base.Text;
            }
        }

        public string MaskedText
        {
            get { return mask(base.Text); }
        }

        public string ClearText
        {
            get { return base.Text; }
        }

        public bool IsUpdated
        {
            get{  
                if (m_isMaskEnable)
                {
                    if (base.Text.Length == 11 && !isMasked(base.Text))
                        return true;

                    if (m_isNotUpdated)
                        return false;
                }
                return true;
            }
        }

        private string mask(string clear)
        {
            if (string.IsNullOrEmpty(clear))
                return "";
            return Regex.Replace(clear, @"\d\d\d-\d\d", "***-**");
        }

        private bool isMasked(string input)
        {
            if (string.IsNullOrEmpty(input))
                return false;
            return !Regex.IsMatch(input, @"\d\d\d-\d\d-\d\d\d\d");
        }     
    }
}
