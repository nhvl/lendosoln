using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Web.UI.Design.WebControls;
using System.Text;
using System.Drawing;

[assembly:TagPrefix("MeridianLink.CommonControls", "ml")]
namespace MeridianLink.CommonControls
{
	/// <summary>
	/// Summary description for FixedHeaderDataGridV2.
	/// </summary>
	[ToolboxData("<{0}:ScrollableFixedHeaderDataGridV2 runat=server></{0}:ScrollableFixedHeaderDataGridV2>")]
	public class  ScrollableFixedHeaderDataGridV2: System.Web.UI.WebControls.DataGrid
	{
		protected override void Render(HtmlTextWriter output)
		{	
			bool designMode = ((Site != null) && (Site.DesignMode));
			StringWriter sw = new StringWriter();
			HtmlTextWriter htw = new HtmlTextWriter(sw);
			base.Render(htw);
			htw.Close();
			sw.Close();
			
			if (!designMode)
			{
				Height = new Unit("0px");
				//This style is important for matching column widths later.
				Style["TABLE-LAYOUT"] = "fixed";
				StringBuilder sbRenderedTable = sw.GetStringBuilder();
				string temp = sbRenderedTable.ToString();
				Debug.Assert((sbRenderedTable.Length > 0),
					"Rendered HTML string is empty. Check viewstate usage and databinding.");
			
				if (sbRenderedTable.Length > 0)
				{
					//Replace the table's ID with a new ID.
					//It is tricky that we must only replace the 1st occurence,
					//since the rest occurences can be used for postback scripts for sorting.
					sbRenderedTable.Replace(ID, "DataGridHeaders", 0, (temp.IndexOf(ID) + ID.Length));
					//We only need the headers, stripping the rest contents.
					temp = sbRenderedTable.ToString();
					string tableHeaders = temp.Substring(0, (temp.ToLower()).IndexOf(@"</tr>") + 5);
					
					output.Write(tableHeaders);
					output.WriteEndTag("table");
				}
			}
			//StringBuilder sbRenderedBody = sw.GetStringBuilder();
			output.WriteBeginTag("div");
			output.WriteAttribute("width", "100%");
			output.Write(HtmlTextWriter.TagRightChar);
			base.Render(output);
			output.WriteEndTag("div");
		}
/*
		protected override void OnInit(EventArgs e)
		{
			if (0 == Width.Value) Width = new Unit("400px");
			if (0 == Height.Value) Height =  new Unit("100%");
			//Transparent header is not allowed.
			if (HeaderStyle.BackColor.IsEmpty)
			{
				HeaderStyle.BackColor = Color.White;
			}
			//Transparent pager is not allowed.
			if (PagerStyle.BackColor.IsEmpty)
			{
				PagerStyle.BackColor = Color.White;
			}
			base.OnInit (e);
		}
		*/

		[Browsable(false)]
		public override bool ShowHeader
		{
			get
			{
				return true;
			}
			set
			{
				if (false == value)
					throw new InvalidOperationException("Use the original DataGrid to set ShowHeaders to false.");
			}
		}
	}
}
