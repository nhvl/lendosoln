﻿using System;
using System.Web.UI.WebControls;

namespace MeridianLink.CommonControls
{
    public class PassthroughLiteral : Literal
    {
        public new LiteralMode Mode
        {
            get
            {
                return LiteralMode.PassThrough;
            }
        }

        public PassthroughLiteral()
        {
            base.Mode = LiteralMode.PassThrough;
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.Mode = LiteralMode.PassThrough;
            base.OnPreRender(e);
        }
    }
}
