using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;

namespace MeridianLink.CommonControls
{
	/// <summary>
	/// Summary description for PercentTextBox.
	/// </summary>
    [DefaultProperty("Text"), 
    ToolboxData("<{0}:PercentTextBox runat=server preset='percent' width='70'/>")]
    public class PercentTextBox : GenericMaskTextBox 
    {
        public PercentTextBox()
        {
            this.Attributes.Add("preset", "percent");
            this.Width = new Unit(70);
        }
    }
}
