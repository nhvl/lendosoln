using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;
using System.Text;

namespace MeridianLink.CommonControls
{
    /// <summary>
    /// Summary description for PhoneTextBox.
    /// </summary>
    [DefaultProperty("Text"), 
    ToolboxData("<{0}:PhoneTextBox runat=server preset='phone' width='120'/>")]
    public class PhoneTextBox : GenericMaskTextBox
    {
    }
}
