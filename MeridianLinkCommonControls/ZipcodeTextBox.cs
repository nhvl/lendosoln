using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;

namespace MeridianLink.CommonControls
{
	/// <summary>
	/// Summary description for ZipcodeTextBox.
	/// </summary>
    [DefaultProperty("Text"), 
    ToolboxData("<{0}:ZipcodeTextBox runat=server preset='zipcode' width='50'/>")]
    public class ZipcodeTextBox : GenericMaskTextBox 
    {
        public ZipcodeTextBox()
        {
            this.Attributes.Add("preset", "zipcode");
            this.Width = new Unit(50);
        }

        private void RegisterAttributes(string city, string state, string county)
        {
            this.Attributes.Add("city", city ?? string.Empty);
            this.Attributes.Add("state", state ?? string.Empty);
            this.Attributes.Add("county", county ?? string.Empty);
        }

        public void SmartZipcode(Control cityCtl, Control stateCtl, Control countyCtl) 
        {
            // 3/4/2004 dd - Store previous zipcode value. If zipcode value does not change then don't perform lookup.
            this.RegisterAttributes(cityCtl?.ClientID, stateCtl?.ClientID, countyCtl?.ClientID);
            this.Attributes.Add("onblur", "smartZipcodeAuto(event);");
            this.Attributes.Add("onfocus", "this.oldValue = this.value;");
            
        }

        public void SmartZipcode(Control cityCtl, Control stateCtl) 
        {
            // 3/4/2004 dd - Store previous zipcode value. If zipcode value does not change then don't perform lookup.

            this.RegisterAttributes(cityCtl?.ClientID, stateCtl?.ClientID, string.Empty);
            this.Attributes.Add("onblur", "smartZipcodeAuto(event);");
            this.Attributes.Add("onfocus", "this.oldValue = this.value;");

        }

		public void SmartZipcode(Control cityCtl, Control stateCtl, string countyID) 
		{
            this.RegisterAttributes(cityCtl?.ClientID, stateCtl?.ClientID, countyID);
            this.Attributes.Add("onblur", "smartZipcodeAuto(event);");
			this.Attributes.Add("onfocus", "this.oldValue = this.value;");
		}
    }
}
