using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;
using System.Collections;
using System.Collections.Specialized;
using System.Text;

namespace MeridianLink.CommonControls
{
	/// <summary>
	/// Summary description for StateDropDownList.
	/// </summary>
    [DefaultProperty("Text"), 
    ToolboxData("<{0}:StateDropDownList runat=server></{0}:StateDropDownList>")]
    public class StateDropDownList : System.Web.UI.WebControls.WebControl, IPostBackDataHandler
    {
        // 4/11/2012 dd - Per OPM 65673 - We will always include these US territories 
        //  PR, VI, GU, AS, MP
        private static readonly string[] m_states = {"", "AK", "AL", "AR", "AS", "AZ", "CA", "CO", "CT", "DC", "DE", "FL", 
                                                        "GA", "GU", "HI", "IA", "ID", "IL", "IN", "KS", "KY", "LA", "MA", 
                                                        "MD", "ME", "MI", "MN", "MO", "MP", "MS", "MT", "NC", "ND", "NE",  
                                                        "NH", "NJ", "NM", "NV", "NY", "OH", "OK", "OR", 
                                                        "PA", "PR", "RI", "SC", "SD", "TN", "TX", "UT", "VA", "VI", "VT", 
                                                        "WA", "WI", "WV", "WY"};

        private static readonly string[] s_territories = { "AA", "AE", "AP" };

        private static string[] s_statesAndTerritories = null;

        static StateDropDownList() 
        {
            ArrayList list = new ArrayList();
            foreach (string s in m_states)
                list.Add(s);
            foreach (string s in s_territories)
                list.Add(s);

            list.Sort();

            s_statesAndTerritories = (string[]) list.ToArray(typeof(string));    
        }

        private string m_value = "";

		[Bindable(true), 
			Category("Misc"), 
			DefaultValue("")] 
		public string Value 
		{
			get { return m_value; }

			set { m_value = value; }
		}

        private bool m_includeTerritories = true;

        [Bindable(true), Category("Misc"), DefaultValue(true)]
        public bool IncludeTerritories 
        {
            get { return m_includeTerritories; }
            set { m_includeTerritories = value; }
        }

        public bool LoadPostData(String postDataKey, NameValueCollection values) 
        {

            m_value = (string) values[this.UniqueID];
            return false;
        }

        public void RaisePostDataChangedEvent() 
        {

            // Part of the IPostBackDataHandler contract.  Invoked if we ever returned true from the
            // LoadPostData method (indicates that we want a change notification raised).  Since we
            // always return false, this method is just a no-op.
        }

		/// <summary> 
		/// Render this control to the output parameter specified.
		/// </summary>
		/// <param name="output"> The HTML writer to write out to </param>
		protected override void Render(HtmlTextWriter output)
		{
            //            <select name="m_BillingAddress:m_State" id="m_BillingAddress_m_State" style="font-weight:bold;">
            //	<option selected="selected" value="CA">CA</option>
            //
            //</select>
            StringBuilder sb = new StringBuilder();
            sb.Append("<select name=\"").Append(this.UniqueID).Append("\" id=\"");
            sb.Append(this.ClientID).Append("\"");
            if (CssClass != null && CssClass != "") 
            {
                sb.Append(" class=\"").Append(CssClass).Append("\"");
            }
            foreach (string key in Attributes.Keys) 
            {
                sb.AppendFormat(" {0}=\"{1}\"", key, Attributes[key]);
            }
            sb.Append(" tabIndex=\"").Append(TabIndex).Append("\"");

            if (!Enabled) 
                sb.Append(" disabled");

            sb.Append(">");

            string[] list = IncludeTerritories ? s_statesAndTerritories : m_states;

            foreach (string item in list) 
            {
                sb.Append("<option value=\"").Append(item).Append("\"");
                if (item == m_value) 
                {
                    sb.Append(" selected=\"selected\"");
                }
                sb.Append(">").Append(item).Append("</option>");
            }
            sb.Append("</select>");
            output.Write(sb.ToString());
		}
	}
}
