using System;
using System.Collections.Specialized;
using System.Web;
using System.Web.UI;
using System.ComponentModel;

namespace MeridianLink.CommonControls
{
	/// <summary>
	/// Summary description for TimeTextBox.
	/// </summary>
    [DefaultProperty("Text"), 
    ToolboxData("<{0}:TimeTextBox runat='server' />")]	
	public class TimeTextBox : System.Web.UI.Control, IPostBackDataHandler
	{
        private DateTime m_datetime = DateTime.Today; // Set default to current with time default at 12:00 AM.

        public string VirtualRoot 
        {
            get 
            {
                if (Context != null) 
                {
                    return Page.Request.ApplicationPath == "/" ? "" :Page.Request.ApplicationPath;
                } 
                else 
                {
                    return "";
                }
            }
        }
        /// <summary>
        /// Gets/Sets AM/PM part of time. Valid values are "AM", "PM".
        /// </summary>
		public TimeTextBox()
		{

		}
        /// <summary>
        /// Set time in following format: HH:MM AM/PM
        /// </summary>
        public string Text 
        {
            get { return m_datetime.ToShortTimeString(); } 
            set 
            {
                try 
                {
                    m_datetime = DateTime.Parse(value);
                } 
                catch 
                {
                    m_datetime = DateTime.Today;
                }
            }
        }

        public DateTime Time 
        {
            get { return m_datetime; }
        }

        public bool LoadPostData(String postDataKey, NameValueCollection values) 
        {
            string hr = values[this.UniqueID];
            string mm = values[this.UniqueID + "_minute"];
            string am = values[this.UniqueID + "_am"];
            

            m_datetime = DateTime.Parse(string.Format("{0}:{1} {2}", hr, mm, am));
            return false;
        }

        public void RaisePostDataChangedEvent() 
        {

            // Part of the IPostBackDataHandler contract.  Invoked if we ever returned true from the
            // LoadPostData method (indicates that we want a change notification raised).  Since we
            // always return false, this method is just a no-op.
        }

        protected override void Render(System.Web.UI.HtmlTextWriter writer) 
        {
            int hour = 12;
            if (m_datetime.Hour > 0 && m_datetime.Hour < 13)
                hour = m_datetime.Hour;
            else if (m_datetime.Hour > 12)
                hour = m_datetime.Hour - 12;

            writer.Write(string.Format(@"<input type=text name={0} maxlength=2 style=""width:20px"" onkeyup=""time_onhourkeyup(this, '{0}_minute');"" onblur=""time_onhourblur(this);"" value={1:00} id={2}> ",
                this.UniqueID, hour, this.ClientID));
            writer.Write(string.Format(@" : <input type=text name={0}_minute maxlength=2 style=""width:20px"" onblur=""time_onminuteblur(this);"" value={1:00} id={2}_minute>", this.UniqueID, m_datetime.Minute, this.ClientID));
            writer.Write(string.Format(@"<select name={0}_am id={3}_am><option {1} value='AM'>AM</option><option {2} value='PM'>PM</option></select>",
                this.UniqueID,
                m_datetime.Hour > 11 ? "" : "selected",
                m_datetime.Hour > 11 ? "selected" : "",
                this.ClientID));
        }

        /// <summary>
        /// Register to include js file on this page.
        /// </summary>
        /// <param name="js"></param>
        protected void RegisterJsScript(string js) 
        {
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), js, string.Format("<script src='{0}/inc/{1}'></script>", VirtualRoot, js));
        }
        protected override void OnInit(EventArgs e) 
        {
            RegisterJsScript("mask.js");
            base.OnInit(e);
        }

	}
}
