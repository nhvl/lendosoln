﻿using System;
using System.Web.UI.WebControls;

namespace MeridianLink.CommonControls
{
    public class EncodedLiteral : Literal
    { 
        public new LiteralMode Mode
        {
            get
            {
                return LiteralMode.Encode;
            }
        }

        public EncodedLiteral()
        {
            base.Mode = LiteralMode.Encode;
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.Mode = LiteralMode.Encode;
            base.OnPreRender(e);
        }
    }
}
