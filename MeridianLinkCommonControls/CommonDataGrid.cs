using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;
using System.Web.UI.HtmlControls;

namespace MeridianLink.CommonControls
{
    /// <summary>
    /// 09/15/03  - Chuong changed OnItemCreated to support HeaderImageUrl
    /// 08/15/03  - Chuong,
	///				Implemented feature that supports multi-column sort
    ///             Multiple columns must be separated by commas.
    /// 4/17/2003 - Implemented feature of sorting without using Viewstate.
    ///             Before, Viewstate is storing the whole content of the table
    ///             even though we never resuse the content. Viewstate is required
    ///             for sorting to work. Therefore page size is direct proportional to
    ///             number of rows. Now that requirement is no longer there
    ///             page size should be reduce significantly.
    /// Usage: it is also simplify a little bit than before.
    ///        
    ///        protected CommonDataGrid m_myDatagrid;
    ///        DataSet ds; // Fill dataset from SqlDataAdapter.
    ///        m_myDataGrid.DataSource = ds.Tables[0].DefaultView;
    ///        m_myDataGrid.DataBind();
    ///        
    /// Notice: Sorting will only work if you set DataView object to DataSource, else no
    /// sorting will take place.
    ///   Also, ItemCommand WILL NOT worked. Therefore if you need to have bound ItemCommand event
    ///   please see me. I promise there will be a document on this :-).
    /// 
    /// </summary>
    [ToolboxData("<{0}:CommonDataGrid runat=server></{0}:CommonDataGrid>")] 
    public class CommonDataGrid : System.Web.UI.WebControls.DataGrid, IPostBackEventHandler
    {
        private string m_defaultSortExpression;

        /// <summary>
        /// Gets/Sets default sort expression when datagrid is first display.
        /// </summary>
        public string DefaultSortExpression 
        {
            get { return m_defaultSortExpression; }
            set { m_defaultSortExpression = value; }
        }

        public event CommandEventHandler ItemCustomCommand;    

        public string VirtualRoot 
        {
            get 
            {
                return (Context == null || Page.Request.ApplicationPath == "/") ? "" : Page.Request.ApplicationPath;
            }
        }
        private string m_currentSortKey;
        private string m_currentSortDirection;
        public CommonDataGrid()
        {
            // Set standard properties.
            this.Width = new Unit(100, UnitType.Percentage);
            this.CssClass = "DataGrid";
            this.AlternatingItemStyle.CssClass = "GridAlternatingItem";
            this.AlternatingItemStyle.VerticalAlign = VerticalAlign.Top;
            this.HeaderStyle.CssClass = "GridHeader";
            this.ItemStyle.CssClass = "GridItem";
            this.ItemStyle.VerticalAlign = VerticalAlign.Top;
            this.AllowSorting = true;
            this.AutoGenerateColumns = false;
            this.EnableViewState = false;
        }
        protected override void OnInit(EventArgs e) 
        {
            this.Page.ClientScript.GetPostBackEventReference(this, ""); // NEED TO HAVE THIS LINE in order for .NET to register __doPostBack
            base.OnInit(e);
        }
        protected override void OnItemCreated(System.Web.UI.WebControls.DataGridItemEventArgs e) 
        {
            base.OnItemCreated(e);

            //Page.Trace.Warn(this.ClientID + " - OnItemCreate");
            if (e.Item.ItemType != ListItemType.Header) return;

            for (int index = 0; index < Columns.Count; index++) 
            {
                string sortExpr = Columns[index].SortExpression;
                if (sortExpr != "") 
                {
                    string sortDir = (sortExpr != m_currentSortKey  || m_currentSortDirection == "DESC") ? "ASC" : "DESC";
                    e.Item.Cells[index].Controls.Clear();
            
                    //string img = string.Format("&nbsp;<img src='{0}/images/Tri_{1}.gif' border=0>", VirtualRoot, m_currentSortDirection);
                    HtmlImage img = new HtmlImage();
                    img.Src = string.Format("{0}/images/Tri_{1}.gif", VirtualRoot, m_currentSortDirection);
                    img.Border = 0;
                    HyperLink link = new HyperLink();

                    if (Columns[index].HeaderImageUrl != "")
                    {
                        HtmlImage headerImage = new HtmlImage();
                        headerImage.Border = 0;
                        headerImage.Src = Columns[index].HeaderImageUrl;
                        //link.Text = string.Format("<img src='{0}' border=0>", Columns[index].HeaderImageUrl);
                        link.Controls.Add(headerImage);
                    }
                    
                    if (sortExpr == m_currentSortKey) 
                    {
                        if (Columns[index].HeaderText != "")
                        {
                            Literal literal = new Literal();
                            literal.Text = Columns[index].HeaderText + " ";
                            link.Controls.Add(literal);
                            link.Controls.Add(img);
                            //link.Text += Columns[index].HeaderText + img;
                        }
                    }
                    else 
                    {
                        Literal literal = new Literal();
                        literal.Text = Columns[index].HeaderText + " ";
                        link.Controls.Add(literal);

                        //link.Text += Columns[index].HeaderText ;
                    }
                    // USE Page.GetPostBackClientHyperlink;
                    
                    string[] d = sortExpr.Split(',');
                    if (d.Length > 0)
						link.NavigateUrl = string.Format("javascript:__doPostBack('{0}','sort:{1}')", this.ClientID, d[0] + " " + sortDir + sortExpr.Substring(d[0].Length));
                    else
	                    link.NavigateUrl = string.Format("javascript:__doPostBack('{0}','sort:{1} {2}')", this.ClientID, sortExpr, sortDir);
            
                    e.Item.Cells[index].Controls.Add(link);
                }
            }
            

        }
        public void RaisePostBackEvent(string eventArgument)
        {
            if (ItemCustomCommand != null) 
            {
                string cmd = "";
                string arg = "";
                if (eventArgument != null) 
                {
                    // Event argument is either cmd:arg or arg
                    string[] s = eventArgument.Split(':');
                    cmd = s.Length == 1 ? "" : s[0];
                    arg = s.Length == 1 ? s[0] : s[1];
                }

                ItemCustomCommand(this, new CommandEventArgs(cmd, arg));
            }
        }

        public bool DoesClientSendSortCmd
        {
            get
            {
                if (Page.Request.Form["__EVENTTARGET"] == this.ClientID) 
                {
                    string arg = Page.Request.Form["__EVENTARGUMENT"];
                    return arg != null && arg.StartsWith("sort:"); 
                }
                return false;
            }
        }


        protected override void OnDataBinding(EventArgs e) 
        {
            //Page.Trace.Warn(this.ClientID + " - OnDataBinding");
            if (Page.IsPostBack) 
            {
                // Get previous sortexpression;
                string sort = Page.Request.Form[this.ClientID + "_PreviousSort"];
                if (Page.Request.Form["__EVENTTARGET"] == this.ClientID) 
                {
                    if (Page.Request.Form["__EVENTARGUMENT"].StartsWith("sort:")) 
                    {
                        sort = Page.Request.Form["__EVENTARGUMENT"].Substring(5);
                    }
                }

                // Only support sorting on dataview right now.
                if (DataSource is System.Data.DataView) 
                {
                    // If no sort key then use default sort expression.
                    if (sort == null || sort.Trim() == "") 
                    {
                        sort = m_defaultSortExpression ;
                    }
                    if (sort != null && sort != "") 
                    {
                        string[] d = sort.Split(',') ;
                        if (d.Length == 0)
                        {
							d = sort.Split(' ') ;
							m_currentSortKey = d.Length > 0 ? d[0] : "" ;
							m_currentSortDirection = d.Length > 1 ? d[1] : "" ;
                        }
                        else
                        {
							string temp = sort.Substring(d[0].Length) ;
							d = d[0].Split(' ') ;
							if (d.Length == 0)
							{
								m_currentSortKey = sort ;
								m_currentSortDirection = "" ;
							}
							else
							{
								m_currentSortKey = d[0] + temp ;
								m_currentSortDirection = d.Length > 1 ? d[1] : "" ;
							}
                        }
							
                        System.Data.DataView view = (System.Data.DataView) DataSource;

						try
						{
							// 2/5/2005 kb - We should always try to set the sort
							// for this view, but watch out!  If the view has no
							// column info, then we'll throw looking for the col
							// in the empty view.  We don't care if this fails,
							// right?

							view.Sort = sort;
						}
						catch
						{
						}
                    } 
                }

                Page.ClientScript.RegisterHiddenField(this.ClientID + "_PreviousSort", sort);
            } 
            else 
            {
                // Take care of default sort expression on first load here.
                if (DataSource is System.Data.DataView) 
                {
                    if (m_defaultSortExpression != null && m_defaultSortExpression.Trim() != "") 
                    {
						string[] d = m_defaultSortExpression.Split(',') ;
						if (d.Length == 0)
						{
							d = m_defaultSortExpression.Split(' ') ;
							m_currentSortKey = d.Length > 0 ? d[0] : "" ;
							m_currentSortDirection = d.Length > 1 ? d[1] : "" ;
						}
						else
						{
							string temp = m_defaultSortExpression.Substring(d[0].Length) ;
							d = d[0].Split(' ') ;
							if (d.Length == 0)
							{
								m_currentSortKey = m_defaultSortExpression ;
								m_currentSortDirection = "" ;
							}
							else
							{
								m_currentSortKey = d[0] + temp ;
								m_currentSortDirection = d.Length > 1 ? d[1] : "" ;
							}
						}
                        
                        System.Data.DataView view = (System.Data.DataView) DataSource;

						try
						{
							// 2/5/2005 kb - We should always try to set the sort
							// for this view, but watch out!  If the view has no
							// column info, then we'll throw looking for the col
							// in the empty view.  We don't care if this fails,
							// right?

							view.Sort = m_defaultSortExpression;
						}
						catch
						{
						}

                        Page.ClientScript.RegisterHiddenField(this.ClientID + "_PreviousSort", m_defaultSortExpression);
                    }
                }
            }
            base.OnDataBinding(e);
        }


    }
}
