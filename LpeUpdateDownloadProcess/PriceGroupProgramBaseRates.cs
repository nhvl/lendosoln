﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using LendersOffice.LoanPrograms;

namespace LpeUpdateDownloadProcess
{
    public class PriceGroupProgramBaseRates
    {
        public Guid PriceGroupId { get; set; }
        public Guid ProgramId { get; set; }
        public RateSheetLineItems Rates { get; set; }
    }
}
