﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Collections;
using LendersOfficeApp.ObjLib.AcceptableRatesheet;
using CommonLib;
using LpeUpdateBotLib.Common;
using LendersOffice.Common;
using LqbGrammar.DataTypes;
using LendersOffice.Drivers.SqlServerDB;

namespace LpeUpdateDownloadProcess
{
    public class DownloadInfoWeb : AbstractDownloadInfo, IDownloadInfo
    {
        private string m_description = "";
        private string m_url = "";
        private string[] m_fileNames = new string[0]; // OPM 26294
        private string m_mainFileName = ""; // OPM 26294
        private DateTime m_dLastSuccessfulDL = DateTime.MinValue;
        private string m_lastDlResult = "";
        private int m_errorCount = -1;
        private string m_conversion = "";
        private string m_botType = "";
        private string m_login = "";
        private string m_password = "";
        private DateTime m_lastRatesheetTimestamp = DateTime.MinValue;
        private string m_token = "";

        protected override string TableName
        {
            get { return "Download_List"; }
        }

        #region Public Properties
        public string Description
        {
            get { return m_description; }
            set { m_description = value; }
        }
        public string Url
        {
            get { return m_url; }
            set { m_url = value; }
        }

        // OPM 26294
        public string[] FileNames
        {
            get { return m_fileNames; }
            set
            {
                m_fileNames = value ?? new string[0];
                if (m_fileNames.Length > 0)
                {
                    m_mainFileName = m_fileNames[0];
                }
            }
        }

        // OPM 26294
        public string MainFileName
        {
            get { return m_mainFileName; }
            set
            {
                m_mainFileName = value;
                if (m_fileNames != null)
                {
                    if (m_fileNames.Length > 0)
                    {
                        m_fileNames[0] = m_mainFileName;
                    }
                    else
                    {
                        m_fileNames = new string[] { m_mainFileName };
                    }
                }
            }
        }

        public DateTime DLastSuccessfulDL
        {
            get { return m_dLastSuccessfulDL; }
        }
        public string LastDlResult
        {
            get { return m_lastDlResult; }
        }
        public int ErrorCount
        {
            get { return m_errorCount; }
        }
        public string Conversion
        {
            get { return m_conversion; }
        }
        public string BotType
        {
            get { return m_botType; }
            set { m_botType = value; }
        }
        public string Login
        {
            get { return m_login; }
        }
        public string Password
        {
            get { return m_password; }
        }
        public DateTime LastRatesheetTimestamp
        {
            get { return m_lastRatesheetTimestamp; }
        }
        public string Token
        {
            get { return m_token; }
        }

        /// <summary>
        /// Gets a value indicating whether the bot is running as an LP Live bot.
        /// Implemented in OPM 184983, this is indicated by "(LP_Live)" in the <see cref="BotType"/>.
        /// These bots allow for the user to specify when to download the ratesheet.
        /// </summary>
        public bool IsLPLive { get; private set; }
        #endregion

        public DownloadInfoWeb()
        {
        }
        public DownloadInfoWeb(IDataReader reader)
        {
            m_id = SafeConvert.ToInt(reader["ID"]);
            m_url = SafeConvert.ToString(reader["URL"]);
            m_description = SafeConvert.ToString(reader["Description"]);
            m_login = SafeConvert.ToString(reader["Login"]);
            m_password = SafeConvert.ToString(reader["Password"]);
            m_conversion = SafeConvert.ToString(reader["Conversion"]);
            m_lastDlResult = SafeConvert.ToString(reader["LastDLResult"]);
            m_errorCount = SafeConvert.ToInt(reader["ErrorCount"]);
            m_fileNames = ParseFileNameList(SafeConvert.ToString(reader["FileName"])); // OPM 26294
            if (m_fileNames != null && m_fileNames.Length > 0)
                m_mainFileName = m_fileNames[0]; // OPM 26294
            m_botType = SafeConvert.ToString(reader["BotType"]);
            m_lastRatesheetTimestamp = SafeConvert.ToDateTime(reader["LastRatesheetTimestamp"]); //BB (iOPM 32384)
            m_token = SafeConvert.ToString(reader["Token"]);

            int indexOfLPLiveFlag = this.BotType.IndexOf(LendersOffice.Constants.ConstAppDavid.LPLiveBotFlag, StringComparison.OrdinalIgnoreCase);
            if (indexOfLPLiveFlag >= 0)
            {
                this.IsLPLive = true;
                this.BotType = this.BotType.Remove(indexOfLPLiveFlag, LendersOffice.Constants.ConstAppDavid.LPLiveBotFlag.Length).Trim();
            }
        }

        public void RecordLastRatesheetTimestamp(DateTime timestamp)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@Id", m_id));

            if (timestamp == DateTime.MinValue)
            {
                parameters.Add(new SqlParameter("@LastRatesheetTimestamp", null));
            }
            else
            {
                parameters.Add(new SqlParameter("@LastRatesheetTimestamp", timestamp));
            }

            DataAccess.StoredProcedureHelper.ExecuteNonQuery(DataAccess.DataSrc.RateSheet, "RS_UpdateDownloadListRatesheetTimestamp", 0, parameters);
        }

        public void RecordLastSuccessfulDL()
        {
            SQLUpdateStringBuilder sqlb = new SQLUpdateStringBuilder();
            sqlb.AddNonString("dLastSuccessfulDL", "GETDATE()");
            sqlb.AddString("LastDlResult", "SUCCESS");
            sqlb.AddString("ErrorCount", "0");

            UpdateDB(sqlb);
        }

        public void Deactivate()
        {
            SQLUpdateStringBuilder sqlb = new SQLUpdateStringBuilder();
            sqlb.AddString("Active", "F");

            var listParams = new SqlParameter[] { DataAccess.DbAccessUtils.SqlParameterForVarchar("@login", this.m_login),
                                                    DataAccess.DbAccessUtils.SqlParameterForVarchar("@desc", this.m_description) };

            var sql = "UPDATE Download_List " + sqlb.ToString() + " WHERE Login=@login AND Description = @desc";

            DataAccess.DBUpdateUtility.Update(DataAccess.DataSrc.RateSheet, sql, null, listParams);
        }

        public void RecordLastSuccessfulDLAndLastUpdated()
        {
            SQLUpdateStringBuilder sqlb = new SQLUpdateStringBuilder();
            sqlb.AddNonString("dLastUpdated", "GETDATE()");
            sqlb.AddNonString("dLastSuccessfulDL", "GETDATE()");
            sqlb.AddString("LastDlResult", "SUCCESS");
            sqlb.AddString("ErrorCount", "0");

            UpdateDB(sqlb);
        }

        public void RecordLastUpdated()
        {
            SQLUpdateStringBuilder sqlb = new SQLUpdateStringBuilder();
            sqlb.AddNonString("dLastUpdated", "GETDATE()");
            sqlb.AddString("LastDlResult", "SUCCESS");
            sqlb.AddString("ErrorCount", "0");

            UpdateDB(sqlb);
        }

        public void RecordErrorMessage(Exception exc)
        {
            if ((m_lastDlResult != SafeString.Left(exc.Message, 50).Trim()) || m_errorCount >= 4)
            {
                m_errorCount = -1;
            }
            m_errorCount++;

            SQLUpdateStringBuilder sqlb = new SQLUpdateStringBuilder();
            sqlb.AddString("LastDlResult", exc.Message, 50);
            sqlb.AddString("ErrorCount", m_errorCount.ToString());

            UpdateDB(sqlb);
        }

        public void ExpireRatesheet()
        {
            foreach (string ratesheetName in m_fileNames)
            {
                ExpireRatesheet(ratesheetName);
            }
        }

        // OPM 21897 - allow bots to expire pricing for a particular ratesheet
        private void ExpireRatesheet(string ratesheetName)
        {
            string botName = (m_botType == "") ? "Generic Bot" : m_botType;

            // First look up the ratesheet ID from its name
            // Then mark all acceptable_rs_files associated with that ratesheet ID as expired

            try
            {
                // Set the IsMapAndBotWorkingOk value to false to immediately expire this output file
                using (DbDataReader reader = DataAccess.StoredProcedureHelper.ExecuteReader(DataAccess.DataSrc.RateSheet, "RS_InvestorXlsFile_GetIdByFilename", new SqlParameter("@InvestorXlsFileName", ratesheetName)))
                {
                    if (reader.Read())
                    {
                        long investorXlsFileId = (long)reader["InvestorXlsFileId"];
                        List<SqlParameter> parameters = new List<SqlParameter>();
                        parameters.Add(new SqlParameter("@IsBothRsMapAndBotWorking", false));
                        parameters.Add(new SqlParameter("@InvestorXlsFileId", investorXlsFileId));

                        DataAccess.StoredProcedureHelper.ExecuteNonQuery(DataAccess.DataSrc.RateSheet, "RS_File_UpdateRsAndBotWorking", 0, parameters);
                        DataAccess.Tools.LogInfo(string.Format("<RATESHEET_EXPIRATION>{2}Ratesheet: {0}{2}Status: EXPIRED{2}Reason: Ratesheet '{0}' has been expired by bot '{1}'.", ratesheetName, botName, Environment.NewLine));
                    }
                    else
                        throw new Exception("<RATESHEET_EXPIRATION> Cannot expire ratesheet because no entry for it is found in the Investor Ratesheets table.");
                }

                // Mark the IsExpirationIssuedByInvestor bit to true for the latest version of all output files associated with this ratesheet
                AcceptableRsFileVersion.SetExpirationIssuedByInvestorValue(ratesheetName, true);
            }
            catch (Exception e)
            {
                DataAccess.Tools.LogErrorWithCriticalTracking(string.Format("<RATESHEET_EXPIRATION> Unable to expire ratesheet '{0}' via bot '{1}'.  Exception details: {2}", ratesheetName, botName, e.ToString()));
            }
        }
    }

}
