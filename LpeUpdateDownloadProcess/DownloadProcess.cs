﻿namespace LpeUpdateDownloadProcess
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Diagnostics;
    using CommonProjectLib.Runnable;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Drivers.Gateways;
    using LpeUpdateBotLib.Common;

    public class DownloadProcess : IRunnable
    {
        public const string ARCHIVE_DATE_FORMAT = "_yyMMdd_HHmm";

        public string Description { get { return "Runs bots to download ratesheets."; } }

        public DownloadProcess()
        {
            PathCfg.VerifyAndSetupRelatedPaths();
            Logger.SetImpl(new PaulBunyanLogger());
        }

        public void Run()
        {
            if (Config.IsDuringLpeUpdatePeriod == false)
            {
                return;
            }

            var monitorLog = new ServiceExecutionLogging.Logger("LpeDownload");

            Tools.ResetLogCorrelationId();
            Tools.SetupServicePointManager();
            ReportingTools.Reset();

            Stopwatch sw = Stopwatch.StartNew();

            try
            {
                FileCfg.CurrentRunTime = DateTime.Now;
                if (Config.IsDuringDownloadBotPeriod)
                {
                    ReportingTools.Log("About to start DownloadProcess.DownloadFromWeb");
                    DownloadFromWeb();
                }
                else
                {
                    ReportingTools.Log("Skip DownloadProcess.DownloadFromWeb due to time constraint.");
                }
                // 10/12/2011 dd - Record the last time this lpeupdate run successfully.
                TextFileHelper.WriteString(Config.FILES_PATH + "LastLpeDownloadRun.txt", DateTime.Now.ToString(), false);
                monitorLog.Record(ServiceExecutionLogging.Logger.StatusType.OK, ConstAppDavid.ServerName + " - Executed in " + sw.ElapsedMilliseconds.ToString("0,0") + " ms.");
            }
            catch (Exception exc)
            {
                ReportingTools.Log("EXCEPTION");
                ReportingTools.Log(exc.ToString());
                monitorLog.Record(ServiceExecutionLogging.Logger.StatusType.FAILED, ConstAppDavid.ServerName + " - ERROR - " + exc.Message);
                throw;
            }
            finally
            {
                ReportingTools.Log("Total Processing Time " + sw.ElapsedMilliseconds.ToString("0,0") + " ms.");
                Tools.LogInfo("LpeDownload", "[LpeDownload] Executed in " + sw.ElapsedMilliseconds.ToString("0,0") + " ms." + Environment.NewLine + ReportingTools.GetLog());
            }
        }

        private void DownloadFromWeb()
        {
            List<AbstractWebBot> botList = new List<AbstractWebBot>();

            string sSql = "SELECT * FROM DOWNLOAD_LIST WHERE Active='T'";

            Action<IDataReader> readHandler = delegate(IDataReader reader)
            {
                while (reader.Read())
                {
                    DownloadInfoWeb info = new DownloadInfoWeb(reader);

                    try
                    {
                        if (info.IsLPLive)
                        {
                            info.Deactivate(); // NOT SURE IF THIS IS OK.  Cues off of Description and Login; I'd rather trust ID.
                        }

                        AbstractWebBot webBot = WebBotFactory.CreateBot(info);
                        if (null != webBot)
                        {
                            botList.Add(webBot);
                        }
                        else
                        {
                            LpeAdminMessaging.SendEmail(E_LpeErrorAreaT.WebBotDownload, "Unhandle Bot Type=" + info.BotType, "Unhandle Bot Type=" + info.BotType);
                        }
                    }
                    catch (Exception exc)
                    {
                        LpeAdminMessaging.SendEmail(E_LpeErrorAreaT.WebBotDownload, "Unable to create bot [" + info.BotType + "].", exc.ToString());
                    }
                }
            };

            DBSelectUtility.ProcessDBData(DataSrc.RateSheet, sSql, null, null, readHandler);

            MultiThreadsDownloader.MultiThreadsDownloader.Execute(botList, Config.NumberOfDownloadThreads);
        }

        /// <summary>
        /// Postfix the date and time to the given string
        /// </summary>
        /// <param name="str"></param>
        /// <returns>The postfixed string with form [str]_[yyMMdd]_[HHmm]
        /// Year, month, day, hour, and minute segments will each be 2 digits long.  
        /// "HH" formats the hour in military time.
        /// If the string has an extension, the postfix will be inserted before it.
        /// </returns>
        public static string PostFixDatetime(string str)
        {
            string pf = DateTime.Now.ToString(ARCHIVE_DATE_FORMAT);
            int extIndex = str.LastIndexOf(".");

            if (extIndex > -1)
                return str.Substring(0, extIndex) + pf + str.Substring(extIndex);
            return str + pf;
        }

    }
}