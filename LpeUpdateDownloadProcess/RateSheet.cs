﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LpeUpdateDownloadProcess
{
    class RateSheet
    {
        public string RawFile { get; set; }
        public string ExcelFile { get; set; }
        public RateSheet()
        {
        }
        public RateSheet(string rawFile, string excelFile)
        {
            RawFile = rawFile;
            ExcelFile = excelFile;
        }
    }
}
