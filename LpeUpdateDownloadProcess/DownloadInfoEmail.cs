﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using CommonLib;
using System.Data.SqlClient;
using LpeUpdateBotLib.Common;
using LendersOfficeApp.ObjLib.AcceptableRatesheet;
using System.IO;
using System.Xml;
using System.Text.RegularExpressions;

namespace LpeUpdateDownloadProcess
{
    class DownloadInfoEmail : AbstractDownloadInfo, IEmailDownloadInfo
    {
        private string m_sender = "";
        private string m_body = "";
        private string[] m_targetFileNames = new string[0]; // OPM 26294
        private string m_mainTargetFileName = "";  // OPM 26294
        private string m_botType = "";
        private string m_mailFileName = "";
        private string m_subject = "";
        private string m_description = "";  // OPM 458236

        private Dictionary<string, PriceGroupProgramBaseRates> m_rates = new Dictionary<string, PriceGroupProgramBaseRates>();
        private List<string> m_subjectList = new List<string>();

        public string Sender
        {
            get { return m_sender; }
        }

        public string Body
        {
            get { return m_body; }
        }

        // OPM 26294
        public string[] TargetFileNames
        {
            get { return m_targetFileNames; }
            set
            {
                m_targetFileNames = value ?? new string[0];
                if (m_targetFileNames.Length > 0)
                    m_mainTargetFileName = m_targetFileNames[0];
            }
        }

        // OPM 26294
        public string MainTargetFileName
        {
            get { return m_mainTargetFileName; }
            set
            {
                m_mainTargetFileName = value;
                if (m_targetFileNames != null)
                {
                    if (m_targetFileNames.Length > 0)
                    {
                        m_targetFileNames[0] = m_mainTargetFileName;
                    }
                    else
                    {
                        m_targetFileNames = new string[] { m_mainTargetFileName };
                    }
                }
            }
        }

        public string MailFileName
        {
            get { return m_mailFileName; }
        }
        public string BotType
        {
            get { return m_botType; }
        }

        public string Subject
        {
            get { return m_subject; }
        }

        protected override string TableName
        {
            get { return "Email_Download_List"; }
        }

        public string Description
        {
            get { return m_description; }
        }

        public DownloadInfoEmail(IDataReader reader)
        {
            m_id = SafeConvert.ToInt(reader["ID"]);
            m_sender = SafeConvert.ToString(reader["sender"]).ToUpper(); // OPM 24047
            m_body = SafeConvert.ToString(reader["body"]);
            m_mailFileName = SafeConvert.ToString(reader["mailfilename"]);
            m_targetFileNames = ParseFileNameList(SafeConvert.ToString(reader["targetfilename"]));
            if (m_targetFileNames != null && m_targetFileNames.Length > 0)
                m_mainTargetFileName = m_targetFileNames[0];
            m_botType = SafeConvert.ToString(reader["bottype"]);
            m_subject = SafeConvert.ToString(reader["subject"]);

            m_subjectList.Add(SafeConvert.ToString(reader["subject"]));
            m_subjectList.Add(SafeConvert.ToString(reader["subject1"]));
            m_subjectList.Add(SafeConvert.ToString(reader["subject2"]));
            m_subjectList.Add(SafeConvert.ToString(reader["subject3"]));
            m_subjectList.Add(SafeConvert.ToString(reader["subject4"]));
            m_subjectList.Add(SafeConvert.ToString(reader["subject5"]));
            m_subjectList.Add(SafeConvert.ToString(reader["subject6"]));
            m_subjectList.Add(SafeConvert.ToString(reader["subject7"]));
            m_subjectList.Add(SafeConvert.ToString(reader["subject8"]));
            m_subjectList.Add(SafeConvert.ToString(reader["subject9"]));

            m_description = SafeConvert.ToString(reader["Description"]);
        }
        public void UpdateLastUpdated()
        {
            SQLUpdateStringBuilder sqlb = new SQLUpdateStringBuilder();
            sqlb.AddNonString("dLastUpdated", "GETDATE()");

            UpdateDB(sqlb);
        }

        public void PersistRateData(DateTime startTime, DateTime endTime, string outputFileName)
        {

            if (m_rates.Count == 0)
            {
                Logger.LogErrorAndSendEmail("No rates found - Feedbot not enabling programs.");
                return; 
            }


            foreach (PriceGroupProgramBaseRates rate in m_rates.Values)
            {
                if (!rate.Rates.IsValidRateOption())
                {
                    Logger.LogErrorAndSendEmail("Rates did not validate.");
                    return;
                }
            }

            AcceptableRsFile rsFile = new AcceptableRsFile(outputFileName);
            rsFile.UseForBothMapAndBot = true;
            if (rsFile.DeploymentType.ToLower() == "useversionaftergeneratinganoutputfileok")
            {
                rsFile.DeploymentType = "UseVersion";
            }
            rsFile.Save(); 

            
            AcceptableRsFileVersion version = new AcceptableRsFileVersion();
            version.FirstEffective = DateTime.Now;
            version.IsExpirationIssuedByInvestor = false;
            version.LastestEffective = DateTime.Now;
            version.RatesheetFileId = outputFileName;
            version.Save();

            foreach (PriceGroupProgramBaseRates rate in m_rates.Values)
            {
            
                SqlParameter[] parmaeters = new SqlParameter[] {
                    new SqlParameter("@LpePriceGroupId", rate.PriceGroupId),
                    new SqlParameter("@lLpTemplateId", rate.ProgramId),
                    new SqlParameter("@lRateSheetXmlContent", rate.Rates.ToString()),
                    new SqlParameter("@LpeAcceptableRsFileId", outputFileName),
                    new SqlParameter("@LpeAcceptableRsFileVersionNumber", version.VersionNumber),
                    new SqlParameter("@lRateSheetDownloadStartD", startTime),
                    new SqlParameter("@lRateSheetDownloadEndD", endTime)
                };

                DataAccess.StoredProcedureHelper.ExecuteNonQuery(DataAccess.DataSrc.LpeSrc, "PriceGroupRateOptionUpload", 2, parmaeters);
            }

            m_rates.Clear();
        }

        private PriceGroupProgramBaseRates GetSelectedRate(Guid priceGroupId, Guid templateId)
        {
            string key = String.Format("{0}_{1}", priceGroupId.ToString("N"), templateId.ToString("N"));
            PriceGroupProgramBaseRates selectedRates;

            if (!m_rates.TryGetValue(key, out selectedRates))
            {
                selectedRates = new PriceGroupProgramBaseRates();
                selectedRates.PriceGroupId = priceGroupId;
                selectedRates.ProgramId = templateId;
                selectedRates.Rates = new LendersOffice.LoanPrograms.RateSheetLineItems();
                m_rates.Add(key, selectedRates);
            }

            return selectedRates;
        }
        public void RegisterRate(Guid priceGroupId, Guid templateId, string rate, string point, string margin, string qualrate)
        {
            PriceGroupProgramBaseRates selectedRates = GetSelectedRate(priceGroupId, templateId);
            selectedRates.Rates.Add(rate, point, margin, qualrate, "");
        }

        public void RegisterLockPeriod(Guid priceGroupId, Guid templateId, int start, int end)
        {
            PriceGroupProgramBaseRates selectedRates = GetSelectedRate(priceGroupId, templateId);
            selectedRates.Rates.Add("LOCK", start.ToString(), end.ToString(), "", "");
        }

        public void ExpireCurrentRateSheet()
        {
            AcceptableRsFile.ExpireOnNewRatesheetDetected(m_mailFileName);
        }

        /// <summary>
        /// Check to see if given subject match with a list of subject rules.
        /// OPM 454294
        /// </summary>
        /// <param name="subject">Subject to match.</param>
        /// <returns>Return true if subject match with subject rules.</returns>
        public bool IsSubjectMatch(string subject)
        {
            string regExKeyword = "regex::";

            if (subject == null)
            {
                subject = string.Empty; // Treat null as empty string.
            }

            foreach (var subjectToMatch in m_subjectList)
            {
                if (string.IsNullOrEmpty(subjectToMatch))
                {
                    continue;
                }

                // 5/31/2017 dd - Only perform regex if it start with "regex::"
                bool isRegExMatch = subjectToMatch.StartsWith(regExKeyword, StringComparison.OrdinalIgnoreCase);
                if (isRegExMatch)
                {
                    string regExPattern = subjectToMatch.Substring(regExKeyword.Length);

                    bool isMatch = Regex.IsMatch(subject, regExPattern, RegexOptions.IgnoreCase);

                    if (isMatch)
                    {
                        return true; // Subject only need to match to 1 of the rule.
                    }
                }
                else
                {
                    if (subject.IndexOf(subjectToMatch, StringComparison.OrdinalIgnoreCase) >= 0)
                    {
                        return true;
                    }

                }
            }

            return false;
        }
    }

}
