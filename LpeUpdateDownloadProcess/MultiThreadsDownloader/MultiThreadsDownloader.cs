﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LpeUpdateBotLib.Common;
using System.Threading;

namespace LpeUpdateDownloadProcess.MultiThreadsDownloader
{
    public static class MultiThreadsDownloader
    {
        private static List<AbstractWebBot> s_workList;
        private static object s_workListLock = new object();

        private static void SetWorkList(List<AbstractWebBot> workList)
        {
            lock (s_workListLock)
            {
                s_workList = new List<AbstractWebBot>();
                foreach (var o in workList)
                {
                    s_workList.Add(o);
                }
            }
        }

        public static AbstractWebBot GetNextWorkList()
        {
            lock (s_workListLock)
            {
                if (s_workList == null || s_workList.Count == 0)
                {
                    return null;
                }

                AbstractWebBot o = s_workList[0];
                s_workList.RemoveAt(0);
                return o;
            }
        }

        public static void Execute(List<AbstractWebBot> workList, int threadCount)
        {


            SetWorkList(workList);

            AbstractDownloader[] downloaderList = new AbstractDownloader[threadCount];

            for (int i = 0; i < threadCount; i++)
            {
                AbstractDownloader downloader = new ThreadBotDownloader(i);
                downloaderList[i] = downloader;
                downloader.StartThread();
            }
            bool isDone = false;
            while (isDone == false)
            {
                isDone = true;
                foreach (var downloader in downloaderList)
                {
                    if (downloader.IsExceedTimeout())
                    {
                        downloader.AbortCurrentDownload();
                    }

                    if (downloader.IsAlive)
                    {
                        isDone = false;
                    }
                }
                if (isDone == false)
                {
                    Thread.Sleep(5000);
                }
            }

        }
    }
}
