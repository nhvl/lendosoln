﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LpeUpdateBotLib.Common;
using LendersOffice.Common;
using System.Threading;
using System.Diagnostics;

namespace LpeUpdateDownloadProcess.MultiThreadsDownloader
{
    abstract class AbstractDownloader
    {
        private const string AbortManualObject = "AbortManually";
        private Thread m_thread = null;
        private Stopwatch m_stopwatch = null;
        private long m_timeoutInMs = 0;
        private object m_lock = new object();

        public bool IsAlive
        {
            get { return m_thread.IsAlive; }
        }

        protected AbstractDownloader(int threadIndex)
        {
            m_thread = new Thread(this.Execute);
            m_thread.Name = "Thread_" + threadIndex;
        }

        public void StartThread()
        {
            m_thread.Start();
        }
        public void AbortCurrentDownload()
        {
            m_thread.Abort(AbortManualObject);
        }
        public bool IsExceedTimeout()
        {
            lock (m_lock)
            {
                if (m_stopwatch == null || m_timeoutInMs <= 0)
                {
                    return false;
                }
                return (m_stopwatch.ElapsedMilliseconds > m_timeoutInMs);
            }

        }
        protected abstract void Download(AbstractWebBot workItem);

        public void Execute()
        {
            AbstractWebBot workItem = GetNextWorkItem();

            while (null != workItem)
            {
                try
                {
                    lock (m_lock)
                    {
                        m_stopwatch = Stopwatch.StartNew();
                        m_timeoutInMs = workItem.TimeoutInMs;
                    }
                    Download(workItem);
                }
                catch (ThreadAbortException exc)
                {
                    string msg = exc.ExceptionState as string;

                    if (msg != null && msg.Equals(AbortManualObject))
                    {
                        LpeAdminMessaging.SendEmail(E_LpeErrorAreaT.WebBotDownload, workItem.BotName + " is TIMEOUT.", "Download of " + workItem.BotName + " FAILED due to timeout.");

                        // 9/22/2011 dd - Resume download of different bot.
                        Thread.ResetAbort();
                    }
                }
                catch (Exception exc)
                {
                    LpeAdminMessaging.SendEmail(E_LpeErrorAreaT.WebBotDownload, exc.Message, exc.ToString());
                }
                workItem = GetNextWorkItem();
            }
        }

        private AbstractWebBot GetNextWorkItem()
        {
            return MultiThreadsDownloader.GetNextWorkList();
        }

    }
}
