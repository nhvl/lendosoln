﻿namespace LpeUpdateDownloadProcess.MultiThreadsDownloader
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Diagnostics;
    using System.Linq;
    using System.Text;
    using DataAccess;
    using LendersOffice.Drivers.Gateways;
    using LendersOfficeApp.ObjLib.AcceptableRatesheet;
    using LpeUpdateBotLib.Common;

    class ThreadBotDownloader : AbstractDownloader
    {
        public ThreadBotDownloader(int threadIndex)
            : base(threadIndex)
        {
        }
        protected override void Download(AbstractWebBot bot)
        {
            if (bot == null)
                return;

            ReportingTools.Log("Begin Download " + bot.BotName);
            Stopwatch stopwatch = Stopwatch.StartNew();

            //OPM 19397
            CurrentRateLockExpActivatedRatesheets activatedRatesheets = new CurrentRateLockExpActivatedRatesheets();
            string status = "UNKNOWN";
            try
            {
                RateSheet rs = null;
                bool isDownloadSuccessful = false;
                E_BotDownloadStatusT downloadStatus = bot.Download();
                if (downloadStatus == E_BotDownloadStatusT.SuccessfulWithRatesheet)
                {
                    string fileName = bot.RatesheetFileName;
                    isDownloadSuccessful = true;
                    rs = RateSheetLibrary.GetRateSheet(fileName, bot.LibraryKey, ExcelConvert.ToMethodType(bot.ConversionMethod), bot.Info);
                }
                else if (downloadStatus == E_BotDownloadStatusT.SuccessfulWithNoRatesheet)
                {
                    isDownloadSuccessful = true;
                    rs = null;
                }
                else if (downloadStatus == E_BotDownloadStatusT.SuccessfulWithManualRatesheet)
                {
                    string fileName = bot.RatesheetFileName;
                    isDownloadSuccessful = true;
                    rs = RateSheetLibrary.GetRateSheet(fileName, bot.LibraryKey, ExcelConvert.ToMethodType(bot.ConversionMethod), bot.Info);
                }
                if (rs != null)
                {
                    // put files in DownloadPath
                    // OPM 26294 - make multiple copies if specified in the download list
                    string[] sFileNames = bot.Info.FileNames;

                    foreach (string sFileName in sFileNames)
                    {
                        Tools.FileCopy(rs.ExcelFile, PathCfg.BOT_DOWNLOAD_PATH + sFileName);
                    }

                    // put a copy in the archive
                    // OPM 26294 - per the case, we only need to add the main (first) filename in the list to the archive
                    Tools.FileCopy(rs.ExcelFile, PathCfg.DOWNLOAD_RsArchive + DownloadProcess.PostFixDatetime(bot.Info.MainFileName));

                    status = "OK_HAS_RS";
                    if (isDownloadSuccessful)
                    {
                        bot.Info.RecordLastSuccessfulDLAndLastUpdated();
                    }
                    else
                    {
                        bot.Info.RecordLastUpdated();
                    }

                    // OPM 26294 - do this for all filenames specified in the download list
                    // Mark the deployment type of all lpe acceptable rs files as UseVersionAfterGeneratingAnOutputFileOk
                    // Since we have detected a new ratesheet.  The deployment type will get reset to UseVersion
                    // once the lpe acceptable file has been successfully uploaded into the system.
                    // NOTE: Only lpe acceptable files whose current deployment type is UseVersion will get changed here
                    // If the deployment type is set to AlwaysBlocked, for example, that would not get overridden here

                    AcceptableRsFile.ExpireOnNewRatesheetDetected(sFileNames);
                    AcceptableRsFileVersion.SetExpirationIssuedByInvestorValue(sFileNames, false);
                }
                else
                {
                    // No rateshet return.
                    if (isDownloadSuccessful)
                    {
                        status = "OK_RS_NOT_CHANGE";
                        bot.Info.RecordLastSuccessfulDL();

                        // OPM 23763
                        // We want to detect if an investor sent a re-price notice, but then released the same
                        // ratesheet that they had released earlier. If this happens, we need to un-expire pricing. 
                        foreach (string sFileName in bot.Info.FileNames)
                        {
                            string fullDownloadPath = System.IO.Path.Combine(PathCfg.BOT_DOWNLOAD_PATH, sFileName);

                            if (!activatedRatesheets.IsRatesheetMarkedOk(sFileName.ToUpper()) && !FileOperationHelper.Exists(fullDownloadPath))
                            {
                                // Per case 21897, also check if the IsExpirationIssuedByInvestor value for the latest
                                // version of the output files associated with this ratesheet has been set to true.  
                                // If so, we do not want to automatically unexpire here - 
                                // we will only unexpire when a new ratesheet has been downloaded.
                                bool isExpirationIssuedByInvestor = false;
                                try
                                {
                                    using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.RateSheet, "RS_File_GetAccRsFileIdsByInvXlsFileName", new SqlParameter("@InvestorXlsFileName", sFileName)))
                                    {
                                        while (reader.Read())
                                        {
                                            string lpeAcceptableRsFileId = reader["LpeAcceptableRsFileId"].ToString();

                                            // db - If any of the output files' current versions have their expiration issued by investor,
                                            // then consider the whole ratesheet to have the isExpirationIssuedByInvestor value to be true.
                                            using (DbDataReader reader2 = StoredProcedureHelper.ExecuteReader(DataSrc.RateSheet, "RS_FileVersion_GetIsExpirationIssuedByInvestorByAccRsFile", new SqlParameter("@LpeAcceptableRsFileId", lpeAcceptableRsFileId)))
                                            {
                                                while (reader2.Read())
                                                {
                                                    isExpirationIssuedByInvestor = isExpirationIssuedByInvestor || (bool)reader2["IsExpirationIssuedByInvestor"];
                                                }
                                            }
                                        }
                                    }
                                }
                                catch (Exception e)
                                {
                                    DataAccess.Tools.LogError("An error occurred while trying to determine the IsExpirationIssuedByInvestor value for ratesheet: " + sFileName + ".  IsExpirationIssuedByInvestor is currently set to: " + isExpirationIssuedByInvestor, e);
                                }

                                //string msg = String.Format("LpeUpdate has detected that ratesheet '{0}' is currently expired, and the current version of this ratesheet that was just downloaded is the same as a previous version.  Therefore, this ratesheet will not process and will not be un-expired.  Please check to see if it needs to be manually un-expired (see internal case 23763).", bot.Info.FileName.ToUpper());
                                if (isExpirationIssuedByInvestor == false)
                                {
                                    activatedRatesheets.MarkStatus(sFileName.ToUpper(), E_RateLockExpActivatedRSStatus.IsWorking);
                                    string msg = string.Format("<RATESHEET_EXPIRATION>{1}Ratesheet: {0}{1}Status: UNEXPIRED{1}Reason: Ratesheet '{0}' status is being marked as 'IsWorking' and (therefore should be unexpired), because the newly downloaded ratesheet is the same as the previous version, and the investor has not issued an expiration message.{1}Location: ThreadBotDownloader.Download function, rs == null else block.", sFileName, Environment.NewLine);
                                    ReportingTools.Log(msg);
                                }
                            }
                        }
                    }
                    else
                    {
                        activatedRatesheets.MarkStatus(bot.Info.FileNames, E_RateLockExpActivatedRSStatus.IsNotWorking);
                    }
                }
            }
            catch (Exception exc)
            {
                status = "ERROR";
                string errorMessage = "[WebBotDownloadError] - " + bot.Info.MainFileName + " failed.";
                Tools.LogError(errorMessage, exc);
                bot.Info.RecordErrorMessage(exc);

                // OPM 26294 - we will mark all filenames as expired because if we got to this exception handler,
                // something most likely went wrong with the actual download itself, in which case all ratesheet copies
                // are affected.  If we run into problems where over-expiration is occurring because an exception should
                // only have affected 1 of the ratesheets, we will need to pull this entire functionality out and
                // into a loop so we can catch the specific exception for each individual ratesheet copy.
                activatedRatesheets.MarkStatus(bot.Info.FileNames, E_RateLockExpActivatedRSStatus.IsNotWorking);
                //foreach (string sFileName in bot.Info.FileNames)
                //{
                //    activatedRatesheets.MarkStatus(sFileName.ToUpper(), E_RateLockExpActivatedRSStatus.IsNotWorking);
                //    string msg = string.Format("<RATESHEET_EXPIRATION>{1}Ratesheet: {0}{1}Status: EXPIRED{1}Reason: Ratesheet '{0}' status is being marked as 'IsNotWorking' because an exception occurred during the download phase.{1}Location: ThreadBotDownloader.Download function, Catch Exception block.", sFileName, Environment.NewLine);
                //    Tools.LogInfo(msg);
                //}
            }
            finally
            {
                activatedRatesheets.Save(); //OPM 19397
            }
            stopwatch.Stop();
            string str = string.Format("Finish Download BotId={1}, BotName=[{5}], BotType={2} in {3}. Status={4}.", System.Threading.Thread.CurrentThread.Name, ((DownloadInfoWeb)bot.Info).Id, bot.Info.BotType,
                FriendlyDisplay(stopwatch.Elapsed), status, bot.BotName);
            ReportingTools.Log(str);
        }

        private string FriendlyDisplay(TimeSpan ts)
        {
            return string.Format("{0:0#}:{1:0#}:{2:0#}", ts.Hours, ts.Minutes, ts.Seconds);
        }

    }
}
