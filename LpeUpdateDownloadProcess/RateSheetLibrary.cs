﻿namespace LpeUpdateDownloadProcess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Xml;
    using DataAccess;
    using LendersOffice.Drivers.Gateways;
    using LpeUpdateBotLib.Common;

    /// <summary>
    /// Keeps track of previously downloaded files so that we do not process duplicate data.
    /// 
    /// Raw/Excel files are stored in numerical formats with .raw or .xls extension. They're
    /// mapped to the key by index.xml.
    /// </summary>
    class RateSheetLibrary
    {
        /// <summary>
        /// Converts the specified file to a ratesheet, compares the values with the current ratesheet to determine if update is needed,
        /// and replaces the existing ratesheet with the new version if updated.
        /// </summary>
        /// <param name="fileName">The name of the candidate file to replace the existing ratesheet.</param>
        /// <param name="libraryKey">The key to the existing ratesheet in the library of existing ratesheets.</param>
        /// <param name="conversionMethod">The conversion method necessary to create the ratesheet.</param>
        /// <param name="botInfo">The download info corresponding to the current bot.</param>
        /// <returns>The new ratesheet, or null if the newly converted ratesheet matches the existing ratesheet.</returns>
        public static RateSheet GetRateSheet(string fileName, string libraryKey, ExcelConvertMethodType conversionMethod, IDownloadInfo botInfo)
        {
            RateSheet rs = null;

            if (null != fileName)
            {
                ExcelConvert convert = new ExcelConvert();
                rs = convert.ConvertToRateSheet(fileName, conversionMethod);

                DownloadInfoWeb downloadInfo = botInfo as DownloadInfoWeb;
                if ((downloadInfo == null || !downloadInfo.IsLPLive) && AlreadyExists(libraryKey, rs))
                {
                    rs = null;
                }

                if (null != rs)
                {
                    CheckIn(libraryKey, rs);
                }
            }

            return rs;
        }

        private static bool AlreadyExists(string sKey, RateSheet rsToCheck)
        {
            string sId = GetIndexId(sKey);
            if (sId == null) return false;

            RateSheet rsArchive = GetRateSheet(sId);
            return Tools.IsFileIdentical(rsToCheck.RawFile, rsArchive.RawFile);
        }
        private static void CheckIn(string sKey, RateSheet rsCheckIn)
        {
            string sId = GetIndexId(sKey);
            if (sId == null) sId = AddIndexId(sKey);

            RateSheet rsArchive = GetRateSheet(sId);

            Tools.FileCopy(rsCheckIn.RawFile, rsArchive.RawFile);
            Tools.FileCopy(rsCheckIn.ExcelFile, rsArchive.ExcelFile);
        }
        private static RateSheet GetRateSheet(string sId)
        {
            return new RateSheet(PathCfg.MapLibraryPath(sId + ".raw"), PathCfg.MapLibraryPath(sId + ".xls"));
        }
        private static string GetIndexId(string sKey)
        {
            lock (s_xdIndexLock)
            {
                // Not using selectSingleNode because sKey will probably contain characters not
                // compatible with XPATH. Maybe we can cache the index in a dictionary in the future?
                foreach (XmlElement xe in s_xdIndex.DocumentElement.ChildNodes)
                {
                    if (xe.InnerText == sKey)
                        return xe.GetAttribute("id");
                }
            }

            return null;
        }
        private static string AddIndexId(string sKey)
        {
            lock (s_xdIndexLock)
            {
                XmlElement xe = s_xdIndex.CreateElement("entry");
                xe.SetAttribute("id", s_xdIndex.DocumentElement.ChildNodes.Count.ToString());
                xe.InnerText = sKey;
                s_xdIndex.DocumentElement.AppendChild(xe);

                // save the new Index to Disk
                s_xdIndex.Save(INDEX_PATH);

                return xe.GetAttribute("id");
            }
        }
        static XmlDocument s_xdIndex;
        static string INDEX_PATH;

        private static object s_xdIndexLock = new object();
        static RateSheetLibrary()
        {
            lock (s_xdIndexLock)
            {
                INDEX_PATH = PathCfg.MapLibraryPath("index.xml");
                s_xdIndex = new XmlDocument();
                if (!FileOperationHelper.Exists(INDEX_PATH))
                    s_xdIndex.LoadXml("<index/>");
                else
                    s_xdIndex.Load(INDEX_PATH);
            }
        }

    }
}
