﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommonLib;
using System.Data.SqlClient;
using LqbGrammar.DataTypes;
using LendersOffice.Drivers.SqlServerDB;

namespace LpeUpdateDownloadProcess
{
    public abstract class AbstractDownloadInfo
    {
        protected int m_id = -1;
        protected DateTime m_dLastUpdated = DateTime.MinValue;

        public int Id
        {
            get { return m_id; }
        }

        public DateTime DLastUpdated
        {
            get { return m_dLastUpdated; }
        }

        protected abstract string TableName
        {
            get;
        }

        // OPM 26294
        protected string[] ParseFileNameList(string fileNames)
        {
            string[] fileNameList = new string[0];

            if (string.IsNullOrEmpty(fileNames))
                return fileNameList;


            fileNameList = fileNames.Split(',');

            for (int i = 0; i < fileNameList.Length; ++i)
            {
                if (string.IsNullOrEmpty(fileNameList[i]) == false)
                {
                    fileNameList[i] = fileNameList[i].Trim().ToUpper();
                }
            }


            return fileNameList;
        }

        protected void UpdateDB(SQLUpdateStringBuilder sqlb)
        {
            if (m_id == -1)
                return;

            // NOTE: Table is hard-coded in inheriting classes so it doesn't require validation

            var listParams = new SqlParameter[] { new SqlParameter("@id", this.m_id) };
            string sSql = string.Format("UPDATE {0} {1} WHERE Id=@id", TableName, sqlb);

            DataAccess.DBUpdateUtility.Update(DataAccess.DataSrc.RateSheet, sSql, null, listParams);
        }
    }

}
