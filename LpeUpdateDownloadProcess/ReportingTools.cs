﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace LpeUpdateDownloadProcess
{
    static class ReportingTools
    {
        private static StringBuilder s_buffer = null;
        private static object s_lock = new object();

        private static void Initialize()
        {
            s_buffer = new StringBuilder();
        }
        public static void Log(string message)
        {
            lock (s_lock)
            {
                if (null == s_buffer)
                {
                    Initialize();
                }

                s_buffer.AppendFormat("[{0}] - {1} - {2}{3}", DateTime.Now.ToLongTimeString(), string.IsNullOrEmpty(Thread.CurrentThread.Name) ? "" : "[" + Thread.CurrentThread.Name + "]", message, Environment.NewLine);
            }
        }

        public static string GetLog()
        {
            lock (s_lock)
            {
                if (null != s_buffer)
                {
                    return s_buffer.ToString();
                }
                return string.Empty;
            }
        }
        public static void Reset()
        {
            lock (s_lock)
            {
                s_buffer = null;
            }
        }
    }
}
