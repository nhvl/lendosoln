﻿namespace LpeUpdateDownloadProcess
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.IO;
    using System.Threading;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Drivers.Gateways;
    using LendingQBPop3Mail;
    using LpeUpdateBotLib.Common;

    class EmailServer
    {
        private const int MaxNumberOfRetries = 5;

        public static IEnumerable<Pop3Message> GetNewMessages()
        {
            ReportingTools.Log("About to connect to POP3 Server");

            string emailAddress = string.Empty;
            string emailPassword = string.Empty;
            string lastMessageMarker = string.Empty;

            if (Config.IsTestingMode) // OPM 23968
            {
                emailAddress = Config.PmlRatesTestEmailAddress;
                emailPassword = Config.PmlRatesTestEmailAddressPassword;
            }
            else
            {
                emailAddress = ConstSite.PmlRatesEmailAddress;
                emailPassword = Config.PmlRatesEmailAddressPassword;
            }

            string smtpServer = ConfigurationManager.AppSettings["PopMailServer"];

            if (string.IsNullOrEmpty(smtpServer))
            {
                smtpServer = "11.12.13.32";
            }

            lastMessageMarker = GetLastMessageMarker();

            Exception e = null;

            for (int numOfRetries = 0; numOfRetries < MaxNumberOfRetries; numOfRetries++)
            {
                try
                {
                    var result = GetNewMessagesImpl(smtpServer, emailAddress, emailPassword, lastMessageMarker);

                    SetLastMessageMarker(result.Item2);
                    ReportingTools.Log("The new last email message marker is [" + result.Item2 + "]");

                    return result.Item1;
                }
                catch (Exception exc)
                {
                    ReportingTools.Log("ERROR: Tries #" + numOfRetries + ". Exc=" + exc);
                    e = exc;
                    Thread.Sleep(30000); // Sleep before retry.
                }
            }

            var msg = $"Unable to connect to POP server '{smtpServer}' after " + MaxNumberOfRetries + " retries.";
            DataAccess.Tools.LogError(msg, e);
            throw new Exception(msg);
        }

        private static Tuple<IEnumerable<Pop3Message>, string> GetNewMessagesImpl(string smtpServer, string emailAddress, string emailPassword, string lastMessageMarker)
        {
            List<Pop3Message> messageList = new List<Pop3Message>();
            Pop3Class pop3 = new Pop3Class();
            pop3.Connect(emailAddress, emailPassword, smtpServer, 110, false);

            ReportingTools.Log(String.Format(pop3.Description + " - Connected to POP3 successfully (site =[{0}], server=[{1}]).", emailAddress, smtpServer));
            try
            {
                if (pop3.Count == 0)
                {
                    // 10/6/2014 dd - OPM 192987 - When there is no new email in inbox then stop early.
                    ReportingTools.Log("No email in inbox.");

                    return Tuple.Create<IEnumerable<Pop3Message>, string>(new List<Pop3Message>(), lastMessageMarker);
                }


                int nIndex = 1;
                if (null != lastMessageMarker && lastMessageMarker.Length > 0)
                {
                    // skip messages that are already processed
                    for (int i = 1; i <= pop3.Count; i++)
                    {
                        if (lastMessageMarker == pop3.GetMessageUID(i))
                        {
                            nIndex = i;
                            break;
                        }
                    }
                }
                // retrieve the list of unprocessed mails
                int nLastMsg = pop3.Count; /// put pop.count in temp variable so that we won't break if new emails arrive
                ReportingTools.Log("There are " + (nLastMsg - nIndex) + " new email messages. Last Message Marker=[" + lastMessageMarker + "]");
                for (int i = nIndex + 1; i <= nLastMsg; i++)
                {
                    Pop3Message msg = pop3.GetMessage(i);
                    string str = "Msg[" + i + "] - From: [" + msg.From + "] Date: [" + msg.Date + "] Subj: [" + msg.Subject + "] UID: [" + pop3.GetMessageUID(i) + "]";
                    ReportingTools.Log(str);
                    messageList.Add(msg);
                }

                // mark the last message marker so that we do not process duplicate messages
                lastMessageMarker = pop3.GetMessageUID(nLastMsg);

                return Tuple.Create<IEnumerable<Pop3Message>, string>(messageList, lastMessageMarker);
            }
            catch (Exception exc)
            {
                LpeAdminMessaging.SendEmail(E_LpeErrorAreaT.EmailBotDownload, exc.Message, exc.ToString());
                throw;
            }
            finally
            {
                try
                {
                    pop3.Disconnect();
                }
                catch (IOException exc)
                {
                    // 11/10/2015 - dd - For some strange reason there are random error when try to disconnect.
                    // To avoid waste a good messages I will ignore this.
                    ReportingTools.Log("WARNING: Unable to disconnect. Exception=" + exc);
                }
            }
        }

        private static string GetLastMessageMarker()
        {
            if (FileOperationHelper.Exists(FileCfg.LAST_MESSAGE_FLAG) == false)
            {
                return string.Empty;
            }

            string line = null;
            Action<TextFileHelper.LqbTextReader> readHandler = delegate(TextFileHelper.LqbTextReader sr)
            {
                line = sr.ReadLine();
            };

            TextFileHelper.OpenRead(FileCfg.LAST_MESSAGE_FLAG, readHandler);
            return line;
        }

        private static void SetLastMessageMarker(string marker)
        {
            if (FileOperationHelper.Exists(FileCfg.LAST_MESSAGE_FLAG))
            {
                FileOperationHelper.Delete(FileCfg.LAST_MESSAGE_FLAG);
            }

            Action<TextFileHelper.LqbTextWriter> writeHandler = delegate(TextFileHelper.LqbTextWriter sw)
            {
                sw.AppendLine(marker);
            };

            TextFileHelper.OpenNew(FileCfg.LAST_MESSAGE_FLAG, writeHandler);
        }
    }
}