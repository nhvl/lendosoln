﻿namespace LpeUpdateDownloadProcess
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Diagnostics;
    using CommonLib;
    using CommonProjectLib.Runnable;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Drivers.Gateways;
    using LendersOfficeApp.ObjLib.AcceptableRatesheet;
    using LendingQBPop3Mail;
    using LpeUpdateBotLib.Common;

    public class EmailDownloadProcess : IRunnable
    {
        public string Description
        {
            get { return "Run bots to download email."; }
        }

        public EmailDownloadProcess()
        {
            PathCfg.VerifyAndSetupRelatedPaths();
            Logger.SetImpl(new PaulBunyanLogger());
        }

        public void Run()
        {
            if (Config.IsDuringLpeUpdatePeriod == false)
            {
                return;
            }

            Tools.ResetLogCorrelationId();
            ReportingTools.Reset();
            Stopwatch sw = Stopwatch.StartNew();
            try
            {
                FileCfg.CurrentRunTime = DateTime.Now;

                ReportingTools.Log("About to start EmailDownloadProcess.DownloadFromEmail");
                DownloadFromEmail();

                // 10/12/2011 dd - Record the last time this Lpe Email Download run successfully.
                TextFileHelper.WriteString(Config.FILES_PATH + "LastLpeEmailDownloadRun.txt", DateTime.Now.ToString(), false);
            }
            catch (Exception exc)
            {
                ReportingTools.Log("EXCEPTION");
                ReportingTools.Log(exc.ToString());
                throw;
            }
            finally
            {
                ReportingTools.Log("Total Processing Time " + sw.ElapsedMilliseconds.ToString("0,0") + " ms.");
                Tools.LogInfo("LpeEmailDownload", "[LpeEmailDownload] Executed in " + sw.ElapsedMilliseconds.ToString("0,0") + " ms." + Environment.NewLine + ReportingTools.GetLog());
            }
        }

        /// <summary>
        /// Entry point from Schedule Executable.
        /// </summary>
        /// <param name="args"></param>
        public static void Execute(string[] args)
        {
            EmailDownloadProcess process = new EmailDownloadProcess();
            process.Run();
        }

        private void DownloadFromEmail()
        {
            List<MailMask> listMailMask = new List<MailMask>();
            List<DownloadInfoEmail> listEmailBot = new List<DownloadInfoEmail>();

            string sSQL = "SELECT * FROM Email_Download_List WHERE Active='T'";

            Action<IDataReader> readHandler = delegate(IDataReader sdr)
            {
                while (sdr.Read())
                {
                    DownloadInfoEmail info = new DownloadInfoEmail(sdr);

                    if (info.BotType != "")
                    {
                        listEmailBot.Add(info);
                    }
                    else
                    {
                        listMailMask.Add(new MailMask(info));
                    }
                }
            };

            DBSelectUtility.ProcessDBData(DataSrc.RateSheet, sSQL, null, null, readHandler);

            try
            {
                foreach (var msg in EmailServer.GetNewMessages())
                {
                    if (msg.Date.AddDays(2) < DateTime.Now)
                    {
                        // 4/9/2012 dd - Skip if email is older than 2 days.
                        ReportingTools.Log("SKIP EMAIL: " + msg.Date + " - From: [" + msg.From + "] - Subject [" + msg.Subject + "]");
                    }
                    // Preprocess these compare fields. We always compare in upper case format.
                    msg.From = SafeConvert.ToString(msg.From).ToUpper();
                    msg.FromName = SafeConvert.ToString(msg.FromName).ToUpper();

                    CurrentRateLockExpActivatedRatesheets activatedRatesheets = null;
                    try
                    {
                        DataAccess.Tools.LogInfo("Process " + msg.From);

                        bool bMatchFound = false;

                        activatedRatesheets = new CurrentRateLockExpActivatedRatesheets(); //OPM 19451
                        #region Look for email bot
                        // 9/14/2006 nw - OPM 6897 - Look for email bot before trying to match attachments
                        foreach (DownloadInfoEmail botInfo in listEmailBot)
                        {
                            if (botInfo.Sender.Length > 0 && msg.From.IndexOf(botInfo.Sender) < 0 && msg.FromName.IndexOf(botInfo.Sender) < 0)  //iOPM 204291
                            {
                                continue;
                            }

                            // 5/1/2017 dd - OPM 454294 - Perform subject match against list of rules.
                            if (!botInfo.IsSubjectMatch(msg.Subject))
                            {
                                continue; // Email subject does not match for this bot. Skip.
                            }

                            try
                            {
                                DownloadInfoWeb dlinfo = new DownloadInfoWeb();
                                dlinfo.Url = "EMAIL://" + botInfo.BotType.ToUpper(); // This will be the default RSLibraryKey
                                dlinfo.FileNames = botInfo.TargetFileNames; // OPM 21897
                                dlinfo.BotType = botInfo.BotType; // OPM 21897

                                MailMask mailMask = new MailMask(botInfo);

                                AbstractEmailBot bot = WebBotFactory.CreateEmailBot(dlinfo, msg);

                                if (bot == null)
                                {
                                    LpeAdminMessaging.SendEmail(E_LpeErrorAreaT.EmailBotDownload, "No Email Bot for BotType=" + botInfo.BotType, "No Email Bot for BotType=" + botInfo.BotType);
                                    continue;
                                }

                                if (!bot.IsValidEmailMessage(mailMask, msg))
                                {
                                    continue;
                                }
                                bMatchFound = true;
                                ProcessEmailBot(bot, botInfo, activatedRatesheets);
                            }
                            catch (Exception ex)
                            {
                                string errorMessage = GenerateErrorMessege(ex, msg);
                                string subject = "Error processing email: " + (msg == null ? "" : msg.From);
                                LpeAdminMessaging.SendEmail(E_LpeErrorAreaT.EmailBotDownload, subject, errorMessage);
                                Tools.LogError(ex);
                                continue;
                            }
                        }
                        #endregion

                        if (bMatchFound)
                            continue;		// msg only contains link and no attachment, move on to the next msg

                        // for each mask; this allows us to process
                        // multiple attachment in a single email

                        // Preprocess these compare fields. We always compare in upper case format.
                        msg.From = SafeConvert.ToString(msg.From).ToUpper();
                        msg.FromName = SafeConvert.ToString(msg.FromName).ToUpper();
                        #region Look in MailMask List
                        foreach (MailMask mask in listMailMask)
                        {
                            Pop3Attachment atm;
                            if ((atm = mask.Match(msg)) != null)
                            {
                                bMatchFound = true;

                                if (mask.TargetFileNames.Length == 0)
                                {
                                    mask.UpdateLastUpdated();
                                    continue; // skip if the target filename is empty
                                }
                                else if (atm.Equals(MailMask.EMPTY_ATTACHMENT))
                                {
                                    string s = string.Format("EMPTY_ATTACHMENT ERROR WHEN PROCESSING ATTACHMENTS IN SECOND MATCH PHASE \nEMAIL: {0}\nSUBJECT: {1}", msg.From, msg.Subject);
                                    string subject = string.Format("Error processing email: {0} {1}", msg.From, msg.Subject);
                                    LpeAdminMessaging.SendEmail(E_LpeErrorAreaT.EmailBotDownload, subject, s);
                                }
                                else
                                {
                                    string sFileName = mask.MainTargetFileName;

                                    string sRawFile = FileCfg.TEMP_FILE;
                                    atm.SaveToFile(sRawFile);

                                    // skip files that are not NEW
                                    // We don't need to check all of the ratesheet filenames to see if they're identical
                                    // for this entry in the e-mail download list because they are all just copies of 
                                    // each other, so if the main (first in the list) ratesheet is the same, the rest will be too
                                    if (Tools.IsFileIdentical(sRawFile, PathCfg.DOWNLOAD_RawCache + sFileName))
                                    {
                                        Tools.LogVerbose("File has not change..skipping to next item");

                                        foreach (string filename in mask.TargetFileNames)
                                        {
                                            // OPM 23763
                                            // We want to detect if an investor sent a re-price notice, but then released the same
                                            // ratesheet that they had released earlier. If this happens, we need to un-expire pricing.
                                            string fullDownloadPath = System.IO.Path.Combine(PathCfg.BOT_DOWNLOAD_PATH, filename);
                                            if (!activatedRatesheets.IsRatesheetMarkedOk(filename.ToUpper()) && !FileOperationHelper.Exists(fullDownloadPath))
                                            {
                                                // Per case 21897, also check if the IsExpirationIssuedByInvestor value for the latest
                                                // version of the output files associated with this ratesheet has been set to true.  
                                                // If so, we do not want to automatically unexpire here - 
                                                // we will only unexpire when a new ratesheet has been downloaded.
                                                bool isExpirationIssuedByInvestor = GetIsExpirationIssuedByInvestor(filename.ToUpper());

                                                //string saemsg = String.Format("LpeUpdate has detected that ratesheet '{0}' is currently expired, and the current version of this ratesheet that was just downloaded is the same as a previous version.  Therefore, this ratesheet will not process and will not be un-expired.  Please check to see if it needs to be manually un-expired (see internal case 23763).", sFileName.ToUpper());
                                                if (isExpirationIssuedByInvestor == false)
                                                {
                                                    // status is being marked as 'IsWorking' and (therefore should be unexpired), because the newly downloaded ratesheet is the same as the previous version, and the investor has not issued an expiration message.
                                                    activatedRatesheets.MarkStatus(filename.ToUpper(), E_RateLockExpActivatedRSStatus.IsWorking);
                                                }
                                            }
                                        }

                                        continue;
                                    }

                                    // process NEW files
                                    ExcelConvert convert = new ExcelConvert();
                                    string sInputFile;
                                    try
                                    {
                                        sInputFile = convert.Convert(sRawFile, ExcelConvertMethodType.None);
                                        mask.UpdateLastUpdated();
                                    }
                                    catch (Exception exc)
                                    {
                                        foreach (string filename in mask.TargetFileNames)
                                        {
                                            // status is being marked as 'IsNotWorking' because an exception occurred while trying to convert the file
                                            activatedRatesheets.MarkStatus(filename, E_RateLockExpActivatedRSStatus.IsNotWorking); //OPM 19451
                                        }
                                        string errorMessage = GenerateErrorMessege(exc, msg);
                                        string subject = "Error processing email: " + (msg == null ? "" : msg.From);
                                        LpeAdminMessaging.SendEmail(E_LpeErrorAreaT.EmailBotDownload, subject, errorMessage);
                                        continue;
                                    }

                                    foreach (string filename in mask.TargetFileNames)
                                    {
                                        // put copy of file in cache for future duplicate checking
                                        Tools.FileCopy(sRawFile, PathCfg.DOWNLOAD_RawCache + filename);

                                        // create backup of processed cache
                                        Tools.FileCopy(sInputFile, PathCfg.DOWNLOAD_ProcessedCache + filename);

                                        // put file in DownloadPath
                                        Tools.FileCopy(sInputFile, PathCfg.BOT_DOWNLOAD_PATH + filename);

                                        // put a copy in the archive
                                        Tools.FileCopy(sInputFile, PathCfg.DOWNLOAD_RsArchive + PostFixDatetime(filename));

                                        // Mark the deployment type of all lpe acceptable rs files as UseVersionAfterGeneratingAnOutputFileOk
                                        // Since we have detected a new ratesheet.  The deployment type will get reset to UseVersion
                                        // once the lpe acceptable file has been successfully uploaded into the system.
                                        // NOTE: Only lpe acceptable files whose current deployment type is UseVersion will get changed here
                                        // If the deployment type is set to AlwaysBlocked, for example, that would not get overridden here
                                        AcceptableRsFile.ExpireOnNewRatesheetDetected(filename);

                                        AcceptableRsFileVersion.SetExpirationIssuedByInvestorValue(filename, false);
                                    }
                                }
                            }
                        } // end foreach (MailMask mask in listMailMask)
                        #endregion
                        if (!bMatchFound)
                        {
                            bool containsExcelAttachment = false;
                            foreach (var attachment in msg.Attachments)
                            {
                                if (IsExcelFile(attachment.Name))
                                {
                                    containsExcelAttachment = true;
                                    break;
                                }
                            }
                            string subject = string.Empty;
                            string errMsg = string.Empty;
                            if (containsExcelAttachment)
                            {
                                subject = "Unexpected email with Excel. " + msg.From + " / " + msg.Subject;
                                errMsg = string.Format("Unexpected email with Excel attachment:\nEMAIL: {0}\nSUBJECT: {1}\nPlease verify whether the attachment is a rate sheet that needs to be processed.", msg.From, msg.Subject);

                            }
                            else
                            {
                                subject = "No match. " + msg.From + " / " + msg.Subject;
                                errMsg = string.Format("Cannot find attachment mask for\nEMAIL: {0}\nSUBJECT: {1}", msg.From, msg.Subject);
                            }
                            LpeAdminMessaging.SendEmail(E_LpeErrorAreaT.EmailBotDownload, subject, errMsg);
                        }
                    }
                    catch (Exception exc)
                    {
                        string errorMessage = GenerateErrorMessege(exc, msg);
                        string subject = "Error processing email: " + (msg == null ? "" : msg.From);
                        LpeAdminMessaging.SendEmail(E_LpeErrorAreaT.EmailBotDownload, subject, errorMessage);
                    }
                    finally
                    {
                        if (activatedRatesheets != null)
                            activatedRatesheets.Save(); //OPM 19451
                    }
                } // end foreach (jmail.Message msg in emailserver.GetNewMessages())
            }
            catch (Exception exc)
            {
                // 2/3/2014 dd - Send email to critical everytime there is an error when process email ratesheet.
                EmailUtilities.SendToCritical("[LpeUpdateDownlodProcess] - Unable to access LPE email " + ConstSite.PmlRatesEmailAddress, exc.ToString());
                throw;
            }
        }
        private bool IsExcelFile(string name)
        {
            name = name.ToLower();
            return name.EndsWith("xls") || name.EndsWith("xlsx") || name.EndsWith("xlsm") || name.EndsWith("xlsb");
        }
        private void ProcessEmailBot(AbstractEmailBot bot, DownloadInfoEmail botInfo, CurrentRateLockExpActivatedRatesheets activatedRatesheets)
        {
            if (bot.PreProcessEmailAndQuit())
            {
                return;
            }
            #region Process Email Bot
            bool isDownloadSuccessful = false;
            RateSheet rs = null;
            E_BotDownloadStatusT downloadStatus = bot.Download();
            if (downloadStatus == E_BotDownloadStatusT.SuccessfulWithRatesheet)
            {
                string fileName = bot.RatesheetFileName;
                isDownloadSuccessful = true;
                rs = RateSheetLibrary.GetRateSheet(fileName, bot.LibraryKey, ExcelConvert.ToMethodType(bot.ConversionMethod), bot.Info);
            }
            else if (downloadStatus == E_BotDownloadStatusT.SuccessfulWithNoRatesheet)
            {
                isDownloadSuccessful = true;
                rs = null;
            }
            if (rs != null)
            {
                // the bot already checked in the Rate Sheet into the RsLibrary, 
                // copy Excel file into DownloadPath and RsArchive then update the database

                // put file in DownloadPath
                foreach (string sFileName in botInfo.TargetFileNames)
                {
                    Tools.FileCopy(rs.ExcelFile, PathCfg.BOT_DOWNLOAD_PATH + sFileName);
                }

                // put a copy in the archive
                Tools.FileCopy(rs.ExcelFile, PathCfg.DOWNLOAD_RsArchive + PostFixDatetime(botInfo.MainTargetFileName));

                botInfo.UpdateLastUpdated();

                // Mark the deployment type of all lpe acceptable rs files as UseVersionAfterGeneratingAnOutputFileOk
                // Since we have detected a new ratesheet.  The deployment type will get reset to UseVersion
                // once the lpe acceptable file has been successfully uploaded into the system.
                // NOTE: Only lpe acceptable files whose current deployment type is UseVersion will get changed here
                // If the deployment type is set to AlwaysBlocked, for example, that would not get overridden here
                AcceptableRsFile.ExpireOnNewRatesheetDetected(botInfo.TargetFileNames); // Expiration msg logged in function

                // OPM 21897 - remove the IsExpirationIssuedByInvestor value since we've downloaded a new ratesheet
                AcceptableRsFileVersion.SetExpirationIssuedByInvestorValue(botInfo.TargetFileNames, false);
            }
            else
            {
                if (!isDownloadSuccessful)
                {
                    if (!botInfo.BotType.Equals("REPRICE")) // OPM 22789 - we don't want to expire if the matched bot is the reprice bot
                    {
                        ReportingTools.Log("Marking the following ratesheet as 'bot not working' - " + botInfo.MainTargetFileName.ToString().ToUpper());
                        activatedRatesheets.MarkStatus(botInfo.TargetFileNames, E_RateLockExpActivatedRSStatus.IsNotWorking); //OPM 19451

                    }
                    else
                    {
                        // 1/6/2017 - eOPM 1048105 - Add timestamp to reprice email bot.
                        botInfo.UpdateLastUpdated();
                    }
                }
                else
                {
                    foreach (string filename in bot.Info.FileNames)
                    {
                        // OPM 23763
                        // We want to detect if an investor sent a re-price notice, but then released the same
                        // ratesheet that they had released earlier. If this happens, we need to un-expire pricing.
                        string fullDownloadPath = System.IO.Path.Combine(PathCfg.BOT_DOWNLOAD_PATH, filename);
                        if (!activatedRatesheets.IsRatesheetMarkedOk(filename.ToUpper()) && !FileOperationHelper.Exists(fullDownloadPath))
                        {
                            // Per case 21897, also check if the IsExpirationIssuedByInvestor value for the latest
                            // version of the output files associated with this ratesheet has been set to true.  
                            // If so, we do not want to automatically unexpire here - 
                            // we will only unexpire when a new ratesheet has been downloaded.

                            bool isExpirationIssuedByInvestor = GetIsExpirationIssuedByInvestor(filename.ToUpper());

                            //string saemsg = String.Format("LpeUpdate has detected that ratesheet '{0}' is currently expired, and the current version of this ratesheet that was just downloaded is the same as a previous version.  Therefore, this ratesheet will not process and will not be un-expired.  Please check to see if it needs to be manually un-expired (see internal case 23763).", bot.Info.FileName.ToUpper());
                            if (isExpirationIssuedByInvestor == false)
                            {
                                activatedRatesheets.MarkStatus(filename.ToUpper(), E_RateLockExpActivatedRSStatus.IsWorking);

                            }
                        }
                    }
                }
            }
            #endregion

        }

        private bool GetIsExpirationIssuedByInvestor(string ratesheetName)
        {
            bool isExpirationIssuedByInvestor = false;
            try
            {
                using (DbDataReader reader = DataAccess.StoredProcedureHelper.ExecuteReader(DataAccess.DataSrc.RateSheet, "RS_File_GetAccRsFileIdsByInvXlsFileName", new SqlParameter("@InvestorXlsFileName", ratesheetName)))
                {
                    while (reader.Read())
                    {
                        string lpeAcceptableRsFileId = reader["LpeAcceptableRsFileId"].ToString();

                        // db - If any of the output files' current versions have their expiration issued by investor,
                        // then consider the whole ratesheet to have the isExpirationIssuedByInvestor value to be true.
                        using (DbDataReader reader2 = DataAccess.StoredProcedureHelper.ExecuteReader(DataAccess.DataSrc.RateSheet, "RS_FileVersion_GetIsExpirationIssuedByInvestorByAccRsFile", new SqlParameter("@LpeAcceptableRsFileId", lpeAcceptableRsFileId)))
                        {
                            while (reader2.Read())
                            {
                                isExpirationIssuedByInvestor = isExpirationIssuedByInvestor || (bool)reader2["IsExpirationIssuedByInvestor"];
                            }
                        }
                    }
                }
            }
            catch (CBaseException e)
            {
                DataAccess.Tools.LogError("An error occurred while trying to determine the IsExpirationIssuedByInvestor value for ratesheet: " + ratesheetName + ".  IsExpirationIssuedByInvestor is currently set (in the code, not the database) to: " + isExpirationIssuedByInvestor, e);
            }

            return isExpirationIssuedByInvestor;
        }

        /// <summary>
        /// Adds the subject and sender to the exception tostring. 
        /// This is for opm  19423
        /// </summary>
        /// <param name="e"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        private string GenerateErrorMessege(Exception e, Pop3Message msg)
        {
            string errorMsg = e.ToString();
            if (msg != null)
            {
                errorMsg = string.Format("Subject: {0} Sender: {1} Error: {2}", msg.Subject ?? "-empty-", msg.From ?? "-empty-", errorMsg);
            }
            return errorMsg;
        }

        /// <summary>
        /// Postfix the date and time to the given string
        /// </summary>
        /// <param name="str"></param>
        /// <returns>The postfixed string with form [str]_[yyMMdd]_[HHmm]
        /// Year, month, day, hour, and minute segments will each be 2 digits long.  
        /// "HH" formats the hour in military time.
        /// If the string has an extension, the postfix will be inserted before it.
        /// </returns>
        public static string PostFixDatetime(string str)
        {
            string pf = DateTime.Now.ToString(DownloadProcess.ARCHIVE_DATE_FORMAT);
            int extIndex = str.LastIndexOf(".");

            if (extIndex > -1)
                return str.Substring(0, extIndex) + pf + str.Substring(extIndex);
            return str + pf;
        }
    }
}