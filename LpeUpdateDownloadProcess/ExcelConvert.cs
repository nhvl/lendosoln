﻿namespace LpeUpdateDownloadProcess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Threading;
    using DataAccess;
    using ExcelLibraryClient;
    using LendersOffice.Drivers.Gateways;
    using LpeUpdateBotLib.Common;

    public enum ExcelConvertMethodType
    {
        None,
        StripDateTimeFromExcel,
        CwPq,
        Html2Excel,
        CountryWide,
        Pdf2Excel,
        Csv2ExcelByCopy,
        Cpx,
        Xml2Excel,
        ConvertToData
    }

    /// <summary>
    /// Convert files from another format (i.e., HTML, CSV) to Excel
    /// </summary>
    class ExcelConvert
    {
        public static ExcelConvertMethodType ToMethodType(string conversionMethod)
        {
            if (string.IsNullOrEmpty(conversionMethod))
            {
                return ExcelConvertMethodType.None;
            }
            if (conversionMethod == "STRIP_DATETIME_FROM_EXCEL")
            {
                return ExcelConvertMethodType.StripDateTimeFromExcel;
            }
            else if (conversionMethod == "CW_PQ")
            {
                return ExcelConvertMethodType.CwPq;
            }
            else if (conversionMethod == "COUNTRYWIDE")
            {
                return ExcelConvertMethodType.CountryWide;
            }
            else if (conversionMethod == "HTML2EXCEL")
            {
                return ExcelConvertMethodType.Html2Excel;
            }
            else if (conversionMethod == "PDF2EXCEL")
            {
                return ExcelConvertMethodType.Pdf2Excel;
            }
            else if (conversionMethod == "CSV2EXCELBYCOPY")
            {
                return ExcelConvertMethodType.Csv2ExcelByCopy;
            }
            else if (conversionMethod == "CPX")
            {
                return ExcelConvertMethodType.Cpx;
            }
            else if (conversionMethod == "XML2EXCEL")
            {
                return ExcelConvertMethodType.Xml2Excel;
            }
            else if (conversionMethod == "CONVERTTODATA")
            {
                return ExcelConvertMethodType.ConvertToData;
            }
            else
            {
                throw new Exception("Unhandle ConversionMethod=[" + conversionMethod + "]");
            }
        }
        public ExcelConvert()
        {
        }
        public RateSheet ConvertToRateSheet(string sFileName, ExcelConvertMethodType conversionMethod)
        {
            RateSheet rs = new RateSheet();
            switch (conversionMethod)
            {
                case ExcelConvertMethodType.StripDateTimeFromExcel:
                    //case "STRIP_DATETIME_FROM_EXCEL":
                    rs.ExcelFile = sFileName;
                    rs.RawFile = Convert(sFileName, conversionMethod);
                    break;
                case ExcelConvertMethodType.CwPq:
                    //case "CW_PQ":
                    rs.ExcelFile = Convert(sFileName, ExcelConvertMethodType.Html2Excel);
                    rs.RawFile = ConvertToCSV(Convert(sFileName, ExcelConvertMethodType.CountryWide));
                    break;
                default:
                    rs.RawFile = sFileName;
                    rs.ExcelFile = Convert(sFileName, conversionMethod);
                    break;

            }
            return rs;
        }
        public string Convert(string sRawFile, ExcelConvertMethodType conversionMethod)
        {
            string sProcessedFile = FileCfg.TEMP_FILE;
            switch (conversionMethod)
            {
                case ExcelConvertMethodType.CountryWide:
                    //case "COUNTRYWIDE":
                    Countrywide(sRawFile, sProcessedFile);
                    break;
                case ExcelConvertMethodType.Html2Excel:
                    //case "HTML2EXCEL":
                    Html2Excel(sRawFile, sProcessedFile);
                    break;
                case ExcelConvertMethodType.Pdf2Excel:
                    //case "PDF2EXCEL":
                    Pdf2Excel(sRawFile, sProcessedFile);
                    break;
                case ExcelConvertMethodType.Csv2ExcelByCopy:
                    //case "CSV2EXCELBYCOPY":
                    Csv2ExcelByCopy(sRawFile, sProcessedFile);
                    break;
                case ExcelConvertMethodType.StripDateTimeFromExcel:
                    //case "STRIP_DATETIME_FROM_EXCEL":
                    StripDateTime(ConvertToCSV(sRawFile), sProcessedFile);
                    break;
                case ExcelConvertMethodType.Cpx:
                    //case "CPX": // OPM 32391
                    sProcessedFile = sRawFile;
                    break;
                case ExcelConvertMethodType.Xml2Excel:
                    Xml2Cpx(sRawFile, sProcessedFile);
                    break;
                case ExcelConvertMethodType.ConvertToData:
                    // 12/17/2014 dd - OPM 198027 - Rename first worksheet to DATA.
                    ConvertToData(sRawFile, sProcessedFile);
                    break;
                default:
                    if (IsExcelFile(sRawFile))
                    {
                        sProcessedFile = sRawFile;
                        break;
                    }
                    else
                    {
                        throw new ApplicationException("FILE IS NOT A VALID EXCEL FILE. File=" + sRawFile);
                    }
            }

            return sProcessedFile;
        }

        private void Xml2Cpx(string sRawFile, string sProcessedFile)
        {
            // 11/29/2011 dd - eOPM 314172. 
            // The xml file has Excel file structure, therefore we just rename to xml extension and compact it.
            // Excel library will handle open of this xml file.
            // 2/15/2011 dd - When convert html to excel the extension of sSrcHtml must be .htm
            string sTemp = sRawFile + ".xml";
            if (FileOperationHelper.Exists(sTemp))
            {
                FileOperationHelper.Delete(sTemp);
            }
            Tools.FileCopy(sRawFile, sTemp);
            ExcelLibrary.SaveAsExcel(sTemp, sProcessedFile);
        }
        private string ConvertToCSV(string sSrcExcel, string sDestCSV)
        {
            if (IsExcelFile(sSrcExcel))
            {
                if (FileOperationHelper.Exists(sDestCSV)) FileOperationHelper.Delete(sDestCSV);

                ExcelLibrary.ConvertToCSV(sSrcExcel, sDestCSV);
            }
            else
            {
                throw new ApplicationException("FILE IS NOT A VALID EXCEL FILE. File=" + sSrcExcel);
            }

            return sDestCSV;
        }
        private string ConvertToCSV(string sSrcExcel)
        {
            return ConvertToCSV(sSrcExcel, FileCfg.TEMP_FILE);
        }
        private void Countrywide(string sSrcFile, string sDestExcel)
        {
            string[] rgBadRegex = {
                                      @"(\d+)/(\d+)/(\d+)", //e.g. 7/4/05, 07/04/2005, etc.
                                      @"((\d+):){1,2}(\d+)[ ]*[aApP][mM][ ]*[PT]*", //e.g. 12:22:34 PM, 2:30 PM PT
                                      @"(January|February|March|April|May|June|July|August|September|October|November|December) ([0-9]{1,2})([,]{0,1}) ([0-9]{4})", //e.g. July 4, 2005
                                      @"([0-9]{1,2})-([a-zA-Z]+)-([0-9]{2})", //e.g. 04-July-05
                                      @"(Today's 6 Month LIBOR Index:)([\s]*)([0-9]*)[\.]([0-9]*)"
                                  };

            StripBadRegexThenConvert2Excel(sSrcFile, sDestExcel, rgBadRegex);
        }
        private void StripDateTime(string sSrcFile, string sDestFile)
        {
            string[] rgBadRegex = {
									  @"(\d+)/(\d+)/(\d+)", //e.g. 7/4/05, 07/04/2005, etc.
									  @"((\d+):){1,2}(\d+)[ ]*[aApP][mM]", //e.g. 12:22:34 AM, 2:30 PM
									  @"(January|February|March|April|May|June|July|August|September|October|November|December) ([0-9]{1,2})([,]{0,1}) ([0-9]{4})", //e.g. July 4, 2005
			};

            StripBadRegex(sSrcFile, sDestFile, rgBadRegex);
        }

        private void StripBadRegex(string sSrcFile, string sDestFile, string[] rgBadRegex)
        {
            var sbData = new StringBuilder(TextFileHelper.ReadFile(sSrcFile));
            foreach (string regex in rgBadRegex)
                sbData = new System.Text.StringBuilder(Regex.Replace(sbData.ToString(), regex, " ")); //"" isn't allowed as a replacement

            TextFileHelper.WriteString(sDestFile, sbData.ToString(), false);
        }
        private void StripBadRegexThenConvert2Excel(string sSrcFile, string sDestExcel, string[] rgBadRegex)
        {
            string sTempFile = FileCfg.TEMP_FILE;
            StripBadRegex(sSrcFile, sTempFile, rgBadRegex);

            //Html2Excel(sTempFile, sDestExcel);
            ExcelLibrary.SaveAsExcel(sTempFile, sDestExcel);
        }

        private void ConvertToData(string sSrcFile, string sDestExcel)
        {
            // 12/17/2014 dd - OPM 198027 - Rename the worksheet to "DATA", "DATA1", "DATA2", ... "DATA19"
            // DO NOT INSERT new name in middle. Always insert at the end.

            string[] nameList = {"DATA", "DATA1", "DATA2", "DATA3", "DATA4", "DATA5","DATA6", "DATA6", "DATA8","DATA9", 
                                 "DATA10", "DATA11", "DATA12", "DATA13", "DATA14", "DATA15", "DATA16", "DATA17", "DATA18", "DATA19"};

            ExcelLibrary.RenameExcelWorksheet(sSrcFile, sDestExcel, nameList);
            
        }

        private void Html2Excel(string sSrcHtml, string sDestExcel)
        {
            // 2/15/2011 dd - When convert html to excel the extension of sSrcHtml must be .htm
            string sTemp = sSrcHtml + ".htm";
            if (FileOperationHelper.Exists(sTemp))
            {
                FileOperationHelper.Delete(sTemp);
            }
            Tools.FileCopy(sSrcHtml, sTemp);
            ExcelLibrary.SaveAsExcel(sTemp, sDestExcel);
        }

        private void Csv2ExcelByCopy(string sSrcCsv, string sDestExcel)
        {
            string sTemp = sDestExcel.Replace(".txt", ".csv");
            if (FileOperationHelper.Exists(sTemp))
                FileOperationHelper.Delete(sTemp);

            Tools.FileCopy(sSrcCsv, sTemp);
            TextFileHelper.AppendString(sTemp, System.Environment.NewLine + DateTime.Now);
            ExcelLibrary.SaveAsExcel(sTemp, sDestExcel);
            //Html2Excel(sTemp, sDestExcel);
        }

        private void Pdf2Excel(string sSrcPdf, string sDestExcel)
        {
            string sTemp = FileCfg.TEMP_FILE;
            try
            {

                lock (s_lockPdfConvertor)
                {
                    int currentTries = 0;
                    while (true)
                    {
                        PDFConvertor.IPDF2XLS pdf2excel = new PDFConvertor.CPDF2XLSClass();

                        try
                        {
                            if (!pdf2excel.Convert(sSrcPdf, sTemp))
                            {
                                throw new Exception(pdf2excel.LastErrorMessage);
                            }
                            break;
                        }
                        catch (Exception exc)
                        {
                            if (currentTries < 5)
                            {
                                Thread.Sleep(500); // Sleep for half second before continue;
                                currentTries++;
                                continue;
                            }
                            else
                            {
                                throw new ApplicationException("Pdf2Excel failed for " + sSrcPdf, exc);
                            }
                        }
                        finally
                        {
                            System.Runtime.InteropServices.Marshal.ReleaseComObject(pdf2excel);
                        }
                    }
                }

                // PDFConvertor will convert to old version of Excel. Therefore I have to resave the xls to the latest version.
                ExcelLibrary.SaveAsExcel(sTemp, sDestExcel);
            }
            finally
            {
                if (FileOperationHelper.Exists(sTemp))
                    FileOperationHelper.Delete(sTemp);

            }
        }
        /// <summary>
        /// Peak at the file header to determine if it's a valid Excel file or not
        /// </summary>
        private bool IsExcelFile(string sFileName)
        {
            byte[] buf = new byte[m_ExcelBinaryPrefix.Length];
            bool isExcel = true;
            bool isNewerExcel = false;

            Action<BinaryFileHelper.LqbBinaryStream> readHandler = delegate(BinaryFileHelper.LqbBinaryStream fs)
            {
                fs.Stream.Read(buf, 0, buf.Length);

                // 6/20/2011 dd - Check old excel version.
                for (int i = 0; i < buf.Length; i++)
                {
                    if (m_ExcelBinaryPrefix[i] != buf[i])
                    {
                        isExcel = false;
                        break;
                        //return false;
                    }
                }
                if (isExcel == false)
                {
                    isExcel = true; // Assume is newer excel.
                    isNewerExcel = true;

                    List<byte[]> newExcelXslxSignatureList = new List<byte[]> {
                                                                 m_ExcelXlsxBinaryPrefix,
                                                                 m_ExcelXlsxBinaryPrefix2,
                                                                 m_ExcelXlsxBinaryPrefix3,
                                                                 m_ExcelXlsxBinaryPrefix4,
                                                                 m_ExcelXslxBinaryPrefix5
                                                             };

                    isExcel = false;
                    foreach (byte[] expectedSignature in newExcelXslxSignatureList)
                    {
                        if (IsBinaryMatch(buf, expectedSignature))
                        {
                            isExcel = true;
                            break;
                        }
                    }

                }
            };

            BinaryFileHelper.OpenRead(sFileName, readHandler);

            if (isExcel && isNewerExcel == false)
            {
                // 5/22/2013 dd - Log how offen will still receive old xls file.

                Tools.LogInfo("[ExcelConvert] - " + sFileName + " is using old xls file.");
            }
            return isExcel;
        }

        private bool IsBinaryMatch(byte[] actual, byte[] expected)
        {
            for (int i = 0; i < actual.Length; i++)
            {
                if (actual[i] != expected[i])
                {
                    return false;
                }
            }

            return true;
        }
        // 6/20/2011 dd - These two excel prefixes must match in length.
        private byte[] m_ExcelBinaryPrefix = { 0xD0, 0xCF, 0x11, 0xE0, 0xA1, 0xB1, 0x1A, 0xE1 };
        private byte[] m_ExcelXlsxBinaryPrefix = { 0x50, 0x4B, 0x03, 0x04, 0x14, 0x00, 0x06, 0x00 };
        private byte[] m_ExcelXlsxBinaryPrefix2 = { 0x50, 0x4B, 0x03, 0x04, 0x14, 0x00, 0x08, 0x00 }; // Some Xlsx has 0x08.
        private byte[] m_ExcelXlsxBinaryPrefix3 = { 0x50, 0x4B, 0x03, 0x04, 0x14, 0x00, 0x00, 0x00 }; // 11/24/2014 dd - OPM 197229 - Handle excel generate by SpreadSheet Gear.
        private byte[] m_ExcelXlsxBinaryPrefix4 = { 0x50, 0x4B, 0x03, 0x04, 0x14, 0x00, 0x08, 0x08 }; // 6/22/2015 dd.
        private byte[] m_ExcelXslxBinaryPrefix5 = { 0x50, 0x4B, 0x03, 0x04, 0x0A, 0x00, 0x00, 0x00 }; // 8/8/2018 - dd - OPM 472911.
    
        private static object s_lockPdfConvertor = new object();

    }
}
