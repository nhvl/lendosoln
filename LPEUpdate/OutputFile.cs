using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using CommonLib;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Constants;
using LendersOfficeApp.ObjLib.AcceptableRatesheet;

namespace LPEUpdate
{
    public class OutputFile
	{
		public enum EXPORT_TYPE { Adjustments, Rates } ;

		public OutputFile(DbDataReader sdr)
		{
			m_effectiveTime = SmallDateTime.MinValue;

			m_nID = Convert.ToInt32(sdr["ID"]) ;
			m_sFileName = sdr["FileName"].ToString().ToUpper() ;
			m_bEnabled = sdr["Enabled"].ToString() == "T" ;
			m_bDirty = sdr["Dirty"].ToString() == "T" ;
			switch(sdr["ExportType"].ToString())
			{
				case "A":
					m_eExportType = EXPORT_TYPE.Adjustments ;
					break ;
				case "R":
					m_eExportType = EXPORT_TYPE.Rates ;
					break ;
				default:
					throw new ApplicationException("Invalid export type detected for OutputFileID " + m_nID.ToString()) ;
			}
			m_sWorksheet = sdr["Worksheet"].ToString() ;
		}

		public void SetEffectiveDateInfo( DateTime effectiveTime, bool hasChanged, bool processorSuccessful, string outputFilename)
		{
			m_effectiveTime = effectiveTime;

			try
			{
				try
				{
					m_accRsFile = new AcceptableRsFile(outputFilename);
				}
				catch(Exception)
				{
					return;
				}
				
				m_accRsFileVersion = new AcceptableRsFileVersion(outputFilename);

				if(processorSuccessful)
				{
					if(m_accRsFile.DeploymentType.ToLower() == "useversionaftergeneratinganoutputfileok")
						m_accRsFile.DeploymentType = "UseVersion";
					m_accRsFile.UseForBothMapAndBot = true;
					if(hasChanged)
					{
						AcceptableRsFileVersion newVersion = new AcceptableRsFileVersion();
						newVersion.RatesheetFileId = outputFilename;
						newVersion.FirstEffective = m_effectiveTime;
						newVersion.LastestEffective = m_effectiveTime;
						m_accRsFileVersion = newVersion;
					}
					else
					{
                        if (m_accRsFileVersion.IsNew)
                        {
                            m_accRsFileVersion.FirstEffective = m_effectiveTime;
                        }
						m_accRsFileVersion.LastestEffective = m_effectiveTime;
                        m_accRsFileVersion.IsExpirationIssuedByInvestor = false; // dd 5/8/2018 - OPM 468433 - Also unexpired the ratesheet version when update the latest effective time.
					}
					m_accRsFile.Save();
					m_accRsFileVersion.Save();
					m_versionNumber = m_accRsFileVersion.VersionNumber;
				}
				else
				{
					m_accRsFile.UseForBothMapAndBot = false;
					m_accRsFile.Save();
				}
			}
			catch(Exception e)
			{
				Tools.LogError("Unable to create LpeAcceptableFile representation (Required for Rate Lock Expiration feature).  Details: " + e);
			}
		}

		public int OutputFileId
		{
			get { return m_nID ; }
		}
		public string FileName
		{
			get { return m_sFileName ; }
		}
		public bool Enabled
		{
			get { return m_bEnabled ; }
		}
		public string Worksheet
		{
			get { return m_sWorksheet ; }
		}
		public EXPORT_TYPE ExportType
		{
			get { return m_eExportType ; }
		}

		public long VersionNumber
		{
			get { return m_versionNumber; }
		}
		public bool Dirty
		{
			get
			{
				return m_bDirty ;
			}
			set
			{
				if (m_bDirty == value) return ;
				m_bDirty = value ;

                var listParams = new SqlParameter[] { DataAccess.DbAccessUtils.SqlParameterForVarchar("@dirty", (this.m_bDirty) ? "T" : "F"), new SqlParameter("@id", this.m_nID) };
				string sql = "UPDATE Output_File SET Dirty=@dirty, dUpdated=GETDATE() WHERE ID=@id";

                DBUpdateUtility.Update(DataSrc.RateSheet, sql, null, listParams);
			}
		}
		public static void HandleUploadResult(int outputFileId, bool isSuccessful, string errorMessage, string tempFile)
		{
			if (isSuccessful)
			{
                var listParams = new SqlParameter[] { new SqlParameter("@id", outputFileId) };
                string sql = "UPDATE Output_File SET LastUlResult='SUCCESS', ErrorCount=0, Dirty='F', dUpdated=GETDATE() WHERE ID=@id";

                DBUpdateUtility.Update(DataSrc.RateSheet, sql, null, listParams);
                SendNotificationEmail(outputFileId) ;
			}
			else
			{
                string shortFileName = GetShortFileName(outputFileId);
				errorMessage = string.Format("{0}::{1}::{2}", shortFileName, SafeConvert.ToString(tempFile), errorMessage) ;
                Tools.LogBug(errorMessage);

				int errorCount = GetErrorCount(outputFileId) + 1 ;

                LpeAdminMessaging.SendEmail(E_LpeErrorAreaT.RateUpload, shortFileName, errorMessage);

				// 1/4/08 db - OPM 19545 - move this truncation to after the message gets logged and e-mailed so the SAE's can see the entire message
				errorMessage = SafeString.Left(errorMessage, 250) ; // truncate message length to fit in field

                var listParams = new SqlParameter[] { DataAccess.DbAccessUtils.SqlParameterForVarchar("@last", errorMessage),
                                                    new SqlParameter("@count", errorCount),
                                                    new SqlParameter("@id", outputFileId) };

                string sql = "UPDATE Output_File SET LastUlResult=@last, ErrorCount=@count WHERE ID=@id";

                DBUpdateUtility.Update(DataSrc.RateSheet, sql, null, listParams);
			}
		}
		private static void SendNotificationEmail(int outputFileId)
		{
            var listParams = new SqlParameter[] { new SqlParameter("@id", outputFileId) };
            string sql = "SELECT FileName, UpdateMonitorEmails FROM Output_File WHERE ID=@id";


            Action<IDataReader> readHandler = delegate(IDataReader sdr)
            {
                if (!sdr.Read()) throw new ApplicationException(string.Format("Strange, why is the OutputFiles for ID# {0} missing?", outputFileId));

                string shortFileName = (string)sdr["FileName"];
                string recipient = SafeConvert.ToString(sdr["UpdateMonitorEmails"]);
                if (recipient.Length > 0)
                {
                    try
                    {
                        var cbe = new LendersOffice.Email.CBaseEmail(ConstAppDavid.SystemBrokerGuid)
                        {
                            DisclaimerType = E_DisclaimerType.NORMAL,
                            From = ConstSite.PmlRatesEmailAddress,
                            To = recipient,
                            Subject = "LPE_MANAGER MESSAGE",
                            Message = string.Format("{0} has been successfully uploaded to the engine.", shortFileName),
                            IsHtmlEmail = false,
                            BccToSupport = false
                        };
                        EmailUtilities.SendEmail(cbe);
                    }
                    catch (Exception ex)
                    {
                        Tools.LogError("CANNOT SEND EMAIL TO MONITORS", ex);
                    }
                }
            };

            DBSelectUtility.ProcessDBData(DataSrc.RateSheet, sql, null, listParams, readHandler);
		}
		private static string GetShortFileName(int outputFileId)
		{
            var listParams = new SqlParameter[] { new SqlParameter("@id", outputFileId) };
            string sql = "SELECT FileName FROM Output_File WHERE ID=@id";

            string fileName = null;
            Action<IDataReader> readHandler = delegate(IDataReader sdr)
            {
                if (!sdr.Read()) throw new ApplicationException(string.Format("Strange, why is the OutputFiles for ID# {0} missing?", outputFileId));

                fileName = (string)sdr["FileName"];
            };

            DBSelectUtility.ProcessDBData(DataSrc.RateSheet, sql, null, listParams, readHandler);
            return fileName;
		}
		private static int GetErrorCount(int outputFileId)
		{
            var listParams = new SqlParameter[] { new SqlParameter("@id", outputFileId) };
            string sql = "SELECT ErrorCount FROM Output_File WHERE ID=@id";

            int errCount = 0;
            Action<IDataReader> readHandler = delegate(IDataReader sdr)
            {
                if (!sdr.Read()) throw new ApplicationException(string.Format("Strange, why is the OutputFiles for ID# {0} missing?", outputFileId));

                errCount = SafeConvert.ToInt(sdr["ErrorCount"]);
            };

            DBSelectUtility.ProcessDBData(DataSrc.RateSheet, sql, null, listParams, readHandler);
            return errCount;
		}

		private int m_nID ;
		private string m_sFileName ;
		private bool m_bEnabled ;
		private bool m_bDirty ;
		private EXPORT_TYPE m_eExportType ;
		private string m_sWorksheet ;

		private DateTime m_effectiveTime = SmallDateTime.MinValue;

		private long m_versionNumber = -1;
		private AcceptableRsFile m_accRsFile = null;
		private AcceptableRsFileVersion m_accRsFileVersion = null;
	}
}
