﻿namespace LPEUpdate
{
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Text;
    using DataAccess;
    using ExcelLibraryClient;
    using LendersOffice;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Drivers.Gateways;
    using LendersOfficeApp.ObjLib.AcceptableRatesheet;
    using LendersOfficeApp.ObjLib.RulesManagement;
    using LPEUpdate.CustomRatesheetProcessing;
    using LPEUpdate.InputSchemas;
    using LpeUpdateBotLib.Common;
    using LqbGrammar.Exceptions;
    using RatesheetMap;

    class ProcessInputFileProcessor
    {
        private List<LpeAcceptableFile> m_RateOptionFilesToUpload = new List<LpeAcceptableFile>();
        private List<LpeAcceptableFile> m_AdjustTableFilesToUpload = new List<LpeAcceptableFile>();
        private List<ICustomRatesheet> m_customRatesheetListToUpload = new List<ICustomRatesheet>();
        private StringBuilder LogMessageBuffer = new StringBuilder();

        public string GetLog()
        {
            return this.LogMessageBuffer.ToString();
        }

        public List<LpeAcceptableFile> GetRateOptionFilesToUpload()
        {
            return m_RateOptionFilesToUpload;
        }

        public List<ICustomRatesheet> GetCustomRatesheetListToUpload()
        {
            return m_customRatesheetListToUpload;
        }

        /// <summary>
        /// Process all the files in the DownloadPath using the RatesheetMapProcessor (and the Schema validator for Adjustment tables, as of 4/27/09)
        /// </summary>
        public void Execute(HashSet<string> processedRateSheetSet, OutputFileList outputfiles)
        {
            TimerHelper timer = new TimerHelper();
            timer.start();
            Log("ProcessInputFiles started.");
            
            // 3/17/2011 dd - OPM 64154. Because LpeDownload and LpeUpload are two independent processes, therefore
            // the LpeDownload process could either add or update ratesheet in download path while LpeUpload is processing.
            // To ensure the consistency I need to verify that this method use the same file.
            List<KeyValuePair<string, DateTime>> excelFileInDownloadPathList = new List<KeyValuePair<string, DateTime>>();


            foreach (string sFilePath in this.GetEligibleFiles(PathCfg.DOWNLOAD_PATH))
            {
                string fileName = Path.GetFileName(sFilePath);
                // 7/24/2018 - dd - To avoid process the same ratesheet in the same run, I will skip file that already process in this LpeUpdate run.
                if (processedRateSheetSet.Contains(fileName))
                {
                    continue;
                }
                processedRateSheetSet.Add(fileName);
                excelFileInDownloadPathList.Add(new KeyValuePair<string, DateTime>(sFilePath, FileOperationHelper.GetLastWriteTime(sFilePath)));
            }

            var orderedList = excelFileInDownloadPathList.OrderBy(o => o.Value).Take(ConstStage.MaxNumberOfRatesheetsToProcess);

            // OPM 22789 - we will record the modified datetime of each of the files in the download path,
            // which indicates that this is a new version of the ratesheet (since if it was the same version
            // as before, we would have rejected it in the bot and not put it into the download path).
            foreach (var o in orderedList)
            {

                string sFilePath = o.Key;
                try
                {
                    string filename = Path.GetFileName(sFilePath);
                    DateTime lastWrite = FileOperationHelper.GetLastWriteTime(sFilePath);
                    if (o.Value != lastWrite)
                    {
                        Log("    " + filename + " was update by LpeDownload. Skip file this iteration");
                        continue;
                    }
                    // Need to use the file's modify date because when an existing file is replaced with a new one,
                    // it keeps the created date and only updates the modified date.

                    SqlParameter[] parameters = new SqlParameter[] {
                        new SqlParameter("@InvestorXlsFileName", filename),
                        new SqlParameter("@FileVersionDateTime", lastWrite)
                    };
                    Log("    RS_UpdateFileVersionDateTime " + filename + " " + lastWrite);
                    //"<RS_EXPIRATION> Status: REMOVING CLIENT MANUAL EXPIRATION (if exists) 
                    // Reason: Ratesheet is new (or at least assumed so since it's in the download path) and 
                    //         therefore we are updating its File Version Date, which will effectively remove 
                    //         any manual expiration that was set to Expire until next successful download.

                    StoredProcedureHelper.ExecuteNonQuery(DataSrc.RateSheet, "RS_UpdateFileVersionDateTime", 0, parameters);
                }
                catch (Exception e)
                {
                    Tools.LogErrorWithCriticalTracking("Unable to set FileVersionDateTime for ratesheet: " + sFilePath, e);
                }
            }

            DependTree tree = new DependTree();

            CurrentRateLockExpActivatedRatesheets activatedRatesheets = new CurrentRateLockExpActivatedRatesheets();
            SchemaManager schemaManager = new SchemaManager();
            bool bProcessMarketIndexDepFiles = false;
            MarketIndexDependentFiles miDepFiles = new MarketIndexDependentFiles();

            //Process the market index file first, so that all subsequent files that depend on it will have the updated version.
            string fullMIDownloadPath = Path.Combine(PathCfg.DOWNLOAD_PATH, Config.MarketDataFileName);
            if (FileOperationHelper.Exists(fullMIDownloadPath))
            {
                Log("Process Market Index File: " + fullMIDownloadPath);
                bProcessMarketIndexDepFiles = true;
                bProcessMarketIndexDepFiles = ProcessMarketIndexFile(fullMIDownloadPath, outputfiles, schemaManager, tree);
                // If the processing of the market index file is successful, copy it to the market index folder and
                // templates folders, and remove it from the download path.
                if (bProcessMarketIndexDepFiles)
                {
                    string sDestPath = Config.FILES_PATH + Path.GetFileName(fullMIDownloadPath);
                    Tools.FileCopy(fullMIDownloadPath, Path.Combine(PathCfg.MARKET_INDEX_PATH, Config.MarketDataFileName));
                    Tools.FileCopy(fullMIDownloadPath, sDestPath);
                    Tools.FileDelete(fullMIDownloadPath);
                    SaveMarketIndexDataToDB(); // 10/12/09 db - adding for Google integration - not required yet
                }
            }

            Stopwatch globalSchemaValidationStopwatch = new Stopwatch();
            Stopwatch globalRatesheetMapProcessorStopwatch = new Stopwatch();

            ICustomRatesheet pml0176CustomRatesheet = new Pml0176PacUnionCustomRateSheet(string.Empty);

            foreach (var o in orderedList)
            {
                string sFilePath = o.Key;

                TimerHelper timer1 = new TimerHelper();
                timer1.start();

                string nameAsIs = Path.GetFileName(sFilePath);
                string sFileName = nameAsIs.ToUpper();
                if (FileOperationHelper.Exists(sFilePath) == false)
                {
                    Log("    " + sFilePath + " is no longer available. Skip.");
                    continue;
                }
                if (o.Value != FileOperationHelper.GetLastWriteTime(sFilePath))
                {
                    Log("    " + nameAsIs + " was update by LpeDownload. Skip file this iteration");
                    continue;
                }

                // dd 10/22/2018 - Handle detection of Custom Ratesheet.
                if (pml0176CustomRatesheet.RatesheetName.Equals(nameAsIs, StringComparison.OrdinalIgnoreCase))
                {
                    try
                    {
                        this.m_customRatesheetListToUpload.Add(new Pml0176PacUnionCustomRateSheet(sFilePath));
                    }
                    catch(Exception exc)
                    {
                        try
                        {
                            Tools.FileCopy(sFilePath, Path.Combine(PathCfg.DOWNLOAD_PATH_VALIDATION_FAILED, sFileName));
                            Tools.FileDelete(sFilePath);
                        }
                        catch (SystemException)
                        {
                        }

                        Log($"    Move '{sFileName}' to DOWNLOAD_PATH_VALIDATION_FAILED");
                        LpeAdminMessaging.SendEmail(E_LpeErrorAreaT.LpeUpdateGeneral, "LpeUpdate::Pml0176PacUnionCustomRateSheet " + DateTime.Now + " FAILED... INVESTIGATE IMMEDIATELY", exc.GetType().FullName + "::" + exc.Message + Environment.NewLine + exc.StackTrace);
                        Tools.LogError("LpeUpdate::Pml0176PacUnionCustomRateSheet failed", exc);
                    }

                    continue;
                }

                string xslxVersion = IsNewerExcelFile(sFilePath) ? " (xlsx)" : "";

                bool mapExists = false;
                bool mapSuccess = false;
                bool schemaExists = false;
                bool bValidated = false;
                LpeAcceptableFile lpeAccFile = null;
                long rsMapDuration = 0;
                long rsSchemaValidationDuration = 0;
                StringBuilder subDebugString = new StringBuilder();
                try
                {
                    Stopwatch sw = null;


                    // 11/06/07 db - adding for testing the Ratesheet Map Processor, OPM 17150
                    if (Config.IsRunRSMapProcessorInTandem)
                    {
                        sw = Stopwatch.StartNew();
                        using (PerformanceStopwatch.Start("RatesheetMapProcessor" + xslxVersion))
                        {
                            globalRatesheetMapProcessorStopwatch.Start();

                            lpeAccFile = RunRatesheetMapProcessor(sFilePath, ref mapExists, ref mapSuccess, miDepFiles, subDebugString);
                            globalRatesheetMapProcessorStopwatch.Stop();
                        }
                        sw.Stop();
                        rsMapDuration = sw.ElapsedMilliseconds;
                    }
                    sw = Stopwatch.StartNew();
                    using (PerformanceStopwatch.Start("SchemaValidation" + xslxVersion))
                    {
                        globalSchemaValidationStopwatch.Start();
                        RunSchemaValidation(sFilePath, ref schemaExists, ref bValidated, mapExists, activatedRatesheets, schemaManager, tree, outputfiles, subDebugString);
                        globalSchemaValidationStopwatch.Stop();
                    }
                    sw.Stop();

                    rsSchemaValidationDuration = sw.ElapsedMilliseconds;

                    string sDestPath = Config.FILES_PATH + sFileName;
                    E_RatesheetFileMoveAction moveAction = GetRatesheetFileMoveAction(mapExists, mapSuccess, schemaExists, bValidated);

                    if (lpeAccFile != null)
                    {
                        lpeAccFile.RatesheetFileMoveAction = moveAction;
                    }

                    switch (moveAction)
                    {
                        case E_RatesheetFileMoveAction.CopyToTemplatesFolder:
                            Tools.FileCopy(sFilePath, sDestPath);
                            break;
                        case E_RatesheetFileMoveAction.MoveToTemplatesFolder:
                            Tools.FileCopy(sFilePath, sDestPath);
                            // 2/15/2012 dd - eOPM 314517 Send confirmation email to client
                            // after process ratesheet successfully.
                            // Long term we want to have a nicer framework that handle list
                            // of ratesheet and email template.
                            // For now I will just hardcode the name.

                            if (sFileName.Equals("PML0179_MARGINS.XLS", StringComparison.OrdinalIgnoreCase))
                            {
                                UploadProcess.NotifyICity();
                            }
                            else if (sFileName.Equals("PML0171_MARGIN_ADJ.xls", StringComparison.OrdinalIgnoreCase) ||
                                sFileName.Equals("PML0171_NR-MARGINS.XLS", StringComparison.OrdinalIgnoreCase))
                            {
                                // 6/20/2012 dd - OPM 86592
                                UploadProcess.NotifyGreenway(sFileName);
                            }
                            else if (sFileName.Equals("PML0197_CUSTOM_FLAT.XLS", StringComparison.OrdinalIgnoreCase))
                            {
                                UploadProcess.NotifyIapprov();
                            }
                            else if (sFileName.Equals("PML0176_CUSTOM_CORR_546.XLS", StringComparison.OrdinalIgnoreCase) ||
                                sFileName.Equals("PML0176_CUSTOM_RETAIL_AUSTIN.XLS", StringComparison.OrdinalIgnoreCase) ||
                                sFileName.Equals("PML0176_CUSTOM_RETAIL_AUSTIN_JUMBO_C.XLS", StringComparison.OrdinalIgnoreCase))
                            {
                                UploadProcess.NotifyPacUnion(sFileName); // OPM 237273, 2/8/2016, ML
                            }

                            // OPM 20084 - don't move from download path until import completely successful unless there's only a schema
                            // Add adjust tables into consideration here once they're handled with the ratesheet map
                            if ((!mapExists || (m_RateOptionFilesToUpload.Count == 0)) && schemaExists)
                                Tools.FileDelete(sFilePath);
                            else if ((lpeAccFile == null) && mapExists && mapSuccess) // OPM 25797 - if a map processes successfully but doesn't create output files, move ratesheet into Templates folder
                                Tools.FileDelete(sFilePath);
                            break;
                        case E_RatesheetFileMoveAction.MoveToDownloadPathValidationFailed:
                            Tools.FileCopy(sFilePath, Path.Combine(PathCfg.DOWNLOAD_PATH_VALIDATION_FAILED, sFileName));
                            Tools.FileDelete(sFilePath);
                            subDebugString.AppendLine($"    Move '{sFileName}' to DOWNLOAD_PATH_VALIDATION_FAILED");
                            if (sFileName.Equals("PML0213_CUSTOM.XLS", StringComparison.OrdinalIgnoreCase))
                            {
                                UploadProcess.NotifyCMGError(mapSuccess, bValidated);
                            }
                            break;
                        case E_RatesheetFileMoveAction.LeaveInDownloadPath:
                            if (sFileName.Equals("PML0213_CUSTOM.XLS", StringComparison.OrdinalIgnoreCase))
                            {
                                UploadProcess.NotifyCMGError(mapSuccess, bValidated);
                            }
                            break;
                        default: //do nothing, leave in download path
                            break;
                    }
                }
                catch (Exception ex)
                {
                    bValidated = false;
                    LpeAdminMessaging.SendEmail(E_LpeErrorAreaT.Schema, "Error trying to process " + sFilePath, ex.ToString());
                }
                timer1.stop();

                string schemaSuccessStr = (schemaExists) ? bValidated.ToString() : "N/A";
                string mapSuccessStr = (mapExists) ? mapSuccess.ToString() : "N/A";
                Log(string.Format("   {0}{8}. Schema Validate={1} in {4}ms. Map Validate={3} in {5}ms.  In {2}{6}{7}", 
                    sFileName, //0
                    schemaSuccessStr, //1
                    timer1.DurationString, //2
                    mapSuccessStr, //3
                    rsSchemaValidationDuration, //4
                    rsMapDuration, //5
                    Environment.NewLine, // 6
                    subDebugString.ToString(), // 7
                    xslxVersion // 8
                    ));
            }

            if (bProcessMarketIndexDepFiles)
            {
                Stopwatch sw = Stopwatch.StartNew();
                ProcessMarketIndexDepFiles(miDepFiles);
                sw.Stop();
                Log("    ProcessMarketIndexDepFiles in " + sw.ElapsedMilliseconds + "ms.");
            }

            // 11/3/2014 dd - OPM 195092 - The market index dependent file currently ALWAYS include all ratesheet to this list.
            // I need to update the code so only ratehsheet access global index data need to be include.
            //miDepFiles.Save(); // OPM 19146
            activatedRatesheets.Save(); // OPM 19397

            timer.stop();

            Log("ProcessInputFiles finished in " + timer.DurationString);
            Log("    # of Ratesheets in download path: " + excelFileInDownloadPathList.Count);
            Log("    # of Ratesheets process: " + orderedList.Count());
            Log("    RateSheetMapProcessor executed in " + globalRatesheetMapProcessorStopwatch.ElapsedMilliseconds + "ms.");
            Log("    Schema Validation executed in " + globalSchemaValidationStopwatch.ElapsedMilliseconds + "ms.");
        }
        private bool ProcessMarketIndexFile(string marketIndexFilePath, OutputFileList outputfiles, SchemaManager schemaManager, DependTree tree)
        {
            bool bValidated = false;

            try
            {
                string sFileName = Path.GetFileName(marketIndexFilePath).ToUpper();
                SchemaFile schema = schemaManager.GetSchemaFile(sFileName);
                bool schemaExists = (schema != null);
                if (schemaExists)
                {
                    bValidated = schema.Validate(marketIndexFilePath);
                    if (!bValidated)
                    {
                        LpeAdminMessaging.SendEmail(E_LpeErrorAreaT.Schema, "Schema File Mismatch [" + FileOperationHelper.GetLastWriteTime(marketIndexFilePath).ToString() + "] " + sFileName, schema.ValidateFailureReason);
                    }

                }
                else
                {
                    bValidated = false;
                    LpeAdminMessaging.SendEmail(E_LpeErrorAreaT.Schema, "SchemaManager return null for " + sFileName, "SchemaManager return null for " + sFileName);
                }

                if (bValidated)
                {
                    string[] rgDescendants = tree.GetDescendants(sFileName);
                    if (rgDescendants == null)
                    {
                        LpeAdminMessaging.SendEmail(E_LpeErrorAreaT.Schema, "Descendant for " + sFileName + " not found", "Descendant for " + sFileName + " not found");
                        bValidated = false;
                    }
                    else
                    {
                        foreach (string descendant in rgDescendants)
                        {
                            outputfiles.SetDirtyFlag(descendant);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                bValidated = false;
                LpeAdminMessaging.SendEmail(E_LpeErrorAreaT.MapProcessor, "Error trying to process " + marketIndexFilePath, ex.ToString());
            }

            return bValidated;
        }

        // OPM 108065. If we have processed MarketIndex file, we want to 
        // save updated index values to the PML MASTER account.
        private void SaveMarketIndexDataToDB()
        {
            try
            {
                string marketIndexFilePath = Path.Combine(PathCfg.MARKET_INDEX_PATH, "MarketIndex.xls");

                if (!FileOperationHelper.Exists(marketIndexFilePath))
                {
                    throw new Exception(string.Format("MarketIndex file not found (looking for '{0}')", marketIndexFilePath));
                }

                List<string> dependentFileList = new List<string>();
                foreach (string sFilePath in RatesheetMapCommon.GetExcelFiles(PathCfg.MARKET_INDEX_PATH))
                {
                    if (!FileOperationHelper.Exists(sFilePath))
                        continue;
                    string sFileName = Path.GetFileName(sFilePath);
                    if (!(sFileName.ToLower().Equals("marketindex.xls")))
                    {
                        dependentFileList.Add(sFilePath);
                    }
                }

                foreach (var o in
                ExcelLibrary.GetSystemVariablesForRatesheetProcessor(
                    marketIndexFilePath
                    , dependentFileList.ToArray()
                    , 200))
                {
                    SqlParameter[] parameters = {
                                                    new SqlParameter("@IndexName", o.Key.ToUpper()),
                                                    new SqlParameter("@IndexCurrentValue", o.Value),
                                                    new SqlParameter("@MasterPmlBrokerId", LendersOffice.Admin.BrokerDB.PmlMaster)
                                                };
                    StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOShare, "UpdateSystemArmIndex", 2, parameters);
                }
                SystemArmIndex.PopulateSystemArmIndexToLoanProgramTemplate();
            }
            catch (Exception e)
            {
                Tools.LogError("<LPEUpdate> Unable to save market index data to database.", e);
            }
        }


        private void RunSchemaValidation(string sFilePath, ref bool schemaExists, ref bool bValidated, 
            bool mapExists, CurrentRateLockExpActivatedRatesheets activatedRatesheets, 
            SchemaManager schemaManager, DependTree tree, OutputFileList outputfiles,
            StringBuilder debugString)
        {
            string sFileName = Path.GetFileName(sFilePath).ToUpper();

            Stopwatch sw = Stopwatch.StartNew();
            SchemaFile schema = schemaManager.GetSchemaFile(sFileName);

            schemaExists = (schema != null);
            if (schemaExists)
            {
                sw.Reset();
                sw.Start();
                bValidated = schema.Validate(sFilePath);
                sw.Stop();
                debugString.AppendLine(" Validate=" + sw.ElapsedMilliseconds + "ms. ");
                
                if (!bValidated)
                {
                    activatedRatesheets.MarkStatus(sFileName, E_RateLockExpActivatedRSStatus.IsNotWorking);
                    string msg = string.Format("    [RATESHEET_EXPIRATION]{0}Ratesheet: {1}{0}Status: EXPIRED{0}Reason: Ratesheet '{1}' status is being marked as 'IsNotWorking' because schema validation returned false.{0}Location: MainClass.RunSchemaValidation function, !bValidated block.", Environment.NewLine + "    ", sFileName);
                    debugString.AppendLine(msg);
                    LpeAdminMessaging.SendEmail(E_LpeErrorAreaT.Schema, "Schema File Mismatch - [" + FileOperationHelper.GetLastWriteTime(sFilePath).ToString() + "] " + sFileName, string.Format("SCHEMA: Schema File Mismatch\n{0}\n{1}", sFileName, schema.ValidateFailureReason));
                }

            }
            else
            {
                bValidated = false;
                if (!mapExists)
                {
                    activatedRatesheets.MarkStatus(sFileName, E_RateLockExpActivatedRSStatus.IsNotWorking);
                    string msg = string.Format("    [RATESHEET_EXPIRATION]{0}Ratesheet: {1}{0}Status: EXPIRED{0}Reason: Ratesheet '{1}' status is being marked as 'IsNotWorking' because no schema or map exists to process this file.", Environment.NewLine + "    ", sFileName);
                    debugString.AppendLine(msg);
                    LpeAdminMessaging.SendEmail(E_LpeErrorAreaT.Schema, "SchemaManager return null for " + sFileName, string.Format("SCHEMA: SchemaManager return null for {0}\n", sFileName));
                }
            }
            if (bValidated)
            { // process input files that have been validated
                string[] rgDescendants = tree.GetDescendants(sFileName);
                if (rgDescendants == null)
                {
                    activatedRatesheets.MarkStatus(sFileName, E_RateLockExpActivatedRSStatus.IsNotWorking);
                    string msg = string.Format("    [RATESHEET_EXPIRATION]{0}Ratesheet: {1}{0}Status: EXPIRED{0}Reason: Ratesheet '{1}' status is being marked as 'IsNotWorking' because no descendants were found for this file during schema validation.", Environment.NewLine + "    ", sFileName);
                    debugString.AppendLine(msg);
                    LpeAdminMessaging.SendEmail(E_LpeErrorAreaT.Schema, string.Format("SCHEMA: Descendant for {0} not found", sFileName), string.Format("SCHEMA: Descendant for {0} not found", sFileName));
                    bValidated = false;
                }
                else
                {
                    foreach (string descendant in rgDescendants)
                    {
                        outputfiles.SetDirtyFlag(descendant);
                    }
                }
            }
        }

        private void ProcessMarketIndexDepFiles(MarketIndexDependentFiles miDepFiles)
        {
            try
            {
                bool mapExists = false;
                bool mapSuccess = false;
                LpeAcceptableFile lpeAccFile = null;
                StringBuilder debugString = new StringBuilder();
                foreach (string ratesheetName in miDepFiles.ListMarketIndexFilesNeedReprocessing())
                {
                    string sFilePath = Config.FILES_PATH + ratesheetName;
                    lpeAccFile = RunRatesheetMapProcessor(sFilePath, ref mapExists, ref mapSuccess, miDepFiles, debugString);

                    // If the processing failed, move the ratesheet from the templates folder into the download path so
                    // it will get processed again on the next run
                    if (mapExists && !mapSuccess)
                    {
                        string sDestPath = PathCfg.DOWNLOAD_PATH + ratesheetName;
                        Tools.FileCopy(sFilePath, sDestPath);
                    }

                    if (lpeAccFile != null)
                        lpeAccFile.RatesheetFileMoveAction = E_RatesheetFileMoveAction.LeaveInDownloadPath;
                }
            }
            catch (Exception e)
            {
                Tools.LogError("Unable to process market index dependent file ratesheets (these ratesheets would not normally need to be processed right now, but there is a new market index file that they depend on).", e);
            }
        }

        private LpeAcceptableFile RunRatesheetMapProcessor(string sFilePath, ref bool mapExists, ref bool mapSuccess, MarketIndexDependentFiles miDepFiles, StringBuilder debugString)
        {
            DateTime effectiveDate = DateTime.Now; // OPM 22407
            LpeAcceptableFile lpeAccFile = null;
            Stopwatch sw = Stopwatch.StartNew();
            try
            {
                RatesheetMapProcessor processor = new RatesheetMapProcessor(false /* run standalone mode*/);
                string finalInfo = processor.Process(sFilePath);

                sw.Stop();
                debugString.AppendLine("    RatesheetMapProcessor.Process in " + sw.ElapsedMilliseconds + "ms.");
                debugString.AppendLine(processor.ProcessDebugStringBuilder.ToString());
                mapExists = processor.MapExists;

                string sFileName = Path.GetFileName(sFilePath).ToUpper();

                if (mapExists)
                {
                    if (processor.DateInfoSet)
                        effectiveDate = processor.EffectiveDateTimePT;
                    else
                    {
                        try
                        {
                            effectiveDate = ParseEffectiveDate(sFilePath);
                        }
                        catch (CBaseException c)
                        {
                            Tools.LogWarning("Unable to parse effective date while processing a map using database coordinates for ratesheet: " + sFilePath + " (Defaulting to 'now') DETAILS: " + c);
                        }
                    }
                }

                if (processor.AccessedMarkedIndexFiles)
                {
                    miDepFiles.AddEntry(sFileName, E_MarketIndexFileDependentStatus.DoesNotNeedReprocessing);
                }
                else
                {
                    miDepFiles.RemoveEntry(sFileName);
                }

                mapSuccess = processor.IsCompletelySuccessful;
                sw.Reset();
                sw.Start();
                foreach (string rofilename in processor.RateOptionOutputFilenames)
                {
                    try
                    {
                        bool isBothRsMapAndBotWorking = !processor.DoesOutputFileHaveError(rofilename); // OPM 26334
                        bool hasChanged = CacheAndCompareWithCachedCopy(rofilename);
                        lpeAccFile = new LpeAcceptableFile(rofilename, sFilePath, effectiveDate, isBothRsMapAndBotWorking, hasChanged);
                        lpeAccFile.InvestorRatesheetDownloadTime = FileOperationHelper.GetLastWriteTime(sFilePath); // 12/30/2010 dd - Use last write time as the time ratesheet download.

                        m_RateOptionFilesToUpload.Add(lpeAccFile);
                    }
                    catch (MissingAcceptableRsFileException exc)
                    {
                        Tools.LogError(exc);


                        Log("Error trying to process rates in " + sFilePath + ". " + exc.DeveloperMessage);
                    }
                    catch (Exception e)
                    {
                        Tools.LogErrorWithCriticalTracking(string.Format("MAP PROCESSOR: Error trying to process rates in {0}.\n\n{1}", sFilePath, e.ToString()));
                    }
                }
                sw.Stop();

                sw.Reset();
                sw.Start();
                foreach (string adjfilename in processor.AdjustmentTableOutputFilenames)
                {
                    try
                    {
                        bool isBothRsMapAndBotWorking = !processor.DoesOutputFileHaveError(adjfilename); // OPM 26334
                        bool hasChanged = CacheAndCompareWithCachedCopy(adjfilename);
                        m_AdjustTableFilesToUpload.Add(new LpeAcceptableFile(adjfilename, sFilePath, effectiveDate, isBothRsMapAndBotWorking, hasChanged));
                    }
                    catch (Exception e)
                    {
                        Tools.LogErrorWithCriticalTracking(string.Format("MAP PROCESSOR: Error trying to process adjustments in {0}.\n\n{1}", sFilePath, e.ToString()));
                    }
                }
                sw.Stop();
                if (Config.IsSendRatesheetMapOPMNotif && processor.MapExists && !mapSuccess)
                {
                    if (!string.IsNullOrEmpty(finalInfo))
                    {
                        LpeAdminMessaging.SendEmail(E_LpeErrorAreaT.MapProcessor, "Effective [" + effectiveDate + "] Error process " + Path.GetFileName(sFilePath), finalInfo);
                    }
                }
            }
            catch (Exception ex)
            {
                Tools.LogErrorWithCriticalTracking(string.Format("MAP PROCESSOR: Error trying to process {0}.\n\n{1}", sFilePath, ex.ToString()));
            }
            return lpeAccFile;
        }
        private DateTime ParseEffectiveDate(string ratesheetFilePath)
        {
            DateTime effectiveDate = DateTime.Now; // OPM 22407
            string ratesheetFilename = Path.GetFileName(ratesheetFilePath);

            InvestorXlsFile investorXlsFile = InvestorXlsFile.RetrieveByXlsFileName(ratesheetFilename);

            effectiveDate = ParseRatesheetForEffectiveDate(ratesheetFilePath, investorXlsFile.EffectiveDateTimeWorksheetName,
                investorXlsFile.EffectiveDateTimeLandMarkText, investorXlsFile.EffectiveDateTimeRowOffset, investorXlsFile.EffectiveDateTimeColumnOffset);


            return effectiveDate;
        }

        private DateTime ParseRatesheetForEffectiveDate(string ratesheetFilePath, string worksheetName, string landmarkText, int rowOffset, int colOffset)
        {
            DateTime effectiveDate = DateTime.Now; // OPM 22407

            string date = string.Empty;

            bool isFound = false;

            using (PerformanceStopwatch.Start("ExcelLibrary.GetCellValue"))
            {
                isFound = ExcelLibrary.GetCellValue(ratesheetFilePath, worksheetName, landmarkText, rowOffset, colOffset, out date);
            }

            if (isFound == false)
            {
                throw new CBaseException(String.Format("Could not find cell with value '{0}' in worksheet '{1}' in ratesheet '{2}' - No effective date will be parsed.", landmarkText.Trim(), worksheetName, ratesheetFilePath), "");

            }

            try
            {
                effectiveDate = DateTime.Parse(date);
            }
            catch (Exception e)
            {
                throw new CBaseException(String.Format("Invalid DateTime value of '{2}' in worksheet '{0}' in ratesheet '{1}' - No effective date will be parsed.", worksheetName, ratesheetFilePath, date), e);
            }

            return effectiveDate;
        }
        private bool CacheAndCompareWithCachedCopy(string outputFilePath)
        {
            string outputFilename = Path.GetFileName(outputFilePath);
            string cachedFilePath = Path.Combine(PathCfg.OUTPUT_FILE_CACHE_PATH, outputFilename);

            bool hasChanged = !Tools.IsFileIdentical(outputFilePath, cachedFilePath);

            if (hasChanged)
            {
                Tools.FileCopy(outputFilePath, cachedFilePath);
            }

            return hasChanged;
        }

        private E_RatesheetFileMoveAction GetRatesheetFileMoveAction(bool mapExists, bool mapSuccess, bool schemaExists, bool schemaSuccess)
        {
            if (!mapExists && schemaSuccess)
            {
                return E_RatesheetFileMoveAction.MoveToTemplatesFolder;
            }
            else if (mapSuccess && schemaSuccess)
            {
                return E_RatesheetFileMoveAction.MoveToTemplatesFolder;
            }
            else if (mapSuccess && !schemaExists)
            {
                return E_RatesheetFileMoveAction.MoveToTemplatesFolder;
            }
            else if (mapExists && !mapSuccess && schemaSuccess)
            {
                return E_RatesheetFileMoveAction.CopyToTemplatesFolder;
            }
            else if (mapExists && !mapSuccess)
            {
                return E_RatesheetFileMoveAction.MoveToDownloadPathValidationFailed;
            }
            else if (schemaExists && !schemaSuccess)
            {
                return E_RatesheetFileMoveAction.MoveToDownloadPathValidationFailed;
            }
            else
            {
                return E_RatesheetFileMoveAction.LeaveInDownloadPath;
            }
        }

        private List<string> GetEligibleFiles(string path)
        {
            List<string> searchPatterns = new List<string>() { "*.xls", "*.cpx", "*.xlsx", "*.zip" };
            var result = searchPatterns.SelectMany(x => Directory.GetFiles(path, x));

            return result.ToList();
        }

        /// <summary>
        /// Put message to LogMessageBuffer. This method IS NOT thread safe.
        /// </summary>
        /// <param name="message">Message to log.</param>
        private void Log(string message)
        {
            this.LogMessageBuffer.AppendLine($"[{DateTime.Now.ToLongTimeString()}] - {message}");
        }

        /// <summary>
        /// Peak at the file header to determine if it is Excel 2007 or later
        /// </summary>
        private bool IsNewerExcelFile(string sFileName)
        {

            if (FileOperationHelper.Exists(sFileName) == false)
            {
                return false;
            }
            bool isNewerExcel = true;

            try
            {
                byte[] buf = new byte[m_ExcelXlsxBinaryPrefix.Length];

                Action<BinaryFileHelper.LqbBinaryStream> readHandler = delegate (BinaryFileHelper.LqbBinaryStream fs)
                {
                    fs.Stream.Read(buf, 0, buf.Length);

                    // 6/20/2011 dd - Check old excel version.
                    for (int i = 0; i < buf.Length; i++)
                    {
                        if (m_ExcelXlsxBinaryPrefix[i] != buf[i])
                        {
                            isNewerExcel = false;
                            break;
                        }
                    }
                    if (isNewerExcel == false)
                    {
                        isNewerExcel = true; // Assume is newer excel.
                        // 6/20/2011 dd - Check for new version.
                        for (int i = 0; i < buf.Length; i++)
                        {
                            if (m_ExcelXlsxBinaryPrefix2[i] != buf[i])
                            {
                                isNewerExcel = false;
                                break;
                            }
                        }

                    }
                };

                BinaryFileHelper.OpenRead(sFileName, readHandler);
            }
            catch (IOException exc)
            {
                isNewerExcel = false; // When IO exception occur just assume old excel.
                Log("    IsNewerExcelFile has IO Exception for [" + sFileName + "]. Error=" + exc.ToString());
            }
            catch (DeveloperException exc)
            {
                isNewerExcel = false;
                Tools.LogError("   IsNewerExcelFile has exception for [" + sFileName + "].", exc);
            }

            return isNewerExcel;
        }
        private byte[] m_ExcelXlsxBinaryPrefix = { 0x50, 0x4B, 0x03, 0x04, 0x14, 0x00, 0x06, 0x00 };
        private byte[] m_ExcelXlsxBinaryPrefix2 = { 0x50, 0x4B, 0x03, 0x04, 0x14, 0x00, 0x08, 0x00 }; // Some Xlsx has 0x08.
    }
}