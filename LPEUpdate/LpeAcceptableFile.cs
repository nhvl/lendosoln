using System;
using DataAccess;
using LendersOffice.Common;
using LendersOfficeApp.ObjLib.AcceptableRatesheet;

namespace LPEUpdate
{
    public class LpeAcceptableFile
	{
		#region Variables
		private AcceptableRsFile			m_accRsFile = null;
		private AcceptableRsFileVersion		m_accRsFileVersion = null;
		private DateTime					m_effectiveTime = SmallDateTime.MinValue;
		private bool						m_hasChanged = false;
		private bool						m_processorSuccessful = true;
		private string						m_filePath = "";
		private string						m_filename = "";
		private string						m_ratesheetFilePath = "";
		private string						m_ratesheetFilename = "";
		private long						m_versionNumber = -1;
		private E_RatesheetFileMoveAction	m_ratesheetMoveAction = E_RatesheetFileMoveAction.LeaveInDownloadPath;

		public E_RatesheetFileMoveAction RatesheetFileMoveAction
		{
			get { return m_ratesheetMoveAction; }
			set { m_ratesheetMoveAction = value; }
		}

		public string Filename
		{
			get { return m_filename; }
		}

		public string FilePath
		{
			get { return m_filePath; }
		}

		public string RatesheetFilePath
		{
			get { return m_ratesheetFilePath; }
		}

		public string RatesheetFilename
		{
			get { return m_ratesheetFilename; }
		}

		public long VersionNumber
		{
			get { return m_versionNumber; }
		}

        /// <summary>
        /// Because we are split the LpeUpdate download to separate process, we can no longer use the LpeUpdate.exe start time as the time we download
        /// investor ratesheet. Rather we will use the file timestamp.
        /// </summary>
        public DateTime InvestorRatesheetDownloadTime
        {
            get;
            set;
        }
		#endregion

		public LpeAcceptableFile(string outputFilePath, string ratesheetFilePath, DateTime effectiveTime, bool processedSuccessfully, bool hasChanged)
		{
			m_filePath = outputFilePath;
			m_filename = System.IO.Path.GetFileName(m_filePath);
			m_ratesheetFilePath = ratesheetFilePath;
			m_ratesheetFilename = System.IO.Path.GetFileName(m_ratesheetFilePath);
			m_effectiveTime = effectiveTime;
			m_processorSuccessful = processedSuccessfully;
            m_hasChanged = hasChanged;
            InvestorRatesheetDownloadTime = DateTime.Now;
            try
            {
                m_accRsFile = new AcceptableRsFile(m_filename);

                m_accRsFileVersion = new AcceptableRsFileVersion(m_filename);

                if (m_processorSuccessful)
                {
                    m_accRsFile.UseForBothMapAndBot = true;
                    if (m_accRsFile.DeploymentType.ToLower() == "useversionaftergeneratinganoutputfileok")
                        m_accRsFile.DeploymentType = "UseVersion";
                    if (m_hasChanged)
                    {
                        AcceptableRsFileVersion newVersion = new AcceptableRsFileVersion();
                        newVersion.RatesheetFileId = m_filename;
                        newVersion.FirstEffective = m_effectiveTime;
                        newVersion.LastestEffective = m_effectiveTime;
                        m_accRsFileVersion = newVersion;
                    }
                    else
                    {
                        if (m_accRsFileVersion.IsNew)
                        {
                            m_accRsFileVersion.FirstEffective = m_effectiveTime;
                        }
                        m_accRsFileVersion.LastestEffective = m_effectiveTime;
                        m_accRsFileVersion.IsExpirationIssuedByInvestor = false; // dd 5/15/2018 - OPM 468433 - Also unexpired the ratesheet version when update the latest effective time.
                    }
                    m_accRsFile.Save();
                    m_accRsFileVersion.Save();
                    m_versionNumber = m_accRsFileVersion.VersionNumber;
                }
                else
                {
                    m_accRsFile.UseForBothMapAndBot = false;
                    m_accRsFile.Save();
                }
            }
            catch (MissingAcceptableRsFileException)
            {
                throw;
            }
            catch (Exception e)
            {
                Tools.LogError("Unable to create LpeAcceptableFile representation (Required for Rate Lock Expiration feature).  Details: " + e);
                throw;
            }
		}
	}
}