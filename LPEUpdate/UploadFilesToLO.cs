﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Text;
using DataAccess;
using LendersOffice.Common;
using LPEUpdate.CustomRatesheetProcessing;
using LpeUpdateBotLib.Common;
using Toolbox.Distributed;

namespace LPEUpdate
{
    /// <summary>
    /// Upload all the new output files to Lpe database.
    /// </summary>
    public class UploadFilesToLO
    {
        private List<FileToUpload> FilesToUpload;
        private List<LpeAcceptableFile> RateoptionFilesToUpload;
        private List<ICustomRatesheet> CustomRateSheetListToUpload;
        private StringBuilder LogMessageBuffer;

        public UploadFilesToLO(List<FileToUpload> filesToUpload, List<LpeAcceptableFile> rateoptionFilesToUpload, List<ICustomRatesheet> customRateSheetListToUpload)
        {
            this.FilesToUpload = filesToUpload;
            this.RateoptionFilesToUpload = rateoptionFilesToUpload;
            this.CustomRateSheetListToUpload = customRateSheetListToUpload;
            this.LogMessageBuffer = new StringBuilder();
        }

        public void Upload()
        {
            PerformanceStopwatch.ResetAll();

            bool isNotifyICityMortgage = false;
            bool isNotifyCmg = false;
            bool isNotifyFirstTech = false;

            bool isNotifyCmgJumbo = false;
            bool isNotifyPml0278Orion = false;
            bool isNotifyPacUnionCustomZip = false;
            bool isNotifyPml0185 = false;
            Hashtable ratesheetSuccessStatus = new Hashtable();
            Stopwatch timer = Stopwatch.StartNew();
            Log("UploadFiles started.");

            string holderName = "LPEUpdate " + DateTime.Now.ToShortTimeString() + " ";

            // attempt to acquire an exclusive lock to write data LPE database
            CSingletonDistributed tix = null;
            while (tix == null)
            {
                try
                {
                    tix = CSingletonDistributed.RequestSingleton(CSingletonDistributed.SingletonObjectT.LpeDataAccessTicket, holderName, 120);
                }
                catch (CLpeLoadingDeniedException)
                {
                    Tools.LogWarning("Unable to obtain write Lock. Sleeping for 5 seconds.");
                    System.Threading.Thread.Sleep(5 * 1000); // sleep for 5 seconds
                    Tools.LogWarning("Sleep done. Continuing update.");
                }
            }

            try
            {
                LOInterop lo = new LOInterop();
                #region Ratesheet Map Output files
                //db TODO - add in Adjust Table files here once the importer can handle them - see OPM 32647 for details
                foreach (LpeAcceptableFile lpeAccFile in this.RateoptionFilesToUpload)
                {
                    try
                    {
                        string filepath = lpeAccFile.FilePath;

                        // 12/30/2010 dd - Use the timestamp from the investor ratesheet in download path
                        lo.UploadRates(filepath, lpeAccFile.InvestorRatesheetDownloadTime, lpeAccFile.Filename, lpeAccFile.VersionNumber, this.Log);

                        string sDestPath = Config.FILES_PATH + lpeAccFile.RatesheetFilename; ;
                        string sFilePath = lpeAccFile.RatesheetFilePath;

                        // OPM 20084 - only move ratesheet from download path upon successful import
                        if (lpeAccFile.RatesheetFileMoveAction == E_RatesheetFileMoveAction.MoveToTemplatesFolder)
                        {
                            if (ratesheetSuccessStatus.ContainsKey(lpeAccFile.RatesheetFilename))
                            {
                                ratesheetSuccessStatus[lpeAccFile.RatesheetFilename] = ((bool)ratesheetSuccessStatus[lpeAccFile.RatesheetFilename]) && true;
                            }
                            else
                            {
                                ratesheetSuccessStatus.Add(lpeAccFile.RatesheetFilename, true /*can move from download path*/);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        string rsFilename = lpeAccFile.RatesheetFilename;
                        string filenameNoPath = System.IO.Path.GetFileName(rsFilename);

                        string srcPath = System.IO.Path.Combine(Config.FILES_PATH, filenameNoPath);
                        string destPath = System.IO.Path.Combine(PathCfg.DOWNLOAD_PATH, filenameNoPath);

                        if (ratesheetSuccessStatus.ContainsKey(lpeAccFile.RatesheetFilename))
                        {
                            ratesheetSuccessStatus[lpeAccFile.RatesheetFilename] = false;  // do not remove from download path
                        }
                        else
                        {
                            ratesheetSuccessStatus.Add(lpeAccFile.RatesheetFilename, false /*do not remove from download path*/);
                        }
                        Log(string.Format("   Uploading {0} FAILED {1}.", lpeAccFile.Filename, ex.ToString()));
                        LpeAdminMessaging.SendEmail(E_LpeErrorAreaT.RateUpload, lpeAccFile.Filename, ex.ToString());
                    }
                }

                #endregion

                #region Schema Output Files
                foreach (FileToUpload file in this.FilesToUpload)
                {
                    try
                    {
                        switch (file.UploadType)
                        {
                            case FileToUpload.UPLOAD_TYPE.Policy:
                                lo.UploadAdjustments(file.FileName, this.Log);
                                break;
                            case FileToUpload.UPLOAD_TYPE.Rate:
                                lo.UploadRates(file.FileName, file.InvestorRatesheetDownloadTime, file.LpeAcceptableFilename, file.VersionNumber, this.Log);
                                break;
                        }

                        file.SaveUploadResult(true, null);
                    }
                    catch (Exception ex)
                    {
                        Log(string.Format("   Uploading {0} type {1} FAILED. {2}", file.FileName, file.UploadType, ex.ToString()));

                        file.SaveUploadResult(false, ex.ToString());
                    }
                }
                #endregion

                #region Upload Custom Ratesheet
                if (this.CustomRateSheetListToUpload != null)
                {
                    foreach (var customRatesheet in this.CustomRateSheetListToUpload)
                    {
                        Stopwatch sw = Stopwatch.StartNew();
                        LpeAcceptableFile acceptableFile = null;
                        try
                        {
                            acceptableFile = new LpeAcceptableFile(customRatesheet.GetOutputFile(), customRatesheet.RatesheetName, DateTime.Now, true, true);

                            lo.UploadRates(acceptableFile.FilePath, acceptableFile.InvestorRatesheetDownloadTime, acceptableFile.Filename, acceptableFile.VersionNumber, this.Log);

                            var rateOptionsList = customRatesheet.GetRateOptionsMarginPmlBroker();

                            if (rateOptionsList != null)
                            {
                                foreach (var rateOption in rateOptionsList)
                                {
                                    SqlParameter[] parameters =
                                    {
                                        new SqlParameter("@BrokerId", rateOption.BrokerId),
                                        new SqlParameter("@PmlBrokerIndexNumber", rateOption.PmlBrokerIndexNumber),
                                        new SqlParameter("@RatesMarginProtobufContent", SerializationHelper.ProtobufSerialize(rateOption.Items)),
                                        new SqlParameter("@LastModifiedDate", acceptableFile.InvestorRatesheetDownloadTime)
                                    };

                                    StoredProcedureHelper.ExecuteNonQuery(DataSrc.LpeSrc, "RATE_OPTIONS_MARGIN_PML_BROKER_Upsert", 1, parameters);
                                }
                            }
                            ratesheetSuccessStatus[acceptableFile.RatesheetFilename] = true;
                        }
                        catch (Exception exc)
                        {
                            if (acceptableFile != null)
                            {
                                ratesheetSuccessStatus[acceptableFile.RatesheetFilename] = false;
                                Log($"   Upload CustomRateSheet [{customRatesheet.GetType().FullName}] {acceptableFile.Filename} FAILED {exc.ToString()}.");
                                LpeAdminMessaging.SendEmail(E_LpeErrorAreaT.RateUpload, acceptableFile.Filename, exc.ToString());
                            }
                        }

                        sw.Stop();
                        Log($"    Process CustomRateSheet [{customRatesheet.GetType().FullName}] in {sw.ElapsedMilliseconds:#,#}ms.");

                    }
                }
                #endregion

                foreach (string rsFileName in ratesheetSuccessStatus.Keys)
                {
                    if ((bool)ratesheetSuccessStatus[rsFileName] == true)
                    {
                        string sDestPath = Config.FILES_PATH + rsFileName; ;
                        string sFilePath = PathCfg.DOWNLOAD_PATH + rsFileName;
                        try
                        {
                            Tools.FileCopy(sFilePath, sDestPath);
                            Tools.FileDelete(sFilePath);

                            // 2/15/2012 dd - eOPM 314517 Send confirmation email to client
                            // after process ratesheet successfully.
                            // Long term we want to have a nicer framework that handle list
                            // of ratesheet and email template.
                            // For now I will just hardcode the name.

                            if (rsFileName.Equals("PML0179_MARGINS.XLS", StringComparison.OrdinalIgnoreCase))
                            {
                                isNotifyICityMortgage = true;
                            }
                            if (rsFileName.Equals("PML0213_CUSTOM.xls", StringComparison.OrdinalIgnoreCase))
                            {
                                isNotifyCmg = true;
                            }
                            if (rsFileName.Equals("PML0213_CUSTOM_JUMBO.xls", StringComparison.OrdinalIgnoreCase))
                            {
                                isNotifyCmgJumbo = true;
                            }

                            if (rsFileName.Equals("PML0223_CUSTOM.xls", StringComparison.OrdinalIgnoreCase) ||
                                rsFileName.Equals("PML0223_CUSTOM_EQT.xls", StringComparison.OrdinalIgnoreCase))
                            {
                                isNotifyFirstTech = true; // OPM 143324
                            }

                            // OPM 237273, 2/8/2016, ML
                            if (rsFileName.Equals("PML0176_CUSTOM_CORR_546.XLS", StringComparison.OrdinalIgnoreCase) ||
                                rsFileName.Equals("PML0176_CUSTOM_RETAIL_AUSTIN.XLS", StringComparison.OrdinalIgnoreCase) ||
                                rsFileName.Equals("PML0176_CUSTOM_RETAIL_AUSTIN_JUMBO_C.XLS", StringComparison.OrdinalIgnoreCase))
                            {
                                // We want to include the name of the rate sheet in the email, so instead of deferring
                                // the notification by using a boolean, we'll send the email now and include the name
                                // of the rate sheet.
                                UploadProcess.NotifyPacUnion(rsFileName);
                            }

                            // OPM 476837 - dd - 1/16/2019 - Notify PacUnion when custom zip file is uploaded.
                            if (rsFileName.Equals("PML0176_CUSTOM_CORR_v2_BASE.zip", StringComparison.OrdinalIgnoreCase))
                            {
                                isNotifyPacUnionCustomZip = true;
                            }

                            // OPM 361623 - dd 11/30/2016
                            if (rsFileName.Equals("PML0278_CUSTOM_WHOLESALE.XLS", StringComparison.OrdinalIgnoreCase))
                            {
                                isNotifyPml0278Orion = true;
                            }

                            // OPM 477357 - dd 1/16/2019
                            if (rsFileName.Equals("PML0185_CUSTOM.XLS", StringComparison.OrdinalIgnoreCase) ||
                                rsFileName.Equals("PML0185_GSE_RS.XLS", StringComparison.OrdinalIgnoreCase))
                            {
                                isNotifyPml0185 = true;
                            }
                        }
                        catch (Exception exc)
                        {
                            Log("    Unable to copy " + sFilePath + " to " + sDestPath);
                            Log("        " + exc.Message);
                            LpeAdminMessaging.SendEmail(E_LpeErrorAreaT.Schema, "SOMEONE USING THE FILE " + sDestPath, exc.Message);
                        }
                    }
                }

                if (this.FilesToUpload.Count > 0 || this.RateoptionFilesToUpload.Count > 0)
                {
                    /// call to tell the LPE cache to update itself if changes were made
                    Toolbox.LoanProgram.EventHandlers.AfterLpeUpdateProjectIsRun();

                    Toolbox.RatePrice.Invalidate(); // Trigger price engine data change.
                    LendersOfficeApp.los.RatePrice.LpeReleaseController.FastReleaseSnapshot();
                }
            }
            finally
            {
                tix.Dispose(); /// release the write-lock
            }

            if (isNotifyPacUnionCustomZip)
            {
                UploadProcess.NotifyPacUnion("PML0176_CUSTOM_CORR_v2_BASE.zip");
            }

            if (isNotifyICityMortgage)
            {
                UploadProcess.NotifyICity();
            }
            if (isNotifyCmg)
            {
                UploadProcess.NotifyCMG();
            }

            if (isNotifyCmgJumbo)
            {
                UploadProcess.NotifyLendingQBCMGJumbo();
            }

            if (isNotifyFirstTech)
            {
                UploadProcess.NotifyFirstTech();
            }

            if (isNotifyPml0278Orion)
            {
                UploadProcess.NotifyPml0278Orion();
            }

            if (isNotifyPml0185)
            {
                UploadProcess.NotifyPml0185();
            }

            Log($"UploadFiles finished in {timer.ElapsedMilliseconds:#,#}ms.");

            Tools.LogInfo("LpeUploadRates", $"[LpeUploadRates] Executed in {timer.ElapsedMilliseconds:#,#}ms. {PerformanceStopwatch.ReportOutput} {this.LogMessageBuffer}");
        }

        /// <summary>
        /// Put message to LogMessageBuffer. This method IS NOT thread safe.
        /// </summary>
        /// <param name="message">Message to log.</param>
        private void Log(string message)
        {
            this.LogMessageBuffer.AppendLine($"[{DateTime.Now.ToLongTimeString()}] - {message}");
        }
    }
}