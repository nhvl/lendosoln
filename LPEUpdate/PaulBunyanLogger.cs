/// Author: David Dao
using LendersOffice.Common;
using LpeUpdateBotLib.Common;

namespace LPEUpdate
{
    public class PaulBunyanLogger : ILogger
	{
        public void Log(string msg) 
        {
            DataAccess.Tools.LogInfo(msg);
        }

        public void LogErrorAndSendEmail(string msg) 
        {
            // 7/26/2010 dd - Assume Caller to this log is only in Bot.
            LpeAdminMessaging.SendEmail(E_LpeErrorAreaT.WebBotDownload, msg, msg);
        }

        public void LogErrorAndSendEmail(string subject, string msg)
        {
            LpeAdminMessaging.SendEmail(E_LpeErrorAreaT.WebBotDownload, subject, msg);
        }
	}
}