﻿// <copyright file="ContinuousServiceApplicationInitializer.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
// Author: David D
// Date: 9/22/2016
// </summary>

namespace LendersOffice
{
    using DataAccess;
    using LendersOffice.Drivers.Gateways;
    using LqbGrammar;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// This class will be invoke through reflection in ContinuousService to perform framework initialization.
    /// Do not remove this class despite of zero reference.
    /// </summary>
    public static class ContinuousServiceApplicationInitializer
    {
        /// <summary>
        /// Perform framework initialization before execute code in continuous service.
        /// </summary>
        public static void InitializeApplicationFramework()
        {
            IExceptionHandlerFactory[] arrHandlers = ExceptionHandlerUtil.GetCoreExceptionHandlerFactories();
            using (IApplicationInitialize iAppInit = LqbApplication.CreateInitializer(arrHandlers))
            {
                const string AppName = "ContinuousService";
                iAppInit.SetName(AppName);

                Adapter.ApplicationInitializer.RegisterFactories(AppName, iAppInit);
                LendersOffice.ApplicationInitializer.RegisterFactories(AppName, iAppInit);
            }

            // Moved from front of method to end of method so the file system driver can be used.  Leaving this at the front
            // of the method would force the code to reference System.IO.File directly because the TextFileHelper will
            // eventually use the file system driver.
            TextFileHelper.AppendString(@"C:\Temp\Test.log", System.DateTime.Now + " - InitializeApplicationFramework" + System.Environment.NewLine);

            Tools.SetupServicePointManager();
            Tools.SetupServicePointCallback();
        }
    }
}