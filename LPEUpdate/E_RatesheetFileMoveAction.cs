﻿namespace LPEUpdate
{
    public enum E_RatesheetFileMoveAction
    {
        LeaveInDownloadPath = 0,
        CopyToTemplatesFolder = 1,
        MoveToTemplatesFolder = 2,
        MoveToDownloadPathValidationFailed = 3
    }
}
