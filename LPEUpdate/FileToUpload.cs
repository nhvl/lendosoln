using System;

namespace LPEUpdate
{
    /// <summary>
    /// Base class for files to be uploaded to the pricing engine.
    /// 
    /// There are 2 types of files to upload: one affects rates and one affects policies
    /// 
    /// The purpose of these classes is to delegate the responsibility of updating databases, notifying users, etc.
    /// to the specific user/table that cares about it.
    /// </summary>
    public abstract class FileToUpload
	{
		public enum UPLOAD_TYPE { Policy, Rate } ;

		public FileToUpload(string filename, UPLOAD_TYPE uploadType)
		{
			m_filename = filename ;
			m_uploadType = uploadType ;
			m_lpeAcceptableFilename = System.IO.Path.GetFileName(filename);
		}
		public FileToUpload(string filename, UPLOAD_TYPE uploadType, string lpeAcceptableFilename, long versionNumber)
		{
			m_filename = filename ;
			m_uploadType = uploadType ;
			m_versionNumber = versionNumber;
			m_lpeAcceptableFilename = lpeAcceptableFilename;
		}

		public string FileName
		{
			get { return m_filename ; }
		}
		public UPLOAD_TYPE UploadType
		{
			get { return m_uploadType ; }
		}
		public long VersionNumber
		{
			get { return m_versionNumber; }
		}

		public string LpeAcceptableFilename
		{
			get { return m_lpeAcceptableFilename; }
		}
        /// <summary>
        /// Because we are split the LpeUpdate download to separate process, we can no longer use the LpeUpdate.exe start time as the time we download
        /// investor ratesheet. Rather we will use the file timestamp.
        /// </summary>
        public DateTime InvestorRatesheetDownloadTime
        {
            get;
            set;
        }
		public abstract void SaveUploadResult(bool isSuccessful, string errorMessage) ;

		private string m_filename ;
		private string m_lpeAcceptableFilename = "";
		private long m_versionNumber = -1;
		private UPLOAD_TYPE m_uploadType ;
	}
	public class FileToUpload_OutputFile : FileToUpload
	{
		public FileToUpload_OutputFile(int outputFileId, string filename, FileToUpload.UPLOAD_TYPE uploadType) : base(filename, uploadType)
		{
			m_OutputFileId = outputFileId ;
		}
		public FileToUpload_OutputFile(int outputFileId, string filename, string lpeAcceptableFilename, FileToUpload.UPLOAD_TYPE uploadType, long versionNumber) : base(filename, uploadType, lpeAcceptableFilename, versionNumber)
		{
			m_OutputFileId = outputFileId ;
		}
		public override void SaveUploadResult(bool isSuccessful, string errorMessage)
		{
			// delegate the function call to the OutputFile class
			OutputFile.HandleUploadResult(m_OutputFileId, isSuccessful, errorMessage, this.FileName) ;
		}

		private int m_OutputFileId ;
	}
}