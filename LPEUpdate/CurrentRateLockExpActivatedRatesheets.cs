using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Diagnostics;
using DataAccess;

namespace LPEUpdate
{
    public enum E_RateLockExpActivatedRSStatus
	{
		IsWorking = 1,
		IsNotWorking = 2,
		IsIgnore = 3
	}
	
	public class CurrentRateLockExpActivatedRatesheets
	{
		private Dictionary<string, RequiredRatesheet> m_ratesheets = null;
		public CurrentRateLockExpActivatedRatesheets()
		{
            Initialize(E_RateLockExpActivatedRSStatus.IsIgnore);
		}

        private void Initialize(E_RateLockExpActivatedRSStatus status)
        {
            m_ratesheets = new Dictionary<string, RequiredRatesheet>(StringComparer.OrdinalIgnoreCase);
            FillList(status);
        }

		public void MarkStatus(string ratesheetName, E_RateLockExpActivatedRSStatus eStatus)
		{
			if(m_ratesheets.ContainsKey(ratesheetName))
			{
				RequiredRatesheet requiredRS = m_ratesheets[ratesheetName];
				requiredRS.Status = eStatus;
			}
		}

		public void Save()
		{
            Stopwatch sw = Stopwatch.StartNew();
            foreach (var ratesheet in m_ratesheets.Values)
            {
                bool isBothRsMapAndBotWorking = false;

                switch (ratesheet.Status)
                {
                    case E_RateLockExpActivatedRSStatus.IsWorking:
                        isBothRsMapAndBotWorking = true;
                        break;
                    case E_RateLockExpActivatedRSStatus.IsNotWorking:
                        isBothRsMapAndBotWorking = false;
                        break;
                    case E_RateLockExpActivatedRSStatus.IsIgnore:
                        continue;
                    default:
                        throw new UnhandledEnumException(ratesheet.Status);
                }

                SqlParameter[] parameters = {
                                                new SqlParameter("@InvestorXlsFileId", ratesheet.RatesheetId),
                                                new SqlParameter("@IsBothRsMapAndBotWorking", isBothRsMapAndBotWorking)
                                            };
                StoredProcedureHelper.ExecuteNonQuery(DataSrc.RateSheet, "RS_File_UpdateRsAndBotWorking", 3, parameters);

            }
            sw.Stop();
            ReportingTools.Log("    CurrentRateLockExpActivatedRatesheets.Save - Updated " + m_ratesheets.Count + " rs in " + sw.ElapsedMilliseconds + "ms.");
		}

		private void FillList(E_RateLockExpActivatedRSStatus eStatus)
		{
			using(DbDataReader reader = StoredProcedureHelper.ExecuteReader( DataSrc.RateSheet, "Investor_Xls_File_ListAll"))
			{
				while(reader.Read())
				{
					long ratesheetId = (long)reader["InvestorXlsFileId"];
					string ratesheetName = (string)reader["InvestorXlsFileName"];
					if(!m_ratesheets.ContainsKey(ratesheetName))
					{
						m_ratesheets[ratesheetName] = new RequiredRatesheet(ratesheetName, ratesheetId, eStatus);
					}
				}
			}
		}
	}

	public class RequiredRatesheet
	{
		private string m_ratesheetName = "";
		private long m_ratesheetId = -1;
		private E_RateLockExpActivatedRSStatus m_status = E_RateLockExpActivatedRSStatus.IsIgnore;

		public long RatesheetId
		{
			get { return m_ratesheetId; }
		}

		public E_RateLockExpActivatedRSStatus Status
		{
			get { return m_status; }
			set { m_status = value;}
		}
		
		public RequiredRatesheet(string ratesheetName, long ratesheetId, E_RateLockExpActivatedRSStatus eStatus)
		{
			m_ratesheetName = ratesheetName;
			m_ratesheetId = ratesheetId;
			m_status = eStatus;
		}
	}
}