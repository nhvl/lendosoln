namespace LPEUpdate
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using LendersOffice.Common;
    using LendersOffice.Drivers.Gateways;
    using LendersOffice.LoanPrograms;

    public class LOInterop
	{
		public LOInterop()
		{
		}
		public void UploadRates(string sFileName, DateTime lRateSheetDownloadStartD, string lpeAcceptablefilename, long versionNumber, Action<string> logger)
		{
            using (PerformanceStopwatch.Start("LOInterop.UploadRates"))
            {
                Stopwatch sw = Stopwatch.StartNew();

                Action<BinaryFileHelper.LqbBinaryStream> readHandler = delegate(BinaryFileHelper.LqbBinaryStream str)
                {
                    RateOptionImport import = new RateOptionImport();

                    IEnumerable<string> errorList;

                    int rowsAffected = import.Import(str.Stream, lRateSheetDownloadStartD, lpeAcceptablefilename, versionNumber, out errorList);

                    if (rowsAffected >= 0)
                    {
                        logger($"RATE_UPDATE_FILE import {sFileName} was successful. {rowsAffected} items imported. Execute in {sw.ElapsedMilliseconds:#,#}ms.");
                    }
                    else
                    {
                        string sError = "";
                        foreach (string s in errorList)
                        {
                            sError += "\n" + s;
                        }
                        throw new ApplicationException("RATE_UPDATE_FILE import  " + sFileName + " ***FAILED***: " + sError);
                    }
                };

                BinaryFileHelper.OpenRead(sFileName, readHandler);
            }
		}

		public void UploadAdjustments(string sFileName, Action<string> logger)
		{
            using (PerformanceStopwatch.Start("LOInterop.UploadAdjustments"))
            {
                Action<BinaryFileHelper.LqbBinaryStream> readHandler = delegate(BinaryFileHelper.LqbBinaryStream str)
                {
                    Stopwatch sw = Stopwatch.StartNew();

                    IEnumerable<string> errorList = null;

                    RateAdjustImport import = new RateAdjustImport();
                    if (import.Import(str.Stream, out errorList))
                    {
                        logger($"ADJUSTMENT_UPDATE_FILE import {sFileName} was successful. Execute in {sw.ElapsedMilliseconds:#,#}ms.");
                    }
                    else
                    {
                        string sError = "";
                        foreach (string s in errorList)
                        {
                            sError += "\n" + s;
                        }
                        throw new ApplicationException("ADJUSTMENT_UPDATE_FILE import  " + sFileName + " ***FAILED***: " + sError);
                    }
                };

                BinaryFileHelper.OpenRead(sFileName, readHandler);
            }
		}
	}
}