﻿using LendersOffice.Common;
using LendersOffice.Constants;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LendersOffice.RatePrice.CustomRatesheet;

namespace LPEUpdate.CustomRatesheetProcessing
{
    class Pml0176PacUnionCustomRateSheet : ICustomRatesheet
    {
        public Pml0176PacUnionCustomRateSheet(string zipFile)
        {
            if (string.IsNullOrEmpty(zipFile))
            {
                return; // No-op if zipfile is empty.
            }

            this.rateoptMarginBrokerList = new List<RateOptionsMarginPmlBroker>();

            string path = Path.Combine(ConstApp.TempFolder, Guid.NewGuid().ToString());
            CompressionHelper.DecompressIOCompressedFile(new FileInfo(zipFile), new DirectoryInfo(path), deleteExisting: true);
            foreach (var file in Directory.GetFiles(path, "*.csv"))
            {
                var fi = new FileInfo(file);
                if( !char.IsDigit(fi.Name[0])) // only process file {PmlBrokerIndex}.csv with PmlBrokerIndex as integer  
                {
                    continue;
                }

                this.rateoptMarginBrokerList.Add(ParseTpoFile(fi) );
            }

            // dd 10/23/2018 - The output file name must match with the AcceptableRatesheet in database.
            this.outputFileName = Path.Combine(ConstApp.TempFolder, "PML0176_CUSTOM_CORR_v2_BASE_Output.csv");
            this.CreateOutputFileFromBaseCsv(Path.Combine(path, "base.csv"), this.outputFileName);

            this.isValid = true;

            try
            {
                Directory.Delete(path, true);
            }
            catch( SystemException)
            {
            }
        }

        public string RatesheetName
        {
            get
            {
                return "PML0176_CUSTOM_CORR_v2_BASE.zip";
            }
        }

        public string GetOutputFile()
        {
            return outputFileName;
        }

        public IEnumerable<RateOptionsMarginPmlBroker> GetRateOptionsMarginPmlBroker()
        {
            return rateoptMarginBrokerList;
        }

        public bool Validate()
        {
            return isValid;
        }

        private RateOptionsMarginPmlBroker ParseTpoFile(FileInfo fi)
        {
            /*
                lLpTemplateId,Product Code,Note Rate,Margin
                040b2e82-0d4c-4a58-89cf-3361a523d40c,CFH10,3,2.96
                040b2e82-0d4c-4a58-89cf-3361a523d40c,CFH10,3.125,2.96             
            */
            
            // file name is broker index.
            int brokerIdx = int.Parse(Path.GetFileNameWithoutExtension(fi.Name));

            string[] headers = null;
            var ratesMarginList = new List<RatesMargin>();

            using (FileStream inStream = File.OpenRead(fi.FullName))
            {
                var sr = new ImportStreamReader(inStream);
                Guid curId = Guid.Empty;
                RatesMargin ratesMargin = null;

                try
                {
                    while (true)
                    {
                        string str = sr.ReadLine();
                        if (str == null || str.Trim() == string.Empty)
                        {
                            continue; // Skip Blank line.
                        }

                        if (headers == null)
                        {
                            headers = sr.Parsed;
                            continue;
                        }

                        Guid id = new Guid(sr.Parsed[0]);
                        string noteRate = sr.Parsed[2];
                        string margin = sr.Parsed[3];

                        if (curId != id)
                        {
                            curId = id;
                            ratesMargin = new RatesMargin()
                            {
                                lLpTemplateId = id,
                                Items = new List<RatesMarginItem>()
                            };

                            ratesMarginList.Add(ratesMargin);
                        }

                        ratesMargin.Items.Add(new RatesMarginItem()
                        {
                            NoteRate = noteRate,
                            Margin = margin
                        });
                    }

                }
                catch (EndOfStreamException)
                {
                }

            }

            return new RateOptionsMarginPmlBroker()
            {
                BrokerId = pml0176BrokerId,
                PmlBrokerIndexNumber = brokerIdx,
                Items = ratesMarginList
            };
        }

        private class RowRate
        {
            public string Rate;
            public decimal []Bases;  // Base 7,Base 15,Base 30,Base 45,Base 60,Base 75,Base 90
        }

        private void CreateOutputFileFromBaseCsv(string baseCsvFile, string outputFileName)
        {
            // base.csv content
            //      lLpTemplateId,Product Code,Rate,Base 7,Base 15,Base 30,Base 45,Base 60,Base 75,Base 90,PRICING DATE,DAY ACTIVITY

            string[] headers = null;

            const int idCol = 0;
            const int produceNameCol = 1;
            const int rateCol = 2;
            const int baseCol = 3;

            using (FileStream inStream = File.OpenRead(baseCsvFile))
            using (StreamWriter outStream = new StreamWriter(outputFileName))
            {
                try
                {
                    var sr = new ImportStreamReader(inStream);
                    string curKey = string.Empty;
                    List<RowRate> rowRates = null;
                    string productName = string.Empty;
                    string programId = string.Empty;
                    int[] bases = { 7, 15, 30, 45, 60, 75, 90 }; // Base 7,Base 15,Base 30,Base 45,Base 60,Base 75,Base 75

                    Action write = () =>
                    {
                        if (rowRates == null)
                        {
                            return;
                        }

                        outStream.WriteLine($"Product Name,{productName}");
                        outStream.WriteLine($"Program Id,{programId}");
                        for (int i = 0, prev = -1; i < bases.Length; i++)
                        {
                            // Write Lock,0,7   Lock,8,15 ...
                            outStream.WriteLine($"LOCK,{prev + 1},{bases[i]}");
                            prev = bases[i];
                            foreach (var rowRate in rowRates)
                            {
                                outStream.WriteLine($"{rowRate.Rate},{(100 - rowRate.Bases[i])},0,0,0");
                            }
                        }
                    };

                    while (true)
                    {
                        string str = sr.ReadLine();
                        if (str == null || str.Trim() == string.Empty)
                        {
                            continue; // Skip Blank line.
                        }

                        if (headers == null)
                        {
                            headers = sr.Parsed;
                            if (headers.Length < 12)
                                throw new ArgumentException("Invalid PML0176 base file: expect #column >= 12");
                            continue;
                        }

                        if (string.IsNullOrEmpty(sr.Parsed[0]))
                        {
                            write();
                            rowRates = null;
                            continue;
                        }

                        if (rowRates == null)
                        {
                            rowRates = new List<RowRate>();
                            programId = sr.Parsed[idCol];
                            productName = sr.Parsed[produceNameCol];
                        }

                        var rowRate = new RowRate()
                        {
                            Rate = sr.Parsed[rateCol],
                            Bases = new decimal[bases.Length]
                        };

                        // get "Base 7,Base 15,Base 30,Base 45,Base 60,Base 75,Base 90" (start at column 3)
                        for (int i = 0; i < bases.Length; i++)
                        {
                            rowRate.Bases[i] = decimal.Parse(sr.Parsed[baseCol + i]);
                        }

                        rowRates.Add(rowRate);
                    }

                }
                catch (EndOfStreamException)
                {
                }
            }
        }

        private static Guid pml0176BrokerId = new Guid("79991CF9-E793-4609-A2EA-E63995947A27");
        private bool isValid = false;
        private List<RateOptionsMarginPmlBroker> rateoptMarginBrokerList;
        private string outputFileName = string.Empty;
    }
}
