﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LendersOffice.RatePrice.CustomRatesheet;

namespace LPEUpdate.CustomRatesheetProcessing
{
    public class RateOptionsMarginPmlBroker
    {
        public Guid BrokerId { get; set; }
        public int PmlBrokerIndexNumber { get; set; }
        public List<RatesMargin> Items { get; set; }

    }
}
