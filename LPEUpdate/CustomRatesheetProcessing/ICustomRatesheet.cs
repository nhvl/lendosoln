﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LendersOffice.RatePrice.CustomRatesheet;

namespace LPEUpdate.CustomRatesheetProcessing
{
    public interface ICustomRatesheet
    {
        string RatesheetName { get; }
        bool Validate();
        string GetOutputFile();
        IEnumerable<RateOptionsMarginPmlBroker> GetRateOptionsMarginPmlBroker();
    }
}
