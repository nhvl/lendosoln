namespace LPEUpdate.InputSchemas
{
    using System.IO;
    using ExcelLibraryClient;
    using LendersOffice.Drivers.Gateways;
    using LendersOfficeApp.ObjLib.RulesManagement;
    using LpeUpdateBotLib.Common;
    using System.Linq;
    using System;

    public class SchemaManager
	{
        private static string[] SupportExtensions = { ".xlsx", ".xls" };

		public SchemaManager()
		{
		}

		public SchemaFile GetSchemaFile(string sInputFileName)
		{
            if (string.IsNullOrEmpty(sInputFileName))
            {
                return null;
            }

            string extension = Path.GetExtension(sInputFileName);
            string nameWithoutExtension = Path.GetFileNameWithoutExtension(sInputFileName);

            if (!SupportExtensions.Contains(extension, StringComparer.OrdinalIgnoreCase))
            {
                return null; // Extension is not excel.
            }

            // insert the text "Schema" between the filename and the .XLS extension
            string sSchemaFileName = $"{nameWithoutExtension}_Schema{extension}";
			string sCompactedSchemaFileName = $"{nameWithoutExtension}_Schema.CPX";
			string sSchemaFilePath = PathCfg.MapSchemaPath(sSchemaFileName) ;
			string sCompactedSchemaFilePath = PathCfg.MapCompactedSchemaPath(sCompactedSchemaFileName) ;

			if (!FileOperationHelper.Exists(sSchemaFilePath)) return null ;
			if (!FileOperationHelper.Exists(sCompactedSchemaFilePath) ||
				(FileOperationHelper.GetLastWriteTime(sSchemaFilePath) >= FileOperationHelper.GetLastWriteTime(sCompactedSchemaFilePath)))
			{
                ExcelLibrary.ConvertToCompactFormat(sSchemaFilePath, sCompactedSchemaFilePath);
			}

			return new SchemaFile(sCompactedSchemaFilePath) ;
		}
	}
}