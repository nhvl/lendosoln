namespace LPEUpdate
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using DataAccess;
    using LendersOffice.Drivers.Gateways;
    using LpeUpdateBotLib.Common;

    public enum E_MarketIndexFileDependentStatus
	{
		NeedsReprocessing = 1,
		DoesNotNeedReprocessing = 2
	}
	
	public class MarketIndexDependentFiles
	{
		private Hashtable m_dependentFiles;
		private static string s_filename = System.IO.Path.Combine(PathCfg.MARKET_INDEX_PATH, Config.MarketDataDependentFilesFileName);
		
		public MarketIndexDependentFiles()
		{
			m_dependentFiles = Hashtable.Synchronized(new Hashtable());
			FillList();
		}

		private void FillList()
		{
			if(!FileOperationHelper.Exists(s_filename))
				return;

			try
			{
                foreach (string ratesheetName in TextFileHelper.ReadLines(s_filename))
                {
                    if ((ratesheetName.Trim() != "") && !m_dependentFiles.ContainsKey(ratesheetName.Trim()))
                    {
                        m_dependentFiles.Add(ratesheetName.Trim(), E_MarketIndexFileDependentStatus.NeedsReprocessing);
                    }
                }
			}
			catch(Exception e)
			{
				Tools.LogError("Unable to create market index dependent file representation from file: " + s_filename, e);
			}
		}

		public void RemoveEntry(string ratesheetName)
		{
			if(m_dependentFiles.ContainsKey(ratesheetName))
				m_dependentFiles.Remove(ratesheetName);
		}

		public IEnumerable<string> ListMarketIndexFilesNeedReprocessing()
		{
			List<string> files = new List<string>();
			foreach(string key in m_dependentFiles.Keys)
			{
				if((E_MarketIndexFileDependentStatus)m_dependentFiles[key] == E_MarketIndexFileDependentStatus.NeedsReprocessing)
				{
					files.Add(key);
				}
			}
			return files;
		}

		public void AddEntry(string ratesheetName, E_MarketIndexFileDependentStatus eStatus)
		{
			if(m_dependentFiles.ContainsKey(ratesheetName))
			{
				m_dependentFiles[ratesheetName] = eStatus;
			}
			else
			{
				m_dependentFiles.Add(ratesheetName, eStatus);
			}
		}

	}
}