using System;

namespace LPEUpdate
{
    class MainClass
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            // 9/21/2016 - dd - This method is not in use. Only entry to the LpeUpdate is by using continuous service and create
            // LPEUpdate.LpeUpdate.
            //
            // In future merge this project with LpeUpdateDownloadProcess or LendersOfficeLib project.
        }
    }
}
