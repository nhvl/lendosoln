using System;
using System.Collections;
using System.Collections.Specialized;
using System.Xml;
using LpeUpdateBotLib.Common;
namespace LPEUpdate
{
    public class DependTree
	{
		public static void PopulateDependTreeData()
		{
			Hashtable parents = new Hashtable() ;
			Hashtable children = new Hashtable() ;

			XmlDocument xd = new XmlDocument() ;
			xd.Load(FileCfg.DEPENDENCY_FILE) ;
			foreach (XmlElement xeChild in xd.SelectNodes("DependConfig/Templates/Template"))
			{
				string sChild = xeChild.SelectSingleNode("Name").InnerText.ToUpper() ;

				foreach(XmlElement xeParent in xeChild.SelectNodes("DependOn/Document"))
				{
					string sParent = xeParent.InnerText.ToUpper() ;

					AddToChain(parents, sParent, sChild) ;
					AddToChain(children, sChild, sParent) ;
				}
			}

			s_ancestors = FlattenGraph(children) ;
			s_descendants = FlattenGraph(parents) ;
		}

		static void AddToChain(Hashtable table, string key, string name)
		{
			StringCollection strcol = (StringCollection)table[key] ;
			if (strcol == null)
			{
				strcol = new StringCollection() ;
				table[key] = strcol ;
			}
			strcol.Add(name) ;
		}
		static Hashtable FlattenGraph(Hashtable graph)
		{
			Hashtable flattened = new Hashtable() ;
			foreach(string key in graph.Keys)
			{
				StringCollection dependents = new StringCollection() ;

				Queue q = new Queue() ;
				q.Enqueue(key) ;
				while (q.Count > 0)
				{
					StringCollection strcol = (StringCollection)graph[q.Dequeue().ToString()] ;
					if (strcol != null)
					{
						foreach (string s in strcol)
						{
							if (!dependents.Contains(s))
								dependents.Add(s) ;

							q.Enqueue(s) ;
						}
					}
				}

				string[] rg = new String[dependents.Count] ;
				dependents.CopyTo(rg, 0) ;
				flattened[key] = rg ;
			}

			return flattened ;
		}
		public string[] GetAnscestors(string sNode)
		{
			return (string[])s_ancestors[sNode] ;
		}
		public string[] GetDescendants(string sNode)
		{
			return (string[])s_descendants[sNode] ;
		}

		private static Hashtable s_ancestors ;		// list of anscestors
		private static Hashtable s_descendants ;	// list of descendants
	}
}