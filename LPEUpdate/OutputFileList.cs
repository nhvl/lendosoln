using System;
using System.Collections.Generic;
using DataAccess;
namespace LPEUpdate
{
    public class OutputFileList
	{
		public OutputFileList()
		{
		}
		public void SetDirtyFlag(string sFileName)
		{
            List<OutputFile> list;
            if (m_dictionary.TryGetValue(sFileName, out list)) 
            {
                foreach (var f in list)
                {
                    f.Dirty = true;
                }
            }
		}
		public List<OutputFile> GetEnabledDirtyFiles()
		{
			List<OutputFile> retlist = new List<OutputFile>() ;
			foreach(var tmplist in m_dictionary.Values)
			{
				foreach(OutputFile f in tmplist)
				{
					if (f.Enabled && f.Dirty)
						retlist.Add(f) ;
				}
			}

			return retlist ;
		}

        private Dictionary<string, List<OutputFile>> m_dictionary;

        public void PopulateOutputFileList()
        {
            m_dictionary = new Dictionary<string, List<OutputFile>>(StringComparer.OrdinalIgnoreCase);
            using (var reader = StoredProcedureHelper.ExecuteReader(DataSrc.RateSheet, "RS_OutputFile_ListAll"))
            {
                while (reader.Read())
                {
                    OutputFile output = new OutputFile(reader);

                    List<OutputFile> list = null;

                    if (m_dictionary.TryGetValue(output.FileName, out list) == false)
                    {
                        list = new List<OutputFile>();
                        m_dictionary.Add(output.FileName, list);
                    }
                    list.Add(output);
                }
            }

        }
	}
}
