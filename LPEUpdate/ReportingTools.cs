/// Author: David Dao

namespace LPEUpdate
{
    using System;
    using System.Text;
    using LendersOffice.Drivers.Gateways;
    using LpeUpdateBotLib.Common;

    public class ReportingTools
	{
        private static StringBuilder s_buffer = null;
        private static object s_bufferLock = new object();
        private static bool s_isInitialize = false;
        private static string s_fileName = "";

        public static void Initialize() 
        {
            lock (s_bufferLock) 
            {
                s_buffer = new StringBuilder();
                s_isInitialize = true;
                s_fileName = Config.FILES_PATH + string.Format("Logs\\{0}.log", DateTime.Now.ToString("yyyy-MM-dd_HH_mm"));
            }
        }

        public static void Log(string message) 
        {
            if (!s_isInitialize)
                Initialize();

            lock (s_bufferLock) 
            {
                Action<TextFileHelper.LqbTextWriter> writeHandler = delegate(TextFileHelper.LqbTextWriter writer)
                {
                    writer.AppendLine(string.Format("[{0}] - {1}", DateTime.Now.ToLongTimeString(), message));
                };

                TextFileHelper.OpenForAppend(s_fileName, writeHandler);
                s_buffer.AppendFormat("[{0}] - {1}{2}", DateTime.Now.ToLongTimeString(), message, Environment.NewLine);
            }
        }

        public static string GetLog()
        {
            lock (s_bufferLock)
            {
                if (null != s_buffer)
                {
                    return s_buffer.ToString();
                }
                return string.Empty;
            }
        }
        public static void Reset()
        {
            lock (s_bufferLock)
            {
                s_buffer = null;
                s_isInitialize = false;
            }
        }
	}
}