﻿namespace LendersOffice.Commands
{
    using System;
    using System.Linq;
    using System.Net;
    using System.Security.Cryptography.X509Certificates;
    using Common;
    using Constants;
    using DataAccess;
    using Drivers.FileSystem;
    using Integration.MortgageInsurance;
    using LendersOffice.ObjLib.Edocs.AutoSaveDocType;
    using LendersOffice.ObjLib.Security.ThirdParty;
    using LqbGrammar.Commands;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers.HttpRequest;
    using LqbGrammar.Exceptions;
    using Mismo231.MI.MortgageInsuranceResponse;

    /// <summary>
    /// Provides MI-specific business logic for executing a request to an MI vendor.
    /// Port Dependencies:
    /// <see cref="LqbGrammar.Queries.IConfigurationQueryFactory"/> (through <see cref="ConstStage"/>)
    ///     Uses configuration keys: <see cref="ConstStage.EnableThirdPartyCertificateManager"/>
    /// <see cref="LqbGrammar.Drivers.FileSystem.IFileSystemDriverFactory"/>
    /// <see cref="X509Certificate.CreateFromCertFile(string)"/>
    /// <see cref="ThirdPartyClientCertificateManager.RetrieveCertificate(LqbGrammar.DataTypes.LqbAbsoluteUri)"/>
    /// </summary>
    public class MortgageInsuranceHttpCommunication : IIntegrationHttpCommunication<MIResponseProvider>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MortgageInsuranceHttpCommunication"/> class.
        /// </summary>
        /// <param name="orderInfo">The order-specific data for the request.</param>
        /// <param name="vendor">The vendor information.</param>
        /// <param name="exportPath">The export path.</param>
        public MortgageInsuranceHttpCommunication(MIOrderInfo orderInfo, MortgageInsuranceVendorConfig vendor, LqbAbsoluteUri exportPath)
        {
            this.OrderInfo = orderInfo;
            this.VendorConfig = vendor;
            this.ExportPath = exportPath;
        }

        /// <summary>
        /// Gets the log header for an MI communication.
        /// </summary>
        public string LogHeader => "Mortgage Insurance";

        /// <summary>
        /// Gets a value indicating whether request timing should be captured and logged for this communication.
        /// </summary>
        public bool LogRequestTiming => true;

        /// <summary>
        /// Gets a value indicating whether the communication should be logged. MI Requests within pricing should not be logged, because it causes too many logs without useful info.
        /// </summary>
        public bool ShouldLog => !this.IsPricingRequest;

        /// <summary>
        /// Gets a value indicating whether this MI request is being executed through the pricing engine. Because of the volume of these requests, we want to minimize side effects.
        /// </summary>
        protected bool IsPricingRequest => this.OrderInfo.RequestType == E_MiRequestType.PricingEngine;

        /// <summary>
        /// Gets or sets the order data gathered for the MI request.
        /// </summary>
        protected MIOrderInfo OrderInfo { get; set; }

        /// <summary>
        /// Gets or sets the vendor information for the request.
        /// </summary>
        protected MortgageInsuranceVendorConfig VendorConfig { get; set; }

        /// <summary>
        /// Gets or sets the export path for sending the request.
        /// </summary>
        protected LqbAbsoluteUri ExportPath { get; set; }

        /// <summary>
        /// Determines whether the doc type for a response (MI Quote or MI Policy) is enabled for auto-save for the given broker.
        /// </summary>
        /// <param name="responseIsQuote">Whether the received response was for a Quote order. False = MI Policy order.</param>
        /// <param name="brokerId">The Broker ID of the broker to check for auto-save status.</param>
        /// <returns>Whether or not auto-save is enabled for the MI Document response's associated doc type.</returns>
        public static bool AutoSaveIsEnabledForDocType(bool responseIsQuote, Guid brokerId)
        {
            return (responseIsQuote && AutoSaveDocTypeFactory.IsAutoSaveEnabled(E_AutoSavePage.MortgageInsuranceQuoteDocuments, brokerId, E_EnforceFolderPermissions.False))
                || (!responseIsQuote && AutoSaveDocTypeFactory.IsAutoSaveEnabled(E_AutoSavePage.MortgageInsurancePolicyDocuments, brokerId, E_EnforceFolderPermissions.False));
        }

        /// <summary>
        /// Creates the HTTP Request options object to populate the web request.
        /// </summary>
        /// <returns>A new HTTP Request options object.</returns>
        public HttpRequestOptions GenerateHttpRequest()
        {
            ICredentials credentials =
                this.OrderInfo.LenderSettings == null 
                    ? null
                    : new NetworkCredential(this.OrderInfo.LenderSettings.Value.UserName, this.OrderInfo.LenderSettings.Value.Password);

            X509CertificateCollection certificates = null;
            if (!ConstStage.EnableThirdPartyCertificateManager)
            {
                var certLocation = LocalFilePath.Create($@"{ConstApp.CertificateFolder}\{this.VendorConfig.VendorType:G}.cer").ForceValue();

                if (FileOperationHelper.Exists(certLocation))
                {
                    certificates = new X509CertificateCollection(new[] { X509Certificate.CreateFromCertFile(certLocation.Value) });
                }
            }
            else
            {
                certificates = ThirdPartyClientCertificateManager.RetrieveCertificate(this.ExportPath);
            }

            StringContent request = new StringContent(this.OrderInfo.SerializeRequest());

            var requestOptions = new HttpRequestOptions()
            {
                PostData = request,
                Method = HttpMethod.Post,
                MimeType = MimeType.Application.Xml,
                Timeout = TimeoutInSeconds.Sixty,
                Credentials = credentials,
            };
            requestOptions.ClientCertificates = certificates;
            return requestOptions;
        }

        /// <summary>
        /// Generates additional timing data for the communication, if applicable.
        /// </summary>
        /// <returns>
        /// The additional timing data for the communication, if any.
        /// </returns>
        public IntegrationHttpCommunicationTimingData GetAdditionalTimingData()
        {
            var category = $"MI_{this.VendorConfig.VendorType:G}_{(this.OrderInfo.IsQuoteRequest ? "Quote" : "Policy")}";
            var message = string.Join(
                Environment.NewLine,
                $"IsQuoteRequest: {this.OrderInfo.IsQuoteRequest}",
                $"VendorType: {this.VendorConfig.VendorType:G}",
                $"URL: {this.ExportPath.ToString()}");

            return new IntegrationHttpCommunicationTimingData()
            {
                LogCategory = category,
                LogMessage = message
            };
        }

        /// <summary>
        /// Gets the endpoint path of the communication.
        /// </summary>
        /// <returns>The endpoint path.</returns>
        public LqbAbsoluteUri GetEndpointPath()
        {
            return this.ExportPath;
        }

        /// <summary>
        /// Gets the request in a loggable form.
        /// </summary>
        /// <returns>The loggable request.</returns>
        public string GetRequestForLogging()
        {
            return MIUtil.FormatRequestForLogging(this.OrderInfo.SerializeRequest());
        }

        /// <summary>
        /// Handles and processes a web exception thrown during communication.
        /// </summary>
        /// <param name="exception">The exception to process.</param>
        /// <returns>Whether to swallow the exception and return it in a <see cref="Result{TValue}"/> object.</returns>
        public bool HandleWebException(WebException exception)
        {
            return true;
        }

        /// <summary>
        /// Masks the response body string for logging.
        /// </summary>
        /// <param name="responseBody">The response data to mask.</param>
        /// <returns>The masked response string.</returns>
        public string MaskResponseString(string responseBody)
        {
            if (responseBody == string.Empty)
            {
                return "The vendor response was empty.";
            }

            return MIResponseProvider.MaskSensitiveResponseData(responseBody);
        }

        /// <summary>
        /// Parses the response body using <see cref="SerializationHelper"/>.
        /// </summary>
        /// <param name="response">A response to parse.</param>
        /// <returns>The response from the vendor, or a failure response if parsing failed.</returns>
        public Result<MIResponseProvider> ParseResponse(HttpRequestOptions response)
        {
            try
            {
                var responseGroup = (ResponseGroup)SerializationHelper.XmlDeserialize(response.ResponseBody, typeof(ResponseGroup));
                var responseProvider = new MIResponseProvider(responseGroup, MIResponseDataSource.Vendor);
                if (!this.IsPricingRequest &&
                    (!responseProvider.HasError || responseProvider.HasFatalError || (responseProvider.EncodedPDF.Any() && AutoSaveIsEnabledForDocType(responseProvider.IsQuoteResponse, this.OrderInfo.BrokerID))))
                {
                    // Generally, this kind of thing would be done better outside this method, 
                    // but in this case it uses the response body, which isn't available outside.
                    this.OrderInfo.StoreResponse(this.MaskResponseString(response.ResponseBody));
                }

                return Result<MIResponseProvider>.Success(new MIResponseProvider(responseGroup, MIResponseDataSource.Vendor));
            }
            catch (InvalidOperationException exc)
            {
                return Result<MIResponseProvider>.Failure(new ServerException(ErrorMessage.SystemError, exc));
            }
        }
    }
}
