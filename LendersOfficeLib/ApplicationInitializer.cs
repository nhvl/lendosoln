﻿namespace LendersOffice
{
    using Drivers.Base64Encoding;
    using Drivers.Configuration;
    using Drivers.ConnectionStringDecryption;
    using Drivers.ConversationLog;
    using Drivers.DataEncryption;
    using Drivers.Emailer;
    using Drivers.Encryption;
    using Drivers.Environment;
    using Drivers.FileDB;
    using Drivers.FileSystem;
    using Drivers.HttpRequest;
    using Drivers.Integrations;
    using Drivers.Json;
    using Drivers.Logger;
    using Drivers.MessageQueue;
    using Drivers.MethodInvoke;
    using Drivers.NetFramework;
    using Drivers.OpenXmlDocument;
    using Drivers.Security;
    using Drivers.SecurityEventLogging;
    using Drivers.Sms;
    using Drivers.SqlServerDB;
    using LqbGrammar;
    using LqbGrammar.Drivers;
    using LqbGrammar.Drivers.ConversationLog;
    using LqbGrammar.Drivers.Emailer;
    using LqbGrammar.Drivers.FileSystem;
    using LqbGrammar.Drivers.HttpRequest;
    using LqbGrammar.Drivers.SecurityEventLogging;
    using LqbGrammar.Queries;
    using Queries.Configuration;
    using RatePrice;

    /// <summary>
    /// This is a utility class to make it easier for applications to register
    /// all the standard command, query and driver interfaces with this one call.
    /// Since the factories have
    /// public access, applications are free to just register them themselves.
    /// An alternative is to use this to get all the standard factories and
    /// then overwrite any that the application desires to substitute with its
    /// own implementations.
    /// </summary>
    public static class ApplicationInitializer
    {
        /// <summary>
        /// Register command, query and driver factories here.
        /// </summary>
        /// <param name="appName">Name of the calling application.</param>
        /// <param name="initializer">Application initializer.</param>
        public static void RegisterFactories(string appName, IApplicationInitialize initializer)
        {
            initializer.Register<IApplicationSettingsDriverFactory>(new ApplicationSettingsDriverFactory());
            initializer.Register<IConfigurationQueryFactory>(new ConfigurationQueryFactory());
            initializer.Register<IEmailDriverFactory>(new EmailDriverFactory());
            initializer.Register<IReceiveEmailDriverFactory>(new EmailReceiverDriverFactory());
            initializer.Register<IRegularExpressionDriverFactory>(new RegularExpressionDriverFactory());
            initializer.Register<ISqlDriverFactory>(new SqlServerDriverFactory());
            initializer.Register<IStoredProcedureDriverFactory>(new StoredProcedureDriverFactory());
            initializer.Register<IJsonDriverFactory>(new JsonDriverFactory());
            initializer.Register<IMessageQueueDriverFactory>(new MessageQueueDriverFactory());
            initializer.Register<IFileDbDriverFactory>(new FlexibleFileStorageFactory());
            initializer.Register<IPriceEngineStorageFactory>(new PriceEngineStorageFactory());
            initializer.Register<ICommentLogDriverFactory>(new ConversationLogDriverFactory());
            initializer.Register<IHttpRequestDriverFactory>(new HttpRequestDriverFactory());
            initializer.Register<IFileSystemDriverFactory>(new FileSystemDriverFactory());
            initializer.Register<ILoggingDriverFactory>(new CompoundLoggingDriverFactory());
            initializer.Register<IEncryptionDriverFactory>(new EncryptionDriverFactory());
            initializer.Register<ISmsDriverFactory>(new LoggingBananaSmsDriverFactory());
            initializer.Register<IMethodInvokeDriverFactory>(new MethodInvokeDriverFactory());
            initializer.Register<ISecurityPrincipalDriverFactory>(new SecurityPrincipalDriverFactory());
            initializer.Register<IEnvironmentDriverFactory>(new EnvironmentDriverFactory());
            initializer.Register<IConnectionStringDecryptionDriverFactory>(new ConnectionStringDecryptionDriverFactory());
            initializer.Register<IBase64EncodingDriverFactory>(new Base64EncodingDriverFactory());
            initializer.Register<IDataEncryptionDriverFactory>(new DataEncryptionDriverFactory());
            initializer.Register<IEncryptionKeyDriverFactory>(new CachingKeyDriverDecoratorFactory());
            initializer.Register<IOpenXmlDocumentDriverFactory>(new OpenXmlDocumentDriverFactory());
            initializer.Register<ISecurityEventLogDriverFactory>(new SecurityEventLogDriverFactory());
            initializer.Register<Logging.ILoggerFactory>(new Logging.LoggerManager.MSMQFactory());
            initializer.Register<IIntegrationDriverFactory<Integration.MortgageInsurance.MIResponseProvider>>(new IntegrationHttpDriverFactory<Integration.MortgageInsurance.MIResponseProvider>());
        }
    }
}
