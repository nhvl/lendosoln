using System;
using System.Text;
using System.Reflection;
using System.Collections;
using CommonLib;

namespace LendersOffice.Debugging
{    
    public sealed class DataInput
    {
        public string UserGuidStr
        {
            get { return m_userGuid; }
        }

        public string LoanGuidStr
        {
            get { return m_loanGuid; }
        }

        internal string CommandLine
        {
            get { return m_cmdLine; }
        }

        public string[] Args
        {
            get { return m_args; }
        }

        internal System.Web.UI.Page  Page
        {
            get { return m_page; }
        }

        static private char[] s_delimiters = new char[] {' ', '\t', '\n'};

        public DataInput(string userGuidStr, string loanGuidStr, string cmdLine, System.Web.UI.Page  page)
        {
            m_userGuid = userGuidStr;
            m_loanGuid = loanGuidStr;
            m_page     = page;

            m_cmdLine  = cmdLine == null ? "" : cmdLine;
            if( m_cmdLine == "")
                return;

            string[] tokens = m_cmdLine.Split(s_delimiters);
            int count = 0;
            for( int i=0; i<tokens.Length; i++)
                if( tokens[i] != null && tokens[i] != "")
                    tokens[count++] = tokens[i];
            if( count > 0 )
            {
                string[] values = new string[count];
                System.Array.Copy(tokens, values, count);
                m_args = values;
            }


        }

        string              m_userGuid = "";   // Guid string
        string              m_loanGuid = "";   // Guid string
        string              m_cmdLine  = "";
        string[]            m_args   = EmptyArgs;
        System.Web.UI.Page  m_page;

        static string[] EmptyArgs = new string[0];
    }

    [AttributeUsage(AttributeTargets.Method, AllowMultiple=false)]
    public sealed class DynamicCallAttribute : Attribute
    {
    }

    public class DynamicCallUtility
    {
        static string s_DynamicCallAttribString = typeof(DynamicCallAttribute).FullName;

        //"LendersOffice.Common.DynamicCallAttribute";
        // list all static method such that 
        //  1. it has no argument. 
        //  2. One of its attribute is "MeridianLink.Debug.DynamicCallAttribute"
        static public MethodInfo[] GetDynamicCallMethods( Type type )
        {
            string specAttribute = s_DynamicCallAttribString;

            MethodInfo[] methods = type.GetMethods( BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static );
            ArrayList result = new ArrayList();
            foreach( MethodInfo method in methods )
            {
                ParameterInfo[] paramsInfo = method.GetParameters();

                if( paramsInfo.Length == 1 && 
                    paramsInfo[0].ParameterType == typeof(DataInput) &&
                    ReflectUtility.HasAttribute( method, specAttribute, false ) )
                {
                    result.Add( method );
                }

                string typeName = "";
                if( paramsInfo.Length == 1 )
                {
                    Type type1 = paramsInfo[0].ParameterType;
                    typeName = type1.Name;
                }

            }
            return (MethodInfo[]) result.ToArray( typeof(MethodInfo) );
        }
    }

}

// will move this part to CommonLib
namespace CommonLib
{    
    public interface IDisplay
    {
        Type GetDisplayType();
        string Display( object obj, string indent);
    }

    public class ReflectUtility
    {
        static Hashtable m_DisplayTypes = new Hashtable();

        static public void AddDisplayInterfaces( IDisplay [] arrDisplay)
        {
            Hashtable tmp = new Hashtable(m_DisplayTypes);
            foreach(IDisplay iDisplay in arrDisplay)
                tmp.Add(iDisplay.GetDisplayType(), iDisplay);
            m_DisplayTypes = tmp; 
        }

        static public bool HasAttribute( MemberInfo member, string attrName, bool inherit )
        {
            object[] attributes = member.GetCustomAttributes( inherit );
            foreach( Attribute attribute in attributes )
                if ( attribute.GetType().FullName == attrName )
                    return true;
            return false;
        }

        static public String DumpProperties(Type type, string[] propertyFilter)
        {
            return DumpProperties(type, null, propertyFilter);
        }


        static public String DumpProperties(Object obj, string[] propertyFilter)
        {
            if( obj == null )
                return "null";

            return DumpProperties(obj.GetType(), obj, propertyFilter);
        }

        static public String DumpProperties(Type type)
        {
            return DumpProperties(type, null, null);
        }


        static public String DumpProperties(Object obj)
        {
            if( obj == null )
                return "null";

            return DumpProperties(obj.GetType(), obj, null);
        }

        static private String DumpProperties(Type type, Object obj, string[] propertyFilter)
        {
            StringBuilder sb = new StringBuilder();

            System.Reflection.PropertyInfo [] properties = type.GetProperties(obj != null 
                ? BindingFlags.Instance | BindingFlags.Public
                : BindingFlags.Static   | BindingFlags.Public );
            bool isFirst = true;
            foreach (System.Reflection.PropertyInfo property in properties)
            {                
                if( propertyFilter != null && Array.IndexOf(propertyFilter, property.Name) < 0 )
                    continue;
                
                if( property.CanRead == false )
                    continue;                

                Type prop_type = property.PropertyType;
               
                try
                {
                    string val = "";

                    
                    if( property.GetIndexParameters().Length >= 1 )
                        val = "";
                    else if( m_DisplayTypes.ContainsKey(prop_type) && obj != null)
                    {
                        IDisplay iDisplay = (IDisplay)m_DisplayTypes[ prop_type ];
                        object obj_val = property.GetValue(obj, null);
                        val = "\n" + iDisplay.Display(obj_val, "    ");
                    }
                    else if( (prop_type == typeof(string[])) )      
                    {
                        object obj_val = property.GetValue(obj, null);
                        if( obj_val != null )
                            val  = String.Join(",", (string[])obj_val );
                        else
                            val = "null";
                    }
                    else if(  prop_type == typeof(System.Security.Principal.IPrincipal ) )
                    {
                        object obj_val = property.GetValue(obj, null);
                        val = DumpProperties(obj_val);

                        string []strs = val.Split( new char[] {'\n'} );
                        val = "\n       " + String.Join("\n       ", strs);    
                    }
                    else if( (prop_type == typeof(int))       || 
                        (prop_type == typeof(string))   || 
                        (prop_type == typeof(bool))     || 
                        (prop_type == typeof(float))    || 
                        (prop_type == typeof(double))   || 
                        (prop_type == typeof(UInt32))   || 
                        (prop_type == typeof(UInt64))   || 
                        (prop_type == typeof(UInt32))   || 
                        (prop_type == typeof(UInt16))   || 
                        (prop_type == typeof(char))     || 
                        (prop_type == typeof(Guid))     || 
                        (prop_type == typeof(Decimal))     || 
                        prop_type.IsSubclassOf(typeof(Enum))     ||                     
                        (prop_type == typeof(System.DateTime))
                        )
                    {
                        object obj_val = property.GetValue(obj, null);
                        if (obj_val == null)
                        {
                            val = "null";
                        }
                        else if( prop_type == typeof(string) )
                        {
                            val = string.Format( "\"{0}\"",obj_val.ToString() );
                        }
                        else
                            val = obj_val.ToString();
                    }
                    if( isFirst == false)
                        sb.Append("\n");
                    else
                        isFirst = false;

                    sb.AppendFormat("  {0,-18} = {1}", property.Name, val);
                }
                catch( Exception exc )
                {
                    if( prop_type != null )
                        sb.AppendFormat("\n {0,-18} = Error at here. The exception : {1}", property.Name, exc.Message);

                }

            }
            return sb.ToString();
        }

        static public void GetPropertyList( Type type, ArrayList readonlyList, ArrayList readWriteList)
        {
            readonlyList.Clear();
            readWriteList.Clear();

            System.Reflection.PropertyInfo [] properties = type.GetProperties();

            foreach (System.Reflection.PropertyInfo property in properties)
            {                
                if( property.CanRead == false )
                    continue;
                
                if( property.CanWrite == true )
                    readWriteList.Add(property.Name);
                else
                    readonlyList.Add(property.Name);
            }
        }

        static public string DumpTypes( Assembly assembly, bool onlyExportType )
        {
            StringBuilder sb = new StringBuilder();

            Type[] list = onlyExportType ? assembly.GetExportedTypes() : assembly.GetTypes();
            foreach (Type type in list) 
                sb.Append( type.FullName ).Append("\n");
            return sb.ToString();
        }

    }
}