namespace LendersOffice.BrokerAdmin
{
    using System;
    using System.Collections;
    using System.Collections.Specialized;
    using System.Xml;
    using System.IO;

    using DataAccess;
    using LendersOffice.Security;
    using LendersOffice.Admin;
    using System.Web.UI.WebControls;
    using System.Data;
    using System.Text;
    using System.Threading;

    using System.Data.Common;
    using System.Data.SqlClient;

    /// <summary>
    /// Holds a Set of brokers and their ddl xml configuration. The table is populated at first call. 
    /// </summary>
    public class BrokerOptionConfiguration 
	{	
		private Hashtable m_brokerOptions = new Hashtable();


		public static readonly BrokerOptionConfiguration Instance = new BrokerOptionConfiguration(); 


		public ArrayList GetOptionsFor( string FieldId, Guid BrokerId ) 
		{
			ArrayList list = new ArrayList();
			DDLOptions t = m_brokerOptions[BrokerId.ToString()] as DDLOptions;
			
			if ( t != null ) 
			{
				list = t.GetHiddenFieldsFor( FieldId );
			}
			return list;
		}


		private BrokerOptionConfiguration()
		{
            foreach (DbConnectionInfo connInfo in DbConnectionInfo.ListAll())
            {
                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(connInfo, "GetDDLXMLConfigBrokers", null))
                {
                    while (reader.Read())
                    {
                        try
                        {
                            this.m_brokerOptions.Add(reader["BrokerId"].ToString(), new DDLOptions(new Guid(reader["BrokerId"].ToString()), reader["DdlXml"].ToString()));

                        }
                        catch (Exception e)
                        {
                            Tools.LogError(String.Format("DDL settings for broker {0} were not loaded. The most likely cause is an error in the xml for this broker. XML : {1}", reader["BrokerId"].ToString(), reader["DdlXml"].ToString()), e);
                        }
                    }
                }
            }
		}

	}
		
	
	
	/// <summary>
	/// Configures Supported drop down list based on xml stored in the broker table. 
	/// Supports anything that uses the tool bind method 
	/// </summary>
	public class DDLOptions
	{

		
		private HybridDictionary m_FieldDictionary; //stores the hidden field options
		private XmlDocument m_doc;  //points to the current version of the xml configuration
		private Guid m_brokerId; //the id of the current broker.
		private bool m_XmlIsNotEmpty; 
		
		public bool HasSettings 
		{
			get{ return m_XmlIsNotEmpty; }
		}

		/// <summary>
		/// Creates a new instance of the options class and initializes the broker defined defaults.
		/// </summary>
		/// <param name="BrokerId">The Guid of the current broker.</param>
		public DDLOptions(Guid BrokerId, string Xml)
		{
			m_FieldDictionary = new HybridDictionary();
	
			m_brokerId= BrokerId;

			UpdateFromStringXML(Xml);
		}	

		/// <summary>
		/// Takes the given string as xml and uses it to update the default broker fields. 
		/// </summary>
		/// <param name="XML"></param>
		private void UpdateFromStringXML(string XML) 
		{
			m_XmlIsNotEmpty =  XML != null && XML.Length != 0; 
			if ( m_XmlIsNotEmpty ) 
			{
				
				m_doc = new XmlDocument();
				m_doc.Load(new MemoryStream( System.Text.Encoding.Unicode.GetBytes( XML ) ));
				UpdateBrokerFields( );
			}
		}


		private void UpdateBrokerFields() 
		{

			XmlNodeList nodes = m_doc.SelectNodes( "fields/field" ); 
			ArrayList hiddenNodes; 

			foreach ( XmlElement parent in nodes ) //fields 
			{
				hiddenNodes = new ArrayList();
				m_FieldDictionary[parent.GetAttribute("Name")] = hiddenNodes; //name is required
 
				if ( parent.GetAttribute("NameCount") != ""  )  //see if there are more fields associated with thi page
				{
					int count = System.Convert.ToInt32( parent.GetAttribute("NameCount"));  //get the total number of names 
					for ( int x = 0; x < count; x++ ) //iterate through each one setting their entry to the shared arraylist.
					{ 
						m_FieldDictionary[parent.GetAttribute("Name"+x.ToString())] = hiddenNodes; 
					}
				}
					
				foreach ( XmlElement child in parent.ChildNodes ) //process the children.
				{
					if ( child.HasAttribute("Value") )
						hiddenNodes.Add( child.GetAttribute("Value"));
				}

		
			}
		}


		/// <summary>
		/// Returns the hidden options for the given FieldId. 
		/// </summary>
		/// <param name="FieldId">The name (Id) of the dropdown list.</param>
		/// <returns> An arraylist containing the string representation of the option number that should be blocked.</returns>
		public ArrayList GetHiddenFieldsFor(string FieldId )
		{
		
			ArrayList tempList = new ArrayList();
			if ( m_FieldDictionary.Contains( FieldId ) ) 
			{	
				tempList = (ArrayList) m_FieldDictionary[FieldId]; 
			}

			return tempList; 
		
		}
	}
}
