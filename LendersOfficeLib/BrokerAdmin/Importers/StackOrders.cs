﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Common.TextImport;
using EDocs;


namespace LendersOffice.BrokerAdmin.Importers
{

    public class StackOrderImporter : AbstractImporter
    {
        //Configurable number of columns in case we add more later.
        private const int numColumns = 3;

        //And the first reqColumns are required - errors will be thrown if they're empty.
        private const int reqColumns = 1;

        private const string _headerText = @"DocMagicId, Folder, DocMagicName";
        private const string _sheetName = "Import Stack Order";

        String stackOrder = "";

        public StackOrderImporter()
        {
            HeaderText = _headerText;
            SheetName = _sheetName;
        }

        public String getStackOrder(BrokerDB broker)
        {
            StringBuilder sb = new StringBuilder("The following Stack Order was saved: \n");
            foreach (DocType doc in EDocumentDocType.GetStackOrderedDocTypesByBroker(broker))
            {
                sb.AppendLine(doc.DocTypeId + ", " +doc.Folder.FolderNm + ", " + doc.DocTypeName);
            }
            return sb.ToString();
        }

        public override void Import(string csv, Guid BrokerId, out String Results, out DataTable ErrorList)
        {
            ErrorList = new DataTable();
            Results = "";
            stackOrder = "";
            BrokerDB m_brokerDB = BrokerDB.RetrieveById(BrokerId);
            m_brokerDB.SetStackOrder("");
            try
            {
                bool parseSuccess = ParseCSV(csv, BrokerId, out ErrorList);

                if (parseSuccess)
                {
                    try
                    {
                        //We parsed everything, now we go ahead and check for unaccounted doc types
                        m_brokerDB = BrokerDB.RetrieveById(BrokerId);
                        IEnumerable<DocType> brokerOrderedDocTypes = EDocumentDocType.GetStackOrderedDocTypesByBroker(m_brokerDB);
                        int[] orderedIDs = stackOrder.Split(',').Select((s) => Int32.Parse(s)).ToArray();
                        foreach (DocType docType in brokerOrderedDocTypes)
                        {
                            if (!orderedIDs.Contains(Int32.Parse(docType.DocTypeId)))
                            {
                                stackOrder += "," + docType.DocTypeId;
                            }
                        }
                    
                        m_brokerDB.SetStackOrder(stackOrder);
                        m_brokerDB.Save();
                        Results = getStackOrder(m_brokerDB);
                    }
                    catch (FormatException e)
                    {
                        Results = "No stack order was saved.";
                        ErrorList.Rows.Add(e.Message + " Most likely an ID has a "
                            + "non-numerical character in it.  (Hint:  If the header isn't exactly equal to"
                            + "\"DocMagicId, Folder, DocMagicName\" or \"DocMagicId\", it will treat the header as data.)");
                    }

                    
                }
                else
                {
                    Results += "No Stack Order was saved.";
                }
            }
            catch (CBaseException e)
            {
                Tools.LogError("Failed to import stack order: " + e.Message + ".", e);
                throw;
            }
        }

        /// <summary>
        /// Parse the (hopefully) CSV file in m_Companies.Text
        /// </summary>
        /// <param name="toSave">A list of PML Brokers that need saving</param>
        /// <param name="errorList">Errors that occurred during parsing</param>
        /// <returns>True if parsing was successful, false if not</returns>
        private bool ParseCSV(string csv, Guid BrokerId, out DataTable errorList)
        {

            errorList = new DataTable();

            bool errord = false;
            errorList.Columns.Add(new DataColumn("Errors", typeof(string)));

            ILookup<int, string> parseErrors;
            DataTable data = DelimitedListParser.CsvToDataTable(csv, true, reqColumns, numColumns, out parseErrors);

            //We skip the first line in parsing to make room for a header row, so start at 1            
            int line = 1;
            if (parseErrors.Contains(line))
            {
                foreach (string err in parseErrors[line])
                {
                    errord = true;
                    errorList.Rows.Add("Error on line " + line + ": " + err);
                }
            }

            for (int i = 0; i < data.Rows.Count; i++)
            {
                DataRow row = data.Rows[i];
                line++;
                foreach (string err in parseErrors[line])
                {
                    errord = true;
                    errorList.Rows.Add("Error on line " + line + ": " + err);
                }


                stackOrder += row[0] + ",";
                
                //And then save it later 
                //(after all, we don' want to save anything 
                //if it turns out the CSV file is wrong)
                //toSave.AddLast(pb);
            }
            if (stackOrder == "")
            {
                errord = true;
                errorList.Rows.Add("No data was input.");
            }
            else
                stackOrder = stackOrder.Substring(0, stackOrder.Length - 1);
            //We want to return whether or not parsing was successful, so were there no errors?
            return !errord;
        }
    }
}
