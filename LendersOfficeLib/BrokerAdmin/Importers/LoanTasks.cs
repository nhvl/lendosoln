﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using DataAccess;
using LendersOffice.Common.TextImport;
using LendersOffice.ObjLib.Task;
using LendersOffice.Common;

namespace LendersOffice.BrokerAdmin.Importers
{

    public class LoanTaskImporter : AbstractImporter
    {
        /// <summary>
        /// Used for making a template of a task, since there may be duplicates and we don't want to create
        /// an entire Task object just to throw it away.
        /// </summary>
        private struct TaskTemplate : IEquatable<TaskTemplate>
        {
            public string Subject;
            public ConditionCategory Category;
            public int PermissionLevel;
            public Guid ToBeAssigned;
            public Guid ToBeOwned;
            public string DueDateField;
            public int DueDateCalc;
            public Guid LoanTemplateID;

            //We implement these two methods to ensure that the HashSet will work.
            bool IEquatable<TaskTemplate>.Equals(TaskTemplate other)
            {
                return this.Subject.Equals(other.Subject) &&
                       this.Category.Equals(other.Category) &&
                       this.PermissionLevel.Equals(other.PermissionLevel) &&
                       this.ToBeAssigned.Equals(other.ToBeAssigned) &&
                       this.ToBeOwned.Equals(other.ToBeOwned) &&
                       this.DueDateField.Equals(other.DueDateField) &&
                       this.DueDateCalc.Equals(other.DueDateCalc) &&
                       this.LoanTemplateID.Equals(other.LoanTemplateID);
            }

            public override int GetHashCode()
            {
                unchecked
                {
                    //17 and 31 are generally useful prime numbers for this sort of thing.
                    int hash = 17;
                    //The category is the only thing that might be null, since the rest are value types
                    if (Category != null)
                    {
                        hash = hash * 31 + Category.GetHashCode();
                    }
                    hash = hash * 31 + Subject.GetHashCode();
                    hash = hash * 31 + PermissionLevel.GetHashCode();
                    hash = hash * 31 + ToBeAssigned.GetHashCode();
                    hash = hash * 31 + ToBeOwned.GetHashCode();
                    hash = hash * 31 + ToBeOwned.GetHashCode();
                    hash = hash * 31 + DueDateField.GetHashCode();
                    hash = hash * 31 + DueDateCalc.GetHashCode();
                    hash = hash * 31 + LoanTemplateID.GetHashCode();
                    return hash;
                }
            }

            public void Save(CStoredProcedureExec sproc, Guid BrokerGuid)
            {
                SqlParameter TaskId = new SqlParameter("@TaskId", SqlDbType.VarChar, 10);
                TaskId.Direction = ParameterDirection.Output;
                SqlParameter BorrowerNmCached = new SqlParameter("@BorrowerNmCached", SqlDbType.VarChar, 60);
                BorrowerNmCached.Direction = ParameterDirection.Output;
                SqlParameter LoanNumCached = new SqlParameter("@LoanNumCached", SqlDbType.VarChar, 36);
                LoanNumCached.Direction = ParameterDirection.Output;

                SqlParameter[] parameters = {
                new SqlParameter("@BrokerId", BrokerGuid),
                //No history yet
                new SqlParameter("@TaskHistoryXml", "<TaskHistory/>"),

                //These guys are output params we don't care about, but still need to pass in
                TaskId,
                BorrowerNmCached,
                LoanNumCached,

                //And these must be non-null, but being empty doesn't seem to hurt anything
                new SqlParameter("@TaskAssignedUserId", Guid.Empty),
                new SqlParameter("@TaskOwnerUserId", Guid.Empty),
                new SqlParameter("@TaskCreatedByUserId", Guid.Empty),
                
                //Finally, here's our actual values
                new SqlParameter("@LoanId", this.LoanTemplateID),
                new SqlParameter("@TaskSubject", this.Subject),
                new SqlParameter("@TaskIsCondition", !(this.Category == null)),
                new SqlParameter("@CondCategoryId", this.Category == null ? null : (int?)this.Category.Id),
                new SqlParameter("@TaskPermissionLevelId", this.PermissionLevel),
                new SqlParameter("@TaskToBeAssignedRoleId", this.ToBeAssigned),
                new SqlParameter("@TaskToBeOwnerRoleId", this.ToBeOwned),
                new SqlParameter("@TaskDueDateCalcField", this.DueDateField),
                new SqlParameter("@TaskDueDateCalcDays", this.DueDateCalc),
                };

                sproc.ExecuteNonQuery("TASK_CreateTask", 3, parameters);
            }

        }

        private const string _headerText = @"Subject, Condition Category (leave blank if not a condition), Task Permission Level, To Be Assigned, To Own (review & close), Due Date (field), Due Date Calc (biz days +/-), Loan Template Name (include list delimited by ';')";
        private const string _sheetName = "Import Loan Tasks";

        private readonly string[] columnNames = (from header in _headerText.Split(',') select header.TrimWhitespaceAndBOM().Replace("\"", "")).ToArray();
        private readonly string[] optionalColumnNames = { "Condition Category (leave blank if not a condition)", "Due Date Calc" };

        public LoanTaskImporter()
        {
            HeaderText = _headerText;
            SheetName = _sheetName;
        }


        private string GetSavedTasks(IEnumerable<TaskTemplate> saved, Guid BrokerId)
        {
            if (saved.Count() == 0)
            {
                return "No conditions were saved\n\n";
            }


            //Grab this guy again, so we can give the user nice loan template names.
            DataSet temp = new DataSet();
            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", BrokerId)
                                            , new SqlParameter("@BranchId", Guid.Empty)
                                            , new SqlParameter("@EmployeeId", Guid.Empty)
                                            , new SqlParameter("@FilterByBranch", false)
                                        };

            DataSetHelper.Fill(temp, BrokerId, "RetrieveLoanTemplateByBrokerID",  parameters);

            var templateNames = (from DataRow row in temp.Tables[0].Rows
                                 select new
                                 {
                                     Name = row["sLNm"].ToString().TrimWhitespaceAndBOM(),
                                     Id = new Guid(row["sLId"].ToString())
                                 }).ToDictionary((a) => a.Id, (a) => a.Name);

            StringBuilder sb = new StringBuilder("The following tasks and conditions were successfully saved:\n");
            foreach (TaskTemplate task in saved)
            {
                sb.AppendLine(task.Subject + " in template " + templateNames[task.LoanTemplateID]);
            }

            return sb.ToString();
        }

        private string GetSavedCategories(IEnumerable<ConditionCategory> saved)
        {
            if (saved.Count() == 0)
            {
                return "No new categories were saved\n\n";
            }


            StringBuilder sb = new StringBuilder("The following condition categories were successfully saved:\n");
            foreach (ConditionCategory cat in saved)
            {
                sb.AppendLine(cat.Category);
            }

            return sb.ToString();
        }

        /// <summary>
        /// Import a CSV string of loan tasks for BrokerId
        /// </summary>
        /// <param name="csv"></param>
        /// <param name="BrokerId"></param>
        /// <param name="ErrorList"></param>
        /// <returns></returns>
        public override void Import(string csv, Guid BrokerId, out string Results, out DataTable ErrorList)
        {
            ErrorList = new DataTable();
            Results = "No loan tasks were imported";
            try
            {
                IEnumerable<TaskTemplate> newTasks;
                IEnumerable<ConditionCategory> newCategories;

                bool parseSuccess = ParseCSV(csv, BrokerId, out newTasks, out newCategories, out ErrorList);

                if (parseSuccess)
                {
                    //We parsed everything, so go ahead and save it all

                    //Categories need to be saved first so they can have IDs when we go to save 
                    foreach (ConditionCategory category in newCategories)
                    {
                        try
                        {
                            category.Save();
                        }
                        catch (SqlException e)
                        {
                            ErrorList.Rows.Add("Could not save Condition Category: " + category.Category + "! This is probably a db problem. All previous Condition Categories were saved, but no further imports will be made.");
                            ErrorList.Rows.Add("Error text: " + e.ToString());
                            Results = GetSavedCategories(newCategories.TakeWhile((a) => a.Id != category.Id));
                            throw;
                        }
                        catch (Exception e)
                        {
                            ErrorList.Rows.Add("Could not save Condition Category: " + category.Category + "! This is a completely unexpected error! All previous Condition Categories were saved, but no further imports will be made.");
                            ErrorList.Rows.Add("Error text: " + e.ToString());
                            Results = GetSavedCategories(newCategories.TakeWhile((a) => a.Id != category.Id));
                            throw;
                        }
                    }

                    Results = GetSavedCategories(newCategories);

                    using (CStoredProcedureExec sproc = new CStoredProcedureExec(BrokerId))
                    {
                        sproc.BeginTransactionForWrite();
                        foreach (TaskTemplate task in newTasks)
                        {
                            try
                            {
                                task.Save(sproc, BrokerId);
                            }
                            catch (SqlException e)
                            {
                                ErrorList.Rows.Add("Could not save task: " + task.Subject + "! This is probably due to a db problem. No tasks were saved.");
                                ErrorList.Rows.Add("Error text: " + e.ToString());
                                sproc.RollbackTransaction();
                                throw;
                            }
                            catch (Exception e)
                            {
                                ErrorList.Rows.Add("Could not save task: " + task.Subject + "! This is a totally unexpected error. No tasks were saved.");
                                ErrorList.Rows.Add("Error text: " + e.ToString());
                                sproc.RollbackTransaction();
                                throw;
                            }
                        }
                        sproc.CommitTransaction();
                    }
                    //If we get here, there were no issues saving.
                    Results += GetSavedTasks(newTasks, BrokerId);
                }
            }
            catch (Exception e)
            {
                Tools.LogError("Failed to import tasks: " + e.Message + ".", e);
                throw;
            }
        }



        /// <summary>
        /// Parse the (hopefully) CSV file in the import box
        /// </summary>
        /// <param name="newTasks">A list of task templates that need to be saved</param>
        /// <param name="newCategories">A list of categories that need to be saved</param>
        /// <param name="errorList">Errors that occurred during parsing</param>
        /// <returns>True if parsing was successful, false if not</returns>
        //There's a LOAdmin.Task, so we have to fully qualify the LendingQB Task:(
        private bool ParseCSV(string csvText, Guid BrokerId, out IEnumerable<TaskTemplate> newTasks, out IEnumerable<ConditionCategory> newCategories, out DataTable errorList)
        {

            newTasks = new HashSet<TaskTemplate>();

            newCategories = new LinkedList<ConditionCategory>();
            errorList = new DataTable();

            errorList.Columns.Add(new DataColumn("Errors", typeof(string)));

            #region Load all our lookups
            var validRoles = Tools.GetRolesExcluding("Consumer", "Administrator", "Accountant").ToArray();

            var validCategories = ConditionCategory.GetCategories(BrokerId);

            var validFields = LendersOffice.ObjLib.Task.Task.ListValidDateFieldParameters();

            var validTaskPermissionLevels = LendersOffice.ObjLib.Task.PermissionLevel.RetrieveAll(BrokerId);

            //And a list of permission levels that are only valid for conditions
            var validConditionPermissionLevels = (from perm in LendersOffice.ObjLib.Task.PermissionLevel.RetrieveAll(BrokerId)
                                                  where perm.IsAppliesToConditions
                                                  select perm).ToList<PermissionLevel>();

            //And finally a list of valid loan template ids and names
            DataSet temp = new DataSet();
            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", BrokerId)
                                            //This doesn't really matter because we're not gonna filter by branch
                                                , new SqlParameter("@BranchId", Guid.Empty)
                                            //This doesn't matter because we only want templates from branch level or above
                                                , new SqlParameter("@EmployeeId", Guid.Empty)
                                                , new SqlParameter("@FilterByBranch", false)
                                        };

            DataSetHelper.Fill(temp, BrokerId, "RetrieveLoanTemplateByBrokerID", parameters);

            var validTemplates = (from DataRow row in temp.Tables[0].Rows
                                  select new
                                  {
                                      Name = row["sLNm"].ToString(),
                                      Id = new Guid(row["sLId"].ToString())
                                  }
                                        ).ToList();
            #endregion

            //ILookups are nice, pity nothing really implements them except ToLookup
            ILookup<int, string> parseErrors;

            //Finally, start the parsing
            DataTable data;
            using (StringReader sr = new StringReader(csvText))
            {
                string firstLine = sr.ReadLine();
                //Did they include our header row?
                if (firstLine.TrimWhitespaceAndBOM().Equals(_headerText, StringComparison.InvariantCultureIgnoreCase))
                {
                    //Yes, so just parse it
                    data = DelimitedListParser.DsvToDataTable(csvText, ',', true, out parseErrors);
                }
                else
                {
                    //They messed with it, so prepend a new one
                    data = DelimitedListParser.DsvToDataTable(_headerText + "\n" + csvText, ',', true, out parseErrors);
                }
            }

            //Validate the columns
            ILookup<int, string> validateErrors = DataParsing.ValidateColumns(data, columnNames,
                                                                      from s in columnNames  //Every column that's not optional is required
                                                                      where !optionalColumnNames.Contains(s)
                                                                      select s,
                                                                      1);

            //Don't dump the parse errors into the error list right now, though - we want all the errors to show up in line order.

            //However, since we don't touch the header row and that might have generated errors, add them now.
            int line = 1;

            foreach (string err in parseErrors[line])
            {
                errorList.Rows.Add("Error on line " + line + ": " + err);
            }
            foreach (string err in validateErrors[line])
            {
                errorList.Rows.Add("Error on line " + line + ": " + err);
            }

            for (int i = 0; i < data.Rows.Count; i++)
            {
                //We start on line 2 because there was a header row
                line++;

                //Did this line check out in parsing?
                if (parseErrors.Contains(line))
                {
                    foreach (string err in parseErrors[line])
                    {
                        errorList.Rows.Add("Error on line " + line + ": " + err);
                    }
                }
                if (validateErrors.Contains(line))
                {
                    foreach (string err in validateErrors[line])
                    {
                        errorList.Rows.Add("Error on line " + line + ": " + err);
                    }
                }
                //If the number of columns is wrong, we have to just skip this row.
                if (data.Rows[i].ItemArray.Length != columnNames.Count())
                {
                    continue;
                }

                //Make a nice little anonymous class, so we have well-typed entries instead of
                //referring to row[2] all the time.
                var entry = new
                {
                    Subject = data.Rows[i]["Subject"].ToString(),
                    PermissionLevelName = data.Rows[i]["Task Permission Level"].ToString(),
                    ToBeAssignedName = data.Rows[i]["To Be Assigned"].ToString(),
                    ToOwnName = data.Rows[i]["To Own (review & close)"].ToString(),
                    DueDateField = data.Rows[i]["Due Date (field)"].ToString(),
                    //Since the list of loan template names is literally just a list of elements separated by ; and nothing else, 
                    //we can actually use split.
                    LoanTemplateNames = from e in data.Rows[i][@"Loan Template Name (include list delimited by ';')"].ToString().Split(';') select e.TrimWhitespaceAndBOM(),
                    ConditionCategoryName = data.Rows[i]["Condition Category (leave blank if not a condition)"].ToString().TrimWhitespaceAndBOM(),
                    sDueDateCalc = data.Rows[i]["Due Date Calc (biz days +/-)"].ToString()
                };


                TaskTemplate t = new TaskTemplate();

                t.Subject = entry.Subject;
                //Check to see if it looks like there was truncation; 
                //Excel to DataTable via OleDB will cause this if: 
                //1. There exist entries which are > 255 characters long in the sheet
                //2. None of those entries are in the first 8 rows.
                if (entry.Subject.Length == 255)
                {
                    errorList.Rows.Add("Error on line " + line + ": " +
                                       "The task subject was exactly 255 characters long, which might indicate truncation." +
                                       "If it has been truncated, move it to the top of the import list and try again." +
                                       "If it hasn't been truncated, add a space or something so it isn't exactly 255 characters long.");
                }

                #region Figuring out the permission level
                var permissionLevel = DataParsing.GetByIdOrName(validTaskPermissionLevels, entry.PermissionLevelName, (a) => a.Name, (a) => a.Id);

                if (permissionLevel.Count() > 1)
                {
                    errorList.Rows.Add("Error on line " + line + ": " +
                             "The permission level name \"" + entry.PermissionLevelName + "\" is ambiguous. " +
                             "There are " + permissionLevel.Count() + " matching permission levels. " +
                             "This search ignores case, so try using the permission level ID. " +
                             "If that doesn't work, something is broken.");
                    t.PermissionLevel = -1;
                }

                else if (permissionLevel.Count() == 0)
                {
                    errorList.Rows.Add("Error on line " + line + ": " +
                                  "No permission level named \"" + entry.PermissionLevelName + "\" was found. " +
                                  "Try using the permission level ID. " +
                                  "If that doesn't work, something is broken.");
                    t.PermissionLevel = -1;
                }
                else
                {
                    t.PermissionLevel = permissionLevel.FirstOrDefault().Id;
                }
                #endregion

                #region Figuring out assigned and owner roles

                //We'll just use first here, since there should never be two roles with the same name

                t.ToBeAssigned = (from r in validRoles where r.Value == entry.ToBeAssignedName select r.Key).FirstOrDefault();

                if (t.ToBeAssigned == Guid.Empty)
                {
                    errorList.Rows.Add("Error on line " + line + ": " +
                                       "Role to be assigned by could not be parsed from \"" + entry.ToBeAssignedName + "\". " +
                                       "Try putting a space between the words?");

                }

                var ownerRole = (from r in validRoles where r.Value == entry.ToOwnName select r).FirstOrDefault();
                t.ToBeOwned = ownerRole.Key;

                //If there's an owner and the permission level exists, 
                //we need to check that the permissions actually allow the owner to own the task.
                if (t.ToBeOwned != Guid.Empty && t.PermissionLevel != -1)
                {
                    PermissionLevel current = PermissionLevel.Retrieve(BrokerId, t.PermissionLevel);

                    //We have to do a match by name because there doesn't seem to be a way to get a E_RoleT from a Role guid
                    var manageMatches = from role in current.GetRolesWithPermissionLevel(E_PermissionLevel.Manage)
                                        where role.GetFriendlyName().Equals(ownerRole.Value)
                                        select role;

                    if (!manageMatches.Any())
                    {
                        errorList.Rows.Add("Error on line " + line + ": " +
                                           "Permission level " + current.Name + " may not be owned by " + ownerRole.Value);
                    }

                }
                else if (t.ToBeOwned == Guid.Empty)
                {
                    //Of course if there isn't an owner we have to complain
                    errorList.Rows.Add("Error on line " + line + ": " +
                                       "Role to be owned by could not be parsed from \"" + entry.ToOwnName + "\". " +
                                       "Try putting a space between the words?");
                }
                //Otherwise there's an owner and the permission level may or may not exist, but we've complained about the latter already
                //if it doesn't.
                #endregion

                #region Figuring out the due date field id
                string fieldId;

                var dueDates = DataParsing.GetByIdOrName(validFields, entry.DueDateField, (a) => a.FriendlyName, (a) => a.Id);

                if (dueDates.Count() > 1)
                {
                    errorList.Rows.Add("Error on line " + line + ": \"" +
                                        entry.DueDateField + "\" is ambiguous. " +
                                        "There are " + dueDates.Count() + " matching fields. " +
                                        "Try using the field Id, and if that doesn't work it's broken.");
                    fieldId = "Ambiguous";
                }

                else if (dueDates.Count() == 0)
                {
                    errorList.Rows.Add("Error on line " + line + ": \"" +
                                        entry.DueDateField + "\" was not found as either a field id or a field name.");
                    fieldId = "None";
                }

                else
                {
                    fieldId = dueDates.First().Id;
                }

                t.DueDateField = fieldId;
                #endregion

                //Skip the loan template names for now, since it's a list and we want to make one task per each.

                #region Figuring out the category

                ConditionCategory cat;

                if (string.IsNullOrEmpty(entry.ConditionCategoryName))
                {
                    cat = null;
                }
                else if (entry.ConditionCategoryName.Equals("-1"))
                {
                    errorList.Rows.Add("Error on line " + line + ": -1 is not a valid category name or id for importing. Add it manually if you really want to do that.");
                    cat = null;
                }
                else
                {
                    var matchingCategories = DataParsing.GetByIdOrName(validCategories.Union(newCategories),
                                                            entry.ConditionCategoryName, (a) => a.Category, (a) => a.Id);
                    if (matchingCategories.Count() > 1)
                    {
                        cat = null;
                        errorList.Rows.Add("Error on line " + line + ": " +
                                           "Multiple matches for category \"" + entry.ConditionCategoryName + "\" found, " +
                                           "there are " + matchingCategories.Count() + " categories." +
                                           "Try using the category's id instead. If that doesn't work, something is broken.");
                    }
                    else if (matchingCategories.Count() == 0)
                    {
                        //This category doesn't exist, so we'll have to create it.
                        cat = new ConditionCategory(BrokerId);
                        cat.Category = entry.ConditionCategoryName;

                        //Give the category the same default permission level as the owning task
                        cat.DefaultTaskPermissionLevelId = t.PermissionLevel;

                        ((LinkedList<ConditionCategory>)newCategories).AddLast(cat);
                    }
                    else
                    {
                        cat = matchingCategories.FirstOrDefault();
                        //If we have a condition category, we have to double check that the permission level applies to categories.
                        //Note that categories we just made will have a default permission level of -1, so we can ignore them
                        if (cat.DefaultTaskPermissionLevelId != -1 && 
                            !validConditionPermissionLevels.Any((a) => a.Id == cat.DefaultTaskPermissionLevelId))
                        {
                            var level = PermissionLevel.Retrieve(BrokerId, cat.DefaultTaskPermissionLevelId);
                            {
                                errorList.Rows.Add("Error on line " + line + ": " +
                                                   "Condition category \"" + cat.Category + "\" has permission level \"" +
                                                   level.Name +
                                                   "\" which is not valid for categories.");
                            }
                        }
                        /* TODO: Ask Brian if this test is actually necessary
                        //Make sure that the category and the task have the same permission level
                        if (cat.DefaultTaskPermissionLevelId != t.PermissionLevel)
                        {
                            string assignedPermName = (from a in validTaskPermissionLevels where a.Id == t.PermissionLevel select a.Name).FirstOrDefault();
                            string categoryPermName = (from a in validTaskPermissionLevels where a.Id == cat.DefaultTaskPermissionLevelId select a.Name).FirstOrDefault();
                            errorList.Rows.Add("Error on line " + line + ": " + "Task subject \"" + t.Subject.Substring(0, 100) + "\" has permission level of " + assignedPermName +
                                               ", but it is being assigned to condition category \"" + cat.Category + "\" which has default permission level of " +
                                               categoryPermName + ". Make sure the permission levels match.");
                        }
                        */
                    }
                }

                t.Category = cat;
                #endregion

                #region Figuring out DueDateCalc
                int dueDateAdd;

                if (int.TryParse(entry.sDueDateCalc, out dueDateAdd))
                {
                    t.DueDateCalc = dueDateAdd;
                }
                else
                {
                    t.DueDateCalc = 0;
                }
                #endregion

                #region Applying loan template names
                foreach (string name in entry.LoanTemplateNames)
                {
                    var templateMatches = DataParsing.GetByIdOrName(validTemplates, name, (a) => a.Name, (a) => a.Id);

                    if (templateMatches.Count() > 1)
                    {
                        errorList.Rows.Add("Error on line " + line + ": " +
                                           "Template name " + name + " is ambiguous; there are " + templateMatches.Count() +
                                           " templates with that name. Try using the template id, and if that doesn't work something is broken.");
                        t.LoanTemplateID = Guid.Empty;
                    }
                    else if (templateMatches.Count() == 0)
                    {
                        errorList.Rows.Add("Error on line " + line + ": " +
                                           "Template " + name + " could not be found. Double check the spelling, and make sure it contains no semi-colons (';')");
                    }
                    else
                    {
                        t.LoanTemplateID = templateMatches.FirstOrDefault().Id;
                        //Since TaskTemplates are structs, this makes a copy of the current one and sticks it in the HashSet.                        
                        ((HashSet<TaskTemplate>)newTasks).Add(t);
                    }
                }
                #endregion
            }

            //We want to return true if parsing was successful, so were there at most 0 errors?
            return !(errorList.Rows.Count > 0);
        }
    }

}
