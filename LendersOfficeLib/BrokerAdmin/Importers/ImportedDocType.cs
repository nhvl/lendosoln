﻿// <summary>
//    Author: Michael Leinweaver
//    Date:   10/29/2015
// </summary>
namespace LendersOffice.BrokerAdmin.Importers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// A container class for an imported document type that
    /// includes the document type ID and name along with the
    /// containing folder ID and folder name.
    /// </summary>
    internal class ImportedDocType
    {
        /// <summary>
        /// Gets or sets the ID of the imported document type.
        /// </summary>
        /// <value>The ID of the imported document type.</value>
        public string DocTypeId { get; set; }

        /// <summary>
        /// Gets or sets the name of the imported document type.
        /// </summary>
        /// <value>The ID of the imported document type.</value>
        public string DocTypeName { get; set; }

        /// <summary>
        /// Gets or sets the ID of the containing folder for the 
        /// imported document type.
        /// </summary>
        /// <value>
        /// The ID of the containing folder for the imported document type.
        /// </value>
        public string FolderId { get; set; }

        /// <summary>
        /// Gets or sets the name of the containing folder for the 
        /// imported document type.
        /// </summary>
        /// <value>
        /// The name of the containing folder for the imported document type.
        /// </value>
        public string FolderName { get; set; }
    }
}
