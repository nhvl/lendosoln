﻿// <summary>
//    Author: Michael Leinweaver
//    Date:   10/29/2015
// </summary>
namespace LendersOffice.BrokerAdmin.Importers
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using DataAccess;
    using EDocs;
    using LendersOffice.Common;
    using LendersOffice.Common.TextImport;
    using LendersOffice.Security;

    /// <summary>
    /// Represents an importer that handles shipping template imports.
    /// </summary>
    public class ShippingTemplateImporter : AbstractImporter
    {
        /// <summary>
        /// The name of the Excel spreadsheet, as required by DataParsing.FileToCSV().
        /// </summary>
        private const string ExcelSheetName = "Import Shipping Templates";

        /// <summary>
        /// Determines whether a given document type matches a new document type
        /// that should be imported.
        /// </summary>
        private static readonly Func<ImportedDocType, string, string, bool> HasDuplicate =
            (importedDocType, folderId, docTypeId) =>
                importedDocType.FolderId.Equals(folderId, StringComparison.OrdinalIgnoreCase) &&
                importedDocType.DocTypeId.Equals(docTypeId, StringComparison.OrdinalIgnoreCase);

        /// <summary>
        /// Configurable number of columns in case we add more later.
        /// </summary>
        private readonly int totalNumberOfColumns;

        /// <summary>
        /// The number of required columns in the import file relative to the first column. 
        /// Errors will be thrown if they're empty.
        /// </summary>
        private readonly int requiredNumberOfColumns;

        /// <summary>
        /// Initializes a new instance of the <see cref="ShippingTemplateImporter"/> class.
        /// </summary>
        public ShippingTemplateImporter()
        {
            var columnNameArray = Enum.GetNames(typeof(ShippingTemplateImporterColumns));

            this.totalNumberOfColumns = columnNameArray.Length;

            // All columns are required for the import to be successful.
            this.requiredNumberOfColumns = columnNameArray.Length;

            this.HeaderText = string.Join(", ", columnNameArray);

            this.SheetName = ShippingTemplateImporter.ExcelSheetName;
        }

        /// <summary>
        /// The expected format of the CSV file we're parsing.
        /// </summary>
        private enum ShippingTemplateImporterColumns
        {
            /// <summary>
            /// The column corresponding to the shipping template name.
            /// </summary>
            ShippingTemplateName,

            /// <summary>
            /// The column corresponding to the name of the folder for 
            /// the document type to be placed.
            /// </summary>
            FolderName,

            /// <summary>
            /// The column corresponding to the document type. The order
            /// of the rows in the table determines stack order.
            /// </summary>
            DocType
        }

        /// <summary>
        /// Gets or sets the id of the broker currently having shipping templates
        /// imported.
        /// </summary>
        private Guid BrokerId { get; set; }

        /// <summary>
        /// Imports a set of shipping templates from a CSV file.
        /// </summary>
        /// <param name="csv">The CSV file content containing the templates to import.</param>
        /// <param name="brokerId">The ID of the importing broker.</param>
        /// <param name="results">The results of the import.</param>
        /// <param name="errorList">A table containing any parsing errors that occurred.</param>
        public override void Import(string csv, Guid brokerId, out string results, out DataTable errorList)
        {
            this.BrokerId = brokerId;

            errorList = new DataTable();

            errorList.Columns.Add(new DataColumn("Line", typeof(int)));

            errorList.Columns.Add(new DataColumn("Error Description", typeof(string)));

            // The ILookup returned from DelimitedListParser.CsvToDataTable() is not always
            // correctly sorted, so we'll be explicit to sort by line number.
            errorList.DefaultView.Sort = "Line asc";

            results = string.Empty;

            try
            {
                ILookup<int, string> parseErrors;

                var csvDataTable = DelimitedListParser.CsvToDataTable(csv, true, this.requiredNumberOfColumns, this.totalNumberOfColumns, out parseErrors);

                if (parseErrors.Any())
                {
                    this.RecordParseErrors(parseErrors, errorList);
                }

                var importedTemplates = this.ParseCSV(csvDataTable, errorList);

                if (errorList.Rows.Count > 0)
                {
                    // Display the file content again to the user so it's easy to see
                    // where the error(s) occurred.
                    results = csv;
                }
                else
                {
                    this.SaveImportedTemplates(importedTemplates);

                    results = this.GetNamesOfImportedTemplates(importedTemplates);
                }
            }
            catch (Exception e)
            {
                Tools.LogError("Failed to import shipping templates due to the following exception: " + e.Message, e);

                throw;
            }
        }

        /// <summary>
        /// Records a list of parse errors to the <paramref name="errorList"/> table.
        /// </summary>
        /// <param name="parseErrors">The list of parsing errors.</param>
        /// <param name="errorList">The DataTable to add error notifications to.</param>
        private void RecordParseErrors(ILookup<int, string> parseErrors, DataTable errorList)
        {
            foreach (var errorGrouping in parseErrors)
            {
                foreach (var errorDescription in errorGrouping.ToList())
                {
                    errorList.Rows.Add(errorGrouping.Key, errorDescription);
                }
            }
        }

        /// <summary>
        /// Parses a CSV file and returns the template/document type pairs that were parsed.
        /// </summary>
        /// <param name="csvDataTable">The data table containing the templates to import.</param>
        /// <param name="errorList">A data table to add any parsing errors to that will be displayed to the user.</param>
        /// <returns>The template/document type pairs that were parsed.</returns>
        private Dictionary<string, ImportedShippingTemplate> ParseCSV(DataTable csvDataTable, DataTable errorList)
        {
            var importedTemplates = new Dictionary<string, ImportedShippingTemplate>();

            var folderList = EDocumentFolder.GetFoldersInBroker(this.BrokerId);

            var docTypeList = EDocumentDocType.GetDocTypesByBroker(this.BrokerId, EnforcePermissions: false);

            // Errors in the CSV file will be relative to the second line
            // in the file, after the header line.
            int line = 2;

            foreach (DataRow row in csvDataTable.Rows)
            {
                var importTemplateName = row[(int)ShippingTemplateImporterColumns.ShippingTemplateName].ToString();

                var importFolderName = row[(int)ShippingTemplateImporterColumns.FolderName].ToString();

                var importDocTypeName = row[(int)ShippingTemplateImporterColumns.DocType].ToString();

                var specifiedFolder = folderList.FirstOrDefault(existingFolder =>
                    existingFolder.FolderNm.Equals(importFolderName, StringComparison.OrdinalIgnoreCase));

                if (specifiedFolder == null)
                {
                    errorList.Rows.Add(line, "Could not find specified folder '" + importFolderName + "'.");
                }

                var specifiedDocType = docTypeList.FirstOrDefault(existingDocType =>
                    existingDocType.DocTypeName.Equals(importDocTypeName, StringComparison.OrdinalIgnoreCase));

                if (specifiedDocType == null)
                {
                    errorList.Rows.Add(line, "Could not find specified doc type '" + importDocTypeName + "'.");
                }

                // We'll wait until this point to continue the loop so we can check
                // for both the existence of the doc type and the existence of the 
                // folder.
                if (specifiedFolder == null || specifiedDocType == null)
                {
                    ++line;

                    continue;
                }
                else if (specifiedDocType.Folder.FolderId != specifiedFolder.FolderId)
                {
                    var msg = string.Format(
                        "This folder/doc type combination does not exist: Folder '{0}', DocType '{1}'.",
                        specifiedFolder.FolderNm,
                        specifiedDocType.DocTypeName);

                    errorList.Rows.Add(line, msg);

                    ++line;

                    continue;
                }

                var folderId = specifiedFolder.FolderId.ToString();

                var docTypeId = specifiedDocType.DocTypeId;

                if (importedTemplates.ContainsKey(importTemplateName))
                {
                    // Avoid adding duplicate imported document types for the same shipping template.
                    if (!importedTemplates[importTemplateName].DocTypes.Any(importedDocType =>
                            ShippingTemplateImporter.HasDuplicate(importedDocType, folderId, docTypeId)))
                    {
                        importedTemplates[importTemplateName].DocTypes.Add(new ImportedDocType()
                        {
                            DocTypeId = docTypeId,
                            DocTypeName = importDocTypeName,
                            FolderId = folderId,
                            FolderName = importFolderName
                        });
                    }
                }
                else
                {
                    var newShippingTemplate = new ImportedShippingTemplate(importTemplateName);

                    newShippingTemplate.DocTypes.Add(new ImportedDocType()
                    {
                        DocTypeId = docTypeId,
                        DocTypeName = importDocTypeName,
                        FolderId = folderId,
                        FolderName = importFolderName
                    });

                    importedTemplates.Add(importTemplateName, newShippingTemplate);
                }

                ++line;
            }

            return importedTemplates;
        }

        /// <summary>
        /// Saves <paramref name="importedTemplates"/> by either adding a new template if the imported template
        /// does not exist or editing an existing template with the same name.
        /// </summary>
        /// <param name="importedTemplates">The dictionary of imported templates to folder-document type pairs to save.</param>
        private void SaveImportedTemplates(Dictionary<string, ImportedShippingTemplate> importedTemplates)
        {
            var edocsShippingTemplate = new EDocumentShippingTemplate(this.BrokerId);

            var brokerTemplateList = edocsShippingTemplate.CombinedShippingTemplateList;

            var newTemplateList = importedTemplates.Where(importedTemplate =>
                !brokerTemplateList.Any(brokerTemplate => brokerTemplate.ShippingTemplateName.Equals(importedTemplate.Key, StringComparison.OrdinalIgnoreCase)));

            foreach (var newTemplate in newTemplateList)
            {
                newTemplate.Value.IsNew = true;

                var docTypeIds = newTemplate.Value.DocTypes.Select(docType => docType.DocTypeId);

                // The parsing mechanism used by the stored procedure expects the 
                // following stacking order format, including the last comma: 1,2,3,
                edocsShippingTemplate.AddShippingTemplateType(newTemplate.Key, string.Join(",", docTypeIds) + ",");
            }

            var existingEditedTemplateList = importedTemplates.Except(newTemplateList);

            foreach (var existingEditedTemplate in existingEditedTemplateList)
            {
                existingEditedTemplate.Value.IsNew = false;

                var docTypeIds = existingEditedTemplate.Value.DocTypes.Select(docType => docType.DocTypeId);

                var shippingTemplateId = brokerTemplateList
                                            .First(brokerTemplate => brokerTemplate.ShippingTemplateName.Equals(existingEditedTemplate.Key, StringComparison.OrdinalIgnoreCase))
                                            .ShippingTemplateId;

                // The parsing mechanism used by the stored procedure expects the 
                // following stacking order format, including the last comma: 1,2,3,
                edocsShippingTemplate.EditShippingTemplateType(
                    existingEditedTemplate.Key,
                    string.Join(",", docTypeIds) + ",",
                    Convert.ToInt32(shippingTemplateId));
            }
        }

        /// <summary>
        /// Returns a string containing the names of the templates that were successfully imported.
        /// </summary>
        /// <param name="importedTemplates">
        /// The dictionary of template name/doc type pairs that were saved.
        /// </param>
        /// <returns>A string indicating the templates that were successfully imported.</returns>
        private string GetNamesOfImportedTemplates(Dictionary<string, ImportedShippingTemplate> importedTemplates)
        {
            if (!importedTemplates.Any())
            {
                return "No templates were imported.";
            }

            var sb = new StringBuilder();

            sb.AppendLine("The following templates were successfully imported:");

            sb.AppendLine();

            // For readability, we want to include a few spaces before the folder/doc type
            // name. We use a verbatim string here because spaces created with PadLeft()
            // do not correctly display.
            const string DocTypePrintFormat = @"    {0} : {1}";

            foreach (var templateKVP in importedTemplates)
            {
                sb.AppendLine(templateKVP.Key + (templateKVP.Value.IsNew ? " (new)" : " (edited)"));

                var docTypeNamesWithFolders = templateKVP.Value.DocTypes.Select(docType =>
                    string.Format(DocTypePrintFormat, docType.FolderName, docType.DocTypeName));

                foreach (var docTypeNameWithFolder in docTypeNamesWithFolders)
                {
                    sb.AppendLine(docTypeNameWithFolder);
                }

                sb.AppendLine();
            }

            return sb.ToString();
        }
    }
}
