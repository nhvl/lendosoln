﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using CommonProjectLib.Common.Lib;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Common.TextImport;
using LendersOffice.Security;
using LendersOffice.Common;

namespace LendersOffice.BrokerAdmin.Importers
{

    public class InternalEmployeeImporter : AbstractImporter
    {
        #region Internal classes
        protected class Relationship
        {
            public string AsA;
            public EmployeeDependency RequiredBy;
        }

        /// <summary>
        /// Track relationship dependencies, so we can (for instance) import Joe and his manager
        /// in the same pass
        /// </summary>
        [DebuggerDisplay("Name = {FullName}, Login = {LoginName}")]
        protected class EmployeeDependency
        {
            public List<Relationship> RequiresMe;
            public List<EmployeeDependency> IRequire;
            public EmployeeDB BaseDBObj;
            public string LoginName;
            public string FullName;
            public string[] Roles;
            public int FromLine;
            public Guid Id;
            //For cycle detection - you do a dfs and mark every node, after you've gone through
            //all the roots of the tree and never seen a marked node twice it's acyclic
            public bool Mark;
            public EmployeeDependency()
            {
                RequiresMe = new List<Relationship>();
                IRequire = new List<EmployeeDependency>();
                Roles = new string[] { };

            }

            internal void CopyOver(EmployeeDependency other)
            {
                this.RequiresMe.AddRange(other.RequiresMe);
                this.IRequire.AddRange(other.IRequire);
                this.Roles = this.Roles.Union(other.Roles).ToArray();

                this.FullName = other.FullName;
                this.BaseDBObj = other.BaseDBObj;
                this.LoginName = other.LoginName;
                this.FromLine = other.FromLine;
                this.Id = other.Id;
            }
        }
        #endregion

        #region Column setup
        private static readonly string[] columnNames = 
        { 
            "First Name",
            "Middle Name",
            "Last Name",
            "Email",
            "Phone",
            "Cellphone",
            "IsCellphonePrivate",
            "EnableAuthCodeSms",
            "Fax",
            "Originator Compensation Is Only Paid For First Lien Of Combo",
            "Originator Compensation %",
            "Minimum",
            "Maximum",
            "Fixed Amount",
            "Login Name",
            "Password",
            "Role 1",
            "Role 2",
            "Role 3",
            "Manager",
            "Processor",
            "Lender Account Executive",
            "Lock Desk",
            "Underwriter",
            "Junior Processor",
            "Junior Underwriter",
            "Loan Officer Assistant",
            "Price Group",
            "Branch",
            "Access Level",
            "NMLS ID",
            "Name on Loan Document"
        };

        private static readonly string[] reqColumnNames = 
        { 
            "First Name",
            "Last Name",
            "Email",
            "Phone",
            "Login Name",
            "Password",
            "Role 1",
            "Access Level" 
        };
        #endregion

        #region Relationship setup
        private static readonly List<E_RoleT> AvailableRelationshipRoles = new List<E_RoleT>() 
        { 
            E_RoleT.Manager,
            E_RoleT.Processor,
            E_RoleT.LenderAccountExecutive, 
            E_RoleT.LockDesk, 
            E_RoleT.Underwriter,
            E_RoleT.JuniorProcessor,
            E_RoleT.JuniorUnderwriter,
            E_RoleT.LoanOfficerAssistant
        };
        //Maps relationship Roles (above) to the EmployeeId GUID fields in the EmployeeDB object that they refer to.
        //If it wasn't for LenderAccountExec we wouldn't need to do this :(
        private static readonly Dictionary<string, PropertyInfo> RoleToField = new Dictionary<string, PropertyInfo>() 
        {
            {"Manager", typeof(EmployeeDB).GetProperty("ManagerEmployeeID")},
            {"Processor", typeof(EmployeeDB).GetProperty("ProcessorEmployeeID")},
            {"Lender Account Exec", typeof(EmployeeDB).GetProperty("LenderAcctExecEmployeeID")},
            {"Lock Desk", typeof(EmployeeDB).GetProperty("LockDeskEmployeeID")},
            {"Underwriter", typeof(EmployeeDB).GetProperty("UnderwriterEmployeeID")},
            {"Junior Processor", typeof(EmployeeDB).GetProperty("JuniorProcessorEmployeeID")},
            {"Junior Underwriter", typeof(EmployeeDB).GetProperty("JuniorUnderwriterEmployeeID")},
            {"Loan Officer Assistant", typeof(EmployeeDB).GetProperty("LoanOfficerAssistantEmployeeID")},
        };
        #endregion

        public InternalEmployeeImporter()
        {
            HeaderText = string.Join(", ", columnNames);
            SheetName = "Import Internal Employees";
        }

        public override void Import(string csv, Guid BrokerId, out string Results, out DataTable ErrorList)
        {
            ErrorList = new DataTable();
            Results = "";
            IEnumerable<EmployeeDB> newEmployees = new List<EmployeeDB>();
            bool parseSuccess;
            StringBuilder log = new StringBuilder("Importing employees");
            log.AppendLine();

            var processedCount = 0;

            try
            {
                ILookup<string, Action<CStoredProcedureExec>> afterSaveActions;

                parseSuccess = ParseCSV(csv, BrokerId, out newEmployees, out afterSaveActions, out ErrorList);

                if(!parseSuccess) {
                    log.AppendLine("Could not parse employee csv, errors are: ");

                    foreach (DataRow row in ErrorList.Rows)
                    {
                        log.AppendLine(row[0].ToString());
                    }
                }

                if (parseSuccess)
                {
                    //We parsed everything, so go ahead and save it all
                    log.AppendLine("Saving employees");
                    using (CStoredProcedureExec sproc = new CStoredProcedureExec(BrokerId))
                    {
                        sproc.BeginTransactionForWrite();
                        foreach (EmployeeDB emp in newEmployees)
                        {
                            try
                            {
                                log.AppendLine("Saving employee: " + emp.LoginName);
                                emp.Save(sproc);
                                var actions = afterSaveActions[emp.LoginName];
                                log.AppendLine("Employee saved, doing post-save actions. There are: " + actions.Count());
                                foreach (var action in actions)
                                {
                                    action(sproc);
                                    log.AppendLine("Action completed");
                                }
                                log.AppendLine(" Post-save actions successful");
                                log.AppendLine("");
                                ++processedCount;
                            }
                            catch (SqlException e)
                            {
                                ErrorList.Rows.Add("Could not save employee: " + emp.FullName + "! This is probably due to a db problem. No employees were saved.");
                                ErrorList.Rows.Add("Error text: " + e.ToString());
                                sproc.RollbackTransaction();
                                throw;
                            }
                            catch (Exception e)
                            {
                                ErrorList.Rows.Add("Could not save employee: " + emp.FullName + "! This is a totally unexpected error. No employees were saved.");
                                ErrorList.Rows.Add("Error text: " + e.ToString());
                                sproc.RollbackTransaction();
                                throw;
                            }
                        }
                        sproc.CommitTransaction();
                    }
                }
            }
            catch (Exception e)
            {
                Tools.LogError("Failed to import employees: " + e.Message + ".", e);
                ErrorList.Rows.Add("Failed to import employees: " + e.Message + ".");

                for (var processedEmployeeIndex = 0; processedEmployeeIndex < processedCount; ++processedEmployeeIndex)
                {
                    PrincipalFactory.AllUserSharedDbDelete(BrokerId, newEmployees.ElementAt(processedEmployeeIndex).LoginName);
                }

                throw;
            }
            finally
            {
                Tools.LogInfo(log.ToString());
            }

            //Short circuit evaluation keeps this from ever causing a null pointer exception
            if (newEmployees == null || newEmployees.Count() == 0)
            {
                Results = "No employees were saved.";
            }
            else
            {
                Results = "The following employees were successfully saved: \n\n" +
                            newEmployees.Aggregate(new StringBuilder(),
                            (accumulator, next) => accumulator.AppendLine(next.FullName)).ToString();
            }
        }



        /// <summary>
        /// Parse the (hopefully) CSV text to import
        /// </summary>
        /// <param name="newEmployees">A list of employees that need to be saved</param>
        /// <param name="BrokerID">The broker ID for which these employees should be imported</param>
        /// <param name="csvText">The CSV text to import</param>
        /// <param name="afterSave">A list of categories that need to be saved</param>
        /// <param name="errorList">Errors that occurred during parsing</param>
        /// <returns>True if parsing was successful, false if not</returns>
        private static bool ParseCSV(string csvText, Guid BrokerId, out IEnumerable<EmployeeDB> newEmployees, out ILookup<string, Action<CStoredProcedureExec>> afterSaveProcesses, out DataTable errorList)
        {
            //Temporarily hold new employees here, we'll assign them an order based on their 
            //dependency requirements later.
            newEmployees = new List<EmployeeDB>();
            errorList = new DataTable();
            var afterSave = new List<Tuple<string, Action<CStoredProcedureExec>>>();

            //Keep track of which new logins we're going to create, so we can know about conflicts we're going to cause.
            var newLoginNames = new HashSet<string>();

            //This is a dictionary that maps user login name -> dependency
            var EmpDep = new Dictionary<string, EmployeeDependency>();

            errorList.Columns.Add(new DataColumn("Errors", typeof(string)));

            #region Loading references so we can look things up

            //Role table, to see which employees have which roles
            BrokerLoanAssignmentTable roleTable = new BrokerLoanAssignmentTable();
            foreach (var roleType in AvailableRelationshipRoles)
            {
                roleTable.Retrieve(BrokerId, roleType);
            }

            //All employees, so we can be all like "this dude exists but he's not a manager",
            //or "Exists but isn't active"
            var employeeNames = new List<ShortEmployeeInfo>();
            SqlParameter[] parameters = {
                                new SqlParameter("@BrokerId", BrokerId),
                                new SqlParameter("@IsActiveFilter", true)
                            };
            using (IDataReader reader = StoredProcedureHelper.ExecuteReader(BrokerId, "ListEmployeeByBrokerId", parameters))
            {
                while (reader.Read())
                {
                    employeeNames.Add(new ShortEmployeeInfo()
                    {
                        IsActive = bool.Parse(reader["IsActive"].ToString()),
                        Id = new Guid(reader["EmployeeId"].ToString()),
                        Login = reader["LoginNm"].ToString(),
                        FullName = reader["UserFirstNm"] + " " + reader["UserLastNm"],
                    });
                }
            }
            employeeNames = employeeNames.OrderBy((a) => a.FullName).ToList();

            //Pricing groups, both name and id
            var pricingGroups = new List<ShortPricingGroupInfo>();
            parameters = new SqlParameter[]{
                        new SqlParameter("@BrokerId", BrokerId),
                        };
            using (IDataReader reader = StoredProcedureHelper.ExecuteReader(BrokerId, "ListPricingGroupByBrokerId", parameters))
            {
                while (reader.Read())
                {
                    pricingGroups.Add(new ShortPricingGroupInfo()
                    {
                        Name = (string)reader["LpePriceGroupName"],
                        Id = new Guid(reader["LpePriceGroupId"].ToString()),
                    });
                }
            }

            //Get branch info, so we can set up the user
            var availableBranches = new List<ShortBranchInfo>();
            using (IDataReader reader = BranchDB.GetBranches(BrokerId))
            {
                while (reader.Read())
                {
                    Guid DefaultPriceGroupId = Guid.Empty;
                    if (!string.IsNullOrEmpty(reader["BranchLpePriceGroupIdDefault"].ToString()))
                    {
                        DefaultPriceGroupId = new Guid(reader["BranchLpePriceGroupIdDefault"].ToString());
                    }
                    availableBranches.Add(new ShortBranchInfo()
                    {
                        CanonicalName = (string)reader["Name"],
                        DisplayName = (string)reader["DisplayNm"],
                        DefaultPriceGroup = DefaultPriceGroupId,
                        Id = new Guid(reader["BranchID"].ToString()),
                    });
                }
            }

            //Valid roles - note that this filters based on RoleDesc, but returns RoleModifiableDesc
            //That's handy because we're going by RoleModifiableDesc
            //It's less handy because this is a dictionary of Guid -> Role Name, 
            //and we want it the other way around. Fortunately Linq will swap it for us.
            //Note that this will break if anyone is silly enough to name two roles to the same thing.
            var validRoles = Tools.GetRolesExcluding("Consumer", "BrokerProcessor").ToDictionary((a) => a.Value, (a) => a.Key, StringComparer.InvariantCultureIgnoreCase);
            #endregion

            //ILookups are nice, pity nothing really implements them except ToLookup
            ILookup<int, string> parseErrors;

            //Finally, start the parsing
            DataTable data = DelimitedListParser.DsvToDataTable(csvText, ',', true, out parseErrors);

            //Validate the columns
            ILookup<int, string> validateErrors = DataParsing.ValidateColumns(data, columnNames, reqColumnNames, 1);

            //Don't dump the parse errors into the error list right now, though - we want all the errors to show up in line order.

            //However, since we don't touch the header row and that might have generated errors, add them now.
            int line = 1;

            foreach (string err in parseErrors[line])
            {
                errorList.Rows.Add("Error on line " + line + ": " + err);
            }
            foreach (string err in validateErrors[line])
            {
                errorList.Rows.Add("Error on line " + line + ": " + err);
            }

            for (int i = 0; i < data.Rows.Count; i++)
            {
                //We start on line 2 because there was a header row
                line++;

                //And if it's totally empty, skip it too
                bool skip = true;
                foreach (var thing in data.Rows[i].ItemArray)
                {
                    if (!string.IsNullOrEmpty(thing.ToString().TrimWhitespaceAndBOM()))
                    {
                        skip = false;
                        break;
                    }
                }
                if (skip)
                {
                    continue;
                }

                //Did this line check out in parsing?
                if (parseErrors.Contains(line))
                {
                    foreach (string err in parseErrors[line])
                    {
                        errorList.Rows.Add("Error on line " + line + ": " + err);
                    }
                    continue;
                }
                if (validateErrors.Contains(line))
                {
                    foreach (string err in validateErrors[line])
                    {
                        errorList.Rows.Add("Error on line " + line + ": " + err);
                    }
                    continue;
                }
                //If the number of columns is wrong, we have to just skip this row.
                if (data.Rows[i].ItemArray.Length != columnNames.Count())
                {
                    continue;
                }

                //Make a nice little anonymous class, so we have well-typed entries instead of
                //referring to row[2] all the time.
                var entry = new
                {
                    #region anon class contents
                    FirstName = (string)data.Rows[i]["First Name"],
                    MiddleName = (string)data.Rows[i]["Middle Name"],
                    LastName = (string)data.Rows[i]["Last Name"],
                    Email = (string)data.Rows[i]["Email"],
                    Phone = (string)data.Rows[i]["Phone"],
                    Fax = (string)data.Rows[i]["Fax"],
                    OrigCompOnlyForFirstLien = (string)data.Rows[i]["Originator Compensation Is Only Paid For First Lien Of Combo"],
                    OrigCompPercent = (string)data.Rows[i]["Originator Compensation %"],
                    MinComp = (string)data.Rows[i]["Minimum"],
                    MaxComp = (string)data.Rows[i]["Maximum"],
                    FixedComp = (string)data.Rows[i]["Fixed Amount"],
                    Login = (string)data.Rows[i]["Login Name"],
                    Password = (string)data.Rows[i]["Password"],

                    UserRoles = FilterRoles(new string[] {
                                    (string)data.Rows[i]["Role 1"], 
                                    (string)data.Rows[i]["Role 2"], 
                                    (string)data.Rows[i]["Role 3"]
                    }),
                    Relationships = from string s in AvailableRelationshipRoles.Select(roleT => Role.GetRoleDescription(roleT))
                                    where !string.IsNullOrEmpty((string)data.Rows[i][s])
                                    select new
                                    {
                                        Role = s,
                                        AssignedTo = (string)data.Rows[i][s],
                                    },

                    PriceGroup = (string)data.Rows[i]["Price Group"],
                    Branch = (string)data.Rows[i]["Branch"],
                    AccessLevel = (string)data.Rows[i]["Access Level"],
                    NMLSId = (string)data.Rows[i]["NMLS ID"],
                    NameOnLoanDocumentDocument = (string)data.Rows[i]["Name on Loan Document"],
                    Cellphone = (string)data.Rows[i]["Cellphone"],
                    IsCellphonePrivate = (string)data.Rows[i]["IsCellphonePrivate"],
                    EnableAuthCodeSms = (string)data.Rows[i]["IsCellphonePrivate"]
                    #endregion
                };

                //And of course our new employee
                EmployeeDB emp = new EmployeeDB();
                emp.BrokerID = BrokerId;

                //Constants for all new employees
                emp.PasswordExpirationD = EmployeeDB.MIN_SQL_SMALLDATETIME; //special value, means they must change pwd on next login
                emp.WhoDoneIt = Guid.Empty;
                emp.IsActive = true;
                emp.AllowLogin = true;


                //Variable assignments, easy strings first because they'll always translate properly
                //and we don't need to do much checking
                emp.FirstName = entry.FirstName;
                emp.MiddleName = entry.MiddleName;
                emp.LastName = entry.LastName;
                emp.Email = entry.Email;
                emp.Password = entry.Password;
                emp.LosIdentifier = entry.NMLSId;
                emp.LoginName = entry.Login;

                //Phone numbers may need to be converted to the proper format
                emp.Phone = FixupPhone(entry.Phone);
                emp.Fax = FixupPhone(entry.Fax);
                emp.CellPhone = FixupPhone(entry.Cellphone);

                bool isCellMfaOnly;
                if (bool.TryParse(entry.IsCellphonePrivate, out isCellMfaOnly))
                {
                    emp.IsCellphoneForMultiFactorOnly = isCellMfaOnly;
                }
                else
                {
                    errorList.Rows.Add($"Error on line {line}. {entry.IsCellphonePrivate} not a valid value for IsCellphonePrivate");
                }

                bool enableAuthSms;
                if (bool.TryParse(entry.EnableAuthCodeSms, out enableAuthSms))
                {
                    emp.EnableAuthCodeViaSms = enableAuthSms;
                }
                else
                {
                    errorList.Rows.Add($"Error on line {line}. {entry.EnableAuthCodeSms} not a valid value for EnableAuthCodeSms");
                }

                if(!string.IsNullOrEmpty(entry.NameOnLoanDocumentDocument))
                {
                    emp.NmOnLoanDocsLckd = true;
                    emp.NmOnLoanDocs = entry.NameOnLoanDocumentDocument;
                }

                #region Figure out employee Ids (or names if they don't exist) for relationships (e.g, manager)
                //First, some validity checking - if you have role A, 
                //you cannot have someone with role A above you.
                //e.g, a Manager cannot have a Manager.
                foreach (var relationship in entry.Relationships)
                {
                    if (entry.UserRoles.Contains(relationship.Role))
                    {
                        errorList.Rows.Add("Error on line " + line + ": Employee " + emp.FullName + " is a " + relationship.Role + ", and so cannot have " + relationship.AssignedTo + " assigned as " + relationship.Role);
                    }
                }

                //Then, figure out the user's relationship dependencies;
                //If any exist, we need to set them up after the superior has been set up.
                EmployeeDependency myDependency = new EmployeeDependency()
                {
                    LoginName = emp.LoginName,
                    BaseDBObj = emp,
                    Roles = entry.UserRoles,
                    FromLine = line,
                    FullName = emp.FullName,
                };

                //We may have been referenced before by another employee, so make sure that our stuff is copied over
                if (EmpDep.ContainsKey(emp.FullName))
                {
                    //We were previously stored by full name, replace that with login name
                    if (EmpDep.ContainsKey(emp.LoginName))
                    {
                        errorList.Rows.Add("Error on line " + line + ": Login name " + emp.LoginName + " was assigned to more than one user; see also line " + EmpDep[emp.LoginName].FromLine);
                        continue;
                    }
                    EmpDep.Add(emp.LoginName, EmpDep[emp.FullName]);
                    EmpDep.Remove(emp.FullName);
                    EmpDep[emp.LoginName].CopyOver(myDependency);

                }
                else if (EmpDep.ContainsKey(emp.LoginName))
                {
                    EmpDep[emp.LoginName].CopyOver(myDependency);
                }
                else
                {
                    EmpDep.Add(emp.LoginName, myDependency);
                }

                //And then make sure our reference is right, since we may have just copied stuff over.
                myDependency = EmpDep[emp.LoginName];
                foreach (var r in entry.Relationships)
                {
                    //These may be empty, so just skip if they are
                    if (string.IsNullOrEmpty(r.AssignedTo))
                    {
                        continue;
                    }

                    //This might be a guid, a user login, or a user's real name
                    string reqAssignedToName;
                    //If it's a real name in firstname, lastname format, this will fix it
                    //It'll pass anything else through unchanged
                    if (!TryNormalizeName(r.AssignedTo, out reqAssignedToName))
                    {
                        errorList.Rows.Add("Error on line " + line + ": Could not understand name " + r.AssignedTo + ", which is being assigned as " + r.Role + ". Try entering it as Firstname Lastname, or use their id.");
                        continue;
                    }

                    //Now figure out the user's login
                    var assignedEmployeeByGuid = DataParsing.GetByIdOrName(employeeNames, reqAssignedToName, (a) => a.FullName, (a) => a.Id);
                    var assignedEmployeeByUsername = DataParsing.GetByIdOrName(employeeNames, reqAssignedToName, (a) => a.FullName, (a) => a.Login);
                    if (assignedEmployeeByUsername.Count() > 1)
                    {
                        errorList.Rows.Add("Error on line " + line + ": Employee name or login \"" + r.AssignedTo + "\" is ambiguous for role " + r.Role + ". If you're using the employee name, try using their id or login");
                        continue;
                    }
                    else if (assignedEmployeeByGuid.Count() > 1)
                    {
                        errorList.Rows.Add("Error on line " + line + ": Employee name or id \"" + r.AssignedTo + "\" is ambiguous for role " + r.Role + ". If you're using the employee name, try using their id or login");
                        continue;
                    }
                    else if ((assignedEmployeeByUsername.Count() == 0) &&
                             (assignedEmployeeByGuid.Count() == 0))
                    {
                        //If the requested employee doesn't exist yet, we may need to do some dependency resolution with them later; either way we skip for now.
                        var assignedEmployee = DataParsing.GetByIdOrName(EmpDep, reqAssignedToName, (a) => a.Value.FullName, (a) => a.Key);
                        if (assignedEmployee.Count() == 0)
                        {
                            //potentially all we know about them right now is their name or their login (and we don't know which it is), but we'll fix that later
                            //This assumes that the user is referred to the same way throughout the import doc.
                            EmpDep.Add(reqAssignedToName, new EmployeeDependency() { FromLine = line, FullName = reqAssignedToName });
                        }
                        else
                        {
                            reqAssignedToName = assignedEmployee.First().Key;
                        }
                        var rel = new Relationship() { AsA = r.Role, RequiredBy = myDependency };

                        EmpDep[reqAssignedToName].RequiresMe.Add(rel);
                        myDependency.IRequire.Add(EmpDep[reqAssignedToName]);
                        continue;
                    }

                    //On the other hand, if the requested employee does exist we can assign them now.
                    ShortEmployeeInfo e = assignedEmployeeByGuid.Union(assignedEmployeeByUsername).First();
                    //Make sure they're active
                    if (!e.IsActive)
                    {
                        errorList.Rows.Add("Error on line " + line + ": Employee name or id \"" + r.AssignedTo + "\" is may not exist?");
                        continue;
                    }

                    var employeeAvailableRoles = roleTable[e.Id]; //.Cast<LendersOffice.Admin.BrokerLoanAssignmentTable.Role>();
                    if (employeeAvailableRoles == null)
                    {
                        errorList.Rows.Add("Error on line " + line + ": Employee name or id \"" + r.AssignedTo + "\" is not a " + r.Role + " and cannot be assigned as one.");
                        continue;
                    }
                    //Can this employee actually fill this role?
                    if (!employeeAvailableRoles.Any((a) => a.ModifiableDesc.Equals(r.Role, StringComparison.InvariantCultureIgnoreCase)))
                    {
                        errorList.Rows.Add("Error on line " + line + ": Employee name or id \"" + r.AssignedTo + "\" is not a " + r.Role + " and cannot be assigned as one.");
                        continue;
                    }

                    //Since we can already fulfill this dependency, just do it now.
                    try
                    {
                        RoleToField[r.Role].SetValue(emp, e.Id, null);
                    }
                    catch (KeyNotFoundException)
                    {
                        errorList.Rows.Add("Error on line " + line + " : Key Not Found " + r.Role);
                        continue;
                    }
                }
                #endregion

                #region Parsing compensation decimals - note that any of them may be empty

                if (!string.IsNullOrEmpty(entry.OrigCompPercent))
                {
                    decimal OrigCompPercent;
                    if (decimal.TryParse(entry.OrigCompPercent, out OrigCompPercent))
                    {
                        emp.OriginatorCompensationPercent = OrigCompPercent;
                    }
                    else
                    {
                        errorList.Rows.Add("Error on line " + line + ": " +
                                    "Could not parse \"" + entry.OrigCompPercent + " \" as a decimal " +
                                    "for employee originator compensation percent.");
                    }
                }

                if (!string.IsNullOrEmpty(entry.MaxComp))
                {
                    decimal max;
                    if (decimal.TryParse(entry.MaxComp, out max))
                    {
                        emp.OriginatorCompensationMaxAmount = max;
                    }
                    else
                    {
                        errorList.Rows.Add("Error on line " + line + ": " +
                                    "Could not parse \"" + entry.MaxComp + " \" as a decimal " +
                                    "for employee originator maximum compensation.");
                    }
                }

                if (!string.IsNullOrEmpty(entry.MinComp))
                {
                    decimal min;
                    if (decimal.TryParse(entry.MinComp, out min))
                    {
                        emp.OriginatorCompensationMinAmount = min;
                    }
                    else
                    {
                        errorList.Rows.Add("Error on line " + line + ": " +
                                    "Could not parse \"" + entry.MinComp + " \" as a decimal " +
                                    "for employee originator minimum compensation.");
                    }
                }

                bool origCompOnlyForFirstLien = false;
                if (bool.TryParse(entry.OrigCompOnlyForFirstLien, out origCompOnlyForFirstLien))
                {
                    emp.OriginatorCompensationIsOnlyPaidForFirstLienOfCombo = origCompOnlyForFirstLien;
                }
                #endregion

                #region Assigning the pricing group
                //May be empty, so we need to skip over this if it is
                if (!string.IsNullOrEmpty(entry.PriceGroup))
                {
                    var assignedPricingGroup = DataParsing.GetByIdOrName(pricingGroups, entry.PriceGroup, (a) => a.Name, (a) => a.Id);
                    if (assignedPricingGroup.Count() == 0)
                    {
                        errorList.Rows.Add("Error on line " + line + ": Could not find any pricing group by id or name of " + entry.PriceGroup);
                    }
                    else if (assignedPricingGroup.Count() > 1)
                    {
                        errorList.Rows.Add("Error on line " + line + ": Pricing group name or id " + entry.PriceGroup + " is ambiguous");
                    }
                    else
                    {
                        emp.LpePriceGroupID = assignedPricingGroup.First().Id;
                    }
                }
                #endregion

                #region Assigning permissions
                //We have to do this the hard way, because there's no easy mapping 
                //from what the UI says to what the enums are.
                Permission perm;
                switch (entry.AccessLevel.ToLowerInvariant())
                {
                    case ("corporate"):
                        perm = Permission.BrokerLevelAccess; //see?
                        break;
                    case ("individual"):
                        perm = Permission.IndividualLevelAccess;
                        break;
                    case ("branch"):
                        perm = Permission.BranchLevelAccess;
                        break;
                    case ("team"):
                        perm = Permission.TeamLevelAccess;
                        break;                        
                    default:
                        errorList.Rows.Add("Error on line " + line + ": Could not parse access level " + entry.AccessLevel + ". Please use one of Corporate, Individual or Branch.");
                        perm = Permission.IndividualLevelAccess; //This isn't going to be used anyway
                        break;
                }

                //And then we have to assign them the hard way too - the employee needs an Id 
                //in order to save their permissions, but that id doesn't get assigned until 
                //after they're saved.
                //So we'll store a lambda that'll save the perms later.
                afterSave.Add(new Tuple<string, Action<CStoredProcedureExec>>
                    (emp.LoginName, (spExec) =>
                    {
                        // The Access Level is exclusive!  We can't have more than one set to true, or it will default to the higher value (Broker -> Branch -> Individual -> Team).
                        BrokerUserPermissions bUP = new BrokerUserPermissions(BrokerId, emp.ID, spExec);

                        bUP.SetPermission(Permission.BrokerLevelAccess, false);
                        bUP.SetPermission(Permission.BranchLevelAccess, false);
                        bUP.SetPermission(Permission.TeamLevelAccess, false);
                        bUP.SetPermission(Permission.IndividualLevelAccess, false);

                        bUP.SetPermission(perm, true);
                        bUP.Update(spExec);
                    }));
                #endregion

                #region Assigning user roles
                bool AllRolesValid = true;
                //Make sure that the roles are actually valid roles
                foreach (string r in entry.UserRoles)
                {
                    if (!validRoles.ContainsKey(r))
                    {
                        AllRolesValid = false;
                        //Lender account executive is the one people have problems with, so check if it's 
                        //similar to that
                        string suggestion = null;
                        //Lender without the l, for upper or lower case since contains doesn't let you pick
                        if (r.Contains("ender"))
                        {
                            suggestion = "Did you mean Lender Account Executive?";
                        }

                        errorList.Rows.Add("Error on line " + line + ": Role " + r + " is not valid. " + suggestion ?? "");
                    }
                }
                if (AllRolesValid)
                {
                    //This needs to be a csv of role guids, so we have to convert that.
                    emp.Roles = string.Join(",", (from s in entry.UserRoles select validRoles[s].ToString()).ToArray());
                }
                #endregion

                #region Assigning branches
                var reqBranches = DataParsing.GetByIdOrName(availableBranches, entry.Branch,
                                                            (a) => a.CanonicalName, (a) => a.Id);
                if (!string.IsNullOrEmpty(entry.Branch) && reqBranches.Count() == 0)
                {
                    errorList.Rows.Add("Error on line " + line + ": Branch name or id " + entry.Branch + " could not be found. Try using the branch id.");
                }
                else if (reqBranches.Count() > 1)
                {
                    errorList.Rows.Add("Error on line " + line + ": Branch name " + entry.Branch + " is ambiguous. Try using the branch id.");
                }
                //There's three cases here:
                //1. There was an error of some sort, in which case this won't get used so it doesn't matter
                //2. The user specified a branch, in which case it will be the only element
                //3. The user did not specify a branch, in which case we just want the first element
                emp.BranchID = (reqBranches.FirstOrDefault() ?? availableBranches.FirstOrDefault()).Id;


                #endregion


                //And finally, make sure their login name is unique
                if (!Tools.IsLoginUnique(entry.Login, Guid.Empty))
                {
                    errorList.Rows.Add("Error on line " + line + ": Login name \"" + entry.Login + "\" is not unique. Try a different one.");
                }
                else if (!newLoginNames.Add(entry.Login))
                {
                    errorList.Rows.Add("Error on line " + line + ": Login name \"" + entry.Login + "\" has already been used during this import. Give them a different one.");
                }

                emp.LoginName = entry.Login;
                ((List<EmployeeDB>)newEmployees).Add(emp);
            }

            #region Verify that relationships are possible before figuring out creation order
            //Scan the employee list, make sure that everyone has their employeedb object
            foreach (var entry in EmpDep)
            {
                if (entry.Value.BaseDBObj == null)
                {
                    entry.Value.BaseDBObj = (from e in newEmployees
                                             where e.LoginName.Equals(entry.Value.LoginName, StringComparison.InvariantCultureIgnoreCase)
                                             select e).FirstOrDefault();
                    //If we still couldn't find them, that's an error.
                    if (entry.Value.BaseDBObj == null)
                    {
                        errorList.Rows.Add("Error from line " + entry.Value.FromLine + ": User \"" +
                            entry.Key + "\" was referenced as a " +
                            entry.Value.RequiresMe.Aggregate("",
                                (current, next) => current + next.AsA + " for " + next.RequiredBy.FullName + ", ") +
                            "but is not an existing employee, and was not created in this spreadsheet.");
                    }
                    else
                    {
                        //If we did find them, make sure they fill the role they're asked to.
                        foreach (var reqRole in entry.Value.RequiresMe)
                        {
                            if (!entry.Value.Roles.Contains(reqRole.AsA))
                            {
                                errorList.Rows.Add("Error from line " + reqRole.RequiredBy.FromLine + ": User " +
                                    entry.Value.LoginName + " is listed as being a " + reqRole.AsA + " for user " +
                                    reqRole.RequiredBy.LoginName + ", but does not have that role.");
                            }
                        }
                    }
                }
            }
            #endregion

            //If there were any errors so far, skip the dependency resolution since we don't really
            //need it.
            if (errorList.Rows.Count > 0)
            {
                newEmployees = null;
                afterSaveProcesses = null;
                return false;
            }

            #region Graph algorithms hooray!
            //So now we've mapped out all the inter-employee dependencies, build up a tree of them
            //The "roots" of the tree will be employees with no dependencies, going down will be
            //employees dependent on the previous level.

            #region Pruning the initial dictionary
            //Scan over the employee dependency list, remove anyone who is dependent on someone else
            //The only references left to the employees will be the roots of the dependency tree,
            //but if the whole thing is one giant cycle we'll leave at least one guy in there 
            //so we can pinpoint it for the user.
            List<string> toRemove = new List<string>();
            foreach (var dep in EmpDep.Values)
            {
                if (dep.IRequire.Count > 0)
                {
                    //Can't change the collection while we're foreaching over it, sooo...
                    //Also keep in mind that the entry may be keyed off of login name or full name.
                    if (!string.IsNullOrEmpty(dep.LoginName))
                    {
                        toRemove.Add(dep.LoginName);
                    }
                    toRemove.Add(dep.BaseDBObj.FullName);
                }
            }
            foreach (string name in toRemove)
            {
                //Make sure we leave at least one thing in here - if it's part of a cycle,
                //the cycle detection will error it.
                if (EmpDep.Count == 1)
                {
                    break;
                }

                EmpDep.Remove(name);

            }
            #endregion

            #region cycle detection
            List<EmployeeDependency> roots = EmpDep.Values.ToList();
            //Do some cycle checking - otherwise, our topo sort won't work.
            for (int i = 0; i < roots.Count; i++)
            {
                EmployeeDependency root = roots[i];
                Stack<EmployeeDependency> cycle;
                try
                {
                    cycle = CycleDetection(root);
                }
                catch (StackOverflowException)
                {
                    newEmployees = null;
                    afterSaveProcesses = null;
                    errorList.Rows.Add("Your management structure is too complicated! Try splitting it into two import files.");
                    return false;
                }

                if (cycle != null)
                {
                    StringBuilder errorCycle = new StringBuilder();
                    foreach (var element in cycle)
                    {
                        errorCycle.Append(element.FullName + " -> ");
                    }
                    newEmployees = null;
                    afterSaveProcesses = null;
                    errorList.Rows.Add("There was a management cycle along this line: " + errorCycle.ToString().TrimEnd(' ', '-', '>') + "\n Please remove at least one of the links in this cycle.");
                    return false;
                }
            }
            #endregion

            #region topological sort
            //Then do a topological sort starting from whatever's still in empdep
            //The way it works is you take a root of the tree 
            //(though it's probably more a shrub, since there may be several roots), 
            //remove all child references to it, and then take all the previously 
            //referencing children which are now roots and add them to the list of roots.
            //Because of the previous pass, we know that everything still in empdep is a root,
            //so that gives us a place to start.
            for (int i = 0; i < roots.Count; i++)
            {
                EmployeeDependency ed = roots[i];
                List<EmployeeDependency> toCheck = new List<EmployeeDependency>();
                foreach (var relationship in ed.RequiresMe)
                {
                    //Remove the child node's requirements
                    relationship.RequiredBy.IRequire.RemoveAll((req) => Object.ReferenceEquals(req, ed));
                    //And if they're now a root, add them to the list of things to check.
                    if (relationship.RequiredBy.IRequire.Count == 0)
                    {
                        roots.Add(relationship.RequiredBy);
                    }
                }
            }

            //So now, assuming the cycle detection worked, roots contains the empdb objects in a 
            //creatable order. Fill the after save list with functions that'll
            //fill in the required role ids after the employees have an id.
            foreach (EmployeeDependency superior in roots)
            {
                foreach (Relationship rel in superior.RequiresMe)
                {
                    afterSave.Add(new Tuple<string, Action<CStoredProcedureExec>>(
                        superior.LoginName,
                        (sproc) =>
                            //By the time this executes, the superior will have an id.
                            RoleToField[rel.AsA].SetValue(rel.RequiredBy.BaseDBObj, superior.BaseDBObj.ID, null))
                        );
                }
            }
            #endregion

            //Put things into newEmployees in the right order
            newEmployees = from dep in roots select dep.BaseDBObj;
            afterSaveProcesses = afterSave.ToLookup((a) => a.Item1, (a) => a.Item2);
            #endregion
            return !(errorList.Rows.Count > 0);
        }


        /// <summary>
        /// Filters out empty strings in the role columns, and splits up semi-colon delimited role lists into indvidual role names.
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        private static string[] FilterRoles(string[] p)
        {
            char delim = ';';
            List<string> filteredRoles = new List<string>();
            foreach (string s in p)
            {
                if (s.Contains(delim))
                {
                    filteredRoles.AddRange(s.Split(new char[] { delim },
                            StringSplitOptions.RemoveEmptyEntries));
                }
                else if (!string.IsNullOrEmpty(s))
                {
                    filteredRoles.Add(s);
                }
            }
            return filteredRoles.ToArray();
        }

        /// <summary>
        /// Simple recursive depth first cycle detection algorithm. Walks down the dependency tree marking visited nodes; if a marked node is seen twice, a cycle exists.
        /// </summary>
        /// <param name="current">The root of the tree</param>
        /// <returns>A stack indicating the path from the root to the cycle</returns>
        private static Stack<EmployeeDependency> CycleDetection(EmployeeDependency current)
        {
            Stack<EmployeeDependency> ret = null;
            if (current.Mark)
            {
                //We're marked, so there's a cycle involving us
                ret = new Stack<EmployeeDependency>();
                ret.Push(current);
            }

            //Mark the current node as visited
            current.Mark = true;
            var children = from relationship in current.RequiresMe select relationship.RequiredBy;
            //Do cycle detection on our children, if we didn't start out marked.
            if (ret == null)
            {
                foreach (EmployeeDependency dependent in children)
                {
                    ret = CycleDetection(dependent);
                    if (ret != null)
                    {
                        ret.Push(current);
                        break;
                    }
                }
            }

            //Cleanup - unmark all nodes while unrolling
            current.Mark = false;
            return ret;
        }

        /// <summary>
        /// Takes a name in a standard format and puts it into "Firstname Lastname" format. Understands "Lastname, Firstname" and of course "Firstname Lastname".
        /// Note that this will pass GUIDs and most logins through without changing them.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        private static bool TryNormalizeName(string name, out string normalized)
        {
            //Does it look like they wrote it in lastname, firstname format?
            if (name.Contains(','))
            {
                string[] commaParts = name.Split(',');
                //It looks like it, but there was more than one comma in there; we can't parse this.
                if (commaParts.Length != 2)
                {
                    normalized = null;
                    return false;
                }

                normalized = commaParts[0].TrimWhitespaceAndBOM() + " " + commaParts[1].TrimWhitespaceAndBOM();
                return true;
            }

            //No, so just trim multiple interior spaces out
            string[] spaceParts = name.Split(new char[] { ' ', '\t' },
                                    StringSplitOptions.RemoveEmptyEntries);

            normalized = spaceParts.Aggregate("", (accumulator, next) => accumulator + " " + next.TrimWhitespaceAndBOM()).TrimWhitespaceAndBOM();
            return true;
        }

        /// <summary>
        /// Fixes up an input phone number so it matches the (###) ###-####+ format.
        /// </summary>
        /// <param name="phone"></param>
        /// <returns></returns>
        private static string FixupPhone(string phone)
        {
            //If it's blank, we don't want to do anything.
            if (string.IsNullOrEmpty(phone))
            {
                return "";
            }

            //Remove everything that's not a digit
            phone = Regex.Replace(phone, @"\D", "");

            return "(" + phone.Substring(0, 3) + ") " + phone.Substring(3, 3) + "-" + phone.Substring(6); //No .Take(4) on the last one, they might have a really long phone number and we don't want to mess that up

        }
    }

}
