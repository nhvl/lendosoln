﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Common.TextImport;


namespace LendersOffice.BrokerAdmin.Importers
{

    public class LicensesImporter : AbstractImporter
    {
        //Configurable number of columns in case we add more later.
        private const int numColumns = 4;

        //And the first reqColumns are required - errors will be thrown if they're empty.
        private const int reqColumns = 4;

        private const string _headerText = @"Login Name, State, Expiration Date, License Number";
        private const string _sheetName = "Import Licenses";

        private Dictionary<string, Guid> dict = new Dictionary<string, Guid>();
        private Dictionary<string, List<List<String>>> licenseList = new Dictionary<string, List<List<String>>>();
        private List<String> savedUserLicense = new List<String>();

        public LicensesImporter()
        {
            HeaderText = _headerText;
            SheetName = _sheetName;
            loadUser();
        }

        private string GetSavedLicenses()
        {
            if (savedUserLicense.Count() == 0)
            {
                return "No user licenses were saved";
            }

            StringBuilder sb = new StringBuilder("The following user licenses were successfully saved:\n");
            foreach (String user in savedUserLicense)
            {
                sb.AppendLine(user);
            }

            return sb.ToString();
        }

        public override void Import(string csv, Guid BrokerId, out String Results, out DataTable ErrorList)
        {
            ErrorList = new DataTable();
            Results = "";
            
            try
            {
                bool parseSuccess = ParseCSV(csv, BrokerId, out ErrorList);

                if (parseSuccess)
                {
                    //We parsed everything, so go ahead and save it all                                        
                    foreach (String user in licenseList.Keys)
                    if( dict.ContainsKey(user))
                        {                        
                            EmployeeDB e = EmployeeDB.RetrieveById(BrokerId, dict[user]);
                            foreach (List<String> l in licenseList[user])
                            {                            
                                LicenseInfo license = new LicenseInfo(l[0], l[1], l[2]);                            
                                try
                                {
                                    e.LicenseInformationList.Add(license);                                    
                                }
                                catch (ArgumentException) { }
                                savedUserLicense.Add(user + "," + l[2]);                                                  
                            }
                            e.Save();                                                     
                        }
                    Results = GetSavedLicenses();
                    
                }
            }
            catch (CBaseException e)
            {
                Tools.LogError("Failed to import user licenses: " + e.Message + ".", e);
                throw;
            }
        }

        /// <summary>
        /// Parse the (hopefully) CSV file in m_Companies.Text
        /// </summary>
        /// <param name="toSave">A list of PML Brokers that need saving</param>
        /// <param name="errorList">Errors that occurred during parsing</param>
        /// <returns>True if parsing was successful, false if not</returns>
        private bool ParseCSV(string csv, Guid BrokerId, out DataTable errorList)
        {
            licenseList.Clear();
            savedUserLicense.Clear();

            errorList = new DataTable();

            bool errord = false;
            errorList.Columns.Add(new DataColumn("Errors", typeof(string)));

            ILookup<int, string> parseErrors;
            DataTable data = DelimitedListParser.CsvToDataTable(csv, true, reqColumns, numColumns, out parseErrors);

            //We skip the first line in parsing to make room for a header row, so start at 1            
            int line = 1;
            if (parseErrors.Contains(line))
            {
                foreach (string err in parseErrors[line])
                {
                    errord = true;
                    errorList.Rows.Add("Error on line " + line + ": " + err);
                }
            }

            for (int i = 0; i < data.Rows.Count; i++)
            {
                DataRow row = data.Rows[i];
                line++;
                foreach (string err in parseErrors[line])
                {
                    errord = true;
                    errorList.Rows.Add("Error on line " + line + ": " + err);
                }

                //PmlBroker pb = PmlBroker.CreatePmlBroker(BrokerId);

                List<String> newLicense = new List<String>();
                newLicense.Add(row[1].ToString());
                newLicense.Add(row[2].ToString());
                newLicense.Add(row[3].ToString());
                if (licenseList.ContainsKey(row[0].ToString()))
                {
                    licenseList[row[0].ToString()].Add(newLicense);
                }
                else
                {
                    List<List<String>> nList = new List<List<String>>();
                    nList.Add(newLicense);
                    licenseList.Add(row[0].ToString(), nList);
                }               
                //And then save it later 
                //(after all, we don' want to save anything 
                //if it turns out the CSV file is wrong)
                //toSave.AddLast(pb);
            }
            //We want to return whether or not parsing was successful, so were there no errors?
            return !errord;
        }
        private void loadUser()
        {            
            dict.Add("dut", new Guid("a1be81a6-48dd-4d70-9b93-e087e4cc6cbf"));
            dict.Add("vamshim", new Guid("aa4b4906-4f99-4659-82d9-90d1ad057200"));            
        }
    }
}
