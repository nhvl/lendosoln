﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DataAccess;
using System.Data.Common;
using System.Data.SqlClient;
using System.Reflection;
using System.IO;
using LendersOffice.Common.TextImport;
using LendersOffice.Security;
using System.Text.RegularExpressions;
using CommonProjectLib.Common.Lib;
using System.Diagnostics;
using LendersOffice.ObjLib.Task;

namespace LendersOffice.BrokerAdmin.Importers
{
    public abstract class AbstractImporter
    {
        public string HeaderText { get; protected set; }
        public string SheetName { get; protected set; }

        #region Internal classes
        protected class ShortBranchInfo
        {
            public Guid Id;
            public string CanonicalName;
            public string DisplayName;
            public Guid DefaultPriceGroup;
        }

        /// <summary>
        /// Keeps track of only the employee info we need
        /// </summary>
        protected class ShortEmployeeInfo
        {
            public bool IsActive;
            public Guid Id;
            public string Login;
            public string FullName;
        }

        /// <summary>
        /// Keeps track of only the pricing group info we need
        /// </summary>
        protected class ShortPricingGroupInfo
        {
            public Guid Id;
            public string Name;
        }
        #endregion


        /// <summary>
        /// Imports a given csv string using the format appropriate for the concrete implementation
        /// </summary>
        /// <param name="csv">The csv string</param>
        /// <param name="BrokerId">The broker for whom to import</param>
        /// <param name="Results">A textual representation of the results (it's passed as an out parameter in case of exceptions being thrown)</param>
        /// <param name="ErrorList">A table of errors that were encountered during importing</param>
        public abstract void Import(string csv, Guid BrokerId, out string Results, out DataTable ErrorList);
    }
}
