﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Common.TextImport;
using LendersOffice.ObjLib.Task;

namespace LendersOffice.BrokerAdmin.Importers
{
    public class ConditionChoiceImporter : AbstractImporter
    {
        private enum ConditionChoiceImporterColumns
        {
            Category,
            ConditionType,
            LoanType,
            Subject,
            ToBeAssignedTo,
            ToBeOwnedBy,
            DueDateFieldName,
            AddNumDaysToDueDate,
            CategoryDefaultPermissionLevel,
            RequiredDocType,
            IsHidden
        }

        private const string _sheetName = "Import Condition Choices";

        /// <summary>
        /// A regular expression used to remove any separators between
        /// strings corresponding to the names of enumeration constants.
        /// </summary>
        private const string WordSeparatorRegex = @"[\s-\\\/]+";

        // The first reqColumns are required - errors will be thrown if they're empty.
        private const int reqColumns = 8;

        // Configurable number of columns in case we add more later.
        private readonly int numColumns;

        public ConditionChoiceImporter()
        {
            SheetName = _sheetName;

            var columns = Enum.GetNames(typeof(ConditionChoiceImporterColumns));

            HeaderText = string.Join(", ", columns);

            numColumns = columns.Length;
        }

        private string GetSavedChoices(IEnumerable<ConditionChoice> saved)
        {
            if (saved.Any() == false)
            {
                return "No conditions were saved\n\n";
            }

            StringBuilder sb = new StringBuilder("The following condition choices were successfully saved:\n");
            foreach (ConditionChoice choice in saved)
            {
                sb.AppendLine(choice.ConditionSubject);
            }

            return sb.ToString();
        }

        private string GetSavedCategories(IEnumerable<ConditionCategory> saved)
        {
            if (saved.Any() == false)
            {
                return "No new categories were saved\n\n";
            }

            StringBuilder sb = new StringBuilder("The following condition categories were successfully saved:\n");
            foreach (ConditionCategory cat in saved)
            {
                sb.AppendLine(cat.Category);
            }

            return sb.ToString();
        }

        public override void Import(string csv, Guid BrokerId, out string Results, out DataTable ErrorList)
        {
            Results = string.Empty;
            ErrorList = new DataTable();
            try
            {
                LinkedList<ConditionChoice> newChoices;
                LinkedList<ConditionCategory> newCategories;

                bool parseSuccess = ParseCSV(csv, BrokerId, out newChoices, out newCategories, out ErrorList);

                if (parseSuccess)
                {
                    //We parsed everything, so go ahead and save it all

                    //Categories need to be saved first so they can have IDs when we go to save 
                    foreach (ConditionCategory category in newCategories)
                    {
                        try
                        {
                            category.Save();
                        }
                        catch (SqlException e)
                        {
                            throw new CBaseException(
                                "Could not save Condition Category: '" + category.Category + "'! This is probably a db problem. " +
                                "All previous Condition Categories were saved, but no further imports will be made. Saved categories: " +
                                string.Join(", ", newCategories.TakeWhile((a) => a.Id != category.Id).Select(cat => cat.Category)), 
                                e);
                        }
                        catch (Exception e)
                        {
                            throw new CBaseException(
                                "Could not save Condition Category: '" + category.Category + "'! This is a completely unexpected error. " +
                                "All previous Condition Categories were saved, but no further imports will be made. Saved categories: " +
                                string.Join(", ", newCategories.TakeWhile((a) => a.Id != category.Id).Select(cat => cat.Category)),
                                e);
                        }
                    }

                    Results += GetSavedCategories(newCategories) + "\n\n";

                    using (CStoredProcedureExec sproc = new CStoredProcedureExec(BrokerId))
                    {
                        sproc.BeginTransactionForWrite();
                        foreach (ConditionChoice choice in newChoices)
                        {
                            try
                            {
                                choice.Save(choice.BrokerId, sproc);
                            }
                            catch (SqlException e)
                            {
                                sproc.RollbackTransaction();

                                throw new CBaseException(
                                    "Could not save Condition Choice: '" + choice.ConditionSubject + "'! This is probably due to a db problem. " +
                                    "No Condition Choices were saved.",
                                    e);
                            }
                            catch (Exception e)
                            {
                                sproc.RollbackTransaction();

                                throw new CBaseException(
                                    "Could not save Condition Choice: '" + choice.ConditionSubject + "'! This is a totally unexpected error. " +
                                    "No Condition Choices were saved.",
                                    e);
                            }
                        }
                        sproc.CommitTransaction();
                    }
                    //If we get here, there were no issues saving.
                    Results += GetSavedChoices(newChoices);
                }
                else
                {
                    // Put the original file content back on the screen so the user can determine 
                    // what the error was.
                    Results = csv;
                }
            }
            catch (Exception e)
            {
                Tools.LogError("Failed to import condition choices: " + e.Message + ".", e);
                throw;
            }
        }

        /// <summary>
        /// Parse the (hopefully) CSV file in the import box
        /// </summary>
        /// <param name="newChoices">A list of choices that need to be saved</param>
        /// <param name="newCategories">A list of categories that need to be saved</param>
        /// <param name="errorList">Errors that occurred during parsing</param>
        /// <returns>True if parsing was successful, false if not</returns>
        private bool ParseCSV(string csv, Guid BrokerId, out LinkedList<ConditionChoice> newChoices, out LinkedList<ConditionCategory> newCategories, out DataTable errorList)
        {
            newChoices = new LinkedList<ConditionChoice>();
            newCategories = new LinkedList<ConditionCategory>();
            errorList = new DataTable();

            errorList.Columns.Add(new DataColumn("Line", typeof(int)));
            errorList.Columns.Add(new DataColumn("Error", typeof(string)));
            errorList.DefaultView.Sort = "Line asc";

            //Get the list of roles so we can look them up later
            var roles = Tools.GetRolesExcluding("Consumer", "Administrator", "Accountant").ToArray();

            //Get the categories we have available right now, so we know when we need to add one
            IEnumerable<ConditionCategory> validCategories = ConditionCategory.GetCategories(BrokerId);

            //We also need the list of available field names (parameters)
            var validFields = Task.ListValidDateFieldParameters();

            var validConditionTypes = string.Join(", ", Enum.GetNames(typeof(E_ConditionType)));

            var validConditionLoanTypes = string.Join(", ", Enum.GetNames(typeof(E_ConditionChoice_sLT)));

            //And a list of valid permission levels (cache them into a list, since we'll be looking over this a lot)
            var validPermissionLevels = (from perm in PermissionLevel.RetrieveAll(BrokerId)
                                         where perm.IsAppliesToConditions
                                         select perm).ToList<PermissionLevel>();

            //ILookups are nice, pity nothing really implements them except ToLookup
            ILookup<int, string> parseErrors;

            //Pull the csv file into a data table
            DataTable data = DelimitedListParser.CsvToDataTable(csv, true, reqColumns, numColumns, out parseErrors);

            //Don't dump the parse errors into the error list right now, though - we want all the errors to show up in line order.

            //However, since we don't touch the header row and that might have generated errors, add them now.
            int line = 1;
            if (parseErrors.Contains(line))
            {
                foreach (string err in parseErrors[line])
                {
                    errorList.Rows.Add(err);
                }
            }

            for (int i = 0; i < data.Rows.Count; i++)
            {
                DataRow row = data.Rows[i];
                //We start on line 2 because there was a header row
                line++;

                //Did this line check out in parsing?
                if (parseErrors.Contains(line))
                {
                    foreach (string err in parseErrors[line])
                    {
                        errorList.Rows.Add(line, err);
                    }
                }

                ConditionCategory cat;

                #region Figuring out the category

                string reqCatName = row[(int)ConditionChoiceImporterColumns.Category].ToString();

                //I need to special case this name because uncreated conditions have id = -1
                //and I need to search through them later.
                if (reqCatName.Equals("-1"))
                {
                    errorList.Rows.Add( 
                        line, 
                        "-1 is not a valid category name or ID for importing. " +
                        "Please add this category manually if -1 is desired.");

                    // We won't be able to assign to any ConditionCategory
                    // object in this case, we'll continue.
                    continue;
                }

                var matchingCategories = DataParsing.GetByIdOrName(validCategories.Union(newCategories), reqCatName, (a) => a.Category, (a) => a.Id);

                var matchingCategoriesCount = matchingCategories.Count();

                if (matchingCategoriesCount > 1)
                {
                    errorList.Rows.Add( 
                        line, 
                        "Multiple matches for category '" + row[(int)ConditionChoiceImporterColumns.Category] + "' found: " +
                        string.Join(", ", matchingCategories.Select(c => c.Category)) + ". Try using the category's ID instead. " +
                        "If that doesn't work, something is broken.");

                    // We won't be able to assign to any ConditionCategory
                    // object because we don't know which category to pick,
                    // so we'll continue.
                    continue;
                }
                else if (matchingCategoriesCount == 0)
                {
                    //This category doesn't exist, so we'll have to create it.
                    cat = new ConditionCategory(BrokerId);
                    cat.Category = row[(int)ConditionChoiceImporterColumns.Category].ToString();

                    string reqPermLevelName = row[(int)ConditionChoiceImporterColumns.CategoryDefaultPermissionLevel].ToString();

                    //Get its permission level
                    var permissionLevel = DataParsing.GetByIdOrName(validPermissionLevels, reqPermLevelName, (a) => a.Name, (a) => a.Id);

                    var permissionLevelCount = permissionLevel.Count();

                    if (permissionLevelCount > 1)
                    {
                        errorList.Rows.Add( 
                            line, 
                            "Multiple matches for permission '" + reqPermLevelName + "' found: " +
                            string.Join(", ", permissionLevel.Select(perm => perm.Name)) +
                            ". Try using the permission's ID instead. If that doesn't work, something is broken.");
                    }
                    else if (permissionLevelCount == 0)
                    {
                        errorList.Rows.Add( 
                            line, 
                            "Permission '" + reqPermLevelName + "' could not be found. Make sure the permission level applies " +
                            "to conditions, or try using the permission level ID. If that doesn't work, something is broken.");
                    }
                    else
                    {
                        cat.DefaultTaskPermissionLevelId = permissionLevel.FirstOrDefault().Id;
                    }

                    if ((errorList.Rows.Count > 0 || parseErrors.Any()) == false)
                    {
                        newCategories.AddLast(cat);
                    }
                }
                else
                {
                    cat = matchingCategories.FirstOrDefault();

                    var specifiedDefaultPermission = row[(int)ConditionChoiceImporterColumns.CategoryDefaultPermissionLevel].ToString();

                    if (string.IsNullOrEmpty(specifiedDefaultPermission) == false)
                    {
                        //Make sure they didn't provide a default permission level id, if this category already exists
                        errorList.Rows.Add( 
                            line, 
                            "A default permission level was specified for category '" + cat.Category + "', but that category already exists. " +
                            "Remove the default permission ID '" + specifiedDefaultPermission + "' to continue.");
                    }
                }
                #endregion

                ConditionChoice choice = new ConditionChoice(BrokerId, cat);

                E_ConditionType conditionType;

                var importConditionType = Regex.Replace(
                    row[(int)ConditionChoiceImporterColumns.ConditionType].ToString(),
                    ConditionChoiceImporter.WordSeparatorRegex,
                    string.Empty);

                if (Enum.TryParse(importConditionType, out conditionType))
                {
                    choice.ConditionType = conditionType;
                }
                else
                {
                    errorList.Rows.Add( 
                        line, 
                        "Condition type could not be parsed from '" + importConditionType + "'. " +
                        "The following condition type names are supported: " + validConditionTypes);
                }

                E_ConditionChoice_sLT conditionLoanType;

                var importLoanType = Regex.Replace(
                    row[(int)ConditionChoiceImporterColumns.LoanType].ToString(),
                    ConditionChoiceImporter.WordSeparatorRegex,
                    string.Empty);

                if (Enum.TryParse(importLoanType, out conditionLoanType))
                {
                    choice.sLT = conditionLoanType;
                }
                else
                {
                    errorList.Rows.Add( 
                        line, 
                        "Loan type could not be parsed from '" + importLoanType + "'. " +
                        "The following condition loan types are supported: " + validConditionLoanTypes);
                }

                //The only simple assignment
                choice.ConditionSubject = row[(int)ConditionChoiceImporterColumns.Subject].ToString();

                //We'll just use first here, since there should never be two roles with the same name
                choice.ToBeAssignedRoleId = (from r in roles where r.Value == row[(int)ConditionChoiceImporterColumns.ToBeAssignedTo].ToString() select r.Key).FirstOrDefault();
                if (choice.ToBeAssignedRoleId == Guid.Empty)
                {
                    errorList.Rows.Add( 
                        line, 
                        "Role to be assigned by could not be parsed from '" + row[(int)ConditionChoiceImporterColumns.ToBeAssignedTo] + "'. " +
                        "Try putting a space between the words?");

                }

                choice.ToBeOwnedByRoleId = (from r in roles where r.Value == row[(int)ConditionChoiceImporterColumns.ToBeOwnedBy].ToString() select r.Key).FirstOrDefault();
                if (choice.ToBeOwnedByRoleId == Guid.Empty)
                {
                    errorList.Rows.Add( 
                        line, 
                        "Role to be owned by could not be parsed from '" + row[(int)ConditionChoiceImporterColumns.ToBeOwnedBy] + "'. " +
                        "Try putting a space between the words?");
                }

                string reqFieldName = row[(int)ConditionChoiceImporterColumns.DueDateFieldName].ToString();
                string fieldId;

                #region Figuring out the due date field id

                var dueDates = DataParsing.GetByIdOrName(validFields, reqFieldName, (a) => a.FriendlyName, (a) => a.Id);

                var dueDatesCount = dueDates.Count();

                if (dueDatesCount > 1)
                {
                    errorList.Rows.Add( 
                        line, 
                        "Multiple matches for due date field '" + reqFieldName + "' found: " +
                        string.Join(", ", dueDates.Select(date => date.FriendlyName)) +
                        ". Try using the field's ID instead. If that doesn't work, something is broken.");

                    fieldId = "Ambiguous";
                }

                else if (dueDatesCount == 0)
                {
                    errorList.Rows.Add(line, "'" + reqFieldName + "' was not found as either a field ID or a field name.");
                    fieldId = "None";
                }

                else
                {
                    fieldId = dueDates.First().Id;
                }

                #endregion

                //Yes, despite the name this wants the field's id, not its name.
                choice.DueDateFieldName = fieldId;

                int dueDateAdd;
                if (int.TryParse(row[(int)ConditionChoiceImporterColumns.AddNumDaysToDueDate].ToString(), out dueDateAdd))
                {
                    choice.DueDateAddition = dueDateAdd;
                }
                else
                {
                    choice.DueDateAddition = 0;
                }

                var importRequiredDocTypeName = row[(int)ConditionChoiceImporterColumns.RequiredDocType].ToString();

                if (string.IsNullOrWhiteSpace(importRequiredDocTypeName) == false)
                {
                    var matchingBrokerDocType = EDocs.EDocumentDocType.GetDocTypesByBroker(BrokerId, EnforcePermissions: false).FirstOrDefault(brokerDocType =>
                        brokerDocType.DocTypeName.Equals(importRequiredDocTypeName, StringComparison.OrdinalIgnoreCase));

                    if (matchingBrokerDocType == null)
                    {
                        errorList.Rows.Add(line, "Could not find doc type with name '" + importRequiredDocTypeName + "'");
                    }
                    else
                    {
                        choice.RequiredDocTypeId = Convert.ToInt32(matchingBrokerDocType.DocTypeId);
                    }
                }

                var importIsHidden = row[(int)ConditionChoiceImporterColumns.IsHidden].ToString();

                if (string.IsNullOrWhiteSpace(importIsHidden) == false)
                {
                    switch(importIsHidden.ToLower())
                    {
                        case "yes":
                            choice.IsHidden = true;
                            break;
                        case "no":
                            choice.IsHidden = false;
                            break;
                        default:
                            errorList.Rows.Add(line, "'" + importIsHidden + "' is not a recognized value of 'Yes' or 'No'.");
                            break;
                    }
                }

                //And then save it later 
                //(after all, we don't want to save anything 
                //if it turns out the CSV file is wrong.
                if ((errorList.Rows.Count > 0 || parseErrors.Any()) == false)
                {
                    newChoices.AddLast(choice);
                }
            }

            //We want to return true if parsing were successful, so were there more than 0 errors or any parse errors?
            //(turns out that Rows doesn't have a .Any method :( )
            return (errorList.Rows.Count > 0 || parseErrors.Any()) == false;
        }
    }
}
