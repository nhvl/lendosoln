﻿// <summary>
//    Author: Michael Leinweaver
//    Date:   10/29/2015
// </summary>
namespace LendersOffice.BrokerAdmin.Importers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using EDocs;

    /// <summary>
    /// Represents an imported shipping template, including the template's name 
    /// and associated folder and document types.
    /// </summary>
    internal class ImportedShippingTemplate
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ImportedShippingTemplate"/> class with 
        /// the given <paramref name="name"/>.
        /// </summary>
        /// <param name="name">The name of the shipping template.</param>
        public ImportedShippingTemplate(string name)
        {
            this.Name = name;

            this.DocTypes = new List<ImportedDocType>();
        }

        /// <summary>
        /// Gets or sets the name of the shipping template.
        /// </summary>
        /// <value>The name of the shipping template.</value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this shipping template is new.
        /// </summary>
        /// <value>A boolean indicating if this shipping template is new.</value>
        public bool IsNew { get; set; }

        /// <summary>
        /// Gets or sets the list of associated document types.
        /// </summary>
        /// <value>The list of associated document types.</value>
        public List<ImportedDocType> DocTypes { get; set; }
    }
}
