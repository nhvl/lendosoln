﻿using System;
using LendersOffice.Admin;

namespace LendersOffice.BrokerAdmin.Importers
{
    public class ImportField
    {
        public string FieldId { get; set; }
        public string Name { get; set; }
        public bool CanMatch { get; set; }
        public bool CanBeDestination { get; set; } = true;

        public ImportFieldType DataType { get; set; }
        
        public int MaxLength { get; set; }

        [System.Web.Script.Serialization.ScriptIgnore]
        public Func<string,ImporterDataCache,bool> ValidateInputData { set; get; }

        [System.Web.Script.Serialization.ScriptIgnore]
        public Action<PmlBroker,string,ImporterDataCache> SetBrokerData { set; get; }

        [System.Web.Script.Serialization.ScriptIgnore]
        public Action<LicenseInfo, string> SetLicenseInfo { get; set; }

        public bool IsRequired { get; set; }

        public enum ImportFieldType
        {
            Guid = 0,
            String = 1,
            Bool = 2,
            Decimal = 3,
            Int = 4,
            DateTime = 5,
            OCStatusType = 6,
            PercentBase = 7,
            UnderwritingAuthority = 8
        }
    }
}
