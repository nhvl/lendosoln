﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataAccess;
using System.Data;
using LendersOffice.Admin;
using LendersOffice.ObjLib.TPO;
using LendersOffice.ObjLib.PriceGroups;
using LendersOffice.ObjLib.UI.Themes;

namespace LendersOffice.BrokerAdmin.Importers
{
    // We need a class of Lazy-loaded dictionaries for
    // both pre-validation and id mapping for assigment.
    // this can be passed into setters who need it.
    // What about putting them in the base class?

    // Simple helper class
    public class ImporterDataCache
    {
        private Lazy<Dictionary<string, Guid>> priceGroupCache;
        private Lazy<Dictionary<string, Guid>> branchCache;
        private Lazy<Dictionary<string, Guid>> groupCache;
        private Lazy<Dictionary<string, Guid>> tpoPortalCache;
        private Lazy<Dictionary<string, Guid>> tpoColorThemeCache;
        private Lazy<Dictionary<string, int>> companyTierCache;


        public Dictionary<string, Guid> PriceGroups
        {
            get { return priceGroupCache.Value; }
        }

        public Dictionary<string, Guid> Branches
        {
            get { return branchCache.Value; }
        }

        public Dictionary<string, Guid> Groups
        {
            get { return groupCache.Value; }
        }

        public Dictionary<string, Guid> TpoPortals
        {
            get { return tpoPortalCache.Value; }
        }
        public Dictionary<string, Guid> TpoColorThemes
        {
            get { return tpoColorThemeCache.Value; }
        }

        public Dictionary<string, int> CompanyTiers
        {
            get { return companyTierCache.Value; }
        }

        private ImporterDataCache()
        { }

        public ImporterDataCache(Guid brokerId)
        {
            priceGroupCache = new Lazy<Dictionary<string, Guid>>(() => {
                return PriceGroup.RetrieveAllFromBroker(brokerId)
                     .ToDictionary(pg => pg.Name, pg => pg.ID);
            });

            branchCache = new Lazy<Dictionary<string, Guid>>(() => {
                return BranchDB.GetBranchObjects(brokerId)
                     .ToDictionary(b => b.Name, b => b.BranchID);
            });

            groupCache = new Lazy<Dictionary<string, Guid>>(() => {
                return GroupDB.GetAllGroupIdsAndNames(brokerId, GroupType.PmlBroker)
                    .ToDictionary(g => g.Item2, g => g.Item1);
            });

            tpoPortalCache = new Lazy<Dictionary<string, Guid>>(() => {
                return TPOPortalConfig.RetrieveAllActiveAtBroker(brokerId)
                    .ToDictionary(p => p.Name, p => p.ID.Value);
            });

            tpoColorThemeCache = new Lazy<Dictionary<string, Guid>>(() => {
                var themeCollection = new ThemeCollection(brokerId, ThemeCollectionType.TpoPortal);
                themeCollection.Retrieve();
                return themeCollection.GetThemes().ToDictionary(t => t.Name, t => t.Id);
            });

            companyTierCache = new Lazy<Dictionary<string, int>>(() => {
                return BrokerDB.RetrieveById(brokerId).PmlCompanyTiers.List.ToDictionary(tier => tier.Name, tier => tier.Id);
            });

        }
    }
}
