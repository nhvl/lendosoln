﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;
using System.Data;
using LendersOffice.Admin;
using CommonProjectLib.Common.Lib;
using LendersOffice.Common;
using LqbGrammar.DataTypes;
using System.Text.RegularExpressions;
using LendersOffice.Security;

namespace LendersOffice.BrokerAdmin.Importers
{
    public class OriginatingCompanyImportDestination : AvailableImportDestination
    {
        public override string Name
        {
            get { return "OriginatingCompanies"; }
        }

        protected override string RecordSPName
        {
            get { return "ListAllPmlBrokerByBrokerId"; }
        }

        protected override string PrimaryKey
        {
            get { return "PmlBrokerId"; }
        }


        private Dictionary<String, Guid> branchNameToId;
        private void SetBranch(PmlBroker broker, string branchName, Action<Guid> set)
        {
            if (this.branchNameToId == null)
            {
                branchNameToId = new Dictionary<string, Guid>(StringComparer.OrdinalIgnoreCase);
                foreach (var item in BranchDB.GetBranchObjects(broker.BrokerId))
                {
                    branchNameToId.Add(item.Name, item.BranchID);
                }
            }

            if (branchNameToId.ContainsKey(branchName))
            {
                set(branchNameToId[branchName]);
            }
            else
            {
                Tools.LogError("Could not find branch " + branchName + " for OC company " + broker.Name);
            }


        }

        private void SetIfExists(string value, Action<string> set)
        {
            if (!String.IsNullOrEmpty(value))
            {
                set(value);
            }
        }

        private bool ValidateCompanyRoles(string data)
        {
            HashSet<E_OCRoles> rolesList;
            return ProcessCompanyRoles(data, out rolesList);
        }

        internal static bool IsValidPhone(string value)
        {
            // Use our LQB default Phone number regex, but PDE also wants to
            // force this to be closer to what the UI would accept, which is
            // in the format: '(###) ###-####???????'.

            //if ( !PhoneNumber.Create(value).HasValue )
            //{
            //    return false;
            //}

            if(value == string.Empty)
            {
                // Our UIs generally allow blank phones.  The Requried validation
                // happens at a higher level.
                return true;
            }
            
            var regex = new Regex(@"^\([0-9]{3}\)\s[0-9]{3}[-][0-9]{4}[-a-z0-9. ]*$");
            return regex.IsMatch(value);
        }

        private bool ProcessCompanyRoles(string data, out HashSet<E_OCRoles> rolesList)
        {
            {
                rolesList = new HashSet<E_OCRoles>();
                if (String.IsNullOrEmpty(data))
                {
                    return false;
                }

                foreach (string role in data.Replace("[", string.Empty).Replace("]", string.Empty).Split(','))
                {
                    if (!Enum.IsDefined(typeof(E_OCRoles), role))
                    {
                        return false;
                    }

                    rolesList.Add((E_OCRoles)Enum.Parse(typeof(E_OCRoles), role));

                }

                // UI forces at least 1 choice.  Empty role list not allowed.
                return rolesList.Count > 0;
            }

        }

        private bool PopulatePmlBrokerWithImportData(PmlBroker broker, ColumnMapping[] mapping, DataRow row, List<string> errorList)
        {
            bool importLicense = false;
            LicenseInfo li = new LicenseInfo();

            // The mapping of PriceGroups, Branches, etc.
            var dataCache = new ImporterDataCache(broker.BrokerId);

            bool hasErrors = false;

            foreach (ColumnMapping column in mapping)
            {
                string data = row[column.ColumnIndex].ToString();
                var importField = column.Field;

                if (importField.CanBeDestination)
                {
                    if ( data.Equals( NO_CHANGE_INDICATOR, StringComparison.CurrentCultureIgnoreCase ))
                    {
                        continue;
                    }

                    try
                    {
                        if (importField.SetBrokerData != null)
                        {
                            importField.SetBrokerData.Invoke(broker, data, dataCache);
                        }
                        
                        if (importField.SetLicenseInfo != null)
                        {
                            importField.SetLicenseInfo.Invoke(li, data);
                            importLicense = true;
                        }
                    }
                    catch (CBaseException exc)
                    {
                        // Some fields throw in their setters.  Report the error and 
                        // do not allow the save.
                        errorList.Add(exc.Message);
                        hasErrors = true;
                    }
                }
            }

            if (importLicense && !hasErrors)
            {
                broker.LicenseInfoList.Clear();
                broker.LicenseInfoList.Add(li);
            }

            return !hasErrors;
        }

        public override bool AddNewRow(Guid brokerId, ColumnMapping[] mapping, DataRow row, List<string> errorList)
        {
            //todo need to implement adding to group membership
            PmlBroker broker = PmlBroker.CreatePmlBroker(brokerId);
            broker.IsActive = true;

            if (PopulatePmlBrokerWithImportData(broker, mapping, row, errorList))
            {
                try
                {
                    broker.Save();
                    return true;
                }
                catch (CBaseException exc)
                {
                    // Let this error float up to UI for handle.
                    errorList.Add("Failed to save " + broker.Name + ": " + exc.DeveloperMessage);
                }
            }
            return false;
        }

        private IEnumerable<Tuple<Guid, string>> m_members = null;

        public override bool ImportRow(Guid brokerId, string key, ColumnMapping[] mapping, DataRow row, List<string> errorList, AbstractUserPrincipal principal)
        {
            PmlBroker broker = PmlBroker.RetrievePmlBrokerById(new Guid(key), brokerId);

            if (PopulatePmlBrokerWithImportData(broker, mapping, row, errorList))
            {
                try
                {
                    broker.Save();
                }
                catch (CBaseException exc)
                {
                    // Let this error float up to UI for handle.
                    errorList.Add("Failed to save " + broker.Name + ": " + exc.DeveloperMessage);
                    return false;
                }

                if (!string.IsNullOrEmpty(broker.GroupIdList_ImporterUse))
                {
                    if (m_members == null)
                    {
                        m_members = GroupDB.GetAllGroupIdsAndNames(brokerId, GroupType.PmlBroker);
                    }

                    StringBuilder sb = new StringBuilder();
                    foreach (var name in broker.GroupIdList_ImporterUse.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries))
                    {
                        if (string.IsNullOrEmpty(name))
                        {
                            continue;
                        }

                        var item = m_members.FirstOrDefault(p => p.Item2.Equals(name, StringComparison.OrdinalIgnoreCase));
                        if (item != null)
                        {
                            sb.Append(String.Concat(item.Item1, ","));
                        }
                    }
                    if (sb.Length > 0)
                    {
                        sb.Remove(sb.Length - 1, 1);
                        GroupDB.UpdatePmlBrokerGroups(brokerId, broker.PmlBrokerId, sb.ToString());
                    }
                }
                return true;
            }

            return false;
        }

        public override ImportField[] AvailableFields
        {
            get
            {
                return new ImportField[] {
                    new ImportField() {
                        FieldId = "PmlBrokerId",
                        Name = "PmlBrokerId",
                        CanMatch = true,
                        CanBeDestination = false,
                        SetBrokerData = (broker, data, dataCache) => { /* No-op*/ },
                        DataType = ImportField.ImportFieldType.Guid
                    },

                    new ImportField() {
                         FieldId = "IsActive",
                         Name = "IsActive",
                         CanMatch = false,
                         DataType = ImportField.ImportFieldType.Bool,
                         IsRequired = true,
                         SetBrokerData = (broker, data, dataCache) => { broker.IsActive = bool.Parse(data); }
                        },

                    new ImportField() {
                         FieldId = "BrokerStatus",
                         Name = "Broker Status",
                         CanMatch = false,
                         DataType = ImportField.ImportFieldType.OCStatusType,
                         IsRequired = false,
                         SetBrokerData = (broker, data, dataCache) => { broker.BrokerRoleStatusT = (OCStatusType)Enum.Parse(typeof(OCStatusType), data); }
                        },

                    new ImportField() {
                         FieldId = "MiniCorrStatus",
                         Name = "Mini Corr Status",
                         CanMatch = false,
                         DataType = ImportField.ImportFieldType.OCStatusType,
                         IsRequired = false,
                         SetBrokerData = (broker, data, dataCache) => { broker.MiniCorrRoleStatusT = (OCStatusType)Enum.Parse(typeof(OCStatusType), data); }
                        },

                    new ImportField() {
                         FieldId = "CorrStatus",
                         Name = "Corr Status",
                         CanMatch = false,
                         DataType = ImportField.ImportFieldType.OCStatusType,
                         IsRequired = false,
                         SetBrokerData = (broker, data, dataCache) => { broker.CorrRoleStatusT = (OCStatusType)Enum.Parse(typeof(OCStatusType), data); }
                        },

                    new ImportField() {
                         FieldId = "Name",
                         Name = "Name",
                         CanMatch = true,
                         MaxLength = 100,
                         DataType = ImportField.ImportFieldType.String,
                         IsRequired = true,
                         SetBrokerData = (broker, data, dataCache) => { broker.Name = data; }
                        },

                    new ImportField() {
                         FieldId = "Addr",
                         Name = "Address",
                         CanMatch = true,
                         MaxLength = 60,
                         DataType= ImportField.ImportFieldType.String,
                         IsRequired = true,
                         SetBrokerData = (broker, data, dataCache) => { broker.Addr = data; }
                        },

                    new ImportField() {
                         FieldId = "City",
                         Name = "City",
                         CanMatch = true,
                         MaxLength = 36,
                         DataType= ImportField.ImportFieldType.String,
                         IsRequired = true,
                         SetBrokerData = (broker, data, dataCache) => { broker.City = data; }
                        },

                    new ImportField() {
                         FieldId = "State",
                         Name = "State",
                         CanMatch = true,
                         MaxLength = 2,
                         DataType= ImportField.ImportFieldType.String,
                         ValidateInputData = (value, dataCache) => { return UnitedStatesState.CreateWithValidation(value).HasValue; },
                         IsRequired = true,
                         SetBrokerData = (broker, data, dataCache) => { broker.State = data; }
                        },

                    new ImportField() {
                         FieldId = "Zip",
                         Name = "Zip",
                         CanMatch = true,
                         MaxLength = 5,
                         DataType= ImportField.ImportFieldType.String,
                         ValidateInputData = (value, dataCache) => { return Zipcode.CreateWithValidation(value).HasValue; },
                         IsRequired = true,
                         SetBrokerData = (broker, data, dataCache) => { broker.Zip = data; }
                        },

                    new ImportField() {
                         FieldId = "Phone",
                         Name = "Phone",
                         CanMatch = true,
                         MaxLength= 21,
                         DataType= ImportField.ImportFieldType.String,
                         ValidateInputData = (value, dataCache) => { return IsValidPhone(value); },
                         IsRequired = true,
                         SetBrokerData = (broker, data, dataCache) => { broker.Phone = data; }
                        },

                    new ImportField() {
                         FieldId = "Fax",
                         Name = "Fax",
                         CanMatch = true,
                         MaxLength= 21,
                         DataType= ImportField.ImportFieldType.String,
                         ValidateInputData = (value, dataCache) => { return IsValidPhone(value); },
                         IsRequired = false,
                         SetBrokerData = (broker, data, dataCache) => { broker.Fax = data; }
                        },

                    new ImportField() {
                         FieldId = "CompanyId",
                         Name = "CompanyId",
                         CanMatch = true,
                         MaxLength = 36,
                         DataType= ImportField.ImportFieldType.String,
                         IsRequired = false,
                         SetBrokerData = (broker, data, dataCache) => { broker.CompanyId = data; }
                        },

                    new ImportField() {
                         FieldId = "TaxId",
                         Name = "TaxId",
                         CanMatch = true,
                         MaxLength = 36,
                         DataType= ImportField.ImportFieldType.String,
                         ValidateInputData = (value, dataCache) => { return EmployerTaxIdentificationNumber.Create(value).HasValue; },
                         IsRequired = false,
                         SetBrokerData = (broker, data, dataCache) => { broker.TaxId = data; }
                        },

                    new ImportField() {
                         FieldId = "PrincipalName1",
                         Name = "PrincipalName1",
                         CanMatch = true,
                         MaxLength = 100,
                         DataType= ImportField.ImportFieldType.String,
                         IsRequired = false,
                         SetBrokerData = (broker, data, dataCache) => { broker.PrincipalName1 = data; }
                        },

                    new ImportField() {
                         FieldId = "PrincipalName2",
                         Name = "PrincipalName2",
                         CanMatch = true,
                         MaxLength = 100,
                         DataType= ImportField.ImportFieldType.String,
                         IsRequired = false,
                         SetBrokerData = (broker, data, dataCache) => { broker.PrincipalName2 = data; }
                        },

                    new ImportField() {
                         FieldId = "NmLsIdentifier",
                         Name = "NmLsIdentifier",
                         CanMatch = true,
                         MaxLength = 50,
                         DataType= ImportField.ImportFieldType.String,
                         IsRequired = false,
                         SetBrokerData = (broker, data, dataCache) => { broker.NmLsIdentifier = data; }
                        },

                    new ImportField() {
                         FieldId = "CustomPricingPolicyField1",
                         Name = "CustomPricingPolicyField1",
                         CanMatch = false,
                         MaxLength = 25,
                         DataType= ImportField.ImportFieldType.String,
                         IsRequired = false,
                         SetBrokerData = (broker, data, dataCache) => { broker.CustomPricingPolicyField1 = data; }
                        },

                    new ImportField() {
                         FieldId = "CustomPricingPolicyField2",
                         Name = "CustomPricingPolicyField2",
                         CanMatch = false,
                         MaxLength = 25,
                         DataType= ImportField.ImportFieldType.String,
                         IsRequired = false,
                         SetBrokerData = (broker, data, dataCache) => { broker.CustomPricingPolicyField2 = data; }
                        },

                    new ImportField() {
                         FieldId = "CustomPricingPolicyField3",
                         Name = "CustomPricingPolicyField3",
                         CanMatch = false,
                         MaxLength = 25,
                         DataType= ImportField.ImportFieldType.String,
                         IsRequired = false,
                         SetBrokerData = (broker, data, dataCache) => { broker.CustomPricingPolicyField3 = data; }
                        },

                    new ImportField() {
                         FieldId = "CustomPricingPolicyField4",
                         Name = "CustomPricingPolicyField4",
                         CanMatch = false,
                         MaxLength = 25,
                         DataType= ImportField.ImportFieldType.String,
                         IsRequired = false,
                         SetBrokerData = (broker, data, dataCache) => { broker.CustomPricingPolicyField4 = data; }
                        },

                    new ImportField() {
                         FieldId = "CustomPricingPolicyField5",
                         Name = "CustomPricingPolicyField5",
                         CanMatch = false,
                         MaxLength = 25,
                         DataType= ImportField.ImportFieldType.String,
                         IsRequired = false,
                         SetBrokerData = (broker, data, dataCache) => { broker.CustomPricingPolicyField5 = data; }
                        },

                    new ImportField() {
                         FieldId = "TPOPortalConfigID",
                         Name = "Broker Landing Page Id",
                         CanMatch = false,
                         DataType = ImportField.ImportFieldType.Guid,
                         ValidateInputData = (data, dataCache) => { return dataCache.TpoPortals.ContainsValue(Guid.Parse(data)); },
                         IsRequired = false,
                         SetBrokerData = (broker, data, dataCache) => { broker.TPOPortalConfigID = Guid.Parse(data); }
                        },

                      new ImportField() {
                         FieldId = "TPOPortalConfigNm",
                         Name = "Broker Landing Page Name",
                         CanMatch = false,
                         DataType = ImportField.ImportFieldType.String,
                         MaxLength = 1000,
                         ValidateInputData = (data, dataCache) => { return dataCache.TpoPortals.ContainsKey(data); },
                         IsRequired = false,
                         SetBrokerData = (broker, data, dataCache) => { broker.TPOPortalConfigID = dataCache.TpoPortals[data]; }
                        },

                    new ImportField() {
                         FieldId = "LicenseInfoList.Add.State",
                         Name = "State License",
                         CanMatch = false,
                         MaxLength = 2,
                         DataType= ImportField.ImportFieldType.String,
                         ValidateInputData = (value, dataCache) => { return UnitedStatesState.CreateWithValidation(value).HasValue; },
                         IsRequired = true,
                         SetLicenseInfo = (license, data) => { license.State = data; },
                        },

                    new ImportField() {
                         FieldId = "LicenseInfoList.Add.ExpirationDate",
                         Name = "State License Expiration Date",
                         CanMatch = false,
                         MaxLength = 11,
                         DataType= ImportField.ImportFieldType.String,
                         IsRequired = true,
                         SetLicenseInfo = (license, data) => { license.ExpD = SafeDateTime(data).Value.ToShortDateString(); },
                        },

                    new ImportField() {
                         FieldId = "LicenseInfoList.Add.LicenseNum",
                         Name = "State License LicenseNumber",
                         CanMatch = false,
                         MaxLength = 200,
                         DataType= ImportField.ImportFieldType.String,
                         IsRequired = true,
                         SetLicenseInfo = (license, data) => { license.License = data; },
                        },

                    new ImportField() {
                         FieldId = "OriginatorCompensationPercent",
                         Name = "OriginatorCompensationPercent",
                         CanMatch = false,
                         DataType = ImportField.ImportFieldType.Decimal,
                         IsRequired = false,
                         SetBrokerData = (broker, data, dataCache) => { broker.OriginatorCompensationPercent = SafeGetDecimal(data); }
                        },

                    new ImportField() {
                         FieldId = "OriginatorCompensationIsOnlyPaidForFirstLienOfCombo",
                         Name = "Originator Compensation Is Only Paid For First Lien Of Combo",
                         CanMatch = false,
                         DataType = ImportField.ImportFieldType.Bool,
                         IsRequired = false,
                         SetBrokerData = (broker, data, dataCache) => { broker.OriginatorCompensationIsOnlyPaidForFirstLienOfCombo = bool.Parse(data); }
                        },

                    new ImportField() {
                         FieldId = "OriginatorCompensationBaseT",
                         Name = "OriginatorCompensationBaseT",
                         CanMatch = false,
                         DataType = ImportField.ImportFieldType.PercentBase,
                         IsRequired = false,
                         SetBrokerData = (broker, data, dataCache) => { broker.OriginatorCompensationBaseT = (E_PercentBaseT)Enum.Parse(typeof(E_PercentBaseT), data.Replace(" ", "")); }
                        },

                    new ImportField() {
                         FieldId = "OriginatorCompensationMinAmount",
                         Name = "OriginatorCompensationMinAmount",
                         CanMatch = false,
                         DataType = ImportField.ImportFieldType.Decimal,
                         IsRequired = false,
                         SetBrokerData = (broker, data, dataCache) => { broker.OriginatorCompensationMinAmount = SafeGetDecimal(data); }
                        },

                    new ImportField() {
                         FieldId = "OriginatorCompensationMaxAmount",
                         Name = "OriginatorCompensationMaxAmount",
                         CanMatch = false,
                         DataType = ImportField.ImportFieldType.Decimal,
                         IsRequired = false,
                         SetBrokerData = (broker, data, dataCache) => { broker.OriginatorCompensationMaxAmount = SafeGetDecimal(data); }
                        },

                    new ImportField() {
                         FieldId = "OriginatorCompensationFixedAmount",
                         Name = "OriginatorCompensationFixedAmount",
                         CanMatch = false,
                         DataType = ImportField.ImportFieldType.Decimal,
                         IsRequired = false,
                         SetBrokerData = (broker, data, dataCache) => { broker.OriginatorCompensationFixedAmount = SafeGetDecimal(data); }
                        },

                    new ImportField() {
                         FieldId = "OriginatorCompensationNotes",
                         Name = "OriginatorCompensationNotes",
                         CanMatch = false,
                         MaxLength = 200,
                         DataType= ImportField.ImportFieldType.String,
                         IsRequired = false,
                         SetBrokerData = (broker, data, dataCache) => { broker.OriginatorCompensationNotes = data; }
                        },

                    new ImportField() {
                         FieldId = "OriginatorCompensationLastModifiedDate",
                         Name = "OriginatorCompensationLastModifiedDate",
                         CanMatch = false,
                         DataType = ImportField.ImportFieldType.DateTime,
                         IsRequired = false,
                         SetBrokerData = (broker, data, dataCache) => { broker.OriginatorCompensationLastModifiedDate = SafeDateTime(data); }
                        },

                    new ImportField() {
                         FieldId = "GroupMembership",
                         Name = "Group Membership",
                         CanMatch = false,
                         MaxLength = 8000,
                         DataType= ImportField.ImportFieldType.String,
                         IsRequired = true,
                         ValidateInputData = (data, dataCache) => {
                             return data.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries)
                                .All(groupName => dataCache.Groups.ContainsKey(groupName));
                         },

                         SetBrokerData = (broker, data, dataCache) => { broker.GroupIdList_ImporterUse = data; }
                        },

                    new ImportField() {
                         FieldId = "BranchNm",
                         Name = "Wholesale Branch Name",
                         CanMatch = false,
                         MaxLength = 36,
                         DataType= ImportField.ImportFieldType.String,
                         IsRequired = true,
                         ValidateInputData = (data, dataCache) => { return dataCache.Branches.ContainsKey(data); },
                         SetBrokerData = (broker, data, dataCache) => { broker.BranchId = dataCache.Branches[data]; }
                        },

                    new ImportField() {
                         FieldId = "MiniCorrespondentBranchNm",
                         Name = "Mini Correspondent Branch Name",
                          CanBeDestination= true,
                         CanMatch = false ,

                         MaxLength = 36,
                         DataType= ImportField.ImportFieldType.String,
                         IsRequired = true,
                         ValidateInputData = (data, dataCache) => { return dataCache.Branches.ContainsKey(data); },
                         SetBrokerData = (broker, data, dataCache) => { broker.MiniCorrespondentBranchId = dataCache.Branches[data]; }
                        },

                    new ImportField() {
                         FieldId = "CorrespondentBranchNm",
                         Name = "Correspondent Branch Name",
                         CanBeDestination= true,
                         CanMatch = false ,
                         MaxLength = 36,
                         DataType= ImportField.ImportFieldType.String,
                         IsRequired = true,
                         ValidateInputData = (data, dataCache) => { return dataCache.Branches.ContainsKey(data); },
                         SetBrokerData = (broker, data, dataCache) => { broker.CorrespondentBranchId = dataCache.Branches[data]; }
                        },

                    new ImportField() {
                         FieldId = "BrokerPriceGroupId",
                         Name = "Broker Price Group Id",
                         CanMatch = false,
                         DataType = ImportField.ImportFieldType.Guid,
                         IsRequired = true,
                         ValidateInputData = (data, dataCache) => { return dataCache.PriceGroups.ContainsValue(Guid.Parse(data)); },
                         SetBrokerData = (broker, data, dataCache) => { broker.LpePriceGroupId = Guid.Parse(data); }
                        },

                        new ImportField() {
                         FieldId = "BrokerPriceGroupNm",
                         Name = "Broker Price Group Name",
                         CanMatch = false,
                         DataType = ImportField.ImportFieldType.String,
                         MaxLength = 100,
                         IsRequired = true,
                         ValidateInputData = (data, dataCache) => { return dataCache.PriceGroups.ContainsKey(data); },
                         SetBrokerData = (broker, data, dataCache) => { broker.LpePriceGroupId = dataCache.PriceGroups[data]; }
                        },

                    new ImportField() {
                         FieldId = "MiniCorrPriceGroupId",
                         Name = "Mini Corr Price Group Id",
                         CanMatch = false,
                         DataType = ImportField.ImportFieldType.Guid,
                         IsRequired = true,
                         ValidateInputData = (data, dataCache) => { return dataCache.PriceGroups.ContainsValue(Guid.Parse(data)); },
                         SetBrokerData = (broker, data, dataCache) => { broker.MiniCorrespondentLpePriceGroupId = Guid.Parse(data); }
                        },

                        new ImportField() {
                         FieldId = "MiniCorrPriceGroupNm",
                         Name = "Mini Corr Price Group Name",
                         CanMatch = false,
                         DataType = ImportField.ImportFieldType.String,
                         IsRequired = true,
                         ValidateInputData = (data, dataCache) => { return dataCache.PriceGroups.ContainsKey(data); },
                         SetBrokerData = (broker, data, dataCache) => { broker.MiniCorrespondentLpePriceGroupId = dataCache.PriceGroups[data]; }
                        },

                        new ImportField() {
                         FieldId = "CorrPriceGroupId",
                         Name = "Corr Price Group Id",
                         CanMatch = false,
                         DataType = ImportField.ImportFieldType.Guid,
                         IsRequired = true,
                         ValidateInputData = (data, dataCache) => { return dataCache.PriceGroups.ContainsValue(Guid.Parse(data)); },
                         SetBrokerData = (broker, data, dataCache) => { broker.CorrespondentLpePriceGroupId = Guid.Parse(data); }
                        },

                         new ImportField() {
                         FieldId = "CorrPriceGroupNm",
                         Name = "Corr Price Group Name",
                         CanMatch = false,
                         DataType = ImportField.ImportFieldType.String,
                         IsRequired = true,
                         ValidateInputData = (data, dataCache) => { return dataCache.PriceGroups.ContainsKey(data); },
                         SetBrokerData = (broker, data, dataCache) => { broker.CorrespondentLpePriceGroupId = dataCache.PriceGroups[data]; }
                        },

                    new ImportField() {
                         FieldId = "CompanyTier",
                         Name = "Company Tier Id",
                         CanMatch = false,
                         DataType = ImportField.ImportFieldType.Int,
                         IsRequired = false,
                         ValidateInputData = (data, dataCache) => { return data == "0" || dataCache.CompanyTiers.ContainsValue(int.Parse(data)); },
                         SetBrokerData = (broker, data, dataCache) => { broker.PmlCompanyTierId = int.Parse(data); }
                        },

                    new ImportField() {
                         FieldId = "CompanyTierNm",
                         Name = "Company Tier Name",
                         CanMatch = false,
                         DataType = ImportField.ImportFieldType.String,
                         MaxLength = 500,
                         IsRequired = false,
                         ValidateInputData = (data, dataCache) => { return dataCache.CompanyTiers.ContainsKey(data); },
                         SetBrokerData = (broker, data, dataCache) => { broker.PmlCompanyTierId = dataCache.CompanyTiers[data]; }
                        },

                    new ImportField() {
                         FieldId = "CorrespondentTPOPortalConfigID",
                         Name = "Correspondent Landing Page Id",
                         CanMatch = true,
                         DataType = ImportField.ImportFieldType.Guid,
                         IsRequired = false,
                         ValidateInputData = (data, dataCache) => { return dataCache.TpoPortals.ContainsValue(Guid.Parse(data)); },
                         SetBrokerData = (broker, data, dataCache) => { broker.CorrespondentTPOPortalConfigID = Guid.Parse(data); }
                        },

                    new ImportField() {
                         FieldId = "CorrespondentTPOPortalConfigNm",
                         Name = "Correspondent Landing Page Name",
                         CanMatch = true,
                         DataType = ImportField.ImportFieldType.String,
                         MaxLength = 1000,
                         IsRequired = false,
                         ValidateInputData = (data, dataCache) => { return dataCache.TpoPortals.ContainsKey(data); },
                         SetBrokerData = (broker, data, dataCache) => { broker.CorrespondentTPOPortalConfigID = dataCache.TpoPortals[data]; }
                        },


                    new ImportField() {
                         FieldId = "MiniCorrespondentTPOPortalConfigID",
                         Name = "Mini-Correspondent Landing Page Id",
                         CanMatch = true,
                         DataType = ImportField.ImportFieldType.Guid,
                         IsRequired = false,
                         ValidateInputData = (data, dataCache) => { return dataCache.TpoPortals.ContainsValue(Guid.Parse(data)); },
                         SetBrokerData = (broker, data, dataCache) => { broker.MiniCorrespondentTPOPortalConfigID = Guid.Parse(data); }
                        },

                    new ImportField() {
                         FieldId = "MiniCorrespondentTPOPortalConfigNm",
                         Name = "Mini-Correspondent Landing Page Name",
                         CanMatch = true,
                         DataType = ImportField.ImportFieldType.String,
                         MaxLength = 1000,
                         IsRequired = false,
                         ValidateInputData = (data, dataCache) => { return dataCache.TpoPortals.ContainsKey(data); },
                         SetBrokerData = (broker, data, dataCache) => { broker.MiniCorrespondentTPOPortalConfigID = dataCache.TpoPortals[data]; }
                        },


                    new ImportField() {
                         FieldId = "NmlsName",
                         Name = "Name on Loan Documents",
                         CanMatch = false,
                         MaxLength = 100,
                         DataType = ImportField.ImportFieldType.String,
                         IsRequired = false,
                         SetBrokerData = (broker, data, dataCache) => { broker.NmlsName = data; }
                        },

                    new ImportField() {
                         FieldId = "NmlsNameLckd",
                         Name = "Name on Loan Documents Locked",
                         CanMatch = false,
                         DataType = ImportField.ImportFieldType.Bool,
                         IsRequired = false,
                         SetBrokerData = (broker, data, dataCache) => { broker.NmlsNameLckd = bool.Parse(data); }
                        },

                    new ImportField() {
                         FieldId = "PMLNavigationPermissions",
                         Name = "Tpo Navigation Permissions",
                         CanMatch = false,
                         MaxLength = 2000,
                         DataType = ImportField.ImportFieldType.String,
                         IsRequired = false,
                         SetBrokerData = (broker, data, dataCache) => { broker.PMLNavigationPermissions = data; }
                        },

                    new ImportField() {
                         FieldId = "Roles",
                         Name = "Company Roles",
                         CanMatch = false,
                         MaxLength = 50,
                         DataType = ImportField.ImportFieldType.String,
                         ValidateInputData = (value, dataCache) => { return ValidateCompanyRoles(value); },
                         IsRequired = false,
                         SetBrokerData = (broker, data, dataCache) => {
                                                        HashSet<E_OCRoles> roles;
                                                        ProcessCompanyRoles(data, out roles);
                                                        broker.Roles = roles;
                                                     }
                        },

                    new ImportField() {
                         FieldId = "TpoColorThemeId",
                         Name = "TPO Portal Color Theme",
                         CanMatch = true,
                         DataType = ImportField.ImportFieldType.Guid,
                         IsRequired = false,
                         ValidateInputData = (data, dataCache) => { return dataCache.TpoColorThemes.ContainsValue(Guid.Parse(data)); },
                         SetBrokerData = (broker, data, dataCache) => { broker.TpoColorThemeId = Guid.Parse(data); }
                        },

                    new ImportField() {
                         FieldId = "UnderwritingAuthority",
                         Name = "Underwriting Authority",
                         CanMatch = true,
                         DataType = ImportField.ImportFieldType.UnderwritingAuthority,
                         IsRequired = true,
                         SetBrokerData = (broker, data, dataCache) => { broker.OriginatorCompensationBaseT = (E_PercentBaseT)Enum.Parse(typeof(E_UnderwritingAuthority), data); }
                        },

                    new ImportField() {
                         FieldId = "UseDefaultCorrBranchId",
                         Name = "Use Default Correspondent Branch",
                         CanMatch = false,
                         DataType = ImportField.ImportFieldType.Bool,
                         IsRequired = false,
                         SetBrokerData = (broker, data, dataCache) => { broker.UseDefaultCorrBranchId = bool.Parse(data); }
                        },

                    new ImportField() {
                         FieldId = "UseDefaultCorrPriceGroupId",
                         Name = "Use DefaultCorrespondent Price Group",
                         CanMatch = false,
                         DataType = ImportField.ImportFieldType.Bool,
                         IsRequired = false,
                         SetBrokerData = (broker, data, dataCache) => { broker.UseDefaultCorrPriceGroupId = bool.Parse(data); }
                        },

                    new ImportField() {
                         FieldId = "UseDefaultMiniCorrBranchId",
                         Name = "Use Default Mini-Correspondent Branch",
                         CanMatch = false,
                         DataType = ImportField.ImportFieldType.Bool,
                         IsRequired = false,
                         SetBrokerData = (broker, data, dataCache) => { broker.UseDefaultMiniCorrBranchId = bool.Parse(data); }
                        },

                    new ImportField() {
                         FieldId = "UseDefaultMiniCorrPriceGroupId",
                         Name = "Use Default Mini-Correspondent Price Group",
                         CanMatch = false,
                         DataType = ImportField.ImportFieldType.Bool,
                         IsRequired = false,
                         SetBrokerData = (broker, data, dataCache) => { broker.UseDefaultWholesalePriceGroupId = bool.Parse(data); }
                        },

                    new ImportField() {
                         FieldId = "UseDefaultWholesaleBranchId",
                         Name = "Use Default Wholesale Branch",
                         CanMatch = false,
                         DataType = ImportField.ImportFieldType.Bool,
                         IsRequired = false,
                         SetBrokerData = (broker, data, dataCache) => { broker.UseDefaultWholesaleBranchId = bool.Parse(data); }
                        },

                     new ImportField() {
                         FieldId = "UseDefaultWholesalePriceGroupId",
                         Name = "Use Default Wholesale PriceGroup",
                         CanMatch = false,
                         DataType = ImportField.ImportFieldType.Bool,
                         IsRequired = false,
                         SetBrokerData = (broker, data, dataCache) => { broker.UseDefaultWholesalePriceGroupId = bool.Parse(data); }
                        },
                };
            }
        }
    }
}
