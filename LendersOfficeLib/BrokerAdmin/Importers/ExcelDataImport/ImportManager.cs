﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataAccess;
using System.Data;
using LendersOffice.Security;

namespace LendersOffice.BrokerAdmin.Importers
{

    public sealed class ImportManager
    {
        private Dictionary<string, AvailableImportDestination> m_destinations;

        public ImportManager()
        {
            AvailableImportDestination[] destinations = new AvailableImportDestination[] {
                new OriginatingCompanyImportDestination(),
                new PMLUserDataImport()
            };

            m_destinations = destinations.ToDictionary(p => p.Name, StringComparer.OrdinalIgnoreCase);
        }

        public IEnumerable<string> GetAvailableImporters()
        {
            return m_destinations.Keys;
        }

        public ImportField[] GetAvailableFields(string id)
        {
            return m_destinations[id].AvailableFields;
        }

        public void MatchData(string id, Guid brokerId, DataTable importData, out IEnumerable<string> errors, AbstractUserPrincipal principal)
        {
            m_destinations[id].MatchData(brokerId, importData, false, out errors, principal);
        }


        public void ImportData(string id, Guid brokerId, DataTable importData, out IEnumerable<string> errors, AbstractUserPrincipal principal)
        {
            m_destinations[id].MatchData(brokerId, importData, true, out errors, principal);
        }


        public bool IsValidData(string id, DataTable importData, Guid brokerId, out IEnumerable<string> errors, out IEnumerable<string> warnings)
        {
            return m_destinations[id].IsValidData(importData, brokerId, out errors, out warnings);
        }


    }
}
