﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using DataAccess;
using System.Data;
using CommonProjectLib.Common.Lib;
using LendersOffice.Security;

namespace LendersOffice.BrokerAdmin.Importers
{
    public abstract class AvailableImportDestination
    {
        public const string NO_CHANGE_INDICATOR = "[nochange]";
        
        /// <summary>
        /// Unique name to identify importer by. 
        /// </summary>
        public abstract string Name { get; }

        /// <summary>
        /// Store procedure to pull current records from should just take brokerid
        /// </summary>
        protected abstract string RecordSPName { get; }

        /// <summary>
        /// The primary key. Used in the import.
        /// </summary>
        protected abstract string PrimaryKey { get; }

        /// <summary>
        /// List of fields in the object. Importer can set all of them except if CanMatch = true.
        /// </summary>
        public abstract ImportField[] AvailableFields { get; }


        protected decimal SafeGetDecimal(string data)
        {
            if (!string.IsNullOrEmpty(data))
            {
                return decimal.Parse(data);
            }
            else
            {
                return 0;
            }
        }

        protected DateTime? SafeDateTime(string data)
        {
            if (string.IsNullOrEmpty(data))
            {
                return null;
            }
            else
            {
                return DateTime.Parse(data);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="brokerId"></param>
        /// <returns></returns>
        private DataTable GetCurrentData(Guid brokerId)
        {
            DataSet set = new DataSet();
            SqlParameter[] paramters = {
                                           new SqlParameter("@BrokerId", brokerId)
                                       };

            DataSetHelper.Fill(set, brokerId, RecordSPName, paramters);
            if (set.Tables.Count > 0)
            {
                return set.Tables[0];
            }
            return null;
        }

        /// <summary>
        /// Translates the columns name to field ids.  If the column is not found as an availble destination 
        /// a null record is added.
        /// </summary>
        /// <param name="importData"></param>
        /// <returns></returns>
        public virtual ColumnMapping[] TranslateColumns(DataTable importData)
        {
            ColumnMapping[] importFields = new ColumnMapping[importData.Columns.Count];

            Dictionary<string, ImportField> availableFieldsById = AvailableFields.ToDictionary(p => p.FieldId, StringComparer.OrdinalIgnoreCase);
            string keyString = "key:";
            for (int i = 0; i < importData.Columns.Count; i++)
            {
                DataColumn column = importData.Columns[i];
                bool isMatchingColumn = false;
                string name = column.ColumnName;
                if (name.StartsWith(keyString, StringComparison.OrdinalIgnoreCase))
                {
                    isMatchingColumn = true;
                    name = name.Substring(keyString.Length);
                }

                ImportField currentField;

                availableFieldsById.TryGetValue(name, out currentField);

                importFields[i] = (new ColumnMapping()
                {
                    ColumnIndex = i,
                    Field = currentField,
                    IsMatchingColumn = isMatchingColumn
                });
            }

            return importFields;
        }

        public bool IsValidData(DataTable importData, Guid brokerId, out IEnumerable<string> errorMessages, out IEnumerable<string> warningMessages)
        {
            ColumnMapping[] mappings = TranslateColumns(importData);
            List<string> errors = new List<string>();
            List<string> warnings = new List<string>();
            bool hasMatchingColumn = false;
            bool hasDestinationColumn = false;
            foreach (ColumnMapping mapping in mappings)
            {
                if (mapping.Field == null)
                {
                    errors.Add(string.Concat("Column ", mapping.ColumnIndex.ToString(),
                        " ", importData.Columns[mapping.ColumnIndex].ColumnName,
                        " was not found as a valid destination column."));
                }

                else if (mapping.IsMatchingColumn && !mapping.Field.CanMatch)
                {
                    errors.Add(string.Concat("Column ", mapping.ColumnIndex.ToString(),
                        " ", importData.Columns[mapping.ColumnIndex].ColumnName,
                        " cannot be used as a key column."));
                }
                else if (!mapping.IsMatchingColumn && !mapping.Field.CanBeDestination)
                {
                    errors.Add(string.Concat("Column ", mapping.ColumnIndex.ToString(),
                        " ", importData.Columns[mapping.ColumnIndex].ColumnName,
                        " does not allow changes through importer."));
                }

                else if (mapping.IsMatchingColumn)
                {
                    hasMatchingColumn = true;
                }
                else
                {
                    hasDestinationColumn = true;
                }
            }

            if (!hasMatchingColumn)
            {
                errors.Add("There is no key column to do the lookups by.");
            }
            if (!hasDestinationColumn)
            {
                errors.Add("There is no data to import.");
            }

            // Verify the formats of the row.
            int rowNum = 1;
            foreach (DataRow row in importData.Rows)
            {
                foreach (ColumnMapping column in mappings)
                {
                    if (column.Field == null)
                    {
                        // This is happens when the the field is not mappable.
                        // User will already get warning about column.
                        continue;
                    }

                    string data = row[column.ColumnIndex].ToString();
                    string validationError;
                    string validationWarning;

                    if (!IsFieldValueValid(column.Field, data, brokerId, out validationError, out validationWarning))
                    {
                        errors.Add($"Row {rowNum} {validationError}");
                    }

                    if ( !String.IsNullOrEmpty(validationWarning))
                    {
                        warnings.Add($"Row {rowNum} {validationWarning}");
                    }
                }
                rowNum++;
            }
            errorMessages = errors;
            warningMessages = warnings;
            return errors.Count == 0;
        }

        /// <summary>
        /// Helper method to do simple validation on the format and length
        /// of the data and give more useful error message.
        /// </summary>
        /// <param name="field"></param>
        /// <param name="value"></param>
        /// <param name="error"></param>
        /// <returns></returns>
        protected virtual bool IsFieldValueValid(ImportField field, string value, Guid brokerId, out string error, out string warning)
        {
            error = string.Empty;
            warning = string.Empty;

            if ( value.Equals( NO_CHANGE_INDICATOR, StringComparison.CurrentCultureIgnoreCase ))
            {
                // Can always choose to not set.
                return true;
            }

            if ( value.Length == 0)
            {
                if ( field.IsRequired)
                {
                    error = $"{field.Name}: A Non-blank value is required.";
                    return false;
                }
                else
                {
                    warning = $"{field.Name}: Will be cleared.  Be sure this was intended.";
                }
            }

            if (field.IsRequired && value.Length == 0)
            {
                error = $"{field.Name}: A Non-blank value is required.";
                return false;
            }

            var dataType = field.DataType;
            if (dataType == ImportField.ImportFieldType.String)
            {
                var valueLength = value.Length;
                var allowedLength = field.MaxLength;

                if (allowedLength > 0)
                {
                    if (value.Length > field.MaxLength)
                    {
                        error = $"{field.Name}: length {valueLength} too long for field.  Max length: {allowedLength}. Value: {Tools.TruncateString(value, 50)}.";
                        return false;
                    }
                }
            }
            else if (dataType == ImportField.ImportFieldType.Guid)
            {
                Guid parsedGuid;
                if (!String.IsNullOrEmpty(value) && !Guid.TryParse(value, out parsedGuid))
                {
                    error = $"{field.Name}: Could not parse value as Guid. Value: {Tools.TruncateString(value, 50)}.";
                    return false;
                }
            }
            else if (dataType == ImportField.ImportFieldType.Bool)
            {
                bool parsedBool;
                if (!bool.TryParse(value, out parsedBool))
                {
                    error = $"{field.Name}: Could not parse value as bool. Value: {Tools.TruncateString(value, 50)}.";
                    return false;
                }
            }
            else if (dataType == ImportField.ImportFieldType.Decimal)
            {
                decimal parsedDecimal;
                if (!String.IsNullOrEmpty(value) && !Decimal.TryParse(value, out parsedDecimal))
                {
                    error = $"{field.Name}: Could not parse value as Decimal. Value: {Tools.TruncateString(value, 50)}.";
                    return false;
                }
            }
            else if (dataType == ImportField.ImportFieldType.Int)
            {
                int parsedInt;
                if (!int.TryParse(value, out parsedInt))
                {
                    error = $"{field.Name}: Could not parse value as Integer. Value: {Tools.TruncateString(value, 50)}.";
                    return false;
                }
            }
            else if (dataType == ImportField.ImportFieldType.DateTime)
            {
                DateTime parsedDate;
                if (!String.IsNullOrEmpty(value) && !DateTime.TryParse(value, out parsedDate))
                {
                    error = $"{field.Name}: Could not parse value as DateTime. Value: {Tools.TruncateString(value, 50)}.";
                    return false;
                }
            }
            else if (dataType == ImportField.ImportFieldType.OCStatusType)
            {
                if (!String.IsNullOrEmpty(value) && !Enum.IsDefined(typeof(OCStatusType), value))
                {
                    error = $"{field.Name}: '{Tools.TruncateString(value, 50)}' not a valid value for OCStatusType.";
                    return false;
                }
            }
            else if (dataType == ImportField.ImportFieldType.PercentBase)
            {
                if (!String.IsNullOrEmpty(value) && !Enum.IsDefined(typeof(E_PercentBaseT), value))
                {
                    error = $"{field.Name}: '{Tools.TruncateString(value, 50)}' not a valid value for PercentBase.";
                    return false;
                }
            }
            else if (dataType == ImportField.ImportFieldType.UnderwritingAuthority)
            {
                if (!String.IsNullOrEmpty(value) && !Enum.IsDefined(typeof(E_UnderwritingAuthority), value))
                {
                    error = $"{field.Name}: '{Tools.TruncateString(value, 50)}' not a valid value for UnderwritingAuthority.";
                    return false;
                }
            }
            else
            {
                // This is not a import data validation error--it is a developer error.  Throw hard.
                throw new UnhandledEnumException(dataType);
            }


            var importerCache = new ImporterDataCache(brokerId);
            if (field.ValidateInputData != null)
            {
                if (!field.ValidateInputData(value, importerCache))
                {
                    error = $"{field.Name}: '{Tools.TruncateString(value, 50)}' is not a valid value.";
                    return false;
                }
            }

            return true;
        }

        public abstract bool AddNewRow(Guid brokerId, ColumnMapping[] mapping, DataRow row, List<string> errorList);

        public abstract bool ImportRow(Guid brokerId, string key, ColumnMapping[] mapping, DataRow row, List<string> errorList, AbstractUserPrincipal principal);

        /// <summary>
        /// Adds a new column (RecordsMatched at pos 0) to import data containing the number of matches the row matched in the current data.
        /// In order to speciy which column to use as a matching column string Key: must be added to front of column headers.
        /// </summary>
        /// <param name="brokerId">The broker whose data should be used to do the comparison (improt destination)</param>
        /// <param name="importData">The data to be imported</param>
        /// <param name="criterias">The list of criteria that should be used to determine if a record matches</param>
        /// <param name="doImport">Whether this should import.</param>
        /// <param name="errors">Any errors encountered.</param>
        /// <returns>True if all columns match</returns>
        public virtual void MatchData(Guid brokerId, DataTable importData, bool doImport, out IEnumerable<string> errorList, AbstractUserPrincipal principal)
        {
            List<string> overallErrors = new List<string>();

            IEnumerable<string> errors = new List<string>();
            IEnumerable<string> warnings = new List<string>();
            if (doImport && !IsValidData(importData, brokerId, out errors, out warnings))
            {
                throw CBaseException.GenericException("Import Data is not in correct format.");
            }

            overallErrors.AddRange(errors);

            //Find the Column Keys 
            ColumnMapping[] columnKeys = TranslateColumns(importData);

            foreach (var item in columnKeys)
            {
                item.ColumnIndex += 1; //add column below
            }

            //Add new column to keep track of matchings. -- status
            DataColumn column = importData.Columns.Add("RecordsMatched", typeof(int));
            column.DefaultValue = 0;
            column.SetOrdinal(0);


            DataTable currentData = GetCurrentData(brokerId);
            Dictionary<string, int> lookupTable = new FriendlyDictionary<string, int>(StringComparer.OrdinalIgnoreCase);
            Dictionary<string, List<string>> matchingIds = new FriendlyDictionary<string, List<string>>();

            //Build up a lookup table based on criteria.
            //the keyw ill be criteria values joined by _
            foreach (DataRow row in currentData.Rows)
            {
                List<string> key = new List<string>();

                foreach (ColumnMapping mapping in columnKeys)
                {
                    if (mapping != null && mapping.IsMatchingColumn)
                    {
                        key.Add(row[mapping.Field.FieldId].ToString());
                    }
                }

                string currentKey = String.Join("_", key.ToArray());
                int count;

                if (!lookupTable.TryGetValue(currentKey, out count))
                {
                    lookupTable.Add(currentKey, 1);
                    matchingIds.Add(currentKey, new List<string>() { row[PrimaryKey].ToString() });
                }
                else
                {
                    lookupTable[currentKey] += 1;
                    matchingIds[currentKey].Add(row[PrimaryKey].ToString());
                }
            }

            foreach (DataRow row in importData.Rows)
            {
                List<string> key = new List<string>();

                foreach (ColumnMapping mapping in columnKeys)
                {
                    if (mapping != null && mapping.IsMatchingColumn)
                    {
                        key.Add(row[mapping.ColumnIndex].ToString());
                    }
                }

                string currentKey = String.Join("_", key.ToArray());

                int count = 0;

                if (!lookupTable.TryGetValue(currentKey, out count))
                {
                    count = 0;
                    if (doImport)
                    {
                        AddNewRow(brokerId, columnKeys, row, overallErrors);
                    }
                }
                else if (doImport)
                {
                    if (matchingIds.ContainsKey(currentKey))
                    {
                        foreach (string sKey in matchingIds[currentKey])
                        {
                            ImportRow(brokerId, sKey, columnKeys, row, overallErrors, principal);
                        }
                    }
                    else
                    {
                        string message = $"Could not find: {currentKey}";
                        overallErrors.Add(message);
                        Tools.LogError(message);
                    }
                }

                row["RecordsMatched"] = count;
            }

            errorList = overallErrors;
        }
    }
}
