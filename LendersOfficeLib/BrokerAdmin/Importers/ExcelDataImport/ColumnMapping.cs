﻿namespace LendersOffice.BrokerAdmin.Importers
{
    public sealed class ColumnMapping
    {
        public int ColumnIndex { get; set; }
        public bool IsMatchingColumn { get; set; }
        public ImportField Field { get; set; }
    }
}
