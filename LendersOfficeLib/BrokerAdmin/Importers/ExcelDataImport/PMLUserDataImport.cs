﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataAccess;
using System.Data;
using LendersOffice.Admin;
using CommonProjectLib.Common.Lib;
using LendersOffice.ObjLib.PriceGroups;
using LendersOffice.Common;
using LendersOffice.Security;

namespace LendersOffice.BrokerAdmin.Importers
{
    public class PMLUserDataImport : AvailableImportDestination
    {

        public override string Name
        {
            get { return "PMLUserDataImport"; }
        }

        protected override string RecordSPName
        {
            get { return "PML_USER_GetLoginAndPriceGroupId"; }
        }

        protected override string PrimaryKey
        {
            get { return "EmployeeId"; }
        }

        public override ImportField[] AvailableFields
        {
            get
            {
                return new ImportField[] {
                    new ImportField() { CanBeDestination = false, CanMatch = true, FieldId = "LoginNm", Name = "Login Name" },
                    new ImportField() {  CanMatch = false, FieldId = "LpePriceGroupName", Name ="Price Group Name" },
                    new ImportField() {  CanMatch = false, FieldId = "MiniCorrPriceGroupName", Name ="Mini Corr Price Group Name" },
                    new ImportField() {  CanMatch = false, FieldId = "CorrPriceGroupName", Name ="Corr Price Group Name" },
                    new ImportField() {  CanMatch = false, FieldId = "BranchNm", Name ="Branch Name" },
                    new ImportField() { FieldId = "MiniCorrespondentBranchNm", Name = "Mini Correspondent Branch Name",  CanBeDestination= true, CanMatch = false  },
                    new ImportField() { FieldId = "CorrespondentBranchNm", Name = "Correspondent Branch Name",  CanBeDestination= true, CanMatch = false  },
                    new ImportField() { FieldId = "DUAutoLoginNm", Name = "DU Auto Login",  CanBeDestination= true, CanMatch = false  },
                    new ImportField() { FieldId = "DUAutoPassword", Name = "DU Auto Password",  CanBeDestination= true, CanMatch = false  },
                    new ImportField() { FieldId = "DOAutoLoginNm", Name = "DO Auto Login",  CanBeDestination= true, CanMatch = false  },
                    new ImportField() { FieldId = "DOAutoPassword", Name = "DO Auto Password",  CanBeDestination= true, CanMatch = false  },
                    new ImportField() { FieldId = "Roles", Name = "Roles",  CanMatch = false },
                    new ImportField() { FieldId = "AccessLevel", Name = "AccessLevel",  CanMatch = false },
                    new ImportField() { FieldId = "AdditionalCustomPageVisiblity", Name = "AdditionalCustomPageVisiblity",  CanMatch = false },
                    new ImportField() { FieldId = "RemoveCustomPageVisibility", Name = "RemoveCustomPageVisibility",  CanMatch = false }
                };
            }
        }

        public override bool AddNewRow(Guid brokerId, ColumnMapping[] mapping, DataRow row, List<string> errorList)
        {
            //no op
            return false;
        }

        private Dictionary<string, Guid> m_priceGroupCache = null;
        private Dictionary<string, Guid> m_branchCache = null;
        private Dictionary<Guid, string> m_tpoPagePortalCache = null;
        private BrokerDB brokerDbCache = null;
        public override bool ImportRow(Guid brokerId, string key, ColumnMapping[] mapping, DataRow row, List<string> errorList, AbstractUserPrincipal principal)
        {
            if (m_priceGroupCache == null)
            {
                m_priceGroupCache = new FriendlyDictionary<string, Guid>(StringComparer.OrdinalIgnoreCase);
                foreach (var item in PriceGroup.RetrieveAllFromBroker(brokerId))
                {
                    m_priceGroupCache.Add(item.Name, item.ID);
                }
            }

            if (m_branchCache == null)
            {
                m_branchCache = new FriendlyDictionary<string, Guid>(StringComparer.OrdinalIgnoreCase);
                foreach (var item in BranchDB.GetBranchObjects(brokerId))
                {
                    m_branchCache.Add(item.Name, item.BranchID);
                }
            }

            EmployeeDB db = EmployeeDB.RetrieveById(brokerId, new Guid(key));

            if (row.Table.Columns.Contains("LpePriceGroupName"))
            {
                db.LpePriceGroupID = m_priceGroupCache[row["LpePriceGroupName"].ToString().TrimWhitespaceAndBOM()];
            }

            if (row.Table.Columns.Contains("MiniCorrPriceGroupName"))
            {
                db.MiniCorrespondentPriceGroupID = m_priceGroupCache[row["MiniCorrPriceGroupName"].ToString().TrimWhitespaceAndBOM()];
            }

            if (row.Table.Columns.Contains("CorrPriceGroupName"))
            {

                db.CorrespondentPriceGroupID = m_priceGroupCache[row["CorrPriceGroupName"].ToString().TrimWhitespaceAndBOM()];
            }

            if (row.Table.Columns.Contains("BranchNm"))
            {
                db.BranchID = m_branchCache[row["BranchNm"].ToString().TrimWhitespaceAndBOM()];
            }


            if (row.Table.Columns.Contains("MiniCorrespondentBranchNm"))
            {
                db.MiniCorrespondentBranchID = m_branchCache[row["MiniCorrespondentBranchNm"].ToString().TrimWhitespaceAndBOM()];
            }

            if (row.Table.Columns.Contains("CorrespondentBranchNm"))
            {
                db.CorrespondentBranchID = m_branchCache[row["CorrespondentBranchNm"].ToString().TrimWhitespaceAndBOM()];
            }

            if (row.Table.Columns.Contains("DUAutoLoginNm"))
            {
                db.DUAutoLoginName = row["DUAutoLoginNm"].ToString().TrimWhitespaceAndBOM();
            }

            if (row.Table.Columns.Contains("DUAutoPassword"))
            {
                db.DUAutoPassword = row["DUAutoPassword"].ToString();
            }

            if (row.Table.Columns.Contains("DOAutoLoginNm"))
            {
                db.DOAutoLoginName = row["DOAutoLoginNm"].ToString().TrimWhitespaceAndBOM();
            }

            if (row.Table.Columns.Contains("DOAutoPassword"))
            {
                db.DOAutoPassword = row["DOAutoPassword"].ToString();
            }

            if (row.Table.Columns.Contains("Roles"))
            {
                string fullRoleString = row["Roles"].ToString();
                string[] roles = fullRoleString.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                if (roles.Length == 0)
                {
                    throw new CBaseException(ErrorMessages.Generic, $"Invalid value for PML User Roles. Key: {key} Value: {fullRoleString}.");
                }

                List<string> roleGuids = new List<string>();
                db.IsPmlManager = false;
                foreach (string role in roles)
                {
                    if (string.Equals(role, "Loan Officer", StringComparison.OrdinalIgnoreCase))
                    {
                        roleGuids.Add(CEmployeeFields.s_LoanRepRoleId.ToString());
                    }
                    else if (string.Equals(role, "Processor", StringComparison.OrdinalIgnoreCase))
                    {
                        roleGuids.Add(CEmployeeFields.s_BrokerProcessorId.ToString());
                    }
                    else if (string.Equals(role, "Secondary", StringComparison.OrdinalIgnoreCase))
                    {
                        roleGuids.Add(CEmployeeFields.s_ExternalSecondaryId.ToString());
                    }
                    else if (string.Equals(role, "Post-Closer", StringComparison.OrdinalIgnoreCase))
                    {
                        roleGuids.Add(CEmployeeFields.s_ExternalPostCloserId.ToString());
                    }
                    else if (string.Equals(role, "Supervisor", StringComparison.OrdinalIgnoreCase))
                    {
                        db.IsPmlManager = true;
                        roleGuids.Add(CEmployeeFields.s_AdministratorRoleId.ToString());
                    }
                    else
                    {
                        throw new CBaseException(ErrorMessages.Generic, $"Invalid value for PML User Roles. Key: {key} Value: {fullRoleString}.");
                    }
                }

                db.Roles = string.Join(",", roleGuids);
            }

            if (row.Table.Columns.Contains("AccessLevel"))
            {
                E_PmlLoanLevelAccess accessLevel;
                string accessLvlString = row["AccessLevel"].ToString();
                if (Enum.TryParse(accessLvlString, out accessLevel) && Enum.IsDefined(typeof(E_PmlLoanLevelAccess), accessLevel))
                {
                    db.PmlLevelAccess = accessLevel;
                }
                else
                {
                    throw new CBaseException(ErrorMessages.Generic, $"Invalid value for PML User Access Level. Key: {key} Value: {accessLvlString}.");
                }
            }

            if (row.Table.Columns.Contains("AdditionalCustomPageVisiblity"))
            {
                if (this.m_tpoPagePortalCache == null)
                {
                    if (this.brokerDbCache == null)
                    {
                        this.brokerDbCache = BrokerDB.RetrieveById(brokerId);
                    }

                    this.m_tpoPagePortalCache = this.brokerDbCache.GetPmlNavigationConfiguration(principal).GetPageNodeNames().ToDictionary((tuple) => tuple.Item1, (tuple) => tuple.Item2);
                }

                string[] stringIds = row["AdditionalCustomPageVisiblity"].ToString().Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                HashSet<Guid> pageIds = new HashSet<Guid>(db.AccessiblePmlNavigationLinksId);
                foreach (string stringId in stringIds)
                {
                    Guid id;
                    if (!Guid.TryParse(stringId, out id) || !this.m_tpoPagePortalCache.ContainsKey(id))
                    {
                        throw new CBaseException(ErrorMessages.Generic, $"Invalid value for Custom Portal Page Visiblity. Key: {key} Value: {stringId}");
                    }

                    pageIds.Add(id);
                }

                db.AccessiblePmlNavigationLinksId = pageIds.ToArray();
            }

            if (row.Table.Columns.Contains("RemoveCustomPageVisibility"))
            {
                string[] stringIds = row["RemoveCustomPageVisibility"].ToString().Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                HashSet<Guid> pageIds = new HashSet<Guid>(db.AccessiblePmlNavigationLinksId);
                foreach (string stringId in stringIds)
                {
                    Guid id;
                    if (!Guid.TryParse(stringId, out id) || !pageIds.Contains(id))
                    {
                        throw new CBaseException(ErrorMessages.Generic, $"Cannot remove Custom Portal Page since it was not previously selected.");
                    }

                    pageIds.Remove(id);
                }

                db.AccessiblePmlNavigationLinksId = pageIds.ToArray();
            }

            db.Save();
            return true;
        }


        protected override bool IsFieldValueValid(ImportField field, string value, Guid brokerId, out string error, out string warnings)
        {
            // We accept PMLUser Values.
            error = string.Empty;
            warnings = string.Empty;

            return true;
        }



    }
}
