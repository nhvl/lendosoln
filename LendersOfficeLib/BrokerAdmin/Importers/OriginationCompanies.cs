﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Common;
using LendersOffice.Common.TextImport;
using LendersOffice.Security;

namespace LendersOffice.BrokerAdmin.Importers
{
    public class OriginationCompanyImporter : AbstractImporter
    {
        //Configurable number of columns in case we add more later.
        private readonly int numColumns = Enum.GetNames(typeof(OCImporterColumns)).Length;

        //And the first reqColumns are required - errors will be thrown if they're empty.
        private const int reqColumns = 8;

        private enum OCImporterColumns
        {
            // Start required columns
            CompanyName,
            StreetAddress, 
            City, 
            State, 
            Zip, 
            Phone,
            CompanyRoles, 
            UnderwritingAuthority, 
            // End required columns
            CompanyTier,
            BrokerBranch, 
            BrokerPriceGroup,
            BrokerLandingPage,
            BrokerStatus,
            MiniCorrespondantBranch,
            MiniCorrespondantPriceGroup,
            MiniCorrespondentLandingPage,
            MiniCorrespondentStatus,
            CorrespondantBranch, 
            CorrespondantPriceGroup, 
            CorrespondentLandingPage,
            CorrespondantStatus,
            Fax, 
            CompanyID,
            TaxID, 
            PrincipalName1, 
            PrincipalName2, 
            CompanyNMLS,
            CompOnlyForFirstLien,
            CompPercent, 
            CompDollarAmount, 
            CompCeiling, 
            CompFloor, 
            CompensationType,
            CompNotes, 
            LicenseNumber, 
            LicenseState,
            LicenseExpirationDate,
            BrokerManager,
            BrokerProcessor,
            BrokerJuniorProcessor,
            BrokerAccountExec,
            BrokerUnderwriter,
            BrokerJuniorUnderwriter,
            BrokerLockDesk,
            MiniCorrManager,
            MiniCorrProcessor,
            MiniCorrJuniorProcessor,
            MiniCorrAccountExec,
            MiniCorrUnderwriter,
            MiniCorrJuniorUnderwriter,
            MiniCorrCreditAuditor,
            MiniCorrLegalAuditor,
            MiniCorrLockDesk,
            MiniCorrPurchaser,
            MiniCorrSecondary,
            CorrManager,
            CorrProcessor,
            CorrJuniorProcessor,
            CorrAccountExec,
            CorrUnderwriter,
            CorrJuniorUnderwriter,
            CorrCreditAuditor,
            CorrLegalAuditor,
            CorrLockDesk,
            CorrPurchaser,
            CorrSecondary

        }

        private const string _sheetName = "Import Origination Companies";

        /// <summary>
        /// Maps the rows to the necessary role for the relationships.
        /// </summary>
        private readonly Dictionary<OCImporterColumns, Tuple<E_RoleT, E_OCRoles>> columnToRoleMapping = new Dictionary<OCImporterColumns, Tuple<E_RoleT, E_OCRoles>>()
        {
            { OCImporterColumns.BrokerManager, new Tuple<E_RoleT, E_OCRoles>(E_RoleT.Manager, E_OCRoles.Broker) },
            { OCImporterColumns.BrokerProcessor, new Tuple<E_RoleT, E_OCRoles>(E_RoleT.Processor, E_OCRoles.Broker) },
            { OCImporterColumns.BrokerJuniorProcessor, new Tuple<E_RoleT, E_OCRoles>(E_RoleT.JuniorProcessor, E_OCRoles.Broker) },
            { OCImporterColumns.BrokerAccountExec, new Tuple<E_RoleT, E_OCRoles>(E_RoleT.LenderAccountExecutive, E_OCRoles.Broker) },
            { OCImporterColumns.BrokerUnderwriter, new Tuple<E_RoleT, E_OCRoles>(E_RoleT.Underwriter, E_OCRoles.Broker) },
            { OCImporterColumns.BrokerJuniorUnderwriter, new Tuple<E_RoleT, E_OCRoles>(E_RoleT.JuniorUnderwriter, E_OCRoles.Broker) },
            { OCImporterColumns.BrokerLockDesk, new Tuple<E_RoleT, E_OCRoles>(E_RoleT.LockDesk, E_OCRoles.Broker) },
            { OCImporterColumns.MiniCorrManager, new Tuple<E_RoleT, E_OCRoles>(E_RoleT.Manager, E_OCRoles.MiniCorrespondent) },
            { OCImporterColumns.MiniCorrProcessor, new Tuple<E_RoleT, E_OCRoles>(E_RoleT.Processor, E_OCRoles.MiniCorrespondent) },
            { OCImporterColumns.MiniCorrJuniorProcessor, new Tuple<E_RoleT, E_OCRoles>(E_RoleT.JuniorProcessor, E_OCRoles.MiniCorrespondent) },
            { OCImporterColumns.MiniCorrAccountExec, new Tuple<E_RoleT, E_OCRoles>(E_RoleT.LenderAccountExecutive, E_OCRoles.MiniCorrespondent) },
            { OCImporterColumns.MiniCorrUnderwriter, new Tuple<E_RoleT, E_OCRoles>(E_RoleT.Underwriter, E_OCRoles.MiniCorrespondent) },
            { OCImporterColumns.MiniCorrJuniorUnderwriter, new Tuple<E_RoleT, E_OCRoles>(E_RoleT.JuniorUnderwriter, E_OCRoles.MiniCorrespondent) },
            { OCImporterColumns.MiniCorrCreditAuditor, new Tuple<E_RoleT, E_OCRoles>(E_RoleT.CreditAuditor, E_OCRoles.MiniCorrespondent) },
            { OCImporterColumns.MiniCorrLegalAuditor, new Tuple<E_RoleT, E_OCRoles>(E_RoleT.CreditAuditor, E_OCRoles.MiniCorrespondent) },
            { OCImporterColumns.MiniCorrLockDesk, new Tuple<E_RoleT, E_OCRoles>(E_RoleT.LockDesk, E_OCRoles.MiniCorrespondent) },
            { OCImporterColumns.MiniCorrPurchaser, new Tuple<E_RoleT, E_OCRoles>(E_RoleT.Purchaser, E_OCRoles.MiniCorrespondent) },
            { OCImporterColumns.MiniCorrSecondary, new Tuple<E_RoleT, E_OCRoles>(E_RoleT.Secondary, E_OCRoles.MiniCorrespondent) },
            { OCImporterColumns.CorrManager, new Tuple<E_RoleT, E_OCRoles>(E_RoleT.Manager, E_OCRoles.Correspondent) },
            { OCImporterColumns.CorrProcessor, new Tuple<E_RoleT, E_OCRoles>(E_RoleT.Processor, E_OCRoles.Correspondent) },
            { OCImporterColumns.CorrJuniorProcessor, new Tuple<E_RoleT, E_OCRoles>(E_RoleT.JuniorProcessor, E_OCRoles.Correspondent) },
            { OCImporterColumns.CorrAccountExec, new Tuple<E_RoleT, E_OCRoles>(E_RoleT.LenderAccountExecutive, E_OCRoles.Correspondent) },
            { OCImporterColumns.CorrUnderwriter, new Tuple<E_RoleT, E_OCRoles>(E_RoleT.Underwriter, E_OCRoles.Correspondent) },
            { OCImporterColumns.CorrJuniorUnderwriter, new Tuple<E_RoleT, E_OCRoles>(E_RoleT.JuniorUnderwriter, E_OCRoles.Correspondent) },
            { OCImporterColumns.CorrCreditAuditor, new Tuple<E_RoleT, E_OCRoles>(E_RoleT.CreditAuditor, E_OCRoles.Correspondent) },
            { OCImporterColumns.CorrLegalAuditor, new Tuple<E_RoleT, E_OCRoles>(E_RoleT.LegalAuditor, E_OCRoles.Correspondent) },
            { OCImporterColumns.CorrLockDesk, new Tuple<E_RoleT, E_OCRoles>(E_RoleT.LockDesk, E_OCRoles.Correspondent) },
            { OCImporterColumns.CorrPurchaser, new Tuple<E_RoleT, E_OCRoles>(E_RoleT.Purchaser, E_OCRoles.Correspondent) },
            { OCImporterColumns.CorrSecondary, new Tuple<E_RoleT, E_OCRoles>(E_RoleT.Secondary, E_OCRoles.Correspondent) }
        };

        // OPM 199559, ML, 8/14/2015
        // If any new Company Roles are added to E_OCRoles, you must add a new entry
        // in this dictionary.
        private readonly Dictionary<string, E_OCRoles> CompanyRoles = new Dictionary<string, E_OCRoles>
        {
            {"Broker", E_OCRoles.Broker},
            {"Correspondent", E_OCRoles.Correspondent},
            {"MiniCorrespondent", E_OCRoles.MiniCorrespondent}
        };

        // OPM 199559, ML, 8/14/2015
        // If any new Underwriting Authorities are added to E_UnderwritingAuthority, you must add a new entry
        // in this dictionary.
        private readonly Dictionary<string, E_UnderwritingAuthority> UnderwritingAuthorities = new Dictionary<string, E_UnderwritingAuthority>
        {
            {"Delegated", E_UnderwritingAuthority.Delegated},
            {"PriorApproved", E_UnderwritingAuthority.PriorApproved}
        };

        // OPM 199559, ML, 8/18/2015
        // If any new Compensation Amounts applicable to a PmlBroker are added to E_PercentBaseT, 
        // you must add a new entry in this dictionary.
        private readonly Dictionary<string, E_PercentBaseT> LoanCompensationTypes = new Dictionary<string, E_PercentBaseT>
        {
            {"LoanAmount", E_PercentBaseT.LoanAmount},
            {"TotalLoanAmount", E_PercentBaseT.TotalLoanAmount}
        };

        public OriginationCompanyImporter()
        {
            HeaderText = string.Join(", ", Enum.GetNames(typeof(OCImporterColumns)));
            SheetName = _sheetName;
        }

        private string GetSavedCompanies(IEnumerable<PmlBroker> saved)
        {
            if (saved.Count() == 0)
            {
                return "No companies were saved";
            }

            StringBuilder sb = new StringBuilder("The following companies were successfully saved:\n");
            foreach (PmlBroker pb in saved)
            {
                sb.AppendLine(pb.Name);
            }

            return sb.ToString();
        }

        public override void Import(string csv, Guid BrokerId, out string Results, out DataTable ErrorList)
        {
            ErrorList = new DataTable();
            Results = "";
            try
            {
                LinkedList<PmlBroker> toSave;

                bool parseSuccess = ParseCSV(csv, BrokerId, out toSave, out ErrorList);

                if (parseSuccess)
                {
                    //We parsed everything, so go ahead and save it all
                    foreach (PmlBroker pb in toSave)
                    {
                        try
                        {
                            pb.Save();
                        }
                        catch (SqlException e)
                        {
                            ErrorList.Rows.Add("Could not save PML Broker: " + pb.Name + "! This is probably due to a db problem. No further imports will be made, but all brokers before this one were saved.");
                            ErrorList.Rows.Add("Error text: " + e.ToString());
                            //Take everything up until the erroring pml broker
                            Results = GetSavedCompanies(toSave.TakeWhile((b) => !b.Equals(pb)));
                            throw;
                        }
                        catch (Exception e)
                        {
                            ErrorList.Rows.Add("Could not save PML Broker: " + pb.Name + "! This is a totally unexpected exception. No further imports will be made, but all brokers before this one were saved.");
                            ErrorList.Rows.Add("Error text: " + e.ToString());
                            Results = GetSavedCompanies(toSave.TakeWhile((b) => !b.Equals(pb)));
                            throw;
                        }
                    }
                    //If we get here, there were no issues saving.
                    Results = GetSavedCompanies(toSave);
                }
            }
            catch (Exception e)
            {
                Tools.LogError("Failed to import companies: " + e.Message + ".", e);
                throw;
            }
        }

        /// <summary>
        /// Parse the (hopefully) CSV file in m_Companies.Text
        /// </summary>
        /// <param name="toSave">A list of PML Brokers that need saving</param>
        /// <param name="errorList">Errors that occurred during parsing</param>
        /// <returns>True if parsing was successful, false if not</returns>
        private bool ParseCSV(string csv, Guid BrokerId, out LinkedList<PmlBroker> toSave, out DataTable errorList)
        {
            toSave = new LinkedList<PmlBroker>();
            errorList = new DataTable();

            bool errord = false;
            errorList.Columns.Add(new DataColumn("Errors", typeof(string)));

            ILookup<int, string> parseErrors;
            DataTable data = DelimitedListParser.CsvToDataTable(csv, true, reqColumns, numColumns, out parseErrors);


            ////Added by JustinJ at 6/15/2015
            SqlParameter[] parameters = { new SqlParameter("BrokerId", BrokerId) };
            Dictionary<string, PmlBroker> compIdMapPmlBroker = new Dictionary<string, PmlBroker>();

            DataTable pmlBrokerDataTable = StoredProcedureHelper.ExecuteDataTable(BrokerId, "ListAllPmlBrokerByBrokerId", parameters);
            DataRowCollection rows = pmlBrokerDataTable.Rows;
            foreach (DataRow row in rows)
            {
                var companyName = row["Name"].ToString();
                var companyId = row["CompanyId"].ToString();
                var key = companyName + ":" + companyId;

                if (!compIdMapPmlBroker.ContainsKey(key))
                {
                    PmlBroker curPmlBroker = PmlBroker.RetrievePmlBrokerByCompanyId(companyId, BrokerId);
                    curPmlBroker.LicenseInfoList = LicenseInfoList.ToObject(row["LicenseXmlContent"].ToString());
                    compIdMapPmlBroker.Add(key, curPmlBroker);
                }
            }
            //Fin by JustinJ

            //We skip the first line in parsing to make room for a header row, so start at 1            
            int line = 1;
            if (parseErrors.Contains(line))
            {
                foreach (string err in parseErrors[line])
                {
                    errord = true;
                    errorList.Rows.Add("Error on line " + line + ": " + err);
                }
            }

            BrokerDB addingBroker = BrokerDB.RetrieveById(BrokerId);

            var addingBrokerCompanyTierList = addingBroker.PmlCompanyTiers;

            var addingBrokerLandingPages = addingBroker.GetLandingPages();

            var addingBrokerBranches = addingBroker.GetBranches();

            var addingBrokerPriceGroups = addingBroker.GetPriceGroups();

            for (int i = 0; i < data.Rows.Count; i++)
            {
                DataRow row = data.Rows[i];
                line++;
                foreach (string err in parseErrors[line])
                {
                    errord = true;
                    errorList.Rows.Add("Error on line " + line + ": " + err);
                }

                var curCompanyName = row[(int)OCImporterColumns.CompanyName].ToString();
                var curCompanyId = row[(int)OCImporterColumns.CompanyID].ToString();
                var key = curCompanyName + ":" + curCompanyId;

                PmlBroker pb = null;

                if (!compIdMapPmlBroker.TryGetValue(key, out pb))
                {
                    pb = PmlBroker.CreatePmlBroker(BrokerId);
                }

                //Copy info over
                pb.CompanyId = curCompanyId;
                pb.Name = curCompanyName;
                pb.Addr = row[(int)OCImporterColumns.StreetAddress].ToString();
                pb.City = row[(int)OCImporterColumns.City].ToString();
                pb.State = row[(int)OCImporterColumns.State].ToString();
                pb.Zip = row[(int)OCImporterColumns.Zip].ToString();
                pb.Phone = row[(int)OCImporterColumns.Phone].ToString();

                // OPM 199559, 8/11/2015, ML
                string roles = row[(int)OCImporterColumns.CompanyRoles].ToString().TrimWhitespaceAndBOM();

                if (roles != string.Empty)
                {
                    DelimitedListParser roleParser = new DelimitedListParser();

                    E_OCRoles roleToAdd;

                    string matchToEnumConstant;

                    foreach (var parsedRole in roleParser.Eat(roles))
                    {
                        // Convert the match to potentially one of the E_OCRoles constants by:
                        // 1. Stripping any extra leading/trailing spaces.
                        // 2. Removing the dash if "Mini-Correspondent" is supplied.
                        //
                        // Note: if a new role is added to E_OCRoles that has a text equivalent
                        // with multiple words and special separators, be sure to add a call to Replace() 
                        // so that the parsed string for the role matches the constant defined in E_OCRoles
                        // (e.g. Replace("-", string.Empty) to get rid of the dash in Mini-Correspondent so
                        // that it matches E_OCRoles.MiniCorrespondent).
                        matchToEnumConstant = parsedRole.ToString().TrimWhitespaceAndBOM().Replace("-", string.Empty);

                        if (CompanyRoles.TryGetValue(matchToEnumConstant, out roleToAdd))
                        {
                            pb.Roles.Add(roleToAdd);
                        }
                    }
                }

                // We need to remove any spaces in the authority name, as the constants in 
                // E_UnderwritingAuthority contain no spaces.
                string underwritingAuthorityName = row[(int)OCImporterColumns.UnderwritingAuthority].ToString().TrimWhitespaceAndBOM().Replace(" ", string.Empty);

                E_UnderwritingAuthority addAuthority;

                if (UnderwritingAuthorities.TryGetValue(underwritingAuthorityName, out addAuthority))
                {
                    pb.UnderwritingAuthority = addAuthority;
                }

                PmlCompanyTier tierWithName = addingBrokerCompanyTierList.GetByName(row[(int)OCImporterColumns.CompanyTier].ToString().TrimWhitespaceAndBOM());

                if (tierWithName != null)
                {
                    pb.PmlCompanyTierId = tierWithName.Id;
                }

                // If any new Company Roles are added to E_OCRoles, make sure to add a new check
                // here to set that new role's BranchID, LpePriceGroupId and landing page as appropriate.
                pb.BranchId = GetGuidKeyFromListUsingNameValue(row[(int)OCImporterColumns.BrokerBranch].ToString().TrimWhitespaceAndBOM(), addingBrokerBranches);
                pb.LpePriceGroupId = GetGuidKeyFromListUsingNameValue(row[(int)OCImporterColumns.BrokerPriceGroup].ToString().TrimWhitespaceAndBOM(), addingBrokerPriceGroups);
                pb.TPOPortalConfigID = GetGuidKeyFromListUsingNameValue(row[(int)OCImporterColumns.BrokerLandingPage].ToString().TrimWhitespaceAndBOM(), addingBrokerLandingPages);

                pb.MiniCorrespondentBranchId = GetGuidKeyFromListUsingNameValue(row[(int)OCImporterColumns.MiniCorrespondantBranch].ToString().TrimWhitespaceAndBOM(), addingBrokerBranches);
                pb.MiniCorrespondentLpePriceGroupId = GetGuidKeyFromListUsingNameValue(row[(int)OCImporterColumns.MiniCorrespondantPriceGroup].ToString().TrimWhitespaceAndBOM(), addingBrokerPriceGroups);
                pb.MiniCorrespondentTPOPortalConfigID = GetGuidKeyFromListUsingNameValue(row[(int)OCImporterColumns.MiniCorrespondentLandingPage].ToString().TrimWhitespaceAndBOM(), addingBrokerLandingPages);

                pb.CorrespondentBranchId = GetGuidKeyFromListUsingNameValue(row[(int)OCImporterColumns.CorrespondantBranch].ToString().TrimWhitespaceAndBOM(), addingBrokerBranches);
                pb.CorrespondentLpePriceGroupId = GetGuidKeyFromListUsingNameValue(row[(int)OCImporterColumns.CorrespondantPriceGroup].ToString().TrimWhitespaceAndBOM(), addingBrokerPriceGroups);
                pb.CorrespondentTPOPortalConfigID = GetGuidKeyFromListUsingNameValue(row[(int)OCImporterColumns.CorrespondentLandingPage].ToString().TrimWhitespaceAndBOM(), addingBrokerLandingPages);

                //// IR - OPM 236101: Wholesale Channel Branch and Price Group must be specified when updating an existing PML Broker.
                if (compIdMapPmlBroker.ContainsKey(key) && !pb.BranchId.HasValue)
                {
                    errord = true;
                    errorList.Rows.Add("Error on line " + line + ": " + "Originating Company with Id " + pb.CompanyId + " needs to be associated with a valid Broker Branch, regardless of role");
                }

                if (compIdMapPmlBroker.ContainsKey(key) && !pb.LpePriceGroupId.HasValue)
                {
                    errord = true;
                    errorList.Rows.Add("Error on line " + line + ": " + "Originating Company with Id " + pb.CompanyId + " needs to be associated with a valid Broker Price Group, regardless of role");
                }

                pb.Fax = row[(int)OCImporterColumns.Fax].ToString();
                pb.TaxId = row[(int)OCImporterColumns.TaxID].ToString();
                pb.PrincipalName1 = row[(int)OCImporterColumns.PrincipalName1].ToString();
                pb.PrincipalName2 = row[(int)OCImporterColumns.PrincipalName2].ToString();
                pb.NmLsIdentifier = row[(int)OCImporterColumns.CompanyNMLS].ToString();

                bool suc = false;
                bool boolVal;
                suc = bool.TryParse(row[(int)OCImporterColumns.CompOnlyForFirstLien].ToString(), out boolVal);
                if (suc)
                {
                    pb.OriginatorCompensationIsOnlyPaidForFirstLienOfCombo = boolVal;
                }

                Decimal decVal;
                suc = Decimal.TryParse(row[(int)OCImporterColumns.CompPercent].ToString(), out decVal);
                if (suc)
                {
                    pb.OriginatorCompensationPercent = decVal;
                }

                suc = Decimal.TryParse(row[(int)OCImporterColumns.CompDollarAmount].ToString(), out decVal);
                if (suc)
                {
                    pb.OriginatorCompensationFixedAmount = decVal;
                }

                suc = Decimal.TryParse(row[(int)OCImporterColumns.CompCeiling].ToString(), out decVal);
                if (suc)
                {
                    pb.OriginatorCompensationMaxAmount = decVal;
                }

                suc = Decimal.TryParse(row[(int)OCImporterColumns.CompFloor].ToString(), out decVal);
                if (suc)
                {
                    pb.OriginatorCompensationMinAmount = decVal;
                }

                string compensationType = row[(int)OCImporterColumns.CompensationType].ToString().Replace(" ", string.Empty);

                E_PercentBaseT brokerCompensationType;

                if (LoanCompensationTypes.TryGetValue(compensationType, out brokerCompensationType))
                {
                    pb.OriginatorCompensationBaseT = brokerCompensationType;
                }

                pb.OriginatorCompensationNotes = row[(int)OCImporterColumns.CompNotes].ToString();

                string curLicenseNum = row[(int)OCImporterColumns.LicenseNumber].ToString();
                string curLicenseState = row[(int)OCImporterColumns.LicenseState].ToString();
                string curExpDate = row[(int)OCImporterColumns.LicenseExpirationDate].ToString();

                // OPM 235672, 12/30/2015, ML
                // Origination company import files do not have to specify license information,
                // so we'll only attempt to parse what's entered in the file if the number,
                // state, and expiration date are all specified.
                if (!string.IsNullOrWhiteSpace(curLicenseNum) &&
                    !string.IsNullOrWhiteSpace(curLicenseState) &&
                    !string.IsNullOrWhiteSpace(curExpDate))
                {
                    if (pb.LicenseInfoList.Exists(curLicenseNum))
                    {
                        pb.LicenseInfoList.UpdateLicense(curLicenseNum, curLicenseState, curExpDate);
                    }
                    else
                    {
                        pb.LicenseInfoList.Add(new LicenseInfo(curLicenseState, curExpDate, curLicenseNum));
                    }
                }

                var ocStatus = OCStatusType.Blank;

                if (pb.Roles.Contains(E_OCRoles.Broker))
                {
                    var brokerStatus = row[(int)OCImporterColumns.BrokerStatus].ToString();

                    if (Enum.TryParse(brokerStatus, ignoreCase: true, result: out ocStatus) == false)
                    {
                        errord = true;
                        errorList.Rows.Add("Error on line " + line + ": Unknown Broker OC status type '" + brokerStatus + "'");
                    }
                    else
                    {
                        pb.BrokerRoleStatusT = ocStatus;
                    }
                }

                if (pb.Roles.Contains(E_OCRoles.MiniCorrespondent))
                {
                    var miniCorrStatus = row[(int)OCImporterColumns.MiniCorrespondentStatus].ToString();

                    if (Enum.TryParse(miniCorrStatus, ignoreCase: true, result: out ocStatus) == false)
                    {
                        errord = true;
                        errorList.Rows.Add("Error on line " + line + ": Unknown MiniCorrespondent OC status type '" + miniCorrStatus + "'");
                    }
                    else
                    {
                        pb.MiniCorrRoleStatusT = ocStatus;
                    }
                }

                if (pb.Roles.Contains(E_OCRoles.Correspondent))
                {
                    var corrStatus = row[(int)OCImporterColumns.CorrespondantStatus].ToString();

                    if (Enum.TryParse(corrStatus, ignoreCase: true, result: out ocStatus) == false)
                    {
                        errord = true;
                        errorList.Rows.Add("Error on line " + line + ": Unknown Correspondent OC status type '" + corrStatus + "'");
                    }
                    else
                    {
                        pb.CorrRoleStatusT = ocStatus;
                    }
                }

                this.SetPmlBrokerRelationships(pb, BrokerId, row, errorList);

                if (!compIdMapPmlBroker.ContainsKey(curCompanyId))
                {
                    compIdMapPmlBroker.Add(curCompanyId, pb);
                }

                toSave.AddLast(pb);
            }
            
            //We want to return whether or not parsing was successful, so were there no errors?
            return !errord;
        }

        private bool SetPmlBrokerRelationships(PmlBroker pmlBroker, Guid brokerId, DataRow row, DataTable errorList)
        {
            BrokerLoanAssignmentTable possibleEmployees = new BrokerLoanAssignmentTable();
            bool hasErrors = false;
            foreach (var pair in this.columnToRoleMapping)
            {
                OCImporterColumns column = pair.Key;
                var loginName = row[(int)column].ToString();
                if (!string.IsNullOrEmpty(loginName))
                {
                    E_RoleT role = pair.Value.Item1;
                    PmlBrokerRelationship pbRelationship;
                    if (pair.Value.Item2 == E_OCRoles.Broker)
                    {
                        pbRelationship = pmlBroker.BrokerRoleRelationships;
                    }
                    else if (pair.Value.Item2 == E_OCRoles.Correspondent)
                    {
                        pbRelationship = pmlBroker.CorrRoleRelationships;
                    }
                    else
                    {
                        pbRelationship = pmlBroker.MiniCorrRoleRelationships;
                    }
                    if (possibleEmployees[role] == null)
                    {
                        possibleEmployees.Retrieve(brokerId, Guid.Empty, string.Empty, role, E_EmployeeStatusFilterT.ActiveOnly);
                    }

                    IEnumerable<BrokerLoanAssignmentTable.Spec> usersForThisRole = possibleEmployees[role];
                    var foundUser = usersForThisRole.FirstOrDefault((spec) => spec.LoginName.Equals(loginName, StringComparison.OrdinalIgnoreCase));
                    if (foundUser == null)
                    {
                        errorList.Rows.Add($"User not found.  Column: {pair.Key.ToString()} Login name: {loginName}.");
                        hasErrors = true;
                    }
                    else
                    {
                        pbRelationship.SetRelationship(role, foundUser.EmployeeId, foundUser.FullName);
                    }
                }
            }

            return hasErrors;
        }

        // OPM 199559, 8/18/2015, ML
        private Guid? GetGuidKeyFromListUsingNameValue(string nameValue, IEnumerable<KeyValuePair<Guid, string>> list)
        {
            if (string.IsNullOrEmpty(nameValue))
            {
                return null;
            }

            var landingPageSearch = list.FirstOrDefault(pair => pair.Value.Equals(nameValue));

            Guid? returnGuid = null;

            if (!landingPageSearch.Equals(default(KeyValuePair<Guid, string>)))
            {
                returnGuid = landingPageSearch.Key;
            }

            return returnGuid;
        }
    }

}
