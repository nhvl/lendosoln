﻿/// <copyright file="EdocsImporter.cs" company="MeridianLink">
///     Copyright (c) MeridianLink. All rights reserved.
/// </copyright>
/// <summary>
/// Author: Michael Leinweaver
/// Date:   1/18/2015
/// </summary>
namespace LendersOffice.BrokerAdmin.Importers.Edocs
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using DataAccess;
    using EDocs;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Common.TextImport;
    using LendersOffice.Edocs.DocMagic;
    using LendersOffice.ObjLib.Edocs;
    using LendersOffice.ObjLib.Edocs.DocuTech;
    using LendersOffice.ObjLib.Edocs.IDS;

    /// <summary>
    /// Represents an importer for document folders, document types, and
    /// Lender's Office to document vendor document type mappings.
    /// </summary>
    public class EdocsImporter
    {
        /// <summary>
        /// The number of columns present and required in the document type import file.
        /// </summary>
        private const int DocTypeTotalColumns = 2;

        /// <summary>
        /// The number of columns present and required in the document type import file.
        /// </summary>
        private const int FolderTotalColumns = 2;

        /// <summary>
        /// The number of columns present in the document type mapping file.
        /// </summary>
        private const int MappingTotalColumns = 4;

        /// <summary>
        /// The number of columns required in the document type mapping file.
        /// </summary>
        private const int MappingReqColumns = 1;

        /// <summary>
        /// Represents the maximum length of a folder name in the database.
        /// </summary>
        private const int MaximumFolderNameLength = 50;

        /// <summary>
        /// Represents the maximum length of a doc type name in the database.
        /// </summary>
        private const int MaximumDocTypeNameLength = 50;

        /// <summary>
        /// The dummy name of the sheet in the import file.
        /// </summary>
        private const string DummySheetName = "BLANK";

        /// <summary>
        /// Represents the possible separators between the words of a role name.
        /// </summary>
        private const string RoleNamePotentialSeparators = @"[-\s:;]";

        /// <summary>
        /// Indicates whether case should be ignored when parsing folder roles.
        /// </summary>
        private const bool IgnoreRoleCase = true;

        /// <summary>
        /// The character used to separate the roles allowed to access an imported folder.
        /// </summary>
        private static readonly char[] FolderRoleSeparator = new char[] { ',' };

        /// <summary>
        /// Represents the names of the columns present in the document
        /// folder import file.
        /// </summary>
        private enum FolderFileColumns
        {
            /// <summary>
            /// The name of the containing folder for the document type.
            /// </summary>
            FolderName,

            /// <summary>
            /// The comma-separated list of roles that are allowed to access
            /// the folder.
            /// </summary>
            IncludedRoles
        }

        /// <summary>
        /// Represents the names of the columns present in the document
        /// type import file.
        /// </summary>
        private enum DocTypeFileColumns
        {
            /// <summary>
            /// The name of the containing folder for the document type.
            /// </summary>
            FolderName,

            /// <summary>
            /// The name of the document type to import.
            /// </summary>
            DocTypeName
        }

        /// <summary>
        /// Represents the names of the columns present in the mappings 
        /// import file.
        /// </summary>
        private enum MappingFileColumns
        {
            /// <summary>
            /// The name of the Lender's Office document type.
            /// </summary>
            LQBDocType,

            /// <summary>
            /// The name of the DocMagic document type.
            /// </summary>
            DocMagicDoctype,

            /// <summary>
            /// The name of the <code>DocuTech</code> document type.
            /// </summary>
            DocuTechDoctype,

            /// <summary>
            /// The name of the IDS document type.
            /// </summary>
            IDSDoctype,
        }

        /// <summary>
        /// Imports a set of folders from the specified <paramref name="path"/>
        /// for the broker with the specified <paramref name="brokerId"/>.
        /// </summary>
        /// <param name="brokerId">
        /// The ID of the broker to import folders.
        /// </param>
        /// <param name="path">
        /// The path to the CSV file containing folders to import.
        /// </param>
        public static void ImportFolders(Guid brokerId, string path)
        {
            var table = EdocsImporter.ParseDataTable(
                path,
                EdocsImporter.FolderTotalColumns,
                EdocsImporter.FolderTotalColumns);

            if (table.Rows.Count < 1)
            {
                throw new InvalidOperationException(ErrorMessages.EdocsImporter.NoFoldersSpecifiedMessage);
            }

            var existingFolders = EDocumentFolder.GetFoldersInBroker(brokerId).ToDictionary(
    f => f.FolderNm.Trim(),
    StringComparer.OrdinalIgnoreCase);

            foreach (var folder in EdocsImporter.GetFoldersFromDataTable(table, existingFolders))
            {
                var edocFolder = EDocumentFolder.Create(brokerId, folder.Value);

                edocFolder.FolderNm = folder.Key.ToUpper();

                // 10/12/2017 - dd - Save each folder in its own transaction. Do not try to optimize and save all folders in single transaction.
                // The code of EDocumentFolder is not friendly with saving multiple folders in single transaction. You will either get connection leak
                // or timeout when put everything in single transaction.
                edocFolder.Save();
            }

            using (var exec = new CStoredProcedureExec(brokerId))
            {
                exec.BeginTransactionForWrite();

                try
                {
                    EDocumentFolder.CreateUnclassifiedFolderAndSave(brokerId, exec);

                    exec.CommitTransaction();
                }
                catch
                {
                    exec.RollbackTransaction();

                    throw;
                }
            }
        }

        /// <summary>
        /// Imports a set of document types from the specified <paramref name="path"/>
        /// for the broker with the specified <paramref name="brokerId"/>.
        /// </summary>
        /// <param name="brokerId">
        /// The ID of the broker to import document types.
        /// </param>
        /// <param name="path">
        /// The path to the CSV file containing document types to import.
        /// </param>
        public static void ImportDocTypes(Guid brokerId, string path)
        {
            var table = EdocsImporter.ParseDataTable(
                path,
                EdocsImporter.DocTypeTotalColumns,
                EdocsImporter.DocTypeTotalColumns);

            if (table.Rows.Count < 1)
            {
                throw new InvalidOperationException(ErrorMessages.EdocsImporter.NoDocTypesSpecifiedMessage);
            }

            using (var exec = new CStoredProcedureExec(brokerId))
            {
                exec.BeginTransactionForWrite();

                try
                {
                    var existingFolders = EDocumentFolder.GetFoldersInBroker(brokerId).ToDictionary(
                        folder => folder.FolderNm.Trim(),
                        StringComparer.OrdinalIgnoreCase);

                    var existingDocTypesToIds = EDocumentDocType.GetDocTypesByBroker(brokerId, EnforcePermissions: false).ToDictionary(
                        docType => docType.DocTypeName.Trim(),
                        docType => docType.Id,
                        StringComparer.OrdinalIgnoreCase);

                    List<string> stackOrderedDocTypes;

                    foreach (var folderDocTypeTuple in EdocsImporter.GetDocTypesFromTable(table, existingFolders, existingDocTypesToIds, out stackOrderedDocTypes))
                    {
                        var folderId = existingFolders[folderDocTypeTuple.Key].FolderId;

                        foreach (var docTypeName in folderDocTypeTuple.Value)
                        {
                            var docTypeId = EDocumentDocType.AddDocType(docTypeName, brokerId, exec);

                            EDocumentFolder.AssignDocTypeToFolder(folderId, docTypeId, exec);

                            existingDocTypesToIds.Add(docTypeName, docTypeId);
                        }
                    }

                    var broker = BrokerDB.RetrieveById(brokerId);

                    broker.SetStackOrder(string.Join(",", stackOrderedDocTypes.Select(docTypeName => existingDocTypesToIds[docTypeName])));

                    broker.Save(exec);

                    exec.CommitTransaction();
                }
                catch
                {
                    exec.RollbackTransaction();

                    throw;
                }
            }
        }

        /// <summary>
        /// Imports a set of mappings from the specified <paramref name="path"/>
        /// for the broker with the specified <paramref name="brokerId"/>.
        /// </summary>
        /// <param name="brokerId">
        /// The ID of the broker to import mappings.
        /// </param>
        /// <param name="path">
        /// The path to the CSV file containing mappings to import.
        /// </param>
        public static void ImportMappings(Guid brokerId, string path)
        {
            var table = EdocsImporter.ParseDataTable(
                path,
                EdocsImporter.MappingTotalColumns,
                EdocsImporter.MappingReqColumns);

            if (table.Rows.Count < 1)
            {
                throw new InvalidOperationException(ErrorMessages.EdocsImporter.NoMappingsSpecifiedMessage);
            }

            using (var exec = new CStoredProcedureExec(brokerId))
            {
                exec.BeginTransactionForWrite();

                try
                {
                    var brokerDocTypes = EDocumentDocType.GetDocTypesByBroker(brokerId, EnforcePermissions: false).ToDictionary(
                        docType => docType.DocTypeName.Trim(),
                        StringComparer.OrdinalIgnoreCase);

                    var docMagicMapper = DocTypeMapperFactory.Create(E_DocBarcodeFormatT.DocMagic, brokerId);

                    var docuTechMapper = DocTypeMapperFactory.Create(E_DocBarcodeFormatT.DocuTech, brokerId);

                    var idsMapper = DocTypeMapperFactory.Create(E_DocBarcodeFormatT.IDS, brokerId);

                    foreach (var mapping in EdocsImporter.GetDocTypesWithBarcodesFromTable(table, brokerDocTypes))
                    {
                        if (mapping.Value.DocMagicDocType != null)
                        {
                            docMagicMapper.SetAssociation(mapping.Key, new IDocVendorDocType[] { mapping.Value.DocMagicDocType });
                        }

                        if (mapping.Value.DocuTechDocType != null)
                        {
                            docuTechMapper.SetAssociation(mapping.Key, new IDocVendorDocType[] { mapping.Value.DocuTechDocType });
                        }

                        if (mapping.Value.IDSDocType != null)
                        {
                            idsMapper.SetAssociation(mapping.Key, new IDocVendorDocType[] { mapping.Value.IDSDocType });
                        }
                    }
                }
                catch
                {
                    exec.RollbackTransaction();

                    throw;
                }
            }
        }

        /// <summary>
        /// Obtains a list of the folders specified in the import file
        /// by parsing the entries in <paramref name="roleImportTable"/>
        /// and ignoring any existing entries in <paramref name="existingFolders"/>.
        /// </summary>
        /// <param name="roleImportTable">
        /// The table containing folders to import.
        /// </param>
        /// <param name="existingFolders">
        /// A list of the existing folders for the importing broker.
        /// </param>
        /// <returns>
        /// A dictionary of folder names to authorized roles.
        /// </returns>
        private static Dictionary<string, List<ImportedFolderInfo>> GetFoldersFromDataTable(
            DataTable roleImportTable,
            Dictionary<string, EDocumentFolder> existingFolders)
        {
            var importedFolders = new Dictionary<string, List<ImportedFolderInfo>>(
                StringComparer.OrdinalIgnoreCase);

            foreach (DataRow row in roleImportTable.Rows)
            {
                var folderName = row[(int)EdocsImporter.FolderFileColumns.FolderName].ToString().Trim();

                if (existingFolders.ContainsKey(folderName))
                {
                    continue;
                }

                if (importedFolders.ContainsKey(folderName))
                {
                    var errorMsg = string.Format(
                        ErrorMessages.EdocsImporter.DuplicateFoldersFoundMessageFormat,
                        folderName);

                    throw new InvalidOperationException(errorMsg);
                }

                if (folderName.Length > EdocsImporter.MaximumFolderNameLength)
                {
                    var errorMessage = string.Format(
                        ErrorMessages.EdocsImporter.FolderNameExceedsMaxLengthMessageFormat,
                        folderName,
                        EdocsImporter.MaximumFolderNameLength);

                    throw new InvalidOperationException(errorMessage);
                }

                importedFolders.Add(
                    folderName,
                    EdocsImporter.GetFolderRolesFromRow(row));
            }

            return importedFolders;
        }

        /// <summary>
        /// Obtains a list of the roles that are authorized to view a folder
        /// from the specified <paramref name="row"/>.
        /// </summary>
        /// <param name="row">
        /// The row for a folder that contains authorized roles.
        /// </param>
        /// <returns>
        /// A list of <see cref="ImportedFolderInfo"/> objects representing the 
        /// roles authorized to view a folder.
        /// </returns>
        private static List<ImportedFolderInfo> GetFolderRolesFromRow(DataRow row)
        {
            var folderInfoList = new List<ImportedFolderInfo>();

            var roleList = row[(int)EdocsImporter.FolderFileColumns.IncludedRoles]
                .ToString()
                .Split(EdocsImporter.FolderRoleSeparator, StringSplitOptions.RemoveEmptyEntries)
                .Select(roleName => Regex.Replace(roleName, EdocsImporter.RoleNamePotentialSeparators, string.Empty))
                .Distinct();

            foreach (var roleName in roleList)
            {
                CEmployeeFields.E_Role roleEnum;

                var isPmlRole = false;

                // This is a special case role, as there is no Pml supervisor constant
                // in CEmployeeFields. Instead, a role is considered a Pml supervisor
                // if it is an administrator and a pml role.
                if (roleName.Equals("ExternalSupervisor", StringComparison.OrdinalIgnoreCase))
                {
                    roleEnum = CEmployeeFields.E_Role.Administrator;

                    isPmlRole = true;
                }
                else if (Enum.TryParse(roleName, EdocsImporter.IgnoreRoleCase, out roleEnum))
                {
                    isPmlRole = CEmployeeFields.IsPmlRole(roleEnum);
                }
                else
                {
                    var errorMsg = string.Format(
                        ErrorMessages.EdocsImporter.InvalidRolePresentMessageFormat,
                        roleName);

                    throw new InvalidOperationException(errorMsg);
                }

                folderInfoList.Add(new ImportedFolderInfo()
                {
                    RoleId = CEmployeeFields.GetRoleId(roleEnum),
                    IsPmlUser = isPmlRole
                });
            }

            return folderInfoList;
        }

        /// <summary>
        /// Obtains a list of the document types specified in the import file
        /// by parsing the entries in <paramref name="table"/> and ignoring any 
        /// existing document types in <paramref name="existingDocTypesToIds"/>.
        /// </summary>
        /// <param name="table">
        /// The table containing document types to import.
        /// </param>
        /// <param name="existingFolders">
        /// A list of the existing folders for the importing broker.
        /// </param>
        /// <param name="existingDocTypesToIds">
        /// A list of the existing document types for the importing broker.
        /// </param>
        /// <param name="stackOrderedDocTypes">
        /// A list containing the stack order for the doc types. Existing doc types
        /// will be added to this stack order.
        /// </param>
        /// <returns>
        /// A dictionary of folder names to document type names to import.
        /// </returns>
        private static Dictionary<string, List<string>> GetDocTypesFromTable(
            DataTable table,
            Dictionary<string, EDocumentFolder> existingFolders,
            Dictionary<string, int> existingDocTypesToIds,
            out List<string> stackOrderedDocTypes)
        {
            stackOrderedDocTypes = new List<string>();

            var docTypesByFolderName = new Dictionary<string, List<string>>(
                StringComparer.OrdinalIgnoreCase);

            var seenDocTypeNames = new HashSet<string>(StringComparer.OrdinalIgnoreCase);

            foreach (DataRow row in table.Rows)
            {
                var folderName = row[(int)EdocsImporter.DocTypeFileColumns.FolderName].ToString().Trim();

                if (!existingFolders.ContainsKey(folderName))
                {
                    var errorMsg = string.Format(
                        ErrorMessages.EdocsImporter.UnknownFolderSpecifiedMessageFormat,
                        folderName);

                    throw new InvalidOperationException(errorMsg);
                }

                var docTypeName = row[(int)EdocsImporter.DocTypeFileColumns.DocTypeName].ToString().Trim();

                stackOrderedDocTypes.Add(docTypeName);

                if (existingDocTypesToIds.ContainsKey(docTypeName))
                {
                    continue;
                }

                if (seenDocTypeNames.Contains(docTypeName))
                {
                    var errorMessage = string.Format(
                        ErrorMessages.EdocsImporter.DuplicateDocTypeFoundMessageFormat,
                        docTypeName);

                    throw new InvalidOperationException(errorMessage);
                }

                if (docTypeName.Length > EdocsImporter.MaximumDocTypeNameLength)
                {
                    var errorMessage = string.Format(
                        ErrorMessages.EdocsImporter.DocTypeNameExceedsMaxLengthMessageFormat,
                        docTypeName,
                        folderName,
                        EdocsImporter.MaximumDocTypeNameLength);

                    throw new InvalidOperationException(errorMessage);
                }

                seenDocTypeNames.Add(docTypeName);

                if (docTypesByFolderName.ContainsKey(folderName))
                {
                    docTypesByFolderName[folderName].Add(docTypeName);
                }
                else
                {
                    docTypesByFolderName.Add(
                        folderName,
                        new List<string>() { docTypeName });
                }
            }

            return docTypesByFolderName;
        }

        /// <summary>
        /// Obtains a list of the document type vendor mappings specified in the 
        /// import file by parsing the entries in the specified <paramref name="table"/>.
        /// </summary>
        /// <param name="table">
        /// The table containing document type vendor mappings to import.
        /// </param>
        /// <param name="brokerDocTypes">
        /// The dictionary of broker document types by document type name.
        /// </param>
        /// <returns>
        /// A dictionary of vendor document type mappings by Lender's Office document type name.
        /// </returns>
        private static Dictionary<int, DocTypeMappingInfo> GetDocTypesWithBarcodesFromTable(
            DataTable table,
            Dictionary<string, DocType> brokerDocTypes)
        {
            var docMagicDocTypes = DocMagicDocType.ListAll();

            var docuTechDocTypes = DocuTechDocType.ListAll();

            var idsDocTypes = IDSDocType.ListAll();

            var mappingsByDocType = new Dictionary<int, DocTypeMappingInfo>();

            foreach (DataRow row in table.Rows)
            {
                var docTypeName = row[(int)EdocsImporter.MappingFileColumns.LQBDocType].ToString().Trim();

                if (!brokerDocTypes.ContainsKey(docTypeName))
                {
                    var errorMsg = string.Format(
                        ErrorMessages.EdocsImporter.UnknownDocTypeSpecifiedMessageFormat,
                        docTypeName);

                    throw new InvalidOperationException(errorMsg);
                }

                var docTypeId = int.Parse(brokerDocTypes[docTypeName].DocTypeId);

                if (mappingsByDocType.ContainsKey(docTypeId))
                {
                    var errorMessage = string.Format(
                        ErrorMessages.EdocsImporter.DuplicateDocTypeMappingFoundMessageFormat,
                        docTypeName);

                    throw new InvalidOperationException(errorMessage);
                }

                var docMagicDocType = EdocsImporter.GetDocVenderDocTypeFromRow(
                    row,
                    EdocsImporter.MappingFileColumns.DocMagicDoctype,
                    docMagicDocTypes,
                    E_DocBarcodeFormatT.DocMagic);

                var docuTechDocType = EdocsImporter.GetDocVenderDocTypeFromRow(
                    row,
                    EdocsImporter.MappingFileColumns.DocuTechDoctype,
                    docuTechDocTypes,
                    E_DocBarcodeFormatT.DocuTech);

                var idsDocType = EdocsImporter.GetDocVenderDocTypeFromRow(
                    row,
                    EdocsImporter.MappingFileColumns.IDSDoctype,
                    idsDocTypes,
                    E_DocBarcodeFormatT.IDS);

                var mappingInfo = new DocTypeMappingInfo()
                {
                    DocMagicDocType = docMagicDocType,
                    DocuTechDocType = docuTechDocType,
                    IDSDocType = idsDocType
                };

                mappingsByDocType.Add(docTypeId, mappingInfo);
            }

            return mappingsByDocType;
        }

        /// <summary>
        /// Obtains a tuple of document vendor mappings from the specified
        /// <paramref name="row"/>.
        /// </summary>
        /// <param name="row">
        /// The row containing document vendor mappings.
        /// </param>
        /// <param name="column">
        /// The column corresponding to the document vendor mapping description.
        /// </param>
        /// <param name="docTypes">
        /// The list of vendor document types to search based on the description
        /// given in the import file.
        /// </param>
        /// <param name="vendor">
        /// The document vendor for the specified <paramref name="column"/>.
        /// </param>
        /// <returns>
        /// A document vendor-specific document type containing an ID, description,
        /// and barcode classification.
        /// </returns>
        private static IDocVendorDocType GetDocVenderDocTypeFromRow(
            DataRow row,
            MappingFileColumns column,
            IEnumerable<IDocVendorDocType> docTypes,
            E_DocBarcodeFormatT vendor)
        {
            var description = row[(int)column].ToString().Trim();

            if (string.IsNullOrWhiteSpace(description))
            {
                return null;
            }

            var foundDocType = docTypes.FirstOrDefault(docType => vendor == E_DocBarcodeFormatT.IDS ?
                ((IDSDocType)docType).InternalDescription.Equals(description, StringComparison.OrdinalIgnoreCase) :
                docType.Description.Equals(description, StringComparison.OrdinalIgnoreCase));

            if (foundDocType == null)
            {
                var errorString = string.Format(
                    ErrorMessages.EdocsImporter.InvalidDocVendorDocTypeMessageFormat,
                    Enum.GetName(typeof(E_DocBarcodeFormatT), vendor),
                    description);

                throw new InvalidOperationException(errorString);
            }

            return foundDocType;
        }

        /// <summary>
        /// Parses the data table at the specified <paramref name="path"/> with the specified 
        /// number of total and required columns.
        /// </summary>
        /// <param name="path">
        /// The path to the CSV file containing document types to import.
        /// </param>
        /// <param name="numColumns">
        /// The total number of columns in the CSV file.
        /// </param>
        /// <param name="reqColumns">
        /// The number of required columns in the CSV file.
        /// </param>
        /// <returns>
        /// A <see cref="DataTable"/> comprised of the content present in the CSV file.
        /// </returns>
        private static DataTable ParseDataTable(string path, int numColumns, int reqColumns)
        {
            var csv = DataParsing.FileToCSV(
                path,
                KeepFirstRow: true,
                SheetName: EdocsImporter.DummySheetName);

            ILookup<int, string> parseErrors;

            var table = DelimitedListParser.CsvToDataTable(
                csv,
                hasHeader: true,
                reqColumns: numColumns,
                numColumns: reqColumns,
                errors: out parseErrors);

            if (parseErrors.Any())
            {
                var errorBuilder = new StringBuilder();

                foreach (var errorGrouping in parseErrors)
                {
                    foreach (var errorDescription in errorGrouping.ToList())
                    {
                        errorBuilder.AppendFormat(
                            ErrorMessages.EdocsImporter.InvalidImportFileLineMessageFormat,
                            errorGrouping.Key,
                            errorDescription);
                    }
                }

                throw new InvalidOperationException(errorBuilder.ToString());
            }

            return table;
        }
    }
}
