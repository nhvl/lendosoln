﻿/// <copyright file="DocTypeMappingInfo.cs" company="MeridianLink">
///     Copyright (c) MeridianLink. All rights reserved.
/// </copyright>
/// <summary>
/// Author: Michael Leinweaver
/// Date:   1/18/2015
/// </summary>
namespace LendersOffice.BrokerAdmin.Importers.Edocs
{
    using LendersOffice.ObjLib.Edocs;

    /// <summary>
    /// Encapsulates information regarding document type mappings
    /// for DocMagic, <code>DocuTech</code>, and IDS.
    /// </summary>
    public class DocTypeMappingInfo
    {
        /// <summary>
        /// Gets or sets the value for the DocMagic document type.
        /// </summary>
        /// <value>The <see cref="DocMagicDocType"/>.</value>
        public IDocVendorDocType DocMagicDocType { get; set; }

        /// <summary>
        /// Gets or sets the value for the <code>DocuTech</code> 
        /// document type.
        /// </summary>
        /// <value>The <see cref="DocuTechDocType"/>.</value>
        public IDocVendorDocType DocuTechDocType { get; set; }

        /// <summary>
        /// Gets or sets the value for the IDS document type.
        /// </summary>
        /// <value>The <see cref="IDSDocType"/>.</value>
        public IDocVendorDocType IDSDocType { get; set; }
    }
}
