﻿/// <copyright file="ImportedFolderInfo.cs" company="MeridianLink">
///     Copyright (c) MeridianLink. All rights reserved.
/// </copyright>
/// <summary>
/// Author: Michael Leinweaver
/// Date:   1/18/2015
/// </summary>
namespace LendersOffice.BrokerAdmin.Importers.Edocs
{
    using System;

    /// <summary>
    /// Encapsulates information for imported folders, including
    /// the ID of the folder and whether the folder is for a PML
    /// role.
    /// </summary>
    public class ImportedFolderInfo
    {
        /// <summary>
        /// Gets or sets the value for the role ID of the folder.
        /// </summary>
        /// <value>The GUID for the folder.</value>
        public Guid RoleId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the folder is for PML roles.
        /// </summary>
        /// <value>True if the folder is for a PML role, false otherwise.</value>
        public bool IsPmlUser { get; set; }
    }
}
