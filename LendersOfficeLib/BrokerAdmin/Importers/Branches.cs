﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Common.TextImport;


namespace LendersOffice.BrokerAdmin.Importers
{

    public class BranchImporter : AbstractImporter
    {
        //Configurable number of columns in case we add more later.
        private const int numColumns = 12;

        //And the first reqColumns are required - errors will be thrown if they're empty.
        private readonly string[] reqColumns = { "Name", "Display Name", "Branch Code", "Channel", "Street", "City", "State", "Zip", "Phone" };
        private readonly string[] allColumns = { "Name", "Display Name", "Branch Code", "Channel", "Street", "City", "State", "Zip", "Phone", "Fax", "Default Price Group", "Branch Groups", "NMLS Id" };

        private const string _sheetName = "Branches";

        public BranchImporter()
        {
            HeaderText = string.Join(", ", allColumns);
            SheetName = _sheetName;
        }

        private string GetSavedBranches(IEnumerable<PmlBroker> saved)
        {
            if (saved.Count() == 0)
            {
                return "No branches were saved";
            }

            StringBuilder sb = new StringBuilder("The following branches were successfully saved:\n");
            foreach (PmlBroker pb in saved)
            {
                sb.AppendLine(pb.Name);
            }

            return sb.ToString();
        }

        public override void Import(string csv, Guid BrokerId, out string Results, out DataTable ErrorList)
        {
            ErrorList = new DataTable();
            Results = "";
            try
            {
                List<BranchDB> toSave;
                List<Action<Guid>> afterSave;
                bool parseSuccess = ParseCSV(csv, BrokerId, out toSave, out afterSave, out ErrorList);

                if (parseSuccess)
                {
                    /*
                    //We parsed everything, so go ahead and save it all
                    foreach (PmlBroker pb in toSave)
                    {
                        try
                        {
                            pb.Save();
                        }
                        catch (SqlException e)
                        {
                            ErrorList.Rows.Add("Could not save branch: " + pb.Name + "! This is probably due to a db problem. No further imports will be made, but all branches before this one were saved.");
                            ErrorList.Rows.Add("Error text: " + e.ToString());
                            //Take everything up until the erroring pml broker
                            Results = GetSavedBranches(toSave.TakeWhile((b) => !b.Equals(pb)));
                            throw;
                        }
                        catch (Exception e)
                        {
                            ErrorList.Rows.Add("Could not save branch: " + pb.Name + "! This is a totally unexpected exception. No further imports will be made, but all branches before this one were saved.");
                            ErrorList.Rows.Add("Error text: " + e.ToString());
                            Results = GetSavedBranches(toSave.TakeWhile((b) => !b.Equals(pb)));
                            throw;
                        }
                    }
                    //If we get here, there were no issues saving.
                    Results = GetSavedBranches(toSave);
                     */
                }
            }
            catch (Exception e)
            {
                Tools.LogError("Failed to import branches: " + e.Message + ".", e);
                throw;
            }
        }

        private bool ParseCSV(string csv, Guid BrokerId, out List<BranchDB> toSave, out List<Action<Guid>> afterSave, out DataTable errorList)
        {
            toSave = new List<BranchDB>();
            afterSave = new List<Action<Guid>>();
            errorList = new DataTable();
            errorList.Columns.Add(new DataColumn("Errors", typeof(string)));

            ILookup<int, string> parseErrors;
            DataTable data = DelimitedListParser.DsvToDataTable(csv, ',', true, out parseErrors);
            ILookup<int, string> validateErrors = DataParsing.ValidateColumns(data, allColumns, reqColumns, 1);

            var availableBranchGroups = GroupDB.GetAllGroupIdsAndNames(BrokerId, GroupType.Branch);

            var availablePricingGroups = new List<ShortPricingGroupInfo>();
            SqlParameter[] parameters = new SqlParameter[]{
                        new SqlParameter("@BrokerId", BrokerId),
                        };
            using (IDataReader reader = StoredProcedureHelper.ExecuteReader(BrokerId, "ListPricingGroupByBrokerId", parameters))
            {
                while (reader.Read())
                {
                    availablePricingGroups.Add(new ShortPricingGroupInfo()
                    {
                        Name = (string)reader["LpePriceGroupName"],
                        Id = new Guid(reader["LpePriceGroupId"].ToString()),
                    });
                }
            }


            //We skip the first line in parsing to make room for a header row, so start at 1            
            int line = 1;
            if (parseErrors.Contains(line))
            {
                foreach (string err in parseErrors[line])
                {
                    errorList.Rows.Add("Error on line " + line + ": " + err);
                }
            }

            if (validateErrors.Contains(line))
            {
                foreach (string err in validateErrors[line])
                {
                    errorList.Rows.Add("Error on line " + line + ": " + err);
                }
            }

            for (int i = 0; i < data.Rows.Count; i++)
            {
                DataRow row = data.Rows[i];
                line++;
                if (parseErrors.Contains(line))
                {
                    foreach (string err in parseErrors[line])
                    {
                        errorList.Rows.Add("Error on line " + line + ": " + err);
                    }
                }

                if (validateErrors.Contains(line))
                {
                    foreach (string err in validateErrors[line])
                    {
                        errorList.Rows.Add("Error on line " + line + ": " + err);
                    }
                }

                BranchDB branch = new BranchDB();

                //Copy info over
                branch.Name = row["Name"].ToString();
                branch.DisplayNm = row["Display Name"].ToString();
                branch.BranchCode = row["Branch Code"].ToString();
                branch.BranchChannelT = (E_BranchChannelT)Enum.Parse(typeof(E_BranchChannelT), row["Channel"].ToString());
                branch.Address.StreetAddress = row["Street"].ToString();
                branch.Address.City = row["City"].ToString();
                branch.Address.State = row["State"].ToString();
                branch.Address.Zipcode = row["Zip"].ToString();
                branch.Phone = row["Phone"].ToString();
                branch.Fax = row["Fax"].ToString();

                string reqGroup = row["Branch Groups"].ToString();
                var potentialGroups = DataParsing.GetByIdOrName(availableBranchGroups, reqGroup, (a) => a.Item2, (a) => a.Item1);
                if (potentialGroups.Count() > 1)
                {
                    errorList.Rows.Add("Group name or id " + reqGroup + " was ambiguous. Try using the group id.");
                    continue;
                }
                else if (potentialGroups.Count() == 0)
                {
                    errorList.Rows.Add("Group name or id " + reqGroup + " could not be found.");
                    continue;
                }

                afterSave.Add((BranchId) => GroupDB.UpdateBranchGroups(BrokerId, BranchId, potentialGroups.First().Item1.ToString()));


                string reqPricingGroup = row["Default Pricing Group"].ToString();
                var potentialPricingGroups = DataParsing.GetByIdOrName(availablePricingGroups, reqPricingGroup, (a) => a.Name, (a) => a.Id);

                if (potentialPricingGroups.Count() > 1)
                {
                    errorList.Rows.Add("Pricing group name or id " + reqPricingGroup + " is ambiguous. Try using the pricing group id.");
                    continue;
                }
                else if (potentialPricingGroups.Count() == 0)
                {
                    errorList.Rows.Add("Group name or id " + reqPricingGroup + " could not be found.");
                }

                branch.BranchLpePriceGroupIdDefault = potentialPricingGroups.First().Id;
                toSave.Add(branch);
            }
            //We want to return true if parsing was successful, so are there no errors?
            return errorList.Rows.Count == 0;
        }
    }
}
