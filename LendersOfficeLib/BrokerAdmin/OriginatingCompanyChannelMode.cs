﻿/// <copyright file="OriginatingCompanyChannelMode.cs" company="MeridianLink">
///     Copyright (c) MeridianLink. All rights reserved.
/// </copyright>
/// <summary>
/// Author: Geoffrey Feltman
/// Date:   11/18/2015
/// </summary>
namespace LendersOffice.BrokerAdmin
{
    /// <summary>
    /// Defines the broker channel for an originating company.
    /// </summary>
    public enum OriginatingCompanyChannelMode
    {
        /// <summary>
        /// Represents the broker channel.
        /// </summary>
        Broker = 0,

        /// <summary>
        /// Represents the mini-correspondent channel.
        /// </summary>
        MiniCorrespondent = 1,

        /// <summary>
        /// Represents the correspondent channel.
        /// </summary>
        Correspondent = 2
    }
}
