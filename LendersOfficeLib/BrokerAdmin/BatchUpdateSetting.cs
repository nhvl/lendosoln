﻿/// <copyright file="PmlBrokerPermission.cs" company="MeridianLink">
///     Copyright (c) MeridianLink. All rights reserved.
/// </copyright>
/// <summary>
/// Author: Geoffrey Feltman, Michael Leinweaver
/// Date:   11/18/2015, 2/18/2016
/// </summary>
namespace LendersOffice.BrokerAdmin
{
    using System;
    using System.Collections.Generic;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Security;

    /// <summary>
    /// Provides a container for batch update settings.
    /// </summary>
    public class BatchUpdateSetting
    {
        /// <summary>
        /// Gets or sets the value for the manager's employee id.
        /// </summary>
        /// <value>
        /// The <see cref="Guid"/> employee id.
        /// </value>
        public Guid? ManagerEmployeeId { get; set; }

        /// <summary>
        /// Gets or sets the value for the processor's employee id.
        /// </summary>
        /// <value>
        /// The <see cref="Guid"/> employee id.
        /// </value>
        public Guid? ProcessorEmployeeId { get; set; }

        /// <summary>
        /// Gets or sets the value for the junior processor's 
        /// employee id.
        /// </summary>
        /// <value>
        /// The <see cref="Guid"/> employee id.
        /// </value>
        public Guid? JuniorProcessorEmployeeId { get; set; }

        /// <summary>
        /// Gets or sets the value for the lender account executive's 
        /// employee id.
        /// </summary>
        /// <value>
        /// The <see cref="Guid"/> employee id.
        /// </value>
        public Guid? LenderAccountExecEmployeeId { get; set; }

        /// <summary>
        /// Gets or sets the value for the underwriter's employee id.
        /// </summary>
        /// <value>
        /// The <see cref="Guid"/> employee id.
        /// </value>
        public Guid? UnderwriterEmployeeId { get; set; }

        /// <summary>
        /// Gets or sets the value for the junior underwriter's 
        /// employee id.
        /// </summary>
        /// <value>
        /// The <see cref="Guid"/> employee id.
        /// </value>
        public Guid? JuniorUnderwriterEmployeeId { get; set; }

        /// <summary>
        /// Gets or sets the value for the credit auditor's
        /// employee id.
        /// </summary>
        /// <value>
        /// The <see cref="Guid"/> employee id.
        /// </value>
        public Guid? CreditAuditorEmployeeId { get; set; }

        /// <summary>
        /// Gets or sets the value for the legal auditor's
        /// employee id.
        /// </summary>
        /// <value>
        /// The <see cref="Guid"/> employee id.
        /// </value>
        public Guid? LegalAuditorEmployeeId { get; set; }

        /// <summary>
        /// Gets or sets the value for the lock desk's
        /// employee id.
        /// </summary>
        /// <value>
        /// The <see cref="Guid"/> employee id.
        /// </value>
        public Guid? LockDeskEmployeeId { get; set; }

        /// <summary>
        /// Gets or sets the value for the purchaser's employee id.
        /// </summary>
        /// <value>
        /// The <see cref="Guid"/> employee id.
        /// </value>
        public Guid? PurchaserEmployeeId { get; set; }

        /// <summary>
        /// Gets or sets the value for the secondary's employee id.
        /// </summary>
        /// <value>
        /// The <see cref="Guid"/> employee id.
        /// </value>
        public Guid? SecondaryEmployeeId { get; set; }

        /// <summary>
        /// Gets or sets the value for the supervisor's employee id.
        /// </summary>
        /// <value>
        /// The <see cref="Guid"/> employee id.
        /// </value>
        public Guid SupervisorId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether
        /// the supervisor should be updated.
        /// </summary>
        /// <value>
        /// True if the value should be updated, 
        /// false otherwise.
        /// </value>
        public bool UpdateSupervisor { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether
        /// "Is Supervisor?" should be updated.
        /// </summary>
        /// <value>
        /// True if the value should be updated, 
        /// false otherwise.
        /// </value>
        public bool UpdateIsSupervisor { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether
        /// the employee should be made supervisor.
        /// </summary>
        /// <value>
        /// True if the employee should be made supervisor,
        /// false otherwise.
        /// </value>
        public bool MakeSupervisor { get; set; }

        /// <summary>
        /// Gets or sets the value for the branch id.
        /// </summary>
        /// <value>
        /// The <see cref="Guid"/> branch id.
        /// </value>
        public Guid? BranchId { get; set; }

        /// <summary>
        /// Gets or sets the value for the price group id.
        /// </summary>
        /// <value>
        /// The <see cref="Guid"/> price group id.
        /// </value>
        public Guid? PriceGroupId { get; set; }

        /// <summary>
        /// Gets or sets the value for the Pml landing page id.
        /// </summary>
        /// <value>
        /// The <see cref="Guid"/> landing page id.
        /// </value>
        public Guid? TpoLandingPageID { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether
        /// the employee should use the originating company's 
        /// branch id.
        /// </summary>
        /// <value>
        /// True if the employee should use the originating
        /// company's branch id, false otherwise.
        /// </value>
        public bool UseOriginatingCompanyBranchId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether
        /// the employee should use the originating company's 
        /// price group id.
        /// </summary>
        /// <value>
        /// True if the employee should use the originating 
        /// company's price group id, false otherwise.
        /// </value>
        public bool UseOriginatingCompanyPriceGroupId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the
        /// "Update Pricing Without Credit" permission
        /// should be updated.
        /// </summary>
        /// <value>
        /// True if the value should be updated, 
        /// false otherwise.
        /// </value>
        public bool UpdatePricingWithoutCredit { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether
        /// the employee can run pricing without
        /// a credit report on file.
        /// </summary>
        /// <value>
        /// The <see cref="Guid"/> employee id.
        /// </value>
        public bool RunPricingWithoutCredit { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the
        /// "Register/Lock Without Credit" permission
        /// should be updated.
        /// </summary>
        /// <value>
        /// True if the value should be updated, 
        /// false otherwise.
        /// </value>
        public bool UpdateSubmitWithoutCredit { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether
        /// the employee can register/lock without
        /// a credit report on file.
        /// </summary>
        /// <value>
        /// The <see cref="Guid"/> employee id.
        /// </value>
        public bool SubmitWithoutCredit { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether
        /// the "Apply for Ineligible Loan Programs" permission
        /// should be updated.
        /// </summary>
        /// <value>
        /// True if the value should be updated, 
        /// false otherwise.
        /// </value>
        public bool UpdateApplyForIneligibleLoanPrograms { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether
        /// the employee is allowed to apply for ineligible
        /// loan programs.
        /// </summary>
        /// <value>
        /// True if the employee can apply for ineligible loan programs,
        /// false otherwise.
        /// </value>
        public bool AllowApplyingForIneligibleLoanPrograms { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether
        /// the "Allow Ordering 4506-T" permission
        /// should be updated.
        /// </summary>
        /// <value>
        /// True if the value should be updated, 
        /// false otherwise.
        /// </value>
        public bool UpdateAllow4506T { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether
        /// the employee is allowed to order a 
        /// 4506-T.
        /// </summary>
        /// <value>
        /// True if the employee is allowed to order
        /// a 4506-T, false otherwise.
        /// </value>
        public bool Allow4506T { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether
        /// the "Allow Viewing Wholesale Loans" permission
        /// should be updated.
        /// </summary>
        /// <value>
        /// True if the value should be updated, 
        /// false otherwise.
        /// </value>
        public bool UpdateAllowViewWholesaleLoan { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether
        /// the employee can view wholesale loans.
        /// </summary>
        /// <value>
        /// True if the employee can view wholesale loans,
        /// false otherwise.
        /// </value>
        public bool AllowViewWholesaleLoan { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether
        /// the "Allow Viewing Correspondent Loans" permission
        /// should be updated.
        /// </summary>
        /// <value>
        /// True if the value should be updated, 
        /// false otherwise.
        /// </value>
        public bool UpdateAllowViewCorrLoan { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether
        /// the employee can view correspondent loans.
        /// </summary>
        /// <value>
        /// True if the employee can view correspondent 
        /// loans, false otherwise.
        /// </value>
        public bool AllowViewCorrLoan { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether
        /// the "Allow Viewing Mini-Correspondent Loans" 
        /// permission should be updated.
        /// </summary>
        /// <value>
        /// True if the value should be updated, 
        /// false otherwise.
        /// </value>
        public bool UpdateAllowViewMiniCorrLoan { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether
        /// the employee can view mini-correspondent 
        /// loans.
        /// </summary>
        /// <value>
        /// True if the employee can view mini-correspondent 
        /// loans, false otherwise.
        /// </value>
        public bool AllowViewMiniCorrLoan { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether
        /// the "Allow Creating Wholesale Loans" permission
        /// should be updated.
        /// </summary>
        /// <value>
        /// True if the value should be updated, 
        /// false otherwise.
        /// </value>
        public bool UpdateAllowCreateWholesaleLoan { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether
        /// the employee is allowed to create wholesale
        /// loans.
        /// </summary>
        /// <value>
        /// True if the employee can create wholesale loans,
        /// false otherwise.
        /// </value>
        public bool AllowCreateWholesaleLoan { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether
        /// the "Allow Creating Correspondent Loans" permission
        /// should be updated.
        /// </summary>
        /// <value>
        /// True if the value should be updated, 
        /// false otherwise.
        /// </value>
        public bool UpdateAllowCreateCorrLoan { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether
        /// the employee is allowed to create correspondent
        /// loans.
        /// </summary>
        /// <value>
        /// True if the employee can create wholesale loans,
        /// false otherwise.
        /// </value>
        public bool AllowCreateCorrLoan { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether
        /// the "Allow Creating Mini-Correspondent Loans" 
        /// permission should be updated.
        /// </summary>
        /// <value>
        /// True if the value should be updated, 
        /// false otherwise.
        /// </value>
        public bool UpdateAllowCreateMiniCorrLoan { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether
        /// the employee is allowed to create mini-
        /// correspondent loans.
        /// </summary>
        /// <value>
        /// True if the employee can create mini-correspondent 
        /// loans, false otherwise.
        /// </value>
        public bool AllowCreateMiniCorrLoan { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether
        /// the status of an employee should be updated.
        /// </summary>
        /// <value>
        /// True if the value should be updated, 
        /// false otherwise.
        /// </value>
        public bool UpdateStatus { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether
        /// the employee is active.
        /// </summary>
        /// <value>
        /// True if the employee is active, false 
        /// otherwise.
        /// </value>
        public bool Status { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether
        /// the employee's broker processor status
        /// should be updated.
        /// </summary>
        /// <value>
        /// True if the value should be updated, 
        /// false otherwise.
        /// </value>
        public bool UpdateIsBrokerProcessor { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether
        /// the employee is a broker processor.
        /// </summary>
        /// <value>
        /// True if the employee is a broker processor,
        /// false otherwise.
        /// </value>
        public bool IsBrokerProcessor { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether
        /// the employee's loan officer status
        /// should be updated.
        /// </summary>
        /// <value>
        /// True if the value should be updated, 
        /// false otherwise.
        /// </value>
        public bool UpdateIsLoanOfficer { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether
        /// the employee is a loan officer.
        /// </summary>
        /// <value>
        /// True if the employee is a loan officer,
        /// false otherwise.
        /// </value>
        public bool IsLoanOfficer { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether
        /// the employee's external secondary status
        /// should be updated.
        /// </summary>
        /// <value>
        /// True if the value should be updated, 
        /// false otherwise.
        /// </value>
        public bool UpdateIsExternalSecondary { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether
        /// the employee is an external secondary.
        /// </summary>
        /// <value>
        /// True if the employee is an external secondary,
        /// false otherwise.
        /// </value>
        public bool IsExternalSecondary { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether
        /// the employee's external post closer status
        /// should be updated.
        /// </summary>
        /// <value>
        /// True if the value should be updated, 
        /// false otherwise.
        /// </value>
        public bool UpdateIsExternalPostCloser { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether
        /// the employee is an external post closer.
        /// </summary>
        /// <value>
        /// True if the employee is an external post closer,
        /// false otherwise.
        /// </value>
        public bool IsExternalPostCloser { get; set; }

        /// <summary>
        /// Gets or sets the value for the employee's
        /// Pml navigation links.
        /// </summary>
        /// <value>
        /// A list containing the Pml navigation links for
        /// the employee.
        /// </value>
        public List<string> TpoNavLinks { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether ip access is restricted.
        /// </summary>
        /// <value>It's -1 if the value is not changing, 0 if false, 1 if true.</value>
        public int EnabledIpRestriction { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the user can install certificates.
        /// </summary>
        /// <value>It's -1 if the value is not changing, 0 if false, 1 if true.</value>
        public int EnabledClientDigitalCertificateInstall { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether sending the auth code via sms is enabled.
        /// </summary>
        /// <value>It's -1 if the value is not changing, 0 if false, 1 if true.</value>
        public int EnableAuthCodeViaSms { get; set; }

        /// <summary>
        /// Updates the settings for the <see cref="BatchUpdateSetting"/>
        /// using the permissions from the specified <paramref name="pmlBroker"/>.
        /// </summary>
        /// <param name="pmlBroker">
        /// The <see cref="PmlBroker"/> to obtain permission settings.
        /// </param>
        public void SetPermissions(PmlBroker pmlBroker)
        {
            this.UpdatePricingWithoutCredit = true;
            this.UpdateSubmitWithoutCredit = true;
            this.UpdateApplyForIneligibleLoanPrograms = true;
            this.UpdateAllow4506T = true;

            var permissions = pmlBroker.Permissions;

            this.RunPricingWithoutCredit = permissions.CanRunPricingEngineWithoutCreditReport;
            this.SubmitWithoutCredit = permissions.CanSubmitWithoutCreditReport;
            this.AllowApplyingForIneligibleLoanPrograms = permissions.CanApplyForIneligibleLoanPrograms;
            this.Allow4506T = permissions.AllowOrder4506T;

            // We want to ignore settings that are not set at
            // the OC level.
            this.UpdateStatus = false;
            this.UpdateIsBrokerProcessor = false;
            this.UpdateIsLoanOfficer = false;
            this.UpdateIsExternalSecondary = false;
            this.UpdateIsExternalPostCloser = false;
            this.UseOriginatingCompanyBranchId = false;
            this.UseOriginatingCompanyPriceGroupId = false;
            this.UpdateAllowViewWholesaleLoan = false;
            this.UpdateAllowViewCorrLoan = false;
            this.UpdateAllowViewMiniCorrLoan = false;
            this.UpdateAllowCreateWholesaleLoan = false;
            this.UpdateAllowCreateCorrLoan = false;
            this.UpdateAllowCreateMiniCorrLoan = false;

            this.TpoNavLinks = new List<string>();
            this.BranchId = null;
            this.PriceGroupId = null;
            this.TpoLandingPageID = null;
        }

        /// <summary>
        /// Updates the relationships for the <see cref="BatchUpdateSetting"/>
        /// using the permissions from the specified <paramref name="pmlBroker"/>.
        /// </summary>
        /// <param name="pmlBroker">
        /// The <see cref="PmlBroker"/> to obtain permission settings.
        /// </param>
        /// <param name="mode">
        /// The <see cref="OriginatingCompanyChannelMode"/> used for the update.
        /// </param>
        public void SetRelationships(PmlBroker pmlBroker, OriginatingCompanyChannelMode mode)
        {
            PmlBrokerRelationship relationships;

            switch (mode)
            {
                case OriginatingCompanyChannelMode.Broker:
                    relationships = pmlBroker.BrokerRoleRelationships;
                    break;
                case OriginatingCompanyChannelMode.MiniCorrespondent:
                    relationships = pmlBroker.MiniCorrRoleRelationships;
                    break;
                case OriginatingCompanyChannelMode.Correspondent:
                    relationships = pmlBroker.CorrRoleRelationships;
                    break;
                default:
                    throw new UnhandledEnumException(mode);
            }

            this.ManagerEmployeeId = relationships[E_RoleT.Manager].Id;
            this.ProcessorEmployeeId = relationships[E_RoleT.Processor].Id;
            this.JuniorProcessorEmployeeId = relationships[E_RoleT.JuniorProcessor].Id;
            this.LenderAccountExecEmployeeId = relationships[E_RoleT.LenderAccountExecutive].Id;
            this.UnderwriterEmployeeId = relationships[E_RoleT.Underwriter].Id;
            this.JuniorUnderwriterEmployeeId = relationships[E_RoleT.JuniorUnderwriter].Id;
            this.CreditAuditorEmployeeId = relationships[E_RoleT.CreditAuditor].Id;
            this.LegalAuditorEmployeeId = relationships[E_RoleT.LegalAuditor].Id;
            this.LockDeskEmployeeId = relationships[E_RoleT.LockDesk].Id;
            this.PurchaserEmployeeId = relationships[E_RoleT.Purchaser].Id;
            this.SecondaryEmployeeId = relationships[E_RoleT.Secondary].Id;

            // We want to ignore settings that are not set at
            // the OC level.
            this.SupervisorId = Guid.Empty;
            this.UpdateIsSupervisor = false;
            this.UpdateSupervisor = false;
            this.MakeSupervisor = false;
        }
    }
}
