﻿using System;
using System.Collections;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using LendersOffice.Common;
using DataAccess;
using System.Collections.Generic;

namespace LendersOffice
{
    /// <summary>
    /// Maintain loan program arm index row.
    /// </summary>

    [XmlRoot]
    public class ARMIndexDescriptor
    {
        /// <summary>
        /// Maintain loan program template row.
        /// </summary>

        #region ( Attributes )

        private Guid m_IndexIdGuid;
        private Guid m_BrokerIdGuid;
        private String m_IndexNameVstr;
        private Decimal m_IndexCurrentValueDecimal;
        private E_sArmIndexT m_FannieMaeArmIndexT;
        private E_sFreddieArmIndexT m_FreddieArmIndexT;
        private String m_IndexBasedOnVstr;
        private String m_IndexCanBeFoundVstr;
        private DateTime m_EffectiveD;

        #endregion

        #region ( Fields )

        // Guid IndexIdGuid
        // Guid BrokerIdGuid
        // String IndexNameVstr
        // Decimal IndexCurrentValueDecimal
        // E_sArmIndexT FannieMaeArmIndexT
        // E_sFreddieArmIndexT FreddieArmIndexT
        // String IndexBasedOnVstr
        // String IndexCanBeFoundVstr
        // DateTime EffectiveD

        #endregion

        #region ( Properties )

        [XmlIgnore]
        public DataRow Contents
        {
            // Initialize members.

            set
            {
                Object o;

                o = value["IndexIdGuid"];

                if (o != null && o is DBNull == false)
                {
                    m_IndexIdGuid = (Guid)o;
                }
                else
                {
                    m_IndexIdGuid = Guid.Empty;
                }

                o = value["BrokerIdGuid"];

                if (o != null && o is DBNull == false)
                {
                    m_BrokerIdGuid = (Guid)o;
                }
                else
                {
                    m_BrokerIdGuid = Guid.Empty;
                }

                o = value["IndexNameVstr"];

                if (o != null && o is DBNull == false)
                {
                    m_IndexNameVstr = (String)o;
                }
                else
                {
                    m_IndexNameVstr = "";
                }

                o = value["IndexCurrentValueDecimal"];

                if (o != null && o is DBNull == false)
                {
                    m_IndexCurrentValueDecimal = (Decimal)o;
                }
                else
                {
                    m_IndexCurrentValueDecimal = 0;
                }

                o = value["FannieMaeArmIndexT"];

                if (o != null && o is DBNull == false)
                {
                    m_FannieMaeArmIndexT = (E_sArmIndexT)(Int32)o;
                }
                else
                {
                    m_FannieMaeArmIndexT = E_sArmIndexT.LeaveBlank;
                }

                o = value["FreddieArmIndexT"];

                if (o != null && o is DBNull == false)
                {
                    m_FreddieArmIndexT = (E_sFreddieArmIndexT)(Int32)o;
                }
                else
                {
                    m_FreddieArmIndexT = E_sFreddieArmIndexT.LeaveBlank;
                }

                o = value["IndexBasedOnVstr"];

                if (o != null && o is DBNull == false)
                {
                    m_IndexBasedOnVstr = (String)o;
                }
                else
                {
                    m_IndexBasedOnVstr = "";
                }

                o = value["IndexCanBeFoundVstr"];

                if (o != null && o is DBNull == false)
                {
                    m_IndexCanBeFoundVstr = (String)o;
                }
                else
                {
                    m_IndexCanBeFoundVstr = "";
                }

                o = value["EffectiveD"];

                if (o != null && o is DBNull == false)
                {
                    m_EffectiveD = (DateTime)o;
                }
                else
                {
                    m_EffectiveD = SmallDateTime.MinValue;
                }
            }
        }

        [XmlElement]
        public Guid IndexIdGuid
        {
            set { m_IndexIdGuid = value; }
            get { return m_IndexIdGuid; }
        }

        [XmlElement]
        public Guid BrokerIdGuid
        {
            set { m_BrokerIdGuid = value; }
            get { return m_BrokerIdGuid; }
        }

        [XmlElement]
        public String IndexNameVstr
        {
            set { m_IndexNameVstr = value; }
            get { return m_IndexNameVstr; }
        }

        [XmlElement]
        public Decimal IndexCurrentValueDecimal
        {
            set { m_IndexCurrentValueDecimal = value; }
            get { return m_IndexCurrentValueDecimal; }
        }

        [XmlElement]
        public E_sArmIndexT FannieMaeArmIndexT
        {
            set { m_FannieMaeArmIndexT = value; }
            get { return m_FannieMaeArmIndexT; }
        }

        [XmlElement]
        public E_sFreddieArmIndexT FreddieArmIndexT
        {
            set { m_FreddieArmIndexT = value; }
            get { return m_FreddieArmIndexT; }
        }

        [XmlElement]
        public String IndexBasedOnVstr
        {
            set { m_IndexBasedOnVstr = value; }
            get { return m_IndexBasedOnVstr; }
        }

        [XmlElement]
        public String IndexCanBeFoundVstr
        {
            set { m_IndexCanBeFoundVstr = value; }
            get { return m_IndexCanBeFoundVstr; }
        }

        [XmlElement]
        public DateTime EffectiveD
        {
            set { m_EffectiveD = value; }
            get { return m_EffectiveD; }
        }

        #endregion

        #region ( Constructors )

        /// <summary>
        /// Construct copy from source.
        /// </summary>

        public ARMIndexDescriptor(ARMIndexDescriptor aThat)
        {
            // Initialize members.

            m_IndexIdGuid = aThat.m_IndexIdGuid;
            m_BrokerIdGuid = aThat.m_BrokerIdGuid;
            m_IndexNameVstr = aThat.m_IndexNameVstr;
            m_IndexCurrentValueDecimal = aThat.m_IndexCurrentValueDecimal;
            m_FannieMaeArmIndexT = aThat.m_FannieMaeArmIndexT;
            m_FreddieArmIndexT = aThat.m_FreddieArmIndexT;
            m_IndexBasedOnVstr = aThat.m_IndexBasedOnVstr;
            m_IndexCanBeFoundVstr = aThat.m_IndexCanBeFoundVstr;
            m_EffectiveD = aThat.m_EffectiveD;
        }

        /// <summary>
        /// Construct default.
        /// </summary>

        public ARMIndexDescriptor()
        {
            // Initialize members.

            m_IndexIdGuid = Guid.Empty;
            m_BrokerIdGuid = Guid.Empty;
            m_IndexNameVstr = "";
            m_IndexCurrentValueDecimal = 0;
            m_FannieMaeArmIndexT = E_sArmIndexT.LeaveBlank;
            m_FreddieArmIndexT = E_sFreddieArmIndexT.LeaveBlank;
            m_IndexBasedOnVstr = "";
            m_IndexCanBeFoundVstr = "";
            m_EffectiveD = SmallDateTime.MinValue;
        }

        #endregion

    }

    /// <summary>
    /// Store broker's arm index descriptors in a set.
    /// </summary>

    [XmlRoot
   ]
    public class ARMIndexDescriptorSet
    {
        /// <summary>
        /// Store broker's arm index descriptors in a set.
        /// </summary>

        private ArrayList m_Set;
        private Int32 m_Version;

        [XmlArray
       ]
        [XmlArrayItem(typeof(ARMIndexDescriptor))
       ]
        public ArrayList Items
        {
            // Access member.

            set
            {
                m_Set = value;
            }
            get
            {
                return m_Set;
            }
        }

        [XmlElement
       ]
        public Int32 Version
        {
            // Access member.

            set
            {
                m_Version = value;
            }
            get
            {
                return m_Version;
            }
        }

        /// <summary>
        /// Check the active loan products and see if any one
        /// of them associates with the given index.
        /// </summary>

        public bool IsARMIndexInUse(Guid indexId)
        {
            // Lookup loan products with the given arm index id.

            try
            {
                // If we find any, we need to return true.  Let's
                // just serve up a count.  Switch to a reader if
                // you need a list of loan products.

                object nRows = StoredProcedureHelper.ExecuteScalar(DataSrc.LpeSrc, "GetArmIndexUseCount", new SqlParameter("@IndexId", indexId));

                if (nRows != null && nRows is Int32 == true)
                {
                    if ((Int32)nRows > 0)
                    {
                        return true;
                    }
                }

                return false;
            }
            catch (Exception)
            {
                // Oops!

                throw new CBaseException(ErrorMessages.FailedToCheckARMIndexUsage, ErrorMessages.FailedToCheckARMIndexUsage);
            }
        }

        /// <summary>
        /// Get all current arm index descriptors from the
        /// database for this broker.
        /// </summary>

        public void Retrieve(Guid brokerId)
        {
            // Pull current descriptor set from the database.

            try
            {
                // Load up a dataset of indexes and transfer to
                // our set.
                DataSet dS = new DataSet();
                SqlParameter[] parameters = {
                                                new SqlParameter("@BrokerId", brokerId)
                                            };

                DataSetHelper.Fill(dS, brokerId, "ListArmIndexByBrokerId", parameters);

                m_Set.Clear();

                foreach (DataTable dT in dS.Tables)
                {
                    foreach (DataRow dR in dT.Rows)
                    {
                        // Pull contents from the row and save.

                        ARMIndexDescriptor aDesc = new ARMIndexDescriptor();

                        aDesc.Contents = dR;

                        m_Set.Add(aDesc);
                    }
                }
            }
            catch (Exception)
            {
                // Oops!

                throw new CBaseException(ErrorMessages.FailedToLoadARMIndexDescriptors, ErrorMessages.FailedToLoadARMIndexDescriptors);
            }
        }

        /// <summary>
        /// Write back the current set to the given broker.  If the
        /// item's id is empty, then we create a new one.  If there
        /// was an item, but isn't in this set, then we remove it
        /// from the database.  If the item in this set is not empty,
        /// but also not persisted, then we skip it.  Note: this is
        /// a DANGEROUS call -- only call commit on an EMPTY SET
        /// when you want to CLEAN OUT all the records in the db.
        /// </summary>

        public void Commit(Guid brokerId)
        {
            try
            {
                List<Guid> idList = new List<Guid>();

                SqlParameter[] armParameters = {
                                                   new SqlParameter("@BrokerId", brokerId)
                                               };

                using (var reader = StoredProcedureHelper.ExecuteReader(brokerId, "ListArmIndexByBrokerId", armParameters))
                {
                    while (reader.Read())
                    {
                        idList.Add((Guid)reader["IndexIdGuid"]);
                    }
                }

                using (CStoredProcedureExec spExec = new CStoredProcedureExec(brokerId))
                {
                    // Use the id list and update or create new
                    // index entries in the table one at a time.

                    spExec.BeginTransactionForWrite();

                    try
                    {
                        foreach (ARMIndexDescriptor aDesc in m_Set)
                        {
                            if (aDesc.IndexIdGuid == Guid.Empty)
                            {
                                SqlParameter newId = new SqlParameter("@IndexId", Guid.Empty);

                                newId.Direction = ParameterDirection.Output;

                                spExec.ExecuteNonQuery
                                    ("CreateArmIndex"
                                    , 0
                                    , false
                                    , new SqlParameter("@BrokerId", brokerId)
                                    , new SqlParameter("@IndexName", aDesc.IndexNameVstr)
                                    , new SqlParameter("@IndexCurrentValue", aDesc.IndexCurrentValueDecimal)
                                    , new SqlParameter("@EffectiveD", aDesc.EffectiveD)
                                    , newId
                                    );

                                aDesc.IndexIdGuid = (Guid)newId.Value;

                                idList.Add((Guid)newId.Value);
                            }

                            if (idList.Contains(aDesc.IndexIdGuid) == true)
                            {
                                spExec.ExecuteNonQuery
                                    ("UpdateArmIndex"
                                    , 0
                                    , false
                                    , new SqlParameter("@IndexId", aDesc.IndexIdGuid)
                                    , new SqlParameter("@IndexName", aDesc.IndexNameVstr)
                                    , new SqlParameter("@IndexCurrentValue", aDesc.IndexCurrentValueDecimal)
                                    , new SqlParameter("@IndexCanBeFound", aDesc.IndexCanBeFoundVstr)
                                    , new SqlParameter("@IndexBasedOn", aDesc.IndexBasedOnVstr)
                                    , new SqlParameter("@FannieMaeArmIndexT", aDesc.FannieMaeArmIndexT)
                                    , new SqlParameter("@FreddieArmIndexT", aDesc.FreddieArmIndexT)
                                    , new SqlParameter("@EffectiveD", aDesc.EffectiveD)
                                    , new SqlParameter("@BrokerId", brokerId)
                                    );
                            }

                            idList.Remove(aDesc.IndexIdGuid);
                        }

                        foreach (Guid leftOver in idList)
                        {
                            spExec.ExecuteNonQuery
                                ("RemoveArmIndex"
                                , 0
                                , false
                                , new SqlParameter("@IndexId", leftOver)
                                );
                        }

                        // Done!

                        spExec.CommitTransaction();
                    }
                    catch
                    {
                        // Oops!

                        spExec.RollbackTransaction();

                        throw;
                    }
                }


                // 7/14/2014 dd - Spread the change to LOAN_PROGRAM_TEMPLATE.
                foreach (ARMIndexDescriptor aDesc in m_Set)
                {
                    SqlParameter[] parameters = {
                                                    new SqlParameter("@IndexIdGuid", aDesc.IndexIdGuid),
                                                    new SqlParameter("@IndexNameVstr", aDesc.IndexNameVstr),
                                                    new SqlParameter("@IndexCurrentValueDecimal", aDesc.IndexCurrentValueDecimal),
                                                    new SqlParameter("@EffectiveD", aDesc.EffectiveD),
                                                    new SqlParameter("@FreddieArmIndexT", (int)aDesc.FreddieArmIndexT),
                                                    new SqlParameter("@IndexBasedOnVstr", aDesc.IndexBasedOnVstr),
                                                    new SqlParameter("@IndexCanBeFoundVstr", aDesc.IndexCanBeFoundVstr)
                                                };
                    StoredProcedureHelper.ExecuteNonQuery(DataSrc.LpeSrc, "UpdateArmIndexToLoanProgramTemplate", 1, parameters);
                }

            }
            catch (Exception)
            {
                // Oops!

                throw new CBaseException(ErrorMessages.FailedToSaveARMIndexDescriptors, ErrorMessages.FailedToSaveARMIndexDescriptors);
            }
        }

        /// <summary>
        /// Create a new entry and push it into first place.
        /// </summary>

        public void Push(String indexName, Decimal indexValue, E_sArmIndexT fmIndexT, DateTime effectiveD, String basedOn, String canBeFound)
        {
            // Create a new entry and push it into first place.

            try
            {
                // Create a new entry and push it into first place.

                ARMIndexDescriptor aDesc = new ARMIndexDescriptor();

                aDesc.IndexNameVstr = indexName;
                aDesc.IndexCurrentValueDecimal = indexValue;
                aDesc.FannieMaeArmIndexT = fmIndexT;
                aDesc.EffectiveD = effectiveD;
                aDesc.IndexCanBeFoundVstr = canBeFound;
                aDesc.IndexBasedOnVstr = basedOn;

                m_Set.Insert(0, aDesc);
            }
            catch (Exception)
            {
                throw new CBaseException(ErrorMessages.FailedToPushARMIndexDescriptors, ErrorMessages.FailedToPushARMIndexDescriptors);
            }
        }

        /// <summary>
        /// Get our set's looping interface for accessing
        /// retrieved descriptors.
        /// </summary>

        public IEnumerator GetEnumerator()
        {
            // Access our set's loop interface.

            return m_Set.GetEnumerator();
        }

        #region ( Serialization Operators )

        /// <summary>
        /// Translate stream form of xml document as loan program set.
        /// </summary>
        /// <param name="sXml">
        /// Stream form of uploaded document.
        /// </param>
        /// <returns>
        /// Instance of parsed object.
        /// </returns>

        public static ARMIndexDescriptorSet ToObject(System.IO.Stream sXml)
        {
            // Take the stream form of a loan program set document
            // and produce an instance.

            XmlSerializer xs = new XmlSerializer(typeof(ARMIndexDescriptorSet));

            return xs.Deserialize(sXml) as ARMIndexDescriptorSet;
        }

        /// <summary>
        /// Translate string form of xml document as loan program set.
        /// </summary>
        /// <param name="sXml">
        /// String form of uploaded document.
        /// </param>
        /// <returns>
        /// Instance of parsed object.
        /// </returns>

        public static ARMIndexDescriptorSet ToObject(System.String sXml)
        {
            // Take the stream form of a loan program set document
            // and produce an instance.

            XmlSerializer xs = new XmlSerializer(typeof(ARMIndexDescriptorSet));
            StringReader sr = new StringReader(sXml);

            return xs.Deserialize(sr) as ARMIndexDescriptorSet;
        }

        /// <summary>
        /// Translate instance to an xml document.
        /// </summary>
        /// <returns>
        /// Serialized instance.
        /// </returns>

        public override String ToString()
        {
            // Perform simple serialization according to hints.

            XmlSerializer xs = new XmlSerializer(typeof(ARMIndexDescriptorSet));
            StringWriter8 sw = new StringWriter8();

            m_Version = s_VersionStamp;

            xs.Serialize(sw, this);

            return sw.ToString();
        }

        #endregion

        #region ( Constructors )

        /// <summary>
        /// Construct copy from source.
        /// </summary>

        public ARMIndexDescriptorSet(ARMIndexDescriptorSet aThat)
            : this()
        {
            // Initialize members.

            foreach (ARMIndexDescriptor aDesc in aThat.m_Set)
            {
                m_Set.Add(new ARMIndexDescriptor(aDesc));
            }
        }

        /// <summary>
        /// Construct default.
        /// </summary>

        public ARMIndexDescriptorSet()
        {
            // Initialize members.

            m_Version = s_VersionStamp;

            m_Set = new ArrayList();
        }

        #endregion

        #region ( Version Stamp )

        /// <summary>
        /// Each time we update this class' layout, in such a way
        /// that older versions don't conform to the new layout,
        /// then we need to bump up the version number by one.
        /// </summary>

        private static Int32 s_VersionStamp = 1;

        #endregion

    }

    // Simple Read only listing of master entries.  
    // Keep seperate from the other listing that is associated with a broker.
    public class SystemArmIndex
    {
        public Guid IndexIdGuid { get; private set; }
        public string IndexNameVstr { get; private set; }
        public decimal IndexCurrentValueDecimal { get; private set; }
        public DateTime EffectiveD { get; private set; }
        public E_sFreddieArmIndexT FreddieArmIndexT { get; private set; }
        public E_sArmIndexT FannieMaeArmIndexT { get; private set; }
        public string IndexBasedOnVstr { get; private set; }
        public string IndexCanBeFoundVstr { get; private set; }


        private static SystemArmIndex ReadFromRow(DbDataReader reader)
        {
            return new SystemArmIndex()
            {
                IndexIdGuid = (Guid)reader["IndexIdGuid"],
                IndexNameVstr = reader["IndexNameVstr"].ToString(),
                IndexCurrentValueDecimal = (decimal)reader["IndexCurrentValueDecimal"],
                EffectiveD = (DateTime)reader["EffectiveD"],
                FreddieArmIndexT = (E_sFreddieArmIndexT)reader["FreddieArmIndexT"],
                FannieMaeArmIndexT = (E_sArmIndexT)reader["FannieMaeArmIndexT"],
                IndexBasedOnVstr = reader["IndexBasedOnVstr"].ToString(),
                IndexCanBeFoundVstr = reader["IndexCanBeFoundVstr"].ToString()
            };
        }

        public static IEnumerable<SystemArmIndex> ListSystemArmIndexes()
        {
            List<SystemArmIndex> indexList = new List<SystemArmIndex>();

            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", LendersOffice.Admin.BrokerDB.PmlMaster)
                                        };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(
                LendersOffice.Admin.BrokerDB.PmlMaster, "ListArmIndexByBrokerId", parameters))
            {
                while (reader.Read())
                {
                    indexList.Add(ReadFromRow(reader));
                }
            }
            return indexList;
        }

        /// <summary>
        /// Update all LOAN_PROGRAM_TEMPLATE using system ARM index with the current value.
        /// Only call this method if system ARM index is modified.
        /// </summary>
        public static void PopulateSystemArmIndexToLoanProgramTemplate()
        {
            foreach (SystemArmIndex armIndex in ListSystemArmIndexes())
            {
                SqlParameter[] parameters = {
                                                    new SqlParameter("@IndexIdGuid", armIndex.IndexIdGuid),
                                                    new SqlParameter("@IndexNameVstr", armIndex.IndexNameVstr),
                                                    new SqlParameter("@IndexCurrentValueDecimal", armIndex.IndexCurrentValueDecimal),
                                                    new SqlParameter("@EffectiveD", armIndex.EffectiveD),
                                                    new SqlParameter("@FreddieArmIndexT", (int)armIndex.FreddieArmIndexT),
                                                    new SqlParameter("@IndexBasedOnVstr", armIndex.IndexBasedOnVstr),
                                                    new SqlParameter("@IndexCanBeFoundVstr", armIndex.IndexCanBeFoundVstr)
                                                };
                StoredProcedureHelper.ExecuteNonQuery(DataSrc.LpeSrc, "UpdateArmIndexToLoanProgramTemplate", 1, parameters);
            }
        }
    }


}
