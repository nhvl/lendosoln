using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Common;
using LendersOffice.Constants;
using LendersOffice.Events;
using LendersOffice.ObjLib.DatabaseMessageQueue;
using LendersOffice.Security;
using LqbGrammar.Drivers.SecurityEventLogging;

namespace LendersOffice.Roles
{
    /// <summary>
    /// // 5/1/2015 dd - Convert this class to continuous process. Only one server run this processor. The old code does not implement to be run on multiple servers.
    /// </summary>
    public class RoleChangeProcessor : CommonProjectLib.Runnable.IRunnable
    {
        /// <summary>
        /// We pull email requests from our queue.  A higher
        /// level processor sets our current path.
        /// </summary>

        private BrokerEmployeeNameCache m_Cache = new BrokerEmployeeNameCache();

        /// <summary>
        /// Gets the description of the continuous process. Run every 20 seconds
        /// </summary>
        /// <value>The description of the continuous process.</value>
        public string Description
        {
            get { return "Task Alert Processor."; }
        }

        /// <summary>
        /// Eat email objects from the message queue, deserialize
        /// them, and then send them out using our server.
        /// </summary>

        public void Run()
        {
            // Loop through all role change events and wait for new ones
            // from the listening message queue.
            Tools.ResetLogCorrelationId();
            Tools.LogInfo("DB Based Role change pump: Started running...");

            try
            {


                // Get next batch of messages.  While we are fetching, we
                // lockup the queue.  We know we have run out of messages
                // to process when the queue takes longer than a few hundred
                // milliseconds to respond.  Until empty, we keep loading
                // up our batch.  After we receive all the messages, we
                // combine and send out the minimal email notification set.

                List<DBMessage> msgqBatch = new List<DBMessage>();
                Hashtable uAffected = new Hashtable();
                Hashtable loanDelta = new Hashtable();
                ArrayList idsBroker = new ArrayList();

                DBMessageQueue mQ = new DBMessageQueue(ConstMsg.RoleQueue);

                foreach (DBMessage msg in mQ.ReceiveAllSynchronized())
                {
                    msgqBatch.Add(msg);
                }

                foreach (DBMessage rcMsg in msgqBatch)
                {
                    RoleChange rC = RoleChange.ToObject(rcMsg.BodyStream);

                    Tools.LogInfo("RoleChangeProcessor", "RoleChangeProcess: Received " + rcMsg.Id + " " + rcMsg.Data);
                    // Add the role change to our loan delta list.  We need
                    // to track all known changes for a single loan so we
                    // can build the composite for each user.

                    ArrayList changeSet = loanDelta[rC.Loan] as ArrayList;

                    if (changeSet == null)
                    {
                        loanDelta.Add(rC.Loan, changeSet = new ArrayList());
                    }

                    if (rC.Old != Guid.Empty || rC.New != Guid.Empty)
                    {
                        changeSet.Add(rC);
                    }

                    // We can process this role change event now.  We
                    // organize the role changes by affected user, and
                    // then by affected loan.  The role change must be
                    // a change in order to be indexed.

                    if (rC.Old != rC.New || rC.Type == E_RoleChangeT.Import)
                    {
                        if (rC.Old != Guid.Empty)
                        {
                            // Gather all the loans that changed for the
                            // old employee.

                            ArrayList idSet = uAffected[rC.Old] as ArrayList;

                            if (idSet == null)
                            {
                                uAffected.Add(rC.Old, idSet = new ArrayList());
                            }

                            if (idSet.Contains(rC.Loan) == false)
                            {
                                idSet.Add(rC.Loan);
                            }
                        }

                        if (rC.New != Guid.Empty)
                        {
                            // Gather all the loans that changed for the
                            // new employee.

                            ArrayList idSet = uAffected[rC.New] as ArrayList;

                            if (idSet == null)
                            {
                                uAffected.Add(rC.New, idSet = new ArrayList());
                            }

                            if (idSet.Contains(rC.Loan) == false)
                            {
                                idSet.Add(rC.Loan);
                            }
                        }

                        if (idsBroker.Contains(rC.BrokerId) == false)
                        {
                            // Remember this broker for assignment lookup.

                            idsBroker.Add(rC.BrokerId);
                        }
                    }

                }

                // Given the indexed batch of role change events, we
                // need to construct emails to send out.  We send one
                // email to every affected user.

                foreach (Guid empId in uAffected.Keys)
                {
                    // Lookup the affected employee's affected loans.

                    ArrayList lAffected = uAffected[empId] as ArrayList;

                    if (lAffected == null)
                    {
                        continue;
                    }
                    Guid brokerId = Guid.Empty;

                    // Gather role change records for each affected loan
                    // for this user.  It seems like we may be pulling
                    // loan details more than once for the same loan for
                    // 2 different employees, but each gathering is from
                    // the current's employee's perspective, so the loan
                    // information is different.

                    ArrayList recSet = new ArrayList();

                    foreach (Guid loanId in lAffected)
                    {
                        // Get all the role changes affecting the loan.

                        ArrayList roleChanges = loanDelta[loanId] as ArrayList;

                        if (roleChanges == null)
                        {
                            continue;
                        }

                        // Construct the before and after snapshot
                        // for the current, affected loan.  We cache
                        // the loan name and borrower for each role
                        // change so we don't have to get it from the
                        // db for each afffected loan.  Now, the info
                        // could be stale.  But it *was* accurate at
                        // the time of assignment, so that should be
                        // acceptable.  To minimize stale names, we
                        // use the most recent role change event for
                        // a loan to determine the loan name and the
                        // primary borrower's full name.  Btw, each
                        // role change keeps a cached set of the
                        // assignments at the time of update.  So,
                        // since we sort the changes oldest to latest,
                        // the last one will stick.

                        RoleChangeSnapshot snapShot = new RoleChangeSnapshot();

                        snapShot.LoanName = "Unknown";
                        snapShot.Borrower = "Unknown";

                        roleChanges.Sort();

                        foreach (RoleChange rC in roleChanges)
                        {
                            foreach (CurrentRole cR in rC.Cached)
                            {
                                snapShot.Insert(cR.Role, cR.Curr);
                            }

                            snapShot.LoanName = rC.LoanName;
                            snapShot.Borrower = rC.Borrower;
                            snapShot.LoanId = rC.Loan;
                            snapShot.LoanBranchId = rC.Branch;
                            snapShot.BrokerId = rC.BrokerId;
                            brokerId = rC.BrokerId;
                        }

                        foreach (RoleChange rC in roleChanges)
                        {
                            // Look for collisions.  We insert on top of the
                            // current role change entry when the existing is
                            // of lesser rank than the one being inserted.
                            // If rank is equal, we need to know what to do
                            // based on whether or not the collision would
                            // hide changes that affect the current employee.
                            //
                            // Imagine this scenario:
                            //
                            //     role X: A -> B
                            //
                            //     then
                            //
                            //     role X: B -> C
                            //
                            // If the current employee is A, then we don't
                            // want to lose the A -> B role change.  However,
                            // C is the final result, so we need to display
                            // A -> C in the email log.  If the current
                            // employee is B, then we need to show both
                            // A -> B and B -> C in the same loan section of
                            // the log.  If the current employee is C, or
                            // some other, then we don't need to save the
                            // first role change -- employee C would just
                            // need to see the B -> C transition and other
                            // employees would only care to see C as the
                            // final resulting assignment.  Now, if A = C,
                            // such that we have cycled back to the first
                            // employee, then we need to append all final
                            // assignments.  I think capturing the first
                            // departure and last return is all that we
                            // need -- the in between events are noise.

                            Boolean didCollide = false;

                            foreach (RoleChangeEntry rcEntry in snapShot)
                            {
                                if (rcEntry.Role == rC.Role)
                                {
                                    if (rcEntry.Curr == rC.Old && rcEntry.Type == rC.Type)
                                    {
                                        if (rcEntry.Prev == empId)
                                        {
                                            if (rC.New != empId)
                                            {
                                                snapShot.Update(rC.Role, rC.Old, rC.New, rC.Type);
                                            }
                                            else
                                            {
                                                snapShot.Append(rC.Role, rC.Old, rC.New, rC.Type);
                                            }

                                            didCollide = true;

                                            break;
                                        }
                                        else
                                        {
                                            if (rcEntry.Curr == empId)
                                            {
                                                snapShot.Append(rC.Role, rC.Old, rC.New, rC.Type);

                                                didCollide = true;

                                                break;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (rcEntry.Prev == rC.New && rcEntry.Type == rC.Type)
                                        {
                                            if (rcEntry.Prev == empId)
                                            {
                                                snapShot.Append(rC.Role, rC.Old, rC.New, rC.Type);

                                                didCollide = true;

                                                break;
                                            }
                                        }
                                    }
                                }
                            }

                            if (didCollide == false)
                            {
                                snapShot.Insert(rC.Role, rC.Old, rC.New, rC.Type);
                            }
                        }

                        // Write out this loan's changes according to
                        // the before and after snap shot we constructed.

                        LoanAssignmentRecord loanRec = new LoanAssignmentRecord();

                        loanRec.Borrower = snapShot.Borrower;
                        loanRec.LoanName = snapShot.LoanName;
                        loanRec.LoanId = snapShot.LoanId;
                        loanRec.BranchId = snapShot.LoanBranchId;

                        foreach (RoleChangeEntry rE in snapShot)
                        {
                            // List out each role change for this loan.

                            String b, a, r, t;

                            try
                            {
                                if (rE.Prev != Guid.Empty)
                                {
                                    b = m_Cache[rE.Prev].FullName;
                                }
                                else
                                {
                                    b = "";
                                }
                            }
                            catch (Exception e)
                            {
                                // Oops!

                                Tools.LogError(e);

                                b = "Unknown";
                            }

                            try
                            {
                                if (rE.Curr != Guid.Empty)
                                {
                                    a = m_Cache[rE.Curr].FullName;
                                }
                                else
                                {
                                    a = "";
                                }
                            }
                            catch (Exception e)
                            {
                                // Oops!

                                Tools.LogError(e);

                                a = "Unknown";
                            }

                            if ((r = Role.Get(rE.Role).ModifiableDesc) == null)
                            {
                                r = "Unknown";
                            }

                            switch (rE.Type)
                            {
                                case E_RoleChangeT.Import:
                                    {
                                        t = "Import";
                                    }
                                    break;

                                case E_RoleChangeT.Overwrite:
                                    {
                                        t = "Overwrite";
                                    }
                                    break;

                                default:
                                    {
                                        t = "";
                                    }
                                    break;
                            }

                            if ((rE.Prev == empId || rE.Curr == empId) && rE.Prev != rE.Curr)
                            {
                                if (rE.Prev != Guid.Empty && rE.Curr != Guid.Empty)
                                {
                                    loanRec.Add(r, b, a, t, LoanAssignmentRecord.E_ChangeMode.Replace);
                                }
                                else
                                    if (rE.Prev != Guid.Empty)
                                    {
                                        loanRec.Add(r, b, a, t, LoanAssignmentRecord.E_ChangeMode.Remove);
                                    }
                                    else
                                        if (rE.Curr != Guid.Empty)
                                        {
                                            loanRec.Add(r, b, a, t, LoanAssignmentRecord.E_ChangeMode.Assign);
                                        }
                            }
                            else
                            {
                                loanRec.Set(r, a);
                            }
                        }

                        recSet.Add(loanRec);
                    }

                    // Send out the loan assignment change notification for
                    // this affected employee.  We show the records we gathered
                    // during message queue processing.  If an error occurs, we
                    // will catch in our loop and track the error counts.


                    LoanAssignmentsUpdated loanEvt = new LoanAssignmentsUpdated();

                    loanEvt.Initialize(brokerId, recSet, empId, E_ApplicationT.LendersOffice);

                    loanEvt.Send();

                }



            }
            catch (Exception e)
            {
                Tools.LogError(e);
                throw;
            }


        }



    }

    /// <summary>
    /// Whenever an exception occurs during processing of
    /// email sending, we can throw this base.
    /// </summary>

    public class RoleProcessorException : CBaseException
    {
        /// <summary>
        /// Construct default.
        /// </summary>

        public RoleProcessorException(String sMessage, Exception inner)
            : base(ErrorMessages.Generic, sMessage + ".\r\n\r\n" + inner.ToString())
        {
        }

        /// <summary>
        /// Construct default.
        /// </summary>

        public RoleProcessorException(String sMessage)
            : base(ErrorMessages.Generic, sMessage)
        {
        }

    }

    /// <summary>
    /// Track before and after snapshot for a single role.
    /// </summary>

    public class RoleChangeEntry
    {
        /// <summary>
        /// Track before and after snapshot for a single role.
        /// </summary>

        private Guid m_Role, m_Prev, m_Curr;
        private E_RoleChangeT m_Type;

        #region ( Role change line item )

        public E_RoleChangeT Type
        {
            set { m_Type = value; }
            get { return m_Type; }
        }

        public Guid Role
        {
            set { m_Role = value; }
            get { return m_Role; }
        }

        public Guid Prev
        {
            set { m_Prev = value; }
            get { return m_Prev; }
        }

        public Guid Curr
        {
            set { m_Curr = value; }
            get { return m_Curr; }
        }

        #endregion

        #region ( Constructors )

        /// <summary>
        /// Construct default.
        /// </summary>

        public RoleChangeEntry(Guid idRole)
        {
            // Initialize members.

            m_Type = E_RoleChangeT.Invalid;

            m_Prev = Guid.Empty;
            m_Curr = Guid.Empty;

            m_Role = idRole;
        }

        #endregion

    }

    /// <summary>
    /// Build up list of role changes to process at the
    /// end of a loop.
    /// </summary>

    public class RoleChangeSet : ArrayList
    {
        /// <summary>
        /// Insert changed role assignment in place.
        /// </summary>

        public void Insert(Guid idRole, Guid idPrev, Guid idCurr, E_RoleChangeT eType)
        {
            // Add entry with specified role changes.

            RoleChangeEntry rcE = new RoleChangeEntry(idRole);

            rcE.Curr = idCurr;
            rcE.Prev = idPrev;
            rcE.Type = eType;

            foreach (RoleChangeEntry rcEntry in this)
            {
                if (rcEntry.Role == idRole)
                {
                    // Respect current entry's rank.

                    if (rcEntry.Type <= eType)
                    {
                        rcEntry.Prev = idPrev;
                        rcEntry.Curr = idCurr;

                        rcEntry.Type = eType;

                        return;
                    }
                }
            }

            Add(rcE);
        }

    }

    /// <summary>
    /// Track before and after snapshot for a single loan.
    /// 
    /// 10/20/2004 kb - This is the class that needs its
    /// constructor updated everytime we include a new
    /// role in the delta notification sent out when loan
    /// assignments change.
    /// </summary>

    internal class RoleChangeSnapshot : ArrayList
    {
        /// <summary>
        /// Track before and after snapshot for a single loan.
        /// Roles are fixed by position.
        /// </summary>

        private String m_LoanName;
        private String m_Borrower;
        private Guid m_LoanId;
        private Guid m_LoanBranchId;

        public String LoanName
        {
            set { m_LoanName = value; }
            get { return m_LoanName; }
        }

        public String Borrower
        {
            set { m_Borrower = value; }
            get { return m_Borrower; }
        }

        public Guid LoanId
        {
            set { m_LoanId = value; }
            get { return m_LoanId; }
        }

        public Guid LoanBranchId
        {
            set { m_LoanBranchId = value; }
            get { return m_LoanBranchId; }
        }

        public Guid BrokerId { get; set; }

        /// <summary>
        /// Overwrite existing role assignment change in place
        /// by keeping the previous and updating the current.
        /// The existing current must match the given previous.
        /// </summary>

        public void Update(Guid idRole, Guid idPrev, Guid idCurr, E_RoleChangeT eType)
        {
            // Add entry with specified role changes.

            foreach (RoleChangeEntry rcEntry in this)
            {
                if (rcEntry.Role == idRole)
                {
                    // Respect current entry's rank.

                    if (rcEntry.Curr == idPrev && rcEntry.Type <= eType)
                    {
                        rcEntry.Curr = idCurr;

                        rcEntry.Type = eType;

                        break;
                    }
                }
            }
        }

        /// <summary>
        /// Insert changed role assignment in place.
        /// </summary>

        public void Insert(Guid idRole, Guid idPrev, Guid idCurr, E_RoleChangeT eType)
        {
            // Add entry with specified role changes.

            foreach (RoleChangeEntry rcEntry in this)
            {
                if (rcEntry.Role == idRole)
                {
                    // Respect current entry's rank.

                    if (rcEntry.Type <= eType)
                    {
                        rcEntry.Prev = idPrev;
                        rcEntry.Curr = idCurr;

                        rcEntry.Type = eType;
                    }

                    break;
                }
            }
        }

        /// <summary>
        /// Insert current role assignment in place.
        /// </summary>

        public void Insert(Guid idRole, Guid idCurr)
        {
            // Add entry with no change taking place.

            Insert(idRole, idCurr, idCurr, E_RoleChangeT.Invalid);
        }

        /// <summary>
        /// Inject a new role change entry as an additional entry for
        /// a single role and place it after all the current role
        /// entries of the same type.
        /// </summary>

        public void Append(Guid idRole, Guid idPrev, Guid idCurr, E_RoleChangeT eType)
        {
            // Add entry with specified role changes.

            RoleChangeEntry rcNew = new RoleChangeEntry(idRole);

            rcNew.Curr = idCurr;
            rcNew.Prev = idPrev;

            rcNew.Type = eType;

            for (Int32 i = Count; i > 0; --i)
            {
                RoleChangeEntry rcEntry = this[i - 1] as RoleChangeEntry;

                if (rcEntry.Role == idRole)
                {
                    // Append onto the tail of same roles.

                    Insert(i, rcNew);

                    return;
                }
            }

            Add(rcNew);
        }

        #region ( Constructors )

        /// <summary>
        /// Construct default.
        /// </summary>

        internal RoleChangeSnapshot()
        {
            // Initialize members.

            Add(new RoleChangeEntry(CEmployeeFields.s_ManagerRoleId));
            Add(new RoleChangeEntry(CEmployeeFields.s_LoanRepRoleId));
            Add(new RoleChangeEntry(CEmployeeFields.s_ProcessorRoleId));
            Add(new RoleChangeEntry(CEmployeeFields.s_LoanOpenerId));
            Add(new RoleChangeEntry(CEmployeeFields.s_LenderAccountExecRoleId));
            Add(new RoleChangeEntry(CEmployeeFields.s_LockDeskRoleId));
            Add(new RoleChangeEntry(CEmployeeFields.s_UnderwriterRoleId));
            Add(new RoleChangeEntry(CEmployeeFields.s_CallCenterAgentRoleId));
            Add(new RoleChangeEntry(CEmployeeFields.s_RealEstateAgentRoleId));
            Add(new RoleChangeEntry(CEmployeeFields.s_CloserId));
            Add(new RoleChangeEntry(CEmployeeFields.s_ShipperId));
            Add(new RoleChangeEntry(CEmployeeFields.s_FunderRoleId));
            Add(new RoleChangeEntry(CEmployeeFields.s_PostCloserId));
            Add(new RoleChangeEntry(CEmployeeFields.s_InsuringId));
            Add(new RoleChangeEntry(CEmployeeFields.s_CollateralAgentId));
            Add(new RoleChangeEntry(CEmployeeFields.s_DocDrawerId));
            Add(new RoleChangeEntry(CEmployeeFields.s_CreditAuditorId));
            Add(new RoleChangeEntry(CEmployeeFields.s_DisclosureDeskId));
            Add(new RoleChangeEntry(CEmployeeFields.s_JuniorProcessorId));
            Add(new RoleChangeEntry(CEmployeeFields.s_JuniorUnderwriterId));
            Add(new RoleChangeEntry(CEmployeeFields.s_LegalAuditorId));
            Add(new RoleChangeEntry(CEmployeeFields.s_LoanOfficerAssistantId));
            Add(new RoleChangeEntry(CEmployeeFields.s_PurchaserId));
            Add(new RoleChangeEntry(CEmployeeFields.s_QCComplianceId));
            Add(new RoleChangeEntry(CEmployeeFields.s_SecondaryId));
            Add(new RoleChangeEntry(CEmployeeFields.s_ServicingId));

            m_LoanName = "";
            m_Borrower = "";
            m_LoanId = Guid.Empty;
        }

        #endregion

    }

    /// <summary>
    /// Maintain lookup table of available roles.
    /// </summary>

    internal class RoleCache
    {
        /// <summary>
        /// Maintain lookup table of available roles.
        /// </summary>

        #region ( Cache mechanics )

        private Hashtable m_Table = new Hashtable();

        public String this[Guid roleId]
        {
            // Access member.

            get
            {
                return m_Table[roleId] as String;
            }
        }

        /// <summary>
        /// Construct default.
        /// </summary>

        public RoleCache()
        {
            // Load up the current roles we support.  We
            // use this as a lookup table.  If the set
            // changes, we have to rebuild the app.

            foreach (Role role in Role.LendingQBRoles)
            {
                m_Table.Add(role.Id, role.ModifiableDesc);
            }
        }

        #endregion

    }



    /// <summary>
    /// Sit in our own thread and process role changes that
    /// currently reside in our message queue.  Note that
    /// the item value is also its rank.
    /// </summary>

    public enum E_RoleChangeT
    {
        Invalid = 0,
        Overwrite = 1,
        Import = 2,
        Update = 3
    };

    /// <summary>
    /// Maintain current employee assignment for a particular
    /// role within a single loan.
    /// </summary>

    [XmlRoot]
    public class CurrentRole
    {
        /// <summary>
        /// Maintain current employee assignment for a
        /// particular role within a single loan.
        /// </summary>

        private Guid m_Role;
        private Guid m_Curr;

        #region ( Current Employee Assignment )

        public Guid Role
        {
            set { m_Role = value; }
            get { return m_Role; }
        }

        public Guid Curr
        {
            set { m_Curr = value; }
            get { return m_Curr; }
        }

        #endregion

        #region ( Constructors )

        /// <summary>
        /// Construct default.
        /// </summary>

        public CurrentRole()
        {
            // Initialize members.

            m_Role = Guid.Empty;
            m_Curr = Guid.Empty;
        }

        #endregion

    }

    /// <summary>
    /// Keep track of simple role change description.
    /// </summary>

    [XmlRoot]
    public class RoleChange : IComparable
    {
        /// <summary>
        /// Keep track of simple role change description.
        /// </summary>

        private Guid m_Label;
        private Guid m_BrokerId;
        private Guid m_Old, m_New;
        private Guid m_Role;
        private Guid m_Loan;
        private Guid m_Branch;
        private String m_LoanName;
        private String m_Borrower;
        private E_RoleChangeT m_Type;
        private ArrayList m_Cached;
        private DateTime m_Date;

        #region ( Role Change Elements )

        [XmlAttribute]
        public Guid Label
        {
            set { m_Label = value; }
            get { return m_Label; }
        }

        [XmlElement]
        public Guid BrokerId
        {
            set { m_BrokerId = value; }
            get { return m_BrokerId; }
        }

        [XmlElement]
        public Guid Loan
        {
            set { m_Loan = value; }
            get { return m_Loan; }
        }

        [XmlElement]
        public Guid Branch
        {
            set { m_Branch = value; }
            get { return m_Branch; }
        }


        [XmlElement]
        public string LoanName
        {
            set { m_LoanName = value; }
            get { return m_LoanName; }
        }

        [XmlElement]
        public string Borrower
        {
            set { m_Borrower = value; }
            get { return m_Borrower; }
        }

        [XmlElement]
        public Guid Old
        {
            set { m_Old = value; }
            get { return m_Old; }
        }

        [XmlElement]
        public Guid New
        {
            set { m_New = value; }
            get { return m_New; }
        }

        [XmlElement]
        public Guid Role
        {
            set { m_Role = value; }
            get { return m_Role; }
        }

        [XmlElement]
        public E_RoleChangeT Type
        {
            set { m_Type = value; }
            get { return m_Type; }
        }

        [XmlArray]
        [XmlArrayItem(typeof(CurrentRole))]
        public ArrayList Cached
        {
            set { m_Cached = value; }
            get { return m_Cached; }
        }

        [XmlElement]
        public DateTime Date
        {
            set { m_Date = value; }
            get { return m_Date; }
        }

        #endregion

        /// <summary>
        /// Append new cached current role entry.
        /// </summary>

        public void Add(Guid employeeId, Guid roleId)
        {
            // Insert current role entry at end.

            CurrentRole cRole = new CurrentRole();

            cRole.Role = roleId;
            cRole.Curr = employeeId;

            m_Cached.Add(cRole);
        }

        /// <summary>
        /// Compare dates for sorting during processing.
        /// </summary>

        public Int32 CompareTo(Object rChange)
        {
            // Compare dates and order earlier to latest.

            RoleChange rC = rChange as RoleChange;

            if (rC != null)
            {
                return m_Date.CompareTo(rC.m_Date);
            }

            return -1;
        }

        #region ( Serialization Operators )

        /// <summary>
        /// Translate xml document back into email message.
        /// </summary>
        /// <returns>
        /// Email message representation.
        /// </returns>

        public static RoleChange ToObject(Stream sXml)
        {
            // Translate xml document back into email message.

            XmlSerializer xs = new XmlSerializer(typeof(RoleChange));

            return xs.Deserialize(sXml) as RoleChange;
        }

        /// <summary>
        /// Translate role change into xml document.
        /// </summary>
        /// <returns>
        /// Xml document representation.
        /// </returns>

        public override String ToString()
        {
            // Translate email message into xml document.

            XmlSerializer xs = new XmlSerializer(typeof(RoleChange));
            StringWriter8 sw = new StringWriter8();

            xs.Serialize(sw, this);

            return sw.ToString();
        }

        #endregion

        #region ( Constructors )

        /// <summary>
        /// Construct default.
        /// </summary>

        public RoleChange()
        {
            // Initialize members.

            m_Date = DateTime.Now;

            m_Type = E_RoleChangeT.Invalid;

            m_Cached = new ArrayList();

            m_BrokerId = Guid.Empty;
            m_Loan = Guid.Empty;
            m_Old = Guid.Empty;
            m_New = Guid.Empty;

            m_LoanName = "";
            m_Borrower = "";

            m_Label = Guid.Empty;

            m_Role = Guid.Empty;
        }

        #endregion

        public static void Mark(Guid idLabel, Guid idBroker, Guid idOld, Guid idNew, Guid idRole, E_RoleChangeT eType)
        {
            CPageData dataLoan = new CEmployeeDataWithAccessControl(idLabel);
            dataLoan.InitLoad();
            Mark(idLabel, idBroker, dataLoan, idOld, idNew, idRole, eType);
        }

        /// <summary>
        /// Save a new loan assignment change in the message queue.  We
        /// gather these up periodically and send out a summary to every
        /// affected user we find in the batch.
        /// </summary>

        public static void Mark(Guid idLabel, Guid idBroker, CPageData pbLoan, Guid idOld, Guid idNew, Guid idRole, E_RoleChangeT eType)
        {
            // Place it in our queue.  This path must be where we
            // initialized our pump, or it won't get sent.
            if (idRole == CEmployeeFields.s_BrokerProcessorId
                || idRole == CEmployeeFields.s_ExternalSecondaryId
                || idRole == CEmployeeFields.s_ExternalPostCloserId)
            {
                return;
                //av bp PML users do not get notification
            }


            DBMessageQueue mQ = new DBMessageQueue(ConstMsg.RoleQueue);

            RoleChange rC = new RoleChange();

            rC.Label = idLabel;
            rC.BrokerId = idBroker;

            rC.Loan = pbLoan.sLId;
            rC.LoanName = pbLoan.sLNm;
            rC.Branch = pbLoan.sBranchId;
            rC.Borrower = pbLoan.sPrimBorrowerFullNm;

            rC.Add(pbLoan.sEmployeeCallCenterAgentId, CEmployeeFields.s_CallCenterAgentRoleId);
            rC.Add(pbLoan.sEmployeeLenderAccExecId, CEmployeeFields.s_LenderAccountExecRoleId);
            rC.Add(pbLoan.sEmployeeUnderwriterId, CEmployeeFields.s_UnderwriterRoleId);
            rC.Add(pbLoan.sEmployeeLoanOpenerId, CEmployeeFields.s_LoanOpenerId);
            rC.Add(pbLoan.sEmployeeLoanRepId, CEmployeeFields.s_LoanRepRoleId);
            rC.Add(pbLoan.sEmployeeManagerId, CEmployeeFields.s_ManagerRoleId);
            rC.Add(pbLoan.sEmployeeProcessorId, CEmployeeFields.s_ProcessorRoleId);
            rC.Add(pbLoan.sEmployeeRealEstateAgentId, CEmployeeFields.s_RealEstateAgentRoleId);
            rC.Add(pbLoan.sEmployeeCloserId, CEmployeeFields.s_CloserId);
            rC.Add(pbLoan.sEmployeeLockDeskId, CEmployeeFields.s_LockDeskRoleId);
            rC.Add(pbLoan.sEmployeeFunderId, CEmployeeFields.s_FunderRoleId);
            rC.Add(pbLoan.sEmployeeShipperId, CEmployeeFields.s_ShipperId);
            rC.Add(pbLoan.sEmployeePostCloserId, CEmployeeFields.s_PostCloserId);
            rC.Add(pbLoan.sEmployeeInsuringId, CEmployeeFields.s_InsuringId);
            rC.Add(pbLoan.sEmployeeCollateralAgentId, CEmployeeFields.s_CollateralAgentId);
            rC.Add(pbLoan.sEmployeeDocDrawerId, CEmployeeFields.s_DocDrawerId);
            rC.Add(pbLoan.sEmployeeCreditAuditorId, CEmployeeFields.s_CreditAuditorId);
            rC.Add(pbLoan.sEmployeeDisclosureDeskId, CEmployeeFields.s_DisclosureDeskId);
            rC.Add(pbLoan.sEmployeeJuniorProcessorId, CEmployeeFields.s_JuniorProcessorId);
            rC.Add(pbLoan.sEmployeeJuniorUnderwriterId, CEmployeeFields.s_JuniorUnderwriterId);
            rC.Add(pbLoan.sEmployeeLegalAuditorId, CEmployeeFields.s_LegalAuditorId);
            rC.Add(pbLoan.sEmployeeLoanOfficerAssistantId, CEmployeeFields.s_LoanOfficerAssistantId);
            rC.Add(pbLoan.sEmployeePurchaserId, CEmployeeFields.s_PurchaserId);
            rC.Add(pbLoan.sEmployeeQCComplianceId, CEmployeeFields.s_QCComplianceId);
            rC.Add(pbLoan.sEmployeeSecondaryId, CEmployeeFields.s_SecondaryId);
            rC.Add(pbLoan.sEmployeeServicingId, CEmployeeFields.s_ServicingId);

            rC.Type = eType;

            rC.Role = idRole;

            rC.Old = idOld;
            rC.New = idNew;

            mQ.Send(rC.LoanName, rC.ToString());

            if (ConstStage.DebugQRoleChangeProcessor)
            {
                Tools.LogMadness(Tools.LogMadnessType.RolePumpMsgQ, "SENT  : " + rC.ToString());
            }

        }

    }

    /// <summary>
    /// Keep the details ready for display in a grid.
    /// </summary>

    public class ChangeItem
    {
        /// <summary>
        /// Keep the details ready for display in a grid.
        /// </summary>

        private String m_LoanName = "";
        private String m_Borrower = "";
        private String m_Role = "";
        private String m_Old = "";
        private String m_New = "";
        private String m_Id = "";

        #region ( Entry Attributes )

        public String LoanName
        {
            set { m_LoanName = value; }
            get { return m_LoanName; }
        }

        public string Borrower
        {
            set { m_Borrower = value; }
            get { return m_Borrower; }
        }

        public string Role
        {
            set { m_Role = value; }
            get { return m_Role; }
        }

        public string Old
        {
            set { m_Old = value; }
            get { return m_Old; }
        }

        public string New
        {
            set { m_New = value; }
            get { return m_New; }
        }

        public string Id
        {
            set { m_Id = value; }
            get { return m_Id; }
        }

        #endregion

        /// <summary>
        /// Provide default interface for accessing entry's
        /// key during data binding.
        /// </summary>

        public override string ToString()
        {
            // Return the entry's id by default.

            return m_Id;
        }

    }
}