﻿// <copyright file="ContinuousServiceApplicationInitializer.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
// Author: David D
// Date: 9/22/2016
// </summary>

namespace LendersOffice
{
    using DataAccess;
    using LqbGrammar;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// This class will be invoke through reflection in ContinuousService to perform framework initialization.
    /// Do not remove this class despite of zero reference.
    /// </summary>
    public static class ContinuousServiceApplicationInitializer
    {
        /// <summary>
        /// Perform framework initialization before execute code in continuous service.
        /// </summary>
        public static void InitializeApplicationFramework()
        {
            System.IO.File.AppendAllText(@"C:\Temp\Test.log", System.DateTime.Now + " - InitializeApplicationFramework" + System.Environment.NewLine);
            IExceptionHandlerFactory[] arrHandlers = ExceptionHandlerUtil.GetCoreExceptionHandlerFactories();
            using (IApplicationInitialize iAppInit = LqbApplication.CreateInitializer(arrHandlers))
            {
                const string AppName = "ContinuousService";
                iAppInit.SetName(AppName);

                Adapter.ApplicationInitializer.RegisterFactories(AppName, iAppInit);
                LendersOffice.ApplicationInitializer.RegisterFactories(AppName, iAppInit);
            }

            Tools.SetupServicePointManager();
            Tools.SetupServicePointCallback();
        }
    }
}