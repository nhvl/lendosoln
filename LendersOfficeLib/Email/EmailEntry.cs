﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LendersOffice.Email
{
    /// <summary>
    /// Wrap an individual email and provide a standard interface
    /// for interrogating the email pump's current contents.
    /// </summary>
    
    public class EmailEntry
    {
        /// <summary>
        /// Wrap an individual email and provide a standard
        /// interface for interrogating the email pump's current
        /// contents.
        /// </summary>

        private CBaseEmail m_Data = null;
        private string m_Label = "";
        private string m_Id = "";

        public string Id
        {
            set { m_Id = value; }
            get { return m_Id; }
        }

        public string Label
        {
            set { m_Label = value; }
            get { return m_Label; }
        }

        public CBaseEmail Data
        {
            set { m_Data = value; }
        }

        public DateTime Retry
        {
            get { return m_Data.Retry; }
        }

        public string Subject
        {
            get { return m_Data.Subject; }
        }

        public string Message
        {
            get { return m_Data.Message; }
        }

        public string From
        {
            get { return m_Data.From; }
        }

        public string Bcc
        {
            get { return m_Data.Bcc; }
        }

        public string To
        {
            get { return m_Data.To; }
        }

        /// <summary>
        /// Provide default interface for accessing entry's
        /// key during data binding.
        /// </summary>

        public override string ToString()
        {
            // Return the entry's id by default.

            return m_Id;
        }

    }
}
