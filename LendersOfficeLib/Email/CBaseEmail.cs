﻿/// <summary>
/// Keep track of simple email description.  Note that the
/// to list is a single field that accepts semi-colon delimited
/// destination addresses.
/// </summary>

using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Mail;
using System.Runtime.InteropServices;
using System.Web;
using System.Xml.Serialization;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Constants;

namespace LendersOffice.Email
{
    [XmlRoot]
    public class CBaseEmail
    {
        /// <summary>
        /// Keep track of simple email description.
        /// </summary>

        #region ( Private Variable Declarations )
        private Guid m_BrokerId = Guid.Empty;
        private String m_Subject = String.Empty;
        private String m_From = String.Empty;
        private String m_To = String.Empty;
        private String m_Bcc = String.Empty;
        private String m_Message = String.Empty;
        private DateTime m_Retry = DateTime.Now;
        private Int32 m_Attempts = 0;
        private E_DisclaimerType m_disclaimerType = E_DisclaimerType.NORMAL;
        private List<Attachment> m_GenericAttachments;
        private Boolean m_bccToSupport = true;
        private String m_ccRecipient = String.Empty;
        #endregion

        #region ( Constructors And Helpers )
        public CBaseEmail(Guid brokerId)
            : this()
        {
            m_BrokerId = brokerId;
        }

        // Constructor for serialization.
        private CBaseEmail ()
        {
            this.IsHtmlEmailSpecified = true;
            m_GenericAttachments= new List<Attachment>();
        }

        #endregion

        #region ( Member Access Methods )
        [XmlElement]
        public Guid BrokerId
        {

            set { m_BrokerId = value; }
            get { return m_BrokerId; }
        }

        [XmlElement]
        public String Subject
        {
            // Access member.

            set
            {
                if (value != null)
                {
                    if (value.Length > ConstAppDavid.EmailSubjectMaxLength)
                    {
                        Tools.LogWarning("Email Subject Too Long. Will get truncate. Subject=[" + value + "]");
                        value = value.Substring(0, ConstAppDavid.EmailSubjectMaxLength);

                    }
                    m_Subject = value.TrimWhitespaceAndBOM();
                }
                else
                {
                    m_Subject = "";
                }
            }
            get
            {
                return m_Subject;
            }
        }

        [XmlElement]
        public String From
        {
            // Access member.

            set
            {
                if (value != null)
                {
                    m_From = value.TrimWhitespaceAndBOM();
                }
                else
                {
                    m_From = "";
                }
            }
            get
            {
                return m_From;
            }
        }

        [XmlElement]
        public String To
        {
            // Access member.

            set
            {
                if (value != null)
                {
                    m_To = value.TrimWhitespaceAndBOM().Replace("\'", "").Replace("\"", "").Replace(";",",");
                }
                else
                {
                    m_To = "";
                }
            }
            get { return m_To; }
        }

        [XmlElement]
        public String Bcc
        {
            set
            {
                if (value != null)
                {
                    m_Bcc = value.TrimWhitespaceAndBOM().Replace("\'", "").Replace("\"", "").Replace(";", ",");
                }
                else
                {
                    m_Bcc = "";
                }
            }
            get { return m_Bcc; }
        }
        [XmlElement]
        public String Message
        {
            set
            {
                if (value != null)
                {
                    m_Message = value;
                }
                else
                {
                    m_Message = "";
                }
            }
            get { return m_Message; }
        }

        [XmlAttribute]
        public E_DisclaimerType DisclaimerType
        {
            get { return m_disclaimerType; }
            set { m_disclaimerType = value; }
        }

        [XmlIgnore]
        public bool IsHtmlEmailSpecified
        {
            get;
            set;
        }

        [XmlElement]
        public bool IsHtmlEmail
        {
            get;
            set;
        }

        [XmlElement]
        public DateTime Retry
        {
            set { m_Retry = value; }
            get { return m_Retry; }
        }

        [XmlElement]
        public Int32 Attempts
        {
            set { m_Attempts = value; }
            get { return m_Attempts; }
        }

        [XmlElement]
        public String CCRecipient
        {
            set
            {
                m_ccRecipient = value.TrimWhitespaceAndBOM().Replace("\'", "").Replace("\"", "").Replace(";", ",");
            }

            get
            {
                return m_ccRecipient;
            }
        }

        [XmlElement]
        public Boolean BccToSupport
        {
            //! This should *ALMOST ALWAYS* be set to true
            set { m_bccToSupport = value; }
            get { return m_bccToSupport; }
        }


        [XmlElement("Attachment")]  
        public Attachment[] Attachments
        {
            set
            {
                if (value == null) return;
                m_GenericAttachments = new List<Attachment>(value);
            }
            get
            {
                return m_GenericAttachments.ToArray();
            }
        }

        #endregion

        #region ( Methods )

        public void AddAttachment(string attachmentName, string attachmentData)
        {
            m_GenericAttachments.Add(new Attachment(attachmentName, attachmentData));
        }

        public void AddAttachment(string attachmentName, byte[] attachmentData)
        {
            m_GenericAttachments.Add(new Attachment(attachmentName, attachmentData));
        }

        public void AddAttachment(string attachmentName, byte[] attachmentData, bool asLinkedResource)
        {
            Attachment attachment = new Attachment(attachmentName, attachmentData);
            attachment.IsLinkedResource = true;
            m_GenericAttachments.Add(attachment);
        }

        public void Send()
        {
            EmailProcessor emp = new EmailProcessor();
            emp.Send(this);
        }

        /// <summary>
        /// Use this method for sending time critical email that cannot be wait.
        /// Note: 2/21/2015 dd - In the long run we probably want to put this in high priority queue. However 
        /// due to time constraint just send directly.
        /// 
        /// For now this method DOES NOT attempt to retry when error occur.
        /// </summary>
        public void SendImmediately()
        {
            try
            {
                EmailUtilities.SendEmail(this);
                Tools.LogInfo("CBaseEmail.SendImmediately", "Send Email to [" + this.To + "] - [" + this.Subject + "]");
            }
            catch (InvalidOperationException exc)
            {
                Tools.LogError("Unable to send email to [" + this.To + "] - [" + this.Subject + "]", exc);
            }
            catch (COMException exc)
            {
                Tools.LogError("Unable to send email to [" + this.To + "] - [" + this.Subject + "]", exc);
            }
            catch (HttpException exc)
            {
                Tools.LogError("Unable to send email to [" + this.To + "] - [" + this.Subject + "]", exc);
            }
            catch (SmtpException exc)
            {
                Tools.LogError("Unable to send email to [" + this.To + "] - [" + this.Subject + "]", exc);
            }
            catch (LqbGrammar.Exceptions.LqbException exc) when (exc.InnerException is SmtpException)
            {
                Tools.LogError("Unable to send email to [" + this.To + "] - [" + this.Subject + "]", exc.InnerException);
            }
            catch (Exception exc)
            {
                Tools.LogError("Unable to send email to [" + this.To + "] - [" + this.Subject + "]", exc);
                throw;
            }

        }

        #endregion

        #region ( Serialization Operators )

        /// <summary>
        /// Translate xml document back into email message.
        /// </summary>
        /// <returns>
        /// Email message representation.
        /// </returns>

        public static CBaseEmail ToObject(Stream sXml)
        {
            // Translate xml document back into email message.
            return SerializationHelper.XmlDeserialize(sXml, typeof(CBaseEmail)) as CBaseEmail;
        }

        /// <summary>
        /// Translate email message into xml document.
        /// </summary>
        /// <returns>
        /// Xml document representation.
        /// </returns>

        public override String ToString()
        {
            // Translate email message into xml document.

            //return SerializationHelper.XmlSerialize(this);
            XmlSerializer xs = new XmlSerializer(typeof(CBaseEmail));
            StringWriter8 sw = new StringWriter8();

            xs.Serialize(sw, this);

            return sw.ToString();
        }

        #endregion

    }
}