﻿// <copyright file="SendEmailProcessor.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: David Dao
//    Date:   5/21/2014 2:17:55 AM 
// </summary>
namespace LendersOffice.Email
{
    using System;
    using System.Diagnostics;
    using System.Net.Mail;
    using System.Text;
    using System.Threading;
    using System.Web;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.ObjLib.DatabaseMessageQueue;

    /// <summary>
    /// A continuous process that process email from DB Message queue and send through SMTP server.
    /// </summary>
    public class SendEmailProcessor : MarshalByRefObject, CommonProjectLib.Runnable.IRunnable
    {
        /// <summary>
        /// Number of retries.
        /// </summary>
        private const int MaxAttempts = 10;

        /// <summary>
        /// Gets the description of the continuous process.
        /// </summary>
        /// <value>The description of the continuous process.</value>
        public string Description
        {
            get { return "Send email from DB Message queue."; }
        }

        /// <summary>
        /// Retrieve message from DB message queue and send email.
        /// </summary>
        public void Run()
        {
            DBMessageQueue queue = new DBMessageQueue(ConstMsg.EmailQueue);
            int processCount = 0;
            Tools.ResetLogCorrelationId();

            using (var workerTiming = new WorkerExecutionTiming("SendEmailProcessor"))
            {
                while (processCount++ < 1000)
                {
                    // 3/11/2015 dd - To minimize SmtpStatusCode=ServiceNotAvailable error, we are going to sleep briefly after every 10 processings.
                    if (processCount % 10 == 0)
                    {
                        LendersOffice.Sleeping.PerformanceLoggingSleeper.Instance.Sleep(150);
                    }

                    Stopwatch stopwatch = Stopwatch.StartNew();
                    DBMessage item = null;

                    try
                    {
                        item = queue.Receive();
                    }
                    catch (DBMessageQueueException)
                    {
                        return;
                    }

                    if (item == null)
                    {
                        return;
                    }

                    using (var itemTiming = workerTiming.RecordItem(item.Subject1, item.InsertionTime))
                    {
                        // Send out this message.  If we fail to send, then
                        // we re-queue the message onto the tail for future
                        // attempts.  Each re-queue bumps up the attempt
                        // count by one.  Once a threshold is reached, we
                        // pull the email and log it for manual retry.
                        CBaseEmail baseEmail = CBaseEmail.ToObject(item.BodyStream);

                        if (baseEmail == null)
                        {
                            continue; // Skip to next item.
                        }

                        if (baseEmail.Retry > DateTime.Now || item.Subject2 == "Error")
                        {
                            // Not ready to send this one, so put it back in the
                            // oven.  Once ready, we'll check the server again.
                            queue.Send(baseEmail.Subject, baseEmail.ToString(), item.Subject2);
                        }
                        else if (string.IsNullOrEmpty(baseEmail.From))
                        {
                            this.MarkAsError(queue, baseEmail);
                        }
                        else if (string.IsNullOrEmpty(baseEmail.To) && string.IsNullOrEmpty(baseEmail.Bcc))
                        {
                            // No recipient address put in error queue.
                            this.MarkAsError(queue, baseEmail);
                        }
                        else
                        {
                            // We have something to work with, so attempt to send it out.
                            if (baseEmail.Attempts < MaxAttempts)
                            {
                                baseEmail.Attempts++;

                                StringBuilder debugInfo = new StringBuilder();
                                try
                                {
                                    debugInfo.AppendFormat(
                                        "SendEmailProcessor [{6}] From=[{0}], To=[{1}], Bcc=[{2}], Subject=[{3}], Insertion Time=[{4}], Attempts=[{5}] ",
                                        baseEmail.From,
                                        baseEmail.To,
                                        baseEmail.Bcc,
                                        baseEmail.Subject,
                                        item.InsertionTime,
                                        baseEmail.Attempts - 1,
                                        processCount);

                                    if (string.IsNullOrEmpty(baseEmail.Bcc) == false && baseEmail.Bcc != baseEmail.To)
                                    {
                                        // OPM 3189
                                        string bccMessage = "You are set to receive a copy of the following event notification:\n------------------------------------------------------------------\n\n";
                                        if (baseEmail.Message.IndexOf(bccMessage) < 0)
                                        {
                                            baseEmail.Message = bccMessage + baseEmail.Message;
                                        }

                                        EmailUtilities.SendEmail(baseEmail);
                                    }

                                    EmailUtilities.SendEmail(baseEmail);

                                    debugInfo.Append("OK. Execute in " + stopwatch.ElapsedMilliseconds + "ms.");

                                    Tools.LogInfo("SendEmailProcessor", debugInfo.ToString());
                                }
                                catch (FormatException exc)
                                {
                                    // 3/5/2015 dd - When From/To email address is not in valid format.
                                    this.MarkAsError(queue, baseEmail);

                                    Tools.LogError(debugInfo.ToString(), exc);
                                }
                                catch (InvalidOperationException exc)
                                {
                                    this.MarkAsError(queue, baseEmail);

                                    Tools.LogError(debugInfo.ToString(), exc);
                                }
                                catch (HttpException exc)
                                {
                                    if (exc.Message.StartsWith("The server rejected one or more recipient addresses."))
                                    {
                                        // 5/23/2014 dd - No point of retry when recipient addresses are invalid.
                                        this.MarkAsError(queue, baseEmail);
                                    }
                                    else
                                    {
                                        // Will attempt to retry.
                                        baseEmail.Retry = DateTime.Now.AddMinutes(5);
                                        queue.Send(baseEmail.Subject, baseEmail.ToString(), item.Subject2);
                                    }

                                    Tools.LogError(debugInfo.ToString(), exc);
                                }
                                catch (SmtpException exc)
                                {
                                    bool hardError = false;
                                    if (exc.Message.StartsWith("Mailbox unavailable"))
                                    {
                                        // 5/30/2014 dd - On CMG hosted server, even this exception was throw by mail server but it still send through.
                                        // So do not retry sending email to avoid email duplication.
                                        this.MarkAsError(queue, baseEmail);
                                        hardError = true;
                                    }
                                    else if (exc.StatusCode == SmtpStatusCode.SyntaxError)
                                    {
                                        // 3/5/2015 dd - No point of retry if server is reject on syntax error (i.e addresses are invalid)
                                        this.MarkAsError(queue, baseEmail);
                                        hardError = true;
                                    }
                                    else
                                    {
                                        baseEmail.Retry = DateTime.Now.AddSeconds(30);
                                        queue.Send(baseEmail.Subject, baseEmail.ToString(), item.Subject2);
                                    }

                                    if (hardError)
                                    {
                                        debugInfo.AppendLine(" FAILED SmtpStatusCode=[" + exc.StatusCode + "]");
                                        Tools.LogError(debugInfo.ToString(), exc);
                                    }
                                    else if (baseEmail.Attempts <= 3)
                                    {
                                        debugInfo.AppendLine(" FAILED_WILL_RETRY SmtpStatusCode=[" + exc.StatusCode + "] - " + exc.Message);
                                        Tools.LogInfo("SendEmailProcessor", debugInfo.ToString());
                                    }
                                    else
                                    {
                                        debugInfo.AppendLine(" FAILED_WILL_RETRY SmtpStatusCode=[" + exc.StatusCode + "]");
                                        Tools.LogError(debugInfo.ToString(), exc);
                                    }
                                }
                                catch (LqbGrammar.Exceptions.LqbException exc) when (exc.InnerException is SmtpException)
                                {
                                    // NOTE: ultimately the exception is from the adapter which places the .net framework exception as the inner exception
                                    var frameworkException = exc.InnerException as System.Net.Mail.SmtpException;
                                    var statusCode = frameworkException.StatusCode;
                                    bool hardError = false;
                                    if (frameworkException.Message.StartsWith("Mailbox unavailable"))
                                    {
                                        // 5/30/2014 dd - On CMG hosted server, even this exception was throw by mail server but it still send through.
                                        // So do not retry sending email to avoid email duplication.
                                        this.MarkAsError(queue, baseEmail);
                                        hardError = true;
                                    }
                                    else if (statusCode == System.Net.Mail.SmtpStatusCode.SyntaxError)
                                    {
                                        // 3/5/2015 dd - No point of retry if server is reject on syntax error (i.e addresses are invalid)
                                        this.MarkAsError(queue, baseEmail);
                                        hardError = true;
                                    }
                                    else
                                    {
                                        baseEmail.Retry = DateTime.Now.AddSeconds(30);
                                        queue.Send(baseEmail.Subject, baseEmail.ToString(), item.Subject2);
                                    }

                                    if (hardError)
                                    {
                                        debugInfo.AppendLine(" FAILED SmtpStatusCode=[" + statusCode + "]");
                                        Tools.LogError(debugInfo.ToString(), frameworkException);
                                    }
                                    else if (baseEmail.Attempts <= 3)
                                    {
                                        debugInfo.AppendLine(" FAILED_WILL_RETRY SmtpStatusCode=[" + statusCode + "] - " + frameworkException.Message);
                                        Tools.LogInfo("SendEmailProcessor", debugInfo.ToString());
                                    }
                                    else
                                    {
                                        debugInfo.AppendLine(" FAILED_WILL_RETRY SmtpStatusCode=[" + statusCode + "]");
                                        Tools.LogError(debugInfo.ToString(), frameworkException);
                                    }
                                }
                                catch (Exception exc)
                                {
                                    this.MarkAsError(queue, baseEmail);

                                    Tools.LogError(debugInfo.ToString(), exc);

                                    throw;
                                }
                            }
                            else
                            {
                                // Max Attempt Handle.
                                //
                                // Unable to send, so bump this message through retry state machine. We retry in a couple of hours.
                                // and then archive if not possible
                                if (item.Subject2 == "Archived")
                                {
                                    this.MarkAsError(queue, baseEmail);
                                }
                                else if (item.Subject2 == "Retry")
                                {
                                    baseEmail.Retry = DateTime.Now.AddHours(6);
                                    baseEmail.Attempts = 0;
                                    queue.Send(baseEmail.Subject, baseEmail.ToString(), "Archived");
                                }
                                else if (item.Subject2 == "Mail")
                                {
                                    baseEmail.Retry = DateTime.Now.AddHours(1);
                                    baseEmail.Attempts = 0;
                                    queue.Send(baseEmail.Subject, baseEmail.ToString(), "Retry");
                                }
                                else if (item.Subject2 == "Note")
                                {
                                    baseEmail.Retry = baseEmail.Retry.AddHours(1);
                                    baseEmail.Attempts = 0;
                                    queue.Send(baseEmail.Subject, baseEmail.ToString(), "Note");
                                }
                            }
                        }
                    } // using itemTiming
                } // while
            } // using workerTiming
        }

        /// <summary>
        /// Mark the email as error.
        /// </summary>
        /// <param name="queue">The error message queue.</param>
        /// <param name="email">The email to mark as error.</param>
        private void MarkAsError(DBMessageQueue queue, CBaseEmail email)
        {
            queue.Send(email.Subject, email.ToString(), "Error");
        }
    }
}