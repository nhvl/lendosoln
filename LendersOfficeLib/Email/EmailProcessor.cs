using System;
using System.Collections.Generic;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Constants;
using LendersOffice.ObjLib.DatabaseMessageQueue;

namespace LendersOffice.Email
{
	
	/// <summary>
	/// Sit in our own thread and process emails that currently reside
	/// in our message queue.
	/// </summary>

	public class EmailProcessor
	{
        private const string DO_NOT_SEND_EMAIL = "Do_Not_Send@lendingqb.com"; // OPM 142377

		public IEnumerable<EmailEntry> Entries
		{
			get 
            {
                List<EmailEntry> list = new List<EmailEntry>();
                DBMessageQueue mQ = new DBMessageQueue(ConstMsg.EmailQueue);

                foreach (DBMessage mItem in mQ)
                {
                    // The email info is translated from xml to our
                    // email container.
                    EmailEntry eEntry = new EmailEntry();
                    eEntry.Data = CBaseEmail.ToObject(mItem.BodyStream);
                    eEntry.Label = mItem.Subject2;
                    eEntry.Id = mItem.Id.ToString();
                    list.Add(eEntry);
                }
                return list; 
            }
		}

		/// <summary>
		/// Place a new email message into the queue for subsequent
		/// sending by our background pump.  We place the message
		/// into the queue regardless of the state of the pump.
        /// Assumes normal disclaimer
		/// </summary>
        public void Send(CBaseEmail cbe)
        {
            // OPM 142377 - Prevent emails from being sent out
            if (cbe.From == DO_NOT_SEND_EMAIL)
            {
                return;
            }

            // Place it in our queue.  This path must be where we
            // initialized our pump, or it won't get sent.

            // For each recipient in the recipient list, add a message to the queue addressed to that recipient.
            // additionally add another recipient (the Bcc) if that one wasn't already in the toList.
            HashSet<string> toHashSet = new HashSet<string>(StringComparer.InvariantCultureIgnoreCase);
            // Do the same for cc
            HashSet<string> bccHashSet = new HashSet<string>(StringComparer.InvariantCultureIgnoreCase);

            HashSet<string> emailWhitelist = ConstStage.LoDevEmailWhitelist;

            if (string.IsNullOrEmpty(cbe.To) == false)
            {
                //make sure each one is unique there is no difference between bcc and to and bcc so we can use the same list
                //to keep uniques
                foreach (string sTo in cbe.To.Split(',', ';'))
                {
                    toHashSet.Add(sTo.TrimWhitespaceAndBOM());
                }
            }
            if (false == string.IsNullOrEmpty(cbe.Bcc))
            {
                foreach (string sBcc in cbe.Bcc.Split(',', ';'))
                {
                    bccHashSet.Add(sBcc.TrimWhitespaceAndBOM());
                }
            }

            if (toHashSet.Count == 0)
            {
                throw new CBaseException(ErrorMessages.Generic, "Missing to or bcc address.");
            }
            cbe.Bcc = "";

            // Send to the "to" and "cc" recipients
            List<string> toList = new List<string>(toHashSet.Count);
            List<string> ccList = new List<string>();
            if (string.IsNullOrEmpty(cbe.CCRecipient) == false)
            {
                foreach (string sCc in cbe.CCRecipient.Split(',', ';'))
                {
                    if(!WillSend(sCc))
                    {
                        continue;
                    }

                    ccList.Add(sCc);
                }
            }

            foreach (var item in toHashSet)
            {
                if(!WillSend(item))
                {
                    continue;
                }

                toList.Add(item);
            }

            if (toList.Count != 0)
            {
                cbe.To = string.Join(",", toList.ToArray());
                cbe.CCRecipient = string.Join(",", ccList.ToArray());
                lock (this)
                {
                    DBMessageQueue mQ = new DBMessageQueue(ConstMsg.EmailQueue);
                    mQ.Send(cbe.Subject, cbe.ToString(), "Mail");
                }
            }

            // Send to the "bcc" recipients one at a time using the "to" field
            cbe.CCRecipient = "";
            foreach (string sEmail in bccHashSet)
            {
                if(!WillSend(sEmail))
                {
                    continue;
                }

                cbe.To = sEmail;

                lock (this)
                {
                    DBMessageQueue mQ = new DBMessageQueue(ConstMsg.EmailQueue);
                    mQ.Send(cbe.Subject, cbe.ToString(), "Mail");
                }
            }
        }

        /// <summary>
        /// Returns false if it's some local/dev stage but the email isn't in the stage's LoDevEmailWhitelist.
        /// </summary>
        /// <param name="email">The email we are considering sending to.</param>
        /// <returns>Truee iff we should go ahend and send the email.</returns>
        private bool WillSend(string email)
        {
            if ((ConstAppDavid.CurrentServerLocation == ServerLocation.LocalHost || ConstAppDavid.CurrentServerLocation == ServerLocation.Development || ConstAppDavid.CurrentServerLocation == ServerLocation.DevCopy)
                && !ConstStage.LoDevEmailWhitelist.Contains(email))
            {
                // If we are on localhost or dev, do not include emails that are not in the whitelist.
                Tools.LogInfo($"{email} is not in the whitelist. Will not send email to this recipient."); 
                return false;
            }

            return true;
        }

		/// <summary>
		/// Fetch existing email from the queue and resubmit it with
		/// a fresh attempts count and standard Subject2.
		/// </summary>

		public void Send( String sMessageId , String sTo )
		{
			// Load the specified message from the queue.  We access
			// as a client so that we don't lock up background
			// processing.  The message is reset and resent to the
			// queue and should be processed again as if it were
			// a new email request.  Email content remains unchanged.
			//
			// 10/15/2004 kb - We assume that we have an updated to
			// address so that a resend could be more successful.

            HashSet<string> emailWhitelist = ConstStage.LoDevEmailWhitelist;

            if(!WillSend(sTo))
            {
                return;
            }

			try
			{
				if( sMessageId != null )
				{
					DBMessageQueue mQ = new DBMessageQueue( ConstMsg.EmailQueue ) ;
					
					CBaseEmail eM;

					try
					{
						eM = CBaseEmail.ToObject( mQ.ReceiveById( Convert.ToInt64(sMessageId )).BodyStream );
					}
					catch( Exception e )
					{
                        throw new CBaseException(ErrorMessages.FailedToReceiveMessage, ErrorMessages.FailedToReceiveMessage + " " + e.ToString());
					}

                    // OPM 142377 - Prevent emails from being sent out
                    if (eM.From == DO_NOT_SEND_EMAIL)
                        return;

					eM.Retry    = DateTime.Now;
					eM.Attempts = 0;

					eM.To = sTo;

					mQ.Send(eM.Subject, eM.ToString() , "Mail" );
				}
				
			}
			catch( Exception e )
			{
                Tools.LogError("Email processor: Failed to send existing message.", e);
                throw;
			}
		}

		/// <summary>
		/// Fetch existing email from the queue and let it dissolve.
		/// By not adding it back into the queue, we have essentially
		/// dropped it from our processor.
		/// </summary>

		public void Drop( String sMessageId )
		{
			// Load the specified message from the queue and let it
			// die outside the processor's set.
           
			try
			{
				if( sMessageId != null )
				{
					DBMessageQueue mQ = new DBMessageQueue( ConstMsg.EmailQueue );
					
					try
					{
						mQ.ReceiveById( Convert.ToInt64(sMessageId ));
					}
					catch( Exception e )
					{
                        throw new CBaseException(ErrorMessages.FailedToRemoveMessage, ErrorMessages.FailedToRemoveMessage + " " + e.ToString());
					}
					
				}
			}
			catch( Exception e )
			{
				// Pass on the love.
				Tools.LogError( "Email processor: Failed to drop existing message.", e );

				throw;
			}
		}

		/// <summary>
		/// Remove all entries from the email queue.  We skip non-
		/// mail messages in case other things are in the queue.
		/// </summary>

		public void Clear()
		{
			// Get the message queue and remove all mail messages.

			try
			{
				DBMessageQueue mQ = new DBMessageQueue( ConstMsg.EmailQueue );
				{
					try
					{
						foreach( DBMessage qM in mQ )
						{
							if( qM.Subject2 == "Mail" )
							{
								try
								{
									mQ.ReceiveById( Convert.ToInt64(qM.Id));
								}
								catch
								{
									continue;
								}
							}
						}
					}
					catch( Exception e )
					{
						throw new CBaseException(ErrorMessages.FailedToClearMessages, ErrorMessages.FailedToClearMessages + " " + e.ToString());
					}
				}
			}
			catch( Exception e )
			{
				// Pass on the love.

				Tools.LogError( "Email processor: Failed to clear message queue.", e);
				Tools.LogError( e );

				throw;
			}
		}

	}

}