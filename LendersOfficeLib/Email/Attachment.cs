﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace LendersOffice.Email
{

    public class Attachment
    {
        private string sAttachmentName;
        private string sAttachmentData;
        private bool wasBinary;

        #region ( Constructors )
        public Attachment()
        {
            sAttachmentName = "";
            sAttachmentData = "";
        }

        public Attachment(string AttachmentName, string AttachmentData)
        {
            sAttachmentName = AttachmentName;
            sAttachmentData = AttachmentData;
            wasBinary = false;
            IsLinkedResource = false;
        }

        public Attachment(string AttachmentName, byte[] AttachmentData)
        {
            sAttachmentName = AttachmentName;
            sAttachmentData = Convert.ToBase64String(AttachmentData);
            wasBinary = true;
            IsLinkedResource = false;
        }


        #endregion

        #region ( Properties ) 
        [XmlAttribute("name")]
        public string Name
        {
            set { sAttachmentName = value; }
            get { return sAttachmentName; }
        }

        [XmlElement("data")]
        public string Data
        {
            set { sAttachmentData = value; }
            get { return sAttachmentData; }
        }

        [XmlAttribute("wasBinary")]
        public bool WasBinary
        {
            set { wasBinary = value; }
            get { return wasBinary; }
        }

        [XmlAttribute("isLinkedResource")]
        public bool IsLinkedResource
        {
            get;
            set; 
        }

        #endregion
    }

}
