﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;
using LendersOffice.Security;

namespace LendersOffice.PriceMyLoan
{

    public class ResultActionLink
    {
        public const string ResultActionLinkTitle_RegisterLock = "register loan/lock rate"; // 7/28/2009 dd - OPM 32212
        public const string ResultActionLinkTitle_RegisterOnly = "register loan"; // 8/24/2009 dd - OPM 34423
        public const string ResultActionLinkTitle_LockOnly = "lock rate"; // 8/24/2009 dd - OPM 33832
        public const string ResultActionLinkTitle_GoTo2ndLien = "go to 2nd lien";
        public const string ResultActionLinkTitle_LockUnavailable = "lock unavailable"; // 8/25/2009 dd - OPM 33832
        public const string ResultActionLinkTitle_Select = "select";
        public const string ResultActionLinkTitle_RegisterLockDisabled = "register loan/?";
        public const string ResultActionLinkTitle_LockDisabled = "?";

        /// <summary>
        /// Return two links title to used in best price pricing mode. The first link will be use for normal rate. The second link will be use when rate is expire.
        /// If RSE feature is not turn on the first link and second will be the same.
        /// </summary>
        /// <returns></returns>
        public static List<string> GetLinkTitles_NoWorkflow(bool has2ndLoan, E_sLienQualifyModeT currentLienQualifyModeT, 
            bool isPmlSubmissionAllowed, bool isEncompassIntegration, E_LpeRunModeT lpeRunModeT, bool isRateLockedAtSubmission)
        {
            string[] titles = new string[2];


            if (has2ndLoan && currentLienQualifyModeT == E_sLienQualifyModeT.ThisLoan)
            {
                titles[0] = ResultActionLinkTitle_GoTo2ndLien;
                titles[1] = ResultActionLinkTitle_GoTo2ndLien;
            }
            else
            {
                if (!isPmlSubmissionAllowed && !isEncompassIntegration)
                {
                    // 8/25/2009 dd - For Citi Integration
                    titles[0] = ResultActionLinkTitle_Select;
                    titles[1] = ResultActionLinkTitle_Select;
                }
                else
                {
                    if (isRateLockedAtSubmission)
                    {
                        switch (lpeRunModeT)
                        {
                            case E_LpeRunModeT.NotAllowed:
                                titles[0] = string.Empty;
                                titles[1] = string.Empty;
                                break;
                            case E_LpeRunModeT.OriginalProductOnly_Direct:
                                titles[0] = ResultActionLinkTitle_LockOnly;
                                titles[1] = ResultActionLinkTitle_LockUnavailable;
                                break;
                            case E_LpeRunModeT.Full:
                                titles[0] = ResultActionLinkTitle_RegisterLock;
                                titles[1] = ResultActionLinkTitle_RegisterOnly;
                                break;
                            default:
                                throw new UnhandledEnumException(lpeRunModeT);
                        }
                    }
                    else
                    {
                        switch (lpeRunModeT)
                        {
                            case E_LpeRunModeT.NotAllowed:
                            case E_LpeRunModeT.OriginalProductOnly_Direct:
                                titles[0] = string.Empty;
                                titles[1] = string.Empty;
                                break;
                            case E_LpeRunModeT.Full:
                                titles[0] = ResultActionLinkTitle_RegisterOnly;
                                titles[1] = ResultActionLinkTitle_RegisterOnly;
                                break;
                            default:
                                throw new UnhandledEnumException(lpeRunModeT);
                        }
                    }
                }
            }
            return new List<string>(titles);
        }

        /// <summary>
        /// Return two links title to used in best price pricing mode. The first link will be use for normal rate. The second link will be use when rate is expire.
        /// If RSE feature is not turn on the first link and second will be the same.
        /// </summary>
        /// <returns></returns>
        public static List<string> GetLinkTitles(bool has2ndLoan, E_sLienQualifyModeT currentLienQualifyModeT
            , bool isPmlSubmissionAllowed, bool isEncompassIntegration, E_LpeRunModeT lpeRunModeT, bool isRateLockedAtSubmission
            , bool isAllowSubmitForLock, bool isLoanRegistered, bool userCanRegister)
        {
            string[] titles = new string[2];


            #region Special Cases for Combo and Citi
            // Special case 1: This is results for a combo, but we are
            // looking at the first lien result.
            if (has2ndLoan && currentLienQualifyModeT == E_sLienQualifyModeT.ThisLoan)
            {
                return new List<string>( new string[] {
                ResultActionLinkTitle_GoTo2ndLien, // Normal Link
                ResultActionLinkTitle_GoTo2ndLien  // Expired Link
                } );
            }

            // Special case 2: Citi gets "select" link always
            if (!isPmlSubmissionAllowed && !isEncompassIntegration)
            {
                // 8/25/2009 dd - For Citi Integration
                return new List<string>(new string[] {
                ResultActionLinkTitle_Select, // Normal Link
                ResultActionLinkTitle_Select  // Expired Link
                });
            }
            #endregion

            string regularLink;
            string expiredLink;

            switch (lpeRunModeT)
            {
                case E_LpeRunModeT.Full:
                    {
                        expiredLink = ResultActionLinkTitle_RegisterOnly;

                        if (isRateLockedAtSubmission == false)
                        {
                            regularLink = ResultActionLinkTitle_RegisterOnly;
                        }
                        else if (isAllowSubmitForLock)
                        {
                            regularLink = ResultActionLinkTitle_RegisterLock;
                        }
                        else
                        {
                            regularLink = ResultActionLinkTitle_RegisterLockDisabled;
                        }

                        break;
                    }
                case E_LpeRunModeT.OriginalProductOnly_Direct:
                    {
                        if (isRateLockedAtSubmission == false)
                        {
                            regularLink = string.Empty;
                            expiredLink = string.Empty;
                        }
                        else
                        {
                            expiredLink = ResultActionLinkTitle_LockUnavailable;

                            if (isAllowSubmitForLock)
                                regularLink = ResultActionLinkTitle_LockOnly;
                            else
                                regularLink = ResultActionLinkTitle_LockDisabled;
                        }
                        break;
                    }
                    
                case E_LpeRunModeT.NotAllowed:
                    {
                        expiredLink = string.Empty;
                        regularLink = string.Empty;
                    }
                    break;
                default:
                    throw new UnhandledEnumException(lpeRunModeT);
            }

            if (!userCanRegister)
            {
                regularLink = regularLink.Replace("register loan", "register loan?");
                expiredLink = expiredLink.Replace("register loan", "register loan?");
            }
            return new List<string>(new string[] { regularLink, expiredLink });
        }
    }
}
