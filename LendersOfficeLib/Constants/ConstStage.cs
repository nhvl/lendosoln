///
/// Author: David Dao
///
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using DataAccess;
using LendersOffice.Common;
using LqbGrammar.DataTypes;

namespace LendersOffice.Constants
{
    public class StageConfigKeyNotFoundException : NotFoundException
    {
        public StageConfigKeyNotFoundException(string key) : base (key + " not define in Stage_Config.", key + " not define in Stage_Config.")
        {
        }
    }

    /// <summary>
    /// Constants that must be different between localhost, demo and production. All these constants should
    /// be store in Stage_Config DB table.
    /// </summary>
    /// <remarks>THERE ARE MANY LOCATIONS WHERE STAGE CALLS CASCADE INTO SITE CALLS.  THIS IS ALLOWED, BUT CANNOT GO THE OTHER WAY ROUND.</remarks>
    public class ConstStage
    {
        /// <summary>
        /// 2019-02-20 - Indicate whether to route iTextSharp call to the separate web api. Eventually we need to retire this settings and always route to webapi.
        /// This settings is stem from case 478569.
        /// </summary>
        public static bool UseiTextSharpWebApi => GetIntValue(StageConfigDatum.UseiTextSharpWebApi, 0) == 1;

        /// <summary>
        /// 2019-02-20 -dd - The url of the iTextSharp web api. The iTextSharp web api must setup on each of the server because it use local file to access the pdf file.
        /// Since the web api is local on the machine, there is no need to do https.
        /// </summary>
        public static string ITextSharpWebApiUrl => GetStringValue(StageConfigDatum.ITextSharpWebApiUrl, "http://itextsharp");

        /// <summary>
        /// Gets the number of SQL parameters sent to 
        /// the database in a single query for LPE UI.
        /// </summary>
        /// <remarks>
        /// The "View Associations" popup and other LPE UI pages
        /// were sending several thousand SQL parameters in a single
        /// DB query and exceeding the 2048 hard cap in place for SQL 
        /// Server when viewed for master programs, see cases 469021
        /// and 475020. Chunking was introduced as a stopgap fix
        /// until this page can be rewritten to use table-valued
        /// parameters in a stored procedure, which the LPE
        /// database does not yet support. Increasing this
        /// value will decrease the total number of SQL calls
        /// per query but will increase the dynamic SQL string
        /// length and processing time. 
        /// </remarks>
        public static int LpeUiSqlParameterChunkSize => GetIntValue(StageConfigDatum.LpeUiSqlParameterChunkSize, 50);

        /// <summary>
        /// Gets a value indicating whether PDF generation should be completed synchronously
        /// instead of being routed to the background jobs framework.
        /// </summary>
        public static bool UseSynchronousPdfGenerationFallback => GetIntValue(StageConfigDatum.UseSynchronousPdfGenerationFallback, 0) == 1;

        /// <summary>
        /// List of whitelist ip to access /CheckInternalResource.aspx
        /// </summary>
        public static string CheckInternalResourceWhitelistIps => GetStringValue(StageConfigDatum.CheckInternalResourceWhitelistIps, string.Empty);

        /// <summary>
        /// Gets the host name of alternative servers to forward async postback. For example, when vendor POST to secure.lendingqb.com with
        /// loan id of wsecure.lendingqb.com, we will forward the request to wsecure.lendingqb.com. The value for this will be just host name without scheme.
        /// Example value is wsecure.lendingqb.com.
        /// Leave value empty if the site is the last in the chain.
        /// </summary>
        public static string AsyncPostbackForwardingHostname => GetStringValue(StageConfigDatum.AsyncPostbackForwardingHostname, string.Empty);

        /// <summary>
        /// Gets the site code indicating where the LQB web services authentication ticket is valid.
        /// </summary>
        public static string SiteCode => GetStringValue(StageConfigDatum.SiteCode, string.Empty);

        /// <summary>
        /// Gets a value indiciating whether the BJP should be disabled for VOA.
        /// </summary>
        public static bool DisableVoaViaBJP => GetIntValue(StageConfigDatum.DisableVoaViaBJP, 0) == 1;

        /// <summary>
        /// Gets the seconds between polling calls for VOA to the BJP.
        /// </summary>
        public static int VoaBJPPollingIntervalInSeconds => GetIntValue(StageConfigDatum.VoaBJPPollingIntervalInSeconds, 10);

        /// <summary>
        /// Gets the max polling attempts for VOA to the BJP.
        /// </summary>
        public static int VoaBJPMaxPollingAttempts => GetIntValue(StageConfigDatum.VoaBJPMaxPollingAttempts, 18);

        /// <summary>
        /// The size is rows * columns.  Zero is the magic number meaning no value.  See opm 478538 / PR 12722.
        /// </summary>
        public static int MaxReportSizeBeforeFailCustomReport => GetIntValue(StageConfigDatum.MaxReportSizeBeforeFailCustomReport, 0);

        /// <summary>
        /// Gets the number of seconds between successive polling calls for
        /// background PDF generation runs.
        /// </summary>
        public static int PdfGenerationBackgroundJobPollingIntervalSeconds => GetIntValue(StageConfigDatum.PdfGenerationBackgroundJobPollingIntervalSeconds, 5);

        /// <summary>
        /// Gets the maximum number of attempts to poll for PDF generation jobs
        /// before quitting.
        /// </summary>
        public static int PdfGenerationBackgroundJobMaxPollingAttempts => GetIntValue(StageConfigDatum.PdfGenerationBackgroundJobMaxPollingAttempts, 10);

        /// <summary>
        /// Disables ordering mi orders via the BJP. Default to false.
        /// </summary>
        public static bool DisableMIFrameworkViaBJP => GetIntValue(StageConfigDatum.DisableMIFrameworkViaBJP, 0) == 1;

        /// <summary>
        /// Disables ordering credit via the BJP. Default to false.
        /// </summary>
        public static bool DisableCreditOrderingViaBJP => GetIntValue(StageConfigDatum.DisableCreditOrderingViaBJP, 0) == 1;

        /// <summary>
        /// Gets a value indicating whether the BrowserXT verification post-login
        /// step is disabled across all lenders.
        /// </summary>
        public static bool DisableBrowserXtVerificationPostLogInStep => GetIntValue(StageConfigDatum.DisableBrowserXtVerificationPostLogInStep, 0) == 1;

        /// <summary>
        /// The max attempts to try when polling the background job processor for MI framework request results.
        /// </summary>
        public static int MIFrameworkRequestJobPollingMaxAttempts => GetIntValue(StageConfigDatum.MIFrameworkRequestJobPollingMaxAttempts, 18);

        /// <summary>
        /// The interval to use between polling the background job processor for an MI Framework Request job.
        /// </summary>
        public static int MIFrameworkRequestJobPollingInterval => GetIntValue(StageConfigDatum.MIFrameworkRequestJobPollingInterval, 10);

        /// <summary>
        /// Gets the environment name to log to PB and IIS. Sample values are: dev, alpha, beta, secure, wsecure.
        /// </summary>
        public static string EnvironmentName => GetStringValue(StageConfigDatum.EnvironmentName, string.Empty);

        /// <summary>
        /// Gets the number of seconds a failed <see cref="TaskAutomationJob"/> should wait before retrying.
        /// </summary>
        public static int TaskAutomationJobRetryWaitInterval => GetIntValue(StageConfigDatum.TaskAutomationJobRetryWaitInterval, 10);

        /// <summary>
        /// Gets the number of seconds between successive polling calls for
        /// background custom report runs.
        /// </summary>
        public static int CustomReportBackgroundJobPollingIntervalSeconds => GetIntValue(StageConfigDatum.CustomReportBackgroundJobPollingIntervalSeconds, 10);

        /// <summary>
        /// Gets the maximum number of polling attempts for a custom report job.
        /// </summary>
        public static int CustomReportJobPollingRetryLimit => GetIntValue(StageConfigDatum.CustomReportJobRetryLimit, 10);

        /// <summary>
        /// Gets the amount of time the disclosure count on the pipeline should be cached. A value of 0 disables the caching and a negative value removes the tab.
        /// </summary>
        public static int DisclosureCountOnPipelineCachingTimeInMs => GetIntValue(StageConfigDatum.DisclosureCountOnPipelineCachingTimeInMs, 0);

        /// <summary>
        /// Gets a value indicating whether the task tab should be disabled.
        /// </summary>
        public static bool DisableTaskPipelineTab => GetIntValue(StageConfigDatum.DisableTaskPipelineTab, 0) == 1;

        /// <summary>
        /// Avoids locking the schema object in custom reports prior to translating between
        /// report values and SQL.
        /// </summary>
        /// <remarks>
        /// This lock appears to be unnecessary, as the schema is initialized in a static 
        /// constructor and not modified after initialization. This bit can be removed once
        /// the changes have been vetted.
        /// </remarks>
        public static bool AvoidLockForCustomReportSqlQueryGeneration => GetIntValue(StageConfigDatum.AvoidLockForCustomReportSqlQueryGeneration, 0) == 1;

        /// Forces getters and setters for encrypted loan and application fields to
        /// retrieve and store the plaintext values.
        /// </summary>
        /// <remarks>
        /// This bit will act as a kill switch for the changes being made to encrypt
        /// SSN fields in case 467073 and can be removed once the changes have been
        /// vetted.
        /// </remarks>
        public static bool ForcePlaintextValueforEncryptedLoanFields => GetIntValue(StageConfigDatum.ForcePlaintextValueforEncryptedLoanFields, 0) == 1;

        /// <summary>
        /// Gets a value indicating whether <see cref="EDocs.EDocument.GetPDFTempFile_Current"/>
        /// and <see cref="EDocs.EDocument.GetPDFTempFile_Original"/> will check if the document's 
        /// broker is using Amazon for edocs and, if so, retrieve the PDF directly from Amazon.
        /// </summary>
        public static bool GenerateNonDestructiveEdocsUsingAmazon => GetIntValue(StageConfigDatum.GenerateNonDestructiveEdocsUsingAmazon, 1) == 1;

        /// <summary>
        /// Gets the export URL for MeridianLink's hosted Kiva solution for exporting data to core systems.
        /// </summary>
        public static string KivaCoreExportUrl => GetStringValue(StageConfigDatum.KivaCoreExportUrl, string.Empty);

        /// <summary>
        /// Gets the secret key sent in the encrypted XML payload to KTA for use
        /// with document uploads to Amazon.
        /// </summary>
        [Constant(IsSensitiveData = true)]
        public static string KtaAmazonSecretKey => GetStringValue(StageConfigDatum.KtaAmazonSecretKey, string.Empty);

        /// <summary>
        /// Gets the interval to poll for results after an upload to KTA.
        /// </summary>
        public static int KtaUploadPollInterval => GetIntValue(StageConfigDatum.KtaUploadPollInterval, 3000);

        /// <summary>
        /// Gets the URL for the KTA API.
        /// </summary>
        public static string KtaApiUrl => GetStringValue(StageConfigDatum.KtaApiUrl, string.Empty);

        /// <summary>
        /// Gets the URL for the KTA Document Capture Review portal.
        /// </summary>
        public static string KtaDocumentCaptureReviewPortalUrl => GetStringValue(StageConfigDatum.KtaDocumentCaptureReviewPortalUrl, string.Empty);

        /// <summary>
        /// Gets the URL for the KTA Document Capture Review portal for JMAC. This URL automates login
        /// and passes the user directly to the KTA dashboard.
        /// </summary>
        /// <remarks>This should ONLY be used for JMAC (PML0229).</remarks>
        public static string KtaDocumentCaptureReviewPassthroughUrlForJmac => GetStringValue(StageConfigDatum.KtaDocumentCaptureReviewPassthroughUrlForJmac, string.Empty);

        /// <summary>
        /// Gets the URL for the KTA upload service.
        /// </summary>
        public static string KtaUploadServiceUrl => GetStringValue(StageConfigDatum.KtaUploadServiceUrl, string.Empty);

        /// <summary>
        /// Gets the ID for the KTA document capture vendor.
        /// </summary>
        public static Guid? KtaVendorId
        {
            get
            {
                var vendorStr = GetStringValue(StageConfigDatum.KtaVendorId, string.Empty);

                Guid ret;
                if (!string.IsNullOrEmpty(vendorStr) && Guid.TryParse(vendorStr, out ret))
                {
                    return ret;
                }

                return null;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the cache datalayer will use the pre-expanded list
        /// of affected cache fields from the select statement provider as the list of invalidated
        /// cache fields instead of expanding the same input list.
        /// </summary>
        public static bool UseAffectedCacheFieldListForInvalidatedCacheFields => GetIntValue(StageConfigDatum.UseAffectedCacheFieldListForInvalidatedCacheFields, 0) == 1;

        /// <summary>
        /// Gets the license key for ExpertPDF HTML-2-PDF api.
        /// </summary>
        public static string PdfConverterLicenseKey => GetStringValue(StageConfigDatum.PdfConverterLicenseKey, string.Empty);

        /// <summary>
        /// Gets the cache duration in minutes for encryption keys.
        /// </summary>
        public static int EncryptionKeyCacheDurationInMinutes => GetIntValue(StageConfigDatum.EncryptionKeyCacheDurationInMinutes, 30);

        /// <summary>
        /// Gets a value indicating whether retrieving <see cref="CPageBase.sTotalRenovationCosts"/> 
        /// always uses the database value instead of the calculation to avoid potential loops.
        /// </summary>
        /// <remarks>
        /// This kill switch is designed to prevent situations that naturally arise due to the
        /// loan amount for renovation loans depending on the total renovation costs, which depend
        /// on the loan amount. These calculations should have all been updated, but just in case,
        /// we'll use this to completely disable the calculation if needed. This bit can be removed
        /// after the changes have been vetted for the 2/9/2018 release.
        /// </remarks>
        public static bool ForceDatabaseValueForTotalRenovationCosts => GetIntValue(StageConfigDatum.ForceDatabaseValueForTotalRenovationCosts, 0) == 1;

        /// <summary>
        /// Enables setting the <code>m_loanfileFieldValidator</code> field for <see cref="CPageBase"/>,
        /// which is used to prevent calls to <seealso cref="CPageBase.CalculatePmlLoans()"/> when running validation
        /// that alter loan field values.
        /// </summary>
        /// <remarks>
        /// See OPM 130113 for additional details.
        /// </remarks>
        public static bool EnableSettingLoanFileFieldValidatorOnInitLoad => GetIntValue(StageConfigDatum.EnableSettingLoanFileFieldValidatorOnInitLoad, 1) == 1;

        /// <summary>
        /// Gets a value indicating whether the contains check for spec sets
        /// will use the internal ID instead of looping through the entire set.
        /// </summary>
        public static bool UseSpecSetContainsOptimization => GetIntValue(StageConfigDatum.UseSpecSetContainsOptimization, 0) == 1;

        /// <summary>
        /// Gets a value indicating whether a pre-defined list of properties
        /// with <see cref="ObsoleteAttribute"/> applied is used for <see cref="CFieldInfoTable"/>
        /// initialization.
        /// </summary>
        public static bool UsePredefinedObsoleteListForFieldInfoInit => GetIntValue(StageConfigDatum.UsePredefinedObsoleteListForFieldInfoInit, 0) == 1;

        /// <summary>
        /// Disables selection of the initial page for the loan editor and
        /// expanded lead editor via an employee's favorites.
        /// </summary>
        /// <remarks>
        /// This bit can be removed after the February 2018 release once the
        /// changes have been verified.
        /// </remarks>
        public static bool DisableInitialPageSelectionFromFavorites => GetIntValue(StageConfigDatum.DisableInitialPageSelectionFromFavorites, 0) == 1;

        /// <summary>
        /// Enables the Loan Closing Advisor integration once we have been approved for integration to Freddie Mac.
        /// </summary>
        /// <remarks>
        /// Once we are approved on all environments, this bit can be removed.
        /// </remarks>
        public static bool EnableLoanClosingAdvisor => GetIntValue(StageConfigDatum.EnableLoanClosingAdvisor, 0) == 1;

        /// <summary>
        /// Gets a list of ip addresses of our vulnerability scan. These vulnerability scan normally generate critical email. Since it is expect behavior we want a way to skip
        /// sending error email if it from this ip list. Just seperate them with a '|' character.
        /// Empty values are ignored.
        /// </summary>
        public static IEnumerable<string> VulnerabilityScanWhitelistedIps
        {
            get
            {
                string fullIpString;
                fullIpString = GetStringValue(StageConfigDatum.VulnerabilityScanWhitelistedIps, string.Empty);

                return fullIpString.Split('|').Where((ip) => !string.IsNullOrEmpty(ip));
            }
        }

        /// <summary>
        /// Gets the hashed value for the KTA document capture partner key.
        /// </summary>
        public static string KtaPartnerKey_Hash => GetStringValue(StageConfigDatum.KtaPartnerKey_Hash, string.Empty);

        /// <summary>
        /// Gets the salt for the hashed KTA document capture partner key.
        /// </summary>
        public static string KtaPartnerKey_Salt => GetStringValue(StageConfigDatum.KtaPartnerKey_Salt, string.Empty);

        /// <summary>
        /// Gets a value indicating whether e-signed docs are allowed in the batch editor.
        /// </summary>
        /// <remarks>
        /// OPM 463549. This bit can be removed once the hotfix has been rolled out and verified
        /// by clients.
        /// </remarks>
        public static bool AllowESignedDocsInBatchEditor => GetIntValue(StageConfigDatum.AllowESignedDocsInBatchEditor, 1) == 1;

        /// <summary>
        /// Gets a value indicating whether we will perform encoding on some user control property by default.
        /// After a year (added 10/2/2017) we should remove this property and default to true.
        /// </summary>
        public static bool EnableAntiXssOnUIControls => GetIntValue(StageConfigDatum.EnableAntiXssOnUIControls, 0) == 1;

        /// <summary>
        /// The seamless LPA url to hit.
        /// </summary>
        public static string SeamlessLpaUrl => GetStringValue(StageConfigDatum.SeamlessLpaUrl, "https://lp.fmrei.com/lpa-s2s-cte/LPSTSServlet");

        /// <summary>
        /// The retry limit for polling for seamless LPA results.
        /// </summary>
        public static int SeamlessLpaPollingRetryLimit => GetIntValue(StageConfigDatum.SeamlessLpaPollingRetryLimit, 10);
        
        /// <summary>
        /// Enables customized TPO portal color themes.
        /// </summary>
        /// <remarks>
        /// If disabled, only the LQB default color theme and its precompiled CSS will be used.
        /// </remarks>
        public static bool AllowCustomizedTpoThemes => GetIntValue(StageConfigDatum.AllowCustomizedTpoThemes, 1) == 1;

        /// <summary>
        /// Gets the duration to cache SASS content.
        /// </summary>
        /// <value>
        /// The duration to cache SASS content.
        /// </value>
        public static TimeSpan SassCacheDuration
        {
            get
            {
                var duration = GetIntValue(StageConfigDatum.SassCacheDuration, 5);
                return TimeSpan.FromMinutes(Math.Max(1, duration));
            }
        }

        /// <summary>
        /// Secret key in order to invoke Pricing.asmx::GetCurrentReleaseInfo web service.
        /// </summary>
        [Constant(IsSensitiveData = true)]
        public static string SecretKeyForLpeReleaseInfo => GetStringValue(StageConfigDatum.SecretKeyForLpeReleaseInfo, string.Empty);

        /// <summary>
        /// Gets the URL for sending a POST request to Fannie Mae's UCD production environment.
        /// </summary>
        public static string FannieMaeUcdProductionPostUrl => GetStringValue(StageConfigDatum.FannieMaeUcdProductionPostUrl, "https://businessgateway.efanniemae.com/service");

        /// <summary>
        /// Gets the URL for sending a POST request to Fannie Mae's UCD test environment.
        /// </summary>
        public static string FannieMaeUcdTestPostUrl => GetStringValue(StageConfigDatum.FannieMaeUcdTestPostUrl, "https://businessgateway-clve.efanniemae.com/service");

        /// <summary>
        /// Gets the URL for sending a GET request to Fannie Mae's UCD production environment using casefileid.
        /// </summary>
        public static string FannieMaeUcdProductionCasefileIdGetUrlFormatString => GetStringValue(StageConfigDatum.FannieMaeUcdProductionCasefileIdGetUrlFormatString, "https://businessgateway.efanniemae.com/service?tz=EDT&CaseFileId={0}&QueryType=7");

        /// <summary>
        /// Gets the URL for sending a GET request to Fannie Mae's UCD test environment using a casefile id.
        /// </summary>
        public static string FannieMaeUcdTestCasefileIdGetUrlFormatString => GetStringValue(StageConfigDatum.FannieMaeUcdTestCasefileIdGetUrlFormatString, "https://businessgateway-clve.efanniemae.com/service?tz=EDT&CaseFileId={0}&QueryType=7");

        /// <summary>
        /// Doc Magic BETA customer id.
        /// </summary>
        public static string DocMagicServerBetaCustomerId => GetStringValue(StageConfigDatum.DocMagicServerBetaCustomerId, "231856");

        /// <summary>
        /// Doc Magic BETA username.
        /// </summary>
        public static string DocMagicServerBetaUsername => GetStringValue(StageConfigDatum.DocMagicServerBetaUsername, "LQBDocMagicProduction@gmail.com");

        /// <summary>
        /// Doc Magic Production URL.
        /// </summary>
        public static string DocMagicProductionServerUrl => GetStringValue(StageConfigDatum.DocMagicProductionServerUrl, "https://www.docmagic.com/webservices/dmdirect/xml");

        /// <summary>
        /// Doc Magic BETA URL.
        /// </summary>
        public static string DocMagicBetaServerUrl => GetStringValue(StageConfigDatum.DocMagicBetaServerUrl, "https://stage-www.docmagic.com/webservices/dmdirect/xml");

        /// <summary>
        /// Doc Magic DocType URL for MISMO Closing V2.
        /// </summary>
        public static string DocMagicMismoClosingV2DocTypeUrl => GetStringValue(StageConfigDatum.DocMagicMismoClosingV2DocTypeUrl, "https://www.docmagic.com/webservices/ws/dtd/DSIDocRequestMISMOClosingV2.dtd");

        /// <summary>
        /// Doc Magic BETA DocType URL for UCD.
        /// </summary>
        public static string DocMagicUcdBetaDocTypeUrl => GetStringValue(StageConfigDatum.DocMagicUcdBetaDocTypeUrl, "http://stage-www.docmagic.com/webservices/dtd/DSIDocumentServerRequest.dtd");

        /// <summary>
        /// Doc Magic Production DocType URL for UCD.
        /// </summary>
        public static string DocMagicUcdProductionDocTypeUrl => GetStringValue(StageConfigDatum.DocMagicUcdProductionDocTypeUrl, "http://www.docmagic.com/webservices/dtd/DSIDocumentServerRequest.dtd");

        /// <summary>
        /// The folder that cache Lpe data.
        /// </summary>
        public static string LpeDataCachePath => GetStringValue(StageConfigDatum.LpeDataCachePath, @"C:\LqbLpeCache");

        /// <summary>
        /// Get the S3 bucket contains the LPE data (rate options, price policy, snapshot). Only relevant for instance on AWS.
        /// </summary>
        public static string AwsS3BucketForLpeData => GetStringValue(StageConfigDatum.AwsS3BucketForLpeData, "ml-lqb-product-pricing");

        /// <summary>
        /// Whether or not to use Lpe database for pricing. Only on AWS where this value should be false. dd 7/21/2017.
        /// </summary>
        public static bool UseLpeDatabaseForPricing => GetIntValue(StageConfigDatum.UseLpeDatabaseForPricing, 1) == 1;

        /// <summary>
        /// Enables logging for LendingQBExecutingEngine.CanPerform.
        /// </summary>
        public static bool LogLoanValueEvaluatorGets => GetIntValue(StageConfigDatum.LogLoanValueEvaluatorGets, 0) == 1;

        /// <summary>
        /// The email to send SMF error logs to.
        /// </summary>
        public static string EmailForSMFErrorLogs => GetStringValue(StageConfigDatum.EmailForSMFErrorLogs, "LOsupport@meridianlink.com");

        public static bool EnableTaskNotificationPermissionCaching => GetIntValue(StageConfigDatum.EnableTaskNotificationPermissionCaching, 0) == 1;

        public static bool EnableGdmsCollateralUnderwriterData => GetIntValue(StageConfigDatum.EnableGdmsCollateralUnderwriterData, 1) == 1; // remove after successful 2017-12 release

        /// <summary>
        /// Enables management of third party client certificates instead of using hardcoded paths and certs.
        /// </summary>
        public static bool EnableThirdPartyCertificateManager => GetIntValue(StageConfigDatum.EnableThirdPartyCertificateManager, 0) == 1;

        public static bool EnableTaskNotificationEmailCaching => GetIntValue(StageConfigDatum.EnableTaskNotificationEmailCaching, 0) == 1;

        /// <summary>
        /// The url that hosts the LQB Service Monitor Framework API.
        /// </summary>
        public static string ServiceMonitorFrameworkApiUrl => GetStringValue(StageConfigDatum.ServiceMonitorFrameworkApiUrl, string.Empty);

        /// <summary>
        /// The url that host the kayako instance api. 
        /// </summary>
        public static string SupportSiteApiUrl => GetStringValue(StageConfigDatum.SupportSiteApiUrl);

        /// <summary>
        /// Check the email address on kayako.
        /// </summary>
        public static bool SupportSiteIsEmailVerificationEnabled => GetIntValue(StageConfigDatum.SupportSiteIsEmailVerificationEnabled, 0) == 1;

        /// <summary>
        /// The api key to query for tickets.
        /// </summary>
        public static string SupportSiteApiKey => GetStringValue(StageConfigDatum.SupportSiteApiKey);

        /// <summary>
        /// The secret key used to authenticate request to kayako.
        /// </summary>
        public static string SupportSiteApiSecretKey => GetStringValue(StageConfigDatum.SupportSiteApiSecretKey);

        /// <summary>
        /// If the value is set the login page will set a support cookie with the given data. 
        /// Null => Do not send a cookie, 1 = secure.lendingqb.com 2 = w-secure.lendingqb.com.
        /// </summary>
        public static string SupportRedirectionCookie => GetStringValue(StageConfigDatum.SupportRedirectionCookie, null);

        /// <summary>
        /// Pages on these URLs will be able to post to PML/ExternalMethods.aspx from the user's browser with our cookie
        /// through Cross-Origin Resource Sharing.
        /// </summary>
        public static List<Uri> CrossOriginUrls => GetStringValue(StageConfigDatum.CrossOriginUrls, string.Empty)
            .Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries)
            .Select(url => new Uri(url))
            .ToList();

        /// <summary>
        /// The encryption key id used to encrypt auth tickets.
        /// </summary>
        public static EncryptionKeyIdentifier WebserviceAuthTicketEncryptionkeyId
        {
            get
            {
                string key = GetStringValue(StageConfigDatum.WebserviceAuthTicketEncryptionkeyId, null);
                EncryptionKeyIdentifier? id; 
                if (!string.IsNullOrEmpty(key) && (id = EncryptionKeyIdentifier.Create(key)).HasValue)
                {
                    return id.Value;
                }

                return EncryptionKeyIdentifier.Default;
            }
        }

        /// <summary>
        /// Timeout for VOX requests.
        /// </summary>
        public static int VOXTimeoutInMilliseconds
        {
            get
            {
                return GetIntValue(StageConfigDatum.VOXTimeoutInMilliseconds, 0);
            }
        }

        /// <summary>
        /// This value determines how long the browser should wait for displaying the mask 
        /// overlay when the user is waiting for an ajax call to execute.
        /// </summary>
        public static int AsyncOverlayTimeBuffer
        {
            get
            {
                return GetIntValue(StageConfigDatum.AsyncOverlayTimeBuffer, -1);
            }
        }

        /// <summary>
        /// Determines whether the environment should add the compatibility meta tag
        /// with the value "Edge" in all pages.
        /// </summary>
        public static bool IsForceEdgeEverywhere
        {
            get
            {
                return GetIntValue(StageConfigDatum.IsForceEdgeEverywhere, 0) == 1;
            }
        }

        /// <summary>
        /// Enables the use of <see cref="System.Collections.Generic.SortedSet{T}"/> for select statement lists, 
        /// which sorts added values in O(log n) time.
        /// </summary>
        public static bool UseSortedSetForSelectStatementProvider => GetIntValue(StageConfigDatum.UseSortedSetForSelectStatementProvider, 0) == 1;

        /// <summary>
        /// Enables using the <see cref="CAgentFields.Delete(Security.AbstractUserPrincipal)"/> instance method
        /// for agent deletions.
        /// </summary>
        /// <remarks>
        /// Constructing a new instance of <see cref="CAgentFields"/> will call <seealso cref="DataSet.AcceptChanges"/>,
        /// preventing an error where a row in the "detached" state is deleted and forced to accept changes when it was not
        /// a part of the DataSet by adding that row to the dataset if necessary. This error can occur when a row is created 
        /// from the dataset but not added, such as when deserializing agent data from XML.
        /// </remarks>
        public static bool UseAgentFieldsMethodForAgentDeletion => GetIntValue(StageConfigDatum.UseAgentFieldsMethodForAgentDeletion, 1) == 1;

        /// <summary>
        /// Enables the legacy qual rate calculation that uses the qual rate directly
        /// from the rate option or loan program template.
        /// </summary>
        /// <remarks>
        /// This bit can be removed once case 29138 has been vetted.
        /// </remarks>
        public static bool EnableLegacyQualRateCalculation => GetIntValue(StageConfigDatum.EnableLegacyQualRateCalculation, 0) == 1;

        /// <summary>
        /// Enables logging for setting certain tax IDs.
        /// </summary>
        /// <remarks>
        /// See OPM case 456490.
        /// </remarks>
        public static bool EnableTaxIdLogging
        {
            get { return GetIntValue(StageConfigDatum.EnableTaxIdLogging, 1) == 1; }
        }

        /// <summary>
        /// Temporary option on whether to disable to CleanUpMemory call. Default value is false.
        /// </summary>
        public static bool OPM_456501_DisableCleanupMemory
        {
            get { return GetIntValue(StageConfigDatum.OPM_456501_DisableCleanupMemory, 0) == 1; }
        }

        /// <summary>
        /// 4/28/2017 - Whether to enable the fix for case 456337.  This config should be remove in June 2017 once we verify enable this fix does not
        /// cause issue.
        /// </summary>
        public static bool EnableOPM_456337_Fix_CustomReportList
        {
            get { return GetIntValue(StageConfigDatum.EnableOPM_456337_Fix_CustomReportList, 0) == 1; }
        }
        /// <summary>
        /// Gets a value for the maximum number of threads to run in parallel
        /// for the nightly task runner processor.
        /// </summary>
        public static int MaxNightlyTaskThreads
        {
            get
            {
                return Math.Max(1, GetIntValue(StageConfigDatum.MaxNightlyTaskThreads, Environment.ProcessorCount * 2));
            }
        }

        public static bool EnableGroupLogging
        {
            get
            {
                return GetIntValue(StageConfigDatum.EnableGroupLogging, 0) == 1;
            }
        }

        /// <summary>
        /// Use Batch Workflow checker instead of checking each loan one at a time. 
        /// </summary>
        public static bool UseBatchWorkflowReadChecker => GetIntValue(StageConfigDatum.UseBatchWorkflowReadChecker, 0) == 1;

        /// <summary>
        /// Gets a value indicating whether <seealso cref="CFieldInfoTable"/>
        /// initialization will attempt to use cached data.
        /// </summary>
        /// <value>
        /// True if the initialization will use cached data, false otherwise.
        /// </value>
        public static bool EnableFieldInfoCache
        {
            get { return GetIntValue(StageConfigDatum.EnableFieldInfoCache, 0) == 1; }
        }

        /// <summary>
        /// Tells the allowed bytes per file in the LoTemp directory for 'B' users.
        /// </summary>
        public static int AllowedBytesPerFileInTempForSpecialUsers
        {
            get
            {
                var defaultValue = 10 * 1000 * 1000;
                return GetIntValue(StageConfigDatum.AllowedBytesPerFileInTempForSpecialUser, defaultValue);
            }
        }

        /// <summary>
        /// Tells the bytes allowed to be read/written per soap request.
        /// </summary>
        public static int AllowedBytesPerReadWriteOverSoap
        {
            get
            {
                var defaultValue = 10 * 1000;
                return GetIntValue(StageConfigDatum.AllowedBytesPerReadWriteOverSoap, defaultValue);
            }
        }

        /// <summary>
        /// Tells how long to allow a user to have access to a temporary file created in LoTemp after their last access.
        /// </summary>
        public static int SlidingMinutesToKeepUsersAccessToTemporaryFile
        {
            get
            {
                var defaultValue = 30;
                return GetIntValue(StageConfigDatum.SlidingMinutesToKeepUsersAccessToTemporaryFile, defaultValue);
            }
        }

        /// <summary>
        /// A 'B' user must have one of these login names to be allowed to use the broker replicate service methods.
        /// </summary>
        public static IEnumerable<string> LoginNamesRequiredOfBUsersForBrokerReplicateServiceMethods
        {
            get
            {
                return FromDelimitedList(StageConfigDatum.LoginNamesRequiredOfBUsersForBrokerReplicateServiceMethods);
            }
        }

        /// <summary>
        /// A 'B' user must have one of these ip addresses to be allowed to use the loan replicate service methods.
        /// </summary>
        public static IEnumerable<string> IpsRequiredOfBUsersForBrokerReplicateServiceMethods
        {
            get
            {
                return FromDelimitedList(StageConfigDatum.IpsRequiredOfBUsersForBrokerReplicateServiceMethods);
            }
        }

        /// <summary>
        /// A 'B' user must have one of these login names to be allowed to use the loan replicate service methods.
        /// </summary>
        public static IEnumerable<string> LoginNamesRequiredOfBUsersForLoanReplicateServiceMethods
        {
            get
            {
                return FromDelimitedList(StageConfigDatum.LoginNamesRequiredOfBUsersForLoanReplicateServiceMethods);
            }
        }

        /// <summary>
        /// A 'B' user must have one of these ip addresses to be allowed to use the broker replicate service methods.
        /// </summary>
        public static IEnumerable<string> IpsRequiredOfBUsersForLoanReplicateServiceMethods
        {
            get
            {
                return FromDelimitedList(StageConfigDatum.IpsRequiredOfBUsersForLoanReplicateServiceMethods);
            }
        }

        public static bool ExecuteTaskNightlyInParallel
        {
            get
            {
                return GetIntValue(StageConfigDatum.ExecuteTaskNightlyInParallel, 1) == 1;
            }
        }

        public static int DataVerifyDRIVETimeOutInMilliseconds
        {
            get
            {
                return GetIntValue(StageConfigDatum.DataVerifyDRIVETimeOutInMilliseconds, 180 * 1000);
            }
        }

        /// <summary>
        /// Emergency shut off for bulk loading the rate sheet info in the RateOptionExpirationDataLoader.
        /// </summary>
        public static bool ShouldBulkLoadRateSheetInfoForROExpiration
        {
            get
            {
                return GetIntValue(StageConfigDatum.ShouldBulkLoadRateSheetInfoForROExpiration, 1) == 1;
            }
        }

        /// <summary>
        /// Gets the secret key for the RecordInitialDisclosure webservice.
        /// </summary>
        public static Guid? WebserviceRecordInitialDisclosureSecretKey
        {
            get
            {
                string stringKey = GetStringValue(StageConfigDatum.WebserviceRecordInitialDisclosureSecretKey, string.Empty);

                if (string.IsNullOrEmpty(stringKey))
                {
                    return null;
                }

                Guid key;
                if (!Guid.TryParse(stringKey, out key))
                {
                    return null;
                }

                return key;
            }
        }

        /// <summary>
        /// Gets a value indicating whether we should enforce the CanModifyLoanName user permission in the data layer.
        /// Essentially an emergency shut off valve.
        /// </summary>
        public static bool ShouldEnforceCanModifyLoanName
        {
            get
            {
                return GetIntValue(StageConfigDatum.ShouldEnforceCanModifyLoanName, 1) == 1;
            }
        }

        /// <summary>
        /// Determines whether <see cref="CFieldInfoTable.GatherRequiredIndepFields(IEnumerable{string}, bool)"/>
        /// will gather fields in parallel.
        /// </summary>
        public static bool EnableParallelFieldGathering
        {
            get
            {
                return GetIntValue(StageConfigDatum.EnableParallelFieldGathering, 1) == 1;
            }
        }

        /// <summary>
        /// The URL to hit when attempting to pull the top 10 counseling organizations from CFPB.
        /// </summary>
        public static string CFPBCounselingOrganizationURLFormatString
        {
            get
            {
                return GetStringValue(StageConfigDatum.CFPBCounselingOrganizationURLFormatString, "https://www.consumerfinance.gov/hud-api-replace/{0}/");
            }
        }

        public static bool AllowDisqualifiedRateOptionsInParRateCalc
        {
            get
            {
                return GetIntValue(StageConfigDatum.AllowDisqualifiedRateOptionsInParRateCalc, 0) == 1;
            }
        }

        /// <summary>
        /// Get maximum number of ratesheets to be process during single LpeUpdate run.
        /// </summary>
        public static int MaxNumberOfRatesheetsToProcess
        {
            get
            {
                return GetIntValue(StageConfigDatum.MaxNumberOfRatesheetsToProcess, 150);
            }
        }
        /// <summary>
        /// Gets a list of ip addresses that we can automatically let through when trying to authenticate through webservices.
        /// This can hold complete IP addresses or ranges. Just seperate them with a '|' character.
        /// Empty values are ignored.
        /// </summary>
        public static IEnumerable<string> WebserviceWhitelistedIps
        {
            get
            {
                string fullIpString;
                fullIpString = GetStringValue(StageConfigDatum.WebserviceWhitelistedIps, string.Empty);

                return fullIpString.Split('|').Where((ip) => !string.IsNullOrEmpty(ip));
            }
        }

        /// <summary>
        /// Note that this works in concert with the client ip, as in one needs both to be able to test.
        /// </summary>
        public static IEnumerable<string> RequiredLoginNmToViewLoanTestPageWithoutPermission
        {
            get
            {
                string fullLoginNmString;
                fullLoginNmString = GetStringValue(StageConfigDatum.RequiredLoginNmToViewLoanTestPageWithoutPermission, string.Empty);

                return fullLoginNmString.Split(new[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
            }
        }

        /// <summary>
        /// Note that this works in concert with the login name, as in one needs both to be able to test.
        /// </summary>
        public static IEnumerable<string> RequiredUserIpToViewLoanTestPageWithoutPermission
        {
            get
            {
                string fullIpString;
                fullIpString = GetStringValue(StageConfigDatum.RequiredUserIpToViewLoanTestPageWithoutPermission, string.Empty);

                return fullIpString.Split(new[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
            }
        }

        /// <summary>
        /// Gets the url for TFA requests for the Banana Security API.
        /// </summary>
        public static string BananaSecurityTfaUrl
        {
            get
            {
                return GetStringValue(StageConfigDatum.BananaSecurityTfaUrl, "https://banana.assistdirect.com/v1/services/tfa");
            }
        }

        /// <summary>
        /// Gets the url for SMS requests for the Banana Security API.
        /// In case it ever differs between Dev and Prod.
        /// </summary>
        public static string BananaSecuritySmsUrl
        {
            get
            {
                return GetStringValue(StageConfigDatum.BananaSecuritySmsUrl, string.Empty);
            }
        }

        /// <summary>
        /// Gets the hashed value for the token to use in Banana Security API requests.
        /// </summary>
        public static string BananaSecurityToken
        {
            get
            {
                return GetStringValue(StageConfigDatum.BananaSecurityToken, string.Empty);
            }
        }

        /// <summary>
        /// Gets the source number for SMS messages for Banana Security API.
        /// </summary>
        public static string BananaSecuritySmsSourceNumber
        {
            get
            {
                return GetStringValue(StageConfigDatum.BananaSecuritySmsSourceNumber, string.Empty);
            }
        }

        /// <summary>
        /// Gets the hashed value for the password to use in Banana Security API requests.
        /// </summary>
        public static string BananaSecurityPassword
        {
            get
            {
                return GetStringValue(StageConfigDatum.BananaSecurityPassword, string.Empty);
            }
        }

        /// <summary>
        /// Gets the sample custom report login name.
        /// </summary>
        public static string SampleCustomReportLoginNm
        {
            get
            {
                return GetStringValue(StageConfigDatum.SampleCustomReportLoginNm, string.Empty);
            }
        }

        /// <summary>
        /// Enables deleting filedb liability content for QuickPricer pool files. 
        /// </summary>
        public static bool IsEnabledDeleteQPPoolFileDBContent
        {
            get
            {
                return GetIntValue(StageConfigDatum.DeleteQPPoolFileDBContent, 1) == 1;
            }
        }

        /// <summary>
        /// Gets the number of milliseconds to cache the FileDB get content.
        /// </summary>
        public static int FileDBExpirationInMs
        {
            get
            {
                return GetIntValue(StageConfigDatum.FileDBExpirationInMs, 1000);
            }
        }

        /// <summary>
        /// Gets the connection string for the loan file cache updater queue.
        /// </summary>
        /// <value>The connection string for the loan file cache updater queue.</value>
        public static string LoanFileCacheQueueConnectionString
        {
            get
            {
                return GetStringValue(StageConfigDatum.LoanFileCacheQueueConnectionString, string.Empty);
            }
        }

        /// <summary>
        /// Gets the connection string for the priority loan updater queue.
        /// </summary>
        /// <value>The connection string for the priority loan updater queue.</value>
        public static string PriorityLoanUpdaterQueueConnectionString
        {
            get
            {
                return GetStringValue(StageConfigDatum.PriorityLoanUpdaterQueueConnectionString, string.Empty);
            }
        }

        /// <summary>
        /// Gets a value indicating whether EDocs Polling should load all edocs but only the 4 columns needed.
        /// This should drastically reduce bandiwth usage with sql.
        /// </summary>
        public static bool PollingReturnAllDocs
        {
            get
            {
                return GetIntValue(StageConfigDatum.PollingReturnAllDocs, 1) == 1;
            }
        }

        /// <summary>
        /// Gets the Amazon edocs url.
        /// </summary>
        public static string AmazonEdocsClientUrl
        {
            get
            {
                const string defaultValue = "http://edocs.test.lendingqb.com";

                return GetStringValue(StageConfigDatum.AmazonEdocsClientUrl, defaultValue);
            }
        }

        /// <summary>
        /// Gets the Amazon API url that LendingQB server submit request to.
        /// </summary>
        public static string AmazonEdocsApiUrl
        {
            get
            {
                const string defaultValue = "https://edocs.test.lendingqb.com/lendingqbapi.aspx";

                return GetStringValue(StageConfigDatum.AmazonEdocsApiUrl, defaultValue);
            }
        }

        /// <summary>
        /// Gets the secret key that require to authenticate with Amazon API.
        /// </summary>
        public static string AmazonApiSecretKey
        {
            get
            {
                return GetStringValue(StageConfigDatum.AmazonApiSecretKey, string.Empty);
            }
        }

        /// <summary>
        /// Gets the number of times our server will check for results. Theres half a second interval 
        /// before each attempt so 240 would be 2 minutes.
        /// </summary>
        public static int NumberOfHalfSecondPricingChecks
        {
            get
            {
                const int defaultValue = 240;

                return GetIntValue(StageConfigDatum.NumberOfHalfSecondPricingChecks, defaultValue);
            }
        }

        /// <summary>
        /// The upload queue for ocr uploads to vendor. 
        /// </summary>
        public static string MSMQ_OCR_Upload
        {
            get
            {
                return GetStringValue(StageConfigDatum.MSMQ_OCR_Upload, string.Empty);
            }
        }

        /// <summary>
        /// Whether to turn on the intermediate field caching in CCalcCacheData. The default is true.
        /// </summary>
        public static bool EnableCachingInCalcCacheData
        {
            get
            {
                return GetIntValue(StageConfigDatum.EnableCachingInCalcCacheData, 1) == 1;
            }
        }

        /// We now load trigger field dependencies only on init save. The following stage constant reverts said behavior. 
        /// <value>True if the system should load all the trigger dependencies all the time, false if it should only do so on init save.</value>
        /// </summary>
        public static bool LoadTriggerFieldDependenciesAllTheTime
        {
            get
            {
                return GetIntValue(StageConfigDatum.LoadTriggerFieldDependenciesAllTheTime, 1) == 1;
            }
        }

        /// <summary>
        /// Writes all doc vendor request to the temp files of the stash server.
        /// </summary>
        public static bool EnableDocVendorRequestSaving
        {
            get
            {
                return GetIntValue(StageConfigDatum.EnableDocVendorRequestSaving, 0) == 1;
            }
        }

        /// <summary>
        /// Gets a value indicating whether to use the EDocs site for the document
        /// generation process.
        /// </summary>
        /// <remarks>
        /// Added for Case 238263. Hopefully, this is a temporary bit, but we're
        /// adding it as a way to turn off this behavior.
        /// </remarks>
        public static bool UseEDocsSiteForDocumentGeneration
        {
            get
            {
                return GetIntValue(StageConfigDatum.UseEDocsSiteForDocumentGeneration, 1) == 1;
            }
        }

        /// <summary>
        /// 10/22/2015 - dd - Since we just introduce some caching to EnableGetGfeToleranceInfoCache, we want to have a way to turn off the feature incase it has bug.
        /// </summary>
        public static bool EnableGetGfeToleranceInfoCache
        {
            get
            {
                return GetIntValue(StageConfigDatum.EnableGetGfeToleranceInfoCache, 1) == 1;
            }
        }

        /// <summary>
        /// 8/12/2015 - dd - Since we just introduce some caching to PricingEngineData.cs, we want to have a way to turn off the feature incase it has bug.
        /// </summary>
        public static bool EnablePricingDataFieldCache
        {
            get
            {
                return GetIntValue(StageConfigDatum.EnablePricingDataFieldCache, 1) == 1;
            }
        }

        public static bool UsePDFRasterizerServiceForPDFConversion
        {
            get
            {
                return GetIntValue(StageConfigDatum.UsePDFRasterizerServiceForPDFConversion, 0) == 1;
            }
        }

        /// <summary>
        /// Gets the path for the batch file that needs to be executed to restart the pdf conversion process
        /// </summary>
        public static string RestartPdfConversionProcessBatchFilePath
        {
            get
            {
                return GetStringValue(StageConfigDatum.RestartPdfConversionProcessBatchFilePath, string.Empty);
            }
        }

        /// <summary>
        /// Gets a value indicating whether the PerformanceStopwatch class should add lines to PerformanceMonitorItem.
        /// </summary>
        public static bool EnablePerformanceStopwatchLogging
        {
            get
            {
                return GetIntValue(StageConfigDatum.EnablePerformanceStopwatchLogging, 1) == 0;
            }
        }

        /// <summary>
        /// Gets the support system url used to authenticate LQB Users.
        /// </summary>
        public static string SupportSystemAuthUrlFormat
        {
            get
            {
                return GetStringValue(StageConfigDatum.SupportSystemAuthUrlFormat, string.Empty);
            }
        }

        /// <summary>
        /// Gets the number of seconds it takes to convert 1 page.
        /// </summary>
        public static int EDocs_ConversionSecondsPerPage
        {
            get
            {
                const int defaultValue = 5;

                return GetIntValue(StageConfigDatum.EDocs_ConversionSecondsPerPage, defaultValue);
            }
        }

        /// <summary>
        /// Gets the fix number of seconds to convert one edoc.
        /// </summary>
        public static int EDocs_ConversionFixedAmount
        {
            get
            {
                const int defaultValue = 5;

                return GetIntValue(StageConfigDatum.EDocs_ConversionFixedAmount, defaultValue);
            }
        }

        /// <summary>
        /// Enables sending to the BCC Inbox for support.
        /// </summary>
        public static bool IsEnableBCCToSystemSentBox
        {
            get
            {
                return GetIntValue(StageConfigDatum.IsEnableBCCToSystemSentBox, 0) == 1;
            }
        }

        /// <summary>
        /// Gets the incoming task inbox.
        /// </summary>
        public static string TaskEmailAddress
        {
            get
            {
                return GetStringValue(StageConfigDatum.TaskEmailAddress, string.Empty);
            }
        }

        /// <summary>
        /// Gets the password of the incoming task inbox.
        /// </summary>
        public static string TaskEmailPassword
        {
            get
            {
                return GetStringValue(StageConfigDatum.TaskEmailPassword, string.Empty);
            }
        }

        public static string EmailReceivingServer
        {
            get
            {
                const string defaultValue = "mailer1.mortgagecreditlink.com";

                return GetStringValue(StageConfigDatum.EmailReceivingServer, defaultValue);
            }
        }

        /// <summary>
        /// Gets the pop3 port to use to send emails.
        /// </summary>
        public static int EmailReceivingServerPort
        {
            get
            {
                return GetIntValue(StageConfigDatum.EmailReceivingServerPort, 110);
            }
        }

        /// <summary>
        /// Gets the ssl setting for pop3.
        /// </summary>
        public static bool EmailReceivingServerIsSsl
        {
            get
            {
                return GetIntValue(StageConfigDatum.EmailReceivingServerIsSsl, 0) == 1;
            }
        }

        /// <summary>
        /// Gets the number of seconds that we should cache the BrokerUserPrincipal object.
        /// Set 0 seconds mean no cache.
        /// </summary>
        public static int BrokerUserPrincipalCacheInSeconds
        {
            get
            {
                const int defaultValue = 2;

                return GetIntValue(StageConfigDatum.BrokerUserPrincipalCacheInSeconds, defaultValue);
            }
        }
        /// <summary>
        /// A configuration that allow us to fall back to old behavior of BrokerDB.RetrieveById.
        /// To turn back old behavior, need to add UsingBrokerDBCacheV2 and set that to 1.
        /// </summary>
        public static int UsingBrokerDBCacheV2
        {
            get
            {
                const int defaultValue = 3;

                // 6/30/2015 dd - A config value to determine which version of BrokerDB.RetrieveById we use.
                // Version 1 - The original version where broker info is only cache per request.
                // Version 2 - We cache the broker info, so we do not need to retrieve full broker info from db. However there
                //      is a bug that it could cause it to hold a lock on ALL requests.
                // Version 3 - 6/3/2015 - Give each broker it own lock. So one broker retrieve will not hold a lock on other broker.
                return GetIntValue(StageConfigDatum.UsingBrokerDBCacheV2, defaultValue);
            }
        }

        /// <summary>
        /// Gets a value indicating whether the select query should force order when joining
        /// the loan file tables. This may help deal with deadlocks due to random orders.
        /// </summary>
        public static bool IsLoanForceOrderHintEnabled
        {
            get
            {
                return GetIntValue(StageConfigDatum.IsLoanForceOrderHintEnabled, 1) == 1;
            }
        }

        public static HashSet<string> AcceptedNonLoadminSmoketesters
        {
            get
            {
                string users;
                users = GetStringValue(StageConfigDatum.AcceptedNonLoadminSmoketesters, string.Empty);

                if (string.IsNullOrEmpty(users))
                {
                    return new HashSet<string>();
                }

                return new HashSet<string>(users.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries), StringComparer.OrdinalIgnoreCase);
            }
        }

        public static IList<Tuple<string, string, string>> ThirdPartyReferrers
        {
            get
            {
                string data;
                data = GetStringValue(StageConfigDatum.ThirdPartyReferrers, string.Empty);

                if (string.IsNullOrEmpty(data))
                {
                    return new List<Tuple<string, string, string>>();
                }

                string[] sites = data.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
                List<Tuple<string, string, string>> referrals = new List<Tuple<string, string, string>>();
                foreach (string site in sites)
                {
                    string[] parts = site.Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries);
                    if (parts.Length < 2)
                    {
                        continue;
                    }
                    else if (parts.Length == 2)
                    {
                        referrals.Add(Tuple.Create(parts[0], parts[1], string.Empty));
                    }
                    else
                    {
                        referrals.Add(Tuple.Create(parts[0], parts[1], parts[2]));
                    }
                }

                return referrals.AsReadOnly();
            }
        }

        public static string ThirdPartyEncryptionKey
        {
            get
            {
                return GetStringValue(StageConfigDatum.ThirdPartyEncryptionKey);
            }
        }

        public static string ThirdPartyEncryptionIV
        {
            get
            {
                return GetStringValue(StageConfigDatum.ThirdPartyEncryptionIV);
            }
        }

        public static string NonContextBaseUrl
        {
            get
            {
                return GetStringValue(StageConfigDatum.NonContextBaseUrl, string.Empty);
            }
        }

        public static string DUTestUserName
        {
            get
            {
                return GetStringValue(StageConfigDatum.DUTestUserName, string.Empty);
            }
        }

        public static string DUTestPassword
        {
            get
            {
                return GetStringValue(StageConfigDatum.DUTestPassword, string.Empty);
            }
        }

        /// <summary>
        /// Gets the number of pages required for an edoc to be sent to the Batch queue. This queue is slower because most of the docs will be bigger
        /// or come from integrations.
        /// </summary>
        public static int BatchQueuePageCountCutOff
        {
            get
            {
                const int defaultValue = 10;

                return GetIntValue(StageConfigDatum.BatchQueuePageCountCutOff, defaultValue);
            }
        }

        /// <summary>
        /// Gets the default base url for integration web hooks, where the third party needs to push data back to a custom endpoint on the LQB servers.
        /// </summary>
        public static string IntegrationWebhookDefaultBaseUrl => GetStringValue(StageConfigDatum.IntegrationWebhookDefaultBaseUrl, string.Empty);

        /// <summary>
        /// When host on 3rd party server (i.e CMG), the postback url should be use by 3rd party. Default for LendingQB is https://secure.lendingqb.com/4506TPost.aspx.
        /// </summary>
        public static string Irs4056TPostBackUrl
        {
            get
            {
                return GetStringValue(StageConfigDatum.Irs4056TPostBackUrl, string.Empty);
            }
        }

        /// <summary>
        /// Gets the postback URL to which the mortgage insurance vendor should post any asynchronous responses to mortgage insurance requests from LendingQB.
        /// </summary>
        public static string MIFrameworkPostBackUrl
        {
            get
            {
                const string defaultValue = "https://secure.lendingqb.com/MIFrameworkPost.aspx";

                return GetStringValue(StageConfigDatum.MIFrameworkPostBackUrl, defaultValue);
            }
        }

        public static string CommunicationFrameworkPostBackUrl => GetStringValue(StageConfigDatum.CommunicationFrameworkPostBackUrl, "https://secure.lendingqb.com/VendorCommunicationPost.aspx");

        public static string AppraisalFrameworkPostBackUrl => GetStringValue(StageConfigDatum.AppraisalFrameworkPostBackUrl, "https://secure.lendingqb.com/AppraisalStatusUpdate.aspx");

        public static string GenericFrameworkWebServiceDomain => GetStringValue(StageConfigDatum.GenericFrameworkWebServiceDomain, "https://webservices.lendingqb.com");

        public static string AllowedIpForLOAdmin
        {
            get
            {
                return GetStringValue(StageConfigDatum.AllowedIpForLOAdmin, string.Empty);
            }
        }

        public static string HostingServer
        {
            // 3/19/2014 dd - Have a quick way to determine if this is CMG hosting or our LendingQB hosting.
            get
            {
                const string defaultValue = "LendingQB";

                return GetStringValue(StageConfigDatum.HostingServer, defaultValue);
            }
        }
        public static string DefaultDoNotReplyAddress
        {
            // 6/25/2014 dd - Allow CMG hosting to use different DoNotReply.
            get
            {
                const string defaultValue = "Do_Not_Reply@lendingqb.com";

                return GetStringValue(StageConfigDatum.DefaultDoNotReplyAddress, defaultValue);
            }
        }
        public static string TaskAliasEmailAddress
        {
            // 6/25/2014 dd - Allow CMG hosting to use different task alias email
            get
            {
                const string defaultValue = "tasksystem@task.lendingqb.com";

                return GetStringValue(StageConfigDatum.TaskAliasEmailAddress, defaultValue);
            }
        }
        public static string DefaultErrorEmailFromAddress
        {
            // 6/25/2014 dd - Allow CMG hosting to use different email.
            get
            {
                const string defaultValue = "mclerror@meridianlink.com";

                return GetStringValue(StageConfigDatum.DefaultErrorEmailFromAddress, defaultValue);
            }
        }

        public static string ComplianceEaseProductionUrl
        {
            get
            {
                return GetStringValue(StageConfigDatum.ComplianceEaseProductionUrl, ConstAppDavid.ComplianceEase_ProductionUrl);
            }
        }

        public static string ComplianceEaseBetaUrl
        {
            get
            {
                return GetStringValue(StageConfigDatum.ComplianceEaseBetaUrl, ConstAppDavid.ComplianceEase_BetaUrl);
            }
        }
        public static string ComplianceEaseTestUserAccount
        {
            get
            {
                const string defaultValue = "systemuat@lendingqb.com";

                // 10/3/2014 dd - OPM 193063 - Allow CMG to specify their own ComplianceEase Test Account.
                // Note: Is this variable always same as ComplianceEasePTMTestLogin?
                return GetStringValue(StageConfigDatum.ComplianceEaseTestUserAccount, defaultValue);
            }
        }
        /// <summary>
        /// Gets the CE early exit events that should be logged to PB. Early exit events are those where an audit request is not made to CE.
        /// </summary>
        public static IEnumerable<string> ComplianceEaseEarlyExitEventsToLog
        {
            get
            {
                string events;
                events = GetStringValue(StageConfigDatum.ComplianceEaseEarlyExitEventsToLog, string.Empty);

                if (string.IsNullOrEmpty(events))
                {
                    return new string[] { };
                }

                return events.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            }
        }

        public static bool IsLendingQBHostingServer
        {
            get { return HostingServer == "LendingQB"; }
        }
        public static IEnumerable<string> AntiVirusScannerIPs
        {
            get
            {
                string ips;
                ips = GetStringValue(StageConfigDatum.AntiVirusScannerIPs, string.Empty);

                if (string.IsNullOrEmpty(ips))
                {
                    return new string[] { };
                }

                return ips.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            }
        }

        public static IEnumerable<string> AllowableIPsForAuthenticationPartnerWebservice
        {
            get
            {
                // 4/28/2015 dd - OPM 212493 - This contains a list of our internal IP that will be allow to make
                // AuthService.GetBrokerPartnerAuthTicket call.
                string ips;
                ips = GetStringValue(StageConfigDatum.AllowableIPsForAuthenticationPartnerWebservice, string.Empty);

                if (string.IsNullOrEmpty(ips))
                {
                    return new string[] { };
                }

                return ips.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            }
        }

        public static E_PermissionType NewLoanEditorUIPermissionType
        {
            get
            {
                const string defaultValue = "DeferToMoreGranularLevel";

                string type;
                type = GetStringValue(StageConfigDatum.NewLoanEditorUIPermissionType, defaultValue);

                return (E_PermissionType)Enum.Parse(typeof(E_PermissionType), type);
            }
        }


        public static bool IsDisableRequireOcFieldsDatalayer
        {
            get
            {
                return GetIntValue(StageConfigDatum.IsDisableRequireOcFieldsDatalayer, 0) == 1;
            }
        }

        public static string SmtpServer
        {
            get
            {
                const string defaultValue = "smtp.meridianlink.com";

                return GetStringValue(StageConfigDatum.SmtpServer, defaultValue);
            }
        }

        public static int SmtpServerPortNumber
        {
            get
            {
                return GetIntValue(StageConfigDatum.SmtpServerPortNumber, 25);
            }
        }


        public static bool IsEnableEDocVerify
        {
            get
            {
                return GetIntValue(StageConfigDatum.IsEnableEDocVerify, 0) == 1;
            }
        }

        /// <summary>
        /// opm 204808
        /// </summary>
        public static int MaxSqlRecordsForPipeline
        {
            get
            {
                const int defaultValue = 750;

                return GetIntValue(StageConfigDatum.MaxSqlRecordsForPipeline, defaultValue);
            }
        }

        public static bool DoesBrokerHaveExpirationDebugEnabled(Guid brokerId)
        {
            string enabled;
            enabled = GetStringValue(StageConfigDatum.DoesBrokerHaveExpirationDebugEnabled, string.Empty);

            return enabled.Contains(brokerId.ToString());
        }

        public static string ConsumerPortalVersion2BaseUrl
        {
            get
            {
                return GetStringValue(StageConfigDatum.ConsumerPortalVersion2BaseUrl);
            }
        }

        /// <summary>
        /// 6/1/2013 dd -
        /// A secret key that allow consumer portal v2 to utilize webservice for administration
        /// functionalities.
        /// </summary>
        public static string ConsumerPortalPartnerKey
        {
            get
            {
                return GetStringValue(StageConfigDatum.ConsumerPortalPartnerKey);
            }
        }

        /// <summary>
        /// This is used by LoanUpdater class to migrate fields. Do not modify this unless the queue is empty.
        /// Comma seperated list of fields to update.
        /// </summary>
        public static string LoanUpdaterFieldsForMigration
        {
            get
            {
                return GetStringValue(StageConfigDatum.LoanUpdaterFieldsForMigration, string.Empty);
            }
        }

        public static bool AllowQuickPricer2InTPOPortal
        {
            get
            {
                return GetIntValue(StageConfigDatum.AllowQuickPricer2InTPOPortal, 0) == 1;
            }
        }

        public static int QuickPricer2InUseLoanExpirationDays // opm 185732
        {
            get
            {
                const int defaultValue = 2;

                return GetIntValue(StageConfigDatum.QuickPricer2InUseLoanExpirationDays, defaultValue);
            }
        }

        /// <summary>
        /// opm 212045, controls whether or not users have a button to migrate to adjustments mode.
        /// </summary>
        public static bool DisableUserMigrationToAdjustmentsMode
        {
            get
            {
                return GetIntValue(StageConfigDatum.DisableUserMigrationToAdjustmentsMode, 0) == 1;
            }
        }

        /// <summary>
        /// opm 212045, controls whether or not adjustments mode can be on at all.
        /// </summary>
        public static bool DisableAdjustmentsMode
        {
            get
            {
                return GetIntValue(StageConfigDatum.DisableAdjustmentsMode, 0) == 1;
            }
        }

        public static int MinutesToDedupException
        {
            get
            {
                const int defaultValue = 5;

                return GetIntValue(StageConfigDatum.MinutesToDedupException, defaultValue);
            }
        }
        /// <summary>
        /// Allow us to configure a ban client IP that will not send out email.
        /// </summary>
        public static string BanIpsFromEmail
        {
            get
            {
                return GetStringValue(StageConfigDatum.BanIpsFromEmail, string.Empty);
            }
        }

        /// <summary>
        /// Localhost and Dev should not be sending emails to clients. Keep a list of email
        /// addresses that we want the email processor to work with on Localhost and Dev.
        /// </summary>
        public static HashSet<string> LoDevEmailWhitelist
        {
            get
            {
                string raw;
                raw = GetStringValue(StageConfigDatum.LoDevEmailWhitelist, string.Empty);

                string noSpaces = raw.Replace(" ", string.Empty);
                return new HashSet<string>(noSpaces.Split(';'), StringComparer.OrdinalIgnoreCase);
            }
        }

        public static List<List<string>> InvestorNameSynonyms
        {
            get
            {
                string raw;
                raw = GetStringValue(StageConfigDatum.InvestorNameSynonyms, string.Empty);

                var synonymParts = raw.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                var temp = (from string synonymSet in synonymParts select
                            (from string a in synonymSet.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                             select a.TrimWhitespaceAndBOM()).ToList()).ToList();
                return temp;
            }
        }
        public static bool DisablePMLDuplicateSubmissionBlocker
        {
            get
            {
                const string defaultValue = "false";

                string disable;
                disable = GetStringValue(StageConfigDatum.DisablePMLDuplicateSubmissionBlocker, defaultValue);

                return disable.ToLower().TrimWhitespaceAndBOM() == "true";
            }
        }

        public static string InvestorNameSynonymsRaw
        {
            get
            {
                return GetStringValue(StageConfigDatum.InvestorNameSynonymsRaw, string.Empty);
            }
        }

        public static string MsMqLogConnectStr
        {
            get
            {
                // Try to get settings from site.config first.
                string s = ConstSite.MsMqLogConnectStr;

                if (string.IsNullOrEmpty(s) == false)
                {
                    return s;
                }

                return GetStringValue(StageConfigDatum.MsMqLogConnectStr, string.Empty);
            }
        }


        public static string BarcodeReaderUrl
        {
            get
            {
                return GetStringValue(StageConfigDatum.BarcodeReaderUrl);
            }
        }

        public static string ConsumerPortalBaseUrl
        {
            get
            {
                return GetStringValue(StageConfigDatum.ConsumerPortalBaseUrl);
            }
        }

        public static string EVaultVerifyUrl
        {
            get
            {
                return GetStringValue(StageConfigDatum.EVaultVerifyUrl, string.Empty);
            }
        }

        /// <summary>
        /// default is false.  For the pipeline > your settings > download page files which were OOMing.
        /// </summary>
        public static bool UseBufferingWhenSendingAppDownloadFiles
        {
            get
            {
                return GetIntValue(StageConfigDatum.UseBufferingWhenSendingAppDownloadFiles, 0) == 1;
            }
        }

        /// <summary>
        /// default is false.  Controls whether or not to log the inputs when FhaPremiumCalculator > ComputeAnnualAverageBalance errors.
        /// </summary>
        public static bool LogAnnualAverageBalanceComputationErrors
        {
            get
            {
                return GetIntValue(StageConfigDatum.LogAnnualAverageBalanceComputationErrors, 0) == 1;
            }
        }

        public static Guid H4HLoanProgramId
        {
            get
            {
                string defaultGuid = Guid.Empty.ToString();

                string guid;
                guid = GetStringValue(StageConfigDatum.H4HLoanProgramId, defaultGuid);

                return new Guid(guid);
            }
        }

        public static string FHAConnectionPilotDomain
        {
            get
            {
                return GetStringValue(StageConfigDatum.FHAConnectionPilotDomain);
            }
        }

        public static string FHAConnectionProductionDomain
        {
            get
            {
                return GetStringValue(StageConfigDatum.FHAConnectionProductionDomain);
            }
        }

        public static string FHATOTALTestLogin
        {
            get
            {
                return GetStringValue(StageConfigDatum.FHATOTALTestLogin);
            }
        }

        public static string FHATOTALTestPassword
        {
            get
            {
                return GetStringValue(StageConfigDatum.FHATOTALTestPassword);
            }
        }

        public static string FHATOTALTestURL
        {
            get
            {
                return GetStringValue(StageConfigDatum.FHATOTALTestURL);
            }
        }

        public static string FHATOTALProductionLogin
        {
            get
            {
                return GetStringValue(StageConfigDatum.FHATOTALProductionLogin);
            }
        }

        public static string FHATOTALProductionPassword
        {
            get
            {
                return GetStringValue(StageConfigDatum.FHATOTALProductionPassword);
            }
        }

        public static string FHATOTALProductionURL
        {
            get
            {
                return GetStringValue(StageConfigDatum.FHATOTALProductionURL);
            }
        }

        public static string FHATOTALProductionLogin_SOAPMISMO
        {
            get
            {
                return GetStringValue(StageConfigDatum.FHATOTALProductionLogin_SOAPMISMO);
            }
        }

        public static string FHATOTALProductionPassword_SOAPMISMO
        {
            get
            {
                return GetStringValue(StageConfigDatum.FHATOTALProductionPassword_SOAPMISMO);
            }
        }

        public static string FHATOTALTestLogin_SOAPMISMO
        {
            get
            {
                return GetStringValue(StageConfigDatum.FHATOTALTestLogin_SOAPMISMO);
            }
        }

        public static string FHATOTALTestPassword_SOAPMISMO
        {
            get
            {
                return GetStringValue(StageConfigDatum.FHATOTALTestPassword_SOAPMISMO);
            }
        }

        public static bool DisableUserInitatedLoanFileCreation
        {
            get
            {
                return GetIntValue(StageConfigDatum.DisableUserInitatedLoanFileCreation, 0) == 1;
            }
        }
        public static string FileDBSiteCode
        {
            get
            {
                return GetStringValue(StageConfigDatum.FileDBSiteCode);
            }
        }

        public static string FileDBSiteCodeForEdms
        {
            get
            {
                return GetStringValue(StageConfigDatum.FileDBSiteCodeForEdms);
            }
        }

        public static string FileDBSiteCodeForEdmsPdf => GetStringValue(StageConfigDatum.FileDBSiteCodeForEdmsPdf);

        public static string FileDBSiteCodeForPricingSnapshot => GetStringValue(StageConfigDatum.FileDBSiteCodeForPricingSnapshot);

        public static string FileDBSiteCodeForTempFile
        {
            get
            {
                return GetStringValue(StageConfigDatum.FileDBSiteCodeForTempFile);
            }
        }

        public static string PmlBaseUrl
        {
            get
            {
                return GetStringValue(StageConfigDatum.PmlBaseUrl);
            }
        }

        public static string ExceptionDeveloperSupport
        {
            get
            {
                return GetStringValue(StageConfigDatum.ExceptionDeveloperSupport);
            }
        }

        // OPM #6953
        public static string PmlFeedbackSupport
        {
            get
            {
                return GetStringValue(StageConfigDatum.PmlFeedbackSupport);
            }
        }

        public static string PmlTimingSupport
        {
            get
            {
                return GetStringValue(StageConfigDatum.PmlTimingSupport);
            }
        }

        public static int PeriodOfLogsInErrorsOfDMBadPackages
        {
            get
            {
                const int defaultValue = 50;

                return GetIntValue(StageConfigDatum.PeriodOfLogsInErrorsOfDMBadPackages, defaultValue);
            }
        }

        public static int LpeTimingEmailThresholdInSeconds
        {
            get
            {
                const int defaultValue = 90;

                return GetIntValue(StageConfigDatum.LpeTimingEmailThresholdInSeconds, defaultValue);
            }
        }

        public static int WaitBeforeRequeuingForImageGeneration
        {
            get
            {
                return GetIntValue(StageConfigDatum.WaitBeforeRequeuingForImageGeneration);
            }
        }

        public static int VendorCommunicationWebServiceCooldownMinutes
        {
            get
            {
                return GetIntValue(StageConfigDatum.VendorCommunicationWebServiceCooldownMinutes, 2);
            }
        }

        public static string LpeSnapshot1Name
        {
            get
            {
                return GetStringValue(StageConfigDatum.LpeSnapshot1Name);
            }
        }

        public static string LpeSnapshot2Name
        {
            get
            {
                return GetStringValue(StageConfigDatum.LpeSnapshot2Name);
            }
        }

        public static bool EnableDroppingDbSnapshot
        {
            get { return GetIntValue(StageConfigDatum.EnableDroppingDbSnapshot, 1) == 1; }
        }

        // Frozen policy is one that is loaded in memory but no loan progran references to it.
        // when timeout happends, the frozen policy will be unloaded.
        public static int TimeoutInMinutes4FrozenPolicy
        {
            get
            {
                const int defaultValue = 3;

                return GetIntValue(StageConfigDatum.TimeoutInMinutes4FrozenPolicy, defaultValue);
            }
        }

        public static bool LpeReuseDataOfPrevSnapshot
        {
            get
            {
                return GetIntValue(StageConfigDatum.LpeReuseDataOfPrevSnapshot, 1) != 0;
            }
        }

        public static bool EnableConversationLogQuickPosting
        {
            get
            {
                const int defaultValue = 1;

                return GetIntValue(StageConfigDatum.EnableConversationLogQuickPosting, defaultValue) == 1;
            }
        }

        /// <summary>
        /// Opm 450195.  When true, people will be asked for permission levels to post, and permission levels will determine viewing / posting / replying / hiding for a thread.
        /// </summary>
        public static bool EnableConversationLogPermissions
        {
            get
            {
                // Note: for this R3 2017 release, we plan to turn off new features by default.
                const int defaultValue = 0;

                return GetIntValue(StageConfigDatum.EnableConversationLogPermissions, defaultValue) == 1;
            }
        }

        public static string ConversationLogIP
        {
            get
            {
                // Try to get settings from site.config first.
                string s = ConstSite.ConversationLogIP;
                if (!string.IsNullOrEmpty(s))
                {
                    return s;
                }

                const string defaultValue = "localhost";

                return GetStringValue(StageConfigDatum.ConversationLogIP, defaultValue);
            }
        }

        public static int ConversationLogPort
        {
            get
            {
                // Try to get settings from site.config first.
                int n = ConstSite.ConversationLogPort;
                if (n > 0)
                {
                    return n;
                }

                const int defaultValue = 0;

                return GetIntValue(StageConfigDatum.ConversationLogPort, defaultValue);
            }
        }

        public static bool AdjustParRateOptionEnable
        {
            get
            {
                return GetIntValue(StageConfigDatum.AdjustParRateOptionEnable, 1) != 0;
            }
        }
        public static bool DebugQRoleChangeProcessor
        {
            get
            {
                return GetIntValue(StageConfigDatum.DebugQRoleChangeProcessor, 0) == 1;
            }
        }

        public static bool AdjustPolicyCounterEnable
        {
            get
            {
                return GetIntValue(StageConfigDatum.AdjustPolicyCounterEnable, 1) == 1;
            }
        }

        public static string CreditTestPasswordForCBC
        {
            get
            {
                return GetStringValue(StageConfigDatum.CreditTestPasswordForCBC);
            }
        }

        public static string CreditTestPasswordForCredco
        {
            get
            {
                return GetStringValue(StageConfigDatum.CreditTestPasswordForCredco);
            }
        }

        public static string CreditTestPasswordForCSC
        {
            get
            {
                return GetStringValue(StageConfigDatum.CreditTestPasswordForCSC);
            }
        }

        public static string CreditTestPasswordForCSD
        {
            get
            {
                return GetStringValue(StageConfigDatum.CreditTestPasswordForCSD);
            }
        }

        public static string CreditTestPasswordForFannieMae
        {
            get
            {
                return GetStringValue(StageConfigDatum.CreditTestPasswordForFannieMae);
            }
        }

        public static string CreditTestPasswordForFiserv
        {
            get
            {
                return GetStringValue(StageConfigDatum.CreditTestPasswordForFiserv);
            }
        }

        public static string CreditTestPasswordForKroll
        {
            get
            {
                return GetStringValue(StageConfigDatum.CreditTestPasswordForKroll);
            }
        }

        public static string CreditTestPasswordForLandSafe
        {
            get
            {
                return GetStringValue(StageConfigDatum.CreditTestPasswordForLandSafe);
            }
        }

        public static string CreditTestPasswordForMcl
        {
            get
            {
                return GetStringValue(StageConfigDatum.CreditTestPasswordForMcl);
            }
        }
        public static string CreditTestPasswordForORCS
        {
            get
            {
                return GetStringValue(StageConfigDatum.CreditTestPasswordForORCS);
            }
        }
        public static string CreditTestPasswordForUniversal
        {
            get
            {
                return GetStringValue(StageConfigDatum.CreditTestPasswordForUniversal);
            }
        }
        public static string CreditTestPasswordForSharperLending
        {
            get
            {
                return GetStringValue(StageConfigDatum.CreditTestPasswordForSharperLending);
            }
        }
        public static string CreditTestPasswordForCQS
        {
            get
            {
                return GetStringValue(StageConfigDatum.CreditTestPasswordForCQS);
            }
        }
        public static string CreditTestPasswordForMCLInstantView
        {
            get
            {
                return GetStringValue(StageConfigDatum.CreditTestPasswordForMCLInstantView);
            }
        }

        public static string CertificateCheckBypassUrl
        {
            get
            {
                string url;
                url = GetStringValue(StageConfigDatum.CertificateCheckBypassUrl, string.Empty);

                return url.Replace(" ", string.Empty);
            }
        }

        public static bool IsGroupLpeTaskByProductCode
        {
            // 10/31/2008 dd - Currently, we are break a list of LP base on investor + product code.
            // I want to experiment that we only break LPs base on investor to have less LpeTask. I want
            // to see if the performance get a little better.
            get
            {
                return GetIntValue(StageConfigDatum.IsGroupLpeTaskByProductCode, 1) == 1;
            }
        }

        public static Guid TotalScorecardLoanProgramId
        {
            get
            {
                const string defaultValue = "e3058ce0-8680-4d15-a66f-75e7649811ca";

                string guid;
                guid = GetStringValue(StageConfigDatum.TotalScorecardLoanProgramId, defaultValue);

                return new Guid(guid);
            }
        }

        public static bool TotalScorecardSOAPMISMOEnabled
        {
            get
            {
                return GetIntValue(StageConfigDatum.TotalScorecardSOAPMISMOEnabled, 0) == 1;
            }
        }

        // The time that is spent waiting before rechecking the Edocs notification message queue
        public static int EDocNotificationCheckerSleepTimeInMinutes
        {
            get
            {
                const int defaultValue = 15;

                return GetIntValue(StageConfigDatum.EDocNotificationCheckerSleepTimeInMinutes, defaultValue);
            }
        }

        // How old a notification has to be before an email is sent out for it
        public static int EDocNotificationCutoffInMinutes
        {
            get
            {
                const int defaultValue = 0;

                return GetIntValue(StageConfigDatum.EDocNotificationCutoffInMinutes, defaultValue);
            }
        }

        // Estimate of how long it takes to convert a single page
        public static int EDocImageGeneratingMillisecondPerPageEstimate
        {
            get
            {
                return GetIntValue(StageConfigDatum.EDocImageGeneratingMillisecondPerPageEstimate);
            }
        }

        // Estimate of how long the Edoc will be in the queue
        public static int EDocImageGeneratingQueueWaitingMillisecondAvgEstimate
        {
            get
            {
                return GetIntValue(StageConfigDatum.EDocImageGeneratingQueueWaitingMillisecondAvgEstimate);
            }
        }

        // The number of messages that the task notification handler grabs from the queue each time it runs
        public static int TaskNotificationNumMessagesToProcess
        {
            get
            {
                const int defaultValue = 1000;

                return GetIntValue(StageConfigDatum.TaskNotificationNumMessagesToProcess, defaultValue);
            }
        }

        public static int QueuePdfConverter_LOQSlice
        {
            get
            {
                return GetIntValue(StageConfigDatum.QueuePdfConverter_LOQSlice);
            }
        }

        public static int QueuePdfConverter_PMLQSlice
        {
            get
            {
                return GetIntValue(StageConfigDatum.QueuePdfConverter_PMLQSlice);
            }
        }

        public static int QueuePdfConverter_FAXQSlice
        {
            get
            {
                return GetIntValue(StageConfigDatum.QueuePdfConverter_FAXQSlice);
            }
        }

        public static string MSMQ_MobileLoanActivity
        {
            get
            {
                return GetStringValue(StageConfigDatum.MSMQ_MobileLoanActivity, string.Empty);
            }
        }

        public static string MSMQ_IISLog
        {
            get
            {
                // Try to get settings from site.config first.
                string s = ConstSite.MSMQ_IISLog;

                if (string.IsNullOrEmpty(s) == false)
                {
                    return s;
                }

                return GetStringValue(StageConfigDatum.MSMQ_IISLog, string.Empty);
            }
        }

        public static string MSMQ_PricingLog
        {
            get
            {
                return GetStringValue(StageConfigDatum.MSMQ_PricingLog, string.Empty);
            }
        }

        public static string MSMQ_TaskTriggerTemplate
        {
            get
            {
                return GetStringValue(StageConfigDatum.MSMQ_TaskTriggerTemplate, string.Empty);
            }
        }

        public static string MSMQ_TrackIntegrationModifiedFile
        {
            get
            {
                return GetStringValue(StageConfigDatum.MSMQ_TrackIntegrationModifiedFile, string.Empty);
            }
        }

        public static string MSMQ_EDoc_StatusUpdateQueue
        {
            get
            {
                return GetStringValue(StageConfigDatum.MSMQ_EDoc_StatusUpdateQueue, string.Empty);
            }
        }

        public static string MSMQ_EDoc_NormalQueue
        {
            get
            {
                return GetStringValue(StageConfigDatum.MSMQ_EDoc_NormalQueue, string.Empty);
            }
        }

        public static string MSMQ_EDoc_PMLQueue
        {
            get
            {
                return GetStringValue(StageConfigDatum.MSMQ_EDoc_PMLQueue, string.Empty);
            }
        }

        public static string MSMQ_EDoc_FaxQueue
        {
            get
            {
                return GetStringValue(StageConfigDatum.MSMQ_EDoc_FaxQueue, string.Empty);
            }
        }

        public static bool EnableEDocsMSMQ
        {
            get
            {
                return GetIntValue(StageConfigDatum.EnableEDocsMSMQ, 0) == 1;
            }
        }

        public static string MSMQ_XsltReportRequest
        {
            get
            {
                return GetStringValue(StageConfigDatum.MSMQ_XsltReportRequest, string.Empty);
            }
        }

        public static string MSMQ_ElasticSearch
        {
            get
            {
                // Try to get settings from site.config first.
                string s = ConstSite.MSMQ_ElasticSearch;

                if (string.IsNullOrEmpty(s) == false)
                {
                    return s;
                }

                return GetStringValue(StageConfigDatum.MSMQ_ElasticSearch, string.Empty);
            }
        }

        public static string MSMQ_PdfRasterize
        {
            get
            {
                return GetStringValue(StageConfigDatum.MSMQ_PdfRasterize, string.Empty);
            }
        }

        public static string MSMQ_EdocsNightlyMigration
        {
            get
            {
                return GetStringValue(StageConfigDatum.MSMQ_EdocsNightlyMigration, string.Empty);
            }
        }

        public static string MSMQ_EdocsMigration
        {
            get
            {
                return GetStringValue(StageConfigDatum.MSMQ_EdocsMigration, string.Empty);
            }
        }

        public static string ESignEmailAddress
        {
            get
            {
                return GetStringValue(StageConfigDatum.ESignEmailAddress, string.Empty);
            }
        }

        public static string ESignEncryptedEmailPassword
        {
            get
            {
                return GetStringValue(StageConfigDatum.ESignEncryptedEmailPassword, string.Empty);
            }
        }

        public static string ComplianceEasePTMLogin
        {
            get
            {
                return GetStringValue(StageConfigDatum.ComplianceEasePTMLogin);
            }
        }

        public static string ComplianceEasePTMPassword
        {
            get
            {
                return GetStringValue(StageConfigDatum.ComplianceEasePTMPassword);
            }
        }

        public static string ComplianceEasePTMEditorUserId
        {
            get
            {
                return GetStringValue(StageConfigDatum.ComplianceEasePTMEditorUserId);
            }
        }

        public static string ComplianceEasePTMTestLogin
        {
            get
            {
                return GetStringValue(StageConfigDatum.ComplianceEasePTMTestLogin);
            }
        }

        public static string ComplianceEasePTMTestPassword
        {
            get
            {
                return GetStringValue(StageConfigDatum.ComplianceEasePTMTestPassword);
            }
        }

        public static int ComplianceEaseIndicatorRefreshSeconds
        {
            get
            {
                const int defaultValue = 60 * 2;

                return GetIntValue(StageConfigDatum.ComplianceEaseIndicatorRefreshSeconds, defaultValue);
            }
        }

        public static string CBCDriveLoginId
        {
            get
            {
                const string defaultValue = "lendqb59";

                return GetStringValue(StageConfigDatum.CBCDriveLoginId, defaultValue);
            }
        }

        public static string CBCDriveLoginPassword
        {
            get
            {
                const string defaultValue = "lending1";

                return GetStringValue(StageConfigDatum.CBCDriveLoginPassword, defaultValue);
            }
        }

        public static int PerRatePricingIterationLimit
        {
            get
            {
                const int defaultValue = 10;

                return GetIntValue(StageConfigDatum.PerRatePricingIterationLimit, defaultValue);
            }
        }

        public static bool DisableConversationLogFeature
        {
            get
            {
                const int defaultValue = 1;

                return GetIntValue(StageConfigDatum.DisableConversationLogFeature, defaultValue) == 1;
            }
        }

        public static bool DisableStatusEventFeature
        {
            get
            {
                const int defaultValue = 0;

                return GetIntValue(StageConfigDatum.DisableStatusEventFeature, defaultValue) == 1;
            }
        }


        /// <summary>
        /// The maximum number of replies before they start all appearing as leaves in the reply tree.
        /// </summary>
        public static int ConversationLogMaxVisibleReplyDepth
        {
            get
            {
                const int defaultValue = 10;

                return GetIntValue(StageConfigDatum.ConversationLogMaxVisibleReplyDepth, defaultValue);
            }
        }

        public static string EmailAddressOfSupport
        {
            get
            {
                const string defaultValue = "support@lendingqb.com";

                return GetStringValue(StageConfigDatum.EmailAddressOfSupport, defaultValue);
            }
        }

        public static string SenderEmailAddressOfSupport
        {
            get
            {
                const string defaultValue = "support@lendingqb.com";

                return GetStringValue(StageConfigDatum.SenderEmailAddressOfSupport, defaultValue);
            }
        }

        public static string EmailAddressOfCriticalOPM
        {
            get
            {
                const string defaultValue = "criticalissuesOPM@lendingqb.com";

                return GetStringValue(StageConfigDatum.EmailAddressOfCriticalOPM, defaultValue);
            }
        }

        public static int DisclosureDelayedTriggerCutoffMinutes
        {
            get
            {
                const int defaultValue = 30;

                return GetIntValue(StageConfigDatum.DisclosureDelayedTriggerCutoffMinutes, defaultValue);
            }
        }

        public static string DisclosureWorkflowRESPATriggerName
        {
            get
            {
                const string defaultValue = "RESPA";

                return GetStringValue(StageConfigDatum.DisclosureWorkflowRESPATriggerName, defaultValue);
            }
        }

        public static bool IsEnableRateFilterForOPM_130011
        {
            get
            {
                return GetIntValue(StageConfigDatum.IsEnableRateFilterForOPM_130011, 1) == 1;
            }
        }

        public static string ComplianceEagleProductionURL
        {
            get
            {
                return GetStringValue(StageConfigDatum.ComplianceEagleProductionURL);
            }
        }

        public static string ComplianceEagleTestURL
        {
            get
            {
                return GetStringValue(StageConfigDatum.ComplianceEagleTestURL);
            }
        }

        public static string ComplianceEaglePTMEditorUserId
        {
            get
            {
                return GetStringValue(StageConfigDatum.ComplianceEaglePTMEditorUserId);
            }
        }

        /// <summary>
        /// Gets the thumb print of LendingQB Intermediate Certificate Authority.
        /// </summary>
        public static string IntermediateCertificateAuhorityThumbPrint
        {
            get
            {
                return GetStringValue(StageConfigDatum.IntermediateCertificateAuhorityThumbPrint, string.Empty);
            }
        }

        public static int DocumentRequestQueueWaitTime
        {
            get
            {
                const int defaultValue = 15;

                return GetIntValue(StageConfigDatum.DocumentRequestQueueWaitTime, defaultValue);
            }
        }

        public static bool AllowLoanPurposeToggleInQP2
        {
            get
            {
                return GetIntValue(StageConfigDatum.AllowLoanPurposeToggleInQP2, 1) == 1;
            }
        }

        /// <summary>
        /// Minimum number of loans to have in the quick pricer 2 loan pool per user (with the feature enabled) until the next creation process. <para></para>
        /// Value was arbitrarily chosen to be 1.<para></para>
        /// OPM 185732
        /// </summary>
        public static int MinNumUnusedQP2LoansToHaveOnHandPerUser
        {
            get
            {
                const int defaultValue = 1;

                return GetIntValue(StageConfigDatum.MinNumUnusedQP2LoansToHaveOnHandPerUser, defaultValue);
            }
        }

        /// <summary>
        /// Maximum number of loans to have in the quick pricer 2 loan pool per user (with the feature enabled) until the next creation process. <para></para>
        /// Value was arbitrarily chosen to be 10.<para></para>
        /// OPM 185732
        /// </summary>
        public static int MaxNumUnusedQP2LoansToHaveOnHandPerUser
        {
            get
            {
                const int defaultValue = 10;

                return GetIntValue(StageConfigDatum.MaxNumUnusedQP2LoansToHaveOnHandPerUser, defaultValue);
            }
        }

        public static Guid AutoDisclosureSystemUserId
        {
            get
            {
                string guid;
                guid = GetStringValue(StageConfigDatum.AutoDisclosureSystemUserId);

                return new Guid(guid);
            }
        }

        // Message of the Day style notification shown on login screen and in header on pipeline page
        public static string GeneralUserNotification
        {
            get
            {
                return GetStringValue(StageConfigDatum.GeneralUserNotification, string.Empty);
            }
        }

        /// <summary>
        /// OPM 211909 - pipeline and loan editor support center link text.
        /// </summary>
        public static string SupportCenterLinkText
        {
            get
            {
                const string defaultValue = "Support Center";

                return GetStringValue(StageConfigDatum.SupportCenterLinkText, defaultValue);
            }
        }

        // Message of the Day style notification for CPortal
        public static string CPortalUserNotification
        {
            get
            {
                return GetStringValue(StageConfigDatum.CPortalUserNotification, string.Empty);
            }
        }

        /// <summary>
        /// True if we are enforcing session invalidation in LendingQB.
        /// OPM 247103.  Not being able to login is a major issue that
        /// can put us in violation of SLA level 1 - the highest level.
        /// To shut the feature off in case of over-enforcement, we can
        /// set this bit to 0.
        /// </summary>
        public static bool UseEnhancedSessionManagement
        {
            get
            {
                return GetIntValue(StageConfigDatum.UseEnhancedSessionManagement, 1) == 1;
            }
        }

        /// <summary>
        /// Forces the use of the old page if set to 1 in case the new page has a bug in it.  Default is false.
        /// </summary>
        public static bool UseOldQualifiedLoanProgramSearchResult
        {
            get
            {
                return GetIntValue(StageConfigDatum.UseOldQualifiedLoanProgramSearchResult, 0) == 1;
            }
        }

        /// <summary>
        /// If true do the logging.  Default is true.
        /// </summary>
        public static bool LogRespaTriggerEvaluation
        {
            get
            {
                return GetIntValue(StageConfigDatum.LogRespaTriggerEvaluation, 1) == 1;
            }
        }

        // Destination address for smoke test emails.
        public static string SmokeTestEmailDestination
        {
            get
            {
                return GetStringValue(StageConfigDatum.SmokeTestEmailDestination, string.Empty);
            }
        }

        /// <summary>
        /// Gets the time out setting for sql server.
        /// OPM 203633 - parent 148855.
        /// Allow user to decide time out for the major query in the broker export and import tool.
        /// </summary>
        /// <value>The time out setting for sql query.</value>
        public static int JustinOneSqlQueryTimeOutInSeconds
        {
            get
            {
                const int defaultValue = 30;

                return GetIntValue(StageConfigDatum.JustinOneSqlQueryTimeOutInSeconds, defaultValue);
            }
        }

        /// <summary>
        /// A temporary bit.  <para></para>
        /// If it's determined we don't need to rollback the code updates, this can be gotten rid of. <para></para>
        /// </summary>
        public static bool UseLibForBrokerReplication
        {
            get
            {
                const int defaultValue = 1;
                return GetIntValue(StageConfigDatum.UseLibForBrokerReplication, defaultValue) == 1;
            }
        }

        /// <summary>
        /// A temporary bit.  <para></para>
        /// If it's determined we don't need to rollback the code updates, this can be gotten rid of. <para></para>
        /// </summary>
        public static bool UseLibForLoanReplication
        {
            get
            {
                const int defaultValue = 1;
                return GetIntValue(StageConfigDatum.UseLibForLoanReplication, defaultValue) == 1;
            }
        }

        /// <summary>
        /// Gets the time out setting for export in dot net server.
        /// OPM 203633 - parent 148855.
        /// Allow user to decide time out for the one export in broker export/import tool.
        /// </summary>
        /// <value>The time out setting for dot net server.</value>
        public static int JustinBrokerExportTimeOutInSeconds
        {
            get
            {
                const int defaultValue = 600;

                return GetIntValue(StageConfigDatum.JustinBrokerExportTimeOutInSeconds, defaultValue);
            }
        }

        /// <summary>
        /// Gets the pattern of the url of production for broker export tool.
        /// </summary>
        /// <value>When exporting from production, only allow exporting from lo author.</value>
        public static string JustinLoauthUrlPatternChecking
        {
            get
            {
                const string defaultValue = "loauth";

                return GetStringValue(StageConfigDatum.JustinLoauthUrlPatternChecking, defaultValue);
            }
        }

        /// <summary>
        /// Gets the vendor ID for DocMagic's production environment.
        /// </summary>
        /// <value>The vendor ID for DocMagic's production environment.</value>
        public static Guid DocMagicProductionVendorId
        {
            get
            {
                const string defaultValue = "58E1D971-F32E-4569-A157-0A1E821B8478";

                string guid;
                guid = GetStringValue(StageConfigDatum.DocMagicProductionVendorId, defaultValue);

                return new Guid(guid);
            }
        }

        /// <summary>
        /// Gets the vendor ID for DocMagic's stage environment.
        /// </summary>
        /// <value>The vendorID for DocMagic's stage environment.</value>
        public static Guid DocMagicStageVendorId
        {
            get
            {
                const string defaultValue = "58E1D971-F32E-4569-A157-0A1E821B8478";

                string guid;
                guid = GetStringValue(StageConfigDatum.DocMagicStageVendorId, defaultValue);

                return new Guid(guid);
            }
        }

        /// <summary>
        /// Gets a value indicating whether we should use the XmlSerializer constructor without the XmlRoot parameter.
        /// </summary>
        public static bool UseXmlSerializerWithNoXmlRoot
        {
            get
            {
                return GetIntValue(StageConfigDatum.UseXmlSerializerWithNoXmlRoot, 1) == 1;
            }
        }

        /// <summary>
        /// Gets the time out setting for import in dot net server.
        /// OPM 203633 - parent 148855.
        /// Allow user to decide time out for the one import in broker export/import tool.
        /// </summary>
        /// <value>The time out seconds for import in dot net server.</value>
        public static int JustinBrokerImportTimeOutInSeconds
        {
            get
            {
                const int defaultValue = 600;

                return GetIntValue(StageConfigDatum.JustinBrokerImportTimeOutInSeconds, defaultValue);
            }
        }

        /// <summary>
        /// Gets a value indicating the number of seconds to wait between calls
        /// to retrieve barcode scanning status updates on the document list.
        /// </summary>
        /// <value>
        /// The number of seconds to wait between calls to retrieve barcode 
        /// status info. Default value is 30 seconds.
        /// </value>
        public static int DocListBarcodeStatusRefreshIntervalSeconds
        {
            get
            {
                const int defaultValue = 30;

                return GetIntValue(StageConfigDatum.DocListBarcodeStatusRefreshIntervalSeconds, defaultValue);
            }
        }

        /// <summary>
        /// Gets the number of seconds that must elapse between calls to
        /// GetGfeToleranceInfo().
        /// </summary>
        /// <value>
        /// The number of seconds that must elapse between calls to
        /// GetGfeToleranceInfo(). The default value is 0, so we will not enforce
        /// a minimum elapsed time unless explicitly added to the stage config.
        /// </value>
        public static int MinimumSecondsBetweenGfeToleranceRefreshes
        {
            get
            {
                const int defaultValue = 0;

                return GetIntValue(StageConfigDatum.MinimumSecondsBetweenGfeToleranceRefreshes, defaultValue);
            }
        }


        /// <summary>
        /// Gets a value indicating whether the UniqueChecksum member of the
        /// CApplicantPrice class should be logged.
        /// </summary>
        /// <value>
        /// True if the value is 1. Otherwise, false. Default value is true.
        /// </value>
        public static bool LogPricingChecksum
        {
            get
            {
                return GetIntValue(StageConfigDatum.LogPricingChecksum, 1) == 1;
            }
        }

        public static bool EnableDelayedESignNotificationLeCdRowUpdates
        {
            get
            {
                return GetIntValue(StageConfigDatum.EnableDelayedESignNotificationLeCdRowUpdates, 0) == 1;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the information to debug duplicate
        /// login issues should be logged. This includes logging when we assign
        /// a new session id to a user and logging duplicate login exceptions
        /// on edocs.lendingqb.com.
        /// </summary>
        /// <value>
        /// True if the value is 1. Otherwise, false. Default value is true.
        /// </value>
        public static bool LogDuplicateLoginDebugInfo
        {
            get
            {
                return GetIntValue(StageConfigDatum.LogDuplicateLoginDebugInfo, 1) == 1;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the TPO portal pipeline retrieves
        /// tasks based on user assignment. When this is enabled, the task
        /// counters will be more accurate but the pipeline will take longer
        /// to load.
        /// </summary>
        /// <value>
        /// True if the value is 1. Otherwise, false. Defualt value is true.
        /// </value>
        public static bool UseNewTpoPortalPipelineTaskRetrieval
        {
            get
            {
                return GetIntValue(StageConfigDatum.UseNewTpoPortalPipelineTaskRetrieval, 1) == 1;
            }
        }

        public static bool LogSplitDocumentInfoInBarcodeSplitter
        {
            get
            {
                return GetIntValue(StageConfigDatum.LogSplitDocumentInfoInBarcodeSplitter, 1) == 1;
            }
        }

        // OPM 233251.
        public static bool TrackIntegrationDeletesWithoutQueue
        {
            get
            {
                return GetIntValue(StageConfigDatum.TrackIntegrationDeletesWithoutQueue, 0) == 1;
            }
        }

        /// Gets LQB Integration Support email
        /// OPM 232337
        /// </summary>
        /// <value>LQB Integration Support email. Default is integrations@lendingqb.com</value>
        public static string LQBIntegrationEmail
        {
            get
            {
                const string defaultValue = "integrations@lendingqb.com";

                return GetStringValue(StageConfigDatum.LQBIntegrationEmail, defaultValue);
            }
        }

        /// Gets the internal LQB Integration team alias email.
        /// OPM 463659
        /// </summary>
        /// <value>LQB Integration team email. Default is lendingqbise@meridianlink.com</value>
        public static string LQBIntegrationTeamEmail
        {
            get
            {
                const string defaultValue = "lendingqbise@meridianlink.com";

                return GetStringValue(StageConfigDatum.LQBIntegrationTeamEmail, defaultValue);
            }
        }

        public static IEnumerable<string> CenlarIseNotificationEmails
        {
            get
            {
                const string defaultValue = "ISEErrors@lendingqb.com";

                var emails = Tools.SplitEmailList(GetStringValue(StageConfigDatum.CenlarIseNotificationEmails, defaultValue));
                return emails;
            }
        }

        /// <summary>
        /// Gets whether or not we want to email ISEs on webexceptions for webrequests built with the result of an LoXml export and an Xsl file.
        /// </summary>
        /// <value>Whether or not to email ISE team for webexceptions when the webrequest was built with a lo format export and an Xsl transform.</value>
        public static bool AlertIsesOfWebFailuresOnXsltBuiltRequests
        {
            get
            {
                const int defaultValue = 1;

                return GetIntValue(StageConfigDatum.AlertIsesOfWebFailuresOnXsltBuiltRequests, defaultValue) == 1;
            }
        }

        /// <summary>
        /// Enables Json Net String Escaping.
        /// </summary>
        public static bool UseJsonNetHtmlEscaping
        {
            get
            {
                return GetIntValue(StageConfigDatum.UseJsonNetHtmlEscaping , 0) == 1;
            }
        }

        // OPM 199082
        public static bool EnableDefaultBranchesAndPriceGroups
        {
            get
            {
                return GetIntValue(StageConfigDatum.EnableDefaultBranchesAndPriceGroups, 1) == 1;
            }
        }

        public static int IdsDocumentGenerationResultNumberOfPolls
        {
            get
            {
                const int defaultValue = 20;
                const int maxValue = 40;

                int rawValue;
                rawValue = GetIntValue(StageConfigDatum.IdsDocumentGenerationResultNumberOfPolls, defaultValue);

                return rawValue > maxValue ? maxValue : rawValue;
            }
        }

        public static int IdsDocumentGenerationResultPollingIntervalInSeconds
        {
            get
            {
                const int defaultValue = 15;
                const int minValue = 15;

                int rawValue;
                rawValue = GetIntValue(StageConfigDatum.IdsDocumentGenerationResultPollingIntervalInSeconds, defaultValue);

                return rawValue < minValue ? minValue : rawValue;
            }
        }

        public static string LoanProspectorUrl
        {
            get
            {
                const string defaultValue = "https://las.fmrei.com/lsp_cte/las-sec/api/lpa/w2w";

                return GetStringValue(StageConfigDatum.LoanProspectorUrl, defaultValue);
            }
        }

        public static bool EnableESignNotificationEmailFromDocumentFrameworkRequests
        {
            get
            {
                return GetIntValue(StageConfigDatum.EnableESignNotificationEmailFromDocumentFrameworkRequests, 0) == 1;
            }
        }

        public static string ElasticServer
        {
            get
            {
                const string defaultValue = "https://lqbes.meridianlink.com";

                return GetStringValue(StageConfigDatum.ElasticServer, defaultValue);
            }
        }

        public static string MethodInvokeControl(string key)
        {
            if (key == "MessageQueue")
            {
                return GetStringValue(StageConfigDatum.MethodInvokeControl_MessageQueue);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Max number of rows to be returned by RetrieveCustomReport web service when run by a PML user.
        /// </summary>
        public static int RetrieveCustomReportPmlUserMaxRowsReturned
        {
            get
            {
                const int defaultValue = 250;

                return GetIntValue(StageConfigDatum.RetrieveCustomReportPmlUserMaxRowsReturned, defaultValue);
            }
        }

        /// <summary>
        /// Gets a list of server names for local developer environments.
        /// </summary>
        /// <value>A list of server names for local developer environments.</value>
        public static HashSet<string> LocalServerNames
		{
			get
			{
                string users = null;
                users = GetStringValue(StageConfigDatum.LocalServerNames, string.Empty);
                
				if (string.IsNullOrEmpty(users))
				{
					return new HashSet<string>();
				}

				return new HashSet<string>(users.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries), StringComparer.OrdinalIgnoreCase);
			}
		}

        public static int MaxAutomaticCacheUpdateRetries
        {
            get
            {
                return GetIntValue(StageConfigDatum.MaxAutomaticCacheUpdateRetries, defaultValue: 5);
            }
        }

        public static int SecurityEventLogMaxSearchResults
        {
            get
            {
                return GetIntValue(StageConfigDatum.SecurityEventLogMaxSearchResults, defaultValue: 5000);
            }
        }

        public static int SecurityEventLogMaxDownloadResults
        {
            get
            {
                return GetIntValue(StageConfigDatum.SecurityEventLogMaxDownloadResults, defaultValue: 5000);
            }
        }

        public static bool EnableUladCollectionLoading
        {
            get
            {
                return GetIntValue(StageConfigDatum.EnableUladCollectionLoading, defaultValue: 1) == 1;
            }
        }

        public static bool EnableEnhancedPostLoginChecks
        {
            get
            {
                return GetIntValue(StageConfigDatum.EnableEnhancedPostLoginChecks, defaultValue: 1) == 1;
            }
        }

        public static bool DisableBorrowerApplicationCollectionTUpdates
        {
            get
            {
                return GetIntValue(StageConfigDatum.DisableBorrowerApplicationCollectionTUpdates, defaultValue: 1) == 1;
            }
        }

        public static string FirstAmericanPostbackUrl
        {
            get
            {
                return GetStringValue(StageConfigDatum.FirstAmericanPostbackUrl, defaultValue: "https://secure.lendingqb.com/los/webservice/TitleVendorRquest.aspx");
            }
        }

        public static string DocFrameworkPostbackUrl
        {
            get
            {
                return GetStringValue(StageConfigDatum.DocFrameworkPostbackUrl, defaultValue: "https://secure.lendingqb.com/ESignUpdate.aspx");
            }
        }

        /// <summary>
        /// Gets whether we should treat a Closing Disclosure as a Redisclosure for the method 'ProcessDocDisclosureTrigger'.
        /// Can be removed after October Release, 2018.
        /// </summary>
        public static bool IsTreatClosingDisclosureAsRedisclosure
        {
            get
            {
                return GetIntValue(StageConfigDatum.IsTreatClosingDisclosureAsRedisclosure, defaultValue: 1) == 1;
            }
        }

        /// <summary>
        /// Gets a value indicating whether legacy alrogirthm used for the class UpdateGraph
        /// </summary>
        public static bool UseUpdateGraphLegacy => GetIntValue(StageConfigDatum.UseUpdateGraphLegacy, 1) == 1;

        #region Test Intergration
        public static string TestIntegrations_LoansPQ_Innovistar
        {
            get
            {
                return GetStringValue(StageConfigDatum.TestIntegrations_LoansPQ_Innovistar, string.Empty);
            }
        }
        public static string TestIntegrations_LoansPQ_Secure_Inovistar
        {
            get
            {
                return GetStringValue(StageConfigDatum.TestIntegrations_LoansPQ_Secure_Inovistar, string.Empty);
            }
        }
        public static string TestIntegrations_LoansPQ_Eplqa
        {
            get
            {
                return GetStringValue(StageConfigDatum.TestIntegrations_LoansPQ_Eplqa, string.Empty);
            }
        }
        public static string TestIntegrations_LoansPQ_PS
        {
            get
            {
                return GetStringValue(StageConfigDatum.TestIntegrations_LoansPQ_PS, string.Empty);
            }
        }
        public static string TestIntegrations_LoansPQ_Californiacu
        {
            get
            {
                return GetStringValue(StageConfigDatum.TestIntegrations_LoansPQ_Californiacu, string.Empty);
            }
        }
        public static string TestIntegrations_LoansPQ_CS
        {
            get
            {
                return GetStringValue(StageConfigDatum.TestIntegrations_LoansPQ_CS, string.Empty);
            }
        }

        public static string TestIntegrations_LoansPQ_PFFCU
        {
            get
            {
                return GetStringValue(StageConfigDatum.TestIntegrations_LoansPQ_PFFCU, string.Empty);
            }
        }

        public static string TestIntegrations_LoansPQ_Kinecta
        {
            get
            {
                return GetStringValue(StageConfigDatum.TestIntegrations_LoansPQ_Kinecta, string.Empty);
            }
        }

        public static string TestIntegrations_LoansPQ_Calcoastcu
        {
            get
            {
                return GetStringValue(StageConfigDatum.TestIntegrations_LoansPQ_Calcoastcu, string.Empty);
            }
        }

        public static string TestIntegrations_LoansPQ_Demo
        {
            get
            {
                return GetStringValue(StageConfigDatum.TestIntegrations_LoansPQ_Demo, string.Empty);
            }
        }

        public static string TestIntegrations_LoansPQ_Onpoint
        {
            get
            {
                return GetStringValue(StageConfigDatum.TestIntegrations_LoansPQ_Onpoint, string.Empty);
            }
        }
        public static string TestIntegrations_LoansPQ_Oregoncommunitycu
        {
            get
            {
                return GetStringValue(StageConfigDatum.TestIntegrations_LoansPQ_Oregoncommunitycu, string.Empty);
            }
        }
        public static string TestIntegrations_LoansPQ_Secure
        {
            get
            {
                return GetStringValue(StageConfigDatum.TestIntegrations_LoansPQ_Secure, string.Empty);
            }
        }
        public static string TestIntegrations_LoansPQ_ES
        {
            get
            {
                return GetStringValue(StageConfigDatum.TestIntegrations_LoansPQ_ES, string.Empty);
            }
        }
        public static string TestIntegrations_LoansPQ_Innovistar_Sumitloan
        {
            get
            {
                return GetStringValue(StageConfigDatum.TestIntegrations_LoansPQ_Innovistar_Sumitloan, string.Empty);
            }
        }
        public static string TestIntegrations_LoansPQ_Aacreditunion
        {
            get
            {
                return GetStringValue(StageConfigDatum.TestIntegrations_LoansPQ_Aacreditunion, string.Empty);
            }
        }
        public static string TestIntegrations_LoansPQ_Beta
        {
            get
            {
                return GetStringValue(StageConfigDatum.TestIntegrations_LoansPQ_Beta, string.Empty);
            }
        }

        public static string TestIntegrations_LoansPQ_JHA
        {
            get
            {
                return GetStringValue(StageConfigDatum.TestIntegrations_LoansPQ_JHA, string.Empty);
            }
        }

        public static string TestIntegrations_LoansPQ_Coastal24
        {
            get
            {
                return GetStringValue(StageConfigDatum.TestIntegrations_LoansPQ_Coastal24, string.Empty);
            }
        }

        public static string TestIntegrations_LoansPQ_BCU
        {
            get
            {
                return GetStringValue(StageConfigDatum.TestIntegrations_LoansPQ_BCU, string.Empty);
            }
        }

        public static string TestIntegrations_LoansPQ_Summitcreditunion
        {
            get
            {
                return GetStringValue(StageConfigDatum.TestIntegrations_LoansPQ_Summitcreditunion, string.Empty);
            }
        }

        public static string TestIntegrations_LoansPQ_WS
        {
            get
            {
                return GetStringValue(StageConfigDatum.TestIntegrations_LoansPQ_WS, string.Empty);
            }
        }

        public static string TestIntegrations_LoansPQ_Join
        {
            get
            {
                return GetStringValue(StageConfigDatum.TestIntegrations_LoansPQ_Join, string.Empty);
            }
        }

        public static string TestIntegrations_LoansPQ_JDCU
        {
            get
            {
                return GetStringValue(StageConfigDatum.TestIntegrations_LoansPQ_JDCU, string.Empty);
            }
        }

        public static string TestIntegrations_LoansPQ_Bankoncit
        {
            get
            {
                return GetStringValue(StageConfigDatum.TestIntegrations_LoansPQ_Bankoncit, string.Empty);
            }
        }
        public static string TestIntegrations_LoansPQ_CACU
        {
            get
            {
                return GetStringValue(StageConfigDatum.TestIntegrations_LoansPQ_CACU, string.Empty);
            }
        }
        public static string TestIntegrations_LoansPQ_Epl
        {
            get
            {
                return GetStringValue(StageConfigDatum.TestIntegrations_LoansPQ_Epl, string.Empty);
            }
        }
        public static string TestIntegrations_LoansPQ_JhaDemo
		{
            get
            {
                return GetStringValue(StageConfigDatum.TestIntegrations_LoansPQ_JhaDemo, string.Empty);
            }
        }
        public static string TestIntegrations_GlobalDMS_LookupMethods
        {
            get
            {
                return GetStringValue(StageConfigDatum.TestIntegrations_GlobalDMS_LookupMethods, string.Empty);
            }
        }
        public static string TestIntegrations_GlobalDMS_Orders
        {
            get
            {
                return GetStringValue(StageConfigDatum.TestIntegrations_GlobalDMS_Orders, string.Empty);
            }
        }

        public static string TestIntegrations_GlobalDMS_FileUpload
        {
            get
            {
                return GetStringValue(StageConfigDatum.TestIntegrations_GlobalDMS_FileUpload, string.Empty);
            }
        }

        public static string TestIntegrations_OtherIntegration_FreddieMac
        {
            get
            {
                return GetStringValue(StageConfigDatum.TestIntegrations_OtherIntegration_FreddieMac, string.Empty);
            }
        }

        public static string TestIntegrations_OtherIntegration_DataVerify
        {
            get
            {
                return GetStringValue(StageConfigDatum.TestIntegrations_OtherIntegration_DataVerify, string.Empty);
            }
        }

        public static bool UseNewWebRequest
        {
            get
            {
                return GetIntValue(StageConfigDatum.UseNewWebRequest, 0) == 1;
            }
        }

        public static string LON_URL
        {
            get
            {
                const string defaultValue = "https://secure.loanoriginator.net/webservice/loanstatus.php";
                return GetStringValue(StageConfigDatum.LON_URL, defaultValue);
            }
        }

        public static string LON_USERNAME
        {
            get
            {
                const string defaultValue = "lo_superuser";
                return GetStringValue(StageConfigDatum.LON_USERNAME, defaultValue);
            }
        }

        public static string LON_PASSWORD
        {
            get
            {
                const string defaultValue = "LO23d43$82!";
                return GetStringValue(StageConfigDatum.LON_PASSWORD, defaultValue);
            }
        }

        public static bool UseNewEmailer
        {
            get
            {
                return GetIntValue(StageConfigDatum.UseNewEmailer, 0) == 1;
            }
        }

        public static bool UseNewFileDb
        {
            get
            {
                return GetIntValue(StageConfigDatum.UseNewFileDb, 0) == 1;
            }
        }

        public static bool UseFileDbEncryption
        {
            get
            {
                return GetIntValue(StageConfigDatum.UseFileDbEncryption, 0) == 1;
            }
        }

        public static bool UseAwsS3Storage
        {
            get
            {
                return GetIntValue(StageConfigDatum.UseAwsS3Storage, 0) == 1;
            }
        }

        public static bool UseLayeredLoanFileFields
        {
            get
            {
                return GetIntValue(StageConfigDatum.UseLayeredLoanFileFields, 0) == 1;
            }
        }

        public static bool UseNewFileSystem
        {
            get
            {
                return GetIntValue(StageConfigDatum.UseNewFileSystem, 0) == 1;
            }
        }

        public static bool EnableTempOptionRetrievalCaching
        {
            get
            {
                return GetIntValue(StageConfigDatum.EnableTempOptionRetrievalCaching, 0) == 1;
            }
        }

        public static bool EnableTempOptionRetrievalLogging
        {
            get
            {
                return GetIntValue(StageConfigDatum.EnableTempOptionRetrievalLogging, 0) == 1;
            }
        }

        public static string MismoAppraisalIdentifierOwnerURI
        {
            get
            {
                const string defaultValue = "https://www.uniformdataportal.com/VAMAuthUtility/login.aspx";

                return GetStringValue(StageConfigDatum.MismoAppraisalIdentifierOwnerURI, defaultValue);
            }
        }

        public static int RegionSetCacheSeconds
        {
            get
            {
                return GetIntValue(StageConfigDatum.RegionSetCacheSeconds, 120);
            }
        }

        public static bool EnableEmailDomainAuthorization
        {
            get
            {
                return GetIntValue(StageConfigDatum.EnableEmailDomainAuthorization, 0) == 1;
            }
        }

        public static string DefaultSenderAddress
        {
            get
            {
                return GetStringValue(StageConfigDatum.DefaultSenderAddress, "messenger@lendingqb.com");
            }
        }
        #endregion

        #region Utilities methods

        private static volatile Tuple<Dictionary<string, string>, IReadOnlyList<KeyValuePair<string, string>>> s_partnerKeysData = null;

        private static volatile Func<object, bool> configForPartnerKeysIsSameInstance = o => false;

        /// <summary>
        /// Gets the partner key salts and hashes from the global list of constants.  This value is refreshed with the changes to the constant list.
        /// </summary>
        /// <returns>The partner key salts and hashes.</returns>
        public static Tuple<Dictionary<string, string>, IReadOnlyList<KeyValuePair<string, string>>> GetPartnerKeysData()
        {
            var driver = LqbGrammar.GenericLocator<LqbGrammar.Queries.IConfigurationQueryFactory>.Factory.CreateStageConfiguration();

            var configuration = driver.ReadAllValues();
            if (!configForPartnerKeysIsSameInstance(configuration))
            {
                s_partnerKeysData = ComputePartnerKeysData(configuration);

                // 2018-04 tj 468456 - Implementation detail of CachedStageConfigurationQuery is that it will
                // reuse instances, so we'll exploit this for now to improve performance.  Note that this occurs
                // after s_partnerKeysData has already been updated, so concurrent threads may re-compute the
                // partner keys and reset the predicate, but the resulting values will still be consistent.
                configForPartnerKeysIsSameInstance = o => object.ReferenceEquals(o, configuration);
            }

            // Thread safety requires that setting and retrieving static data must be done as an atomic operation,
            // so I'm combining the partner key salts and hashes into a single data structure (which is a class, not a struct).
            return s_partnerKeysData;
        }

        /// <summary>
        /// A pure function for computing the partner key data from the set of all <see cref="ConstStage"/> configuration values.
        /// </summary>
        /// <param name="configuration">The set of all <see cref="ConstStage"/> configuration values.</param>
        /// <returns>The partner key data.</returns>
        private static Tuple<Dictionary<string, string>, IReadOnlyList<KeyValuePair<string, string>>> ComputePartnerKeysData(Dictionary<string, Tuple<int, string>> configuration)
        {
            var partnerKeysHash = new List<KeyValuePair<string, string>>();
            var partnerKeysSalt = new Dictionary<string, string>();
            foreach (KeyValuePair<string, Tuple<int, string>> configValue in configuration)
            {
                if (!configValue.Key.StartsWith("PartnerKey_", StringComparison.Ordinal))
                {
                    continue;
                }

                if (configValue.Key.StartsWith("PartnerKey_Salt_", StringComparison.Ordinal))
                {
                    string partnerName = configValue.Key.Substring(16);
                    partnerKeysSalt.Add(partnerName, configValue.Value.Item2);
                }
                else
                {
                    string partnerName = configValue.Key.Substring(11);
                    partnerKeysHash.Add(new KeyValuePair<string, string>(partnerName, configValue.Value.Item2));
                }
            }

            return new Tuple<Dictionary<string, string>, IReadOnlyList<KeyValuePair<string, string>>>(partnerKeysSalt, partnerKeysHash);
        }

        private static int GetIntValue(StageConfigValue datum)
        {
            int? value = datum.GetIntValue();
            if (value == null)
            {
                throw new StageConfigKeyNotFoundException(datum.KeyName);
            }

            return value.Value;
        }

        private static int GetIntValue(StageConfigValue datum, int defaultValue)
        {
            int? value = datum.GetIntValue();
            return value ?? defaultValue;
        }

        private static string GetStringValue(StageConfigValue datum)
        {
            string value = datum.GetStringValue();
            if (value == null)
            {
                throw new StageConfigKeyNotFoundException(datum.KeyName);
            }

            return value;
        }

        private static string GetStringValue(StageConfigValue datum, string defaultValue)
        {
            string value = datum.GetStringValue();
            return value ?? defaultValue;
        }

        private static DateTime GetDate(StageConfigValue datum, DateTime defaultValue)
        {
            string value = datum.GetStringValue();
            if (value == null)
            {
                return defaultValue;
            }

            DateTime ret;
            bool ok = DateTime.TryParse(value, out ret);
            return ok ? ret : defaultValue;
        }

        /// <summary>
        /// Helper method for getting lists from stage config strings.  Careful that the strings should not contain the delimiter which is '|' by default.
        /// </summary>
        /// <param name="stageConfigValue">The value from stage config of interest.</param>
        /// <param name="defaultValue">The value if no stage config value is present. Default is empty string.</param>
        /// <param name="delimiter">The delimiter for parsing the stage config value.  Default is the pipe character.</param>
        /// <param name="stringSplitOptions">Whether or not to include empty entries.  Default excludes empty entries.</param>
        /// <returns>The list of delimited values from stage config.</returns>
        private static IEnumerable<string> FromDelimitedList(StageConfigValue stageConfigValue, string defaultValue = "", char delimiter = '|', StringSplitOptions stringSplitOptions = StringSplitOptions.RemoveEmptyEntries)
        {
            string delimitedList = GetStringValue(stageConfigValue, defaultValue);

            return delimitedList.Split(new char[] { delimiter }, stringSplitOptions);
        }

        #endregion

    }
}
