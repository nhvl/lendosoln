﻿
namespace LendersOffice.Constants
{
    public enum E_ServerT
    {
        StandAlone = 0,
        CalcOnly = 1,
        RequestOnly = 2,
        Undeclared = 3,
        DevMinimum = 4,
        AuthorTest = 5,
    }
}
