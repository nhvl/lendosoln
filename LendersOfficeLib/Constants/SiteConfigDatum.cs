﻿namespace LendersOffice.Constants
{
    /// <summary>
    /// Holds onto list of named datum retrievers.  This class is public only for the sake of testing.
    /// </summary>
    public static class SiteConfigDatum
    {
        public static readonly SiteConfigValue FieldInfoCacheFilePath = new SiteConfigValue("FieldInfoCacheFilePath");
        public static readonly SiteConfigValue MaxNumberOfProductsInParallelTask = new SiteConfigValue("MaxNumberOfProductsInParallelTask");
        public static readonly SiteConfigValue TempPriceEngineStorageLocalPath = new SiteConfigValue("TempPriceEngineStorageLocalPath");
        public static readonly SiteConfigValue PriceEngineStorageMode = new SiteConfigValue("PriceEngineStorageMode");
        public static readonly SiteConfigValue EnableRedirectToCustomFrame = new SiteConfigValue("EnableRedirectToCustomFrame");
        public static readonly SiteConfigValue MsMqLogConnectStr = new SiteConfigValue("MsMqLogConnectStr");
        public static readonly SiteConfigValue MsMqPbLogConnectStr = new SiteConfigValue("MsMqPbLogConnectStr");
        public static readonly SiteConfigValue MSMQ_ElasticSearch = new SiteConfigValue("MSMQ_ElasticSearch");
        public static readonly SiteConfigValue MSMQ_IISLog = new SiteConfigValue("MSMQ_IISLog");
        public static readonly SiteConfigValue EnableLogFor3rdParty = new SiteConfigValue("EnableLogFor3rdParty");
        public static readonly SiteConfigValue LoginRedirectUrl = new SiteConfigValue("LoginRedirectUrl");
        public static readonly SiteConfigValue EDocSignerAuthUrl = new SiteConfigValue("EDocSignerAuthUrl");
        public static readonly SiteConfigValue ErrorEmailAddressRecipient = new SiteConfigValue("ErrorEmailAddressRecipient");
        public static readonly SiteConfigValue SchemaMapErrorRecipient = new SiteConfigValue("SchemaMapErrorRecipient");
        public static readonly SiteConfigValue WebBotDownloadErrorRecipient = new SiteConfigValue("WebBotDownloadErrorRecipient");
        public static readonly SiteConfigValue PmlRatesEmailAddress = new SiteConfigValue("PmlRatesEmailAddress");
        public static readonly SiteConfigValue ErrorMessageDuplicationIntervalInMinutes = new SiteConfigValue("ErrorMessageDuplicationIntervalInMinutes");
        public static readonly SiteConfigValue StandardPdfLocation = new SiteConfigValue("StandardPdfLocation");
        public static readonly SiteConfigValue PdfRasterizerHost = new SiteConfigValue("PdfRasterizerHost");
		public static readonly SiteConfigValue PmlBaseUrl = new SiteConfigValue("PmlBaseUrl");
        public static readonly SiteConfigValue LoginUrl = new SiteConfigValue("LoginUrl");
        public static readonly SiteConfigValue LimitUserSearchResults = new SiteConfigValue("LimitUserSearchResults");
        public static readonly SiteConfigValue LpeReleaseControllerUrl = new SiteConfigValue("LpeReleaseControllerUrl");
        public static readonly SiteConfigValue ConversationLogIP = new SiteConfigValue("ConversationLogIP");
        public static readonly SiteConfigValue ConversationLogPort = new SiteConfigValue("ConversationLogPort");
        public static readonly SiteConfigValue EnableConversationLogPermissions = new SiteConfigValue("EnableConversationLogPermissions");
        public static readonly SiteConfigValue UnderwritingTaskThreadCount = new SiteConfigValue("UnderwritingTaskThreadCount");
        public static readonly SiteConfigValue UnderwritingServerType = new SiteConfigValue("UnderwritingServerType");
        public static readonly SiteConfigValue IsUsingDBSnapshotForLpe = new SiteConfigValue("IsUsingDBSnapshotForLpe");
        public static readonly SiteConfigValue DisplayPolicyAndRuleIdOnLpeResult = new SiteConfigValue("DisplayPolicyAndRuleIdOnLpeResult");
        public static readonly SiteConfigValue LpeLoadDelayXSeconds = new SiteConfigValue("LpeLoadDelayXSeconds");
        public static readonly SiteConfigValue MinLpeMemory = new SiteConfigValue("MinLpeMemory");
        public static readonly SiteConfigValue MaxLpeMemory = new SiteConfigValue("MaxLpeMemory");
        public static readonly SiteConfigValue MinLpePrograms = new SiteConfigValue("MinLpePrograms");
        public static readonly SiteConfigValue MaxLpePrograms = new SiteConfigValue("MaxLpePrograms");
        public static readonly SiteConfigValue GetLoanProgramSetTimeOut = new SiteConfigValue("GetLoanProgramSetTimeOut");
        public static readonly SiteConfigValue LpeGetTotalMemory = new SiteConfigValue("LpeGetTotalMemory");
        public static readonly SiteConfigValue LpeDisplayDetail = new SiteConfigValue("LpeDisplayDetail");
        public static readonly SiteConfigValue EmailIfDetectLpeReleaseError = new SiteConfigValue("EmailIfDetectLpeReleaseError");
        public static readonly SiteConfigValue VerboseLogEnable4Pricing = new SiteConfigValue("VerboseLogEnable4Pricing");
        public static readonly SiteConfigValue IsSecureCookieRequired = new SiteConfigValue("IsSecureCookieRequired");
        public static readonly SiteConfigValue TestDataFolder = new SiteConfigValue("TestDataFolder");
        public static readonly SiteConfigValue TestRsMapDataFolder = new SiteConfigValue("FILES_PATH");
        public static readonly SiteConfigValue LqbAuthCookieName = new SiteConfigValue("LqbAuthCookieName");
        public static readonly SiteConfigValue TimeToLiveInSecond4NewKeyword = new SiteConfigValue("TimeToLiveInSecond4NewKeyword");
        public static readonly SiteConfigValue LendOSolnTestDll = new SiteConfigValue("LendOSolnTestDll");
        public static readonly SiteConfigValue EDocsSite = new SiteConfigValue("EDocsSite");
        public static readonly SiteConfigValue LOSite = new SiteConfigValue("LOSite");
        public static readonly SiteConfigValue ExportXsltFilePath = new SiteConfigValue("ExportXsltFilePath");
        public static readonly SiteConfigValue ShareLoginCookieWithSubdomain = new SiteConfigValue("ShareLoginCookieWithSubdomain");
        public static readonly SiteConfigValue CookieDomain = new SiteConfigValue("CookieDomain");
        public static readonly SiteConfigValue IsTaskMigrationProcess = new SiteConfigValue("IsTaskMigrationProcess");
        public static readonly SiteConfigValue IsDevMachine = new SiteConfigValue("IsDevMachine");
        public static readonly SiteConfigValue TableReplicateServiceUrl = new SiteConfigValue("TableReplicateServiceUrl");
        public static readonly SiteConfigValue EnableFilebasedCmp = new SiteConfigValue("FilebasedCmp_Enabled");
        public static readonly SiteConfigValue RateOptionHistory = new SiteConfigValue("FilebasedCmp_UseRateOptionHistory");
        public static readonly SiteConfigValue LoanProgramTemplateHistory = new SiteConfigValue("FilebasedCmp_UseLoanProgramTemplateHistory");
        public static readonly SiteConfigValue ManualCompareEnabled = new SiteConfigValue("FilebasedCmp_ManualCompare");
        public static readonly SiteConfigValue FilebasedDeveloperEmail = new SiteConfigValue("FilebasedCmp_Email");
        public static readonly SiteConfigValue MachineDisplayName = new SiteConfigValue("MachineDisplayName");
        public static readonly SiteConfigValue GenericResourceLocation = new SiteConfigValue("GenericResourceLocation");
        public static readonly SiteConfigValue CustomResourceLocation = new SiteConfigValue("CustomResourceLocation");
        public static readonly SiteConfigValue PmlCookieName = new SiteConfigValue("PmlCookieName");
        public static readonly SiteConfigValue EnablePerLenderSnapshot = new SiteConfigValue("EnablePerLenderSnapshot");
        public static readonly SiteConfigValue BackgroundJobTypeFilter = new SiteConfigValue("BackgroundJobTypeFilter");
        public static readonly SiteConfigValue EnablePerLenderActualSnapshot = new SiteConfigValue("EnablePerLenderActualSnapshot");
    }
}
