using System;

namespace LendersOffice.Constants
{
    [AttributeUsage(AttributeTargets.Property,AllowMultiple=false)]
	public class ConstantAttribute : Attribute
	{
        private bool m_isSensitiveData = false;
        private string m_actualKey = "";

        public bool IsSensitiveData 
        {
            get { return m_isSensitiveData; }
            set { m_isSensitiveData = value; }
        }
        public string ActualKey 
        {
            get { return m_actualKey; }
            set { m_actualKey = value; }
        }

		public ConstantAttribute()
		{
		}
	}
}
