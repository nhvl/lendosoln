﻿namespace LendersOffice.Constants
{
    using LqbGrammar;
    using LqbGrammar.Queries;
    using Queries.Configuration;

    /// <summary>
    /// Used to retrieve a named configuration datum from the site configuration.  This class is public only for the sake of testing.
    /// </summary>
    public sealed class SiteConfigValue : ConfigurationValueBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SiteConfigValue"/> class.
        /// </summary>
        /// <param name="name">The data item's name.</param>
        public SiteConfigValue(string name)
            : base(name)
        {
        }

        /// <summary>
        /// Interface for retrieving a string valued datum.
        /// </summary>
        /// <returns>The string value of the datum.</returns>
        public override string GetStringValue()
        {
            return this.RetrieveConfigurationValue(this.KeyName);
        }

        /// <summary>
        /// Interface for retrieving a string valued datum using an applicationId.
        /// </summary>
        /// <param name="applicationId">The application ID.</param>
        /// <returns>The string value of the datum.</returns>
        public string GetStringValue(string applicationId)
        {
            string keyName = SiteConfigurationQuery.GetKey(applicationId, this.KeyName);
            return this.RetrieveConfigurationValue(keyName);
        }

        /// <summary>
        /// Pull the configuration data and grab the value for this datum.
        /// </summary>
        /// <param name="keyName">The key for the lookup.</param>
        /// <returns>The value for this datum.</returns>
        private string RetrieveConfigurationValue(string keyName)
        {
            var factory = GenericLocator<IConfigurationQueryFactory>.Factory;
            var driver = factory.CreateSiteConfiguration();

            var configuration = driver.ReadAllValues();

            return configuration.ContainsKey(keyName) ? configuration[keyName] : null;
        }
    }
}
