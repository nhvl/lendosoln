﻿namespace LendersOffice.Constants
{
    using LqbGrammar.DataTypes;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Configuration information consists of name/value pairs
    /// that are retrieved as a collection class.  This base
    /// class holds the name of a datum and defines the
    /// interface to retrieve a datum that is of string type
    /// as all configuration sources contain string data.
    /// This class is public only for the sake of testing.
    /// </summary>
    public abstract class ConfigurationValueBase
    {
        /// <summary>
        /// The name for a particular configuration datum.
        /// </summary>
        private ConfigurationItemName itemName;

        /// <summary>
        /// Initializes a new instance of the <see cref="ConfigurationValueBase"/> class.
        /// </summary>
        /// <param name="name">The data item's name.</param>
        protected ConfigurationValueBase(string name)
        {
            ConfigurationItemName? checkValue = ConfigurationItemName.Create(name);
            if (checkValue == null)
            {
                throw new DeveloperException(ErrorMessage.SystemError);
            }

            this.itemName = checkValue.Value;
        }

        /// <summary>
        /// Gets the key name as a string for use as index into a configuration dictionary.
        /// </summary>
        /// <value>The key name.</value>
        public string KeyName
        {
            get { return this.itemName.ToString(); }
        }

        /// <summary>
        /// Interface for retrieving a string valued datum.
        /// </summary>
        /// <returns>The string value of the datum.</returns>
        public abstract string GetStringValue();
    }
}
