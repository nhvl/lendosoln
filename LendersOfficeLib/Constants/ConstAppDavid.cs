namespace LendersOffice.Constants
{
    using System;
    using System.Web;

    public enum ServerLocation 
    {
        LocalHost,
        Development,
        DevCopy,
        Demo,
        Production,
        Beta,
        Alpha
    }

    public class ConstAppDavid
	{
        public const string CheckEligibilityEmployeeGroup = "Check Eligibility";
        public const string CoreExportEmployeeGroup = "Core Export";
        public const string ThirdPartyExportEmployeeGroup = "3rd Party Export";
        public static readonly Guid SystemBrokerGuid = new Guid("11111111-1111-1111-1111-111111111111");
        public const float PdfPngScaling = 1.75F; // 4/6/2011 dd - When update this scaling value, need to update the PdfScaling variable in PdfEditor.js
        public const float PdfPngThumbOnDemandScaling = .208f;
        public const float PdfPngThumbRequeueScaling = PdfPngScaling * PdfPngThumbOnDemandScaling;
        public static readonly string PdfPngThumbFileDbSuffix = "_thumb_s" + PdfPngThumbRequeueScaling.ToString();

        public const string CustomReportEngineFieldPrefix = "engine_";
        // 7/29/2010 dd - Now InitSave required expected version to be pass in as an argument. For DataObject that we want
        // to bypass version check then constant SkipVersionCheck need to pass in. The reason I made this number hard to remember
        // is because I want all code want to bypass must use this constant.
        public const int SkipVersionCheck = -9421812; 

        /// <summary>
        /// This returns the server location first based on the url, or on the filedb site code if there's no context. <para></para>
        /// Be careful with this if you happen to change between stages on your local machine. <para></para>
        /// If it doesn't know, it returns Production.
        /// </summary>
        public static ServerLocation CurrentServerLocation 
        {
            get 
            {
                ServerLocation ret = ServerLocation.Production;
                HttpContext context = HttpContext.Current;
                
                if (null != context)
                {
                    var host = context.Request.Url.Host;

                    if (context.Request.Url.IsLoopback)
                    {
                        ret = ServerLocation.LocalHost;
                    }
                    else if (host.StartsWith("11.12.13.") || host.StartsWith("172.30.26.") || host.StartsWith("dev.lendersoffice.com") || host.StartsWith("vndev.dory.vn") || host.StartsWith("222.255.206.52/ext"))
                    {
                        if (host == "11.12.13.130" || host == "172.30.26.130")
                        {
                            ret = GetServerLocationFromFileDbSiteCode();
                        }
                        else
                        {
                            ret = ServerLocation.Development;
                        }
                    }
                    else if (host.StartsWith("test.lendersoffice.com") || host.StartsWith("test.pricemyloan.com"))
                    {
                        ret = ServerLocation.Demo;
                    }
                    else if (host.StartsWith("10.11.60.140") || host.StartsWith("beta.lendingqb.com"))
                    {
                        ret = ServerLocation.Beta;
                    }
                    else if (host.StartsWith("10.11.60.141") || host.StartsWith("alpha.lendingqb.com"))
                    {
                        ret = ServerLocation.Alpha;
                    }
                    else if (DataAccess.Tools.IsLocalServer(context.Request))
                    {
                        if (host == "11.12.13.130" || host == "172.30.26.130" || host == "scottkwrk2")
                        {
                            ret = GetServerLocationFromFileDbSiteCode();
                        }
                        else
                        {
                            ret = ServerLocation.Development;
                        }
                    }
                }
                else
                {
                    ret = GetServerLocationFromFileDbSiteCode();
                }

                return ret;
            }

        }

        /// <summary>
        /// Careful, this can be tampered with just by changing the fieldb site code in stage config.
        /// </summary>
        private static ServerLocation GetServerLocationFromFileDbSiteCode()
        {
            var ret = ServerLocation.Production;

            // 6/15/2011 dd - When a caller is call from a thread instead of IIS then use location of FileDB to determine server location.
            if (ConstStage.FileDBSiteCode == "LOBETA")
            {
                ret = ServerLocation.Development;
            }
            else if (ConstStage.FileDBSiteCode == "LODEMO")
            {
                ret = ServerLocation.Demo;
            }
            else if (ConstStage.FileDBSiteCode == "LOCLIENTBETA")
            {
                ret = ServerLocation.Beta;
            }
            else if (ConstStage.FileDBSiteCode == "LOALPHA")
            {
                return ServerLocation.Alpha;
            }
            else if (ConstStage.FileDBSiteCode == "LOREPL")
            {
                return ServerLocation.DevCopy;
            }
            

            return ret;
        }

        public static string ServerName
        {
            get
            {
                return Environment.MachineName;
            }
        }
        private static string x_currentDomainFriendlyName = null;
        public static string CurrentDomainFriendlyName
        {
            get
            {
                if (null == x_currentDomainFriendlyName)
                {
                    try
                    {
                        x_currentDomainFriendlyName = AppDomain.CurrentDomain.FriendlyName;
                    }
                    catch
                    {
                        x_currentDomainFriendlyName = "";
                    }
                }
                return x_currentDomainFriendlyName;
            }
        }

        //  Thien 5/25/2017: \xa9 is copyright character. With html, it can represent &copy;  
        // At this time, AspxTools.HtmlString( ) does not support " &copy; ". Therefore we need to use \xa9
        public static readonly string CopyrightMessage = "Copyright \xa9 2000-" + DateTime.Today.Year + " LendingQB. All rights reserved.";

        // OPM 244822, 8/25/2016, ML
        // Per MS TechNet, Exchange supports subjects with up
        // to 255 characters.
        public const int EmailSubjectMaxLength = 255;
        public const string LOXmlFormatVersion = "1.0"; 
        public const int CookieVersion = 28;

        public static readonly TimeSpan RoleChangeDelay = new TimeSpan(0, 0, 20); // 20 seconds.
        public static readonly TimeSpan TaskAlertDelay = new TimeSpan(0, 60, 0);
        public static readonly TimeSpan TaskAlertCheck = new TimeSpan(0, 0, 5);

        public const string PmlSaltFormat = "{mm}!{dd}:{hh}#";
        public const string PmlStrangeKey = "{39AA11E3-B916-406E-D44A896D7229}";
        public const int StageConfigExpirationSeconds = 300;// 7/27/2011 dd - Change to 5 minutes.

        public const int MaxViewableRecordsInPipeline = 250;

        public const string RuleGroupsXmlFile = "~/rule-groups.xml";

        public const int ThresholdSizeForCreateXmlDoc = 4 * 1024;
        public const string PdfMasterPassword = "1L5Uc1aRul3s";
        public static readonly Guid DummyPointServiceCompany = new Guid("398a8188-320e-49c0-9074-3a87c535a30c");
        public static readonly Guid DummyFannieMaeServiceCompany = new Guid("68E2E68C-42D8-4546-B016-5BF0A4A2565F");

        public const string ServiceFileDuplication = "Duplication";
        public const string ServiceFileOrder = "OrderFromCRA";

        public const string CustomPdfProtocolPrefix = "custompdf://";

        #region LPE
        public static readonly Guid LPE_NoProductRequestId = new Guid("11111111-1111-1111-1111-111111111111");
        public const int LPE_NumberOfMinutesBeforeMessageExpire = 3;
        #endregion

        public const string AuditEvent_PmlSubmission_Registered = "Loan Registration";
        public const string AuditEvent_PmlSubmission_RegisteredLock = "Loan Registration and Rate Lock";
        public const string AuditEvent_PmlSubmission_Lock = "Loan Rate Lock";

        public const string SendToEncompassPage = "sendtoencompass.aspx";

        #region Download File Unique Key
        public const string DownloadFile_Path = "~/common/download/";

        public const string DownloadFile_EprintV4_Updated = "HCL-LYNK-79MB";

        public const string DownloadFile_VPrinter = "H4W-DJ7P-EPWL";
        public const string DownloadFile_VPrinterForCitrix = "ASG-RXKY-L2DV";

        public const string DownloadFile_BaseRates = "MD4-JTPS-123H";
        public const string DownloadFile_Rates = "281A-JGHP-4W2G";

        public const string DownloadFile_BrowserXtInfo = "XHD-LGKX-TXMD";
        public const string DownloadFile_BrowserXtUninstallDirections = "T47-PX9W-CD69";
        public const string DownloadFile_BrowserXtUninstaller = "MLG-JEFA-C4LX";

        #endregion

        public const string FannieMae_MimeSeparator = "ieoau._._+2_8_GoodLuck8.3-ds0d0J0S0Kl234324jfLdsjfdAuaoei--";
        public const string FannieMae_SoftwareProviderCode = "VML11807W"; // 11/21/2007 dd - Provided by FannieMae on 11/21/2007

        // 10/7/2013 dd - Dodie @ FNMA asks us use the following provider code for DU XIS and EarlyCheck.
        public const string FannieMae_SoftwareProviderCode_DU_EarlyCheck = "VML11807X";
        public const string FannieMae_BetaServer = "https://direct.intgfanniemae.com/servicerequest2";
        public const string FannieMae_ProductionServer = "https://direct.efanniemae.com/servicerequest2";
        public const string FannieMae_TestDoUserId = "w2n7eonu";
        public const string FannieMae_TestDoPassword = "M3d-link";
        public const string FannieMae_TestDoUserId2 = "w2n7emxt";
        public const string FannieMae_TestDuUserId = "w2n7ewnt";
        public const string FannieMae_Dweb_DuProductionServer = "https://desktopunderwriter.fanniemae.com/Workflow/DI/ServiceRouter.aspx";
        public const string FannieMae_Dweb_DoProductionServer = "https://desktoporiginator.fanniemae.com/Workflow/DI/ServiceRouter.aspx";
        public const string FannieMae_Dweb_DuBetaServer = "https://desktopunderwriter.intgfanniemae.com/Workflow/DI/ServiceRouter.aspx";
        public const string FannieMae_Dweb_DoBetaServer = "https://desktoporiginator.intgfanniemae.com/Workflow/DI/ServiceRouter.aspx";
        public const string FannieMae_SpecialFeatureCode_DURefiPlus = "147";

        public const string OnlineDocuments_TestCustomerCode = "7318";
        public const string OnlineDocuments_TestUserName = "davidd";
        public const string OnlineDocuments_TestPassword = "1nt3rf@c3";
        public const int OnlineDocuments_TestScenarioID = 52;
        public const string OnlineDocuments_ProductionUrl = "https://www.onlineexpressweb.com/mismoimport/mismoimportwebservice.asmx";
        public const string OnlineDocuments_TestUrl = "https://test.onlinedocuments.com/mismoimport/mismoimportwebservice.asmx";

        public const string DocuTech_ProductionUrl = "https://www.conformx.com/ws/enginews.asmx";
        public const string DocuTech_TestUrl = "https://stage.conformx.com/cx4/cxws/enginews.asmx";
        public const string DocuTech_TestUserAccount = "PRMLadmin";
        public const string DocuTech_TestPassword = "docutech2";
        public const string DocuTech_Temp_GFE_Redisclosure_PackageID = "Temp_DocuTech_GFE_Redisclosure_PACKAGEID";

        //Test accounts other than "PRMLadmin" added for case 70087
        public static readonly string[] DocuTech_TestUserAccounts = {
                                                                      "PRMLadmin"                                                                    
                                                                    };

        public static readonly string[] DocuTech_TestLQBLogins = {
                                                                        "lisa2"
                                                                   };

        public const string BlockedRateLockSubmission_ExpiredMessage = "EXPIRED"; // Eventually becomes ErrorMessages.BlockedRateLockSubmission_NormalUserExpiredMessage or ErrorMessages.BlockedRateLockSubmission_LockDeskExpiredMessage
        public const string BlockedRateLockSubmission_CutOffMessage = "CUTOFF::"; // -> ErrorMessages.BlockedRateLockSubmission_NormalUserCutOffMessageFormat or ErrorMessages.BlockedRateLockSubmission_LockDeskCutOffMessageFormat
        public const string BlockedRateLockSubmission_OutsideNormalHour = "HOUR::"; // -> ErrorMessages.BlockedRateLockSubmission_OutsideNormalHourFormat
        public const string BlockedRateLockSubmission_OutsideClosureDayHour = "CLOSURE_DAY_HOUR::"; // -> ErrorMessages.BlockedRateLockSubmission_OutsideClosureDayHourFormat
        public const string BlockedRateLockSubmission_OutsideNormalHourAndPassInvestorCutoff = "HOUR_CUTOFF::"; // -> ErrorMessages.BlockedRateLockSubmission_OutsideNormalHourAndPassInvestorCutoffFormat
        public const string BlockedRateLockSubmission_LenderManual = "LENDER_MANUAL::"; // -> ErrorMessages.BlockedRateLockSubmission_NormalUserExpiredMessage

        public const string LoanProspector_SoftwareVersion = "WW4.2.02"; //"WW4.2.01"; //"2WWX4.1";
        public const string LoanProspector_XmlSoftwareVersion = "WW4.2.02";
        public const string LoanProductAdvisor_SoftwareVersion = "WW4.3.00";
        public const string LoanProductAdvisor_XmlSoftwareVersion = "WW4.3.00";
        public const string LoanProspector_Sys2SysVersion = "S4.2.01";
        public const string LoanProspector_PendingLoanTransaction = "Pending";
        public const string LoanProspector_VendorIdentifier = "000184";
        public const string LoanProspector_EntryPoint_GoToMain = "h";
        public const string LoanProspector_EntryPoint_SendLp = "s";
        public const string LoanProspector_EntryPoint_ViewFeedback = "v";
        public const string ContextItemKey_PerformanceMonitorItem = "PerformanceMonitorItem";
        public const string PmlMortgageLeagueLogin = "GML"; // 1/17/2005 dd - Generic PML account for MortgageLeague
        public const string PmlQuickPricerAnonymousLogin = "QuickPricer";
        public const string PmlQuickPricerAnonymousPassword = "Qu1ckPr1cer";
        public const string AnonymousQpCookieName = "_ANONQP";

        public const string ComplianceEase_BetaUrl = "https://uat.complianceease.com/ca/services/2015/03/ComplianceAuditService";
        public const string ComplianceEase_ProductionUrl = "https://www.complianceease.com/ca/services/2015/03/ComplianceAuditService";
        
        /// <summary>
        /// Used in conjunction with ConstStage.ComplianceEaseLoggingEvents to determine which ComplianceEase early-exit events should be logged.
        /// </summary>
        public const string ComplianceEaseEarlyExitEvent_ExpectedVersionMismatch = "ExpectedVersionMismatch";
        public const string ComplianceEaseEarlyExitEvent_IsTemplate = "IsTemplate";
        public const string ComplianceEaseEarlyExitEvent_IsIgnoreLoanStatus = "IsIgnoreLoanStatus";
        public const string ComplianceEaseEarlyExitEvent_IsChecksumValueIdentical = "IsChecksumValueIdentical";
        public const string ComplianceEaseEarlyExitEvent_InvalidInitialLoanEstimate = "InvalidInitialLoanEstimate";

        /// <summary>
        /// Note, if you change this, you need to add it to E_FileDBKeyType. (Don't just update the Normal_Loan_COMPLIANCE_EAGLE_REPORT_PDF) <para></para>
        /// And update all the associated switches.
        /// </summary>
        public const string ComplianceEagle_Report_Tag = "_COMPLIANCE_EAGLE_REPORT_PDF";

        public const int PmlCompanyListPagingSize = 100; //08/10/09 fs - Number of PML companies to be shown on each page.

        public const string EmailRegexValidator = "^\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$"; //fs 08/18/09

        public const string FakePasswordDisplay = "******";

        public const string DocuTech_SystemDocumentIdentifier = "LendersOffice";

        public const string HttpContextItem_IsServicePageKey = "LO_IsServicePage";

        public static readonly string[] NmlsCallReportRequiredColumns = {
                                                             "sOpenedD",
                                                             "sBranchChannelT",
                                                             "sFinalLAmt", 
                                                             "sHmdaActionD",
                                                             "sHmdaActionTaken",
                                                             "sLT",
                                                             "sHmdaPropT",
                                                             "sLPurposeT",
                                                             "sHmdaReportAsHomeImprov",
                                                             "sHmdaReportAsHoepaLoan",
                                                             "sLienPosT",
                                                             "sHmdaLienT",
                                                             "sBrokerFeesCollected",
                                                             "sLenderFeesCollected",
                                                             "sSpState",
                                                             "sApp1003InterviewerLoanOriginatorIdentifier",
                                                             "sNMLSApplicationAmount"
                                                         };

        public static readonly string[] NMLSCallReportExpandedColumns = {
                                                             "sLpTemplateNm",
                                                             "sFinMethT", 
                                                             "sMortgageLoanT",
                                                             "sJumboT",
                                                             "sDocumentationT",
                                                             "sIOnlyMon",
                                                             "sIsOptionArm",
                                                             "sHasPrepaymentPenalty",
                                                             "sNMLSLoanPurposeT",
                                                             "sOccT",
                                                             "sHasPrivateMortgageInsurance",
                                                             "sHasPiggybackFinancing",
                                                             "sCreditScoreLpeQual",
                                                             "sNMLSLtvR",
                                                             "sNMLSCLtvR",
                                                             "sPurchaseAdviceSummaryServicingStatus",
                                                             "sDaysInWarehouse",
                                                             "sNoteIR"
                                                         };

        public static readonly string[] NMLSCallReportVersion4StandardColumns = 
        {
            "sQMStatusT",
            "sNMLSServicingIntentT",
            "sServicingByUsEndD",
            "sServicingByUsStartD",
            "sNMLSServicingTransferInD",
            "sNMLSServicingTransferOutD",
            "sNMLSDaysDelinquentAsOfPeriodEnd",
            "sNMLSPeriodForDaysDelinquentT",
            "sGLServTransEffD",
            "sLoanSaleDispositionT",
            "sNMLSNetChangeApplicationAmount"
        };

        public static readonly string[] NMLSCallReportVersion5StandardColumns =
        {
            "sNmlsApplicationDate",
            "sIsExemptFromAtr"
        };

        /// <summary>
        /// This string's presence in the LPE Update BotType marks bots using the live pricing feature implemented in OPM 184983.<para />
        /// Comparison to this should be case insensitive.
        /// </summary>
        public static readonly string LPLiveBotFlag = "(LP_Live)";
	}
}
