using System;

namespace LendersOffice.Constants
{
	public class ConstAppMatthew
	{
		// 01/15/08 mf. OPM 19604: we want to keep debug code off production PML.
		// I replaced the debug operations from the common.js file with
		// calls to pml_action function here. We also only serve this script 
		// block when the application is running in debug mode.

		public static string PmlLocalDebugScript =
			@"
<script language=javascript>
<!--

var gIsDebug = false;

function pml_action(action, arg)
{
  switch (action)
  {
    case 'populateForm':
      if (gIsDebug)
        alert('Time executed in populateForm is ' + (arg[1] - arg[0]) + ' ms. Count = ' + arg[2] + ', Fields count=' + arg[3]);
      break;
    case 'getAllFormValues':
      if (gIsDebug)
        alert('Time executed in getAllFormValues is ' + (arg[1] - arg[0]) + ' ms. Count = ' + arg[2]);
        break;
    case 'highlightField':
      if (gIsDebug || self.location.host == 'localhost') 
        window.status = 'Field ID: ' + arg;
      break;
    case 'event_onkeyup':
      debug_event_onkeyup();
      break;
    case 'buildXmlRequest':
      if (gIsDebug) alert('<request><data ' + arg + '/></request>');
      break;
    case 'f_call_response':
      if (gIsDebug) alert(arg);
      break;
    case 'f_call_report':
      if( gIsDebug ) debugObject(arg);
      break;
  }
}

function debug_event_onkeyup()
{
    
  // Allow us to press Ctrl + Alt + S to save the source file of the current window.
  // This will help us to debug the source on modal dialog. To disable the save just
  // comment out the code.
  
  //alert('Ctrl = ' + event.ctrlKey + ', Alt = ' + event.altKey + ', keycode = ' + event.keyCode);
  if (event.ctrlKey && event.altKey && event.keyCode == 83) {
    window.document.execCommand('SaveAs', true);
  } else if (event.ctrlKey && event.altKey && event.keyCode == 86) {
     // Ctrl + Alt + V - Display viewstate size.
     if (window.document.all.__VIEWSTATE != null) {
       alert('ViewState size = ' + window.document.all.__VIEWSTATE.value.length);
     } else 
       alert('No viewstate available');
  } else if (event.ctrlKey && event.altKey && event.keyCode == 82) {
    // Ctrl + Alt + R - Resize window to 800x600. Unfortunately this will force the page to reload.
      parent.resizeTo(800,600);
  } else if (event.ctrlKey && event.altKey && event.keyCode == 68) {
    // Ctrl + Alt + D - Toggle gIsDebug
    gIsDebug = !gIsDebug;
    if (gIsDebug)
      displayObnoxiousColor();
    alert('Debug for this frame is ' + gIsDebug);
  }
}

function debugObject(o) {
  var str = '';
  var i = 0;
  for (p in o)
    if (p != 'outerText' && p != 'innerHTML' && p != 'innerText' && p!= 'outerHTML')
      str += 'o.' + p + ' = ' + o[p] + (i++ % 10 == 0 ? '\n\r' : ';');
  alert(str);
}  

//-->
</script>
";
	}
}