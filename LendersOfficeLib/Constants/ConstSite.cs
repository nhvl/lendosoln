namespace LendersOffice.Constants
{
    using System;
    using System.Web;
    using LendersOffice.Common;

    /// <summary>
    /// Only constants stored in .config file store here.
    /// This gets values from Web.config and site.config. The site.config file overrides Web.config.
    /// </summary>
    /// <remarks>
    /// The first remark is that the above summary was already out of date prior to the introduction of the useNewDrivers flag.
    /// The second remark is:
    /// THERE ARE MANY LOCATIONS WHERE STAGE CALLS CASCADE INTO SITE CALLS.  IT IS CRUCIALLY IMPORTANT THAT CALLS NEVER GO THE OTHER WAY
    /// AS THAT MAY CREATE DEADLOCKS BECAUSE BOTH SYSTEMS ARE CACHED AND PERIODICALLY RELOADED.
    /// </remarks>
    public class ConstSite
	{
 
        public ConstSite()
        {
            // This is here explicitely as a reminder that there are locations where an instance is constructed even though everything in this class is static.
        }

        /// <summary>
        /// Gets a value for the path to the folder containing the 
        /// <seealso cref="DataAccess.CFieldInfoTable"/> cache file.
        /// </summary>
        public static string FieldInfoCacheFilePath
        {
            get { return GetString(SiteConfigDatum.FieldInfoCacheFilePath, string.Empty); }
        }

        public static int MaxNumberOfProductsInParallelTask
        {
            get { return GetInt(SiteConfigDatum.MaxNumberOfProductsInParallelTask, 20); }
        }

        public static string TempPriceEngineStorageLocalPath
        {
            get
            {
                return GetString(SiteConfigDatum.TempPriceEngineStorageLocalPath, string.Empty);
            }
        }

        public static string PriceEngineStorageMode
        {
            get
            {
                return GetString(SiteConfigDatum.PriceEngineStorageMode, string.Empty);
            }
        }

		public static bool EnableRedirectToCustomFrame
		{
			get
			{
                return GetBool(SiteConfigDatum.EnableRedirectToCustomFrame, false);
			}
		}

		public static string MsMqLogConnectStr
		{
			get
			{
                return GetString(SiteConfigDatum.MsMqLogConnectStr, string.Empty);
			}
		}

		public static string MsMqPbLogConnectStr
		{
			get
			{
                return GetString(SiteConfigDatum.MsMqPbLogConnectStr, string.Empty);
			}
		}

		public static string MSMQ_ElasticSearch
		{
			get
			{
                return GetString(SiteConfigDatum.MSMQ_ElasticSearch, string.Empty);
			}
		}

		public static string MSMQ_IISLog
		{
			get
			{
                return GetString(SiteConfigDatum.MSMQ_IISLog, string.Empty);
			}
		}

		/// <summary>
		/// 7/15/2015 - To avoid using too much of memory to log, we are no longer log large request/response to log.
		/// To enable we have to turn it on server by server. The reason we use as ConstSite instead of ConstStage because we want to see
		/// if the memory saving is worth it.
		/// </summary>
		public static bool EnableLogFor3rdParty
		{
			get
			{
                return GetBool(SiteConfigDatum.EnableLogFor3rdParty, true);
			}
		}

		public static string LoginRedirectUrl
		{
			get
			{
                return GetString(SiteConfigDatum.LoginRedirectUrl, string.Empty);
			}
		}

		public static string EDocSignerAuthUrl
		{
			get
			{
                return GetString(SiteConfigDatum.EDocSignerAuthUrl, string.Empty);
			}
		}

		public static string ErrorEmailAddressRecipient
		{
			get
			{
				const string defaultValue = "lpeopm@pricemyloan.com";

                return GetString(SiteConfigDatum.ErrorEmailAddressRecipient, defaultValue);
			}
		}
		public static string SchemaMapErrorRecipient
		{
			get
			{
				const string defaultValue = "schemamaperrors@pricemyloan.com";

                return GetString(SiteConfigDatum.SchemaMapErrorRecipient, defaultValue);
			}
		}
		public static string WebBotDownloadErrorRecipient
		{
			get
			{
				const string defaultValue = "ratedownloaderrors@pricemyloan.com";

                return GetString(SiteConfigDatum.WebBotDownloadErrorRecipient, defaultValue);
			}
		}
		public static string PmlRatesEmailAddress
		{
			get
			{
				const string defaultValue = "pmlrates@meridianlink.com";

                return GetString(SiteConfigDatum.PmlRatesEmailAddress, defaultValue);
			}
		}
		public static int ErrorMessageDuplicationIntervalInMinutes
		{
			get
			{
				const int defaultValue = 30;

                return GetInt(SiteConfigDatum.ErrorMessageDuplicationIntervalInMinutes, defaultValue);
			}
		}

		public static string StandardPdfLocation
		{
			get
			{
                return GetString(SiteConfigDatum.StandardPdfLocation);
			}
		}
		public static string PdfRasterizerHost
		{
			get
			{
                return GetString(SiteConfigDatum.PdfRasterizerHost, string.Empty);
			}
		}

		// ADDED DEFAULT CLAUSE
		public static string PmlBaseUrl
		{
			get
			{
                return GetString(SiteConfigDatum.PmlBaseUrl, string.Empty);
			}
		}

        public static string CustomResourceLocation
        {
            get
            {
                return GetString(SiteConfigDatum.CustomResourceLocation, "~/pml_custom");
            }
        }

        public static string GenericResourceLocation
        {
            get
            {
                return GetString(SiteConfigDatum.GenericResourceLocation, "~/pml_shared");
            }
        }

        public static string LoginUrl
		{
			get
			{
                return GetString(SiteConfigDatum.LoginUrl);
			}
		}

		public static bool LimitUserSearchResults
		{
			get
			{
                return GetBool(SiteConfigDatum.LimitUserSearchResults, true);
			}
		}

		public static string LpeReleaseControllerUrl
		{
			get
			{
                return GetString(SiteConfigDatum.LpeReleaseControllerUrl, string.Empty);
			}
		}

        public static string ConversationLogIP
        {
            get
            {
                return GetString(SiteConfigDatum.ConversationLogIP, string.Empty);
            }
        }

        public static int ConversationLogPort
        {
            get
            {
                const int defaultValue = 0;
                return GetInt(SiteConfigDatum.ConversationLogPort, defaultValue);
            }
        }

        public static int UnderwritingTaskThreadCount
		{
			get
			{
				const int defaultValue = 1;

                return GetInt(SiteConfigDatum.UnderwritingTaskThreadCount, defaultValue);
			}
		}

        /// <summary>
        /// Gets the configured server type. Do not use directly, used DistributeUnderwritingSettings.ServerT. 
        /// </summary>
		public static E_ServerT UnderwritingServerType
		{
			get
			{
				string serverT = GetString(SiteConfigDatum.UnderwritingServerType, string.Empty);

				switch (serverT.TrimWhitespaceAndBOM().ToUpper())
				{
					case "STANDALONE": return E_ServerT.StandAlone;
					case "CALCONLY": return E_ServerT.CalcOnly;
					case "REQUESTONLY": return E_ServerT.RequestOnly;
					case "DEVMINIMUM": return E_ServerT.DevMinimum;
					case "AUTHORTEST": return E_ServerT.AuthorTest;
					default:
						throw new GenericUserErrorMessageException("Unhandled underwriting server type:  serverT = " + serverT);
				}
			}
		}

		public static bool IsUsingDBSnapshotForLpe
		{
			get
			{
                return GetBool(SiteConfigDatum.IsUsingDBSnapshotForLpe, false);
			}
		}

		public static bool DisplayPolicyAndRuleIdOnLpeResult
		{
			get
			{
                return GetBool(SiteConfigDatum.DisplayPolicyAndRuleIdOnLpeResult, false);
			}
		}

		public static int LpeLoadDelayXSeconds
		{
			get
			{
				const int defaultValue = 0;

                return GetInt(SiteConfigDatum.LpeLoadDelayXSeconds, defaultValue);
			}
		}

		public static int MinLpeMemory
		{
			get
			{
				const int defaultValue = 100;

                return GetInt(SiteConfigDatum.MinLpeMemory, defaultValue);
			}
		}

		public static int MaxLpeMemory
		{
			get
			{
				const int defaultValue = 150;

                return GetInt(SiteConfigDatum.MaxLpeMemory, defaultValue);
			}
		}

		public static int MinLpePrograms
		{
			get
			{
				const int defaultValue = 5000;

                return GetInt(SiteConfigDatum.MinLpePrograms, defaultValue);
			}
		}

		public static int MaxLpePrograms
		{
			get
			{
				const int defaultValue = 6000;

                return GetInt(SiteConfigDatum.MaxLpePrograms, defaultValue);
			}
		}

		public static int GetLoanProgramSetTimeOut
		{
			get
			{
				const int defaultValue = 25;

                return GetInt(SiteConfigDatum.GetLoanProgramSetTimeOut, defaultValue);
			}
		}

		public static bool LpeGetTotalMemory
		{
			get
			{
                return GetBool(SiteConfigDatum.LpeGetTotalMemory, true);
			}
		}

		public static bool LpeDisplayDetail
		{
			get
			{
                return GetBool(SiteConfigDatum.LpeDisplayDetail, false);
			}
		}

		public static bool EmailIfDetectLpeReleaseError
		{
			get
			{
                return GetBool(SiteConfigDatum.EmailIfDetectLpeReleaseError, false);
			}
		}

		public static bool VerboseLogEnable4Pricing
		{
			get
			{
                return GetBool(SiteConfigDatum.VerboseLogEnable4Pricing, false);
			}
		}

		public static bool IsSecureCookieRequired
		{
			get
			{
                return GetBool(SiteConfigDatum.IsSecureCookieRequired, false);
			}
		}

		public static string TestDataFolder
		{
			get
			{
                return GetString(SiteConfigDatum.TestDataFolder, string.Empty);
			}
		}
		[Constant(ActualKey = "FILES_PATH")]
		public static string TestRsMapDataFolder
		{
			get
			{
                return GetString(SiteConfigDatum.TestRsMapDataFolder, string.Empty);
			}
		}

		/// <summary>
		/// Gets the LendingQB authentication cookie name. Keep default in sync
		/// with LendersOfficeApp Web.Config or EmbeddedPml will break. Only override
		/// if you are changing the forms name in the web.config (need to do this to let 
		/// people log into both beta and secure at the same time).
		/// </summary>
		public static string LqbAuthCookieName
		{
			get
			{
				const string defaultValue = "_LENDINGQBUSER";

                return GetString(SiteConfigDatum.LqbAuthCookieName, defaultValue);
			}
		}

		//TimeToLive4NewKeywordInSecond
		public static int TimeToLiveInSecond4NewKeyword
		{
			get
			{
				const int defaultValue = 10 * 60;

                return GetInt(SiteConfigDatum.TimeToLiveInSecond4NewKeyword, defaultValue);
			}
		}

		// ADDED DEFAULT CLAUSE
		public static string LendOSolnTestDll
		{
			get
			{
                return GetString(SiteConfigDatum.LendOSolnTestDll, string.Empty);
			}
		}

		public static string EDocsSite
		{
			get
			{
                return GetString(SiteConfigDatum.EDocsSite, string.Empty);
			}
		}
		public static string LOSite
		{
			get
			{
                return GetString(SiteConfigDatum.LOSite, string.Empty);
			}
		}

		public static string ExportXsltFilePath
		{
			get
			{
                return GetString(SiteConfigDatum.ExportXsltFilePath, string.Empty);
			}
		}
		/// <summary>
		/// If the login cookie is shared between subdomains,
		/// this will allow links between subdomains to work.
		/// Otherwise, the link should gracefully degrade to
		/// use the main site.
		/// </summary>
		public static bool ShareLoginCookieWithSubdomain
		{
			get
			{
                return GetBool(SiteConfigDatum.ShareLoginCookieWithSubdomain, false);
			}
		}

		public static string CookieDomain
		{
			get
			{
                return GetString(SiteConfigDatum.CookieDomain, string.Empty);
			}
		}

		public static bool IsTaskMigrationProcess
		{
			get
			{
                return GetBool(SiteConfigDatum.IsTaskMigrationProcess, false);
			}
		}

		public static bool IsDevMachine
		{
			get
			{
                return GetBool(SiteConfigDatum.IsDevMachine, false);
			}
		}

		/// <summary>
		/// Machine name used when rendered to client.
		/// Per OPM 246520, it is insecure to allow the client this information.
		/// If not set in site config, default to generic name.
		/// </summary>
		public static string MachineDisplayName
		{
			get { return GetString(SiteConfigDatum.MachineDisplayName, "WEBSERVER"); }
		}

		public static string TableReplicateServiceUrl
		{
			get
			{
                return GetString(SiteConfigDatum.TableReplicateServiceUrl, string.Empty);
			}
		}

		// FilebasedCmp Config

		// Note: want to create the constant ConstSite.PricingVerboseLogEnabled, however ConstSite.VerboseLogEnable4Pricing already used for loading lpe data's log.
		// Therefore I create the following constant.
		internal static bool PricingCompactLogEnabled
		{
			get
			{
				
				return false;
			}
		}


		internal static bool EnableFilebasedCmp
		{
			get
			{
                // opm 471349: Improve pricing execution by return static value for ConstSite.EnableFilebasedCmp
                // We don't use "Filebased Compare" anymore and will remove this constant. 
                // For simple hotfix, we return false for this constant.
                return false;
			}
		}

		static internal string FilebasedFolder
		{
			get
			{
				return @"C:\SnapshotFolder\";
			}
		}

		internal static RatePrice.E_LoanProgramSetPricingT RateOptionHistory
		{
			get
			{
                bool isRateHistory = GetBool(SiteConfigDatum.RateOptionHistory, true);

				return isRateHistory ? RatePrice.E_LoanProgramSetPricingT.Historical : RatePrice.E_LoanProgramSetPricingT.Snapshot;
			}
		}

		internal static RatePrice.E_LoanProgramSetPricingT LoanProgramTemplateHistory
		{
			get
			{
                bool isLoanHistory = GetBool(SiteConfigDatum.LoanProgramTemplateHistory, true);

				return isLoanHistory ? RatePrice.E_LoanProgramSetPricingT.Historical : RatePrice.E_LoanProgramSetPricingT.Snapshot;
			}
		}

		internal static bool ManualCompareEnabled
		{
			get
			{
                return GetBool(SiteConfigDatum.ManualCompareEnabled, false);
			}
		}

		internal static string FilebasedDeveloperEmail
		{
			get
			{
                return GetString(SiteConfigDatum.FilebasedDeveloperEmail, string.Empty);
			}
		}

        internal static string PmlCookieName
        {
            get
            {
                return GetString(SiteConfigDatum.PmlCookieName, string.Empty);
            }
        }

        internal static bool EnablePerLenderSnapshot
        {
            get
            {
                return GetBool(SiteConfigDatum.EnablePerLenderSnapshot, false);
            }
        }

        /// <summary>
        /// Gets a string of comma separated ints used to determining which Background Jobs to process.
        /// </summary>
        public static string BackgroundJobTypeFilter
        {
            get { return GetString(SiteConfigDatum.BackgroundJobTypeFilter, null); }
        }

        internal static bool EnablePerLenderActualSnapshot
        {
            get
            {
                return GetBool(SiteConfigDatum.EnablePerLenderActualSnapshot, false);
            }
        }


        #region Utilities methods

        private static string GetString(SiteConfigValue datum)
		{
			return GetStringImpl(datum, string.Empty, true);
		}

		private static string GetString(SiteConfigValue datum, string defaultValue)
		{
			return GetStringImpl(datum, defaultValue, false);
		}

		private static string GetStringImpl(SiteConfigValue datum, string defaultValue, bool throwException)
		{
			string ret = defaultValue;
			bool isFound = false;

			if (datum.KeyName.Equals("SecureDBConnStrReadOnly", StringComparison.OrdinalIgnoreCase))
			{
				string value = datum.GetStringValue();
				if (value != null)
				{
					isFound = true;
					ret = value;
				}
			}
			else
			{
				string applicationId = GetApplicationID();
				if (!string.IsNullOrEmpty(applicationId))
				{
					string value = datum.GetStringValue(applicationId);
					if (value != null)
					{
						isFound = true;
						ret = value;
					}
				}

				if (!isFound)
				{
					string value = datum.GetStringValue();
					if (value != null)
					{
						isFound = true;
						ret = value;
					}
				}
			}

			if (!isFound && throwException)
			{
				throw new GenericUserErrorMessageException(datum.KeyName + " is not defined in config file.");
			}

			return ret;
		}

		private static string GetApplicationID()
		{
			string applicationId = string.Empty;
			if (HttpContext.Current != null)
			{
				try
				{
					HttpRequest httpRequest = HttpContext.Current.Request;

					applicationId = httpRequest.Url.Host;
				}
				catch (HttpException)
				{
					// 4/30/2015 dd - This happen when a ConstSite is try to access during Application_Start.
				}
			}

			return applicationId;
		}

		private static int GetInt(SiteConfigValue datum, int defaultValue)
		{
			int value = defaultValue;

			string ret = GetString(datum, string.Empty);

			if (!string.IsNullOrEmpty(ret))
			{
				if (!int.TryParse(ret, out value))
				{
					// 4/30/2015 dd - Use defaultValue when unable to parse.
					value = defaultValue;
				}
			}

			return value;
		}

		private static bool GetBool(SiteConfigValue datum, bool? defaultValue = null)
		{
			string ret = defaultValue.HasValue ? GetString(datum, string.Empty) : GetString(datum) ;

			switch (ret.ToLower())
			{
				case "true": return true;
				case "false": return false;
				default:
                    if (defaultValue.HasValue)
                    {
                        return defaultValue.Value;
                    }

					throw new GenericUserErrorMessageException(datum.KeyName + " defined in config file is not a valid bool value ['True', 'False'].  Value = " + ret);

			}
		}
		#endregion
	}
}