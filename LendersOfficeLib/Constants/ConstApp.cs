namespace LendersOffice.Constants
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.IO;
    using System.Text.RegularExpressions;
    using System.Web.UI;
    using System.Web.UI.HtmlControls;
    using CommonLib;
    using DataAccess;
    using LendersOffice.Security;

    public partial class ConstApp
	{
        /// <summary>
        /// The name of cookie that we use to track unique id of a client browser through *.lendingqb.com.
        /// DO NOT modified this value because other AWS Edocs project is rely on this name.
        /// </summary>
        public const string CookieFingerPrint = "_fp";

        /// <summary>
        /// Represents the smallest positive Decimal value greater than zero.
        /// </summary>
        /// <remarks>
        /// This field is primarily intended for use with rounding and truncation
        /// calculations for decimals to avoid floating point errors.
        /// </remarks>
        public static readonly decimal DecimalEpsilon = new decimal(1, 0, 0, isNegative: false, scale: 28);

        /// <summary>
        /// Represents the date when LendingQB will no longer allow users
        /// to log in if BrowserXT is detected on their work station.
        /// </summary>
        public static readonly DateTime BrowserXtVerificationHardStopDate = new DateTime(2018, 9, 19);

        /// <summary>
        /// Represents the name of the query string variable indicating
        /// that PDF generation should be completed synchronously.
        /// </summary>
        public const string SynchronousPdfFallbackQueryStringParameterName = "submitPdfSynchronous";

        /// <summary>
        /// The name of the cookie stored when an internal user 
        /// logs in to the TPO portal.
        /// </summary>
        /// <remarks>
        /// This cookie is used by the TPO logout logic to route 
        /// the internal user to the correct session expired page.
        /// </remarks>
        public const string InternalTpoCookieName = "__ITPO";

        public const int LargeLogInBytesThreshold = 75000;
        public const int MaxSqlCommandTextCharacters = 100;

        public const string FhaLenderIdForNoSponsoredOriginatorEIN = "6999609996";
        public const int LoanRecentModificationWarningPeriodInMinutes = 30;
        public const int LoanRecentMOdificationPendingDirtyCheckInMinutes = 5;

        public const string TempFolder                       = LqbGrammar.Utils.TempFileUtils.TempFolder;
        
        /// <summary>
        /// The folder where digital certificates are stored.
        /// </summary>
        public const string CertificateFolder = @"C:\LendersOffice\Cert";
        
        public const string CredcoCertificateLocation        = @"C:\LendersOffice\Cert\credco.cer";
        public const string KrollFactualDataCertificateLocation = @"C:\LendersOffice\Cert\KrollFactualData.cer";

        public static readonly TimeSpan MessageQueueTimeout  = new TimeSpan(0, 0, 1);

        public const int ExceptionCountForCriticalCase = 100;

        // 6/18/2004 dd - Change default value from 5 hrs to 4 hrs for following reason.
        // From pipeline Kirk's task monitor will generate the first hour of activity before turn itself off.
        // After the first hour, user will have extra 4 hrs of inactivity before cookie expire.
        public const int LOCookieExpiresInMinutes            = 240; // 4 hrs.

        public const int GenericFrameworkTicketExpiresInMinutes = 30;

		public const string LinkpointStoreId                 = "1005188" ;

		public const int LifeOfTempPdfFilesInSeconds         = 600; // 10 min.  // Also note that currently this is for ALL files in Lotemp, not just PDFs anymore

		//constants used for the BaseException.cs
		//in relation to these 2 variables, it defines an escalation as 5 consecutive errors within 15minutes
		public const int TimeBetweenErrorThreshold			 = 15; //15minutes, this is the time limit to store the errors in the static arraylist
		public const int IncreaseErrorLevelThreshold		 = 5; //5 times, number of repetition of errors, before it increases its notify method
		public const string DefaultErrorEmailFromAddress	 = "mclerror@meridianlink.com";
        
		public const int CPTempPasswordValidLengthInDays     = 7;
        public const string DefaultaPresTotHExpDesc = "Add. Housing Expense";

        public const int SendingBufferLength = 4096;

        /// <summary>
        /// Represents the code used to report "Not Applicable" to HMDA for multi-family units.
        /// </summary>
        /// <remarks>
        /// As discussed with MF in OPM 423246, we decided to go with a sentinel value instead of
        /// null to represent "Not Applicable" in the database.
        /// </remarks>
        public const int HmdaMultiFamilyUnitsNotApplicableValue = -1;

        /// <summary>
        /// Represents the action taken by a lender to purchase a loan, for use in certain HMDA field calculations.
        /// </summary>
        public const string HmdaPurchasedActionTaken = "Loan purchased by your institution";

        /// <summary>
        /// Represents the action taken by a lender to close a loan, for use in certain HMDA field calculations.
        /// </summary>
        public const string HmdaFileClosedForIncompleteness = "File Closed for Incompleteness";

        /// <summary>
        /// Represents the action taken by a borrower to withdrawn an application, for use in certain HMDA field calculations.
        /// </summary>
        public const string HmdaApplicationWithdrawnByApplicant = "Application withdrawn";

        /// <summary>
        /// Represents the action taken by a lender to deny an application.
        /// </summary>
        public const string HmdaApplicationDenied = "Application denied";

        /// <summary>
        /// Represents the action taken by a lender to deny a pre-approval.
        /// </summary>
        public const string HmdaPreapprovalDenied = "Preapproval request denied";

        /// <summary>
        /// Represents the minimum value for the "System ID" of a 
        /// contact entry.
        /// </summary>
        public const int MinimumContactSystemId = 1;

        /// <summary>
        /// Represents the minimum value for the "Branch ID Number" of a 
        /// branch.
        /// </summary>
        public const int MinimumBranchIdNumber = 1;

        /// <summary>
        /// Represents the maximum number of columns supported for
        /// a SELECT statement.
        /// </summary>
        public const int MaxSqlQueryColumns = 4096;

        // 3/12/2015 dd - Moved from ConstAppDiana.cs
        public const string LoUserCCodeOverride = "LENDERSOFFICE";

		#region Role Name 
        public const string ROLE_UNDERWRITER                 = "Underwriter";
        public const string ROLE_ADMINISTRATOR               = "Administrator";
        public const string ROLE_LOAN_OPENER                 = "LoanOpener";
        public const string ROLE_LOCK_DESK                   = "LockDesk";
        public const string ROLE_CALL_CENTER_AGENT           = "Telemarketer";
        public const string ROLE_PROCESSOR                   = "Processor";
        public const string ROLE_ACCOUNTANT                  = "Accountant";
        public const string ROLE_MANAGER                     = "Manager";
        public const string ROLE_LOAN_OFFICER                = "Agent";
        public const string ROLE_REAL_ESTATE_AGENT           = "RealEstateAgent";
        public const string ROLE_LENDER_ACCOUNT_EXEC         = "LenderAccountExec";
        public const string ROLE_FUNDER                      = "Funder";
        public const string ROLE_CLOSER                      = "Closer";
        public const string ROLE_CONSUMER                    = "Consumer"; // Consumer Portal project
        public const string ROLE_SHIPPER                     = "Shipper"; // EDocs project
        public const string ROLE_BROKERPROCESSOR             = "BrokerProcessor"; // Workflow project
        public const string ROLE_POSTCLOSER                  = "PostCloser"; // OPM 108148 gf start
        public const string ROLE_INSURING                    = "Insuring";
        public const string ROLE_COLLATERALAGENT             = "CollateralAgent";
        public const string ROLE_DOCDRAWER                   = "DocDrawer"; // OPM 108148 gf end
        public const string ROLE_CREDITAUDITOR               = "CreditAuditor"; // 11/19/2013 gf - OPM 145015 start
        public const string ROLE_DISCLOSUREDESK              = "DisclosureDesk";
        public const string ROLE_JUNIORPROCESSOR             = "JuniorProcessor";
        public const string ROLE_JUNIORUNDERWRITER           = "JuniorUnderwriter";
        public const string ROLE_LEGALAUDITOR                = "LegalAuditor";
        public const string ROLE_LOANOFFICERASSISTANT        = "LoanOfficerAssistant";
        public const string ROLE_PURCHASER                   = "Purchaser";
        public const string ROLE_QCCOMPLIANCE                = "QCCompliance";
        public const string ROLE_SECONDARY                   = "Secondary";
        public const string ROLE_SERVICING                   = "Servicing"; // 11/19/2013 gf - OPM 145015 end
        public const string ROLE_EXTERNAL_SECONDARY          = "ExternalSecondary";
        public const string ROLE_EXTERNAL_POST_CLOSER        = "ExternalPostCloser";
        #endregion

        #region Feature Constants
        public static readonly Guid Feature_PricingEngine    = new Guid( "5B542BB7-872A-4A4E-B1A1-228FDFC2988A" );
        public static readonly Guid Feature_PriceMyLoan      = new Guid( "0FAB7C8C-B3C4-49E5-92BF-2DCDD45B701F" );
        public static readonly Guid Feature_MarketingTools   = new Guid( "859E7352-9C4F-4ED2-AC3C-CCF9EA813CF2" );
        public static readonly Guid Feature_BasicUser        = new Guid( "8D2F9319-A83B-4940-9AB0-D996EB5E829A" );
        #endregion

        public const string BCCEMAIL = "SystemSentBox@LendingQB.com";

        /// <summary>
        /// The county in which LendingQB corporate resides.
        /// </summary>
        public const string lqbCounty = "Orange";

        /// <summary>
        /// The phone number for LendingQB corporate.
        /// </summary>
        public const string lqbPhoneNumber = "888-285-3912";

        public const string CustomCocDefaultDescription = "Custom Redisclosure Field Changed";

        private static Dictionary<string, string> browserNames = new Dictionary<string, string>()
        {
            {"IE", "Microsoft Internet Explorer"},
			{"Firefox", "Mozilla Firefox"}
        };

        public static bool IsIE11(string userAgent)
        {
            if (string.IsNullOrEmpty(userAgent))
            {
                return false;
            }
            return  Regex.IsMatch(userAgent, @"Trident/7");
        }

        private static string GetBrowserLongName(string browserShortName, string browserVersion)
        {
            string browserLongName;
            try
            {
                browserLongName = browserNames[browserShortName] + " " + browserVersion;
            }
            catch (KeyNotFoundException)
            {
                browserLongName = browserShortName + " " + browserVersion;
            }
            return browserLongName;
        }

        public const string BrowserIncompatibleNotification = "Please use Internet Explorer 11 or contact your administrator to request the ability to use Chrome, Firefox, and Edge.";

        public const string BrowserObsoleteNotification = "LendingQB no longer supports your version of Internet Explorer.";
		
		public static bool DetermineIsIE(System.Web.HttpRequest request)
        {
            return (request.Browser.Browser.Equals("IE") || request.Browser.Browser.Equals("InternetExplorer"));
        }
        public static string GetBrowserObsoleteNotification(string browserName, string userAgent)
        {
            if (browserName == "IE")
            {
                Match tridentMatch = Regex.Match(userAgent, @"Trident/(\d.\d)");
                if (tridentMatch.Success && tridentMatch.Groups.Count > 1)
                {
                    string tridentVersion = tridentMatch.Groups[1].Value;

                    switch (tridentVersion)
                    {
                        case "3.1":
                        case "4.0":
                        case "5.0":
                        case "6.0":
                            return BrowserObsoleteNotification;
                        default:
                            return string.Empty;
                    }
                }
            }

            return string.Empty;
        }

        public static string GetBrowserIncompatibleNotification(string browserName, string browserVersion, string userAgent, AbstractUserPrincipal principal)
        {
            var obsoleteNotification = GetBrowserObsoleteNotification(browserName, userAgent);
            
            if(!principal.BrokerDB.IsAllowOldIe && !string.IsNullOrEmpty(obsoleteNotification))
            {
                return obsoleteNotification;
            }

            if (principal.BrokerDB.GetCrossBrowserSettings().IsEnabledForCurrentUser(principal))
            {
                return string.Empty;
            }

            string browserLongName = GetBrowserLongName(browserName, browserVersion);
            browserName = userAgent.Contains("Edge") ? "Edge" : browserName;

            if (browserName.Equals("InternetExplorer") || browserName.Equals("IE"))
            {
                //IE and InternetExplorer can both be used by IE11.
                //IE is used if the site is added to compatibility view settings.
                // InternetExplorer is used everywhere else.
                return string.Empty;
            }
            else
            {
                return $"We have detected that you are using {browserLongName}. {ConstApp.BrowserIncompatibleNotification}";
            }
        }

        /// <summary>
        /// Get browser incompatible notification for PML. It's separated out because of case 241087
        /// </summary>
        /// <returns>Browser Incompatible message</returns>
        public static string GetPmlBrowserIncompatibleNotification(string browserName, string browserVersion, string userAgent)
        {
            if (browserName == "InternetExplorer" || browserName == "IE")
            {
                Match tridentMatch = Regex.Match(userAgent, @"Trident/(\d.\d)");
                if (tridentMatch.Success && tridentMatch.Groups.Count > 1)
                {
                    string tridentVersion = tridentMatch.Groups[1].Value;

                    if ((tridentVersion.StartsWith("7") || tridentVersion.StartsWith("8")) && browserVersion.StartsWith("7"))
                    {
                        //ie 11 in comapt mode dont return anything.
                        return "";
                    }
                }

                string browserLongName = GetBrowserLongName(browserName, browserVersion);

                if (browserVersion != "11.0")
                {
                    StringWriter stringWriter = new StringWriter();
                    HtmlTextWriter writer = new HtmlTextWriter(stringWriter);
                    writer.Write("We have detected that you are using {0}.&nbsp;", browserLongName);
                    writer.Write(@"Microsoft no longer supports versions of Internet Explorer 10 or older. Please use Internet Explorer 11, or another browser such as Chrome, or Firefox to access this portal. After 6/10/2016, we will no longer support Internet Explorer 10 or older.");
                    writer.AddAttribute("href", BrowseHappyLink);
                    writer.AddAttribute(HtmlTextWriterAttribute.Target, "_blank");
                    writer.AddAttribute(HtmlTextWriterAttribute.Title, "Click here for help");
                    writer.RenderBeginTag(HtmlTextWriterTag.A);
                    writer.Write("Please download the latest browser.");
                    writer.RenderEndTag();

                    return stringWriter.ToString();
                }
            }

            return "";
        }

        public const string BrowserCompatibilityViewLink = "https://secure.lendingqb.com/help/troubleshooting/RunningLendingQBincompati.aspx";
        public const string BrowserTrustedSitesLink = "https://support.lendingqb.com/index.php?/Knowledgebase/Article/View/47";
        public const string BrowseHappyLink = "http://browsehappy.com";

        public static string GetBrowserTrustedSitesAdviceOld(string browserName, string browserVersion, string userAgent)
        {

            if (IsIE11(userAgent))
            {
                browserName = "IE";
                browserVersion = "11.0";
            }

            StringWriter stringWriter = new StringWriter();
            HtmlTextWriter writer = new HtmlTextWriter(stringWriter);

            writer.RenderBeginTag(HtmlTextWriterTag.B);
            string browserLongName = GetBrowserLongName(browserName, browserVersion);

            if (browserVersion.Equals("7.0") && userAgent.Contains("Trident/6"))
            {
                browserLongName = GetBrowserLongName(browserName, "10.0");
            }

            writer.Write("We have detected that you are using {0}.", browserLongName);
            writer.RenderEndTag();

            writer.WriteBreak();

            writer.Write("Please ");

            if (browserVersion == "10.0" || browserVersion == "11.0") // IE10 should be in compatibility view
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Href, BrowserCompatibilityViewLink);
                writer.AddAttribute(HtmlTextWriterAttribute.Target, "_blank");
                writer.AddAttribute(HtmlTextWriterAttribute.Title, "Click here for help");
                writer.RenderBeginTag(HtmlTextWriterTag.A);
                writer.Write("run LendingQB in compatibility view");
                writer.RenderEndTag();

                writer.Write(" and ");
            }

            writer.AddAttribute(HtmlTextWriterAttribute.Href, BrowserTrustedSitesLink);
            writer.AddAttribute(HtmlTextWriterAttribute.Target, "_blank");
            writer.AddAttribute(HtmlTextWriterAttribute.Title, "Click here for help");
            writer.RenderBeginTag(HtmlTextWriterTag.A);
            writer.Write("add LendingQB to the Trusted Sites zone");
            writer.RenderEndTag();

            
            
            writer.Write(" for better compatibility.");

            return stringWriter.ToString();
        }

        // 09/08/08 ck - OPM 24629.  Update password changing rules text.
        public const string PasswordGuidelines = @"<b>Passwords must follow these guidelines:</b>
																<ul>
																<li>Password must have a minimum of 6 characters.</li>
																<li>Password must contain at least one numeric and one alphabetic character.</li>
																<li>Password cannot contain first name, last name, or login name.</li>
																<li>Password cannot contain 3 identical characters in a row (Example: AAAsample1, sample1111).</li>
																<li>Password cannot contain 3 consecutive alphabetical or numeric characters in a row (Example: sampleABC, MNOsample, sampleOPQsample, sample123sample).</li>
																<li>Password cannot be the same as the previous password. </li>
                                                                <li>Passwords can only contain safe characters. Not Allowed: &lt; &gt; "" ' % ; ) ( &amp; + - =</li></ul>
                                                                ";

        public static HtmlControl PasswordGuidelinesHtml
        {
            get
            {
                HtmlGenericControl container = new HtmlGenericControl();

                HtmlGenericControl header = new HtmlGenericControl("b");
                header.InnerText = "Passwords must follow these guidelines:";
                container.Controls.Add(header);

                HtmlGenericControl ul = new HtmlGenericControl("ul");

                string[] rules = {
                                     "Password must have a minimum of 6 characters."
                                     , "Password must contain at least one numeric and one alphabetic character."
                                     , "Password cannot contain first name, last name, or login name."
                                     , "Password cannot contain 3 identical characters in a row (Example: AAAsample1, sample1111)."
                                     , "Password cannot contain 3 consecutive alphabetical or numeric characters in a row (Example: sampleABC, MNOsample, sampleOPQsample, sample123sample)."
                                     , "Password cannot be the same as the previous password."
                                     , "Passwords can only contain safe characters. Not Allowed: < > \" ' % ; ) ( & + - ="
                                 };
                foreach (string rule in rules)
                {
                    HtmlGenericControl li = new HtmlGenericControl("li");
                    li.InnerText = rule;
                    ul.Controls.Add(li);
                }
                container.Controls.Add(ul);
                return container;
            }
        }

        public const int RateLockQuestionMaxLength = 250;
        public const int RateLockAnswerMaxLength = 250;

        public const int ScanDocMagicBarcodesDocTypeId = -100;

        public const int EdocsImagesRemovalAgeInMonths = 4;

        public static bool IsEmailValid(string email)
        {
            return Regex.IsMatch(email, EmailValidationExpression);
        }

        public const string EmailValidationExpression = @"^[A-Za-z0-9&._%+-]+@[A-Za-z0-9&.-]+\.[A-Za-z]{2,}$";
        public const string SearchMatchQuotesExpression = @"(?<match>\w+)|\""(?<match>[\w\s]*)""";

        public const string LoWebUnitTestIgnoreAttributesRegex = "Connection Dependent";

        public const uint BROKER_MAX_NUMBER_OF_LOCK_PERIODS = 9;

        public const int MAX_NUMBER_CUSTOM_PML_FIELDS = 20;
        public const int MAX_NUMBER_CUSTOM_PRICING_POLICY_FIELDS = 5;

        public const string LendingQBAuthenticationCodePage = "~/AuthenticationCodePage.aspx";
        public const string CookieMultiFactorAuthentication = "MultiFA";

        public const string LendingQBPipelinePage = "~/los/main.aspx?showAlert=t";

        public const string LendingQBErrorPage = "~/common/AppError.aspx";

        // OPM 105770 - Dependencies for evaluating the field change triggers for
        // disclosure automation. These fields are used in 
        // CPageBase.ProcessFieldChangeDisclosureTriggers. It is crucial that
        // these stay in sync with the actual dependencies of the method.
        private static readonly string[] x_disclosureFieldChangeDependencies = new string[]
        {
            "sStatusT", "sFinalLAmt", "sLDiscnt", "sApprVal", "sCreditScoreLpeQual", 
            "sLpTemplateNmSubmitted", "sBranchChannelT", "sNeedInitialDisc", 
            "sNeedRedisc", "sDisclosuresDueD", "sDisclosureNeededT", "sTilGfeDueD",
            "sLastDisclosedD", "sApr", "sLastDiscAPR", "sFinMethT",
            "sfUpdateDisclosuresDueDForChangedCircumstance", "sIsLineOfCredit",
            "sMldsHasImpound", "sPurchPrice", "ForceInitialDisclosureEvalOnSave",
            "sDisclosureRegulationT", "sSubmitD", "sDisclosureNeededTDescription", 
            "sBrokerId"
        };
        public static readonly ReadOnlyCollection<string> DisclosureFieldChangeDependencies =
            new ReadOnlyCollection<string>(x_disclosureFieldChangeDependencies);

        // OPM 105770 - Dependencies for evaluating the RESPA system workflow trigger
        // for disclosure automation. These fields are used in
        // CPageBase.EvaluateWorkflowTriggersForDisclosureAutomation. It is crucial
        // that these stay in sync with the actual dependencies of the RESPA
        // workflow trigger.
        private static readonly string[] x_disclosureWorkflowFieldDependencies = new string[]
        { 
            "sLAmtCalc", "sLPurposeT", "sSpAddrTBD", "aBNm", "aBSsn", "sProdDocT",
            "sApprVal","sIsCreditQualifying","sLTotI","sHasAppraisal", 
            "sLastDisclosedD", "sNeedInitialDisc", "sDisclosureNeededT", 
            "sDisclosuresDueD", "sTilGfeDueD", "sBranchChannelT", "sfGetPreparerOfForm",
            "sDocumentCheckD", "sPurchPrice", "ForceInitialDisclosureEvalOnSave",
            "sConsumerPortalSubmittedD", "sConsumerPortalCreationD", "sConsumerPortalVerbalSubmissionD", // opm 209851
            "sfMigrateOnTRID2015", nameof(CPageBase.sfApplyCompensationFromCurrentOriginator), nameof(CPageBase.sLoanVersionT)
        };
        public static readonly ReadOnlyCollection<string> DisclosureWorkflowFieldDependencies =
            new ReadOnlyCollection<string>(x_disclosureWorkflowFieldDependencies);

        public static readonly Guid BaseDisclosurePipelineReportId = new Guid("4d329299-4352-4d3a-a94b-1f1b7635e425");

        public static ReadOnlyCollection<Guid> RolesAddedForOPM145015 = new List<Guid>()
        {
            CEmployeeFields.s_CreditAuditorId,
            CEmployeeFields.s_DisclosureDeskId,
            CEmployeeFields.s_JuniorProcessorId,
            CEmployeeFields.s_JuniorUnderwriterId,
            CEmployeeFields.s_LegalAuditorId,
            CEmployeeFields.s_LoanOfficerAssistantId,
            CEmployeeFields.s_PurchaserId,
            CEmployeeFields.s_QCComplianceId,
            CEmployeeFields.s_SecondaryId,
            CEmployeeFields.s_ServicingId
        }.AsReadOnly();

        // 4/11/2012 dd - OPM 65673 - Add GU, PR, VI, AS, MP to property state.
        // 12/6/2013 gf - opm 89690 - Pull this into ConstApp.cs to avoid duplication.
        public static ReadOnlyCollection<string> ValidStateCodesWithoutMilitaryMailCodes = new List<string>
        {
            "", "AL", "AK", "AZ", "AR", "CA", "CO", "CT", "DE", "DC", "FL", 
            "GA", "HI", "ID", "IL", "IN", "IA", "KS", "KY", "LA", "ME", 
            "MD", "MA", "MI", "MN", "MS", "MO", "MT", "NC", "ND", "NE",  
            "NH", "NJ", "NM", "NV", "NY", "OH", "OK", "OR", 
            "PA", "RI", "SC", "SD", "TN", "TX", "UT", "VT", "VA", 
            "WA", "WV", "WI", "WY", "GU", "PR", "VI", "AS", "MP"
        }.AsReadOnly();

        /// <summary>
        /// DO NOT MODIFY THIS COLLECTION. It was added for opm 171896, and is not a readonly collection due
        /// to the inefficiency of the Contains method for readonly collections and potential memory concerns.
        /// </summary>
        public static HashSet<string> FieldsToExcludeWhenApplyingGFEArchiveForAprCalc = new HashSet<string>()
        {
            "aOccT",
            "s800U1FProps",
            "s800U2FProps",
            "s800U3FProps",
            "s800U4FProps",
            "s800U5FProps",
            "s900U1PiaProps",
            "s904PiaProps",
            "sApprFProps",
            "sApprVal",
            "sAttorneyFProps",
            "sBuydwnMon1",
            "sBuydwnMon2",
            "sBuydwnMon3",
            "sBuydwnMon4",
            "sBuydwnMon5",
            "sBuydwnR1",
            "sBuydwnR2",
            "sBuydwnR3",
            "sBuydwnR4",
            "sBuydwnR5",
            "sConsummationD",
            "sConsummationDLckd",
            "sCountyRtcProps",
            "sCrFProps",
            "sDocPrepFProps",
            "sDue",
            "sEscrowFProps",
            "sEstCloseD",
            "sFHAPurposeIsStreamlineRefiWithAppr",
            "sFHAPurposeIsStreamlineRefiWithoutAppr",
            "sFfUfMipIsBeingFinanced",
            "sFfUfmipR",
            "sFinalLAmt",
            "sFinMethT",
            "sFloodCertificationFProps",
            "sGfeDiscountPointFProps",
            "sGfeLenderCreditFProps",
            "sGfeOriginatorCompFProps",
            "sGradPmtR",
            "sGradPmtYrs",
            "sHouseValPelckd",
            "sHouseValPeval",
            "sInspectFProps",
            "sIOnlyMon",
            "sIPerDay",
            "sIPerDayLckd",
            "sIPia",
            "sIPiaDy",
            "sIPiaDyLckd",
            "sIsFullAmortAfterRecast",
            "sIsOptionArm",
            "sLAmtCalc",
            "sLandCost",
            "sLDiscntProps",
            "sLOrigFProps",
            "sLPurposeT",
            "sLPurposeTPelckd", 
            "sLPurposeTPeval",
            "sLT",
            "sMBrokFProps",
            "sMInsRsrvMon",
            "sMipPiaProps",
            "sNotaryFProps",
            "sNoteIR",
            "sOccTPelckd",
            "sOccTPeval",
            "sOptionArmInitialFixMinPmtPeriod",
            "sOptionArmIntroductoryPeriod",
            "sOptionArmMinPayIsIOOnly",
            "sOptionArmMinPayOptionT",
            "sOptionArmMinPayPeriod",
            "sOptionArmNoteIRDiscount",
            "sOptionArmPmtDiscount",
            "sOptionArmTeaserR",
            "sOriginalAppraisedValue",
            "sOwnerTitleInsProps",
            "sPestInspectFProps",
            "sPmtAdjCapMon",
            "sPmtAdjCapR",
            "sPmtAdjMaxBalPc",
            "sPmtAdjRecastPeriodMon",
            "sPmtAdjRecastStop",
            "sProcFProps",
            "sProMIns",
            "sProMIns2Mon",
            "sProMInsCancelLtv",
            "sProMInsCancelMinPmts",
            "sProMInsLckd",
            "sProMInsMb",
            "sProMInsMidptCancel",
            "sProMInsMon",
            "sProMInsR",
            "sProMInsR2",
            "sProMInsT",
            "sPurchPrice",
            "sRAdj1stCapMon",
            "sRAdj1stCapR",
            "sRAdjCapMon",
            "sRAdjCapR",
            "sRAdjFloorR",
            "sRAdjIndexR",
            "sRAdjLifeCapR",
            "sRAdjMarginR",
            "sRAdjRoundT",
            "sRAdjRoundToR",
            "sRAdjWorstIndex",
            "sRecFProps",
            "sStateRtcProps",
            "sTerm",
            "sTitleInsFProps",
            "sTxServFProps",
            "sU1GovRtcProps",
            "sU1ScProps",
            "sU1TcProps",
            "sU2GovRtcProps",
            "sU2ScProps",
            "sU2TcProps",
            "sU3GovRtcProps",
            "sU3ScProps",
            "sU3TcProps",
            "sU4ScProps",
            "sU4TcProps",
            "sU5ScProps",
            "sUwFProps",
            "sVaFfProps",
            "sWireFProps"
        };

        public const string SystemDefualtBlankLeadSourceName = "No selection";
        public const int SystemDefaultBlankLeadSourceId = 0;

        public const string SystemDefualtBlankPmlCompanyTierName = "No selection";
        public const int SystemDefaultBlankPmlCompanyTierId = 0;

        /// <summary>
        /// DO NOT MODIFY THIS COLLECTION, this should eventually be updated
        /// to a ReadOnlyHashset.
        /// This set includes the ids of fields which should not be included as 
        /// options for the task resolution date setter. 
        /// </summary>
        /// <remarks>
        /// Fields should generally be included in this set if they do not have
        /// a public set property or are calculated with the option of "locking"
        /// to set a value.
        /// This set exists to make it obvious when a date field will not be
        /// available to the resolution date setter.
        /// </remarks>
        public static HashSet<string> TaskResolutionDateSetterFieldIdBlacklist = new HashSet<string>()
        {
            "sTenBusinessDaysFromSchedFundD",
            "sApp1003InterviewerPrepareDate",
            "sApprovDTime",
            "sCanceledDTime",
            "sClearToCloseDTime",
            "sClearToPurchaseDTime",
            "sClosedDTime",
            "sConditionReviewDTime",
            "sCounterOfferDTime",
            "sCreatedDTime",
            "sDocsBackDTime",
            "sDocsDrawnDTime",
            "sDocsOrderedDTime",
            "sDocsDTime",
            "sDocumentCheckDTime",
            "sDocumentCheckFailedDTime",
            "sEstCloseDTime",
            "sFinalDocsDTime",
            "sFinalUnderwritingDTime",
            "sFundDTime",
            "sFundingConditionsDTime",
            "sGfeTilPrepareDate",
            "sInFinalPurchaseReviewDTime",
            "sInPurchaseReviewDTime",
            "sSuspendedByInvestorDTime",
            "sCondSentToInvestorDTime",
            "sLatestActivityDateForMobileApp",
            "sLeadCanceledD",
            "sLeadDeclinedD",
            "sNewEdocsUploadedD",
            "sArchivedDTime",
            "sRejectDTime",
            "sPurchasedDTime",
            "sShippedToInvestorDTime",
            "sLPurchaseDTime",
            "sStatusD",
            "sWithdrawnDTime",
            "sRLckdExpiredD",
            "sLeadDTime",
            "sOnHoldDTime",
            "sOpenedDTime",
            "sPreApprovDTime",
            "sPreDocQCDTime",
            "sPreProcessingDTime",
            "sPrePurchaseConditionsDTime",
            "sPreQualDTime",
            "sPreUnderwritingDTime",
            "sProcessingDTime",
            "sReadyForSaleDTime",
            "sRecordedDTime",
            "sSubmitDTime",
            "sSchedFundDTime",
            "sSchedDueD2",
            "sLoanSubmittedDTime",
            "sSubmittedForFinalPurchaseDTime",
            "sSubmittedForPurchaseReviewDTime",
            "sSuspendedDTime",
            "sSchedDueD3",
            "sTilPrepareDate",
            "sTilGfeDocumentD",
            "sOriginatorCompensationEffectiveD",
            "sOriginatorCompensationPlanAppliedD",
            "sCrOd",
            "sAgentInvestorSoldD",
            "sGfeInitialDisclosureD",
            "sServicingNextPmtDue",
            "sAppExpD",
            "sAppReceivedByLenderD",
            "sAppSubmittedD",
            "sDocExpirationD",
            "sDocMagicDocumentD",
            "sMersTosD",
            "sSchedDueD1",
            "sTaxTable1008DueD",
            "sTaxTable1009DueD",
            "sTaxTableRealEtxDueD",
            "sTaxTableSchoolDueD",
            "sTaxTableU3DueD",
            "sTaxTableU4DueD",
            "sServicingByUsStartD",
            "sServicingByUsEndD",
            "sNMLSServicingTransferInD",
            "sNMLSServicingTransferOutD",
            "sConsumerPortalVerbalSubmissionD",
            "sTRIDLoanEstimateNoteIRAvailTillD",
            "sEstCloseD",
            "sInitialClosingDisclosureReceivedDeadlineD",
            "sInitialClosingDisclosureMailedDeadlineD",
            "sIntialLoanEstimateMailedOrReceivedDeadlineForCurrentClosingD",
            "sFinalClosingDisclosureCreatedD",
            "sFinalClosingDisclosureReceivedD",
            "sInitialClosingDisclosureCreatedD",
            "sInitialClosingDisclosureReceivedD",
            "sInitialLoanEstimateCreatedD",
            "sInitialLoanEstimateReceivedD",
            "sLastClosingDisclosureBeforeConsummationCreatedD",
            "sLastClosingDisclosureBeforeConsummationReceivedD",
            "sLastDisclosedLoanEstimateCreatedD",
            "sLastDisclosedLoanEstimateReceivedD",
            "sPreviewClosingDisclosureCreatedD",
            "sPreviewClosingDisclosureReceivedD",
            "sGfeEstScAvailTillD",
            "sLoanEstScAvailTillD",
            "sRevisedLoanEstimateReceivedDeadlineForCurrentClosingD",
            "sQCCompDateTime",
            "sFinancingContingencyDeadlineD",
            "sPropertyTransferD",
            "sInterestRateSetD",
            "sNmlsApplicationDate",
            "sDocMagicDisbursementD",
            "sConsummationD",
            "sHmdaApplicationDate",
            "sInitialLoanEstimateMailedOrReceivedDeadlineForCurrentAppD",
            "sUnderwritingDTime",
            "sTpoRequestForInitialDisclosureGenerationStatusD",
            "sTpoRequestForInitialClosingDisclosureGenerationStatusD",
            "sFirstUcdDeliveryD",
            "sLastUcdDeliveryD",
            "sFinalClosingDisclosureIssuedD",
            "sInitialClosingDisclosureIssuedD",
            "sInitialLoanEstimateIssuedD",
            "sLastClosingDisclosureBeforeConsummationIssuedD",
            "sLastDisclosedLoanEstimateIssuedD",
            "sPreviewClosingDisclosureIssuedD"
        };

        public static HashSet<string> CreditDescriptionsForLoanLevelCredits = new HashSet<string>()
        {
            "Lender Credit",
            "Borrower Paid Fees",
            "Paid Outside of Closing",
			"Broker Credit"
        };

        /// <summary>
        /// Provides the names of the TPO loan navigation page names.
        /// </summary>
        /// <remarks>
        /// Keep these names in sync with TpoLoanNavigation.xml.config.
        /// </remarks>
        public static readonly IEnumerable<string> TpoLoanNavigationPageNames = new[]
        {
            "Status and Agents",
            "Application Information",
            "Closing Costs",
            "Pricing",
            "Loan Information",
            "Rate Lock",
            "QM",
            "Disclosures",
            "E-docs",
            "Tasks",
            "Conditions",
            "Order Services"
        };

        /// <summary>
        /// Loans in these statuses will not be auto-disclosed.
        /// </summary>
        /// <remarks>
        /// If you want to change this list of statuses, make sure to update the
        /// ListLoansForAutoDisclosure stored procedure.
        /// </remarks>
        public static HashSet<E_sStatusT> StatusesExcludedFromAutoDisclosure = new HashSet<E_sStatusT>()
        {
            E_sStatusT.Loan_Funded,
            E_sStatusT.Loan_OnHold,
            E_sStatusT.Loan_Canceled,
            E_sStatusT.Loan_Rejected,
            E_sStatusT.Loan_Closed,
            E_sStatusT.Lead_Canceled,
            E_sStatusT.Lead_Declined,
            E_sStatusT.Lead_Other,
            E_sStatusT.Loan_Other,
            E_sStatusT.Loan_Recorded,
            E_sStatusT.Loan_Shipped,
            E_sStatusT.Loan_DocsBack,
            E_sStatusT.Loan_FundingConditions,
            E_sStatusT.Loan_FinalDocs,
            E_sStatusT.Loan_LoanPurchased,
            E_sStatusT.Loan_InvestorConditions,
            E_sStatusT.Loan_InvestorConditionsSent,
            E_sStatusT.Loan_ReadyForSale,
            E_sStatusT.Loan_SubmittedForPurchaseReview,
            E_sStatusT.Loan_InPurchaseReview,
            E_sStatusT.Loan_PrePurchaseConditions,
            E_sStatusT.Loan_SubmittedForFinalPurchaseReview,
            E_sStatusT.Loan_InFinalPurchaseReview,
            E_sStatusT.Loan_ClearToPurchase,
            E_sStatusT.Loan_Purchased,
            E_sStatusT.Loan_CounterOffer,
            E_sStatusT.Loan_Withdrawn,
            E_sStatusT.Loan_Archived
        };

        public static readonly ReadOnlyCollection<string> TridAutoMigrationTriggerFieldIds = new List<string>()
        {
            "sDisclosureRegulationT",
            "sAppSubmittedD",
            "sOpenedD",
            "sDisclosureRegulationTLckd",
        }.AsReadOnly();

        /// <summary>
        /// Get an <see cref="Address"/> object containing the LendingQB corporate address.
        /// </summary>
        /// <returns>The LendingQB address.</returns>
        public static Address GetLendingQBAddress()
        {
            Address address = new Address();
            address.ParseStreetAddress("1124 Bristol Street");
            address.City = "Costa Mesa";
            address.State = "CA";
            address.Zipcode = "92626";

            return address;
        }

        public static readonly Permission[] SkipConditionBasePagePermissionCheck = new Permission[] { };

        public static readonly Permission[] DefaultConditionPagePermission = 
            new Permission[] { Permission.AllowUnderwritingAccess };

        /// <summary>
        /// The thresholds for determining the allowable QM Points and Fees.
        /// </summary>
        public class QMPointsFees
        {
            public decimal tier1LAmt { get; set; }
            public decimal tier2LAmt { get; set; }
            public decimal tier3LAmt { get; set; }
            public decimal tier4LAmt { get; set; }
            public decimal tier1Pc { get; set; }
            public decimal tier2Amt { get; set; }
            public decimal tier3Pc { get; set; }
            public decimal tier4Amt { get; set; }
            public decimal tier5Pc { get; set; }
        }

        /// <summary>
        /// Maps years to the thresholds for that year that determine the allowable QM Points and Fees. <para></para>
        /// Will need to have an entry added each year as the thresholds are adjusted for inflation. <para></para>
        /// For pre-2015 thresholds, use <see cref="Pre2015QMPointsFees"/>.
        /// </summary>
        public static readonly Dictionary<int, QMPointsFees> QMPointsFeesByYear = new Dictionary<int, QMPointsFees>
        {
            { 
                // OPM 191021
                2015,
                new QMPointsFees 
                { 
                    tier1LAmt = 101953.00M,
                    tier2LAmt = 61172.00M,
                    tier3LAmt = 20391.00M,
                    tier4LAmt = 12744.00M,
                    tier1Pc = 3.000M,
                    tier2Amt = 3059.00M,
                    tier3Pc = 5.000M,
                    tier4Amt = 1020.00M,
                    tier5Pc = 8.000M
                }
            },
            { 
                // OPM 226682
                2016,
                new QMPointsFees 
                { 
                    tier1LAmt = 101749.00M,
                    tier2LAmt = 61050.00M,
                    tier3LAmt = 20350.00M,
                    tier4LAmt = 12719.00M,
                    tier1Pc = 3.000M,
                    tier2Amt = 3052.00M,
                    tier3Pc = 5.000M,
                    tier4Amt = 1017.00M,
                    tier5Pc = 8.000M
                }
            },
            { 
                // OPM 245614
                2017,
                new QMPointsFees
                {
                    tier1LAmt = 102894.00M,
                    tier2LAmt = 61737.00M,
                    tier3LAmt = 20579.00M,
                    tier4LAmt = 12862.00M,
                    tier1Pc = 3.000M,
                    tier2Amt = 3087.00M,
                    tier3Pc = 5.000M,
                    tier4Amt = 1029.00M,
                    tier5Pc = 8.000M
                }
            },
            {
                // OPM 461545
                2018,
                new QMPointsFees
                {
                    tier1LAmt = 105158.00M,
                    tier2LAmt = 63095.00M,
                    tier3LAmt = 21032.00M,
                    tier4LAmt = 13145.00M,
                    tier1Pc = 3.000M,
                    tier2Amt = 3155.00M,
                    tier3Pc = 5.000M,
                    tier4Amt = 1052.00M,
                    tier5Pc = 8.000M
                }
            },
            {
                // OPM 473834
                2019,
                new QMPointsFees
                {
                    tier1LAmt = 107747.00M,
                    tier2LAmt = 64648.00M,
                    tier3LAmt = 21549.00M,
                    tier4LAmt = 13468.00M,
                    tier1Pc = 3.000M,
                    tier2Amt = 3232.00M,
                    tier3Pc = 5.000M,
                    tier4Amt = 1077.00M,
                    tier5Pc = 8.000M
                }
            }
        };

        /// <summary>
        /// Contains the QM points and fees for years that are pre-2015. <para></para>
        /// For post-2014 years, use the <see cref="QMPointsFeesByYear"/> dictionary.
        /// </summary>
        public static QMPointsFees Pre2015QMPointsFees = new QMPointsFees
        {
            tier1LAmt = 100000.00M,
            tier2LAmt = 60000.00M,
            tier3LAmt = 20000.00M,
            tier4LAmt = 12500.00M,
            tier1Pc = 3.000M,
            tier2Amt = 3000.00M,
            tier3Pc = 5.000M,
            tier4Amt = 1000.00M,
            tier5Pc = 8.000M
        };

        /// <summary>
        /// Represents the fields that should always be included in a loan's audit history.
        /// </summary>
        /// <remarks>
        /// Per PDE in OPM 236046, all loan status dates should always be included in the
        /// audit history.
        /// </remarks>
        public static readonly HashSet<string> AlwaysAuditFieldIds = new HashSet<string>(
            StringComparer.OrdinalIgnoreCase)
        {
            "aBNm",
            "aBSsn",
            "aCNm",
            "aCSsn",
            "sApprovD",
            "sArchivedD",
            "sCanceledD",
            "sClearToCloseD",
            "sClearToPurchaseD",
            "sClosedD",
            "sClosingCostFeeVersionT",
            "sConditionReviewD",
            "sCondSentToInvestorD",
            "sConsumerPortalVerbalSubmissionComments",
            "sConsumerPortalVerbalSubmissionD",
            "sCounterOfferD",
            "sDocsBackD",
            "sDocsD",
            "sDocsDrawnD",
            "sDocsOrderedD",
            "sDocumentCheckD",
            "sDocumentCheckFailedD",
            "sFinalDocsD",
            "sFinalUnderwritingD",
            "sFundD",
            "sFundingConditionsD",
            "sInFinalPurchaseReviewD",
            "sInPurchaseReviewD",
            "sLoanSubmittedD",
            "sLPurchaseD",
            "sOnHoldD",
            "sOpenedD",
            "sPreApprovD",
            "sPreDocQCD",
            "sPreProcessingD",
            "sPrePurchaseConditionsD",
            "sPreQualD",
            "sPreUnderwritingD",
            "sProcessingD",
            "sPurchasedD",
            "sReadyForSaleD",
            "sRecordedD",
            "sRejectD",
            "sShippedToInvestorD",
            "sSpAddr",
            "sSubmitD",
            "sSubmittedForFinalPurchaseD",
            "sSubmittedForPurchaseReviewD",
            "sSuspendedByInvestorD",
            "sSuspendedD",
            "sUnderwritingD",
            "sWithdrawnD",
            "sU1LStatDesc",
            "sU2LStatDesc",
            "sU3LStatDesc",
            "sU4LStatDesc",
            "sU1LStatD",
            "sU2LStatD",
            "sU3LStatD",
            "sU4LStatD",
            "sU1LStatN",
            "sU2LStatN",
            "sU3LStatN",
            "sU4LStatN",
            "sLNm",
            nameof(CPageBase.sStatusT),
            nameof(CAppBase.aBNmFirstEnteredD),
            nameof(CAppBase.aBSsnFirstEnteredD),
            nameof(CAppBase.aCNmFirstEnteredD),
            nameof(CAppBase.aCSsnFirstEnteredD),
            nameof(CPageBase.sRespaBorrowerNameCollectedD),
            nameof(CPageBase.sRespaBorrowerSsnCollectedD),
            nameof(CPageBase.sRespaIncomeFirstEnteredD),
            nameof(CPageBase.sRespaIncomeCollectedD),
            nameof(CPageBase.sRespaPropAddressFirstEnteredD),
            nameof(CPageBase.sRespaPropAddressCollectedD),
            nameof(CPageBase.sRespaPropValueFirstEnteredD),
            nameof(CPageBase.sRespaPropValueCollectedD),
            nameof(CPageBase.sRespaLoanAmountFirstEnteredD),
            nameof(CPageBase.sRespaLoanAmountCollectedD),
            nameof(CPageBase.sLTotI),
            nameof(CPageBase.sApprVal),
            nameof(CPageBase.sPurchPrice),
            nameof(CPageBase.sLAmtCalc),
            nameof(CPageBase.sRespa6D),
            nameof(CPageBase.sAppSubmittedD),
            nameof(CPageBase.sInitAPR),
            nameof(CPageBase.sLastDiscAPR),
            nameof(CPageBase.sCalculateInitAprAndLastDiscApr),
            nameof(CPageBase.sHmdaActionTaken),
            nameof(CPageBase.sHmdaActionD),
            nameof(CPageBase.sComplianceEaseId),
        };

        /// <summary>
        /// Provides mappings between constants in <see cref="OCStatusType"/> and
        /// their user-friendly string representations.
        /// </summary>
        public static readonly Dictionary<OCStatusType, string> FriendlyOCStatusTypes = new Dictionary<OCStatusType, string>()
        {
            { OCStatusType.Blank, string.Empty },
            { OCStatusType.Approved, "Approved" },
            { OCStatusType.Suspended, "Suspended" },
            { OCStatusType.Inactive, "Inactive" },
            { OCStatusType.Terminated, "Terminated" },
            { OCStatusType.Pending, "Pending" },
            { OCStatusType.Declined, "Declined" },
            { OCStatusType.Watchlist, "Watch List" }
        };

        public const string UseLenderLevelDefaultValue = "UseLenderLevelDefault";

        /// <summary>
        /// The maximum length of a serialized JSON string. Keep this value in sync
        /// with the <code>jsonSerialization</code> element in the web.config files
        /// for LendersOfficeApp and PML.
        /// </summary>
        public const int MaxJsonLength = 40000000;
    }

    public class CompilerConst
    {
#if DEBUG
        public const bool IsDebugMode = true;
#else
        public const bool IsDebugMode   = false;
#endif

        public const bool IsReleaseMode = IsDebugMode ? false : true;

        public static bool IsDebugModeVar
        {
            get { return IsDebugMode; }
        }

        public static bool IsReleaseModeVar
        {
            get { return IsReleaseMode; }
        }



    }

}
