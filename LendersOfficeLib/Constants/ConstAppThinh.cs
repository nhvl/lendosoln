using System;

namespace LendersOffice.Constants
{
	public class ConstAppThinh
	{
		public const string LpeSnapshot1Id = "LpeDataSnapshot1";
		public const string LpeSnapshot2Id = "LpeDataSnapshot2";

        public const string LpePrefixUndropSnapshotId = "ss0";
        public const string LpeUndropSnapshot1Id = "ss01";
        public const string LpeUndropSnapshot2Id = "ss02";
    }
}
