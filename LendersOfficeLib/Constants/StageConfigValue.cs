﻿namespace LendersOffice.Constants
{
    using System;
    using LqbGrammar;
    using LqbGrammar.Queries;

    /// <summary>
    /// Used to retrieve a named configuration datum from the stage configuration.  This class is public only for the sake of testing.
    /// </summary>
    public sealed class StageConfigValue : ConfigurationValueBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="StageConfigValue"/> class.
        /// </summary>
        /// <param name="name">The data item's name.</param>
        public StageConfigValue(string name)
            : base(name)
        {
        }

        /// <summary>
        /// Interface for retrieving a string valued datum.
        /// </summary>
        /// <returns>The string value of the datum.</returns>
        public override string GetStringValue()
        {
            var tuple = this.RetrieveConfigurationValue(this.KeyName);
            return tuple?.Item2;
        }

        /// <summary>
        /// Interface for retrieving an integer valued datum.
        /// </summary>
        /// <returns>The integer value of the datum.</returns>
        public int? GetIntValue()
        {
            var tuple = this.RetrieveConfigurationValue(this.KeyName);
            return tuple?.Item1;
        }

        /// <summary>
        /// Pull the configuration data and grab the value for this datum.
        /// </summary>
        /// <param name="keyName">The key for the lookup.</param>
        /// <returns>The value for this datum.</returns>
        private Tuple<int, string> RetrieveConfigurationValue(string keyName)
        {
            var factory = GenericLocator<IConfigurationQueryFactory>.Factory;
            var driver = factory.CreateStageConfiguration();

            var configuration = driver.ReadAllValues();

            return configuration.ContainsKey(keyName) ? configuration[keyName] : null;
        }
    }
}
