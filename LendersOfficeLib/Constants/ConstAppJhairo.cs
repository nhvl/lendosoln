﻿namespace LendersOffice.Constants
{
    using System;

    /// <summary>
    /// A collection of miscellaneous app fields.
    /// </summary>
    public partial class ConstApp
    {
        /// <summary>
        /// Hard Coded Test Loan File Environment Id for testing Fee Service.
        /// </summary>
        public static readonly Guid TestFeeServiceEnvironmentId = new Guid("0C0D1F6C-25CC-11E5-80E0-BFD9A1AA3235");

        /// <summary>
        /// Date by which all users must update their passwords.
        /// </summary>
        public static readonly DateTime AllUsersMandatoryPasswordChangeDate = new DateTime(2016, 2, 12);
    }
}