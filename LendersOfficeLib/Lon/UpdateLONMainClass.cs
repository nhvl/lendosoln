﻿namespace LendersOffice.Lon
{
    using System;
    using System.Net;
    using System.Xml;
    using DataAccess;
    using LendersOffice.Constants;
    using LendersOffice.ObjLib.DatabaseMessageQueue;
    using LendersOfficeLib.ObjLib.Lon;

    /// <summary>
    /// The class for updating a loan with the Lon integration.
    /// </summary>
    public class UpdateLONMainClass
    {
        /// <summary>
        /// Executes the Integration update with the given args.
        /// </summary>
        /// <param name="args">The arguments.</param>
        public static void Execute(string[] args)
        {
            int countProcessed = 0;
            try
            {
                System.Collections.Hashtable hash = new System.Collections.Hashtable(100);
                DBMessageQueue dbqueue = new DBMessageQueue(ConstMsg.NHCQueue);

                var lonUsername = ConstStage.LON_USERNAME;
                var lonPassword = ConstStage.LON_PASSWORD;

                while (true)
                {
                    DBMessage msg = dbqueue.Receive();
                    if (msg == null)
                    {
                        return;
                    }

                    Guid loanId = new Guid(msg.Subject1);

                    if (hash.ContainsKey(loanId.ToString()))
                    {
                        // Loan already processed in this method invocation.
                        continue; 
                    }
                    else
                    {
                        hash.Add(loanId.ToString(), null);

                        // 11/22/2004 kb - Updated the loan data object we use
                        // so that access control checking is skipped.  We need
                        // to read loans, but we really lack access.
                        CLonIntegrationAllData loanData = new CLonIntegrationAllData(loanId);
                        ProcessMessage(loanData, lonUsername, lonPassword);

                        countProcessed++;
                    }
                }
            }
            catch (DBMessageQueueException exc)
            {
                if (exc.MessageQueueErrorCode == DBMessageQueueErrorCode.EmptyQueue)
                {
                    Tools.LogInfo("Processed " + countProcessed.ToString() + " messages.");
                    return;
                }
                else
                {
                    Tools.LogErrorWithCriticalTracking(exc);
                    throw;
                }
            }
            catch (Exception ex)
            {
                Tools.LogErrorWithCriticalTracking("UpdateLON.exe: Error encountered. See next pb log entry", ex);
                throw;
            }
        }

        /// <summary>
        /// Process the message for the given loan data.
        /// </summary>
        /// <param name="loanData">The data of the loan to process the message for.</param>
        /// <param name="lonUsername">The LQB username for the LON integration.</param>
        /// <param name="lonPassword">The password for the LON integration.</param>
        /// <returns>The status after processing.</returns>
        public static string ProcessMessage(CPageData loanData, string lonUsername, string lonPassword)
        {
            loanData.InitLoad();

            LendersOffice.Conversions.LON.LoanLONXmlWriter lonWriter = new LendersOffice.Conversions.LON.LoanLONXmlWriter();
            using (System.IO.MemoryStream memStream = new System.IO.MemoryStream(100000))
            {
                XmlTextWriter xmlWriter = new XmlTextWriter(memStream, System.Text.Encoding.ASCII);
                xmlWriter.WriteStartElement("LON_LOANSTATUS_XML");
                lonWriter.Write(xmlWriter, loanData);
                xmlWriter.WriteEndElement();
                xmlWriter.Flush();

                string stringXml = System.Text.ASCIIEncoding.ASCII.GetString(memStream.GetBuffer(), 0, (int)memStream.Position);
                LoanStatus_Service service = new LoanStatus_Service();

                service.PreAuthenticate = false;
                service.Credentials = new NetworkCredential(lonUsername, lonPassword);

                string status = service.saveLoanStatus(stringXml, loanData.sBrokerId.ToString());
                Tools.LogInfo("XML: " + stringXml + ", Status=" + status);

                return status;
            }
        }
    }
}
