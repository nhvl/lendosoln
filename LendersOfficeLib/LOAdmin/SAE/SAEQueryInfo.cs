﻿/// <copyright file="DatabaseQueryExecutor.cs" company="MeridianLink">
///     Copyright (c) MeridianLink. All rights reserved.
/// </copyright>
/// <summary>
/// Author: Michael Leinweaver
/// Date:   1/22/2016
/// </summary>
namespace LendersOffice.LOAdmin.SAE
{
    using System;
    using System.Collections.Generic;
    using DataAccess;
    using MeridianLink.CommonControls;

    /// <summary>
    /// Provides a container for the information associated with an
    /// SAE database query.
    /// </summary>
    public class SAEQueryInfo
    {
        /// <summary>
        /// Represents the broker ID for <code>PML0176</code> used for the 
        /// "Generate List of Pacific Union TPO Users" query.
        /// </summary>
        public static readonly Guid PacUnionBrokerId = new Guid("79991CF9-E793-4609-A2EA-E63995947A27");

        /// <summary>
        /// Represents the <code>PacUnion</code> TPO portal user query that is run
        /// only when the user clicks on the "Export Pacific Union Users" button
        /// on the database queries page.
        /// </summary>
        public static readonly SAEQueryInfo PacUnionTPOQuery = new SAEQueryInfo()
        {
            Name = "Generate List of Pacific Union Originator Portal Users",
            Description = "Query Performed: This query will return a list of all of the Originator Portal users for Pacific Union, PML0176. <u>Due to the number of users present in the list, this table is not generated on the page. Please click 'Export Pacific Union Users' above to save the results.</u>",
            StoredProcedureName = "SAEDB_ListBrokerTPOUsers",
            ExportFilename = "PacificUnionTPOUsers",
            ExportFileHeader = "User Name,First Name,Last Name,Email,Price Group,Company Name,Branch Name,Street Address,City,State,Is Supervisor?",
            ShowDataGridWithExportResultsButton = false,
            ShowExportPacUnionButton = true
        };

        /// <summary>
        /// Gets or sets the value for the name of the query.
        /// </summary>
        /// <value>
        /// The string name of the query.
        /// </value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the value for the description of the query.
        /// </summary>
        /// <value>
        /// The string description of the query.
        /// </value>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the value for the method called to perform
        /// this query.
        /// </summary>
        /// <value>
        /// The <see cref="Action"/> used to perform this query.
        /// </value>
        public Action<SAEQueryInfo, CommonDataGrid> Method { get; set; }

        /// <summary>
        /// Gets or sets the value for the data source of the query.
        /// </summary>
        /// <value>
        /// The <see cref="DataSrc"/> of the query.
        /// </value>
        public DataSrc DataSource { get; set; }

        /// <summary>
        /// Gets or sets the value for the name of the stored procedure
        /// for the query.
        /// </summary>
        /// <value>
        /// The string name of the stored procedure for the query.
        /// </value>
        public string StoredProcedureName { get; set; }

        /// <summary>
        /// Gets or sets the value for the filename of the query.
        /// </summary>
        /// <value>
        /// The string filename for the query.
        /// </value>
        public string ExportFilename { get; set; }

        /// <summary>
        /// Gets or sets the value for the header of the export file
        /// for the query.
        /// </summary>
        /// <value>
        /// The string name of the query.
        /// </value>
        public string ExportFileHeader { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the data grid and 'Export Results' button
        /// should be shown after this query is performed.
        /// </summary>
        /// <value>
        /// True if the data grid and 'Export Results' button should be shown, false otherwise.
        /// </value>
        public bool ShowDataGridWithExportResultsButton { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the export <code>PacUnion</code> 
        /// results button should be shown after this query is performed.
        /// </summary>
        /// <value>
        /// True if the export <code>PacUnion</code> results button should be shown, 
        /// false otherwise.
        /// </value>
        public bool ShowExportPacUnionButton { get; set; }

        /// <summary>
        /// Obtains the list of <see cref="SAEQueryInfo"/> queries that can be executed.
        /// </summary>
        /// <returns>
        /// A list of <see cref="SAEQueryInfo"/> queries that can be executed.
        /// </returns>
        public static IEnumerable<SAEQueryInfo> ListAll()
        {
            return new List<SAEQueryInfo>()
            {
                new SAEQueryInfo()
                {
                    Name = "<-- Select query -->",
                    Description = "Query Performed: none.",
                    ShowDataGridWithExportResultsButton = false,
                    ShowExportPacUnionButton = false
                },
                new SAEQueryInfo()
                {
                    Name = "No Loan Product Type",
                    Description = "Query Performed: This query will return the loan product information where the loan is not a master nor derived and the 'Loan Product Type' (field lLpProductType) is left empty.",
                    Method = (queryInfo, dataGrid) => SAEDatabaseQueryExecutor.NoLoanProductTypeQuery(queryInfo, dataGrid),
                    DataSource = DataSrc.LpeSrc,
                    StoredProcedureName = "SAEDB_NoLoanProductType",
                    ExportFilename = "NoLoanProductTypePrograms",
                    ExportFileHeader = "Broker Name,Folder Name,Template Name,Creation Date",
                    ShowDataGridWithExportResultsButton = true,
                    ShowExportPacUnionButton = false
                },
                new SAEQueryInfo()
                {
                    Name = "List all PMLMASTER Loan Programs",
                    Description = "Query Performed: This query will return the entire list of Loan Programs in the PMLMASTER account (not including Master or Derived programs). Note: Programs with the attached policy \"GENERIC::DISCONTINUE\" (guid:bce6e620-d267-4b06-845b-073b85366226) have been excluded from the list.",
                    Method = (queryInfo, dataGrid) => SAEDatabaseQueryExecutor.ListPmlMasterProgramsQuery(queryInfo, dataGrid),
                    DataSource = DataSrc.LpeSrc,
                    StoredProcedureName = "SAEDB_ListPmlMasterPrograms",
                    ExportFilename = "PMLMasterLoanPrograms",
                    ExportFileHeader = "Investor Name,Product Code,Lien,Program Name",
                    ShowDataGridWithExportResultsButton = true,
                    ShowExportPacUnionButton = false
                },
                new SAEQueryInfo()
                {
                    Name = "List Loan Programs with Invalid Investors",
                    Description = "Query Performed: This query will return a list of Loan Programs created by an SAE using invalid investors (not including Derived programs). ",
                    Method = (queryInfo, dataGrid) => SAEDatabaseQueryExecutor.ListInvalidInvestorProgramsQuery(queryInfo, dataGrid),
                    DataSource = DataSrc.LpeSrc,
                    StoredProcedureName = "SAEDB_ListInvalidInvestorPrograms",
                    ExportFilename = "InvalidInvestorLoanPrograms",
                    ExportFileHeader = "Broker Name,Investor Name,Template Name,Master",
                    ShowDataGridWithExportResultsButton = true,
                    ShowExportPacUnionButton = false
                },
                new SAEQueryInfo()
                {
                    Name = "Get FHA Loan Limits",
                    Description = "Query Performed: This query will return a list of the FHA loan limits currently stored. ",
                    Method = (queryInfo, dataGrid) => SAEDatabaseQueryExecutor.ShowLoanLimits(queryInfo, dataGrid),
                    DataSource = DataSrc.LOShareROnly,
                    StoredProcedureName = "SAEDB_ListFHALoanLimits",
                    ExportFilename = "FHALoanLimits",
                    ExportFileHeader = "Fips Code, Limit 1 Unit, Limit 2 Unit, Limit 3 Unit, Limit 4 Unit, Last Revised",
                    ShowDataGridWithExportResultsButton = true,
                    ShowExportPacUnionButton = false
                },
                new SAEQueryInfo()
                {
                    Name = "Get Conforming Loan Limits",
                    Description = "Query Performed: This query will return a list of the CONFORMING loan limits currently stored. ",
                    Method = (queryInfo, dataGrid) => SAEDatabaseQueryExecutor.ShowLoanLimits(queryInfo, dataGrid),
                    DataSource = DataSrc.LOShareROnly,
                    StoredProcedureName = "SAEDB_ListConformingLoanLimits",
                    ExportFilename = "ConformingLoanLimits",
                    ExportFileHeader = "Fips Code, Limit 1 Unit, Limit 2 Unit, Limit 3 Unit, Limit 4 Unit, Last Revised",
                    ShowDataGridWithExportResultsButton = true,
                    ShowExportPacUnionButton = false
                },
                new SAEQueryInfo()
                {
                    Name = "List Brokers with 'Always Display Exact Par Rate Option' Enabled",
                    Description = "Query Performed: This query will return a list of brokers that have the 'Always Display Exact Par Rate Option' enabled.",
                    Method = (queryInfo, dataGrid) => SAEDatabaseQueryExecutor.GetBrokersWithExactParRateOptionEnabledQuery(queryInfo, dataGrid),
                    DataSource = DataSrc.LOShareROnly,
                    StoredProcedureName = "SAEDB_ListBrokersWithExactParRateOption",
                    ExportFilename = "BrokersWithAlwaysDisplayExactParRateOptionEnabled",
                    ExportFileHeader = "Company Name, Customer Code",
                    ShowDataGridWithExportResultsButton = true,
                    ShowExportPacUnionButton = false
                },
                SAEQueryInfo.PacUnionTPOQuery
            };
        }
    }
}
