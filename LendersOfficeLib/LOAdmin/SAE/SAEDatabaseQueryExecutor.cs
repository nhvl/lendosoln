﻿/// <copyright file="DatabaseQueryExecutor.cs" company="MeridianLink">
///     Copyright (c) MeridianLink. All rights reserved.
/// </copyright>
/// <summary>
/// Author: Michael Leinweaver
/// Date:   1/22/2016
/// </summary>
namespace LendersOffice.LOAdmin.SAE
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
using System.Data.SqlClient;
    using System.IO;
    using System.Linq;
    using System.Web.UI.WebControls;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common.TextImport;
    using MeridianLink.CommonControls;

    /// <summary>
    /// Provides a means for executing SAE-related database queries.
    /// </summary>
    public static class SAEDatabaseQueryExecutor
    {
        /// <summary>
        /// Represents the list of queries that can be performed.
        /// </summary>
        private static readonly Dictionary<string, SAEQueryInfo> QueryList;

        /// <summary>
        /// Initializes static members of the <see cref="SAEDatabaseQueryExecutor"/>
        /// class.
        /// </summary>
        static SAEDatabaseQueryExecutor()
        {
            SAEDatabaseQueryExecutor.QueryList = SAEQueryInfo.ListAll().ToDictionary(
                queryInfo => queryInfo.Name);
        }

        /// <summary>
        /// Executes the query with the specified <paramref name="queryName"/>.
        /// </summary>
        /// <param name="queryName">
        /// The name of the query to perform.
        /// </param>
        /// <param name="dataGrid">
        /// The result grid on the page.
        /// </param>
        /// <exception cref="InvalidOperationException">
        /// No query with the specified <paramref name="queryName"/> exists.
        /// </exception>
        /// <returns>
        /// The information for the executed query.
        /// </returns>
        public static SAEQueryInfo ExecuteQuery(string queryName, CommonDataGrid dataGrid)
        {
            if (SAEDatabaseQueryExecutor.QueryList.ContainsKey(queryName) == false)
            {
                throw new InvalidOperationException(
                    "Query '" + queryName + "' is not a recognized database query.");
            }

            var queryInfo = SAEDatabaseQueryExecutor.QueryList[queryName];

            if (queryInfo.Method != null)
            {
                queryInfo.Method(queryInfo, dataGrid);
            }

            return queryInfo;
        }

        /// <summary>
        /// Exports the results of a query found in the specified <paramref name="dataGrid"/>.
        /// </summary>
        /// <param name="queryName">
        /// The name of the query that was run to fill the <paramref name="dataGrid"/>.
        /// </param>
        /// <param name="dataGrid">
        /// The <see cref="CommonDataGrid"/> containing query results to export.
        /// </param>
        /// <returns>
        /// The path to the export file.
        /// </returns>
        public static string ExportDataGrid(string queryName, CommonDataGrid dataGrid)
        {
            if (SAEDatabaseQueryExecutor.QueryList.ContainsKey(queryName) == false)
            {
                throw new InvalidOperationException(
                    "Query '" + queryName + "' is not a recognized database query.");
            }

            var queryInfo = SAEDatabaseQueryExecutor.QueryList[queryName];

            var tempFileName = TempFileUtils.Name2Path(queryInfo.ExportFilename);

            var dataTable = (DataTable)dataGrid.DataSource;

            using (var writer = new StreamWriter(tempFileName))
            {
                writer.Write(queryInfo.ExportFileHeader + "\r\n\r\n");

                foreach (DataRow row in dataTable.Rows)
                {
                    var csvLine = from cell in row.ItemArray
                                  select cell == null ? string.Empty : DataParsing.sanitizeForCSV(cell.ToString());

                    writer.Write(string.Join(",", csvLine) + "\r\n");
                }
            }

            return tempFileName;
        }

        /// <summary>
        /// Exports the list of TPO users for <code>PacUnion</code>.
        /// </summary>
        /// <returns>
        /// The path to the export file.
        /// </returns>
        public static string ExportPacUnionUsers()
        {
            var parameters = new SqlParameter[] 
            { 
                new SqlParameter("@BrokerId", SAEQueryInfo.PacUnionBrokerId) 
            };

            var tempFileName = TempFileUtils.Name2Path(SAEQueryInfo.PacUnionTPOQuery.ExportFilename);

            using (var fileWriter = new StreamWriter(tempFileName))
            using (var sqlReader = StoredProcedureHelper.ExecuteReader(SAEQueryInfo.PacUnionBrokerId, SAEQueryInfo.PacUnionTPOQuery.StoredProcedureName, parameters))
            {
                fileWriter.Write(SAEQueryInfo.PacUnionTPOQuery.ExportFileHeader + "\r\n\r\n");

                while (sqlReader.Read())
                {
                    var csvLine = from column in Enumerable.Range(0, sqlReader.FieldCount).Select(sqlReader.GetString)
                                  select column == null ? string.Empty : DataParsing.sanitizeForCSV(column);

                    fileWriter.Write(string.Join(",", csvLine) + "\r\n");
                }
            }

            return tempFileName;
        }

        /// <summary>
        /// Returns a list of names for the SAE queries that can
        /// be performed.
        /// </summary>
        /// <returns>
        /// A list containing all SAE database queries that can be
        /// performed.
        /// </returns>
        public static IEnumerable<string> GetQueryNamesList()
        {
            return SAEDatabaseQueryExecutor.QueryList.Keys;
        }

        /// <summary>
        /// Executes the "No Loan Product Type" query.
        /// </summary>
        /// <param name="queryInfo">
        /// The information for the query.
        /// </param>
        /// <param name="dataGrid">
        /// The grid to write results to.
        /// </param>
        internal static void NoLoanProductTypeQuery(
            SAEQueryInfo queryInfo, 
            CommonDataGrid dataGrid)
        {
            var dataTable = StoredProcedureHelper.ExecuteDataTable(
                queryInfo.DataSource, 
                queryInfo.StoredProcedureName, 
                parameters: null);

            SAEDatabaseQueryExecutor.TranslateBrokerIdToBrokerName(dataTable);

            dataGrid.Columns.Clear();

            dataGrid.Columns.Add(SAEDatabaseQueryExecutor.NewBoundColumn("BrokerNm", "Broker Name", "BrokerNm"));
            dataGrid.Columns.Add(SAEDatabaseQueryExecutor.NewBoundColumn("FolderName", "Folder Name", "FolderName"));
            dataGrid.Columns.Add(SAEDatabaseQueryExecutor.NewBoundColumn("TemplateName", "Template Name", "TemplateName"));
            dataGrid.Columns.Add(SAEDatabaseQueryExecutor.NewBoundColumn("CreatedD", "Creation Date", "CreatedD"));

            dataTable.DefaultView.Sort = "BrokerNm, FolderName, TemplateName, CreatedD DESC";

            dataGrid.DataSource = dataTable.DefaultView;
            dataGrid.DataBind();
        }

        /// <summary>
        /// Executes the "List all PMLMASTER Loan Programs" query.
        /// </summary>
        /// <param name="queryInfo">
        /// The information for the query.
        /// </param>
        /// <param name="dataGrid">
        /// The grid to write results to.
        /// </param>
        internal static void ListPmlMasterProgramsQuery(
            SAEQueryInfo queryInfo, 
            CommonDataGrid dataGrid)
        {
            var parameters = new SqlParameter[] 
            { 
                new SqlParameter("@PmlMasterBrokerId", BrokerDB.PmlMaster)
            };

            var dataTable = StoredProcedureHelper.ExecuteDataTable(
                queryInfo.DataSource,
                queryInfo.StoredProcedureName,
                parameters);

            dataGrid.Columns.Clear();

            dataGrid.Columns.Add(SAEDatabaseQueryExecutor.NewBoundColumn("lLpInvestorNm", "Investor Name", "lLpInvestorNm"));
            dataGrid.Columns.Add(SAEDatabaseQueryExecutor.NewBoundColumn("ProductCode", "Product Code", "ProductCode"));
            dataGrid.Columns.Add(SAEDatabaseQueryExecutor.NewBoundColumn("Lien", "Lien", "lLienPosT"));
            dataGrid.Columns.Add(SAEDatabaseQueryExecutor.NewBoundColumn("lLpTemplateNm", "Program Name", "lLpTemplateNm"));

            dataGrid.DataSource = dataTable;
            dataGrid.DataBind();
        }

        /// <summary>
        /// Executes the "List Loan Programs with Invalid Investors"
        /// query.
        /// </summary>
        /// <param name="queryInfo">
        /// The information for the query.
        /// </param>
        /// <param name="dataGrid">
        /// The grid to write results to.
        /// </param>
        internal static void ListInvalidInvestorProgramsQuery(
            SAEQueryInfo queryInfo, 
            CommonDataGrid dataGrid)
        {
            var dataTable = StoredProcedureHelper.ExecuteDataTable(
                queryInfo.DataSource,
                queryInfo.StoredProcedureName,
                parameters: null);

            SAEDatabaseQueryExecutor.ExpandTemplatePath(dataTable);

            SAEDatabaseQueryExecutor.TranslateBrokerIdToBrokerName(dataTable);

            dataGrid.Columns.Clear();

            dataGrid.Columns.Add(SAEDatabaseQueryExecutor.NewBoundColumn("BrokerNm", "Broker Name", "BrokerNm"));
            dataGrid.Columns.Add(SAEDatabaseQueryExecutor.NewBoundColumn("lLpInvestorNm", "Investor Name", "lLpInvestorNm"));
            dataGrid.Columns.Add(SAEDatabaseQueryExecutor.NewBoundColumn("lLpTemplateNm", "Template Name", "lLpTemplateNm"));
            dataGrid.Columns.Add(SAEDatabaseQueryExecutor.NewBoundColumn("Master", "Master", "Master"));
            dataGrid.Columns.Add(SAEDatabaseQueryExecutor.NewBoundColumn("ProductCode", "Product Code", "ProductCode"));

            dataTable.DefaultView.Sort = "BrokerNm, lLpInvestorNm, lLpTemplateNm, Master, ProductCode";

            dataGrid.DataSource = dataTable.DefaultView;
            dataGrid.DataBind();
        }

        /// <summary>
        /// Executes the query to show loan limits.
        /// </summary>
        /// <param name="queryInfo">
        /// The information for the query.
        /// </param>
        /// <param name="dataGrid">
        /// The grid to write results to.
        /// </param>
        internal static void ShowLoanLimits(
            SAEQueryInfo queryInfo, 
            CommonDataGrid dataGrid)
        {
            var dataTable = StoredProcedureHelper.ExecuteDataTable(
                queryInfo.DataSource,
                queryInfo.StoredProcedureName,
                parameters: null);

            dataGrid.Columns.Clear();

            dataGrid.Columns.Add(SAEDatabaseQueryExecutor.NewBoundColumn("FipsCountyCode", "Fips Code", "FipsCountyCode"));
            dataGrid.Columns.Add(SAEDatabaseQueryExecutor.NewBoundColumn("Limit1Units", "Limit 1 Unit", "Limit1Units"));
            dataGrid.Columns.Add(SAEDatabaseQueryExecutor.NewBoundColumn("Limit2Units", "Limit 2 Unit", "Limit2Units"));
            dataGrid.Columns.Add(SAEDatabaseQueryExecutor.NewBoundColumn("Limit3Units", "Limit 3 Unit", "Limit3Units"));
            dataGrid.Columns.Add(SAEDatabaseQueryExecutor.NewBoundColumn("Limit4Units", "Limit 4 Unit", "Limit4Units"));
            dataGrid.Columns.Add(SAEDatabaseQueryExecutor.NewBoundColumn("LastRevised", "Last Revised", "LastRevised"));

            dataGrid.DataSource = dataTable;
            dataGrid.DataBind();
        }

        /// <summary>
        /// Executes the "List Brokers with 'Always Display Exact 
        /// Par Rate Option' Enabled" query.
        /// </summary>
        /// <param name="queryInfo">
        /// The information for the query.
        /// </param>
        /// <param name="dataGrid">
        /// The grid to write results to.
        /// </param>
        internal static void GetBrokersWithExactParRateOptionEnabledQuery(
            SAEQueryInfo queryInfo, 
            CommonDataGrid dataGrid)
        {
            var dataTable = StoredProcedureHelper.ExecuteDataTable(
                queryInfo.DataSource,
                queryInfo.StoredProcedureName,
                parameters: null);

            dataGrid.Columns.Clear();

            dataGrid.Columns.Add(SAEDatabaseQueryExecutor.NewBoundColumn("CustomerCode", "Customer Code", "CustomerCode"));
            dataGrid.Columns.Add(SAEDatabaseQueryExecutor.NewBoundColumn("CompanyName", "Company Name", "CompanyName"));

            dataGrid.DataSource = dataTable;
            dataGrid.DataBind();
        }

        /// <summary>
        /// Expands the names of the templates in the specified 
        /// <paramref name="dataTable"/>.
        /// </summary>
        /// <param name="dataTable">
        /// The <see cref="DataTable"/> containing template names
        /// to expand.
        /// </param>
        private static void ExpandTemplatePath(DataTable dataTable)
        {
            var folderDictionary = new Dictionary<Guid, Folder>();

            foreach (DataRow row in dataTable.Rows)
            {
                var folderId = row["folderid"] as Guid? ?? Guid.Empty;

                var templateName = (string)row["lLpTemplateNm"];

                row["lLpTemplateNm"] = new InvalidInvestorTemplate(templateName, folderId, folderDictionary).FullName;
            }
        }

        /// <summary>
        /// Adds the name of the broker to the rows of the specified 
        /// <paramref name="dataTable"/>.
        /// </summary>
        /// <param name="dataTable">
        /// The <see cref="DataTable"/> containing broker IDs that will
        /// be translated to broker names.
        /// </param>
        private static void TranslateBrokerIdToBrokerName(DataTable dataTable)
        {
            dataTable.Columns.Add("BrokerNm", typeof(string));

            var brokersList = new Dictionary<Guid, BrokerDbLite>();

            foreach (DataRow row in dataTable.Rows)
            {
                var brokerId = (Guid)row["BrokerId"];

                BrokerDbLite broker = null;

                if (brokersList.TryGetValue(brokerId, out broker) == false)
                {
                    broker = BrokerDbLite.RetrieveById(brokerId);

                    brokersList.Add(brokerId, broker);
                }

                row["BrokerNm"] = broker.BrokerNm;
            }
        }

        /// <summary>
        /// Creates and returns a new <see cref="BoundColumn"/> using 
        /// the specified <paramref name="data"/>, <paramref name="header"/>,
        /// and <paramref name="sort"/> rules.
        /// </summary>
        /// <param name="data">
        /// The data for the column.
        /// </param>
        /// <param name="header">
        /// The header for the bound column.
        /// </param>
        /// <param name="sort">
        /// The sort rules for the bound column.
        /// </param>
        /// <returns>
        /// A new <see cref="BoundColumn"/> with the specified information.
        /// </returns>
        private static BoundColumn NewBoundColumn(string data, string header, string sort)
        {
            if (data == null || header == null || sort == null)
            {
                throw new ArgumentNullException();
            }

            return new BoundColumn()
            {
                DataField = data,
                HeaderText = header,
                SortExpression = sort
            };
        }
    }
}
