﻿/// <copyright file="LPEInvalidInvestor.cs" company="MeridianLink">
///     Copyright (c) MeridianLink. All rights reserved.
/// </copyright>
/// <summary>
/// Author: Francesc Serra, Michael Leinweaver
/// Date:   2/5/2009, 2/5/2016
/// </summary>
namespace LendersOffice.LOAdmin.SAE
{
    using System;
    using System.Collections.Generic;
    using DataAccess;
    using LendersOffice.Common;

    /// <summary>
    /// Represents a container for invalid investor information.
    /// </summary>
    public class InvalidInvestorTemplate
    {
        /// <summary>
        /// Represents a separator between the names of folders in
        /// a path to a template.
        /// </summary>
        private const string FolderSeparator = "\\";

        /// <summary>
        /// The name of the template with an invalid investor.
        /// </summary>
        private string templateName;

        /// <summary>
        /// The internal path to the template used to build the full
        /// path string.
        /// </summary>
        private string pathInternal;

        /// <summary>
        /// The name of the containing folder for the template.
        /// </summary>
        private Folder folder;

        /// <summary>
        /// The lookup containing folders to search.
        /// </summary>
        private Dictionary<Guid, Folder> lookup;

        /// <summary>
        /// Initializes a new instance of the <see cref="InvalidInvestorTemplate"/> class
        /// with the specified <paramref name="name"/>, <paramref name="folder"/>, and
        /// <paramref name="lookup"/>.
        /// </summary>
        /// <param name="name">
        /// The name of the <see cref="InvalidInvestorTemplate"/>.
        /// </param>
        /// <param name="folder">
        /// The folder for the <see cref="InvalidInvestorTemplate"/>.
        /// </param>
        /// <param name="lookup">
        /// The dictionary lookup for the <see cref="InvalidInvestorTemplate"/>.
        /// </param>
        public InvalidInvestorTemplate(string name, Guid folder, Dictionary<Guid, Folder> lookup)
        {
            if (name == null || folder == null || lookup == null)
            {
                throw new ArgumentNullException();
            }

            this.templateName = name;
            this.folder = new Folder(folder, lookup);
            this.lookup = lookup;
        }

        /// <summary>
        /// Gets the value for the full name of the template with an invalid investor.
        /// </summary>
        /// <value>
        /// The string representing the full path and name of the template with an 
        /// invalid investor.
        /// </value>
        public string FullName
        {
            get
            {
                return this.Path + this.templateName;
            }
        }

        /// <summary>
        /// Gets the value for the path to the template with an invalid investor.
        /// </summary>
        /// <value>
        /// The string path to the template with an invalid investor.
        /// </value>
        private string Path
        {
            get
            {
                if (!string.IsNullOrEmpty(this.pathInternal))
                {
                    return InvalidInvestorTemplate.FolderSeparator + this.pathInternal;
                }
                else
                {
                    this.pathInternal = string.Empty;

                    var folder = this.folder;

                    while (folder.ParentID != Guid.Empty)
                    {
                        this.pathInternal = folder.Name + InvalidInvestorTemplate.FolderSeparator + this.pathInternal;

                        folder = this.lookup[folder.ParentID];

                        if (folder == null)
                        {
                            throw new CBaseException(ErrorMessages.Generic, "Lookup table failed!");
                        }
                    }
                }

                return this.pathInternal.Length == 0 ? string.Empty : InvalidInvestorTemplate.FolderSeparator + this.pathInternal;
            }
        }
    }
}
