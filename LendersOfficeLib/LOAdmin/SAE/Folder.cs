﻿/// <copyright file="LPEInvalidInvestor.cs" company="MeridianLink">
///     Copyright (c) MeridianLink. All rights reserved.
/// </copyright>
/// <summary>
/// Author: Francesc Serra, Michael Leinweaver
/// Date:   2/5/2009, 2/5/2016
/// </summary>
namespace LendersOffice.LOAdmin.SAE
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
using System.Data.SqlClient;
    using DataAccess;
    using LendersOffice.Common;

    /// <summary>
    /// Represents an LPE folder.
    /// </summary>
    public class Folder
    {
        /// <summary>
        /// The name of the folder.
        /// </summary>
        private string name;

        /// <summary>
        /// The ID of the parent folder.
        /// </summary>
        private Guid parentId;

        /// <summary>
        /// Determines whether the lookup dictionary has been filled.
        /// </summary>
        private bool filled = false;

        /// <summary>
        /// The lookup dictionary for folder names.
        /// </summary>
        private Dictionary<Guid, Folder> lookup;

        /// <summary>
        /// Initializes a new instance of the <see cref="Folder"/> class
        /// with the specified <paramref name="id"/> and <paramref name="lookup"/>.
        /// </summary>
        /// <param name="id">
        /// The ID of the <see cref="Folder"/>.
        /// </param>
        /// <param name="lookup">
        /// The lookup for the <see cref="Folder"/> to use.
        /// </param>
        public Folder(Guid id, Dictionary<Guid, Folder> lookup)
        {
            this.ID = id;
            this.filled = false;
            this.lookup = lookup;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Folder"/> class
        /// with the specified <paramref name="id"/>, <paramref name="lookup"/>,
        /// <paramref name="name"/>, and <paramref name="parent"/>.
        /// </summary>
        /// <param name="id">
        /// The ID of the <see cref="Folder"/>.
        /// </param>
        /// <param name="lookup">
        /// The lookup for the <see cref="Folder"/> to use.
        /// </param>
        /// <param name="name">
        /// The name of the <see cref="Folder"/>.
        /// </param>
        /// <param name="parent">
        /// The parent of the <see cref="Folder"/>.
        /// </param>
        public Folder(Guid id, Dictionary<Guid, Folder> lookup, string name, Guid parent)
        {
            this.ID = id;
            this.filled = true;
            this.parentId = parent;
            this.name = name;
            this.lookup = lookup;
        }

        /// <summary>
        /// Gets or sets the value for the ID of the <see cref="Folder"/>.
        /// </summary>
        /// <value>
        /// The GUID for the <see cref="Folder"/>.
        /// </value>
        public Guid ID { get; set; }

        /// <summary>
        /// Gets the value for the ID of the parent of the
        /// <see cref="Folder"/>.
        /// </summary>
        /// <value>
        /// The GUID for the parent of the <see cref="Folder"/>.
        /// </value>
        public Guid ParentID
        {
            get
            {
                if (!this.filled)
                {
                    this.Retrieve();
                }

                return this.parentId;
            }
        }

        /// <summary>
        /// Gets the value for the name of the <see cref="Folder"/>.
        /// </summary>
        /// <value>
        /// The string name for the <see cref="Folder"/>.
        /// </value>
        public string Name
        {
            get
            {
                if (!this.filled)
                {
                    this.Retrieve();
                }

                return this.name;
            }
        }

        /// <summary>
        /// Retrieves the hierarchy for the <see cref="Folder"/>.
        /// </summary>
        private void Retrieve()
        {
            if (this.lookup.ContainsKey(this.ID))
            {
                this.parentId = this.lookup[this.ID].ParentID;
                this.name = this.lookup[this.ID].Name;
            }
            else
            {
                var lookupId = this.ID;

                while (lookupId != Guid.Empty)
                {
                    var parentId = Guid.Empty;

                    if (!this.lookup.ContainsKey(lookupId))
                    {
                        var parameters = new SqlParameter[] { new SqlParameter("@FolderId", lookupId) };

                        using (var reader = StoredProcedureHelper.ExecuteReader(DataSrc.LpeSrc, "SAEDB_ListInvalidInvestorProgramsFolderLookup", parameters))
                        {
                            if (reader.Read())
                            {
                                var name = (string)reader["FolderName"];

                                parentId = reader["ParentFolderId"] as Guid? ?? Guid.Empty;

                                if (lookupId != this.ID)
                                {
                                    this.lookup.Add(lookupId, new Folder(lookupId, this.lookup, name, parentId));
                                }
                                else
                                {
                                    this.name = name;
                                    this.parentId = parentId;
                                    this.lookup.Add(this.ID, this);
                                }
                            }
                            else
                            {
                                throw new CBaseException(ErrorMessages.Generic, "Folder id '" + this.ID + "' not found.");
                            }
                        }
                    }

                    lookupId = parentId;
                }
            }

            this.filled = true;
        }
    }
}
