namespace LendersOffice.LOAdmin.Internal
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Text.RegularExpressions;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Security;

    #region InternalStrongPasswordStatus enum
    // OPM 24226 - Ethan
    //
    // enum type value for the Internal user to determine password strength
    public enum InternalStrongPasswordStatus 
	{
		OK,
		PasswordContainsProhibitedWord,
		PasswordMustBeAlphaNumeric,
		PasswordTooShort
	}
	#endregion


	public class InternalUserDB
	{
        #region Private member variables
        private string m_loginName;
        private bool m_isPasswordUpdate;
        private string m_firstName;
        private string m_lastName;
        private string m_permissions;
        private bool m_isActive;
		
		// OPM 23946 - Ethan
		//
		// added for the password checking functionality
		private string m_passwordHash;
        private string m_passwordSalt;

		// OPM 24342 - Ethan
		//
		// added to force internal users to change their passwords
		public readonly static DateTime MAX_SQL_SMALLDATETIME = new DateTime(2079, 1, 1);
		public readonly static DateTime MIN_SQL_SMALLDATETIME = new DateTime(1900, 1, 1);

		private DateTime m_passwordExpirationD = MAX_SQL_SMALLDATETIME;
		private bool m_isStrongPassword;

        #endregion

        #region Public getter/setter.

		// OPM 24342 - Ethan
		public bool hasStrongPassword
		{
			get { return m_isStrongPassword; }
			set { m_isStrongPassword = value; }
		}
        public Guid UserId { get; private set; } 

        public string LoginName 
        {
            get { return m_loginName; }
            set { m_loginName = value; }
        }
        public string FirstName 
        {
            get { return m_firstName; }
            set { m_firstName = value; }
        }
        public string LastName 
        {
            get { return m_lastName; }
            set { m_lastName = value; }
        }
        public string Permissions 
        {
            get { return m_permissions; }
            set { m_permissions = value; }
        }
        public bool IsActive 
        {
            get { return m_isActive; }
            set { m_isActive = value; }
        }
        public bool IsNew { get; private set; }

        /// <summary>
        /// Has password encoding been updated to the latest version?
        /// </summary>
        public bool IsUpdatedPassword { get; private set; }
        #endregion



		public InternalUserDB()
		{
            this.IsNew = true;
		}
        public InternalUserDB(Guid userID) 
        {
            this.IsNew = false;
            this.UserId = userID;
        }



		public void updatePassword(string newPassword)
		{
            m_passwordHash = EncryptionHelper.GeneratePBKDF2Hash(newPassword, out m_passwordSalt);
            m_isPasswordUpdate = true;
			m_isStrongPassword = true;
			m_passwordExpirationD = MAX_SQL_SMALLDATETIME;
            IsUpdatedPassword = true;
		}

		


		// OPM 24342 - Ethan
		//
		// function will modify an internal user account so that the next time he/she logs in, a required password change is forced
		public void forcePasswordChangeNextLogin()
		{
			m_passwordExpirationD = MIN_SQL_SMALLDATETIME;
			m_isStrongPassword = false;
		}
		



		// OPM 23946 - Ethan
		// 
		// allows functionality for password validation
		public bool IsCurrentPassword(string password) 
		{
            if (IsUpdatedPassword)
            {
                return EncryptionHelper.ValidatePBKDF2Hash(password, m_passwordHash, m_passwordSalt);
            }
            else
            {
                string passwordHash = Tools.HashOfPassword(password);
                return passwordHash == m_passwordHash;
            }
		}




		// OPM 24226 - Ethan
		// 
		// This method will test the password for the following criteria:
		//     a) Password cannot contain first name or last name or login name.
		//     b) Password must have minimum of 6 characters, in which at least one must be numeric and at least one must be a character
		public InternalStrongPasswordStatus IsStrongPassword(string password) 
		{
			InternalStrongPasswordStatus ret = InternalStrongPasswordStatus.OK;

			ArrayList prohibitWords = new ArrayList();

			// add to the list of prohibited words (login name, first name, last name)
			if (null != m_firstName && m_firstName.TrimWhitespaceAndBOM() != "")
			{
				prohibitWords.Add(m_firstName.ToLower());
			}

			if (null != m_lastName && m_lastName.TrimWhitespaceAndBOM() != "")
			{
				prohibitWords.Add(m_lastName.ToLower());
			}

			if (null != m_loginName && m_loginName.TrimWhitespaceAndBOM() != "")
			{
				prohibitWords.Add(m_loginName.ToLower());
			}

			// now check to make sure no prohibited word is in the password
            string str = password.ToLower();
			foreach (string word in prohibitWords) 
			{
                if (str.IndexOf(word) > -1) 
				{
					ret = InternalStrongPasswordStatus.PasswordContainsProhibitedWord;
					break;
				}
			}

			// do the last check, making sure there's at least one number and one character and that the length is greater than 6
			if (ret == InternalStrongPasswordStatus.OK) 
			{
				if (!Regex.IsMatch(password, "((.*[a-zA-Z]+.*[0-9]+.*)|(.*[0-9]+.*[a-zA-Z]+.*))"))
				{
					ret = InternalStrongPasswordStatus.PasswordMustBeAlphaNumeric;
				}
				else if (password.Length < 6) 
				{
					ret = InternalStrongPasswordStatus.PasswordTooShort;
				}
			}

			return ret;
		}





		// OPM 24342 - Ethan
		//
		// this handles an internal user's password expiration (although current functionality isn't really determined by date)
		// min and max date for password expiration are for possible future changes to the internal user
		private static DateTime ToDateTime(object o) 
		{
			try 
			{
				if (null == o || Convert.IsDBNull(o))
					return MAX_SQL_SMALLDATETIME;
				else
					return Convert.ToDateTime(o);
			} 
			catch (FormatException) { }
            catch (InvalidCastException) { }

            return MAX_SQL_SMALLDATETIME;
        }

        private DbConnectionInfo GetConnectionInfo()
        {
            return DbConnectionInfo.DefaultConnectionInfo;
        }

		public bool Retrieve() 
		{
			if (this.IsNew) 
			{
				return false;
			}
			bool successful = false;

			SqlParameter[] parameters = new SqlParameter[] { new SqlParameter("@UserID", this.UserId) };
			using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(GetConnectionInfo(), "RetrieveInternalUserByID", parameters))
			{
				if (reader.Read()) 
				{
					m_loginName   = (string) reader[ "LoginNm"     ];
					m_firstName   = (string) reader[ "FirstNm"     ];
					m_lastName    = (string) reader[ "LastNm"      ];
                    m_permissions = (string) reader[ "Permissions" ];
					m_isActive    = (bool)   reader[ "IsActive"    ];



					// OPM 23946 - Ethan
					//
					// added m_passwordHash in order to add functionality for password matching, similar to the EmployeeDB class
					m_passwordHash = (string) reader[ "PasswordHash"];
                    m_passwordSalt = (string)reader["PasswordSalt"]; // OPM 194431 - added password salt for more security.

                    IsUpdatedPassword = (bool) reader["IsUpdatedPassword"]; // OPM 194431

					// OPM 24342 - Ethan
					m_passwordExpirationD = ToDateTime( reader[ "PasswordExpirationD" ] );

					if ( DateTime.Now.CompareTo(m_passwordExpirationD) > 0 )
					{
						m_isStrongPassword = false;
					}
					else
					{
						m_isStrongPassword = true;
					}

					successful = true;
				}
			}

			return successful;
		}


        public static DataSet GetInternalUserList()
        {
            DataSet users = new DataSet();

            SqlParameter[] parameters = {
                                            new SqlParameter("@ShowInactive", 1)
                                        };

            DataSetHelper.Fill(users, DataSrc.LOShare, "List_InternalAdminAccounts", parameters);
            return users;
        }

        public bool Save() 
        {
            bool successful = false;

            string procedureName = null;
            List<SqlParameter> parameters = new List<SqlParameter>();

            if (this.IsNew) 
            {
                this.UserId = Guid.NewGuid();

                procedureName = "CreateInternalUser";
            } 
            else 
            {
                procedureName = "UpdateInternalUser";
            }

            parameters.Add(new SqlParameter("@UserID", this.UserId));
            parameters.Add(new SqlParameter("@LoginName", this.LoginName));
            if (m_isPasswordUpdate && !string.IsNullOrEmpty(m_passwordHash) && !string.IsNullOrEmpty(m_passwordSalt)) 
            {
                parameters.Add(new SqlParameter("@PasswordHash", m_passwordHash));
                parameters.Add(new SqlParameter("@PasswordSalt", m_passwordSalt));

                // OPM 194431 - Mark password as updated
                parameters.Add(new SqlParameter("@IsUpdatedPassword", true));   // Actual value of IsUpdatedPassword does not matter. If password is being updated then it's true.
            }

            parameters.Add(new SqlParameter("@FirstNm", this.FirstName));
            parameters.Add(new SqlParameter("@LastNm", this.LastName));
            parameters.Add(new SqlParameter("@Permissions", this.Permissions));
            parameters.Add(new SqlParameter("@IsActive", this.IsActive));


			// OPM 24342 - Ethan
			parameters.Add(new SqlParameter("@PasswordExpirationD", m_passwordExpirationD));

            StoredProcedureHelper.ExecuteNonQuery(GetConnectionInfo(), procedureName, 0, parameters);
            PrincipalFactory.AllUserSharedDbSync(Guid.Empty, this.UserId, this.LoginName);

            successful = true;
            if (this.IsNew)
            {
                this.IsNew = false;
            }

            return successful;
        }

	}
}
