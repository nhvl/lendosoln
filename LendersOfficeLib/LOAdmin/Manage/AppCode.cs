﻿/// <copyright file="ManageModifiedLoans.aspx.cs" company="MeridianLink">
///     Copyright (c) MeridianLink. All rights reserved.
/// </copyright>
/// <summary>
/// Author: Michael Leinweaver
/// Date:   3/18/2016
/// </summary>
namespace LendersOffice.LOAdmin.Manage
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
using System.Data.SqlClient;
    using System.Runtime.Serialization;
    using DataAccess;

    /// <summary>
    /// Provides a container for app code information.
    /// </summary>
    [DataContract]
    public class AppCode
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AppCode"/>
        /// class using the specified <paramref name="reader"/>.
        /// </summary>
        /// <param name="reader">
        /// The <see cref="DbDataReader"/> containing information
        /// to populate the app code.
        /// </param>
        private AppCode(DbDataReader reader)
        {
            this.Id = reader.SafeGuid("AppCode");
            this.Name = reader.SafeString("AppName");
        }

        /// <summary>
        /// Gets the value for the id of the app code.
        /// </summary>
        /// <value>
        /// The <see cref="Guid"/> id of the app code.
        /// </value>
        [DataMember]
        public Guid Id { get; private set; }

        /// <summary>
        /// Gets the value for the name of the app code.
        /// </summary>
        /// <value>
        /// The <see cref="string"/> name of the app code.
        /// </value>
        [DataMember]
        public string Name { get; private set; }

        /// <summary>
        /// Retrieves app code information for the broker with
        /// the specified <paramref name="brokerId"/>.
        /// </summary>
        /// <param name="brokerId">
        /// The <see cref="Guid"/> for the broker.
        /// </param>
        /// <returns>
        /// A list of <see cref="AppCode"/> records for the
        /// broker.
        /// </returns>
        public static List<AppCode> Retrieve(Guid brokerId)
        {
            var appCodes = new List<AppCode>();

            SqlParameter[] parameters = { new SqlParameter("@BrokerId", brokerId) };

            using (var reader = StoredProcedureHelper.ExecuteReader(brokerId, "BROKER_WEBSERVICE_APP_CODE_Fetch", parameters))
            {
                while (reader.Read())
                {
                    appCodes.Add(new AppCode(reader));
                }
            }

            return appCodes;
        }
    }
}
