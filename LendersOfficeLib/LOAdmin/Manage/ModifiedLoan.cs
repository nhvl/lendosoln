﻿/// <copyright file="ManageModifiedLoans.aspx.cs" company="MeridianLink">
///     Copyright (c) MeridianLink. All rights reserved.
/// </copyright>
/// <summary>
/// Author: Michael Leinweaver
/// Date:   3/18/2016
/// </summary>
namespace LendersOffice.LOAdmin.Manage
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Xml.Linq;
    using Common;
    using DataAccess;

    /// <summary>
    /// Provides a container for modified loan information.
    /// </summary>
    [DataContract]
    public class ModifiedLoan
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ModifiedLoan"/>
        /// class using the specified <paramref name="reader"/>.
        /// </summary>
        /// <param name="reader">
        /// The <see cref="DbDataReader"/> containing information to
        /// populate the modified loan.
        /// </param>
        private ModifiedLoan(DbDataReader reader)
        {
            this.LoanId = reader.SafeGuid("sLId");
            this.LoanName = reader.SafeString("sLNm");
            this.LastModifiedDate = reader.SafeDateTime("LastModifiedD").ToString();
            this.OldLoanName = reader.SafeString("sOldLNm");
            this.BorrowerName = reader.SafeString("aBNm");
            this.LoanStatus = ((E_sStatusT)reader.SafeInt("sStatusT")).ToString("G");
            this.IsValid = reader.SafeBool("IsValid") ? "Yes" : "No";
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ModifiedLoan"/> class
        /// with the specified <paramref name="loanId"/>, <paramref name="loanName"/>,
        /// and <paramref name="updateStatus"/>.
        /// </summary>
        /// <param name="loanId">
        /// The <see cref="Guid"/> for the loan.
        /// </param>
        /// <param name="loanName">
        /// The name of the loan.
        /// </param>
        /// <param name="updateStatus">
        /// The update status for the modified loan.
        /// </param>
        /// <param name="statusLog">
        /// Optional log content corresponding to the <paramref name="updateStatus"/>.
        /// </param>
        private ModifiedLoan(Guid loanId, string loanName, string updateStatus, string statusLog = null)
        {
            this.LoanId = loanId;
            this.LoanName = loanName;
            this.UpdateStatus = updateStatus;
            this.UpdateStatusLog = statusLog ?? string.Empty;
        }

        /// <summary>
        /// Gets the value for the id of the loan.
        /// </summary>
        /// <value>
        /// The <see cref="Guid"/> for the loan.
        /// </value>
        [DataMember]
        public Guid LoanId { get; private set; }

        /// <summary>
        /// Gets the value for the name of the loan.
        /// </summary>
        /// <value>
        /// The <see cref="string"/> name of the loan.
        /// </value>
        [DataMember]
        public string LoanName { get; private set; }

        /// <summary>
        /// Gets the value for the last modified date 
        /// of the loan.
        /// </summary>
        /// <value>
        /// The <see cref="string"/> last modified date
        /// of the loan.
        /// </value>
        /// <remarks>
        /// The last modified date is stored as a string
        /// instead of a DateTime object to make parsing
        /// the serialized date on the page easier.
        /// </remarks>
        [DataMember]
        public string LastModifiedDate { get; private set; }

        /// <summary>
        /// Gets the value for the old name of the loan.
        /// </summary>
        /// <value>
        /// The <see cref="string"/> old name for the loan.
        /// </value>
        [DataMember]
        public string OldLoanName { get; private set; }

        /// <summary>
        /// Gets the value for the name of the borrower
        /// for the loan.
        /// </summary>
        /// <value>
        /// The <see cref="string"/> name for the borrower 
        /// of the loan.
        /// </value>
        [DataMember]
        public string BorrowerName { get; private set; }

        /// <summary>
        /// Gets the value for the status of the loan.
        /// </summary>
        /// <value>
        /// The <see cref="string"/> status of the loan.
        /// </value>
        /// <remarks>
        /// The loan status is stored as a string to prevent
        /// having to map between the enumeration name and
        /// value on the page.
        /// </remarks>
        [DataMember]
        public string LoanStatus { get; private set; }

        /// <summary>
        /// Gets the value for whether the loan is valid.
        /// </summary>
        /// <value>
        /// Yes if the loan is valid, No otherwise.
        /// </value>
        /// <remarks>
        /// This boolean is stored as a string to save a 
        /// check in the UI when displaying this value.
        /// </remarks>
        [DataMember]
        public string IsValid { get; private set; }

        /// <summary>
        /// Gets the value for the update status of the
        /// loan.
        /// </summary>
        /// <value>
        /// The <see cref="string"/> update status for 
        /// the loan.
        /// </value>
        [DataMember]
        public string UpdateStatus { get; private set; }

        /// <summary>
        /// Gets the value for the update status log of the
        /// loan.
        /// </summary>
        /// <value>
        /// The <see cref="string"/> update status log for 
        /// the loan.
        /// </value>
        [DataMember]
        public string UpdateStatusLog { get; private set; }

        /// <summary>
        /// Retrieves modified loan information using the 
        /// specified <paramref name="appCodeId"/> for the 
        /// broker with the specified <paramref name="brokerId"/>.
        /// </summary>
        /// <param name="brokerId">
        /// The <see cref="Guid"/> of the broker.
        /// </param>
        /// <param name="appCodeId">
        /// The <see cref="Guid"/> of the app code.
        /// </param>
        /// <returns>
        /// A list of loans modified within the last 90 days.
        /// </returns>
        public static List<ModifiedLoan> Search(Guid brokerId, Guid appCodeId)
        {
            var modifiedLoans = new List<ModifiedLoan>();

            SqlParameter[] parameters = 
            {
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@AppCode", appCodeId)
            };

            using (var reader = StoredProcedureHelper.ExecuteReader(brokerId, "ListIntegrationModifiedFilesByBrokerId", parameters))
            {
                while (reader.Read())
                {
                    modifiedLoans.Add(new ModifiedLoan(reader));
                }
            }

            return modifiedLoans;
        }

        /// <summary>
        /// Marks loans in the specified <paramref name="loanList"/> as modified
        /// using the specified <paramref name="appCodeId"/> for the broker
        /// with the specified <paramref name="brokerId"/>. A boolean indicates
        /// if a loan's name or id is specified.
        /// </summary>
        /// <param name="brokerId">
        /// The broker for the modified loans.
        /// </param>
        /// <param name="appCodeId">
        /// The <see cref="AppCode"/> that will mark the loan as modified.
        /// </param>
        /// <param name="loanList">
        /// The list of loan separated by newlines.
        /// </param>
        /// <param name="useLoanName">
        /// True if <paramref name="loanList"/> is comprised of loan names, false
        /// if <paramref name="loanList"/> is comprised directly of loan ids.
        /// </param>
        /// <returns>
        /// A list representing the results for the update.
        /// </returns>
        public static List<ModifiedLoan> Update(Guid brokerId, Guid appCodeId, string loanList, bool useLoanName)
        {
            var modifiedLoans = new List<ModifiedLoan>();

            var updateIndex = 0;

            foreach (var loan in loanList.Split(new string[] { "\n" }, StringSplitOptions.RemoveEmptyEntries))
            {
                // We'll initialize the tuple here so that in case the 
                // parsing logic fails, we can sill use the tuple's data
                // when adding a "FAIL" loan to the list.
                var loanNumberIdPair = new LoanNumberIdPair(loan, Guid.Empty);

                try
                {
                    if (useLoanName)
                    {
                        loanNumberIdPair = ModifiedLoan.TranslateNameToId(brokerId, loan);
                    }
                    else
                    {
                        loanNumberIdPair = ModifiedLoan.ObtainLoanId(brokerId, loan);
                    }

                    SqlParameter[] parameters =
                    {
                        new SqlParameter("@BrokerId", brokerId),
                        new SqlParameter("@AppCode", appCodeId),
                        new SqlParameter("@LoanName", loanNumberIdPair.LoanNumber),
                        new SqlParameter("@LoanId", loanNumberIdPair.LoanId)
                    };

                    StoredProcedureHelper.ExecuteNonQuery(brokerId, "TrackIntegrationModifiedFile_SpecificAppCode", 1, parameters);

                    modifiedLoans.Add(new ModifiedLoan(loanNumberIdPair.LoanId, loanNumberIdPair.LoanNumber, "OK"));
                }
                catch (SystemException exc)
                {
                    Tools.LogError("Update failed for loan " + loanNumberIdPair.LoanNumber, exc);

                    modifiedLoans.Add(new ModifiedLoan(
                        loanNumberIdPair.LoanId, 
                        loanNumberIdPair.LoanNumber, 
                        "FAIL", 
                        "See PB for full exception details: " + exc.ToString().Truncate(200)));
                }

                // We need to update loans in batches to prevent all of the loans
                // from having the same last modified date.
                if (++updateIndex > 5)
                {
                    updateIndex = 0;
                    
                    Tools.SleepAndWakeupRandomlyWithin(2000, 2500, logWarning: false);
                }
            }

            return modifiedLoans;
        }

        /// <summary>
        /// Adds the given list of loans to the modified list for the given brokerId and app code.
        /// </summary>
        /// <param name="brokerId">The Broker ID for the lender that the loans belong to.</param>
        /// <param name="appCodeId">The app code for the application that is tracking modified files.</param>
        /// <param name="loanList">A delimited list of loan IDs to add to the modified list.</param>
        /// <param name="splitString">The string to use as a delimiter for <paramref name="loanList"/>.</param>
        /// <param name="useLoanName">Whether or not to use the loan name as the ID in <paramref name="loanList"/>. If false, uses Loan IDs.</param>
        /// <returns>A object that contains a message and a invaid loan list.</returns>
        public static object UpdateInBatch(Guid brokerId, Guid appCodeId, string loanList, string splitString, bool useLoanName)
        {
            var loanEleList = new List<XElement>();
            var invalidLoans = new List<ModifiedLoan>();

            foreach (var loan in loanList.Split(new string[] { "\n" }, StringSplitOptions.RemoveEmptyEntries))
            {
                var loanNumberIdPair = new LoanNumberIdPair(loan, Guid.Empty);

                try
                {
                    if (useLoanName)
                    {
                        loanNumberIdPair = ModifiedLoan.TranslateNameToId(brokerId, loan);
                    }
                    else
                    {
                        loanNumberIdPair = ModifiedLoan.ObtainLoanId(brokerId, loan);
                    }

                    var ele = new XElement(
                                    "row",
                                    new XAttribute("sLId", loanNumberIdPair.LoanId));
                    loanEleList.Add(ele);
                }
                catch (SystemException exc)
                {
                    Tools.LogError("Update failed for loan " + loanNumberIdPair.LoanNumber, exc);

                    invalidLoans.Add(new ModifiedLoan(
                        loanNumberIdPair.LoanId,
                        loanNumberIdPair.LoanNumber,
                        "FAIL",
                        "See PB for full exception details: " + exc.ToString().Truncate(200)));
                }
            }

            string msg = string.Empty;
            if (loanEleList.Count > 0)
            {
                // Stored procedure expects format like root/row/@sLId
                // e.g. <root> <row sLId = "00000000-0000-0000-0000-000000000000" /> <row sLId = "00000000-0000-0000-0000-000000000001" /> <root>
                XElement loanIdsXml = new XElement("root", loanEleList);

                SqlParameter[] parameters =
                {
                    new SqlParameter("@BrokerId", brokerId),
                    new SqlParameter("@AppCode", appCodeId),
                    new SqlParameter("@LoanIdListXml", loanIdsXml.ToString(SaveOptions.DisableFormatting)),
                    new SqlParameter("@NewestLastModified", DateTime.Now)
                };

                StoredProcedureHelper.ExecuteNonQuery(brokerId, "TrackIntegrationModifiedFile_AddFilesByIdList", 1, parameters);

                msg = $"Adding {loanEleList.Count} loan IDs to modified list for Broker ID " + brokerId + " and App Code " + appCodeId + " was successful.";
            }
            
            return new { Message = msg, InvalidLoans = SerializationHelper.JsonNetSerialize(invalidLoans) };
        }

        /// <summary>
        /// Adds all of the lender's valid, non-template, non-sandbox, non-test loans to the modified loan list with the specified app code.
        /// </summary>
        /// <param name="brokerId">The brokerId of the lender to add loans for.</param>
        /// <param name="appCodeId">The app code for the application that is tracking modified files.</param>
        /// <param name="markValidLoans">True if valid loans should be marked, false if invalid loans should be marked.</param>
        /// <returns>Whether or not the operation to add all loans completed successfully.</returns>
        public static string AddAllLoansToModifiedList(Guid brokerId, Guid appCodeId, bool markValidLoans)
        {
            SqlParameter[] parameters =
            {
                    new SqlParameter("@BrokerId", brokerId),
                    new SqlParameter("@AppCode", appCodeId),
                    new SqlParameter("@NewestLastModified", DateTime.Now),
                    new SqlParameter("@MarkValidLoans", markValidLoans)
            };

            StoredProcedureHelper.ExecuteNonQuery(brokerId, "TrackIntegrationModifiedFile_AddAllFilesToModifiedList", 1, parameters);

            return "Adding all loan IDs to modified list for Broker ID " + brokerId + " and App Code " + appCodeId + " was successful.";
        }

        /// <summary>
        /// Removes loans from the modified list for the given broker older than the given Last Modified date.
        /// </summary>
        /// <param name="brokerId">The brokerId of the lender to clear loans for.</param>
        /// <param name="appCodeId">The app code for the application that is tracking modified files.</param>
        /// <param name="lastModified">The modified list will be cleared of loans with a Last Modified Date prior to this date.</param>
        /// <returns>A status message describing whether or not the operation to clear loans completed successfully.</returns>
        public static string ClearModifiedList(Guid brokerId, Guid appCodeId, DateTime? lastModified = null)
        {
            SqlParameter[] parameters =
            {
                    new SqlParameter("@BrokerId", brokerId),
                    new SqlParameter("@AppCode", appCodeId),
                    new SqlParameter("@LastModifiedD", lastModified ?? DateTime.Now)
            };

            StoredProcedureHelper.ExecuteNonQuery(brokerId, "ClearIntegrationModifiedFilesByBrokerId", nRetry: 1, parameters: parameters);

            return "Removing loan IDs modified before " + lastModified + " from the modified list for Broker ID " + brokerId + " and App Code " + appCodeId + " was successful.";
        }

        /// <summary>
        /// Translates the specified <paramref name="loanName"/> to a 
        /// loan id.
        /// </summary>
        /// <param name="brokerId">
        /// The id of the broker for the <paramref name="loanName"/>.
        /// </param>
        /// <param name="loanName">
        /// The loan name to translate.
        /// </param>
        /// <returns>
        /// An <see cref="Tuple"/> containing the name and translated
        /// id for the loan.
        /// </returns>
        /// <exception cref="InvalidOperationException">
        /// The loan could not be translated as it does not exist for
        /// the specified broker.
        /// </exception>
        private static LoanNumberIdPair TranslateNameToId(Guid brokerId, string loanName)
        {
            var loanId = Tools.GetLoanIdByLoanName(brokerId, loanName);

            if (loanId == Guid.Empty)
            {
                throw new InvalidOperationException("Loan '" + loanName + "' does not exist for this broker.");
            }

            return new LoanNumberIdPair(loanName, loanId);
        }

        /// <summary>
        /// Parses the loan id in the specified <paramref name="stringLoanId"/>.
        /// </summary>
        /// <param name="brokerId">
        /// The <see cref="Guid"/> for the broker of the <paramref name="stringLoanId"/>.
        /// </param>
        /// <param name="stringLoanId">
        /// The string loan id to parse.
        /// </param>
        /// <returns>
        /// An <see cref="Tuple"/> containing the the parsed loan id and 
        /// corresponding loan name.
        /// </returns>
        /// <exception cref="InvalidOperationException">
        /// <paramref name="stringLoanId"/> is not a valid <see cref="Guid"/>
        /// format, <paramref name="stringLoanId"/> was parsed as <see cref="Guid.Empty"/>,
        /// or <paramref name="stringLoanId"/> does not exist for the specified broker.
        /// </exception>
        private static LoanNumberIdPair ObtainLoanId(Guid brokerId, string stringLoanId)
        {
            var parsedGuid = Guid.Empty;

            if (!Guid.TryParse(stringLoanId, out parsedGuid))
            {
                throw new InvalidOperationException("Could not parse '" + stringLoanId + "' as a GUID.");
            }

            if (parsedGuid == Guid.Empty)
            {
                throw new InvalidOperationException("The empty GUID cannot be passed as a loan id.");
            }

            var loanName = Tools.GetLoanNameByLoanId(brokerId, parsedGuid);

            if (loanName == string.Empty)
            {
                throw new InvalidOperationException("Loan " + parsedGuid + " does not exist for this broker.");
            }

            return new LoanNumberIdPair(loanName, parsedGuid);
        }

        /// <summary>
        /// Contains a Loan Number and ID that both correspond to the same loan.
        /// </summary>
        private struct LoanNumberIdPair
        {
            /// <summary>
            /// The loan number for the loan.
            /// </summary>
            public readonly string LoanNumber;

            /// <summary>
            /// The loan ID for the loan.
            /// </summary>
            public readonly Guid LoanId;

            /// <summary>
            /// Initializes a new instance of the <see cref="LoanNumberIdPair"/> struct.
            /// </summary>
            /// <param name="loanNumber">The loan number.</param>
            /// <param name="loanId">The loan ID.</param>
            public LoanNumberIdPair(string loanNumber, Guid loanId)
            {
                this.LoanNumber = loanNumber;
                this.LoanId = loanId;
            }
        }
    }
}
