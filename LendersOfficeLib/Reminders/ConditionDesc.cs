﻿using System;

namespace LendersOffice.Reminders
{
    /// <summary>
    /// Hold simple description of a condition task.
    /// </summary>

    public class ConditionDesc
    {
        private Guid m_Id = Guid.Empty;
        private string m_Category = "";
        private string m_Description = "";
        private string m_DateDone = "";
        private string m_CondCompletedByUserNm = string.Empty;
        private bool m_IsDone = false;
        private bool m_IsHidden = false;
        private string m_CondDeleteByUserNm = string.Empty;
        private string m_CondDeleteD = string.Empty;

        public Guid Id
        {
            set { m_Id = value; }
            get { return m_Id; }
        }
        public string Category
        {
            set { m_Category = value; }
            get { return m_Category; }
        }

        public string Description
        {
            set { m_Description = value; }
            get { return m_Description; }
        }

        public string DateDone
        {
            set { m_DateDone = value; }
            get { return m_DateDone; }
        }

        public bool IsDone
        {
            set { m_IsDone = value; }
            get { return m_IsDone; }
        }
        public bool IsHidden
        {
            set { m_IsHidden = value; }
            get { return m_IsHidden; }
        }

        public string CondCompletedByUserNm
        {
            set { m_CondCompletedByUserNm = value; }
            get { return m_CondCompletedByUserNm; }
        }

        public string CondDeleteByUserNm
        {
            set { m_CondDeleteByUserNm = value; }
            get { return m_CondDeleteByUserNm; }
        }
        public string CondDeleteD
        {
            set { m_CondDeleteD = value; }
            get { return m_CondDeleteD; }
        }



    }
}
