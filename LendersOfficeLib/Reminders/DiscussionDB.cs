using System;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.Reminders
{
    /// <summary>
    /// Provide simple wrapper for discussion log items
    /// in the database.  For now, we allow read only
    /// access.
    /// </summary>

    public class CDiscussion
    {
		/// <summary>
		/// Each discussion contains the fields of a particular
		/// topic.  Associated notifications are bound to this
		/// object at a higher level through the disucssion id.
		/// </summary>

        private Guid m_DiscId; // discussion topic id
        private DataRow m_Row; // properties of topic

        public Guid Id
        {
            // Access member.

            get
            {
                return m_DiscId;
            }
        }

        #region ( Discussion log fields )

        /// <summary>
        /// These fields read from the row as initialized during
        /// construction or explicit init.
        /// </summary>

        protected String GetStr( String sFieldName , Type tField )
        {
            // Retrieve member.

            object o = m_Row == null ? null : m_Row[ sFieldName ];

            if( o != null && o is DBNull == false )
            {
                if( tField.IsEnum == true )
                {
                    return Enum.GetName( tField , o );
                }
                else
                {
                    return o.ToString();
                }
            }

            return "";
        }

		protected DateTime GetDtm( String sFieldName )
		{
			// Retrieve member.

			object o = m_Row == null ? null : m_Row[ sFieldName ];

			if( o != null && o is DBNull == false )
			{
				return ( DateTime ) o;
			}

			return DateTime.MinValue;
		}
		
		protected Int32 GetInt( String sFieldName )
		{
			// Retrieve member.

			object o = m_Row == null ? null : m_Row[ sFieldName ];

			if( o != null && o is DBNull == false )
			{
				return ( Int32 ) o;
			}

			return -1;
		}

		public String Subject
        {
            // Access member.

            get
            {
                return GetStr( "DiscSubject" , typeof( String ) );
            }
        }

        public String Priority
        {
            // Access member.

            get
            {
                return GetStr( "DiscPriority" , typeof( E_DiscPriority ) );
            }
        }

        public String Status
        {
            // Access member.

            get
            {
                return GetStr( "DiscStatus" , typeof( E_DiscStatus ) );
            }
        }

        public String ActiveParticipants
        {
            // Access member.

            get
            {
                return GetStr( "DiscActiveParticipants" , typeof( String ) );
            }
        }

        public String PastParticipants
        {
            // Access member.

            get
            {
                return GetStr( "DiscPastParticipants" , typeof( String ) );
            }
        }

        public String DueDate
        {
            // Access member.

            get
            {
                return GetStr( "DiscDueDate" , typeof( DateTime ) );
            }
        }

        public String CreatedOn
        {
            // Access member.

            get
            {
                return GetStr( "DiscCreatedDate" , typeof( DateTime ) );
            }
        }

        public String CreatedBy
        {
            // Access member.

            get
            {
                return GetStr( "DiscCreatorLoginNm" , typeof( String ) );
            }
        }

        public String Log
        {
            // Access member.

            get
            {
                return GetStr( "DiscLog" , typeof( String ) );
            }
        }

        #endregion

		#region ( Constructors )

		/// <summary>
        /// Construct from db row.
        /// </summary>
        /// <param name="dRow">
        /// Db row to use.
        /// </param>

        public CDiscussion( DataRow dRow )
        : this()
        {
            // Initialize members.

            if( dRow != null && dRow[ "DiscLogId" ] != null )
            {
                // Reparse id for integrity.  If it blows up,
                // the row will remain initialized default.

                m_DiscId = new Guid( dRow[ "DiscLogId" ].ToString() );

                m_Row = dRow;
            }
        }

        /// <summary>
        /// Construct default.
        /// </summary>

        public CDiscussion()
        {
            // Initialize members.

            m_DiscId = Guid.Empty;
            m_Row    = null;
        }

		#endregion

    }

    /// <summary>
    /// Capture the discussion topics that are logically grouped
    /// together.  For now, we fetch all the discussion topics
    /// bound to a particular loan.
    /// </summary>

    public class CDiscussionSet : IEnumerable
    {
        /// <summary>
        /// Capture the discussion topics that are logically grouped
        /// together.  For now, we fetch all the discussion topics
        /// bound to a particular loan.
        /// </summary>

        protected ArrayList m_Set; // set of discussion db objects

		protected virtual void Fill( DataSet dS , DataSet pS , Guid loanId )
		{
			// Load up the discussion tasks for the given loan id.

			try
			{
				// Get the actual task objects for the given loan.
                Guid brokerId = Guid.Empty;

                DbConnectionInfo.GetConnectionInfoByLoanId(loanId, out brokerId);

                SqlParameter[] parameters = {
                                                new SqlParameter( "@LoanId"         , loanId )
					                            , new SqlParameter( "@IsValid"        , true   )
                                            };

				DataSetHelper.Fill( dS, brokerId, "ListDiscLogsByLoanId", parameters);

				// Get the participants by connecting to all the
				// related notification objects -- valid and not.

                parameters = new SqlParameter[] {
                                                    new SqlParameter( "@LoanId" , loanId )
                                                };
				DataSetHelper.Fill( pS, brokerId, "ListDiscParticipantsByLoanId", parameters);
			}
			catch( Exception )
			{
				// Oops!

				throw new CBaseException
					( ErrorMessages.FailedToLoad
					, "Failed to load discussion set."
					);
			}
		}

		protected virtual void Add( DataRow dRow )
		{
			// Append this row to our set as a discussion task.

			try
			{
				// Place the new instance on the end.

				m_Set.Add( new CDiscussion( dRow ) );
			}
			catch( Exception )
			{
				// Oops!

				throw new CBaseException
                    (ErrorMessages.FailedToLoad
                    , "Failed to load discussion set."
                    );
			}
		}

        public CDiscussion this[ int iDisc ]
        {
            // Access member.

            get
            {
                return m_Set[ iDisc ] as CDiscussion;
            }
        }

        public int Count
        {
            // Access member.

            get
            {
                return m_Set.Count;
            }
        }

        /// <summary>
        /// Load up the discussion logs associated with the given
        /// loan identifier.
        /// </summary>
        /// <param name="loanId">
        /// Loan identifier.
        /// </param>
        /// <returns>
        /// This set.
        /// </returns>

        public CDiscussionSet LoadByLoanId( Guid loanId )
        {
            // The resulting data set needs a column added that
            // contains the actual discussion history text.

            try
            {
                // Build up the data set using the loan id.  We
                // don't clear the existing set.  Each row is the
                // combination of the discussion log attributes,
                // the collected notification elements, and the
                // raw comment history.

                Hashtable al = new Hashtable();
                Hashtable pl = new Hashtable();
                DataSet   ds = new DataSet();
                DataSet   ps = new DataSet();

				Fill( ds , ps , loanId );

                ds.Tables[ 0 ].Columns.Add( "DiscActiveParticipants" );
                ds.Tables[ 0 ].Columns.Add( "DiscPastParticipants"   );
                ds.Tables[ 0 ].Columns.Add( "DiscLog"                );

				if( ps.Tables.Count > 0 )
				{
					foreach( DataRow row in ps.Tables[ 0 ].Rows )
					{
						// Collect the participating users and build
						// up a comma-delimited list for active and
						// past participants.

						Object id = row[ "DiscLogId" ];

						if( row[ "NotifIsValid" ] is Boolean == false )
						{
							continue;
						}

						// Add the user to the active or past list,
						// depending on whether the notification is
						// valid or old.

						Boolean isvalid = ( Boolean ) row[ "NotifIsValid" ];

						if( isvalid == true )
						{
							// Append to the active list.

							String list = al[ id ] as String;

							if( list != null )
							{
								list += ", " + row[ "NotifReceiverLoginNm" ].ToString();

								al[ id ] = list;
							}
							else
							{
								al.Add( id , row[ "NotifReceiverLoginNm" ].ToString() );
							}
						}
						else
						{
							// Append to the past list.

							String list = pl[ id ] as String;

							if( list != null )
							{
								list += ", " + row[ "NotifReceiverLoginNm" ].ToString();

								pl[ id ] = list;
							}
							else
							{
								pl.Add( id , row[ "NotifReceiverLoginNm" ].ToString() );
							}
						}
					}
				}

				if( ds.Tables.Count > 0 )
				{
					foreach( DataRow row in ds.Tables[ 0 ].Rows )
					{
						// Now initialize the current discussion row with
						// the list of participants.

						Object id = row[ "DiscLogId" ];

						if( al.Contains( id ) == true )
						{
							row[ "DiscActiveParticipants" ] = al[ id ];
						}
						else
						{
							row[ "DiscActiveParticipants" ] = "";
						}

						if( pl.Contains( id ) == true )
						{
							row[ "DiscPastParticipants" ] = pl[ id ];
						}
						else
						{
							row[ "DiscPastParticipants" ] = "";
						}
					}

					foreach( DataRow row in ds.Tables[ 0 ].Rows )
					{
						// Dig up each discussion topic's history
						// from the file archive repository.

						CFileDBFields ff = new CFileDBFields( new Guid( row[ "DiscLogId" ].ToString() ) );

						row[ "DiscLog" ] = ff.Load( "DiscussionLog" );
					}

					foreach( DataRow row in ds.Tables[ 0 ].Rows )
					{
						// Create a new discussion topic for our
						// set and for consumption.

						Add( row );
					}
				}
            }
            catch( Exception )
            {
				// Oops!

				throw new CBaseException
					( ErrorMessages.FailedToLoad
					, "Unable to load loan's discussions by id."
					);
            }

            return this;
        }

        /// <summary>
        /// These enumerators were made for walking, and
        /// that's just what they'll do...  One of these
        /// days these enumerators are gonna walk all...
        /// </summary>
        /// <returns>
        /// Enumeration interface.
        /// </returns>

        public IEnumerator GetEnumerator()
        {
            // Retrieve our enumeration interface.

            return m_Set.GetEnumerator();
        }

		#region ( Constructors )

        /// <summary>
        /// Constuct and initialize.
        /// </summary>
        /// <param name="uLoanId">
        /// Loan to gather from.
        /// </param>

        public CDiscussionSet( Guid uLoanId )
        : this()
        {
            // Initialize members.

            LoadByLoanId( uLoanId );
        }

        /// <summary>
        /// Construct default.
        /// </summary>

        public CDiscussionSet()
        {
            // Initialize members.

            m_Set = new ArrayList();
        }

		#endregion

    }

}