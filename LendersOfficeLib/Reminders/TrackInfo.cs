﻿using System;

namespace LendersOffice.Reminders
{
    /// <summary>
    /// Maintain individual tracking details.
    /// </summary>

    public class TrackInfo
    {

        public Guid UserId { get; set; }
        public Guid TrackId { get; set; }

        public Guid NotifId { get; set; }

        public bool IsValid { get; set; }

        #region ( Constructors )

        /// <summary>
        /// Construct info.
        /// </summary>
        /// <param name="userId">
        /// Tracked user's id.
        /// </param>
        /// <param name="trackId">
        /// Tracking id.
        /// </param>
        /// <param name="notifId">
        /// Notify id.
        /// </param>
        /// <param name="isValid">
        /// Status flag.
        /// </param>

        public TrackInfo(Guid userId, Guid trackId, Guid notifId, bool isValid)
        {
            // Initialize members.

            TrackId = trackId;
            NotifId = notifId;
            IsValid = isValid;
            UserId = userId;
        }

        #endregion

    }
}
