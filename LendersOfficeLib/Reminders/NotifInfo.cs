﻿using System;

namespace LendersOffice.Reminders
{
    /// <summary>
    /// Maintain individual notification details.
    /// </summary>

    public class NotifInfo
    {
        public Guid UserId { get; set; }
        public Guid NotifId { get; set; }
        public bool IsValid { get; set; }
        public bool Status { get; set; }

        #region ( Constructors )

        /// <summary>
        /// Construct info.
        /// </summary>
        /// <param name="userId">
        /// Receiver's id.
        /// </param>
        /// <param name="notifId">
        /// Notify id.
        /// </param>
        /// <param name="isValid">
        /// Validity flag.
        /// </param>

        public NotifInfo(Guid userId, Guid notifId, bool isValid)
        {
            // Initialize members.

            NotifId = notifId;
            IsValid = isValid;
            UserId = userId;

            Status = false;
        }

        #endregion

    }
}
