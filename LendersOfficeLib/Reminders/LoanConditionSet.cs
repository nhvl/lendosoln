﻿using System;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Constants;
using System.Diagnostics;
using LendersOffice.HttpModule;

namespace LendersOffice.Reminders
{
    /// <summary>
    /// Set of all conditions associated with a loan.  Note that if only
    /// reading conditions, CConditionSet class is a better choice.
    /// </summary>

    public class LoanConditionSetObsolete : ArrayList
    {
        /// <summary>
        /// Keep track of what we retrieved.
        /// </summary>

        private Guid m_LoanId = Guid.Empty;
        private bool m_allowSave = true; // To avoid dangerous programmer error.

        public ConditionDesc[] Digest
        {
            // Get a short list of condition tasks for binding.

            get
            {
                // Walk the list and make an array.

                Int32 i = 0; ConditionDesc[] cdList = new ConditionDesc[Count];

                foreach (CLoanConditionObsolete lC in this)
                {
                    ConditionDesc cD = new ConditionDesc();

                    cD.Category = lC.CondCategoryDesc;
                    cD.Description = lC.CondDesc;
                    cD.IsHidden = lC.CondIsHidden;
                    cD.Id = lC.CondId;
                    cD.CondDeleteByUserNm = lC.CondDeleteByUserNm;
                    cD.CondDeleteD = lC.CondDeleteD.HasValue? Tools.GetDateTimeDescription(lC.CondDeleteD.Value) : string.Empty;

                    if (lC.CondStatus == E_CondStatus.Done)
                    {
                        cD.DateDone = lC.CondStatusDate.ToShortDateString();

                        cD.IsDone = true;
                    }
                    else
                    {
                        cD.IsDone = false;
                    }

                    cdList[i] = cD;

                    ++i;
                }

                return cdList;
            }
        }

        /// <summary>
        /// Add a new condition to this set  and make sure
        /// the category and description are unique.
        /// </summary>
        public void AddUniqueCondition(String sCategory, String sDescription)
        {
            AddUniqueCondition(sCategory, sDescription, false /* IsHidden */);
        }

        /// <summary>
        /// Add a new hidden condition to this set  and make sure
        /// the category and description are unique.
        /// </summary>
        public void AddUniqueHiddenCondition(String sCategory, String sDescription)
        {
            AddUniqueCondition(sCategory, sDescription, true /* IsHidden */);
        }

        private void AddUniqueCondition(String sCategory, String sDescription, bool bIsHidden)
        {
            // Look for exact match and skip add if found.
            if (string.IsNullOrEmpty(sCategory) == false)
            {
                sCategory = sCategory.TrimWhitespaceAndBOM();
            }
            if (string.IsNullOrEmpty(sDescription) == false)
            {
                sDescription = sDescription.TrimWhitespaceAndBOM();
            }
            try
            {
                // Search and punt if match.
                // Note that we do not care if the match is deleted or not.

                foreach (CLoanConditionObsolete lC in this)
                {
                    if (lC.CondCategoryDesc.TrimWhitespaceAndBOM() == sCategory && lC.CondDesc.TrimWhitespaceAndBOM() == sDescription)
                    {
                        return;
                    }
                }

                // It's novel, so add a new condition.

                CLoanConditionObsolete newCondition = new CLoanConditionObsolete(this.BrokerId);

                newCondition.CondCategoryDesc = sCategory;
                newCondition.CondDesc = sDescription;
                newCondition.LoanId = m_LoanId;
                newCondition.CondIsHidden = bIsHidden;

                Add(newCondition);
            }
            catch (Exception e)
            {
                // Oops!

                Tools.LogError("Failed to add to loan's condition set.", e);

                throw;
            }
        }

        // 10/18/07 mf. OPM 7325. Return the matching condition if it exists, otherwise null.
        // 4/3/09 db Making the comparison case-insensitive for the DataTrac integration
        public CLoanConditionObsolete GetMatch(string Category, string Description)
        {
            foreach (CLoanConditionObsolete lC in this)
            {
                if (lC.CondCategoryDesc.TrimWhitespaceAndBOM().ToLower() == Category.TrimWhitespaceAndBOM().ToLower() && lC.CondDesc.TrimWhitespaceAndBOM().ToLower() == Description.TrimWhitespaceAndBOM().ToLower())
                {
                    return lC;
                }
            }

            return null;
        }

        public Guid BrokerId { get; private set; }

        // 10/18/07 mf. OPM 7325.
        /// <summary>
        /// Load from DB with special options
        /// </summary>
        /// <param name="loanId"></param>
        /// <param name="ShowHidden">Inclue hidden in results set.</param>
        /// <param name="LoadDeleted">Show deleted conditions</param>
        public void Retrieve(Guid loanId, bool LoadNotes, bool ShowHidden, bool Deleted)
        {
            // Interface booleans reduce readability of code significantly!
            // Consider enums for these or a single flag enum parameter.

            // Situations to handle: (hidden deleted)
            // Load in Editor with special permission - (1 0)
            // Load in Editor witout special permission, PDFs, etc. - (0 0)
            // Load deleted loans with special permission - (1 1)
            // Load deleted loans without special permission - (0 1)
            // Load everything for cert generation - should use RetrieveAll()
            Guid brokerId = Guid.Empty;
            DbConnectionInfo.GetConnectionInfoByLoanId(loanId, out brokerId);

            this.BrokerId = brokerId;

            m_LoanId = loanId;
            m_allowSave = LoadNotes; // We cannot allow a save if we skipped the notes.

            SqlParameter[] parameters = { new SqlParameter( "@LoanId", loanId )
                                            , new SqlParameter( "@Deleted", Deleted )
                                            , new SqlParameter("@Hidden", ShowHidden) 
											, new SqlParameter("@ShowAll", false )
										};


            Retrieve(loanId, LoadNotes, parameters);
        }

        // Load every condition for this loan (regardless if it is hidden or is deleted)
        public void RetrieveAll(Guid loanId, bool LoadNotes)
        {
            m_LoanId = loanId;
            
            Guid brokerId = Guid.Empty;
            DbConnectionInfo.GetConnectionInfoByLoanId(loanId, out brokerId);

            this.BrokerId = brokerId;


            m_allowSave = LoadNotes; // We cannot allow a save if we skipped the notes.

            SqlParameter[] parameters = { new SqlParameter( "@LoanId", loanId )
											, new SqlParameter("@ShowAll", true )
											 };


            Retrieve(loanId, LoadNotes, parameters);
        }

        private void Retrieve(Guid loanId, bool LoadNotes, SqlParameter[] loadParams)
        {

            m_allowSave = LoadNotes; // We cannot allow a save if we skipped the notes.
            // Fetch and add to existing set.

            try
            {
                // Initialize using a single reader.
                using (PerformanceMonitorItem.Time("LoanConditionSet.Retrieve()"))
                {


                    using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(this.BrokerId, "ListLoanConditionsByLoanId", loadParams))
                    {
                        while (reader.Read())
                        {
                            // Create empty instance and initialize it using the
                            // reader's current cursor.  All ids are copied over
                            // on load.

                            CLoanConditionObsolete lC = new CLoanConditionObsolete(this.BrokerId);
                            lC.UseNotes = LoadNotes;

                            lC.LoadConditionFields(reader);

                            Add(lC);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                // Oops!

                Tools.LogError("Failed to retrieve loan's condition set.", e);

                throw;
            }
        }

        /// <summary>
        /// Save all the contained conditions.  Any error halts the
        /// loop and punts.
        /// </summary>

        public void Save(CStoredProcedureExec spExec)
        {
            if (!m_allowSave)
                throw new CBaseException(ErrorMessages.Generic, "Cannot save this set without all fields loaded.");


            // Try and save all the tasks in their current state.

            try
            {
                // Run the rack and fire off saves.  First fail
                // calls it quits.

                foreach (CLoanConditionObsolete lC in this)
                {
                    lC.Save(spExec);
                }
            }
            catch (Exception e)
            {
                // Oops!

                Tools.LogErrorWithCriticalTracking("Failed to save loan's condition set.", e);

                throw;
            }
        }

        /// <summary>
        /// Save all the contained tasks.  Any error halts the
        /// loop and punts.
        /// </summary>

        public void Save(Guid brokerId)
        {
            if (!m_allowSave)
                throw new CBaseException(ErrorMessages.Generic, "Cannot save this set without all fields loaded.");

            // Wrap saving and throw if any error.  We rollback
            // all changes when we throw.
            this.BrokerId = brokerId;

            using (CStoredProcedureExec spExec = new CStoredProcedureExec(brokerId))
            {
                try
                {
                    // Rock and roll.

                    spExec.BeginTransactionForWrite();

                    Save(spExec);

                    spExec.CommitTransaction();
                }
                catch
                {
                    spExec.RollbackTransaction();

                    throw;
                }
            }
        }

    }
}
