using System;
using System.Collections;
using System.IO;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Xml;
using System.Xml.Serialization;
using System.Threading;
using System.Configuration;
using LendersOffice.Admin;
using LendersOffice.Events;
using LendersOffice.Security;
using LendersOffice.Common;
using LendersOffice.Constants;
using DataAccess;
using LendersOffice.ObjLib.DatabaseMessageQueue;
using LqbGrammar.Drivers.SecurityEventLogging;

namespace LendersOffice.Reminders
{
    /// <summary>
    /// // 5/1/2015 dd - Convert this class to continuous process. Only one server run this processor. The old code does not implement to be run on multiple servers.
    /// </summary>
    public class TaskAlertProcessor : CommonProjectLib.Runnable.IRunnable
    {
        /// <summary>
        /// If we need to extend the range of who gets an email when loan
        /// conditions change, just add a new role to this list.  If we
        /// decide that underwriters should be notified, then we can just
        /// append 'Underwriter' to this list and they'll be in the
        /// loop (literally).  If we need a dynamic mechanism, we can
        /// use a lightweight cache that stores and refreshes settings
        /// per broker.
        /// </summary>

        private static E_RoleT[] s_RolesToNotify =  {
                                                       E_RoleT.LenderAccountExecutive,
                                                       E_RoleT.LoanOfficer
                                                               };

        //private E_TaskAlertPumpState m_State = E_TaskAlertPumpState.Invalid;
        private AffectedTasks m_WorkingSet = new AffectedTasks();
        private UserEventCache m_Events = new UserEventCache();
        private TimeSpan m_Delay = ConstAppDavid.TaskAlertDelay;
        private TimeSpan m_Check = ConstAppDavid.TaskAlertCheck;

        /// <summary>
        /// Gets the description of the continuous process. Note: Run every hour.
        /// </summary>
        /// <value>The description of the continuous process.</value>
        public string Description
        {
            get { return "Task Alert Processor."; }
        }

        public void Run()
        {
            Tools.ResetLogCorrelationId();
            Tools.LogInfo("TaskAlertProcessor", "TaskAlertProcessor Run");

            // Sit in this loop and handle all task events that show up.
            try
            {
                // Initialize the pump by refreshing any leftovers from a
                // previously crashed attempt.

                // Reset every in use item to force a retry on these
                // guys.  If they're in the queue as in use, then we
                // left the loop with unfinished business.

                DBMessageQueue mQ = new DBMessageQueue(ConstMsg.TaskQueue);

                foreach (DBMessage mItem in mQ)
                {
                    if (mItem.Subject2 == "InUse")
                    {
                        // Pull out the task change alert and update any
                        // state to reset it for reconsideration.  We
                        // requeue it as an alert.
                        TaskChangeAlert tM = TaskChangeAlert.ToObject(mItem.BodyStream);
                        mQ.ReceiveById(mItem.Id);
                        mQ.Send(tM.LoanId.ToString(), tM.ToString(), "Alert");
                    }
                }

                // No start trucking with a known starting point.  We gather
                // and then send.  Many gathers take place, till the delay
                // has expired and it becomes time to send.

                // Gather and then send.  These 2 phases loop endlessly.

                // Gather all the current messages.  We populate an alert
                // table with each one.  The table stores affected tasks by
                // their loans.

                AffectedTasks affectedTasks = new AffectedTasks();

                try
                {
                    // Get all we can find in this grab.  We only consider fresh alerts.  Once we timeout, we'll bounce to either
                    // sleep or break out and start the send.
                    // Check if there's something to grab.  If not, we skip past checking the queue, and check if it's
                    // time to send out alerts.

                        mQ = new DBMessageQueue(ConstMsg.TaskQueue);

                        foreach (DBMessage mItem in mQ)
                        {
                            if (mItem.Subject2 == "Alert")
                            {
                                // Consider the packaged alert info and index it in our working set.
                                //
                                // 4/28/2005 kb - Per case #1654, we are now processing all task change events.  When conditions are marked
                                // done, we want to save and collect them for the loan officers who are watching the loan.  All other task
                                // change events are tracked right now.
                                TaskChangeAlert tM = TaskChangeAlert.ToObject(mItem.BodyStream);
                                if (tM.IsCondition == true && tM.IsNowDone == true)
                                {
                                    // Now, requeue the entry so we don't lose it in the event of failure before send.  If requeing
                                    // fails, we still want to try and add to our working set.
                                    try
                                    {
                                        mQ.ReceiveById(mItem.Id);
                                        mQ.Send(tM.LoanId.ToString(), tM.ToString(), "InUse");
                                    }
                                    catch
                                    {
                                    }
                                    finally
                                    {
                                        m_WorkingSet.Add(tM);
                                    }

                                    continue;
                                }
                                else if (tM.IsCondition == false)
                                {
                                    // Send the task state to all listening user's cache sets.  If not cached, then skip it -- when the
                                    // user pings for his/her set, we'll pull from the database directly.  We remove the message from the
                                    // queue so that we aren't tracking twice.
                                    try
                                    {
                                        mQ.ReceiveById(mItem.Id);
                                    }
                                    catch
                                    {
                                    }
                                    finally
                                    {
                                        m_Events.Track(tM);
                                    }
                                    continue;
                                }
                                else
                                {
                                    // 5/2/2005 kb - We don't track this entry, so dump it. If we didn't handle it above, then there's no place
                                    // for it.  We sometimes land here when conditions are updated, but not marked done.  Those should be kept
                                    // and processed too (soon).
                                    try
                                    {
                                        mQ.ReceiveById(mItem.Id);
                                    }
                                    catch
                                    {
                                    }
                                }

                            } // foreach( Message mItem in mQ )
                        }

                    
                }
                catch (DBMessageQueueException e)
                {
                    // Check if we just timed out.  If so, then try
                    // again after sleeping.  If not, then punt.

                    if (e.MessageQueueErrorCode != DBMessageQueueErrorCode.IOTimeout && DBMessageQueueErrorCode.EmptyQueue != e.MessageQueueErrorCode)
                    {
                        throw;
                    }
                }

                // Send out what we've gathered.  For each loan entry,
                // we build the corresponding email notification, send
                // it, and then remove the in-use alerts that pertain
                // to that loan.  If we get interrupted, the recovery
                // loop during init should pick up our left over in-use
                // message in the queue.

                SendOutTaskChangeAlerts();

                // Reset the next send time, so we can know when to
                // wake up and send again.




            }
            catch (ThreadAbortException)
            {
            }
            catch (Exception e)
            {
                // Expect aborts to stop the thread.  Other errors
                // need to be recorded.

                Tools.LogError("Task alert pump: Unexpected failure.", e);
            }

        }

        /// <summary>
        /// Take the current working set (whatever state it is in), lock
        /// down the pump, and process the events.  We sleep between
        /// loan processing, so we don't scorch the rest of the users by
        /// hogging the cpu.
        /// </summary>

        private void SendOutTaskChangeAlerts()
        {
            // Walk the current working set and don't let it go until we
            // have emptied it.  Note that the set cannot grow from the
            // outside while we've locked the pump.
            int notificationCount = 0;
            while (m_WorkingSet.IsEmpty == false)
            {
                // Get the current loan's assignment info and the employee's
                // notification settings.  We construct an email that lists
                // all the tasks for a particular loan that have changed.
                // Once we make it to the end, we send the email, and then
                // clean out the entries in the queue.  It's up to the email
                // to secure the letter because we won't be able to reproduce
                // it.  If the sending fails somewhere, we eat the current
                // loan so we don't get stuck trying on it over and over
                // again, but we leave the contributing alert messages in
                // our queue for some recovery mechanism to sweep up.

                try
                {
                    // Get loan assignments and permissions for each employee.
                    // We collect the loan condition records first.

                    LoanConditionsChanged lcChanged = new LoanConditionsChanged();
                    ArrayList changeRecords = new ArrayList();
                    LoanAccessInfo loanInfo = new LoanAccessInfo();

                    if (m_WorkingSet.Current == Guid.Empty)
                    {
                        // 5/16/2015 dd - Skip when loan id is Guid.Empty.
                        continue;
                    }

                    loanInfo.Retrieve(m_WorkingSet.Current);

                    foreach (TaskChangeAlert tM in m_WorkingSet.Set)
                    {
                        // Capture the change records for each modified condition.
                        // We are getting latest and greatest here, so no need to
                        // worry about listing a condition more than once -- we
                        // don't show a change history.

                        ConditionChangeRecord chRec = new ConditionChangeRecord();

                        if (tM.TaskId != Guid.Empty)
                        {
                            // Load the condition task from the database.  We will
                            // mark it as done if it is done in here.  For now, I
                            // think that the only condition tasks that make it
                            // into this list are the ones marked done on save.
                            // That should change soon.
                            //
                            // 5/3/2005 kb - No need to load the participants; we
                            // only want the subject and description.
                            //
                            // 11/6/2006 mf -  We are removing conditions from tasks.
                            // So we must use the new condition framework here.

                            CLoanConditionObsolete condition = new CLoanConditionObsolete(loanInfo.BrokerId, tM.TaskId);
                            condition.Retrieve();

                            chRec.Condition = condition.CondDesc;
                            chRec.Category = condition.CondCategoryDesc;

                            if (condition.CondStatus == E_CondStatus.Done)
                            {
                                chRec.DateDone = condition.CondStatusDate.ToShortDateString();
                                chRec.IsDone = true;
                            }
                            else
                            {
                                chRec.IsDone = false;
                            }

                            chRec.AlertDate = tM.EventD.ToString();
                            chRec.Who = tM.ByWhom;
                        }

                        changeRecords.Add(chRec);
                    }

                    // Now send the message to the employees.  We expect the message
                    // to be sent (at least, not lost), so no need to worry about
                    // retrying after successful send.  If we fail and throw, then
                    // the alert records will still be around waiting for us
                    // on reset.

                    foreach (E_RoleT sRole in s_RolesToNotify)
                    {
                        if (loanInfo.Assignments.HasAssignment(sRole) == true)
                        {
                            lcChanged.Initialize
                                (changeRecords
                                , loanInfo.Assignments[sRole].EmployeeId
                                , loanInfo.LoanId
                                , E_ApplicationT.LendersOffice
                                );

                            lcChanged.Send();
                            notificationCount++;
                        }
                    }

                    // Clean up the queue of all in use notifications for
                    // the current loan.

                    DBMessageQueue mQ = new DBMessageQueue(ConstMsg.TaskQueue);

                    foreach (DBMessage mItem in mQ)
                    {
                        if (mItem.Subject2 == "InUse")
                        {
                            TaskChangeAlert aM = TaskChangeAlert.ToObject(mItem.BodyStream);
                            if (aM.LoanId == m_WorkingSet.Current)
                            {
                                try
                                {
                                    mQ.ReceiveById(mItem.Id);
                                }
                                catch
                                {
                                    continue;
                                }
                            }

                            continue;
                        }

                    }
                }
                catch (Exception e)
                {
                    // Oops!

                    Tools.LogError("Failed to send out task change alert for loan " + m_WorkingSet.Current + ".", e);
                }
                finally
                {
                    // Drop the current entry and move on.

                    m_WorkingSet.Remove(m_WorkingSet.Current);
                }

                // Sleep for a spell to ease up cpu hits.

                LendersOffice.Sleeping.PerformanceLoggingSleeper.Instance.Sleep(ConstApp.MessageQueueTimeout);
            }
            Tools.LogInfo("TaskAlertProcessor", "Send " + notificationCount + " notifications.");
        }



        /// <summary>
        /// Save a task-changed alert in the global message queue.
        /// </summary>

        public static void Alert(Guid taskId, String byWhom, E_DiscStatus eStatus, DateTime createdD, DateTime alertD, DateTime dueD, DateTime modifiedD, DateTime ackD, Boolean isNowDone, Boolean isCondition, Boolean isRead, ArrayList aParticipants, Guid loanId, Guid userId, DateTime eventD)
        {
            // Construct a container for this alert and stream it into
            // a message queue message.
            try
            {
                DBMessageQueue mQ = new DBMessageQueue(ConstMsg.TaskQueue);
                TaskChangeAlert aM = new TaskChangeAlert();

                aM.ByWhom = byWhom;
                aM.Status = eStatus;
                aM.CreatedD = createdD;
                aM.AlertD = alertD;
                aM.DueD = dueD;
                aM.ModifiedD = modifiedD;
                aM.AckD = ackD;
                aM.IsNowDone = isNowDone;
                aM.IsCondition = isCondition;
                aM.IsRead = isRead;
                aM.Participants = aParticipants;
                aM.LoanId = loanId;
                aM.UserId = userId;
                aM.EventD = eventD;
                aM.TaskId = taskId;

                mQ.Send("Task Stuff", aM.ToString(), "Alert");
            }

            catch (Exception e)
            {
                Tools.LogError("Task alert processor: Failed to save alert message.", e);
                throw;
            }
        }

    }

    /// <summary>
    /// Whenever an exception occurs during processing of
    /// discussion events, we can throw this base.
    /// </summary>

    public class TaskProcessorException : CBaseException
    {
        #region ( Constructors )

        /// <summary>
        /// Construct default.
        /// </summary>

        public TaskProcessorException(string message, Exception inner)
            : base(ErrorMessages.Generic, message + ".\r\n\r\n" + inner.ToString())
        {
        }

        /// <summary>
        /// Construct default.
        /// </summary>

        public TaskProcessorException(string message)
            : base(ErrorMessages.Generic, message)
        {
        }

        #endregion

    }

    /// <summary>
    /// Store task id lists organized by loan id.  We let
    /// clients add all they want, but we maintain unique
    /// lists.
    /// </summary>

    public class AffectedTasks
    {
        /// <summary>
        /// Store task id lists organized by loan id.
        /// </summary>

        private Hashtable m_Table = new Hashtable();
        private ArrayList m_Order = new ArrayList();

        public Guid Current
        {
            get
            {
                return (Guid)m_Order[0];
            }
        }

        public ICollection Set
        {
            get
            {
                return m_Table[(Guid)m_Order[0]] as ICollection;
            }
        }

        public bool IsEmpty
        {
            get { return m_Order.Count == 0; }
        }

        /// <summary>
        /// Insert loan and task change alert into our lookup
        /// tables (ordered by loan insertion order).
        /// </summary>

        public void Add(TaskChangeAlert tcAlert)
        {
            // Lookup loan entry and add the task to the set.
            // A contained loan will always have at least one
            // task alert.

            IList taskSet = m_Table[tcAlert.LoanId] as IList;

            if (taskSet == null)
            {
                m_Table.Add(tcAlert.LoanId, taskSet = new ArrayList());

                m_Order.Add(tcAlert.LoanId);
            }

            bool foundIt = false;

            foreach (TaskChangeAlert aItem in taskSet)
            {
                if (aItem.TaskId == tcAlert.TaskId)
                {
                    // 3/7/2005 kb - We will either replace it, or do
                    // nothing, but we don't want to add it twice (so,
                    // either way, we've found it).

                    if (aItem.EventD < tcAlert.EventD)
                    {
                        taskSet.Remove(aItem);

                        taskSet.Add(tcAlert);
                    }

                    foundIt = true;

                    break;
                }
            }

            if (foundIt == false)
            {
                taskSet.Add(tcAlert);
            }
        }

        /// <summary>
        /// Clean out entry from our table.  We remove from
        /// the order first, incase table removal fails.
        /// </summary>

        public void Remove(Guid loanId)
        {
            // Clean out entry from our table.  We remove from
            // the order first, incase table removal fails.

            m_Order.Remove(loanId);
            m_Table.Remove(loanId);
        }

    }

    /// <summary>
    /// Current options we track in latest object state.
    /// Users ping the cached state of all dashboards to
    /// determine if a client display should light up.
    /// </summary>

    public class UserTaskMonitor
    {
        /// <summary>
        /// Current options we track in latest object state.
        /// </summary>

        private int m_HasNewTask = 0;
        private int m_TimedAlert = 0;
        private int m_TaskNowDue = 0;
        protected Guid m_UserId = Guid.Empty;

        #region ( Dashboard options )

        public int HasNewTask
        {
            set { m_HasNewTask = value; }
            get { return m_HasNewTask; }
        }

        public int TimedAlert
        {
            set { m_TimedAlert = value; }
            get { return m_TimedAlert; }
        }

        public int TaskNowDue
        {
            set { m_TaskNowDue = value; }
            get { return m_TaskNowDue; }
        }

        public Guid UserId
        {
            set { m_UserId = value; }
            get { return m_UserId; }
        }

        #endregion

    }

    /// <summary>
    /// Keep track of users' tasks in our cache of events.  We calc
    /// users' dashboards by counting up all the applicable events.
    /// </summary>

    public class TaskDescriptor
    {
        /// <summary>
        /// Keep track of users' tasks in our cache of events.  We
        /// calc users' dashboards by counting up all the applicable
        /// events.
        /// </summary>

        private DateTime m_CreatedD = DateTime.MaxValue;
        private DateTime m_ModifiedD = DateTime.MaxValue;
        private DateTime m_DueD = DateTime.MaxValue;
        private DateTime m_AlertD = DateTime.MaxValue;
        private DateTime m_LastReadD = DateTime.MinValue;
        private DateTime m_AckD = DateTime.MinValue;
        private Boolean m_IsRead = true;
        private Guid m_TaskId = Guid.Empty;

        #region ( Event properties )

        public IDataReader Contents
        {
            // Access member.

            set
            {
                Object o;

                o = value["DiscCreatedDate"];

                if (o != null && o is DBNull == false)
                {
                    m_CreatedD = (DateTime)o;
                }
                else
                {
                    m_CreatedD = DateTime.MinValue;
                }

                o = value["DiscLastModifyD"];

                if (o != null && o is DBNull == false)
                {
                    m_ModifiedD = (DateTime)o;
                }
                else
                {
                    m_ModifiedD = DateTime.MinValue;
                }

                o = value["DiscDueDate"];

                if (o != null && o is DBNull == false)
                {
                    m_DueD = (DateTime)o;
                }
                else
                {
                    m_DueD = DateTime.MaxValue;
                }

                o = value["NotifAlertDate"];

                if (o != null && o is DBNull == false)
                {
                    m_AlertD = (DateTime)o;
                }
                else
                {
                    m_AlertD = DateTime.MaxValue;
                }

                o = value["NotifLastReadD"];

                if (o != null && o is DBNull == false)
                {
                    m_LastReadD = (DateTime)o;
                }
                else
                {
                    m_LastReadD = DateTime.MinValue;
                }

                o = value["NotifAckD"];

                if (o != null && o is DBNull == false)
                {
                    m_AckD = (DateTime)o;
                }
                else
                {
                    m_AckD = DateTime.MinValue;
                }

                o = value["NotifIsRead"];

                if (o != null && o is DBNull == false)
                {
                    m_IsRead = (Boolean)o;
                }
                else
                {
                    m_IsRead = true;
                }

                o = value["DiscLogId"];

                if (o != null && o is DBNull == false)
                {
                    m_TaskId = (Guid)o;
                }
                else
                {
                    m_TaskId = Guid.Empty;
                }
            }
        }

        public DateTime CreatedD
        {
            set { m_CreatedD = value; }
            get { return m_CreatedD; }
        }

        public DateTime ModifiedD
        {
            set { m_ModifiedD = value; }
            get { return m_ModifiedD; }
        }

        public DateTime DueD
        {
            set { m_DueD = value; }
            get { return m_DueD; }
        }

        public DateTime AlertD
        {
            set { m_AlertD = value; }
            get { return m_AlertD; }
        }

        public DateTime LastReadD
        {
            set { m_LastReadD = value; }
            get { return m_LastReadD; }
        }

        public DateTime AckD
        {
            set { m_AckD = value; }
            get { return m_AckD; }
        }

        public bool IsRead
        {
            set { m_IsRead = value; }
            get { return m_IsRead; }
        }

        public Guid TaskId
        {
            set { m_TaskId = value; }
            get { return m_TaskId; }
        }

        #endregion

        #region ( Constructors )

        /// <summary>
        /// Construct default.
        /// </summary>

        public TaskDescriptor(TaskDescriptor dThat)
        {
            // Initialize members.

            m_CreatedD = dThat.m_CreatedD;
            m_ModifiedD = dThat.m_ModifiedD;
            m_DueD = dThat.m_DueD;
            m_AlertD = dThat.m_AlertD;
            m_LastReadD = dThat.m_LastReadD;
            m_AckD = dThat.m_AckD;
            m_IsRead = dThat.m_IsRead;
            m_TaskId = dThat.m_TaskId;
        }

        /// <summary>
        /// Construct default.
        /// </summary>

        public TaskDescriptor()
        {
        }

        #endregion

    }

    /// <summary>
    /// Keep active set of task alert events for a particular
    /// user.
    /// </summary>

    public class TaskAlertEventSet
    {
        /// <summary>
        /// Keep active set of task alert events for a
        /// particular user.
        /// </summary>

        private ArrayList m_Set = new ArrayList();
        private DateTime m_LastPull = SmallDateTime.MinValue;
        private Guid m_UserId = Guid.Empty;

        public TaskDescriptor this[Guid taskId]
        {
            // Access member.

            get
            {
                foreach (TaskDescriptor tD in m_Set)
                {
                    if (tD.TaskId == taskId)
                    {
                        return tD;
                    }
                }

                return null;
            }
        }

        public int Count
        {
            get { return m_Set.Count; }
        }

        public DateTime LastPull
        {
            set { m_LastPull = value; }
            get { return m_LastPull; }
        }

        public Guid UserId
        {
            set { m_UserId = value; }
            get { return m_UserId; }
        }

        /// <summary>
        /// Append a new descriptor to this set.  If colliding, we
        /// overwrite with the assumption that the new one came
        /// later in time than the current (make sure this is so).
        /// </summary>

        public void Add(TaskDescriptor aTask)
        {
            // Go through and find a match.  If not found, we
            // append.

            int i = 0;

            foreach (TaskDescriptor tD in m_Set)
            {
                if (tD.TaskId == aTask.TaskId)
                {
                    m_Set[i] = aTask;

                    return;
                }

                ++i;
            }

            m_Set.Add(aTask);
        }

        /// <summary>
        /// Nix the task descriptor with the matching id if it
        /// exists within the current set.  If not present, then
        /// no changes are made.
        /// </summary>

        public void Drop(Guid taskId)
        {
            // Search the list and remove the match by position.

            int i = 0;

            foreach (TaskDescriptor tD in m_Set)
            {
                if (tD.TaskId == taskId)
                {
                    m_Set.RemoveAt(i);

                    break;
                }

                ++i;
            }
        }

        /// <summary>
        /// Get event set's interface for walking within a loop.
        /// </summary>

        public IEnumerator GetEnumerator()
        {
            // Return walking interface for looping.

            return m_Set.GetEnumerator();
        }

    }

    /// <summary>
    /// We track users' events for dashboard calculation.  If a
    /// user is not in the set, we allocate a slot and query the
    /// database for latest information.
    /// </summary>

    public class UserEventCache
    {
        /// <summary>
        /// Keep track of users in a fifo queue where oldest user
        /// records are evicted first when we need to make room
        /// for new entries.
        /// </summary>

        private Hashtable m_Table = new Hashtable();
        private ArrayList m_Users = new ArrayList();
        private int m_MaxEntries = 256;

        public TaskAlertEventSet this[Guid userId]
        {
            // Access member.

            get
            {
                TaskAlertEventSet tSet = m_Table[userId] as TaskAlertEventSet;

                if (tSet == null)
                {
                    // Not currently cached.  Load the user's tasks from the
                    // database and track the essentials.  If we exceed the
                    // current max entry limit, then we evict to make room.
                    // If the cache is off (limit == 0), then we return the
                    // calculated set without saving it.
                    //
                    // 4/27/2005 kb - This lookup could fail, and we wouldn't
                    // really care.  If it times out, then just return an empty
                    // event set (but, don't store it our set as empty).

                    tSet = new TaskAlertEventSet();

                    try
                    {
                        Guid brokerId = Guid.Empty;
                        DbConnectionInfo.GetConnectionInfoByUserId(userId, out brokerId);
                        SqlParameter[] parameters = {
                                                        new SqlParameter( "@UserId" , userId ) 
                                                    };
                        using (IDataReader sR = StoredProcedureHelper.ExecuteReader(brokerId, "ListTaskEventsByUserId", parameters))
                        {
                            while (sR.Read() == true)
                            {
                                TaskDescriptor aTask = new TaskDescriptor();

                                aTask.Contents = sR;

                                tSet.Add(aTask);
                            }
                        }
                        tSet.LastPull = DateTime.Now;
                    }
                    catch (Exception e)
                    {
                        // Oops!

                        Tools.LogError("Failed to load task descriptors for alert set for " + userId + ".", e);

                        return tSet;
                    }

                    while (m_Users.Count > m_MaxEntries && m_Users.Count > 0)
                    {
                        m_Table.Remove(m_Users[0]);

                        m_Users.RemoveAt(0);
                    }

                    if (m_Users.Count < m_MaxEntries)
                    {
                        m_Table.Add(userId, tSet);
                    }
                    else
                    {
                        return tSet;
                    }
                }

                m_Users.Remove(userId);

                m_Users.Add(userId);

                return tSet;
            }
        }

        /// <summary>
        /// Look up the user's set and drop it.  If not found, then no
        /// worries... skip it.
        /// </summary>

        public void Quit(Guid userId, Guid taskId)
        {
            // Look up the user's set and drop it.  If not found,
            // then no worries... skip it.

            TaskAlertEventSet tSet = m_Table[userId] as TaskAlertEventSet;

            if (tSet != null)
            {
                tSet.Drop(taskId);
            }
        }

        /// <summary>
        /// Remember task change description and distribute it to
        /// all the affected participants.
        /// </summary>

        public void Track(TaskChangeAlert tAlert)
        {
            // Walk the participant list and populate all existing user's
            // task set.  For the user who is changing the task, we need
            // to update all the stats.  For all other participants, we
            // only update the created date, last modified date, and due
            // date for the task.

            TaskAlertEventSet tSet = m_Table[tAlert.UserId] as TaskAlertEventSet;

            if (tSet != null)
            {
                TaskDescriptor tD = tSet[tAlert.TaskId];

                if (tD != null)
                {
                    // The alerting user has the task, so update the user-specific
                    // attributes that only affect this user's dashboard.

                    tD.AlertD = tAlert.AlertD;
                    tD.LastReadD = tAlert.EventD;
                    tD.AckD = tAlert.AckD;
                    tD.IsRead = tAlert.IsRead;
                }
            }

            foreach (TaskParticipant tP in tAlert.Participants)
            {
                tSet = m_Table[tP.UserId] as TaskAlertEventSet;

                if (tSet == null)
                {
                    continue;
                }

                if (tP.IsActive == true)
                {
                    // Update the active participant's parameters for this
                    // task.  We overwrite the aspects that a listening
                    // user would want to know changed (i.e., things that
                    // affect them).

                    TaskDescriptor tD = tSet[tAlert.TaskId];

                    if (tD == null)
                    {
                        // Append this task to the listening user.  We add
                        // the task descriptor as if this task were new, as
                        // if the participant had never seen it before.

                        tD = new TaskDescriptor();

                        tD.CreatedD = tAlert.CreatedD;
                        tD.AlertD = tAlert.AlertD;
                        tD.DueD = tAlert.DueD;
                        tD.ModifiedD = tAlert.ModifiedD;

                        if (tP.UserId == tAlert.UserId)
                        {
                            tD.LastReadD = tAlert.EventD;
                            tD.AlertD = tAlert.AlertD;
                            tD.AckD = tAlert.AckD;
                            tD.IsRead = tAlert.IsRead;
                        }
                        else
                        {
                            tD.IsRead = false;
                        }

                        tD.TaskId = tAlert.TaskId;

                        tSet.Add(tD);
                    }
                    else
                    {
                        tD.ModifiedD = tAlert.ModifiedD;
                        tD.CreatedD = tAlert.CreatedD;
                        tD.DueD = tAlert.DueD;
                    }
                }
                else
                {
                    // This user is no longer an active participant.  Remove
                    // the task from their set.  This is probably redundant,
                    // isn't harmful.

                    tSet.Drop(tAlert.TaskId);
                }
            }
        }

        /// <summary>
        /// Remove all cached entries; forcing all subsequent
        /// queries to refresh from the database.
        /// </summary>

        public void Clear()
        {
            // Nix all existing user entries.

            m_Users.Clear();
            m_Table.Clear();
        }

    }

    /// <summary>
    /// We keep track of the current phase of the pump so it's easy
    /// to tell what the thread is doing.
    /// </summary>

    public enum E_TaskAlertPumpState
    {
        Invalid = 0,
        Initializing = 1,
        Gathering = 2,
        Sending = 3,
        Error = 4,
    }

    /// <summary>
    /// Maintain loop thread for processing all task change alerts in
    /// the pump's queue.  We process in 2 stages to try to keep from
    /// locking up the system with our hourly bursts.
    /// </summary>
    /// <remarks>
    /// There are many ways to provide caching of task events so that
    /// everyone's dashboard can be kept responsive and accurate, and
    /// that affected users are notified of changes in a timely manner.
    /// We chose to use a background threaded pump, which extracts the
    /// task events stored in a global message queue, to provide accuracy
    /// without bogging down user's post back processing.
    /// 
    /// There are 3 main classes to discuss (one is actually a portal),
    /// the task alert pump, the task alert processor, and the admin
    /// console (a portal for manipulating the task alert configuration
    /// and state).
    /// 
    /// The task alert processor is the main contract for all application
    /// interaction with the task alert framework, which mainly consists
    /// of alerting the framework of task/condition changes, and querying
    /// the cached changes to see real time status or generate users'
    /// notifications.  Consider this class a proxy to the pump.
    /// 
    /// The task alert pump is a the background thread that does all
    /// the gathering of change events and organizes them in to tables
    /// for quick analysis.  The pump runs for the lifetime of the
    /// application and is not directly accessed (the singleton instance
    /// is exposed through the processor's methods as an embedded,
    /// static, private member).
    /// 
    /// The admin console provides realtime status of the pump, and
    /// the ability to manipulate its configuration.  Many times, we
    /// just need to visit this admin panel to confirm that the pump
    /// is running in the background and to check for particular
    /// events being recorded during debugging.
    /// 
    /// The best place to start is to discuss what task alert events
    /// are and how they're tracked.
    /// 
    /// I) Task alert events
    /// 
    /// We associate tasks with loans and participating users in our
    /// application.  When a task has changed state, we notify the
    /// participating users in one of 2 ways, email or via the dashboard.
    /// This approach generally holds.  In practice, we actually do
    /// much less.
    /// 
    /// Task is updated   - Need to remember that timestamp so users
    ///                     who are associated with the task can be
    ///                     told with flashing lights in their
    ///                     dashboard.
    /// 
    /// Task is now due   - The task has a deadline date and time set,
    ///	                    which has just expired.  Participants'
    ///	                    dashboards light up in this case.
    /// 
    /// Task is alerted   - The task has an alert date and time set
    ///                     (think of it like an alarm to remind the
    ///                     participants that a deadline is looming),
    ///	                    which has just expired.  Participants'
    ///	                    dashboards light up in this case.
    /// 
    /// Condition is done - Conditions are tasks (but that may change
    ///                     soon), but are only tracked when marked
    ///                     as done in the loan editor.  We notify
    ///                     the associated users (currently the loan's
    ///                     assigned lender account exec and officer)
    ///                     with an email telling them of the event.
    /// 
    /// Whenever a task is saved using the task editor, one of the
    /// last things that happens is that the editor signals the pump
    /// (by calling alert() on a processor proxy) and the details are
    /// placed in the message queue.  Conditions are handled similarly
    /// when marked done.
    /// 
    /// In general, all events are gathered by the pump in the looping
    /// thread and cached for later scrutiny.  Whenever a user wants
    /// the latest state for their dashboard, then a request is made
    /// through the processor proxy to gather and calculate all
    /// relevant task details.  The returned result includes counts
    /// for all the tasks that were recently updated, now due, or in
    /// an alarm/alert state.
    /// 
    /// II) Pump mechanics
    /// 
    /// Events are logged through the processor proxy and stored in
    /// a global message queue that the pump polls.  Note that only
    /// asynchronous, dynamically generated events follow this route.
    /// Timeout events like "now due" and "alerted" are detected
    /// using an on-demand calculation (it's not expensive at all)
    /// at the time of dashboard request (see the user event cache
    /// topic for more info on this).
    /// 
    /// Task change events are cached in 2 places: The message queue
    /// contains them on first reception, then they are pulled out
    /// and indexed into a working-set table for deferred processing.
    /// We keep the original copies in the queue until we can confirm
    /// they have been handled.
    /// 
    /// The pump performs the following circuit:
    /// 
    /// Init - Get the currently queued up messages and reset state
    ///        to retry previous incomplete processing.
    /// 
    /// Loop - Get current messages from the message queue.  Handle
    ///        condition events (marked done).  Track task events
    ///        (task updated).  Continue until next-send time has
    ///        passed.  We keep gathering in this loop until the
    ///        current gathering period has elapsed.
    /// 
    /// Send - Once gathering is done, send out notifications to
    ///        all users whose conditions have been marked as done.
    ///        For each notification we send out, we yank the
    ///        queued messages that contributed to the notification
    ///        and remove them from the queue so we don't double-
    ///        send emails.
    /// 
    /// After we send out any condition marked-done notifications,
    /// we return to the main gathering loop for another period.
    /// In this way, we defer sending out notifications until we
    /// can gather enough changes to batch up the emails all at
    /// once.  Why?  Conditions tend to change in packs.  A user
    /// will mark 3-4 conditions as done in one edit.  If we
    /// responded to each event and sent a single email for each,
    /// then we would be doing a lot of redundant work and
    /// bombarding the receivers with emails that all tell a
    /// partial story of what's happening to their loan.  It's
    /// better to show all the conditions that have been recently
    /// marked as done in a single email so the user can see the
    /// changes at a glance (one email for each loan).
    /// 
    /// Also, it should be noted that the dashboard is hidden when
    /// the pump is off.  If you expect to see the dashboard for
    /// a particular user and it is hidden, then most likely, the
    /// task alert pump has been turned off or crashed.  Restart
    /// it by logging into the task admin panel and click "start".
    /// 
    /// III) Task event notifications
    /// 
    /// As mentioned, we only send out notifications for condition
    /// events when they're marked done.  By deferring the
    /// processing of these events in our working-set, we can
    /// batch together events that occur on a single loan into
    /// a single notification.  We then send out each notification
    /// to the loan's assigned officer and lender account exec.
    /// 
    /// We actually maintain a list of roles that are the intended
    /// recipients, so extending this list with additional roles
    /// is not difficult, but there are already plans in effect
    /// to create a notification subscription framework that
    /// would allow each employee to specify what events they want
    /// to be notified on and how they want to be notified.  With
    /// this in mind, sending notifications via this background
    /// loop may get obsoleted by a notification listener that
    /// participates in some future event notification framework.
    /// Keep in mind that this is all speculation because I was
    /// responsible for implementing these enhancements and I'm
    /// leaving.
    /// 
    /// IV) Caching task status
    /// 
    /// The pump maintains a cached table for all users who have
    /// recently requested task status to derive the light-up
    /// state for thier client-side dashboard.  The dashboard is
    /// the only customer (at this time) of this information.
    /// 
    /// The user-event-cache stores the state of all tasks by
    /// user.  Because many users can belong to a task, we may
    /// find the same task description in multiple users' lists.
    /// That's okay, because all users see tasks equally.  Mainly,
    /// timestamps are checked to compare each task's last-saved
    /// time with each user's last-checked time for that task.
    /// Any user who has a task that was saved after they last
    /// checked it has a task that has been updated.  Updated
    /// tasks are reported in the dashboard lights with the count
    /// of how many exist with respect to the querying user.  The
    /// same type of analysis is performed for now-due tasks and
    /// alerted tasks.
    /// 
    /// I should note that the user-event-cache only stores task
    /// descriptors -- not the actual tasks.  We don't load all
    /// the details, but only the dates and bits needed to
    /// determine the dashboard status for each user.
    /// 
    /// Also, the user-event-cache only stores the task descriptors
    /// for users who have actively requested these results.  The
    /// cache is on-demand (oh no! jargon alert!), in that content
    /// is pulled from the database only when needed.  Note that
    /// the cache has a max user count, but that doesn't necessarily
    /// limit the memory consumption (2 users could have 1000 updated
    /// tasks each).  Entries are evicted when the limit is reached.
    /// 
    /// V) Dashboard mechanics
    /// 
    /// The dashboard is a client-side interface to each user's task
    /// status that uses background callbacks to query the task
    /// alert pump's cache.  The callbacks occur regularly, every
    /// 10 seconds, in the background of the pipeline page's window.
    /// When mouse events are recorded in the pipeline, we query
    /// the task status every 2 seconds.  After 30 minutes from the
    /// time of the last refresh of the pipeline, we stop retrieving 
    /// the task status for the dashboard in the background.  Mouse
    /// events will continue to generate query requests every 2
    /// seconds.
    /// 
    /// When you click a flashing dashboard light, you are redirected
    /// to the task listing for the current user.  The grid is sorted
    /// according to the light that was clicked (shows the tasks that
    /// contributed to the particular flashing on top of the listing).
    /// 
    /// The "acknowledge" button generates a request to mark all
    /// outstanding task events as acknowledged.  That is, the event
    /// no longer contributes to dashboard flashing.
    /// 
    /// VI) Debugging
    /// 
    /// We currently only care about 2 kinds of events, so will
    /// only discuss debugging these 2 activities.
    /// 
    /// For conditions that are marked done, the easiest way to
    /// debug is to wait for the email to come to the officer or
    /// lender account exec (make sure they are set to receive
    /// loan event notifications).  Keep in mind that you'll be
    /// waiting until the next send-out time, which could be an
    /// hour later.  So, let's take a more immediate look:  After
    /// setting a loan's conditions to "done", log out and enter
    /// the task admin panel.  You should see the condition events
    /// listed in the grid and marked as "inuse".  If they're
    /// marked as "alert", then they have yet to be gathered by
    /// the pump, or the pump is off.  If off, then start it and
    /// refresh the panel.  If on, then keep refreshing the panel
    /// until the "alert" entries become "inuse" entries.  Once
    /// they are all "inuse", click "send now" to force the pump
    /// to leave gathering and send out notifications for all
    /// currently known events.  The grid should be cleaned out
    /// and no errors recorded on the page.  
    /// 
    /// For task events, the admin portal is not much use.  Make
    /// sure the task alert pump is running by checking it's
    /// status in the task admin panel.  A quick way to check is
    /// to become a user and look for their dashboard.  If the
    /// pump is stopped, then the dashboard will be hidden from
    /// view.  If visible, then update tasks that have multiple
    /// active participants.  Once saved, log out and log in as
    /// one of the other participants.  The updated dashboard
    /// light should be flashing.  Click it and you will be
    /// whisked off to the task list, sorted to show the updated
    /// tasks on top.  Edit an updated task and save it by clicking
    /// "ok".  The dashboard should update in 5 seconds and show
    /// an updated count decreased by one (or lights out if count
    /// is now zero).  Additionally, test the timeouts by setting
    /// a due date or warning date in the past for a particular
    /// task and save it.  The dashboard should respond accordingly
    /// after 5 seconds.
    /// 
    /// Clicking acknowledge in the dashboard will send the request
    /// to mark all outstanding tasks as acknowledged.
    /// </remarks>

    /// <summary>
    /// Keep track of who is associated with this task.
    /// </summary>

    [XmlRoot]
    public class TaskParticipant
    {
        /// <summary>
        /// Keep track of who is associated with this task.
        /// </summary>

        private Guid m_UserId = Guid.Empty;
        private Boolean m_IsActive = false;

        #region ( Participant properties )

        [XmlAttribute]
        public Boolean IsActive
        {
            set { m_IsActive = value; }
            get { return m_IsActive; }
        }

        [XmlElement]
        public Guid UserId
        {
            set { m_UserId = value; }
            get { return m_UserId; }
        }

        #endregion

    }

    /// <summary>
    /// Keep track of task changes by queueing up the loan and task
    /// identifiers.  The user's task cache takes these alerts as
    /// input when tracking.  See the track function to see how we
    /// incorporate these events into each user's set.
    /// </summary>

    [XmlRoot]
    public class TaskChangeAlert
    {
        /// <summary>
        /// Keep track of task changes by queueing up the loan and
        /// task identifiers.
        /// </summary>

        private String m_ByWhom = "";
        private E_DiscStatus m_Status = E_DiscStatus.Cancelled;
        private DateTime m_CreatedD = DateTime.MinValue;
        private DateTime m_AlertD = DateTime.MinValue;
        private DateTime m_DueD = DateTime.MinValue;
        private DateTime m_ModifiedD = DateTime.MinValue;
        private DateTime m_AckD = DateTime.MinValue;
        private Boolean m_IsNowDone = false;
        private Boolean m_IsCondition = false;
        private Boolean m_IsRead = true;
        private ArrayList m_Participants = null;
        private Guid m_LoanId = Guid.Empty;
        private Guid m_UserId = Guid.Empty;
        private Guid m_TaskId = Guid.Empty;
        private DateTime m_EventD = DateTime.Now;

        #region ( Task alert properties )
        [XmlElement]
        public String ByWhom
        {
            set { m_ByWhom = value; }
            get { return m_ByWhom; }
        }

        [XmlElement]
        public E_DiscStatus Status
        {
            set { m_Status = value; }
            get { return m_Status; }
        }

        [XmlElement]
        public DateTime CreatedD
        {
            set { m_CreatedD = value; }
            get { return m_CreatedD; }
        }

        [XmlElement]
        public DateTime AlertD
        {
            set { m_AlertD = value; }
            get { return m_AlertD; }
        }

        [XmlElement]
        public DateTime DueD
        {
            set { m_DueD = value; }
            get { return m_DueD; }
        }

        [XmlElement]
        public DateTime ModifiedD
        {
            set { m_ModifiedD = value; }
            get { return m_ModifiedD; }
        }

        [XmlElement]
        public DateTime AckD
        {
            set { m_AckD = value; }
            get { return m_AckD; }
        }

        [XmlElement]
        public bool IsCondition
        {
            set { m_IsCondition = value; }
            get { return m_IsCondition; }
        }

        [XmlElement]
        public bool IsNowDone
        {
            set { m_IsNowDone = value; }
            get { return m_IsNowDone; }
        }

        [XmlElement]
        public bool IsRead
        {
            set { m_IsRead = value; }
            get { return m_IsRead; }
        }

        [XmlArray]
        [XmlArrayItem(typeof(TaskParticipant))]
        public ArrayList Participants
        {
            set { m_Participants = value; }
            get { return m_Participants; }
        }

        [XmlElement]
        public Guid LoanId
        {
            set { m_LoanId = value; }
            get { return m_LoanId; }
        }

        [XmlElement]
        public Guid UserId
        {
            set { m_UserId = value; }
            get { return m_UserId; }
        }

        [XmlElement]
        public Guid TaskId
        {
            set { m_TaskId = value; }
            get { return m_TaskId; }
        }

        [XmlElement]
        public DateTime EventD
        {
            set { m_EventD = value; }
            get { return m_EventD; }
        }

        #endregion

        #region ( Serialization methods )

        /// <summary>
        /// Translate xml document back into alert message.
        /// </summary>
        /// <returns>
        /// Alert message representation.
        /// </returns>

        public static TaskChangeAlert ToObject(Stream sXml)
        {
            // Translate xml document back into alert message.

            XmlSerializer xs = new XmlSerializer(typeof(TaskChangeAlert));

            return xs.Deserialize(sXml) as TaskChangeAlert;
        }

        /// <summary>
        /// Translate alert message into xml document.
        /// </summary>
        /// <returns>
        /// Xml document representation.
        /// </returns>

        public override String ToString()
        {
            // Translate alert message into xml document.

            XmlSerializer xs = new XmlSerializer(typeof(TaskChangeAlert));
            StringWriter8 sw = new StringWriter8();

            xs.Serialize(sw, this);

            return sw.ToString();
        }

        #endregion

    }

    /// <summary>
    /// Track individual messages in the message queue.
    /// </summary>

    public class TaskAlertEntry
    {
        /// <summary>
        /// Track individual messages in the message queue.
        /// </summary>

        private Guid m_TaskId = Guid.Empty;
        private String m_ByWhom = String.Empty;
        private DateTime m_EventD = DateTime.MinValue;
        private String m_Label = String.Empty;
        private String m_Id = String.Empty;

        #region ( Entry Properties )

        public Guid TaskId
        {
            set { m_TaskId = value; }
            get { return m_TaskId; }
        }

        public String ByWhom
        {
            set { m_ByWhom = value; }
            get { return m_ByWhom; }
        }

        public DateTime EventD
        {
            set { m_EventD = value; }
            get { return m_EventD; }
        }

        public String Label
        {
            set { m_Label = value; }
            get { return m_Label; }
        }

        public String Id
        {
            set { m_Id = value; }
            get { return m_Id; }
        }

        #endregion

        /// <summary>
        /// Provide default id representation for simpler data binding.
        /// </summary>

        public override string ToString()
        {
            // Overload serialization to return our message id, so
            // we better support default databinding.

            return m_Id.ToString();
        }
    }

}
