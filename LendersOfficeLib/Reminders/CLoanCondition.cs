﻿using System;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Constants;
namespace LendersOffice.Reminders
{
    public class CLoanConditionObsolete
    {
        #region ( Private member variables )

        private Guid m_CondId = Guid.Empty;
        private Guid m_LoanId = Guid.Empty;
        private string m_CondDesc = "";
        private E_CondStatus m_CondPreviousStatus = E_CondStatus.Active;
        private E_CondStatus m_CondStatus = E_CondStatus.Active;
        private DateTime m_CondStatusDate = DateTime.MinValue;
        private bool m_IsLoanStillValid = true;
        private string m_CondNotes;
        private string m_CondCategoryDesc = "";
        private int m_CondOrderedPosition = 0;
        private bool m_IsRetrieved = false;
        private bool m_CondIsHidden;
        private DateTime m_CreatedD;
        private bool m_CondIsValid = true;
        private bool m_UseNotes = true; // Determines if we want to hit filedb for notes field.
        private string m_CondCompletedByUserNm = string.Empty;
        private DateTime? m_CondDeleteD = null;
        private string m_CondDeleteByUserNm = string.Empty;

        #endregion
        private bool IsDirty
        {
            get;
            set;
        }
        public override string ToString()
        {
            // For debugging, we output details
            return String.Format
                ("[CondId    ] {0}\r\n"
                + "[LoanId    ] {1}\r\n"
                + "[CondCat   ] {2}\r\n"
                + "[CondDesc  ] {3}\r\n"
                + "[CondNotes ] {4}\r\n"
                + "[CondStatus] {5}\r\n"
                + "[CondStatD ] {6}\r\n"
                + "[Position  ] {7}\r\n"
                + "[Hidden    ] {8}\r\n"
                + "[LoanValid ] {9}\r\n"
                + "[CondValid ] {10}\r\n"
                + "[CreatedD  ] {11}\r\n"
                + "[CondCompletedByUserNm] {12}\r\n"
                + "[CondDeleteD] {13}\r\n"
                + "[CondCompletedByUserNm] {14}\r\n"
                , m_CondId
                , m_LoanId
                , m_CondCategoryDesc
                , m_CondDesc
                , m_CondNotes
                , m_CondStatus
                , m_CondStatusDate
                , m_CondOrderedPosition
                , m_CondIsHidden
                , m_IsLoanStillValid
                , m_CondIsValid
                , m_CreatedD
                , m_CondCompletedByUserNm
                , m_CondDeleteD?? DateTime.MinValue
                , m_CondDeleteByUserNm
                );
        }

        public bool IsRetrieved
        {
            get { return m_IsRetrieved; }
        }

        public bool IsNew
        {
            get { return m_CondId == Guid.Empty; }
        }

        public bool UseNotes
        {
            get { return m_UseNotes; }
            set { m_UseNotes = value; }
        }

        public Guid CondId
        {
            get { return m_CondId; }
        }

        public Guid LoanId
        {
            get { return m_LoanId; }
            set { m_LoanId = value; }
        }

        public string CondDesc
        {
            get { return m_CondDesc; }
            set { m_CondDesc = Tools.ReplaceInvalidUnicodeChars(value); }
        }

        public E_CondStatus CondStatus
        {
            get { return m_CondStatus; }
            set
            {
                if (value == E_CondStatus.Active)
                    m_CondCompletedByUserNm = string.Empty;

                m_CondStatus = value;
            }
        }

        public DateTime CondStatusDate
        {
            get { return m_CondStatusDate; }
            set { m_CondStatusDate = value; }
        }

        public string CondCategoryDesc
        {
            get { return m_CondCategoryDesc; }
            set { m_CondCategoryDesc = value; }
        }

        public int CondOrderedPosition
        {
            get { return m_CondOrderedPosition; }
            set { if (value == m_CondOrderedPosition) { return; } m_CondOrderedPosition = value; IsDirty = true; }
        }

        public bool IsLoanStillValid
        {
            get { return m_IsLoanStillValid; }
        }

        public string CondNotes
        {
            get { return m_CondNotes; }
            set { m_CondNotes = value; }
        }
        public bool CondIsHidden
        {
            get { return m_CondIsHidden; }
            set { m_CondIsHidden = value; }
        }
        public DateTime CreatedD
        {
            get { return m_CreatedD; }
        }
        public bool CondIsValid
        {
            get { return m_CondIsValid; }
            set 
            {
                if (value)
                {
                    m_CondDeleteByUserNm = string.Empty;
                    m_CondDeleteD = null;
           
                }
                if (value != m_CondIsValid)
                {
                    IsDirty = true; //the user is undeleting go ahead and flag this as dirty
                }

                m_CondIsValid = value; 
            }
        }
        public string CondCompletedByUserNm
        {
            get { return m_CondCompletedByUserNm; }
            set
            {
                if (m_CondCompletedByUserNm != string.Empty)
                    m_CondStatus = E_CondStatus.Done;

                m_CondCompletedByUserNm = value;
            }
        }
        public DateTime? CondDeleteD
        {
            get { return m_CondDeleteD; }
            set { m_CondDeleteD = value; }
        }
        public string CondDeleteByUserNm
        {
            get { return m_CondDeleteByUserNm; }
            set
            {
                if (m_CondCompletedByUserNm != value)
                {
                    IsDirty = true;
                    m_CondDeleteByUserNm = value;
                }
            }
        }

        private DateTime SaveStatusDate
        {
            // If we never set a date, we are new, so we use current.
            get { return (m_CondStatusDate == DateTime.MinValue) ? DateTime.Now : m_CondStatusDate; }
        }

        private void CreateCondition(CStoredProcedureExec exec)
        {
            //Tools.LogInfo("CREATING:\n\r " + this );

            // Create a new loan condition.
            SqlParameter CondIdParam = new SqlParameter("@CondId", SqlDbType.UniqueIdentifier);
            CondIdParam.Direction = ParameterDirection.Output;


            SqlParameter[] parameters = {	  CondIdParam,
									  new SqlParameter( "@LoanId", LoanId ),
									  new SqlParameter( "@CondDesc", CondDesc ),
                                      new SqlParameter( "@CondIsValid", CondIsValid ),
									  new SqlParameter( "@CondStatus", CondStatus ),
									  new SqlParameter( "@CondStatusDate", SaveStatusDate ),
									  new SqlParameter( "@CondCategoryDesc" , CondCategoryDesc ),
									  new SqlParameter( "@CondOrderedPosition" , CondOrderedPosition ),
									  new SqlParameter( "@CondIsHidden" , CondIsHidden ), 
	                                  new SqlParameter( "@CondCompletedByUserNm", CondCompletedByUserNm ),
                                      new SqlParameter( "@CondNotes", CondNotes),
                                      new SqlParameter( "@CondDeleteD", CondDeleteD.HasValue ? (object) CondDeleteD : DBNull.Value),
                                      new SqlParameter( "@CondDeleteByUserNm", CondDeleteByUserNm)
			};

            exec.ExecuteNonQuery("CreateLoanCondition", 2, parameters);

            m_CondId = (Guid)CondIdParam.Value;
        }

        private void CreateConditionWithNotes(CStoredProcedureExec exec)
        {
            // Used by Duplicate(), we create a copy of this instance in the DB.
            // The orignal is left alone.
            SqlParameter CondIdParam = new SqlParameter("@CondId", SqlDbType.UniqueIdentifier);

            CondIdParam.Direction = ParameterDirection.Output;

            exec.ExecuteNonQuery
                ("CreateLoanCondition"
                , 2
                , new SqlParameter("@LoanId", LoanId)
                , new SqlParameter("@CondDesc", CondDesc)
                , new SqlParameter("@CondStatus", CondStatus)
                , new SqlParameter("@CondStatusDate", SaveStatusDate)
                , new SqlParameter("@CondCategoryDesc", CondCategoryDesc)
                , new SqlParameter("@CondOrderedPosition", CondOrderedPosition)
                , new SqlParameter("@CondIsHidden", CondIsHidden)
                , new SqlParameter("@CondIsValid", CondIsValid)
                , new SqlParameter("@CondCompletedByUserNm", CondCompletedByUserNm)
                , new SqlParameter("@CondNotes", CondNotes)
                , new SqlParameter("@CondDeleteD", CondDeleteD.HasValue ? (object) CondDeleteD : DBNull.Value)
                , new SqlParameter("@CondDeleteByUserNm", CondDeleteByUserNm)
                , CondIdParam
                );

            m_CondId = (Guid)CondIdParam.Value;


        }

        private void UpdateCondition(CStoredProcedureExec exec)
        {
            // Update existing loan condition.  Note that filedb update fails, we
            // throw and caller will rollback transaction.
            exec.ExecuteNonQuery
                ("UpdateLoanCondition"
                , 2
                , new SqlParameter("@CondId", CondId)
                , new SqlParameter("@LoanId", LoanId)
                , new SqlParameter("@CondDesc", CondDesc)
                , new SqlParameter("@CondStatus", CondStatus)
                , new SqlParameter("@CondStatusDate", CondStatusDate)
                , new SqlParameter("@IsLoanStillValid", IsLoanStillValid)
                , new SqlParameter("@CondCategoryDesc", CondCategoryDesc)
                , new SqlParameter("@CondOrderedPosition", CondOrderedPosition)
                , new SqlParameter("@CondIsHidden", CondIsHidden)
                , new SqlParameter("@CondIsValid", CondIsValid)
                , new SqlParameter("@CondCompletedByUserNm", CondCompletedByUserNm)
                , new SqlParameter("@CondNotes", CondNotes)
                , new SqlParameter("@CondDeleteD", CondDeleteD.HasValue ? (object) CondDeleteD : DBNull.Value)
                , new SqlParameter("@CondDeleteByUserNm", CondDeleteByUserNm)
                , new SqlParameter("@IsDirty", IsDirty)
                );

        }

        /// <summary>
        /// Save as a new condtion.
        /// </summary>
        /// <returns>
        /// True if successful.
        /// </returns>

        public bool Duplicate()
        {
            // Take the currently retrieved or initialized condition and make
            // a new one using the user's credentials. 
            try
            {
                using (CStoredProcedureExec spExec = new CStoredProcedureExec(this.BrokerId))
                {
                    // Attempt the save after setting up the transaction.

                    try
                    {
                        Guid sourceId = CondId;

                        spExec.BeginTransactionForWrite();

                        CreateConditionWithNotes(spExec);

                        spExec.CommitTransaction();

                        return true;
                    }
                    catch
                    {
                        // Oops!

                        spExec.RollbackTransaction();

                        throw;
                    }
                }
            }
            catch (Exception e)
            {
                // Oops!

                Tools.LogErrorWithCriticalTracking(e);
            }

            // We landed here because an exception was thrown.

            return false;
        }

        /// <summary>
        /// Save the condition's details.
        /// </summary>
        /// <param name="spExec">
        /// Transactional execution facilitator.
        /// </param>
        public void Save(CStoredProcedureExec spExec)
        {
            // Save this condition instance
            // Assume the transaction is open for writing.

            if (Guid.Empty == CondId)
            {
                //if its negative one it means that the order wasnt set if its warning leave it there so they always appear at the top. The order can be the same at that point 
                //we sort by desc which i think will be fine. Alternative is to load the count  and renumber them all the time which i think is more of a pain.
                if (CondOrderedPosition == -1)   
                {
                    if (false == CondCategoryDesc.Equals("Warning", StringComparison.InvariantCultureIgnoreCase))
                    {
                        CondOrderedPosition = 10000; //set some big number so it appears at the end.  
                    }
                }
                CreateCondition(spExec);
            }
            else
            {
                UpdateCondition(spExec);
            }

        }

        /// <summary>
        /// Once all saving has occurred, the saving function should
        /// notify the task alert processor in case any emails need
        /// to be sent. Only do this on successful saving of a condition. 
        /// </summary>

        public void Signal(String userDisplayName, DateTime dEventTime)
        {
            try
            {
                // Currently we use the task system's alert implementation.
                // The processor only sends when a condition changes
                // from active to done, so we only alert if that is true.
                if (m_CondPreviousStatus != E_CondStatus.Done && m_CondStatus == E_CondStatus.Done && !m_CondIsHidden)
                {
                    TaskAlertProcessor.Alert
                        (m_CondId
                        , userDisplayName
                        , (E_DiscStatus)m_CondStatus
                        , DateTime.MinValue // Created Date
                        , DateTime.MinValue // Alert Date
                        , DateTime.MinValue // Due Date
                        , DateTime.MinValue // Modified Date
                        , DateTime.MinValue // Acknowledge Date
                        , true // IsNowDone 
                        , true // IsForCondition
                        , false // IsRead
                        , null // Participants
                        , m_LoanId
                        , Guid.Empty // NotifReceiverId
                        , dEventTime
                        );
                }
            }
            catch (Exception e)
            {
                // Oops!  Keep trucking!

                Tools.LogError("Failed to tell processor of update of condition.", e);
            }

        }

        public Guid BrokerId { get; private set; }

        public CLoanConditionObsolete(Guid brokerId)
        {
            this.BrokerId = brokerId;
            CondOrderedPosition = -1;
            IsDirty = true;
        }

        public CLoanConditionObsolete(Guid brokerId, Guid condId)
        {
            this.BrokerId = brokerId;
            m_CondId = condId;
            IsDirty = false;
        }

        internal void LoadConditionFields(DbDataReader reader)
        {
            m_CondId = (Guid)reader["CondId"];
            m_LoanId = (Guid)reader["LoanId"];
            m_CondDesc = Tools.ReplaceInvalidUnicodeChars((string)reader["CondDesc"]);
            m_CondStatus = (E_CondStatus)reader["CondStatus"];
            m_CondPreviousStatus = m_CondStatus;
            m_CondStatusDate = (DateTime)reader["CondStatusDate"];
            m_IsLoanStillValid = (bool)reader["IsLoanStillValid"];
            m_CondCategoryDesc = (string)reader["CondCategoryDesc"];
            m_CondOrderedPosition = (int)reader["CondOrderedPosition"];
            m_CondIsHidden = (bool)reader["CondIsHidden"];
            m_CreatedD = (DateTime)reader["CreatedD"];
            m_CondIsValid = (bool)reader["CondIsValid"];
            m_CondCompletedByUserNm = (string)reader["CondCompletedByUserNm"];
            if (reader["CondDeleteD"] is DBNull == false)
                m_CondDeleteD = (DateTime)reader["CondDeleteD"];
            m_CondDeleteByUserNm = (string)reader["CondDeleteByUserNm"];

            if (m_UseNotes)
            {
                m_CondNotes = (string)reader["CondNotes"]; // 6/16/2009 dd - OPM 11501 - Move condition notes to db.
            }

            IsDirty = false;
        }

        public void Retrieve(CStoredProcedureExec spExec)
        {
            m_IsRetrieved = false;

            try
            {
                using (DbDataReader sR = spExec.ExecuteReader("RetrieveLoanConditionByCondId", new SqlParameter("@CondId", m_CondId)))
                {
                    if (sR.Read())
                    {
                        LoadConditionFields(sR);
                    }
                }

                m_IsRetrieved = true;
            }
            catch (Exception e)
            {
                // Oops!

                Tools.LogErrorWithCriticalTracking(e);

                throw;
            }
        }

        // Non-transactional retrieve
        public bool Retrieve()
        {
            m_IsRetrieved = false;

            try
            {
                SqlParameter[] parameters = {
                                                new SqlParameter("@CondId", m_CondId)
                                            };
                using (DbDataReader sR = StoredProcedureHelper.ExecuteReader(this.BrokerId, "RetrieveLoanConditionByCondId", parameters))
                {
                    if (sR.Read())
                    {
                        LoadConditionFields(sR);
                    }
                }
            }
            catch (Exception e)
            {
                // Oops!

                Tools.LogErrorWithCriticalTracking("Unable to retrieve condition", e);

                return false;
            }

            m_IsRetrieved = true;

            return true;
        }

        // 11/06/06 mf. We now delete conditions (per OPM 3593) instead of just marking them invalid.
        public static void DeleteLoanCondition(CStoredProcedureExec spExec, Guid CondId, Guid LoanId)
        {
            spExec.ExecuteNonQuery("DeleteLoanCondition", 2, false, new SqlParameter("@CondId", CondId), new SqlParameter("@LoanId", LoanId));

            FileDBTools.Delete(E_FileDB.Normal, CondId.ToString());
        }
    }
}