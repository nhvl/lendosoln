using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using LendersOffice.Common;

namespace LendersOffice.Reminders
{
    	
	/// <summary>
	/// Readonly condition view.
	/// </summary>

	public class CConditionObsolete : IComparable, IComparable<CConditionObsolete>
	{

        public Guid Id { get; private set; }

		#region ( Condition fields )

		/// <summary>
		/// These fields read from the row as initialized during
		/// construction or explicit init.
		/// </summary>

        private string GetString(object o)
        {
            if (o != null && o is DBNull == false)
            {
                return o.ToString();
            }
            return string.Empty;
        }
        private DateTime GetDateTime(object o)
        {
            if (o != null && o is DBNull == false)
            {
                return (DateTime)o;
            }
            return DateTime.MinValue;
        }
        private int GetInt(object o)
        {
            if (o != null && o is DBNull == false)
            {
                return (int)o;
            }
            return 0;
        }

        public string CondDesc { get; private set; }

        public string CondCompletedByUserNm { get; private set; }
        private E_CondStatus m_condStatus;

        public string CondCategoryDesc { get; private set; }

        private DateTime m_condStatusDate = DateTime.MinValue;
		public DateTime DateDone
		{
			get	{ return ( IsDone ) ? m_condStatusDate : DateTime.MinValue; }
		}

		public bool IsDone
		{
			get { return m_condStatus == E_CondStatus.Done; }
		}

		public bool IsNote
		{
			get	{ return CondCategoryDesc.ToLower().TrimWhitespaceAndBOM() == "notes"; }
		}

		#endregion

		#region ( Constructors )

        public CConditionObsolete(DbDataReader reader)
        {
            Id = (Guid)reader["CondId"];
            CondDesc = GetString(reader["CondDesc"]);
            CondCategoryDesc = GetString(reader["CondCategoryDesc"]);
            m_condStatusDate = GetDateTime(reader["CondStatusDate"]);
            m_condStatus = (E_CondStatus)GetInt(reader["CondStatus"]);
            CondCompletedByUserNm = GetString(reader["CondCompletedByUserNm"]);
        }

		#endregion

		public int CompareTo(CConditionObsolete o) 
		{
            return this.CondDesc.CompareTo(o.CondDesc);
		}

        #region IComparable Members

        public int CompareTo(object obj)
        {
            CConditionObsolete _obj = obj as CConditionObsolete;
            if (null != _obj)
            {
                return this.CondDesc.CompareTo(_obj.CondDesc);
            }
            return -1;
        }

        #endregion
    }


}