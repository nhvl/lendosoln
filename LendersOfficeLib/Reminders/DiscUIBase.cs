namespace LendersOffice.Reminders
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Web.UI.WebControls;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.ConfigSystem;
    using LendersOffice.ConfigSystem.Operations;
    using LendersOffice.Security;

    public class CDiscUIBase : System.Web.UI.UserControl
	{
		protected const int MAX_FILTER_LENGTH = 50; // Size of each Task List filter

		protected string GetDiscJoinId(object discLogId, object notifId, object notifIsValid)
		{
            if (notifIsValid == DBNull.Value)
            {
                return discLogId.ToString();
            }
            else
            {
                return notifId.ToString();
            }
		}

        protected string GetDiscJoinLinkFunctionName(object notifIsValid)
        {
            if (notifIsValid == DBNull.Value)
            {
                return "joinDiscussion";
            }
            else if ((bool)notifIsValid)
            {
                return "editNotif";
            }
            else
            {
                return "activateNotif";
            }
        }

        protected string GetDiscJoinLinkDescription(object notifIsValid)
        {
            if (notifIsValid == DBNull.Value)
            {
                return "join";
            }
            else if ((bool)notifIsValid)
            {
                return "edit";
            }
            else
            {
                return "rejoin";
            }
        }

        protected string DisplayTrackNotifIsReadValue(bool TrackNotifIsRead)
        {
            return TrackNotifIsRead ? "Yes" : "No";
        }

        protected string DisplayDiscPriorityClass(int discPriority)
        {
            return GetDiscPriorityCss((E_DiscPriority)discPriority);
        }

        protected string DisplayDiscPriorityValue(int discPriority)
        {
            return GetDiscPriorityDesc((E_DiscPriority)discPriority);
        }

        protected string DisplayDiscStatusClass(int status)
        {
            return GetDiscStatusCss((E_DiscStatus)status);
        }

        protected string DisplayDiscStatusValue(int status)
        {
            return GetDiscStatusDesc((E_DiscStatus)status);
        }

        protected string DisplayDiscDueDateClass(object dueDate)
        {
            if (dueDate is DateTime)
            {
                return GetDiscDueDateCss((DateTime)dueDate);
            }
            else return "";
        }

        private IEnumerable<string> x_dependencyList = null;
        private IEnumerable<string> GetDependencyFieldList()
        {
            if (x_dependencyList == null)
            {
                BrokerUserPrincipal currentUser = BrokerUserPrincipal.CurrentPrincipal;

                IEnumerable<string> list = LendingQBExecutingEngine.GetDependencyFieldsByOperation(currentUser.BrokerId, WorkflowOperations.ReadLoanOrTemplate,
                    WorkflowOperations.WriteLoanOrTemplate);

                List<string> tempList = new List<string>(list);
                tempList.Add("sStatusT");
                x_dependencyList = tempList;
            }
            return x_dependencyList;
        }
		protected void GenerateLoanLinks(object sender, System.Web.UI.WebControls.DataGridItemEventArgs a, Hashtable accessHash)
		{
			// 02/07/06 MF. OPM 3613.  We now allow users to work with their loans
			// via links from the three task tabs.  Here we determine which links to show
			// based on access.  Hash to avoid repeated lookups of same lead/loan.
			
			if (a.Item.ItemType == ListItemType.Item || a.Item.ItemType == ListItemType.AlternatingItem)
			{
                BrokerUserPrincipal currentUser = BrokerUserPrincipal.CurrentPrincipal;

				Guid loanId = (Guid) System.Web.UI.DataBinder.Eval(a.Item.DataItem, "DiscRefObjId");
				
				// No need to process further if there is no referenced loan
				if (loanId.ToString() == "00000000-0000-0000-0000-000000000000")
					return;

				// Determine Loan Status (need to know if it is a Lead)
                LoanValueEvaluator valueEvaluator = new LoanValueEvaluator(currentUser.ConnectionInfo, GetDependencyFieldList(), loanId);
                valueEvaluator.SetEvaluatingPrincipal(ExecutingEnginePrincipal.CreateFrom(currentUser));


                IList<int> sStatusTList = valueEvaluator.GetIntValues("sStatusT");
                if (sStatusTList == null || sStatusTList.Count != 1)
                {
                    throw new GenericUserErrorMessageException("Could not load sStatusT");
                }
                E_sStatusT loanStatus = (E_sStatusT) sStatusTList[0];
                
				bool isLeadFile = false;
				switch(loanStatus)
				{
					case E_sStatusT.Lead_New:
					case E_sStatusT.Lead_Canceled:
					case E_sStatusT.Lead_Declined:
					case E_sStatusT.Lead_Other:
						isLeadFile = true;
						break;
				}
				
				// Check Access
				bool[] loanInfo;
				
				if ( !accessHash.Contains(loanId))
				{
                    bool canWritePermission = LendingQBExecutingEngine.CanPerform(WorkflowOperations.WriteLoanOrTemplate, valueEvaluator);
                    bool canReadPermission = LendingQBExecutingEngine.CanPerform(WorkflowOperations.ReadLoanOrTemplate, valueEvaluator);

					// Store a boolean array that contains only the needed data:
					// if the user can write, read, and it if is a lead
                    loanInfo = new bool[] { canWritePermission, canReadPermission, isLeadFile };
					accessHash.Add(loanId, loanInfo);
				}
				else
					loanInfo = (bool[]) accessHash[ loanId ];

				// Determine which links to display. Use simliar rules as pipeline
				// with exception that if a person has no access at all to the object,
				// the row is still displayed, but no  links are displayed.

				const int canWrite = 0;
				const int canRead = 1;
				const int isLead = 2;
					
				// Hide all links if user cannot even read the referenced loan
				Panel aPanel = a.Item.FindControl("LoanNumberPlainText") as Panel;
				if (!loanInfo[canRead] && !loanInfo[canWrite])
				{
					if (aPanel != null)
					{
						aPanel.Visible = true;
						aPanel = a.Item.FindControl("LoanNumberLinks") as Panel;
						if (aPanel != null)
							aPanel.Visible = false;
					}
				}

				else
				{
					// Set visiblity for viewing loan in editor
					aPanel = a.Item.FindControl("LoadLink") as Panel;
					if (aPanel != null)
					{  
						if (loanInfo[isLead] || !currentUser.HasPermission(Permission.CanViewLoanInEditor)  
							|| !loanInfo[canRead] || loanInfo[canWrite] )
							aPanel.Visible = false;

						else
							aPanel.Visible = true;
					}

					// Set visibility for viewing loan in summary				
					aPanel = a.Item.FindControl( "ViewLink" ) as Panel;
					if( aPanel != null )
					{
						if( loanInfo[isLead] || ( loanInfo[canRead] && !loanInfo[canWrite] 
							&& currentUser.HasPermission(Permission.CanViewLoanInEditor)))
							aPanel.Visible = false;
						else
							aPanel.Visible = true;
					}

					// Set visibility for viewing lead in summary				
					aPanel = a.Item.FindControl( "ViewLeadLink" ) as Panel;
					if( aPanel != null )
					{
						if (!loanInfo[isLead] || (loanInfo[canRead] && !loanInfo[canWrite]
							&& currentUser.HasPermission(Permission.CanViewLoanInEditor)))
							aPanel.Visible = false;
						else
							aPanel.Visible = true;
					}

					// Set visibility for viewing lead in editor
					aPanel = a.Item.FindControl( "LoadLeadLink" ) as Panel;
					if( aPanel != null )
					{
						if (!loanInfo[isLead] || loanInfo[canWrite] || !loanInfo[canRead]
							|| !currentUser.HasPermission(Permission.CanViewLoanInEditor))
							aPanel.Visible = false;
						else
							aPanel.Visible = true;
					}

					// Set visibility for editing the loan
					aPanel = a.Item.FindControl( "EditLink" ) as Panel;
					if( aPanel != null )
					{
						WebControl cShow = aPanel.FindControl( "Show" ) as WebControl;
						WebControl cHide = aPanel.FindControl( "Hide" ) as WebControl;
						if( !loanInfo[canWrite] || loanInfo[isLead] )
						{  
							if( loanInfo[isLead] ) // Hide it completely
								aPanel.Visible = false;
							else
							{ // Display disabled link
								aPanel.Visible = true;
								cShow.Visible = false;
								cHide.Visible = true;
							}
						}
						else
						{  // Display real link
							aPanel.Visible = true;
							cHide.Visible = false;
							cShow.Visible = true;
						}
					}
				
					// Set visibility for editing lead
					aPanel = a.Item.FindControl( "LeadLink" ) as Panel;
					if( aPanel != null )
					{
						WebControl cShow = aPanel.FindControl( "ShowLeadEdit" ) as WebControl;
						WebControl cHide = aPanel.FindControl( "DisabledLeadEdit" ) as WebControl;
						if( !loanInfo[canWrite] || !loanInfo[isLead] )
						{
							if( !loanInfo[isLead] ) // Hide it completely
								aPanel.Visible = false;
							else
							{ // Display disabled link
								aPanel.Visible = true;
								cShow.Visible = false;
								cHide.Visible = true;
							}
						}
						else
						{ // Display real link
							aPanel.Visible = true;
							cHide.Visible = false;
							cShow.Visible = true;
						}
					}

					// Set visibility of the export link
					aPanel = a.Item.FindControl( "ExportLink" ) as Panel;
					if(aPanel != null)
					{
						if (!currentUser.HasPermission(Permission.CanExportLoan))
							aPanel.Visible = false;
						else
							aPanel.Visible = true;
					}
				}
			}
		}


		#region Enums

		// MF 01/10/06 OPM 3506. Enum for indices of checkbox flags for DB flag array.
		// MF 01/24/06 OPM 3747. Also save filter for Tracked Tasks.
		
		// Tasks in Your Loans
		protected enum checkBoxes
		{
			TaskStatusActive = 0,
			TaskStatusDone = 1,
			TaskStatusSuspended = 2,
			TaskStatusCancelled = 3,
			ParticipationActive = 4,
			ParticipationInactive = 5,
			ParticipationOther = 6,
			PriorityLow = 7,
			PriorityNormal = 8,
			PriorityHigh = 9,
			LoanStatusOpen = 10,
			LoanStatusPreQual = 11,
			LoanStatusPreApprove = 12,
			LoanStatusSubmitted = 13,
			LoanStatusApproved = 14,
			LoanStatusDocs = 15,
			LoanStatusFunded = 16,
			LoanStatusOnHold = 17,
			LoanStatusUnderwritten = 18,
			LoanStatusRecorded = 19,
			LoanStatusSuspended = 20,
			LoanStatusRejected = 21,
			LoanStatusCancelled = 22,
			LoanStatusClosed = 23,
			LoanStatusOther = 24,
			TaskAuthorMine = 25,
			TaskAuthorOthers = 26,
		}

		//Tracked Tasks
		protected enum trackCheckBoxes
		{
			ReadStatusRead = 0,
			ReadStatusNotRead = 1,
			TaskStatusActive = 2,
			TaskStatusDone = 3,
			TaskStatusSuspended = 4,
			TaskStatusCancelled = 5,
			PriorityLow = 6,
			PriorityNormal = 7,
			PriorityHigh = 8
		}


		#endregion

		#region Style CSS
		
		static public string GetTrackNotifIsReadCss( bool isRead, bool isNotifValid )
		{
			if( isRead )
				return isNotifValid ? "DiscTrackNotifIsReadYesNotifValidYes" : "DiscTrackNotifIsReadYesNotifValidNo";
			else
				return isNotifValid ? "DiscTrackNotifIsReadNoNotifValidYes" : "DiscTrackNotifIsReadNoNotifValidNo";
		}

		static public string GetDiscSubjectCss( bool isRead )
		{
			return isRead ? "DiscSubjectRead" : "DiscSubjectUnread";
		}

        static public string GetDiscStatusCss( E_DiscStatus discStatus )
		{
			switch( discStatus )
			{
				case E_DiscStatus.Active:		return "DiscStatusActive";
				case E_DiscStatus.Cancelled:	return "DiscStatusCancelled";
				case E_DiscStatus.Done:			return "DiscStatusDone";
				case E_DiscStatus.Suspended:	return "DiscStatusSuspended";
				default:
					return "";
			}
		}
		
		static public string GetDiscPriorityCss( E_DiscPriority discPriority )
		{
			switch( discPriority )
			{
				case E_DiscPriority.High:		return "DiscPriorityHigh";
				case E_DiscPriority.Low:		return "DiscPriorityLow";
				case E_DiscPriority.Normal:		return "DiscPriorityNormal";
				default:
					return "";
			}
		}
		static public string GetDiscDueDateCss( DateTime discDueDate )
		{
			if( discDueDate.CompareTo( DateTime.Now ) < 0 )
				return "DiscDue";
			else
				return "DiscDueNot";
		}

		static public string GetDiscNotifAlertDateCss( DateTime discNotifAlertDate )
		{
			if( discNotifAlertDate.CompareTo( DateTime.Now ) < 0 )
				return "DiscNotifAlert";
			else
				return "DiscNotifAlertNot";
		}

		static public string GetDiscStatusDesc( E_DiscStatus discStatus )
		{
			switch( discStatus )
			{
				case E_DiscStatus.Active:		return "Active";
				case E_DiscStatus.Cancelled:	return "Cancelled";
				case E_DiscStatus.Done:			return "Done";
				case E_DiscStatus.Suspended:	return "Suspended";
				default:
					return "???";
			}
		}
		static public string GetDiscPriorityDesc( E_DiscPriority discPriority )
		{
			switch( discPriority )
			{
				case E_DiscPriority.High:		return "High";
				case E_DiscPriority.Low:		return "Low";
				case E_DiscPriority.Normal:		return "Normal";
				default:
					return "";
			}
		}

		#endregion // Style

	}

}
