﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LendersOffice.Reminders
{
    public enum E_CondStatus
    {
        Active = 0,
        Done = 1,
    }
}
