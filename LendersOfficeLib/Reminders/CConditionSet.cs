﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using DataAccess;
using System.Diagnostics;
using LendersOffice.HttpModule;
using LendersOffice.Common;

namespace LendersOffice.Reminders
{
    /// <summary>
    /// Read-only set of loan conditions.  If you need to modify
    /// conditions, use the LoanConditionSet class instead.  We
    /// only load non-hidden, non-deleted conditions.
    /// </summary>

    public class CConditionSetObsolete : IEnumerable<CConditionObsolete>
    {

        protected List<CConditionObsolete> m_Set; // set of discussion db objects

        public ConditionDesc[] Digest
        {
            // 11/1/06 mf. I brought Digest over from LoanConditionSet
            // because it makes sense to use this lighter class instead.
            // This is used by the loan view screen from the pipeline.

            get
            {
                // Walk the list and make an array.

                int i = 0; 
                ConditionDesc[] cdList = new ConditionDesc[m_Set.Count];

                foreach (CConditionObsolete condition in m_Set)
                {

                    ConditionDesc cD = new ConditionDesc();

                    cD.Category = condition.CondCategoryDesc;
                    cD.Description = condition.CondDesc;
                    cD.IsHidden = false;
                    cD.Id = condition.Id;


                    cD.IsDone = condition.IsDone;

                    if (condition.IsDone)
                    {
                        cD.DateDone = condition.DateDone.ToShortDateString();
                    }

                    cdList[i] = cD;

                    ++i;
                }

                return cdList;
            }
        }


        /// <summary>
        /// Load up the conditions associated with the given
        /// loan identifier.
        /// </summary>
        /// <param name="loanId">
        /// Loan identifier.
        /// </param>
        private void LoadByLoanId(Guid loanId)
        {
            using (PerformanceMonitorItem.Time("CConditionSet.LoadByLoanId"))
            {
                Guid brokerId = Guid.Empty;
                DbConnectionInfo.GetConnectionInfoByLoanId(loanId, out brokerId);
                m_Set = new List<CConditionObsolete>();
                SqlParameter[] parameters = {
                                            new SqlParameter("@LoanId", loanId)
                                            , new SqlParameter("@Deleted", false)
                                            , new SqlParameter("@Hidden", false)
                                            , new SqlParameter("@ShowAll", false)
                                        };
                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "ListLoanConditionsByLoanId", parameters))
                {
                    while (reader.Read())
                    {
                        m_Set.Add(new CConditionObsolete(reader));
                    }
                }
            }
        }

        #region ( Constructors )

        public CConditionSetObsolete(Guid loanId)
        {
            LoadByLoanId(loanId);
        }


        #endregion


        #region IEnumerable<CCondition> Members

        IEnumerator<CConditionObsolete> IEnumerable<CConditionObsolete>.GetEnumerator()
        {
            return m_Set.GetEnumerator();
        }

        #endregion

        #region IEnumerable Members

        IEnumerator IEnumerable.GetEnumerator()
        {
            return m_Set.GetEnumerator();
        }

        #endregion
    }
}
