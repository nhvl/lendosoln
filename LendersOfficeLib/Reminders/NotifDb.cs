using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using LendersOffice.DataLock;
using LendersOffice.Common;
using DataAccess;

namespace LendersOffice.Reminders
{
	/// <summary>
	/// Throw when we fail to save or edit or create a discussion
	/// log object (including its notifications).
	/// </summary>

	public class TaskException : CBaseException
	{
		#region ( Constructors )

		/// <summary>
		/// Construct default.
		/// </summary>

		public TaskException( CDiscNotif dLog , String message , Exception inner )
            : base(ErrorMessages.TaskOperationFailed, String.Format("Task operation failed ({0}).\r\n{1}.\r\n{2}", message, dLog, inner))
		{
		}

		/// <summary>
		/// Construct default.
		/// </summary>

		public TaskException( CDiscNotif dLog , Exception inner )
		: base(ErrorMessages.TaskOperationFailed, String.Format( "Task operation failed.\r\n{0}.\r\n{1}", dLog, inner))
		{
		}

		/// <summary>
		/// Construct default.
		/// </summary>

		public TaskException( CDiscNotif dLog , String message )
		: base(ErrorMessages.TaskOperationFailed, String.Format( "Task operation failed ({0}).\r\n{1}", message, dLog))
		{
		}

		/// <summary>
		/// Construct default.
		/// </summary>

		public TaskException( CDiscNotif dLog )
		: base(ErrorMessages.TaskOperationFailed, String.Format( "Task operation failed.\r\n{0}", dLog))
		{
		}

		/// <summary>
		/// Construct default.
		/// </summary>

		public TaskException( String message )
		: base(ErrorMessages.TaskOperationFailed, String.Format( "Task operation failed ({0}).", message))
		{
		}

		/// <summary>
		/// Construct default.
		/// </summary>

		public TaskException()
		: base(ErrorMessages.TaskOperationFailed, ErrorMessages.TaskOperationFailed)
		{
		}

		#endregion

	}
	
	/// <summary>
	/// Discussion status
	/// </summary>

	public enum E_DiscStatus 
	{
		Active = 0,
		Done = 1,
		Suspended = 2,
		Cancelled = 3
	}

	/// <summary>
	/// Discussion priority
	/// </summary>

	public enum E_DiscPriority 
	{
		High = 0,
		Normal = 1,
		Low = 2
	}

	/// <summary>
	/// Summary description for CDiscNotif.
	/// </summary>

	public class CDiscNotif
	{
        #region ( Private member variables )

		// Memo specific
		private Guid			m_notifId = Guid.Empty;
		private DateTime		m_notifAlertDate    = DateTime.MinValue;
		private DateTime		m_notifUpdatedDate  = DateTime.MinValue;
		private DateTime		m_notifLastReadDate = DateTime.MinValue;
		private DateTime        m_notifAckDate      = DateTime.MinValue;
		private bool			m_notifIsRead = false; // new notif to user's self should be unread as default
		private Guid			m_notifReceiverUserId;
		private string			m_notifReceiverLoginNm = "";
		private bool			m_notifIsValid = true;
		private DateTime		m_notifInvalidDate = DateTime.MinValue;

		// Discussion Log 
		private Guid			m_discLogId = Guid.Empty;
		private Guid			m_discCreatorUserId = Guid.Empty;
		private Guid			m_discRefObjId = Guid.Empty;
		private string			m_discRefObjType = "Loan_File"; // Default to loan file object.
		private string			m_discRefObjNm1 = ""; // loan name
		private string			m_discRefObjNm2 = ""; // primary borrower name		
		private DateTime		m_discCreatedDate = DateTime.MinValue;
		private DateTime		m_discDueDate = DateTime.MinValue;
		private string			m_discSubject = "";
		private E_DiscPriority	m_discPriority = E_DiscPriority.Normal;
		private E_DiscStatus	m_discPrevious = E_DiscStatus.Active;
		private E_DiscStatus	m_discStatus = E_DiscStatus.Active;
		private DateTime        m_discStatusDate = DateTime.MinValue;
		private bool			m_discIsRefObjValid = true;
		private string			m_discCreatorLoginNm = "";
		private string			m_discHistory;
		private string			m_newMsg = "";
		private ArrayList		m_discParticipants = new ArrayList();
		private bool            m_isRetrieved = false;
		private bool            m_isUpdated = false;

		private StringBuilder	m_addHistory = new StringBuilder( 200 );

		private Guid			m_discLastModifierUserId = Guid.Empty;
		private string			m_discLastModifierLoginNm = "";
		private DateTime        m_discLastModifyDate = DateTime.MinValue;

		/// <summary>
		/// 12/17/2004 kb - Is valid is now used to check if the
		/// discussion task was deleted.  We also have the state
		/// of being without any attached notifications.  This
		/// is not deleted, only unlinked to any participant.
		/// We default true for new tasks.  Make sure to load
		/// this in on retrieve.
		/// </summary>

		private bool m_discIsValid = true;
        
        #endregion

		#region ( Data lock complex )

		public class Locker : LockAdapter
		{
			/// <summary>
			/// Each adapter of this kind uses exactly one controller
			/// to manage all the checked out objects of this type.
			/// To manage the size, we should keep count of all the
			/// open instances and delete the lock record when the
			/// last one has left.  Because adapters sit outside of
			/// the lifetime of a page, we may never know when an
			/// adapter has died.
			/// </summary>

			private static LockController m_lC = new LockController();

			/// <summary>
			/// Construct default.
			/// </summary>

			public Locker( Guid discLogId ) : base( m_lC , discLogId )
			{
			}

		}

		#endregion

        public Guid BrokerID { get; private set; }

		public override String ToString()
		{
			// Dump the important details for debugging
			// when exceptions are recorded.

			return String.Format
				( "[B] {0}\r\n"
				+ "[D] {1}\r\n"
				+ "[N] {2}\r\n"
				+ "[R] {3}\r\n"
				+ "[R] {4}\r\n"
				+ "[U] {5}\r\n"
				+ "[U] {6}\r\n"
				+ "[S] {7}\r\n"
				+ "[L] {8}\r\n"
				+ "[B] {9}\r\n"
				+ "[F] {10}\r\n"
				+ "[R] {11}\r\n"
				+ "[V] {12}\r\n"
				, this.BrokerID
				, m_discLogId
				, m_notifId
				, m_notifReceiverLoginNm
				, m_notifReceiverUserId
				, m_discCreatorLoginNm
				, m_discCreatorUserId
				, m_discSubject
				, m_discRefObjNm1
				, m_discRefObjNm2
				, m_discIsRefObjValid
				, m_isRetrieved
				, m_discIsValid
				);
		}

		#region ( Valid check )

		public Boolean IsRetrieved
		{
			// Access member.

			get
			{
				if( m_isRetrieved == true )
				{
					return true;
				}

				return false;
			}
		}

		public Boolean IsValid
		{
			// Access member.

			get
			{
				return m_discIsValid;
			}
		}

		public Boolean IsNew
		{
			// Access member.

			get
			{
				if( m_discLogId == Guid.Empty && m_notifId == Guid.Empty )
				{
					return true;
				}

				return false;
			}
		}

		#endregion

		#region ( Notif )

		public Guid NotifId 
		{
			get { return m_notifId; }
		}

		private Guid NotifId_
		{
			set { m_notifId = value; }
		}

		public DateTime			NotifAlertDate 
		{
			get { return m_notifAlertDate; }
			set 
			{ 
				m_notifAlertDate = value;
			}
		}

		private DateTime NotifUpdatedDate
		{
			get { return m_notifUpdatedDate; }
		}

		public Guid	NotifReceiverUserId
		{
			get { return m_notifReceiverUserId;  } 
			set { m_notifReceiverUserId = value; }
		}

		public string NotifReceiverLoginNm
		{
			get { return m_notifReceiverLoginNm;  }
			set { m_notifReceiverLoginNm = value; }
		}
		
		private DateTime NotifLastReadDate
		{
			get { return m_notifLastReadDate;  }
            set { m_notifLastReadDate = value; }
		}

		private DateTime NotifAckDate
		{
			get { return m_notifAckDate;  }
			set { m_notifAckDate = value; }
		}

		public bool NotifIsValid
		{
			get { return m_notifIsValid; }
			set { m_notifIsValid = value; }
		}

		public bool	NotifIsRead
		{
			get { return m_notifIsRead; }
			set { m_notifIsRead = value; }
		}

		/// <summary>
		/// Set this to DateTime.MinValue for n/a value
		/// </summary>

		public DateTime NotifInvalidDate
		{
			get { return m_notifInvalidDate; }
			set { m_notifInvalidDate = value; }
		}

		public Boolean DeleteNotif() 
		{
			// Remove the user's hook into this task.  The task will
			// still exist after the last person has quit because the
			// loan still references it (you can re-join via the tasks
			// in my loans interface).

			SqlParameter taskId = new SqlParameter( "@DiscLogId"           , Guid.Empty );
			SqlParameter userId = new SqlParameter( "@NotifReceiverUserId" , Guid.Empty );
			Int32 nRes;

			taskId.Direction = ParameterDirection.Output;
			userId.Direction = ParameterDirection.Output;

            SqlParameter[] parameters = {
                                            new SqlParameter( "@NotifId" , m_notifId ),
                                            taskId,
                                            userId
                                        };
			nRes = StoredProcedureHelper.ExecuteNonQuery( this.BrokerID, "DeleteDiscNotif", 1, parameters);

			if( nRes > 0 )
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		
		#endregion // Notifications
		
		#region ( Discussion )
		public Guid DiscLastModifierUserId
		{
			get { return m_discLastModifierUserId; }
			set { m_discLastModifierUserId = value; }
		}

		public string DiscLastModifierLoginNm
		{
			get { return m_discLastModifierLoginNm; }
			set { m_discLastModifierLoginNm = value; }
		}

		public DateTime DiscLastModifyDate
		{
			get { return m_discLastModifyDate; }
		}

		public Guid DiscLogId
		{
			get { return m_discLogId; }
		}
		private Guid DiscLogId_
		{
			set { m_discLogId = value; }
		}

		public string NewMsg
		{
			set 
			{ 
				string newVal = value.TrimWhitespaceAndBOM();
				if( newVal != m_newMsg )
				{
					m_addHistory.Append( "\nMESSAGE:" );
					m_addHistory.Append( ' ', INDENT_SPACE - "MESSAGE:".Length );
					m_addHistory.Append( newVal );

					m_newMsg = value;

					m_isUpdated = true;
				}
			}
		}
		private string NewMsg_
		{
			get { return m_newMsg; }
		}

		public Guid DiscCreatorUserId
		{
			get { return m_discCreatorUserId; }
			set { m_discCreatorUserId = value; } 
		}

		private static int INDENT_SPACE = 20; // TODO: Avoid negative number when use this constant.
		public Guid DiscRefObjId
		{
			get{ return m_discRefObjId; }
			set{ m_discRefObjId = value; }
		}
		public string			DiscRefObjType 
		{
			get { return m_discRefObjType; }
			set { m_discRefObjType = value.TrimWhitespaceAndBOM(); }
		}
		/// <summary>
		/// Loan File Number
		/// </summary>
		public string DiscRefObjNm1 
		{
			get { return m_discRefObjNm1; }
			set 
			{ 
				if( null == value )
				{
					m_discRefObjNm1 = "";
					return;
				}

				string val = value.TrimWhitespaceAndBOM();
				if( m_discRefObjNm1 != val )
				{
					m_addHistory.Append( "\nLOAN FILE:" );
					m_addHistory.Append( ' ', INDENT_SPACE - "LOAN FILE:".Length );

					if( DiscLogId == Guid.Empty )
						m_addHistory.Append( val );
					else
						m_addHistory.Append( m_discRefObjNm1 + " -> " + val );

					m_discRefObjNm1 = val;
				}				
			}
		}


		public string DiscRefObjNm2
		{
			get{ return m_discRefObjNm2; }
			set{ m_discRefObjNm2 = value; }
		}

		
		public DateTime		DiscCreatedDate
		{
			get { return m_discCreatedDate; }
		}
		
		/// <summary>
		/// Pass DateTime.MinValue to clear due-date.
		/// </summary>
		public DateTime DiscDueDate
		{
			get { return m_discDueDate; }
			set
			{
				if( m_discDueDate != value )
				{
					m_addHistory.Append( "\nDUE DATE:" );
					m_addHistory.Append( ' ', INDENT_SPACE - "DUE DATE:".Length );

					if( DiscLogId == Guid.Empty )
						m_addHistory.Append( value );
					else
					{
						string oldDate = m_discDueDate == DateTime.MinValue ? "none" : m_discDueDate.ToString(); 
						string newDate = value == DateTime.MinValue ? "none" : value.ToString();
						m_addHistory.Append( oldDate + " -> " + newDate );
					}

					m_discDueDate = value; 

					m_isUpdated = true;
				}
			}
		}

		public string DiscSubject 
		{
			get { return m_discSubject; }
			set 
			{ 
				if( m_discSubject != value )
				{
					m_addHistory.Append( "\nSUBJECT:" );
					m_addHistory.Append( ' ', INDENT_SPACE - "SUBJECT:".Length );

					if( DiscLogId == Guid.Empty )
						m_addHistory.Append( value );
					else
						m_addHistory.Append( m_discSubject + " -> " + value );

					m_discSubject = value;

					m_isUpdated = true;
				}
			}
		}
		
		/// <summary>
		/// 0 - High, 1 - Normal, 2 - Low
		/// </summary>
		public E_DiscPriority  DiscPriority 
		{
			get { return m_discPriority; }
			set 
			{ 
				if( m_discPriority != value )
				{
					m_addHistory.Append( "\nPRIORITY:" );
					m_addHistory.Append( ' ', 14 - "nPRIORITY:".Length );
					if( DiscLogId == Guid.Empty )
						m_addHistory.Append( DiscPriorityDesc( value ) );
					else
						m_addHistory.Append( DiscPriorityDesc( m_discPriority ) + " -> " + DiscPriorityDesc( value ) );

					m_discPriority = value; 

					m_isUpdated = true;
				}
			}
		}
		
		public E_DiscStatus	DiscStatus 
		{
			get { return m_discStatus; }
			set 
			{ 
				if( m_discStatus != value )
				{
					m_addHistory.Append( "\nSTATUS:" );
					m_addHistory.Append( ' ', INDENT_SPACE - "STATUS:".Length );
					if( DiscLogId == Guid.Empty )
						m_addHistory.Append( DiscStatusDesc( value ) );
					else
						m_addHistory.Append( DiscStatusDesc( m_discStatus ) + " -> " + DiscStatusDesc( value ) );

					m_discStatusDate = DateTime.Now;

					m_discStatus = value;

					m_isUpdated = true;
				}
			}
		}

		public DateTime DiscStatusDate
		{
			get { return  m_discStatusDate; }
			set
			{
				if( value != m_discStatusDate )
				{
					m_isUpdated = true;
				}

				m_discStatusDate = value;
			}
		}

		public bool DiscIsRefObjValid
		{
			get { return m_discIsRefObjValid; }
		}

		public bool DiscIsValid
		{
			get { return  m_discIsValid; }
			set { m_discIsValid = value; }
		}

		public string DiscCreatorLoginNm
		{
			get { return  m_discCreatorLoginNm; }
			set { m_discCreatorLoginNm = value; } 
		}

		public string DiscHistory 
		{
			get { return m_discHistory; }
			set { m_discHistory = value; }
		}
		
		public static string DiscPriorityDesc( E_DiscPriority prior )
		{
			switch( prior )
			{
				case E_DiscPriority.High:	return "High";
				case E_DiscPriority.Low:	return "Low";
				case E_DiscPriority.Normal: return "Normal";
				default: return "???";
			}
		}

		public static string DiscStatusDesc( E_DiscStatus discStatus) 
		{
			switch (discStatus) 
			{
				case E_DiscStatus.Active:		return "Active"; 
				case E_DiscStatus.Cancelled:	return "Cancelled";
				case E_DiscStatus.Done:			return "Done";
				case E_DiscStatus.Suspended:	return "Suspended";
			}
			return "";
		}
        #endregion // DISCUSSION

		private static DateTime GetDate( object dbValObj )
		{
			if (dbValObj == DBNull.Value) 
				return DateTime.MinValue;
			else 
				return (DateTime) dbValObj;
		}

		private static object GetSaveDate( DateTime dt )
		{
			if( dt == DateTime.MinValue )
				return DBNull.Value;
			return dt;
		}

		private void CreateDisc( CStoredProcedureExec exec, string userLoginNm , bool bAutoGenerateHistory , DateTime dCreatedDate )
		{
			/*
			CREATE PROCEDURE CreateDiscussionLog
				@DiscLogId uniqueidentifier out,
				@DiscCreatorUserId uniqueidentifier,
				@DiscRefObjId uniqueidentifier,
				@DiscRefObjType varchar(36),
				@DiscRefObjNm1 varchar(36),
				@DiscRefObjNm2 varchar(100),
				@DiscDueDate datetime,
				@DiscSubject varchar (100),
				@DiscPriority int,
				@DiscStatus int,
				@DiscCreatorLoginNm varchar(36),
				@BrokerId uniqueidentifier
			*/

			DiscLogId_ = Guid.NewGuid();

			SqlParameter logIdParam = new SqlParameter( "@DiscLogId", SqlDbType.UniqueIdentifier );
			logIdParam.Direction = ParameterDirection.Output;

			m_discCreatedDate = dCreatedDate;

			SqlParameter[] pars = {	  logIdParam,
									  new SqlParameter( "@DiscCreatorUserId", DiscCreatorUserId ),
									  new SqlParameter( "@DiscRefObjId", DiscRefObjId ),
									  new SqlParameter( "@DiscRefObjType", DiscRefObjType ),
									  new SqlParameter( "@DiscRefObjNm1", DiscRefObjNm1 ),
									  new SqlParameter( "@DiscRefObjNm2", DiscRefObjNm2 ),
									  new SqlParameter( "@DiscDueDate", GetSaveDate( DiscDueDate ) ),
									  new SqlParameter( "@DiscSubject", DiscSubject ),
									  new SqlParameter( "@DiscPriority", DiscPriority ),
									  new SqlParameter( "@DiscStatus", DiscStatus ),
									  new SqlParameter( "@DiscStatusDate", DiscCreatedDate ),
									  new SqlParameter( "@DiscCreatorLoginNm", DiscCreatorLoginNm ),
									  new SqlParameter( "@DiscCreationDate" , DiscCreatedDate ),
									  new SqlParameter( "@IsValid" , DiscIsValid ),
									  new SqlParameter( "@BrokerId", BrokerID )
								  };

			exec.ExecuteNonQuery
				( "CreateDiscussionLog"
				, 2
				, pars
				);

			m_discLogId = (Guid) logIdParam.Value;

			CFileDBFields fileDbField = new CFileDBFields( m_discLogId );

			if( bAutoGenerateHistory == true )
			{
				fileDbField.Save( "DiscussionLog", CreateHistoryForFirstTime( userLoginNm ) );
			}
			else
			{
				fileDbField.Save( "DiscussionLog", DiscHistory );
			}
		}

		private void CreateDiscAndEmbedLog( CStoredProcedureExec exec , string userLoginNm , bool bAutoGenerateHistory , DateTime dCreatedDate )
		{
			try
			{
				// Copy this discussion log/task by copying the contents into
				// a new instance within the database.
				//
				// CREATE PROCEDURE CreateDiscussionLog
				//     @DiscLogId uniqueidentifier out,
				//     @DiscCreatorUserId uniqueidentifier,
				//     @DiscRefObjId uniqueidentifier,
				//     @DiscRefObjType varchar(36),
				//     @DiscRefObjNm1 varchar(36),
				//     @DiscRefObjNm2 varchar(100),
				//     @DiscDueDate datetime,
				//     @DiscSubject varchar (100),
				//     @DiscPriority int,
				//     @DiscStatus int,
				//     @DiscCreatorLoginNm varchar(36),
				//     @BrokerId uniqueidentifier

				SqlParameter logIdParam = new SqlParameter( "@DiscLogId" , SqlDbType.UniqueIdentifier );

				m_discCreatedDate = dCreatedDate;
				
				logIdParam.Direction = ParameterDirection.Output;

				exec.ExecuteNonQuery
					( "CreateDiscussionLog"
					, 2
					, new SqlParameter( "@DiscCreatorUserId"   , DiscCreatorUserId )
					, new SqlParameter( "@DiscRefObjId"        , DiscRefObjId )
					, new SqlParameter( "@DiscRefObjType"      , DiscRefObjType )
					, new SqlParameter( "@DiscRefObjNm1"       , DiscRefObjNm1 )
					, new SqlParameter( "@DiscRefObjNm2"       , DiscRefObjNm2 )
					, new SqlParameter( "@DiscDueDate"         , GetSaveDate( DiscDueDate ) )
					, new SqlParameter( "@DiscSubject"         , DiscSubject )
					, new SqlParameter( "@DiscPriority"        , DiscPriority )
					, new SqlParameter( "@DiscStatus"          , DiscStatus )
					, new SqlParameter( "@DiscStatusDate"      , DiscCreatedDate )
					, new SqlParameter( "@DiscCreatorLoginNm"  , DiscCreatorLoginNm )
					, new SqlParameter( "@DiscCreationDate"    , DiscCreatedDate )
					, new SqlParameter( "@IsValid"             , DiscIsValid )
					, new SqlParameter( "@BrokerId"            , BrokerID )
					, logIdParam
					);

				m_discLogId = ( Guid ) logIdParam.Value;

				if( DiscLogId == Guid.Empty )
				{
					throw new CBaseException(ErrorMessages.Generic, "Empty id returned." );
				}

				// Record the log in our file-based persistent store.  This log
				// has the opening block of a new task as well as the complete
				// record of the source task's log as an embedded entry.

				CFileDBFields fileDbField = new CFileDBFields( DiscLogId );

				if( bAutoGenerateHistory == true )
				{
					fileDbField.Save( "DiscussionLog" , CreateHistoryForFirstTime( userLoginNm , DiscHistory ) );
				}
				else
				{
					fileDbField.Save( "DiscussionLog" , DiscHistory );
				}
			}
			catch( Exception e )
			{
				throw new CBaseException(ErrorMessages.Generic, "Failed to create new task with embedded history (" + e.Message + ")." );
			}
		}

		private void ReplicateNotifs(  CStoredProcedureExec exec , Guid sourceId , DateTime dReplicatedDate )
		{
			try
			{
				// Duplicate any active participants as well.  For now, we
				// create vanilla notifications.  Each one just references
				// the user without the personal alert dates and such.

				DataSet ds = new DataSet();
						
				exec.Fill( ds , "ListDiscParticipantsByDiscId"
					, new SqlParameter( "@DiscLogId" , sourceId )
					, new SqlParameter( "@IsValid"   , true     )
					);

				foreach( DataRow row in ds.Tables[ 0 ].Rows )
				{
					exec.ExecuteNonQuery
						( "CreateDiscNotif"
						, 2
						, new SqlParameter( "@NotifReceiverUserId"  , row[ "NotifReceiverUserId"  ] )
						, new SqlParameter( "@NotifReceiverLoginNm" , row[ "NotifReceiverLoginNm" ] )
						, new SqlParameter( "@NotifId"              , Guid.NewGuid()                )
						, new SqlParameter( "@DiscLogId"            , DiscLogId                     )
						, new SqlParameter( "@NotifBrokerId"        , BrokerID                      )
						, new SqlParameter( "@NotifUpdatedDate"     , dReplicatedDate               )
						, new SqlParameter( "@NotifIsRead"          , false                         )
						, new SqlParameter( "@NotifIsValid"         , true                          )
						);
				}
			}
			catch( Exception e )
			{
				throw new CBaseException(ErrorMessages.Generic, "Failed to replicate task notifications (" + e.Message + ")." );
			}
		}

		private void UpdateNotif( CStoredProcedureExec exec , DateTime dUpdatedDate )
    	{
            m_notifUpdatedDate = dUpdatedDate;

			exec.ExecuteNonQuery
				( "UpdateDiscNotif"
				, 2
				, new SqlParameter( "@NotifId", NotifId )
				, new SqlParameter( "@NotifAlertDate", GetSaveDate( NotifAlertDate ) )
				, new SqlParameter( "@NotifLastReadDate", GetSaveDate( NotifLastReadDate ) )
				, new SqlParameter( "@NotifIsRead", NotifIsRead )
				, new SqlParameter( "@NotifIsValid", NotifIsValid )
				, new SqlParameter( "@NotifInvalidDate", GetSaveDate( NotifInvalidDate ) )
				, new SqlParameter( "@NotifUpdatedDate" , NotifUpdatedDate )
				);
		}	

		private void CreateNotif( CStoredProcedureExec exec , DateTime dUpdatedDate )
		{
			/*
			CREATE PROCEDURE CreateDiscNotif 
				@NotifId uniqueidentifier,
				@NotifAlertDate datetime,
				@NotifUpdatedDate datetime,
				@NotifIsRead bit,
				@DiscLogId uniqueidentifier,
				@NotifReceiverUserId uniqueidentifier,
				@NotifReceiverLoginNm varchar(36),
				@NotifBrokerId uniqueidentifier,
				@NotifIsValid bit,
				@NotifInvalidDate datetime
			*/
			NotifId_ = Guid.NewGuid();

            m_notifUpdatedDate  = dUpdatedDate;
			m_notifLastReadDate = dUpdatedDate;

			exec.ExecuteNonQuery
				( "CreateDiscNotif"
				, 2
				, new SqlParameter( "@NotifId", NotifId )
				, new SqlParameter( "@NotifAlertDate", GetSaveDate( NotifAlertDate ) )
				, new SqlParameter( "@NotifIsRead", NotifIsRead )
				, new SqlParameter( "@DiscLogId", DiscLogId )
				, new SqlParameter( "@NotifReceiverUserId",NotifReceiverUserId )
				, new SqlParameter( "@NotifReceiverLoginNm", NotifReceiverLoginNm )
				, new SqlParameter( "@NotifBrokerId", BrokerID )
				, new SqlParameter( "@NotifIsValid", NotifIsValid )
				, new SqlParameter( "@NotifInvalidDate", GetSaveDate( NotifInvalidDate ) )
				, new SqlParameter( "@NotifUpdatedDate", NotifUpdatedDate )
				, new SqlParameter( "@NotifLastReadD" , NotifLastReadDate )
				);																					
		}
	
		private void UpdateDisc( CStoredProcedureExec exec , string userDisplayName , bool bAutoGenerateHistory , DateTime dUpdatedDate )
		{
			/*
			CREATE PROCEDURE UpdateDiscussionLog
				@DiscLogId uniqueidentifier,
				@DiscRefObjId uniqueidentifier='00000000-0000-0000-0000-000000000000',
				@DiscRefObjType [varchar] (36),
				@DiscRefObjNm1 [varchar] (36),
				@DiscRefObjNm2 [varchar] (100),
				@DiscDueDate [datetime] ,
				@DiscSubject [varchar] (100),
				@DiscPriority [int],
				@DiscStatus [int],
				@DiscIsRefObjValid [bit]
				*/

			if( m_isUpdated == true )
			{
				m_discLastModifyDate = dUpdatedDate;
			}

			exec.ExecuteNonQuery
				( "UpdateDiscussionLog"
				, 2
				, new SqlParameter( "@DiscLogId", DiscLogId )
				, new SqlParameter( "@DiscRefObjId", DiscRefObjId )
				, new SqlParameter( "@DiscRefObjType", DiscRefObjType )
				, new SqlParameter( "@DiscRefObjNm1", DiscRefObjNm1 )
				, new SqlParameter( "@DiscRefObjNm2", DiscRefObjNm2 )
				, new SqlParameter( "@DiscDueDate", CDiscNotif.GetSaveDate( DiscDueDate ) )
				, new SqlParameter( "@DiscSubject", DiscSubject )
				, new SqlParameter( "@DiscPriority", DiscPriority )
				, new SqlParameter( "@DiscStatus", DiscStatus )
				, new SqlParameter( "@DiscStatusDate", DiscStatusDate )
				, new SqlParameter( "@DiscIsRefObjValid", DiscIsRefObjValid )
				, new SqlParameter( "@DiscLastModifierLoginNm", DiscLastModifierLoginNm )
				, new SqlParameter( "@DiscLastModifierUserId", DiscLastModifierUserId )
				, new SqlParameter( "@DiscLastModifyDate" , DiscLastModifyDate )
				, new SqlParameter( "@IsValid" , DiscIsValid )
				);

			try
			{
				// 3/7/2005 kb - More and more users will be using tasks and
				// updating the notes.  We could have a situation where 2 users
				// edit and save at the same time (both appending their portion
				// to the master notes text).  We need to be robust and retry,
				// and not lose the competing saved data.
				//
				// 3/7/2005 kb - We will implement locking at the db level next
				// week.  Expect each read to lock the task.  The write will
				// unlock.  Every colliding write will fail (that means, every
				// editor read with the intent to write should fail).

				CFileDBFields fileDbField = new CFileDBFields( m_discLogId );

				if( HasAdditionalHistory == true && bAutoGenerateHistory == true )
				{
					fileDbField.Save( "DiscussionLog", GetNewHistory( userDisplayName ) );
				}
				else
				{
					fileDbField.Save( "DiscussionLog", DiscHistory );
				}
			}
			catch
			{
				throw;
			}
		}

		/// <summary>
		/// Save the discussion log details as a new task and duplicate
		/// the notification structure currently in place for this task.
		/// The log is constructed as new by placing a header block and
		/// then embedding the previous history within.
		/// </summary>
		/// <param name="userLoginNm">
		/// Login id of the editing user.
		/// </param>
		/// <param name="dSavedTime">
		/// Externally generated timestamp for all update dates
		/// that require an action stamp.  We use one date for
		/// all so that lags in the processing of a request
		/// don't create inconsistencies in dashboard event
		/// processing.  The point at which the user's request
		/// was received by the server is the instant at which
		/// all these saving actions took place (though, now,
		/// it is well into the future by at least 500 nanoseconds).
		/// </param>
		/// <returns>
		/// True if successful.
		/// </returns>

		public bool Duplicate( String userDisplayName , Boolean bAutoGenerateHistory , DateTime dSavedTime )
		{	
			// Take the currently retrieved or initialized task and make
			// a new one using the user's credentials.  We take the existing
			// history and add it to the new one as an embedded log.
			//
			// 10/5/2004 kb - To accommodate conditions as tasks, we need
			// to make history handling optional.  Conditions don't get
			// the formatted, change-based log that tasks get.

			try
			{
				using( CStoredProcedureExec spExec = new CStoredProcedureExec(this.BrokerID) )
				{
					// Attempt the save after setting up the transaction.

					try
					{
						Guid sourceId = DiscLogId;

						spExec.BeginTransactionForWrite();

						try
						{
							CreateDiscAndEmbedLog( spExec , userDisplayName , bAutoGenerateHistory , dSavedTime );
						}
						catch( Exception e )
						{
							throw new TaskException( this , "Failed to create disc and embed log during duplicate" , e );
						}

						try
						{
							ReplicateNotifs( spExec , sourceId , dSavedTime );
						}
						catch( Exception e )
						{
							throw new TaskException( this , "Failed to replicate notifs during duplicate" , e );
						}

						spExec.CommitTransaction();

						return true;
					}
					catch
					{
						// Oops!

						spExec.RollbackTransaction();

						throw;
					}
				}
			}
			catch( TaskException e ) 
			{
				// Oops!

				Tools.LogErrorWithCriticalTracking( e );
			}
			catch( Exception e ) 
			{
				// Oops!

				Tools.LogErrorWithCriticalTracking( e );
			}

			// We landed here because an exception was thrown.

			return false;
		}

		/// <summary>
		/// Save the discussion log details and the invoking user's
		/// notification details.  Active participation information
		/// is saved elsewhere, in a separate transaction.
		/// </summary>
		/// <param name="userDisplayName">
		/// Full name of the editing user.
		/// </param>
		/// <param name="bAutoGenerateHistory">
		/// Should we auto-generate the standard task history?
		/// </param>
		/// <param name="bSaveNotif">
		/// Create new or update existing notification for user?
		/// </param>
		/// <param name="bAcknowledge">
		/// Do we want to acknowledge this task and so take it out
		/// of the dashboard's calculation set?
		/// </param>
		/// <param name="bKeepMonitor">
		/// Do we want to force this task back into the dashboard's
		/// calculation set?
		/// </param>
		/// <param name="bSetLastRead">
		/// Should we mark this save as the point of last read?
		/// </param>
		/// <param name="dSavedTime">
		/// Externally generated timestamp for all update dates
		/// that require an action stamp.  We use one date for
		/// all so that lags in the processing of a request
		/// don't create inconsistencies in dashboard event
		/// processing.  The point at which the user's request
		/// was received by the server is the instant at which
		/// all these saving actions took place (though, now,
		/// it is well into the future by at least 500 nanoseconds).
		/// </param>
		/// <param name="spExec">
		/// Transactional execution facilitator.
		/// </param>

		private void Save( String userDisplayName , Boolean bAutoGenerateHistory , Boolean bSaveNotif , Boolean bAcknowledge , Boolean bKeepMonitor , Boolean bSetLastRead , DateTime dSavedTime , CStoredProcedureExec spExec )
		{	
			// Save the discussion log details and the invoking user's notification
			// details.  Active participation information is saved elsewhere, in a
			// separate transaction.

			try
			{
				// Assume the transaction is open for writing.

				if( Guid.Empty == DiscLogId )
					CreateDisc( spExec, userDisplayName , bAutoGenerateHistory , dSavedTime );
				else
					UpdateDisc( spExec, userDisplayName , bAutoGenerateHistory , dSavedTime );

				if( bSaveNotif == true )
				{
					// 9/30/2004 kb - We make saving the notification optional
					// because conditions don't need notifications at this
					// time.  Notif id should stay empty if already empty.

					if( Guid.Empty == NotifId )
						CreateNotif( spExec , dSavedTime );
					else
						UpdateNotif( spExec , dSavedTime );
				}

				if( bSetLastRead == true )
				{
					// Sometimes, we want to track the last time a notify
					// was read by the user.  The dashboard lights may key
					// off of the last read and determine that certain
					// events were implicitly acknowledged.

					SqlParameter[] pa = new SqlParameter[ 2 ];

					pa[ 0 ] = new SqlParameter( "@NotifId"      , m_notifId  );
					pa[ 1 ] = new SqlParameter( "@LastReadDate" , dSavedTime );

					spExec.ExecuteNonQuery
						( "UpdateDiscNotificationLastRead"
						, 2
						, pa
						);

					m_notifLastReadDate = dSavedTime;
				}

				if( bKeepMonitor == true )
				{
					// Clear out the latest acknowledgement date for this
					// notification so that any event associated with this
					// notify will appear new and to be considered in the
					// user's dashboard.

					SqlParameter[] pa = new SqlParameter[ 2 ];

					pa[ 0 ] = new SqlParameter( "@NotifId" , m_notifId             );
					pa[ 1 ] = new SqlParameter( "@UserId"  , m_notifReceiverUserId );

					spExec.ExecuteNonQuery
						( "UpdateDiscNotificationAcks"
						, 2
						, pa
						);

					m_notifAckDate = DateTime.MinValue;
				}
				else
				if( bAcknowledge == true )
				{
					// Mark this notification as acknowledged by updating
					// the notif's acknowledged date to now.

					SqlParameter[] pa = new SqlParameter[ 3 ];

					pa[ 0 ] = new SqlParameter( "@NotifId" , m_notifId             );
					pa[ 1 ] = new SqlParameter( "@UserId"  , m_notifReceiverUserId );
					pa[ 2 ] = new SqlParameter( "@AckDate" , dSavedTime            );

					spExec.ExecuteNonQuery
						( "UpdateDiscNotificationAcks"
						, 2
						, pa
						);

					m_notifAckDate = dSavedTime;
				}
			}
			catch( Exception e ) 
			{
				// Oops!

                throw new TaskException( this , "Failed to save." , e );
			}
		}

		/// <summary>
		/// Save the discussion log details and the invoking user's
		/// notification details.  Active participation information
		/// is saved elsewhere, in a separate transaction.
		/// </summary>
		/// <param name="userDisplayName">
		/// Full name of the editing user.
		/// </param>
		/// <param name="bSaveNotif">
		/// Create new or update existing notification for user?
		/// </param>
		/// <param name="bAcknowledge">
		/// Do we want to acknowledge this task and so take it out
		/// of the dashboard's calculation set?
		/// </param>
		/// <param name="bKeepMonitor">
		/// Do we want to force this task back into the dashboard's
		/// calculation set?
		/// </param>
		/// <param name="bSetLastRead">
		/// Should we mark this save as the point of last read?
		/// </param>
		/// <param name="dSavedTime">
		/// Externally generated timestamp for all update dates
		/// that require an action stamp.  We use one date for
		/// all so that lags in the processing of a request
		/// don't create inconsistencies in dashboard event
		/// processing.  The point at which the user's request
		/// was received by the server is the instant at which
		/// all these saving actions took place (though, now,
		/// it is well into the future by at least 500 nanoseconds).
		/// </param>
		/// <returns>
		/// True if successful.
		/// </returns>

		private bool Save( String userDisplayName , Boolean bSaveNotif , Boolean bAcknowledge , Boolean bKeepMonitor , Boolean bSetLastRead , DateTime dSavedTime )
		{
			// Save the discussion log details and the invoking user's notification
			// details.  Active participation information is saved elsewhere, in a
			// separate transaction.

			using( CStoredProcedureExec spExec = new CStoredProcedureExec(this.BrokerID) )
			{
				try
				{
					// Attempt the save after setting up the transaction.

					try
					{
						spExec.BeginTransactionForWrite();

						Save( userDisplayName , true , bSaveNotif , bAcknowledge , bKeepMonitor , bSetLastRead , dSavedTime , spExec );

						spExec.CommitTransaction();

						return true;
					}
					catch
					{
						spExec.RollbackTransaction();

						throw;
					}
				}
				catch( TaskException e )
				{
					// Oops!

					Tools.LogErrorWithCriticalTracking( e );
				}
				catch( Exception e )
				{
					// Oops!

					Tools.LogErrorWithCriticalTracking( e );
				}
			}

			// We landed here because an exception was thrown.

			return false;
		}

		/// <summary>
		/// Save the discussion log details and the invoking user's
		/// notification details.  Active participation information
		/// is saved elsewhere, in a separate transaction.
		/// </summary>
		/// <param name="userDisplayName">
		/// Full name of the editing user.
		/// </param>
		/// <param name="bAcknowledge">
		/// Do we want to acknowledge this task and so take it out
		/// of the dashboard's calculation set?
		/// </param>
		/// <param name="bKeepMonitor">
		/// Do we want to force this task back into the dashboard's
		/// calculation set?
		/// </param>
		/// <param name="bSetLastRead">
		/// Should we mark this save as the point of last read?
		/// </param>
		/// <param name="dSavedTime">
		/// Externally generated timestamp for all update dates
		/// that require an action stamp.  We use one date for
		/// all so that lags in the processing of a request
		/// don't create inconsistencies in dashboard event
		/// processing.  The point at which the user's request
		/// was received by the server is the instant at which
		/// all these saving actions took place (though, now,
		/// it is well into the future by at least 500 nanoseconds).
		/// </param>
		/// <returns>
		/// True if successful.
		/// </returns>

		public bool Save( String userDisplayName , Boolean bAcknowledge , Boolean bKeepMonitor , Boolean bSetLastRead , DateTime dSavedTime )
		{
			// Delegate to detailed implementation.

			return Save( userDisplayName , true , bAcknowledge , bKeepMonitor , bSetLastRead , dSavedTime );
		}

		/// <summary>
		/// Calculate the delta between the current participant state
		/// of this discussion log, and the desired state, with respect
		/// to the invoking user, and make the changes in the database.
		/// Anyone not listed in the active set, yet currently active
		/// according to the database is dropped.
		/// </summary>
		/// <param name="userLoginNm">
		/// Login id of the editing user.
		/// </param>
		/// <param name="activeParticipants">
		/// Desired participant and tracking state according to the
		/// user's edits on save.
		/// </param>
		/// <returns>
		/// True if successful.
		/// </returns>

		public bool Join( Guid editorUserId , ICollection activeParticipants )
		{
			DateTime timeStamp = DateTime.Now;

			try
			{
				if( activeParticipants == null )
				{
					throw new ArgumentNullException( "activeParticipants" , "Null participant set not allowed." );
				}

				using( CStoredProcedureExec exec = new CStoredProcedureExec(this.BrokerID) )
				{
					try
					{
						// Start transaction and pull out the current database
						// state regarding who is participating (active and
						// past) and who is being tracked.

						DataSet ts = new DataSet();
						DataSet ps = new DataSet();

						exec.BeginTransactionForWrite();

						exec.Fill( ps , "ListDiscParticipantsByDiscId"
							, new SqlParameter( "@DiscLogId" , DiscLogId )
							);

						exec.Fill( ts , "ListDiscTracks"
							, new SqlParameter( "@DiscLogId"     , DiscLogId    )
							, new SqlParameter( "@CreatorUserId" , editorUserId )
							);

						// Build up a list of current notifications and tracking
						// details so we can use a more manageable description
						// of the current state of the database.

						TrackTable track = new TrackTable();
						NotifTable notif = new NotifTable();

						foreach( DataRow row in ts.Tables[ 0 ].Rows )
						{
							track.Add( row );
						}

						foreach( DataRow row in ps.Tables[ 0 ].Rows )
						{
							notif.Add( row );
						}

						// Perform the merge using the gathered lists.  Each
						// entry in the current task, as edited by the user,
						// is maintained, unless it was added in this session.
						// If added, we create it now.  If removed, we disable
						// it by removal.

						try
						{
							Guid notifId;

							foreach( ParticipantEntry entry in activeParticipants )
							{
								NotifInfo info = notif[ entry.UserId ];
								TrackInfo trkd = track[ entry.UserId ];

								notifId = Guid.NewGuid();

								if( info != null )
								{
									if( info.IsValid == false )
									{
										// Reset this notification to be valid.  We are
										// essentially making a previously active user
										// now active again.

										exec.ExecuteNonQuery
											( "UpdateDiscNotifToNotify"
											, 2
											, new SqlParameter( "@NotifId"          , info.NotifId )
											, new SqlParameter( "@NotifUpdatedDate" , timeStamp    )
											);
									}

									notifId = info.NotifId;

									info.Status = true;
								}
								else
								{
									// We don't currently have an existing notification
									// to this user, so let's create one.
									//
									// 12/8/2004 kb - Caching the login name in the task
									// notify works well, though we don't use it anymore
									// and the login name can change.  I actually think
									// we don't need this parameter anymore.

									exec.ExecuteNonQuery
										( "CreateDiscNotif"
										, 2
										, new SqlParameter( "@DiscLogId"           , DiscLogId    )
										, new SqlParameter( "@NotifBrokerId"       , BrokerID     )
										, new SqlParameter( "@NotifReceiverUserId" , entry.UserId )
										, new SqlParameter( "@NotifIsValid"        , true         )
										, new SqlParameter( "@NotifIsRead"         , false        )
										, new SqlParameter( "@NotifUpdatedDate"    , timeStamp    )
										, new SqlParameter( "@NotifId"             , notifId      )
										);
								}

								if( trkd != null )
								{
									if( trkd.IsValid == true && entry.IsTracked == false )
									{
										// Nix the tracking.  We only delete tracking for tracking
										// that was previously in place for this log.

										exec.ExecuteNonQuery
											( "DeleteDiscTrack"
											, 2
											, new SqlParameter( "@TrackId"     , trkd.TrackId )
											, new SqlParameter( "@InvalidDate" , timeStamp    )
											);
									}
									else
									{
										if( trkd.IsValid == false && entry.IsTracked == true )
										{
											// This active participant is being tracked, so let's start
											// the tracking.  The sproc handles the pre-existing tracking
											// row and making a new one (if needed).

											exec.ExecuteNonQuery
												( "StartDiscTrack"
												, 2
												, new SqlParameter( "@TrackBrokerId"      , BrokerID     )
												, new SqlParameter( "@TrackCreatorUserId" , editorUserId )
												, new SqlParameter( "@NotifId"            , notifId      )
												);
										}
									}
								}
								else
								{
									if( entry.IsTracked == true )
									{
										// This active participant is being tracked, so let's start
										// the tracking.  We make a new tracking row in this case.

										exec.ExecuteNonQuery
											( "StartDiscTrack"
											, 2
											, new SqlParameter( "@TrackBrokerId"      , BrokerID     )
											, new SqlParameter( "@TrackCreatorUserId" , editorUserId )
											, new SqlParameter( "@NotifId"            , notifId      )
											);
									}
								}
							}
						}
						catch( Exception e )
						{
							// Oops!

							throw new TaskException( this , "Failed to add during joining" , e );
						}

						// Now, any participant that wasn't listed in the active
						// set, but is currently active needs to be removed
						// because the user has nixed them.

						try
						{
							foreach( NotifInfo info in notif )
							{
								if( info.Status == false && info.IsValid == true )
								{
									// The notification was removed from the client's
									// view of who is active, so let's sync up by
									// nixing this instance.

									SqlParameter taskId = new SqlParameter( "@DiscLogId"           , Guid.Empty );
									SqlParameter userId = new SqlParameter( "@NotifReceiverUserId" , Guid.Empty );

									taskId.Direction = ParameterDirection.Output;
									userId.Direction = ParameterDirection.Output;

									exec.ExecuteNonQuery
										( "DeleteDiscNotif"
										, 2
										, new SqlParameter( "@NotifId"     , info.NotifId )
										, new SqlParameter( "@InvalidDate" , timeStamp    )
										, taskId
										, userId
										);

									// We force removal of any tracking that may be
									// associated with this notification.

									exec.ExecuteNonQuery
										( "DeleteDiscTrack"
										, 2
										, new SqlParameter( "@NotifId"       , info.NotifId  )
										, new SqlParameter( "@CreatorUserId" , editorUserId  )
										, new SqlParameter( "@InvalidDate"   , timeStamp     )
										);
								}
							}
						}
						catch( Exception e )
						{
							// Oops!

							throw new TaskException( this , "Failed to nix during joining" , e );
						}

						// Commit what has been updated or created.

						exec.CommitTransaction();
					}
					catch
					{
						// Oops!

						exec.RollbackTransaction();

						throw;
					}
				}

				// 4/29/2005 kb - We have the final active list (passed in)
				// and we need to refresh the participant list (especially
				// for accurate signalling after save).

				foreach( TaskParticipant tP in m_discParticipants )
				{
					tP.IsActive = false;
				}

				foreach( ParticipantEntry pE in activeParticipants )
				{
					Boolean foundIt = false;

					foreach( TaskParticipant tP in m_discParticipants )
					{
						if( tP.UserId == pE.UserId )
						{
							tP.IsActive = true;

							foundIt = true;

							break;
						}
					}

					if( foundIt == false )
					{
						TaskParticipant tP = new TaskParticipant();

						tP.UserId   = pE.UserId;
						tP.IsActive = true;

						m_discParticipants.Add( tP );
					}
				}

				return true;
			}
			catch( TaskException e )
			{
				// Oops!

				Tools.LogErrorWithCriticalTracking( e );
			}
			catch( Exception e )
			{
				// Oops!

				Tools.LogErrorWithCriticalTracking( e );
			}

			// We landed here because an exception was thrown.

			return false;
		}

		/// <summary>
		/// Once all saving has occurred, the saving function should
		/// notify all listening users.  Only do this on successful
		/// saving of a task.  We read from the db to get the list
		/// of participants, so don't call this within a high level
		/// transaction that is writing to the task tables.
		/// </summary>

		public void Signal( String userDisplayName , DateTime dEventTime )
		{
			// 2/15/2005 kb - Now notify the background processor that
			// a task has been updated.  We keep track of changes to
			// tasks that are conditions associated with loans.  We
			// can expand this later.
			//
			// 4/28/2005 kb - We now write out update notifications to
			// our processor whenever any changes take place, not just
			// when conditions are marked done, per case #1654.
			//
			// 4/28/2005 kb - We eat the exception (if any) because a
			// notify should never disrupt normal operation.

            try
            {
                TaskAlertProcessor.Alert
                    (m_discLogId
                    , userDisplayName
                    , m_discStatus
                    , m_discCreatedDate
                    , m_notifAlertDate
                    , m_discDueDate
                    , m_discLastModifyDate
                    , m_notifAckDate
                    , m_discPrevious != E_DiscStatus.Done && m_discStatus == E_DiscStatus.Done
                    , false // Is Condition
                    , m_notifIsRead
                    , m_discParticipants
                    , m_discRefObjId
                    , m_notifReceiverUserId
                    , dEventTime
                    );
            }
            catch (Exception e)
            {
                // Oops!  Keep trucking!

                Tools.LogError("Failed to tell processor of update of condition.", e);
            }
		}

		/// <summary>
		/// Passing Guid.Empty if you don't know discLogId
		/// </summary>
		public CDiscNotif( Guid brokerId, Guid notifId, Guid discLogId )
		{
            this.BrokerID = brokerId;
			m_notifId = notifId;
			m_discLogId = discLogId;
		}

		public static void		Bind_DiscStatus(System.Web.UI.WebControls.DropDownList ddl) 
		{
			ddl.Items.Add( Tools.CreateEnumListItem( "Active",		E_DiscStatus.Active ));
			ddl.Items.Add( Tools.CreateEnumListItem( "Done",		E_DiscStatus.Done ));
			ddl.Items.Add( Tools.CreateEnumListItem( "Suspended",	E_DiscStatus.Suspended ));
			ddl.Items.Add( Tools.CreateEnumListItem( "Cancelled",	E_DiscStatus.Cancelled ));
		}

		public static void		Bind_DiscPriority( System.Web.UI.WebControls.DropDownList ddl )
		{
			ddl.Items.Add( Tools.CreateEnumListItem( "High",	E_DiscPriority.High ) );
			ddl.Items.Add( Tools.CreateEnumListItem( "Normal",	E_DiscPriority.Normal ) );
			ddl.Items.Add( Tools.CreateEnumListItem( "Low",		E_DiscPriority.Low ) );
		}

		private void LoadDiscFields( DbDataReader reader )
		{
			m_discLogId				  = (Guid)	reader["DiscLogId"];
			m_discCreatorUserId		  = (Guid)	reader["DiscCreatorUserId"];
			m_discRefObjId			  = (Guid)	reader["DiscRefObjId"];
			m_discRefObjType		  = (string) reader["DiscRefObjType"];
			m_discRefObjNm1			  = (string) reader["DiscRefObjNm1"];
			m_discRefObjNm2			  = (string) reader["DiscRefObjNm2"];
			m_discCreatedDate		  = GetDate( reader["DiscCreatedDate"] );
			m_discDueDate			  = GetDate( reader["DiscDueDate"] ); 
			m_discSubject			  = (string) reader["DiscSubject"]; 
			m_discPriority			  = (E_DiscPriority) reader["DiscPriority"]; 
			m_discPrevious            = (E_DiscStatus) reader["DiscStatus"];
			m_discStatus			  = (E_DiscStatus) reader["DiscStatus"];
			m_discStatusDate		  = (DateTime) reader["DiscStatusDate"];
			m_discIsRefObjValid		  = (bool) reader["DiscIsRefObjValid"];
			m_discCreatorLoginNm	  = (string) reader["DiscCreatorLoginNm"];
			m_discLastModifyDate      = (DateTime) reader["DiscLastModifyD"];
			m_discLastModifierUserId  = (Guid) reader["DiscLastModifierUserId"];
			m_discLastModifierLoginNm = (string) reader["DiscLastModifierLoginNm"];
			m_discIsValid             = (bool) reader["IsValid"];

			// Both
			this.BrokerID				= (Guid) reader["DiscBrokerId"];

			CFileDBFields			fileDbField = new CFileDBFields( m_discLogId );
			m_discHistory = fileDbField.Load( "DiscussionLog" );
		}

		private void LoadNotifFields( DbDataReader reader )
		{
			m_notifAlertDate       = GetDate( reader["NotifAlertDate"] );
			m_notifUpdatedDate     = GetDate( reader["NotifUpdatedDate"] );
			m_notifLastReadDate    = GetDate( reader["NotifLastReadD"] );
			m_notifAckDate         = GetDate( reader["NotifAckD"] );
			m_notifIsRead          = (Boolean) reader["NotifIsRead"];
			m_notifReceiverUserId  = (Guid) reader["NotifReceiverUserId"];
			m_notifReceiverLoginNm = (String) reader["NotifReceiverLoginNm"];
			m_notifIsValid         = (Boolean) reader["NotifIsValid"];
			m_notifInvalidDate     = GetDate( reader["NotifInvalidDate"] );
		}

		public Boolean Retrieve( Boolean markNotifAsRead , Boolean loadParticipants )
		{
			m_isRetrieved = false;

			try
			{
				if( m_notifId == Guid.Empty )
				{
                    SqlParameter[] parameters = {
                                                    new SqlParameter( "@DiscLogId" , m_discLogId )
                                                };

					using( DbDataReader sR = StoredProcedureHelper.ExecuteReader( this.BrokerID, "RetrieveDiscByDiscId", parameters ) )
					{
						if( sR.Read() )
						{
							try
							{
								LoadDiscFields( sR );
							}
							catch( Exception e )
							{
								throw new TaskException( this , "Failed to load disc fields" , e );
							}
						}
					}
				}
				else
				{
                    SqlParameter[] parameters = {
                                                    new SqlParameter( "@NotifId" , m_notifId ), 
                                                    new SqlParameter("@MarkAsRead", markNotifAsRead )
                                                };
					using( DbDataReader sR = StoredProcedureHelper.ExecuteReader(this.BrokerID, "RetrieveDiscNotifByNotifId", parameters))
					{
						if( sR.Read() )
						{
							// Notif specific

							try
							{
								LoadNotifFields( sR );
							}
							catch( Exception e )
							{
								throw new TaskException( this , "Failed to load notif fields" , e );
							}

							// Discussion log

							try
							{
								LoadDiscFields( sR );
							}
							catch( Exception e )
							{
								throw new TaskException( this , "Failed to load disc fields" , e );
							}
						}
					}
				}

				m_discParticipants.Clear();

				if( loadParticipants == true )
				{
                    SqlParameter[] parameters = {
                                                    new SqlParameter( "@DiscLogId", DiscLogId )
                                                };

					using( DbDataReader sR = StoredProcedureHelper.ExecuteReader(this.BrokerID, "ListDiscParticipantsByDiscId", parameters ) )
					{
						while( sR.Read() == true )
						{
							TaskParticipant tP = new TaskParticipant();

							tP.UserId   = ( Guid    ) sR[ "NotifReceiverUserId" ];
							tP.IsActive = ( Boolean ) sR[ "NotifIsValid"        ];

							m_discParticipants.Add( tP );
						}
					}
				}
			}
			catch( Exception e )
			{
				// Oops!

				Tools.LogErrorWithCriticalTracking( e );

				return false;
			}

			m_isRetrieved = true;

			return true;
		}

		private bool HasAdditionalHistory
		{
			get
			{
				return m_addHistory.Length > 0;
			}
		}

		private string GetNewHistory( string userDisplayName )
		{
			if( m_addHistory.Length > 0 )
			{
				m_addHistory.Insert( 0, "\n----- " + DateTime.Now.ToString() + " By " + userDisplayName + " -----" );
				if( DiscLogId != Guid.Empty )
					m_addHistory.Insert( 0, "\n" );
				m_addHistory.Insert( 0, DiscHistory );
				return m_addHistory.ToString();
			}
			return "";
		}

		private string CreateHistoryForFirstTime( string userDisplayName , string embeddedHistory )
		{
			StringBuilder strBuilder = new StringBuilder( 200 );
			strBuilder.Insert( 0, "\n----- " + DateTime.Now.ToString() + " By " + userDisplayName + " -----" );
			if( "" != m_discSubject	)
				strBuilder.Append( "\nSUBJECT:   " + DiscSubject );
			if( "" != DiscRefObjNm1 )
				strBuilder.Append( "\nLOAN FILE:  " + DiscRefObjNm1 );
			if( "" != DiscRefObjNm2 )
				strBuilder.Append( "\nBORROWER NAME: " + DiscRefObjNm2 );
			if( DateTime.MinValue != m_discDueDate )
				strBuilder.Append( "\nDUE DATE:   " + m_discDueDate.ToString() );
			strBuilder.Append( "\nPRIORITY:     " + CDiscUIBase.GetDiscPriorityDesc( DiscPriority ) );
			strBuilder.Append( "\nDISCUSSION STATUS:  "  + m_discStatus.ToString() );
			strBuilder.Append( "\nMESSAGE: \n" + NewMsg_ );
			strBuilder.Append( "\nTASK LOG FROM TEMPLATE: " + embeddedHistory.Replace( "\n" , "\n--* " ) );
			strBuilder.Append( "\nEND OF TEMPLATE LOG. \n" );
			return strBuilder.ToString();
		}

		private string CreateHistoryForFirstTime( string userDisplayName )
		{
			StringBuilder strBuilder = new StringBuilder( 200 );
			strBuilder.Insert( 0, "\n----- " + DateTime.Now.ToString() + " By " + userDisplayName + " -----" );
			if( "" != m_discSubject	)
				strBuilder.Append( "\nSUBJECT:   " + DiscSubject );
			if( "" != DiscRefObjNm1 )
				strBuilder.Append( "\nLOAN FILE:  " + DiscRefObjNm1 );
			if( "" != DiscRefObjNm2 )
				strBuilder.Append( "\nBORROWER NAME: " + DiscRefObjNm2 );
			if( DateTime.MinValue != m_discDueDate )
				strBuilder.Append( "\nDUE DATE:   " + m_discDueDate.ToString() );
			strBuilder.Append( "\nPRIORITY:     " + CDiscUIBase.GetDiscPriorityDesc( DiscPriority ) );
			strBuilder.Append( "\nDISCUSSION STATUS:  "  + m_discStatus.ToString() );
			strBuilder.Append( "\nMESSAGE: \n" + NewMsg_ );
			return strBuilder.ToString();
		}
		
	}

	public class CPersonInfo
	{
		private string m_userEnteredLoginNm = "";
		private string m_dbLoginNm = "";
		private Guid m_existingNotifId = Guid.Empty;
		private Guid m_branchId = Guid.Empty;
		private Guid   m_userId = Guid.Empty;

		public CPersonInfo( string userEnteredLoginNm )
		{
			m_userEnteredLoginNm = userEnteredLoginNm;
		}

		/// <summary>
		/// LoginNm version that user entered it in. Part of handling mixed case.
		/// </summary>
		public string UserEnteredLoginNm 
		{
			get { return m_userEnteredLoginNm; }
			set { m_userEnteredLoginNm = value; }
		}

		/// <summary>
		/// LoginNm version stored in db.
		/// </summary>
		public string DBLoginNm
		{
			get { return m_dbLoginNm; }
			set { m_dbLoginNm = value; }
		}

		/// <summary>
		/// NotifId of user's existing notification for the discussion.
		/// </summary>
		public Guid ExistingNotifId
		{
			get { return m_existingNotifId; }
			set { m_existingNotifId = value; }
		}

		public Guid BranchId
		{
			get{ return m_branchId;  }
			set{ m_branchId = value; }
		}

		public Guid UserId
		{
			get{ return m_userId;  }
			set{ m_userId = value; }
		}
	}

	/// <summary>
	/// Trap simple participant details.  Login must
	/// be unique.
	/// </summary>

	public class ParticipantEntry
	{
		/// <summary>
		/// Trap simple participant details.  Login must
		/// be unique.
		/// </summary>

		private String  m_FirstName;
		private String   m_LastName;
		private Guid       m_UserId;
		private Boolean m_IsTracked;

		public String FirstName
		{
			// Access member.

			set
			{
				m_FirstName = value;
			}
			get
			{
				return m_FirstName;
			}
		}

		public String LastName
		{
			// Access member.

			set
			{
				m_LastName = value;
			}
			get
			{
				return m_LastName;
			}
		}

		public Guid UserId
		{
			// Access member.

			set
			{
				m_UserId = value;
			}
			get
			{
				return m_UserId;
			}
		}

		public Boolean IsTracked
		{
			// Access member.

			set
			{
				m_IsTracked = value;
			}
			get
			{
				return m_IsTracked;
			}
		}

		#region ( Constructors )

		/// <summary>
		/// Construct particpant descriptor.
		/// </summary>
		/// <param name="sFirstName">
		/// Name of user.
		/// </param>
		/// <param name="sLastName">
		/// Name of user.
		/// </param>
		/// <param name="sUserId">
		/// User's unique id.
		/// </param>
		/// <param name="bTracked">
		/// Is the participant tracked?
		/// </param>

		public ParticipantEntry( String sFirstName , String sLastName , Guid userId , Boolean bTracked )
		{
			// Initialize members.

			m_FirstName = sFirstName;
			m_LastName  = sFirstName;
			m_UserId    = userId;

			m_IsTracked = bTracked;
		}

		/// <summary>
		/// Construct default.
		/// </summary>

		public ParticipantEntry()
		{
			// Initialize members.

			m_FirstName = "";
			m_LastName  = "";

			m_UserId = Guid.Empty;

			m_IsTracked = false;
		}

		#endregion

	}



	/// <summary>
	/// Maintain lookup table of tracked users.
	/// </summary>

	public class TrackTable
	{
		/// <summary>
		/// Maintain lookup table of tracked users.
		/// </summary>

		private ArrayList m_List;

		public TrackInfo this[ Guid userId ]
		{
			// Access member.

			get
			{
				// Search for match and return.

				foreach( TrackInfo item in m_List )
				{
					if( item.UserId == userId )
					{
						return item;
					}
				}

				return null;
			}
		}

		/// <summary>
		/// Add row to the table.  We pull out the pertinent
		/// fields and assume they are valid.
		/// </summary>
		/// <param name="dRow">
		/// Row to append.
		/// </param>

		public void Add( DataRow dRow )
		{
			// Append to the end if unique.

			Guid key;

			if( dRow != null )
			{
				key = ( Guid ) dRow[ "NotifReceiverUserId" ];
			}
			else
			{
				return;
			}

			foreach( TrackInfo item in m_List )
			{
				if( item.UserId == key )
				{
					return;
				}
			}

			m_List.Add( new TrackInfo
				( ( Guid    ) dRow[ "NotifReceiverUserId" ]
				, ( Guid    ) dRow[ "TrackId"             ]
				, ( Guid    ) dRow[ "NotifId"             ]
				, ( Boolean ) dRow[ "TrackIsValid"        ]
				));
		}

		/// <summary>
		/// Search for a match and return.
		/// </summary>
		/// <param name="userId">
		/// Key to look for.
		/// </param>
		/// <returns>
		/// True if found.
		/// </returns>

		public bool Has( Guid userId )
		{
			// Search for a match and return.

			if( userId == Guid.Empty )
			{
				return false;
			}

			foreach( TrackInfo item in m_List )
			{
				if( item.UserId == userId )
				{
					return true;
				}
			}

			return false;
		}

		/// <summary>
		/// Search for a match and return.
		/// </summary>
		/// <param name="oUserId">
		/// Key to look for.
		/// </param>
		/// <returns>
		/// True if found.
		/// </returns>

		public bool Has( Object oUserId )
		{
			// Delegate to implementation.

			return Has( ( Guid ) oUserId );
		}

		#region ( Constuctors )

		/// <summary>
		/// Construct default.
		/// </summary>

		public TrackTable()
		{
			// Initialize members.

			m_List = new ArrayList();
		}

		#endregion

	}



	/// <summary>
	/// Maintain lookup table of notified users.
	/// </summary>

	public class NotifTable
	{
		/// <summary>
		/// Maintain lookup table of notified users.
		/// </summary>

		private ArrayList m_List;

		public NotifInfo this[ Guid userId ]
		{
			// Access member.

			get
			{
				// Search for match and return.

				foreach( NotifInfo item in m_List )
				{
					if( item.UserId == userId )
					{
						return item;
					}
				}

				return null;
			}
		}

		public IEnumerator GetEnumerator()
		{
			// Delegate to list collection.

			return m_List.GetEnumerator();
		}

		/// <summary>
		/// Add row to the table.  We pull out the pertinent
		/// fields and assume they are valid.
		/// </summary>
		/// <param name="dRow">
		/// Row to append.
		/// </param>

		public void Add( DataRow dRow )
		{
			// Append to the end if unique.

			Guid key;

			if( dRow != null )
			{
				key = ( Guid ) dRow[ "NotifReceiverUserId" ];
			}
			else
			{
				return;
			}

			foreach( NotifInfo item in m_List )
			{
				if( item.UserId == key )
				{
					return;
				}
			}

			m_List.Add( new NotifInfo
				( ( Guid    ) dRow[ "NotifReceiverUserId"  ]
				, ( Guid    ) dRow[ "NotifId"              ]
				, ( Boolean ) dRow[ "NotifIsValid"         ]
				));
		}

		/// <summary>
		/// Search for a match and return.
		/// </summary>
		/// <param name="userId">
		/// Key to look for.
		/// </param>
		/// <returns>
		/// True if found.
		/// </returns>

		public bool Has( Guid userId )
		{
			// Search for a match and return.

			if( userId == Guid.Empty )
			{
				return false;
			}

			foreach( NotifInfo item in m_List )
			{
				if( item.UserId == userId )
				{
					return true;
				}
			}

			return false;
		}

		/// <summary>
		/// Search for a match and return.
		/// </summary>
		/// <param name="oUserId">
		/// Key to look for.
		/// </param>
		/// <returns>
		/// True if found.
		/// </returns>

		public bool Has( Object oUserId )
		{
            // Delegate to implementation.

            return Has( ( Guid ) oUserId );
		}

		#region ( Constuctors )

		/// <summary>
		/// Construct default.
		/// </summary>

		public NotifTable()
		{
			// Initialize members.

			m_List = new ArrayList();
		}

		#endregion

	}


}
