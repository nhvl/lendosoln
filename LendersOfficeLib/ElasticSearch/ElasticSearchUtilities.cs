﻿namespace LendersOffice.ElasticSearch
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Utility for Elastic Search.
    /// </summary>
    public class ElasticSearchUtilities
    {
        /// <summary>
        /// Convert datetime to utc and return its represented string as  2015-01-13T08:55:56+00:00 .
        /// </summary>
        /// <param name="dt">Date time.</param>
        /// <returns>Return utc date string.</returns>
        public static string ToUtcDisplayString(DateTime dt)
        {
            DateTime utc = dt.ToUniversalTime();

            return utc.ToString("yyyy-MM-ddTHH:mm:ss.fff") + "+00:00";
        }
    }
}
