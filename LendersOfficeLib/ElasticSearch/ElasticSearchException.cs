﻿namespace LendersOffice.ElasticSearch
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Base class for exceptions thrown in Elastic Search.
    /// </summary>
    public class ElasticSearchException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ElasticSearchException"/> class.
        /// </summary>
        /// <param name="message">The error message.</param>
        public ElasticSearchException(string message) : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ElasticSearchException"/> class.
        /// </summary>
        /// <param name="message">The error message.</param>
        /// <param name="inner">Contained inner exception.</param>
        public ElasticSearchException(string message, Exception inner) : base(message, inner)
        {
        }
    }
}
