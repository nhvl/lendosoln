﻿namespace LendersOffice.ElasticSearch
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Scroll Hit Extra Information. 
    /// The callback hitProcess will receive this information.
    /// </summary>
    public interface IScrollHitExtraArgs
    {
        /// <summary>
        /// Gets Hit Sequence.
        /// </summary>
        int HitSequence { get; }

        /// <summary>
        /// Gets Total Hit.
        /// </summary>
        int TotalHit { get; }

        /// <summary>
        /// Stop receive result from Elastic Search.
        /// </summary>
        void StopProcess();
    }
}
