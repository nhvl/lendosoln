﻿namespace LendersOffice.ElasticSearch
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Exception thrown when using scroll API in ES.
    /// </summary>
    public class ScrollException : ElasticSearchException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ScrollException"/> class.
        /// </summary>
        /// <param name="message">The error message.</param>
        public ScrollException(string message) : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ScrollException"/> class.
        /// </summary>
        /// <param name="message">The error message.</param>
        /// <param name="inner">Contained inner exception.</param>
        public ScrollException(string message, Exception inner) : base(message, inner)
        {
        }
    }
}
