﻿namespace LendersOffice.ElasticSearch
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Threading.Tasks;
    using DataAccess;
    using LendersOffice.Constants;
    using Newtonsoft.Json.Linq;

    /// <summary>
    /// Simple Elastic Search Client.
    /// </summary>
    public static class SimpleElasticSearchClient
    {
        /// <summary>
        /// Scroll first page.
        /// </summary>
        /// <param name="indexName">Elastic Index.</param>
        /// <param name="indexType">Index Type.</param>
        /// <param name="queryJson">Query in json string format.</param>
        /// <param name="scrollTime">Scroll Time.</param>
        /// <returns>Returned result string from ES.</returns>
        public static string ScrollInitial(string indexName, string indexType, string queryJson, string scrollTime)
        {
            string path = string.Format("/{0}/{1}/_search?scroll={2}", indexName, indexType, scrollTime);

            HttpStatusCode statusCode = HttpStatusCode.InternalServerError;
            return CreatePostRequest(path, queryJson, out statusCode);
        }

        /// <summary>
        /// Scroll next page.
        /// </summary>
        /// <param name="scrollId">Scroll Id.</param>
        /// <param name="scrollTime">Scroll Time.</param>
        /// <returns>Return json string.</returns>
        public static string Scroll(string scrollId, string scrollTime = "5m")
        {
            string path = "/_search/scroll";
            string queryJsonTemplate = @"{ ""scroll"" : ""{@scrollTime}"", ""scroll_id"" : ""{@scrollId}"" }";

            string queryJson = queryJsonTemplate.Replace("{@scrollId}", scrollId).Replace("{@scrollTime}", scrollTime);

            HttpStatusCode statusCode = HttpStatusCode.InternalServerError;
            return CreatePostRequest(path, queryJson, out statusCode);
        }

        // the "IScanScrollOpResult value" that returns by this method associates with scroll_id. That scroll_id is valid during scrollTime.
        // Therefore we need to process the result of this method asap. Otherwise, we will have timeout issue.

        /// <summary>
        /// Using scoll API to query.
        /// </summary>
        /// <param name="indexName">Elastic Index.</param>
        /// <param name="indexType">Index Type.</param>
        /// <param name="queryJson">Query in json string format.</param>
        /// <param name="scrollTime">Scroll Time.</param>
        /// <returns>Return scroll pages results.</returns>
        public static IEnumerable<IScanScrollOpResult> ScanScroll(string indexName, string indexType, string queryJson, string scrollTime = "1m")
        {
            Stopwatch sw;
            string scrollId = string.Empty;

            bool isDone = false;
            int scollCounter = -1;
            int totalHit = 0;
            int downloadItemCount = 0;
            long downloadedDataSize = 0;
            long totalDurationInMs = 0;

            while (isDone == false)
            {
                scollCounter++;
                sw = Stopwatch.StartNew();

                string scrollResult;
                if (scollCounter == 0)
                {
                    scrollResult = SimpleElasticSearchClient.ScrollInitial(indexName, indexType, queryJson, scrollTime);
                }
                else
                {
                    scrollResult = SimpleElasticSearchClient.Scroll(scrollId, scrollTime);
                }

                if (string.IsNullOrEmpty(scrollResult))
                {
                    yield break;
                }

                JObject jobject = JObject.Parse(scrollResult);
                JToken scroll_id_token = jobject.SelectToken("_scroll_id");
                if (scroll_id_token == null)
                {
                    throw new ScrollException("ScrollError: Can not find property '_scroll_id' in the json: " + Environment.NewLine + jobject.ToString());
                }

                string nextScrollId = scroll_id_token.Value<string>();
                var jarray = jobject.SelectToken("hits.hits");
                int numItems = jarray != null ? jarray.Count() : 0;

                if (numItems == 0)
                {
                    //// SimpleElasticSearchClient.ClearScroll(nextScrollId);
                    yield break;
                }

                downloadItemCount += numItems;
                downloadedDataSize += scrollResult.Length;

                if (totalHit == 0)
                {
                    totalHit = jobject.SelectToken("hits.total").Value<int>();
                }

                sw.Stop();
                totalDurationInMs += sw.ElapsedMilliseconds;

                ScanScrollOpResultImp scanScrollOpResult = new ScanScrollOpResultImp()
                {
                    Sequence = scollCounter,
                    ResultJsonStr = scrollResult,
                    Result = jobject,
                    Hits = (JArray)jarray,
                    HitCount = numItems,
                    DurationInMs = sw.ElapsedMilliseconds,

                    TotalHit = totalHit,
                    TotalDownloadedHit = downloadItemCount,
                    DownloadedDataSize = downloadedDataSize,
                    TotalDurationInMs = totalDurationInMs,
                    ScrollId = nextScrollId
                };

                yield return scanScrollOpResult;

                if (string.IsNullOrEmpty(scanScrollOpResult.ScrollId))
                {
                    yield break;
                }

                if (downloadItemCount == totalHit)
                {
                    //// SimpleElasticSearchClient.ClearScroll(nextScrollId);
                    yield break;
                }

                scrollId = nextScrollId;
            }
        }

        /// <summary>
        /// Query Elastic Search by scroll.
        /// </summary>
        /// <param name="indexName">Elastic Index.</param>
        /// <param name="indexType">Index Type.</param>
        /// <param name="queryJson">Query in json string format.</param>
        /// <param name="hitCallback">The callback method for each hit.</param>
        /// <param name="scrollTime">Scroll Time.</param>
        /// <param name="showProgress">Show progress.</param>
        public static void ScanScroll(
                           string indexName, 
                           string indexType, 
                           string queryJson, 
                           Action<JToken /*hit*/, IScrollHitExtraArgs> hitCallback, 
                           string scrollTime = "1m", 
                           bool showProgress = false)
        {
            IEnumerable<IScanScrollOpResult> scrollResults = ScanScroll(indexName, indexType, queryJson, scrollTime);

            int hitSeq = 0;
            ScrollHitExtraArgsImp scrollHitExtraArgs = new ScrollHitExtraArgsImp()
            {
                HitSequence = 0,
                TotalHit = 0,
                IsRunning = true
            };

            foreach (var scrollResult in scrollResults)
            {
                if (showProgress)
                {
                    Tools.LogInfo($"{scrollResult.Sequence}. Download {scrollResult.HitCount} items,  duration {scrollResult.DurationInMs} ms"
                                  + $" *** total download {scrollResult.TotalDownloadedHit:N0} / {scrollResult.TotalHit:N0} items"
                                  + $", total duration {scrollResult.TotalDurationInMs / 1000.0:0.000} seconds.");
                }

                scrollHitExtraArgs.TotalHit = scrollResult.TotalHit;

                foreach (var hit in scrollResult.Hits)
                {
                    scrollHitExtraArgs.HitSequence = hitSeq++;

                    hitCallback(hit, scrollHitExtraArgs);
                    if (!scrollHitExtraArgs.IsRunning)
                    {
                        scrollResult.IgnoreScroll();
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// Create http post request.
        /// </summary>
        /// <param name="path">Http Path.</param>
        /// <param name="content">Post Content.</param>
        /// <param name="statusCode">Returned status code.</param>
        /// <returns>Return responsed string.</returns>
        private static string CreatePostRequest(string path, string content, out HttpStatusCode statusCode)
        {
            statusCode = HttpStatusCode.SeeOther;

            HttpWebRequest webRequest = CreateRequestImpl("POST", path);
            webRequest.Proxy = null;

            byte[] data = System.Text.ASCIIEncoding.ASCII.GetBytes(content);

            webRequest.ContentLength = data.Length;

            using (Stream stream = webRequest.GetRequestStream())
            {
                stream.Write(data, 0, data.Length);
            }

            try
            {
                using (HttpWebResponse webResponse = (HttpWebResponse)webRequest.GetResponse())
                {
                    statusCode = webResponse.StatusCode;
                    return GetString(webResponse.GetResponseStream());
                }
            }
            catch (WebException exc)
            {
                statusCode = HttpStatusCode.InternalServerError;
                return GetString(exc.Response.GetResponseStream());
            }
        }

        /// <summary>
        /// Create HttpWebRequest object.
        /// </summary>
        /// <param name="method">Http method as get, post... .</param>
        /// <param name="path">Http path.</param>
        /// <returns>Return HttpWebRequest object.</returns>
        private static HttpWebRequest CreateRequestImpl(string method, string path)
        {
            ServicePointManager.Expect100Continue = false;
            string url = ConstStage.ElasticServer + path;
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);
            webRequest.Method = method;
            webRequest.ContentType = "application/json";
            webRequest.KeepAlive = true;
            webRequest.SendChunked = false;
            //// webRequest.Headers.Add("kbn-version", "4.5.0");

            return webRequest;
        }

        /// <summary>
        /// Get string from stream.
        /// </summary>
        /// <param name="stream">Stream object.</param>
        /// <returns>Return string.</returns>
        private static string GetString(Stream stream)
        {
            const int BUFFER_SIZE = 50000;
            byte[] buffer = new byte[BUFFER_SIZE];
            int n = 0;

            StringBuilder sb = new StringBuilder();
            while ((n = stream.Read(buffer, 0, BUFFER_SIZE)) > 0)
            {
                sb.Append(System.Text.ASCIIEncoding.ASCII.GetString(buffer, 0, n));
            }

            return sb.ToString();
        }

        /// <summary>
        /// Implement the interface IScanScrollOpResult.
        /// </summary>
        private class ScanScrollOpResultImp : IScanScrollOpResult
        {
            /// <summary>
            /// Gets or sets scroll page sequence. This value starts at 0.
            /// </summary>
            public int Sequence { get; set; }

            /// <summary>
            /// Gets or sets current scroll page' search result in json string format .
            /// </summary>
            public string ResultJsonStr { get; set; }

            /// <summary>
            /// Gets or sets current scroll page' search result in JObject format.
            /// This value equals JObject.Parse(ResultJsonStr) .
            /// </summary>
            public JObject Result { get; set; }

            /// <summary>
            /// Gets or sets array of hit for current scroll page.
            /// This value equals Result.SelectToken("hits.hits").
            /// </summary>
            public JArray Hits { get; set; }

            /// <summary>
            /// Gets or sets number of hit for current scroll page.
            /// This value equals Hits.Count.
            /// </summary>
            public int HitCount { get; set; }

            /// <summary>
            /// Gets or sets duration for send request and receive data from current scroll page. 
            /// This value doesn't include process time from caller.
            /// More detail, the repeating scroll processes: prepare data for request, send request to ES, get response data from ES, 
            /// parse response data and "yield" data to user. This duration is the time interval from "prepare data for request" 
            /// to "yield data to user" for a scroll operator.
            /// </summary>
            public long DurationInMs { get; set; }

            /// <summary>
            /// Gets or sets total hits that will received by ES.
            /// </summary>
            public int TotalHit { get; set; } // Result.SelectToken("hits.total").Value<int>();

            //// accumulate information

            /// <summary>
            /// Gets or sets number downloaded hits from begin to current scroll page.
            /// </summary>
            public int TotalDownloadedHit { get; set; }

            /// <summary>
            /// Gets or sets total "received" data from begin to current scroll page.
            /// </summary>
            public long DownloadedDataSize { get; set; }

            /// <summary>
            /// Gets or sets accumulated duration for begining to current scroll page. 
            /// This value doesn't include process time from caller.
            /// </summary>
            public long TotalDurationInMs { get; set; }

            /// <summary>
            /// Gets or sets Scroll Id for next scroll page.
            /// </summary>
            public string ScrollId { get; set; }

            /// <summary>
            /// Ignore scroll process. In other words, stop to receive result from ES.
            /// </summary>
            public void IgnoreScroll()
            {
                if (!string.IsNullOrEmpty(this.ScrollId))
                {
                    //// SimpleElasticSearchClient.ClearScroll(ScrollId);
                    this.ScrollId = string.Empty;
                }
            }
        }

        /// <summary>
        /// Scroll Hit Extra Information. 
        /// The callback hitProcess will receive this information.
        /// </summary>
        private class ScrollHitExtraArgsImp : IScrollHitExtraArgs
        {
            /// <summary>
            /// Gets or sets Hit Sequence.
            /// </summary>
            public int HitSequence { get; set; }

            /// <summary>
            /// Gets or sets Total Hit.
            /// </summary>
            public int TotalHit { get; set; }

            /// <summary>
            /// Gets or sets a value indicating whether query process still is running. 
            /// </summary>
            public bool IsRunning { get; set; } = true;

            /// <summary>
            /// Stop receive result from Elastic Search.
            /// </summary>
            public void StopProcess()
            {
                this.IsRunning = false;
            }
        }
    }
}
