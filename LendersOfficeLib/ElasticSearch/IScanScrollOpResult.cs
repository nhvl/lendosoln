﻿namespace LendersOffice.ElasticSearch
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Newtonsoft.Json.Linq;

    /// <summary>
    /// The returned data for each ES Scroll API.
    /// The method ScanScroll(...) will return list of IScanScrollOpResult.
    /// </summary>
    public interface IScanScrollOpResult
    {
        /// <summary>
        /// Gets scroll page sequence. This value starts at 0.
        /// </summary>
        int Sequence { get; }

        /// <summary>
        /// Gets current scroll page' search result in json string format .
        /// </summary>
        string ResultJsonStr { get; }

        /// <summary>
        /// Gets current scroll page' search result in JObject format.
        /// This value equals JObject.Parse(ResultJsonStr) .
        /// </summary>
        JObject Result { get; }

        /// <summary>
        /// Gets array of hit for current scroll page.
        /// This value equals Result.SelectToken("hits.hits").
        /// </summary>
        JArray Hits { get; }

        /// <summary>
        /// Gets number of hit for current scroll page.
        /// This value equals Hits.Count.
        /// </summary>
        int HitCount { get; }

        /// <summary>
        /// Gets duration for send request and receive data from current scroll page. 
        /// This value doesn't include process time from caller.
        /// More detail, the repeating scroll processes: prepare data for request, send request to ES, get response data from ES, 
        /// parse response data and "yield" data to user. This duration is the time interval from "prepare data for request" 
        /// to "yield data to user" for a scroll operator.
        /// </summary>
        long DurationInMs { get; }

        /// <summary>
        /// Gets total hits that will received by ES.
        /// </summary>
        int TotalHit { get; } // Result.SelectToken("hits.total").Value<int>();

        //// accumulate information

        /// <summary>
        /// Gets number downloaded hits from begin to current scroll page.
        /// </summary>
        int TotalDownloadedHit { get; }

        /// <summary>
        /// Gets total "received" data from begin to current scroll page.
        /// </summary>
        long DownloadedDataSize { get; }

        /// <summary>
        /// Gets accumulate duration for begining to current scroll page. 
        /// This value doesn't include process time from caller.
        /// </summary>
        long TotalDurationInMs { get; }

        /// <summary>
        /// Ignore scroll process. In other words, stop to receive result from ES.
        /// </summary>
        void IgnoreScroll();
    }
}
