﻿namespace LendersOffice.ElasticSearch
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Data.SqlClient;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Security.Cryptography;
    using System.Text;
    using System.Threading.Tasks;
    using Admin;
    using DataAccess;
    using Drivers.Gateways;
    using LendersOffice.Common;
    using Newtonsoft.Json.Linq;

    /// <summary>
    /// Generate Daily IIS Log Report.
    /// </summary>
    public class GenerateDailyIISReport
    {
        /// <summary>
        /// List exported field.
        /// </summary>
        private static Tuple<string, string, System.Type>[] exportedFieldNames =
        {
            Tuple.Create("StatusCode", "status_code", typeof(long)),
            Tuple.Create("DateTime", "timestamp", typeof(DateTime)),
            Tuple.Create("ClientIP", "clientip", typeof(string)),
            Tuple.Create("path", "path", typeof(string)),
            Tuple.Create("Correlation", "correlation", typeof(string)),
            Tuple.Create("Referrer", "referrer", typeof(string)),
            Tuple.Create("RequestSize", "request_size", typeof(long)),
            Tuple.Create("HttpMethod", "method", typeof(string)),
            Tuple.Create("UniqueClientID", "unique_client_id", typeof(string)),
            Tuple.Create("UserName", "user", typeof(string)),
            Tuple.Create("ResponseSize", "response_size", typeof(long)),
            Tuple.Create("LoanId", "slid", typeof(string)),
            Tuple.Create("UserAgent", "user_agent", typeof(string)),
        };

        /// <summary>
        /// Generate Daily IIS Log Report for active brokers.
        /// </summary>
        /// <param name="args">Args has 3 strings for programName, startDate, endDate.</param>
        public static void Execute(string[] args)
        {
            if (args.Length != 3)
            {
                Console.WriteLine("Usage: GenerateDailyIISReport startDateTime endDateTime\r\n" +
                                  "       with date time format yyyy-MM-ddTHH:mm:ss");
                return;
            }

            DateTime from = DateTime.ParseExact(args[1], "MM-dd-yyyyTHH:mm", CultureInfo.CurrentCulture);
            DateTime to = DateTime.ParseExact(args[2], "MM-dd-yyyyTHH:mm", CultureInfo.CurrentCulture);

            Run(from, to);
        }

        /// <summary>
        /// Generate Daily IIS Log Report for active brokers.
        /// </summary>
        /// <param name="from">Start Time for filter.</param>
        /// <param name="to">End time for filter.</param>
        public static void Run(DateTime from, DateTime to)
        {
            foreach (Guid brokerId in Tools.GetAllActiveBrokers())
            {
                var broker = BrokerDB.RetrieveById(brokerId);
                if (broker.IsGenerateDailyIISReport)
                {
                    ExportLog(brokerId, broker.CustomerCode, from, to);
                }
            }
        }

        /// <summary>
        /// Export log for one particular broker.
        /// </summary>
        /// <param name="brokerId">Broker Id.</param>
        /// <param name="customerCode">Broker's Customer Code.</param>
        /// <param name="from">Start Time for filter.</param>
        /// <param name="to">End time for filter.</param>
        private static void ExportLog(Guid brokerId, string customerCode, DateTime from, DateTime to)
        {
            var sb = new StringBuilder("LogId");
            foreach (var field in exportedFieldNames)
            {
                sb.Append(",").Append(field.Item1);
            }

            string csvHeader = sb.ToString();
            sb.AppendLine();

            int totalItems = 0, itemCount = 0;
            string logId = string.Empty;
            StreamWriter writer = null;

            Action<JToken, IScrollHitExtraArgs> processHit = (hit, hitExtraArgs) =>
            {
                string id = (string)hit.SelectToken("_id");
                sb.Append(id);

                totalItems = hitExtraArgs.TotalHit;
                JToken item = hit["_source"];
                foreach (var field in exportedFieldNames)
                {
                    string val = ParseField(item, field.Item2, field.Item3);
                    sb.Append(",").Append(val.SafeCsvString());
                }

                sb.AppendLine();
                itemCount++;

                writer.Write(sb.ToString());
                sb.Clear();
            };

            string jsonQuery = BuildJsonQuery(customerCode, from, to);
            string tempFile = TempFileUtils.NewTempFilePath();

            using (writer = new StreamWriter(tempFile, false, Encoding.UTF8))
            {
                SimpleElasticSearchClient.ScanScroll("lqb_v2_iis_log-*", "iis", jsonQuery, processHit);
            }

            writer = null;

            string key = Guid.NewGuid().ToString();
            FileDBTools.WriteFile(E_FileDB.Normal, key, tempFile);

            DateTime generatedDate = DateTime.Now;
            SqlParameter[] parameters = new SqlParameter[] 
            {
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@ReportedDate", from.Date),
                new SqlParameter("@FileDbKey", key),
                new SqlParameter("@GeneratedDate", generatedDate),
            };

            StoredProcedureHelper.ExecuteNonQuery(brokerId, "BROKER_IIS_LOG_DATA_Insert", 1, parameters);
            Tools.LogInfo($"GenerateDailyIISReport {from} *** brokerId:{brokerId} customerCode {customerCode} FileDbKey {key} #entries {itemCount:N0}");

            try
            {
                FileOperationHelper.Delete(tempFile);
            }
            catch (IOException)
            {
                // NO-OP
            }
        }

        /// <summary>
        /// Create json query to get iis logs for particular broker.
        /// </summary>
        /// <param name="customerCode">Broker's Customer Code.</param>
        /// <param name="from">Start Time for filter.</param>
        /// <param name="to">End time for filter.</param>
        /// <returns>Return query string in json format.</returns>
        private static string BuildJsonQuery(string customerCode, DateTime from, DateTime to)
        {
            var sb = new StringBuilder();
            sb.Append("'_source': [");
            bool isFirst = true;
            foreach (var field in exportedFieldNames)
            {
                if (isFirst)
                {
                    isFirst = false;
                }
                else
                {
                    sb.Append(",");
                }

                sb.Append($"'{field.Item2}'");
            }

            sb.Append("]");
            string fields = sb.ToString();

            return GetIISLogJsonQuery("customer_code:" + customerCode, from, to, 5000, fields);
        }

        /// <summary>
        /// Create json query.
        /// </summary>
        /// <param name="query">Plain text query string with Lucene syntax.</param>
        /// <param name="from">Start Time for filter.</param>
        /// <param name="to">End time for filter.</param>
        /// <param name="size">How many items will return from ES.</param>
        /// <param name="fields">List of fields will return from ES.</param>
        /// <returns>Return json query string.</returns>
        private static string GetIISLogJsonQuery(string query, DateTime from, DateTime to, int size = 10, string fields = "")
        {
            if (!string.IsNullOrEmpty(query))
            {
                query = Newtonsoft.Json.JsonConvert.ToString(query);
                if (query.Length >= 2 && query[0] == '\"' && query[query.Length - 1] == '\"')
                {
                    query = query.Substring(1, query.Length - 2);
                }
            }

            string queryJson =
                queryJson =
@"{
    'query': {
        'bool': {
            'must':   {@query_string},
            'filter': {'range': { 'timestamp': {'gte':'{@from}', 'lt':'{@to}'}}}
        }
    },

    {@fields},
    'from': 0, 'size': {@size},
    'sort': { 'timestamp': { 'order': 'desc'} }
}";

            string query_string;

            if (string.IsNullOrEmpty(query))
            {
                query_string = @"{ ""match_all"": {} }";
            }
            else
            {
                query_string =
@"{
                'query_string': {
                    'query': '{@query}',
                    'fields': ['path', 'user^2', 'customer_code^2', 'slid^2', 'query'],
                    'use_dis_max': true,
                    'default_operator': 'and'
                }
            }";
                query_string = query_string.Replace('\'', '\"')
                                           .Replace("{@query}", query);
            }

            if (fields == null)
            {
                queryJson = queryJson.Replace("{@fields},", string.Empty);
            }
            else
            {
                if (fields == string.Empty)
                {
                    fields = "'_source': ['timestamp', 'customer_code', 'user', 'method', 'host', 'path', 'soap_method', 'status_code', 'time_processing', 'server']";
                }

                queryJson = queryJson.Replace("{@fields}", fields);
            }

            queryJson = queryJson.Replace('\'', '\"')
                                 .Replace("{@to}", ElasticSearchUtilities.ToUtcDisplayString(to))
                                 .Replace("{@from}", ElasticSearchUtilities.ToUtcDisplayString(from))
                                 .Replace("{@size}", size.ToString())
                                 .Replace("{@query_string}", query_string);

            return queryJson;
        }

        /// <summary>
        /// Extract field's value from jtoken object.
        /// </summary>
        /// <param name="jtoken">Jtoken object.</param>
        /// <param name="name">Field Name.</param>
        /// <param name="type">Field's type.</param>
        /// <returns>Return field value.</returns>
        private static string ParseField(JToken jtoken, string name, Type type)
        {
            if (type.Equals(typeof(long)))
            {
                jtoken.SelectToken(name).Value<long>();
            }
            else if (type.Equals(typeof(int)))
            {
                jtoken.SelectToken(name).Value<int>();
            }
            else if (type.Equals(typeof(DateTime)))
            {
                var dt = jtoken.SelectToken(name).Value<DateTime>().ToLocalTime();
                return dt.ToString("MM/dd/yyyy HH:mm:ss");
            }
            else if (!type.Equals(typeof(string)))
            {
                throw new ArgumentException($"Don't support type {type.ToString()} for field {name}");
            }

            return jtoken.SelectToken(name).Value<string>();
        }
    }
}
