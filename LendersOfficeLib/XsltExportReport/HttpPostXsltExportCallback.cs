﻿namespace LendersOffice.XsltExportReport
{
    using System;
    using Common;
    using Drivers.FileSystem;
    using Drivers.HttpRequest;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers.HttpRequest;

    /// <summary>
    /// Defines a simple POST call to send the result file to a url.<para/>
    /// For now, this assumes the content-type is text/xml.
    /// </summary>
    public class HttpPostXsltExportCallback : IXsltExportCallBack
    {
        /// <summary>
        /// Handles the operation complete event for an XSLT export.
        /// </summary>
        /// <param name="brokerId">The broker ID.</param>
        /// <param name="request">The XSLT export request.</param>
        /// <param name="result">The XSLT export result.</param>
        public void OperationComplete(Guid brokerId, XsltExportRequest request, XsltExportResult result)
        {
            if (result.HasError)
            {
                return;
            }

            LqbAbsoluteUri url = LqbAbsoluteUri.Create(request.GetCallBackParameter("url")).ForceValue();
            var options = new HttpRequestOptions();
            options.Method = HttpMethod.Post;
            options.MimeType = MimeType.Text.Xml;
            options.PostData = new ByteContent(BinaryFileHelper.ReadAllBytes(LocalFilePath.Create(result.OutputFileLocation).ForceValue()));
            WebRequestHelper.ExecuteCommunication(url, options);
        }
    }
}
