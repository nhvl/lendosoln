namespace LendersOffice.XsltExportReport
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Xml.Linq;
    using Common;
    using DataAccess;
    using LendersOffice.Drivers.SqlServerDB;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers.FtpRequest;
    using ObjLib.UI;

    /// <summary>
    /// Represents the settings for a scheduled SFTP Export to be processed by <c>ScheduleExecutable.SFTPXsltExport</c>.
    /// </summary>
    public class XsltExportTransferSettings
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="XsltExportTransferSettings"/> class.
        /// </summary>
        /// <param name="brokerId">Broker Id of the settings. This is required value.</param>
        /// <param name="exportId">Export ID for specifying an existing record to modify. Set to null for a new record.</param>
        /// <param name="exportName">The name of the export.</param>
        /// <param name="userId">References <see cref="AbstractUserPrincipal.UserId"/>.</param>
        /// <param name="reportId">References the Custom Reports DB table REPORT_QUERY.</param>
        /// <param name="notes">General notes of the settings.</param>
        /// <param name="protocol">The FTP protocol type.</param>
        /// <param name="hostName">The host part of the destination web address.</param>
        /// <param name="port">The port of the destination address.</param>
        /// <param name="destPath">The path on the destination host to post to. {DATE} and {TIME} substrings are replaced by the date and time of transmission.</param>
        /// <param name="hostKeyFingerprint">SSH fingerprint for the upload.</param>
        /// <param name="to">Whoever will receive the email.</param>
        /// <param name="from">Whoever is sending the email.</param>
        /// <param name="mapName">Mapping name.</param>
        /// <param name="useDotNet">Indicates whether to use .NET framework FTP libraries (true) or the WinSCP program (false) for the transfer.</param>
        /// <param name="login">SFTP login name.</param>
        /// <param name="encryptedPassword">Encrypted password.</param>
        /// <param name="securityMode">The security setting to use.</param>
        /// <param name="hostCertificateFingerprint">The host's SSL fingerprint to specify.</param>
        /// <param name="usePassive">Whether or not to use passive FTP.</param>
        /// <param name="additionalPostUrl">An additional URL to which to transfer the same data.</param>
        /// <param name="comments">Internal comments about the configuration.</param>
        /// <param name="fileName">Name of the attached file in the email.</param>
        /// <param name="subject">Subject of the email.</param>
        /// <param name="message">Message of the email.</param>
        private XsltExportTransferSettings(
            Guid brokerId,
            int? exportId,
            string exportName,
            Guid userId,
            Guid reportId,
            string notes,
            FileTransferProtocol protocol,
            string hostName,
            int port,
            string destPath,
            string hostKeyFingerprint,
            string to,
            string from,
            string mapName = "",
            bool useDotNet = true,
            string login = "",
            string encryptedPassword = "",
            FileTransferProtocol.SecurityMode? securityMode = null,
            string hostCertificateFingerprint = "",
            bool usePassive = false,
            LqbAbsoluteUri? additionalPostUrl = null,
            string comments = "",
            string fileName = "",
            string subject = "",
            string message = "")
        {
            if (exportId.HasValue)
            {
                this.ExportId = exportId.Value;
            }

            this.IsNewObject = !exportId.HasValue || exportId.Value <= 0;
            this.ExportName = exportName;
            this.MapName = mapName;
            this.UserId = userId;
            this.ReportId = reportId;
            this.BrokerId = brokerId;
            this.Notes = notes;
            this.TransferOptions = new FtpRequestOptions
            {
                Protocol = protocol,
                UserName = login,
                Password = EncryptionHelper.Decrypt(encryptedPassword),
                HostKeyFingerprint = hostKeyFingerprint,
                HostCertificateFingerprint = hostCertificateFingerprint,
                UsePassive = usePassive
            };

            if (securityMode.HasValue)
            {
                this.TransferOptions.Mode = securityMode.Value;
            }

            this.HostName = hostName;
            this.Port = port;
            this.DestPath = destPath;
            this.UseDotNet = useDotNet;
            this.AdditionalPostUrl = additionalPostUrl;
            this.Comments = comments;

            this.To = to;
            this.From = from;
            this.FileName = fileName;
            this.Subject = subject;
            this.Message = message;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="XsltExportTransferSettings"/> class from the given data record. <para></para>
        /// Note: Even though the database column name is "Password", we actually store an EncryptedPassword in the database.
        /// </summary>
        /// <param name="dataRow">A data record to use for loading the data.</param>
        private XsltExportTransferSettings(IDataRecord dataRow)
        {
            this.BrokerId = (Guid)dataRow["BrokerId"];
            this.CustomerCode = dataRow.SafeString("CustomerCode");
            this.BrokerName = dataRow.SafeString("BrokerNm");
            this.Notes = (string)dataRow["Notes"];
            this.ExportId = (int)dataRow["ExportId"];
            this.ExportName = (string)dataRow["ExportName"];
            this.MapName = (string)dataRow["MapName"];
            this.UserId = (Guid)dataRow["UserId"];
            this.UserLoginName = dataRow.SafeString("UserLoginName");

            this.ReportId = (Guid)dataRow["ReportId"];
            this.ReportName = dataRow.SafeString("QueryName");
            this.TransferOptions = new FtpRequestOptions()
            {
                UserName = dataRow["Login"] != DBNull.Value ? (string)dataRow["Login"] : string.Empty,
                Password = dataRow["Password"] != DBNull.Value ? EncryptionHelper.Decrypt((string)dataRow["Password"]) : string.Empty,
                UsePassive = dataRow["UsePassive"] != DBNull.Value ? (bool)dataRow["UsePassive"] : false,
                HostCertificateFingerprint = dataRow["SSLHostFingerprint"] != DBNull.Value ? (string)dataRow["SSLHostFingerprint"] : string.Empty,
                HostKeyFingerprint = dataRow["SSHFingerprint"] != DBNull.Value ? (string)dataRow["SSHFingerprint"] : string.Empty,
            };

            var protocol = FileTransferProtocol.Parse(dataRow["Protocol"] != DBNull.Value ? (string)dataRow["Protocol"] : string.Empty);
            if (protocol.HasValue)
            {
                this.TransferOptions.Protocol = protocol.Value;
            }

            var mode = FileTransferProtocol.SecurityMode.Parse(dataRow["SecurityMode"] != DBNull.Value ? (string)dataRow["SecurityMode"] : "None");
            if (mode.HasValue)
            {
                this.TransferOptions.Mode = mode.Value;
            }

            this.HostName = dataRow["HostName"] != DBNull.Value ? (string)dataRow["HostName"] : string.Empty;
            this.Port = dataRow["Port"] != DBNull.Value ? (int)dataRow["Port"] : 21;
            this.DestPath = dataRow["DestPath"] != DBNull.Value ? (string)dataRow["DestPath"] : string.Empty;
            this.UseDotNet = dataRow["Use_DotNet"] != DBNull.Value ? (bool)dataRow["Use_DotNet"] : true;
            this.AdditionalPostUrl = LqbAbsoluteUri.Create(dataRow["AdditionalPostUrl"] != DBNull.Value ? (string)dataRow["AdditionalPostUrl"] : null);
            this.Comments = dataRow["Comments"] != DBNull.Value ? (string)dataRow["Comments"] : string.Empty;

            this.To = dataRow["To"] != DBNull.Value ? (string)dataRow["To"] : string.Empty;
            this.From = dataRow["From"] != DBNull.Value ? (string)dataRow["From"] : string.Empty;
            this.FileName = dataRow["FileName"] != DBNull.Value ? (string)dataRow["FileName"] : string.Empty;
            this.Subject = dataRow["Subject"] != DBNull.Value ? (string)dataRow["Subject"] : string.Empty;
            this.Message = dataRow["Message"] != DBNull.Value ? (string)dataRow["Message"] : string.Empty;          
        }

        /// <summary>
        /// Gets a value indicating whether this is a new object that needs to be inserted into the DB (true), or if false, an existing record to be updated.
        /// </summary>
        /// <value>New object indicator.</value>
        public bool IsNewObject
        {
            get; private set;
        }

        /// <summary>
        /// Gets the broker id of this settings object.
        /// </summary>
        /// <value>Broker Id.</value>
        public Guid BrokerId { get; private set; }

        /// <summary>
        /// Gets the broker customer code.
        /// </summary>
        /// <value>Broker customer code.</value>
        public string CustomerCode { get; private set; }

        /// <summary>
        /// Gets the broker name.
        /// </summary>
        /// <value>Broker name.</value>
        public string BrokerName { get; private set; }

        /// <summary>
        /// Gets the broker display name.
        /// </summary>
        /// <value>Broker display name.</value>
        public string BrokerDisplayName
        {
            get { return $"{CustomerCode} - {BrokerName}"; }
        }

        /// <summary>
        /// Gets the unique ID used in the database to identify the settings object.
        /// </summary>
        /// <value>Export ID.</value>
        public int ExportId
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets or sets the human-readable export name/ID.
        /// </summary>
        /// <value>Export Name.</value>
        public string ExportName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the export mapping name.
        /// </summary>
        /// <value>The Map Name.</value>
        public string MapName
        {
            get; set;
        }

        /// <summary>
        /// Gets or sets the User Id of the associated <see cref="AbstractUserPrincipal"/>.
        /// </summary>
        /// <value>The User ID.</value>
        public Guid UserId
        {
            get; set;
        }

        /// <summary>
        /// Gets the login name of user id.
        /// </summary>
        /// <value>The login name of user id.</value>
        public string UserLoginName { get; private set; }

        /// <summary>
        /// Gets or sets the Report ID of the custom report to be run. DB reference: REPORT_QUERY(QueryId).
        /// </summary>
        /// <value>Report ID.</value>
        public Guid ReportId
        {
            get; set;
        }

        /// <summary>
        /// Gets the name of the report.
        /// </summary>
        /// <value>The name of the report.</value>
        public string ReportName { get; private set; }

        /// <summary>
        /// Gets or sets the notes / comment of this settings.
        /// </summary>
        /// <value>The notes / comment.</value>
        public string Notes { get; set; }

        /// <summary>
        /// Gets or sets the hostname of the address to send to.
        /// </summary>
        /// <value>Host Name.</value>
        public string HostName
        {
            get; set;
        }

        /// <summary>
        /// Gets or sets the Port number to use for the connection.
        /// </summary>
        /// <value>Port number.</value>
        public int Port
        {
            get; set;
        }

        /// <summary>
        /// Gets or sets the path part of the destination address for the transfer. Used with <see cref="HostName"/> and <see cref="Port"/> to construct a full URL.
        /// {DATE} and {TIME} substrings are replaced by the date and time of transmission.
        /// </summary>
        /// <value>Destination Path.</value>
        public string DestPath
        {
            get; set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not to use the .NET Framework's FTP tools for the transmission.
        /// Alternative (false) is WinSCP.
        /// </summary>
        /// <value>Whether to use .NET.</value>
        public bool UseDotNet
        {
            get; set;
        }

        /// <summary>
        /// Gets or sets the options object associated with the FTP transfer.
        /// Be sure to set <see cref="FtpRequestOptions.UploadFile"/> in code before executing an FTP request with this object.
        /// </summary>
        /// <value>Transfer options.</value>
        public FtpRequestOptions TransferOptions
        {
            get; set;
        }

        /// <summary>
        /// Gets or sets an additional post URL to receive the same report data.
        /// </summary>
        /// <value>Additional post URL.</value>
        public LqbAbsoluteUri? AdditionalPostUrl
        {
            get; set;
        }

        /// <summary>
        /// Gets or sets comments for the context of the setting and its use, 
        /// including things like the name of the user account or the 
        /// human-readable client and vendor names associated with the export.
        /// </summary>
        /// <value>Internal comments.</value>
        public string Comments
        {
            get; set;
        }

        /// <summary>
        /// Gets or sets the section of the email that defines who the email 
        /// is going to. Can designate multiple by separating the email addresses
        /// with semicolons.
        /// </summary>
        /// <value>To section of the email.</value>
        public string To
        {
            get; set;
        }

        /// <summary>
        /// Gets or sets the section of the email that defines who the email 
        /// is coming from. Can designate multiple by separating the email addresses
        /// with semicolons.
        /// </summary>
        /// <value>From section of the email.</value>
        public string From
        {
            get; set;
        }

        /// <summary>
        /// Gets or sets the name of the attached file in the email.
        /// </summary>
        /// <value>Name of the file.</value>
        public string FileName
        {
            get; set;
        }

        /// <summary>
        /// Gets or sets the subject portion of the email.       
        /// </summary>
        /// <value>Subject of the email.</value>
        public string Subject
        {
            get; set;
        }

        /// <summary>
        /// Gets or sets the message portion of the email.       
        /// </summary>
        /// <value>Message of the email.</value>
        public string Message
        {
            get; set;
        }

        /// <summary>
        /// Retrieves the <see cref="XsltExportTransferSettings"/> object with the given ID from the database.
        /// </summary>
        /// <param name="brokerId">The broker id of the settings.</param>
        /// <param name="exportId">The unique export identifier to retrieve from the database.</param>
        /// <returns>An instance of the <see cref="XsltExportTransferSettings"/> class with data loaded from the database. <c>null</c> if no record was found with the given ID.</returns>
        public static XsltExportTransferSettings Retrieve(Guid brokerId, int exportId)
        {
            var parameters = new SqlParameter[]
            {
                new SqlParameter("@ExportId", exportId),
                new SqlParameter("@BrokerId", brokerId)
            };

            using (var connection = DbAccessUtils.GetConnection(brokerId))
            {
                using (var reader = StoredProcedureDriverHelper.ExecuteReader(
                    conn: connection,
                    transaction: null,
                    procedureName: StoredProcedureName.Create("SCHEDULED_XSLT_EXPORTS_Retrieve").Value,
                    parameters: parameters,
                    timeout: TimeoutInSeconds.Thirty))
                {
                    if (reader.Read())
                    {
                        return new XsltExportTransferSettings(reader);
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Retrieves the <see cref="XsltExportTransferSettings"/> object with the given exportName and customerCode from the database.
        /// </summary>
        /// <param name="exportName">The unique export name to retrieve from the database.</param>
        /// <param name="customerCode">Client customer code.</param>
        /// <returns>An instance of the <see cref="XsltExportTransferSettings"/> class with data loaded from the database. <c>null</c> if no record was found.</returns>
        public static XsltExportTransferSettings RetrieveByExportName(string exportName, string customerCode)
        {
            Guid brokerId;
            var connInfo = DbConnectionInfo.GetConnectionInfoByCustomerCode(customerCode, out brokerId);
            return RetrieveByExportNameImpl(exportName, brokerId, connInfo);
        }

        /// <summary>
        /// Retrieves the <see cref="XsltExportTransferSettings"/> object with the given exportName and brokerId from the database.
        /// </summary>
        /// <param name="exportName">The unique export name to retrieve from the database.</param>
        /// <param name="brokerId">Client broker id.</param>
        /// <returns>An instance of the <see cref="XsltExportTransferSettings"/> class with data loaded from the database. <c>null</c> if no record was found.</returns>
        public static XsltExportTransferSettings RetrieveByExportName(string exportName, Guid brokerId)
        {
            var connInfo = DbConnectionInfo.GetConnectionInfo(brokerId);
            return RetrieveByExportNameImpl(exportName, brokerId, connInfo);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="XsltExportTransferSettings"/> class from the given view model.
        /// </summary>
        /// <param name="viewModel">The view model to convert into this class for DB storage and data layer operations.</param>
        /// <param name="errorMessages">A list of error messages encountered in converting view model to settings object.</param>
        /// <returns>If successful, a new instance of <see cref="XsltExportTransferSettings"/>, or if there were errors, null.</returns>
        public static XsltExportTransferSettings CreateFromViewModel(SFTPConfigViewModel viewModel, out List<string> errorMessages)
        {
            errorMessages = new List<string>();

            // Add breaking errors here.
            if (errorMessages.Count > 0)
            {
                return null;
            }

            return new XsltExportTransferSettings(
                brokerId: viewModel.BrokerId,
                exportId: viewModel.ExportId,
                exportName: viewModel.ExportName,
                userId: viewModel.UserId ?? Guid.Empty,
                reportId: viewModel.ReportId ?? Guid.Empty,
                notes: viewModel.Notes,
                protocol: FileTransferProtocol.Parse(viewModel.Protocol) ?? FileTransferProtocol.Ftp,
                hostName: viewModel.HostName,
                port: viewModel.Port ?? 21, // Default FTP port
                destPath: viewModel.DestPath,
                hostKeyFingerprint: viewModel.SSHFingerprint,
                mapName: viewModel.MapName,
                useDotNet: viewModel.Use_DotNet,
                login: viewModel.Login,
                encryptedPassword: viewModel.EncryptedPassword,
                securityMode: FileTransferProtocol.SecurityMode.Parse(viewModel.FtpSecure),
                hostCertificateFingerprint: viewModel.SSLHostFingerprint,
                usePassive: viewModel.UsePassive,
                additionalPostUrl: LqbAbsoluteUri.Create(viewModel.AdditionalPostUrl),
                comments: viewModel.Comments,
                to: viewModel.To,
                from: viewModel.From,
                fileName: viewModel.FileName,
                subject: viewModel.Subject,
                message: viewModel.Message);
        }
        
        /// <summary>
        /// Retrieves all scheduled FTP exports from the database.
        /// </summary>
        /// <returns>A list of all the scheduled FTP exports.</returns>
        public static List<XsltExportTransferSettings> RetrieveAll()
        {
            List<XsltExportTransferSettings> allExports = new List<XsltExportTransferSettings>();
            foreach (var connInfo in DbConnectionInfo.ListAll())
            {
                using (var reader = StoredProcedureDriverHelper.ExecuteReader(
                    conn: connInfo.GetConnection(),
                    transaction: null,
                    procedureName: StoredProcedureName.Create("SCHEDULED_XSLT_EXPORTS_ListAll").Value,
                    parameters: new SqlParameter[] { },
                    timeout: TimeoutInSeconds.Thirty))
                {
                    while (reader.Read())
                    {
                        allExports.Add(new XsltExportTransferSettings(reader));
                    }
                }
            }

            return allExports;
        }

        /// <summary>
        /// Saves this object to the database using the appropriate INSERT or UPDATE stored procedure. <para></para>
        /// Note: Even though the database column name is "Password", we actually store an EncryptedPassword in the database.
        /// </summary>
        public void Save()
        {
            if (!this.IsValid())
            {
                throw new CBaseException("Unable to save due to invalid BrokerId, UserId or ReportId", "Unable to save due to invalid BrokerId, UserId or ReportId");
            }

            var spName = StoredProcedureName.Invalid;
            var parameters = new List<SqlParameter>()
            {
                new SqlParameter("@BrokerId", this.BrokerId),
                new SqlParameter("@ExportName", this.ExportName),
                new SqlParameter("@MapName", this.MapName),
                new SqlParameter("@UserId", this.UserId),
                new SqlParameter("@ReportId", this.ReportId),
                new SqlParameter("@Notes", this.Notes),
                new SqlParameter("@Protocol", this.TransferOptions.Protocol.ToString()),
                new SqlParameter("@HostName", this.HostName),
                new SqlParameter("@Login", this.TransferOptions.UserName),
                new SqlParameter("@Password", EncryptionHelper.Encrypt(this.TransferOptions.Password)),
                new SqlParameter("@Port", this.Port),
                new SqlParameter("@SSHFingerprint", this.TransferOptions.HostKeyFingerprint),
                new SqlParameter("@DestPath", this.DestPath),
                new SqlParameter("@Use_DotNet", this.UseDotNet),
                new SqlParameter("@SecurityMode", this.TransferOptions.Mode.ToString()),
                new SqlParameter("@SSLHostFingerprint", this.TransferOptions.HostCertificateFingerprint),
                new SqlParameter("@UsePassive", this.TransferOptions.UsePassive),
                new SqlParameter("@AdditionalPostUrl", this.AdditionalPostUrl?.ToString() ?? string.Empty),
                new SqlParameter("@Comments", this.Comments),
                new SqlParameter("@To", this.To),
                new SqlParameter("@From", this.From),
                new SqlParameter("@FileName", this.FileName),
                new SqlParameter("@Subject", this.Subject),
                new SqlParameter("@Message", this.Message)
            };

            if (this.IsNewObject)
            {
                spName = StoredProcedureName.Create("SCHEDULED_XSLT_EXPORTS_Insert").Value;
            }
            else
            {
                spName = StoredProcedureName.Create("SCHEDULED_XSLT_EXPORTS_Update").Value;
                parameters.Add(new SqlParameter("@ExportId", this.ExportId));
            }
            
            using (var connection = DbAccessUtils.GetConnection(this.BrokerId))
            {
                this.ExportId = (int)StoredProcedureDriverHelper.ExecuteScalar(
                    conn: connection,
                    transaction: null,
                    procedureName: spName,
                    parameters: parameters,
                    timeout: TimeoutInSeconds.Thirty);
            }

            this.IsNewObject = false;
        }

        /// <summary>
        /// Check if the userId and reportId of this object match the same broker. 
        /// </summary>
        /// <remarks>
        /// Unfortunately, there is no custom report object, so this uses an ad-hoc SQL query to join the necessary tables.
        /// </remarks>
        /// <returns>True if the report and user match (same BrokerId) AND the user is an active user, false if they do not.</returns>
        public bool IsValid()
        {
            List<SqlParameter> parameters;
            bool recordFound = false;
            using (var conn = DbAccessUtils.GetConnection(this.BrokerId))
            {
                parameters = new List<SqlParameter>()
                {
                    new SqlParameter("@UserId", this.UserId),
                    new SqlParameter("@QueryId", this.ReportId),
                    new SqlParameter("@BrokerId", this.BrokerId)
                };

                var queryString = SQLQueryString.Create(@"
                        SELECT QueryId, u.UserId, u.BrokerId
                        FROM REPORT_QUERY r
                        JOIN [dbo].[VIEW_ACTIVE_LO_AND_PML_USER_BY_BROKERID] u ON r.BrokerId = u.BrokerId
                        WHERE u.UserId = @UserId AND r.QueryId = @QueryId AND r.BrokerId = @BrokerId").Value;

                using (IDataReader reader = SqlServerHelper.Select(conn, null, queryString, parameters, TimeoutInSeconds.Default))
                {
                    recordFound = recordFound || reader.Read();
                }
            }

            return recordFound;
        }

        /// <summary>
        /// Converts the settings object into a view model object suitable for converting to JSON and using in the UI.
        /// </summary>
        /// <returns>A new instance of <see cref="SFTPConfigViewModel"/> with values from this <see cref="XsltExportTransferSettings"/> object.</returns>
        public SFTPConfigViewModel ToViewModel()
        {
            return new SFTPConfigViewModel
            {
                BrokerId = this.BrokerId,
                BrokerDisplayName = this.BrokerDisplayName,
                Notes = this.Notes,
                AdditionalPostUrl = this.AdditionalPostUrl?.ToString(),
                Comments = this.Comments,
                DestPath = this.DestPath,
                ExportId = this.ExportId,
                ExportName = this.ExportName,
                FtpSecure = this.TransferOptions.Mode.ToString(),
                HostName = this.HostName,
                Login = this.TransferOptions.UserName,
                MapName = this.MapName,
                EncryptedPassword = EncryptionHelper.Encrypt(this.TransferOptions.Password),
                Port = this.Port,
                Protocol = this.TransferOptions.Protocol.ToString(),
                ReportId = this.ReportId,
                ReportName = this.ReportName,
                SSHFingerprint = this.TransferOptions.HostKeyFingerprint,
                SSLHostFingerprint = this.TransferOptions.HostCertificateFingerprint,
                UsePassive = this.TransferOptions.UsePassive,
                UserId = this.UserId,
                UserLoginName = this.UserLoginName,
                Use_DotNet = this.UseDotNet,
                IsValid = this.IsValid(),
                From = this.From,
                To = this.To,
                FileName = this.FileName,
                Subject = this.Subject,
                Message = this.Message            
            };
        }

        /// <summary>
        /// Converts the settings to an <see cref="XElement"/> instance as it is would be in the <see cref="SFTPXsltExportSettings.xml"></see> file <para></para>
        /// Note: the xml attribute "password" is actually an encrypted password.  This is for consistency with <see cref="SFTPXsltExportSettings.xml"/>.
        /// </summary>
        /// <returns>An <see cref="XElement"/> representing the relevant settings.</returns>
        public XElement ToXElement()
        {
            var x = new XElement(
                "item",
                new XAttribute("id", this.ExportName),
                new XAttribute("map_name", this.MapName),
                new XAttribute("userid", this.UserId.ToString()),
                new XAttribute("reportid", this.ReportId.ToString()),
                new XAttribute("protocol", this.TransferOptions.Protocol.ToString()),
                new XAttribute("host_name", this.HostName),
                new XAttribute("login", this.TransferOptions.UserName),
                new XAttribute("password", EncryptionHelper.Encrypt(this.TransferOptions.Password)),
                new XAttribute("port", this.Port.ToString()),
                new XAttribute("ssh_finger_print", this.TransferOptions.HostKeyFingerprint),
                new XAttribute("dest_path", this.DestPath),
                new XAttribute("use_net", this.UseDotNet.ToString()),
                new XAttribute("ftp_secure", this.TransferOptions.Mode.ToString()),
                new XAttribute("ssl_host_finger_print", this.TransferOptions.HostCertificateFingerprint),
                new XAttribute("use_passive", this.TransferOptions.UsePassive.ToString()),
                new XAttribute("additional_post_url", this.AdditionalPostUrl?.ToString() ?? string.Empty));

            return x;
        }

        /// <summary>
        /// Validate email information.
        /// </summary>
        /// <returns>True if have valid data for email.</returns>
        public bool IsSupportedEmail()
        {
            return !string.IsNullOrEmpty(this.To);
        }

        /// <summary>
        /// Validate ftp information.
        /// </summary>
        /// <returns>True if have valid data for ftp.</returns>
        public bool IsSupportedFtp()
        {
            return !string.IsNullOrEmpty(this.HostName);
        }

        /// <summary>
        /// Implementation for retrieving the <see cref="XsltExportTransferSettings"/> object.
        /// </summary>
        /// <param name="exportName">The unique export name to retrieve from the database.</param>
        /// <param name="brokerId">Client broker id.</param>
        /// <param name="connInfo">DB connection info.</param>
        /// <returns>An instance of the <see cref="XsltExportTransferSettings"/> class with data loaded from the database. <c>null</c> if no record was found.</returns>
        private static XsltExportTransferSettings RetrieveByExportNameImpl(string exportName, Guid brokerId, DbConnectionInfo connInfo)
        {
            var parameters = new SqlParameter[]
            {
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@ExportName", exportName)
            };

            using (var reader = StoredProcedureHelper.ExecuteReader(connInfo, "SCHEDULED_XSLT_EXPORTS_RetrieveByExportName", TimeoutInSeconds.Thirty.Value, parameters))
            {
                if (reader.Read())
                {
                    return new XsltExportTransferSettings(reader);
                }
            }

            return null;
        }
    }
}
