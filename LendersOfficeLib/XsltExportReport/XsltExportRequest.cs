﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using DataAccess;
using System.Reflection;

namespace LendersOffice.XsltExportReport
{
    public class XsltExportRequest
    {
        public string ReportId { get; private set; }
        public IEnumerable<Guid> LoanIdList { get; private set; }
        public string ReportName { get; private set; }
        public string MapName { get; private set; }
        public Guid UserId { get; private set; }
        public string OutputFileName { get; private set; }
        public DateTime CreatedDate { get; private set; }
        public XsltMapItem MapItem { get; private set; }

        private IEnumerable<KeyValuePair<string, string>> m_callBackParameterList = null;

        public string GetCallBackParameter(string name)
        {
            if (m_callBackParameterList == null) {
                return string.Empty;
            }

            foreach(var o in m_callBackParameterList) {
                if (o.Key.Equals(name, StringComparison.OrdinalIgnoreCase))
                {
                    return o.Value;
                }
            }
            return string.Empty;
        }

        public IXsltExportCallBack CallBack { get; private set; }
        public XsltExportRequest(Guid userId, IEnumerable<Guid> loanIdList, string mapName, IXsltExportCallBack callback, IEnumerable<KeyValuePair<string,string>> callbackParameters)
        {

            XsltMapItem mapItem = XsltMap.Get(mapName);

            this.MapItem = mapItem;
            this.ReportName = mapItem.Description;
            this.UserId = userId;
            this.LoanIdList = loanIdList;
            this.MapName = mapName;
            this.CallBack = callback;
            this.CreatedDate = DateTime.Now;
            this.OutputFileName = mapItem.OutputFileName;
            m_callBackParameterList = callbackParameters;
            CPmlFIdGenerator generator = new CPmlFIdGenerator();

            this.ReportId = generator.GenerateNewFriendlyId();
        }
        public XDocument ToXDocument()
        {
            XDocument xdoc = new XDocument();
            XElement root = new XElement("XsltExportRequest");
            xdoc.Add(root);

            root.SetAttributeValue("ReportId", ReportId);
            root.SetAttributeValue("ReportName", ReportName);
            root.SetAttributeValue("MapName", MapName);
            root.SetAttributeValue("UserId", UserId.ToString());
            root.SetAttributeValue("CreatedDate", CreatedDate.ToString("MM-dd-yyyy HH:mm:ss"));
            root.SetAttributeValue("OutputFileName", OutputFileName);
            root.SetAttributeValue("CallBackType", CallBack == null ? string.Empty : CallBack.GetType().FullName);

            foreach (Guid sLId in LoanIdList)
            {
                root.Add(new XElement("sLId", sLId.ToString()));
            }

            if (m_callBackParameterList != null) 
            {
                foreach (var o in m_callBackParameterList) 
                {
                    root.Add(new XElement("CallBackParam", new XAttribute("key", o.Key), new XAttribute("value", o.Value)));
                }
            }

            return xdoc;
        }

        public string ToXml()
        {
            return ToXDocument().ToString();
        }
        public static XsltExportRequest Parse(string xml)
        {
            if (string.IsNullOrEmpty(xml))
            {
                return null;
            }

            XDocument xdoc = XDocument.Parse(xml);
            return Parse(xdoc);

        }
        public static XsltExportRequest Parse(XDocument xdoc)
        {
            XElement root = xdoc.Root;

            string reportName = root.Attribute("ReportName").Value;
            string mapName = root.Attribute("MapName").Value;
            Guid userId = new Guid(root.Attribute("UserId").Value);
            DateTime createdDate = DateTime.Parse(root.Attribute("CreatedDate").Value);
            string reportId = root.Attribute("ReportId").Value;
            string callBackType = root.Attribute("CallBackType").Value;
            string outputFileName = root.Attribute("OutputFileName").Value;

            IXsltExportCallBack callback = null;
            if (string.IsNullOrEmpty(callBackType) == false)
            {

                Assembly assembly = typeof(IXsltExportCallBack).Assembly;

                Type t = assembly.GetType(callBackType);
                if (t != null && t.GetInterface(typeof(IXsltExportCallBack).Name) != null)
                {
                    ConstructorInfo c = t.GetConstructor(new Type[0]);
                    if (c != null)
                    {
                        callback = (IXsltExportCallBack)c.Invoke(new object[0]);

                    }

                }
            }

            List<Guid> loanIdList = new List<Guid>();
            foreach (XElement child in root.Elements("sLId"))
            {
                loanIdList.Add(new Guid(child.Value));
            }

            List<KeyValuePair<string, string>> callBackParameterList = new List<KeyValuePair<string,string>>();
            foreach (XElement child in root.Elements("CallBackParam")) {
                string key = child.Attribute("key").Value;
                string value = child.Attribute("value").Value;
                callBackParameterList.Add(new KeyValuePair<string,string>(key, value));
            }
            XsltExportRequest exportRequest = new XsltExportRequest(userId, loanIdList, mapName, callback, callBackParameterList);
            exportRequest.CreatedDate = createdDate;
            exportRequest.ReportId = reportId;
            return exportRequest;

        }

        /// <summary>
        /// Generate an export request that will automatically include all loans
        /// in the given custom report.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="reportId"></param>
        /// <param name="mapName"></param>
        /// <param name="callback"></param>
        /// <param name="callbackParameters"></param>
        /// <returns></returns>
        public static XsltExportRequest CreateFromCustomReport(Guid brokerId, Guid userId, Guid reportId, string mapName,
             IXsltExportCallBack callback, IEnumerable<KeyValuePair<string, string>> callbackParameters)
        {
            LendersOffice.Reports.LoanReporting loanReporting = new LendersOffice.Reports.LoanReporting();

            LendersOffice.QueryProcessor.Report report = loanReporting.RunReport(brokerId, reportId, userId, E_ReportExtentScopeT.Default);

            List<Guid> loanIdList = new List<Guid>();

            foreach (var o in report.Flatten())
            {
                loanIdList.Add(o.Key);
            }


            return new XsltExportRequest(userId, loanIdList, mapName, callback, callbackParameters);

        }

        /// <summary>
        /// Generate an export request that will automatically include all loans
        /// in the given custom report.
        /// </summary>
        public static XsltExportRequest CreateFromCustomReport(Guid brokerId, Guid userId, Guid reportId, string mapName,
             IXsltExportCallBack callback, IEnumerable<KeyValuePair<string, string>> callbackParameters, HashSet<Guid> excludeList)
        {
            LendersOffice.Reports.LoanReporting loanReporting = new LendersOffice.Reports.LoanReporting();

            LendersOffice.QueryProcessor.Report report = loanReporting.RunReport(brokerId, reportId, userId, E_ReportExtentScopeT.Default);

            List<Guid> loanIdList = new List<Guid>();

            foreach (var o in report.Flatten())
            {
                if (excludeList != null && excludeList.Contains(o.Key))
                {
                    continue;
                }
                loanIdList.Add(o.Key);
            }


            return new XsltExportRequest(userId, loanIdList, mapName, callback, callbackParameters);

        }
    }
}
