﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;
using System.Xml.Linq;
using System.Collections.Specialized;
using LendersOffice.Email;

namespace LendersOffice.XsltExportReport
{
    public class EmailTemplatingXsltExportCallBack : IXsltExportCallBack
    {
        #region IXsltExportCallBack Members

        public void OperationComplete(Guid brokerId, XsltExportRequest request, XsltExportResult result)
        {
            if (result.HasError)
            {
                Tools.LogErrorWithCriticalTracking("Report Id: " + result.ReportId + " of " + result.ReportName + " failed. Search in PB Log for detail message.");
                return;
            }

            #region Convert CSV xml to a list of NameValueCollection.
            XDocument xdoc = XDocument.Load(result.OutputFileLocation);

            List<string> headerList = null;

            List<NameValueCollection> rowList = new List<NameValueCollection>();
            

            foreach (XElement rowElement in xdoc.Root.Elements())
            {
                if (headerList == null)
                {
                    headerList = new List<string>();
                    foreach (XElement colElement in rowElement.Elements())
                    {
                        headerList.Add(colElement.Attribute("v").Value);
                    }
                }
                else
                {
                    int colIndex = 0;
                    NameValueCollection nv = new NameValueCollection();
                    foreach (XElement colElement in rowElement.Elements())
                    {
                        nv[headerList[colIndex]] = colElement.Attribute("v").Value;
                        colIndex++;
                    }
                    rowList.Add(nv);
                }

            }
            #endregion

            string templateEmailXml = request.GetCallBackParameter("TemplateEmailXml");

            XDocument templateXDoc = XDocument.Parse(templateEmailXml);

            string fromFormat = templateXDoc.Root.Element("from").Value;
            string toFormat = templateXDoc.Root.Element("to").Value;
            string subjectFormat = templateXDoc.Root.Element("subject").Value;
            string messageFormat = templateXDoc.Root.Element("message").Value;

            bool isTurnOffDefaultDisclaimer = false;

            if (templateXDoc.Root.Attribute("turn_off_disclaimer") != null)
            {
                isTurnOffDefaultDisclaimer = templateXDoc.Root.Attribute("turn_off_disclaimer").Value.Equals("true", StringComparison.OrdinalIgnoreCase);
            }

            bool isHtmlEmail = false;

            if (templateXDoc.Root.Attribute("is_html_email") != null) 
            {
                isHtmlEmail = templateXDoc.Root.Attribute("is_html_email").Value.Equals("true", StringComparison.OrdinalIgnoreCase);
            }
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("EmailTemplatingXsltExportCallBack. ReportName=[" + result.ReportName + "]");


            foreach (NameValueCollection nv in rowList)
            {
                CBaseEmail email = new CBaseEmail(brokerId);
                email.Subject = ApplyTransformation(subjectFormat, nv);
                email.From = ApplyTransformation(fromFormat, nv);
                email.To = ApplyTransformation(toFormat, nv);
                email.Message = ApplyTransformation(messageFormat, nv);

                if (isTurnOffDefaultDisclaimer)
                {
                    email.DisclaimerType = LendersOffice.Common.E_DisclaimerType.NONE;
                }

                if (isHtmlEmail)
                {
                    email.IsHtmlEmail = true;
                }
                if (string.IsNullOrEmpty(email.To))
                {
                    continue;
                }
                email.Send();
                sb.AppendLine("    From=[" + email.From + "], To=[" + email.To + "], Subject=[" + email.Subject + "]");
            }
            Tools.LogInfo(sb.ToString());
        }

        #endregion

        private string ApplyTransformation(string format, NameValueCollection nv)
        {
            // 6/26/2013 dd - Search and replace all {{keyword}} with actual content.
            // keyword is in nv.Key

            foreach (string key in nv.Keys)
            {
                format = format.Replace("{{" + key + "}}", nv[key]);
            }
            return format;
        }

    }
}
