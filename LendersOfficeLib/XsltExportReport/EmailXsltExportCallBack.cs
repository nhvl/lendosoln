﻿namespace LendersOffice.XsltExportReport
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using DataAccess;
    using LendersOffice.Drivers.Gateways;
    using LendersOffice.Email;

    public class EmailXsltExportCallBack : IXsltExportCallBack
    {
        #region IXsltExportCallBack Members

        public void OperationComplete(Guid brokerId, XsltExportRequest request, XsltExportResult result)
        {
            
            if (result.HasError)
            {
                Tools.LogErrorWithCriticalTracking("Report Id: " + result.ReportId + " of " + result.ReportName + " failed. Search in PB Log for detail message.");
                return;
            }

            string subject = request.GetCallBackParameter("Subject");
            string from = request.GetCallBackParameter("From");
            string to = request.GetCallBackParameter("To");
            string message = request.GetCallBackParameter("Message");

            byte[] data = BinaryFileHelper.ReadAllBytes(result.OutputFileLocation);

            CBaseEmail email = new CBaseEmail(brokerId);
            email.Subject = subject;
            email.From = from;
            email.To = to;
            email.Message = message;
            email.AddAttachment(request.OutputFileName, data);
            email.Send();
        }

        #endregion
    }
}
