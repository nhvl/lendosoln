﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendersOffice.Constants;
using System.Messaging;
using CommonProjectLib.Common;
using System.Xml.Linq;

namespace LendersOffice.XsltExportReport
{
    public static class XsltExportRequestQueue
    {
        public static void Send(XsltExportRequest item)
        {
            if (null == item)
            {
                return;
            }

            if (string.IsNullOrEmpty(ConstStage.MSMQ_XsltReportRequest))
            {
                return;
            }

            Drivers.Gateways.MessageQueueHelper.SendXML(ConstStage.MSMQ_XsltReportRequest, null, item.ToXDocument());
        }

        public static XsltExportRequest Receive()
        {
            if (string.IsNullOrEmpty(ConstStage.MSMQ_XsltReportRequest))
            {
                return null;
            }

            using (var queue = Drivers.Gateways.MessageQueueHelper.PrepareForXML(ConstStage.MSMQ_XsltReportRequest, false))
            {
                try
                {
                    DateTime arrivalTime;
                    XDocument xdoc = Drivers.Gateways.MessageQueueHelper.ReceiveXML(queue, null, out arrivalTime);
                    if (xdoc == null)
                    {
                        return null; // same behavior as MessageQueueException.MessageQueueErrorCode == MessageQueueErrorCode.IOTimeout
                    }

                    return XsltExportRequest.Parse(xdoc);
                }
                catch (MessageQueueException e)
                {
                    if (e.MessageQueueErrorCode == MessageQueueErrorCode.IOTimeout)
                    {
                        return null;
                    }
                    throw;
                }
            }
        }
    }
}
