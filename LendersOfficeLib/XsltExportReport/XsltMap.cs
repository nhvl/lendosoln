﻿namespace LendersOffice.XsltExportReport
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Xml.Linq;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Constants;

    public enum XsltMapItemSupportModeT
    {
        MultipleLoansOnly = 0,
        SingleLoanOnly = 1,
        MultipleOrSingleLoan = 2
    }
    public class XsltMapItem
    {
        public string Name { get; private set; }
        public string Description { get; private set; }
        public string OutputFileName { get; private set; }
        
        public string CustomFieldId { get; private set; }
        public string CustomFieldDescription { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the UTF8 byte order mark should be removed after xsl transform.
        /// </summary>
        public bool StripUTF8BOM { get; private set; }

        public bool IsActive { get; private set; }
        public string ExportUrl { get; private set; }
        public string ExportContentType { get; private set; }
        public XsltMapItemSupportModeT SupportModeT { get; private set; }

        public IEnumerable<string> XslFileList { get; private set; }

        // 8/14/2013 dd - Contains a list of customer code that have access to this report.
        //                If list is empty then every broker will have access to this.
        private List<string> CustomerCodeList = new List<string>();
        internal XsltMapItem(XElement el)
        {
            this.Name = el.Attribute("name").Value;

            this.Description = el.Attribute("desc").Value;
            this.OutputFileName = el.Attribute("output_name").Value;
            this.IsActive = el.Attribute("active").Value.Equals("true", StringComparison.OrdinalIgnoreCase);

            List<string> list = new List<string>();

            foreach (XElement item in el.Elements("item"))
            {
                list.Add(item.Attribute("xsl").Value);
            }

            this.XslFileList = list;

            if(el.Attribute("custom_field_id")!=null)
                this.CustomFieldId = el.Attribute("custom_field_id").Value;

            if (el.Attribute("custom_field_desc") != null)
            {
                this.CustomFieldDescription = el.Attribute("custom_field_desc").Value;
            }
            if (el.Attribute("customer_code_list") != null)
            {
                string[] parts = el.Attribute("customer_code_list").Value.Split(',');

                foreach (var o in parts)
                {
                    if (string.IsNullOrEmpty(o) == false)
                    {
                        CustomerCodeList.Add(o.TrimWhitespaceAndBOM().ToUpper());
                    }
                }
            }

            if (el.Attribute("export_url") != null)
            {
                this.ExportUrl = el.Attribute("export_url").Value;
            }
            if (el.Attribute("export_content_type") != null)
            {
                this.ExportContentType = el.Attribute("export_content_type").Value;
            }

            if (el.Attribute("support_mode") != null)
            {
                string v = el.Attribute("support_mode").Value;

                if (string.IsNullOrEmpty(v))
                {
                    this.SupportModeT = XsltMapItemSupportModeT.MultipleLoansOnly;
                }
                else if (v == "SingleLoanOnly")
                {
                    this.SupportModeT = XsltMapItemSupportModeT.SingleLoanOnly;
                }
                else if (v == "MultipleLoansOnly")
                {
                    this.SupportModeT = XsltMapItemSupportModeT.MultipleLoansOnly;
                }
                else if (v == "MultipleOrSingleLoan")
                {
                    this.SupportModeT = XsltMapItemSupportModeT.MultipleOrSingleLoan;
                }
                else
                {
                    throw CBaseException.GenericException("Unknown value for support_mode. Value=[" + v + "]");
                }
            }

            if (el.Attribute("strip_utf8_bom") != null)
            {
                this.StripUTF8BOM = el.Attribute("strip_utf8_bom").Value.Equals("true", StringComparison.OrdinalIgnoreCase);
            }
            else
            {
                this.StripUTF8BOM = false;
            }
        }

        public bool IsAllow(BrokerDB brokerDB)
        {
            if (CustomerCodeList.Count == 0)
            {
                return true;
            }
            return CustomerCodeList.Contains(brokerDB.CustomerCode, StringComparer.OrdinalIgnoreCase);
        }

        public bool IsAllowMultipleLoans
        {
            get
            {
                switch (this.SupportModeT)
                {
                    case XsltMapItemSupportModeT.MultipleLoansOnly:
                    case XsltMapItemSupportModeT.MultipleOrSingleLoan:
                        return true;
                    case XsltMapItemSupportModeT.SingleLoanOnly:
                        return false;
                    default:
                        throw new UnhandledEnumException(this.SupportModeT);
                }
            }
        }

        public bool IsAllowSingleLoan
        {
            get
            {
                switch (this.SupportModeT)
                {
                    case XsltMapItemSupportModeT.MultipleLoansOnly:
                        return false;
                    case XsltMapItemSupportModeT.SingleLoanOnly:
                    case XsltMapItemSupportModeT.MultipleOrSingleLoan:
                        return true;
                    default:
                        throw new UnhandledEnumException(this.SupportModeT);
                }
            }
        }
    }
    public static class XsltMap
    {
        private static object s_lock = new object();
        private static DateTime s_fileVersion = DateTime.MinValue;

        private static Dictionary<string, XsltMapItem> s_mapTable = null;

        private static string XsltMapFilePath
        {
            get { return ConstSite.ExportXsltFilePath + "xslt_map.xml"; }
        }

        private static void Initialize()
        {
            FileInfo fi = new FileInfo(XsltMapFilePath);

            if (s_mapTable != null && fi.LastWriteTime == s_fileVersion)
            {
                return;
            }

            s_mapTable = new Dictionary<string, XsltMapItem>(StringComparer.OrdinalIgnoreCase);

            XDocument xdoc = XDocument.Load(XsltMapFilePath);

            foreach (XElement mapElement in xdoc.Root.Elements("map"))
            {
                XsltMapItem item = new XsltMapItem(mapElement);
                try
                {
                    s_mapTable.Add(item.Name, item);
                }
                catch (ArgumentException)
                {
                    throw CBaseException.GenericException("Duplicate map found: " + item.Name);
                }
            }

            fi = new FileInfo(XsltMapFilePath);
            s_fileVersion = fi.LastWriteTime;
        }

        public static XsltMapItem Get(string mapName)
        {
            lock (s_lock)
            {
                Initialize();

                try
                {
                    return s_mapTable[mapName];
                }
                catch (KeyNotFoundException)
                {
                    throw new NotFoundException("Could not locate map [" + mapName + "]");
                }
            }
        }
        public static IEnumerable<XsltMapItem> ListActive(BrokerDB broker)
        {
            lock (s_lock)
            {
                Initialize();

                List<XsltMapItem> list = new List<XsltMapItem>();

                foreach (var o in s_mapTable.Values)
                {
                    if (o.IsActive && o.IsAllow(broker) && o.IsAllowMultipleLoans)
                    {
                        list.Add(o);
                    }
                }
                return list;
            }
        }
        public static IEnumerable<XsltMapItem> ListActiveForSingleLoan(BrokerDB broker)
        {
            lock (s_lock)
            {
                Initialize();

                List<XsltMapItem> list = new List<XsltMapItem>();

                foreach (var o in s_mapTable.Values)
                {
                    if (o.IsActive && o.IsAllow(broker) && o.IsAllowSingleLoan)
                    {
                        list.Add(o);
                    }
                }
                return list;
            }
        }
    }
}
