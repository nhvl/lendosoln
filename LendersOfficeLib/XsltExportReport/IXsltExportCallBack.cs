﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LendersOffice.XsltExportReport
{
    public interface IXsltExportCallBack
    {
        void OperationComplete(Guid brokerId, XsltExportRequest request, XsltExportResult result);
    }
}
