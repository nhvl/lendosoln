﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.XPath;
using System.Text.RegularExpressions;
using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.XsltExportReport
{
    public class XsltExportExtensionObject
    {

        private bool IsEquals(string a, string b)
        {
            if (string.IsNullOrEmpty(a))
            {
                return string.IsNullOrEmpty(b);
            }

            return a.Equals(b, StringComparison.OrdinalIgnoreCase);
        }
        public string NamespaceUri
        {
            get { return "urn:lqb-xslt"; }
        }
        public string Key(XPathNavigator nodeSet, string key)
        {
            if (string.IsNullOrEmpty(key))
            {
                return string.Empty;
            }

            XmlNamespaceManager mgr = new XmlNamespaceManager(nodeSet.NameTable);
            mgr.AddNamespace("a", "http://www.w3.org/1999/xhtml");

            var o = nodeSet.SelectSingleNode("//a:item[@key='" + key + "']", mgr);

            if (o == null)
            {
                throw new KeyNotFoundException("[" + key + "] is not found.");
            }
            return o.GetAttribute("value", "");

        }
        public string Key(XPathNavigator nodeSet, string name, string key)
        {
            if (string.IsNullOrEmpty(name) || string.IsNullOrEmpty(key))
            {
                return string.Empty;
            }

            string ns = "http://www.w3.org/1999/xhtml";

            var nodeIterator = nodeSet.SelectDescendants("item", ns, false);

            bool matchName = true;
            bool matchKey = false;
            while (nodeIterator.MoveNext())
            {
                XPathNavigator currentNode = nodeIterator.Current;

                string k = currentNode.GetAttribute("key", "");
                string v = currentNode.GetAttribute("value", "");

                if (matchName && k.Equals(name, StringComparison.OrdinalIgnoreCase) && string.IsNullOrEmpty(v))
                {
                    // Match the name section.
                    matchName = false;
                    matchKey = true;
                }
                else if (matchKey && k.Equals(key, StringComparison.OrdinalIgnoreCase))
                {
                    return v;
                }
                else if (matchKey && string.IsNullOrEmpty(v))
                {
                    // Cross over to next name section.
                    break;
                }
            }
            return string.Empty;
        }

        #region Private Helper Functions for Map() function
        private string[] MapColumnNameList = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J" };
        private string GetColumnName(int columnNumber)
        {
            if (columnNumber < 0 || columnNumber >= MapColumnNameList.Length)
            {
                throw new CBaseException(columnNumber + " is not supported.", columnNumber + " is not supported. Only support up to " + MapColumnNameList.Length);
            }
            return MapColumnNameList[columnNumber];
        }
        private bool IsInputMatch(XPathNavigator currentNode, string columnName, string expectedValue)
        {
            string v = currentNode.GetAttribute(columnName, "");
            if (string.IsNullOrEmpty(v) || v.Equals("<Any>", StringComparison.OrdinalIgnoreCase))
            {
                // 5/13/2013 dd - The wild card will be <Any>. After the release remove the empty string as wild card.
                // If value is blank for this column then treat it as wild card.
                return true;
            }
            else if (v.Equals("<Blank>", StringComparison.OrdinalIgnoreCase))
            {
                // 6/24/2013 dd - Explicitly test for blank.
                return string.IsNullOrEmpty(expectedValue);
            }
            else if (v.Equals("<Omit>", StringComparison.OrdinalIgnoreCase))
            {
                // 6/24/2013 dd - Always return false.
                return false;
            }
            if (v.StartsWith("<In ", StringComparison.OrdinalIgnoreCase) && v.EndsWith(">"))
            {
                return IsIn(v, expectedValue);
            }
            if (v.StartsWith("<Not_In ", StringComparison.OrdinalIgnoreCase) && v.EndsWith(">"))
            {
                return IsNotIn(v, expectedValue);
            }
            if (v.StartsWith("<Regex", StringComparison.OrdinalIgnoreCase) && v.EndsWith(">"))
            {
                return IsRegExMatch(v, expectedValue);
            }
            if (v.StartsWith("<Range ", StringComparison.OrdinalIgnoreCase) &&
                v.EndsWith(">"))
            {
                return IsInRange(v, expectedValue);

            }
            return IsEquals(v, expectedValue);
        }

        public bool IsRegExMatch(string expr, string value)
        {
            string regexPattern = "<regex\\s*\"([^\"]+)\"\\s*>";

            string pattern = string.Empty;

            Match m = Regex.Match(expr, regexPattern, RegexOptions.IgnoreCase);

            if (m.Success)
            {
                pattern = m.Groups[1].Value;
                return Regex.IsMatch(value, pattern, RegexOptions.IgnoreCase);
            }

            return false;

            
        }
        private bool IsIn(string expr, string value)
        {
            IEnumerable<string> inputList = GetOpINParameter(expr);

            foreach (var s in inputList)
            {
                if (s.Equals(value, StringComparison.OrdinalIgnoreCase))
                {
                    return true;
                }
            }
            return false;
        }
        private bool IsNotIn(string expr, string value)
        {
            IEnumerable<string> inputList = GetOpNOTINParameter(expr);

            foreach (var s in inputList)
            {
                if (s.Equals(value, StringComparison.OrdinalIgnoreCase))
                {
                    return false;
                }
            }
            return true;
        }
        /// <summary>
        /// Parse all the inputs between &ltIN and &gt;
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        private IEnumerable<string> GetOpINParameter(string s)
        {
            // 6/24/2013 dd - Example: <IN "Foo" "Bar" >

            List<string> list = new List<string>();

            // 6/24/2013 dd - input must be between double quote (there is no nested double quote).
            //                input can only contains these characters a-z 0-9 space . ' - $ ,
            // 9/16/2013 dd - String matching will be case insensitive
            Regex regex = new Regex("<IN (\"([a-zA-Z0-9 _.'\\-$,]+)\"\\s*)+>", RegexOptions.IgnoreCase);

            Match m = regex.Match(s);
            while (m.Success)
            {
                Group g = m.Groups[2];
                foreach (Capture c in g.Captures)
                {
                    list.Add(c.Value);
                }
                m = m.NextMatch();
            }
            return list;
        }
        /// <summary>
        /// Parse all the inputs between &ltIN and &gt;
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        private IEnumerable<string> GetOpNOTINParameter(string s)
        {
            // 6/24/2013 dd - Example: <IN "Foo" "Bar" >

            List<string> list = new List<string>();

            // 6/24/2013 dd - input must be between double quote (there is no nested double quote).
            //                input can only contains these characters a-z 0-9 space . ' - $ ,
            // 9/16/2013 dd - String matching will be case insensitive
            Regex regex = new Regex("<NOT_IN (\"([a-zA-Z0-9 _.'\\-$,]+)\"\\s*)+>", RegexOptions.IgnoreCase);

            Match m = regex.Match(s);
            while (m.Success)
            {
                Group g = m.Groups[2];
                foreach (Capture c in g.Captures)
                {
                    list.Add(c.Value);
                }
                m = m.NextMatch();
            }
            return list;
        }
        /// <summary>
        /// 6/20/2013 dd - Handle Range lower upper
        /// Range is inclusive check.
        /// lower is numeric value or min
        /// upper is numeric value or max
        /// i.e:  &lt;RANGE 0 4&gt;
        /// &lt;RANGE 9 MAX&gt;
        /// </summary>
        /// <param name="rangeExpr"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool IsInRange(string rangeExpr, string value)
        {
            string[] parts = rangeExpr.Substring("<Range ".Length, rangeExpr.Length - ("<Range ".Length + 1)).TrimWhitespaceAndBOM().Split(' ');
            if (parts.Length != 2)
            {
                return false;
            }

            decimal min = decimal.MinValue;
            decimal max = decimal.MaxValue;

            if (parts[0].Equals("min", StringComparison.OrdinalIgnoreCase) == false)
            {
                if (decimal.TryParse(parts[0], out min) == false)
                {
                    min = decimal.MinValue;
                }
            }
            if (parts[1].Equals("max", StringComparison.OrdinalIgnoreCase) == false)
            {
                if (decimal.TryParse(parts[1], out max) == false)
                {
                    max = decimal.MaxValue;
                }
            }

            decimal v;
            if (decimal.TryParse(value, out v))
            {
                return v >= min && v <= max;
            }
            return false;

        }
        private bool IsMapBlankLine(XPathNavigator currentNode)
        {
            foreach (string columnName in MapColumnNameList)
            {
                if (string.IsNullOrEmpty(currentNode.GetAttribute(columnName, "")) == false)
                {
                    return false;
                }
            }
            return true;
        }
        private bool IsInputMatch(XPathNavigator currentNode, string[] expectedInputList)
        {
            if (expectedInputList.Length > MapColumnNameList.Length - 1)
            {
                throw new CBaseException(expectedInputList.Length + " is exceed max support limit.", expectedInputList.Length + " is exceed max support limit. Only support up to " + (MapColumnNameList.Length - 1));
            }

            for (int i = 0; i < expectedInputList.Length; i++)
            {
                string columnName = GetColumnName(i);

                if (IsInputMatch(currentNode, columnName, expectedInputList[i]) == false)
                {
                    return false;
                }
            }
            return true;
        }
        private string MapImpl(XPathNavigator nodeSet, string name, string[] expectedInputList)
        {
            if (string.IsNullOrEmpty(name))
            {
                return string.Empty;
            }
            string ns = "http://www.w3.org/1999/xhtml";

            var nodeIterator = nodeSet.SelectDescendants("item", ns, false);

            bool matchName = true;
            bool matchKey = false;
            while (nodeIterator.MoveNext())
            {
                XPathNavigator currentNode = nodeIterator.Current;

                if (IsMapBlankLine(currentNode))
                {
                    continue;
                }
                string k = currentNode.GetAttribute("A", "");
                string v = currentNode.GetAttribute("B", "");

                if (matchName && IsEquals(k, "KEY_NAME") && IsEquals(v, name))
                {
                    // Match the name section.
                    matchName = false;
                    matchKey = true;
                }
                else if (matchKey && IsInputMatch(currentNode, expectedInputList))
                {
                    string resultColumnName = GetColumnName(expectedInputList.Length);
                    return currentNode.GetAttribute(resultColumnName, "");
                }
                else if (matchKey && IsEquals(k, "KEY_NAME"))
                {
                    // Cross over to next name section.
                    break;
                }
            }
            if (matchName == true)
            {
                // 5/13/2013 dd - This mean the name is not found in LOOKUP.
                throw new NotFoundException("KEY_NAME=[" + name + "] is not found in lookup table.");
            }
            return string.Empty;
        }
        #endregion

        #region Map function
        public string Map(XPathNavigator nodeSet, string name, string input0)
        {
            return MapImpl(nodeSet, name, new string[] { input0 });
        }

        public string Map(XPathNavigator nodeSet, string name, string input0, string input1)
        {
            return MapImpl(nodeSet, name, new string[] { input0, input1 });

        }
        public string Map(XPathNavigator nodeSet, string name, string input0, string input1, string input2)
        {
            return MapImpl(nodeSet, name, new string[] { input0, input1, input2 });
        }

        public string Map(XPathNavigator nodeSet, string name, string input0, string input1, string input2, string input3)
        {
            return MapImpl(nodeSet, name, new string[] { input0, input1, input2, input3 });
        }
        public string Map(XPathNavigator nodeSet, string name, string input0, string input1, string input2, string input3, string input4)
        {
            return MapImpl(nodeSet, name, new string[] { input0, input1, input2, input3, input4 });
        }
        public string Map(XPathNavigator nodeSet, string name, string input0, string input1, string input2, string input3, string input4, string input5)
        {
            return MapImpl(nodeSet, name, new string[] { input0, input1, input2, input3, input4, input5 });
        }
        public string Map(XPathNavigator nodeSet, string name, string input0, string input1, string input2, string input3, string input4, string input5, string input6)
        {
            return MapImpl(nodeSet, name, new string[] { input0, input1, input2, input3, input4, input5, input6 });
        }
        public string Map(XPathNavigator nodeSet, string name, string input0, string input1, string input2, string input3, string input4, string input5, string input6, string input7)
        {
            return MapImpl(nodeSet, name, new string[] { input0, input1, input2, input3, input4, input5, input6, input7 });
        }
        public string Map(XPathNavigator nodeSet, string name, string input0, string input1, string input2, string input3, string input4, string input5, string input6, string input7, string input8)
        {
            return MapImpl(nodeSet, name, new string[] { input0, input1, input2, input3, input4, input5, input6, input7, input8 });
        }
        public string Map(XPathNavigator nodeSet, string name, string input0, string input1, string input2, string input3, string input4, string input5, string input6, string input7, string input8, string input9)
        {
            return MapImpl(nodeSet, name, new string[] { input0, input1, input2, input3, input4, input5, input6, input7, input8, input9 });
        }
        #endregion

        #region Map_Eval function
        public string Map_Eval(XPathNavigator current, XPathNavigator nodeSet, string name, string input0)
        {
            return Eval(current,  MapImpl(nodeSet, name, new string[] { input0 }));
        }

        public string Map_Eval(XPathNavigator current, XPathNavigator nodeSet, string name, string input0, string input1)
        {
            return Eval(current, MapImpl(nodeSet, name, new string[] { input0, input1 }));

        }
        public string Map_Eval(XPathNavigator current, XPathNavigator nodeSet, string name, string input0, string input1, string input2)
        {
            return Eval(current, MapImpl(nodeSet, name, new string[] { input0, input1, input2 }));
        }

        public string Map_Eval(XPathNavigator current, XPathNavigator nodeSet, string name, string input0, string input1, string input2, string input3)
        {
            return Eval(current, MapImpl(nodeSet, name, new string[] { input0, input1, input2, input3 }));
        }
        public string Map_Eval(XPathNavigator current, XPathNavigator nodeSet, string name, string input0, string input1, string input2, string input3, string input4)
        {
            return Eval(current, MapImpl(nodeSet, name, new string[] { input0, input1, input2, input3, input4 }));
        }
        public string Map_Eval(XPathNavigator current, XPathNavigator nodeSet, string name, string input0, string input1, string input2, string input3, string input4, string input5)
        {
            return Eval(current, MapImpl(nodeSet, name, new string[] { input0, input1, input2, input3, input4, input5 }));
        }
        public string Map_Eval(XPathNavigator current, XPathNavigator nodeSet, string name, string input0, string input1, string input2, string input3, string input4, string input5, string input6)
        {
            return Eval(current, MapImpl(nodeSet, name, new string[] { input0, input1, input2, input3, input4, input5, input6 }));
        }
        public string Map_Eval(XPathNavigator current, XPathNavigator nodeSet, string name, string input0, string input1, string input2, string input3, string input4, string input5, string input6, string input7)
        {
            return Eval(current, MapImpl(nodeSet, name, new string[] { input0, input1, input2, input3, input4, input5, input6, input7 }));
        }
        public string Map_Eval(XPathNavigator current, XPathNavigator nodeSet, string name, string input0, string input1, string input2, string input3, string input4, string input5, string input6, string input7, string input8)
        {
            return Eval(current, MapImpl(nodeSet, name, new string[] { input0, input1, input2, input3, input4, input5, input6, input7, input8 }));
        }
        public string Map_Eval(XPathNavigator current, XPathNavigator nodeSet, string name, string input0, string input1, string input2, string input3, string input4, string input5, string input6, string input7, string input8, string input9)
        {
            return Eval(current, MapImpl(nodeSet, name, new string[] { input0, input1, input2, input3, input4, input5, input6, input7, input8, input9 }));
        }

        #endregion
        public string If(bool test, string true_string, string false_string)
        {
            return test ? true_string : false_string;
        }

        public string FormatDate(string datetime, string format)
        {
            DateTime dt = DateTime.MinValue;

            if (DateTime.TryParse(datetime, out dt))
            {
                return dt.ToString(format);
            }
            return string.Empty;
        }
        public string FormatDecimal(string value, string format)
        {
            decimal v = decimal.MinValue;

            if (decimal.TryParse(value, out v))
            {
                return v.ToString(format);
            }
            return string.Empty;
        }

        public string Left(string str, int length)
        {
            if (string.IsNullOrEmpty(str))
            {
                return string.Empty;
            }
            if (length < 0)
            {
                return str;
            }

            return str.Substring(0, str.Length < length ? str.Length : length);
        }

        public string Right(string str, int length)
        {
            if (string.IsNullOrEmpty(str))
            {
                return string.Empty;
            }
            if (length < 0)
            {
                return str;
            }

            int startIndex = str.Length < length ? 0 : str.Length - length;

            return str.Substring(startIndex);
        }
        public string Substring(string str, int startIndex, int length)
        {
            if (string.IsNullOrEmpty(str))
            {
                return string.Empty;
            }
            if (startIndex < 0)
            {
                startIndex = 0;
            }
            if (length < 0)
            {
                length = 0;
            }

            return str.Substring(Math.Min(startIndex, str.Length),
                Math.Min(length, Math.Max(str.Length - startIndex, 0)));
        }
        public string SafeCsvString(string s)
        {
            return s.SafeCsvString();
        }
        public string FixedFormatValue(string s, string length, string align, string pad_character)
        {
            if (string.IsNullOrEmpty(length))
            {
                throw new Exception("length is required for fixed format.");
            }
            int _length = 0;
            if (int.TryParse(length, out _length) == false)
            {
                throw new Exception("length=[" + length + "] is not a valid number.");
            }

            string alignMode = "left"; // Default Align Mode
            if (align == "right")
            {
                alignMode = "right";
            }

            char padding = ' '; // Default padding character.

            if (string.IsNullOrEmpty(pad_character) == false)
            {
                padding = pad_character[0]; // Get First Character.
            }

            if (s.Length > _length)
            {
                return s.Substring(0, _length);
            }
            if (alignMode == "left")
            {
                return s.PadRight(_length, padding);
            }
            else
            {
                return s.PadLeft(_length, padding);

            }
        }


        public string RegexReplace(string input, string pattern, string replacement)
        {
            if (string.IsNullOrEmpty(input))
            {
                return string.Empty;
            }
            return Regex.Replace(input, pattern, replacement);
        }

        public bool RegexIsMatch(string input, string pattern)
        {
            return Regex.IsMatch(input, pattern);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public bool GfeItemProps_Apr(string input)
        {
            int props = 0;
            int.TryParse(input, out props);

            return LosConvert.GfeItemProps_Apr(props);
        }

        /// <summary>
        /// </summary>
        /// <param name="input"></param>
        /// <returns>
        /// 0: Paid by borrower, out of pocket.
        /// 1: Paid by borrower but he/she will finance it.
        /// 2: Paid by seller
        /// 3: Paid by lender
        /// 4: Paid by broker
        /// </returns>
        public string GfeItemProps_Payer(string input)
        {
            int props = 0;
            int.TryParse(input, out props);

            return LosConvert.GfeItemProps_Payer(props).ToString() ;
        }

        public bool GfeItemProps_Dflp(string input)
        {
            int props = 0;
            int.TryParse(input, out props);

            return LosConvert.GfeItemProps_Dflp(props);

        }
        public bool GfeItemProps_ToBr(string input)
        {
            int props = 0;
            int.TryParse(input, out props);

            return LosConvert.GfeItemProps_ToBr(props);
        }
        public bool GfeItemProps_Poc(string input)
        {
            int props = 0;
            int.TryParse(input, out props);

            return LosConvert.GfeItemProps_Poc(props);

        }
        public bool GfeItemProps_FhaAllow(string input)
        {
            int props = 0;
            int.TryParse(input, out props);

            return LosConvert.GfeItemProps_FhaAllow(props);

        }
        public bool GfeItemProps_BF(string input)
        {
            int props = 0;
            int.TryParse(input, out props);

            return LosConvert.GfeItemProps_BF(props);

        }
        #region SumDecimal functions
        public string SumDecimal(string num0, string num1)
        {
            return SumDecimalImpl(num0, num1);
        }

        public string SumDecimal(string num0, string num1, string num2)
        {
            return SumDecimalImpl(num0, num1, num2);
        }

        public string SumDecimal(string num0, string num1, string num2, string num3)
        {
            return SumDecimalImpl(num0, num1, num2, num3);
        }

        public string SumDecimal(string num0, string num1, string num2, string num3, string num4)
        {
            return SumDecimalImpl(num0, num1, num2, num3, num4);
        }
        private string SumDecimalImpl(params string[] list)
        {
            decimal total = 0;

            foreach (var o in list)
            {
                decimal v = 0;
                if (decimal.TryParse(o, out v) == true)
                {
                    total += v;
                }
            }
            return total.ToString();
        }
        #endregion
        #region SumInt functions
        public string SumInt(string num0, string num1)
        {
            return SumIntImpl(num0, num1);
        }

        public string SumInt(string num0, string num1, string num2)
        {
            return SumIntImpl(num0, num1, num2);
        }

        public string SumInt(string num0, string num1, string num2, string num3)
        {
            return SumIntImpl(num0, num1, num2, num3);
        }

        public string SumInt(string num0, string num1, string num2, string num3, string num4)
        {
            return SumIntImpl(num0, num1, num2, num3, num4);
        }
        private string SumIntImpl(params string[] list)
        {
            int total = 0;

            foreach (var o in list)
            {
                int v = 0;
                if (int.TryParse(o, out v) == true)
                {
                    total += v;
                }
            }
            return total.ToString();
        }
        #endregion

        public string AddMonths(string datetime, int months)
        {
            DateTime dt = DateTime.MinValue;

            if (DateTime.TryParse(datetime, out dt) == false)
            {
                return string.Empty;
            }
            return dt.AddMonths(months).ToString();
        }

        public string AddDays(string datetime, int days)
        {
            DateTime dt = DateTime.MinValue;

            if (DateTime.TryParse(datetime, out dt) == false)
            {
                return string.Empty;
            }
            return dt.AddDays(days).ToString();

        }

        public string Eval(XPathNavigator nodeSet, string xpath)
        {
            XPathNodeIterator result = nodeSet.Evaluate(xpath) as XPathNodeIterator;

            if (result != null)
            {
                if (result.MoveNext())
                {
                    return result.Current.Value;
                }
                
            }
            return string.Empty;
        }
    }
}
