﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace LendersOffice.XsltExportReport
{
    public class XsltExportResult
    {
        public string OutputFileLocation { get; private set; }
        public IEnumerable<string> ErrorList { get; private set; }
        public IEnumerable<string> WarningList { get; private set; }
        public string ReportId { get; private set; }
        public string ReportName { get; private set; }
        public Guid UserId { get; private set; }
        public int ExecutionInMs { get; private set; }
        public DateTime CreatedDate { get; private set; }

        public IEnumerable<Guid> LoanIdList { get; private set; }
        public bool HasError
        {
            get { return ErrorList.Count() > 0; }
        }

        internal XsltExportResult(string reportId, string reportName, Guid userId, IEnumerable<Guid> loanIdList, string outputFileLocation, IEnumerable<string> errorList, IEnumerable<string> warningList, int executionInMs)
        {
            this.OutputFileLocation = outputFileLocation;
            this.UserId = userId;
            this.ErrorList = errorList;
            this.WarningList = warningList;
            this.ReportId = reportId;
            this.ReportName = reportName;
            this.LoanIdList = loanIdList;
            this.CreatedDate = DateTime.Now;
            this.ExecutionInMs = executionInMs;
        }

    }
}
