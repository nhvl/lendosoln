﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;
using LendersOffice.Email;
using LendersOffice.Constants;

namespace LendersOffice.XsltExportReport
{
    public class DefaultXsltExportCallBack : IXsltExportCallBack
    {
        #region IXsltExportCallBack Members

        public void OperationComplete(Guid brokerId, XsltExportRequest request, XsltExportResult result)
        {

            string emailAddress = request.GetCallBackParameter("Email");
            CBaseEmail email = new CBaseEmail(brokerId);
            email.Subject = "Report is ready."; // Feel Free to update the subject.
            email.From = ConstStage.DefaultDoNotReplyAddress;
            email.To = emailAddress;
            email.Message = "Report " + request.ReportName + " is ready. Login to LendingQB to download report.";
            email.Send();


            if (result.HasError == false)
            {
                string field = request.GetCallBackParameter("UpdatedData");
                string fieldId = request.GetCallBackParameter("UpdatedField");

                if (PageDataUtilities.ContainsField(fieldId))
                {
                    StringBuilder warningMessage = new StringBuilder();
                    bool hasError = false;
                    foreach (Guid sLId in request.LoanIdList)
                    {
                        try
                        {
                            CPageData dataLoan = new CPageData(sLId, "DefaultXsltExportCallback", new string[] { fieldId });
                            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
                            CAppData dataApp = dataLoan.GetAppData(0);
                            PageDataUtilities.SetValue(dataLoan, dataApp, fieldId, field);
                            dataLoan.Save();
                        }
                        catch (CBaseException exc)
                        {
                            warningMessage.AppendLine("Unable to set [" + fieldId + "] =[" + field + "] for sLId=[" + sLId + "]. Error=" + exc.ToString());
                            hasError = true;
                        }
                    }
                    if (hasError == true)
                    {
                        Tools.LogError("[DefaultXsltExportCallBack] ReportId:" + request.ReportId + " - " + warningMessage.ToString());
                    }
                }
            }
        }

        #endregion
    }
}
