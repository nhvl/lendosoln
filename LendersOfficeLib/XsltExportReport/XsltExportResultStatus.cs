﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Data.SqlClient;
using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.XsltExportReport
{
    public class XsltExportResultStatusItem
    {
        public XsltExportResultStatusItem(string reportId, string reportName, DateTime createdDate, E_XsltExportResultStatusT status, string outputFileName, String RequestXml)
        {
            this.ReportId = reportId;
            this.ReportName = reportName;
            this.CreatedDate = createdDate;
            this.Status = status;
            this.OutputFileName = outputFileName;
            this.RequestXml = RequestXml;
        }
        public string ReportId { get; private set; }
        public string ReportName { get; private set; }
        public DateTime CreatedDate { get; private set; }
        public string OutputFileName { get; private set; }
        public E_XsltExportResultStatusT Status { get; private set; }
        public string RequestXml;

    }
    public enum E_XsltExportResultStatusT
    {
        Pending = 0,
        Complete = 1,
        Error = 2
    }
    public static class XsltExportResultStatus
    {

        public static void Insert(Guid brokerId, XsltExportRequest request)
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@ReportId", request.ReportId),
                                            new SqlParameter("@UserId", request.UserId),
                                            new SqlParameter("@ReportName", request.ReportName),
                                            new SqlParameter("@RequestXml", request.ToXml()),
                                            new SqlParameter("@OutputFileName", request.OutputFileName)
                                        };

            StoredProcedureHelper.ExecuteNonQuery(brokerId, "XSLT_EXPORT_RESULT_STATUS_InsertRequest", 3, parameters);
        }

        public static void Update(XsltExportResult result)
        {
            E_XsltExportResultStatusT status = E_XsltExportResultStatusT.Complete;

            if (result.HasError)
            {
                status = E_XsltExportResultStatusT.Error;
            }
            else
            {
                // Save the result to FileDB.
                FileDBTools.WriteFile(E_FileDB.Normal, result.ReportId, result.OutputFileLocation);
            }
            Guid brokerId;

            DbConnectionInfo.GetConnectionInfoByUserId(result.UserId, out brokerId);

            SqlParameter[] parameters = {
                                            new SqlParameter("@ReportId", result.ReportId),
                                            new SqlParameter("@UserId", result.UserId),
                                            new SqlParameter("@Status", status),
                                            new SqlParameter("@ExecutionInMs", result.ExecutionInMs)
                                        };
            StoredProcedureHelper.ExecuteNonQuery(brokerId, "XSLT_EXPORT_RESULT_STATUS_UpdateResponse", 3, parameters);
        }

        public static XsltExportResultStatusItem GetResult(Guid brokerId, Guid userId, string reportId)
        {
            if (string.IsNullOrEmpty(reportId))
            {
                throw new ArgumentNullException("reportId cannot be empty");
            }

            SqlParameter[] parameters = {
                                            new SqlParameter("@ReportId", reportId),
                                            new SqlParameter("@UserId", userId)

                                        };
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "XSLT_EXPORT_RESULT_STATUS_RetrieveByReportId", parameters))
            {
                if (reader.Read())
                {
                    string reportName = (string)reader["ReportName"];
                    DateTime createdDate = (DateTime)reader["CreatedDate"];
                    string outputFileName = (string)reader["OutputFileName"];
                    E_XsltExportResultStatusT status = (E_XsltExportResultStatusT)reader["Status"];
                    string requestXml = (string)reader["RequestXml"];


                    return new XsltExportResultStatusItem(reportId, reportName, createdDate, status, outputFileName, requestXml);

                }
            }
            throw new NotFoundException(reportId + " is not found.");
        }

        public static IEnumerable<XsltExportResultStatusItem> ListByUserId(Guid brokerId, Guid userId)
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@UserId", userId)
                                        };
            List<XsltExportResultStatusItem> list = new List<XsltExportResultStatusItem>();
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "XSLT_EXPORT_RESULT_STATUS_ByUserId", parameters))
            {
                while (reader.Read())
                {
                    string reportId = (string)reader["ReportId"];
                    string reportName = (string)reader["ReportName"];
                    DateTime createdDate = (DateTime)reader["CreatedDate"];
                    string outputFileName = (string)reader["OutputFileName"];
                    E_XsltExportResultStatusT status = (E_XsltExportResultStatusT)reader["Status"];
                    string requestXml = (string)reader["RequestXml"];

                    list.Add(new XsltExportResultStatusItem(reportId, reportName, createdDate, status, outputFileName, requestXml));
                }
            }
            return list;
        }
    }
}
