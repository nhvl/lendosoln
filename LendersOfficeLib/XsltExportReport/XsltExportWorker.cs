﻿namespace LendersOffice.XsltExportReport
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Xml;
    using System.Xml.Linq;
    using System.Xml.Xsl;

    using ClosedXML.Excel;
    using Common.SerializationTypes;
    using DataAccess;
    using Integration.UcdDelivery;
    using Integration.UcdDelivery.FannieMae;
    using Integration.UcdDelivery.FreddieMac;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Conversions;
    using LendersOffice.Drivers.Gateways;
    using LendersOffice.ObjLib.Conversions;
    using LendersOffice.ObjLib.Conversions.GinnieNet;
    using LendersOffice.ObjLib.Conversions.ProvidentFunding;
    using LendersOffice.ObjLib.Rolodex;
    using LendersOffice.ObjLib.ToleranceCureDebug;
    using LendersOffice.Security;
    using LqbGrammar.Drivers.SecurityEventLogging;

    public class XsltExportWorker
    {
        #region Private Static Functions
        private string GetFullPath(string xsltName)
        {
            string customizeXslt = ConstSite.ExportXsltFilePath + "xsl\\" + xsltName + "." + BrokerDB.CustomerCode;

            if (FileOperationHelper.Exists(customizeXslt))
            {
                return customizeXslt;
            }

            return ConstSite.ExportXsltFilePath + "xsl\\" + xsltName;
        }
        private string GetFileInXsltFolder(string fileName)
        {
            return ConstSite.ExportXsltFilePath + "xsl\\" + fileName;

        }
        private string GetTempOutputPath(string fileName)
        {
            return ConstApp.TempFolder + "\\" + fileName;
        }
        private string GetIntermediateLoanFileName(string reportId)
        {
            return GetTempOutputPath(reportId + ".loan.xml");
        }
        private string GetIntermediateOutputFile(string reportId, string xslt)
        {
            return GetTempOutputPath(reportId + "." + xslt + ".out");
        }
        private string GetOutputLogFile(string reportId)
        {
            return GetTempOutputPath(reportId + ".summary.txt");
        }
        public HashSet<string> ExtractPossibleLoanFields(IEnumerable<string> xsltList)
        {
            HashSet<string> set = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
            Regex regex = new Regex("([a-zA-Z0-9_]+)");

            foreach (var fileName in xsltList)
            {
                if (IsSpecialXslt(fileName))
                {
                    continue;
                }
                string content = TextFileHelper.ReadFile(GetFullPath(fileName));

                Match m = regex.Match(content);

                while (true)
                {
                    if (m.Success)
                    {
                        set.Add(m.Groups[1].Value);
                    }
                    else
                    {
                        break;
                    }
                    m = m.NextMatch();
                }

            }
            return set;
        }

        private bool IsSpecialXslt(string xslt)
        {
            return xslt.StartsWith("CONVERT_EXCEL:");
        }


        #endregion

        private BrokerDB x_brokerDB = null;
        private BrokerDB BrokerDB
        {
            get
            {
                if (x_brokerDB == null)
                {
                    x_brokerDB = BrokerDB.RetrieveById(UserPrincipal.BrokerId);
                }
                return x_brokerDB;
            }
        }

        private AbstractUserPrincipal x_userPrincipal = null;

        private AbstractUserPrincipal UserPrincipal
        {
            get
            {
                if (x_userPrincipal == null)
                {
                    Guid tmpBrokerId = Guid.Empty;

                    DbConnectionInfo.GetConnectionInfoByUserId(m_userId, out tmpBrokerId);
                    x_userPrincipal = PrincipalFactory.Create(tmpBrokerId, m_userId, "B", true/* allowDuplicateLogin */, true /* isStoreToCookie */, initialPrincipalType, initialUserId);
                }
                return x_userPrincipal;
            }
        }

        private Guid m_userId;

        private XsltExportRequest m_xsltExportRequest = null;

        private bool ContainsSensitiveData = false;

        private InitialPrincipalTypeT initialPrincipalType = InitialPrincipalTypeT.NonInternal;

        private Guid initialUserId = Guid.Empty;

        public bool IsAsync { get; private set; }

        public XsltExportWorker(XsltExportRequest request, AbstractUserPrincipal principal = null) : this(request, true, principal)
        {
        }

        public XsltExportWorker(XsltExportRequest request, bool isAsync, AbstractUserPrincipal principal = null)
        {
            this.m_userId = request.UserId;
            m_xsltExportRequest = request;
            IsAsync = isAsync;

            if (principal != null)
            {
                this.initialPrincipalType = principal.InitialPrincipalType;
                this.initialUserId = principal.InitialUserId;
            }
        }

        public XsltExportResult Execute()
        {
            StringBuilder debug = new StringBuilder();
            Stopwatch sw = Stopwatch.StartNew();
            try
            {
                string reportId = m_xsltExportRequest.ReportId;
                debug.AppendLine(string.Format("[XsltExport] ReportId:{0}", reportId));
                debug.AppendLine();
                debug.AppendLine("Request");
                debug.AppendLine(m_xsltExportRequest.ToXml());

                IEnumerable<string> xsltList = this.m_xsltExportRequest.MapItem.XslFileList;

                XsltExportResult specialExportResult = this.RunNonXsltSpecialExport(reportId, debug);
                if (specialExportResult != null)
                {
                    return specialExportResult;
                }

                string inputFile = GetIntermediateLoanFileName(reportId);
                string outputFile = string.Empty;
                Stopwatch sw0 = new Stopwatch();
                sw0.Start();
                GenerateLoanData(m_xsltExportRequest.UserId, m_xsltExportRequest.LoanIdList, xsltList, reportId);
                sw0.Stop();
                debug.AppendLine("Load up " + m_xsltExportRequest.LoanIdList.Count() + " loans in " + sw0.ElapsedMilliseconds + "ms.");

                sw0.Reset();
                sw0.Start();
                foreach (var xslt in xsltList)
                {
                    if (!FileOperationHelper.Exists(inputFile))
                    {
                        m_errorList.Add("There is no input file. " + inputFile);
                        break;
                    }

                    if (IsSpecialXslt(xslt))
                    {
                        outputFile = GetIntermediateOutputFile(reportId, Guid.NewGuid().ToString());
                        ProcessSpecialXslt(xslt, inputFile, outputFile);
                        inputFile = outputFile;
                    }
                    else
                    {
                        outputFile = GetIntermediateOutputFile(reportId, xslt);

                        XsltArgumentList xsltArgument = new XsltArgumentList();
                        xsltArgument.AddParam("CustomerCode", "", BrokerDB.CustomerCode);
                        XsltExportExtensionObject extensionObject = new XsltExportExtensionObject();
                        xsltArgument.AddExtensionObject(extensionObject.NamespaceUri, extensionObject);

                        XslTransformHelper.TransformToFile(GetFullPath(xslt), inputFile, outputFile, xsltArgument, XsltSettings.TrustedXslt);

                        inputFile = outputFile;
                    }
                }

                if (xsltList.Count() > 0 && this.m_xsltExportRequest.MapItem.StripUTF8BOM)
                {
                    string bomlessFile = GetIntermediateOutputFile(reportId + Guid.NewGuid().ToString(), xsltList.Last());

                    if (Utilities.CopyFileWithoutUTF8BOM(outputFile, bomlessFile))
                    {
                        inputFile = outputFile = bomlessFile;
                    }
                }

                sw0.Stop();
                debug.AppendLine("Perform Transformation in " + sw0.ElapsedMilliseconds + "ms.");

                debug.AppendLine();
                debug.AppendLine("Response:");
                using (StreamWriter stream = new StreamWriter(GetOutputLogFile(reportId)))
                {
                    stream.WriteLine("Executed in " + sw.ElapsedMilliseconds + "ms.");
                    stream.WriteLine(string.Format("{0} warning. {1} error.", m_warningList.Count, m_errorList.Count));
                    if (m_warningList.Count > 0)
                    {
                        debug.AppendLine("Warning:");
                        stream.WriteLine("Warning:");
                        stream.WriteLine();
                        foreach (var s in m_warningList)
                        {
                            debug.AppendLine(s);
                            stream.WriteLine(s);
                        }
                        debug.AppendLine();
                    }
                    if (m_errorList.Count > 0)
                    {
                        debug.AppendLine("Error:");
                        stream.WriteLine("Error:");
                        stream.WriteLine();
                        foreach (var s in m_errorList)
                        {
                            debug.AppendLine(s);
                            stream.WriteLine(s);
                        }
                    }
                }

                XsltExportResult result = new XsltExportResult(reportId, m_xsltExportRequest.ReportName, m_xsltExportRequest.UserId, m_xsltExportRequest.LoanIdList, outputFile, m_errorList, m_warningList, (int)sw.ElapsedMilliseconds);

                if (m_xsltExportRequest.CallBack != null)
                {
                    m_xsltExportRequest.CallBack.OperationComplete(UserPrincipal.BrokerId, m_xsltExportRequest, result);
                }

                if (IsAsync)
                {
                    XsltExportResultStatus.Update(result);
                }

                debug.AppendLine("Executed in " + sw.ElapsedMilliseconds + "ms.");

                if (this.ContainsSensitiveData)
                {
                    var log = $"User {UserPrincipal.DisplayName} ({UserPrincipal.UserId}) exported report {result.ReportName} containing unmasked SSNs at {result.CreatedDate}. ";
                    var affectedLoans = "Loans in report: " + Environment.NewLine + string.Join(Environment.NewLine, result.LoanIdList);
                    Tools.LogInfo("SensitiveReportExport", log + affectedLoans);

                    SecurityEventLogHelper.CreateSensitiveDataExportLog(UserPrincipal, SensitiveReportType.BatchExport, m_xsltExportRequest.ReportName);
                }

                return result;
            }
            catch (Exception exc)
            {
                debug.AppendLine("Error:");
                debug.AppendLine(exc.ToString());
                debug.AppendLine();
                m_errorList.Add(exc.Message);

                if (IsAsync)
                {
                    // 5/7/2013 dd - Update the status of the report to Error so user does not get misleading status.
                    XsltExportResult result = new XsltExportResult(m_xsltExportRequest.ReportId, m_xsltExportRequest.ReportName, m_xsltExportRequest.UserId, m_xsltExportRequest.LoanIdList, string.Empty, m_errorList, m_warningList, (int)sw.ElapsedMilliseconds);

                    XsltExportResultStatus.Update(result);
                }


                throw;
            }
            finally
            {
                if (m_errorList.Count > 0)
                {
                    Tools.LogError(debug.ToString());
                }
                else
                {
                    Tools.LogInfo(debug.ToString());
                }
            }

        }

        /// <summary>
        /// These exports are a hack to use the powerful asynchronous and callback capabilities built in to the batch export framework.  Eventually, they should be moved to another location.
        /// </summary>
        private XsltExportResult RunNonXsltSpecialExport(string reportId, StringBuilder debug)
        {
            if (m_xsltExportRequest.MapName.Equals("ULDD-Fannie", StringComparison.OrdinalIgnoreCase) || m_xsltExportRequest.MapName.Equals("ULDD-Freddie", StringComparison.OrdinalIgnoreCase))
            {
                bool success = false;
                E_sGseDeliveryTargetT target = m_xsltExportRequest.MapName.Equals("ULDD-Fannie", StringComparison.OrdinalIgnoreCase) ? E_sGseDeliveryTargetT.FannieMae : E_sGseDeliveryTargetT.FreddieMac;
                try
                {
                    if (UserPrincipal == null)
                    {
                        throw new NotFoundException("Principal not found.");
                    }

                    UlddExporter exporter = new UlddExporter(m_xsltExportRequest.LoanIdList, target);
                    List<UlddExportError> errors;
                    string path = null;

                    Stopwatch sw0 = new Stopwatch();
                    sw0.Start();
                    if (exporter.Verify(out errors))
                    {
                        path = TempFileUtils.NewTempFilePath();
                        exporter.Export(path);
                    }
                    sw0.Stop();
                    var errorList = from p in errors
                                    from y in p.ErrorList
                                    select string.Format("Loan: {0} FieldId: {1} Error: {2}", p.sLNm, y.FieldId, y.ErrorMessage);

                    debug.AppendLine("Errors:");
                    foreach (var item in errorList)
                    {
                        debug.AppendLine(item);
                    }
                    var t = new XsltExportResult(reportId, m_xsltExportRequest.ReportName, m_xsltExportRequest.UserId, m_xsltExportRequest.LoanIdList, path, errorList, new string[0], (int)sw0.ElapsedMilliseconds);
                    XsltExportResultStatus.Update(t);

                    if (errorList.Count() > 0)
                    {
                        string emailAddress = m_xsltExportRequest.GetCallBackParameter("Email");
                        var email = new LendersOffice.Email.CBaseEmail(UserPrincipal.BrokerId);
                        email.Subject = "Report had errors."; // Feel Free to update the subject.
                        email.From = ConstStage.DefaultDoNotReplyAddress;
                        email.To = emailAddress;

                        StringBuilder message = new StringBuilder();
                        message.AppendLine(string.Format("Report {0} had the following errors:", m_xsltExportRequest.ReportName));
                        message.AppendLine();
                        foreach (var error in errorList)
                        {
                            message.AppendLine(error);
                        }
                        email.Message = message.ToString();
                        email.Send();

                        debug.AppendLine(string.Format("Sent email error notification to {0}", emailAddress));
                        debug.AppendLine();
                    }

                    success = true;
                    return t;
                }

                catch (Exception e)
                {
                    debug.AppendLine("Exception");
                    debug.AppendLine(e.ToString());
                    Tools.LogError(debug.ToString());

                    if (success == false)
                    {
                        var r = new XsltExportResult(reportId, m_xsltExportRequest.ReportName, m_xsltExportRequest.UserId, m_xsltExportRequest.LoanIdList, "", new string[] { e.ToString() }, new string[0], 0);
                        XsltExportResultStatus.Update(r);
                    }
                    throw;
                }
                finally
                {
                    if (!success)
                    {
                        Tools.LogInfo(debug.ToString());
                    }
                }
            }

            if (m_xsltExportRequest.MapName.Equals("GinnieNet Mortgage Fields", StringComparison.OrdinalIgnoreCase))
            {

                HashSet<string> errors = null;
                bool success = false;
                try
                {
                    if (UserPrincipal == null)
                    {
                        throw new NotFoundException("Principal not found.");
                    }

                    string path = null;

                    Stopwatch sw0 = new Stopwatch();
                    sw0.Start();
                    string csv = GinnieNetExporter.ExportMSegmentsToCsv(m_xsltExportRequest.LoanIdList, out errors);

                    // Write to temp file
                    if (errors.Count == 0 && !String.IsNullOrEmpty(csv))
                    {
                        path = TempFileUtils.NewTempFilePath();
                        TextFileHelper.WriteString(path, csv, true);
                    }

                    sw0.Stop();

                    // Add errors to debug string
                    debug.AppendLine("Errors:");
                    foreach (string error in errors)
                    {
                        debug.AppendLine(error);
                    }

                    // Output results
                    var t = new XsltExportResult(reportId, m_xsltExportRequest.ReportName, m_xsltExportRequest.UserId, m_xsltExportRequest.LoanIdList, path, errors, new string[0], (int)sw0.ElapsedMilliseconds);
                    XsltExportResultStatus.Update(t);

                    // Send Error email
                    if (errors.Count() > 0)
                    {
                        string emailAddress = m_xsltExportRequest.GetCallBackParameter("Email");
                        var email = new LendersOffice.Email.CBaseEmail(UserPrincipal.BrokerId);
                        email.Subject = "Report had errors."; // Feel Free to update the subject.
                        email.From = ConstStage.DefaultDoNotReplyAddress;
                        email.To = emailAddress;

                        StringBuilder message = new StringBuilder();
                        message.AppendLine(string.Format("Report {0} had the following errors:", m_xsltExportRequest.ReportName));
                        message.AppendLine();
                        foreach (string error in errors)
                        {
                            message.AppendLine(error);
                        }
                        email.Message = message.ToString();
                        email.Send();

                        debug.AppendLine(string.Format("Sent email error notification to {0}", emailAddress));
                        debug.AppendLine();
                    }

                    success = true;
                    return t;
                }

                catch (Exception e)
                {
                    debug.AppendLine("Exception");
                    debug.AppendLine(e.ToString());
                    Tools.LogError(debug.ToString());

                    if (success == false)
                    {
                        var r = new XsltExportResult(reportId, m_xsltExportRequest.ReportName, m_xsltExportRequest.UserId, m_xsltExportRequest.LoanIdList, "", new string[] { e.ToString() }, new string[0], 0);
                        XsltExportResultStatus.Update(r);
                    }
                    throw;
                }
                finally
                {
                    if (errors != null && errors.Count > 0)
                    {
                        Tools.LogError(debug.ToString());
                    }
                }
            }

            // OPM 450990
            if (m_xsltExportRequest.MapName.Equals("Export All TPO Users", StringComparison.OrdinalIgnoreCase))
            {
                bool success = false;
                try
                {
                    if (UserPrincipal == null)
                    {
                        throw new NotFoundException("Principal not found.");
                    }

                    Stopwatch sw0 = new Stopwatch();
                    sw0.Start();

                    string csv = TpoUserSearchUtils.ExportAllTpoUsersToCsv(UserPrincipal.BrokerId);

                    string path = null;

                    // Write to temp file
                    if (csv.Length > 0)
                    {
                        path = TempFileUtils.NewTempFilePath();
                        using (FileStream fs = File.Create(path))
                        {
                            using (StreamWriter writer = new StreamWriter(fs, System.Text.Encoding.UTF8))
                            {
                                writer.Write(csv);
                            }
                        }
                    }

                    sw0.Stop();

                    // Output results
                    var t = new XsltExportResult(reportId, m_xsltExportRequest.ReportName, m_xsltExportRequest.UserId, m_xsltExportRequest.LoanIdList, path, Enumerable.Empty<string>(), new string[0], (int)sw0.ElapsedMilliseconds);

                    if (m_xsltExportRequest.CallBack != null)
                    {
                        m_xsltExportRequest.CallBack.OperationComplete(UserPrincipal.BrokerId, m_xsltExportRequest, t);
                    }

                    XsltExportResultStatus.Update(t);

                    success = true;
                    return t;
                }

                catch (Exception e)
                {
                    debug.AppendLine("Exception");
                    debug.AppendLine(e.ToString());
                    Tools.LogError(debug.ToString());

                    if (success == false)
                    {
                        var r = new XsltExportResult(reportId, m_xsltExportRequest.ReportName, m_xsltExportRequest.UserId, m_xsltExportRequest.LoanIdList, "", new string[] { e.ToString() }, new string[0], 0);
                        XsltExportResultStatus.Update(r);
                    }
                    throw;
                }
            }

            if (StringComparer.OrdinalIgnoreCase.Equals(this.m_xsltExportRequest.MapName, "SendDataToMCT"))
            {
                XsltExportResult result = null;
                try
                {
                    if (this.UserPrincipal == null)
                    {
                        throw new NotFoundException("Principal not found.");
                    }

                    Stopwatch stopwatch = new Stopwatch();
                    stopwatch.Start();
                    string exportData = MctXmlExport.GenerateXml(this.UserPrincipal.BrokerId, this.UserPrincipal.BrokerDB.CustomerCode).ToString(SaveOptions.DisableFormatting);
                    string outputPath = null;
                    if (!string.IsNullOrEmpty(exportData))
                    {
                        outputPath = TempFileUtils.NewTempFilePath();
                        TextFileHelper.WriteString(outputPath, exportData, true);
                    }

                    stopwatch.Stop();
                    result = new XsltExportResult(reportId, m_xsltExportRequest.ReportName, m_xsltExportRequest.UserId, m_xsltExportRequest.LoanIdList, outputPath, this.m_errorList, this.m_warningList, (int)stopwatch.ElapsedMilliseconds);
                    this.m_xsltExportRequest.CallBack?.OperationComplete(UserPrincipal.BrokerId, this.m_xsltExportRequest, result);

                    return result;
                }
                catch (Exception e)
                {
                    result = null;
                    string exceptionString = e.ToString();
                    this.m_errorList.Add(exceptionString);
                    Tools.LogError(debug.AppendLine("Exception").AppendLine(exceptionString).ToString());
                    throw;
                }
                finally
                {
                    if (this.IsAsync)
                    {
                        XsltExportResultStatus.Update(result ?? new XsltExportResult(reportId, m_xsltExportRequest.ReportName, m_xsltExportRequest.UserId, m_xsltExportRequest.LoanIdList, string.Empty, this.m_errorList, this.m_warningList, 0));
                    }
                }
            }

            return null;
        }

        // Validate the field permissions of the B user that the REQUEST belongs to
        public bool UserHasFieldLevelAccess()
        {
            return UserHasFieldLevelAccess(UserPrincipal as BrokerUserPrincipal);
        }
        // Validate the field permissions of a B user that the request may/may not belong to
        public bool UserHasFieldLevelAccess(BrokerUserPrincipal buPrincipal)
        {
            IEnumerable<string> xsltList = null;
            var mapItem = XsltMap.Get(m_xsltExportRequest.MapName);
            xsltList = mapItem.XslFileList;
            
            if (xsltList != null)
            {
                HashSet<string> possibleFieldSet = ExtractPossibleLoanFields(xsltList);

                foreach (string sFieldId in possibleFieldSet)
                {
                    if (LoanFieldSecurityManager.IsSecureField(sFieldId) && !LoanFieldSecurityManager.CanReadField(sFieldId, buPrincipal))
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        private void ProcessSpecialXslt(string xslt, string inputFile, string outputFile)
        {
            if (xslt.StartsWith("CONVERT_EXCEL:"))
            {
                string[] parts = xslt.Split(':');
                string excelTemplateFile = parts[1];
                string sheetName = parts[2];
                string startColumn = parts[3];
                int startRow =1;

                if (int.TryParse(parts[4], out startRow) == false)
                {
                    startRow = 1;
                }
                ConvertExcel(inputFile, outputFile, excelTemplateFile, sheetName, startColumn, startRow);
            }
            else
            {
                throw new CBaseException("Not handle Special Xslt:[" + xslt + "]", "Not handle Special Xslt:[" + xslt + "]");
            }
        }

        private void ConvertExcel(string inputFile, string outputFile, string excelTemplateFile, string sheetName, string startColumn, int startRow)
        {
            string tempFile = GetTempOutputPath(Guid.NewGuid() + ".xlsx");
            FileOperationHelper.Copy(GetFileInXsltFolder(excelTemplateFile), tempFile, true);
            FileOperationHelper.TurnOffReadOnly(tempFile);
            XLWorkbook workbook = new XLWorkbook(tempFile, XLEventTracking.Disabled);

            IXLWorksheet worksheet = null;
            if (string.IsNullOrEmpty(sheetName))
            {
                worksheet = workbook.Worksheet(1); // Get first worksheet.
            }
            else
            {
                worksheet = workbook.Worksheet(sheetName);
            }
            if (worksheet == null)
            {
                throw new CBaseException("Unable to locate sheet name[" + sheetName + "]", "Unable to locate sheet name[" + sheetName + "]");
            }
            
            XDocument xdoc = XDocument.Load(inputFile);
            int row = startRow;
            foreach (XElement rowElement in xdoc.Root.Elements())
            {
                int column = XLHelper.GetColumnNumberFromLetter(startColumn);
                foreach (XElement colElement in rowElement.Elements())
                {
                    string type = "string";

                    XAttribute typeAttr = colElement.Attribute("t");
                    if (typeAttr != null)
                    {
                        if (typeAttr.Value.Equals("number", StringComparison.OrdinalIgnoreCase))
                        {
                            type = "number";
                        }
                    }
                    XAttribute attr = colElement.Attribute("v");
                    if (attr != null)
                    {
                        var cell = worksheet.Cell(row,column);
                        if (type == "number")
                        {
                            double d = 0;
                            if (double.TryParse(attr.Value, out d) == false)
                            {
                                cell.SetValue(attr.Value);
                            }
                            else
                            {
                                cell.SetValue(d);
                            }
                        }
                        else
                        {
                            cell.SetValue(attr.Value);
                        }
                    }
                    column++;
                }
                row++;
            }

            workbook.SaveAs(outputFile);
            //throw new NotImplementedException();
        }

        private List<string> m_errorList = new List<string>();
        private List<string> m_warningList = new List<string>();

        /// <summary>
        /// Converts to "1" or "0".
        /// </summary>
        /// <param name="b">A boolean value to convert to a bit representation.</param>
        /// <returns>"1" if true, "0" otherwise.</returns>
        internal static string GetBoolString(bool b)
        {
            return b ? "1" : "0";
        }

        /// <summary>
        /// Converts an Enum to its numerical string value.
        /// </summary>
        /// <param name="e">An enum value.</param>
        /// <returns>The numerical (integer) value of the Enum.</returns>
        private static string GetEnumString(Enum e)
        {
            return e.ToString("D");
        }

        // 1/27/2015 tj - Making this for fields that can't easily be added to PageDataUtilities
        private static readonly Dictionary<string, Func<CPageData, string>> fieldRepsNotInPageDataUtilities = new Dictionary<string, Func<CPageData, string>>(StringComparer.OrdinalIgnoreCase)
        {
            // Data layer bug in the Time fields means that sLoanSubmittedDTime returns "2/17/2015",
            // while sLoanSubmittedDTime_rep returns "10:15:02 AM PST".
            // Since the type is string, PageDataUtilities reads the actual field instead of the rep.
            { "sApprovDTime_rep", dataLoan => dataLoan.sApprovDTime_rep },
            { "sArchivedDTime_rep", dataLoan => dataLoan.sArchivedDTime_rep },
            { "sCanceledDTime_rep", dataLoan => dataLoan.sCanceledDTime_rep },
            { "sClearToCloseDTime_rep", dataLoan => dataLoan.sClearToCloseDTime_rep },
            { "sClearToPurchaseDTime_rep", dataLoan => dataLoan.sClearToPurchaseDTime_rep },
            { "sClosedDTime_rep", dataLoan => dataLoan.sClosedDTime_rep },
            { "sConditionReviewDTime_rep", dataLoan => dataLoan.sConditionReviewDTime_rep },
            { "sCondSentToInvestorDTime_rep", dataLoan => dataLoan.sCondSentToInvestorDTime_rep },
            { "sCounterOfferDTime_rep", dataLoan => dataLoan.sCounterOfferDTime_rep },
            { "sDocsBackDTime_rep", dataLoan => dataLoan.sDocsBackDTime_rep },
            { "sDocsDrawnDTime_rep", dataLoan => dataLoan.sDocsDrawnDTime_rep },
            { "sDocsDTime_rep", dataLoan => dataLoan.sDocsDTime_rep },
            { "sDocsOrderedDTime_rep", dataLoan => dataLoan.sDocsOrderedDTime_rep },
            { "sDocumentCheckDTime_rep", dataLoan => dataLoan.sDocumentCheckDTime_rep },
            { "sDocumentCheckFailedDTime_rep", dataLoan => dataLoan.sDocumentCheckFailedDTime_rep },
            { "sEstCloseDTime_rep", dataLoan => dataLoan.sEstCloseDTime_rep },
            { "sFinalDocsDTime_rep", dataLoan => dataLoan.sFinalDocsDTime_rep },
            { "sFinalUnderwritingDTime_rep", dataLoan => dataLoan.sFinalUnderwritingDTime_rep },
            { "sFundDTime_rep", dataLoan => dataLoan.sFundDTime_rep },
            { "sFundingConditionsDTime_rep", dataLoan => dataLoan.sFundingConditionsDTime_rep },
            { "sInFinalPurchaseReviewDTime_rep", dataLoan => dataLoan.sInFinalPurchaseReviewDTime_rep },
            { "sInPurchaseReviewDTime_rep", dataLoan => dataLoan.sInPurchaseReviewDTime_rep },
            { "sLeadDTime_rep", dataLoan => dataLoan.sLeadDTime_rep },
            { "sLoanSubmittedDTime_rep", dataLoan => dataLoan.sLoanSubmittedDTime_rep },
            { "sLPurchaseDTime_rep", dataLoan => dataLoan.sLPurchaseDTime_rep },
            { "sOnHoldDTime_rep", dataLoan => dataLoan.sOnHoldDTime_rep },
            { "sOpenedDTime_rep", dataLoan => dataLoan.sOpenedDTime_rep },
            { "sPreApprovDTime_rep", dataLoan => dataLoan.sPreApprovDTime_rep },
            { "sPreDocQCDTime_rep", dataLoan => dataLoan.sPreDocQCDTime_rep },
            { "sPreProcessingDTime_rep", dataLoan => dataLoan.sPreProcessingDTime_rep },
            { "sPrePurchaseConditionsDTime_rep", dataLoan => dataLoan.sPrePurchaseConditionsDTime_rep },
            { "sPreQualDTime_rep", dataLoan => dataLoan.sPreQualDTime_rep },
            { "sPreUnderwritingDTime_rep", dataLoan => dataLoan.sPreUnderwritingDTime_rep },
            { "sProcessingDTime_rep", dataLoan => dataLoan.sProcessingDTime_rep },
            { "sPurchasedDTime_rep", dataLoan => dataLoan.sPurchasedDTime_rep },
            { "sReadyForSaleDTime_rep", dataLoan => dataLoan.sReadyForSaleDTime_rep },
            { "sRecordedDTime_rep", dataLoan => dataLoan.sRecordedDTime_rep },
            { "sRejectDTime_rep", dataLoan => dataLoan.sRejectDTime_rep },
            { "sRLckdDTime_rep", dataLoan => dataLoan.sRLckdDTime_rep },
            { "sSchedFundDTime_rep", dataLoan => dataLoan.sSchedFundDTime_rep },
            { "sShippedToInvestorDTime_rep", dataLoan => dataLoan.sShippedToInvestorDTime_rep },
            { "sSubmitDTime_rep", dataLoan => dataLoan.sSubmitDTime_rep },
            { "sSubmittedForFinalPurchaseDTime_rep", dataLoan => dataLoan.sSubmittedForFinalPurchaseDTime_rep },
            { "sSubmittedForPurchaseReviewDTime_rep", dataLoan => dataLoan.sSubmittedForPurchaseReviewDTime_rep },
            { "sSuspendedByInvestorDTime_rep", dataLoan => dataLoan.sSuspendedByInvestorDTime_rep },
            { "sSuspendedDTime_rep", dataLoan => dataLoan.sSuspendedDTime_rep },
            { "sUnderwritingDTime_rep", dataLoan => dataLoan.sUnderwritingDTime_rep },
            { "sWithdrawnDTime_rep", dataLoan => dataLoan.sWithdrawnDTime_rep },

            { "sMortgagePoolId", dataLoan => dataLoan.sMortgagePoolId_rep } // Return type is "long?"; neither long nor nullable is supported.
        };

        private void GenerateLoanData(Guid userId, IEnumerable<Guid> loanIdList, IEnumerable<string> xsltList, string reportId)
        {
            HashSet<string> possibleFieldSet = ExtractPossibleLoanFields(xsltList);

            #region Get List of Loan/App fields
            HashSet<string> loanFieldSet = new HashSet<string>();
            HashSet<string> appFieldSet = new HashSet<string>();

            HashSet<string> additionalDependencyFields = new HashSet<string>();
            HashSet<string> nonPageDataUtilitiesFields = new HashSet<string>();
            HashSet<string> cacheTableOnlyFields = new HashSet<string>();

            bool isIncludeAgentOfRole = false;
            bool isIncludePreparerOfForm = false;
            bool isIncludeCustomFieldCollection = false;
            bool isIncludeEscrowSchedule = false;
            bool isIncludeBrokerLockAdjustments = false;
            bool isIncludeInvestorLockAdjustments = false;
            bool isIncludeInvestorInfo = false;
            bool isIncludeAssignedEmployees = false;
            bool isIncludeServicingPayments = false;
            bool isIncludeLastDisclosedGfeArchive = false;

            bool isIncludeLiaCollection = false;
            bool isIncludeAssetCollection = false;

            bool isIncludePurchaseAdviceAdjustments = false;
            bool isIncludePurchaseAdviceFees = false;

            bool isIncludeAggregateEscrowAccount = false;

            bool isIncludeClosingCostSet = false;

            bool isIncludeAdjustmentList = false;

            bool isIncludeLoanEstimateDatesInfo = false;
            bool isIncludeClosingDisclosureDatesInfo = false;
            bool isIncludeHousingExpenses = false;

            bool isIncludeToleranceCureDebug = false;

            bool isIncludeTransactions = false;

            bool isIncludeUcdDeliveryCollection = false;

            foreach (string fieldId in possibleFieldSet)
            {
                if (!this.ContainsSensitiveData && (fieldId.Equals("aBSsn", StringComparison.OrdinalIgnoreCase) || fieldId.Equals("aCSsn", StringComparison.OrdinalIgnoreCase)))
                {
                    this.ContainsSensitiveData = true;
                }

                // 9/24/2013 AV - 139179 Add data layer to return the assigned loan officer's first and last name
                if (fieldId.Equals("sAssignedEmployee", StringComparison.OrdinalIgnoreCase))
                {
                    isIncludeAssignedEmployees = true;
                    additionalDependencyFields.Add("TableEmployee");

                }
                else if (fieldId.Equals("sClosingCostSet", StringComparison.OrdinalIgnoreCase))
                {
                    // 3/24/2015 dd - OPM 198222 Add data layer to return new GFE 2015 structure.
                    isIncludeClosingCostSet = true;
                    additionalDependencyFields.Add("sClosingCostSet");
                }
                else if (fieldId.Equals("sAdjustmentList", StringComparison.OrdinalIgnoreCase))
                {
                    isIncludeAdjustmentList = true;
                    additionalDependencyFields.Add("sAdjustmentList");
                }
                else if (fieldId.Equals("sLoanEstimateDatesInfo", StringComparison.OrdinalIgnoreCase))
                {
                    isIncludeLoanEstimateDatesInfo = true;
                    additionalDependencyFields.Add("sLoanEstimateDatesInfo");
                }
                else if (fieldId.Equals("sClosingDisclosureDatesInfo", StringComparison.OrdinalIgnoreCase))
                {
                    isIncludeClosingDisclosureDatesInfo = true;
                    additionalDependencyFields.Add("sClosingDisclosureDatesInfo");
                }
                else if (fieldId.Equals("sHousingExpenses", StringComparison.OrdinalIgnoreCase))
                {
                    isIncludeHousingExpenses = true;
                    additionalDependencyFields.Add("sHousingExpenses");
                }
                else if (fieldId.Equals("sAgentOfRole", StringComparison.OrdinalIgnoreCase))
                {
                    isIncludeAgentOfRole = true;
                    additionalDependencyFields.Add("sfGetAgentOfRole");
                }
                else if (fieldId.Equals("sPreparerOfForm", StringComparison.OrdinalIgnoreCase))
                {
                    isIncludePreparerOfForm = true;
                    additionalDependencyFields.Add("sfGetPreparerOfForm");
                }
                else if (fieldId.Equals("sEscrowScheduleItem", StringComparison.OrdinalIgnoreCase))
                {
                    isIncludeEscrowSchedule = true;
                    additionalDependencyFields.Add("sEscrowScheduleList");

                }
                else if (fieldId.Equals("sInvestorInfo", StringComparison.OrdinalIgnoreCase))
                {
                    isIncludeInvestorInfo = true;
                    additionalDependencyFields.Add("sInvestorInfo");
                }
                else if (fieldId.Equals("sCustomField", StringComparison.OrdinalIgnoreCase))
                {
                    isIncludeCustomFieldCollection = true;

                    for (int i = 1; i <= 20; i++)
                    {
                        additionalDependencyFields.Add("sCustomField" + i + "Desc");
                        additionalDependencyFields.Add("sCustomField" + i + "Money");
                        additionalDependencyFields.Add("sCustomField" + i + "D");
                        additionalDependencyFields.Add("sCustomField" + i + "Pc");
                        additionalDependencyFields.Add("sCustomField" + i + "Bit");
                        additionalDependencyFields.Add("sCustomField" + i + "Notes");
                    }
                }
                else if (fieldId.Equals("sBrokerLockAdjustments", StringComparison.OrdinalIgnoreCase))
                {
                    isIncludeBrokerLockAdjustments = true;
                    additionalDependencyFields.Add("sBrokerLockAdjustments");
                }
                else if (fieldId.Equals("sInvestorLockAdjustments", StringComparison.OrdinalIgnoreCase))
                {
                    isIncludeInvestorLockAdjustments = true;
                    additionalDependencyFields.Add("sInvestorLockAdjustments");
                }
                else if (fieldId.Equals("sServicingPayments", StringComparison.OrdinalIgnoreCase))
                {
                    isIncludeServicingPayments = true;
                    additionalDependencyFields.Add("sServicingPayments");
                }
                else if (fieldId.Equals("aLiaCollection", StringComparison.OrdinalIgnoreCase))
                {
                    isIncludeLiaCollection = true;
                    additionalDependencyFields.Add("aLiaCollection");
                }
                else if (fieldId.Equals("aAssetCollection", StringComparison.OrdinalIgnoreCase))
                {
                    isIncludeAssetCollection = true;
                    additionalDependencyFields.Add("aAssetCollection");
                }
                else if (fieldId.Equals("sPurchaseAdviceAdjustments", StringComparison.OrdinalIgnoreCase))
                {
                    isIncludePurchaseAdviceAdjustments = true;
                    additionalDependencyFields.Add("sPurchaseAdviceAdjustments");
                }
                else if (fieldId.Equals("sPurchaseAdviceFees", StringComparison.OrdinalIgnoreCase))
                {
                    isIncludePurchaseAdviceFees = true;
                    additionalDependencyFields.Add("sPurchaseAdviceFees");
                }
                else if (fieldId.Equals("sAggregateEscrowAccount", StringComparison.OrdinalIgnoreCase))
                {
                    isIncludeAggregateEscrowAccount = true;
                    additionalDependencyFields.Add("sAggregateEscrowAccount");
                }
                else if (fieldId.Equals("sLastDisclosedGfeArchive", StringComparison.OrdinalIgnoreCase))
                {
                    isIncludeLastDisclosedGfeArchive = true;
                    additionalDependencyFields.Add("LastDisclosedGFEArchive");
                }
                else if (fieldId.Equals("sToleranceCureDebug", StringComparison.OrdinalIgnoreCase))
                {
                    isIncludeToleranceCureDebug = true;
                    additionalDependencyFields.Add("sToleranceCureDebug");
                }
                else if (fieldId.Equals("sTransactions", StringComparison.OrdinalIgnoreCase))
                {
                    isIncludeTransactions = true;
                    additionalDependencyFields.Add("sTransactions");
                }
                else if (fieldId.Equals("sUcdDeliveryCollection", StringComparison.OrdinalIgnoreCase))
                {
                    isIncludeUcdDeliveryCollection = true;
                    additionalDependencyFields.Add("sUcdDeliveryCollection");
                }

                else if (fieldRepsNotInPageDataUtilities.ContainsKey(fieldId))
                {
                    nonPageDataUtilitiesFields.Add(fieldId);
                    additionalDependencyFields.Add(fieldId); // Do this to skip creating another costly union of IEnumerable<T>
                }
                else if (PageDataUtilities.ContainsField(fieldId))
                {
                    E_PageDataClassType pageDataClassType;

                    if (PageDataUtilities.GetPageDataClassType(fieldId, out pageDataClassType))
                    {
                        switch (pageDataClassType)
                        {
                            case E_PageDataClassType.CBasePage:
                                loanFieldSet.Add(fieldId);
                                break;
                            case E_PageDataClassType.CAppBase:
                                appFieldSet.Add(fieldId);
                                break;
                            default:
                                throw new UnhandledEnumException(pageDataClassType);
                        }
                    }
                }
                else if (LoanFileCache.IsCachedField(fieldId))
                {
                    // Only check Cache table fields after normal datalayer has failed.
                    cacheTableOnlyFields.Add(fieldId);
                }
            }
            #endregion
            XmlWriterSettings xmlWriterSettings = new XmlWriterSettings();
            xmlWriterSettings.Indent = true;

            string[] combinedFieldArray = new string[loanFieldSet.Count + appFieldSet.Count + additionalDependencyFields.Count];
            int combinedFieldSetIndex = 0;
            foreach (string field in loanFieldSet)
            {
                combinedFieldArray[combinedFieldSetIndex++] = DependencyUtils.NormalizeMemberName(field);
            }
            foreach (string field in appFieldSet)
            {
                combinedFieldArray[combinedFieldSetIndex++] = DependencyUtils.NormalizeMemberName(field);
            }
            foreach (string field in additionalDependencyFields)
            {
                combinedFieldArray[combinedFieldSetIndex++] = field;
            }

            CSelectStatementProvider selectStatementProvider = CSelectStatementProvider.GetProviderFor(E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, combinedFieldArray);


            // Need to call UserPrincipal so that a proper credential is set before loading data object.
            if (UserPrincipal == null)
            {
                m_errorList.Add("User does not existed.");
                return;

            }


            using (SanitizedXmlWriterWrapper xmlWriter = new SanitizedXmlWriterWrapper(XmlWriter.Create(GetIntermediateLoanFileName(reportId), xmlWriterSettings)))
            {
                xmlWriter.WriteStartElement("result");
                foreach (Guid sLId in loanIdList)
                {
                    try
                    {
                        CPageData dataLoan = new CPageData(sLId, "XsltExport", selectStatementProvider);
                        dataLoan.InitLoad();
                        dataLoan.SetFormatTarget(FormatTarget.XsltExport);

                        xmlWriter.WriteStartElement("loan");
                        foreach (var loanFieldId in loanFieldSet)
                        {
                            string v = PageDataUtilities.GetValue(dataLoan, null, loanFieldId);
                            xmlWriter.WriteAttributeString(loanFieldId, v);
                        }

                        foreach (var loanFieldId in nonPageDataUtilitiesFields)
                        {
                            xmlWriter.WriteAttributeString(loanFieldId, fieldRepsNotInPageDataUtilities[loanFieldId](dataLoan));
                        }

                        if (cacheTableOnlyFields.Count > 0)
                        {
                            LoanFileCache loanFileCache = new LoanFileCache(sLId, cacheTableOnlyFields);
                            foreach (string fieldId in cacheTableOnlyFields)
                            {
                                xmlWriter.WriteAttributeString(fieldId, loanFileCache.GetStringRep(fieldId, dataLoan.m_convertLos));
                            }
                        }

                        if (isIncludeAgentOfRole)
                        {
                            #region Generate AgentOfRole Record
                            for (int i = 0; i < dataLoan.GetAgentRecordCount(); i++)
                            {
                                CAgentFields agent = dataLoan.GetAgentFields(i);
                                if (agent.IsValid)
                                {
                                    xmlWriter.WriteStartElement("sAgentOfRole");
                                    xmlWriter.WriteAttributeString("AgentRoleT", agent.AgentRoleT.ToString());
                                    xmlWriter.WriteAttributeString("EmployeeIDInCompany", agent.EmployeeIDInCompany);
                                    xmlWriter.WriteAttributeString("AgentRoleDescription", agent.AgentRoleDescription);
                                    xmlWriter.WriteAttributeString("AgentName", agent.AgentName);
                                    xmlWriter.WriteAttributeString("TaxId", agent.TaxId);
                                    xmlWriter.WriteAttributeString("LicenseNumOfAgent", agent.LicenseNumOfAgent);
                                    xmlWriter.WriteAttributeString("LicenseNumOfCompany", agent.LicenseNumOfCompany);
                                    xmlWriter.WriteAttributeString("DepartmentName", agent.DepartmentName);
                                    xmlWriter.WriteAttributeString("CaseNum", agent.CaseNum);
                                    xmlWriter.WriteAttributeString("CompanyName", agent.CompanyName);
                                    xmlWriter.WriteAttributeString("StreetAddr", agent.StreetAddr);
                                    xmlWriter.WriteAttributeString("City", agent.City);
                                    xmlWriter.WriteAttributeString("State", agent.State);
                                    xmlWriter.WriteAttributeString("Zip", agent.Zip);
                                    xmlWriter.WriteAttributeString("OtherAgentRoleTDesc", agent.OtherAgentRoleTDesc);
                                    xmlWriter.WriteAttributeString("Phone", agent.Phone);
                                    xmlWriter.WriteAttributeString("CellPhone", agent.CellPhone);
                                    xmlWriter.WriteAttributeString("PagerNum", agent.PagerNum);
                                    xmlWriter.WriteAttributeString("FaxNum", agent.FaxNum);
                                    xmlWriter.WriteAttributeString("EmailAddr", agent.EmailAddr);
                                    xmlWriter.WriteAttributeString("Notes", agent.Notes);
                                    xmlWriter.WriteAttributeString("IsUseOriginatorCompensationAmount", GetBoolString(agent.IsUseOriginatorCompensationAmount));
                                    xmlWriter.WriteAttributeString("CommissionMinBase", agent.CommissionMinBase_rep);
                                    xmlWriter.WriteAttributeString("CommissionPointOfLoanAmount", agent.CommissionPointOfLoanAmount_rep);
                                    xmlWriter.WriteAttributeString("CommissionPointOfGrossProfit", agent.CommissionPointOfGrossProfit_rep);
                                    xmlWriter.WriteAttributeString("Commission", agent.Commission_rep);
                                    xmlWriter.WriteAttributeString("IsCommissionPaid", GetBoolString(agent.IsCommissionPaid));
                                    xmlWriter.WriteAttributeString("CommissionPaid", agent.CommissionPaid_rep);
                                    xmlWriter.WriteAttributeString("PaymentNotes", agent.PaymentNotes);
                                    xmlWriter.WriteAttributeString("InvestorSoldDate", agent.InvestorSoldDate_rep);
                                    xmlWriter.WriteAttributeString("InvestorBasisPoints", agent.InvestorBasisPoints_rep);
                                    xmlWriter.WriteAttributeString("IsListedInGFEProviderForm", GetBoolString(agent.IsListedInGFEProviderForm));
                                    xmlWriter.WriteAttributeString("IsLender", GetBoolString(agent.IsLender));
                                    xmlWriter.WriteAttributeString("IsOriginator", GetBoolString(agent.IsOriginator));
                                    xmlWriter.WriteAttributeString("IsOriginatorAffiliate", GetBoolString(agent.IsOriginatorAffiliate));
                                    xmlWriter.WriteAttributeString("IsLenderAssociation", GetBoolString(agent.IsLenderAssociation));
                                    xmlWriter.WriteAttributeString("IsLenderAffiliate", GetBoolString(agent.IsLenderAffiliate));
                                    xmlWriter.WriteAttributeString("IsLenderRelative", GetBoolString(agent.IsLenderRelative));
                                    xmlWriter.WriteAttributeString("HasLenderRelationship", GetBoolString(agent.HasLenderRelationship));
                                    xmlWriter.WriteAttributeString("HasLenderAccountLast12Months", GetBoolString(agent.HasLenderAccountLast12Months));
                                    xmlWriter.WriteAttributeString("IsUsedRepeatlyByLenderLast12Months", GetBoolString(agent.IsUsedRepeatlyByLenderLast12Months));
                                    xmlWriter.WriteAttributeString("ProviderItemNumber", agent.ProviderItemNumber);
                                    xmlWriter.WriteAttributeString("PhoneOfCompany", agent.PhoneOfCompany);
                                    xmlWriter.WriteAttributeString("FaxOfCompany", agent.FaxOfCompany);
                                    xmlWriter.WriteAttributeString("IsNotifyWhenLoanStatusChange", GetBoolString(agent.IsNotifyWhenLoanStatusChange));
                                    xmlWriter.WriteAttributeString("LoanOriginatorIdentifier", agent.LoanOriginatorIdentifier);
                                    xmlWriter.WriteAttributeString("CompanyLoanOriginatorIdentifier", agent.CompanyLoanOriginatorIdentifier);
                                    xmlWriter.WriteAttributeString("BranchName", agent.BranchName);
                                    xmlWriter.WriteAttributeString("PayToBankName", agent.PayToBankName);
                                    xmlWriter.WriteAttributeString("PayToBankCityState", agent.PayToBankCityState);
                                    xmlWriter.WriteAttributeString("PayToABANumber", agent.PayToABANumber.Value);
                                    xmlWriter.WriteAttributeString("PayToAccountNumber", agent.PayToAccountNumber.Value);
                                    xmlWriter.WriteAttributeString("PayToAccountName", agent.PayToAccountName);
                                    xmlWriter.WriteAttributeString("FurtherCreditToAccountNumber", agent.FurtherCreditToAccountNumber.Value);
                                    xmlWriter.WriteAttributeString("FurtherCreditToAccountName", agent.FurtherCreditToAccountName);
                                    xmlWriter.WriteAttributeString("RecordId", agent.RecordId.ToString());
                                    xmlWriter.WriteEndElement(); // </sAgentOfRole>

                                    if (!this.ContainsSensitiveData && agent.TaxIdValid)
                                    {
                                        this.ContainsSensitiveData = true;
                                    }
                                }
                            }
                            #endregion
                        }

                        if (isIncludePreparerOfForm)
                        {
                            #region Generate PreparerOfForm Record
                            foreach (var preparer in dataLoan.PreparerCollection.Values)
                            {
                                if (preparer.IsValid)
                                {
                                    xmlWriter.WriteStartElement("sPreparerOfForm");
                                    xmlWriter.WriteAttributeString("PhoneOfCompany", preparer.PhoneOfCompany);
                                    xmlWriter.WriteAttributeString("FaxOfCompany", preparer.FaxOfCompany);
                                    xmlWriter.WriteAttributeString("PreparerFormT", preparer.PreparerFormT.ToString());
                                    xmlWriter.WriteAttributeString("PreparerName", preparer.PreparerName);
                                    xmlWriter.WriteAttributeString("TaxId", preparer.TaxId);
                                    xmlWriter.WriteAttributeString("Title", preparer.Title);
                                    xmlWriter.WriteAttributeString("LicenseNumOfAgent", preparer.LicenseNumOfAgent);
                                    xmlWriter.WriteAttributeString("LicenseNumOfCompany", preparer.LicenseNumOfCompany);
                                    xmlWriter.WriteAttributeString("DepartmentName", preparer.DepartmentName);
                                    xmlWriter.WriteAttributeString("CaseNum", preparer.CaseNum);
                                    xmlWriter.WriteAttributeString("CompanyName", preparer.CompanyName);
                                    xmlWriter.WriteAttributeString("StreetAddr", preparer.StreetAddr);
                                    xmlWriter.WriteAttributeString("City", preparer.City);
                                    xmlWriter.WriteAttributeString("State", preparer.State);
                                    xmlWriter.WriteAttributeString("Zip", preparer.Zip);
                                    xmlWriter.WriteAttributeString("Phone", preparer.Phone);
                                    xmlWriter.WriteAttributeString("CellPhone", preparer.CellPhone);
                                    xmlWriter.WriteAttributeString("PagerNum", preparer.PagerNum);
                                    xmlWriter.WriteAttributeString("FaxNum", preparer.FaxNum);
                                    xmlWriter.WriteAttributeString("EmailAddr", preparer.EmailAddr);
                                    xmlWriter.WriteAttributeString("LoanOriginatorIdentifier", preparer.LoanOriginatorIdentifier);
                                    xmlWriter.WriteAttributeString("CompanyLoanOriginatorIdentifier", preparer.CompanyLoanOriginatorIdentifier);
                                    xmlWriter.WriteAttributeString("PrepareDate", preparer.PrepareDate_rep);
                                    xmlWriter.WriteEndElement(); //</sPreparerOfForm>

                                    if (!this.ContainsSensitiveData && preparer.TaxIdValid)
                                    {
                                        this.ContainsSensitiveData = true;
                                    }
                                }
                            }
                            #endregion
                        }

                        if (isIncludeCustomFieldCollection)
                        {
                            #region Generate CustomField Collection
                            xmlWriter.WriteStartElement("sCustomField");
                            xmlWriter.WriteAttributeString("Index", "1");
                            xmlWriter.WriteAttributeString("Desc", dataLoan.sCustomField1Desc);
                            xmlWriter.WriteAttributeString("Money", dataLoan.sCustomField1Money_rep);
                            xmlWriter.WriteAttributeString("Date", dataLoan.sCustomField1D_rep);
                            xmlWriter.WriteAttributeString("Percent", dataLoan.sCustomField1Pc_rep);
                            xmlWriter.WriteAttributeString("Bit", GetBoolString(dataLoan.sCustomField1Bit));
                            xmlWriter.WriteAttributeString("Notes", dataLoan.sCustomField1Notes);
                            xmlWriter.WriteEndElement(); //</sCustomField>

                            xmlWriter.WriteStartElement("sCustomField");
                            xmlWriter.WriteAttributeString("Index", "2");
                            xmlWriter.WriteAttributeString("Desc", dataLoan.sCustomField2Desc);
                            xmlWriter.WriteAttributeString("Money", dataLoan.sCustomField2Money_rep);
                            xmlWriter.WriteAttributeString("Date", dataLoan.sCustomField2D_rep);
                            xmlWriter.WriteAttributeString("Percent", dataLoan.sCustomField2Pc_rep);
                            xmlWriter.WriteAttributeString("Bit", GetBoolString(dataLoan.sCustomField2Bit));
                            xmlWriter.WriteAttributeString("Notes", dataLoan.sCustomField2Notes);
                            xmlWriter.WriteEndElement(); //</sCustomField>


                            xmlWriter.WriteStartElement("sCustomField");
                            xmlWriter.WriteAttributeString("Index", "3");
                            xmlWriter.WriteAttributeString("Desc", dataLoan.sCustomField3Desc);
                            xmlWriter.WriteAttributeString("Money", dataLoan.sCustomField3Money_rep);
                            xmlWriter.WriteAttributeString("Date", dataLoan.sCustomField3D_rep);
                            xmlWriter.WriteAttributeString("Percent", dataLoan.sCustomField3Pc_rep);
                            xmlWriter.WriteAttributeString("Bit", GetBoolString(dataLoan.sCustomField3Bit));
                            xmlWriter.WriteAttributeString("Notes", dataLoan.sCustomField3Notes);
                            xmlWriter.WriteEndElement(); //</sCustomField>


                            xmlWriter.WriteStartElement("sCustomField");
                            xmlWriter.WriteAttributeString("Index", "4");
                            xmlWriter.WriteAttributeString("Desc", dataLoan.sCustomField4Desc);
                            xmlWriter.WriteAttributeString("Money", dataLoan.sCustomField4Money_rep);
                            xmlWriter.WriteAttributeString("Date", dataLoan.sCustomField4D_rep);
                            xmlWriter.WriteAttributeString("Percent", dataLoan.sCustomField4Pc_rep);
                            xmlWriter.WriteAttributeString("Bit", GetBoolString(dataLoan.sCustomField4Bit));
                            xmlWriter.WriteAttributeString("Notes", dataLoan.sCustomField4Notes);
                            xmlWriter.WriteEndElement(); //</sCustomField>


                            xmlWriter.WriteStartElement("sCustomField");
                            xmlWriter.WriteAttributeString("Index", "5");
                            xmlWriter.WriteAttributeString("Desc", dataLoan.sCustomField5Desc);
                            xmlWriter.WriteAttributeString("Money", dataLoan.sCustomField5Money_rep);
                            xmlWriter.WriteAttributeString("Date", dataLoan.sCustomField5D_rep);
                            xmlWriter.WriteAttributeString("Percent", dataLoan.sCustomField5Pc_rep);
                            xmlWriter.WriteAttributeString("Bit", GetBoolString(dataLoan.sCustomField5Bit));
                            xmlWriter.WriteAttributeString("Notes", dataLoan.sCustomField5Notes);
                            xmlWriter.WriteEndElement(); //</sCustomField>


                            xmlWriter.WriteStartElement("sCustomField");
                            xmlWriter.WriteAttributeString("Index", "6");
                            xmlWriter.WriteAttributeString("Desc", dataLoan.sCustomField6Desc);
                            xmlWriter.WriteAttributeString("Money", dataLoan.sCustomField6Money_rep);
                            xmlWriter.WriteAttributeString("Date", dataLoan.sCustomField6D_rep);
                            xmlWriter.WriteAttributeString("Percent", dataLoan.sCustomField6Pc_rep);
                            xmlWriter.WriteAttributeString("Bit", GetBoolString(dataLoan.sCustomField6Bit));
                            xmlWriter.WriteAttributeString("Notes", dataLoan.sCustomField6Notes);
                            xmlWriter.WriteEndElement(); //</sCustomField>


                            xmlWriter.WriteStartElement("sCustomField");
                            xmlWriter.WriteAttributeString("Index", "7");
                            xmlWriter.WriteAttributeString("Desc", dataLoan.sCustomField7Desc);
                            xmlWriter.WriteAttributeString("Money", dataLoan.sCustomField7Money_rep);
                            xmlWriter.WriteAttributeString("Date", dataLoan.sCustomField7D_rep);
                            xmlWriter.WriteAttributeString("Percent", dataLoan.sCustomField7Pc_rep);
                            xmlWriter.WriteAttributeString("Bit", GetBoolString(dataLoan.sCustomField7Bit));
                            xmlWriter.WriteAttributeString("Notes", dataLoan.sCustomField7Notes);
                            xmlWriter.WriteEndElement(); //</sCustomField>


                            xmlWriter.WriteStartElement("sCustomField");
                            xmlWriter.WriteAttributeString("Index", "8");
                            xmlWriter.WriteAttributeString("Desc", dataLoan.sCustomField8Desc);
                            xmlWriter.WriteAttributeString("Money", dataLoan.sCustomField8Money_rep);
                            xmlWriter.WriteAttributeString("Date", dataLoan.sCustomField8D_rep);
                            xmlWriter.WriteAttributeString("Percent", dataLoan.sCustomField8Pc_rep);
                            xmlWriter.WriteAttributeString("Bit", GetBoolString(dataLoan.sCustomField8Bit));
                            xmlWriter.WriteAttributeString("Notes", dataLoan.sCustomField8Notes);
                            xmlWriter.WriteEndElement(); //</sCustomField>

                            xmlWriter.WriteStartElement("sCustomField");
                            xmlWriter.WriteAttributeString("Index", "9");
                            xmlWriter.WriteAttributeString("Desc", dataLoan.sCustomField9Desc);
                            xmlWriter.WriteAttributeString("Money", dataLoan.sCustomField9Money_rep);
                            xmlWriter.WriteAttributeString("Date", dataLoan.sCustomField9D_rep);
                            xmlWriter.WriteAttributeString("Percent", dataLoan.sCustomField9Pc_rep);
                            xmlWriter.WriteAttributeString("Bit", GetBoolString(dataLoan.sCustomField9Bit));
                            xmlWriter.WriteAttributeString("Notes", dataLoan.sCustomField9Notes);
                            xmlWriter.WriteEndElement(); //</sCustomField>

                            xmlWriter.WriteStartElement("sCustomField");
                            xmlWriter.WriteAttributeString("Index", "10");
                            xmlWriter.WriteAttributeString("Desc", dataLoan.sCustomField10Desc);
                            xmlWriter.WriteAttributeString("Money", dataLoan.sCustomField10Money_rep);
                            xmlWriter.WriteAttributeString("Date", dataLoan.sCustomField10D_rep);
                            xmlWriter.WriteAttributeString("Percent", dataLoan.sCustomField10Pc_rep);
                            xmlWriter.WriteAttributeString("Bit", GetBoolString(dataLoan.sCustomField10Bit));
                            xmlWriter.WriteAttributeString("Notes", dataLoan.sCustomField10Notes);
                            xmlWriter.WriteEndElement(); //</sCustomField>


                            xmlWriter.WriteStartElement("sCustomField");
                            xmlWriter.WriteAttributeString("Index", "11");
                            xmlWriter.WriteAttributeString("Desc", dataLoan.sCustomField11Desc);
                            xmlWriter.WriteAttributeString("Money", dataLoan.sCustomField11Money_rep);
                            xmlWriter.WriteAttributeString("Date", dataLoan.sCustomField11D_rep);
                            xmlWriter.WriteAttributeString("Percent", dataLoan.sCustomField11Pc_rep);
                            xmlWriter.WriteAttributeString("Bit", GetBoolString(dataLoan.sCustomField11Bit));
                            xmlWriter.WriteAttributeString("Notes", dataLoan.sCustomField11Notes);
                            xmlWriter.WriteEndElement(); //</sCustomField>

                            xmlWriter.WriteStartElement("sCustomField");
                            xmlWriter.WriteAttributeString("Index", "12");
                            xmlWriter.WriteAttributeString("Desc", dataLoan.sCustomField12Desc);
                            xmlWriter.WriteAttributeString("Money", dataLoan.sCustomField12Money_rep);
                            xmlWriter.WriteAttributeString("Date", dataLoan.sCustomField12D_rep);
                            xmlWriter.WriteAttributeString("Percent", dataLoan.sCustomField12Pc_rep);
                            xmlWriter.WriteAttributeString("Bit", GetBoolString(dataLoan.sCustomField12Bit));
                            xmlWriter.WriteAttributeString("Notes", dataLoan.sCustomField12Notes);
                            xmlWriter.WriteEndElement(); //</sCustomField>


                            xmlWriter.WriteStartElement("sCustomField");
                            xmlWriter.WriteAttributeString("Index", "13");
                            xmlWriter.WriteAttributeString("Desc", dataLoan.sCustomField13Desc);
                            xmlWriter.WriteAttributeString("Money", dataLoan.sCustomField13Money_rep);
                            xmlWriter.WriteAttributeString("Date", dataLoan.sCustomField13D_rep);
                            xmlWriter.WriteAttributeString("Percent", dataLoan.sCustomField13Pc_rep);
                            xmlWriter.WriteAttributeString("Bit", GetBoolString(dataLoan.sCustomField13Bit));
                            xmlWriter.WriteAttributeString("Notes", dataLoan.sCustomField13Notes);
                            xmlWriter.WriteEndElement(); //</sCustomField>


                            xmlWriter.WriteStartElement("sCustomField");
                            xmlWriter.WriteAttributeString("Index", "14");
                            xmlWriter.WriteAttributeString("Desc", dataLoan.sCustomField14Desc);
                            xmlWriter.WriteAttributeString("Money", dataLoan.sCustomField14Money_rep);
                            xmlWriter.WriteAttributeString("Date", dataLoan.sCustomField14D_rep);
                            xmlWriter.WriteAttributeString("Percent", dataLoan.sCustomField14Pc_rep);
                            xmlWriter.WriteAttributeString("Bit", GetBoolString(dataLoan.sCustomField14Bit));
                            xmlWriter.WriteAttributeString("Notes", dataLoan.sCustomField14Notes);
                            xmlWriter.WriteEndElement(); //</sCustomField>


                            xmlWriter.WriteStartElement("sCustomField");
                            xmlWriter.WriteAttributeString("Index", "15");
                            xmlWriter.WriteAttributeString("Desc", dataLoan.sCustomField15Desc);
                            xmlWriter.WriteAttributeString("Money", dataLoan.sCustomField15Money_rep);
                            xmlWriter.WriteAttributeString("Date", dataLoan.sCustomField15D_rep);
                            xmlWriter.WriteAttributeString("Percent", dataLoan.sCustomField15Pc_rep);
                            xmlWriter.WriteAttributeString("Bit", GetBoolString(dataLoan.sCustomField15Bit));
                            xmlWriter.WriteAttributeString("Notes", dataLoan.sCustomField15Notes);
                            xmlWriter.WriteEndElement(); //</sCustomField>


                            xmlWriter.WriteStartElement("sCustomField");
                            xmlWriter.WriteAttributeString("Index", "16");
                            xmlWriter.WriteAttributeString("Desc", dataLoan.sCustomField16Desc);
                            xmlWriter.WriteAttributeString("Money", dataLoan.sCustomField16Money_rep);
                            xmlWriter.WriteAttributeString("Date", dataLoan.sCustomField16D_rep);
                            xmlWriter.WriteAttributeString("Percent", dataLoan.sCustomField16Pc_rep);
                            xmlWriter.WriteAttributeString("Bit", GetBoolString(dataLoan.sCustomField16Bit));
                            xmlWriter.WriteAttributeString("Notes", dataLoan.sCustomField16Notes);
                            xmlWriter.WriteEndElement(); //</sCustomField>


                            xmlWriter.WriteStartElement("sCustomField");
                            xmlWriter.WriteAttributeString("Index", "17");
                            xmlWriter.WriteAttributeString("Desc", dataLoan.sCustomField17Desc);
                            xmlWriter.WriteAttributeString("Money", dataLoan.sCustomField17Money_rep);
                            xmlWriter.WriteAttributeString("Date", dataLoan.sCustomField17D_rep);
                            xmlWriter.WriteAttributeString("Percent", dataLoan.sCustomField17Pc_rep);
                            xmlWriter.WriteAttributeString("Bit", GetBoolString(dataLoan.sCustomField17Bit));
                            xmlWriter.WriteAttributeString("Notes", dataLoan.sCustomField17Notes);
                            xmlWriter.WriteEndElement(); //</sCustomField>


                            xmlWriter.WriteStartElement("sCustomField");
                            xmlWriter.WriteAttributeString("Index", "18");
                            xmlWriter.WriteAttributeString("Desc", dataLoan.sCustomField18Desc);
                            xmlWriter.WriteAttributeString("Money", dataLoan.sCustomField18Money_rep);
                            xmlWriter.WriteAttributeString("Date", dataLoan.sCustomField18D_rep);
                            xmlWriter.WriteAttributeString("Percent", dataLoan.sCustomField18Pc_rep);
                            xmlWriter.WriteAttributeString("Bit", GetBoolString(dataLoan.sCustomField18Bit));
                            xmlWriter.WriteAttributeString("Notes", dataLoan.sCustomField18Notes);
                            xmlWriter.WriteEndElement(); //</sCustomField>

                            xmlWriter.WriteStartElement("sCustomField");
                            xmlWriter.WriteAttributeString("Index", "19");
                            xmlWriter.WriteAttributeString("Desc", dataLoan.sCustomField19Desc);
                            xmlWriter.WriteAttributeString("Money", dataLoan.sCustomField19Money_rep);
                            xmlWriter.WriteAttributeString("Date", dataLoan.sCustomField19D_rep);
                            xmlWriter.WriteAttributeString("Percent", dataLoan.sCustomField19Pc_rep);
                            xmlWriter.WriteAttributeString("Bit", GetBoolString(dataLoan.sCustomField19Bit));
                            xmlWriter.WriteAttributeString("Notes", dataLoan.sCustomField19Notes);
                            xmlWriter.WriteEndElement(); //</sCustomField>

                            xmlWriter.WriteStartElement("sCustomField");
                            xmlWriter.WriteAttributeString("Index", "20");
                            xmlWriter.WriteAttributeString("Desc", dataLoan.sCustomField20Desc);
                            xmlWriter.WriteAttributeString("Money", dataLoan.sCustomField20Money_rep);
                            xmlWriter.WriteAttributeString("Date", dataLoan.sCustomField20D_rep);
                            xmlWriter.WriteAttributeString("Percent", dataLoan.sCustomField20Pc_rep);
                            xmlWriter.WriteAttributeString("Bit", GetBoolString(dataLoan.sCustomField20Bit));
                            xmlWriter.WriteAttributeString("Notes", dataLoan.sCustomField20Notes);
                            xmlWriter.WriteEndElement(); //</sCustomField>

                            #endregion
                        }

                        if (isIncludeEscrowSchedule)
                        {
                            WriteEscrowScheduleList(xmlWriter, dataLoan.m_convertLos, dataLoan.sEscrowScheduleList);
                        }

                        if (isIncludeBrokerLockAdjustments)
                        {
                            WriteLockAdjustments(xmlWriter, "sBrokerLockAdjustments", dataLoan.sBrokerLockAdjustments);
                        }
                        if (isIncludeInvestorLockAdjustments)
                        {
                            WriteLockAdjustments(xmlWriter, "sInvestorLockAdjustments", dataLoan.sInvestorLockAdjustments);
                        }
                        if (isIncludeInvestorInfo)
                        {
                            WriteInvestorInfo(xmlWriter, "sInvestorInfo", dataLoan.sInvestorInfo);
                        }
                        if (isIncludeServicingPayments)
                        {
                            // 11/18/2013 dd - OPM 137779 - Allow batch export to access servicing payment.
                            WriteServicingPayments(xmlWriter, dataLoan.sServicingPayments);
                        }
                        if (isIncludePurchaseAdviceFees)
                        {
                            WriteList(xmlWriter, dataLoan.sPurchaseAdviceFees);
                        }

                        if (isIncludePurchaseAdviceAdjustments)
                        {
                            WriteList(xmlWriter, dataLoan.sPurchaseAdviceAdjustments);
                        }
                        // 9/24/2013 AV - 139179 Add data layer to return the assigned loan officer's first and last name
                        if (isIncludeAssignedEmployees)
                        {
                            #region Generate Employee XML

                            CEmployeeFields[] assignedEmployees = new CEmployeeFields[] {
                                dataLoan.sEmployeeBrokerProcessor,
                                dataLoan.sEmployeeCallCenterAgent,
                                dataLoan.sEmployeeCloser,
                                dataLoan.sEmployeeCollateralAgent,
                                dataLoan.sEmployeeCreditAuditor,
                                dataLoan.sEmployeeDisclosureDesk,
                                dataLoan.sEmployeeDocDrawer,
                                dataLoan.sEmployeeExternalPostCloser,
                                dataLoan.sEmployeeExternalSecondary,
                                dataLoan.sEmployeeFunder,
                                dataLoan.sEmployeeInsuring,
                                dataLoan.sEmployeeJuniorProcessor,
                                dataLoan.sEmployeeJuniorUnderwriter,
                                dataLoan.sEmployeeLegalAuditor,
                                dataLoan.sEmployeeLenderAccExec,
                                dataLoan.sEmployeeLoanOfficerAssistant,
                                dataLoan.sEmployeeLoanOpener,
                                dataLoan.sEmployeeLoanRep,
                                dataLoan.sEmployeeLockDesk,
                                dataLoan.sEmployeeManager,
                                dataLoan.sEmployeePostCloser,
                                dataLoan.sEmployeeProcessor,
                                dataLoan.sEmployeePurchaser,
                                dataLoan.sEmployeeQCCompliance,
                                dataLoan.sEmployeeRealEstateAgent,
                                dataLoan.sEmployeeSecondary,
                                dataLoan.sEmployeeServicing,
                                dataLoan.sEmployeeShipper,
                                dataLoan.sEmployeeUnderwriter
                            };


                            foreach (CEmployeeFields field in assignedEmployees)
                            {
                                if (!field.IsValid)
                                {
                                    continue; //not assigned
                                }

                                xmlWriter.WriteStartElement("sAssignedEmployee");
                                xmlWriter.WriteAttributeString("Id", field.EmployeeId.ToString());
                                xmlWriter.WriteAttributeString("Email", field.Email);
                                xmlWriter.WriteAttributeString("Fax", field.Fax);
                                xmlWriter.WriteAttributeString("FirstName", field.FirstName);
                                xmlWriter.WriteAttributeString("FullName", field.FullName);
                                xmlWriter.WriteAttributeString("LastName", field.LastName);
                                xmlWriter.WriteAttributeString("Phone", field.Phone);
                                xmlWriter.WriteAttributeString("PmlUserBrokerNm", field.PmlUserBrokerNm);
                                xmlWriter.WriteAttributeString("Role", field.Role.ToString());
                                xmlWriter.WriteAttributeString("PmlBrokerCompanyId", field.PmlBrokerCompanyId);
                                xmlWriter.WriteEndElement();
                            }
                            #endregion
                        }

                        if (isIncludeAggregateEscrowAccount)
                        {
                            // 5/29/2014 dd - Export Aggregate Escrow Account Analysis table.
                            WriteAggregateEscrowAccount(xmlWriter, dataLoan.sAggregateEscrowAccount);
                        }

                        if (isIncludeLastDisclosedGfeArchive)
                        {
                            WriteGfeArchive(xmlWriter, "sLastDisclosedGfeArchive", new LendersOffice.Common.SerializationTypes.GFEFormattedArchive(dataLoan.LastDisclosedGFEArchive, dataLoan.m_convertLos));
                        }

                        if (isIncludeClosingCostSet)
                        {
                            Func<BaseClosingCostFee, bool> borrowerFilter = ClosingCostSetUtils.GetOrigCompDiscountPrepsSecGMIFilter(dataLoan.sClosingCostSet,
                                                                                                                                     false,
                                                                                                                                     dataLoan.sOriginatorCompensationPaymentSourceT,
                                                                                                                                     dataLoan.sDisclosureRegulationT,
                                                                                                                                     dataLoan.sGfeIsTPOTransaction);
                            WriteClosingCostSet(xmlWriter, dataLoan.sClosingCostSet, borrowerFilter);
                        }

                        if (isIncludeAdjustmentList)
                        {
                            WriteAdjustmentList(xmlWriter, dataLoan.sAdjustmentList);
                        }

                        if (isIncludeHousingExpenses)
                        {
                            this.WriteHousingExpensesCollection(xmlWriter, dataLoan.sHousingExpenses, dataLoan.m_convertLos);
                        }

                        if(isIncludeLoanEstimateDatesInfo)
                        {
                            WriteLoanEstimateDisclosuresCollection(xmlWriter, dataLoan.sLoanEstimateDatesInfo, dataLoan.m_convertLos, dataLoan.GetValidArchivesForLoanEstimateDates());
                        }

                        if(isIncludeClosingDisclosureDatesInfo)
                        {
                            WriteClosingDisclosuresCollection(xmlWriter, dataLoan.sClosingDisclosureDatesInfo, dataLoan.m_convertLos, dataLoan.GetValidArchivesForClosingDisclosureDates());
                        }

                        if (isIncludeToleranceCureDebug)
                        {
                            WriteToleranceCureDebug(xmlWriter, dataLoan.sToleranceCureDebug);
                        }

                        if (isIncludeTransactions)
                        {
                            WriteTransactions(xmlWriter, dataLoan.sTransactions);
                        }

                        if (isIncludeUcdDeliveryCollection)
                        {
                            WriteUcdDeliveryCollection(xmlWriter, dataLoan.sUcdDeliveryCollection);
                        }

                        if (appFieldSet.Count > 0)
                        {
                            for (int i = 0; i < dataLoan.nApps; i++)
                            {
                                xmlWriter.WriteStartElement("app");
                                CAppData dataApp = dataLoan.GetAppData(i);
                                foreach (var appFieldId in appFieldSet)
                                {
                                    string v = PageDataUtilities.GetValue(dataLoan, dataApp, appFieldId);
                                    xmlWriter.WriteAttributeString(appFieldId, v);
                                }

                                if (isIncludeAssetCollection)
                                {
                                    WriteAssetCollection(xmlWriter, dataApp.aAssetCollection);
                                }
                                if (isIncludeLiaCollection)
                                {
                                    WriteLiaCollection(xmlWriter, dataApp.aLiaCollection);
                                }

                                xmlWriter.WriteEndElement(); // </app>

                            }
                        }
                        xmlWriter.WriteEndElement(); // </loan>
                    }
                    catch (AccessDenied exc)
                    {
                        m_warningList.Add("Unable to retrieve loan id=" + sLId + ". " + exc.UserMessage);
                    }
                    catch (PageDataLoadDenied exc)
                    {
                        m_warningList.Add("Unable to retrieve loan id=" + sLId + ". " + exc.UserMessage);
                        xmlWriter.WriteEndElement(); // </result>
                        throw;
                    }
                }

                xmlWriter.WriteEndElement(); // </result>
            }
        }

        private void WriteLoanEstimateDisclosuresCollection(SanitizedXmlWriterWrapper xmlWriter, LoanEstimateDatesInfo leDatesInfo, LosConvert converter, Dictionary<Guid, ClosingCostArchive> validArchives)
        {
            if (leDatesInfo == null)
            {
                return;
            }

            xmlWriter.WriteStartElement("sLoanEstimateDatesInfo");

            foreach (LoanEstimateDates loanEstimate in leDatesInfo.LoanEstimateDatesList)
            {
                bool hasValidArchive = loanEstimate.ArchiveId == Guid.Empty || validArchives.ContainsKey(loanEstimate.ArchiveId);

                xmlWriter.WriteStartElement("LoanEstimateDates");
                xmlWriter.WriteAttributeString("CreatedDate", converter.ToDateTimeString(loanEstimate.CreatedDate));
                xmlWriter.WriteAttributeString("IssuedDate", converter.ToDateTimeString(loanEstimate.IssuedDate));
                xmlWriter.WriteAttributeString("DeliveryMethod", loanEstimate.DeliveryMethod.ToString("D"));
                xmlWriter.WriteAttributeString("ReceivedDate", converter.ToDateTimeString(loanEstimate.ReceivedDate));
                xmlWriter.WriteAttributeString("SignedDate", converter.ToDateTimeString(loanEstimate.SignedDate));
                xmlWriter.WriteAttributeString("IsInitial", GetBoolString(loanEstimate.IsInitial));
                xmlWriter.WriteAttributeString("IsManual", GetBoolString(loanEstimate.IsManual));
                xmlWriter.WriteAttributeString("ArchiveDate", converter.ToDateTimeString(loanEstimate.ArchiveDate));
                xmlWriter.WriteAttributeString("ArchiveId", loanEstimate.ArchiveId.ToString());
                xmlWriter.WriteAttributeString("TransactionId", loanEstimate.TransactionId);
                xmlWriter.WriteAttributeString("Valid", GetBoolString(hasValidArchive));
                xmlWriter.WriteAttributeString("DisclosedAprLckd", GetBoolString(loanEstimate.DisclosedAprLckd));
                xmlWriter.WriteAttributeString("DisclosedApr", loanEstimate.DisclosedApr_rep);
                xmlWriter.WriteAttributeString("LqbApr", loanEstimate.LqbApr_rep);
                xmlWriter.WriteAttributeString("DocVendorApr", loanEstimate.DocVendorApr_rep);
                xmlWriter.WriteAttributeString("LastDisclosedTRIDLoanProductDescription", loanEstimate.LastDisclosedTRIDLoanProductDescription);

                this.WriteDisclosureDatesByConsumerId(xmlWriter, loanEstimate.DisclosureDatesByConsumerId, converter);

                xmlWriter.WriteEndElement();
            }

            xmlWriter.WriteEndElement();
        }

        private void WriteClosingDisclosuresCollection(SanitizedXmlWriterWrapper xmlWriter, ClosingDisclosureDatesInfo cdDatesInfo, LosConvert converter, Dictionary<Guid, ClosingCostArchive> validArchives)
        {
            if (cdDatesInfo == null)
            {
                return;
            }

            xmlWriter.WriteStartElement("sClosingDisclosureDatesInfo");

            foreach (ClosingDisclosureDates cDisclosures in cdDatesInfo.ClosingDisclosureDatesList)
            {
                bool hasValidArchive = cDisclosures.ArchiveId == Guid.Empty || validArchives.ContainsKey(cDisclosures.ArchiveId);

                xmlWriter.WriteStartElement("ClosingDisclosureDates");
                xmlWriter.WriteAttributeString("CreatedDate", converter.ToDateTimeString(cDisclosures.CreatedDate));
                xmlWriter.WriteAttributeString("IssuedDate", converter.ToDateTimeString(cDisclosures.IssuedDate));
                xmlWriter.WriteAttributeString("DeliveryMethod", cDisclosures.DeliveryMethod.ToString("D"));
                xmlWriter.WriteAttributeString("ReceivedDate", converter.ToDateTimeString(cDisclosures.ReceivedDate));
                xmlWriter.WriteAttributeString("SignedDate", converter.ToDateTimeString(cDisclosures.SignedDate));
                xmlWriter.WriteAttributeString("IsInitial", GetBoolString(cDisclosures.IsInitial));
                xmlWriter.WriteAttributeString("IsPreview", GetBoolString(cDisclosures.IsPreview));
                xmlWriter.WriteAttributeString("IsFinal", GetBoolString(cDisclosures.IsFinal));
                xmlWriter.WriteAttributeString("IsManual", GetBoolString(cDisclosures.IsManual));
                xmlWriter.WriteAttributeString("ArchiveDate", converter.ToDateTimeString(cDisclosures.ArchiveDate));
                xmlWriter.WriteAttributeString("ArchiveId", cDisclosures.ArchiveId.ToString());
                xmlWriter.WriteAttributeString("TransactionId", cDisclosures.TransactionId);
                xmlWriter.WriteAttributeString("Valid", GetBoolString(hasValidArchive));
                xmlWriter.WriteAttributeString("UcdDocument", cDisclosures.UcdDocument.ToString());
                xmlWriter.WriteAttributeString("DisclosedAprLckd", GetBoolString(cDisclosures.DisclosedAprLckd));
                xmlWriter.WriteAttributeString("DisclosedApr", cDisclosures.DisclosedApr_rep);
                xmlWriter.WriteAttributeString("LqbApr", cDisclosures.LqbApr_rep);
                xmlWriter.WriteAttributeString("DocVendorApr", cDisclosures.DocVendorApr_rep);
                xmlWriter.WriteAttributeString("DocCode", cDisclosures.DocCode);
                xmlWriter.WriteAttributeString("IsPostClosing", GetBoolString(cDisclosures.IsPostClosing));
                xmlWriter.WriteAttributeString("LastDisclosedTRIDLoanProductDescription", cDisclosures.LastDisclosedTRIDLoanProductDescription);
                xmlWriter.WriteAttributeString("IsDisclosurePostClosingDueToCureForToleranceViolation", GetBoolString(cDisclosures.IsDisclosurePostClosingDueToCureForToleranceViolation));
                xmlWriter.WriteAttributeString("IsDisclosurePostClosingDueToNonNumericalClericalError", GetBoolString(cDisclosures.IsDisclosurePostClosingDueToNonNumericalClericalError));
                xmlWriter.WriteAttributeString("IsDisclosurePostClosingDueToNumericalChangeInAmountPaidBySeller", GetBoolString(cDisclosures.IsDisclosurePostClosingDueToNumericalChangeInAmountPaidBySeller));
                xmlWriter.WriteAttributeString("IsDisclosurePostClosingDueToNumericalChangeInAmountPaidByBorrower", GetBoolString(cDisclosures.IsDisclosurePostClosingDueToNumericalChangeInAmountPaidByBorrower));
                xmlWriter.WriteAttributeString("PostConsummationKnowledgeOfEventDate", converter.ToDateTimeString(cDisclosures.PostConsummationKnowledgeOfEventDate));
                xmlWriter.WriteAttributeString("PostConsummationRedisclosureReasonDate", converter.ToDateTimeString(cDisclosures.PostConsummationRedisclosureReasonDate));

                this.WriteDisclosureDatesByConsumerId(xmlWriter, cDisclosures.DisclosureDatesByConsumerId, converter);

                xmlWriter.WriteEndElement();
            }

            xmlWriter.WriteEndElement();
        }

        /// <summary>
        /// Writes disclosure dates by consumer ID to the specified XML writer.
        /// </summary>
        /// <param name="xmlWriter">
        /// The XML writer.
        /// </param>
        /// <param name="disclosureDatesByConsumerId">
        /// The disclosure dates by consumer ID.
        /// </param>
        /// <param name="converter">
        /// The conversion object.
        /// </param>
        private void WriteDisclosureDatesByConsumerId(
            SanitizedXmlWriterWrapper xmlWriter, 
            ReadOnlyCollection<KeyValuePair<Guid, BorrowerDisclosureDates>> disclosureDatesByConsumerId,
            LosConvert converter)
        {
            foreach (var disclosureDatesPair in disclosureDatesByConsumerId)
            {
                var disclosureDates = disclosureDatesPair.Value;

                xmlWriter.WriteStartElement("DisclosureDatesByConsumerId");

                xmlWriter.WriteAttributeString("ConsumerId", disclosureDatesPair.Key.ToString());
                xmlWriter.WriteAttributeString("ConsumerName", disclosureDates.ConsumerName);
                xmlWriter.WriteAttributeString("ExcludeFromCalculations", GetBoolString(disclosureDates.ExcludeFromCalculations));
                xmlWriter.WriteAttributeString("IssuedDate", converter.ToDateTimeString(disclosureDates.IssuedDate ?? CDateTime.InvalidDateTime));
                xmlWriter.WriteAttributeString("DeliveryMethod", GetEnumString(disclosureDates.DeliveryMethod));
                xmlWriter.WriteAttributeString("ReceivedDate", converter.ToDateTimeString(disclosureDates.ReceivedDate ?? CDateTime.InvalidDateTime));
                xmlWriter.WriteAttributeString("SignedDate", converter.ToDateTimeString(disclosureDates.SignedDate ?? CDateTime.InvalidDateTime));

                xmlWriter.WriteEndElement(); // </DisclosureDatesByConsumerId>
            }
        }

        private void WriteHousingExpensesCollection(SanitizedXmlWriterWrapper xmlWriter, HousingExpenses expenses, LosConvert converter)
        {
            if (expenses == null)
            {
                return;
            }

            xmlWriter.WriteStartElement("sHousingExpenses"); // <sHousingExpenses>

            List<BaseHousingExpense> allExpenses = new List<BaseHousingExpense>(expenses.ExpensesToUse);

            foreach (BaseHousingExpense expense in allExpenses)
            {
                this.WriteHousingExpense(xmlWriter, expense, converter);
            }

            xmlWriter.WriteEndElement(); // </sHousingExpenses>
        }

        private void WriteHousingExpense(SanitizedXmlWriterWrapper xmlWriter, BaseHousingExpense expense, LosConvert converter)
        {
            if (expense == null)
            {
                return;
            }

            xmlWriter.WriteStartElement("Expense"); // <Expense>

            xmlWriter.WriteAttributeString("ExpensePrefix", expense.ExpensePrefix);
            xmlWriter.WriteAttributeString("DefaultExpenseDescription", expense.DefaultExpenseDescription);
            xmlWriter.WriteAttributeString("ExpenseDescription", expense.ExpenseDescription);
            xmlWriter.WriteAttributeString("CalculatedHousingExpenseType", expense.CalculatedHousingExpenseType.ToString("D"));
            xmlWriter.WriteAttributeString("HousingExpenseType", expense.HousingExpenseType.ToString("D"));
            xmlWriter.WriteAttributeString("TaxType", expense.TaxType.ToString("D"));
            xmlWriter.WriteAttributeString("AnnualAmtCalcType", expense.AnnualAmtCalcType.ToString("D"));
            xmlWriter.WriteAttributeString("AnnualAmt", expense.AnnualAmt_rep);
            xmlWriter.WriteAttributeString("MonthlyAmtTotal", expense.MonthlyAmtTotal_rep);
            xmlWriter.WriteAttributeString("MonthlyAmtServicing", expense.MonthlyAmtServicing_rep);
            xmlWriter.WriteAttributeString("AnnualAmtCalcBasePerc", expense.AnnualAmtCalcBasePerc_rep);
            xmlWriter.WriteAttributeString("AnnualAmtCalcBaseType", expense.AnnualAmtCalcBaseType.ToString("D"));
            xmlWriter.WriteAttributeString("AnnualAmtCalcBaseAmt", expense.AnnualAmtCalcBaseAmt_rep);
            xmlWriter.WriteAttributeString("MonthlyAmtFixedAmt", expense.MonthlyAmtFixedAmt_rep);
            xmlWriter.WriteAttributeString("IsPrepaid", GetBoolString(expense.IsPrepaid));
            xmlWriter.WriteAttributeString("PrepaidMonths", expense.PrepaidMonths_rep);
            xmlWriter.WriteAttributeString("PrepaidAmt", expense.PrepaidAmt_rep);
            xmlWriter.WriteAttributeString("IsEscrowedAtClosing", GetBoolString(expense.IsEscrowedAtClosing == E_TriState.Yes ? true : false));
            xmlWriter.WriteAttributeString("ReserveMonths", expense.ReserveMonths_rep);
            xmlWriter.WriteAttributeString("ReserveMonthsLckd", GetBoolString(expense.ReserveMonthsLckd));
            xmlWriter.WriteAttributeString("ReserveAmt", expense.ReserveAmt_rep);
            xmlWriter.WriteAttributeString("DisbursementRepIntervalType", expense.DisbursementRepInterval.ToString("D"));
            xmlWriter.WriteAttributeString("CustomExpenseLineNumType", expense.CustomExpenseLineNum.ToString("D"));
            xmlWriter.WriteAttributeString("LineNumAsString", expense.LineNumAsString);
            xmlWriter.WriteAttributeString("CushionMonths", expense.CushionMonths_rep);
            xmlWriter.WriteAttributeString("SchedJan", converter.ToCountString(expense.DisbursementScheduleMonths[1]));
            xmlWriter.WriteAttributeString("SchedFeb", converter.ToCountString(expense.DisbursementScheduleMonths[2]));
            xmlWriter.WriteAttributeString("SchedMar", converter.ToCountString(expense.DisbursementScheduleMonths[3]));
            xmlWriter.WriteAttributeString("SchedApr", converter.ToCountString(expense.DisbursementScheduleMonths[4]));
            xmlWriter.WriteAttributeString("SchedMay", converter.ToCountString(expense.DisbursementScheduleMonths[5]));
            xmlWriter.WriteAttributeString("SchedJun", converter.ToCountString(expense.DisbursementScheduleMonths[6]));
            xmlWriter.WriteAttributeString("SchedJul", converter.ToCountString(expense.DisbursementScheduleMonths[7]));
            xmlWriter.WriteAttributeString("SchedAug", converter.ToCountString(expense.DisbursementScheduleMonths[8]));
            xmlWriter.WriteAttributeString("SchedSep", converter.ToCountString(expense.DisbursementScheduleMonths[9]));
            xmlWriter.WriteAttributeString("SchedOct", converter.ToCountString(expense.DisbursementScheduleMonths[10]));
            xmlWriter.WriteAttributeString("SchedNov", converter.ToCountString(expense.DisbursementScheduleMonths[11]));
            xmlWriter.WriteAttributeString("SchedDec", converter.ToCountString(expense.DisbursementScheduleMonths[12]));

            IEnumerable<SingleDisbursement> disbList;
            if (expense.AnnualAmtCalcType == E_AnnualAmtCalcTypeT.LoanValues)
            {
                disbList = expense.ProjectedDisbursements.OrderBy(o => o.DueDate);
            }
            else
            {
                disbList = expense.ActualDisbursementsList;
            }

            foreach (SingleDisbursement disbursement in disbList)
            {
                if (disbursement == null)
                {
                    continue;
                }

                xmlWriter.WriteStartElement("Disbursement"); // <Disbursement>

                xmlWriter.WriteAttributeString("DisbId", disbursement.DisbId.ToString());
                xmlWriter.WriteAttributeString("DisbPaidDateType", disbursement.DisbPaidDateType.ToString("D"));
                xmlWriter.WriteAttributeString("DisbursementType", disbursement.DisbursementType.ToString("D"));
                xmlWriter.WriteAttributeString("DueDate", disbursement.DueDate_rep);
                xmlWriter.WriteAttributeString("DisbursementAmt", disbursement.DisbursementAmt_rep);
                xmlWriter.WriteAttributeString("CoveredMonths", disbursement.CoveredMonths_rep);
                xmlWriter.WriteAttributeString("PaidByType", disbursement.PaidBy.ToString("D"));
                xmlWriter.WriteAttributeString("PaidDate", disbursement.PaidDate_rep);
                xmlWriter.WriteAttributeString("BillingPeriodStartD", disbursement.BillingPeriodStartD_rep);
                xmlWriter.WriteAttributeString("BillingPeriodEndD", disbursement.BillingPeriodEndD_rep);

                xmlWriter.WriteEndElement(); // </Disbursement>
            }

            xmlWriter.WriteEndElement(); // </Expense>
        }

        private void WriteAdjustmentList(SanitizedXmlWriterWrapper xmlWriter, ObjLib.AdjustmentsAndOtherCredits.AdjustmentList adjustments)
        {
            if (adjustments == null)
            {
                return;
            }
            xmlWriter.WriteStartElement("sAdjustmentList"); // <sAdjustmentList>

            foreach (var adjustment in adjustments)
            {
                xmlWriter.WriteStartElement("Adjustment"); // <Adjustment>

                xmlWriter.WriteAttributeString("AdjustmentType", adjustment.AdjustmentType.ToString("D"));
                xmlWriter.WriteAttributeString("Amount", adjustment.Amount_rep);
                xmlWriter.WriteAttributeString("AmountToBorrower", adjustment.AmountToBorrower_rep);
                xmlWriter.WriteAttributeString("Description", adjustment.Description);
                xmlWriter.WriteAttributeString("DFLP", GetBoolString(adjustment.DFLP));
                xmlWriter.WriteAttributeString("IncludeAdjustmentInLeCdForThisLien", GetBoolString(adjustment.IncludeAdjustmentInLeCdForThisLien));
                xmlWriter.WriteAttributeString("IsPaidFromBorrower", GetBoolString(adjustment.IsPaidFromBorrower));
                xmlWriter.WriteAttributeString("IsPaidFromSeller", GetBoolString(adjustment.IsPaidFromSeller));
                xmlWriter.WriteAttributeString("IsPaidToBorrower", GetBoolString(adjustment.IsPaidToBorrower));
                xmlWriter.WriteAttributeString("IsPaidToSeller", GetBoolString(adjustment.IsPaidToSeller));
                xmlWriter.WriteAttributeString("IsPopulateToLineLOn1003", GetBoolString(adjustment.IsPopulateToLineLOn1003));
                xmlWriter.WriteAttributeString("IsPopulateToLineLOn1003Disabled", GetBoolString(adjustment.IsPopulateToLineLOn1003Disabled));
                xmlWriter.WriteAttributeString("PaidFromParty", adjustment.PaidFromParty.ToString("D"));
                xmlWriter.WriteAttributeString("PaidToParty", adjustment.PaidToParty.ToString("D"));
                xmlWriter.WriteAttributeString("POC", GetBoolString(adjustment.POC));
                xmlWriter.WriteAttributeString("PopulateTo", adjustment.PopulateTo.ToString("D"));
                xmlWriter.WriteAttributeString("SellerAmount", adjustment.SellerAmount_rep);                

                xmlWriter.WriteEndElement(); // </Adjustment>
            }
        
            xmlWriter.WriteEndElement(); // </sAdjustmentList>
        }

        private void WriteClosingCostSet(SanitizedXmlWriterWrapper xmlWriter, BorrowerClosingCostSet closingCostSet, Func<BaseClosingCostFee, bool> borrowerSetFilter)
        {
            if (null == closingCostSet)
            {
                return;
            }
            xmlWriter.WriteStartElement("sClosingCostSet");

            foreach (BorrowerClosingCostFee fee in closingCostSet.GetFees(borrowerSetFilter))
            {
                //xmlWriter.WriteAttributeString("Val", regularAsset.Val_rep);
                xmlWriter.WriteStartElement("ClosingCostFee");
                xmlWriter.WriteAttributeString("Id", fee.UniqueId.ToString());
                xmlWriter.WriteAttributeString("HudLine", fee.HudLine_rep);
                xmlWriter.WriteAttributeString("ClosingCostFeeTypeId", fee.ClosingCostFeeTypeId.ToString());
                xmlWriter.WriteAttributeString("Description", fee.Description);
                xmlWriter.WriteAttributeString("OriginalDescription", fee.OriginalDescription);
                xmlWriter.WriteAttributeString("LegacyGfeFieldT", fee.LegacyGfeFieldT.ToString("D"));
                xmlWriter.WriteAttributeString("MismoFeeT", fee.MismoFeeT.ToString("D"));
                xmlWriter.WriteAttributeString("IsApr", GetBoolString(fee.IsApr));
                xmlWriter.WriteAttributeString("IsFhaAllowable", GetBoolString(fee.IsFhaAllowable));
                xmlWriter.WriteAttributeString("IsVaAllowable", GetBoolString(fee.IsVaAllowable));
                xmlWriter.WriteAttributeString("GfeSectionT", fee.GfeSectionT.ToString("D"));
                xmlWriter.WriteAttributeString("IntegratedDisclosureSectionT", fee.IntegratedDisclosureSectionT.ToString("D"));
                xmlWriter.WriteAttributeString("IsTitleFee", GetBoolString(fee.IsTitleFee));
                xmlWriter.WriteAttributeString("IsOptional", GetBoolString(fee.IsOptional));
                xmlWriter.WriteAttributeString("IsThirdParty", GetBoolString(fee.IsThirdParty));
                xmlWriter.WriteAttributeString("IsShowQmWarning", GetBoolString(fee.IsShowQmWarning));
                xmlWriter.WriteAttributeString("CanShop", GetBoolString(fee.CanShop));
                xmlWriter.WriteAttributeString("DidShop", GetBoolString(fee.DidShop));
                xmlWriter.WriteAttributeString("IsAffiliate", GetBoolString(fee.IsAffiliate));
                xmlWriter.WriteAttributeString("GfeProviderChoiceT", fee.GfeProviderChoiceT.ToString("D"));
                xmlWriter.WriteAttributeString("ProviderChosenByLender", fee.ProviderChosenByLender.ToString("D"));
                xmlWriter.WriteAttributeString("Beneficiary", fee.Beneficiary.ToString("D"));
                xmlWriter.WriteAttributeString("BeneficiaryDescription", fee.BeneficiaryDescription);
                xmlWriter.WriteAttributeString("BeneficiaryAgentId", fee.BeneficiaryAgentId.ToString());
                xmlWriter.WriteAttributeString("GfeResponsiblePartyT", fee.GfeResponsiblePartyT.ToString("D"));
                xmlWriter.WriteAttributeString("TotalAmount", fee.TotalAmount_rep);
                xmlWriter.WriteAttributeString("IsSystemLegacyFee", GetBoolString(fee.IsSystemLegacyFee));
                xmlWriter.WriteAttributeString("IsIncludedInQm", GetBoolString(fee.IsIncludedInQm));
                xmlWriter.WriteAttributeString("Percent_rep", fee.Percent_rep);
                xmlWriter.WriteAttributeString("PercentBaseT", fee.PercentBaseT.ToString("D"));
                xmlWriter.WriteAttributeString("BaseAmount", fee.BaseAmount_rep);
                xmlWriter.WriteAttributeString("NumberOfPeriods", fee.NumberOfPeriods_rep);
                xmlWriter.WriteAttributeString("FormulaT", fee.FormulaT.ToString("D"));
                xmlWriter.WriteAttributeString("QmAmount", fee.QmAmount_rep);
                xmlWriter.WriteAttributeString("FinancedQmAmount", fee.FinancedQmAmount_rep);
                xmlWriter.WriteAttributeString("IsBonaFide", GetBoolString(fee.IsBonaFide));
                xmlWriter.WriteAttributeString("DFLP", GetBoolString(fee.Dflp));

                foreach (var payment in fee.Payments)
                {
                    xmlWriter.WriteStartElement("Payment");
                    xmlWriter.WriteAttributeString("Id", payment.Id.ToString());
                    xmlWriter.WriteAttributeString("Amount", payment.Amount_rep);
                    xmlWriter.WriteAttributeString("Entity", payment.Entity.ToString("D"));
                    xmlWriter.WriteAttributeString("PaidByT", payment.PaidByT.ToString("D"));
                    xmlWriter.WriteAttributeString("GfeClosingCostFeePaymentTimingT", payment.GfeClosingCostFeePaymentTimingT.ToString("D"));
                    xmlWriter.WriteAttributeString("PaymentDate", payment.PaymentDate_rep);
                    xmlWriter.WriteAttributeString("IsFinanced", GetBoolString(payment.IsFinanced));
                    xmlWriter.WriteAttributeString("IsMade", GetBoolString(payment.IsMade));
                    xmlWriter.WriteAttributeString("IsSystemGenerated", GetBoolString(payment.IsSystemGenerated));
                    xmlWriter.WriteAttributeString("ResponsiblePartyT", payment.ResponsiblePartyT.ToString("D"));
                    xmlWriter.WriteEndElement(); // </Payment>
                }

                xmlWriter.WriteEndElement(); // </ClosingCostFee>
            }
            xmlWriter.WriteEndElement(); // </sClosingCostSet>
        }

        private void WriteGfeArchive(SanitizedXmlWriterWrapper xmlWriter, string elementName, LendersOffice.Common.SerializationTypes.GFEFormattedArchive gfeArchive)
        {
            if (gfeArchive.GfeArchive == null)
            {
                return;
            }

            xmlWriter.WriteStartElement(elementName);
            xmlWriter.WriteAttributeString("DateArchived", gfeArchive.DateArchived);
            xmlWriter.WriteAttributeString("s1006ProHExp", gfeArchive.s1006ProHExp_rep);
            xmlWriter.WriteAttributeString("s1006ProHExpDesc", gfeArchive.s1006ProHExpDesc);
            xmlWriter.WriteAttributeString("s1006Rsrv", gfeArchive.s1006Rsrv_rep);
            xmlWriter.WriteAttributeString("s1006RsrvMon", gfeArchive.s1006RsrvMon_rep);
            xmlWriter.WriteAttributeString("s1006RsrvMonLckd", GetBoolString(gfeArchive.s1006RsrvMonLckd));
            xmlWriter.WriteAttributeString("s1006RsrvProps", gfeArchive.s1006RsrvProps.ToString());
            xmlWriter.WriteAttributeString("s1007ProHExp", gfeArchive.s1007ProHExp_rep);
            xmlWriter.WriteAttributeString("s1007ProHExpDesc", gfeArchive.s1007ProHExpDesc);
            xmlWriter.WriteAttributeString("s1007Rsrv", gfeArchive.s1007Rsrv_rep);
            xmlWriter.WriteAttributeString("s1007RsrvMon", gfeArchive.s1007RsrvMon_rep);
            xmlWriter.WriteAttributeString("s1007RsrvMonLckd", GetBoolString(gfeArchive.s1007RsrvMonLckd));
            xmlWriter.WriteAttributeString("s1007RsrvProps", gfeArchive.s1007RsrvProps.ToString());
            xmlWriter.WriteAttributeString("s800U1F", gfeArchive.s800U1F_rep);
            xmlWriter.WriteAttributeString("s800U1FDesc", gfeArchive.s800U1FDesc);
            xmlWriter.WriteAttributeString("s800U1FGfeSection", GetEnumString(gfeArchive.s800U1FGfeSection));
            xmlWriter.WriteAttributeString("s800U1FPaidTo", gfeArchive.s800U1FPaidTo);
            xmlWriter.WriteAttributeString("s800U1FProps", gfeArchive.s800U1FProps.ToString());
            xmlWriter.WriteAttributeString("s800U2F", gfeArchive.s800U2F_rep);
            xmlWriter.WriteAttributeString("s800U2FDesc", gfeArchive.s800U2FDesc);
            xmlWriter.WriteAttributeString("s800U2FGfeSection", GetEnumString(gfeArchive.s800U2FGfeSection));
            xmlWriter.WriteAttributeString("s800U2FPaidTo", gfeArchive.s800U2FPaidTo);
            xmlWriter.WriteAttributeString("s800U2FProps", gfeArchive.s800U2FProps.ToString());
            xmlWriter.WriteAttributeString("s800U3F", gfeArchive.s800U3F_rep);
            xmlWriter.WriteAttributeString("s800U3FDesc", gfeArchive.s800U3FDesc);
            xmlWriter.WriteAttributeString("s800U3FGfeSection", GetEnumString(gfeArchive.s800U3FGfeSection));
            xmlWriter.WriteAttributeString("s800U3FPaidTo", gfeArchive.s800U3FPaidTo);
            xmlWriter.WriteAttributeString("s800U3FProps", gfeArchive.s800U3FProps.ToString());
            xmlWriter.WriteAttributeString("s800U4F", gfeArchive.s800U4F_rep);
            xmlWriter.WriteAttributeString("s800U4FDesc", gfeArchive.s800U4FDesc);
            xmlWriter.WriteAttributeString("s800U4FGfeSection", GetEnumString(gfeArchive.s800U4FGfeSection));
            xmlWriter.WriteAttributeString("s800U4FPaidTo", gfeArchive.s800U4FPaidTo);
            xmlWriter.WriteAttributeString("s800U4FProps", gfeArchive.s800U4FProps.ToString());
            xmlWriter.WriteAttributeString("s800U5F", gfeArchive.s800U5F_rep);
            xmlWriter.WriteAttributeString("s800U5FDesc", gfeArchive.s800U5FDesc);
            xmlWriter.WriteAttributeString("s800U5FGfeSection", GetEnumString(gfeArchive.s800U5FGfeSection));
            xmlWriter.WriteAttributeString("s800U5FPaidTo", gfeArchive.s800U5FPaidTo);
            xmlWriter.WriteAttributeString("s800U5FProps", gfeArchive.s800U5FProps.ToString());
            xmlWriter.WriteAttributeString("s900U1Pia", gfeArchive.s900U1Pia_rep);
            xmlWriter.WriteAttributeString("s900U1PiaDesc", gfeArchive.s900U1PiaDesc);
            xmlWriter.WriteAttributeString("s900U1PiaGfeSection", GetEnumString(gfeArchive.s900U1PiaGfeSection));
            xmlWriter.WriteAttributeString("s900U1PiaProps", gfeArchive.s900U1PiaProps.ToString());
            xmlWriter.WriteAttributeString("s904Pia", gfeArchive.s904Pia_rep);
            xmlWriter.WriteAttributeString("s904PiaDesc", gfeArchive.s904PiaDesc);
            xmlWriter.WriteAttributeString("s904PiaGfeSection", GetEnumString(gfeArchive.s904PiaGfeSection));
            xmlWriter.WriteAttributeString("s904PiaProps", gfeArchive.s904PiaProps.ToString());
            xmlWriter.WriteAttributeString("sAggregateAdjRsrv", gfeArchive.sAggregateAdjRsrv_rep);
            xmlWriter.WriteAttributeString("sAggregateAdjRsrvLckd", GetBoolString(gfeArchive.sAggregateAdjRsrvLckd));
            xmlWriter.WriteAttributeString("sAggregateAdjRsrvProps", gfeArchive.sAggregateAdjRsrvProps.ToString());
            xmlWriter.WriteAttributeString("sApprF", gfeArchive.sApprF_rep);
            xmlWriter.WriteAttributeString("sApprFPaid", GetBoolString(gfeArchive.sApprFPaid));
            xmlWriter.WriteAttributeString("sApprFPaidTo", gfeArchive.sApprFPaidTo);
            xmlWriter.WriteAttributeString("sApprFProps", gfeArchive.sApprFProps.ToString());
            xmlWriter.WriteAttributeString("sAttorneyF", gfeArchive.sAttorneyF_rep);
            xmlWriter.WriteAttributeString("sAttorneyFGfeSection", GetEnumString(gfeArchive.sAttorneyFGfeSection));
            xmlWriter.WriteAttributeString("sAttorneyFPaidTo", gfeArchive.sAttorneyFPaidTo);
            xmlWriter.WriteAttributeString("sAttorneyFProps", gfeArchive.sAttorneyFProps.ToString());
            xmlWriter.WriteAttributeString("sBranchChannelT", GetEnumString(gfeArchive.sBranchChannelT));
            xmlWriter.WriteAttributeString("sCcTemplateNm", gfeArchive.sCcTemplateNm);
            xmlWriter.WriteAttributeString("sConsummationD", gfeArchive.sConsummationD_rep);
            xmlWriter.WriteAttributeString("sCountyRtc", gfeArchive.sCountyRtc_rep);
            xmlWriter.WriteAttributeString("sCountyRtcBaseT", GetEnumString(gfeArchive.sCountyRtcBaseT));
            xmlWriter.WriteAttributeString("sCountyRtcDesc", gfeArchive.sCountyRtcDesc);
            xmlWriter.WriteAttributeString("sCountyRtcMb", gfeArchive.sCountyRtcMb_rep);
            xmlWriter.WriteAttributeString("sCountyRtcPc", gfeArchive.sCountyRtcPc_rep);
            xmlWriter.WriteAttributeString("sCountyRtcProps", gfeArchive.sCountyRtcProps.ToString());
            xmlWriter.WriteAttributeString("sCrF", gfeArchive.sCrF_rep);
            xmlWriter.WriteAttributeString("sCrFPaid", GetBoolString(gfeArchive.sCrFPaid));
            xmlWriter.WriteAttributeString("sCrFPaidTo", gfeArchive.sCrFPaidTo);
            xmlWriter.WriteAttributeString("sCrFProps", gfeArchive.sCrFProps.ToString());
            xmlWriter.WriteAttributeString("sDaysInYr", gfeArchive.sDaysInYr_rep);
            xmlWriter.WriteAttributeString("sDocPrepF", gfeArchive.sDocPrepF_rep);
            xmlWriter.WriteAttributeString("sDocPrepFGfeSection", GetEnumString(gfeArchive.sDocPrepFGfeSection));
            xmlWriter.WriteAttributeString("sDocPrepFPaidTo", gfeArchive.sDocPrepFPaidTo);
            xmlWriter.WriteAttributeString("sDocPrepFProps", gfeArchive.sDocPrepFProps.ToString());
            xmlWriter.WriteAttributeString("sDue", gfeArchive.sDue_rep);
            xmlWriter.WriteAttributeString("sEscrowF", gfeArchive.sEscrowF_rep);
            xmlWriter.WriteAttributeString("sEscrowFGfeSection", GetEnumString(gfeArchive.sEscrowFGfeSection));
            xmlWriter.WriteAttributeString("sEscrowFProps", gfeArchive.sEscrowFProps.ToString());
            xmlWriter.WriteAttributeString("sEscrowFTable", gfeArchive.sEscrowFTable);
            xmlWriter.WriteAttributeString("sEstCloseD", gfeArchive.sEstCloseD_rep);
            xmlWriter.WriteAttributeString("sFfUfMipIsBeingFinanced", GetBoolString(gfeArchive.sFfUfMipIsBeingFinanced));
            xmlWriter.WriteAttributeString("sFinalLAmt", gfeArchive.sFinalLAmt_rep);
            xmlWriter.WriteAttributeString("sFinMethT", GetEnumString(gfeArchive.sFinMethT));
            xmlWriter.WriteAttributeString("sFloodCertificationDeterminationT", GetEnumString(gfeArchive.sFloodCertificationDeterminationT));
            xmlWriter.WriteAttributeString("sFloodCertificationF", gfeArchive.sFloodCertificationF_rep);
            xmlWriter.WriteAttributeString("sFloodCertificationFPaidTo", gfeArchive.sFloodCertificationFPaidTo);
            xmlWriter.WriteAttributeString("sFloodCertificationFProps", gfeArchive.sFloodCertificationFProps.ToString());
            xmlWriter.WriteAttributeString("sFloodInsRsrv", gfeArchive.sFloodInsRsrv_rep);
            xmlWriter.WriteAttributeString("sFloodInsRsrvMon", gfeArchive.sFloodInsRsrvMon_rep);
            xmlWriter.WriteAttributeString("sFloodInsRsrvMonLckd", GetBoolString(gfeArchive.sFloodInsRsrvMonLckd));
            xmlWriter.WriteAttributeString("sFloodInsRsrvProps", gfeArchive.sFloodInsRsrvProps.ToString());
            xmlWriter.WriteAttributeString("sGfeAdjOriginationCharge", gfeArchive.sGfeAdjOriginationCharge_rep);
            xmlWriter.WriteAttributeString("sGfeBalloonDueInYrs", gfeArchive.sGfeBalloonDueInYrs);
            xmlWriter.WriteAttributeString("sGfeBalloonPmt", gfeArchive.sGfeBalloonPmt_rep);
            xmlWriter.WriteAttributeString("sGfeCanLoanBalanceIncrease", GetBoolString(gfeArchive.sGfeCanLoanBalanceIncrease));
            xmlWriter.WriteAttributeString("sGfeCanPaymentIncrease", GetBoolString(gfeArchive.sGfeCanPaymentIncrease));
            xmlWriter.WriteAttributeString("sGfeCanRateIncrease", GetBoolString(gfeArchive.sGfeCanRateIncrease));
            xmlWriter.WriteAttributeString("sGfeCreditLenderPaidItemF", gfeArchive.sGfeCreditLenderPaidItemF_rep);
            xmlWriter.WriteAttributeString("sGfeCreditLenderPaidItemT", GetEnumString(gfeArchive.sGfeCreditLenderPaidItemT));
            xmlWriter.WriteAttributeString("sGfeDailyInterestTotalFee", gfeArchive.sGfeDailyInterestTotalFee_rep);
            xmlWriter.WriteAttributeString("sGfeDiscountPointF", gfeArchive.sGfeDiscountPointF_rep);
            xmlWriter.WriteAttributeString("sGfeDiscountPointFPc", gfeArchive.sGfeDiscountPointFPc_rep);
            xmlWriter.WriteAttributeString("sGfeDiscountPointFProps", gfeArchive.sGfeDiscountPointFProps.ToString());
            xmlWriter.WriteAttributeString("sGfeEstScAvailTillD_Date", gfeArchive.sGfeEstScAvailTillD_Date);
            xmlWriter.WriteAttributeString("sGfeEstScAvailTillD_Time", gfeArchive.sGfeEstScAvailTillD_Time);
            xmlWriter.WriteAttributeString("sGfeEstScAvailTillDLckd", GetBoolString(gfeArchive.sGfeEstScAvailTillDLckd));
            xmlWriter.WriteAttributeString("sGfeEstScAvailTillDTimeZoneT", GetEnumString(gfeArchive.sGfeEstScAvailTillDTimeZoneT));
            xmlWriter.WriteAttributeString("sGfeFirstAdjProThisMPmtAndMIns", gfeArchive.sGfeFirstAdjProThisMPmtAndMIns_rep);
            xmlWriter.WriteAttributeString("sGfeFirstInterestChangeIn", gfeArchive.sGfeFirstInterestChangeIn);
            xmlWriter.WriteAttributeString("sGfeFirstPaymentChangeIn", gfeArchive.sGfeFirstPaymentChangeIn);
            xmlWriter.WriteAttributeString("sGfeGovtRecTotalFee", gfeArchive.sGfeGovtRecTotalFee_rep);
            xmlWriter.WriteAttributeString("sGfeHavePpmtPenalty", gfeArchive.sGfeHavePpmtPenalty_rep);
            xmlWriter.WriteAttributeString("sGfeHomeOwnerInsuranceTotalFee", gfeArchive.sGfeHomeOwnerInsuranceTotalFee_rep);
            xmlWriter.WriteAttributeString("sGfeInitialDisclosureD", gfeArchive.sGfeInitialDisclosureD_rep);
            xmlWriter.WriteAttributeString("sGfeInitialImpoundDeposit", gfeArchive.sGfeInitialImpoundDeposit_rep);
            xmlWriter.WriteAttributeString("sGfeIsBalloon", gfeArchive.sGfeIsBalloon_rep);
            xmlWriter.WriteAttributeString("sGfeIsTPOTransaction", GetBoolString(gfeArchive.sGfeIsTPOTransaction));
            xmlWriter.WriteAttributeString("sGfeLenderCreditF", gfeArchive.sGfeLenderCreditF_rep);
            xmlWriter.WriteAttributeString("sGfeLenderCreditFPc", gfeArchive.sGfeLenderCreditFPc_rep);
            xmlWriter.WriteAttributeString("sGfeLenderCreditFProps", gfeArchive.sGfeLenderCreditFProps.ToString());
            xmlWriter.WriteAttributeString("sGfeLenderTitleTotalFee", gfeArchive.sGfeLenderTitleTotalFee_rep);
            xmlWriter.WriteAttributeString("sGfeLockPeriodBeforeSettlement", gfeArchive.sGfeLockPeriodBeforeSettlement_rep);
            xmlWriter.WriteAttributeString("sGfeMaxLoanBalance", gfeArchive.sGfeMaxLoanBalance_rep);
            xmlWriter.WriteAttributeString("sGfeMaxPpmtPenaltyAmt", gfeArchive.sGfeMaxPpmtPenaltyAmt_rep);
            xmlWriter.WriteAttributeString("sGfeMaxProThisMPmtAndMIns", gfeArchive.sGfeMaxProThisMPmtAndMIns_rep);
            xmlWriter.WriteAttributeString("sGfeNotApplicableF", gfeArchive.sGfeNotApplicableF_rep);
            xmlWriter.WriteAttributeString("sGfeNoteIRAvailTillD_Date", gfeArchive.sGfeNoteIRAvailTillD_Date);
            xmlWriter.WriteAttributeString("sGfeNoteIRAvailTillD_Time", gfeArchive.sGfeNoteIRAvailTillD_Time);
            xmlWriter.WriteAttributeString("sGfeNoteIRAvailTillDLckd", GetBoolString(gfeArchive.sGfeNoteIRAvailTillDLckd));
            xmlWriter.WriteAttributeString("sGfeNoteIRAvailTillDTimeZoneT", GetEnumString(gfeArchive.sGfeNoteIRAvailTillDTimeZoneT));
            xmlWriter.WriteAttributeString("sGfeOriginationF", gfeArchive.sGfeOriginationF_rep);
            xmlWriter.WriteAttributeString("sGfeOriginatorCompF", gfeArchive.sGfeOriginatorCompF_rep);
            xmlWriter.WriteAttributeString("sGfeOriginatorCompFBaseT", GetEnumString(gfeArchive.sGfeOriginatorCompFBaseT));
            xmlWriter.WriteAttributeString("sGfeOriginatorCompFMb", gfeArchive.sGfeOriginatorCompFMb_rep);
            xmlWriter.WriteAttributeString("sGfeOriginatorCompFPc", gfeArchive.sGfeOriginatorCompFPc_rep);
            xmlWriter.WriteAttributeString("sGfeOriginatorCompFProps", gfeArchive.sGfeOriginatorCompFProps.ToString());
            xmlWriter.WriteAttributeString("sGfeOwnerTitleTotalFee", gfeArchive.sGfeOwnerTitleTotalFee_rep);
            xmlWriter.WriteAttributeString("sGfeProThisMPmtAndMIns", gfeArchive.sGfeProThisMPmtAndMIns_rep);
            xmlWriter.WriteAttributeString("sGfeRateLockPeriod", gfeArchive.sGfeRateLockPeriod_rep);
            xmlWriter.WriteAttributeString("sGfeRateLockPeriodLckd", GetBoolString(gfeArchive.sGfeRateLockPeriodLckd));
            xmlWriter.WriteAttributeString("sGfeRedisclosureD", gfeArchive.sGfeRedisclosureD_rep);
            xmlWriter.WriteAttributeString("sGfeRequiredServicesTotalFee", gfeArchive.sGfeRequiredServicesTotalFee_rep);
            xmlWriter.WriteAttributeString("sGfeRequirePaidToFromContacts", GetBoolString(gfeArchive.sGfeRequirePaidToFromContacts));
            xmlWriter.WriteAttributeString("sGfeServicesYouShopTotalFee", gfeArchive.sGfeServicesYouShopTotalFee_rep);
            xmlWriter.WriteAttributeString("sGfeShoppingCartLoan1CanLoanBalanceIncreaseTri", GetEnumString(gfeArchive.sGfeShoppingCartLoan1CanLoanBalanceIncreaseTri));
            xmlWriter.WriteAttributeString("sGfeShoppingCartLoan1CanMonthlyPmtIncreaseTri", GetEnumString(gfeArchive.sGfeShoppingCartLoan1CanMonthlyPmtIncreaseTri));
            xmlWriter.WriteAttributeString("sGfeShoppingCartLoan1CanRateIncreaseTri", GetEnumString(gfeArchive.sGfeShoppingCartLoan1CanRateIncreaseTri));
            xmlWriter.WriteAttributeString("sGfeShoppingCartLoan1HavePpmtPenaltyTri", GetEnumString(gfeArchive.sGfeShoppingCartLoan1HavePpmtPenaltyTri));
            xmlWriter.WriteAttributeString("sGfeShoppingCartLoan1InitialPmt", gfeArchive.sGfeShoppingCartLoan1InitialPmt_rep);
            xmlWriter.WriteAttributeString("sGfeShoppingCartLoan1IsBalloonTri", GetEnumString(gfeArchive.sGfeShoppingCartLoan1IsBalloonTri));
            xmlWriter.WriteAttributeString("sGfeShoppingCartLoan1LoanAmt", gfeArchive.sGfeShoppingCartLoan1LoanAmt_rep);
            xmlWriter.WriteAttributeString("sGfeShoppingCartLoan1LoanTerm", gfeArchive.sGfeShoppingCartLoan1LoanTerm_rep);
            xmlWriter.WriteAttributeString("sGfeShoppingCartLoan1NoteIR", gfeArchive.sGfeShoppingCartLoan1NoteIR_rep);
            xmlWriter.WriteAttributeString("sGfeShoppingCartLoan1OriginatorName", gfeArchive.sGfeShoppingCartLoan1OriginatorName);
            xmlWriter.WriteAttributeString("sGfeShoppingCartLoan1RateLockPeriod", gfeArchive.sGfeShoppingCartLoan1RateLockPeriod_rep);
            xmlWriter.WriteAttributeString("sGfeShoppingCartLoan1TotalClosingCost", gfeArchive.sGfeShoppingCartLoan1TotalClosingCost_rep);
            xmlWriter.WriteAttributeString("sGfeShoppingCartLoan2CanLoanBalanceIncreaseTri", GetEnumString(gfeArchive.sGfeShoppingCartLoan2CanLoanBalanceIncreaseTri));
            xmlWriter.WriteAttributeString("sGfeShoppingCartLoan2CanMonthlyPmtIncreaseTri", GetEnumString(gfeArchive.sGfeShoppingCartLoan2CanMonthlyPmtIncreaseTri));
            xmlWriter.WriteAttributeString("sGfeShoppingCartLoan2CanRateIncreaseTri", GetEnumString(gfeArchive.sGfeShoppingCartLoan2CanRateIncreaseTri));
            xmlWriter.WriteAttributeString("sGfeShoppingCartLoan2HavePpmtPenaltyTri", GetEnumString(gfeArchive.sGfeShoppingCartLoan2HavePpmtPenaltyTri));
            xmlWriter.WriteAttributeString("sGfeShoppingCartLoan2InitialPmt", gfeArchive.sGfeShoppingCartLoan2InitialPmt_rep);
            xmlWriter.WriteAttributeString("sGfeShoppingCartLoan2IsBalloonTri", GetEnumString(gfeArchive.sGfeShoppingCartLoan2IsBalloonTri));
            xmlWriter.WriteAttributeString("sGfeShoppingCartLoan2LoanAmt", gfeArchive.sGfeShoppingCartLoan2LoanAmt_rep);
            xmlWriter.WriteAttributeString("sGfeShoppingCartLoan2LoanTerm", gfeArchive.sGfeShoppingCartLoan2LoanTerm_rep);
            xmlWriter.WriteAttributeString("sGfeShoppingCartLoan2NoteIR", gfeArchive.sGfeShoppingCartLoan2NoteIR_rep);
            xmlWriter.WriteAttributeString("sGfeShoppingCartLoan2OriginatorName", gfeArchive.sGfeShoppingCartLoan2OriginatorName);
            xmlWriter.WriteAttributeString("sGfeShoppingCartLoan2RateLockPeriod", gfeArchive.sGfeShoppingCartLoan2RateLockPeriod_rep);
            xmlWriter.WriteAttributeString("sGfeShoppingCartLoan2TotalClosingCost", gfeArchive.sGfeShoppingCartLoan2TotalClosingCost_rep);
            xmlWriter.WriteAttributeString("sGfeShoppingCartLoan3CanLoanBalanceIncreaseTri", GetEnumString(gfeArchive.sGfeShoppingCartLoan3CanLoanBalanceIncreaseTri));
            xmlWriter.WriteAttributeString("sGfeShoppingCartLoan3CanMonthlyPmtIncreaseTri", GetEnumString(gfeArchive.sGfeShoppingCartLoan3CanMonthlyPmtIncreaseTri));
            xmlWriter.WriteAttributeString("sGfeShoppingCartLoan3CanRateIncreaseTri", GetEnumString(gfeArchive.sGfeShoppingCartLoan3CanRateIncreaseTri));
            xmlWriter.WriteAttributeString("sGfeShoppingCartLoan3HavePpmtPenaltyTri", GetEnumString(gfeArchive.sGfeShoppingCartLoan3HavePpmtPenaltyTri));
            xmlWriter.WriteAttributeString("sGfeShoppingCartLoan3InitialPmt", gfeArchive.sGfeShoppingCartLoan3InitialPmt_rep);
            xmlWriter.WriteAttributeString("sGfeShoppingCartLoan3IsBalloonTri", GetEnumString(gfeArchive.sGfeShoppingCartLoan3IsBalloonTri));
            xmlWriter.WriteAttributeString("sGfeShoppingCartLoan3LoanAmt", gfeArchive.sGfeShoppingCartLoan3LoanAmt_rep);
            xmlWriter.WriteAttributeString("sGfeShoppingCartLoan3LoanTerm", gfeArchive.sGfeShoppingCartLoan3LoanTerm_rep);
            xmlWriter.WriteAttributeString("sGfeShoppingCartLoan3NoteIR", gfeArchive.sGfeShoppingCartLoan3NoteIR_rep);
            xmlWriter.WriteAttributeString("sGfeShoppingCartLoan3OriginatorName", gfeArchive.sGfeShoppingCartLoan3OriginatorName);
            xmlWriter.WriteAttributeString("sGfeShoppingCartLoan3RateLockPeriod", gfeArchive.sGfeShoppingCartLoan3RateLockPeriod_rep);
            xmlWriter.WriteAttributeString("sGfeShoppingCartLoan3TotalClosingCost", gfeArchive.sGfeShoppingCartLoan3TotalClosingCost_rep);
            xmlWriter.WriteAttributeString("sGfeTotalEstimateSettlementCharge", gfeArchive.sGfeTotalEstimateSettlementCharge_rep);
            xmlWriter.WriteAttributeString("sGfeTotalOtherSettlementServiceFee", gfeArchive.sGfeTotalOtherSettlementServiceFee_rep);
            xmlWriter.WriteAttributeString("sGfeTradeOffLowerCCClosingCost", gfeArchive.sGfeTradeOffLowerCCClosingCost_rep);
            xmlWriter.WriteAttributeString("sGfeTradeOffLowerCCClosingCostDiff", gfeArchive.sGfeTradeOffLowerCCClosingCostDiff_rep);
            xmlWriter.WriteAttributeString("sGfeTradeOffLowerCCLoanAmt", gfeArchive.sGfeTradeOffLowerCCLoanAmt_rep);
            xmlWriter.WriteAttributeString("sGfeTradeOffLowerCCMPmtAndMIns", gfeArchive.sGfeTradeOffLowerCCMPmtAndMIns_rep);
            xmlWriter.WriteAttributeString("sGfeTradeOffLowerCCMPmtAndMInsDiff", gfeArchive.sGfeTradeOffLowerCCMPmtAndMInsDiff_rep);
            xmlWriter.WriteAttributeString("sGfeTradeOffLowerCCNoteIR", gfeArchive.sGfeTradeOffLowerCCNoteIR_rep);
            xmlWriter.WriteAttributeString("sGfeTradeOffLowerRateClosingCost", gfeArchive.sGfeTradeOffLowerRateClosingCost_rep);
            xmlWriter.WriteAttributeString("sGfeTradeOffLowerRateClosingCostDiff", gfeArchive.sGfeTradeOffLowerRateClosingCostDiff_rep);
            xmlWriter.WriteAttributeString("sGfeTradeOffLowerRateLoanAmt", gfeArchive.sGfeTradeOffLowerRateLoanAmt_rep);
            xmlWriter.WriteAttributeString("sGfeTradeOffLowerRateMPmtAndMIns", gfeArchive.sGfeTradeOffLowerRateMPmtAndMIns_rep);
            xmlWriter.WriteAttributeString("sGfeTradeOffLowerRateMPmtAndMInsDiff", gfeArchive.sGfeTradeOffLowerRateMPmtAndMInsDiff_rep);
            xmlWriter.WriteAttributeString("sGfeTradeOffLowerRateNoteIR", gfeArchive.sGfeTradeOffLowerRateNoteIR_rep);
            xmlWriter.WriteAttributeString("sGfeTransferTaxTotalFee", gfeArchive.sGfeTransferTaxTotalFee_rep);
            xmlWriter.WriteAttributeString("sGfeUsePaidToFromOfficialContact", GetBoolString(gfeArchive.sGfeUsePaidToFromOfficialContact));
            xmlWriter.WriteAttributeString("sHazInsPia", gfeArchive.sHazInsPia_rep);
            xmlWriter.WriteAttributeString("sHazInsPiaMon", gfeArchive.sHazInsPiaMon_rep);
            xmlWriter.WriteAttributeString("sHazInsPiaPaidTo", gfeArchive.sHazInsPiaPaidTo);
            xmlWriter.WriteAttributeString("sHazInsPiaProps", gfeArchive.sHazInsPiaProps.ToString());
            xmlWriter.WriteAttributeString("sHazInsRsrv", gfeArchive.sHazInsRsrv_rep);
            xmlWriter.WriteAttributeString("sHazInsRsrvMon", gfeArchive.sHazInsRsrvMon_rep);
            xmlWriter.WriteAttributeString("sHazInsRsrvMonLckd", GetBoolString(gfeArchive.sHazInsRsrvMonLckd));
            xmlWriter.WriteAttributeString("sHazInsRsrvProps", gfeArchive.sHazInsRsrvProps.ToString());
            xmlWriter.WriteAttributeString("sInspectF", gfeArchive.sInspectF_rep);
            xmlWriter.WriteAttributeString("sInspectFPaidTo", gfeArchive.sInspectFPaidTo);
            xmlWriter.WriteAttributeString("sInspectFProps", gfeArchive.sInspectFProps.ToString());
            xmlWriter.WriteAttributeString("sIOnlyMon", gfeArchive.sIOnlyMon_rep);
            xmlWriter.WriteAttributeString("sIPerDay", gfeArchive.sIPerDay_rep);
            xmlWriter.WriteAttributeString("sIPerDayLckd", GetBoolString(gfeArchive.sIPerDayLckd));
            xmlWriter.WriteAttributeString("sIPia", gfeArchive.sIPia_rep);
            xmlWriter.WriteAttributeString("sIPiaDy", gfeArchive.sIPiaDy_rep);
            xmlWriter.WriteAttributeString("sIPiaDyLckd", GetBoolString(gfeArchive.sIPiaDyLckd));
            xmlWriter.WriteAttributeString("sIPiaProps", gfeArchive.sIPiaProps.ToString());
            xmlWriter.WriteAttributeString("sIsItemizeBrokerCommissionOnIFW", GetBoolString(gfeArchive.sIsItemizeBrokerCommissionOnIFW));
            xmlWriter.WriteAttributeString("sIsOriginationCompensationSourceRequired", GetBoolString(gfeArchive.sIsOriginationCompensationSourceRequired));
            xmlWriter.WriteAttributeString("sIsPrintTimeForGfeNoteIRAvailTillD", GetBoolString(gfeArchive.sIsPrintTimeForGfeNoteIRAvailTillD));
            xmlWriter.WriteAttributeString("sIsPrintTimeForsGfeEstScAvailTillD", GetBoolString(gfeArchive.sIsPrintTimeForsGfeEstScAvailTillD));
            xmlWriter.WriteAttributeString("sIsRateLocked", GetBoolString(gfeArchive.sIsRateLocked));
            xmlWriter.WriteAttributeString("sIsRequireFeesFromDropDown", GetBoolString(gfeArchive.sIsRequireFeesFromDropDown));
            xmlWriter.WriteAttributeString("sLDiscnt", gfeArchive.sLDiscnt_rep);
            xmlWriter.WriteAttributeString("sLDiscntBaseT", GetEnumString(gfeArchive.sLDiscntBaseT));
            xmlWriter.WriteAttributeString("sLDiscntFMb", gfeArchive.sLDiscntFMb_rep);
            xmlWriter.WriteAttributeString("sLDiscntPc", gfeArchive.sLDiscntPc_rep);
            xmlWriter.WriteAttributeString("sLOrigF", gfeArchive.sLOrigF_rep);
            xmlWriter.WriteAttributeString("sLOrigFMb", gfeArchive.sLOrigFMb_rep);
            xmlWriter.WriteAttributeString("sLOrigFPc", gfeArchive.sLOrigFPc_rep);
            xmlWriter.WriteAttributeString("sLOrigFProps", gfeArchive.sLOrigFProps.ToString());
            xmlWriter.WriteAttributeString("sLpTemplateNm", gfeArchive.sLpTemplateNm);
            xmlWriter.WriteAttributeString("sLPurposeT", GetEnumString(gfeArchive.sLPurposeT));
            xmlWriter.WriteAttributeString("sLT", GetEnumString(gfeArchive.sLT));
            xmlWriter.WriteAttributeString("sMBrokF", gfeArchive.sMBrokF_rep);
            xmlWriter.WriteAttributeString("sMBrokFBaseT", GetEnumString(gfeArchive.sMBrokFBaseT));
            xmlWriter.WriteAttributeString("sMBrokFMb", gfeArchive.sMBrokFMb_rep);
            xmlWriter.WriteAttributeString("sMBrokFPc", gfeArchive.sMBrokFPc_rep);
            xmlWriter.WriteAttributeString("sMBrokFProps", gfeArchive.sMBrokFProps.ToString());
            xmlWriter.WriteAttributeString("sMInsRsrv", gfeArchive.sMInsRsrv_rep);
            xmlWriter.WriteAttributeString("sMInsRsrvMon", gfeArchive.sMInsRsrvMon_rep);
            xmlWriter.WriteAttributeString("sMInsRsrvMonLckd", GetBoolString(gfeArchive.sMInsRsrvMonLckd));
            xmlWriter.WriteAttributeString("sMInsRsrvProps", gfeArchive.sMInsRsrvProps.ToString());
            xmlWriter.WriteAttributeString("sMipPia", gfeArchive.sMipPia_rep);
            xmlWriter.WriteAttributeString("sMipPiaPaidTo", gfeArchive.sMipPiaPaidTo);
            xmlWriter.WriteAttributeString("sMipPiaProps", gfeArchive.sMipPiaProps.ToString());
            xmlWriter.WriteAttributeString("sMldsHasImpound", GetBoolString(gfeArchive.sMldsHasImpound));
            xmlWriter.WriteAttributeString("sNotaryF", gfeArchive.sNotaryF_rep);
            xmlWriter.WriteAttributeString("sNotaryFGfeSection", GetEnumString(gfeArchive.sNotaryFGfeSection));
            xmlWriter.WriteAttributeString("sNotaryFPaidTo", gfeArchive.sNotaryFPaidTo);
            xmlWriter.WriteAttributeString("sNotaryFProps", gfeArchive.sNotaryFProps.ToString());
            xmlWriter.WriteAttributeString("sNoteIR", gfeArchive.sNoteIR_rep);
            xmlWriter.WriteAttributeString("sOpenedD", gfeArchive.sOpenedD_rep);
            xmlWriter.WriteAttributeString("sOriginatorCompensationPaymentSourceT", GetEnumString(gfeArchive.sOriginatorCompensationPaymentSourceT));
            xmlWriter.WriteAttributeString("sOwnerTitleInsF", gfeArchive.sOwnerTitleInsF_rep);
            xmlWriter.WriteAttributeString("sOwnerTitleInsPaidTo", gfeArchive.sOwnerTitleInsPaidTo);
            xmlWriter.WriteAttributeString("sOwnerTitleInsProps", gfeArchive.sOwnerTitleInsProps.ToString());
            xmlWriter.WriteAttributeString("sPestInspectF", gfeArchive.sPestInspectF_rep);
            xmlWriter.WriteAttributeString("sPestInspectFProps", gfeArchive.sPestInspectFProps.ToString());
            xmlWriter.WriteAttributeString("sPestInspectPaidTo", gfeArchive.sPestInspectPaidTo);
            xmlWriter.WriteAttributeString("sPmtAdjCapMon", gfeArchive.sPmtAdjCapMon_rep);
            xmlWriter.WriteAttributeString("sPmtAdjCapR", gfeArchive.sPmtAdjCapR_rep);
            xmlWriter.WriteAttributeString("sPmtAdjMaxBalPc", gfeArchive.sPmtAdjMaxBalPc_rep);
            xmlWriter.WriteAttributeString("sPmtAdjRecastPeriodMon", gfeArchive.sPmtAdjRecastPeriodMon_rep);
            xmlWriter.WriteAttributeString("sPmtAdjRecastStop", gfeArchive.sPmtAdjRecastStop_rep);
            xmlWriter.WriteAttributeString("sProcF", gfeArchive.sProcF_rep);
            xmlWriter.WriteAttributeString("sProcFPaid", GetBoolString(gfeArchive.sProcFPaid));
            xmlWriter.WriteAttributeString("sProcFPaidTo", gfeArchive.sProcFPaidTo);
            xmlWriter.WriteAttributeString("sProcFProps", gfeArchive.sProcFProps.ToString());
            xmlWriter.WriteAttributeString("sProFloodIns", gfeArchive.sProFloodIns_rep);
            xmlWriter.WriteAttributeString("sProHazIns", gfeArchive.sProHazIns_rep);
            xmlWriter.WriteAttributeString("sProHazInsMb", gfeArchive.sProHazInsMb_rep);
            xmlWriter.WriteAttributeString("sProHazInsR", gfeArchive.sProHazInsR_rep);
            xmlWriter.WriteAttributeString("sProHazInsT", GetEnumString(gfeArchive.sProHazInsT));
            xmlWriter.WriteAttributeString("sProMIns", gfeArchive.sProMIns_rep);
            xmlWriter.WriteAttributeString("sProRealETxMb", gfeArchive.sProRealETxMb_rep);
            xmlWriter.WriteAttributeString("sProRealETxR", gfeArchive.sProRealETxR_rep);
            xmlWriter.WriteAttributeString("sProRealETxT", GetEnumString(gfeArchive.sProRealETxT));
            xmlWriter.WriteAttributeString("sProSchoolTx", gfeArchive.sProSchoolTx_rep);
            xmlWriter.WriteAttributeString("sProU3Rsrv", gfeArchive.sProU3Rsrv_rep);
            xmlWriter.WriteAttributeString("sProU4Rsrv", gfeArchive.sProU4Rsrv_rep);
            xmlWriter.WriteAttributeString("sRAdj1stCapMon", gfeArchive.sRAdj1stCapMon_rep);
            xmlWriter.WriteAttributeString("sRAdj1stCapR", gfeArchive.sRAdj1stCapR_rep);
            xmlWriter.WriteAttributeString("sRAdjCapMon", gfeArchive.sRAdjCapMon_rep);
            xmlWriter.WriteAttributeString("sRAdjCapR", gfeArchive.sRAdjCapR_rep);
            xmlWriter.WriteAttributeString("sRAdjLifeCapR", gfeArchive.sRAdjLifeCapR_rep);
            xmlWriter.WriteAttributeString("sRealETxRsrv", gfeArchive.sRealETxRsrv_rep);
            xmlWriter.WriteAttributeString("sRealETxRsrvMon", gfeArchive.sRealETxRsrvMon_rep);
            xmlWriter.WriteAttributeString("sRealETxRsrvMonLckd", GetBoolString(gfeArchive.sRealETxRsrvMonLckd));
            xmlWriter.WriteAttributeString("sRealETxRsrvProps", gfeArchive.sRealETxRsrvProps.ToString());
            xmlWriter.WriteAttributeString("sRecBaseT", GetEnumString(gfeArchive.sRecBaseT));
            xmlWriter.WriteAttributeString("sRecDeed", gfeArchive.sRecDeed_rep);
            xmlWriter.WriteAttributeString("sRecF", gfeArchive.sRecF_rep);
            xmlWriter.WriteAttributeString("sRecFDesc", gfeArchive.sRecFDesc);
            xmlWriter.WriteAttributeString("sRecFLckd", GetBoolString(gfeArchive.sRecFLckd));
            xmlWriter.WriteAttributeString("sRecFMb", gfeArchive.sRecFMb_rep);
            xmlWriter.WriteAttributeString("sRecFPc", gfeArchive.sRecFPc_rep);
            xmlWriter.WriteAttributeString("sRecFProps", gfeArchive.sRecFProps.ToString());
            xmlWriter.WriteAttributeString("sRecMortgage", gfeArchive.sRecMortgage_rep);
            xmlWriter.WriteAttributeString("sRecRelease", gfeArchive.sRecRelease_rep);
            xmlWriter.WriteAttributeString("sRLifeCapR", gfeArchive.sRLifeCapR_rep);
            xmlWriter.WriteAttributeString("sSchedDueD1", gfeArchive.sSchedDueD1_rep);
            xmlWriter.WriteAttributeString("sSchedDueD1Lckd", GetBoolString(gfeArchive.sSchedDueD1Lckd));
            xmlWriter.WriteAttributeString("sSchoolTxRsrv", gfeArchive.sSchoolTxRsrv_rep);
            xmlWriter.WriteAttributeString("sSchoolTxRsrvMon", gfeArchive.sSchoolTxRsrvMon_rep);
            xmlWriter.WriteAttributeString("sSchoolTxRsrvMonLckd", GetBoolString(gfeArchive.sSchoolTxRsrvMonLckd));
            xmlWriter.WriteAttributeString("sSchoolTxRsrvProps", gfeArchive.sSchoolTxRsrvProps.ToString());
            xmlWriter.WriteAttributeString("sStateRtc", gfeArchive.sStateRtc_rep);
            xmlWriter.WriteAttributeString("sStateRtcBaseT", GetEnumString(gfeArchive.sStateRtcBaseT));
            xmlWriter.WriteAttributeString("sStateRtcDesc", gfeArchive.sStateRtcDesc);
            xmlWriter.WriteAttributeString("sStateRtcMb", gfeArchive.sStateRtcMb_rep);
            xmlWriter.WriteAttributeString("sStateRtcPc", gfeArchive.sStateRtcPc_rep);
            xmlWriter.WriteAttributeString("sStateRtcProps", gfeArchive.sStateRtcProps.ToString());
            xmlWriter.WriteAttributeString("sTerm", gfeArchive.sTerm_rep);
            xmlWriter.WriteAttributeString("sTitleInsF", gfeArchive.sTitleInsF_rep);
            xmlWriter.WriteAttributeString("sTitleInsFProps", gfeArchive.sTitleInsFProps.ToString());
            xmlWriter.WriteAttributeString("sTitleInsFTable", gfeArchive.sTitleInsFTable);
            xmlWriter.WriteAttributeString("sTxServF", gfeArchive.sTxServF_rep);
            xmlWriter.WriteAttributeString("sTxServFPaidTo", gfeArchive.sTxServFPaidTo);
            xmlWriter.WriteAttributeString("sTxServFProps", gfeArchive.sTxServFProps.ToString());
            xmlWriter.WriteAttributeString("sU1GovRtc", gfeArchive.sU1GovRtc_rep);
            xmlWriter.WriteAttributeString("sU1GovRtcBaseT", GetEnumString(gfeArchive.sU1GovRtcBaseT));
            xmlWriter.WriteAttributeString("sU1GovRtcDesc", gfeArchive.sU1GovRtcDesc);
            xmlWriter.WriteAttributeString("sU1GovRtcGfeSection", GetEnumString(gfeArchive.sU1GovRtcGfeSection));
            xmlWriter.WriteAttributeString("sU1GovRtcMb", gfeArchive.sU1GovRtcMb_rep);
            xmlWriter.WriteAttributeString("sU1GovRtcPaidTo", gfeArchive.sU1GovRtcPaidTo);
            xmlWriter.WriteAttributeString("sU1GovRtcPc", gfeArchive.sU1GovRtcPc_rep);
            xmlWriter.WriteAttributeString("sU1GovRtcProps", gfeArchive.sU1GovRtcProps.ToString());
            xmlWriter.WriteAttributeString("sU1Sc", gfeArchive.sU1Sc_rep);
            xmlWriter.WriteAttributeString("sU1ScDesc", gfeArchive.sU1ScDesc);
            xmlWriter.WriteAttributeString("sU1ScGfeSection", GetEnumString(gfeArchive.sU1ScGfeSection));
            xmlWriter.WriteAttributeString("sU1ScPaidTo", gfeArchive.sU1ScPaidTo);
            xmlWriter.WriteAttributeString("sU1ScProps", gfeArchive.sU1ScProps.ToString());
            xmlWriter.WriteAttributeString("sU1Tc", gfeArchive.sU1Tc_rep);
            xmlWriter.WriteAttributeString("sU1TcDesc", gfeArchive.sU1TcDesc);
            xmlWriter.WriteAttributeString("sU1TcGfeSection", GetEnumString(gfeArchive.sU1TcGfeSection));
            xmlWriter.WriteAttributeString("sU1TcPaidTo", gfeArchive.sU1TcPaidTo);
            xmlWriter.WriteAttributeString("sU1TcProps", gfeArchive.sU1TcProps.ToString());
            xmlWriter.WriteAttributeString("sU2GovRtc", gfeArchive.sU2GovRtc_rep);
            xmlWriter.WriteAttributeString("sU2GovRtcBaseT", GetEnumString(gfeArchive.sU2GovRtcBaseT));
            xmlWriter.WriteAttributeString("sU2GovRtcDesc", gfeArchive.sU2GovRtcDesc);
            xmlWriter.WriteAttributeString("sU2GovRtcGfeSection", GetEnumString(gfeArchive.sU2GovRtcGfeSection));
            xmlWriter.WriteAttributeString("sU2GovRtcMb", gfeArchive.sU2GovRtcMb_rep);
            xmlWriter.WriteAttributeString("sU2GovRtcPaidTo", gfeArchive.sU2GovRtcPaidTo);
            xmlWriter.WriteAttributeString("sU2GovRtcPc", gfeArchive.sU2GovRtcPc_rep);
            xmlWriter.WriteAttributeString("sU2GovRtcProps", gfeArchive.sU2GovRtcProps.ToString());
            xmlWriter.WriteAttributeString("sU2Sc", gfeArchive.sU2Sc_rep);
            xmlWriter.WriteAttributeString("sU2ScDesc", gfeArchive.sU2ScDesc);
            xmlWriter.WriteAttributeString("sU2ScGfeSection", GetEnumString(gfeArchive.sU2ScGfeSection));
            xmlWriter.WriteAttributeString("sU2ScPaidTo", gfeArchive.sU2ScPaidTo);
            xmlWriter.WriteAttributeString("sU2ScProps", gfeArchive.sU2ScProps.ToString());
            xmlWriter.WriteAttributeString("sU2Tc", gfeArchive.sU2Tc_rep);
            xmlWriter.WriteAttributeString("sU2TcDesc", gfeArchive.sU2TcDesc);
            xmlWriter.WriteAttributeString("sU2TcGfeSection", GetEnumString(gfeArchive.sU2TcGfeSection));
            xmlWriter.WriteAttributeString("sU2TcPaidTo", gfeArchive.sU2TcPaidTo);
            xmlWriter.WriteAttributeString("sU2TcProps", gfeArchive.sU2TcProps.ToString());
            xmlWriter.WriteAttributeString("sU3GovRtc", gfeArchive.sU3GovRtc_rep);
            xmlWriter.WriteAttributeString("sU3GovRtcBaseT", GetEnumString(gfeArchive.sU3GovRtcBaseT));
            xmlWriter.WriteAttributeString("sU3GovRtcDesc", gfeArchive.sU3GovRtcDesc);
            xmlWriter.WriteAttributeString("sU3GovRtcGfeSection", GetEnumString(gfeArchive.sU3GovRtcGfeSection));
            xmlWriter.WriteAttributeString("sU3GovRtcMb", gfeArchive.sU3GovRtcMb_rep);
            xmlWriter.WriteAttributeString("sU3GovRtcPaidTo", gfeArchive.sU3GovRtcPaidTo);
            xmlWriter.WriteAttributeString("sU3GovRtcPc", gfeArchive.sU3GovRtcPc_rep);
            xmlWriter.WriteAttributeString("sU3GovRtcProps", gfeArchive.sU3GovRtcProps.ToString());
            xmlWriter.WriteAttributeString("sU3Rsrv", gfeArchive.sU3Rsrv_rep);
            xmlWriter.WriteAttributeString("sU3RsrvDesc", gfeArchive.sU3RsrvDesc);
            xmlWriter.WriteAttributeString("sU3RsrvMon", gfeArchive.sU3RsrvMon_rep);
            xmlWriter.WriteAttributeString("sU3RsrvMonLckd", GetBoolString(gfeArchive.sU3RsrvMonLckd));
            xmlWriter.WriteAttributeString("sU3RsrvProps", gfeArchive.sU3RsrvProps.ToString());
            xmlWriter.WriteAttributeString("sU3Sc", gfeArchive.sU3Sc_rep);
            xmlWriter.WriteAttributeString("sU3ScDesc", gfeArchive.sU3ScDesc);
            xmlWriter.WriteAttributeString("sU3ScGfeSection", GetEnumString(gfeArchive.sU3ScGfeSection));
            xmlWriter.WriteAttributeString("sU3ScPaidTo", gfeArchive.sU3ScPaidTo);
            xmlWriter.WriteAttributeString("sU3ScProps", gfeArchive.sU3ScProps.ToString());
            xmlWriter.WriteAttributeString("sU3Tc", gfeArchive.sU3Tc_rep);
            xmlWriter.WriteAttributeString("sU3TcDesc", gfeArchive.sU3TcDesc);
            xmlWriter.WriteAttributeString("sU3TcGfeSection", GetEnumString(gfeArchive.sU3TcGfeSection));
            xmlWriter.WriteAttributeString("sU3TcPaidTo", gfeArchive.sU3TcPaidTo);
            xmlWriter.WriteAttributeString("sU3TcProps", gfeArchive.sU3TcProps.ToString());
            xmlWriter.WriteAttributeString("sU4Rsrv", gfeArchive.sU4Rsrv_rep);
            xmlWriter.WriteAttributeString("sU4RsrvDesc", gfeArchive.sU4RsrvDesc);
            xmlWriter.WriteAttributeString("sU4RsrvMon", gfeArchive.sU4RsrvMon_rep);
            xmlWriter.WriteAttributeString("sU4RsrvMonLckd", GetBoolString(gfeArchive.sU4RsrvMonLckd));
            xmlWriter.WriteAttributeString("sU4RsrvProps", gfeArchive.sU4RsrvProps.ToString());
            xmlWriter.WriteAttributeString("sU4Sc", gfeArchive.sU4Sc_rep);
            xmlWriter.WriteAttributeString("sU4ScDesc", gfeArchive.sU4ScDesc);
            xmlWriter.WriteAttributeString("sU4ScGfeSection", GetEnumString(gfeArchive.sU4ScGfeSection));
            xmlWriter.WriteAttributeString("sU4ScPaidTo", gfeArchive.sU4ScPaidTo);
            xmlWriter.WriteAttributeString("sU4ScProps", gfeArchive.sU4ScProps.ToString());
            xmlWriter.WriteAttributeString("sU4Tc", gfeArchive.sU4Tc_rep);
            xmlWriter.WriteAttributeString("sU4TcDesc", gfeArchive.sU4TcDesc);
            xmlWriter.WriteAttributeString("sU4TcGfeSection", GetEnumString(gfeArchive.sU4TcGfeSection));
            xmlWriter.WriteAttributeString("sU4TcPaidTo", gfeArchive.sU4TcPaidTo);
            xmlWriter.WriteAttributeString("sU4TcProps", gfeArchive.sU4TcProps.ToString());
            xmlWriter.WriteAttributeString("sU5Sc", gfeArchive.sU5Sc_rep);
            xmlWriter.WriteAttributeString("sU5ScDesc", gfeArchive.sU5ScDesc);
            xmlWriter.WriteAttributeString("sU5ScGfeSection", GetEnumString(gfeArchive.sU5ScGfeSection));
            xmlWriter.WriteAttributeString("sU5ScPaidTo", gfeArchive.sU5ScPaidTo);
            xmlWriter.WriteAttributeString("sU5ScProps", gfeArchive.sU5ScProps.ToString());
            xmlWriter.WriteAttributeString("sUwF", gfeArchive.sUwF_rep);
            xmlWriter.WriteAttributeString("sUwFPaidTo", gfeArchive.sUwFPaidTo);
            xmlWriter.WriteAttributeString("sUwFProps", gfeArchive.sUwFProps.ToString());
            xmlWriter.WriteAttributeString("sVaFf", gfeArchive.sVaFf_rep);
            xmlWriter.WriteAttributeString("sVaFfPaidTo", gfeArchive.sVaFfPaidTo);
            xmlWriter.WriteAttributeString("sVaFfProps", gfeArchive.sVaFfProps.ToString());
            xmlWriter.WriteAttributeString("sWireF", gfeArchive.sWireF_rep);
            xmlWriter.WriteAttributeString("sWireFPaidTo", gfeArchive.sWireFPaidTo);
            xmlWriter.WriteAttributeString("sWireFProps", gfeArchive.sWireFProps.ToString());
            xmlWriter.WriteEndElement(); // </elementName>
        }

        private void WriteAggregateEscrowAccount(SanitizedXmlWriterWrapper xmlWriter, AggregateEscrowAccount aggregateEscrowAccount)
        {
            xmlWriter.WriteStartElement("sAggregateEscrowAccount");
            xmlWriter.WriteAttributeString("AggregateEscrowAdjustment", aggregateEscrowAccount.AggregateEscrowAdjustment_rep);
            xmlWriter.WriteAttributeString("InitialDeposit", aggregateEscrowAccount.InitialDeposit_rep);
            foreach (var item in aggregateEscrowAccount.AggregateItems)
            {
                xmlWriter.WriteStartElement("AggregateEscrowItem");
                xmlWriter.WriteAttributeString("Bal", item.Bal_rep);
                xmlWriter.WriteAttributeString("Desc", item.Desc);
                xmlWriter.WriteAttributeString("Mon", item.Mon);
                xmlWriter.WriteAttributeString("MonInt", item.MonInt.ToString());
                xmlWriter.WriteAttributeString("PmtFromEscrow", item.PmtFromEscrow_rep);
                xmlWriter.WriteEndElement(); // </AggregateEscrowItem>
            }
            xmlWriter.WriteEndElement(); // </sAggregateEscrowAccount>
        }

        private void WriteLiaCollection(SanitizedXmlWriterWrapper xmlWriter, ILiaCollection cLiaCollection)
        {
            var collection = cLiaCollection.GetSubcollection(true, E_DebtGroupT.ALL);

            foreach (var item in collection)
            {
                var lia = (ILiability)item;
                ILiabilityRegular regularLiability = lia as ILiabilityRegular;

                xmlWriter.WriteStartElement("aLiaCollection");

                if (regularLiability != null)
                {
                    xmlWriter.WriteAttributeString("AccNm", regularLiability.AccNm);
                    xmlWriter.WriteAttributeString("AccNum", regularLiability.AccNum.Value);
                    xmlWriter.WriteAttributeString("Attention", regularLiability.Attention);
                    xmlWriter.WriteAttributeString("AutoYearMake", regularLiability.AutoYearMake);
                    xmlWriter.WriteAttributeString("Bal", regularLiability.Bal_rep);
                    xmlWriter.WriteAttributeString("ComAddr", regularLiability.ComAddr);
                    xmlWriter.WriteAttributeString("ComCity", regularLiability.ComCity);
                    xmlWriter.WriteAttributeString("ComFax", regularLiability.ComFax);
                    xmlWriter.WriteAttributeString("ComNm", regularLiability.ComNm);
                    xmlWriter.WriteAttributeString("ComPhone", regularLiability.ComPhone);
                    xmlWriter.WriteAttributeString("ComState", regularLiability.ComState);
                    xmlWriter.WriteAttributeString("ComZip", regularLiability.ComZip);
                    xmlWriter.WriteAttributeString("DebtT", regularLiability.DebtT.ToString("D"));
                    xmlWriter.WriteAttributeString("Desc", regularLiability.Desc);
                    xmlWriter.WriteAttributeString("Due", regularLiability.Due_rep);
                    xmlWriter.WriteAttributeString("ExcFromUnderwriting", GetBoolString(regularLiability.ExcFromUnderwriting));//
                    xmlWriter.WriteAttributeString("IncInBankruptcy", GetBoolString(regularLiability.IncInBankruptcy));
                    xmlWriter.WriteAttributeString("IncInForeclosure", GetBoolString(regularLiability.IncInForeclosure));
                    xmlWriter.WriteAttributeString("IncInReposession", GetBoolString(regularLiability.IncInReposession));
                    xmlWriter.WriteAttributeString("IsForAuto", GetBoolString(regularLiability.IsForAuto));
                    xmlWriter.WriteAttributeString("IsMortFHAInsured", GetBoolString(regularLiability.IsMortFHAInsured));
                    xmlWriter.WriteAttributeString("IsPiggyBack", GetBoolString(regularLiability.IsPiggyBack));
                    xmlWriter.WriteAttributeString("IsSeeAttachment", GetBoolString(regularLiability.IsSeeAttachment));
                    xmlWriter.WriteAttributeString("IsSubjectProperty1stMortgage", GetBoolString(regularLiability.IsSubjectProperty1stMortgage));//
                    xmlWriter.WriteAttributeString("IsSubjectPropertyMortgage", GetBoolString(regularLiability.IsSubjectPropertyMortgage));
                    xmlWriter.WriteAttributeString("Late30_rep", regularLiability.Late30_rep);
                    xmlWriter.WriteAttributeString("Late60_rep", regularLiability.Late60_rep);
                    xmlWriter.WriteAttributeString("Late90Plus_rep", regularLiability.Late90Plus_rep);
                    xmlWriter.WriteAttributeString("NotUsedInRatio", GetBoolString(regularLiability.NotUsedInRatio));
                    xmlWriter.WriteAttributeString("OrigDebtAmt_rep", regularLiability.OrigDebtAmt_rep);
                    xmlWriter.WriteAttributeString("OwnerT", regularLiability.OwnerT.ToString("D"));
                    xmlWriter.WriteAttributeString("Pmt_rep", regularLiability.Pmt_rep);
                    xmlWriter.WriteAttributeString("PmtRemainMons_rep1003", regularLiability.PmtRemainMons_rep1003);
                    xmlWriter.WriteAttributeString("R_rep", regularLiability.R_rep);
                    xmlWriter.WriteAttributeString("ReconcileStatusT", regularLiability.ReconcileStatusT.ToString("D"));
                    xmlWriter.WriteAttributeString("RemainMons_rep", regularLiability.RemainMons_rep);
                    xmlWriter.WriteAttributeString("VerifExpD_rep", regularLiability.VerifExpD_rep);
                    xmlWriter.WriteAttributeString("VerifRecvD_rep", regularLiability.VerifRecvD_rep);
                    xmlWriter.WriteAttributeString("VerifReorderedD_rep", regularLiability.VerifReorderedD_rep);
                    xmlWriter.WriteAttributeString("VerifSentD_rep", regularLiability.VerifSentD_rep);
                    xmlWriter.WriteAttributeString("WillBePdOff", GetBoolString(regularLiability.WillBePdOff));

                }
                else
                {
                    var liaSpecial = lia as ILiabilitySpecial;
                    if (liaSpecial != null)
                    {
                        xmlWriter.WriteAttributeString("DebtT", liaSpecial.DebtT.ToString("D"));
                        xmlWriter.WriteAttributeString("Pmt", liaSpecial.Pmt_rep);
                        xmlWriter.WriteAttributeString("RemainMons", liaSpecial.RemainMons_rep);
                        xmlWriter.WriteAttributeString("NotUsedInRatio", GetBoolString(liaSpecial.NotUsedInRatio));

                        switch (liaSpecial.DebtT)
                        {
                            case E_DebtSpecialT.Alimony:
                                xmlWriter.WriteAttributeString("OwedTo", ((ILiabilityAlimony)liaSpecial).OwedTo);
                                break;
                            case E_DebtSpecialT.ChildSupport:
                                xmlWriter.WriteAttributeString("OwedTo", ((ILiabilityChildSupport) liaSpecial).OwedTo);
                                break;
                            case E_DebtSpecialT.JobRelatedExpense:
                                xmlWriter.WriteAttributeString("ExpenseDesc", ((ILiabilityJobExpense)liaSpecial).ExpenseDesc);
                                break;
                            default:
                                throw new UnhandledEnumException(liaSpecial.DebtT);
                        }

                    }
                }
                xmlWriter.WriteEndElement();
            }

        }

        private void WriteAssetCollection(SanitizedXmlWriterWrapper xmlWriter, IAssetCollection cAssetCollection)
        {
            var collection = cAssetCollection.GetSubcollection(true, E_AssetGroupT.All);

            foreach (var item in collection) 
            {
                var asset = (IAsset)item;
                var regularAsset = asset as IAssetRegular;

                xmlWriter.WriteStartElement("aAssetCollection");
                if (regularAsset != null) 
                {
                    xmlWriter.WriteAttributeString("Val", regularAsset.Val_rep);
                    xmlWriter.WriteAttributeString("Desc", regularAsset.Desc);
                    xmlWriter.WriteAttributeString("OwnerT", regularAsset.OwnerT.ToString("D"));
                    xmlWriter.WriteAttributeString("AssetT", regularAsset.AssetT.ToString("D"));
                    xmlWriter.WriteAttributeString("Attention", regularAsset.Attention);
                    xmlWriter.WriteAttributeString("GiftSource", regularAsset.GiftSource.ToString("D"));
                    xmlWriter.WriteAttributeString("VerifExpD", regularAsset.VerifExpD_rep);
                    xmlWriter.WriteAttributeString("VerifRecvD", regularAsset.VerifRecvD_rep);
                    xmlWriter.WriteAttributeString("VerifSentD", regularAsset.VerifSentD_rep);
                    xmlWriter.WriteAttributeString("VerifReorderedD", regularAsset.VerifReorderedD_rep);
                    xmlWriter.WriteAttributeString("StAddr", regularAsset.StAddr);
                    xmlWriter.WriteAttributeString("City", regularAsset.City);
                    xmlWriter.WriteAttributeString("State", regularAsset.State);
                    xmlWriter.WriteAttributeString("Zip", regularAsset.Zip);
                    xmlWriter.WriteAttributeString("AccNum", regularAsset.AccNum.Value);
                    xmlWriter.WriteAttributeString("AccNm", regularAsset.AccNm);
                    xmlWriter.WriteAttributeString("DepartmentName", regularAsset.DepartmentName);
                    xmlWriter.WriteAttributeString("ComNm", regularAsset.ComNm);
                    xmlWriter.WriteAttributeString("OtherTypeDesc", regularAsset.OtherTypeDesc);
                } 
                else 
                {
                    xmlWriter.WriteAttributeString("Val", asset.Val_rep);
                    xmlWriter.WriteAttributeString("Desc", asset.Desc);
                    xmlWriter.WriteAttributeString("OwnerT", asset.OwnerT.ToString("D"));
                    xmlWriter.WriteAttributeString("AssetT", asset.AssetT.ToString("D"));

                }
                xmlWriter.WriteEndElement();
            }

        }

        private void WriteList(SanitizedXmlWriterWrapper xmlWriter, IEnumerable<CPurchaseAdviceFeesFields> list)
        {
            if (list == null)
            {
                return;
            }
            foreach (var o in list)
            {
                if (o == null)
                {
                    continue;
                }
                xmlWriter.WriteStartElement("sPurchaseAdviceFees");
                xmlWriter.WriteAttributeString("FeeDesc", o.FeeDesc);
                xmlWriter.WriteAttributeString("FeeAmt", o.FeeAmt_rep);
                xmlWriter.WriteEndElement();
            }
        }
        private void WriteList(SanitizedXmlWriterWrapper xmlWriter, IEnumerable<CPurchaseAdviceAdjustmentsFields> list)
        {
            if (list == null)
            {
                return;
            }
            foreach (var o in list)
            {
                if (o == null)
                {
                    continue;
                }
                xmlWriter.WriteStartElement("sPurchaseAdviceAdjustments");
                xmlWriter.WriteAttributeString("AdjDesc", o.AdjDesc);
                xmlWriter.WriteAttributeString("AdjPc", o.AdjPc_rep);
                xmlWriter.WriteAttributeString("AdjAmt", o.AdjAmt_rep);
                xmlWriter.WriteAttributeString("IsSRP", GetBoolString(o.IsSRP));
                xmlWriter.WriteEndElement();
            }
        }
        private void WriteServicingPayments(SanitizedXmlWriterWrapper xmlWriter, IEnumerable<CServicingPaymentFields> list)
        {
            if (list == null)
            {
                return;
            }
            foreach (var o in list)
            {
                if (o == null)
                {
                    continue;
                }
                xmlWriter.WriteStartElement("sServicingPayments");
                xmlWriter.WriteAttributeString("ServicingTransactionT", o.ServicingTransactionT);
                xmlWriter.WriteAttributeString("DueD", o.DueD_rep);
                xmlWriter.WriteAttributeString("DueAmt", o.DueAmt_rep);
                xmlWriter.WriteAttributeString("PaymentD", o.PaymentD_rep);
                xmlWriter.WriteAttributeString("PaymentReceivedD", o.PaymentReceivedD_rep);
                xmlWriter.WriteAttributeString("PaymentAmt", o.PaymentAmt_rep);
                xmlWriter.WriteAttributeString("Principal", o.Principal_rep);
                xmlWriter.WriteAttributeString("Interest", o.Interest_rep);
                xmlWriter.WriteAttributeString("Escrow", o.Escrow_rep);
                xmlWriter.WriteAttributeString("Other", o.Other_rep);
                xmlWriter.WriteAttributeString("LateFee", o.LateFee_rep);
                xmlWriter.WriteAttributeString("Notes", o.Notes);
                xmlWriter.WriteAttributeString("RowNum", o.RowNum.ToString());
                xmlWriter.WriteAttributeString("Payee", o.Payee);
                xmlWriter.WriteAttributeString("TransactionD", o.TransactionD_rep);
                xmlWriter.WriteAttributeString("PaymentStatementDocumentId", o.PaymentStatementDocumentId.ToString());
                xmlWriter.WriteEndElement();
            }
        }

        private void WriteInvestorInfo(SanitizedXmlWriterWrapper xmlWriter, string elementName, InvestorRolodexEntry investorRolodexEntry)
        {
            if (investorRolodexEntry == null)
            {
                return;
            }
            xmlWriter.WriteStartElement(elementName);

            xmlWriter.WriteAttributeString("SellerId", investorRolodexEntry.SellerId);
            xmlWriter.WriteAttributeString("InvestorName", investorRolodexEntry.InvestorName);
            xmlWriter.WriteAttributeString("MERSOrganizationId", investorRolodexEntry.MERSOrganizationId);
            xmlWriter.WriteAttributeString("CompanyName", investorRolodexEntry.CompanyName);
            xmlWriter.WriteAttributeString("MainAttention", investorRolodexEntry.MainAttention);
            xmlWriter.WriteAttributeString("MainAddress", investorRolodexEntry.MainAddress);
            xmlWriter.WriteAttributeString("MainState", investorRolodexEntry.MainState);
            xmlWriter.WriteAttributeString("MainCity", investorRolodexEntry.MainCity);
            xmlWriter.WriteAttributeString("MainZip", investorRolodexEntry.MainZip);
            xmlWriter.WriteAttributeString("MainPhone", investorRolodexEntry.MainPhone);
            xmlWriter.WriteAttributeString("MainFax", investorRolodexEntry.MainFax);
            xmlWriter.WriteAttributeString("MainContactName", investorRolodexEntry.MainContactName);
            xmlWriter.WriteAttributeString("MainEmail", investorRolodexEntry.MainEmail);
            xmlWriter.WriteAttributeString("NoteShipToAttention", investorRolodexEntry.NoteShipToAttention);
            xmlWriter.WriteAttributeString("NoteShipToAddress", investorRolodexEntry.NoteShipToAddress);
            xmlWriter.WriteAttributeString("NoteShipToState", investorRolodexEntry.NoteShipToState);
            xmlWriter.WriteAttributeString("NoteShipToCity", investorRolodexEntry.NoteShipToCity);
            xmlWriter.WriteAttributeString("NoteShipToZip", investorRolodexEntry.NoteShipToZip);
            xmlWriter.WriteAttributeString("NoteShipToPhone", investorRolodexEntry.NoteShipToPhone);
            xmlWriter.WriteAttributeString("NoteShipToFax", investorRolodexEntry.NoteShipToFax);
            xmlWriter.WriteAttributeString("NoteShipToContactName", investorRolodexEntry.NoteShipToContactName);
            xmlWriter.WriteAttributeString("NoteShipToEmail", investorRolodexEntry.NoteShipToEmail);
            xmlWriter.WriteAttributeString("PaymentToAttention", investorRolodexEntry.PaymentToAttention);
            xmlWriter.WriteAttributeString("PaymentToAddress", investorRolodexEntry.PaymentToAddress);
            xmlWriter.WriteAttributeString("PaymentToState", investorRolodexEntry.PaymentToState);
            xmlWriter.WriteAttributeString("PaymentToCity", investorRolodexEntry.PaymentToCity);
            xmlWriter.WriteAttributeString("PaymentToZip", investorRolodexEntry.PaymentToZip);
            xmlWriter.WriteAttributeString("PaymentToPhone", investorRolodexEntry.PaymentToPhone);
            xmlWriter.WriteAttributeString("PaymentToFax", investorRolodexEntry.PaymentToFax);
            xmlWriter.WriteAttributeString("PaymentToContactName", investorRolodexEntry.PaymentToContactName);
            xmlWriter.WriteAttributeString("PaymentToEmail", investorRolodexEntry.PaymentToEmail);
            xmlWriter.WriteAttributeString("LossPayeeAttention", investorRolodexEntry.LossPayeeAttention);
            xmlWriter.WriteAttributeString("LossPayeeAddress", investorRolodexEntry.LossPayeeAddress);
            xmlWriter.WriteAttributeString("LossPayeeState", investorRolodexEntry.LossPayeeState);
            xmlWriter.WriteAttributeString("LossPayeeCity", investorRolodexEntry.LossPayeeCity);
            xmlWriter.WriteAttributeString("LossPayeeZip", investorRolodexEntry.LossPayeeZip);
            xmlWriter.WriteAttributeString("LossPayeePhone", investorRolodexEntry.LossPayeePhone);
            xmlWriter.WriteAttributeString("LossPayeeFax", investorRolodexEntry.LossPayeeFax);
            xmlWriter.WriteAttributeString("LossPayeeContactName", investorRolodexEntry.LossPayeeContactName);
            xmlWriter.WriteAttributeString("LossPayeeEmail", investorRolodexEntry.LossPayeeEmail);

            xmlWriter.WriteEndElement();
        }
        private void WriteLockAdjustments(SanitizedXmlWriterWrapper xmlWriter, string name, IEnumerable<PricingAdjustment> adjustmentList)
        {
            if (adjustmentList == null)
            {
                return;
            }

            foreach (var o in adjustmentList)
            {
                if (o == null)
                {
                    continue;
                }
                xmlWriter.WriteStartElement(name);
                xmlWriter.WriteAttributeString("Description", o.Description);
                xmlWriter.WriteAttributeString("Rate", SanitizePercent(o.Rate));
                xmlWriter.WriteAttributeString("Fee", SanitizePercent(o.Fee));
                xmlWriter.WriteAttributeString("Price", SanitizePercent(o.Price));
                xmlWriter.WriteAttributeString("Margin", SanitizePercent(o.Margin));
                xmlWriter.WriteAttributeString("QualifyingRate", SanitizePercent(o.QualifyingRate));
                xmlWriter.WriteAttributeString("TeaserRate", SanitizePercent(o.TeaserRate));
                xmlWriter.WriteAttributeString("IsHidden", GetBoolString(o.IsHidden));
                xmlWriter.WriteAttributeString("IsLenderAdjustment", GetBoolString(o.IsLenderAdjustment));
                xmlWriter.WriteAttributeString("IsSRPAdjustment", GetBoolString(o.IsSRPAdjustment));

                xmlWriter.WriteEndElement();
            }
        }
        private string SanitizePercent(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return string.Empty;
            }
            return value.Replace("%", "");
        }

        private void WriteEscrowScheduleList(SanitizedXmlWriterWrapper xmlWriter, LosConvert losConvert,
            IEnumerable<EscrowScheduleItem> sEscrowScheduleList)
        {
            if (sEscrowScheduleList == null)
            {
                return;
            }
            foreach (var o in sEscrowScheduleList)
            {
                if (o == null)
                {
                    continue;
                }
                xmlWriter.WriteStartElement("sEscrowScheduleItem");
                xmlWriter.WriteAttributeString("EscrowItemT", o.EscrowItemT.ToString("D"));
                xmlWriter.WriteAttributeString("TaxTableT", o.TaxTableT.ToString("D"));
                xmlWriter.WriteAttributeString("ParcelNum", o.ParcelNum);
                xmlWriter.WriteAttributeString("PayeeNm", o.PayeeNm);
                xmlWriter.WriteAttributeString("NumOfPmt", o.NumOfPmt);
                xmlWriter.WriteAttributeString("PaidByT", o.PaidByT.ToString("D"));

                foreach (TaxScheduleItem scheduleItem in o.TaxScheduleItemList)
                {
                    xmlWriter.WriteStartElement("TaxScheduleItem");
                    xmlWriter.WriteAttributeString("DueD", scheduleItem.DueD_rep);
                    xmlWriter.WriteAttributeString("AmtDue", losConvert.ToMoneyString(scheduleItem.AmtDue, FormatDirection.ToRep));
                    xmlWriter.WriteAttributeString("PmtNum", scheduleItem.PmtNum.ToString());
                    xmlWriter.WriteEndElement(); // </TaxScheduleItem>
                }

                xmlWriter.WriteEndElement(); // </sEscrowScheduleItem>
            }
        }

        private void WriteToleranceCureDebug(SanitizedXmlWriterWrapper xmlWriter, ToleranceCureDebug debugData)
        {
            if (debugData == null)
            {
                return;
            }

            xmlWriter.WriteStartElement("sToleranceCureDebug");

            // Total Tolerance Cure.
            xmlWriter.WriteStartElement("TotalToleranceCure");
            xmlWriter.WriteAttributeString("amount", debugData.ToleranceCure_rep);
            xmlWriter.WriteEndElement(); // TotalToleranceCure

            // Zero Tolerance Cure.
            xmlWriter.WriteStartElement("ZeroToleranceCure");
            xmlWriter.WriteAttributeString("isLocked", GetBoolString(debugData.ZeroToleranceCure.IsCureAmountLckd));
            xmlWriter.WriteAttributeString("amount", debugData.ZeroToleranceCure.CureAmount_rep);
            xmlWriter.WriteAttributeString("lastDisclosedLeArchiveId", debugData.ZeroToleranceCure.ArchiveId.ToString());
            xmlWriter.WriteAttributeString("lastDisclosedLeArchiveDate", debugData.ZeroToleranceCure.ArchiveDate);

            // Only create Fees element if fees exist.
            if (debugData.ZeroToleranceCure.HasLenderCredit || debugData.ZeroToleranceCure.Fees.Any())
            {
                xmlWriter.WriteStartElement("Fees");

                // Lender Credit Dummy Fee.
                if (debugData.ZeroToleranceCure.HasLenderCredit)
                {
                    xmlWriter.WriteStartElement("Fee");
                    xmlWriter.WriteAttributeString("Description", "Lender Credit");
                    xmlWriter.WriteAttributeString("Difference", debugData.ZeroToleranceCure.LenderCreditDifferenceAmount_rep);
                    xmlWriter.WriteAttributeString("Cure", debugData.ZeroToleranceCure.LenderCreditCureAmount_rep);

                    xmlWriter.WriteStartElement("LastDisclosedLeFeeInfo");
                    xmlWriter.WriteAttributeString("amount", debugData.ZeroToleranceCure.LenderCreditArchiveAmount_rep);
                    xmlWriter.WriteEndElement(); // LastDisclosedLeFeeInfo

                    xmlWriter.WriteStartElement("ClosingDisclosureFeeInfo");
                    xmlWriter.WriteAttributeString("amount", debugData.ZeroToleranceCure.LenderCreditLiveAmount_rep);
                    xmlWriter.WriteEndElement(); // ClosingDisclosureFeeInfo

                    xmlWriter.WriteEndElement(); // Fee
                }

                foreach (ToleranceCureDebugFee debugFee in debugData.ZeroToleranceCure.Fees)
                {
                    WriteToleranceCureDebugFee(xmlWriter, debugFee, "LastDisclosedLeFeeInfo", true);
                }

                xmlWriter.WriteEndElement(); // Fees
            }

            xmlWriter.WriteEndElement(); // ZeroToleranceCure

            // Ten Tolerance Cure.
            xmlWriter.WriteStartElement("TenPercentCumulativeToleranceCure");
            xmlWriter.WriteAttributeString("isLocked", GetBoolString(debugData.TenPercentCumulativeToleranceCure.IsCureAmountLckd));
            xmlWriter.WriteAttributeString("amount", debugData.TenPercentCumulativeToleranceCure.CureAmount_rep);
            xmlWriter.WriteAttributeString("TenPercentBasisArchiveId", debugData.TenPercentCumulativeToleranceCure.ArchiveId.ToString());
            xmlWriter.WriteAttributeString("TenPercentBasisArchiveDate", debugData.TenPercentCumulativeToleranceCure.ArchiveDate);


            // Only create Fees and Sums elements if fees exist.
            if (debugData.TenPercentCumulativeToleranceCure.AreRecordingFeesIncluded || debugData.TenPercentCumulativeToleranceCure.NonRecordingFees.Any())
            {
                xmlWriter.WriteStartElement("Fees");

                // Recording Fees.
                if (debugData.TenPercentCumulativeToleranceCure.AreRecordingFeesIncluded)
                {
                    xmlWriter.WriteStartElement("Fee");
                    xmlWriter.WriteAttributeString("Description", "Recording Fee");
                    xmlWriter.WriteAttributeString("Difference", debugData.TenPercentCumulativeToleranceCure.RecordingFeeDifferenceAmount_rep);

                    xmlWriter.WriteStartElement("TenPercentBasisFeeInfo");
                    xmlWriter.WriteAttributeString("amount", debugData.TenPercentCumulativeToleranceCure.RecordingFeeArchiveAmount_rep);
                    xmlWriter.WriteEndElement(); // TenPercentBasisFeeInfo

                    xmlWriter.WriteStartElement("ClosingDisclosureFeeInfo");
                    xmlWriter.WriteAttributeString("amount", debugData.TenPercentCumulativeToleranceCure.RecordingFeeLiveAmount_rep);
                    xmlWriter.WriteEndElement(); // ClosingDisclosureFeeInfo

                    xmlWriter.WriteStartElement("RecordingFees");

                    foreach (ToleranceCureDebugFee debugFee in debugData.TenPercentCumulativeToleranceCure.RecordingFees)
                    {
                        WriteToleranceCureDebugFee(xmlWriter, debugFee, "TenPercentBasisFeeInfo", false);
                    }

                    xmlWriter.WriteEndElement(); // RecordingFees
                    xmlWriter.WriteEndElement(); // Fee
                }

                foreach (ToleranceCureDebugFee debugFee in debugData.TenPercentCumulativeToleranceCure.NonRecordingFees)
                {
                    WriteToleranceCureDebugFee(xmlWriter, debugFee, "TenPercentBasisFeeInfo", false);
                }

                xmlWriter.WriteEndElement(); // Fees

                // Sums row.
                xmlWriter.WriteStartElement("SumOfTenPercentFees");
                xmlWriter.WriteAttributeString("sumOfTenPercentBasisFeeInfo", debugData.TenPercentCumulativeToleranceCure.ArchiveFeesSumAmount_rep);
                xmlWriter.WriteAttributeString("tenPercentOfSumOfTenPercentBasisFeeInfo", debugData.TenPercentCumulativeToleranceCure.TenPercentArchiveFeesSumAmount_rep);
                xmlWriter.WriteAttributeString("sumOfClosingDisclosureFeeInfo", debugData.TenPercentCumulativeToleranceCure.LiveFeesSumAmount_rep);
                xmlWriter.WriteAttributeString("sumOfDifference", debugData.TenPercentCumulativeToleranceCure.FeesSumDifferenceAmount_rep);
                xmlWriter.WriteEndElement(); // SumOfTenPercentFees
            }

            xmlWriter.WriteEndElement(); // TenPercentCumulativeToleranceCure

            // Unlimited Tolerance Fees.
            xmlWriter.WriteStartElement("UnlimitedToleranceFees");
            xmlWriter.WriteAttributeString("lastDisclosedLeArchiveId", debugData.UnlimitedToleranceFees.ArchiveId.ToString());
            xmlWriter.WriteAttributeString("lastDisclosedLeArchiveDate", debugData.UnlimitedToleranceFees.ArchiveDate);


            // Only create Fees element if fees exist.
            if (debugData.UnlimitedToleranceFees.Fees.Any())
            {

                xmlWriter.WriteStartElement("Fees");

                foreach (ToleranceCureDebugFee debugFee in debugData.UnlimitedToleranceFees.Fees)
                {
                    WriteToleranceCureDebugFee(xmlWriter, debugFee, "LastDisclosedLeFeeInfo", false);
                }

                xmlWriter.WriteEndElement(); // Fees
            }

            xmlWriter.WriteEndElement(); // UnlimitedToleranceFees

            xmlWriter.WriteEndElement(); // sToleranceCureDebug
        }

        private void WriteToleranceCureDebugFee(SanitizedXmlWriterWrapper xmlWriter, ToleranceCureDebugFee debugFee, string archiveColName, bool isCureIncluded)
        {
            xmlWriter.WriteStartElement("Fee");
            xmlWriter.WriteAttributeString("Description", debugFee.Description);
            xmlWriter.WriteAttributeString("FeeType", debugFee.OriginalDescription);
            xmlWriter.WriteAttributeString("Difference", debugFee.DifferenceAmount_rep);

            if (isCureIncluded)
            {
                xmlWriter.WriteAttributeString("Cure", debugFee.CureAmount_rep);
            }

            if (debugFee.ArchiveFee != null)
            {
                xmlWriter.WriteStartElement(archiveColName);
                xmlWriter.WriteAttributeString("TotalAmount", debugFee.ArchiveFee.TotalAmount_rep);
                xmlWriter.WriteAttributeString("IntegratedDisclosureSectionT", ClosingCostSetUtils.MapIntegratedDisclosureSectionToString(debugFee.ArchiveFee.IntegratedDisclosureSectionT));
                xmlWriter.WriteAttributeString("IsThirdParty", GetBoolString(debugFee.ArchiveFee.IsThirdParty));
                xmlWriter.WriteAttributeString("IsAffiliate", GetBoolString(debugFee.ArchiveFee.IsAffiliate));
                xmlWriter.WriteAttributeString("CanShop", GetBoolString(debugFee.ArchiveFee.CanShop));
                xmlWriter.WriteAttributeString("DidShop", GetBoolString(debugFee.ArchiveFee.DidShop));
                xmlWriter.WriteEndElement(); // archiveColName
            }

            if (debugFee.LiveFee != null)
            {
                xmlWriter.WriteStartElement("ClosingDisclosureFeeInfo");
                xmlWriter.WriteAttributeString("TotalAmount", debugFee.LiveFee.TotalAmount_rep);
                xmlWriter.WriteAttributeString("IntegratedDisclosureSectionT", ClosingCostSetUtils.MapIntegratedDisclosureSectionToString(debugFee.LiveFee.IntegratedDisclosureSectionT));
                xmlWriter.WriteAttributeString("IsThirdParty", GetBoolString(debugFee.LiveFee.IsThirdParty));
                xmlWriter.WriteAttributeString("IsAffiliate", GetBoolString(debugFee.LiveFee.IsAffiliate));
                xmlWriter.WriteAttributeString("CanShop", GetBoolString(debugFee.LiveFee.CanShop));
                xmlWriter.WriteAttributeString("DidShop", GetBoolString(debugFee.LiveFee.DidShop));
                xmlWriter.WriteEndElement(); // ClosingDisclosureFeeInfo
            }

            xmlWriter.WriteEndElement(); // Fee
        }

        private void WriteUcdDeliveryCollection(SanitizedXmlWriterWrapper xmlWriter, UcdDeliveryCollection ucdDeliveryCollection)
        {
            xmlWriter.WriteStartElement("sUcdDeliveryCollection");

            foreach (UcdDelivery delivery in ucdDeliveryCollection)
            {
                xmlWriter.WriteStartElement("UcdDelivery");

                xmlWriter.WriteAttributeString("UcdDeliveryId", delivery.UcdDeliveryId.ToString());
                xmlWriter.WriteAttributeString("UcdDeliveryType", GetEnumString(delivery.UcdDeliveryType));
                xmlWriter.WriteAttributeString("DeliveredTo", GetEnumString(delivery.DeliveredTo));
                xmlWriter.WriteAttributeString("CaseFileId", delivery.CaseFileId);
                xmlWriter.WriteAttributeString("DateOfDelivery", delivery.DateOfDelivery.ToStringWithDefaultFormatting());

                // Type specific fields.
                switch (delivery.UcdDeliveryType)
                {
                    case UcdDeliveryType.Manual:
                        // DO NOTHING. Manual has no extra fields.
                        break;
                    case UcdDeliveryType.FannieMaeUcdDelivery:
                        FannieMaeUcdDelivery fannie = (FannieMaeUcdDelivery)delivery;
                        xmlWriter.WriteAttributeString("BatchId", fannie.BatchId);
                        xmlWriter.WriteAttributeString("Status", GetEnumString(fannie.CasefileStatus));
                        break;
                    case UcdDeliveryType.FreddieMacUcdDelivery:
                        FreddieMacUcdDelivery freddie = (FreddieMacUcdDelivery)delivery;
                        xmlWriter.WriteAttributeString("BatchId", freddie.BatchId);
                        xmlWriter.WriteAttributeString("Status", GetEnumString(freddie.UcdRequirementStatus));
                        break;
                    default:
                        throw new UnhandledEnumException(delivery.UcdDeliveryType);
                }

                xmlWriter.WriteAttributeString("Note", delivery.Note);

                xmlWriter.WriteEndElement(); // UcdDelivery
            }

            xmlWriter.WriteEndElement(); // sUcdDeliveryCollection
        }

        /// <summary>
        /// Writes out a list of transactions to the export.
        /// </summary>
        /// <param name="xmlWriter">An XML writer.</param>
        /// <param name="transactions">A list of transactions.</param>
        private void WriteTransactions(SanitizedXmlWriterWrapper xmlWriter, List<CTransactionFields> transactions)
        {
            if (transactions == null || !transactions.Any())
            {
                return;
            }

            xmlWriter.WriteStartElement("sTransactions");

            foreach (var transaction in transactions)
            {
                xmlWriter.WriteStartElement("Transaction");
                xmlWriter.WriteAttributeString("Entity", transaction.Entity);
                xmlWriter.WriteAttributeString("ItemDescription", transaction.ItemDescription);
                xmlWriter.WriteAttributeString("Notes", transaction.Notes);
                xmlWriter.WriteAttributeString("PmtDir", transaction.PmtDir_rep);
                xmlWriter.WriteAttributeString("PmtMade", GetBoolString(transaction.PmtMade));
                xmlWriter.WriteAttributeString("TransactionAmt", transaction.TransactionAmt_rep);
                xmlWriter.WriteAttributeString("TransactionD", transaction.TransactionD_rep);
                xmlWriter.WriteAttributeString("TransNum", transaction.TransNum);
                xmlWriter.WriteEndElement(); // Transaction
            }

            xmlWriter.WriteEndElement(); // sTransactions
        }

        /// <summary>
        /// Wraps an XmlWriter to allow for easy sanitization of all values input into 
        /// attributes and elements.<para/>
        /// Does not sanitize node names, since those should be safe.
        /// </summary>
        /// <remarks>
        /// I would inherit from XmlWriter, but it is abstract and XmlWriter.Create(...)
        /// returns an instance of one of several subclasses.  Thus the simplest solution
        /// is a façade class. - TJ 10/21/2015, Case 229638
        /// </remarks>
        private class SanitizedXmlWriterWrapper : IDisposable
        {
            private readonly XmlWriter writer;

            /// <summary>
            /// Initializes a new instance of the <see cref="SanitizedXmlWriterWrapper"/> class.
            /// </summary>
            /// <param name="writer">The <see cref="XmlWriter"/> performing the underlying write operations.</param>
            public SanitizedXmlWriterWrapper(XmlWriter writer)
            {
                this.writer = writer;
            }

            /// <summary>
            /// Releases all resources used by the current instance.
            /// </summary>
            public void Dispose()
            {
                this.writer.Dispose();
            }

            /// <summary>
            /// Writes out the attribute with the specified local name and value.
            /// </summary>
            /// <param name="localName">The local name of the attribute.</param>
            /// <param name="value">The value of the attribute.</param>
            public void WriteAttributeString(string localName, string value)
            {
                this.writer.WriteAttributeString(localName, GetSanitizedString(value));
            }

            /// <summary>
            /// Writes out a start tag with the specified local name.
            /// </summary>
            /// <param name="localName">The local name of the element.</param>
            public void WriteStartElement(string localName)
            {
                this.writer.WriteStartElement(localName);
            }

            /// <summary>
            /// Closes one element and pops the corresponding namespace scope.
            /// </summary>
            public void WriteEndElement()
            {
                this.writer.WriteEndElement();
            }

            /// <summary>
            /// Sanitizes the input string into a string of only valid Xml characters.
            /// </summary>
            /// <param name="input">A string with possibly one or more invalid Xml characters.</param>
            /// <returns>A string version of input, without any invalid Xml characters.</returns>
            /// <example>"Some \x07Invalid String" would become "Some Invalid String".</example>
            private static string GetSanitizedString(string input)
            {
                if (string.IsNullOrEmpty(input))
                {
                    return input;
                }

                IList<int> invalidCharacterIndices = DetermineInvalidXmlCharacterIndices(input);

                if (invalidCharacterIndices.Count == 0)
                {
                    return input;
                }

                return BuildStringSkippingIndices(input, invalidCharacterIndices);
            }

            /// <summary>
            /// Determines which indices (if any) of <paramref name="input"/> are invalid Xml characters.
            /// </summary>
            /// <param name="input">A string which might contain some invalid Xml characters.</param>
            /// <returns>A list of the indices of invalid characters in <paramref name="input"/>.</returns>
            private static IList<int> DetermineInvalidXmlCharacterIndices(string input)
            {
                List<int> invalidCharacterIndices = new List<int>();
                for (int i = 0; i < input.Length; ++i)
                {
                    if (!Tools.IsLegalXmlChar(input[i]))
                    {
                        invalidCharacterIndices.Add(i);
                    }
                }

                return invalidCharacterIndices;
            }

            /// <summary>
            /// Builds a string from input without the characters at the indices specified by <paramref name="indicesToSkip"/>.
            /// </summary>
            /// <param name="input">The input string to use.</param>
            /// <param name="indicesToSkip">The indices to skip.</param>
            /// <returns>A new string without the specified indices.</returns>
            /// <remarks>This method trusts its inputs; it is intentionally left private.</remarks>
            private static string BuildStringSkippingIndices(string input, IList<int> indicesToSkip)
            {
                // It will have to copy the array to build the final string, but it would have
                // to do even more with a StringBuilder, so it seems best to keep it simple.
                char[] result = new char[input.Length - indicesToSkip.Count];
                int subSectionStartingPoint = 0;
                for (int currentSkipIndex = 0; currentSkipIndex < indicesToSkip.Count; ++currentSkipIndex)
                {
                    for (int i = subSectionStartingPoint; i < indicesToSkip[currentSkipIndex]; ++i)
                    {
                        result[i - currentSkipIndex] = input[i];
                    }

                    subSectionStartingPoint = indicesToSkip[currentSkipIndex] + 1;
                }

                for (int i = subSectionStartingPoint; i < input.Length; ++i)
                {
                    result[i - indicesToSkip.Count] = input[i];
                }

                return new string(result);
            }
        }
    }
}
