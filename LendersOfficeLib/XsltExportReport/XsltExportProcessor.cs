﻿using System;
using System.Threading;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Constants;

namespace LendersOffice.XsltExportReport
{
    /// <summary>
    /// 1/26/2018 - dd - This class will be remove once XsltExportCancellableRunnable is verify on production.
    /// DO NOT UPDATE this class.
    /// </summary>
    public class XsltExportProcessor : CommonProjectLib.Runnable.IRunnable
    {
        #region IRunnable Members
        
        public string Description
        {
            get
            {
                return "Generates Batch Exports From MSMQ:  " + ConstStage.MSMQ_XsltReportRequest;
            }
        }


        public void Run()
        {
            Console.WriteLine("Starting RUN");
            // Retrieve Request From Queue.
            // De-serialize
            // Construct the Processor
            using (var workerTiming = new WorkerExecutionTiming("XsltExportProcessor"))
            {
                while (true)
                {
                    XsltExportRequest item = XsltExportRequestQueue.Receive();

                    if (item == null)
                    {
                        return;
                    }

                    Guid brokerId;
                    var info = DbConnectionInfo.GetConnectionInfoByUserId(item.UserId, out brokerId);
                    XsltExportResultStatusItem status = XsltExportResultStatus.GetResult(brokerId, item.UserId, item.ReportId);

                    if (status != null &&  status.Status == E_XsltExportResultStatusT.Complete)
                    {
                        Tools.LogInfo($"XsltExportProcessor Skipping item {item.MapName} {item.ReportName} is already complete.");
                        return;
                    }

                    using (var itemTiming = workerTiming.RecordItem("[" + item.MapName + "] - " + item.ReportName, item.CreatedDate))
                    {
                        try
                        {
                            Log($"XsltExportProcessor picked up {item.MapName} {item.ReportName} {item.CreatedDate} {DateTime.Now}");
                            Tools.ResetLogCorrelationId();
                         
                            XsltExportWorker worker = new XsltExportWorker(item);

                            worker.Execute();
                        }
                        catch (Exception exc)
                        {
                            itemTiming.RecordError(exc);
                            throw;
                        }
                    }
                }
            }
        }

        private void Log(string msg)
        {
            Console.WriteLine(msg);
            Tools.LogInfo(msg);
        }

        #endregion
    }

    public class XsltExportCancellableRunnable : CommonProjectLib.Runnable.ICancellableRunnable
    {
        public string Description
        {
            get
            {
                return "Generates Batch Exports From MSMQ:  " + ConstStage.MSMQ_XsltReportRequest;
            }
        }

        public void Run(CancellationToken cancellationToken)
        {
            // Retrieve Request From Queue.
            // De-serialize
            // Construct the Processor
            using (var workerTiming = new WorkerExecutionTiming("XsltExportProcessor"))
            {
                while (!cancellationToken.IsCancellationRequested)
                {
                    XsltExportRequest item = XsltExportRequestQueue.Receive();

                    if (item == null)
                    {
                        return;
                    }

                    Guid brokerId;
                    var info = DbConnectionInfo.GetConnectionInfoByUserId(item.UserId, out brokerId);
                    XsltExportResultStatusItem status = XsltExportResultStatus.GetResult(brokerId, item.UserId, item.ReportId);

                    if (status != null && status.Status == E_XsltExportResultStatusT.Complete)
                    {
                        Tools.LogInfo($"XsltExportProcessor Skipping item {item.MapName} {item.ReportName} is already complete.");
                        return;
                    }

                    using (var itemTiming = workerTiming.RecordItem("[" + item.MapName + "] - " + item.ReportName, item.CreatedDate))
                    {
                        try
                        {
                            Tools.LogInfo($"XsltExportProcessor picked up {item.MapName} {item.ReportName} {item.CreatedDate} {DateTime.Now}");
                            Tools.ResetLogCorrelationId();

                            XsltExportWorker worker = new XsltExportWorker(item);

                            worker.Execute();
                        }
                        catch (Exception exc)
                        {
                            itemTiming.RecordError(exc);
                            throw;
                        }
                    }
                }
            }
        }
    }
}