﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendersOffice.Constants;
using System.Text.RegularExpressions;
using System.IO;
using DataAccess;
using System.Xml;
using LendersOffice.Security;
using LendersOffice.Common;
using System.Xml.Xsl;
using System.Diagnostics;
using System.Data.Common;
using System.Data.SqlClient;

namespace LendersOffice.XsltExportReport
{
    public class XsltExport
    {

        public static void SubmitRequest(Guid brokerId, XsltExportRequest request)
        {
            // Send Request to MSMQ
            // Return Report Id
            XsltExportRequestQueue.Send(request);
            XsltExportResultStatus.Insert(brokerId, request);

        }

        public static void ResubmitRequest(Guid brokerId, Guid userId, string reportId)
        {
            // Retrieve Request.

            
            SqlParameter[] parameters = {
                                            new SqlParameter("@ReportId", reportId),
                                            new SqlParameter("@UserId", userId)
                                        };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "XSLT_EXPORT_RESULT_STATUS_PrepareResubmit", parameters))
            {
                if (reader.Read())
                {
                    string xml = (string)reader["RequestXml"] ;
                    XsltExportRequest request = XsltExportRequest.Parse(xml);

                    XsltExportRequestQueue.Send(request);
                }
            }
        }

        public static XsltExportResultStatusItem GetResult(Guid brokerId, Guid userId, string reportId)
        {
            return XsltExportResultStatus.GetResult(brokerId, userId, reportId);
        }

        public static IEnumerable<XsltExportResultStatusItem> ListByUserId(Guid brokerId, Guid userId)
        {
            return XsltExportResultStatus.ListByUserId(brokerId, userId);
        }
        public static XsltExportResult ExecuteSynchronously(XsltExportRequest request, AbstractUserPrincipal principal = null)
        {
            string reportName = string.Empty;

            XsltExportWorker worker = new XsltExportWorker(request, false, /* isAsync*/ principal);
            return worker.Execute();
        }
    }
}
