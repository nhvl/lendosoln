﻿namespace LendersOffice.Queries.Configuration
{
    using LqbGrammar.Queries;

    /// <summary>
    /// Factory for creating configuration queries.
    /// </summary>
    public sealed class ConfigurationQueryFactory : IConfigurationQueryFactory
    {
        /// <summary>
        /// Cache the site configuration to avoid spurious object creations.
        /// </summary>
        private static ISiteConfigurationQuery cachedSiteQuery;

        /// <summary>
        /// Cache the stage configuration to avoid spurious object creations.
        /// </summary>
        private static IStageConfigurationQuery cachedStageQuery;

        /// <summary>
        /// Since creation of the cached data will be deferred,
        /// this lock object is necessary to ensure a single thread
        /// does the initialization.
        /// </summary>
        private static object initLock = new object();

        /// <summary>
        /// Return an instance of ISiteConfigurationQuery to the calling code.
        /// </summary>
        /// <returns>An instance of ISiteConfigurationQuery.</returns>
        public ISiteConfigurationQuery CreateSiteConfiguration()
        {
            CreateCachedQueries();
            return cachedSiteQuery;
        }

        /// <summary>
        /// Return an instance of IStageConfigurationQuery to the calling code.
        /// </summary>
        /// <returns>An instance of IStageConfigurationQuery.</returns>
        public IStageConfigurationQuery CreateStageConfiguration()
        {
            CreateCachedQueries();
            return cachedStageQuery;
        }

        /// <summary>
        /// Create the cached queries the first time this is called.
        /// </summary>
        private static void CreateCachedQueries()
        {
            if (cachedSiteQuery == null)
            {
                lock (initLock)
                {
                    if (cachedSiteQuery == null)
                    {
                        var siteQuery = new SiteConfigurationQuery();
                        var siteCache = new CachedSiteConfigurationQuery(siteQuery);

                        var stageQuery = new StageConfigurationQuery();
                        var stageCache = new CachedStageConfigurationQuery(stageQuery);

                        cachedStageQuery = stageCache;
                        cachedSiteQuery = siteCache;
                    }
                }
            }
         }
    }
}
