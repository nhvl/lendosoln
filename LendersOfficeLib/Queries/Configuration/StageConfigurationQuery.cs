﻿namespace LendersOffice.Queries.Configuration
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
using System.Data.SqlClient;
    using DataAccess;
    using Drivers.SqlServerDB;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Queries;
    using LqbGrammar.Utils;

    /// <summary>
    /// Configuration driver to get data in the Stage category.
    /// </summary>
    internal sealed class StageConfigurationQuery : IStageConfigurationQuery
    {
        /// <summary>
        /// The validated name of the stored procedure used to retrieve the configuration data.
        /// </summary>
        private static readonly StoredProcedureName SPName;

        /// <summary>
        /// Initializes static members of the <see cref="StageConfigurationQuery" /> class.
        /// </summary>
        static StageConfigurationQuery()
        {
            var test = StoredProcedureName.Create("ListStageConfigKeys");
            SPName = (test == null) ? StoredProcedureName.Invalid : test.Value;
        }

        /// <summary>
        /// Deliver the configuration data to the calling code.
        /// </summary>
        /// <returns>The configuration data.</returns>
        public Dictionary<string, Tuple<int, string>> ReadAllValues()
        {
            return this.LoadStageConfiguration();
        }

        /// <summary>
        /// This method reads the configuration data and assigns it to the member
        /// field that holds the data.  It is called by the PeriodicExpiringResource
        /// instance whenever the data has expired.
        /// </summary>
        /// <returns>The configuration data.</returns>
        private Dictionary<string, Tuple<int, string>> LoadStageConfiguration()
        {
            var config = new Dictionary<string, Tuple<int, string>>();

            // NOTE: this forces site configuration to get loaded first via the GetConnection(DataSrc.LOShareROnly) call.
            using (DbConnection conn = DbAccessUtils.GetConnection(DataSrc.LOShareROnly))
            {
                using (var reader = StoredProcedureDriverHelper.ExecuteReader(conn, null, SPName, null, TimeoutInSeconds.Sixty))
                {
                    while (reader.Read())
                    {
                        string keyIdStr = (string)reader["KeyIdStr"];
                        int optionContentInt = (int)reader["OptionContentInt"];
                        string optionContentStr = (string)reader["OptionContentStr"];

                        if (keyIdStr == "FullQueryThresholdLoggingInMs")
                        {
                            ObjLib.Profiling.PerformanceMonitor.PerformanceMonitorStateChangeHandler.FullQueryThresholdLoggingInMs = optionContentInt;
                        }

                        if (keyIdStr == "FullQueryThresholdLoggingUserName")
                        {
                            HashSet<string> users;

                            if (!string.IsNullOrEmpty(optionContentStr))
                            {
                                users = new HashSet<string>(optionContentStr.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries), StringComparer.OrdinalIgnoreCase);
                                ObjLib.Profiling.PerformanceMonitor.PerformanceMonitorStateChangeHandler.FullQueryThresholdLoggingUsers = users;
                            }
                        }

                        config[keyIdStr] = new Tuple<int, string>(optionContentInt, optionContentStr);
                    }
                }
            }

            return config;
        }
    }
}
