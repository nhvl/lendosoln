﻿namespace LendersOffice.Queries.Configuration
{
    using System;
    using System.Collections.Generic;
    using LqbGrammar.Queries;
    using LqbGrammar.Utils;

    /// <summary>
    /// Decorator for StageConfigurationQuery, this class caches the
    /// configuration data.
    /// </summary>
    internal sealed class CachedStageConfigurationQuery : IStageConfigurationQuery
    {
        /// <summary>
        /// The cached data.
        /// </summary>
        private static Data cachedData;

        /// <summary>
        /// Since creation of the cached data will be deferred,
        /// this lock object is necessary to ensure a single thread
        /// does the initialization.
        /// </summary>
        private static object initLock = new object();

        /// <summary>
        /// Initializes a new instance of the <see cref="CachedStageConfigurationQuery"/> class.
        /// </summary>
        /// <param name="query">The contained query instance.</param>
        public CachedStageConfigurationQuery(IStageConfigurationQuery query)
        {
            if (cachedData == null)
            {
                lock (initLock)
                {
                    if (cachedData == null)
                    {
                        cachedData = new Data(query);
                    }
                }
            }
        }

        /// <summary>
        /// Deliver the configuration data to the calling code.
        /// </summary>
        /// <returns>The configuration data.</returns>
        public Dictionary<string, Tuple<int, string>> ReadAllValues()
        {
            return cachedData.ConfigValues;
        }

        /// <summary>
        /// Utility class for the cache logic.
        /// </summary>
        private class Data
        {
            /// <summary>
            /// The stage configuration data will be refreshed every 15 minutes.
            /// </summary>
            private const int FifteenMinutes = 15 * 60 * 1000;

            /// <summary>
            /// The contained query that this class decorates.
            /// </summary>
            private IStageConfigurationQuery simpleQuery;

            /// <summary>
            /// This instance will be called prior to returning the data to the caller.  If the
            /// data has expired then this class will coordinate the data refresh logic.
            /// </summary>
            private PeriodicExpiringResource expireCheck;

            /// <summary>
            /// This is the configuration data.
            /// </summary>
            private Dictionary<string, Tuple<int, string>> configuration;

            /// <summary>
            /// Initializes a new instance of the <see cref="Data"/> class.
            /// </summary>
            /// <param name="query">The query instance used to retrieve the data.</param>
            internal Data(IStageConfigurationQuery query)
            {
                this.simpleQuery = query;
                this.LoadConfiguration();
                this.expireCheck = new PeriodicExpiringResource(FifteenMinutes, this.LoadConfiguration);
            }

            /// <summary>
            /// Gets the configuration data.
            /// </summary>
            /// <value>The configuration data.</value>
            internal Dictionary<string, Tuple<int, string>> ConfigValues
            {
                get
                {
                    this.expireCheck.CheckExpired(this);
                    return this.configuration;
                }
            }

            /// <summary>
            /// Retrieve the configuration data using the contained query object.
            /// </summary>
            private void LoadConfiguration()
            {
                this.configuration = this.simpleQuery.ReadAllValues();
            }
        }
    }
}
