﻿Configuration settings are stored in three locations, the application configuration file
and two tables in the database.  One table is called STAGE_CONFIG and these values are
known as 'stage constants'.  The other table is called SITE_CONFIG and a merge of these
values with the values from the application config file contains values that are knows
as 'site constants'.

The original way to access these values was through two classes, ConstSite and ConstStage.
These classes did a lot of work, including caching and post-processing of the values.

The new design has a multi-layer approach, where each layer adds some functionality.  I
will describe these layers from the top down:
- The ConstSite and ConstStage classes continue to represent the top layer that calling
  code uses to retrieve configuration data.  There is currently a hard-coded feature flag
  that can switch between the old and new configurations.  There are unit tests that
  confirm switching between these two implementations yield the same results.
  The post-processing of the values continues to be done here using the same code no
  matter how the feature flag is set.
- The next layer consists of the classes SiteConfigDatum and StageConfigDatum.  These
  classes map the property names to the key names for the configuration values within
  the respective configuration sources.  Usually the key names and property names are
  the same, but they do occasionally differ and this layer handles that mapping.
  At first blush this layer may appear to be of limited utility, but it allows us
  to keep single instances of the classes in the next lower data, which means we only
  need to validate the key names once rather than constantly throughout the lifetime
  of the application.
- The next layer consists of the classes SiteConfigValue and StageConfigValue, both of
  which inherit from ConfigurationValueBase.  These classes are the bridge to the FOOL
  architecture.  They validate the key names and use ILqbQuery interfaces to retrieve the
  configuration values.
- The next layer consists of the classes CachedSiteConfigurationQuery and
  CachedStageConfigurationQuery.  These are decorator objects that add the caching
  logic to the configuration system.
  The caching is handled using the newly introduced class PerodicExpiringResource, which
  inherits from ExpiringResource.  These classes encapsulate resource expiration checking
  and reloading.  They avoid having multiple threads lock while a resource is reloaded
  and also avoid using an extra thread to handle the locking.  This is achieved via
  two flags.  One is a public flag that all threads see and advises the threads that even
  though a resource has expired they should continue to use the existing resource.  The
  second flag is private and held in thread local storage.  The first thread that detects
  the expiration of the resource gets this flag set and that signals the thread to reload
  the resource.
- The next layer consists of the classes SiteConfigurationQuery and StageConfigurationQuery.
  These classes handle retrieval of the data using the appropriate drivers.
- The next layer consists of the classes ApplicationSettingsDriver and StoredProcedureDriver.
  These drivers use associated adapters to pull the configuration settings from the
  storage sources.
- The lowest layer consists of the classes ApplicationSettingsAdapter and
  StoredProcedureAdapter.  These classes use the dotnet framework to get the data.
