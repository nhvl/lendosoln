﻿namespace LendersOffice.Queries.Configuration
{
    using System.Collections.Generic;
    using System.Data.Common;
using System.Data.SqlClient;
    using DataAccess;
    using Drivers.Configuration;
    using Drivers.SqlServerDB;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Queries;

    /// <summary>
    /// Configuration driver to get data in the Site category.
    /// </summary>
    internal sealed class SiteConfigurationQuery : ISiteConfigurationQuery
    {
        /// <summary>
        /// We cannot load the connection string for accessing the stored procedure
        /// that gathers configuration from the not yet initialized configuration.
        /// Instead, we have to get the value from the application settings, using
        /// this key name.
        /// </summary>
        private const string BootstrapConnStrName = "SecureDBConnStrReadOnly";

        /// <summary>
        /// The validated name of the stored procedure used to retrieve the configuration data.
        /// </summary>
        private static readonly StoredProcedureName SPName;

        /// <summary>
        /// Initializes static members of the <see cref="SiteConfigurationQuery" /> class.
        /// </summary>
        static SiteConfigurationQuery()
        {
            var test = StoredProcedureName.Create("SITE_CONFIG_ListAll");
            SPName = (test == null) ? StoredProcedureName.Invalid : test.Value;
        }

        /// <summary>
        /// Formulate the dictionary key from the component parts.
        /// </summary>
        /// <param name="applicationId">An identifier for a particular application.</param>
        /// <param name="keyName">An identifier for a particular datum.</param>
        /// <returns>A dictionary key that will be used to look up the configuration values.</returns>
        public static string GetKey(string applicationId, string keyName)
        {
            return applicationId + "::" + keyName;
        }

        /// <summary>
        /// Deliver the configuration data to the calling code.
        /// </summary>
        /// <returns>The configuration data.</returns>
        public Dictionary<string, string> ReadAllValues()
        {
            var appSettings = this.LoadAppSettings();
            var sqlSettings = this.LoadSiteConfiguration(appSettings);
            return this.MergeConfigSources(appSettings, sqlSettings);
        }

        /// <summary>
        /// Pull the application settings from the application's
        /// configuration file.
        /// </summary>
        /// <returns>The configuration settings found in the application's configuration file.</returns>
        private Dictionary<string, string> LoadAppSettings()
        {
            return ApplicationSettingsHelper.ReadAllSettings();
        }

        /// <summary>
        /// This method reads the configuration data and assigns it to the member
        /// field that holds the data.  It is called by the PeriodicExpiringResource
        /// instance whenever the data has expired.
        /// </summary>
        /// <param name="appSettings">The application settings, one of which is the database connection string.</param>
        /// <returns>The configuration found in the database.</returns>
        private Dictionary<string, string> LoadSiteConfiguration(Dictionary<string, string> appSettings)
        {
            var config = new Dictionary<string, string>();
            using (DbConnection conn = this.RetrieveBootstrapConnection(appSettings))
            {
                using (var reader = StoredProcedureDriverHelper.ExecuteReader(conn, null, SPName, null, TimeoutInSeconds.Sixty))
                {
                    while (reader.Read())
                    {
                        string applicationId = (string)reader["ApplicationId"];
                        string keyName = (string)reader["KeyName"];
                        string value = (string)reader["Value"];

                        string dictionaryKey = GetKey(applicationId, keyName);

                        config[dictionaryKey] = value;
                    }
                }
            }

            return config;
        }

        /// <summary>
        /// Merge two configuration sources in such a way that the primary data overrides the secondary data.
        /// </summary>
        /// <param name="primary">The data from the primary configuration source.</param>
        /// <param name="secondary">The data from the secondary configuration source.</param>
        /// <returns>The merged data.</returns>
        private Dictionary<string, string> MergeConfigSources(Dictionary<string, string> primary, Dictionary<string, string> secondary)
        {
            // A datum in the primary will over-write a datum in the secondary if the keys are the same.
            foreach (string key in primary.Keys)
            {
                secondary[key] = primary[key];
            }

            return secondary;
        }

        /// <summary>
        /// Use the bootstrap connection string name to create the connection
        /// for retrieving the configuration data.
        /// </summary>
        /// <param name="appSettings">The application settings, one of which is the connection string.</param>
        /// <returns>A connection to the database that holds the configuration data.</returns>
        private DbConnection RetrieveBootstrapConnection(Dictionary<string, string> appSettings)
        {
            return DbAccessUtils.GetConnection(DataSrc.LOShareROnly);
        }
    }
}
