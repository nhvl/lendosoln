﻿namespace LendersOffice.Queries.MethodInvoke
{
    using System;
    using System.Data.Common;
using System.Data.SqlClient;
    using Constants;
    using DataAccess;
    using LendersOffice.Drivers.FileSystem;
    using LendersOffice.QueryProcessor;
    using LendersOffice.Reports;
    using LendersOffice.Security;
    using LqbGrammar;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Interface used to access the custom report functionality.
    /// </summary>
    public interface ICustomReportRunner : ILqbQuery
    {
        /// <summary>
        /// Create an instance of CustomReportRunner that is initialized for execution.
        /// </summary>
        /// <param name="queryId">Identifier for the custom report query.</param>
        /// <param name="brokerId">Identifier for the broker.</param>
        /// <param name="datakey">The filedb datakey to which the report should be written.</param>
        /// <param name="enforceReportSizeCap">Whether or not we wish to fail if the report would be too large.  Default is false.</param>
        /// <returns>An instance of CustomReportRunner that is initialized for execution.</returns>
        CustomReportRunner CreatePreparedRunner(ReportQueryIdentifier queryId, BrokerIdentifier brokerId, FileIdentifier datakey, bool enforceReportSizeCap = false);
    }

    /// <summary>
    /// Run custom reports using the method invoke mechanism.
    /// </summary>
    public sealed class CustomReportRunner : ICustomReportRunner, IMethodInvokeClass<CustomReportRunner, string>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CustomReportRunner"/> class.
        /// </summary>
        /// <remarks>This constructor is used by the method invocation framework.</remarks>
        public CustomReportRunner()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomReportRunner"/> class.
        /// </summary>
        /// <param name="queryId">Identifier for the custom report query.</param>
        /// <param name="brokerId">Identifier for the broker.</param>
        /// <param name="datakey">The filedb datakey to which the report should be written.</param>
        /// <param name="enforceReportSizeCap">Whether or not to enforce a cap on the report size.</param>
        private CustomReportRunner(ReportQueryIdentifier queryId, BrokerIdentifier brokerId, FileIdentifier datakey, bool enforceReportSizeCap = false)
        {
            this.QueryId = queryId;
            this.BrokerId = brokerId;
            this.DataKey = datakey;
            this.EnforceReportSizeCap = enforceReportSizeCap;
        }

        /// <summary>
        /// Gets or sets the identifier for the custom report query.
        /// </summary>
        /// <value>The identifier for the custom report query.</value>
        private ReportQueryIdentifier QueryId { get; set; }

        /// <summary>
        /// Gets or sets the identifier for the broker.
        /// </summary>
        /// <value>The identifier for the broker.</value>
        private BrokerIdentifier BrokerId { get; set; }

        /// <summary>
        /// Gets or sets the filedb datakey.
        /// </summary>
        /// <value>The filedb datakey.</value>
        private FileIdentifier DataKey { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether or not to enforce a cap on the report.
        /// </summary>
        /// <value>Whether or not to enforce the cap on the report being run.</value>
        private bool EnforceReportSizeCap { get; set; }

        /// <summary>
        /// Create a properly initialized instance that a webpage can use to run custom reports.
        /// The instance retrieved from the service locator will be an instanton and so shouldn't 
        /// be used.  In effect, we are merging the factory code into the this class.
        /// </summary>
        /// <param name="queryId">Identifier for the custom report query.</param>
        /// <param name="brokerId">Identifier for the broker.</param>
        /// <param name="datakey">The filedb datakey to which the report should be written.</param>
        /// <param name="enforceReportSizeCap">Whether or not we wish to fail if the report would be too large.  Default is false.</param>
        /// <returns>A properly initialized instance that a webpage can use to run custom reports.</returns>
        public CustomReportRunner CreatePreparedRunner(ReportQueryIdentifier queryId, BrokerIdentifier brokerId, FileIdentifier datakey, bool enforceReportSizeCap = false)
        {
            return new CustomReportRunner(queryId, brokerId, datakey, enforceReportSizeCap: enforceReportSizeCap);
        }

        /// <summary>
        /// Prepare for a streamlined method call.
        /// </summary>
        /// <returns>The streamlined method call.</returns>
        public SingleArgumentMethod<CustomReportRunner, string> GetMethod()
        {
            var factory = GenericLocator<ISecurityPrincipalDriverFactory>.Factory;
            var driver = factory.Create();
            string principal = driver.SerializePrincipal();

            string details = string.Join("|", this.QueryId.ToString(), this.BrokerId.ToString(), this.DataKey.ToString(), principal, this.EnforceReportSizeCap.ToString());
            return new SingleArgumentMethod<CustomReportRunner, string>(crr => crr.Execute(details), details);
        }

        /// <summary>
        /// Called by the method invocation framework, this method executes the custom report code.
        /// </summary>
        /// <param name="details">Bar delimited: queryId, brokerId, datakey, principal.</param>
        public void Execute(string details)
        {
            string datakey = null;

            try
            {
                string[] controls = details.Split('|');

                var queryId = Guid.Parse(controls[0]);
                var brokerId = Guid.Parse(controls[1]);
                datakey = controls[2];
                var serializedPrincipal = controls[3];
                
                var factory = GenericLocator<ISecurityPrincipalDriverFactory>.Factory;
                if (factory == null)
                {
                    throw new DeveloperException(ErrorMessage.BadConfiguration);
                }

                var driver = factory.Create();
                driver.RecoverPrincipal(serializedPrincipal);

                bool enforceReportSizeCap = false;
                if (controls.Length > 4)
                {
                    enforceReportSizeCap = bool.Parse(controls[4]);
                }

                this.RunQuery(queryId, brokerId, datakey, enforceReportSizeCap: enforceReportSizeCap);
            }
            catch (CBaseException cb)
            {
                this.StoreError(datakey, cb.UserMessage);
                Tools.LogError(cb);
            }
            catch (LqbException ex)
            {
                this.StoreError(datakey, ex.Message);
                ex.PassToHandler();
            }
        }

        /// <summary>
        /// Write the an error message to the temporary filedb file.
        /// </summary>
        /// <param name="datakey">The datakey to use when writing the report to the temp filedb.</param>
        /// <param name="message">The error message.</param>
        private void StoreError(string datakey, string message)
        {
            const string ErrorPrefix = "ERROR: ";
            var location = FileStorageIdentifier.TryParse(ConstStage.FileDBSiteCodeForTempFile);
            if (location != null)
            {
                var key = FileIdentifier.TryParse(datakey);
                if (key != null)
                {
                    string contents = ErrorPrefix + message;
                    this.StoreFile(location.Value, key.Value, contents);
                }
            }
        }

        /// <summary>
        /// Write data to a temporary filedb file.
        /// </summary>
        /// <param name="location">The filedb storage location.</param>
        /// <param name="key">The filedb file identifier.</param>
        /// <param name="contents">The content of the data to be written.</param>
        private void StoreFile(FileStorageIdentifier location, FileIdentifier key, string contents)
        {
            Action<LocalFilePath> saveHandler = delegate(LocalFilePath path)
            {
                TextFileHelper.WriteString(path, contents, false);
            };

            var factory = GenericLocator<IFileDbDriverFactory>.Factory;
            var driver = factory.Create();
            driver.SaveNewFile(location, key, saveHandler);
        }

        /// <summary>
        /// Ensure that only LqbExceptions come out so as not to be forced into bypassing StyleCop's excption handling rule.
        /// </summary>
        /// <param name="queryId">Identifier for the custom report query.</param>
        /// <param name="brokerId">Identifier for the broker.</param>
        /// <param name="datakey">The datakey to use when writing the report to the temp filedb.</param>
        /// <param name="enforceReportSizeCap">Whether or not we wish to fail if the report would be too large.  Default is false.</param>
        private void RunQuery(Guid queryId, Guid brokerId, string datakey, bool enforceReportSizeCap = false)
        {
            try
            {
                this.RunQueryImpl(queryId, brokerId, datakey, enforceReportSizeCap: enforceReportSizeCap);
            }
            catch (CBaseException)
            {
                throw;
            }
            catch (LqbException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ServerException(ErrorMessage.SystemError, ex);
            }
        }

        /// <summary>
        /// The code that runs the query is here, copied from LendersOfficeApp\los\Reports\LoanReports.aspx.cs.
        /// </summary>
        /// <param name="queryId">Identifier for the custom report query.</param>
        /// <param name="brokerId">Identifier for the broker.</param>
        /// <param name="datakey">The datakey to use when writing the report to the temp filedb.</param>
        /// <param name="enforceReportSizeCap">Whether or not we wish to fail if the report would be too large.  Default is false.</param>
        private void RunQueryImpl(Guid queryId, Guid brokerId, string datakey, bool enforceReportSizeCap = false)
        {
            var location = FileStorageIdentifier.TryParse(ConstStage.FileDBSiteCodeForTempFile);
            if (location == null)
            {
                var context = new SimpleContext("Missing or incorrect ConstStage.FileDBSiteCodeForTempFile found in custom report runner; query_id=" + queryId.ToString());
                throw new DeveloperException(ErrorMessage.BadConfiguration, context);
            }

            var key = FileIdentifier.TryParse(datakey);
            if (key == null)
            {
                var context = new SimpleContext("Invalid datakey passed to custom report runner; query_id=" + queryId.ToString());
                throw new DeveloperException(ErrorMessage.SystemError, context);
            }

            Query query = null;
            SqlParameter[] parameters = { new SqlParameter("@QueryId", queryId), new SqlParameter("@BrokerId", brokerId) };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "GetReportQueryContentById", parameters))
            {
                if (reader.Read())
                {
                    query = reader["XmlContent"].ToString();
                }
                else
                {
                    var context = new SimpleContext("Loan reporting: Failed to get report query; Unable to load query_id=" + queryId.ToString());
                    throw new ServerException(ErrorMessage.SystemError, context);
                }
            }

            if (query.Columns.Count > 0)
            {
                var loans = new LoanReporting();
                var principal = BrokerUserPrincipal.CurrentPrincipal;
                var ra = loans.RunReport(query, principal, !principal.HasPermission(Permission.AllowExportingFullSsnViaCustomReports), E_ReportExtentScopeT.Default, enforceReportSizeCap: enforceReportSizeCap);
                if (ra != null)
                {
                    ra.Label.Title = query.Label.Title;
                    ra.Id = Guid.NewGuid();

                    string xml = ra.ToString();
                    this.StoreFile(location.Value, key.Value, xml);
                }
                else
                {
                    var context = new SimpleContext("Unable to run report.  Please check query parameters; query_id=" + queryId.ToString());
                    throw new ServerException(ErrorMessage.SystemError, context);
                }
            }
            else
            {
                var context = new SimpleContext("Unable to run report.  Please specify fields in the report's layout; query_id=" + queryId.ToString());
                throw new ServerException(ErrorMessage.SystemError, context);
            }
        }
    }
}
