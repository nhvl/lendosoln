﻿// <copyright file="ScheduleCustomReport.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>Sending Custom Report on schedule.
// </summary>
namespace LendersOffice.QueryProcessor
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using DataAccess;
    using LendersOffice.Email;
    using LendersOffice.Constants;
    using System.Data;
    using System.Data.SqlClient;
    using System.Data.Common;
    using LendersOffice.Reports;
    using System.Threading;
    using System.Collections;
    using Security;

    /// <summary></summary>
    public static class ScheduleCustomReport
    {
        public class CsvResult
        {
            public string Csv { get; private set; }
            public int RecordCount { get; private set; }

            public CsvResult(string csv, int recordCount)
            {
                this.Csv = csv;
                this.RecordCount = recordCount;
            }
        }

        public enum ReportType
        {
            Daily,
            Weekly,
            MonthlyByDayOfWeek,
            MonthlyByDayOfMonth
        }

        /// <summary>
        /// Holds the configuration information necessary for a given run
        /// </summary>
        private class ReportRunConfig
        {
            public string GetNextRunInfoSproc;
            public string GetReportRunInfoSproc;
            public string UpdateNextRunInfoSproc;
            public Func<IGrouping<int, DataRow>, IEnumerable<DateTime>> UpdateNextRun;

        }

        /// <summary>
        /// Holds an exception and a string.
        /// </summary>
        private class ExceptionHolder
        {
            //I wish I had C# tuples :( 
            //Tuples are in some library, and I don't want to add more references to this project
            public Exception e;
            public string msg;
        }

        //Hopefully we're not still using C# at this point
        //If we are, come get me and I'll fix it :)
        private static readonly DateTime Never = new DateTime(9999, 1, 1);

        //Months are 1-indexed, so we need a blank spot at 0
        private static readonly List<string> Months = new List<string>(new string[] { "Blank", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" });

        private static List<ExceptionHolder> ExceptionBucket = new List<ExceptionHolder>();

        private static StringBuilder s_logBuffer = new StringBuilder();

        private static void ResetLog()
        {
            s_logBuffer = new StringBuilder();
            Log("Schedule Custom Report Started");
        }

        private static void Log(string message)
        {
            s_logBuffer.AppendFormat("[{0}] - {1}{2}", DateTime.Now.ToLongTimeString(), message, Environment.NewLine);
        }

        private static void LogError(string message, Exception exc)
        {
            s_logBuffer.AppendFormat("[{0}] [ERROR] - {1}. Exc={2}{3}", DateTime.Now.ToLongTimeString(), message, exc.Message, Environment.NewLine);
            Tools.LogError(message, exc);
        }

        private static void FinalizeLog()
        {
            Tools.LogInfo("ScheduleCustomReport", s_logBuffer.ToString());
        }

        #region testing stubs
        private static Action<DataSet, DbConnectionInfo, string, IEnumerable<SqlParameter>> DataFiller;
        private static Action<CBaseEmail> SendEmail;
        private static Func<DbConnectionInfo, string, int, IEnumerable<SqlParameter>, int> SprocExecutor;
        private static Func<Guid, Guid, Guid, CsvResult> GetReportCSV;
        private static Action<string, Exception> HandleError;


        //And this is stubbed out because we don't want the durn thing sleeping during tests
        private static Action<int> Sleep;
        #endregion

        #region test hook methods


        /// <summary>
        /// Tests reports, with all major IO operations mocked out. "Expected" errors will be handled by the HandleError function, and the program will set the current date to TestDate.
        /// </summary>
        /// <param name="t">The report type to run</param>
        /// <param name="TestDataFiller">The function calls this to fill its dataset</param>
        /// <param name="TestSendEmail">The function calls this to send out e-mails</param>
        /// <param name="TestSprocExecutor">The function calls this to execute stored procedures</param>
        /// <param name="TestGetReportCSV">The function calls this to get the CSV version of a report</param>
        /// <param name="TestErrorHandler">The function that will handle errors</param>
        /// <param name="TestDate">The date during which you want these tests to happen</param>
        public static void TestReportRun(ReportType t, Action<DataSet, DbConnectionInfo, string, IEnumerable<SqlParameter>> TestDataFiller, Action<CBaseEmail> TestSendEmail, Func<DbConnectionInfo, string, int, IEnumerable<SqlParameter>, int> TestSprocExecutor, Func<Guid, Guid, Guid, CsvResult> TestGetReportCSV, Action<string, Exception> TestErrorHandler, DateTime TestDate)
        {
            //Sleep is always mocked out, we don't need to test that junk!
            Sleep = (a) => { return; };
            DataFiller = TestDataFiller;
            SendEmail = TestSendEmail;
            SprocExecutor = TestSprocExecutor;
            GetReportCSV = TestGetReportCSV;
            HandleError = TestErrorHandler;


            ResetLog();

            //We also don't care about e-mailing people anything yet
            //var emails = new Dictionary<string, StringBuilder>();

            DbConnectionInfo connInfo = DbConnectionInfo.DefaultConnectionInfo;


            RunReports(connInfo, TestDate, t);

            FinalizeLog();
        }
        #endregion

        public static void Execute(string[] args)
        {
            var now = DateTime.Now.TimeOfDay;
            var startTime = new TimeSpan(16, 40, 0);
            var endTime = new TimeSpan(3, 20, 0);

            if (now <= startTime && now >= endTime)
            {
                Tools.LogInfo("Cannot run schedule at " + DateTime.Now + ". Valid Range are [" + startTime + "] - [" + endTime + "]");
                return;
            }

            if (args.Length < 2)
            {
                // dd - 7/8/2016 Will remove this option when the new approach working.
                Execute_ToBeRemove(args);
            }
            else
            {
                string type = args[1];

                ReportType reportType;

                if (Enum.TryParse(type, out reportType) == false)
                {
                    throw new Exception("Invalid ReportType=[" + type + "]");
                }
                else
                {
                    ExecuteReportType(reportType);
                }
            }
        }

        private static void ExecuteReportType(ReportType reportType)
        {
            EmailProcessor emailer = new EmailProcessor();

            DataFiller = DataSetHelper.Fill;
            SendEmail = emailer.Send;
            SprocExecutor = StoredProcedureHelper.ExecuteNonQuery;
            HandleError = LogError;
            Sleep = LendersOffice.Sleeping.PerformanceLoggingSleeper.Instance.Sleep;
            GetReportCSV = RunReportAndGetCSV;

            ResetLog();
            try
            {
                Log("Beginning updates");

                DateTime today = DateTime.Now;
                foreach (DbConnectionInfo connInfo in DbConnectionInfo.ListAll())
                {
                    RunReports(connInfo, today, reportType);
                }
            }
            catch (Exception exc)
            {
                LogError("Failed to run report type", exc);
                throw;
            }
            finally
            {
                FinalizeLog();
            }
        }

        /// <summary>
        /// This class finds out the reports that need to run today,
        /// runs and emails them and then updates the database.
        /// 7/29/2015: Modified by Long Nguyen
        /// https://lqbopm/default.asp?215368
        /// </summary>
        /// <param name="args">Ignored</param>
        public static void Execute_ToBeRemove(string[] args) {
            // 7/8/2016 - dd - This method is fragile because it keep the main program run for couple hours.
            // If there is an interruption then the whole program stop.
            // Will delete this after August 2016 release.

            var now = DateTime.Now.TimeOfDay;
            var startTime = new TimeSpan(16, 40, 0);
            var endTime = new TimeSpan(3, 20, 0);
            Thread thread;
            //Check running time
            while (now <= startTime && now >= endTime)
            {
                Log("Cannot run schedule at " + DateTime.Now);
                LendersOffice.Sleeping.PerformanceLoggingSleeper.Instance.Sleep(30 * 60 * 1000);
                //Start too early. Wait 30 minutes and recheck.
                now = DateTime.Now.TimeOfDay;
            }
            ////If now is in running time
            while (now <= endTime||now>=startTime)
            //while(true)
            {
                //execute schedule for every hour in separate thread.
                thread = new Thread(() => ScheduleExecute(args));
                thread.Start();
                //Next schedule is 1 hour
                LendersOffice.Sleeping.PerformanceLoggingSleeper.Instance.Sleep(1 * 60 * 60 * 1000);
                //LendersOffice.Sleeping.PerformanceLoggingSleeper.Instance.Sleep(10 * 60 * 1000);
                now = DateTime.Now.TimeOfDay;
            }
        }
        private static void ScheduleExecute(string[] args)
        {
            EmailProcessor emailer = new EmailProcessor();

            DataFiller = DataSetHelper.Fill;
            SendEmail = emailer.Send;
            SprocExecutor = StoredProcedureHelper.ExecuteNonQuery;
            HandleError = LogError;
            Sleep = LendersOffice.Sleeping.PerformanceLoggingSleeper.Instance.Sleep;
            GetReportCSV = RunReportAndGetCSV;

            ResetLog();
            try
            {
                Log("Beginning updates");
                DateTime today = DateTime.Now;
                foreach (DbConnectionInfo connInfo in DbConnectionInfo.ListAll())
                {
                    //Use thread to make report send mail out individually.
                    var thread = new Thread(() => RunReports(connInfo, today, ReportType.Daily));
                    thread.Start();

                    thread = new Thread(() => RunReports(connInfo, today, ReportType.Weekly));
                    thread.Start();

                    thread = new Thread(() => RunReports(connInfo, today, ReportType.MonthlyByDayOfWeek));
                    thread.Start();

                    thread = new Thread(() => RunReports(connInfo, today, ReportType.MonthlyByDayOfMonth));
                    thread.Start();
                }
            }
            finally
            {
                //Log("Finished at " + DateTime.Now.ToShortTimeString());

                //Dump whatever's in our log buffer.
                FinalizeLog();
            }

        }
        static void AddToBucket(string msg, Exception e)
        {
            LogError(msg, e);
            ExceptionBucket.Add(new ExceptionHolder() { e = e, msg = msg });
        }
        private static void SendErrorToDev(ExceptionHolder h)
        {
            var devEmail = ConstStage.ExceptionDeveloperSupport;
            if (string.IsNullOrEmpty(devEmail) == false)
            {
                StringBuilder devEmailText = new StringBuilder("The following exceptions occurred while running scheduled reports:" + Environment.NewLine);
                devEmailText.AppendLine(h.msg + ", error is: " + h.e.ToString());

                CBaseEmail toDev = new CBaseEmail(ConstAppDavid.SystemBrokerGuid)
                {
                    Subject = "Error running scheduled reports on server: " +
                              (string.IsNullOrEmpty(Environment.MachineName) ?
                                "Unknown" : Environment.MachineName) +
                              " date: " + DateTime.Now.ToShortDateString(),
                    From = ConstStage.DefaultDoNotReplyAddress,
                    To = devEmail,
                    Message = devEmailText.ToString(),
                    IsHtmlEmail = false
                };

                SendEmail(toDev);
            }
        }
        private static void SendErrorNotification(Guid brokerId, string errorEmail,string scheduleName, List<ExceptionHolder> exceptionBucket)
        {
            Log("Finished at " + DateTime.Now.ToShortTimeString());

            //Dump whatever's in our log buffer.
            //FinalizeLog();
            //Complain about the errors, if there were any
            if (exceptionBucket.Count > 0)
            {
                var devEmail = ConstStage.ExceptionDeveloperSupport;
                var email = errorEmail;
                if (string.IsNullOrEmpty(devEmail) == false)
                {
                    StringBuilder devEmailText = new StringBuilder("The following exceptions occurred while running scheduled report " + scheduleName + Environment.NewLine);
                    foreach (ExceptionHolder h in exceptionBucket)
                    {
                        devEmailText.AppendLine(h.msg + ", error is: " + h.e.ToString());
                    }

                    CBaseEmail toDev = new CBaseEmail(ConstAppDavid.SystemBrokerGuid)
                    {
                        Subject = "Error running scheduled reports on server: " +
                                  (string.IsNullOrEmpty(Environment.MachineName) ?
                                    "Unknown" : Environment.MachineName) +
                                  " date: " + DateTime.Now.ToShortDateString(),
                        From = ConstStage.DefaultDoNotReplyAddress,
                        To = devEmail,
                        Message = devEmailText.ToString(),
                        IsHtmlEmail = false
                    };

                    SendEmail(toDev);
                }

                
                CBaseEmail toUser = new CBaseEmail(brokerId)
                {
                    Subject = "Error running scheduled reports",
                    From = ConstStage.DefaultDoNotReplyAddress,
                    To = Convert.ToString(email),

                    Message = "We encountered problems running the scheduled report, " + scheduleName + ". "
                        + "Attempts to rerun this report have failed and this issue has now been forwarded to our support time for investigation. In the meantime you may manually run this report from the Scheduled Reports editor."
                        + Environment.NewLine + Environment.NewLine
                        + "Thank you," + Environment.NewLine
                        + "LendingQB Support Team" + Environment.NewLine
                        + "https://support.lendingqb.com/",
                    IsHtmlEmail = false
                };

                SendEmail(toUser);
            }
        }
        private static ReportRunConfig GetConfig(ReportType t)
        {
            switch (t)
            {
                case ReportType.Daily:
                    return new ReportRunConfig
                    {
                        GetNextRunInfoSproc = "GetDailyReports",
                        GetReportRunInfoSproc = "GetQueryDetailDaily",
                        UpdateNextRunInfoSproc = "UpdateDailyReport",
                        UpdateNextRun = GetNextRunDateDaily
                    };

                case ReportType.Weekly:
                    return new ReportRunConfig
                    {
                        GetNextRunInfoSproc = "GetWeeklyReports",
                        GetReportRunInfoSproc = "GetQueryDetailWeekly",
                        UpdateNextRunInfoSproc = "UpdateWeeklyReport",
                        UpdateNextRun = GetNextRunDateWeekly
                    };

                case ReportType.MonthlyByDayOfWeek:
                    return new ReportRunConfig
                    {
                        GetNextRunInfoSproc = "GetMonthlyReports1",
                        GetReportRunInfoSproc = "GetQueryDetailMonthly1",
                        UpdateNextRunInfoSproc = "UpdateMonthlyReport",
                        UpdateNextRun = GetNextRunDateMonthly1
                    };

                case ReportType.MonthlyByDayOfMonth:
                    return new ReportRunConfig
                    {
                        GetNextRunInfoSproc = "GetMonthlyReports2",
                        GetReportRunInfoSproc = "GetQueryDetailMonthly2",
                        UpdateNextRunInfoSproc = "UpdateMonthlyReport",
                        UpdateNextRun = GetNextRunDateMonthly2
                    };

                default:
                    throw new UnhandledEnumException(t);
            }
        }
        private static Dictionary<int, int> GetRetryTimes(DataSet NextRunInfo)
        {
            var rs = new Dictionary<int, int>();
            foreach (DataRow row in NextRunInfo.Tables[0].Rows) {
                if (rs.ContainsKey(int.Parse(row["ReportId"].ToString()))) continue;
                rs.Add(int.Parse(row["ReportId"].ToString()), CalculateRetryTimes(DateTime.Parse(row["RunTime"].ToString()).TimeOfDay, DateTime.Parse(row["RetryUntil"].ToString()).TimeOfDay));
            }
            return rs;
        }
        private static Dictionary<int, int> GetZeroRetryTimes(Dictionary<int, int> retry)
        {
            var rs = new Dictionary<int, int>();
            foreach (var key in retry.Keys)
            {
                rs.Add(key, 0);
            }
            return rs;
        }
        private static Dictionary<int, List<ExceptionHolder>> GetEmptyBucket(Dictionary<int, int> retry)
        {
            var rs = new Dictionary<int, List<ExceptionHolder>>();
            foreach (var key in retry.Keys)
            {
                rs.Add(key, new List<ExceptionHolder>());
            }
            return rs;
        }
        private static int CalculateRetryTimes(TimeSpan RunTime, TimeSpan RetryUntil) {
            //Retry every 30 minute
            var diff = RetryUntil.Subtract(RunTime);
            if (diff.Hours < 0) diff = diff.Add(new TimeSpan(1, 0, 0, 0));
            return diff.Hours * 2;
        }
        private static bool isRetry(Dictionary<int, int> retryTime)
        {
            foreach (var k in retryTime.Keys) {
                if (retryTime[k] > 0) return true;
            }
            return false;
        }
        
        private static void decreaseRetry(ref Dictionary<int, int> retryTime) {
            var keys = new List<int>(retryTime.Keys);
            foreach (var k in keys)
            {
                if (retryTime[k] == -1) continue;
                retryTime[k]--;
            }
        }
        private static string GetRetryTimesString(int remaining, int maxTimes)
        {
            var n = maxTimes - remaining;
            switch(n){
                case 1: return "Second";
                case 2: return "Third";
                default: return (n + 1) + "th";
            }
        }
        /// <summary>
        /// Runs reports for a given report type
        /// </summary>
        /// <param name="t">The type of report to run</param>
        private static void RunReports(DbConnectionInfo connInfo, DateTime reportDate, ReportType t)
        {
            try
            {
                Log(Environment.NewLine);
                Log("Running reports of type " + t + ". DbConnectionInfo=[" + connInfo.FriendlyDisplay + "].");

                ReportRunConfig c = GetConfig(t);

                DataSet NextRunInfo = new DataSet();
                DataSet CurrentReportRunInfo = new DataSet();
                //Dictionary<reportId, retryTime>
                Dictionary<int, int> retryTime = new Dictionary<int, int>();
                Dictionary<int, int> retryTimeConstant = new Dictionary<int, int>();
                //Error Message text
                var msg = "";
                //Every report has its own exceptionBucket
                Dictionary<int, List<ExceptionHolder>> exceptionBucket = new Dictionary<int, List<ExceptionHolder>>();
                try
                {
                    DataFiller(NextRunInfo, connInfo, c.GetNextRunInfoSproc, null);
                    DataFiller(CurrentReportRunInfo, connInfo, c.GetReportRunInfoSproc, null);
                    retryTimeConstant = GetRetryTimes(NextRunInfo);
                    retryTime = GetZeroRetryTimes(retryTimeConstant);
                    exceptionBucket = GetEmptyBucket(retryTimeConstant);
                }
                catch (SqlException e)
                {
                    msg = "Custom reports updater encountered an error loading data";
                    HandleError(msg, e);
                    SendErrorToDev(new ExceptionHolder() { e = e, msg = msg });
                    return;
                }

                Dictionary<int, string> ReportIdToScheduleName = (from DataRow row in CurrentReportRunInfo.Tables[0].Rows select row).ToDictionary((row) => int.Parse(row["ReportId"].ToString()), (row) => row["ScheduleName"].ToString());
                Dictionary<int, string> ReportIdToEmail = (from DataRow row in CurrentReportRunInfo.Tables[0].Rows select row).ToDictionary((row) => int.Parse(row["ReportId"].ToString()), (row) => row["Email"].ToString());
                Dictionary<int, Guid> ReportIdToBrokerId = (from DataRow row in CurrentReportRunInfo.Tables[0].Rows select row).ToDictionary((row) => int.Parse(row["ReportId"].ToString()), (row) => new Guid(Convert.ToString(row["BrokerId"])));


                if (NextRunInfo.Tables[0].Rows.Count == 0)
                {
                    //Nothing to do
                    Log("No reports of this type to run.");
                    return;
                }
                //Gather up the data we'll need
                var NextRunRowsById = from row in NextRunInfo.Tables[0].AsEnumerable()
                                      group row by int.Parse(row["ReportId"].ToString());
                var ThisReportRunRowsById = from row in CurrentReportRunInfo.Tables[0].AsEnumerable()
                                            group row by int.Parse(row["ReportId"].ToString());
                Log("");
                Log("Got these ids to calculate next runs: " + GetIdList(NextRunRowsById));
                Log("Got these ids to run reports with:    " + GetIdList(ThisReportRunRowsById));
                Log("");

                Log("Calculating next runs for reports:");
                //Then update our next run dates...
                var failedNextRunUpdates = UpdateNextRun(connInfo, reportDate, NextRunRowsById, c.UpdateNextRun, c.UpdateNextRunInfoSproc);

                //And run our reports
                Log("Running and e-mailing reports:");
                var failedReportEmails = RunAndEmail(reportDate, ThisReportRunRowsById);

                //Initialize retryTime
                foreach (var key in failedNextRunUpdates.Keys)
                {
                    retryTime[key] = retryTimeConstant[key];
                }
                foreach (var key in failedReportEmails.Keys)
                {
                    retryTime[key] = retryTimeConstant[key];
                }
                //Check retryTime to retry report
                while (isRetry(retryTime))
                {
                    decreaseRetry(ref retryTime);
                    //Everything went well, exit out.
                    if (!failedNextRunUpdates.Any() && !failedReportEmails.Any())
                    {
                        Log("No errors for this run.");
                        return;
                    }

                    Log("");
                    Log("The following errors occurred while updating and running reports: ");
                    Log("");

                    if (failedNextRunUpdates.Keys.Count > 0)
                    {
                        Log("The following reports need to have their next run updates retried: ");
                        foreach (var k in failedNextRunUpdates.Keys)
                        {
                            Log("* " + k);
                        }
                    }
                    else
                    {
                        Log("All next run updates were successful.");
                    }
                    Log("");

                    if (failedReportEmails.Keys.Count > 0)
                    {
                        Log("The following reports need to be re-run and e-mailed");
                        foreach (var k in failedReportEmails.Keys)
                        {
                            Log("* " + k);
                        }
                    }
                    else
                    {
                        Log("All reports were successfully run and e-mailed");
                    }
                    Log("");

                    //If there were errors, sleep for 30 minutes and try again.
                    Sleep(30 * 60 * 1000);
                    //Sleep(1 * 60 * 1000);

                    Log("Doing retries...");

                    var retrySuccess = new HashSet<int>();

                    foreach (int key in failedNextRunUpdates.Keys)
                    {
                        //Create error message text first
                        msg = GetRetryTimesString(retryTime[key], retryTimeConstant[key]) + " attempt at next run failed at " + DateTime.Now + " for report id: " + key;
                        try
                        {
                            if (retryTime[key] == -1) continue;
                            failedNextRunUpdates[key]();
                            retrySuccess.Add(key);
                        }
                        catch (DbException e)
                        {
                            //This time, we have to whine about it
                            HandleError(msg, e);
                            exceptionBucket[key].Add(new ExceptionHolder() { e = e, msg = msg });
                            //last retry
                            if (retryTime[key] == 0) SendErrorNotification(ReportIdToBrokerId[key], ReportIdToEmail[key], ReportIdToScheduleName[key], exceptionBucket[key]);
                        }
                        catch (CBaseException e)
                        {
                            //This time, we have to whine about it
                            HandleError(msg, e);
                            exceptionBucket[key].Add(new ExceptionHolder() { e = e, msg = msg });
                            //last retry
                            if (retryTime[key] == 0) SendErrorNotification(ReportIdToBrokerId[key], ReportIdToEmail[key], ReportIdToScheduleName[key], exceptionBucket[key]);
                        }
                        catch (NoNullAllowedException e)
                        {
                            //This time, we have to whine about it
                            HandleError(msg, e);
                            exceptionBucket[key].Add(new ExceptionHolder() { e = e, msg = msg });
                            //last retry
                            if (retryTime[key] == 0) SendErrorNotification(ReportIdToBrokerId[key], ReportIdToEmail[key], ReportIdToScheduleName[key], exceptionBucket[key]);
                        }
                        catch (Exception e)
                        {
                            HandleError(msg, e);
                            exceptionBucket[key].Add(new ExceptionHolder() { e = e, msg = msg });
                            //last retry
                            if (retryTime[key] == 0) SendErrorNotification(ReportIdToBrokerId[key], ReportIdToEmail[key], ReportIdToScheduleName[key], exceptionBucket[key]);
                            //throw;
                        }
                    }
                    //Remove successful report ids in failedNextRunUpdates
                    foreach (var key in retrySuccess)
                    {
                        failedNextRunUpdates.Remove(key);
                    }
                    retrySuccess = new HashSet<int>();
                    foreach (int key in failedReportEmails.Keys)
                    {
                        msg = GetRetryTimesString(retryTime[key], retryTimeConstant[key]) + " attempt report run and e-mail failed at " + DateTime.Now + " for report id: " + key;
                        try
                        {
                            if (retryTime[key] == -1) continue;
                            //if (key!=0) throw new Exception("fail again");//Toggle this when release
                            failedReportEmails[key]();
                            retrySuccess.Add(key);
                        }
                        catch (DbException e)
                        {
                            HandleError(msg, e);
                            exceptionBucket[key].Add(new ExceptionHolder() { e = e, msg = msg });
                            //last retry
                            if (retryTime[key] == 0) SendErrorNotification(ReportIdToBrokerId[key], ReportIdToEmail[key], ReportIdToScheduleName[key], exceptionBucket[key]);
                        }
                        catch (CBaseException e)
                        {
                            HandleError(msg, e);
                            exceptionBucket[key].Add(new ExceptionHolder() { e = e, msg = msg });
                            //last retry
                            if (retryTime[key] == 0) SendErrorNotification(ReportIdToBrokerId[key], ReportIdToEmail[key], ReportIdToScheduleName[key], exceptionBucket[key]);
                        }
                        catch (NoNullAllowedException e)
                        {
                            HandleError(msg, e);
                            exceptionBucket[key].Add(new ExceptionHolder() { e = e, msg = msg });
                            //last retry
                            if (retryTime[key] == 0) SendErrorNotification(ReportIdToBrokerId[key], ReportIdToEmail[key], ReportIdToScheduleName[key], exceptionBucket[key]);
                        }
                        catch (Exception e)
                        {
                            HandleError(msg, e);
                            exceptionBucket[key].Add(new ExceptionHolder() { e = e, msg = msg });
                            //last retry
                            if (retryTime[key] == 0) SendErrorNotification(ReportIdToBrokerId[key], ReportIdToEmail[key], ReportIdToScheduleName[key], exceptionBucket[key]);
                            //throw;
                        }
                    }
                    //Remove successful report ids in failedReportEmails
                    foreach (var key in retrySuccess)
                    {
                        failedReportEmails.Remove(key);
                    }
                }
                FinalizeLog();
            }
            catch (Exception e)
            {
                var msg = "Something completely unexpected went wrong in the custom reports updater! Current report type is: " + t.ToString();
                HandleError(msg, e);
                SendErrorToDev(new ExceptionHolder() { e = e, msg = msg });
                //throw;
            }
        }

        private static string GetIdList(IEnumerable<IGrouping<int, DataRow>> data)
        {
            StringBuilder sb = new StringBuilder();
            foreach (var g in data)
            {
                sb.Append(g.Key + ", ");
            }

            // Remove the final ", "
            if (sb.Length >= 2)
            {
                sb.Length -= 2;
            }

            return sb.ToString();
        }

        /// <summary>
        /// Calculates the next run of a list of reports
        /// </summary>
        /// <param name="rowsById">Database report configuration rows, grouped by Id</param>
        /// <param name="getNextRun">The function that calculates the next run for this particular report</param>
        /// <param name="updateSproc">The stored procedure that updates the database with the next run information</param>
        private static Dictionary<int, Action> UpdateNextRun(DbConnectionInfo connInfo, DateTime reportDate, IEnumerable<IGrouping<int, DataRow>> rowsById, Func<IGrouping<int, DataRow>, IEnumerable<DateTime>> potentialNextRuns, string updateSproc)
        {
            var FailedSavesToDb = new Dictionary<int, Action>();
            foreach (var entry in rowsById)
            {
                DataRow first = entry.First();
                DateTime end_date = Convert.ToDateTime(first["EndDate"].ToString());
                DateTime last_run = reportDate;
                DateTime next_run;
                try
                {
                    next_run = (from a in potentialNextRuns(entry) where a > reportDate select a).FirstOrDefault();
                }
                catch (ArgumentException e)
                {
                    //This is almost purely deterministic, so there's no point in re-trying it.
                    //Let devs know if there was an error calculating the next run date
                    HandleError("Could not calculate a date for report id: " + entry.Key, e);
                    continue;
                }

                if (next_run > end_date)
                {
                    next_run = Never;
                }

                SqlParameter[] parameters = {
                                                new SqlParameter("@NextRun", next_run),
                                                new SqlParameter("@LastRun", last_run),
                                                new SqlParameter("@ReportId", entry.Key)
                                            };
                try
                {
                    var rows = SprocExecutor(connInfo, updateSproc, 3, parameters);

                    Log(entry.Key + ", next run is " + next_run + ", rows affected: " + rows);
                }
                catch (DbException exc)
                {
                    //save it to re-run later - note that this is a lambda that calls the function,
                    //not a call to the function itself.
                    FailedSavesToDb.Add(entry.Key, () => SprocExecutor(connInfo, updateSproc, 3, parameters));
                    LogError("Report id " + entry.Key + " failed to save its next run date of " + next_run, exc);
                }
            }
            return FailedSavesToDb;
        }


        /// <summary>
        /// Runs a report as a given user. This is really only its own function for testing purposes, 
        /// since we don't want the test to have to actually run real reports
        /// </summary>
        /// <param name="queryId">Id of the report query to execute</param>
        /// <param name="userId">Id of the user that the report query is executed as</param>
        /// <returns>The result of the report query as a CSV file</returns>
        private static CsvResult RunReportAndGetCSV(Guid brokerId, Guid queryId, Guid userId)
        {
            //If the report runner fails, it will return a null report instead of throwing an exception.
            LoanReporting loanReporting = new LoanReporting();

            var principal = PrincipalFactory.RetrievePrincipalForUser(brokerId, userId, "?");
            Report ra = loanReporting.RunReport(brokerId, queryId, principal, E_ReportExtentScopeT.Default);
            if (ra != null)
            {
                int count = ra.Flatten().Count;
                return new CsvResult(ra.ConvertToCsv(principal), count);
            }

            throw new NoNullAllowedException("Tried to run report query id = " + queryId + ", but the result was null");
        }

        /// <summary>
        /// Runs reports and e-mails their results.
        /// </summary>
        /// <param name="rowsById">A list of database rows representing a report run, grouped by id</param>
        private static Dictionary<int, Action> RunAndEmail(DateTime reportDate, IEnumerable<IGrouping<int, DataRow>> rowsById)
        {
            var FailedReports = new Dictionary<int, Action>();
            foreach (var rows in rowsById)
            {
                if (rows.Count() == 0)
                {
                    Log(rows.Key + ": There were no rows for this report, skipping it.");
                    continue;
                }

                DataRow row = rows.First();

                bool isActive = bool.Parse(row["IsActive"].ToString());
                // 7/14/2010 dd - OPM 53515 - Only allow to run schedule report of active user.
                if (!isActive)
                {
                    Log(rows.Key + ": " + row["ScheduleName"] + " belongs to an inactive user, skipping it");
                    //User isn't active, continue silently
                    continue;
                }

                Guid queryId = new Guid(Convert.ToString(row["QueryId"]));
                Guid userId = new Guid(Convert.ToString(row["UserId"]));
                string scheduleName = Convert.ToString(row["ScheduleName"]);
                Guid brokerId = new Guid(Convert.ToString(row["BrokerId"]));

                CBaseEmail cbe = new CBaseEmail(brokerId)
                {
                    Subject = string.Format("SCHEDULED REPORT: {0} ({1})", scheduleName, reportDate.ToString("M/yyyy")),//EmailSubject(Convert.ToString(row["ScheduleName"])),
                    From = ConstStage.DefaultDoNotReplyAddress,
                    Bcc = Convert.ToString(row["CcIds"]),
                    To = Convert.ToString(row["Email"]),
                    Message = string.Format("You are receiving this email because you are listed as a recipient of the following scheduled report: {0}", scheduleName), //EMAIL_BODY +  Convert.ToString(row["ScheduleName"]),
                    IsHtmlEmail = true
                };

                try
                {
                    //if (scheduleName.Contains("fail")) throw new Exception("Fail report");//Toggle this when release
                    MailReport(brokerId, cbe, queryId, userId);
                    Log(rows.Key + ": " + scheduleName + " run and emailed");
                }
                catch (ArgumentException e)
                {
                    if (e.Message.Contains("User not found."))
                    {
                        Log(rows.Key + ": " + scheduleName + " belongs to a user that does not exist (id: " + e.ParamName + "), skipping it");
                        continue;

                    }

                    FailedReports.Add(rows.Key, () => MailReport(brokerId, cbe, queryId, userId));
                    LogError("Schedule Report Id:" + rows.Key + ", Name=[" + scheduleName + "]", e);
                    continue;
                }
                catch (DbException e)
                {
                    FailedReports.Add(rows.Key, () => MailReport(brokerId, cbe, queryId, userId));
                    LogError("Schedule Report Id:" + rows.Key + ", Name=[" + scheduleName + "]", e);
                    continue;
                }
                catch (NotFiniteNumberException e)
                {
                    FailedReports.Add(rows.Key, () => MailReport(brokerId, cbe, queryId, userId));
                    LogError("Schedule Report Id:" + rows.Key + ", Name=[" + scheduleName + "]", e);
                    continue;
                }
                catch (CBaseException e)
                {
                    // 3/14/2012 dd - Stem from OPM 80312 - We need the executable to continue
                    // to process the next custom report even if one of them fail.
                    FailedReports.Add(rows.Key, () => MailReport(brokerId, cbe, queryId, userId));
                    LogError("Schedule Report Id:" + rows.Key + ", Name=[" + scheduleName + "]", e);
                    continue;

                }
                catch (NoNullAllowedException e)
                {
                    FailedReports.Add(rows.Key, () => MailReport(brokerId, cbe, queryId, userId));
                    LogError("Schedule Report Id:" + rows.Key + ", Name=[" + scheduleName + "]", e);
                    continue;
                }
                catch (Exception e)
                {
                    FailedReports.Add(rows.Key, () => MailReport(brokerId, cbe, queryId, userId));
                    LogError("Schedule Report Id:" + rows.Key + ", Name=[" + scheduleName + "]", e);
                    continue;
                    //throw;

                }
            }

            return FailedReports;
        }

        private static void MailReport(Guid brokerId, CBaseEmail email, Guid queryId, Guid userId)
        {
            var result = GetReportCSV(brokerId, queryId, userId);
            email.Subject += " - " + result.RecordCount + " Record" + (result.RecordCount != 1 ? "s" : "");
            email.AddAttachment("Report.csv", result.Csv);
            SendEmail(email);
        }

        /// <summary>
        /// Calculates the next run date for a report that is being run every N days, optionally skipping Saturday and Sunday
        /// </summary>
        /// <param name="rows">The configuration rows related to this report run</param>
        /// <returns>An infinite series of potential next run dates</returns>
        static IEnumerable<DateTime> GetNextRunDateDaily(IEnumerable<DataRow> rows)
        {
            //In this case there should only ever be one entry
            DataRow row = rows.First();

            DateTime last_run = Convert.ToDateTime(row["NextRun"].ToString());
            DateTime effectiveDate = Convert.ToDateTime(row["EffectiveDate"].ToString());
            int freq = Convert.ToInt32(row["Frequency"].ToString());
            bool sat_sun = Convert.ToBoolean(row["IncludeSatSun"].ToString());

            DateTime next_run;
            next_run = last_run > effectiveDate ? last_run : effectiveDate;
            //Does this count Saturdays and Sundays?
            while (true)
            {
                var dayOffset = freq;
                if (!sat_sun)
                {
                    //No, so we have to be tricky about it.
                    while (dayOffset > 0)
                    {
                        int offset;
                        switch (next_run.DayOfWeek)
                        {
                            //If it's Friday, skip to the next Monday
                            case DayOfWeek.Friday:
                                offset = 3;
                                break;
                            //If it's Saturday, skip to Monday too 
                            //(note that this only happens if we began on a Sat)
                            case DayOfWeek.Saturday:
                                offset = 2;
                                break;
                            //Otherwise, just go to the next day
                            //(This also covers beginning on Sunday)
                            default:
                                offset = 1;
                                break;
                        }

                        next_run = next_run.AddDays(offset);
                        dayOffset--;
                    }
                }
                else
                {
                    //Yes, so it's easy.
                    next_run = next_run.AddDays(dayOffset);
                }

                yield return next_run;
            }
        }

        /// <summary>
        /// Calculates the next run date for a weekly report that is being run every N weeks on a list of specific weekdays
        /// </summary>
        /// <param name="rows">The list of configuration rows for this report's next run</param>
        /// <returns></returns>
        static IEnumerable<DateTime> GetNextRunDateWeekly(IEnumerable<DataRow> rows)
        {
            /*
             * Okay, this is kinda confusing - 
             * I've always thought that Sunday is the last day of the week, but according to
             * the DayOfWeek enum (and, you know, everything else) it's actually the first.
             * Saturday is the last. Just keep that in mind.
             */

            //We only want to pull run information from the first row
            DataRow first = rows.First();

            DateTime end_date = Convert.ToDateTime(first["EndDate"].ToString());
            DateTime last_run = Convert.ToDateTime(first["NextRun"].ToString());
            DateTime effective_date = Convert.ToDateTime(first["EffectiveDate"].ToString());

            //Gather up the weekdays on which this runs
            var weekdays = (from row in rows
                            select (DayOfWeek)Enum.Parse(
                                typeof(DayOfWeek),
                                row["Weekday"].ToString())).ToList();

            int freq = Convert.ToInt32(first["Frequency"].ToString());



            System.DateTime next_run = last_run;
            // If this is the first time that the report will run,
            // We start checking from the effective date, 
            // else we start checking from one day past last run.
            if (effective_date.Date > last_run.Date)
            {
                next_run = effective_date;
            }

            while (true)
            {
                //We need to increment the day by one step, but that's tricky if we started on Saturday;
                //Saturdays skip ahead a full week, since they're the last day of the week
                if (next_run.DayOfWeek == DayOfWeek.Saturday)
                {
                    next_run = next_run.AddDays(7 * freq);
                    //  Saturdays are a special case.  If this is a weekly Saturday report, the days already added should be enough.  Due to Saturday being the end of the week,
                    //  continuing the bottom code will throw off the date by 1 week.
                    if (next_run.DayOfWeek == weekdays.Min())
                    {
                        yield return next_run;
                    }
                }


                next_run = next_run.AddDays(1);

                //Try to skip ahead to the next valid day.
                var nextValidDays = from DayOfWeek d in weekdays where d >= next_run.DayOfWeek select d;
                DayOfWeek nextDay;

                if (nextValidDays.Count() > 0)
                {
                    nextDay = nextValidDays.Min();
                }
                else
                {
                    //No more days left in this week, skip ahead to the next Sunday
                    //(since Sunday is zero in the enum, we subtract from Saturday and add 1)
                    next_run = next_run.AddDays((DayOfWeek.Saturday - next_run.DayOfWeek) + 1);
                    //(it's freq-1 because we just manually added a week)
                    next_run = next_run.AddDays(7 * (freq - 1));

                    //Now we're on the first day of the next week, so just take the first day we can
                    nextDay = weekdays.Min();
                }

                //These enums get cast to their int representations, which means that I can subtract 
                //them like this and get a daywise distance between now and our target date
                next_run = next_run.AddDays(nextDay - next_run.DayOfWeek);
                yield return next_run;
            }
        }

        /// <summary>
        /// Gets the next run date for a monthly report which is being run on the Nth [weekday] of certain months
        /// </summary>
        /// <param name="rows">The DataRows representing this run configuration</param>
        /// <returns>An infinite series of potential next run date</returns>
        static IEnumerable<DateTime> GetNextRunDateMonthly1(IEnumerable<DataRow> rows)
        {
            DataRow first = rows.First();
            DateTime start = Convert.ToDateTime(first["NextRun"].ToString());
            DateTime effective_date = Convert.ToDateTime(first["EffectiveDate"].ToString());

            List<int> allValidMonths = (from row in rows select Months.IndexOf(row["Month"].ToString())).ToList();
            DayOfWeek weekday = (DayOfWeek)Enum.Parse(typeof(DayOfWeek), first["Weekday"].ToString());
            int weekno = Convert.ToInt32(first["DateOfTheMonthOrWeekNo"].ToString());

            //Option 1 will only ever run once per month, 
            //so we start on the first day of the next month after the last run
            start = start.AddMonths(1);
            if (effective_date.Date >= start.Date)
            {
                //Or the first day of the effective date month 
                //(if I start checking from the effective date things get messy)
                start = effective_date;
            }

            start = new DateTime(start.Year, start.Month, 1);

            //Weekno comes in as: 1 for 1st, 2 for 2nd, 3 for 3rd, 4 for 4th, and 5 for last
            //Now, all months have a fourth repetition of a weekday, but not all have a fifth
            //This means there's some overlap between 4th and last, and we'll have to check.            

            //Figure out when the next valid month is
            var nextValidMonths = from m in allValidMonths where m >= start.Month select m;
            int nextMonth;

            if (nextValidMonths.Count() > 0)
            {
                //There's one this year, so go there
                nextMonth = nextValidMonths.Min();
            }
            else
            {
                //Otherwise the next valid month is next year
                nextMonth = allValidMonths.Min();
                start = start.AddYears(1);
            }

            while (true)
            {
                start = new DateTime(start.Year, nextMonth, 1);

                //Keep track of the current month to know when we've rolled over
                int currMonth = start.Month;

                //Find the first instance of our desired weekday
                for (int j = 0; j < 7; j++)
                {
                    if (start.DayOfWeek == weekday)
                    {
                        break;
                    }

                    start = start.AddDays(1);
                }

                //Offset it by weekno - by subtracting 1 from it,
                //0 == 1st etc and we can just do this directly.
                start = start.AddDays((weekno - 1) * 7);
                if (currMonth != start.Month)
                {
                    //Whoops, we went too far 
                    //(only happens when we're asked to calculate the last DoW in a month
                    //that's too short - e.g, the last Friday in February would come up
                    //as the first Friday in March)

                    //So roll it back a week
                    start = start.AddDays(-7);
                }

                yield return start;
            }
        }

        /// <summary>
        /// Calculates the next run date for a monthly report that is being run on the Nth day of certain months
        /// </summary>
        /// <param name="rows">The DataRows representing this run configuration</param>
        /// <returns>The next run date</returns>
        static IEnumerable<DateTime> GetNextRunDateMonthly2(IEnumerable<DataRow> rows)
        {
            DataRow first = rows.First();
            DateTime last_run = Convert.ToDateTime(first["NextRun"].ToString());
            DateTime effective_date = Convert.ToDateTime(first["EffectiveDate"].ToString());

            //This shows up in the db as a cross join of months <=> valid days :(
            //Despite that, there's no actual linkage between months and days in the UI, 
            //so we just care about the distinct months and distinct days                                             
            List<int> validMonths = (from row in rows select row["Month"].ToString())
                                    .Distinct()
                                    .Select(p => Months.IndexOf(p))
                                    .ToList();
            List<int> validDays = (from row in rows select row["DateOfTheMonthOrWeekNo"].ToString())
                                    .Distinct()
                                    .Select(p => int.Parse(p))
                                    .ToList();

            validDays.Sort();

            //Start either a day after the last run, or the effective date if that's later than the last run
            DateTime start = last_run.Date;
            if (effective_date > last_run)
            {
                start = effective_date;
            }

            while (true)
            {
                start = start.AddDays(1);

                //We want to go through 13 months at most, since we might have a yearly wrap-around 
                //(12 months will only examine a given month once, we want the initial month examined twice)
                for (int i = 0; i < 13; i++)
                {
                    if (!validMonths.Contains(start.Month))
                    {
                        //If it's not a valid month, go to the next one.
                        //Use this constructor to make sure we're always 
                        //on the first day of the next month
                        start = start.AddMonths(1);
                        start = new DateTime(start.Year, start.Month, 1);
                        continue;
                    }

                    //If this is a month we want, skip along to the next valid day
                    foreach (var nextValidDay in from int d in validDays where d >= start.Day select d)
                    {
                        //Keep track of the current month so we know if we've rolled over
                        int currMonth = start.Month;
                        start = start.AddDays(nextValidDay - start.Day);

                        //Special cases whoo hoo
                        //All months are guaranteed to have days 1 - 28, if the next valid day was outside that range
                        //we have to check on it.
                        if (nextValidDay > 28)
                        {
                            if (start.Month != currMonth)
                            {
                                //We've rolled over to the next month, so this previous month didn't contain any valid days
                                //(yes, if we're supposed to run on the 31st of every month, we'll only run on the 31st of months
                                //with 31 days; all other months will be skipped).

                                //Go back to the first day of the next month and try again
                                start = new DateTime(start.Year, start.Month, 1);
                                break;
                            }
                        }

                        //If we got here, we've got a valid day
                        yield return start;
                    }

                    //No more days we're okay with in this month, abandon it
                    start = new DateTime(start.Year, start.Month, 1);
                    start = start.AddMonths(1);
                }

                //On the other hand, if we got here something is wrong.
                throw new ArgumentException("Monthly2 could not calculate next run date");
            }
        }
    }

}
