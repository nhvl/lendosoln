﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LendersOffice.QueryProcessor
{
    public class QueryUtils
    {
        /// <summary>
        /// looks in the query's filters for something matching the parameters, and tries to check it / uncheck it.
        /// returns true if it (potentially) changed the query.
        /// 
        /// todo: this was blatantly stolen from FilterClassUserControl.ascx.cs.  I should refactor the code so that calls this.
        /// </summary>
        public static bool SetClassOption(Query query, bool isToBeChecked, string filterID, string argumentValue)
        {
            // Find the matching condition in our filters and update
            // the tree according to the checked status specified.
            //FilterClassStringPair pair = oArgument as FilterClassStringPair;
            bool isDirty = false;

            if (query == null || argumentValue == null || filterID == null)
            {
                return isDirty;
            }

            // Find the corresponding condition in the filter and
            // toggle it.  If it is not found or is not matching,
            // then we remove the condition and add a must match
            // condition in its place.

            Condition matching = null;

            foreach (object item in query.Filters.Walk)
            {
                Condition condition = item as Condition;

                if (condition == null)
                {
                    continue;
                }

                if (condition.Id == filterID && condition.Argument.Type == E_ArgumentType.Const && condition.Argument.Value == argumentValue)
                {
                    matching = condition;

                    break;
                }
            }

            if (matching != null)
            {
                // Check if this is a non-op.  If so,
                // don't continue to mark the tree
                // as dirty.

                if (matching.Type == E_ConditionType.Ne && isToBeChecked == false)
                {
                    return isDirty;
                }
                else
                    if (matching.Type == E_ConditionType.Eq && isToBeChecked == true)
                    {
                        return isDirty;
                    }
            }

            if (matching != null)
            {
                // Remove this instance completely.

                foreach (object item in query.Filters.Walk)
                {
                    Conditions conditions = item as Conditions;

                    if (conditions == null)
                    {
                        continue;
                    }

                    conditions.Nix(matching);
                }

                query.Filters.Nix(matching);
            }

            // Now, we add a must match condition if the class
            // option is to be checked, or we add a must not
            // match condition if the option is to be turned
            // off and unchecked.  Either way, we make sure
            // we have the supporting condition group.

            if (isToBeChecked == false)
            {
                // Add a new must not match condition to the root
                // all match conditions group.  If the root is not
                // all must match, we drop in a new conditions.
                //
                // 3/25/2005 kb - We previously used the root.  Now,
                // we need to make our own and-group to house the
                // classes.

                Conditions conditions = null;

                foreach (Object item in query.Filters)
                {
                    Conditions current = item as Conditions;

                    if (current == null)
                    {
                        continue;
                    }

                    if (current.Type == E_ConditionsType.An)
                    {
                        conditions = current;

                        break;
                    }
                }

                if (conditions == null)
                {
                    // And-group not found, so add one.

                    query.Filters.Add(conditions = new Conditions(E_ConditionsType.An));
                }

                // Add the must match condition.

                Condition condition = new Condition();

                condition.Type = E_ConditionType.Ne;
                condition.Id = filterID;

                condition.Argument.Type = E_ArgumentType.Const;
                condition.Argument.Value = argumentValue;

                conditions.Add(condition);
            }
            else
            {
                // Add a new must match condition to the any match
                // conditions group just under the filter root.
                //
                // 3/25/2005 kb - We previously used the root.  Now,
                // we need to make our own and-group to house the
                // classes.  Match classes must reside in an or-child
                // of a toplevel and-group.

                Conditions conditions = null;
                Conditions anymatches = null;

                foreach (Object item in query.Filters)
                {
                    Conditions current = item as Conditions;

                    if (current == null)
                    {
                        continue;
                    }

                    if (current.Type == E_ConditionsType.An)
                    {
                        conditions = current;

                        break;
                    }
                }

                if (conditions == null)
                {
                    // And-group not found, so add one.

                    query.Filters.Add(conditions = new Conditions(E_ConditionsType.An));
                }

                foreach (Object item in conditions)
                {
                    Conditions current = item as Conditions;

                    if (current == null)
                    {
                        continue;
                    }

                    if (current.Type == E_ConditionsType.Or)
                    {
                        anymatches = current;

                        break;
                    }
                }

                if (anymatches == null)
                {
                    // All-group not found, so add one.

                    conditions.Add(anymatches = new Conditions(E_ConditionsType.Or));
                }

                // Add the must match condition.

                Condition condition = new Condition();

                condition.Type = E_ConditionType.Eq;
                condition.Id = filterID;

                condition.Argument.Type = E_ArgumentType.Const;
                condition.Argument.Value = argumentValue;

                anymatches.Add(condition);
            }

            // We're dirty.  We should check if anything
            // actually changed -- oh well.

            isDirty = true;
            return isDirty;
        }
    }
}
