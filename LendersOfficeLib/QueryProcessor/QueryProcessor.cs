///
/// Author: Kirk Bulis
/// Author: David Dao (Took over)
/// 
namespace LendersOffice.QueryProcessor
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Collections;
    using System.Collections.Specialized;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Xml;
    using System.Xml.Linq;
    using System.Xml.Serialization;
    using CommonProjectLib.Common.Lib;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.ConfigSystem;
    using LendersOffice.ConfigSystem.Operations;
    using LendersOffice.Constants;
    using LendersOffice.Drivers.Gateways;
    using LendersOffice.Reports;
    using LendersOffice.Security;
    using LqbGrammar.DataTypes;
    using Drivers.SqlServerDB;
    using global::ConfigSystem.Engine;

    /// <summary>
    /// We provide a simple event handler delegate for dirty bit
    /// maintenance; we propogate update events to the container.
    /// </summary>
    public delegate void QueryUpdateEventHandler(object sender, string e);

    /// <summary>
    /// Composite objects (ones that contain leaf nodes, as well
    /// as instances of the same type as self) need a simple way
    /// to be traversed in order.  We provide an interface for
    /// walking trees using a push-down stack.
    /// </summary>

    public abstract class Composite
    {
        /// <summary>
        /// Provide interface stub to be implemented by
        /// specialized composite group implementation.
        /// </summary>
        /// <returns>
        /// Enumerator for traversal.
        /// </returns>

        public abstract IEnumerator GetEnumerator();

        /// <summary>
        /// We need a composite enumerator that presents a
        /// flattened interface for a tree.
        /// </summary>

        private class Walker : IEnumerator
        {
            /// <summary>
            /// Tree status
            /// 
            /// As we hit nested composite nodes, we push them down and
            /// proceed through the nesting, until we finish the current
            /// and pop it up.
            /// </summary>

            private Stack m_Stack; // enumeration history
            private Composite m_Root; // enumerable object
            private bool m_Move; // just move flag

            public object Current
            {
                // Access member.

                get
                {
                    // Pull the current off the top of the
                    // stack.  If empty, return null.

                    if (m_Stack.Count > 0)
                    {
                        IEnumerator iter = m_Stack.Peek() as IEnumerator;

                        return iter.Current;
                    }

                    return null;
                }
            }

            /// <summary>
            /// Advance to the next entry, no matter where we are in
            /// the composite tree.
            /// </summary>
            /// <returns>
            /// Current flag -- true means current is valid.
            /// </returns>

            public bool MoveNext()
            {
                // Pull the current off the top of the
                // stack.  If we find ourselves looking
                // at a composite, then we descend in.

                while (m_Stack.Count > 0)
                {
                    // Get the top of the stack and try to advance.

                    IEnumerator enumerator = m_Stack.Peek() as IEnumerator;

                    if (enumerator != null)
                    {
                        // Check if we descend once we've moved.

                        if (m_Move == false)
                        {
                            // Get the current object and see if the
                            // current is a composite to descend in.

                            Composite current = enumerator.Current as Composite;

                            if (current != null)
                            {
                                // Descend into the nested entries.

                                m_Stack.Push(enumerator = current.GetEnumerator());
                            }
                        }

                        // We advance the current stack entry so
                        // we don't get stuck with the same group.

                        if (enumerator.MoveNext() == true)
                        {
                            m_Move = false;

                            return true;
                        }

                        m_Move = true;
                    }

                    m_Stack.Pop();
                }

                return false;
            }

            /// <summary>
            /// Remove all history and restart the enumerator.
            /// </summary>

            public void Reset()
            {
                // Reset the enumerator to its initial state.

                m_Stack.Clear();

                m_Stack.Push(m_Root.GetEnumerator());
            }

            /// <summary>
            /// Construct walker for this composite group.
            /// </summary>
            /// <param name="cRoot">
            /// Enumerable specialization to walk.
            /// </param>

            public Walker(Composite cRoot)
            {
                // Initialize members.

                m_Stack = new Stack();

                if (cRoot != null)
                {
                    m_Stack.Push(cRoot.GetEnumerator());
                }

                m_Root = cRoot;

                m_Move = true;
            }

        }

        /// <summary>
        /// Encapsulation of walker generator.
        /// </summary>

        private class Walkable : IEnumerable
        {
            /// <summary>
            /// Cached root
            /// 
            /// We cache the root object so we can generate
            /// multiple enumerators.
            /// </summary>

            Composite m_Root; // enumerable object

            /// <summary>
            /// Expose walking enumerator.
            /// </summary>
            /// <returns>
            /// Enumeration interface.
            /// </returns>

            public IEnumerator GetEnumerator()
            {
                // Allocate new composite walker.

                return new Walker(m_Root);
            }

            /// <summary>
            /// Construct walker generator.
            /// </summary>
            /// <param name="cRoot">
            /// Enumerable root.
            /// </param>

            public Walkable(Composite cRoot)
            {
                // Initialize members.

                m_Root = cRoot;
            }

        }

        /// <summary>
        /// Provide flat enumeration interface for walking
        /// all nested nodes in order in a single loop.
        /// </summary>

        [XmlIgnore]
        public IEnumerable Walk
        {
            get
            {
                // Return stack-based tree walker.

                return new Walkable(this);
            }
        }

    }

    /// <summary>
    /// We provide a list of pairs in the order they were
    /// added, as well as quick lookup dictionaries.
    /// See <see cref="T:LendersOffice.QueryProcessor.Field"/>
    /// for more information.
    /// </summary>

    public class Mapping : IEnumerable
    {
        /// <summary>
        /// Mapping set
        /// 
        /// We provide a list of pairs in the order they were
        /// added, as well as quick lookup tables.
        /// </summary>

        private Lookup m_ByKey; // by key dictionary
        private Lookup m_ByVal; // by val dictionary
        private ArrayList m_Set; // array of items

        private class Lookup : IIndex, ITable
        {
            /// <summary>
            /// Hashtable wrapper
            /// 
            /// We provide wrapper around lookup to allow
            /// exposing only the index interface.
            /// </summary>

            private Hashtable m_Hashtable = new Hashtable();

            public string this[string sIn]
            {
                // Access member.

                get
                {
                    // Mirror value if not present.

                    string res = m_Hashtable[sIn] as string;

                    if (res == null)
                    {
                        res = sIn;
                    }

                    return res;
                }
            }

            /// <summary>
            /// Check if key is present in our table.
            /// </summary>
            /// <param name="sKey">
            /// Key to search for.
            /// </param>
            /// <returns>
            /// True if key is present.
            /// </returns>

            public bool Contains(string sKey)
            {
                // Delegate to embedded hash table.

                return m_Hashtable.Contains(sKey);
            }

            /// <summary>
            /// Provide simple insertion interace.
            /// </summary>
            /// <param name="sKey">
            /// Key to map.
            /// </param>
            /// <param name="sVal">
            /// Val to map.
            /// </param>

            public void Add(string sKey, string sVal)
            {
                // Append if unique.

                if (m_Hashtable.Contains(sKey) == false)
                {
                    m_Hashtable.Add(sKey, sVal);
                }
            }

        }

        public IIndex ByKey
        {
            get { return m_ByKey; }
        }

        public IIndex ByVal
        {
            get { return m_ByVal; }
        }

        public int Count
        {
            get { return m_Set.Count; }
        }

        /// <summary>
        /// Append new pair if unique.
        /// </summary>
        /// <param name="sKey">
        /// Key of pair to add.
        /// </param>
        /// <param name="sVal">
        /// Val of pair to add.
        /// </param>

        public void Add(string sKey, string sVal)
        {
            // Append new pair if unique.

            if (m_ByKey.Contains(sKey) == false)
            {
                // Add pair to map and index the entry.

                m_ByKey.Add(sKey, sVal);
                m_ByVal.Add(sVal, sKey);

                m_Set.Add(sVal);
            }
        }

        /// <summary>
        /// Allow walking of pairs in order they were added.
        /// </summary>
        /// <returns>
        /// Enumeration interface.
        /// </returns>

        public IEnumerator GetEnumerator()
        {
            // Delegate to embedded collection.

            return m_Set.GetEnumerator();
        }

        /// <summary>
        /// Construct default mapping.
        /// </summary>

        public Mapping()
        {
            // Initialize members.

            m_Set = new ArrayList();
            m_ByKey = new Lookup();
            m_ByVal = new Lookup();
        }

    }

    /// <summary>
    /// Individual field description.
    /// See <see cref="T:LendersOffice.QueryProcessor.Schema"/>
    /// for more information.
    /// </summary>

    public class Field
    {
        /// <summary>
        /// Field description
        /// 
        /// Maintain complete declaration of a reportable
        /// field at runtime.  We cache delegators at
        /// construction.
        /// </summary>

        public enum E_FieldType
        {
            Invalid = 0, // invalid
            Boo = 1, // boolean
            Int = 2, // integer
            Str = 3, // string
            Dbl = 4, // double
            Dec = 5, // decimal
            Enu = 6, // enumeration
            Dtm = 7, // date
            Obj = 8, // date object
        }

        public enum E_ClassType
        {
            Invalid = 0, // invalid
            Txt = 1, // text
            Pct = 2, // percent
            Cnt = 3, // count
            Csh = 4, // cash
            Cal = 5, // calendar
            LongText = 6 // TEXT in DB. These columns cannot be include in ORDER BY
        }

        private String m_Domain; // field domain
        private String m_Name; // field label
        private String m_Id; // field uid
        private String m_Selection; // select representation
        private String m_Format; // representation format
        private Mapping m_Mapping; // selects mapped to key
        private E_FieldType m_Type; // runtime type of field
        private E_ClassType m_Kind; // runtime kind of field
        private FieldPermissions m_Perm;

        public virtual string Selection
        {
            get { return m_Selection; }
        }

        public string GroupBySelection
        {
            get
            {
                // When date fields are used in a group by we want to strip
                // out the time because we use the field in an order by in
                // the underlying SQL. Otherwise, sorting within a group may
                // look incorrect because the group is sorted by time, too.
                // See opm 206937.
                if (this.Selection == this.Id &&
                    this.Type == E_FieldType.Dtm)
                {
                    return string.Format("datediff(dd, 0, {0})", this.Id);
                }
                else
                {
                    return this.Selection;
                }
            }
        }

        public string Format
        {
            get { return m_Format; }
        }

        public E_FieldType Type
        {
            get { return m_Type; }
        }

        public E_ClassType Kind
        {
            get { return m_Kind; }
        }

        public Mapping Mapping
        {
            get { return m_Mapping; }
        }

        public string Domain
        {
            get { return m_Domain; }
        }

        public string Name
        {
            get { return m_Name; }
        }
        [XmlIgnore]
        public String NamePerm
        {
            get { return m_Name + m_Perm.ToString(); }
        }
        [XmlIgnore]
        public SecureField Perm
        {
            set
            {
                m_Perm.Permissions = value;
            }
        }

        public string Id
        {
            get { return m_Id; }
        }

        [XmlIgnore]
        public string EnumName { get; private set; }

        /// <summary>
        /// Construct complete reportable field descriptor.
        /// </summary>
        /// <remarks>
        /// The id and type are required to properly access
        /// and compare the values we acquire through this
        /// descriptor.
        /// </remarks>

        public Field(String sDomain, String sName, String sId, String sSelection, Field.E_FieldType eType, Field.E_ClassType cType, String sFormat, params IPair[] aMapping)
            : this(sDomain, sName, sId, eType, cType, sFormat, aMapping)
        {
            // Initialize members.

            if (sSelection != String.Empty)
            {
                m_Selection = sSelection;
            }
        }

        /// <summary>
        /// Construct complete reportable field descriptor.
        /// </summary>
        /// <remarks>
        /// The id and type are required to properly access
        /// and compare the values we acquire through this
        /// descriptor.
        /// </remarks>

        public Field(String sDomain, String sName, String sId, Field.E_FieldType eType, Field.E_ClassType cType, String sFormat, params IPair[] aMapping)
            : this(sDomain, sName, sId, eType, cType, aMapping)
        {
            // Initialize members.

            if (sFormat != String.Empty)
            {
                m_Format = sFormat;
            }
        }

        /// <summary>
        /// Construct complete reportable field descriptor.
        /// </summary>
        /// <remarks>
        /// The id and type are required to properly access
        /// and compare the values we acquire through this
        /// descriptor.
        /// </remarks>

        public Field(String sDomain, String sName, String sId, Field.E_FieldType eType, Field.E_ClassType cType, params IPair[] aMapping)
            : this()
        {
            // Initialize members.

            try
            {
                // Set members to given.

                m_Type = eType;
                m_Kind = cType;

                m_Selection = sId;

                m_Domain = sDomain;
                m_Name = sName;
                m_Id = sId;

                // Lookup the corresponding representation mapping.
                // This field, as with the others, is optional.

                if (aMapping != null)
                {
                    // We have a map because we present options to
                    // the user during condition specification.
                    // Essentially, we have a set of key-value pairs
                    // that detail all the possible values this field
                    // can support.

                    foreach (IPair entry in aMapping)
                    {
                        m_Mapping.Add(entry.Key, entry.Val);
                    }
                }
                else if (m_Type == E_FieldType.Boo)
                {
                    // Add defaults to the boolean tha lacks mapping.

                    m_Mapping.Add("0", Boolean.FalseString);
                    m_Mapping.Add("1", Boolean.TrueString);
                }

                if (m_Type == E_FieldType.Enu)
                {
                    if (m_Mapping.Count == 0)
                    {
                        throw new FieldLacksMapping(sId);
                    }

                    if (aMapping.Length > 0)
                    {
                        EnumRepMap enumRepMap = aMapping[0] as EnumRepMap;
                        if (enumRepMap != null)
                        {
                            EnumName = enumRepMap.EnumCode.GetType().FullName;
                        }
                    }
                }
            }
            catch (ReportingException e)
            {
                throw new FieldFailed(sId, e);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Construct reportable field descriptor.
        /// </summary>

        public Field()
        {
            // Initialize members.

            m_Type = Field.E_FieldType.Invalid;
            m_Kind = Field.E_ClassType.Invalid;

            m_Mapping = new Mapping();

            m_Selection = "";

            m_Format = "";

            m_Domain = "";
            m_Name = "";
            m_Id = "";
            m_Perm = new FieldPermissions();
        }

    }

    /// <summary>
    /// Represents a field that requires special handling.
    /// </summary>
    public class SpecialField : Field
    {
        public IReadOnlyDictionary<string, string> DependentFields { get; private set; }

        public SpecialField(
            string domain, 
            string name, 
            string id, 
            string selection, 
            E_FieldType fieldType, 
            E_ClassType classType, 
            string format, 
            IDictionary<string, string> dependentFields,
            params IPair[] mappings) : base(domain, name, id, selection, fieldType, classType, format, mappings)
        {
            // Construct a shallow copy of the dictionary to prevent 
            // changes to the reference affecting the dependent fields.
            this.DependentFields = new Dictionary<string, string>(dependentFields, StringComparer.OrdinalIgnoreCase);
        }
    }

    /// <summary>
    /// We maintain a general purpose listing of report fields.
    /// See <see cref="T:LendersOffice.QueryProcessor.Query"/>
    /// for more information.
    /// </summary>

    public class Fields : IEnumerable
    {
        /// <summary>
        /// Field list
        /// 
        /// We maintain a set of column references.  Note
        /// that we do not allocate objects -- we simply
        /// reference them.  Additions don't need to be
        /// unique.
        /// </summary>

        private ArrayList m_Set; // set of items

        public Field this[int iField]
        {
            // Access member.

            get
            {
                // Fetch specified item.

                return m_Set[iField] as Field;
            }
        }

        public int Count
        {
            get { return m_Set.Count; }
        }

        /// <summary>
        /// Search for matching entry and return true if
        /// found in this set.
        /// </summary>
        /// <param name="fField">
        /// Field to match.
        /// </param>
        /// <returns>
        /// True if match is found.
        /// </returns>

        public bool Has(Field fField)
        {
            // Search each field for match.

            return Has(fField.Id);
        }

        /// <summary>
        /// Search for matching entry and return true if
        /// found in this set.
        /// </summary>
        /// <param name="sId">
        /// Field to match.
        /// </param>
        /// <returns>
        /// True if match is found.
        /// </returns>

        public bool Has(string sId)
        {
            // Search each field for match.

            foreach (Field field in m_Set)
            {
                if (field.Id == sId)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Append new field only if unique.
        /// </summary>
        /// <param name="fField">
        /// Field instance to append.
        /// </param>

        public void Add(Field fField)
        {
            // Append new column.

            m_Set.Add(fField);
        }

        /// <summary>
        /// Remove matching instance.
        /// </summary>
        /// <param name="sId">
        /// Identifier to match.
        /// </param>

        public void Nix(Field fField)
        {
            // Find match then nix instance.

            Nix(fField.Id);
        }

        /// <summary>
        /// Remove matching instance.
        /// </summary>
        /// <param name="sId">
        /// Identifier to match.
        /// </param>

        public void Nix(string sId)
        {
            // Find match then nix instance.

            foreach (Field field in m_Set)
            {
                if (field.Id == sId)
                {
                    m_Set.Remove(field);

                    return;
                }
            }
        }

        /// <summary>
        /// Get enumeration interface.
        /// </summary>
        /// <returns>
        /// Exposed interface.
        /// </returns>

        public IEnumerator GetEnumerator()
        {
            // Delegate to set.

            return m_Set.GetEnumerator();
        }

        /// <summary>
        /// Reset the entry set.
        /// </summary>

        public void Clear()
        {
            // Nix entries.

            m_Set.Clear();
        }

        /// <summary>
        /// Construct default fields.
        /// </summary>

        public Fields()
        {
            // Initialize members.

            m_Set = new ArrayList();
        }

    }

    /// <summary>
    /// Individual field realization.  Embedded references may
    /// be duplicated among the various representation pointers.
    /// See <see cref="T:LendersOffice.QueryProcessor.Schema"/>
    /// for more information.
    /// </summary>

    public class Value : IComparable
    {
        /// <summary>
        /// We have slimmed down the state of a report result to
        /// just contain what is representable as in individual
        /// cell.  Get the schema information from the report
        /// result's columns.  Keep in mind that xml serialization
        /// will lose the type information when go out and come
        /// back in as string.
        /// </summary>

        private Object m_Data;
        private string m_format = null;

        [XmlIgnore]
        public string Format
        {
            get { return m_format; }
            set { m_format = value; }
        }

        [XmlAttribute("v")]
        public String Text
        {
            set { m_Data = value; }
            get
            {
                if (m_Data != null)
                {
                    if (m_Data is DateTime)
                    {
                        if (!string.IsNullOrEmpty(m_format))
                            return ((DateTime)m_Data).ToString(m_format);
                        return ((DateTime)m_Data).ToString("M/d/yyyy");
                    }
                    else if (m_format != null)
                    {
                        return string.Format("{0:" + m_format + "}", m_Data);

                    }
                    else
                    {
                        return m_Data.ToString();
                    }
                }

                return "";
            }
        }

        [XmlIgnore]
        public Field.E_FieldType Type
        {
            // Access member.

            get
            {
                if (m_Data is DateTime == true)
                {
                    return Field.E_FieldType.Dtm;
                }

                if (m_Data is Boolean == true)
                {
                    return Field.E_FieldType.Boo;
                }

                if (m_Data is Decimal == true)
                {
                    return Field.E_FieldType.Dec;
                }

                if (m_Data is String == true)
                {
                    return Field.E_FieldType.Str;
                }

                if (m_Data is Double == true)
                {
                    return Field.E_FieldType.Dbl;
                }

                if (m_Data is Single == true)
                {
                    return Field.E_FieldType.Dbl;
                }

                if (m_Data is Int32 == true)
                {
                    return Field.E_FieldType.Int;
                }

                if (m_Data is Int16 == true)
                {
                    return Field.E_FieldType.Int;
                }

                return Field.E_FieldType.Invalid;
            }
        }

        [XmlIgnore]
        public Double Real
        {
            // Access member.

            get
            {
                if (m_Data is Decimal == true)
                {
                    return (double)(decimal)m_Data;
                }

                if (m_Data is Double == true)
                {
                    return (double)m_Data;
                }

                if (m_Data is Single == true)
                {
                    return (float)m_Data;
                }

                if (m_Data is Int32 == true)
                {
                    return (int)m_Data;
                }

                if (m_Data is Int16 == true)
                {
                    return (int)m_Data;
                }

                return 0;
            }
        }

        [XmlIgnore]
        public Object Data
        {
            set
            {
                if (value != null && value != DBNull.Value)
                {
                    m_Data = value;
                }
                else
                {
                    m_Data = null;
                }
            }
            get
            {
                return m_Data;
            }
        }

        [XmlAttribute("err")]
        public bool FatalError { get; set; }

        /// <summary>
        /// Provide generic comparison functionality so that we can
        /// form sets of values.
        /// </summary>
        /// <returns>
        /// Comparison result according to standard convention.
        /// </returns>

        public int CompareTo(Object oValue)
        {
            // Crack open the other value and compare the instances
            // if they are comparable.

            Value oThat = oValue as Value;

            if (oThat != null)
            {
                IComparable cData = m_Data as IComparable;

                if (cData != null)
                {
                    return cData.CompareTo(oThat.m_Data);
                }
            }

            return -1;
        }

        /// <summary>
        /// Construct copy of given value.  We transfer the
        /// embedded object references.
        /// </summary>
        /// <param name="vThat">
        /// Value to copy.
        /// </param>

        public Value(Value vThat)
        {
            // Initialize members.

            m_Data = vThat.m_Data;
            m_format = vThat.m_format;
            this.FatalError = vThat.FatalError;
        }

        /// <summary>
        /// Construct default reportable cell.
        /// </summary>

        public Value()
        {
            // Initialize member.

            m_Data = null;
        }
    }

    /// <summary>
    /// Store values in a unique set and preserve insertion order.
    /// See <see cref="T:LendersOffice.QueryProcessor.Schema"/>
    /// for more information.
    /// </summary>

    public class ValueSet
    {
        /// <summary>
        /// Store values in a unique set and preserve
        /// insertion order.
        /// </summary>

        private ArrayList m_Set; // ordered set of values

        [XmlIgnore]
        public Value this[int iValue]
        {
            get { return m_Set[iValue] as Value; }
        }

        [XmlIgnore]
        public int Count
        {
            get { return m_Set.Count; }
        }

        /// <summary>
        /// Add new entry to the set and guarantee that
        /// the content is unique.
        /// </summary>

        public void Add(Value vItem)
        {
            // Compare against existing entries and only
            // append novel values.

            foreach (Value vV in m_Set)
            {
                if (vV.CompareTo(vItem) == 0)
                {
                    return;
                }
            }

            m_Set.Add(vItem);
        }

        /// <summary>
        /// Get enumeration interface for walking our
        /// value set.
        /// </summary>

        public IEnumerator GetEnumerator()
        {
            // Retrieve looping accessor for walking.

            return m_Set.GetEnumerator();
        }

        /// <summary>
        /// Construct default set.
        /// </summary>

        public ValueSet()
        {
            // Initialize members.

            m_Set = new ArrayList();
        }

    }

    /// <summary>
    /// We track a group of fields that belong to a
    /// particular domain.  This collection is used
    /// to support listing by domain.  See
    /// <see cref="T:LendersOffice.QueryProcessor.Listing"/>
    /// for more information.
    /// </summary>

    public class FieldLabel
    {
        /// <summary>
        /// Label mapping
        /// 
        /// Each member of a domain is known by its user-friendly
        /// name and by its unique identifier.
        /// </summary>

        private string m_Name; // field name
        private string m_Id; // field uid
        private FieldPermissions m_Perm;

        public string Name
        {
            get { return m_Name; }
        }

        public string Id
        {
            get { return m_Id; }
        }

        public string NameAndPerm
        {
            get
            {
                return m_Name + " " + m_Perm.ToString();
            }
        }
        public SecureField Perm
        {
            set
            {
                m_Perm.Permissions = value;
            }
        }


        /// <summary>
        /// Construct specified label.
        /// </summary>
        /// <param name="sName">
        /// Label name.
        /// </param>
        /// <param name="sId">
        /// Label Uid.
        /// </param>

        public FieldLabel(string sName, string sId)
        {
            // Initialize members.

            m_Name = sName;
            m_Id = sId;
            m_Perm = new FieldPermissions();
        }

        /// <summary>
        /// Construct default label.
        /// </summary>

        public FieldLabel()
        {
            // Initialize members.

            m_Name = "";
            m_Id = "";
            m_Perm = new FieldPermissions();
        }

    }

    public class FieldPermissions
    {
        private bool[] m_Perm; // field Permissions
        private static readonly int FIELD_PERMISSIONS = 4; // Currently we have 4: Closer, Finance, LockDesk, and InvestorInfo
        private static readonly string[] PermNames = { "C", "F", "L", "I" };
        private readonly string PERMISSION_NOT_SET_ERROR = "Field Permissions have not been set, therefore user access cannot be computed.";
        private bool m_IsPermissionSet = false;

        public SecureField Permissions
        {
            set
            {
                if (value == null)
                    throw new ArgumentException();
                m_Perm[0] = value.HasPermCloser;
                m_Perm[1] = value.HasPermAccountant;
                m_Perm[2] = value.HasPermLockDesk;
                m_Perm[3] = value.HasPermInvestorInfo;
                m_IsPermissionSet = true;
            }
        }

        public static string GetPermissionString(SecureField field)
        {
            if (field == null)
                return "";

            bool[] perm = new bool[FIELD_PERMISSIONS];
            perm[0] = field.HasPermCloser;
            perm[1] = field.HasPermAccountant;
            perm[2] = field.HasPermLockDesk;
            perm[3] = field.HasPermInvestorInfo;

            bool isFirst = true;
            StringBuilder sb = new StringBuilder();
            sb.Append("(");

            for (int i = 0; i < perm.Length; i++)
            {
                if (perm[i])
                {
                    sb.Append(isFirst ? PermNames[i] : "," + PermNames[i]);
                    isFirst = false;
                }
            }
            sb.Append(")");
            return isFirst ? "" : sb.ToString();
        }


        public bool hasAccess(BrokerUserPrincipal bP)
        {
            if (!m_IsPermissionSet)
                throw new Exception(PERMISSION_NOT_SET_ERROR);

            return checkUserAccess(m_Perm, bP);
        }

        private bool checkUserAccess(bool[] fieldAccess, BrokerUserPrincipal bP)
        {
            bool hasCloser = bP.HasPermission(Permission.AllowCloserRead) || bP.HasPermission(Permission.AllowCloserWrite);
            bool hasLockDesk = bP.HasPermission(Permission.AllowLockDeskRead) || bP.HasPermission(Permission.AllowLockDeskWrite);
            bool hasFinance = bP.HasPermission(Permission.AllowAccountantRead) || bP.HasPermission(Permission.AllowAccountantWrite);
            bool hasInvestorInfo = bP.HasPermission(Permission.AllowViewingInvestorInformation)
                || bP.HasPermission(Permission.AllowEditingInvestorInformation);
            bool[] has = { hasCloser, hasFinance, hasLockDesk, hasInvestorInfo };

            for (int i = 0; i < fieldAccess.Length; i++)
            {
                if (fieldAccess[i] && !has[i])
                    return false;
            }
            return true;
        }

        public string sPerm
        {
            get
            {
                try
                {
                    if (!m_IsPermissionSet)
                        throw new Exception(PERMISSION_NOT_SET_ERROR);

                    if (m_Perm == null || m_Perm.Length == 0)
                        return "";

                    bool isFirst = true;
                    StringBuilder sb = new StringBuilder();
                    sb.Append("(");

                    for (int i = 0; i < m_Perm.Length; i++)
                    {
                        if (m_Perm[i])
                        {
                            sb.Append(isFirst ? PermNames[i] : "," + PermNames[i]);
                            isFirst = false;
                        }
                    }
                    sb.Append(")");
                    return isFirst ? "" : sb.ToString();

                }
                catch { }
                return "";
            }
        }

        public override string ToString()
        {
            string p = sPerm;
            if (p.Length != 0)
                p = p.Insert(0, " ");
            return p;
        }

        public FieldPermissions()
        {
            m_Perm = new bool[FIELD_PERMISSIONS];
            m_IsPermissionSet = false;
        }
    }

    /// <summary>
    /// We track a group of fields that belong to a
    /// particular domain.  This collection is used
    /// to support listing by domain.  See
    /// <see cref="T:LendersOffice.QueryProcessor.Listing"/>
    /// for more information.
    /// </summary>

    public class FieldGroup : IEnumerable
    {
        /// <summary>
        /// Domain items
        /// 
        /// Each domain contains one or more fields.
        /// We list the names here.  it is important
        /// to note that we maintain insertion order
        /// at all times.
        /// </summary>

        private ArrayList m_Set; // set of items.
        private string m_Domain; // group name

        public string Domain
        {
            get { return m_Domain; }
        }

        /// <summary>
        /// Append field name to this set.
        /// </summary>
        /// <param name="sName">
        /// Name of the field.
        /// </param>

        public void Add(string sName, string sId)
        {
            // Append new entry if unique.

            foreach (FieldLabel label in m_Set)
            {
                if (label.Id == sId)
                {
                    return;
                }
            }

            m_Set.Add(new FieldLabel(sName, sId));
        }

        /// <summary>
        /// Get enumeration interface.
        /// </summary>
        /// <returns>
        /// Exposed interface.
        /// </returns>

        public IEnumerator GetEnumerator()
        {
            // Delegate to set.

            return m_Set.GetEnumerator();
        }

        /// <summary>
        /// Construct default grouping.
        /// </summary>
        /// <param name="sDomain">
        /// Name of this domain group.
        /// </param>

        public FieldGroup(string sDomain)
        {
            // Initialize members.

            m_Set = new ArrayList();
            m_Domain = sDomain;
        }

        /// <summary>
        /// Construct default grouping.
        /// </summary>

        public FieldGroup()
        {
            // Initialize members.

            m_Set = new ArrayList();
            m_Domain = "";
        }

    }

    /// <summary>
    /// We track a list of domains that belong to a
    /// particular schema.  This collection is used
    /// to support listing all domains.  See
    /// <see cref="T:LendersOffice.QueryProcessor.Schema"/>
    /// for more information.
    /// </summary>

    public class Listing
    {
        /// <summary>
        /// Listing items
        /// 
        /// We store a collection of groups.  It is
        /// important to note that we maintain insertion
        /// order at all times.
        /// </summary>

        private ArrayList m_Set; // set of items.

        public FieldGroup this[string sDomain]
        {
            // Access member.

            get
            {
                // Search for match.

                foreach (FieldGroup group in m_Set)
                {
                    if (group.Domain == sDomain)
                    {
                        return group;
                    }
                }

                return null;
            }
        }

        /// <summary>
        /// Append field group to this set.
        /// </summary>
        /// <param name="fGroup">
        /// New domain group.
        /// </param>

        public void Add(FieldGroup fGroup)
        {
            // Append new entry if unique.

            foreach (FieldGroup group in m_Set)
            {
                if (group.Domain == fGroup.Domain)
                {
                    return;
                }
            }

            m_Set.Add(fGroup);
        }

        /// <summary>
        /// Get enumeration interface.
        /// </summary>
        /// <returns>
        /// Exposed interface.
        /// </returns>

        public IEnumerator GetEnumerator()
        {
            // Delegate to set.

            return m_Set.GetEnumerator();
        }

        /// <summary>
        /// Construct default listing.
        /// </summary>

        public Listing()
        {
            // Initialize members.

            m_Set = new ArrayList();
        }

    }

    /// <summary>
    /// Interface for collections of reportable fields.
    /// See <see cref="T:LendersOffice.QueryProcessor.Schema"/>
    /// for more information.
    /// </summary>

    public interface FieldTable
    {
        /// <summary>
        /// Access lookup table and retrieve corresponding
        /// field descriptor, according to lookup interface.
        /// </summary>
        /// <param name="sId">
        /// Unique identifier of reportable field.  This id
        /// should be unique within the context of this
        /// local schema.
        /// </param>
        /// <returns>
        /// Reference to matching reportable field.
        /// </returns>
        Field LookupById(string sId);

        /// <summary>
        /// Access lookup table and retrieve corresponding
        /// field descriptor, according to lookup interface,
        /// for fields that require special handling.
        /// </summary>
        /// <param name="sId">
        /// Unique identifier of reportable field.  This id
        /// should be unique within the context of this
        /// local schema.
        /// </param>
        /// <returns>
        /// Reference to matching reportable field.
        /// </returns>
        SpecialField LookupSpecialField(string id);
    }

    /// <summary>
    /// Interface for collections of reportable fields.
    /// See <see cref="T:LendersOffice.QueryProcessor.Schema"/>
    /// for more information.
    /// </summary>

    public interface FieldLookup : FieldTable
    {
        /// <summary>
        /// Access lookup table and retrieve corresponding
        /// field descriptor, according to lookup interface.
        /// </summary>
        /// <param name="sDomain">
        /// Domain of the field.
        /// </param>
        /// <param name="sName">
        /// Name of the field.
        /// </param>
        /// <returns>
        /// Reference to matching reportable field.
        /// </returns>

        Field LookupByLabel(String sDomain, String sName);

        /// <summary>
        /// Access lookup table and retrieve corresponding
        /// field descriptor, according to lookup interface.
        /// We return the first matching field.
        /// </summary>
        /// <param name="sName">
        /// Name of the field.
        /// </param>
        /// <returns>
        /// Reference to matching reportable field.
        /// </returns>

        Field LookupByName(String sName);

        /// <summary>
        /// Retrieve the set of fields from the underlying
        /// schema.  We expect them to be organized by domain
        /// in the order that they're added to the schema.
        /// </summary>

        Listing Fields
        {
            get;
        }

        /// <summary>
        /// Retrieve the set of macros from the underlying
        /// schema.  We expect them to be organized by domain
        /// in the order that they're added to the schema.
        /// </summary>

        Listing Macros
        {
            get;
        }

        IEnumerable<Field> GetAllFields();

    }

    /// <summary>
    /// Interface for catching thrown application exceptions.
    /// See <see cref="T:LendersOffice.QueryProcessor.Schema"/>
    /// for more information.
    /// </summary>

    public interface ExceptionCatcher
    {
        /// <summary>
        /// Handle caught exceptions and determine if the
        /// offending function should continue.
        /// </summary>
        /// <param name="exCaught">
        /// Exception to process.
        /// </param>
        /// <returns>
        /// True to continue, false to throw.
        /// </returns>

        Boolean Catch(Exception exCaught);

    }

    /// <summary>
    /// We maintain a list of reportable field descriptors that we
    /// allow to be indexed by id and by domain with name.  These
    /// fields completely describe how to access reportable fields
    /// within a reportable object.  Any type can be reportable.
    /// </summary>

    public class Schema : FieldLookup
    {

        public XDocument ExportToXDoc()
        {
            // 4/30/2013 dd - Debug method that will dump all the fields in m_Lookup
            // to a friendly XmlDocument.

            if (m_Lookup == null)
            {
                return null;
            }
            XDocument xdoc = new XDocument();
            XElement root = new XElement("custom_report_fields",
                new XAttribute("created_date", DateTime.Now.ToString()));

            xdoc.Add(root);
            foreach (Field field in m_Lookup.Values)
            {
                XElement element = new XElement("field");
                element.SetAttributeValue("id", field.Id);
                element.SetAttributeValue("name", field.Name);
                element.SetAttributeValue("nameperm", field.NamePerm);
                element.SetAttributeValue("domain", field.Domain);
                element.SetAttributeValue("enum_name", field.EnumName);
                element.SetAttributeValue("format", field.Format);
                element.SetAttributeValue("kind", field.Kind.ToString());
                element.SetAttributeValue("selection", field.Selection);
                if (field.Mapping != null && field.Mapping.Count > 0)
                {
                    foreach (string val in field.Mapping)
                    {
                        XElement mapElement = new XElement("map");
                        element.Add(mapElement);

                        mapElement.SetAttributeValue("key", field.Mapping.ByVal[val]);
                        mapElement.SetAttributeValue("val", val);
                    }
                }
                root.Add(element);
            }
            return xdoc;
        }
        /// <summary>
        /// Field description table
        /// 
        /// We maintain a runtime list of fields that are
        /// accessible to the reporting infrastructure.
        /// This set is initialized at application startup
        /// by a specialized instance, preferably static.
        /// </summary>

        private Hashtable m_Lookup; // lookup table
        private Hashtable m_Xlater; // name mapping
        private Hashtable m_Index; // index table
        private Dictionary<string, SpecialField> specialFieldLookup;
        private Listing m_Set; // set of items
        private Listing m_Mac; // set of items

        public Listing Fields
        {
            get { return m_Set; }
        }

        public Listing Macros
        {
            get { return m_Mac; }
        }

        public int Count
        {
            get { return m_Lookup.Count; }
        }

        /// <summary>
        /// Access lookup table and retrieve corresponding
        /// field descriptor, according to lookup interface.
        /// </summary>
        /// <param name="sDomain">
        /// Domain of the field.
        /// </param>
        /// <param name="sName">
        /// Name of the field.
        /// </param>
        /// <returns>
        /// Reference to matching reportable field.
        /// </returns>

        public Field LookupByLabel(String sDomain, String sName)
        {
            // Delegate to hashtable to find matching
            // reportable field.

            Hashtable table = m_Index[sDomain] as Hashtable;

            if (table != null)
            {
                return table[sName] as Field;
            }

            return null;
        }

        /// <summary>
        /// Access lookup table and retrieve corresponding
        /// field descriptor, according to lookup interface.
        /// We return the first matching field.
        /// </summary>
        /// <param name="sName">
        /// Name of the field.
        /// </param>
        /// <returns>
        /// Reference to matching reportable field.
        /// </returns>

        public Field LookupByName(String sName)
        {
            // Delegate to hashtable to find matching
            // reportable field.

            foreach (FieldGroup group in m_Set)
            {
                Hashtable table = m_Index[group.Domain] as Hashtable;

                if (table != null)
                {
                    if (table.Contains(sName) == true)
                    {
                        return table[sName] as Field;
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Access lookup table and retrieve corresponding
        /// field descriptor, according to lookup interface.
        /// </summary>
        /// <param name="sId">
        /// Unique identifier of reportable field.  This id
        /// should be unique within the context of this
        /// local schema.
        /// </param>
        /// <returns>
        /// Reference to matching reportable field.
        /// </returns>
        public Field LookupById(String sId)
        {
            // Access descriptor directly.  We first lookup
            // in the translation mapping to convert old ids.

            string xlate = m_Xlater[sId] as string;

            if (xlate != null)
            {
                return m_Lookup[xlate] as Field;
            }

            return m_Lookup[sId] as Field;
        }

        /// <summary>
        /// Access lookup table and retrieve corresponding
        /// field descriptor, according to lookup interface
        /// for fields requiring special handling.
        /// </summary>
        /// <param name="id">
        /// Unique identifier of reportable field.  This id
        /// should be unique within the context of this
        /// local schema.
        /// </param>
        /// <returns>
        /// Reference to matching reportable field.
        /// </returns>
        public SpecialField LookupSpecialField(string id)
        {
            SpecialField field;
            this.specialFieldLookup.TryGetValue(id, out field);
            return field;
        }

        /// <summary>
        /// Append a new field descriptor to this schema.  We
        /// index this field according to its unique identifier
        /// and by its domain and name.  Use this method when
        /// initializing a static, class-specific schema.
        /// </summary>
        /// <returns>
        /// We submit caught exceptions to our catcher.
        /// </returns>
        /// <remarks>
        /// The id and type are required to properly access
        /// and compare the values we acquire through this
        /// descriptor.
        /// </remarks>

        public Exception Add(String sDomain, String sName, String sId, String sSelection, Field.E_FieldType eType, Field.E_ClassType cType, String sFormat, params IPair[] aMapping)
        {
            // Append new field descriptor and index it into
            // our lookup tables.

            try
            {
                // Create new descriptor and save it in our
                // complete listing set.

                Field field = new Field(sDomain, sName, sId, sSelection, eType, cType, sFormat, aMapping);

                if (m_Lookup.Contains(sId) == true)
                {
                    throw new FieldNotUnique(sId);
                }

                m_Lookup.Add(sId, field);

                // Add this field to our indexed table.  When
                // searching for field info through the ui,
                // this interface will get used a lot.

                Hashtable indexed = m_Index[sDomain] as Hashtable;

                if (indexed == null)
                {
                    m_Index.Add(sDomain, indexed = new Hashtable());
                }

                indexed.Add(sName, field);

                // Initialize an entry in the listing set.  We
                // provide this interface mainly for the ui.

                FieldGroup grouped = m_Set[sDomain];

                if (grouped == null)
                {
                    m_Set.Add(grouped = new FieldGroup(sDomain));
                }

                grouped.Add(sName, sId);
            }
            catch (Exception e)
            {
                // Something failed, so report it to invoker.

                return new SchemaInitializationFailed(e);
            }

            return null;
        }

        /// <summary>
        /// Append a new field descriptor to this schema.  We
        /// index this field according to its unique identifier
        /// and by its domain and name.  Use this method when
        /// initializing a static, class-specific schema.
        /// </summary>
        /// <returns>
        /// We submit caught exceptions to our catcher.
        /// </returns>
        /// <remarks>
        /// The id and type are required to properly access
        /// and compare the values we acquire through this
        /// descriptor.
        /// </remarks>

        public Exception Add(String sDomain, String sName, String sId, String sSelection, Field.E_FieldType eType, Field.E_ClassType cType, params IPair[] aMapping)
        {
            // Delegate to our official add implementation.

            return Add(sDomain, sName, sId, sSelection, eType, cType, "", aMapping);
        }

        /// <summary>
        /// Append a new field descriptor to this schema.  We
        /// index this field according to its unique identifier
        /// and by its domain and name.  Use this method when
        /// initializing a static, class-specific schema.
        /// </summary>
        /// <returns>
        /// We submit caught exceptions to our catcher.
        /// </returns>
        /// <remarks>
        /// The id and type are required to properly access
        /// and compare the values we acquire through this
        /// descriptor.
        /// </remarks>

        public Exception Add(String sDomain, String sName, String sId, Field.E_FieldType eType, Field.E_ClassType cType, String sFormat, params IPair[] aMapping)
        {
            // Delegate to our official add implementation.

            return Add(sDomain, sName, sId, "", eType, cType, sFormat, aMapping);
        }

        /// <summary>
        /// Append a new field descriptor to this schema.  We
        /// index this field according to its unique identifier
        /// and by its domain and name.  Use this method when
        /// initializing a static, class-specific schema.
        /// </summary>
        /// <returns>
        /// We submit caught exceptions to our catcher.
        /// </returns>
        /// <remarks>
        /// The id and type are required to properly access
        /// and compare the values we acquire through this
        /// descriptor.
        /// </remarks>

        public Exception Add(String sDomain, String sName, String sId, Field.E_FieldType eType, Field.E_ClassType cType, params IPair[] aMapping)
        {
            // Delegate to our official add implementation.

            return Add(sDomain, sName, sId, "", eType, cType, "", aMapping);
        }

        /// <summary>
        /// Adds a special field to the schema.
        /// </summary>
        /// <param name="domain">
        /// The domain of the field.
        /// </param>
        /// <param name="id">
        /// The ID of the field.
        /// </param>
        /// <param name="selection">
        /// The selection logic for pulling data from the database.
        /// </param>
        /// <param name="fieldType">
        /// The type of the field.
        /// </param>
        /// <param name="classType">
        /// The database type.
        /// </param>
        /// <param name="mappings">
        /// Any mappings for the field.
        /// </param>
        /// <returns>
        /// Any exception that occurred or null if no exception occurred.
        /// </returns>
        public Exception AddSpecial(
            string domain, 
            string id, 
            string selection, 
            Field.E_FieldType fieldType, 
            Field.E_ClassType classType, 
            IDictionary<string, string> dependentFields,
            params IPair[] mappings)
        {
            try
            {
                if (this.specialFieldLookup.ContainsKey(id))
                {
                    return new SchemaInitializationFailed(new FieldNotUnique(id));
                }

                var field = new SpecialField(domain, string.Empty, id, selection, fieldType, classType, string.Empty, dependentFields, mappings);
                this.specialFieldLookup.Add(id, field);
            }
            catch (Exception e)
            {
                return new SchemaInitializationFailed(e);
            }

            return null;
        }

        /// <summary>
        /// Append a new macro descriptor to this schema.  We
        /// index this field according to its unique identifier
        /// and by its domain and name.  Use this method when
        /// initializing a static, class-specific schema.
        /// </summary>
        /// <remarks>
        /// The id and type are required to properly access
        /// and compare the values we acquire through this
        /// descriptor.  Make sure the id matches the field
        /// name of the get-accessor exposed through the
        /// specified class.  The comparable and representable
        /// delegators are acquired using the base id with
        /// an underscore and the suffix.
        /// </remarks>

        public Exception Mac(Field fMacro)
        {
            // Append new field descriptor and index it into
            // our lookup tables.

            try
            {
                // Verify new macro is valid.  We do not accept
                // duplicate entries.

                if (fMacro == null)
                {
                    throw new ArgumentNullException("fMacro", "Null macro fields not allowed.");
                }

                if (m_Lookup.Contains(fMacro.Id) == true)
                {
                    throw new FieldNotUnique(fMacro.Id);
                }

                m_Lookup.Add(fMacro.Id, fMacro);

                // Initialize an entry in the listing set.  We
                // provide this interface mainly for the ui.

                FieldGroup grouped;

                grouped = m_Set[fMacro.Domain];

                if (grouped == null)
                {
                    m_Set.Add(grouped = new FieldGroup(fMacro.Domain));
                }

                grouped.Add(fMacro.Name, fMacro.Id);

                grouped = m_Mac[fMacro.Domain];

                if (grouped == null)
                {
                    m_Mac.Add(grouped = new FieldGroup(fMacro.Domain));
                }

                grouped.Add(fMacro.Name, fMacro.Id);
            }
            catch (Exception e)
            {
                // Something failed, so report it to invoker.

                return new SchemaInitializationFailed(e);
            }

            return null;
        }

        /// <summary>
        /// Set a new id translation entry to keep track of old ids
        /// so that old queries can be properly unserialized into a
        /// valid query instance with valid field references.
        /// </summary>
        /// <param name="sOldId">
        /// Old identifier.
        /// </param>
        /// <param name="sNewId">
        /// New identifier.
        /// </param>
        /// <returns>
        /// We submit caught exceptions to our catcher.
        /// </returns>

        public Exception Set(string sOldId, string sNewId)
        {
            // We add to our translation set a single new id mapping.

            try
            {
                // Add the identifier for lookup routing from old
                // to new.  We don't bother checking if it's
                // valid anymore (creates an order of operations
                // dependency that I think is overkill).

                m_Xlater.Add(sOldId, sNewId);
            }
            catch (Exception e)
            {
                // Something failed, so report it to invoker.

                return new SchemaTranslationFailed(e);
            }

            return null;
        }

        public IEnumerable<Field> GetAllFields()
        {
            foreach (DictionaryEntry entry in m_Lookup)
            {
                Field field = (Field)entry.Value;
                yield return field;
            }
        }

        /// <summary>
        /// Initialize default schema.
        /// </summary>

        public Schema()
        {
            // Initialize members.

            m_Lookup = new Hashtable();
            m_Xlater = new Hashtable();
            m_Index = new Hashtable();
            this.specialFieldLookup = new Dictionary<string, SpecialField>(StringComparer.OrdinalIgnoreCase);
            m_Set = new Listing();
            m_Mac = new Listing();
        }

    }

    /// <summary>
    /// Each field could have aggregation functions associated
    /// with it as part of the query.  Each function is applied
    /// to the entire table for the associated field.
    /// See <see cref="T:LendersOffice.QueryProcessor.Query"/>
    /// for more information.
    /// 
    /// 1/7/2005 kb - It occurred to me that I should put a
    /// warning in here to tell anyone who thinks about
    /// modifying this enum that these values are persisted
    /// as numbers in the existing reports.  If you add to
    /// this list, don't change the existing entries' values.
    /// </summary>

    public enum E_FunctionType
    {
        Invalid = 0, // invalid
        Av = 1, // average
        Sm = 2, // sum
        Ct = 3, // count
        Mn = 4, // minimum
        Mx = 5, // maximum
    }

    public class Function
    {
        /// <summary>
        /// Aggregation specification
        /// 
        /// Track functions that are applied to a field across
        /// the entire table.
        /// </summary>

        private E_FunctionType m_Type; // type of aggregation

        [XmlElement]
        public E_FunctionType Type
        {
            set { m_Type = value; }
            get { return m_Type; }
        }

        /// <summary>
        /// Construct field function.
        /// </summary>
        /// <param name="fThat">
        /// Copy to construct from.
        /// </param>

        public Function(Function fThat)
        {
            // Initialize members.

            m_Type = fThat.Type;
        }

        /// <summary>
        /// Construct field function.
        /// </summary>
        /// <param name="eType">
        /// Type of function.
        /// </param>

        public Function(E_FunctionType eType)
        {
            // Initialize members.

            m_Type = eType;
        }

        /// <summary>
        /// Construct default field function.
        /// </summary>

        public Function()
        {
            // Initialize members.

            m_Type = E_FunctionType.Invalid;
        }

    }

    /// <summary>
    /// We maintain a set of calculations to be performed
    /// for each field, though no field needs to have any.
    /// See <see cref="T:LendersOffice.QueryProcessor.Query"/>
    /// for more information.
    /// </summary>

    public class Functions
    {
        /// <summary>
        /// List of functions
        /// 
        /// Apply this set to the field.
        /// </summary>

        private ArrayList m_Set; // set of items

        [XmlArray]
        [XmlArrayItem(typeof(Function))]
        public ArrayList Items
        {
            set { m_Set = value; }
            get { return m_Set; }
        }

        public int Count
        {
            get { return m_Set.Count; }
        }

        /// <summary>
        /// Append new function to the list.
        /// </summary>
        /// <param name="fFunction">
        /// Function to append.
        /// </param>

        public void Add(Function fFunction)
        {
            // Append to the set if unique.

            if (fFunction != null)
            {
                foreach (Function item in m_Set)
                {
                    if (item.Type == fFunction.Type)
                    {
                        return;
                    }
                }

                m_Set.Add(fFunction);
            }
        }

        /// <summary>
        /// Append new function to the list.
        /// </summary>
        /// <param name="eFunction">
        /// Function to append.
        /// </param>

        public void Add(E_FunctionType eFunction)
        {
            // Append to the set if unique.

            Add(new Function(eFunction));
        }

        /// <summary>
        /// Search for a matching function type.
        /// </summary>
        /// <param name="eFunction">
        /// Function to match.
        /// </param>
        /// <returns>
        /// True if found.
        /// </returns>

        public bool Has(E_FunctionType eFunction)
        {
            // Search for a match.

            foreach (Function item in m_Set)
            {
                if (item.Type == eFunction)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Find first match and remove it.
        /// </summary>
        /// <param name="eFunction">
        /// Function to remove.
        /// </param>

        public void Nix(E_FunctionType eFunction)
        {
            // Remove from the set if found.

            foreach (Function item in m_Set)
            {
                if (item.Type == eFunction)
                {
                    m_Set.Remove(item);

                    break;
                }
            }
        }

        /// <summary>
        /// Clear out this set.
        /// </summary>

        public void Clear()
        {
            // Nix all entries.

            m_Set.Clear();
        }

        /// <summary>
        /// Exposes iteration interface.
        /// </summary>
        /// <returns>
        /// Enumerator to this set.
        /// </returns>

        public IEnumerator GetEnumerator()
        {
            // Delegate to set.

            return m_Set.GetEnumerator();
        }

        /// <summary>
        /// Construct set.
        /// </summary>
        /// <param name="csThat">
        /// Source to construct from.
        /// </param>

        public Functions(Functions fThat)
            : this()
        {
            // Initialize members.

            foreach (Function item in fThat.m_Set)
            {
                Add(new Function(item));
            }
        }

        /// <summary>
        /// Construct default set.
        /// </summary>

        public Functions()
        {
            // Initialize members.

            m_Set = new ArrayList();
        }

    }

    /// <summary>
    /// Report query layout column.  Each column refers to a
    /// single field description found in the active schema.
    /// See <see cref="T:LendersOffice.QueryProcessor.Query"/>
    /// for more information.
    /// </summary>

    public class Column
    {
        /// <summary>
        /// Column characteristics
        /// 
        /// The column is simply a pointer back to a particular
        /// field descriptor.  As we build a query, each column
        /// must specify a reportable field found in the schema.
        /// </summary>

        Field.E_FieldType m_Type; // field's representation type
        Field.E_ClassType m_Kind; // field's representation kind
        String m_Format; // field's string format spec
        String m_Id; // field's unique identifier
        String m_Name; // field's user name label
        Boolean m_Show; // report visibility status
        Functions m_Functions; // aggregation calculations
        FieldPermissions m_Perm;

        [XmlAttribute]
        public Field.E_FieldType Type
        {
            set { m_Type = value; }
            get { return m_Type; }
        }

        [XmlAttribute]
        public Field.E_ClassType Kind
        {
            set { m_Kind = value; }
            get { return m_Kind; }
        }

        [XmlElement]
        public String Format
        {
            set { m_Format = value; }
            get { return m_Format; }
        }

        [XmlElement]
        public String Id
        {
            set { m_Id = value; }
            get { return m_Id; }
        }

        [XmlElement]
        public String Name
        {
            set { m_Name = value; }
            get { return m_Name; }
        }
        [XmlIgnore]
        public String NamePerm
        {
            get { return m_Name + m_Perm.ToString(); }
        }
        [XmlIgnore]
        public SecureField Perm
        {
            set
            {
                m_Perm.Permissions = value;
            }
        }


        [XmlElement]
        public Functions Functions
        {
            // Access member.

            set
            {
                // Transfer function items.

                m_Functions.Clear();

                foreach (Function item in value)
                {
                    m_Functions.Add(item);
                }
            }
            get
            {
                return m_Functions;
            }
        }

        [XmlAttribute]
        public Boolean Show
        {
            set { m_Show = value; }
            get { return m_Show; }
        }

        /// <summary>
        /// Return the column's id for default control binding.
        /// </summary>

        public override String ToString()
        {
            // Return the column's id for default binding.

            return m_Id;
        }

        #region ( Constructors )

        /// <summary>
        /// Construct column from source.
        /// </summary>
        /// <param name="cColumn">
        /// Column to copy.
        /// </param>

        public Column(Column cColumn)
            : this()
        {
            // Initialize members.

            m_Type = cColumn.m_Type;
            m_Kind = cColumn.m_Kind;

            foreach (Function item in cColumn.m_Functions)
            {
                m_Functions.Add(new Function(item));
            }

            m_Format = cColumn.m_Format;
            m_Id = cColumn.m_Id;
            //			m_Domain = cColumn.m_Domain;
            m_Name = cColumn.m_Name;
            m_Show = cColumn.m_Show;
        }


        /// <summary>
        /// Construct column with specific field descriptor.
        /// </summary>

        public Column(Field fField, bool bShow)
            : this()
        {
            // Initialize members.

            m_Type = fField.Type;
            m_Kind = fField.Kind;
            m_Format = fField.Format;
            //			m_Domain = fField.Domain;
            m_Name = fField.Name;
            m_Id = fField.Id;

            m_Show = bShow;
        }

        /// <summary>
        /// Construct column with specific field description.
        /// </summary>

        public Column(Field.E_FieldType eType, Field.E_ClassType eKind, String sName)
            : this()
        {
            // Initialize members.

            m_Type = eType;
            m_Kind = eKind;
            m_Name = sName;
        }

        /// <summary>
        /// Construct default.
        /// </summary>

        public Column()
        {
            // Initialize members.

            m_Functions = new Functions();

            m_Type = Field.E_FieldType.Invalid;
            m_Kind = Field.E_ClassType.Invalid;

            m_Format = "";

            //			m_Domain = "";
            m_Name = "";
            m_Id = "";
            m_Perm = new FieldPermissions();
            m_Show = true;
        }

        #endregion

    }

    /// <summary>
    /// Report query layout columns.  We maintain an ordered list
    /// of field description.  The query initializing object is
    /// responsible for allocating the underlying column objects.
    /// See <see cref="T:LendersOffice.QueryProcessor.Query"/>
    /// for more information.
    /// </summary>

    public class Columns
    {
        /// <summary>
        /// Column list
        /// 
        /// We maintain a set of column references.  Note
        /// that we do not allocate objects -- we simply
        /// reference them.
        /// </summary>

        private ArrayList m_Set; // set of items

        [XmlArray]
        [XmlArrayItem(typeof(Column))]
        public ArrayList Items
        {
            set { m_Set = value; }
            get { return m_Set; }
        }

        [XmlIgnore]
        public Column this[int iColumn]
        {
            get
            {
                // Fetch specified item.

                return m_Set[iColumn] as Column;
            }
        }

        [XmlIgnore]
        public int Count
        {
            get { return m_Set.Count; }
        }


        /// <summary>
        /// Used to know if the branch table should be joined or not.
        /// </summary>
        /// 
        /// <returns>
        /// Bool value indicating whether a branch related field is in the query.
        /// </returns>
        [XmlIgnore]
        public bool bNeedsBranch
        {
            //opm 32079 fs 09/07/09
            get { return (this.Has("sBranchCode") || this.Has("sBranchNm") || this.Has("sBranchDivision")); }
        }

        /// <summary>
        /// Used to know if the PML_BROKER table should be joined
        /// </summary>
        /// <returns>
        /// Bool value indicating a field in PML_BROKER is in the query
        /// </returns>
        [XmlIgnore]
        public bool bNeedsPMLBroker
        {
            get { return this.Has("sEmployeeLoanRepCompanyID"); }
        }

        [XmlIgnore]
        public bool bNeedsMortgagePool
        {
            get
            {
                return this.Has("PoolNumberByAgency") || this.Has("PoolInvestorName") || this.Has("PoolCommitmentNum")
                    || this.Has("PoolTradeNumbers") || this.Has("PoolCommitmentD") || this.Has("PoolCommitmentExpD");
            }
        }

        /// <summary>
        /// Search for column with matching field.
        /// </summary>
        /// <param name="sId">
        /// Identifier associated with field.
        /// </param>
        /// <returns>
        /// Reference to our matching column.
        /// </returns>

        public Column Find(string sId)
        {
            // Walk the array and find a match.

            foreach (Column column in m_Set)
            {
                if (column.Id.Equals(sId, StringComparison.OrdinalIgnoreCase))
                {
                    return column;
                }
            }

            return null;
        }

        /// <summary>
        /// Search for column with matching field and return the index position. If name is not found then return -1.
        /// </summary>
        /// <param name="sId"></param>
        /// <returns></returns>
        public int FindIndex(string sId)
        {
            int index = 0;
            foreach (Column column in m_Set)
            {
                if (column.Id.Equals(sId, StringComparison.OrdinalIgnoreCase))
                {
                    return index;
                }
                index++;
            }
            return -1;
        }
        /// <summary>
        /// Search for column with matching field.
        /// </summary>
        /// <param name="sId">
        /// Identifier associated with field.
        /// </param>
        /// <returns>
        /// True if found.
        /// </returns>

        public Boolean Has(string sId)
        {
            // Walk the array and find a match.

            if (Find(sId) != null)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Append new column even if already present in this
        /// set of column references.
        /// </summary>
        /// <param name="cColumn">
        /// Column instance to append.
        /// </param>

        public void Add(Column cColumn)
        {
            // Append column, even if already present.

            if (cColumn != null)
            {
                m_Set.Add(cColumn);
            }
        }

        /// <summary>
        /// Find the matching column and nix it.
        /// </summary>
        /// <param name="cColumn">
        /// Column to match.
        /// </param>

        public void Nix(Column cColumn)
        {
            // Drop the matching column.

            Nix(cColumn.Id);
        }

        /// <summary>
        /// Find the matching column and nix it.
        /// </summary>
        /// <param name="sId">
        /// Column to match.
        /// </param>

        public void Nix(string sId)
        {
            // Drop the matching column.

            foreach (Column column in m_Set)
            {
                if (column.Id == sId)
                {
                    m_Set.Remove(column);

                    return;
                }
            }
        }

        /// <summary>
        /// Get enumeration interface.
        /// </summary>
        /// <returns>
        /// Exposed interface.
        /// </returns>

        public IEnumerator GetEnumerator()
        {
            // Delegate to set.

            return m_Set.GetEnumerator();
        }

        /// <summary>
        /// Reset the entry set.
        /// </summary>

        public void Clear()
        {
            // Reset the entry set.

            m_Set.Clear();
        }

        /// <summary>
        /// Construct default columns.
        /// </summary>

        public Columns()
        {
            // Initialize members.

            m_Set = new ArrayList();
        }

    }

    /// <summary>
    /// We maintain these details to describe an individual
    /// field as referenced by the sorting or group by sets.
    /// See <see cref="T:LendersOffice.QueryProcessor.Query"/>
    /// for more information.
    /// </summary>

    public class Descriptor
    {
        /// <summary>
        /// Placeholder details
        /// 
        /// We maintain these details to describe an individual
        /// field as referenced by the sorting or group by sets.
        /// We want a structure that can expand yet not break
        /// the persistent xml format.
        /// 
        /// 1/13/2005 kb - Added an option flag.  Right now,
        /// we will use this to tell the processor that group
        /// by fields are to be crossed.  We can also use it
        /// to determine sorting type (ascending/descending).
        /// </summary>

        private string m_Id; // field unique identifier
        private int m_Option; // field specific option

        [XmlElement]
        public string Id
        {
            set { m_Id = value; }
            get { return m_Id; }
        }

        [XmlElement]
        public int Option
        {
            set { m_Option = value; }
            get { return m_Option; }
        }

        [XmlIgnore]
        public bool IsGroupBy { get; set; }

        /// <summary>
        /// Construct copy of descriptor.
        /// </summary>
        /// <param name="dThat">
        /// Source to copy.
        /// </param>

        public Descriptor(Descriptor dThat)
        {
            // Initialize members.

            m_Id = dThat.m_Id;
            m_Option = dThat.m_Option;
        }

        /// <summary>
        /// Construct descriptor.
        /// </summary>
        /// <param name="sId">
        /// Id of descriptor.
        /// </param>
        /// <param name="iOption">
        /// Field specific option.
        /// </param>

        public Descriptor(string sId, int iOption)
        {
            // Initialize members.

            m_Id = sId;
            m_Option = iOption;
        }

        /// <summary>
        /// Construct default descriptor.
        /// </summary>

        public Descriptor()
        {
            // Initialize members.

            m_Id = "";
            m_Option = 0;
        }

    }

    /// <summary>
    /// We maintain a list of unique field identifiers for
    /// sorting and group by lists.  List must be unique.
    /// See <see cref="T:LendersOffice.QueryProcessor.Query"/>
    /// for more information.
    /// </summary>

    public class Descriptors
    {
        /// <summary>
        /// Unique set
        /// 
        /// We maintain a unique set of field identifiers.
        /// </summary>

        private ArrayList m_Set; // unique set

        [XmlIgnore]
        public Descriptor this[string sId]
        {
            // Access member.

            get
            {
                foreach (Descriptor iDesc in m_Set)
                {
                    if (iDesc.Id == sId)
                    {
                        return iDesc;
                    }
                }

                return null;
            }
        }

        [XmlIgnore]
        public Descriptor this[int iId]
        {
            get
            {
                return m_Set[iId] as Descriptor;
            }
        }

        [XmlArray]
        [XmlArrayItem(typeof(Descriptor))]
        public ArrayList Items
        {
            set { m_Set = value; }
            get { return m_Set; }
        }

        [XmlIgnore]
        public Descriptor Last
        {
            get
            {
                if (m_Set.Count > 0)
                {
                    return m_Set[m_Set.Count - 1] as Descriptor;
                }

                return null;
            }
        }

        [XmlIgnore]
        public int Count
        {
            get { return m_Set.Count; }
        }

        /// <summary>
        /// Append new descriptor only if unique.
        /// </summary>
        /// <param name="dDescriptor">
        /// Descriptor instance to append.
        /// </param>

        public void Add(Descriptor dDescriptor)
        {
            // Append new descriptor.

            if (dDescriptor != null)
            {
                foreach (Descriptor desc in m_Set)
                {
                    if (desc.Id == dDescriptor.Id)
                    {
                        return;
                    }
                }

                m_Set.Add(dDescriptor);
            }
        }

        /// <summary>
        /// Append new descriptor only if unique.
        /// </summary>
        /// <param name="sId">
        /// Descriptor instance to append.
        /// </param>
        /// <param name="iOption">
        /// Option to control field use.
        /// </param>

        public void Add(string sId, int iOption)
        {
            // Append id, only if unique.

            Add(new Descriptor(sId, iOption));
        }

        /// <summary>
        /// Append new descriptor only if unique.
        /// </summary>
        /// <param name="sId">
        /// Descriptor instance to append.
        /// </param>

        public void Add(string sId)
        {
            // Append id, only if unique.

            Add(new Descriptor(sId, 0));
        }

        /// <summary>
        /// Search for matching id.
        /// </summary>
        /// <param name="sId">
        /// Id to match.
        /// </param>
        /// <returns>
        /// True if found.
        /// </returns>

        public bool Has(string sId)
        {
            // Find matching id and report if found.

            foreach (Descriptor desc in m_Set)
            {
                if (desc.Id == sId)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Swap positions of ids to reorder id layout.
        /// </summary>
        /// <param name="sA">
        /// Id to swap.
        /// </param>
        /// <param name="sB">
        /// Id to swap.
        /// </param>

        public void Swap(Descriptor dA, Descriptor dB)
        {
            // Lookup ids and reset entries if present.

            int a, b;

            a = m_Set.IndexOf(dA);
            b = m_Set.IndexOf(dB);

            if (a < m_Set.Count && b < m_Set.Count)
            {
                m_Set[b] = dA;
                m_Set[a] = dB;
            }
        }

        /// <summary>
        /// Find the matching identifier and nix it.
        /// </summary>
        /// <param name="sId">
        /// Id to match.
        /// </param>

        public void Nix(string sId)
        {
            // Drop the matching id.

            foreach (Descriptor desc in m_Set)
            {
                if (desc.Id == sId)
                {
                    m_Set.Remove(desc);

                    return;
                }
            }
        }

        /// <summary>
        /// Get enumeration interface.
        /// </summary>
        /// <returns>
        /// Exposed interface.
        /// </returns>

        public IEnumerator GetEnumerator()
        {
            // Delegate to set.

            return m_Set.GetEnumerator();
        }

        /// <summary>
        /// Reset the id set.
        /// </summary>

        public void Clear()
        {
            // Reset the entry set.

            m_Set.Clear();
        }

        /// <summary>
        /// Construct default identifiers set.
        /// </summary>

        public Descriptors()
        {
            // Initialize members.

            m_Set = new ArrayList();
        }

    }

    /// <summary>
    /// Condition arguments can be of multiple classes.  When
    /// we resolve a condition to test it, we compare a field
    /// value against a condition's argument according to the
    /// class of the argument.  Constant arguments are common.
    /// See <see cref="T:LendersOffice.QueryProcessor.Query"/>
    /// for more information.
    /// </summary>

    public enum E_ArgumentType
    {
        Invalid = 0, // invalid
        Const = 1, // constant
        Field = 2, // field id
    }

    public class Argument
    {
        /// <summary>
        /// Condition arguments can be of multiple classes.
        /// When we resolve a condition to test it, we compare
        /// a field value against a condition's argument according
        /// to the class of the argument.  Constant arguments
        /// are common.
        /// </summary>

        private E_ArgumentType m_Type;
        string m_Value;

        [XmlAttribute]
        public E_ArgumentType Type
        {
            set { m_Type = value; }
            get { return m_Type; }
        }

        [XmlElement]
        public string Value
        {
            set { m_Value = value; }
            get { return m_Value; }
        }

        public static bool operator ==(Argument aA, Argument aB)
        {
            // Compare elements.

            return aA.m_Type == aB.m_Type && aA.m_Value == aB.m_Value;
        }

        public static bool operator !=(Argument aA, Argument aB)
        {
            // Compare elements.

            return aA.m_Type != aB.m_Type || aA.m_Value != aB.m_Value;
        }

        public override int GetHashCode()
        {
            // Implement support method.

            return m_Type.GetHashCode() * m_Value.GetHashCode();
        }

        public override bool Equals(object oThat)
        {
            // Implement support method.

            if (oThat is Argument)
            {
                return this == oThat as Argument;
            }

            return false;
        }

        /// <summary>
        /// Construct condition argument.
        /// </summary>
        /// <param name="aThat">
        /// Copy to construct from.
        /// </param>

        public Argument(Argument aThat)
        {
            // Initialize members.

            m_Type = aThat.Type;
            m_Value = aThat.Value;
        }

        /// <summary>
        /// Construct condition argument from specified.
        /// </summary>
        /// <param name="eType">
        /// Type of argument.
        /// </param>
        /// <param name="sValue">
        /// Argument value.
        /// </param>

        public Argument(E_ArgumentType eType, string sValue)
        {
            // Initialize members.

            if (sValue != null)
            {
                m_Value = sValue;
            }
            else
            {
                m_Value = "";
            }

            m_Type = eType;
        }

        /// <summary>
        /// Construct condition argument from specified.
        /// </summary>
        /// <param name="sValue">
        /// Argument value.
        /// </param>

        public Argument(string sValue)
        {
            // Initialize members.

            m_Type = E_ArgumentType.Const;

            if (sValue != null)
            {
                m_Value = sValue;
            }
            else
            {
                m_Value = "";
            }
        }

        /// <summary>
        /// Construct default condition argument.
        /// </summary>

        public Argument()
        {
            // Initialize members.

            m_Type = E_ArgumentType.Invalid;
            m_Value = "";
        }

    }

    /// <summary>
    /// Every condition has a type and a value associated
    /// with it.  We store the value as a string and convert
    /// it to the representation type for comparison during
    /// report query processing.  If the string is invalid,
    /// then we probably will throw an exception on compare.
    /// See <see cref="T:LendersOffice.QueryProcessor.Query"/>
    /// for more information.
    /// </summary>

    public enum E_ConditionType
    {
        Invalid = 0, // invalid
        Eq = 1, // equal to
        Bw = 2, // begins with
        Nw = 3, // not begins with
        Ne = 4, // not equal to
        Ge = 5, // greater than or equal to
        Gt = 6, // greater than
        Le = 7, // less than or equal to
        Lt = 8, // less than
    }

    public class Condition
    {
        /// <summary>
        /// Condition specification
        /// 
        /// Every condition has a type and a value associated
        /// with it.  We store the value as a string and convert
        /// it to the representation type for comparison during
        /// report query processing.
        /// </summary>

        private E_ConditionType m_Type; // type of constraint
        private String m_Id; // field lookup uid
        private Argument m_Argument; // predicate argument
        private FieldPermissions m_Perm;


        [XmlAttribute]
        public E_ConditionType Type
        {
            set { m_Type = value; }
            get { return m_Type; }
        }

        [XmlAttribute]
        public String Id
        {
            set { m_Id = value; }
            get { return m_Id; }
        }

        [XmlElement]
        public Argument Argument
        {
            set { m_Argument = value; }
            get { return m_Argument; }
        }

        public SecureField Permissions
        {
            set { m_Perm.Permissions = value; }
        }

        public FieldPermissions Perm
        {
            get { return m_Perm; }
        }

        /// <summary>
        /// Construct field constraint.
        /// </summary>
        /// <param name="cThat">
        /// Copy to construct from.
        /// </param>

        public Condition(Condition cThat)
        {
            // Initialize members.

            m_Type = cThat.m_Type;
            m_Id = cThat.Id;
            m_Argument = cThat.m_Argument;
            m_Perm = cThat.Perm;
        }

        /// <summary>
        /// Construct field condition.
        /// </summary>
        /// <param name="eType">
        /// Type of constraint operation.
        /// </param>
        /// <param name="sId">
        /// Unique id of condition field.
        /// </param>
        /// <param name="aArgument">
        /// Single operand for condition evaluation.
        /// </param>

        public Condition(E_ConditionType eType, String sId, Argument aArgument)
        {
            // Initialize members.

            m_Type = eType;
            m_Id = sId;
            m_Argument = aArgument;
            m_Perm = new FieldPermissions();
        }

        /// <summary>
        /// Construct field condition.
        /// </summary>
        /// <param name="eType">
        /// Type of constraint operation.
        /// </param>
        /// <param name="sId">
        /// Unique id of condition field.
        /// </param>
        /// <param name="aArgument">
        /// Single operand for condition evaluation. Uses the enum's underlying value (e.g, an int)
        /// </param>

        public Condition(E_ConditionType eType, String sId, Enum aArgument)
        {
            // Initialize members.

            m_Type = eType;
            m_Id = sId;
            m_Argument = new Argument(Convert.ChangeType(aArgument, aArgument.GetTypeCode()).ToString());
            m_Perm = new FieldPermissions();
        }

        /// <summary>
        /// Construct field condition.
        /// </summary>
        /// <param name="eType">
        /// Type of constraint operation.
        /// </param>
        /// <param name="sId">
        /// Unique id of condition field.
        /// </param>
        /// <param name="aArgument">
        /// Single operand for condition evaluation. Uses the object's .ToString() value.
        /// </param>

        public Condition(E_ConditionType eType, String sId, object aArgument)
        {
            // Initialize members.

            m_Type = eType;
            m_Id = sId;
            m_Argument = new Argument(aArgument.ToString());
            m_Perm = new FieldPermissions();
        }

        /// <summary>
        /// Construct field condition.
        /// </summary>

        public Condition()
        {
            // Initialize members.

            m_Type = E_ConditionType.Invalid;
            m_Id = "";
            m_Argument = new Argument();
            m_Perm = new FieldPermissions();
        }

    }

    /// <summary>
    /// We allow groups to contain other groups, or conditions.
    /// The conditions are the leaves that we evaluate and
    /// group according to their parent's grouping operator.
    /// See <see cref="T:LendersOffice.QueryProcessor.Query"/>
    /// for more information.
    /// </summary>

    public enum E_ConditionsType
    {
        Invalid = 0, // invalid
        An = 1, // and all members together
        Or = 2, // or all members together
    }

    public class Conditions : Composite
    {
        /// <summary>
        /// Composite node
        /// 
        /// We allow groups to contain other groups, or conditions.
        /// The conditions are the leaves that we evaluate and group
        /// according to their parent's grouping operator.
        /// </summary>

        private ArrayList m_Set; // composite pattern tree node of ordered members
        private E_ConditionsType m_Type; // type of condition grouping operation

        [XmlAttribute]
        public E_ConditionsType Type
        {
            set { m_Type = value; }
            get { return m_Type; }
        }

        [XmlArray]
        [XmlArrayItem(typeof(Conditions))]
        [XmlArrayItem(typeof(Condition))]
        public ArrayList Items
        {
            set { m_Set = value; }
            get { return m_Set; }
        }

        [XmlIgnore]
        public object this[int iEntry]
        {
            get { return m_Set[iEntry]; }
        }

        [XmlIgnore]
        public int Count
        {
            get { return m_Set.Count; }
        }

        [XmlIgnore]
        public bool bNeedsBranch
        {
            get
            {
                return UsesField("sBranchCode", "sBranchNm", "sBranchDivision");
            }
        }

        [XmlIgnore]
        public bool bNeedsPMLBroker
        {
            get { return UsesField("sEmployeeLoanRepCompanyID"); }
        }

        private bool UsesField(params string[] fieldId)
        {
            foreach (Object oItem in this.Walk)
            {
                Condition cC = oItem as Condition;
                if (cC == null)
                    continue;

                if (fieldId.Contains(cC.Id, StringComparer.OrdinalIgnoreCase))
                    return true;
                if (cC.Argument.Type == E_ArgumentType.Field && fieldId.Contains(cC.Argument.Value, StringComparer.OrdinalIgnoreCase))
                    return true;
            }
            return false;
        }

        [XmlIgnore]
        public bool bNeedsMortgagePool
        {
            get
            {
                return UsesField("PoolNumberByAgency", "PoolInvestorName", "PoolCommitmentNum",
                    "PoolTradeNumbers", "PoolCommitmentD", "PoolCommitmentExpD");
            }
        }

        public bool hasFieldsAccess(AbstractUserPrincipal bp)
        {
            foreach (Object oItem in this.Walk)
            {
                Condition cC = oItem as Condition;
                if (cC == null)
                    continue;

                if (!LoanFieldSecurityManager.CanReadField(cC.Id, bp))
                    return false;
                if (cC.Argument.Type == E_ArgumentType.Field && (!LoanFieldSecurityManager.CanReadField(cC.Argument.Value, bp)))
                    return false;
            }
            return true;
        }

        [XmlIgnore]
        public bool ContainsSecureFields
        {
            get
            {
                foreach (Object oItem in this.Walk)
                {
                    Condition cC = oItem as Condition;
                    if (cC == null)
                        continue;

                    if (LoanFieldSecurityManager.IsSecureField(cC.Id))
                        return true;
                    if (cC.Argument.Type == E_ArgumentType.Field && (LoanFieldSecurityManager.IsSecureField(cC.Argument.Value)))
                        return true;
                }
                return false;
            }
        }

        /// <summary>
        /// Append condition onto the list.
        /// </summary>
        /// <param name="eType">
        /// Type of condition to append.
        /// </param>
        /// <param name="sId">
        /// Major field of condition.
        /// </param>
        /// <param name="aArgument">
        /// Opaque condition argument.
        /// </param>

        public void Add(E_ConditionType eType, String sId, Argument aArgument)
        {
            // Add to the set if unique.

            Add(new Condition(eType, sId, aArgument));
        }

        /// <summary>
        /// Append condition onto the list.
        /// </summary>
        /// <param name="cCondition">
        /// Condition to append.
        /// </param>

        public void Add(Condition cCondition)
        {
            // Search for match in heterogeneous set.  If
            // found, skip append by reference.

            foreach (object item in m_Set)
            {
                Condition condition = item as Condition;

                if (condition == null)
                {
                    continue;
                }

                if (condition.Type == cCondition.Type && condition.Id == cCondition.Id && condition.Argument == cCondition.Argument)
                {
                    return;
                }
            }

            m_Set.Add(cCondition);
        }

        /// <summary>
        /// Append group onto the list.  We might think that
        /// merging would work here.  That is, if an 'or'-group
        /// is already in the set, and we're adding an 'or'-group,
        /// then why not blend together.  It depends on the
        /// container.  We would not blend if the container type
        /// was 'and'.
        /// </summary>
        /// <param name="cGroup">
        /// Group to append.
        /// </param>

        public void Add(Conditions cConditions)
        {
            // Append this object by reference.

            m_Set.Add(cConditions);
        }

        /// <summary>
        /// Remove the conditions that matches the id.
        /// </summary>
        /// <param name="cConditions">
        /// Conditions to match by id.
        /// </param>

        public void Nix(Conditions cConditions)
        {
            // Remove conditions by matching ids.

            m_Set.Remove(cConditions);
        }

        /// <summary>
        /// Remove the condition.
        /// </summary>
        /// <param name="cCondition">
        /// Condition to match.
        /// </param>

        public void Nix(Condition cCondition)
        {
            // Remove condition by matching instance.

            m_Set.Remove(cCondition);
        }

        /// <summary>
        /// Remove all entries from this set.
        /// </summary>

        public void Clear()
        {
            // Nix all entries.

            m_Set.Clear();
        }

        /// <summary>
        /// Exposes iteration interface.
        /// </summary>
        /// <returns>
        /// Enumerator to this set.
        /// </returns>

        public override IEnumerator GetEnumerator()
        {
            // Delegate to set.

            return m_Set.GetEnumerator();
        }

        /// <summary>
        /// Construct conditions from copy.
        /// </summary>
        /// <param name="cThat">
        /// Conditions with which to copy.
        /// </param>

        public Conditions(Conditions cThat)
            : this()
        {
            // Tranfer copy of source.  Watch out
            // because we're using recursion here.

            foreach (object item in cThat.m_Set)
            {
                if (item is Conditions)
                {
                    m_Set.Add(new Conditions(item as Conditions));
                }
                else
                    if (item is Condition)
                    {
                        m_Set.Add(new Condition(item as Condition));
                    }
            }

            m_Type = cThat.m_Type;
        }

        /// <summary>
        /// Construct conditions with given type.
        /// </summary>
        /// <param name="eType">
        /// Type of condition group.
        /// </param>

        public Conditions(E_ConditionsType eType)
            : this()
        {
            // Initialize members.

            m_Type = eType;
        }

        /// <summary>
        /// Construct default array.
        /// </summary>

        public Conditions()
        {
            // Allocate new objects.  We default the
            // group type so that uninitialization is
            // not too painful.

            m_Set = new ArrayList();

            m_Type = E_ConditionsType.An;
        }

    }

    /// <summary>
    /// We maintain a set of option strings for future use.
    /// See <see cref="T:LendersOffice.QueryProcessor.Query"/>
    /// for more information.
    /// </summary>

    public class Options
    {
        /// <summary>
        /// Option set
        /// 
        /// We track query options as strings.  Example option
        /// would be: "page break before tables".
        /// </summary>

        private ArrayList m_Set = new ArrayList();

        [XmlArray]
        [XmlArrayItem(typeof(String))]
        public ArrayList Items
        {
            set { m_Set = value; }
            get { return m_Set; }
        }

        [XmlIgnore]
        public Object this[int iEntry]
        {
            get { return m_Set[iEntry]; }
        }

        [XmlIgnore]
        public int Count
        {
            get { return m_Set.Count; }
        }

        /// <summary>
        /// Append option onto the list.
        /// </summary>
        /// <param name="sOption">
        /// Option to append.
        /// </param>

        public void Add(String sOption)
        {
            // Add without checking current set.  Skip
            // if object is invalid.

            if (sOption != null)
            {
                String sOpt = sOption.ToLower();

                foreach (String sItem in m_Set)
                {
                    if (sItem.ToLower() == sOpt)
                    {
                        return;
                    }
                }

                m_Set.Add(sOption);
            }
        }

        /// <summary>
        /// Look for a match (case-insensitive search).
        /// </summary>

        public Boolean Has(String sOption)
        {
            // Validate input and linear search for it.

            if (sOption != null)
            {
                String sOpt = sOption.ToLower();

                foreach (String sItem in m_Set)
                {
                    if (sItem.ToLower() == sOpt)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Find the matching option and remove it
        /// (case-insensitive search).
        /// </summary
        /// <param name="sOption">
        /// Option to match and nix.
        /// </param>

        public void Nix(String sOption)
        {
            // Find matching option and nix it.

            if (sOption != null)
            {
                String sOpt = sOption.ToLower();

                foreach (String sItem in m_Set)
                {
                    if (sItem.ToLower() == sOpt)
                    {
                        m_Set.Remove(sItem);

                        break;
                    }
                }
            }
        }

        /// <summary>
        /// Remove all entries from this set.
        /// </summary>

        public void Clear()
        {
            // Nix all entries.

            m_Set.Clear();
        }

        /// <summary>
        /// Exposes iteration interface.
        /// </summary>
        /// <returns>
        /// Enumerator to this set.
        /// </returns>

        public IEnumerator GetEnumerator()
        {
            // Delegate to set.

            return m_Set.GetEnumerator();
        }

    }

    /// <summary>
    /// Queries contain a full description of a user's designed report.
    /// </summary>
    /// <remarks>
    /// The report, when applied to the data set using the query processor is
    /// known as a query.  The returned hierarchical table result is called
    /// as a report.  Really, the term is overloaded:  Users look at results
    /// and call them "reports".  They also speak in terms of "running a
    /// report" to get the result.  So, to our typical customer, you run a
    /// "report" to see the "report".
    /// 
    /// For our purposes, we will refer to the report's structure and
    /// predicate as the "query".  Simply put, a query is just that:
    /// structure and predicate.  We track the column layout of the report
    /// with the conditions for selecting the loans to include.  Each
    /// query maps 1:1 with some sql select statement.  I know this becuase
    /// each query is translated to a valid select statement to retrieve
    /// the results from the database.  We only use reports/queries to
    /// pull and analyze loan data, but any class of data object can be
    /// queries using this framework.
    /// 
    /// Look in the loan-specific schema code file to see comments relating
    /// to maintaining the schema for custom loan reporting.
    /// 
    /// I) Structure by columns
    /// 
    /// A valid report must have at least one column.  A column is a description
    /// of a reportable field to be included in the report.  The field is the
    /// key point of reference for each column, because we map the field id
    /// to the database to retrieve the data of interest.  Fields can be
    /// pure data, calculated, or derived from a mapping of pure data into
    /// a lookup table of friendly representations.  For this design discussion,
    /// we will consider the referenced fields as reportable fields.  Each
    /// field id maps to a single reportable field descriptor maintained by
    /// the specialized schema implementation.  The schema (which will be
    /// described later) contains the mapping between reportable fields and
    /// the underlying database.
    /// 
    /// The order of the columns in the query determines the left-to-right
    /// order of the reportable fields in the result.  Some columns may be
    /// hidden, in which case they would not be rendered.  However, this is
    /// not used anymore.
    /// 
    /// The query engine allows specifying column sorting and column grouping.
    /// Both query specification consist of listing the participating columns
    /// in the required order of precedence.
    /// 
    /// Sorting consists of a list of column identifiers with modifiers to
    /// designate ascending or descending.  Just as a select statement can
    /// have multiple "order by" columns, so also we allow multiple columns.
    /// We sort by the first listed column, then by the second, then by the
    /// third, etc.  Each listed column contributes to the overall sorting
    /// with decreasing precedence.
    /// 
    /// Grouping is handled similarly: each group-by column is listed with
    /// a modifier to designate the type of grouping, straight or crossed.
    /// Straight grouping is the kind you would expect to find: all rows
    /// with group-by column values that match are "grouped" into a single
    /// table.  If you have more than one level of grouping, then all
    /// columns must match to belong to a table.  Crossed grouping is more
    /// complex.  There are 2 orders of crossing.  The first order will
    /// collapse out all the row details and construct a combination table
    /// from the aggregate calculation results of the contributing tables.
    /// 
    /// For example, imagine we had the following loan data result, grouped
    /// by loan origination branch name (2 tables of shown):
    /// 
    /// Table: Branch = Main
    /// [ 001 , Joe A Borrower , Main   , Loan Open      ,       $124,500.00 ,     4.3% ]
    /// [ 002 , Zoe A Borrower , Main   , Loan Submitted ,       $173,000.00 ,     5.3% ]
    /// [ 003 , Boe A Borrower , Main   , Loan Funded    ,       $400,500.00 ,     4.3% ]
    /// [ 004 , Qoe A Borrower , Main   , Loan Docs      ,       $500,000.00 ,     6.3% ]
    ///                                                 tot: $1,198,000.00  avg: 5.1%
    /// 
    /// Table: Branch = Orange
    /// [ 005 , Foe A Borrower , Orange , Loan Submitted ,       $373,000.00 ,     5.3% ]
    /// [ 009 , Soe A Borrower , Orange , Loan Submitted ,       $653,000.00 ,     8.3% ]
    /// [ 007 , Aoe A Borrower , Orange , Loan Submitted ,       $200,000.00 ,     7.3% ]
    /// [ 008 , Ioe A Borrower , Orange , Loan Submitted ,       $200,000.00 ,     1.3% ]
    ///                                                 tot: $1,426,000.00  avg: 5.6%
    /// 
    /// If we crossed the group-by specification for "Branch", then we would
    /// generate the following single table from the previous contributors:
    /// 
    /// [ Main   ,     $1,198,000.00 ,     5.1% ]
    /// [ Orange ,     $1,426,000.00 ,     5.6% ]
    ///           tot: $2,634,000.00  avg: 5.4%
    /// 
    /// Note how each table for a given branch is collapsed down into a
    /// single row of the crossed table.  First-order crossing performs
    /// this kind of collapsing.  Second-order crossing is invoked when
    /// you cross 2 or more group-by columns.  I won't go into the details
    /// of second-order crossing (you can try it and see for yourself).
    /// 
    /// II) Querying the data rows with conditions
    /// 
    /// We call them "conditions" because they describe the conditions or
    /// terms that must be met to include the data instance in the report.
    /// We provide a tree-based, composite structure for organizing
    /// conditions; each node is a boolean operator with one or more
    /// children -- a child can be a single condition, or a nested node.
    /// 
    /// For example, say we're reporting on loans and only want include
    /// the loans opened after January 1st, 2005.  The condition tree
    /// would look like the following:
    /// 
    /// [All]
    ///   |
    ///   |
    ///   |
    ///   |
    ///   |
    /// [Con]-( "Opened Date" gt "1/1/2005" )
    /// 
    /// This simple tree simply states that "all" conditions contained
    /// in the "all" node must be true.  Namely, the "opened date" for
    /// all loans needs to be greater than "1/1/2005".
    /// 
    /// For another example, imagine we want to only include loans that
    /// were opened during 2005 and are currently closed:
    /// 
    /// [All]
    ///   |\
    ///   | \
    ///   |  |\
    ///   |  | \
    ///   |  |  |
    /// [Con]-( "Opened Date" ge "1/1/2005" )
    ///      |  |
    ///    [Con]-( "Opened Date" lt "1/1/2006" )
    ///         |
    ///       [Con]-( "Status" eq "Closed" )
    ///       
    /// Similar to the previous tree, this condition composite contains
    /// 3 child conditions and can be stated thus: The "opened date" for
    /// all loans needs to be greater than or equal to "1/1/2005" and
    /// less than "1/1/2006", and the loan's status must presently equal
    /// "closed".  Because the conditions are contained by an "all" node,
    /// then all the conditions must be true to satisfy inclusion.
    /// 
    /// For a final example, we will complicate inclusion by allowing for
    /// multiple current statuses:
    /// 
    /// [All]
    ///   |\
    ///   | \
    ///   |  |\
    ///   |  | \
    ///   |  |  |
    /// [Con]-( "Opened Date" ge "1/1/2005" )
    ///      |  |
    ///    [Con]-( "Opened Date" lt "1/1/2006" )
    ///         |
    ///       [Any]
    ///         |\
    ///         | \
    ///         |  |\
    ///         |  | \
    ///         |  |  |
    ///       [Con]-( "Status" eq "Submitted" )
    ///            |  |
    ///          [Con]-( "Status" eq "Funded" )
    ///               |
    ///             [Con]-( "Status" eq "Docs" )
    /// 
    /// This condition composite is the same as the previous, except that
    /// it allows for loans to be in one of three statuses, "Submitted",
    /// "Funded", or "Docs".  The "any" node contained by the "all" node
    /// allows for "any" of it's children conditions to be true.
    /// 
    /// For our query model, we supply 2 composite trees: "filters" and
    /// "relates".  The distinction is almost arbitrary.  Both are condition
    /// trees that specify the inclusion terms and both must be true.  Both
    /// trees combine into a single where-clause when constructing the
    /// corresponding sql select statement.  However, we found some benefit
    /// in separating the regular conditions from standard field-based
    /// filtering.  The take home message about our composite trees is that
    /// conditions lie at the leaves.
    /// 
    /// Any reportable field can be specified in a condition, either as
    /// the field being examined or as the comparison argument.  For example
    /// we can have a loan query condition that compares the "Qualified Rate"
    /// with the "Note Rate".  The condition field need not be part of the
    /// query as a visible column.  The only requirement we impose is that
    /// the condition field and its argument agree in representation.
    /// 
    /// III) Serialization
    /// 
    /// Queries can be translated to xml documents and back to queries again
    /// using static serializer methods.  We store the query in the database
    /// as an xml document.  This was the easiest way to get persisted queries
    /// that were editable as an object and persistable as a string.  Note
    /// that the query class design is careful not to contain transient or
    /// time sensitive data.
    /// 
    /// IV) Editing reports
    /// 
    /// Our current editor is designed to edit loan reports, but the logic
    /// and participating user controls are general, relying only on the
    /// object model of a query to do their work.  It's best to describe
    /// the panels of the editor and then dive into the user controls.
    /// 
    /// The opening panel gives a snapshot of the important aspects of the
    /// query.  In this view you will find the following:
    /// 
    /// Report details - Name and possibly other attributes down the road.
    /// 
    /// Report fields  - Reorder the included reportable fields and specify
    ///                  the aggregate functions to calculate, as well as
    ///                  what fields to sort or group-by.  Click the button
    ///                  to include or remove reportable fields from the
    ///                  report.
    /// 
    /// Sorting order  - Specify the sorting order of the fields selected
    ///                  for sorting in the field listing's checkboxes.
    /// 
    /// Grouping order - Specify the grouping characteristics for fields
    ///                  selected for grouping in the field listing's
    ///                  checkboxes.
    /// 
    /// Conditions     - View a readonly composite tree showing the current
    ///	                 condition set for the query.  Click the button to
    ///	                 
    /// 
    /// Report summary - A simple description for users to read when selecting
    ///                  a report to run -- helps explain without opening it
    ///                  up in the editor and figuring out the purpose.
    /// 
    /// Most users use this panel to manipulate the field layout and setup
    /// aggregate functions for particular columns.  You access the other
    /// panels by clicking the embedded buttons.
    /// 
    /// The next panel to discuss is the field choosing panel.  Access the
    /// field chooser by clicking "add / remove fields".  Once there, you
    /// will see 2 main sections:
    /// 
    /// Fields to include - Every reportable field is listed by section.
    ///	                    You can use the "search for" control to find
    ///	                    fields using partial label matching.  Include
    ///	                    a field in the report by clicking the "include"
    ///	                    link to the left of each field's label.
    /// 
    /// Report fields     - A listing of what was already included.  You
    ///                     shuffle the layout order using the "up/down"
    ///                     arrows, or remove the field from the report
    ///                     by clicking "remove".
    /// 
    /// From the main panel, the next panel to discuss is the condition
    /// editor.  By far, this is the most complex aspect of the editor.
    /// Access the condition editor from the main panel by clicking the
    /// "add/edit/remove conditions" button.  Once there, you will see
    /// 2 sections:
    /// 
    /// Conditions                  - Here lies an editable copy of the
    ///                               condition tree mentioned above.
    ///                               The 2 main actions taken are adding
    ///                               child conditions, or adding child
    ///                               groups.  There are 2 possible groups
    ///                               to add: "any" and "all", and both are
    ///                               added as immediate children of the
    ///                               invoking group.  Adding conditions
    ///                               will require selecting fields, operators
    ///                               and parameters from the adjacent
    ///                               "parameters" section.  Added conditions
    ///                               become immediate children of the
    ///                               invoking group.  A group may contain
    ///                               nested groups and/or conditions.  All
    ///                               nodes in the tree may be deleted by
    ///                               clicking the "x" button (note that
    ///                               deleting a group will delete all its
    ///                               children as well).
    /// 
    /// Select condition parameters - This section remains blank unless you
    ///                               are editing a condition.  To control
    ///                               the presentation of this section, edit
    ///                               a condition and select each of the 3
    ///                               variables of a condition (field, operator,
    ///                               and parameter).
    /// 
    /// Building a condition tree for a report can take some time.  It's
    /// best to proceed carefully and make sure each condition expresses
    /// the real inclusion terms for the report.
    /// 
    /// To access the final panel, click "set range & run" from any of the
    /// 3 panels mentioned up to this point.  This last panel is where we
    /// edit the so called "filters" for this query.  We provide a simpler
    /// interface for specifying commonly-used conditions for filtering
    /// what data objects should be included.  Again, we have 2 sections:
    /// 
    /// Status event range - Users can choose multiple choices and set a
    ///                      date range around them.  For our loan editor,
    ///                      we have setup this user control to allow the
    ///                      user to quickly select multiple status types,
    ///                      and then set a date range specifying when said
    ///                      event occurred.  If any event fits the corresponding
    ///                      range, then the loan is considered valid (at
    ///                      least, we won't filter it out because of these
    ///                      settings).
    /// 
    /// Include only files - In this section for our loan query editor, we
    ///                      allow the user to select multiple statuses
    ///                      that they want to include in the result.  That
    ///                      is, for a loan to be part of the result, its
    ///                      current status must be one of the specified
    ///                      options.  This section could contain any enum-
    ///                      based field designation, but for loans, we chose
    ///                      to use status because it is the most common
    ///                      filtering field for loan reports.
    /// 
    /// This panel provides the single invocation point for "running" the
    /// report by clicking "generate report", which will launch the result
    /// into its own window.  Typically, users will open the report for
    /// editing and click through to this last panel to test the report by
    /// running it.
    /// 
    /// The editor is composed of at least 12 user controls.  Each one is
    /// independent and edits or displays the piece of the query that it
    /// is designed to care about.  The independence is important; we want
    /// a design that supports replacing any single control without affecting
    /// the others.  The bad part is that we sacrifice load times and post
    /// back times to pull it off (most of the user controls load the same
    /// information redundantly).  A revision of what we expose to each
    /// user control might clean this up.
    /// 
    /// The user controls attempt to do much of their interaction purely
    /// client-side.  This means that they leverage jscript and html divs
    /// heavily.  It makes the ui responsive, but very hard to maintain.
    /// If you adjust the client-side interaction of the user controls,
    /// expect to spend extra time debugging and testing.  You were warned.
    /// 
    /// Each user control will follow the same initialization and saving
    /// sequence, so you should see some semblance of a framework in there.
    /// For initialization, each control binds to the query using the field
    /// lookup table for schema details.  Each control will pull out what
    /// it needs to render correctly, so the containing page must bind the
    /// query in edit to each control.  For saving, the same process is
    /// invoked except each control will write to the query the latest
    /// state it maintains without blowing away others' changes.
    /// 
    /// V) Managing reports
    /// 
    /// Currently, we only report on loans, so this topic will cover how
    /// we organize loan reports.
    /// 
    /// There are 3 lists of reports from which to select: "Your queries",
    /// "others' queries", and "sample queries".  The first contains all
    /// the reports that you authored (not edited, but created -- anyone
    /// with the required permission can edit a report).  The second shows
    /// all the other reports.  Finally, the sample reports are a listing
    /// of reports we have authored and maintained to help demonstrate
    /// proper use of the report processing engine.  From each list, you
    /// can run, edit, copy, or delete a query.
    /// 
    /// The sample reports are maintained within a special broker account
    /// that is located by finding the special user "lobrokeradmin".
    /// Whoever this user belongs to is the broker that contains the
    /// sample reports.  Login as "lobrokeradmin" and edit the custom
    /// reports contained within to make changes to everybody's sample
    /// reports.
    /// 
    /// The LendingQB pipeline also displays reports.  Each user has
    /// the ability to select a report they want to see whenever they
    /// visit "your pipeline".  Select a new pipeline to display by clicking
    /// the "change" link in the upper righthand corner of the pipeline.
    /// The only difference between running a report using the normal channel
    /// and seeing the result in the pipeline is that the pipeline ignores
    /// crossed grouping.
    /// 
    /// The final way to access reports is through publishing.  This feature
    /// is not used all that much, but has some interesting aspects.  Namely,
    /// publishing requires explicit permission.  Also, when a user publishes
    /// his/her report, all other users have immediate access to running
    /// the report -- all published reports list in their own control in the
    /// bottom righthand corner of the main page.  Finally, when users run
    /// a published report, they will see the results that the publishing
    /// user would see if they had run it.  Thus, publishing allows a manager
    /// or administrator to prepare a view of the company's pipeline that
    /// everyone needs to see and track (sales contest results, perhaps).
    /// 
    /// VI) Running reports
    /// 
    /// Whenever reports are run, the result is the current state of the
    /// company's data.  We don't cache the result.  If a user runs the
    /// report at 2 different times, they may see different outcomes.
    /// However, to facilitate exporting what the user sees as the result,
    /// we do cache each report after it is run through normal channels
    /// so that we can quickly convert the cached instance into a
    /// consistent, exportable representation (csv, tab-delimited text).
    /// </remarks>

    [XmlRoot("Query")]
    public class Query
    {
        /// <summary>
        /// Version label
        /// 
        /// We update the current version every time the implementation
        /// changes.  We need to be able to distinguish persisted, old
        /// versions of a report so that we can properly migrate it
        /// into the latest form.
        /// </summary>

        private const long c_CurrentVersion = 4;

        /// <summary>
        /// Report layout
        /// 
        /// A complete report query describes the layout of the
        /// report, including what fields are shown or hidden,
        /// the sorting order, the grouping set, and the fields'
        /// conditions that each resulting record must meet.
        /// </summary>

        private ProcessingLabel m_Label; // report processing info
        private Columns m_Columns; // report layout columns
        private Descriptors m_Sorting; // hierarchical sort set
        private Descriptors m_GroupBy; // ordered grouping list
        private Conditions m_Relates; // qualifying predicate
        private Conditions m_Filters; // qualifying filter
        private Options m_Options; // query processor opts
        private String m_Details; // query user comments
        private Guid m_Id; // system-wide uid

        #region ( Serialization operators )

        public static implicit operator Query(string sThat)
        {
            // Deserialize the query and generate an instance
            // from the string form that we can process and use.

            try
            {
                // Deserialize the query according to the xml
                // deserialization api and attributes scattered
                // throughout the object model.


                return (Query)SerializationHelper.XmlDeserialize(sThat, typeof(Query));



            }
            catch (Exception e)
            {
                // Pass on the love.

                throw new InvalidFormat(e);
            }
        }

        public static implicit operator string(Query qThat)
        {
            // Serialize the query and generate an xml document
            // in string form that we can persist or use however.

            try
            {

                // Serialize the query according to the xml
                // serialization api and attributes scattered
                // throughout the object model.  We force the
                // version to latest to reflect that every
                // serialization of this object meets that
                // version's requirements.

                qThat.Label.Version = c_CurrentVersion;

                return SerializationHelper.XmlSerialize(qThat);
            }
            catch (Exception e)
            {
                // Pass on the love.

                throw new InvalidFormat(e);
            }
        }

        #endregion

        [XmlElement]
        public ProcessingLabel Label
        {
            set { m_Label = value; }
            get { return m_Label; }
        }

        [XmlElement]
        public Columns Columns
        {
            set { m_Columns = value; }
            get { return m_Columns; }
        }

        [XmlElement]
        public Descriptors Sorting
        {
            set { m_Sorting = value; }
            get { return m_Sorting; }
        }

        [XmlElement]
        public Descriptors GroupBy
        {
            set { m_GroupBy = value; }
            get { return m_GroupBy; }
        }

        [XmlElement]
        public Conditions Relates
        {
            set { m_Relates = value; }
            get { return m_Relates; }
        }

        [XmlElement]
        public Conditions Filters
        {
            set { m_Filters = value; }
            get { return m_Filters; }
        }

        [XmlElement]
        public Options Options
        {
            set { m_Options = value; }
            get { return m_Options; }
        }

        [XmlElement]
        public String Details
        {
            set { m_Details = value; }
            get { return m_Details; }
        }

        [XmlElement]
        public Guid Id
        {
            set { m_Id = value; }
            get { return m_Id; }
        }

        [XmlElement]
        public bool DisplayTablesOnSeparatePages
        {
            get;
            set;
        }

        [XmlArray]
        public List<Guid> RestrictToLoanIds
        {
            get;
            set;
        }

        public bool CanUserRun(AbstractUserPrincipal bp)
        {
            //Check fields used in columns
            foreach (Column column in m_Columns)
            {
                if (!LoanFieldSecurityManager.CanReadField(column.Id, bp))
                {
                    return false;
                }
            }

            //And check fields used in conditions
            if (!m_Filters.hasFieldsAccess(bp) || !m_Relates.hasFieldsAccess(bp))
                return false;

            return true;
        }

        /// <summary>
        /// Gets the query associated with the given query id.  This is a big hit on the database since the 
        /// queries are rather large.
        /// </summary>
        /// <param name="queryId">The identifier of the query we want.</param>
        /// <returns>The query if it exist or null.</returns>
        public static Query GetQuery(Guid brokerId, Guid queryId)
        {
            Query query = null;

            SqlParameter[] parameters =
                {
                    new SqlParameter("@QueryID", queryId),
                    new SqlParameter("@BrokerID", brokerId)
                };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "GetReportQuery", parameters))
            {
                if (reader.Read())
                {
                    query = reader["XmlContent"].ToString();
                }
            }

            return query;
        }

        /// <summary>
        /// Gets the name of a query with the given query ID.
        /// </summary>
        /// <param name="brokerId">
        /// The ID of the broker for the query.
        /// </param>
        /// <param name="queryId">
        /// The ID of the query.
        /// </param>
        /// <returns>
        /// The name of the query.
        /// </returns>
        public static string GetQueryNameById(Guid brokerId, Guid queryId)
        {
            var procedureName = StoredProcedureName.Create("GetReportQueryNameByQueryId").ForceValue();
            SqlParameter[] parameters =
            {
                new SqlParameter("@QueryId", queryId)
            };

            using (var connection = DbAccessUtils.GetConnection(brokerId))
            {
                var result = StoredProcedureDriverHelper.ExecuteScalar(connection, null, procedureName, parameters, TimeoutInSeconds.Thirty);
                return result.ToString();
            }
        }

        [XmlIgnore]
        public bool hasSecureFields
        {
            get
            {
                //Check fields used in columns
                foreach (Column column in m_Columns)
                {
                    if (LoanFieldSecurityManager.IsSecureField(column.Id))
                    {
                        return true;
                    }
                }

                //And check fields used in conditions
                if (m_Filters.ContainsSecureFields || m_Relates.ContainsSecureFields)
                    return true;

                return false;
            }
        }

        /// <summary>
        /// Search current layout and return true if field is listed in
        /// the report.
        /// </summary>
        /// <param name="sFieldId">
        /// Id of field to search.
        /// </param>
        /// <returns>
        /// True if present.
        /// </returns>

        public bool HasFieldInLayout(string sFieldId)
        {
            // We search for shown matching fields in our columns.

            foreach (Column column in m_Columns)
            {
                if (column.Id == sFieldId && column.Show == true)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Gets a value indicating whether the query has encrypted fields.
        /// </summary>
        [XmlIgnore]
        public bool HasEncryptedFields => this.Columns.Has("aBSsn") || this.Columns.Has("aCSsn");

        /// <summary>
        /// Initialize this query back to a starting, empty state.
        /// </summary>

        public void Clear()
        {
            // Reset this query.

            m_Details = String.Empty;

            m_Columns.Clear();
            m_Sorting.Clear();
            m_GroupBy.Clear();
            m_Relates.Clear();
            m_Filters.Clear();
            m_Options.Clear();
        }

        /// <summary>
        /// Convert to a serialized string using implicit
        /// string transformation api.
        /// </summary>
        /// <returns>
        /// Serialized form of this query.
        /// </returns>

        public override string ToString()
        {
            // Convert to string using serialization.

            try
            {
                return this;
            }
            catch (Exception e)
            {
                // Pass on the love.

                throw e;
            }
        }

        /// <summary>
        /// Construct from source.
        /// </summary>
        /// <param name="rQuery">
        /// Source to copy.
        /// </param>

        public Query(Query rQuery)
            : this()
        {
            // Initialize members.

            m_Relates = new Conditions(rQuery.m_Relates);
            m_Filters = new Conditions(rQuery.m_Filters);

            foreach (Column item in rQuery.m_Columns)
            {
                m_Columns.Add(new Column(item));
            }

            foreach (Descriptor item in rQuery.m_Sorting)
            {
                m_Sorting.Add(new Descriptor(item));
            }

            foreach (Descriptor item in rQuery.m_GroupBy)
            {
                m_GroupBy.Add(new Descriptor(item));
            }

            foreach (string item in rQuery.Options)
            {
                m_Options.Add(item);
            }

            m_Label.Version = rQuery.m_Label.Version;
            m_Label.Title = rQuery.m_Label.Title;

            m_Details = "";

            m_Id = rQuery.m_Id;
        }

        /// <summary>
        /// Allocate default query.
        /// </summary>

        public Query()
        {
            // Initialize members.

            m_Label = new ProcessingLabel();
            m_Columns = new Columns();
            m_Sorting = new Descriptors();
            m_GroupBy = new Descriptors();
            m_Relates = new Conditions();
            m_Filters = new Conditions();
            m_Options = new Options();

            m_Relates.Type = E_ConditionsType.An;
            m_Filters.Type = E_ConditionsType.An;

            m_Label.Version = c_CurrentVersion;

            m_Details = "";

            m_Id = Guid.Empty;
        }
    }

    /// <summary>
    /// Keep notes on each row's derived access level.  When a
    /// specialized reporting engine pulls records and stores them
    /// in a report (as part of tables), we leave an access level
    /// hook for all users to derive some ui to access.
    /// </summary>

    public enum E_RowAccessLevel
    {
        None = 0,
        Read = 1,
        Edit = 2,
        NotChecked = 3,
    }

    /// <summary>
    /// Collect access level details for security and debugging.
    /// </summary>

    public class RowAccessSpec
    {
        /// <summary>
        /// Collect access level details for security and
        /// debugging.
        /// </summary>

        private String m_Comment = String.Empty;
        private E_RowAccessLevel m_Level = E_RowAccessLevel.NotChecked;

        public String Comment
        {
            set { m_Comment = value; }
            get { return m_Comment; }
        }

        public E_RowAccessLevel Level
        {
            set { m_Level = value; }
            get { return m_Level; }
        }
    }

    /// <summary>
    /// We maintain rows in tables, and tables within the result.
    /// Each row's length is based on the number of shown columns
    /// found in the generating query.  Columns kept elsewhere.
    /// See <see cref="T:LendersOffice.QueryProcessor.Report"/>
    /// for more information.
    /// 
    /// 8/16/2005 kb - Added comments to each row so that access
    /// control results can be quickly debugged.
    /// </summary>

    public class Row
    {
        /// <summary>
        /// Array of values
        /// 
        /// We create this array at construction, using the number
        /// of shown report columns as our length.
        /// </summary>

        private Value[] m_Set; // dynamically allocated array of values
        private Guid m_Key; // associated object key for binding
        private Int32 m_Arg; // associated object arg for binding
        private Int32 m_Hits; // object contribution count
        private String m_Comment; // generic comment for high level rendering
        private E_RowAccessLevel m_Level; // derived access level for ui rendering

        [XmlIgnore]
        public Value this[Int32 iValue]
        {
            set { m_Set[iValue] = value; }
            get { return m_Set[iValue]; }
        }

        [XmlArray]
        [XmlArrayItem(typeof(Value))]
        public Value[] Items
        {
            set { m_Set = value; }
            get { return m_Set; }
        }

        [XmlElement]
        public Guid Key
        {
            set { m_Key = value; }
            get { return m_Key; }
        }

        [XmlElement]
        public Int32 Arg
        {
            set { m_Arg = value; }
            get { return m_Arg; }
        }

        [XmlElement]
        public Int32 Hits
        {
            set { m_Hits = value; }
            get { return m_Hits; }
        }

        [XmlElement]
        public String Comment
        {
            set { m_Comment = value; }
            get { return m_Comment; }
        }

        [XmlElement]
        public E_RowAccessLevel Level
        {
            set { m_Level = value; }
            get { return m_Level; }
        }

        [XmlIgnore]
        public Int32 Count
        {
            get { return m_Set.Length; }
        }

        /// <summary>
        /// Return the key of this row for default binding to the
        /// ui's grids.
        /// </summary>

        public override String ToString()
        {
            // Return the key for default binding.

            return m_Key.ToString();
        }

        /// <summary>
        /// Construct copy of specified row.
        /// </summary>
        /// <param name="rThat">
        /// Row to copy.
        /// </param>

        public Row(Row rThat)
        {
            // Allocate new array.

            m_Set = new Value[rThat.m_Set.Length];

            for (int i = 0; i < m_Set.Length; ++i)
            {
                // Copy value object one at a time.

                m_Set[i] = new Value(rThat.m_Set[i]);
            }

            m_Level = rThat.m_Level;

            m_Comment = rThat.Comment;

            m_Hits = rThat.m_Hits;

            m_Key = rThat.m_Key;
            m_Arg = rThat.m_Arg;
        }

        /// <summary>
        /// Construct dynamic row.  We allocate in one shot, as
        /// opposed to adding with successive operations, to keep
        /// the overhead of new and delete to a minimum.
        /// </summary>
        /// <param name="nLength">
        /// Array count.  Must be greater than zero.
        /// </param>

        public Row(Int32 nLength, Int32 nHits)
        {
            // Allocate objects.

            m_Level = E_RowAccessLevel.NotChecked;

            m_Comment = String.Empty;

            m_Key = Guid.Empty;
            m_Arg = -1;

            if (nLength > 0)
            {
                // Create array and add valid instances.

                m_Set = new Value[nLength];

                for (int i = 0; i < nLength; ++i)
                {
                    m_Set[i] = new Value();
                }
            }
            else
            {
                m_Set = new Value[0];
            }

            m_Hits = nHits;
        }

        /// <summary>
        /// Construct dynamic row.  We allocate in one shot, as
        /// opposed to adding with successive operations, to keep
        /// the overhead of new and delete to a minimum.
        /// </summary>
        /// <param name="nLength">
        /// Array count.  Must be greater than zero.
        /// </param>

        public Row(Int32 nLength)
        {
            // Allocate objects.

            m_Level = E_RowAccessLevel.NotChecked;

            m_Comment = String.Empty;

            m_Key = Guid.Empty;
            m_Arg = -1;

            if (nLength > 0)
            {
                // Create array and add valid instances.

                m_Set = new Value[nLength];

                for (int i = 0; i < nLength; ++i)
                {
                    m_Set[i] = new Value();
                }
            }
            else
            {
                m_Set = new Value[0];
            }

            m_Hits = 0;
        }

        /// <summary>
        /// Construct empty row.
        /// </summary>

        public Row()
        {
            // Initialize members.

            m_Level = E_RowAccessLevel.NotChecked;

            m_Comment = String.Empty;

            m_Set = new Value[0];
            m_Key = Guid.Empty;
            m_Arg = -1;
            m_Hits = 0;
        }

    }

    /// <summary>
    /// We maintain a list of ordered indices into some
    /// arbitrary set.  This is useful when you need
    /// placeholders for field positions within a row.
    /// See <see cref="T:LendersOffice.QueryProcessor.Report"/>
    /// for more information.
    /// </summary>

    public class LookupArray
    {
        /// <summary>
        /// Index set
        /// 
        /// We maintain a list of ordered indices into some
        /// arbitrary set.  This is useful when you need
        /// placeholders for field positions within a row.
        /// </summary>

        private ArrayList m_Set = new ArrayList();

        [XmlIgnore
       ]
        public Int32 Last
        {
            // Access member.

            get
            {
                return (Int32)m_Set[m_Set.Count - 1];
            }
        }

        [XmlIgnore]
        public int Count
        {
            get { return m_Set.Count; }
        }

        /// <summary>
        /// Append onto the list.
        /// </summary>
        /// <param name="iIndex">
        /// Index to store.
        /// </param>

        public void Add(Int32 iIndex)
        {
            // Add onto the list.  Check for duplicates first.

            foreach (Int32 item in m_Set)
            {
                if (item == iIndex)
                {
                    return;
                }
            }

            m_Set.Add(iIndex);
        }

        /// <summary>
        /// Search for a match and return true if found.
        /// </summary>

        public bool Has(Int32 iIndex)
        {
            // Search for a match and return true if found.

            foreach (Int32 item in m_Set)
            {
                if (item == iIndex)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Delegate to set and retrieve accessor.
        /// </summary>
        /// <returns>
        /// Enumeration object.
        /// </returns>

        public IEnumerator GetEnumerator()
        {
            // Get accessor to list.

            return m_Set.GetEnumerator();
        }

    }

    /// <summary>
    /// We maintain a list of ordered indices into some
    /// arbitrary set.  This is useful when you need
    /// placeholders for field positions within a row.
    /// Key point about this stack is that indices come
    /// out in reverse order.  Sorting is the only use
    /// of this kind of lookup list at this time.
    /// See <see cref="T:LendersOffice.QueryProcessor.Report"/>
    /// for more information.
    /// </summary>

    public class LookupStack
    {
        /// <summary>
        /// Index set
        /// 
        /// We maintain a list of ordered indices into some
        /// arbitrary set.  This is useful when you need
        /// placeholders for field positions within a row.
        /// </summary>

        private Stack m_Set = new Stack();

        public int Count
        {
            get { return m_Set.Count; }
        }

        /// <summary>
        /// Append onto the list.
        /// </summary>
        /// <param name="iIndex">
        /// Index to store.
        /// </param>

        public void Add(int iIndex)
        {
            // Add onto the list.  Check for duplicates first.

            foreach (int item in m_Set)
            {
                if (item == iIndex)
                {
                    return;
                }
            }

            m_Set.Push(iIndex);
        }

        /// <summary>
        /// Delegate to set and retrieve accessor.
        /// </summary>
        /// <returns>
        /// Enumeration object.
        /// </returns>

        public IEnumerator GetEnumerator()
        {
            // Get accessor to list.

            return m_Set.GetEnumerator();
        }

    }

    /// <summary>
    /// We keep a list of tagged object positions to
    /// form a collected key for specific objects.
    /// See <see cref="T:LendersOffice.QueryProcessor.Report"/>
    /// for more information.
    /// </summary>

    public class LookupValue
    {
        /// <summary>
        /// Group by descriptor
        /// 
        /// We keep a list of tagged object positions to
        /// form a collected key for specific objects.
        /// </summary>

        private int m_Index;
        private string m_Value;

        [XmlElement]
        public int Index
        {
            set { m_Index = value; }
            get { return m_Index; }
        }

        [XmlElement]
        public string Value
        {
            set { m_Value = value; }
            get { return m_Value; }
        }

        /// <summary>
        /// Construct copy of lookup value.
        /// </summary>
        /// <param name="lThat">
        /// Source to copy from.
        /// </param>

        public LookupValue(LookupValue lThat)
            : this()
        {
            // Initialize members.

            m_Index = lThat.m_Index;
            m_Value = lThat.m_Value;
        }

        /// <summary>
        /// Construct lookup value.
        /// </summary>
        /// <param name="iIndex">
        /// Index of particular object.
        /// </param>
        /// <param name="sValue">
        /// Value tag at this particular reference.
        /// </param>

        public LookupValue(int iIndex, string sValue)
        {
            // Initialize members.

            m_Index = iIndex;
            m_Value = sValue;
        }

        /// <summary>
        /// Construct lookup value.
        /// </summary>

        public LookupValue()
        {
            // Initialize members.

            m_Index = -1;
            m_Value = "";
        }

    }

    /// <summary>
    /// We keep a list of tagged object positions to
    /// form a collected key for specific objects.
    /// See <see cref="T:LendersOffice.QueryProcessor.Report"/>
    /// for more information.
    /// </summary>

    public class LookupLabel
    {
        /// <summary>
        /// Group by descriptor
        /// 
        /// We keep a list of tagged object positions to
        /// form a collected key for specific objects.
        /// </summary>

        private string m_Label;
        private string m_Value;

        [XmlElement]
        public string Label
        {
            set { m_Label = value; }
            get { return m_Label; }
        }

        [XmlElement]
        public string Value
        {
            set { m_Value = value; }
            get { return m_Value; }
        }

        /// <summary>
        /// Construct copy of lookup value.
        /// </summary>
        /// <param name="lThat">
        /// Source to copy from.
        /// </param>

        public LookupLabel(LookupLabel lThat)
            : this()
        {
            // Initialize members.

            m_Label = lThat.m_Label;
            m_Value = lThat.m_Value;
        }

        /// <summary>
        /// Construct lookup label.
        /// </summary>
        /// <param name="sLabel">
        /// Label of particular object.
        /// </param>
        /// <param name="sValue">
        /// Value tag at this particular reference.
        /// </param>

        public LookupLabel(string sLabel, string sValue)
        {
            // Initialize members.

            m_Label = sLabel;
            m_Value = sValue;
        }

        /// <summary>
        /// Construct lookup label.
        /// </summary>

        public LookupLabel()
        {
            // Initialize members.

            m_Label = "";
            m_Value = "";
        }

    }

    /// <summary>
    /// Each report table has zero or more group by
    /// constraints that govern what is listed.
    /// See <see cref="T:LendersOffice.QueryProcessor.Report"/>
    /// for more information.
    /// </summary>

    public class LookupGroup
    {
        /// <summary>
        /// Array elements
        /// 
        /// We store each group by item by reference in this
        /// set.
        /// </summary>

        private ArrayList m_Set; // array of items

        [XmlIgnore]
        public LookupLabel this[int iItem]
        {
            get
            {
                return m_Set[iItem] as LookupLabel;
            }
        }

        [XmlArray]
        [XmlArrayItem(typeof(LookupLabel))]
        public ArrayList Items
        {
            set { m_Set = value; }
            get { return m_Set; }
        }

        [XmlIgnore]
        public int Count
        {
            get { return m_Set.Count; }
        }

        /// <summary>
        /// Look for existing entry that matches.
        /// </summary>
        /// <param name="sLabel">
        /// Item label to match against.
        /// </param>
        /// <param name="sValue">
        /// Item value to match against.
        /// </param>
        /// <returns>
        /// True if found, else false.
        /// </returns>

        public bool Has(string sLabel, string sValue)
        {
            // Search for exact match entry.

            foreach (LookupLabel group in m_Set)
            {
                if (group.Label == sLabel && group.Value == sValue)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Look for existing entry that matches.
        /// </summary>
        /// <param name="sValue">
        /// Item value to match against.
        /// </param>
        /// <returns>
        /// True if found, else false.
        /// </returns>

        public bool Has(string sValue)
        {
            // Search for exact match entry.

            foreach (LookupLabel group in m_Set)
            {
                if (group.Value == sValue)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Append to this set if value is unique.
        /// </summary>
        /// <param name="sLabel">
        /// Label to append to set.
        /// </param>
        /// <param name="sValue">
        /// Value to append to set.
        /// </param>

        public void Add(string sLabel, string sValue)
        {
            // Append to this set if unique.

            Add(new LookupLabel(sLabel, sValue));
        }

        /// <summary>
        /// Append to this set if value is unique.
        /// </summary>
        /// <param name="gLabel">
        /// Value to append to the set.
        /// </param>

        public void Add(LookupLabel gLabel)
        {
            // Append to this set if unique.

            foreach (LookupLabel group in m_Set)
            {
                if (group.Label == gLabel.Label && group.Value == gLabel.Value)
                {
                    return;
                }
            }

            m_Set.Add(gLabel);
        }

        /// <summary>
        /// Expose enumeration interface.
        /// </summary>
        /// <returns>
        /// Enumeration object.
        /// </returns>

        public IEnumerator GetEnumerator()
        {
            // Delegate to embedded type.

            return m_Set.GetEnumerator();
        }

        /// <summary>
        /// Expose comparison interface.
        /// </summary>
        /// <param name="lgThat">
        /// Group to compare this object against.
        /// </param>
        /// <returns>
        /// True if same, else false.
        /// </returns>

        public bool CompareTo(LookupGroup lgThat)
        {
            // Match set against this set.

            foreach (LookupLabel group in lgThat)
            {
                if (Has(group.Label, group.Value) == false)
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Construct with an empty set.
        /// </summary>

        public LookupGroup()
        {
            // Initialize members.

            m_Set = new ArrayList();
        }

    }

    /// <summary>
    /// We need to track value positions by field identifier.
    /// See <see cref="T:LendersOffice.QueryProcessor.Report"/>
    /// for more information.
    /// </summary>

    public class LookupTable
    {
        /// <summary>
        /// We lookup ids and return absolute index offsets.
        /// </summary>

        private Hashtable m_Table; // lookup positions dictionary

        [XmlIgnore]
        public int this[string sId]
        {
            get
            {
                object index = m_Table[sId];

                if (index != null)
                {
                    return (int)index;
                }

                return -1;
            }
        }

        [XmlIgnore]
        public int Count
        {
            get { return m_Table.Count; }
        }

        /// <summary>
        /// Insert new labeled position into lookup table.  If the
        /// id already exists, we update the stored position.
        /// </summary>
        /// <param name="sId">
        /// Unique identifier key.
        /// </param>
        /// <param name="iPosition">
        /// Position of object.
        /// </param>

        public void Add(string sId, int iPosition)
        {
            // Add to the set.  If existing, then we overwrite.

            m_Table.Add(sId, iPosition);
        }

        /// <summary>
        /// Perform existence check on lookup table.
        /// </summary>
        /// <param name="sId">
        /// Field id to search with.
        /// </param>
        /// <returns>
        /// True if found within the set.
        /// </returns>

        public bool Has(string sId)
        {
            // Check for existing entry in the list.

            if (m_Table.Contains(sId) == true)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Construct default table.
        /// </summary>

        public LookupTable()
        {
            // Initialize members.

            m_Table = new Hashtable();
        }

    }

    /// <summary>
    /// Track calculations that make up a report.
    /// See <see cref="T:LendersOffice.QueryProcessor.Report"/>
    /// </summary>

    public class CalculationResult
    {
        /// <summary>
        /// Field details
        /// 
        /// Track fields that make up a report.
        /// </summary>

        private int m_Column; // field position
        private double m_Result; // calculation result
        private int m_Hits; // contribution count
        private E_FunctionType m_What; // result kind

        [XmlElement]
        public int Column
        {
            set { m_Column = value; }
            get { return m_Column; }
        }

        [XmlElement]
        public double Result
        {
            set { m_Result = value; }
            get { return m_Result; }
        }

        [XmlElement]
        public int Hits
        {
            set { m_Hits = value; }
            get { return m_Hits; }
        }

        [XmlElement]
        public E_FunctionType What
        {
            set { m_What = value; }
            get { return m_What; }
        }

        [XmlElement]
        public bool FatalError { get; set; }

        /// <summary>
        /// Merge given result into this one based on the function
        /// and current value with contribution count.  Update this
        /// handler when new function types show up.  If types mis-
        /// match, we just ignore the request.  At some point,
        /// mixing different function types may make sense.  Note:
        /// we don't check column positions.  Maybe we should?
        /// </summary>

        public void Combine(Double dValue, Int32 nHits)
        {
            // Use matching function and combine the give result
            // with our current result.  We assume that our functions
            // are linear (that is, we can add two together), and
            // that holds for almost all arithmetic operations.  The
            // average function is a little trickier, but we use a
            // hits count to do the combination properly.

            if (m_Hits != 0)
            {
                switch (m_What)
                {
                    case E_FunctionType.Av:
                        {
                            m_Result = (m_Result * m_Hits + dValue * nHits) / (m_Hits + nHits);
                        }
                        break;

                    case E_FunctionType.Mn:
                        {
                            if (m_Result > dValue)
                            {
                                m_Result = dValue;
                            }
                        }
                        break;

                    case E_FunctionType.Mx:
                        {
                            if (m_Result < dValue)
                            {
                                m_Result = dValue;
                            }
                        }
                        break;

                    case E_FunctionType.Sm:
                    case E_FunctionType.Ct:
                        {
                            m_Result += dValue;
                        }
                        break;
                }
            }
            else
            {
                m_Result = dValue;
            }

            m_Hits += nHits;
        }

        /// <summary>
        /// Merge given result into this one based on the function
        /// and current value with contribution count.  Update this
        /// handler when new function types show up.  If types mis-
        /// match, we just ignore the request.  At some point,
        /// mixing different function types may make sense.  Note:
        /// we don't check column positions.  Maybe we should?
        /// </summary>

        public void Combine(CalculationResult cThat)
        {
            // Match up functions and delegate to combination
            // handler.

            if (m_What == cThat.m_What)
            {
                if (cThat.FatalError)
                {
                    this.FatalError = true;
                    this.Hits = 0;
                    this.Result = 0;
                }
                else
                {
                    Combine(cThat.m_Result, cThat.m_Hits);
                }
            }
        }

        /// <summary>
        /// Construct copy of given calculation result.
        /// </summary>
        /// <param name="cThat">
        /// Source to copy.
        /// </param>

        public CalculationResult(CalculationResult cThat)
        {
            // Initialize members.

            m_Column = cThat.m_Column;
            m_Result = cThat.m_Result;
            m_Hits = cThat.m_Hits;
            m_What = cThat.m_What;
            this.FatalError = cThat.FatalError;
        }

        /// <summary>
        /// Construct a calculation result.
        /// </summary>
        /// <param name="iColumn">
        /// Position of generating column
        /// </param>
        /// <param name="dResult">
        /// Calculated result to stash.
        /// </param>
        /// <param name="eWhat">
        /// Kind of result calculated.
        /// </param>

        public CalculationResult(int iColumn, double dResult, int nHits, E_FunctionType eWhat, bool fatalError)
        {
            // Initialize members.

            m_Column = iColumn;
            m_Result = dResult;
            m_Hits = nHits;
            m_What = eWhat;
            this.FatalError = fatalError;
        }

        /// <summary>
        /// Construct a default calculation result
        /// </summary>

        public CalculationResult()
        {
            // Initialize members.

            m_What = E_FunctionType.Invalid;

            m_Column = 0;
            m_Result = 0;

            m_Hits = 0;
        }

        public string GetFormattedResult(string format)
        {
            if (this.FatalError)
            {
                return "ERROR";
            }

            return this.Result.ToString(format);
        }
    }

    /// <summary>
    /// Track query calculations that make up a report.
    /// See <see cref="T:LendersOffice.QueryProcessor.Report"/>
    /// </summary>

    public class CalculationResults
    {
        /// <summary>
        /// Array elements
        /// 
        /// We store each field result item by reference in this
        /// set.
        /// </summary>

        private ArrayList m_Set; // array of items

        [XmlIgnore]
        public CalculationResult this[int iResult]
        {
            get
            {
                return m_Set[iResult] as CalculationResult;
            }
        }

        [XmlArray]
        [XmlArrayItem(typeof(CalculationResult))]
        public ArrayList Items
        {
            set { m_Set = value; }
            get { return m_Set; }
        }

        [XmlIgnore]
        public int Count
        {
            get { return m_Set.Count; }
        }

        /// <summary>
        /// Append new calculation result.
        /// </summary>
        /// <param name="iColumn">
        /// Corresponding field column.
        /// </param>
        /// <param name="dResult">
        /// Value of the result.
        /// </param>
        /// <param name="eWhat">
        /// Type of the result.
        /// </param>

        public void Add(int iColumn, double dResult, int nHits, E_FunctionType eWhat, bool fatalError)
        {
            // Delegate to implementation.

            Add(new CalculationResult(iColumn, dResult, nHits, eWhat, fatalError));
        }

        /// <summary>
        /// Append new calculation result.
        /// </summary>
        /// <param name="cResult">
        /// Calculation result to add.
        /// </param>

        public void Add(CalculationResult cResult)
        {
            // Append to this set.  We insert in order according
            // to column rank.
            //
            // 1/7/2005 kb - I added sorting here because I needed
            // to get the calculations to line up according to
            // layout when showing the cross-tab results.  If the
            // ordering is a problem, we need to defer sorting to
            // when the cross-tab generator creates the new column
            // layout based on the report's calculation list.

            for (int i = 0; i < m_Set.Count; ++i)
            {
                CalculationResult rItem = m_Set[i] as CalculationResult;

                if (cResult.Column < rItem.Column)
                {
                    m_Set.Insert(i, cResult);

                    return;
                }
            }

            m_Set.Add(cResult);
        }

        /// <summary>
        /// Exposes enumeration interface.
        /// </summary>
        /// <returns>
        /// Iteration interface.
        /// </returns>

        public IEnumerator GetEnumerator()
        {
            // Delegate to embedded type.

            return m_Set.GetEnumerator();
        }

        /// <summary>
        /// Construct with an empty set.
        /// </summary>

        public CalculationResults()
        {
            // Initialize members.

            m_Set = new ArrayList();
        }

    }

    /// <summary>
    /// A report is composed of one or more tables.  Each table
    /// contains one or more rows.  We add rows one at a time.
    /// What we want to minimize is excessive object allocation.
    /// See <see cref="T:LendersOffice.QueryProcessor.Report"/>
    /// for more information.
    /// </summary>

    public class Rows
    {
        /// <summary>
        /// Table rows
        /// 
        /// We want to add rows that are generated by the
        /// processor during query processing.  To minimize
        /// over allocating and throwing away, we need to
        /// only allocate a new row when we are adding to
        /// a report table.  We plan that the processor
        /// forms a master view table before cutting into
        /// grouped tables.  Thus, we add a reference of an
        /// existing row into the table to limit allocation.
        /// We also cache the calculation results of each
        /// function applied to this table.
        /// 
        /// 5/13/2005 kb - Rows are now composite, containing
        /// other tables or rows.  Though mixed-mode tables
        /// are possible, they don't make sense.
        /// </summary>

        private CalculationResults m_Results; // function results
        private LookupGroup m_GroupBy; // group by specs
        private ArrayList m_Set; // array of items

        [XmlElement]
        public CalculationResults Results
        {
            set { m_Results = value; }
            get { return m_Results; }
        }

        [XmlElement]
        public LookupGroup GroupBy
        {
            set { m_GroupBy = value; }
            get { return m_GroupBy; }
        }

        [XmlArray]
        [XmlArrayItem(typeof(Rows))]
        [XmlArrayItem(typeof(Row))]
        public ArrayList Items
        {
            set { m_Set = value; }
            get { return m_Set; }
        }

        [XmlIgnore]
        public Row this[int iRow]
        {
            get { return m_Set[iRow] as Row; }
        }

        [XmlIgnore]
        public int Count
        {
            get { return m_Set.Count; }
        }

        /// <summary>
        /// Recursively descend into each table and gather all the
        /// child table's results.  If not found, then we make a
        /// new 
        /// </summary>

        public void Combine(Int32 iColumn, E_FunctionType eWhat)
        {
            // Calculate a new result from the children.  We descend
            // into the children to make sure we have a result.  If
            // not currently defined within a child table, then we
            // will end up propagating zero.

            CalculationResult result = new CalculationResult(iColumn, 0, 0, eWhat, fatalError: false);

            foreach (Object o in m_Set)
            {
                Rows r = o as Rows;

                if (r != null)
                {
                    r.Combine(iColumn, eWhat);

                    foreach (CalculationResult c in r.m_Results)
                    {
                        if (c.Column == iColumn && c.What == eWhat)
                        {
                            result.Combine(c);

                            break;
                        }
                    }
                }
                else
                {
                    return;
                }
            }

            foreach (CalculationResult c in m_Results)
            {
                if (c.Column == iColumn && c.What == eWhat)
                {
                    c.Result = result.Result;
                    c.Hits = result.Hits;
                    c.FatalError = result.FatalError;

                    return;
                }
            }

            m_Results.Add(result);
        }

        /// <summary>
        /// Calculate the average numerical value for this field.
        /// </summary>
        /// <param name="iColumn">
        /// Field with which to perform calculation.
        /// </param>

        public void AvgVal(int iColumn)
        {
            // Walk the table and average the values associated with
            // the specified field.  Only numerical fields are used
            // in the calculation.  Non-numerical fields will yield
            // an average of zero.  We assume a valid column.

            double result = 0;
            int nitems = 0;
            bool error = false;

            for (int i = 0; i < m_Set.Count; ++i)
            {
                // Get the value and add it to the pot.

                if (m_Set[i] is Rows)
                {
                    Rows r = m_Set[i] as Rows;

                    r.AvgVal(iColumn);

                    foreach (CalculationResult c in r.Results)
                    {
                        // Find result and combine with this result using
                        // intermediate arithmetic.

                        if (c.Column == iColumn && c.What == E_FunctionType.Av)
                        {
                            result += c.Result * c.Hits;

                            nitems += c.Hits;

                            error |= c.FatalError;

                            break;
                        }
                    }
                }
                else
                {
                    Row r = m_Set[i] as Row;

                    try
                    {
                        if (r[iColumn].FatalError)
                        {
                            error = true;
                            continue;
                        }

                        // Contribute to the calculation.
                        //
                        // Failure is ok -- take it as 0.

                        double c = r[iColumn].Real;

                        result += c;

                        ++nitems;
                    }
                    catch
                    {
                        continue;
                    }
                }
            }

            if (nitems > 0)
            {
                result /= nitems;
            }

            // Save the result in our aggregation stash.
            // We typically display this calculation at
            // the end of the table once it has been
            // displayed.  All these calculations are kept
            // in the results array for that purpose.  Note
            // that fields may participate in multiple
            // aggregation calculations.

            m_Results.Add(iColumn, result, nitems, E_FunctionType.Av, error);
        }

        /// <summary>
        /// Calculate the total numerical value for this field.
        /// </summary>
        /// <param name="iColumn">
        /// Field with which to perform calculation.
        /// </param>

        public void SumTot(int iColumn)
        {
            // Walk the table and total the values associated with
            // the specified field.  Only numerical fields are used
            // in the calculation.
            //
            // 11/30/2004 kb - We have made room for non-numerical
            // fields by letting non-null items count as one.

            double result = 0;
            int nitems = 0;
            bool error = false;

            for (int i = 0; i < m_Set.Count; ++i)
            {
                // Get the value and add it to the pot.

                if (m_Set[i] is Rows)
                {
                    Rows r = m_Set[i] as Rows;

                    r.SumTot(iColumn);

                    foreach (CalculationResult c in r.Results)
                    {
                        // Find result and combine with this result using
                        // intermediate arithmetic.

                        if (c.Column == iColumn && c.What == E_FunctionType.Sm)
                        {
                            result += c.Result;

                            nitems += c.Hits;

                            error |= c.FatalError;

                            break;
                        }
                    }
                }
                else
                {
                    Row r = m_Set[i] as Row;

                    try
                    {
                        if (r[iColumn].FatalError)
                        {
                            error = true;
                            continue;
                        }

                        // Contribute to the calculation.
                        //
                        // Failure is ok -- take it as 0.

                        double c = 0.0;

                        if (r[iColumn].Type == Field.E_FieldType.Dec || r[iColumn].Type == Field.E_FieldType.Dbl || r[iColumn].Type == Field.E_FieldType.Int)
                        {
                            c = r[iColumn].Real;
                        }
                        else
                            if (r[iColumn].Data != null)
                            {
                                c = 1.0;
                            }

                        result += c;

                        ++nitems;
                    }
                    catch
                    {
                        continue;
                    }
                }
            }

            // Save the result in our aggregation stash.
            // We typically display this calculation at
            // the end of the table once it has been
            // displayed.  All these calculations are kept
            // in the results array for that purpose.  Note
            // that fields may participate in multiple
            // aggregation calculations.

            m_Results.Add(iColumn, result, nitems, E_FunctionType.Sm, error);
        }

        /// <summary>
        /// Calculate the total count value for this field.  All
        /// non-empty values contribute 1 to the total.
        /// </summary>
        /// <param name="iColumn">
        /// Field with which to perform calculation.
        /// </param>

        public void NumTot(int iColumn)
        {
            // Walk the table and total the values associated with
            // the specified field.  All non-empty values contribute
            // exactly one to the count.

            double result = 0;
            int nitems = 0;
            bool error = false;

            for (int i = 0; i < m_Set.Count; ++i)
            {
                // Get the value and add it to the pot.

                if (m_Set[i] is Rows)
                {
                    Rows r = m_Set[i] as Rows;

                    r.NumTot(iColumn);

                    foreach (CalculationResult c in r.Results)
                    {
                        // Find result and combine with this result using
                        // intermediate arithmetic.

                        if (c.Column == iColumn && c.What == E_FunctionType.Ct)
                        {
                            result += c.Result;

                            nitems += c.Hits;

                            error |= c.FatalError;

                            break;
                        }
                    }
                }
                else
                {
                    Row r = m_Set[i] as Row;

                    try
                    {
                        if (r[iColumn].FatalError)
                        {
                            error = true;
                            continue;
                        }

                        // Contribute to the calculation.
                        //
                        // Failure is ok -- take it as 0.

                        double c = 0.0;

                        if (r[iColumn].Data != null)
                        {
                            c = 1.0;
                        }

                        result += c;

                        ++nitems;
                    }
                    catch
                    {
                        continue;
                    }
                }
            }

            // Save the result in our aggregation stash.
            // We typically display this calculation at
            // the end of the table once it has been
            // displayed.  All these calculations are kept
            // in the results array for that purpose.  Note
            // that fields may participate in multiple
            // aggregation calculations.

            m_Results.Add(iColumn, result, nitems, E_FunctionType.Ct, error);
        }

        /// <summary>
        /// Calculate the minimal numerical value for this field.
        /// </summary>
        /// <param name="iColumn">
        /// Field with which to perform calculation.
        /// </param>

        public void MinVal(int iColumn)
        {
            // Walk the table and minimum the values associated with
            // the specified field.  Only numerical fields are used
            // in the calculation.  Non-numerical fields will yield
            // a minimum of zero.  We assume a valid column.

            double result = 0;
            int nitems = 0;
            bool error = false;

            for (int i = 0; i < m_Set.Count; ++i)
            {
                // Get the value and add it to the pot.

                if (m_Set[i] is Rows)
                {
                    Rows r = m_Set[i] as Rows;

                    r.MinVal(iColumn);

                    foreach (CalculationResult c in r.Results)
                    {
                        // Find result and combine with this result using
                        // intermediate arithmetic.

                        if (c.Column == iColumn && c.What == E_FunctionType.Mn)
                        {
                            if (nitems == 0 || c.Result < result)
                            {
                                result = c.Result;
                            }

                            nitems += c.Hits;

                            error |= c.FatalError;

                            break;
                        }
                    }
                }
                else
                {
                    Row r = m_Set[i] as Row;

                    try
                    {
                        if (r[iColumn].FatalError)
                        {
                            error = true;
                            continue;
                        }

                        // Contribute to the calculation.
                        //
                        // Failure is ok -- take it as 0.

                        double c = r[iColumn].Real;

                        if (nitems == 0 || c < result)
                        {
                            result = c;
                        }

                        ++nitems;
                    }
                    catch
                    {
                        continue;
                    }
                }
            }

            // Save the result in our aggregation stash.
            // We typically display this calculation at
            // the end of the table once it has been
            // displayed.  All these calculations are kept
            // in the results array for that purpose.  Note
            // that fields may participate in multiple
            // aggregation calculations.

            m_Results.Add(iColumn, result, nitems, E_FunctionType.Mn, error);
        }

        /// <summary>
        /// Calculate the maximal numerical value for this field.
        /// </summary>
        /// <param name="iColumn">
        /// Field with which to perform calculation.
        /// </param>

        public void MaxVal(int iColumn)
        {
            // Walk the table and maximum the values associated with
            // the specified field.  Only numerical fields are used
            // in the calculation.  Non-numerical fields will yield
            // a maximum of zero.  We assume a valid column.

            double result = 0;
            int nitems = 0;
            bool error = false;

            for (int i = 0; i < m_Set.Count; ++i)
            {
                // Get the value and add it to the pot.

                if (m_Set[i] is Rows)
                {
                    Rows r = m_Set[i] as Rows;

                    r.MaxVal(iColumn);

                    foreach (CalculationResult c in r.Results)
                    {
                        // Find result and combine with this result using
                        // intermediate arithmetic.

                        if (c.Column == iColumn && c.What == E_FunctionType.Mx)
                        {
                            if (nitems == 0 || c.Result > result)
                            {
                                result = c.Result;
                            }

                            nitems += c.Hits;

                            error |= c.FatalError;

                            break;
                        }
                    }
                }
                else
                {
                    Row r = m_Set[i] as Row;

                    try
                    {
                        if (r[iColumn].FatalError)
                        {
                            error = true;
                            continue;
                        }

                        // Contribute to the calculation.
                        //
                        // Failure is ok -- take it as 0.

                        double c = r[iColumn].Real;

                        if (nitems == 0 || c > result)
                        {
                            result = c;
                        }

                        ++nitems;
                    }
                    catch
                    {
                        continue;
                    }
                }
            }

            // Save the result in our aggregation stash.
            // We typically display this calculation at
            // the end of the table once it has been
            // displayed.  All these calculations are kept
            // in the results array for that purpose.  Note
            // that fields may participate in multiple
            // aggregation calculations.

            m_Results.Add(iColumn, result, nitems, E_FunctionType.Mx, error);
        }

        /// <summary>
        /// Append to the tail with the rows.
        /// </summary>
        /// <param name="rRow">
        /// Table to append.
        /// </param>

        public void Add(Rows rRows)
        {
            // Add onto the end with a reference.

            m_Set.Add(rRows);
        }

        /// <summary>
        /// Append to the tail with the row.
        /// </summary>
        /// <param name="rRow">
        /// Row to append.
        /// </param>

        public void Add(Row rRow)
        {
            // Add onto the end with a reference.

            m_Set.Add(rRow);
        }

        /// <summary>
        /// Nix all rows from this table.
        /// </summary>

        public void Clear()
        {
            // Nix all entries.  This should tell the gc to
            // cleanup our rows once they're gone.

            m_Set.Clear();
        }

        /// <summary>
        /// Fetch iterator from embedded set.
        /// </summary>
        /// <returns>
        /// Return enumeration interface.
        /// </returns>

        public IEnumerator GetEnumerator()
        {
            // Retrieve enumeration interface.

            return m_Set.GetEnumerator();
        }

        /// <summary>
        /// Construct copy of source.
        /// </summary>

        public Rows(Rows rThat)
            : this()
        {
            // Initialize members.

            foreach (CalculationResult item in rThat.m_Results)
            {
                m_Results.Add(new CalculationResult(item));
            }

            foreach (LookupLabel item in rThat.m_GroupBy)
            {
                m_GroupBy.Add(new LookupLabel(item));
            }

            foreach (Object item in rThat.m_Set)
            {
                if (item is Rows)
                {
                    m_Set.Add(new Rows(item as Rows));
                }

                if (item is Row)
                {
                    m_Set.Add(new Row(item as Row));
                }
            }
        }

        /// <summary>
        /// Construct grouped rows.
        /// </summary>
        /// <param name="lGroupBy">
        /// Grouping specs.
        /// </param>

        public Rows(LookupGroup lGroupBy)
            : this()
        {
            // Initialize members.

            foreach (LookupLabel item in lGroupBy)
            {
                m_GroupBy.Add(new LookupLabel(item));
            }
        }

        /// <summary>
        /// Construct default rows.
        /// </summary>

        public Rows()
        {
            // Initialize members.

            m_Results = new CalculationResults();
            m_GroupBy = new LookupGroup();
            m_Set = new ArrayList();
        }

    }

    /// <summary>
    /// We organize the columns of the report into groups that
    /// have one of the 3 valid types.  Keys are meant to show
    /// as highlighted for quick reference.  Columns within a
    /// data group should present similarly, though adjacent
    /// data groups should present as different.  A group of
    /// result columns should present distinct from keys and
    /// from the possible data presentations.
    /// </summary>

    public enum E_LayoutGroupType
    {
        Invalid = 0,
        Key = 1,
        Data = 2,
        Result = 3
    }

    /// <summary>
    /// Bind a unique set of column indices with a layout group
    /// type.  The presentation layer should respect our organization.
    /// </summary>

    public class LayoutGroup
    {
        /// <summary>
        /// Bind a unique set of column indices with a layout
        /// group type.  The presentation layer should respect
        /// our organization.
        /// </summary>

        private E_LayoutGroupType m_Type = E_LayoutGroupType.Invalid;
        private LookupArray m_Set = new LookupArray();

        [XmlElement]
        public E_LayoutGroupType Type
        {
            set { m_Type = value; }
            get { return m_Type; }
        }

        [XmlElement]
        public LookupArray Set
        {
            set { m_Set = value; }
            get { return m_Set; }
        }

        /// <summary>
        /// Construct default.
        /// </summary>

        public LayoutGroup(E_LayoutGroupType eType)
        {
            // Initialize members.

            m_Type = eType;
        }

        /// <summary>
        /// Construct default.
        /// </summary>

        public LayoutGroup()
        {
        }

    }

    /// <summary>
    /// Keep track of our set of layout groups.  To prevent any
    /// confusion, we will throw when we add a group that contains
    /// an index that is already contained by another group in this
    /// layout.
    /// </summary>

    public class Layout
    {
        /// <summary>
        /// Keep track of our set of layout groups.  To prevent
        /// any confusion, we will throw when we add a group
        /// that contains an index that is already contained
        /// by another group in this layout.
        /// </summary>

        private ArrayList m_Set = new ArrayList();

        [XmlIgnore]
        public LayoutGroup this[int iGroup]
        {
            get { return m_Set[iGroup] as LayoutGroup; }
        }

        [XmlArray]
        [XmlArrayItem(typeof(LayoutGroup))]
        public ArrayList Items
        {
            set { m_Set = value; }
            get { return m_Set; }
        }

        [XmlIgnore]
        public int Count
        {
            get { return m_Set.Count; }
        }

        /// <summary>
        /// Lookup the match and return the corresponding group type.
        /// If not found, we return invalid.
        /// </summary>

        public E_LayoutGroupType Match(Int32 iIndex)
        {
            // Lookup the match and return the corresponding group
            // type.  If not found, we return invalid.

            foreach (LayoutGroup lGroup in m_Set)
            {
                if (lGroup.Set.Has(iIndex) == true)
                {
                    return lGroup.Type;
                }
            }

            return E_LayoutGroupType.Invalid;
        }

        /// <summary>
        /// Append to our set and look for overlaps between groups.
        /// If overlapping, punt.
        /// </summary>

        public void Add(E_LayoutGroupType eType, Int32 iIndex)
        {
            // Look for overlaps.  If none found, then append to
            // the tail.

            LayoutGroup lGroup = new LayoutGroup();

            lGroup.Type = eType;

            foreach (LayoutGroup gItem in m_Set)
            {
                if (gItem.Set.Has(iIndex) == true)
                {
                    throw new ReportingException("Found overlap when adding index to layout.");
                }
            }

            lGroup.Set.Add(iIndex);

            m_Set.Add(lGroup);
        }

        /// <summary>
        /// Append to our set and look for overlaps between groups.
        /// If overlapping, punt.
        /// </summary>

        public void Add(LayoutGroup lGroup)
        {
            // Look for overlaps.  If none found, then append to
            // the tail.

            foreach (LayoutGroup gItem in m_Set)
            {
                foreach (Int32 iItem in lGroup.Set)
                {
                    if (gItem.Set.Has(iItem) == true)
                    {
                        throw new ReportingException("Found overlap when adding group to layout.");
                    }
                }
            }

            m_Set.Add(lGroup);
        }

        /// <summary>
        /// Fetch the looping interface for walking our set.
        /// </summary>

        public IEnumerator GetEnumerator()
        {
            // Fetch the looping interface for walking.

            return m_Set.GetEnumerator();
        }

    }

    /// <summary>
    /// This report is composed of tables that conform to the
    /// given columns format.  Each table contains zero or more
    /// grouping specs.  Tables are cut from the subset of the
    /// report view after filtering and during processing.
    /// Each row in a table has values that match the table's
    /// grouping spec.  The processor builds the report after
    /// the view is filtered and only valid rows remain.
    /// See <see cref="T:LendersOffice.QueryProcessor.Processor"/>
    /// for more information.
    /// </summary>

    [XmlRoot("Report")]
    public class Report
    {
        /// <summary>
        /// Version label
        /// 
        /// We update the current version every time the implementation
        /// changes.  We need to be able to distinguish persisted, old
        /// versions of a report so that we can properly migrate it
        /// into the latest form.
        /// </summary>

        private const long c_CurrentVersion = 3;

        /// <summary>
        /// Report set
        /// 
        /// We keep track of tables in our report.  If the
        /// report specifies grouping, then expect the set
        /// to be sorted according to the group fields.
        /// </summary>

        private ProcessingLabel m_Label; // label of report
        private Layout m_Layout; // column grouping
        private Columns m_Columns; // report columns
        private CalculationResults m_Results; // report results
        private Descriptors m_Grouping; // grouping order
        private ArrayList m_Set; // array of tables
        private ProcessingStatistics m_Stats; // processing stats
        private Guid m_Id; // system-wide uid

        private bool ContainsColumnRequiredForNmlsCallReport
        {
            get
            {

                foreach (var requiredColumn in ConstAppDavid.NmlsCallReportRequiredColumns)
                {
                    if (m_Columns.Has(requiredColumn) == false)
                    {
                        return false;
                    }
                }
                return true;
            }
        }
        public bool IsNmlsCallReport
        {
            get
            {
                // 7/21/2011 dd - OPM 67784 - Simple trick to determine whether to display the Mortgage call report by
                // check if "Call Report" is in custom report name.
                return Label.Title.Contains("Call Report") && ContainsColumnRequiredForNmlsCallReport;
            }
        }
        #region Serialization operators

        public static implicit operator Report(string sThat)
        {
            // Deserialize the report and generate an instance
            // from the string form that we can process and use.

            try
            {
                return (Report)SerializationHelper.XmlDeserialize(sThat, typeof(Report));
            }
            catch (Exception e)
            {
                throw new InvalidFormat(e);
            }
        }

        public static Report GetReport(LocalFilePath filePath)
        {
            if (filePath == LocalFilePath.Invalid)
            {
                throw new ArgumentException("File path is invalid.");
            }

            Report retValue = null;
            Action<BinaryFileHelper.LqbBinaryStream> readHandler = delegate(BinaryFileHelper.LqbBinaryStream fs)
            {
                retValue = (Report)(fs.Stream);
            };

            BinaryFileHelper.OpenRead(filePath.Value, readHandler);
            return retValue;
        }

        public static implicit operator Report(Stream sThat)
        {
            try
            {
                return (Report)SerializationHelper.XmlDeserialize(sThat, typeof(Report));
            }
            catch (Exception e)
            {
                throw new InvalidFormat(e);
            }
        }

        public static implicit operator string(Report rThat)
        {
            // Serialize the report and generate an xml document
            // in string form that we can persist or use however.

            try
            {
                return SerializationHelper.XmlSerialize(rThat);
            }
            catch (Exception e)
            {
                // Pass on the love.

                throw new InvalidFormat(e);
            }
        }

        #endregion

        [XmlElement]
        public ProcessingLabel Label
        {
            set { m_Label = value; }
            get { return m_Label; }
        }

        [XmlElement]
        public Layout Layout
        {
            set { m_Layout = value; }
            get { return m_Layout; }
        }

        [XmlElement]
        public Columns Columns
        {
            set { m_Columns = value; }
            get { return m_Columns; }
        }

        [XmlElement]
        public CalculationResults Results
        {
            set { m_Results = value; }
            get { return m_Results; }
        }

        [XmlElement]
        public Descriptors Grouping
        {
            set { m_Grouping = value; }
            get { return m_Grouping; }
        }

        [XmlArray]
        [XmlArrayItem(typeof(Rows))]
        public ArrayList Tables
        {
            set { m_Set = value; }
            get { return m_Set; }
        }

        [XmlElement]
        public ProcessingStatistics Stats
        {
            set { m_Stats = value; }
            get { return m_Stats; }
        }

        [XmlElement]
        public Guid Id
        {
            set { m_Id = value; }
            get { return m_Id; }
        }

        [XmlAttribute]
        public DateTime Start
        {
            get;
            set;
        }

        [XmlAttribute]
        public DateTime End
        {
            get;
            set;
        }

        [XmlAttribute]
        public bool DisplayTablesOnSeparatePages
        {
            get;
            set;
        }


        [XmlIgnore]
        public int Count
        {
            get { return m_Set.Count; }
        }

        public List<Row> Flatten()
        {
            List<Rows> allTables = new List<Rows>();

            if (this.Tables.Count > 0)
            {
                List<Rows> toDo = new List<Rows>();

                foreach (Rows rT in this.Tables)
                {
                    toDo.Add(rT);
                }

                for (int i = 0; i < toDo.Count; ++i)
                {
                    bool isContent = false;

                    foreach (var dO in toDo[i])
                    {
                        Row dR = dO as Row;

                        if (dR != null)
                        {
                            if (isContent == false)
                            {
                                allTables.Add(toDo[i]);
                                isContent = true;
                            }
                        }
                        else
                        {
                            toDo.Add((Rows)dO);
                        }
                    }
                }
            }
            //Take all Rows elements in the List<Rows> and merge them into a single List<Row>
            return allTables.Aggregate(new List<Row>(), (curr, next) => { curr.AddRange(next.Items.Cast<Row>()); return curr; }).ToList();
        }

        /// <summary>
        /// Append existing rows by reference.
        /// </summary>
        /// <param name="rRows">
        /// Rows to append.
        /// </param>

        public void Add(Rows rRows)
        {
            // Add by reference.  Keep allocation
            // to a minimum -- let higher level
            // manage the lifetimes.

            if (rRows != null)
            {
                m_Set.Add(rRows);
            }
        }

        /// <summary>
        /// Fetch iterator from embedded set.
        /// </summary>
        /// <returns>
        /// Return enumeration interface.
        /// </returns>

        public IEnumerator GetEnumerator()
        {
            // Retrieve enumeration interface.

            return m_Set.GetEnumerator();
        }

        /// <summary>
        /// Convert to a serialized string using implicit
        /// string transformation api.
        /// </summary>
        /// <returns>
        /// Serialized form of this report.
        /// </returns>

        public override string ToString()
        {
            // Convert to string using serialization.

            return this;
        }

        /// <summary>
        /// Construct default table.
        /// </summary>

        public Report()
        {
            // Initialize members.

            m_Label = new ProcessingLabel();
            m_Layout = new Layout();
            m_Columns = new Columns();
            m_Results = new CalculationResults();
            m_Grouping = new Descriptors();
            m_Set = new ArrayList();
            m_Stats = new ProcessingStatistics();

            m_Label.Version = c_CurrentVersion;

            m_Id = Guid.Empty;
            Start = DateTime.MaxValue;
            End = DateTime.MinValue;
        }

        /// <summary>
        /// Convert this report to CSV.
        /// </summary>
        /// <param name="principal">
        /// The user running the report.
        /// </param>
        public string ConvertToCsv(AbstractUserPrincipal principal)
        {
            return ReportExporter.ConvertToCsvAsString(principal, this);
        }

    }

    /// <summary>
    /// Each report should be known by a title and by an
    /// implementation version.
    /// </summary>

    public class ProcessingLabel
    {
        /// <summary>
        /// Label tag
        /// 
        /// Each report should be known by a title and
        /// by an implementation version.
        /// </summary>

        private String m_Title; // title of the report
        private String m_SavedBy; // name of saving user
        private DateTime m_SavedOn; // date of recent save
        private Guid m_SavedId; // eid of saving user
        private long m_Version; // version of report

        [XmlElement]
        public String Title
        {
            set { m_Title = value; }
            get { return m_Title; }
        }

        [XmlElement]
        public String SavedBy
        {
            set { m_SavedBy = value; }
            get { return m_SavedBy; }
        }

        [XmlElement]
        public DateTime SavedOn
        {
            set { m_SavedOn = value; }
            get { return m_SavedOn; }
        }

        [XmlElement]
        public Guid SavedId
        {
            set { m_SavedId = value; }
            get { return m_SavedId; }
        }

        [XmlElement]
        public long Version
        {
            set { m_Version = value; }
            get { return m_Version; }
        }

        /// <summary>
        /// Construct default label.
        /// </summary>

        public ProcessingLabel()
        {
            // Initialize members.

            m_Title = "";

            m_SavedBy = "";
            m_SavedOn = DateTime.Today;
            m_SavedId = Guid.Empty;

            m_Version = 0;
        }

    }

    /// <summary>
    /// We track report processing information for the
    /// invoking user.  Feel free to add more stuff.
    /// </summary>

    public class ProcessingStatistics
    {
        /// <summary>
        /// Result stats
        /// 
        /// We track report processing information for the
        /// invoking user.  Feel free to add more stuff.
        /// </summary>

        private DateTime m_StartNow; // total elapsed processing time
        private DateTime m_FinalNow; // total elapsed processing time
        private int m_ViewSize; // total number of view objects
        private int m_HitCount; // number of returned objects

        [XmlElement]
        public DateTime StartNow
        {
            set { m_StartNow = value; }
            get { return m_StartNow; }
        }

        [XmlElement]
        public DateTime FinalNow
        {
            set { m_FinalNow = value; }
            get { return m_FinalNow; }
        }

        [XmlElement]
        public int ViewSize
        {
            set { m_ViewSize = value; }
            get { return m_ViewSize; }
        }

        [XmlElement]
        public int HitCount
        {
            set { m_HitCount = value; }
            get { return m_HitCount; }
        }

        public TimeSpan ProcTime
        {
            get
            {
                return m_FinalNow.Subtract(m_StartNow);
            }
        }

        /// <summary>
        /// Construct default statistics.
        /// </summary>

        public ProcessingStatistics()
        {
            // Initialize members.

            m_StartNow = DateTime.Now;
            m_FinalNow = DateTime.Now;

            m_ViewSize = 0;
            m_HitCount = 0;
        }

    }

    /// <summary>
    /// Need a description...
    /// </summary>

    public class Processor
    {
        private Tuple<DateTime, DateTime> GetDateRange(Query query, FieldLookup f)
        {
            foreach (var temp in query.Filters)
            {
                if (temp.GetType() != typeof(Conditions)) continue;
                var cond = (Conditions)temp;

                //Find an Or
                if (cond.Type != E_ConditionsType.Or) continue;

                //Whose conditions children
                var children = cond.Items.OfType<Conditions>();

                //exist
                if (!children.Any()) continue;

                //and are all
                if (!children.All(child =>
                    {

                        //ands
                        if (child.Type != E_ConditionsType.An) return false;

                        //which themselves have children that are a pair of Condition
                        var childConditions = child.Items.OfType<Condition>();
                        if (childConditions.Count() != 2) return false;

                        //which consists of a GE element with a date field
                        var start = childConditions.FirstOrDefault(a => a.Type == E_ConditionType.Ge &&
                                                                        f.LookupById(a.Id).Kind == Field.E_ClassType.Cal);
                        if (start == default(Condition)) return false;

                        //and an LE element with a date field
                        var end = childConditions.FirstOrDefault(a => a.Type == E_ConditionType.Le &&
                                                                      f.LookupById(a.Id).Kind == Field.E_ClassType.Cal);
                        if (end == default(Condition)) return false;

                        return true;
                    }))
                {
                    continue;
                }

                //okay, so this is probably the condition that restricts the date range. Now we need to pull actual values 
                //out of one of the conditions, we'll just take the first one for sanity. 
                // (note that we're also only using the first condition that matches, again for sanity)

                var grandchildren = children.First().Items.OfType<Condition>();
                var reportBegin = grandchildren.FirstOrDefault(a => a.Type == E_ConditionType.Ge);
                var reportEnd = grandchildren.FirstOrDefault(a => a.Type == E_ConditionType.Le);

                return new Tuple<DateTime, DateTime>(GetDateFromArgument(reportBegin.Argument, f), GetDateFromArgument(reportEnd.Argument, f));
            }

            return new Tuple<DateTime, DateTime>(DateTime.MaxValue, DateTime.MinValue);
        }


        private static Regex DateFinder = new Regex(@"(\d+/\d+/\d+)", RegexOptions.Compiled);
        private DateTime GetDateFromArgument(Argument arg, FieldLookup f)
        {
            DateTime ret;
            if (arg.Type == E_ArgumentType.Const)
            {
                if (DateTime.TryParse(arg.Value, out ret))
                {
                    return ret;
                }

                return DateTime.MaxValue;
            }

            var field = f.LookupById(arg.Value);
            if (field == null) return DateTime.MaxValue;

            var test = DateFinder.Match(field.Selection).Captures;
            if (test.Count == 0) return DateTime.MaxValue;

            if (DateTime.TryParse(test[0].Value, out ret)) return ret;

            return DateTime.MaxValue;
        }

        /// <summary>
        /// Process the query in at most two passes.  We initially retrieve
        /// all the results from the class info accessor using static
        /// constraints.  Calculated fields are further scrutinized.  If
        /// fields don't exist in the data class, or are missing in a
        /// result object, then exception.
        /// </summary>
        /// <param name="rQuery">
        /// Query specified in terms of field names and constraints.
        /// </param>
        /// <param name="fLookup">
        /// Static field accessor for initializing report cells.
        /// </param>
        /// <param name="dTable">
        /// Result from database of the working set.  We currently accept
        /// every row.
        /// </param>
        /// <returns>
        /// Resulting report table match.  Each row represents a
        /// single result of the query.  All rows meet the specified
        /// field conditions.
        /// </returns>

        public Report Process(Query rQuery, ExceptionCatcher eCatcher, FieldLookup fLookup, SimpleRawResultDataTable resultDataTable, int maxViewableRecordsInPipeline)
        {
            // Process the given query.  We don't care about the underlying
            // type -- we just enumerate through the extent and process.

            //try
            //{
            // Sumbit the first pass to the class accessor.  We start by
            // invoking the class info object's lookup method.  The returned
            // objects match the given query.  We need to further filter out
            // rows that contain calculated fields that don't match the
            // given query criteria.  We also need the query's columns to
            // serve as a template for all created tables.  Visible columns
            // make up the report.  We do, however, collect all the fields
            // mentioned in the report (including condition fields) and put
            // them in our access list.

            var brokerId = resultDataTable.UserPrincipal.BrokerId;
            var fieldDependencyList = this.GetEngineFieldDependencyList(brokerId);
            var engine = LendingQBExecutingEngine.GetEngineByBrokerId(brokerId);

            Report report = new Report();
            LookupTable lookup = new LookupTable();
            Fields access = new Fields();
            Fields fields = new Fields();

            report.Stats.StartNow = DateTime.Now;

            report.Stats.ViewSize = 0;
            report.Stats.HitCount = 0;

            foreach (Column column in rQuery.Columns)
            {
                // If visible, the report will reference the query's instance
                // of the column.  We load up the field accessor as well.
                // A lookup index is maintained according to the id of the
                // column, and not the field.  Why?  Because column ids come
                // from a query which might be out of sync with the latest
                // field id, but able to stay connected through translation.
                // Also note that the lookup must maintain an index entry for
                // both the query's id and the actual id for a particular
                // reportable field.

                Field field = fLookup.LookupById(column.Id);

                if (field == null)
                {
                    throw new FieldNotValid(column.Id);
                }

                if (column.Show == true)
                {
                    report.Columns.Add(column);
                }

                if (access.Has(field.Id) == false)
                {
                    //						layout.Add( field.Id , dTable.Columns.IndexOf( field.Id ) );

                    lookup.Add(column.Id, access.Count);

                    if (field.Id != column.Id)
                    {
                        lookup.Add(field.Id, access.Count);
                    }

                    access.Add(field);
                }

                // 3/24/2005 kb - Keep track of all the column's fields in the
                // same layout as the columns.  We may get the same field more
                // than once in this array, but that's okay because we want to
                // quickly map columns by position to their corresponding fields.

                fields.Add(field);
            }

            if (report.Columns.Count == 0)
            {
                // The visible report is empty, so ignore the request and
                // return our empty set.

                report.Stats.FinalNow = DateTime.Now;

                return report;
            }

            // Gather the query's aggregation specifications in the order
            // specified by the query.  We initially group all the filtered
            // rows into one table.  After filtering, we group the rows into
            // individual tables, according to the grouping specification.
            // Because we sort before this, all grouping will build tables
            // in sorted order.  We force this outcome by adding the group
            // by fields to the sorting list as well.

            LookupArray group = new LookupArray();
            //				LookupArray sorts = new LookupArray();
            LookupArray avgup = new LookupArray();
            LookupArray sumup = new LookupArray();
            LookupArray minup = new LookupArray();
            LookupArray maxup = new LookupArray();
            LookupArray count = new LookupArray();

            foreach (Column column in report.Columns)
            {
                // Get the positions of all the fields with aggregation
                // functions associated with them and put them in the
                // appropriate lists for post processing.  We only consider
                // the visible report fields at this time.

                if (lookup.Has(column.Id) == false)
                {
                    // This shouldn't happen.

                    continue;
                }

                foreach (Function func in column.Functions)
                {
                    switch (func.Type)
                    {
                        case E_FunctionType.Av: avgup.Add(lookup[column.Id]);
                            break;

                        case E_FunctionType.Sm: sumup.Add(lookup[column.Id]);
                            break;

                        case E_FunctionType.Mn: minup.Add(lookup[column.Id]);
                            break;

                        case E_FunctionType.Mx: maxup.Add(lookup[column.Id]);
                            break;

                        case E_FunctionType.Ct: count.Add(lookup[column.Id]);
                            break;
                    }
                }
            }

            foreach (Descriptor desc in rQuery.GroupBy)
            {
                // Get the group by field column positions according to our
                // index of all fields that participate in this report.
                // Note that we populate the sorting list too.  This forces
                // group by fields to be sorted first.

                if (lookup.Has(desc.Id) == false)
                {
                    // Should not happen.

                    continue;
                }

                group.Add(lookup[desc.Id]);

                report.Grouping.Add(desc);
            }
            StringBuilder sb = null;
            if (PrincipalFactory.CurrentPrincipal != null && PrincipalFactory.CurrentPrincipal.LoginNm.Equals("apayne", StringComparison.OrdinalIgnoreCase))
            {
                sb = new StringBuilder();
            }


            // We walk the view as fetched from the type specific extent.
            // Each hit returned is an object that we can pull reportable
            // fields out of.  We use the following counter to track the
            // row index when errors occur.  We could add this to the text
            // of the exception so we know what row offended the processor.

            Row content = new Row(lookup.Count, 1);

            //try
            //{

            for (int rowIndex = 0; rowIndex < resultDataTable.RowCount; rowIndex++)
            {
                ++report.Stats.ViewSize;
                foreach (Field field in access)
                {
                    int i = lookup[field.Id];
                    Value value = content[i];
                    try
                    {
                        value.Format = field.Format;
                        value.Data = resultDataTable.GetValue(rowIndex, field.Id);
                        value.FatalError = false;

                        if (field.Type == Field.E_FieldType.Dec)
                        {
                            var fieldInfo = CFieldInfoTable.GetInstance()[field.Id];

                            if (fieldInfo.DbInfo != null
                                && fieldInfo.DbInfo.m_mumericPrecision != null
                                && !Convert.IsDBNull(fieldInfo.DbInfo.m_mumericPrecision)
                                && fieldInfo.DbInfo.m_numericScale != null
                                && !Convert.IsDBNull(fieldInfo.DbInfo.m_numericScale))
                            {
                                var precision = Convert.ToByte(fieldInfo.DbInfo.m_mumericPrecision);
                                var scale = Convert.ToByte(fieldInfo.DbInfo.m_numericScale);
                                if (DecimalInfo.IsSentinelValue(Convert.ToDecimal(value.Data), precision, scale))
                                {
                                    value.Format = "";
                                    value.Data = "ERROR";
                                    value.FatalError = true;
                                }
                            }
                        }
                    }
                    catch
                    {
                        value.Data = null;
                    }
                }


                // Generate a group by key according to the current
                // row and the specified group by fields.  If this
                // combination hasn't been added, we make a new table
                // and add it to the lookup set.
                //
                // 5/13/2005 kb - We now nest tables within tables as
                // a composite.  Each table will have associated sub-
                // totals for each calculation.  This way, we can show
                // subtotals at each level of grouping.  To pull it off,
                // we need to nest rows according to increasingly
                // specific hierarchy of grouping.

                LookupGroup key = new LookupGroup();
                Rows container = null;

                foreach (Rows table in report)
                {
                    // Seed the report with a table that lacks any key.  This
                    // is the root table.  All grouped tables will be a child
                    // of this one.  If no grouping is specified, then we'll
                    // add each new row to this table.

                    if (table.GroupBy.CompareTo(key) == true)
                    {
                        container = table;

                        break;
                    }
                }

                if (container == null)
                {
                    container = new Rows(key);
                    report.Add(container);
                }

                foreach (int col in group)
                {
                    // Find the matching table.  Once we finish constructing
                    // the key, we will append a new row to the most nested
                    // container table.  If the key is not present, then we
                    // need to make a new table and hook it up.

                    Rows inner = null;


                    key.Add(report.Columns[col].Name, string.Format("{0:" + fields[col].Format + "}", content[col].Data));

                    foreach (Rows table in container)
                    {
                        if (table.GroupBy.CompareTo(key) == true)
                        {
                            inner = table;

                            break;
                        }
                    }

                    if (inner == null)
                    {
                        container.Add(inner = new Rows(key));
                    }

                    container = inner;
                }


                var dataRow = resultDataTable.GetRowAtIndex(rowIndex);
                var rowAccessSpec = this.GetRowAccessSpec(resultDataTable.UserPrincipal, fieldDependencyList, dataRow, engine);

                if (sb != null)
                {
                    string nm = (string)resultDataTable.GetValue(rowIndex, "sLNm");
                    sb.AppendFormat("{0} Access : {1}", nm, rowAccessSpec.Level);
                    sb.AppendLine();
                }


                if (rowAccessSpec.Level == E_RowAccessLevel.None)
                    continue;

                content.Comment = rowAccessSpec.Comment;
                content.Level = rowAccessSpec.Level;
                content.Key = (Guid)resultDataTable.GetValue(rowIndex, "Id");
                content.Arg = (int)resultDataTable.GetValue(rowIndex, "Status");

                container.Add(new Row(content));

                ++report.Stats.HitCount;
                if (report.Stats.HitCount == maxViewableRecordsInPipeline)
                {
                    break;
                }
            }
            if (sb != null)
            {
                Tools.LogInfo("LoanReporting:AccessSpec" + sb.ToString());
            }
            //}
            //catch( Exception )
            //{
            //    // Oops!

            //    throw;
            //}

            // We need to calculate the results for aggregation functions
            // for all rows at once.  This is our cumulative totals part
            // of the report that we may compare individual tables against.
            //
            // 5/13/2005 kb - Because we now nest tables, we should walk
            // each direct table child of the report and process each
            // independently.  As of today, expect only one table directly
            // contained by the report.

            foreach (Rows table in report)
            {
                foreach (int col in avgup)
                {
                    table.AvgVal(col);
                }

                foreach (int col in sumup)
                {
                    table.SumTot(col);
                }

                foreach (int col in minup)
                {
                    table.MinVal(col);
                }

                foreach (int col in maxup)
                {
                    table.MaxVal(col);
                }

                foreach (int col in count)
                {
                    table.NumTot(col);
                }

                foreach (CalculationResult result in table.Results)
                {
                    // It is possible that we are tracking calculation
                    // results on fields that aren't visible in the final
                    // report.
                    //
                    // 5/13/2005 kb - I don't think this is true anymore,
                    // and if so, I don't care -- they won't get rendered.
                    // What is new is that we expect any number of tables
                    // contained by the report's array of tables.  We need
                    // to combine all their results into the report's lone
                    // set of accumulative results.

                    bool found = false;

                    foreach (CalculationResult accum in report.Results)
                    {
                        if (accum.Column == result.Column && accum.What == result.What)
                        {
                            accum.Combine(result);

                            found = true;

                            break;
                        }
                    }

                    if (found == false)
                    {
                        report.Results.Add(result);
                    }
                }
            }

            // Track the finish time and return the report tables.

            report.Stats.FinalNow = DateTime.Now;

            return report;
            //}
            //catch( Exception e )
            //{
            //    // Oops!

            //    throw new ProcessingAborted( e );
            //}
        }

        private RowAccessSpec GetRowAccessSpec(AbstractUserPrincipal principal, IEnumerable<string> fieldList, DataRow row, ExecutingEngine engine)
        {
            var spec = new RowAccessSpec();

            var valueEvaluator = new LoanValueEvaluator((Guid)row["id"], row, fieldList, o => { return ConstAppDavid.CustomReportEngineFieldPrefix + o; });
            valueEvaluator.SetEvaluatingPrincipal(ExecutingEnginePrincipal.CreateFrom(principal));

            bool canRead;
            bool canWrite;

            canRead = LendingQBExecutingEngine.CanPerform(WorkflowOperations.ReadLoanOrTemplate, valueEvaluator, engine);
            canWrite = LendingQBExecutingEngine.CanPerform(WorkflowOperations.WriteLoanOrTemplate, valueEvaluator, engine);

            if (canWrite)
            {
                spec.Level = E_RowAccessLevel.Edit;
            }
            else if (canRead)
            {
                // 12/6/2010 dd - OPM 60360 - If LendingQB LOS user has a Run PML privilege but no write privilege then still display
                // edit link in pipeline.
                if (LendingQBExecutingEngine.CanPerform(WorkflowOperations.RunPmlForAllLoans, valueEvaluator, engine) ||
                    LendingQBExecutingEngine.CanPerform(WorkflowOperations.RunPmlForRegisteredLoans, valueEvaluator, engine) ||
                    LendingQBExecutingEngine.CanPerform(WorkflowOperations.RunPmlToStep3, valueEvaluator, engine))
                {
                    spec.Level = E_RowAccessLevel.Edit;
                }
                else
                {
                    spec.Level = E_RowAccessLevel.Read;
                }
            }
            else
            {
                spec.Level = E_RowAccessLevel.None;
            }


            return spec;
        }
        
        private IEnumerable<string> GetEngineFieldDependencyList(Guid brokerId)
        {
            return LendingQBExecutingEngine.GetDependencyFieldsByOperation(
                brokerId, 
                WorkflowOperations.ReadLoanOrTemplate,
                WorkflowOperations.WriteLoanOrTemplate, 
                WorkflowOperations.RunPmlForAllLoans, 
                WorkflowOperations.RunPmlForRegisteredLoans,
                WorkflowOperations.RunPmlToStep3);
        }

        /// <summary>
        /// Process the query in at most two passes.  We initially retrieve
        /// all the results from the class info accessor using static
        /// constraints.  Calculated fields are further scrutinized.  If
        /// fields don't exist in the data class, or are missing in a
        /// result object, then exception.  Once generated, we collapse
        /// the aggregate results and fold them into a derivation report
        /// that shows grouped results as an individual row.
        /// 
        /// 2/3/2005 kb - Extended the report model to include a layout
        /// specification, which is really just column-group tagging.  We
        /// will group columns together and tag them with layout types.
        /// The presentation layer should shade columns with respect to
        /// the associated layout type.
        /// </summary>
        /// <param name="rQuery">
        /// Query specified in terms of field names and constraints.
        /// </param>
        /// <param name="fLookup">
        /// Static field accessor for initializing report cells.
        /// </param>
        /// <param name="dTable">
        /// Result from database of the working set.  We currently accept
        /// every row.
        /// </param>
        /// <returns>
        /// Resulting report table match.  Each row represents a
        /// single result of the query.  All rows meet the specified
        /// field conditions.
        /// </returns>

        public Report CrossIt(Query rQuery, ExceptionCatcher eCatcher, FieldLookup fLookup, SimpleRawResultDataTable resultDataTable, int maxViewableRecordsInPipeline)
        {
            try
            {
                // 11/30/2004 kb - To support collapsed views
                // of the data, we will try to show only accumulated items
                // when groupings are more than one.

                Report report;
                Int32 groupIx;
                Int32 crossIx;

                report = Process(rQuery, eCatcher, fLookup, resultDataTable, maxViewableRecordsInPipeline);
                for (groupIx = report.Grouping.Count - 1; groupIx >= 0; groupIx--)
                {
                    Descriptor dGroup = report.Grouping[groupIx];

                    if (dGroup.Option != 0)
                    {
                        break;
                    }
                }

                if (groupIx != -1)
                {
                    // Last entry in the group by column list will become
                    // the starting key for the collapsed view of the
                    // data.  All other group-by fields stand.  We take the
                    // calculated results and make them row entries.

                    Report grouped = new Report();

                    grouped.Stats = report.Stats;

                    foreach (Descriptor dGroup in report.Grouping)
                    {
                        // Add the grouping descriptors that we are not using to
                        // do this cross operation.

                        if (dGroup.Id != report.Grouping[groupIx].Id)
                        {
                            grouped.Grouping.Add(dGroup);
                        }
                    }

                    grouped.Columns.Add(rQuery.Columns.Find(report.Grouping[groupIx].Id));

                    grouped.Layout.Add(E_LayoutGroupType.Key, 0);

                    foreach (CalculationResult calcRes in report.Results)
                    {
                        // Make a new column for each aggregation result of the
                        // input report.  Note that we already added a lone column
                        // for holding the group by value that we're crossing against.

                        Column crossCol = new Column(report.Columns[calcRes.Column]);

                        if (calcRes.What == E_FunctionType.Av)
                        {
                            crossCol.Name = "Avg " + crossCol.Name;
                        }
                        else if (calcRes.What == E_FunctionType.Ct)
                        {
                            crossCol.Name = "Num " + crossCol.Name;
                        }
                        else if (calcRes.What == E_FunctionType.Mn)
                        {
                            crossCol.Name = "Min " + crossCol.Name;
                        }
                        else if (calcRes.What == E_FunctionType.Mx)
                        {
                            crossCol.Name = "Max " + crossCol.Name;
                        }
                        else if (calcRes.What == E_FunctionType.Sm)
                        {
                            crossCol.Name = "Tot " + crossCol.Name;
                        }

                        grouped.Results.Add(grouped.Columns.Count, calcRes.Result, calcRes.Hits, calcRes.What, calcRes.FatalError);

                        crossCol.Type = Field.E_FieldType.Dbl;

                        grouped.Columns.Add(crossCol);
                    }

                    // 5/17/2005 kb - Gather the leaf tables, convert into
                    // rows, and add to new tables that correspond to grouped and
                    // collapsed result.  If the table doesn't contain other
                    // tables, then it is a leaf table,  and needs to be
                    // collapsed.

                    ArrayList allTables = new ArrayList();
                    ArrayList toDo = new ArrayList();

                    foreach (Rows rowTable in report.Tables)
                    {
                        toDo.Add(rowTable);
                    }

                    for (int i = 0; i < toDo.Count; ++i)
                    {
                        foreach (Object rowItem in toDo[i] as Rows)
                        {
                            Rows rowTable = rowItem as Rows;

                            if (rowTable == null)
                            {
                                allTables.Add(toDo[i]);

                                break;
                            }
                            else
                            {
                                toDo.Add(rowTable);
                            }
                        }
                    }

                    foreach (Rows rowTable in allTables)
                    {
                        // Generate a group by key according to the current
                        // table and the specified group by fields.  If this
                        // combination hasn't been added, we make a new table
                        // and add it to the lookup set.  As we build the key
                        // for the collapsed table, we want to create all the
                        // containing tables as we go.
                        //
                        // Find the matching table and append a new row to it.
                        // If the kay is not present, then we need to make a
                        // new table and hook it up.

                        LookupGroup groupKey = new LookupGroup();
                        Rows aggTable = null;

                        foreach (Rows curTable in grouped.Tables)
                        {
                            if (curTable.GroupBy.CompareTo(groupKey) == true)
                            {
                                aggTable = curTable;

                                break;
                            }
                        }

                        if (aggTable == null)
                        {
                            grouped.Add(aggTable = new Rows(groupKey));
                        }

                        foreach (LookupLabel lGroup in rowTable.GroupBy)
                        {
                            bool foundIt = false;

                            if (lGroup.Label == rowTable.GroupBy[groupIx].Label)
                            {
                                continue;
                            }

                            groupKey.Add(lGroup);

                            foreach (Rows curTable in aggTable)
                            {
                                if (curTable.GroupBy.CompareTo(groupKey) == true)
                                {
                                    aggTable = curTable;

                                    foundIt = true;

                                    break;
                                }
                            }

                            if (foundIt == false)
                            {
                                // 5/19/2005 kb - Create the new table as a place holder
                                // to continue building the hierarchy.  We will initialize
                                // leaf table's results afterwards.

                                Rows newTable = new Rows(groupKey);

                                aggTable.Add(newTable);

                                aggTable = newTable;
                            }
                        }

                        if (aggTable.Results.Count == 0)
                        {
                            // 5/19/2005 kb - Build up the leaf table's results using the
                            // report as a template.  We calculate the final values at
                            // the end.

                            foreach (CalculationResult calcRes in grouped.Results)
                            {
                                aggTable.Results.Add(calcRes.Column, 0, 0, calcRes.What, fatalError: false);
                            }
                        }

                        // Add this row to the matching subset table.  If no
                        // group by fields are specified, then we're basically
                        // pulling out the same table every time.
                        //
                        // 1/14/2005 kb - We now build aggregation results from
                        // the existing ones through combination (not through
                        // recalculation from row values).  We do this to build
                        // results like averages where we need to recreate the
                        // total sums to get an accurate average of 2 tables.
                        // This will allow us to support all future calculation
                        // results without changing this handling code.
                        //
                        // 2/2/2005 kb - The column layout of the aggregation
                        // table now includes a unit calc.  We start the pulled
                        // cells' data after this slot in the new table.

                        Row calcRow = new Row(grouped.Columns.Count);

                        foreach (Row rRow in rowTable)
                        {
                            calcRow.Hits += rRow.Hits;
                        }

                        int x = 1;

                        foreach (CalculationResult calcRes in rowTable.Results)
                        {
                            aggTable.Results[x - 1].Combine(calcRes);

                            if (report.Columns[calcRes.Column].Format.StartsWith("c", StringComparison.OrdinalIgnoreCase)) //format currency data. OPM 33946
                            {
                                calcRow[x].Data = calcRes.GetFormattedResult(report.Columns[calcRes.Column].Format);
                            }
                            else
                            {
                                calcRow[x].Data = calcRes.FatalError ? (object)"ERROR" : (object)calcRes.Result;
                            }

                            calcRow[x].FatalError = calcRes.FatalError;

                            ++x;
                        }

                        calcRow[0].Data = rowTable.GroupBy[groupIx].Value;

                        aggTable.Add(calcRow);
                    }

                    foreach (CalculationResult cRes in grouped.Results)
                    {
                        // 5/17/2005 kb - The report's tables are now nested, so we
                        // need to recursively combine all child calcs into each
                        // table as subtotals, so that we can combine them into the
                        // report's result set.

                        foreach (Rows curTable in grouped.Tables)
                        {
                            curTable.Combine(cRes.Column, cRes.What);
                        }
                    }

                    // We have formed the first order derivative of
                    // the current report.  Each table result has
                    // been collapsed into a manufactured row of the
                    // common table.  We expect sorting to hold.

                    grouped.Stats.FinalNow = DateTime.Now;

                    report = grouped;
                }

                for (crossIx = report.Grouping.Count - 1; crossIx >= 0; crossIx--)
                {
                    Descriptor dGroup = report.Grouping[crossIx];

                    if (dGroup.Option != 0)
                    {
                        break;
                    }
                }

                if (crossIx != -1)
                {
                    // Take each table and cross its entries.  This is not
                    // a collapsing-like operation as with grouping.  We
                    // gather the first column as a key and generate a lateral
                    // description of each table, such that the new table is
                    // a collection of the previous tables with a common
                    // major grouping such that each sub table is now a row
                    // in the new table.  Clearly, this operation doesn't
                    // work for all data sets, but should be closed so that
                    // every table has a valid result (however goofy).

                    Report crossed = new Report();
                    ValueSet crossKeys = new ValueSet();

                    crossed.Stats = report.Stats;

                    foreach (Descriptor dGroup in report.Grouping)
                    {
                        if (dGroup.Id != report.Grouping[crossIx].Id)
                        {
                            crossed.Grouping.Add(dGroup);
                        }
                    }

                    crossed.Columns.Add(rQuery.Columns.Find(report.Grouping[crossIx].Id));

                    crossed.Layout.Add(E_LayoutGroupType.Key, 0);

                    // 5/17/2005 kb - Gather the leaf tables, convert into
                    // rows, and add to new tables that correspond to grouped and
                    // collapsed result.  If the table doesn't contain other
                    // tables, then it is a leaf table,  and needs to be
                    // collapsed.

                    ArrayList allTables = new ArrayList();
                    ArrayList toDo = new ArrayList();

                    foreach (Rows rowTable in report.Tables)
                    {
                        toDo.Add(rowTable);
                    }

                    for (int i = 0; i < toDo.Count; ++i)
                    {
                        foreach (Object rowItem in toDo[i] as Rows)
                        {
                            Rows rowTable = rowItem as Rows;

                            if (rowTable == null)
                            {
                                allTables.Add(toDo[i]);

                                break;
                            }
                            else
                            {
                                toDo.Add(rowTable);
                            }
                        }
                    }

                    foreach (Rows rowTable in allTables)
                    {
                        // Layout will be extended horizontally.  Every table in
                        // the result will have the same layout.  We need to get
                        // the superset of all the possible keys from the first
                        // column of each table.

                        foreach (Row rRow in rowTable)
                        {
                            if (rRow.Count == 0)
                            {
                                continue;
                            }

                            crossKeys.Add(rRow[0]);
                        }
                    }

                    for (int i = 0; i < crossKeys.Count; ++i)
                    {
                        // We group the layout by unique key value with all
                        // the calculation results associated with that key
                        // value's row for a particular table.  Things get
                        // wide pretty fast.
                        //
                        // 2/1/2005 kb - Note that we add the blank calculation
                        // results for the whole report here.  We will use this
                        // set as a template for how we layout full rows'
                        // results for this 2nd order crossing operation.

                        LayoutGroup datGroup = new LayoutGroup(E_LayoutGroupType.Data);
                        Column crossKey = new Column(report.Columns[0]);

                        datGroup.Set.Add(crossed.Columns.Count);

                        crossed.Columns.Add(crossKey);

                        foreach (CalculationResult calcRes in report.Results)
                        {
                            // Layout the column set for the current key.  We use
                            // the kind of the originating column, and force the
                            // representation to double because it will hold a
                            // calculation result.

                            Column crossCol = new Column(report.Columns[calcRes.Column]);

                            crossed.Results.Add(crossed.Columns.Count, 0, 0, calcRes.What, fatalError: false);

                            datGroup.Set.Add(crossed.Columns.Count);

                            crossCol.Type = Field.E_FieldType.Dbl;

                            crossed.Columns.Add(crossCol);
                        }

                        crossed.Layout.Add(datGroup);
                    }

                    foreach (CalculationResult calcRes in report.Results)
                    {
                        // Now append the totals for each calculation for each
                        // table.  These results will be kept on each table-row.
                        // The report-wide result doesn't change, so we include
                        // it now.

                        Column crossCol = new Column(report.Columns[calcRes.Column]);

                        crossed.Results.Add(crossed.Columns.Count, 0, 0, calcRes.What, fatalError: false);

                        crossed.Layout.Add(E_LayoutGroupType.Result, crossed.Columns.Count);

                        crossCol.Type = Field.E_FieldType.Dbl;

                        crossed.Columns.Add(crossCol);
                    }

                    foreach (Rows rowTable in allTables)
                    {
                        // Generate a group by key according to the current
                        // table and the specified group by fields.  If this
                        // combination hasn't been added, we make a new table
                        // and add it to the lookup set.

                        LookupGroup groupKey = new LookupGroup();
                        Rows crsTable = null;

                        foreach (Rows curTable in crossed.Tables)
                        {
                            if (curTable.GroupBy.CompareTo(groupKey) == true)
                            {
                                crsTable = curTable;

                                break;
                            }
                        }

                        if (crsTable == null)
                        {
                            crossed.Add(crsTable = new Rows(groupKey));
                        }

                        foreach (LookupLabel lGroup in rowTable.GroupBy)
                        {
                            bool foundIt = false;

                            if (lGroup.Label == rowTable.GroupBy[crossIx].Label)
                            {
                                continue;
                            }

                            groupKey.Add(lGroup);

                            foreach (Rows curTable in crsTable)
                            {
                                if (curTable.GroupBy.CompareTo(groupKey) == true)
                                {
                                    crsTable = curTable;

                                    foundIt = true;

                                    break;
                                }
                            }

                            if (foundIt == false)
                            {
                                // 5/19/2005 kb - Create the new table as a place holder
                                // to continue building the hierarchy.  We will initialize
                                // leaf table's results afterwards.

                                Rows newTable = new Rows(groupKey);

                                crsTable.Add(newTable);

                                crsTable = newTable;
                            }
                        }

                        if (crsTable.Results.Count == 0)
                        {
                            // 5/19/2005 kb - Build up the leaf table's results using the
                            // report as a template.  We calculate the final values at
                            // the end.

                            foreach (CalculationResult calcRes in crossed.Results)
                            {
                                crsTable.Results.Add(calcRes.Column, 0, 0, calcRes.What, fatalError: false);
                            }
                        }

                        // Build the super row from this table.  We locate the
                        // offset using the first cell of each row as a key.
                        // All cells with aggregation calculations are copied
                        // into the super row.
                        //
                        // The row layout should look like the following pattern:
                        //
                        // group | key1 , a11 , a12 , ... | key2 , ...
                        //
                        // We cap the row with the totals from the table,
                        // which is how we turn an entire table on its side.
                        //
                        // 1/14/2005 kb - Table totals for cap results are now
                        // generated by combination of these capping calculation
                        // results.

                        Row fullRow = new Row(crossed.Columns.Count);

                        int w = rowTable.Results.Count;

                        int i = 1;
                        int j = 0;

                        int c = 0;

                        while (j < crossKeys.Count)
                        {
                            fullRow[i] = crossKeys[j];

                            i += w + 1;

                            c += w;

                            ++j;
                        }

                        foreach (CalculationResult calcRes in rowTable.Results)
                        {
                            // 2/1/2005 kb - We are combining the calculation results
                            // we have for this table.  Note that these results go
                            // in the right, bottom corner of the table's aggregation
                            // calculation set.
                            //
                            // The c and i indexes point to the tail end of the new
                            // full row and the end group of the cross-table's results.

                            crsTable.Results[c].Combine(calcRes);

                            if (report.Columns[calcRes.Column].Format.StartsWith("c", StringComparison.OrdinalIgnoreCase)) //format currency data. OPM 33946
                            {
                                fullRow[i].Data = calcRes.GetFormattedResult(report.Columns[calcRes.Column].Format);
                            }
                            else
                            {
                                fullRow[i].Data = calcRes.FatalError ? (object)"ERROR" : (object)calcRes.Result;
                            }

                            fullRow[i].FatalError = calcRes.FatalError;

                            ++c;

                            ++i;
                        }

                        foreach (Row rRow in rowTable)
                        {
                            int x = 2;
                            int y = 0;

                            while (y < crossKeys.Count)
                            {
                                // Find current row's offset in the new lateral row.
                                // Now it seems wierd to compare 2 values by their
                                // text representation (especially for numeric types),
                                // but keep in mind that these cross keys were pulled
                                // from the very values we're comparing against.  In
                                // other words, the text representation ought to be
                                // consistent.  As well, it is expected that all values
                                // have the same type, or are null.  String columns
                                // that have empty and null values will match (both
                                // are represented as "").
                                //
                                // 3/24/2005 kb - We no longer use text comparison,
                                // though it is pretty effective at decoupling the
                                // form of the value from our processor.  Now, we
                                // just compare outright.

                                if (crossKeys[y].CompareTo(rRow[0]) == 0)
                                {
                                    break;
                                }

                                x += w + 1;

                                ++y;
                            }

                            if (y < crossKeys.Count)
                            {
                                foreach (CalculationResult calcRes in rowTable.Results)
                                {
                                    // 2/1/2005 kb - The source table's row value is from
                                    // a previous crossing (i.e., it's a calculation result
                                    // that has been stripped out.  So, we can reconstruct
                                    // the result that was used by taking the row count and
                                    // value from this row, and the type from the cross
                                    // table's result at this position.

                                    foreach (CalculationResult crosRes in crsTable.Results)
                                    {
                                        if (crosRes.Column == x)
                                        {
                                            var fieldValue = rRow[calcRes.Column];

                                            if (fieldValue.FatalError)
                                            {
                                                crosRes.Hits = 0;
                                                crosRes.Result = 0;
                                                crosRes.FatalError = true;
                                            }
                                            else
                                            {
                                                crosRes.Combine(fieldValue.Real, rRow.Hits);
                                            }
                                        }
                                    }

                                    fullRow[x] = rRow[calcRes.Column];

                                    ++x;
                                }
                            }

                            fullRow.Hits += rRow.Hits;
                        }

                        fullRow[0].Data = rowTable.GroupBy[crossIx].Value;

                        crsTable.Add(fullRow);
                    }

                    foreach (Rows curTable in crossed)
                    {
                        // 2/2/2005 kb - Now gather each table's result and combine
                        // it with the report's results, so that we have our final
                        // calculations.  We simply combine each one with it's
                        // matching, report-scoped result.
                        //
                        // 5/17/2005 kb - The report's tables are now nested, so we
                        // need to recursively combine all child calcs into each
                        // table as subtotals, so that we can combine them into the
                        // report's result set.

                        foreach (CalculationResult cRes in crossed.Results)
                        {
                            curTable.Combine(cRes.Column, cRes.What);

                            foreach (CalculationResult tRes in curTable.Results)
                            {
                                if (tRes.Column == cRes.Column && tRes.What == cRes.What)
                                {
                                    cRes.Combine(tRes);
                                }
                            }
                        }
                    }

                    // We have formed the second order derivative of
                    // the current report.  Each table has been extended
                    // laterally into a single row of a composite table.

                    crossed.Stats.FinalNow = DateTime.Now;

                    report = crossed;
                }

                var range = GetDateRange(rQuery, fLookup);
                report.Start = range.Item1;
                report.End = range.Item2;

                report.DisplayTablesOnSeparatePages = rQuery.DisplayTablesOnSeparatePages;
                return report;
            }
            catch (ProcessingAborted)
            {
                // Pass-thru!

                throw;
            }
            catch (Exception e)
            {
                // Oops!

                throw new ProcessingAborted(e);
            }
        }

        private object MaskSsn(object oSsn)
        {
            string ssn = oSsn as string;
            try
            {
                if (ssn != null)
                {
                    return Regex.Replace(ssn, @"\d{3}-\d{2}-(\d{4})", "***-**-$1");
                }
            }
            catch { }
            return oSsn;
        }

        public Report MaskSsnReport(Report report)
        {
            Exception exc = null;

            try
            {
                if (!report.Columns.Has("aBSsn") && !report.Columns.Has("aCSsn"))
                {
                    return report;
                }

                //Gather list containing all tables
                ArrayList allTablesList = new ArrayList();
                ArrayList toExpand = new ArrayList();

                foreach (Rows rowTable in report.Tables)
                    toExpand.Add(rowTable);

                for (int i = 0; i < toExpand.Count; ++i)
                {
                    foreach (Object rowItem in toExpand[i] as Rows)
                    {
                        Rows rowTable = rowItem as Rows;
                        if (rowTable == null)
                        {
                            allTablesList.Add(toExpand[i]);
                            break;
                        }
                        else
                        {
                            toExpand.Add(rowTable);
                        }
                    }
                }

                //For every table in the list, access all its rows
                foreach (Rows rowTable in allTablesList)
                {
                    if (rowTable.GroupBy.Count > 0)
                    {
                        foreach (LookupLabel group in rowTable.GroupBy)
                        {
                            if (group.Label.ToLower().Contains("ssn"))
                                group.Value = MaskSsn(group.Value) as string;
                        }
                    }
                    int colIndex = 0;
                    foreach (Column col in report.Columns)
                    {
                        if (col.Id == "aBSsn" || col.Id == "aCSsn")
                        {
                            foreach (Row row in rowTable)
                            {
                                if (row != null)
                                {
                                    try
                                    {
                                        if (colIndex >= 0 && colIndex < row.Count)
                                        {
                                            Value val = row[colIndex];
                                            val.Data = MaskSsn(val.Data);
                                        }
                                    }
                                    catch (Exception e) { if (exc == null) exc = e; }
                                }
                            }
                        }
                        colIndex++;
                    }
                }

            }
            catch (Exception e)
            {
                StringBuilder errMsg = new StringBuilder();

                errMsg.AppendLine("Error found when masking SSN from custom report.");

                if (report != null)
                {
                    errMsg.AppendLine("report Id: " + report.Id.ToString());

                    if (report.Label != null)
                    {
                        errMsg.AppendLine("Report Name: " + report.Label.Title);
                        errMsg.AppendLine("User: " + report.Label.SavedBy);
                        errMsg.AppendLine("User id: " + report.Label.SavedId.ToString());
                    }
                }
                Tools.LogError(errMsg.ToString(), e);
            }

            if (exc != null) Tools.LogError(exc);

            return report;
        }

    }

    /// <summary>
    /// We expose an id and name to simplify list binding.
    /// </summary>

    public class FieldEntry
    {
        /// <summary>
        /// We expose an id and name to simplify list binding.
        /// </summary>

        private string m_Id;
        private string m_Name;
        private int m_Option;

        public string Id
        {
            get { return m_Id; }
        }

        public string Name
        {
            get { return m_Name; }
        }

        public int Option
        {
            get { return m_Option; }
        }

        /// <summary>
        /// Construct field entry for listing.
        /// </summary>
        /// <param name="sName">
        /// Name of field.
        /// </param>
        /// <param name="sId">
        /// Id of field.
        /// </param>
        /// <param name="iOption">
        /// Field option.
        /// </param>

        public FieldEntry(string sName, string sId, int iOption)
        {
            // Initialize members.

            m_Name = sName;
            m_Id = sId;
            m_Option = iOption;
        }

        /// <summary>
        /// Construct default.
        /// </summary>

        public FieldEntry()
        {
            // Initialize members.

            m_Name = "";
            m_Id = "";
            m_Option = 0;
        }
    }


    /// <summary>
    /// We need to separate the dictionary elements to expose access
    /// but limit update.
    /// </summary>

    public interface IIndex
    {
        /// <summary>
        /// Provide access to lookup mapping.
        /// </summary>

        String this[String sIn]
        {
            get;
        }

    }

    /// <summary>
    /// We need to separate the dictionary elements to expose access
    /// but limit update.
    /// </summary>

    public interface ITable
    {
        /// <summary>
        /// Provide simple insertion interace.
        /// </summary>
        /// <param name="sKey">
        /// Key to map.
        /// </param>
        /// <param name="sVal">
        /// Val to map.
        /// </param>

        void Add(String sKey, String sVal);

    }

    /// <summary>
    /// Keep track of all entries added in the order they were
    /// added.
    /// </summary>

    public class PairSet : IEnumerable
    {
        /// <summary>
        /// Keep track of all entries added in the order they
        /// were added.
        /// </summary>

        private ArrayList m_Set = new ArrayList();

        /// <summary>
        /// List individual key-value pairs in our own entry.
        /// </summary>

        public class Entry
        {
            public String A = String.Empty;
            public String B = String.Empty;

        }

        /// <summary>
        /// Append onto the end of our set, but only if unique.
        /// </summary>

        public void Add(String sA, String sB)
        {
            // Prepare new entry for adding and append to
            // the end only if unique.

            Entry nE = new Entry();

            if (sA == null || sB == null)
            {
                throw new ArgumentException("Invalid entry parameters for pair set.");
            }

            nE.A = sA;
            nE.B = sB;

            foreach (Entry pE in m_Set)
            {
                if (pE.A == nE.A && pE.B == nE.B)
                {
                    return;
                }
            }

            m_Set.Add(nE);
        }

        /// <summary>
        /// Return the looping interface for our set of pairs.
        /// </summary>

        public IEnumerator GetEnumerator()
        {
            // Return looping walker for set of pairs.

            return m_Set.GetEnumerator();
        }

    }

    /// <summary>
    /// Translate queries into their query processor interface
    /// components.
    /// </summary>

    public interface DbQueryTranslation
    {
        /// <summary>
        /// Adopt query and generate layout, clause, and orders from
        /// the contents.
        /// </summary>

        void Translate(AbstractUserPrincipal principal, Query rQuery, PairSet includeTheseFields, FieldTable fTable, bool maskSsn);

        /// <summary>
        /// Each translation will generate details from the given query,
        /// but we keep separate the source of the data set.  Typically,
        /// the source is specific to the use of the query translator.
        /// </summary>

        String Source
        {
            set;
            get;
        }

        /// <summary>
        /// Provide string-based description of the fields of interest
        /// according to the internal representation of the query's
        /// columns.
        /// </summary>

        String Layout
        {
            get;
        }

        /// <summary>
        /// Capture the query's predicate in as an infix expression
        /// using 'and' and 'or' operations with parenthesis for
        /// grouping.
        /// </summary>

        String Clause
        {
            get;
        }

        /// <summary>
        /// Provide a string-based representation of the columns
        /// (listed by position, base-relative to '1') and the
        /// direction of sorting.
        /// </summary>

        String Orders
        {
            get;
        }

        /// <summary>
        /// Keep track of parameters we add (as strings) to the
        /// query during translation.  All input arguments need
        /// to be packaged as parameters to avoid sql injection
        /// attacks.
        /// </summary>

        StringDictionary Params
        {
            get;
        }

    }

    /// <summary>
    /// Every good query representation needs to map onto an sql
    /// interface.  Note: this one is probably sqlserver specific,
    /// using transact-sql in propietary ways.  Sorry.
    /// </summary>

    public class SqlQueryTranslation : DbQueryTranslation
    {
        /// <summary>
        /// Track query components as we build the translation.
        /// </summary>

        private String m_Layout = String.Empty;
        private String m_Source = String.Empty;
        private String m_Clause = String.Empty;
        private String m_Orders = String.Empty;
        private StringDictionary m_Params = new StringDictionary();

        #region ( Translation Properties )

        /// <summary>
        /// Adopt query and generate layout, clause, and orders from
        /// the contents.
        /// </summary>

        public void Translate(AbstractUserPrincipal principal, Query rQuery, PairSet includeTheseFields, FieldTable fTable, bool maskSsn)
        {
            // Initialize components and then recursively
            // translate the elements.

            m_Layout = String.Empty;
            m_Clause = String.Empty;
            m_Orders = String.Empty;

            StringBuilder sbClause = new StringBuilder();

            StringBuilder sbLayout = new StringBuilder(500);

            m_Params.Clear();

            if (rQuery == null)
            {
                throw new ArgumentNullException("rQuery", "Null report query instance not supported.");
            }

            if (fTable == null)
            {
                throw new ArgumentNullException("fTable", "Null field lookup table not supported.");
            }
            
            foreach (Column cColumn in rQuery.Columns)
            {
                // Get the field layout by dumping columns in order.  We need
                // to translate relative fields into their sql representation.
                // To do this effectively, the field should cough up an expression
                // that includes all the dependencies (we'll need it again when
                // we generate the predicate) and their arithmetic relationship
                // to the result.
                //
                // 2/28/2005 kb - If something fails with the selection fetch,
                // then we put in null as a placeholder.  If we see all nulls
                // for a column, then suspect that the schema is messed up.
                // When 'null' shows up in a select statement, then you know
                // the schema is wiggn'.
                
                if (this.HandleSpecialCaseFieldSelect(cColumn.Id, maskSsn, fTable, sbLayout, includeTheseFields))
                {
                    continue;
                }

                Field cField = fTable.LookupById(cColumn.Id);
                string sSpec;

                if (cField == null)
                {
                    throw new ArgumentException("Query column not found in table.", cColumn.Id);
                }

                sSpec = cField.Selection;

                if (sSpec.Length == 0)
                {
                    sSpec = "null";
                }

                if (sbLayout.Length > 0)
                {
                    sbLayout.Append(" ,");
                }

                if (cField.Mapping.Count == 0)
                {
                    if (sSpec == cField.Id)
                        sbLayout.AppendFormat(" {0}", sSpec);
                    else
                        sbLayout.AppendFormat(" {0} as {1}", sSpec, cField.Id);
                }
                else
                {
                    sbLayout.AppendFormat(" case( {0} )", sSpec);

                    foreach (string sVal in cField.Mapping)
                    {
                        string sKey = cField.Mapping.ByVal[sVal];

                        if (cField.Type == Field.E_FieldType.Enu)
                        {
                            sbLayout.AppendFormat(" when '{0}' then '{1}'", sKey, sVal);
                        }
                        else
                        {
                            sbLayout.AppendFormat(" when {0} then '{1}'", sKey, sVal);
                        }
                    }
                    sbLayout.AppendFormat(" end as {0}", cField.Id);
                }
            }

            HashSet<string> couldBeShareWithExecutingEngineFields = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
            if (includeTheseFields != null)
            {
                // 7/14/2005 kb - Tack these fields onto the tail so we can pull
                // necessary details out of the db as well.  We currently use
                // this hook to combine query processing and access control
                // lookup (see case 2354).

                foreach (PairSet.Entry eField in includeTheseFields)
                {
                    if (eField == null || eField.A == String.Empty || eField.B == String.Empty)
                    {
                        continue;
                    }

                    if (sbLayout.Length > 0)
                        sbLayout.Append(" , ");

                    if (eField.A == eField.B)
                        sbLayout.Append(eField.A);
                    else
                        sbLayout.AppendFormat("{0} as {1}", eField.A, eField.B);

                    couldBeShareWithExecutingEngineFields.Add(eField.B);

                }
            }

            // 9/25/2010 dd - Automatically add fields that require from new ExecutingEngine. To avoid collide with the
            // existing field add by user, I prefix all fields with "engine_"
            IEnumerable<string> engineFieldDependencyList = LendingQBExecutingEngine.GetDependencyFieldsByOperation(principal.BrokerId,
                WorkflowOperations.WriteLoanOrTemplate,
                WorkflowOperations.ReadLoanOrTemplate,

                // 11/26/2012 dd - The pipeline need dependency from these 3 operations.
                WorkflowOperations.RunPmlForAllLoans,
                WorkflowOperations.RunPmlForRegisteredLoans,
                WorkflowOperations.RunPmlToStep3
                );
            foreach (var o in engineFieldDependencyList)
            {
                // 9/25/2010 dd - Only append field in loan file cache table.
                CFieldDbInfoList fieldDbInfoList = CFieldDbInfoList.TheOnlyInstance;
                CFieldDbInfo field = fieldDbInfoList.Get(o);
                if (null == field || field.IsCachedField == false)
                {
                    continue; // 9/25/2010 dd - These fields must belong to loan file cache.
                }
                if (couldBeShareWithExecutingEngineFields.Contains(o))
                {
                    // 1/26/2015 dd - Skip - This is already part of the query.
                    continue;
                }
                if (sbLayout.Length > 0)
                {
                    sbLayout.Append(", ");
                }
                sbLayout.AppendFormat("{0} as {1}{0}", o, ConstAppDavid.CustomReportEngineFieldPrefix);
            }
            m_Layout = sbLayout.ToString();

            if (rQuery.Relates.Count > 0)
            {
                // Now transform the query's relates (the conditions designed by the
                // user when constructing the query).  We concat with 'and'.

                String sXform = Xform(principal.BrokerId, rQuery.Relates, m_Params, fTable).ToString();

                if (sbClause.Length > 0)
                {
                    if (sXform.Length > 0)
                    {
                        sbClause.Append(" and " + sXform);
                    }
                }
                else
                {
                    sbClause = new StringBuilder(sXform);
                }
            }

            if (rQuery.Filters.Count > 0)
            {
                // Now transform the query's filters (the other half of the predicate
                // imposed by the editor).  We concat with 'and'.

                String sXform = Xform(principal.BrokerId, rQuery.Filters, m_Params, fTable).ToString();

                if (sbClause.Length > 0)
                {
                    if (sXform.Length > 0)
                    {
                        sbClause.Append(" and " + sXform);
                    }
                }
                else
                {
                    sbClause = new StringBuilder(sXform);
                }
            }

            m_Clause = sbClause.ToString();

            var orderByExpressions = new HashSet<string>();

            if (rQuery.Sorting.Count > 0 || rQuery.GroupBy.Count > 0)
            {
                // Get all the normal grouped by fields and sort by them first
                // because table grouping will have the highest sorting priority.
                // The group descriptions with a non-zero option are crossed,
                // and make up the next highest priority of sorting.  After this,
                // we add in all the sorted columns.
                //
                // If any columns are missing from the query, or not defined as
                // a valid field, then we punt.

                Descriptors aDesc = new Descriptors();

                foreach (Descriptor gDesc in rQuery.GroupBy)
                {
                    if (gDesc.Option != 0)
                    {
                        continue;
                    }

                    var desc = new Descriptor(gDesc);
                    desc.IsGroupBy = true;
                    aDesc.Add(desc);
                }

                foreach (Descriptor gDesc in rQuery.GroupBy)
                {
                    if (gDesc.Option == 0)
                    {
                        continue;
                    }

                    var desc = new Descriptor(gDesc);
                    desc.IsGroupBy = true;
                    aDesc.Add(desc);
                }

                foreach (Descriptor gDesc in aDesc)
                {
                    if (rQuery.Columns.Has(gDesc.Id) == false)
                    {
                        throw new ArgumentException("Group by column not found in query.", gDesc.Id);
                    }
                }

                List<Descriptor> missingSortColumns = new List<Descriptor>();
                foreach (Descriptor sDesc in rQuery.Sorting)
                {
                    if (rQuery.Columns.Has(sDesc.Id) == false)
                    {
                        missingSortColumns.Add(sDesc);
                        continue;
                    }

                    aDesc.Add(sDesc);
                }

                foreach (Descriptor descriptor in missingSortColumns)
                {
                    rQuery.Sorting.Items.Remove(descriptor);
                }

                foreach (Descriptor oDesc in aDesc)
                {
                    // Get the field descriptor so we can insert the proper
                    // sorting spec for this column.  Some columns sort by
                    // their raw value, some by their mapped value, and
                    // some mapped columns sort by their raw (rare, though).
                    //
                    // 4/4/2005 kb - Revising the sorting to use the mapping
                    // to enforce an arbitrary ordering for each mapped
                    // column.  If not mapped, we use the selection text of
                    // the column's field.  For all fields, the selection
                    // text is it's id, unless it's a derived field.  In
                    // that case, we want the derived value for sorting.
                    // If the derived field has mapping, then we will build
                    // a sorting spec using the mapping and the derived
                    // value for that column.

                    Field oField = fTable.LookupById(oDesc.Id);

                    if (oField != null)
                    {
                        if (oField.Kind == Field.E_ClassType.LongText)
                        {
                            // 7/31/2006 dd - If sort column has type TEXT in database then it cannot be sort.
                            continue;
                        }
                        // Generate order by clause in terms of the physical
                        // column names, or some expression derived from such.

                        StringBuilder sSpec;
                        if (oDesc.IsGroupBy)
                        {
                            sSpec = new StringBuilder(oField.GroupBySelection);
                        }
                        else
                        {
                            sSpec = new StringBuilder(oField.Selection);
                        }

                        String sOrder = "asc";

                        if (oField.Mapping.Count > 0)
                        {
                            sSpec.Insert(0, "case( ");
                            sSpec.Append(")");

                            int i = 0;

                            foreach (String sVal in oField.Mapping)
                            {
                                String sKey = oField.Mapping.ByVal[sVal];

                                sSpec.Append(" when '" + sKey + "' then " + i);

                                ++i;
                            }

                            sSpec.Append(" else " + i + " end");
                        }

                        foreach (Descriptor sDesc in rQuery.Sorting)
                        {
                            if (sDesc.Id == oDesc.Id && sDesc.Option != 0)
                            {
                                sOrder = "desc";

                                break;
                            }
                        }

                        orderByExpressions.Add(sSpec.ToString() + " " + sOrder);
                    }
                    else
                    {
                        throw new ArgumentException("Order by field not found in query.", oDesc.Id);
                    }
                }

                m_Orders = string.Join(", ", orderByExpressions.ToArray());
            }
        }

        /// <summary>
        /// Handles special fields in a select statement for a query.
        /// </summary>
        /// <param name="fieldId">
        /// The ID of the field.
        /// </param>
        /// <param name="maskSsn">
        /// True if SSN values are being masked, false otherwise.
        /// </param>
        /// <param name="fieldLookup">
        /// The lookup table for custom report fields.
        /// </param>
        /// <param name="queryBuilder">
        /// The string builder for the query.
        /// </param>
        /// <param name="includeTheseFields">
        /// The list of additional fields to include.
        /// </param>
        /// <returns>
        /// True if a special field was handled, false otherwise.
        /// </returns>
        private bool HandleSpecialCaseFieldSelect(string fieldId, bool maskSsn, FieldTable fieldTable, StringBuilder queryBuilder, PairSet includeTheseFields)
        {
            var field = fieldTable.LookupSpecialField(fieldId);
            if (field == null)
            {
                return false;
            }

            if (queryBuilder.Length > 0)
            {
                queryBuilder.Append(" , ");
            }

            queryBuilder.Append(field.Selection);

            if (!maskSsn && field.DependentFields != null)
            {
                foreach (var dependentFieldPair in field.DependentFields)
                {
                    includeTheseFields.Add(dependentFieldPair.Key, dependentFieldPair.Value);
                }
            }

            return true;
        }

        /// <summary>
        /// Produce sql-like predicate using condition tree.
        /// </summary>
        /// <param name="cConditions">
        /// Condition tree (composite) to transform.
        /// </param>
        /// <returns>
        /// String representation of boolean relationship using common
        /// arithmetic operators (typical of a basic sql predicate).
        /// </returns>

        private StringBuilder Xform(Guid brokerId, Conditions cConditions, StringDictionary aParams, FieldTable fTable)
        {
            // Take the current conditions tree and traverse it in
            // order to produce a parenthetically nested where clause.

            StringBuilder sC = new StringBuilder();

            try
            {
                // Walk the set and append what bubbles up.  We need
                // to ignore empty results from the children because
                // empty groups need to stay silent -- not affecting
                // the select.

                foreach (Object oItem in cConditions)
                {
                    // Get the child contribution and include
                    // if non-empty.

                    StringBuilder sS = new StringBuilder(); ;

                    if (oItem is Conditions)
                    {
                        sS = Xform(brokerId, oItem as Conditions, aParams, fTable);
                    }
                    else
                        if (oItem is Condition)
                        {
                            sS = Xform(brokerId, oItem as Condition, aParams, fTable);
                        }

                    if (sC.Length != 0 && sS.Length != 0)
                    {
                        switch (cConditions.Type)
                        {
                            case E_ConditionsType.An: sC.Append(" and " + sS.ToString());
                                break;

                            case E_ConditionsType.Or: sC.Append(" or " + sS.ToString());
                                break;
                        }
                    }
                    else if (sS.Length != 0)
                    {
                        sC.Append(sS.ToString());
                    }
                }

                if (sC.Length != 0)
                {
                    // Nest the group with explicit parentheses.

                    sC.Insert(0, "( ");
                    sC.Append(" )");
                }
            }
            catch (Exception e)
            {
                // Share the love;

                throw e;
            }

            return sC;
        }

        /// <summary>
        /// Produce sql-like predicate using condition item.
        /// </summary>
        /// <param name="cConditions">
        /// Condition tree (composite) to transform.
        /// </param>
        /// <returns>
        /// String representation of boolean relationship using common
        /// arithmetic operators (typical of a basic sql predicate).
        /// </returns>

        private StringBuilder Xform(Guid brokerId, Condition cCondition, StringDictionary aParams, FieldTable fTable)
        {
            // Take the current condition leaf and write it out to
            // the string.

            StringBuilder sC = new StringBuilder();

            try
            {
                // Write out the details of this condition.
                //
                // 12/13/2004 kb - We now generate script to handle
                // the empty argument case and assume we're looking
                // for non-defined cells.

                Field cField = fTable.LookupById(cCondition.Id), aField = null;

                if (cCondition.Type == E_ConditionType.Eq &&
                    cCondition.Argument.Type == E_ArgumentType.Const &&
                    cCondition.Argument.Value == null)
                {
                    sC.Append("(");
                    sC.Append(cField.Id);
                    sC.Append(" is null)");
                    return sC;
                }

                if (cCondition.Argument.Type == E_ArgumentType.Field)
                {
                    aField = fTable.LookupById(cCondition.Argument.Value);
                }

                // opm 459868 - when trying to compare text data type with varchar, convert the text into varchar first
                if (cField.Kind == Field.E_ClassType.LongText && (
                    cCondition.Type == E_ConditionType.Ne ||
                    cCondition.Type == E_ConditionType.Eq ||
                    cCondition.Type == E_ConditionType.Ge ||
                    cCondition.Type == E_ConditionType.Gt ||
                    cCondition.Type == E_ConditionType.Le ||
                    cCondition.Type == E_ConditionType.Lt))
                {
                    sC.Append("Convert(varchar(max), " + cField.Id + ")");
                }
                else if (cCondition.Argument.Type != E_ArgumentType.Const || cCondition.Argument.Value != "")
                {
                    if (cField.Kind == Field.E_ClassType.Cal)
                    {
                        if (cCondition.Argument.Type != E_ArgumentType.Field)
                        {
                            sC.Append("datediff( dd , '" + cCondition.Argument.Value + "' , " + cField.Selection + " )");
                        }
                        else
                        {
                            if (aField is LastBusinessDays)
                            {
                                sC.Append("datediff( dd , " + ((LastBusinessDays)aField).GetSelectionByBrokerId(brokerId) + " , " + cField.Selection + " )");
                            }
                            else
                            {
                                sC.Append("datediff( dd , " + aField.Selection + " , " + cField.Selection + " )");
                            }
                        }
                    }
                    else
                    {
                        sC.Append(cField.Selection);
                    }
                }
                else
                {
                    sC.Append(cField.Selection);
                }

                if (cField.Type == Field.E_FieldType.Str || cCondition.Argument.Type != E_ArgumentType.Const || cCondition.Argument.Value != "")
                {
                    switch (cCondition.Type)
                    {
                        case E_ConditionType.Nw: sC.Append(" not like ");
                            break;

                        case E_ConditionType.Bw: sC.Append(" like ");
                            break;

                        case E_ConditionType.Eq: sC.Append("  = ");
                            break;

                        case E_ConditionType.Ne: sC.Append(" <> ");
                            break;

                        case E_ConditionType.Ge: sC.Append(" >= ");
                            break;

                        case E_ConditionType.Gt: sC.Append("  > ");
                            break;

                        case E_ConditionType.Le: sC.Append(" <= ");
                            break;

                        case E_ConditionType.Lt: sC.Append("  < ");
                            break;
                        default:
                            DataAccess.Tools.LogBug("Unhandle E_ConditionType::" + cCondition.Type);
                            break;
                    }

                    if (cField.Kind != Field.E_ClassType.Cal)
                    {
                        switch (cField.Kind)
                        {
                            case Field.E_ClassType.Cnt: sC.Append("convert( int     , ");
                                break;

                            case Field.E_ClassType.Csh: sC.Append("convert( money   , ");
                                break;

                            case Field.E_ClassType.Pct: sC.Append("convert( float     , ");
                                break;

                            case Field.E_ClassType.LongText:
                            case Field.E_ClassType.Txt:
                                // 12/21/06 mf. OPM 7532 convert( varchar, '...' ) defaults to varchar(30),
                                // so longer arguments are producing inaccurate reports.
                                // Changing so that we convert based on the length of our argument.								
                                // In the long term we may want to work field length into the schema, but
                                // for now, this will only provide a benefit when comparing two columns,
                                // which is an uncommon case.								

                                int length;
                                if (cCondition.Argument.Type == E_ArgumentType.Const)
                                {
                                    if (cCondition.Type == E_ConditionType.Bw || cCondition.Type == E_ConditionType.Nw)
                                    {
                                        length = cCondition.Argument.Value.Length + 1; // Wildcard character
                                    }
                                    else
                                    {
                                        // convert() does not allow zero-length datatype, so for empty string we use varchar(1)
                                        length = (cCondition.Argument.Value.Length == 0) ? 1 : cCondition.Argument.Value.Length;
                                    }
                                }
                                else
                                {
                                    length = 100; // Max of cached fields
                                }

                                sC.Append("convert( varchar( " + length + " ) , ");
                                break;
                            default:
                                DataAccess.Tools.LogBug("Unhandle Field.E_ClassType::  " + cField.Kind);
                                break;
                        }

                        if (cCondition.Argument.Type == E_ArgumentType.Field)
                        {
                            // Add the field id as a table column for the argument, or
                            // right hand side of the condition.  We don't worry about
                            // sql injection attacks here, because only valid field
                            // names are allowed (we should really verify that).

                            sC.Append(aField.Selection);
                        }
                        else
                            if (cCondition.Argument.Type == E_ArgumentType.Const)
                            {
                                // We DO worry about sql injection attacks here.  To protect
                                // our report processor, we use parameters for each argument
                                // value.  We assume that parameters are considered directly
                                // by the database query processor.

                                String paramId = "@arg" + aParams.Count.ToString();

                                if (cCondition.Type == E_ConditionType.Bw || cCondition.Type == E_ConditionType.Nw)
                                {
                                    aParams.Add(paramId, cCondition.Argument.Value + "%");
                                }
                                else
                                {
                                    aParams.Add(paramId, cCondition.Argument.Value);
                                }

                                sC.Append(paramId);
                            }
                            else
                            {
                                sC.Append("???");
                            }

                        sC.Append(" )");
                    }
                    else
                    {
                        sC.Append("0");
                    }

                    if (cCondition.Type == E_ConditionType.Ne || cCondition.Type == E_ConditionType.Nw)
                    {
                        if (cField.Type != Field.E_FieldType.Str)
                        {
                            sC.Append(" or " + cField.Selection + " is null");
                        }
                    }
                }
                else
                {
                    // 12/13/2004 kb - If we pass in an empty string as an argument
                    // then it is assumed that null values are also interesing to
                    // the user.  The rendering code may throw out nulls, or mask
                    // them with an individual dot.  They won't contribute to any
                    // aggregate calculation results, but they should show up.
                    // Note that we already handle the not-equal-to-empty (!= '')
                    // case for all not-equal conditions.

                    if (cCondition.Type == E_ConditionType.Eq)
                    {
                        if (cCondition.Argument.Type == E_ArgumentType.Const && cCondition.Argument.Value.TrimWhitespaceAndBOM() == "")
                        {
                            sC.Append(" is null");
                        }
                    }
                    else
                        if (cCondition.Type == E_ConditionType.Ne)
                        {
                            if (cCondition.Argument.Type == E_ArgumentType.Const && cCondition.Argument.Value.TrimWhitespaceAndBOM() == "")
                            {
                                sC.Append(" is not null");
                            }
                        }
                }

                //Surround with Parentheses
                if (sC.Length != 0)
                {
                    sC.Insert(0, "(");
                    sC.Append(")");
                }
            }
            catch (Exception e)
            {
                // Share the love;

                throw e;
            }

            return sC;
        }

        /// <summary>
        /// Each translation will generate details from the given query,
        /// but we keep separate the source of the data set.  Typically,
        /// the source is specific to the use of the query translator.
        /// </summary>

        public String Source
        {
            set { m_Source = value; }
            get { return m_Source; }
        }

        /// <summary>
        /// Provide string-based description of the fields of interest
        /// according to the internal representation of the query's
        /// columns.
        /// </summary>
        public String Layout
        {
            get { return m_Layout; }
        }

        /// <summary>
        /// Capture the query's predicate in as an infix expression
        /// using 'and' and 'or' operations with parenthesis for
        /// grouping.
        /// </summary>
        public String Clause
        {
            get { return m_Clause; }
        }

        /// <summary>
        /// Provide a string-based representation of the columns
        /// (listed by position, base-relative to '1') and the
        /// direction of sorting.
        /// </summary>

        public String Orders
        {
            get { return m_Orders; }
        }

        /// <summary>
        /// Keep track of parameters we add (as strings) to the
        /// query during translation.  All input arguments need
        /// to be packaged as parameters to avoid sql injection
        /// attacks.
        /// </summary>

        public StringDictionary Params
        {
            get { return m_Params; }
        }

        #endregion

    }


}
