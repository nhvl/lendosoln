///
/// Author: David Dao
///
using System;
using DataAccess;

namespace LendersOffice.QueryProcessor
{
    /// <summary>
    /// Base class for catching report processing errors.
    /// </summary>
    public class ReportingException : CBaseException
    {
        /// <summary>
        /// Construct a reporting exception.
        /// </summary>
        /// <param name="sReason">
        /// Message that describes the error.
        /// </param>

        public ReportingException( string sReason )
            : base(sReason, sReason)
        {
        }
        public ReportingException(string sReason, Exception e) : base(sReason, e)
        {
        }

    }

    public abstract class AbstractReportFieldException : ReportingException 
    {
        private string m_Id;
        public string Id 
        {
            get { return m_Id; }
        }

        protected AbstractReportFieldException(string sReason, string sId) : base (sReason) 
        {
            m_Id = sId;
        }
    }

    /// <summary>
    /// Thrown when fields are not unique during schema initialization.
    /// </summary>
    public class FieldNotUnique : AbstractReportFieldException
    {
        /// <summary>
        /// Construct a reporting exception.
        /// </summary>
        /// <param name="sName">
        /// Name of offending field.
        /// </param>
        public FieldNotUnique( string sId ) : base( "Field not unique (" + sId + ")", sId )
        {
        }
    }

    /// <summary>
    /// Thrown when fields are not part of the reportable
    /// class schema.
    /// </summary>
    public class FieldNotValid : AbstractReportFieldException
    {
        public FieldNotValid( string sId ) : base( "Field not valid (" + sId + ")", sId )
        {
        }
    }


    /// <summary>
    /// Something failed when processing a serialized document.
    /// </summary>
    public class InvalidFormat : ReportingException
    {
        /// <summary>
        /// Construct a reporting exception.
        /// </summary>
        public InvalidFormat( Exception eEx ) : base( "Document doesn't conform: " + eEx.Message, eEx)
        {
        }

        /// <summary>
        /// Construct a reporting exception.
        /// </summary>
        public InvalidFormat() : base( "Document doesn't conform" )
        {
        }

    }

    /// <summary>
    /// The reportable field should have mapping information,
    /// but none was found.
    /// </summary>
    public class FieldLacksMapping : AbstractReportFieldException
    {
        public FieldLacksMapping( string sId , Exception eEx ) : base( "Field lacks mapping ( " + sId + " ): " + eEx.Message, sId )
        {
        }

        /// <summary>
        /// Construct a reporting exception.
        /// </summary>

        public FieldLacksMapping( string sId ) : base( "Field lacks mapping ( " + sId + " )", sId )
        {
        }

    }


    /// <summary>
    /// We were unable to initialize the field using the id and
    /// the given containing class.
    /// </summary>
    public class FieldFailed : AbstractReportFieldException
    {
        public FieldFailed( string sId , Exception eEx ) : base( "Unable to initialize field ( " + sId + " ): " + eEx.Message, sId )
        {
        }

        public FieldFailed( string sId ) : base( "Unable to initialize field ( " + sId + " )", sId )
        {
        }

    }

    /// <summary>
    /// Something failed when initializing the reportabel field
    /// schema for some specialized domain.
    /// </summary>

    public class SchemaInitializationFailed : ReportingException
    {
        public SchemaInitializationFailed( Exception eEx ) : base( "Unable to build up schema: " + eEx.Message )
        {
        }

        public SchemaInitializationFailed() : base( "Unable to build up schema" )
        {
        }

    }

    /// <summary>
    /// Something failed when initializing the reportabel field
    /// schema for some specialized domain.
    /// </summary>
    public class SchemaTranslationFailed : ReportingException
    {
        public SchemaTranslationFailed( Exception eEx ) : base( "Unable to set new schema translation: " + eEx.Message )
        {
        }

        public SchemaTranslationFailed() : base( "Unable to build up schema" )
        {
        }

    }

    /// <summary>
    /// Something failed when testing a field's conditions against
    /// the field value.
    /// </summary>

    public class ConditionTestingFailed : ReportingException
    {
        public ConditionTestingFailed( Exception eEx ) : base( "Unable to test conditions: " + eEx.Message )
        {
        }

        public ConditionTestingFailed() : base( "Unable to test conditions" )
        {
        }

    }

    /// <summary>
    /// Thrown when fields are inaccessible or null within
    /// returned reportable objects that form the resulting
    /// report table.
    /// </summary>

    public class ProcessingAborted : ReportingException
    {
        public ProcessingAborted( Exception eEx ) : base( "Report processing aborted: " + eEx.Message )
        {
        }

        public ProcessingAborted() : base( "Report processing aborted" )
        {
        }

    }

}
