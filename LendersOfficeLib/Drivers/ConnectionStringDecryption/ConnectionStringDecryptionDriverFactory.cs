﻿namespace LendersOffice.Drivers.ConnectionStringDecryption
{
    using CommonLib;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;

    /// <summary>
    /// Factory for creation of a ConnectionStringDecryption driver.
    /// </summary>
    public sealed class ConnectionStringDecryptionDriverFactory : IConnectionStringDecryptionDriverFactory
    {
        /// <summary>
        /// Storing the data in a reference type for thread safety.
        /// </summary>
        private LoadedDriver loadedFile;

        /// <summary>
        /// Create an implementation of the IConnectionStringDecryptionDriver interface.
        /// for reading the key configuration from an xml file.
        /// </summary>
        /// <remarks>Will throw exception if the file is not found or cannot be loaded.</remarks>
        /// <param name="configFileLocation">The expected location of the config file.</param>
        /// <returns>An implementation of the IConnectionStringDecryptionDriver interface.</returns>
        public IConnectionStringDecryptionDriver CreateXmlFileDecrypter(LocalFilePath configFileLocation)
        {
            // Note: Factories are shared across the app, so essentially all
            // members should be treated as if shared static.  Additionally,
            // we want to avoid expensive thread coordination through locking.
            // We are using the ModifiedFileExpiringResource to coordinate the 
            // file modification monitoring.
            // Finally, we also avoid the use of static constructor for the
            // setup because factory creation is generally not intended to have 
            // any complex logic or throw exception.

            // Read shared data by local reference only to make sure our 
            // data does not change under us.  Use a class to combine all
            // data to ensure we can change all at once with a thread-safe
            // reference change.
            var currentLoadedDriver = this.loadedFile;

            if (currentLoadedDriver == null)
            {
                // Nothing had been loaded yet at the time we tried to pull 
                // a copy.  We create a new driver instance to load the file
                // and associated file modification monitor.

                // It is possible that another thread has started or completed
                // this load, but we do not care.  The end result will be same.
                // We are assuming the file path will not change in the app 
                // lifetime. Currently it can only come from an application 
                // config, which would cause an app restart upon a change in a 
                // web app, and executables would likely be read once at start.
                var driver = new XmlConfigFileConnectionStringDecryptionDriver(configFileLocation);
                var modifiedFileExpiringResource = new ModifiedFileExpiringResource(
                        configFileLocation.Value,
                        new ExpiringResource.HandleExpiredDelegate(this.ReloadDriver));

                currentLoadedDriver = LoadedDriver.Create(driver, modifiedFileExpiringResource, configFileLocation);
                this.loadedFile = currentLoadedDriver;
                return currentLoadedDriver.DecryptionDriver;
            }

            currentLoadedDriver.ModifiedFileExpiringResource.CheckExpired(this);
            currentLoadedDriver = this.loadedFile;

            return currentLoadedDriver.DecryptionDriver;
        }

        /// <summary>
        /// Reload the driver.
        /// </summary>
        private void ReloadDriver()
        {
            var localLoadedFile = this.loadedFile;

            // We must always create new.
            var newLoadedFile = LoadedDriver.Create(
                 new XmlConfigFileConnectionStringDecryptionDriver(localLoadedFile.ConfigFilePath),
                 localLoadedFile.ModifiedFileExpiringResource,
                 localLoadedFile.ConfigFilePath);

            this.loadedFile = newLoadedFile;
        }

        /// <summary>
        /// A simple class to hold the data loaded driver data.
        /// </summary>
        /// <remarks>It is very intentional that there are no public setters
        /// for these properties, because for thread-safety, we would always 
        /// want to create a new instance to avoid a mixed read.</remarks>
        private class LoadedDriver
        {
            /// <summary>
            /// Prevents a default instance of the <see cref="LoadedDriver" /> class from being created.
            /// </summary>
            private LoadedDriver()
            {
            }

            /// <summary>
            /// Gets the decryption driver.
            /// </summary>
            public IConnectionStringDecryptionDriver DecryptionDriver { get; private set; }

            /// <summary>
            /// Gets the location of the config file.
            /// </summary>
            public LocalFilePath ConfigFilePath { get; private set; }

            /// <summary>
            /// Gets a ModifiedFileExpiringResource that coordinates the reloading of the file.
            /// </summary>
            public ModifiedFileExpiringResource ModifiedFileExpiringResource { get; private set; }

            /// <summary>
            /// Create a new class instance.
            /// </summary>
            /// <param name="decryptionDriver">Decryption driver.</param>
            /// <param name="modifiedFileExpiringResource">The ModifiedFileExpiringResource.</param>
            /// <param name="configFilePath">Config file path.</param>
            /// <returns>A new instance.</returns>
            public static LoadedDriver Create(IConnectionStringDecryptionDriver decryptionDriver, ModifiedFileExpiringResource modifiedFileExpiringResource, LocalFilePath configFilePath)
            {
                return new LoadedDriver()
                {
                    DecryptionDriver = decryptionDriver,
                    ModifiedFileExpiringResource = modifiedFileExpiringResource,
                    ConfigFilePath = configFilePath
                };
            }
        }
    }
}
