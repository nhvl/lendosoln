﻿namespace LendersOffice.Drivers.ConnectionStringDecryption
{
    using System;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Xml.Linq;
    using Base64Encoding;
    using DataEncryption;
    using LqbGrammar;
    using LqbGrammar.Adapters;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;
    using LqbGrammar.Drivers.FileSystem;

    /// <summary>
    /// Decrypt connection strings loaded from xml file.
    /// </summary>
    internal sealed class XmlConfigFileConnectionStringDecryptionDriver : IConnectionStringDecryptionDriver
    {
        /// <summary>
        /// Parsed encryption key data.
        /// </summary>
        private EncryptionKey currentKey;

        /// <summary>
        /// Parsed encryption key data.
        /// </summary>
        private EncryptionKey previousKey;

        /// <summary>
        /// Initializes a new instance of the <see cref="XmlConfigFileConnectionStringDecryptionDriver"/> class.
        /// </summary>
        /// <param name="configFileLocation">The expected location of the config file.</param>
        public XmlConfigFileConnectionStringDecryptionDriver(LocalFilePath configFileLocation)
        {
            this.LoadKeysFromFile(configFileLocation);
        }

        /// <summary>
        /// Tries to decrypt the given connection string.
        /// </summary>
        /// <param name="encryptedConnectionString">The encrypted connection string.</param>
        /// <param name="decryptedConnectionString">The resulting decrypted connection string.</param>
        /// <returns>True if decryption was successful.  Otherwise false.</returns>
        public bool TryDecryptConnectionString(DBConnectionString encryptedConnectionString, out DBConnectionString decryptedConnectionString)
        {
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(encryptedConnectionString.Value);

            string userId;
            if (!this.TryDecryptWithFallback(builder.UserID, out userId))
            {
                decryptedConnectionString = DBConnectionString.Invalid;
                return false;
            }

            string password;
            if (!this.TryDecryptWithFallback(builder.Password, out password))
            {
                decryptedConnectionString = DBConnectionString.Invalid;
                return false;
            }

            DBConnectionString? newConnectionString = null;
            try
            {
                builder.UserID = userId;
                builder.Password = password;
                newConnectionString = DBConnectionString.Create(builder.ConnectionString);
            }
            catch (SystemException)
            {
                decryptedConnectionString = DBConnectionString.Invalid;
                return false;
            }

            if (newConnectionString.HasValue)
            {
                decryptedConnectionString = newConnectionString.Value;
                return decryptedConnectionString != DBConnectionString.Invalid;
            }
            else
            {
                decryptedConnectionString = DBConnectionString.Invalid;
                return false;
            }
        }

        /// <summary>
        /// Reads the file as xml.
        /// </summary>
        /// <param name="filePath">Location of the xml file.</param>
        /// <returns>The xml document loaded from the file.</returns>
        private static LqbXmlElement LoadXmlFromPath(LocalFilePath filePath)
        {
            var factory = GenericLocator<IFileSystemDriverFactory>.Factory;
            var result = factory.CreateXmlFileDriver();
            return result.Load(filePath);
        }

        /// <summary>
        /// Parse the key from the xml.
        /// </summary>
        /// <param name="lqbXmlDoc">The xml doc.</param>
        /// <param name="node">The node name to be loaded.</param>
        /// <returns>The parsed encryption key data.</returns>
        private static EncryptionKey ParseKeyFromXml(LqbXmlElement lqbXmlDoc, string node)
        {
            var xmlDoc = lqbXmlDoc.Contained;

            var keyElement = xmlDoc.Descendants().Where(p => p.Name == node).First();
            var key = LoadKeyFromXmlElement(keyElement);

            return key;
        }

        /// <summary>
        /// Load the encryption key from the xml element describing it.
        /// </summary>
        /// <param name="keyElement">Xml element describing the key.</param>
        /// <returns>EncryptionKey built from the xml element.</returns>
        private static EncryptionKey LoadKeyFromXmlElement(XElement keyElement)
        {
            var keyString = keyElement.Elements().First(p => p.Name == "Key").Value;

            if (keyString == string.Empty)
            {
                // A blank key. Should be treated as not decrypting.
                return null;
            }

            var iVString = keyElement.Elements().First(p => p.Name == "IV").Value;
            var versionString = keyElement.Attribute("version").Value;

            var iVBase64 = Base64EncodedData.Create(iVString);
            var keyBase64 = Base64EncodedData.Create(keyString);

            var key = Base64EncodingDriverHelper.Decode(keyBase64.Value);
            var iv = Base64EncodingDriverHelper.Decode(iVBase64.Value);

            var version = Convert.ToInt32(versionString);

            return CreateEncryptionKey(version, key, iv);
        }
        
        /// <summary>
        /// Create an encryption key from its component parts.
        /// </summary>
        /// <param name="version">The version number for the encryption key.</param>
        /// <param name="key">The encryption key.</param>
        /// <param name="iv">The optional initialization vector, or null.</param>
        /// <returns>The initialized encryption key.</returns>
        private static EncryptionKey CreateEncryptionKey(int version, byte[] key, byte[] iv)
        {
            var factory = GenericLocator<IDataEncryptionAdapterFactory>.Factory;
            return factory.CreateAesKey(version, key, iv, null);
        }

        /// <summary>
        /// Load the keys from the config file.
        /// </summary>
        /// <param name="configFileLocation">The expected location of the config file.</param>
        private void LoadKeysFromFile(LocalFilePath configFileLocation)
        {
            var lqbXmlDoc = LoadXmlFromPath(configFileLocation);
            this.currentKey = ParseKeyFromXml(lqbXmlDoc, "Current");
            this.previousKey = ParseKeyFromXml(lqbXmlDoc, "Previous");
        }

        /// <summary>
        /// Decrypts the given value by trying the current key and
        /// falling back to the previous key upon failure.
        /// </summary>
        /// <param name="encodedEncryptedValue">The encoded encrypted connection string.</param>
        /// <param name="decryptedValue">The resulting decrypted string.</param>
        /// <returns>True if decryption was successful.  Otherwise false.</returns>
        private bool TryDecryptWithFallback(string encodedEncryptedValue, out string decryptedValue)
        {
            if (this.TryDecryptWithSingleKey(encodedEncryptedValue, this.currentKey, out decryptedValue))
            {
                return true;
            }
            else if (this.previousKey != null)
            {
                return this.TryDecryptWithSingleKey(encodedEncryptedValue, this.previousKey, out decryptedValue);
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Decrypts the given value with the given key.
        /// </summary>
        /// <param name="encodedEncryptedValue">The encoded and encrypted value.</param>
        /// <param name="encryptionKey">The encryption key to use.</param>
        /// <param name="decryptedValue">The resulting decrypted value.</param>
        /// <returns>True if decryption was successful.</returns>
        private bool TryDecryptWithSingleKey(string encodedEncryptedValue, EncryptionKey encryptionKey, out string decryptedValue)
        {
            if (encryptionKey == null)
            {
                decryptedValue = encodedEncryptedValue;
                return false;
            }

            var encodedEncryptedData = Base64EncodedData.Create(encodedEncryptedValue);
            if (encodedEncryptedData.HasValue)
            {
                byte[] decryptedResult;

                var encryptedValue = Base64EncodingDriverHelper.Decode(encodedEncryptedData.Value);

                if (DataEncryptionDriverHelper.TryDecrypt(encryptedValue, encryptionKey, out decryptedResult))
                {
                    decryptedValue = System.Text.Encoding.UTF8.GetString(decryptedResult);
                    return true;
                }
                else
                {
                    decryptedValue = encodedEncryptedValue;
                    return false;
                }
            }
            else
            {
                decryptedValue = encodedEncryptedValue;
                return false;
            }
        }
    }
}
