﻿namespace LendersOffice.Drivers.ConnectionStringDecryption
{
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;

    /// <summary>
    /// Factory for creating a mock ConnectionStringDecryptionDriver.
    /// </summary>
    public sealed class MockConnectionStringDecryptionDriverFactory : IConnectionStringDecryptionDriverFactory
    {
        /// <summary>
        /// Create a mock ConnectionStringDecryptionDriver.
        /// </summary>
        /// <param name="configFileLocation">Location of the config file.</param>
        /// <returns>A ConnectionStringDecryptionDriver.</returns>
        public IConnectionStringDecryptionDriver CreateXmlFileDecrypter(LocalFilePath configFileLocation)
        {
            // Unit test will need to modify location of config for each test.
            return new XmlConfigFileConnectionStringDecryptionDriver(configFileLocation);
        }
    }
}