﻿namespace LendersOffice.Drivers.ConnectionStringDecryption
{
    using System;
    using LqbGrammar;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Utility class for interacting with the ConnectionStringDecryption driver.
    /// </summary>
    public static class ConnectionStringDecryptionDriverHelper
    {
        /// <summary>
        /// Decrypts the given connection string.
        /// </summary>
        /// <param name="encryptedConnectionString">The encrypted connection string.</param>
        /// <param name="decryptedConnectionString">The resulting decrypted connection string.</param>
        /// <returns>True if decryption was successful.  Otherwise false.</returns>
        public static bool TryDecryptConnectionString(DBConnectionString encryptedConnectionString, out DBConnectionString decryptedConnectionString)
        {
            try
            {
                var driver = GetDriver();
                return driver.TryDecryptConnectionString(encryptedConnectionString, out decryptedConnectionString);
            }
            catch (DeveloperException exc) when (exc.GetBaseException() is ArgumentException)
            {
                // The driver creation will throw if it cannot find the key file.
                decryptedConnectionString = encryptedConnectionString;
                return false;
            }
        }

        /// <summary>
        /// Attempt to get the local file path for the secure configuration
        /// xml file based on the configuration flag of the current app.
        /// </summary>
        /// <remarks>This will return an invalid file path if the file path 
        /// is not specified or it does conform to a file path string. This
        /// will not check if the file exists or is accessible.</remarks>
        /// <returns>The file path of the secure configuration file.</returns>
        private static LocalFilePath GetSecureFilePathFromConfig()
        {
            const string SecureConfigFilePathKey = "SecureConfigFilePath";
            var secureConfigFilePath = System.Configuration.ConfigurationManager.AppSettings[SecureConfigFilePathKey];

            if (string.IsNullOrEmpty(secureConfigFilePath))
            {
                // No file is specified.
                return LocalFilePath.Invalid;
            }

            var secureConnectionKeyLocalFilePath = LocalFilePath.Create(secureConfigFilePath);
            if (!secureConnectionKeyLocalFilePath.HasValue)
            {
                // File was specified but it is not a valid file path.
                return LocalFilePath.Invalid;
            }

            return secureConnectionKeyLocalFilePath.Value;
        }

        /// <summary>
        /// Retrieve the driver.
        /// </summary>
        /// <returns>The driver.</returns>
        private static IConnectionStringDecryptionDriver GetDriver()
        {
            var factory = GenericLocator<IConnectionStringDecryptionDriverFactory>.Factory;

            var securePath = GetSecureFilePathFromConfig();
            return factory.CreateXmlFileDecrypter(securePath);
        }
    }
}
