﻿namespace LendersOffice.Drivers.OpenXmlDocument
{
    using LqbGrammar;
    using LqbGrammar.Adapters;
    using LqbGrammar.Drivers;

    /// <summary>
    /// Factory for creating implementations of open XML document drivers.
    /// </summary>
    public class OpenXmlDocumentDriverFactory : IOpenXmlDocumentDriverFactory
    {
        /// <summary>
        /// Create and return an implementation of the open XML document driver.
        /// </summary>
        /// <returns>
        /// Implementation of the open XML document driver.
        /// </returns>
        public IOpenXmlDocumentDriver Create()
        {
            var adapterFactory = GenericLocator<IOpenXmlDocumentAdapterFactory>.Factory;
            var adapter = adapterFactory.Create();
            return new OpenXmlDocumentDriver(adapter);
        }
    }
}
