﻿namespace LendersOffice.Drivers.OpenXmlDocument
{
    using System.Collections.Generic;
    using LqbGrammar;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;

    /// <summary>
    /// Provides a helper for the open XML document driver.
    /// </summary>
    public static class OpenXmlDocumentDriverHelper
    {
        /// <summary>
        /// Sets values for the list of fields in the open XML document.
        /// </summary>
        /// <param name="localFilePath">
        /// The path to the document.
        /// </param>
        /// <param name="fieldNameValueDict">
        /// The field name - value dictionary.
        /// </param>
        public static void SetTextFieldsOnDocument(LocalFilePath localFilePath, IDictionary<string, string> fieldNameValueDict)
        {
            var driver = GetDriver();
            driver.SetTextFieldsOnDocument(localFilePath, fieldNameValueDict);
        }

        /// <summary>
        /// Gets the open XML document driver.
        /// </summary>
        /// <returns>
        /// The open XML document driver.
        /// </returns>
        private static IOpenXmlDocumentDriver GetDriver()
        {
            var factory = GenericLocator<IOpenXmlDocumentDriverFactory>.Factory;
            return factory.Create();
        }
    }
}
