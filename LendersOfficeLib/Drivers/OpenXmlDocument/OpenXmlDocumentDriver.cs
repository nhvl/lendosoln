﻿namespace LendersOffice.Drivers.OpenXmlDocument
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using LqbGrammar.Adapters;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;

    /// <summary>
    /// Defines the interface to open XML documents.
    /// </summary>
    internal class OpenXmlDocumentDriver : IOpenXmlDocumentDriver
    {
        /// <summary>
        /// Holds a references to the adapter.
        /// </summary>
        private readonly IOpenXmlDocumentAdapter adapter;

        /// <summary>
        /// Initializes a new instance of the <see cref="OpenXmlDocumentDriver"/> class.
        /// </summary>
        /// <param name="adapter">
        /// The wrapped adapter.
        /// </param>
        public OpenXmlDocumentDriver(IOpenXmlDocumentAdapter adapter)
        {
            this.adapter = adapter;
        }

        /// <summary>
        /// Sets values for the list of fields in the open XML document.
        /// </summary>
        /// <param name="localFilePath">
        /// The path to the document.
        /// </param>
        /// <param name="fieldNameValueDict">
        /// The field name - value dictionary.
        /// </param>
        public void SetTextFieldsOnDocument(LocalFilePath localFilePath, IDictionary<string, string> fieldNameValueDict)
        {
            this.adapter.SetTextFieldsOnDocument(localFilePath, fieldNameValueDict);
        }
    }
}
