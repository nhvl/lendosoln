﻿namespace LendersOffice.Drivers.Gateways
{
    using System;
    using System.IO;
    using System.Messaging;
    using System.Text;
    using System.Xml.Linq;
    using Common;
    using CommonProjectLib.Common;
    using Constants;
    using LqbGrammar;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Utility class to centralize usage of the message queue libraries.
    /// </summary>
    public static class MessageQueueHelper
    {
        /// <summary>
        /// Send the xml document to the message queue.
        /// </summary>
        /// <param name="queue">The name of the message queue.</param>
        /// <param name="label">Optional label to be added to the message.</param>
        /// <param name="doc">The xml document to send to the message queue.</param>
        public static void SendXML(string queue, string label, XDocument doc)
        {
            if (string.IsNullOrEmpty(queue) || doc == null)
            {
                return;
            }

            var data = LendersOffice.Drivers.MessageQueue.MessageQueueHelper.ValidateQueueInfo(queue, label);

            LqbXmlElement? goodElem = LqbXmlElement.Create(doc.Root);
            if (goodElem == null)
            {
                throw new DeveloperException(ErrorMessage.SystemError);
            }

            LendersOffice.Drivers.MessageQueue.MessageQueueHelper.SendXML(data.Item1, data.Item2, goodElem.Value);
        }

        /// <summary>
        /// Serialize an object to JSON format and send it to the message queue.
        /// </summary>
        /// <param name="queue">The name of the message queue.</param>
        /// <param name="label">Optional label to be added to the message.</param>
        /// <param name="obj">The object to be sent to the message queue.</param>
        public static void SendJSON(string queue, string label, object obj)
        {
            if (string.IsNullOrEmpty(queue) || obj == null)
            {
                return;
            }

            var data = LendersOffice.Drivers.MessageQueue.MessageQueueHelper.ValidateQueueInfo(queue, label);

            LendersOffice.Drivers.MessageQueue.MessageQueueHelper.SendJSON(data.Item1, data.Item2, obj);
        }

        /// <summary>
        /// Create and initialize a message queue object that can be used for retrieving XML documents.
        /// </summary>
        /// <param name="queue">The path to the message queue.</param>
        /// <param name="setFilterDefaults">Counter-intuitively, calling SetDefaults on the message filter is different than using the defaults when just using the MessageQueue constructor.</param>
        /// <returns>A message queue object.</returns>
        public static MessageQueue PrepareForXML(string queue, bool setFilterDefaults)
        {
            var data = LendersOffice.Drivers.MessageQueue.MessageQueueHelper.ValidateQueueInfo(queue, null);

            var theQ = LendersOffice.Drivers.MessageQueue.MessageQueueHelper.PrepareForXML(data.Item1, setFilterDefaults);
            return theQ.Value;
        }

        /// <summary>
        /// Create and initialize a message queue object that can be used for retrieving objects serialized as JSON.
        /// </summary>
        /// <param name="queue">The path to the message queue.</param>
        /// <returns>A message queue object.</returns>
        public static MessageQueue PrepareForJSON(string queue)
        {
            var data = LendersOffice.Drivers.MessageQueue.MessageQueueHelper.ValidateQueueInfo(queue, null);

            var theQ = LendersOffice.Drivers.MessageQueue.MessageQueueHelper.PrepareForJSON(data.Item1);
            return theQ.Value;
        }

        /// <summary>
        /// Retrieve an XDocument from the message queue.
        /// </summary>
        /// <param name="queue">The message queue object.</param>
        /// <param name="timeoutInSeconds">The time to wait for a message.  If null then 10 milliseconds is the wait time.</param>
        /// <param name="arrivalTime">The time that the message was delivered to the queue.</param>
        /// <returns>An XDocument from the queue.</returns>
        public static XDocument ReceiveXML(MessageQueue queue, int? timeoutInSeconds, out DateTime arrivalTime)
        {
            if (timeoutInSeconds == null)
            {
                LqbXmlElement? elem = LendersOffice.Drivers.MessageQueue.MessageQueueHelper.ReceiveXML(new LqbMessageQueue(queue), null, out arrivalTime);
                return elem?.Contained.Document;
            }
            else
            {
                TimeoutInSeconds? to = TimeoutInSeconds.Create(timeoutInSeconds.Value);
                if (to == null)
                {
                    throw new DeveloperException(ErrorMessage.SystemError);
                }

                LqbXmlElement? elem = LendersOffice.Drivers.MessageQueue.MessageQueueHelper.ReceiveXML(new LqbMessageQueue(queue), to, out arrivalTime);
                return elem?.Contained.Document;
            }
        }

        /// <summary>
        /// Retrieve an object from the message queue that has been serialized as JSON.
        /// </summary>
        /// <typeparam name="T">The expected type of the object.</typeparam>
        /// <param name="queue">The message queue object.</param>
        /// <param name="timeoutInSeconds">The time to wait for a message.  If null then 10 milliseconds is the wait time.</param>
        /// <param name="arrivalTime">The time that the message was delivered to the queue.</param>
        /// <returns>An instance of the expected type.</returns>
        public static T ReceiveJSON<T>(MessageQueue queue, int? timeoutInSeconds, out DateTime arrivalTime) where T : class, new()
        {
            if (timeoutInSeconds == null)
            {
                return LendersOffice.Drivers.MessageQueue.MessageQueueHelper.ReceiveJSON<T>(new LqbMessageQueue(queue), null, out arrivalTime);
            }
            else
            {
                TimeoutInSeconds? to = TimeoutInSeconds.Create(timeoutInSeconds.Value);
                if (to == null)
                {
                    throw new DeveloperException(ErrorMessage.SystemError);
                }

                return LendersOffice.Drivers.MessageQueue.MessageQueueHelper.ReceiveJSON<T>(new LqbMessageQueue(queue), to, out arrivalTime);
            }
        }
    }
}
