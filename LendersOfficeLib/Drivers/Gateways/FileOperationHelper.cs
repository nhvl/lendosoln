﻿namespace LendersOffice.Drivers.Gateways
{
    using System;
    using System.IO;
    using LendersOffice.Constants;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Utility for carrying out file operations.
    /// </summary>
    public static class FileOperationHelper
    {
        /// <summary>
        /// Copy the source file to the target file.
        /// </summary>
        /// <param name="source">The source file.</param>
        /// <param name="target">The target file.</param>
        /// <param name="allowOverwrite">True if overwriting an existing file is allowed, false otherwise.</param>
        public static void Copy(string source, string target, bool allowOverwrite)
        {
            if (ConstStage.UseNewFileSystem)
            {
                var localSource = ConvertFilePath(source);
                var localTarget = ConvertFilePath(target);
                LendersOffice.Drivers.FileSystem.FileOperationHelper.Copy(localSource, localTarget, allowOverwrite);
            }
            else
            {
                File.Copy(source, target, allowOverwrite);
            }
        }

        /// <summary>
        /// Move the source file to the target location if the target doesn't already exist.
        /// If the target exists an exception will be thrown, the Copy and Delete methods 
        /// must be used instead.
        /// </summary>
        /// <param name="source">The file that will be moved.</param>
        /// <param name="target">The location to which the file will be moved.</param>
        public static void Move(string source, string target)
        {
            if (ConstStage.UseNewFileSystem)
            {
                var localSource = ConvertFilePath(source);
                var localTarget = ConvertFilePath(target);
                LendersOffice.Drivers.FileSystem.FileOperationHelper.Move(localSource, localTarget);
            }
            else
            {
                File.Move(source, target);
            }
        }

        /// <summary>
        /// Deletes the specified file.
        /// </summary>
        /// <param name="path">The file to delete.</param>
        public static void Delete(string path)
        {
            if (ConstStage.UseNewFileSystem)
            {
                var localPath = ConvertFilePath(path);
                LendersOffice.Drivers.FileSystem.FileOperationHelper.Delete(localPath);
            }
            else
            {
                File.Delete(path);
            }
        }

        /// <summary>
        /// Detects whether or not a specified file exists.
        /// </summary>
        /// <param name="path">The file to check.</param>
        /// <returns>True if the file exists, false otherwise.</returns>
        public static bool Exists(string path)
        {
            if (ConstStage.UseNewFileSystem)
            {
                var localPath = ConvertFilePath(path);
                return LendersOffice.Drivers.FileSystem.FileOperationHelper.Exists(localPath);
            }
            else
            {
                return File.Exists(path);
            }
        }

        /// <summary>
        /// Ensure the specified file is not read-only.
        /// </summary>
        /// <param name="path">The specified file.</param>
        public static void TurnOffReadOnly(string path)
        {
            if (ConstStage.UseNewFileSystem)
            {
                var localPath = ConvertFilePath(path);
                LendersOffice.Drivers.FileSystem.FileOperationHelper.TurnOffReadOnly(localPath);
            }
            else
            {
                if (File.Exists(path))
                {
                    var attributes = File.GetAttributes(path);
                    if ((attributes & FileAttributes.ReadOnly) > 0)
                    {
                        attributes ^= FileAttributes.ReadOnly; // Turn off read-only bit
                        File.SetAttributes(path, attributes);
                    }
                }
            }
        }

        /// <summary>
        /// Retrieve the timestamp when the file was last modified.
        /// </summary>
        /// <param name="path">The specified file.</param>
        /// <returns>The timestamp when the file was last modified.</returns>
        public static DateTime GetLastWriteTime(string path)
        {
            if (ConstStage.UseNewFileSystem)
            {
                var localPath = ConvertFilePath(path);
                var eventVal = LendersOffice.Drivers.FileSystem.FileOperationHelper.GetLastWriteTime(localPath);

                string eventString = eventVal.ToString("O");
                var utc = DateTime.Parse(eventString, null, System.Globalization.DateTimeStyles.RoundtripKind);
                return utc.ToLocalTime();
            }
            else
            {
                return File.GetLastWriteTime(path);
            }
        }

        /// <summary>
        /// Set the last write time propertry for the specified file.
        /// </summary>
        /// <param name="path">The specified file.</param>
        /// <param name="timestamp">The value that is to be set as the last write time.</param>
        public static void SetLastWriteTime(string path, DateTime timestamp)
        {
            if (ConstStage.UseNewFileSystem)
            {
                var localPath = ConvertFilePath(path);
                var eventVal = ConvertEventDate(timestamp);
                LendersOffice.Drivers.FileSystem.FileOperationHelper.SetLastWriteTime(localPath, eventVal);
            }
            else
            {
                File.SetLastWriteTime(path, timestamp);
            }
        }

        /// <summary>
        /// Convert a file path that is represented as a string into a validated semantic file path.
        /// </summary>
        /// <param name="path">The file path represented as a string.</param>
        /// <returns>A validated semantic file path.</returns>
        private static LocalFilePath ConvertFilePath(string path)
        {
            var localPath = LocalFilePath.Create(path);
            if (localPath == null)
            {
                throw new DeveloperException(ErrorMessage.SystemError);
            }

            return localPath.Value;
        }

        /// <summary>
        /// Convert a DateTime into a validated semantic event.
        /// </summary>
        /// <param name="timestamp">The DateTime to be converted.</param>
        /// <returns>A validated semantic event.</returns>
        private static LqbEventDate ConvertEventDate(DateTime timestamp)
        {
            var working = timestamp;
            if (working.Kind != DateTimeKind.Utc)
            {
                working = working.ToUniversalTime();
            }

            var eventval = LqbEventDate.Create(working);
            if (eventval == null)
            {
                throw new DeveloperException(ErrorMessage.SystemError);
            }

            return eventval.Value;
        }
    }
}
