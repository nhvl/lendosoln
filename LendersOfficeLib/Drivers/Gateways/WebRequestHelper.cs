﻿namespace LendersOffice.Drivers.Gateways
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Net;
    using System.Security.Cryptography.X509Certificates;
    using System.Text;
    using System.Xml;
    using LendersOffice.Constants;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Wrapper to centralize the usage of the WebRequest class.
    /// The current LQB code is all over the place with respect
    /// to how data is written to the request stream and read 
    /// from the response stream.  Rather than trying to preserve
    /// all of that and make a very complex, ugly class here,
    /// I've decided to write clean code that works and abandon
    /// existing code does things differently.
    /// </summary>
    public static class WebRequestHelper
    {
        /// <summary>
        /// Carry out the communication with the target url.
        /// </summary>
        /// <param name="url">The target url.</param>
        /// <param name="options">The data that affects the write request/read response process.</param>
        public static void ExecuteCommunication(string url, Options options)
        {
            ExecuteCommunication(new Uri(url), options);
        }

        /// <summary>
        /// Carry out the communication with the target url.
        /// </summary>
        /// <param name="uri">The target uri.</param>
        /// <param name="options">The data that affects the write request/read response process.</param>
        public static void ExecuteCommunication(Uri uri, Options options)
        {
            if (ConstStage.UseNewWebRequest)
            {
                var uriLqb = LqbAbsoluteUri.Create(uri.OriginalString);
                if (uriLqb == null)
                {
                    throw new DeveloperException(ErrorMessage.SystemError);
                }

                var driverOptions = options.Convert();
                LendersOffice.Drivers.HttpRequest.WebRequestHelper.ExecuteCommunication(uriLqb.Value, driverOptions);
            }
            else
            {
                var webRequest = (HttpWebRequest)WebRequest.Create(uri);
                options.InitializeRequest(webRequest);

                options.InjectPostData(webRequest);

                using (var webResponse = (HttpWebResponse)webRequest.GetResponse())
                {
                    options.ReadResponse(webResponse);
                }
            }
        }

        /// <summary>
        /// All the possible data for a web request are gathered here in order
        /// to simplify a very complex interface.
        /// </summary>
        public sealed class Options
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="Options"/> class.
            /// </summary>
            public Options()
            {
                // post data
                this.BinaryPostData = null;
                this.StringPostData = null;
                this.XmlPostData = null;
                this.XmlWriterSettings = null;

                // WebRequest - I'm assigning the default values as documented at MSDN
                this.HttpMethod = "GET";
                this.ContentType = null;
                this.KeepAlive = true;
                this.Timeout = 100000; // 100 seconds
                this.UserAgent = null;
                this.AllowAutoRedirect = true;
                this.MaximumAutomaticRedirections = 50;
                this.ServicePointConnectionLimit = null;
                this.MaximumResponseHeadersLength = null;
                this.SetDecompressionMethods = false;
                this.DefaultAcceptLanguage = null;
                this.Credentials = null;
                this.DefaultAcceptHeader = null;
                this.UseHTTPProtocol10 = false;
                this.Referer = null;
                this.RequestHeaders = null;
                this.RequestCookieContainer = null;

                // WebResponse
                this.IgnoreResponse = false;
                this.ResponseFileName = null; // set this to force writing of the response to a file
                this.XmlResponseDelegate = null; // set this to force reading the response as xml, processed by the delegate
                this.XmlReaderSettings = null; // set this to control how the response is read into xml
                this.ResponseBody = null; // where the response body is written if not to a file or handed off to an xml handling delegate
                this.ResponseStatusCode = HttpStatusCode.InternalServerError; // 500
                this.ResponseStatusDescription = "NO STATUS CODE AVAILABLE";
                this.ResponseCookieContainer = new CookieContainer(); // Setting to null will signal that response cookies aren't desired.
                this.ResponseHeaders = new Dictionary<string, string>(); // Setting to null will signal that response headers aren't desired.
            }

            /// <summary>
            /// Gets or sets binary data to be posted to the target url.
            /// </summary>
            /// <value>Data to be posted to the target url.</value>
            public byte[] BinaryPostData { get; set; }

            /// <summary>
            /// Gets or sets string data to be posted to the target url.
            /// </summary>
            /// <value>Data to be posted to the target url.</value>
            public string StringPostData { get; set; }

            /// <summary>
            /// Gets or sets xml data to be posted to the target url.
            /// </summary>
            /// <value>Xml data to be posted to the target url.</value>
            public XmlDocument XmlPostData { get; set; }

            /// <summary>
            /// Gets or sets settings that influence how xml data is written to the network stream.
            /// </summary>
            /// <value>Settings that influence how xml data is written to the network stream.</value>
            public XmlWriterSettings XmlWriterSettings { get; set; }

            /// <summary>
            /// Gets or sets the path to which the response should be written,
            /// instead of adding the full response to the ResponseBody property.
            /// </summary>
            /// <value>The path to which the response should be written.</value>
            public string ResponseFileName { get; set; }

            /// <summary>
            /// Gets or sets a delegate that will process xml from the response,
            /// instead of adding the full response to the ResponseBody property.
            /// </summary>
            /// <value>A delegate that will process xml from the response.</value>
            public Action<XmlReader> XmlResponseDelegate { get; set; }

            /// <summary>
            /// Gets or sets settings that influence how xml data is read from the response stream.
            /// </summary>
            /// <value>Settings that influence how xml data is read from the response stream.</value>
            public XmlReaderSettings XmlReaderSettings { get; set; }

            /// <summary>
            /// Gets or sets the method to assign when calling the target url, GET or POST.
            /// </summary>
            /// <value>GET or POST.</value>
            public string HttpMethod { get; set; }

            /// <summary>
            /// Gets or sets the content type.
            /// </summary>
            /// <value>The content type.</value>
            public string ContentType { get; set; }

            /// <summary>
            /// Gets or sets a value indicating whether or not the KeepAlive header should be set.
            /// </summary>
            /// <value>Keep alive flag.</value>
            public bool KeepAlive { get; set; }

            /// <summary>
            /// Gets or sets the timeout in milliseconds.
            /// </summary>
            /// <value>The timeout in milliseconds.</value>
            public int Timeout { get; set; }

            /// <summary>
            /// Gets or sets the user agent string.
            /// </summary>
            /// <value>The user agent string.</value>
            public string UserAgent { get; set; }

            /// <summary>
            /// Gets or sets a value indicating whether or not auto-redirect is allowed.
            /// </summary>
            /// <value>Auto-redirect flag.</value>
            public bool AllowAutoRedirect { get; set; }

            /// <summary>
            /// Gets or sets the maximum number of times re-directs are followed prior to erroring out.
            /// </summary>
            /// <value>The maximum number of times re-directs are followed prior to erroring out.</value>
            public int MaximumAutomaticRedirections { get; set; }

            /// <summary>
            /// Gets or sets the maximum number of connections allowed on the ServicePoint object.  When null the default value is used.
            /// </summary>
            /// <value>The maximum number of connections allowed on the ServicePoint object.</value>
            public int? ServicePointConnectionLimit { get; set; }

            /// <summary>
            /// Gets or sets the maximum length of the data for the response headers.  Null means the default
            /// behavior is used (gathered from the application's configuration file).
            /// </summary>
            /// <value>The maximum length of the data for the response headers.</value>
            public int? MaximumResponseHeadersLength { get; set; }

            /// <summary>
            /// Gets or sets a value indicating whether or not to indicate the response can be compressed.
            /// </summary>
            /// <value>Response compression flag.</value>
            public bool SetDecompressionMethods { get; set; }

            /// <summary>
            /// Gets or sets the default language that is accepted.
            /// </summary>
            /// <value>The default language that is accepted.</value>
            public string DefaultAcceptLanguage { get; set; }

            /// <summary>
            /// Gets or sets credentials to be sent with the request.
            /// </summary>
            /// <value>Credentials to be sent with the request.</value>
            public ICredentials Credentials { get; set; }

            /// <summary>
            /// Gets or sets the value of the Accept header.
            /// </summary>
            /// <value>The value of the Accept header.</value>
            public string DefaultAcceptHeader { get; set; }

            /// <summary>
            /// Gets or sets a value indicating whether to override the default protocol (1.1) to use 1.0 instead.
            /// </summary>
            /// <value>Protocol flag.</value>
            public bool UseHTTPProtocol10 { get; set; }

            /// <summary>
            /// Gets or sets the Referer header.
            /// </summary>
            /// <value>The Referer header.</value>
            public string Referer { get; set; }

            /// <summary>
            /// Gets or sets headers that are to be written into the request.
            /// </summary>
            /// <value>Headers that are to be written into the request.</value>
            public Dictionary<string, string> RequestHeaders { get; set; }

            /// <summary>
            /// Gets or sets the container of cookies to send.
            /// </summary>
            /// <value>The container of cookies.</value>
            public CookieContainer RequestCookieContainer { get; set; }

            /// <summary>
            /// Gets or sets the certificate to use.
            /// </summary>
            /// <value>The certificate to use.</value>
            public X509Certificate X509Certificate { get; set; }

            /// <summary>
            /// Gets or sets a value indicating whether the response should
            /// be parsed or ignored.  The reason to ignore the response is
            /// because the calling code isn't going to use it.
            /// </summary>
            /// <value>Flag instructing code to not bother parsing the response.</value>
            public bool IgnoreResponse { get; set; }

            /// <summary>
            /// Gets or sets the body of the response.
            /// </summary>
            /// <value>The body of the response.</value>
            public string ResponseBody { get; set; }

            /// <summary>
            /// Gets or sets the container of cookies from the response.  It
            /// is an option because if it is null then the response cookies
            /// will not be gathered, but if is not null then the response
            /// cookies will be added to the container.
            /// </summary>
            /// <value>The container to receive response cookies.</value>
            public CookieContainer ResponseCookieContainer { get; set; }

            /// <summary>
            /// Gets or sets the response headers.  It is an options because
            /// if it is null then the response headers will not be gathered,
            /// but if it is not null then the response headers will be 
            /// added to the dictionary.
            /// </summary>
            /// <value>The response headers.</value>
            public Dictionary<string, string> ResponseHeaders { get; set; }

            /// <summary>
            /// Gets or sets the response status code, which will always be 
            /// written here.
            /// </summary>
            /// <value>The response status code.</value>
            public HttpStatusCode ResponseStatusCode { get; set; }

            /// <summary>
            /// Gets or sets the response status description, which will
            /// always be written here.
            /// </summary>
            /// <value>The response status description.</value>
            public string ResponseStatusDescription { get; set; }

            /// <summary>
            /// Configure the web request with the options.
            /// </summary>
            /// <param name="request">The web request.</param>
            public void InitializeRequest(HttpWebRequest request)
            {
                request.KeepAlive = this.KeepAlive;
                request.Method = this.HttpMethod;
                request.ContentType = this.ContentType;
                request.AllowAutoRedirect = this.AllowAutoRedirect;
                request.MaximumAutomaticRedirections = this.MaximumAutomaticRedirections;
                request.UserAgent = this.UserAgent;
                request.Timeout = this.Timeout;
                request.Credentials = this.Credentials;
                request.Accept = this.DefaultAcceptHeader;
                request.Referer = this.Referer;
                request.CookieContainer = this.RequestCookieContainer;

                if (this.ServicePointConnectionLimit != null)
                {
                    request.ServicePoint.ConnectionLimit = this.ServicePointConnectionLimit.Value;
                }

                if (this.MaximumResponseHeadersLength != null)
                {
                    request.MaximumResponseHeadersLength = this.MaximumResponseHeadersLength.Value;
                }

                if (this.SetDecompressionMethods)
                {
                    request.AutomaticDecompression = DecompressionMethods.Deflate | DecompressionMethods.GZip;
                }
                else
                {
                    request.AutomaticDecompression = DecompressionMethods.None;
                }

                if (!string.IsNullOrEmpty(this.DefaultAcceptLanguage))
                {
                    request.Headers.Add(HttpRequestHeader.AcceptLanguage, this.DefaultAcceptLanguage);
                }

                if (this.UseHTTPProtocol10)
                {
                    request.ProtocolVersion = HttpVersion.Version10;
                }

                if (this.RequestHeaders != null)
                {
                    foreach (string key in this.RequestHeaders.Keys)
                    {
                        request.Headers.Add(key, this.RequestHeaders[key]);
                    }
                }

                if (this.X509Certificate != null)
                {
                    request.ClientCertificates.Clear();
                    request.ClientCertificates.Add(this.X509Certificate);
                }
            }

            /// <summary>
            /// When data is posted to the target url, this method handles
            /// injecting the data into the request stream.  Note that 
            /// while the current LQB code often uses ASCII encoding, this 
            /// method uses UTF8 encoding instead.  The idea is that any 
            /// data that can be streamed as ASCII will generate an identical
            /// stream when UTF8 encoding is used.  On the other hand, non
            /// ASCII characters can be handled correctly with the UTF8
            /// encoding while the ASCII encoder has undefined behavior.
            /// </summary>
            /// <param name="webRequest">The http web request.</param>
            public void InjectPostData(HttpWebRequest webRequest)
            {
                if (this.HttpMethod != "POST")
                {
                    return;
                }

                using (Stream stream = webRequest.GetRequestStream())
                {
                    if (this.BinaryPostData != null)
                    {
                        stream.Write(this.BinaryPostData, 0, this.BinaryPostData.Length);
                    }
                    else if (this.StringPostData != null)
                    {
                        using (StreamWriter writer = new StreamWriter(stream, Encoding.UTF8))
                        {
                            writer.Write(this.StringPostData);
                        }
                    }
                    else if (this.XmlPostData != null)
                    {
                        var settings = this.XmlWriterSettings;
                        if (settings == null)
                        {
                            settings = new XmlWriterSettings();
                        }

                        settings.Encoding = new UTF8Encoding(false);

                        using (XmlWriter writer = XmlWriter.Create(stream, settings))
                        {
                            this.XmlPostData.Save(writer);
                        }
                    }
                }
            }

            /// <summary>
            /// Read the response data into the options data structure.
            /// Note that while current LQB code often uses ASCII
            /// encoding, only UTF8 encoding is used here.  The idea 
            /// is that when a response is successfully parsed using
            /// an ASCII encoder, a UTF8 encoder will work as well.
            /// On the other hand, should there be true UTF8 encoding
            /// in the response then only a UTF8 encoder will work.
            /// There are no other encoders used in the LQB code so
            /// always using UTF8 encoding should be sufficient.
            /// </summary>
            /// <param name="response">The response.</param>
            public void ReadResponse(HttpWebResponse response)
            {
                if (this.IgnoreResponse)
                {
                    return;
                }

                this.ResponseStatusCode = response.StatusCode;
                this.ResponseStatusDescription = response.StatusDescription;

                if (this.ResponseHeaders != null)
                {
                    foreach (var key in response.Headers.AllKeys)
                    {
                        this.ResponseHeaders.Add(key, response.Headers.Get(key));
                    }
                }

                if (this.ResponseCookieContainer != null)
                {
                    this.ResponseCookieContainer.Add(response.Cookies);
                }

                using (Stream stream = response.GetResponseStream())
                {
                    if (!string.IsNullOrEmpty(this.ResponseFileName))
                    {
                        using (var fileStream = File.Open(this.ResponseFileName, FileMode.Create))
                        {
                            stream.CopyTo(fileStream);
                        }
                    }
                    else if (this.XmlResponseDelegate != null)
                    {
                        // XmlReaderSettings doesn't have an Encoding property,
                        // the xml stream itself is used to detect the encoding.
                        using (var reader = XmlReader.Create(stream, this.XmlReaderSettings))
                        {
                            this.XmlResponseDelegate(reader);
                        }
                    }
                    else
                    {
                        using (var reader = new StreamReader(stream, new UTF8Encoding(false)))
                        {
                            this.ResponseBody = reader.ReadToEnd();
                        }
                    }
                }
            }

            /// <summary>
            /// Convert this data into the equivalent structure in the FOOL architecture.
            /// </summary>
            /// <returns>The options structure that is used within the FOOL architecture.</returns>
            public LqbGrammar.Drivers.HttpRequest.HttpRequestOptions Convert()
            {
                // TODO: finish with the properties that have been missed - some were added in PRs that haven't yet been merged.
                var fool = new LqbGrammar.Drivers.HttpRequest.HttpRequestOptions();
                fool.AllowAutoRedirect = this.AllowAutoRedirect;
                if (this.BinaryPostData != null)
                {
                    fool.PostData = new ByteContent(this.BinaryPostData);
                }
                else if (this.StringPostData != null)
                {
                    fool.PostData = new StringContent(this.StringPostData);
                }
                else if (this.XmlPostData != null)
                {
                    fool.PostData = new XmlContent(LqbXmlElement.Create(this.XmlPostData.ToString()).Value, this.XmlWriterSettings);
                }

                fool.Credentials = this.Credentials;
                fool.IgnoreResponse = this.IgnoreResponse;
                fool.KeepAlive = this.KeepAlive;
                fool.MaximumAutomaticRedirections = this.MaximumAutomaticRedirections;
                if (this.MaximumResponseHeadersLength.HasValue)
                {
                    fool.MaximumResponseHeadersLength = this.MaximumResponseHeadersLength.Value;
                }

                switch (this.HttpMethod)
                {
                    case "POST":
                        fool.Method = LqbGrammar.DataTypes.HttpMethod.Post;
                        break;
                    case "GET":
                        fool.Method = LqbGrammar.DataTypes.HttpMethod.Get;
                        break;
                    default:
                        throw new DeveloperException(ErrorMessage.SystemError);
                }

                if (this.Timeout > 0)
                {
                    fool.Timeout = TimeoutInSeconds.Create(this.Timeout / 1000).Value;
                }

                fool.UseHTTPProtocol10 = this.UseHTTPProtocol10;
                
                if (!string.IsNullOrEmpty(this.UserAgent))
                {
                    if (this.UserAgent == HttpUserAgent.IE6.ToString())
                    {
                        fool.UserAgent = HttpUserAgent.IE6;
                    }
                    else if (this.UserAgent == HttpUserAgent.IE8.ToString())
                    {
                        fool.UserAgent = HttpUserAgent.IE8;
                    }
                    else if (this.UserAgent == HttpUserAgent.IE9.ToString())
                    {
                        fool.UserAgent = HttpUserAgent.IE9;
                    }
                    else if (this.UserAgent == HttpUserAgent.LqbWebBot.ToString())
                    {
                        fool.UserAgent = HttpUserAgent.LqbWebBot;
                    }
                    else if (this.UserAgent == HttpUserAgent.Mozilla5.ToString())
                    {
                        fool.UserAgent = HttpUserAgent.Mozilla5;
                    }
                }

                if (!string.IsNullOrEmpty(this.ContentType))
                {
                    if (this.ContentType == MimeType.Text.Xml.ToString())
                    {
                        fool.MimeType = MimeType.Text.Xml;
                    }
                    else if (this.ContentType == MimeType.Application.Xml.ToString())
                    {
                        fool.MimeType = MimeType.Application.Xml;
                    }
                    else if (this.ContentType == MimeType.Application.Xhtml.ToString())
                    {
                        fool.MimeType = MimeType.Application.Xhtml;
                    }
                    else if (this.ContentType == MimeType.Application.UrlEncoded.ToString())
                    {
                        fool.MimeType = MimeType.Application.UrlEncoded;
                    }
                    else if (this.ContentType.StartsWith(MimeType.MultiPart.CreateFormData(string.Empty).MediaType.ToString()))
                    {
                        var match = System.Text.RegularExpressions.Regex.Match(this.ContentType, $@"multipart/(?<subtype>[!#$%&'*+-.^_`|~0-9A-Za-z]+);.*boundary=""?(?<boundary>{RegularExpressionString.MimeMultipartBoundary.ToString()})""");

                        if (match.Success)
                        {
                            fool.MimeType = new MimeType.MultiPart.MultiPartMimeType(MimeType.MimeTypeToken.Create(match.Groups["subtype"].Value).Value, match.Groups["boundary"].Value);
                        }
                        else
                        {
                            fool.MimeType = MimeType.MultiPart.CreateFormData(this.ContentType.Substring("multipart/form-data; boundary=".Length));
                        }
                    }
                    else if (this.ContentType == MimeType.Text.Plain.ToString())
                    {
                        fool.MimeType = MimeType.Text.Plain;
                    }
                    else if (this.ContentType == MimeType.Text.Csv.ToString())
                    {
                        fool.MimeType = MimeType.Text.Csv;
                    }
                    else if (this.ContentType == MimeType.Text.CommaSeparatedValues.ToString())
                    {
                        fool.MimeType = MimeType.Text.CommaSeparatedValues;
                    }
                    else if (this.ContentType == MimeType.Text.Html.ToString())
                    {
                        fool.MimeType = MimeType.Text.Html;
                    }
                    else if (this.ContentType == MimeType.Text.Javascript.ToString())
                    {
                        fool.MimeType = MimeType.Text.Javascript;
                    }
                    else if (this.ContentType == MimeType.Application.Csv.ToString())
                    {
                        fool.MimeType = MimeType.Application.Csv;
                    }
                    else if (this.ContentType == MimeType.Application.Doc.ToString())
                    {
                        fool.MimeType = MimeType.Application.Doc;
                    }
                    else if (this.ContentType == MimeType.Application.Download.ToString())
                    {
                        fool.MimeType = MimeType.Application.Download;
                    }
                    else if (this.ContentType == MimeType.Application.Excel.ToString())
                    {
                        fool.MimeType = MimeType.Application.Excel;
                    }
                    else if (this.ContentType == MimeType.Application.Json.ToString())
                    {
                        fool.MimeType = MimeType.Application.Json;
                    }
                    else if (this.ContentType == MimeType.Application.OctetStream.ToString())
                    {
                        fool.MimeType = MimeType.Application.OctetStream;
                    }
                    else if (this.ContentType == MimeType.Application.Pdf.ToString())
                    {
                        fool.MimeType = MimeType.Application.Pdf;
                    }
                    else if (this.ContentType == MimeType.Application.SpreadsheetXml.ToString())
                    {
                        fool.MimeType = MimeType.Application.SpreadsheetXml;
                    }
                    else if (this.ContentType == MimeType.Application.Text.ToString())
                    {
                        fool.MimeType = MimeType.Application.Text;
                    }
                    else if (this.ContentType == MimeType.Application.Zip.ToString())
                    {
                        fool.MimeType = MimeType.Application.Zip;
                    }
                    else if (this.ContentType == MimeType.Image.Gif.ToString())
                    {
                        fool.MimeType = MimeType.Image.Gif;
                    }
                    else if (this.ContentType == MimeType.Image.Jpeg.ToString())
                    {
                        fool.MimeType = MimeType.Image.Jpeg;
                    }
                    else if (this.ContentType == MimeType.Image.Png.ToString())
                    {
                        fool.MimeType = MimeType.Image.Png;
                    }
                }

                fool.XmlReaderSettings = this.XmlReaderSettings;
                fool.XmlResponseDelegate = this.XmlResponseDelegate;

                return fool;
            }
        }
    }
}
