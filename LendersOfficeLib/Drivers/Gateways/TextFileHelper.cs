﻿namespace LendersOffice.Drivers.Gateways
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;
    using LendersOffice.Constants;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Wrapper class around usage of the System.IO.File text file operations.
    /// </summary>
    public static class TextFileHelper
    {
        /// <summary>
        /// Opens the file, appends the string to the file, and then closes the file.
        /// If the file does not exist, this method creates a file, writes the specified
        /// string to the file, then closes the file.
        /// </summary>
        /// <param name="path">The file to which the text is appended.</param>
        /// <param name="text">The text that is appended to the file.</param>
        public static void AppendString(string path, string text)
        {
            if (ConstStage.UseNewFileSystem)
            {
                var localPath = ConvertFilePath(path);
                LendersOffice.Drivers.FileSystem.TextFileHelper.AppendString(localPath, text);
            }
            else
            {
                File.AppendAllText(path, text);
            }
        }

        /// <summary>
        /// Open an existing text file in append mode, or create a new file if it
        /// does not exist, for writing.
        /// </summary>
        /// <param name="path">The file to which text can be appended.</param>
        /// <param name="writeHandler">Delegate that handles writing text to the file.</param>
        public static void OpenForAppend(string path, Action<LqbTextWriter> writeHandler)
        {
            if (ConstStage.UseNewFileSystem)
            {
                var localPath = ConvertFilePath(path);

                Action<LqbGrammar.Drivers.FileSystem.LqbTextFileWriter> driverHandler = delegate(LqbGrammar.Drivers.FileSystem.LqbTextFileWriter writer)
                {
                    writeHandler(new LqbTextWriter(writer.Writer));
                };

                LendersOffice.Drivers.FileSystem.TextFileHelper.OpenForAppend(localPath, driverHandler);
            }
            else
            {
                using (StreamWriter writer = File.AppendText(path))
                {
                    writeHandler(new LqbTextWriter(writer));
                }
            }
        }

        /// <summary>
        /// Opens and existing file in append mode, or create a new file if it
        /// does not exist, for writing.  This method isn't safe because it
        /// requires the client code to close the writer.
        /// </summary>
        /// <param name="path">The file to which text can be appended.</param>
        /// <returns>An object that can be used to write the file.</returns>
        public static LqbTextWriter OpenAppend_UNSAFE(string path)
        {
            if (ConstStage.UseNewFileSystem)
            {
                // There is no driver method that will allow this to happen.  The client code
                // is going to require re-writing when we eliminate the feature flag and
                // drop this temporary helper class.
                return new LqbTextWriter(File.AppendText(path));
            }
            else
            {
                return new LqbTextWriter(File.AppendText(path));
            }
        }

        /// <summary>
        /// Creates a new text file if it doesn't exist, or overwrite an existing file,
        /// with the specified text and then closes the file.
        /// </summary>
        /// <param name="path">The file to which the text is written.</param>
        /// <param name="text">The text that is written to the file.</param>
        /// <param name="writeBOM">True if the a byte order mark should be included at the beginning of the file.</param>
        public static void WriteString(string path, string text, bool writeBOM)
        {
            if (ConstStage.UseNewFileSystem)
            {
                var localPath = ConvertFilePath(path);
                LendersOffice.Drivers.FileSystem.TextFileHelper.WriteString(localPath, text, writeBOM);
            }
            else
            {
                if (writeBOM)
                {
                    var encoding = new UTF8Encoding(true);
                    File.WriteAllText(path, text, encoding);
                }
                else
                {
                    File.WriteAllText(path, text);
                }
            }
        }

        /// <summary>
        /// Create a new text file if it doesn't exist, or overwrite an existing text file.
        /// </summary>
        /// <param name="path">The file to which text can be written.</param>
        /// <param name="writeHandler">Delegate that handles writing text to the file.</param>
        public static void OpenNew(string path, Action<LqbTextWriter> writeHandler)
        {
            if (ConstStage.UseNewFileSystem)
            {
                var localPath = ConvertFilePath(path);

                Action<LqbGrammar.Drivers.FileSystem.LqbTextFileWriter> driverHandler = delegate(LqbGrammar.Drivers.FileSystem.LqbTextFileWriter writer)
                {
                    writeHandler(new LqbTextWriter(writer.Writer));
                };

                LendersOffice.Drivers.FileSystem.TextFileHelper.OpenNew(localPath, driverHandler);
            }
            else
            {
                using (StreamWriter writer = File.CreateText(path))
                {
                    writeHandler(new LqbTextWriter(writer));
                }
            }
        }

        /// <summary>
        /// Open, read all the contents, then close a text file.
        /// </summary>
        /// <param name="path">The file that is to be read.</param>
        /// <returns>The file contents.</returns>
        public static string ReadFile(string path)
        {
            if (ConstStage.UseNewFileSystem)
            {
                var localPath = ConvertFilePath(path);
                return LendersOffice.Drivers.FileSystem.TextFileHelper.ReadFile(localPath);
            }
            else
            {
                return File.ReadAllText(path);
            }
        }

        /// <summary>
        /// Open, read all contents as a set of lines, then close a text file.
        /// Note that this method yields lines before they are all in memory
        /// so it is efficient for large files.
        /// </summary>
        /// <param name="path">The file that is to be read.</param>
        /// <returns>The file contents as a set of lines.</returns>
        public static IEnumerable<string> ReadLines(string path)
        {
            if (ConstStage.UseNewFileSystem)
            {
                var localPath = ConvertFilePath(path);
                return LendersOffice.Drivers.FileSystem.TextFileHelper.ReadLines(localPath);
            }
            else
            {
                return File.ReadLines(path);
            }
        }

        /// <summary>
        /// Open a text file for reading.
        /// </summary>
        /// <param name="path">The file that is available for reading.</param>
        /// <param name="readHandler">Delegate the reads the file.</param>
        public static void OpenRead(string path, Action<LqbTextReader> readHandler)
        {
            if (ConstStage.UseNewFileSystem)
            {
                var localPath = ConvertFilePath(path);

                Action<LqbGrammar.Drivers.FileSystem.LqbTextFileReader> driverHandler = delegate(LqbGrammar.Drivers.FileSystem.LqbTextFileReader reader)
                {
                    readHandler(new LqbTextReader(reader.Reader));
                };

                LendersOffice.Drivers.FileSystem.TextFileHelper.OpenRead(localPath, driverHandler);
            }
            else
            {
                using (StreamReader reader = File.OpenText(path))
                {
                    readHandler(new LqbTextReader(reader));
                }
            }
        }

        /// <summary>
        /// Convert a file path that is represented as a string into a validated semantic file path.
        /// </summary>
        /// <param name="path">The file path represented as a string.</param>
        /// <returns>A validated semantic file path.</returns>
        private static LocalFilePath ConvertFilePath(string path)
        {
            var localPath = LocalFilePath.Create(path);
            if (localPath == null)
            {
                throw new DeveloperException(ErrorMessage.SystemError);
            }

            return localPath.Value;
        }

        /// <summary>
        /// Class used to read text from a file.
        /// </summary>
        public sealed class LqbTextReader
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="LqbTextReader"/> class.
            /// </summary>
            /// <param name="reader">StreamReader used to read the text.</param>
            internal LqbTextReader(StreamReader reader)
            {
                this.Reader = reader;
            }

            /// <summary>
            /// Gets the encapsulated stream reader.
            /// </summary>
            /// <value>The encapsulated stream reader.</value>
            public StreamReader Reader { get; private set; }

            /// <summary>
            /// Read a line of text from the text file,
            /// or return null when at the end of the file.
            /// </summary>
            /// <returns>A line of text from the file, or null.</returns>
            public string ReadLine()
            {
                return this.Reader.ReadLine();
            }
        }

        /// <summary>
        /// Class used to write text to a file.
        /// </summary>
        public sealed class LqbTextWriter
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="LqbTextWriter"/> class.
            /// </summary>
            /// <param name="writer">StreamWriter used to write the text.</param>
            internal LqbTextWriter(StreamWriter writer)
            {
                this.Writer = writer;
            }

            /// <summary>
            /// Gets the encapsulated stream writer.
            /// </summary>
            /// <value>The encapsulated stream writer.</value>
            public StreamWriter Writer { get; private set; }

            /// <summary>
            /// Sets a value indicating whether the stream should be auto-flushed.
            /// </summary>
            /// <value>A value indicating whether the stream should be auto-flushed.</value>
            public bool AutoFlush
            {
                set
                {
                    this.Writer.AutoFlush = value;
                }
            }

            /// <summary>
            /// Flush the data in the stream.
            /// </summary>
            public void Flush()
            {
                this.Writer.Flush();
            }

            /// <summary>
            /// Append text to a file and add a new line.
            /// </summary>
            /// <param name="text">The text that will be added to the file.</param>
            public void AppendLine(string text)
            {
                this.Writer.WriteLine(text);
            }

            /// <summary>
            /// Append text to a file without adding a new line.
            /// </summary>
            /// <param name="text">The text that will be added to the file.</param>
            public void Append(string text)
            {
                this.Writer.Write(text);
            }

            /// <summary>
            /// Close the encapsulated writer.  This should only be
            /// called when the client has taken control over the
            /// writer's lifetime by calling TextFileHelper.OpenAppend_UNSAFE().
            /// </summary>
            public void Close()
            {
                this.Writer.Close();
            }
        }
    }
}
