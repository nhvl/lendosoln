﻿namespace LendersOffice.Drivers.Gateways
{
    using System;
    using System.IO;
    using LendersOffice.Constants;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Wrapper class around usage of the System.IO.File binary file operations.
    /// </summary>
    public static class BinaryFileHelper
    {
        /// <summary>
        /// Creates a new file, writes the specified byte array to the file,
        /// and then closes the file.  If the target file already exists, it is overwritten.
        /// </summary>
        /// <param name="path">The file to which the data is written.</param>
        /// <param name="bytes">The data that is written to the file.</param>
        public static void WriteAllBytes(string path, byte[] bytes)
        {
            if (ConstStage.UseNewFileSystem)
            {
                var localPath = ConvertFilePath(path);
                LendersOffice.Drivers.FileSystem.BinaryFileHelper.WriteAllBytes(localPath, bytes);
            }
            else
            {
                File.WriteAllBytes(path, bytes);
            }
        }

        /// <summary>
        /// Creates a new file, writes all the data from the input stream to the file,
        /// and then closes the file.  If the target file already exists, it is overwritten.
        /// </summary>
        /// <param name="path">The file to which the data is written.</param>
        /// <param name="stream">The data stream that is written to the file.</param>
        public static void WriteAllBytes(string path, Stream stream)
        {
            if (ConstStage.UseNewFileSystem)
            {
                var localPath = ConvertFilePath(path);

                byte[] bytes = null;
                if (stream is MemoryStream)
                {
                    bytes = (stream as MemoryStream).ToArray();
                }
                else
                {
                    using (MemoryStream mem = new MemoryStream())
                    {
                        stream.CopyTo(mem);
                        bytes = mem.ToArray();
                    }
                }

                LendersOffice.Drivers.FileSystem.BinaryFileHelper.WriteAllBytes(localPath, bytes);
            }
            else
            {
                using (var fs = File.Create(path))
                {
                    stream.CopyTo(fs);
                }
            }
        }

        /// <summary>
        /// Create or overwrite a binary file.
        /// </summary>
        /// <param name="path">The file to create or overwrite.</param>
        /// <param name="writeHandler">Delegate that writes data to the file.</param>
        public static void OpenNew(string path, Action<LqbBinaryStream> writeHandler)
        {
            if (ConstStage.UseNewFileSystem)
            {
                var localPath = ConvertFilePath(path);

                Action<LqbGrammar.Drivers.FileSystem.LqbBinaryStream> driverHandler = delegate(LqbGrammar.Drivers.FileSystem.LqbBinaryStream fs)
                {
                    writeHandler(new LqbBinaryStream(fs.Stream));
                };

                LendersOffice.Drivers.FileSystem.BinaryFileHelper.OpenNew(localPath, driverHandler);
            }
            else
            {
                using (FileStream stream = File.Create(path))
                {
                    writeHandler(new LqbBinaryStream(stream));
                }
            }
        }

        /// <summary>
        /// Open an existing binary file for reading.
        /// </summary>
        /// <param name="path">The file to open.</param>
        /// <param name="readHandler">Delegate that reads data from the file.</param>
        public static void OpenRead(string path, Action<LqbBinaryStream> readHandler)
        {
            if (ConstStage.UseNewFileSystem)
            {
                var localPath = ConvertFilePath(path);

                Action<LqbGrammar.Drivers.FileSystem.LqbBinaryStream> driverHandler = delegate(LqbGrammar.Drivers.FileSystem.LqbBinaryStream fs)
                {
                    readHandler(new LqbBinaryStream(fs.Stream));
                };

                LendersOffice.Drivers.FileSystem.BinaryFileHelper.OpenRead(localPath, driverHandler);
            }
            else
            {
                using (FileStream stream = File.OpenRead(path))
                {
                    readHandler(new LqbBinaryStream(stream));
                }
            }
        }

        /// <summary>
        /// Read all the data within a binary file.
        /// </summary>
        /// <param name="path">The binary file from which the data will be read.</param>
        /// <returns>The data read from the binary file.</returns>
        public static byte[] ReadAllBytes(string path)
        {
            if (ConstStage.UseNewFileSystem)
            {
                var localPath = ConvertFilePath(path);
                return LendersOffice.Drivers.FileSystem.BinaryFileHelper.ReadAllBytes(localPath);
            }
            else
            {
                return File.ReadAllBytes(path);
            }
        }

        /// <summary>
        /// Convert a file path that is represented as a string into a validated semantic file path.
        /// </summary>
        /// <param name="path">The file path represented as a string.</param>
        /// <returns>A validated semantic file path.</returns>
        private static LocalFilePath ConvertFilePath(string path)
        {
            var localPath = LocalFilePath.Create(path);
            if (localPath == null)
            {
                throw new DeveloperException(ErrorMessage.SystemError);
            }

            return localPath.Value;
        }

        /// <summary>
        /// Wrapper around a binary file's FileStream.
        /// </summary>
        public sealed class LqbBinaryStream
        {
            /// <summary>
            /// The FileStream used to write data.
            /// </summary>
            private FileStream stream;

            /// <summary>
            /// Initializes a new instance of the <see cref="LqbBinaryStream"/> class.
            /// </summary>
            /// <param name="stream">FileStream used to write data.</param>
            internal LqbBinaryStream(FileStream stream)
            {
                this.stream = stream;
            }

            /// <summary>
            /// Gets the underlying FileStream so client code can pass it into a utility.
            /// </summary>
            /// <value>The underlying FileStream.</value>
            public FileStream Stream
            {
                get
                {
                    return this.stream;
                }
            }
        }
    }
}
