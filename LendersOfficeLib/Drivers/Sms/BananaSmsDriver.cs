﻿namespace LendersOffice.Drivers.Sms
{
    using Constants;
    using LqbGrammar.Adapters;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Driver that uses the Banana Api adpater to send SMS messages.
    /// </summary>
    internal sealed class BananaSmsDriver : ISmsDriver
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BananaSmsDriver"/> class.
        /// </summary>
        /// <param name="adapter">The adapter used to send the SMS messages.</param>
        /// <param name="monitor">Optional monitor, can be null.</param>
        public BananaSmsDriver(ISmsAdapter adapter, ISmsMonitor monitor)
        {
            this.Adapter = adapter;
            this.Monitor = monitor;
        }

        /// <summary>
        /// Gets or sets the adapter used to send the SMS messages.
        /// </summary>
        private ISmsAdapter Adapter { get; set; }

        /// <summary>
        /// Gets or sets the interface used to monitor the details of the adapter's operation.
        /// </summary>
        /// <value>Used to monitor the details of the adapter's operation.</value>
        private ISmsMonitor Monitor { get; set; }

        /// <summary>
        /// Send an authentication code message via SMS.
        /// </summary>
        /// <param name="number">The phone number to which the SMS message will be sent.</param>
        /// <param name="message">A message containing an LQB authentication code.</param>
        /// <returns>True if OK response recieved, false otherwise.</returns>
        public bool SendAuthenticationCode(PhoneNumber number, SmsAuthCodeMessage message)
        {
            return this.Adapter.SendAuthenticationCode(number, message, this.Monitor);
        }
    }
}
