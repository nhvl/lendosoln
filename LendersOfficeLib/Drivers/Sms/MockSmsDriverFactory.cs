﻿namespace LendersOffice.Drivers.Sms
{
    using System;
    using System.Linq;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LqbGrammar;
    using LqbGrammar.Adapters;
    using LqbGrammar.Drivers;

    /// <summary>
    /// Factory for creating mock SMS drivers.
    /// </summary>
    public sealed class MockSmsDriverFactory : ISmsDriverFactory, ISmsMonitor
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MockSmsDriverFactory"/> class.
        /// </summary>
        /// <param name="useTestPage">True if the test page is the target url rather than the production SMS web service.</param>
        /// <param name="shouldSucceed">True if the send message call should return as successful, false otherwise.</param>
        public MockSmsDriverFactory(bool useTestPage, bool shouldSucceed)
        {
            this.UseTestPage = useTestPage;
            this.ShouldSucceed = shouldSucceed;
        }

        /// <summary>
        /// Gets the phone number where the message was to be sent.
        /// </summary>
        /// <value>The phone number where the message was to be sent.</value>
        public string PhoneNumber { get; internal set; }

        /// <summary>
        /// Gets the message that was to be sent.
        /// </summary>
        /// <value>The message that was to be sent.</value>
        public string Message { get; internal set; }

        /// <summary>
        /// Gets the request sent to the test page.
        /// </summary>
        /// <value>The request sent to the test page.</value>
        public string TestPageRequest { get; internal set; }

        /// <summary>
        /// Gets the response from the test page.
        /// </summary>
        /// <value>The response from the test page.</value>
        public string TestPageResponse { get; internal set; }

        /// <summary>
        /// Gets or sets a value indicating whether the test page should receive the message request (not the SMS, just the api call).
        /// </summary>
        /// <value>A value indicating whether the test page should receive the message request.</value>
        private bool UseTestPage { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the send message call should succeed.
        /// </summary>
        /// <value>A value indicating whether the send message call should succeed.</value>
        private bool ShouldSucceed { get; set; }

        /// <summary>
        /// Create an implementation of the ISmsDriver interface.
        /// </summary>
        /// <returns>An implementation of the ISmsDriver interface.</returns>
        public ISmsDriver Create()
        {
            if (this.UseTestPage)
            {
                string url = "http://localhost/LendersOfficeApp/LOAdmin/Test/DumpRequest.aspx";
                string source = ConstStage.BananaSecuritySmsSourceNumber;
                string token = ConstStage.BananaSecurityToken;

                string password = "Password";
                string pwd = EncryptionHelper.Encrypt(password);

                // TODO: BananaSecuritySmsSourceNumber has been updated to contain multiple numbers. The SMS adapters and drivers need to be updated to account for this.
                var bananaSource = LqbGrammar.DataTypes.PhoneNumber.CreateWithValidation(source.Split(new char[] { '|' }, System.StringSplitOptions.RemoveEmptyEntries).ElementAtOrDefault(0));

                // Assume that the correct adapter factory has been registered, since there are no others.
                var factory = GenericLocator<ISmsAdapterFactory>.Factory;
                var adapter = factory.Create(bananaSource.Value, url, token, pwd);

                return new BananaSmsDriver(adapter, this);
            }
            else
            {
                return new MockSmsDriver(this, this.ShouldSucceed);
            }
        }

        /// <summary>
        /// Save the SMS api request.
        /// </summary>
        /// <param name="message">The SMS api request.</param>
        public void BeforeSend(string message)
        {
            this.TestPageRequest = message;
        }

        /// <summary>
        /// Save the response from the test page.
        /// </summary>
        /// <param name="code">Should be 200.</param>
        /// <param name="response">The response from the test page, should be the path to a temp file.</param>
        public void AfterSend(string code, string response)
        {
            this.TestPageResponse = response;
        }

        /// <summary>
        /// Handle an exception, this method ignores the exception.
        /// </summary>
        /// <param name="ex">The parameter is not used.</param>
        public void OnException(Exception ex)
        {
            // do nothing
        }
    }
}
