﻿namespace LendersOffice.Drivers.Sms
{
    using System;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;

    /// <summary>
    /// Mock SMS driver.
    /// </summary>
    internal sealed class MockSmsDriver : ISmsDriver
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MockSmsDriver"/> class.
        /// </summary>
        /// <param name="factory">The factory where the data will be saved for examination by client code.</param>
        /// <param name="shouldSucceed">True if the send message call should return as successful, false otherwise.  Ignored if useTestPage is true.</param>
        public MockSmsDriver(MockSmsDriverFactory factory, bool shouldSucceed)
        {
            this.Factory = factory;
            this.ShouldSucceed = shouldSucceed;
        }

        /// <summary>
        /// Gets or sets the factory where the data will be saved for examination by client code.
        /// </summary>
        /// <value>The factory where the data will be saved for examination by client code.</value>
        private MockSmsDriverFactory Factory { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the send message call should succeed.
        /// </summary>
        /// <value>A value indicating whether the send message call should succeed.</value>
        private bool ShouldSucceed { get; set; }

        /// <summary>
        /// Send an authentication code message via SMS.
        /// </summary>
        /// <param name="number">The phone number to which the SMS message will be sent.</param>
        /// <param name="message">A message containing an LQB authentication code.</param>
        /// <returns>True if OK response recieved, false otherwise.</returns>
        public bool SendAuthenticationCode(PhoneNumber number, SmsAuthCodeMessage message)
        {
            this.Factory.PhoneNumber = number.ToString();
            this.Factory.Message = message.ToString();
            return this.ShouldSucceed;
        }
    }
}
