﻿namespace LendersOffice.Drivers.Sms
{
    using System.Linq;
    using Constants;
    using LqbGrammar;
    using LqbGrammar.Adapters;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Factory for creating drivers that use the Banana Api to send SMS messages.
    /// </summary>
    public sealed class BananaSmsDriverFactory : ISmsDriverFactory
    {
        /// <summary>
        /// Create an implementation of the ISmsDriver interface.
        /// </summary>
        /// <returns>An implementation of the ISmsDriver interface.</returns>
        public ISmsDriver Create()
        {
            return Create(null);
        }

        /// <summary>
        /// Create an implementation of the ISmsDriver interface.
        /// </summary>
        /// <param name="monitor">An optional monitor.</param>
        /// <returns>An implementation of the ISmsDriver interface.</returns>
        internal static ISmsDriver Create(ISmsMonitor monitor)
        {
            string url = ConstStage.BananaSecuritySmsUrl;
            string source = ConstStage.BananaSecuritySmsSourceNumber;
            string token = ConstStage.BananaSecurityToken;
            string pwd = ConstStage.BananaSecurityPassword;

            // TODO: BananaSecuritySmsSourceNumber has been updated to contain multiple numbers. The SMS adapters and drivers need to be updated to account for this.
            var bananaSource = PhoneNumber.CreateWithValidation(source.Split(new char[] { '|' }, System.StringSplitOptions.RemoveEmptyEntries).ElementAtOrDefault(0));
            if (bananaSource == null)
            {
                throw new DeveloperException(ErrorMessage.BadConfiguration);
            }

            // Assume that the correct adapter factory has been registered, since there are no others.
            var factory = GenericLocator<ISmsAdapterFactory>.Factory;
            var adapter = factory.Create(bananaSource.Value, url, token, pwd);

            return new BananaSmsDriver(adapter, monitor);
        }
    }
}
