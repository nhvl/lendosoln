﻿namespace LendersOffice.Drivers.Sms
{
    using LqbGrammar;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;

    /// <summary>
    /// Utility class that uses an Sms driver to send
    /// SMS messages.
    /// </summary>
    public static class SmsHelper
    {
        /// <summary>
        /// Send an authentication code to a phone number in the form of a text message.
        /// </summary>
        /// <param name="number">The phone number to which the message is sent.</param>
        /// <param name="message">The message containing the authentication code.</param>
        /// <returns>True if the message was sent successfully, false othwerwise (or an exception may be thrown as well).</returns>
        public static bool SendMessage(PhoneNumber number, SmsAuthCodeMessage message)
        {
            var factory = GenericLocator<ISmsDriverFactory>.Factory;
            var driver = factory.Create();
            return driver.SendAuthenticationCode(number, message);
        }
    }
}
