﻿namespace LendersOffice.Drivers.Sms
{
    using System;
    using System.Text.RegularExpressions;
    using DataAccess;
    using LqbGrammar.Adapters;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;

    /// <summary>
    /// Driver that logs the SMS communication details of the Banana Api for sending SMS messages.
    /// The implementation technique is somewhat awkward, but the intent is to make this appear
    /// as a decorator to the underlying simple driver.
    /// </summary>
    internal sealed class LoggingBananaSmsDriver : ISmsDriver
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LoggingBananaSmsDriver"/> class.
        /// </summary>
        /// <param name="driver">The simple driver that passed calls on to the adapter.</param>
        public LoggingBananaSmsDriver(ISmsDriver driver)
        {
            this.SimpleDriver = driver;
        }

        /// <summary>
        /// Gets or sets the simple driver that passed calls on to the adapter.
        /// </summary>
        /// <value>The simple driver that passed calls on to the adapter.</value>
        private ISmsDriver SimpleDriver { get; set; }

        /// <summary>
        /// The driver factory will require a monitor prior to creating the driver,
        /// so this convenience method is used to get the monitor.
        /// </summary>
        /// <param name="uri">The url for the Banana Api webservice, which finds its way into the logged messages.</param>
        /// <returns>A monitor that will log details about the Banana Api communication.</returns>
        public static ISmsMonitor CreateMonitor(string uri)
        {
            return new LoggingMonitor(uri);
        }

        /// <summary>
        /// Send an authentication code message via SMS.
        /// </summary>
        /// <param name="number">The phone number to which the SMS message will be sent.</param>
        /// <param name="message">A message containing an LQB authentication code.</param>
        /// <returns>True if OK response recieved, false otherwise.</returns>
        public bool SendAuthenticationCode(PhoneNumber number, SmsAuthCodeMessage message)
        {
            return this.SimpleDriver.SendAuthenticationCode(number, message);
        }

        /// <summary>
        /// Encapsulate the logging logic.
        /// </summary>
        private sealed class LoggingMonitor : ISmsMonitor
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="LoggingMonitor"/> class.
            /// </summary>
            /// <param name="uri">The URL for the Banana Api web service.</param>
            public LoggingMonitor(string uri)
            {
                this.BananaURL = uri;
            }

            /// <summary>
            /// Gets or sets the URL to the Banana API web service.
            /// </summary>
            /// <value>The URL to the Banana API web service.</value>
            private string BananaURL { get; set; }

            /// <summary>
            /// The actual message sent to the SMS provider, which may differ from the content of the SMS message.
            /// </summary>
            /// <param name="message">The full message passed to the SMS provider.</param>
            public void BeforeSend(string message)
            {
                string loggingXml = message;
                loggingXml = Regex.Replace(loggingXml, " password=\"[^ ]+\"", " password=\"******\"");
                loggingXml = Regex.Replace(loggingXml, "Authentication Code: [^ <]+([ <])", "Authentication Code: *****$1");
                loggingXml = Regex.Replace(loggingXml, "phone_number=\"[^ ]+\"", "phone_number=\"******\"");
                loggingXml = Regex.Replace(loggingXml, "source_number=\"[^ ]+\"", "source_number=\"******\"");
                Tools.LogInfo("BananaSecuritySmsRequest", loggingXml);
            }

            /// <summary>
            /// The adapter only returns a boolean to flag success or failure of the SMS message.
            /// More detailed information can be reported to this interface.
            /// </summary>
            /// <param name="code">A success or error code can be passed here.</param>
            /// <param name="response">A response message can be passed here.</param>
            public void AfterSend(string code, string response)
            {
                Tools.LogInfo("BananaSecuritySmsResponse", response);

                if (code != null)
                {
                    Tools.LogError($"Error code: {code}.");
                }
            }

            /// <summary>
            /// If an exception is thrown by the SMS provider, this is called instead of AfterSend.
            /// </summary>
            /// <param name="ex">The exception thrown by the SMS provider.</param>
            public void OnException(Exception ex)
            {
                if (ex is SystemException)
                {
                    Tools.LogError($"URL hit: <{this.BananaURL}>{Environment.NewLine}", ex);
                }
            }
        }
    }
}
