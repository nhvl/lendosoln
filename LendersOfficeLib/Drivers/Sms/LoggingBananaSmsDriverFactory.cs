﻿namespace LendersOffice.Drivers.Sms
{
    using Constants;
    using LqbGrammar.Drivers;

    /// <summary>
    /// Factory for creating logging drivers for sending SMS messages via the Banana Api.
    /// </summary>
    public sealed class LoggingBananaSmsDriverFactory : ISmsDriverFactory
    {
        /// <summary>
        /// Create an implementation of the ISmsDriver interface.
        /// </summary>
        /// <returns>An implementation of the ISmsDriver interface.</returns>
        public ISmsDriver Create()
        {
            string url = ConstStage.BananaSecuritySmsUrl;
            var monitor = LoggingBananaSmsDriver.CreateMonitor(url);

            var simpleDriver = BananaSmsDriverFactory.Create(monitor);
            return new LoggingBananaSmsDriver(simpleDriver);
        }
    }
}
