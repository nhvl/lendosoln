﻿namespace LendersOffice.Drivers.ConversationLog
{
    using System;
    using System.Collections.Generic;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers.ConversationLog;

    /// <summary>
    /// Mock object for testing the conversation log client code.
    /// </summary>
    public class MockConversationLogDriver : ICategoryDriver, ICommentDriver
    {
        /// <summary>
        /// List of categories.
        /// </summary>
        private List<Category> categories;

        /// <summary>
        /// List of conversations.
        /// </summary>
        private List<Conversation> conversations; // NOTE: assumed that Comments is List<Comment>.

        /// <summary>
        /// Used like a DB current identity value.
        /// </summary>
        private long maxCategoryId;

        /// <summary>
        /// Used like a DB current identity value.
        /// </summary>
        private long maxCommentId;

        /// <summary>
        /// Initializes a new instance of the <see cref="MockConversationLogDriver"/> class.
        /// </summary>
        /// <param name="cats">The list of categories.</param>
        /// <param name="testData">The list of convesations.</param>
        internal MockConversationLogDriver(List<Category> cats, List<Conversation> testData)
        {
            this.categories = cats;
            this.conversations = testData;

            this.maxCategoryId = this.GetMaxCategoryId();
            this.maxCommentId = this.GetMaxCommentId();
        }

        /// <summary>
        /// Retrieve all conversations that have been attached to an LQB resource.
        /// </summary>
        /// <param name="secToken">Security token for the authenticated principal making this method call.</param>
        /// <param name="resource">The LQB resource.</param>
        /// <returns>All conversations.  The order is determined by the category order, followed by the creation dates of the conversations.</returns>
        public IEnumerable<Conversation> GetAllConversations(SecurityToken secToken, ResourceId resource)
        {
            return this.conversations;
        }

        /// <summary>
        /// Post a new comment to a category, beginning a new conversation. <para/>
        /// Sets the id, created date, and depth to 0 of the new comment.
        /// </summary>
        /// <param name="secToken">Security token for the authenticated principal making this method call.</param>
        /// <param name="resource">The LQB resource.</param>
        /// <param name="category">The category under which this comment is posted.</param>
        /// <param name="comment">The comment data.</param>
        /// <param name="permissionLevelId">The id of the permission level associated with the conversation, or null if not yet migrated.</param>
        /// <param name="bypassPermissionCheck">Bit to bypass the permission check.</param>
        public void Post(SecurityToken secToken, ResourceId resource, CategoryReference category, Comment comment, long? permissionLevelId, bool bypassPermissionCheck = false)
        {
            DateTime created = DateTime.UtcNow;

            int currIndex = -1;
            int nextIndex = -1;
            foreach (var item in this.conversations)
            {
                ++currIndex;

                if (this.InOrder(category.Id, item.Category.Id))
                {
                    nextIndex = currIndex;
                    break;
                }
            }

            comment.Created = LqbEventDate.Create(created).Value;
            comment.Identity.Id = ++this.maxCommentId;
            comment.Depth = Depth.Create(0).Value;

            var list = new List<Comment>();
            list.Add(comment);

            var convo = new Conversation();
            convo.Category = category;
            convo.Comments = list;
            convo.PermissionLevelId = permissionLevelId;

            if (nextIndex < 0)
            {
                this.conversations.Add(convo);
            }
            else
            {
                this.conversations.Insert(nextIndex, convo);
            }
        }

        /// <summary>
        /// Reply to an existing comment in the conversation log system. <para/>
        /// Sets the date, id, and depth of the comment.
        /// </summary>
        /// <param name="secToken">Security token for the authenticated principal making this method call.</param>
        /// <param name="parent">The comment toward which this reply is directed.</param>
        /// <param name="comment">The comment data that is the reply.</param>
        public void Reply(SecurityToken secToken, CommentReference parent, Comment comment)
        {
            // This shouldn't be called too often or on complex graphs as the order isn't strictly maintained here.
            // We aren't testing the ordering per se, we are just testing that the GUI adds the reply to the display.
            DateTime created = DateTime.UtcNow;

            Conversation containingConversation = null;
            Comment actualParentComment = null;

            // this is the index of the matching parents next sibling, which is where the reply should go by order.
            int parentsNextSiblingsCommentIndex = 0;
            foreach (var convo in this.conversations)
            {
                parentsNextSiblingsCommentIndex = 0;
                foreach (var comm in convo.Comments)
                {
                    if (actualParentComment != null)
                    {
                        if (comm.Depth.Value <= actualParentComment.Depth.Value)
                        {
                            break;
                        }
                    }
                    else if (comm.Identity.Id == parent.Id)
                    {
                        actualParentComment = comm;
                        containingConversation = convo;
                    }

                    parentsNextSiblingsCommentIndex++;
                }

                if (actualParentComment != null)
                {
                    break;
                }
            }

            if (actualParentComment != null)
            {
                comment.Created = LqbEventDate.Create(created).Value;
                comment.Identity.Id = ++this.maxCommentId;
                comment.Depth = Depth.Create(actualParentComment.Depth.Value + 1).Value;

                var previousComments = new List<Comment>(containingConversation.Comments);
                previousComments.Insert(parentsNextSiblingsCommentIndex, comment);
                containingConversation.Comments = previousComments;
            }
            else
            {
                throw new Exception("no matching comment to reply to.");
            }
        }

        /// <summary>
        /// Retrieve all the categories relevant to the logged in user, in the current order.
        /// </summary>
        /// <param name="secToken">Security token for the authenticated principal making this method call.</param>
        /// <returns>The list of categories.</returns>
        public List<Category> GetAllCategories(SecurityToken secToken)
        {
            return this.categories;
        }

        /// <summary>
        /// Either create a new category or modify and existing category.
        /// </summary>
        /// <param name="secToken">Security token for the authenticated principal making this method call.</param>
        /// <param name="category">A category in the comment log system.</param>
        /// <returns>The current state of the category in the log system after the method has completed.</returns>
        public Category SetCategory(SecurityToken secToken, Category category)
        {
            if (category.Identity.Id >= 0)
            {
                foreach (var cat in this.categories)
                {
                    if (cat.Identity.Id == category.Identity.Id)
                    {
                        cat.Identity.Name = category.Identity.Name;
                        cat.IsActive = category.IsActive;
                        cat.DisplayName = category.DisplayName;
                    }
                }
            }
            else
            {
                category.Identity.Id = ++this.maxCategoryId;
                this.categories.Add(category);
            }

            return category;
        }

        /// <summary>
        /// Set the order in which categories are to be presented to users of the comment system.
        /// </summary>
        /// <param name="secToken">Security token for the authenticated principal making this method call.</param>
        /// <param name="categoryOrder">An ordered list of category references, the order will be reflected in subsequent GUI views.</param>
        public void SetCategoryOrder(SecurityToken secToken, List<CategoryReference> categoryOrder)
        {
            var newCats = new List<Category>();
            var newConvos = new List<Conversation>();

            foreach (var item in categoryOrder)
            {
                foreach (var cat in this.categories)
                {
                    if (cat.Identity.Id == item.Id)
                    {
                        newCats.Add(cat);
                        break;
                    }
                }

                foreach (var convo in this.conversations)
                {
                    if (convo.Category.Id == item.Id)
                    {
                        newConvos.Add(convo);
                    }
                }
            }

            this.categories = newCats;
            this.conversations = newConvos;
        }

        /// <summary>
        /// Hides the specified comment.  Not implemented yet.
        /// </summary>
        /// <param name="secToken">Security token for the authenticated principal making this method call.</param>
        /// <param name="commentId">The id of the comment to be hidden.</param>
        /// <returns>True iff it succeeded.</returns>
        public bool HideComment(SecurityToken secToken, CommentReference commentId)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Unhides the specified comment. Not implemented yet.
        /// </summary>
        /// <param name="secToken">Security token for the authenticated principal making this method call.</param>
        /// <param name="commentId">The id of the comment to be unhidden.</param>
        /// <returns>True iff it succeeded.</returns>
        public bool ShowComment(SecurityToken secToken, CommentReference commentId)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Determine whether the second category id is supposed to follow the first category id.
        /// </summary>
        /// <param name="first">The first category id.</param>
        /// <param name="second">The second category id.</param>
        /// <returns>True if the category ids are in the correct order, false otherwise.</returns>
        private bool InOrder(long first, long second)
        {
            if (first == second)
            {
                return false;
            }

            foreach (var cat in this.categories)
            {
                if (cat.Identity.Id == first)
                {
                    return true;
                }
                else if (cat.Identity.Id == second)
                {
                    return false;
                }
            }

            return false; // should never get here...else the input test data is incorrect
        }

        /// <summary>
        /// Calculates the maximum id for the list of categories.
        /// </summary>
        /// <returns>The maximum id for the list of categories.</returns>
        private long GetMaxCategoryId()
        {
            long max = -1;
            foreach (var cat in this.categories)
            {
                if (cat.Identity.Id > max)
                {
                    max = cat.Identity.Id;
                }
            }

            return max;
        }

        /// <summary>
        /// Calculates the maximum id for the comments with the list of conversations.
        /// </summary>
        /// <returns>The maximum id for the comments.</returns>
        private long GetMaxCommentId()
        {
            long max = -1;
            foreach (var convo in this.conversations)
            {
                foreach (var comm in convo.Comments)
                {
                    if (comm.Identity.Id > max)
                    {
                        max = comm.Identity.Id;
                    }
                }
            }

            return max;
        }
    }
}
