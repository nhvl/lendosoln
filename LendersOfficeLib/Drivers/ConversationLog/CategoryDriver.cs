﻿namespace LendersOffice.Drivers.ConversationLog
{
    using System.Collections.Generic;
    using LqbGrammar;
    using LqbGrammar.Adapters;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers.ConversationLog;

    /// <summary>
    /// Driver for managing the categories in the conversation log system.
    /// </summary>
    internal sealed class CategoryDriver : ICategoryDriver
    {
        /// <summary>
        /// The adapter that communicates with the conversation log system.
        /// </summary>
        private ICategoryAdapter adapter;

        /// <summary>
        /// Initializes a new instance of the <see cref="CategoryDriver"/> class.
        /// </summary>
        /// <param name="location">The location of the service.</param>
        /// <param name="port">The port on which the service is hosted.</param>
        public CategoryDriver(string location, int port)
        {
            var factory = GenericLocator<ICommentLogAdapterFactory>.Factory;
            this.adapter = factory.CreateCategoryAdapter(location, port);
        }

        /// <summary>
        /// Retrieve all the categories relevant to the logged in user, in the current order.
        /// </summary>
        /// <param name="secToken">Security token for the authenticated principal making this method call.</param>
        /// <returns>The list of categories.</returns>
        public List<Category> GetAllCategories(SecurityToken secToken)
        {
            return this.adapter.GetAllCategories(secToken);
        }

        /// <summary>
        /// Either create a new category or modify and existing category.
        /// </summary>
        /// <param name="secToken">Security token for the authenticated principal making this method call.</param>
        /// <param name="category">A category in the comment log system.</param>
        /// <returns>The current state of the category in the log system after the method has completed.</returns>
        public Category SetCategory(SecurityToken secToken, Category category)
        {
            return this.adapter.SetCategory(secToken, category);
        }

        /// <summary>
        /// Set the order in which categories are to be presented to users of the comment system.
        /// </summary>
        /// <param name="secToken">Security token for the authenticated principal making this method call.</param>
        /// <param name="categoryOrder">An ordered list of category references, the order will be reflected in subsequent GUI views.</param>
        public void SetCategoryOrder(SecurityToken secToken, List<CategoryReference> categoryOrder)
        {
            this.adapter.SetCategoryOrder(secToken, categoryOrder);
        }
    }
}
