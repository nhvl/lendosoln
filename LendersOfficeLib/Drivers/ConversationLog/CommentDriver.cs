﻿namespace LendersOffice.Drivers.ConversationLog
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Constants;
    using LqbGrammar;
    using LqbGrammar.Adapters;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers.ConversationLog;
    using LqbGrammar.Exceptions;
    using ObjLib.Security.Authorization.ConversationLog;

    /// <summary>
    /// Driver for managing the comments in the conversation log system.
    /// </summary>
    /// <remarks>
    /// TODO: put all permission checks lower, e.g. in the service.
    /// </remarks>
    internal sealed class CommentDriver : ICommentDriver
    {
        /// <summary>
        /// The adapter that communicates with the conversation log system.
        /// </summary>
        private ICommentAdapter adapter;

        /// <summary>
        /// Initializes a new instance of the <see cref="CommentDriver"/> class.
        /// </summary>
        /// <param name="location">The location of the service.</param>
        /// <param name="port">The port on which the service is hosted.</param>
        public CommentDriver(string location, int port)
        {
            var factory = GenericLocator<ICommentLogAdapterFactory>.Factory;
            this.adapter = factory.CreateCommentAdapter(location, port);
        }

        /// <summary>
        /// Retrieve the conversations that the user has access to that have been attached to the specified LQB resource.
        /// </summary>
        /// <param name="secToken">Security token for the authenticated principal making this method call.</param>
        /// <param name="resource">The LQB resource.</param>
        /// <returns>All conversations.  The order is determined by the category order, followed by the creation dates of the conversations.</returns>
        public IEnumerable<Conversation> GetAllConversations(SecurityToken secToken, ResourceId resource)
        {
            var conversations = this.adapter.GetAllConversations(secToken, resource);
            var conversationsUserCanView = new List<Conversation>();
            var permissionLevelIdsToCheckPermissionFor = new HashSet<long?>(conversations.Select(c => c.PermissionLevelId));
            var canHideByPermissionLevelId = new Dictionary<long, bool>(); ////! ummm... need to set the comment text to empty per comment.
            var canViewByPermissionLevelId = new Dictionary<long, bool>();            

            bool canViewNullPermissionLevel = false;
            bool canHideNullPermissionLevel = false;
            foreach (var permissionLevelId in permissionLevelIdsToCheckPermissionFor)
            {
                if (permissionLevelId == null)
                {
                    long? nullId = null;
                    canViewNullPermissionLevel = PermissionLevelAuthorizer.Instance.CanRun(secToken, nullId, ConvoLogOperationType.View);
                    canHideNullPermissionLevel = PermissionLevelAuthorizer.Instance.CanRun(secToken, nullId, ConvoLogOperationType.Hide);
                }
                else
                {
                    canViewByPermissionLevelId.Add(permissionLevelId.Value, PermissionLevelAuthorizer.Instance.CanRun(secToken, permissionLevelId, ConvoLogOperationType.View));
                    canHideByPermissionLevelId.Add(permissionLevelId.Value, PermissionLevelAuthorizer.Instance.CanRun(secToken, permissionLevelId, ConvoLogOperationType.Hide));
                }
            }

            foreach (var conversation in conversations)
            {
                var permissionLevelId = conversation.PermissionLevelId;
                if (permissionLevelId.HasValue)
                {
                    if (canViewByPermissionLevelId[permissionLevelId.Value])
                    {
                        if (!canHideByPermissionLevelId[permissionLevelId.Value])
                        {
                            this.HideHiddenCommentsForConvesation(conversation);
                        }

                        conversationsUserCanView.Add(conversation);                        
                    }
                }
                else
                {
                    if (canViewNullPermissionLevel)
                    {
                        if (canViewNullPermissionLevel)
                        {
                            this.HideHiddenCommentsForConvesation(conversation);
                        }

                        conversationsUserCanView.Add(conversation);
                    }
                }
            }

            return conversationsUserCanView;
        }

        /// <summary>
        /// Post a new comment to a category, beginning a new conversation, or throw an error if the user lacks permission.
        /// </summary>
        /// <param name="secToken">Security token for the authenticated principal making this method call.</param>
        /// <param name="resource">The LQB resource.</param>
        /// <param name="category">The category under which this comment is posted.</param>
        /// <param name="comment">The comment data.</param>
        /// <param name="permissionLevelId">The id of the permission level associated with the conversation, or null if not yet migrated.</param>
        /// <param name="bypassPermissionCheck">Bit to bypass the permission check.</param>
        public void Post(SecurityToken secToken, ResourceId resource, CategoryReference category, Comment comment, long? permissionLevelId, bool bypassPermissionCheck = false)
        {
            if (!bypassPermissionCheck && !PermissionLevelAuthorizer.Instance.CanRun(secToken, permissionLevelId, ConvoLogOperationType.Post))
            {
                throw new DeveloperException(ErrorMessage.FromTemplate(ErrorMessageTemplate.PermissionDenied, "post to", "conversation with this permission level"));
            }

            if (permissionLevelId < 0)
            {
                permissionLevelId = null;
            }

            this.adapter.Post(secToken, resource, category, comment, permissionLevelId);
        }

        /// <summary>
        /// Reply to an existing comment in the conversation log system, or throw an error if the user lacks permission.
        /// </summary>
        /// <param name="secToken">Security token for the authenticated principal making this method call.</param>
        /// <param name="parent">The comment toward which this reply is directed.</param>
        /// <param name="comment">The comment data that is the reply.</param>
        public void Reply(SecurityToken secToken, CommentReference parent, Comment comment)
        {
            var permissionLevelId = this.GetPermissionLevelIdByCommentReference(secToken, parent);
               
            if (!PermissionLevelAuthorizer.Instance.CanRun(secToken, permissionLevelId, ConvoLogOperationType.Reply))
            {
                throw new DeveloperException(ErrorMessage.FromTemplate(ErrorMessageTemplate.PermissionDenied, "reply to", "comments on this conversation thread"));
            }

            this.adapter.Reply(secToken, parent, comment);
        }

        /// <summary>
        /// Hides an existing comment.
        /// </summary>
        /// <param name="securityToken">Security token for the calling principal.</param>
        /// <param name="commentId">An identifier of the comment to be hidden.</param>
        /// <returns>True iff it succeeded.</returns>
        public bool HideComment(SecurityToken securityToken, CommentReference commentId)
        {
            var permissionLevelId = this.GetPermissionLevelIdByCommentReference(securityToken, commentId);

            if (!PermissionLevelAuthorizer.Instance.CanRun(securityToken, permissionLevelId, ConvoLogOperationType.Hide))
            {
                throw new DeveloperException(ErrorMessage.FromTemplate(ErrorMessageTemplate.PermissionDenied, "hide", "comments on this conversation thread"));
            }

            return this.adapter.HideComment(securityToken, commentId);
        }

        /// <summary>
        /// Unhides an existing comment.
        /// </summary>
        /// <param name="securityToken">Security token for the calling principal.</param>
        /// <param name="commentId">An identifier of the comment to be unhidden.</param>
        /// <returns>True iff it succeeded.</returns>
        public bool ShowComment(SecurityToken securityToken, CommentReference commentId)
        {
            var permissionLevelId = this.GetPermissionLevelIdByCommentReference(securityToken, commentId);

            if (!PermissionLevelAuthorizer.Instance.CanRun(securityToken, permissionLevelId, ConvoLogOperationType.Hide))
            {
                throw new DeveloperException(ErrorMessage.FromTemplate(ErrorMessageTemplate.PermissionDenied, "show", "comments on this conversation thread"));
            }

            return this.adapter.ShowComment(securityToken, commentId);
        }

        /// <summary>
        /// Gets the permission level id for the specified comment.
        /// </summary>
        /// <param name="securityToken">Security token for the calling principal.</param>
        /// <param name="commentReference">The reference to the comment we want the permission level of.</param>
        /// <returns>The permission level id of the conversation that the comment belongs to, or null if it hasn't been set to the default by migration.</returns>
        private long? GetPermissionLevelIdByCommentReference(SecurityToken securityToken, CommentReference commentReference)
        {
            if (commentReference == null)
            {
                throw new ArgumentNullException(nameof(commentReference));
            }

            return this.adapter.GetPermissionLevelIdByCommentReference(securityToken, commentReference.Id);
        }

        /// <summary>
        /// Strips the text from the hidden comments in the conversation.
        /// </summary>
        /// <param name="conversation">The conversation we'd like to have the comments.</param>
        private void HideHiddenCommentsForConvesation(Conversation conversation)
        {
            if (conversation == null)
            {
                throw new ArgumentNullException(nameof(conversation));
            }

            foreach (var comment in conversation.Comments)
            {
                if (comment.IsHidden)
                {
                    comment.Value = CommentLogEntry.EmptyComment;
                }
            }
        }
    }
}
