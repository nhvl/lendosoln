﻿namespace LendersOffice.Drivers.ConversationLog
{
    using System;
    using System.Collections.Generic;
    using LqbGrammar.Drivers.ConversationLog;

    /// <summary>
    /// Factory class for creating mock drivers for the conversation log feature.
    /// </summary>
    public class MockConversationLogDriverFactory : ICommentLogDriverFactory
    {
        /// <summary>
        /// List of categories.
        /// </summary>
        private List<Category> categories;

        /// <summary>
        /// List of conversations.
        /// </summary>
        private List<Conversation> conversations; // NOTE: assumed that Comments is List<Comment>.

        /// <summary>
        /// Initializes a new instance of the <see cref="MockConversationLogDriverFactory"/> class.
        /// </summary>
        /// <param name="cats">The list of categories.</param>
        /// <param name="testData">The list of convesations.</param>
        public MockConversationLogDriverFactory(List<Category> cats, List<Conversation> testData)
        {
            this.categories = cats;
            this.conversations = testData;
        }

        /// <summary>
        /// Create an implementation of the category management interface.
        /// </summary>
        /// <param name="location">The parameter is not used.</param>
        /// <param name="port">The parameter is not used.</param>
        /// <returns>An implementation of the category management interface.</returns>
        public ICategoryDriver CreateCategoryDriver(string location, int port)
        {
            return new MockConversationLogDriver(this.categories, this.conversations);
        }

        /// <summary>
        /// Create an implementation of the comment management interface.
        /// </summary>
        /// <param name="location">The parameter is not used.</param>
        /// <param name="port">The parameter is not used.</param>
        /// <returns>An implementation of the comment management interface.</returns>
        public ICommentDriver CreateCommentDriver(string location, int port)
        {
            return new MockConversationLogDriver(this.categories, this.conversations);
        }
    }
}
