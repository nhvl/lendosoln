﻿namespace LendersOffice.Drivers.ConversationLog
{
    using System;
    using LqbGrammar.Drivers.ConversationLog;

    /// <summary>
    /// Factory for creating drivers for the conversation log system.
    /// </summary>
    public class ConversationLogDriverFactory : ICommentLogDriverFactory
    {
        /// <summary>
        /// Create an implementation of the category management interface.
        /// </summary>
        /// <param name="location">The location of the service.</param>
        /// <param name="port">The port on which the service is hosted.</param>
        /// <returns>An implementation of the category management interface.</returns>
        public ICategoryDriver CreateCategoryDriver(string location, int port)
        {
            return new CategoryDriver(location, port);
        }

        /// <summary>
        /// Create an implementation of the comment management interface.
        /// </summary>
        /// <param name="location">The location of the service.</param>
        /// <param name="port">The port on which the service is hosted.</param>
        /// <returns>An implementation of the comment management interface.</returns>
        public ICommentDriver CreateCommentDriver(string location, int port)
        {
            return new CommentDriver(location, port);
        }
    }
}
