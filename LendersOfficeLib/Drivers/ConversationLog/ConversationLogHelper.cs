﻿namespace LendersOffice.Drivers.ConversationLog
{
    using System.Collections.Generic;
    using LqbGrammar;
    using LqbGrammar.Adapters;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers.ConversationLog;

    /// <summary>
    /// Utility for interacting with the conversation log drivers.
    /// </summary>
    public static class ConversationLogHelper
    {
        /// <summary>
        /// Retrieve all the categories relevant to the logged in user, in the current order.
        /// </summary>
        /// <param name="secToken">Security token for the authenticated principal making this method call.</param>
        /// <param name="location">The location of the service.</param>
        /// <param name="port">The port on which the service is hosted.</param>
        /// <returns>The list of categories.</returns>
        public static List<Category> GetAllCategories(SecurityToken secToken, string location, int port)
        {
            var driver = GetCategoryDriver(location, port);
            return driver.GetAllCategories(secToken);
        }

        /// <summary>
        /// Either create a new category or modify and existing category.
        /// </summary>
        /// <param name="secToken">Security token for the authenticated principal making this method call.</param>
        /// <param name="category">A category in the comment log system.</param>
        /// <param name="location">The location of the service.</param>
        /// <param name="port">The port on which the service is hosted.</param>
        /// <returns>The current state of the category in the log system after the method has completed.</returns>
        public static Category SetCategory(SecurityToken secToken, Category category, string location, int port)
        {
            var driver = GetCategoryDriver(location, port);
            return driver.SetCategory(secToken, category);
        }

        /// <summary>
        /// Set the order in which categories are to be presented to users of the comment system.
        /// </summary>
        /// <param name="secToken">Security token for the authenticated principal making this method call.</param>
        /// <param name="categoryOrder">An ordered list of category references, the order will be reflected in subsequent GUI views.</param>
        /// <param name="location">The location of the service.</param>
        /// <param name="port">The port on which the service is hosted.</param>
        public static void SetCategoryOrder(SecurityToken secToken, List<CategoryReference> categoryOrder, string location, int port)
        {
            var driver = GetCategoryDriver(location, port);
            driver.SetCategoryOrder(secToken, categoryOrder);
        }

        /// <summary>
        /// Retrieve all conversations that have been attached to an LQB resource.
        /// </summary>
        /// <param name="secToken">Security token for the authenticated principal making this method call.</param>
        /// <param name="resource">The LQB resource.</param>
        /// <param name="location">The location of the service.</param>
        /// <param name="port">The port on which the service is hosted.</param>
        /// <returns>All conversations.  The order is determined by the category order, followed by the creation dates of the conversations.</returns>
        public static IEnumerable<Conversation> GetAllConversations(SecurityToken secToken, ResourceId resource, string location, int port)
        {
            var driver = GetCommentDriver(location, port);
            return driver.GetAllConversations(secToken, resource);
        }

        /// <summary>
        /// Post a new comment to a category, beginning a new conversation.
        /// </summary>
        /// <param name="secToken">Security token for the authenticated principal making this method call.</param>
        /// <param name="resource">The LQB resource.</param>
        /// <param name="category">The category under which this comment is posted.</param>
        /// <param name="comment">The comment data.</param>
        /// <param name="permissionLevelId">The id of the permission level associated with the conversation, or null if not yet migrated.</param>
        /// <param name="location">The location of the service.</param>
        /// <param name="port">The port on which the service is hosted.</param>
        /// <param name="bypassPermissionCheck">Bit to bypass the permission check.</param>
        public static void Post(SecurityToken secToken, ResourceId resource, CategoryReference category, Comment comment, long? permissionLevelId, string location, int port, bool bypassPermissionCheck = false)
        {
            var driver = GetCommentDriver(location, port);
            driver.Post(secToken, resource, category, comment, permissionLevelId, bypassPermissionCheck);
        }

        /// <summary>
        /// Reply to an existing comment in the conversation log system.
        /// </summary>
        /// <param name="secToken">Security token for the authenticated principal making this method call.</param>
        /// <param name="parent">The comment toward which this reply is directed.</param>
        /// <param name="comment">The comment data that is the reply.</param>
        /// <param name="location">The location of the service.</param>
        /// <param name="port">The port on which the service is hosted.</param>
        public static void Reply(SecurityToken secToken, CommentReference parent, Comment comment, string location, int port)
        {
            var driver = GetCommentDriver(location, port);
            driver.Reply(secToken, parent, comment);
        }

        /// <summary>
        /// Hides an existing comment.
        /// </summary>
        /// <param name="securityToken">Security token for the calling principal.</param>
        /// <param name="commentId">An identifier of the comment to be hidden.</param>
        /// <param name="location">The location of the service.</param>
        /// <param name="port">The port on which the service is hosted.</param>
        /// <returns>True iff it succeeded.</returns>
        public static bool HideComment(SecurityToken securityToken, CommentReference commentId, string location, int port)
        {
            var driver = GetCommentDriver(location, port);
            return driver.HideComment(securityToken, commentId);
        }

        /// <summary>
        /// Unhides an existing comment.
        /// </summary>
        /// <param name="securityToken">Security token for the calling principal.</param>
        /// <param name="commentId">An identifier of the comment to be unhidden.</param>
        /// <param name="location">The location of the service.</param>
        /// <param name="port">The port on which the service is hosted.</param>
        /// <returns>True iff it succeeded.</returns>
        public static bool ShowComment(SecurityToken securityToken, CommentReference commentId, string location, int port)
        {
            var driver = GetCommentDriver(location, port);
            return driver.ShowComment(securityToken, commentId);
        }

        /// <summary>
        /// Create a category driver.
        /// </summary>
        /// <param name="location">The location of the service.</param>
        /// <param name="port">The port on which the service is hosted.</param>
        /// <returns>A category driver.</returns>
        private static ICategoryDriver GetCategoryDriver(string location, int port)
        {
            var factory = GenericLocator<ICommentLogDriverFactory>.Factory;
            return factory.CreateCategoryDriver(location, port);
        }

        /// <summary>
        /// Create a comment driver.
        /// </summary>
        /// <param name="location">The location of the service.</param>
        /// <param name="port">The port on which the service is hosted.</param>
        /// <returns>A comment driver.</returns>
        private static ICommentDriver GetCommentDriver(string location, int port)
        {
            var factory = GenericLocator<ICommentLogDriverFactory>.Factory;
            return factory.CreateCommentDriver(location, port);
        }
    }
}
