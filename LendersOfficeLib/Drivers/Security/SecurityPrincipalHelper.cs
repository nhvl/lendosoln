﻿namespace LendersOffice.Drivers.Security
{
    using LqbGrammar;
    using LqbGrammar.Drivers;

    /// <summary>
    /// Utility for interacting with the security principal driver.
    /// </summary>
    public static class SecurityPrincipalHelper
    {
        /// <summary>
        /// Recover a principal and register it as the current thread's principal.
        /// </summary>
        /// <param name="serialized">Serialization of a principal.</param>
        public static void RecoverPrincipal(string serialized)
        {
            var driver = GetDriver();
            driver.RecoverPrincipal(serialized);
        }

        /// <summary>
        /// Retrieve a serialization of the current thread's registered principal.
        /// </summary>
        /// <returns>The serialization of the principal.</returns>
        public static string SerializePrincipal()
        {
            var driver = GetDriver();
            return driver.SerializePrincipal();
        }

        /// <summary>
        /// Retrieve the driver.
        /// </summary>
        /// <returns>The driver.</returns>
        private static ISecurityPrincipalDriver GetDriver()
        {
            var factory = GenericLocator<ISecurityPrincipalDriverFactory>.Factory;
            return factory.Create();
        }
    }
}
