﻿namespace LendersOffice.Drivers.Security
{
    using LqbGrammar.Drivers;

    /// <summary>
    /// Driver for working with a thread's registered security principal.  This skips the normal pattern
    /// of using an adapter since we are working with an LQB security principal, which is not available
    /// at the adapter level.
    /// </summary>
    internal sealed class SecurityPrincipalDriver : ISecurityPrincipalDriver
    {
        /// <summary>
        /// Recover a principal and register it as the current thread's principal.
        /// </summary>
        /// <param name="serialized">Serialization of a principal.</param>
        public void RecoverPrincipal(string serialized)
        {
            var principal = LendersOffice.Security.PrincipalFactory.RetrieveFromSimpleSerialized(serialized);
            if (principal != null)
            {
                System.Threading.Thread.CurrentPrincipal = principal;
            }
        }

        /// <summary>
        /// Retrieve a serialization of the current thread's registered principal.
        /// </summary>
        /// <returns>The serialization of the principal.</returns>
        public string SerializePrincipal()
        {
            return LendersOffice.Security.PrincipalFactory.GetSimpleSerializedString();
        }
    }
}
