﻿namespace LendersOffice.Drivers.Security
{
    using System;
    using LqbGrammar.Drivers;

    /// <summary>
    /// Factory for creating instances of the SecurityPrincipalDriver class.
    /// </summary>
    public sealed class SecurityPrincipalDriverFactory : ISecurityPrincipalDriverFactory
    {
        /// <summary>
        /// Create an implementation of the ISecurityPrincipalDriver interface.
        /// </summary>
        /// <returns>An implementation of the ISecurityPrincipalDriver interface.</returns>
        public ISecurityPrincipalDriver Create()
        {
            return new SecurityPrincipalDriver();
        }
    }
}
