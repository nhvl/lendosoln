﻿namespace LendersOffice.Drivers.Configuration
{
    using System;
    using LqbGrammar;
    using LqbGrammar.Adapters;
    using LqbGrammar.Drivers;

    /// <summary>
    /// Factory for creating instances of the ApplicationSettingsDriver class.
    /// </summary>
    public sealed class ApplicationSettingsDriverFactory : IApplicationSettingsDriverFactory
    {
        /// <summary>
        /// To avoid unnecessary instances of a class that merely reads from an already
        /// static resource, keep a singleton instance instead.
        /// </summary>
        private static IApplicationSettingsDriver cachedDriver;

        /// <summary>
        /// Lock used for initialization of the cached driver.  We don't want to use a static constructor as a factory need to have trivial creation code.
        /// </summary>
        private static object initLock = new object();

        /// <summary>
        /// Create an instance of the ApplicationSettingsDriver class.
        /// </summary>
        /// <returns>An instance of the ApplicationSettingsDriver class.</returns>
        public IApplicationSettingsDriver Create()
        {
            if (cachedDriver == null)
            {
                lock (initLock)
                {
                    if (cachedDriver == null)
                    {
                        var adapterFactory = GenericLocator<IApplicationSettingsAdapterFactory>.Factory;
                        var adapter = adapterFactory.Create();

                        cachedDriver = new ApplicationSettingsDriver(adapter);
                    }
                }
            }

            return cachedDriver;
        }
    }
}
