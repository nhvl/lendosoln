﻿namespace LendersOffice.Drivers.Configuration
{
    using System;
    using System.Collections.Generic;
    using LqbGrammar;
    using LqbGrammar.Adapters;
    using LqbGrammar.Drivers;

    /// <summary>
    /// Simple implementation of the IApplicationSettingsDriver interface, deferring to the adapter.
    /// </summary>
    internal sealed class ApplicationSettingsDriver : IApplicationSettingsDriver
    {
        /// <summary>
        /// Holds a reference to the adapter.
        /// </summary>
        private IApplicationSettingsAdapter adapter;

        /// <summary>
        /// Initializes a new instance of the <see cref="ApplicationSettingsDriver"/> class.
        /// </summary>
        /// <param name="adapter">The adapter used by the driver.</param>
        public ApplicationSettingsDriver(IApplicationSettingsAdapter adapter)
        {
            this.adapter = adapter;
        }

        /// <summary>
        /// Retrieve the application settings.
        /// Note that the name and value of each setting is not encapsulated into a data type for
        /// verification.  We will defer that to a wrapper for the driver to simplify the implementation
        /// since we are dealing with a collection class rather than individual values.
        /// </summary>
        /// <returns>A dictionary with the name/value pairs from the application settings.</returns>
        public Dictionary<string, string> ReadAllSettings()
        {
            return this.adapter.ReadAllSettings();
        }
    }
}
