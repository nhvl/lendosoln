﻿namespace LendersOffice.Drivers.Configuration
{
    using System;
    using System.Collections.Generic;
    using LqbGrammar.Drivers;

    /// <summary>
    /// Mock implementation of the IApplicationSettingsDriver interface.
    /// </summary>
    internal sealed class MockApplicationSettingsDriver : IApplicationSettingsDriver
    {
        /// <summary>
        /// The dictionary of key/value pairs that are needed for some unit test.
        /// </summary>
        private Dictionary<string, string> dictionary;

        /// <summary>
        /// Initializes a new instance of the <see cref="MockApplicationSettingsDriver"/> class.
        /// </summary>
        /// <param name="dictionary">The dictionary that this class holds and delivers.</param>
        public MockApplicationSettingsDriver(Dictionary<string, string> dictionary)
        {
            this.dictionary = dictionary;
        }

        /// <summary>
        /// Return the dictionary.
        /// </summary>
        /// <returns>The dictionary.</returns>
        public Dictionary<string, string> ReadAllSettings()
        {
            return this.dictionary;
        }
    }
}
