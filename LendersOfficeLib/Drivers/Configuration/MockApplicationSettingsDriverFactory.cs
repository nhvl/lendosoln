﻿namespace LendersOffice.Drivers.Configuration
{
    using System;
    using System.Collections.Generic;
    using LqbGrammar.Drivers;

    /// <summary>
    /// Factory for creating instances of the MockApplicationSettingsDriver class.
    /// </summary>
    public sealed class MockApplicationSettingsDriverFactory : IApplicationSettingsDriverFactory
    {
        /// <summary>
        /// The dictionary of key/value pairs that are needed for some unit test.
        /// </summary>
        private Dictionary<string, string> dictionary;

        /// <summary>
        /// Initializes a new instance of the <see cref="MockApplicationSettingsDriverFactory"/> class.
        /// </summary>
        /// <param name="dictionary">The dictionary that this class holds and delivers.</param>
        public MockApplicationSettingsDriverFactory(Dictionary<string, string> dictionary)
        {
            this.dictionary = dictionary;
        }

        /// <summary>
        /// Create in instance of the MockApplicationSettingsDriver class.
        /// </summary>
        /// <returns>Interface used for retrieving application settings.</returns>
        public IApplicationSettingsDriver Create()
        {
            return new MockApplicationSettingsDriver(this.dictionary);
        }
    }
}
