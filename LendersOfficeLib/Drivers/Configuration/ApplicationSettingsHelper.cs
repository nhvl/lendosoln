﻿namespace LendersOffice.Drivers.Configuration
{
    using System.Collections.Generic;
    using LqbGrammar;
    using LqbGrammar.Drivers;

    /// <summary>
    /// Utility for interacting with the application settings driver.
    /// </summary>
    public static class ApplicationSettingsHelper
    {
        /// <summary>
        /// Retrieve the application settings.
        /// </summary>
        /// <returns>A dictionary with the name/value pairs from the application settings.</returns>
        public static Dictionary<string, string> ReadAllSettings()
        {
            var factory = GenericLocator<IApplicationSettingsDriverFactory>.Factory;
            var driver = factory.Create();
            return driver.ReadAllSettings();
        }
    }
}
