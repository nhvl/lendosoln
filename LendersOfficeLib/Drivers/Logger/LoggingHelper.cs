﻿namespace LendersOffice.Drivers.Logger
{
    using System;
    using System.Collections.Generic;
    using Adapter.Logger;
    using LendersOffice.Logging;
    using LqbGrammar;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Utility class that hides the details of the FOOL architecture from client code.
    /// </summary>
    public static class LoggingHelper
    {
        /// <summary>
        /// Create the data structure for email logging.
        /// </summary>
        /// <param name="server">The email server to use when sending the email.</param>
        /// <param name="portNumber">The email server's port number.</param>
        /// <param name="emailFrom">The address from which the email is sent.</param>
        /// <param name="emailTo">List of addresses to which the email is sent.</param>
        /// <returns>The data structure for email logging.</returns>
        public static LoggingTargetInfo CreateEmailInfo(EmailServerName server, PortNumber portNumber, EmailAddress emailFrom, List<EmailAddress> emailTo)
        {
            return LoggingTargetInfoFactory.CreateEmailInfo(server, portNumber, emailFrom, emailTo);
        }

        /// <summary>
        /// Create the data structure for msmq logging.
        /// </summary>
        /// <param name="path">The path to the message queue.</param>
        /// <returns>The data structure for msmq logging.</returns>
        public static LoggingTargetInfo CreateMsmqInfo(MessageQueuePath path)
        {
            return LoggingTargetInfoFactory.CreateMsmqInfo(path);
        }

        /// <summary>
        /// Log a message to the logging system.
        /// </summary>
        /// <param name="level">The severity of the log message.</param>
        /// <param name="message">The message to be logged.</param>
        /// <param name="properties">Optional set of name/value pairs to include with the log message.</param>
        /// <param name="targets">The data to initialize the loggers.</param>
        public static void Log(LoggingLevel level, LogMessage message, Dictionary<LogPropertyName, LogPropertyValue> properties, params LoggingTargetInfo[] targets)
        {
            var factory = GenericLocator<ILoggingDriverFactory>.Factory;
            var logger = factory.Create(targets);

            switch (level)
            {
                case LoggingLevel.Debug:
                    logger.LogDebug(message, typeof(LoggingHelper), properties);
                    break;

                case LoggingLevel.Error:
                    logger.LogError(message, typeof(LoggingHelper), properties);
                    break;

                case LoggingLevel.Info:
                    logger.LogInfo(message, typeof(LoggingHelper), properties);
                    break;

                case LoggingLevel.Trace:
                    logger.LogTrace(message, typeof(LoggingHelper), properties);
                    break;

                case LoggingLevel.Warn:
                    logger.LogWarning(message, typeof(LoggingHelper), properties);
                    break;

                default:
                    throw new DeveloperException(ErrorMessage.SystemError);
            }
        }
    }
}
