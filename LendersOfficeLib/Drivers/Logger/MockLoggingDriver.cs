﻿namespace LendersOffice.Drivers.Logger
{
    using System;
    using System.Collections.Generic;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;

    /// <summary>
    /// Mock logger that stores the logged information in the factory.
    /// </summary>
    internal sealed class MockLoggingDriver : ILoggingDriver
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MockLoggingDriver"/> class.
        /// </summary>
        /// <param name="factory">The factory that created this instance and stores the method call data.</param>
        public MockLoggingDriver(MockLoggingDriverFactory factory)
        {
            this.Factory = factory;
        }

        /// <summary>
        /// Gets or sets the factory where the log data will be stored.
        /// </summary>
        /// <value>The factory where the log data will be stored.</value>
        private MockLoggingDriverFactory Factory { get; set; }

        /// <summary>
        /// Store bug data in the factory for post-call inspection by unit tests.
        /// </summary>
        /// <param name="message">The message to be logged.</param>
        /// <param name="boundaryType">The type that holds the method that client code calls.</param>
        /// <param name="properties">Optional set of name/value pairs to include with the log message.</param>
        public void LogBug(LogMessage message, Type boundaryType, Dictionary<LogPropertyName, LogPropertyValue> properties)
        {
            this.StoreData("Bug", message, boundaryType, properties);
        }

        /// <summary>
        /// Store debug data in the factory for post-call inspection by unit tests.
        /// </summary>
        /// <param name="message">The message to be logged.</param>
        /// <param name="boundaryType">The type that holds the method that client code calls.</param>
        /// <param name="properties">Optional set of name/value pairs to include with the log message.</param>
        public void LogDebug(LogMessage message, Type boundaryType, Dictionary<LogPropertyName, LogPropertyValue> properties)
        {
            this.StoreData("Debug", message, boundaryType, properties);
        }

        /// <summary>
        /// Store error data in the factory for post-call inspection by unit tests.
        /// </summary>
        /// <param name="message">The message to be logged.</param>
        /// <param name="boundaryType">The type that holds the method that client code calls.</param>
        /// <param name="properties">Optional set of name/value pairs to include with the log message.</param>
        public void LogError(LogMessage message, Type boundaryType, Dictionary<LogPropertyName, LogPropertyValue> properties)
        {
            this.StoreData("Error", message, boundaryType, properties);
        }

        /// <summary>
        /// Log an error message using the input stack trace rather than pulling it from the call stack.
        /// </summary>
        /// <param name="message">The message to log.</param>
        /// <param name="stackTrace">Stack trace to use in the log.</param>
        /// <param name="properties">Optional set of properties that will be included in the log.</param>
        public void LogError(LogMessage message, string stackTrace, Dictionary<LogPropertyName, LogPropertyValue> properties)
        {
            this.StoreData("Error", message, null, properties);
        }

        /// <summary>
        /// Store information data in the factory for post-call inspection by unit tests.
        /// </summary>
        /// <param name="message">The message to be logged.</param>
        /// <param name="boundaryType">The type that holds the method that client code calls.</param>
        /// <param name="properties">Optional set of name/value pairs to include with the log message.</param>
        public void LogInfo(LogMessage message, Type boundaryType, Dictionary<LogPropertyName, LogPropertyValue> properties)
        {
            this.StoreData("Info", message, boundaryType, properties);
        }

        /// <summary>
        /// Store trace data in the factory for post-call inspection by unit tests.
        /// </summary>
        /// <param name="message">The message to be logged.</param>
        /// <param name="boundaryType">The type that holds the method that client code calls.</param>
        /// <param name="properties">Optional set of name/value pairs to include with the log message.</param>
        public void LogTrace(LogMessage message, Type boundaryType, Dictionary<LogPropertyName, LogPropertyValue> properties)
        {
            this.StoreData("Trace", message, boundaryType, properties);
        }

        /// <summary>
        /// Store warning data in the factory for post-call inspection by unit tests.
        /// </summary>
        /// <param name="message">The message to be logged.</param>
        /// <param name="boundaryType">The type that holds the method that client code calls.</param>
        /// <param name="properties">Optional set of name/value pairs to include with the log message.</param>
        public void LogWarning(LogMessage message, Type boundaryType, Dictionary<LogPropertyName, LogPropertyValue> properties)
        {
            this.StoreData("Warn", message, boundaryType, properties);
        }

        /// <summary>
        /// Store message data in the factory for post-call inspection by unit tests.
        /// </summary>
        /// <param name="method">The message log type.</param>
        /// <param name="message">The message to be logged.</param>
        /// <param name="boundaryType">The type that holds the method that client code calls.</param>
        /// <param name="properties">Optional set of name/value pairs to include with the log message.</param>
        private void StoreData(string method, LogMessage message, Type boundaryType, Dictionary<LogPropertyName, LogPropertyValue> properties)
        {
            this.Factory.LogType = method;
            this.Factory.Message = message.ToString();

            if (boundaryType != null)
            {
                this.Factory.BoundaryType = boundaryType.FullName;
            }

            if (properties != null)
            {
                var storeProps = new Dictionary<string, string>();
                foreach (var key in properties.Keys)
                {
                    string storeKey = key.ToString();
                    string storeValue = properties[key].ToString();
                    storeProps[storeKey] = storeValue;
                }

                this.Factory.Properties = storeProps;
            }
        }
    }
}
