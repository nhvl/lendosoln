﻿namespace LendersOffice.Drivers.Logger
{
    using System;
    using System.Collections.Generic;
    using Constants;
    using LqbGrammar;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Driver that logs exceptions.
    /// </summary>
    public sealed class ExceptionLoggingDriver : IExceptionLoggingDriver
    {
        /// <summary>
        /// Log the information contained within the exception.
        /// </summary>
        /// <param name="ex">The exception to log.</param>
        /// <returns>True if the exception information was logged successfully, false otherwise.</returns>
        public bool LogException(LqbException ex)
        {
            var driver = this.GetDriver();
            if (driver != null)
            {
                var message = LogMessage.Create(ex.Message);
                if (message != null)
                {
                    var context = this.ReadContext(ex);
                    driver.LogError(message.Value, ex.StackTrace, context);
                }
            }

            return false;
        }

        /// <summary>
        /// Retrieve the driver used for logging exceptions.
        /// </summary>
        /// <returns>The driver.</returns>
        private ILoggingDriver GetDriver()
        {
            var factory = GenericLocator<ILoggingDriverFactory>.Factory;
            if (factory != null)
            {
                var path = MessageQueuePath.Create(ConstStage.MsMqLogConnectStr);
                if (path != null)
                {
                    var target = LoggingHelper.CreateMsmqInfo(path.Value);
                    return factory.Create(target);
                }
            }

            return null;
        }

        /// <summary>
        /// Parse the context information into a set of properties.
        /// </summary>
        /// <param name="ex">The exception containing context information.</param>
        /// <returns>The properties.</returns>
        private Dictionary<LogPropertyName, LogPropertyValue> ReadContext(LqbException ex)
        {
            string contextString = null;
            Dictionary<LogPropertyName, LogPropertyValue> contextData = null;
            if (ex.Context != null)
            {
                contextData = ex.Context.GetProperties();
                if (contextData == null)
                {
                    contextString = ex.Context.Serialize();
                }
            }

            if (contextData == null && !string.IsNullOrEmpty(contextString))
            {
                var propName = LogPropertyName.Create("LqbContext").Value;
                var propValue = LogPropertyValue.Create(contextString);
                if (propValue != null)
                {
                    contextData = new Dictionary<LogPropertyName, LogPropertyValue>();
                    contextData[propName] = propValue.Value;
                }
            }

            return contextData;
        }
    }
}
