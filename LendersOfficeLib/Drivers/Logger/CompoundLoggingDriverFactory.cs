﻿namespace LendersOffice.Drivers.Logger
{
    using Adapter.Logger;
    using LqbGrammar.Adapters;
    using LqbGrammar.Drivers;

    /// <summary>
    /// Factory for creating compound logging driver instances.
    /// </summary>
    public sealed class CompoundLoggingDriverFactory : ILoggingDriverFactory
    {
        /// <summary>
        /// Create an implementation of the ILoggingAdapter interface.
        /// </summary>
        /// <param name="targets">Set of logging targets to be used for a particular message.</param>
        /// <returns>An implementation of the ILoggingDriver interface.</returns>
        public ILoggingDriver Create(params LoggingTargetInfo[] targets)
        {
            var adapters = new ILoggingAdapter[targets.Length];
            for (int i = 0; i < targets.Length; ++i)
            {
                adapters[i] = LoggingTargetInfoFactory.GetAdapter(targets[i]);
            }

            return new CompoundLoggingDriver(adapters);
        }

        /// <summary>
        /// Create a driver for logging exceptions.
        /// </summary>
        /// <returns>A driver for logging exceptions.</returns>
        public IExceptionLoggingDriver CreateExceptionLogger()
        {
            return new ExceptionLoggingDriver();
        }
    }
}
