﻿namespace LendersOffice.Drivers.Logger
{
    using System.Collections.Generic;
    using LqbGrammar.Drivers;

    /// <summary>
    /// Factory for creating a mock logging driver.
    /// </summary>
    public sealed class MockLoggingDriverFactory : ILoggingDriverFactory
    {
        /// <summary>
        /// Gets the type of logging done.
        /// </summary>
        /// <value>The type of logging done.</value>
        public string LogType { get; internal set; }

        /// <summary>
        /// Gets the logged message.
        /// </summary>
        /// <value>The logged message.</value>
        public string Message { get; internal set; }

        /// <summary>
        /// Gets the Fullname property for the boundary class used to log the message.
        /// </summary>
        /// <value>The type string for the boundary class used to log the message.</value>
        public string BoundaryType { get; internal set; }

        /// <summary>
        /// Gets the properties included with the log message.
        /// </summary>
        /// <value>The properties included with the log message.</value>
        public Dictionary<string, string> Properties { get; internal set; }

        /// <summary>
        /// Create a mock logging driver.
        /// </summary>
        /// <param name="targets">The parameter is not used.</param>
        /// <returns>A mock logging driver.</returns>
        public ILoggingDriver Create(params LoggingTargetInfo[] targets)
        {
            return new MockLoggingDriver(this);
        }

        /// <summary>
        /// Create a driver for logging exceptions.
        /// </summary>
        /// <returns>A driver for logging exceptions.</returns>
        public IExceptionLoggingDriver CreateExceptionLogger()
        {
            return null;
        }
    }
}
