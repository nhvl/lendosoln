﻿namespace LendersOffice.Drivers.Logger
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using LqbGrammar.Adapters;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;

    /// <summary>
    /// Logging driver that dispatches calls to multiple adapters.
    /// </summary>
    internal sealed class CompoundLoggingDriver : ILoggingDriver
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CompoundLoggingDriver"/> class.
        /// </summary>
        /// <param name="adapters">Logging adapters used for the logging operation.</param>
        public CompoundLoggingDriver(ILoggingAdapter[] adapters)
        {
            this.Adapters = adapters;
        }

        /// <summary>
        /// Gets or sets the list of adapters.
        /// </summary>
        /// <value>The list of adapters.</value>
        private ILoggingAdapter[] Adapters { get; set; }

        /// <summary>
        /// Log a bug report.
        /// </summary>
        /// <param name="message">The message to log.</param>
        /// <param name="boundaryType">Type of class that is used as the starting point for logging messages.</param>
        /// <param name="properties">Optional set of properties that will be included in the log.</param>
        public void LogBug(LogMessage message, Type boundaryType, Dictionary<LogPropertyName, LogPropertyValue> properties)
        {
            Action<ILoggingAdapter> handler = delegate(ILoggingAdapter adapter)
            {
                adapter.LogBug(message, boundaryType, properties);
            };

            this.CallAdapters(handler);
        }

        /// <summary>
        /// Log debug information.
        /// </summary>
        /// <param name="message">The message to log.</param>
        /// <param name="boundaryType">Type of class that is used as the starting point for logging messages.</param>
        /// <param name="properties">Optional set of properties that will be included in the log.</param>
        public void LogDebug(LogMessage message, Type boundaryType, Dictionary<LogPropertyName, LogPropertyValue> properties)
        {
            Action<ILoggingAdapter> handler = delegate(ILoggingAdapter adapter)
            {
                adapter.LogDebug(message, boundaryType, properties);
            };

            this.CallAdapters(handler);
        }

        /// <summary>
        /// Log an error message.
        /// </summary>
        /// <param name="message">The message to log.</param>
        /// <param name="boundaryType">Type of class that is used as the starting point for logging messages.</param>
        /// <param name="properties">Optional set of properties that will be included in the log.</param>
        public void LogError(LogMessage message, Type boundaryType, Dictionary<LogPropertyName, LogPropertyValue> properties)
        {
            Action<ILoggingAdapter> handler = delegate(ILoggingAdapter adapter)
            {
                adapter.LogError(message, boundaryType, properties);
            };

            this.CallAdapters(handler);
        }

        /// <summary>
        /// Log an error message using the input stack trace rather than pulling it from the call stack.
        /// </summary>
        /// <param name="message">The message to log.</param>
        /// <param name="stackTrace">Stack trace to use in the log.</param>
        /// <param name="properties">Optional set of properties that will be included in the log.</param>
        public void LogError(LogMessage message, string stackTrace, Dictionary<LogPropertyName, LogPropertyValue> properties)
        {
            Action<ILoggingAdapter> handler = delegate(ILoggingAdapter adapter)
            {
                adapter.LogError(message, stackTrace, properties);
            };

            this.CallAdapters(handler);
        }

        /// <summary>
        /// Log an informational message.
        /// </summary>
        /// <param name="message">The message to log.</param>
        /// <param name="boundaryType">Type of class that is used as the starting point for logging messages.</param>
        /// <param name="properties">Optional set of properties that will be included in the log.</param>
        public void LogInfo(LogMessage message, Type boundaryType, Dictionary<LogPropertyName, LogPropertyValue> properties)
        {
            Action<ILoggingAdapter> handler = delegate(ILoggingAdapter adapter)
            {
                adapter.LogInfo(message, boundaryType, properties);
            };

            this.CallAdapters(handler);
        }

        /// <summary>
        /// Log a trace message.
        /// </summary>
        /// <param name="message">The message to log.</param>
        /// <param name="boundaryType">Type of class that is used as the starting point for logging messages.</param>
        /// <param name="properties">Optional set of properties that will be included in the log.</param>
        public void LogTrace(LogMessage message, Type boundaryType, Dictionary<LogPropertyName, LogPropertyValue> properties)
        {
            Action<ILoggingAdapter> handler = delegate(ILoggingAdapter adapter)
            {
                adapter.LogTrace(message, boundaryType, properties);
            };

            this.CallAdapters(handler);
        }

        /// <summary>
        /// Log a warning message.
        /// </summary>
        /// <param name="message">The message to log.</param>
        /// <param name="boundaryType">Type of class that is used as the starting point for logging messages.</param>
        /// <param name="properties">Optional set of properties that will be included in the log.</param>
        public void LogWarning(LogMessage message, Type boundaryType, Dictionary<LogPropertyName, LogPropertyValue> properties)
        {
            Action<ILoggingAdapter> handler = delegate(ILoggingAdapter adapter)
            {
                adapter.LogWarning(message, boundaryType, properties);
            };

            this.CallAdapters(handler);
        }

        /// <summary>
        /// Loop through each adapter and call an action handler with it.
        /// </summary>
        /// <param name="handler">An action handler.</param>
        private void CallAdapters(Action<ILoggingAdapter> handler)
        {
            foreach (var adapter in this.Adapters)
            {
                this.CallAdapter(handler, adapter);
            }
        }

        /// <summary>
        /// Call an action handler and ensure there are no exceptions thrown.
        /// </summary>
        /// <param name="handler">An action handler.</param>
        /// <param name="adapter">Adapter that is the argument to the action handler.</param>
        [SuppressMessage(category: "LendingQBStyleCop.LendingQBCustomRules", checkId: "LB1003:CannotSwallowGeneralException", Justification = "Logging errors shouldn't throw errors.")]
        private void CallAdapter(Action<ILoggingAdapter> handler, ILoggingAdapter adapter)
        {
            try
            {
                handler(adapter);
            }
            catch
            {
                // An error when writing to a logger shouldn't get out since code is likely to attempt logging that error.
                // We could try to log the information somewhere else, but there is a risk that will fail as well.  If someone wishes
                // to that, I will make a LastDitchLogger class that logs to the Windows Event Log as that is the most stable location.
            }
        }
    }
}
