﻿namespace LendersOffice.Drivers.Base64Encoding
{
    using LqbGrammar;
    using LqbGrammar.Adapters;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;

    /// <summary>
    /// Encodes and decodes base64.
    /// </summary>
    internal sealed class Base64EncodingDriver : IBase64EncodingDriver
    {
        /// <summary>
        /// Adapter to do the work.
        /// </summary>
        private IBase64EncodingAdapter adapter;

        /// <summary>
        /// Initializes a new instance of the <see cref="Base64EncodingDriver"/> class.
        /// </summary>
        public Base64EncodingDriver()
        {
            var factory = GenericLocator<IBase64EncodingAdapterFactory>.Factory;
            this.adapter = factory.Create();
        }

        /// <summary>
        /// Decode the value in base64.
        /// </summary>
        /// <param name="base64EncodedData">Base 64 encoded data.</param>
        /// <returns>The decoded value.</returns>
        public byte[] Decode(Base64EncodedData base64EncodedData)
        {
            return this.adapter.Decode(base64EncodedData);
        }

        /// <summary>
        /// Encode the value in base64.
        /// </summary>
        /// <param name="unencodedData">Unencoded data.</param>
        /// <returns>The base64 encoded value.</returns>
        public Base64EncodedData Encode(byte[] unencodedData)
        {
            return this.adapter.Encode(unencodedData);
        }
    }
}
