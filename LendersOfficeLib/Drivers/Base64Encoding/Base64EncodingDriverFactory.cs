﻿namespace LendersOffice.Drivers.Base64Encoding
{
    using LqbGrammar;
    using LqbGrammar.Drivers;

    /// <summary>
    /// Factory for creation of a Base64Encoding driver.
    /// </summary>
    public sealed class Base64EncodingDriverFactory : IBase64EncodingDriverFactory
    {
        /// <summary>
        /// Cached base64 encoding driver.  Since the adapter and driver only
        /// call static methods, we use a singleton.
        /// </summary>
        private static IBase64EncodingDriver base64EncodingDriver;

        /// <summary>
        /// Lock for initializing the cached driver.  Avoiding static constructor.
        /// </summary>
        private static object initLock = new object();

        /// <summary>
        /// Create an implementation of the IBase64EncodingDriver interface.
        /// </summary>
        /// <returns>An IBase64EncodingDriver instance.</returns>
        public IBase64EncodingDriver Create()
        {
            if (base64EncodingDriver == null)
            {
                lock (initLock)
                {
                    if (base64EncodingDriver == null)
                    {
                        base64EncodingDriver = new Base64EncodingDriver();
                    }
                }
            }

            return base64EncodingDriver;
        }
    }
}