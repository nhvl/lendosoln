﻿namespace LendersOffice.Drivers.Base64Encoding
{
    using LqbGrammar;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;

    /// <summary>
    /// Utility class for interacting with the Base64Encoding driver.
    /// </summary>
    public static class Base64EncodingDriverHelper
    {
        /// <summary>
        /// Decode the value in base64.
        /// </summary>
        /// <param name="base64EncodedData">The encoded data.</param>
        /// <returns>The decoded value.</returns>
        public static byte[] Decode(Base64EncodedData base64EncodedData)
        {
            var driver = GetDriver();
            return driver.Decode(base64EncodedData);
        }

        /// <summary>
        /// Encode the value in base64.
        /// </summary>
        /// <param name="unencodedData">The data to encode.</param>
        /// <returns>The base64 encoded value.</returns>
        public static Base64EncodedData Encode(byte[] unencodedData)
        {
            var driver = GetDriver();
            return driver.Encode(unencodedData);
        }

        /// <summary>
        /// Retrieve the driver from the service locator.
        /// </summary>
        /// <returns>The base64 encoding driver.</returns>
        private static IBase64EncodingDriver GetDriver()
        {
            var factory = GenericLocator<IBase64EncodingDriverFactory>.Factory;
            return factory.Create();
        }
    }
}
