﻿namespace LendersOffice.Drivers.Encryption
{
    using LqbGrammar.Drivers;

    /// <summary>
    /// Factory for the caching encryption key driver.
    /// </summary>
    public sealed class CachingKeyDriverDecoratorFactory : IEncryptionKeyDriverFactory
    {
        /// <summary>
        /// Creates a new encryption key driver.
        /// </summary>
        /// <returns>A new encryption key driver.</returns>
        public IEncryptionKeyDriver Create()
        {
            var factory = new EncryptionKeyDriverFactory();
            IEncryptionKeyDriver baseDriver = factory.Create();
            return new CachingKeyDriverDecorator(baseDriver);
        }
    }
}
