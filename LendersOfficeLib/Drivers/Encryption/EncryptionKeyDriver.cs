﻿namespace LendersOffice.Drivers.Encryption
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using Constants;
    using DataAccess;
    using LqbGrammar;
    using LqbGrammar.Adapters;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;
    using LqbGrammar.Exceptions;
    using SqlServerDB;

    /// <summary>
    /// Provides the functionality needed to access and generate keys.
    /// </summary>
    public class EncryptionKeyDriver : IEncryptionKeyDriver
    {
        /// <summary>
        /// The encryption adapter factory. This class exposes the functionality required to make keys.
        /// </summary>
        private IDataEncryptionAdapterFactory encryptionAdapterFactory;

        /// <summary>
        /// Initializes a new instance of the <see cref="EncryptionKeyDriver"/> class.
        /// </summary>
        public EncryptionKeyDriver()
        {
            this.encryptionAdapterFactory = GenericLocator<IDataEncryptionAdapterFactory>.Factory;
        }

        /// <summary>
        /// Generates a new encryption key and returns the key identifier and
        /// the key.
        /// </summary>
        /// <returns>The new key identifier and the key.</returns>
        public KeyValuePair<EncryptionKeyIdentifier, EncryptionKey> GenerateKey()
        {
            var identifier = EncryptionKeyIdentifier.Create(Guid.NewGuid().ToString()).Value;
            EncryptionKey key = this.encryptionAdapterFactory.GenerateKey();
            this.WriteKeyToDatabase(identifier, key);
            return new KeyValuePair<EncryptionKeyIdentifier, EncryptionKey>(identifier, key);
        }

        /// <summary>
        /// Gets an encryption key by its identifier.
        /// </summary>
        /// <param name="identifier">The identifier.</param>
        /// <returns>The key associated with the identifier.</returns>
        public EncryptionKey GetKey(EncryptionKeyIdentifier identifier)
        {
            if (identifier == EncryptionKeyIdentifier.DbConnectionString)
            {
                var factory = new Adapter.Drivers.DbConnectionOnlyEncryptionKeyDriver.Factory();
                return factory.Create().GetKey(identifier);
            }
            else if (identifier == EncryptionKeyIdentifier.ThirdParties)
            {
                return this.GetThirdPartiesKey();
            }
            else if (identifier == EncryptionKeyIdentifier.MeritMatrix)
            {
                return this.GetMeritMatrixKey();
            }

            return this.ReadKeyFromDatabase(identifier);
        }

        /// <summary>
        /// Gets the key for third parties.
        /// </summary>
        /// <returns>The key for third parties.</returns>
        private EncryptionKey GetThirdPartiesKey()
        {
            // This will always be the code for third parties.
            byte[] key = Convert.FromBase64String(ConstStage.ThirdPartyEncryptionKey);
            byte[] iv = Convert.FromBase64String(ConstStage.ThirdPartyEncryptionIV);

            return this.encryptionAdapterFactory.CreateAesKey(201701, key, iv, 256); // using a pseudo-year for the longer iv length
        }

        /// <summary>
        /// Gets the key for Merit Matrix.
        /// </summary>
        /// <returns>The key for Merit Matrix.</returns>
        private EncryptionKey GetMeritMatrixKey()
        {
            // This will always be the code for merit matrix.
            string start = DateTime.Now.Year.ToString() + "Fantasy Island 12345"; // length = 24
            byte[] key = System.Text.ASCIIEncoding.ASCII.GetBytes(start);

            byte[] iv = new byte[16];
            Array.Copy(key, 0, iv, 0, 16);

            return this.encryptionAdapterFactory.CreateAesKey(201702, key, iv, null); // using pseudo-year for the shorter key length
        }

        /// <summary>
        /// Reads an encryption key from the database.
        /// </summary>
        /// <param name="identifier">The encryption key identifier.</param>
        /// <returns>The encryption key.</returns>
        private EncryptionKey ReadKeyFromDatabase(EncryptionKeyIdentifier identifier)
        {
            var sproc = StoredProcedureName.Create("ENCRYPTION_KEYS_RetrieveByIdentifier").Value;
            var parameters = new[] 
            {
                new SqlParameter("Identifier", identifier.ToString())
            };

            using (DbConnection connection = DbAccessUtils.GetConnection(DataSrc.LQBSecure))
            using (IDataReader reader = StoredProcedureDriverHelper.ExecuteReader(connection, null, sproc, parameters, TimeoutInSeconds.Default))
            {
                if (reader.Read())
                {
                    var version = (int)reader["Version"];
                    var key = (byte[])reader["Field1"];
                    var iv = (byte[])reader["Field2"];

                    return this.encryptionAdapterFactory.CreateAesKey(version, key, iv, blockSize: null);
                }
            }

            throw new DeveloperException(ErrorMessage.SystemError);
        }

        /// <summary>
        /// Writes an encryption key to the database.
        /// </summary>
        /// <param name="identifier">The encryption key identifier.</param>
        /// <param name="key">The encryption key.</param>
        private void WriteKeyToDatabase(EncryptionKeyIdentifier identifier, EncryptionKey key)
        {
            Guid id = Guid.Parse(identifier.ToString());
            EncryptionKeyBackingFields backingFields = key.GetBackingFields();

            var sproc = StoredProcedureName.Create("ENCRYPTION_KEYS_Insert").Value;
            var parameters = new[]
            {
                new SqlParameter("Identifier", id),
                new SqlParameter("Description", string.Empty),
                new SqlParameter("Version", backingFields.Version),
                new SqlParameter("Field1", backingFields.Field1),
                new SqlParameter("Field2", backingFields.Field2),
            };

            using (DbConnection connection = DbAccessUtils.GetConnection(DataSrc.LQBSecure))
            {
                StoredProcedureDriverHelper.ExecuteNonQuery(connection, null, sproc, parameters, TimeoutInSeconds.Default);
            }
        }
    }
}
