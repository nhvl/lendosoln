﻿namespace LendersOffice.Drivers.Encryption
{
    using System;
    using LqbGrammar.Drivers;

    /// <summary>
    /// Factory for creation of an encryption driver.
    /// </summary>
    public sealed class EncryptionDriverFactory : IEncryptionDriverFactory
    {
        /// <summary>
        /// Create an implementation of the IEncryptionDriver interface.
        /// </summary>
        /// <returns>An implementation of the IEncryptionDriver interface.</returns>
        public IEncryptionDriver Create()
        {
            return new EncryptionDriver();
        }
    }
}
