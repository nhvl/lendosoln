﻿namespace LendersOffice.Drivers.Encryption
{
    using System.Collections.Generic;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;

    /// <summary>
    /// Base encryption key driver decorator.
    /// </summary>
    public class EncryptionKeyDriverDecorator : IEncryptionKeyDriver
    {
        /// <summary>
        /// The decorated driver.
        /// </summary>
        private IEncryptionKeyDriver wrapped;

        /// <summary>
        /// Initializes a new instance of the <see cref="EncryptionKeyDriverDecorator"/> class.
        /// </summary>
        /// <param name="wrapped">The driver to decorate.</param>
        public EncryptionKeyDriverDecorator(IEncryptionKeyDriver wrapped)
        {
            this.wrapped = wrapped;
        }

        /// <summary>
        /// Generates a new encryption key and returns the key identifier and
        /// the key.
        /// </summary>
        /// <returns>The new key identifier and the key.</returns>
        public virtual KeyValuePair<EncryptionKeyIdentifier, EncryptionKey> GenerateKey()
        {
            return this.wrapped.GenerateKey();
        }

        /// <summary>
        /// Gets an encryption key by its identifier.
        /// </summary>
        /// <param name="identifier">The identifier.</param>
        /// <returns>The key associated with the identifier.</returns>
        public virtual EncryptionKey GetKey(EncryptionKeyIdentifier identifier)
        {
            return this.wrapped.GetKey(identifier);
        }
    }
}
