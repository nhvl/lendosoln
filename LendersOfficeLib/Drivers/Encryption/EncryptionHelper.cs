﻿namespace LendersOffice.Drivers.Encryption
{
    using LqbGrammar;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;

    /// <summary>
    /// Utility for interacting with the encryption driver.
    /// </summary>
    public static class EncryptionHelper
    {
        /// <summary>
        /// High-level decryption.
        /// </summary>
        /// <param name="keyId">The identifier for the key to use.</param>
        /// <param name="encrypted">Data to decrypt.</param>
        /// <returns>The decrypted string.</returns>
        public static string DecodeAndDecrypt(EncryptionKeyIdentifier keyId, string encrypted)
        {
            var driver = GetDriver();
            return driver.DecodeAndDecrypt(keyId, encrypted);
        }

        /// <summary>
        /// Low-level decryption.
        /// </summary>
        /// <param name="keyId">The identifier for the key to use.</param>
        /// <param name="encrypted">Data to decrypt.</param>
        /// <returns>The decrypted data.</returns>
        public static byte[] DecryptBytes(EncryptionKeyIdentifier keyId, byte[] encrypted)
        {
            var driver = GetDriver();
            return driver.DecryptBytes(keyId, encrypted);
        }

        /// <summary>
        /// Mid-level decryption.
        /// </summary>
        /// <param name="keyId">The identifier for the key to use.</param>
        /// <param name="encrypted">Data to decrypt.</param>
        /// <returns>The decrypted string.</returns>
        public static string DecryptString(EncryptionKeyIdentifier keyId, byte[] encrypted)
        {
            var driver = GetDriver();
            return driver.DecryptString(keyId, encrypted);
        }

        /// <summary>
        /// High-level encryption.
        /// </summary>
        /// <param name="keyId">The identifier for the key to use.</param>
        /// <param name="inputData">String to encrypt.</param>
        /// <returns>The encrypted string base64 encoded.</returns>
        public static string EncryptAndEncode(EncryptionKeyIdentifier keyId, string inputData)
        {
            var driver = GetDriver();
            return driver.EncryptAndEncode(keyId, inputData);
        }

        /// <summary>
        /// Low-level encryption.
        /// </summary>
        /// <param name="keyId">The identifier for the key to use.</param>
        /// <param name="inputData">Data to encrypt.</param>
        /// <returns>The encrypted data.</returns>
        public static byte[] EncryptBytes(EncryptionKeyIdentifier keyId, byte[] inputData)
        {
            var driver = GetDriver();
            return driver.EncryptBytes(keyId, inputData);
        }

        /// <summary>
        /// Mid-level encryption.
        /// </summary>
        /// <param name="keyId">The identifier for the key to use.</param>
        /// <param name="inputData">String to encrypt.</param>
        /// <returns>The encrypted string.</returns>
        public static byte[] EncryptString(EncryptionKeyIdentifier keyId, string inputData)
        {
            var driver = GetDriver();
            return driver.EncryptString(keyId, inputData);
        }

        /// <summary>
        /// Generates a new encryption key and returns the key identifier.
        /// </summary>
        /// <returns>The identifier to the key.</returns>
        public static EncryptionKeyIdentifier GenerateNewKey()
        {
            return GenericLocator<IEncryptionKeyDriverFactory>.Factory.Create().GenerateKey().Key;
        }

        /// <summary>
        /// Retrieve the driver.
        /// </summary>
        /// <returns>The driver.</returns>
        private static IEncryptionDriver GetDriver()
        {
            var factory = GenericLocator<IEncryptionDriverFactory>.Factory;
            return factory.Create();
        }
    }
}
