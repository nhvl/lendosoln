﻿namespace LendersOffice.Drivers.Encryption
{
    using LqbGrammar.Drivers;

    /// <summary>
    /// Factory for encryption key drivers.
    /// </summary>
    public sealed class EncryptionKeyDriverFactory : IEncryptionKeyDriverFactory
    {
        /// <summary>
        /// Creates a new encryption key driver.
        /// </summary>
        /// <returns>A new encryption key driver.</returns>
        public IEncryptionKeyDriver Create()
        {
            return new EncryptionKeyDriver();
        }
    }
}
