﻿namespace LendersOffice.Drivers.Encryption
{
    using System;
    using System.Linq;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;

    /// <summary>
    /// Simple implementation of the IEncryptionDriver interface.
    /// </summary>
    internal sealed class EncryptionDriver : IEncryptionDriver
    {
        /// <summary>
        /// High-level decryption.
        /// </summary>
        /// <param name="keyId">The identifier for the key to use.</param>
        /// <param name="encrypted">Data to decrypt.</param>
        /// <returns>The decrypted string.</returns>
        public string DecodeAndDecrypt(EncryptionKeyIdentifier keyId, string encrypted)
        {
            if (this.StringInputBad(encrypted))
            {
                return null;
            }

            var decoded = Convert.FromBase64String(encrypted);
            return this.DecryptString(keyId, decoded);
        }

        /// <summary>
        /// Low-level decryption.
        /// </summary>
        /// <param name="keyId">The identifier for the key to use.</param>
        /// <param name="encrypted">Data to decrypt.</param>
        /// <returns>The decrypted data.</returns>
        public byte[] DecryptBytes(EncryptionKeyIdentifier keyId, byte[] encrypted)
        {
            if (this.BinaryInputBad(encrypted))
            {
                return null;
            }

            return Adapter.LqbEncryptor.Decrypt(keyId, encrypted);
        }

        /// <summary>
        /// Mid-level decryption.
        /// </summary>
        /// <param name="keyId">The identifier for the key to use.</param>
        /// <param name="encrypted">Data to decrypt.</param>
        /// <returns>The decrypted string.</returns>
        public string DecryptString(EncryptionKeyIdentifier keyId, byte[] encrypted)
        {
            if (this.BinaryInputBad(encrypted))
            {
                return null;
            }

            var decrypted = this.DecryptBytes(keyId, encrypted);
            return this.BytesToString(decrypted);
        }

        /// <summary>
        /// High-level encryption.
        /// </summary>
        /// <param name="keyId">The identifier for the key to use.</param>
        /// <param name="inputData">String to encrypt.</param>
        /// <returns>The encrypted string base64 encoded.</returns>
        public string EncryptAndEncode(EncryptionKeyIdentifier keyId, string inputData)
        {
            if (this.StringInputBad(inputData))
            {
                return null;
            }

            var encrypted = this.EncryptString(keyId, inputData);
            return Convert.ToBase64String(encrypted);
        }

        /// <summary>
        /// Low-level encryption.
        /// </summary>
        /// <param name="keyId">The identifier for the key to use.</param>
        /// <param name="inputData">Data to encrypt.</param>
        /// <returns>The encrypted data.</returns>
        public byte[] EncryptBytes(EncryptionKeyIdentifier keyId, byte[] inputData)
        {
            if (this.BinaryInputBad(inputData))
            {
                return null;
            }

            return Adapter.LqbEncryptor.Encrypt(keyId, inputData);
        }

        /// <summary>
        /// Mid-level encryption.
        /// </summary>
        /// <param name="keyId">The identifier for the key to use.</param>
        /// <param name="inputData">String to encrypt.</param>
        /// <returns>The encrypted string.</returns>
        public byte[] EncryptString(EncryptionKeyIdentifier keyId, string inputData)
        {
            if (this.StringInputBad(inputData))
            {
                return null;
            }

            if (keyId == EncryptionKeyIdentifier.ThirdParties)
            {
                var inputBuffer = System.Text.UTF8Encoding.UTF8.GetBytes(inputData);
                return this.EncryptBytes(keyId, inputBuffer);
            }
            else
            {
                var inputBuffer = this.StringToBytes(inputData);
                return this.EncryptBytes(keyId, inputBuffer);
            }
        }

        /// <summary>
        /// Determine whether or not the input data can be sent to the cryptographic algorithm.
        /// </summary>
        /// <param name="input">String data to check.</param>
        /// <returns>True if the data is bad, false if the data can be sent to the cryptographic algorithm.</returns>
        private bool StringInputBad(string input)
        {
            return string.IsNullOrEmpty(input);
        }

        /// <summary>
        /// Determine whether or not the input data can be sent to the cryptographic algorithm.
        /// </summary>
        /// <param name="input">Binary data to check.</param>
        /// <returns>True if the data is bad, false if the data can be sent to the cryptographic algorithm.</returns>
        private bool BinaryInputBad(byte[] input)
        {
            return input == null || input.All(b => b == 0); // buffer all zeros causes System.Security.Cryptography.CryptographicException: Length of the data to decrypt is invalid.
        }

        /// <summary>
        /// Convert the input string into an array of bytes.
        /// </summary>
        /// <param name="inputData">The input string that will be converted.</param>
        /// <returns>Array of bytes representing the input string.</returns>
        private byte[] StringToBytes(string inputData)
        {
            // This should have used UTF8, but the existing LQB code uses ASCII for some reason.
            return System.Text.ASCIIEncoding.ASCII.GetBytes(inputData);
        }

        /// <summary>
        /// Convert the input array of bytes to a string.
        /// </summary>
        /// <param name="inputData">The input array of bytes that will be converted.</param>
        /// <returns>String representing the input array of bytes.</returns>
        private string BytesToString(byte[] inputData)
        {
            // This should have used UTF8, but the existing LQB code uses ASCII for some reason.
            return System.Text.ASCIIEncoding.ASCII.GetString(inputData);
        }
    }
}
