﻿namespace LendersOffice.Drivers.Encryption
{
    using System;
    using System.Collections.Generic;

    using CommonProjectLib.Caching;
    using Constants;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;

    /// <summary>
    /// A decorator to cache keys that have been loaded from the database.
    /// </summary>
    public class CachingKeyDriverDecorator : EncryptionKeyDriverDecorator
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CachingKeyDriverDecorator"/> class.
        /// </summary>
        /// <param name="wrapped">The driver to decorate.</param>
        public CachingKeyDriverDecorator(IEncryptionKeyDriver wrapped)
            : base(wrapped)
        {
        }

        /// <summary>
        /// Gets an encryption key by its identifier. Will attempt to use cache.
        /// </summary>
        /// <param name="identifier">The identifier.</param>
        /// <returns>The key associated with the identifier.</returns>
        public override EncryptionKey GetKey(EncryptionKeyIdentifier identifier)
        {
            if (identifier == EncryptionKeyIdentifier.DbConnectionString)
            {
                // The database connection string key is hard-coded. We will not
                // cache the result of the call because the caching mechanism
                // depends on ConstStage and we can't retrieve values from the
                // stage config if we haven't loaded the connection strings from
                // the secure database. We could pick a hard-coded cache duration
                // for the db connection string key, but it is relatively cheap
                // to get the hard-coded version.
                return base.GetKey(identifier);
            }

            return MemoryCacheUtilities.GetOrAddExisting(
                GetCacheKey(identifier),
                () => base.GetKey(identifier),
                TimeSpan.FromMinutes(ConstStage.EncryptionKeyCacheDurationInMinutes));
        }

        /// <summary>
        /// Generates a new encryption key and returns the key identifier and
        /// the key.
        /// </summary>
        /// <returns>The new key identifier and the key.</returns>
        public override KeyValuePair<EncryptionKeyIdentifier, EncryptionKey> GenerateKey()
        {
            var kvp = base.GenerateKey();
            string key = GetCacheKey(kvp.Key);
            MemoryCacheUtilities.GetOrAddExisting(
                key,
                () => kvp.Value,
                TimeSpan.FromMinutes(ConstStage.EncryptionKeyCacheDurationInMinutes));
            return kvp;
        }

        /// <summary>
        /// Gets the key for the cache item.
        /// </summary>
        /// <param name="identifier">The key identifier.</param>
        /// <returns>The key for the cache item.</returns>
        private static string GetCacheKey(EncryptionKeyIdentifier identifier)
        {
            return "encryption_key_" + identifier.ToString();
        }
    }
}
