﻿namespace LendersOffice.Drivers.Integrations
{
    using LqbGrammar.Drivers;

    /// <summary>
    /// Creates instances of the <see cref="IntegrationHttpDriver{TResponse}"/> class.
    /// </summary>
    /// <typeparam name="TResponse">The type of response that will be created by the communication.</typeparam>
    public class IntegrationHttpDriverFactory<TResponse> : IIntegrationDriverFactory<TResponse>
    {
        /// <summary>
        /// Creates an implementation of the <see cref="IIntegrationDriver{TResponse}"/> interface.
        /// </summary>
        /// <returns>A new instance of the <see cref="IntegrationHttpDriver{TResponse}"/> class.</returns>
        public IIntegrationDriver<TResponse> Create()
        {
            return new IntegrationHttpDriver<TResponse>();
        }
    }
}
