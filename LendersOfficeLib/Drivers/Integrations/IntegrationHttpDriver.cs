﻿namespace LendersOffice.Drivers.Integrations
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Net;
    using Common;
    using LendersOffice.Logging;
    using LqbGrammar.Adapters;
    using LqbGrammar.Commands;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Driver for communicating with Integration vendors.
    /// </summary>
    /// <remarks>
    /// Factories depended on:
    /// <see cref="IHttpRequestAdapterFactory"/> (direct).
    /// <see cref="ILoggingDriverFactory"/> (through <see cref="PaulBunyanHelper"/>).
    /// <see cref="LqbGrammar.Queries.IConfigurationQueryFactory"/> (through <see cref="ConstSite"/> and through <see cref="ConstStage"/> in <see cref="PaulBunyanHelper"/>).
    ///     Uses configuration keys: <see cref="ConstSite.MsMqPbLogConnectStr"/>, <see cref="ConstSite.MsMqLogConnectStr"/>.
    /// </remarks>
    /// <typeparam name="TResponse">The type of response object understood by the integration business logic.</typeparam>
    public class IntegrationHttpDriver<TResponse> : IIntegrationDriver<TResponse>
    {
        /// <summary>
        /// Executes a communication with an integration vendor through HTTP.
        /// </summary>
        /// <param name="communication">The communication object to use in executing the communication.</param>
        /// <returns>A result object containing the response object or an error.</returns>
        public Result<TResponse> ExecuteCommunication(IIntegrationHttpCommunication<TResponse> communication)
        {
            if (communication.ShouldLog)
            {
                var logEntry = communication.GetRequestForLogging();
                var requestLogMessage = $"=== {communication.LogHeader} Request === {Environment.NewLine}{logEntry}";
                this.Log(LoggingLevel.Info, communication.LogHeader, requestLogMessage);
            }

            var request = communication.GenerateHttpRequest();
            System.Diagnostics.Stopwatch sw = null;
            if (communication.ShouldLog && communication.LogRequestTiming)
            {
                sw = new System.Diagnostics.Stopwatch();
                sw.Start();
            }

            bool communicationSuccess = false;
            try
            {
                communicationSuccess = LqbGrammar.GenericLocator<IHttpRequestAdapterFactory>.Factory.Create().ExecuteCommunication(communication.GetEndpointPath(), request);
                sw?.Stop();

                if (communication.ShouldLog)
                {
                    var responseMessage = $"=== {communication.LogHeader} Response === {Environment.NewLine}{(request.ResponseBody != null ? communication.MaskResponseString(request.ResponseBody) : "Response could not be read. Dump path: " + request.ResponseDumpPath?.Value)}";
                    this.Log(communicationSuccess ? LoggingLevel.Info : LoggingLevel.Error, communication.LogHeader, responseMessage);
                }

                if (communicationSuccess)
                {
                    return communication.ParseResponse(request);
                }
                else
                {
                    return Result<TResponse>.Failure(null);
                }
            }
            catch (WebException we) when (this.LogWebExceptionAndReturnFalse(communication.ShouldLog, we, communication.LogHeader) || communication.HandleWebException(we))
            {
                return Result<TResponse>.Failure(we);
            }
            catch (LqbException le) when (le.InnerException is WebException && (this.LogWebExceptionAndReturnFalse(communication.ShouldLog, (WebException)le.InnerException, communication.LogHeader) || communication.HandleWebException((WebException)le.InnerException)))
            {
                return Result<TResponse>.Failure(le);
            }
            finally
            {
                this.StopTimingAndLog(communication, sw, communicationSuccess);
            }
        }

        /// <summary>
        /// If the stopwatch exists and is running, this method stops it and logs the milliseconds.
        /// </summary>
        /// <param name="communication">The communication object to use in executing the communication.</param>
        /// <param name="sw">The stopwatch to stop and log with.</param>
        /// <param name="success">True if the request was successful, false otherwise.</param>
        private void StopTimingAndLog(IIntegrationHttpCommunication<TResponse> communication, System.Diagnostics.Stopwatch sw, bool success)
        {
            if (sw != null)
            {
                sw.Stop();

                var additionalTimingData = communication.GetAdditionalTimingData();

                var timingCategory = additionalTimingData?.LogCategory ?? communication.LogHeader;
                var timingMessage = string.Join(
                    Environment.NewLine,
                    $"=== {communication.LogHeader} Request Timer ====",
                    $"Time to response: {sw.ElapsedMilliseconds} ms.",
                    $"Success: {success}",
                    additionalTimingData?.LogMessage);

                var logProperties = new Dictionary<string, string>(1)
                {
                    ["timing_processing"] = sw.ElapsedMilliseconds.ToString()
                };

                this.Log(LoggingLevel.Info, timingCategory, timingMessage, logProperties);
            }
        }

        /// <summary>
        /// Logs an exception and returns false for the purposes of exception filtering.
        /// </summary>
        /// <param name="shouldLog">Whether this method should actually log.</param>
        /// <param name="we">The web exception to log.</param>
        /// <param name="logHeader">The log header label to use in the log message.</param>
        /// <returns>False. This will cause a "when" clause to continue without modifying the exception.</returns>
        private bool LogWebExceptionAndReturnFalse(bool shouldLog, WebException we, string logHeader)
        {
            if (shouldLog)
            {
                string responseText = null;
                if (we.Response != null)
                {
                    using (var responseStream = we.Response.GetResponseStream())
                    using (System.IO.TextReader reader = new System.IO.StreamReader(responseStream))
                    {
                        responseText = reader.ReadToEnd();
                    }
                }

                var logMessage = $"=== {logHeader} Communication Error === {Environment.NewLine}Message: {we.Message} {Environment.NewLine}{(responseText == null ? "No Response Body." : $"Body: {Environment.NewLine}{responseText}")}";
                this.Log(LoggingLevel.Error, logHeader, logMessage);
            }

            return false;
        }

        /// <summary>
        /// Logs a message.
        /// </summary>
        /// <param name="loggingLevel">The logging level of the message.</param>
        /// <param name="category">The category of the log.</param>
        /// <param name="message">The message to log.</param>
        /// <param name="additionalProperties">Additional properties to log.</param>
        private void Log(LoggingLevel loggingLevel, string category, string message, Dictionary<string, string> additionalProperties = null)
        {
            PaulBunyanHelper.Log(loggingLevel, category, message, additionalProperties, useDriver: true);
        }
    }
}
