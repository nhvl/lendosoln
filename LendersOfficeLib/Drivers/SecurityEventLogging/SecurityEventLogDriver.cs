﻿namespace LendersOffice.Drivers.SecurityEventLogging
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using LqbGrammar;
    using LqbGrammar.Adapters.SecurityEventLogging;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers.SecurityEventLogging;    

    /// <summary>
    /// Security event log driver.
    /// </summary>
    public class SecurityEventLogDriver : ISecurityEventLogDriver
    {
        /// <summary>
        /// Private adapter object.
        /// </summary>
        private ISecurityEventLogAdapter adapter;

        /// <summary>
        /// Initializes a new instance of the <see cref="SecurityEventLogDriver" /> class.
        /// </summary>        
        public SecurityEventLogDriver()
        {
            var creator = GenericLocator<ISecurityEventLogAdapterFactory>.Factory;
            this.adapter = creator.Create();
        }

        /// <summary>
        /// Creates a new security event log.
        /// </summary>
        /// <param name="principal">IUserPrincipal object.</param>
        /// <param name="clientIp">Client IP.</param>
        /// <param name="eventType">Security event type.</param>
        /// <param name="description">Log description.</param>
        /// <param name="conn">IDbConnection object.</param>
        /// <param name="trans">IDbTransaction object.</param>
        public void CreateSecurityEventLog(IUserPrincipal principal, string clientIp, SecurityEventType eventType, string description, IDbConnection conn, IDbTransaction trans)
        {
            this.adapter.CreateSecurityEventLog(principal, clientIp, eventType, description, conn, trans);
        }

        /// <summary>
        /// Gets security event logs for download.
        /// </summary>
        /// <param name="filter">Security Events Log filter object.</param>
        /// <param name="conn">IDbConnection object.</param>
        /// <param name="maxDownloadResults">Maximum number allowed for download.</param>
        /// <returns>Security event logs.</returns>
        public IEnumerable<SecurityEventLog> GetSecurityEventLogsForDownload(SecurityEventLogsFilter filter, IDbConnection conn, int maxDownloadResults)
        {
            return this.adapter.GetSecurityEventLogsForDownload(filter, conn, maxDownloadResults);
        }

        /// <summary>
        /// Search security event logs.
        /// </summary>
        /// <param name="filter">Security Events Log filter object.</param>
        /// <param name="currentPage">Current page count in the search.</param>
        /// <param name="searchResultsPerPage">Number of search results per page.</param>
        /// <param name="conn">IDbConnection object.</param>
        /// <param name="maxSearchResults">Maximum number allowed for search.</param>
        /// <returns>Search results view model.</returns>
        public SecurityEventLogSearchResults SearchSecurityEventLog(SecurityEventLogsFilter filter, int currentPage, int searchResultsPerPage, IDbConnection conn, int maxSearchResults)
        {
            return this.adapter.SearchSecurityEventLog(filter, currentPage, searchResultsPerPage, conn, maxSearchResults);
        }        
    }
}
