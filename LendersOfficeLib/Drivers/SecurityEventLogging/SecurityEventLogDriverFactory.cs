﻿namespace LendersOffice.Drivers.SecurityEventLogging
{
    using System.Data;
    using LqbGrammar.Drivers.SecurityEventLogging;

    /// <summary>
    /// Security Event Log Driver Factory.
    /// </summary>
    public class SecurityEventLogDriverFactory : ISecurityEventLogDriverFactory
    {
        /// <summary>
        /// Creates new Security Event Log Driver.
        /// </summary>
        /// <returns>ISecurityEventLogDriver object.</returns>
        public ISecurityEventLogDriver Create()
        {
            return new SecurityEventLogDriver();
        }
    }
}
