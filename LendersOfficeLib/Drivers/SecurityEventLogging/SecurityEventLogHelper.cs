﻿namespace LendersOffice.Common
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Text;
    using System.Web;
    using Constants;
    using DataAccess;
    using Drivers.FileSystem;
    using LqbGrammar;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;
    using LqbGrammar.Drivers.SecurityEventLogging;
    using Security;
    using TextImport;

    /// <summary>
    /// Helper class for Security Event Logs.
    /// </summary>
    public static class SecurityEventLogHelper
    {
        /// <summary>
        /// Search results per page.
        /// </summary>
        public const int SearchResultsPerPage = 100;

        /// <summary>
        /// ISecurityEventLogDriverFactory factory.
        /// </summary>
        private static ISecurityEventLogDriverFactory factory = GenericLocator<ISecurityEventLogDriverFactory>.Factory;
        
        /// <summary>
        /// Creates login security event log.
        /// </summary>
        /// <param name="principal">IUserPrincipal object.</param>
        public static void CreateLoginLog(IUserPrincipal principal)
        {
            if (principal == null)
            {
                return;
            }

            var driver = factory.Create();

            using (var conn = DbConnectionInfo.GetConnection(principal.BrokerId))
            {
                if (conn.State != ConnectionState.Open)
                {
                    conn.Open();
                }

                using (var trans = conn.BeginTransaction())
                {
                    try
                    {
                        driver.CreateSecurityEventLog(principal, RequestHelper.ClientIP, SecurityEventType.LogIn, $"Account '{principal.LoginNm}' has logged in.", conn, trans);

                        if (principal.UserId != Guid.Empty)
                        {
                            UpdateAllUserRecentLogin(conn, trans, principal.UserId);
                        }

                        trans.Commit();
                    }
                    catch (Exception e)
                    {
                        Tools.LogError(e);
                        trans.Rollback();
                        throw;
                    }
                }                
            }            
        }

        /// <summary>
        /// Creates web service authentication security event log.
        /// </summary>
        /// <param name="principal">IUserPrincipal object.</param>
        public static void CreateWebserviceAuthenticationLog(IUserPrincipal principal)
        {
            if (principal == null)
            {
                return;
            }

            var driver = factory.Create();

            using (var conn = DbConnectionInfo.GetConnection(principal.BrokerId))
            {
                if (conn.State != ConnectionState.Open)
                {
                    conn.Open();
                }

                using (var trans = conn.BeginTransaction())
                {
                    try
                    {
                        driver.CreateSecurityEventLog(principal, RequestHelper.ClientIP, SecurityEventType.WebserviceAuthentication, $"Webservice authentication ticket granted to '{principal.LoginNm}'. This ticket expires {Tools.GetDateTimeDescription(DateTime.Now.AddHours(4),"G")}.", conn, trans);
                        UpdateAllUserRecentLogin(conn, trans, principal.UserId);

                        trans.Commit();
                    }
                    catch (Exception e)
                    {
                        Tools.LogError(e);
                        trans.Rollback();
                        throw;
                    }
                }
            }
        }

        /// <summary>
        /// Creates either a failed login log or a failed webservice authentication log.
        /// </summary>
        /// <param name="principal">IUserPrincipal object.</param>
        /// <param name="loginSource">Login Source, whether through webservice or regular website.</param>
        public static void CreateFailedAuthenticationLog(IUserPrincipal principal, LoginSource loginSource)
        {
            if (principal == null)
            {
                return;
            }

            var driver = factory.Create();

            using (var conn = DbConnectionInfo.GetConnection(principal.BrokerId))
            {
                if (loginSource == LoginSource.Webservice)
                {
                    driver.CreateSecurityEventLog(principal, RequestHelper.ClientIP, SecurityEventType.FailedWebserviceAuthentication, $"Webservice authentication failure for account '{principal.LoginNm}'.", conn, null);
                }
                else
                {
                    driver.CreateSecurityEventLog(principal, RequestHelper.ClientIP, SecurityEventType.FailedLogin, $"Login attempt failure for account '{principal.LoginNm}'.", conn, null);
                }
            }
        }
        
        /// <summary>
        /// Creates account lock security event log.
        /// </summary>
        /// <param name="principal">IUserPrincipal object.</param>
        public static void CreateAccountLockLog(IUserPrincipal principal)
        {
            if (principal == null)
            {
                return;
            }

            var driver = factory.Create();

            using (var conn = DbConnectionInfo.GetConnection(principal.BrokerId))
            {
                driver.CreateSecurityEventLog(principal, RequestHelper.ClientIP, SecurityEventType.AccountLocking, $"Account '{principal.LoginNm}' has been locked due to too many incorrect attempts.", conn, null);
            }
        }

        /// <summary>
        /// Create a log when the user logs out.
        /// </summary>
        /// <param name="principal">Principal object.</param>
        public static void CreateLogoutLog(AbstractUserPrincipal principal)
        {
            if (principal == null)
            {
                return;
            }

            var driver = factory.Create();

            using (var conn = DbConnectionInfo.GetConnection(principal.BrokerId))
            {
                driver.CreateSecurityEventLog(principal, RequestHelper.ClientIP, SecurityEventType.LogOut, $"Account '{principal.LoginNm}' has been logged out.", conn, null);
            }
        }

        /// <summary>
        /// Create a log when an internal LQB user becomes an actual user.
        /// </summary>
        /// <param name="principal">Principal object.</param>
        public static void CreateBecomeUserLog(AbstractUserPrincipal principal)
        {
            if (principal == null)
            {
                return;
            }

            var driver = factory.Create();

            using (var conn = DbConnectionInfo.GetConnection(principal.BrokerId))
            {
                driver.CreateSecurityEventLog(principal, RequestHelper.ClientIP, SecurityEventType.BecomeUser, $"LQB User logging in as '{principal.LoginNm}'.", conn, null);
            }
        }
        
        /// <summary>
        /// Create a log when a user creates a new account.
        /// </summary>
        /// <param name="principal">Principal object.</param>
        /// <param name="editedEmployeeNm">Name of the employee created.</param>
        public static void CreateAccountCreationLog(AbstractUserPrincipal principal, string editedEmployeeNm)
        {
            if (principal == null)
            {
                return;
            }

            var driver = factory.Create();

            using (var conn = DbConnectionInfo.GetConnection(principal.BrokerId))
            {
                driver.CreateSecurityEventLog(principal, RequestHelper.ClientIP, SecurityEventType.AccountCreation, $"New account '{editedEmployeeNm}' has been created by '{principal.FirstName} {principal.LastName}'.", conn, null);
            }
        }

        /// <summary>
        /// Create a log when a user modifies an employee.
        /// </summary>
        /// <param name="principal">Principal object.</param>
        /// <param name="editedEmployeeNm">Name of the employee being modified.</param>
        public static void CreateEmployeeSettingsChangeLog(AbstractUserPrincipal principal, string editedEmployeeNm)
        {
            if (principal == null)
            {
                return;
            }

            var driver = factory.Create();

            string description;

            if (principal.ApplicationType == E_ApplicationT.PriceMyLoan)
            {
                description = "OC User";
            }
            else
            {
                description = "User";
            }

            description += $" '{editedEmployeeNm}' has been edited by '{principal.FirstName} {principal.LastName}'.";

            using (var conn = DbConnectionInfo.GetConnection(principal.BrokerId))
            {
                driver.CreateSecurityEventLog(principal, RequestHelper.ClientIP, SecurityEventType.ChangingUserSettings, description, conn, null);
            }
        }

        /// <summary>
        /// Create a log when sensitive data is exported.
        /// </summary>
        /// <param name="principal">Principal object.</param>
        /// <param name="reportType">Sensitive report type.</param>
        /// <param name="reportName">Sensitive report name.</param>
        public static void CreateSensitiveDataExportLog(AbstractUserPrincipal principal, SensitiveReportType reportType, string reportName)
        {
            if (principal == null)
            {
                return;
            }

            var driver = factory.Create();

            string description = reportType == SensitiveReportType.BatchExport ? "Batch Export" : "Custom report";

            description += $" '{reportName}' with sensitive information was exported.";

            using (var conn = DbConnectionInfo.GetConnection(principal.BrokerId))
            {
                driver.CreateSecurityEventLog(principal, RequestHelper.ClientIP, SecurityEventType.SensitiveReportExport, description, conn, null);
            }
        }

        /// <summary>
        /// Search security event log.
        /// </summary>
        /// <param name="filter">Security Events Log filter object.</param>
        /// <param name="currentPage">Current page index.</param>
        /// <returns>Security event log search results view model object.</returns>
        public static SecurityEventLogSearchResults SearchSecurityEventLogs(SecurityEventLogsFilter filter, int currentPage)
        {
            var factory = GenericLocator<ISecurityEventLogDriverFactory>.Factory;
            var driver = factory.Create();

            try
            {
                using (var conn = DbConnectionInfo.GetConnection(filter.BrokerId))
                {
                    return driver.SearchSecurityEventLog(filter, currentPage, SearchResultsPerPage, conn, ConstStage.SecurityEventLogMaxSearchResults);
                }
            }
            catch (Exception e)
            {
                throw new CBaseException("Search results are too large. Please restrict the search results.", e);
            }
        }

        /// <summary>
        /// Download security events.
        /// </summary>
        /// <param name="filter">Security Events Log filter object.</param>
        public static void DownloadSecurityEventLogs(SecurityEventLogsFilter filter)
        {
            var factory = GenericLocator<ISecurityEventLogDriverFactory>.Factory;
            var driver = factory.Create();

            IEnumerable<SecurityEventLog> logs;

            try
            {
                using (var conn = DbConnectionInfo.GetConnection(filter.BrokerId))
                {
                    logs = driver.GetSecurityEventLogsForDownload(filter, conn, ConstStage.SecurityEventLogMaxDownloadResults);
                }
            }
            catch (Exception e)
            {
                throw new CBaseException("Error while downloading security event logs.", e);
            }

            var csvBuilder = new StringBuilder();

            csvBuilder.AppendLine("Security Event Type, Event Description, User Name, Login Name, User Type, Timestamp, IP Address");

            foreach (SecurityEventLog log in logs)
            {
                csvBuilder.AppendLine($"{DataParsing.sanitizeForCSV(log.SecurityEventType)}, {DataParsing.sanitizeForCSV(log.DescriptionText)}, {DataParsing.sanitizeForCSV(log.UserFullNm)}, {DataParsing.sanitizeForCSV(log.LoginNm)}, {DataParsing.sanitizeForCSV(log.UserType)}, {DataParsing.sanitizeForCSV(log.TimeStamp)}, {DataParsing.sanitizeForCSV(log.ClientIP)}");
            }

            string path = TempFileUtils.NewTempFilePath() + ".csv";
            LendersOffice.Drivers.Gateways.TextFileHelper.WriteString(path, csvBuilder.ToString(), false);
            RequestHelper.SendFileToClient(HttpContext.Current, path, "application/download", "SecurityEventLog.csv");
            HttpContext.Current.Response.SuppressContent = true;
            HttpContext.Current.ApplicationInstance.CompleteRequest();

            // Clean up the temp file
            FileOperationHelper.Delete(LocalFilePath.Create(path).Value);
        }
        
        /// <summary>
        /// Update all user table's recent login column.
        /// </summary>
        /// <param name="conn">IDbConnection object.</param>
        /// <param name="trans">IDbTransaction object.</param>
        /// <param name="userId">User Id Guid.</param>
        private static void UpdateAllUserRecentLogin(IDbConnection conn, IDbTransaction trans, Guid userId)
        {
            IStoredProcedureDriverFactory factory = GenericLocator<IStoredProcedureDriverFactory>.Factory;
            var driver = factory.Create(TimeoutInSeconds.Default);

            List<SqlParameter> allUserParameters = new List<SqlParameter>()
                    {
                        new SqlParameter("@UserId", userId),
                        new SqlParameter("@IpAddress", RequestHelper.ClientIP),
                        new SqlParameter("@EventDate", DateTime.Now)
                    };

            StoredProcedureName allUserUpdateSpName = StoredProcedureName.Create("ALL_USER_UPDATE_RECENT_LOGIN").Value;

            driver.ExecuteNonQuery(conn, trans, allUserUpdateSpName, allUserParameters);
        }
    }
}
