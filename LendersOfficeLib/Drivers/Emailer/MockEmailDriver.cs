﻿namespace LendersOffice.Drivers.Emailer
{
    using LqbGrammar.Drivers.Emailer;

    /// <summary>
    /// This class is a mock email driver.
    /// </summary>
    internal sealed class MockEmailDriver : IEmailDriver
    {
        /// <summary>
        /// The mock method doesn't do anything, no email is actually sent.
        /// </summary>
        /// <param name="email">The parameter is not used.</param>
        public void SendEmail(EmailPackage email)
        {
            // do nothing
        }
    }
}
