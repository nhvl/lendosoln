﻿namespace LendersOffice.Drivers.Emailer
{
    using LqbGrammar;
    using LqbGrammar.Adapters;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers.Emailer;

    /// <summary>
    /// Factory for creating instances of the EmailReceiverDriver class.
    /// </summary>
    public sealed class EmailReceiverDriverFactory : IReceiveEmailDriverFactory
    {
        /// <summary>
        /// Create an email driver.
        /// </summary>
        /// <param name="server">The email server name that will be used for sending the email.</param>
        /// <param name="userName">The user name for logging into the email server.</param>
        /// <param name="password">The password for logging into the email server.</param>
        /// <returns>An email driver.</returns>
        public static IReceiveEmailDriver CreateEmailReceiver(EmailServerName server, string userName, string password)
        {
            var factory = GenericLocator<IReceiveEmailDriverFactory>.Factory;
            return factory.Create(server, userName, password);
        }

        /// <summary>
        /// Create an email driver.
        /// </summary>
        /// <param name="server">The email server name that will be used for sending the email.</param>
        /// <param name="userName">The user name for logging into the email server.</param>
        /// <param name="password">The password for logging into the email server.</param>
        /// <returns>An email driver.</returns>
        public IReceiveEmailDriver Create(EmailServerName server, string userName, string password)
        {
            // NOTE: LQB only makes use of port 110 (pop3) when receiving emails.
            var factory = GenericLocator<IReceiveEmailAdapterFactory>.Factory;
            var adapter = factory.Create(server, userName, password, EmailServerName.Pop3Port);

            return new EmailReceiverDriver(adapter);
        }
    }
}
