﻿namespace LendersOffice.Drivers.Emailer
{
    using LqbGrammar.Adapters;
    using LqbGrammar.Drivers.Emailer;

    /// <summary>
    /// Simple driver that uses the registered adapter to carry out all calls.
    /// </summary>
    internal sealed class EmailReceiverDriver : IReceiveEmailDriver
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EmailReceiverDriver"/> class.
        /// </summary>
        /// <param name="adapter">The adapter used to carry out all calls.</param>
        public EmailReceiverDriver(IReceiveEmailAdapter adapter)
        {
            this.Adapter = adapter;
        }

        /// <summary>
        /// Gets the number of email messages available on the server.
        /// </summary>
        /// <value>The number of email messages available on the server.</value>
        public int Count
        {
            get
            {
                return this.Adapter.Count;
            }
        }

        /// <summary>
        /// Gets or sets the adapter used to carry out all calls.
        /// </summary>
        /// <value>The adapter used to carry out all calls.</value>
        private IReceiveEmailAdapter Adapter { get; set; }

        /// <summary>
        /// Disconnect from the email server.
        /// </summary>
        public void Dispose()
        {
            // The timing of Dispose calls on the finalizer thread is unpredictable
            // so we won't bother with a finalizer method for this class.
            this.Adapter.Dispose();
        }

        /// <summary>
        /// Retrieve the email data for the input index.
        /// </summary>
        /// <param name="index">A value in the range [0, Count).</param>
        /// <returns>The data for the specified email.</returns>
        public IReceivedEmailData GetEmail(int index)
        {
            return this.Adapter.GetEmail(index);
        }

        /// <summary>
        /// Delete the email data for the input index.
        /// </summary>
        /// <param name="index">A value in the range [0, Count).</param>
        public void DeleteEmail(int index)
        {
            this.Adapter.DeleteEmail(index);
        }

        /// <summary>
        /// Retrieve an identifer for the email with the input index.
        /// </summary>
        /// <param name="index">A value in the range [0, Count).</param>
        /// <returns>An identifier for the specified email.</returns>
        public EmailIdentifier GetIdentifier(int index)
        {
            return this.Adapter.GetIdentifier(index);
        }
    }
}
