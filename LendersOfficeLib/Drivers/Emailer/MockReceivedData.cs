﻿namespace LendersOffice.Drivers.Emailer
{
    using System;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers.Emailer;

    /// <summary>
    /// Mock for simulating a recieved email message.
    /// </summary>
    internal sealed class MockReceivedData : IReceivedEmailData
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MockReceivedData"/> class.
        /// </summary>
        /// <param name="package">The email that was sent.</param>
        /// <param name="dateSent">The date the email was sent.</param>
        /// <param name="autoResponseSuppressed">Indicate whether auto-response is suppressed.</param>
        /// <param name="rawMessage">Raw message - can be anything as this is only logged by client code.</param>
        public MockReceivedData(EmailPackage package, DateTime dateSent, bool autoResponseSuppressed, string rawMessage)
        {
            this.Contents = package;

            if (dateSent.Kind != DateTimeKind.Utc)
            {
                dateSent = dateSent.ToUniversalTime();
            }

            this.DateSent = LqbEventDate.Create(dateSent).Value;
            this.IsAutoResponseSuppressed = autoResponseSuppressed;

            var encoder = new System.Text.ASCIIEncoding(); // existing pop3 client code assumes ascii encoding
            this.RawMessage = encoder.GetBytes(rawMessage);
        }

        /// <summary>
        /// Gets the contents of the email at the time it was sent.
        /// </summary>
        /// <value>The contents of the email at the time it was sent.</value>
        public EmailPackage Contents { get; private set; }

        /// <summary>
        /// Gets the timestamp of when the email was sent.
        /// </summary>
        /// <value>The timestamp of when the email was sent.</value>
        public LqbEventDate DateSent { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the X-Auto-Response-Suppress header was set.
        /// </summary>
        /// <value>A value indicating whether the X-Auto-Response-Suppress header was set.</value>
        public bool IsAutoResponseSuppressed { get; private set; }

        /// <summary>
        /// Gets the full raw message that was received.
        /// </summary>
        /// <value>The full raw message that was received.</value>
        public byte[] RawMessage { get; private set; }
    }
}
