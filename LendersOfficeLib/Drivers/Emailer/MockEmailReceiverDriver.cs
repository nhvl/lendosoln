﻿namespace LendersOffice.Drivers.Emailer
{
    using LqbGrammar.Adapters;
    using LqbGrammar.Drivers.Emailer;

    /// <summary>
    /// Mock driver that mimics receiving emails.
    /// </summary>
    internal sealed class MockEmailReceiverDriver : IReceiveEmailDriver
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MockEmailReceiverDriver"/> class.
        /// </summary>
        /// <param name="emails">The emails to return.</param>
        /// <param name="identifiers">The identifiers to return.</param>
        public MockEmailReceiverDriver(MockReceivedData[] emails, string[] identifiers)
        {
            this.Emails = emails;
            this.Identifiers = identifiers;
        }

        /// <summary>
        /// Gets the number of email messages available on the server.
        /// </summary>
        /// <value>The number of email messages available on the server.</value>
        public int Count
        {
            get
            {
                return this.Emails.Length;
            }
        }

        /// <summary>
        /// Gets or sets the mock emails.
        /// </summary>
        /// <value>The mock emails.</value>
        private MockReceivedData[] Emails { get; set; }

        /// <summary>
        /// Gets or sets the mock email identifiers.
        /// </summary>
        /// <value>The mock email identifiers.</value>
        private string[] Identifiers { get; set; }

        /// <summary>
        /// Do nothing.
        /// </summary>
        public void Dispose()
        {
        }

        /// <summary>
        /// Retrieve the email data for the input index.
        /// </summary>
        /// <param name="index">A value in the range [0, Count).</param>
        /// <returns>The data for the specified email.</returns>
        public IReceivedEmailData GetEmail(int index)
        {
            return this.Emails[index];
        }

        /// <summary>
        /// Delete the email data for the input index.
        /// </summary>
        /// <param name="index">A value in the range [0, Count).</param>
        public void DeleteEmail(int index)
        {
            // do nothing
        }

        /// <summary>
        /// Retrieve an identifer for the email with the input index.
        /// </summary>
        /// <param name="index">A value in the range [0, Count).</param>
        /// <returns>An identifier for the specified email.</returns>
        public EmailIdentifier GetIdentifier(int index)
        {
            return EmailIdentifier.Create(this.Identifiers[index]);
        }
    }
}
