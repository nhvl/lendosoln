﻿namespace LendersOffice.Drivers.Emailer
{
    using System;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers.Emailer;

    /// <summary>
    /// Factory for creating mock email receiver drivers.
    /// </summary>
    public sealed class MockEmailReceiverDriverFactory : IReceiveEmailDriverFactory
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MockEmailReceiverDriverFactory"/> class.
        /// </summary>
        /// <param name="package">The emails that were sent.</param>
        /// <param name="identifiers">Identifiers for the emails.</param>
        /// <param name="dateSent">The dates the email were sent.</param>
        /// <param name="autoResponseSuppressed">Indicate whether auto-response is suppressed.</param>
        /// <param name="rawMessage">Raw messages - can be anything as this is only logged by client code.</param>
        public MockEmailReceiverDriverFactory(EmailPackage[] package, string[] identifiers, DateTime[] dateSent, bool[] autoResponseSuppressed, string[] rawMessage)
        {
            this.Contents = package;
            this.Identifiers = identifiers;
            this.DateSent = dateSent;
            this.IsAutoResponseSuppressed = autoResponseSuppressed;
            this.RawMessage = rawMessage;
        }

        /// <summary>
        /// Gets or sets the contents of the email at the time it was sent.
        /// </summary>
        /// <value>The contents of the email at the time it was sent.</value>
        private EmailPackage[] Contents { get; set; }

        /// <summary>
        /// Gets or sets the identifiers for the emails.
        /// </summary>
        /// <value>The identifiers for the emails.</value>
        private string[] Identifiers { get; set; }

        /// <summary>
        /// Gets or sets the timestamp of when the email was sent.
        /// </summary>
        /// <value>The timestamp of when the email was sent.</value>
        private DateTime[] DateSent { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the X-Auto-Response-Suppress header was set.
        /// </summary>
        /// <value>A value indicating whether the X-Auto-Response-Suppress header was set.</value>
        private bool[] IsAutoResponseSuppressed { get; set; }

        /// <summary>
        /// Gets or sets the full raw message that was received.
        /// </summary>
        /// <value>The full raw message that was received.</value>
        private string[] RawMessage { get; set; }

        /// <summary>
        /// Create an email adapter.
        /// </summary>
        /// <param name="server">The parameter is not used.</param>
        /// <param name="userName">The parameter is not used.</param>
        /// <param name="password">The parameter is not used.</param>
        /// <returns>An email adapter.</returns>
        public IReceiveEmailDriver Create(EmailServerName server, string userName, string password)
        {
            var data = new MockReceivedData[this.Contents.Length];
            for (int i = 0; i < this.Contents.Length; ++i)
            {
                data[i] = new MockReceivedData(this.Contents[i], this.DateSent[i], this.IsAutoResponseSuppressed[i], this.RawMessage[i]);
            }

            return new MockEmailReceiverDriver(data, this.Identifiers);
        }
    }
}
