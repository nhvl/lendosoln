﻿namespace LendersOffice.Drivers.Emailer
{
    using LendersOffice.Constants;
    using LendersOffice.ObjLib.Email;
    using LqbGrammar;
    using LqbGrammar.Adapters;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers.Emailer;

    /// <summary>
    /// This is the standard email driver.
    /// </summary>
    internal sealed class EmailDriver : IEmailDriver
    {
        /// <summary>
        /// The name of the email server.
        /// </summary>
        private EmailServerName server;

        /// <summary>
        /// The port number of the server.
        /// </summary>
        private PortNumber portNumber;

        /// <summary>
        /// Initializes a new instance of the <see cref="EmailDriver"/> class.
        /// </summary>
        /// <param name="server">Name of the email server.</param>
        /// <param name="portNumber">Port number of the server.</param>
        public EmailDriver(EmailServerName server, PortNumber portNumber)
        {
            this.server = server;
            this.portNumber = portNumber;
        }

        /// <summary>
        /// Send an email via the standard email adapter.
        /// </summary>
        /// <param name="email">This contains all the data that composes an email.</param>
        public void SendEmail(EmailPackage email)
        {
            // OPM 461108 - Check if from address has been validated for spoofing.
            // If authorized, set the sender address to the from address.
            // If not authorized, use "On behalf of" and set the from and sender to the LQB default address.
            // NOTE: Added to Driver instead of Adapter because of need for access to LendersOffice namespace.
            if (ConstStage.EnableEmailDomainAuthorization)
            {
                if (AuthorizedEmailDomainUtilities.IsAddressFromAuthorizedDomain(email.From, email.BrokerId))
                {
                    email.Sender = email.From;
                }
                else
                {
                    email.UpdateFromAddress(EmailAddress.Create(ConstStage.DefaultSenderAddress, email.From).Value);
                    email.Sender = EmailAddress.Create(ConstStage.DefaultSenderAddress).Value;
                }
            }

            IEmailAdapterFactory iFactory = GenericLocator<IEmailAdapterFactory>.Factory;
            IEmailAdapter iAdapter = iFactory.Create(this.server, this.portNumber);
            iAdapter.SendEmail(email);
        }
    }
}
