﻿namespace LendersOffice.Drivers.Emailer
{
    using LqbGrammar;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers.Emailer;

    /// <summary>
    /// This is the factory for creating the standard email driver.
    /// </summary>
    public sealed class EmailDriverFactory : IEmailDriverFactory
    {
        /// <summary>
        /// This is the method for creating the standard email driver.
        /// </summary>
        /// <param name="server">This is the name of the email server.</param>
        /// <param name="portNumber">The port number of the server.</param>
        /// <returns>The driver interface.</returns>
        public IEmailDriver Create(EmailServerName server, PortNumber portNumber)
        {
            return new EmailDriver(server, portNumber);
        }
    }
}
