﻿namespace LendersOffice.Drivers.Emailer
{
    using LqbGrammar;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers.Emailer;

    /// <summary>
    /// Utility for interacting with the email driver.
    /// </summary>
    public static class EmailHelper
    {
        /// <summary>
        /// Send an email via the standard email adapter.
        /// </summary>
        /// <param name="server">The email server used to send emails.</param>
        /// <param name="portNumber">The email server's port number.</param>
        /// <param name="email">This contains all the data that composes an email.</param>
        public static void SendEmail(EmailServerName server, PortNumber portNumber, EmailPackage email)
        {
            var factory = GenericLocator<IEmailDriverFactory>.Factory;
            var driver = factory.Create(server, portNumber);
            driver.SendEmail(email);
        }
    }
}
