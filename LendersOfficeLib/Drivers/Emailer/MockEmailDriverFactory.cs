﻿namespace LendersOffice.Drivers.Emailer
{
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers.Emailer;

    /// <summary>
    /// This is a factory for creation of a mock email driver.
    /// </summary>
    public sealed class MockEmailDriverFactory : IEmailDriverFactory
    {
        /// <summary>
        /// Create a mock email driver.
        /// </summary>
        /// <param name="server">The parameter is not used.</param>
        /// <param name="portNumber">The parameter is not used.</param>
        /// <returns>The email driver interface.</returns>
        public IEmailDriver Create(EmailServerName server, PortNumber portNumber)
        {
            return new MockEmailDriver();
        }
    }
}
