﻿namespace LendersOffice.Drivers.Json
{
    using System;
    using LqbGrammar;
    using LqbGrammar.Adapters;
    using LqbGrammar.Drivers;

    /// <summary>
    /// Factory for creating implementations of the IJsonDriver interface.
    /// </summary>
    public class JsonDriverFactory : IJsonDriverFactory
    {
        /// <summary>
        /// Create an implementation of the IJsonDriver interface.
        /// </summary>
        /// <param name="serializer">The serializer to use when generating JSON.</param>
        /// <param name="options">The options that drive the behavior of the JSON generation.</param>
        /// <returns>An implementation of the IJsonDriver interface.</returns>
        public IJsonDriver Create(JsonSerializer serializer, JsonSerializerOptions options)
        {
            var adapterFactory = GenericLocator<IJsonAdapterFactory>.Factory;
            var adapter = adapterFactory.Create(serializer, options);

            return new JsonDriver(adapter);
        }
    }
}
