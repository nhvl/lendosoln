﻿namespace LendersOffice.Drivers.Json
{
    using System.IO;
    using LqbGrammar;
    using LqbGrammar.Drivers;

    /// <summary>
    /// Utility for interacting with the json driver.
    /// </summary>
    public static class JsonHelper
    {
        /// <summary>
        /// Deserialize a JSON string back to an instance of the indicated type.
        /// </summary>
        /// <typeparam name="T">The type of the object to be deserialize.</typeparam>
        /// <param name="serialized">The JSON serialization of an instance of the target type.</param>
        /// <param name="sanitize">True if the input json should be sanitized.</param>
        /// <param name="serializer">The serializer to use when generating JSON.</param>
        /// <param name="options">The options that drive the behavior of the JSON generation.</param>
        /// <returns>The deserialized instance of the target type.</returns>
        public static T Deserialize<T>(string serialized, bool sanitize, JsonSerializer serializer, JsonSerializerOptions options) where T : class, new()
        {
            var driver = GetDriver(serializer, options);
            return driver.Deserialize<T>(serialized, sanitize);
        }

        /// <summary>
        /// Deserialize a JSON string back to an instance of the indicated type.
        /// </summary>
        /// <typeparam name="T">The type of the object to be deserialize.</typeparam>
        /// <typeparam name="R">The type used as the converter.</typeparam>
        /// <param name="reader">The JSON serialization of an instance of the target type.</param>
        /// <param name="converter">The converter used during deserialization.</param>
        /// <param name="serializer">The serializer to use when generating JSON.</param>
        /// <param name="options">The options that drive the behavior of the JSON generation.</param>
        /// <returns>The deserialized instance of the target type.</returns>
        public static T Deserialize<T, R>(StreamReader reader, R converter, JsonSerializer serializer, JsonSerializerOptions options)
            where T : class, new()
            where R : class
        {
            var driver = GetDriver(serializer, options);
            return driver.Deserialize<T, R>(reader, converter);
        }

        /// <summary>
        /// Serialize an object of type T to a JSON string.
        /// </summary>
        /// <typeparam name="T">The type of the object to be serialized.</typeparam>
        /// <param name="forSerialization">An instance of the type to serialize.</param>
        /// <param name="sanitize">True if the output json should be sanitized.</param>
        /// <param name="serializer">The serializer to use when generating JSON.</param>
        /// <param name="options">The options that drive the behavior of the JSON generation.</param>
        /// <returns>The serialization of the instance to a JSON string.</returns>
        public static string Serialize<T>(T forSerialization, bool sanitize, JsonSerializer serializer, JsonSerializerOptions options) where T : class, new()
        {
            var driver = GetDriver(serializer, options);
            return driver.Serialize<T>(forSerialization, sanitize);
        }

        /// <summary>
        /// Serialize an object of type T to a JSON string.
        /// </summary>
        /// <typeparam name="T">The type of the object to be serialized.</typeparam>
        /// <typeparam name="R">The type used as the resolver.</typeparam>
        /// <param name="forSerialization">An instance of the type to serialize.</param>
        /// <param name="resolver">A class that is used for type resolution during serialization.</param>
        /// <param name="sanitize">True if the output json should be sanitized.</param>
        /// <param name="serializer">The serializer to use when generating JSON.</param>
        /// <param name="options">The options that drive the behavior of the JSON generation.</param>
        /// <returns>The serialization of the instance to a JSON string.</returns>
        public static string Serialize<T, R>(T forSerialization, R resolver, bool sanitize, JsonSerializer serializer, JsonSerializerOptions options)
            where T : class, new()
            where R : class
        {
            var driver = GetDriver(serializer, options);
            return driver.Serialize<T, R>(forSerialization, resolver, sanitize);
        }

        /// <summary>
        /// Retrieve the driver.
        /// </summary>
        /// <param name="serializer">The serializer to use when generating JSON.</param>
        /// <param name="options">The options that drive the behavior of the JSON generation.</param>
        /// <returns>The driver.</returns>
        private static IJsonDriver GetDriver(JsonSerializer serializer, JsonSerializerOptions options)
        {
            var factory = GenericLocator<IJsonDriverFactory>.Factory;
            return factory.Create(serializer, options);
        }
    }
}
