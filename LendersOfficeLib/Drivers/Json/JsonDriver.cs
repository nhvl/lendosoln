﻿namespace LendersOffice.Drivers.Json
{
    using System;
    using System.IO;
    using LqbGrammar.Adapters;
    using LqbGrammar.Drivers;

    /// <summary>
    /// Driver that implements the IJsonDriver interface.
    /// </summary>
    internal class JsonDriver : IJsonDriver
    {
        /// <summary>
        /// The adapter that implements the IJsonAdapter interface.
        /// </summary>
        private IJsonAdapter adapter;

        /// <summary>
        /// Initializes a new instance of the <see cref="JsonDriver"/> class.
        /// </summary>
        /// <param name="adapter">An adapter that implements the IJsonAdapter interface.</param>
        public JsonDriver(IJsonAdapter adapter)
        {
            this.adapter = adapter;
        }

        /// <summary>
        /// Deserialize a JSON string back to an instance of the indicated type.
        /// </summary>
        /// <typeparam name="T">The type of the object to be deserialize.</typeparam>
        /// <param name="serialized">The JSON serialization of an instance of the target type.</param>
        /// <param name="sanitize">True if the input json should be sanitized.</param>
        /// <returns>The deserialized instance of the target type.</returns>
        public T Deserialize<T>(string serialized, bool sanitize) where T : class, new()
        {
            if (sanitize)
            {
                serialized = this.adapter.SanitizeJsonString(serialized);
            }

            return this.adapter.Deserialize<T>(serialized);
        }

        /// <summary>
        /// Deserialize a JSON string back to an instance of the indicated type.
        /// </summary>
        /// <typeparam name="T">The type of the object to be deserialize.</typeparam>
        /// <typeparam name="R">The type used as the converter.</typeparam>
        /// <param name="reader">The JSON serialization of an instance of the target type.</param>
        /// <param name="converter">The converter used during deserialization.</param>
        /// <returns>The deserialized instance of the target type.</returns>
        public T Deserialize<T, R>(StreamReader reader, R converter)
            where T : class, new()
            where R : class
        {
            return this.adapter.Deserialize<T, R>(reader, converter);
        }

        /// <summary>
        /// Serialize an object of type T to a JSON string.
        /// </summary>
        /// <typeparam name="T">The type of the object to be serialized.</typeparam>
        /// <param name="forSerialization">An instance of the type to serialize.</param>
        /// <param name="sanitize">True if the output json should be sanitized.</param>
        /// <returns>The serialization of the instance to a JSON string.</returns>
        public string Serialize<T>(T forSerialization, bool sanitize) where T : class, new()
        {
            string serialized = this.adapter.Serialize<T>(forSerialization);
            if (sanitize)
            {
                serialized = this.adapter.SanitizeJsonString(serialized);
            }

            return serialized;
        }

        /// <summary>
        /// Serialize an object of type T to a JSON string.
        /// </summary>
        /// <typeparam name="T">The type of the object to be serialized.</typeparam>
        /// <typeparam name="R">The type used as the resolver.</typeparam>
        /// <param name="forSerialization">An instance of the type to serialize.</param>
        /// <param name="resolver">A class that is used for type resolution during serialization.</param>
        /// <param name="sanitize">True if the output json should be sanitized.</param>
        /// <returns>The serialization of the instance to a JSON string.</returns>
        public string Serialize<T, R>(T forSerialization, R resolver, bool sanitize)
            where T : class, new()
            where R : class
        {
            string serialized = this.adapter.Serialize<T, R>(forSerialization, resolver);
            if (sanitize)
            {
                serialized = this.adapter.SanitizeJsonString(serialized);
            }

            return serialized;
        }
    }
}
