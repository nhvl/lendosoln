﻿namespace LendersOffice.Drivers.HttpRequest
{
    using LqbGrammar;
    using LqbGrammar.Adapters;
    using LqbGrammar.Drivers.HttpRequest;

    /// <summary>
    /// Factory for creating drivers that carry out http communication.
    /// </summary>
    public sealed class HttpRequestDriverFactory : IHttpRequestDriverFactory
    {
        /// <summary>
        /// Create an implementation of the IHttpRequestDriver interface.
        /// </summary>
        /// <returns>An implementation of the IHttpRequestDriver interface.</returns>
        public IHttpRequestDriver Create()
        {
            var factory = GenericLocator<IHttpRequestAdapterFactory>.Factory;
            var adapter = factory.Create();

            return new HttpRequestDriver(adapter);
        }
    }
}
