﻿namespace LendersOffice.Drivers.HttpRequest
{
    using System;
    using System.IO;
    using System.Net;
    using System.Xml;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers.HttpRequest;

    /// <summary>
    /// Mock implementation of the IHttpRequestDriver interface.
    /// </summary>
    internal sealed class MockHttpRequestDriver : IHttpRequestDriver
    {
        /// <summary>
        /// The path where the fake download file can be retrieved.
        /// </summary>
        private string fakePath;

        /// <summary>
        /// The string valued response to return.
        /// </summary>
        private string fakeResponse;

        /// <summary>
        /// The xml document to return.
        /// </summary>
        private XmlDocument fakeXml;

        /// <summary>
        /// Initializes a new instance of the <see cref="MockHttpRequestDriver"/> class.
        /// </summary>
        /// <param name="data">Either the file path or response string.</param>
        /// <param name="isFilePath">True if data is a file path, false otherwise.</param>
        public MockHttpRequestDriver(string data, bool isFilePath)
        {
            if (isFilePath)
            {
                this.fakePath = data;
            }
            else
            {
                this.fakeResponse = data;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MockHttpRequestDriver"/> class.
        /// </summary>
        /// <param name="xmlDoc">The xml document to return.</param>
        public MockHttpRequestDriver(XmlDocument xmlDoc)
        {
            this.fakeXml = xmlDoc;
        }

        /// <summary>
        /// Carry out the communication with the indicated URL.
        /// </summary>
        /// <param name="url">The parameter is not used.</param>
        /// <param name="options">The options that control the request and response parsing.</param>
        /// <returns>True if the communication was successful, false otherwise.</returns>
        public bool ExecuteCommunication(LqbAbsoluteUri url, HttpRequestOptions options)
        {
            options.ResponseStatusCode = HttpStatusCode.OK;
            options.ResponseStatusDescription = "Success";

            if (options.ResponseFileName != null)
            {
                this.HandleFile(options.ResponseFileName.Value.Value);
            }
            else if (options.XmlResponseDelegate != null)
            {
                this.HandleXml(options.XmlResponseDelegate, options.XmlReaderSettings);
            }
            else
            {
                options.ResponseBody = this.GetText();
            }

            return true;
        }

        /// <summary>
        /// Write fake response to a file.
        /// </summary>
        /// <param name="path">Location of the output file.</param>
        private void HandleFile(string path)
        {
            if (!string.IsNullOrEmpty(this.fakePath))
            {
                File.Copy(this.fakePath, path);
            }
            else if (this.fakeXml != null)
            {
                this.fakeXml.Save(path);
            }
            else
            {
                using (var writer = File.CreateText(path))
                {
                    writer.Write(this.fakeResponse);
                }
            }
        }

        /// <summary>
        /// Send fake response to the client's xml handling code.
        /// </summary>
        /// <param name="handler">The client's xml handling code.</param>
        /// <param name="settings">Settings that affect how the xml is read.</param>
        private void HandleXml(Action<XmlReader> handler, XmlReaderSettings settings)
        {
            XmlDocument doc = null;
            if (!string.IsNullOrEmpty(this.fakePath))
            {
                doc = new XmlDocument();
                doc.Load(this.fakePath);
            }
            else if (this.fakeXml != null)
            {
                doc = this.fakeXml;
            }
            else
            {
                doc = new XmlDocument();
                doc.LoadXml(this.fakeResponse);
            }

            using (var outReader = new XmlNodeReader(this.fakeXml))
            {
                using (var reader = settings == null ? outReader : XmlReader.Create(outReader, settings))
                {
                    handler(reader);
                }
            }
        }

        /// <summary>
        /// Retrieve the fake response as a string.
        /// </summary>
        /// <returns>The fake response as a string.</returns>
        private string GetText()
        {
            if (!string.IsNullOrEmpty(this.fakePath))
            {
                using (var reader = File.OpenText(this.fakePath))
                {
                    return reader.ReadToEnd();
                }
            }
            else if (this.fakeXml != null)
            {
                return this.fakeXml.OuterXml;
            }
            else
            {
                return this.fakeResponse;
            }
        }
    }
}
