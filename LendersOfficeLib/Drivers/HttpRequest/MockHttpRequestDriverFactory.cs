﻿namespace LendersOffice.Drivers.HttpRequest
{
    using System;
    using System.Xml;
    using LqbGrammar;
    using LqbGrammar.Drivers.HttpRequest;

    /// <summary>
    /// Factory for creating a fake mock driver that implements the IHttpRequestDriver interface.
    /// </summary>
    public sealed class MockHttpRequestDriverFactory : IHttpRequestDriverFactory
    {
        /// <summary>
        /// The path where the fake download file can be retrieved.
        /// </summary>
        private string fakePath;

        /// <summary>
        /// The string valued response to return.
        /// </summary>
        private string fakeResponse;

        /// <summary>
        /// The xml document to return.
        /// </summary>
        private XmlDocument fakeXml;

        /// <summary>
        /// Initializes a new instance of the <see cref="MockHttpRequestDriverFactory"/> class.
        /// </summary>
        /// <param name="data">Either the file path or response string.</param>
        /// <param name="isFilePath">True if data is a file path, false otherwise.</param>
        public MockHttpRequestDriverFactory(string data, bool isFilePath)
        {
            if (isFilePath)
            {
                this.fakePath = data;
            }
            else
            {
                this.fakeResponse = data;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MockHttpRequestDriverFactory"/> class.
        /// </summary>
        /// <param name="xmlDoc">The xml document to return.</param>
        public MockHttpRequestDriverFactory(XmlDocument xmlDoc)
        {
            this.fakeXml = xmlDoc;
        }

        /// <summary>
        /// Create a mock driver.
        /// </summary>
        /// <returns>A mock driver.</returns>
        public IHttpRequestDriver Create()
        {
            if (this.fakePath != null)
            {
                return new MockHttpRequestDriver(this.fakePath, true);
            }
            else if (this.fakeResponse != null)
            {
                return new MockHttpRequestDriver(this.fakeResponse, false);
            }
            else
            {
                return new MockHttpRequestDriver(this.fakeXml);
            }
        }
    }
}
