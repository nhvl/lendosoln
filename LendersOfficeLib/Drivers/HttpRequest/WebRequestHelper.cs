﻿namespace LendersOffice.Drivers.HttpRequest
{
    using LqbGrammar;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers.HttpRequest;

    /// <summary>
    /// Semantic wrapper that invokes the driver that carries out web communication.
    /// </summary>
    public static class WebRequestHelper
    {
        /// <summary>
        /// Carry out the communication with the indicated URL.
        /// </summary>
        /// <param name="url">The target URL.</param>
        /// <param name="options">The options that control the request and response parsing.</param>
        /// <returns>True if the communication was successful, false otherwise.</returns>
        public static bool ExecuteCommunication(LqbAbsoluteUri url, HttpRequestOptions options)
        {
            var driver = GetDriver();
            return driver.ExecuteCommunication(url, options);
        }

        /// <summary>
        /// Retrieve the registered driver that implements the IHttpRequestDriver interface.
        /// </summary>
        /// <returns>The registered driver.</returns>
        private static IHttpRequestDriver GetDriver()
        {
            var factory = GenericLocator<IHttpRequestDriverFactory>.Factory;
            return factory.Create();
        }
    }
}
