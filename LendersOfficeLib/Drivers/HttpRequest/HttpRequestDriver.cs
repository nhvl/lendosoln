﻿namespace LendersOffice.Drivers.HttpRequest
{
    using LqbGrammar.Adapters;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers.HttpRequest;

    /// <summary>
    /// Driver for communicating with http servers.
    /// </summary>
    internal sealed class HttpRequestDriver : IHttpRequestDriver
    {
        /// <summary>
        /// Adapter that implements the http communication.
        /// </summary>
        private IHttpRequestAdapter adapter;

        /// <summary>
        /// Initializes a new instance of the <see cref="HttpRequestDriver"/> class.
        /// </summary>
        /// <param name="adapter">The adapter that impelments the http communication.</param>
        public HttpRequestDriver(IHttpRequestAdapter adapter)
        {
            this.adapter = adapter;
        }

        /// <summary>
        /// Carry out the communication with the indicated URL.
        /// </summary>
        /// <param name="url">The target URL.</param>
        /// <param name="options">The options that control the request and response parsing.</param>
        /// <returns>True if the communication was successful, false otherwise.</returns>
        public bool ExecuteCommunication(LqbAbsoluteUri url, HttpRequestOptions options)
        {
            return this.adapter.ExecuteCommunication(url, options);
        }
    }
}
