﻿namespace LendersOffice.Drivers.NetFramework
{
    using System;
    using LqbGrammar;
    using LqbGrammar.Drivers;

    /// <summary>
    /// Factory for creating instances of RegularExpressionDriver.
    /// </summary>
    public sealed class RegularExpressionDriverFactory : IRegularExpressionDriverFactory
    {
        /// <summary>
        /// Create an instance of RegularExpressionDriver.
        /// </summary>
        /// <returns>An instance of RegularExpressionDriver.</returns>
        public IRegularExpressionDriver Create()
        {
            return new RegularExpressionDriver();
        }
    }
}
