﻿namespace LendersOffice.Drivers.NetFramework
{
    using LqbGrammar;
    using LqbGrammar.Adapters;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;

    /// <summary>
    /// Driver for a regular expression utility.
    /// </summary>
    internal sealed class RegularExpressionDriver : IRegularExpressionDriver
    {
        /// <summary>
        /// Carry out a full-string match.
        /// </summary>
        /// <param name="regex">The regular expression to use for matching.</param>
        /// <param name="validationTarget">The string that will be matched against the regular expression.</param>
        /// <returns>True if string matches the regular expression, false otherwise.</returns>
        public bool IsMatch(RegularExpressionString regex, string validationTarget)
        {
            var factory = GenericLocator<IRegularExpressionAdapterFactory>.Factory;
            var adapter = factory.Create();
            return adapter.IsMatch(regex, validationTarget);
        }

        /// <summary>
        /// In a specified input string, replaces all strings that match a specified regular expression with a specified replacement string.
        /// </summary>
        /// <param name="regex">The regular expression.</param>
        /// <param name="input">The string to search for a match.</param>
        /// <param name="replacement">The replacement string.</param>
        /// <returns>
        /// A new string that is identical to the input string, except that the replacement string takes the place of each matched string.
        /// If pattern is not matched in the current instance, the method returns the current instance unchanged.
        /// </returns>
        public string Replace(RegularExpressionString regex, string input, string replacement)
        {
            var factory = GenericLocator<IRegularExpressionAdapterFactory>.Factory;
            var adapter = factory.Create();
            return adapter.Replace(regex, input, replacement);
        }
    }
}
