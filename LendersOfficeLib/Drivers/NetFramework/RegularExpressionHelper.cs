﻿namespace LendersOffice.Drivers.NetFramework
{
    using LqbGrammar;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;

    /// <summary>
    /// Utility for interacting with the regular expression driver.
    /// </summary>
    public static class RegularExpressionHelper
    {
        /// <summary>
        /// Since this class is invoked from within loops, cache the driver so we don't wind up with
        /// a large number of object creations.
        /// </summary>
        private static IRegularExpressionDriver driver;

        /// <summary>
        /// Initializes static members of the <see cref="RegularExpressionHelper" /> class.
        /// </summary>
        static RegularExpressionHelper()
        {
            // Lifetime management would normally be the responsibility of the factory class.  However, this 
            // scenario is a bit different.  There is nothing inherent about the regular expression driver that 
            // would require special handling.  It is the particular usage of the driver in the client code 
            // that is driving the decision to use a singleton.  Since this helper class is designed to 
            // cater to client code, it seems reasonable to put the singleton here.
            var factory = GenericLocator<IRegularExpressionDriverFactory>.Factory;
            driver = factory.Create();
        }

        /// <summary>
        /// Carry out a full-string match.
        /// </summary>
        /// <param name="regex">The regular expression to use for matching.</param>
        /// <param name="validationTarget">The string that will be matched against the regular expression.</param>
        /// <returns>True if string matches the regular expression, false otherwise.</returns>
        public static bool IsMatch(RegularExpressionString regex, string validationTarget)
        {
            return driver.IsMatch(regex, validationTarget);
        }

        /// <summary>
        /// In a specified input string, replaces all strings that match a specified regular expression with a specified replacement string.
        /// </summary>
        /// <param name="regex">The regular expression.</param>
        /// <param name="input">The string to search for a match.</param>
        /// <param name="replacement">The replacement string.</param>
        /// <returns>
        /// A new string that is identical to the input string, except that the replacement string takes the place of each matched string.
        /// If pattern is not matched in the current instance, the method returns the current instance unchanged.
        /// </returns>
        public static string Replace(RegularExpressionString regex, string input, string replacement)
        {
            return driver.Replace(regex, input, replacement);
        }
    }
}
