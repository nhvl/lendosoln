﻿namespace LendersOffice.Drivers.FileSystem
{
    using System;
    using LqbGrammar;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers.FileSystem;

    /// <summary>
    /// Semantic wrapper class that invokes the binary file driver.
    /// </summary>
    public static class BinaryFileHelper
    {
        /// <summary>
        /// Create or overwrite a binary file.
        /// </summary>
        /// <param name="path">The file to create or overwrite.</param>
        /// <param name="writeHandler">Delegate that writes data to the file.</param>
        public static void OpenNew(LocalFilePath path, Action<LqbBinaryStream> writeHandler)
        {
            var driver = GetDriver();
            driver.OpenNew(path, writeHandler);
        }

        /// <summary>
        /// Open an existing binary file for reading.
        /// </summary>
        /// <param name="path">The file to open.</param>
        /// <param name="readHandler">Delegate that reads data from the file.</param>
        public static void OpenRead(LocalFilePath path, Action<LqbBinaryStream> readHandler)
        {
            var driver = GetDriver();
            driver.OpenRead(path, readHandler);
        }

        /// <summary>
        /// Read all the data within a binary file.
        /// </summary>
        /// <param name="path">The binary file from which the data will be read.</param>
        /// <returns>The data read from the binary file.</returns>
        public static byte[] ReadAllBytes(LocalFilePath path)
        {
            var driver = GetDriver();
            return driver.ReadAllBytes(path);
        }

        /// <summary>
        /// Creates a new file, writes the specified byte array to the file,
        /// and then closes the file.  If the target file already exists, it is overwritten.
        /// </summary>
        /// <param name="path">The file to which the data is written.</param>
        /// <param name="bytes">The data that is written to the file.</param>
        public static void WriteAllBytes(LocalFilePath path, byte[] bytes)
        {
            var driver = GetDriver();
            driver.WriteAllBytes(path, bytes);
        }

        /// <summary>
        /// Retrieve the driver from the service locator.
        /// </summary>
        /// <returns>The binary file driver.</returns>
        private static IBinaryFileDriver GetDriver()
        {
            var factory = GenericLocator<IFileSystemDriverFactory>.Factory;
            return factory.CreateBinaryFileDriver();
        }
    }
}
