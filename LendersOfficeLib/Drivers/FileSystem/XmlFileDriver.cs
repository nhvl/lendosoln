﻿namespace LendersOffice.Drivers.FileSystem
{
    using System;
    using LqbGrammar.Adapters;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers.FileSystem;

    /// <summary>
    /// Simple implementation of the IXmlFileDriver interface.
    /// </summary>
    internal sealed class XmlFileDriver : IXmlFileDriver
    {
        /// <summary>
        /// The adapter used to do the work.
        /// </summary>
        private IXmlFileAdapter adapter;

        /// <summary>
        /// Initializes a new instance of the <see cref="XmlFileDriver"/> class.
        /// </summary>
        /// <param name="adapter">The adapter used to do the work.</param>
        public XmlFileDriver(IXmlFileAdapter adapter)
        {
            this.adapter = adapter;
        }

        /// <summary>
        /// Read a file containing XML.
        /// </summary>
        /// <param name="path">The file to read.</param>
        /// <returns>The XML data.</returns>
        public LqbXmlElement Load(LocalFilePath path)
        {
            return this.adapter.Load(path);
        }

        /// <summary>
        /// Write XML data to a file.
        /// </summary>
        /// <param name="path">The file to which the XML is to be written.</param>
        /// <param name="xml">The XML data.</param>
        public void Save(LocalFilePath path, LqbXmlElement xml)
        {
            this.adapter.Save(path, xml);
        }
    }
}
