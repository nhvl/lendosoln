﻿namespace LendersOffice.Drivers.FileSystem
{
    using System;
    using System.Collections.Generic;
    using LqbGrammar;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers.FileSystem;

    /// <summary>
    /// Semantic wrapper class that invokes the text file driver.
    /// </summary>
    public static class TextFileHelper
    {
        /// <summary>
        /// Opens the file, appends the string to the file, and then closes the file.
        /// If the file does not exist, this method creates a file, writes the specified
        /// string to the file, then closes the file.
        /// </summary>
        /// <param name="path">The file to which the text is appended.</param>
        /// <param name="text">The text that is appended to the file.</param>
        public static void AppendString(LocalFilePath path, string text)
        {
            var driver = GetDriver();
            driver.AppendString(path, text);
        }

        /// <summary>
        /// Open an existing text file in append mode, or create a new file if it
        /// does not exist, for writing.
        /// </summary>
        /// <param name="path">The file to which text can be appended.</param>
        /// <param name="writeHandler">Delegate that handles writing text to the file.</param>
        public static void OpenForAppend(LocalFilePath path, Action<LqbTextFileWriter> writeHandler)
        {
            var driver = GetDriver();
            driver.OpenForAppend(path, writeHandler);
        }

        /// <summary>
        /// Create a new text file if it doesn't exist, or overwrite an existing text file.
        /// </summary>
        /// <param name="path">The file to which text can be written.</param>
        /// <param name="writeHandler">Delegate that handles writing text to the file.</param>
        public static void OpenNew(LocalFilePath path, Action<LqbTextFileWriter> writeHandler)
        {
            var driver = GetDriver();
            driver.OpenNew(path, writeHandler);
        }

        /// <summary>
        /// Open a text file for reading.
        /// </summary>
        /// <param name="path">The file that is available for reading.</param>
        /// <param name="readHandler">Delegate the reads the file.</param>
        public static void OpenRead(LocalFilePath path, Action<LqbTextFileReader> readHandler)
        {
            var driver = GetDriver();
            driver.OpenRead(path, readHandler);
        }

        /// <summary>
        /// Open, read all the contents, then close a text file.
        /// </summary>
        /// <param name="path">The file that is to be read.</param>
        /// <returns>The file contents.</returns>
        public static string ReadFile(LocalFilePath path)
        {
            var driver = GetDriver();
            return driver.ReadFile(path);
        }

        /// <summary>
        /// Open, read all contents as a set of lines, then close a text file.
        /// Note that this method yields lines before they are all in memory
        /// so it is efficient for large files.
        /// </summary>
        /// <param name="path">The file that is to be read.</param>
        /// <returns>The file contents as a set of lines.</returns>
        public static IEnumerable<string> ReadLines(LocalFilePath path)
        {
            var driver = GetDriver();
            return driver.ReadLines(path);
        }

        /// <summary>
        /// Creates a new text file if it doesn't exist, or overwrite an existing file,
        /// with the specified text and then closes the file.
        /// </summary>
        /// <param name="path">The file to which the text is written.</param>
        /// <param name="text">The text that is written to the file.</param>
        /// <param name="writeBOM">True if the a byte order mark should be included at the beginning of the file.</param>
        public static void WriteString(LocalFilePath path, string text, bool writeBOM)
        {
            var driver = GetDriver();
            driver.WriteString(path, text, writeBOM);
        }

        /// <summary>
        /// Retrieve the driver from the service locator.
        /// </summary>
        /// <returns>The text file driver.</returns>
        private static ITextFileDriver GetDriver()
        {
            var factory = GenericLocator<IFileSystemDriverFactory>.Factory;
            return factory.CreateTextFileDriver();
        }
    }
}
