﻿namespace LendersOffice.Drivers.FileSystem
{
    using System;
    using LqbGrammar;
    using LqbGrammar.Adapters;
    using LqbGrammar.Drivers.FileSystem;

    /// <summary>
    /// Factory for creating drivers to work with local files.
    /// </summary>
    public sealed class FileSystemDriverFactory : IFileSystemDriverFactory
    {
        /// <summary>
        /// Create a driver used to work with binary files.
        /// </summary>
        /// <returns>A binary file driver.</returns>
        public IBinaryFileDriver CreateBinaryFileDriver()
        {
            var factory = GenericLocator<IFileSystemAdapterFactory>.Factory;
            var adapter = factory.CreateBinaryFileAdapter();

            return new BinaryFileDriver(adapter);
        }

        /// <summary>
        /// Create a driver used to carry out file operations.
        /// </summary>
        /// <returns>A file operation driver.</returns>
        public IFileOperationDriver CreateFileOperationDriver()
        {
            var factory = GenericLocator<IFileSystemAdapterFactory>.Factory;
            var adapter = factory.CreateFileOperationAdapter();

            return new FileOperationDriver(adapter);
        }

        /// <summary>
        /// Create a driver used to work with text files.
        /// </summary>
        /// <returns>A text file driver.</returns>
        public ITextFileDriver CreateTextFileDriver()
        {
            var factory = GenericLocator<IFileSystemAdapterFactory>.Factory;
            var adapter = factory.CreateTextFileAdapter();

            return new TextFileDriver(adapter);
        }

        /// <summary>
        /// Create a driver used to work with XML files.
        /// </summary>
        /// <returns>An XML file driver.</returns>
        public IXmlFileDriver CreateXmlFileDriver()
        {
            var factory = GenericLocator<IFileSystemAdapterFactory>.Factory;
            var adapter = factory.CreateXmlFileAdapter();

            return new XmlFileDriver(adapter);
        }
    }
}
