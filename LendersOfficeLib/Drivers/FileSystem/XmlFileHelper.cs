﻿namespace LendersOffice.Drivers.FileSystem
{
    using LqbGrammar;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers.FileSystem;

    /// <summary>
    /// Semantic wrapper class that invokes the xml file driver.
    /// </summary>
    public static class XmlFileHelper
    {
        /// <summary>
        /// Read a file containing XML.
        /// </summary>
        /// <param name="path">The file to read.</param>
        /// <returns>The XML data.</returns>
        public static LqbXmlElement Load(LocalFilePath path)
        {
            var driver = GetDriver();
            return driver.Load(path);
        }

        /// <summary>
        /// Write XML data to a file.
        /// </summary>
        /// <param name="path">The file to which the XML is to be written.</param>
        /// <param name="xml">The XML data.</param>
        public static void Save(LocalFilePath path, LqbXmlElement xml)
        {
            var driver = GetDriver();
            driver.Save(path, xml);
        }

        /// <summary>
        /// Retrieve the driver from the service locator.
        /// </summary>
        /// <returns>The xml file driver.</returns>
        private static IXmlFileDriver GetDriver()
        {
            var factory = GenericLocator<IFileSystemDriverFactory>.Factory;
            return factory.CreateXmlFileDriver();
        }
    }
}
