﻿namespace LendersOffice.Drivers.FileSystem
{
    using System;
    using LqbGrammar.Adapters;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers.FileSystem;

    /// <summary>
    /// Simple implementation of the IBinaryFileDriver interface.
    /// </summary>
    internal sealed class BinaryFileDriver : IBinaryFileDriver
    {
        /// <summary>
        /// Adapter used to do the work.
        /// </summary>
        private IBinaryFileAdapter adapter;

        /// <summary>
        /// Initializes a new instance of the <see cref="BinaryFileDriver"/> class.
        /// </summary>
        /// <param name="adapter">Adapter used to do the work.</param>
        public BinaryFileDriver(IBinaryFileAdapter adapter)
        {
            this.adapter = adapter;
        }

        /// <summary>
        /// Create or overwrite a binary file.
        /// </summary>
        /// <param name="path">The file to create or overwrite.</param>
        /// <param name="writeHandler">Delegate that writes data to the file.</param>
        public void OpenNew(LocalFilePath path, Action<LqbBinaryStream> writeHandler)
        {
            this.adapter.OpenNew(path, writeHandler);
        }

        /// <summary>
        /// Open an existing binary file for reading.
        /// </summary>
        /// <param name="path">The file to open.</param>
        /// <param name="readHandler">Delegate that reads data from the file.</param>
        public void OpenRead(LocalFilePath path, Action<LqbBinaryStream> readHandler)
        {
            this.adapter.OpenRead(path, readHandler);
        }

        /// <summary>
        /// Read all the data within a binary file.
        /// </summary>
        /// <param name="path">The binary file from which the data will be read.</param>
        /// <returns>The data read from the binary file.</returns>
        public byte[] ReadAllBytes(LocalFilePath path)
        {
            return this.adapter.ReadAllBytes(path);
        }

        /// <summary>
        /// Creates a new file, writes the specified byte array to the file,
        /// and then closes the file.  If the target file already exists, it is overwritten.
        /// </summary>
        /// <param name="path">The file to which the data is written.</param>
        /// <param name="bytes">The data that is written to the file.</param>
        public void WriteAllBytes(LocalFilePath path, byte[] bytes)
        {
            this.adapter.WriteAllBytes(path, bytes);
        }
    }
}
