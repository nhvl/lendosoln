﻿namespace LendersOffice.Drivers.FileSystem
{
    using System;
    using LqbGrammar;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers.FileSystem;

    /// <summary>
    /// Semantic wrapper class that invokes the file operation driver.
    /// </summary>
    public static class FileOperationHelper
    {
        /// <summary>
        /// Copy the source file to the target file.
        /// </summary>
        /// <param name="source">The source file.</param>
        /// <param name="target">The target file.</param>
        /// <param name="allowOverwrite">True if overwriting an existing file is allowed, false otherwise.</param>
        public static void Copy(LocalFilePath source, LocalFilePath target, bool allowOverwrite)
        {
            var driver = GetDriver();
            driver.Copy(source, target, allowOverwrite);
        }

        /// <summary>
        /// Deletes the specified file.
        /// </summary>
        /// <param name="path">The file to delete.</param>
        public static void Delete(LocalFilePath path)
        {
            var driver = GetDriver();
            driver.Delete(path);
        }

        /// <summary>
        /// Detects whether or not a specified file exists.
        /// </summary>
        /// <param name="path">The file to check.</param>
        /// <returns>True if the file exists, false otherwise.</returns>
        public static bool Exists(LocalFilePath path)
        {
            var driver = GetDriver();
            return driver.Exists(path);
        }

        /// <summary>
        /// Retrieve the timestamp when the file was last modified.
        /// </summary>
        /// <param name="path">The specified file.</param>
        /// <returns>The timestamp when the file was last modified.</returns>
        public static LqbEventDate GetLastWriteTime(LocalFilePath path)
        {
            var driver = GetDriver();
            return driver.GetLastWriteTime(path);
        }

        /// <summary>
        /// Move the source file to the target location if the target doesn't already exist.
        /// If the target exists an exception will be thrown, the Copy and Delete methods 
        /// must be used instead.
        /// </summary>
        /// <param name="source">The file that will be moved.</param>
        /// <param name="target">The location to which the file will be moved.</param>
        public static void Move(LocalFilePath source, LocalFilePath target)
        {
            var driver = GetDriver();
            driver.Move(source, target);
        }

        /// <summary>
        /// Set the last write time propertry for the specified file.
        /// </summary>
        /// <param name="path">The specified file.</param>
        /// <param name="timestamp">The value that is to be set as the last write time.</param>
        public static void SetLastWriteTime(LocalFilePath path, LqbEventDate timestamp)
        {
            var driver = GetDriver();
            driver.SetLastWriteTime(path, timestamp);
        }

        /// <summary>
        /// Ensure the specified file is not read-only.
        /// </summary>
        /// <param name="path">The specified file.</param>
        public static void TurnOffReadOnly(LocalFilePath path)
        {
            var driver = GetDriver();
            driver.TurnOffReadOnly(path);
        }

        /// <summary>
        /// Retrieve the driver from the service locator.
        /// </summary>
        /// <returns>The binary file driver.</returns>
        private static IFileOperationDriver GetDriver()
        {
            var factory = GenericLocator<IFileSystemDriverFactory>.Factory;
            return factory.CreateFileOperationDriver();
        }
    }
}
