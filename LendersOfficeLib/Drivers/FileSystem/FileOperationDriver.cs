﻿namespace LendersOffice.Drivers.FileSystem
{
    using System;
    using LqbGrammar.Adapters;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers.FileSystem;

    /// <summary>
    /// Simplest implementation of the IFileOperationDriver interface.
    /// </summary>
    internal sealed class FileOperationDriver : IFileOperationDriver
    {
        /// <summary>
        /// Adapter that implements the file operations.
        /// </summary>
        private IFileOperationAdapter adapter;

        /// <summary>
        /// Initializes a new instance of the <see cref="FileOperationDriver"/> class.
        /// </summary>
        /// <param name="adapter">Adapter that does the work.</param>
        public FileOperationDriver(IFileOperationAdapter adapter)
        {
            this.adapter = adapter;
        }

        /// <summary>
        /// Copy the source file to the target file.
        /// </summary>
        /// <param name="source">The source file.</param>
        /// <param name="target">The target file.</param>
        /// <param name="allowOverwrite">True if overwriting an existing file is allowed, false otherwise.</param>
        public void Copy(LocalFilePath source, LocalFilePath target, bool allowOverwrite)
        {
            this.adapter.Copy(source, target, allowOverwrite);
        }

        /// <summary>
        /// Deletes the specified file.
        /// </summary>
        /// <param name="path">The file to delete.</param>
        public void Delete(LocalFilePath path)
        {
            this.adapter.Delete(path);
        }

        /// <summary>
        /// Detects whether or not a specified file exists.
        /// </summary>
        /// <param name="path">The file to check.</param>
        /// <returns>True if the file exists, false otherwise.</returns>
        public bool Exists(LocalFilePath path)
        {
            return this.adapter.Exists(path);
        }

        /// <summary>
        /// Retrieve the timestamp when the file was last modified.
        /// </summary>
        /// <param name="path">The specified file.</param>
        /// <returns>The timestamp when the file was last modified.</returns>
        public LqbEventDate GetLastWriteTime(LocalFilePath path)
        {
            return this.adapter.GetLastWriteTime(path);
        }

        /// <summary>
        /// Move the source file to the target location if the target doesn't already exist.
        /// If the target exists an exception will be thrown, the Copy and Delete methods 
        /// must be used instead.
        /// </summary>
        /// <param name="source">The file that will be moved.</param>
        /// <param name="target">The location to which the file will be moved.</param>
        public void Move(LocalFilePath source, LocalFilePath target)
        {
            this.adapter.Move(source, target);
        }

        /// <summary>
        /// Set the last write time propertry for the specified file.
        /// </summary>
        /// <param name="path">The specified file.</param>
        /// <param name="timestamp">The value that is to be set as the last write time.</param>
        public void SetLastWriteTime(LocalFilePath path, LqbEventDate timestamp)
        {
            this.adapter.SetLastWriteTime(path, timestamp);
        }

        /// <summary>
        /// Ensure the specified file is not read-only.
        /// </summary>
        /// <param name="path">The specified file.</param>
        public void TurnOffReadOnly(LocalFilePath path)
        {
            this.adapter.TurnOffReadOnly(path);
        }
    }
}
