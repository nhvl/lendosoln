﻿namespace LendersOffice.Drivers.MethodInvoke
{
    using System;
    using LendersOffice.Constants;
    using LqbGrammar.Adapters;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;

    /// <summary>
    /// Driver used to execute a method in a potentially distributed fashion.
    /// </summary>
    internal sealed class MethodInvokeDriver : IMethodInvokeDriver
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MethodInvokeDriver"/> class.
        /// </summary>
        /// <param name="adapter">Adapter used to carry out the method call.</param>
        public MethodInvokeDriver(IMethodInvokeAdapter adapter)
        {
            this.Adapter = adapter;

            string key = adapter.ControlInformationKey;
            if (!string.IsNullOrEmpty(key))
            {
                string control = ConstStage.MethodInvokeControl(key);
                adapter.SetControlInformation(control);
            }
        }

        /// <summary>
        /// Gets or sets the adapter used to carry out the method call.
        /// </summary>
        /// <value>The adapter used to carry out the method call.</value>
        private IMethodInvokeAdapter Adapter { get; set; }

        /// <summary>
        /// Determine whether or not the call will be made directly or done on a different thread.
        /// This will allow the client code to make efficient calls when a direct call is made.
        /// </summary>
        /// <returns>True if the method will be called using a different thread, rather than directly.</returns>
        public bool InvokeIsOffThread()
        {
            return this.Adapter.InvokeIsOffThread();
        }

        /// <summary>
        /// Execute a method that accepts an argument of type T and returns void.
        /// </summary>
        /// <typeparam name="T">Type of the class that contains the method.</typeparam>
        /// <typeparam name="S">Type of the argument to the method.</typeparam>
        /// <param name="call">The method that will invoked.</param>
        /// <param name="previousCallId">An identifier that links this call to the end of a previous call, or null.</param>
        /// <returns>A job identifier, or null if there is no identifier.</returns>
        public MethodInvokeIdentifier Run<T, S>(SingleArgumentMethod<T, S> call, MethodInvokeIdentifier previousCallId) where T : new()
        {
            return this.Adapter.Run(call, previousCallId);
        }

        /// <summary>
        /// After waiting for a delay time, execute a method that accepts an argument of type S and returns void.
        /// </summary>
        /// <typeparam name="T">Type of the class that contains the method.</typeparam>
        /// <typeparam name="S">Type of the argument to the method.</typeparam>
        /// <param name="call">The method that will get invoked.</param>
        /// <param name="delay">The delay time to wait prior to executing the method.</param>
        /// <returns>A job identifier, or null if there is no identifier.</returns>
        public MethodInvokeIdentifier RunAfterDelay<T, S>(SingleArgumentMethod<T, S> call, TimeoutInSeconds delay) where T : new()
        {
            return this.Adapter.RunAfterDelay(call, delay);
        }
    }
}
