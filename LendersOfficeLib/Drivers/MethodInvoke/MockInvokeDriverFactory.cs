﻿namespace LendersOffice.Drivers.MethodInvoke
{
    using LqbGrammar.Drivers;

    /// <summary>
    /// Factory for creation of mock method invocation driver.
    /// </summary>
    public sealed class MockInvokeDriverFactory : IMethodInvokeDriverFactory
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MockInvokeDriverFactory"/> class.
        /// </summary>
        /// <param name="makeDirectCall">If true, the method will be invoked directly, else it won't be invoked at all.</param>
        public MockInvokeDriverFactory(bool makeDirectCall)
        {
            this.DirectCall = makeDirectCall;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the method should be called directly or ignored.
        /// </summary>
        private bool DirectCall { get; set; }

        /// <summary>
        /// Create an implementation of the IMethodInvokeDriver interface.
        /// </summary>
        /// <returns>An implementation of the IMethodInvokeDriver interface.</returns>
        public IMethodInvokeDriver Create()
        {
            return new MockInvokeDriver(this.DirectCall);
        }
    }
}
