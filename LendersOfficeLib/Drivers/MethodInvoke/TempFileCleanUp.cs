﻿namespace LendersOffice.Drivers.MethodInvoke
{
    using System;
    using DataAccess;
    using LqbGrammar.Drivers;

    /// <summary>
    /// Delete a file in the temporary database, which can be performed
    /// within a method invoke infrastructure.  Normally this class will
    /// be used to perform clean up tasks as part of the method invoke infrastructure.
    /// </summary>
    public sealed class TempFileCleanUp : IMethodInvokeClass<TempFileCleanUp, string>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TempFileCleanUp"/> class.
        /// </summary>
        public TempFileCleanUp()
        {
            // Constructor used by the method invocation framework.
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TempFileCleanUp"/> class.
        /// </summary>
        /// <param name="tempFileKey">The filedb file's key.</param>
        public TempFileCleanUp(string tempFileKey)
        {
            this.FileKey = tempFileKey;
        }

        /// <summary>
        /// Gets or sets the filedb file's key.
        /// </summary>
        /// <value>The filedb file's key.</value>
        private string FileKey { get; set; }

        /// <summary>
        /// Delete a file in the temporary filedb repository.
        /// </summary>
        /// <param name="tempFileKey">The filedb file's key.</param>
        public void DeleteFile(string tempFileKey)
        {
            if (!string.IsNullOrEmpty(tempFileKey))
            {
                FileDBTools.Delete(E_FileDB.Temp, tempFileKey);
            }
        }

        /// <summary>
        /// Prepare for a streamlined method call.
        /// </summary>
        /// <returns>The streamlined method call.</returns>
        public SingleArgumentMethod<TempFileCleanUp, string> GetMethod()
        {
            return new SingleArgumentMethod<TempFileCleanUp, string>(tfc => tfc.DeleteFile(this.FileKey), this.FileKey);
        }
    }
}
