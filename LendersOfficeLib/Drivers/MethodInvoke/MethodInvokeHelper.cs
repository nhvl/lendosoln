﻿namespace LendersOffice.Drivers.MethodInvoke
{
    using System;
    using LqbGrammar;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;

    /// <summary>
    /// Utility class to centralize usage of the IMethodInvokeDriver.
    /// </summary>
    public static class MethodInvokeHelper
    {
        /// <summary>
        /// Initializes static members of the <see cref="MethodInvokeHelper" /> class.
        /// </summary>
        static MethodInvokeHelper()
        {
            // Since multiple objects are created to decide the algorithm
            // used to do method invokes (decided by the registered adapter),
            // we will cache the value.
            var factory = GenericLocator<IMethodInvokeDriverFactory>.Factory;
            var driver = factory.Create();
            ApplicationUsesOffThreadInvocations = driver.InvokeIsOffThread();
        }

        /// <summary>
        /// Gets or sets a value indicating whether the application uses off-thread method invocation.
        /// </summary>
        /// <value>A value indicating whether the application uses off-thread method invocation.</value>
        private static bool ApplicationUsesOffThreadInvocations { get; set; }

        /// <summary>
        /// Execute a method that takes a single argument.
        /// </summary>
        /// <typeparam name="T">The type of class containing the method.</typeparam>
        /// <typeparam name="S">The type of the argument.</typeparam>
        /// <param name="hostClass">The class that hosts the method that will be invoked.</param>
        /// <param name="previousCallId">An identifier that links this call to the end of a previous call, or null.</param>
        /// <returns>A tuple with the Hangfire job identifier and the method arguement.</returns>
        public static Tuple<MethodInvokeIdentifier, S> Run<T, S>(IMethodInvokeClass<T, S> hostClass, MethodInvokeIdentifier previousCallId) where T : new()
        {
            var factory = GenericLocator<IMethodInvokeDriverFactory>.Factory;
            var driver = factory.Create();

            var method = hostClass.GetMethod();
            var id = driver.Run(method, previousCallId);
            return new Tuple<MethodInvokeIdentifier, S>(id, method.Argument);
        }

        /// <summary>
        /// After waiting for a delay time, execute a method that accepts an argument of type S and returns void.
        /// </summary>
        /// <typeparam name="T">Type of the class that contains the method.</typeparam>
        /// <typeparam name="S">Type of the argument to the method.</typeparam>
        /// <param name="hostClass">The class that hosts the method that will be invoked.</param>
        /// <param name="delay">The delay time to wait prior to executing the method.</param>
        /// <returns>A job identifier, or null if there is no identifier.</returns>
        public static MethodInvokeIdentifier RunAfterDelay<T, S>(IMethodInvokeClass<T, S> hostClass, TimeoutInSeconds delay) where T : new()
        {
            var factory = GenericLocator<IMethodInvokeDriverFactory>.Factory;
            var driver = factory.Create();

            var method = hostClass.GetMethod();
            return driver.RunAfterDelay(method, delay);
        }

        /// <summary>
        /// Determine whether or not the call will be made directly or done on a different thread.
        /// This will allow the client code to make efficient calls when a direct call is made.
        /// </summary>
        /// <returns>True if the method will be called using a different thread, rather than directly.</returns>
        public static bool InvokeIsOffThread()
        {
            return ApplicationUsesOffThreadInvocations;
        }
    }
}
