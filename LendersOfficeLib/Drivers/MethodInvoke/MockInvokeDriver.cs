﻿namespace LendersOffice.Drivers.MethodInvoke
{
    using System;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;

    /// <summary>
    /// Mock used to handle method invocation.
    /// </summary>
    internal sealed class MockInvokeDriver : IMethodInvokeDriver
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MockInvokeDriver"/> class.
        /// </summary>
        /// <param name="makeDirectCall">If true, the method will be invoked directly, else it won't be invoked at all.</param>
        public MockInvokeDriver(bool makeDirectCall)
        {
            this.DirectCall = makeDirectCall;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the method should be called directly or ignored.
        /// </summary>
        private bool DirectCall { get; set; }

        /// <summary>
        /// Determine whether or not the call will be made directly or done on a different thread.
        /// This will allow the client code to make efficient calls when a direct call is made.
        /// </summary>
        /// <returns>True if the method will be called using a different thread, rather than directly.</returns>
        public bool InvokeIsOffThread()
        {
            return false;
        }

        /// <summary>
        /// Execute a method that accepts an argument of type S and returns void.
        /// </summary>
        /// <typeparam name="T">Type of the class that contains the method.</typeparam>
        /// <typeparam name="S">Type of the argument to the method.</typeparam>
        /// <param name="call">The method that will invoked.</param>
        /// <param name="previousCallId">The parameter is not used.</param>
        /// <returns>A job identifier, or null if there is no identifier.</returns>
        public MethodInvokeIdentifier Run<T, S>(SingleArgumentMethod<T, S> call, MethodInvokeIdentifier previousCallId) where T : new()
        {
            if (this.DirectCall)
            {
                SingleArgumentMethod<T, S>.Execute(call);
            }

            return null;
        }

        /// <summary>
        /// After waiting for a delay time, execute a method that accepts an argument of type S and returns void.
        /// </summary>
        /// <typeparam name="T">Type of the class that contains the method.</typeparam>
        /// <typeparam name="S">Type of the argument to the method.</typeparam>
        /// <param name="call">The method that will get invoked.</param>
        /// <param name="delay">The delay time to wait prior to executing the method.</param>
        /// <returns>A job identifier, or null if there is no identifier.</returns>
        public MethodInvokeIdentifier RunAfterDelay<T, S>(SingleArgumentMethod<T, S> call, TimeoutInSeconds delay) where T : new()
        {
            System.Threading.Thread.Sleep(TimeSpan.FromSeconds(delay.Value));
            if (this.DirectCall)
            {
                SingleArgumentMethod<T, S>.Execute(call);
            }

            return null;
        }
    }
}
