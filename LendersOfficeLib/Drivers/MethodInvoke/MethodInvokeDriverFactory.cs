﻿namespace LendersOffice.Drivers.MethodInvoke
{
    using LqbGrammar;
    using LqbGrammar.Adapters;
    using LqbGrammar.Drivers;

    /// <summary>
    /// Factory for creating a drivers that execute methods.
    /// </summary>
    public sealed class MethodInvokeDriverFactory : IMethodInvokeDriverFactory
    {
        /// <summary>
        /// Create an implementation of the IMethodInvokeDriver interface.
        /// </summary>
        /// <returns>An implementation of the IMethodInvokeDriver interface.</returns>
        public IMethodInvokeDriver Create()
        {
            var factory = GenericLocator<IMethodInvokeAdapterFactory>.Factory;
            var adapter = factory.Create();

            return new MethodInvokeDriver(adapter);
        }
    }
}
