﻿namespace LendersOffice.Drivers.SqlServerDB
{
    using System;
    using System.Data;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;

    /// <summary>
    /// Factory for creating a mock that can handle multiple stored procedure calls.
    /// </summary>
    public sealed class MockSprocMultiCallDriverFactory : IStoredProcedureDriverFactory
    {
        /// <summary>
        /// Keeps track of which call in the sequence is being made.
        /// </summary>
        private int index;

        /// <summary>
        /// The sequence of drivers to use for the stored procedure calls.
        /// </summary>
        private MockStoredProcedureDriver[] drivers;

        /// <summary>
        /// Initializes a new instance of the <see cref="MockSprocMultiCallDriverFactory"/> class.
        /// </summary>
        /// <param name="callReturnDataItems">The sequence of return data from the stored procedure calls.</param>
        public MockSprocMultiCallDriverFactory(params object[] callReturnDataItems)
        {
            this.drivers = new MockStoredProcedureDriver[callReturnDataItems.Length];
            for (int i = 0; i < callReturnDataItems.Length; ++i)
            {
                object item = callReturnDataItems[i];
                if (item is ModifiedRowCount)
                {
                    var data = (ModifiedRowCount)item;
                    var factory = new MockStoredProcedureDriverFactory(data);
                    this.drivers[i] = (MockStoredProcedureDriver)factory.Create(TimeoutInSeconds.Default);
                }
                else if (item is DataSet)
                {
                    var data = (DataSet)item;
                    var factory = new MockStoredProcedureDriverFactory(data);
                    this.drivers[i] = (MockStoredProcedureDriver)factory.Create(TimeoutInSeconds.Default);
                }
                else if (item is IDataReader)
                {
                    var data = (IDataReader)item;
                    var factory = new MockStoredProcedureDriverFactory(data);
                    this.drivers[i] = (MockStoredProcedureDriver)factory.Create(TimeoutInSeconds.Default);
                }
                else
                {
                    var factory = new MockStoredProcedureDriverFactory(item);
                    this.drivers[i] = (MockStoredProcedureDriver)factory.Create(TimeoutInSeconds.Default);
                }
            }
        }

        /// <summary>
        /// Create and return an implementation of IStoredProcedureDriver.
        /// </summary>
        /// <param name="timeout">The timeout to use when communicating with a database.</param>
        /// <returns>An implementation of IStoredProcedureDriver.</returns>
        public IStoredProcedureDriver Create(TimeoutInSeconds timeout)
        {
            return this.drivers[this.index++];
        }

        /// <summary>
        /// Retrieve the driver that was used for a particular call in the sequence.
        /// </summary>
        /// <param name="index">The sequence index for the driver.</param>
        /// <returns>The driver that was used for the sequence index.</returns>
        public MockStoredProcedureDriver GetCallDriver(int index)
        {
            return this.drivers[index];
        }
    }
}
