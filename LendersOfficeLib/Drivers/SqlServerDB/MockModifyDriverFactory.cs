﻿namespace LendersOffice.Drivers.SqlServerDB
{
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;

    /// <summary>
    /// This is the factory for creating instances of the MockModifyDriver class.
    /// </summary>
    public sealed class MockModifyDriverFactory : ISqlDriverFactory
    {
        /// <summary>
        /// Value that will be returned from the SQL queries.
        /// </summary>
        private int modifiedCount;

        /// <summary>
        /// The created instance so unit tests can interrogate it.
        /// </summary>
        private MockModifyDriver instance;

        /// <summary>
        /// Initializes a new instance of the <see cref="MockModifyDriverFactory"/> class.
        /// </summary>
        /// <param name="modifiedCount">The desired return value.</param>
        public MockModifyDriverFactory(int modifiedCount)
        {
            this.modifiedCount = modifiedCount;
        }

        /// <summary>
        /// Gets the instance that this factory has created.
        /// </summary>
        /// <value>The driver instance.</value>
        public MockModifyDriver Driver
        {
            get { return this.instance; }
        }

        /// <summary>
        /// Create an instance of the MockModifyDriver class.
        /// </summary>
        /// <param name="timeout">The parameter is not used.</param>
        /// <returns>An instance of the MockModifyDriver class.</returns>
        public ISqlDriver Create(TimeoutInSeconds timeout)
        {
            this.instance = new MockModifyDriver(this.modifiedCount);
            return this.instance;
        }

        /// <summary>
        /// Create an instance of the MockModifyDriver class.
        /// </summary>
        /// <param name="timeout">The parameter is not used.</param>
        /// <param name="decorator">The parameter is not used.</param>
        /// <returns>An instance of the MockModifyDriver class.</returns>
        public ISqlDriver Create(TimeoutInSeconds timeout, SqlDecoratorType decorator)
        {
            this.instance = new MockModifyDriver(this.modifiedCount);
            return this.instance;
        }
    }
}
