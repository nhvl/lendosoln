﻿namespace LendersOffice.Drivers.SqlServerDB
{
    using System;
    using System.Collections.Generic;
    using System.Data;

    /// <summary>
    /// Implemenation of IDataReader that can be used when testing.
    /// </summary>
    public sealed class MockDataReader : IDataReader
    {
        /// <summary>
        /// Flag is set when close or dispose is called.
        /// </summary>
        private bool closed;

        /// <summary>
        /// Flag is set when EndConstruction is called.
        /// </summary>
        private bool constructed;

        /// <summary>
        /// The current table under interrogation.
        /// </summary>
        private Table currentTable;

        /// <summary>
        /// The current row under interrogation.
        /// </summary>
        private Row currentRow;

        /// <summary>
        /// The index for the current table.
        /// </summary>
        private int tableIndex = -1;

        /// <summary>
        /// The index for the current row.
        /// </summary>
        private int rowIndex = -1;

        /// <summary>
        /// All the tables are held here.
        /// </summary>
        private List<Table> tables;

        /// <summary>
        /// Gets the depth of nesting for the current row, inherited from IDataReader.
        /// </summary>
        /// <value>The depth of nesting for the current row.</value>
        int IDataReader.Depth
        {
            get
            {
                return this.tableIndex;
            }
        }

        /// <summary>
        /// Gets the number of columns, inherited from IDataRecord.
        /// </summary>
        /// <value>The number of columns.</value>
        int IDataRecord.FieldCount
        {
            get
            {
                return this.currentRow.ColumnCount;
            }
        }

        /// <summary>
        /// Gets a value indicating whether IDisposable.Close has been called.
        /// </summary>
        /// <value>True if IDisposable.Close has been called, false otherwise.</value>
        bool IDataReader.IsClosed
        {
            get
            {
                return this.closed;
            }
        }

        /// <summary>
        /// Gets the number of records that were affected by the query that generated this result.
        /// </summary>
        /// <value>Always zero since this mock is only used for sql select statements.</value>
        int IDataReader.RecordsAffected
        {
            get
            {
                return 0;
            }
        }

        /// <summary>
        /// Inherited from IDataRecord.
        /// </summary>
        /// <param name="name">The column name.</param>
        /// <returns>The column's value.</returns>
        object IDataRecord.this[string name]
        {
            get
            {
                return this.currentRow.GetValue(name);
            }
        }

        /// <summary>
        /// Inherited from IDataRecord.
        /// </summary>
        /// <param name="i">The column's index.</param>
        /// <returns>The columns value.</returns>
        object IDataRecord.this[int i]
        {
            get
            {
                return this.currentRow.GetValue(i);
            }
        }

        /// <summary>
        /// Inherited from IDataReader.
        /// </summary>
        void IDataReader.Close()
        {
            this.closed = true;
        }

        /// <summary>
        /// Inherited from IDisposable.
        /// </summary>
        void IDisposable.Dispose()
        {
            this.closed = true;
        }

        /// <summary>
        /// Inherited from IDataRecord.
        /// </summary>
        /// <param name="i">Index for the column.</param>
        /// <returns>The column value.</returns>
        bool IDataRecord.GetBoolean(int i)
        {
            return (bool)this.currentRow.GetValue(i);
        }

        /// <summary>
        /// Inherited from IDataRecord.
        /// </summary>
        /// <param name="i">Index for the column.</param>
        /// <returns>The column value.</returns>
        byte IDataRecord.GetByte(int i)
        {
            return (byte)this.currentRow.GetValue(i);
        }

        /// <summary>
        /// Inherited from IDataRecord.
        /// </summary>
        /// <param name="i">Index for the column.</param>
        /// <param name="fieldOffset">Offset into the data buffer.</param>
        /// <param name="buffer">Buffer to which data will be written.</param>
        /// <param name="bufferoffset">Offset in the buffer to begin writing data.</param>
        /// <param name="length">The number of bytes to write from the data buffer into the input buffer.</param>
        /// <returns>The column value.</returns>
        long IDataRecord.GetBytes(int i, long fieldOffset, byte[] buffer, int bufferoffset, int length)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Inherited from IDataRecord.
        /// </summary>
        /// <param name="i">Index for the column.</param>
        /// <returns>The column value.</returns>
        char IDataRecord.GetChar(int i)
        {
            return (char)this.currentRow.GetValue(i);
        }

        /// <summary>
        /// Inherited from IDataRecord.
        /// </summary>
        /// <param name="i">Index for the column.</param>
        /// <param name="fieldoffset">Offset into the data buffer.</param>
        /// <param name="buffer">Buffer to which data will be written.</param>
        /// <param name="bufferoffset">Offset in the buffer to begin writing data.</param>
        /// <param name="length">The number of chars to write from the data buffer into the input buffer.</param>
        /// <returns>The column value.</returns>
        long IDataRecord.GetChars(int i, long fieldoffset, char[] buffer, int bufferoffset, int length)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Inherited from IDataReader.
        /// </summary>
        /// <param name="i">Index for the column.</param>
        /// <returns>Throws an exception as this method isn't supported.</returns>
        IDataReader IDataRecord.GetData(int i)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Inherited from IDataRecord.
        /// </summary>
        /// <param name="i">Index for the column.</param>
        /// <returns>The data type of the column's value.</returns>
        string IDataRecord.GetDataTypeName(int i)
        {
            return this.currentRow.GetValue(i).GetType().Name;
        }

        /// <summary>
        /// Inherited from IDataRecord.
        /// </summary>
        /// <param name="i">Index for the column.</param>
        /// <returns>The column value.</returns>
        DateTime IDataRecord.GetDateTime(int i)
        {
            return (DateTime)this.currentRow.GetValue(i);
        }

        /// <summary>
        /// Inherited from IDataRecord.
        /// </summary>
        /// <param name="i">Index for the column.</param>
        /// <returns>The column value.</returns>
        decimal IDataRecord.GetDecimal(int i)
        {
            return (decimal)this.currentRow.GetValue(i);
        }

        /// <summary>
        /// Inherited from IDataRecord.
        /// </summary>
        /// <param name="i">Index for the column.</param>
        /// <returns>The column value.</returns>
        double IDataRecord.GetDouble(int i)
        {
            return (double)this.currentRow.GetValue(i);
        }

        /// <summary>
        /// Inherited from IDataRecord.
        /// </summary>
        /// <param name="i">Index for the column.</param>
        /// <returns>The column value.</returns>
        Type IDataRecord.GetFieldType(int i)
        {
            return this.currentRow.GetValue(i).GetType();
        }

        /// <summary>
        /// Inherited from IDataRecord.
        /// </summary>
        /// <param name="i">Index for the column.</param>
        /// <returns>The column value.</returns>
        float IDataRecord.GetFloat(int i)
        {
            return (float)this.currentRow.GetValue(i);
        }

        /// <summary>
        /// Inherited from IDataRecord.
        /// </summary>
        /// <param name="i">Index for the column.</param>
        /// <returns>The column value.</returns>
        Guid IDataRecord.GetGuid(int i)
        {
            return (Guid)this.currentRow.GetValue(i);
        }

        /// <summary>
        /// Inherited from IDataRecord.
        /// </summary>
        /// <param name="i">Index for the column.</param>
        /// <returns>The column value.</returns>
        short IDataRecord.GetInt16(int i)
        {
            return (short)this.currentRow.GetValue(i);
        }

        /// <summary>
        /// Inherited from IDataRecord.
        /// </summary>
        /// <param name="i">Index for the column.</param>
        /// <returns>The column value.</returns>
        int IDataRecord.GetInt32(int i)
        {
            return (int)this.currentRow.GetValue(i);
        }

        /// <summary>
        /// Inherited from IDataRecord.
        /// </summary>
        /// <param name="i">Index for the column.</param>
        /// <returns>The column value.</returns>
        long IDataRecord.GetInt64(int i)
        {
            return (long)this.currentRow.GetValue(i);
        }

        /// <summary>
        /// Inherited from IDataRecord.
        /// </summary>
        /// <param name="i">Index for the column.</param>
        /// <returns>The column's name.</returns>
        string IDataRecord.GetName(int i)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Inherited from IDataRecord.
        /// </summary>
        /// <param name="name">A column's name.</param>
        /// <returns>The index for that column (pedantically, ordinal minus one).</returns>
        int IDataRecord.GetOrdinal(string name)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Inherited from IDataReader.
        /// </summary>
        /// <returns>An exception is thrown as this will not be implemented.</returns>
        DataTable IDataReader.GetSchemaTable()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Inherited from IDataRecord.
        /// </summary>
        /// <param name="i">Index for the column.</param>
        /// <returns>The column value.</returns>
        string IDataRecord.GetString(int i)
        {
            return (string)this.currentRow.GetValue(i);
        }

        /// <summary>
        /// Inherited from IDataRecord.
        /// </summary>
        /// <param name="i">Index for the column.</param>
        /// <returns>The column value.</returns>
        object IDataRecord.GetValue(int i)
        {
            return this.currentRow.GetValue(i);
        }

        /// <summary>
        /// Inherited from IDataRecord.
        /// </summary>
        /// <param name="values">An array that will get populated with the column data.</param>
        /// <returns>The number of columns, which is min(available, values.Length).</returns>
        int IDataRecord.GetValues(object[] values)
        {
            for (int i = 0; i < values.Length; ++i)
            {
                values[i] = null;
            }

            int len = Math.Min(this.currentRow.ColumnCount, values.Length);
            for (int i = 0; i < len; ++i)
            {
                values[i] = this.currentRow.GetValue(i);
            }

            return len;
        }

        /// <summary>
        /// Inherited from IDataRecord.
        /// </summary>
        /// <param name="i">Index for the column.</param>
        /// <returns>True if the value for the column at the input index is null.</returns>
        bool IDataRecord.IsDBNull(int i)
        {
            object o = this.currentRow.GetValue(i);
            return (o == null) || Convert.IsDBNull(o);
        }

        /// <summary>
        /// Inherited from IDataReader.
        /// </summary>
        /// <returns>True if there is another table to read, false otherwise.</returns>
        bool IDataReader.NextResult()
        {
            ++this.tableIndex;
            if (this.tableIndex >= this.tables.Count)
            {
                this.rowIndex = -1;
                this.currentTable = null;
                this.currentRow = null;
                return false;
            }
            else
            {
                this.rowIndex = -1;
                this.currentTable = this.tables[this.tableIndex];
                this.currentRow = null;
                return true;
            }
        }

        /// <summary>
        /// Inherited from IDataReader.
        /// </summary>
        /// <returns>True if there is another row to read, false otherwise.</returns>
        bool IDataReader.Read()
        {
            if (this.tableIndex >= this.tables.Count)
            {
                return false;
            }
            else
            {
                if (this.currentTable == null)
                {
                    // first time
                    this.tableIndex = 0;
                    this.currentTable = this.tables[this.tableIndex];
                    this.rowIndex = 0;
                    this.currentRow = this.currentTable.GetRow(this.rowIndex);
                    return true;
                }
                else
                {
                    ++this.rowIndex;
                    if (this.rowIndex >= this.currentTable.RowCount)
                    {
                        return false;
                    }
                    else
                    {
                        this.currentRow = this.currentTable.GetRow(this.rowIndex);
                        return true;
                    }
                }
            }
        }

        /// <summary>
        /// The first call to make when constructing a mock data reader.
        /// </summary>
        public void BeginConstruction()
        {
            if (!this.constructed)
            {
                this.tables = new List<Table>();
            }
        }

        /// <summary>
        /// The last call to make when done constructing a mock reader, 
        /// places the state into a it's read-only mode.
        /// </summary>
        public void EndConstruction()
        {
            if (!this.constructed)
            {
                this.currentRow = null;
                this.currentTable = null;
                this.rowIndex = -1;
                this.tableIndex = -1;
                this.constructed = true;
            }
        }

        /// <summary>
        /// The call begins a new table during construction.
        /// </summary>
        public void BeginTable()
        {
            if (!this.constructed)
            {
                this.currentTable = new Table();
                this.tables.Add(this.currentTable);
            }
        }

        /// <summary>
        /// The call begins a new row during construction,
        /// which is added to the current table.
        /// </summary>
        public void BeginRow()
        {
            if (!this.constructed)
            {
                this.currentRow = this.currentTable.CreateNewRow();
            }
        }

        /// <summary>
        /// Add a column value to the current row during construction.
        /// </summary>
        /// <param name="name">The name of the column.</param>
        /// <param name="value">The value for the column.</param>
        public void AddColumn(string name, object value)
        {
            if (!this.constructed)
            {
                this.currentRow.AddColumn(name, value);
            }
        }

        /// <summary>
        /// Model a database table.
        /// </summary>
        private sealed class Table
        {
            /// <summary>
            /// The list of rows for this table.
            /// </summary>
            private List<Row> rows;

            /// <summary>
            /// Initializes a new instance of the <see cref="Table"/> class.
            /// </summary>
            public Table()
            {
                this.rows = new List<Row>();
            }

            /// <summary>
            /// Gets the number of rows.
            /// </summary>
            /// <value>The number of rows.</value>
            public int RowCount
            {
                get
                {
                    return this.rows.Count;
                }
            }

            /// <summary>
            /// Create a new row and add it to the table.
            /// </summary>
            /// <returns>The new row.</returns>
            public Row CreateNewRow()
            {
                var row = new Row();
                this.rows.Add(row);
                return row;
            }

            /// <summary>
            /// Retrieve a row by index.
            /// </summary>
            /// <param name="index">The index.</param>
            /// <returns>The row located at the index.</returns>
            public Row GetRow(int index)
            {
                return this.rows[index];
            }
        }

        /// <summary>
        /// Model a row of column data.
        /// </summary>
        private sealed class Row
        {
            /// <summary>
            /// The column data.
            /// </summary>
            private List<object> data;

            /// <summary>
            /// The column names.
            /// </summary>
            private List<string> columnNames;

            /// <summary>
            /// Initializes a new instance of the <see cref="Row"/> class.
            /// </summary>
            public Row()
            {
                this.data = new List<object>();
                this.columnNames = new List<string>();
            }

            /// <summary>
            /// Gets the number of columns.
            /// </summary>
            /// <value>The number of columns.</value>
            public int ColumnCount
            {
                get
                {
                    return this.data.Count;
                }
            }

            /// <summary>
            /// Add a column value to the row.
            /// </summary>
            /// <param name="name">The name of the column.</param>
            /// <param name="value">The value of the column.</param>
            public void AddColumn(string name, object value)
            {
                this.columnNames.Add(name);
                this.data.Add(value);
            }

            /// <summary>
            /// Retrieve a column's value based on the column order.
            /// </summary>
            /// <param name="index">The ordinal for the column (less one, to be pedantic).</param>
            /// <returns>The column value.</returns>
            public object GetValue(int index)
            {
                return this.data[index];
            }

            /// <summary>
            /// Retrieve a column's value based on the column's name.
            /// </summary>
            /// <param name="columnName">The name of the column.</param>
            /// <returns>The column value.</returns>
            public object GetValue(string columnName)
            {
                for (int i = 0; i < this.columnNames.Count; ++i)
                {
                    string name = this.columnNames[i];
                    if (name == columnName)
                    {
                        return this.data[i];
                    }
                }

                throw new IndexOutOfRangeException("No column with the specified name was found.");
            }
        }
    }
}
