﻿namespace LendersOffice.Drivers.SqlServerDB
{
    using System.Collections.Generic;
    using System.Data.Common;
using System.Data.SqlClient;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Base class for SQL mock classes.
    /// </summary>
    public abstract class MockDBBase
    {
        /// <summary>
        /// The query string that was passed in.
        /// </summary>
        private string sqlQuery;

        /// <summary>
        /// The sql parameter list that was passed in.
        /// </summary>
        private Dictionary<string, object> sqlParams;

        /// <summary>
        /// Initializes a new instance of the <see cref="MockDBBase"/> class.
        /// </summary>
        public MockDBBase()
        {
            this.sqlParams = new Dictionary<string, object>();
        }

        /// <summary>
        /// Gets the query that was passed into this mock.
        /// </summary>
        /// <returns>The query that was passed into this mock.</returns>
        public string GetQuery()
        {
            return this.sqlQuery;
        }

        /// <summary>
        /// Gets the string-ified value of the passed in SqlParameter with the specified name.
        /// </summary>
        /// <param name="paramName">The name of the desired SqlParameter.</param>
        /// <returns>The value of the SqlParameter, after ToString() is called on it.</returns>
        public string GetParamValue(string paramName)
        {
            return this.sqlParams.ContainsKey(paramName) ? this.sqlParams[paramName].ToString() : null;
        }

        /// <summary>
        /// Store query details.
        /// </summary>
        /// <param name="query">The SQL query.</param>
        /// <param name="parameters">A list of parameters passed in with the query.</param>
        protected void StoreQuery(SQLQueryString query, IEnumerable<DbParameter> parameters)
        {
            this.sqlQuery = query.Value;

            this.sqlParams.Clear();
            foreach (var param in parameters)
            {
                this.sqlParams[param.ParameterName] = param.Value;
            }
        }

        /// <summary>
        /// Store query details.
        /// </summary>
        /// <param name="storedProcedure">The name of the stored procedure.</param>
        /// <param name="parameters">A list of parameters passed in with the query.</param>
        protected void StoreStoredProcedureDetails(StoredProcedureName storedProcedure, IEnumerable<DbParameter> parameters)
        {
            this.sqlQuery = storedProcedure.ToString();

            this.sqlParams.Clear();
            foreach (var param in parameters)
            {
                this.sqlParams[param.ParameterName] = param.Value;
            }
        }
    }
}
