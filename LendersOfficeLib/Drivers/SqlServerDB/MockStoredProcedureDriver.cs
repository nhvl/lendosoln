﻿namespace LendersOffice.Drivers.SqlServerDB
{
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;

    /// <summary>
    /// Mock for testing when stored procedures are used.
    /// </summary>
    public sealed class MockStoredProcedureDriver : MockDBBase, IStoredProcedureDriver
    {
        /// <summary>
        /// A row count to return.
        /// </summary>
        private ModifiedRowCount rowCount;

        /// <summary>
        /// A data set to return.
        /// </summary>
        private DataSet dataSet;

        /// <summary>
        /// A data reader to return.
        /// </summary>
        private IDataReader dataReader;

        /// <summary>
        /// A scalar object to return.
        /// </summary>
        private object scalar;

        /// <summary>
        /// Initializes a new instance of the <see cref="MockStoredProcedureDriver"/> class.
        /// </summary>
        /// <param name="rowCount">A row count to return.</param>
        public MockStoredProcedureDriver(ModifiedRowCount rowCount)
        {
            this.rowCount = rowCount;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MockStoredProcedureDriver"/> class.
        /// </summary>
        /// <param name="dataSet">A data set to return.</param>
        public MockStoredProcedureDriver(DataSet dataSet)
        {
            this.dataSet = dataSet;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MockStoredProcedureDriver"/> class.
        /// </summary>
        /// <param name="dataReader">A data reader to return.</param>
        public MockStoredProcedureDriver(IDataReader dataReader)
        {
            this.dataReader = dataReader;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MockStoredProcedureDriver"/> class.
        /// </summary>
        /// <param name="scalar">A scalar object to return.</param>
        public MockStoredProcedureDriver(object scalar)
        {
            this.scalar = scalar;
        }

        /// <summary>
        /// Return the data set passed in the constructor.
        /// </summary>
        /// <param name="conn">The parameter is not used.</param>
        /// <param name="transaction">If using a transaction, pass it in here, else pass in null.</param>
        /// <param name="procedureName">The parameter is not used.</param>
        /// <param name="parameters">The parameter is not used.</param>
        /// <returns>The data set passed in the constructor.</returns>
        public DataSet ExecuteDataSet(IDbConnection conn, IDbTransaction transaction, StoredProcedureName procedureName, IEnumerable<DbParameter> parameters)
        {
            this.StoreStoredProcedureDetails(procedureName, parameters);
            return this.dataSet;
        }

        /// <summary>
        /// Return the row count passed in the constructor.
        /// </summary>
        /// <param name="conn">The parameter is not used.</param>
        /// <param name="transaction">If using a transaction, pass it in here, else pass in null.</param>
        /// <param name="procedureName">The parameter is not used.</param>
        /// <param name="parameters">The parameter is not used.</param>
        /// <returns>The row count passed in the constructor.</returns>
        public ModifiedRowCount ExecuteNonQuery(IDbConnection conn, IDbTransaction transaction, StoredProcedureName procedureName, IEnumerable<DbParameter> parameters)
        {
            this.StoreStoredProcedureDetails(procedureName, parameters);
            return this.rowCount;
        }

        /// <summary>
        /// Return the data reader passed in the constructor.
        /// </summary>
        /// <param name="conn">The parameter is not used.</param>
        /// <param name="transaction">If using a transaction, pass it in here, else pass in null.</param>
        /// <param name="procedureName">The parameter is not used.</param>
        /// <param name="parameters">The parameter is not used.</param>
        /// <returns>The data reader passed in the constructor.</returns>
        public IDataReader ExecuteReader(IDbConnection conn, IDbTransaction transaction, StoredProcedureName procedureName, IEnumerable<DbParameter> parameters)
        {
            this.StoreStoredProcedureDetails(procedureName, parameters);
            return this.dataReader;
        }

        /// <summary>
        /// Return the scalar passed in the constructor.
        /// </summary>
        /// <param name="conn">The parameter is not used.</param>
        /// <param name="transaction">If using a transaction, pass it in here, else pass in null.</param>
        /// <param name="procedureName">The parameter is not used.</param>
        /// <param name="parameters">The parameter is not used.</param>
        /// <returns>The scalar passed in the constructor.</returns>
        public object ExecuteScalar(IDbConnection conn, IDbTransaction transaction, StoredProcedureName procedureName, IEnumerable<DbParameter> parameters)
        {
            this.StoreStoredProcedureDetails(procedureName, parameters);
            return this.scalar;
        }
    }
}