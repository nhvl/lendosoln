﻿namespace LendersOffice.Drivers.SqlServerDB
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using LqbGrammar;
    using LqbGrammar.Adapters;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;

    /// <summary>
    /// Simplest implementation of the ISqlDriver interface.
    /// </summary>
    internal sealed class SqlServerDriver : ISqlDriver
    {
        /// <summary>
        /// Holds a reference to the adapter.
        /// </summary>
        private ISqlAdapter adapter;

        /// <summary>
        /// Initializes a new instance of the <see cref="SqlServerDriver"/> class.
        /// </summary>
        /// <param name="timeout">The timeout to use when calling to SQL Server.</param>
        public SqlServerDriver(TimeoutInSeconds timeout)
        {
            var factory = GenericLocator<ISqlAdapterFactory>.Factory;
            this.adapter = factory.Create(DataAccess.DbAccessUtils.GetDbProvider(), timeout);
        }

        /// <summary>
        /// Delete data from a SQL database.
        /// </summary>
        /// <param name="connection">The connection to the DB.</param>
        /// <param name="transaction">If using a transaction, pass it in here, else pass in null.</param>
        /// <param name="query">The query string.</param>
        /// <param name="parameters">The query parameters.</param>
        /// <returns>The number of rows that were deleted.</returns>
        public ModifiedRowCount Delete(DbConnection connection, DbTransaction transaction, SQLQueryString query, IEnumerable<DbParameter> parameters)
        {
            return this.adapter.Delete(connection, transaction, query, parameters);
        }

        /// <summary>
        /// Insert a record into a table but do not retrieve the primary key.
        /// This is usually used for tables where the primary key is a set
        /// of columns rather than a generated key.
        /// </summary>
        /// <param name="connection">The connection to the DB.</param>
        /// <param name="transaction">If using a transaction, pass it in here, else pass in null.</param>
        /// <param name="query">The query string.</param>
        /// <param name="parameters">The query parameters.</param>
        public void InsertNoKey(DbConnection connection, DbTransaction transaction, SQLQueryString query, IEnumerable<DbParameter> parameters)
        {
            this.adapter.InsertNoKey(connection, transaction, query, parameters);
        }

        /// <summary>
        /// Insert a record into a table that has a uniqueidentifier-valued primary key.
        /// </summary>
        /// <param name="connection">The connection to the DB.</param>
        /// <param name="transaction">If using a transaction, pass it in here, else pass in null.</param>
        /// <param name="query">The query string.</param>
        /// <param name="parameters">The query parameters.</param>
        /// <returns>The primary key of the inserted record.</returns>
        public PrimaryKeyGuid InsertGetGuid(DbConnection connection, DbTransaction transaction, SQLQueryString query, IEnumerable<DbParameter> parameters)
        {
            return this.adapter.InsertGetGuid(connection, transaction, query, parameters);
        }

        /// <summary>
        /// Insert a record into a table that has an integer-valued primary key.
        /// </summary>
        /// <param name="connection">The connection to the DB.</param>
        /// <param name="transaction">If using a transaction, pass it in here, else pass in null.</param>
        /// <param name="query">The query string.</param>
        /// <param name="parameters">The query parameters.</param>
        /// <returns>The primary key of the inserted record.</returns>
        public PrimaryKeyInt32 InsertGetInt32(DbConnection connection, DbTransaction transaction, SQLQueryString query, IEnumerable<DbParameter> parameters)
        {
            return this.adapter.InsertGetInt32(connection, transaction, query, parameters);
        }

        /// <summary>
        /// Retrieve data from a SQL database.
        /// </summary>
        /// <param name="connection">The connection to the DB.</param>
        /// <param name="transaction">If using a transaction, pass it in here, else pass in null.</param>
        /// <param name="query">The query string.</param>
        /// <param name="parameters">The query parameters.</param>
        /// <returns>The retrieved data.</returns>
        public DbDataReader Select(DbConnection connection, DbTransaction transaction, SQLQueryString query, IEnumerable<DbParameter> parameters)
        {
            return this.adapter.Select(connection, transaction, query, parameters);
        }

        /// <summary>
        /// Retrieve data from a SQL database and populate the input data set.
        /// </summary>
        /// <param name="connection">The connection to the DB.</param>
        /// <param name="transaction">If using a transaction, pass it in here, else pass in null.</param>
        /// <param name="fillTarget">The data set that will be filled.</param>
        /// <param name="query">The query string.</param>
        /// <param name="parameters">The query parameters.</param>
        public void FillDataSet(DbConnection connection, DbTransaction transaction, DataSet fillTarget, SQLQueryString query, IEnumerable<DbParameter> parameters)
        {
            this.adapter.FillDataSet(connection, transaction, fillTarget, query, parameters);
        }

        /// <summary>
        /// Retrieve data from a SQL database and populate the input data set using the pre-prepared adapter.
        /// </summary>
        /// <param name="adapter">The adapter used to fill the data set.</param>
        /// <param name="fillTarget">The data set that will be filled.</param>
        /// <param name="tableName">Optional value that is the table name for updating.</param>
        public void FillDataSet(DbDataAdapter adapter, DataSet fillTarget, DBTableName? tableName)
        {
            this.adapter.FillDataSet(adapter, fillTarget, tableName);
        }

        /// <summary>
        /// Update data to a SQL database and with changes in the data set using the pre-prepared adapter.
        /// </summary>
        /// <param name="adapter">The adapter used to update the data set.</param>
        /// <param name="withChanges">The data set that will be updated to the SQL database.</param>
        /// <param name="tableName">Optional value that is the table name for updating.</param>
        /// <returns>The number of rows that were modified.</returns>
        public ModifiedRowCount UpdateDataSet(DbDataAdapter adapter, DataSet withChanges, DBTableName? tableName)
        {
            return this.adapter.UpdateDataSet(adapter, withChanges, tableName);
        }

        /// <summary>
        /// Update data held in a SQL database.
        /// </summary>
        /// <param name="connection">The connection to the DB.</param>
        /// <param name="transaction">If using a transaction, pass it in here, else pass in null.</param>
        /// <param name="query">The query string.</param>
        /// <param name="parameters">The query parameters.</param>
        /// <returns>The number of rows that were modified.</returns>
        public ModifiedRowCount Update(DbConnection connection, DbTransaction transaction, SQLQueryString query, IEnumerable<DbParameter> parameters)
        {
            return this.adapter.Update(connection, transaction, query, parameters);
        }
    }
}