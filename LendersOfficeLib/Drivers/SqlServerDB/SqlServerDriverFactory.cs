﻿namespace LendersOffice.Drivers.SqlServerDB
{
    using System;
    using LqbGrammar;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Factory for creating instances of SqlServerDriver.
    /// </summary>
    public sealed class SqlServerDriverFactory : ISqlDriverFactory
    {
        /// <summary>
        /// Create an instance of the driver type that is the current default implementation of ISqlDriver.
        /// </summary>
        /// <param name="timeout">Timeout to use when calling SQL Server.</param>
        /// <returns>An instance of the default driver type.</returns>
        public ISqlDriver Create(TimeoutInSeconds timeout)
        {
            return this.Create(timeout, SqlDecoratorType.Default);
        }

        /// <summary>
        /// Create an driver instance of the specified type.
        /// </summary>
        /// <param name="timeout">Timeout to use when calling SQL Server.</param>
        /// <param name="decorator">The desired driver type.</param>
        /// <returns>An implementation of ISqlDriver.</returns>
        public ISqlDriver Create(TimeoutInSeconds timeout, SqlDecoratorType decorator)
        {
            var simpleDriver = new SqlServerDriver(timeout);

            switch (decorator)
            {
                case SqlDecoratorType.None:
                    return simpleDriver;

                case SqlDecoratorType.StopWatch:
                    return new StopwatchDriver(timeout, simpleDriver);

                case SqlDecoratorType.Default:
                case SqlDecoratorType.Logging:
                    return new LoggingDriver(timeout, simpleDriver);

                default:
                    throw new ServerException(ErrorMessage.SystemError);
            }
        }
    }
}
