﻿namespace LendersOffice.Drivers.SqlServerDB
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;
    using DataAccess;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Handle statistics and error logging, in addition to the database calls.
    /// </summary>
    internal sealed class LoggingDriver : ISqlDriver
    {
        /// <summary>
        /// Handles the database calls.
        /// </summary>
        private ISqlDriver innerDriver;

        /// <summary>
        /// Initializes a new instance of the <see cref="LoggingDriver"/> class.
        /// </summary>
        /// <param name="timeout">The timeout to use when calling to SQL Server.</param>
        /// <param name="innerDriver">The driver that carries out the SQL calls.</param>
        public LoggingDriver(TimeoutInSeconds timeout, ISqlDriver innerDriver)
        {
            this.innerDriver = innerDriver;
        }

        /// <summary>
        /// Delete data from a SQL database.
        /// </summary>
        /// <param name="connection">The connection to the DB.</param>
        /// <param name="transaction">If using a transaction, pass it in here, else pass in null.</param>
        /// <param name="query">The query string.</param>
        /// <param name="parameters">The query parameters.</param>
        /// <returns>The number of rows that were deleted.</returns>
        public ModifiedRowCount Delete(DbConnection connection, DbTransaction transaction, SQLQueryString query, IEnumerable<DbParameter> parameters)
        {
            try
            {
                return this.innerDriver.Delete(connection, transaction, query, parameters);
            }
            catch (ServerException ex) when (ex.InnerException is DbException)
            {
                Tools.LogError(ex.InnerException);
                throw;
            }
        }

        /// <summary>
        /// Insert a record into a table but do not retrieve the primary key.
        /// This is usually used for tables where the primary key is a set
        /// of columns rather than a generated key.
        /// </summary>
        /// <param name="connection">The connection to the DB.</param>
        /// <param name="transaction">If using a transaction, pass it in here, else pass in null.</param>
        /// <param name="query">The query string.</param>
        /// <param name="parameters">The query parameters.</param>
        public void InsertNoKey(DbConnection connection, DbTransaction transaction, SQLQueryString query, IEnumerable<DbParameter> parameters)
        {
            try
            {
                this.innerDriver.InsertNoKey(connection, transaction, query, parameters);
            }
            catch (ServerException ex) when (ex.InnerException is DbException)
            {
                Tools.LogError(ex.InnerException);
                throw;
            }
        }

        /// <summary>
        /// Insert a record into a table that has a uniqueidentifier-valued primary key.
        /// </summary>
        /// <param name="connection">The connection to the DB.</param>
        /// <param name="transaction">If using a transaction, pass it in here, else pass in null.</param>
        /// <param name="query">The query string.</param>
        /// <param name="parameters">The query parameters.</param>
        /// <returns>The primary key of the inserted record.</returns>
        public PrimaryKeyGuid InsertGetGuid(DbConnection connection, DbTransaction transaction, SQLQueryString query, IEnumerable<DbParameter> parameters)
        {
            try
            {
                return this.innerDriver.InsertGetGuid(connection, transaction, query, parameters);
            }
            catch (ServerException ex) when (ex.InnerException is DbException)
            {
                Tools.LogError(ex.InnerException);
                throw;
            }
        }

        /// <summary>
        /// Insert a record into a table that has an integer-valued primary key.
        /// </summary>
        /// <param name="connection">The connection to the DB.</param>
        /// <param name="transaction">If using a transaction, pass it in here, else pass in null.</param>
        /// <param name="query">The query string.</param>
        /// <param name="parameters">The query parameters.</param>
        /// <returns>The primary key of the inserted record.</returns>
        public PrimaryKeyInt32 InsertGetInt32(DbConnection connection, DbTransaction transaction, SQLQueryString query, IEnumerable<DbParameter> parameters)
        {
            try
            {
                return this.innerDriver.InsertGetInt32(connection, transaction, query, parameters);
            }
            catch (ServerException ex) when (ex.InnerException is DbException)
            {
                Tools.LogError(ex.InnerException);
                throw;
            }
        }

        /// <summary>
        /// Retrieve data from a SQL database.
        /// </summary>
        /// <param name="connection">The connection to the DB.</param>
        /// <param name="transaction">If using a transaction, pass it in here, else pass in null.</param>
        /// <param name="query">The query string.</param>
        /// <param name="parameters">The query parameters.</param>
        /// <returns>The retrieved data.</returns>
        public DbDataReader Select(DbConnection connection, DbTransaction transaction, SQLQueryString query, IEnumerable<DbParameter> parameters)
        {
            try
            {
                return this.innerDriver.Select(connection, transaction, query, parameters);
            }
            catch (ServerException ex) when (ex.InnerException is DbException)
            {
                Tools.LogError(ex.InnerException);
                throw;
            }
        }

        /// <summary>
        /// Retrieve data from a SQL database and populate the input data set.
        /// </summary>
        /// <param name="connection">The connection to the DB.</param>
        /// <param name="transaction">If using a transaction, pass it in here, else pass in null.</param>
        /// <param name="fillTarget">The data set that will be filled.</param>
        /// <param name="query">The query string.</param>
        /// <param name="parameters">The query parameters.</param>
        [SuppressMessage(category: "LendingQBStyleCop.LendingQBCustomRules", checkId: "LB1003:CannotSwallowGeneralException", Justification = "Retrying on error.")]
        public void FillDataSet(DbConnection connection, DbTransaction transaction, DataSet fillTarget, SQLQueryString query, IEnumerable<DbParameter> parameters)
        {
            Exception lastException = null;
            int tryCount = 3;
            var parameterList = parameters;
            for (int i = 0; i < tryCount; ++i)
            {
                try
                {
                    this.FillWithLogging(connection, transaction, fillTarget, query, parameterList);

                    // We needed to copy the parameters due to a SQL exception
                    if (parameterList != parameters)
                    {
                        this.ResetParameterValues(parameters, parameterList);
                    }

                    return;
                }
                catch (ServerException ex) when (ex.InnerException is DbException)
                {
                    lastException = ex;
                    if (i < (tryCount - 1))
                    {
                        Tools.SleepAndWakeupRandomlyWithin(1000, 4500);
                        parameterList = this.CopyParameters(parameters);
                    }
                }
            }

            if (lastException.InnerException is DbException)
            {
                GenericSqlException genericSqlException = new GenericSqlException("LoggingDriver.FillDataSet:: sql = " + query.Value, lastException.InnerException as System.Data.SqlClient.SqlException);
                Tools.LogErrorWithCriticalTracking(genericSqlException);
            }

            string msg = string.Format("Db access error in LoggingDriver.FillDataSet(). Sql statement is = {0} ", query.Value);
            var error = ErrorMessage.FromTemplateWithSql(ErrorMessageTemplate.FillDataSetAccessFailed, query.Value);
            throw new LqbGrammar.Exceptions.ServerException(error);
        }

        /// <summary>
        /// Retrieve data from a SQL database and populate the input data set using the pre-prepared adapter.
        /// </summary>
        /// <param name="adapter">The adapter used to fill the data set.</param>
        /// <param name="fillTarget">The data set that will be filled.</param>
        /// <param name="tableName">Optional value that is the table name for updating.</param>
        public void FillDataSet(DbDataAdapter adapter, DataSet fillTarget, DBTableName? tableName)
        {
            this.innerDriver.FillDataSet(adapter, fillTarget, tableName);
        }

        /// <summary>
        /// Update data to a SQL database and with changes in the data set using the pre-prepared adapter.
        /// </summary>
        /// <param name="adapter">The adapter used to update the data set.</param>
        /// <param name="withChanges">The data set that will be updated to the SQL database.</param>
        /// <param name="tableName">Optional value that is the table name for updating.</param>
        /// <returns>The number of rows that were modified.</returns>
        public ModifiedRowCount UpdateDataSet(DbDataAdapter adapter, DataSet withChanges, DBTableName? tableName)
        {
            return this.innerDriver.UpdateDataSet(adapter, withChanges, tableName);
        }

        /// <summary>
        /// Update data held in a SQL database.
        /// </summary>
        /// <param name="connection">The connection to the DB.</param>
        /// <param name="transaction">If using a transaction, pass it in here, else pass in null.</param>
        /// <param name="query">The query string.</param>
        /// <param name="parameters">The query parameters.</param>
        /// <returns>The number of rows that were modified.</returns>
        public ModifiedRowCount Update(DbConnection connection, DbTransaction transaction, SQLQueryString query, IEnumerable<DbParameter> parameters)
        {
            try
            {
                return this.innerDriver.Update(connection, transaction, query, parameters);
            }
            catch (ServerException ex) when (ex.InnerException is DbException)
            {
                Tools.LogError(ex.InnerException);
                throw;
            }
        }

        /// <summary>
        /// Retrieve data from a SQL database and populate the input data set.
        /// </summary>
        /// <param name="connection">The connection to the DB.</param>
        /// <param name="transaction">If using a transaction, pass it in here, else pass in null.</param>
        /// <param name="fillTarget">The data set that will be filled.</param>
        /// <param name="query">The query string.</param>
        /// <param name="parameters">The query parameters.</param>
        private void FillWithLogging(DbConnection connection, DbTransaction transaction, DataSet fillTarget, SQLQueryString query, IEnumerable<DbParameter> parameters)
        {
            try
            {
                this.innerDriver.FillDataSet(connection, transaction, fillTarget, query, parameters);
            }
            catch (ServerException ex) when (ex.InnerException is DbException)
            {
                Tools.LogError(ex.InnerException);
                throw;
            }
        }

        /// <summary>
        /// Replicates the parameters in the specified <paramref name="parameterList"/>.
        /// </summary>
        /// <param name="parameterList">
        /// The list of parameters to copy.
        /// </param>
        /// <returns>
        /// A list of copied parameters.
        /// </returns>
        private IEnumerable<DbParameter> CopyParameters(IEnumerable<DbParameter> parameterList)
        {
            if (parameterList == null)
            {
                return null;
            }

            var copyList = new List<DbParameter>();

            foreach (var parameter in parameterList)
            {
                var clone = ((ICloneable)parameter).Clone();
                copyList.Add((DbParameter)clone);
            }

            return copyList;
        }

        /// <summary>
        /// Resets the values of the specified <paramref name="originalParameters"/> to
        /// the values present in the specified <paramref name="copiedParameters"/>.
        /// </summary>
        /// <param name="originalParameters">
        /// The original parameters.
        /// </param>
        /// <param name="copiedParameters">
        /// The copied parameters.
        /// </param>
        private void ResetParameterValues(IEnumerable<DbParameter> originalParameters, IEnumerable<DbParameter> copiedParameters)
        {
            foreach (var item in originalParameters.Zip(copiedParameters, Tuple.Create))
            {
                item.Item2.Value = item.Item1.Value;
            }
        }
    }
}