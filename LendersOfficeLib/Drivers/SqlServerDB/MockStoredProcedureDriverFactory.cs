﻿namespace LendersOffice.Drivers.SqlServerDB
{
    using System;
    using System.Data;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Factory for creating mock drivers for stored procedure calls.
    /// </summary>
    public sealed class MockStoredProcedureDriverFactory : IStoredProcedureDriverFactory
    {
        /// <summary>
        /// A row count to return.
        /// </summary>
        private ModifiedRowCount rowCount;

        /// <summary>
        /// A data set to return.
        /// </summary>
        private DataSet dataSet;

        /// <summary>
        /// A data reader to return.
        /// </summary>
        private IDataReader dataReader;

        /// <summary>
        /// A scalar object to return.
        /// </summary>
        private object scalar;

        /// <summary>
        /// The stored data type.
        /// </summary>
        private DataType dataType;

        /// <summary>
        /// Hold onto the driver for later interrogation.
        /// </summary>
        private MockStoredProcedureDriver driver;

        /// <summary>
        /// Initializes a new instance of the <see cref="MockStoredProcedureDriverFactory"/> class.
        /// </summary>
        /// <param name="rowCount">A row count to return.</param>
        public MockStoredProcedureDriverFactory(ModifiedRowCount rowCount)
        {
            this.dataType = DataType.RowCount;
            this.rowCount = rowCount;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MockStoredProcedureDriverFactory"/> class.
        /// </summary>
        /// <param name="dataSet">A data set to return.</param>
        public MockStoredProcedureDriverFactory(DataSet dataSet)
        {
            this.dataType = DataType.DataSet;
            this.dataSet = dataSet;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MockStoredProcedureDriverFactory"/> class.
        /// </summary>
        /// <param name="dataReader">A data reader to return.</param>
        public MockStoredProcedureDriverFactory(IDataReader dataReader)
        {
            this.dataType = DataType.DataReader;
            this.dataReader = dataReader;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MockStoredProcedureDriverFactory"/> class.
        /// </summary>
        /// <param name="scalar">A scalar object to return.</param>
        public MockStoredProcedureDriverFactory(object scalar)
        {
            this.dataType = DataType.Scalar;
            this.scalar = scalar;
        }

        /// <summary>
        /// Define the data type being used for the test.
        /// </summary>
        private enum DataType
        {
            /// <summary>
            /// A ModifiedRowCount is used.
            /// </summary>
            RowCount,

            /// <summary>
            /// A data set is used.
            /// </summary>
            DataSet,

            /// <summary>
            /// A data reader is used.
            /// </summary>
            DataReader,

            /// <summary>
            /// A scalar is used.
            /// </summary>
            Scalar
        }

        /// <summary>
        /// Create a mock stored procedure driver primed to return the desired type.
        /// </summary>
        /// <param name="timeout">The parameter is not used.</param>
        /// <returns>A mock stored procedure driver primed to return the desired type.</returns>
        public IStoredProcedureDriver Create(TimeoutInSeconds timeout)
        {
            switch (this.dataType)
            {
                case DataType.DataReader:
                    this.driver = new MockStoredProcedureDriver(this.dataReader);
                    break;
                case DataType.DataSet:
                    this.driver = new MockStoredProcedureDriver(this.dataSet);
                    break;
                case DataType.RowCount:
                    this.driver = new MockStoredProcedureDriver(this.rowCount);
                    break;
                case DataType.Scalar:
                    this.driver = new MockStoredProcedureDriver(this.scalar);
                    break;
                default:
                    throw new DeveloperException(ErrorMessage.SystemError);
            }

            return this.driver;
        }

        /// <summary>
        /// Retrieve the last driver created for interrogation by unit test code.
        /// </summary>
        /// <returns>The last driver created.</returns>
        public MockStoredProcedureDriver GetLastDriver()
        {
            return this.driver;
        }
    }
}
