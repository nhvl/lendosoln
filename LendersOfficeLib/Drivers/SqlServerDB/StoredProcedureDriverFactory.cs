﻿namespace LendersOffice.Drivers.SqlServerDB
{
    using DataAccess;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;

    /// <summary>
    /// Factory for creating instances of StoredProcedureDriver.
    /// </summary>
    public sealed class StoredProcedureDriverFactory : IStoredProcedureDriverFactory
    {
        /// <summary>
        /// Create an instance of StoredProcedureDriver.
        /// </summary>
        /// <param name="timeout">Timeout to use when calling SQL Server.</param>
        /// <returns>An instance of StoredProcedureDriver.</returns>
        public IStoredProcedureDriver Create(TimeoutInSeconds timeout)
        {
            return new StoredProcedureDriver(DbAccessUtils.GetDbProvider(), timeout);
        }

        // NOTE: I will need to add decorator drivers and new factory methods similar to how the adhoc query drivers work.
    }
}
