﻿namespace LendersOffice.Drivers.SqlServerDB
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using LqbGrammar;
    using LqbGrammar.Adapters;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;

    /// <summary>
    /// Simplest implementation of the IStoredProcedureDriver interface.
    /// </summary>
    internal sealed class StoredProcedureDriver : IStoredProcedureDriver
    {
        /// <summary>
        /// Holds a reference to the adapter.
        /// </summary>
        private IStoredProcedureAdapter adapter;

        /// <summary>
        /// Initializes a new instance of the <see cref="StoredProcedureDriver"/> class.
        /// </summary>
        /// <param name="factory">The database provider factory.</param>
        /// <param name="timeout">The timeout to use when calling to SQL Server.</param>
        public StoredProcedureDriver(DbProviderFactory factory, TimeoutInSeconds timeout)
        {
            var creator = GenericLocator<IStoredProcedureAdapterFactory>.Factory;
            this.adapter = creator.Create(factory, timeout);
        }

        /// <summary>
        /// Call a stored procedure that returns data, with the full data stored in a DataSet.
        /// </summary>
        /// <param name="conn">The connection to the DB.</param>
        /// <param name="transaction">If using a transaction, pass it in here, else pass in null.</param>
        /// <param name="procedureName">The name of the stored procedure.</param>
        /// <param name="parameters">The procedure's parameters.</param>
        /// <returns>The data set containing the full returned data.</returns>
        public DataSet ExecuteDataSet(IDbConnection conn, IDbTransaction transaction, StoredProcedureName procedureName, IEnumerable<DbParameter> parameters)
        {
            return this.adapter.ExecuteDataSet(conn, transaction, procedureName, parameters);
        }

        /// <summary>
        /// Execute a stored procedure that modifies data.
        /// </summary>
        /// <param name="conn">The connection to the DB.</param>
        /// <param name="transaction">If using a transaction, pass it in here, else pass in null.</param>
        /// <param name="procedureName">The name of the stored procedure.</param>
        /// <param name="parameters">The procedure's parameters.</param>
        /// <returns>The number of rows modified by the stored procedure.</returns>
        public ModifiedRowCount ExecuteNonQuery(IDbConnection conn, IDbTransaction transaction, StoredProcedureName procedureName, IEnumerable<DbParameter> parameters)
        {
            return this.adapter.ExecuteNonQuery(conn, transaction, procedureName, parameters);
        }

        /// <summary>
        /// Call a stored procedure that returns a row set.
        /// </summary>
        /// <param name="conn">The connection to the DB.</param>
        /// <param name="transaction">If using a transaction, pass it in here, else pass in null.</param>
        /// <param name="procedureName">The name of the stored procedure.</param>
        /// <param name="parameters">The procedure's parameters.</param>
        /// <returns>An interface used to iterate through the returned row set.</returns>
        public IDataReader ExecuteReader(IDbConnection conn, IDbTransaction transaction, StoredProcedureName procedureName, IEnumerable<DbParameter> parameters)
        {
            return this.adapter.ExecuteReader(conn, transaction, procedureName, parameters);
        }

        /// <summary>
        /// Call a stored procedure that returns a single value.
        /// </summary>
        /// <param name="conn">The connection to the DB.</param>
        /// <param name="transaction">If using a transaction, pass it in here, else pass in null.</param>
        /// <param name="procedureName">The name of the stored procedure.</param>
        /// <param name="parameters">The procedure's parameters.</param>
        /// <returns>The single valued return data.</returns>
        public object ExecuteScalar(IDbConnection conn, IDbTransaction transaction, StoredProcedureName procedureName, IEnumerable<DbParameter> parameters)
        {
            return this.adapter.ExecuteScalar(conn, transaction, procedureName, parameters);
        }
    }
}