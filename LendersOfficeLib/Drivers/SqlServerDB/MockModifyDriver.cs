﻿namespace LendersOffice.Drivers.SqlServerDB
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;

    /// <summary>
    /// Mock driver to avoid SQL Update and Delete calls to the live DB.
    /// </summary>
    public sealed class MockModifyDriver : MockDBBase, ISqlDriver
    {
        /// <summary>
        /// Value that will be returned from the SQL queries.
        /// </summary>
        private ModifiedRowCount modifiedCount;

        /// <summary>
        /// Initializes a new instance of the <see cref="MockModifyDriver"/> class.
        /// </summary>
        /// <param name="modifiedCount">The desired return value.</param>
        public MockModifyDriver(int modifiedCount)
        {
            ModifiedRowCount? count = ModifiedRowCount.Create(modifiedCount);
            if (count != null)
            {
                this.modifiedCount = count.Value;
            }
            else
            {
                throw new ArgumentException("Invalid modified count, must be >= 0", "a_nModifiedCount");
            }
        }

        /// <summary>
        /// Store query and return the pre-defined row count value.
        /// </summary>
        /// <param name="connection">The parameter is not used.</param>
        /// <param name="transaction">The parameter is not used.</param>
        /// <param name="query">The SQL query.</param>
        /// <param name="parameters">A list of parameters passed in with the query.</param>
        /// <returns>The pre-defined row count value.</returns>
        public ModifiedRowCount Delete(DbConnection connection, DbTransaction transaction, SQLQueryString query, IEnumerable<DbParameter> parameters)
        {
            this.StoreQuery(query, parameters);
            return this.modifiedCount;
        }

        /// <summary>
        /// Throws NotImplementedException because this mock class is only for update and delete queries.
        /// </summary>
        /// <param name="connection">The parameter is not used.</param>
        /// <param name="transaction">The parameter is not used.</param>
        /// <param name="query">The parameter is not used.</param>
        /// <param name="parameters">The parameter is not used.</param>
        public void InsertNoKey(DbConnection connection, DbTransaction transaction, SQLQueryString query, IEnumerable<DbParameter> parameters)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Throws NotImplementedException because this mock class is only for update and delete queries.
        /// </summary>
        /// <param name="connection">The parameter is not used.</param>
        /// <param name="transaction">The parameter is not used.</param>
        /// <param name="query">The parameter is not used.</param>
        /// <param name="parameters">The parameter is not used.</param>
        /// <returns>Nothing since an exception is always thrown.</returns>
        public PrimaryKeyGuid InsertGetGuid(DbConnection connection, DbTransaction transaction, SQLQueryString query, IEnumerable<DbParameter> parameters)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Throws NotImplementedException because this mock class is only for update and delete queries.
        /// </summary>
        /// <param name="connection">The parameter is not used.</param>
        /// <param name="transaction">The parameter is not used.</param>
        /// <param name="query">The parameter is not used.</param>
        /// <param name="parameters">The parameter is not used.</param>
        /// <returns>Nothing since an exception is always thrown.</returns>
        public PrimaryKeyInt32 InsertGetInt32(DbConnection connection, DbTransaction transaction, SQLQueryString query, IEnumerable<DbParameter> parameters)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Throws NotImplementedException because this mock class is only for update and delete queries.
        /// </summary>
        /// <param name="connection">The parameter is not used.</param>
        /// <param name="transaction">The parameter is not used.</param>
        /// <param name="query">The parameter is not used.</param>
        /// <param name="parameters">The parameter is not used.</param>
        /// <returns>Nothing since an exception is always thrown.</returns>
        public DbDataReader Select(DbConnection connection, DbTransaction transaction, SQLQueryString query, IEnumerable<DbParameter> parameters)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Throws NotImplementedException because this mock class is only for update and delete queries.
        /// </summary>
        /// <param name="connection">The parameter is not used.</param>
        /// <param name="transaction">The parameter is not used.</param>
        /// <param name="fillTarget">The parameter is not used.</param>
        /// <param name="query">The parameter is not used.</param>
        /// <param name="parameters">The parameter is not used.</param>
        public void FillDataSet(DbConnection connection, DbTransaction transaction, DataSet fillTarget, SQLQueryString query, IEnumerable<DbParameter> parameters)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Throws NotImplementedException because this mock class is only for update and delete queries.
        /// </summary>
        /// <param name="adapter">The parameter is not used.</param>
        /// <param name="fillTarget">The parameter is not used.</param>
        /// <param name="tableName">The parameter is not used.</param>
        public void FillDataSet(DbDataAdapter adapter, DataSet fillTarget, DBTableName? tableName)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Do nothing.
        /// </summary>
        /// <param name="adapter">The parameter is not used.</param>
        /// <param name="withChanges">The parameter is not used.</param>
        /// <param name="tableName">The parameter is not used.</param>
        /// <returns>The number of rows that were modified.</returns>
        public ModifiedRowCount UpdateDataSet(DbDataAdapter adapter, DataSet withChanges, DBTableName? tableName)
        {
            return this.modifiedCount;
        }

        /// <summary>
        /// Throws NotImplementedException because this mock class is only for update and delete queries.
        /// </summary>
        /// <param name="connection">The parameter is not used.</param>
        /// <param name="transaction">The parameter is not used.</param>
        /// <param name="query">The parameter is not used.</param>
        /// <param name="parameters">The parameter is not used.</param>
        /// <returns>Nothing since an exception is always thrown.</returns>
        public ModifiedRowCount Update(DbConnection connection, DbTransaction transaction, SQLQueryString query, IEnumerable<DbParameter> parameters)
        {
            this.StoreQuery(query, parameters);
            return this.modifiedCount;
        }
    }
}