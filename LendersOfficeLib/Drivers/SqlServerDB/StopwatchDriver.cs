﻿namespace LendersOffice.Drivers.SqlServerDB
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Diagnostics;
    using System.Diagnostics.CodeAnalysis;
    using DataAccess;
    using LendersOffice.Constants;
    using LendersOffice.HttpModule;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Wrap DB calls with a stopwatch, and log the timing.
    /// </summary>
    internal sealed class StopwatchDriver : ISqlDriver
    {
        /// <summary>
        /// We want to log write queries that take longer than this.
        /// </summary>
        private const int WriteThresholdInMs = 1000;

        /// <summary>
        /// We want to log read queries that take longer than this.
        /// </summary>
        private const int ReadThresholdInMs = 1000;

        /// <summary>
        /// Handles the database calls.
        /// </summary>
        private ISqlDriver innerDriver;

        /// <summary>
        /// Initializes a new instance of the <see cref="StopwatchDriver"/> class.
        /// </summary>
        /// <param name="timeout">The timeout to use when calling to SQL Server.</param>
        /// <param name="innerDriver">The driver that carries out the SQL calls.</param>
        public StopwatchDriver(TimeoutInSeconds timeout, ISqlDriver innerDriver)
        {
            this.innerDriver = innerDriver;
        }

        /// <summary>
        /// Delete data from a SQL database.
        /// </summary>
        /// <param name="connection">The connection to the DB.</param>
        /// <param name="transaction">If using a transaction, pass it in here, else pass in null.</param>
        /// <param name="query">The query string.</param>
        /// <param name="parameters">The query parameters.</param>
        /// <returns>The number of rows that were deleted.</returns>
        public ModifiedRowCount Delete(DbConnection connection, DbTransaction transaction, SQLQueryString query, IEnumerable<DbParameter> parameters)
        {
            ModifiedRowCount rowCount = ModifiedRowCount.Invalid;
            Stopwatch stopwatch = new Stopwatch();
            try
            {
                rowCount = this.innerDriver.Delete(connection, transaction, query, parameters);
                return rowCount;
            }
            finally
            {
                stopwatch.Stop();
                this.LogIfTooLong(stopwatch, query, rowCount, WriteThresholdInMs);
            }
        }

        /// <summary>
        /// Retrieve data from a SQL database and populate the input data set.
        /// </summary>
        /// <param name="connection">The connection to the DB.</param>
        /// <param name="transaction">If using a transaction, pass it in here, else pass in null.</param>
        /// <param name="fillTarget">The data set that will be filled.</param>
        /// <param name="query">The query string.</param>
        /// <param name="parameters">The query parameters.</param>
        public void FillDataSet(DbConnection connection, DbTransaction transaction, DataSet fillTarget, SQLQueryString query, IEnumerable<DbParameter> parameters)
        {
            ModifiedRowCount rowCount = ModifiedRowCount.Invalid;
            Stopwatch stopwatch = new Stopwatch();
            try
            {
                this.innerDriver.FillDataSet(connection, transaction, fillTarget, query, parameters);

                int rowsInDS = (fillTarget.Tables.Count > 0) ? fillTarget.Tables[0].Rows.Count : 0;
                ModifiedRowCount? check = ModifiedRowCount.Create(rowsInDS); // don't bother checking for null
                rowCount = check.Value;
            }
            finally
            {
                stopwatch.Stop();
                this.LogIfTooLong(stopwatch, query, rowCount, ReadThresholdInMs);
            }
        }

        /// <summary>
        /// Retrieve data from a SQL database and populate the input data set using the pre-prepared adapter.
        /// </summary>
        /// <param name="adapter">The adapter used to fill the data set.</param>
        /// <param name="fillTarget">The data set that will be filled.</param>
        /// <param name="tableName">Optional value that is the table name for updating.</param>
        public void FillDataSet(DbDataAdapter adapter, DataSet fillTarget, DBTableName? tableName)
        {
            this.innerDriver.FillDataSet(adapter, fillTarget, tableName);
        }

        /// <summary>
        /// Update data to a SQL database and with changes in the data set using the pre-prepared adapter.
        /// </summary>
        /// <param name="adapter">The adapter used to update the data set.</param>
        /// <param name="withChanges">The data set that will be updated to the SQL database.</param>
        /// <param name="tableName">Optional value that is the table name for updating.</param>
        /// <returns>The number of rows that were modified.</returns>
        public ModifiedRowCount UpdateDataSet(DbDataAdapter adapter, DataSet withChanges, DBTableName? tableName)
        {
            return this.innerDriver.UpdateDataSet(adapter, withChanges, tableName);
        }

        /// <summary>
        /// Insert a record into a table that has a uniqueidentifier-valued primary key.
        /// </summary>
        /// <param name="connection">The connection to the DB.</param>
        /// <param name="transaction">If using a transaction, pass it in here, else pass in null.</param>
        /// <param name="query">The query string.</param>
        /// <param name="parameters">The query parameters.</param>
        /// <returns>The primary key of the inserted record.</returns>
        public PrimaryKeyGuid InsertGetGuid(DbConnection connection, DbTransaction transaction, SQLQueryString query, IEnumerable<DbParameter> parameters)
        {
            ModifiedRowCount rowCount = ModifiedRowCount.Invalid;
            Stopwatch stopwatch = new Stopwatch();
            try
            {
                var pk = this.innerDriver.InsertGetGuid(connection, transaction, query, parameters);

                int rowsInserted = (pk != PrimaryKeyGuid.Invalid) ? 1 : 0;
                ModifiedRowCount? check = ModifiedRowCount.Create(rowsInserted); // don't bother checking for null
                rowCount = check.Value;

                return pk;
            }
            finally
            {
                stopwatch.Stop();
                this.LogIfTooLong(stopwatch, query, rowCount, WriteThresholdInMs);
            }
        }

        /// <summary>
        /// Insert a record into a table that has an integer-valued primary key.
        /// </summary>
        /// <param name="connection">The connection to the DB.</param>
        /// <param name="transaction">If using a transaction, pass it in here, else pass in null.</param>
        /// <param name="query">The query string.</param>
        /// <param name="parameters">The query parameters.</param>
        /// <returns>The primary key of the inserted record.</returns>
        public PrimaryKeyInt32 InsertGetInt32(DbConnection connection, DbTransaction transaction, SQLQueryString query, IEnumerable<DbParameter> parameters)
        {
            ModifiedRowCount rowCount = ModifiedRowCount.Invalid;
            Stopwatch stopwatch = new Stopwatch();
            try
            {
                var pk = this.innerDriver.InsertGetInt32(connection, transaction, query, parameters);

                int rowsInserted = (pk != PrimaryKeyInt32.Invalid) ? 1 : 0;
                ModifiedRowCount? check = ModifiedRowCount.Create(rowsInserted); // don't bother checking for null
                rowCount = check.Value;

                return pk;
            }
            finally
            {
                stopwatch.Stop();
                this.LogIfTooLong(stopwatch, query, rowCount, WriteThresholdInMs);
            }
        }

        /// <summary>
        /// Insert a record into a table but do not retrieve the primary key.
        /// This is usually used for tables where the primary key is a set
        /// of columns rather than a generated key.
        /// </summary>
        /// <param name="connection">The connection to the DB.</param>
        /// <param name="transaction">If using a transaction, pass it in here, else pass in null.</param>
        /// <param name="query">The query string.</param>
        /// <param name="parameters">The query parameters.</param>
        public void InsertNoKey(DbConnection connection, DbTransaction transaction, SQLQueryString query, IEnumerable<DbParameter> parameters)
        {
            ModifiedRowCount rowCount = ModifiedRowCount.Invalid;
            Stopwatch stopwatch = new Stopwatch();
            try
            {
                this.innerDriver.InsertNoKey(connection, transaction, query, parameters);

                ModifiedRowCount? check = ModifiedRowCount.Create(1); // don't bother checking for null
                rowCount = check.Value;
            }
            finally
            {
                stopwatch.Stop();
                this.LogIfTooLong(stopwatch, query, rowCount, WriteThresholdInMs);
            }
        }

        /// <summary>
        /// Retrieve data from a SQL database.
        /// </summary>
        /// <param name="connection">The connection to the DB.</param>
        /// <param name="transaction">If using a transaction, pass it in here, else pass in null.</param>
        /// <param name="query">The query string.</param>
        /// <param name="parameters">The query parameters.</param>
        /// <returns>The retrieved data.</returns>
        public DbDataReader Select(DbConnection connection, DbTransaction transaction, SQLQueryString query, IEnumerable<DbParameter> parameters)
        {
            ModifiedRowCount rowCount = ModifiedRowCount.Invalid;
            Stopwatch stopwatch = new Stopwatch();
            try
            {
                var reader = this.innerDriver.Select(connection, transaction, query, parameters);

                // I don't really know how many rows have been returned, so I'll just use the value 1.
                ModifiedRowCount? check = ModifiedRowCount.Create(1); // don't bother checking for null
                rowCount = check.Value;

                return reader;
            }
            finally
            {
                stopwatch.Stop();
                this.LogIfTooLong(stopwatch, query, rowCount, ReadThresholdInMs);
            }
        }

        /// <summary>
        /// Update data held in a SQL database.
        /// </summary>
        /// <param name="connection">The connection to the DB.</param>
        /// <param name="transaction">If using a transaction, pass it in here, else pass in null.</param>
        /// <param name="query">The query string.</param>
        /// <param name="parameters">The query parameters.</param>
        /// <returns>The number of rows that were modified.</returns>
        public ModifiedRowCount Update(DbConnection connection, DbTransaction transaction, SQLQueryString query, IEnumerable<DbParameter> parameters)
        {
            ModifiedRowCount rowCount = ModifiedRowCount.Invalid;
            Stopwatch stopwatch = new Stopwatch();
            try
            {
                rowCount = this.innerDriver.Update(connection, transaction, query, parameters);
                return rowCount;
            }
            finally
            {
                stopwatch.Stop();
                this.LogIfTooLong(stopwatch, query, rowCount, WriteThresholdInMs);
            }
        }

        /// <summary>
        /// If the stopwatch is too long, log the timing and the sql query.
        /// </summary>
        /// <param name="stopwatch">The stopwatch that contains the time it took to execute the sql query.</param>
        /// <param name="query">The sql query that was executed.</param>
        /// <param name="rowCount">The number of rows affected by the query.</param>
        /// <param name="threshold">Log stopwatch times greater than this value.</param>
        private void LogIfTooLong(Stopwatch stopwatch, SQLQueryString query, ModifiedRowCount? rowCount, int threshold)
        {
            if (stopwatch.ElapsedMilliseconds >= threshold)
            {
                int numRecords = 0;
                if (rowCount != null)
                {
                    numRecords = rowCount.Value.Value;
                }

                string sqlQuery = (query.Value.Length > 405) ? query.Value.Substring(0, 400) + " ... " : query.Value;

                Tools.LogInfo("Timing", string.Format("StopwatchDriver( {0} ) has {1} records. Execute in {2} ms.", sqlQuery, numRecords.ToString(), stopwatch.ElapsedMilliseconds.ToString()));
            }
        }
    }
}