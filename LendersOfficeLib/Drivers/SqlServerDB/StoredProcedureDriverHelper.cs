﻿namespace LendersOffice.Drivers.SqlServerDB
{
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
using System.Data.SqlClient;
    using LqbGrammar;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;

    /// <summary>
    /// Utility for interacting with the stored procedure driver.
    /// </summary>
    public static class StoredProcedureDriverHelper
    {
        /// <summary>
        /// Call a stored procedure that returns data, with the full data stored in a DataSet.
        /// </summary>
        /// <param name="conn">The connection to the DB.</param>
        /// <param name="transaction">If using a transaction, pass it in here, else pass in null.</param>
        /// <param name="procedureName">The name of the stored procedure.</param>
        /// <param name="parameters">The procedure's parameters.</param>
        /// <param name="timeout">Timeout to use when calling SQL Server.</param>
        /// <returns>The data set containing the full returned data.</returns>
        public static DataSet ExecuteDataSet(DbConnection conn, DbTransaction transaction, StoredProcedureName procedureName, IEnumerable<SqlParameter> parameters, TimeoutInSeconds timeout)
        {
            var driver = GetDriver(timeout);
            return driver.ExecuteDataSet(conn, transaction, procedureName, parameters);
        }

        /// <summary>
        /// Execute a stored procedure that modifies data.
        /// </summary>
        /// <param name="conn">The connection to the DB.</param>
        /// <param name="transaction">If using a transaction, pass it in here, else pass in null.</param>
        /// <param name="procedureName">The name of the stored procedure.</param>
        /// <param name="parameters">The procedure's parameters.</param>
        /// <param name="timeout">Timeout to use when calling SQL Server.</param>
        /// <returns>The number of rows modified by the stored procedure.</returns>
        public static ModifiedRowCount ExecuteNonQuery(DbConnection conn, DbTransaction transaction, StoredProcedureName procedureName, IEnumerable<SqlParameter> parameters, TimeoutInSeconds timeout)
        {
            var driver = GetDriver(timeout);
            return driver.ExecuteNonQuery(conn, transaction, procedureName, parameters);
        }

        /// <summary>
        /// Call a stored procedure that returns a row set.
        /// </summary>
        /// <param name="conn">The connection to the DB.</param>
        /// <param name="transaction">If using a transaction, pass it in here, else pass in null.</param>
        /// <param name="procedureName">The name of the stored procedure.</param>
        /// <param name="parameters">The procedure's parameters.</param>
        /// <param name="timeout">Timeout to use when calling SQL Server.</param>
        /// <returns>An interface used to iterate through the returned row set.</returns>
        public static IDataReader ExecuteReader(DbConnection conn, DbTransaction transaction, StoredProcedureName procedureName, IEnumerable<SqlParameter> parameters, TimeoutInSeconds timeout)
        {
            var driver = GetDriver(timeout);
            return driver.ExecuteReader(conn, transaction, procedureName, parameters);
        }

        /// <summary>
        /// Call a stored procedure that returns a single value.
        /// </summary>
        /// <param name="conn">The connection to the DB.</param>
        /// <param name="transaction">If using a transaction, pass it in here, else pass in null.</param>
        /// <param name="procedureName">The name of the stored procedure.</param>
        /// <param name="parameters">The procedure's parameters.</param>
        /// <param name="timeout">Timeout to use when calling SQL Server.</param>
        /// <returns>The single valued return data.</returns>
        public static object ExecuteScalar(DbConnection conn, DbTransaction transaction, StoredProcedureName procedureName, IEnumerable<SqlParameter> parameters, TimeoutInSeconds timeout)
        {
            var driver = GetDriver(timeout);
            return driver.ExecuteScalar(conn, transaction, procedureName, parameters);
        }

        /// <summary>
        /// Retrieve the driver.
        /// </summary>
        /// <param name="timeout">Timeout to use when calling SQL Server.</param>
        /// <returns>The driver.</returns>
        private static IStoredProcedureDriver GetDriver(TimeoutInSeconds timeout)
        {
            var factory = GenericLocator<IStoredProcedureDriverFactory>.Factory;
            return factory.Create(timeout);
        }
    }
}
