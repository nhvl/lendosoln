﻿namespace LendersOffice.Drivers.SqlServerDB
{
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using LqbGrammar;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;

    /// <summary>
    /// Utility for interacting with the sql server ad hoc query driver.
    /// </summary>
    public static class SqlServerHelper
    {
        /// <summary>
        /// Return a specific driver to the client code.
        /// </summary>
        /// <param name="timeout">Timeout to use when calling SQL Server.</param>
        /// <param name="decorator">The desired driver type.</param>
        /// <returns>A specific driver.</returns>
        public static ISqlDriver CreateSpecificDriver(TimeoutInSeconds timeout, SqlDecoratorType decorator)
        {
            // I'm exposing this as public because otherwise I'd have to implement
            // two versions of every ISqlDriver method.
            var factory = GenericLocator<ISqlDriverFactory>.Factory;
            return factory.Create(timeout, decorator);
        }

        /// <summary>
        /// Delete data from a SQL database.
        /// </summary>
        /// <param name="connection">The connection to the DB.</param>
        /// <param name="transaction">If using a transaction, pass it in here, else pass in null.</param>
        /// <param name="query">The query string.</param>
        /// <param name="parameters">The query parameters.</param>
        /// <param name="timeout">Timeout to use when calling SQL Server.</param>
        /// <returns>The number of rows that were deleted.</returns>
        public static ModifiedRowCount Delete(DbConnection connection, DbTransaction transaction, SQLQueryString query, IEnumerable<SqlParameter> parameters, TimeoutInSeconds timeout)
        {
            var driver = GetDriver(timeout);
            return driver.Delete(connection, transaction, query, parameters);
        }

        /// <summary>
        /// Insert a record into a table but do not retrieve the primary key.
        /// This is usually used for tables where the primary key is a set
        /// of columns rather than a generated key.
        /// </summary>
        /// <param name="connection">The connection to the DB.</param>
        /// <param name="transaction">If using a transaction, pass it in here, else pass in null.</param>
        /// <param name="query">The query string.</param>
        /// <param name="parameters">The query parameters.</param>
        /// <param name="timeout">Timeout to use when calling SQL Server.</param>
        public static void InsertNoKey(DbConnection connection, DbTransaction transaction, SQLQueryString query, IEnumerable<SqlParameter> parameters, TimeoutInSeconds timeout)
        {
            var driver = GetDriver(timeout);
            driver.InsertNoKey(connection, transaction, query, parameters);
        }

        /// <summary>
        /// Insert a record into a table that has a uniqueidentifier-valued primary key.
        /// </summary>
        /// <param name="connection">The connection to the DB.</param>
        /// <param name="transaction">If using a transaction, pass it in here, else pass in null.</param>
        /// <param name="query">The query string.</param>
        /// <param name="parameters">The query parameters.</param>
        /// <param name="timeout">Timeout to use when calling SQL Server.</param>
        /// <returns>The primary key of the inserted record.</returns>
        public static PrimaryKeyGuid InsertGetGuid(DbConnection connection, DbTransaction transaction, SQLQueryString query, IEnumerable<SqlParameter> parameters, TimeoutInSeconds timeout)
        {
            var driver = GetDriver(timeout);
            return driver.InsertGetGuid(connection, transaction, query, parameters);
        }

        /// <summary>
        /// Insert a record into a table that has an integer-valued primary key.
        /// </summary>
        /// <param name="connection">The connection to the DB.</param>
        /// <param name="transaction">If using a transaction, pass it in here, else pass in null.</param>
        /// <param name="query">The query string.</param>
        /// <param name="parameters">The query parameters.</param>
        /// <param name="timeout">Timeout to use when calling SQL Server.</param>
        /// <returns>The primary key of the inserted record.</returns>
        public static PrimaryKeyInt32 InsertGetInt32(DbConnection connection, DbTransaction transaction, SQLQueryString query, IEnumerable<SqlParameter> parameters, TimeoutInSeconds timeout)
        {
            var driver = GetDriver(timeout);
            return driver.InsertGetInt32(connection, transaction, query, parameters);
        }

        /// <summary>
        /// Retrieve data from a SQL database.
        /// </summary>
        /// <param name="connection">The connection to the DB.</param>
        /// <param name="transaction">If using a transaction, pass it in here, else pass in null.</param>
        /// <param name="query">The query string.</param>
        /// <param name="parameters">The query parameters.</param>
        /// <param name="timeout">Timeout to use when calling SQL Server.</param>
        /// <returns>The retrieved data.</returns>
        public static IDataReader Select(DbConnection connection, DbTransaction transaction, SQLQueryString query, IEnumerable<SqlParameter> parameters, TimeoutInSeconds timeout)
        {
            var driver = GetDriver(timeout);
            return driver.Select(connection, transaction, query, parameters);
        }

        /// <summary>
        /// Retrieve data from a SQL database and populate the input data set.
        /// </summary>
        /// <param name="connection">The connection to the DB.</param>
        /// <param name="transaction">If using a transaction, pass it in here, else pass in null.</param>
        /// <param name="fillTarget">The data set that will be filled.</param>
        /// <param name="query">The query string.</param>
        /// <param name="parameters">The query parameters.</param>
        /// <param name="timeout">Timeout to use when calling SQL Server.</param>
        public static void FillDataSet(DbConnection connection, DbTransaction transaction, DataSet fillTarget, SQLQueryString query, IEnumerable<SqlParameter> parameters, TimeoutInSeconds timeout)
        {
            var driver = GetDriver(timeout);
            driver.FillDataSet(connection, transaction, fillTarget, query, parameters);
        }

        /// <summary>
        /// Retrieve data from a SQL database and populate the input data set using the pre-prepared adapter.
        /// </summary>
        /// <param name="adapter">The adapter used to fill the data set.</param>
        /// <param name="fillTarget">The data set that will be filled.</param>
        /// <param name="tableName">Optional value that is the table name for updating.</param>
        /// <param name="timeout">Timeout to use when calling SQL Server.</param>
        public static void FillDataSet(DbDataAdapter adapter, DataSet fillTarget, DBTableName? tableName, TimeoutInSeconds timeout)
        {
            var driver = GetDriver(timeout);
            driver.FillDataSet(adapter, fillTarget, tableName);
        }

        /// <summary>
        /// Update data to a SQL database and with changes in the data set using the pre-prepared adapter.
        /// </summary>
        /// <param name="adapter">The adapter used to update the data set.</param>
        /// <param name="withChanges">The data set that will be updated to the SQL database.</param>
        /// <param name="tableName">Optional value that is the table name for updating.</param>
        /// <param name="timeout">Timeout to use when calling SQL Server.</param>
        /// <returns>The number of rows that were modified.</returns>
        public static ModifiedRowCount UpdateDataSet(DbDataAdapter adapter, DataSet withChanges, DBTableName? tableName, TimeoutInSeconds timeout)
        {
            var driver = GetDriver(timeout);
            return driver.UpdateDataSet(adapter, withChanges, tableName);
        }

        /// <summary>
        /// Update data held in a SQL database.
        /// </summary>
        /// <param name="connection">The connection to the DB.</param>
        /// <param name="transaction">If using a transaction, pass it in here, else pass in null.</param>
        /// <param name="query">The query string.</param>
        /// <param name="parameters">The query parameters.</param>
        /// <param name="timeout">Timeout to use when calling SQL Server.</param>
        /// <returns>The number of rows that were modified.</returns>
        public static ModifiedRowCount Update(DbConnection connection, DbTransaction transaction, SQLQueryString query, IEnumerable<SqlParameter> parameters, TimeoutInSeconds timeout)
        {
            var driver = GetDriver(timeout);
            return driver.Update(connection, transaction, query, parameters);
        }

        /// <summary>
        /// Retrieve the default driver.
        /// </summary>
        /// <param name="timeout">Timeout to use when calling SQL Server.</param>
        /// <returns>The default driver.</returns>
        private static ISqlDriver GetDriver(TimeoutInSeconds timeout)
        {
            var factory = GenericLocator<ISqlDriverFactory>.Factory;
            return factory.Create(timeout);
        }
    }
}
