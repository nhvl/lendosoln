﻿namespace LendersOffice.Drivers.FileDB
{
    using System;
    using LqbGrammar.Adapters;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Driver for the FileDB system that can suppress encryption on Save.
    /// </summary>
    public sealed class FileDbControlledEncryptionDriver : IFileDbDriver
    {
        /// <summary>
        /// Flag to determine whether or not to encrypt files when saving.
        /// </summary>
        private bool encryptOnSave;

        /// <summary>
        /// The adapter that is used to communicate with the FileDB system.
        /// </summary>
        private IFileDbAdapter adapter;

        /// <summary>
        /// A driver that does the encryption/decryption.
        /// </summary>
        private IFileDbDriver encryptingDriver;

        /// <summary>
        /// Initializes a new instance of the <see cref="FileDbControlledEncryptionDriver"/> class.
        /// </summary>
        /// <param name="encryptOnSave">True if the filedb files should be encrypted prior to saving, false otherwise.</param>
        /// <param name="adapter">An adapter used to implement the communication with the FileDB system.</param>
        /// <param name="encryptingDriver">A driver implementation that does the encryption/decryption of fileddb files.</param>
        internal FileDbControlledEncryptionDriver(bool encryptOnSave, IFileDbAdapter adapter, IFileDbDriver encryptingDriver)
        {
            this.encryptOnSave = encryptOnSave;
            this.adapter = adapter;
            this.encryptingDriver = encryptingDriver;
        }

        /// <summary>
        /// Delete the file from the file storage.
        /// </summary>
        /// <param name="location">The location of the file storage repository.</param>
        /// <param name="key">The file identifier used to delete.</param>
        public void DeleteFile(FileStorageIdentifier location, FileIdentifier key)
        {
            this.encryptingDriver.DeleteFile(location, key);
        }

        /// <summary>
        /// Check whether the given file exist in the file storage.
        /// </summary>
        /// <param name="location">The location of the file storage repository.</param>
        /// <param name="key">The file identifier.</param>
        /// <returns>True if the file exist false otherwise.</returns>
        public bool FileExists(FileStorageIdentifier location, FileIdentifier key)
        {
            return this.encryptingDriver.FileExists(location, key);
        }

        /// <summary>
        /// Save a file that was previously retrieved from the file storage using the UseFile method.
        /// </summary>
        /// <param name="location">The location of the file storage repository.</param>
        /// <param name="key">The file identifier used to retrieve the file later, this can be the same as or different from the key passed into the UseFile method.</param>
        /// <param name="token">A file token that holds the location of the file in the local environment.</param>
        public void ReSaveFile(FileStorageIdentifier location, FileIdentifier key, IFileToken token)
        {
            if (this.encryptOnSave)
            {
                this.encryptingDriver.ReSaveFile(location, key, token);
            }
            else
            {
                this.adapter.ReSaveFile(location, key, null, token);
            }
        }

        /// <summary>
        /// Retrieve a file from the file storage.
        /// </summary>
        /// <param name="location">The location of the file storage repository.</param>
        /// <param name="key">The file identifier previously used to save the file.</param>
        /// <param name="readFileHandler">A delegate which will read the file from the location provided in the LocalFilePath.</param>
        public void RetrieveFile(FileStorageIdentifier location, FileIdentifier key, Action<LocalFilePath> readFileHandler)
        {
            this.encryptingDriver.RetrieveFile(location, key, readFileHandler);
        }

        /// <summary>
        /// Save a new file into the file storage.
        /// </summary>
        /// <param name="location">The location of the file storage repository.</param>
        /// <param name="key">The file identifier used to retrieve the file later.</param>
        /// <param name="saveFileHandler">A delegate which will save the file to the location provided in the LocalFilePath.</param>
        public void SaveNewFile(FileStorageIdentifier location, FileIdentifier key, Action<LocalFilePath> saveFileHandler)
        {
            if (this.encryptOnSave)
            {
                this.encryptingDriver.SaveNewFile(location, key, saveFileHandler);
            }
            else
            {
                this.adapter.SaveNewFile(location, key, null, saveFileHandler);
            }
        }

        /// <summary>
        /// Retrieve a file from the file storage and return a token that can be used to re-store the file.
        /// </summary>
        /// <param name="location">The location of the file storage repository.</param>
        /// <param name="key">The file identifier previously used to save the file.</param>
        /// <param name="useFileHandler">A delegate which will read the file from the location provided in the IFileToken, and can re-store the file with this token.</param>
        public void UseFile(FileStorageIdentifier location, FileIdentifier key, Action<IFileToken> useFileHandler)
        {
            this.encryptingDriver.UseFile(location, key, useFileHandler);
        }
    }
}
