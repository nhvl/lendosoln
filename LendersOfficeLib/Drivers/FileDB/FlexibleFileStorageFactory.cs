﻿namespace LendersOffice.Drivers.FileDB
{
    using LqbGrammar;
    using LqbGrammar.Adapters;
    using LqbGrammar.Drivers;

    /// <summary>
    /// Factory for creating implementations of the IFileDbDriver interface.
    /// </summary>
    public sealed class FlexibleFileStorageFactory : IFileDbDriverFactory
    {
        /// <summary>
        /// Create an implementation of the IFileDbDriver interface.
        /// </summary>
        /// <returns>An implementation of the IFileDbDriver interface.</returns>
        public IFileDbDriver Create()
        {
            // Since this class is going to be registered as the factory implementing IFileDbDriverFactory,
            // I cannot use the locator to retrieve the factory for the simple implementation of IFileDbDriver.
            // Since we know that driver is what we want here, we can use the associated factory directly.
            FileDbDriverFactory filedbFactory = new FileDbDriverFactory();
            IFileDbDriver filedbDriver = filedbFactory.Create();

            // There is no factory for the AWS driver.  That system isn't used in isolation, only as an 
            // alternative to FileDB.  Accordingly, we will do the work here that an AWS driver factory
            // would have done.
            IAwsFileStorageAdapterFactory awsFactory = GenericLocator<IAwsFileStorageAdapterFactory>.Factory;
            IAwsFileStorageAdapter awsAdapter = awsFactory.Create();
            IFileDbDriver awsDriver = new AwsDriver(awsAdapter);

            return new FlexibleFileStorageDriver(filedbDriver, awsDriver);
        }
    }
}
