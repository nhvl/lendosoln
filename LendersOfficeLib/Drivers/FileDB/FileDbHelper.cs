﻿namespace LendersOffice.Drivers.FileDB
{
    using System;
    using LqbGrammar;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;

    /// <summary>
    /// Utility for interacting with the FileDB driver.
    /// </summary>
    public static class FileDbHelper
    {
        /// <summary>
        /// Save a file that was previously retrieved from the file storage using the UseFile method.
        /// </summary>
        /// <param name="location">The location of the file storage repository.</param>
        /// <param name="key">The file identifier used to retrieve the file later, this can be the same as or different from the key passed into the UseFile method.</param>
        /// <param name="token">A file token that holds the location of the file in the local environment.</param>
        public static void ReSaveFile(FileStorageIdentifier location, FileIdentifier key, IFileToken token)
        {
            var driver = GetDriver();
            driver.ReSaveFile(location, key, token);
        }

        /// <summary>
        /// Retrieve a file from the file storage.
        /// </summary>
        /// <param name="location">The location of the file storage repository.</param>
        /// <param name="key">The file identifier previously used to save the file.</param>
        /// <param name="readFileHandler">A delegate which will read the file from the location provided in the LocalFilePath.</param>
        public static void RetrieveFile(FileStorageIdentifier location, FileIdentifier key, Action<LocalFilePath> readFileHandler)
        {
            var driver = GetDriver();
            driver.RetrieveFile(location, key, readFileHandler);
        }

        /// <summary>
        /// Save a new file into the file storage.
        /// </summary>
        /// <param name="location">The location of the file storage repository.</param>
        /// <param name="key">The file identifier used to retrieve the file later.</param>
        /// <param name="saveFileHandler">A delegate which will save the file to the location provided in the LocalFilePath.</param>
        public static void SaveNewFile(FileStorageIdentifier location, FileIdentifier key, Action<LocalFilePath> saveFileHandler)
        {
            var driver = GetDriver();
            driver.SaveNewFile(location, key, saveFileHandler);
        }

        /// <summary>
        /// Retrieve a file from the file storage and return a token that can be used to re-store the file.
        /// </summary>
        /// <param name="location">The location of the file storage repository.</param>
        /// <param name="key">The file identifier previously used to save the file.</param>
        /// <param name="useFileHandler">A delegate which will read the file from the location provided in the IFileToken, and can re-store the file with this token.</param>
        public static void UseFile(FileStorageIdentifier location, FileIdentifier key, Action<IFileToken> useFileHandler)
        {
            var driver = GetDriver();
            driver.UseFile(location, key, useFileHandler);
        }

        /// <summary>
        /// Delete the file from the file storage.
        /// </summary>
        /// <param name="location">The location of the file storage repository.</param>
        /// <param name="key">The file identifier used to delete.</param>
        public static void DeleteFile(FileStorageIdentifier location, FileIdentifier key)
        {
            var driver = GetDriver();
            driver.DeleteFile(location, key);
        }

        /// <summary>
        /// Check whether the given file exist in the file storage.
        /// </summary>
        /// <param name="location">The location of the file storage repository.</param>
        /// <param name="key">The file identifier.</param>
        /// <returns>True if the file exist false otherwise.</returns>
        public static bool FileExists(FileStorageIdentifier location, FileIdentifier key)
        {
            var driver = GetDriver();
            return driver.FileExists(location, key);
        }

        /// <summary>
        /// Retrieve the driver.
        /// </summary>
        /// <returns>The driver.</returns>
        private static IFileDbDriver GetDriver()
        {
            var factory = GenericLocator<IFileDbDriverFactory>.Factory;
            return factory.Create();
        }
    }
}
