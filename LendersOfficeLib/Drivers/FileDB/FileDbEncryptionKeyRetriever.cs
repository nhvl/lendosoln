﻿namespace LendersOffice.Drivers.FileDB
{
    using System;
    using System.Data.Common;
using System.Data.SqlClient;
    using DataAccess;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Class used to retrieve a key that encrypts/decrypts FileDB files.
    /// </summary>
    public static class FileDbEncryptionKeyRetriever
    {
        /// <summary>
        /// Name of the stored procedure used to pull the encryption key.
        /// </summary>
        private const string SPNAME = "FILE_DB_ENCRYPTION_KEYS_Retrieve_Key";

        /// <summary>
        /// The minimum value the primary key can take.
        /// </summary>
        private const int MINVAL = 0;

        /// <summary>
        /// The maximum value the primary key can take.
        /// </summary>
        private const int MAXVAL = 0xFFFF;

        /// <summary>
        /// Retrieve a FileDB encryption key from the database.
        /// </summary>
        /// <param name="id">The primary key into the database table.</param>
        /// <returns>The encryption key.</returns>
        public static byte[] RetrieveKey(int id)
        {
            if (id < MINVAL || id > MAXVAL)
            {
                throw new DeveloperException(ErrorMessage.SystemError);
            }

            var parameters = new SqlParameter[] { new SqlParameter("@keyId", id) };
            object o = StoredProcedureHelper.ExecuteScalar(DataSrc.LOShareROnly, SPNAME, parameters);
            if (Convert.IsDBNull(o) || (o == null))
            {
                return null;
            }

            return Convert.FromBase64String(Convert.ToString(o));
        }
    }
}
