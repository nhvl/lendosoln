﻿namespace LendersOffice.Drivers.FileDB
{
    using LendersOffice.Constants;
    using LqbGrammar;
    using LqbGrammar.Adapters;
    using LqbGrammar.Drivers;

    /// <summary>
    /// Factory for creating implementations of the IFileDbDriver interface.
    /// </summary>
    public sealed class FileDbDriverFactory : IFileDbDriverFactory
    {
        /// <summary>
        /// Create an implementation of the IFileDbDriver interface.
        /// </summary>
        /// <returns>An implementation of the IFileDbDriver interface.</returns>
        public IFileDbDriver Create()
        {
            IFileDbAdapterFactory adapterFactory = GenericLocator<IFileDbAdapterFactory>.Factory;
            IFileDbAdapter adapter = adapterFactory.Create();

            IFileDbDriver encryptingDriver = new FileDbDriver(adapter);
            IFileDbDriver controlDriver = new FileDbControlledEncryptionDriver(ConstStage.UseFileDbEncryption, adapter, encryptingDriver);

            return new LogTimingFileDbDriver(controlDriver);
        }
    }
}
