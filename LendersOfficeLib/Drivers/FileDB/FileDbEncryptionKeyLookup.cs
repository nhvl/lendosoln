﻿namespace LendersOffice.Drivers.FileDB
{
    using System;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Utility class to calculate the lookup identifier for an encryption key used for FileDB files.
    /// </summary>
    public static class FileDbEncryptionKeyLookup
    {
        /// <summary>
        /// Calculate the lookup identifier.
        /// </summary>
        /// <param name="location">The FileDB location identifier.</param>
        /// <param name="key">The FileDB file identifier.</param>
        /// <returns>An integer in the range [0, 2^16 - 1] used to lookup the encryption key.</returns>
        public static int CalculateLookup(FileStorageIdentifier location, FileIdentifier key)
        {
            string lookup = EncryptionLookupKeyString(location, key);
            byte[] input = System.Text.Encoding.UTF8.GetBytes(lookup);
            byte[] output = null;
            using (var hasher = System.Security.Cryptography.SHA256Managed.Create())
            {
                output = hasher.ComputeHash(input);
            }

            return CompressBytes(output);
        }

        /// <summary>
        /// Combine the location and key into a single string.
        /// </summary>
        /// <param name="location">The FileDB location identifier.</param>
        /// <param name="key">The FileDB file identifier.</param>
        /// <returns>A string that combines the location and key.</returns>
        private static string EncryptionLookupKeyString(FileStorageIdentifier location, FileIdentifier key)
        {
            return string.Concat(location.ToString(), "|", key.ToString());
        }

        /// <summary>
        /// The hash algorithm generates 256 bits, or 32 bytes.  Compactify the result into a 16 bit uint via xor.
        /// </summary>
        /// <param name="compressSource">The result of the hash algorithm.</param>
        /// <returns>An integer in the range [0, 2^16-1].</returns>
        private static int CompressBytes(byte[] compressSource)
        {
            ushort value = 0;
            for (int i = 0; i < compressSource.Length / 2; ++i)
            {
                byte b0 = compressSource[2 * i];
                byte b1 = compressSource[(2 * i) + 1];

                ushort u1 = Convert.ToUInt16(b1);
                ushort u0 = Convert.ToUInt16(Convert.ToUInt16(b0) << 8);
                ushort field = Convert.ToUInt16(u0 + u1);

                value ^= field;
            }

            return Convert.ToInt32(value);
        }
    }
}
