﻿namespace LendersOffice.Drivers.FileDB
{
    using System;
    using LqbGrammar.Adapters;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;

    /// <summary>
    /// Driver for the AWS file storage system.
    /// </summary>
    internal sealed class AwsDriver : IFileDbDriver
    {
        /// <summary>
        /// The adapter that is used to communicate with the FileDB system.
        /// </summary>
        private IAwsFileStorageAdapter adapter;

        /// <summary>
        /// Initializes a new instance of the <see cref="AwsDriver"/> class.
        /// </summary>
        /// <param name="adapter">An adapter used to implement the communication with the AWS file storage system.</param>
        internal AwsDriver(IAwsFileStorageAdapter adapter)
        {
            this.adapter = adapter;
        }

        /// <summary>
        /// Save a file that was previously retrieved from the file storage using the UseFile method.
        /// </summary>
        /// <param name="location">The location of the file storage repository.</param>
        /// <param name="key">The file identifier used to retrieve the file later, this can be the same as or different from the key passed into the UseFile method.</param>
        /// <param name="token">A file token that holds the location of the file in the local environment.</param>
        public void ReSaveFile(FileStorageIdentifier location, FileIdentifier key, IFileToken token)
        {
            this.adapter.ReSaveFile(location, key, token);
        }

        /// <summary>
        /// Retrieve a file from the file storage.
        /// </summary>
        /// <param name="location">The location of the file storage repository.</param>
        /// <param name="key">The file identifier previously used to save the file.</param>
        /// <param name="readFileHandler">A delegate which will read the file from the location provided in the LocalFilePath.</param>
        public void RetrieveFile(FileStorageIdentifier location, FileIdentifier key, Action<LocalFilePath> readFileHandler)
        {
            this.adapter.RetrieveFile(location, key, readFileHandler);
        }

        /// <summary>
        /// Save a new file into the file storage.
        /// </summary>
        /// <param name="location">The location of the file storage repository.</param>
        /// <param name="key">The file identifier used to retrieve the file later.</param>
        /// <param name="saveFileHandler">A delegate which will save the file to the location provided in the LocalFilePath.</param>
        public void SaveNewFile(FileStorageIdentifier location, FileIdentifier key, Action<LocalFilePath> saveFileHandler)
        {
            this.adapter.SaveNewFile(location, key, saveFileHandler);
        }

        /// <summary>
        /// Retrieve a file from the file storage and return a token that can be used to re-store the file.
        /// </summary>
        /// <param name="location">The location of the file storage repository.</param>
        /// <param name="key">The file identifier previously used to save the file.</param>
        /// <param name="useFileHandler">A delegate which will read the file from the location provided in the IFileToken, and can re-store the file with this token.</param>
        public void UseFile(FileStorageIdentifier location, FileIdentifier key, Action<IFileToken> useFileHandler)
        {
            this.adapter.UseFile(location, key, useFileHandler);
        }

        /// <summary>
        /// Delete the file from the file storage.
        /// </summary>
        /// <param name="location">The location of the file storage repository.</param>
        /// <param name="key">The file identifier used to delete.</param>
        public void DeleteFile(FileStorageIdentifier location, FileIdentifier key)
        {
            this.adapter.DeleteFile(location, key);
        }

        /// <summary>
        /// Check whether the given file exist in the file storage.
        /// </summary>
        /// <param name="location">The location of the file storage repository.</param>
        /// <param name="key">The file identifier.</param>
        /// <returns>True if the file exist false otherwise.</returns>
        public bool FileExists(FileStorageIdentifier location, FileIdentifier key)
        {
            return this.adapter.FileExists(location, key);
        }
    }
}
