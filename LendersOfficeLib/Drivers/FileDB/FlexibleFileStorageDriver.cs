﻿namespace LendersOffice.Drivers.FileDB
{
    using System;
    using Constants;
    using LqbGrammar.Adapters;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;

    /// <summary>
    /// Decorator class that can switch between FileDB and AWS file storage systems.
    /// </summary>
    internal sealed class FlexibleFileStorageDriver : IFileDbDriver
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FlexibleFileStorageDriver"/> class.
        /// </summary>
        /// <param name="filedbDriver">The FileDB driver.</param>
        /// <param name="awsDriver">The AWS driver.</param>
        public FlexibleFileStorageDriver(IFileDbDriver filedbDriver, IFileDbDriver awsDriver)
        {
            this.FiledbDriver = filedbDriver;
            this.AwsDriver = awsDriver;
        }

        /// <summary>
        /// Gets or sets the FileDB driver.
        /// </summary>
        /// <value>The FileDB driver.</value>
        private IFileDbDriver FiledbDriver { get; set; }

        /// <summary>
        /// Gets or sets the AWS driver.
        /// </summary>
        /// <value>The AWS driver.</value>
        private IFileDbDriver AwsDriver { get; set; }

        /// <summary>
        /// Save a file that was previously retrieved from the file storage using the UseFile method.
        /// </summary>
        /// <param name="location">The location of the file storage repository.</param>
        /// <param name="key">The file identifier used to retrieve the file later, this can be the same as or different from the key passed into the UseFile method.</param>
        /// <param name="token">A file token that holds the location of the file in the local environment.</param>
        public void ReSaveFile(FileStorageIdentifier location, FileIdentifier key, IFileToken token)
        {
            if (this.UseAWS())
            {
                this.AwsDriver.ReSaveFile(location, key, token);
            }
            else
            {
                this.FiledbDriver.ReSaveFile(location, key, token);
            }
        }

        /// <summary>
        /// Retrieve a file from the file storage.
        /// </summary>
        /// <param name="location">The location of the file storage repository.</param>
        /// <param name="key">The file identifier previously used to save the file.</param>
        /// <param name="readFileHandler">A delegate which will read the file from the location provided in the LocalFilePath.</param>
        public void RetrieveFile(FileStorageIdentifier location, FileIdentifier key, Action<LocalFilePath> readFileHandler)
        {
            if (this.UseAWS())
            {
                this.AwsDriver.RetrieveFile(location, key, readFileHandler);
            }
            else
            {
                this.FiledbDriver.RetrieveFile(location, key, readFileHandler);
            }
        }

        /// <summary>
        /// Save a new file into the file storage.
        /// </summary>
        /// <param name="location">The location of the file storage repository.</param>
        /// <param name="key">The file identifier used to retrieve the file later.</param>
        /// <param name="saveFileHandler">A delegate which will save the file to the location provided in the LocalFilePath.</param>
        public void SaveNewFile(FileStorageIdentifier location, FileIdentifier key, Action<LocalFilePath> saveFileHandler)
        {
            if (this.UseAWS())
            {
                this.AwsDriver.SaveNewFile(location, key, saveFileHandler);
            }
            else
            {
                this.FiledbDriver.SaveNewFile(location, key, saveFileHandler);
            }
        }

        /// <summary>
        /// Retrieve a file from the file storage and return a token that can be used to re-store the file.
        /// </summary>
        /// <param name="location">The location of the file storage repository.</param>
        /// <param name="key">The file identifier previously used to save the file.</param>
        /// <param name="useFileHandler">A delegate which will read the file from the location provided in the IFileToken, and can re-store the file with this token.</param>
        public void UseFile(FileStorageIdentifier location, FileIdentifier key, Action<IFileToken> useFileHandler)
        {
            if (this.UseAWS())
            {
                this.AwsDriver.UseFile(location, key, useFileHandler);
            }
            else
            {
                this.FiledbDriver.UseFile(location, key, useFileHandler);
            }
        }

        /// <summary>
        /// Delete the file from the file storage.
        /// </summary>
        /// <param name="location">The location of the file storage repository.</param>
        /// <param name="key">The file identifier used to delete.</param>
        public void DeleteFile(FileStorageIdentifier location, FileIdentifier key)
        {
            if (this.UseAWS())
            {
                this.AwsDriver.DeleteFile(location, key);
            }
            else
            {
                this.FiledbDriver.DeleteFile(location, key);
            }
        }

        /// <summary>
        /// Check whether the given file exist in the file storage.
        /// </summary>
        /// <param name="location">The location of the file storage repository.</param>
        /// <param name="key">The file identifier.</param>
        /// <returns>True if the file exist false otherwise.</returns>
        public bool FileExists(FileStorageIdentifier location, FileIdentifier key)
        {
            if (this.UseAWS())
            {
                return this.AwsDriver.FileExists(location, key);
            }
            else
            {
                return this.FiledbDriver.FileExists(location, key);
            }
        }

        /// <summary>
        /// Determine whether to AWS or FileDB.
        /// </summary>
        /// <returns>True if AWS, false for FileDB.</returns>
        private bool UseAWS()
        {
            return ConstStage.UseAwsS3Storage;
        }
    }
}