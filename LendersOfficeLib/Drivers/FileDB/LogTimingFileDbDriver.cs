﻿namespace LendersOffice.Drivers.FileDB
{
    using System;
    using System.Diagnostics;
    using System.IO;
    using LendersOffice.HttpModule;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;

    /// <summary>
    /// Log timing for FileDb driver.
    /// </summary>
    internal sealed class LogTimingFileDbDriver : IFileDbDriver
    {
        /// <summary>
        /// Handle the filedb call.
        /// </summary>
        private IFileDbDriver innerDriver;

        /// <summary>
        /// Full type name of inner driver. This is for logging purpose.
        /// </summary>
        private string innerDriverTypeName;

        /// <summary>
        /// Initializes a new instance of the <see cref="LogTimingFileDbDriver"/> class.
        /// </summary>
        /// <param name="driver">A driver implement the FileDB call.</param>
        internal LogTimingFileDbDriver(IFileDbDriver driver)
        {
            this.innerDriver = driver;
            this.innerDriverTypeName = driver.GetType().FullName;
        }

        /// <summary>
        /// Save a file that was previously retrieved from the file storage using the UseFile method.
        /// </summary>
        /// <param name="location">The location of the file storage repository.</param>
        /// <param name="key">The file identifier used to retrieve the file later, this can be the same as or different from the key passed into the UseFile method.</param>
        /// <param name="token">A file token that holds the location of the file in the local environment.</param>
        public void ReSaveFile(FileStorageIdentifier location, FileIdentifier key, IFileToken token)
        {
            string description = $"{innerDriverTypeName}::ReSaveFile - [{location}] [{key}]";
            using (PerformanceMonitorItem.Time(description))
            {
                this.innerDriver.ReSaveFile(location, key, token);
            }
        }

        /// <summary>
        /// Retrieve a file from the file storage.
        /// </summary>
        /// <param name="location">The location of the file storage repository.</param>
        /// <param name="key">The file identifier previously used to save the file.</param>
        /// <param name="readFileHandler">A delegate which will read the file from the location provided in the LocalFilePath.</param>
        public void RetrieveFile(FileStorageIdentifier location, FileIdentifier key, Action<LocalFilePath> readFileHandler)
        {
            long byteLength = -1;

            Action<LocalFilePath> wrapperHandler = delegate(LocalFilePath filePath) 
            {
                FileInfo fi = new FileInfo(filePath.Value);
                byteLength = fi.Length;

                readFileHandler(filePath);
            };

            Stopwatch sw = Stopwatch.StartNew();

            this.innerDriver.RetrieveFile(location, key, wrapperHandler);

            sw.Stop();

            var currentMonitor = PerformanceMonitorItem.Current;

            if (currentMonitor != null)
            {
                currentMonitor.AddFileDBGet(key.ToString(), location.ToString(), byteLength, sw.ElapsedMilliseconds, $"{innerDriverTypeName}:RetrieveFile");
            }
        }

        /// <summary>
        /// Save a new file into the file storage.
        /// </summary>
        /// <param name="location">The location of the file storage repository.</param>
        /// <param name="key">The file identifier used to retrieve the file later.</param>
        /// <param name="saveFileHandler">A delegate which will save the file to the location provided in the LocalFilePath.</param>
        public void SaveNewFile(FileStorageIdentifier location, FileIdentifier key, Action<LocalFilePath> saveFileHandler)
        {
            long byteLength = -1;

            Action<LocalFilePath> wrapperHandler = delegate(LocalFilePath filePath)
            {
                saveFileHandler(filePath);

                FileInfo fi = new FileInfo(filePath.Value);
                byteLength = fi.Length;
            };

            Stopwatch sw = Stopwatch.StartNew();

            this.innerDriver.SaveNewFile(location, key, wrapperHandler);

            sw.Stop();

            var currentMonitor = PerformanceMonitorItem.Current;

            if (currentMonitor != null)
            {
                currentMonitor.AddFileDBPut(key.ToString(), location.ToString(), byteLength, sw.ElapsedMilliseconds, $"{innerDriverTypeName}:SaveNewFile");
            }
        }

        /// <summary>
        /// Retrieve a file from the file storage and return a token that can be used to re-store the file.
        /// </summary>
        /// <param name="location">The location of the file storage repository.</param>
        /// <param name="key">The file identifier previously used to save the file.</param>
        /// <param name="useFileHandler">A delegate which will read the file from the location provided in the IFileToken, and can re-store the file with this token.</param>
        public void UseFile(FileStorageIdentifier location, FileIdentifier key, Action<IFileToken> useFileHandler)
        {
            string description = $"{innerDriverTypeName}::UseFile - [{location}] [{key}]";
            using (PerformanceMonitorItem.Time(description))
            {
                this.innerDriver.UseFile(location, key, useFileHandler);
            }
        }

        /// <summary>
        /// Delete the file from the file storage.
        /// </summary>
        /// <param name="location">The location of the file storage repository.</param>
        /// <param name="key">The file identifier used to delete.</param>
        public void DeleteFile(FileStorageIdentifier location, FileIdentifier key)
        {
            string description = $"{innerDriverTypeName}::DeleteFile - [{location}] [{key}]";

            using (PerformanceMonitorItem.Time(description))
            {
                this.innerDriver.DeleteFile(location, key);
            }
        }

        /// <summary>
        /// Check whether the given file exist in the file storage.
        /// </summary>
        /// <param name="location">The location of the file storage repository.</param>
        /// <param name="key">The file identifier.</param>
        /// <returns>True if the file exist false otherwise.</returns>
        public bool FileExists(FileStorageIdentifier location, FileIdentifier key)
        {
            string description = $"{innerDriverTypeName}::FileExists - [{location}] [{key}]";

            using (PerformanceMonitorItem.Time(description))
            {
                return this.innerDriver.FileExists(location, key);
            }
        }
    }
}