﻿namespace LendersOffice.Drivers.DataEncryption
{
    using LqbGrammar.Drivers;

    /// <summary>
    /// Factory for creation of a DataEncryption driver.
    /// </summary>
    public sealed class DataEncryptionDriverFactory : IDataEncryptionDriverFactory
    {
        /// <summary>
        /// Create an implementation of the IDataEncryption interface.
        /// </summary>
        /// <returns>An IDataEncryptionDriver instance.</returns>
        public IDataEncryptionDriver Create()
        {
            return new DataEncryptionDriver();
        }
    }
}
