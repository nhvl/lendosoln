﻿namespace LendersOffice.Drivers.DataEncryption
{
    using System;
    using System.Security.Cryptography;
    using Base64Encoding;
    using LqbGrammar;
    using LqbGrammar.Adapters;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Data encryption with arbitrary key.
    /// </summary>
    internal sealed class DataEncryptionDriver : IDataEncryptionDriver
    {
        /// <summary>
        /// Adapter to do the work.
        /// </summary>
        private IDataEncryptionAdapter adapter;

        /// <summary>
        /// Initializes a new instance of the <see cref="DataEncryptionDriver"/> class.
        /// </summary>
        public DataEncryptionDriver()
        {
            var factory = GenericLocator<IDataEncryptionAdapterFactory>.Factory;
            this.adapter = factory.Create();
        }

        /// <summary>
        /// Decode and decrypt the base64 encodeded data.
        /// </summary>
        /// <param name="encodedEncryptedData">Base64 encoded encrypted data.</param>
        /// <param name="encryptionKey">The key to use for decryption.</param>
        /// <returns>The decoded and decrypted value.</returns>
        public byte[] DecodeAndDecrypt(Base64EncodedData encodedEncryptedData, EncryptionKey encryptionKey)
        {
            try
            {
                var decodedData = Base64EncodingDriverHelper.Decode(encodedEncryptedData);
                return this.adapter.Decrypt(decodedData, encryptionKey);
            }
            catch (Exception ex) when (!(ex is LqbException))
            {
                throw new DeveloperException(ErrorMessage.SystemError, ex);
            }
}

        /// <summary>
        /// Decrypt the provided encrypted data.
        /// </summary>
        /// <param name="encryptedData">The encrypted data.</param>
        /// <param name="encryptionKey">The key to use in decryption.</param>
        /// <returns>The decrypted data.</returns>
        public byte[] Decrypt(byte[] encryptedData, EncryptionKey encryptionKey)
        {
            try
            {
                return this.adapter.Decrypt(encryptedData, encryptionKey);
            }
            catch (Exception ex) when (!(ex is LqbException))
            {
                throw new DeveloperException(ErrorMessage.SystemError, ex);
            }
        }

        /// <summary>
        /// Encrypt the data with the key.
        /// </summary>
        /// <param name="unencryptedData">The data to be encrypted.</param>
        /// <param name="encryptionKey">The key to use in the encryption.</param>
        /// <returns>The encrypted data.</returns>
        public byte[] Encrypt(byte[] unencryptedData, EncryptionKey encryptionKey)
        {
            return this.adapter.Encrypt(unencryptedData, encryptionKey);
        }

        /// <summary>
        /// Encrypt and encode in base64.
        /// </summary>
        /// <param name="unencryptedData">The data to encrypt and encode.</param>
        /// <param name="encryptionKey">The key to use for encryption.</param>
        /// <returns>The encoded encrypted value.</returns>
        public Base64EncodedData EncryptAndEncode(byte[] unencryptedData, EncryptionKey encryptionKey)
        {
            var encryptedData = this.adapter.Encrypt(unencryptedData, encryptionKey);
            return Base64EncodingDriverHelper.Encode(encryptedData);
        }

        /// <summary>
        /// Try to decrypt the provided encrypted data.
        /// </summary>
        /// <param name="encryptedData">The encrypted data.</param>
        /// <param name="encryptionKey">The key data to use in decryption.</param>
        /// <param name="decryptedValue">The resulting decoded and decrypted value.</param>
        /// <returns>True if decoding and decryption was successful.  Otherwise false.</returns>
        public bool TryDecrypt(byte[] encryptedData, EncryptionKey encryptionKey, out byte[] decryptedValue)
        {
            try
            {
                decryptedValue = this.adapter.Decrypt(encryptedData, encryptionKey);
                return true;
            }
            catch (LqbException ex)
            {
                var baseException = ex.GetBaseException();
                
                if (baseException is CryptographicException
                    || baseException is FormatException)
                {
                    // Decryption was unsuccessful.  This "try" failed.
                    decryptedValue = null;
                    return false;
                }

                throw;
            }
            catch (Exception ex)
            {
                // Adapters are not supposed to allow non LQBExceptions out,
                // however let us be sure here.
                throw new DeveloperException(ErrorMessage.SystemError, ex);
            }
        }
    }
}
