﻿namespace LendersOffice.Drivers.DataEncryption
{
    using LqbGrammar;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;

    /// <summary>
    /// Helper class for use of DataEncryptionAdapter.
    /// </summary>
    public static class DataEncryptionDriverHelper
    {
        /// <summary>
        /// Try to decrypt the provided encrypted data.
        /// </summary>
        /// <param name="encryptedData">The encrypted data.</param>
        /// <param name="encryptionKey">The key data to use in decryption.</param>
        /// <param name="decodedDecryptedValue">The resulting decoded and decrypted value.</param>
        /// <returns>True if decoding and decryption was successful.  Otherwise false.</returns>
        public static bool TryDecrypt(byte[] encryptedData, EncryptionKey encryptionKey, out byte[] decodedDecryptedValue)
        {
            var driver = GetDriver();
            return driver.TryDecrypt(encryptedData, encryptionKey, out decodedDecryptedValue);
        }

        /// <summary>
        /// Encrypt the data with the key.
        /// </summary>
        /// <param name="unencryptedData">The data to be encrypted.</param>
        /// <param name="encryptionKey">The key to use in the encryption.</param>
        /// <returns>The encrypted data.</returns>
        public static byte[] Encrypt(byte[] unencryptedData, EncryptionKey encryptionKey)
        {
            var driver = GetDriver();
            return driver.Encrypt(unencryptedData, encryptionKey);
        }

        /// <summary>
        /// Decrypt the provided encrypted data.
        /// </summary>
        /// <param name="encryptedData">The encrypted data.</param>
        /// <param name="encryptionKey">The key to use in decryption.</param>
        /// <returns>The decrypted data.</returns>
        public static byte[] Decrypt(byte[] encryptedData, EncryptionKey encryptionKey)
        {
            var driver = GetDriver();
            return driver.Decrypt(encryptedData, encryptionKey);
        }

        /// <summary>
        /// Decode and decrypt the base64 encodeded data.
        /// </summary>
        /// <param name="encodedEncryptedData">Base64 encoded encrypted data.</param>
        /// <param name="encryptionKey">The key to use for decryption.</param>
        /// <returns>The decoded and decrypted value.</returns>
        public static byte[] DecodeAndDecrypt(Base64EncodedData encodedEncryptedData, EncryptionKey encryptionKey)
        {
            var driver = GetDriver();
            return driver.DecodeAndDecrypt(encodedEncryptedData, encryptionKey);
        }

        /// <summary>
        /// Encrypt and encode in base64.
        /// </summary>
        /// <param name="unencryptedData">The data to encrypt and encode.</param>
        /// <param name="encryptionKey">The key to use for encryption.</param>
        /// <returns>The encoded encrypted value.</returns>
        public static Base64EncodedData EncryptAndEncode(byte[] unencryptedData, EncryptionKey encryptionKey)
        {
            var driver = GetDriver();
            return driver.EncryptAndEncode(unencryptedData, encryptionKey);
        }

        /// <summary>
        /// Retrieve the driver from the service locator.
        /// </summary>
        /// <returns>The data encryption driver.</returns>
        private static IDataEncryptionDriver GetDriver()
        {
            var factory = GenericLocator<IDataEncryptionDriverFactory>.Factory;
            return factory.Create();
        }
    }
}
