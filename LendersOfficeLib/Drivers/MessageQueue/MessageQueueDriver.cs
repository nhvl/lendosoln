﻿namespace LendersOffice.Drivers.MessageQueue
{
    using System;
    using LqbGrammar.Adapters;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;

    /// <summary>
    /// Driver for interacting with a message queue.
    /// </summary>
    internal sealed class MessageQueueDriver : IMessageQueueDriver
    {
        /// <summary>
        /// Adapter that implements the interaction with a message queue.
        /// </summary>
        private IMessageQueueAdapter adapter;

        /// <summary>
        /// Initializes a new instance of the <see cref="MessageQueueDriver"/> class.
        /// </summary>
        /// <param name="adapter">The adapter that implements the interaction with a message queue.</param>
        public MessageQueueDriver(IMessageQueueAdapter adapter)
        {
            this.adapter = adapter;
        }

        /// <summary>
        /// Initialize a message queue for reading JSON messages.
        /// </summary>
        /// <param name="queue">The path to the message queue.</param>
        /// <returns>The message queue.</returns>
        public LqbMessageQueue PrepareSourceQueueForJSON(MessageQueuePath queue)
        {
            return this.adapter.PrepareSourceQueueForJSON(queue);
        }

        /// <summary>
        /// Initialize a message queue for reading XML messages.
        /// </summary>
        /// <param name="queue">The path to the message queue.</param>
        /// <returns>The message queue.</returns>
        public LqbMessageQueue PrepareSourceQueueForXML(MessageQueuePath queue)
        {
            return this.adapter.PrepareSourceQueueForXML(queue);
        }

        /// <summary>
        /// Read a JSON message from the message queue and deserialize to the specified type, waiting for the timeout.
        /// </summary>
        /// <typeparam name="T">The type that expected for the JSON data.</typeparam>
        /// <param name="queue">The message queue.</param>
        /// <param name="timeout">The time to wait for a message prior to returning.  If null is passed in then the wait will be very short (1/10 millisecond).</param>
        /// <param name="arrivalTime">The time the message arrived in the queue.</param>
        /// <returns>A deserialized instance of the expected type, or null if there are no messages within the timeout waiting period.</returns>
        public T ReceiveJSON<T>(LqbMessageQueue queue, TimeoutInSeconds? timeout, out DateTime arrivalTime) where T : class, new()
        {
            return this.adapter.ReceiveJSON<T>(queue, timeout, out arrivalTime);
        }

        /// <summary>
        /// Read an XML message from the message queue, waiting for the timeout.
        /// </summary>
        /// <param name="queue">The message queue.</param>
        /// <param name="timeout">The time to wait for a message prior to returning.  If null is passed in then the wait will be very short (1/10 millisecond).</param>
        /// <param name="arrivalTime">The time the message arrived in the queue.</param>
        /// <returns>The received message, or null if there are no messages within the timeout waiting period.</returns>
        public LqbXmlElement? ReceiveXML(LqbMessageQueue queue, TimeoutInSeconds? timeout, out DateTime arrivalTime)
        {
            return this.adapter.ReceiveXML(queue, timeout, out arrivalTime);
        }

        /// <summary>
        /// Send a JSON representation of an object to a message queue.
        /// </summary>
        /// <param name="queue">The path to the message queue.</param>
        /// <param name="label">An optional label to attach to the message.</param>
        /// <param name="obj">The object to send.</param>
        public void SendJSON(MessageQueuePath queue, MessageLabel? label, object obj)
        {
            this.adapter.SendJSON(queue, label, obj);
        }

        /// <summary>
        /// Send XML to a message queue.
        /// </summary>
        /// <param name="queue">The path to the message queue.</param>
        /// <param name="label">An optional label to attach to the message.</param>
        /// <param name="xml">The XML to be sent.</param>
        public void SendXML(MessageQueuePath queue, MessageLabel? label, LqbXmlElement xml)
        {
            this.adapter.SendXML(queue, label, xml);
        }
    }
}
