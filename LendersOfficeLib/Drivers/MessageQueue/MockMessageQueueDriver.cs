﻿namespace LendersOffice.Drivers.MessageQueue
{
    using System;
    using System.Xml.Linq;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;

    /// <summary>
    /// Mock class for suppressing message queue messages.
    /// </summary>
    internal sealed class MockMessageQueueDriver : IMessageQueueDriver
    {
        /// <summary>
        /// An object used for the JSON methods.
        /// </summary>
        private object jsonObject;

        /// <summary>
        /// An object used for the XML methods.
        /// </summary>
        private XElement xmlObject;

        /// <summary>
        /// Initializes a new instance of the <see cref="MockMessageQueueDriver"/> class.
        /// </summary>
        /// <param name="xmlObject">An object used for the XML methods.</param>
        /// <param name="jsonObject">An object used for the JSON methods.</param>
        /// <param name="factory">The factory that is creating this instance.</param>
        public MockMessageQueueDriver(XElement xmlObject, object jsonObject, MockMessageQueueDriverFactory factory)
        {
            this.xmlObject = xmlObject;
            this.jsonObject = jsonObject;
            this.Factory = factory;
        }

        /// <summary>
        /// Gets or sets the factory that created this driver.
        /// </summary>
        /// <value>The factory that created this driver.</value>
        private MockMessageQueueDriverFactory Factory { get; set; }

        /// <summary>
        /// Prepare a dummy message queue object.
        /// </summary>
        /// <param name="queue">The parameter is not used.</param>
        /// <returns>A dummy message queue object.</returns>
        public LqbMessageQueue PrepareSourceQueueForJSON(MessageQueuePath queue)
        {
            return new LqbMessageQueue(null);
        }

        /// <summary>
        /// Prepare a dummy message queue object.
        /// </summary>
        /// <param name="queue">The parameter is not used.</param>
        /// <returns>A dummy message queue object.</returns>
        public LqbMessageQueue PrepareSourceQueueForXML(MessageQueuePath queue)
        {
            return new LqbMessageQueue(null);
        }

        /// <summary>
        /// Return the JSON member object.
        /// </summary>
        /// <typeparam name="T">The type of the JSON object.</typeparam>
        /// <param name="queue">The parameter is not used.</param>
        /// <param name="timeout">The timeout, it only checks for null.</param>
        /// <param name="arrivalTime">A time that makes sense in the context of the call, given the passed in timeout value.</param>
        /// <returns>The JSON member object.</returns>
        public T ReceiveJSON<T>(LqbMessageQueue queue, TimeoutInSeconds? timeout, out DateTime arrivalTime) where T : class, new()
        {
            arrivalTime = (timeout == null) ? DateTime.Now : DateTime.Now.AddMilliseconds(-100);
            return (this.jsonObject == null) ? null : (T)this.jsonObject;
        }

        /// <summary>
        /// Return the XML member object.
        /// </summary>
        /// <param name="queue">The parameter is not used.</param>
        /// <param name="timeout">The timeout, it only checks for null.</param>
        /// <param name="arrivalTime">A time that makes sense in the context of the call, given the passed in timeout value.</param>
        /// <returns>The XML member object.</returns>
        public LqbXmlElement? ReceiveXML(LqbMessageQueue queue, TimeoutInSeconds? timeout, out DateTime arrivalTime)
        {
            arrivalTime = DateTime.Now.AddMilliseconds(-100);
            if (this.xmlObject == null)
            {
                return null;
            }
            else
            {
                return LqbXmlElement.Create(this.xmlObject);
            }
        }

        /// <summary>
        /// Don't do anything, just swallow the message.
        /// </summary>
        /// <param name="queue">The parameter is not used.</param>
        /// <param name="label">The parameter is not used.</param>
        /// <param name="obj">The parameter is not used.</param>
        public void SendJSON(MessageQueuePath queue, MessageLabel? label, object obj)
        {
            // do nothing
        }

        /// <summary>
        /// Don't do anything, just swallow the message.
        /// </summary>
        /// <param name="queue">The parameter is not used.</param>
        /// <param name="label">The parameter is not used.</param>
        /// <param name="xml">The parameter is not used.</param>
        public void SendXML(MessageQueuePath queue, MessageLabel? label, LqbXmlElement xml)
        {
            this.Factory.MessageBody = xml.Contained.ToString();
        }
    }
}
