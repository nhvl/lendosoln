﻿namespace LendersOffice.Drivers.MessageQueue
{
    using System;
    using LqbGrammar;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Utility class that uses a message queue driver to 
    /// interact with the message queue.
    /// </summary>
    public static class MessageQueueHelper
    {
        /// <summary>
        /// Send the xml document to the message queue.
        /// </summary>
        /// <param name="queue">The name of the message queue.</param>
        /// <param name="label">Optional label to be added to the message.</param>
        /// <param name="root">The xml document to send to the message queue.</param>
        public static void SendXML(MessageQueuePath queue, MessageLabel? label, LqbXmlElement root)
        {
            var driver = GetDriver();
            driver.SendXML(queue, label, root);
        }

        /// <summary>
        /// Serialize an object to JSON format and send it to the message queue.
        /// </summary>
        /// <param name="queue">The name of the message queue.</param>
        /// <param name="label">Optional label to be added to the message.</param>
        /// <param name="obj">The object to be sent to the message queue.</param>
        public static void SendJSON(MessageQueuePath queue, MessageLabel? label, object obj)
        {
            var driver = GetDriver();
            driver.SendJSON(queue, label, obj);
        }

        /// <summary>
        /// Create and initialize a message queue object that can be used for retrieving XML documents.
        /// </summary>
        /// <param name="queue">The path to the message queue.</param>
        /// <param name="setFilterDefaults">Counter-intuitively, calling SetDefaults on the message filter is different than using the defaults when just using the MessageQueue constructor.</param>
        /// <returns>A message queue object.</returns>
        public static LqbMessageQueue PrepareForXML(MessageQueuePath queue, bool setFilterDefaults)
        {
            var driver = GetDriver();
            return driver.PrepareSourceQueueForXML(queue);
        }

        /// <summary>
        /// Create and initialize a message queue object that can be used for retrieving objects serialized as JSON.
        /// </summary>
        /// <param name="queue">The path to the message queue.</param>
        /// <returns>A message queue object.</returns>
        public static LqbMessageQueue PrepareForJSON(MessageQueuePath queue)
        {
            var driver = GetDriver();
            return driver.PrepareSourceQueueForJSON(queue);
        }

        /// <summary>
        /// Retrieve an XDocument from the message queue.
        /// </summary>
        /// <param name="queue">The message queue object.</param>
        /// <param name="timeoutInSeconds">The time to wait for a message.  If null then 10 milliseconds is the wait time.</param>
        /// <param name="arrivalTime">The time that the message was delivered to the queue.</param>
        /// <returns>An XDocument from the queue.</returns>
        public static LqbXmlElement? ReceiveXML(LqbMessageQueue queue, TimeoutInSeconds? timeoutInSeconds, out DateTime arrivalTime)
        {
            var driver = GetDriver();
            return driver.ReceiveXML(queue, timeoutInSeconds, out arrivalTime);
        }

        /// <summary>
        /// Retrieve an object from the message queue that has been serialized as JSON.
        /// </summary>
        /// <typeparam name="T">The expected type of the object.</typeparam>
        /// <param name="queue">The message queue object.</param>
        /// <param name="timeoutInSeconds">The time to wait for a message.  If null then 10 milliseconds is the wait time.</param>
        /// <param name="arrivalTime">The time that the message was delivered to the queue.</param>
        /// <returns>An instance of the expected type.</returns>
        public static T ReceiveJSON<T>(LqbMessageQueue queue, TimeoutInSeconds? timeoutInSeconds, out DateTime arrivalTime) where T : class, new()
        {
            var driver = GetDriver();
            return driver.ReceiveJSON<T>(queue, timeoutInSeconds, out arrivalTime);
        }

        /// <summary>
        /// Validate the correctness of the input data and return safe equivalents.
        /// </summary>
        /// <param name="queue">The message queue path.</param>
        /// <param name="label">The label for a message.</param>
        /// <returns>Validated equivalents of the queue path and message label.</returns>
        public static Tuple<MessageQueuePath, MessageLabel?> ValidateQueueInfo(string queue, string label)
        {
            MessageQueuePath? goodQueue = MessageQueuePath.Create(queue);
            if (goodQueue == null)
            {
                throw new DeveloperException(ErrorMessage.SystemError);
            }

            MessageLabel? goodLabel = MessageLabel.Create(label);

            return new Tuple<MessageQueuePath, MessageLabel?>(goodQueue.Value, goodLabel);
        }

        /// <summary>
        /// Retrieve a driver for interacting with the message queue system.
        /// </summary>
        /// <returns>A driver for interacting with the message queue system.</returns>
        private static IMessageQueueDriver GetDriver()
        {
            var factory = GenericLocator<IMessageQueueDriverFactory>.Factory;
            return factory.Create();
        }
    }
}
