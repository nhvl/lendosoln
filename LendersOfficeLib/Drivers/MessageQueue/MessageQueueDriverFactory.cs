﻿namespace LendersOffice.Drivers.MessageQueue
{
    using System;
    using LqbGrammar;
    using LqbGrammar.Adapters;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;

    /// <summary>
    /// Factory for creating a message queue driver.
    /// </summary>
    public sealed class MessageQueueDriverFactory : IMessageQueueDriverFactory
    {
        /// <summary>
        /// Create an implementation of the IMessageQueueDriver interface.
        /// </summary>
        /// <returns>An implementation of the IMessageQueueDriver interface.</returns>
        public IMessageQueueDriver Create()
        {
            var factory = GenericLocator<IMessageQueueAdapterFactory>.Factory;
            var adapter = factory.Create();

            return new MessageQueueDriver(adapter);
        }
    }
}
