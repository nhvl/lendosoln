﻿namespace LendersOffice.Drivers.MessageQueue
{
    using System;
    using System.Xml.Linq;
    using LqbGrammar.Drivers;

    /// <summary>
    /// Factory for creating instances of a mock message queue driver.
    /// </summary>
    public sealed class MockMessageQueueDriverFactory : IMessageQueueDriverFactory
    {
        /// <summary>
        /// An object used for the JSON methods.
        /// </summary>
        private object jsonObject;

        /// <summary>
        /// An object used for the XML methods.
        /// </summary>
        private XElement xmlObject;

        /// <summary>
        /// Initializes a new instance of the <see cref="MockMessageQueueDriverFactory"/> class.
        /// </summary>
        /// <param name="xmlObject">An object used for the XML methods.</param>
        /// <param name="jsonObject">An object used for the JSON methods.</param>
        public MockMessageQueueDriverFactory(XElement xmlObject, object jsonObject)
        {
            this.xmlObject = xmlObject;
            this.jsonObject = jsonObject;
        }

        /// <summary>
        /// Gets the message body sent to the queue.
        /// </summary>
        /// <value>The message body sent to the queue.</value>
        public string MessageBody { get; internal set; }

        /// <summary>
        /// Create a mock message queue driver.
        /// </summary>
        /// <returns>A mock message queue driver.</returns>
        public IMessageQueueDriver Create()
        {
            return new MockMessageQueueDriver(this.xmlObject, this.jsonObject, this);
        }
    }
}
