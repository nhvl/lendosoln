﻿namespace LendersOffice.Drivers.Environment
{
    using LqbGrammar;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;

    /// <summary>
    /// Utility class for retrieving environment data.
    /// </summary>
    public sealed class EnvironmentHelper
    {
        /// <summary>
        /// Gets the current working directory of the application in which the calling code is running.
        /// </summary>
        /// <value>The current working directory of the application in which the calling code is running.</value>
        public static string CurrentDirectory
        {
            get
            {
                return GetDriver().CurrentDirectory;
            }
        }

        /// <summary>
        /// Gets the name of the machine on which the calling code is running.
        /// </summary>
        /// <value>The name of the machine on which the calling code is running.</value>
        public static MachineName MachineName
        {
            get
            {
                return GetDriver().MachineName;
            }
        }

        /// <summary>
        /// Gets the preferred newline string for the current environment.
        /// </summary>
        /// <value>The preferred newline string for the current environment.</value>
        public static string NewLine
        {
            get
            {
                return GetDriver().NewLine;
            }
        }

        /// <summary>
        /// Gets the number of processors hosted by the machine on which the calling code is running.
        /// </summary>
        /// <value>The number of processors hosted by the machine on which the calling code is running.</value>
        public static int ProcessorCount
        {
            get
            {
                return GetDriver().ProcessorCount;
            }
        }

        /// <summary>
        /// Gets the current stack trace information.
        /// </summary>
        /// <value>The current stack trace information.</value>
        public static string StackTrace
        {
            get
            {
                return GetDriver().StackTrace;
            }
        }

        /// <summary>
        /// Terminates the current process and returns the exit code to the operating system.
        /// </summary>
        /// <param name="code">The exit code returned to the operating system.</param>
        public static void Exit(int code)
        {
            GetDriver().Exit(code);
        }

        /// <summary>
        /// Retrieve a driver for handling client calls.
        /// </summary>
        /// <returns>A driver for handling client calls.</returns>
        private static IEnvironmentDriver GetDriver()
        {
            var factory = GenericLocator<IEnvironmentDriverFactory>.Factory;
            return factory.Create();
        }
    }
}
