﻿namespace LendersOffice.Drivers.Environment
{
    using LqbGrammar.DataTypes;

    /// <summary>
    /// When overriding environment data during testing, use this class.
    /// </summary>
    public sealed class MockEnvironmentData
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MockEnvironmentData"/> class.
        /// </summary>
        public MockEnvironmentData()
        {
            this.CurrentDirectory = null;
            this.MachineName = null;
            this.ProcessorCount = null;
        }

        /// <summary>
        /// Gets or sets the current working directory of the application in which the calling code is running.
        /// </summary>
        /// <value>The current working directory of the application in which the calling code is running.</value>
        public string CurrentDirectory { get; set; }

        /// <summary>
        /// Gets or sets the name of the machine on which the calling code is running.
        /// </summary>
        /// <value>The name of the machine on which the calling code is running.</value>
        public MachineName? MachineName { get; set; }

        /// <summary>
        /// Gets or sets the number of processors hosted by the machine on which the calling code is running.
        /// </summary>
        /// <value>The number of processors hosted by the machine on which the calling code is running.</value>
        public int? ProcessorCount { get; set; }
    }
}
