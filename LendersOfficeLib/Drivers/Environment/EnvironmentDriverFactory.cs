﻿namespace LendersOffice.Drivers.Environment
{
    using System;
    using LqbGrammar;
    using LqbGrammar.Adapters;
    using LqbGrammar.Drivers;

    /// <summary>
    /// Factory for creating instances of the EnvironmentDriver class.
    /// </summary>
    public sealed class EnvironmentDriverFactory : IEnvironmentDriverFactory
    {
        /// <summary>
        /// Lock used when creating the singleton driver.
        /// </summary>
        private static readonly object DriverLock;

        /// <summary>
        /// Keep a singleton instance as ultimately the System.Environment is a static class.
        /// </summary>
        private static IEnvironmentDriver driver;

        /// <summary>
        /// Initializes static members of the <see cref="EnvironmentDriverFactory" /> class.
        /// </summary>
        static EnvironmentDriverFactory()
        {
            DriverLock = new object();
        }

        /// <summary>
        /// Create an implementation of the IEnvironmentDriver interface.
        /// </summary>
        /// <returns>An implementation of the IEnvironmentDriver interface.</returns>
        public IEnvironmentDriver Create()
        {
            if (driver == null)
            {
                lock (DriverLock)
                {
                    if (driver == null)
                    {
                        var factory = GenericLocator<IEnvironmentAdapterFactory>.Factory;
                        var adapter = factory.Create();
                        driver = new EnvironmentDriver(adapter);
                    }
                }
            }

            return driver;
        }
    }
}
