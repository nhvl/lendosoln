﻿namespace LendersOffice.Drivers.Environment
{
    using LqbGrammar.Adapters;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;

    /// <summary>
    /// Driver for interacting with the System.Environment class.
    /// </summary>
    internal sealed class EnvironmentDriver : IEnvironmentDriver
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EnvironmentDriver"/> class.
        /// </summary>
        /// <param name="adapter">The adapter used to interact with the System.Environment class.</param>
        public EnvironmentDriver(IEnvironmentAdapter adapter)
        {
            this.Adapter = adapter;
        }

        /// <summary>
        /// Gets the current working directory of the application in which the calling code is running.
        /// </summary>
        /// <value>The current working directory of the application in which the calling code is running.</value>
        public string CurrentDirectory
        {
            get
            {
                return this.Adapter.CurrentDirectory;
            }
        }

        /// <summary>
        /// Gets the name of the machine on which the calling code is running.
        /// </summary>
        /// <value>The name of the machine on which the calling code is running.</value>
        public MachineName MachineName
        {
            get
            {
                return this.Adapter.MachineName;
            }
        }

        /// <summary>
        /// Gets the preferred newline string for the current environment.
        /// </summary>
        /// <value>The preferred newline string for the current environment.</value>
        public string NewLine
        {
            get
            {
                return this.Adapter.NewLine;
            }
        }

        /// <summary>
        /// Gets the number of processors hosted by the machine on which the calling code is running.
        /// </summary>
        /// <value>The number of processors hosted by the machine on which the calling code is running.</value>
        public int ProcessorCount
        {
            get
            {
                return this.Adapter.ProcessorCount;
            }
        }

        /// <summary>
        /// Gets the current stack trace information.
        /// </summary>
        /// <value>The current stack trace information.</value>
        public string StackTrace
        {
            get
            {
                return this.Adapter.StackTrace;
            }
        }

        /// <summary>
        /// Gets or sets the adapter used to interact with the System.Environment class.
        /// </summary>
        /// <value>The adapter used to interact with the System.Environment class.</value>
        private IEnvironmentAdapter Adapter { get; set; }

        /// <summary>
        /// Terminates the current process and returns the exit code to the operating system.
        /// </summary>
        /// <param name="code">The exit code returned to the operating system.</param>
        public void Exit(int code)
        {
            this.Adapter.Exit(code);
        }
    }
}
