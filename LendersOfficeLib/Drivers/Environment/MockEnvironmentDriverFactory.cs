﻿namespace LendersOffice.Drivers.Environment
{
    using System;
    using LqbGrammar.Drivers;

    /// <summary>
    /// Factory for creating instances of the MockEnvironmentDriver class.
    /// </summary>
    public sealed class MockEnvironmentDriverFactory : IEnvironmentDriverFactory
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MockEnvironmentDriverFactory"/> class.
        /// </summary>
        /// <param name="data">Data that can override the values returned by the System.Environment class.</param>
        public MockEnvironmentDriverFactory(MockEnvironmentData data)
        {
            this.Data = data;
        }

        /// <summary>
        /// Gets or sets the override data.
        /// </summary>
        /// <value>The override data.</value>
        private MockEnvironmentData Data { get; set; }

        /// <summary>
        /// Create an implementation of the IEnvironmentDriver interface.
        /// </summary>
        /// <returns>An implementation of the IEnvironmentDriver interface.</returns>
        public IEnvironmentDriver Create()
        {
            return new MockEnvironmentDriver(this.Data);
        }
    }
}
