using System;
using System.Xml;

namespace LendersOffice.CreditReport
{
	public interface ICreditReportResponse
	{
        string ReportID { get; }
        string Status { get; }
        bool HasError { get; }
        bool IsReady { get; }
        string ErrorMessage { get; }
        string RawXmlResponse { get; }
        ////ICreditReport CreditReport { get; }
        string BrandedProductName { get; }
	}
}
