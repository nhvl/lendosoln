using System;
using System.Data.Common;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Xml;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Constants;
using LendersOffice.CreditReport.FannieMae;
using LendersOffice.CreditReport.Mcl;
using LendersOffice.CreditReport.Mismo;
using LendersOffice.HttpModule;
using LendersOffice.Security;
using LendersOffice.Audit;

namespace LendersOffice.CreditReport
{

	/// <summary>
	/// Use WebRequest/WebResponse to order credit report request.
	/// </summary>
	public class CreditReportServer
	{
        private static string MaskCreditCommunicationForLogging(string creditCommunication)
        {
            creditCommunication = Regex.Replace(creditCommunication, " password=\"[^ ]+\"", " password=\"******\"");
            creditCommunication = Regex.Replace(creditCommunication, " LoginAccountPassword=\"[^ ]+\"", " LoginAccountPassword=\"******\"");
            creditCommunication = Regex.Replace(creditCommunication, " InternalAccountIdentifier=\"[^ ]+\"", " InternalAccountIdentifier=\"******\"");
            creditCommunication = Regex.Replace(creditCommunication, " _Password=\"[^ ]+\"", " _Password=\"******\"");
            creditCommunication = Regex.Replace(creditCommunication, " _AccountIdentifier=\"[^ ]+\"", " _AccountIdentifier=\"******\"");
            creditCommunication = Regex.Replace(creditCommunication, " _AccountExpirationDate=\"[^ ]+\"", " _AccountExpirationDate=\"******\"");
            creditCommunication = Regex.Replace(creditCommunication, " _SecondaryAccountIdentifier=\"[^ ]+\"", " _SecondaryAccountIdentifier=\"******\"");
            creditCommunication = Regex.Replace(creditCommunication, "cvv=\"[^ ]+\"", "cvv=\"***\"");
            creditCommunication = Regex.Replace(creditCommunication, "card_number=\"[^ ]+\"", "card_number=\"***\"");
            return creditCommunication;
        }

        private static string GetRawCreditReportXml(Guid loanId, ICreditReportRequest request, CreditReportProtocol protocol, AbstractUserPrincipal principal)
        {
            if (loanId != Guid.Empty)
            {
                AuthServiceHelper.CheckOrderCreditAuthorization(loanId, protocol, principal);
            }

            // 3/21/2008 dd - Exclude the overall time we take to order credit report from 3rd party
            PerformanceMonitorItem monitorItem = PerformanceMonitorItem.Current;
            if (null != monitorItem)
            {
                monitorItem.StopTiming();
            }

            string identifierString = $"LoanId: {loanId} UserId: {principal?.UserId} LoginNm: {principal?.LoginNm}{Environment.NewLine}";
            int MAX_RETRIES = 5;
            int currentRetries = 0;
            bool isRunning = true;
            string sResponse = string.Empty;
            while (isRunning)
            {
                isRunning = false;
                try
                {
                    // 3/7/2005 dd - Combine 3 separate PB log entries to single PB log entry.
                    Tools.LogInfo("CreditReportServer", $"{identifierString}Url =[{request.Url}]");
                    byte[] bytes = request.GeneratePostContent();

                    if (Tools.IsLogLargeRequest(bytes.Length))
                    {
                        Tools.LogInfo("CreditReportServerRequest", identifierString + MaskCreditCommunicationForLogging(System.Text.Encoding.UTF8.GetString(bytes)));
                    }
                    else
                    {
                        Tools.LogInfo("CreditReportServerRequest", identifierString + bytes.Length + " bytes exceeding threshold for logging.");
                    }


                    //IFetchResponse interface added for Credit interfaces other than generic Http request/response. The request class implements function to fetch its own response. Added for FundingSuite which has asmx service. -Ajay
                    if (request is IFetchResponse)
                    {
                        sResponse = ((IFetchResponse)request).FetchResponse();
                    }
                    else
                    {
                        HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(request.Url);

                        webRequest.KeepAlive = false; // 11/4/2004 dd - DO NOT REMOVE THIS LINE. Else you will get "Cannot access a disposed object named "System.Net.TlsStream"" error randomly.
                        webRequest.ContentType = "application/x-www-form-urlencoded";

                        request.SetupAuthentication(webRequest);
                        webRequest.Method = "POST";

                        webRequest.ContentLength = bytes.Length;

                        using (Stream stream = webRequest.GetRequestStream())
                        {
                            stream.Write(bytes, 0, bytes.Length);
                        }

                        WebResponse webResponse = webRequest.GetResponse();

                        StringBuilder sb = new StringBuilder();
                        using (Stream stream = webResponse.GetResponseStream())
                        {
                            byte[] buffer = new byte[60000];
                            int size = stream.Read(buffer, 0, buffer.Length);
                            while (size > 0)
                            {
                                sb.Append(System.Text.Encoding.UTF8.GetString(buffer, 0, size));
                                size = stream.Read(buffer, 0, buffer.Length);
                            }
                        }
                        webResponse.Close();

                        sResponse = sb.ToString();
                    }

                    if (sResponse.Length == 0)
                    {
                        Tools.LogInfo("CreditReportServerResponse", identifierString + "No response from server. Will Retries");
                        if (currentRetries++ < MAX_RETRIES)
                        {
                            isRunning = true;
                            continue;
                        }
                    }
                    else
                    {
                        if (Tools.IsLogLargeRequest(sResponse.Length))
                        {
                            Tools.LogInfo("CreditReportServerResponse", identifierString + MaskCreditCommunicationForLogging(sResponse));
                        }
                        else
                        {
                            Tools.LogInfo("CreditReportServerResponse", identifierString + sResponse.Length + " bytes exceeding threshold for logging.");
                        }
                    }
                }
                catch (WebException exc)
                {
                    WebExceptionStatus webExceptionStatus = exc.Status;
                    HttpStatusCode? httpStatus = (exc.Response as HttpWebResponse)?.StatusCode;

                    var statusString = $"WebExceptionStatus: {webExceptionStatus} | HttpStatusCode: {httpStatus}";
                    if (currentRetries++ < MAX_RETRIES)
                    {
                        Tools.LogWarning($"{identifierString} {request.Url} is timeout. ErrorMessage = {exc.Message}. {statusString}. Retries #{currentRetries}");
                        LendersOffice.Sleeping.PerformanceLoggingSleeper.Instance.Sleep(10000); // Sleep for 10 seconds before retry
                        isRunning = true; // Retries if connection is timeout.
                    }
                    else
                    {
                        string devErrorMsg = (null == request ? ErrorMessages.ICreditReportRequestIsNull : request.Url) + Environment.NewLine + exc.ToString() + Environment.NewLine + statusString;
                        throw new OrderCreditException(devErrorMsg);
                    }

                }
                catch (Exception exc)
                {
                    string devErrorMsg = (null == request ? ErrorMessages.ICreditReportRequestIsNull : request.Url) + Environment.NewLine + exc.ToString();
                    throw new OrderCreditException(devErrorMsg);
                }
            }
            if (null != monitorItem)
            {
                monitorItem.ResumeTiming();
            }

            return sResponse;
        }

        public static ICreditReportResponse OrderCreditReport(CreditRequestData creditRequestData, AbstractUserPrincipal principal)
        {
            string temp;
            return OrderCreditReport(creditRequestData, principal, out temp);
        }

        /// <summary>
        /// Order credit report asynchronous. Need to keep check status of the response
        /// and continue to poll the server until report is complete.
        /// Return null if exception occurs.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public static ICreditReportResponse OrderCreditReport(Guid loanId, ICreditReportRequest request, CreditReportProtocol protocol, AbstractUserPrincipal principal, out string rawResponse) 
        {
            rawResponse = string.Empty;
            try
            {
                rawResponse = GetRawCreditReportXml(loanId, request, protocol, principal);
                return CreditReportUtilities.TransformRawResponseXml(request, rawResponse);
            }
            catch (Exception exc)
            {

                string devErrorMsg = (null == request ? ErrorMessages.ICreditReportRequestIsNull : request.Url) + Environment.NewLine + exc.ToString();

                if (request is LendersOffice.CreditReport.CBC.CBC_CreditReportRequest && exc is XmlException && rawResponse.Contains("<html>"))
                {
                    string error = @"Please contact Macs.Support@cbc-companies.com and ask them to do the following for your CBC account: 
1. Add IP address range 12.106.86.X to the list of accepted ranges  
2. Enable XML format";

                    throw new OrderCreditException(error, devErrorMsg);
                }
                else
                {
                    throw new OrderCreditException(devErrorMsg);
                }
            }
        }

        public static ICreditReportResponse OrderCreditReport(CreditRequestData creditRequestData, AbstractUserPrincipal principal, out string rawResponse)
        {
            ICreditReportRequest creditRequest = CreditReportUtilities.TransformCreditRequestData(creditRequestData, principal);
            return OrderCreditReport(creditRequestData.LoanId, creditRequest, creditRequestData.CreditProtocol, principal, out rawResponse);
        }

        public static void DeleteCreditReport(DbConnectionInfo connInfo, Guid applicationId)
        {
            // 6/17/2005 dd - We only mark the entry in Service_File to be invalid. We are no longer remove
            // the actual credit report from FileDB
            SqlParameter[] parameters = {
                                            new SqlParameter("@Owner", applicationId),
                                            new SqlParameter("@DeleteLqi", true)
                                        };
            StoredProcedureHelper.ExecuteNonQuery(connInfo, "InvalidateCreditReport", 0, parameters);

        }

        public static void DeleteCreditReport(Guid brokerId, Guid applicationID) 
        {
            DeleteCreditReport(brokerId, applicationID, Guid.Empty, CreditReportDeletedAuditItem.E_CreditReportDeletionT.Other, true);
        }

        public static void DeleteCreditReport(Guid brokerId, Guid applicationID, Guid loanId, CreditReportDeletedAuditItem.E_CreditReportDeletionT deletionType, bool deleteLqi) 
        {
            // 6/17/2005 dd - We only mark the entry in Service_File to be invalid. We are no longer remove
            // the actual credit report from FileDB
            SqlParameter[] parameters = {
                                            new SqlParameter("@Owner", applicationID),
                                            new SqlParameter("@DeleteLqi", deleteLqi)
                                        };
            StoredProcedureHelper.ExecuteNonQuery(brokerId, "InvalidateCreditReport", 0, parameters);

            if (loanId != Guid.Empty)
            {
                CPageData dataLoan = new CPageData(loanId, "DeleteCreditReport", new string[] { "aBNm", "aCNm" });
                dataLoan.InitLoad();
                CAppData app = dataLoan.GetAppData(applicationID);
                AbstractAuditItem audit = new CreditReportDeletedAuditItem(
                    PrincipalFactory.CurrentPrincipal,
                    app.aBNm,
                    app.aCNm,
                    DateTime.Now,
                    deletionType);
                AuditManager.RecordAudit(loanId, audit);
            }
        }
	}
}
