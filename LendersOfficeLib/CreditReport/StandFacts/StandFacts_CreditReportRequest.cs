using System;
using System.Xml;

using LendersOffice.CreditReport;

namespace LendersOffice.CreditReport.StandFacts
{
	public class StandFacts_CreditReportRequest : LendersOffice.CreditReport.Mismo2_3.Mismo2_3CreditReportRequest
	{
        private const string URL = "https://www.standfacts.com/infinity/webinterface.exe/mismo";
		public StandFacts_CreditReportRequest() : base("")
		{
		}

        protected override bool IncludeMailingAddress
        {
            get { return false; }
        }

        public override string Url 
        {
            get { return URL; }
        }

        public override ICreditReportResponse CreateResponse(XmlDocument doc) 
        {
            return new StandFacts_CreditReportResponse(doc);
        }
	}
}

