using System;
using System.Xml;

using LendersOffice.CreditReport;

namespace LendersOffice.CreditReport.StandFacts
{
	public class StandFacts_CreditReportResponse : LendersOffice.CreditReport.Mismo2_3.Mismo2_3CreditReportResponse
	{
		public StandFacts_CreditReportResponse(XmlDocument doc) : base(doc)
		{
		}
	}
}
