using System;

namespace LendersOffice.CreditReport
{
	
	//TODO: Rename this to be E_BkFlags
	[ Flags ]
	public enum E_BkType
	{
		Ch7 = 1,
		Ch11 = 2,
		Ch12 = 4,
		Ch13 = 8,
		UnknownType = 16,
		Any = Ch7 | Ch11 | Ch12 | Ch13 | UnknownType
	}

	// 7/5/2006 nw - OPM 5752 - redefined the enum values
	public enum E_BkChar
	{
		Any = 0,
		Discharged = 1,		// accounts for discharged and paid BK
		Dismissed = 2,		// accounts for dismissed BK
		Closed = 3,			// accounts for discharged, dismissed, and paid BK
		NondischargedAndNonpaid = 4,	// accounts for all BK not included in Closed
	}
    public enum E_DerogT
    {
        Repossession = 0,
        Foreclosure = 1,
        ChargeOff = 2,
        Collection = 3,
		TaxLien = 4,
		Judgment = 5,
        ANY = 6, // this would include the ones that not listed in this E_DerogT
    }

	/*
	public enum E_DerogFlags
	{
		Repossession = 1,
		Foreclosure = 2,
		ChargeOff = 4,
		Collection = 8,
		TaxLien = 16,
		ALL = 0xFFFF
	}
	*/


	// TODO: Rename this to be E_AdverseFlags
    /// <summary>
    /// This is different than MismoAdverseRating Type. AdverseType only contains negative rating can be found in payment pattern.
    /// </summary>
    [Flags]
    public enum E_AdverseType 
    {
		None = 0,
        Late30 = 1,
        Late60 = 2,
        Late90 = 4,
        Late120 = 8,
        Late150 = 16,
        Late180 = 32,
        Late120Plus = Late120 | Late150 | Late180,
        Late90Plus = Late90 | Late120 | Late150 | Late180,
        AllLate = Late30 | Late60 | Late90 | Late120 | Late150 | Late180,
        Bankruptcy = 64,
        RepossessionOrForeclosure = 128,
        ChargeOffOrCollection = 256,
        ALL = AllLate | Bankruptcy | RepossessionOrForeclosure | ChargeOffOrCollection,
    }

    public enum E_CreditPublicRecordDispositionType 
    {
        Adjudicated, // Mismo
        Appealed, // Mismo
        Canceled, // MCL, Mismo
        Completed, // MCL, Mismo.  MCL code is PLAN COMPLETE
        Converted, // Mismo
        Discharged, // MCL, Mismo
        Dismissed, // MCL, Mismo
        Distributed, // Mismo
        Filed, // MCL, Mismo
        Granted, // Mismo
        InvoluntarilyDischarged, // MCL, Mismo
        Nonadjudicated, // MCL, Mismo
        Other, // Mismo
        Paid, // MCL, Mismo
        PaidNotSatisfied, // MCL, Mismo
        Pending, // Mismo
        RealEstateSold, // Mismo
        Released, // MCL, Mismo
        RelievedInBankruptcy, // MCL
        Rescinded, // Mismo
        Revived, // MCL
        Satisfied, // MCL, Mismo
        Settled, // Mismo
        TerminatedUnsuccessfully, // MCL
        Unknown, // Mismo
        Unreleased, // MCL, Mismo
        Unsatisfied,
        Vacated, // MCL, Mismo
        VacatedOrReversed, // MCL
        VoluntarilyDischarged, // Mismo
        Voluntary, // MCL
        VoluntaryDismissal, // MCL
        VoluntaryWithdrawal, // MCL
        Withdrawn // Mismo    
    }


    public enum E_CreditPublicRecordType 
    {
        Annulment, // Mismo
        Attachment, // MCL, Mismo
        BankruptcyChapter11, // MCL, Mismo
        BankruptcyChapter12, // MCL, Mismo
        BankruptcyChapter13, // MCL, Mismo
        BankruptcyChapter7, // MCL, Mismo
        BankruptcyChapter7Involuntary, // Mismo only
        BankruptcyChapter7Voluntary, // Mismo only
        BankruptcyTypeUnknown, // MCL, Mismo
        ChildSupport, // MCL
        Collection, // MCL, Mismo
        CustodyAgreement, // Mismo
        DivorceDecree, // Mismo
        FicticiousName, // Mismo
        FinancialCounseling, // MCL, Mismo
        FinancingStatement, // MCL, Mismo
        ForcibleDetainer, // MCL, Mismo
        Foreclosure, // MCL, Mismo
        Garnishment, // MCL, Mismo
        HOALien, // MCL
        HospitalLien, // MCL
        Judgment, // MCL, MIsmo
        JudicialLien, // MCL
        LawSuit, // MCL, Mismo
        Lien, // Mismo
        MechanicLien, // MCL
        NonResponsibility, // MCL, Mismo
        NoticeOfDefault, // Mismo
        Other, // Mismo
        PavingAssessmentLien, // MCL
        PublicSale, // Mismo
        RealEstateLien, // MCL
        RealEstateRecording, // Mismo
        Repossession, // Mismo
        SupportDebt, // Mismo
        TaxLienCity, // MCL, Mismo
        TaxLienCounty, // MCL, Mismo
        TaxLienFederal, // MCL, Mismo
        TaxLienOther, // MCL, Mismo
        TaxLienState, // MCL, Mismo
        Trusteeship, // MCL, Mismo
        Unknown, // Mismo
        UnlawfulDetainer, // Mismo
        WaterAndSewerLien, // MCL

    }

}