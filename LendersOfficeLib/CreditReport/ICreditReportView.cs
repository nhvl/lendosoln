using System;

namespace LendersOffice.CreditReport
{
	public interface ICreditReportView
	{
        bool HasPdf { get; }
        bool HasHtml { get; }
        string PdfContent { get; }
        string HtmlContent { get; }
        string RawXml { get; }
	}
}
