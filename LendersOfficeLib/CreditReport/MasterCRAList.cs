using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

using System.Xml;

using DataAccess;
using LendersOffice.Admin;
using LendersOffice.CreditReport.UniversalCreditSystems;
using LendersOffice.Common;
using LendersOffice.Security;
using LqbGrammar.Drivers.SecurityEventLogging;

namespace LendersOffice.CreditReport
{
    public class CRA
    {
        private Guid m_id;
        private string m_vendorName;
        private CreditReportProtocol m_protocol = CreditReportProtocol.Mcl;
        private string m_url;
        private bool m_isInternalOnly;
        private bool m_isWarningOn;
        private string m_warningMsg;
        private DateTime m_warningStartD;
        private string m_sProviderID;   //mcl only   av opm 25327 
        private int? voxVendorId = null;

        // nw - old constructor 
        // av 10 22 08 opm 25327 use one url and pass porovider id instead
        public CRA(Guid id, string vendorName, string url, CreditReportProtocol protocol, bool isInternalOnly, string providerId, int? voxVendorId)
        {
            m_id = id;
            m_vendorName = vendorName;
            m_url = url;
            m_protocol = protocol;
            m_isInternalOnly = isInternalOnly;
            m_isWarningOn = false;
            m_warningMsg = "";
            m_warningStartD = DateTime.MinValue;
            m_sProviderID = providerId;
            this.voxVendorId = voxVendorId;
        }

        // nw - OPM 10105 - new constructor, contain info to display warning msg to user  
        // av 10 22 08 opm 25327 use one url and pass mclcracode instead
        public CRA(Guid id, string vendorName, string url, CreditReportProtocol protocol, bool isInternalOnly, bool isWarningOn, string warningMsg, DateTime warningStartD, string providerId, int? voxVendorId)
        {
            m_id = id;
            m_vendorName = vendorName;
            m_url = url;
            m_protocol = protocol;
            m_isInternalOnly = isInternalOnly;
            m_isWarningOn = isWarningOn;
            m_warningMsg = warningMsg;
            m_warningStartD = warningStartD;
            m_sProviderID = providerId;
            this.voxVendorId = voxVendorId;
        }
        // av 10 22 08 opm 25327 use one url and pass provider id ( cra unique ) instead
        public string ProviderId
        {
            get { return m_sProviderID; }
        }
        public Guid ID
        {
            get { return m_id; }
        }
        public string VendorName
        {
            get { return m_vendorName; }
        }
        public CreditReportProtocol Protocol
        {
            get { return m_protocol; }
        }

        public string Url
        {
            get { return m_url; }
        }
        public bool IsInternalOnly
        {
            get { return m_isInternalOnly; }
        }
        public bool IsWarningOn
        {
            get { return m_isWarningOn; }
        }
        public string WarningMsg
        {
            get { return m_warningMsg; }
        }
        public DateTime WarningStartD
        {
            get { return m_warningStartD; }
        }
        public int? VoxVendorId
        {
            get { return this.voxVendorId; }
        }
    }

    public class MasterCRAList
    {
        public static List<Guid> RetrieveHiddenCras(Guid brokerID, E_ApplicationT applicationT)
        {
            BrokerDB broker = BrokerDB.RetrieveById(brokerID);
            return RetrieveHiddenCras(broker, applicationT);
        }
        public static List<Guid> RetrieveHiddenCras(BrokerDB broker, E_ApplicationT applicationT)
        {
            var list = new List<Guid>();
            XmlDocument blockedCRAsXmlContent = null;
            try
            {
                if (broker.BlockedCRAsXmlContent != null && broker.BlockedCRAsXmlContent != "")
                {
                    blockedCRAsXmlContent = Tools.CreateXmlDoc(broker.BlockedCRAsXmlContent);
                }
            }
            catch { }

            if (null != blockedCRAsXmlContent)
            {
                string xpath = "";
                if (applicationT == E_ApplicationT.LendersOffice)
                {
                    xpath = "//list/lo/item";
                }
                else
                {
                    xpath = "//list/pml/item";
                }
                XmlNodeList nodeList = blockedCRAsXmlContent.SelectNodes(xpath);
                foreach (XmlElement el in nodeList)
                {
                    try
                    {
                        list.Add(new Guid(el.GetAttribute("id")));
                    }
                    catch { }
                }


            }


            return list;

        }
        public static IEnumerable<CRA> RetrieveCrabyMclCraCode(string MclCraCode)
        {
            List<CRA> list = new List<CRA>();


            if (string.IsNullOrEmpty(MclCraCode) || MclCraCode.Length != 2)
            {
                return list;
            }

            SqlParameter[] parameters = { new SqlParameter("@CraCode", MclCraCode) };

            //If parameter is NULL, stored procedure returns list of ALL cra's with non-empty CraCode
            using (var reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShareROnly, "RetrieveCrabyMclCraCode", parameters))
            {
                while (reader.Read())
                {
                    Guid comID = (Guid)reader["ComId"];
                    string comNm = (string)reader["ComNm"];
                    string comUrl = (string)reader["ComUrl"];
                    CreditReportProtocol protocolType = (CreditReportProtocol)(int)reader["ProtocolType"];
                    bool isInternalOnly = (bool)reader["IsInternalOnly"];
                    string mclcracode = (string)reader["MclCraCode"];// av 10 22 08 opm 25327 use one url and pass mclcracode instead
                    int? voxVendorId = null;
                    if (reader["VoxVendorId"] != DBNull.Value)
                    {
                        voxVendorId = (int)reader["VoxVendorId"];
                    }

                    list.Add(new CRA(comID, comNm, comUrl, protocolType, isInternalOnly, mclcracode, voxVendorId));
                }
            }

            return list;
        }
        public static IEnumerable<CRA> RetrieveAvailableCras(BrokerDB broker)
        {
            E_ShowCredcoOptionT showCredcoOptionT = E_ShowCredcoOptionT.DisplayOnlyFNMA;

            if (null != broker)
            {
                showCredcoOptionT = broker.ShowCredcoOptionT;
            }

            List<CRA> list = new List<CRA>();
            using (var reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShareROnly, "ListAllCras"))
            {
                while (reader.Read())
                {
                    Guid comID = (Guid)reader["ComId"];
                    string comNm = (string)reader["ComNm"];
                    string comUrl = (string)reader["ComUrl"];
                    CreditReportProtocol protocolType = (CreditReportProtocol)(int)reader["ProtocolType"];
                    bool isInternalOnly = (bool)reader["IsInternalOnly"];
                    string mclcracode = (string)reader["MclCraCode"];// av 10 22 08 opm 25327 use one url and pass mclcracode instead
                    int? voxVendorId = null;
                    if (reader["VoxVendorId"] != DBNull.Value)
                    {
                        voxVendorId = (int)reader["VoxVendorId"];
                    } 

                    if (comID == UniversalCreditHelper.ID)
                    {
                        if (null != broker)
                        {
                            if (!UniversalCreditHelper.AllowForThisBroker(broker.BrokerID))
                            {
                                // This broker doesn't have access to Universal Credit
                                continue;
                            }
                        }
                    }
                    if (protocolType == CreditReportProtocol.Credco && showCredcoOptionT == E_ShowCredcoOptionT.DisplayOnlyFNMA)
                    {
                        // Broker only want to display Credco using FNMA connection
                        continue;
                    }
                    if (protocolType == CreditReportProtocol.FannieMae && comUrl == "001" && showCredcoOptionT == E_ShowCredcoOptionT.DisplayOnlyDirect)
                    {
                        continue;
                    }


                    if (!isInternalOnly)
                    {
                        // 10/29/2008 dd - Only add CRA that does not mark as Internal Only.
                        list.Add(new CRA(comID, comNm, comUrl, protocolType, isInternalOnly, mclcracode, voxVendorId));
                    }
                }
            }

            return list;

        }
        public static IEnumerable<CRA> RetrieveAvailableCras(Guid brokerID)
        {
            BrokerDB broker = null;
            if (brokerID != Guid.Empty)
            {
                // 6/18/2009 dd - BrokerID could be empty if add new broker through LOAdmin.
                broker = BrokerDB.RetrieveById(brokerID);
            }
            return RetrieveAvailableCras(broker);
        }
        private static bool IsBrokerAllowToCra(string key, XmlDocument options)
        {
            if (null != options)
            {
                XmlElement node = (XmlElement)options.SelectSingleNode("//option[@name='" + key + "']");

                if (null != node)
                {
                    return node.GetAttribute("value").ToLower() == "true";
                }
            }

            return false;
        }

        public static CRA FindById(Guid id, bool includeInvalidCRAs)
        {
            CRA cra = null;
            SqlParameter[] parameters = {
                                            new SqlParameter("@ComId", id),
                                            new SqlParameter("@returnInvalid", includeInvalidCRAs)
                                        };

            using (var reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShareROnly, "RetrieveServiceCompanyByID", parameters))
            {
                if (reader.Read())
                {
                    Guid comID = (Guid)reader["ComId"];
                    string comNm = (string)reader["ComNm"];
                    string comUrl = (string)reader["ComUrl"];
                    CreditReportProtocol protocolType = (CreditReportProtocol)(int)reader["ProtocolType"];
                    bool isInternalOnly = (bool)reader["IsInternalOnly"];
                    bool isWarningOn = (bool)reader["IsWarningOn"];
                    string warningMsg = (string)reader["WarningMsg"];
                    DateTime warningStartD = DateTime.MinValue;
                    if (reader["WarningStartD"] != DBNull.Value)
                    {
                        warningStartD = (DateTime)reader["WarningStartD"];
                    }
                    int? voxVendorId = null;
                    if (reader["VoxVendorId"] != DBNull.Value)
                    {
                        voxVendorId = (int)reader["VoxVendorId"];
                    }

                    string mclcracode = (string)reader["MclCraCode"];// av 10 22 08 opm 25327 use one url and pass mclcracode instead
                    cra = new CRA(comID, comNm, comUrl, protocolType, isInternalOnly, isWarningOn, warningMsg, warningStartD, mclcracode, voxVendorId);
                }
            }
            return cra;
        }

        /// <summary>
        /// Returns the cra info for the given id. If the id does not belong to cra but a credit account proxy thtat information is returned
        /// and the information to the cra that the credit account proxy is pointing to. Note: This requires a broker id if searchProxyTable is on 
        /// for security purposes. 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="searchProxyTable">Whether a credit account proxy should be searched for if the cra doesnt exist</param>
        /// <param name="brokerId">Requesters broker id.</param>
        /// <returns>Null if the cra doesnt exist</returns>
        public static CRA FindById(Guid id, bool searchProxyTable, Guid brokerId)
        {
            return FindById(id, searchProxyTable, brokerId, /*includeInvalidCRAs*/false);
        }

        /// <summary>
        /// Returns the cra info for the given id. If the id does not belong to cra but a credit account proxy thtat information is returned
        /// and the information to the cra that the credit account proxy is pointing to. Note: This requires a broker id if searchProxyTable is on 
        /// for security purposes. 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="searchProxyTable">Whether a credit account proxy should be searched for if the cra doesnt exist</param>
        /// <param name="brokerId">Requesters broker id.</param>
        /// <param name="includeInvalidCRAs">Includes CRAs set as invalid. It should always be false in LO/PML (AUD needs it).</param>
        /// <returns>Null if the cra doesnt exist</returns>
        public static CRA FindById(Guid id, bool searchProxyTable, Guid brokerId, bool includeInvalidCRAs)
        {
            CRA cra = FindById(id, includeInvalidCRAs);

            if (cra == null && searchProxyTable && brokerId != Guid.Empty)
            {
                var proxy = CreditReportAccountProxy.Helper.RetrieveCreditProxyByProxyID(brokerId, id);
                if (proxy != null)
                {
                    CRA actualCra = FindById(proxy.CraId, includeInvalidCRAs);
                    LenderMappedCRA tCra = new LenderMappedCRA(actualCra.ID, actualCra.VendorName, actualCra.Url, actualCra.Protocol, actualCra.IsInternalOnly, actualCra.IsWarningOn, actualCra.WarningMsg, actualCra.WarningStartD, actualCra.ProviderId, actualCra.VoxVendorId);
                    tCra.AccountIdentifier = proxy.CraAccId;
                    tCra.Username = proxy.CraUserName;
                    tCra.Password = proxy.CraPassword;
                    tCra.FannieMaeMORNETUserID = proxy.CreditMornetPlusUserId;
                    tCra.FannieMaeMORNETPassword = proxy.CreditMornetPlusPassword;
                    tCra.IncludeEquifax = proxy.IsEquifaxPulled;
                    tCra.IncludeExperian = proxy.IsExperianPulled;
                    tCra.IncludeTransUnion = proxy.IsTransUnionPulled;
                    tCra.IncludeFannie = proxy.IsFannieMaePulled;
                    cra = tCra;
                }
            }

            return cra;

        }

    }

    public class LenderMappedCRA : CRA
    {
        public string AccountIdentifier { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string FannieMaeMORNETUserID { get; set; }
        public string FannieMaeMORNETPassword { get; set; }
        public bool IncludeEquifax { get; set; }
        public bool IncludeExperian { get; set; }
        public bool IncludeTransUnion { get; set; }
        /// <summary>
        /// This is actually obselete leaving it though. 
        /// </summary>
        public bool IncludeFannie { get; set; }

        public LenderMappedCRA(Guid id, string vendorName, string url, CreditReportProtocol protocol, bool isInternalOnly, bool isWarningOn, string warningMsg, DateTime warningStartD, string providerId, int? voxVendorId) :
            base(id, vendorName, url, protocol, isInternalOnly, isWarningOn, warningMsg, warningStartD, providerId, voxVendorId) { }
    }
}
