﻿// Generated via GenerateRecordClass.ttinclude
namespace LendersOffice.CreditReport
{
    /// <summary>
    /// Encapsulates a document received from the credit vendor, e.g., for an additional service.
    /// </summary>
    public partial class CreditVendorAdditionalDocument
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CreditVendorAdditionalDocument"/> class.
        /// </summary>
        /// <param name="fileName">The file name given to the document by the vendor to uniquely identify it.</param>
        /// <param name="base64">The base-64 encoded data of the file.</param>
        public CreditVendorAdditionalDocument(string fileName, LqbGrammar.DataTypes.Base64EncodedData base64)
        {
            this.FileName = fileName;
            this.Base64 = base64;
        }

        /// <summary>
        /// Gets the file name given to the document by the vendor to uniquely identify it.
        /// </summary>
        public string FileName { get; }

        /// <summary>
        /// Gets the base-64 encoded data of the file.
        /// </summary>
        public LqbGrammar.DataTypes.Base64EncodedData Base64 { get; }
    }
}