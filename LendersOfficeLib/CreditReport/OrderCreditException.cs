///
/// Author: David Dao
///
using System;

using LendersOffice.Common;
using DataAccess;

namespace LendersOffice.CreditReport
{
	public class OrderCreditException : CBaseException
	{
        public OrderCreditException(string customError, string devErrorMessage) : base (customError, devErrorMessage)
        {

        }
        public OrderCreditException(string devErrorMessage) : base(ErrorMessages.CreditReport_GenericError, devErrorMessage) 
		{
		}
        public override string EmailSubjectCode 
        {
            get { return "ORDER_CREDIT"; }
        }
	}
}
