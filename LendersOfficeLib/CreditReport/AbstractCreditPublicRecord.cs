using System;
using System.Xml;

using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.CreditReport
{
    public abstract class AbstractCreditPublicRecord 
    {
        private AbstractCreditReport m_parentCreditReport = null;

        protected AbstractCreditReport ParentCreditReport 
        {
            set { m_parentCreditReport = value; }
        }

        public void LogCreditWarning(Exception exc) 
        {
            if (null != exc)
                LogCreditWarning(exc.ToString());
        }
        public void LogCreditWarning(string msg) 
        {
            if (null != m_parentCreditReport)
                m_parentCreditReport.LogCreditWarning(msg);
            else
                Tools.LogWarning(msg); // Parent object should not be null. But add this else just in case.

        }

        public void LogCreditBug(string msg) 
        {
            if (null != m_parentCreditReport)
                m_parentCreditReport.LogCreditBug(msg);
            else
                Tools.LogBug(msg); // Parent object should not be null. But add this else just in case.
        }
        #region Abstract methods required to be implement by class inherited from this.
        public abstract string ID { get; }

        // Return DateTime.MinValue for invalid data.
        public abstract DateTime ReportedDate { get; } // 11/13/2004 dd - I used MISMO attribute name rather than MCL attribute name.

        // Return DateTime.MinValue for invalid data.
        public abstract DateTime DispositionDate { get; } // 11/13/2004 dd - I used MISMO attribute name rather than MCL attribute name.
        public abstract E_CreditPublicRecordDispositionType DispositionType { get; }
        public abstract decimal BankruptcyLiabilitiesAmount { get; }
        public abstract E_CreditPublicRecordType Type { get; }
        public abstract E_PublicRecordOwnerT PublicRecordOwnerT { get; }
        #endregion

        public abstract DateTime LastEffectiveDate 
        {
            get ;
        }


        public abstract DateTime BkFileDate 
        {
            get;
        }


        public abstract string CourtName 
        {
            get;
        }

        public bool IsBankruptcyFiled 
        {
            get 
            {
                // 9/9/2004 dd - A bankruptcy consider to be filed if the following criteria match.
                //    PublicRecordType = B1,  Chapter 11 Bankruptcy
                //                       B2,  Chapter 12 Bankruptcy
                //                       B3,  Chapter 13 Bankruptcy
                //                       B7,  Chapter 7 Bankruptcy
                //                       BK   Bankruptcy (General)
                //    AND
                //    StatusText = "FILED", "NOT RELEASED", "NOT SATISFIED", "REVIVED"
                //

                switch (Type) 
                {
                    case E_CreditPublicRecordType.BankruptcyChapter11:
                    case E_CreditPublicRecordType.BankruptcyChapter12:
                    case E_CreditPublicRecordType.BankruptcyChapter13:
                    case E_CreditPublicRecordType.BankruptcyChapter7:
                    case E_CreditPublicRecordType.BankruptcyChapter7Involuntary:
                    case E_CreditPublicRecordType.BankruptcyChapter7Voluntary:
                    case E_CreditPublicRecordType.BankruptcyTypeUnknown:
                    {
                        switch (DispositionType) 
                        {
                            case E_CreditPublicRecordDispositionType.Filed:
                            case E_CreditPublicRecordDispositionType.Unreleased:
                            case E_CreditPublicRecordDispositionType.Unsatisfied:
                            case E_CreditPublicRecordDispositionType.Revived:
                                return true;
                        }
                        break;
                    }
                }
                return false;

            }
        }

		public bool IsDischarged
		{
			get
			{
				switch( DispositionType )
				{
					case E_CreditPublicRecordDispositionType.Discharged:
					case E_CreditPublicRecordDispositionType.Dismissed:
						return true;
					default: 
						return false;
				}
			}
		}

		internal bool IsBkOfTypeAndCharacter( E_BkType types, E_BkChar bkChars)
		{
			return IsBkOfTypeAndCharacter(types, bkChars, "");
		}

        internal bool IsBkOfTypeAndCharacter( E_BkType types, E_BkChar bkChars, string sBkKeywordType) 
        {
            switch (Type) 
            {
                case E_CreditPublicRecordType.BankruptcyChapter11:
                    if (0 == (types & E_BkType.Ch11))
                        return false;
                    break;
                case E_CreditPublicRecordType.BankruptcyChapter12:
                    if (0 == (types & E_BkType.Ch12))
                        return false;
                    break;
                case E_CreditPublicRecordType.BankruptcyChapter13:
                    if (0 == (types & E_BkType.Ch13))
                        return false;
                    break;
                case E_CreditPublicRecordType.BankruptcyChapter7:
                case E_CreditPublicRecordType.BankruptcyChapter7Involuntary:
                case E_CreditPublicRecordType.BankruptcyChapter7Voluntary:
                    if (0 == (types & E_BkType.Ch7))
                        return false;
                    break;
                case E_CreditPublicRecordType.BankruptcyTypeUnknown:
                    if (0 == (types & E_BkType.UnknownType))
                        return false;
                    break;
                default:
                    return false;

            }
            if (bkChars == E_BkChar.Any)
                return true;

            switch (DispositionType) 
            {
				case E_CreditPublicRecordDispositionType.Discharged:
				{
					switch (bkChars)
					{
						case E_BkChar.Discharged:
						case E_BkChar.Closed:
							return true;
						case E_BkChar.Dismissed:
						case E_BkChar.NondischargedAndNonpaid:
							return false;
						default:
							Tools.LogBug("Unhandled enum value of E_BkChar");
							throw new CBaseException( ErrorMessages.EnumValueNotHandled, "Unhandled enum value of E_BkChar" );
					}
				}
                case E_CreditPublicRecordDispositionType.Dismissed:
                {
                    switch (bkChars) 
                    {
						case E_BkChar.Dismissed:
						case E_BkChar.Closed:
							return true;
                        case E_BkChar.Discharged:
                        case E_BkChar.NondischargedAndNonpaid:
							return false;
                        default:
                            Tools.LogBug("Unhandled enum value of E_BkChar");
                            throw new CBaseException( ErrorMessages.EnumValueNotHandled, "Unhandled enum value of E_BkChar" );

                    }
                }
                case E_CreditPublicRecordDispositionType.Paid:
                {
                    switch( bkChars )
                    {
                        case E_BkChar.Discharged:
						case E_BkChar.Closed:
							if (sBkKeywordType.StartsWith("HasDischarged"))
								// old BK keywords return false for public record with status "Paid"
								return false;
							else
								// new BK keywords will treat "Paid" as discharged and closed
								return true;
                        case E_BkChar.Dismissed:
                        case E_BkChar.NondischargedAndNonpaid:
                            return false;
                        default:
                            Tools.LogBug( "Unhandled enum value of E_BkChar" );
                            throw new CBaseException( ErrorMessages.EnumValueNotHandled, "Unhandled enum value of E_BkChar" );
                    }
                }
                default: 
                {
                    switch( bkChars )
                    {
                        case E_BkChar.NondischargedAndNonpaid:
                            return true;
						case E_BkChar.Discharged:
						case E_BkChar.Dismissed:
						case E_BkChar.Closed:
							return false;
                        default:
                            Tools.LogBug( "Unhandled enum value of E_BkChar" );
                            throw new CBaseException( ErrorMessages.EnumValueNotHandled, "Unhandled enum value of E_BkChar" );
                    }
                }
            }

        }

        public bool IsCollection 
        {
            get { return Type == E_CreditPublicRecordType.Collection; }
        }


        public bool IsCollectionPaid 
        {
            get { return IsCollection && !IsActive; }
        }


        public bool IsCollectionUnpaid 
        {
            get { return IsCollection && IsActive; }
        }


        public bool IsJudgment 
        {
            get { return Type == E_CreditPublicRecordType.Judgment; }
        }


        public bool IsJudgmentUnpaid 
        {
            get { return IsJudgment && IsActive; }
        }


        public bool IsJudgmentPaid 
        {
            get { return IsJudgment && !IsActive; }
        }


		public bool IsForeclosure
		{
			get { return Type == E_CreditPublicRecordType.Foreclosure; }
		}


		public bool IsForeclosurePaid 
		{
			get { return IsForeclosure && !IsActive; }
		}


		public bool IsForeclosureUnpaid 
		{
			get { return IsForeclosure && IsActive; }
		}


        public bool IsDerogUnpaid
        {
            get 
            {
                return IsCollectionUnpaid || IsJudgmentUnpaid || IsLienUnpaid || IsForeclosureUnpaid;
            }
        }


        private bool IsActive 
        {
            get 
            {
                switch (DispositionType) 
                {
                    case E_CreditPublicRecordDispositionType.Canceled:
                    case E_CreditPublicRecordDispositionType.Discharged:
                    case E_CreditPublicRecordDispositionType.Dismissed:
                    case E_CreditPublicRecordDispositionType.Paid:
                    case E_CreditPublicRecordDispositionType.Completed:
                    case E_CreditPublicRecordDispositionType.Released:
                    case E_CreditPublicRecordDispositionType.Satisfied:
                    case E_CreditPublicRecordDispositionType.Vacated:
                    case E_CreditPublicRecordDispositionType.VacatedOrReversed:
                    case E_CreditPublicRecordDispositionType.VoluntaryDismissal:
                    case E_CreditPublicRecordDispositionType.VoluntaryWithdrawal:
                    case E_CreditPublicRecordDispositionType.VoluntarilyDischarged:
					case E_CreditPublicRecordDispositionType.Settled: // 3/3/06: While looking at this code, I believe 'Settled' should be on this list as well.
                        return false;
                    default:
                        return true;
                }
            }
        }


        public bool IsNontaxLien 
        {
            get 
            {
                switch (Type) 
                {
                    case E_CreditPublicRecordType.HOALien:
                    case E_CreditPublicRecordType.HospitalLien:
                    case E_CreditPublicRecordType.JudicialLien:
                    case E_CreditPublicRecordType.MechanicLien:
                    case E_CreditPublicRecordType.RealEstateLien:
                    case E_CreditPublicRecordType.WaterAndSewerLien:
                    case E_CreditPublicRecordType.Lien:
                        return true;
                    default:
                        return false;
                    
                }
            }
        }


        public bool IsTaxLien 
        {
            get 
            {
                switch (Type) 
                {
                    case E_CreditPublicRecordType.TaxLienCounty:
                    case E_CreditPublicRecordType.TaxLienCity:
                    case E_CreditPublicRecordType.TaxLienFederal:
                    case E_CreditPublicRecordType.TaxLienState:
                    case E_CreditPublicRecordType.TaxLienOther:
                        return true;
                    default:
                        return false;
                }
            }
        }


        public bool IsNontaxLienUnpaid 
        {
            get { return IsNontaxLien && IsActive; }
        }


        public bool IsNontaxLienPaid 
        {
            get { return IsNontaxLien && !IsActive; }
        }


        public bool IsTaxLienUnpaid 
        {
            get { return IsTaxLien && IsActive; }
        }

        
        public bool IsLien 
        {
            get 
            {
                switch (Type) 
                {
                    case E_CreditPublicRecordType.HOALien:
                    case E_CreditPublicRecordType.HospitalLien:
                    case E_CreditPublicRecordType.JudicialLien:
                    case E_CreditPublicRecordType.Lien:
                    case E_CreditPublicRecordType.MechanicLien:
                    case E_CreditPublicRecordType.RealEstateLien:
                    case E_CreditPublicRecordType.PavingAssessmentLien:
                    case E_CreditPublicRecordType.TaxLienCity:
                    case E_CreditPublicRecordType.TaxLienCounty:
                    case E_CreditPublicRecordType.TaxLienFederal:
                    case E_CreditPublicRecordType.TaxLienState:
                    case E_CreditPublicRecordType.TaxLienOther:
                    case E_CreditPublicRecordType.WaterAndSewerLien:
                        return true;
                    default:
                        return false;
                }
            }
        }


        public bool IsLienUnpaid 
        {
            get { return IsLien && IsActive; }
        }

        protected int GetInt(XmlElement el, string attrName) 
        {
            try 
            {
                return int.Parse(el.GetAttribute(attrName));
            } 
            catch 
            {
                // 5/17/2005 dd - Only log for non-empty string value.
                if (el.GetAttribute(attrName) != "")
                    LogCreditWarning("AbstractCreditPublicRecord.GetInt: Unable to parse [" + el.GetAttribute(attrName) + "]. Default to 0.");
                return 0;
            }
        }
        protected decimal GetDecimal(XmlElement el, string attrName) 
        {
            try 
            {
                // 12/1/2004 dd - Advantage Credit FL include $ symbol in their _BankruptcyLiabilitiesAmount
                string s = el.GetAttribute(attrName).Replace("$", "");
                return decimal.Parse(s);
            } 
            catch 
            {
                // 5/17/2005 dd - Only log for non-empty string value.
                if (el.GetAttribute(attrName) != "")
                    LogCreditWarning("AbstractCreditPublicRecord.GetDecimal: Unable to parse [" + el.GetAttribute(attrName) + "]. Default to 0.");
                return 0.0M;
            }
        }
    }



}
