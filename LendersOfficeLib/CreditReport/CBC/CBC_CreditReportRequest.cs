using System;
using System.Collections.Generic;
using System.Net;
using System.Xml;
using LendersOffice.CreditReport.Mismo;
using DataAccess;
using CreditCardSystemLib;

namespace LendersOffice.CreditReport.CBC
{
	public class CBC_CreditReportRequest : LendersOffice.CreditReport.Mismo2_3.Mismo2_3CreditReportRequest
	{
        private const string UrlBase = "https://www.creditbureaureports.com";

        private const string TestUserName = "qa42026";
        private const string TestUrlBase = "https://qa.creditbureaureports.com";

        private List<string> urlOptions = new List<string>
        {
            "ORD=IN",
            "PA=XM",
            "TEXT=N",
            "PS=A",
            "REVL=Y",
            "REVF=X4",
            "software=IL", // 7/25/2012 dd - OPM 88848 - Add 2 digit code IL to specific it come from ILS Lending QB Interface
        };

        public string BillingFirstName { get; set; }
        public string BillingSurName { get; set; }
        public string CardNumber { get; set; }
        public string BillingStreet { get; set; }
        public string BillingCity { get; set; }
        public string BillingState { get; set; }
        public string BillingZip { get; set; }
        public int ExpirationMonth { get; set; }
        public int ExpirationYear { get; set; }
        public string BillingCvv { get; set; }

        public void SetIsMortgageOnlyReport(bool value)
        {
            if (value)
            {
                CreditReportType = E_CreditReportType.Other;
                CreditReportTypeOtherDescription = "MO";

            }
            else
            {
                CreditReportType = E_CreditReportType.Merge;
                CreditReportTypeOtherDescription = string.Empty;
            }
        }
		public CBC_CreditReportRequest() : base("")
		{
            AppendOutputFormat(E_MismoOutputFormat.PDF); // OPM 51271 - order CBC credit requests as PDF instead of text
		}

		public override string Url
		{
			get { return this.UrlSchemeAndServer + this.UrlPathAndQuery; }
        }

        private string UrlSchemeAndServer
        {
            get { return string.Equals(this.LoginInfo.UserName, TestUserName) ? TestUrlBase : UrlBase; }
        }

        private string UrlPathAndQuery
        {
            get { return $"/servlet/gnbank?logid={WebUtility.UrlEncode(this.LoginInfo.UserName)}&command=apiordretpost&options={WebUtility.UrlEncode(string.Join(" ", this.urlOptions))}"; }
        }

        private string brandedProductName = null;
        public override bool CanConvertRequestTypeToLqi => true;
        public override void ConvertRequestTypeToLqi(CreditRequestData creditRequestData)
        {
            this.urlOptions.Insert(1, "UP=LN");
            this.RequestType = E_CreditReportRequestActionType.Upgrade;
            this.CreditReportType = E_CreditReportType.Other;
            this.CreditReportTypeOtherDescription = "LQ";
            this.brandedProductName = "LQCC Report";
        }

        public override ICreditReportResponse CreateResponse(XmlDocument doc)
        {
            return new CBC_CreditReportResponse(doc, LoginInfo, brandedProductName);
        }

		public override void SetupAuthentication(HttpWebRequest webRequest) 
		{
            webRequest.ContentType = "text/xml";
		}
        public override System.Xml.XmlDocument GenerateXml()
        {
            System.Xml.XmlDocument xmlDoc = base.GenerateXml();
            XmlNode node = xmlDoc.SelectSingleNode("//REQUEST/REQUEST_DATA/CREDIT_REQUEST");
            if (node != null && UseCreditCardPayment)
            {
                XmlElement creditElement = CreateCreditCardPaymentElement(xmlDoc);
                if (creditElement != null) node.AppendChild(creditElement);
            }
            node = xmlDoc.SelectSingleNode("//REQUEST");
            node.AppendChild(CreateKeyElement(xmlDoc));
            return xmlDoc;
        }
        private XmlElement CreateKeyElement(XmlDocument doc)
        {
            XmlElement el = doc.CreateElement("KEY");
            el.SetAttribute("_Name", "Loan Number");
            el.SetAttribute("_Value", LenderCaseIdentifier);
            return el;
        }

        private XmlElement CreateCreditCardPaymentElement(XmlDocument doc)
        {
                XmlElement el = doc.CreateElement("SERVICE_PAYMENT");
                el.SetAttribute("_AccountHolderName", BillingFirstName + " " + BillingSurName);                
                el.SetAttribute("_AccountHolderStreetAddress", BillingStreet);
                el.SetAttribute("_AccountHolderStreetAddress2", "");
                el.SetAttribute("_AccountHolderCity", BillingCity);
                el.SetAttribute("_AccountHolderState", BillingState);
                el.SetAttribute("_AccountHolderPostalCode", BillingZip);
                el.SetAttribute("_AccountHolderTelephoneNumber", "");                
                el.SetAttribute("_AccountIdentifier", CardNumber);
                el.SetAttribute("_AccountExpirationDate", ExpirationYear.ToString() + "-" +ExpirationMonth.ToString());
                el.SetAttribute("_SecondaryAccountIdentifier", BillingCvv);

                CardNumber ccNumber = new CardNumber(CardNumber);                
                string sCardType = CreditReportUtilities.GetMismoCCTypeFromCCSystemCardType(ccNumber.CardType);
                el.SetAttribute("_MethodType", sCardType);
                el.SetAttribute("_MethodTypeOtherDescription", sCardType == "Other" ? ccNumber.CardType : string.Empty);
                return el;
        }
	}
}
