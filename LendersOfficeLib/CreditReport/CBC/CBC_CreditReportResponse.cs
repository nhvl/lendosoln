using System;
using System.Xml;
using DataAccess;
using LendersOffice.Email;

namespace LendersOffice.CreditReport.CBC
{
	public class CBC_CreditReportResponse : LendersOffice.CreditReport.ICreditReportResponse
	{
		private string		m_reportID;
		private string		m_status;
		private bool		m_hasError;
		private bool		m_isReady;
		protected string	m_errorMessage;
		private string		m_rawXmlResponse = "";

        private LoginInfo m_requestingUserLoginInfo;

        public CBC_CreditReportResponse(XmlDocument doc, LoginInfo requestingUserLoginInfo, string brandedProductName)
        {
            this.m_requestingUserLoginInfo = requestingUserLoginInfo;
            this.BrandedProductName = brandedProductName;

            if (doc != null)
            {
                this.Parse(doc);
            }
        }

		protected virtual string DetermineErrorStatus(string code) 
		{
			return "ERROR"; // Generic error status
		}

		private void Parse(XmlDocument doc) 
		{
			XmlElement oCreditResponseElement = (XmlElement) doc.SelectSingleNode("//RESPONSE_GROUP/RESPONSE/RESPONSE_DATA/CREDIT_RESPONSE");
			if (null != oCreditResponseElement && oCreditResponseElement.GetAttribute("CreditReportType").ToUpper() != "ERROR")
			{
				// No error occurs.
				m_reportID = oCreditResponseElement.GetAttribute("CreditReportIdentifier");
				m_isReady = true;
				m_status = "READY"; 
			}
			else
			{
                XmlElement errorStatusElement = (XmlElement)doc.SelectSingleNode("//RESPONSE_GROUP/RESPONSE/STATUS");
                XmlElement errorElement = (XmlElement)doc.SelectSingleNode("//RESPONSE_GROUP/RESPONSE/RESPONSE_DATA/CREDIT_RESPONSE/CREDIT_ERROR_MESSAGE");
                if (null != errorStatusElement)
                {
                    m_isReady = false;
                    m_hasError = true;
                    m_status = "ERROR";

                    XmlAttribute errorTextElement = errorStatusElement.Attributes["_Description"];
                    if (null != errorTextElement)
                    {
                        m_errorMessage = errorTextElement.InnerText;
                    }
                    else
                    {                        
                        m_errorMessage = "Error while ordering report.";
                    }
                    m_errorMessage += "<br> If you are unable to login for credit after several tries, please contact your Credit Reporting Agency for assistance.";
                }
                else if (null != errorElement)
                {
                    m_isReady = false;
                    m_hasError = true;
                    XmlNodeList nodeList = errorElement.SelectNodes("_Text");
                    m_status = "ERROR";

                    //XmlNodeList msgNodes = errorElement.ChildNodes;
                    foreach (XmlNode msgNode in nodeList)
                    {
                        m_errorMessage += msgNode.InnerText + "<br/>";
                    }
                    m_errorMessage += "Your credit request could not be completed. Please contact CBC's Support for assistance.<br/>";

                    //Check Unauthorized machine problem. OPM 61304
                    if (m_errorMessage.Contains("unauthorized machine"))
                    {
                        m_errorMessage = @"Please contact Macs.Support@cbc-companies.com and ask them to do the following for your CBC account: 
1. Add IP address range 12.106.86.X to the list of accepted ranges  
2. Enable XML format";

//                        //Change the message returned to the user
//                        m_errorMessage = @"No record was generated because of the following problems:
//You are trying to access from an unauthorized machine.
//To resolve this issue, please contact CBC support at Macs.Support@cbc-companies.com and ask them to add 'LendingQB' to your authorized machines.";
                    }
                }
                else
                {
                    throw new DataAccess.CBaseException("Error while ordering report", "CBC response indicates an error, but no error element is present. Your credit request could not be completed. Please contact CBC's Support for assistance.");
                }
			}
			if (m_isReady) 
				m_rawXmlResponse = doc.InnerXml;
		}
       
		#region Implementation of ICreditReportResponse
		public string ReportID 
		{
			get { return m_reportID; }
		}
		public string Status 
		{
			get { return m_status; }
		}

		public bool HasError 
		{
			get { return m_hasError; }
		}
		public string ErrorMessage 
		{
			get { return m_errorMessage; }
		}

		public bool IsReady 
		{
			get { return m_isReady; }
		}

		public string RawXmlResponse 
		{
			get { return m_rawXmlResponse; }
		}
        public string BrandedProductName { get; }
        #endregion
	}
}
