using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.CreditReport.Mcl;
using System.Collections.Specialized;
using System.Text.RegularExpressions;
using System.Text;
using System.Linq;

namespace LendersOffice.CreditReport
{

    public class CreditMigratedAttribute : Attribute
    {
    }
    #region README NOTES
#if(NOTES)
		1.	Collection: is an event that lasts for some time. It�s more of a state.
			a.	The affective date should be of the first date the collection event shows up in payment history not the last one � based on a search of past to now (reverse of what we actually do in the code)
		2.	ChargeOff, Repossession, Foreclosure: one time event
			a.	The affective date is the last one we find � based on a search of past to now (reverse of what we actually do in the code)
		3.	For public record: always use �status date� for all date inquiries as it�s the one represent when the bank decides the public record to have the new status. The report date is when the beauro receives the last update.
		4.	Late 30 and 60 and 90 that are already parsed (shown in the credit report printout) they are non-rolling non-progressive.
		5.	The (balance > 0) test to determine if an account is unpaid is applicable for only and always for tradeline  (not public record)
#endif
    #endregion

	public class CreditReportDebugInfo 
	{
		private Guid m_sLId;
		private string m_loginNm;
		private string m_fileDbKey;

		public CreditReportDebugInfo(Guid sLId, string loginNm) 
		{
			m_sLId = sLId;
			m_loginNm = loginNm;
		}
		public string FileDbKey 
		{
			set { m_fileDbKey = value; }
		}

		public override string ToString() 
		{
			return string.Format("[CreditReportDebugInfo: sLId={0}, LoginNm={1}, FileDbKey={2}]", m_sLId, m_loginNm, m_fileDbKey);
		}
		public void LogCreditWarning(string msg) 
		{
			System.Diagnostics.StackTrace st = new System.Diagnostics.StackTrace();
			Tools.LogWarning(string.Format("{0}{1}{1}[DEBUG INFO]: {2}{1}{1}[CALL STACK]: {3}", msg, Environment.NewLine, ToString(), st));
		}

		public void LogCreditBug(string msg) 
		{
			System.Diagnostics.StackTrace st = new System.Diagnostics.StackTrace();
			Tools.LogBug(string.Format("{0}{1}[DEBUG INFO]: {2}{1}[CALL STACK]: {3}", msg, Environment.NewLine, ToString(), st));
		}
	}
	public abstract class AbstractCreditReport : ICreditReport
	{

		private bool m_isDebug = false;
		private Hashtable m_tagHashTable = null;
		private CreditReportDebugInfo m_extraDebugInfo = null;
        private E_aBorrowerCreditModeT m_aBorrowerCreditModeT = E_aBorrowerCreditModeT.Both;

        public E_aBorrowerCreditModeT aBorrowerCreditModeT
        {
            get { return m_aBorrowerCreditModeT; }
            set { m_aBorrowerCreditModeT = value; }
        }

		public bool IsDebug 
		{
			get { return m_isDebug; }
			set { m_isDebug = value; }
		}


        #region Logging Utilities
        [CreditMigrated]
		public void LogCreditWarning(Exception exc) 
		{
			if (null != exc)
				LogCreditWarning(exc.ToString());
		}

        [CreditMigrated]
		public void LogCreditWarning(string msg) 
		{
			if (null != m_extraDebugInfo)
				m_extraDebugInfo.LogCreditWarning(msg);

		}

        [CreditMigrated]
		public void LogCreditBug(string msg) 
		{
			if (null != m_extraDebugInfo)
				m_extraDebugInfo.LogCreditBug(msg);

		}

		protected CreditReportDebugInfo ExtraDebugInfo 
		{
			set { m_extraDebugInfo = value; }
		}

        #endregion

        #region Abstract methods required to be implement by class inherited from this.
		public abstract DateTime CreditReportFirstIssuedDate { get; }
        public abstract DateTime CreditReportLastUpdatedDate { get; }
        public abstract CreditScore BorrowerScore { get; }
		public abstract CreditScore CoborrowerScore { get; }

		public abstract BorrowerInfo BorrowerInfo { get; }
		public abstract BorrowerInfo CoborrowerInfo { get; }
		public abstract CreditBureauInformation ExperianContactInformation { get; }
		public abstract CreditBureauInformation TransUnionContactInformation { get; }
		public abstract CreditBureauInformation EquifaxContactInformation { get; }
		public abstract ArrayList AllLiabilities { get; }
        public abstract ArrayList LiabilitiesExcludeFromUnderwriting { get; }
		public abstract ArrayList AllPublicRecords { get; }
		public abstract string CreditRatingCodeType { get; }
		public abstract bool SwapPrimaryBorrower { get; }
		public abstract void SetLoanData(CAppBase dataApp);

        public abstract CAppBase DataApp { get; }
		public abstract void Parse();
        #endregion

        /// <summary>
        /// Return liabilities based on aBorrowerCreditModeT
        /// </summary>
        public IEnumerable<AbstractCreditLiability> EffectiveLiabilities
        {
            get 
            {
                foreach (AbstractCreditLiability o in AllLiabilities)
                {
                    if (IsLiabilityExclude(o))
                        continue;
                    yield return o;
                }
            }
        }

        public int RepresentativeScore
        {
            get
            {
                
                if (m_aBorrowerCreditModeT != E_aBorrowerCreditModeT.Coborrower)
                {
                    return GetRepresentativeScore(BorrowerScore.Equifax, BorrowerScore.Experian, BorrowerScore.TransUnion);
                }
                else
                {
                    return GetRepresentativeScore(CoborrowerScore.Equifax, CoborrowerScore.Experian, CoborrowerScore.TransUnion);
                }
            }
        }

        private int GetRepresentativeScore(params int[] scores)
        {
            Array.Sort(scores);

            if (scores.Length != 3) 
            {
                throw CreateGenericExceptionWithDevMessage("Programmer Error Representative score array does not contain 3 entries.");
            }

            //if there are no scores that are 0 then the middle element is the representative score  
            //if there is one that is zero it will be element 0 of the array  so the lower score will be the middle still
            if (scores[1] != 0)
            {
                return scores[1]; 
            }
            //if the middle score is also 0 then that means that there were 2 zero scores so the third one will be the lowest non zero.
            return scores[2]; 
        }

        /// <summary>
        /// Return public records based on aBorrowerCreditModeT
        /// </summary>
        public IEnumerable<AbstractCreditPublicRecord> EffectivePublicRecords
        {
            get
            {
                foreach (AbstractCreditPublicRecord o in AllPublicRecords)
                {
                    if (IsPublicRecordExclude(o))
                        continue;
                    yield return o;
                }
            }
        }

        [CreditMigrated]
		public int CreditReportAge 
		{
			get 
			{
				// Return number of days since credit report first reissue.
				return ((TimeSpan) DateTime.Now.Subtract(this.CreditReportFirstIssuedDate)).Days;
			}
		}
		
		#region DEBUG UTILITIES
		private void TagItem(string methodName, AbstractCreditLiability o) 
		{
			// 12/1/2004 dd - This method is only useful for debugging purpose. Therefore in production
			// this method should be NO-OP
			if (!m_isDebug)
				return;

			TagItem(methodName, string.Format(@"[Debug - Liability:ID={0}, AccountIdentifier={1}, CreditorName={2}]", o.ID, o.AccountIdentifier, o.CreditorName));
		}
		private void TagItem(string methodName, AbstractCreditPublicRecord o) 
		{
			// 12/1/2004 dd - This method is only useful for debugging purpose. Therefore in production
			// this method should be NO-OP
			if (!m_isDebug)
				return;

			TagItem(methodName, string.Format(@"[Debug - PublicRecord:ID={0}]", o.ID));
		}
		private void TagItem(string methodName, string desc) 
		{
			// 12/1/2004 dd - This method is only useful for debugging purpose. Therefore in production
			// this method should be NO-OP
			if (!m_isDebug)
				return;

			if (null == m_tagHashTable)
				m_tagHashTable = new Hashtable();

			ArrayList list = (ArrayList) m_tagHashTable[methodName];
			if (null == list) 
			{
				list = new ArrayList();
				m_tagHashTable[methodName] = list;
			}

			// 12/1/2004 dd - List only contains unique data.
			if (!list.Contains(desc))
				list.Add(desc);

		}

		public ArrayList GetTagItems(string methodName) 
		{
			if (null != m_tagHashTable)
				return (ArrayList) m_tagHashTable[methodName];

			return null;
		}

		public abstract string GetLiabilityXml(string id);
		public abstract string GetPublicRecordXml(string id);
		#endregion // DEBUG UTILITIES
        
        #region Caching Methods
		private Hashtable m_cache = new Hashtable(50);
        private string __GenerateCacheKey(string key)
        {
            // 4/3/2009 dd - The cache key will now tie with the borrower credit mode.
            return m_aBorrowerCreditModeT.ToString() + "::" + key;
        }
		private bool HasCache(string key) 
		{
			return m_cache.Contains(__GenerateCacheKey(key));
		}

		private int GetCacheInt(string key) 
		{
			return (int) m_cache[__GenerateCacheKey(key)];
		}

		private decimal GetCacheDecimal(string key) 
		{
			return (decimal) m_cache[__GenerateCacheKey(key)];
		}
		private bool GetCacheBool(string key) 
		{
			return (bool) m_cache[__GenerateCacheKey(key)];
		}

		private void InsertCache(string key, object value) 
		{
			m_cache.Add(__GenerateCacheKey(key), value);
		}

        #endregion

		// -------- Implementation of ICreditReport ------------
        [CreditMigrated]
		virtual public bool IsInCreditCounseling 
		{
			get 
			{
				string methodName="IsInCreditCounseling";
				// OPM 2083 - Loop through each liability and check if there is any liability in Credit Counseling.
				foreach (AbstractCreditLiability o in EffectiveLiabilities) 
				{

					if (o.IsInCreditCounseling) 
					{
						TagItem(methodName, o);
						return true;
					}
				}

				return false;
			}
		}

		#region OLD METHODS

        protected bool IsLiabilityExclude(AbstractCreditLiability o)
        {
            if (o == null)
                return true;

            // 4/6/2009 dd - OPM 25872 - Supporting Unmarried Coborrower.
            // If the borrowerCreditMode is set to Borrower then credit calculation should only include tradeline belongs to Borrower or Joint.
            // If the BorrowerCreditMode is set to Coborrower then credit calculation should only include tradeline belongs to Coborrower Or Joint.
            // If the borrowerCreditMode is set to Both then old behavior maintain. Count all tradelines.
            if (aBorrowerCreditModeT == E_aBorrowerCreditModeT.Both)
            {
                return false;
            }
            else if (o.LiaOwnerT == E_LiaOwnerT.Borrower && aBorrowerCreditModeT == E_aBorrowerCreditModeT.Borrower)
            {
                return false;
            }
            else if (o.LiaOwnerT == E_LiaOwnerT.CoBorrower && aBorrowerCreditModeT == E_aBorrowerCreditModeT.Coborrower)
            {
                return false;
            }
            else if (o.LiaOwnerT == E_LiaOwnerT.Joint)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        protected bool IsPublicRecordExclude(AbstractCreditPublicRecord o)
        {
            if (o == null)
                return true;

            // 4/6/2009 dd - OPM 25872 - Supporting Unmarried Coborrower.
            // If the borrowerCreditMode is set to Borrower then credit calculation should only include tradeline belongs to Borrower or Joint.
            // If the BorrowerCreditMode is set to Coborrower then credit calculation should only include tradeline belongs to Coborrower Or Joint.
            // If the borrowerCreditMode is set to Both then old behavior maintain. Count all tradelines.
            if (aBorrowerCreditModeT == E_aBorrowerCreditModeT.Both)
            {
                return false;
            }
            else if (o.PublicRecordOwnerT == E_PublicRecordOwnerT.Borrower && aBorrowerCreditModeT == E_aBorrowerCreditModeT.Borrower)
            {
                return false;
            }
            else if (o.PublicRecordOwnerT == E_PublicRecordOwnerT.CoBorrower && aBorrowerCreditModeT == E_aBorrowerCreditModeT.Coborrower)
            {
                return false;
            }
            else if (o.PublicRecordOwnerT == E_PublicRecordOwnerT.Joint)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// Returns the number of short sales that occured within n Months.
        /// </summary>
        /// <param name="months">Number of months short sales must be within to be counted.</param>
        /// <returns>The number of short sales that occured within the given number of months.</returns>
        /// <remarks>OPM 24318.</remarks>
        virtual public int GetShortSales(int months)
        {
            string key = "GetShortSales:" + months;
            if (HasCache(key))
                return GetCacheInt(key);

            int count = 0;

            foreach (AbstractCreditLiability o in EffectiveLiabilities)
            {
                if (!o.IsTypeMortgage || !o.IsShortSale)
                {
                    continue;
                }

                DateTime currentDate = DateTime.Today;

                // OPM 24318 - Short sale date is the most recent of last activity date and payment pattern start date. If neither of these
                // dates is valid, then use account reported date.
                DateTime shortSaleDate = o.LastActivityDate > o.PaymentPatternStartDate ? o.LastActivityDate : o.PaymentPatternStartDate;
                if (shortSaleDate == DateTime.MinValue)
                {
                    shortSaleDate = o.AccountReportedDate;
                }

                if (shortSaleDate.Date != DateTime.MinValue)
                {
                    if (Tools.IsDateWithin(shortSaleDate.Date, months, currentDate))
                    {
                        TagItem(key, o);
                        count++;
                    }
                }

            }

            InsertCache(key, count);
            return count;
        }

        [CreditMigrated]
		virtual public int GetMortgageLates(int nMonths)
		{
			string key = "GetMortgageLates:" + nMonths;
			if (HasCache(key))
				return GetCacheInt(key);

			int count = 0;

			foreach (AbstractCreditLiability o in EffectiveLiabilities) 
			{
				if (!o.LiabilityType.EqualsOneOf(E_DebtRegularT.Mortgage, E_DebtRegularT.Heloc))
					continue;

				if (o.LateOnlyCreditAdverseRatingList.Count == 0)
					continue;

				// 8/1/2006 nw - OPM 6683 - MortgageLates keywords should count trades that are in BK status
				// if (o.IsBankruptcy || o.IsForeclosure)
				if (o.IsForeclosure)
					continue;

				DateTime currentDate = DateTime.Today;

				foreach (CreditAdverseRating late in o.LateOnlyCreditAdverseRatingList) 
				{
					if (late.Date != DateTime.MinValue) 
					{
						if (Tools.IsDateWithin(late.Date, nMonths, currentDate)) 
						{
							TagItem(key, o);
							count++;
						}

					}
				}
			}

			InsertCache(key, count);
			return count;

		}

        /// <summary>
        /// Not rolling
        /// </summary>
        [CreditMigrated]
		virtual public int GetMortgage30Lates(int nMonths)
		{
			string key = "GetMortgage30Lates:" + nMonths;
			if (HasCache(key))
				return GetCacheInt(key);

			int count = GetNumberOfLates(nMonths, E_DebtRegularT.Mortgage, E_AdverseType.Late30, key);

			InsertCache(key, count);
			return count;

		}

        [CreditMigrated]
		virtual public int GetMortgage60Lates(int nMonths)
		{
			string key = "GetMortgage60Lates:" + nMonths;
			if (HasCache(key))
				return GetCacheInt(key);


			int count = GetNumberOfLates(nMonths, E_DebtRegularT.Mortgage, E_AdverseType.Late60, key);

			InsertCache(key, count);
			return count;
		}

        [CreditMigrated]
		virtual public int GetMortgage90PlusLates(int nMonths)
		{
			string key = "GetMortgage90PlusLates:" + nMonths;
			if (HasCache(key))
				return GetCacheInt(key);


			int count = GetNumberOfLates(nMonths, E_DebtRegularT.Mortgage, E_AdverseType.Late90Plus, key);

			InsertCache(key, count);
			return count;

		}

        [CreditMigrated]
		virtual public int GetMortgage90Lates(int nMonths)
		{
			string key = "GetMortgage90Lates:" + nMonths;
			if (HasCache(key))
				return GetCacheInt(key);


			int count = GetNumberOfLates(nMonths, E_DebtRegularT.Mortgage, E_AdverseType.Late90, key);

			InsertCache(key, count);
			return count;
		}


		virtual public int GetMortgage120Lates(int nMonths)
		{
			string key = "GetMortgage120Lates:" + nMonths;
			if (HasCache(key))
				return GetCacheInt(key);

			int count = GetNumberOfLates(nMonths, E_DebtRegularT.Mortgage, E_AdverseType.Late120, key);

			InsertCache(key, count);
			return count;
		}

        [CreditMigrated]
		virtual public int GetMortgage150PlusLates(int nMonths)
		{
			string key = "GetMortgage150PlusLates:" + nMonths;
			if (HasCache(key))
				return GetCacheInt(key);

			int count = GetNumberOfLates(nMonths, E_DebtRegularT.Mortgage, E_AdverseType.Late150 | E_AdverseType.Late180, key);

			InsertCache(key, count);
			return count;

		}

        [CreditMigrated]
		public int GetInstallment30Lates(int nMonths)
		{
			string key = "GetInstallment30Lates:" + nMonths;
			if (HasCache(key))
				return GetCacheInt(key);

			int count = GetNumberOfLates(nMonths, E_DebtRegularT.Installment, E_AdverseType.Late30, key);

			InsertCache(key, count);
			return count;

		}

		public int GetInstallment60Lates(int nMonths)
		{
			string key = "GetInstallment60Lates:" + nMonths;
			if (HasCache(key))
				return GetCacheInt(key);

			int count = GetNumberOfLates(nMonths, E_DebtRegularT.Installment, E_AdverseType.Late60, key);

			InsertCache(key, count);
			return count;

		}

        [CreditMigrated]
		public int GetInstallment90PlusLates(int nMonths)
		{
			string key = "GetInstallment90PlusLates:" + nMonths;
			if (HasCache(key))
				return GetCacheInt(key);

			int count = GetNumberOfLates(nMonths, E_DebtRegularT.Installment, E_AdverseType.Late90Plus, key);

			InsertCache(key, count);
			return count;

		}

        [CreditMigrated]
		public int GetRevolving30Lates(int nMonths)
		{
			string key = "GetRevolving30Lates:" + nMonths;
			if (HasCache(key))
				return GetCacheInt(key);

			int count = GetNumberOfLates(nMonths, E_DebtRegularT.Revolving, E_AdverseType.Late30, key);

			InsertCache(key, count);
			return count;
		}

        [CreditMigrated]
		public int GetRevolving60Lates(int nMonths)
		{
			string key = "GetRevolving60Lates:" + nMonths;
			if (HasCache(key))
				return GetCacheInt(key);

			int count = GetNumberOfLates(nMonths, E_DebtRegularT.Revolving, E_AdverseType.Late60, key);

			InsertCache(key, count);
			return count;
		}

        [CreditMigrated]
		public int GetRevolving90PlusLates(int nMonths)
		{
			string key = "GetRevolving90PlusLates:" + nMonths;
			if (HasCache(key))
				return GetCacheInt(key);

			int count = GetNumberOfLates(nMonths, E_DebtRegularT.Revolving, E_AdverseType.Late90Plus, key);

			InsertCache(key, count);
			return count;
		}

        /// <summary>
        /// Get the total number of late payments for the given debt type, with lateType number of days late, in the past n months.
        /// </summary>
        /// <param name="nMonths">The number of months to scan.</param>
        /// <param name="type">The debt type to check (usually Installment, Revolving, or Mortgage).</param>
        /// <param name="lateType">The type of lates to include.</param>
        /// <param name="rolling">Whether to count rolling late payments as single lates or multiple.</param>
        /// <returns>The number of lates found.</returns>
        public int GetNumberOfLates(int nMonths, E_DebtRegularT type, E_AdverseType lateType, bool rolling)
        {
            string adverseTypeString;
            switch (lateType)
            {
                case E_AdverseType.Late30:
                    adverseTypeString = "30";
                    break;
                case E_AdverseType.Late60:
                    adverseTypeString = "60";
                    break;
                case E_AdverseType.Late90:
                    adverseTypeString = "90";
                    break;
                case E_AdverseType.Late90Plus:
                    adverseTypeString = "90Plus";
                    break;
                case E_AdverseType.Late120:
                    adverseTypeString = "120";
                    break;
                case E_AdverseType.Late120Plus:
                    adverseTypeString = "120Plus";
                    break;
                case E_AdverseType.Late150:
                    adverseTypeString = "150";
                    break;
                case E_AdverseType.Late180:
                    adverseTypeString = "180";
                    break;
                case E_AdverseType.AllLate:
                    adverseTypeString = "All";
                    break;
                default:
                    throw new UnhandledEnumException(lateType);
            }

            string key;
            if (rolling)
            {
                key = $"Get{type.ToString("G")}Lates{adverseTypeString}ProgressiveRollingCountWithin:{nMonths}";
            }
            else
            {
                key = $"Get{type.ToString("G")}{adverseTypeString}Lates:{nMonths}";
            }

            if (HasCache(key))
                return GetCacheInt(key);

            int count;
            if (rolling)
            {
                count = GetLatesIntermittentProgressiveCountWithin(nMonths, type, lateType, true, key);
            }
            else
            {
                count = GetNumberOfLates(nMonths, type, lateType, key);
            }

            InsertCache(key, count);
            return count;
        }

		/// <summary>
		/// Calculate late - not rolling.
		/// </summary>
		/// <param name="nMonths"></param>
		/// <param name="type"></param>
		/// <param name="lateType"></param>
		/// <param name="debugMethodName"></param>
		/// <returns></returns>
		private int GetNumberOfLates(int nMonths, E_DebtRegularT type, E_AdverseType lateType, string sCacheKey) 
		{

			int count = 0;
			foreach (AbstractCreditLiability o in EffectiveLiabilities) 
			{
				if (o.LiabilityType != type)
					continue;

				if (o.LateOnlyCreditAdverseRatingList.Count == 0)
					continue;

				// OPM# 3230: MortgageLates should ignore trades that are in BK or Foreclosed status   
				// 8/1/2006 nw - OPM 6683 - MortgageLates keywords should count trades that are in BK status
				// if(type == E_DebtRegularT.Mortgage && (o.IsBankruptcy || o.IsForeclosure))
				if(type.EqualsOneOf(E_DebtRegularT.Mortgage, E_DebtRegularT.Heloc) && o.IsForeclosure)
					continue;

				DateTime currentDate = DateTime.Today;
				foreach (CreditAdverseRating late in o.LateOnlyCreditAdverseRatingList) 
				{

					if ((late.AdverseType & lateType) != 0) 
					{
						if (late.Date != DateTime.MinValue) 
						{
							if (Tools.IsDateWithin(late.Date, nMonths, currentDate)) 
							{
								TagItem(sCacheKey, o);
								count++;
							}
						}
					}

				}
			}

			return count;
		}


		virtual public int GetBankruptcy(int nMonths)
		{
			string key = "GetBankruptcy:" + nMonths;
			if (HasCache(key))
				return GetCacheInt(key);

			int ret = 0;
			// case 2357, since HasDischargedBkWithin is adjusting the look back period by -1, we need to 
			// add 1 to make sure the inclusive look back period is covered. 
			if(HasBkFiledWithin(nMonths) > 0 || HasDischargedBkWithin(nMonths + 1) > 0)
				ret = 1;

			InsertCache(key, ret);
			return ret;
		}

        [CreditMigrated]
		virtual public int GetChargeOff(int nMonths)
		{
			string key = "GetChargeOff:" + nMonths;
			if (HasCache(key))
				return GetCacheInt(key);

			int count = 0;
			DateTime currentDate = DateTime.Today;
			foreach (AbstractCreditLiability o in EffectiveLiabilities) 
			{
				if (o.IsChargeOff) 
				{
					try 
					{
						// The date which the action happen is either report in
						// Last Date Activity field or Date Reported.
						// Last Date Activity is more accurate
						// Last Date Activity has the following format 'yyyymm'
						// If last date activity is unknown then it is 'UUUUUU'
						// Date_reported has the following format 'mm/dd/yy'
						DateTime dt = o.AccountReportedDate;
						if (o.LastActivityDate != DateTime.MinValue) 
						{
							dt = o.LastActivityDate;
						}
						if (Tools.IsDateWithin(dt, nMonths, currentDate)) 
						{
							TagItem(key, o);
							count++;
						}
					} 
					catch 
					{
						// Skip malformat string.
					}

				}
			}

			InsertCache(key, count);
			return count;
		}
        [CreditMigrated]
		virtual public decimal DerogatoryAmountTotal
		{
			get
			{
				string key = "DerogatoryAmountTotal";
				if (HasCache(key))
					return GetCacheDecimal(key);

				decimal value = 0.0M;

				string methodName = "DerogatoryAmountTotal";

				foreach (AbstractCreditLiability o in EffectiveLiabilities) 
				{
					if (o.IsDerogCurrently) 
					{
						TagItem(methodName, o);
						value += o.UnpaidBalanceAmount;
					}
				}

				InsertCache(key, value);
				return value;
			}
		}

		#endregion // OLD METHODS

		#region GENERIC

        [CreditMigrated]
		virtual public int TradeCount 
		{ 
			get
			{ 
				string methodName = "TradeCount";
				if (HasCache(methodName))
					return GetCacheInt(methodName);
                
				int count = 0;

				foreach(AbstractCreditLiability o in EffectiveLiabilities)
				{

					try
					{                  
						if(o.IsBankruptcy)
							continue;

						TagItem(methodName, o);
						++count;
					}
					catch(Exception ex)
					{
						LogCreditWarning(ex);
					}
				}

				InsertCache(methodName, count);
				return count;
			}
		}

		//6/28/07 db OPM 16602
		virtual public decimal MaxHighCreditAmountX(string parameters)
		{
			decimal MaxHighCreditAmountValue = 0;
			EvaluateTradeCountX(parameters, this, ref MaxHighCreditAmountValue, true /*MaxHighCreditAmountMode*/);
			return MaxHighCreditAmountValue;
		}

		static public int EvaluateTradeCountX(string parameters, AbstractCreditReport creditReport)
		{
			decimal MaxHighCreditAmountValue = -1;
			return EvaluateTradeCountX(parameters, creditReport, ref MaxHighCreditAmountValue, false /*MaxHighCreditAmountMode*/);
		}

		virtual public  int TradeCountX(string parameters)
		{
			return EvaluateTradeCountX(parameters, this);
		}

		//opm 8365
		/// <summary>
		/// Evaluates the TradeCountX keyword
		/// </summary>
		/// <param name="parameters"></param>
		/// <param name="creditReport"></param>
		/// <param name="MaxHighCreditAmountValue">Ref parameter that will return the MaxHighCreditAmount of all
		/// tradelines that TradeCountX would return.</param>
		/// <param name="MaxHighCreditAmountMode">Set to true to get the max high credit amount, or false to get 
		/// the regular TradeCountX value</param>
		/// <returns></returns>
		static public  int EvaluateTradeCountX(string parameters, AbstractCreditReport creditReport, ref decimal MaxHighCreditAmountValue, bool MaxHighCreditAmountMode)
		{ 			
			AbstractCreditLiability itemToTag = null; //6/28/07 db OPM 16602
			
			string key;

			if(MaxHighCreditAmountMode == true)
				key = "MaxHighCreditAmountX:" + parameters.ToLower();
			else
				key = "TradeCountX:" + parameters.ToLower();

			MaxHighCreditAmountValue = 0; //6/28/07 db OPM 16602

            bool isCountSubjPropTradelinesOnly = false; // 11/17/2009 dd - OPM 41227 - Default value for isCountSubjPropTradelinesOnly parameter.
            bool isCountSubjProp1stMortgageOnly = false;

			if(creditReport != null && creditReport.HasCache(key))
				return creditReport.GetCacheInt(key);

			// Type: T
			string typeOp = "="; // must be =, we can support != in the future
			string types = "irmoulh"; // 'i', 'r', 'm', 'o', 'u', 'l', 'h' and any combination. 'o': open and  'u': unknown, other.

			string ownershipOp = "=";
			string ownershipVals = "pc"; // 'p', 'c', 'pc'
			
			// Is closed account included: ClosedOk
			string isClosedOkOp = "="; // must be =
			bool isClosedOkVal = true; // y,n

			// How long ago Closed Date was, if account is closed.  Default: count all
			string closedDateOp = "<="; // must be <=
			int closedDateMonths = 1000; // must be >= 0
			
			// How derog the LAST status is:  Status
			string statusOp = "<=";
			string statusVal = "180"; // none,30,60,90,120,150,180,dead. Dead is one of these: fk, bk, repos, charge-off, col
			
			// How long ago Last Activity Date was: LastActivityDate. Default: count all
			string ladOp = "<="; // <=, >.   "<=" means "within", ">" means older than
			int ladMonths = 1000; // must be >= 0

			// How long ago Open Date was: OpenDate. Default: count all
			string odOp = "<="; // <, <=, >, >=. "<=" means "within", ">" means older than
			int odMonths = 1000; // must be >= 0

			// 2/8/2007 nw - OPM 10210
			// How long has this tradeline been open: MonthsOpened. Default: count all
			string monthsOpenedOp = ">="; // <, <=, >, >=
			int monthsOpenedVal = 1; // must be >= 1

			// Months reviewed: Reviewed
			string revOp = ">="; // ">=", ">"
			int revMonths = 0;

			// High credit: HighCredit
			string highCreditOp = ">="; // >, >=
			Int64 highCreditVal = 0; // default to get ALL

			// Paid off prior to BK: PdPriorBkOk
			string pdPriorBkOkOp = "=";
			bool pdPriorBkOkVal = false; // y,n, 
			
			// Ok to count student loans that are not in repayment: StudentLoanNotInRepayOk
			string studentLoanNotInRepayOkOp = "=";
			bool studentLoanNotInRepayOkVal = true; // y, n

			// Ok to count authorized-user accounts: AuthorizedUserOk
			string authorizedUserOkOp = "=";
			bool authorizedUserOkVal = true; // y,n

			// 9/22/2006 nw - External OPM 13872 - Government Miscellaneous Debt tradelines should not be included by default
			// Ok to count Government Miscellaneous Debt: GovMiscDebtOk
			string govMiscDebtOkOp = "=";
			bool govMiscDebtOkVal = false; // y, n

			// 3/21/2007 nw - OPM 11927 - Do not count tradeline with type COLLECTION regardless of status
			// Ok to count Collection tradelines: IncludeCollection
			string IncludeCollectionOp = "=";
			bool IncludeCollectionVal = false; // y, n

			// 4/19/2007 nw - OPM 10552 - New parameters for credit info after latest BK
			string OpenedAfterBkOnlyOp = "=";
			bool OpenedAfterBkOnlyVal = false; // y, n

            // Default this to empty so we do not evaluate this parameter unless 
            // it is explicitly specified. gf opm 273330
            string ReviewedAfterBkOp = ""; // >=, >
            int ReviewedAfterBkMonths = 0; // 0 <= ReviewedAfterBkMonths <= 1000

            string WorstDerogOp = "<=";
			string WorstDerogVal = "dead"; // none, 30, 60, 90, 120, 150, 180, dead.  Dead is one of these: fc, bk, repos, charge-off, col

			string WorstDerogAfterBkOp = "<=";
			string WorstDerogAfterBkVal = "dead"; // none, 30, 60, 90, 120, 150, 180, dead.  Dead is one of these: fc, bk, repos, charge-off, col


			#region PARSING THE 'parameters'
		
			// Supporting operators are : <, <=, >, >=, =
			char[] op_chars = new  char[] { '<', '>', '=' }; // TODO: Consider moving this outside to re-use.

			foreach(string attribute in parameters.ToLower().Split('&'))
			{
				if(attribute == "," || attribute == "")
					continue;

				string name = "";
				string op = "";
				string val = "";
				
				// Extracting name
				// 12/21/2006 nw - OPM 5606 - Handle mistyped parameters/attributes
				int nameLen = attribute.IndexOfAny(op_chars);
				try
				{
					name = attribute.Substring(0, nameLen);
				}
				catch	// must be an invalid format, log bug below and throw exception in the switch statement
				{
					name = attribute;
				}

				// Extracting operator
				StringBuilder opBuilder = new StringBuilder(5);
				Match match = System.Text.RegularExpressions.Regex.Match(attribute, "[<>=]");
				while(match.Success)
				{
					string token = match.Groups[0].Value;
					opBuilder.Append(token);
					match = match.NextMatch();
				}
				op = opBuilder.ToString();

				// Extracting value
				try
				{
					val = attribute.Substring(nameLen + op.Length);
				}
				catch{} // must be an invalid format 

				if(name == "" || op == "" || val == "")
					Tools.LogBug(string.Format("None of these can be empty. Name={0}. Op={1}. Val={2}", name, op, val));

				switch(name)
				{
					case "t":
						if(op != "=")
							throw CreateGenericExceptionWithDevMessage(string.Format("{0} is not a supported operator for parameter T in keyword TradeCountX", op));
						typeOp = op;
			
						int len = val.Length;
						for(int i = 0; i < len; ++i)
						{
							switch(val[i])
							{
								case 'i':
								case 'r':
								case 'm':
								case 'o':
								case 'u':
                                case 'l':
                                case 'h':
									break;
								default:
									throw CreateGenericExceptionWithDevMessage(string.Format("{0} character is not a supported argument for parameter T in keyword TradeCountX", val[i].ToString()));
							}
						}
						types = val;
						break;
					case "ownership":
						if(op != "=")
							throw CreateGenericExceptionWithDevMessage(string.Format("{0} is not a supported operator for parameter Ownership in keyword TradeCountX", op));
						ownershipOp = op;
						switch(val)
						{
							case "p":
							case "c":
							case "pc":
							case "cp":
								break;
							default:
								throw CreateGenericExceptionWithDevMessage(string.Format("{0} character is not a supported argument for parameter Ownership in keyword TradeCountX", val));
						}
						ownershipVals = val;
						break;
					case "closedok":
						if(op != "=")
							throw CreateGenericExceptionWithDevMessage(string.Format("{0} is not a supported operator for parameter ClosedOk in keyword TradeCountX", op));
						isClosedOkOp = op;
					switch(val)
					{
						case "y": isClosedOkVal = true; break;
						case "n": isClosedOkVal = false; break;
						default:
							throw CreateGenericExceptionWithDevMessage(string.Format("{0} is not a supported argument for parameter ClosedOk in keyword TradeCountX ", val));
					}
						break;
						// 6/28/2006 - nw - OPM 5667
					case "closeddate":
						if (op != "<=")
							throw CreateGenericExceptionWithDevMessage(string.Format("{0} is not a supported operator for parameter ClosedDate in keyword TradeCountX", op));
						closedDateOp = op;
						try
						{
							closedDateMonths = int.Parse(val);
							if (closedDateMonths < 0)
								throw CreateGenericExceptionWithDevMessage("Closed date months (closedDateMonths) was < 0.");
						}
						catch
						{
							throw CreateGenericExceptionWithDevMessage(string.Format("{0} is not a supported argument for parameter ClosedDate in keyword TradeCountX", val));
						}
						break;
					case "status":
						if(op != "<=")
							throw CreateGenericExceptionWithDevMessage(string.Format("{0} is not a supported operator for parameter Status in keyword TradeCountX", op));
						statusOp = op;

					switch(val)
					{
						case "none":
						case "30":
						case "60":
						case "90":
						case "120":
						case "150":
						case "180":
						case "dead":
							statusVal = val;
							break;
						default:
							throw CreateGenericExceptionWithDevMessage(string.Format("{0} is not a supported argument for parameter Status in keyword TradeCountX", val));
					}
						break;
					case "lastactivitydate":
					switch(op)
					{
						case "<=":
						case ">":
							ladOp = op;
							break;
						default:
							throw CreateGenericExceptionWithDevMessage(string.Format("{0} is not a supported operator  for parameter LastActivityDate in keyword TradeCountX", op));
					}

						try
						{
							ladMonths = int.Parse(val);
							if(ladMonths < 0)
								throw CreateGenericExceptionWithDevMessage("Last activity date months (ladMonths) was < 0.");
						}
						catch
						{
							throw CreateGenericExceptionWithDevMessage(string.Format("{0} is not a supported argument for parameter LastActivityDate in keyword TradeCountX", val));
						}
						break;
					case "opendate":
					switch(op)
					{
						case "<":
						case "<=":
						case ">":
						case ">=":
							odOp = op;
							break;
						default:
							throw CreateGenericExceptionWithDevMessage(string.Format("{0} is not a supported operator for parameter OpenDate in keyword TradeCountX", op));
					}
						
						try
						{
							odMonths = int.Parse(val);
							if(odMonths < 0)
								throw CreateGenericExceptionWithDevMessage("Open date months (odMonths) was < 0.");
						}
						catch
						{
							throw CreateGenericExceptionWithDevMessage(string.Format("{0} is not a supported argument for parameter OpenDate in keyword TradeCountX", val));
						}
						break;
					case "monthsopened":
					switch(op)
					{
						case "<":
						case "<=":
						case ">":
						case ">=":
							monthsOpenedOp = op;
							break;
						default:
							throw CreateGenericExceptionWithDevMessage(string.Format("{0} is not a supported operator for parameter MonthsOpened in keyword TradeCountX", op));
					}
						
						try
						{
							monthsOpenedVal = int.Parse(val);
							if(monthsOpenedVal < 1 || monthsOpenedVal > 1000)
								throw CreateGenericExceptionWithDevMessage("Months Opened value (monthsOpenedVal) was < 1 or > 1000.");
						}
						catch
						{
							throw CreateGenericExceptionWithDevMessage(string.Format("{0} is not a supported argument for parameter MonthsOpened in keyword TradeCountX", val));
						}
						break;
					case "reviewed":
					switch(op)
					{
						case ">=":
						case ">":
							revOp = op;
							break;
						default:
							throw CreateGenericExceptionWithDevMessage(string.Format("{0} is not a supported operator for parameter Reviewed in keyword TradeCountX", op));
					}
						
						try
						{
							revMonths = int.Parse(val);
							if(revMonths < 0)
								throw CreateGenericExceptionWithDevMessage("Months reviewed (revMonths) was < 0.");
						}
						catch
						{
							throw CreateGenericExceptionWithDevMessage(string.Format("{0} is not a supported argument for parameter Reviewed in keyword TradeCountX", val));
						}
						break;
					case "highcredit":
					switch(op)
					{
						case ">=":
						case ">":
							highCreditOp = op;
							break;
						default:
							throw CreateGenericExceptionWithDevMessage(string.Format("{0} is not a supported operator for parameter HighCredit in keyword TradeCountX", op));
					}
						
						try
						{
							highCreditVal = Int64.Parse(val);
							if(highCreditVal < 0)
								throw CreateGenericExceptionWithDevMessage("High credit (highCreditVal) was < 0.");
						}
						catch
						{
							throw CreateGenericExceptionWithDevMessage(string.Format("{0} is not a supported argument for parameter HighCredit in keyword TradeCountX", val));
						}
						break;
					case "pdpriorbkok":
						if(op != "=")
							throw CreateGenericExceptionWithDevMessage(string.Format("{0} is not a supported operator for parameter PdPriorBkOk in keyword TradeCountX", op));						
						pdPriorBkOkOp = op;

					switch(val)
					{
						case "y": pdPriorBkOkVal = true; break;
						case "n": pdPriorBkOkVal = false; break;
						default: throw CreateGenericExceptionWithDevMessage(string.Format("{0} is not a supported argument for parameter PdPriorBkOk in keyword TradeCountX", val));
					}
						break;
					case "studentloannotinrepayok":
						if(op != "=")
							throw CreateGenericExceptionWithDevMessage(string.Format("{0} is not a supported operator for parameter StudentLoanNotInRepayOk in keyword TradeCountX", op));						
						studentLoanNotInRepayOkOp = op;

					switch(val)
					{
						case "y": studentLoanNotInRepayOkVal = true; break;
						case "n": studentLoanNotInRepayOkVal = false; break;
						default: throw CreateGenericExceptionWithDevMessage(string.Format("{0} is not a supported argument for parameter StudentLoanNotInRepayOk in keyword TradeCountX", val));
					}
						break;
					case "authorizeduserok":
						if(op != "=")
							throw CreateGenericExceptionWithDevMessage(string.Format("{0} is not a supported operator for parameter AuthorizedStudentOk in keyword TradeCountX", op));						
						authorizedUserOkOp = op;

					switch(val)
					{
						case "y": authorizedUserOkVal = true; break;
						case "n": authorizedUserOkVal = false; break;
						default: throw CreateGenericExceptionWithDevMessage(string.Format("{0} is not a supported argument for parameter AuthorizedStudentOk in keyword TradeCountX", val));
					}
						break;
					case "govmiscdebtok":
						if(op != "=")
							throw CreateGenericExceptionWithDevMessage(string.Format("{0} is not a supported operator for parameter GovMiscDebtOk in keyword TradeCountX", op));						
						govMiscDebtOkOp = op;

					switch(val)
					{
						case "y": govMiscDebtOkVal = true; break;
						case "n": govMiscDebtOkVal = false; break;
						default: throw CreateGenericExceptionWithDevMessage(string.Format("{0} is not a supported argument for parameter GovMiscDebtOk in keyword TradeCountX", val));
					}
						break;
					case "includecollection":
						if(op != "=")
							throw CreateGenericExceptionWithDevMessage(string.Format("{0} is not a supported operator for parameter IncludeCollection in keyword TradeCountX", op));						
						IncludeCollectionOp = op;

					switch(val)
					{
						case "y": IncludeCollectionVal = true; break;
						case "n": IncludeCollectionVal = false; break;
						default: throw CreateGenericExceptionWithDevMessage(string.Format("{0} is not a supported argument for parameter IncludeCollection in keyword TradeCountX", val));
					}
						break;
					case "openedafterbkonly":
						if(op != "=")
							throw CreateGenericExceptionWithDevMessage(string.Format("{0} is not a supported operator for parameter OpenedAfterBkOnly in keyword TradeCountX", op));						
						OpenedAfterBkOnlyOp = op;

					switch(val)
					{
						case "y": OpenedAfterBkOnlyVal = true; break;
						case "n": OpenedAfterBkOnlyVal = false; break;
						default: throw CreateGenericExceptionWithDevMessage(string.Format("{0} is not a supported argument for parameter OpenedAfterBkOnly in keyword TradeCountX", val));
					}
						break;
					case "reviewedafterbk":
					switch(op)
					{
						case ">=":
						case ">":
							ReviewedAfterBkOp = op;
							break;
						default:
							throw CreateGenericExceptionWithDevMessage(string.Format("{0} is not a supported operator for parameter ReviewedAfterBk in keyword TradeCountX", op));
					}
						
						try
						{
							ReviewedAfterBkMonths = int.Parse(val);
							if(ReviewedAfterBkMonths < 0 || ReviewedAfterBkMonths > 1000)
								throw CreateGenericExceptionWithDevMessage("Months reviewed after BK (ReviewedAfterBkMonths) value is invalid.");
						}
						catch
						{
							throw CreateGenericExceptionWithDevMessage(string.Format("{0} is not a supported argument for parameter ReviewedAfterBk in keyword TradeCountX", val));
						}
						break;
					case "worstderog":
						if(op != "<=")
							throw CreateGenericExceptionWithDevMessage(string.Format("{0} is not a supported operator for parameter WorstDerog in keyword TradeCountX", op));
						WorstDerogOp = op;

					switch(val)
					{
						case "none":
						case "30":
						case "60":
						case "90":
						case "120":
						case "150":
						case "180":
						case "dead":
							WorstDerogVal = val;
							break;
						default:
							throw CreateGenericExceptionWithDevMessage(string.Format("{0} is not a supported argument for parameter WorstDerog in keyword TradeCountX", val));
					}
						break;
					case "worstderogafterbk":
						if(op != "<=")
							throw CreateGenericExceptionWithDevMessage(string.Format("{0} is not a supported operator for parameter WorstDerogAfterBk in keyword TradeCountX", op));
						WorstDerogAfterBkOp = op;

					switch(val)
					{
						case "none":
						case "30":
						case "60":
						case "90":
						case "120":
						case "150":
						case "180":
						case "dead":
							WorstDerogAfterBkVal = val;
							break;
						default:
							throw CreateGenericExceptionWithDevMessage(string.Format("{0} is not a supported argument for parameter WorstDerogAfterBk in keyword TradeCountX", val));
					}
						break;
                    case "countsubjproptradelinesonly":
                        if (op != "=")
                            throw CreateGenericExceptionWithDevMessage(string.Format("{0} is not a supported operator for parameter CountSubjPropTradelinesOnly in keyword TradeCountX", op));

                        switch (val)
                        {
                            case "y": isCountSubjPropTradelinesOnly = true; break;
                            case "n": isCountSubjPropTradelinesOnly = false; break;
                            default: throw CreateGenericExceptionWithDevMessage(string.Format("{0} is not a supported argument for parameter CountSubjPropTradelinesOnly in keyword TradeCountX", val));
                        }
                        break;
                    case "countsubjprop1stmortgageonly":
                        if (op != "=")
                            throw CreateGenericExceptionWithDevMessage(string.Format("{0} is not a supported operator for parameter CountSubjProp1stMortgageOnly in keyword TradeCountX", op));

                        switch (val)
                        {
                            case "y": isCountSubjProp1stMortgageOnly = true; break;
                            case "n": isCountSubjProp1stMortgageOnly = false; break;
                            default: throw CreateGenericExceptionWithDevMessage(string.Format("{0} is not a supported argument for parameter CountSubjProp1stMortgageOnly in keyword TradeCountX", val));
                        }
                        break;

					default:
						string s = string.Format("Error encountered in TradeCountX keyword, parameter name:{0} is not valid", name);
						Tools.LogError(s);
						throw CreateGenericExceptionWithDevMessage(s);					
				}
			}

			#endregion // PARSING THE 'parameters'

			if(creditReport == null)
				return 0;

			DateTime bkDateLatest = DateTime.MinValue;
			
			#region BK Date Info
			foreach (AbstractCreditPublicRecord o in creditReport.EffectivePublicRecords)
			{
				if(!o.IsBkOfTypeAndCharacter(E_BkType.Any, E_BkChar.Any))
					continue;

				DateTime bkDate = DateTime.MinValue;

				if(o.DispositionDate != DateTime.MinValue)
					bkDate = o.DispositionDate;
					// 11/29/2006 nw - External OPM 17907 - using Settled Date instead of Disposition Date
				else if(o.LastEffectiveDate != DateTime.MinValue && o.LastEffectiveDate != DateTime.Today)
					bkDate = o.LastEffectiveDate;
				else if(o.BkFileDate != DateTime.MinValue)
					bkDate = o.BkFileDate;

				if(bkDate != DateTime.MinValue && bkDateLatest.CompareTo(bkDate) < 0)
				{
					bkDateLatest = bkDate;
				}
			}
			#endregion // BK Date Info
			
			int count = 0; 

			foreach(AbstractCreditLiability o in creditReport.EffectiveLiabilities)
			{
				try
				{
					#region parameter:T
					if(!o.IsOfType(types))
						continue;

					#endregion // paremeter: T

                    #region parameter: CountSubjPropTradelinesOnly 
                    if (isCountSubjPropTradelinesOnly)
                    {
                        if (!o.IsSubjectPropertyMortgage)
                        {
                            continue; // skip.
                        }
                    }
                    #endregion

                    #region parameter: CountSubjProp1stMortgageOnly
                    if (isCountSubjProp1stMortgageOnly)
                    {
                        if (!o.IsSubjectProperty1stMortgage)
                        {
                            continue; // skip.
                        }
                    }
                    #endregion

                    #region parameter: Ownership
                    switch (ownershipVals)
					{
						case "pc":
							break;
						case "cp":
							break;
						case "p":
							if(creditReport.SwapPrimaryBorrower)
							{
								if(o.LiaOwnerT == E_LiaOwnerT.Borrower)
									continue;
							}
							else
							{
								if(o.LiaOwnerT == E_LiaOwnerT.CoBorrower)
									continue;
							}
							break;
						case "c":
							if(creditReport.SwapPrimaryBorrower)
							{
								if(o.LiaOwnerT == E_LiaOwnerT.CoBorrower)
									continue;
							}
							else
							{
								if(o.LiaOwnerT == E_LiaOwnerT.Borrower)
									continue;
							}
							break;
						default:
							throw CreateGenericExceptionWithDevMessage("bug in TradeCountX. Validating code of Ownership parameter is not accurate");	
					}

					#endregion

					#region parameter:ClosedOk
					
					if(!isClosedOkVal && !o.IsActive)
						continue;												
					
					#endregion	// parameter:ClosedOk									

					// 6/28/2006 - nw - OPM 5667
					#region parameter:ClosedDate

					if (!o.IsActive)
					{
						DateTime dtClosedDate = (o.LastActivityDate > o.PaymentPatternStartDate) ? o.LastActivityDate : o.PaymentPatternStartDate;
						if (dtClosedDate == DateTime.MinValue)
							dtClosedDate = o.AccountReportedDate;
						if (!Tools.IsDateWithin(dtClosedDate, closedDateMonths))
							continue;
					}

					#endregion // parameter:ClosedDate

					#region parameter:Status
					
					// Only operator we support now is "<="
					switch(statusVal)
					{
						case "none": 	// No late or derog is allowed
							if(o.IsLastStatusDerog)
								continue;
							break;
							// 9/7/2006 nw - OPM 7303 - "Status" should only look at the LAST status, ignore past lates
						case "30":
							try
							{
								if (o.IsLastStatusDerog)
								{
									if (o.HasTerminalDerog)
										continue;
								
									// since tradeline is currently late, Credit Adverse Rating List's 0th element will tell us how late
									CreditAdverseRating rating = (CreditAdverseRating)o.CreditAdverseRatingList[0];
									if (rating.AdverseType > E_AdverseType.Late30)
										continue;
								}
							}
							catch{}
							break;
						case "60":
							try
							{
								if (o.IsLastStatusDerog)
								{
									if (o.HasTerminalDerog)
										continue;
								
									CreditAdverseRating rating = (CreditAdverseRating)o.CreditAdverseRatingList[0];
									if (rating.AdverseType > E_AdverseType.Late60)
										continue;
								}
							}
							catch{}
							break;
						case "90":
							try
							{
								if (o.IsLastStatusDerog)
								{
									if (o.HasTerminalDerog)
										continue;
								
									CreditAdverseRating rating = (CreditAdverseRating)o.CreditAdverseRatingList[0];
									if (rating.AdverseType > E_AdverseType.Late90)
										continue;
								}
							}
							catch{}
							break;
						case "120":
							try
							{
								if (o.IsLastStatusDerog)
								{
									if (o.HasTerminalDerog)
										continue;
								
									CreditAdverseRating rating = (CreditAdverseRating)o.CreditAdverseRatingList[0];
									if (rating.AdverseType > E_AdverseType.Late120)
										continue;
								}
							}
							catch{}
							break;
						case "150":
							try
							{
								if (o.IsLastStatusDerog)
								{
									if (o.HasTerminalDerog)
										continue;
								
									CreditAdverseRating rating = (CreditAdverseRating)o.CreditAdverseRatingList[0];
									if (rating.AdverseType > E_AdverseType.Late150)
										continue;
								}
							}
							catch{}
							break;
						case "180":
							try
							{
								if (o.IsLastStatusDerog)
								{
									if (o.HasTerminalDerog)
										continue;
								
									CreditAdverseRating rating = (CreditAdverseRating)o.CreditAdverseRatingList[0];
									if (rating.AdverseType > E_AdverseType.Late180)
										continue;
								}
							}
							catch{}
							break;
						case "dead": // status <= dead will include everything
							break;
						default:
							throw CreateGenericExceptionWithDevMessage("bug in TradeCountX. Validating code of Status parameter is not accurate");	
					}
									
					#endregion // parameter:Status

					#region parameter:LastActivityDate
					
					DateTime lad = o.LastActivityDate;
					if(lad == DateTime.MinValue)
						lad = o.AccountReportedDate;
					if(lad == DateTime.MinValue)
						lad = o.PaymentPatternStartDate;

					if(ladOp == "<=")
					{
						if(!Tools.IsDateWithin(lad, ladMonths))
							continue;
					}
					else if(ladOp == ">")
					{
						if(Tools.IsDateWithin(lad, ladMonths))
							continue;
					}			
					
					#endregion // parameter:LastActivityDate

					#region parameter:OpenDate

					// 3/15/2007 nw - OPM 11704 - blank liability has opened date of 1/1/1
					if (o.AccountOpenedDate == DateTime.MinValue)
						continue;

					// 7/5/2006 nw - OPM 4932 - if tradeline is closed, restrict range from Open to Close Date, not Open to Current Date
					// 2/8/2007 nw - OPM 10210 - Undo the changes made in OPM 4932, it will now be handled by new parameter MonthsOpened
					
					switch(odOp) 
					{
						case "<":
							if(!Tools.IsDateWithin(o.AccountOpenedDate, odMonths - 1))
								continue;
							break;
						case "<=":
							if(!Tools.IsDateWithin(o.AccountOpenedDate, odMonths))
								continue;
							break;
						case ">":
							if(Tools.IsDateWithin(o.AccountOpenedDate, odMonths))
								continue;
							break;
						case ">=":
							// Note that set of xyz-dates qualified for xyzDate>=0 equals to the set of dates qualified for xyzDate>=1 plus today date.  
							// �xyz� can be Open, Last Activity etc�a minor issue because the margin of error (can be debated) is one day not days or 
							// months.
							if(Tools.IsDateWithin(o.AccountOpenedDate, odMonths - 1))
								continue;
							break;
						default:
							throw CreateGenericExceptionWithDevMessage(string.Format("{0} is bad operator for OpenDate parameter in TradeCountX keyword", odOp));
					}

					#endregion // parameter:OpenDate

					#region parameter:MonthsOpened

					// if tradeline is closed, restrict range from Open to Close Date, 
					// if tradeline is open, use Open to Current Date
					DateTime dtEndDate;
					if (o.IsActive)
						dtEndDate = DateTime.Now;
					else
					{
						dtEndDate = (o.LastActivityDate > o.PaymentPatternStartDate) ? o.LastActivityDate : o.PaymentPatternStartDate;
						if (dtEndDate == DateTime.MinValue)
							dtEndDate = o.AccountReportedDate;
					}

					switch(monthsOpenedOp) 
					{
						case "<":
							if(!Tools.IsDateWithin(o.AccountOpenedDate, monthsOpenedVal - 1, dtEndDate))
								continue;
							break;
						case "<=":
							if(!Tools.IsDateWithin(o.AccountOpenedDate, monthsOpenedVal, dtEndDate))
								continue;
							break;
						case ">":
							if(Tools.IsDateWithin(o.AccountOpenedDate, monthsOpenedVal, dtEndDate))
								continue;
							break;
						case ">=":
							// Note that set of xyz-dates qualified for xyzDate>=0 equals to the set of dates qualified for xyzDate>=1 plus today date.  
							// �xyz� can be Open, Last Activity etc�a minor issue because the margin of error (can be debated) is one day not days or 
							// months.
							if(Tools.IsDateWithin(o.AccountOpenedDate, monthsOpenedVal - 1, dtEndDate))
								continue;
							break;
						default:
							throw CreateGenericExceptionWithDevMessage(string.Format("{0} is bad operator for MonthsOpened parameter in TradeCountX keyword", monthsOpenedOp));
					}

					#endregion // parameter:MonthsOpened
					

					#region parameter:Reviewed
					switch(revOp)
					{
						case ">=":
							if(o.MonthsReviewedCount < revMonths)
								continue;
							break;
						case ">":
							if(o.MonthsReviewedCount <= revMonths)
								continue;
							break;
						default:
							throw CreateGenericExceptionWithDevMessage(string.Format("{0} is invalid operator for parameter Reviewed of keyword TradeCountX", revOp));                       
					}
					#endregion // parameter:Reviewed

					#region parameter:HighCredit
					switch(highCreditOp)
					{
						case ">=":
							if(o.HighCreditAmount < highCreditVal)
								continue;
							break;
						case ">":
							if(o.HighCreditAmount <= highCreditVal)
								continue;
							break;
						default:
							throw CreateGenericExceptionWithDevMessage(string.Format("{0} is not valid operator for the parameter HighCredit in keyword TradeCountX", highCreditOp));   
					}
					#endregion // parameter:HighCredit

					#region parameter:PdPriorBkOk
					if(!pdPriorBkOkVal && bkDateLatest != DateTime.MinValue)
					{
						// We don't have a good way to tell when a debt is paid, so we assume
						// that LastActivityDate of paid account would be the closest to what we want.
						// 12/19/2006 nw - OPM 8534 - Open tradeline with DLA prior to latest BK should also be excluded
						// if(o.IsPaid &&lad.CompareTo(bkDateLatest) < 0)
                        if (lad.CompareTo(bkDateLatest) <= 0)
							continue;
					}
					#endregion // parameter:PdPriorBkOk

					#region parameter:StudentLoanNotInRepayOk
					if(!studentLoanNotInRepayOkVal)
					{
						if(o.IsStudentLoansNotInRepayment)
							continue;
					}
					#endregion // parameter:StudentLoanNotInRepayOk

					#region parameter:AuthorizedUserOk
					if(!authorizedUserOkVal)
					{
						if(o.IsAuthorizedUser)
							continue;
					}
					#endregion // parameter:AuthorizedUserOk

					#region parameter:GovMiscDebtOk
					if(!govMiscDebtOkVal)
					{
						if(o.IsGovMiscDebt)
							continue;
					}
					#endregion // parameter:GovMiscDebtOk

					#region parameter:IncludeCollection
					if (!IncludeCollectionVal)
					{
						// can't use HasMajorDerogEffectiveWithin since it ignores tradeline whose current status doesn't match the derog type
						// if (o.HasMajorDerogEffectiveWithin(new E_DerogT[] { E_DerogT.Collection }, 30000, /* bInclPaidAccs */ true))
						if (o.IsFirstPmtTypeWithin(E_AdverseType.ChargeOffOrCollection, 30000))
							continue;
					}
					#endregion // parameter:IncludeCollection

					#region parameter:OpenedAfterBkOnly
					if (OpenedAfterBkOnlyVal && bkDateLatest != DateTime.MinValue)
					{
						if (o.AccountOpenedDate.CompareTo(bkDateLatest) < 0)
							continue;
					}
					#endregion // parameter:OpenedAfterBkOnly

					#region parameter:ReviewedAfterBk
					if (bkDateLatest != DateTime.MinValue && !string.IsNullOrEmpty(ReviewedAfterBkOp))
					{
						/*
						This parameter will return tradelines that have reported to the bureaus 
						for >= nMonths AFTER the latest bankruptcy date.
						-TradeCountX:Reviewed>=nMonths >= TradeCountX:ReviewedAfterBk>=nMonths
						-This parameter must look at the payment history after the latest Bk.  
						If there is no payment history, define it as the months opened since Bk 
						(i.e. date of last Bk to date last active , if no active date given, then 
						use date last reported)
						*/
						DateTime reviewEndDate = (o.LastActivityDate > o.PaymentPatternStartDate) ? o.LastActivityDate : o.PaymentPatternStartDate;
						if (reviewEndDate == DateTime.MinValue)
							reviewEndDate = o.AccountReportedDate;

                        bool openedAfterBankruptcy = o.AccountOpenedDate.CompareTo(bkDateLatest) > 0;

						switch (ReviewedAfterBkOp)
						{
							case ">=":
                                if (openedAfterBankruptcy)
                                {
                                    if (o.MonthsReviewedCount < ReviewedAfterBkMonths)
                                    {
                                        continue;
                                    }
                                }
                                else if (reviewEndDate.CompareTo(bkDateLatest.AddMonths(ReviewedAfterBkMonths)) < 0)
									continue;
								break;
							case ">":
                                if (openedAfterBankruptcy)
                                {
                                    if (o.MonthsReviewedCount <= ReviewedAfterBkMonths)
                                    {
                                        continue;
                                    }
                                }
								else if (reviewEndDate.CompareTo(bkDateLatest.AddMonths(ReviewedAfterBkMonths)) <= 0)
									continue;
								break;
							default:
								string s = string.Format("ReviewedAfterBkOp {0} is not supported in TradeCountX", ReviewedAfterBkOp);
								Tools.LogError(s);
								throw CreateGenericExceptionWithDevMessage(s);
						}
					}
					#endregion // parameter:ReviewedAfterBk

					#region parameter:WorstDerog and WorstDerogAfterBk
					bool bDoWorstDerog = WorstDerogVal != "dead";
					bool bDoWorstDerogAfterBk = WorstDerogAfterBkVal != "dead";
					if ((bDoWorstDerog || bDoWorstDerogAfterBk) && o.CreditAdverseRatingList.Count != 0)
					{
						bool bDisqualified = false;
						foreach (CreditAdverseRating rating in o.CreditAdverseRatingList)
						{
							if (bDoWorstDerog)
							{
								switch (WorstDerogVal)
								{
									case "none":
										bDisqualified = true;
										break;
									case "30":
										if (rating.AdverseType > E_AdverseType.Late30)
											bDisqualified = true;
										break;
									case "60":
										if (rating.AdverseType > E_AdverseType.Late60)
											bDisqualified = true;
										break;
									case "90":
										if (rating.AdverseType > E_AdverseType.Late90)
											bDisqualified = true;
										break;
									case "120":
										if (rating.AdverseType > E_AdverseType.Late120)
											bDisqualified = true;
										break;
									case "150":
										if (rating.AdverseType > E_AdverseType.Late150)
											bDisqualified = true;
										break;
									case "180":
										if (rating.AdverseType > E_AdverseType.Late180)
											bDisqualified = true;
										break;
									case "dead":
										break;
									default:
										string s = string.Format("WorstDerogVal {0} is not supported in TradeCountX", WorstDerogVal);
										Tools.LogError(s);
										throw CreateGenericExceptionWithDevMessage(s);
								}

								if (bDisqualified)
									break;
							}

							if (bDoWorstDerogAfterBk && (rating.Date.CompareTo(bkDateLatest) > 0))
							{
								switch (WorstDerogAfterBkVal)
								{
									case "none":
										bDisqualified = true;
										break;
									case "30":
										if (rating.AdverseType > E_AdverseType.Late30)
											bDisqualified = true;
										break;
									case "60":
										if (rating.AdverseType > E_AdverseType.Late60)
											bDisqualified = true;
										break;
									case "90":
										if (rating.AdverseType > E_AdverseType.Late90)
											bDisqualified = true;
										break;
									case "120":
										if (rating.AdverseType > E_AdverseType.Late120)
											bDisqualified = true;
										break;
									case "150":
										if (rating.AdverseType > E_AdverseType.Late150)
											bDisqualified = true;
										break;
									case "180":
										if (rating.AdverseType > E_AdverseType.Late180)
											bDisqualified = true;
										break;
									case "dead":
										break;
									default:
										string s = string.Format("WorstDerogAfterBkVal {0} is not supported in TradeCountX", WorstDerogAfterBkVal);
										Tools.LogError(s);
										throw CreateGenericExceptionWithDevMessage(s);
								}

								if (bDisqualified)
									break;
							}
						}

						if (bDisqualified)
							continue;
					}
					#endregion // parameter:WorstDerog and WorstDerogAfterBk

					if(MaxHighCreditAmountMode)
					{
						if(o.HighCreditAmount > MaxHighCreditAmountValue)
						{
							MaxHighCreditAmountValue = o.HighCreditAmount;
							itemToTag = o;
						}
					}
					else //regular TradeCountX mode
					{
						creditReport.TagItem(key, o);
						++count;
					}
				}
				catch(Exception ex)
				{
					creditReport.LogCreditWarning(ex);
				}
			}
			//6/28/07 db OPM 16602
			if(MaxHighCreditAmountMode)
			{
				if(itemToTag != null)
					creditReport.TagItem(key, itemToTag);

				//If itemToTag was null, MaxHighCreditAmountValue will be 0
				creditReport.InsertCache(key, MaxHighCreditAmountValue);
			}
			else
				creditReport.InsertCache(key, count);
			return count;
		}
      
        [CreditMigrated]
		virtual public int GetTradesCountOlderThan(int nMonths)
		{
			string key = "GetTradesCountOlderThan:" + nMonths;

			if (HasCache(key))
				return GetCacheInt(key);

			DateTime nowD = DateTime.Today;
			int count = 0;
			
			foreach(AbstractCreditLiability o in EffectiveLiabilities) 
			{
				try
				{
					if(o.IsCredit)
					{
						if (o.AccountOpenedDate != DateTime.MinValue) 
						{
							// Opm# 3236
							if(o.IsBankruptcy || o.IsForeclosure || o.IsRepos || o.IsChargeOff || o.IsCollection)
								continue;

							if(!Tools.IsDateWithin(o.AccountOpenedDate, nMonths, nowD))
							{
								TagItem(key, o);
								++count;
							}
						}
					}
				}
				catch(Exception ex)
				{
					LogCreditWarning(ex);
				}
			}

			InsertCache(key, count);
			return count;
		}

        [CreditMigrated]
		virtual public int InstallTradeCountOlderThan(int nMonths)
		{
			string key = "InstallTradeCountOlderThan:" + nMonths;

			if (HasCache(key))
				return GetCacheInt(key);

			DateTime nowD = DateTime.Today;
			int count = 0;
			
			foreach(AbstractCreditLiability o in EffectiveLiabilities) 
			{
				try
				{
					if(o.IsCredit && o.IsTypeInstallmentBase)
					{
						if (o.AccountOpenedDate != DateTime.MinValue) 
						{
							if(!o.IsBankruptcy && !Tools.IsDateWithin(o.AccountOpenedDate, nMonths, nowD))
							{
								TagItem(key, o);
								++count;
							}
						}
					}
				}
				catch(Exception ex)
				{
					LogCreditWarning(ex);
				}
			}

			InsertCache(key, count);
			return count;
		}

        [CreditMigrated]
		virtual public int RevolveTradeCountOlderThan(int nMonths)
		{
			string key = "RevolveTradeCountOlderThan:" + nMonths;

			if (HasCache(key))
				return GetCacheInt(key);

			DateTime nowD = DateTime.Today;
			int count = 0;
			
			foreach(AbstractCreditLiability o in EffectiveLiabilities) 
			{
				try
				{
					if(o.IsCredit && (o.IsTypeRevolvingBase || o.IsTypeLease))
					{
						if (o.AccountOpenedDate != DateTime.MinValue) 
						{
							if(!o.IsBankruptcy && !Tools.IsDateWithin(o.AccountOpenedDate, nMonths, nowD))
							{
								TagItem(key, o);
								++count;
							}
						}
					}
				}
				catch(Exception ex)
				{
					LogCreditWarning(ex);
				}
			}

			InsertCache(key, count);
			return count;
		}

        [CreditMigrated]
		public bool Is1stTimeHomeBuyer
		{ 
			get
			{
				string methodName = "Is1stTimeHomeBuyer";
				
				if (HasCache(methodName))
					return GetCacheBool(methodName);

				bool ret = Is1stTimeHomeBuyerWithin(12);

				InsertCache(methodName, ret);
				return ret;
			}
		}
        [CreditMigrated]
		public bool Is1stTimeHomeBuyerWithin(int nMonths)
		{ 
			string key = "Is1stTimeHomeBuyerWithin:" + nMonths;

			if (HasCache(key))
				return GetCacheBool(key);

			bool ret = true;

			foreach(AbstractCreditLiability o in EffectiveLiabilities)
			{
				try
				{
					if(o.IsTypeMortgage && o.MonthsReviewedCount > nMonths)
					{
						TagItem(key, o);
						ret = false;
						break;
					}
				}
				catch(Exception ex)
				{
					LogCreditWarning(ex);
				}
			}

			InsertCache(key, ret);
			return ret;
		}


		// 1/23/2007 nw - External OPM 21337 - New keyword for contiguous mortgage tradelines, accepts 2 parameters:
		// a)int  Months:  default is 24, this is to specify how long the contiguous period must be.
		// b)bool DerogOk: default is true, this is to specify whether derog history is allowed in the contiguous period.
		// RETURNS 1 if true, 0 if false.
		virtual public int HasContiguousMortgageHistory(string parameters)
		{
			return EvaluateHasContiguousMortgageHistory(parameters, this);
		}


		static public int EvaluateHasContiguousMortgageHistory(string parameters, AbstractCreditReport creditReport)
		{
			string key = "HasContiguousMortgageHistory:" + parameters.ToLower();

			if(creditReport != null && creditReport.HasCache(key))
				return creditReport.GetCacheInt(key);

			// Contiguous mortgage history period: Months
			string monthsOp = "=";
			int monthsVal = 24;

			// Is derog history allowed in the contiguous period?: DerogOk
			string derogOkOp = "=";
			bool derogOkVal = true; // y, n

			// Supported operators are : <, <=, >, >=, =, but so far this keyword just needs =
			char[] op_chars = new  char[] { '<', '>', '=' };

			foreach(string attribute in parameters.ToLower().Split('&'))
			{
				if(attribute == "," || attribute == "")
					continue;

				string name = "";
				string op = "";
				string val = "";
				
				// Extracting name
				// 12/21/2006 nw - OPM 5606 - Handle mistyped parameters/attributes
				int nameLen = attribute.IndexOfAny(op_chars);
				try
				{
					name = attribute.Substring(0, nameLen);
				}
				catch	// must be an invalid format, log bug below and throw exception in the switch statement
				{
					name = attribute;
				}

				// Extracting operator
				StringBuilder opBuilder = new StringBuilder(5);
				Match match = System.Text.RegularExpressions.Regex.Match(attribute, "[<>=]");
				while(match.Success)
				{
					string token = match.Groups[0].Value;
					opBuilder.Append(token);
					match = match.NextMatch();
				}
				op = opBuilder.ToString();

				// Extracting value
				try
				{
					val = attribute.Substring(nameLen + op.Length);
				}
				catch{} // must be an invalid format 

				if(name == "" || op == "" || val == "")
					Tools.LogBug(string.Format("None of these can be empty. Name={0}. Op={1}. Val={2}", name, op, val));

				switch(name)
				{
					case "months":
						if(op != "=")
							throw CreateGenericExceptionWithDevMessage(string.Format("{0} is not a supported operator for parameter Months in keyword HasContiguousMortgageHistory", op));
						monthsOp = op;
						
						try
						{
							monthsVal = int.Parse(val);
							if(monthsVal < 1)
								throw CreateGenericExceptionWithDevMessage("Months was < 1.");
						}
						catch
						{
							throw CreateGenericExceptionWithDevMessage(string.Format("{0} is not a supported argument for parameter Months in keyword HasContiguousMortgageHistory", val));
						}
						break;
					case "derogok":
						if(op != "=")
							throw CreateGenericExceptionWithDevMessage(string.Format("{0} is not a supported operator for parameter DerogOk in keyword HasContiguousMortgageHistory", op));
						derogOkOp = op;

					switch(val)
					{
						case "y": derogOkVal = true; break;
						case "n": derogOkVal = false; break;
						default: throw CreateGenericExceptionWithDevMessage(string.Format("{0} is not a supported argument for parameter DerogOk in keyword HasContiguousMortgageHistory", val));
					}
						break;
					default:
						string s = string.Format("Error encountered in HasContiguousMortgageHistory keyword, parameter name:{0} is not valid", name);
						Tools.LogError(s);
						throw CreateGenericExceptionWithDevMessage(s);					
				}
			}

			if(creditReport == null)
				return 0;

			// monthCounterArray[0] stands for current month, monthCounterArray[1] stands for last month, etc.
			// value of 0 means "not covered by existing tradelines", value of 1 means "covered by existing tradelines"
			int[] monthCounterArray = new int[360];
			for (int i = 0; i < 360; i++)
				monthCounterArray[i] = 0;
			bool bFoundOpenMortgageTradeline = false;

			foreach (AbstractCreditLiability o in creditReport.EffectiveLiabilities)
			{
				try
				{
					// Look for Mortgage tradelines only
					if (!o.IsTypeMortgage)
						continue;

					// 5/3/2007 nw - can't have derogs >= Late120 in the past
					if (o.CreditAdverseRatingList.Count != 0)
					{
						bool bFoundBadDerog = false;
						foreach (CreditAdverseRating rating in o.CreditAdverseRatingList)
						{
							if (rating.AdverseType >= E_AdverseType.Late120)
							{
								bFoundBadDerog = true;
								break;
							}
						}
						if (bFoundBadDerog)
							continue;
					}

					creditReport.TagItem(key, o);

					// determine the end date of the tradeline data
					DateTime dtEndDate = (o.LastActivityDate > o.PaymentPatternStartDate) ? o.LastActivityDate : o.PaymentPatternStartDate;
					if (dtEndDate == DateTime.MinValue)
						dtEndDate = o.AccountReportedDate;
					if (dtEndDate == DateTime.MinValue)
						dtEndDate = o.AccountOpenedDate.AddMonths(o.MonthsReviewedCount);

					if (o.IsActive)
					{
						if (o.MonthsReviewedCount >= monthsVal && (!o.HasBeenDerog || derogOkVal == true))
						{
							creditReport.InsertCache(key, 1);
							return 1;
						}
						else if (o.MonthsReviewedCount >= monthsVal && o.HasBeenDerog && derogOkVal == false)
						{
							// date of the most recent derog event needs to be considered
							CreditAdverseRating rating;
							try
							{
								rating = (CreditAdverseRating)o.CreditAdverseRatingList[0];
							}
							catch	// (ArgumentOutOfRangeException exc)
							{
								// the most recent derog event isn't listed in the payment history
								continue;
							}

							if (Tools.IsDateWithin(rating.Date, monthsVal - 1, dtEndDate))
								continue;	// the derog event is too recent, can't use this open tradeline
							else
							{
								// the derog event is sufficiently old that this tradeline still qualifies
								creditReport.InsertCache(key, 1);
								return 1;
							}
						}
						else if (o.MonthsReviewedCount < monthsVal && (!o.HasBeenDerog || derogOkVal == true))
						{
							// tag the months covered by this tradeline
							int endMonthIndex = DateTime.Today.Month - dtEndDate.Month + ((DateTime.Today.Year - dtEndDate.Year) * 12);
							int startMonthIndex = endMonthIndex + o.MonthsReviewedCount - 1;
							for (int i = endMonthIndex; i <= startMonthIndex; i++)
								monthCounterArray[i] = 1;
						}
						else if (o.MonthsReviewedCount < monthsVal && o.HasBeenDerog && derogOkVal == false)
						{
							// tag the months from "most recent derog event month + 1" to tradeline end month
							CreditAdverseRating rating;
							try
							{
								rating = (CreditAdverseRating)o.CreditAdverseRatingList[0];
							}
							catch	// (ArgumentOutOfRangeException exc)
							{
								// the most recent derog event isn't listed in the payment history
								continue;
							}

							int endMonthIndex = DateTime.Today.Month - dtEndDate.Month + ((DateTime.Today.Year - dtEndDate.Year) * 12);
							int startMonthIndex = DateTime.Today.Month - rating.Date.Month - 1 + ((DateTime.Today.Year - rating.Date.Year) * 12);
							for (int i = endMonthIndex; i <= startMonthIndex; i++)
								monthCounterArray[i] = 1;
						}

						bFoundOpenMortgageTradeline = true;
					}

						// mortgage tradeline is closed
					else
					{
						if (!o.HasBeenDerog || derogOkVal == true)
						{
							// tag the months covered by this tradeline
							int endMonthIndex = DateTime.Today.Month - dtEndDate.Month + ((DateTime.Today.Year - dtEndDate.Year) * 12);
							int startMonthIndex = endMonthIndex + o.MonthsReviewedCount - 1;
							for (int i = endMonthIndex; i <= startMonthIndex; i++)
								monthCounterArray[i] = 1;
						}

						else	// (o.HasBeenDerog && derogOkVal == false)
						{
							// tag the months from "most recent derog event month + 1" to tradeline end month
							CreditAdverseRating rating;
							try
							{
								rating = (CreditAdverseRating)o.CreditAdverseRatingList[0];
							}
							catch	// (ArgumentOutOfRangeException exc)
							{
								// the most recent derog event isn't listed in the payment history
								continue;
							}

							int endMonthIndex = DateTime.Today.Month - dtEndDate.Month + ((DateTime.Today.Year - dtEndDate.Year) * 12);
							int startMonthIndex = DateTime.Today.Month - rating.Date.Month - 1 + ((DateTime.Today.Year - rating.Date.Year) * 12);
							for (int i = endMonthIndex; i <= startMonthIndex; i++)
								monthCounterArray[i] = 1;
						}
					}
				}
				catch(Exception ex)
				{
					creditReport.LogCreditWarning(ex);
				}
			}

			// must have at least 1 open mortgage tradeline to satisfy this keyword
			if (!bFoundOpenMortgageTradeline)
			{
				creditReport.InsertCache(key, 0);
				return 0;
			}

			// determine if the most recent contiguous block satisfies monthsVal
			int startIndex = -1;
			for (int i = 0; i < 360; i++)
			{
				if (monthCounterArray[i] == 1)
				{
					startIndex = i;
					break;
				}
			}

			if (startIndex == -1)	// no mortgage tradelines on credit report or none can be used
			{
				creditReport.InsertCache(key, 0);
				return 0;
			}

			for (int i = startIndex; i < startIndex + monthsVal && i < 360; i++)
			{
				if (monthCounterArray[i] == 0)	// contiguous block is not long enough
				{
					creditReport.InsertCache(key, 0);
					return 0;
				}
			}

			creditReport.InsertCache(key, 1);
			return 1;
		}


		virtual public int GetHighestUnpaidBalOfCollectionOlderThan(int nMonths) 
		{ 
			string key = "GetHighestUnpaidBalOfCollectionOlderThan:" + nMonths;

			if (HasCache(key))
				return GetCacheInt(key);

			return GetHighestUnpaidBalOfDerogTypesOlderThan(new E_DerogT[] { E_DerogT.Collection }, nMonths, key);
		}


		virtual public int GetHighestUnpaidBalOfJudgmentOlderThan(int nMonths) 
		{ 
			string key = "GetHighestUnpaidBalOfJudgmentOlderThan:" + nMonths;

			if (HasCache(key))
				return GetCacheInt(key);

			return GetHighestUnpaidBalOfDerogTypesOlderThan(new E_DerogT[] { E_DerogT.Judgment }, nMonths, key);
		}

		//10/25/07 db - OPM 18561
        [CreditMigrated]
		virtual public int GetHighestUnpaidBalOfMedicalCollectionWithin(int nMonths) 
		{ 
			string key = "GetHighestUnpaidBalOfMedicalCollectionWithin:" + nMonths;

			if (HasCache(key))
				return GetCacheInt(key);

			decimal highestBal = 0;

			foreach (AbstractCreditLiability o in EffectiveLiabilities) 
			{
				try
				{
					decimal newBal = o.UnpaidBalanceAmount;
					if(newBal <= highestBal || o.IsPaid)
						continue;

					if(o.IsMedicalTradeline && o.HasMajorDerogEffectiveWithin(new E_DerogT[] { E_DerogT.Collection }, nMonths, /* bInclPaidAccs */ false))
					{
						TagItem(key, o);
						highestBal = o.UnpaidBalanceAmount;
					}
				}
				catch(Exception ex)
				{
					LogCreditWarning(ex);
				}
			}

			return (int) highestBal;
		}


		public int GetHighestUnpaidBalOfChargeOffOlderThan(int nMonths) 
		{ 
			string key = "GetHighestUnpaidBalOfChargeOffOlderThan:" + nMonths;

			if (HasCache(key))
				return GetCacheInt(key);

			return GetHighestUnpaidBalOfDerogTypesOlderThan(new E_DerogT[] { E_DerogT.ChargeOff }, nMonths, key);
		}


		// 5/14/2007 nw - OPM 13060 - New charge off / collection keywords
		virtual public int HighestUnpaidBalOfChargeOffOrCollectionOlderThan(int nMonths)
		{ 
			string key = "HighestUnpaidBalOfChargeOffOrCollectionOlderThan:" + nMonths;

			if (HasCache(key))
				return GetCacheInt(key);

			return GetHighestUnpaidBalOfDerogTypesOlderThan(new E_DerogT[] { E_DerogT.ChargeOff, E_DerogT.Collection }, nMonths, key);
		}



		//1.	Late and the account is still open: this is <= 36 months.
		//2.	Late and was closed, depends on when it was close, if it was closed, use the closed date.
        [CreditMigrated]
		virtual public int GetHighestUnpaidPastDueWithin(int nMonths) 
		{ 
			string key = "GetHighestUnpaidPastDueWithin:" + nMonths;

			if (HasCache(key))
				return GetCacheInt(key);

			DateTime todayDate = DateTime.Today;
			int highestBal = 0;
			foreach(AbstractCreditLiability lia in EffectiveLiabilities)
			{
				int bal = (int) lia.PastDueAmount;

				if(bal > highestBal && lia.IsLate && Tools.IsDateWithin(lia.AccountReportedDate, nMonths, todayDate))
				{
					highestBal = bal;
					TagItem(key, lia);
				}
			}
			return highestBal;
		}

        [CreditMigrated]
		virtual public int GetHighestUnpaidPastDueOlderThan(int nMonths) 
		{ 
			string key = "GetHighestUnpaidPastDueOlderThan:" + nMonths;

			if (HasCache(key))
				return GetCacheInt(key);

			DateTime todayDate = DateTime.Today;

			int highestBal = 0;
			foreach(AbstractCreditLiability lia in EffectiveLiabilities)
			{
				int bal = (int) lia.PastDueAmount;

				if(bal > highestBal && lia.IsLate && !Tools.IsDateWithin(lia.AccountReportedDate, nMonths, todayDate))
				{
					highestBal = bal;
					TagItem(key, lia);
				}
			}
			return highestBal;
		}

		/// <summary>
		/// Used by banks: GMAC
		/// </summary>

		virtual public bool HasTaxOpenTaxLien
		{
			get
			{
				string methodName = "HasTaxOpenTaxLien";

				if (HasCache(methodName))
					return GetCacheBool(methodName);

				bool ret = false;
				foreach (AbstractCreditPublicRecord o in EffectivePublicRecords) 
				{
					try 
					{
						if (o.IsTaxLienUnpaid) 
						{
							ret = true;
							TagItem(methodName, o);
							break;
						}
					} 
					catch (Exception ex) 
					{
						LogCreditWarning(ex);
					}

				}

				InsertCache(methodName, ret);
				return ret;

			}
		}
	

		public int GetHighestUnpaidBalOfCollectionWithin(int nMonths)
		{ 
			string key = "GetHighestUnpaidBalOfCollectionWithin:" + nMonths;

			if (HasCache(key))
				return GetCacheInt(key);

			return GetHighestUnpaidBalOfDerogTypesWithin(new E_DerogT[] { E_DerogT.Collection }, nMonths, key);
		}


		virtual public int GetHighestUnpaidBalOfJudgmentWithin(int nMonths)
		{ 
			string key = "GetHighestUnpaidBalOfJudgmentWithin:" + nMonths;

			if (HasCache(key))
				return GetCacheInt(key);

			return GetHighestUnpaidBalOfDerogTypesWithin(new E_DerogT[] { E_DerogT.Judgment }, nMonths, key);
		}
		


		public int GetHighestUnpaidBalOfChargeOffWithin(int nMonths)
		{ 
			string key = "GetHighestUnpaidBalOfChargeOffWithin:" + nMonths;

			if (HasCache(key))
				return GetCacheInt(key);

			return GetHighestUnpaidBalOfDerogTypesWithin(new E_DerogT[] { E_DerogT.ChargeOff }, nMonths, key);
		}


		virtual public int GetHighestUnpaidBalOfTaxLienWithin(int nMonths) 
		{ 
			string key = "GetHighestUnpaidBalOfTaxLienWithin:" + nMonths;

			if (HasCache(key))
				return GetCacheInt(key);

			return GetHighestUnpaidBalOfDerogTypesWithin(new E_DerogT[] { E_DerogT.TaxLien }, nMonths, key);
		}


		// 5/14/2007 nw - OPM 13060 - New charge off / collection keywords
		virtual public int HighestUnpaidBalOfChargeOffOrCollectionWithin(int nMonths)
		{ 
			string key = "HighestUnpaidBalOfChargeOffOrCollectionWithin:" + nMonths;

			if (HasCache(key))
				return GetCacheInt(key);

			return GetHighestUnpaidBalOfDerogTypesWithin(new E_DerogT[] { E_DerogT.ChargeOff, E_DerogT.Collection }, nMonths, key);
		}


		public int GetTotUnpaidBalOfCollectionWithin(int nMonths) 
		{ 
			string key = "GetTotUnpaidBalOfCollectionWithin:" + nMonths;

			if (HasCache(key))
				return GetCacheInt(key);

			return GetTotUnpaidBalOfDerogTypesWithin(new E_DerogT[] { E_DerogT.Collection }, nMonths, key); 
		}


		virtual public int GetTotUnpaidBalOfJudgmentWithin(int nMonths) 
		{ 
			string key = "GetTotUnpaidBalOfJudgmentWithin:" + nMonths;

			if (HasCache(key))
				return GetCacheInt(key);

			return GetTotUnpaidBalOfDerogTypesWithin(new E_DerogT[] { E_DerogT.Judgment }, nMonths, key); 
		}

		public int GetTotUnpaidBalOfChargeOffWithin(int nMonths) 
		{ 
			string key = "GetTotUnpaidBalOfChargeOffWithin:" + nMonths;

			if (HasCache(key))
				return GetCacheInt(key);

			return GetTotUnpaidBalOfDerogTypesWithin(new E_DerogT[] { E_DerogT.ChargeOff }, nMonths, key); 
		}


		virtual public int GetTotUnpaidBalOfTaxLienWithin(int nMonths)
		{
			string key = "GetTotUnpaidBalOfTaxLienWithin:" + nMonths;

			if (HasCache(key))
				return GetCacheInt(key);

			return GetTotUnpaidBalOfDerogTypesWithin(new E_DerogT[] { E_DerogT.TaxLien }, nMonths, key); 
		}


		virtual public int TotUnpaidBalOfAllDerogWithin(int nMonths)
		{
			string key = "TotUnpaidBalOfAllDerogWithin:" + nMonths;

			if (HasCache(key))
				return GetCacheInt(key);

			return GetTotUnpaidBalOfDerogTypesWithin(new E_DerogT[] { E_DerogT.ANY }, nMonths, key); 
		}


		// 2/7/2007 nw - OPM 10164
		virtual public int TotUnpaidBalOfChargeOffAndCollectionWithin(int nMonths) 
		{ 
			string key = "TotUnpaidBalOfChargeOffAndCollectionWithin:" + nMonths;

			if (HasCache(key))
				return GetCacheInt(key);

			return GetTotUnpaidBalOfDerogTypesWithin(new E_DerogT[] { E_DerogT.ChargeOff, E_DerogT.Collection }, nMonths, key); 
		}


        [CreditMigrated]
		virtual public int GetTradesCountActiveWithin(int nMonths)
		{
			string key = "GetTradesCountActiveWithin:" + nMonths;

			if (HasCache(key))
				return GetCacheInt(key);

			DateTime todayDate = DateTime.Today;
			int count = 0;
			foreach(AbstractCreditLiability lia in EffectiveLiabilities)
			{
				if(lia.IsCredit && !lia.IsBankruptcy && Tools.IsDateWithin(lia.LastActivityDate, nMonths, todayDate))
				{
					TagItem(key, lia);
					++count;
				}
			}
			return count;
		}

        [CreditMigrated]
		virtual public int GetTradesCountWithHighCreditOf(int amount)
		{
			string key = "GetTradesCountWithHighCreditOf:" + amount;

			if (HasCache(key))
				return GetCacheInt(key);

			int count = 0;
			foreach(AbstractCreditLiability lia in EffectiveLiabilities)
			{
				// OPM#2190
				if(lia.IsCredit && lia.HighCreditAmount > amount && !lia.IsBankruptcy && !lia.IsChargeOff && !lia.IsCollection && !lia.IsRepos)
				{
					TagItem(key, lia);
					++count;
				}
			}
			return count;
		}

        [CreditMigrated]
		virtual public int GetTradesCountActiveWithHighCreditOf(int amount)
		{
			string key = "GetTradesCountActiveWithHighCreditOf:" + amount;

			if (HasCache(key))
				return GetCacheInt(key);

			int count = 0;
			foreach(AbstractCreditLiability lia in EffectiveLiabilities)
			{
				if(lia.IsCredit && lia.IsCurrent  && !lia.IsBankruptcy && lia.HighCreditAmount > amount)
				{
					TagItem(key, lia);
					++count;
				}
			}
			return count;
		}


    	#endregion // GENERIC

		#region GENERIC - FORECLOSURE

        [CreditMigrated]
		public int GetForeclosure(int nMonths)
		{
			string key = "GetForeclosure:" + nMonths;
			if (HasCache(key))
				return GetCacheInt(key);
                
			int count = 0;

			// 3/14/2007 nw - OPM 11633 - needs to take public records into consideration
			foreach (AbstractCreditPublicRecord o in EffectivePublicRecords)
			{
				if (o.IsForeclosureUnpaid)
				{
					TagItem(key, o);
					count++;
				}
				else if (o.IsForeclosurePaid && Tools.IsDateWithin(o.DispositionDate, nMonths))
				{
					TagItem(key, o);
					count++;
				}
			}

			foreach (AbstractCreditLiability o in EffectiveLiabilities) 
			{
				if(o.HasMajorDerogEffectiveWithin(new E_DerogT[] { E_DerogT.Foreclosure }, nMonths,  /* bInclPaidAccs */ true)) 
				{
					TagItem(key, o);
					count++;
				}
			}

			InsertCache(key, count);
			return count;
		}

        // 2/22/2013 gf - OPM 75677 - Changed default value of Count120 to false.
        // 02/06/2010 mf - OPM 42418. Brought this keyword back, and updated it to support public records per OPM 11633
        // 8/23/2006 nw - OPM 7126 - New Foreclosure Keyword, accepts 2 parameters:
        // a)int  Within:   default is 1000, this is to specify look-back period of when the derog event occurs.
        // b)bool Count120: default is false, 120 means 120+ late.
        public int FcCountX(string parameters)
        {
            return EvaluateFcCountX(parameters, this);
        }
        static public int EvaluateFcCountX(string parameters, AbstractCreditReport creditReport)
        {
            string key = "FcCountX:" + parameters.ToLower();

            if (creditReport != null && creditReport.HasCache(key))
                return creditReport.GetCacheInt(key);

            // Months reviewed: Within
            string withinOp = "<=";
            int withinMonths = 1000;

            // Count 120+ late: Count120
            string count120Op = "=";
            bool count120Val = false; // y, n

            // Supported operators are : <, <=, >, >=, =, but so far this keyword just needs <= and =
            char[] op_chars = new  char[] { '<', '>', '=' };

            foreach(string attribute in parameters.ToLower().Split('&'))
            {
                if(attribute == "," || attribute == "")
                    continue;

                string name = "";
                string op = "";
                string val = "";

                // Extracting name
                // 12/21/2006 nw - OPM 5606 - Handle mistyped parameters/attributes
                int nameLen = attribute.IndexOfAny(op_chars);
                try
                {
                    name = attribute.Substring(0, nameLen);
                }
                catch	// must be an invalid format, log bug below and throw exception in the switch statement
                {
                    name = attribute;
                }

                // Extracting operator
                StringBuilder opBuilder = new StringBuilder(5);
                Match match = System.Text.RegularExpressions.Regex.Match(attribute, "[<>=]");
                while(match.Success)
                {
                    string token = match.Groups[0].Value;
                    opBuilder.Append(token);
                    match = match.NextMatch();
                }
                op = opBuilder.ToString();

                // Extracting value
                try
                {
                    val = attribute.Substring(nameLen + op.Length);
                }
                catch{} // must be an invalid format 

                if(name == "" || op == "" || val == "")
                    Tools.LogBug(string.Format("None of these can be empty. Name={0}. Op={1}. Val={2}", name, op, val));

                switch(name)
                {
                    case "within":
                        if(op != "<=")
                            throw CreateGenericExceptionWithDevMessage(string.Format("{0} is not a supported operator for parameter Within in keyword FcCountX", op));
                        withinOp = op;
						
                        try
                        {
                            withinMonths = int.Parse(val);
                            if(withinMonths < 0)
                                throw CreateGenericExceptionWithDevMessage("Months reviewed (withinMonths) was < 0.");
                        }
                        catch
                        {
                            throw CreateGenericExceptionWithDevMessage(string.Format("{0} is not a supported argument for parameter Within in keyword FcCountX", val));
                        }
                        break;
                    case "count120":
                        if(op != "=")
                            throw CreateGenericExceptionWithDevMessage(string.Format("{0} is not a supported operator for parameter Count120 in keyword FcCountX", op));
                        count120Op = op;

                        switch(val)
                        {
                            case "y": count120Val = true; break;
                            case "n": count120Val = false; break;
                            default: throw CreateGenericExceptionWithDevMessage(string.Format("{0} is not a supported argument for parameter Count120 in keyword FcCountX", val));
                        }
                        break;
                    default:
                        string s = string.Format("Error encountered in FcCountX keyword, parameter name:{0} is not valid", name);
                        Tools.LogError(s);
                        throw CreateGenericExceptionWithDevMessage(s);					
                }
            }

            if (creditReport == null)
                return 0;

            int count = 0;

            foreach (AbstractCreditPublicRecord o in creditReport.EffectivePublicRecords)
            {
                if (o.IsForeclosureUnpaid)
                {
                    creditReport.TagItem(key, o);
                    count++;
                }
                else if (o.IsForeclosurePaid && Tools.IsDateWithin(o.DispositionDate, withinMonths))
                {
                    creditReport.TagItem(key, o);
                    count++;
                }
            }

            foreach (AbstractCreditLiability o in creditReport.EffectiveLiabilities)
            {
                try
                {
                    // Look for Mortgage tradelines only.
                    // OPM 24318 - Do not count short sales marked as forclosures.
                    if (!o.IsTypeMortgage || o.IsShortSale)
                        continue;

                    if (o.HasMajorDerogEffectiveWithin(new E_DerogT[] { E_DerogT.Foreclosure }, withinMonths,/* bInclPaidAccs */ true))
                    {
                        creditReport.TagItem(key, o);
                        ++count;
                        continue;
                    }

                    E_AdverseType lates = E_AdverseType.RepossessionOrForeclosure;
                    if (count120Val)
                        lates = lates | E_AdverseType.Late120 | E_AdverseType.Late150 | E_AdverseType.Late180;

                    if (o.IsFirstPmtTypeWithin(lates, withinMonths))
                    {
                        creditReport.TagItem(key, o);
                        ++count;
                        continue;
                    }
                }
                catch(Exception ex)
                {
                    creditReport.LogCreditWarning(ex);
                }
            }

            creditReport.InsertCache(key, count);
            return count;
        }

        // 8/25/2006 nw - OPM 7126 - New Foreclosure Keyword HasFcX, accepts 2 parameters:
		// a)int  Within:   default is 1000, this is to specify look-back period of when the derog event occurs.
		// b)bool Count120: default is false, 120 means 120+ late.
		// RETURNS 1 if true, 0 if false.
		virtual public int HasFcX(string parameters)
		{
			return EvaluateHasFcX(parameters, this);
		}


		static public int EvaluateHasFcX(string parameters, AbstractCreditReport creditReport)
		{
			string key = "HasFcX:" + parameters.ToLower();

			if(creditReport != null && creditReport.HasCache(key))
				return creditReport.GetCacheInt(key);

			// Months reviewed: Within
			string withinOp = "<=";
			int withinMonths = 1000;
			
			// Count 120+ late: Count120
			string count120Op = "=";
			bool count120Val = false; // y, n

			// Supported operators are : <, <=, >, >=, =, but so far this keyword just needs <= and =
			char[] op_chars = new  char[] { '<', '>', '=' };

			foreach(string attribute in parameters.ToLower().Split('&'))
			{
				if(attribute == "," || attribute == "")
					continue;

				string name = "";
				string op = "";
				string val = "";
				
				// Extracting name
				// 12/21/2006 nw - OPM 5606 - Handle mistyped parameters/attributes
				int nameLen = attribute.IndexOfAny(op_chars);
				try
				{
					name = attribute.Substring(0, nameLen);
				}
				catch	// must be an invalid format, log bug below and throw exception in the switch statement
				{
					name = attribute;
				}

				// Extracting operator
				StringBuilder opBuilder = new StringBuilder(5);
				Match match = System.Text.RegularExpressions.Regex.Match(attribute, "[<>=]");
				while(match.Success)
				{
					string token = match.Groups[0].Value;
					opBuilder.Append(token);
					match = match.NextMatch();
				}
				op = opBuilder.ToString();

				// Extracting value
				try
				{
					val = attribute.Substring(nameLen + op.Length);
				}
				catch{} // must be an invalid format 

				if(name == "" || op == "" || val == "")
					Tools.LogBug(string.Format("None of these can be empty. Name={0}. Op={1}. Val={2}", name, op, val));

				switch(name)
				{
					case "within":
						if(op != "<=")
							throw CreateGenericExceptionWithDevMessage(string.Format("{0} is not a supported operator for parameter Within in keyword HasFcX", op));
						withinOp = op;
						
						try
						{
							withinMonths = int.Parse(val);
							if(withinMonths < 0)
								throw CreateGenericExceptionWithDevMessage("Months reviewed (withinMonths) was < 0.");
						}
						catch
						{
							throw CreateGenericExceptionWithDevMessage(string.Format("{0} is not a supported argument for parameter Within in keyword HasFcX", val));
						}
						break;
					case "count120":
						if(op != "=")
							throw CreateGenericExceptionWithDevMessage(string.Format("{0} is not a supported operator for parameter Count120 in keyword HasFcX", op));
						count120Op = op;

					switch(val)
					{
						case "y": count120Val = true; break;
						case "n": count120Val = false; break;
						default: throw CreateGenericExceptionWithDevMessage(string.Format("{0} is not a supported argument for parameter Count120 in keyword HasFcX", val));
					}
						break;
					default:
						string s = string.Format("Error encountered in HasFcX keyword, parameter name:{0} is not valid", name);
						Tools.LogError(s);
						throw CreateGenericExceptionWithDevMessage(s);					
				}
			}

			if(creditReport == null)
				return 0;

			// 3/14/2007 nw - OPM 11633 - needs to take public records into consideration
			foreach (AbstractCreditPublicRecord o in creditReport.EffectivePublicRecords)
			{
				if (o.IsForeclosureUnpaid)
				{
					creditReport.TagItem(key, o);
					creditReport.InsertCache(key, 1);
					return 1;
				}
				else if (o.IsForeclosurePaid && Tools.IsDateWithin(o.DispositionDate, withinMonths))
				{
					creditReport.TagItem(key, o);
					creditReport.InsertCache(key, 1);
					return 1;
				}
			}

			foreach (AbstractCreditLiability o in creditReport.EffectiveLiabilities)
			{
				try
				{
					// Look for Mortgage tradelines only
					if (!o.IsTypeMortgage || o.IsShortSale)
						continue;

					if (o.HasMajorDerogEffectiveWithin(new E_DerogT[] { E_DerogT.Foreclosure }, withinMonths,  /* bInclPaidAccs */ true))
					{
						creditReport.TagItem(key, o);
						creditReport.InsertCache(key, 1);
						return 1;
					}

					E_AdverseType lates = E_AdverseType.RepossessionOrForeclosure;
					if (count120Val)
						lates = lates | E_AdverseType.Late120 | E_AdverseType.Late150 | E_AdverseType.Late180;

					if (o.IsFirstPmtTypeWithin(lates, withinMonths))
					{
						creditReport.TagItem(key, o);
						creditReport.InsertCache(key, 1);
						return 1;
					}
				}
				catch(Exception ex)
				{
					creditReport.LogCreditWarning(ex);
				}
			}

			creditReport.InsertCache(key, 0);
			return 0;
		}
		
		private AbstractCreditPublicRecord LastForeclosure
		{
			get
			{
				DateTime lastEffectiveD = DateTime.MinValue;
				AbstractCreditPublicRecord lastFc = null;

				foreach (AbstractCreditPublicRecord o in EffectivePublicRecords)
				{
					try
					{
						if(o.Type == E_CreditPublicRecordType.Foreclosure)
						{
							DateTime led = o.LastEffectiveDate;

							if(led.CompareTo(lastEffectiveD) > 0)	
							{
								lastFc = o;
								lastEffectiveD = led;
							}
						}
					}
					catch(Exception ex)
					{
						LogCreditWarning(ex);
					}
				}
				return lastFc;
			}
		}
		public CDateTime DischargedDOfLastForeclosure	
		{ 
			get
			{
				AbstractCreditPublicRecord lastFc = LastForeclosure;

				if(lastFc == null || !lastFc.IsDischarged)
					return CDateTime.InvalidWrapValue;
				return CDateTime.Create(lastFc.DispositionDate);
			}
		}

		public CDateTime FileDOfLastForeclosure
		{ 
			get
			{
				AbstractCreditPublicRecord lastFc = LastForeclosure;

				if(lastFc == null)
					return CDateTime.InvalidWrapValue;

				return CDateTime.Create(lastFc.BkFileDate);
			}
		}


		virtual public int ForeclosureUnpaidCount
		{ 
			get 
			{ 
				int count = 0;
				foreach (AbstractCreditLiability o in EffectiveLiabilities) 
				{
					try
					{
						if(o.IsForeclosure && !o.IsPaid)
						{
							TagItem("ForeclosureUnpaidCount", o);
							++count;
						}
					}
					catch(Exception ex)
					{
						LogCreditWarning(ex);
					}
				}
				return count;
			} 
		}

		#endregion 
        #region NEW BK METHODS

		/// <summary>
		/// return true if there is a 7 in the payment string of a mortgage within the # of months specified, 
		/// or if the current status of a mortgage tradeline is a BK status, and the last activity date is within the 
		/// # of months specified.11-10-08 av opm 25329  
		/// </summary>
		/// <param name="nMonths">Number of months the BK occured </param>
		/// <returns>bool</returns>
        [CreditMigrated]
		virtual public bool HasMortgageIncludedInBKWithin (int nMonths) 
		{
			string key = "HasMortgageIncludedInBKWithin:" + nMonths.ToString(); 
			if (HasCache(key)){ return this.GetCacheBool(key); }

			bool hasMortgageIncludedInBK = false; 

			foreach (AbstractCreditLiability o in EffectiveLiabilities)  
			{
				if (! o.IsTypeMortgage) {continue;} //we only care about mortgage type 
				DateTime  latestBk = o.DateOfLastestAdversePmtType(E_AdverseType.Bankruptcy); 
				if (Tools.IsDateWithin(latestBk, nMonths) ||							  //if there is a bk payment within nMonths OR the status is 
					o.IsBankruptcy && Tools.IsDateWithin(o.LastActivityDate, nMonths)) //bk and the last activity was within the time specified 
				{
					TagItem(key, o); 
					hasMortgageIncludedInBK =  true; 
					break;
				}
			}
			InsertCache(key, hasMortgageIncludedInBK); 
			return hasMortgageIncludedInBK; 
		}

		private int HasBkOfTypeAndCharacterWithin(E_BkType bkTypes, E_BkChar bkChars, int nMonths, string sCacheKey)
		{

			foreach (AbstractCreditPublicRecord o in EffectivePublicRecords)
			{
				try 
				{
					if(o.IsBkOfTypeAndCharacter(bkTypes, bkChars, sCacheKey))
					{
						DateTime dt = o.LastEffectiveDate; // will check for status date and fall back to report date if needed.
						if(Tools.IsDateWithin(dt, nMonths)) 
						{
							TagItem(sCacheKey, o);
							return 1;
						}
					}
				} 
				catch (Exception exc)
				{
					LogCreditWarning(exc);
				}
			}
			return 0;
		}



		virtual public int HasDischargedBkWithin(int nMonths)
		{
			string key = "HasDischargedBkWithin:" + nMonths;
			if (HasCache(key))
				return GetCacheInt(key);

			nMonths = nMonths > 1 ? nMonths - 1 : 0; // see case 2357.  7/15/05 -tn.

			int count = HasBkOfTypeAndCharacterWithin(E_BkType.Any, E_BkChar.Closed, nMonths, key);

			InsertCache(key, count);

			return count;

		}


		virtual public int HasDischarged7BkWithin(int nMonths)
		{
			string key = "HasDischarged7BkWithin:" + nMonths;
			if (HasCache(key))
				return GetCacheInt(key);

			nMonths = nMonths > 1 ? nMonths - 1 : 0; // see case 2357.  7/15/05 -tn.

			int count = HasBkOfTypeAndCharacterWithin(E_BkType.Ch7, E_BkChar.Closed, nMonths, key);

			InsertCache(key, count);
			return count;

		}


		virtual public int HasDischarged11BkWithin(int nMonths)
		{
			string key = "HasDischarged11BkWithin:" + nMonths;
			if (HasCache(key))
				return GetCacheInt(key);

			nMonths = nMonths > 1 ? nMonths - 1 : 0; // see case 2357.  7/15/05 -tn.

			int count = HasBkOfTypeAndCharacterWithin(E_BkType.Ch11, E_BkChar.Closed, nMonths, key);

			InsertCache(key, count);
			return count;

		}


		virtual public int HasDischarged12BkWithin(int nMonths)
		{
			string key = "HasDischarged12BkWithin:" + nMonths;
			if (HasCache(key))
				return GetCacheInt(key);

			nMonths = nMonths > 1 ? nMonths - 1 : 0; // see case 2357.  7/15/05 -tn.

			int count = HasBkOfTypeAndCharacterWithin(E_BkType.Ch12, E_BkChar.Closed, nMonths, key);

			InsertCache(key, count);
			return count;

		}

		virtual public int HasDischarged13BkWithin(int nMonths)
		{
			string key = "HasDischarged13BkWithin:" + nMonths;
			if (HasCache(key))
				return GetCacheInt(key);

			nMonths = nMonths > 1 ? nMonths - 1 : 0; // see case 2357.  7/15/05 -tn.

			int count = HasBkOfTypeAndCharacterWithin(E_BkType.Ch13, E_BkChar.Closed, nMonths, key);

			InsertCache(key, count);
			return count;

		}


		virtual public int HasDischargedUnknownTypeBkWithin(int nMonths)
		{
			string key = "HasDischargedUnknownTypeBkWithin:" + nMonths;
			if (HasCache(key))
				return GetCacheInt(key);
            
			nMonths = nMonths > 1 ? nMonths - 1 : 0; // see case 2357.  7/15/05 -tn.

			int count = HasBkOfTypeAndCharacterWithin(E_BkType.UnknownType, E_BkChar.Closed, nMonths, key);

			InsertCache(key, count);
			return count;

		}


		// 7/3/2006 nw - OPM 5752 - New sets of BK keywords
		#region HasClosedBkWithin, replaces the existing HasDischargedBkWithin, counts both discharged and dismissed BK
		virtual public int HasClosedBkWithin(int nMonths)
		{
			string key = "HasClosedBkWithin:" + nMonths;
			if (HasCache(key))
				return GetCacheInt(key);

			nMonths = nMonths > 1 ? nMonths - 1 : 0; // see case 2357.  7/15/05 -tn.

			int count = HasBkOfTypeAndCharacterWithin(E_BkType.Any, E_BkChar.Closed, nMonths, key);

			InsertCache(key, count);

			return count;

		}


		virtual public int HasClosed7BkWithin(int nMonths)
		{
			string key = "HasClosed7BkWithin:" + nMonths;
			if (HasCache(key))
				return GetCacheInt(key);

			nMonths = nMonths > 1 ? nMonths - 1 : 0; // see case 2357.  7/15/05 -tn.

			int count = HasBkOfTypeAndCharacterWithin(E_BkType.Ch7, E_BkChar.Closed, nMonths, key);

			InsertCache(key, count);
			return count;

		}


		virtual public int HasClosed11BkWithin(int nMonths)
		{
			string key = "HasClosed11BkWithin:" + nMonths;
			if (HasCache(key))
				return GetCacheInt(key);

			nMonths = nMonths > 1 ? nMonths - 1 : 0; // see case 2357.  7/15/05 -tn.

			int count = HasBkOfTypeAndCharacterWithin(E_BkType.Ch11, E_BkChar.Closed, nMonths, key);

			InsertCache(key, count);
			return count;

		}


		virtual public int HasClosed12BkWithin(int nMonths)
		{
			string key = "HasClosed12BkWithin:" + nMonths;
			if (HasCache(key))
				return GetCacheInt(key);

			nMonths = nMonths > 1 ? nMonths - 1 : 0; // see case 2357.  7/15/05 -tn.

			int count = HasBkOfTypeAndCharacterWithin(E_BkType.Ch12, E_BkChar.Closed, nMonths, key);

			InsertCache(key, count);
			return count;

		}

		virtual public int HasClosed13BkWithin(int nMonths)
		{
			string key = "HasClosed13BkWithin:" + nMonths;
			if (HasCache(key))
				return GetCacheInt(key);

			nMonths = nMonths > 1 ? nMonths - 1 : 0; // see case 2357.  7/15/05 -tn.

			int count = HasBkOfTypeAndCharacterWithin(E_BkType.Ch13, E_BkChar.Closed, nMonths, key);

			InsertCache(key, count);
			return count;

		}


		virtual public int HasClosedUnknownTypeBkWithin(int nMonths)
		{
			string key = "HasClosedUnknownTypeBkWithin:" + nMonths;
			if (HasCache(key))
				return GetCacheInt(key);
            
			nMonths = nMonths > 1 ? nMonths - 1 : 0; // see case 2357.  7/15/05 -tn.

			int count = HasBkOfTypeAndCharacterWithin(E_BkType.UnknownType, E_BkChar.Closed, nMonths, key);

			InsertCache(key, count);
			return count;

		}
		#endregion HasClosedBkWithin, replaces the existing HasDischargedBkWithin, counts both discharged and dismissed BK


		#region HasDismissedBkWithin, counts only the dismissed BK
		virtual public int HasDismissedBkWithin(int nMonths)
		{
			string key = "HasDismissedBkWithin:" + nMonths;
			if (HasCache(key))
				return GetCacheInt(key);

			nMonths = nMonths > 1 ? nMonths - 1 : 0; // see case 2357.  7/15/05 -tn.

			int count = HasBkOfTypeAndCharacterWithin(E_BkType.Any, E_BkChar.Dismissed, nMonths, key);

			InsertCache(key, count);

			return count;

		}


		virtual public int HasDismissed7BkWithin(int nMonths)
		{
			string key = "HasDismissed7BkWithin:" + nMonths;
			if (HasCache(key))
				return GetCacheInt(key);

			nMonths = nMonths > 1 ? nMonths - 1 : 0; // see case 2357.  7/15/05 -tn.

			int count = HasBkOfTypeAndCharacterWithin(E_BkType.Ch7, E_BkChar.Dismissed, nMonths, key);

			InsertCache(key, count);
			return count;

		}


		virtual public int HasDismissed11BkWithin(int nMonths)
		{
			string key = "HasDismissed11BkWithin:" + nMonths;
			if (HasCache(key))
				return GetCacheInt(key);

			nMonths = nMonths > 1 ? nMonths - 1 : 0; // see case 2357.  7/15/05 -tn.

			int count = HasBkOfTypeAndCharacterWithin(E_BkType.Ch11, E_BkChar.Dismissed, nMonths, key);

			InsertCache(key, count);
			return count;

		}


		virtual public int HasDismissed12BkWithin(int nMonths)
		{
			string key = "HasDismissed12BkWithin:" + nMonths;
			if (HasCache(key))
				return GetCacheInt(key);

			nMonths = nMonths > 1 ? nMonths - 1 : 0; // see case 2357.  7/15/05 -tn.

			int count = HasBkOfTypeAndCharacterWithin(E_BkType.Ch12, E_BkChar.Dismissed, nMonths, key);

			InsertCache(key, count);
			return count;

		}

		virtual public int HasDismissed13BkWithin(int nMonths)
		{
			string key = "HasDismissed13BkWithin:" + nMonths;
			if (HasCache(key))
				return GetCacheInt(key);

			nMonths = nMonths > 1 ? nMonths - 1 : 0; // see case 2357.  7/15/05 -tn.

			int count = HasBkOfTypeAndCharacterWithin(E_BkType.Ch13, E_BkChar.Dismissed, nMonths, key);

			InsertCache(key, count);
			return count;

		}


		virtual public int HasDismissedUnknownTypeBkWithin(int nMonths)
		{
			string key = "HasDismissedUnknownTypeBkWithin:" + nMonths;
			if (HasCache(key))
				return GetCacheInt(key);
            
			nMonths = nMonths > 1 ? nMonths - 1 : 0; // see case 2357.  7/15/05 -tn.

			int count = HasBkOfTypeAndCharacterWithin(E_BkType.UnknownType, E_BkChar.Dismissed, nMonths, key);

			InsertCache(key, count);
			return count;

		}
		#endregion HasDismissedBkWithin, counts only the dismissed BK


		#region HasDischBkWithin, counts only the discharged BK
		virtual public int HasDischBkWithin(int nMonths)
		{
			string key = "HasDischBkWithin:" + nMonths;
			if (HasCache(key))
				return GetCacheInt(key);

			nMonths = nMonths > 1 ? nMonths - 1 : 0; // see case 2357.  7/15/05 -tn.

			int count = HasBkOfTypeAndCharacterWithin(E_BkType.Any, E_BkChar.Discharged, nMonths, key);

			InsertCache(key, count);

			return count;

		}


		virtual public int HasDisch7BkWithin(int nMonths)
		{
			string key = "HasDisch7BkWithin:" + nMonths;
			if (HasCache(key))
				return GetCacheInt(key);

			nMonths = nMonths > 1 ? nMonths - 1 : 0; // see case 2357.  7/15/05 -tn.

			int count = HasBkOfTypeAndCharacterWithin(E_BkType.Ch7, E_BkChar.Discharged, nMonths, key);

			InsertCache(key, count);
			return count;

		}


		virtual public int HasDisch11BkWithin(int nMonths)
		{
			string key = "HasDisch11BkWithin:" + nMonths;
			if (HasCache(key))
				return GetCacheInt(key);

			nMonths = nMonths > 1 ? nMonths - 1 : 0; // see case 2357.  7/15/05 -tn.

			int count = HasBkOfTypeAndCharacterWithin(E_BkType.Ch11, E_BkChar.Discharged, nMonths, key);

			InsertCache(key, count);
			return count;

		}


		virtual public int HasDisch12BkWithin(int nMonths)
		{
			string key = "HasDisch12BkWithin:" + nMonths;
			if (HasCache(key))
				return GetCacheInt(key);

			nMonths = nMonths > 1 ? nMonths - 1 : 0; // see case 2357.  7/15/05 -tn.

			int count = HasBkOfTypeAndCharacterWithin(E_BkType.Ch12, E_BkChar.Discharged, nMonths, key);

			InsertCache(key, count);
			return count;

		}

		virtual public int HasDisch13BkWithin(int nMonths)
		{
			string key = "HasDisch13BkWithin:" + nMonths;
			if (HasCache(key))
				return GetCacheInt(key);

			nMonths = nMonths > 1 ? nMonths - 1 : 0; // see case 2357.  7/15/05 -tn.

			int count = HasBkOfTypeAndCharacterWithin(E_BkType.Ch13, E_BkChar.Discharged, nMonths, key);

			InsertCache(key, count);
			return count;

		}


		virtual  public int HasDischUnknownTypeBkWithin(int nMonths)
		{
			string key = "HasDischUnknownTypeBkWithin:" + nMonths;
			if (HasCache(key))
				return GetCacheInt(key);
            
			nMonths = nMonths > 1 ? nMonths - 1 : 0; // see case 2357.  7/15/05 -tn.

			int count = HasBkOfTypeAndCharacterWithin(E_BkType.UnknownType, E_BkChar.Discharged, nMonths, key);

			InsertCache(key, count);
			return count;

		}
		#endregion HasDischBkWithin, counts only the discharged BK
		// End OPM 5752 - New sets of BK keywords


		private bool HasBkOfTypeAndCharacter(CreditReport.E_BkType bkType, E_BkChar bkChar, string invokingMethodNm)
		{
			foreach (AbstractCreditPublicRecord o in EffectivePublicRecords)
			{
				try 
				{
					if(o.IsBkOfTypeAndCharacter(bkType, bkChar)) 
					{
						TagItem(invokingMethodNm, o);
						return true;
					}
				} 
				catch (Exception exc)
				{
					LogCreditWarning(exc);
				}
			}

			return false;
		}



		virtual public bool HasUnpaidAndNondischargedAnyBk 
		{ 
			get 
			{ 
				string key = "HasUnpaidAndNondischargedAnyBk";
				if (HasCache(key))
					return GetCacheBool(key);


				bool ret = HasBkOfTypeAndCharacter(E_BkType.Any, E_BkChar.NondischargedAndNonpaid, "HasUnpaidAndNondischargedAnyBk");

				InsertCache(key, ret);
				return ret; 
			} 
		}


		virtual public bool HasUnpaidAndNondischarged7Bk 
		{ 
			get 
			{ 
				string key = "HasUnpaidAndNondischarged7Bk";
				if (HasCache(key))
					return GetCacheBool(key);

				bool ret = HasBkOfTypeAndCharacter(E_BkType.Ch7, E_BkChar.NondischargedAndNonpaid, "HasUnpaidAndNondischarged7Bk");

				InsertCache(key, ret);
				return ret; 
			} 
		}


		virtual public bool HasUnpaidAndNondischarged11Bk 
		{ 
			get 
			{ 
				string key = "HasUnpaidAndNondischarged11Bk";
				if (HasCache(key))
					return GetCacheBool(key);

				bool ret = HasBkOfTypeAndCharacter(E_BkType.Ch11, E_BkChar.NondischargedAndNonpaid, "HasUnpaidAndNondischarged11Bk");

				InsertCache(key, ret);
				return ret; 
			} 
		}


		virtual public bool HasUnpaidAndNondischarged12Bk 
		{ 
			get 
			{ 
				string key = "HasUnpaidAndNondischarged12Bk";
				if (HasCache(key))
					return GetCacheBool(key);

				
				bool ret = HasBkOfTypeAndCharacter(E_BkType.Ch12, E_BkChar.NondischargedAndNonpaid, "HasUnpaidAndNondischarged12Bk");

				InsertCache(key, ret);
				return ret; 
			} 
		}


		virtual public bool HasUnpaidAndNondischarged13Bk 
		{ 
			get 
			{ 
				string key = "HasUnpaidAndNondischarged13Bk";
				if (HasCache(key))
					return GetCacheBool(key);


				bool ret = HasBkOfTypeAndCharacter(E_BkType.Ch13, E_BkChar.NondischargedAndNonpaid, "HasUnpaidAndNondischarged13Bk");

				InsertCache(key, ret);
				return ret; 
			} 
		}


		virtual public bool HasUnpaidAndNondischargedUnknownTypeBk 
		{ 
			get 
			{ 
				string key = "HasUnpaidAndNondischargedUnknownTypeBk";
				if (HasCache(key))
					return GetCacheBool(key);

				bool ret = HasBkOfTypeAndCharacter(E_BkType.UnknownType, E_BkChar.NondischargedAndNonpaid, "HasUnpaidAndNondischargedUnknownTypeBk");

				InsertCache(key, ret);
				return ret; 
			} 
		}


		virtual public E_BkMultipleT BkMultipleType 
		{ 
			get 
			{ 
				string key = "BkMultipleType";
				if (HasCache(key))
					return (E_BkMultipleT) GetCacheInt(key);
               
				AbstractCreditPublicRecord bk7 = null;
				AbstractCreditPublicRecord bk11 = null;
				AbstractCreditPublicRecord bk12 = null;
				AbstractCreditPublicRecord bk13 = null;

				foreach (AbstractCreditPublicRecord o in EffectivePublicRecords) 
				{
					try 
					{
						switch(o.Type)
						{
							case E_CreditPublicRecordType.BankruptcyChapter7:
							{
								if(bk7 == null)
								{
									bk7 = o;
									continue;
								}
								if(!Tools.IsDateDiffWithin(bk7.BkFileDate, 1, o.BkFileDate))
								{
									TagItem(key, bk7);
									TagItem(key, o);

									InsertCache(key, E_BkMultipleT.Multiple);
									return E_BkMultipleT.Multiple;
								}
								break;
							}

							case E_CreditPublicRecordType.BankruptcyChapter11:
							{
								if(bk11 == null)
								{
									bk11 = o;
									continue;
								}

								if(!Tools.IsDateDiffWithin(bk11.BkFileDate, 1, o.BkFileDate))
								{
									TagItem(key, bk11);
									TagItem(key, o);

									InsertCache(key, E_BkMultipleT.Multiple);
									return E_BkMultipleT.Multiple;
								}
								break;
							}

							case E_CreditPublicRecordType.BankruptcyChapter12:
							{
								if(bk12 == null)
								{
									bk12 = o;
									continue;
								}

								if(!Tools.IsDateDiffWithin(bk12.BkFileDate, 1, o.BkFileDate))
								{
									TagItem(key, bk12);
									TagItem(key, o);

									InsertCache(key, E_BkMultipleT.Multiple);
									return E_BkMultipleT.Multiple;
								}
								break;
							}

							case E_CreditPublicRecordType.BankruptcyChapter13:
							{
								if(bk13 == null)
								{
									bk13 = o;
									continue;
								}

								if(!Tools.IsDateDiffWithin(bk13.BkFileDate, 1, o.BkFileDate))
								{
									TagItem(key, bk13);
									TagItem(key, o);

									InsertCache(key, E_BkMultipleT.Multiple);
									return E_BkMultipleT.Multiple;
								}
								break;

							} // case
						} // switch 

					} // try
					catch(Exception ex)
					{
						LogCreditWarning(ex);
					}
				}  // for each                   

				int count = 0;
				if(bk7 != null)
					++count;
				if(bk11 != null)
					++count;
				if(bk12 != null)
					++count;
				if(bk13 != null)
					++count;

				// Rollover is possible  when there is bk13 filed before bk7. Rollover happens is when someone filing a BK13
				// then realizing that they've made a mistake and can't really go on a payment plan, roll it over to a BK7.
				if(count == 2)
				{
					if(bk13 != null && bk7 != null && bk13.BkFileDate.CompareTo(bk7.BkFileDate) <= 0)
					{
						TagItem(key, bk13);
						TagItem(key, bk7);
						InsertCache(key, E_BkMultipleT.PossibleRollOver);
						return E_BkMultipleT.PossibleRollOver;
					}
				}

				if(count >= 2)
				{
					if(bk7 != null)
						TagItem(key, bk7);
					if(bk11 != null)
						TagItem(key, bk11);
					if(bk12 != null)
						TagItem(key, bk12);
					if(bk13 != null)
						TagItem(key, bk13);

					InsertCache(key, E_BkMultipleT.Multiple);
					return E_BkMultipleT.Multiple;
				}
              
				InsertCache(key, E_BkMultipleT.None);
				return E_BkMultipleT.None; 
			} // get
		}

		private int HasBkOfTypeFiledWithin(E_BkType types, int nMonths, string sCacheKey)
		{
			foreach (AbstractCreditPublicRecord o in EffectivePublicRecords) 
			{
				try 
				{
					if(o.IsBkOfTypeAndCharacter(types, E_BkChar.Any)) 
					{
						DateTime dt = o.BkFileDate;
						if(Tools.IsDateWithin(dt, nMonths)) 
						{
							TagItem(sCacheKey, o);
							return 1;
						}
					}
				} 
				catch (Exception exc) 
				{
					LogCreditWarning(exc);

				}
			}
			return 0;
		}


		virtual public int HasBkFiledWithin(int nMonths)
		{
			string key = "HasBkFiledWithin:" + nMonths;
			if (HasCache(key))
				return GetCacheInt(key);

			int count = HasBkOfTypeFiledWithin(E_BkType.Any, nMonths, key);

			InsertCache(key, count);
			return count;

		}


		virtual public int Has7BkFiledWithin(int nMonths)
		{
			string key = "Has7BkFiledWithin:" + nMonths;
			if (HasCache(key))
				return GetCacheInt(key);


			int count = HasBkOfTypeFiledWithin(E_BkType.Ch7, nMonths, key);

			InsertCache(key, count);
			return count;

		}


		virtual public int Has11BkFiledWithin(int nMonths)
		{
			string key = "Has11BkFiledWithin:" + nMonths;
			if (HasCache(key))
				return GetCacheInt(key);


			int count = HasBkOfTypeFiledWithin(E_BkType.Ch11, nMonths, key);

			InsertCache(key, count);
			return count;

		}


		virtual public int Has12BkFiledWithin(int nMonths)
		{
			string key = "Has12BkFiledWithin:" + nMonths;
			if (HasCache(key))
				return GetCacheInt(key);

			int count = HasBkOfTypeFiledWithin(E_BkType.Ch12, nMonths, key);

			InsertCache(key, count);
			return count;

		}


		virtual public int Has13BkFiledWithin(int nMonths)
		{
			string key = "Has13BkFiledWithin:" + nMonths;
			if (HasCache(key))
				return GetCacheInt(key);

			int count = HasBkOfTypeFiledWithin(E_BkType.Ch13, nMonths, key);

			InsertCache(key, count);
			return count;

		}


		virtual public int HasUnknownTypeBkFiledWithin(int nMonths)
		{
			string key = "HasUnknownTypeBkFiledWithin:" + nMonths;
			if (HasCache(key))
				return GetCacheInt(key);


			int count = HasBkOfTypeFiledWithin(E_BkType.UnknownType, nMonths, key);

			InsertCache(key, count);
			return count;

		}

		private AbstractCreditPublicRecord LastBkOfType(E_BkType type)
		{
			DateTime lastEffectiveD = DateTime.MinValue;
			AbstractCreditPublicRecord lastBk = null;

			foreach (AbstractCreditPublicRecord o in EffectivePublicRecords) 
			{
				try 
				{
					if(o.IsBkOfTypeAndCharacter(type, E_BkChar.Any)) 
					{
						DateTime led = o.LastEffectiveDate;
						if(led.CompareTo(lastEffectiveD) > 0)
						{
							lastBk = o;
							lastEffectiveD = led;
						}
					}
				} 
				catch (Exception exc) 
				{
					LogCreditWarning(exc);
				}
			}
			return lastBk;
		}

		public CDateTime DischargedDOfLastBkOfType(E_BkType type)
		{
			AbstractCreditPublicRecord lastBk = LastBkOfType(type);

			if(lastBk == null || !lastBk.IsDischarged)
				return CDateTime.InvalidWrapValue;

			return CDateTime.Create(lastBk.DispositionDate);
		}

		public CDateTime FileDOfLastBkOfType(E_BkType type)
		{
			AbstractCreditPublicRecord lastBk = LastBkOfType(type);

			if(lastBk == null)
				return CDateTime.InvalidWrapValue;

			return CDateTime.Create(lastBk.BkFileDate);
		}

		#endregion // NEW BK METHODS

        #region IMPLEMENTING BankAegis

    

		/// <summary>
		/// Text: No derog in past 15 months, including 30 day accts, BK, judgments, chargeoffs, CCC 
		/// </summary>
		/// <param name="nMonths"></param>
		/// <returns></returns>

		virtual public int HasDerogCountEffectiveWithin(int nMonths)
		{
			string key = "HasDerogCountEffectiveWithin:" + nMonths;
			if (HasCache(key))
				return GetCacheInt(key);

			int count = 0;
			DateTime currentDate = DateTime.Today;
			foreach(AbstractCreditPublicRecord o in EffectivePublicRecords)
			{
				try
				{
					if(Tools.IsDateWithin(o.LastEffectiveDate, nMonths, currentDate)) 
					{
						TagItem(key, o);
						++count;
					}
				}
				catch(Exception ex)
				{
					LogCreditWarning(ex);
				}
			}

			foreach(AbstractCreditLiability o in EffectiveLiabilities)
			{
				try
				{
					if(o.HasBadHistoryEffectiveWithin(nMonths, /* bIncPaidAccs */ true)) 
					{
						TagItem(key, o);
						++count;
					}
				}
				catch(Exception ex)
				{
					LogCreditWarning(ex);
				}
			}

			InsertCache(key, count);
			return count;

		}


        [CreditMigrated]
		virtual public int TradesCountNonMajorDerog
		{
			get 
			{
				string methodName = "TradesCountNonMajorDerog";
				string key = "TradesCountNonMajorDerog";
				if (HasCache(key))
					return GetCacheInt(key);

				int count = 0;
				foreach(AbstractCreditLiability o in EffectiveLiabilities)
				{
					try
					{
						if(!o.IsBankruptcy && !o.HasMajorDerogEffectiveWithin(new E_DerogT[] {  E_DerogT.Repossession, 
											   E_DerogT.Foreclosure, 
											   E_DerogT.ChargeOff,
											   E_DerogT.Collection,
											   E_DerogT.TaxLien,
											   E_DerogT.Judgment },  1000, /* bIncPaidAccs */ true)) 
						{
							TagItem(methodName, o);
							++count;
						}
					}
					catch(Exception ex)
					{
						LogCreditWarning(ex);
					}
				}

				InsertCache(key, count);
				return count;
			}
		}

		/// <summary>
		/// Text: No derog in past 15 months, BK, judgments, chargeoffs, CCC. This one doesn't consider lates as derog.
		/// </summary>
		/// <param name="nMonths"></param>
		/// <returns></returns>

		virtual public int HasMajorDerogCountEffectiveWithin(int nMonths)
		{
			string key = "HasMajorDerogCountEffectiveWithin:" + nMonths;
			if (HasCache(key))
				return GetCacheInt(key);

			int count = 0;
			DateTime currentDate = DateTime.Today;
			foreach(AbstractCreditPublicRecord o in EffectivePublicRecords)
			{
				try
				{
					if(Tools.IsDateWithin(o.LastEffectiveDate, nMonths, currentDate)) 
					{
						TagItem(key, o);
						++count;
					}
				}
				catch(Exception ex)
				{
					LogCreditWarning(ex);
				}
			}

			foreach(AbstractCreditLiability o in EffectiveLiabilities)
			{
				try
				{
					if(o.HasMajorDerogEffectiveWithin(new E_DerogT[] {  E_DerogT.Repossession, 
										   E_DerogT.Foreclosure, 
										   E_DerogT.ChargeOff,
										   E_DerogT.Collection,
										   E_DerogT.TaxLien,
										   E_DerogT.Judgment },  nMonths, /* bIncPaidAccs */ true)) 
					{
						TagItem(key, o);
						++count;
					}
				}
				catch(Exception ex)
				{
					LogCreditWarning(ex);
				}
			}

			InsertCache(key, count);
			return count;

		}

		// 6/15/2006 - nw - OPM 4822, nMonths is not used yet, will be used later.
		// The return type should be an int instead of a bool so we can add this method into SymbolTable.
		virtual public int MightHaveUndated120Within(int nMonths)
		{
			string key = "MightHaveUndated120Within:" + nMonths;
			if (HasCache(key))
				return GetCacheInt(key);

			int ret = 0;

			foreach (AbstractCreditLiability o in EffectiveLiabilities)
			{
				try
				{
					if (o.ForeclosureWasStarted)
					{
						TagItem(key, o);
						ret = 1;
						break;
					}
				}
				catch (Exception ex)
				{
					LogCreditWarning(ex);
				}
			}

			InsertCache(key, ret);
			return ret;
		}

		private E_DerogT[]	m_bankAegisTradelineDerogTypes = new E_DerogT[] { E_DerogT.ChargeOff, E_DerogT.Collection };
		private E_DerogT[] BankAegisTradelineDerogTypes
		{
			get
			{
				return m_bankAegisTradelineDerogTypes;
			}
		}

		/// <summary>
		/// Return ArrayList of string[3] elements: name, op, and value (all in lower case)
		/// </summary>
		/// <param name="pars"></param>
		/// <returns></returns>
		private static ArrayList ParseStringParameters(string pars)
		{
			// Supporting operators are : <, <=, >, >=, =
			char[] op_chars = new  char[] { '<', '>', '=' }; // TODO: Consider moving this outside to re-use.

			ArrayList res = new ArrayList(15);
		
			foreach(string attribute in pars.ToLower().Split('&'))
			{
				if(attribute == "," || attribute == "")
					continue;

				string name = "";
				string op = "";
				string val = "";
				
				// Extracting name
				// 12/21/2006 nw - OPM 5606 - Handle mistyped parameters/attributes
				int nameLen = attribute.IndexOfAny(op_chars);
				try
				{
					name = attribute.Substring(0, nameLen);
				}
				catch	// must be an invalid format, log bug below and throw exception in the calling method's switch statement
				{
					name = attribute;
				}

				// Extracting operator
				StringBuilder opBuilder = new StringBuilder(5);
				Match match = System.Text.RegularExpressions.Regex.Match(attribute, "[<>=]");
				while(match.Success)
				{
					string token = match.Groups[0].Value;
					opBuilder.Append(token);
					match = match.NextMatch();
				}
				op = opBuilder.ToString();

				// Extracting value
				try
				{
					val = attribute.Substring(nameLen + op.Length);
				}
				catch{} // must be an invalid format 

				if(name == "" || op == "" || val == "")
					Tools.LogBug(string.Format("None of these can be empty. Name={0}. Op={1}. Val={2}", name, op, val));

				res.Add(new string[3] { name, op, val });
			}

			return res;
		}


		virtual public int MajorDerogCountX(string parameters)
		{
			return EvaluateMajorDerogCountX(parameters, this);
		}

		static public  int EvaluateMajorDerogCountX(string parameters, AbstractCreditReport creditReport)
		{
			string key = "MajorDerogCountX:" + parameters.ToLower();
			
			if (creditReport != null && creditReport.HasCache(key))
				return creditReport.GetCacheInt(key);

			// Definition of Derog: Definition
			string defOp = ">="; 
			string defVal = "90"; // 30,60,90,120,150,180,dead
			E_AdverseType lates = E_AdverseType.Late90Plus;		// 2/14/2007 nw - OPM 10388

			// Effective Date: EffectiveDate
			string effDateOp = "<="; // "<=" means "within"
			int effDateVal = 1000;

			// OPM 9759 - New parameters
			// Only count derog events after the latest BK/FC: CountAfterBkFcOnly
			string CountAfterBkFcOnlyOp = "=";
			bool CountAfterBkFcOnlyVal = false;

			// If CountAfterBkFcOnly == y, count Late120+ as FC: Count120AsFc
			string Count120AsFcOp = "=";
			bool Count120AsFcVal = false;

			// Tradeline Type: T
			string typeOp = "="; // must be =, we can support != in the future
			string typeVal = "irmouplh"; // 'i', 'r', 'm', 'o', 'u', 'l', 'h', 'p' and any combination. 'o': open, 'u': unknown, other, 'p': public record.

            // High credit: HighCredit
            string highCreditOp = ">="; // >, >=
            int highCreditVal = 0; // default to get ALL

			#region PARSING PARAMETERS
			foreach(string[] par in ParseStringParameters(parameters))
			{
				switch(par[ 0 ])
				{
					case "definition":
						
						defOp = par[1];
						if(defOp != ">=") 
							throw CreateGenericExceptionWithDevMessage(string.Format("{0} is not a supported operator for parameter Definition in keyword MajorDerogCountX", defOp));
						defVal = par[2];
					switch(defVal)
					{
						case "30":		lates = E_AdverseType.Late30 | E_AdverseType.Late60 | E_AdverseType.Late90Plus; break;
						case "60":		lates = E_AdverseType.Late60 | E_AdverseType.Late90Plus; break;
						case "90":		lates = E_AdverseType.Late90Plus; break;
						case "120":		lates = E_AdverseType.Late120 | E_AdverseType.Late150 | E_AdverseType.Late180; break;
						case "150":		lates = E_AdverseType.Late150 | E_AdverseType.Late180; break;
						case "180":		lates = E_AdverseType.Late180; break;
						case "dead":	lates = E_AdverseType.None;
							break;
						default:
							throw CreateGenericExceptionWithDevMessage(string.Format("{0} is not a supported argument for parameter Definition in keyword MajorDerogCountX", defVal));
					}
						break;
					case "effectivedate":
						effDateOp = par[1];
						if(effDateOp != "<=")
							throw CreateGenericExceptionWithDevMessage(string.Format("{0} is not a supported operator for parameter EffectiveDate in keyword MajorDerogCountX", effDateOp));
						try
						{ 
							effDateVal = int.Parse(par[2]); 
							if(effDateVal < 0)
								throw CreateGenericExceptionWithDevMessage("Effective date (effDateVal) was < 0.");
						}
						catch
						{
							throw CreateGenericExceptionWithDevMessage(string.Format("{0} is not a supported argument for parameter EffectiveDate in keyword MajorDerogCountX ",  par[2]));
						}
						break;
                    case "highcredit":
                        highCreditOp = par[1];
                        switch (highCreditOp)
                        {
                            case ">=":
                            case ">":

                                break;
                            default:
                                throw CreateGenericExceptionWithDevMessage(string.Format("{0} is not a supported operator for parameter HighCredit in keyword TradeCountX", par[1]));
                        }

                        try
                        {
                            highCreditVal = int.Parse(par[2]);
                            if (highCreditVal < 0)
                                throw CreateGenericExceptionWithDevMessage("High credit (highCreditVal) was < 0.");
                        }
                        catch
                        {
                            throw CreateGenericExceptionWithDevMessage(string.Format("{0} is not a supported argument for parameter HighCredit in keyword TradeCountX", par[2]));
                        }
                        break;

					case "countafterbkfconly":
						CountAfterBkFcOnlyOp = par[1];
						if(CountAfterBkFcOnlyOp != "=")
							throw CreateGenericExceptionWithDevMessage(string.Format("{0} is not a supported operator for parameter CountAfterBkFcOnly in keyword MajorDerogCountX", CountAfterBkFcOnlyOp));

					switch(par[2])
					{
						case "y": CountAfterBkFcOnlyVal = true; break;
						case "n": CountAfterBkFcOnlyVal = false; break;
						default: throw CreateGenericExceptionWithDevMessage(string.Format("{0} is not a supported argument for parameter CountAfterBkFcOnly in keyword MajorDerogCountX", par[2]));
					}
						break;
					case "count120asfc":
						Count120AsFcOp = par[1];
						if(Count120AsFcOp != "=")
							throw CreateGenericExceptionWithDevMessage(string.Format("{0} is not a supported operator for parameter Count120AsFc in keyword MajorDerogCountX", Count120AsFcOp));

					switch(par[2])
					{
						case "y": Count120AsFcVal = true; break;
						case "n": Count120AsFcVal = false; break;
						default: throw CreateGenericExceptionWithDevMessage(string.Format("{0} is not a supported argument for parameter Count120AsFc in keyword MajorDerogCountX", par[2]));
					}
						break;
					case "t":
						typeOp = par[1];
						if(typeOp != "=")
							throw CreateGenericExceptionWithDevMessage(string.Format("{0} is not a supported operator for parameter T in keyword MajorDerogCountX", typeOp));

						typeVal = par[2];
						int len = typeVal.Length;
						for(int i = 0; i < len; ++i)
						{
							switch(typeVal[i])
							{
								case 'i':
								case 'r':
								case 'm':
								case 'o':
								case 'u':
								case 'p':
                                case 'l':
                                case 'h':
									break;
								default:
									throw CreateGenericExceptionWithDevMessage(string.Format("{0} character is not a supported argument for parameter T in keyword MajorDerogCountX", typeVal[i].ToString()));
							}
						}
						break;
					default:
						throw CreateGenericExceptionWithDevMessage(string.Format("{0} is not a supported parameter in keyword MajorDerogCountX ",  par[0]));
				}
			}																													 
			#endregion // PARSING PARAMETERS

			if(creditReport == null)
				return 0;

			#region Latest BK/FC Date Info
			DateTime bkDateLatest = DateTime.MinValue;
			DateTime fcDateLatest = DateTime.MinValue;
			DateTime bkfcDateLatest = DateTime.MinValue;
			if(CountAfterBkFcOnlyVal)
			{
				foreach (AbstractCreditPublicRecord o in creditReport.EffectivePublicRecords) 
				{
					if(o.IsBkOfTypeAndCharacter(E_BkType.Any, E_BkChar.Any))
					{
						DateTime bkDate = DateTime.MinValue;

						if(o.DispositionDate != DateTime.MinValue)
							bkDate = o.DispositionDate;
							// 11/29/2006 nw - External OPM 17907 - some pub rec use Settled Date instead of Disposition Date
						else if(o.LastEffectiveDate != DateTime.MinValue && o.LastEffectiveDate != DateTime.Today)
							bkDate = o.LastEffectiveDate;
						else if(o.BkFileDate != DateTime.MinValue)
							bkDate = o.BkFileDate;

						if(bkDate != DateTime.MinValue && bkDateLatest.CompareTo(bkDate) < 0)
						{
							bkDateLatest = bkDate;
						}
					}

					if (o.Type == E_CreditPublicRecordType.Foreclosure)
					{
						DateTime fcDate = DateTime.MinValue;

						if(o.DispositionDate != DateTime.MinValue)
							fcDate = o.DispositionDate;
						else if(o.LastEffectiveDate != DateTime.MinValue && o.LastEffectiveDate != DateTime.Today)
							fcDate = o.LastEffectiveDate;
						else if(o.BkFileDate != DateTime.MinValue)
							fcDate = o.BkFileDate;	// the name is somewhat misleading but it's safe to use here

						if(fcDate != DateTime.MinValue && fcDateLatest.CompareTo(fcDate) < 0)
						{
							fcDateLatest = fcDate;
						}
					}
				}

				foreach (AbstractCreditLiability o in creditReport.EffectiveLiabilities)
				{
					try
					{
						// Look for Mortgage tradelines only
						if (!o.IsTypeMortgage)
							continue;

                        // 12/7/2009 dd - OPM 43147 - Support HighCredit
                        if (highCreditOp == ">=")
                        {
                            if (o.HighCreditAmount < highCreditVal)
                            {
                                continue;
                            }
                        }
                        else if (highCreditOp == ">")
                        {
                            if (o.HighCreditAmount <= highCreditVal)
                            {
                                continue;
                            }
                        }

						DateTime fcDate = o.GetMajorForeclosureEffectiveWithin(effDateVal,  true);
						
						if(fcDate != DateTime.MinValue && fcDateLatest.CompareTo(fcDate) < 0)
						{
							fcDateLatest = fcDate;
						}

						E_AdverseType lateVal = E_AdverseType.RepossessionOrForeclosure;

						if (Count120AsFcVal)
							lateVal = lateVal | E_AdverseType.Late120 | E_AdverseType.Late150 | E_AdverseType.Late180;
						
						if (o.IsFirstPmtTypeWithin(lateVal, effDateVal))
						{
							fcDate = o.DateOfLastestAdversePmtType(lateVal);
							if(fcDate != DateTime.MinValue && fcDateLatest.CompareTo(fcDate) < 0)
							{
								fcDateLatest = fcDate;
							}
						}
					}
					catch(Exception ex)
					{
						creditReport.LogCreditWarning(ex);
					}
				}

				bkfcDateLatest = (bkDateLatest.CompareTo(fcDateLatest) < 0) ? fcDateLatest : bkDateLatest;
				// 4/13/2007 nw - OPM 9759 - If CountAfterBkFcOnlyVal=Y and borrower has no BK/FC, return 0
				if (bkfcDateLatest == DateTime.MinValue)
				{
					creditReport.InsertCache(key, 0);
					return 0;
				}
			}
			#endregion // Latest BK/FC Date Info

			int count = 0;
			DateTime currentDate = DateTime.Today;

			if (typeVal.IndexOf('p') >= 0)
			{
				foreach(AbstractCreditPublicRecord o in creditReport.EffectivePublicRecords)
				{
					try
					{
						if(Tools.IsDateWithin(o.LastEffectiveDate, effDateVal, currentDate)) 
						{
							// 2/16/2007 nw - OPM 9759 - Check if derog event is after the latest BK/FC date
                            var date = o.BkFileDate == DateTime.Today ? o.LastEffectiveDate : o.BkFileDate;         //Opm 49303 Prefer the Bk File Date over the last effective Date 
                            if (CountAfterBkFcOnlyVal && date.CompareTo(bkfcDateLatest) <= 0)
								continue;

							creditReport.TagItem(key, o);
							++count;
						}
					}
					catch(Exception ex)
					{
						creditReport.LogCreditWarning(ex);
					}
				}
			}

			foreach(AbstractCreditLiability o in creditReport.EffectiveLiabilities)
			{
				#region parameter:T
					
				if(!o.IsOfType(typeVal))
					continue;

				#endregion // paremeter:T

                // 12/7/2009 dd - OPM 43147 - Support HighCredit
                if (highCreditOp == ">=")
                {
                    if (o.HighCreditAmount < highCreditVal)
                    {
                        continue;
                    }
                }
                else if (highCreditOp == ">")
                {
                    if (o.HighCreditAmount <= highCreditVal)
                    {
                        continue;
                    }
                }
				try
				{
					int nOldCount = count;
					if(o.IsLateWithin(effDateVal, lates))
					{
						if (CountAfterBkFcOnlyVal)
						{
							// if tradeline open date was after BK/FC date, count tradeline
							if (o.AccountOpenedDate.CompareTo(bkfcDateLatest) > 0)
							{
								creditReport.TagItem(key, o);
								++count;
								continue;
							}

							foreach (CreditAdverseRating rating in o.LateOnlyCreditAdverseRatingList)
							{
								if (0 != (rating.AdverseType & lates)) 
								{
									if (Tools.IsDateWithin(rating.Date, effDateVal) && rating.Date.CompareTo(bkfcDateLatest) > 0)
									{
										creditReport.TagItem(key, o);
										++count;
									}
									break;	// CreditAdverseRatingList is sorted, so if the first match doesn't satisfy 
									// the date criteria, the rest won't either
								}
							}
						}
						else
						{
							creditReport.TagItem(key, o);
							++count;
						}
					}

					if (nOldCount != count)
						continue;

					if(o.HasMajorDerogEffectiveWithin(new E_DerogT[] {  E_DerogT.Repossession, 
										   E_DerogT.Foreclosure, 
										   E_DerogT.ChargeOff,
										   E_DerogT.Collection,
										   E_DerogT.TaxLien,
										   E_DerogT.Judgment },  effDateVal, /* bIncPaidAccs */ true)) 
					{
						// 2/16/2007 nw - OPM 9759 - Check if derog event is after the latest BK/FC date
						if (CountAfterBkFcOnlyVal)
						{
							// if we don't know the date of the current status, we have to let it slide
							if (o.CreditAdverseRatingList.Count == 0)
								continue;
							//external OPM 44960 - We will ignore tradelines that are part of a discharged BK 
							//(if that can be determined)
							if(o.IsDischargedBk)
								continue;
							CreditAdverseRating mostRecentRating = (CreditAdverseRating) o.CreditAdverseRatingList[0];
							
							if (mostRecentRating.Date.CompareTo(bkfcDateLatest) <= 0)
								continue;
							//OPM 49739 - check that the adverse type of the derog that is found after the foreclosure
							//is greater than or equal to the "definition" value (which becomes the "lates" variable)
							if(lates == E_AdverseType.None) //definition >= dead
							{		
								if(mostRecentRating.AdverseType < E_AdverseType.AllLate)
									continue;
							}
							else
							{
								if(mostRecentRating.AdverseType < lates)
									continue;
							}
						}
						creditReport.TagItem(key, o);
						++count;
					}
						// 2/14/2007 nw - OPM 10388, some currently BK tradelines are missed above, catch them here
						// else if (o.IsBankruptcy && o.IsFirstPmtTypeWithin(E_AdverseType.Bankruptcy, effDateVal))
					else if (o.IsBankruptcy && Tools.IsDateWithin(o.AccountReportedDate, effDateVal))
					{
						// 2/16/2007 nw - OPM 9759 - Check if derog event is after the latest BK/FC date
						if (CountAfterBkFcOnlyVal)
						{
							// if we don't know the date of the current status, we have to let it slide
							if (o.CreditAdverseRatingList.Count == 0)
								continue;
							//External OPM 42421 - if Date of Last Activity is older than latest bk date, let it slide
							if (o.LastActivityDate.CompareTo(bkfcDateLatest) <= 0)
								continue;
							CreditAdverseRating mostRecentRating = (CreditAdverseRating) o.CreditAdverseRatingList[0];
							if (mostRecentRating.Date.CompareTo(bkfcDateLatest) <= 0)
								continue;
						}

						creditReport.TagItem(key, o);
						++count;
					}
					else if (CountAfterBkFcOnlyVal && o.IsTypeMortgage)
					{
						E_AdverseType latesToMatch = E_AdverseType.RepossessionOrForeclosure;
						if (Count120AsFcVal)
							latesToMatch = latesToMatch | E_AdverseType.Late120 | E_AdverseType.Late150 | E_AdverseType.Late180;

						foreach (CreditAdverseRating rating in o.CreditAdverseRatingList) 
						{
							if (0 != (rating.AdverseType & latesToMatch)) 
							{
								if (Tools.IsDateWithin(rating.Date, effDateVal) && rating.Date.CompareTo(bkfcDateLatest) > 0)
								{
									creditReport.TagItem(key, o);
									++count;
								}
								break;	// CreditAdverseRatingList is sorted, so if the first match doesn't satisfy 
								// the date criteria, the rest won't either
							}
						}
					}
				}
				catch(Exception ex)
				{
					creditReport.LogCreditWarning(ex);
				}
			}

			creditReport.InsertCache(key, count);
			return count;

		}

		/// <summary>
		/// Text: No judgments or tax Liens past 24 months
		/// </summary>
		/// <param name="nMonths"></param>
		/// <returns></returns>

		public int BankAegisHasJudgmentOrTaxLienWithin(int nMonths)
		{
			string key = "BankAegisHasJudgmentOrTaxLienWithin:" + nMonths;

			if (HasCache(key))
				return GetCacheInt(key);


			DateTime currentDate = DateTime.Today;
			int ret = 0;
			foreach(AbstractCreditPublicRecord o in EffectivePublicRecords)
			{
				try
				{
					if(o.IsJudgment || o.IsTaxLien)
					{
						if(Tools.IsDateWithin(o.LastEffectiveDate, nMonths, currentDate)) 
						{
							TagItem(key, o);
							ret = 1;
							break;
						}
					}
				}
				catch(Exception ex)
				{
					LogCreditWarning(ex);
				}
			}

			InsertCache(key, ret);
			return ret;

		}


		#endregion // BankAegis

        #region IMPLEMENTING BankImpact

		private E_DerogT[]	m_bankImpactTradelineDerogTypes = new E_DerogT[] { E_DerogT.ChargeOff, E_DerogT.Collection, E_DerogT.Repossession };
		private E_DerogT[] BankImpactTradelineDerogTypes
		{
			get
			{
				return m_bankImpactTradelineDerogTypes;
			}
		}

		/// <summary>
		/// Example: Minimum of 3 trades (All lates based on rolling)
		/// -> Don't know what to do with the one in the parenthesis
		/// <param name="nMonths"></param>
		/// <returns></returns>
        [CreditMigrated]
		virtual public int TradesOpenCountOlderThan(int nMonths)
		{
			string key = "TradesOpenCountOlderThan:" + nMonths;

			if (HasCache(key))
				return GetCacheInt(key);

			DateTime nowD = DateTime.Today;
			int count = 0;
			
			foreach(AbstractCreditLiability o in EffectiveLiabilities) 
			{
				try
				{
					if(o.IsCredit && o.IsCurrent)
					{
						// OPM# 3236
						if(o.IsBankruptcy || o.IsForeclosure || o.IsRepos || o.IsChargeOff || o.IsCollection)
							continue;

						if (o.AccountOpenedDate != DateTime.MinValue) 
						{
							if(!Tools.IsDateWithin(o.AccountOpenedDate, nMonths, nowD))
							{
								TagItem(key, o);
								++count;
							}
						}
					}
				}
				catch(Exception ex)
				{
					LogCreditWarning(ex);
				}
			}

			InsertCache(key, count);
			return count;
		}



		/// <summary>
		/// Text: Any medical collections with a cumulative balance over $500 requires payment prior to or at closing
		/// </summary>
		/// <param name="amount"></param>
		/// <returns></returns>
        [CreditMigrated]
		virtual public int TotBalOfMedCollectionsUnpaid
		{	
			get
			{
				string methodName = "TotBalOfMedCollectionsUnpaid";

				if (HasCache(methodName))
					return GetCacheInt(methodName);

				// TODO: How can we tell a pub rec if it's med related?

				decimal balTot = 0;
				foreach(AbstractCreditLiability o in EffectiveLiabilities)
				{
					try
					{
						if (o.IsCollection && !o.IsPaid && o.IsMedicalTradeline) 
						{
							TagItem(methodName, o);
							balTot += o.UnpaidBalanceAmount;
						}
					}
					catch(Exception ex)
					{
						LogCreditWarning(ex);
					}
				}

				InsertCache(methodName, (int) balTot);
				return (int) balTot;
			}
		}

		// 4/17/2007 nw - OPM 12499 - New medical collection keyword
		virtual public int TotUnpaidBalOfMedCollectionsWithin(int nMonths)
		{
			string key = "TotUnpaidBalOfMedCollectionsWithin:" + nMonths;

			if (HasCache(key))
				return GetCacheInt(key);

			// TODO: How can we tell if a pub rec is medical related?

			decimal balTot = 0;
			foreach(AbstractCreditLiability o in EffectiveLiabilities)
			{
				try
				{
					if (o.IsCollection && !o.IsPaid && o.IsMedicalTradeline)
					{
						if (Tools.IsDateWithin(o.EndDerogDate, nMonths))
						{
							TagItem(key, o);
							balTot += o.UnpaidBalanceAmount;
						}
					}
				}
				catch(Exception ex)
				{
					LogCreditWarning(ex);
				}
			}

			InsertCache(key, (int) balTot);
			return (int) balTot;
		}

		/// <summary>
		/// Text: No Tax Liens in the past 24/12 months
		/// </summary>
		/// <param name="nMonths"></param>
		/// <returns></returns>
		virtual public int BankImpactHasTaxLienWithin(int nMonths)
		{
			string key = "BankImpactHasTaxLienWithin:" + nMonths;

			if (HasCache(key))
				return GetCacheInt(key);

			int ret = 0;
			foreach(AbstractCreditPublicRecord o in EffectivePublicRecords)
			{
				try
				{
					if(o.IsTaxLien)
					{
						if(Tools.IsDateWithin(o.LastEffectiveDate, nMonths)) 
						{
							TagItem(key, o);
							ret = 1;
							break;
						}
					}
				}
				catch(Exception ex)
				{
					LogCreditWarning(ex);
				}
			}

			InsertCache(key, ret);
			return ret;
		}

		/// <summary>
		/// 7/6/2006 nw - OPM 6120 - new keyword "HasTaxLienFiledWithin"
		/// Text: No Tax Liens Filed in the past 24/12 months
		/// </summary>
		/// <param name="nMonths"></param>
		/// <returns></returns>
		virtual public int BankImpactHasTaxLienFiledWithin(int nMonths)
		{
			string key = "BankImpactHasTaxLienFiledWithin:" + nMonths;

			if (HasCache(key))
				return GetCacheInt(key);

			int ret = 0;
			foreach(AbstractCreditPublicRecord o in EffectivePublicRecords)
			{
				try
				{
					if(o.IsTaxLien)
					{
						if(Tools.IsDateWithin(o.BkFileDate, nMonths)) 
						{
							TagItem(key, o);
							ret = 1;
							break;
						}
					}
				}
				catch(Exception ex)
				{
					LogCreditWarning(ex);
				}
			}

			InsertCache(key, ret);
			return ret;
		}

		#endregion // BankImpact

        #region GENERIC - LATE METHODS


		virtual public int GetInstallmentLates30IntermittentProgressiveRollingCountWithin(int nMonths)
		{
			string key = "GetInstallmentLates30IntermittentProgressiveRollingCountWithin:" + nMonths;
			if (HasCache(key))
				return GetCacheInt(key);


			int count = GetLatesIntermittentProgressiveCountWithin(nMonths, E_DebtRegularT.Installment, E_AdverseType.Late30, true, key); 

			InsertCache(key, count);
			return count;

		}


		virtual public int GetInstallmentLates30IntermittentProgressiveNonrollingCountWithin(int nMonths)
		{
			string key = "GetInstallmentLates30IntermittentProgressiveNonrollingCountWithin:" + nMonths;
			if (HasCache(key))
				return GetCacheInt(key);


			int count = GetLatesIntermittentProgressiveCountWithin(nMonths, E_DebtRegularT.Installment, E_AdverseType.Late30, false, key); 

			InsertCache(key, count);
			return count;

		}


		virtual public int GetRevolvingLates30IntermittentProgressiveRollingCountWithin(int nMonths)
		{
			string key = "GetRevolvingLates30IntermittentProgressiveRollingCountWithin:" + nMonths;
			if (HasCache(key))
				return GetCacheInt(key);

			int count = GetLatesIntermittentProgressiveCountWithin(nMonths, E_DebtRegularT.Revolving, E_AdverseType.Late30, true, key); 

			InsertCache(key, count);
			return count;

		}


		virtual public int GetRevolvingLates30IntermittentProgressiveNonrollingCountWithin(int nMonths)
		{
			string key = "GetRevolvingLates30IntermittentProgressiveNonrollingCountWithin:" + nMonths;
			if (HasCache(key))
				return GetCacheInt(key);

			int count = GetLatesIntermittentProgressiveCountWithin(nMonths, E_DebtRegularT.Revolving, E_AdverseType.Late30, false, key); 

			InsertCache(key, count);
			return count;

		}


		virtual public int GetMortgageLates30IntermittentProgressiveRollingCountWithin(int nMonths)
		{
			string key = "GetMortgageLates30IntermittentProgressiveRollingCountWithin:" + nMonths;
			if (HasCache(key))
				return GetCacheInt(key);


			int count = GetLatesIntermittentProgressiveCountWithin(nMonths, E_DebtRegularT.Mortgage, E_AdverseType.Late30, true, key);

			InsertCache(key, count);
			return count;

		}

		virtual public int GetMortgageLates60IntermittentProgressiveRollingCountWithin(int nMonths)
		{
			string key = "GetMortgageLates60IntermittentProgressiveRollingCountWithin:" + nMonths;
			if (HasCache(key))
				return GetCacheInt(key);

			int count = GetLatesIntermittentProgressiveCountWithin(nMonths, E_DebtRegularT.Mortgage, E_AdverseType.Late60, true, key);

			InsertCache(key, count);
			return count;

		}

		virtual public int GetMortgageLates90IntermittentProgressiveRollingCountWithin(int nMonths) 
		{
			string key = "GetMortgageLates90IntermittentProgressiveRollingCountWithin:" + nMonths;
			if (HasCache(key)) 
				return GetCacheInt(key);

			int count = GetLatesIntermittentProgressiveCountWithin(nMonths, E_DebtRegularT.Mortgage, E_AdverseType.Late90, true, key);

			InsertCache(key, count);
			return count;
		}


		virtual public int GetRevolvingLates60IntermittentProgressiveNonrollingCountWithin(int nMonths)
		{
			string key = "GetRevolvingLates60IntermittentProgressiveNonrollingCountWithin:" + nMonths;
			if (HasCache(key))
				return GetCacheInt(key);


			int count = GetLatesIntermittentProgressiveCountWithin(nMonths, E_DebtRegularT.Revolving, E_AdverseType.Late60, false, key);

			InsertCache(key, count);
			return count;

		}


		virtual public int GetRevolvingLates90IntermittentProgressiveNonrollingCountWithin(int nMonths)
		{
			string key = "GetRevolvingLates90IntermittentProgressiveNonrollingCountWithin:" + nMonths;
			if (HasCache(key))
				return GetCacheInt(key);


			int count = GetLatesIntermittentProgressiveCountWithin(nMonths, E_DebtRegularT.Revolving, E_AdverseType.Late90, false, key);

			InsertCache(key, count);
			return count;

		}


		virtual public int GetRevolvingLates120IntermittentProgressiveNonrollingCountWithin(int nMonths)
		{
			string key = "GetRevolvingLates120IntermittentProgressiveNonrollingCountWithin:" + nMonths;
			if (HasCache(key))
				return GetCacheInt(key);


			int count = GetLatesIntermittentProgressiveCountWithin(nMonths, E_DebtRegularT.Revolving, E_AdverseType.Late120, false, key); 

			InsertCache(key, count);
			return count;

		}


		virtual public int GetRevolvingLates150IntermittentProgressiveNonrollingCountWithin(int nMonths)
		{
			string key = "GetRevolvingLates150IntermittentProgressiveNonrollingCountWithin:" + nMonths;
			if (HasCache(key))
				return GetCacheInt(key);


			int count = GetLatesIntermittentProgressiveCountWithin(nMonths, E_DebtRegularT.Revolving, E_AdverseType.Late150, false, key); 

			InsertCache(key, count);
			return count;

		}


		virtual public int GetRevolvingLates60IntermittentProgressiveRollingCountWithin(int nMonths)
		{
			string key = "GetRevolvingLates60IntermittentProgressiveRollingCountWithin:" + nMonths;
			if (HasCache(key))
				return GetCacheInt(key);

			int count = GetLatesIntermittentProgressiveCountWithin(nMonths, E_DebtRegularT.Revolving, E_AdverseType.Late60, true , key);

			InsertCache(key, count);
			return count;

		}


		virtual public int GetRevolvingLates90IntermittentProgressiveRollingCountWithin(int nMonths)
		{
			string key = "GetRevolvingLates90IntermittentProgressiveRollingCountWithin:" + nMonths;
			if (HasCache(key))
				return GetCacheInt(key);

			int count = GetLatesIntermittentProgressiveCountWithin(nMonths, E_DebtRegularT.Revolving, E_AdverseType.Late90, true, key);

			InsertCache(key, count);
			return count;

		}


		virtual public int GetRevolvingLates120IntermittentProgressiveRollingCountWithin(int nMonths)
		{
			string key = "GetRevolvingLates120IntermittentProgressiveRollingCountWithin:" + nMonths;
			if (HasCache(key))
				return GetCacheInt(key);

			int count = GetLatesIntermittentProgressiveCountWithin(nMonths, E_DebtRegularT.Revolving, E_AdverseType.Late120, true, key); 

			InsertCache(key, count);
			return count;

		}


		virtual public int GetRevolvingLates150IntermittentProgressiveRollingCountWithin(int nMonths)
		{
			string key = "GetRevolvingLates150IntermittentProgressiveRollingCountWithin:" + nMonths;
			if (HasCache(key))
				return GetCacheInt(key);

			int count = GetLatesIntermittentProgressiveCountWithin(nMonths, E_DebtRegularT.Revolving, E_AdverseType.Late150, true, key); 

			InsertCache(key, count);
			return count;

		}


		virtual public int GetInstallmentLates60IntermittentProgressiveNonrollingCountWithin(int nMonths)
		{
			string key = "GetInstallmentLates60IntermittentProgressiveNonrollingCountWithin:" + nMonths;
			if (HasCache(key))
				return GetCacheInt(key);


			int count = GetLatesIntermittentProgressiveCountWithin(nMonths, E_DebtRegularT.Installment, E_AdverseType.Late60, false, key);

			InsertCache(key, count);
			return count;

		}


		virtual public int GetInstallmentLates90IntermittentProgressiveNonrollingCountWithin(int nMonths)
		{
			string key = "GetInstallmentLates90IntermittentProgressiveNonrollingCountWithin:" + nMonths;
			if (HasCache(key))
				return GetCacheInt(key);


			int count = GetLatesIntermittentProgressiveCountWithin(nMonths, E_DebtRegularT.Installment, E_AdverseType.Late90, false, key);

			InsertCache(key, count);
			return count;

		}


		virtual public int GetInstallmentLates120IntermittentProgressiveNonrollingCountWithin(int nMonths)
		{
			string key = "GetInstallmentLates120IntermittentProgressiveNonrollingCountWithin:" + nMonths;
			if (HasCache(key))
				return GetCacheInt(key);


			int count = GetLatesIntermittentProgressiveCountWithin(nMonths, E_DebtRegularT.Installment, E_AdverseType.Late120, false, key); 

			InsertCache(key, count);
			return count;

		}


		virtual public int GetInstallmentLates150IntermittentProgressiveNonrollingCountWithin(int nMonths)
		{
			string key = "GetInstallmentLates150IntermittentProgressiveNonrollingCountWithin:" + nMonths;
			if (HasCache(key))
				return GetCacheInt(key);


			int count = GetLatesIntermittentProgressiveCountWithin(nMonths, E_DebtRegularT.Installment, E_AdverseType.Late150, false, key); 

			InsertCache(key, count);
			return count;

		}


		virtual public int GetInstallmentLates60IntermittentProgressiveRollingCountWithin(int nMonths)
		{
			string key = "GetInstallmentLates60IntermittentProgressiveRollingCountWithin:" + nMonths;
			if (HasCache(key))
				return GetCacheInt(key);

			int count = GetLatesIntermittentProgressiveCountWithin(nMonths, E_DebtRegularT.Installment, E_AdverseType.Late60, true, key);

			InsertCache(key, count);
			return count;

		}


		virtual public int GetInstallmentLates90IntermittentProgressiveRollingCountWithin(int nMonths)
		{
			string key = "GetInstallmentLates90IntermittentProgressiveRollingCountWithin:" + nMonths;
			if (HasCache(key))
				return GetCacheInt(key);


			int count = GetLatesIntermittentProgressiveCountWithin(nMonths, E_DebtRegularT.Installment, E_AdverseType.Late90, true, key);

			InsertCache(key, count);
			return count;
		}


		virtual public int GetInstallmentLates120IntermittentProgressiveRollingCountWithin(int nMonths)
		{
			string key = "GetInstallmentLates120IntermittentProgressiveRollingCountWithin:" + nMonths;
			if (HasCache(key))
				return GetCacheInt(key);


			int count = GetLatesIntermittentProgressiveCountWithin(nMonths, E_DebtRegularT.Installment, E_AdverseType.Late120, true, key); 

			InsertCache(key, count);
			return count;

		}


		virtual public int GetInstallmentLates150IntermittentProgressiveRollingCountWithin(int nMonths)
		{
			string key = "GetInstallmentLates150IntermittentProgressiveRollingCountWithin:" + nMonths;
			if (HasCache(key))
				return GetCacheInt(key);

			int count = GetLatesIntermittentProgressiveCountWithin(nMonths, E_DebtRegularT.Installment, E_AdverseType.Late150, true, key); 

			InsertCache(key, count);
			return count;
		}

        public virtual int GetLeaseLates30IntermittentProgressiveRollingCountWithin(int nMonths)
        {
            string key = "GetLeaseLates30IntermittentProgressiveRollingCountWithin:" + nMonths;
            if (HasCache(key))
                return GetCacheInt(key);

            int count = GetLatesIntermittentProgressiveCountWithin(nMonths, E_DebtRegularT.Lease, E_AdverseType.Late30, true, key);

            InsertCache(key, count);
            return count;
        }

        public virtual int GetLeaseLates30IntermittentProgressiveNonrollingCountWithin(int nMonths)
        {
            string key = "GetLeaseLates30IntermittentProgressiveNonrollingCountWithin:" + nMonths;
            if (HasCache(key))
                return GetCacheInt(key);

            int count = GetLatesIntermittentProgressiveCountWithin(nMonths, E_DebtRegularT.Lease, E_AdverseType.Late30, false, key);

            InsertCache(key, count);
            return count;
        }

        public virtual int GetLeaseLates60IntermittentProgressiveRollingCountWithin(int nMonths)
        {
            string key = "GetLeaseLates60IntermittentProgressiveRollingCountWithin:" + nMonths;
            if (HasCache(key))
                return GetCacheInt(key);

            int count = GetLatesIntermittentProgressiveCountWithin(nMonths, E_DebtRegularT.Lease, E_AdverseType.Late60, true, key);

            InsertCache(key, count);
            return count;
        }

        public virtual int GetLeaseLates60IntermittentProgressiveNonrollingCountWithin(int nMonths)
        {
            string key = "GetLeaseLates60IntermittentProgressiveNonrollingCountWithin:" + nMonths;
            if (HasCache(key))
                return GetCacheInt(key);

            int count = GetLatesIntermittentProgressiveCountWithin(nMonths, E_DebtRegularT.Lease, E_AdverseType.Late60, false, key);

            InsertCache(key, count);
            return count;
        }

        public virtual int GetLeaseLates90IntermittentProgressiveRollingCountWithin(int nMonths)
        {
            string key = "GetLeaseLates90IntermittentProgressiveRollingCountWithin:" + nMonths;
            if (HasCache(key))
                return GetCacheInt(key);

            int count = GetLatesIntermittentProgressiveCountWithin(nMonths, E_DebtRegularT.Lease, E_AdverseType.Late90, true, key);

            InsertCache(key, count);
            return count;
        }

        public virtual int GetLeaseLates90IntermittentProgressiveNonrollingCountWithin(int nMonths)
        {
            string key = "GetLeaseLates90IntermittentProgressiveNonrollingCountWithin:" + nMonths;
            if (HasCache(key))
                return GetCacheInt(key);

            int count = GetLatesIntermittentProgressiveCountWithin(nMonths, E_DebtRegularT.Lease, E_AdverseType.Late90, false, key);

            InsertCache(key, count);
            return count;
        }

        public virtual int GetHelocLates30IntermittentProgressiveRollingCountWithin(int nMonths)
        {
            string key = "GetHelocLates30IntermittentProgressiveRollingCountWithin:" + nMonths;
            if (HasCache(key))
                return GetCacheInt(key);

            int count = GetLatesIntermittentProgressiveCountWithin(nMonths, E_DebtRegularT.Heloc, E_AdverseType.Late30, true, key);

            InsertCache(key, count);
            return count;
        }

        public virtual int GetHelocLates30IntermittentProgressiveNonrollingCountWithin(int nMonths)
        {
            string key = "GetHelocLates30IntermittentProgressiveNonrollingCountWithin:" + nMonths;
            if (HasCache(key))
                return GetCacheInt(key);

            int count = GetLatesIntermittentProgressiveCountWithin(nMonths, E_DebtRegularT.Heloc, E_AdverseType.Late30, false, key);

            InsertCache(key, count);
            return count;
        }

        public virtual int GetHelocLates60IntermittentProgressiveRollingCountWithin(int nMonths)
        {
            string key = "GetHelocLates60IntermittentProgressiveRollingCountWithin:" + nMonths;
            if (HasCache(key))
                return GetCacheInt(key);

            int count = GetLatesIntermittentProgressiveCountWithin(nMonths, E_DebtRegularT.Heloc, E_AdverseType.Late60, true, key);

            InsertCache(key, count);
            return count;
        }

        public virtual int GetHelocLates60IntermittentProgressiveNonrollingCountWithin(int nMonths)
        {
            string key = "GetHelocLates60IntermittentProgressiveNonrollingCountWithin:" + nMonths;
            if (HasCache(key))
                return GetCacheInt(key);

            int count = GetLatesIntermittentProgressiveCountWithin(nMonths, E_DebtRegularT.Heloc, E_AdverseType.Late60, false, key);

            InsertCache(key, count);
            return count;
        }

        public virtual int GetHelocLates90IntermittentProgressiveRollingCountWithin(int nMonths)
        {
            string key = "GetHelocLates90IntermittentProgressiveRollingCountWithin:" + nMonths;
            if (HasCache(key))
                return GetCacheInt(key);

            int count = GetLatesIntermittentProgressiveCountWithin(nMonths, E_DebtRegularT.Heloc, E_AdverseType.Late90, true, key);

            InsertCache(key, count);
            return count;
        }

        public virtual int GetHelocLates90IntermittentProgressiveNonrollingCountWithin(int nMonths)
        {
            string key = "GetHelocLates90IntermittentProgressiveNonrollingCountWithin:" + nMonths;
            if (HasCache(key))
                return GetCacheInt(key);

            int count = GetLatesIntermittentProgressiveCountWithin(nMonths, E_DebtRegularT.Heloc, E_AdverseType.Late90, false, key);

            InsertCache(key, count);
            return count;
        }

        /// <summary>
        /// Intermittent & Progressive & Rolling. Example:
        /// a.	6/04-30, 5/04-30 would always be counted as 1x30
        /// b.	6/04-60, 5/04-30 would always be counted as 0x30, 1x60 (because of progressive nature)
        /// c.	7/04-30, 5/04-30 would always be counted as 2x30 (because of intermittent nature)
        /// d.	1/04-30,12/03-120, 11/03-90, 10/03-90, 9/03-60, 8/03-30, 7/03-30:
        /// will be counted as 1x30, 1x120	// 2/22/2007 nw - modifying 2x30 to 1x30
        /// 
        /// Intermittent & Progressive & Unrolling
        /// a.	6/04-30, 5/04-30 would always be counted as 2x30
        /// b.	6/04-60, 5/04-30 would always be counted as 0x30, 1x60 (because progressive nature)
        /// c.	7/04-30, 5/04-30 would always be counted as 2x30 (because of intermittent nature)
        /// d.	1/04-30, 12/03-120, 11/03-90, 10/03-90, 9/03-60, 8/03-30, 7/03-30:
        /// will be counted as 2x30,1x90,1x120
        /// </summary>
        /// <param name="month"></param>
        /// <param name="type"></param>
        /// <param name="lateType"></param>
        /// <returns></returns>
        private int GetLatesIntermittentProgressiveCountWithin(int nMonths, E_DebtRegularT type, E_AdverseType lateType, bool isRollingMode, string sCacheKey) 
		{

			if(nMonths > 24)
			{
				switch(lateType)
				{
					case E_AdverseType.Late30:
					case E_AdverseType.Late60:
					case E_AdverseType.Late90:
						break;
					case E_AdverseType.Late120:
					case E_AdverseType.Late150:
					case E_AdverseType.Late180:
					case E_AdverseType.Late90Plus:
					default:
						string sError = "Cannot calculate late values for 90+, 120, 150, and 180 older than 24 months. No data available.";
						sError = string.Format("{0}{1}{1}Bad parameter in keyword: {2}{1}{1}{3}", sError, System.Environment.NewLine, sCacheKey, m_extraDebugInfo.ToString());
						throw new CBaseException("Loan Program is being updated. System engineers have been notified.", sError);
				}
			}

			int n30		= 0;
			int n60		= 0;
			int n90		= 0;
			int n120	= 0;
			int n150	= 0;
			int n180	= 0;	// 1/17/2007 nw - OPM 2884 - handle Late180

			foreach (AbstractCreditLiability o in EffectiveLiabilities) 
			{

				if(nMonths > 24)
				{
					switch(lateType)
					{
							// From reverse-engineering, the pre-calculated late counts are 
							// done as non-progressive, non-rolling and 
							// intermittent so it's the worst case that we are falling back to.
						case E_AdverseType.Late30:
							try{ n30 += o.Late30Count; }
							catch{}
							continue;
						case E_AdverseType.Late60:
							try{ n60 += o.Late60Count; }
							catch{}
							continue;
						case E_AdverseType.Late90:
						case E_AdverseType.Late90Plus:
							try{ n90 += o.Late90Count + o.Late120Count; }
							catch{}
							continue;
						case E_AdverseType.Late120:
						case E_AdverseType.Late150:
						case E_AdverseType.Late180:
						default:
							string sError = "Cannot calculate late values for 90+, 120, 150, and 180 older than 24 months. No data available.";
							sError = string.Format("{0}{1}{1}Bad parameter in keyword: {2}{1}{1}{3}", sError, System.Environment.NewLine, sCacheKey, m_extraDebugInfo.ToString());
							throw new CBaseException("Loan Program is being updated. System engineers have been notified.", sError);
					}
				}

				if (o.LateOnlyCreditAdverseRatingList.Count == 0) // 11/23/2004 dd - No Late.
					continue;

				switch(type)
				{
					case E_DebtRegularT.Installment:
						if(!o.IsTypeInstallmentBase)
							continue;
						break;
					case E_DebtRegularT.Mortgage:
						if(!o.IsTypeMortgage)
							continue;
						// OPM# 3230: MortgageLates should ignore trades that are in BK or Foreclosed status  
						// 8/1/2006 nw - OPM 6683 - MortgageLates keywords should count trades that are in BK status
						// if(o.IsBankruptcy || o.IsForeclosure)
						if(o.IsForeclosure)
							continue;
						break;
                    case E_DebtRegularT.Heloc:
                        if (!o.IsTypeHeloc)
                            continue;
                        // OPM# 3230: MortgageLates should ignore trades that are in BK or Foreclosed status  
                        // 8/1/2006 nw - OPM 6683 - MortgageLates keywords should count trades that are in BK status
                        // if(o.IsBankruptcy || o.IsForeclosure)
                        if (o.IsForeclosure)
                            continue;
                        break;
                    case E_DebtRegularT.Open:
						if(!o.IsTypeOpen)
							continue;
						break;
					case E_DebtRegularT.Other:
                    case E_DebtRegularT.OtherExpenseLiability:
						if(!o.IsTypeOther)
							continue;
						break;
					case E_DebtRegularT.Revolving:
						if(!o.IsTypeRevolvingBase)
							continue;
						break;
                    case E_DebtRegularT.Lease:
                        if (!o.IsTypeLease)
                        {
                            continue;
                        }
                        break;
                    default:
						Tools.Assert(false, "Unhandled E_DebtRegularT enum value in GetLatesIntermittentProgressiveCountWithin");
						continue;
				}

				int eventNextMon = 0; // possible value: 0(none),30,60,90,120,150,180 "next mon"=="later mon"

				// example of LateDates string 9/02-150+, 8/02-120, 7/02-90, 6/02-60, 5/02-30, 2/02-60

				#if(TESTONLY)
				// TODO: Remove this guy before checking in
				lates = "3/04-60, 1/04-30, 12/03-120, 11/03-90, 10/03-90, 9/03-60, 8/03-30, 7/03-30, 5/03-30".Split(',');
				ROLLING: 30	:2  60	:1  120	:1     UNROLLING: 30: 3   60: 1   90: 1   120:1
				if(lates.Length < 0)
					lates = "3/04-60, 1/04-30, 12/03-120, 11/03-90, 9/03-90, 8/03-60, 7/03-30, 5/03-30".Split(',');
					ROLLING: 30:2  60:1  90:1  120:1
				#endif

				DateTime currentDate = DateTime.Today;
				DateTime nextRecordedDate = DateTime.Today;

				// If it's a mortgage, we always count lates in a conservative way:
				//  1) non-progressive, and
				//	2) if rolling is requested, only roll up to a max consecutive late
				//  3) intermittent
				bool bIsMortgage = o.IsTypeMortgage; 

				// 2/22/2007 nw - OPM 10557 - Max allowed mortgage rolling consecutive lates is 6
				const int Const_MaxMortRollConsecX30Allow = 6;
				const int Const_MaxMortRollConsecX60Allow = 6;		// WMC MORTGAGE CORP only allows 1 for Late60+
				const int Const_MaxMortRollConsecX90Allow = 6;
				const int Const_MaxMortRollConsecX120Allow = 6;
				const int Const_MaxMortRollConsecX150Allow = 6;
				const int Const_MaxMortRollConsecX180Allow = 6;

				// In side the adverse rating loop, we'll assign the correct ones because we always start out 
				// with eventNextMon = 0
				Tools.Assert(eventNextMon == 0, "eventNextMon == 0");

				int	nRollConsecX30Allow =	1000;  
				int nRollConsecX60Allow =	1000; 
				int nRollConsecX90Allow =	1000;
				int nRollConsecX120Allow =   1000;
				int nRollConsecX150Allow =   1000;
				int nRollConsecX180Allow =   1000;

				foreach (CreditAdverseRating late in o.LateOnlyCreditAdverseRatingList)  // we are going from the latest to the earliest
					//foreach(CreditAdverseRating late in t)
				{
					try 
					{
						DateTime recordD = late.Date;
											
						if(!Tools.IsDateWithin(recordD, nMonths))
							break; // done searching

						TagItem(sCacheKey, o);
						if(!Tools.IsDateWithin(recordD, 1, nextRecordedDate))
						{	// Found a break 
							eventNextMon = 0;
						}
						nextRecordedDate = recordD;

						E_AdverseType thisEvent = late.AdverseType; // event happens before 'eventNextMon' chronologically				

						switch(eventNextMon)
						{
							case 0:
							{
								if(bIsMortgage && isRollingMode)
								{
									// Counting/reseting based on 'eventNextMon'
									nRollConsecX30Allow =	Const_MaxMortRollConsecX30Allow ;  
									nRollConsecX60Allow =	Const_MaxMortRollConsecX60Allow ; 
									nRollConsecX90Allow =	Const_MaxMortRollConsecX90Allow ;
									nRollConsecX120Allow =  Const_MaxMortRollConsecX120Allow;
									nRollConsecX150Allow =  Const_MaxMortRollConsecX150Allow;
									nRollConsecX180Allow =  Const_MaxMortRollConsecX180Allow;
								}

								switch(thisEvent)
								{
									case E_AdverseType.Late30:  //0,30
										++n30;	eventNextMon = 30; break;
									case E_AdverseType.Late60:  //0,60
										++n60;	eventNextMon = 60; break;
									case E_AdverseType.Late90:  //0,90
										++n90;	eventNextMon = 90; break;
									case E_AdverseType.Late120: //0,120
										++n120;	eventNextMon = 120;	break;
									case E_AdverseType.Late150: //0,150
										++n150;	eventNextMon = 150;	break;
									case E_AdverseType.Late180: //0,180
										++n180;	eventNextMon = 180;	break;
									default:	// 0, ??
                                        LogCreditWarning(string.Format("Unexpected event string {0}", thisEvent));
										eventNextMon = 0;
										break;
								}
								continue;
							}
							case 30:
							{
								
								if(bIsMortgage && isRollingMode)
								{
									// Counting based on 'eventNextMon'
									--nRollConsecX30Allow; 
									nRollConsecX60Allow =	Const_MaxMortRollConsecX60Allow ; 
									nRollConsecX90Allow =	Const_MaxMortRollConsecX90Allow ;
									nRollConsecX120Allow =  Const_MaxMortRollConsecX120Allow;
									nRollConsecX150Allow =  Const_MaxMortRollConsecX150Allow;
									nRollConsecX180Allow =  Const_MaxMortRollConsecX180Allow;
								}

								switch(thisEvent)
								{
									case E_AdverseType.Late30:  //30,30: rolling
										if(isRollingMode)
										{
											if(bIsMortgage && 0 >= nRollConsecX30Allow)
											{	// Exceed the max sequence allowed, count this late and restart the max allowance count;
												++n30;
												nRollConsecX30Allow = Const_MaxMortRollConsecX30Allow;
											}
										}
										else
											++n30;

										eventNextMon = 30; // this statement is redundant, leave it here for consistency.
										break;
									case E_AdverseType.Late60:  //30,60: 
										++n60;	eventNextMon = 60;	break;
									case E_AdverseType.Late90:  //30,90:
										++n90;	eventNextMon = 90;	break;
									case E_AdverseType.Late120: //30,120
										++n120;	eventNextMon = 120;	break;
									case E_AdverseType.Late150: //30,150
										++n150;	eventNextMon = 150;	break;
									case E_AdverseType.Late180: //30,180
										++n180;	eventNextMon = 180;	break;
									default:
										LogCreditWarning(string.Format("Unexpected event string {0}", thisEvent));
										eventNextMon = 0;
										break;
								}
								continue;
							}
							case 60:
							{
								if(bIsMortgage && isRollingMode)
								{
									// Counting based on 'eventNextMon'
									nRollConsecX30Allow =	Const_MaxMortRollConsecX30Allow ;  
									--nRollConsecX60Allow;
									nRollConsecX90Allow =	Const_MaxMortRollConsecX90Allow ;
									nRollConsecX120Allow =  Const_MaxMortRollConsecX120Allow;
									nRollConsecX150Allow =  Const_MaxMortRollConsecX150Allow;
									nRollConsecX180Allow =  Const_MaxMortRollConsecX180Allow;
								}

								switch(thisEvent)
								{
									case E_AdverseType.Late30:  //60,30: progressive
										if(bIsMortgage)
											++n30; // no progressive for mortgage late
										eventNextMon = 30;	break;
									case E_AdverseType.Late60:  //60,60: rolling
										if(isRollingMode)
										{
											if(bIsMortgage && 0 >= nRollConsecX60Allow)
											{	// Exceed the max sequence allowed, count this late and restart the max allowance count;
												++n60;
												nRollConsecX60Allow = Const_MaxMortRollConsecX60Allow; 
											}
										}
										else
											++n60;
										eventNextMon = 60;			break;
									case E_AdverseType.Late90:  //60,90:
										++n90;	eventNextMon = 90;  break;
									case E_AdverseType.Late120: //60,120
										++n120;	eventNextMon = 120;	
										break;
									case E_AdverseType.Late150: //60,150
										++n150;	eventNextMon = 150;	break;
									case E_AdverseType.Late180: //60,180
										++n180;	eventNextMon = 180;	break;
									default:
										LogCreditWarning(string.Format("Unexpected event string {0}", thisEvent));
										eventNextMon = 0;
										break;
								}
								continue;
							}
							case 90:
							{
								if(bIsMortgage && isRollingMode)
								{
									// Counting based on 'eventNextMon'
									nRollConsecX30Allow =	Const_MaxMortRollConsecX30Allow ;  
									nRollConsecX60Allow =	Const_MaxMortRollConsecX60Allow ; 
									--nRollConsecX90Allow;
									nRollConsecX120Allow =  Const_MaxMortRollConsecX120Allow;
									nRollConsecX150Allow =  Const_MaxMortRollConsecX150Allow;
									nRollConsecX180Allow =  Const_MaxMortRollConsecX180Allow;
								}

								switch(thisEvent)
								{
									case E_AdverseType.Late30:  //90,30
										++n30;	eventNextMon = 30;	
										break;
									case E_AdverseType.Late60:  //90,60: progressive
										if(bIsMortgage)
											++n60; // no progressive for mortgage late
										eventNextMon = 60;			
										break;
									case E_AdverseType.Late90:  //90,90: rolling
										if(isRollingMode)
										{
											if(bIsMortgage && 0 >= nRollConsecX90Allow)
											{	// Exceed the max sequence allowed, count this late and restart the max allowance count;
												++n90;
												nRollConsecX90Allow = Const_MaxMortRollConsecX90Allow; // TODO: do similar for other guys.
											}
										}
										else
											++n90;
										eventNextMon = 90;			break;
									case E_AdverseType.Late120: //90,120
										++n120;	eventNextMon = 120;	
										break;
									case E_AdverseType.Late150: //90,150
										++n150;	eventNextMon = 150;	
										break;
									case E_AdverseType.Late180: //90,180
										++n180;	eventNextMon = 180;	
										break;
									default:
										LogCreditWarning(string.Format("Unexpected event string {0}", thisEvent));
										eventNextMon = 0;
										break;
								}
								continue;
							}
							case 120:
							{
								if(bIsMortgage && isRollingMode)
								{
									// Counting based on 'eventNextMon'
									nRollConsecX30Allow =	Const_MaxMortRollConsecX30Allow ;  
									nRollConsecX60Allow =	Const_MaxMortRollConsecX60Allow ; 
									nRollConsecX90Allow =	Const_MaxMortRollConsecX90Allow ;
									--nRollConsecX120Allow;
									nRollConsecX150Allow =  Const_MaxMortRollConsecX150Allow;
									nRollConsecX180Allow =  Const_MaxMortRollConsecX180Allow;
								}

								switch(thisEvent)
								{
									case E_AdverseType.Late30:  //120,30
										++n30;	eventNextMon = 30;	break;
									case E_AdverseType.Late60:  //120,60
										++n60;	eventNextMon = 60;	break;
									case E_AdverseType.Late90:  //120,90: // progressive
										if(bIsMortgage)
											++n90; // no progressive for mortgage late
										eventNextMon = 90;	break;
									case E_AdverseType.Late120: //120,120 rolling
										if(isRollingMode)
										{
											if(bIsMortgage && 0 >= nRollConsecX120Allow)
											{	// Exceed the max sequence allowed, count this late and restart the max allowance count;
												++n120;
												nRollConsecX120Allow = Const_MaxMortRollConsecX120Allow;
											}
										}
										else
											++n120;	
										eventNextMon = 120;	
										break;
									case E_AdverseType.Late150: //120,150
										++n150;	eventNextMon = 150;	
										break;
									case E_AdverseType.Late180: //120,180
										++n180;	eventNextMon = 180;	
										break;
									default:
										LogCreditWarning(string.Format("Unexpected event string {0}", thisEvent));
										eventNextMon = 0;
										break;
								}
								continue;
							}
							case 150:
							{
								if(bIsMortgage && isRollingMode)
								{
									// Counting based on 'eventNextMon'
									nRollConsecX30Allow =	Const_MaxMortRollConsecX30Allow ;  
									nRollConsecX60Allow =	Const_MaxMortRollConsecX60Allow ; 
									nRollConsecX90Allow =	Const_MaxMortRollConsecX90Allow ;
									nRollConsecX120Allow =  Const_MaxMortRollConsecX120Allow;
									--nRollConsecX150Allow;
									nRollConsecX180Allow =  Const_MaxMortRollConsecX180Allow;
								}

								switch(thisEvent)
								{
									case E_AdverseType.Late30:  //150,30
										++n30;	eventNextMon = 30;	break;
									case E_AdverseType.Late60:  //150,60
										++n60;	eventNextMon = 60;	break;
									case E_AdverseType.Late90:  //150,90: 
										++n90;	eventNextMon = 90;	break;
									case E_AdverseType.Late120: //150,120: progressive
										if(bIsMortgage)
											++n120; // no progressive for mortgage late
										eventNextMon = 120;	break;
									case E_AdverseType.Late150: //150,150: rolling
										if(isRollingMode)
										{
											if(bIsMortgage && 0 >= nRollConsecX150Allow)
											{	// Exceed the max sequence allowed, count this late and restart the max allowance count;
												++n150;
												nRollConsecX150Allow = Const_MaxMortRollConsecX150Allow;
											}
										}
										else
											++n150;	
										eventNextMon = 150;	break;
									case E_AdverseType.Late180:  //150,180: 
										++n180;	eventNextMon = 180;	break;
									default:
                                        LogCreditWarning(string.Format("Unexpected this event string {0}", thisEvent));
										eventNextMon = 0;
										break;
								}
								continue;
							}
							case 180:
							{
								if(bIsMortgage && isRollingMode)
								{
									// Counting based on 'eventNextMon'
									nRollConsecX30Allow =	Const_MaxMortRollConsecX30Allow ;  
									nRollConsecX60Allow =	Const_MaxMortRollConsecX60Allow ; 
									nRollConsecX90Allow =	Const_MaxMortRollConsecX90Allow ;
									nRollConsecX120Allow =  Const_MaxMortRollConsecX120Allow;
									nRollConsecX150Allow =  Const_MaxMortRollConsecX150Allow;
									--nRollConsecX180Allow;
								}

								switch(thisEvent)
								{
									case E_AdverseType.Late30:  //180,30
										++n30;	eventNextMon = 30;	break;
									case E_AdverseType.Late60:  //180,60
										++n60;	eventNextMon = 60;	break;
									case E_AdverseType.Late90:  //180,90: 
										++n90;	eventNextMon = 90;	break;
									case E_AdverseType.Late120: //180,120: 
										++n120;	eventNextMon = 120;	break;
									case E_AdverseType.Late150: //180,150: progressive
										if(bIsMortgage)
											++n150; // no progressive for mortgage late
										eventNextMon = 150;	break;
									case E_AdverseType.Late180: //180,180: rolling
										if(isRollingMode)
										{
											if(bIsMortgage && 0 >= nRollConsecX180Allow)
											{	// Exceed the max sequence allowed, count this late and restart the max allowance count;
												++n180;
												nRollConsecX180Allow = Const_MaxMortRollConsecX180Allow;
											}
										}
										else
											++n180;	
										eventNextMon = 180;	break;
									default:
                                        LogCreditWarning(string.Format("Unexpected this event string {0}", thisEvent));
										eventNextMon = 0;
										break;
								}
								continue;
							}
							default:
								LogCreditWarning(string.Format("Unexpected next month event value {0}", eventNextMon.ToString()));
								continue;
						}
					} 
					catch 
					{
						// Skip malformat string
					}
				}
			}

			int ret = 0;
			switch(lateType)
			{
				case E_AdverseType.Late30: 		ret = n30; break;
				case E_AdverseType.Late60: 		ret = n60; break;
				case E_AdverseType.Late90: 		ret = n90; break;
				case E_AdverseType.Late120:		ret = n120; break;
				case E_AdverseType.Late150:		ret = n150; break;
				case E_AdverseType.Late180:		ret = n180; break;
			}
			if ((lateType & E_AdverseType.Late90Plus) != 0) 
			{
				ret = n90 + n120 + n150 + n180;
			}

			//Tools.LogBug(string.Format("Result={0}x30, {1}x60, {2}x90, {3}x120, {4}x150", n30, n60, n90, n120, n150));

			return ret;
		}

		
		#endregion // NEW GENERIC METHODS
	
		#region SHARING METHODS

		private int GetHighestUnpaidBalOfDerogTypesOlderThan(E_DerogT[] derogTypes, int nMonths, string sCacheKey)
		{
			decimal highestBal = 0;
			DateTime todayDate = DateTime.Today;

			foreach(AbstractCreditPublicRecord o in EffectivePublicRecords)
			{
				try
				{
					decimal newBal = o.BankruptcyLiabilitiesAmount;
					if(newBal <= highestBal)
						continue;

					foreach(E_DerogT derogT in derogTypes)
					{
						switch(derogT)
						{
							case E_DerogT.ChargeOff:
							case E_DerogT.Foreclosure:
							case E_DerogT.Repossession:
								continue; // public rec doesn't have this derog type
							case E_DerogT.Collection:
								if(!o.IsCollectionUnpaid)
									continue;
								break;
							case E_DerogT.TaxLien:
								if(!o.IsTaxLienUnpaid)
									continue;
								break;
							case E_DerogT.Judgment:
								if(!o.IsJudgmentUnpaid)
									continue;
								break;
							default:
								Tools.LogBug("Unhandled E_DerogT enum value in GetHighestUnpaidBalOfDerogTypesOlderThan");
								continue;
						}

						if(!Tools.IsDateWithin(o.LastEffectiveDate, nMonths, todayDate))
						{
							TagItem(sCacheKey, o);
							highestBal = newBal;
						}
						break; // just need to do this once for each pub rec.
					}
				}
				catch(Exception ex)
				{
					LogCreditWarning(ex);
				}
			}

			foreach (AbstractCreditLiability o in EffectiveLiabilities) 
			{
				try
				{
					decimal newBal = o.UnpaidBalanceAmount;
					if(newBal <= highestBal || o.IsPaid)
						continue;

					if(o.HasDerog(derogTypes, false) && !o.HasMajorDerogEffectiveWithin(derogTypes,	nMonths,  /* bInclPaidAccs */ false))
					{
						TagItem(sCacheKey, o);
						highestBal = o.UnpaidBalanceAmount;
					}
				}
				catch(Exception ex)
				{
					LogCreditWarning(ex);
				}
			}

			return (int) highestBal;
		}


		private int GetHighestUnpaidBalOfDerogTypesWithin(E_DerogT[] derogTypes, int nMonths, string sCacheKey)
		{
			decimal highestBal = 0;
			DateTime currentDate = DateTime.Today;

			foreach(AbstractCreditPublicRecord o in EffectivePublicRecords)
			{
				try
				{
					decimal newBal = o.BankruptcyLiabilitiesAmount;
					if(newBal <= highestBal)
						continue;

					foreach(E_DerogT derogT in derogTypes)
					{
						switch(derogT)
						{
							case E_DerogT.ChargeOff:
							case E_DerogT.Foreclosure:
							case E_DerogT.Repossession:
								continue; // public rec doesn't have this derog type
							case E_DerogT.Collection:
								if(!o.IsCollectionUnpaid)
									continue;
								break;
							case E_DerogT.TaxLien:
								if(!o.IsTaxLienUnpaid)
									continue;
								break;
							case E_DerogT.Judgment:
								if(!o.IsJudgmentUnpaid)
									continue;
								break;
							default:
								Tools.LogBug("Unhandled E_DerogT enum value in GetHighestUnpaidBalOfDerogTypesWithin");
								continue;
						}

						if(Tools.IsDateWithin(o.LastEffectiveDate, nMonths, currentDate))
						{
							TagItem(sCacheKey, o);
							highestBal = newBal;
						}
						break; // just need to do this once for each pub rec.
					}
				}
				catch(Exception ex)
				{
					LogCreditWarning(ex);
				}
			}

			foreach (AbstractCreditLiability o in EffectiveLiabilities) 
			{
				try
				{
					decimal newBal = o.UnpaidBalanceAmount;
					if(newBal <= highestBal || o.IsPaid)
						continue;

					if(o.HasMajorDerogEffectiveWithin(derogTypes,	nMonths,  /* bInclPaidAccs */ false))
					{
						TagItem(sCacheKey, o);
						highestBal = o.UnpaidBalanceAmount;
					}
				}
				catch(Exception ex)
				{
					LogCreditWarning(ex);
				}
			}

			return (int) highestBal;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="derogTypes"></param>
		/// <param name="nMonths"></param>
		/// <param name="invokeMethodNm"></param>
		/// <returns></returns>
		private int GetTotUnpaidBalOfDerogTypesWithin(E_DerogT[] derogTypes, int nMonths, string sCacheKey)
		{
			decimal totBal = 0;
			DateTime currentDate = DateTime.Today;

			foreach(AbstractCreditPublicRecord o in EffectivePublicRecords)
			{
				try
				{
					decimal newBal = o.BankruptcyLiabilitiesAmount;
					if(newBal <= 0)
						continue;

                    
					foreach(E_DerogT derogT in derogTypes)
					{
						switch(derogT)
						{
							case E_DerogT.ChargeOff:
							case E_DerogT.Foreclosure:
							case E_DerogT.Repossession:
								continue; // public rec doesn't have this derog type
							case E_DerogT.Collection:
								if(!o.IsCollectionUnpaid)
									continue;
								break;
							case E_DerogT.TaxLien:
								if(!o.IsTaxLienUnpaid)
									continue;
								break;
							case E_DerogT.Judgment:
								if(!o.IsJudgmentUnpaid)
									continue;
								break;
							case E_DerogT.ANY:
								if(!o.IsDerogUnpaid)
									continue; // don't expect any other type to get hit, but just continue to flow through the code logics
								break;
							default:
								Tools.LogBug("Unhandled E_DerogT enum value in GetTotUnpaidBalOfDerogTypesWithin");
								continue;
						} // switch

						if(Tools.IsDateWithin(o.LastEffectiveDate, nMonths, currentDate))
						{
							TagItem(sCacheKey, o);
							totBal += newBal;
						}
						break; // just need to do this once for each pub rec.
					} // foreach(E_DerogT derogT...
				} // try
				catch(Exception ex)
				{
					LogCreditWarning(ex);
				}
			}

			foreach (AbstractCreditLiability o in EffectiveLiabilities) 
			{
				try
				{
					if(o.IsPaid)
						continue;

					if(o.HasMajorDerogEffectiveWithin(derogTypes,	nMonths,  /* bInclPaidAccs */ false))
					{
						TagItem(sCacheKey, o);
						totBal += o.UnpaidBalanceAmount;
					}
				}
				catch(Exception ex)
				{
					LogCreditWarning(ex);
				}
			}

			return (int) totBal;
		}

		#endregion //SHARING METHODS

        public int ChargeOffCountX(string parameters)
        {
            return EvaluateChargeOffCountX(parameters, this);
        }

        public static int EvaluateChargeOffCountX(string parameters, AbstractCreditReport creditReport)
        {
            string key = "ChargeOffCountX:" + parameters.ToLower();

            if (creditReport != null && creditReport.HasCache(key))
            {
                return creditReport.GetCacheInt(key);
            }

            // Months reviewed: Within
            string withinOp = "<=";
            int withinMonths = 1000;

            // Type: T
            string typeOp = "="; // must be =, we can support != in the future
            string types = "irmoulh"; // 'i', 'r', 'm', 'o', 'u', 'l', 'h' and any combination. 'o': open and  'u': unknown, other.

            char[] op_chars = new char[] { '<', '>', '=' };

            foreach (string attribute in parameters.ToLower().Split('&'))
            {
                if (attribute == "," || attribute == "")
                {
                    continue;
                }

                string name = "";
                string op = "";
                string val = "";

                // Extracting name
                // 12/21/2006 nw - OPM 5606 - Handle mistyped parameters/attributes
                int nameLen = attribute.IndexOfAny(op_chars);
                try
                {
                    name = attribute.Substring(0, nameLen);
                }
                catch (ArgumentOutOfRangeException) // must be an invalid format, log bug below and throw exception in the switch statement
                {
                    name = attribute;
                }

                // Extracting operator
                StringBuilder opBuilder = new StringBuilder(5);
                Match match = System.Text.RegularExpressions.Regex.Match(attribute, "[<>=]");
                while (match.Success)
                {
                    string token = match.Groups[0].Value;
                    opBuilder.Append(token);
                    match = match.NextMatch();
                }
                op = opBuilder.ToString();

                // Extracting value
                try
                {
                    val = attribute.Substring(nameLen + op.Length);
                }
                catch (ArgumentOutOfRangeException) { } // must be an invalid format 

                if (name == "" || op == "" || val == "")
                {
                    Tools.LogBug(string.Format("None of these can be empty. Name={0}. Op={1}. Val={2}", name, op, val));
                }

                switch (name)
                {
                    case "within":
                        if (op != "<=")
                        {
                            throw CreateGenericExceptionWithDevMessage(string.Format("{0} is not a supported operator for parameter Within in keyword ChargeOffCountX", op));
                        }
                        withinOp = op;

                        try
                        {
                            withinMonths = int.Parse(val);
                            if (withinMonths < 0)
                            {
                                throw CreateGenericExceptionWithDevMessage("Months reviewed (withinMonths) was < 0.");
                            }
                        }
                        catch
                        {
                            throw CreateGenericExceptionWithDevMessage(string.Format("{0} is not a supported argument for parameter Within in keyword ChargeOffCountX", val));
                        }
                        break;
                    case "t":
                        if (op != "=")
                        {
                            throw CreateGenericExceptionWithDevMessage(string.Format("{0} is not a supported operator for parameter T in keyword ChargeOffCountX", op));
                        }
                        typeOp = op;

                        int len = val.Length;
                        for (int i = 0; i < len; ++i)
                        {
                            switch (val[i])
                            {
                                case 'i':
                                case 'r':
                                case 'm':
                                case 'o':
                                case 'u':
                                case 'l':
                                case 'h':
                                    break;
                                default:
                                    throw CreateGenericExceptionWithDevMessage(string.Format("{0} character is not a supported argument for parameter T in keyword ChargeOffCountX", val[i].ToString()));
                            }
                        }
                        types = val;
                        break;
                    default:
                        string s = string.Format("Error encountered in ChargeOffCountX keyword, parameter name:{0} is not valid", name);
                        Tools.LogError(s);
                        throw CreateGenericExceptionWithDevMessage(s);
                }
            }

            if (creditReport == null)
            {
                return 0;
            }

            int count = 0;
            DateTime currentDate = DateTime.Today;

            foreach (var publicRecord in creditReport.EffectivePublicRecords)
            {
                if (publicRecord.IsCollection &&
                    Tools.IsDateWithin(publicRecord.LastEffectiveDate, withinMonths, currentDate))
                {
                    creditReport.TagItem(key, publicRecord);
                    ++count;
                }
            }

            var liabilities = creditReport.EffectiveLiabilities
                .Where(l => l.IsOfType(types) && !l.IsShortSale);

            var derog = new E_DerogT[] { E_DerogT.ChargeOff, E_DerogT.Collection };
            bool includePaid = true;

            foreach (var liability in liabilities)
            {
                if (liability.HasMajorDerogEffectiveWithin(derog, withinMonths, includePaid))
                {
                    creditReport.TagItem(key, liability);
                    ++count;
                }
            }

            creditReport.InsertCache(key, count);

            return count;
        }

        private static GenericUserErrorMessageException CreateGenericExceptionWithDevMessage(string devMessage)
        {
            var exc = new GenericUserErrorMessageException(devMessage);

            // OPM 228231, ML, 10/27/2015
            exc.IsEmailDeveloper = false;
            return exc;
        }
	}
}
