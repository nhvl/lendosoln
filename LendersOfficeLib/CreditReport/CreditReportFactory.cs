using System;
using System.Data.Common;
using System.Data.SqlClient;
using System.IO;
using System.Xml;
using DataAccess;
using LendersOffice.Common;
using LqbGrammar.DataTypes;

namespace LendersOffice.CreditReport
{

    // 3/15/2006 dd - Thinh, please make change this class from 'public' to 'internal' after you provide access ICreditReportView in
    // CAppData. The reason this is still public because I don't want build break in PML project.
    // 2/9/2017 ml - Making this class public so it is accessible by the fake app class in the test project.
    public class CreditReportFactory
	{
		private CreditReportFactory()
		{
		}

        #region THESE METHODS SHOULD REMAIN IN THIS CLASS

        // 3/15/2006 dd - Will switch these two methods to return CreditReportProxy class once I make this class internal.

        public static CreditReportProxy ConstructOptimisticCreditReport(CAppBase dataApp, CreditReportDebugInfo debugInfo) 
        {
            CCreditReportWithOptimisticDefault creditReport = new CCreditReportWithOptimisticDefault(new CreditReportFactory(), dataApp);
			return new CreditReportProxy( creditReport );
        }

        public static CreditReportProxy ConstructCreditReportFromXmlDoc(XmlDocument doc, CreditReportDebugInfo debugInfo) 
        {
            using (PerformanceStopwatch.Start("CreditReportFactory.ConstructCreditReportFromXmlDoc"))
            {
                AbstractCreditReport abstractCreditReport = null;

                if (null != doc)
                {
                    CreditReportProtocol protocol = DetermineCreditReportType(doc);

                    CreditReportFactory factory = new CreditReportFactory();

                    switch (protocol)
                    {
                        case CreditReportProtocol.Mcl:

                            XmlDocument mdoc = GetMismoDocFromMCL(doc);  //so if theres a mismo version attached to the report use it. 

                            if (mdoc != null)
                            {
                                abstractCreditReport = new Mismo2_3.Mismo2_3CreditReport(factory, CreditReportProtocol.MISMO_2_3, mdoc, debugInfo);
                            }
                            else  //if not default to the xml 
                            {
                                abstractCreditReport = new Mcl.MclCreditReport(factory, doc, debugInfo);
                            }
                            break;
                        case CreditReportProtocol.Landsafe:
                        case CreditReportProtocol.Fiserv:
                        case CreditReportProtocol.FannieMae:
                        case CreditReportProtocol.MISMO_2_1:
                            abstractCreditReport = new Mismo2_1.Mismo2_1CreditReport(factory, protocol, doc, debugInfo);
                            break;
                        case CreditReportProtocol.UniversalCredit:
                        case CreditReportProtocol.SharperLending:
                        case CreditReportProtocol.StandFacts:
                        case CreditReportProtocol.InfoNetwork:
                        case CreditReportProtocol.Credco:
                        case CreditReportProtocol.KrollFactualData:
                        case CreditReportProtocol.CSD:
                        case CreditReportProtocol.MISMO_2_3:
                            abstractCreditReport = new Mismo2_3.Mismo2_3CreditReport(factory, protocol, doc, debugInfo);

                            break;
                        default:
                            throw new CBaseException(ErrorMessages.Generic, protocol + " is unsupport in RetrieveCreditReportData");
                    }
                }

                if (null != abstractCreditReport)
                    return new CreditReportProxy(abstractCreditReport);
                else
                    return null;
            }
        }
		
        public static Sensitive<ICreditReportView> ConstructCreditReportViewFromXmlDoc(XmlDocument doc, CreditReportDebugInfo debugInfo) 
        {

            ICreditReportView creditReportView = null;

            if (null != doc) 
            {
                CreditReportFactory factory = new CreditReportFactory();

                CreditReportProtocol protocol = DetermineCreditReportType(doc);

                switch (protocol) 
                {
                    case CreditReportProtocol.Mcl:
                        creditReportView = new Mcl.MclCreditReportView(factory, doc, debugInfo);
                        break;
                    case CreditReportProtocol.Credco:
                        creditReportView = new Credco.Credco_CreditReportView(factory, doc, debugInfo);
                        break;
                    case CreditReportProtocol.KrollFactualData:
                        creditReportView = new KrollFactualData.KrollFactualData_CreditReportView(factory, doc, debugInfo);
                        break;
                    case CreditReportProtocol.FannieMae:
                    case CreditReportProtocol.MISMO_2_1:
                    case CreditReportProtocol.MISMO_2_3:
                        creditReportView = new Mismo.MismoCreditReportView(factory, doc, debugInfo);
                        break;
                    default:
                        throw new CBaseException(ErrorMessages.Generic, protocol + " is unsupport in RetrieveCreditReportView");
                }
            }

            return new Sensitive<ICreditReportView>(creditReportView);
        }
		
        private static CreditReportProtocol DetermineCreditReportType(XmlDocument doc) 
        {
            if (null == doc)
            {
                throw new CBaseException(ErrorMessages.Generic, "CreditReportFactory.DetermineCreditReportType() parameter is null.");
            }

            XmlElement node = (XmlElement) doc.SelectSingleNode("//RESPONSE_GROUP/RESPONSE/RESPONSE_DATA/CREDIT_RESPONSE");
            CreditReportProtocol protocol = CreditReportProtocol.Mcl;

            if (null != node)
            {
                // 1/4/2005 dd - Credit report is MISMO format.
                string mismoVersionID = node.GetAttribute("MISMOVersionID");

                string respondingPartyName = "";
                XmlElement respondingParty = (XmlElement) doc.SelectSingleNode("//RESPONSE_GROUP/RESPONDING_PARTY");
                if (null != respondingParty)
                    respondingPartyName = respondingParty.GetAttribute("_Name");
                if (respondingPartyName.StartsWith("FannieMae - "))
                {
                    protocol = CreditReportProtocol.FannieMae;
                }
                else if (respondingPartyName.IndexOf("Factual Data") >= 0 || respondingPartyName.IndexOf("Credit Lenders Service Agency") >= 0)
                {
                    protocol = CreditReportProtocol.KrollFactualData;
                } 
                else if (respondingPartyName == "First American CREDCO")
                {
                    protocol = CreditReportProtocol.Credco;

                } 
                else if (mismoVersionID == "2.3" || mismoVersionID == "2.3.1")
                {
                    protocol = CreditReportProtocol.MISMO_2_3;
                } 
                else
                {
                    protocol = CreditReportProtocol.MISMO_2_1;
                }
            } 
            else if ((XmlElement)doc.SelectSingleNode("//OUTPUT/RESPONSE/OUTPUT_FORMAT") != null)
            {
                protocol = CreditReportProtocol.Mcl;
            }
            else
            {
                throw new CBaseException(ErrorMessages.Generic, "Invalid credit report format.");
            }

            return protocol;
        }

        #endregion

        #region THESE METHODS CAN MOVE TO CAppData


        public static Guid RetrieveFileDBKey(Guid brokerId, Guid appId, bool bIsRetrieveLastDeleteCreditReport, CreditReportTypes reportType) 
        {
            using (PerformanceStopwatch.Start("CreditReportFactory.RetrieveFileDBKey"))
            {
                Guid key = Guid.Empty;

                string spName;
                switch (reportType)
                {
                    case CreditReportTypes.Primary:
                        spName = "RetrieveCreditReport";
                        break;
                    case CreditReportTypes.Lqi:
                        spName = "RetrieveLqiCreditReport";
                        break;
                    default:
                        throw new UnhandledEnumException(reportType);
                }

                SqlParameter[] parameters = { new SqlParameter("@ApplicationID", appId) };
                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, spName, parameters))
                {
                    if (reader.Read())
                    {
                        key = (Guid)reader["DbFileKey"];
                    }
                }

                if (bIsRetrieveLastDeleteCreditReport && key == Guid.Empty)
                {
                    // 3/15/2006 dd - This is the scenario where user want to view the Credco credit report over 5 days old.
                    // Since we delete the actual credit report in our system, we need to detect if last delete credit report is credco
                    // and return a message inform user we already delete this credco report.
                    if (Guid.Empty != appId)
                    {
                        parameters = new SqlParameter[] { new SqlParameter("@ApplicationID", appId) };
                        using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "RetrieveLastDeleteCreditReport", parameters))
                        {
                            if (reader.Read())
                            {
                                // 2/14/2006 dd - Only load if last previous delete credit report is CREDCO.
                                Guid comid = (Guid)reader["comid"];
                                if (comid == new Guid("F27CC198-78F9-4059-9B06-D6C94318C803"))
                                {
                                    key = (Guid)reader["DbFileKey"];
                                }
                            }
                        }
                    }
                }

                return key;
            }
        }

        public static XmlDocument LoadFromFileDB(string key, CreditReportDebugInfo debugInfo) 
        {
            using (PerformanceStopwatch.Start("CreditReportFactory.LoadFromFileDB"))
            {
                if (Guid.Empty.ToString() == key || null == key)
                {
                    return null;
                }
                XmlDocument doc = null;

                try
                {
                    Action<FileInfo> readHandler = delegate (FileInfo fi)
                    {
                        using (StreamReader stream = new StreamReader(fi.FullName))
                        {
                            string xml = stream.ReadToEnd();
                            doc = Tools.CreateXmlDoc(xml);
                        }
                    };

                    FileDBTools.UseFile(E_FileDB.Normal, key, readHandler);
                }
                catch (Exception exc)
                {
                    Tools.LogErrorWithCriticalTracking(exc);
                }

                return doc;
            }
        }


        #endregion

		private static XmlDocument GetMismoDocFromMCL( XmlDocument doc ) 
		{
			if (null == doc) 
				throw new CBaseException(ErrorMessages.Generic, "CreditReportFactory.GetMismoDocFromMCL() parameter is null.");
		
			XmlNode node = doc.SelectSingleNode("/OUTPUT/RESPONSE/OUTPUT_FORMAT[@format_type='MISMO2_3_1']");
			if ( node == null ) return null;
			XmlDocument mDoc = new XmlDocument();
			mDoc.InnerXml = node.InnerText; 
			return mDoc;
			
		}

	}
}
