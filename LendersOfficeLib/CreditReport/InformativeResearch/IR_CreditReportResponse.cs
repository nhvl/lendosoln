﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;

namespace LendersOffice.CreditReport.InformativeResearch
{
    public class IR_CreditReportResponse : LendersOffice.CreditReport.Mismo2_3.Mismo2_3CreditReportResponse
    {
        /// <summary>
        /// Provides an alternative value for <see cref="ReportID"/> with a non-null value.
        /// </summary>
        private readonly string reportIdOverride = null;

        public IR_CreditReportResponse(XmlDocument doc, string brandedProductName)
            : base(doc)
        {
            this.BrandedProductName = brandedProductName;
            this.StatusDescription = doc.SelectSingleNode("/RESPONSE_GROUP/RESPONSE/STATUS[@_Description != '']/@_Description")?.Value;
            if (doc != null && !this.HasError)
            {
                // This error response handling is only really for Retrieve Billing responses
                XmlNodeList errorStatuses = doc.SelectNodes("/RESPONSE_GROUP/RESPONSE/STATUS[(@_Condition='Error' or @_Condition='Failed') and @_Description != '']"); // MISMO says there can be more than one, but our credit model doesn't support it, so I'm not worrying about it.
                if (errorStatuses.Count != 0)
                {
                    this.SetErrorInfo(errorStatuses.Cast<XmlElement>().First().GetAttribute("_Description"));
                    return;
                }
            }

            XmlElement responseData = (XmlElement)doc?.SelectSingleNode("/RESPONSE_GROUP/RESPONSE/RESPONSE_DATA");
            XmlElement verificationResponse = (XmlElement)responseData?.SelectSingleNode("VERIFICATION_RESPONSE");
            XmlElement billingResponse = (XmlElement)responseData?.SelectSingleNode("BILLING_RESPONSE");
            if (verificationResponse != null)
            {
                this.AdditionalDocumentsReceived = verificationResponse.SelectNodes("VERIFICATION_DETERMINATION/EMBEDDED_FILE").Cast<XmlElement>().Select(ParseDocument);
                this.reportIdOverride = verificationResponse.SelectSingleNode("@VerificationIdentifier | VERIFICATION_DETERMINATION/@VerificationIdentifier")?.Value; // The docs say it's on VERIFICATION_RESPONSE, but in practice it's on VERIFICATION_DETERMINATION

                // IR has been very inconsistent here.  Their docs and support indicate that these elements will be returned, but I've never
                // actually seen a response with one.  They also have inconsistency in the documentation and examples over what the element
                // will be named.  I've seen them use both, even in the same example.  For now, the best we can do is be flexible.
                XmlNodeList individualStatuses = verificationResponse.SelectNodes("VERIFICATION_DETERMINATION/*[(local-name() = 'STATUS' or local-name() = 'VERIFICATION_STATUS') and @_Description != '']/@_Description");
                Func<XmlAttribute, string> getStatusDescription = a => a.SelectSingleNode("../../@CreditorName")?.Value + ": " + a.Value;
                if (individualStatuses.Count > 1)
                {
                    this.StatusDescription = string.Join("\n", individualStatuses.Cast<XmlAttribute>().Select((a, i) => $"{i + 1}. {getStatusDescription(a)}"));
                }
                else if (individualStatuses.Count == 1)
                {
                    this.StatusDescription = getStatusDescription(individualStatuses.Cast<XmlAttribute>().First());
                }
            }
            else if (billingResponse != null)
            {
                this.AdditionalDocumentsReceived = billingResponse.SelectNodes("EMBEDDED_FILE").Cast<XmlElement>().Select(ParseDocument);
                this.reportIdOverride = billingResponse.SelectSingleNode("@OrderIdentifier")?.Value;
            }
        }

        /// <inheritdoc cref="base.ReportID"/>
        public override string ReportID => this.reportIdOverride ?? base.ReportID;

        /// <summary>
        /// Gets the PDF documents contained in the response, with each as a Base64-encoded string.
        /// </summary>
        public IEnumerable<CreditVendorAdditionalDocument> AdditionalDocumentsReceived { get; } = Enumerable.Empty<CreditVendorAdditionalDocument>();

        /// <summary>
        /// Gets the status description returned by the credit provider.
        /// </summary>
        public string StatusDescription { get; }

        private CreditVendorAdditionalDocument ParseDocument(XmlElement embeddedFileElement)
        {
            return new CreditVendorAdditionalDocument(
                fileName: embeddedFileElement.SelectSingleNode("@_Name").Value,
                base64: LqbGrammar.DataTypes.Base64EncodedData.Create(embeddedFileElement.SelectSingleNode("DOCUMENT/text()").Value).Value);
        }
    }
}
