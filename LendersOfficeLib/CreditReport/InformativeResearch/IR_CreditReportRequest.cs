﻿using System;
using System.Net;
using System.Xml;
using System.Xml.Linq;
using LendersOffice.CreditReport.Mismo;
using LendersOffice.Common;

namespace LendersOffice.CreditReport.InformativeResearch
{
    public class IR_CreditReportRequest : LendersOffice.CreditReport.Mismo2_3.Mismo2_3CreditReportRequest
    {
        public enum StandardCreditReportType
        {
            Premier = 0,
            SoftQual = 1,
            MortgageOnly = 2,
            MortgageOnlyWithScores = 3,
        }

        public enum LqiCreditReportType
        {
            PreClose = 0,
            QuickLook = 1,
            QuickLookWithScores = 2,
        }

        private const string REQUEST_SOURCE = "1000101";
        private const string RECEIVING_PARTY_NAME = "Informative Research";
        private const string SUBMITTING_PARTY_NAME = "LendingQB";
        private const string TEST_URL = "https://www.getdirectcredit.com/nebula/mismo/envelopepost";
        private const string BILLING_AND_SUPPLEMENT_URL = "https://www.getdirectcredit.com/MismoReportListener/Listener.aspx";
        private const string BILLING_AND_SUPPLEMENT_TEST_URL = "https://www.getdirectcredit.com/STG-MismoReportListener/Listener.aspx";

        /// <summary>
        /// A value to override <see cref="base.Url"/> if it is changed to be non-null.
        /// </summary>
        private string urlOverride = null;

        public IR_CreditReportRequest(string sURL)
            : base(sURL)
        {
            this.ReceivingPartyName = RECEIVING_PARTY_NAME;
            this.SubmittingPartyName = SUBMITTING_PARTY_NAME;
            AddRequestKeyValue("irRequestSource", REQUEST_SOURCE);
            AppendOutputFormat(E_MismoOutputFormat.PDF);
        }

        public override string Url => this.urlOverride ?? base.Url;

        /// <summary>
        /// Checks equality between <paramref name="url"/> and the output URL for Informative Research's test environment.
        /// </summary>
        /// <param name="url">The url to compare against Informative Research's test environment.</param>
        /// <returns>True if the url matches the test environment; false otherwise.</returns>
        public static bool IsTestUrl(string url)
        {
            return string.Equals(url, TEST_URL, StringComparison.OrdinalIgnoreCase);
        }

        public override void SetupAuthentication(HttpWebRequest webRequest)
        {
            webRequest.ContentType = "text/xml";
        }

        private string brandedProductName = null;

        public override bool CanCustomizeRequestDetails => true;
        public override void CustomizeRequestDetails(CreditRequestData creditRequestData)
        {
            if (creditRequestData.RequestActionType == E_CreditReportRequestActionType.Submit)
            {
                StandardCreditReportType creditReportType = creditRequestData.CreditReportType.CastDefined<StandardCreditReportType>();
                this.RequestType = creditReportType == StandardCreditReportType.Premier ? E_CreditReportRequestActionType.Submit : E_CreditReportRequestActionType.Other;
                this.RequestTypeOtherDesc = GetRequestTypeOtherDescriptionFor(creditReportType);
                this.brandedProductName = creditReportType.ToString("G") + " Credit Report";
                return;
            }
            else if (creditRequestData.RequestActionType != E_CreditReportRequestActionType.Other)
            {
                return;
            }

            if (creditRequestData.RequestActionTypeOtherDetail == CreditReportRequestActionTypeOtherDetail.LqiNew)
            {
                LqiCreditReportType creditReportType = creditRequestData.CreditReportType.CastDefined<LqiCreditReportType>();
                this.RequestType = E_CreditReportRequestActionType.Other;
                this.RequestTypeOtherDesc = GetRequestTypeOtherDescriptionFor(creditReportType);
                this.brandedProductName = creditReportType.ToString("G") + " Credit Report";
                if (creditReportType == LqiCreditReportType.PreClose)
                {
                    this.IncludeEquifax = true;
                    this.IncludeExperian = true;
                    this.IncludeTransunion = true;
                }
            }
            else if (creditRequestData.RequestActionTypeOtherDetail == CreditReportRequestActionTypeOtherDetail.RemoveBorrower)
            {
                this.RequestType = E_CreditReportRequestActionType.Other;
                this.RequestTypeOtherDesc = "RemoveBorrower";
                this.Coborrower.CopyFrom(new BorrowerInfo(isCoborrower: true)); // clear co-borrower; calling code must have already passed in the former co-borrower as the borrower
                this.IncludeEquifax = true;
                this.IncludeExperian = true;
                this.IncludeTransunion = true;
            }
            else if (creditRequestData.RequestActionTypeOtherDetail == CreditReportRequestActionTypeOtherDetail.RemoveCoBorrower)
            {
                this.RequestType = E_CreditReportRequestActionType.Other;
                this.RequestTypeOtherDesc = "RemoveCoBorrower";
                this.Coborrower.CopyFrom(new BorrowerInfo(isCoborrower: true)); // clear co-borrower
                this.IncludeEquifax = true;
                this.IncludeExperian = true;
                this.IncludeTransunion = true;
            }
            else if (creditRequestData.RequestActionTypeOtherDetail == CreditReportRequestActionTypeOtherDetail.RetrieveSupplement)
            {
                this.urlOverride = this.IsTest ? BILLING_AND_SUPPLEMENT_TEST_URL : BILLING_AND_SUPPLEMENT_URL;
                this.RequestDataInnerXmlOverride = new XElement(
                    "VERIFICATION_REQUEST",
                    new XAttribute("_ActionType", "StatusQuery"),
                    new XAttribute("LenderCaseIdentifier", this.LenderCaseIdentifier),
                    new XAttribute("VerificationIdentifier", this.ReportID));
            }
            else if (creditRequestData.RequestActionTypeOtherDetail == CreditReportRequestActionTypeOtherDetail.RetrieveBilling)
            {
                this.urlOverride = this.IsTest ? BILLING_AND_SUPPLEMENT_TEST_URL : BILLING_AND_SUPPLEMENT_URL;
                this.RequestDataInnerXmlOverride = new XElement(
                    "BILLING_REQUEST",
                    new XAttribute("_ActionType", "RetrieveBillingReport"),
                    new XAttribute("OrderIdentifier", this.ReportID),
                    new XAttribute("LenderCaseIdentifier", this.LenderCaseIdentifier));
            }
        }

        private static string GetRequestTypeOtherDescriptionFor(StandardCreditReportType creditReportType)
        {
            switch (creditReportType)
            {
                case StandardCreditReportType.Premier: return null; // This uses a non-other request type.
                case StandardCreditReportType.SoftQual: return "SoftQual";
                case StandardCreditReportType.MortgageOnly: return "MTGNew";
                case StandardCreditReportType.MortgageOnlyWithScores: return "MTGSNew";
                default:
                    throw new DataAccess.UnhandledEnumException(creditReportType);
            }
        }

        private static string GetRequestTypeOtherDescriptionFor(LqiCreditReportType creditReportType)
        {
            switch (creditReportType)
            {
                case LqiCreditReportType.PreClose: return "PCRNew";
                case LqiCreditReportType.QuickLook: return "QCKNew";
                case LqiCreditReportType.QuickLookWithScores: return "QCKSNew";
                default:
                    throw new DataAccess.UnhandledEnumException(creditReportType);
            }
        }

        public override ICreditReportResponse CreateResponse(XmlDocument doc)
        {
            return new IR_CreditReportResponse(doc, this.brandedProductName);
        }

        /// <summary>
        /// Compares <see cref="Url"/> against <see cref="TEST_URL"/> to check if this instance will
        /// go to their test environment.
        /// </summary>
        private bool IsTest
        {
            get { return IsTestUrl(this.Url); }
        }
    }
}
