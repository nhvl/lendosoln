using System;
using System.Xml;

using LendersOffice.CreditReport;

namespace LendersOffice.CreditReport.Landsafe
{
    public class Landsafe_CreditReportResponse : LendersOffice.CreditReport.Mismo2_3.Mismo2_3CreditReportResponse
    {
        public Landsafe_CreditReportResponse(XmlDocument doc)
            : base(doc)
        {
        }
    }
}
