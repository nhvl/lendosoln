using System;
using System.Net;
using System.Xml;

using LendersOffice.CreditReport;

namespace LendersOffice.CreditReport.Landsafe
{
    /// <summary>
    /// Summary description for Landsafe_CreditReportRequest.
    /// </summary>
    public class Landsafe_CreditReportRequest : LendersOffice.CreditReport.Mismo2_3.Mismo2_3CreditReportRequest
    {
        private const string LANDSAFE_BETA_SERVER = "https://stage.credit.landsafe.com/xml231/creditReport/pricemyloan";
        private const string LANDSAFE_PRODUCTION_SERVER = "https://credit.landsafe.com/xml231/creditReport/pricemyloan";

        public Landsafe_CreditReportRequest()
            : base("")
        {
        }

        public override string Url
        {
            get
            {
                if (LoginInfo.UserName == "PRICEMYLOANSTAGE01")
                {
                    return LANDSAFE_BETA_SERVER;
                }
                else
                {
                    return LANDSAFE_PRODUCTION_SERVER;
                }
            }
        }

        public override ICreditReportResponse CreateResponse(XmlDocument doc)
        {
            return new Landsafe_CreditReportResponse(doc);
        }

        public override void SetupAuthentication(HttpWebRequest webRequest)
        {
            webRequest.ContentType = "text/xml";
        }
    }
}
