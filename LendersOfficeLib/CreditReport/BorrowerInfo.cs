using System;
using CommonLib;
using DataAccess;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using LendersOffice.Common;
using Newtonsoft.Json;

namespace LendersOffice.CreditReport
{
	/// <summary>
	/// Summary description for BorrowerInfo.
	/// </summary>
	public class BorrowerInfo
	{
        private HashSet<string> m_aliases = null;
        private string m_mismoID = "";
        private string m_firstName = "";
        private string m_middleName = "";
        private string m_lastName = "";
        private string m_suffix = "";
        private string m_ssn = "";
        private string m_dob = "";
        private bool m_isCoborrower = false;
        private E_aBMaritalStatT m_maritalStatus;
        private Address m_currentAddress;
        private Address m_previousAddress;
        private Address m_mailingAddress;
        private string m_yearsAtCurrentAddress = "";
        private string m_yearsAtPreviousAddress = "";

        [JsonConstructor]
        private BorrowerInfo(HashSet<string> aliases, string mismoId, string mismoDob, bool isCoborrower)
        {
            this.m_aliases = aliases;
            this.m_mismoID = mismoId;
            this.m_dob = mismoDob;
            this.m_isCoborrower = isCoborrower;
        }

        /// <summary>
        /// Construct borrower information.
        /// </summary>
		public BorrowerInfo() : this(false)
		{
            m_aliases = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
		}

        public BorrowerInfo(bool isCoborrower) 
        {
            m_aliases = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
            m_isCoborrower = isCoborrower;
            if (m_isCoborrower) 
                m_mismoID = "BorRec0002";
            else
                m_mismoID = "BorRec0001";

        }

        public IEnumerable<string> Aliases
        {
            get { return m_aliases; }
        }

        public void AddAlias(string alias)
        {
            if (string.IsNullOrEmpty(alias))
            {
                return;
            }

            alias = alias.TrimWhitespaceAndBOM();

            if (string.IsNullOrEmpty(alias))
            {
                return;
            }

            //av opm 171261 - Remove leading aka or former name. 
            alias = Regex.Replace(alias, "^(AKA:|FORMER NAME:)", "", RegexOptions.IgnoreCase);
            m_aliases.Add(alias);
        }

        /// <summary>
        /// Return true is first name, last name and SSN is not blank
        /// </summary>
        public bool IsValid 
        {
            get 
            {
                return m_firstName.TrimWhitespaceAndBOM() != "" && m_lastName.TrimWhitespaceAndBOM() != "" && m_ssn.TrimWhitespaceAndBOM() != "";
            }
        }
        public string MismoID 
        {
            get { return m_mismoID; }
        }

        public string MismoSsn 
        {
            get { return m_ssn.Replace("-", ""); }
            set 
            {
                // 8/6/2004 dd - Mismo SSN doesn't contain dash. Insert dash here.
                if (null != value && value.Length == 9) 
                {
                    m_ssn = value.Substring(0, 3) + "-" + value.Substring(3, 2) + "-" + value.Substring(5, 4);
                } 
                else 
                {
                    // 8/6/2004 dd - If SSN is incomplete then copy as is.
                    m_ssn = value;
                }
            }
        }

        public string FirstName
        {
            get { return m_firstName; }
            set { m_firstName = value; }
        }

        public string MiddleName
        {
            get { return m_middleName; }
            set { m_middleName = value; }
        }
        
        public string LastName
        {
            get { return m_lastName; }
            set { m_lastName = value; }
        }

        public string Suffix
        {
            get { return m_suffix; }
            set { m_suffix = value; }
        }

        public string Ssn
        {
            get { return m_ssn; }
            set { m_ssn = value; }
        }
        
        public string DOB
        {
            get { return m_dob; }
            set { m_dob = value; }
        }
        public string MismoDOB 
        {
            get 
            {
                string ret = "";
                try 
                {
                    DateTime dt = DateTime.Parse(m_dob);
                    ret = MismoUtilities.ToDateString(dt);
                } 
                catch {}
                return ret;
            }
        }

        public E_aBMaritalStatT MaritalStatus 
        {
            get { return m_maritalStatus; }
            set { m_maritalStatus = value; }
        }


        public string YearsAtCurrentAddress
        {
            get { return m_yearsAtCurrentAddress; }
            set { m_yearsAtCurrentAddress = value; }
        }

        public string YearsAtPreviousAddress
        {
            get { return m_yearsAtPreviousAddress; }
            set { m_yearsAtPreviousAddress = value; }
        }

        /// <summary>
        /// You can only specify borrower/coborrower in constructor.
        /// </summary>
        public bool IsCoborrower
        {
            get { return m_isCoborrower; }
        }


        public Address CurrentAddress 
        {
            get { return m_currentAddress; }
            set { m_currentAddress = value; }
        }

        public Address PreviousAddress
        {
            get { return m_previousAddress; }
            set { m_previousAddress = value; }
        }

        public Address MailingAddress
        {
            get { return m_mailingAddress; }
            set { m_mailingAddress = value; }
        }

        /// <summary>
        /// Copies the values from an existing instance of <see cref="BorrowerInfo"/>. 
        /// </summary>
        /// <param name="src">The instance to copy values from.</param>
        public void CopyFrom(BorrowerInfo src) 
        {
            this.m_mismoID = src.m_mismoID;
            this.m_firstName = src.m_firstName;
            this.m_middleName = src.m_middleName;
            this.m_lastName = src.m_lastName;
            this.m_suffix = src.m_suffix;
            this.m_ssn = src.m_ssn;
            this.m_dob = src.m_dob;
            this.m_isCoborrower = src.m_isCoborrower;
            this.m_maritalStatus = src.m_maritalStatus;
            this.m_currentAddress = src.m_currentAddress;
            this.m_mailingAddress = src.m_mailingAddress;
            this.m_previousAddress = src.m_previousAddress;
            this.m_yearsAtCurrentAddress = src.m_yearsAtCurrentAddress;
            this.m_yearsAtPreviousAddress = src.m_yearsAtPreviousAddress;
        }


	}
}
