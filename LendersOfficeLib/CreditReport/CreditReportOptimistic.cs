using System;
using System.Collections;
using System.Text.RegularExpressions;
using System.Text;
using System.Xml;
using DataAccess;

using LendersOffice.Common;
namespace LendersOffice.CreditReport
{

	public class CCreditReportWithOptimisticDefault : ICreditReport
	{
        #region Constant string for category grouping
        public const string Category_BankAegis = "BankAegis";
        public const string Category_BankDlj = "BankDlj";
        public const string Category_BankImpact = "BankImpact";
        public const string Category_Generic_Chargeoff = "GENERIC - CHARGEOFF";
        public const string Category_Generic_Collection = "GENERIC - COLLECTION";
        public const string Category_Generic_Foreclosure = "GENERIC - FORECLOSURE";
        public const string Category_Generic_Judgment = "GENERIC - JUDGMENT";
        public const string Category_Generic_Late = "GENERIC - LATE";
        public const string Category_Generic_Misc = "GENERIC - MISC";
        public const string Category_Generic_TaxLien = "GENERIC - TAX LIEN";
        public const string Category_Generic_TradeCount = "GENERIC - TRADE COUNT";
        public const string Category_Generic_Derog = "GENERIC - DEROG";
        public const string Category_NewBkMethods = "NEW BK METHODS";
        public const string Category_OldMethods = "OLD METHODS";

        #endregion

        private int m_aProdCrManualNonRolling30MortLateCount;
        private int m_aProdCrManual30MortLateCount;
        private int m_aProdCrManual60MortLateCount;
        private int m_aProdCrManualRolling60MortLateCount;
        private int m_aProdCrManualRolling90MortLateCount;
        private int m_aProdCrManual90MortLateCount;
        private int m_aProdCrManual120MortLateCount;
        private int m_aProdCrManual150MortLateCount;

        private bool m_aProdCrManualForeclosureHas;
        private int m_aProdCrManualForeclosureRecentFileMon;
        private int m_aProdCrManualForeclosureRecentFileYr;
        private int m_aProdCrManualForeclosureRecentSatisfiedMon;
        private int m_aProdCrManualForeclosureRecentSatisfiedYr;
        private E_sProdCrManualDerogRecentStatusT m_aProdCrManualForeclosureRecentStatusT;

        private bool m_aProdCrManualBk7Has;
        private int m_aProdCrManualBk7RecentFileMon;
        private int m_aProdCrManualBk7RecentFileYr;
        private int m_aProdCrManualBk7RecentSatisfiedMon;
        private int m_aProdCrManualBk7RecentSatisfiedYr;
        private E_sProdCrManualDerogRecentStatusT m_aProdCrManualBk7RecentStatusT;

        private  bool m_aProdCrManualBk13Has;
        private int m_aProdCrManualBk13RecentFileMon;
        private int m_aProdCrManualBk13RecentFileYr;
        private int m_aProdCrManualBk13RecentSatisfiedMon;
        private int m_aProdCrManualBk13RecentSatisfiedYr;
        private E_sProdCrManualDerogRecentStatusT m_aProdCrManualBk13RecentStatusT;

        private int m_aBEquifaxScorePe;
        private int m_aCEquifaxScorePe;
        private int m_aBExperianScorePe;
        private int m_aCExperianScorePe;
        private int m_aBTransUnionScorePe;
        private int m_aCTransUnionScorePe;


        private CAppBase m_dataApp = null;
        internal CCreditReportWithOptimisticDefault(CreditReportFactory factory, CAppBase dataApp) 
        {
            if (null == factory) 
            {
                throw new CBaseException(ErrorMessages.Generic, "CCreditReportWithOptimisticDefault can only be instantiate by CreditReportFactory.");
            }
            m_dataApp = dataApp;
            Parse();
        }
        internal void PopulateData
            (
                int aProdCrManualNonRolling30MortLateCount,
                int aProdCrManual30MortLateCount,
                int aProdCrManual60MortLateCount,
                int aProdCrManualRolling60MortLateCount,
                int aProdCrManualRolling90MortLateCount,
                int aProdCrManual90MortLateCount,
                int aProdCrManual120MortLateCount,
                int aProdCrManual150MortLateCount,

                bool aProdCrManualForeclosureHas,
                int aProdCrManualForeclosureRecentFileMon,
                int aProdCrManualForeclosureRecentFileYr,
                int aProdCrManualForeclosureRecentSatisfiedMon,
                int aProdCrManualForeclosureRecentSatisfiedYr,
                E_sProdCrManualDerogRecentStatusT aProdCrManualForeclosureRecentStatusT,

                bool aProdCrManualBk7Has,
                int aProdCrManualBk7RecentFileMon,
                int aProdCrManualBk7RecentFileYr,
                int aProdCrManualBk7RecentSatisfiedMon,
                int aProdCrManualBk7RecentSatisfiedYr,
                E_sProdCrManualDerogRecentStatusT aProdCrManualBk7RecentStatusT,

                bool aProdCrManualBk13Has,
                int aProdCrManualBk13RecentFileMon,
                int aProdCrManualBk13RecentFileYr,
                int aProdCrManualBk13RecentSatisfiedMon,
                int aProdCrManualBk13RecentSatisfiedYr,
                E_sProdCrManualDerogRecentStatusT aProdCrManualBk13RecentStatusT,

                int aBEquifaxScorePe,
                int aCEquifaxScorePe,
                int aBExperianScorePe,
                int aCExperianScorePe,
                int aBTransUnionScorePe,
                int aCTransUnionScorePe

            )
        {
            m_aProdCrManualNonRolling30MortLateCount =            aProdCrManualNonRolling30MortLateCount;
            m_aProdCrManual30MortLateCount =                                aProdCrManual30MortLateCount;
            m_aProdCrManual60MortLateCount =                                aProdCrManual60MortLateCount;
            m_aProdCrManualRolling60MortLateCount = aProdCrManualRolling60MortLateCount;
            m_aProdCrManualRolling90MortLateCount = aProdCrManualRolling90MortLateCount;
            m_aProdCrManual90MortLateCount =                                aProdCrManual90MortLateCount;
            m_aProdCrManual120MortLateCount =                              aProdCrManual120MortLateCount;  
            m_aProdCrManual150MortLateCount =                              aProdCrManual150MortLateCount;  
                                                                                                              
            m_aProdCrManualForeclosureHas =                                   aProdCrManualForeclosureHas;     
            m_aProdCrManualForeclosureRecentFileMon =               aProdCrManualForeclosureRecentFileMon;                         
            m_aProdCrManualForeclosureRecentFileYr =                   aProdCrManualForeclosureRecentFileYr;                     
            m_aProdCrManualForeclosureRecentSatisfiedMon =      aProdCrManualForeclosureRecentSatisfiedMon;                                  
            m_aProdCrManualForeclosureRecentSatisfiedYr =          aProdCrManualForeclosureRecentSatisfiedYr;                              
            m_aProdCrManualForeclosureRecentStatusT =                aProdCrManualForeclosureRecentStatusT;                         
                                                                                                              
            m_aProdCrManualBk7Has =                                                 aProdCrManualBk7Has;
            m_aProdCrManualBk7RecentFileMon =                             aProdCrManualBk7RecentFileMon;           
            m_aProdCrManualBk7RecentFileYr =                                 aProdCrManualBk7RecentFileYr;       
            m_aProdCrManualBk7RecentSatisfiedMon =                    aProdCrManualBk7RecentSatisfiedMon;                    
            m_aProdCrManualBk7RecentSatisfiedYr =                        aProdCrManualBk7RecentSatisfiedYr;                
            m_aProdCrManualBk7RecentStatusT =                              aProdCrManualBk7RecentStatusT;          
                                                                                                              
            m_aProdCrManualBk13Has =                                               aProdCrManualBk13Has;
            m_aProdCrManualBk13RecentFileMon =                           aProdCrManualBk13RecentFileMon;             
            m_aProdCrManualBk13RecentFileYr =                               aProdCrManualBk13RecentFileYr;         
            m_aProdCrManualBk13RecentSatisfiedMon =                  aProdCrManualBk13RecentSatisfiedMon;                      
            m_aProdCrManualBk13RecentSatisfiedYr =                      aProdCrManualBk13RecentSatisfiedYr;                  
            m_aProdCrManualBk13RecentStatusT =                            aProdCrManualBk13RecentStatusT;

            m_aBEquifaxScorePe = aBEquifaxScorePe;
            m_aCEquifaxScorePe = aCEquifaxScorePe;
            m_aBExperianScorePe = aBExperianScorePe;
            m_aCExperianScorePe = aCExperianScorePe;
            m_aBTransUnionScorePe = aBTransUnionScorePe;
            m_aCTransUnionScorePe = aCTransUnionScorePe;
            
            
        }
		#region BankAegis

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mon">1 - 12</param>
        /// <param name="yr">for example: 1997</param>
        /// <param name="months">look back period</param>
        /// <returns></returns>
        private bool IsDateWithin( int mon, int yr, int nMonths )
        {
            DateTime dt = new DateTime( yr, mon, 1 );
            return Tools.IsDateWithin( dt, nMonths );            
        }

        private bool IsEffectiveWithin( int monLookback, E_sProdCrManualDerogRecentStatusT statusT, int monFile, int yrFile, int monSatisfied, int yrSatisfied )
        {
            if( statusT == E_sProdCrManualDerogRecentStatusT.NotSatisfied )
                return true; // still effective

            try
            {
                if( IsDateWithin( monFile, yrFile, monLookback ) )
                    return true;
            }
            catch{}

            try
            {
                if( IsDateWithin( monSatisfied, yrSatisfied, monLookback ) )
                    return true;
            }
            catch{}

            return false;
        }

		/// <summary>
		/// Text: No derog in past 15 months, including 30 day accts, BK, judgments, chargeoffs, CCC 
		/// </summary>
		/// <param name="nMonths"></param>
		/// <returns></returns>
        [CreditDebug(Category_Generic_Derog)]		
		public int HasDerogCountEffectiveWithin( int nMonths )
		{
            if( GetMortgageLates( nMonths ) > 0 )
                return 1;
            return HasMajorDerogCountEffectiveWithin( nMonths );
		}

        [ CreditDebug( Category_Generic_Derog ) ]
        public int TradesCountNonMajorDerog
        {
            get 
            {
                return 15;
            }
        }

        /// <summary>
        /// Text: No derog in past 15 months, BK, judgments, chargeoffs, CCC.  This doesn't include lates.
        /// </summary>
        /// <param name="nMonths"></param>
        /// <returns></returns>
        [CreditDebug(Category_Generic_Derog)]        
        public int HasMajorDerogCountEffectiveWithin( int nMonths )
        {
            if( m_aProdCrManualForeclosureHas )
            {
                if( IsEffectiveWithin( nMonths, m_aProdCrManualForeclosureRecentStatusT, m_aProdCrManualForeclosureRecentFileMon,
                    m_aProdCrManualForeclosureRecentFileYr, m_aProdCrManualForeclosureRecentSatisfiedMon,m_aProdCrManualForeclosureRecentSatisfiedYr ) )
                    return 1;
            }
            
            if( m_aProdCrManualBk7Has )
            {
                if( IsEffectiveWithin( nMonths, m_aProdCrManualBk7RecentStatusT, m_aProdCrManualBk7RecentFileMon, m_aProdCrManualBk7RecentFileYr,
                    m_aProdCrManualBk7RecentSatisfiedMon, m_aProdCrManualBk7RecentSatisfiedYr ) )
                    return 1;
            }
            
            if( m_aProdCrManualBk13Has )
            {
                if( IsEffectiveWithin( nMonths, m_aProdCrManualBk13RecentStatusT,  m_aProdCrManualBk13RecentFileMon, m_aProdCrManualBk13RecentFileYr,
                    m_aProdCrManualBk13RecentSatisfiedMon, m_aProdCrManualBk13RecentSatisfiedYr ) )
                    return 1;
            }

            return 0;
        }
	

		/// <summary>
		/// Text: No judgments or tax Liens past 24 months
		/// </summary>
		/// <param name="nMonths"></param>
		/// <returns></returns>
        [CreditDebug(Category_BankAegis)]
		public int BankAegisHasJudgmentOrTaxLienWithin( int nMonths ) { return 0; }

		
		#endregion // BankAegis

		#region BankImpact


    	/// <summary>
		/// Example: Minimum of 3 trades (All lates based on rolling)
		/// -> Don't know what to do with the one in the parenthesis
		/// <param name="nMonths"></param>
		/// <returns></returns>
        [CreditDebug(Category_Generic_TradeCount)]
		public int TradesOpenCountOlderThan( int nMonths )
		{
			return 100;
		}
		
		/// <summary>
		/// Text: Any medical collections with a cumulative balance over $500 requires payment prior to or at closing
		/// </summary>
		/// <param name="amount"></param>
		/// <returns></returns>
        [CreditDebug(Category_Generic_Derog)]		
		public int TotBalOfMedCollectionsUnpaid 
		{ 
			get { return 0; }
		}

		// 4/17/2007 nw - OPM 12499 - New medical collection keyword
		[CreditDebug(Category_Generic_Derog)]		
		public int TotUnpaidBalOfMedCollectionsWithin( int nMonths ) 
		{ 
			return 0;
		}

		/// <summary>
		/// Text: No Tax Liens in the past 24/12 months
		/// </summary>
		/// <param name="nMonths"></param>
		/// <returns></returns>
        [CreditDebug(Category_BankImpact)]		
		public int BankImpactHasTaxLienWithin( int nMonths )
		{
			return 0;
		}

		/// <summary>
		/// 7/6/2006 nw - OPM 6120 - new keyword "HasTaxLienFiledWithin"
		/// Text: No Tax Liens Filed in the past 24/12 months
		/// </summary>
		/// <param name="nMonths"></param>
		/// <returns></returns>
		[CreditDebug(Category_BankImpact)]		
		public int BankImpactHasTaxLienFiledWithin( int nMonths )
		{
			return 0;
		}


			

		#endregion // BankImpact

        private E_aBorrowerCreditModeT m_aBorrowerCreditModeT = E_aBorrowerCreditModeT.Both;

        public E_aBorrowerCreditModeT aBorrowerCreditModeT
        {
            get { return m_aBorrowerCreditModeT; }
            set { m_aBorrowerCreditModeT = value; }
        }

		#region Late Calculations
        [CreditDebug(Category_Generic_Late)]
		public int GetInstallmentLates30IntermittentProgressiveRollingCountWithin( int nMonths )
		{
			return 0; 
		}
        [CreditDebug(Category_Generic_Late)]
		public int GetInstallmentLates30IntermittentProgressiveNonrollingCountWithin( int nMonths )
		{
			return 0;
		}
        [CreditDebug(Category_Generic_Late)]
		public int GetRevolvingLates30IntermittentProgressiveRollingCountWithin( int nMonths )
		{
			return 0; 
		}
        [CreditDebug(Category_Generic_Late)]
		public int GetRevolvingLates30IntermittentProgressiveNonrollingCountWithin( int nMonths )
		{
			return 0;
		}
        [CreditDebug(Category_Generic_Late)]
		public int GetMortgageLates30IntermittentProgressiveRollingCountWithin( int nMonths )
		{
			return m_aProdCrManual30MortLateCount;
		}
		[CreditDebug(Category_Generic_Late)]
		public int GetMortgageLates60IntermittentProgressiveRollingCountWithin( int nMonths )
		{
            // 5/12/2006 dd - OPM 4890
			//return m_aProdCrManual60MortLateCount;
            return m_aProdCrManualRolling60MortLateCount;
		}

        [CreditDebug(Category_Generic_Late)]
        public int GetMortgageLates90IntermittentProgressiveRollingCountWithin( int nMonths ) 
        {
            return m_aProdCrManualRolling90MortLateCount;
        }

        [CreditDebug(Category_Generic_Late)]
		public int GetRevolvingLates60IntermittentProgressiveNonrollingCountWithin( int nMonths )
		{
			return 0;
		}
        [CreditDebug(Category_Generic_Late)]
		public int GetRevolvingLates90IntermittentProgressiveNonrollingCountWithin( int nMonths )
		{
			return 0;
		}
        [CreditDebug(Category_Generic_Late)]
		public int GetRevolvingLates120IntermittentProgressiveNonrollingCountWithin( int nMonths )
		{
			return 0;
		}
        [CreditDebug(Category_Generic_Late)]
		public int GetRevolvingLates150IntermittentProgressiveNonrollingCountWithin( int nMonths )
		{
			return 0;
		}
        [CreditDebug(Category_Generic_Late)]
		public int GetRevolvingLates60IntermittentProgressiveRollingCountWithin( int nMonths )
		{
			return 0;
		}
        [CreditDebug(Category_Generic_Late)]
		public int GetRevolvingLates90IntermittentProgressiveRollingCountWithin( int nMonths )
		{
			return 0;
		}
        [CreditDebug(Category_Generic_Late)]
		public int GetRevolvingLates120IntermittentProgressiveRollingCountWithin( int nMonths )
		{
			return 0;
		}
        [CreditDebug(Category_Generic_Late)]
		public int GetRevolvingLates150IntermittentProgressiveRollingCountWithin( int nMonths )
		{
			return 0;
		}
        [CreditDebug(Category_Generic_Late)]
		public int GetInstallmentLates60IntermittentProgressiveNonrollingCountWithin( int nMonths )
		{
			return 0;
		}
        [CreditDebug(Category_Generic_Late)]
		public int GetInstallmentLates90IntermittentProgressiveNonrollingCountWithin( int nMonths )
		{
			return 0;
		}
        [CreditDebug(Category_Generic_Late)]
		public int GetInstallmentLates120IntermittentProgressiveNonrollingCountWithin( int nMonths )
		{
			return 0;
		}
        [CreditDebug(Category_Generic_Late)]
		public int GetInstallmentLates150IntermittentProgressiveNonrollingCountWithin( int nMonths )
		{
			return 0;
		}
        [CreditDebug(Category_Generic_Late)]
		public int GetInstallmentLates60IntermittentProgressiveRollingCountWithin( int nMonths )
		{
			return 0;
		}
        [CreditDebug(Category_Generic_Late)]
		public int GetInstallmentLates90IntermittentProgressiveRollingCountWithin( int nMonths )
		{
			return 0;
		}
        [CreditDebug(Category_Generic_Late)]
		public int GetInstallmentLates120IntermittentProgressiveRollingCountWithin( int nMonths )
		{
			return 0;
		}
        [CreditDebug(Category_Generic_Late)]
		public int GetInstallmentLates150IntermittentProgressiveRollingCountWithin( int nMonths )
		{
			return 0;
		}
        [CreditDebug(Category_Generic_Late)]
        public int GetLeaseLates30IntermittentProgressiveRollingCountWithin(int nMonths)
        {
            return 0;
        }
        [CreditDebug(Category_Generic_Late)]
        public int GetLeaseLates30IntermittentProgressiveNonrollingCountWithin(int nMonths)
        {
            return 0;
        }
        [CreditDebug(Category_Generic_Late)]
        public int GetLeaseLates60IntermittentProgressiveRollingCountWithin(int nMonths)
        {
            return 0;
        }
        [CreditDebug(Category_Generic_Late)]
        public int GetLeaseLates60IntermittentProgressiveNonrollingCountWithin(int nMonths)
        {
            return 0;
        }
        [CreditDebug(Category_Generic_Late)]
        public int GetLeaseLates90IntermittentProgressiveRollingCountWithin(int nMonths)
        {
            return 0;
        }
        [CreditDebug(Category_Generic_Late)]
        public int GetLeaseLates90IntermittentProgressiveNonrollingCountWithin(int nMonths)
        {
            return 0;
        }
        [CreditDebug(Category_Generic_Late)]
        public int GetHelocLates30IntermittentProgressiveRollingCountWithin(int nMonths)
        {
            return 0;
        }
        [CreditDebug(Category_Generic_Late)]
        public int GetHelocLates30IntermittentProgressiveNonrollingCountWithin(int nMonths)
        {
            return 0;
        }
        [CreditDebug(Category_Generic_Late)]
        public int GetHelocLates60IntermittentProgressiveRollingCountWithin(int nMonths)
        {
            return 0;
        }
        [CreditDebug(Category_Generic_Late)]
        public int GetHelocLates60IntermittentProgressiveNonrollingCountWithin(int nMonths)
        {
            return 0;
        }
        [CreditDebug(Category_Generic_Late)]
        public int GetHelocLates90IntermittentProgressiveRollingCountWithin(int nMonths)
        {
            return 0;
        }
        [CreditDebug(Category_Generic_Late)]
        public int GetHelocLates90IntermittentProgressiveNonrollingCountWithin(int nMonths)
        {
            return 0;
        }
        #endregion

        /// <summary>
        /// 6/15/2006 - nw - OPM 4822.  Returns 1 if tradeline remark indicates that foreclosure was started.  
        /// Parameter nMonths isn't used yet.
        /// </summary>
        /// <param name="nMonths"></param>
        /// <returns></returns>
        [CreditDebug(Category_Generic_Derog)]        
		public int MightHaveUndated120Within( int nMonths )
		{
			return 0;
		}

		[CreditDebug(Category_Generic_Collection)]
        public bool IsInCreditCounseling 
        {
            get { return false; }
        }

		/// <summary>
		/// Count number of tradelines that has rating greater than or equal to number of months.
		/// </summary>
		/// <param name="month"></param>
		/// <returns></returns>
		public int GetCountOfTradesRatedAtLeast(int month)
		{
			return 100;
		}

		public int GetCountOfTradesWith5KCreditHighAndRatedFor(int month)
		{
			return 100;
		}

        /// <summary>
        /// Returns the number of short sales that occured within n Months.
        /// </summary>
        /// <param name="months">Number of months short sales must be within to be counted.</param>
        /// <returns>The number of short sales that occured within the given number of months.</returns>
        /// <remarks>OPM 24318.</remarks>
        [CreditDebug(Category_Generic_Misc)]
        virtual public int GetShortSales(int months)
        {
            return 0;
        }

        /// <summary>
        /// Get the total number of late payments of all mortgage account in the past n months.
        /// </summary>
        /// <param name="month"></param>
        /// <returns></returns>
        [CreditDebug(Category_OldMethods)]		
		public int GetMortgageLates(int month)
		{
            return GetMortgage30Lates( month ) + 
                GetMortgage60Lates( month ) +
                GetMortgage90PlusLates( month );
		}

		/// <summary>
		/// Get the total number of 30 days late payments of all mortgage account in the past n months. Not rolling.
		/// </summary>
		/// <param name="month"></param>
		/// <returns></returns>
        [CreditDebug(Category_OldMethods)]		
		public int GetMortgage30Lates(int month)
		{
            // Taking the max because we added the rolling one much later.  Opm case Existing files won't have the non-rolling.
            // Technically the non-rolling count always greater than or equal to rolling count.
			return Math.Max( m_aProdCrManual30MortLateCount, m_aProdCrManualNonRolling30MortLateCount );
		}

		/// <summary>
		/// Get the total number of 60 days late payments of all mortgage account in the past n months.
		/// </summary>
		/// <param name="month"></param>
		/// <returns></returns>
        [CreditDebug(Category_OldMethods)]		
		public int GetMortgage60Lates(int month)
		{
            // Taking the max because we added the rolling one much later.  Opm case Existing files won't have the non-rolling.
            // Technically the non-rolling count always greater than or equal to rolling count.

			return Math.Max(m_aProdCrManual60MortLateCount, m_aProdCrManualRolling60MortLateCount);
		}

		/// <summary>
		/// Get the total number of 90+ days late payments of all mortgage account in the past n months.
		/// </summary>
		/// <param name="month"></param>
		/// <returns></returns>
        [CreditDebug(Category_OldMethods)]		
		public int GetMortgage90PlusLates(int month)
		{
			return GetMortgage90Lates( month ) + GetMortgage120Lates( month ) + GetMortgage150PlusLates( month );
		}

		/// <summary>
		/// Get the total number of 90 days late payments of all mortgage account in the past n months.
		/// </summary>
		/// <param name="month"></param>
		/// <returns></returns>
        [CreditDebug(Category_OldMethods)]		
		public int GetMortgage90Lates(int month)
		{
			return m_aProdCrManual90MortLateCount;
		}

		/// <summary>
		/// Get the total number of 120 days late payments of all mortgage account in the past n months.
		/// </summary>
		/// <param name="month"></param>
		/// <returns></returns>
        [CreditDebug(Category_OldMethods)]
		public int GetMortgage120Lates(int month)
		{
			return m_aProdCrManual120MortLateCount;
		}

		/// <summary>
		/// Get the total number of 150+ days late payments of all mortgage account in the past n months.
		/// </summary>
		/// <param name="month"></param>
		/// <returns></returns>
        [CreditDebug(Category_OldMethods)]		
		public int GetMortgage150PlusLates(int month)
		{
			return m_aProdCrManual150MortLateCount;
		}

		/// <summary>
		/// Get the total number of 30 days late payments of all installment account in the past n months.
		/// </summary>
		/// <param name="month"></param>
		/// <returns></returns>
        [CreditDebug(Category_OldMethods)]		
		public int GetInstallment30Lates(int month)
		{
			return 0;
		}

		/// <summary>
		/// Get the total number of 60 days late payments of all installment account in the past n months.
		/// </summary>
		/// <param name="month"></param>
		/// <returns></returns>
        [CreditDebug(Category_OldMethods)]		
		public int GetInstallment60Lates(int month)
		{
			return 0;
		}

		/// <summary>
		/// Get the total number of 90+ days late payments of all installment account in the past n months.
		/// </summary>
		/// <param name="month"></param>
		/// <returns></returns>
        [CreditDebug(Category_OldMethods)]		
		public int GetInstallment90PlusLates(int month)
		{
			return 0;
		}

		/// <summary>
		/// Get the total number of 30 days late payments of all revolving account in the past n months.
		/// </summary>
		/// <param name="month"></param>
		/// <returns></returns>
        [CreditDebug(Category_OldMethods)]
        public int GetRevolving30Lates(int month)
		{
			return 0;
		}

		/// <summary>
		/// Get the total number of 60 days late payments of all revolving account in the past n months.
		/// </summary>
		/// <param name="month"></param>
		/// <returns></returns>
        [CreditDebug(Category_OldMethods)]
		public int GetRevolving60Lates(int month)
		{
			return 0;
		}


		/// <summary>
		/// Get the total number of 90+ days late payments of all revolving account in the past n months.
		/// </summary>
		/// <param name="month"></param>
		/// <returns></returns>
        [CreditDebug(Category_OldMethods)]
		public int GetRevolving90PlusLates(int month)
		{
			return 0;
        }

        /// <summary>
        /// Get the total number of late payments for the given debt type, with lateType number of days late, in the past n months.
        /// </summary>
        /// <param name="nMonths">The number of months to scan.</param>
        /// <param name="type">The debt type to check (usually Installment, Revolving, or Mortgage).</param>
        /// <param name="lateType">The type of lates to include.</param>
        /// <param name="rolling">Whether to count rolling late payments as single lates or multiple.</param>
        /// <returns>The number of lates found.</returns>
        [CreditDebug(Category_Generic_Late)]
        public int GetNumberOfLates(int nMonths, E_DebtRegularT type, E_AdverseType lateType, bool rolling)
        {
            return 0;
        }


        /// <summary>
        /// Get number of account with foreclosure status '82' in the past n months
        /// </summary>
        /// <param name="month"></param>
        /// <returns></returns>
        [CreditDebug(Category_Generic_Foreclosure)]
		public int GetForeclosure(int nMonths )
		{
            if( m_aProdCrManualForeclosureHas )
            {
                if( IsEffectiveWithin( nMonths,  m_aProdCrManualForeclosureRecentStatusT, m_aProdCrManualForeclosureRecentFileMon, m_aProdCrManualForeclosureRecentFileYr,
                            m_aProdCrManualForeclosureRecentSatisfiedMon, m_aProdCrManualForeclosureRecentSatisfiedYr ) )
                    return 1;
            }
            return 0;
		}

        /// <summary>
        /// 2/22/13 gf - OPM 75677 - Changed the default value of Count120 to false.
        /// 8/23/2006 nw - OPM 7126 - New Foreclosure Keyword, accepts 2 parameters:
        /// a)int  Within:   default is 1000, this is to specify look-back period of when the derog event occurs.
        /// b)bool Count120: default is false, 120 means 120+ late.
        /// Returns the number of mortgage tradelines that satisfy the parameters.
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        [CreditDebug(Category_Generic_Foreclosure)]        
        public int FcCountX(string parameters)
        {
            // Months reviewed: Within
            string withinOp = "<=";
            int withinMonths = 1000;

            // Count 120+ late: Count120
            string count120Op = "=";
            bool count120Val = false; // y, n

            // Supported operators are : <, <=, >, >=, =, but so far this keyword just needs <= and =
            char[] op_chars = new  char[] { '<', '>', '=' };

            foreach( string attribute in parameters.ToLower().Split( '&' ) )
            {
                if( attribute == "," || attribute == "" )
                    continue;

                string name = "";
                string op = "";
                string val = "";
				
                // Extracting name
                int nameLen = attribute.IndexOfAny( op_chars );
                name = attribute.Substring( 0, nameLen );

                // Extracting operator
                StringBuilder opBuilder = new StringBuilder( 5 );
                Match match = System.Text.RegularExpressions.Regex.Match( attribute, "[<>=]"  );
                while( match.Success )
                {
                    string token = match.Groups[0].Value;
                    opBuilder.Append( token );
                    match = match.NextMatch();
                }
                op = opBuilder.ToString();

                // Extracting value
                try
                {
                    val = attribute.Substring( nameLen + op.Length );
                }
                catch{} // must be an invalid format 

                if( name == "" || op == "" || val == "" )
                    Tools.LogBug( string.Format( "None of these can be empty. Name={0}. Op={1}. Val={2}", name, op, val ) );

                switch( name )
                {
                    case "within":
                        if( op != "<=" )
                            throw new CBaseException( ErrorMessages.Generic, string.Format( "{0} is not a supported operator for parameter Within in keyword FcCountX", op ) );
                        withinOp = op;
						
                        try
                        {
                            withinMonths = int.Parse( val );
                            if( withinMonths < 0 )
                                throw new CBaseException( ErrorMessages.Generic, "Months reviewed (withinMonths) was < 0." );
                        }
                        catch
                        {
                            throw new CBaseException( ErrorMessages.Generic, string.Format( "{0} is not a supported argument for parameter Within in keyword FcCountX", val ) );
                        }
                        break;
                    case "count120":
                        if( op != "=" )
                            throw new CBaseException( ErrorMessages.Generic, string.Format( "{0} is not a supported operator for parameter Count120 in keyword FcCountX", op ) );
                        count120Op = op;

                        switch( val )
                        {
                            case "y": count120Val = true; break;
                            case "n": count120Val = false; break;
                            default: throw new CBaseException( ErrorMessages.Generic, string.Format( "{0} is not a supported argument for parameter Count120 in keyword FcCountX", val ) );
                        }
                        break;
                    default:
                        string s = string.Format( "Error encountered in FcCountX keyword, parameter name:{0} is not valid", name );
                        Tools.LogErrorWithCriticalTracking( s );
                        throw new CBaseException( ErrorMessages.Generic, s );					
                }
            }

            if ( m_aProdCrManualForeclosureHas )
            {
                if ( IsEffectiveWithin( withinMonths,  m_aProdCrManualForeclosureRecentStatusT, m_aProdCrManualForeclosureRecentFileMon, m_aProdCrManualForeclosureRecentFileYr,
                    m_aProdCrManualForeclosureRecentSatisfiedMon, m_aProdCrManualForeclosureRecentSatisfiedYr ) )
                    return 1;
            }

            if ( count120Val )
                // we have to ignore the Within value for the lates
                if ( m_aProdCrManual120MortLateCount > 0 || m_aProdCrManual150MortLateCount > 0 )
                    return 1;

            return 0;
        }


		/// <summary>
        /// 2/22/2013 gf - OPM 75677 - Changed default value of Count120 to false.
		/// 8/25/2006 nw - OPM 7126 - New Foreclosure Keyword HasFcX, accepts 2 parameters:
		/// a)int  Within:   default is 1000, this is to specify look-back period of when the derog event occurs.
		/// b)bool Count120: default is false, 120 means 120+ late.
		/// Returns 1 if true, 0 if false.
		/// </summary>
		/// <param name="parameters"></param>
		/// <returns></returns>
		[CreditDebug(Category_Generic_Foreclosure)]        
		public int HasFcX(string parameters)
		{
			// Months reviewed: Within
			string withinOp = "<=";
			int withinMonths = 1000;

			// Count 120+ late: Count120
			string count120Op = "=";
			bool count120Val = false; // y, n

			// Supported operators are : <, <=, >, >=, =, but so far this keyword just needs <= and =
			char[] op_chars = new  char[] { '<', '>', '=' };

			foreach( string attribute in parameters.ToLower().Split( '&' ) )
			{
				if( attribute == "," || attribute == "" )
					continue;

				string name = "";
				string op = "";
				string val = "";
				
				// Extracting name
				int nameLen = attribute.IndexOfAny( op_chars );
				name = attribute.Substring( 0, nameLen );

				// Extracting operator
				StringBuilder opBuilder = new StringBuilder( 5 );
				Match match = System.Text.RegularExpressions.Regex.Match( attribute, "[<>=]"  );
				while( match.Success )
				{
					string token = match.Groups[0].Value;
					opBuilder.Append( token );
					match = match.NextMatch();
				}
				op = opBuilder.ToString();

				// Extracting value
				try
				{
					val = attribute.Substring( nameLen + op.Length );
				}
				catch{} // must be an invalid format 

				if( name == "" || op == "" || val == "" )
					Tools.LogBug( string.Format( "None of these can be empty. Name={0}. Op={1}. Val={2}", name, op, val ) );

				switch( name )
				{
					case "within":
						if( op != "<=" )
							throw new CBaseException( ErrorMessages.Generic, string.Format( "{0} is not a supported operator for parameter Within in keyword HasFcX", op ) );
						withinOp = op;
						
						try
						{
							withinMonths = int.Parse( val );
							if( withinMonths < 0 )
								throw new CBaseException( ErrorMessages.Generic, "Months reviewed (withinMonths) was < 0." );
						}
						catch
						{
							throw new CBaseException( ErrorMessages.Generic, string.Format( "{0} is not a supported argument for parameter Within in keyword HasFcX", val ) );
						}
						break;
					case "count120":
						if( op != "=" )
							throw new CBaseException( ErrorMessages.Generic, string.Format( "{0} is not a supported operator for parameter Count120 in keyword HasFcX", op ) );
						count120Op = op;

					switch( val )
					{
						case "y": count120Val = true; break;
						case "n": count120Val = false; break;
						default: throw new CBaseException( ErrorMessages.Generic, string.Format( "{0} is not a supported argument for parameter Count120 in keyword HasFcX", val ) );
					}
						break;
					default:
						string s = string.Format( "Error encountered in HasFcX keyword, parameter name:{0} is not valid", name );
						Tools.LogErrorWithCriticalTracking( s );
						throw new CBaseException( ErrorMessages.Generic, s );					
				}
			}

			if ( m_aProdCrManualForeclosureHas )
			{
				if ( IsEffectiveWithin( withinMonths,  m_aProdCrManualForeclosureRecentStatusT, m_aProdCrManualForeclosureRecentFileMon, m_aProdCrManualForeclosureRecentFileYr,
					m_aProdCrManualForeclosureRecentSatisfiedMon, m_aProdCrManualForeclosureRecentSatisfiedYr ) )
					return 1;
			}

			if ( count120Val )
				// we have to ignore the Within value for the lates
				if ( m_aProdCrManual120MortLateCount > 0 || m_aProdCrManual150MortLateCount > 0 )
					return 1;

			return 0;
		}

		public CDateTime DischargedDOfLastForeclosure	
		{ 
			get
			{
				if( !m_aProdCrManualForeclosureHas )
					return CDateTime.InvalidWrapValue;

				return CDateTime.Create( new DateTime( m_aProdCrManualForeclosureRecentSatisfiedYr, m_aProdCrManualForeclosureRecentSatisfiedMon, 1 ));
			}
		}

		public CDateTime FileDOfLastForeclosure
		{ 
			get
			{
				if( !m_aProdCrManualForeclosureHas )
					return CDateTime.InvalidWrapValue;

				return CDateTime.Create( new DateTime( m_aProdCrManualForeclosureRecentFileYr, m_aProdCrManualForeclosureRecentFileMon, 1 ));
			}
		}

		/// <summary>
		/// return 1 if there is a bk (either discharged or filed) in the past n months; else return 0
		/// </summary>
		/// <param name="month"></param>
		/// <returns></returns>
        [CreditDebug(Category_OldMethods)]		
		public int GetBankruptcy(int nMonths )
		{
            if( m_aProdCrManualBk7Has )
            {
                if( IsEffectiveWithin( nMonths, m_aProdCrManualBk7RecentStatusT, m_aProdCrManualBk7RecentFileMon, m_aProdCrManualBk7RecentFileYr,
                        m_aProdCrManualBk7RecentSatisfiedMon, m_aProdCrManualBk7RecentSatisfiedYr ) )
                    return 1;
            }

            if( m_aProdCrManualBk13Has )
            {
                if( IsEffectiveWithin( nMonths, m_aProdCrManualBk13RecentStatusT, m_aProdCrManualBk13RecentFileMon, m_aProdCrManualBk13RecentFileYr,
                    m_aProdCrManualBk13RecentSatisfiedMon, m_aProdCrManualBk13RecentSatisfiedYr ) )
                    return 1;
            }

            return 0;
		}

		/// <summary>
		/// Get number of accounts with charge off status 90 in the past n months.
		/// </summary>
		/// <param name="nMonths"></param>
		/// <returns></returns>
        [CreditDebug(Category_OldMethods)]		
		public int GetChargeOff(int nMonths )
		{
			return 0;
		}

		/// <summary>
		/// Return total of all charge-offs, collection tradelines.
		/// </summary>
		/// <returns></returns>
        [CreditDebug(Category_OldMethods)]		
		public decimal DerogatoryAmountTotal 
		{
			get{ return 0; }
		}


		#region NEW BK METHODS

        [CreditDebug(Category_NewBkMethods)]
        public E_BkMultipleT BkMultipleType
        {
            get
            {
                if( m_aProdCrManualBk13Has && m_aProdCrManualBk7Has )
                {
                    try
                    {
                        DateTime dt13 = new DateTime( m_aProdCrManualBk13RecentFileYr, m_aProdCrManualBk13RecentFileMon, 1 );
                        DateTime dt7 = new DateTime( m_aProdCrManualBk7RecentFileYr, m_aProdCrManualBk7RecentFileMon, 1 );
                        if( dt13.CompareTo( dt7 ) <= 0 )
                            return E_BkMultipleT.PossibleRollOver;
                    }
                    catch{}
                    return E_BkMultipleT.Multiple;                    
                }
                return E_BkMultipleT.None;
            }
        }
		[CreditDebug(Category_NewBkMethods)] //av opm 25329
		public bool HasMortgageIncludedInBKWithin ( int nMonths ) 
		{
			return false; 
		}
        [CreditDebug(Category_NewBkMethods)]
        public int HasDischargedBkWithin( int nMonths )
        { 
            if( ( HasDischarged7BkWithin( nMonths ) != 0 ) || ( HasDischarged11BkWithin( nMonths ) != 0 ) || 
                    ( HasDischarged12BkWithin(  nMonths ) != 0 ) ||  ( HasDischarged13BkWithin(  nMonths ) != 0 ) || 
                    ( HasDischargedUnknownTypeBkWithin( nMonths ) != 0 ) )
                return 1;
            return 0;
        }
        
		[CreditDebug(Category_NewBkMethods)]
		public int HasDischarged7BkWithin( int nMonths ) 
        { 
            if(  m_aProdCrManualBk7Has )
			{
				switch( m_aProdCrManualBk7RecentStatusT )
				{
					case E_sProdCrManualDerogRecentStatusT.Discharged:
					case E_sProdCrManualDerogRecentStatusT.Dismissed:
						if( IsEffectiveWithin( nMonths, m_aProdCrManualBk7RecentStatusT, m_aProdCrManualBk7RecentFileMon,
							m_aProdCrManualBk7RecentFileYr, m_aProdCrManualBk7RecentSatisfiedMon, m_aProdCrManualBk7RecentSatisfiedYr ) )
							return 1;
						break;
					case E_sProdCrManualDerogRecentStatusT.NotSatisfied:
						break;
					default:
						throw new CBaseException( ErrorMessages.Generic, "Unhandled enum value of E_sProdCrManualDerogRecentStatusT in CCreditReportWithOptimisticDefault.HasDischarged7BkWithin()" );
				}
            }
            return 0;
        }

        [CreditDebug(Category_NewBkMethods)]
		public int HasDischarged11BkWithin( int nMonths ) { return 0; }
        [CreditDebug(Category_NewBkMethods)]
		public int HasDischarged12BkWithin( int nMonths ) { return 0; }
        [CreditDebug(Category_NewBkMethods)]		
        public int HasDischarged13BkWithin( int nMonths ) 
        { 
            if(  m_aProdCrManualBk13Has )
			{
				switch( m_aProdCrManualBk13RecentStatusT )
				{
					case E_sProdCrManualDerogRecentStatusT.Discharged:
					case E_sProdCrManualDerogRecentStatusT.Dismissed:
						if( IsEffectiveWithin( nMonths, m_aProdCrManualBk13RecentStatusT, m_aProdCrManualBk13RecentFileMon,
							m_aProdCrManualBk13RecentFileYr, m_aProdCrManualBk13RecentSatisfiedMon, m_aProdCrManualBk13RecentSatisfiedYr ) )
							return 1;
						break;
					case E_sProdCrManualDerogRecentStatusT.NotSatisfied:
							return 0;
					default:
							throw new CBaseException( ErrorMessages.Generic, "Unhandled enum value of E_sProdCrManualDerogRecentStatusT in CCreditReportWithOptimisticDefault.HasDischarged13BkWithin()" );
				}
            }
            return 0;
        }

		// This will allow us to generate stip for further review
        [CreditDebug(Category_NewBkMethods)]
		public int HasDischargedUnknownTypeBkWithin( int nMonths ) { return 0; }


		// 7/3/2006 nw - OPM 5752 - New Sets of BK Keywords
		#region HasClosedBkWithin, counts both discharged and dismissed BK
		[CreditDebug(Category_NewBkMethods)]
		public int HasClosedBkWithin( int nMonths )
		{ 
			if( ( HasClosed7BkWithin( nMonths ) != 0 ) || ( HasClosed11BkWithin( nMonths ) != 0 ) || 
				( HasClosed12BkWithin(  nMonths ) != 0 ) ||  ( HasClosed13BkWithin(  nMonths ) != 0 ) || 
				( HasClosedUnknownTypeBkWithin( nMonths ) != 0 ) )
				return 1;
			return 0;
		}
        
		[CreditDebug(Category_NewBkMethods)]
		public int HasClosed7BkWithin( int nMonths ) 
		{ 
			if(  m_aProdCrManualBk7Has )
			{
				switch( m_aProdCrManualBk7RecentStatusT )
				{
					case E_sProdCrManualDerogRecentStatusT.Discharged:
					case E_sProdCrManualDerogRecentStatusT.Dismissed:
						if( IsEffectiveWithin( nMonths, m_aProdCrManualBk7RecentStatusT, m_aProdCrManualBk7RecentFileMon,
							m_aProdCrManualBk7RecentFileYr, m_aProdCrManualBk7RecentSatisfiedMon, m_aProdCrManualBk7RecentSatisfiedYr ) )
							return 1;
						break;
					case E_sProdCrManualDerogRecentStatusT.NotSatisfied:
						break;
					default:
						throw new CBaseException( ErrorMessages.Generic, "Unhandled enum value of E_sProdCrManualDerogRecentStatusT in CCreditReportWithOptimisticDefault.HasClosed7BkWithin()" );
				}
			}
			return 0;
		}

		[CreditDebug(Category_NewBkMethods)]
		public int HasClosed11BkWithin( int nMonths ) { return 0; }
		[CreditDebug(Category_NewBkMethods)]
		public int HasClosed12BkWithin( int nMonths ) { return 0; }
		[CreditDebug(Category_NewBkMethods)]		
		public int HasClosed13BkWithin( int nMonths ) 
		{ 
			if(  m_aProdCrManualBk13Has )
			{
				switch( m_aProdCrManualBk13RecentStatusT )
				{
					case E_sProdCrManualDerogRecentStatusT.Discharged:
					case E_sProdCrManualDerogRecentStatusT.Dismissed:
						if( IsEffectiveWithin( nMonths, m_aProdCrManualBk13RecentStatusT, m_aProdCrManualBk13RecentFileMon,
							m_aProdCrManualBk13RecentFileYr, m_aProdCrManualBk13RecentSatisfiedMon, m_aProdCrManualBk13RecentSatisfiedYr ) )
							return 1;
						break;
					case E_sProdCrManualDerogRecentStatusT.NotSatisfied:
						break;
					default:
						throw new CBaseException( ErrorMessages.Generic, "Unhandled enum value of E_sProdCrManualDerogRecentStatusT in CCreditReportWithOptimisticDefault.HasClosed13BkWithin()" );
				}
			}
			return 0;
		}

		// This will allow us to generate stip for further review
		[CreditDebug(Category_NewBkMethods)]
		public int HasClosedUnknownTypeBkWithin( int nMonths ) { return 0; }
		#endregion HasClosedBkWithin, counts both dismissed and discharged BK


		#region HasDismissedBkWithin, counts only the dismissed BK
		[CreditDebug(Category_NewBkMethods)]
		public int HasDismissedBkWithin( int nMonths )
		{ 
			if( ( HasDismissed7BkWithin( nMonths ) != 0 ) || ( HasDismissed11BkWithin( nMonths ) != 0 ) || 
				( HasDismissed12BkWithin(  nMonths ) != 0 ) ||  ( HasDismissed13BkWithin(  nMonths ) != 0 ) || 
				( HasDismissedUnknownTypeBkWithin( nMonths ) != 0 ) )
				return 1;
			return 0;
		}
        
		[CreditDebug(Category_NewBkMethods)]
		public int HasDismissed7BkWithin( int nMonths ) 
		{ 
			if(  m_aProdCrManualBk7Has )
			{
				switch( m_aProdCrManualBk7RecentStatusT )
				{
					case E_sProdCrManualDerogRecentStatusT.Discharged:
						break;
					case E_sProdCrManualDerogRecentStatusT.Dismissed:
						if( IsEffectiveWithin( nMonths, m_aProdCrManualBk7RecentStatusT, m_aProdCrManualBk7RecentFileMon,
							m_aProdCrManualBk7RecentFileYr, m_aProdCrManualBk7RecentSatisfiedMon, m_aProdCrManualBk7RecentSatisfiedYr ) )
							return 1;
						break;
					case E_sProdCrManualDerogRecentStatusT.NotSatisfied:
						break;
					default:
						throw new CBaseException( ErrorMessages.Generic, "Unhandled enum value of E_sProdCrManualDerogRecentStatusT in CCreditReportWithOptimisticDefault.HasDismissed7BkWithin()" );
				}
			}
			return 0;
		}

		[CreditDebug(Category_NewBkMethods)]
		public int HasDismissed11BkWithin( int nMonths ) { return 0; }
		[CreditDebug(Category_NewBkMethods)]
		public int HasDismissed12BkWithin( int nMonths ) { return 0; }
		[CreditDebug(Category_NewBkMethods)]		
		public int HasDismissed13BkWithin( int nMonths ) 
		{ 
			if(  m_aProdCrManualBk13Has )
			{
				switch( m_aProdCrManualBk13RecentStatusT )
				{
					case E_sProdCrManualDerogRecentStatusT.Discharged:
						break;
					case E_sProdCrManualDerogRecentStatusT.Dismissed:
						if( IsEffectiveWithin( nMonths, m_aProdCrManualBk13RecentStatusT, m_aProdCrManualBk13RecentFileMon,
							m_aProdCrManualBk13RecentFileYr, m_aProdCrManualBk13RecentSatisfiedMon, m_aProdCrManualBk13RecentSatisfiedYr ) )
							return 1;
						break;
					case E_sProdCrManualDerogRecentStatusT.NotSatisfied:
						break;
					default:
						throw new CBaseException( ErrorMessages.Generic, "Unhandled enum value of E_sProdCrManualDerogRecentStatusT in CCreditReportWithOptimisticDefault.HasDismissed13BkWithin()" );
				}
			}
			return 0;
		}

		// This will allow us to generate stip for further review
		[CreditDebug(Category_NewBkMethods)]
		public int HasDismissedUnknownTypeBkWithin( int nMonths ) { return 0; }
		#endregion HasDismissedBkWithin, counts only the dismissed BK


		#region HasDischBkWithin, counts only the discharged BK
		[CreditDebug(Category_NewBkMethods)]
		public int HasDischBkWithin( int nMonths )
		{ 
			if( ( HasDisch7BkWithin( nMonths ) != 0 ) || ( HasDisch11BkWithin( nMonths ) != 0 ) || 
				( HasDisch12BkWithin(  nMonths ) != 0 ) ||  ( HasDisch13BkWithin(  nMonths ) != 0 ) || 
				( HasDischUnknownTypeBkWithin( nMonths ) != 0 ) )
				return 1;
			return 0;
		}
        
		[CreditDebug(Category_NewBkMethods)]
		public int HasDisch7BkWithin( int nMonths ) 
		{ 
			if(  m_aProdCrManualBk7Has )
			{
				switch( m_aProdCrManualBk7RecentStatusT )
				{
					case E_sProdCrManualDerogRecentStatusT.Discharged:
						if( IsEffectiveWithin( nMonths, m_aProdCrManualBk7RecentStatusT, m_aProdCrManualBk7RecentFileMon,
							m_aProdCrManualBk7RecentFileYr, m_aProdCrManualBk7RecentSatisfiedMon, m_aProdCrManualBk7RecentSatisfiedYr ) )
							return 1;
						break;
					case E_sProdCrManualDerogRecentStatusT.Dismissed:
						break;
					case E_sProdCrManualDerogRecentStatusT.NotSatisfied:
						break;
					default:
						throw new CBaseException( ErrorMessages.Generic, "Unhandled enum value of E_sProdCrManualDerogRecentStatusT in CCreditReportWithOptimisticDefault.HasDisch7BkWithin()" );
				}
			}
			return 0;
		}

		[CreditDebug(Category_NewBkMethods)]
		public int HasDisch11BkWithin( int nMonths ) { return 0; }
		[CreditDebug(Category_NewBkMethods)]
		public int HasDisch12BkWithin( int nMonths ) { return 0; }
		[CreditDebug(Category_NewBkMethods)]		
		public int HasDisch13BkWithin( int nMonths ) 
		{ 
			if(  m_aProdCrManualBk13Has )
			{
				switch( m_aProdCrManualBk13RecentStatusT )
				{
					case E_sProdCrManualDerogRecentStatusT.Discharged:
						if( IsEffectiveWithin( nMonths, m_aProdCrManualBk13RecentStatusT, m_aProdCrManualBk13RecentFileMon,
							m_aProdCrManualBk13RecentFileYr, m_aProdCrManualBk13RecentSatisfiedMon, m_aProdCrManualBk13RecentSatisfiedYr ) )
							return 1;
						break;
					case E_sProdCrManualDerogRecentStatusT.Dismissed:
						break;
					case E_sProdCrManualDerogRecentStatusT.NotSatisfied:
						break;
					default:
						throw new CBaseException( ErrorMessages.Generic, "Unhandled enum value of E_sProdCrManualDerogRecentStatusT in CCreditReportWithOptimisticDefault.HasDisch13BkWithin()" );
				}
			}
			return 0;
		}

		// This will allow us to generate stip for further review
		[CreditDebug(Category_NewBkMethods)]
		public int HasDischUnknownTypeBkWithin( int nMonths ) { return 0; }
		#endregion HasDischBkWithin, counts only the discharged BK
		// End OPM 5752 - New Sets of BK Keywords


        [CreditDebug(Category_NewBkMethods)]
		public int HasBkFiledWithin( int nMonths ) 
        { 
            if( Has7BkFiledWithin( nMonths ) != 0 || Has11BkFiledWithin( nMonths ) != 0 || Has12BkFiledWithin( nMonths ) != 0 
                || Has13BkFiledWithin( nMonths ) != 0 || HasUnknownTypeBkFiledWithin( nMonths ) != 0 )
                return 1;
            return 0;
        }
        [CreditDebug(Category_NewBkMethods)]
		public int Has7BkFiledWithin( int nMonths ) 
        { 
            if( m_aProdCrManualBk7Has )
            {
                if( IsDateWithin( m_aProdCrManualBk7RecentFileMon, m_aProdCrManualBk7RecentFileYr, nMonths ) )
                    return 1;
            }
            return 0;
        }
        [CreditDebug(Category_NewBkMethods)]
		public int Has11BkFiledWithin( int nMonths ) { return 0; }
        [CreditDebug(Category_NewBkMethods)]
		public int Has12BkFiledWithin( int nMonths ) { return 0; }
        [CreditDebug(Category_NewBkMethods)]
		public int Has13BkFiledWithin( int nMonths ) 
        { 
            if( m_aProdCrManualBk13Has )
            {
                if( IsDateWithin( m_aProdCrManualBk13RecentFileMon, m_aProdCrManualBk13RecentFileYr, nMonths ) )
                    return 1;
            }
            return 0; 
        }
        [CreditDebug(Category_NewBkMethods)]
		public int HasUnknownTypeBkFiledWithin( int nMonths ) { return 0; }

        [CreditDebug(Category_NewBkMethods)]
		public bool HasUnpaidAndNondischargedAnyBk 
        { 
            get 
            { 
                return HasUnpaidAndNondischarged7Bk || HasUnpaidAndNondischarged11Bk || HasUnpaidAndNondischarged12Bk ||
                    HasUnpaidAndNondischarged13Bk || HasUnpaidAndNondischargedUnknownTypeBk;
            } 
        }
        [CreditDebug(Category_NewBkMethods)]
		public bool HasUnpaidAndNondischarged7Bk 
        { 
            get 
            { 
                return ( m_aProdCrManualBk7Has && m_aProdCrManualBk7RecentStatusT == E_sProdCrManualDerogRecentStatusT.NotSatisfied );                
            } 
        }
        [CreditDebug(Category_NewBkMethods)]
		public bool HasUnpaidAndNondischarged11Bk { get { return false; } }
        [CreditDebug(Category_NewBkMethods)]
		public bool HasUnpaidAndNondischarged12Bk { get { return false; } }
        [CreditDebug(Category_NewBkMethods)]
		public bool HasUnpaidAndNondischarged13Bk 
        { 
            get 
            { 
                return ( m_aProdCrManualBk13Has && m_aProdCrManualBk13RecentStatusT == E_sProdCrManualDerogRecentStatusT.NotSatisfied );                
            } 
        }
        [CreditDebug(Category_NewBkMethods)]
		public bool HasUnpaidAndNondischargedUnknownTypeBk { get { return false; } }

		public CDateTime FileDOfLastBkOfType( E_BkType type )
		{
			switch( type )
			{
				case E_BkType.Ch7:
					if( !m_aProdCrManualBk7Has )
						return CDateTime.InvalidWrapValue;
					return CDateTime.Create(new DateTime(m_aProdCrManualBk7RecentFileYr, m_aProdCrManualBk7RecentFileMon, 1));

				case E_BkType.Ch11:
					return CDateTime.InvalidWrapValue;

				case E_BkType.Ch12:
					return CDateTime.InvalidWrapValue;

				case E_BkType.Ch13:
					if( !m_aProdCrManualBk13Has )
						return CDateTime.InvalidWrapValue;
					return CDateTime.Create(new DateTime(m_aProdCrManualBk13RecentFileYr, m_aProdCrManualBk13RecentFileMon, 1));

				case E_BkType.UnknownType:
					return CDateTime.InvalidWrapValue;

				default:
					throw new CBaseException( ErrorMessages.Generic, "Unhandled enum value for E_BkType in FileDOfLastBkOfType" );
			}
		}

		public CDateTime DischargedDOfLastBkOfType( E_BkType type )
		{
			switch( type )
			{
				case E_BkType.Ch7:
					if( !m_aProdCrManualBk7Has || m_aProdCrManualBk7RecentStatusT == E_sProdCrManualDerogRecentStatusT.NotSatisfied )
						return CDateTime.InvalidWrapValue;
					return CDateTime.Create(new DateTime(this.m_aProdCrManualBk7RecentSatisfiedYr, m_aProdCrManualBk7RecentSatisfiedMon, 1));

				case E_BkType.Ch11:
					return CDateTime.InvalidWrapValue;

				case E_BkType.Ch12:
					return CDateTime.InvalidWrapValue;

				case E_BkType.Ch13:
					if( !m_aProdCrManualBk13Has || m_aProdCrManualBk13RecentStatusT == E_sProdCrManualDerogRecentStatusT.NotSatisfied )
						return CDateTime.InvalidWrapValue;
					return CDateTime.Create(new DateTime(m_aProdCrManualBk13RecentSatisfiedYr, m_aProdCrManualBk13RecentSatisfiedMon, 1));

				case E_BkType.UnknownType:
					return CDateTime.InvalidWrapValue;

				default:
					throw new CBaseException( ErrorMessages.Generic, "Unhandled enum value for E_BkType in DischargedDOfLastBkOfType" );
			}
		}

		#endregion // NEW BK METHODS

        #region SHARING METHODS OF NEW BANKS
		
        [CreditDebug(Category_Generic_Collection)]
        public int GetTradesCountOlderThan( int nMonths )  { return 100; }

        [CreditDebug(Category_Generic_Collection)]
        public int InstallTradeCountOlderThan( int nMonths )  { return 100; }

        [CreditDebug(Category_Generic_Collection)]
        public int RevolveTradeCountOlderThan( int nMonths )  { return 100; }

        [CreditDebug(Category_Generic_Collection)]
		public bool Is1stTimeHomeBuyer 
		{
			get { return false; }
		}
        [CreditDebug(Category_Generic_Collection)]
        public bool Is1stTimeHomeBuyerWithin( int nMonths )
        {
            return false;
        }

		/// <summary>
		/// Used by banks: GMAC
		/// </summary>
        [CreditDebug(Category_Generic_TaxLien)]		
		public bool HasTaxOpenTaxLien 
		{ 
			get { return false; }
		}
		[CreditDebug(Category_Generic_Collection)]		
		public int GetHighestUnpaidBalOfCollectionWithin( int nMonths ){ return 0; }
		[CreditDebug(Category_Generic_Judgment)]
		public int GetHighestUnpaidBalOfJudgmentWithin( int nMonhts ){ return 0; }
		[CreditDebug(Category_Generic_Chargeoff)]
		public int GetHighestUnpaidBalOfChargeOffWithin( int nMonths ) { return 0; }
		[CreditDebug(Category_Generic_TaxLien)]
		public int GetHighestUnpaidBalOfTaxLienWithin( int nMonths ) { return 0; }

		// 5/14/2007 nw - OPM 13060 - New charge off / collection keywords
		[CreditDebug( Category_Generic_Derog )]
		public int HighestUnpaidBalOfChargeOffOrCollectionWithin( int nMonths ) { return 0; }

		[CreditDebug(Category_Generic_Collection)]
		public int GetTotUnpaidBalOfCollectionWithin( int nMonths ) { return 0; }
		[CreditDebug(Category_Generic_Judgment)]
		public int GetTotUnpaidBalOfJudgmentWithin( int nMonths ) { return 0; }
		[CreditDebug(Category_Generic_Chargeoff)]
		public int GetTotUnpaidBalOfChargeOffWithin( int nMonths ) { return 0; }
		[CreditDebug(Category_Generic_TaxLien)]
		public int GetTotUnpaidBalOfTaxLienWithin( int nMonths ) { return 0; }
        [CreditDebug( Category_Generic_Derog )]
        public int TotUnpaidBalOfAllDerogWithin( int nMonths ) { return 0; }

		// 2/7/2007 nw - OPM 10164
		[CreditDebug( Category_Generic_Derog )]
		public int TotUnpaidBalOfChargeOffAndCollectionWithin( int nMonths ) { return 0; }
		
		[CreditDebug(Category_Generic_TradeCount)]
		public int GetTradesCountActiveWithin( int nMonths ) { return 100; }
		[CreditDebug(Category_Generic_TradeCount)]
		public int GetTradesCountWithHighCreditOf( int amount ) { return 100; }
		[CreditDebug(Category_Generic_TradeCount)]
		public int GetTradesCountActiveWithHighCreditOf( int amount ) { return 100; }

        [CreditDebug(Category_Generic_Foreclosure)]
		public int ForeclosureUnpaidCount
        { 
            get 
            { 
                if( m_aProdCrManualForeclosureHas && m_aProdCrManualForeclosureRecentStatusT == E_sProdCrManualDerogRecentStatusT.NotSatisfied )
                    return 1; // 1 instead of 100 because we still try to be optimistic here
                return 0; 
            } 
        }

		//6/28/07 db OPM 16602 - New MaxHighCreditAmountX keyword
		[CreditDebug(Category_Generic_TradeCount)]
		public decimal MaxHighCreditAmountX(string parameters)
		{ 
			return 0;
		}

        [CreditDebug(Category_Generic_Collection)]
		public int GetHighestUnpaidBalOfCollectionOlderThan( int nMonths ) { return 0; }

		//10/25/07 db - OPM 18561
		[CreditDebug(Category_Generic_Collection)]
		public int GetHighestUnpaidBalOfMedicalCollectionWithin( int nMonths ) { return 0; }

		[CreditDebug(Category_Generic_Judgment) ]
		public int GetHighestUnpaidBalOfJudgmentOlderThan( int nMonths ) { return 0; }
        [CreditDebug(Category_Generic_Chargeoff) ]
		public int GetHighestUnpaidBalOfChargeOffOlderThan( int nMonths ) { return 0; }

		// 5/14/2007 nw - OPM 13060 - New charge off / collection keywords
		[CreditDebug( Category_Generic_Derog )]
		public int HighestUnpaidBalOfChargeOffOrCollectionOlderThan( int nMonths ) { return 0; }

		[CreditDebug(Category_Generic_Late)]
		public int GetHighestUnpaidPastDueWithin( int nMonths ) { return 0; }
		[CreditDebug(Category_Generic_Late)]
		public int GetHighestUnpaidPastDueOlderThan( int nMonths ) { return 0; }

		#endregion //

		/// <summary>
		/// Retrieve HTML or PDF_BASE64 format.
		/// </summary>
		/// <param name="format"></param>
		/// <returns></returns>
		public string RetrieveRawFormat(string format) 
		{
            string errMsg = "CCreditReportWithOptimisticDefault class doesn't not have real credit report for RetrieveRawFormat()";
			Tools.LogBug(errMsg);
			throw new CBaseException(ErrorMessages.Generic, errMsg); 
		}

        public int RepresentativeScore
        {
            get 
            {
                return m_aBorrowerCreditModeT != E_aBorrowerCreditModeT.Coborrower ?
                    GetRepresentativeScore(m_aBEquifaxScorePe, m_aBExperianScorePe, m_aBTransUnionScorePe) :
                    GetRepresentativeScore(m_aCEquifaxScorePe, m_aCExperianScorePe, m_aCTransUnionScorePe);
            }
        }

        private int GetRepresentativeScore(params int[] scores)
        {
            Array.Sort(scores);

            if (scores.Length != 3)
            {
                throw new CBaseException(ErrorMessages.Generic, "Programmer Error Representative score array does not contain 3 entries.");
            }

            //if there are no scores that are 0 then the middle element is the representative score  
            //if there is one that is zero it will be element 0 of the array  so the lower score will be the middle still
            if (scores[1] != 0)
            {
                return scores[1];
            }
            //if the middle score is also 0 then that means that there were 2 zero scores so the third one will be the lowest non zero.
            return scores[2];
        }

		public CreditScore BorrowerScore 
		{ 
			get
			{ 
				throw new CBaseException( ErrorMessages.Generic, "CCreditReportWithOptimisticDefault doesn't have score" ); 
			}
		}
		public CreditScore CoborrowerScore
		{ 
			get
			{
                throw new CBaseException(ErrorMessages.Generic, "CCreditReportWithOptimisticDefault doesn't have score"); 
			}
		}


		public BorrowerInfo BorrowerInfo
		{ 
			get
			{
                string errMsg = "CCreditReportWithOptimisticDefault doesn't have borrower info";
				Tools.LogWarning(errMsg);
                throw new CBaseException(ErrorMessages.Generic, errMsg); 
			}
		}

		public BorrowerInfo CoborrowerInfo 
		{ 
			get
			{
                string errMsg = "CCreditReportWithOptimisticDefault doesn't have borrower info";
				Tools.LogWarning(errMsg);
                throw new CBaseException(ErrorMessages.Generic, errMsg); 
			}
		}
		public CreditBureauInformation ExperianContactInformation 
		{ 
			get 
			{
                string errMsg = "CCreditReportWithOptimisticDefault doesn't have ExperianContactInformation";
                Tools.LogWarning(errMsg);
                throw new CBaseException(ErrorMessages.Generic, errMsg);
			}
		}
		public CreditBureauInformation TransUnionContactInformation 
		{ 
			get 
			{
                string errMsg = "CCreditReportWithOptimisticDefault doesn't have TransUnionContactInformation";
                Tools.LogWarning(errMsg);
                throw new CBaseException(ErrorMessages.Generic, errMsg);
			}
		}
		public CreditBureauInformation EquifaxContactInformation 
		{ 
			get 
			{
                string errMsg = "CCreditReportWithOptimisticDefault doesn't have EquifaxContactInformation";
                Tools.LogWarning(errMsg);
                throw new CBaseException(ErrorMessages.Generic, errMsg);
			}
		}
        public int CreditReportAge 
        {
            get { return 1; }
        }
		public DateTime CreditReportFirstIssuedDate
		{
			get { return DateTime.MinValue; }
		}

        public DateTime CreditReportLastUpdatedDate
        {
            get { return DateTime.MinValue; }
        }

        /// <summary>
        /// Return list of liabilies for this report.
        /// </summary>
        public ArrayList AllLiabilities 
		{ 
			get
            { 
                Tools.LogWarning( "Optimistic/manual credit object is returning list of empty liabilities, very likely that it won't yield optimistic result." );
                return new ArrayList(); 
            }
		}

        public ArrayList LiabilitiesExcludeFromUnderwriting 
        {
            get 
            {
                Tools.LogWarning( "Optimistic/manual credit object is returning list of empty exclude liabilities, very likely that it won't yield optimistic result." );
                return new ArrayList(); 

            }
        }

        public ArrayList AllPublicRecords 
        {
            get { return new ArrayList(); }
        }

       [CreditDebug(Category_Generic_TradeCount)]
        public int TradeCount { get { return 100; } }

		[CreditDebug(Category_Generic_TradeCount)]
		public int TradeCountX ( string parameters )
		{ 
			return 100;
		}

		[CreditDebug(Category_Generic_TradeCount)]
		public int MajorDerogCountX ( string parameters )
		{ 
			return 0;
		}

        [CreditDebug(Category_Generic_Chargeoff)]
        public int ChargeOffCountX(string parameters)
        {
            return 0;
        }

		[CreditDebug(Category_Generic_TradeCount)]
		public int HasContiguousMortgageHistory ( string parameters )
		{
			return 1;
		}

		// 11/2/2004 dd - Added
		public void SetLoanData(CAppBase dataApp) { }

		public void Parse() 
        {
            m_dataApp.LoadOptimisticCreditReportValue(this);
        }

        private bool m_isDebug = false;
        public bool IsDebug 
        {
            get { return m_isDebug; }
            set { m_isDebug = value; }
        }

        public ArrayList GetTagItems(string methodName) 
        {
            return new ArrayList();
        }

        public string CreditRatingCodeType
		{
			get
			{
				return "None";
			}
		}

	}
}
