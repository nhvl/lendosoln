﻿namespace LendersOffice.CreditReport
{
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;
    using LendersOffice.Common;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// This partial extends the <see cref="CreditVendorAdditionalSavedDocument"/> class by providing database utility methods.
    /// </summary>
    public partial class CreditVendorAdditionalSavedDocument
    {
        /// <summary>
        /// Loads the existing documents for the specified report.
        /// </summary>
        /// <param name="brokerId">The identifier of the lender.</param>
        /// <param name="appId">The identifier of the loan application.</param>
        /// <param name="companyId">The identifier of the credit vendor.</param>
        /// <param name="reportId">The identifier of the report as specified by the credit vendor.</param>
        /// <returns>A list of documents.</returns>
        public static IReadOnlyList<CreditVendorAdditionalSavedDocument> GetAllForReportId(Guid brokerId, Guid appId, Guid companyId, string reportId)
        {
            List<CreditVendorAdditionalSavedDocument> documents = new List<CreditVendorAdditionalSavedDocument>();
            var parameters = new[]
            {
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@AppId", appId),
                new SqlParameter("@VendorId", companyId),
                new SqlParameter("@ReportId", reportId),
            };
            using (var connection = DataAccess.DbAccessUtils.GetConnection(brokerId))
            using (var reader = Drivers.SqlServerDB.StoredProcedureDriverHelper.ExecuteReader(
                connection.OpenDbConnection(),
                null,
                StoredProcedureName.Create("SERVICE_FILE_RECEIVED_DOCUMENTS_GetAllForReportId").ForceValue(),
                parameters,
                TimeoutInSeconds.Default))
            {
                while (reader.Read())
                {
                    documents.Add(new CreditVendorAdditionalSavedDocument(
                        (int)reader["Id"],
                        (Guid)reader["BrokerId"],
                        (Guid)reader["LoanId"],
                        (Guid)reader["AppId"],
                        (Guid)reader["VendorId"],
                        (string)reader["ReportId"],
                        (string)reader["FileName"],
                        (Guid)reader["DocumentId"],
                        SHA256Checksum.Create((string)reader["DocumentChecksum"]).ForceValue()));
                }
            }

            return documents;
        }

        /// <summary>
        /// Updates the database entry to reference a different <see cref="EDocs.EDocument.DocumentId"/>.
        /// </summary>
        /// <param name="existingDocument">The existing document.</param>
        /// <param name="documentId">The identifier of the new EDocument to associate.</param>
        /// <param name="documentChecksum">The checksum of the document content.</param>
        public static void UpdateDocument(CreditVendorAdditionalSavedDocument existingDocument, Guid documentId, SHA256Checksum documentChecksum)
        {
            var parameters = new[]
            {
                new SqlParameter("@Id", existingDocument.Id),
                new SqlParameter("@BrokerId", existingDocument.BrokerId),
                new SqlParameter("@DocumentId", documentId),
                new SqlParameter("@DocumentChecksum", documentChecksum.Value),
            };
            using (var connection = DataAccess.DbAccessUtils.GetConnection(existingDocument.BrokerId))
            {
                Drivers.SqlServerDB.StoredProcedureDriverHelper.ExecuteNonQuery(
                    connection.OpenDbConnection(),
                    null,
                    StoredProcedureName.Create("SERVICE_FILE_RECEIVED_DOCUMENTS_UpdateDocument").ForceValue(),
                    parameters,
                    TimeoutInSeconds.Default);
            }
        }

        /// <summary>
        /// Saves a new document to the database.
        /// </summary>
        /// <param name="brokerId">The identifier of the lender.</param>
        /// <param name="loanId">The identifier of the loan.</param>
        /// <param name="appId">The identifier of the loan application.</param>
        /// <param name="companyId">The identifier of the vendor.</param>
        /// <param name="reportId">The identifier of the order in the credit vendor's system.</param>
        /// <param name="fileName">The unique file name specified by the vendor for this document.</param>
        /// <param name="documentId">The identifier of the EDocument created from the document of the vendor.</param>
        /// <param name="documentChecksum">The checksum of the document content.</param>
        internal static void SaveNewDocument(Guid brokerId, Guid loanId, Guid appId, Guid companyId, string reportId, string fileName, Guid documentId, SHA256Checksum documentChecksum)
        {
            var parameters = new[]
            {
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@LoanId", loanId),
                new SqlParameter("@AppId", appId),
                new SqlParameter("@VendorId", companyId),
                new SqlParameter("@ReportId", reportId),
                new SqlParameter("@FileName", fileName),
                new SqlParameter("@DocumentId", documentId),
                new SqlParameter("@DocumentChecksum", documentChecksum.Value),
            };
            using (var connection = DataAccess.DbAccessUtils.GetConnection(brokerId))
            {
                Drivers.SqlServerDB.StoredProcedureDriverHelper.ExecuteNonQuery(
                    connection.OpenDbConnection(),
                    null,
                    StoredProcedureName.Create("SERVICE_FILE_RECEIVED_DOCUMENTS_SaveNewDocument").ForceValue(),
                    parameters,
                    TimeoutInSeconds.Default);
            }
        }
    }
}
