using System;
using System.Net;
using System.Xml;
using System.Xml.Linq;
using System.Linq;
using System.Text;
using System.Threading;
using LendersOffice.CreditReport;
using LendersOffice.Common;
using LendersOffice.CreditReport.Mismo;
using LendersOffice.com.fundingsuite.v2;
using LendersOffice.com.fundingsuite.v2.test;
using DataAccess;

namespace LendersOffice.CreditReport.FundingSuite
{    

    /// <summary>
    /// Documentation : https://v2.fundingsuite.com/netws/testcr/wsDocument.htm
    /// </summary>
    public class FundingSuite_CreditReportRequest : LendersOffice.CreditReport.Mismo2_3.Mismo2_3CreditReportRequest, IFetchResponse
    {
        //The urls are for reference purpose only. The actuall call is made through asmx service proxy
        private const string FUNDINGSUITE_TEST_SERVER = "https://stagingv2.fundingsuite.com/netws/credit/LendersOffice.asmx";
        private const string FUNDINGSUITE_BETA_SERVER = "https://v2.fundingsuite.com/netws/testcr/credit.asmx";
        private const string FUNDINGSUITE_PRODUCTION_SERVER = "https://v2.fundingsuite.com/netws/credit/LendersOffice.asmx";

        private const int MAX_TRIES = 5;
        private string _status = string.Empty;
        private string _errorMessage = string.Empty;
        
        private bool IsTest
        {
            get { return LoginInfo.UserName == "wsStage_LendingQB"; }
        }

        private bool IsBeta
        {
            get { return LoginInfo.UserName == "wsTester_MeridianLink"; }
        }

        public FundingSuite_CreditReportRequest()
            : base("")
        {

        }

        protected override bool IncludeMailingAddress
        {
            get { return false; }
        }

        public override string Url
        {
            get
            {
                if (IsTest)
                {
                    return FUNDINGSUITE_TEST_SERVER;
                }
                else if (IsBeta)
                {
                    return FUNDINGSUITE_BETA_SERVER;
                }
                else
                {
                    return FUNDINGSUITE_PRODUCTION_SERVER;
                }
            }
        }
        public override ICreditReportResponse CreateResponse(XmlDocument doc)
        {
            return new FundingSuite_CreditReportResponse(doc);
        }

        public override void SetupAuthentication(HttpWebRequest webRequest)
        {
            webRequest.ContentType = "text/xml";
        }

        //IFetchResponse
        public string FetchResponse()
        {
            //https://v2.fundingsuite.com/netws/testcr/wsDocument.htm
            //Step 1: Submit request

            //for Reissue, creditActionType has to be "Retrieve" . "Submit" wont work 
            E_CreditReportRequestActionType creditActionType = (ReportID == "" ? E_CreditReportRequestActionType.Submit : E_CreditReportRequestActionType.Retrieve);

            string sResponse = InvokeSubmitMISMOCreditRequest(creditActionType);
            
            if(_status.ToLower() == "merge" || _status == "error") //return response on success or failure
                return sResponse;            
                        
            //Step 2 : Ping the server for process status
            for (int _nTries = 0; _nTries < MAX_TRIES && (_status == "status" || _status == string.Empty); _nTries++)
            {
                LendersOffice.Sleeping.PerformanceLoggingSleeper.Instance.Sleep(20000);

                sResponse = InvokeSubmitMISMOCreditRequest(E_CreditReportRequestActionType.Retrieve);
                if (_status.ToLower() == "merge")
                    return sResponse;
                if (_status == "error")
                   Tools.LogError(_errorMessage);
            }
            //could not retrieve the credit report. This should be detected in response
            return sResponse;
        }


        private string InvokeSubmitMISMOCreditRequest(E_CreditReportRequestActionType requestType)        
        {
            RequestType = requestType;
            string sMismoRequest = GenerateXml().InnerXml;
            string sResponseXml = string.Empty;

            if (IsBeta)
            {
                Credit credit = new Credit();
                sResponseXml = credit.SubmitMISMOCreditRequest(sMismoRequest);
            }
            else
            {
                LendersOffice.com.fundingsuite.v2.LendersOffice credit = new LendersOffice.com.fundingsuite.v2.LendersOffice();
                credit.Url = this.Url;
                sResponseXml = credit.SubmitMISMOCreditRequest(sMismoRequest);
            }           
            
            //Remove invalid XML characters and default namespaces
            sResponseXml = XmlUtils.RemoveInvalidXmlChars(sResponseXml);
            sResponseXml = XmlUtils.StripNS(sResponseXml);

            XmlDocument xmlDoc = new XmlDocument();          

            xmlDoc.LoadXml(sResponseXml);

            if (requestType == E_CreditReportRequestActionType.Submit)
            { 
                //Retrieve the Credit report ID

                XmlElement creditResponseElement = (XmlElement)xmlDoc.SelectSingleNode("//RESPONSE_GROUP/RESPONSE/RESPONSE_DATA/CREDIT_RESPONSE");
                if (null != creditResponseElement)
                {
                    ReportID = creditResponseElement.GetAttribute("CreditReportIdentifier");
                }
            }

            //Parse response. /CREDIT_RESPONSE/@CreditReportType  can be  �status� (pending) or �Merge� (ready)
            //if it is pending. wait
            XmlElement statusElement = (XmlElement)xmlDoc.SelectSingleNode("//RESPONSE_GROUP/RESPONSE/RESPONSE_DATA/CREDIT_RESPONSE");
            _status = statusElement.Attributes["CreditReportType"].Value.ToLower();
            if(_status == "error")
                _errorMessage = statusElement.SelectSingleNode("//CREDIT_RESPONSE/CREDIT_ERROR_MESSAGE/_Text").InnerText;
            
            return sResponseXml;
        }       
    }
}
