using System;
using System.Xml;

namespace LendersOffice.CreditReport.Credco
{

	public class Credco_CreditReport : LendersOffice.CreditReport.Mismo2_3.Mismo2_3CreditReport
	{
        private const string CREDCO_ERROR_FORMAT = @"
<RESPONSE_GROUP VersionID=""2.3"">
        <RESPONSE>
        <RESPONSE_DATA>
        <CREDIT_RESPONSE MISMOVersionID=""2.3"">
             <EMBEDDED_FILE _Type=""HTML"" _Version=""3.2"" _Name="""" _Extension=""htm"">
        <DOCUMENT>
        <![CDATA[
        <html>
        <body>
        <table width='100%' height='100%'>
        <tr><td align='center' valign='center' style='font-weight:bold;font-size:18px'>The credit report has been deleted from the file on {0} in accordance with requirements from First American Credco.
You can re-issue the credit report with the following report id:&nbsp; <u>{1}</u>
        </td></tr>
        </table>
    		
        </body>
        </html>
        ]]>
        </DOCUMENT>
        </EMBEDDED_FILE>
        </CREDIT_RESPONSE>
        </RESPONSE_DATA>
        </RESPONSE>
        </RESPONSE_GROUP>
";

        public static string CreateDeleteNotice(DateTime dt, string reportId) 
        {
            return string.Format(CREDCO_ERROR_FORMAT, dt, reportId);
        }
		private Credco_CreditReport(CreditReportFactory factory, CreditReportProtocol creditReportProtocol, XmlDocument doc, CreditReportDebugInfo debugInfo) : base(factory, creditReportProtocol, doc, debugInfo)
		{
		}

	}
}
