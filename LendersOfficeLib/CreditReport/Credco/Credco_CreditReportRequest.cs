namespace LendersOffice.CreditReport.Credco
{
    using System.Net;
    using System.Security.Cryptography.X509Certificates;
    using System.Xml;
    using LendersOffice.Constants;
    using LendersOffice.CreditReport.Mismo;
    using ObjLib.Security.ThirdParty;

    public class Credco_CreditReportRequest : LendersOffice.CreditReport.Mismo2_3.Mismo2_3CreditReportRequest
	{
        private const string SUBMITTING_PARTY_IDENTIFIER = "LQB";

        protected override string MISMOVersionID
        {
            get { return "2.3.1"; }
        }

        protected override bool IncludeMailingAddress
        {
            get { return false; }
        }

        public Credco_CreditReportRequest(string url) : base(url)
		{
            this.SubmittingPartyIdentifier = SUBMITTING_PARTY_IDENTIFIER;

			AppendOutputFormat(E_MismoOutputFormat.Other);
            this.PreferredResponseFormatOtherDescription = "HTML";

			// 12/26/2006 nw - OPM 8979 - Order PDF file format for Credco credit report.  
			// PDF will only be returned if the Credco user account is setup to return PDF in Credco's system.
			AppendOutputFormat(E_MismoOutputFormat.PDF);
		}
        public override ICreditReportResponse CreateResponse(XmlDocument doc) 
        {
            return new Credco_CreditReportResponse(doc);
        }
        public override void SetupAuthentication(HttpWebRequest webRequest) 
        {
            if (ConstStage.EnableThirdPartyCertificateManager)
            {
                ThirdPartyClientCertificateManager.AppendCertificate(webRequest);
            }
            else
            {
                X509Certificate mycert = X509Certificate.CreateFromCertFile(ConstApp.CredcoCertificateLocation);
                webRequest.ClientCertificates.Add(mycert);
            }
        }
	}
}
