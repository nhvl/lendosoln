using System;
using System.Xml;

namespace LendersOffice.CreditReport.Credco
{
	public class Credco_CreditReportResponse : LendersOffice.CreditReport.Mismo2_3.Mismo2_3CreditReportResponse
	{
		public Credco_CreditReportResponse(XmlDocument doc) : base(doc)
		{
		}
        protected override string GetCustomErrorMessage(string code) 
        {
            if (code == "E034") 
            {
                // Invalid Report Type, due to the account does not have XML, and HTML settings in their report.
                // Display message tell client to contact CREDCO and configure their account properly.
                return "Please contact CoreLogic CREDCO 1-800-423-1150 and have your account configures to receive MISMO 2.3 with XML and HTML report.";
            } 
            else 
            {
                return "If you are unable to login for credit after several tries, please contact CoreLogic CREDCO 1-800-423-1150 for assistance. <br><br>If you are unable to use the existing Credco account to order credit, then you need to contact Credco's sale rep and establish a new account. This new account will need to be compatible with Mismo 2.3 and HTML Wrapped image.";
            }
        }
        protected override string DetermineErrorStatus(string code) 
        {
            // Credco error code.
            // E036 - User id is invalid
            // E037 - Password is invalid

            string status = "ERROR";
            switch (code) 
            {
                case "E036":
                case "E037":
                    status = "LOGIN_FAILED";
                    break;
            }
            return status;
        }
	}
}
