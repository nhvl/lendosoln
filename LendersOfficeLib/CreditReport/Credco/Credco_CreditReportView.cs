using System;
using System.Xml;

using LendersOffice.CreditReport;

namespace LendersOffice.CreditReport.Credco
{
	public class Credco_CreditReportView : LendersOffice.CreditReport.Mismo.MismoCreditReportView
	{
		internal Credco_CreditReportView(CreditReportFactory factory, XmlDocument doc, CreditReportDebugInfo debugInfo) : base(factory, doc, debugInfo)
		{
		}

        protected override void Parse() 
        {
            XmlNode node = m_doc.SelectSingleNode("//RESPONSE_GROUP/RESPONSE/RESPONSE_DATA/CREDIT_RESPONSE/EMBEDDED_FILE[@_Type='HTML']/DOCUMENT");
            if (null != node) 
            {
                // 11/1/2004 dd - Credco encode their HTML format as Base64 (don't know why.)  Need to decode
                m_hasHtml = true;
				m_htmlContent = System.Text.ASCIIEncoding.ASCII.GetString(Convert.FromBase64String(node.InnerText));
            }
			else
			{
				// 6/21/2006 - nw - OPM 5718, handle base64 encoding for Text credit report.  
				// Some systems use "Text" instead of "TEXT", I believe Credco is one of them
				node = m_doc.SelectSingleNode("//RESPONSE_GROUP/RESPONSE/RESPONSE_DATA/CREDIT_RESPONSE/EMBEDDED_FILE[@_Type='Text']/DOCUMENT");
				if (null == node)
					node = m_doc.SelectSingleNode("//RESPONSE_GROUP/RESPONSE/RESPONSE_DATA/CREDIT_RESPONSE/EMBEDDED_FILE[@_Type='TEXT']/DOCUMENT");

				if (null != node)
				{
					m_hasHtml = true;
					if (((XmlElement) node.ParentNode).GetAttribute("_EncodingType").ToLower() == "base64")
						m_htmlContent = System.Text.ASCIIEncoding.ASCII.GetString(Convert.FromBase64String(node.InnerText));
					else
						m_htmlContent = node.InnerText;

					m_htmlContent = string.Format("<html><body><pre>{0}</pre></body></html>", m_htmlContent);
				}
			}

			// 12/26/2006 nw - OPM 8979 - Handle PDF file in Credco credit report
			node = m_doc.SelectSingleNode("//RESPONSE_GROUP/RESPONSE/RESPONSE_DATA/CREDIT_RESPONSE/EMBEDDED_FILE[@_Type='PDF']/DOCUMENT");
			if (null != node) 
			{
				m_hasPdf = true;
				m_pdfContent = node.InnerText;
			}

			if (!m_hasHtml && !m_hasPdf)
			{
				// 1/4/2005 dd - This CREDCO account doesn't config properly to return HTML report. Display error message to user
				// and ask them to contact CREDCO Tech support.

				m_hasHtml = true;
				m_htmlContent = "<html><body><table width='100%' height='100%'><tr><td align=center valign=center style='font-size:18px;font-weight:bold;color:red'>CREDCO account setup incorrect. <br><br>Contact First American CREDCO technical support at 1-800-423-1150 and have your account configured to receive MISMO 2.3 with XML and HTML report.</td></tr></table>";
			}
        }
	}
}
