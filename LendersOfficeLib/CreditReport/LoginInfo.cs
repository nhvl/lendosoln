using System;

namespace LendersOffice.CreditReport
{
    public class LoginInfo 
    {
        private string m_userName = "";
        private string m_password = "";
        private string m_accountIdentifier = "";
        private string m_fannieMaeCreditProvider = "";
        private string m_fannieMaeMORNETUserID = "";
        private string m_fannieMaeMORNETPassword = "";

        public string AccountIdentifier 
        {
            get { return m_accountIdentifier; }
            set { m_accountIdentifier = value; }
        }

        public string UserName
        {
            get { return m_userName; }
            set { m_userName = value; }
        }

        public string Password
        {
            get { return m_password; }
            set { m_password = value; }
        }
        public string FannieMaeCreditProvider 
        {
            get { return m_fannieMaeCreditProvider; }
            set { m_fannieMaeCreditProvider = value; }
        }
        public string FannieMaeMORNETUserID 
        {
            get { return m_fannieMaeMORNETUserID; }
            set { m_fannieMaeMORNETUserID = value; }
        }
        public string FannieMaeMORNETPassword 
        {
            get { return m_fannieMaeMORNETPassword; }
            set { m_fannieMaeMORNETPassword = value; }
        }

        public void CopyFrom(LoginInfo src) 
        {
            this.m_userName = src.m_userName;
            this.m_password = src.m_password;
            this.m_accountIdentifier = src.m_accountIdentifier;
            this.m_fannieMaeCreditProvider = src.m_fannieMaeCreditProvider;
            this.m_fannieMaeMORNETUserID = src.m_fannieMaeMORNETUserID;
            this.m_fannieMaeMORNETPassword = src.m_fannieMaeMORNETPassword;
        }
    }
}
