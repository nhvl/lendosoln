using System;
using System.Xml;

namespace LendersOffice.CreditReport
{
	/// <summary>
	/// Summary description for ICreditReportRequest.
	/// </summary>
	public interface ICreditReportRequest
	{
        /// <summary>
        /// The url of the server to poll credit report from.
        /// </summary>
        string Url { get; }

        /// <summary>
        /// Return the credit report request in Xml format.
        /// This class expected the credit request can be express in Xml format.
        /// </summary>
        XmlDocument GenerateXml();

        byte[] GeneratePostContent();

        ICreditReportResponse CreateResponse(XmlDocument doc);

        void SetupAuthentication(System.Net.HttpWebRequest request);
	}
}
