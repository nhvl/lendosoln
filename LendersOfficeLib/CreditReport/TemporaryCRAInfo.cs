﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LendersOffice.CreditReport
{
    public sealed class TemporaryCRAInfo
    {
        public string VendorName { get; set; }
        public string ProtocolUrl { get; set; }
        public Guid ProtocolID { get; set; }
        public CreditReportProtocol ProtocolType { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string AccountID { get; set; }
        public bool IsExperianPulled { get; set; }
        public bool IsEquifaxPulled { get; set; }
        public bool IsTransUnionPulled { get; set; }
        public bool IsFannieMaePulled { get; set; }
        public string CreditMornetPlusUserId { get; set; }
        public string CreditMornetPlusPassword { get; set; }
        public string ProviderId { get; set; }

        public TemporaryCRAInfo(string vendorName, string url, Guid protocolID, CreditReportProtocol protocolType, string providerId /*MCL Code*/)
        {
            this.VendorName = vendorName;
            this.ProtocolUrl = url;
            this.ProtocolID = protocolID;
            this.ProtocolType = protocolType;
            this.ProviderId = providerId;
        }

        public IEnumerable<TemporaryCRAInfo> GetAvailableCRAs(Guid brokerId)
        {
            foreach (CRA o in MasterCRAList.RetrieveAvailableCras(brokerId))
            {
                yield return new TemporaryCRAInfo(o.VendorName, o.Url, o.ID, o.Protocol, o.ProviderId);
            }
        }
    }
}
