using System;
using System.Xml;

namespace LendersOffice.CreditReport.KrollFactualData
{
    // 4/19/2012 dd - OLD VERSION DO NOT USE
	public class KrollFactualData_CreditReportResponse : LendersOffice.CreditReport.Mismo2_3.Mismo2_3CreditReportResponse
	{
		public KrollFactualData_CreditReportResponse(XmlDocument doc) : base(doc)
		{
		}

        protected override string GetCustomErrorMessage(string code) 
        {
            return "If you are unable to login for credit after several tries, please contact Kroll Factual Data 1-800-673-5525 for assistance.";
        }
	}
}
