﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace LendersOffice.CreditReport.KrollFactualData
{
    public class KrollFactualData26_CreditReportResponse : LendersOffice.CreditReport.Mismo2_3.Mismo2_3CreditReportResponse
    {
        public KrollFactualData26_CreditReportResponse(XmlDocument doc, string brandedProductName)
            : base(doc)
		{
            this.BrandedProductName = brandedProductName;
		}
    }
}
