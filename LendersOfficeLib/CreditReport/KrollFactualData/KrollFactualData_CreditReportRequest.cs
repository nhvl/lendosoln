using System;
using System.Collections.Specialized;
using System.Xml;

using LendersOffice.CreditReport;

namespace LendersOffice.CreditReport.KrollFactualData
{
    // 4/19/2012 dd - OLD VERSION DO NOT USE
	public class KrollFactualData_CreditReportRequest : LendersOffice.CreditReport.Mismo2_3.Mismo2_3CreditReportRequest
	{
        private const string OFFICE_CODE = "0603";
        private const string CLIENT_CODE = "INLEND";
        private const string USER_NAME = "ILSACCT";
        private const string PASSWORD = "1L5ACCT";
        private const string SUBMITTING_PARTY_NAME = "Insight Lending Solutions";
        private const string RECEIVING_PARTY_NAME = "Factual Data";

        private const string KROLL_FACTUAL_DATA_URL = "https://widow1.factualdata.com/j/elend";
        private const string BETA_SERVER = "ELENDING_BETA";
        private const string PRODUCTION_SERVER = "ELENDING";

		public KrollFactualData_CreditReportRequest() : base(KROLL_FACTUAL_DATA_URL)
		{
            this.SubmittingPartyName = SUBMITTING_PARTY_NAME;
            this.ReceivingPartyName = RECEIVING_PARTY_NAME;

            AddRequestKeyValue("HTMLFile", "true");
//            NameValueCollection ret = new NameValueCollection();
//            ret["HTMLFile"] = "true";
//
//            this.RequestKey = ret;

		}

        public override byte[] GeneratePostContent() 
        {
            string server = PRODUCTION_SERVER;
            // 6/4/2004 dd - If AccountIdentifier match our unique internal OFFICE_CODE and CLIENT_CODE then
            // use beta server.
            if (LoginInfo.AccountIdentifier == OFFICE_CODE + CLIENT_CODE)
                server = BETA_SERVER;

            XmlDocument doc = GenerateXml();
            string str = string.Format("officecode={0}&clientcode={1}&username={2}&password={3}&{4}(MISMO2.1({5}))", 
                OFFICE_CODE, CLIENT_CODE, USER_NAME, PASSWORD, server, doc.InnerXml);

            return System.Text.Encoding.UTF8.GetBytes(str);
        }
        public override ICreditReportResponse CreateResponse(XmlDocument doc) 
        {
            return new KrollFactualData_CreditReportResponse(doc);
        }
	}
}
