﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Security.Cryptography.X509Certificates;
using System.Net;
using LendersOffice.Constants;
using LendersOffice.ObjLib.Security.ThirdParty;

namespace LendersOffice.CreditReport.KrollFactualData
{
    public class KrollFactualData26_CreditReportRequest : LendersOffice.CreditReport.Mismo2_3.Mismo2_3CreditReportRequest
    {
        private const string OFFICE_CODE = "0610";
        private const string CLIENT_CODE = "LENDQB";
        private const string USER_NAME = "LENDQBUSER";
        private const string PASSWORD = "L3ndqbu3r";
        private const string SUBMITTING_PARTY_NAME = "LendingQB";

        private const string BetaUrlSchemeAndServer = "https://edge-beta.krollfactualdata.com";
        private const string ProductionUrlSchemeAndServer = "https://edge.krollfactualdata.com";
        private const string PrimaryCreditReportUrlPathAndQuery = "/services/mismo23/kfdlos.aspx";
        private const string LqiCreditReportUrlPathAndQuery = "/services/LRR/kfdlos.aspx";
        public KrollFactualData26_CreditReportRequest() : base("")
        {
            this.SubmittingPartyName = SUBMITTING_PARTY_NAME;

            AddRequestKeyValue("HTMLFile", "true");
            AddRequestKeyValue("PDFFile", "true");
            AddRequestKeyValue("ConsumerCopyHTML", "true");
        }

        public override string Url
        {
            get { return this.UrlSchemeAndServer + this.UrlPathAndQuery; }
        }

        private string UrlSchemeAndServer { get; } = ProductionUrlSchemeAndServer;

        private string UrlPathAndQuery { get; set; } = PrimaryCreditReportUrlPathAndQuery;

        private string brandedProductName;

        public override bool CanConvertRequestTypeToLqi => true;
        public override void ConvertRequestTypeToLqi(CreditRequestData creditRequestData)
        {
            this.UrlPathAndQuery = LqiCreditReportUrlPathAndQuery;
            this.CreditReportType = Mismo.E_CreditReportType.LoanQuality;
            if (creditRequestData.IsRequestLqiReport)
            {
                this.RequestType = Mismo.E_CreditReportRequestActionType.Submit;
            }
            else if (creditRequestData.IsRequestReissueLqiReport)
            {
                this.RequestType = Mismo.E_CreditReportRequestActionType.Reissue;
            }

            this.ReportID = creditRequestData.ReportID;
            this.IncludeEquifax = true;
            this.IncludeExperian = true;
            this.IncludeTransunion = true;
            this.brandedProductName = "LRR";
        }

        protected override bool IncludeMailingAddress
        {
            get { return false; }
        }

        public override ICreditReportResponse CreateResponse(XmlDocument doc)
        {
            return new KrollFactualData26_CreditReportResponse(doc, this.brandedProductName);
        }

        public override void SetupAuthentication(HttpWebRequest webRequest)
        {
            webRequest.ContentType = "text/xml";
            webRequest.PreAuthenticate = true;

            webRequest.Timeout = 120000;
            if (ConstStage.EnableThirdPartyCertificateManager)
            {
                ThirdPartyClientCertificateManager.AppendCertificate(webRequest);
            }
            else
            {
                X509Certificate mycert = X509Certificate.CreateFromCertFile(ConstApp.KrollFactualDataCertificateLocation);
                webRequest.ClientCertificates.Clear();
                webRequest.ClientCertificates.Add(mycert);
            }
        }
    }
}
