using System;
using System.Xml;

using LendersOffice.CreditReport;

namespace LendersOffice.CreditReport.KrollFactualData
{
    // 4/19/2012 dd - OLD VERSION DO NOT USE
	public class KrollFactualData_CreditReportView : LendersOffice.CreditReport.Mismo.MismoCreditReportView
	{
		internal KrollFactualData_CreditReportView(CreditReportFactory factory, XmlDocument doc, CreditReportDebugInfo debugInfo) : base(factory, doc, debugInfo)
		{
		}

        protected override void Parse() 
        {
            XmlNode node = m_doc.SelectSingleNode("//RESPONSE_GROUP/RESPONSE/RESPONSE_DATA/CREDIT_RESPONSE/EMBEDDED_FILE[@_Type='Factual Data Standard HTML']/DOCUMENT");
            if (null != node) 
            {
                m_htmlContent = node.InnerText;
                m_hasHtml = true;
            }

            node = m_doc.SelectSingleNode("//RESPONSE_GROUP/RESPONSE/RESPONSE_DATA/CREDIT_RESPONSE/EMBEDDED_FILE[@_Type='PDF']/DOCUMENT");
            if (null != node)
            {
                m_hasPdf = true;
                m_pdfContent = node.InnerText;
            }

        }
	}
}
