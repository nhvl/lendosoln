using System;
using System.Threading;
using CommonLib;
using DataAccess;
using LendersOffice.CreditReport.Mcl;
using LendersOffice.CreditReport.Mismo;
using LendersOffice.Security;
using Newtonsoft.Json;

namespace LendersOffice.CreditReport
{
    /// <summary>
    /// Contains all the data needed for a credit report request. 
    /// todo Clean up. Maybe break up since theres a lot of MCL only stuff in here. 
    /// </summary>
    public class CreditRequestData
    {
        private BorrowerInfo m_borrower = new BorrowerInfo();
        private BorrowerInfo m_coborrower = new BorrowerInfo(true);
        private LoginInfo m_loginInfo = new LoginInfo();

        [JsonConstructor]
        private CreditRequestData(Guid loanId, Guid appId)
        {
            RequestActionType = E_CreditReportRequestActionType.Submit;
            ReferenceNumber = "";
            ReportID = "";
            InstantViewID = "";
            Url = "";
            RequestingPartyRequestedByName = "";
            BillingFirstName = "";
            BillingLastName = "";
            BillingStreetAddress = "";
            BillingCity = "";
            BillingState = "";
            BillingZipcode = "";
            ProviderId = "";

            LoanId = loanId;
            ApplicationId = appId;
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not we're requesting an LQI report.
        /// </summary>
        /// <value>Value indicating whether or not we're requesting an LQI report.</value>
        public bool IsRequestLqiReport
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not we're requesting a reissue of an LQI report.
        /// </summary>
        /// <value>A value indicating whether or not we're requesting a reissue of an LQI report.</value>
        public bool IsRequestReissueLqiReport { get; set; }

        /// <summary>
        /// 7/26/2013 dd - OPM 128903 - Support Mortgage-Only Credit Report for CBC
        /// </summary>
        public bool IsMortgageOnly { get; set; }
        /// <summary>
        /// GETS or sets the MCL Provider ID
        /// mcl only   av opm 25327 
        /// </summary>
        public string ProviderId { get; set; }
        /// <summary>
        /// OPM 15995 MCL only 
        /// </summary>
        public string ReferenceNumber { get; set; }
        public E_CreditReportRequestActionType RequestActionType { get; set; }

        /// <summary>
        /// When <see cref="RequestActionType"/> is <see cref="E_CreditReportRequestActionType.Other"/>, this specifies
        /// the non-standard credit report action that is occurring.
        /// </summary>
        public CreditReportRequestActionTypeOtherDetail RequestActionTypeOtherDetail { get; set; }

        public string RequestingPartyName { get; set; }
        public string RequestingPartyStreetAddress { get; set; }
        public string RequestingPartyCity { get; set; }
        public string RequestingPartyState { get; set; }
        public string RequestingPartyZipcode { get; set; }
        public bool IsPdfViewNeeded { get; set; }
        public bool IncludeEquifax { get; set; }
        public bool IncludeExperian { get; set; }
        public bool IncludeTransUnion { get; set; }

        /// <summary>
        /// The credit report type, for all CRAs except MCL.
        /// </summary>
        /// <remarks>Trust the UI and the backend to coordinate the enum values.</remarks>
        public int CreditReportType { get; set; }

        /// <summary>
        /// 10/20/2014 jl - OPM 134848 - Support different credit report types (MCL only)
        /// </summary>
        public MclCreditReportType MclCreditReportType { get; set; } = MclCreditReportType.Standard;

        /// <summary>
        /// MCL Only
        /// </summary>
        public bool IncludeFannie { get; set; }

        public string ReportID { get; set; }
        /// <summary>
        /// MCL Only
        /// </summary>
        public string InstantViewID { get; set; }
        public string Url { get; set; }
        public CreditReportProtocol CreditProtocol { get; set; }

        public MclRequestType MclRequestType{ get; set; }
        /// <summary>
        /// MCL Only
        /// </summary>
        public bool UseCreditCardPayment { get; set; }
        public string BillingFirstName { get; set; }
        public string BillingLastName { get; set; }
        public string BillingCardNumber { get; set; }
        public string BillingStreetAddress { get; set; }
        public string BillingCity { get; set; }
        public string BillingState { get; set; }
        public string BillingZipcode { get; set; }
        public int BillingExpirationMonth { get; set; }
        public int BillingExpirationYear { get; set; }
        public string BillingCVV { get; set; }
        /// <summary>
        /// Mismo Only
        /// </summary>
        public string RequestingPartyRequestedByName { get; set; }
        public Guid ActualCraID { get; set; }
        [JsonProperty]
        public BorrowerInfo Borrower
        {
            get { return m_borrower; }
            private set
            {
                m_borrower = value;
            }
        }
        [JsonProperty]
        public BorrowerInfo Coborrower
        {
            get { return m_coborrower; }
            private set
            {
                m_coborrower = value;
            }
        }
        [JsonProperty]
        public LoginInfo LoginInfo
        {
            get { return m_loginInfo; }
            private set
            {
                m_loginInfo = value;
            }
        }
        /// <summary>
        /// This is set to true by UpdateCraInfoFrom  when the given cra id belong to a lender proxy. 
        /// </summary>
        [JsonProperty]
        public bool WasLenderMapping { get; private set; }
        [JsonProperty]
        public Guid ProxyId { get; private set; }

        [JsonProperty]
        public Guid CraId { get; private set; }
        public Guid ApplicationId { get; set; }
        public Guid LoanId { get; set; }

        /// <summary>
        /// Given a cra id it will search for it if it is not found in cra table it will then look for proxy with matching id. 
        /// It will then go ahead and set all the information  and return the cra. 
        /// </summary>
        /// <param name="craId"></param>
        /// <param name="brokerID"></param>
        /// <param name="searchForProxy"></param>
        /// <returns></returns>
        public CRA UpdateCRAInfoFrom(Guid craId, Guid brokerID, bool searchForProxy)
        {
            return UpdateCRAInfoFrom(craId, brokerID, searchForProxy, false);
        }

        /// <summary>
        /// Given a cra id it will search for it if it is not found in cra table it will then look for proxy with matching id. 
        /// It will then go ahead and set all the information  and return the cra. 
        /// </summary>
        /// <param name="craId"></param>
        /// <param name="brokerID"></param>
        /// <param name="searchForProxy"></param>
        /// <param name="includeInvalidCRAs">Includes CRAs set as invalid. It should always be false in LO/PML (AUD needs it).</param>
        /// <returns></returns>
        public CRA UpdateCRAInfoFrom(Guid craId, Guid brokerID, bool searchForProxy, bool includeInvalidCRAs)
        {
            CRA cra = MasterCRAList.FindById(craId, searchForProxy, brokerID, includeInvalidCRAs);
            if (cra == null)
            {
                throw new ArgumentException("The given cra is not valid.");
            }
            CraId = craId;
            Url = cra.Url;
            CreditProtocol = cra.Protocol;
            ProviderId = cra.ProviderId;
            ActualCraID = cra.ID;

            if (cra is LenderMappedCRA)
            {
                LenderMappedCRA map = cra as LenderMappedCRA;
                LoginInfo.UserName = map.Username;
                LoginInfo.Password = map.Password;
                LoginInfo.FannieMaeMORNETPassword = map.FannieMaeMORNETPassword;
                LoginInfo.FannieMaeMORNETUserID = map.FannieMaeMORNETUserID;
                LoginInfo.AccountIdentifier = map.AccountIdentifier; 
                IncludeEquifax = map.IncludeEquifax;
                IncludeExperian = map.IncludeExperian;
                IncludeFannie = map.IncludeFannie;
                IncludeTransUnion = map.IncludeTransUnion;
                ActualCraID = map.ID;
                WasLenderMapping = true;
                
            }
            return cra;
        }

        public static CreditRequestData Create(Guid loanId, Guid appId)
        {
            return new CreditRequestData(loanId, appId);
        }

        public static CreditRequestData CreateForTesting()
        {
            return new CreditRequestData(Guid.Empty, Guid.Empty);
        }
    }
}
