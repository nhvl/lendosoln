using System;
using System.Collections;
using System.Xml;

using DataAccess;
using LendersOffice.CreditReport;
using LendersOffice.Common;
using LendersOffice.CreditReport.Mismo;

namespace LendersOffice.CreditReport.Mismo2_3
{
    public class Mismo2_3CreditReport : MismoCreditReport 
    {
        internal Mismo2_3CreditReport(CreditReportFactory factory, CreditReportProtocol creditReportProtocol, XmlDocument doc, CreditReportDebugInfo debugInfo) : base (factory, creditReportProtocol, doc, debugInfo)
        {
        }
    }
}
