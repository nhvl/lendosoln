using System;
using System.Collections;
using System.Xml;

using DataAccess;
using LendersOffice.CreditReport;
using LendersOffice.Common;

namespace LendersOffice.CreditReport.Mismo2_3
{

	public class Mismo2_3CreditReportResponse : ICreditReportResponse
	{
        private string m_reportID;
        private string m_status;
        private bool m_hasError;
        private bool m_isReady;
        protected string m_errorMessage;
//        private ICreditReport m_creditReport;
        private string m_rawXmlResponse = "";
	
        public Mismo2_3CreditReportResponse(XmlDocument doc) 
        {
            if (null != doc)
                Parse(doc);
        }

        protected virtual string DetermineErrorStatus(string code) 
        {
            return "ERROR"; // Generic error status
        }

        /// <summary>
        /// This method should return contact information for Credit agency.
        /// </summary>
        /// <returns></returns>
        protected virtual string GetCustomErrorMessage(string code) 
        {
            return "";
        }
        protected bool IsValidMismoResponse(XmlDocument doc) 
        {
            if (doc == null) return false;

            XmlElement mismoElement = (XmlElement) doc.SelectSingleNode("//RESPONSE_GROUP/RESPONSE");
            return null != mismoElement;

        }

        protected virtual void SetErrorInfo(string message) 
        {
            m_isReady = false;
            m_hasError = true;
            m_status = "ERROR";
            m_errorMessage = message;
        }
        
        private void Parse(XmlDocument doc) 
        {

            // Check to make sure Xml is valid Mismo document.
            if (!IsValidMismoResponse(doc)) 
            {
                m_isReady = false;
                m_hasError = true;
                m_errorMessage = "Not a valid MISMO response. <br>" + GetCustomErrorMessage("");
                return;
            }
            XmlElement creditResponseElement = (XmlElement) doc.SelectSingleNode("//RESPONSE_GROUP/RESPONSE/RESPONSE_DATA/CREDIT_RESPONSE");
            if (creditResponseElement != null && creditResponseElement.GetAttribute("CreditReportType").ToUpper() != "ERROR")
            {
                // No error occurs.
                m_isReady = true;
                m_status = "READY";
            }
            else
            {
                XmlElement errorElement = (XmlElement)doc.SelectSingleNode("//RESPONSE_GROUP/RESPONSE/RESPONSE_DATA/CREDIT_RESPONSE/CREDIT_ERROR_MESSAGE");
                if (null != errorElement)
                {
                    // 12/29/2003 dd - I currently don't have a list of what error code mean.
                    // After talking with Alan, there is no standard way in MISMO to determine if error related to login failure.
                    m_status = DetermineErrorStatus(errorElement.GetAttribute("_Code"));

                    m_isReady = false;
                    m_hasError = true;
                    XmlNodeList errorNodeList = errorElement.SelectNodes("_Text");

                    foreach (XmlNode msgNode in errorNodeList)
                    {
                        m_errorMessage += msgNode.InnerText + "<br/>";
                    }

                    if(string.IsNullOrEmpty(m_errorMessage))
                    {
                        // 12/29/2003 dd - Generic error message since I can't determine from credit response.
                        m_errorMessage = "Error while ordering report.";
                    }

                    string msg = GetCustomErrorMessage(errorElement.GetAttribute("_Code"));
                    if (msg == "")
                    {
                        // If there is no custom agency information then return generic message.
                        msg = "If you are unable to login for credit after several tries, please contact your Credit Reporting Agency for assistance.";
                    }
                    m_errorMessage += "<br>" + msg;
                }
                else
                {
                    // No error occurs.
                    m_isReady = true;
                    m_status = "READY";
                }
            }

            // 10/12/2011 dd - Always try to parse CreditReportIdentifier.
            //XmlElement creditResponseElement = (XmlElement)doc.SelectSingleNode("//RESPONSE_GROUP/RESPONSE/RESPONSE_DATA/CREDIT_RESPONSE");
            if (null != creditResponseElement)
            {
                m_reportID = creditResponseElement.GetAttribute("CreditReportIdentifier");
            }
            m_rawXmlResponse = doc.InnerXml;
        }

        #region Implementation of ICreditReportResponse
        public virtual string ReportID 
        {
            get { return m_reportID; }
        }
        public string Status 
        {
            get { return m_status; }
        }

        public bool HasError 
        {
            get { return m_hasError; }
        }
        public string ErrorMessage 
        {
            get { return m_errorMessage; }
        }
//        public ICreditReport CreditReport 
//        {
//            get { return m_creditReport; }
//        }

        public bool IsReady 
        {
            get { return m_isReady; }
        }
        public string RawXmlResponse 
        {
            get { return m_rawXmlResponse; }
        }

        public string BrandedProductName { get; protected set; }
        #endregion

    }
}
