using System;
using System.Collections;
using System.Collections.Specialized;
using System.Xml;

using CommonLib;
using DataAccess;
using LendersOffice.CreditReport;
using LendersOffice.CreditReport.Mismo;
using LendersOffice.Common;

namespace LendersOffice.CreditReport.Mismo2_3
{
    public class Mismo2_3CreditReportRequest : MismoCreditReportRequest
    {
        protected override string MISMOVersionID 
        {
            get { return "2.3.1"; }
        }

        protected override bool IncludeMailingAddress
        {
            get { return true; }
        }

        public Mismo2_3CreditReportRequest(string url) : base (url)
        {
        }

        #region Implementation of ICreditReportRequest

        public override LendersOffice.CreditReport.ICreditReportResponse CreateResponse(System.Xml.XmlDocument doc)
        {
            return new Mismo2_3CreditReportResponse(doc);
        }
        #endregion

    }
}
