using System;
using System.Collections;
using System.Xml;

using DataAccess;
using LendersOffice.CreditReport;
using LendersOffice.CreditReport.Mismo;
using LendersOffice.Common;

namespace LendersOffice.CreditReport.Mismo2_3
{
	/// <summary>
	/// Summary description for MISMO2_3CreditLiability.
	/// </summary>
	public class Mismo2_3CreditLiability : MismoCreditLiability
	{
        public Mismo2_3CreditLiability(XmlElement el, string borrowerID, string coborrowerID, AbstractCreditReport parent) : base(el, borrowerID, coborrowerID, parent)
        {
        }


	}
}
