﻿namespace LendersOffice.CreditReport.Equifax
{
    using System.Xml;

    /// <summary>
    /// Equifax credit report response.
    /// </summary>
    public class Equifax_CreditReportResponse : LendersOffice.CreditReport.Mismo2_3.Mismo2_3CreditReportResponse
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Equifax_CreditReportResponse"/> class.
        /// </summary>
        /// <param name="doc">The xml document.</param>
        /// <param name="brandedProductName">The branded product name of this order.</param>
        public Equifax_CreditReportResponse(XmlDocument doc, string brandedProductName)
            : base(doc)
        {
            this.BrandedProductName = brandedProductName;
        }
    }
}
