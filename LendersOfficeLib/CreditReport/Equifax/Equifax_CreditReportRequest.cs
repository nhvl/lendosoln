﻿namespace LendersOffice.CreditReport.Equifax
{
    using System.Net;
    using System.Xml;
    using Mismo;

    /// <summary>
    /// Equifax credit report request.
    /// </summary>
    public class Equifax_CreditReportRequest : LendersOffice.CreditReport.Mismo2_3.Mismo2_3CreditReportRequest
    {
        /// <summary>
        /// The submitting party name.
        /// </summary>
        private const string SubmittingParty = "9K";

        /// <summary>
        /// The product name represented by this request.
        /// </summary>
        private string brandedProductName = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="Equifax_CreditReportRequest"/> class.
        /// </summary>
        /// <param name="url">The url to hit.</param>
        public Equifax_CreditReportRequest(string url)
            : base(url)
        {
            this.SubmittingPartyName = SubmittingParty;
            this.AppendOutputFormat(E_MismoOutputFormat.PDF);
            this.AppendOutputFormat(E_MismoOutputFormat.XML);
        }

        /// <summary>
        /// Gets a value indicating whether this instance can be converted into a Loan Quality Initiative Credit Report.
        /// </summary>
        /// <value>A value indicating whether this instance can be converted into a Loan Quality Initiative Credit Report.</value>
        public override bool CanConvertRequestTypeToLqi => true;

        /// <summary>
        /// Converts this instance into a Loan Quality Initiative Credit Report.
        /// </summary>
        /// <param name="creditRequestData">The credit request data to apply to this instance.</param>
        public override void ConvertRequestTypeToLqi(CreditRequestData creditRequestData)
        {
            this.RequestType = E_CreditReportRequestActionType.Submit;
            this.CreditReportTypeOtherDescription = "Preclose";
            this.CreditReportType = E_CreditReportType.Other;
            this.IncludeEquifax = true;
            this.IncludeExperian = true;
            this.IncludeTransunion = true;
            this.brandedProductName = "Preclose Credit Report";
        }

        /// <summary>
        /// Creates an Equifax credit report response.
        /// </summary>
        /// <param name="doc">The xml document.</param>
        /// <returns>An Equifax credit report response.</returns>
        public override ICreditReportResponse CreateResponse(XmlDocument doc)
        {
            return new Equifax_CreditReportResponse(doc, this.brandedProductName);
        }

        /// <summary>
        /// Sets the web request content type to text/xml.
        /// </summary>
        /// <param name="webRequest">The web request to modify.</param>
        public override void SetupAuthentication(HttpWebRequest webRequest)
        {
            webRequest.ContentType = "text/xml";
        }
    }
}
