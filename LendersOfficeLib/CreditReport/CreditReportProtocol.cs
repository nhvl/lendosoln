using System;

namespace LendersOffice.CreditReport
{
    public enum CreditReportProtocol
    {
        Mcl = 0, // Mortgage Credit Link credit providers
        MISMO_2_1 = 1, // Generic MISMO 2.1
        Landsafe = 2, // Landsafe using MISMO 2.1
        MISMO_2_3 = 3, // Generic MISMO 2.3
        KrollFactualData = 4,  // Kroll Factual Data using MISMO 2.3
        Credco = 5, // CoreLogic CREDCO
        StandFacts = 6, // StandFacts Credit Services. MISMO 2.3
        UniversalCredit = 7, // Universal Credit. MISMO 2.3
        Fiserv = 8, // Fiserv / CredStar  Mismo 2.1
        FannieMae = 9,
        InfoNetwork = 10, // Information Network. Mismo 2.3
        SharperLending = 11, // Formerly Sarma. Mismo 2.3
		CreditInterlink = 12, // Credit Quick Services and Old Republic Credit Services. MISMO 2.3
		CSC = 13, // Equifax MISMO 2.3
		CSD = 14, // Credit Systems Design MISMO 2.3
		CBC = 15, // CBCInnovis
        FundingSuite = 16, // Funding Suite MISMO 2.3
        InformativeResearch = 17, // Informative Research MISMO 2.3
        Equifax = 18, // Equifax MISMO 2.3
        LenderMappingCRA = 255 // Special CRA that Lender use to mask with actual CRA.
    }
}
