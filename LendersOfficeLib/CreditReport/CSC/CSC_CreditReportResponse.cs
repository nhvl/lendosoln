using System;
using System.Xml;
using DataAccess;

namespace LendersOffice.CreditReport.CSC
{
	public class CSC_CreditReportResponse : LendersOffice.CreditReport.ICreditReportResponse
	{
		private string		m_reportID;
		private string		m_status;
		private bool		m_hasError;
		private bool		m_isReady;
		protected string	m_errorMessage;
		private string		m_rawXmlResponse = "";
		
		public CSC_CreditReportResponse(XmlDocument doc)
		{
			if (null != doc)
				Parse(doc);
		}

		protected virtual string DetermineErrorStatus(string code) 
		{
			return "ERROR"; // Generic error status
		}

		private void Parse(XmlDocument doc) 
		{
			XmlElement creditResponseElement = (XmlElement) doc.SelectSingleNode("//RESPONSE_GROUP/RESPONSE/RESPONSE_DATA/CREDIT_RESPONSE");

			if (null == creditResponseElement)
				throw new DataAccess.CBaseException(Common.ErrorMessages.InvalidMismoFormat, "Invalid Mismo Format");

			XmlElement errorElement = (XmlElement) doc.SelectSingleNode("//RESPONSE_GROUP/RESPONSE/RESPONSE_DATA/CREDIT_RESPONSE/CREDIT_ERROR_MESSAGE");

			if (null != errorElement) 
			{
				m_isReady = false;
				m_hasError = true;
				XmlNodeList nodeList = errorElement.SelectNodes("_Text");

				m_status = "ERROR";

				//CSC seems to use the following format:
				//<_Text>Transaction type: REQUEST ERROR</_Text>
				//<_Text>Invalid Password</_Text>
				// So we typically need to grab the second text, not the first, unless only 1 exists
				if(nodeList.Count == 1)
				{
					m_errorMessage = ((XmlElement)nodeList[0]).InnerText;
				}
				else if(nodeList.Count > 1)
				{
					m_errorMessage = ((XmlElement)nodeList[1]).InnerText;
				}
				else
				{
					// 12/29/2003 dd - Generic error message since I can't determine from credit response.
					m_errorMessage = "Error while ordering report.";
				}
				m_errorMessage += "<br> If you are unable to login for credit after several tries, please contact your Credit Reporting Agency for assistance.";
			} 
			else 
			{
				// No error occurs.
				m_reportID = creditResponseElement.GetAttribute("CreditReportIdentifier");
				m_isReady = true;
				m_status = "READY"; 
			}

			if (m_isReady) 
				m_rawXmlResponse = doc.InnerXml;
		}

		#region Implementation of ICreditReportResponse
		public string ReportID 
		{
			get { return m_reportID; }
		}
		public string Status 
		{
			get { return m_status; }
		}

		public bool HasError 
		{
			get { return m_hasError; }
		}
		public string ErrorMessage 
		{
			get { return m_errorMessage; }
		}

		public bool IsReady 
		{
			get { return m_isReady; }
		}

		public string RawXmlResponse 
		{
			get { return m_rawXmlResponse; }
        }

        public string BrandedProductName { get; }
        #endregion
    }
}