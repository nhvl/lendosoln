using System;
using System.Net;
using System.Xml;
using LendersOffice.CreditReport.Mismo;
using DataAccess;

namespace LendersOffice.CreditReport.CSC
{
	public class CSC_CreditReportRequest : LendersOffice.CreditReport.Mismo2_3.Mismo2_3CreditReportRequest
	{
		private const string URL = "https://emsws.equifax.com/emsws/services/post/MergeCreditWWW";
		
		public CSC_CreditReportRequest() : base("")
		{
			// 4/03/2008 db - OPM 21295 - Add key/value pair to indicate the request came from us
			AddRequestKeyValue("pmlTransaction", "yes");
		}

        protected override bool IncludeMailingAddress
        {
            get { return false; }
        }

        public override string Url 
		{
			get { return URL; }
		}

		public override ICreditReportResponse CreateResponse(XmlDocument doc) 
		{
			return new CSC_CreditReportResponse(doc);
		}

		public override void SetupAuthentication(HttpWebRequest webRequest) 
		{
			webRequest.ContentType = "application/xml";
			//For CSC, Account Id is Login Id
			this.LoginInfo.AccountIdentifier = this.LoginInfo.UserName;
		}
	}
}
