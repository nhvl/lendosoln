﻿// Generated via GenerateRecordClass.ttinclude
namespace LendersOffice.CreditReport
{
    using System;

    /// <summary>
    /// Encapsulates a document saved by the system after being received from the credit vendor, e.g., for an additional service.
    /// </summary>
    public partial class CreditVendorAdditionalSavedDocument
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CreditVendorAdditionalSavedDocument"/> class.
        /// </summary>
        /// <param name="id">The primary key of the row for the saved document.</param>
        /// <param name="brokerId">The identifier of the lender.</param>
        /// <param name="loanId">The identifier of the loan.</param>
        /// <param name="appId">The identifier of the loan application.</param>
        /// <param name="vendorId">The identifier of the vendor.</param>
        /// <param name="reportId">The identifier of the order in the credit vendor's system.</param>
        /// <param name="fileName">The unique file name specified by the vendor for this document.</param>
        /// <param name="documentId">The identifier of the EDocument created from the document of the vendor.</param>
        /// <param name="documentChecksum">The checksum of the document content.</param>
        public CreditVendorAdditionalSavedDocument(int id, Guid brokerId, Guid loanId, Guid appId, Guid vendorId, string reportId, string fileName, Guid documentId, LqbGrammar.DataTypes.SHA256Checksum documentChecksum)
        {
            this.Id = id;
            this.BrokerId = brokerId;
            this.LoanId = loanId;
            this.AppId = appId;
            this.VendorId = vendorId;
            this.ReportId = reportId;
            this.FileName = fileName;
            this.DocumentId = documentId;
            this.DocumentChecksum = documentChecksum;
        }

        /// <summary>
        /// Gets the primary key of the row for the saved document.
        /// </summary>
        public int Id { get; }

        /// <summary>
        /// Gets the identifier of the lender.
        /// </summary>
        public Guid BrokerId { get; }

        /// <summary>
        /// Gets the identifier of the loan.
        /// </summary>
        public Guid LoanId { get; }

        /// <summary>
        /// Gets the identifier of the loan application.
        /// </summary>
        public Guid AppId { get; }

        /// <summary>
        /// Gets the identifier of the vendor.
        /// </summary>
        public Guid VendorId { get; }

        /// <summary>
        /// Gets the identifier of the order in the credit vendor's system.
        /// </summary>
        public string ReportId { get; }

        /// <summary>
        /// Gets the unique file name specified by the vendor for this document.
        /// </summary>
        public string FileName { get; }

        /// <summary>
        /// Gets the identifier of the EDocument created from the document of the vendor.
        /// </summary>
        public Guid DocumentId { get; }

        /// <summary>
        /// Gets the checksum of the document content.
        /// </summary>
        public LqbGrammar.DataTypes.SHA256Checksum DocumentChecksum { get; }
    }
}