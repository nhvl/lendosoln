using System;
using System.Collections;
using System.Xml;

using DataAccess;
using LendersOffice.Common;
using LendersOffice.CreditReport.Mcl;
using LendersOffice.CreditReport.Mcl.Code;
using LendersOffice.CreditReport.Mismo;

namespace LendersOffice.CreditReport
{
    internal class CreditAdverseRatingComparer : IComparer  
    {

        public int Compare(object a, object b) 
        {
            // 10/6/2004 dd - Perform descending order.
            CreditAdverseRating _a = (CreditAdverseRating) a;
            CreditAdverseRating _b = (CreditAdverseRating) b;
            return _b.Date.CompareTo(_a.Date);
        }

    }    
    public class CreditAdverseRating 
    {
        private E_AdverseType m_adverseType;
        private DateTime m_date;

        // 6/28/2005 dd - Relate to case 2234 - This two bits added to separate out ForeclosureOrRepossession adverse type. In credco credit report
        // you could tell if the adverse rating is either Foreclosure or Repossession from _PRIOR_ADVERSE_RATING, not sure about other CRA.
        private bool m_isRepossessionOnly = false;
        private bool m_isForeclosureOnly = false;

        public bool IsRepossessionOnly 
        {
            get { return m_isRepossessionOnly; }
            set 
            { 
                m_isRepossessionOnly = true; 
                m_isForeclosureOnly = false;
            }
        }

        public bool IsForeclosureOnly 
        {
            get { return m_isForeclosureOnly; }
            set 
            {
                m_isForeclosureOnly = true;
                m_isRepossessionOnly = false;
            }
        }

        /// <summary>
        /// Use this property if E_AdverseType.RepossessionOrForeclosure is not enough. Relate to case 2234.
        /// </summary>
        public bool IsRepossession 
        {
            get 
            {
                if (m_adverseType == E_AdverseType.RepossessionOrForeclosure) 
                {
                    if (m_isRepossessionOnly) 
                        return true;
                    else if (!m_isForeclosureOnly && !m_isRepossessionOnly)
                        return true; // Return true because unable to distinguish if it is a repossession or foreclosure only
                }
                return false;
            }
        }

        /// <summary>
        /// Use this property if E_AdverseType.RepossessionOrForeclosure is not enough. Relate to case 2234.
        /// </summary>
        public bool IsForeclosure 
        {
            get 
            {
                if (m_adverseType == E_AdverseType.RepossessionOrForeclosure) 
                {
                    if (m_isForeclosureOnly)
                        return true;
                    else if (!m_isForeclosureOnly && !m_isRepossessionOnly)
                        return true; // Return true because unable to distinguish if it is a repossession or foreclosure only
                }
                return false;
            }

        }

        public CreditAdverseRating(E_AdverseType adverseType, DateTime date) 
        {
            m_adverseType = adverseType;
            m_date = date;
        }

        public E_AdverseType AdverseType 
        {
            get { return m_adverseType; }
            set { m_adverseType = value; }
        }
        public DateTime Date 
        {
            get { return m_date; }
        }

        public override bool Equals(object o) 
        {
            CreditAdverseRating a = o as CreditAdverseRating;
            if (null == a)
                return false;
            // 11/22/2004 dd - Adverse Rating are the same if same type occur in the same month. Ignore date.
            // 12/15/2004 dd - Late120 is consider equal to Late120 | Late150 | Late180
            return ((a.AdverseType & this.AdverseType) != 0 && a.Date.Year == this.Date.Year && a.Date.Month == this.Date.Month);
        }
        public override int GetHashCode() 
        {
            return m_adverseType.GetHashCode() ^ m_date.GetHashCode();
        }
        public override string ToString() 
        {
            return string.Format("[AdverseType:Type={0},Date={1}, IsForeclosureOnly={2}, IsRepossessionOnly={3}]", m_adverseType, m_date.ToShortDateString(), m_isForeclosureOnly, m_isRepossessionOnly);
        }
    }


	public abstract class AbstractCreditLiability 
	{
		private ArrayList m_lateOnlyCreditAdverseRatingList = null;
		private AbstractCreditReport m_parentCreditReport = null;
		private DateTime dtLatestFC = DateTime.MinValue;
		private DateTime dtLatestBK = DateTime.MinValue;
		private DateTime dtLatestCollection = DateTime.MinValue;
        private AbstractCreditPublicRecord latestBKRecord = null;
        private bool m_ImportWillBePaidOff = false;
        private bool m_WillBePaidOff = false;
        private bool m_Deleted = false;

        /// <summary>
        /// av lpq 45130 this is only for LPQ and MCL XML Format DONT USE.
        /// </summary>
        public bool ImportWillBePaidOff
        {
            get { return m_ImportWillBePaidOff; }
            protected set { m_ImportWillBePaidOff = value; }
        }

        public bool Deleted
        {
            get { return m_Deleted; }
            protected set { m_Deleted = value; }
        }

        /// <summary>
        /// av lpq 45130 this is only for LPQ and MCL XML Format DONT USE.
        /// </summary>
        public bool WillBePaidOff
        {
            get { return m_WillBePaidOff; }
            protected set { m_WillBePaidOff = value; }
        }

        protected AbstractCreditPublicRecord LatestBKRecord 
        {
            get { return latestBKRecord; } 
        }
		protected AbstractCreditReport ParentCreditReport 
		{
			set { m_parentCreditReport = value; }
			get { return m_parentCreditReport; }
		}

		public void LogCreditWarning(Exception exc) 
		{
			if (null != exc)
				LogCreditWarning(exc.ToString());
		}
		public void LogCreditWarning(string msg) 
		{
			msg = "[TradelineID:" + this.ID + "] " + msg;
			if (null != m_parentCreditReport)
				m_parentCreditReport.LogCreditWarning(msg);
			else
				Tools.LogWarning(msg); // Parent object should not be null. But add this else just in case.

		}

		public void LogCreditBug(string msg) 
		{
			msg = "[TradelineID:" + this.ID + "] " + msg;

			if (null != m_parentCreditReport)
				m_parentCreditReport.LogCreditBug(msg);
			else
				Tools.LogBug(msg); // Parent object should not be null. But add this else just in case.
		}

        #region Abstract methods required to be implement by class inherited from this.
		public abstract string ID { get; }
		public abstract string CreditorName { get; }
		public abstract string CreditorAddress { get; }
		public abstract string CreditorCity { get; }
		public abstract string CreditorState { get; }
		public abstract string CreditorZipcode { get; }
		public abstract string CreditorPhone { get; }

		public abstract DateTime AccountOpenedDate { get; }
		public abstract DateTime AccountReportedDate { get; }
		public abstract decimal PastDueAmount { get; }
		public abstract decimal HighCreditAmount { get; }
		public abstract string AccountIdentifier { get; }
		public abstract string TermsMonthsCount { get; }
		public abstract decimal UnpaidBalanceAmount { get; }
        public abstract void UpdateUnpaidBalanceAmount(decimal value);
        public abstract string MonthsRemainingCount { get; }

		public abstract decimal MonthlyPaymentAmount { get; }
	    public abstract void UpdateMonthlyPaymentAmount(decimal value);
        public abstract bool IsModified { get; }
		public abstract bool IsDischargedBk { get; }
        public abstract string ModifiedString { get; }
        public abstract bool IsAuthorizedUser 
		{
			// MCL Definition: Authorized user: A joint account where the borrower is an authorized user, but has no contractual responsibility
			get; 
		}
		public abstract bool IsStudentLoansNotInRepayment { get; }
		public abstract bool IsGovMiscDebt { get; }

        /// <summary>
        /// Return whether the current mortgage tradeline is for subject property. This depend on Account Number is match between credit report
        /// and LendingQB Liability. 11/16/2009 dd.
        /// </summary>
        public bool IsSubjectPropertyMortgage 
        {
            get
            {
                if (LiabilityType.EqualsOneOf(E_DebtRegularT.Mortgage, E_DebtRegularT.Heloc) && UnpaidBalanceAmount > 0)
                {
                    CAppBase dataApp = this.ParentCreditReport.DataApp;
                    if (null != dataApp)
                    {
                        ILiaCollection aLiaCollection = dataApp.aLiaCollection;
                        for (int i = 0; i < aLiaCollection.CountRegular; i++)
                        {
                            ILiabilityRegular liaRegular = aLiaCollection.GetRegularRecordAt(i);
                            if (liaRegular.DebtT.EqualsOneOf(E_DebtRegularT.Mortgage, E_DebtRegularT.Heloc) && 
                                liaRegular.AccNum.Value == this.AccountIdentifier)
                            {
                                return liaRegular.IsSubjectPropertyMortgage;
                            }
                        }
                    }
                    return true; // 11/16/2009 dd - Unable to find AppData default to true. This is a mortgage for subject property.
                }
                else
                {
                    return false;
                }
            }
        }
        public bool IsSubjectProperty1stMortgage
        {
            get
            {
                if (LiabilityType.EqualsOneOf(E_DebtRegularT.Mortgage, E_DebtRegularT.Heloc) && UnpaidBalanceAmount > 0)
                {
                    CAppBase dataApp = this.ParentCreditReport.DataApp;
                    if (null != dataApp)
                    {
                        ILiaCollection aLiaCollection = dataApp.aLiaCollection;
                        for (int i = 0; i < aLiaCollection.CountRegular; i++)
                        {
                            ILiabilityRegular liaRegular = aLiaCollection.GetRegularRecordAt(i);
                            if (liaRegular.DebtT.EqualsOneOf(E_DebtRegularT.Mortgage, E_DebtRegularT.Heloc) && 
                                liaRegular.AccNum.Value == this.AccountIdentifier)
                            {
                                return liaRegular.IsSubjectProperty1stMortgage;
                            }
                        }
                    }
                    return true; // 11/16/2009 dd - Unable to find AppData default to true. This is a mortgage for subject property.
                }
                else
                {
                    return false;
                }
            }
        }
		/// <summary>
		/// Must be active &amp; is not derog currently regardless of the past derog.
		/// </summary>
		/// <returns></returns>
		public bool IsCurrent
		{
			get
			{
				if( IsTypeInstallmentBase && IsPaid )
					return false;

				if( !IsActive )
					return false;

				if( IsDerogCurrently )
					return false;

				return true;
			}
		}

		public abstract string Late30 { get; }
		public abstract string Late60 { get; }
		public abstract string Late90 { get; }
		public abstract string Late120 { get; } // Only define in Mismo.

		public abstract int MonthsReviewedCount { get; }
		public abstract DateTime LastActivityDate { get; }

		public abstract E_DebtRegularT LiabilityType { get; }
		public abstract E_LiaOwnerT LiaOwnerT { get; }
		public abstract bool IsForeclosure { get; }
		public abstract bool IsBankruptcy { get; }
		public abstract bool IsChargeOff { get; }
		public abstract DateTime PaymentPatternStartDate { get; }
		public abstract bool IsCollection { get; }
		public abstract bool IsRepos { get; }

		public abstract bool IsInCreditCounseling { get; }

		/*
		public abstract bool IsInactiveButNeutralStatus { get; }
		public abstract bool IsInactiveButNegativeStatus { get; }
		*/

		/// <summary>
		/// Return true if current tradeline has late status.
		/// In MCL, if account_status_code is '30 days', '60 days', '90 days', '120+ days', then it will return true.
		/// </summary>
		public abstract bool IsLate { get; }

		/// <summary>
		/// Return a sorted list (latest to earliest) of all adverse rating (late, foreclosure, bk, etc..) happen to this accounts.
		/// In MCL: Combine from two_year_history and late_dates attributes.
		/// 
		/// In Mismo: Combine from _HIGHEST_ADVERSE_RATING, _MOST_RECENT_ADVERSE_RATING, _PAYMENT_PATTERN, _PRIOR_ADVERSE_RATING
		/// </summary>
		public abstract ArrayList CreditAdverseRatingList { get; }


        #endregion

        #region Abstract methods 
		public abstract bool IsMedicalTradeline { get; }
        #endregion


        #region Methods could be override in base class if implementation is different.
		public virtual bool IsTypeRevolvingBase 
		{ 
			get { return LiabilityType == E_DebtRegularT.Revolving; }
		}

		public virtual bool IsTypeInstallmentBase 
		{ 
			get 
			{ 
				switch (LiabilityType) 
				{
					case E_DebtRegularT.Installment:
					case E_DebtRegularT.Mortgage:
                    case E_DebtRegularT.Heloc:
                        return true;
					default:
						return false;

				}
			}
		}

		public virtual bool IsTypeMortgage 
		{ 
			get { return LiabilityType == E_DebtRegularT.Mortgage; }
		}

        public virtual bool IsTypeLease
        {
            get { return LiabilityType == E_DebtRegularT.Lease; }
        }

        public virtual bool IsTypeHeloc
        {
            get { return LiabilityType == E_DebtRegularT.Heloc; }
        }

        public virtual bool IsTypeOpen 
		{ 
			get { return LiabilityType == E_DebtRegularT.Open; }
		}

		public virtual bool IsTypeOther 
		{ 
			get { return LiabilityType.EqualsOneOf(E_DebtRegularT.Other, E_DebtRegularT.OtherExpenseLiability); }
		}

        /// <summary>
        /// Get a value indicating whether this liability is for a short sale.
        /// </summary>
        /// <value>True, if this liability is for a short sale.</value>
        public virtual bool IsShortSale
        {
            get
            {
                return false;
            }
        }

        // 6/15/2006 - nw - OPM 4822, by default, this will always return false, 
        // except when we do a hardcoded text conversion in a MCL Liability.
        public virtual bool ForeclosureWasStarted
		{
			get { return false; }
		}


        #endregion

		/// <summary>
		/// Will return 0 if late number string is blank (undefined)
		/// </summary>
		public int Late30Count 
		{
			get 
			{ 
				try 
				{
					return int.Parse(Late30); 
				} 
				catch 
				{ 
					return 0; 
				}
			}
		}

		/// <summary>
		/// Will return 0 if late number string is blank (undefined)
		/// </summary>
		public int Late60Count 
		{
			get 
			{ 
				try 
				{
					return int.Parse(Late60); 
				} 
				catch 
				{
					return 0; 
				}
			}
		}

		/// <summary>
		/// Will return 0 if late number string is blank (undefined)
		/// </summary>
		public int Late90Count 
		{
			get 
			{ 
				try 
				{

					return int.Parse(Late90); 
				} 
				catch 
				{
					return 0;
				}
			}
		}


        /// <summary>
        /// Will return 0 if late number string is blank (undefined)
        /// </summary>
        public int Late120Count 
        {
            get 
            { 
                try 
                {

                    return int.Parse(Late120); 
                } 
                catch 
                {
                    return 0;
                }
            }
        }

		public int Late150Count
		{
			get
			{
				int count = 0;
                foreach (CreditAdverseRating o in LateOnlyCreditAdverseRatingList) 
                {
                    if( ( o.AdverseType & E_AdverseType.Late150 ) != 0 )
                        count++;
                }
				return count;
			}
		}

		public int Late180Count
		{
			get
			{
				int count = 0;
                foreach (CreditAdverseRating o in LateOnlyCreditAdverseRatingList) 
                {
                    if( ( o.AdverseType & E_AdverseType.Late180 ) != 0 )
                        count++;
                }
				return count;
			}
		}

		public bool IsLateWithin( int nMonths, E_AdverseType lateTypes )
		{
			if( nMonths > 24 )
			{
				if( ( ( lateTypes & E_AdverseType.Late30 ) != 0 ) && Late30Count > 0 )
					return true;
				if( ( ( lateTypes & E_AdverseType.Late60 ) != 0 ) && Late60Count > 0 )
					return true;
				if( ( ( lateTypes & E_AdverseType.Late90 ) != 0 ) && Late90Count > 0 )
					return true;
				if( ( ( lateTypes & E_AdverseType.Late120 ) != 0 ) && Late120Count > 0 )
					return true;
				if( ( ( lateTypes & E_AdverseType.Late150 ) != 0 ) && Late150Count > 0 )
					return true;
				if( ( ( lateTypes & E_AdverseType.Late180 ) != 0 ) && Late180Count > 0 )
					return true;
				return false;
			}

			E_AdverseType lates = lateTypes & E_AdverseType.AllLate;
			Tools.Assert( lates == lateTypes, "Expect AbstractCreditLiability:IsLateWithin() to receive only late types as argument." );
            
			foreach ( CreditAdverseRating r in LateOnlyCreditAdverseRatingList )  // we are going from the latest to the earliest
				//foreach( CreditAdverseRating late in t )
			{
				DateTime recordD = r.Date;
											
				if( !Tools.IsDateWithin( recordD, nMonths ) )
					break; // done searching

				if( ( r.AdverseType & lates ) != 0 )
					return true;				
			}

			return false;
		}

        /// <summary>
        /// This should override status as status is often not accurately updated.
        /// </summary>
        public bool IsPaid
        {
            get{ return UnpaidBalanceAmount <= 0; }
        }

        public bool IsCredit 
        {
            get { return !LiabilityType.EqualsOneOf(E_DebtRegularT.Other, E_DebtRegularT.OtherExpenseLiability); }
        }

        abstract public bool IsActive
        {
            get;
        }

        abstract public  bool HasBeenDerog
        {
            get;
        }

		/// <summary>
		/// If last status of the 
		/// </summary>
        abstract public bool IsDerogCurrently
        {
            get;
        }

		abstract public bool IsLastStatusDerog
		{
			get;
		}

        /// <summary>
        /// Filter out CreditAdverseRatingList for only Late type. Late30, Late60, Late90, Late120, Late150, Late180
        /// </summary>
        public ArrayList LateOnlyCreditAdverseRatingList 
        {
            get 
            {
                if (null == m_lateOnlyCreditAdverseRatingList) 
                {
                    m_lateOnlyCreditAdverseRatingList = new ArrayList();

                    foreach (CreditAdverseRating o in CreditAdverseRatingList) 
                    {
//                        if ((o.AdverseType & E_AdverseType.AllLate) == o.AdverseType)
                        if ((o.AdverseType & E_AdverseType.AllLate) != 0)
                            m_lateOnlyCreditAdverseRatingList.Add(o);
                    }
                }
                return m_lateOnlyCreditAdverseRatingList;
            }
        }


        /// <summary>
        /// 11/20/04 dd - This method will check if Adverse rating occured within nMonths.
        /// 
        /// i.e: Today Date: 11/04.
        ///      Adverse History. Late30 11/04, Late30 11/02.
        ///      
        ///      This method will return true for Late30 within 12 months.
        /// 
        /// </summary>
        /// <returns></returns>
        public bool IsFirstPmtTypeWithin(E_AdverseType adverseType, int nMonths) 
        {
            foreach (CreditAdverseRating o in CreditAdverseRatingList) 
            {
                if (0 != (o.AdverseType & adverseType)) 
                {
                    if (Tools.IsDateWithin(o.Date, nMonths))
                        return true;
                }
            }
            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="adverseType"></param>
        /// <returns>Returns DateTime.MinValue if there isn't such adverseType in the pmt history</returns>
        public DateTime DateOfLastestAdversePmtType( E_AdverseType adverseType )
        {
            foreach (CreditAdverseRating o in CreditAdverseRatingList) 
            {
                if (0 != (o.AdverseType & adverseType)) 
                {
                    return o.Date;
                }
            }
            return DateTime.MinValue;
        }

        abstract public bool HasTerminalDerog { get; }

        /// <summary>
        /// See OPM case 3464
        /// </summary>
        virtual public DateTime EndDerogDate 
        {
            get
            {
                if( HasTerminalDerog )
                {
					if (CreditAdverseRatingList.Count != 0)
					{
						// 4/9/2007 nw - OPM 3464 - Terminal derog may not be current, may be reported prematurely in the past
						CreditAdverseRating rating = (CreditAdverseRating) CreditAdverseRatingList[0];
						E_AdverseType mostRecentDerogType = rating.AdverseType;
						E_AdverseType allTerminalDerogTypes = E_AdverseType.Bankruptcy | 
															  E_AdverseType.ChargeOffOrCollection | 
															  E_AdverseType.RepossessionOrForeclosure;

						if ((mostRecentDerogType & allTerminalDerogTypes) == 0)
							return rating.Date;

						// most recent derog type is a terminal derog, find the first month of possible consecutive reporting of this derog type
						// start from index 1 since we already know index 0 is a terminal derog
						int index;
						for (index = 1; index < CreditAdverseRatingList.Count; index++)
						{
							CreditAdverseRating o = (CreditAdverseRating) CreditAdverseRatingList[index];
							if ((o.AdverseType & allTerminalDerogTypes) == 0)
								break;
						}
						return ((CreditAdverseRating) CreditAdverseRatingList[index - 1]).Date;
					}

                    if (LastActivityDate != DateTime.MinValue)
                        return LastActivityDate;
 
                    if (AccountReportedDate != DateTime.MinValue)
                        return AccountReportedDate;

                    if (PaymentPatternStartDate != DateTime.MinValue)
                        return PaymentPatternStartDate;

                    return DateTime.Today;
                }
              
                return DateOfLastestAdversePmtType( E_AdverseType.ALL );
            }
        }

        /*
        /// <summary>
        /// Only for MCL Liability, we need to override this method to include "Last Late Date". Otherwise it should be the same as EndDate
        /// Do not use this to check if a trade is derog or not. Use it when you know that the tradeline has had derog.
        /// </summary>
        abstract public DateTime EndDerogDate
        { 
            get; 
        }
        */


		public bool HasDerog(E_DerogT[] derogTs, bool bIncPaidAccs) 
		{
			if( IsPaid && !bIncPaidAccs )
				return false;

			for( int i = 0; i < derogTs.Length; ++i )
			{
				try
				{
					switch( derogTs[i] )
					{
						case E_DerogT.Repossession:
						{
							if( !IsRepos )
								continue;

							return true;
						}
						case E_DerogT.Foreclosure:
						{
							if( !IsForeclosure )
								continue;
							
							return true;
						}
						case E_DerogT.ChargeOff:
						{
							if( !IsChargeOff )
								continue;
								
							return true;
						}
						case E_DerogT.Collection:
						{
							if( !IsCollection )
								continue;

							return true;
						}

							/* NOT SURE WHAT TO DO WITH BANKRUPCY YET, LEAVING ALONE AND USE THE OLD METHODS INSTEAD
						case E_DerogT.Bankcruptcy:
						{
							if( !IsBankrupcy )
								continue;

							if( !IsPaid )
								return true; // this is the case regardless of value of parameter nMonths

							// We got a paid account here

							if( !bIncPaidAccs )
								continue;

							if( Tools.IsDateWithin( EndDate, nMonths ) )
								return true; 

							break;
						}
						*/
						case E_DerogT.Judgment:
						case E_DerogT.TaxLien:
							// Break and continue does the same thing in this loop
							break; // Lia rec doesn't have this type of derog.
						
						default:
						{
							Tools.LogBug( "Unhandled enum value of E_DerogT" );
							break;
						}
					}
				}
				catch( Exception ex )
				{
					Tools.LogError( ex );
				}
			}
			return false;
		}

		/// <summary>
		/// 6/27/07 db OPM 16685 - This is only used for determining the latest foreclosure date
		internal DateTime GetMajorForeclosureEffectiveWithin(int nMonths, bool bIncPaidAccs)
		{
			if(!IsForeclosure)
				return DateTime.MinValue;
			try
			{
				// 5/7/2007 nw - OPM 12892 - Most recent foreclosure date
                /*
                The foreclosure date should be the most recent of the following:
                If mtg was listed as in foreclosure, use the most recent foreclosure date.
                If mtg was listed as in collection/charge-off, use the most recent collection date
                If mtg was listed as in bk, use the most recent BK date
                If mtg was listed with a 7, 8, or 9 in the payment history use the most recent occurring date in the history

                If the result date is outside of the tradeline's active dates, use the most recent date available in the tradeline
                */
                E_AdverseType typeForeclosureMatch = E_AdverseType.RepossessionOrForeclosure;
				if (LiabilityType.EqualsOneOf(E_DebtRegularT.Mortgage, E_DebtRegularT.Heloc))
					typeForeclosureMatch |= E_AdverseType.Bankruptcy;
				DateTime dtLatestFCFromPaymentHistory = DateOfLastestAdversePmtType( typeForeclosureMatch );

				DateTime fcEffectiveD = dtLatestFCFromPaymentHistory.CompareTo(dtLatestFC) > 0 ? dtLatestFCFromPaymentHistory : dtLatestFC;
				fcEffectiveD = fcEffectiveD.CompareTo(dtLatestBK) > 0 ? fcEffectiveD : dtLatestBK;
				fcEffectiveD = fcEffectiveD.CompareTo(dtLatestCollection) > 0 ? fcEffectiveD : dtLatestCollection;
				if (fcEffectiveD == DateTime.MinValue)
					fcEffectiveD = LastActivityDate;

				DateTime dtAccountEndDate = (LastActivityDate > PaymentPatternStartDate) ? LastActivityDate : PaymentPatternStartDate;
				if (dtAccountEndDate == DateTime.MinValue)
					dtAccountEndDate = AccountReportedDate;

				if (!IsActive)
				{
					if (fcEffectiveD.CompareTo(dtAccountEndDate) > 0)
						fcEffectiveD = dtAccountEndDate;
				}

				if (AccountOpenedDate != DateTime.MinValue && fcEffectiveD.CompareTo(AccountOpenedDate) < 0)
					fcEffectiveD = dtAccountEndDate;

				if( Tools.IsDateWithin( fcEffectiveD, nMonths ) )
				{
					return fcEffectiveD;
				}
			}
			catch
			{}

			DateTime pmtStartD = PaymentPatternStartDate;
			if( pmtStartD != DateTime.MinValue )
			{
				if( Tools.IsDateWithin( pmtStartD, nMonths ) )
				{
					return pmtStartD;
				}
			}

			DateTime accReportD = AccountReportedDate;
			if( accReportD != DateTime.MinValue )
			{
				if( Tools.IsDateWithin( accReportD, nMonths ) )
				{
					return accReportD;
				}
			}

			return DateTime.MinValue;
		}

        /// <summary>
        /// Doesn't cover lates. Use HasBadHistoryEffectiveWithin to cover both late and foreclosure collection etc...
        /// </summary>
        /// <param name="derogTs"></param>
        /// <param name="nMonths"></param>
        /// <param name="bIncPaidAccs"></param>
        /// <returns></returns>
        internal bool HasMajorDerogEffectiveWithin(E_DerogT[] derogTs, int nMonths, bool bIncPaidAccs) 
        {
			if( IsPaid && !bIncPaidAccs )
				return false;

            E_DerogT[] usedTs = null;

            for( int i = 0; i < derogTs.Length; ++i )
            {
                // Translate ANY type to all other types that we handle.
                if( E_DerogT.ANY == derogTs[ i ] )
                    usedTs = new E_DerogT[] { E_DerogT.ChargeOff, E_DerogT.Collection, E_DerogT.Foreclosure, E_DerogT.Judgment, E_DerogT.Repossession };
            }

            if( null == usedTs )
                usedTs = derogTs;
            
            for( int i = 0; i < usedTs.Length; ++i )
            {
                try
                {
                    switch( usedTs[i] )
                    {
                        case E_DerogT.Repossession:
                        {
							if( !IsRepos )
                                continue;

                            // If last activity or reported date is out of range and has been paid then it should NOT be what we are looking for.
                            if( !Tools.IsDateWithin( EndDerogDate, nMonths ) )
                                continue; 

                            // Opm case#3209. Basically, even if it's closed with derog status and the EndDerogDate is within the 
                            // lookup period (tested right above this code), we have to count it.  Pmt pattern/history might not have
                            // the last derog recorded, we just have to look at the status of the tradeline.
							if( IsDerogCurrently )
								return true; 

							if( nMonths > 24 || CreditAdverseRatingList.Count <= 0 )
								return true;

                            try
                            {
								if( IsFirstPmtTypeWithin( E_AdverseType.RepossessionOrForeclosure, nMonths ) )
									return true;
                            }
                            catch{}

                            break;
                        }
                        case E_DerogT.Foreclosure:
                        {
                            if( !IsForeclosure )
                                continue;
                            
                            // Related cases: 3209, 3384, 3410, 3435

                            try
                            {
								// 5/7/2007 nw - OPM 12892 - Most recent foreclosure date
								/*
								The foreclosure date should be the most recent of the following:
								If mtg was listed as in foreclosure, use the most recent foreclosure date.
								If mtg was listed as in collection/charge-off, use the most recent collection date
								If mtg was listed as in bk, use the most recent BK date
								If mtg was listed with a 7, 8, or 9 in the payment history use the most recent occurring date in the history
								
								If the result date is outside of the tradeline's active dates, use the most recent date available in the tradeline
								*/
								E_AdverseType typeForeclosureMatch = E_AdverseType.RepossessionOrForeclosure;
								if (LiabilityType.EqualsOneOf(E_DebtRegularT.Mortgage, E_DebtRegularT.Heloc))
									typeForeclosureMatch |= E_AdverseType.Bankruptcy;
								DateTime dtLatestFCFromPaymentHistory = DateOfLastestAdversePmtType( typeForeclosureMatch );

								DateTime fcEffectiveD = dtLatestFCFromPaymentHistory.CompareTo(dtLatestFC) > 0 ? dtLatestFCFromPaymentHistory : dtLatestFC;
								fcEffectiveD = fcEffectiveD.CompareTo(dtLatestBK) > 0 ? fcEffectiveD : dtLatestBK;
								fcEffectiveD = fcEffectiveD.CompareTo(dtLatestCollection) > 0 ? fcEffectiveD : dtLatestCollection;
								if (fcEffectiveD == DateTime.MinValue)
									fcEffectiveD = LastActivityDate;

								DateTime dtAccountEndDate = (LastActivityDate > PaymentPatternStartDate) ? LastActivityDate : PaymentPatternStartDate;
								if (dtAccountEndDate == DateTime.MinValue)
									dtAccountEndDate = AccountReportedDate;

								if (!IsActive)
								{
									if (fcEffectiveD.CompareTo(dtAccountEndDate) > 0)
										fcEffectiveD = dtAccountEndDate;
								}

								if (AccountOpenedDate != DateTime.MinValue && fcEffectiveD.CompareTo(AccountOpenedDate) < 0)
									fcEffectiveD = dtAccountEndDate;

								if( Tools.IsDateWithin( fcEffectiveD, nMonths ) )
									return true;
                                break; // We can't return false here yet in case this function is called for  multiple derog types
                            }
                            catch
                            {}

                            DateTime pmtStartD = PaymentPatternStartDate;
                            if( pmtStartD != DateTime.MinValue )
                            {
								if( Tools.IsDateWithin( pmtStartD, nMonths ) )
									return true;
                                break; // We can't return false here yet in case this function is called for  multiple derog types
                            }

                            DateTime accReportD = AccountReportedDate;
                            if( accReportD != DateTime.MinValue )
                            {
								if( Tools.IsDateWithin( accReportD, nMonths ) )
									return true;
                                break; // We can't return false here yet in case this function is called for  multiple derog types
                            }

                            // Since none of the 4 important dates are valid, we must let this one slide
                            break; // We can't return false here yet in case this function is called for  multiple derog types
                        }
                        case E_DerogT.ChargeOff:
                        {
                            if( !IsChargeOff )
                                continue;
								
                            // If last activity or reported date is out of range and has been paid then it should NOT be what we are looking for.
                            if( !Tools.IsDateWithin( EndDerogDate, nMonths ) )
                                continue; 

                            // Opm case#3209. Basically, even if it's closed with derog status and the EndDerogDate is within the 
                            // lookup period (tested right above this code), we have to count it.  Pmt pattern/history might not have
                            // the last derog recorded, we just have to look at the status of the tradeline.
							if( IsDerogCurrently )
								return true; 

							if( nMonths > 24 || CreditAdverseRatingList.Count <= 0 )
								return true;

                            try
                            {
								if( IsFirstPmtTypeWithin( E_AdverseType.ChargeOffOrCollection, nMonths ) )
									return true;
                            }
                            catch
                            {}

                            break;
                        }
                        case E_DerogT.Collection:
                        {
                            if( !IsCollection )
                                continue;

                            // If last activity or reported date is out of range and has been paid then it should NOT be what we are looking for.
                            if( !Tools.IsDateWithin( EndDerogDate, nMonths ) )
                                continue; 

                            // Opm case#3209. Basically, even if it's closed with derog status and the EndDerogDate is within the 
                            // lookup period (tested right above this code), we have to count it.  Pmt pattern/history might not have
                            // the last derog recorded, we just have to look at the status of the tradeline.
							if( IsDerogCurrently )
								return true; 

                            // If end date is within requested lookback period but requested lookback period is over 24 months
                            // we can't tell when it happened
							if( nMonths > 24 || CreditAdverseRatingList.Count <= 0 )
								return true;

                            try
                            {	
								if( IsPmtTypeSequenceFirstOccurredWithin( E_AdverseType.ChargeOffOrCollection, nMonths ) )
									return true;
                            }
                            catch
                            {}

                            break;
                        }

                            /* NOT SURE WHAT TO DO WITH BANKRUPCY YET, LEAVING ALONE AND USE THE OLD METHODS INSTEAD
                        case E_DerogT.Bankcruptcy:
                        {
                            if( !IsBankrupcyOrWas )
                                continue;

                            if( !IsPaid )
                                return true; // this is the case regardless of value of parameter nMonths

                            // We got a paid account here

                            if( !bIncPaidAccs )
                                continue;

                            if( Tools.IsDateWithin( EndDate, nMonths ) )
                                return true; 

                            break;
                        }
                        */
						case E_DerogT.Judgment:
						case E_DerogT.TaxLien:
							break; // Lia rec doesn't have this type of derog.
						
                        default:
                        {
                            Tools.LogBug( "Unhandled enum value of E_DerogT" );
                            break;
                        }
                    }
                }
                catch( Exception ex )
                {
                    Tools.LogError( ex );
                }
            } // for( int i = 0; i < usedTs.Length; ++i )
            return false;
        }

		protected void FindLatestPublicRecordDateOfType(string sPublicRecordType)
		{
			switch (sPublicRecordType.ToUpper())
			{
				case "FORECLOSURE":
					foreach (AbstractCreditPublicRecord o in ParentCreditReport.EffectivePublicRecords)
					{
						try
						{
							if( o.Type == E_CreditPublicRecordType.Foreclosure )
							{
								DateTime led = o.LastEffectiveDate;

								if( led.CompareTo( dtLatestFC ) > 0 )
								{
									dtLatestFC = led;
								}
							}
						}
						catch( Exception ex )
						{
							LogCreditWarning( ex );
						}
					}
					break;
				case "BANKRUPTCY":
					foreach (AbstractCreditPublicRecord o in ParentCreditReport.EffectivePublicRecords)
					{
						try
						{
							if( !o.IsBkOfTypeAndCharacter( E_BkType.Any, E_BkChar.Any ) )
								continue;

							DateTime bkDate = DateTime.MinValue;

							if( o.DispositionDate != DateTime.MinValue )
								bkDate = o.DispositionDate;
							// 11/29/2006 nw - External OPM 17907 - using Settled Date instead of Disposition Date
							else if( o.LastEffectiveDate != DateTime.MinValue && o.LastEffectiveDate != DateTime.Today )
								bkDate = o.LastEffectiveDate;
							else if( o.BkFileDate != DateTime.MinValue )
								bkDate = o.BkFileDate;

							if( bkDate != DateTime.MinValue && dtLatestBK.CompareTo( bkDate ) < 0 )
							{
								dtLatestBK = bkDate;
                                latestBKRecord = o; 
							}
						}
						catch( Exception ex )
						{
							LogCreditWarning( ex );
						}
					}
					break;
				case "COLLECTION":
					foreach (AbstractCreditPublicRecord o in ParentCreditReport.EffectivePublicRecords)
					{
						try
						{
							if( o.Type == E_CreditPublicRecordType.Collection )
							{
								DateTime led = o.LastEffectiveDate;

								if( led.CompareTo( dtLatestCollection ) > 0 )
								{
									dtLatestCollection = led;
								}
							}
						}
						catch( Exception ex )
						{
							LogCreditWarning( ex );
						}
					}
					break;
				default:
					throw new CBaseException( ErrorMessages.Generic, string.Format("Unhandled public record type {0} in FindLatestPublicRecordDateOfType(string)", sPublicRecordType) );
			}
		}

        /// <summary>
        /// For lates, this method will count non-progressive & non-rolling lates.  "Bad History" includes lates and other
        /// ones like Foreclosure etc..
        /// </summary>
        /// <param name="nMonths">watch out if you plan to pass in a value greater than 24</param>
        /// <param name="bIncPaidAccs"></param>
        /// <returns></returns>
        internal bool HasBadHistoryEffectiveWithin(int nMonths, bool bIncPaidAccs) 
        {
            if( !bIncPaidAccs && IsPaid )
                return false;

            // If last activity or reported date is out of range and has been paid then it should NOT be what we are looking for.
            if( !Tools.IsDateWithin( EndDerogDate, nMonths ) )
                return false;

            try
            {
                if( IsFirstPmtTypeWithin( E_AdverseType.AllLate, Math.Min( 24, nMonths ) ) )
                    return true;
            }
            catch {}

            try
            {
                if( nMonths > 24 && ( Late30Count > 0 || Late60Count > 0 || Late90Count > 0 || Late120Count > 0 || Late150Count > 0 || Late180Count > 0 ) )
                {
                    LogCreditWarning( string.Format( "Lookback period is {0} which is more than 24 month limit of pmt history. Returning true because there is late record.", nMonths.ToString() ) );

                    return true;
                }
            }
            catch {}

            try
            {
                if( HasMajorDerogEffectiveWithin( 
                    new E_DerogT[] { E_DerogT.ChargeOff, E_DerogT.Collection, E_DerogT.Foreclosure, E_DerogT.Repossession },
                    nMonths, /* bIncPaidAccs */ bIncPaidAccs ) )
                    return true;
            }
            catch {}
           
            if( !IsActive )
            {
                if( HasBeenDerog && Tools.IsDateWithin( EndDerogDate, nMonths ) )
                    return true; // inactive but negative statuses
            }
            else // IsActive
            {  
                // Active and negative status. Used to be just IsLate
                if( IsDerogCurrently ) // and IsActive
                    return true;
            }

            // If a bad status but survive all the tests, should deserve a good status.
            return false;
        }


        /// <summary>
        /// Should be used for collection only. This method can throw exception when 
        /// it's pmt history doesn't exist for the tradeline
        /// </summary>
        /// <param name="pmtType"></param>
        /// <param name="nMonths"></param>
        /// <returns></returns>
        private bool IsPmtTypeSequenceFirstOccurredWithin(E_AdverseType adverseType, int nMonths) 
        {

            int count = CreditAdverseRatingList.Count;

            for (int i = 0; i < count; i++)
            {
                CreditAdverseRating o = (CreditAdverseRating) CreditAdverseRatingList[i];
                
                if (0 != (o.AdverseType & adverseType)) 
                {
                    if (Tools.IsDateWithin(o.Date, nMonths)) 
                    {
                        // Look for the first time this adverse type occur. Must be consecutive.
                        CreditAdverseRating prevAdverse = o;
                        for (int j = i; j < count; j++) 
                        {
                            CreditAdverseRating nextAdverse = (CreditAdverseRating) CreditAdverseRatingList[j];
                            if (prevAdverse.AdverseType == nextAdverse.AdverseType && Tools.IsDateWithin(nextAdverse.Date, 1, prevAdverse.Date)) 
                            {
                                // Prev and next adverse rating are the same type and within one month appart.
                                prevAdverse = nextAdverse;
                                continue;
                            } 
                            else 
                            {
                                return Tools.IsDateWithin(prevAdverse.Date, nMonths);
                            }

                        }
                    } 
                    else 
                    {
                        // This adverse rating aleady fall out of valid range. Return immediately.
                        return false;
                    }
                }
            }
            return false;

        }

        protected int GetInt(XmlElement el, string attrName) 
        {
            try 
            {
                return int.Parse(el.GetAttribute(attrName));
            } 
            catch 
            {
                // 5/17/2005 dd - Only log for non-empty string value.
                if (el.GetAttribute(attrName) != "")
                    LogCreditWarning("AbstractCreditLiability.GetInt: Unable to parse [" + el.GetAttribute(attrName) + "]. Default to 0.");

                return 0;
            }
        }
        protected decimal GetDecimal(XmlElement el, string attrName) 
        {
            try 
            {
                string s = el.GetAttribute(attrName).Replace("$", "");

                // 8/30/2005 dd - Information Network uses "CLOSED" for balance on close trade line. To prevent, warning log to PB, 
                // just return 0.
                if ("CLOSED" == s || "UNKNOWN" == s) 
                    return 0.0M;

                return decimal.Parse(s);
            } 
            catch 
            {
                // 5/17/2005 dd - Only log for non-empty string value.
                if (el.GetAttribute(attrName) != "")
                    LogCreditWarning("AbstractCreditLiability.GetDecimal: Unable to parse [" + el.GetAttribute(attrName) + "]. Default to 0.");

                return 0.0M;
            }
        }

        /// <summary>
        /// Indicates whether the liability is one of the types included in
        /// types parameter.
        /// </summary>
        /// <param name="types">
        /// String of one or more types. 
        /// 'I': InstallmentBase, 'R': RevolvingBase, 'M': Mortgage, 'O': Open, 'U': Other
        /// </param>
        /// <returns>
        /// True if the liability has ANY of the given types. Otherwise, false.
        /// If types is null or empty, returns false.
        /// </returns>
        public bool IsOfType(string types)
        {
            if (string.IsNullOrEmpty(types))
            {
                return false;
            }

            for (int i = 0; i < types.Length; ++i)
            {
                char type = char.ToUpper(types[i]);

                if ((type == 'I' && this.IsTypeInstallmentBase) ||
                    (type == 'R' && this.IsTypeRevolvingBase) ||
                    (type == 'M' && this.IsTypeMortgage) ||
                    (type == 'O' && this.IsTypeOpen) ||
                    (type == 'U' && this.IsTypeOther) ||
                    (type == 'L' && this.IsTypeLease) ||
                    (type == 'H' && this.IsTypeHeloc))
                {
                    return true;
                }
            }

            return false;
        }
    }

}
