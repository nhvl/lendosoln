using System;
using System.Xml;
using System.Net;

using LendersOffice.CreditReport;

namespace LendersOffice.CreditReport.UniversalCreditSystems
{
    public class UniversalCreditHelper 
    {
        public static bool AllowForThisBroker(Guid brokerID) 
        {
            return (brokerID == new Guid("2e596c2c-d74e-4f47-b525-2abb5b7911f7") /* Arlington Broker */ || 
                brokerID == new Guid("a830cc26-03b8-4937-ae44-37d5fdb62336") /* Internal Broker on Production  & Demo & Localhost */ ||
                brokerID == Guid.Empty /* LOAdmin */ );
        }
        public const string Name = "UNIVERSAL CREDIT";
        public static readonly Guid ID = new Guid("{00252960-1C77-4f90-A5F1-8B26491A3C3D}");


    }
    public class Universal_CreditReportRequest : LendersOffice.CreditReport.Mismo2_1.Mismo2_1CreditReportRequest
    {
        // 4/19/2005 dd - After refactor make all these const private.
        private const string TEST_USER_NAME = "PriceMyLoanTest1";
        private const string TEST_PASSWORD = "r7e2jj2g2bsv";
        private const string PRODUCTION_USER_NAME = "PriceMyLoan234ucs";
        private const string PRODUCTION_PASSWORD = "4jahccmgccn7";

        private const string URL = "https://www.mortgagereports.net/partner/postRequest.asp?reply=report&xsl=PRICEMYLOAN";
        public Universal_CreditReportRequest() : base("")
        {
        }
        public override string Url 
        {
            get { return URL; }
        }
        public override ICreditReportResponse CreateResponse(XmlDocument doc) 
        {
            return new Universal_CreditReportResponse(doc);
        }
        public override void SetupAuthentication(HttpWebRequest webRequest) 
        {
            webRequest.KeepAlive = true; // 4/19/2005 dd - For some strange reason if KeepAlive=False, BasicAuthentication will failed.
            webRequest.ContentType = "text/xml"; // 4/19/2005 dd - ISS required content type to be text/xml

            // 4/20/2005 dd - Universal Credit uses Basic Authentication.
            if (this.LoginInfo.UserName.ToLower() == "testpricemyloan") 
            {
                webRequest.Credentials = new NetworkCredential(TEST_USER_NAME, TEST_PASSWORD);
            } 
            else 
            {
                webRequest.Credentials = new NetworkCredential(PRODUCTION_USER_NAME, PRODUCTION_PASSWORD);
            }

        }

    }
}
