using System;
using System.Xml;

using LendersOffice.CreditReport;

namespace LendersOffice.CreditReport.UniversalCreditSystems
{
	public class Universal_CreditReportResponse : LendersOffice.CreditReport.Mismo2_3.Mismo2_3CreditReportResponse
	{
        public Universal_CreditReportResponse(XmlDocument doc) : base(doc)
        {
        }
	}
}
