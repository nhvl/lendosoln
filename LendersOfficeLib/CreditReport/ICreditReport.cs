using System;
using System.Collections;
using System.Text;
using System.Xml;
using DataAccess;

using LendersOffice.Common;
namespace LendersOffice.CreditReport
{
	public class CreditBureauInformation 
	{
		private string m_companyName = "";
		private string m_streetAddress = "";
		private string m_city = "";
		private string m_state = "";
		private string m_zipcode = "";
		private string m_phone = "";

		public string CompanyName 
		{
			get { return m_companyName; }
			set { m_companyName = value; }
		}
		public string StreetAddress 
		{ 
			get { return m_streetAddress; } 
			set { m_streetAddress = value; } 
		} 
		public string City 
		{ 
			get { return m_city; } 
			set { m_city = value; } 
		} 
		public string State 
		{ 
			get { return m_state; } 
			set { m_state = value; } 
		} 
		public string Zipcode 
		{ 
			get { return m_zipcode; } 
			set { m_zipcode = value; } 
		} 
		public string Phone 
		{
			get { return m_phone; }
			set { m_phone = value; }
		}

		public static CreditBureauInformation DefaultExperianInformation 
		{
			get 
			{
				// 2/8/2005 dd - Default address from MCL
				// Experian (XP)
				// P.O. Box 2002
				// Allen, TX 75013
				// 888-397-3742
				CreditBureauInformation info = new CreditBureauInformation();
				info.CompanyName = "EXPERIAN (XP)";
				info.StreetAddress = "P.O. Box 2002";
				info.City = "Allen";
				info.State = "TX";
				info.Zipcode = "75013";
				info.Phone = "(888) 397-3742";
				return info;

			}
		}
		public static CreditBureauInformation DefaultTransUnionInformation 
		{
			get 
			{
				// TRANS UNION (TU)
				// P.O. Box 2000
				// Chester, PA 19022
				// 800-916-8800
				CreditBureauInformation info = new CreditBureauInformation();
				info.CompanyName = "TRANS UNION (TU)";
				info.StreetAddress = "P.O. Box 2000";
				info.City = "Chester";
				info.State = "PA";
				info.Zipcode = "19022";
				info.Phone = "(800) 916-8800";
				return info;

			}
		}
		public static CreditBureauInformation DefaultEquifaxInformation 
		{
			get 
			{
				// EQUIFAX (EF)
				// P.O. Box 740256
				// Atlanta, GA 30374
				// 800-685-1111
				CreditBureauInformation info = new CreditBureauInformation();
				info.CompanyName = "EQUIFAX (EF)";
				info.StreetAddress = "P.O. Box 740256";
				info.City = "Atlanta";
				info.State = "GA";
				info.Zipcode = "30374";
				info.Phone = "(800) 685-1111";
				return info;
			}
		}

	}
	public class CreditScore 
	{
		private int m_eqScore = 0;
		private int m_exScore = 0;
		private int m_tuScore = 0;

        private int m_eqScorePercentile = 0;
        private int m_exScorePercentile = 0;
        private int m_tuScorePercentile = 0;

        private string m_eqModelName = "";
		private string m_exModelName = "";
		private string m_tuModelName = "";

        private ArrayList m_eqFactorList = new ArrayList();
		private ArrayList m_exFactorList = new ArrayList();
		private ArrayList m_tuFactorList = new ArrayList();

		public CreditScore() 
		{
		}
		public CreditScore(int eq, int ex, int tu) 
		{
			m_eqScore = eq;
			m_exScore = ex;
			m_tuScore = tu;
		}
		public int Experian 
		{
			get { return m_exScore; }
			set { m_exScore = value; }
		}
        public int ExperianPercentile
        {
            get { return m_exScorePercentile; }
            set { m_exScorePercentile = value; }
        }
        public string ExperianModelName 
		{
			get { return m_exModelName; }
			set { m_exModelName = value; }
		}
        public string ExperianModelNameOtherDescription { get; set; } = string.Empty;

        /// <summary>
        /// Gets the effective model name for Experian.
        /// </summary>
        /// <value>
        /// The description of the model if the model name is "Other",
        /// the model name otherwise.
        /// </value>
        public string EffectiveExperianModelName
        {
            get
            {
                if (string.IsNullOrWhiteSpace(this.ExperianModelNameOtherDescription))
                {
                    return this.ExperianModelName;
                }

                return this.ExperianModelNameOtherDescription;
            }
        }

		public int Equifax 
		{
			get { return m_eqScore; }
			set { m_eqScore = value; }
		}
        public int EquifaxPercentile
        {
            get { return m_eqScorePercentile; }
            set { m_eqScorePercentile = value; }
        }
        public string EquifaxModelName 
		{
			get { return m_eqModelName; }
			set { m_eqModelName = value; }
        }
        public string EquifaxModelNameOtherDescription { get; set; } = string.Empty;

        /// <summary>
        /// Gets the effective model name for Equifax.
        /// </summary>
        /// <value>
        /// The description of the model if the model name is "Other",
        /// the model name otherwise.
        /// </value>
        public string EffectiveEquifaxModelName
        {
            get
            {
                if (string.IsNullOrWhiteSpace(this.EquifaxModelNameOtherDescription))
                {
                    return this.EquifaxModelName;
                }

                return this.EquifaxModelNameOtherDescription;
            }
        }

        public int TransUnion 
		{
			get { return m_tuScore; }
			set { m_tuScore = value; }
		}
        public int TransUnionPercentile
        {
            get { return m_tuScorePercentile; }
            set { m_tuScorePercentile = value; }
        }
        public string TransUnionModelName 
		{
			get { return m_tuModelName; }
			set { m_tuModelName = value; }
        }
        public string TransUnionModelNameOtherDescription { get; set; } = string.Empty;

        /// <summary>
        /// Gets the effective model name for TransUnion.
        /// </summary>
        /// <value>
        /// The description of the model if the model name is "Other",
        /// the model name otherwise.
        /// </value>
        public string EffectiveTransUnionModelName
        {
            get
            {
                if (string.IsNullOrWhiteSpace(this.TransUnionModelNameOtherDescription))
                {
                    return this.TransUnionModelName;
                }

                return this.TransUnionModelNameOtherDescription;
            }
        }

        /// <summary>
        /// bureau code is "XP", "EQ", "TU"
        /// </summary>
        /// <param name="bureau"></param>
        /// <param name="factor"></param>
        public void AddFactor(string bureau, string factor) 
		{
            if (bureau == "XP")
                m_exFactorList.Add(factor);
            else if (bureau == "EF")
                m_eqFactorList.Add(factor);
            else if (bureau == "TU")
                m_tuFactorList.Add(factor);
            else 
                Tools.LogWarning("CreditScore.AddFactor: Bureau need to be XP, EQ, or TU, but received " + bureau);
		}

		/// <summary>
		/// Return descriptions of all risk factors of bureau (XP, EF, TU)
		/// </summary>
		public string GetFactors(string bureau) 
		{
			StringBuilder sb = new StringBuilder();
			ArrayList list = null;
            if (bureau == "XP")
                list = m_exFactorList;
            else if (bureau == "EF")
                list = m_eqFactorList;
            else if (bureau == "TU")
                list = m_tuFactorList;
            else 
            {
                Tools.LogWarning("CreditScore.GetFactors: Bureau need to be XP, EF, or TU, but received " + bureau);
                return "";
            }

			foreach (string o in list)
				sb.Append(o).Append(Environment.NewLine);

			return sb.ToString();
		}

	}
	/// <summary>
	/// Summary description for ICreditReport.
	/// </summary>
	public interface ICreditReport
	{

		#region BankAegis

		/// <summary>
		/// Text: No derog in past 15 months, including 30 day accts, BK, judgments, chargeoffs, CCC 
		/// </summary>
		/// <param name="nMonths"></param>
		/// <returns></returns>
		int HasDerogCountEffectiveWithin( int nMonths );
		
		/// <summary>
		/// TradesCountNonMajorDerog would include all trades that haven't been majorly derogatory, i.e no BK, chargoffs, collections, CCC, etc.  Would ignore lates.
		/// </summary>
		int TradesCountNonMajorDerog  { get; }

		/// <summary>
		/// Text: No derog in past 15 months BK, judgments, chargeoffs, CCC, but this one is not consider lates.
		/// </summary>
		/// <param name="nMonths"></param>
		/// <returns></returns>
		int HasMajorDerogCountEffectiveWithin( int nMonths );

		/// <summary>
		/// Text: No judgments or tax Liens past 24 months
		/// </summary>
		/// <param name="nMonths"></param>
		/// <returns></returns>
		int BankAegisHasJudgmentOrTaxLienWithin( int nMonths );

		
		#endregion // BankAegis

		#region BankImpact

		/// <summary>
		/// Example: Minimum of 3 trades (All lates based on rolling)
		/// -> Don't know what to do with the one in the parenthesis
		/// <param name="nMonths"></param>
		/// <returns></returns>
		int TradesOpenCountOlderThan( int nMonths );

		/// <summary>
		/// Text: Any medical collections with a cumulative balance over $500 requires payment prior to or at closing
		/// </summary>
		/// <param name="amount"></param>
		/// <returns></returns>
		int TotBalOfMedCollectionsUnpaid { get; }

		// 4/17/2007 nw - OPM 12499 - New medical collection keyword
		int TotUnpaidBalOfMedCollectionsWithin( int nMonths );



		/// <summary>
		/// Text: No Tax Liens in the past 24/12 months
		/// </summary>
		/// <param name="nMonths"></param>
		/// <returns></returns>
		int BankImpactHasTaxLienWithin( int nMonths );

		/// <summary>
		/// 7/6/2006 nw - OPM 6120 - new keyword "HasTaxLienFiledWithin"
		/// Text: No Tax Liens Filed in the past 24/12 months
		/// </summary>
		/// <param name="nMonths"></param>
		/// <returns></returns>
		int BankImpactHasTaxLienFiledWithin( int nMonths );


			

		#endregion // BankImpact

		#region Late Calculations
		int GetInstallmentLates30IntermittentProgressiveRollingCountWithin( int nMonths );
		int GetInstallmentLates30IntermittentProgressiveNonrollingCountWithin( int nMonths );
		int GetRevolvingLates30IntermittentProgressiveRollingCountWithin( int nMonths );
		int GetRevolvingLates30IntermittentProgressiveNonrollingCountWithin( int nMonths );
		int GetMortgageLates30IntermittentProgressiveRollingCountWithin( int nMonths );
		int GetMortgageLates60IntermittentProgressiveRollingCountWithin( int nMonths );
        int GetMortgageLates90IntermittentProgressiveRollingCountWithin( int nMonths );

		int GetRevolvingLates60IntermittentProgressiveNonrollingCountWithin( int nMonths );
		int GetRevolvingLates90IntermittentProgressiveNonrollingCountWithin( int nMonths );
		int GetRevolvingLates120IntermittentProgressiveNonrollingCountWithin( int nMonths );
		int GetRevolvingLates150IntermittentProgressiveNonrollingCountWithin( int nMonths );

		int GetRevolvingLates60IntermittentProgressiveRollingCountWithin( int nMonths );
		int GetRevolvingLates90IntermittentProgressiveRollingCountWithin( int nMonths );
		int GetRevolvingLates120IntermittentProgressiveRollingCountWithin( int nMonths );
		int GetRevolvingLates150IntermittentProgressiveRollingCountWithin( int nMonths );

		int GetInstallmentLates60IntermittentProgressiveNonrollingCountWithin( int nMonths );
		int GetInstallmentLates90IntermittentProgressiveNonrollingCountWithin( int nMonths );
		int GetInstallmentLates120IntermittentProgressiveNonrollingCountWithin( int nMonths );
		int GetInstallmentLates150IntermittentProgressiveNonrollingCountWithin( int nMonths );

		int GetInstallmentLates60IntermittentProgressiveRollingCountWithin( int nMonths );
		int GetInstallmentLates90IntermittentProgressiveRollingCountWithin( int nMonths );
		int GetInstallmentLates120IntermittentProgressiveRollingCountWithin( int nMonths );
		int GetInstallmentLates150IntermittentProgressiveRollingCountWithin( int nMonths );

        int GetLeaseLates30IntermittentProgressiveRollingCountWithin(int nMonths);
        int GetLeaseLates30IntermittentProgressiveNonrollingCountWithin(int nMonths);
        int GetLeaseLates60IntermittentProgressiveRollingCountWithin(int nMonths);
        int GetLeaseLates60IntermittentProgressiveNonrollingCountWithin(int nMonths);
        int GetLeaseLates90IntermittentProgressiveRollingCountWithin(int nMonths);
        int GetLeaseLates90IntermittentProgressiveNonrollingCountWithin(int nMonths);

        int GetHelocLates30IntermittentProgressiveRollingCountWithin(int nMonths);
        int GetHelocLates30IntermittentProgressiveNonrollingCountWithin(int nMonths);
        int GetHelocLates60IntermittentProgressiveRollingCountWithin(int nMonths);
        int GetHelocLates60IntermittentProgressiveNonrollingCountWithin(int nMonths);
        int GetHelocLates90IntermittentProgressiveRollingCountWithin(int nMonths);
        int GetHelocLates90IntermittentProgressiveNonrollingCountWithin(int nMonths);

        #endregion


        int RepresentativeScore { get; }
		/// <summary>
		/// 6/15/2006 - nw - OPM 4822.  Returns 1 if tradeline remark indicates that foreclosure was started.  
		/// Parameter nMonths isn't used yet.
		/// </summary>
		/// <param name="nMonths"></param>
		/// <returns></returns>
		int MightHaveUndated120Within( int nMonths );

        /// <summary>
        /// Returns the number of short sales that occured within n Months.
        /// </summary>
        /// <param name="months">Number of months short sales must be within to be counted.</param>
        /// <returns>The number of short sales that occured within the given number of months.</returns>
        /// <remarks>OPM 24318.</remarks>
        int GetShortSales(int months);

        /// <summary>
        /// Get the total number of late payments of all mortgage account in the past n months.
        /// </summary>
        /// <param name="month"></param>
        /// <returns></returns>
        int GetMortgageLates(int month);

		/// <summary>
		/// Get the total number of 30 days late payments of all mortgage account in the past n months.
		/// </summary>
		/// <param name="month"></param>
		/// <returns></returns>
		int GetMortgage30Lates(int month);

		/// <summary>
		/// Get the total number of 60 days late payments of all mortgage account in the past n months.
		/// </summary>
		/// <param name="month"></param>
		/// <returns></returns>
		int GetMortgage60Lates(int month);

		/// <summary>
		/// Get the total number of 90+ days late payments of all mortgage account in the past n months.
		/// </summary>
		/// <param name="month"></param>
		/// <returns></returns>
		int GetMortgage90PlusLates(int month);

		/// <summary>
		/// Get the total number of 90 days late payments of all mortgage account in the past n months.
		/// </summary>
		/// <param name="month"></param>
		/// <returns></returns>
		int GetMortgage90Lates(int month);

		/// <summary>
		/// Get the total number of 120 days late payments of all mortgage account in the past n months.
		/// </summary>
		/// <param name="month"></param>
		/// <returns></returns>
		int GetMortgage120Lates(int month);

		/// <summary>
		/// Get the total number of 150+ days late payments of all mortgage account in the past n months.
		/// </summary>
		/// <param name="month"></param>
		/// <returns></returns>
		int GetMortgage150PlusLates(int month);

		/// <summary>
		/// Get the total number of 30 days late payments of all installment account in the past n months.
		/// </summary>
		/// <param name="month"></param>
		/// <returns></returns>
		int GetInstallment30Lates(int month);

		/// <summary>
		/// Get the total number of 60 days late payments of all installment account in the past n months.
		/// </summary>
		/// <param name="month"></param>
		/// <returns></returns>
		int GetInstallment60Lates(int month);

		/// <summary>
		/// Get the total number of 90+ days late payments of all installment account in the past n months.
		/// </summary>
		/// <param name="month"></param>
		/// <returns></returns>
		int GetInstallment90PlusLates(int month);

		/// <summary>
		/// Get the total number of 30 days late payments of all revolving account in the past n months.
		/// </summary>
		/// <param name="month"></param>
		/// <returns></returns>
		int GetRevolving30Lates(int month);

		/// <summary>
		/// Get the total number of 60 days late payments of all revolving account in the past n months.
		/// </summary>
		/// <param name="month"></param>
		/// <returns></returns>
		int GetRevolving60Lates(int month);

		/// <summary>
		/// Get the total number of 90+ days late payments of all revolving account in the past n months.
		/// </summary>
		/// <param name="month"></param>
		/// <returns></returns>
		int GetRevolving90PlusLates(int month);

        /// <summary>
        /// Get the total number of late payments for the given debt type, with lateType number of days late, in the past n months.
        /// </summary>
        /// <param name="nMonths">The number of months to scan.</param>
        /// <param name="type">The debt type to check (usually Installment, Revolving, or Mortgage).</param>
        /// <param name="lateType">The type of lates to include.</param>
        /// <param name="rolling">Whether to count rolling late payments as single lates or multiple.</param>
        /// <returns>The number of lates found.</returns>
        int GetNumberOfLates(int nMonths, E_DebtRegularT type, E_AdverseType lateType, bool rolling);

		/// <summary>
		/// Get number of account with foreclosure status '82' in the past n months
		/// </summary>
		/// <param name="month"></param>
		/// <returns></returns>
		int GetForeclosure(int month);

        /// <summary>
        /// 8/23/2006 nw - OPM 7126 - New Foreclosure Keyword, accepts 2 parameters:
        /// a)int  Within:   default is 1000, this is to specify look-back period of when the derog event occurs.
        /// b)bool Count120: default is true, 120 means 120+ late.
        /// Returns the number of mortgage tradelines that satisfy the parameters.
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        int FcCountX(string parameters);


		/// <summary>
		/// 8/25/2006 nw - OPM 7126 - New Foreclosure Keyword HasFcX, accepts 2 parameters:
		/// a)int  Within:   default is 1000, this is to specify look-back period of when the derog event occurs.
		/// b)bool Count120: default is true, 120 means 120+ late.
		/// Returns 1 if true, 0 if false.
		/// </summary>
		/// <param name="parameters"></param>
		/// <returns></returns>
		int HasFcX(string parameters);

		/// <summary>
		/// return 1 if there is a bk (either discharged or filed) in the past n months; else return 0
		/// </summary>
		/// <param name="month"></param>
		/// <returns></returns>
		int GetBankruptcy(int month);

		/// <summary>
		/// Get number of accounts with charge off status 90 in the past n months.
		/// </summary>
		/// <param name="nMonths"></param>
		/// <returns></returns>
		int GetChargeOff(int nMonths );

		/// <summary>
		/// Return total of all charge-offs, collection tradelines.
		/// </summary>
		/// <returns></returns>
		decimal DerogatoryAmountTotal { get; }

		/// <summary>
		/// OPM 2083 - Detect Consumer Credit Counseling Account
		/// </summary>
		bool IsInCreditCounseling { get; }

        int ChargeOffCountX(string parameters);

		#region NEW BK METHODS

		E_BkMultipleT BkMultipleType { get; }
		bool HasMortgageIncludedInBKWithin ( int nMonths );  //av 25329
		int HasDischargedBkWithin( int nMonths );
		int HasDischarged7BkWithin( int nMonths );
		int HasDischarged11BkWithin( int nMonths );
		int HasDischarged12BkWithin( int nMonths );
		int HasDischarged13BkWithin( int nMonths );
		// This will allow us to generate stip for further review
		int HasDischargedUnknownTypeBkWithin( int nMonths ); 

		// 6/30/2006 nw - OPM 5752 - New BK Keywords
		int HasClosedBkWithin( int nMonths );
		int HasClosed7BkWithin( int nMonths );
		int HasClosed11BkWithin( int nMonths );
		int HasClosed12BkWithin( int nMonths );
		int HasClosed13BkWithin( int nMonths );
		int HasClosedUnknownTypeBkWithin( int nMonths );

		int HasDismissedBkWithin( int nMonths );
		int HasDismissed7BkWithin( int nMonths );
		int HasDismissed11BkWithin( int nMonths );
		int HasDismissed12BkWithin( int nMonths );
		int HasDismissed13BkWithin( int nMonths );
		int HasDismissedUnknownTypeBkWithin( int nMonths );

		int HasDischBkWithin( int nMonths );
		int HasDisch7BkWithin( int nMonths );
		int HasDisch11BkWithin( int nMonths );
		int HasDisch12BkWithin( int nMonths );
		int HasDisch13BkWithin( int nMonths );
		int HasDischUnknownTypeBkWithin( int nMonths );
		// End OPM 5752 - New BK Keywords

		int HasBkFiledWithin( int nMonths );
		int Has7BkFiledWithin( int nMonths );
		int Has11BkFiledWithin( int nMonths );
		int Has12BkFiledWithin( int nMonths );
		int Has13BkFiledWithin( int nMonths );
		int HasUnknownTypeBkFiledWithin( int nMonths );

		bool HasUnpaidAndNondischargedAnyBk { get; }
		bool HasUnpaidAndNondischarged7Bk { get; }
		bool HasUnpaidAndNondischarged11Bk { get; }
		bool HasUnpaidAndNondischarged12Bk { get; }
		bool HasUnpaidAndNondischarged13Bk { get; }
		bool HasUnpaidAndNondischargedUnknownTypeBk { get; }

		// These methods are used for custom reporting purpose only
		CDateTime DischargedDOfLastBkOfType( E_BkType type );
		CDateTime FileDOfLastBkOfType( E_BkType type );
		
		#endregion // NEW BK METHODS



		int CreditReportAge { get; }
		DateTime CreditReportFirstIssuedDate { get; }
        DateTime CreditReportLastUpdatedDate { get; }
        CreditScore BorrowerScore { get; }
		CreditScore CoborrowerScore { get; }

		BorrowerInfo BorrowerInfo { get; }
		BorrowerInfo CoborrowerInfo { get; }

		CreditBureauInformation ExperianContactInformation { get; }
		CreditBureauInformation TransUnionContactInformation { get; }
		CreditBureauInformation EquifaxContactInformation { get; }

		//6/28/07 db OPM 16602 - New MaxHighCreditAmountX keyword
		decimal MaxHighCreditAmountX(string parameters);

		/// <summary>
		/// Return list of liabilies for this report.
		/// </summary>
		ArrayList AllLiabilities { get; }

        ArrayList LiabilitiesExcludeFromUnderwriting { get; }

		ArrayList AllPublicRecords { get; }

		// 7/22/05 tn - added
		int TradeCount { get; }

		int TradeCountX ( string parameters );
		int MajorDerogCountX( string parameters );

		int HasContiguousMortgageHistory ( string parameters );
	
		// 11/2/2004 dd - Added
		// 8/23/07 db - Changed to take CAppData instead of specific liability lists
		void SetLoanData(CAppBase dataApp);

		// 3/11/2005 dd - Added
		void Parse();

		bool IsDebug 
		{ 
			get;
			set;
		}

        E_aBorrowerCreditModeT aBorrowerCreditModeT
        {
            get;
            set;
        }
		ArrayList GetTagItems(string methodName);

		string CreditRatingCodeType { get; }

		#region SHARING METHODS OF NEW BANKS
		
        /// <summary>
        /// Example: Minimum of 3 trades (All lates based on rolling)
        /// -> Don't know what to do with the one in the parenthesis
        /// <param name="nMonths"></param>
        /// <returns>Number of trades open nMonths ago, regardless of current status</returns>
        int GetTradesCountOlderThan( int nMonths );

        int InstallTradeCountOlderThan( int nMonths );
        int RevolveTradeCountOlderThan( int nMonths );

		bool Is1stTimeHomeBuyer { get; }
        bool Is1stTimeHomeBuyerWithin( int nMonths );

		/// <summary>
		/// Used by banks: GMAC
		/// </summary>
		bool HasTaxOpenTaxLien { get; }
		
		int GetHighestUnpaidBalOfCollectionWithin( int nMonths );
		int GetHighestUnpaidBalOfJudgmentWithin( int nMonhts );
		int GetHighestUnpaidBalOfChargeOffWithin( int nMonths );
		int GetHighestUnpaidBalOfTaxLienWithin( int nMonths );
		int HighestUnpaidBalOfChargeOffOrCollectionWithin( int nMonths );
		int GetTotUnpaidBalOfCollectionWithin( int nMonths );
		int GetTotUnpaidBalOfJudgmentWithin( int nMonths );
		int GetTotUnpaidBalOfChargeOffWithin( int nMonths );
		int GetTotUnpaidBalOfTaxLienWithin( int nMonths );
        int TotUnpaidBalOfAllDerogWithin( int nMonths );
		int TotUnpaidBalOfChargeOffAndCollectionWithin( int nMonths );
		int GetTradesCountActiveWithin( int nMonths );
		int GetTradesCountWithHighCreditOf( int amount );
		int GetTradesCountActiveWithHighCreditOf( int amount );

		int ForeclosureUnpaidCount{ get; }
		CDateTime DischargedDOfLastForeclosure	{ get; }
		CDateTime FileDOfLastForeclosure{ get; }

		int GetHighestUnpaidBalOfCollectionOlderThan( int nMonths );
		int GetHighestUnpaidBalOfJudgmentOlderThan( int nMonths );
		int GetHighestUnpaidBalOfChargeOffOlderThan( int nMonths );
		int HighestUnpaidBalOfChargeOffOrCollectionOlderThan( int nMonths );
		int GetHighestUnpaidPastDueWithin( int nMonths );
		int GetHighestUnpaidPastDueOlderThan( int nMonths );
		int GetHighestUnpaidBalOfMedicalCollectionWithin( int nMonths );
		#endregion // 
	}

}