﻿namespace LendersOffice.CreditReport
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using Common;

    /// <summary>
    /// Defines a record loaded from the <c>CREDIT_REPORT_ACCOUNT_PROXY</c> table.
    /// </summary>
    /// <remarks>
    /// 2018-08 tj - I'm creating this class since there were many ad hoc stored procedure calls all over the UI.
    /// We're trying to make the encryption handling consistent, and creating this class is the best way to ensure
    /// consistency.  I'm not completely certain of all the business requirements involved, so I'm trying my best
    /// to keep the behavior little more than a simple POCO for holding the data from the table.
    /// </remarks>
    public class CreditReportAccountProxy
    {
        /// <summary>
        /// The encryption key identifier the database record uses to encrypt the passwords.
        /// </summary>
        private LqbGrammar.DataTypes.EncryptionKeyIdentifier encryptionKeyId;

        /// <summary>
        /// The lazy backing field for <see cref="CraPassword"/>.
        /// </summary>
        private Lazy<string> lazyCraPassword;

        /// <summary>
        /// The lazy backing field for <see cref="CreditMornetPlusPassword"/>.
        /// </summary>
        private Lazy<string> lazyCreditMornetPlusPassword;

        /// <summary>
        /// Initializes a new instance of the <see cref="CreditReportAccountProxy"/> class.<para/>
        /// Use this constructor for new instances that don't exist in the database yet.
        /// </summary>
        /// <param name="brokerId">The identifier of the lender.</param>
        public CreditReportAccountProxy(Guid brokerId)
        {
            if (brokerId == Guid.Empty)
            {
                throw DataAccess.CBaseException.GenericException("Invalid BrokerId");
            }

            this.BrokerId = brokerId;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CreditReportAccountProxy"/> class.<para/>
        /// Use this constructor for records loaded from the database.
        /// </summary>
        /// <param name="brokerId">The identifier of the lender.</param>
        /// <param name="record">The database record.</param>
        private CreditReportAccountProxy(Guid brokerId, IDataRecord record)
            : this(brokerId)
        {
            this.CrAccProxyId = (Guid)record["CrAccProxyId"];
            this.CrAccProxyDesc = (string)record["CrAccProxyDesc"];
            this.CraUserName = (string)record["CraUserName"];
            this.CraAccId = (string)record["CraAccId"];
            this.CraId = (Guid)record["CraId"];
            this.IsExperianPulled = (bool)record["IsExperianPulled"];
            this.IsEquifaxPulled = (bool)record["IsEquifaxPulled"];
            this.IsTransUnionPulled = (bool)record["IsTransUnionPulled"];
            this.IsFannieMaePulled = (bool)record["IsFannieMaePulled"];
            this.CreditMornetPlusUserId = (string)record["CreditMornetPlusUserId"];
            this.AllowPmlUserToViewReports = (bool)record["AllowPmlUserToViewReports"];
            var maybeEncryptionKeyId = LqbGrammar.DataTypes.EncryptionKeyIdentifier.Create((Guid)record["EncryptionKeyId"]);
            if (maybeEncryptionKeyId.HasValue)
            {
                this.encryptionKeyId = maybeEncryptionKeyId.Value;
                byte[] encryptedCraPassword = (byte[])record["EncryptedCraPassword"];
                this.lazyCraPassword = new Lazy<string>(() => EncryptionHelper.DecryptString(maybeEncryptionKeyId.Value, encryptedCraPassword));
                byte[] encryptedCreditMornetPlusPassword = (byte[])record["EncryptedCreditMornetPlusPassword"];
                this.lazyCreditMornetPlusPassword = new Lazy<string>(() => EncryptionHelper.DecryptString(maybeEncryptionKeyId.Value, encryptedCreditMornetPlusPassword));
            }
            else
            {
                string craPassword = (string)record["CraPassword"];
                this.lazyCraPassword = new Lazy<string>(() => craPassword);
                string creditMornetPlusPassword = (string)record["CreditMornetPlusPassword"];
                this.lazyCreditMornetPlusPassword = new Lazy<string>(() => creditMornetPlusPassword);
            }
        }

        /// <summary>
        /// Gets the identifier of the lender.
        /// </summary>
        public Guid BrokerId { get; }

        /// <summary>
        /// Gets the identifier of the credit report account proxy.<para/>
        /// This value is <see cref="Guid.Empty"/> for in memory records, but should always have a value for records from the DB.
        /// </summary>
        public Guid CrAccProxyId { get; }

        /// <summary>
        /// Gets or sets the description of the credit report account proxy.
        /// </summary>
        public string CrAccProxyDesc { get; set; }

        /// <summary>
        /// Gets or sets the user name for the CRA.
        /// </summary>
        public string CraUserName { get; set; }

        /// <summary>
        /// Gets or sets the password for the CRA.
        /// </summary>
        public string CraPassword
        {
            get { return this.lazyCraPassword.Value; }
            set { this.lazyCraPassword = new Lazy<string>(() => value); }
        }

        /// <summary>
        /// Gets or sets the account id for the CRA.
        /// </summary>
        public string CraAccId { get; set; }

        /// <summary>
        /// Gets or sets the identifier of the CRA.
        /// </summary>
        public Guid CraId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether Experian should be pulled.
        /// </summary>
        public bool IsExperianPulled { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether Equifax should be pulled.
        /// </summary>
        public bool IsEquifaxPulled { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether TransUnion should be pulled.
        /// </summary>
        public bool IsTransUnionPulled { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether Fannie Mae should be pulled.
        /// </summary>
        public bool IsFannieMaePulled { get; set; }

        /// <summary>
        /// Gets or sets the user ID for Credit Mornet Plus.
        /// </summary>
        public string CreditMornetPlusUserId { get; set; }

        /// <summary>
        /// Gets or sets the password for Credit Mornet Plus.
        /// </summary>
        public string CreditMornetPlusPassword
        {
            get { return this.lazyCreditMornetPlusPassword.Value; }
            set { this.lazyCreditMornetPlusPassword = new Lazy<string>(() => value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether a PML user is allowed to view reports.
        /// </summary>
        public bool AllowPmlUserToViewReports { get; set; }

        /// <summary>
        /// Defines a set of static helpers for interacting with DB records for the <see cref="CreditReportAccountProxy"/> class.
        /// </summary>
        /// <remarks>
        /// I've separated these methods into their own static class, but it's common for us to just intersperse these sorts of CRUD methods
        /// willy-nilly in the lib class they load.  I'm generally against nested classes, but using it like this to instead simply group a
        /// set of static methods seems like the lightest case.
        /// </remarks>
        public static class Helper
        {
            /// <summary>
            /// Deletes a credit report account proxy from the database.
            /// </summary>
            /// <param name="brokerId">The identifier of the lender.</param>
            /// <param name="proxyId">The identifier of the credit report account proxy (aka <see cref="CrAccProxyId"/>).</param>
            public static void DeleteCreditProxy(Guid brokerId, Guid proxyId)
            {
                SqlParameter[] parameters = new[]
                {
                    new SqlParameter("@BrokerID", brokerId),
                    new SqlParameter("@CrAccProxyId", proxyId)
                };
                DataAccess.StoredProcedureHelper.ExecuteNonQuery(brokerId, "DeleteCreditProxy", 3, parameters);
            }

            /// <summary>
            /// Retrieves the first credit report account proxy by the broker id.
            /// </summary>
            /// <param name="brokerId">The identifier of the lender.</param>
            /// <returns>The first found credit report account proxy, or null if none are in the database.</returns>
            /// <remarks>
            /// 2018-08 tj - Honestly, I don't understand why this stored procedure exists, and why it loads all the records but only uses the first one.  However,
            /// it is used in one particular way by the lender edit page, and I'm struggling to find documentation of how this feature should work to understand it better.
            /// </remarks>
            public static CreditReportAccountProxy RetrieveCreditProxyByBrokerID(Guid brokerId)
            {
                SqlParameter[] parameters = new[]
                {
                    new SqlParameter("@BrokerID", brokerId)
                };
                using (IDataReader reader = DataAccess.StoredProcedureHelper.ExecuteReader(brokerId, "RetrieveCreditProxyByBrokerID", parameters))
                {
                    if (reader.Read())
                    {
                        return new CreditReportAccountProxy(brokerId, reader);
                    }
                }

                return null;
            }

            /// <summary>
            /// Gets an ordered list of credit report account proxies for a given lender.
            /// </summary>
            /// <param name="brokerId">The identifier of the lender.</param>
            /// <returns>The list of all credit account proxies found.</returns>
            public static IEnumerable<CreditReportAccountProxy> ListCreditProxyByBrokerID(Guid brokerId)
            {
                var list = new List<CreditReportAccountProxy>();
                SqlParameter[] parameters = new[]
                {
                    new SqlParameter("@BrokerID", brokerId),
                };
                using (IDataReader reader = DataAccess.StoredProcedureHelper.ExecuteReader(brokerId, "ListCreditProxyByBrokerID", parameters))
                {
                    while (reader.Read())
                    {
                        list.Add(new CreditReportAccountProxy(brokerId, reader));
                    }
                }

                return list;
            }

            /// <summary>
            /// Retrieves the credit report account proxy for the specified identifier.
            /// </summary>
            /// <param name="brokerId">The identifier of the lender.</param>
            /// <param name="proxyId">The identifier of the credit report account proxy (aka <see cref="CrAccProxyId"/>).</param>
            /// <returns>The credit report account proxy or null if not found.</returns>
            public static CreditReportAccountProxy RetrieveCreditProxyByProxyID(Guid brokerId, Guid proxyId)
            {
                SqlParameter[] parameters = new[]
                {
                    new SqlParameter("@CrAccProxyId", proxyId),
                };
                using (IDataReader reader = DataAccess.StoredProcedureHelper.ExecuteReader(brokerId, "RetrieveCreditProxyByProxyID", parameters))
                {
                    if (reader.Read())
                    {
                        return new CreditReportAccountProxy(brokerId, reader);
                    }
                }

                return null;
            }

            /// <summary>
            /// Updates (or creates) the DB record for the specified credit report account proxy.
            /// </summary>
            /// <param name="proxy">The proxy to update or create in the database.</param>
            public static void UpdateCreditProxyByBrokerID(CreditReportAccountProxy proxy)
            {
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@BrokerID", proxy.BrokerId));
                if (proxy.CrAccProxyId == Guid.Empty)
                {
                    parameters.Add(new SqlParameter("@CrAccProxyId", DBNull.Value));

                    proxy.encryptionKeyId = EncryptionHelper.GenerateNewKey();
                    parameters.Add(new SqlParameter("@EncryptionKeyId", proxy.encryptionKeyId.Value));
                }
                else
                {
                    parameters.Add(new SqlParameter("@CrAccProxyId", proxy.CrAccProxyId));
                }

                byte[] encryptedCraPassword = null;
                byte[] encryptedCreditMornetPlusPassword = null;
                if (proxy.encryptionKeyId != default(LqbGrammar.DataTypes.EncryptionKeyIdentifier))
                {
                    encryptedCraPassword = EncryptionHelper.EncryptString(proxy.encryptionKeyId, proxy.CraPassword);
                    encryptedCreditMornetPlusPassword = EncryptionHelper.EncryptString(proxy.encryptionKeyId, proxy.CreditMornetPlusPassword);
                }

                parameters.Add(new SqlParameter("@CraUserName", proxy.CraUserName));
                parameters.Add(new SqlParameter("@CraPassword", proxy.CraPassword));
                parameters.Add(new SqlParameter("@EncryptedCraPassword", encryptedCraPassword ?? new byte[0]));
                parameters.Add(new SqlParameter("@CrAccProxyDesc", proxy.CrAccProxyDesc));
                parameters.Add(new SqlParameter("@CreditMornetPlusUserId", proxy.CreditMornetPlusUserId));
                parameters.Add(new SqlParameter("@CreditMornetPlusPassword", proxy.CreditMornetPlusPassword));
                parameters.Add(new SqlParameter("@EncryptedCreditMornetPlusPassword", encryptedCreditMornetPlusPassword ?? new byte[0]));

                parameters.Add(new SqlParameter("@CraId", proxy.CraId));
                parameters.Add(new SqlParameter("@CraAccId", proxy.CraAccId));
                parameters.Add(new SqlParameter("@IsExperianPulled", proxy.IsExperianPulled));
                parameters.Add(new SqlParameter("@IsEquifaxPulled", proxy.IsEquifaxPulled));
                parameters.Add(new SqlParameter("@IsTransUnionPulled", proxy.IsTransUnionPulled));
                parameters.Add(new SqlParameter("@IsFannieMaePulled", proxy.IsFannieMaePulled));
                parameters.Add(new SqlParameter("@AllowPmlUserToViewReports", proxy.AllowPmlUserToViewReports));

                DataAccess.StoredProcedureHelper.ExecuteNonQuery(proxy.BrokerId, "UpdateCreditProxyByBrokerID", 3, parameters);
            }
        }
    }
}
