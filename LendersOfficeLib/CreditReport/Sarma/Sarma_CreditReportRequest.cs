using System;

using System.Xml;
using System.Net;

using LendersOffice.CreditReport.Mismo;

namespace LendersOffice.CreditReport.Sarma
{
	public class Sarma_CreditReportRequest : LendersOffice.CreditReport.Mismo2_1.Mismo2_1CreditReportRequest
	{
        private const string BETA_URL = "https://beta.xpertonline.net/interface/LendersOfficeMISMO.asp";
        private const string URL = "https://www.xpertonline.net/interface/LendersOfficeMISMO.asp";
        private const string BETA_PROVIDER_ID = "8888";
        private const string SARMA_PROVIDER_ID = "109";

		public Sarma_CreditReportRequest(bool isBeta) : base("")
		{
            if (isBeta) 
            {
                this.ReceivingPartyName = BETA_PROVIDER_ID;
            } 
            else 
            {
                this.ReceivingPartyName = SARMA_PROVIDER_ID;
            }
		}
        public override string Url 
        {
            get 
            {
                if (LoginInfo.AccountIdentifier == "ILS" && LoginInfo.UserName == "test") 
                {
                    return BETA_URL;
                } 
                else 
                {
                    return URL;
                }
            }
        }

        public override ICreditReportResponse CreateResponse(XmlDocument doc) 
        {
            return new Sarma_CreditReportResponse(doc);
        }
        public override void SetupAuthentication(HttpWebRequest webRequest) 
        {

            webRequest.ContentType = "application/xml";

        }
	}
}
