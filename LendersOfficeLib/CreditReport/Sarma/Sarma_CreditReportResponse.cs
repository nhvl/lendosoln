using System;
using System.Xml;

using LendersOffice.CreditReport;

namespace LendersOffice.CreditReport.Sarma
{
	public class Sarma_CreditReportResponse : LendersOffice.CreditReport.Mismo2_1.Mismo2_1CreditReportResponse
	{
		public Sarma_CreditReportResponse(XmlDocument doc) : base(doc)
		{
		}

		protected override bool HasErrorInResponse(XmlDocument doc)
		{
			bool hasError = false;
			string errorMessage = "";

			// 1/2/2007 nw - OPM 9213 - SARMA response error detection needs to work a little differently
			XmlElement oCreditResponseElement = (XmlElement) doc.SelectSingleNode("//RESPONSE_GROUP/RESPONSE/RESPONSE_DATA/CREDIT_RESPONSE");
			if (null != oCreditResponseElement && oCreditResponseElement.GetAttribute("CreditReportType").ToUpper() != "ERROR")
				return false;

			XmlElement errorElement = (XmlElement) doc.SelectSingleNode("//RESPONSE_GROUP/RESPONSE/RESPONSE_DATA/CREDIT_RESPONSE/CREDIT_ERROR_MESSAGE");
			if (null != errorElement) 
			{
				// 12/29/2003 dd - I currently don't have a list of what error code mean.
				// After talking with Alan, there is no standard way in MISMO to determine if error related to login failure.
				SetStatusCode( DetermineErrorStatus(errorElement.GetAttribute("_Code")) );

				hasError = true;
				XmlElement errorTextElement = (XmlElement) errorElement.SelectSingleNode("_Text");
				if (null != errorTextElement) 
				{
					errorMessage = errorTextElement.InnerText;
				} 
				else 
				{
					// 12/29/2003 dd - Generic error message since I can't determine from credit response.
					errorMessage = "Error while ordering report.";
				}

				string msg = GetCustomErrorMessage();
				if (msg == "") 
				{
					// If there is no custom agency information then return generic message.
					msg = "If you are unable to login for credit after several tries, please contact your Credit Reporting Agency for assistance.";
				}
				errorMessage += "<br>" + msg;
			} 

			SetErrorMessage(errorMessage);
			return hasError;
		}
	}
}
