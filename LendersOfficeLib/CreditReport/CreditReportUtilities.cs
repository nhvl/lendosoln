using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Xml;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.AntiXss;
using LendersOffice.Audit;
using LendersOffice.Common;
using LendersOffice.Constants;
using LendersOffice.CreditReport.FannieMae;
using LendersOffice.CreditReport.Mcl;
using LendersOffice.CreditReport.Mismo;
using LendersOffice.Security;

namespace LendersOffice.CreditReport
{
	public static class CreditReportUtilities
	{
        public static ICreditReportResponse TransformRawResponseXml(ICreditReportRequest request, string rawResponseXml)
        {
            ICreditReportResponse response = null;
            if (request is LendersOffice.CreditReport.FannieMae.FannieMae_CreditReportRequest)
            {
                response = ((LendersOffice.CreditReport.FannieMae.FannieMae_CreditReportRequest)request).CreateResponse(rawResponseXml);
            }
            else
            {
                XmlDocument doc = Tools.CreateXmlDoc(rawResponseXml);
                response = request.CreateResponse(doc);
            }

            return response;
        }

        public static ICreditReportRequest TransformCreditRequestData(CreditRequestData creditRequestData, AbstractUserPrincipal principal)
        {
            ICreditReportRequest creditRequest = null;

            if (creditRequestData.CreditProtocol == CreditReportProtocol.Mcl)
            {
                MclCreditReportRequest mclRequest = new MclCreditReportRequest();
                SetMclRequest(mclRequest, creditRequestData);
                creditRequest = mclRequest;
            }
            else if (creditRequestData.CreditProtocol == CreditReportProtocol.LenderMappingCRA)
            {
                CRA cra = MasterCRAList.FindById(creditRequestData.ActualCraID, false);

                creditRequestData.CreditProtocol = cra.Protocol;
                creditRequestData.Url = cra.Url;
                creditRequestData.ProviderId = cra.ProviderId;
                if (creditRequestData.CreditProtocol == CreditReportProtocol.FannieMae)
                {
                    creditRequestData.LoginInfo.FannieMaeCreditProvider = cra.Url;
                }

                return TransformCreditRequestData(creditRequestData, principal); //this is calling self with updated cra info in credit Request data
            }
            else if (creditRequestData.CreditProtocol == CreditReportProtocol.FannieMae)
            {
                FannieMae_CreditReportRequest fannieMaeRequest = new FannieMae.FannieMae_CreditReportRequest();
                SetFannieMaeRequest(fannieMaeRequest, creditRequestData);

                creditRequest = fannieMaeRequest;
            }
            else
            {
                MismoCreditReportRequest mismoRequest = null;
                switch (creditRequestData.CreditProtocol)
                {
                    case CreditReportProtocol.MISMO_2_1:
                        mismoRequest = new Mismo2_1.Mismo2_1CreditReportRequest(creditRequestData.Url);
                        break;
                    case CreditReportProtocol.MISMO_2_3:
                        mismoRequest = new Mismo2_3.Mismo2_3CreditReportRequest(creditRequestData.Url);
                        break;
                    case CreditReportProtocol.Credco:
                        mismoRequest = new Credco.Credco_CreditReportRequest(creditRequestData.Url);

                        if (creditRequestData.UseCreditCardPayment)
                        {
                            mismoRequest.UseCreditCardPayment = true;
                            mismoRequest.ServicePayment_AccountHolderName = creditRequestData.BillingFirstName + " " + creditRequestData.BillingLastName;
                            mismoRequest.ServicePayment_AccountHolderStreetAddress = creditRequestData.BillingStreetAddress;
                            mismoRequest.ServicePayment_AccountHolderCity = creditRequestData.BillingCity;
                            mismoRequest.ServicePayment_AccountHolderState = creditRequestData.BillingState;
                            mismoRequest.ServicePayment_AccountHolderPostalCode = creditRequestData.BillingZipcode;
                            mismoRequest.ServicePayment_AccountIdentifier = creditRequestData.BillingCardNumber;
                            mismoRequest.ServicePayment_AccountExpirationDate = MismoUtilities.FormatBillingExpirationDate(creditRequestData.BillingExpirationYear, creditRequestData.BillingExpirationMonth);
                            mismoRequest.ServicePayment_SecondaryAccountIdentifier = creditRequestData.BillingCVV;
                            CreditCardSystemLib.CardNumber ccNumber = new CreditCardSystemLib.CardNumber(creditRequestData.BillingCardNumber);
                            string sCardType = CreditReportUtilities.GetMismoCCTypeFromCCSystemCardType(ccNumber.CardType);
                            mismoRequest.ServicePayment_MethodType = sCardType;
                        }

                        break;
                    case CreditReportProtocol.KrollFactualData:
                        mismoRequest = new KrollFactualData.KrollFactualData26_CreditReportRequest();
                        mismoRequest.LenderCaseIdentifier = creditRequestData.ReferenceNumber; // Loan Number.
                        creditRequestData.RequestingPartyRequestedByName = creditRequestData.LoginInfo.UserName;

                        if (creditRequestData.UseCreditCardPayment)
                        {
                            mismoRequest.UseCreditCardPayment = true;
                            mismoRequest.ServicePayment_AccountHolderName = creditRequestData.BillingFirstName + " " + creditRequestData.BillingLastName;
                            mismoRequest.ServicePayment_AccountHolderStreetAddress = creditRequestData.BillingStreetAddress;
                            mismoRequest.ServicePayment_AccountHolderCity = creditRequestData.BillingCity;
                            mismoRequest.ServicePayment_AccountHolderState = creditRequestData.BillingState;
                            mismoRequest.ServicePayment_AccountHolderPostalCode = creditRequestData.BillingZipcode;
                            mismoRequest.ServicePayment_AccountIdentifier = creditRequestData.BillingCardNumber;
                            mismoRequest.ServicePayment_AccountExpirationDate = MismoUtilities.FormatBillingExpirationDate(creditRequestData.BillingExpirationYear, creditRequestData.BillingExpirationMonth);
                            mismoRequest.ServicePayment_SecondaryAccountIdentifier = creditRequestData.BillingCVV;
                            CreditCardSystemLib.CardNumber ccNumber = new CreditCardSystemLib.CardNumber(creditRequestData.BillingCardNumber);
                            string sCardType = CreditReportUtilities.GetMismoCCTypeFromCCSystemCardType(ccNumber.CardType);
                            mismoRequest.ServicePayment_MethodType = sCardType;
                        }

                        break;
                    case CreditReportProtocol.Landsafe:
                        mismoRequest = new Landsafe.Landsafe_CreditReportRequest();
                        break;
                    case CreditReportProtocol.StandFacts:
                        mismoRequest = new StandFacts.StandFacts_CreditReportRequest();
                        break;
                    case CreditReportProtocol.UniversalCredit:
                        mismoRequest = new UniversalCreditSystems.Universal_CreditReportRequest();
                        break;
                    case CreditReportProtocol.Fiserv:
                        mismoRequest = new Fiserv.Fiserv_CreditReportRequest();
                        break;
                    case CreditReportProtocol.CBC:
                        mismoRequest = new CBC.CBC_CreditReportRequest();
                        mismoRequest.LenderCaseIdentifier = creditRequestData.ReferenceNumber;
                        if (!creditRequestData.IsRequestLqiReport && !string.IsNullOrEmpty(creditRequestData.ReportID))   //opm 39968 av 9 10 
                        {
                            creditRequestData.IncludeEquifax = true;
                            creditRequestData.IncludeExperian = true;
                            creditRequestData.IncludeTransUnion = true;
                        }
                        CBC.CBC_CreditReportRequest request = ((CBC.CBC_CreditReportRequest)mismoRequest);
                        if (creditRequestData.UseCreditCardPayment)
                        {
                            request.UseCreditCardPayment = true;
                            request.BillingFirstName = creditRequestData.BillingFirstName;
                            request.BillingSurName = creditRequestData.BillingLastName;
                            request.BillingStreet = creditRequestData.BillingStreetAddress;
                            request.BillingCity = creditRequestData.BillingCity;
                            request.BillingState = creditRequestData.BillingState;
                            request.BillingZip = creditRequestData.BillingZipcode;
                            request.CardNumber = creditRequestData.BillingCardNumber;
                            request.ExpirationMonth = creditRequestData.BillingExpirationMonth;
                            request.ExpirationYear = creditRequestData.BillingExpirationYear;
                            request.BillingCvv = creditRequestData.BillingCVV;
                        }
                        if (creditRequestData.IsMortgageOnly)
                        {
                            request.SetIsMortgageOnlyReport(true);
                        }
                        break;
                    case CreditReportProtocol.CSC:
                        mismoRequest = new CSC.CSC_CreditReportRequest();
                        break;
                    case CreditReportProtocol.CSD:
                        mismoRequest = new CSD.CSD_CreditReportRequest();
                        // 1/8/2010 dd - For CSD When order a joint credit report, it require address for both borrower and coborrower.
                        if (creditRequestData.Coborrower.IsValid)
                        {
                            creditRequestData.Coborrower.CurrentAddress = creditRequestData.Borrower.CurrentAddress;
                            creditRequestData.Coborrower.PreviousAddress = creditRequestData.Borrower.PreviousAddress;
                        }

                        break; //OPM 18327
                    case CreditReportProtocol.SharperLending:
                        string url = "";
                        if (creditRequestData.LoginInfo.AccountIdentifier == "ILS")
                            url = "8888";
                        else
                            url = creditRequestData.Url;
                        mismoRequest = new SharperLending.SharperLending_CreditReportRequest(url);
                        mismoRequest.LenderCaseIdentifier = creditRequestData.ReferenceNumber;
                        if (creditRequestData.UseCreditCardPayment)
                        {
                            mismoRequest.UseCreditCardPayment = true;
                            mismoRequest.ServicePayment_AccountHolderName = creditRequestData.BillingFirstName + " " + creditRequestData.BillingLastName;
                            mismoRequest.ServicePayment_AccountHolderStreetAddress = creditRequestData.BillingStreetAddress;
                            mismoRequest.ServicePayment_AccountHolderCity = creditRequestData.BillingCity;
                            mismoRequest.ServicePayment_AccountHolderState = creditRequestData.BillingState;
                            mismoRequest.ServicePayment_AccountHolderPostalCode = creditRequestData.BillingZipcode;
                            mismoRequest.ServicePayment_AccountIdentifier = creditRequestData.BillingCardNumber;
                            mismoRequest.ServicePayment_AccountExpirationDate = MismoUtilities.FormatBillingExpirationDate(creditRequestData.BillingExpirationYear, creditRequestData.BillingExpirationMonth);
                            mismoRequest.ServicePayment_SecondaryAccountIdentifier = creditRequestData.BillingCVV;
                            CreditCardSystemLib.CardNumber ccNumber = new CreditCardSystemLib.CardNumber(creditRequestData.BillingCardNumber);
                            string sCardType = CreditReportUtilities.GetMismoCCTypeFromCCSystemCardType(ccNumber.CardType);
                            mismoRequest.ServicePayment_MethodType = sCardType;
                        }
                        break;
                    case CreditReportProtocol.InfoNetwork:
                        mismoRequest = new InfoNetwork.InfoNetwork_CreditReportRequest();
                        mismoRequest.CreditReportType = creditRequestData.IncludeFannie ? E_CreditReportType.MergePlus : E_CreditReportType.Merge;
                        break;
                    case CreditReportProtocol.CreditInterlink:
                        mismoRequest = new CreditInterlink.CreditInterlink_CreditReportRequest(creditRequestData.Url);
                        mismoRequest.LenderCaseIdentifier = creditRequestData.ReferenceNumber;
                        if (creditRequestData.UseCreditCardPayment)
                        {
                            mismoRequest.UseCreditCardPayment = true;
                            mismoRequest.ServicePayment_AccountHolderName = creditRequestData.BillingFirstName + " " + creditRequestData.BillingLastName;
                            mismoRequest.ServicePayment_AccountHolderStreetAddress = creditRequestData.BillingStreetAddress;
                            mismoRequest.ServicePayment_AccountHolderCity = creditRequestData.BillingCity;
                            mismoRequest.ServicePayment_AccountHolderState = creditRequestData.BillingState;
                            mismoRequest.ServicePayment_AccountHolderPostalCode = creditRequestData.BillingZipcode;
                            mismoRequest.ServicePayment_AccountIdentifier = creditRequestData.BillingCardNumber;
                            mismoRequest.ServicePayment_AccountExpirationDate = MismoUtilities.FormatBillingExpirationDate(creditRequestData.BillingExpirationYear, creditRequestData.BillingExpirationMonth);
                            mismoRequest.ServicePayment_SecondaryAccountIdentifier = creditRequestData.BillingCVV;
                            if (creditRequestData.BillingCardNumber == "1111222233334444")
                            {
                                // 5/23/2014 dd - Test credit card number and hard code the method type.
                                // OPM 181935
                                mismoRequest.ServicePayment_MethodType = "Visa";
                            }
                            else
                            {
                                CreditCardSystemLib.CardNumber ccNumber = new CreditCardSystemLib.CardNumber(creditRequestData.BillingCardNumber);
                                string sCardType = CreditReportUtilities.GetMismoCCTypeFromCCSystemCardType(ccNumber.CardType);
                                mismoRequest.ServicePayment_MethodType = sCardType;
                            }
                        }
                        break;
                    case CreditReportProtocol.FundingSuite:
                        mismoRequest = new FundingSuite.FundingSuite_CreditReportRequest();
                        break;
                    case CreditReportProtocol.InformativeResearch:
                        mismoRequest = new InformativeResearch.IR_CreditReportRequest(creditRequestData.Url);
                        mismoRequest.LenderCaseIdentifier = creditRequestData.ReferenceNumber; // Loan Number

                        AbstractUserPrincipal requesting_user = principal;
                        if (requesting_user != null)
                        {
                            creditRequestData.RequestingPartyRequestedByName = requesting_user.DisplayName;
                        }

                        // IR requires the requesting party block zip to have format NNNNN; if zip is unknown then use the value 00000 for blank
                        if (string.IsNullOrEmpty(creditRequestData.RequestingPartyZipcode))
                        {
                            creditRequestData.RequestingPartyZipcode = "00000";
                        }

                        if (creditRequestData.UseCreditCardPayment)
                        {
                            mismoRequest.UseCreditCardPayment = true;
                            mismoRequest.ServicePayment_AccountHolderName = creditRequestData.BillingFirstName + " " + creditRequestData.BillingLastName;
                            mismoRequest.ServicePayment_AccountHolderStreetAddress = creditRequestData.BillingStreetAddress;
                            mismoRequest.ServicePayment_AccountHolderCity = creditRequestData.BillingCity;
                            mismoRequest.ServicePayment_AccountHolderState = creditRequestData.BillingState;
                            mismoRequest.ServicePayment_AccountHolderPostalCode = creditRequestData.BillingZipcode;
                            mismoRequest.ServicePayment_AccountIdentifier = creditRequestData.BillingCardNumber;
                            mismoRequest.ServicePayment_AccountExpirationDate = MismoUtilities.FormatBillingExpirationDate(creditRequestData.BillingExpirationYear, creditRequestData.BillingExpirationMonth);
                            mismoRequest.ServicePayment_SecondaryAccountIdentifier = creditRequestData.BillingCVV;
                            CreditCardSystemLib.CardNumber ccNumber = new CreditCardSystemLib.CardNumber(creditRequestData.BillingCardNumber);
                            string sCardType = CreditReportUtilities.GetMismoCCTypeFromCCSystemCardType(ccNumber.CardType);
                            mismoRequest.ServicePayment_MethodType = sCardType;
                        }

                        //// OPM 190009 - IR requires address for both borrower and co-borrower. Using the same method as was done for CSD.
                        //// Outside of a hot-fix context, perhaps the credit order page & service should be revamped to pull in the co-borrower residence data on a vendor basis.
                        if (creditRequestData.Coborrower.IsValid)
                        {
                            creditRequestData.Coborrower.CurrentAddress = creditRequestData.Borrower.CurrentAddress;
                            creditRequestData.Coborrower.PreviousAddress = creditRequestData.Borrower.PreviousAddress;
                        }
                        break;
                    case CreditReportProtocol.Equifax:
                        mismoRequest = new Equifax.Equifax_CreditReportRequest(creditRequestData.Url);
                        creditRequestData.RequestingPartyRequestedByName = principal?.DisplayName;
                        break;
                    default:
                        Tools.LogError(string.Format("Unrecognized CreditReportProtocol type {0} in CreditReportServer.OrderCreditReport()", creditRequestData.CreditProtocol.ToString()));
                        break;
                }

                SetMismoRequest(mismoRequest, creditRequestData);

                creditRequest = mismoRequest;
            }

            return creditRequest;
        }

        private static void SetMclRequest(MclCreditReportRequest request, CreditRequestData creditRequestData)
        {
            if (creditRequestData.MclRequestType == MclRequestType.Upgrade || creditRequestData.MclRequestType == MclRequestType.Refresh)
            {
                creditRequestData.IncludeEquifax = true;
                creditRequestData.IncludeExperian = true;
                creditRequestData.IncludeTransUnion = true;
            }

            request.ReferenceNumber = creditRequestData.ReferenceNumber; //OPM 15995
            request.RequestType = creditRequestData.MclRequestType;
            request.ReportID = creditRequestData.ReportID;
            request.ProviderID = creditRequestData.ProviderId; //av opm 25327
            request.LoginInfo.CopyFrom(creditRequestData.LoginInfo);
            request.InstantViewID = creditRequestData.InstantViewID;
            request.CreditReportType = creditRequestData.MclCreditReportType;

            //av opm 21340 
            if (request.RequestType == MclRequestType.Get &&
                 request.LoginInfo.UserName.TrimWhitespaceAndBOM().Length > 0 &&
                 request.LoginInfo.Password.TrimWhitespaceAndBOM().Length > 0 &&
                 request.ReportID.TrimWhitespaceAndBOM().Length > 0)
            {
                request.Borrower.CopyFrom(creditRequestData.Borrower);
                request.Coborrower.CopyFrom(creditRequestData.Coborrower);
            }

            //OPM 12769 - jM
            if (request.RequestType != MclRequestType.Get)
            {
                request.Borrower.CopyFrom(creditRequestData.Borrower);
                request.Coborrower.CopyFrom(creditRequestData.Coborrower);
                request.IncludeEquifax = creditRequestData.IncludeEquifax;
                request.IncludeExperian = creditRequestData.IncludeExperian;
                request.IncludeTransunion = creditRequestData.IncludeTransUnion;
            }

            if (request.RequestType == MclRequestType.New)
            {
                request.IncludeFannie = creditRequestData.IncludeFannie;
            }

            if (creditRequestData.UseCreditCardPayment)
            {
                request.UseCreditCardPayment = true;
                request.BillingFirstName = creditRequestData.BillingFirstName;
                request.BillingSurName = creditRequestData.BillingLastName;
                request.BillingStreet = creditRequestData.BillingStreetAddress;
                request.BillingCity = creditRequestData.BillingCity;
                request.BillingState = creditRequestData.BillingState;
                request.BillingZip = creditRequestData.BillingZipcode;
                request.CardNumber = creditRequestData.BillingCardNumber;
                request.ExpirationMonth = creditRequestData.BillingExpirationMonth;
                request.ExpirationYear = creditRequestData.BillingExpirationYear;
                request.BillingCvv = creditRequestData.BillingCVV;
            }

            if (creditRequestData.IsPdfViewNeeded)
                request.AppendOutputFormat(MclOutputFormat.PDF_BASE64);

            request.AppendOutputFormat(MclOutputFormat.HTML_INLINDED);
            //av 11 9 09 removing xml from mcl request ( opm  31360 ) 
            //request.AppendOutputFormat(MclOutputFormat.XML);
            //av opm 21340              
            request.AppendOutputFormat(MclOutputFormat.MISMO2_3_1);
        }
        private static void SetMismoRequest(MismoCreditReportRequest request, CreditRequestData creditRequestData)
        {
            request.RequestingPartyRequestedByName = creditRequestData.RequestingPartyRequestedByName;
            request.RequestingPartyName = creditRequestData.RequestingPartyName;
            request.RequestingPartyStreetAddress = creditRequestData.RequestingPartyStreetAddress;
            request.RequestingPartyCity = creditRequestData.RequestingPartyCity;
            request.RequestingPartyState = creditRequestData.RequestingPartyState;
            request.RequestingPartyZipcode = creditRequestData.RequestingPartyZipcode;
            request.IncludeEquifax = creditRequestData.IncludeEquifax;
            request.IncludeExperian = creditRequestData.IncludeExperian;
            request.IncludeTransunion = creditRequestData.IncludeTransUnion;
            request.RequestType = creditRequestData.ReportID.Length > 0 ? E_CreditReportRequestActionType.Reissue : E_CreditReportRequestActionType.Submit;

            if (creditRequestData.RequestActionType == E_CreditReportRequestActionType.Upgrade
                && !(request is InformativeResearch.IR_CreditReportRequest))
            {
                request.IncludeEquifax = true;
                request.IncludeExperian = true;
                request.IncludeTransunion = true;
            }

            // 4/20/2012 dd - Support Upgrade For Kroll Factual Data, CBC, IR, and CSD
            if (request is KrollFactualData.KrollFactualData26_CreditReportRequest ||
                request is CBC.CBC_CreditReportRequest ||
                request is InformativeResearch.IR_CreditReportRequest ||
                request is CSD.CSD_CreditReportRequest ||
                request is Equifax.Equifax_CreditReportRequest)
            {
                if (creditRequestData.RequestActionType == E_CreditReportRequestActionType.Upgrade)
                {
                    request.RequestType = E_CreditReportRequestActionType.Upgrade;
                }
                else if (creditRequestData.RequestActionType == E_CreditReportRequestActionType.Retrieve)
                {
                    request.RequestType = E_CreditReportRequestActionType.Retrieve;
                }
            }
            // For async approach i.e Information Network
            if (creditRequestData.RequestActionType == LendersOffice.CreditReport.Mismo.E_CreditReportRequestActionType.StatusQuery)
            {
                request.RequestType = LendersOffice.CreditReport.Mismo.E_CreditReportRequestActionType.StatusQuery;
            }
            request.ReportID = creditRequestData.ReportID;
            request.LoginInfo.CopyFrom(creditRequestData.LoginInfo);
            request.Borrower.CopyFrom(creditRequestData.Borrower);
            request.Coborrower.CopyFrom(creditRequestData.Coborrower);

            // 4/15/2010 dd - OPM 48256 - Support Tri-Merge for SharperLending.
            if (request is SharperLending.SharperLending_CreditReportRequest && creditRequestData.RequestActionType == E_CreditReportRequestActionType.Upgrade)
            {
                // 4/15/2010 dd - Only requirement for tri-merge to work is to set Include 3 bureau bit set to true.
                // and RequestActionType = "Submit" instead of Reissue.
                request.RequestType = E_CreditReportRequestActionType.Submit;
            }

            // 12/15/2015 BS - OPM 232068 Support Credco Upgrade
            if (request is Credco.Credco_CreditReportRequest && creditRequestData.RequestActionType == E_CreditReportRequestActionType.Upgrade)
            {
                request.RequestType = E_CreditReportRequestActionType.Upgrade;
            }

            if (request is Equifax.Equifax_CreditReportRequest)
            {
                // We want to send the Login as the Account Id
                request.LoginInfo.AccountIdentifier = creditRequestData.LoginInfo.UserName;

                if (creditRequestData.RequestActionType == E_CreditReportRequestActionType.Retrieve)
                {
                    request.RequestType = E_CreditReportRequestActionType.Retrieve;
                }
            }

            if (request.CanConvertRequestTypeToLqi
                && creditRequestData.RequestActionType == E_CreditReportRequestActionType.Other
                && (creditRequestData.IsRequestLqiReport || creditRequestData.IsRequestReissueLqiReport))
            {
                request.ConvertRequestTypeToLqi(creditRequestData);
            }

            if (request.CanCustomizeRequestDetails)
            {
                request.CustomizeRequestDetails(creditRequestData);
            }

            request.AppendOutputFormat(E_MismoOutputFormat.Text);
        }

        private static void SetFannieMaeRequest(FannieMae_CreditReportRequest request, CreditRequestData creditRequestData)
        {
            request.CreditReportIdentifier = creditRequestData.ReportID;
            request.LoginInfo.CopyFrom(creditRequestData.LoginInfo);
            request.Borrower.CopyFrom(creditRequestData.Borrower);
            request.Coborrower.CopyFrom(creditRequestData.Coborrower);
            request.IncludeEquifax = creditRequestData.IncludeEquifax;
            request.IncludeExperian = creditRequestData.IncludeExperian;
            request.IncludeTransunion = creditRequestData.IncludeTransUnion;
        }

        public static bool IsNonCreditReport(E_CreditReportAuditSourceT creditReportAuditSource)
        {
            return creditReportAuditSource == E_CreditReportAuditSourceT.RetrieveSupplement
                || creditReportAuditSource == E_CreditReportAuditSourceT.RetrieveBillingReport;
        }

        /// <summary>
        /// Gets the status description for a non-credit report.
        /// </summary>
        /// <param name="response">The response from the credit provider.</param>
        /// <returns>The description, or null if none exists.</returns>
        public static string GetNonCreditReportStatusDescription(ICreditReportResponse response)
        {
            return (response as InformativeResearch.IR_CreditReportResponse)?.StatusDescription;
        }

        /// <summary>
        /// Saves a non-credit report (a supplement or billing report) to the database and to EDocs.  This will ignore service responses without documents.
        /// </summary>
        /// <param name="response">The response from the credit provider.</param>
        /// <param name="principal">The user ordering the service.</param>
        /// <param name="sLId">The loan id for the service.</param>
        /// <param name="aAppId">The loan application id for the service.</param>
        /// <param name="requestData">The data used to send the order request for this service.</param>
        /// <param name="creditReportAuditSource">The audit type for the service.</param>
        /// <returns>The status description of the documents uploaded to EDocs.</returns>
        public static string SaveNonCreditReport(ICreditReportResponse response, AbstractUserPrincipal principal, Guid sLId, Guid aAppId, CreditRequestData requestData, E_CreditReportAuditSourceT creditReportAuditSource)
        {
            IEnumerable<CreditVendorAdditionalDocument> documentsReceived = (response as InformativeResearch.IR_CreditReportResponse)?.AdditionalDocumentsReceived ?? Enumerable.Empty<CreditVendorAdditionalDocument>();
            if (!IsNonCreditReport(creditReportAuditSource) || !documentsReceived.Any())
            {
                return string.Empty; // per discussion with Brian, the presence of documents appears to be the most reliable way to know if a non-credit report is completed.
            }

            E_AutoSavePage autoSavePage;
            string defaultProductName;
            string serviceFileTableFileType;
            switch (creditReportAuditSource)
            {
                case E_CreditReportAuditSourceT.RetrieveSupplement:
                    autoSavePage = E_AutoSavePage.CreditSupplement;
                    defaultProductName = "Credit Supplement";
                    serviceFileTableFileType = "CreditSupplement";
                    break;
                case E_CreditReportAuditSourceT.RetrieveBillingReport:
                    autoSavePage = E_AutoSavePage.BillingReport;
                    defaultProductName = "Billing Report";
                    serviceFileTableFileType = "BillingReport";
                    break;
                default:
                    throw new UnhandledEnumException(creditReportAuditSource);
            }

            Guid fileDbKey = Guid.NewGuid();
            SaveReportToFileDB(fileDbKey, response.RawXmlResponse);
            InsertReportFileToDb(response, principal, aAppId, requestData.ActualCraID, requestData.ProxyId, fileDbKey,
                spName: "SERVICE_FILE_Insert",
                defaultProductName: defaultProductName,
                fileType: serviceFileTableFileType);
            var edocRepository = EDocs.EDocumentRepository.GetSystemRepository(principal.BrokerId);
            var existingDocumentsByFileName = CreditVendorAdditionalSavedDocument.GetAllForReportId(principal.BrokerId, aAppId, requestData.ActualCraID, response.ReportID).ToDictionary(doc => doc.FileName);
            int skippedDocumentCount = 0;
            int uploadedDocumentCount = 0;
            foreach (var documentReceived in documentsReceived)
            {
                byte[] fileByteContent = LendersOffice.Drivers.Base64Encoding.Base64EncodingDriverHelper.Decode(documentReceived.Base64);
                LqbGrammar.DataTypes.SHA256Checksum fileChecksum = EncryptionHelper.ComputeSHA256Hash(fileByteContent);
                var existingDocument = existingDocumentsByFileName.GetValueOrNull(documentReceived.FileName);
                if (existingDocument?.DocumentChecksum == fileChecksum)
                {
                    skippedDocumentCount += 1;
                    continue;
                }

                Guid? documentId = LendersOffice.ObjLib.Edocs.AutoSaveDocType.AutoSaveDocTypeFactory.SavePdfDocAsSystem(
                    pageId: autoSavePage,
                    bytes: fileByteContent,
                    BrokerId: principal.BrokerId,
                    LoanId: sLId,
                    AppId: aAppId,
                    description: string.Empty,
                    enforceFolderPermissionsT: E_EnforceFolderPermissions.False);
                if (documentId.HasValue)
                {
                    uploadedDocumentCount += 1;
                    if (existingDocument != null)
                    {
                        CreditVendorAdditionalSavedDocument.UpdateDocument(existingDocument, documentId.Value, fileChecksum);
                        var existingEdoc = edocRepository.GetDocumentById(existingDocument.DocumentId);
                        existingEdoc.DocStatus = EDocs.E_EDocStatus.Obsolete;
                        edocRepository.Save(existingEdoc);
                    }
                    else
                    {
                        CreditVendorAdditionalSavedDocument.SaveNewDocument(principal.BrokerId, sLId, aAppId, requestData.ActualCraID, response.ReportID, documentReceived.FileName, documentId.Value, fileChecksum);
                    }
                }
            }

            AuditManager.RecordAudit(sLId, CreditReportAuditItem.CreateSimpleAudit(principal, creditReportAuditSource));

            string uploadStatus = uploadedDocumentCount > 0 ? uploadedDocumentCount + " document" + (uploadedDocumentCount == 1 ? null : "s") + " saved to EDocs." : null;
            string skipStatus = skippedDocumentCount > 0 ? skippedDocumentCount + " document" + (skippedDocumentCount == 1 ? " was" : "s were") + " already up-to-date in EDocs." : null;
            return (uploadStatus + " " + skipStatus).Trim();
        }

        public static int SaveXmlCreditReport(ICreditReportResponse response, AbstractUserPrincipal principal, Guid sLId, Guid aAppId, Guid comId, Guid crAccProxyId, E_CreditReportAuditSourceT creditReportAuditSource)
        {
            if (sLId == Guid.Empty || aAppId == Guid.Empty)
            {
                throw CBaseException.GenericException("Validate inputs; sLId = " + sLId + "; aAppId = " + aAppId);
            }

            // 12/18/2006 nw - post credit order/reissue checkings will now be done here
            PostCreditOrderChecks(response, principal, sLId);

            bool isLqiReport = creditReportAuditSource == E_CreditReportAuditSourceT.OrderLqiReport || creditReportAuditSource == E_CreditReportAuditSourceT.ReissueLqiReport;
            string xml = response.RawXmlResponse;

            Guid dbFileKey = Guid.NewGuid(); // 6/16/2005 dd - Always create new DBFileKey
            SaveReportToFileDB(dbFileKey, xml);

            XmlDocument doc = Tools.CreateXmlDoc(xml);
            CreditReportDebugInfo debugInfo = new CreditReportDebugInfo(sLId, principal.LoginNm);
            ICreditReport report = CreditReportFactory.ConstructCreditReportFromXmlDoc(doc, debugInfo);
            int fileVersion = SaveReportDataToLoanFile(principal, sLId, aAppId, isLqiReport, report);

            InsertReportFileToDb(response, principal, aAppId, comId, crAccProxyId, dbFileKey,
                spName: isLqiReport ? "InsertLqiCreditReportFile" : "InsertCreditReportFile",
                defaultProductName: isLqiReport ? "LQI Credit Report" : "Credit Report");

            E_AutoSavePage pageId = isLqiReport ? E_AutoSavePage.CreditReportLqi : E_AutoSavePage.CreditReport;
            AutoSaveReport(principal, sLId, aAppId, doc, debugInfo, report, pageId);

            AbstractAuditItem auditItem = new CreditReportAuditItem(principal, dbFileKey, creditReportAuditSource);
            AuditManager.RecordAudit(sLId, auditItem);

            return fileVersion;
        }

        private static int SaveReportDataToLoanFile(AbstractUserPrincipal principal, Guid sLId, Guid aAppId, bool isLqiReport, ICreditReport report)
        {
            if (aAppId == Guid.Empty)
            {
                throw CBaseException.GenericException("Invalid App Id");
            }

            // Record the receive date of credit report to the application track view.
            // 3/17/14 gf - opm 174135, updating the file is considered a system operation here, bypass workflow checks
            CPageData dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(sLId, typeof(CreditReportUtilities));
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            CAppData dataApp = dataLoan.GetAppData(aAppId);

            // 12/19/2006 nw - OPM 8079 - set aCrOd to credit report's ordered date
            if (isLqiReport)
            {
                var orderedDate = CDateTime.Create(report.CreditReportLastUpdatedDate);
                dataApp.aLqiCrOd = orderedDate.IsValid ? orderedDate : CDateTime.Create(DateTime.Now);
                dataApp.aLqiCrRd = CDateTime.Create(DateTime.Now);
            }
            else
            {
                dataApp.aCrOd = CDateTime.Create(report.CreditReportFirstIssuedDate);
                dataApp.aCrRd = CDateTime.Create(DateTime.Now);

                UpdateAliases(dataApp, report);
                UpdateFTHB(principal, dataLoan, report);
                dataLoan.UpdateCreditReportDirtyBit();
            }

            dataLoan.Save();
            return dataLoan.sFileVersion;
        }

        private static void InsertReportFileToDb(ICreditReportResponse response, AbstractUserPrincipal principal, Guid aAppId, Guid comId, Guid crAccProxyId, Guid dbFileKey, string spName, string defaultProductName, string fileType = null)
        {
            var parameters = new List<SqlParameter>
            {
                new SqlParameter("@Owner", aAppId),
                new SqlParameter("@ExternalFileId", response.ReportID),
                new SqlParameter("@DbFileKey", dbFileKey),
                new SqlParameter("@ComId", comId),
                new SqlParameter("@UserId", principal.UserId),
                new SqlParameter("@CrAccProxyId", crAccProxyId == Guid.Empty ? (object) DBNull.Value : (object) crAccProxyId),
                new SqlParameter("@HowDidItGetHere", ConstAppDavid.ServiceFileOrder),
                new SqlParameter("@BrandedProductName", response.BrandedProductName ?? defaultProductName),
            };

            if (fileType != null)
            {
                parameters.Add(new SqlParameter("@FileType", fileType));
            }

            StoredProcedureHelper.ExecuteNonQuery(principal.BrokerId, spName, 0, parameters);
        }

        private static void UpdateFTHB(AbstractUserPrincipal principal, CPageData dataLoan, ICreditReport report)
        {
            if (principal.HasFeatures(E_BrokerFeatureT.PriceMyLoan))
            {
                // 11/16/2006 dd - Only applicable to user with PML enabled.
                // When new credit report save to loan, we will try to initialize the FTHB to value 
                // from credit report. See OPM 7997
                var originalCalcMode = dataLoan.CalcModeT;

                dataLoan.CalcModeT = E_CalcModeT.PriceMyLoan;
                dataLoan.sHas1stTimeBuyerPe = report.Is1stTimeHomeBuyerWithin(12);
                dataLoan.CalcModeT = originalCalcMode;
            }
        }

        private static void UpdateAliases(CAppData dataApp, ICreditReport report)
        {
            // Only add aliases if we have not seen the name before and there is space to do so
            var bAliases = new HashSet<string>(dataApp.aBAliases);
            bAliases.Add(dataApp.aBNm);

            if (report.BorrowerInfo != null)
            {
                var newAliases = new List<string>(dataApp.aBAliases);

                foreach (string alias in report.BorrowerInfo.Aliases)
                {
                    if (bAliases.Contains(alias))
                    {
                        continue;
                    }
                    newAliases.Add(alias);
                }

                dataApp.aBAliases = newAliases;
            }

            var cAliases = new HashSet<string>(dataApp.aCAliases);
            cAliases.Add(dataApp.aCNm);

            if (report.CoborrowerInfo != null)
            {
                var newAliases = new List<string>(dataApp.aCAliases);

                foreach (string alias in report.CoborrowerInfo.Aliases)
                {
                    if (cAliases.Contains(alias))
                    {
                        continue;
                    }
                    newAliases.Add(alias);
                }

                dataApp.aCAliases = newAliases;
            }
        }

        private static void SaveReportToFileDB(Guid key, string xml)
        {
            byte[] data = System.Text.Encoding.UTF8.GetBytes(xml);
            FileDBTools.WriteData(E_FileDB.Normal, key.ToString(), data);
        }

        // OPM 111224 - Auto-save the credit report.
        private static void AutoSaveReport(AbstractUserPrincipal principal, Guid sLId, Guid aAppId, XmlDocument doc, CreditReportDebugInfo debugInfo, ICreditReport report, E_AutoSavePage pageId)
        {
            ICreditReportView view = CreditReportFactory.ConstructCreditReportViewFromXmlDoc(doc, debugInfo).Value;
            if (view.HasPdf)
            {
                LendersOffice.ObjLib.Edocs.AutoSaveDocType.AutoSaveDocTypeFactory.SavePdfDocAsSystem(
                    pageId,
                    Convert.FromBase64String(view.PdfContent),
                    principal.BrokerId,
                    sLId,
                    aAppId,
                    "",
                    E_EnforceFolderPermissions.False);
            }
            else if (view.HasHtml)
            {
                LendersOffice.ObjLib.Edocs.AutoSaveDocType.AutoSaveDocTypeFactory.SaveHtmlDocAsSystem(
                    pageId,
                    () => view.HtmlContent,
                    principal.BrokerId,
                    sLId,
                    aAppId,
                    "",
                    E_EnforceFolderPermissions.False);
            }
            else if (!string.IsNullOrEmpty(view.RawXml))
            {
                LendersOffice.ObjLib.Edocs.AutoSaveDocType.AutoSaveDocTypeFactory.SaveHtmlDocAsSystem(
                    pageId,
                    () => CreditReportUtilities.GenerateSinglePageCreditReportHtml(report),
                    principal.BrokerId,
                    sLId,
                    aAppId,
                    "",
                    E_EnforceFolderPermissions.False);
            }
        }

        private static void PostCreditOrderChecks(ICreditReportResponse response, AbstractUserPrincipal principal, Guid sLId)
		{
			// 5/25/2006 nw - OPM 3210 - Before we import/save the credit report, verify that the response 
			// includes both a XML data portion and a viewable data portion (HTML, PDF, TEXT).
            // removing checks for viewable data av opm      33653
            XmlDocument oDoc = Tools.CreateXmlDoc(response.RawXmlResponse);

			XmlNode oNode = null;
			bool bHasXMLData = false;							
			
            oNode = oDoc.SelectSingleNode("//OUTPUT/RESPONSE/OUTPUT_FORMAT");

			if (null != oNode)
			{
    
				XmlNodeList oMCLXMLOutputFormatList = oDoc.SelectNodes("//OUTPUT/RESPONSE/OUTPUT_FORMAT");

				foreach (XmlNode o in oMCLXMLOutputFormatList)
				{
					XmlElement oElem = (XmlElement) o;
					string sType = oElem.GetAttribute("format_type").ToUpper();
                    if (sType == "MISMO2_3_1" || sType == "XML")
                    {
                        bHasXMLData = true;
                    }
				}
			}

			oNode = oDoc.SelectSingleNode("//RESPONSE_GROUP/RESPONSE/RESPONSE_DATA/CREDIT_RESPONSE");

			if (null != oNode)
			{
				// we have MISMO response
				bHasXMLData = true;

				// 12/18/2006 nw - OPM 7000 - Handle rare cases of multiple RESPONSE_DATA elements
				XmlNodeList responseDataNodeList = oDoc.SelectNodes("//RESPONSE_GROUP/RESPONSE/RESPONSE_DATA");
				if (responseDataNodeList.Count > 1)
				{
					// Send email to warn us
					string msg = string.Format("Multiple RESPONSE_DATA elements found in a credit report, will only process the 1st element.{0}{0}Loan ID = {1}{0}Login Name = {2}", Environment.NewLine, sLId.ToString(), principal.LoginNm);
					Tools.LogWarning(msg);
				}
			}

			if (!bHasXMLData)
				throw new CBaseException(ErrorMessages.CreditReportMissingXml, "Credit response missing XML data LoanId " + sLId);
		}
	
		public static ICreditReport GetReportFrom( XmlDocument doc ) 
		{
			return CreditReportFactory.ConstructCreditReportFromXmlDoc( doc, null )  as ICreditReport ; 
		}

        public static void UpdateLastUsedCra(Guid brokerId, string existingLoginName, string newLoginName, string existingAccountId, string newAccountId, Guid existingProtocolID, Guid newProtocolID, Guid userID )
        {
            if (existingLoginName != newLoginName || existingProtocolID != newProtocolID || existingAccountId != newAccountId)
            {
                SqlParameter[] parameters = {
                                                new SqlParameter("@UserID", userID),
                                                new SqlParameter("@LastUsedCreditLoginNm", newLoginName),
                                                new SqlParameter("@LastUsedCreditProtocolID", newProtocolID),
                                                new SqlParameter("@LastUsedCreditAccountId", newAccountId)
                                            };
                StoredProcedureHelper.ExecuteNonQuery(brokerId, "UpdateLastUsedCredit", 1, parameters);
            }
        }

        /// <summary>
        /// Imports the liabilities from credit report currently on file. It also imports the public records.  It clears the public records all the time. 
        /// </summary>
        /// <param name="loanID"></param>
        /// <param name="applicationID">If empty it uses the 0 app</param>
        /// <param name="skipZeroBalanceLiab"></param>
        /// <param name="deleteExistingLiab"></param>
        /// <param name="importBorrowerInfo"></param>
        /// <param name="importIDFromLiabilities"> LPQ ONLY USE ONLY FALSE</param>
        public static int ImportLiabilities(Guid loanID, Guid applicationID, bool skipZeroBalanceLiab, bool deleteExistingLiab, bool importBorrowerInfo,  bool importIDFromLiabilities = false)
        {
            CPageData dataLoan = new CLOImportLiabilityData(loanID);
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            CAppData dataApp;
            if (applicationID == Guid.Empty)
            {
                dataApp = dataLoan.GetAppData(0);
            }
            else
            {
                dataApp = dataLoan.GetAppData(applicationID);
            }

            return ImportLiabilities(dataLoan, dataApp, skipZeroBalanceLiab, deleteExistingLiab, importBorrowerInfo, importIDFromLiabilities);
        }

        /// <summary>
        /// Imports the liabilities from credit report currently on file. It also imports the public records.  It clears the public records all the time. 
        /// </summary>
        /// <param name="dataLoan">Loan to import into. Should be InitSaved.</param>
        /// <param name="appData">If null it uses the primary app from the loan.</param>
        /// <param name="skipZeroBalanceLiabilities"></param>
        /// <param name="deleteExistingLiabilities">Whether to delete existing regular liabilities before importing from the credit report.</param>
        /// <param name="importBorrowerInfo"></param>
        /// <param name="importIdFromLiabilities"> LPQ ONLY USE ONLY FALSE</param>
        public static int ImportLiabilities(CPageData dataLoan, CAppData appData, bool skipZeroBalanceLiabilities, bool deleteExistingLiabilities, bool importBorrowerInfo, bool importIdFromLiabilities = false)
        {
            if (appData == null)
            {
                appData = dataLoan.GetAppData(0);
            }

            if (deleteExistingLiabilities)
            {
                foreach (var lia in appData.aLiaCollection.GetSubcollection(true, E_DebtGroupT.Regular).Cast<ILiability>())
                {
                    lia.IsOnDeathRow = true;
                }
                appData.aLiaCollection.Flush();
            }

            appData.ImportLiabilitiesFromCreditReport(skipZeroBalanceLiabilities, importBorrowerInfo, importIdFromLiabilities);
            // 2/27/2007 nw - OPM 10630 - import public records
            appData.aPublicRecordCollection.ClearAll();
            appData.aPublicRecordCollection.Flush();
            appData.ImportPublicRecordsFromCreditReport();
            dataLoan.Save();
            return dataLoan.sFileVersion;
        }

        // OPM 50438
        private static bool DoMiddleNamesMatch(string loanFileMiddleName, string creditReportMiddleName)
        {
            string tempCR = creditReportMiddleName.TrimEnd('.').ToLower();
            string tempLF = loanFileMiddleName.TrimEnd('.').ToLower();
            // If either middle names are blank but the other is not, they do not match
            if ((tempCR.Length == 0 && tempLF.Length != 0) || (tempLF.Length == 0 && tempCR.Length != 0))
            {
                return false;
            }
            else if (tempCR.Equals(tempLF))
            {
                return true;
            }
            else if (tempCR.Length == 1 && tempCR[0] == tempLF[0])
            {
                return true;
            }
            else if (tempLF.Length == 1 && tempLF[0] == tempCR[0])
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool IsBorrowerInformationMatch(Guid sLId, Guid aAppId, CreditRequestData requestData, ICreditReportResponse response)
        {
            if (null == response || string.IsNullOrEmpty(response.RawXmlResponse))
            {
                return false;
            }

            string xml = response.RawXmlResponse;

            XmlDocument doc = Tools.CreateXmlDoc(xml);
            CreditReportDebugInfo debugInfo = new CreditReportDebugInfo(sLId, "");
            ICreditReport creditReport = CreditReportFactory.ConstructCreditReportFromXmlDoc(doc, debugInfo);

            if (creditReport == null)
            {
                return false; // No credit report on file.
            }

            CPageData dataLoan = CPageData.CreateUsingSmartDependency(sLId, typeof(CreditReportUtilities));
            dataLoan.InitLoad();
            CAppData dataApp = dataLoan.GetAppData(aAppId);

            if (requestData.RequestActionType == Mismo.E_CreditReportRequestActionType.Other
                && (requestData.RequestActionTypeOtherDetail == Mismo.CreditReportRequestActionTypeOtherDetail.RemoveBorrower
                    || requestData.RequestActionTypeOtherDetail == Mismo.CreditReportRequestActionTypeOtherDetail.RemoveCoBorrower))
            {
                // For remove borrower, we rely on the caller to have already adjusted the borrower to match the primary borrower on the credit report.
                // For both credit reports, there is no longer a co-borrower on the credit report.
                return IsBorrowerInformationMatch(creditReport, aBLastNm: dataApp.aBLastNm, aBSsn: dataApp.aBSsn, aBNm: dataApp.aBNm, aCLastNm: string.Empty, aCSsn: string.Empty, aCNm: string.Empty);
            }

            return IsBorrowerInformationMatch(creditReport, dataApp.aBLastNm, dataApp.aBSsn, dataApp.aBNm, dataApp.aCLastNm, dataApp.aCSsn, dataApp.aCNm);
        }

        public static bool IsBorrowerInformationMatch(Guid sLId, Guid aAppId, string aBFirstNm, string aBMidNm, string aBLastNm, string aBSuffix, string aBSsn, string aCFirstNm, string aCMidNm, string aCLastNm, string aCSuffix, string aCSsn, ICreditReportResponse response)
        {
            if (null == response || string.IsNullOrEmpty(response.RawXmlResponse))
            {
                return false;
            }
            string xml = response.RawXmlResponse;

            XmlDocument doc = Tools.CreateXmlDoc(xml);
            CreditReportDebugInfo debugInfo = new CreditReportDebugInfo(sLId, "");
            ICreditReport creditReport = CreditReportFactory.ConstructCreditReportFromXmlDoc(doc, debugInfo);

            if (creditReport == null)
            {
                return false; // No credit report on file.
            }

            CPageData dataLoan = CPageData.CreateUsingSmartDependency(sLId, typeof(CreditReportUtilities));
            dataLoan.InitLoad();

            CAppData dataApp = dataLoan.GetAppData(aAppId);
            if (string.IsNullOrEmpty(aBSsn))
                aBSsn = dataApp.aBSsn;
            if (string.IsNullOrEmpty(aCSsn))
                aCSsn = dataApp.aCSsn;

            return IsBorrowerInformationMatch(creditReport, aBLastNm, aBSsn, aBFirstNm + aBLastNm, aCLastNm, aCSsn, aCFirstNm + aCLastNm);
        }

        private static bool IsBorrowerInformationMatch(ICreditReport creditReport, string aBLastNm, string aBSsn, string aBNm, string aCLastNm, string aCSsn, string aCNm)
        {
            int lastNameMaxLength = 50;
            if (creditReport == null)
            {
                return false; // No credit report on file.
            }

            if (creditReport.BorrowerInfo != null)
            {
                string aReportBLastNm = creditReport.BorrowerInfo.LastName.Trim();
                if (aReportBLastNm.Length > lastNameMaxLength)
                {
                    aReportBLastNm = aReportBLastNm.Substring(0, lastNameMaxLength);
                }

                if (!string.Equals(aBLastNm, aReportBLastNm, StringComparison.OrdinalIgnoreCase) ||
                    !string.Equals(aBSsn.Replace("-", ""), creditReport.BorrowerInfo.Ssn.Replace("-", "")))
                {
                    return false;
                }
            }
            else if (creditReport.BorrowerInfo == null && !string.IsNullOrEmpty(aBNm))
            {
                return false;
            }

            if (creditReport.CoborrowerInfo != null)
            {
                //OPM 60854 - Match borrower only if the loan file has borrower info
                if (!string.IsNullOrEmpty(aCLastNm) || !string.IsNullOrEmpty(aCSsn))
                {
                    string aReportCLastNm = creditReport.CoborrowerInfo.LastName.Trim();
                    if (aReportCLastNm.Length > lastNameMaxLength)
                    {
                        aReportCLastNm = aReportCLastNm.Substring(0, lastNameMaxLength);
                    }

                    if (!string.Equals(aCLastNm, aReportCLastNm, StringComparison.OrdinalIgnoreCase) ||
                        !string.Equals(aCSsn.Replace("-", ""), creditReport.CoborrowerInfo.Ssn.Replace("-", "")))
                    {
                        return false;
                    }
                }

            }
            else if (creditReport.CoborrowerInfo == null && !string.IsNullOrEmpty(aCNm))
            {
                return false;
            }

            return true;
        }

        public static bool IsCraPdfEnabled( CreditReportProtocol protocol, Guid brokerId, Guid comId )
        {
            if (protocol != CreditReportProtocol.Mcl) { return false; }

            bool foundCRA = false;
            bool isPdfNeeded = false;
            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerID", brokerId)
                                        };
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "ListBrokerCra", parameters))
            {
                while (reader.Read() && !foundCRA)
                {
                    if (comId == (Guid) reader["ServiceComId"] )
                    {
                        isPdfNeeded = (bool)reader["IsPdfViewNeeded"];
                        foundCRA = true;
                    }
                }
            }

            return isPdfNeeded; 
        }

        /// <summary>
        /// case 73227
        /// Converts the card types returned by the Credit card system to those specified in Mismo credit specification.
        /// </summary>
        /// <param name="sCardType"></param>
        /// <returns></returns>
        public static string GetMismoCCTypeFromCCSystemCardType(string sCardType)
        {
            switch (sCardType.ToUpper())
            { 
                case "VISA":
                    return "Visa";
                case "MASTER_CARD":
                    return "MasterCard";
                case "AMERICAN_EXPRESS":
                    return "AmericanExpress";                
                case "DISCOVER":
                    return "Discover";
                case "DINERS_CLUB":
                    return "DinersClub";
                case "JCB":
                default:
                    return "Other";
            }
        }

        /// <summary>
        /// Generate a single page with the Summary, Liabilities, and Public Records information.
        /// Does not include any styling information.
        /// </summary>
        /// <param name="creditReport"></param>
        /// <returns></returns>
        public static string GenerateSinglePageCreditReportHtml(ICreditReport creditReport)
        {
            var htmlBuilder = new System.Text.StringBuilder();
            htmlBuilder.Append("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\" >");
            htmlBuilder.Append("<html><head><title>Credit Report</title></head><body>");
            htmlBuilder.Append(GenerateSummaryHtml(creditReport));
            htmlBuilder.Append(GenerateLiabilitiesHtml(creditReport));
            htmlBuilder.Append(GeneratePublicRecordsHtml(creditReport));
            htmlBuilder.Append("</body></html>");
            return htmlBuilder.ToString();
        }

        /// <summary>
        /// Generates an HTML table to display the borrowers' scores.
        /// Returns empty string if it is an Optimistic Default report.
        /// </summary>
        /// <param name="creditReport"></param>
        /// <returns></returns>
        public static string GenerateSummaryHtml(ICreditReport creditReport)
        {
            if (creditReport == null || creditReport is CCreditReportWithOptimisticDefault) return "";

            var htmlBuilder = new System.Text.StringBuilder();

            if (null != creditReport.BorrowerInfo)
            {
                htmlBuilder.AppendFormat("<table><tr><td class='fieldlabel'>Borrower Name:</td><td>{0} {1}</td></tr>", creditReport.BorrowerInfo.FirstName, creditReport.BorrowerInfo.LastName);
                htmlBuilder.AppendFormat(@"<tr><td class='fieldlabel'>Borrower Credit Scores</td><td></td></tr>
                        <tr><td></td><td>Equifax:   <b>{0}</b><br/> Experian:   <b>{1}</b><br/> TransUnion: <b>{2}</b></td></tr>", creditReport.BorrowerScore.Equifax, creditReport.BorrowerScore.Experian, creditReport.BorrowerScore.TransUnion);
            }
            if (null != creditReport.CoborrowerInfo)
            {
                htmlBuilder.AppendFormat("<tr><td></td></tr><tr><td class='fieldlabel'>Coborrower Name:</td><td>{0} {1}</td></tr>", creditReport.CoborrowerInfo.FirstName, creditReport.CoborrowerInfo.LastName);
                htmlBuilder.AppendFormat(@"<tr><td class='fieldlabel'>CoBorrower Credit Scores</td></tr>
                        <tr><td></td><td>Equifax:   <b>{0}</b><br/> Experian:   <b>{1}</b><br/> TransUnion: <b>{2}</b></td></tr>", creditReport.CoborrowerScore.Equifax, creditReport.CoborrowerScore.Experian, creditReport.CoborrowerScore.TransUnion);
            }
            htmlBuilder.AppendFormat("<tr><td></td><td></td></tr><tr><td class='fieldlabel'>Credit Rating Code Used:</td><td>{0}</td></tr>", creditReport.CreditRatingCodeType);
            htmlBuilder.Append("</table>");

            return htmlBuilder.ToString();
        }

        /// <summary>
        /// Generates a header and table for the liabilities on the given file.
        /// </summary>
        /// <param name="creditReport"></param>
        /// <returns></returns>
        public static string GenerateLiabilitiesHtml(ICreditReport creditReport)
        {
            if (creditReport == null) return "";

            var htmlBuilder = new System.Text.StringBuilder();

            htmlBuilder.Append("<h6>Liabilities Count = " + creditReport.AllLiabilities.Count + "</h6>");
            foreach (AbstractCreditLiability o in creditReport.AllLiabilities)
            {
                string[][] args = 
                              {
                                  new string[] {"ID", o.ID},
                                  new string[] {"Is Active?", o.IsActive.ToString()},
                                  new string[] {"Is Bankruptcy?", o.IsBankruptcy.ToString()},

                                  new string[] {"Account Identifier", o.AccountIdentifier},
                                  new string[] {"Is Late?", o.IsLate.ToString()},
                                  new string[] {"Is ChargeOff?", o.IsChargeOff.ToString()},

                                  new string[] {"Creditor Name", o.CreditorName},
                                  new string[] {"Is Credit?", o.IsCredit.ToString()},                                

                                  new string[] {"Account Opened Date", o.AccountOpenedDate.ToShortDateString()},
                                  new string[] {"Is Paid?", o.IsPaid.ToString()},
                                  new string[] {"Is Collection?", o.IsCollection.ToString()},

                                  new string[] {"Account Reported Date", o.AccountReportedDate.ToShortDateString()},
                                  new string[] {"Late30", o.Late30},
                                  new string[] {"Is Foreclosure?", o.IsForeclosure.ToString()},

                                  new string[] {"Last Activity Date", o.LastActivityDate.ToShortDateString()},
                                  new string[] {"Late60", o.Late60},

                                  new string[] {"Is Subject Property Mortgage?", o.IsSubjectPropertyMortgage.ToString()},
                                  new string[] {"Is Subject Property 1st Mortgage?", o.IsSubjectProperty1stMortgage.ToString()},

                                  new string[] {"Late90", o.Late90},
                                  new string[] {"Is Repos?", o.IsRepos.ToString()},

                                  new string[] {"Months Reviewed Count", o.MonthsReviewedCount.ToString()},
                                  new string[] {"Late120", o.Late120},
                                  new string[] {"Is Derog Currently?", o.IsDerogCurrently.ToString()},

                                  new string[] {"Has Been Derog?", o.HasBeenDerog.ToString()},
                                  new string[] {"Account End Derog Date", o.EndDerogDate.ToShortDateString()},
                                  new string[] {"Is Current?", o.IsCurrent.ToString()},

                                  new string[] {"LiabilityType", o.LiabilityType.ToString()},
                                  new string[] {"MonthlyPaymentAmount", o.MonthlyPaymentAmount.ToString()},
                                  new string[] {"Unpaid Balance Amount", o.UnpaidBalanceAmount.ToString()},
                                  new string[] {"Payment Pattern Start Date", o.PaymentPatternStartDate.ToShortDateString()},                                  

                                  new string[] {"High Credit Amount", o.HighCreditAmount.ToString()},
                                  new string[] {"Is Authorized User?", o.IsAuthorizedUser.ToString()},
                                  new string[] {"Is Student Loans Not In Repayment?", o.IsStudentLoansNotInRepayment.ToString()}
                              };

                htmlBuilder.Append(@"<div class='recordgrid'><table class='recordgrid' cellspacing='0' cellpadding='2'>");
                int count = args.Length;
                int columnsPerRow = 3;
                int j = 0;
                for (int i = 0; i < count; i++)
                {
                    if (j == 0)
                    {
                        htmlBuilder.Append("<tr>");
                    }

                    string[] items = args[i];
                    htmlBuilder.AppendFormat("<td class='fieldlabel'>{0}</td><td class='fieldvalue'>{1}</td>", items[0], items[1]);

                    if (j == (columnsPerRow - 1))
                    {
                        htmlBuilder.Append("</tr>");
                        j = -1;
                    }
                    j++;
                }

                htmlBuilder.AppendFormat("<tr><td colspan='{0}' class='fieldlabel'>Late Dates</td></tr><tr><td colspan={0}><ul>", columnsPerRow * 2);
                foreach (CreditAdverseRating rating in o.LateOnlyCreditAdverseRatingList)
                {
                    htmlBuilder.Append("<li>" + rating + "</li>");
                }
                if (o.LateOnlyCreditAdverseRatingList.Count == 0)
                    htmlBuilder.Append("<li style='list-style-type:none'>None</li>");

                htmlBuilder.AppendFormat("</ul></td></tr><tr><td class='fieldlabel' colspan={0}>All Adverse Rating List</td></tr><tr><td colspan={0}><ul>", columnsPerRow * 2);
                foreach (CreditAdverseRating rating in o.CreditAdverseRatingList)
                {
                    htmlBuilder.Append("<li>" + rating + "</li>");
                }
                if (o.CreditAdverseRatingList.Count == 0)
                    htmlBuilder.Append("<li style='list-style-type:none'>None</li>");

                htmlBuilder.Append("</ul></td></tr>");
                htmlBuilder.Append("</table></div>");
            }

            return htmlBuilder.ToString();
        }

        /// <summary>
        /// Generates a header and table for the public records on the given file
        /// </summary>
        /// <param name="creditReport"></param>
        /// <returns></returns>
        public static string GeneratePublicRecordsHtml(ICreditReport creditReport)
        {
            if (creditReport == null) return "";

            var htmlBuilder = new System.Text.StringBuilder();
            htmlBuilder.Append("<h6>Public Records Count = " + creditReport.AllPublicRecords.Count + "</h6>");

            foreach (AbstractCreditPublicRecord o in creditReport.AllPublicRecords)
            {
                string[][] args = 
                              {
                                  new string[] {"ID", o.ID},
                                  new string[] {"Type", o.Type.ToString()},
                                  new string[] {"Last Effective Date", o.LastEffectiveDate.ToShortDateString()},

                                  new string[] {"Reported Date", o.ReportedDate.ToShortDateString()},
                                  new string[] {"Bk File Date", o.BkFileDate.ToShortDateString()},
                                  new string[] {"Disposition Date", o.DispositionDate.ToShortDateString()},

                                  new string[] {"Disposition Type", o.DispositionType.ToString()},
                                  new string[] {"Bankruptcy Liabilities Amount", o.BankruptcyLiabilitiesAmount.ToString()},
                                  new string[] {"Is Discharged?", o.IsDischarged.ToString()},
                                  new string[] {"Is Bankruptcy Filed?", o.IsBankruptcyFiled.ToString()},
                                  new string[] {"Is Collection?", o.IsCollection.ToString()},
                                  new string[] {"Is Collection Paid?", o.IsCollectionPaid.ToString()},
                                  new string[] {"Is Collection Unpaid?", o.IsCollectionUnpaid.ToString()},
                                  new string[] {"Is Judgment?", o.IsJudgment.ToString()},
                                  new string[] {"Is Judgment Paid?", o.IsJudgmentPaid.ToString()},
                                  new string[] {"Is Judgment Unpaid?", o.IsJudgmentUnpaid.ToString()},
                                  new string[] {"Is Lien?", o.IsLien.ToString()},
                                  new string[] {"Is Lien Unpaid?", o.IsLienUnpaid.ToString()},
                                  new string[] {"Is Non-Tax Lien?", o.IsNontaxLien.ToString()},
                                  new string[] {"Is Non-Tax Lien Paid?", o.IsNontaxLienPaid.ToString()},
                                  new string[] {"Is Non-Tax Lien Unpaid?", o.IsNontaxLienUnpaid.ToString()},
                                  new string[] {"Is Tax Lien?", o.IsTaxLien.ToString()},
                                  new string[] {"Is Tax Lien Unpaid?", o.IsTaxLienUnpaid.ToString()}
                              };

                htmlBuilder.Append(@"<div class='recordgrid'><table class='recordgrid' cellspacing='0' cellpadding='2'>");
                int count = args.Length;
                int columnsPerRow = 3;
                int j = 0;
                for (int i = 0; i < count; i++)
                {
                    if (j == 0)
                    {
                        htmlBuilder.Append("<tr>");
                    }

                    string[] items = args[i];
                    htmlBuilder.AppendFormat("<td class='fieldlabel'>{0}</td><td class='fieldvalue'>{1}</td>", items[0], items[1]);

                    if (j == (columnsPerRow - 1))
                    {
                        htmlBuilder.Append("</tr>");
                        j = -1;
                    }
                    j++;
                }
                htmlBuilder.Append("</table></div>");
            }

            return htmlBuilder.ToString();
        }

        /// <summary>
        /// Retrieves borrower name and credit reference numbers per application.
        /// </summary>
        /// <returns>The data formatted in HTML for displaying in the UI.</returns>
        public static string RetrieveCreditReferenceDataPerApplication(Guid loanId)
        {
            var dataLoan = CPageData.CreateUsingSmartDependency(loanId, typeof(CreditReportUtilities));
            dataLoan.InitLoad();

            var creditReferenceData = new System.Text.StringBuilder();

            for (int i = 0; i < dataLoan.nApps; i++)
            {
                var app = dataLoan.GetAppData(i);
                string name = app.aBNm;
                if (app.aCNm != string.Empty)
                {
                    name += " & " + app.aCNm;
                }

                SqlParameter[] parameters = { new SqlParameter("@ApplicationID", app.aAppId) };

                string creditReference = "Not Available";
                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(dataLoan.sBrokerId, "RetrieveCreditReport", parameters))
                {
                    if (reader.Read())
                    {
                        creditReference = (string)reader["ExternalFileID"];
                    }
                }

                creditReferenceData.Append($"<br /><hr /><u>Borrower</u>: {AspxTools.HtmlString(name)}<br><u>Credit Reference Number</u>: <span id=\"credit{i}\" class=\"CreditReferenceNumber\">{AspxTools.HtmlString(creditReference)}</span>&nbsp;<input type=\"button\" value=\"Copy\" class=\"ButtonStyle CopyButton\" NoHighlight onclick=\"copyToClipboard($j('#credit{i}').html());\" />");
            }

            return creditReferenceData.ToString();
        }
    }
}