using System;

using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.CreditReport
{
	/// <summary>
	/// Summary description for MismoUtilities.
	/// </summary>
	public class MismoUtilities
	{
        /// <summary>
        /// Return CCYY-MM-DD
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static string ToDateString(DateTime dt) 
        {
            return dt.ToString("yyyy-MM-dd");
        }

        /// <summary>
        /// Return CCYY-MM-DDTHH:MM:SS
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static string ToDateTimeString(DateTime dt) 
        {
            return dt.ToString("yyyy-MM-ddTHH:mm:ss");
        }

        public static DateTime ParseDateTime(string s) 
        {
            DateTime dt = DateTime.MinValue;
            try 
            {
                if (null != s && "" != s)
                    dt = DateTime.ParseExact(s, new string[] {
                        "yyyy-M-d", 
                        "yyyy-M", 
                        "yyyy-M-dTH:m:s", 
                        "yyyy-M-dTH:m:s.fffzzz", 
                        "M/yy",
                        "M/yyyy",
                        "yyyy-M-d H:m"
                },null, System.Globalization.DateTimeStyles.None);
            } 
            catch (Exception exc)
            {
                Tools.LogWarning("MismoUtilities.ParseDateTime:" + s, exc);
            }
            return dt;
        }
        public static bool ParseBool(string s) 
        {
            return s == "Y";
        }
        public static string FormatBillingExpirationDate(int year, int month)
        {
            return year.ToString("0000") + "-" + month.ToString("00");
        }
	}

    public class MclUtilities 
    {
        /// <summary>
        /// Convert string to DateTime object base on format. Valid format are 'mm/yy', 'mm/dd/yy', 'yyyymm'. Return DateTime.MinValue as Invalid
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="format"></param>
        /// <returns></returns>
        public static DateTime ParseDateTime(string dt, string format) 
        {
            switch( dt.TrimWhitespaceAndBOM().ToUpper() )
            {
                case "NO LATES IN PAST 24 MONTHS":
                case "UUUUUU":
                    return DateTime.MinValue;
            }

            DateTime datetime = DateTime.Today;

            try
            {
                if (format == "mm/yy") 
                {
                    string[] values = dt.Split('/');
                    int month = int.Parse(values[0]);
                    int year = int.Parse(values[1]);
                    int day = DateTime.Today.Day + 1;

                    if (year < 30) 
                        year += 2000;
                    else 
                        year += 1900;
                    return new DateTime(year, month, day);
                } 
                else if (format == "mm/dd/yy") 
                {
                    string[] values = dt.Split('/');
                    int month = int.Parse(values[0]);
                    int day = int.Parse(values[1]);
                    int year = int.Parse(values[2]);
                    if (year < 30) 
                        year += 2000;
                    else 
                        year += 1900;
                    return new DateTime(year, month, day);

                } 
                else if (format== "yyyymm") 
                {
                    int year = int.Parse(dt.Substring(0, 4));
                    int month = int.Parse(dt.Substring(4));
					// 4/12/2007 nw - OPM 12400 - Default day should be 1 instead of Today.day
                    // int day = DateTime.Today.Day;
					int day = 1;

                    return new DateTime(year, month, day);
                }
                Tools.LogWarning("MCL DateFormat " + format + " is currently not supported.");
                return DateTime.MinValue;
            }
            catch
            {
                return DateTime.MinValue;
            }
        }

    }

    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Property ,AllowMultiple=false, Inherited=false)]
    public class CreditDebugAttribute : Attribute 
    {
        private string m_category;
        public string Category 
        {
            get { return m_category; }
        }
        public CreditDebugAttribute(string category) 
        {
            m_category = category;
        }
    }

}
