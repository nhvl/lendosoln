﻿// <copyright file="CreditReportTypes.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: jhairoe
//    Date:   07/12/2015 4:55:48 PM 
// </summary>
namespace LendersOffice.CreditReport
{
    using System;

    /// <summary>
    /// Credit report type.
    /// </summary>
    public enum CreditReportTypes
    {
        /// <summary>
        /// Primary credit report.
        /// </summary>
        Primary = 0,

        /// <summary>
        /// Loan quality initiative credit report.
        /// </summary>
        Lqi = 1
    }
}
