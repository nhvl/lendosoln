﻿namespace LendersOffice.CreditReport
{
    using System;
    using System.Collections.Generic;
    using DataAccess;
    using ObjLib.BackgroundJobs;
    using Security;

    /// <summary>
    /// Handles requests for ordering credit reports.
    /// </summary>
    public class CreditReportRequestHandler
    {
        /// <summary>
        /// The credit request data.
        /// </summary>
        private CreditRequestData creditRequestData;

        /// <summary>
        /// The principal of the user making the request.
        /// </summary>
        private AbstractUserPrincipal principal;

        /// <summary>
        /// The ID of the loan this request is for.
        /// </summary>
        private Guid loanId;

        /// <summary>
        /// The retry interval in seconds for the processor. Not considered if doing synchronous calls.
        /// </summary>
        private int? retryIntervalInSeconds;

        /// <summary>
        /// The maximum number of retries the processor will take if the document is not ready.
        /// </summary>
        private int? maxRetryCount;

        /// <summary>
        /// Whether the processor will reissue the request if the response is not ready. Not considered for synchronous calls.
        /// </summary>
        private bool? shouldReissueOnResponseNotReady;

        /// <summary>
        /// Initializes a new instance of the <see cref="CreditReportRequestHandler"/> class.
        /// </summary>
        /// <param name="requestData">The request data.</param>
        /// <param name="principal">The principal of the user making the request.</param>
        /// <param name="shouldReissueOnResponseNotReady">Whether the processor should reissue non-ready results.</param>
        /// <param name="retryIntervalInSeconds">The retry interval in seconds for the processor.</param>
        /// <param name="maxRetryCount">The maximum number of retry counts the processor will take if the document is not ready, if we need to reissue on non-ready results.</param>
        /// <param name="loanId">The ID of the loan this reposrt is being ordered for.</param>
        public CreditReportRequestHandler(CreditRequestData requestData, AbstractUserPrincipal principal, bool shouldReissueOnResponseNotReady, int retryIntervalInSeconds, int? maxRetryCount, Guid loanId)
        {
            this.creditRequestData = requestData;
            this.retryIntervalInSeconds = retryIntervalInSeconds;
            this.shouldReissueOnResponseNotReady = shouldReissueOnResponseNotReady;
            this.maxRetryCount = maxRetryCount;
            this.principal = principal;
            this.loanId = loanId;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CreditReportRequestHandler"/> class. Meant for synchronous calls.
        /// </summary>
        /// <param name="requestData">The request data.</param>
        /// <param name="principal">The principal of the user making the request.</param>
        public CreditReportRequestHandler(CreditRequestData requestData, AbstractUserPrincipal principal)
        {
            this.creditRequestData = requestData;
            this.principal = principal;
        }

        /// <summary>
        /// Checks the processor for results to the rerquest.
        /// </summary>
        /// <param name="publicJobId">The public job id.</param>
        /// <returns>The request result.</returns>
        public static CreditReportRequestResult CheckForRequestResults(Guid publicJobId)
        {
            var jobResult = CreditReportRequestJob.CheckJob(publicJobId);
            switch (jobResult.ProcessingStatus)
            {
                case ProcessingStatus.Completed:
                    var rawResponse = jobResult.RawResponse;
                    var creditRequestData = jobResult.CreditRequestData;
                    var usedPrincipal = jobResult.Principal;

                    var transformedCreditRequestData = CreditReportUtilities.TransformCreditRequestData(creditRequestData, usedPrincipal);
                    var creditResponseData = CreditReportUtilities.TransformRawResponseXml(transformedCreditRequestData, rawResponse);

                    return CreditReportRequestResult.CreateCompletedResult(creditRequestData, creditResponseData, rawResponse);
                case ProcessingStatus.Incomplete:
                case ProcessingStatus.Processing:
                    return CreditReportRequestResult.CreateProcessingResult(publicJobId);
                case ProcessingStatus.Invalid:
                    return CreditReportRequestResult.CreateErrorResult(jobResult.ErrorMessages);
                default:
                    throw new UnhandledEnumException(jobResult.ProcessingStatus);
            }
        }

        /// <summary>
        /// Submits the request synchronously, aka not through the processor.
        /// </summary>
        /// <returns>Completed results if the request went through. Error otherwise. Note that the response may still be null or an error response even if the request is completed.</returns>
        public CreditReportRequestResult SubmitSynchronously()
        {
            if (this.creditRequestData == null)
            {
                return CreditReportRequestResult.CreateErrorResult(new List<string>() { "Please specify credit request data." });
            }

            try
            {
                string rawResponse = string.Empty;
                var creditResult = CreditReportServer.OrderCreditReport(this.creditRequestData, this.principal, out rawResponse);
                return CreditReportRequestResult.CreateCompletedResult(this.creditRequestData, creditResult, rawResponse);
            }
            catch (OrderCreditException exc)
            {
                return CreditReportRequestResult.CreateErrorResult(new List<string>() { exc.UserMessage });
            }
        }

        /// <summary>
        /// Submits the request to the processor. 
        /// </summary>
        /// <returns>A processing result once the job has been added to the queue. Error otherwise.</returns>
        public CreditReportRequestResult SubmitToProcessor()
        {
            if (!this.retryIntervalInSeconds.HasValue || !this.shouldReissueOnResponseNotReady.HasValue || (this.shouldReissueOnResponseNotReady.Value && !this.maxRetryCount.HasValue))
            {
                Tools.LogError($"pollingIntervalInSeconds: {this.retryIntervalInSeconds}. shouldReissueOnResponseNotready: {this.shouldReissueOnResponseNotReady}. maxRetryCount: {this.maxRetryCount}");
                return CreditReportRequestResult.CreateErrorResult(new List<string>() { "Invalid data specified." });
            }

            if (this.creditRequestData == null)
            {
                return CreditReportRequestResult.CreateErrorResult(new List<string>() { "Credit report data not specified." });
            }

            var publicJobId = CreditReportRequestJob.AddCreditReportRequestJob(this.creditRequestData, this.principal, this.retryIntervalInSeconds.Value, this.shouldReissueOnResponseNotReady.Value, this.maxRetryCount, this.loanId);
            return CreditReportRequestResult.CreateProcessingResult(publicJobId);
        }
    }
}
