using System;
using System.Net;
using System.Xml;
using LendersOffice.CreditReport.Mismo;

namespace LendersOffice.CreditReport.SharperLending
{
	public class SharperLending_CreditReportRequest : LendersOffice.CreditReport.Mismo2_3.Mismo2_3CreditReportRequest
	{
		private const string BETA_URL = "https://beta.xpertonline.net/interface/PriceMyLoanMISMO231.asp";
		private const string URL = "https://www.xpertonline.net/interface/PriceMyLoanMISMO231.asp";

		protected override string MISMOVersionID 
		{
			get { return "2.3.1"; }
		}
		
		//url = provider id
		public SharperLending_CreditReportRequest(string url) : base(url)
		{
			this.ReceivingPartyName = url;
		}

		public override string Url 
		{
			get 
			{
				if (LoginInfo.AccountIdentifier == "ILS" && LoginInfo.UserName == "test") 
				{
					return BETA_URL;
				} 
				else 
				{
					return URL;
				}
			}
		}

		public override ICreditReportResponse CreateResponse(XmlDocument doc) 
		{
			return new SharperLending_CreditReportResponse(doc);
		}

		public override void SetupAuthentication(HttpWebRequest webRequest) 
		{
			webRequest.ContentType = "application/xml";
        }
	}
}