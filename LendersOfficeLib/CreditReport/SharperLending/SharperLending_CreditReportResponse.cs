using System;
using System.Xml;
using DataAccess;

namespace LendersOffice.CreditReport.SharperLending
{
	public class SharperLending_CreditReportResponse : LendersOffice.CreditReport.ICreditReportResponse
	{
		private string		m_reportID;
		private string		m_status;
		private bool		m_hasError;
		private bool		m_isReady;
		protected string	m_errorMessage;
		private string		m_rawXmlResponse = "";

		public SharperLending_CreditReportResponse(XmlDocument doc)
		{
			if (null != doc)
				Parse(doc);
		}

		protected virtual string DetermineErrorStatus(string code) 
		{
			return "ERROR"; // Generic error status
		}

		private void Parse(XmlDocument doc) 
		{
			// 1/2/2007 nw - OPM 9213 - SARMA response error detection needs to work a little differently
			XmlElement oCreditResponseElement = (XmlElement) doc.SelectSingleNode("//RESPONSE_GROUP/RESPONSE/RESPONSE_DATA/CREDIT_RESPONSE");
			if (null != oCreditResponseElement && oCreditResponseElement.GetAttribute("CreditReportType").ToUpper() != "ERROR")
			{
				// No error occurs.
				m_reportID = oCreditResponseElement.GetAttribute("CreditReportIdentifier");
				m_isReady = true;
				m_status = "READY"; 
			}
			else
			{
				XmlElement errorElement = (XmlElement) doc.SelectSingleNode("//RESPONSE_GROUP/RESPONSE/RESPONSE_DATA/CREDIT_RESPONSE/CREDIT_ERROR_MESSAGE");
				if (null != errorElement) 
				{
					m_isReady = false;
					m_hasError = true;
					m_status = "ERROR";

					XmlElement errorTextElement = (XmlElement) errorElement.SelectSingleNode("_Text");
					if (null != errorTextElement) 
					{
						m_errorMessage = errorTextElement.InnerText;
					} 
					else 
					{
						// 12/29/2003 dd - Generic error message since I can't determine from credit response.
						m_errorMessage = "Error while ordering report.";
					}
					m_errorMessage += "<br> If you are unable to login for credit after several tries, please contact your Credit Reporting Agency for assistance.";
				}
				else
				{
					throw new DataAccess.CBaseException("Error while ordering report", "SharperLending response indicates an error, but no error element is present");
				}	
			}
			if (m_isReady) 
				m_rawXmlResponse = doc.InnerXml;
		}

		#region Implementation of ICreditReportResponse
		public string ReportID 
		{
			get { return m_reportID; }
		}
		public string Status 
		{
			get { return m_status; }
		}

		public bool HasError 
		{
			get { return m_hasError; }
		}
		public string ErrorMessage 
		{
			get { return m_errorMessage; }
		}

		public bool IsReady 
		{
			get { return m_isReady; }
		}

		public string RawXmlResponse 
		{
			get { return m_rawXmlResponse; }
        }

        public string BrandedProductName { get; }
        #endregion
    }
}