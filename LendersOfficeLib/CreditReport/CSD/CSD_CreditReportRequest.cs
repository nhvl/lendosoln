using System;
using System.Net;
using System.Xml;
using LendersOffice.CreditReport.Mismo;
using DataAccess;

namespace LendersOffice.CreditReport.CSD
{
	public class CSD_CreditReportRequest : LendersOffice.CreditReport.Mismo2_3.Mismo2_3CreditReportRequest
	{
        private const string SUBMITTING_PARTY_NAME = "LendingQB";
		private const string URL = "https://www.ultraamps.com/uaweb/mismo";

        protected override string MISMOVersionID
        {
            get { return "2.3.1"; }
        }
		public CSD_CreditReportRequest() : base("")
		{
            this.SubmittingPartyName = SUBMITTING_PARTY_NAME;

            // 12/10/2009 dd - Add PDF and HTML.
            this.AppendOutputFormat(E_MismoOutputFormat.PDF);
            this.AppendOutputFormat(E_MismoOutputFormat.Other);
            this.PreferredResponseFormatOtherDescription = "HTML";

		}

		public override string Url 
		{
			get { return URL; }
		}

		public override ICreditReportResponse CreateResponse(XmlDocument doc) 
		{
			return new CSD_CreditReportResponse(doc);
		}

		public override void SetupAuthentication(HttpWebRequest webRequest)
		{
			webRequest.ContentType = "text/xml";
		}
	}
}