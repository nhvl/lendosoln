﻿namespace LendersOffice.CreditReport
{
    /// <summary>
    /// The status of the Credit Report request.
    /// </summary>
    public enum CreditReportRequestResultStatus
    {
        /// <summary>
        /// Generic failure status.
        /// </summary>
        Failure = 0,

        /// <summary>
        /// The request has been completed.
        /// </summary>
        Completed = 1,

        /// <summary>
        /// The request is being processed by a job processor.
        /// </summary>
        Processing = 2
    }
}
