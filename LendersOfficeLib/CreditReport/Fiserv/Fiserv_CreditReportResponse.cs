using System;
using System.Xml;

namespace LendersOffice.CreditReport.Fiserv
{
    public class Fiserv_CreditReportResponse : LendersOffice.CreditReport.Mismo2_1.Mismo2_1CreditReportResponse 
    {
        public Fiserv_CreditReportResponse(XmlDocument doc) : base(doc) 
        {
        }
    }
}

