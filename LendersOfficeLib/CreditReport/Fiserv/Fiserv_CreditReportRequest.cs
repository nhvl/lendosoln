using System;
using System.Xml;
using LendersOffice.CreditReport;
using LendersOffice.CreditReport.Mismo;

namespace LendersOffice.CreditReport.Fiserv
{
    public class Fiserv_CreditReportRequest : LendersOffice.CreditReport.Mismo2_1.Mismo2_1CreditReportRequest
    {
        private const string URL = "https://client.credstar.com/wsmismo/synchronous.aspx";
        public Fiserv_CreditReportRequest() : base("")
        {
            // 3/28/2006 dd - OPM 4489 - This will return all liabilities include $0 balance.
            AddRequestKeyValue("ReturnAllLiabilities", "YES");
        }
        public override string Url 
        {
            get { return URL; }
        }
        public override ICreditReportResponse CreateResponse(XmlDocument doc) 
        {
            return new Fiserv_CreditReportResponse(doc);
        }
    }
}
