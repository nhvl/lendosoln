using System;
using System.Collections;

using DataAccess;
namespace LendersOffice.CreditReport
{
	public class CreditReportProxy : ICreditReport
	{
        #region Constant string for category grouping
		public const string Category_NEW_DEBUGGING = "AAA";
		public const string Category_BankAegis = "BankAegis";
		public const string Category_BankDlj = "BankDlj";
		public const string Category_BankImpact = "BankImpact";
		public const string Category_Generic_Chargeoff = "GENERIC - CHARGEOFF";
		public const string Category_Generic_Collection = "GENERIC - COLLECTION";
		public const string Category_Generic_Foreclosure = "GENERIC - FORECLOSURE";
		public const string Category_Generic_Judgment = "GENERIC - JUDGMENT";
		public const string Category_Generic_Late = "GENERIC - LATE";
		public const string Category_Generic_Misc = "GENERIC - MISC";
		public const string Category_Generic_TaxLien = "GENERIC - TAX LIEN";
		public const string Category_Generic_TradeCount = "GENERIC - TRADE COUNT";
		public const string Category_Generic_Derog = "GENERIC - DEROG";
		public const string Category_NewBkMethods = "NEW BK METHODS";
		public const string Category_OldMethods = "OLD METHODS";

        #endregion


		private bool m_isParse = false;
		private ICreditReport m_creditReportImpl = null;


		public CreditReportProxy(ICreditReport creditReport) 
		{
			m_creditReportImpl = creditReport;
		}
		/// <summary>
		/// Invoke this method before calling m_creditReportImpl to ensure the object is parse correctly.
		/// </summary>
		private void Initialize() 
		{
			if (!m_isParse) 
			{
				m_creditReportImpl.Parse();
				m_isParse = true;
			}

		}

		public void Invalidate() 
		{
			m_isParse = false;
		}

		public bool IsOptimisticDefault
		{
			get
			{
				ICreditReport obj = m_creditReportImpl;
				if( null == obj )
					return false;

				return obj is CCreditReportWithOptimisticDefault;
			}
		}
        public E_aBorrowerCreditModeT aBorrowerCreditModeT
        {
            get { return m_creditReportImpl.aBorrowerCreditModeT; }
            set { m_creditReportImpl.aBorrowerCreditModeT = value; }
        }
        public bool IsDebug 
        {
            get { return m_creditReportImpl.IsDebug; }
            set { m_creditReportImpl.IsDebug = value; }
        }

        public ArrayList GetTagItems(string methodName) 
        {
            return m_creditReportImpl.GetTagItems(methodName);
        }
        public string GetLiabilityXml(string id) 
        {
            AbstractCreditReport abstractCreditReport = m_creditReportImpl as AbstractCreditReport;
            if (null != abstractCreditReport) 
            {
                Initialize();
                return abstractCreditReport.GetLiabilityXml(id);
            }
            return "";
            
        }
        public string GetPublicRecordXml(string id) 
        {
            AbstractCreditReport abstractCreditReport = m_creditReportImpl as AbstractCreditReport;
            if (null != abstractCreditReport) 
            {
                Initialize();
                return abstractCreditReport.GetPublicRecordXml(id);
            }
            return "";
        }

		public AbstractCreditReport InnerCreditReport 
		{
			get { return m_creditReportImpl as AbstractCreditReport; } 
		}

        #region BankAegis

        /// <summary>
        /// Text: No derog in past 15 months, including 30 day accts, BK, judgments, chargeoffs, CCC 
        /// </summary>
        /// <param name="nMonths"></param>
        /// <returns></returns>
        [CreditDebug(Category_Generic_Derog)]        
        public int HasDerogCountEffectiveWithin( int nMonths ) 
        {
            Initialize();
            return m_creditReportImpl.HasDerogCountEffectiveWithin( nMonths );
        }
		
        /// <summary>
        /// TradesCountNonMajorDerog would include all trades that haven't been majorly derogatory, i.e no BK, chargoffs, collections, CCC, etc.  Would ignore lates.
        /// </summary>
        [ CreditDebug( Category_Generic_Derog ) ]        
        public int TradesCountNonMajorDerog  
        { 
            get 
            {
                Initialize();
                return m_creditReportImpl.TradesCountNonMajorDerog; 
            }
        }

        /// <summary>
        /// Text: No derog in past 15 months BK, judgments, chargeoffs, CCC, but this one is not consider lates.
        /// </summary>
        /// <param name="nMonths"></param>
        /// <returns></returns>
        [CreditDebug(Category_Generic_Derog)]        
        public int HasMajorDerogCountEffectiveWithin( int nMonths ) 
        {
            Initialize();
            return m_creditReportImpl.HasMajorDerogCountEffectiveWithin( nMonths );
        }

        /// <summary>
        /// Text: No judgments or tax Liens past 24 months
        /// </summary>
        /// <param name="nMonths"></param>
        /// <returns></returns>
        [CreditDebug(Category_BankAegis)]        
        public int BankAegisHasJudgmentOrTaxLienWithin( int nMonths ) 
        {
            Initialize();
            return m_creditReportImpl.BankAegisHasJudgmentOrTaxLienWithin( nMonths );
        }

		
		#endregion // BankAegis

		#region BankImpact

        /// <summary>
        /// Example: Minimum of 3 trades (All lates based on rolling)
        /// -> Don't know what to do with the one in the parenthesis
        /// <param name="nMonths"></param>
        /// <returns></returns>
        [CreditDebug(Category_Generic_TradeCount)]        
        public int TradesOpenCountOlderThan( int nMonths ) 
        {
            Initialize();
            return m_creditReportImpl.TradesOpenCountOlderThan( nMonths );
        }
		
		#if( OBSOLETE )


		/// <summary>
		/// Text: Older items ( before 24 months ) must be paid prior to or at closing
		/// </summary>
		/// <param name="nMonths"></param>
		/// <returns></returns>
		int BankImpactHasDerogUnpaidOlderThan( int nMonths );

		/// <summary>
		/// Text: Any open items not to exceed $500/$1000 (cumulative balance) within the last 12 months, 
		/// however must be paid prior to or at closing.
		/// -> Regardless if the item has been paid or not.
		/// </summary>
		/// <param name="nMonths"></param>
		/// <returns></returns>
		int BankImpactGetHighestBalOfDerogWithin( int nMonths );

		/// <summary>
		/// Text: Open items must be paid at or prior to closing
		/// -> Regardless if the item has been paid or not.
		/// </summary>
		/// <param name="nMonths"></param>
		/// <returns></returns>
		int BankImpactGetHighestBalDerogUnpaidWithin( int nMonths );
				#endif // OBSOLETE

        /// <summary>
        /// Text: Any medical collections with a cumulative balance over $500 requires payment prior to or at closing
        /// </summary>
        /// <param name="amount"></param>
        /// <returns></returns>
        [CreditDebug(Category_Generic_Derog)]
        public int TotBalOfMedCollectionsUnpaid 
        { 
            get 
            {
                Initialize();
                return m_creditReportImpl.TotBalOfMedCollectionsUnpaid; 
            }
        }

		// 4/17/2007 nw - OPM 12499 - New medical collection keyword
		[CreditDebug(Category_Generic_Derog)]
		public int TotUnpaidBalOfMedCollectionsWithin( int nMonths )
		{
			Initialize();
			return m_creditReportImpl.TotUnpaidBalOfMedCollectionsWithin( nMonths ); 
		}



        /// <summary>
        /// Text: No Tax Liens in the past 24/12 months
        /// </summary>
        /// <param name="nMonths"></param>
        /// <returns></returns>
        [CreditDebug(Category_BankImpact)]        
        public int BankImpactHasTaxLienWithin( int nMonths ) 
        {
            Initialize();
            return m_creditReportImpl.BankImpactHasTaxLienWithin( nMonths );
        }

		/// <summary>
		/// 7/6/2006 nw - OPM 6120 - new keyword "HasTaxLienFiledWithin"
		/// Text: No Tax Liens Filed in the past 24/12 months
		/// </summary>
		/// <param name="nMonths"></param>
		/// <returns></returns>
		[CreditDebug(Category_BankImpact)]        
		public int BankImpactHasTaxLienFiledWithin( int nMonths ) 
		{
			Initialize();
			return m_creditReportImpl.BankImpactHasTaxLienFiledWithin( nMonths );
		}


			

		#endregion // BankImpact

		#region Late Calculations
        [CreditDebug(Category_Generic_Late)]
        public int GetInstallmentLates30IntermittentProgressiveRollingCountWithin( int nMonths ) 
        {
            Initialize();
            return m_creditReportImpl.GetInstallmentLates30IntermittentProgressiveRollingCountWithin( nMonths );
        }
        [CreditDebug(Category_Generic_Late)]
        public int GetInstallmentLates30IntermittentProgressiveNonrollingCountWithin( int nMonths ) 
        {
            Initialize();
            return m_creditReportImpl.GetInstallmentLates30IntermittentProgressiveNonrollingCountWithin( nMonths );
        }
        [CreditDebug(Category_Generic_Late)]
        public int GetRevolvingLates30IntermittentProgressiveRollingCountWithin( int nMonths ) 
        {
            Initialize();
            return m_creditReportImpl.GetRevolvingLates30IntermittentProgressiveRollingCountWithin(  nMonths );
        }
        [CreditDebug(Category_Generic_Late)]
        public int GetRevolvingLates30IntermittentProgressiveNonrollingCountWithin( int nMonths ) 
        {
            Initialize();
            return m_creditReportImpl.GetRevolvingLates30IntermittentProgressiveNonrollingCountWithin(  nMonths );
        }
        [CreditDebug(Category_Generic_Late)]
        public int GetMortgageLates30IntermittentProgressiveRollingCountWithin( int nMonths ) 
        {
            Initialize();
            return m_creditReportImpl.GetMortgageLates30IntermittentProgressiveRollingCountWithin(  nMonths );
        }
		[CreditDebug(Category_Generic_Late)]
		public int GetMortgageLates60IntermittentProgressiveRollingCountWithin( int nMonths ) 
		{
			Initialize();
			return m_creditReportImpl.GetMortgageLates60IntermittentProgressiveRollingCountWithin(  nMonths );
		}

        public int GetMortgageLates90IntermittentProgressiveRollingCountWithin( int nMonths) 
        {
            Initialize();
            return m_creditReportImpl.GetMortgageLates90IntermittentProgressiveRollingCountWithin( nMonths );
        }
        [CreditDebug(Category_Generic_Late)]
        public int GetRevolvingLates60IntermittentProgressiveNonrollingCountWithin( int nMonths ) 
        {
            Initialize();
            return m_creditReportImpl.GetRevolvingLates60IntermittentProgressiveNonrollingCountWithin(  nMonths );
        }
        [CreditDebug(Category_Generic_Late)]
        public int GetRevolvingLates90IntermittentProgressiveNonrollingCountWithin( int nMonths ) 
        {
            Initialize();
            return m_creditReportImpl.GetRevolvingLates90IntermittentProgressiveNonrollingCountWithin(  nMonths );
        }
        [CreditDebug(Category_Generic_Late)]
        public int GetRevolvingLates120IntermittentProgressiveNonrollingCountWithin( int nMonths ) 
        {
            Initialize();
            return m_creditReportImpl.GetRevolvingLates120IntermittentProgressiveNonrollingCountWithin(  nMonths );
        }
        [CreditDebug(Category_Generic_Late)]
        public int GetRevolvingLates150IntermittentProgressiveNonrollingCountWithin( int nMonths ) 
        {
            Initialize();
            return m_creditReportImpl.GetRevolvingLates150IntermittentProgressiveNonrollingCountWithin(  nMonths );
        }
        [CreditDebug(Category_Generic_Late)]
        public int GetRevolvingLates60IntermittentProgressiveRollingCountWithin(  int nMonths ) 
        {
            Initialize();
            return m_creditReportImpl.GetRevolvingLates60IntermittentProgressiveRollingCountWithin(  nMonths );
        }
        [CreditDebug(Category_Generic_Late)]
        public int GetRevolvingLates90IntermittentProgressiveRollingCountWithin( int nMonths ) 
        {
            Initialize();
            return m_creditReportImpl.GetRevolvingLates90IntermittentProgressiveRollingCountWithin(  nMonths );
        }
        [CreditDebug(Category_Generic_Late)]
        public int GetRevolvingLates120IntermittentProgressiveRollingCountWithin( int nMonths ) 
        {
            Initialize();
            return m_creditReportImpl.GetRevolvingLates120IntermittentProgressiveRollingCountWithin(  nMonths );
        }
        [CreditDebug(Category_Generic_Late)]
        public int GetRevolvingLates150IntermittentProgressiveRollingCountWithin( int nMonths ) 
        {
            Initialize();
            return m_creditReportImpl.GetRevolvingLates150IntermittentProgressiveRollingCountWithin(  nMonths );
        }
        [CreditDebug(Category_Generic_Late)]
        public int GetInstallmentLates60IntermittentProgressiveNonrollingCountWithin( int nMonths ) 
        {
            Initialize();
            return m_creditReportImpl.GetInstallmentLates60IntermittentProgressiveNonrollingCountWithin( nMonths );
        }
        [CreditDebug(Category_Generic_Late)]
        public int GetInstallmentLates90IntermittentProgressiveNonrollingCountWithin( int nMonths ) 
        {
            Initialize();
            return m_creditReportImpl.GetInstallmentLates90IntermittentProgressiveNonrollingCountWithin( nMonths );
        }
        [CreditDebug(Category_Generic_Late)]
        public int GetInstallmentLates120IntermittentProgressiveNonrollingCountWithin( int nMonths ) 
        {
            Initialize();
            return m_creditReportImpl.GetInstallmentLates120IntermittentProgressiveNonrollingCountWithin( nMonths );
        }
        [CreditDebug(Category_Generic_Late)]
        public int GetInstallmentLates150IntermittentProgressiveNonrollingCountWithin( int nMonths ) 
        {
            Initialize();
            return m_creditReportImpl.GetInstallmentLates150IntermittentProgressiveNonrollingCountWithin( nMonths );
        }
        [CreditDebug(Category_Generic_Late)]
        public int GetInstallmentLates60IntermittentProgressiveRollingCountWithin( int nMonths ) 
        {
            Initialize();
            return m_creditReportImpl.GetInstallmentLates60IntermittentProgressiveRollingCountWithin( nMonths );
        }
        [CreditDebug(Category_Generic_Late)]
        public int GetInstallmentLates90IntermittentProgressiveRollingCountWithin( int nMonths ) 
        {
            Initialize();
            return m_creditReportImpl.GetInstallmentLates90IntermittentProgressiveRollingCountWithin( nMonths );
        }
        [CreditDebug(Category_Generic_Late)]
        public int GetInstallmentLates120IntermittentProgressiveRollingCountWithin( int nMonths ) 
        {
            Initialize();
            return m_creditReportImpl.GetInstallmentLates120IntermittentProgressiveRollingCountWithin( nMonths );
        }
        [CreditDebug(Category_Generic_Late)]
        public int GetInstallmentLates150IntermittentProgressiveRollingCountWithin( int nMonths ) 
        {
            Initialize();
            return m_creditReportImpl.GetInstallmentLates150IntermittentProgressiveRollingCountWithin( nMonths );
        }
        [CreditDebug(Category_Generic_Late)]
        public int GetLeaseLates30IntermittentProgressiveRollingCountWithin(int nMonths)
        {
            Initialize();
            return m_creditReportImpl.GetLeaseLates30IntermittentProgressiveRollingCountWithin(nMonths);
        }
        [CreditDebug(Category_Generic_Late)]
        public int GetLeaseLates30IntermittentProgressiveNonrollingCountWithin(int nMonths)
        {
            Initialize();
            return m_creditReportImpl.GetLeaseLates30IntermittentProgressiveNonrollingCountWithin(nMonths);
        }
        [CreditDebug(Category_Generic_Late)]
        public int GetLeaseLates60IntermittentProgressiveRollingCountWithin(int nMonths)
        {
            Initialize();
            return m_creditReportImpl.GetLeaseLates60IntermittentProgressiveRollingCountWithin(nMonths);
        }
        [CreditDebug(Category_Generic_Late)]
        public int GetLeaseLates60IntermittentProgressiveNonrollingCountWithin(int nMonths)
        {
            Initialize();
            return m_creditReportImpl.GetLeaseLates60IntermittentProgressiveNonrollingCountWithin(nMonths);
        }
        [CreditDebug(Category_Generic_Late)]
        public int GetLeaseLates90IntermittentProgressiveRollingCountWithin(int nMonths)
        {
            Initialize();
            return m_creditReportImpl.GetLeaseLates90IntermittentProgressiveRollingCountWithin(nMonths);
        }
        [CreditDebug(Category_Generic_Late)]
        public int GetLeaseLates90IntermittentProgressiveNonrollingCountWithin(int nMonths)
        {
            Initialize();
            return m_creditReportImpl.GetLeaseLates90IntermittentProgressiveNonrollingCountWithin(nMonths);
        }
        [CreditDebug(Category_Generic_Late)]
        public int GetHelocLates30IntermittentProgressiveRollingCountWithin(int nMonths)
        {
            Initialize();
            return m_creditReportImpl.GetHelocLates30IntermittentProgressiveRollingCountWithin(nMonths);
        }
        [CreditDebug(Category_Generic_Late)]
        public int GetHelocLates30IntermittentProgressiveNonrollingCountWithin(int nMonths)
        {
            Initialize();
            return m_creditReportImpl.GetHelocLates30IntermittentProgressiveNonrollingCountWithin(nMonths);
        }
        [CreditDebug(Category_Generic_Late)]
        public int GetHelocLates60IntermittentProgressiveRollingCountWithin(int nMonths)
        {
            Initialize();
            return m_creditReportImpl.GetHelocLates60IntermittentProgressiveRollingCountWithin(nMonths);
        }
        [CreditDebug(Category_Generic_Late)]
        public int GetHelocLates60IntermittentProgressiveNonrollingCountWithin(int nMonths)
        {
            Initialize();
            return m_creditReportImpl.GetHelocLates60IntermittentProgressiveNonrollingCountWithin(nMonths);
        }
        [CreditDebug(Category_Generic_Late)]
        public int GetHelocLates90IntermittentProgressiveRollingCountWithin(int nMonths)
        {
            Initialize();
            return m_creditReportImpl.GetHelocLates90IntermittentProgressiveRollingCountWithin(nMonths);
        }
        [CreditDebug(Category_Generic_Late)]
        public int GetHelocLates90IntermittentProgressiveNonrollingCountWithin(int nMonths)
        {
            Initialize();
            return m_creditReportImpl.GetHelocLates90IntermittentProgressiveNonrollingCountWithin( nMonths );
        }
        #endregion


        /// <summary>
        /// 6/15/2006 - nw - OPM 4822.  Returns 1 if tradeline remark indicates that foreclosure was started.  
        /// Parameter nMonths isn't used yet.
        /// </summary>
        /// <param name="nMonths"></param>
        /// <returns></returns>
        [CreditDebug(Category_Generic_Derog)]        
		public int MightHaveUndated120Within( int nMonths )
		{
			Initialize();
			return m_creditReportImpl.MightHaveUndated120Within( nMonths );
		}

        /// <summary>
        /// Returns the number of short sales that occured within n Months.
        /// </summary>
        /// <param name="months">Number of months short sales must be within to be counted.</param>
        /// <returns>The number of short sales that occured within the given number of months.</returns>
        /// <remarks>OPM 24318.</remarks>
        [CreditDebug(Category_Generic_Misc)]  
        public int GetShortSales(int months)
        {
            Initialize();
            return m_creditReportImpl.GetShortSales(months);
        }

        /// <summary>
        /// Get the total number of late payments of all mortgage account in the past n months.
        /// </summary>
        /// <param name="month"></param>
        /// <returns></returns>
        [CreditDebug(Category_OldMethods)]        
        public int GetMortgageLates(int month) 
        {
            Initialize();
            return m_creditReportImpl.GetMortgageLates(month);
        }

        /// <summary>
        /// Get the total number of 30 days late payments of all mortgage account in the past n months.
        /// </summary>
        /// <param name="month"></param>
        /// <returns></returns>
        [CreditDebug(Category_OldMethods)]        
        public int GetMortgage30Lates(int month) 
        {
            Initialize();
            return m_creditReportImpl.GetMortgage30Lates(month);
        }

        /// <summary>
        /// Get the total number of 60 days late payments of all mortgage account in the past n months.
        /// </summary>
        /// <param name="month"></param>
        /// <returns></returns>
        [CreditDebug(Category_OldMethods)]        
        public int GetMortgage60Lates(int month) 
        {
            Initialize();
            return m_creditReportImpl.GetMortgage60Lates(month);
        }

        /// <summary>
        /// Get the total number of 90+ days late payments of all mortgage account in the past n months.
        /// </summary>
        /// <param name="month"></param>
        /// <returns></returns>
        [CreditDebug(Category_OldMethods)]        
        public int GetMortgage90PlusLates(int month) 
        {
            Initialize();
            return m_creditReportImpl.GetMortgage90PlusLates(month);
        }

        /// <summary>
        /// Get the total number of 90 days late payments of all mortgage account in the past n months.
        /// </summary>
        /// <param name="month"></param>
        /// <returns></returns>
        [CreditDebug(Category_OldMethods)]        
        public int GetMortgage90Lates(int month) 
        {
            Initialize();
            return m_creditReportImpl.GetMortgage90Lates(month);
        }

        /// <summary>
        /// Get the total number of 120 days late payments of all mortgage account in the past n months.
        /// </summary>
        /// <param name="month"></param>
        /// <returns></returns>
        [CreditDebug(Category_OldMethods)]        
        public int GetMortgage120Lates(int month) 
        {
            Initialize();
            return m_creditReportImpl.GetMortgage120Lates(month);
        }

        /// <summary>
        /// Get the total number of 150+ days late payments of all mortgage account in the past n months.
        /// </summary>
        /// <param name="month"></param>
        /// <returns></returns>
        [CreditDebug(Category_OldMethods)]        
        public int GetMortgage150PlusLates(int month) 
        {
            Initialize();
            return m_creditReportImpl.GetMortgage150PlusLates(month);
        }

        /// <summary>
        /// Get the total number of 30 days late payments of all installment account in the past n months.
        /// </summary>
        /// <param name="month"></param>
        /// <returns></returns>
        [CreditDebug(Category_OldMethods)]        
        public int GetInstallment30Lates(int month) 
        {
            Initialize();
            return m_creditReportImpl.GetInstallment30Lates(month);
        }

        /// <summary>
        /// Get the total number of 60 days late payments of all installment account in the past n months.
        /// </summary>
        /// <param name="month"></param>
        /// <returns></returns>
        [CreditDebug(Category_OldMethods)]       
        public int GetInstallment60Lates(int month) 
        {
            Initialize();
            return m_creditReportImpl.GetInstallment60Lates(month);
        }

        /// <summary>
        /// Get the total number of 90+ days late payments of all installment account in the past n months.
        /// </summary>
        /// <param name="month"></param>
        /// <returns></returns>
        [CreditDebug(Category_OldMethods)]        
        public int GetInstallment90PlusLates(int month) 
        {
            Initialize();
            return m_creditReportImpl.GetInstallment90PlusLates(month);
        }

        /// <summary>
        /// Get the total number of 30 days late payments of all revolving account in the past n months.
        /// </summary>
        /// <param name="month"></param>
        /// <returns></returns>
        [CreditDebug(Category_OldMethods)]        
        public int GetRevolving30Lates(int month) 
        {
            Initialize();
            return m_creditReportImpl.GetRevolving30Lates(month);
        }

        /// <summary>
        /// Get the total number of 60 days late payments of all revolving account in the past n months.
        /// </summary>
        /// <param name="month"></param>
        /// <returns></returns>
        [CreditDebug(Category_OldMethods)]        
        public int GetRevolving60Lates(int month) 
        {
            Initialize();
            return m_creditReportImpl.GetRevolving60Lates(month);
        }

        /// <summary>
        /// Get the total number of 90+ days late payments of all revolving account in the past n months.
        /// </summary>
        /// <param name="month"></param>
        /// <returns></returns>
        [CreditDebug(Category_OldMethods)]        
        public int GetRevolving90PlusLates(int month) 
        {
            Initialize();
            return m_creditReportImpl.GetRevolving90PlusLates(month);
        }

        /// <summary>
        /// Get the total number of late payments for the given debt type, with lateType number of days late, in the past n months.
        /// </summary>
        /// <param name="nMonths">The number of months to scan.</param>
        /// <param name="type">The debt type to check (usually Installment, Revolving, or Mortgage).</param>
        /// <param name="lateType">The type of lates to include.</param>
        /// <param name="rolling">Whether to count rolling late payments as single lates or multiple.</param>
        /// <returns>The number of lates found.</returns>
        [CreditDebug(Category_Generic_Late)]
        public int GetNumberOfLates(int nMonths, E_DebtRegularT type, E_AdverseType lateType, bool rolling)
        {
            Initialize();
            return m_creditReportImpl.GetNumberOfLates(nMonths, type, lateType, rolling);
        }

        /// <summary>
        /// Get number of account with foreclosure status '82' in the past n months
        /// </summary>
        /// <param name="month"></param>
        /// <returns></returns>
		[CreditDebug(Category_Generic_Foreclosure)]        
        public int GetForeclosure(int month) 
        {
            Initialize();
            return m_creditReportImpl.GetForeclosure(month);
        }

		/// <summary>
		/// 8/23/2006 nw - OPM 7126 - New Foreclosure Keyword, accepts 2 parameters:
		/// a)int  Within:   default is 1000, this is to specify look-back period of when the derog event occurs.
		/// b)bool Count120: default is true, 120 means 120+ late.
		/// Returns the number of mortgage tradelines that satisfy the parameters.
		/// </summary>
		/// <param name="parameters"></param>
		/// <returns></returns>
		[CreditDebug(Category_Generic_Foreclosure)]        
		public int FcCountX(string parameters)
		{
			Initialize();
			return m_creditReportImpl.FcCountX(parameters);
		}

		/// <summary>
		/// 8/25/2006 nw - OPM 7126 - New Foreclosure Keyword HasFcX, accepts 3 parameters:
		/// a)int  Within:   default is 1000, this is to specify look-back period of when the derog event occurs.
		/// b)bool CountNOD: default is true, NOD stands Notice Of Default.
		/// c)bool Count120: default is true, 120 means 120+ late.
		/// RETURNS 1 if true, 0 if false.
		/// </summary>
		/// <param name="parameters"></param>
		/// <returns></returns>
		[CreditDebug(Category_Generic_Foreclosure)]        
		public int HasFcX(string parameters)
		{
			Initialize();
			return m_creditReportImpl.HasFcX(parameters);
		}

        /// <summary>
        /// return 1 if there is a bk (either discharged or filed) in the past n months; else return 0
        /// </summary>
        /// <param name="month"></param>
        /// <returns></returns>
        [CreditDebug(Category_OldMethods)]        
        public int GetBankruptcy(int month) 
        {
            Initialize();
            return m_creditReportImpl.GetBankruptcy(month);
        }

        /// <summary>
        /// Get number of accounts with charge off status 90 in the past n months.
        /// </summary>
        /// <param name="nMonths"></param>
        /// <returns></returns>
        [CreditDebug(Category_OldMethods)]        
        public int GetChargeOff(int nMonths ) 
        {
            Initialize();
            return m_creditReportImpl.GetChargeOff(nMonths );
        }

        /// <summary>
        /// Return total of all charge-offs, collection tradelines.
        /// </summary>
        /// <returns></returns>
        [CreditDebug(Category_OldMethods)]        
        public decimal DerogatoryAmountTotal 
        { 
            get 
            {
                Initialize();
                return m_creditReportImpl.DerogatoryAmountTotal; 
            }
        }

        /// <summary>
        /// OPM 2083 - Detect Consumer Credit Counseling Account
        /// </summary>
		[CreditDebug(Category_Generic_Misc)]        
        public bool IsInCreditCounseling 
        { 
            get 
            {
                Initialize();
                return m_creditReportImpl.IsInCreditCounseling; 
            }
        }


		#region NEW BK METHODS
		[CreditDebug(Category_NewBkMethods)] //av opm 25329
		public bool HasMortgageIncludedInBKWithin( int nMonths ) 
		{
			Initialize(); 
			return m_creditReportImpl.HasMortgageIncludedInBKWithin( nMonths );
		}
		[CreditDebug(Category_NewBkMethods)]
        public E_BkMultipleT BkMultipleType 
        { 
            get 
            {
                Initialize();
                return m_creditReportImpl.BkMultipleType; 
            }
        }
        [CreditDebug(Category_NewBkMethods)]
        public int HasDischargedBkWithin( int nMonths ) 
        {
            Initialize();
            return m_creditReportImpl.HasDischargedBkWithin( nMonths );
        }
        [CreditDebug(Category_NewBkMethods)]
        public int HasDischarged7BkWithin( int nMonths ) 
        {
            Initialize();
            return m_creditReportImpl.HasDischarged7BkWithin( nMonths );
        }
        [CreditDebug(Category_NewBkMethods)]
        public int HasDischarged11BkWithin( int nMonths ) 
        {
            Initialize();
            return m_creditReportImpl.HasDischarged11BkWithin( nMonths );
        }
        [CreditDebug(Category_NewBkMethods)]
        public int HasDischarged12BkWithin( int nMonths ) 
        {
            Initialize();
            return m_creditReportImpl.HasDischarged12BkWithin( nMonths );
        }
        [CreditDebug(Category_NewBkMethods)]
        public int HasDischarged13BkWithin( int nMonths ) 
        {
            Initialize();
            return m_creditReportImpl.HasDischarged13BkWithin( nMonths );
        }
        // This will allow us to generate stip for further review
        [CreditDebug(Category_NewBkMethods)]
        public int HasDischargedUnknownTypeBkWithin( int nMonths ) 
        {
            Initialize();
            return m_creditReportImpl.HasDischargedUnknownTypeBkWithin( nMonths );
        }

		// 6/30/2006 nw - OPM 5752 - New BK Keywords
		[CreditDebug(Category_NewBkMethods)]
		public int HasClosedBkWithin( int nMonths ) 
		{
			Initialize();
			return m_creditReportImpl.HasClosedBkWithin( nMonths );
		}
		[CreditDebug(Category_NewBkMethods)]
		public int HasClosed7BkWithin( int nMonths ) 
		{
			Initialize();
			return m_creditReportImpl.HasClosed7BkWithin( nMonths );
		}
		[CreditDebug(Category_NewBkMethods)]
		public int HasClosed11BkWithin( int nMonths ) 
		{
			Initialize();
			return m_creditReportImpl.HasClosed11BkWithin( nMonths );
		}
		[CreditDebug(Category_NewBkMethods)]
		public int HasClosed12BkWithin( int nMonths ) 
		{
			Initialize();
			return m_creditReportImpl.HasClosed12BkWithin( nMonths );
		}
		[CreditDebug(Category_NewBkMethods)]
		public int HasClosed13BkWithin( int nMonths ) 
		{
			Initialize();
			return m_creditReportImpl.HasClosed13BkWithin( nMonths );
		}
		[CreditDebug(Category_NewBkMethods)]
		public int HasClosedUnknownTypeBkWithin( int nMonths ) 
		{
			Initialize();
			return m_creditReportImpl.HasClosedUnknownTypeBkWithin( nMonths );
		}

		[CreditDebug(Category_NewBkMethods)]
		public int HasDismissedBkWithin( int nMonths ) 
		{
			Initialize();
			return m_creditReportImpl.HasDismissedBkWithin( nMonths );
		}
		[CreditDebug(Category_NewBkMethods)]
		public int HasDismissed7BkWithin( int nMonths ) 
		{
			Initialize();
			return m_creditReportImpl.HasDismissed7BkWithin( nMonths );
		}
		[CreditDebug(Category_NewBkMethods)]
		public int HasDismissed11BkWithin( int nMonths ) 
		{
			Initialize();
			return m_creditReportImpl.HasDismissed11BkWithin( nMonths );
		}
		[CreditDebug(Category_NewBkMethods)]
		public int HasDismissed12BkWithin( int nMonths ) 
		{
			Initialize();
			return m_creditReportImpl.HasDismissed12BkWithin( nMonths );
		}
		[CreditDebug(Category_NewBkMethods)]
		public int HasDismissed13BkWithin( int nMonths ) 
		{
			Initialize();
			return m_creditReportImpl.HasDismissed13BkWithin( nMonths );
		}
		[CreditDebug(Category_NewBkMethods)]
		public int HasDismissedUnknownTypeBkWithin( int nMonths ) 
		{
			Initialize();
			return m_creditReportImpl.HasDismissedUnknownTypeBkWithin( nMonths );
		}

		[CreditDebug(Category_NewBkMethods)]
		public int HasDischBkWithin( int nMonths ) 
		{
			Initialize();
			return m_creditReportImpl.HasDischBkWithin( nMonths );
		}
		[CreditDebug(Category_NewBkMethods)]
		public int HasDisch7BkWithin( int nMonths ) 
		{
			Initialize();
			return m_creditReportImpl.HasDisch7BkWithin( nMonths );
		}
		[CreditDebug(Category_NewBkMethods)]
		public int HasDisch11BkWithin( int nMonths ) 
		{
			Initialize();
			return m_creditReportImpl.HasDisch11BkWithin( nMonths );
		}
		[CreditDebug(Category_NewBkMethods)]
		public int HasDisch12BkWithin( int nMonths ) 
		{
			Initialize();
			return m_creditReportImpl.HasDisch12BkWithin( nMonths );
		}
		[CreditDebug(Category_NewBkMethods)]
		public int HasDisch13BkWithin( int nMonths ) 
		{
			Initialize();
			return m_creditReportImpl.HasDisch13BkWithin( nMonths );
		}
		[CreditDebug(Category_NewBkMethods)]
		public int HasDischUnknownTypeBkWithin( int nMonths ) 
		{
			Initialize();
			return m_creditReportImpl.HasDischUnknownTypeBkWithin( nMonths );
		}
		// End OPM 5752 - New BK Keywords

        [CreditDebug(Category_NewBkMethods)]
        public int HasBkFiledWithin( int nMonths ) 
        {
            Initialize();
            return m_creditReportImpl.HasBkFiledWithin( nMonths );
        }

        [CreditDebug(Category_NewBkMethods)]
        public int Has7BkFiledWithin( int nMonths ) 
        {
            Initialize();
            return m_creditReportImpl.Has7BkFiledWithin( nMonths );
        }
        [CreditDebug(Category_NewBkMethods)]
        public int Has11BkFiledWithin( int nMonths ) 
        {
            Initialize();
            return m_creditReportImpl.Has11BkFiledWithin( nMonths );
        }
        [CreditDebug(Category_NewBkMethods)]
        public int Has12BkFiledWithin( int nMonths ) 
        {
            Initialize();
            return m_creditReportImpl.Has12BkFiledWithin( nMonths );
        }
        [CreditDebug(Category_NewBkMethods)]
        public int Has13BkFiledWithin( int nMonths ) 
        {
            Initialize();
            return m_creditReportImpl.Has13BkFiledWithin( nMonths );
        }
        [CreditDebug(Category_NewBkMethods)]
        public int HasUnknownTypeBkFiledWithin( int nMonths ) 
        {
            Initialize();
            return m_creditReportImpl.HasUnknownTypeBkFiledWithin( nMonths );
        }
        [CreditDebug(Category_NewBkMethods)]
        public bool HasUnpaidAndNondischargedAnyBk 
        { 
            get 
            {
                Initialize();
                return m_creditReportImpl.HasUnpaidAndNondischargedAnyBk; 
            }
        }
        [CreditDebug(Category_NewBkMethods)]
        public bool HasUnpaidAndNondischarged7Bk 
        { 
            get 
            {
                Initialize();
                return m_creditReportImpl.HasUnpaidAndNondischarged7Bk; 
            }
        }
        [CreditDebug(Category_NewBkMethods)]
        public bool HasUnpaidAndNondischarged11Bk 
        { 
            get 
            {
                Initialize();
                return m_creditReportImpl.HasUnpaidAndNondischarged11Bk; 
            }
        }

        [CreditDebug(Category_NewBkMethods)]
        public bool HasUnpaidAndNondischarged12Bk 
        { 
            get 
            {
                Initialize();
                return m_creditReportImpl.HasUnpaidAndNondischarged12Bk; 
            }
        }

        [CreditDebug(Category_NewBkMethods)]
        public bool HasUnpaidAndNondischarged13Bk 
        { 
            get 
            {
                Initialize();
                return m_creditReportImpl.HasUnpaidAndNondischarged13Bk; 
            }
        }

        [CreditDebug(Category_NewBkMethods)]
        public bool HasUnpaidAndNondischargedUnknownTypeBk 
        { 
            get 
            {
                Initialize();
                return m_creditReportImpl.HasUnpaidAndNondischargedUnknownTypeBk; 
            }
        }

        // These methods are used for custom reporting purpose only
        public CDateTime DischargedDOfLastBkOfType( E_BkType type ) 
        {
            Initialize();
            return m_creditReportImpl.DischargedDOfLastBkOfType( type );
        }
        public CDateTime FileDOfLastBkOfType( E_BkType type ) 
        {
            Initialize();
            return m_creditReportImpl.FileDOfLastBkOfType( type );
        }
		
		#endregion // NEW BK METHODS



        [CreditDebug(Category_Generic_TradeCount)]
        public int CreditReportAge 
        { 
            get 
            {
                Initialize();
                return m_creditReportImpl.CreditReportAge; 
            }
        }
		public DateTime CreditReportFirstIssuedDate
		{
			get
			{
				Initialize();
				return m_creditReportImpl.CreditReportFirstIssuedDate;
			}
		}
        public DateTime CreditReportLastUpdatedDate
        {
            get
            {
                Initialize();
                return m_creditReportImpl.CreditReportLastUpdatedDate;
            }
        }
        public CreditScore BorrowerScore 
		{ 
			get 
			{
				Initialize();
				return m_creditReportImpl.BorrowerScore; 
			}
		}
        public CreditScore CoborrowerScore 
        { 
            get 
            {
                Initialize();
                return m_creditReportImpl.CoborrowerScore; 
            }
        }

        public int RepresentativeScore
        {
            get
            {
                Initialize();
                return m_creditReportImpl.RepresentativeScore; 
            }
        }
        public BorrowerInfo BorrowerInfo 
        { 
            get 
            {
                Initialize();
                return m_creditReportImpl.BorrowerInfo; 
            }
        }
        public BorrowerInfo CoborrowerInfo 
        { 
            get 
            {
                Initialize();
                return m_creditReportImpl.CoborrowerInfo; 
            }
        }

        public CreditBureauInformation ExperianContactInformation 
        { 
            get 
            {
                Initialize();
                return m_creditReportImpl.ExperianContactInformation; 
            }
        }
        public CreditBureauInformation TransUnionContactInformation 
        { 
            get 
            {
                Initialize();
                return m_creditReportImpl.TransUnionContactInformation; 
            }
        }
        public CreditBureauInformation EquifaxContactInformation 
        { 
            get 
            {
                Initialize();
                return m_creditReportImpl.EquifaxContactInformation; 
            }
        }

        /// <summary>
        /// Return list of liabilies for this report.
        /// </summary>
        public ArrayList AllLiabilities 
        { 
            get 
            {
                Initialize();
                return m_creditReportImpl.AllLiabilities; 
            }
        }

        public ArrayList LiabilitiesExcludeFromUnderwriting 
        {
            get 
            {
                Initialize();
                return m_creditReportImpl.LiabilitiesExcludeFromUnderwriting;
            }
        }

        public ArrayList AllPublicRecords 
        {
            get 
            {
                Initialize();
                return m_creditReportImpl.AllPublicRecords;
            }
        }

        // 7/22/05 tn - added
       [CreditDebug(Category_Generic_TradeCount)]
        public int TradeCount 
        { 
            get 
            {
                Initialize();
                return m_creditReportImpl.TradeCount; 
            }
        }
		[CreditDebug(Category_NEW_DEBUGGING)]
        public int TradeCountX ( string parameters ) 
        {
            Initialize();
            return m_creditReportImpl.TradeCountX(parameters);
        }
		[CreditDebug(Category_NEW_DEBUGGING)]
        public int MajorDerogCountX( string parameters ) 
        {
            Initialize();
            return m_creditReportImpl.MajorDerogCountX(parameters);
        }

		[CreditDebug(Category_Generic_Misc)]
		public int HasContiguousMortgageHistory ( string parameters )
		{
			Initialize();
			return m_creditReportImpl.HasContiguousMortgageHistory(parameters);
		}

		//6/28/07 db OPM 16602 - New MaxHighCreditAmountX keyword
		[CreditDebug(Category_Generic_Misc)]
		public decimal MaxHighCreditAmountX ( string parameters )
		{
			Initialize();
			return m_creditReportImpl.MaxHighCreditAmountX(parameters);
		}

        [CreditDebug(Category_Generic_Chargeoff)]
        public int ChargeOffCountX(string parameters)
        {
            this.Initialize();
            return this.m_creditReportImpl.ChargeOffCountX(parameters);
        }

        // 11/2/2004 dd - Added
		
		public void SetLoanData(CAppBase dataApp) 
		{
			// 3/17/2006 dd - SetLoanData is a special method. Inside, Parse() method will get called.
			// Therefore, after invoke the actual SetLoanData, indicate with proxy no need to reparse.
			m_creditReportImpl.SetLoanData(dataApp);
			m_isParse = true;
		}

        // 3/11/2005 dd - Added
        public void Parse() 
        {
            m_creditReportImpl.Parse();
        }

		public string CreditRatingCodeType
		{
			get
			{
				Initialize();
				return m_creditReportImpl.CreditRatingCodeType;
			}
		}

		#region SHARING METHODS OF NEW BANKS
		
        /// <summary>
        /// Example: Minimum of 3 trades (All lates based on rolling)
        /// -> Don't know what to do with the one in the parenthesis
        /// <param name="nMonths"></param>
        /// <returns>Number of trades open nMonths ago, regardless of current status</returns>
        [CreditDebug(Category_Generic_TradeCount)]        
        public int GetTradesCountOlderThan( int nMonths ) 
        {
            Initialize();
            return m_creditReportImpl.GetTradesCountOlderThan( nMonths );
        }
        [CreditDebug(Category_Generic_TradeCount)]
        public int InstallTradeCountOlderThan( int nMonths ) 
        {
            Initialize();
            return m_creditReportImpl.InstallTradeCountOlderThan( nMonths );
        }
        [CreditDebug(Category_Generic_TradeCount)]
        public int RevolveTradeCountOlderThan( int nMonths ) 
        {
            Initialize();
            return m_creditReportImpl.RevolveTradeCountOlderThan( nMonths );
        }

		[CreditDebug(Category_Generic_Misc)]
        public bool Is1stTimeHomeBuyer 
        { 
            get 
            {
                Initialize();
                return m_creditReportImpl.Is1stTimeHomeBuyer; 
            }
        }
        public bool Is1stTimeHomeBuyerWithin( int nMonths ) 
        {
            Initialize();
            return m_creditReportImpl.Is1stTimeHomeBuyerWithin(nMonths);
        }

        /// <summary>
        /// Used by banks: GMAC
        /// </summary>
        [CreditDebug(Category_Generic_TaxLien)]        
        public bool HasTaxOpenTaxLien 
        { 
            get 
            {
                Initialize();
                return m_creditReportImpl.HasTaxOpenTaxLien; 
            }
        }
		
		[CreditDebug(Category_Generic_Collection)]
        public int GetHighestUnpaidBalOfCollectionWithin( int nMonths ) 
        {
            Initialize();
            return m_creditReportImpl.GetHighestUnpaidBalOfCollectionWithin( nMonths );
        }

		[CreditDebug(Category_Generic_Judgment)]
        public int GetHighestUnpaidBalOfJudgmentWithin( int nMonhts ) 
        {
            Initialize();
            return m_creditReportImpl.GetHighestUnpaidBalOfJudgmentWithin( nMonhts );
        }
		[CreditDebug(Category_Generic_Chargeoff)]
        public int GetHighestUnpaidBalOfChargeOffWithin( int nMonths ) 
        {
            Initialize();
            return m_creditReportImpl.GetHighestUnpaidBalOfChargeOffWithin( nMonths );
        }
		[CreditDebug(Category_Generic_TaxLien)]
        public int GetHighestUnpaidBalOfTaxLienWithin( int nMonths ) 
        {
            Initialize();
            return m_creditReportImpl.GetHighestUnpaidBalOfTaxLienWithin( nMonths );
        }

		// 5/14/2007 nw - OPM 13060 - New charge off / collection keywords
		[CreditDebug( Category_Generic_Derog )]
		public int HighestUnpaidBalOfChargeOffOrCollectionWithin( int nMonths )
		{
			Initialize();
			return m_creditReportImpl.HighestUnpaidBalOfChargeOffOrCollectionWithin( nMonths );
		}

		[CreditDebug(Category_Generic_Collection)]
        public int GetTotUnpaidBalOfCollectionWithin( int nMonths ) 
        {
            Initialize();
            return m_creditReportImpl.GetTotUnpaidBalOfCollectionWithin( nMonths );
        }
		[CreditDebug(Category_Generic_Judgment)]
        public int GetTotUnpaidBalOfJudgmentWithin( int nMonths ) 
        {
            Initialize();
            return m_creditReportImpl.GetTotUnpaidBalOfJudgmentWithin( nMonths );
        }
		[CreditDebug(Category_Generic_Chargeoff)]
        public int GetTotUnpaidBalOfChargeOffWithin( int nMonths ) 
        {
            Initialize();
            return m_creditReportImpl.GetTotUnpaidBalOfChargeOffWithin( nMonths );
        }
		[CreditDebug(Category_Generic_TaxLien)]
        public int GetTotUnpaidBalOfTaxLienWithin( int nMonths ) 
        {
            Initialize();
            return m_creditReportImpl.GetTotUnpaidBalOfTaxLienWithin( nMonths );
        }
        [CreditDebug( Category_Generic_Derog )]
        public int TotUnpaidBalOfAllDerogWithin( int nMonths ) 
        {
            Initialize();
            return m_creditReportImpl.TotUnpaidBalOfAllDerogWithin( nMonths );
        }

		// 2/7/2007 nw - OPM 10164
		[CreditDebug( Category_Generic_Derog )]
		public int TotUnpaidBalOfChargeOffAndCollectionWithin( int nMonths )
		{
			Initialize();
			return m_creditReportImpl.TotUnpaidBalOfChargeOffAndCollectionWithin( nMonths );
		}

		[CreditDebug(Category_Generic_TradeCount)]
        public int GetTradesCountActiveWithin( int nMonths ) 
        {
            Initialize();
            return m_creditReportImpl.GetTradesCountActiveWithin( nMonths );
        }
		[CreditDebug(Category_Generic_TradeCount)]
        public int GetTradesCountWithHighCreditOf( int amount ) 
        {
            Initialize();
            return m_creditReportImpl.GetTradesCountWithHighCreditOf( amount );
        }
		[CreditDebug(Category_Generic_TradeCount)]
        public int GetTradesCountActiveWithHighCreditOf( int amount ) 
        {
            Initialize();
            return m_creditReportImpl.GetTradesCountActiveWithHighCreditOf( amount );
        }

        [CreditDebug(Category_Generic_Foreclosure)]
        public int ForeclosureUnpaidCount 
        { 
            get 
            {
                Initialize();
                return m_creditReportImpl.ForeclosureUnpaidCount; 
            }
        }
        public CDateTime DischargedDOfLastForeclosure	
        { 
            get 
            {
                Initialize();
                return m_creditReportImpl.DischargedDOfLastForeclosure; 
            }
        }
        public CDateTime FileDOfLastForeclosure 
        { 
            get 
            {
                Initialize();
                return m_creditReportImpl.FileDOfLastForeclosure; 
            }
        }
        [CreditDebug(Category_Generic_Collection)]
        public int GetHighestUnpaidBalOfCollectionOlderThan( int nMonths ) 
        {
            Initialize();
            return m_creditReportImpl.GetHighestUnpaidBalOfCollectionOlderThan( nMonths );
        }

		//10/25/07 db - OPM 18561
		[CreditDebug(Category_Generic_Collection)]
		public int GetHighestUnpaidBalOfMedicalCollectionWithin( int nMonths ) 
		{
			Initialize();
			return m_creditReportImpl.GetHighestUnpaidBalOfMedicalCollectionWithin( nMonths );
		}

		[CreditDebug(Category_Generic_Judgment) ]
        public int GetHighestUnpaidBalOfJudgmentOlderThan( int nMonths ) 
        {
            Initialize();
            return m_creditReportImpl.GetHighestUnpaidBalOfJudgmentOlderThan( nMonths );
        }
		[CreditDebug(Category_Generic_Chargeoff) ]
        public int GetHighestUnpaidBalOfChargeOffOlderThan( int nMonths ) 
        {
            Initialize();
            return m_creditReportImpl.GetHighestUnpaidBalOfChargeOffOlderThan( nMonths );
        }

		// 5/14/2007 nw - OPM 13060 - New charge off / collection keywords
		[CreditDebug( Category_Generic_Derog )]
		public int HighestUnpaidBalOfChargeOffOrCollectionOlderThan( int nMonths )
		{
			Initialize();
			return m_creditReportImpl.HighestUnpaidBalOfChargeOffOrCollectionOlderThan( nMonths );
		}

		[CreditDebug(Category_Generic_Late)]
        public int GetHighestUnpaidPastDueWithin( int nMonths ) 
        {
            Initialize();
            return m_creditReportImpl.GetHighestUnpaidPastDueWithin( nMonths );
        }
		[CreditDebug(Category_Generic_Late)]
        public int GetHighestUnpaidPastDueOlderThan( int nMonths ) 
        {
            Initialize();
            return m_creditReportImpl.GetHighestUnpaidPastDueOlderThan( nMonths );
        }
		#endregion // 
    }
}
