using System;
using System.Collections;
using System.Xml;

using CommonLib;
using LendersOffice.CreditReport;
using LendersOffice.CreditReport.Mismo;
using DataAccess;

namespace LendersOffice.CreditReport.Mismo2_1
{
	public class Mismo2_1CreditReportRequest : MismoCreditReportRequest
	{
        protected override string MISMOVersionID 
        {
            get { return "2.1"; }
        }


        public Mismo2_1CreditReportRequest(string url) : base(url)
        {
        }


        #region Implementation of ICreditReportRequest
        public override LendersOffice.CreditReport.ICreditReportResponse CreateResponse(System.Xml.XmlDocument doc)
        {
            return new Mismo2_1CreditReportResponse(doc);
        }
        #endregion

    }
}
