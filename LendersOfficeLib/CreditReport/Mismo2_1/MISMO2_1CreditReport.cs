using System;
using System.Collections;
using System.Xml;
using DataAccess;
using LendersOffice.CreditReport;
using LendersOffice.CreditReport.Mismo;

namespace LendersOffice.CreditReport.Mismo2_1
{
    public class Mismo2_1CreditReport : MismoCreditReport 
    {
        internal Mismo2_1CreditReport(CreditReportFactory factory, CreditReportProtocol creditReportProtocol, XmlDocument doc, CreditReportDebugInfo debugInfo) : base(factory, creditReportProtocol, doc, debugInfo)
        {
        }
    }
}
