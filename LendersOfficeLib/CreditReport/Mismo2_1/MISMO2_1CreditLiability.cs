using System;
using System.Collections;
using System.Xml;

using DataAccess;
using LendersOffice.CreditReport;
using LendersOffice.CreditReport.Mismo;
using LendersOffice.Common;

namespace LendersOffice.CreditReport.Mismo2_1
{
	/// <summary>
	/// Summary description for MISMO2_1CreditLiability.
	/// </summary>
	public class Mismo2_1CreditLiability : MismoCreditLiability
	{

        public Mismo2_1CreditLiability(XmlElement element, string borrowerID, string coborrowerID, AbstractCreditReport parent) : base(element, borrowerID, coborrowerID, parent)
        {
        }

    }
}
