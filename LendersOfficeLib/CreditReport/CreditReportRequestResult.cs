﻿namespace LendersOffice.CreditReport
{
    using System;
    using System.Collections.Generic;
    using Newtonsoft.Json;

    /// <summary>
    /// Class holding credit report request results.
    /// </summary>
    public class CreditReportRequestResult
    {
        /// <summary>
        /// Prevents a default instance of the <see cref="CreditReportRequestResult"/> class from being created.
        /// </summary>
        [JsonConstructor]
        private CreditReportRequestResult()
        {
        }

        /// <summary>
        /// Gets the result status.
        /// </summary>
        [JsonProperty]
        public CreditReportRequestResultStatus Status
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the credit request data. This may have been mutated during the request process so it is safest to use this over the input credit request data.
        /// Only exists for Completed results.
        /// </summary>
        [JsonProperty]
        public CreditRequestData CreditRequestData
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the credit report response.
        /// Only exists for Completed results.
        /// </summary>
        [JsonProperty]
        public ICreditReportResponse CreditReportResponse
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the raw response.
        /// Only exists for Completed results.
        /// </summary>
        public string RawCreditReportResponse
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the public job id.
        /// Only exists for Processing results.
        /// </summary>
        [JsonProperty]
        public Guid? PublicJobId
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the error messages.
        /// Only exists for Failure results.
        /// </summary>
        [JsonProperty]
        public IEnumerable<string> ErrorMessages
        {
            get;
            private set;
        }

        /// <summary>
        /// Creates an error result.
        /// </summary>
        /// <param name="errorMessages">The error messages to pass back.</param>
        /// <returns>The error result.</returns>
        public static CreditReportRequestResult CreateErrorResult(IEnumerable<string> errorMessages)
        {
            return new CreditReportRequestResult()
            {
                Status = CreditReportRequestResultStatus.Failure,
                ErrorMessages = errorMessages
            };
        }

        /// <summary>
        /// Creates a Processing result.
        /// </summary>
        /// <param name="publicJobId">The public job id of the queued up job.</param>
        /// <returns>The Processing result.</returns>
        public static CreditReportRequestResult CreateProcessingResult(Guid publicJobId)
        {
            return new CreditReportRequestResult()
            {
                Status = CreditReportRequestResultStatus.Processing,
                PublicJobId = publicJobId
            };
        }

        /// <summary>
        /// Creates a Completed result.
        /// </summary>
        /// <param name="requestData">The request data that was used in the request.</param>
        /// <param name="response">The response for the request.</param>
        /// <param name="rawResponse">The raw response.</param>
        /// <returns>The Completed result.</returns>
        public static CreditReportRequestResult CreateCompletedResult(CreditRequestData requestData, ICreditReportResponse response, string rawResponse)
        {
            return new CreditReportRequestResult()
            {
                Status = CreditReportRequestResultStatus.Completed,
                CreditReportResponse = response,
                CreditRequestData = requestData,
                RawCreditReportResponse = rawResponse
            };
        }
    }
}
