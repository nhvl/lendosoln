﻿using DataAccess;

/// <summary>
/// Struct representing a credit score calculation result.
/// </summary>
public struct CreditScoreCalculationResult
{
    /// <summary>
    /// Credit Score.
    /// </summary>
    public readonly int CreditScore;

    /// <summary>
    /// Hmda Report Credit Score as type. Can be borrower, coborrower, or blank.
    /// </summary>
    public readonly sHmdaReportCreditScoreAsT HmdaReportAsT;

    /// <summary>
    /// Integer representing the index of the application in the application list.
    /// </summary>
    public readonly int ApplicationIndex;

    /// <summary>   
    /// Initializes a new instance of the <see cref="CreditScoreCalculationResult" /> struct.
    /// </summary>
    /// <param name="creditScore">Credit Score.</param>
    /// <param name="hmdaReportAsT">Hmda Report as T type.</param>
    /// <param name="applicationIndex">Application index.</param>
    public CreditScoreCalculationResult(int creditScore, sHmdaReportCreditScoreAsT hmdaReportAsT, int applicationIndex)
    {
        this.CreditScore = creditScore;
        this.HmdaReportAsT = hmdaReportAsT;
        this.ApplicationIndex = applicationIndex;
    }
}