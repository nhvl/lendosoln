using System;
using System.Xml;

using LendersOffice.CreditReport;
using LendersOffice.Common;
using DataAccess;

namespace LendersOffice.CreditReport.CreditInterlink
{
    public class CreditInterlink_CreditReportResponse : LendersOffice.CreditReport.Mismo2_3.Mismo2_3CreditReportResponse
    {
        public CreditInterlink_CreditReportResponse(XmlDocument doc)
            : base(doc)
        {


            XmlElement node = (XmlElement)doc.SelectSingleNode("//RESPONSE_GROUP/RESPONSE/STATUS[@_Condition = 'Error']");

            if (node != null)
            {
                if (node.HasAttribute("_Description"))
                {
                    if (node.GetAttribute("_Description").Equals("User not found", StringComparison.InvariantCultureIgnoreCase))
                    {
                        SetErrorInfo(ErrorMessages.InvalidLoginPassword);
                    }
                    else
                    {
                        SetErrorInfo("CRA returned error : " + node.GetAttribute("_Description"));
                    }
                }
                else
                {
                    SetErrorInfo(ErrorMessages.Generic);
                    Tools.LogError("Error parsing credit report please investigate no _Description found in the error tag for CQS");
                }
            }

        }
    }
}
