using System;
using System.Xml;

using LendersOffice.CreditReport;

namespace LendersOffice.CreditReport.CreditInterlink
{
	public class CreditInterlink_CreditReportRequest : LendersOffice.CreditReport.Mismo2_3.Mismo2_3CreditReportRequest
	{
		private const string CreditQuickServicesURL = "HTTPS://WWW.CREDITQUICKSERVICES.COM/LOSREQUEST/WEBLOSREQUEST.ASPX?CLIENT=PRICEMYLOAN";
		private const string OldRepublicCreditServicesURL = "HTTPS://WWW.ORCREDIT.COM/LOSREQUEST/WEBLOSREQUEST.ASPX?CLIENT=PRICEMYLOAN";

		public CreditInterlink_CreditReportRequest(string url) : base(url)
		{
			if (url.ToUpper() == CreditQuickServicesURL)
			{
				this.ReceivingPartyName = "CQS";
			}
			else if (url.ToUpper() == OldRepublicCreditServicesURL)
			{
				this.ReceivingPartyName = "ORCS";
			}
			else
			{
				DataAccess.Tools.LogError("CreditInterlink URL mismatch.");
			}

			this.SubmittingPartyName = "Insight Lending Solutions";
            // this value will later be set to the name of the person that orders the credit report
            // this.RequestingPartyRequestedByName = "Insight Lending Solutions";

            this.AppendOutputFormat(Mismo.E_MismoOutputFormat.Other);
            this.PreferredResponseFormatOtherDescription = "HTML";
		}
		public override ICreditReportResponse CreateResponse(XmlDocument doc) 
		{
			return new CreditInterlink_CreditReportResponse(doc);
		}
	}
}
