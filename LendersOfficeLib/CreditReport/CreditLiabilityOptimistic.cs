namespace LendersOffice.CreditReport
{
    using System;
    using System.Collections;
    using DataAccess;

    public class CreditLiabilityOptimistic : AbstractCreditLiability
	{
        private string m_id;
        private string m_creditorName;
        private string m_creditorAddress;
        private string m_creditorCity;
        private string m_creditorState;
        private string m_creditorZipcode;
        private string m_creditorPhone;
        private string m_accountIdentifier;
        private string m_termsMonthsCount;
        private decimal m_unpaidBalanceAmount;
        private decimal m_monthlyPaymentAmount;
        private string m_monthsRemainingCount;
        private string m_late30;
        private string m_late60;
        private string m_late90;
        private E_DebtRegularT m_liabilityType;
        private E_LiaOwnerT m_liaOwnerT;
        private bool m_isForeclosure;
        private bool m_isBankruptcy;
        private bool m_isRepos;


        public CreditLiabilityOptimistic(ILiabilityRegular lia, AbstractCreditReport parent) 
        {
            ParentCreditReport = parent;
            m_id = lia.RecordId.ToString();
            m_creditorName = lia.ComNm;
            m_creditorAddress = lia.ComAddr;
            m_creditorCity = lia.ComCity;
            m_creditorState = lia.ComState;
            m_creditorZipcode = lia.ComZip;
            m_creditorPhone = lia.ComPhone;
            m_accountIdentifier = lia.AccNum.Value;
            m_termsMonthsCount = lia.OrigTerm_rep;
            m_unpaidBalanceAmount = lia.Bal;
            m_monthlyPaymentAmount = lia.Pmt;
            m_monthsRemainingCount = lia.RemainMons_rep;
            m_late30 = lia.Late30_rep;
            m_late60 = lia.Late60_rep;
            m_late90 = lia.Late90Plus_rep;
            m_liabilityType = lia.DebtT;
            m_liaOwnerT = lia.OwnerT;
            m_isForeclosure = lia.IncInForeclosure;
            m_isBankruptcy = lia.IncInBankruptcy;
            m_isRepos = lia.IncInReposession;
        }

        public override void UpdateUnpaidBalanceAmount(decimal value) 
        {
        }
        public override void UpdateMonthlyPaymentAmount(decimal value) 
        {
        }

		public override bool IsDischargedBk
		{
			get { return false; }
		}

        public override bool IsModified 
        {
            get { return false; }
        }
        public override string ModifiedString 
        {
            get { return ""; }
        }

        public override bool HasTerminalDerog 
        { 
            get
            {
                return m_isForeclosure || m_isBankruptcy || m_isRepos;
            }
        }

        public override string ID 
        {
            get { return m_id; }
        }
        public override string CreditorName 
        {
            get { return m_creditorName; }
        }
        public override string CreditorAddress 
        {
            get { return m_creditorAddress; }
        }
        public override string CreditorCity 
        {
            get { return m_creditorCity; }
        }
        public override string CreditorState 
        {
            get { return m_creditorState; }
        }
        public override string CreditorZipcode 
        {
            get { return m_creditorZipcode; }
        }
        public override string CreditorPhone 
        {
            get { return m_creditorPhone; }
        }
        public override DateTime AccountOpenedDate 
        {
            get { return DateTime.MinValue; }
        }
        public override DateTime AccountReportedDate 
        {
            get { return DateTime.Now; }
        }
        public override decimal PastDueAmount 
        {
            get { return 0.0M; }
        }
        public override decimal HighCreditAmount 
        {
            get { return 2500.0M; } // Assume for the best
        }
        public override string AccountIdentifier 
        {
            get { return m_accountIdentifier; }
        }

        public override string TermsMonthsCount 
        {
            get { return m_termsMonthsCount; }
        }
        public override decimal UnpaidBalanceAmount 
        {
            get { return m_unpaidBalanceAmount; }
        }
        public override decimal MonthlyPaymentAmount 
        {
            get { return m_monthlyPaymentAmount; }
        }
        public override string MonthsRemainingCount
        {
            get { return m_monthsRemainingCount; }
        }
        public override string Late30 
        {
            get { return m_late30; }
        }
        public override string Late60 
        {
            get { return m_late60; }
        }

        public override string Late90 
        {
            get { return m_late90; }
        }

        public override string Late120 
        {
            get { return "0"; }
        }

        public override int MonthsReviewedCount 
        {
            get { return 24; } // Assume the best.
        }

        public override DateTime LastActivityDate 
        {
            get { return DateTime.Now; }
        }
        public override E_DebtRegularT LiabilityType 
        {
            get { return m_liabilityType; }
        }
        public override E_LiaOwnerT LiaOwnerT 
        {
            get { return m_liaOwnerT; }
        }
        public override bool IsForeclosure 
        {
            get { return m_isForeclosure; }
        }
        public override bool IsBankruptcy 
        {
            get { return m_isBankruptcy; }
        }
        public override bool IsChargeOff 
        {
            get { return false; }
        }
        public override DateTime PaymentPatternStartDate 
        {
            get { return DateTime.MinValue; }
        }
        public override bool IsRepos 
        {
            get { return m_isRepos; }
        }
        public override bool IsCollection
        {
            get { return false; }
        }

        public override bool IsInCreditCounseling 
        {
            get { return false; }
        }

        public override bool IsLate 
        {
            get { return m_late30 != "0" || m_late60 != "0" || m_late90 != "0"; }
        }
        public override ArrayList CreditAdverseRatingList 
        {
            get { return new ArrayList(); }
        }
        public override bool IsMedicalTradeline 
        {
            get { return false; }
        }

        public override bool HasBeenDerog 
        {
            get { return false; }
        }
        public override bool IsDerogCurrently 
        {
            get { return false; }
        }
		public override bool IsLastStatusDerog
		{
			get { return false; }
		}
        public override bool IsActive 
        {
            get { return true; }
        }

        public override bool IsAuthorizedUser 
        {
            // 1/30/2006 dd - OPM 3949 - Implement IsAuthorizedUser
            get { return false; }
        }
        public override bool IsStudentLoansNotInRepayment 
        {
            // 1/30/2006 dd - OPM 3949
            get { return false; }
        }
		public override bool IsGovMiscDebt 
		{
			get { return false; }
		}

	}
}
