using System;
using System.Xml;

using DataAccess;
using LendersOffice.Common;
namespace LendersOffice.CreditReport.Mcl
{

	public class MclCreditReportView : LendersOffice.CreditReport.ICreditReportView
	{
        private CreditReportDebugInfo m_debugInfo;
        private XmlDocument m_doc;

        private bool m_hasPdf = false;
        private bool m_hasHtml = false;
        private string m_htmlContent = "";
        private string m_pdfContent = "";


		internal MclCreditReportView(CreditReportFactory factory, XmlDocument doc, CreditReportDebugInfo debugInfo)
		{
            if (null == factory) 
            {
                throw new CBaseException(ErrorMessages.Generic, "MclCreditReportView can only be instantiate by CreditReportFactory.");
            }
            if (null == doc) 
            {
                throw new CBaseException(ErrorMessages.Generic, "MclCreditReportView has null XmlDocument in constructor.");
            }
            m_doc = doc;
            m_debugInfo = debugInfo;

            Parse();
		}

        private void Parse() 
        {
            XmlNode node = m_doc.SelectSingleNode("/OUTPUT/RESPONSE/OUTPUT_FORMAT[@format_type='HTML-INLINED']");
            if (null != node) 
            {
                m_hasHtml = true;
                m_htmlContent = node.InnerText;
				string m_htmlContentCapitalized = m_htmlContent.ToUpper();

				// 2/5/2007 nw - OPM 8399 - Strip out the hyperlink for "DEBT/HIGH CREDIT" value
				/*
				 * <td class="reportbody" align="right">28939</td><td class="reportlabel" align="right" colspan="2">
				 * DEBT/HIGH CREDIT</td><td class="reportbody" align="right"><A href="#" onclick="openHelp(35); 
				 * return(false);" title="Debt/High Credit ratio = Total Balance/Total High Credit (High Credit 
				 * from accounts that are collections, chargeoff, bankruptcy, or closed are excluded)">107%</A></td>
				 * */
				int tempIndex = m_htmlContentCapitalized.IndexOf("DEBT/HIGH CREDIT");
				if (tempIndex > 0)
				{
					int startIndex = m_htmlContentCapitalized.IndexOf("<A HREF", tempIndex);
					if (startIndex > 0)
					{
						int endIndex = m_htmlContentCapitalized.IndexOf(">", startIndex);
						if (endIndex > 0)
						{
							tempIndex = m_htmlContentCapitalized.IndexOf("</A>", endIndex);
							if (tempIndex > 0)
							{
								m_htmlContent = m_htmlContent.Remove(tempIndex, "</A>".Length);
								m_htmlContent = m_htmlContent.Substring(0, startIndex) + m_htmlContent.Substring(endIndex + 1);
							}
						}
					}
				}
            }
            
            node = m_doc.SelectSingleNode("/OUTPUT/RESPONSE/OUTPUT_FORMAT[@format_type='PDF-BASE64']");
            if (null != node) 
            {
                m_hasPdf = true;
                m_pdfContent = node.InnerText;
            }
        }
        public bool HasPdf 
        {
            get { return m_hasPdf; }
        }
        public bool HasHtml 
        {
            get { return m_hasHtml; }
        }
        public string HtmlContent 
        {
            get { return m_htmlContent; }
        }
        public string PdfContent 
        {
            get { return m_pdfContent; }
        }
        public string RawXml 
        {
            get { return m_doc.InnerXml; }
        }

        public string XmlResponseContent 
        {
            get 
            {
                XmlNode node = m_doc.SelectSingleNode("/OUTPUT/RESPONSE/OUTPUT_FORMAT[@format_type='XML']");
                if (null != node)
                    return node.InnerText;
                else
                    return "";
            }
        }
        public string MismoContent
        {
            get
            {
                XmlNode node = m_doc.SelectSingleNode("/OUTPUT/RESPONSE/OUTPUT_FORMAT[@format_type='MISMO2_3_1']");
                if (null != node)
                    return node.InnerText;
                else
                    return "";

            }
        }
	}
}
