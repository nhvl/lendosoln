using System;
using System.Collections;
using System.Xml;

using LendersOffice.CreditReport;
using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.CreditReport.Mcl.Code
{
	internal class CMclCodePmtHistory
	{
		internal const char Current					= '0'; 
		internal const char Late30Days				= '1';
		internal const char Late60Days				= '2';
		internal const char Late90Days				= '3';
		internal const char Late120Days				= '4';
		internal const char Late150Days				= '5';
		internal const char Late180Days				= '6';
		internal const char PmtPlan					= '7';
		internal const char ReposOrForeclosure		= '8';
		internal const char ChargeOffOrCollection	= '9';
		internal const char UnknownU				= 'U';
		internal const char UnknownHyphen			= '-';
	}

	internal class CMclCodeTradelineT
	{
		internal const string Installment		= "II";
		internal const string Mortgage			= "IM";
		internal const string Auto				= "IA";
		internal const string Education			= "IE";
		internal const string Lease				= "IL";
		internal const string Cosigner			= "IC";
		internal const string CreditCard		= "RC";
		internal const string OpenAccount_RO	= "RO";
		internal const string Revolving			= "RR";
		internal const string Collection		= "YY";
		internal const string Other				= "OT"; // I have seen LINE OF CREDIT listed under this type.
		internal const string OpenAccount_OO	= "OO";
	}

    internal enum E_DerogHistT
    {
        NeverDerog = 0,
        WasDerog = 1, // was derog but not any more
        StillDerog = 2
    }
    internal class CTradelineStatusCharacteristics
    {
        private bool m_isActive;
        private E_DerogHistT m_derogHistT;
        private bool m_hasTerminalDerog;

        internal CTradelineStatusCharacteristics( bool isActive, bool hasTerminalDerog, E_DerogHistT derogHistT )
        {
            m_isActive = isActive;
            m_derogHistT = derogHistT;
            m_hasTerminalDerog = hasTerminalDerog;
        }

        internal bool IsActive { get{ return m_isActive; } }
        internal bool HasTerminalDerog { get { return m_hasTerminalDerog; } }
        internal E_DerogHistT DerogHistT{ get{   return m_derogHistT; } }

    };

	internal class CMclCodeTradelineStatusT
	{
		internal const string NoStatus							= "00";
		internal const string CreditCardLost					= "01";
		internal const string Inactive							= "02";
		internal const string Deleted							= "03";
		internal const string ToNewToRate						= "04"; // * new
		internal const string CurrentAsAgreed					= "10";
		internal const string CurrentWas30Late					= "11";
		internal const string CurrentWas60Late					= "12";
		internal const string CurrentWas90Late					= "13";
		internal const string CurrentWas120Late					= "14";
		internal const string CurrentOnly						= "1A"; // * new
		internal const string CurrentWasCollection				= "1C";
		internal const string DeferredPmt						= "1D"; // * new - not a derog account, student loans are deferred
		internal const string CurrentWasBankruptcy				= "1B";
		internal const string CurrentWasForeclosure 			= "1F";
		internal const string CurrentWasRepossession			= "1R";
		internal const string PaidAcc							= "20";
		internal const string ClosedAcc							= "2X";
		internal const string TransferredAcc					= "2T";
		internal const string PaidWas30Late						= "21";
		internal const string PaidWas60Late						= "22";
		internal const string PaidWas90Late						= "23";
		internal const string PaidWas120Late					= "24";
		internal const string PaidWasCollection_Obsolete		= "2C"; // (obsolete - use 9P)
		internal const string PaidWasRepossession_Obsolete		= "2R"; // (obsolete - use 9R)
		internal const string PaidWasChargeOff_Obsolete			= "2A"; // (obsolete - use 9A)
		internal const string PaidWasForeclosure_Obsolete		= "2F"; // (obsolete - use 9F)
		internal const string PaidVoluntarySurrender_Obsolete	= "2S"; // (obsolete - use 8S)
		internal const string Late30							= "30";
		internal const string Late60							= "40";
		internal const string Late90							= "50";
		internal const string Late120							= "60";
		internal const string Bankruptcy						= "70";
		internal const string PayingUnderPmtPlan				= "71";
		internal const string Repossession						= "80";
		internal const string Foreclosure						= "82";
		internal const string Settled							= "83";
		internal const string VoluntarySurrender				= "8S";
		internal const string ChargeOff							= "90";
		internal const string PlacedForCollection				= "91";
		internal const string PaidCollection					= "9P";
		internal const string PaidWasRepossession				= "9R";
		internal const string PaidWasChargeOff					= "9A";
		internal const string PaidWasForeclosure				= "9F";
		internal const string NowPayingWasChargeOff 			= "92";
		internal const string ClaimFiledWithGov					= "93";
		internal const string ClosedNotPaidAsAgreed 			= "94"; // (will be obsolete - use 2X)
		internal const string CannotLocateConsumer				= "95";

        private static Hashtable s_table = new Hashtable( 70 );
        static CMclCodeTradelineStatusT()
        {
            // IsActive, HasTerminalDerog, Derog History Type
            s_table.Add( NoStatus,             new CTradelineStatusCharacteristics( true, false, E_DerogHistT.NeverDerog ) );
            s_table.Add( CreditCardLost,   new CTradelineStatusCharacteristics( false, false, E_DerogHistT.NeverDerog ) );                          
            s_table.Add( Inactive,                new CTradelineStatusCharacteristics( false, false, E_DerogHistT.NeverDerog ) );
            s_table.Add( Deleted,                new CTradelineStatusCharacteristics( false, false, E_DerogHistT.NeverDerog ) );         
            s_table.Add( ToNewToRate,      new CTradelineStatusCharacteristics( true, false, E_DerogHistT.NeverDerog ) );           
            s_table.Add( CurrentAsAgreed,                new CTradelineStatusCharacteristics( true, false, E_DerogHistT.NeverDerog ) );                  
            s_table.Add( CurrentWas30Late,                new CTradelineStatusCharacteristics( true, false, E_DerogHistT.WasDerog ) );
            s_table.Add( CurrentWas60Late,                new CTradelineStatusCharacteristics( true, false, E_DerogHistT.WasDerog ) );
            s_table.Add( CurrentWas90Late,                new CTradelineStatusCharacteristics( true, false, E_DerogHistT.WasDerog ) );
            s_table.Add( CurrentWas120Late,                new CTradelineStatusCharacteristics( true, false, E_DerogHistT.WasDerog ) );
            s_table.Add( CurrentOnly,                new CTradelineStatusCharacteristics( true, false, E_DerogHistT.NeverDerog ) );
            s_table.Add( CurrentWasCollection,                new CTradelineStatusCharacteristics( true,  true, E_DerogHistT.WasDerog ) );              
            s_table.Add( DeferredPmt,                new CTradelineStatusCharacteristics( true,  false, E_DerogHistT.NeverDerog ) );              
            s_table.Add( CurrentWasBankruptcy,                new CTradelineStatusCharacteristics( true,  true, E_DerogHistT.WasDerog ) );              
            s_table.Add( CurrentWasForeclosure,                new CTradelineStatusCharacteristics( true, true,E_DerogHistT.WasDerog ) );          
            s_table.Add( CurrentWasRepossession,                new CTradelineStatusCharacteristics( true, true, E_DerogHistT.WasDerog ) );                      
            s_table.Add( PaidAcc,                new CTradelineStatusCharacteristics( false,  false, E_DerogHistT.NeverDerog ) );      
            s_table.Add( ClosedAcc,                new CTradelineStatusCharacteristics( false,  false, E_DerogHistT.NeverDerog ) );                  
            s_table.Add( TransferredAcc,                new CTradelineStatusCharacteristics( false,  false, E_DerogHistT.NeverDerog ) );              
            s_table.Add( PaidWas30Late,                new CTradelineStatusCharacteristics( false,  false, E_DerogHistT.WasDerog  ) );                  
            s_table.Add( PaidWas60Late,                new CTradelineStatusCharacteristics( false,  false, E_DerogHistT.WasDerog ) );                  
            s_table.Add( PaidWas90Late,                new CTradelineStatusCharacteristics( false,  false, E_DerogHistT.WasDerog ) );              
            s_table.Add( PaidWas120Late,                new CTradelineStatusCharacteristics( false,  false, E_DerogHistT.WasDerog ) );      
            s_table.Add( PaidWasCollection_Obsolete,                new CTradelineStatusCharacteristics( false, true, E_DerogHistT.WasDerog ) );
            s_table.Add( PaidWasRepossession_Obsolete,                new CTradelineStatusCharacteristics( false, true, E_DerogHistT.WasDerog ) );  
            s_table.Add( PaidWasChargeOff_Obsolete,                new CTradelineStatusCharacteristics( false, true, E_DerogHistT.WasDerog ) );  
            s_table.Add( PaidWasForeclosure_Obsolete,                new CTradelineStatusCharacteristics( false, true, E_DerogHistT.WasDerog ) );
            s_table.Add( PaidVoluntarySurrender_Obsolete,                new CTradelineStatusCharacteristics( false, true, E_DerogHistT.WasDerog ) );                      
            s_table.Add( Late30,                new CTradelineStatusCharacteristics( true,  false, E_DerogHistT.StillDerog ) );                  
            s_table.Add( Late60,                new CTradelineStatusCharacteristics( true,  false, E_DerogHistT.StillDerog ) );                  
            s_table.Add( Late90,                new CTradelineStatusCharacteristics( true,  false, E_DerogHistT.StillDerog ) );                  
            s_table.Add( Late120,                new CTradelineStatusCharacteristics( true,  false, E_DerogHistT.StillDerog ) );              
            s_table.Add( Bankruptcy,                new CTradelineStatusCharacteristics( false, true, E_DerogHistT.WasDerog ) );              
            s_table.Add( PayingUnderPmtPlan,                new CTradelineStatusCharacteristics( true, false, E_DerogHistT.WasDerog ) );                  
            s_table.Add( Repossession,                new CTradelineStatusCharacteristics( false, true, E_DerogHistT.WasDerog ) );                  
            s_table.Add( Foreclosure,                new CTradelineStatusCharacteristics( false, true, E_DerogHistT.WasDerog ) );                  
            s_table.Add( Settled,                new CTradelineStatusCharacteristics( false,  false, E_DerogHistT.WasDerog ) );                  
            s_table.Add( VoluntarySurrender,                new CTradelineStatusCharacteristics( false, true, E_DerogHistT.WasDerog ) );
            s_table.Add( ChargeOff,                new CTradelineStatusCharacteristics( false,  true, E_DerogHistT.WasDerog ) );  
            s_table.Add( PlacedForCollection,                new CTradelineStatusCharacteristics( false, true, E_DerogHistT.WasDerog ) ); // TODO: not sure if this a "was" or "is"
            s_table.Add( PaidCollection,                new CTradelineStatusCharacteristics( false, true, E_DerogHistT.WasDerog ) );              
            s_table.Add( PaidWasRepossession,                new CTradelineStatusCharacteristics( false, true, E_DerogHistT.WasDerog ) );              
            s_table.Add( PaidWasChargeOff,                new CTradelineStatusCharacteristics( false, true, E_DerogHistT.WasDerog ) );              
            s_table.Add( PaidWasForeclosure,                new CTradelineStatusCharacteristics( false, true, E_DerogHistT.WasDerog ) );
            s_table.Add( NowPayingWasChargeOff,                new CTradelineStatusCharacteristics( true, true, E_DerogHistT.WasDerog ) );      
            s_table.Add( ClaimFiledWithGov,                new CTradelineStatusCharacteristics( false, false, E_DerogHistT.WasDerog ) );
            s_table.Add( ClosedNotPaidAsAgreed,                new CTradelineStatusCharacteristics( false, false, E_DerogHistT.StillDerog ) );
            s_table.Add( CannotLocateConsumer,                new CTradelineStatusCharacteristics( false, false, E_DerogHistT.StillDerog ) );
        }

        internal static bool IsActive( string code )
        {
            try
            {
                CTradelineStatusCharacteristics chars = ( CTradelineStatusCharacteristics ) s_table[ code ];
                return chars.IsActive;
            }
            catch
            {
                string errStr = string.Format( "Error. Cannot process this MCL tradeline status code:{0}.",  code );
                Tools.LogErrorWithCriticalTracking( errStr );
                throw new CBaseException( string.Format( ErrorMessages.CannotProcessMclCreditReport, code ), errStr );
            }
        }

        internal static bool HasBeenDerog( string code )
        {
            try
            {
                CTradelineStatusCharacteristics chars = ( CTradelineStatusCharacteristics ) s_table[ code ];
                switch(  chars.DerogHistT )
                {
                    case E_DerogHistT.NeverDerog:
                        return false;
                    case E_DerogHistT.WasDerog:
                    case E_DerogHistT.StillDerog:
                        return true;
                    default:
                        throw new CBaseException( ErrorMessages.MclEnumValueNotHandled, "An enum value of E_DerogHistT is not handled" );
                }
            }
            catch
            {
                string errStr = string.Format( "Error. Cannot process this MCL tradeline status code:{0}." + code );
                Tools.LogErrorWithCriticalTracking( errStr );
                throw new CBaseException( string.Format( ErrorMessages.CannotProcessMclCreditReport, code ), errStr );
            }
        }

        internal static bool IsDerogCurrently( string code )
        {
            try
            {
                CTradelineStatusCharacteristics chars = ( CTradelineStatusCharacteristics ) s_table[ code ];
                switch(  chars.DerogHistT )
                {
                    case E_DerogHistT.NeverDerog:
                    case E_DerogHistT.WasDerog:
                        return false;
                    case E_DerogHistT.StillDerog:
                        return true;
                    default:
                        throw new CBaseException( ErrorMessages.MclEnumValueNotHandled, "An enum value of E_DerogHistT is not handled" );
                }
            }
            catch
            {
                string errStr = string.Format( "Error. Cannot process this MCL tradeline status code:{0}." + code );
                Tools.LogErrorWithCriticalTracking( errStr );
                throw new CBaseException( string.Format( ErrorMessages.CannotProcessMclCreditReport, code ), errStr );
            }
        }

        internal static bool HasTerminalDerog( string code )
        {
            try
            {
                CTradelineStatusCharacteristics chars = ( CTradelineStatusCharacteristics ) s_table[ code ];
                return chars.HasTerminalDerog;
            }
            catch
            {
                string errStr = string.Format( "Error. Cannot process this MCL tradeline status code:{0}.",  code );
                Tools.LogErrorWithCriticalTracking( errStr );
                throw new CBaseException( string.Format( ErrorMessages.CannotProcessMclCreditReport, code ), errStr );
            }
        }
	}

	internal class CMclCodePubRecT
	{
		internal const string Attachment		= "AM";
		internal const string ChildSupport		= "CP";
		internal const string Suit				= "SU";
		internal const string Foreclosure		= "FC";
		internal const string ForcibleDetainer	= "FD";
		internal const string Garnishment		= "GN";
		internal const string HOALien			= "HA";
		internal const string HospitalLien		= "HL";
		internal const string JudicialLien		= "JL";
		internal const string TaxLien			= "TL";
		internal const string Judgment			= "JM";
		internal const string ReLien			= "RL";
		internal const string MechanicLien		= "ML";
		internal const string PavingAssessmentLine = "PG";
		internal const string Trusteeship		= "TR";
		internal const string WaterAndSewerLien = "WS";
		internal const string Bankruptcy11		= "B1";
		internal const string Bankruptcy12		= "B2";
		internal const string Bankruptcy13		= "B3";
		internal const string Bankruptcy7		= "B7";
		internal const string BankruptcyGeneral = "BK";
		internal const string Collection		= "CO";
		internal const string FinancingStatement = "FS";
		internal const string Nonresponsibility = "NR";
		internal const string FinancialCounselor = "FL";
		internal const string FederalTaxLien	= "TA";
		internal const string StateTaxLien		= "TB";
		internal const string CountyTaxLien		= "TC";
		internal const string LocalTaxLien		= "TD";
	}

	internal class CMclCodePubRecStatusT
	{
		internal const string Canceled					= "CANCELED";
		internal const string Discharged				= "DISCHARGED";
		internal const string Dismissed					= "DISMISSED";
		internal const string Filed						= "FILED";
		internal const string Involuntary				= "INVOLUNTARY";
		internal const string NonAdjudicated			= "NON-ADJUDICATED";
		internal const string NotReleased				= "NOT RELEASED";
		internal const string NotSatisfied				= "NOT SATISFIED";
		internal const string Paid						= "PAID";
		internal const string PlanComplete				= "PLAN COMPLETE";
		internal const string Released					= "RELEASED";
		internal const string RelievedInBankruptcy		= "RELIEVED IN BANKRUPTCY";
		internal const string Revived					= "REVIVED";
		internal const string Satisfied					= "SATISFIED";
		internal const string TerminatedUnsuccessfully	= "TERMINATED UNSUCCESSFULLY";
		internal const string Vacated					= "VACATED";
		internal const string VacatedOrReversed			= "VACATED OR REVERSED";
		internal const string Voluntary					= "VOLUNTARY";
		internal const string VoluntaryDismissal		= "VOLUNTARY DISMISSAL";
		internal const string VoluntaryWithdrawal		= "VOLUNTARY WITHDRAWAL";
	}

	internal class CMclCodeKOB
	{
		internal const string Auto					= "A";
		internal const string BanksAndSL			= "B";
		internal const string Clothing				= "C";
		internal const string Department			= "D";
		internal const string Employment			= "E";
		internal const string FinancePersonal		= "F";
		internal const string Groceries				= "G";
		internal const string HomeFurnishing		= "H";
		internal const string Insurance				= "I";
		internal const string JewelryElectronics	= "J";
		internal const string Contractors			= "K";
		internal const string BuildingMaterial		= "L";
		internal const string Medical				= "M";
		internal const string CreditCardEntertainment = "N";
		internal const string OilCompanies			= "O";
		internal const string PersonalServicesNonmed = "P";
		internal const string FinanceNonpersonal	= "Q";
		internal const string RealEstateAndPubAccomodations = "R";
		internal const string SportingGoods			= "S";
		internal const string FarmGardenSupplies	= "T";
		internal const string UtilitiesFuel			= "U";
		internal const string Government			= "V";
		internal const string Wholesale				= "W";
		internal const string Advertising			= "X";
		internal const string CollectionServices	= "Y";
		internal const string MiscAndPubRec			= "Z";
	}
}


