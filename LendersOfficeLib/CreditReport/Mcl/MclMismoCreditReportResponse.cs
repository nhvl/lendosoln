using System;
using System.Xml;
using DataAccess;

namespace LendersOffice.CreditReport.Mcl
{
	/// <summary>
	/// Summary description for MclMismoCreditReportResponse.
	/// </summary>
	public class MclMismoCreditReportResponse : LendersOffice.CreditReport.ICreditReportResponse
	{
		private string		m_reportID;
		private string		m_status;
		private bool		m_hasError;
		private bool		m_isReady;
		protected string	m_errorMessage;
		private string		m_rawXmlResponse = "";
		
		public MclMismoCreditReportResponse(XmlDocument doc)
		{
			if (null != doc)
				Parse(doc);
		}

		protected virtual string DetermineErrorStatus(string code) 
		{
			return "ERROR"; // Generic error status
		}

		private void Parse(XmlDocument doc) 
		{
			XmlElement creditResponseElement = (XmlElement) doc.SelectSingleNode("//RESPONSE_GROUP/RESPONSE/RESPONSE_DATA/CREDIT_RESPONSE");

			if (null == creditResponseElement)
				throw new DataAccess.CBaseException(Common.ErrorMessages.InvalidMismoFormat, "Invalid Mismo Format");

			if (creditResponseElement.GetAttribute("CreditReportType") == "Error") 
			{
				// For MCL Mismo, the informative error message is in status.
				XmlElement statusElement = (XmlElement) doc.SelectSingleNode("//RESPONSE_GROUP/RESPONSE/STATUS");

				switch (statusElement.GetAttribute("_Code")) 
				{
					case "E0046":
						m_status = "LOGIN_FAILED";
						break;
					default:
						m_status = "ERROR";
						break;
				}

				m_isReady = false;
				m_hasError = true;
				int index = 0;
				m_errorMessage = statusElement.GetAttribute("_Condition");

				if (m_errorMessage == "")
				{
					XmlElement errorElement = (XmlElement) doc.SelectSingleNode("//RESPONSE_GROUP/RESPONSE/RESPONSE_DATA/CREDIT_RESPONSE/CREDIT_ERROR_MESSAGE");
					m_errorMessage = errorElement.InnerText;

					index = m_errorMessage.IndexOf("Access");
					if (index > 0)
						m_errorMessage = m_errorMessage.Substring(index);
				}

				// 2/12/2007 nw - OPM 9451 - Replace login failed error message if it contains user login/password
				// Assume error message is of this format - "Access denied: loginUsed passwordUsed"
				index = m_errorMessage.IndexOf("Access denied");
				if (index == 0)
					m_errorMessage = "Access denied.  Please make sure you entered the correct login and password.";

				// If there is no custom agency information then return generic message.
				string msg = "If you are unable to login for credit after several tries, please contact your Credit Reporting Agency for assistance.";

				m_errorMessage += "<br>" + msg;
			} 
			else 
			{
				// No error occurs.
				m_isReady = true;
				m_status = "READY";
				m_reportID = creditResponseElement.GetAttribute("CreditReportIdentifier");

				XmlElement statusElement = (XmlElement) doc.SelectSingleNode("//RESPONSE_GROUP/RESPONSE/STATUS");
				if (null != statusElement) 
				{
					// File is being processed.
					if (statusElement.GetAttribute("_Code") == "S010")
					{
						m_isReady = false;
						m_status = "";
					}
				}
			}

			if (m_isReady) 
				m_rawXmlResponse = doc.InnerXml;
		}

		#region Implementation of ICreditReportResponse
		public string ReportID 
		{
			get { return m_reportID; }
		}
		public string Status 
		{
			get { return m_status; }
		}

		public bool HasError 
		{
			get { return m_hasError; }
		}
		public string ErrorMessage 
		{
			get { return m_errorMessage; }
		}

		public bool IsReady 
		{
			get { return m_isReady; }
		}

		public string RawXmlResponse 
		{
			get { return m_rawXmlResponse; }
        }

        public string BrandedProductName { get; }
        #endregion
    }
}
