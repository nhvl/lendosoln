using System;
using System.Xml;

using LendersOffice.CreditReport;

namespace LendersOffice.CreditReport.Mcl
{
    public class MclCreditReportResponse : ICreditReportResponse
    {
        private string m_reportID;
        private string m_status;
        private bool m_hasError;
        private bool m_isReady;
        private string m_errorMessage;
        private string m_rawXmlResponse = "";


        public MclCreditReportResponse(XmlDocument doc)
        {
            Parse(doc);
        }

        private void Parse(XmlDocument doc) 
        {
            XmlElement orderDetailElement = (XmlElement) doc.SelectSingleNode("//OUTPUT/RESPONSE/ORDER_DETAIL[1]");

            XmlElement errorElement = (XmlElement) doc.SelectSingleNode("//OUTPUT/RESPONSE/INETAPI_ERROR[1]");
            XmlElement errorElement2 = (XmlElement) doc.SelectSingleNode("//OUTPUT/INETAPI_ERROR[1]");
            if (null != errorElement) 
            {
                // 12/23/2003 dd - NoErrorCode=0, Miscellaneous=1, LoginFailure=2, LockedOut=3, TicketExpired=4
                string code = errorElement.GetAttribute("error_code");
                switch (code) 
                {
                    case "2": 
                        m_status = "LOGIN_FAILED";
                        m_errorMessage = errorElement.InnerText + "<br>If you are unable to login for credit after several tries, please contact your Credit Reporting Agency for assistance.";
                        break;
                    default:
                        m_status = "ERROR";
                        m_errorMessage = errorElement.InnerText + "<br>If you are unable to login for credit after several tries, please contact your Credit Reporting Agency for assistance.";
                        break;

                }
                m_isReady = false;
                m_hasError = true;
            } 
            else if (null != errorElement2) 
            {
				m_status = "ERROR";
                m_errorMessage = errorElement2.InnerText + "<br>If you are unable to login for credit after several tries, please contact your Credit Reporting Agency for assistance.";
                m_isReady = false;
                m_hasError = true;

            }
            else if (null != orderDetailElement) 
            {
                m_reportID = orderDetailElement.GetAttribute("report_id");
                m_status = orderDetailElement.GetAttribute("status_code");
                m_isReady = m_status == "READY";

                if (m_status == "ERROR") 
                {
                    m_hasError = true;
                    m_errorMessage = "Error while ordering report.<br>If you are unable to login for credit after several tries, please contact your Credit Reporting Agency for assistance.";
                }
            }

            if (m_isReady) 
            {
                m_rawXmlResponse = doc.InnerXml;
            }


        }

        #region Implementation of ICreditReportResponse
        public string ReportID 
        {
            get { return m_reportID; }
        }
        public string Status 
        {
            get { return m_status; }
        }

        public bool HasError 
        {
            get { return m_hasError; }
        }
        public string ErrorMessage 
        {
            get { return m_errorMessage; }
        }

        public bool IsReady 
        {
            get { return m_isReady; }
        }
        public string RawXmlResponse 
        {
            get { return m_rawXmlResponse; }
        }

        public string BrandedProductName { get; }
        #endregion
    }
}
