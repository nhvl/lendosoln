using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;

using CommonLib;
using LendersOffice.CreditReport;
using LendersOffice.Common;
using LendersOffice.Constants;

namespace LendersOffice.CreditReport.Mcl
{
    public enum MclRequestType 
    {
        New, Upgrade, Get, Refresh
    }

    public enum MclOutputFormat 
    {
        MBA86, FNMA3, HTML, HTML_INLINDED, XML, TEXT, PDF_HEX, PDF_BASE64, MISMO1_0_1, MISMO1_1, MISMO2_1, MISMO2_3_1
    }

    public enum MclCreditReportType
    {
        Standard, MortageOnly, MortgageOnlyWithCreditScores, MortgageOnlyWithScoresAndFactors
    }

	/// <summary>
	/// Summary description for MclCreditReportRequest.
	/// </summary>
	public class MclCreditReportRequest : ICreditReportRequest
	{
		#region variable declerations 
        const string m_url	   = "https://credit.meridianlink.com";//av opm 25327 10 27 08
        private LoginInfo m_loginInfo;

        private BorrowerInfo m_borrower;
        private BorrowerInfo m_coborrower;
        private string m_instantViewID = "";
        private string m_reportID = "";
        private bool m_includeExperian;
        private bool m_includeEquifax;
        private bool m_includeTransunion;
        private bool m_includeFannie;
        private bool m_useCreditCardPayment = false;
        private string m_billingFirstName = "";
        private string m_billingSurName = "";
        private string m_cardNumber = "";
        private string m_billingStreet = "";
        private string m_billingCity = "";
        private string m_billingState = "";
        private string m_billingZip = "";
        private int m_expirationMonth = 0;
        private int m_expirationYear = 0;
        private string m_billingCvv = "";
		private string m_referenceNumber = ""; //OPM 15995
	
        private MclRequestType m_requestType = MclRequestType.New;
        private MclCreditReportType m_creditReportType = MclCreditReportType.Standard;
        private List<MclOutputFormat> m_outputFormat = new List<MclOutputFormat>(30);
		private string m_sProviderId = ""; //av opm 25327 10 27 08
		#endregion
        #region "Public properties"
		public MclCreditReportRequest()
		{
            m_borrower = new BorrowerInfo();
            m_coborrower = new BorrowerInfo(true);
            m_loginInfo = new LoginInfo();

		}

		//av opm 25327 10 27 08
		/// <summary>
		/// Gets and sets Provider ID
		/// </summary>
		public string ProviderID  
		{
			get { return this.m_sProviderId; }
			set { this.m_sProviderId = value;}
		}
        public bool UseCreditCardPayment 
        {
            get { return m_useCreditCardPayment; }
            set { m_useCreditCardPayment = value; }
        }

		//OPM 15995
		public string ReferenceNumber 
		{
			get { return m_referenceNumber; }
			set { m_referenceNumber = value; }
		}

        public string BillingFirstName 
        {
            get { return m_billingFirstName; }
            set { m_billingFirstName = value; }
        }
        public string BillingSurName 
        {
            get { return m_billingSurName; }
            set { m_billingSurName = value; }
        }
        public string CardNumber 
        {
            get { return m_cardNumber; }
            set { m_cardNumber = value; }
        }
        public string BillingStreet 
        {
            get { return m_billingStreet; }
            set { m_billingStreet = value; }
        }
        public string BillingCity 
        {
            get { return m_billingCity; }
            set { m_billingCity = value; }
        }
        public string BillingState 
        {
            get { return m_billingState; }
            set { m_billingState = value; }
        }
        public string BillingZip 
        {
            get { return m_billingZip; }
            set { m_billingZip = value; }
        }
        public int ExpirationMonth 
        {
            get { return m_expirationMonth; }
            set { m_expirationMonth = value; }
        }
        public int ExpirationYear 
        {
            get { return m_expirationYear; }
            set { m_expirationYear = value; }
        }
        public string BillingCvv 
        {
            get { return m_billingCvv; }
            set { m_billingCvv = value; }
        }


            public BorrowerInfo Borrower 
        {
            get { return m_borrower; }
        }

        public BorrowerInfo Coborrower 
        {
            get { return m_coborrower; }
        }

        public LoginInfo LoginInfo 
        {
            get { return m_loginInfo; }
        }

        public string InstantViewID 
        {
            get { return m_instantViewID; }
            set { m_instantViewID = value; }
        }

        public string ReportID
        {
            get { return m_reportID; }
            set { m_reportID = value; }
        }

        public bool IncludeExperian
        {
            get { return m_includeExperian; }
            set { m_includeExperian = value; }
        }

        public bool IncludeEquifax
        {
            get { return m_includeEquifax; }
            set { m_includeEquifax = value; }
        }

        public bool IncludeTransunion
        {
            get { return m_includeTransunion; }
            set { m_includeTransunion = value; }
        }

        public bool IncludeFannie
        {
            get { return m_includeFannie; }
            set { m_includeFannie = value; }
        }
        
        public MclRequestType RequestType
        {
            get { return m_requestType; }
            set { m_requestType = value; }
        }
        
        // OPM 134848
        public MclCreditReportType CreditReportType
        {
            get { return m_creditReportType; }
            set { m_creditReportType = value; }
        }
        #endregion 

        public void AppendOutputFormat(MclOutputFormat format) 
        {
            if (!m_outputFormat.Contains(format)) 
            {
                m_outputFormat.Add(format);
            }
        }


        #region Implementation of ICreditReportRequest
        public XmlDocument GenerateXml()
        {
            XmlDocument doc = new XmlDocument();

            XmlElement rootElement = doc.CreateElement("INPUT");
            doc.AppendChild(rootElement);

            #region "Create <USER_INFO>"
            XmlElement userInfoElement = CreateUserInfoElement(doc);

            rootElement.AppendChild(userInfoElement);
            #endregion 

            #region Create <REQUEST>
            XmlElement requestElement = doc.CreateElement("REQUEST");
            requestElement.SetAttribute("ml_source", "Lender's Office");
			//use for testing to bypass the duplicate check
			//requestElement.SetAttribute("test_suppress_dup_check", "T");
            rootElement.AppendChild(requestElement);

            switch (m_requestType) 
            {
                case MclRequestType.New: CreateRequest(requestElement, "NEW"); break;
                case MclRequestType.Get: CreateGetRequest(requestElement); break;
				case MclRequestType.Upgrade: CreateRequest(requestElement, "UPGRADE"); break; //<john>
                case MclRequestType.Refresh: CreateRequest(requestElement, "REFRESH"); break;
            }
            #endregion



            return doc;
        }

        public byte[] GeneratePostContent() 
        {
            XmlDocument doc = GenerateXml();

            return System.Text.Encoding.UTF8.GetBytes(doc.InnerXml);

        }
        public ICreditReportResponse CreateResponse(XmlDocument doc)
        {
            return new MclCreditReportResponse(doc);
        }

        public string Url
        {
            get 
            {
                string url = m_url;
				//url = url.ToLower().Replace("https://", "http://");
                url +=  //OPM 21340 antoniov  05/06/08 filters out MCL_TEST_SERVER stuff
                        "/inetapi/AU/get_credit_report_mclxml.aspx "; // : "/inetapi/get_credit.aspx"; 
				return url;
            }
        }
        public void SetupAuthentication(System.Net.HttpWebRequest request) 
        {
			//OPM 21340 antoniov  05/06/08 
		
				request.ContentType = "application/xml";
			
        }
        #endregion

        #region Private function creating XmlElement
        private XmlElement CreateUserInfoElement(XmlDocument doc) 
        {
            XmlElement userInfoElement = doc.CreateElement("USER_INFO");

            if (!string.IsNullOrEmpty(m_instantViewID)) 
            {
                    userInfoElement.SetAttribute("login", string.Format("INSTANTVIEW-{0}", m_reportID));
                    userInfoElement.SetAttribute("password", m_instantViewID);
            } 
            else 
            {
                userInfoElement.SetAttribute("login", m_loginInfo.UserName);
                userInfoElement.SetAttribute("password", m_loginInfo.Password);
				userInfoElement.SetAttribute("provider_id", m_sProviderId ); 

				if ( string.IsNullOrEmpty( m_sProviderId ) ) 
				{
					DataAccess.Tools.LogWarning("MCL CREDIT REQUEST: EMPTY PROVIDER ID \nStack :" + System.Environment.StackTrace ); 
				}
            }
			 //OPM 21340 antoniov  05/06/08 
                userInfoElement.SetAttribute("interface", m_loginInfo.UserName.ToLower() == "pml_test" || (!string.IsNullOrEmpty( m_instantViewID ) && m_instantViewID.StartsWith("CC-") )
					? "LENDERS_OFFICE_TEST" : "LENDERS_OFFICE");
			
			return userInfoElement;
        }
        private XmlElement CreateApplicantElement(XmlDocument doc, bool isCoborrower) 
        {
            string code = "";
            BorrowerInfo borrower = isCoborrower ? m_coborrower : m_borrower;
            XmlElement element = null;


            // 12/19/2003 dd - First name, last name, & SSN are required attributes. If any of those
            // fields empty then return null.
            if (borrower.FirstName.TrimWhitespaceAndBOM() != "" && borrower.LastName.TrimWhitespaceAndBOM() != "" && borrower.Ssn.TrimWhitespaceAndBOM() != "") 
            {
                // 12/18/2003 dd - If borrower then create <APPLICANT>, else create <SPOUSE>
                element = doc.CreateElement(isCoborrower ? "SPOUSE" : "APPLICANT");

                element.SetAttribute("firstname", borrower.FirstName.ToUpper());
                element.SetAttribute("middlename", borrower.MiddleName.ToUpper());
                element.SetAttribute("surname", borrower.LastName.ToUpper());
                // 12/19/2003 dd - Suffix code 1 = SR, 2 = JR, 3 = III, 4 = IV, 5 = I, 6 = II 
                string suffix = borrower.Suffix.ToUpper().TrimWhitespaceAndBOM();
                code = "";
                switch (suffix) 
                {
                    case "SR.":
                    case "SR": code = "1"; break;
                    case "JR.":
                    case "JR": code = "2"; break;
                    case "III": code = "3"; break;
                    case "IV": code = "4"; break;
                    case "I": code = "5"; break;
                    case "II": code = "6"; break;
                }

                // 12/19/2003 dd - Only export suffix if code is valid.
                if ("" != code) 
                {
                    element.SetAttribute("suffix", code);
                }

                // 12/19/2003 dd - SSN must be 9 digits, no dashes
                element.SetAttribute("ssn", borrower.Ssn.TrimWhitespaceAndBOM().Replace("-", ""));
                element.SetAttribute("dob", borrower.DOB);
  
            }

            return element;
        }

        // 12/19/2003 dd - Append information requires for new request. <REQUEST> element must already existed.
        private void CreateRequest(XmlElement request, string type) 
        {
			request.SetAttribute("request_type", type);
            request.SetAttribute("async", "T");

            XmlElement el = null;

            el = CreateOrderDetailElement(request.OwnerDocument);
            if (null != el) request.AppendChild(el);

            el = CreateApplicantElement(request.OwnerDocument, false); // 12/19/2003 dd - Create APPLICANT
            if (null != el) request.AppendChild(el);

            el = CreateApplicantElement(request.OwnerDocument, true); // 12/19/2003 dd - Create SPOUSE
            if (null != el) request.AppendChild(el);

            el = CreateCreditCardPaymentElement(request.OwnerDocument);
            if (null != el) request.AppendChild(el);

            el = CreateResidenceAddressElement(request.OwnerDocument, true);
            if (null != el) request.AppendChild(el);

            el = CreateResidenceAddressElement(request.OwnerDocument, false);
            if (null != el) request.AppendChild(el);

            CreateOuputFormatElement(request);
        }

        private void CreateGetRequest(XmlElement request) 
        {
            request.SetAttribute("request_type", "GET");
            request.SetAttribute("async", "T");


            XmlElement el = CreateOrderDetailElement(request.OwnerDocument);
            if (null != el) request.AppendChild(el);
			
			if (  m_loginInfo.UserName.Length != 0 	&& m_loginInfo.Password.Length != 0  && m_reportID.Length > 0 )
			{
				el = CreateApplicantElement( request.OwnerDocument, false );
				if ( el != null ) request.AppendChild( el );  
				el = CreateApplicantElement( request.OwnerDocument, true ); 
				if ( el != null ) request.AppendChild( el );
			}


            CreateOuputFormatElement(request);
        }

        private XmlElement CreateOrderDetailElement(XmlDocument doc) 
        {
            XmlElement el = doc.CreateElement("ORDER_DETAIL");

            if (m_reportID.Length > 0) 
            {
                el.SetAttribute("report_id", m_reportID);
            }
            if (m_creditReportType != MclCreditReportType.Standard)
            {
                switch (m_creditReportType)
                {
                    case MclCreditReportType.MortgageOnlyWithScoresAndFactors:
                        el.SetAttribute("report_type", "MTGONLY_SCORE_FACTORS");
                        break;
                    case MclCreditReportType.MortgageOnlyWithCreditScores:
                        el.SetAttribute("report_type", "MTGONLY_SCORE");
                        break;
                    case MclCreditReportType.MortageOnly:
                        el.SetAttribute("report_type", "MTGONLY");
                        break;
                    // If m_creditReportType is not one of the above three types, assume
                    // a standard credit report and do not include the report_type attribute
                    default:
                        break;
                }
            }
            if (m_requestType == MclRequestType.New || m_requestType == MclRequestType.Upgrade || m_requestType == MclRequestType.Refresh)  //<john>
            {
				el.SetAttribute("reference_number", m_referenceNumber); //OPM 15995
				el.SetAttribute("experian_credit_flag", m_includeExperian ? "T" : "F");
                el.SetAttribute("experian_score_flag", m_includeExperian ? "T" : "F");
                el.SetAttribute("experian_fraud_flag", m_includeExperian ? "T" : "F");
                el.SetAttribute("transunion_credit_flag", m_includeTransunion ? "T" : "F");
                el.SetAttribute("transunion_score_flag", m_includeTransunion ? "T" : "F");
                el.SetAttribute("transunion_fraud_flag", m_includeTransunion ? "T" : "F");
                el.SetAttribute("equifax_credit_flag", m_includeEquifax ? "T" : "F");
                el.SetAttribute("equifax_score_flag", m_includeEquifax ? "T" : "F");            
                el.SetAttribute("equifax_fraud_flag", m_includeEquifax ? "T" : "F");      
                el.SetAttribute("fannie_flag", m_includeFannie ? "T" : "F");
            }

            return el;

        }

        private XmlElement CreateResidenceAddressElement(XmlDocument doc, bool isCurrent) 
        {
            // 12/19/2003 dd - Current & Previous address use the information in borrower.
            XmlElement el = doc.CreateElement(isCurrent ? "CURRENT_ADDRESS" : "PREVIOUS_ADDRESS");

            Address address = isCurrent ? m_borrower.CurrentAddress : m_borrower.PreviousAddress;
            
            if (null != address) 
            {
                el.SetAttribute("streetaddress", address.StreetAddress);
                el.SetAttribute("city", address.City.ToUpper());
                el.SetAttribute("state", address.State.ToUpper());
                el.SetAttribute("zip", address.Zipcode);
                el.SetAttribute("years_at_address", isCurrent ? m_borrower.YearsAtCurrentAddress : m_borrower.YearsAtPreviousAddress);
            }
            return el;
        }
        private void CreateOuputFormatElement(XmlElement request) 
        {
            XmlDocument doc = request.OwnerDocument;

            foreach (MclOutputFormat format in m_outputFormat)
            {
                XmlElement el = doc.CreateElement("OUTPUT_FORMAT");

                string value = "";
                switch (format)
                {
                    case MclOutputFormat.MBA86: value = "MBA86"; break;
                    case MclOutputFormat.FNMA3: value = "FNMA3"; break;
                    case MclOutputFormat.HTML: value = "HTML"; break;
                    case MclOutputFormat.HTML_INLINDED: value = "HTML-INLINED"; break;
                    case MclOutputFormat.XML: value = "XML"; break;
                    case MclOutputFormat.TEXT: value = "TEXT"; break;
                    case MclOutputFormat.PDF_HEX: value = "PDF-HEX "; break;
                    case MclOutputFormat.PDF_BASE64: value = "PDF-BASE64 "; break;
                    case MclOutputFormat.MISMO1_0_1: value = "MISMO1_0_1"; break;
                    case MclOutputFormat.MISMO1_1: value = "MISMO1_1 "; break;
                    case MclOutputFormat.MISMO2_1: value = "MISMO2_1"; break;
                    case MclOutputFormat.MISMO2_3_1: value = "MISMO2_3_1"; break;
                }

                el.SetAttribute("format_type", value);
                request.AppendChild(el);
            }

        }

        private XmlElement CreateCreditCardPaymentElement(XmlDocument doc) 
        {
            XmlElement el = null;

            if (m_useCreditCardPayment) 
            {
                el = doc.CreateElement("CREDITCARD_PAYMENT");
                el.SetAttribute("billing_firstname", m_billingFirstName);
                el.SetAttribute("billing_surname", m_billingSurName);
                el.SetAttribute("card_number", m_cardNumber);
                el.SetAttribute("billing_street", m_billingStreet);
                el.SetAttribute("billing_city", m_billingCity);
                el.SetAttribute("billing_state", m_billingState);
                el.SetAttribute("billing_zip", m_billingZip);
                el.SetAttribute("expiration_month", m_expirationMonth.ToString());
                el.SetAttribute("expiration_year", m_expirationYear.ToString());
                el.SetAttribute("cvv", m_billingCvv);
            }
            return el;
        }
        #endregion
	}
}
