using System;
using System.Collections;
using System.Xml;
using System.Text;

using LendersOffice.CreditReport;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.CreditReport.Mcl.Code;
using LendersOffice.Constants;
using LendersOffice.Email;

namespace LendersOffice.CreditReport.Mcl
{
    /// <summary>
    /// 
    /// </summary>
    /// <remarks>
    /// 1. Collection: is an event that lasts for some time. It's more of a state.
    ///    a. The affective date should be of the first date the collection event shows up in payment history not the last one--based on a search of past to now (reverse of what we actually do in the code)
	/// 2. ChargeOff, Repossession, Foreclosure: one time event
    ///    a. The affective date is the last one we find--based on a search of past to now (reverse of what we actually do in the code) 
    /// 3. For public record: always use "status date" for all date inquiries as it's the one represent when the bank decides the public record to have the new status. The report date is when the bureau receives the last update.
    /// 4. Late 30 and 60 and 90 that are already parsed (shown in the credit report printout) they are non-rolling non-progressive.
    /// 5. The ( balance > 0 ) test to determine if an account is unpaid is applicable for only and always for tradeline (not public record)
    /// </remarks>
	public class MclCreditReport : AbstractCreditReport 
	{
		private bool m_swapPrimaryBorrower = false;
		private XmlDocument m_doc;
		private CreditScore m_borrowerScore;
		private CreditScore m_coborrowerScore;
		private BorrowerInfo m_borrowerInfo = null;
		private BorrowerInfo m_coborrowerInfo = null;
		private CreditBureauInformation m_experianContactInformation = CreditBureauInformation.DefaultExperianInformation;
		private CreditBureauInformation m_transUnionContactInformation = CreditBureauInformation.DefaultTransUnionInformation;
		private CreditBureauInformation m_equifaxContactInformation = CreditBureauInformation.DefaultEquifaxInformation;

		private Hashtable m_creditors = new Hashtable();
		private ArrayList m_liabilities;
		private ArrayList m_liabilitiesExcludeFromUnderwriting;
		private ArrayList m_publicRecords;
		private ILiaCollection m_liaColl;
		private IPublicRecordCollection m_publicRecordCollection;
		private DateTime m_creditReportFirstIssuedDate = DateTime.MinValue;
        private DateTime m_creditReportLastUpdatedDate = DateTime.MinValue;

        #region Define Abstract Methods
        public override DateTime CreditReportFirstIssuedDate 
		{ 
			get { return m_creditReportFirstIssuedDate; }
		}
        public override DateTime CreditReportLastUpdatedDate
        {
            get { return m_creditReportLastUpdatedDate; }
        }
        public override CreditScore BorrowerScore 
		{ 
			get { return m_borrowerScore; }
		}
		public override CreditScore CoborrowerScore 
		{ 
			get { return m_coborrowerScore; }
		}

		public override BorrowerInfo BorrowerInfo 
		{ 
			get { return m_borrowerInfo; }
		}

		public override BorrowerInfo CoborrowerInfo 
		{ 
			get { return m_coborrowerInfo; }
		}
		public override CreditBureauInformation ExperianContactInformation 
		{ 
			get { return m_experianContactInformation; }
		}
		public override CreditBureauInformation TransUnionContactInformation 
		{ 
			get { return m_transUnionContactInformation; }
		}
		public override CreditBureauInformation EquifaxContactInformation 
		{ 
			get { return m_equifaxContactInformation; }
		}

		public override ArrayList AllLiabilities 
		{ 
			get { return m_liabilities; }
		}

		public override ArrayList LiabilitiesExcludeFromUnderwriting 
		{
			get { return m_liabilitiesExcludeFromUnderwriting; }
		}
		public override ArrayList AllPublicRecords 
		{ 
			get { return m_publicRecords; }
		}

		/// <summary>
		/// This method will also invoke Parse() method after finished. DO NOT CALL Parse() again.
		/// </summary>
		/// <param name="liaCollection"></param>
		public override void SetLoanData(CAppBase dataApp) 
		{
			m_liaColl = dataApp.aLiaCollection;
			m_publicRecordCollection = dataApp.aPublicRecordCollection;
			m_swapPrimaryBorrower = dataApp.aIsBorrSpousePrimaryWageEarner;
            m_dataApp = dataApp;
			Parse();
		}
        private CAppBase m_dataApp = null;
        public override CAppBase DataApp
        {
            get { return m_dataApp; }
        }

		public override bool SwapPrimaryBorrower
		{
			get { return m_swapPrimaryBorrower; }
		}

		public override string GetLiabilityXml(string id) 
		{
			string xml = "";
			XmlNode rootNode = m_doc.SelectSingleNode("/OUTPUT/RESPONSE/OUTPUT_FORMAT[@format_type='XML']");
			
			if (rootNode != null) 
			{
				XmlDocument creditDoc = new XmlDocument();
				creditDoc.LoadXml(rootNode.InnerText);
				XmlElement rootElement = creditDoc.DocumentElement;
				XmlNode node = rootElement.SelectSingleNode("/CREDITDATA/TRADELINE[@id='" + id + "']");
				if (null != node)
					xml = node.OuterXml;

			}

			return xml;
		}
		public override string GetPublicRecordXml(string id) 
		{
			string xml = "";
			XmlNode rootNode = m_doc.SelectSingleNode("/OUTPUT/RESPONSE/OUTPUT_FORMAT[@format_type='XML']");
			
			if (rootNode != null) 
			{
				XmlDocument creditDoc = new XmlDocument();
				creditDoc.LoadXml(rootNode.InnerText);
				XmlElement rootElement = creditDoc.DocumentElement;
				XmlNode node = rootElement.SelectSingleNode("/CREDITDATA/PUBLIC_RECORD[@id='" + id + "']");
				if (null != node)
					xml = node.OuterXml;

			}
			return xml;
		}
        #endregion



		internal MclCreditReport(CreditReportFactory factory, XmlDocument doc, CreditReportDebugInfo extraDebugInfo) 
		{
			if (null == factory) 
			{
				throw new CBaseException(ErrorMessages.Generic, "MclCreditReport can only be instantiate by CreditReportFactory.");
			}

			ExtraDebugInfo = extraDebugInfo;

			m_doc = doc;
			m_borrowerScore = new CreditScore();
			m_coborrowerScore = new CreditScore();
			
#if( DEBUG )
			Tools.LogRegTest( string.Format( "Here is the credit report xml doc:\n{0}", m_doc.InnerXml ) );
#endif
		}

		/// <summary>
		/// Call this method before invoke any method regarding the credit report liabilities/public records.
		/// </summary>
		public override void Parse() 
		{

			XmlNode rootNode = m_doc.SelectSingleNode("/OUTPUT/RESPONSE/OUTPUT_FORMAT[@format_type='XML']");
			
			if (rootNode == null) 
			{
				return;
			}
			XmlDocument creditDoc = new XmlDocument();
			creditDoc.LoadXml(rootNode.InnerText);
			
			/*
			//FOR DEBUGGING EXTERNAL XML CREDIT REPORT
			XmlDocument creditDoc = new XmlDocument();
			creditDoc.LoadXml( m_doc.OuterXml );
			*/


			XmlElement rootElement = creditDoc.DocumentElement;
            #region Parse Order Date
			XmlElement headers = (XmlElement) rootElement.SelectSingleNode("HEADER");
			if (null != headers) 
			{
				m_creditReportFirstIssuedDate = MclUtilities.ParseDateTime(headers.GetAttribute("date_ordered"), "mm/dd/yy");
			}
            #endregion

            #region Parse Borrower & Coborrower Information
			XmlNodeList borrowers = rootElement.GetElementsByTagName("CONSUMER");
			foreach (XmlElement node in borrowers) 
			{
				bool isBorrower = node.GetAttribute("consumer_code") == "PC";
				LendersOffice.CreditReport.BorrowerInfo borrower = null;
				if (isBorrower) 
				{
					m_borrowerInfo = new BorrowerInfo();
					borrower = m_borrowerInfo;
				} 
				else 
				{
					m_coborrowerInfo = new BorrowerInfo(true);
					borrower = m_coborrowerInfo;
				}
				borrower.FirstName = node.GetAttribute("firstname");
				borrower.MiddleName = node.GetAttribute("middlename");
				borrower.LastName = node.GetAttribute("lastname");
				// 7/28/2004 dd - TODO Convert suffix enumeration from MCL to description.  suffix
				borrower.DOB = node.GetAttribute("dob");
				borrower.MismoSsn = node.GetAttribute("ssn");
				// 7/28/2004 dd - TODO Marital Status.   marital_status

			}
            #endregion

            #region Parse Credit Scores
			XmlNodeList riskModels = rootElement.GetElementsByTagName("RISK_MODEL");

			foreach (XmlElement node in riskModels) 
			{
				string strScore = node.GetAttribute("risk_score");
				string source = node.GetAttribute("source_bureau");
				string code = node.GetAttribute("consumer_code");
				string modelName = node.GetAttribute("model_name") ?? "";
                string strPercentile = node.GetAttribute("percentile");

                CreditScore creditScore = code == "SC" ? m_coborrowerScore : m_borrowerScore;
				int score = 0;
				if (strScore != null || strScore.TrimWhitespaceAndBOM() != "") 
				{
					try 
					{
						score = int.Parse(strScore);

						if (score > 1500)
							score = 0; // 9/12/2005 dd - OPM #2832 - If score greater than 1500 then probably it is an error code.
					} 
					catch (FormatException) {}
				}

                // OPM 61058 - Parse credit score percentile from report.
                LosConvert convert = new LosConvert();
                int percentile = convert.ToCount(strPercentile);

                if (source == "XP") 
				{
					if (score != 0) 
					{                         //requested per shevonne -- this is the old mcl xml 
                        if ((creditScore.Experian == 0) || modelName.ToLower().Contains("isaac")) 
					    {
							creditScore.Experian = score;
							creditScore.ExperianModelName = modelName;
                            creditScore.ExperianPercentile = percentile;
						}
					}
                    
				} 
				else if (source == "EF") 
				{
					if (score != 0) 
					{
                        //av opm 43099 temporary fix for LPQ. If there are more models encountered for EF then only overwrite existing if the name contains beacon which means fico for equifax. 
						if ((creditScore.Equifax == 0) || modelName.ToLower().Contains("beacon") )
						{
							creditScore.Equifax = score;
							creditScore.EquifaxModelName = modelName;
                            creditScore.EquifaxPercentile = percentile;
                        }
					}
				} 
				else if (source == "TU") 
				{
					if (score != 0) 
					{
						if ((creditScore.TransUnion == 0) || (score < creditScore.TransUnion)) 
						{
							creditScore.TransUnion = score;
							creditScore.TransUnionModelName = modelName;
                            creditScore.TransUnionPercentile = percentile;
                        }
					}
				}
				XmlNodeList factorList = node.SelectNodes("RISK_FACTOR");
				foreach (XmlElement o in factorList)
					creditScore.AddFactor(source, o.GetAttribute("factor_text"));
			}
            #endregion

			// 5/9/2007 nw - OPM 12892 - We need to parse public records before we parse liabilities
			#region Parse Public Records
			if (null == m_publicRecordCollection || m_publicRecordCollection.CountRegular == 0)
			{
				XmlNodeList publicRecords = rootElement.SelectNodes("/CREDITDATA/PUBLIC_RECORD");
				m_publicRecords = new ArrayList();
				foreach (XmlElement element in publicRecords) 
				{
					m_publicRecords.Add(new MclCreditPublicRecord(element, this));
				}
			}
			else
			{
				m_publicRecords = new ArrayList();
				for (int i = 0; i < m_publicRecordCollection.CountRegular; i++)
				{
					var field = m_publicRecordCollection.GetRegularRecordAt(i);
					if (field.IncludeInPricing)
						m_publicRecords.Add(new MclCreditPublicRecord(field, this));
				}
			}
            #endregion

            #region Parse Liability tradelines
			ArrayList userLiabilityList = null;
			if (null != m_liaColl) 
			{
				userLiabilityList = new ArrayList( m_liaColl.CountRegular );

				foreach( ILiabilityRegular lia in m_liaColl.GetSubcollection( true, E_DebtGroupT.Regular ) )
				{
					userLiabilityList.Add( lia );
				}
			}

			XmlNodeList creditors = rootElement.SelectNodes("/CREDITDATA/CREDITOR");
			foreach (XmlElement element in creditors) 
			{
				MclCreditorData data = new MclCreditorData(element);
				m_creditors[data.SubscriberNumber] = data;
			}
			XmlNodeList tradelines = rootElement.SelectNodes("/CREDITDATA/TRADELINE");
			m_liabilities = new ArrayList();
			m_liabilitiesExcludeFromUnderwriting = new ArrayList();

			foreach (XmlElement element in tradelines) 
			{
				string subscriberNumber = element.GetAttribute("subscriber_number");
				MclCreditorData creditor = (MclCreditorData) m_creditors[subscriberNumber];
				if (creditor == null) 
				{

					// Could not find creditor information by subscriber number.
					// Get creditor name in the tradeline element.
					XmlElement creditorElement = rootElement.OwnerDocument.CreateElement("creditor");
					creditorElement.SetAttribute("subscriber_name", element.GetAttribute("creditor_name"));

					creditor = new MclCreditorData(creditorElement);
				}
				AbstractCreditLiability trade = new MclCreditLiability(element, creditor, this);
	
				bool skipTradeline = false;
				if( null != userLiabilityList )
				{
					for (int index = 0; index < userLiabilityList.Count; index++) 
					{
						ILiabilityRegular liaReg = (ILiabilityRegular) userLiabilityList[index];
						if (liaReg.IsSameTradeline(trade.AccountIdentifier, trade.CreditorName)) 
						{
							if (liaReg.Bal != trade.UnpaidBalanceAmount) 
							{
								trade.UpdateUnpaidBalanceAmount(liaReg.Bal);
							}
							if (liaReg.Pmt != trade.MonthlyPaymentAmount) 
							{
								trade.UpdateMonthlyPaymentAmount(liaReg.Pmt);
							}                            
							skipTradeline = liaReg.ExcFromUnderwriting;
							userLiabilityList.RemoveAt(index);
							break;
						}
					}
				}

				if (!skipTradeline)
					m_liabilities.Add( trade );
				else
					m_liabilitiesExcludeFromUnderwriting.Add(trade);
			}
			// If there are remaining liability from user liability then add to the credit report.
			if (null != userLiabilityList) 
			{
				foreach (ILiabilityRegular liaReg in userLiabilityList) 
				{
					if (!liaReg.ExcFromUnderwriting)
						m_liabilities.Add(new CreditLiabilityOptimistic(liaReg, this));
				}
			}
            #endregion

		}

		public override string CreditRatingCodeType
		{
			get { return "MCL Credit Rating Code"; }
		}

	}

}
