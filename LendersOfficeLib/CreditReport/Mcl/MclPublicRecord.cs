using System;
using System.Xml;
using LendersOffice.CreditReport.Mcl.Code;
using DataAccess;

namespace LendersOffice.CreditReport.Mcl
{

    public class MclCreditPublicRecord : AbstractCreditPublicRecord 
    {
        #region Private Member Variables
        private string m_id; // DONE
        private DateTime m_reportedDate = DateTime.MinValue; // DONE
        private DateTime m_dispositionDate = DateTime.MinValue; // DONE
        private E_CreditPublicRecordDispositionType m_dispositionType; // DONE
        private decimal m_bankruptcyLiabilitiesAmount = 0.0M; // DONE
        private E_CreditPublicRecordType m_type; // DONE
        private string m_courtName = "";
        private E_PublicRecordOwnerT m_publicRecordOwnerT = E_PublicRecordOwnerT.Borrower;
        private string m_consumerCode = "";
        #endregion

        #region Implement Abstracts methods.
        public override string ID 
        { 
            get{ return m_id; }
        }

        public override string CourtName 
        {
            get { return m_courtName; }
        }

        public override DateTime ReportedDate 
        { 
            get { return m_reportedDate; } 
        }

        public override DateTime DispositionDate 
        { 
            get { return m_dispositionDate; }
        }

        public override E_CreditPublicRecordDispositionType DispositionType 
        { 
            get { return m_dispositionType; }
        }

        public override decimal BankruptcyLiabilitiesAmount 
        { 
            get { return m_bankruptcyLiabilitiesAmount; }
        }

        public override E_CreditPublicRecordType Type 
        { 
            get { return m_type; }
        }

		override public DateTime BkFileDate 
		{
			get 
			{
				if (ReportedDate != DateTime.MinValue) 
				{
					return ReportedDate;
				} 
				else if (DispositionDate != DateTime.MinValue) 
				{
					//                    Tools.LogWarning("Found a public record with invalid reported date, falling back to Disposition Date.");
					return DispositionDate;
				} 
				else 
				{
					//                    Tools.LogWarning("Found a public record with both invalid Disposition Date and Reported Date, falling back to today date.");
					return DateTime.Today;
				}
			}

		}

		override public DateTime LastEffectiveDate 
		{
			get 
			{
				if (DispositionDate != DateTime.MinValue) 
				{
					return DispositionDate;
				} 
				else if (ReportedDate != DateTime.MinValue) 
				{
					//                    Tools.LogWarning("Found a public record with invalid Disposition Date, falling back to reported date.");
					return ReportedDate;
				}
				else 
				{
					//                    Tools.LogWarning("Found a public record with both invalid Disposition Date and Reported Date, falling back to today date.");
					return DateTime.Today;
				}
			}
		}
        public override E_PublicRecordOwnerT PublicRecordOwnerT
        {
            get { return m_publicRecordOwnerT; }
        }
        #endregion

        public MclCreditPublicRecord(XmlElement el, AbstractCreditReport parent) 
        {
            ParentCreditReport = parent;
            Parse(el);
        }
        private void Parse(XmlElement el) 
        {
            m_id                          = el.GetAttribute("id");
            m_reportedDate                = MclUtilities.ParseDateTime(el.GetAttribute("date_reported"), "mm/dd/yy");
            m_dispositionDate             = MclUtilities.ParseDateTime(el.GetAttribute("status_date"), "mm/dd/yy");
            m_dispositionType             = MapDispositionType(el.GetAttribute("pubrec_status_text"));
            m_bankruptcyLiabilitiesAmount = GetDecimal(el, "liability_amount");
            m_type                        = MapPublicRecordType(el.GetAttribute("pubrec_type_code"));
            m_courtName        = el.GetAttribute("court_name");
            m_consumerCode = el.GetAttribute("consumer_code");
            // Parse these attributes when need.
            //            m_fileID           = GetInt(el, "file_id");
            
            //            m_sourceBureau     = el.GetAttribute("source_bureau");
            
            //            m_subscriberNumber = el.GetAttribute("subscriber_number");
            //            m_plaintiff        = el.GetAttribute("plaintiff");

            //            m_ecoa             = el.GetAttribute("ecoa");
            //            m_referenceNumber  = el.GetAttribute("reference_number");
            //            m_typeText         = el.GetAttribute("pubrect_type_text");
            //            m_isEdited         = GetBool(el, "edited");
            //            m_isSupplement     = GetBool(el, "supplement");
            switch (m_consumerCode)
            {
                case "PC":
                    m_publicRecordOwnerT = E_PublicRecordOwnerT.Borrower;
                    break;
                case "SC":
                    m_publicRecordOwnerT = E_PublicRecordOwnerT.CoBorrower;
                    break;
                case "ALL":
                    m_publicRecordOwnerT = E_PublicRecordOwnerT.Joint;
                    break;
                default:
                    m_publicRecordOwnerT = E_PublicRecordOwnerT.Borrower;
                    break;
            }
        }

		// 3/1/2007 nw - OPM 10631 - Load public records from loan file
		public MclCreditPublicRecord(IPublicRecord field, AbstractCreditReport parent)
		{
			ParentCreditReport = parent;
			Parse(field);
		}
		private void Parse(IPublicRecord field)
		{
			string[] formats = new String[] {"MM/dd/yyyy", "M/dd/yyyy", "MM/d/yyyy", "M/d/yyyy"};

			m_id                          = field.IdFromCreditReport;
			if (field.ReportedD_rep == "")
				m_reportedDate            = DateTime.MinValue;
			else
				m_reportedDate            = DateTime.ParseExact(field.ReportedD_rep, formats, null, System.Globalization.DateTimeStyles.None);
			if (field.DispositionD_rep == "")
				m_dispositionDate         = DateTime.MinValue;
			else
				m_dispositionDate         = DateTime.ParseExact(field.DispositionD_rep, formats, null, System.Globalization.DateTimeStyles.None);
			m_dispositionType             = field.DispositionT;
			m_bankruptcyLiabilitiesAmount = field.BankruptcyLiabilitiesAmount;
			m_type                        = field.Type;
			m_courtName                   = field.CourtName;
            m_publicRecordOwnerT = field.OwnerT;
		}

        private E_CreditPublicRecordType MapPublicRecordType(string typeCode) 
        {
            switch (typeCode) 
            {
                case "AM": return E_CreditPublicRecordType.Attachment;
                case "CP": return E_CreditPublicRecordType.ChildSupport;
                case "SU": return E_CreditPublicRecordType.LawSuit;
                case "FC": return E_CreditPublicRecordType.Foreclosure;
                case "FD": return E_CreditPublicRecordType.ForcibleDetainer;
                case "GN": return E_CreditPublicRecordType.Garnishment;
                case "HA": return E_CreditPublicRecordType.HOALien;
                case "HL": return E_CreditPublicRecordType.HospitalLien;
                case "JL": return E_CreditPublicRecordType.JudicialLien;
                case "TL": return E_CreditPublicRecordType.TaxLienOther;
                case "JM": return E_CreditPublicRecordType.Judgment;
                case "RL": return E_CreditPublicRecordType.RealEstateLien;
                case "ML": return E_CreditPublicRecordType.MechanicLien;
                case "PG": return E_CreditPublicRecordType.PavingAssessmentLien;
                case "TR": return E_CreditPublicRecordType.Trusteeship;
                case "WS": return E_CreditPublicRecordType.WaterAndSewerLien;
                case "B1": return E_CreditPublicRecordType.BankruptcyChapter11;
                case "B2": return E_CreditPublicRecordType.BankruptcyChapter12;
                case "B3": return E_CreditPublicRecordType.BankruptcyChapter13;
                case "BK": // return E_CreditPublicRecordType.BankruptcyTypeUnknown;
                case "B7": 
                    // See OPM 2591. Default all Unknown Bankrupty to Chapter 7.
                    return E_CreditPublicRecordType.BankruptcyChapter7;
                case "CO": return E_CreditPublicRecordType.Collection;
                case "FS": return E_CreditPublicRecordType.FinancingStatement;
                case "NR": return E_CreditPublicRecordType.NonResponsibility;
                case "FL": return E_CreditPublicRecordType.FinancialCounseling;
                case "TA": return E_CreditPublicRecordType.TaxLienFederal;
                case "TB": return E_CreditPublicRecordType.TaxLienState;
                case "TC": return E_CreditPublicRecordType.TaxLienCounty;
                case "TD": return E_CreditPublicRecordType.TaxLienCity;
                default: 
                    LogCreditWarning(typeCode + " is not a valid MCL PublicRecordType. Default to Chapter 7.");
                    return E_CreditPublicRecordType.BankruptcyChapter7; // See OPM 2591
            }

        }


        private E_CreditPublicRecordDispositionType MapDispositionType(string type) 
        {
            if (null == type) throw new ArgumentNullException("Disposition type cannot by null");

            // NOTE: OPM 1845. If the credit file is from TransChicago then MCL just dump out rating remark text.
            // I am trying my best to map the rating remark text to best match DispositionType.

            switch (type.ToUpper()) 
            {
                case "CANCELED": 
                    return E_CreditPublicRecordDispositionType.Canceled;

                case "DISCHARGED": 
                case "BANK DISC": // OPM 1845 - Bankruptcy Discharged
                case "BK 11-DISC": // OPM 1845 - Bankruptcy Chapter 11 Discharged
                case "BK 12-DISC": // OPM 1845 - Bankruptcy Chapter 12 Discharged
                case "BK 13-DISC": // OPM 1845 - Bankruptcy Chapter 13 Discharged
                case "BK 7-DISC": // OPM 1845 - Bankruptcy Chapter 7 Discharged
                case "COLLDISC BK": // OPM 1845 - Collection Discharged - Bankruptcy
                    return E_CreditPublicRecordDispositionType.Discharged;

                case "DISMISSED": 
                case "BANK DISM": // OPM 1845 - Bankruptcy Dismissed
                case "BILL DISM": // OPM 1845 - Separate Bill of Maintenance Dismiss
                case "BK 11-DISM": // OPM 1845 - Bankruptcy 11 Dismissed
                case "BK 12-DISM": // OPM 1845 - Bankruptcy 12 Dismissed
                case "BK 13-DISM": // OPM 1845 - Bankruptcy 13 Dismissed
                case "BK 7-DISM": // OPM 1845 - Bankruptcy 7 Dismissed
                case "DIVORC DISM": // OPM 1845 - Divorce Dismissed
                case "FORE DISM": // OPM 1845 - Dismissed Foreclosure
                case "SUIT DISM": // OPM 1845 - Suit Dismissed
                    return E_CreditPublicRecordDispositionType.Dismissed;

                case "FILED": 
                case "FILING":
                case "BK 11-FILE": // OPM 1845 - Bankruptcy 11 Filed
                case "BK 12-FILE": // OPM 1845 - Bankruptcy 12 Filed
                case "BK 13-FILE": // OPM 1845 - Bankruptcy 13 Filed
                case "BK 7-FILE": // OPM 1845 - Bankruptcy 7 Filed
                case "CIVSUITFILE": // OPM 1845 - Civil Suit Filed
                case "DIVORC FILE": // OPM 1845 - Divorce Filed
                case "JUDGMENT": // OPM 2881.
				case "TX LN OTHER": // OPM 6888
				case "FED TAX LN":  // OPM 6888
				case "STAT TX LN":  // OPM 6888
                    return E_CreditPublicRecordDispositionType.Filed;

                case "INVOLUNTARY": 
                case "BK 7-INVOL": // OPM 1845 - Bankruptcy 7 Involuntary
                    return E_CreditPublicRecordDispositionType.InvoluntarilyDischarged;

                case "NON-ADJUDICATED": 
                case "NON ADJ": // OPM 1845 - Non Adjudicated
                    return E_CreditPublicRecordDispositionType.Nonadjudicated;

                case "NOT RELEASED": 
                    return E_CreditPublicRecordDispositionType.Unreleased;

                case "NOT SATISFIED": 
                case "JUDG PD N/S": // OPM 1845 - Judgment Paid Not Satisfied
                    return E_CreditPublicRecordDispositionType.Unsatisfied;

                case "PAID": 
                case "DISPOSS PD": // OPM 1845 - Dispossessory Paid
                case "TRUST PAID": // OPM 1845 - Trusteeship Paid
				case "JUDGMENT PAID": // OPM 20936 - Judgement Paid
                    return E_CreditPublicRecordDispositionType.Paid;

                case "PLAN COMPLETE": 
                    return E_CreditPublicRecordDispositionType.Completed;

                case "RELEASED": 
                case "CITY LN REL": // OPM 1845 - City Tax Lien Released
                case "CO LN REL": // OPM 1845 - County Tax Lien Released
                case "FED TX REL": // OPM 1845 - Federal Tax Lien Released
                case "REL NTRESP": // OPM 1845 - Release. Non responsible
                case "STAT TX REL": // OPM 1845 - State Tax Lien released
                case "TX RELEASE": // OPM 1845 - Tax Lien Other Released
                case "W/A RELEASE": // OPM 1845 - Wage Assigment Released
                    return E_CreditPublicRecordDispositionType.Released;

                case "RELIEVED IN BANKRUPTCY": 
                    return E_CreditPublicRecordDispositionType.RelievedInBankruptcy;

                case "REVIVED": 
                    return E_CreditPublicRecordDispositionType.Revived;

                case "SATISFIED": 
                case "CIVSUIT SAT": // OPM 1845 - Civil Suit Satisfied
                case "FORE SATIS": // OPM 1845 - Foreclosure Satisfied
                case "JUDG PD SAT": // OPM 1845 - Judgment Paid Satisfied
                case "JUDGMT SAT": // OPM 1845 - Judgment Satisfied
				case "JUDGMENT SATISFIED": // OPM 13073 - Judgment Satisfied
                case "LIEN SAT": // OPM 1845 - Lien Satisfied
                case "R/E ATT SAT": // OPM 1845 - Real Estate Attach Satisfactory
                    return E_CreditPublicRecordDispositionType.Satisfied;

                case "TERMINATED UNSUCCESSFULLY": 
                    return E_CreditPublicRecordDispositionType.TerminatedUnsuccessfully;

                case "VACATED": 
                case "JUDGSATVAC": // OPM 1845 - Jugment Satisfied & Vacated
                    return E_CreditPublicRecordDispositionType.Vacated;

                case "VACATED OR REVERSED": 
                case "JUDG VACAT": // OPM 1845 - Judgment Vacated or Reversed
                    return E_CreditPublicRecordDispositionType.VacatedOrReversed;

                case "VOLUNTARY": 
                    return E_CreditPublicRecordDispositionType.Voluntary;

                case "VOLUNTARY DISMISSAL": 
                    return E_CreditPublicRecordDispositionType.VoluntaryDismissal;

                case "VOLUNTARY WITHDRAWAL": 
                    return E_CreditPublicRecordDispositionType.VoluntaryWithdrawal;

				case "APPEALED":
					return E_CreditPublicRecordDispositionType.Appealed;

				case "SETTLED": // OPM 21668
					return E_CreditPublicRecordDispositionType.Settled;

                default:
                    if (type != "" && type != null)
                        LogCreditWarning("MCL pubrec_status_text=" + type.ToUpper() + "  is not valid type. Default to Filed");
                    return E_CreditPublicRecordDispositionType.Filed;

            }
        }
    }


}
