using System;
using System.Xml;
using LendersOffice.Common;

namespace LendersOffice.CreditReport.Mcl
{
	/// <summary>
	/// Summary description for MclCreditorData.
	/// </summary>
	public class MclCreditorData
	{
        private string m_city = "";
        private string m_state = "";
        private string m_zipcode = "";
        private string m_name = "";
        private string m_subscriberNumber = "";
        private string m_address = "";
        private string m_phone = "";

        public MclCreditorData (XmlElement element) 
        {
            Parse(element);

        }

        private void Parse(XmlElement element) 
        {
            m_subscriberNumber = element.GetAttribute("subscriber_number");
            m_name = element.GetAttribute("subscriber_name");

            if (element.GetAttribute("address2") != null || element.GetAttribute("address2") != "") 
            {
                string address2 = element.GetAttribute("address2");
                // Address2 has the following format  City, State Zip
                string[] tokens = address2.Split(',');
                if (tokens.Length == 2) 
                {
                    m_city = tokens[0].TrimWhitespaceAndBOM();
                    tokens = tokens[1].TrimWhitespaceAndBOM().Split(' ');
                    if (tokens.Length == 2) 
                    {
                        m_state = tokens[0];
                        m_zipcode = tokens[1];
                    }
                }
            }
            m_address = element.GetAttribute("address1");
            m_phone = element.GetAttribute("phone");


        }

        public string SubscriberNumber 
        {
            get { return m_subscriberNumber; }
        }
        public string Name 
        {
            get { return m_name; }
        }
        public string Address 
        {
            get { return m_address; }
        }
        public string City 
        {
            get { return m_city; }
        }
        public string State 
        {
            get { return m_state; }
        }
        public string Zipcode 
        {
            get { return m_zipcode; }
        }
        public string Phone 
        {
            get { return m_phone; }
        }
	}
}
