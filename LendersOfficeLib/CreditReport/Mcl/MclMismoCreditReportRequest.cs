using System;
using System.Xml;
using LendersOffice.CreditReport;
using LendersOffice.CreditReport.Mismo;
using DataAccess;

namespace LendersOffice.CreditReport.Mcl
{
	public class MclMismoCreditReportRequest : LendersOffice.CreditReport.Mismo2_3.Mismo2_3CreditReportRequest
	{
		private const string BETA_URL = "http://beta.mortgagecreditlink.com/inetapi/au/get_credit_report.aspx";
		private const string PRODUCTION_URL = "http://credit.meridianlink.com/inetapi/AU/get_credit_report.aspx";
		private const string SUBMITTING_PARTY_IDENTIFIER = "PML";
		private const string INSTANT_VIEW_ID = "INSTANTVIEW";
		//private string m_instantViewID = "";

		//For future use
		/*public override string InstantViewID 
		{
			get 
			{
				return m_instantViewID; 
			}
			set 
			{ 
				m_instantViewID = value;
				if(!m_instantViewID.TrimWhitespaceAndBOM().Equals(""))
					this.ReceivingPartyIdentifier = "";
			}
		}*/

		public void SetToStatusQuery() 
		{
			this.RequestType = LendersOffice.CreditReport.Mismo.E_CreditReportRequestActionType.StatusQuery;
		}

		public MclMismoCreditReportRequest(string craId) : base(craId)
		{
			this.SubmittingPartyIdentifier = SUBMITTING_PARTY_IDENTIFIER;
			//For future use
			//this.ReceivingPartyIdentifier = craId;

			AppendOutputFormat(E_MismoOutputFormat.Other);
			this.PreferredResponseFormatOtherDescription = "HTML-INLINED";

			// Order PDF file format for MCL Mismo credit report. 
			AppendOutputFormat(E_MismoOutputFormat.PDF);
		}

		public override ICreditReportResponse CreateResponse(XmlDocument doc)
		{
			return new MclMismoCreditReportResponse(doc);
		}

		public override string Url
		{
			get { return PRODUCTION_URL; }
		}
	}
}
