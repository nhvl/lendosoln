using System;
using System.Collections;
using System.Xml;

using LendersOffice.CreditReport;
using DataAccess;
using LendersOffice.CreditReport.Mcl.Code;
using LendersOffice.Common;

namespace LendersOffice.CreditReport.Mcl
{

    public class MclCreditLiability : AbstractCreditLiability 
    {

        #region Private Member Variables
        private string m_id; // DONE
        private string m_creditorName; // DONE
        private string m_creditorAddress; // DONE
        private string m_creditorCity; // DONE
        private string m_creditorState; // DONE
        private string m_creditorZipcode; // DONE
        private string m_creditorPhone; // DONE
        private DateTime m_accountOpenedDate; // DONE
        private DateTime m_accountReportedDate; // DONE
        private decimal m_pastDueAmount; // DONE
        private decimal m_highCreditAmount; // DONE
        private string m_accountIdentifier = ""; // DONE
        private string m_termsMonthsCount; // DONE
        private decimal m_unpaidBalanceAmount; // DONE
        private decimal m_monthlyPaymentAmount; // DONE
        private string m_monthsRemainingCount = "";
        private string m_late30; // DONE
        private string m_late60; // DONE
        private string m_late90; // DONE
        private string m_late120 = "0"; // MCL doesn't have this attribute. Default to 0.
        private int m_monthsReviewedCount; // DONE
        private DateTime m_lastActivityDate; // DONE
        private E_DebtRegularT m_liabilityType; // DONE
        private E_LiaOwnerT m_liaOwnerT; // DONE

        private bool m_isTypeRevolvingBase; // DONE
        private bool m_isTypeInstallmentBase; // DONE
        private string m_accountTypeCode; // DONE

        private bool m_isBankruptcy = false; // DONE
        private bool m_isForeclosure = false; // DONE
        private bool m_isChargeOff = false; // DONE
        private string m_accountStatusCode; // DONE
        private DateTime m_paymentPatternStartDate; // DONE
        private string m_paymentPattern = ""; // DONE
        private string m_lateDates = ""; // DONE
        private bool m_isRepos = false; // DONE
        private bool m_isCollection = false; // DONE
        private bool m_isLate = false; // DONE
        //private bool m_isInactiveButNeutralStatus = false; // DONE
        //private bool m_isInactiveButNegativeStatus = false; // DONE
        private string m_kobCode; // DONE

        private bool m_isActive;
        private bool m_hasBeenDerogStatus;
        private bool m_isDerogCurrently;
        private string m_remarkText;
        private bool m_isInCreditCounseling = false;
        private bool m_hasTerminalDerog = false;
        private bool m_isAuthorizedUser = false;
		private bool m_isGovMiscDebt = false;	// MCL may not have a way to show this info, default to false
		private string m_ecoa;
		private bool m_foreclosureWasStarted = false;

        private ArrayList m_creditAdverseRatingList = new ArrayList();
        private DateTime m_dateLastPastDue;

        #endregion

        #region Implement Abstract Methods
        public override string ID 
        { 
            get { return m_id;  }
        }

        public override string CreditorName 
        { 
            get { return m_creditorName; }
        }

		public override bool IsDischargedBk
		{
			get { return false; }
		}

        public override string CreditorAddress 
        { 
            get { return m_creditorAddress; }
        }

        public override string CreditorCity 
        { 
            get { return m_creditorCity; }
        }
        public override string CreditorState 
        { 
            get { return m_creditorState; }
        }
        public override string CreditorZipcode 
        { 
            get { return m_creditorZipcode; }
        }
        public override string CreditorPhone 
        { 
            get { return m_creditorPhone; }
        }

        public override DateTime AccountOpenedDate 
        { 
            get { return m_accountOpenedDate; }
        }
        public override DateTime AccountReportedDate 
        { 
            get { return m_accountReportedDate; }
        }
        public override decimal PastDueAmount 
        { 
            get { return m_pastDueAmount; }
        }
        public override decimal HighCreditAmount 
        { 
            get { return m_highCreditAmount; }
        }
        public override string AccountIdentifier 
        { 
            get { return m_accountIdentifier; }
        }
        public override string TermsMonthsCount 
        { 
            get { return m_termsMonthsCount; }
        }
        public override decimal UnpaidBalanceAmount 
        { 
            get { return m_unpaidBalanceAmount; }
        }
        public override decimal MonthlyPaymentAmount 
        { 
            get { return m_monthlyPaymentAmount; }
        }
        public override string MonthsRemainingCount
        {
            get { return m_monthsRemainingCount; }
        }
        public override void UpdateUnpaidBalanceAmount(decimal value) 
        {
            LosConvert losConvert = new LosConvert();

            if (m_modifiedString != "")
                m_modifiedString += " ";
            m_modifiedString += "BALANCE ON CREDIT REPORT: " + losConvert.ToMoneyString(m_unpaidBalanceAmount, FormatDirection.ToRep) + ", MODIFIED BALANCE: " + losConvert.ToMoneyString(value, FormatDirection.ToRep) + ".";
            m_unpaidBalanceAmount = value;
            m_isModified = true;
        }
        public override void UpdateMonthlyPaymentAmount(decimal value) 
        {
            LosConvert losConvert = new LosConvert();

            if (m_modifiedString != "")
                m_modifiedString += " ";

            m_modifiedString += "PAYMENT ON CREDIT REPORT: " + losConvert.ToMoneyString(m_monthlyPaymentAmount, FormatDirection.ToRep) + ", MODIFIED PAYMENT: " + losConvert.ToMoneyString(value, FormatDirection.ToRep) + ".";

            m_monthlyPaymentAmount = value;
            m_isModified = true;
        }

        private bool m_isModified = false;
        public override bool IsModified 
        {
            get { return m_isModified; }
        }
        private string m_modifiedString = "";
        public override string ModifiedString 
        {
            get { return m_modifiedString; }
        }

        public override string Late30 
        { 
            get { return m_late30; }
        }
        public override string Late60 
        { 
            get { return m_late60; }
        }
        public override string Late90 
        { 
            get { return m_late90; }
        }
        public override string Late120 
        { 
            get { return m_late120; } // Only define in Mismo.
        }

        public override int MonthsReviewedCount 
        { 
            get { return m_monthsReviewedCount; }
        }
        public override DateTime LastActivityDate 
        { 
            get { return m_lastActivityDate; }
        }

        public override E_DebtRegularT LiabilityType 
        { 
            get { return m_liabilityType; }
        }

        public override E_LiaOwnerT LiaOwnerT 
        {
            get { return m_liaOwnerT; }
        }

        public override bool IsTypeRevolvingBase 
        { 
            get { return m_isTypeRevolvingBase; }
        }
        public override bool IsTypeInstallmentBase 
        { 
            get { return m_isTypeInstallmentBase; }
        }

        public override bool IsTypeOther 
        { 
            get 
			{ 
				switch( m_accountTypeCode )
				{
					case CMclCodeTradelineT.Other:
					case CMclCodeTradelineT.Collection: // Only MCL would have the collection as a possible value type, I
																						// believe it should be installment but don't want to make that conclusion
																						// until we know for sure. Currently when we import collection tradelines to loan file
																						// we make it type "other".  -tn 2/1/2006
						return true; 
					default: 
						return false;
				}
			}
        }

		public override bool ForeclosureWasStarted
		{
			get { return m_foreclosureWasStarted; }
		}

        public override bool IsForeclosure 
        { 
            get 
            {
                // 10/24/2008 dd - Only Mortgage can be flag as isForeclosure. See OPM 25630 and 
                // OPM 3465 - Need to make sure never categorize auto reposession as foreclosure.
                return m_isForeclosure && LiabilityType == E_DebtRegularT.Mortgage; 
            }

        }
        public override bool IsBankruptcy 
        { 
            get { return m_isBankruptcy || IsInCreditCounseling; }
        }
        public override bool IsChargeOff 
        {
            get { return m_isChargeOff; }
        }
        public override DateTime PaymentPatternStartDate 
        {
            get { return m_paymentPatternStartDate; }
        }
        public override bool IsCollection
        { 
            get { return m_isCollection; }
        }
        public override bool IsRepos 
        { 
            get { return m_isRepos; }
        }

        public override bool IsLate 
        {
            get { return m_isLate; }
        }

        public override bool IsInCreditCounseling 
        {
            get { return m_isInCreditCounseling; }
        }
            /*
            public override bool IsInactiveButNeutralStatus 
            {
                get { return m_isInactiveButNeutralStatus; }
            }
            public override bool IsInactiveButNegativeStatus 
            {
                get { return m_isInactiveButNegativeStatus; }
            }
            */

            public override ArrayList CreditAdverseRatingList 
        { 
            get { return m_creditAdverseRatingList; }
        }

        public override bool IsMedicalTradeline 
        {
            get { return m_kobCode == CMclCodeKOB.Medical; }
        }
        public override bool IsAuthorizedUser 
        {
            get { return m_isAuthorizedUser; }
        }
		public override bool IsGovMiscDebt 
		{
			get { return m_isGovMiscDebt; }
		}
        public override bool IsStudentLoansNotInRepayment 
        {
            get 
            {
				// 4/6/2007 nw - OPM 12292
				if (m_remarkText.ToUpper().IndexOf("PAYMENT DEFERRED") >= 0 && m_accountTypeCode == "IE")
					return true;

                // 1/30/2006 dd - OPM 3949.
                switch (m_remarkText.ToUpper()) 
                {
                    case "STUDENT LOAN NOT IN REPAYMENT":
                    case "STUDENT LOAN - PAYMENT DEFERRED":
                    case "STUDENT LOAN-PAYMENT DEFERRED":
                        return true;
                    default:
                        return false;
                }
            }

        }
        #endregion

		/* 4/10/2007 nw - OPM 3464 - EndDerogDate is now fully implemented in AbstractCreditLiability.cs
        public override DateTime EndDerogDate
        {
            get 
            {
                // OPM #1813: If "Last Late Date" existed then use it. Foreclosure also uses this field as well.
                if (m_dateLastPastDue != DateTime.MinValue)
                    return m_dateLastPastDue;

                return base.EndDerogDate;
            }
        }
		*/

        public MclCreditLiability(XmlElement el, LendersOffice.CreditReport.Mcl.MclCreditorData creditor, AbstractCreditReport parent) 
        {
            ParentCreditReport = parent;
            Parse(el, creditor);
        }

        private void Parse(XmlElement el, LendersOffice.CreditReport.Mcl.MclCreditorData creditor) 
        {
            m_id = el.GetAttribute("id");
            // The owner code return from Meridian is started with index 1
            // 1 - Borrower, 2 - Coborrower, 3 - Joint. However in the
            // LOS we start with index 0.
            switch (el.GetAttribute("owner")) 
            {
                case "1": m_liaOwnerT = E_LiaOwnerT.Borrower; break;
                case "2": m_liaOwnerT = E_LiaOwnerT.CoBorrower; break;
                case "3": m_liaOwnerT = E_LiaOwnerT.Joint; break;
                default:
                    m_liaOwnerT = E_LiaOwnerT.Borrower;
                    break;
            }
            //av lpq 45130 not part of normal mcl request
            if (el.HasAttribute("will_be_paid"))
            {
                base.ImportWillBePaidOff = true;
                base.WillBePaidOff = el.GetAttribute("will_be_paid") == "T";
            }

            if (el.HasAttribute("deleted"))
            {
                base.Deleted = el.GetAttribute("deleted") == "T";
            }

            m_late30 = el.GetAttribute("day_30").TrimWhitespaceAndBOM();
            m_late60 = el.GetAttribute("day_60").TrimWhitespaceAndBOM();
            m_late90 = el.GetAttribute("day_90").TrimWhitespaceAndBOM();
            m_accountIdentifier = el.GetAttribute("account_number");
            m_termsMonthsCount = el.GetAttribute("term");
            m_unpaidBalanceAmount = GetDecimal(el, "balance");
            m_monthlyPaymentAmount = GetDecimal(el, "payment");
            m_monthsReviewedCount = GetInt(el, "months_reviewed");
            m_lastActivityDate = MclUtilities.ParseDateTime(el.GetAttribute("last_activity"), "yyyymm");
            m_accountOpenedDate = MclUtilities.ParseDateTime(el.GetAttribute("date_open"), "mm/dd/yy");
            m_accountReportedDate = MclUtilities.ParseDateTime(el.GetAttribute("date_reported"), "mm/dd/yy");
            m_pastDueAmount = GetDecimal(el, "past_due");
            m_highCreditAmount = GetDecimal(el, "high_credit");
            m_paymentPattern = el.GetAttribute("two_year_history");
            m_paymentPatternStartDate = MclUtilities.ParseDateTime(el.GetAttribute("history_startdate"), "mm/dd/yy");
            m_lateDates = el.GetAttribute("late_dates");
            m_kobCode = el.GetAttribute("kind_of_business");
            m_dateLastPastDue = MclUtilities.ParseDateTime(el.GetAttribute("date_last_past_due"), "mm/dd/yy");
            m_remarkText = el.GetAttribute("remark_text");

            if (m_unpaidBalanceAmount > 0) 
            {
                // Case 2083 - Only account with blanace > 0 and contains specific substring will be mark as IsInCreditCounseling
                string str = m_remarkText.ToUpper();
                if (str.IndexOf("PAYMENTS MANAGED BY FINANCIAL COUNSELING PROGRAM") >= 0 ||
                    str.IndexOf("ACCOUNT BEING PAID THROUGH FINANCIAL COUNSELING PLAN") >= 0 ||
                    str.IndexOf("ACCOUNT PAYMENTS MANAGED BY DEBT COUNSELING PROGRAM") >= 0 ||
                    str.IndexOf("DEBT COUNSELING SERVICE") >= 0 ||
                    str.IndexOf("MANAGED BY FINANCIAL COUNSELING SERVICE") >= 0) 
                {
                    m_isInCreditCounseling = true;
                }

            }

            m_ecoa = el.GetAttribute("ecoa");
            // ECOA codes.
            // A - Authorized user: A joint account where the borrower is an authorized user, but has no contractual responsibility
            // I - Individual account: An account solely for this borrower
            // J - Joint account: An account for which both spouses are contractually liable
            // M - Maker: An account where the borrower is primarily responsible, having a cosigner [or Co-maker] with no spousal relationship to assume liability if the borrower defaults
            // S - Co-maker: An account for which the borrower is a cosigner, with no spousal relationship, who becomes liable if the primary signer [or Maker] defaults
            // P - Participating account: A joint account for which contractual liability cannot be determined
            // T - Terminated: A joint or cosigned account where the borrower is no longer associated with the account
            // X - Deceased: The borrower has been reported deceased.
            // U - 	Undesignated
            // 1/30/2006 dd - OPM 3949 Implement IsAuthorizedUser
            m_isAuthorizedUser = m_ecoa == "A";

            #region Parse account_type_code
            m_accountTypeCode = el.GetAttribute("account_type_code");
            m_liabilityType = GetDebtRegularT(m_accountTypeCode);

            // If MCL account_type_code start with 'R' then consider Revolving Type.
            // Here are the code that will return true.
            // RR (Revolving)
            // RC (Credit Card)
            // RO (Open Account)
            //
            // If MCL account_type_code start with 'I' then consider Installment type
            // Here are the code that will be true.
            // II (Installment)
            // IM (Mortgage)
            // IA (Auto)
            // IE (Education)
            // IL (Lease)
            // IC (Co-signer)
            m_isTypeInstallmentBase = false;
            m_isTypeRevolvingBase = false;
            if( m_accountTypeCode.Length > 0 )
            {
                switch( m_accountTypeCode[0] )
                {
                    case 'I': 
                        m_isTypeInstallmentBase = true;
                        break;
                    case 'R':
                        m_isTypeRevolvingBase = true;
                        break;
                }
            }	
            #endregion
            
            m_accountStatusCode = el.GetAttribute("account_status_code");
			// 9/5/2006 nw - OPM 7264 - If we get a blank account_status_code, default it to "00"
			if (m_accountStatusCode == "")
				m_accountStatusCode = "00";

            switch (m_accountStatusCode) 
            {
                case CMclCodeTradelineStatusT.Bankruptcy:
                    m_isBankruptcy = true;
					if (m_liabilityType == E_DebtRegularT.Mortgage)
					{
						FindLatestPublicRecordDateOfType("Bankruptcy");
					}
                    break;
                case CMclCodeTradelineStatusT.Foreclosure:
                    m_isForeclosure = true;
					FindLatestPublicRecordDateOfType("Foreclosure");
                    break;
                case CMclCodeTradelineStatusT.ChargeOff:
                    m_isChargeOff = true;
					if (m_liabilityType == E_DebtRegularT.Mortgage)
					{
						FindLatestPublicRecordDateOfType("Collection");
					}
                    break;
				case CMclCodeTradelineStatusT.PlacedForCollection:
					m_isCollection = true;
					if (m_liabilityType == E_DebtRegularT.Mortgage)
					{
						FindLatestPublicRecordDateOfType("Collection");
					}
					break;
				case CMclCodeTradelineStatusT.PaidCollection:
					//OPM 42990 - Handle paid collections the same way as collections
					m_isCollection = true;
					break;
				case CMclCodeTradelineStatusT.Repossession:
					m_isRepos = true;
					if (m_liabilityType == E_DebtRegularT.Mortgage)
					{
						m_isForeclosure = true;
						FindLatestPublicRecordDateOfType("Foreclosure");
					}
					break;
				case CMclCodeTradelineStatusT.Late30:
				case CMclCodeTradelineStatusT.Late60:
				case CMclCodeTradelineStatusT.Late90:
				case CMclCodeTradelineStatusT.Late120:
					m_isLate = true;
					break;
            }
            m_creditorAddress = creditor.Address;
            m_creditorCity = creditor.City;
            m_creditorState = creditor.State;
            m_creditorZipcode = creditor.Zipcode;
            m_creditorName = creditor.Name;
            m_creditorPhone = creditor.Phone;

            //m_isInactiveButNeutralStatus = _IsInactiveButNeutralStatus(m_accountStatusCode);
            //m_isInactiveButNegativeStatus = _IsInactiveButNegativeStatus(m_accountStatusCode);

            m_isActive = CMclCodeTradelineStatusT.IsActive( m_accountStatusCode );

			if (true == m_isActive)
			{
				string sRemarkCap = m_remarkText.ToUpper();

				// 5/18/2006 nw - OPM 5014 - If a tradeline has a remark that indicates that the tradeline has been 
				// "TRANSFERRED TO ANOTHER LENDER", we need to make sure m_isActive is false
				if (sRemarkCap.IndexOf("TRANSFERRED TO ANOTHER LENDER") >= 0)
					m_isActive = false;
				// 4/20/2007 nw - OPM 12585 - More remark text that would indicate a tradeline is inactive/closed
				else if (sRemarkCap.IndexOf("ACCOUNT CLOSED DUE TO TRANSFER") >= 0)
					m_isActive = false;
				else if (sRemarkCap.IndexOf("ACCOUNT CLOSED DUE TO REFINANCE") >= 0)
					m_isActive = false;
			}

            m_hasTerminalDerog = CMclCodeTradelineStatusT.HasTerminalDerog( m_accountStatusCode );
            m_hasBeenDerogStatus = CMclCodeTradelineStatusT.HasBeenDerog( m_accountStatusCode );
            m_isDerogCurrently = CMclCodeTradelineStatusT.IsDerogCurrently( m_accountStatusCode );
           
            CreateAdverseRatingList();
        }

        public override bool HasTerminalDerog 
        { 
            get { return m_hasTerminalDerog; } 
        }

        private void CreateAdverseRatingList() 
        {
            bool hasBankruptcy = false; 
            m_creditAdverseRatingList = new ArrayList();
            // 11/22/2004 dd - Combine payment history and late_dates into array of adverse rating in descending order.
            // i.e: History start date 11/04 - 001100200 
            // Result is [Late30, 9/04], [Late30, 8/04], [Late60, 5/04]

            #region Parse from two_year_history
            int late120Count = 0;
            if (null != m_paymentPattern && "" != m_paymentPattern && m_paymentPatternStartDate != DateTime.MinValue) 
            {
                char[] list = m_paymentPattern.ToCharArray();
                for (int i = 0; i < list.Length; i++) 
                {
                    // 1 - 30 Days Late
                    // 2 - 60 Days Late
                    // 3 - 90 Days Late
                    // 4 - 120 Days Late
                    // 5 - 150 Days Late
                    // 6 - 180 Days Late
                    // 7 - Wage Earner Plan
                    // 8 - Repossession
                    // 9 - Collection / ChargeOff
                    char ch = list[i];
                    CreditAdverseRating rating = null;

                    switch (list[i]) 
                    {
                        case '1':
                            rating = new CreditAdverseRating(E_AdverseType.Late30, m_paymentPatternStartDate.AddMonths(-1 * i));
                            break;
                        case '2':
                            rating = new CreditAdverseRating(E_AdverseType.Late60, m_paymentPatternStartDate.AddMonths(-1 * i));
                            break;
                        case '3':
                            rating = new CreditAdverseRating(E_AdverseType.Late90, m_paymentPatternStartDate.AddMonths(-1 * i));
                            break;
                        case '4':
                            rating = new CreditAdverseRating(E_AdverseType.Late120, m_paymentPatternStartDate.AddMonths(-1 * i));
                            late120Count++;
                            break;
                        case '5':
                            rating = new CreditAdverseRating(E_AdverseType.Late150, m_paymentPatternStartDate.AddMonths(-1 * i));
                            break;
                        case '6':
                            rating = new CreditAdverseRating(E_AdverseType.Late180, m_paymentPatternStartDate.AddMonths(-1 * i));
                            break;
                        case '7':
							rating = new CreditAdverseRating(E_AdverseType.Bankruptcy, m_paymentPatternStartDate.AddMonths(-1 * i));
                            hasBankruptcy = true;
                            break;
                        case '8':
                            rating = new CreditAdverseRating(E_AdverseType.RepossessionOrForeclosure, m_paymentPatternStartDate.AddMonths(-1 * i));
                            break;
                        case '9':
                            rating = new CreditAdverseRating(E_AdverseType.ChargeOffOrCollection, m_paymentPatternStartDate.AddMonths(-1 * i));
                            break;
                    }
                    if (null != rating) 
                    {
                        m_creditAdverseRatingList.Add(rating);
                    }
                }
            }
            #endregion

            if (!hasBankruptcy && m_liabilityType == E_DebtRegularT.Mortgage && m_accountStatusCode == CMclCodeTradelineStatusT.Bankruptcy)
            {
                DateTime bkDate = LastActivityDate;
                if (LatestBKRecord != null) 
                {
                    switch (LatestBKRecord.Type)
                    {
                        case E_CreditPublicRecordType.BankruptcyChapter7:
                            bkDate = LatestBKRecord.LastEffectiveDate;
                            break;
                        case E_CreditPublicRecordType.BankruptcyChapter13:
                            bkDate = LatestBKRecord.BkFileDate;
                            break;
                    }
                }

                if (m_creditAdverseRatingList.Count == 0 || (PaymentPatternStartDate != bkDate && PaymentPatternStartDate < bkDate))
                {
                    CreditAdverseRating cr = new CreditAdverseRating(E_AdverseType.Bankruptcy | E_AdverseType.Late120, bkDate);
                    m_creditAdverseRatingList.Insert(0, cr);
                    m_paymentPatternStartDate = bkDate;
                }
                else if (PaymentPatternStartDate.Equals(bkDate))
                {
                    CreditAdverseRating mostRecentEntry = m_creditAdverseRatingList[0] as CreditAdverseRating;
                    switch (mostRecentEntry.AdverseType)
                    {
                        case E_AdverseType.Late30:
                        case E_AdverseType.Late60:
                        case E_AdverseType.Late90:
                            mostRecentEntry.AdverseType = E_AdverseType.Bankruptcy | E_AdverseType.Late120;
                            break;
                    }
                }
            }

            if (m_liabilityType == E_DebtRegularT.Mortgage) 
            {
                // 5/3/2006 dd - If there is a late120 from payment pattern list for mortgage but it is not report in raw xml then update the count.
                m_late120 = late120Count.ToString();
            }
			   
            #region Parse from late_dates
            // example of LateDates string 9/02-150+, 8/02-120, 7/02-90, 6/02-60, 5/02-30, 2/02-60
            string[] lates = m_lateDates.Split(',');
            foreach (string late in lates) 
            {
                string[] values = late.TrimWhitespaceAndBOM().Split('-');
                DateTime dt = MclUtilities.ParseDateTime(values[0], "mm/yy");
                // 12/7/2004 dd - Reset each date to beginning of the report month. 9/02 will become 9/1/02
                dt = dt.AddDays(-1 * (dt.Day - 1));

                CreditAdverseRating rating = null;
                if (dt != DateTime.MinValue) 
                {
                    switch (values[1]) 
                    {
                        case "30":
                            rating = new CreditAdverseRating(E_AdverseType.Late30, dt);
                            break;
                        case "60":
                            rating = new CreditAdverseRating(E_AdverseType.Late60, dt);
                            break;
                        case "90":
                            rating = new CreditAdverseRating(E_AdverseType.Late90, dt);
                            break;
                        case "120":
                            rating = new CreditAdverseRating(E_AdverseType.Late120, dt);
                            break;
                        case "150":
                            rating = new CreditAdverseRating(E_AdverseType.Late150, dt);
                            break;
                        case "150+": 
                        {
                            // 12/7/2004 dd - This is a little tricky, 150+ can mean 150 days late or 180 days late.
                            // Need to check to see if the current list already contains 150 or 180 days late.

                            CreditAdverseRating r150 = new CreditAdverseRating(E_AdverseType.Late150, dt);
                            CreditAdverseRating r180 = new CreditAdverseRating(E_AdverseType.Late180, dt);
                            if (!m_creditAdverseRatingList.Contains(r150) && !m_creditAdverseRatingList.Contains(r180)) 
                            {
                                rating = new CreditAdverseRating(E_AdverseType.Late150, dt);
                            }
                            break;
                        }

                    }

                    if (null != rating && !m_creditAdverseRatingList.Contains(rating))
                        m_creditAdverseRatingList.Add(rating);
                }
            }
            #endregion
            #region OPM 42990 - If this is a paid collection, insert a late 120
            // OPM 243947 - Removes the late 120, but still inserts ChargeOffOrCollection.
            if (m_accountStatusCode == CMclCodeTradelineStatusT.PaidCollection)
			{
				CreditAdverseRating rating = new CreditAdverseRating(E_AdverseType.ChargeOffOrCollection, m_accountOpenedDate);
				m_creditAdverseRatingList.Add(rating);
			}
            #endregion

            #region OPM 4822
            // 6/15/2006 - nw - OPM 4822, when tradeline remark indicates that FC was started, 
            // raise a flag so we can return true in the keyword MightHaveUndated120Within.
            // David clarified that this should take place without considering tradeline status for now.
            if (m_remarkText.ToUpper().IndexOf("FORECLOSURE WAS STARTED") >= 0)
				m_foreclosureWasStarted = true;
			#endregion

            // 11/22/2004 dd - Sort the adverse rating in descending order.
            m_creditAdverseRatingList.Sort(new CreditAdverseRatingComparer());
        }

        private bool _IsLate(string code)
        {
            switch (code) 
            {
                case CMclCodeTradelineStatusT.Late30:
                case CMclCodeTradelineStatusT.Late60:
                case CMclCodeTradelineStatusT.Late90:
                case CMclCodeTradelineStatusT.Late120:
                    return true;
                default:
                    return false;

            }
        }
		
		override public bool IsActive
        {
            get{ return m_isActive; }
        }

        /// <summary>
        /// TODO: I'm not happy about this one yet, its name is misleading, it only looks at the status and not the payment history. -tn 6/23/05
        /// </summary>
        override public  bool HasBeenDerog
        {
            // Calling HasBadHistoryEffectiveWithin( 100, true ) here would get ourselves into infinite loop.
            get{ return m_hasBeenDerogStatus; }
        }

        override public bool IsDerogCurrently
        {
            get{ return m_isDerogCurrently; }
        }

		// 4/10/2007 nw - External OPM 30150 - TradeCountX:Status should use this instead of IsDerogCurrently
		override public bool IsLastStatusDerog
		{
			get
			{
				// check current status first, if can't be determined, then check payment pattern
				if (m_isDerogCurrently || IsForeclosure || IsBankruptcy || IsRepos || IsChargeOff || IsCollection)
					return true;

				if (null == m_paymentPattern || "" == m_paymentPattern)
					return false;

				if ('1' <= m_paymentPattern[0] && m_paymentPattern[0] <= '9')
					return true;

				return false;
			}
		}
		
		private E_DebtRegularT GetDebtRegularT(string type) 
        {
            switch (type) 
            {
                case CMclCodeTradelineT.Installment:
                case CMclCodeTradelineT.Auto:
                case CMclCodeTradelineT.Education:
                case CMclCodeTradelineT.Lease:
                case CMclCodeTradelineT.Cosigner:
                    return E_DebtRegularT.Installment;
                case CMclCodeTradelineT.Mortgage:
                    return E_DebtRegularT.Mortgage;
                case CMclCodeTradelineT.Revolving:
                case CMclCodeTradelineT.CreditCard:
                    return E_DebtRegularT.Revolving;
                case CMclCodeTradelineT.OpenAccount_RO:
                case CMclCodeTradelineT.OpenAccount_OO:
                    return E_DebtRegularT.Open;
                default:
                    return E_DebtRegularT.Other;
            }
        }
    }


}
