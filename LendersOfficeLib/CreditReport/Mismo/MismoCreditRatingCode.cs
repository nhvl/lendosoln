using System;
using LendersOffice.Common;
using LendersOffice.CreditReport;
using DataAccess;

namespace LendersOffice.CreditReport.Mismo
{
    public class CredcoCreditRatingCode : MismoCreditRatingCode 
    {
        // CREDCO Universal Rating Code
        // 0 - To new to rate
        // 1 - Current
        // 2 - 30 days late
        // 3 - 60 days late
        // 4 - 90 days late
        // 5 - 120 days late
        // 6 - 150 days late
        // 7 - Wage Earner Plan / Bankruptcy
        // 8 - Repossession or Foreclosure
        // 9 - Collection or Charge-Off
        // U - Unrated
        
        public override bool IsAdverseRating(char code) 
        {
            // 1/4/2005 dd - Only 2, 3, 4, 5, 6, 7, 8, 9 are adverse rating.
            return (code >= '2' && code <='9');

        }

        public override E_AdverseType GetAdverseRating(char code) 
        {
            switch (code) 
            {
                case '2': return E_AdverseType.Late30;
                case '3': return E_AdverseType.Late60;
                case '4': return E_AdverseType.Late90;
                case '5': return E_AdverseType.Late120;
                case '6': return E_AdverseType.Late150;
                case '7': return E_AdverseType.Bankruptcy;
                case '8': return E_AdverseType.RepossessionOrForeclosure;
                case '9': return E_AdverseType.ChargeOffOrCollection;
                default:
					throw new CBaseException(ErrorMessages.Generic, code + " is not a supported adverse rating type.");
            }
        }
    }

    public class ExperianCreditRatingCode : MismoCreditRatingCode 
    {
        // Experian Rating Code.
        // 1 - 30 Days Late
        // 2 - 60 Days Late
        // 3 - 90 Days Late
        // 4 - 120 Days Late
        // 5 - 150 Days Late
        // 6 - 180 Days Late
        // 7 - Wage Earner Plan
        // 8 - Repossession
        // 9 - Collection / ChargeOff
        public override bool IsAdverseRating(char code) 
        {
            // 1/4/2005 dd - Only 1, 2, 3, 4, 5, 6, 7, 8, 9 are adverse rating.
            return (code >= '1' && code <='9');

        }

        public override E_AdverseType GetAdverseRating(char code) 
        {
            switch (code) 
            {
                case '1': return E_AdverseType.Late30;
                case '2': return E_AdverseType.Late60;
                case '3': return E_AdverseType.Late90;
                case '4': return E_AdverseType.Late120;
                case '5': return E_AdverseType.Late150;
                case '6': return E_AdverseType.Late180;
                case '7': return E_AdverseType.Bankruptcy;
                case '8': return E_AdverseType.RepossessionOrForeclosure;
                case '9': return E_AdverseType.ChargeOffOrCollection;
                default:
					throw new CBaseException(ErrorMessages.Generic, code + " is not a supported adverse rating type.");
            }
        }
        
    }

    public class TransUnionCreditRatingCode : MismoCreditRatingCode 
    {

        // 0 - To new to rate
        // 1 - Current
        // 2 - 30 days late
        // 3 - 60 days late
        // 4 - 90 days late
        // 5 - 120 days late
        // 6 - 150 days late
        // 7 - Wage Earner Plan / Bankruptcy
        // 8 - Repossession or Foreclosure
        // 9 - Collection or Charge-Off
        // U - Unrated
        
        public override bool IsAdverseRating(char code) 
        {
            // 1/4/2005 dd - Only 2, 3, 4, 5, 6, 7, 8, 9 are adverse rating.
            return (code >= '2' && code <='9');

        }

        public override E_AdverseType GetAdverseRating(char code) 
        {
            switch (code) 
            {
                case '2': return E_AdverseType.Late30;
                case '3': return E_AdverseType.Late60;
                case '4': return E_AdverseType.Late90;
                case '5': return E_AdverseType.Late120;
                case '6': return E_AdverseType.Late150;
                case '7': return E_AdverseType.Bankruptcy;
                case '8': return E_AdverseType.RepossessionOrForeclosure;
                case '9': return E_AdverseType.ChargeOffOrCollection;
                default:
					throw new CBaseException(ErrorMessages.Generic, code + " is not a supported adverse rating type.");
            }
        }
    }


	public class MismoCreditRatingCode
	{
		protected MismoCreditRatingCode()
		{
		}

        public virtual bool IsAdverseRating(char code) 
        {
            throw new NotImplementedException("Need to override");
        }

        /// <summary>
        /// This method will throw exception if code is not adverse rating. 
        /// Use IsAdverseRating to check before invoke this method to avoid unnecessary exception throwing
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public virtual E_AdverseType GetAdverseRating(char code) 
        {
            throw new NotImplementedException("Need to override");
        }

        public static MismoCreditRatingCode Create(string codeType, string codeTypeOtherDescription) 
        {

//            if (codeType == "Other" && codeTypeOtherDescription == "CREDCO")
//                return new CredcoCreditRatingCode();
//            else
            if (codeType == "Other" && codeTypeOtherDescription == "TransUnion")
                return new TransUnionCreditRatingCode();// 6/21/2005 dd - Fiserv/Chase Credit use TransUnion code .
            else if (codeType == "Equifax") 
            {
                // 8/9/2005 dd - Information Network use Equifax rating code. Similar with TransUion Rating Code.
                return new TransUnionCreditRatingCode();
            }
            else
                return new ExperianCreditRatingCode();
        }
	}
}
