namespace LendersOffice.CreditReport.Mismo
{
    using System;
    using System.Xml;
    using LendersOffice.Common;
    using DataAccess;
    using LendersOffice.CreditReport;

    public enum MismoPublicRecordAccountOwnershipType
    {
        Individual,
        Joint,
        Terminated
    }

    public class MismoPublicRecordAccountOwnershipTypeMap
    {
        public static MismoPublicRecordAccountOwnershipType Parse(string type)
        {
            if (string.IsNullOrEmpty(type))
                return MismoPublicRecordAccountOwnershipType.Individual;

            switch (type.ToLower())
            {
                case "individual": return MismoPublicRecordAccountOwnershipType.Individual;
                case "joint": return MismoPublicRecordAccountOwnershipType.Joint;
                case "terminated": return MismoPublicRecordAccountOwnershipType.Terminated;
                default:
                    return MismoPublicRecordAccountOwnershipType.Individual;

            }

        }
    }
    public enum MismoPublicRecordType 
    {
        Annulment,
        Attachment,
        BankruptcyChapter11,
        BankruptcyChapter12,
        BankruptcyChapter13,
        BankruptcyChapter7,
        BankruptcyChapter7Involuntary,
        BankruptcyChapter7Voluntary,
        BankruptcyTypeUnknown,
        Collection,
        CustodyAgreement,
        DivorceDecree,
        FicticiousName,
        FinancialCounseling,
        FinancingStatement,
        ForcibleDetainer,
        Foreclosure,
        Garnishment,
        Judgment,
        LawSuit,
        Lien,
        NonResponsibility,
        NoticeOfDefault,
        Other,
        PublicSale,
        RealEstateRecording,
        Repossession,
        SupportDebt,
        TaxLienCity,
        TaxLienCounty,
        TaxLienFederal,
        TaxLienOther,
        TaxLienState,
        Trusteeship,
        Unknown,
        UnlawfulDetainer

    }

    public class MismoCreditPublicRecord : AbstractCreditPublicRecord 
    {
        #region Private Member Variables
        private string m_id = null; // DONE
        private DateTime m_reportedDate = DateTime.MinValue; // DONE
        private DateTime m_dispositionDate = DateTime.MinValue; // DONE
        private E_CreditPublicRecordDispositionType m_dispositionType; // DONE
        private decimal m_bankruptcyLiabilitiesAmount = 0.0M; // DONE
        private E_CreditPublicRecordType m_type = E_CreditPublicRecordType.BankruptcyTypeUnknown; // DONE
        private DateTime m_paidDate = DateTime.MinValue;
        private DateTime m_filedDate = DateTime.MinValue;
        private DateTime m_settledDate = DateTime.MinValue;
		private DateTime m_lastEffectiveDate = DateTime.MinValue;
        private string m_dispositionTypeOtherDescription;
		private decimal m_legalObligationAmount = 0.0M;
        private string m_courtName = "";
        private string m_borrowerID;
        private MismoPublicRecordAccountOwnershipType m_accountOwnershipType = MismoPublicRecordAccountOwnershipType.Individual;
        private E_PublicRecordOwnerT m_publicRecordOwnerT = E_PublicRecordOwnerT.Borrower;

        #endregion

        #region Implement Abstracts methods.
        public override string ID 
        { 
            get{ return m_id; }
        }

        public override string CourtName 
        {
            get { return m_courtName; }
        }

        public override DateTime ReportedDate 
        { 
            get { return m_reportedDate; } 
        }

        public override DateTime DispositionDate 
        { 
            get { return m_dispositionDate; }
        }

        public override E_CreditPublicRecordDispositionType DispositionType 
        { 
            get { return m_dispositionType; }
        }

        public override decimal BankruptcyLiabilitiesAmount 
        { 
            get
			{
				// 7/7/2006 nw - OPM 6119 - For non-BK, return the higher of LegalObligationAmount and BankruptcyLiabilitiesAmount
				if (IsBkOfTypeAndCharacter(E_BkType.Any, E_BkChar.Any))
					return m_bankruptcyLiabilitiesAmount;
				else
					return Math.Max(m_legalObligationAmount, m_bankruptcyLiabilitiesAmount);
			}
        }

        public override E_CreditPublicRecordType Type 
        { 
            get { return m_type; }
        }
        
		override public DateTime BkFileDate 
		{
			get 
			{
				if ( m_filedDate != DateTime.MinValue )
					return m_filedDate;
				else if (ReportedDate != DateTime.MinValue) 
					return ReportedDate;
				else if (DispositionDate != DateTime.MinValue) 
					return DispositionDate;
				else 
					return DateTime.Today;
			}

		}

		override public DateTime LastEffectiveDate 
		{
			get 
			{
				if ( m_lastEffectiveDate != DateTime.MinValue )
					return m_lastEffectiveDate;
				else if( m_settledDate != DateTime.MinValue )
					return m_settledDate;
				else if( m_paidDate != DateTime.MinValue )
					return m_paidDate;
				else if (DispositionDate != DateTime.MinValue) 
				{
					return DispositionDate;
				} 
				else if (ReportedDate != DateTime.MinValue) 
				{
					//                    Tools.LogWarning("Found a public record with invalid Disposition Date, falling back to reported date.");
					return ReportedDate;
				}
				else 
				{
					//                    Tools.LogWarning("Found a public record with both invalid Disposition Date and Reported Date, falling back to today date.");
					return DateTime.Today;
				}
			}
		}

        public override E_PublicRecordOwnerT PublicRecordOwnerT
        {
            get { return m_publicRecordOwnerT; }
        }
        #endregion

        public MismoCreditPublicRecord(XmlElement el, string borrowerId, string coborrowerId, AbstractCreditReport parent) 
        {
            ParentCreditReport = parent;
            Parse(el, borrowerId, coborrowerId);
        }

        private void Parse(XmlElement el, string borrowerId, string coborrowerId) 
        {
            m_id                          = el.GetAttribute("CreditPublicRecordID"); 
            m_bankruptcyLiabilitiesAmount = GetDecimal(el, "_BankruptcyLiabilitiesAmount");
            m_dispositionDate             = MismoUtilities.ParseDateTime(el.GetAttribute("_DispositionDate"));
            m_reportedDate                = MismoUtilities.ParseDateTime(el.GetAttribute("_ReportedDate"));
            m_dispositionTypeOtherDescription = el.GetAttribute("_DispositionTypeOtherDescription");
            m_dispositionType             = MapDispositionType(el.GetAttribute("_DispositionType"), m_dispositionTypeOtherDescription);
            m_type                        = MapPublicRecordType(el.GetAttribute("_Type"));
            m_paidDate                    = MismoUtilities.ParseDateTime(el.GetAttribute("_PaidDate"));
            m_filedDate                   = MismoUtilities.ParseDateTime(el.GetAttribute("_FiledDate"));
            m_settledDate                 = MismoUtilities.ParseDateTime(el.GetAttribute("_SettledDate"));
			m_legalObligationAmount       = GetDecimal(el, "_LegalObligationAmount");
            m_courtName                       = el.GetAttribute("_CourtName");

			// 11/1/2006 nw - External OPM 16208 - BK with paid date but no status should be treated as Dismissed
			if (m_paidDate != DateTime.MinValue && el.GetAttribute("_DispositionType") == "")
				m_dispositionType = E_CreditPublicRecordDispositionType.Dismissed;


            // Enable these when need.
            m_borrowerID                      = el.GetAttribute("BorrowerID");
            //            m_creditFileID                    = el.GetAttribute("CreditFileID");
            //            m_creditTradeReferenceID          = el.GetAttribute("CreditTradeReferenceID");
            //            m_attorneyName                    = el.GetAttribute("_AttorneyName");
            //            m_bankruptcyAssetsAmount          = el.GetAttribute("_BankruptcyAssetsAmount");
            //            m_isConsumerDispute               = MismoUtilities.ParseBool(el.GetAttribute("_ConsumerDisputeIndicator"));
            
            //            m_defendantName                   = el.GetAttribute("_DefendantName");
            //            m_isDerogatoryData                = MismoUtilities.ParseBool(el.GetAttribute("_DerogatoryDataIndicator"));
            //            m_docketIdentifier                = el.GetAttribute("_DocketIdentifier");

            //            m_isManualUpdate                  = MismoUtilities.ParseBool(el.GetAttribute("_ManualUpdateIndicator"));
            //            m_paymentFrequencyType            = el.GetAttribute("_PaymentFrequencyType");
            //            m_plaintiffName                   = el.GetAttribute("_PlaintiffName");
            //            m_typeOtherDescription            = el.GetAttribute("_TypeOtherDescription");

            // New fields for Mismo 2.3
            m_accountOwnershipType            =  MismoPublicRecordAccountOwnershipTypeMap.Parse(el.GetAttribute("_AccountOwnershipType"));
            //            m_bankruptcyAdjustmentPercent     = el.GetAttribute("_BankruptcyAdjustmentPercent");
            //            m_bankruptcyExemptAmount          = el.GetAttribute("_BankruptcyExemptAmount");
            //            m_bankruptcyRepaymentPercent      = el.GetAttribute("_BankruptcyRepaymentPercent");
            //            m_bankruptcyType                  = el.GetAttribute("_BankruptcyType");

            switch (m_accountOwnershipType)
            {
                case MismoPublicRecordAccountOwnershipType.Individual:
                    if (m_borrowerID == coborrowerId)
                    {
                        m_publicRecordOwnerT = E_PublicRecordOwnerT.CoBorrower;
                    }
                    else
                    {
                        m_publicRecordOwnerT = E_PublicRecordOwnerT.Borrower;
                    }
                    break;
                case MismoPublicRecordAccountOwnershipType.Joint:
                    m_publicRecordOwnerT = E_PublicRecordOwnerT.Joint;
                    break;
                case MismoPublicRecordAccountOwnershipType.Terminated:
                default:
                    m_publicRecordOwnerT = E_PublicRecordOwnerT.Borrower;
                    break;
            }
        }

		// 3/1/2007 nw - OPM 10631 - Load public records from loan file
		public MismoCreditPublicRecord(IPublicRecord field, AbstractCreditReport parent)
		{
			ParentCreditReport = parent;
			Parse(field);
		}

		private void Parse(IPublicRecord field)
		{
			string[] formats = new String[] {"MM/dd/yyyy", "M/dd/yyyy", "MM/d/yyyy", "M/d/yyyy"};

			m_id                          = field.IdFromCreditReport;
			m_bankruptcyLiabilitiesAmount = field.BankruptcyLiabilitiesAmount;
			if (field.DispositionD_rep == "")
				m_dispositionDate         = DateTime.MinValue;
			else
				m_dispositionDate         = DateTime.ParseExact(field.DispositionD_rep, formats, null, System.Globalization.DateTimeStyles.None);
			if (field.ReportedD_rep == "")
				m_reportedDate            = DateTime.MinValue;
			else
				m_reportedDate            = DateTime.ParseExact(field.ReportedD_rep, formats, null, System.Globalization.DateTimeStyles.None);
			m_dispositionTypeOtherDescription = "";
			m_dispositionType             = field.DispositionT;
			m_type                        = field.Type;
			if (field.BkFileD_rep == "")
				m_filedDate               = DateTime.MinValue;
			else
				m_filedDate               = DateTime.ParseExact(field.BkFileD_rep, formats, null, System.Globalization.DateTimeStyles.None);
			if (field.LastEffectiveD_rep == "")
				m_lastEffectiveDate       = DateTime.MinValue;
			else
				m_lastEffectiveDate       = DateTime.ParseExact(field.LastEffectiveD_rep, formats, null, System.Globalization.DateTimeStyles.None);
			m_legalObligationAmount       = 0.0M;
			m_courtName                   = field.CourtName;
            m_publicRecordOwnerT = field.OwnerT;
		}

        private E_CreditPublicRecordDispositionType MapDispositionType(string type, string dispositionTypeOtherDescription) 
        {
            // 9/12/2005 dd - Just perform case insensitive search.
            if (null != type && "" != type)
                type = type.ToLower();

            switch (type) 
            {
                case "canceled": // MISMO define "Cancelled", but just in case any Mismo derive decide to use "Canceled" instead.
                case "cancelled": return E_CreditPublicRecordDispositionType.Canceled;
                case "discharged": return E_CreditPublicRecordDispositionType.Discharged;
                case "dismissed": return E_CreditPublicRecordDispositionType.Dismissed;
                case "filed": return E_CreditPublicRecordDispositionType.Filed;
                case "involuntarilydischarged": return E_CreditPublicRecordDispositionType.InvoluntarilyDischarged;
                case "nonadjudicated": return E_CreditPublicRecordDispositionType.Nonadjudicated;
                case "unreleased": return E_CreditPublicRecordDispositionType.Unreleased;
                case "paidnotsatisfied": return E_CreditPublicRecordDispositionType.PaidNotSatisfied;
                case "paid": return E_CreditPublicRecordDispositionType.Paid;
                case "completed": return E_CreditPublicRecordDispositionType.Completed;
                case "released": return E_CreditPublicRecordDispositionType.Released;
                case "satisfied": return E_CreditPublicRecordDispositionType.Satisfied;
                case "vacated": return E_CreditPublicRecordDispositionType.Vacated;
                case "withdrawn": return E_CreditPublicRecordDispositionType.Withdrawn;
                case "adjudicated": return E_CreditPublicRecordDispositionType.Adjudicated;
                case "appealed": return E_CreditPublicRecordDispositionType.Appealed;
                case "converted": return E_CreditPublicRecordDispositionType.Converted;
                case "distributed": return E_CreditPublicRecordDispositionType.Distributed;
                case "granted": return E_CreditPublicRecordDispositionType.Granted;
                case "other": 
                    if (dispositionTypeOtherDescription != null && dispositionTypeOtherDescription.ToLower() == "discharged in bankruptcy") 
                    {
                        // 1/11/2006 dd - OPM 3760.
                        return E_CreditPublicRecordDispositionType.Discharged;
                    }
                    return E_CreditPublicRecordDispositionType.Other;
                case "pending": return E_CreditPublicRecordDispositionType.Pending;
                case "realestatesold": return E_CreditPublicRecordDispositionType.RealEstateSold;
                case "rescinded": return E_CreditPublicRecordDispositionType.Rescinded;
                case "settled": return E_CreditPublicRecordDispositionType.Settled;
                case "unknown": return E_CreditPublicRecordDispositionType.Unknown;
                case "voluntarilydischarged": return E_CreditPublicRecordDispositionType.VoluntarilyDischarged;
                case "unsatisfied": return E_CreditPublicRecordDispositionType.Unsatisfied;
                default:
                    if (type != "" && type != null)
                        LogCreditWarning(type + " is invalid Mismo DispositionType. Default to Filed.");
                    return E_CreditPublicRecordDispositionType.Filed;
            }
        }
        private E_CreditPublicRecordType MapPublicRecordType(string type) 
        {
            // 9/12/2005 dd - Just perform case insensitive search.
            if (null != type && "" != type)
                type = type.TrimWhitespaceAndBOM().ToLower();

            switch (type) 
            {
                case "annulment":                     return E_CreditPublicRecordType.Annulment;
                case "attachment":                    return E_CreditPublicRecordType.Attachment;
                case "bankruptcychapter11":           return E_CreditPublicRecordType.BankruptcyChapter11;
                case "bankruptcychapter12":           return E_CreditPublicRecordType.BankruptcyChapter12;
                case "bankruptcychapter13":           return E_CreditPublicRecordType.BankruptcyChapter13;

                case "bankruptcytypeunknown":         // return E_CreditPublicRecordType.BankruptcyTypeUnknown;
                case "bankruptcychapter7":            
                    // OPM 2591. Map all unknown BK to Chapter 7.
                    return E_CreditPublicRecordType.BankruptcyChapter7;
                case "bankruptcychapter7involuntary": return E_CreditPublicRecordType.BankruptcyChapter7Involuntary;
                case "bankruptcychapter7voluntary":   return E_CreditPublicRecordType.BankruptcyChapter7Voluntary;

                case "collection":                    return E_CreditPublicRecordType.Collection;
                case "custodyagreement":              return E_CreditPublicRecordType.CustodyAgreement;
                case "divorcedecree":                 return E_CreditPublicRecordType.DivorceDecree;
                case "ficticiousname":                return E_CreditPublicRecordType.FicticiousName;
                case "financialcounseling":           return E_CreditPublicRecordType.FinancialCounseling;
                case "financingstatement":            return E_CreditPublicRecordType.FinancingStatement;
                case "forcibledetainer":              return E_CreditPublicRecordType.ForcibleDetainer;
                case "foreclosure":                   return E_CreditPublicRecordType.Foreclosure;
                case "garnishment":                   return E_CreditPublicRecordType.Garnishment;
                case "judgement": // In MISMO specification, the correct spelling is without 'e'. However Advantage Credit (FL) use with 'e'. 11/8/04 dd.
                case "judgment":                      return E_CreditPublicRecordType.Judgment;
                case "lawsuit":                       return E_CreditPublicRecordType.LawSuit;
                case "lien":                          return E_CreditPublicRecordType.Lien;
                case "nonresponsibility":             return E_CreditPublicRecordType.NonResponsibility;
                case "noticeofdefault":               return E_CreditPublicRecordType.NoticeOfDefault;
                case "other":                         return E_CreditPublicRecordType.Other;
                case "publicsale":                    return E_CreditPublicRecordType.PublicSale; 
                case "realestaterecording":           return E_CreditPublicRecordType.RealEstateRecording;
                case "repossession":                  return E_CreditPublicRecordType.Repossession;
                case "supportdebt":                   return E_CreditPublicRecordType.SupportDebt;
                case "taxliencity":                   return E_CreditPublicRecordType.TaxLienCity;
                case "taxliencounty":                 return E_CreditPublicRecordType.TaxLienCounty;
                case "taxlienfederal":                return E_CreditPublicRecordType.TaxLienFederal;
                case "taxlienother":                  return E_CreditPublicRecordType.TaxLienOther;
                case "taxlienstate":                  return E_CreditPublicRecordType.TaxLienState;
                case "trusteeship":                   return E_CreditPublicRecordType.Trusteeship;
                case "unknown":                       return E_CreditPublicRecordType.Unknown;
                case "unlawfuldetainer":              return E_CreditPublicRecordType.UnlawfulDetainer;            
                default:
                    LogCreditWarning(type + " is invalid Mismo PublicRecordType. Default to Bankruptcy Ch7");
                    return E_CreditPublicRecordType.BankruptcyChapter7; // See 2591
            }

        }

    }

}
