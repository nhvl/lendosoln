﻿namespace LendersOffice.CreditReport.Mismo
{
    /// <summary>
    /// Mismo Credit Report Address Type.
    /// </summary>
    public enum MismoCreditReportRequestAddressType
    {
        /// <summary>
        /// Current Address.
        /// </summary>
        Current = 0,

        /// <summary>
        /// Previous Address.
        /// </summary>
        Previous = 1,

        /// <summary>
        /// Mailing Address.
        /// </summary>
        Mailing = 2
    }
}
