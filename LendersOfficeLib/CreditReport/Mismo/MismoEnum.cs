using System;

namespace LendersOffice.CreditReport.Mismo
{

    public enum E_CreditReportRequestActionType 
    {
        Other,
        Reissue,
        Retrieve,
        StatusQuery,
        Submit,
        Update,
        Upgrade,
    }

    /// <summary>
    /// Used to track more specific action types for Other for ordering non-standard credit report products.
    /// </summary>
    public enum CreditReportRequestActionTypeOtherDetail
    {
        /// <summary>
        /// A default for when there is no value.
        /// </summary>
        None = 0,

        /// <summary>
        /// An order for a new LQI credit report.
        /// </summary>
        LqiNew,

        /// <summary>
        /// An order for a reissue of an LQI credit report.
        /// </summary>
        LqiReissue,

        /// <summary>
        /// Remove the primary applicant from a credit report.
        /// </summary>
        RemoveBorrower,

        /// <summary>
        /// Remove the secondary applicant (usually spouse) from a credit report.
        /// </summary>
        RemoveCoBorrower,

        /// <summary>
        /// Retrieve a supplemental product.
        /// </summary>
        RetrieveSupplement,

        /// <summary>
        /// Retrieve a billing result.
        /// </summary>
        RetrieveBilling,
    }

    public enum E_MismoOutputFormat 
    {
        Other, PCL, PDF, Text, XML
    }
    public enum E_CreditReportType 
    {
        Merge,
        MergePlus,
        NonTraditional,
        Other,
        RiskScoresOnly,
        RMCR,
        RMCRForeign,
        LoanQuality
    }
}
