using System;
using System.Xml;

using DataAccess;
using LendersOffice.Common;
using LendersOffice.CreditReport;

namespace LendersOffice.CreditReport.Mismo
{
	public class MismoCreditReportView : LendersOffice.CreditReport.ICreditReportView
	{
        private CreditReportDebugInfo m_debugInfo;
        protected XmlDocument m_doc;

        protected bool m_hasPdf = false;
        protected bool m_hasHtml = false;
        protected string m_htmlContent = "";
        protected string m_pdfContent = "";

		internal MismoCreditReportView(CreditReportFactory factory, XmlDocument doc, CreditReportDebugInfo debugInfo)
		{
            if (null == factory) 
            {
                throw new CBaseException(ErrorMessages.Generic, "MismoCreditReportView can only be instantiate by CreditReportFactory.");
            }
            if (null == doc) 
            {
                throw new CBaseException(ErrorMessages.Generic, "MismoCreditReportView has null XmlDocument in constructor.");
            }
            m_doc = doc;
            m_debugInfo = debugInfo;

            Parse();
		}
        protected virtual void Parse() 
        {
            ////Isaac Ribakoff, OPM 243053: Kroll via Desktop Underwriter interface has been including extraneous html file of _Name Address_Residence_Information. We need to ignore that.
            XmlNode node = m_doc.SelectSingleNode("//RESPONSE_GROUP/RESPONSE/RESPONSE_DATA/CREDIT_RESPONSE/EMBEDDED_FILE[@_Type='HTML' and not(contains(@_Name, 'Address_Residence_Information'))]");
            if (null != node) 
            {
                m_hasHtml = true;
                XmlNode valueNode = node.SelectSingleNode("DOCUMENT");

                if (((XmlElement) node).GetAttribute("_EncodingType").ToLower() == "base64") 
                {
                    m_htmlContent = System.Text.ASCIIEncoding.ASCII.GetString(Convert.FromBase64String(valueNode.InnerText));
                } 
                else 
                {
                    m_htmlContent = valueNode.InnerText;
                }

            }
			else
			{
				// 5/15/2006 nw - OPM 4946 - If the HTML report is missing, display the TEXT report if available.  
				// This can be done by treating a TEXT report as a HTML report here.
				node = m_doc.SelectSingleNode("//RESPONSE_GROUP/RESPONSE/RESPONSE_DATA/CREDIT_RESPONSE/EMBEDDED_FILE[@_Type='TEXT']/DOCUMENT");
				if (null == node)
					// some systems use "Text" instead of "TEXT"
					node = m_doc.SelectSingleNode("//RESPONSE_GROUP/RESPONSE/RESPONSE_DATA/CREDIT_RESPONSE/EMBEDDED_FILE[@_Type='Text']/DOCUMENT");

				if (null != node)
				{
					m_hasHtml = true;

					// 6/21/2006 - nw - OPM 5718, handle base64 encoding for Text credit report
					if (((XmlElement) node.ParentNode).GetAttribute("_EncodingType").ToLower() == "base64")
						m_htmlContent = System.Text.ASCIIEncoding.ASCII.GetString(Convert.FromBase64String(node.InnerText));
					else
						m_htmlContent = node.InnerText;

					m_htmlContent = string.Format("<html><body><pre>{0}</pre></body></html>", m_htmlContent);
				}
			}
            
            node = m_doc.SelectSingleNode("//RESPONSE_GROUP/RESPONSE/RESPONSE_DATA/CREDIT_RESPONSE/EMBEDDED_FILE[@_Type='PDF']/DOCUMENT");
            if (null != node) 
            {
                m_hasPdf = true;
                m_pdfContent = node.InnerText;
            }
        }
        public bool HasPdf 
        {
            get { return m_hasPdf; }
        }
        public bool HasHtml 
        {
            get { return m_hasHtml; }
        }
        public string HtmlContent 
        {
            get { return m_htmlContent; }
        }
        public string PdfContent 
        {
            get { return m_pdfContent; }
        }
        public string RawXml 
        {
            get { return m_doc.InnerXml; }
        }
	}
}
