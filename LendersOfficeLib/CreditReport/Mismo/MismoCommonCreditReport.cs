namespace LendersOffice.CreditReport.Mismo
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Xml;

    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.CreditReport;

    public class MismoCreditReport : AbstractCreditReport 
    {
		private bool m_swapPrimaryBorrower = false;
		protected XmlDocument m_doc;
        private CreditScore m_borrowerScore;
        private CreditScore m_coborrowerScore;
        private BorrowerInfo m_borrowerInfo = null;
        private BorrowerInfo m_coborrowerInfo = null;
        private CreditBureauInformation m_experianContactInformation = CreditBureauInformation.DefaultExperianInformation;
        private CreditBureauInformation m_transUnionContactInformation = CreditBureauInformation.DefaultTransUnionInformation;
        private CreditBureauInformation m_equifaxContactInformation = CreditBureauInformation.DefaultEquifaxInformation;
        private ArrayList m_liabilities;
        private ArrayList m_liabilitiesExcludeFromUnderwriting;
        private ArrayList m_publicRecords;

        private string m_borrowerID;
        private string m_coborrowerID;
        private ILiaCollection m_liaColl;
		private IPublicRecordCollection m_publicRecordCollection;
        private DateTime m_creditReportFirstIssuedDate = DateTime.MinValue;
        private DateTime m_creditReportLastUpdatedDate = DateTime.MinValue;

        private MismoCreditRatingCode m_creditRatingCode = null;

        private CreditReportProtocol m_creditReportProtocol = CreditReportProtocol.MISMO_2_3; // Default

        private string m_respondingPartyName = "";

        public string RespondingPartyName 
        {
            get { return m_respondingPartyName; }
        }

        public CreditReportProtocol CreditReportProtocol 
        {
            get { return m_creditReportProtocol; }
        }

        public MismoCreditRatingCode CreditRatingCode 
        {
            get { return m_creditRatingCode; }
        }

        #region Define Abstract Methods
        public override DateTime CreditReportFirstIssuedDate 
        { 
            get { return m_creditReportFirstIssuedDate; }
        }
        public override DateTime CreditReportLastUpdatedDate
        {
            get { return m_creditReportLastUpdatedDate; }
        }
        public override CreditScore BorrowerScore 
        { 
            get { return m_borrowerScore; }
        }
        public override CreditScore CoborrowerScore 
        { 
            get { return m_coborrowerScore; }
        }

        public override BorrowerInfo BorrowerInfo 
        { 
            get { return m_borrowerInfo; }
        }

        public override BorrowerInfo CoborrowerInfo 
        { 
            get { return m_coborrowerInfo; }
        }
        public override CreditBureauInformation ExperianContactInformation 
        { 
            get { return m_experianContactInformation; }
        }
        public override CreditBureauInformation TransUnionContactInformation 
        { 
            get { return m_transUnionContactInformation; }
        }
        public override CreditBureauInformation EquifaxContactInformation 
        { 
            get { return m_equifaxContactInformation; }
        }
        public override ArrayList AllLiabilities 
        { 
            get { return m_liabilities; }
        }
        public override ArrayList LiabilitiesExcludeFromUnderwriting 
        {
            get { return m_liabilitiesExcludeFromUnderwriting; }
        }
        public override ArrayList AllPublicRecords 
        { 
            get { return m_publicRecords; }
        }

        public override string GetLiabilityXml(string id) 
        {
            string xml = "";
            XmlNode node = m_doc.SelectSingleNode("//RESPONSE_GROUP/RESPONSE/RESPONSE_DATA/CREDIT_RESPONSE/CREDIT_LIABILITY[@CreditLiabilityID='" + id + "']");
            if (null != node)
                xml = node.OuterXml;

            return xml;
        }
        public override string GetPublicRecordXml(string id) 
        {
            string xml = "";

            XmlNode node = m_doc.SelectSingleNode("//RESPONSE_GROUP/RESPONSE/RESPONSE_DATA/CREDIT_RESPONSE/CREDIT_PUBLIC_RECORD[@CreditPublicRecordID='" + id + "']");

            if (null != node)
                xml = node.OuterXml;

            return xml;
        }
        /// <summary>
        /// This method will also invoke Parse() method after finished. DO NOT CALL Parse() again.
        /// </summary>
        /// <param name="liaCollection"></param>
		public override void SetLoanData(CAppBase dataApp) 
		{
			m_liaColl = dataApp.aLiaCollection;
			m_publicRecordCollection = dataApp.aPublicRecordCollection;
			m_swapPrimaryBorrower = dataApp.aIsBorrSpousePrimaryWageEarner;
            m_dataApp = dataApp;
			Parse();
		}
        private CAppBase m_dataApp = null;
        public override CAppBase DataApp
        {
            get { return m_dataApp; }
        }
        #endregion
        internal MismoCreditReport(CreditReportFactory factory, CreditReportProtocol creditReportProtocol, XmlDocument doc, CreditReportDebugInfo debugInfo) 
        {
            if (null == factory) 
            {
                throw new CBaseException(ErrorMessages.Generic, "MismoCreditReport can only be instantiate by CreditReportFactory.");
            }
            ExtraDebugInfo = debugInfo;
            m_doc = doc;
            m_borrowerScore = new CreditScore();
            m_coborrowerScore = new CreditScore();
            m_creditReportProtocol = creditReportProtocol;
        }
        /// <summary>
        /// Call this method before invoke any method regarding the credit report liabilities/public records.
        /// </summary>
        public override void Parse() 
        {
            XmlNodeList nodeList = null;
			XmlNodeList creditLiabilityNodeList = null;

			// 12/18/2006 nw - OPM 7000 - Handle rare cases of multiple RESPONSE_DATA elements
			XmlNodeList responseDataNodeList = m_doc.SelectNodes("//RESPONSE_GROUP/RESPONSE/RESPONSE_DATA");
			if (responseDataNodeList.Count > 1)
			{
				// save the 0th RESPONSE_DATA, drop all the later ones
				XmlNode responseNode = m_doc.SelectSingleNode("//RESPONSE_GROUP/RESPONSE");
				for (int i = 1; i < responseDataNodeList.Count; i++)
				{
					responseNode.RemoveChild(responseDataNodeList.Item(i));
				}
			}

            // 3/17/2006 dd - Parse Responding_Party
            XmlElement respondingPartyEl = (XmlElement) m_doc.SelectSingleNode("//RESPONSE_GROUP/RESPONDING_PARTY");
            if (null != respondingPartyEl)
                m_respondingPartyName = respondingPartyEl.GetAttribute("_Name");

            #region Parse CreditRatingCodeType & CreditRatingCodeTypeOtherDescription
            XmlElement el = (XmlElement) m_doc.SelectSingleNode("//RESPONSE_GROUP/RESPONSE/RESPONSE_DATA/CREDIT_RESPONSE");

            string creditResponseId = el.GetAttribute("CreditResponseID").ToLower();
            string creditRatingCodeType = el.GetAttribute("CreditRatingCodeType");
            string creditRatingCodeTypeOtherDescription = el.GetAttribute("CreditRatingCodeTypeOtherDescription");
            m_creditRatingCode = MismoCreditRatingCode.Create(creditRatingCodeType, creditRatingCodeTypeOtherDescription);

			// 9/1/2006 nw - OPM 7218 - Determine the correct Credit Rating Code
			creditLiabilityNodeList = m_doc.SelectNodes("//RESPONSE_GROUP/RESPONSE/RESPONSE_DATA/CREDIT_RESPONSE/CREDIT_LIABILITY");
			m_creditRatingCode = VerifyCreditRatingCode(creditLiabilityNodeList);

            m_creditReportFirstIssuedDate = MismoUtilities.ParseDateTime(el.GetAttribute("CreditReportFirstIssuedDate"));
            m_creditReportLastUpdatedDate = MismoUtilities.ParseDateTime(el.GetAttribute("CreditReportLastUpdatedDate"));
            #endregion

            #region "Parse borrower id & coborrower id"
            nodeList = m_doc.SelectNodes("//RESPONSE_GROUP/RESPONSE/RESPONSE_DATA/CREDIT_RESPONSE/BORROWER");
            foreach(XmlElement o in nodeList) 
            {
                LendersOffice.CreditReport.BorrowerInfo borrower = null;

                if (o.GetAttribute("_PrintPositionType").ToLower() == "coborrower") 
                {
                    m_coborrowerID = o.GetAttribute("BorrowerID");
                    m_coborrowerInfo = new BorrowerInfo(true);
                    borrower = m_coborrowerInfo;
                } 
                else 
                {
                    // Default to borrower
                    m_borrowerID = o.GetAttribute("BorrowerID");
                    m_borrowerInfo = new BorrowerInfo();
                    borrower = m_borrowerInfo;
                }

                

                borrower.FirstName = o.GetAttribute("_FirstName");
                borrower.MiddleName = o.GetAttribute("_MiddleName");
                borrower.LastName = o.GetAttribute("_LastName");
                borrower.Suffix = o.GetAttribute("_NameSuffix");
                borrower.DOB = o.GetAttribute("_BirthDate");
                borrower.MismoSsn = o.GetAttribute("_SSN");
                // 7/28/2004 dd - TODO Marital Status.   marital_status
            }
            #endregion

            #region Parse aliases from CREDIT_FILE/_BORROWER

            
            nodeList = m_doc.SelectNodes("//RESPONSE_GROUP/RESPONSE/RESPONSE_DATA/CREDIT_RESPONSE/CREDIT_FILE/_BORROWER");
            if (nodeList != null)
            {

                foreach (XmlElement o in nodeList)
                {
                    if (m_borrowerInfo == null)
                        continue;

                    LendersOffice.CreditReport.BorrowerInfo borrower = null;

                    string ssn = o.GetAttribute("_SSN");
                    if (ssn != m_borrowerInfo.MismoSsn) // Check if it's the primary borrower or not
                    {
                        if (m_coborrowerInfo == null || ssn != m_coborrowerInfo.MismoSsn)
                            continue;

                        borrower = m_coborrowerInfo;
                    }
                    else
                    {
                        // We've already checked if m_borrowerInfo == null
                        borrower = m_borrowerInfo;
                    }

                    // Case 191267: Add to aliases from _BORROWER node in addition to _BORROWER/_ALIAS
                    borrower.AddAlias(AliasFromXMLElement(o));

                    XmlNodeList aliasList = o.SelectNodes("_ALIAS");
                    if (aliasList != null)
                    {
                        foreach (XmlElement alias in aliasList)
                        {
                            string name = AliasFromXMLElement(alias);

                            borrower.AddAlias(name);
                        }
                    }
                }
            }

            #endregion

            #region "Parse Credit Score Percentile"
            // OPM 61058 - Credit score percentile must be parsed before credit scores are parsed. Percentile values are kept in "KEY" nodes
            // that reference "CREDIT_SCORE" node CreditScoreID attributes, and since we don't store CreditScoreID there is no way to match
            // the percentile to the correct credit score unless we update percentile values as scores are parsed.
            LosConvert convert = new LosConvert();
            Dictionary<string, int> creditScoreIdToPercentile = new Dictionary<string, int>();
            nodeList = m_doc.SelectNodes("//RESPONSE_GROUP/RESPONSE/KEY");
            foreach (XmlElement o in nodeList)
            {
                string prefix = "creditscorerankpercent_" + creditResponseId + "_";
                string name = o.GetAttribute("_Name").TrimWhitespaceAndBOM().ToLower();
                string value = o.GetAttribute("_Value").TrimWhitespaceAndBOM();

                // We are only interested in keys that start with "CreditScoreRankPercent_"
                // and has a matching credit response id.
                if(!name.StartsWith(prefix))
                {
                    continue;
                }

                string creditScoreId = name.Substring(prefix.Length); 
                int parsedValue = convert.ToCount(value);

                creditScoreIdToPercentile.Add(creditScoreId, parsedValue);
            }
            #endregion

            #region "Parse credit scores"
            nodeList = m_doc.SelectNodes("//RESPONSE_GROUP/RESPONSE/RESPONSE_DATA/CREDIT_RESPONSE/CREDIT_SCORE");
            foreach(XmlElement o in nodeList) 
            {
                string creditScoreId = o.GetAttribute("CreditScoreID").ToLower();
                string source = o.GetAttribute("CreditRepositorySourceType");
                string strScore = o.GetAttribute("_Value");
                string id = o.GetAttribute("BorrowerID");
                string modelName = o.GetAttribute("_ModelNameType");
                string modelNameOtherDescription = o.GetAttribute("_ModelNameTypeOtherDescription");
				string creditFileID = o.GetAttribute("CreditFileID");

                CreditScore creditScore = id == m_coborrowerID ? m_coborrowerScore : m_borrowerScore;

				// 5/10/2006 nw - OPM 4909 - Equifax Mortgage Services doesn't use BorrowerID, so we need to use 
				// CreditFileID to figure out the owner of the CREDIT_SCORE elements.
				if ("" == id && "" == m_borrowerID && "" == m_coborrowerID)
				{
                    if (creditFileID.StartsWith("B-"))
                        creditScore = m_borrowerScore;
                    else if (creditFileID.StartsWith("C-")) 
                        creditScore = m_coborrowerScore;
                    else 
                    {
                        LogCreditBug("Unable to determine score belong to borrower or coborrower. Default to borrower score.");
                        creditScore = m_borrowerScore;
                    }
				}

                // 4/29/2005 dd - Hack for Universal Credit. Universal Credit does not use the same BorrowerID as BORROWER tag.
                // Instead it use Borrower / CoBorrower.
                if (id == "CoBorrower")
                    creditScore = m_coborrowerScore;
                else if (id == "Borrower")
                    creditScore = m_borrowerScore;

                int score = 0;
                if (null != strScore || strScore.TrimWhitespaceAndBOM() != "") 
                {
                    try 
                    {
                        score = int.Parse(strScore);

                        if (score > 1500)
                            score = 0; // 9/12/2005 dd - OPM #2832 - If score greater than 1500 then probably it is an error code.
                    } 
                    catch {}
                }
                string bureau = "EF";

                // OPM 61058 - Import score percentile from report.
                int percentile = 0;
                creditScoreIdToPercentile.TryGetValue(creditScoreId, out percentile);

                // 4/20/2005 dd - Check to see if LandSafe, Kroll Factual Data, CREDCO, Advantage Credit, all use _ModelNameType correct.
                // If that the case then I can skip compare CreditRepositorySourceType.
                switch (source) 
                {
                    case "Equifax":
                        bureau = "EF";
                        if (score != 0) 
                        {
                            // 9/12/2005 dd - This code will use lowest non-zero score, if there are multiple score from same bureau.
                            if ((creditScore.Equifax == 0) || (score < creditScore.Equifax)) 
                            {
                                creditScore.Equifax = score;
                                creditScore.EquifaxModelName = modelName;
                                creditScore.EquifaxModelNameOtherDescription = modelNameOtherDescription;
                                creditScore.EquifaxPercentile = percentile;
                            }
                        }
                        break;
                    case "Experian":
                        bureau = "XP";
                        if (score != 0) 
                        {
                            if ((creditScore.Experian == 0) || (score < creditScore.Experian))
                            creditScore.Experian = score;
                            creditScore.ExperianModelName = modelName;
                            creditScore.ExperianModelNameOtherDescription = modelNameOtherDescription;
                            creditScore.ExperianPercentile = percentile;
                        }
                        break;
                    case "TransUnion":
                        bureau = "TU";
                        if (score != 0) 
                        {
                            if ((creditScore.TransUnion == 0) || (score < creditScore.TransUnion)) 
                            {
                                creditScore.TransUnion = score;
                                creditScore.TransUnionModelName = modelName;
                                creditScore.TransUnionModelNameOtherDescription = modelNameOtherDescription;
                                creditScore.TransUnionPercentile = percentile;
                            }
                        }
                        break;
                    default:
                        // 4/20/2005 dd - For CRA doesn't provide CreditRepositorySourceType such as Universal Credit from ISS.
                        // If CRA doesn't provide CreditRepositorySourceType then use _ModelNameType. Only get the first 3 letters
                        // to figure out bureau.
                        if (!string.IsNullOrEmpty(modelName))
                        {
                            string prefix = modelName.ToLower().Substring(0, 3);
                            switch (prefix) 
                            {
                                case "equ":
                                    bureau = "EF";
                                    if (score != 0) 
                                    {
                                        if ((creditScore.Equifax == 0) || (score < creditScore.Equifax)) 
                                        {
                                            creditScore.Equifax = score;
                                            creditScore.EquifaxModelName = modelName;
                                            creditScore.EquifaxModelNameOtherDescription = modelNameOtherDescription;
                                            creditScore.EquifaxPercentile = percentile;
                                        }
                                    }
                                    break;
                                case "exp":
                                    bureau = "XP";
                                    if (score != 0) 
                                    {
                                        if ((creditScore.Experian == 0) || (score < creditScore.Experian)) 
                                        {
                                            creditScore.Experian = score;
                                            creditScore.ExperianModelName = modelName;
                                            creditScore.ExperianModelNameOtherDescription = modelNameOtherDescription;
                                            creditScore.ExperianPercentile = percentile;
                                        }
                                    }
                                    break;
                                case "tra":
                                    bureau = "TU";
                                    if (score != 0) 
                                    {
                                        if ((creditScore.TransUnion == 0) || (score < creditScore.TransUnion)) 
                                        {
                                            creditScore.TransUnion = score;
                                            creditScore.TransUnionModelName = modelName;
                                            creditScore.TransUnionModelNameOtherDescription = modelNameOtherDescription;
                                            creditScore.TransUnionPercentile = percentile;
                                        }
                                    }
                                    break;
                            }
                        }
                        break;
                }
                XmlNodeList factors = o.SelectNodes("_FACTOR");
                foreach (XmlElement f in factors)
                    creditScore.AddFactor(bureau, f.GetAttribute("_Text"));
            }

            #endregion 

			// 5/9/2007 nw - OPM 12892 - We need to parse public records before we parse liabilities
			#region Parse Public Records
			if (null == m_publicRecordCollection || m_publicRecordCollection.CountRegular == 0)
			{
				nodeList = m_doc.SelectNodes("//RESPONSE_GROUP/RESPONSE/RESPONSE_DATA/CREDIT_RESPONSE/CREDIT_PUBLIC_RECORD");
				m_publicRecords = new ArrayList();
				foreach (XmlElement o in nodeList) 
				{
					m_publicRecords.Add(new MismoCreditPublicRecord(o, m_borrowerID, m_coborrowerID, this));
				}
			}
			else
			{
				m_publicRecords = new ArrayList();
				for (int i = 0; i < m_publicRecordCollection.CountRegular; i++)
				{
					var field = m_publicRecordCollection.GetRegularRecordAt(i);
					if (field.IncludeInPricing)
						m_publicRecords.Add(new MismoCreditPublicRecord(field, this));
				}
			}
            #endregion

            // Parse liabilities

            #region Parse Liability Tradelines
            ArrayList userLiabilityList = null;
            if (null != m_liaColl) 
            {
                userLiabilityList = new ArrayList( m_liaColl.CountRegular );

                foreach( ILiabilityRegular lia in m_liaColl.GetSubcollection( true, E_DebtGroupT.Regular ) )
                {
                    userLiabilityList.Add( lia );
                }
            }
            m_liabilities = new ArrayList();
            m_liabilitiesExcludeFromUnderwriting = new ArrayList();
            foreach(XmlElement o in creditLiabilityNodeList) 
            {
                AbstractCreditLiability trade = new MismoCreditLiability(o, m_borrowerID, m_coborrowerID, this);
                bool skipTradeLine = false;
                if (null != userLiabilityList) 
                {
                    for (int index = 0; index < userLiabilityList.Count; index++) 
                    {
                        ILiabilityRegular liaReg = (ILiabilityRegular) userLiabilityList[index];
                        if (liaReg.IsSameTradeline(trade.AccountIdentifier, trade.CreditorName)) 
                        {
                            if (liaReg.Bal != trade.UnpaidBalanceAmount) 
                            {
                                trade.UpdateUnpaidBalanceAmount(liaReg.Bal);
                            }
                            if (liaReg.Pmt != trade.MonthlyPaymentAmount) 
                            {
                                trade.UpdateMonthlyPaymentAmount(liaReg.Pmt);
                            }
                            skipTradeLine = liaReg.ExcFromUnderwriting;
                            userLiabilityList.RemoveAt(index);
                            break;
                        }
                    }
                }
                if (!skipTradeLine)
                    m_liabilities.Add(trade);
                else
                    m_liabilitiesExcludeFromUnderwriting.Add(trade);
            }

            // If there are remaining liability from user liability then add to the credit report.
            if (null != userLiabilityList) 
            {
                foreach (ILiabilityRegular liaReg in userLiabilityList) 
                {
                    if (!liaReg.ExcFromUnderwriting)
                        m_liabilities.Add(new CreditLiabilityOptimistic(liaReg, this));
                }
            }

            #endregion
        }

        /// <summary>
        /// Gives a full-name alias for the _BORROWER or _BORROWER/_ALIAS XML element.
        /// Gives the _UnparsedName, if present. Otherwise, constructs the name from first, last, and middle.
        /// </summary>
        /// <param name="aliasNode">An XMLElement with appropriate name attributes</param>
        /// <returns>A full-name alias for a borrower</returns>
        private string AliasFromXMLElement(XmlElement aliasNode)
        {
            string firstName = aliasNode.GetAttribute("_FirstName");
            string lastName = aliasNode.GetAttribute("_LastName");
            string middleName = aliasNode.GetAttribute("_MiddleName");
            string nameSuffix = aliasNode.GetAttribute("_NameSuffix");

            string[] nameItems = new string[] { firstName, middleName, lastName, nameSuffix };
            nameItems = nameItems.Where(p => !string.IsNullOrEmpty(p)).ToArray();

            string alias = nameItems.Any() ? string.Join(" ", nameItems) : string.Empty;

            if (string.IsNullOrWhiteSpace(alias))
            {
                // Fallback to unparsed name
                alias = aliasNode.GetAttribute("_UnparsedName");

                // Some vendors include descriptors such as "FORMER NAME:" in the unparsed name. Remove them.
                Regex prefixFinder = new Regex(@"^.*:"); // match anything preceding a ':' character (inclusive).
                alias = prefixFinder.Replace(alias, string.Empty);

                // Experian sometimes includes the SSN in the unparsed name. Remove it.
                Regex ssnFinder = new Regex(@"\d{3}\-?\d{2}\-?\d{4}"); // match an SSN with or without dashes
                alias = ssnFinder.Replace(alias, string.Empty);
            }

            alias = alias.Trim();
            return alias;
        }

		// 9/1/2006 nw - OPM 7218 - Determine the correct Credit Rating Code
		private MismoCreditRatingCode VerifyCreditRatingCode(XmlNodeList nodeList)
		{
			foreach (XmlElement o in nodeList)
			{
				XmlElement oElem = (XmlElement) o.SelectSingleNode("_PAYMENT_PATTERN");
				string sPaymentPattern = "";
				if (oElem != null)
					sPaymentPattern = oElem.GetAttribute("_Data").ToUpper();

				// If payment pattern history contains a "C", it's definitely Experian credit rating code
				if (sPaymentPattern.IndexOf("C") >= 0)
					return new ExperianCreditRatingCode();

				oElem = (XmlElement) o.SelectSingleNode("_LATE_COUNT");
				int n30DayLate = -1;
				if (oElem != null)
				{
                    if (int.TryParse(oElem.GetAttribute("_30Days"), out n30DayLate) == false)
                    {
                        n30DayLate = -1;
                    }
                }
                if (n30DayLate == -1)
                {
                    // 9/30/2011 dd - We are unable to determine 30 late from _LATE_COUNT, we will try to look at _DerogatoryDataIndicator
                    if (o.GetAttribute("_DerogatoryDataIndicator") == "N")
                    {
                        n30DayLate = 0; // 9/30/2011 dd - Since there is no derogatory data we assume late 30 =0;
                    }
                }

                if (n30DayLate >= 0)
                {
                    int n1CountFromPaymentPattern = 0;
                    for (int i = 0; i < sPaymentPattern.Length; i++)
                    {
                        if (sPaymentPattern[i] == '1')
                            n1CountFromPaymentPattern++;
                    }

                    if (n1CountFromPaymentPattern > n30DayLate)
                        return new TransUnionCreditRatingCode();
                }
				
			}

			return m_creditRatingCode;
		}

		public override string CreditRatingCodeType
		{
			get
			{
				if (m_creditRatingCode is ExperianCreditRatingCode)
					return "Experian Credit Rating Code";
				else if (m_creditRatingCode is TransUnionCreditRatingCode)
					return "TransUnion Credit Rating Code";
				else if (m_creditRatingCode is CredcoCreditRatingCode)
					return "Credco Credit Rating Code";
				else
					return "Unknown Credit Rating Code";
			}
		}

		public override bool SwapPrimaryBorrower
		{
			get { return m_swapPrimaryBorrower; }
		}

    }
}
