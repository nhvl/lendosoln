namespace LendersOffice.CreditReport.Mismo
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Xml;
    using Common;
    using CommonLib;
    using DataAccess;

    public abstract class MismoCreditReportRequest : ICreditReportRequest
	{
        protected virtual string MISMOVersionID 
        {
            get { return ""; }
        }

        protected virtual bool IncludeMailingAddress
        {
            get { return false; }
        }

        private string m_url;
        private LoginInfo m_loginInfo;

        private BorrowerInfo m_borrower;
        private BorrowerInfo m_coborrower;
        private string m_reportID = "";
        private bool m_includeExperian;
        private bool m_includeEquifax;
        private bool m_includeTransunion;
        private E_CreditReportRequestActionType m_requestType = E_CreditReportRequestActionType.Submit;
        private string m_requestTypeOtherDesc = string.Empty;
        private E_CreditReportType m_creditReportType = E_CreditReportType.Merge;
        private string m_requestingPartyName;
        private string m_requestingPartyStreetAddress;
        private string m_requestingPartyCity;
        private string m_requestingPartyState;
        private string m_requestingPartyZipcode;

        private string m_receivingPartyName;
        private string m_receivingPartyStreetAddress;
        private string m_receivingPartyCity;
        private string m_receivingPartyState;
        private string m_receivingPartyZipcode;

        private string m_submittingPartyName;
        private string m_submittingPartyStreetAddress;
        private string m_submittingPartyCity;
        private string m_submittingPartyState;
        private string m_submittingPartyZipcode;
        private string m_submittingPartyIdentifier;
        private string m_creditRequestID = "CRReq0001"; //REQUEST_GROUP/REQUEST/REQUEST_DATA/CREDIT_REQUEST/CREDIT_REQUEST_DATA[@CreditRequestID]
        private string m_lenderCaseIdentifier = "";
        private string m_requestingPartyRequestedByName = "";

        // 6/2/2004 dd - List of <KEY> for //REQUEST_GROUP/REQUEST
        private NameValueCollection m_request_key; 

        private string m_preferredResponseFormatOtherDescription = "";

        protected Hashtable m_outputFormatHash = new Hashtable(30);
        #region "Public properties"
        public BorrowerInfo Borrower 
        {
            get { return m_borrower; }
        }

        public BorrowerInfo Coborrower 
        {
            get { return m_coborrower; }
        }

        public LoginInfo LoginInfo 
        {
            get { return m_loginInfo; }
        }
        public string ReportID
        {
            get { return m_reportID; }
            set { m_reportID = value; }
        }

        public bool IncludeExperian
        {
            get { return m_includeExperian; }
            set { m_includeExperian = value; }
        }

        public bool IncludeEquifax
        {
            get { return m_includeEquifax; }
            set { m_includeEquifax = value; }
        }

        public bool IncludeTransunion
        {
            get { return m_includeTransunion; }
            set { m_includeTransunion = value; }
        }

        public E_CreditReportRequestActionType RequestType
        {
            get { return m_requestType; }
            set { m_requestType = value; }
        }

        public string RequestTypeOtherDesc
        {
            get { return m_requestTypeOtherDesc; }
            set { m_requestTypeOtherDesc = value; }
        }

        public E_CreditReportType CreditReportType 
        {
            get { return m_creditReportType; }
            set { m_creditReportType = value; }
        }

        public string CreditReportTypeOtherDescription { get; set; }
        

        public string RequestingPartyName
        {
            get { return m_requestingPartyName; }
            set { m_requestingPartyName = value; }
        }

        public string RequestingPartyStreetAddress
        {
            get { return m_requestingPartyStreetAddress; }
            set { m_requestingPartyStreetAddress = value; }
        }

        public string RequestingPartyCity
        {
            get { return m_requestingPartyCity; }
            set { m_requestingPartyCity = value; }
        }

        public string RequestingPartyState
        {
            get { return m_requestingPartyState; }
            set { m_requestingPartyState = value; }
        }

        public string RequestingPartyZipcode
        {
            get { return m_requestingPartyZipcode; }
            set { m_requestingPartyZipcode = value; }
        }
        public string SubmittingPartyName
        {
            get { return m_submittingPartyName; }
            set { m_submittingPartyName = value; }
        }

        public string SubmittingPartyStreetAddress
        {
            get { return m_submittingPartyStreetAddress; }
            set { m_submittingPartyStreetAddress = value; }
        }

        public string SubmittingPartyCity
        {
            get { return m_submittingPartyCity; }
            set { m_submittingPartyCity = value; }
        }

        public string SubmittingPartyState
        {
            get { return m_submittingPartyState; }
            set { m_submittingPartyState = value; }
        }

        public string SubmittingPartyZipcode
        {
            get { return m_submittingPartyZipcode; }
            set { m_submittingPartyZipcode = value; }
        }
        public string SubmittingPartyIdentifier 
        {
            get { return m_submittingPartyIdentifier; }
            set { m_submittingPartyIdentifier = value; }
        }
        public string PreferredResponseFormatOtherDescription 
        {
            get { return m_preferredResponseFormatOtherDescription; }
            set { m_preferredResponseFormatOtherDescription = value; }
        }
        public string CreditRequestID 
        {
            get { return m_creditRequestID; }
            set { m_creditRequestID = value; }
        }
        public string ReceivingPartyName
        {
            get { return m_receivingPartyName; }
            set { m_receivingPartyName = value; }
        }

        public string ReceivingPartyStreetAddress
        {
            get { return m_receivingPartyStreetAddress; }
            set { m_receivingPartyStreetAddress = value; }
        }

        public string ReceivingPartyCity
        {
            get { return m_receivingPartyCity; }
            set { m_receivingPartyCity = value; }
        }

        public string ReceivingPartyState
        {
            get { return m_receivingPartyState; }
            set { m_receivingPartyState = value; }
        }

        public string ReceivingPartyZipcode
        {
            get { return m_receivingPartyZipcode; }
            set { m_receivingPartyZipcode = value; }
        }

        public string LenderCaseIdentifier 
        {
            get { return m_lenderCaseIdentifier; }
            set { m_lenderCaseIdentifier = value; }
        }
        public string RequestingPartyRequestedByName 
        {
            get { return m_requestingPartyRequestedByName; }
            set { m_requestingPartyRequestedByName = value; }
        }

        public void AddRequestKeyValue(string name, string value) 
        {
            if (null == m_request_key)
                m_request_key = new NameValueCollection();

            m_request_key.Add(name, value);
        }
        #endregion 

        public MismoCreditReportRequest(string url)
        {
            m_borrower = new BorrowerInfo();
            m_coborrower = new BorrowerInfo(true);
            m_loginInfo = new LoginInfo();
            m_url = url;

            AppendOutputFormat(E_MismoOutputFormat.XML); // 4/18/2005 dd - XML is always need
        }
        public void AppendOutputFormat(E_MismoOutputFormat format) 
        {
            if (!m_outputFormatHash.ContainsKey(format)) 
            {
                m_outputFormatHash[format] = format.ToString();
            }
        }

        /// <summary>
        /// Whether the current request object will support customization via the request data, e.g. for custom products.
        /// </summary>
        /// <returns>True if the customization is supported; false otherwise.</returns>
        public virtual bool CanCustomizeRequestDetails => false;

        /// <summary>
        /// Converts the current request by applying customizations and custom products.
        /// </summary>
        /// <param name="creditRequestData">The credit request data to apply to the current instance.</param>
        public virtual void CustomizeRequestDetails(CreditRequestData creditRequestData)
        {
            throw new NotSupportedException();
        }
        
        public virtual bool CanConvertRequestTypeToLqi => false;
        public virtual void ConvertRequestTypeToLqi(CreditRequestData creditRequestData)
        {
            throw new NotSupportedException();
        }

        /// <summary>
        /// Allows customization of the REQUEST_DATA element's inner XML.
        /// </summary>
        protected System.Xml.Linq.XElement RequestDataInnerXmlOverride { get; set; }

        #region Implementation of ICreditReportRequest
        public virtual System.Xml.XmlDocument GenerateXml() 
        {
            XmlDocument doc = new XmlDocument();

            XmlElement rootElement = doc.CreateElement("REQUEST_GROUP");
            rootElement.SetAttribute("MISMOVersionID", MISMOVersionID); 
            doc.AppendChild(rootElement);

            rootElement.AppendChild(CreateRequestingPartyElement(doc));
            rootElement.AppendChild(CreateReceivingPartyElement(doc));
            rootElement.AppendChild(CreateSubmittingPartyElement(doc));
            rootElement.AppendChild(CreateRequestElement(doc));

            return doc;
        }
        private XmlElement CreateRequestElement(XmlDocument doc) 
        {
            XmlElement el = doc.CreateElement("REQUEST");

            el.SetAttribute("RequestDatetime", MismoUtilities.ToDateTimeString(DateTime.Now));
            el.SetAttribute("InternalAccountIdentifier", LoginInfo.AccountIdentifier);
            el.SetAttribute("LoginAccountIdentifier", LoginInfo.UserName);
            el.SetAttribute("LoginAccountPassword", LoginInfo.Password);

            if (null != m_request_key) 
            {
                foreach (string key in m_request_key.Keys) 
                {
                    XmlElement keyEl = doc.CreateElement("KEY");
                    keyEl.SetAttribute("_Name", key);
                    keyEl.SetAttribute("_Value", m_request_key[key]);
                    el.AppendChild(keyEl);
                }
            }

            el.AppendChild(CreateRequestDataElement(doc));

            return el;
        }
        private XmlElement CreateRequestDataElement(XmlDocument doc) 
        {
            XmlElement el = doc.CreateElement("REQUEST_DATA");
            el.AppendChild(this.RequestDataInnerXmlOverride?.ToXmlElement(doc) ?? this.CreateCreditRequestElement(doc));
            return el;
        }

        public virtual byte[] GeneratePostContent() 
        {
            XmlDocument doc = GenerateXml();

            return System.Text.Encoding.UTF8.GetBytes(doc.InnerXml);

        }
        public abstract ICreditReportResponse CreateResponse(XmlDocument doc);

        public virtual string Url
        {
            get { return m_url; }
        }
        public virtual void SetupAuthentication(System.Net.HttpWebRequest request) 
        {
        }
        #endregion
        private XmlElement CreateReceivingPartyElement(XmlDocument doc) 
        {
            XmlElement el = doc.CreateElement("RECEIVING_PARTY");

            el.SetAttribute("_Name", m_receivingPartyName);
            el.SetAttribute("_StreetAddress", m_receivingPartyStreetAddress); 
            el.SetAttribute("_StreetAddress2", ""); 
            el.SetAttribute("_City", m_receivingPartyCity); 
            el.SetAttribute("_State", m_receivingPartyState);  
            el.SetAttribute("_PostalCode", m_receivingPartyZipcode); 

            return el;
        }

        private XmlElement CreateCreditRequestDataElement(XmlDocument doc) 
        {
            XmlElement el = doc.CreateElement("CREDIT_REQUEST_DATA");

            el.SetAttribute("CreditRequestID", m_creditRequestID);
            el.SetAttribute("CreditReportIdentifier", ReportID);
            el.SetAttribute("CreditReportRequestActionType", RequestType.ToString());

            if (RequestType == E_CreditReportRequestActionType.Other)
            {
                el.SetAttribute("CreditReportRequestActionTypeOtherDescription", RequestTypeOtherDesc);
            }

            el.SetAttribute("CreditReportType", m_creditReportType.ToString());
            if (m_creditReportType == E_CreditReportType.Other)
            {
                el.SetAttribute("CreditReportTypeOtherDescription", CreditReportTypeOtherDescription);
            }
            string borrowerID = Borrower.MismoID; // BorRec0001 always represent borrower. BorRec0001 BorRec0002 represents both borrower and coborrower

            string creditRequestType = "Individual";
            if (Coborrower.IsValid) 
            {
                creditRequestType = "Joint"; // Has coborrower information
                borrowerID += " " + Coborrower.MismoID;
            }
            el.SetAttribute("CreditRequestType", creditRequestType);
            el.SetAttribute("BorrowerID", borrowerID);

            el.AppendChild(CreateCreditRepositoryIncludedElement(doc));
            return el;
        }
        private XmlElement CreateRequestingPartyElement(XmlDocument doc) 
        {
            XmlElement el = doc.CreateElement("REQUESTING_PARTY");

            el.SetAttribute("_Name", m_requestingPartyName);
            el.SetAttribute("_StreetAddress", m_requestingPartyStreetAddress); 
            el.SetAttribute("_StreetAddress2", ""); 
            el.SetAttribute("_City", m_requestingPartyCity); 
            el.SetAttribute("_State", m_requestingPartyState);  
            el.SetAttribute("_PostalCode", m_requestingPartyZipcode); 

            CreatePreferredResponseElement(el);
            return el;

        }
        private void CreatePreferredResponseElement(XmlElement request) 
        {
            XmlDocument doc = request.OwnerDocument;
            foreach (string value in m_outputFormatHash.Values) 
            {
                XmlElement el = doc.CreateElement("PREFERRED_RESPONSE");
                el.SetAttribute("_Format", value);
                if (value == "Other") 
                {
                    // 11/1/2004 dd - Other format requires to have FormatOtherDescription. Currently,
                    // only CREDCO used this. Try to refactor this section so it work for other CRA.
                    el.SetAttribute("_FormatOtherDescription", m_preferredResponseFormatOtherDescription);
                }
                request.AppendChild(el);

            }
        }
        private XmlElement CreateLoanApplicationElement(XmlDocument doc) 
        {
            XmlElement el = doc.CreateElement("LOAN_APPLICATION");

            if (m_borrower.IsValid) 
            {
                el.AppendChild(CreateBorrowerElement(doc, m_borrower));
            }
            if (m_coborrower.IsValid)
            {
                el.AppendChild(CreateBorrowerElement(doc, m_coborrower));
            }
            return el;
        }
        private XmlElement CreateSubmittingPartyElement(XmlDocument doc) 
        {
            // 6/2/2004 dd - DONE
            XmlElement el = doc.CreateElement("SUBMITTING_PARTY");

            el.SetAttribute("_Name", m_submittingPartyName);
            el.SetAttribute("_StreetAddress", m_submittingPartyStreetAddress); 
            el.SetAttribute("_StreetAddress2", ""); 
            el.SetAttribute("_City", m_submittingPartyCity); 
            el.SetAttribute("_State", m_submittingPartyState);  
            el.SetAttribute("_PostalCode", m_submittingPartyZipcode); 
            el.SetAttribute("_Identifier", m_submittingPartyIdentifier);

            return el;
        }

        private XmlElement CreateCreditRepositoryIncludedElement(XmlDocument doc) 
        {
            XmlElement el = doc.CreateElement("CREDIT_REPOSITORY_INCLUDED");

            el.SetAttribute("_EquifaxIndicator", m_includeEquifax ? "Y" : "N");
            el.SetAttribute("_ExperianIndicator", m_includeExperian ? "Y" : "N");
            el.SetAttribute("_TransUnionIndicator", m_includeTransunion ? "Y" : "N");

            return el;
        }

        private XmlElement CreateBorrowerElement(XmlDocument doc, BorrowerInfo borrower) 
        {
            XmlElement el = doc.CreateElement("BORROWER");
           
            el.SetAttribute("BorrowerID", borrower.MismoID);
            el.SetAttribute("_FirstName", borrower.FirstName);
            el.SetAttribute("_MiddleName", borrower.MiddleName);
            el.SetAttribute("_LastName", borrower.LastName);
            el.SetAttribute("_NameSuffix", NormalizeNameSuffix(borrower.Suffix));
            el.SetAttribute("_SSN", borrower.MismoSsn); 
            el.SetAttribute("_PrintPositionType", borrower.IsCoborrower ? "CoBorrower" : "Borrower");
            el.SetAttribute("_BirthDate", borrower.MismoDOB);

            switch (borrower.MaritalStatus) 
            {
                case E_aBMaritalStatT.Married:
                    el.SetAttribute("MaritalStatusType", "Married");
                    break;
                case E_aBMaritalStatT.NotMarried:
                    el.SetAttribute("MaritalStatusType", "Unmarried");
                    break;
                case E_aBMaritalStatT.Separated:
                    el.SetAttribute("MaritalStatusType", "Separated");
                    break;
                case E_aBMaritalStatT.LeaveBlank:
                    el.SetAttribute("MaritalStatusType", "NotProvided");
                    break;
                default:
                    el.SetAttribute("MaritalStatusType", "Unknown");
                    break;
            }

            if (null != borrower.MailingAddress && MISMOVersionID == "2.3.1" && IncludeMailingAddress)
            {
                el.AppendChild(CreateResidenceElement(doc, borrower.MailingAddress, MismoCreditReportRequestAddressType.Mailing));
            }
            if (null != borrower.CurrentAddress) 
            {
                el.AppendChild(CreateResidenceElement(doc, borrower.CurrentAddress, MismoCreditReportRequestAddressType.Current));
            }
            if (null != borrower.PreviousAddress) 
            {
                el.AppendChild(CreateResidenceElement(doc, borrower.PreviousAddress, MismoCreditReportRequestAddressType.Previous));
            }

            return el;
        }

        private static string NormalizeNameSuffix(string originalNameSuffix)
        {
            StringComparer comparer = StringComparer.OrdinalIgnoreCase;
            if (comparer.Equals(originalNameSuffix, "JR.") || comparer.Equals(originalNameSuffix, "JR"))
            {
                return "JR";
            }
            else if (comparer.Equals(originalNameSuffix, "SR.") || comparer.Equals(originalNameSuffix, "SR"))
            {
                return "SR";
            }
            else if (comparer.Equals(originalNameSuffix, "II"))
            {
                return "II";
            }
            else if (comparer.Equals(originalNameSuffix, "III"))
            {
                return "III";
            }
            else if (comparer.Equals(originalNameSuffix, "IV"))
            {
                return "IV";
            }

            return originalNameSuffix;
        }

        private XmlElement CreateResidenceElement(XmlDocument doc, Address address, MismoCreditReportRequestAddressType addressType) 
        {
            XmlElement el;
            if (addressType == MismoCreditReportRequestAddressType.Mailing)
            {
                el = doc.CreateElement("_MAIL_TO");
            }
            else
            {
                el = doc.CreateElement("_RESIDENCE");
                el.SetAttribute("BorrowerResidencyType", addressType == MismoCreditReportRequestAddressType.Current ? "Current" : "Prior");
                el.AppendChild(CreateParsedStreetAddress(doc, address));
            }

            el.SetAttribute("_StreetAddress", address.StreetAddress);
            el.SetAttribute("_City", address.City);
            el.SetAttribute("_State", address.State);
            el.SetAttribute("_PostalCode", address.Zipcode);
            
            return el;
        }
        private XmlElement CreateParsedStreetAddress(XmlDocument doc, Address address) 
        {
            XmlElement el = doc.CreateElement("PARSED_STREET_ADDRESS");

            el.SetAttribute("_ApartmentOrUnit", address.AptNum);
            if (address.StreetDir != Address.STREET_DIR.blank) 
            {
                el.SetAttribute("_DirectionPrefix", address.StreetDir.ToString());
            }
            if (address.StreetPostDir != Address.STREET_DIR.blank)
            {
                el.SetAttribute("_DirectionSuffix", address.sStPostDir);
            }
            el.SetAttribute("_HouseNumber", address.StreetNumber);
            el.SetAttribute("_StreetName", address.StreetName);
            el.SetAttribute("_StreetSuffix", address.StreetType);


            return el;
        }


        private XmlElement CreateCreditRequestElement(XmlDocument doc) 
        {
            XmlElement el = doc.CreateElement("CREDIT_REQUEST");

            el.SetAttribute("MISMOVersionID", MISMOVersionID);
            el.SetAttribute("LenderCaseIdentifier", m_lenderCaseIdentifier);
            el.SetAttribute("RequestingPartyRequestedByName", m_requestingPartyRequestedByName);

            if (UseCreditCardPayment)
            {
                el.AppendChild(CreateCreditCardPaymentElement(doc));
            }
            el.AppendChild(CreateCreditRequestDataElement(doc));
            el.AppendChild(CreateLoanApplicationElement(doc));

            return el;
        }

        public bool UseCreditCardPayment { get; set; }
        public string ServicePayment_AccountHolderName { get; set; }
        public string ServicePayment_AccountHolderStreetAddress { get; set; }
        public string ServicePayment_AccountHolderCity { get; set; }
        public string ServicePayment_AccountHolderState { get; set; }
        public string ServicePayment_AccountHolderPostalCode { get; set; }
        public string ServicePayment_AccountIdentifier { get; set; }
        public string ServicePayment_AccountExpirationDate { get; set; }
        public string ServicePayment_SecondaryAccountIdentifier { get; set; }
        public string ServicePayment_MethodType { get; set; }

        private XmlElement CreateCreditCardPaymentElement(XmlDocument doc)
        {
            XmlElement el = doc.CreateElement("SERVICE_PAYMENT");
            el.SetAttribute("_AccountHolderName", ServicePayment_AccountHolderName);
            el.SetAttribute("_AccountHolderStreetAddress", ServicePayment_AccountHolderStreetAddress);
            el.SetAttribute("_AccountHolderCity", ServicePayment_AccountHolderCity);
            el.SetAttribute("_AccountHolderState", ServicePayment_AccountHolderState);
            el.SetAttribute("_AccountHolderPostalCode", ServicePayment_AccountHolderPostalCode);

            el.SetAttribute("_AccountIdentifier", ServicePayment_AccountIdentifier);
            el.SetAttribute("_AccountExpirationDate", ServicePayment_AccountExpirationDate);
            el.SetAttribute("_SecondaryAccountIdentifier", ServicePayment_SecondaryAccountIdentifier);

            el.SetAttribute("_MethodType", ServicePayment_MethodType);
            return el;
        }

	}
}
