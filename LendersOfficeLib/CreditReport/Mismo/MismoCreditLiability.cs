namespace LendersOffice.CreditReport.Mismo
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml;

    using DataAccess;
    using LendersOffice.Common;

    #region Mismo Enum
    public enum MismoCreditBusinessType 
    {
        _UNKNOWN, 
        Advertising, 
        Automotive, 
        Banking, 
        Clothing, 
        CollectionServices, 
        Contractors, 
        DepartmentAndMailOrder, 
        Employment, 
        FarmAndGardenSupplies, 
        Finance, 
        Government, 
        Grocery, 
        HomeFurnishing, 
        Insurance, 
        JewelryAndCamera, 
        LumberAndHardware, 
        MedicalAndHealth, 
        MiscellaneousAndPublicRecord, 
        OilAndNationalCreditCards, 
        PersonalServicesNotMedical, 
        RealEstateAndPublicAccommodation, 
        SportingGoods, 
        UtilitiesAndFuel, 
        Wholesale
    }
    public class MismoCreditBusinessTypeMap 
    {
        public static MismoCreditBusinessType Parse(string type) 
        {
            // 9/12/2005 dd - Just perform case insensitive search.
            if (null != type && "" != type)
                type = type.ToLower();

            switch (type) 
            {
                case "advertising": return MismoCreditBusinessType.Advertising; 
                case "automotive": return MismoCreditBusinessType.Automotive; 
                case "banking": return MismoCreditBusinessType.Banking; 
                case "clothing": return MismoCreditBusinessType.Clothing; 
                case "collectionservices": return MismoCreditBusinessType.CollectionServices; 
                case "contractors": return MismoCreditBusinessType.Contractors; 
                case "departmentandmailorder": return MismoCreditBusinessType.DepartmentAndMailOrder; 
                case "employment": return MismoCreditBusinessType.Employment; 
                case "farmandgardensupplies": return MismoCreditBusinessType.FarmAndGardenSupplies; 
                case "finance": return MismoCreditBusinessType.Finance; 
                case "government": return MismoCreditBusinessType.Government; 
                case "grocery": return MismoCreditBusinessType.Grocery; 
                case "homefurnishing": return MismoCreditBusinessType.HomeFurnishing; 
                case "insurance": return MismoCreditBusinessType.Insurance; 
                case "jewelryandcamera": return MismoCreditBusinessType.JewelryAndCamera; 
                case "lumberandhardware": return MismoCreditBusinessType.LumberAndHardware; 
                case "medicalandhealth": return MismoCreditBusinessType.MedicalAndHealth; 
                case "miscellaneousandpublicrecord": return MismoCreditBusinessType.MiscellaneousAndPublicRecord; 
                case "oilandnationalcreditcards": return MismoCreditBusinessType.OilAndNationalCreditCards; 
                case "personalservicesnotmedical": return MismoCreditBusinessType.PersonalServicesNotMedical; 
                case "realestateandpublicaccommodation": return MismoCreditBusinessType.RealEstateAndPublicAccommodation; 
                case "sportinggoods": return MismoCreditBusinessType.SportingGoods; 
                case "utilitiesandfuel": return MismoCreditBusinessType.UtilitiesAndFuel; 
                case "wholesale" : return MismoCreditBusinessType.Wholesale;
                default:
                    return MismoCreditBusinessType._UNKNOWN;
            }
        }
    }

    public enum MismoCreditLiabilityRating 
    {
        AsAgreed,
        BankruptcyOrWageEarnerPlan, // Mismo 2.3
        ChargeOff,
        Collection,
        CollectionOrChargeOff, // Mismo 2.3
        Foreclosure,
        ForeclosureOrRepossession, // Mismo 2.3
        Late30Days,
        Late60Days,
        Late90Days,
        LateOver120Days,
        NoDataAvailable,
        Repossession,
        TooNew,
        WageEarnerPlan
    }

    public enum MismoCreditLoanType 
    {
        Agriculture, 
        Airplane, 
        ApplianceOrFurniture, 
        AttorneyFees, 
        AutoLease, 
        AutoLoanEquityTransfer, 
        Automobile, 
        AutoRefinance, 
        Boat, 
        Business, 
        BusinessCreditCard, 
        ChargeAccount, 
        CheckCreditOrLineOfCredit, 
        ChildSupport, 
        Collection, 
        CollectionAttorney, 
        Comaker, 
        CombinedCreditPlan, 
        CommercialCreditObligation, 
        CommercialLineOfCredit, 
        CommercialMortgage, 
        ConditionalSalesContract, 
        Consolidation, 
        ConventionalRealEstateMortgage, 
        CreditCard, 
        CreditLineSecured, 
        DebitCard, 
        DebtCounselingService, 
        DepositRelated, 
        Educational, 
        Employment, 
        FactoringCompanyAccount, 
        FamilySupport, 
        FarmersHomeAdministrationFHMA, 
        FederalConsolidatedLoan, 
        FHAComakerNotBorrower, 
        FHAHomeImprovement, 
        FHARealEstateMortgage, 
        FinanceStatement, 
        GovernmentBenefit, 
        GovernmentEmployeeAdvance, 
        GovernmentFeeForService, 
        GovernmentFine, 
        GovernmentGrant, 
        GovernmentMiscellaneousDebt, 
        GovernmentOverpayment, 
        GovernmentSecuredDirectLoan, 
        GovernmentSecuredGuaranteeLoan, 
        GovernmentUnsecuredDirectLoan, 
        GovernmentUnsecuredGuaranteeLoan, 
        HomeEquityLineOfCredit, 
        HomeImprovement, 
        HouseholdGoods, 
        HouseholdGoodsAndOtherCollateralAuto, 
        HouseholdGoodsSecured, 
        InstallmentLoan, 
        InstallmentSalesContract, 
        InsuranceClaims, 
        Lease, 
        LenderPlacedInsurance, 
        ManualMortgage, 
        MedicalDebt, 
        MobileHome, 
        Mortgage, 
        NoteLoan, 
        NoteLoanWithComaker, 
        Other, 
        PartiallySecured, 
        PersonalLoan, 
        RealEstateJuniorLiens, 
        RealEstateLoanEquityTransfer, 
        RealEstateMortgageWithoutOtherCollateral, 
        RealEstateSpecificTypeUnknown, 
        Recreational, 
        RecreationalVehicle, 
        Refinance, 
        RefundAnticipationLoan, 
        RentalAgreement, 
        ResidentialLoan, 
        ReturnedCheck, 
        RevolvingBusinessLines, 
        Secured, 
        SecuredByCosigner, 
        SecuredCreditCard, 
        SecuredHomeImprovement, 
        SinglePaymentLoan, 
        SpouseSupport, 
        SummaryOfAccountsWithSameStatus, 
        TimeSharedLoan, 
        Title1Loan, 
        UnknownLoanType, 
        Unsecured, 
        VeteransAdministrationLoan, 
        VeteransAdministrationRealEstateMortgage,
        BiMonthlyMortgageTermInYears, // MISMO 2.3 
        ConstructionLoan, // MISMO 2.3 
        DeferredStudentLoan, // MISMO 2.3 
        SecondMortgage, // MISMO 2.3 
        SemiMonthlyMortgagePayment // MISMO 2.3 

    }
    public class MismoCreditLoanTypeMap 
    {
        public static MismoCreditLoanType Parse(string type, AbstractCreditLiability liability) 
        {
            // 9/12/2005 dd - Just perform case insensitive search.
            if (null != type && "" != type)
                type = type.ToLower().TrimWhitespaceAndBOM();

            switch (type) 
            {
                case "agriculture": return MismoCreditLoanType.Agriculture;
                case "airplane": return MismoCreditLoanType.Airplane;
                case "applianceorfurniture": return MismoCreditLoanType.ApplianceOrFurniture;
                case "attorneyfees": return MismoCreditLoanType.AttorneyFees;
                case "autolease": return MismoCreditLoanType.AutoLease;
                case "autoloanequitytransfer": return MismoCreditLoanType.AutoLoanEquityTransfer;
                case "automobile": return MismoCreditLoanType.Automobile;
                case "autorefinance": return MismoCreditLoanType.AutoRefinance;
                case "boat": return MismoCreditLoanType.Boat;
                case "business": return MismoCreditLoanType.Business;
                case "businesscreditcard": return MismoCreditLoanType.BusinessCreditCard;
                case "chargeaccount": return MismoCreditLoanType.ChargeAccount;
                case "checkcreditorlineofcredit": return MismoCreditLoanType.CheckCreditOrLineOfCredit;
                case "childsupport": return MismoCreditLoanType.ChildSupport;
                case "collection": return MismoCreditLoanType.Collection;
                case "collectionattorney": return MismoCreditLoanType.CollectionAttorney;
                case "comaker": return MismoCreditLoanType.Comaker;
                case "combinedcreditplan": return MismoCreditLoanType.CombinedCreditPlan;
                case "commercialcreditobligation": return MismoCreditLoanType.CommercialCreditObligation;
                case "commerciallineofcredit": return MismoCreditLoanType.CommercialLineOfCredit;
                case "commercialmortgage": return MismoCreditLoanType.CommercialMortgage;
                case "conditionalsalescontract": return MismoCreditLoanType.ConditionalSalesContract;
                case "consolidation": return MismoCreditLoanType.Consolidation;
                case "conventionalrealestatemortgage": return MismoCreditLoanType.ConventionalRealEstateMortgage;
                case "creditcard": return MismoCreditLoanType.CreditCard;
                case "creditlinesecured": return MismoCreditLoanType.CreditLineSecured;
                case "debitcard": return MismoCreditLoanType.DebitCard;
                case "debtcounselingservice": return MismoCreditLoanType.DebtCounselingService;
                case "depositrelated": return MismoCreditLoanType.DepositRelated;
                case "educational": return MismoCreditLoanType.Educational;
                case "employment": return MismoCreditLoanType.Employment;
                case "factoringcompanyaccount": return MismoCreditLoanType.FactoringCompanyAccount;
                case "familysupport": return MismoCreditLoanType.FamilySupport;
                case "farmershomeadministrationfhma": return MismoCreditLoanType.FarmersHomeAdministrationFHMA;
                case "federalconsolidatedloan": return MismoCreditLoanType.FederalConsolidatedLoan;
                case "fhacomakernotborrower": return MismoCreditLoanType.FHAComakerNotBorrower;
                case "fhahomeimprovement": return MismoCreditLoanType.FHAHomeImprovement;
                case "fharealestatemortgage": return MismoCreditLoanType.FHARealEstateMortgage;
                case "financestatement": return MismoCreditLoanType.FinanceStatement;
                case "governmentbenefit": return MismoCreditLoanType.GovernmentBenefit;
                case "governmentemployeeadvance": return MismoCreditLoanType.GovernmentEmployeeAdvance;
                case "governmentfeeforservice": return MismoCreditLoanType.GovernmentFeeForService;
                case "governmentfine": return MismoCreditLoanType.GovernmentFine;
                case "governmentgrant": return MismoCreditLoanType.GovernmentGrant;
                case "governmentmiscellaneousdebt": return MismoCreditLoanType.GovernmentMiscellaneousDebt;
                case "governmentoverpayment": return MismoCreditLoanType.GovernmentOverpayment;
                case "governmentsecureddirectloan": return MismoCreditLoanType.GovernmentSecuredDirectLoan;
                case "governmentsecuredguaranteeloan": return MismoCreditLoanType.GovernmentSecuredGuaranteeLoan;
                case "governmentunsecureddirectloan": return MismoCreditLoanType.GovernmentUnsecuredDirectLoan;
                case "governmentunsecuredguaranteeloan": return MismoCreditLoanType.GovernmentUnsecuredGuaranteeLoan;
                case "homeequitylineofcredit": return MismoCreditLoanType.HomeEquityLineOfCredit;
                case "homeimprovement": return MismoCreditLoanType.HomeImprovement;
                case "householdgoods": return MismoCreditLoanType.HouseholdGoods;
                case "householdgoodsandothercollateralauto": return MismoCreditLoanType.HouseholdGoodsAndOtherCollateralAuto;
                case "householdgoodssecured": return MismoCreditLoanType.HouseholdGoodsSecured;
                case "installmentloan": return MismoCreditLoanType.InstallmentLoan;
                case "installmentsalescontract": return MismoCreditLoanType.InstallmentSalesContract;
                case "insuranceclaims": return MismoCreditLoanType.InsuranceClaims;
                case "lease": return MismoCreditLoanType.Lease;
                case "lenderplacedinsurance": return MismoCreditLoanType.LenderPlacedInsurance;
                case "manualmortgage": return MismoCreditLoanType.ManualMortgage;
                case "medicaldebt": return MismoCreditLoanType.MedicalDebt;
                case "mobilehome": return MismoCreditLoanType.MobileHome;
                case "mortgage": return MismoCreditLoanType.Mortgage;
                case "noteloan": return MismoCreditLoanType.NoteLoan;
                case "noteloanwithcomaker": return MismoCreditLoanType.NoteLoanWithComaker;
                case "other": return MismoCreditLoanType.Other;
                case "partiallysecured": return MismoCreditLoanType.PartiallySecured;
                case "personalloan": return MismoCreditLoanType.PersonalLoan;
                case "realestatejuniorliens": return MismoCreditLoanType.RealEstateJuniorLiens;
                case "realestateloanequitytransfer": return MismoCreditLoanType.RealEstateLoanEquityTransfer;
                case "realestatemortgagewithoutothercollateral": return MismoCreditLoanType.RealEstateMortgageWithoutOtherCollateral;
                case "realestatespecifictypeunknown": return MismoCreditLoanType.RealEstateSpecificTypeUnknown;
                case "recreational": return MismoCreditLoanType.Recreational;
                case "recreationalvehicle": return MismoCreditLoanType.RecreationalVehicle;
                case "refinance": return MismoCreditLoanType.Refinance;
                case "refundanticipationloan": return MismoCreditLoanType.RefundAnticipationLoan;
                case "rentalagreement": return MismoCreditLoanType.RentalAgreement;
                case "residentialloan": return MismoCreditLoanType.ResidentialLoan;
                case "returnedcheck": return MismoCreditLoanType.ReturnedCheck;
                case "revolvingbusinesslines": return MismoCreditLoanType.RevolvingBusinessLines;
                case "secured": return MismoCreditLoanType.Secured;
                case "securedbycosigner": return MismoCreditLoanType.SecuredByCosigner;
                case "securedcreditcard": return MismoCreditLoanType.SecuredCreditCard;
                case "securedhomeimprovement": return MismoCreditLoanType.SecuredHomeImprovement;
                case "singlepaymentloan": return MismoCreditLoanType.SinglePaymentLoan;
                case "spousesupport": return MismoCreditLoanType.SpouseSupport;
                case "summaryofaccountswithsamestatus": return MismoCreditLoanType.SummaryOfAccountsWithSameStatus;
                case "timesharedloan": return MismoCreditLoanType.TimeSharedLoan;
                case "title1loan": return MismoCreditLoanType.Title1Loan;
                case "unknown":
                case "unknownloantype": return MismoCreditLoanType.UnknownLoanType;
                case "unsecured": return MismoCreditLoanType.Unsecured;
                case "veteransadministrationloan": return MismoCreditLoanType.VeteransAdministrationLoan;
                case "veteransadministrationrealestatemortgage": return MismoCreditLoanType.VeteransAdministrationRealEstateMortgage;
                case "bimonthlymortgageterminyears": return MismoCreditLoanType.BiMonthlyMortgageTermInYears;
                case "constructionloan": return MismoCreditLoanType.ConstructionLoan;
                case "deferredstudentloan": return MismoCreditLoanType.DeferredStudentLoan;
                case "secondmortgage": return MismoCreditLoanType.SecondMortgage;
                case "semimonthlymortgagepayment": return MismoCreditLoanType.SemiMonthlyMortgagePayment;

                default:
                    if (type != "") 
                    {
                        if (null != liability)
                            liability.LogCreditWarning(type + " is not a valid Mismo Credit Loan Type.");
                        else
                            Tools.LogWarning(type + " is not a valid Mismo Credit Loan Type.");
                    }
                    return MismoCreditLoanType.CreditCard;
            }
        }

        public static bool IsHomeEquityOrLineCredit(MismoCreditLoanType type) 
        {
            switch (type) 
            {
                // External OPM 50198 - Commercial line of credit should not be considered a mortgage,
				// and this function is used to determine if a tradeline is a mortgage even if its type is not mortgage
				//case MismoCreditLoanType.CommercialLineOfCredit:
                case MismoCreditLoanType.CommercialMortgage:
                case MismoCreditLoanType.ConventionalRealEstateMortgage:
                case MismoCreditLoanType.FHARealEstateMortgage:
                case MismoCreditLoanType.HomeEquityLineOfCredit:
                case MismoCreditLoanType.Mortgage:
                case MismoCreditLoanType.VeteransAdministrationRealEstateMortgage:
                    return true;
                default:
                        return false;
            }
        }
    }
    public class MismoCreditLiabilityRatingMap 
    {
        public static MismoCreditLiabilityRating Parse(string type, AbstractCreditLiability liability) 
        {
            // 9/12/2005 dd - Just perform case insensitive search.
            if (null != type && "" != type)
                type = type.ToLower();

            switch (type) 
            {
                case "asagreed":                   return MismoCreditLiabilityRating.AsAgreed;
                case "chargeoff":                  return MismoCreditLiabilityRating.ChargeOff;
                case "bankruptcyorwageearnerplan": return MismoCreditLiabilityRating.BankruptcyOrWageEarnerPlan;
                case "collection":                 return MismoCreditLiabilityRating.Collection; 
                case "collectionorchargeoff":      return MismoCreditLiabilityRating.CollectionOrChargeOff;  
                case "foreclosure":                return MismoCreditLiabilityRating.Foreclosure; 
                case "foreclosureorrepossession":  return MismoCreditLiabilityRating.ForeclosureOrRepossession; 
                case "late30days":                 return MismoCreditLiabilityRating.Late30Days; 
                case "late60days":                 return MismoCreditLiabilityRating.Late60Days; 
                case "late90days":                 return MismoCreditLiabilityRating.Late90Days; 
                case "lateover120days":            return MismoCreditLiabilityRating.LateOver120Days; 
                case "nodataavailable":            return MismoCreditLiabilityRating.NoDataAvailable;
                case "norating":                   return MismoCreditLiabilityRating.NoDataAvailable;
                case "repossession":               return MismoCreditLiabilityRating.Repossession; 
                case "toonew":                     return MismoCreditLiabilityRating.TooNew; 
                case "wageearnerplan":             return MismoCreditLiabilityRating.WageEarnerPlan; 
                default:
                    if (type != "")
                    {
                        if (null != liability)
                            liability.LogCreditWarning(type + " is not a valid Mismo Rating.");
                        else
                            Tools.LogWarning(type + " is not a valid Mismo Rating.");
                    }
                    return MismoCreditLiabilityRating.AsAgreed;
            }
        }
        public static bool IsReposession(MismoCreditLiabilityRating rating) 
        {
            switch (rating) 
            {
                case MismoCreditLiabilityRating.Repossession:
                case MismoCreditLiabilityRating.ForeclosureOrRepossession:
                    return true;
                default:
                    return false;
            }
        }
        public static bool IsChargeOff(MismoCreditLiabilityRating rating) 
        {
            switch (rating) 
            {
                case MismoCreditLiabilityRating.ChargeOff:
                case MismoCreditLiabilityRating.CollectionOrChargeOff:
                    return true;
                default: 
                    return false;
            }
        }
/*
        public static bool IsCollection(MismoCreditLiabilityRating rating) 
        {
            switch (rating) 
            {
                case MismoCreditLiabilityRating.Collection:
                case MismoCreditLiabilityRating.CollectionOrChargeOff:
                    return true;
                default: 
                    return false;
            }
        }
		*/
        public static bool IsForeclosure(MismoCreditLiabilityRating rating) 
        {
            switch (rating) 
            {
                case MismoCreditLiabilityRating.Foreclosure:
                case MismoCreditLiabilityRating.ForeclosureOrRepossession:
                    return true;
                default:
                    return false;
            }
        }
        public static bool IsLate(MismoCreditLiabilityRating rating) 
        {
            switch (rating) 
            {
                case MismoCreditLiabilityRating.Late30Days:
                case MismoCreditLiabilityRating.Late60Days:
                case MismoCreditLiabilityRating.Late90Days:
                case MismoCreditLiabilityRating.LateOver120Days:
                    return true;
                default:
                    return false;
            }
        }
        public static bool IsBadRating(MismoCreditLiabilityRating rating) 
        {
            switch (rating) 
            {
                case MismoCreditLiabilityRating.BankruptcyOrWageEarnerPlan:
                case MismoCreditLiabilityRating.ChargeOff:
                case MismoCreditLiabilityRating.Collection:
                case MismoCreditLiabilityRating.CollectionOrChargeOff:
                case MismoCreditLiabilityRating.Foreclosure:
                case MismoCreditLiabilityRating.ForeclosureOrRepossession:
                case MismoCreditLiabilityRating.Late30Days:
                case MismoCreditLiabilityRating.Late60Days:
                case MismoCreditLiabilityRating.Late90Days:
                case MismoCreditLiabilityRating.LateOver120Days:
                case MismoCreditLiabilityRating.Repossession:
                case MismoCreditLiabilityRating.WageEarnerPlan:
                    return true;
                default:
                    return false;

            }
        }

        public static bool IsTerminalDerog(MismoCreditLiabilityRating rating) 
        {
            switch (rating) 
            {
                case MismoCreditLiabilityRating.BankruptcyOrWageEarnerPlan:
                case MismoCreditLiabilityRating.ChargeOff:
                case MismoCreditLiabilityRating.Collection:
                case MismoCreditLiabilityRating.CollectionOrChargeOff:
                case MismoCreditLiabilityRating.Foreclosure:
                case MismoCreditLiabilityRating.ForeclosureOrRepossession:
                case MismoCreditLiabilityRating.Repossession:
                case MismoCreditLiabilityRating.WageEarnerPlan:
                    return true;
                default:
                    return false;

            }
        }

        public static bool IsActive(MismoCreditLiabilityRating rating) 
        {
            switch (rating) 
            {
                case MismoCreditLiabilityRating.TooNew:
                case MismoCreditLiabilityRating.AsAgreed:
                case MismoCreditLiabilityRating.NoDataAvailable:
                    return true;
                default:
                    return false;
            }
        }
    }

    public enum MismoCreditLiabilityAccountStatusType 
    {
        Closed,
        Frozen,
        Open,
        Paid,
        Refinanced,
        Transferred
    }
    public class MismoCreditLiabilityAccountStatusTypeMap 
    {
		public static bool IsActive( MismoCreditLiabilityAccountStatusType type )
		{
			switch( type )
			{
				case MismoCreditLiabilityAccountStatusType.Closed:
				case MismoCreditLiabilityAccountStatusType.Frozen:
				case MismoCreditLiabilityAccountStatusType.Paid:
				case MismoCreditLiabilityAccountStatusType.Refinanced:
				case MismoCreditLiabilityAccountStatusType.Transferred:
					return false;
				case MismoCreditLiabilityAccountStatusType.Open:
					return true;
				default:
					throw new CBaseException( ErrorMessages.Generic, "Unhandled MismoCreditLiabilityAccountStatusType enum value in MismoCreditLiabilityAccountStatusTypeMap.IsActive()" );
			}
		}
        public static MismoCreditLiabilityAccountStatusType Parse(string type, AbstractCreditLiability liability) 
        {
            // 9/12/2005 dd - Just perform case insensitive search.
            if (null != type && "" != type)
                type = type.ToLower();
            switch (type) 
            {
                case "closed":      return MismoCreditLiabilityAccountStatusType.Closed;
                case "frozen":      return MismoCreditLiabilityAccountStatusType.Frozen;
                case "open":        return MismoCreditLiabilityAccountStatusType.Open;
                case "paid":        return MismoCreditLiabilityAccountStatusType.Paid;
                case "refinanced":  return MismoCreditLiabilityAccountStatusType.Refinanced;
                case "transferred": return MismoCreditLiabilityAccountStatusType.Transferred;
                default:
                    if (type != "") 
                    {
                        if (null != liability)
                            liability.LogCreditWarning(type + " is not a valid Mismo Liability Account Status Type.");
                        else
                            Tools.LogWarning(type + " is not a valid Mismo Liability Account Status Type.");
                    }
                    return MismoCreditLiabilityAccountStatusType.Open;

            }
        }
    }
    public enum MismoCreditLiabilityAccountOwnershipType 
    {
        AuthorizedUser,
        Comaker,
        Individual,
        JointContractualLiability,
        JointParticipating,
        Maker,
        OnBehalfOf,
        Terminated,
        Undesignated
    }
    public class MismoCreditLiabilityAccountOwnershipTypeMap 
    {
        public static MismoCreditLiabilityAccountOwnershipType Parse(string type, AbstractCreditLiability liability) 
        {
            // 9/12/2005 dd - Just perform case insensitive search.
            if (null != type && "" != type)
                type = type.ToLower();
            switch (type) 
            {
                case "authorizeduser" : return MismoCreditLiabilityAccountOwnershipType.AuthorizedUser;
                case "comaker" : return MismoCreditLiabilityAccountOwnershipType.Comaker;
                case "individual" : return MismoCreditLiabilityAccountOwnershipType.Individual;
                case "jointcontractualliability" : return MismoCreditLiabilityAccountOwnershipType.JointContractualLiability;
                case "jointparticipating" : return MismoCreditLiabilityAccountOwnershipType.JointParticipating;
                case "maker" : return MismoCreditLiabilityAccountOwnershipType.Maker;
                case "onbehalfof" : return MismoCreditLiabilityAccountOwnershipType.OnBehalfOf;
                case "terminated" : return MismoCreditLiabilityAccountOwnershipType.Terminated;
                case "undesignated" : return MismoCreditLiabilityAccountOwnershipType.Undesignated;
                default:
                    if (type != "") 
                    {
                        if (null != liability)
                            liability.LogCreditWarning(type + " is not a valid Mismo Liability Account Ownership Type.");
                        else
                            Tools.LogWarning(type + " is not a valid Mismo Liability Account Ownership Type.");
                    }
                    return MismoCreditLiabilityAccountOwnershipType.Undesignated; //av opm 30868
            }
        }
    }


    public class MismoAdverseRating 
    {
        private string m_code = "";
        private DateTime m_date = DateTime.MinValue;
        private MismoCreditLiabilityRating m_type = MismoCreditLiabilityRating.AsAgreed;
        private decimal m_amount = 0.0M;
        private AbstractCreditLiability m_liability = null;

        public MismoAdverseRating(MismoCreditLiabilityRating type, DateTime date) 
        {
            m_date = date;
            m_type = type;
        }
        public MismoAdverseRating(XmlElement el, AbstractCreditLiability liability) 
        {
            m_liability = liability;
            Parse(el);
        }

        private void Parse(XmlElement el) 
        {
            try 
            {
                m_amount = decimal.Parse(el.GetAttribute("_Amount"));
            } 
            catch {}
            m_code = el.GetAttribute("_Code");
            m_date = MismoUtilities.ParseDateTime(el.GetAttribute("_Date"));
            m_type = MismoCreditLiabilityRatingMap.Parse(el.GetAttribute("_Type"), m_liability);
        }

        public string Code 
        {
            get { return m_code; }
        }

        public DateTime Date 
        {
            get { return m_date; }
        }

        public MismoCreditLiabilityRating Type 
        {
            get { return m_type; }
        }

        public decimal Amount 
        {
            get { return m_amount; }
        }
        public override bool Equals(object o) 
        {
            MismoAdverseRating a = o as MismoAdverseRating;
            if (null == a)
                return false;

            return (a.Type == this.Type && a.Date == this.Date);
        }
        public override int GetHashCode() 
        {
            return m_type.GetHashCode() ^ m_date.GetHashCode();
        }
        public override string ToString() 
        {
            return string.Format("MismoAdverseRating:Type={0},Date={1}", m_type, m_date);
        }
    }

    /// <summary>
    /// Represents a CREDIT_COMMENT element.
    /// </summary>
    public class MismoCreditComment
    {
        /// <summary>
        /// Comment code (bureau specific).
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Comment source (usually a credit bureau).
        /// </summary>
        public string SourceType { get; set;}

        /// <summary>
        /// List of child TEXT elements.
        /// </summary>
        public List<string> Text { get; set; }

        /// <summary>
        /// Parses member values from passed in XML element.
        /// </summary>
        /// <param name="el">A CREDIT_COMMENT XML element.</param>
        public MismoCreditComment(XmlElement el)
        {
            this.Text = new List<string>();
            Parse(el);
        }

        private void Parse(XmlElement el)
        {
            this.SourceType = el.GetAttribute("_SourceType");

            this.Code = el.GetAttribute("_Code");

            XmlNodeList nodeList = el.SelectNodes("_Text");

            foreach (XmlNode node in nodeList)
            {
                if (!this.Text.Contains(node.InnerText))
                    this.Text.Add(node.InnerText);
            }
        }
    }
    #endregion
    
    public class MismoCreditLiability : AbstractCreditLiability 
    {

        #region Private Member Variables
        private string m_id; // DONE
        private string m_creditorName = ""; // DONE
        private string m_creditorAddress = ""; // DONE
        private string m_creditorCity = ""; // DONE
        private string m_creditorState = ""; // DONE
        private string m_creditorZipcode = ""; // DONE
        private string m_creditorPhone = ""; // DONE
        private DateTime m_accountOpenedDate; // DONE
        private DateTime m_accountReportedDate; // DONE
        private DateTime m_accountClosedDate; // DONE
        private decimal m_pastDueAmount; // DONE
        private decimal m_highCreditAmount; // DONE
        private string m_accountIdentifier = ""; // DONE
        private string m_termsMonthsCount = ""; // DONE
        private decimal m_unpaidBalanceAmount; // DONE
        private decimal m_monthlyPaymentAmount; // DONE
        private string m_monthsRemainingCount = "";
        private string m_late30 = ""; // DONE
        private string m_late60 = ""; // DONE
        private string m_late90 = ""; // DONE
        private string m_late120 = ""; // DONE
        private int m_monthsReviewedCount; // DONE
        private DateTime m_lastActivityDate; // DONE
        private E_DebtRegularT m_liabilityType; // DONE
        private E_LiaOwnerT m_liaOwnerT; // DONE

        private Mismo.MismoCreditLiabilityAccountStatusType m_accountStatusType; // DONE
        private Mismo.MismoCreditLiabilityAccountOwnershipType m_accountOwnershipType; // DONE
        private string m_borrowerID; // DONE

        private bool m_isForeclosure = false; // DONE
        private bool m_isBankruptcy = false; // DONE
        private bool m_isChargeOff = false; // DONE
        private MismoCreditLiabilityRating m_currentRatingType = MismoCreditLiabilityRating.AsAgreed; // DONE
        private DateTime m_paymentPatternStartDate; // DONE
        private string m_paymentPatternData; // DONE;
        private bool m_isRepos = false; // DONE
        private bool x_isCollection = false; // DONE
        private bool m_isLate = false; // DONE
        private bool m_isInactiveButNeutralStatus = false; // DONE
        private bool m_isInactiveButNegativeStatus = false; // DONE
        private bool m_isDerogatoryData = false; // DONE

        private MismoCreditBusinessType m_creditBusinessType = MismoCreditBusinessType._UNKNOWN;
        private MismoAdverseRating m_highestAdverseRating = null;
        private MismoAdverseRating m_mostRecentAdverseRating = null;
        private ArrayList m_adverseRatings = new ArrayList(); 

        private MismoCreditLoanType m_creditLoanType;
		private List<MismoCreditComment> m_creditComments = new List<MismoCreditComment>();
        private decimal m_highBalanceAmount = 0.0M;
		private decimal m_creditLimitAmount = 0.0M;
        private bool m_liabilityRepossessionOnly = false;
        private bool m_liabilityForeclosureOnly = false;
        private bool m_hasTerminalDerog = false;

        #endregion

        #region Implement Abstract Methods
        public override string ID 
        { 
            get { return m_id;  }
        }

		//external OPM 44960 
		public override bool IsDischargedBk
		{
			get
			{
				foreach (string sComment in m_creditComments.SelectMany(cc => cc.Text))
				{
					string sCommentCap = sComment.ToUpper();
					if (sCommentCap.IndexOf("DISCHARGED-BANKRUPTCY") >= 0)
						return true;
				}
				return false;
			}
		}

        public override  bool HasTerminalDerog { get { return m_hasTerminalDerog; } }
        public override string CreditorName 
        { 
            get { return m_creditorName; }
        }

        public override string CreditorAddress 
        { 
            get { return m_creditorAddress; }
        }

        public override string CreditorCity 
        { 
            get { return m_creditorCity; }
        }
        public override string CreditorState 
        { 
            get { return m_creditorState; }
        }
        public override string CreditorZipcode 
        { 
            get { return m_creditorZipcode; }
        }
        public override string CreditorPhone 
        { 
            get { return m_creditorPhone; }
        }

        public override DateTime AccountOpenedDate 
        { 
            get { return m_accountOpenedDate; }
        }
        public override DateTime AccountReportedDate 
        { 
            get { return m_accountReportedDate; }
        }
        public override decimal PastDueAmount 
        { 
            get { return m_pastDueAmount; }
        }
        public override decimal HighCreditAmount 
        { 
            get { return m_highCreditAmount; }
        }
        public override string AccountIdentifier 
        { 
            get { return m_accountIdentifier; }
        }
        public override string TermsMonthsCount 
        { 
            get { return m_termsMonthsCount; }
        }
        public override decimal UnpaidBalanceAmount 
        { 
            get { return m_unpaidBalanceAmount; }
        }
        public override decimal MonthlyPaymentAmount 
        { 
            get { return m_monthlyPaymentAmount; }
        }
        public override string MonthsRemainingCount 
        {
            get { return m_monthsRemainingCount; }
        }

        public override void UpdateUnpaidBalanceAmount(decimal value) 
        {
            if (m_modifiedString != "")
                m_modifiedString += " ";
            m_modifiedString += "BALANCE ON CREDIT REPORT: " + m_unpaidBalanceAmount + ", MODIFIED BALANCE: " + value + ".";
            m_unpaidBalanceAmount = value;
            m_isModified = true;
        }
        public override void UpdateMonthlyPaymentAmount(decimal value) 
        {
            if (m_modifiedString != "")
                m_modifiedString += " ";

            m_modifiedString += "PAYMENT ON CREDIT REPORT: " + m_monthlyPaymentAmount + ", MODIFIED PAYMENT: " + value + ".";

            m_monthlyPaymentAmount = value;
            m_isModified = true;
        }

        private bool m_isModified = false;
        public override bool IsModified 
        {
            get { return m_isModified; }
        }
        private string m_modifiedString = "";
        public override string ModifiedString 
        {
            get { return m_modifiedString; }
        }
        public override string Late30 
        { 
            get { return m_late30; }
        }
        public override string Late60 
        { 
            get { return m_late60; }
        }
        public override string Late90 
        { 
            get { return m_late90; }
        }
        public override string Late120 
        { 
            get { return m_late120; } // Only define in Mismo.
        }

        public override int MonthsReviewedCount 
        { 
            get { return m_monthsReviewedCount; }
        }
        public override DateTime LastActivityDate 
        { 
            get { return m_lastActivityDate; }
        }

        public override E_DebtRegularT LiabilityType 
        { 
            get { return m_liabilityType; }
        }
        public override E_LiaOwnerT LiaOwnerT 
        {
            get { return m_liaOwnerT; }
        }
        public override bool IsForeclosure 
        { 
            get 
            {
                // 10/24/2008 dd - Only Mortgage can be flag as isForeclosure. See OPM 25630 and 
                // OPM 3465 - Need to make sure never categorize auto reposession as foreclosure.
                // OPM 24318 - Short sales should not count as foreclosures.
                return m_isForeclosure && LiabilityType == E_DebtRegularT.Mortgage && !IsShortSale; 
            }
        }
        public override bool IsBankruptcy 
        { 
            get { return m_isBankruptcy || IsInCreditCounseling; }
        }
        public override bool IsChargeOff 
        {
            get
            {
                // OPM 24318 - Short sales should not count as charge off.
                return m_isChargeOff && !IsShortSale;
            }
        }
        public override DateTime PaymentPatternStartDate 
        {
            get { return m_paymentPatternStartDate; }
        }

        public override bool IsCollection
        { 
            get { return x_isCollection; }
        }
        public override bool IsRepos 
        { 
            get { return m_isRepos; }
        }
        public override bool IsLate 
        {
            get { return m_isLate; }
        }
        private bool m_creditCounselingIndicator = false;
        public override bool IsInCreditCounseling 
        {
            // OPM 2083 - I don't know how to detect if tradeline is in credit counseling for MISMO
            get 
            { 
                // OPM 3112 - Hopefully this is the right value to detect IsInCreditCouseling for Mismo.
                return  m_creditCounselingIndicator ||  m_creditLoanType == MismoCreditLoanType.DebtCounselingService;
            }
        }

        public override bool IsAuthorizedUser 
        {
            // 1/30/2006 dd - OPM 3949 - Implement IsAuthorizedUser
            get { return m_accountOwnershipType == MismoCreditLiabilityAccountOwnershipType.AuthorizedUser; }
        }
		public override bool IsGovMiscDebt 
		{
			// 9/22/2006 nw - External OPM 13872 - Handle Government Miscellaneous Debt tradelines
			get { return m_creditLoanType == MismoCreditLoanType.GovernmentMiscellaneousDebt; }
		}
        public override bool IsStudentLoansNotInRepayment 
        {
            // 1/30/2006 dd - OPM 3949
            get
			{
				if (m_creditLoanType == MismoCreditLoanType.DeferredStudentLoan)
					return true;

				// 4/26/2007 nw - OPM 12710 - Credco reports this differently from other MISMO reports
				if (m_creditLoanType == MismoCreditLoanType.Educational)
				{
					foreach (string sComment in m_creditComments.SelectMany(cc => cc.Text))
					{
						string sCommentCap = sComment.ToUpper();
						if (sCommentCap.IndexOf("DEFERRED STUDENT LOAN") >= 0)
							return true;
						else if (sCommentCap.IndexOf("PAYMENT DEFERRED") >= 0)
							return true;
						else if (sCommentCap.IndexOf("ACCT TRANSFERRED") >= 0)
							return true;
						else if (sCommentCap.IndexOf("STUDENT LOAN NOT IN REPAYMENT") >= 0)
							return true;
					}
				}

				return false;
			}
        }

        /// <summary>
        /// Get a value indicating whether this liability is for a short sale.
        /// </summary>
        /// <value>True, iof this liability is for a short sale.</value>
        public override bool IsShortSale
        {
            get
            {
                if (!IsTypeMortgage)
                {
                    return false;
                }

                return m_creditComments.Exists(cc =>
                    (string.Equals(cc.SourceType, "Experian", StringComparison.OrdinalIgnoreCase)
                        && string.Equals(cc.Code, "68", StringComparison.OrdinalIgnoreCase))
                    || (string.Equals(cc.SourceType, "Equifax", StringComparison.OrdinalIgnoreCase)
                        && string.Equals(cc.Code, "BZ", StringComparison.OrdinalIgnoreCase))
                    || (string.Equals(cc.SourceType, "Transunion", StringComparison.OrdinalIgnoreCase)
                        && string.Equals(cc.Code, "SET", StringComparison.OrdinalIgnoreCase))
                );
            }
        }
       
            public override ArrayList CreditAdverseRatingList 
        { 
            get { return m_adverseRatings; }
        }

        public override bool IsMedicalTradeline 
        {
            get
			{
				if (m_creditBusinessType == MismoCreditBusinessType.MedicalAndHealth)
					return true;

				// 12/11/2006 nw - OPM 8762 - Detect medical collection tradelines using credit comments
				foreach (string sComment in m_creditComments.SelectMany(cc => cc.Text))
				{
					if (sComment.ToUpper().IndexOf("COLLECTION MEDICAL PAYMENT DATA") >= 0)
						return true;
					else if (sComment.ToUpper().IndexOf("UNPAID MEDICAL") >= 0)
						return true;
					else if (sComment.ToUpper().IndexOf("MEDICAL") >= 0)
						return true;
				}
				return false;
			}
        }
        #endregion

        public MismoCreditLiability(XmlElement el, string borrowerID, string coborrowerID, AbstractCreditReport parent) 
        {
            ParentCreditReport = parent;
            Parse(el, borrowerID, coborrowerID);
        }
        private void Parse(XmlElement el, string borrowerID, string coborrowerID) 
        {
            // 12/15/2004 dd - _PAYMENT_PATTERN need to be the first to be parse for AdverseList to be correct.
            ParsePaymentPattern((XmlElement) el.SelectSingleNode("_PAYMENT_PATTERN")); // DONE

            ParseCreditor((XmlElement) el.SelectSingleNode("_CREDITOR")); // DONE
            ParseCurrentRating((XmlElement) el.SelectSingleNode("_CURRENT_RATING")); // DONE
            ParseLateCount((XmlElement) el.SelectSingleNode("_LATE_COUNT")); // DONE
            ParseCreditComments(el.SelectNodes("CREDIT_COMMENT"));

            m_id                         = el.GetAttribute("CreditLiabilityID");
            m_borrowerID                 = el.GetAttribute("BorrowerID");
            m_accountIdentifier          = el.GetAttribute("_AccountIdentifier");
            m_accountOpenedDate          = MismoUtilities.ParseDateTime(el.GetAttribute("_AccountOpenedDate"));
            m_accountReportedDate        = MismoUtilities.ParseDateTime(el.GetAttribute("_AccountReportedDate"));
            m_accountClosedDate          = MismoUtilities.ParseDateTime(el.GetAttribute("_AccountClosedDate"));

            m_pastDueAmount              = GetDecimal(el, "_PastDueAmount");
            m_termsMonthsCount           = el.GetAttribute("_TermsMonthsCount");
            m_unpaidBalanceAmount        = GetDecimal(el, "_UnpaidBalanceAmount");
            m_monthlyPaymentAmount       = GetDecimal(el, "_MonthlyPaymentAmount");
            m_monthsRemainingCount       = el.GetAttribute("_MonthsRemainingCount");
            m_monthsReviewedCount        = GetInt(el, "_MonthsReviewedCount");
            m_lastActivityDate           = MismoUtilities.ParseDateTime(el.GetAttribute("_LastActivityDate"));
            m_accountOwnershipType       = Mismo.MismoCreditLiabilityAccountOwnershipTypeMap.Parse(el.GetAttribute("_AccountOwnershipType"), this);
            m_accountStatusType          = MismoCreditLiabilityAccountStatusTypeMap.Parse(el.GetAttribute("_AccountStatusType"), this);
            m_isDerogatoryData           = el.GetAttribute("_DerogatoryDataIndicator") == "Y";
            m_creditBusinessType         = MismoCreditBusinessTypeMap.Parse(el.GetAttribute("CreditBusinessType"));
            m_creditLoanType                 = MismoCreditLoanTypeMap.Parse(el.GetAttribute("CreditLoanType"), this);
			
			switch( m_creditBusinessType )
			{
				case MismoCreditBusinessType.CollectionServices:
					x_isCollection = true;
					break;
				default:
					break;
			}

			switch( m_creditLoanType )
			{
				case MismoCreditLoanType.Collection:
					x_isCollection = true;    // see internal opm case 2956.  Tradeline open to collect debt is counted as collection as well as the
											  // original debt got put into collection status. See also external opm 4373.
					break;
				default:
					break;
			}

            m_highCreditAmount           = GetDecimal(el, "_HighCreditAmount");
            m_highBalanceAmount              = GetDecimal(el, "_HighBalanceAmount");
            // See OPM #2629 to see why i modify m_highCreditAmount;
            m_highCreditAmount = Math.Max(m_highCreditAmount, m_highBalanceAmount);

			// 2/16/2007 nw - OPM 10441 - Info1/LandAmerica uses _CreditLimitAmount instead of _HighBalanceAmount
			m_creditLimitAmount              = GetDecimal(el, "_CreditLimitAmount");
			m_highCreditAmount = Math.Max(m_highCreditAmount, m_creditLimitAmount);

            bool hasCreditCounselingIndicator = el.HasAttribute("CreditCounselingIndicator"); // This attribute is new in version 2.3.1
            if (hasCreditCounselingIndicator) 
            {
                m_creditCounselingIndicator = el.GetAttribute("CreditCounselingIndicator") == "Y";
            }
            

            // 6/13/2005 dd - Need to invoke GetDebtRegularT after m_creditLoanType, 
			// nw - and after ParseCreditComment(...), 
			// 11/28/2006 nw - External OPM 17891 - and after m_highCreditAmount, 
			// 2/13/2007 nw - OPM 10299 - but before ParseHighestAdverseRating, ParseMostRecentAdverseRating, ParsePriorAdverseRating
            m_liabilityType              = GetDebtRegularT(el.GetAttribute("_AccountType"));
			ParseHighestAdverseRating((XmlElement) el.SelectSingleNode("_HIGHEST_ADVERSE_RATING")); // DONE
			ParseMostRecentAdverseRating((XmlElement) el.SelectSingleNode("_MOST_RECENT_ADVERSE_RATING")); // DONE
			ParsePriorAdverseRating((XmlNodeList) el.SelectNodes("_PRIOR_ADVERSE_RATING")); // DONE

            switch (m_accountOwnershipType) 
            {
                case Mismo.MismoCreditLiabilityAccountOwnershipType.AuthorizedUser:
                case Mismo.MismoCreditLiabilityAccountOwnershipType.Individual:
                case Mismo.MismoCreditLiabilityAccountOwnershipType.Maker:
                case Mismo.MismoCreditLiabilityAccountOwnershipType.OnBehalfOf:
                    if (m_borrowerID == coborrowerID) 
                    {
                        m_liaOwnerT = E_LiaOwnerT.CoBorrower;
                    } 
                    else 
                    {
                        m_liaOwnerT = E_LiaOwnerT.Borrower;
                    }
                    // 4/29/2005 dd - Hack for Universal Credit. Universal Credit does not use the same BorrowerID as BORROWER tag.
                    // Instead it use Borrower / CoBorrower.
                    if (m_borrowerID == "CoBorrower")
                        m_liaOwnerT = E_LiaOwnerT.CoBorrower;
                    else if (m_borrowerID == "Borrower")
                        m_liaOwnerT = E_LiaOwnerT.Borrower;

                    break;
                case Mismo.MismoCreditLiabilityAccountOwnershipType.Comaker:
                case Mismo.MismoCreditLiabilityAccountOwnershipType.JointContractualLiability:
                case Mismo.MismoCreditLiabilityAccountOwnershipType.JointParticipating:
                    m_liaOwnerT = E_LiaOwnerT.Joint;
                    break;
                default:
                    m_liaOwnerT = E_LiaOwnerT.Borrower;
                    break;
            }

            // 4/27/2007 nw - OPM 12728 - Kroll Factual Data may report BK in credit comment
            foreach (string sComment in m_creditComments.SelectMany(cc => cc.Text))
            {
                string sCommentCap = sComment.ToUpper();
                if (sCommentCap.IndexOf("INCLUDED IN BANKRUPTCY") >= 0 ||
                    sCommentCap.IndexOf("CHAPTER 7") >= 0 ||
                    sCommentCap.IndexOf("CHAPTER 11") >= 0 ||
                    sCommentCap.IndexOf("CHAPTER 12") >= 0 ||
                    sCommentCap.IndexOf("CHAPTER 13") >= 0)
                {
                    m_currentRatingType = MismoCreditLiabilityRating.BankruptcyOrWageEarnerPlan;
                }
            }

            // 7/2/2007 dd - External 38578 - If there is no 'CreditCounselingIndicator' attribute then we should scan through comments to detect if tradeline is in counseling.
            if (!hasCreditCounselingIndicator)
            {
                foreach (string sComment in m_creditComments.SelectMany(cc => cc.Text))
                {
                    string str = sComment.ToUpper();
                    if (str.IndexOf("PAYMENTS MANAGED BY FINANCIAL COUNSELING PROGRAM") >= 0 ||
                        str.IndexOf("ACCOUNT BEING PAID THROUGH FINANCIAL COUNSELING PLAN") >= 0 ||
                        str.IndexOf("ACCOUNT PAYMENTS MANAGED BY DEBT COUNSELING PROGRAM") >= 0 ||
                        str.IndexOf("DEBT COUNSELING SERVICE") >= 0 ||
                        str.IndexOf("MANAGED BY FINANCIAL COUNSELING SERVICE") >= 0)
                    {
                        m_creditCounselingIndicator = true;
                        break;
                    }
                }
            }

            switch (m_currentRatingType) 
            {
                case MismoCreditLiabilityRating.Foreclosure:
                case MismoCreditLiabilityRating.ForeclosureOrRepossession:
                    m_isForeclosure = !IsShortSale;
                    FindLatestPublicRecordDateOfType("Foreclosure");
                    break;
                case MismoCreditLiabilityRating.ChargeOff:
					 m_isChargeOff = !IsShortSale;
					break;
                case MismoCreditLiabilityRating.Collection:
                    x_isCollection = true;
                    break;
                case MismoCreditLiabilityRating.CollectionOrChargeOff:
                    m_isChargeOff = !IsShortSale;
					x_isCollection = true;
                    break;
                case MismoCreditLiabilityRating.WageEarnerPlan:
                case MismoCreditLiabilityRating.BankruptcyOrWageEarnerPlan:
                    m_isBankruptcy = true;
					// 2/13/2007 nw - OPM 10299 - Flag all mortgages in Wage Earner Plan as in foreclosure
                    // 12/08/07 -- not anymore 
					if (m_liabilityType == E_DebtRegularT.Mortgage)
					{
                        //we are now counting them as 120 lates 
						FindLatestPublicRecordDateOfType("Bankruptcy");
					}
                    break;
            }

			if ((IsChargeOff || x_isCollection) && m_liabilityType == E_DebtRegularT.Mortgage)
			{
				FindLatestPublicRecordDateOfType("Collection");
			}

            // Sort m_adverseRatings.
            m_adverseRatings.Sort(new CreditAdverseRatingComparer());

            SetIfLiabilityForeclosureOnly();
            SetIfLiabilityRepossessionOnly();
            FixUpAdverseRatingList();

            // 12/8/2005 dd - All adverse rating list should be final after this line.
            m_hasTerminalDerog = _HasTerminalDerog();

            m_isRepos = _IsRepos();
            m_isLate = _IsLate() ;
            m_isInactiveButNeutralStatus = _IsInactiveButNeutralStatus();
            m_isInactiveButNegativeStatus = __IsInactiveButNegativeStatus();

            
        }

        private void SetIfLiabilityRepossessionOnly() 
        {
            if (m_currentRatingType == MismoCreditLiabilityRating.Repossession) 
            {
                m_liabilityRepossessionOnly = true;
            } 
            else if (m_creditLoanType == MismoCreditLoanType.AutoLease ||
                m_creditLoanType == MismoCreditLoanType.AutoLoanEquityTransfer ||
                m_creditLoanType == MismoCreditLoanType.Automobile ||
                m_creditLoanType == MismoCreditLoanType.AutoRefinance) 
            {
                // 12/8/2005 dd - For all auto relate loan type categorized as repossession.
                m_liabilityRepossessionOnly = true;
            }
        }

        private void SetIfLiabilityForeclosureOnly() 
        {
            if (m_currentRatingType == MismoCreditLiabilityRating.Foreclosure) 
            {
                m_liabilityForeclosureOnly = true;
            }

        }

        private void FixUpAdverseRatingList()
        {
            bool hasBankruptcy = false;
            // 12/8/2005 dd - OPM 3465 - Need to make sure never categorize auto reposession as foreclosure.
            // Need to separate out the rating RepossessionOrForeclosure.
            int late120Count = 0;
            foreach (CreditAdverseRating rating in m_adverseRatings)
            {

                if (rating.AdverseType == E_AdverseType.RepossessionOrForeclosure && !rating.IsForeclosureOnly && !rating.IsRepossessionOnly)
                {
                    // 12/8/2005 dd - Only fixup the rating with inconclusive status.

                    if (m_liabilityRepossessionOnly)
                    {
                        rating.IsRepossessionOnly = true;
                    }
                    else if (m_liabilityForeclosureOnly)
                    {
                        rating.IsForeclosureOnly = true;
                    }
                }

                if (rating.AdverseType == E_AdverseType.Bankruptcy)
                {
                    hasBankruptcy = true;
                }

                // 5/3/2006 dd - OPM 4839 For mortgages where derogatory is > 90, automatically flag it as being x120
                if (m_liabilityType == E_DebtRegularT.Mortgage && (rating.AdverseType & E_AdverseType.Late120Plus) != 0)
                {
                    late120Count++;
                }
            }

            //OPM 10299 av It was decided in a meeting with Doug that we would modify the payment string in order to get the date of the BK.   
            if (!hasBankruptcy && m_currentRatingType == MismoCreditLiabilityRating.BankruptcyOrWageEarnerPlan && m_liabilityType == E_DebtRegularT.Mortgage )
            {
                DateTime bkDate = m_lastActivityDate; 
                if (LatestBKRecord != null )
                {
                    switch (LatestBKRecord.Type)
                    {
                        case E_CreditPublicRecordType.BankruptcyChapter13 :
                            bkDate = LatestBKRecord.BkFileDate;
                            break; 
                        case E_CreditPublicRecordType.BankruptcyChapter7 :
                            bkDate = LatestBKRecord.LastEffectiveDate;
                            break;
                    }
                }

                if (m_adverseRatings.Count == 0 || (PaymentPatternStartDate != bkDate && PaymentPatternStartDate < bkDate))
                {
                    CreditAdverseRating cr = this.ConvertMismoAdverseRatingToCreditAdverseRating(new MismoAdverseRating(m_currentRatingType, bkDate));
                    m_adverseRatings.Insert(0, cr);
                    m_paymentPatternStartDate = bkDate;
                }
                else if (PaymentPatternStartDate.Equals(bkDate))
                {
                    CreditAdverseRating mostRecentEntry = m_adverseRatings[0] as CreditAdverseRating;
                    switch (mostRecentEntry.AdverseType)
                    {
                        case E_AdverseType.Late30:
                        case E_AdverseType.Late60:
                        case E_AdverseType.Late90:
                            mostRecentEntry.AdverseType = E_AdverseType.Bankruptcy;
                            break;
                    }
                }
            }
            if (late120Count > 0 && (m_late120 == "" || m_late120 == "0"))
            {
                // 5/3/2006 dd - If there is a late120 from adverse list but it is not report in raw xml then update the count.
                m_late120 = late120Count.ToString();
            }
        }

        /// <summary>
        /// Iterates through the CREDIT_COMMENT nodes and adds them to m_creditComments. 
        /// </summary>
        /// <param name="el"></param>
        private void ParseCreditComments(XmlNodeList els) 
        {
            if (els.Count == 0)
                return;

            foreach (XmlElement el in els)
            {
                m_creditComments.Add(new MismoCreditComment(el));
            }
        }
        /// <summary>
        /// If the payment pattern start date is missing then it skips rating. Universal credit sometimes doesnt provide payment pattern start date. 
        /// Payment pattern strings start from left to right! This builds the n_adverseRatings list. Only IsAdverseRating is added. 
        /// </summary>
        private void ConversePaymentPatternToAdverseRating() 
        {
            // 10/12/2004 dd - Some Credit Bureau does not implement PRIOR_ADVERSE_RATING list then parse adverse information
            // from PaymentPattern data.
            if (null == m_paymentPatternData || "" == m_paymentPatternData)
                return;

            // 4/25/2005 dd - If Payment Pattern Start Date is missing then skip rating.
            // Universal Credit sometime doesn't provide payment pattern start date
            if (DateTime.MinValue == m_paymentPatternStartDate)
                return;

            char[] list = m_paymentPatternData.ToCharArray();

            MismoCreditReport creditReport = (MismoCreditReport) ParentCreditReport;
            MismoCreditRatingCode creditRatingCode = creditReport.CreditRatingCode;

            for (int i = 0; i < list.Length; i++) 
            {

                char ch = list[i];
                CreditAdverseRating rating = null;
                if (creditRatingCode.IsAdverseRating(ch)) 
                {
                    rating = new CreditAdverseRating(creditRatingCode.GetAdverseRating(ch), m_paymentPatternStartDate.AddMonths(-1 * i));
                }

                if (null != rating && !m_adverseRatings.Contains(rating)) 
                {
                    m_adverseRatings.Add(rating);
                }
            }


        }

        private void ParseCreditor(XmlElement el) 
        {
            if (null == el) return;

            m_creditorName    = el.GetAttribute("_Name");
            m_creditorAddress = el.GetAttribute("_StreetAddress");
            m_creditorCity    = el.GetAttribute("_City");
            m_creditorState   = el.GetAttribute("_State");
            m_creditorZipcode = el.GetAttribute("_PostalCode");

            XmlNodeList nodeList = el.SelectNodes("CONTACT_DETAIL/CONTACT_POINT");
            foreach(XmlElement o in nodeList) 
            {
                if (o.GetAttribute("_Type") == "Phone") 
                {
                    m_creditorPhone = o.GetAttribute("_Value");
                    break; // Only get the first phone number.
                }
            }
        }

        /// <summary>
        /// Parses the late count if it located in the doc. It assumes zeroes if for some reason they are missing. 
        /// </summary>
        /// <param name="el"></param>
        private void ParseLateCount(XmlElement el) 
        {
            if (null == el) return;

            // 10/13/2004 dd - For all number of late count, if it is EMPTY, then assume "0". If it has invalid
            // character then leave it alone. Invalid character will throw exception in LateXXCount.
            m_late30  = el.GetAttribute("_30Days");
            if (null == m_late30 || "" == m_late30)
                m_late30 = "0";

            m_late60  = el.GetAttribute("_60Days");
            if (null == m_late60 || "" == m_late60)
                m_late60 = "0";

            m_late90  = el.GetAttribute("_90Days");
            if (null == m_late90 || "" == m_late90)
                m_late90 = "0";


            m_late120 = el.GetAttribute("_120Days"); 
            if (null == m_late120 || "" == m_late120)
                m_late120 = "0";
        }

        /// <summary>
        /// Sets m_currentRating using The MismocreditLiabilityRatingMap. 
        /// </summary>
        /// <param name="el"></param>
        private void ParseCurrentRating(XmlElement el) 
        {
            if (null == el) return;

            m_currentRatingType = MismoCreditLiabilityRatingMap.Parse(el.GetAttribute("_Type"), this);
            //            m_currentRatingCode = el.GetAttribute("_Code");
        }

        private void ParseHighestAdverseRating(XmlElement el) 
        {
            if (null == el) return;

            m_highestAdverseRating = new MismoAdverseRating(el, this);

            if (null != m_highestAdverseRating) 
            {
                CreditAdverseRating rating = ConvertMismoAdverseRatingToCreditAdverseRating(m_highestAdverseRating);

                #region OPM 3690
                if (m_highestAdverseRating.Type == MismoCreditLiabilityRating.Foreclosure)
                    rating.IsForeclosureOnly = true;
                else if (m_highestAdverseRating.Type == MismoCreditLiabilityRating.Repossession)
                    rating.IsRepossessionOnly = true;

                if (m_highestAdverseRating.Type == MismoCreditLiabilityRating.Foreclosure ||
                    m_highestAdverseRating.Type == MismoCreditLiabilityRating.Repossession) 
                {
                    if (m_adverseRatings.Contains(rating)) 
                    {
                        int i = m_adverseRatings.IndexOf(rating);
                        CreditAdverseRating _r = (CreditAdverseRating) m_adverseRatings[i];
                        if (m_highestAdverseRating.Type == MismoCreditLiabilityRating.Foreclosure)
                            _r.IsForeclosureOnly = true;
                        else if (m_highestAdverseRating.Type == MismoCreditLiabilityRating.Repossession)
                            _r.IsRepossessionOnly = true;
                    }
                }
                #endregion
                if (null != rating && rating.Date != DateTime.MinValue && !m_adverseRatings.Contains(rating))
                    m_adverseRatings.Add(rating);
            }

        }
        /// <summary>
        /// WATCH OUT, this could return NULL if MismoAdverseRating does not have negative rating.
        /// </summary>
        /// <param name="mismo"></param>
        /// <returns></returns>
        private CreditAdverseRating ConvertMismoAdverseRatingToCreditAdverseRating(MismoAdverseRating mismo) 
        {
            switch (mismo.Type) 
            {
                case MismoCreditLiabilityRating.Late30Days:
                    return new CreditAdverseRating(E_AdverseType.Late30, mismo.Date);
                case MismoCreditLiabilityRating.Late60Days:
                    return new CreditAdverseRating(E_AdverseType.Late60, mismo.Date);
                case MismoCreditLiabilityRating.Late90Days:
                    return new CreditAdverseRating(E_AdverseType.Late90, mismo.Date);
                case MismoCreditLiabilityRating.LateOver120Days:
                    return new CreditAdverseRating(E_AdverseType.Late120 | E_AdverseType.Late150 | E_AdverseType.Late180, mismo.Date);
                case MismoCreditLiabilityRating.ChargeOff:
                case MismoCreditLiabilityRating.Collection:
                case MismoCreditLiabilityRating.CollectionOrChargeOff:
                    return new CreditAdverseRating(E_AdverseType.ChargeOffOrCollection, mismo.Date);
                case MismoCreditLiabilityRating.Repossession:
                case MismoCreditLiabilityRating.Foreclosure:
                case MismoCreditLiabilityRating.ForeclosureOrRepossession:
                    return new CreditAdverseRating(E_AdverseType.RepossessionOrForeclosure, mismo.Date);
                case MismoCreditLiabilityRating.BankruptcyOrWageEarnerPlan:
                case MismoCreditLiabilityRating.WageEarnerPlan:
                    return new CreditAdverseRating(E_AdverseType.Bankruptcy, mismo.Date);
            }
            return null;
        }
        private void ParseMostRecentAdverseRating(XmlElement el) 
        {
            if (null == el) return;
            m_mostRecentAdverseRating = new MismoAdverseRating(el, this);

            if (null != m_mostRecentAdverseRating) 
            {
                CreditAdverseRating rating = ConvertMismoAdverseRatingToCreditAdverseRating(m_mostRecentAdverseRating);

                #region OPM 3690
                if (m_mostRecentAdverseRating.Type == MismoCreditLiabilityRating.Foreclosure)
                    rating.IsForeclosureOnly = true;
                else if (m_mostRecentAdverseRating.Type == MismoCreditLiabilityRating.Repossession)
                    rating.IsRepossessionOnly = true;

                if (m_mostRecentAdverseRating.Type == MismoCreditLiabilityRating.Foreclosure ||
                    m_mostRecentAdverseRating.Type == MismoCreditLiabilityRating.Repossession) 
                {
                    if (m_adverseRatings.Contains(rating)) 
                    {
                        int i = m_adverseRatings.IndexOf(rating);
                        CreditAdverseRating _r = (CreditAdverseRating) m_adverseRatings[i];
                        if (m_mostRecentAdverseRating.Type == MismoCreditLiabilityRating.Foreclosure)
                            _r.IsForeclosureOnly = true;
                        else if (m_mostRecentAdverseRating.Type == MismoCreditLiabilityRating.Repossession)
                            _r.IsRepossessionOnly = true;
                    }
                }
                #endregion
                if (null != rating && rating.Date != DateTime.MinValue && !m_adverseRatings.Contains(rating))
                    m_adverseRatings.Add(rating);
            }
        }
        private void ParsePriorAdverseRating(XmlNodeList nodeList) 
        {
            if (null == nodeList) return;

            foreach (XmlElement el in nodeList) 
            {
                MismoAdverseRating adverseRating = new MismoAdverseRating(el, this);

                CreditAdverseRating rating = ConvertMismoAdverseRatingToCreditAdverseRating(adverseRating);

                #region OPM 2234
                if (adverseRating.Type == MismoCreditLiabilityRating.Foreclosure)
                    rating.IsForeclosureOnly = true;
                else if (adverseRating.Type == MismoCreditLiabilityRating.Repossession)
                    rating.IsRepossessionOnly = true;

                // 6/28/2005 dd - Case 2234 - Try to separate out ForeclosureOrRepossession
                if (adverseRating.Type == MismoCreditLiabilityRating.Foreclosure ||
                    adverseRating.Type == MismoCreditLiabilityRating.Repossession) 
                {
                    if (m_adverseRatings.Contains(rating)) 
                    {
                        int i = m_adverseRatings.IndexOf(rating);
                        CreditAdverseRating _r = (CreditAdverseRating) m_adverseRatings[i];

                        if (adverseRating.Type == MismoCreditLiabilityRating.Foreclosure)
                            _r.IsForeclosureOnly = true;
                        else if (adverseRating.Type == MismoCreditLiabilityRating.Repossession)
                            _r.IsRepossessionOnly = true;
                    
                    }
                    
                }
                #endregion

                if (null != rating && rating.Date != DateTime.MinValue && !m_adverseRatings.Contains(rating))
                    m_adverseRatings.Add(rating);

            }
        }
        /// <summary>
        /// Parses the payment pattern.  It also parses the payment start date.  Credco sometimes using payment start day other than first
        /// If thats the case its reset to the first. This method then calls ConversePaymentPatternToAdverseRating(). 
        /// </summary>
        /// <param name="el"></param>
        private void ParsePaymentPattern(XmlElement el) 
        {
            if (null == el) return;

            m_paymentPatternData = el.GetAttribute("_Data");
            m_paymentPatternStartDate = MismoUtilities.ParseDateTime(el.GetAttribute("_StartDate"));
            if (m_paymentPatternStartDate.Day != 1) 
            {
                // 1/4/2005 dd - Always reset PaymentPatternStart date to beginning of each month.
                // The reason this code neccessary is because CREDCO some time have PaymentPatternStart date other than the first.
                m_paymentPatternStartDate = new DateTime(m_paymentPatternStartDate.Year, m_paymentPatternStartDate.Month, 1);
            }

            ConversePaymentPatternToAdverseRating();
        }

        /// <summary>
        /// returns the enum value for the given type. The same except for credit line, if its secured and high credit amount >= 75k  
        /// it returns mortgage else it returns revolving. Unknown = other and default  = warning if it is non empty else nothing. 
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        private E_DebtRegularT GetDebtRegularT(string type) 
        {
			E_DebtRegularT ret = E_DebtRegularT.Other;
            switch (type) 
            {
                case "Installment":
                    ret = E_DebtRegularT.Installment;
                    break;
                case "Mortgage":
                    ret = E_DebtRegularT.Mortgage;
                    break;
                case "Revolving":
                    ret = E_DebtRegularT.Revolving;
                    break;
                case "CreditLine":
					// 11/28/2006 nw - External OPM 17891
					if (m_creditLoanType == MismoCreditLoanType.CreditLineSecured && m_highCreditAmount >= 75000)
						ret = E_DebtRegularT.Mortgage;
					else
						ret = E_DebtRegularT.Revolving; // OPM 25205 db - change "CreditLine" to be Revolving by default instead of Open
					break;
                case "Open":
                    ret = E_DebtRegularT.Open;
                    break;
                case "Unknown":
                    ret = E_DebtRegularT.Other;
                    break;
                default:
                    if (type != "")
                        Tools.LogWarning(type + " is not a valid _AccountType for MISMO <CREDIT_LIABILITY>.");
                    break;
				
            }

            if (IsHomeEquityOrLineCredit()) 
            {
                // 6/13/2005 dd - If Liability is Home Equity or Line of Credit set DebtT to Mortgage.
                ret = E_DebtRegularT.Mortgage;
            }
            return ret;
        }

        private bool IsHomeEquityOrLineCredit() 
        {
			if (MismoCreditLoanTypeMap.IsHomeEquityOrLineCredit(m_creditLoanType))
			{
				return true;
			}

			foreach (string sComment in m_creditComments.SelectMany(cc => cc.Text))
			{
				if (sComment.ToLower() == "home equity loan")	// Factual Data use this in their comment.
				{
					return true;
				}
			}

            return false;

        }

        private bool _HasTerminalDerog() 
        {
            return MismoCreditLiabilityRatingMap.IsTerminalDerog(m_currentRatingType);
        }
        private bool _IsRepos()
        {
            return MismoCreditLiabilityRatingMap.IsReposession(m_currentRatingType);             
        }

		private bool _IsLate() 
        {
            return MismoCreditLiabilityRatingMap.IsLate(m_currentRatingType);
        }

        override public bool IsActive
        {
            get 
			{ 
                bool ret = MismoCreditLiabilityAccountStatusTypeMap.IsActive( m_accountStatusType );


                if (m_accountStatusType == MismoCreditLiabilityAccountStatusType.Paid &&
                    m_liabilityType == E_DebtRegularT.Revolving) 
                {
                    // 3/17/2006 dd - OPM 4269 - For Credco if account is PAID and type is REVOLVING then we will consider it to be active.
                    MismoCreditReport mismoCreditReport = ParentCreditReport as MismoCreditReport;
                    if (null != mismoCreditReport) 
                    {
                        if (mismoCreditReport.CreditReportProtocol == CreditReportProtocol.Credco ||
                            mismoCreditReport.RespondingPartyName.IndexOf("First American CREDCO") >= 0 // Need to do this check, because of credco credit through fannie mae interface.
                            )
                            return true;
                    }
                }

				// 5/18/2006 nw - OPM 5014 - If a tradeline has a remark that indicates that the tradeline has been 
				// "TRANSFERRED TO ANOTHER LENDER", we need to make sure IsActive is false
				if (true == ret)
				{
					foreach (string sComment in m_creditComments.SelectMany(cc => cc.Text))
					{
						string sCommentCap = sComment.ToUpper();
						if (sCommentCap.IndexOf("TRANSFERRED TO ANOTHER LENDER") >= 0)
							return false;
						// 4/20/2007 nw - OPM 12585 - More remark text that would indicate a tradeline is inactive/closed
						if (sCommentCap.IndexOf("ACCOUNT CLOSED DUE TO TRANSFER") >= 0)
							return false;
						if (sCommentCap.IndexOf("ACCOUNT CLOSED DUE TO REFINANCE") >= 0)
							return false;
					}
				}

				return ret;
			}
        }


        override public  bool HasBeenDerog
        {
            get
            {
                // 10/11/2004 dd - Check for late count. Some tradeline have late counts but doesn't include in adverse rating list.
                // 6/7/2007 nw - OPM 16239 - Credco credit reports use "00" instead of "0" for late counts
				if ( (m_late30 != "0" && m_late30 != "00") ||
					 (m_late60 != "0" && m_late60 != "00") ||
					 (m_late90 != "0" && m_late90 != "00") ||
					 (m_late120 != "0" && m_late120 != "00") )
                {
                    return true;
                }
                return IsDerogCurrently || m_adverseRatings.Count > 0 || m_isDerogatoryData;
            }
        }

        override public bool IsDerogCurrently
        {
            get
			{
				// 4/12/2007 nw - bad ratings such as terminal derogs aren't considered "derog currently"
				// return MismoCreditLiabilityRatingMap.IsBadRating(m_currentRatingType);
				return MismoCreditLiabilityRatingMap.IsLate(m_currentRatingType);
			}
        }

		// 4/10/2007 nw - External OPM 30150 - TradeCountX:Status should use this instead of IsDerogCurrently
		override public bool IsLastStatusDerog
		{
			get
			{
				if (MismoCreditLiabilityRatingMap.IsBadRating(m_currentRatingType))
					return true;

				if (null == m_paymentPatternData || "" == m_paymentPatternData)
					return false;

				MismoCreditReport creditReport = (MismoCreditReport) ParentCreditReport;
				MismoCreditRatingCode creditRatingCode = creditReport.CreditRatingCode;
				if (creditRatingCode.IsAdverseRating(m_paymentPatternData[0]))
					return true;

				return false;
			}
		}

        private bool _IsInactiveButNeutralStatus() 
        {
            switch (m_currentRatingType) 
            {
                case MismoCreditLiabilityRating.NoDataAvailable:
                case MismoCreditLiabilityRating.TooNew:
                    return true;
                default:
                    return false;
            }
        }

        private bool __IsInactiveButNegativeStatus() 
        {
            switch (m_currentRatingType) 
            {
                case MismoCreditLiabilityRating.BankruptcyOrWageEarnerPlan:
                case MismoCreditLiabilityRating.ChargeOff:
                case MismoCreditLiabilityRating.Collection:
                case MismoCreditLiabilityRating.CollectionOrChargeOff:
                case MismoCreditLiabilityRating.Foreclosure:
                case MismoCreditLiabilityRating.ForeclosureOrRepossession:
                case MismoCreditLiabilityRating.Repossession:
                case MismoCreditLiabilityRating.WageEarnerPlan:
                    return true;
                default:
                    return false;

            }
        }
    }

}
