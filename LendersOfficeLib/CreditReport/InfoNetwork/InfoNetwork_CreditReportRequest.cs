using System;
using System.Xml;
using System.Net;
using System.Security.Cryptography.X509Certificates;

using LendersOffice.CreditReport;
using LendersOffice.CreditReport.Mismo;

namespace LendersOffice.CreditReport.InfoNetwork
{

    public class InfoNetwork_CreditReportRequest : LendersOffice.CreditReport.Mismo2_3.Mismo2_3CreditReportRequest
    {
		// 10/5/2006 nw - OPM 7697 - Update Credit URLs for InfoNetwork
        // private const string BETA_URL = "https://63.108.182.110/servlet/insightmis23";
        // private const string PRODUCTION_URL = "https://www.winmerge.com/servlet/insightmis23";
		private const string BETA_URL = "https://63.108.182.110/Insightls/Insightls";
		private const string PRODUCTION_URL = "https://www.winmerge.com/Insightls/Insightls";
        public InfoNetwork_CreditReportRequest() : base("")
        {

        }

        protected override bool IncludeMailingAddress
        {
            get { return false; }
        }

        public override string Url 
        {
            get 
            {
                if (LoginInfo.AccountIdentifier == "99993-0000" && LoginInfo.UserName == "Te8s7t")
                    return BETA_URL;
                else 
                    return PRODUCTION_URL; 
            }
        }
        public override ICreditReportResponse CreateResponse(XmlDocument doc) 
        {
            return new InfoNetwork_CreditReportResponse(doc);
        }
        public override void SetupAuthentication(System.Net.HttpWebRequest request) 
        {
        }
        public void SetToStatusQuery() 
        {
            this.RequestType = E_CreditReportRequestActionType.StatusQuery;

        }
    }

}
