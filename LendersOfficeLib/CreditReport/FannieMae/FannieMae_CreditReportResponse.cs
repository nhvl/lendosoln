using System;
using System.Xml;

namespace LendersOffice.CreditReport.FannieMae
{

	public class FannieMae_CreditReportResponse : LendersOffice.CreditReport.Mismo2_1.Mismo2_1CreditReportResponse
	{
        private bool m_hasError = false;

        private bool m_isFannieMaeError = false;

        /// <summary>
        /// Construct FannieMae Response with error message
        /// </summary>
        /// <param name="errorMessage"></param>
        public FannieMae_CreditReportResponse(string errorMessage) : base(null)
        {
            m_hasError = true;
            m_isFannieMaeError = true;
            SetErrorMessage(errorMessage);
            SetStatusCode("ERROR");
        }
		public FannieMae_CreditReportResponse(XmlDocument doc) : base(doc) 
		{
            if (!IsValidMismoResponse(doc)) 
            {
                m_hasError = true;
                m_isFannieMaeError = true;
                SetErrorMessage("Not a valid MISMO response. <br>" + GetCustomErrorMessage());
                SetStatusCode("ERROR");

            }

		}

        public override bool HasError 
        {
            get 
            { 
                if (m_isFannieMaeError)
                    return m_hasError; 
                else return base.HasError;

            }
        }

        protected override bool HasErrorInResponse(XmlDocument doc) 
        {
            // For FannieMae, error message must be construct with special constructor.
            return false;
        }
	}
}
