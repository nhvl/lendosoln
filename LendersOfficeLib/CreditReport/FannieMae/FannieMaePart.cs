using System;
using System.Collections;
using System.Text.RegularExpressions;
using LendersOffice.Constants;
using LendersOffice.Common;

namespace LendersOffice.CreditReport.FannieMae
{

    public class FannieMaePart 
    {

        public const string MimeSeparator = "-----" + ConstAppDavid.FannieMae_MimeSeparator + "---";

        private static Regex s_fannieRegex = null;
        private static Regex s_contentRegex = null;


        private FannieMaePart() 
        {
        }

        static FannieMaePart() 
        {
            string token = MimeSeparator.Replace(".", @"\.").Replace("+", @"\+");

            string pattern = token + @"(.*?)(?=-----" + token + ")";
            s_fannieRegex = new Regex(pattern, RegexOptions.Singleline | RegexOptions.Compiled);

            string contentPattern = @"Content-Disposition: form-data; name=""(?<name>[a-zA-Z0-9_-]+)""; filename=""(?<filename>[a-zA-Z0-9_-]+)""\s+(?<content>.+)";
            s_contentRegex = new Regex(contentPattern, RegexOptions.Singleline | RegexOptions.Compiled);

        }

        private string m_name;
        private string m_fileName;
        private string m_content;

        public string Name 
        {
            get { return m_name; }
        }
        public string FileName 
        {
            get { return m_fileName; }
        }

        public string Content 
        {
            get { return m_content; }
        }

        public static ArrayList SplitFannieMaeResponse(string response) 
        {
            ArrayList list = new ArrayList();

            MatchCollection matchCollection = s_fannieRegex.Matches(response);
            for (int i = 0; i < matchCollection.Count; i++) 
            {
                string body = matchCollection[i].Groups[1].Value.TrimWhitespaceAndBOM();

                MatchCollection bodyCollection = s_contentRegex.Matches(body);
                for (int j = 0; j < bodyCollection.Count; j++) 
                {
                    Match m = bodyCollection[j];

                    FannieMaePart part = new FannieMaePart();
                    part.m_fileName = m.Groups["filename"].Value;
                    part.m_content = m.Groups["content"].Value;
                    part.m_name = m.Groups["name"].Value;

                    list.Add(part);


                }
            }

            return list;
        }
    }
}
