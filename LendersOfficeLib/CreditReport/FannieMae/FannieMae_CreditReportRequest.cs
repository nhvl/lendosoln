using System;
using System.Collections;
using System.Xml;
using System.Web;
using System.IO;
using System.Text.RegularExpressions;
using System.Text;

using LendersOffice.CreditReport;
using DataAccess;
using LendersOffice.Constants;

namespace LendersOffice.CreditReport.FannieMae
{

	public class FannieMae_CreditReportRequest : ICreditReportRequest
	{
        private LoginInfo m_loginInfo;
        private BorrowerInfo m_borrower;
        private BorrowerInfo m_coborrower;
        private string m_creditReportIdentifier;
        private bool m_includeExperian;
        private bool m_includeEquifax;
        private bool m_includeTransunion;

        public bool IncludeExperian
        {
            get { return m_includeExperian; }
            set { m_includeExperian = value; }
        }

        public bool IncludeEquifax
        {
            get { return m_includeEquifax; }
            set { m_includeEquifax = value; }
        }

        public bool IncludeTransunion
        {
            get { return m_includeTransunion; }
            set { m_includeTransunion = value; }
        }
        public BorrowerInfo Borrower 
        {
            get { return m_borrower; }
        }

        public BorrowerInfo Coborrower 
        {
            get { return m_coborrower; }
        }

        public LoginInfo LoginInfo 
        {
            get { return m_loginInfo; }
        }

        public string CreditReportIdentifier 
        {
            get { return m_creditReportIdentifier; }
            set { m_creditReportIdentifier = value; }
        }

		public FannieMae_CreditReportRequest()
		{
            m_borrower = new BorrowerInfo();
            m_coborrower = new BorrowerInfo(true);
            m_loginInfo = new LoginInfo();
		}

        public XmlDocument GenerateXml() 
        {
            return null;
        }

        public byte[] GeneratePostContent() 
        {
            StringBuilder sb = new StringBuilder();

            sb.Append("--").Append(ConstAppDavid.FannieMae_MimeSeparator).Append(Environment.NewLine);
            sb.Append(@"Content-Disposition: form-data; name=""RI"";").Append(Environment.NewLine).Append(Environment.NewLine);

            XmlDocument doc = CreateRequestGroup();

            sb.Append(doc.OuterXml);
            sb.Append(Environment.NewLine).Append(Environment.NewLine);

            sb.Append("--").Append(ConstAppDavid.FannieMae_MimeSeparator).Append(Environment.NewLine);
            sb.Append(@"Content-Disposition: form-data; name=""CI"";").Append(Environment.NewLine).Append(Environment.NewLine);
            
            doc = CreateGetCreditReportRequest();
            sb.Append(doc.OuterXml);
            sb.Append(Environment.NewLine).Append(Environment.NewLine);
            sb.Append("--").Append(ConstAppDavid.FannieMae_MimeSeparator).Append("--");

            return System.Text.UTF8Encoding.UTF8.GetBytes(sb.ToString());
        }

        public string Url 
        {
            get 
            { 
                if (m_loginInfo.FannieMaeMORNETUserID == ConstStage.DUTestUserName || m_loginInfo.FannieMaeMORNETUserID == ConstAppDavid.FannieMae_TestDoUserId)
                    return ConstAppDavid.FannieMae_BetaServer;
                else
                    return ConstAppDavid.FannieMae_ProductionServer;
            }

        }
        public ICreditReportResponse CreateResponse(XmlDocument doc) 
        {
            // 6/21/2005 dd - FannieMae response is not a valid XML format, therefore this method should not be invoke.
            // Use CreateResponse(string body) instead
            throw new NotSupportedException();
        }

        public ICreditReportResponse CreateResponse(string body) 
        {
            ArrayList list = FannieMaePart.SplitFannieMaeResponse(body);
            string mismoXml = "";
            string viewableCreditReport = "";
            string routingOutput = "";
            string controlOutput = "";
            string creditReportLog = "";
            foreach (FannieMaePart part in list) 
            {
                // 8/1/2005 dd - Need to replace invalid character 0x0C, with space
                if (part.Name.IndexOf("PRINTFILE") > 0)
                    viewableCreditReport = string.Format("<html><body><pre style='font-family:courier new'>{0}</pre></body></html>", part.Content.Replace((char) 12, ' '));

                if (part.Name.IndexOf("MISMOFILE") > 0)
                    mismoXml = part.Content;

                if (part.Name == "RO")
                    routingOutput = part.Content;

                if (part.Name == "CO")
                    controlOutput = part.Content;
                if (part.Name == "CREDITREPORT_LOG")
                    creditReportLog = part.Content;
            }

            XmlDocument routingOutputDocument = Tools.CreateXmlDoc(routingOutput);

            if (HasRoutingError(routingOutputDocument)) 
            {
                return new FannieMae_CreditReportResponse(GetRoutingErrorMessage(routingOutputDocument));
            } 
            else 
            {
                XmlDocument controlOutputDocument = Tools.CreateXmlDoc(controlOutput);

                if (HasControlOutputError(controlOutputDocument)) 
                {
                    StringBuilder sb = new StringBuilder(500);
                    sb.Append(GetControlOutputErrorMessage(controlOutputDocument).Replace(Environment.NewLine, "<br>"));
                    sb.Append("<br><br>").Append(creditReportLog.Replace(Environment.NewLine, "<br>"));
                    return new FannieMae_CreditReportResponse(sb.ToString());
                } 
                else 
                {
                    XmlDocument doc = MergeFannieMaeCreditReport(mismoXml, viewableCreditReport);

                    return new FannieMae_CreditReportResponse(doc);
                }
            }
        }

        private bool HasControlOutputError(XmlDocument doc) 
        {
            XmlElement el = (XmlElement) doc.SelectSingleNode("//GET_CREDIT_REPORT_RESPONSE/STATUS");

            if (null != el) 
            {
                return (el.GetAttribute("_Condition") != "SUCCESS");
            }

            return false;
        }
        private string GetControlOutputErrorMessage(XmlDocument doc) 
        {
            XmlElement el = (XmlElement) doc.SelectSingleNode("//GET_CREDIT_REPORT_RESPONSE/STATUS");

            if (null != el) 
            {
                return el.GetAttribute("_Description");
            }

            return "";

        }

        private bool HasRoutingError(XmlDocument doc) 
        {
            XmlElement el = (XmlElement) doc.SelectSingleNode("//RESPONSE_GROUP/RESPONSE/STATUS");

            if (null != el) 
            {
                return (el.GetAttribute("_Condition") != "DONE");
            }

            return false;

        }
        private string GetRoutingErrorMessage(XmlDocument doc) 
        {
            XmlElement el = (XmlElement) doc.SelectSingleNode("//RESPONSE_GROUP/RESPONSE/STATUS");

            if (null != el) 
            {
                return el.GetAttribute("_Description");
            }

            return "";

        }

        public static XmlDocument MergeFannieMaeCreditReport(string mismoXml, string viewableCreditReport) 
        {
            XmlDocument doc;
            try
            {
                doc = Tools.CreateXmlDoc(mismoXml);
            }
            catch (XmlException)
            {
                //OPM 126049 work around for du - they are sending invalid xml. 
                //<?xml version="1.0" encoding="UTF-8"?>
                //</_RESIDENCE>
                //<RESPONSE_GROUP MISMOVersionID="2.3.1">
                int indexOfOpeningTag = mismoXml.IndexOf("<RESPONSE_GROUP");
                if (indexOfOpeningTag > -1)
                {
                    doc = Tools.CreateXmlDoc(mismoXml.Substring(indexOfOpeningTag));
                }
                else
                {
                    throw;
                }
            }
            XmlElement embeddedEl = doc.CreateElement("EMBEDDED_FILE");
            embeddedEl.SetAttribute("_Type", "HTML");

            XmlElement docEl = doc.CreateElement("DOCUMENT");
                
            embeddedEl.AppendChild(docEl);

            XmlCDataSection cdata = doc.CreateCDataSection(viewableCreditReport);
            docEl.AppendChild(cdata);

            XmlNode node = doc.SelectSingleNode("//RESPONSE_GROUP/RESPONSE/RESPONSE_DATA/CREDIT_RESPONSE");

            node.AppendChild(embeddedEl);

            // dd - Modify RESPONDING_PARTY _Name so I can distinguise this report if from FANNIE MAE if needed.
            XmlElement respondingPartyElement = (XmlElement) doc.SelectSingleNode("//RESPONSE_GROUP/RESPONDING_PARTY");
            if (null != respondingPartyElement)
                respondingPartyElement.SetAttribute("_Name", "FannieMae - " + respondingPartyElement.GetAttribute("_Name"));

            // 8/1/2005 dd - I believe FannieMae engine use TransUnion rating code.
            XmlElement creditResponseElement = (XmlElement) doc.SelectSingleNode("//RESPONSE_GROUP/RESPONSE/RESPONSE_DATA/CREDIT_RESPONSE");
            if (null != creditResponseElement) 
            {
                // 8/1/2005 dd - Set rating code to TransUnion
                creditResponseElement.SetAttribute("CreditRatingCodeType", "Other");
                creditResponseElement.SetAttribute("CreditRatingCodeTypeOtherDescription", "TransUnion");
            }

            return doc;
        }
        public void SetupAuthentication(System.Net.HttpWebRequest request) 
        {
            request.ContentType = "multipart/form-data; boundary=" + ConstAppDavid.FannieMae_MimeSeparator;
        }

        private XmlDocument CreateGetCreditReportRequest() 
        {
            XmlDocument doc = new XmlDocument();
            XmlElement getCreditReportRequestEl = doc.CreateElement("GET_CREDIT_REPORT_REQUEST");
            doc.AppendChild(getCreditReportRequestEl);

            CreateServiceProviderElement(getCreditReportRequestEl);
            CreateBorrowerInformation(getCreditReportRequestEl, true);
            if (m_coborrower.IsValid) 
            {
                CreateBorrowerInformation(getCreditReportRequestEl, false);
            }
            CreateCreditReportType(getCreditReportRequestEl);

            CreateSoftwareProvider(getCreditReportRequestEl);
            CreateReturnFile(getCreditReportRequestEl);
            return doc;

        }
        private void CreateServiceProviderElement(XmlElement parentEl) 
        {
            XmlElement el = parentEl.OwnerDocument.CreateElement("SERVICE_PROVIDER");
            parentEl.AppendChild(el);

            if (m_loginInfo.UserName == "test" && m_loginInfo.Password == "test") 
            {
                // Test account settings
                el.SetAttribute("_Name", "200");
                el.SetAttribute("_AccountNumber", "1234567");
                el.SetAttribute("_Password", "fnma");

            } 
            else 
            {
                el.SetAttribute("_Name", m_loginInfo.FannieMaeCreditProvider);
                el.SetAttribute("_AccountNumber", m_loginInfo.UserName);
                el.SetAttribute("_Password", m_loginInfo.Password);
            }
        }

        private void CreateBorrowerInformation(XmlElement parentEl, bool isBorrower) 
        {
            BorrowerInfo currentInfo = isBorrower ? m_borrower : m_coborrower;
            BorrowerInfo spouseInfo = isBorrower ? m_coborrower : m_borrower;

            if (null == currentInfo)
                return; // Unable to create BORROWER_INFORMATION if currentInfo is null.

            XmlElement el = parentEl.OwnerDocument.CreateElement("BORROWER_INFORMATION");
            parentEl.AppendChild(el);

            el.SetAttribute("BorrowerSSN", currentInfo.Ssn.Replace("-", ""));
            el.SetAttribute("BorrowerFirstName", currentInfo.FirstName);
            el.SetAttribute("BorrowerMiddleName", currentInfo.MiddleName);
            el.SetAttribute("BorrowerLastName", currentInfo.LastName);
            el.SetAttribute("BorrowerNameSuffix", currentInfo.Suffix);
            el.SetAttribute("BorrowerTypeCode", isBorrower ? "0" : "1");

            if (null != m_borrower) 
            {
                el.SetAttribute("BorrowerStreetAddress", m_borrower.CurrentAddress.StreetAddress);
                el.SetAttribute("BorrowerCity", m_borrower.CurrentAddress.City);
                el.SetAttribute("BorrowerState", m_borrower.CurrentAddress.State);
                el.SetAttribute("BorrowerPostalCode", m_borrower.CurrentAddress.Zipcode);
            }
            if (null != spouseInfo) 
            {
                if (spouseInfo.IsValid) 
                {
                    el.SetAttribute("CoBorrowerSSN", spouseInfo.Ssn.Replace("-", ""));
                }
            }
            el.SetAttribute("CreditReportIdentifier", m_creditReportIdentifier);

        }
        private void CreateCreditReportType(XmlElement parentEl) 
        {
            XmlElement el = parentEl.OwnerDocument.CreateElement("CREDIT_REPORT_TYPE");
            parentEl.AppendChild(el);

            // Here is the merge code.
            // 01 - EQ, EX, TU data merge.
            // 02 - EQ only
            // 03 - EX only
            // 04 - TU only
            // 05 - EQ + EX
            // 06 - EQ + TU
            // 07 - EX + TU

            string mergeType = "01"; // Equifax, Experian & TransUnion tri-merge
            if (m_includeEquifax && !m_includeExperian && !m_includeTransunion) 
            {
                // 12/1/2005 dd - EQ only
                mergeType = "02";
            } 
            else if (m_includeExperian && !m_includeEquifax && !m_includeTransunion) 
            {
                // 12/1/2005 dd - EX only
                mergeType = "03";
            }
            else if (m_includeTransunion && !m_includeEquifax && !m_includeExperian) 
            {
                // 12/1/2005 dd - TU only
                mergeType = "04";
            } 
            else if (m_includeEquifax && m_includeExperian && !m_includeTransunion) 
            {
                // 12/1/2005 dd - EQ + EX
                mergeType = "05";
            }
            else if (m_includeEquifax && m_includeTransunion && !m_includeExperian) 
            {
                // 12/1/2005 dd - EQ + TU
                mergeType = "06";
            } 
            else if (m_includeExperian && m_includeTransunion && !m_includeEquifax) 
            {
                // 12/1/2005 dd - EX + TU
                mergeType = "07";
            }

            el.SetAttribute("MergeType", mergeType);
            
        }
        private void CreateSoftwareProvider(XmlElement parentEl) 
        {
            XmlElement el = parentEl.OwnerDocument.CreateElement("SOFTWARE_PROVIDER");
            parentEl.AppendChild(el);

            el.SetAttribute("_Code", ConstAppDavid.FannieMae_SoftwareProviderCode);

        }

        private void CreateReturnFile(XmlElement parentEl) 
        {
            XmlElement el = parentEl.OwnerDocument.CreateElement("_RETURN_FILE");
            parentEl.AppendChild(el);

            el.SetAttribute("_FormatType", "6"); // Print Ready & MISMO XML 2.1
        }

        private XmlDocument CreateRequestGroup() 
        {
            XmlDocument doc = new XmlDocument();
            
            XmlElement requestGroupEl = doc.CreateElement("REQUEST_GROUP");
            doc.AppendChild(requestGroupEl);

            requestGroupEl.SetAttribute("MISMOVersionID", "2.1");

            CreateRequest(requestGroupEl);

            return doc;
        }

        private void CreateRequest(XmlElement parentEl) 
        {
            XmlElement el = parentEl.OwnerDocument.CreateElement("REQUEST");
            parentEl.AppendChild(el);

            el.SetAttribute("RequestDateTime", DateTime.Now.ToString());
            el.SetAttribute("LoginAccountIdentifier", m_loginInfo.FannieMaeMORNETUserID);
            el.SetAttribute("LoginAccountPassword", m_loginInfo.FannieMaeMORNETPassword);

            CreateRequestData(el);
        }
        private void CreateRequestData(XmlElement parentEl) 
        {
            XmlElement requestDataEl = parentEl.OwnerDocument.CreateElement("REQUEST_DATA");
            parentEl.AppendChild(requestDataEl);

            XmlElement fnmProductEl = parentEl.OwnerDocument.CreateElement("FNM_PRODUCT");
            requestDataEl.AppendChild(fnmProductEl);

            fnmProductEl.SetAttribute("_Name", "Credit");
            fnmProductEl.SetAttribute("_FunctionName", "GetCreditReport");
            fnmProductEl.SetAttribute("_VersionNumber", "2.0");

            XmlElement connectionEl = parentEl.OwnerDocument.CreateElement("CONNECTION");
            fnmProductEl.AppendChild(connectionEl);

            connectionEl.SetAttribute("_ModeIdentifier", "Synchronous");


        }
	}
}
