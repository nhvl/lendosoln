﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.IO;
using System.Xml;
using DataAccess;
using System.Text.RegularExpressions;
using LendersOffice.Common;

namespace LendersOffice.Reports
{
    [Serializable]
    [XmlRoot("Pipeline")]
    public class CPipelineView
    {
        [XmlAttributeAttribute(AttributeName = "ActiveTabIndex")]
        public int Index;

        [XmlAttributeAttribute(AttributeName = "lastRId")]
        public Guid LastCustomReportId;

        [XmlIgnore]
        private bool m_isDirty = false;

        [XmlIgnore]
        public bool IsDirty
        {
            get
            {
                return m_isDirty;
            }
            set
            {
                m_isDirty = value;
            }
        }

        [XmlArray("Tabs")]
        [XmlArrayItem("Tab")]
        public List<CTab> Tabs;

        // Previously closed tabs. Limit: 8
        [XmlArray("PrevTabs")]
        [XmlArrayItem("PrevTab")]
        public List<CTab> recentlyClosedTabs;

        public bool Add(CTab tab, bool setActive)
        {
            if (tab == null)
                return false;
            try
            {
                Tabs.Add(tab);
                if (setActive)
                {
                    Index = Tabs.Count - 1;
                }
                m_isDirty = true;
            }
            catch
            {
                return false;
            }

            return true;
        }

        public void MoveTo(int currPos, int newPos)
        {
            if (Tabs == null)
                return;
            if (currPos >= Tabs.Count || newPos >= Tabs.Count ||
               currPos < 0 || newPos < 0)
                return;


            CTab currTab = Tabs[currPos];
            
            Tabs.RemoveAt(currPos);
            if (newPos == Tabs.Count)
            {
                Tabs.Add(currTab);
            }
            else
            {
                Tabs.Insert(newPos, currTab);
            }

            // Current tab moved
            if (currPos == Index)
            {
                Index = newPos;
            }
            // Tab moved from left to right of Active. Need to mimic what UI does.
            else if (currPos < Index && newPos >= Index)
            {
                Index--;
            }
            // Tab moved from right to left of Active. Need to mimic what UI does.
            else if (currPos > Index && newPos <= Index)
            {
                Index++;
            }
        }

        public CTab RemoveAt(int index)
        {
            CTab tab = null;
            try
            {
                tab = Tabs[index];
                Tabs.RemoveAt(index);
                if (index == this.Index)
                {
                    SetNextTabAsActive(tab.TabType);
                }
                else if (index < this.Index && this.Index != 0)
                {
                    Index--;
                }

                if (tab.TabType == E_TabTypeT.PipelinePortlet)
                {
                    AddToRecentlyClosedTabsList(tab);
                }
                m_isDirty = true;
            }
            catch
            {
                return null;
            }
            return tab;
        }

        public int Clear()
        {
            int count = Tabs.Count;
            
            m_isDirty = true;
            Tabs.Clear();
            recentlyClosedTabs.Clear();
            Index = 0;
            return count;
        }

        [XmlIgnore]
        public CTab Active
        {
            get
            {
                if (Tabs.Count == 0)
                    return null;
                return Tabs[Index];
            }
            set
            {
                if (Tabs.Contains(value))
                {
                    m_isDirty = true;
                    Index = Tabs.IndexOf(value);
                }
            }
        }

        [XmlIgnore]
        public bool isEmpty
        {
            get
            {
                return (Tabs == null || Tabs.Count == 0);
            }
            
        }

        [XmlIgnore]
        public int Count
        {
            get
            {
                return (Tabs == null) ?  0 : Tabs.Count;
            }
        }

        public CPipelineView()
        {
            Tabs = new List<CTab>();
            recentlyClosedTabs = new List<CTab>();
            m_isDirty = false;
            Index = 0;
        }

        public CTab GetById(Guid id)
        {
            return this.Tabs.FirstOrDefault((tab) => tab.UniqueId == id);
        }

        public string ToXml()
        {
            return CPipelineView.SerializeToXml(this);
        }

        public static string SerializeToXml(CPipelineView pipe)
        {
            foreach (var tab in pipe.Tabs)
            {
                tab.PrepareForSerialization();
            }

            foreach (var prevTab in pipe.recentlyClosedTabs)
            {
                prevTab.PrepareForSerialization();
            }

            string xml = "";
            MemoryStream ms = new MemoryStream();

            XmlSerializerNamespaces customNamespace = new XmlSerializerNamespaces();
            customNamespace.Add("", "");

            XmlWriterSettings xwSettings = new XmlWriterSettings();
            xwSettings.OmitXmlDeclaration = true;
            xwSettings.Indent = true;
            XmlWriter wr = XmlWriter.Create(ms, xwSettings);

            XmlSerializer SerializerObj = new XmlSerializer(typeof(CPipelineView));
            SerializerObj.Serialize(wr, pipe, customNamespace);
            wr.Flush();

            ms.Position = 0;
            StreamReader sr = new StreamReader(ms);
            xml = RemoveWhitespace(sr.ReadToEnd());

            wr.Close();
            sr.Close();
            ms.Close();

            return xml;
        }

        private static T DeserializeXml<T>(string strObject)
        {
            XmlSerializer mySerializer = null;
            XmlTextReader reader = null;

            try
            {
                mySerializer = new XmlSerializer(typeof(T));
                reader = new XmlTextReader(strObject, XmlNodeType.Document, null);

                T result = (T)mySerializer.Deserialize(reader);
                reader.Close();
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (reader != null) { reader.Close(); }
            }
        }

        public static CPipelineView DeserializeFromXml(string xml)
        {
            return CPipelineView.DeserializeXml<CPipelineView>(xml);
        }

        public static string RemoveWhitespace(string xml)
        {
            Regex regex = new Regex(@">\s*<");
            xml = regex.Replace(xml, "><");
            return xml.TrimWhitespaceAndBOM();
        }

        private void SetNextTabAsActive(E_TabTypeT tabType)
        {
            int index = -1;
            for (int i = 0; i < Tabs.Count; i++)
            {
                if (Tabs[i].TabType == tabType)
                {
                    index = i;
                    break;
                }
            }
            if (index == -1) // No tab of same type remaining
            {
                if (Index != 0)
                {
                    index = Index - 1;
                }
                else
                {
                    index = 0;
                }
            }
            Index = index;
        }

        private void AddToRecentlyClosedTabsList(CTab tab)
        {
            if(recentlyClosedTabs.Contains(tab))
            {
                return;
            }
            
            if(recentlyClosedTabs.Count >= 8) // Limit of 8
            {
                recentlyClosedTabs.RemoveAt(0);
            }
            recentlyClosedTabs.Add(tab);
        }
    }

    // Will probably want to make a few Tab classes that inherit from this 
    // if/when other tabs get supported. Otherwise unneeded attributes will
    // be serialized and stored.

    [Serializable]
    [XmlRoot("Query")]
    public class CTab
    {
        [XmlAttributeAttribute(AttributeName = "uid")]
        public Guid UniqueId
        {
            get;
            set;
        }

        [XmlAttributeAttribute(AttributeName = "id")]
        public Guid ReportId;

        [XmlAttributeAttribute(AttributeName = "repTitle")]
        public string ReportTitle
        {
            get;
            set;
        }

        [XmlAttributeAttribute(AttributeName = "scope")]
        public E_ReportExtentScopeT Scope;

        [XmlAttributeAttribute(AttributeName = "tabType")]
        public E_TabTypeT TabType;

        [XmlAttributeAttribute(AttributeName = "tabName")]
        public string TabName
        {
            get;
            set;
        }

        [XmlAttributeAttribute(AttributeName = "sort")]
        public string SortOrder;

        public CTab(E_TabTypeT type, string name, Guid repId, string repTitle, E_ReportExtentScopeT scope)
        {
            ReportId = repId;
            ReportTitle = repTitle;
            TabType = type;
            TabName = name;
            Scope = scope;
        }

        public CTab(E_TabTypeT type, string name)
        {
            if (type == E_TabTypeT.PipelinePortlet)
            {
                this.ReportId = Guid.Empty;
                this.Scope = E_ReportExtentScopeT.Assign;
                ReportTitle = "Simple pipeline <b>(default)</b>";
            }
            TabType = type;
            TabName = name;
        }

        public CTab()
        {
            TabType = E_TabTypeT.PipelinePortlet;
            TabName = "New Tab";
        }

        public void PrepareForSerialization()
        {
            if (this.UniqueId == Guid.Empty)
            {
                this.UniqueId = Guid.NewGuid();
            }

            if (this.TabType != E_TabTypeT.DisclosurePortlet &&
                this.TabType != E_TabTypeT.PipelinePortlet)
            {
                this.TabName = GetTabName(this.TabType);
            }
        }

        public static string GetTabName(E_TabTypeT tabType)
        {
            switch (tabType)
            {
                case E_TabTypeT.PipelinePortlet:
                    return "Your Pipeline";
                case E_TabTypeT.DisclosurePortlet:
                    return "Disclosures"; // Should never use this
                case E_TabTypeT.LoanPoolPortlet:
                    return "Loan Pools";
                case E_TabTypeT.TradeMasterPortlet:
                    return "Trades";
                case E_TabTypeT.MyTasksPortlet:
                    return "Tasks";
                case E_TabTypeT.RemindersPortlet:
                    return "Your Tasks";
                case E_TabTypeT.DisctrackPortlet:
                    return "Tracked Tasks";
                case E_TabTypeT.MyLoanRemindersPortlet:
                    return "Tasks in your Loans";
                case E_TabTypeT.UnassignedLoanPortlet:
                    return "New Loans";
                case E_TabTypeT.MyLeadPortlet:
                    return "Your Leads";
                case E_TabTypeT.UnassignLeadPortlet:
                    return "New Leads";
                case E_TabTypeT.TestPortlet:
                    return "Test Files";
                default:
                    throw new UnhandledEnumException(tabType);
            }
        }

        public override string ToString()
        {
            this.PrepareForSerialization();
            return ObsoleteSerializationHelper.JavascriptJsonSerialize(this);
        } 
    }

}
