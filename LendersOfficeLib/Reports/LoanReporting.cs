namespace LendersOffice.Reports
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Text;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Migration;
    using LendersOffice.ObjLib.TPO;
    using LendersOffice.QueryProcessor;
    using LendersOffice.Security;
    using LendersOffice.ObjLib.TRID2;
    using ObjLib.Extensions;

    /// <summary>
    /// We now use this module to handle all loan reporting queries.
    /// The specifics of schema and access to database is provided
    /// by this interface.  Report results are described as a set
    /// of row tables.
    /// 
    /// ADDING NEW CUSTOM REPORT FIELDS:
    /// 1. Add the new field here: eB += m_Schema.Add(...)
    /// 2. Add the field to LOAN_FILE_CACHE if it's not there already
    /// </summary>
    /// <remarks>
    /// We use this helper for loan custom reporting.  The custom
    /// reporting framework is largely reusable, but there is a
    /// significant piece, namely the schema, that contains all the
    /// field definitions.  We assume that 2 uses of the custom
    /// reporting framework only differ in what reportable fields
    /// they support and where they're located in the database.
    /// 
    /// I) Schema
    /// 
    /// Reportable fields are abstract descriptions are individual
    /// data items.  You can liken them to properties of a data
    /// object or cells in a data row.
    /// 
    /// A field has type and class.  Both of these attributes govern
    /// representation of the underlying data.  Examples of type
    /// are boolean, integer, string, etc.  The class attribute is
    /// a little more geared towards ui representation, e.g. text,
    /// percent, count, cash/money, calendar, etc.  Both attributes
    /// combine to identify how the reporting framework should handle
    /// data items with those characteristics.  As well, the fields
    /// are given friendly names so that we can list them by section
    /// in a selection-friendly manner.  To further assist rendering,
    /// we define reportable fields with a format string that dictates
    /// how the ui representation should show a field value.  A
    /// mapping list is maintained to allow the quick transformation
    /// of raw data to a user-friendly representation.  Finally,
    /// we allow fields to specify their selection string.  That is,
    /// we use this specification to generate a selection statement
    /// for reading the data object's field value from the database.
    /// 
    /// In this file, below, listed within grouped regions, you will
    /// find a field initialization list.  These are the reportable
    /// fields for our application.  We could have loaded these details
    /// from xml, or some other external representation, but currently,
    /// initialization occurs on first use of loan reporting during
    /// static construction.  The best way to add a field is to look
    /// for an example in the existing list with the same data type
    /// and copy its structure and format.  Most fields of the same
    /// data type are loaded identically.
    /// 
    /// As fields are added, they are indexed within the schema
    /// into lookup tables.  Each field is looked up by its identifier.
    /// For example, the schema is available during report editing as a generic
    /// interface for retrieving field specs for display in a field
    /// chooser, or when comparing data types of condition parameters.
    /// 
    /// Not all fields directly map to database fields.  Some are
    /// derived.
    /// 
    /// Derived fields are sometimes called calculated fields.  They
    /// are reportable fields, and have all the common characteristics
    /// as such, but are defined not as single column of a database
    /// table, rather the arithmetic combination of any number of
    /// columns in the database.  With the following selection string,
    /// the field "sStatusDays" is a great example:
    /// 
    /// datediff( dd, cast( sStatusD as datetime ), getdate() )
    /// 
    /// Notice that the derived field depends on the status-date of
    /// the loan and the current date.  We take the day date difference of the
    /// two.  This gives us the count of the number of days the loan
    /// has been in the current status, ignoring time.
    /// 
    /// Some calculated fields are calculated by the data-access-layer
    /// of LendingQB, but stored in our cache table as resolved
    /// value.  From our reporting perspective, we don't consider them
    /// to be derived -- based on other fields' current values.
    /// 
    /// II) Loading data for reports
    /// 
    /// A query (go see the class for a deeper explanation) contains
    /// a list of columns.  These columns reference reportable fields.
    /// To extract the loan data from the database, we must first
    /// gather all the fields we want to display.  The query's columns
    /// tell us what fields we need to query.
    /// 
    /// For simplicity, we put all the reportable fields' content in
    /// a single table, namely, the so called cache table.  To be
    /// more accurate, the cache table is a resource that exists to
    /// speed up display of loan information all over the application.
    /// Loan custom reporting is merely a consumer of this resource.
    /// So, in order to extract loan information in custom reporting,
    /// the columns must be contained within this cache table resource.
    /// 
    /// To extract the loan file content, the query object is translated
    /// to a sql select statement that is then modified to include only
    /// the loan files the executing user can see.  The resulting data
    /// set is then converted into a report and returned to the caller.
    /// However, field information alone is not enough to map the query
    /// onto our cache table.
    /// 
    /// To load the data, we invoke a loan reporting method to get the
    /// loan file extent from the database.  This method is responsible
    /// for generating and executing the appropriate sql statement and
    /// returning the data set for report result initialization.  The
    /// field specification are used to make sure each query column is
    /// properly represented in the sql.  Once retrieved, access control
    /// kicks in to filter out loans the user shouldn't see.
    /// 
    /// III) Adding new reportable fields
    /// 
    /// As mentioned in the schema section, fields are added during
    /// schema initialization.  This instance is kept in memory as a
    /// static variable.  It initialized on the first use of loan
    /// reporting.  It is possible to wire the schema to accept new
    /// fields on the fly, but that has not been set up.  Fields are
    /// loaded when the first use of loan reporting occurs in the
    /// application and the application must be rebuilt and reset
    /// to effect a new reportable field set.
    /// 
    /// The best way to add a new field is to copy a similar one.
    /// For explanation's sake, we will dissect the current method:
    /// 
    /// eB += m_Schema.Add
    /// ( "Loan Information"
    /// , "Loan Name"
    /// , "sLNm"
    /// , Field.E_FieldType.Str , Field.E_ClassType.Txt
    /// );
    /// 
    /// This example is simple enough.  The section/group label is
    /// followed by the field's friendly name.  The third parameter
    /// is the field's identifier, and, when not accompanied by a
    /// selection string spec, doubles as the database column name.
    /// The final 2 parameters denote the type and class for the
    /// field, which govern representation handling.
    /// 
    /// Another example shows how we deal with enumeration types:
    /// 
    /// eB += m_Schema.Add
    /// ( "Loan Information"
    /// , "Loan Type"
    /// , "sLT"
    /// , Field.E_FieldType.Enu , Field.E_ClassType.Txt
    /// , new EnumRepMap( E_sLT.Conventional , "Conventional" )
    /// , new EnumRepMap( E_sLT.FHA          , "FHA"          )
    /// , new EnumRepMap( E_sLT.Other        , "Other"        )
    /// , new EnumRepMap( E_sLT.UsdaRural    , "USDA Rural"   )
    /// , new EnumRepMap( E_sLT.VA           , "VA"           )
    /// );
    /// 
    /// The extra information here pertains to mapping the database-
    /// bound value to a user friendly string-based value.  Under
    /// the hood, this mapping specification will cause the schema
    /// and extent fetching implementation to generate sql that
    /// selects the mentioned field (in this case, "sLT") and map
    /// it to one of the 5 listed strings using case-when construct.
    /// 
    /// The best way to test a new field in localhost is to add
    /// it to the initialization list, pop open the log viewer
    /// and create and run a report that includes the new field.
    /// If everything goes smooth, try running the report again
    /// with aggregate calculations and sorting on the new field,
    /// if applicable.  That's the best you can do to test.
    /// 
    /// IV) Debugging
    /// 
    /// If the custom reporting engine fails because of a particular
    /// field, it's probably because you got the name of the database
    /// column wrong.  However, the second most common error is when
    /// the specified field type doesn't match the underlying database
    /// type.  In general, you should look for the logged select
    /// statement when attempting to debug.  If the engine generates
    /// an sql statement, then you will find the something like the
    /// following as an "info" entry in the log:
    /// 
    /// Loan reporting: Query = select sLId as Id , sStatusT as Status ,
    ///  sLNm as sLNm , aBLastNm as aBLastNm , aBFirstNm as aBFirstNm ,
    ///  sEmployeeLoanRepName as sEmployeeLoanRepName ,
    ///  case( sStatusT )
    ///  when '15' then 'Lead Canceled'
    ///  when '16' then 'Lead Declined'
    ///  when '12' then 'Lead Open'
    ///  when '17' then 'Lead Other'
    ///  when '4'  then 'Loan Approved'
    ///  when '9' then 'Loan Canceled'
    ///  when '11' then 'Loan Closed'
    ///  when '5' then 'Loan Docs'
    ///  when '6' then 'Loan Funded'
    ///  when '7' then 'Loan On hold'
    ///  when '0' then 'Loan Open'
    ///  when '18' then 'Loan Other'
    ///  when '2' then 'Loan Pre-approve'
    ///  when '1' then 'Loan Pre-qual'
    ///  when '19' then 'Loan Recorded'
    ///  when '10' then 'Loan Denied'
    ///  when '3' then 'Loan Submitted'
    ///  when '8' then 'Loan Suspended'
    ///  when '13' then 'Loan Underwriting' end as sStatusT ,
    ///  sStatusD as sStatusD ,
    ///  sEstCloseD as sEstCloseD ,
    ///  sBrokerId as cBrokerId ,
    ///  sBranchId as cBranchId ,
    ///  ...
    ///  ...
    ///  ...
    ///  order by  aBLastNm asc , aBFirstNm asc
    /// 
    /// The sql can be much to look at, but you should be able to find
    /// errors within the statement.  If you can't find the sql, then
    /// the engine must be failing before it queries the database.
    /// </remarks>

    public class LoanReporting
    {

        private const string Category_LoanInfo = "Loan Information";
        private const string Category_Commissions = "Commissions";
        private const string Category_Fees = "Fees";
        private const string Category_GfeFees = "GFE Fees";
        private const string Category_LoanStatus = "Loan Status";
        private const string Category_SubjProperty = "Subject Property Details";
        private const string Category_BorrowerDetails = "Borrower Details";
        private const string Category_PML = "PML Only";
        private const string Category_CreditInformation = "Credit Information";
        private const string Category_AssignedEmployees = "Assigned Employees";
        private const string Category_OfficialAgents = "Official Contact Agents";
        private const string Category_HmdaAuditing = "HMDA Auditing";
        private const string Category_NMLSExpandedCallReport = "NMLS Expanded Call Report";
		private const string Category_CustomFields = "Custom Fields";

        private const string Category_InvestorRateLock = "Investor Rate Lock"; //
        private const string Category_Funding = "Funding";  //
        private const string Category_Servicing = "Servicing";  //
        private const string Category_Transactions = "Transactions";  //
        private const string Category_PurchaseAdvice = "Purchase Advice";  //
        private const string Category_Disbursement = "Disbursement";  //
        private const string Category_Settlement = "Settlement Charges (SC)";  //
        private const string Category_Accounting = "Accounting";  //
        private const string Category_Tasks = "Tasks";
        private const string Category_Shipping = "Shipping";
        private const string Category_TRIDDisclosures = "TRID Disclosures";
        private const string Category_InitialEscrowAccount = "Initial Escrow Account";

		/// <summary>
        /// Keep track of the reporting field schema on first use.
        /// Watch out for implementing auto-update of schema if
        /// the fields change.  We serve up the schema to the editor
        /// and others, so we need to lock access until they're done.
        /// We could get by with serving up the schema instances on
        /// demand and lock access to the reference, so that all
        /// updates just set a new referenced schema.
        /// </summary>

        private static LoanReportingCatch m_Catch = new LoanReportingCatch();
        private static PairSet    m_ControlFields = new PairSet();
        private static Schema            m_Schema = new Schema();

        /// <summary>
        /// This will be used to limit the visible rows.
        /// </summary>
        private int m_maxViewableRowCount = int.MaxValue;
        private int m_maxSqlRowCount = int.MaxValue;

        /// <summary>
        /// Limit number of rows that will be visible to the user (after workflow restrictions). Use int.MaxValue if you don't want the row count restriction.
        /// </summary>
        public int MaxViewableRowCount 
        {
            get { return m_maxViewableRowCount; }
            set { m_maxViewableRowCount = value; }
        }

        /// <summary>
        /// Limit number of row return by SQL server. Use int.MaxValue if you don't want the row count restriction.
        /// </summary>
        public int MaxSqlRowCount
        {
            get { return m_maxSqlRowCount; }
            set { m_maxSqlRowCount = value; }
        }

        private static readonly LqbSingleThreadInitializeLazy<Guid> x_defaultSimplePipelineReportId;
        private static readonly LqbSingleThreadInitializeLazy<Guid> x_defaultSimplePipelineNonPMLReportId;

        public static Guid DefaultSimplePipelineReportId
        {
            get { return x_defaultSimplePipelineReportId.Value; }
        }
        public static Guid DefaultSimplePipelineNonPMLReportId
        {
            get { return x_defaultSimplePipelineNonPMLReportId.Value; }
        }
        private BrokerUserPrincipal BrokerUser
        {
            get { return BrokerUserPrincipal.CurrentPrincipal; }
        }

        /// <summary>
        /// Handles loan reporting queries.
        /// </summary>
        static LoanReporting()
        {
            // Load a new schema instance using our field definition set.

            x_defaultSimplePipelineReportId = new LqbSingleThreadInitializeLazy<Guid>(
                GetDefaultPmlPipelineReportId);

            x_defaultSimplePipelineNonPMLReportId = new LqbSingleThreadInitializeLazy<Guid>(
                GetDefaultNonPmlPipelineReportId);

            try
            {
                // We load the fields in code, but could load them from
                // a local xml file that is updateable.

                ExceptionBucket eB = new ExceptionBucket();

                #region ( Loan Information Fields )

                eB += m_Schema.Add(Category_LoanInfo, "Loan Number", "sLNm", Field.E_FieldType.Str , Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_LoanInfo, "Loan Reference Number", "sLRefNm", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_LoanInfo, "Agency Case Number", "sAgencyCaseNum", Field.E_FieldType.Str, Field.E_ClassType.Txt); //opm 34626 fs 08/27/09
                eB += m_Schema.Add(Category_LoanInfo, "Lender Case Number", "sLenderCaseNum", Field.E_FieldType.Str , Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_LoanInfo, "MERS #", "sMersMin", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_LoanInfo, "MERS Originating Org ID", "sMersOriginatingOrgId", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_LoanInfo, "DU Case ID", "sDuCaseId", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_LoanInfo, "LP AUS Key", "sLpAusKey", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_LoanInfo, "Loan Type", "sLT", Field.E_FieldType.Enu , Field.E_ClassType.Txt
                    , new EnumRepMap( E_sLT.Conventional , "Conventional" )
                    , new EnumRepMap( E_sLT.FHA          , "FHA"          )
                    , new EnumRepMap( E_sLT.Other        , "Other"        )
                    , new EnumRepMap( E_sLT.UsdaRural    , "USDA Rural"   )
                    , new EnumRepMap( E_sLT.VA           , "VA"           )
                    );

                eB += m_Schema.Add(Category_LoanInfo, "Loan Purpose", "sLPurposeT", Field.E_FieldType.Enu , Field.E_ClassType.Txt
                    , new EnumRepMap( E_sLPurposeT.Construct     , "Construction"           )
                    , new EnumRepMap( E_sLPurposeT.ConstructPerm , "Construction Permanent" )
                    , new EnumRepMap( E_sLPurposeT.Other         , "Other"                  )
                    , new EnumRepMap( E_sLPurposeT.Purchase      , "Purchase"               )
                    , new EnumRepMap( E_sLPurposeT.Refin         , "Refinance"              )
                    , new EnumRepMap( E_sLPurposeT.RefinCashout  , "Refinance Cash-out"     )
                    , new EnumRepMap( E_sLPurposeT.FhaStreamlinedRefinance, "FHA Streamlined Refinance")
                    , new EnumRepMap( E_sLPurposeT.VaIrrrl, "VA IRRRL")
                    , new EnumRepMap( E_sLPurposeT.HomeEquity, "Home Equity")
                    );
                eB += m_Schema.Add(Category_LoanInfo, "Is Cashout", "sIsCashout", "case (sLPurposeT) when '2' then 'Yes' else 'No' end", Field.E_FieldType.Str , Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_LoanInfo, "Loan Amount", "sLAmtCalc", Field.E_FieldType.Dec , Field.E_ClassType.Csh, "C");
				eB += m_Schema.Add(Category_LoanInfo, "Total Loan Amount", "sFinalLAmt", Field.E_FieldType.Dec , Field.E_ClassType.Csh, "C"); //opm 11894 fs 08/01/08                
                eB += m_Schema.Add(Category_LoanInfo, "Other Lien Balance", "sProOFinBal", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_LoanInfo, "Loan Number of Linked Loan", "sLinkedLoanName", Field.E_FieldType.Str, Field.E_ClassType.Txt); //opm 189450
                eB += m_Schema.Add(Category_LoanInfo, "Credit Report Order Date", "sCrOd", Field.E_FieldType.Dtm , Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanInfo, "LTV", "sLtvR", Field.E_FieldType.Dec , Field.E_ClassType.Pct, "N3");
                eB += m_Schema.Add(Category_LoanInfo, "CLTV", "sCltvR", Field.E_FieldType.Dec , Field.E_ClassType.Pct, "N3");
                eB += m_Schema.Add(Category_LoanInfo, "HCLTV", "sHcltvR", Field.E_FieldType.Dec, Field.E_ClassType.Pct, "N3");

                eB += m_Schema.Add(Category_LoanInfo, "Doc Type (Fannie Mae)", "sFannieDocT", Field.E_FieldType.Enu , Field.E_ClassType.Txt
                    , new EnumRepMap( E_sFannieDocT.Alternative           , "Alternative"            )
                    , new EnumRepMap( E_sFannieDocT.Full                  , "Full"                   )
                    , new EnumRepMap( E_sFannieDocT.LeaveBlank            , "Leave Blank"            )
                    , new EnumRepMap( E_sFannieDocT.NoDocumentation       , "No Documentation"       )
                    , new EnumRepMap( E_sFannieDocT.Reduced               , "Reduced"                )
                    , new EnumRepMap( E_sFannieDocT.StreamlinedRefinanced , "Streamlined Refinanced" )

                    , new EnumRepMap( E_sFannieDocT.NoRatio, "No Ratio")
                    , new EnumRepMap( E_sFannieDocT.LimitedDocumentation, "Limited Documentation")
                    , new EnumRepMap( E_sFannieDocT.NoIncomeNoEmploymentNoAssets, "No Income, No Employment and No Assets on 1003")
                    , new EnumRepMap( E_sFannieDocT.NoIncomeNoAssets, "No Income and No Assets on 1003")
                    , new EnumRepMap( E_sFannieDocT.NoAssets, "No Assets on 1003")
                    , new EnumRepMap( E_sFannieDocT.NoIncomeNoEmployment, "No Income and No Employment on 1003")
                    , new EnumRepMap( E_sFannieDocT.NoIncome, "No Income on 1003")
                    , new EnumRepMap( E_sFannieDocT.NoVerificationStatedIncomeEmploymentAssets, "No Verification of Stated Income, Employment or Assets")
                    , new EnumRepMap( E_sFannieDocT.NoVerificationStatedIncomeAssets, "No Verification of Stated Income or Assets")
                    , new EnumRepMap( E_sFannieDocT.NoVerificationStatedAssets, "No Verification of Stated Assets")
                    , new EnumRepMap( E_sFannieDocT.NoVerificationStatedIncomeEmployment, "No Verification of Stated Income or Employment")
                    , new EnumRepMap( E_sFannieDocT.NoVerificationStatedIncome, "No Verification of Stated Income")
                    , new EnumRepMap( E_sFannieDocT.VerbalVOE, "Verbal Verification of Employment (VVOE)")
                    , new EnumRepMap( E_sFannieDocT.OnePaystub, "One paystub")
                    , new EnumRepMap( E_sFannieDocT.OnePaystubAndVerbalVOE, "One paystub and VVOE")
                    , new EnumRepMap( E_sFannieDocT.OnePaystubOneW2VerbalVOE, "One paystub and one W-2 and VVOE or one yr 1040")

                    );



                eB += m_Schema.Add(Category_LoanInfo, "Term", "sTerm", Field.E_FieldType.Int , Field.E_ClassType.Cnt, "#,#;-#,#;0");
                eB += m_Schema.Add(Category_LoanInfo, "Due", "sDue", Field.E_FieldType.Int , Field.E_ClassType.Cnt, "#,#;-#,#;0");
                eB += m_Schema.Add(Category_LoanInfo, "Rate Requested at PML rate lock", "sNoteIRSubmitted", Field.E_FieldType.Dec , Field.E_ClassType.Pct, "N3"); //opm 33577 fs 07/29/09
                eB += m_Schema.Add(Category_LoanInfo, "Note Rate", "sNoteIR", Field.E_FieldType.Dec , Field.E_ClassType.Pct, "N3");
                eB += m_Schema.Add(Category_LoanInfo, "Qualified Rate", "sQualIR", Field.E_FieldType.Dec , Field.E_ClassType.Pct, "N3");
                eB += m_Schema.Add(Category_LoanInfo, "APR", "sApr", Field.E_FieldType.Dec , Field.E_ClassType.Pct, "N3");
                eB += m_Schema.Add(Category_LoanInfo, "Initial APR", "sInitAPR", Field.E_FieldType.Dec, Field.E_ClassType.Pct, "N3");
                eB += m_Schema.Add(Category_LoanInfo, "Last Disclosed APR", "sLastDiscAPR", Field.E_FieldType.Dec, Field.E_ClassType.Pct, "N3");
                eB += m_Schema.Add(Category_LoanInfo, "Is Rate Locked", "sIsRateLocked", Field.E_FieldType.Boo , Field.E_ClassType.Txt, new BoolRepMap( true  , "Yes" ), new BoolRepMap( false , "No"  ));
                eB += m_Schema.Add(Category_LoanInfo, "Rate Lock Status", "sRateLockStatusT", Field.E_FieldType.Enu, Field.E_ClassType.Txt,
                    new EnumRepMap(E_sRateLockStatusT.Locked, "Locked"),
                    new EnumRepMap(E_sRateLockStatusT.NotLocked, "Not Locked"), //opm 34229 fs 08/20/09
                    new EnumRepMap(E_sRateLockStatusT.LockRequested, "Lock Requested"),
                    new EnumRepMap(E_sRateLockStatusT.LockSuspended, "Lock Suspended"));
                eB += m_Schema.Add(Category_LoanInfo, "Rate Lock Period", "sRLckdDays", Field.E_FieldType.Int, Field.E_ClassType.Cnt, "#,#;-#,#;0");
                eB += m_Schema.Add(Category_LoanInfo, "PML Rate Lock Status", "sLoanOfficerRateLockStatusT", Field.E_FieldType.Enu, Field.E_ClassType.Txt,
                    new EnumRepMap(E_sLoanOfficerRateLockStatusT.NoRateSelected, "Not Selected"),
                    new EnumRepMap(E_sLoanOfficerRateLockStatusT.RateLockBroken, "Broken"),
                    new EnumRepMap(E_sLoanOfficerRateLockStatusT.RateSelected, "Locked"));
                eB += m_Schema.Add(Category_LoanInfo, "PML Rate Lock Date", "sLoanOfficerRateLockedD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanInfo, "Broker Rate Lock Price", "sBrokerLockFinalBrokComp1PcPrice", Field.E_FieldType.Dec, Field.E_ClassType.Pct, "N3");//opm 47976 fs 03/17/10

				eB += m_Schema.Add(Category_LoanInfo, "Broker Rate Lock Final Price - Fee", "sBrokerLockOriginatorPriceBrokComp1PcFee", Field.E_FieldType.Dec, Field.E_ClassType.Pct, "N3");
                eB += m_Schema.Add(Category_LoanInfo, "Broker Rate Lock Final Price - Price", "sBrokerLockOriginatorPriceBrokComp1PcPrice", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_LoanInfo, "Broker Rate Lock Final Price - Rate", "sBrokerLockOriginatorPriceNoteIR", Field.E_FieldType.Dec, Field.E_ClassType.Pct, "N3");
                eB += m_Schema.Add(Category_LoanInfo, "Broker Rate Lock Total Hidden Adjustments - Price", "sBrokerLockTotHiddenAdjBrokComp1PcPriceCR", Field.E_FieldType.Str, Field.E_ClassType.Txt); //opm 65998 gf 2/19/13
                eB += m_Schema.Add(Category_LoanInfo, "Broker Rate Lock Total Visible Adjustments - Price", "sBrokerLockTotVisibleAdjBrokComp1PcPriceCR", Field.E_FieldType.Str, Field.E_ClassType.Txt); //opm 65998 gf 2/19/13
                eB += m_Schema.Add(Category_LoanInfo, "Rate Lock Requested Date", "sBrokerRateLockRequestD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanInfo, "Interest Only", "sIsIOnly", Field.E_FieldType.Boo , Field.E_ClassType.Txt, new BoolRepMap( true  , "Yes" ), new BoolRepMap( false , "No"  ));
                eB += m_Schema.Add(Category_LoanInfo, "Interest Only Period", "sIOnlyMon", Field.E_FieldType.Int, Field.E_ClassType.Cnt, "#,#;-#,#;0");
                eB += m_Schema.Add(Category_LoanInfo, "Is Option ARM", "sIsOptionArm", Field.E_FieldType.Boo, Field.E_ClassType.Txt, new BoolRepMap(true, "Yes"), new BoolRepMap(false, "No"));
                eB += m_Schema.Add(Category_LoanInfo, "P & I", "sProThisMPmt", Field.E_FieldType.Dec , Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_LoanInfo, "Proposed Hazard Insurance", "sProHazIns", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C"); //opm fs 09/24/08 23346
				eB += m_Schema.Add(Category_LoanInfo, "Proposed Mortgage Insurance", "sProMIns", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
				eB += m_Schema.Add(Category_LoanInfo, "Upfront Mortgage Insurance Premium", "sFfUfmip1003", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C"); //opm fs 09/24/08 23346
				eB += m_Schema.Add(Category_LoanInfo, "Real Estate Taxes", "sProRealETx", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C"); //opm fs 09/24/08 23346
                eB += m_Schema.Add(Category_LoanInfo, "Housing Ratio", "sQualTopR", Field.E_FieldType.Dec, Field.E_ClassType.Pct, "N3");
                eB += m_Schema.Add(Category_LoanInfo, "DTI", "sQualBottomR", Field.E_FieldType.Dec , Field.E_ClassType.Pct, "N3");
                eB += m_Schema.Add(Category_LoanInfo, "Loan Program Name", "sLpTemplateNm", Field.E_FieldType.Str , Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_LoanInfo, "Lien Position", "sLienPosT", Field.E_FieldType.Enu , Field.E_ClassType.Txt, 
                    new EnumRepMap( E_sLienPosT.First  , "First"  ), 
                    new EnumRepMap( E_sLienPosT.Second , "Second" )
                    );

                eB += m_Schema.Add(Category_LoanInfo, "Purchase Price", "sPurchPrice", Field.E_FieldType.Dec , Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_LoanInfo, "Equity", "sEquityCalc", Field.E_FieldType.Dec , Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_LoanInfo, "Lock Request Investor Name", "sLpInvestorNm", Field.E_FieldType.Str , Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_LoanInfo, "Funding Source", "sFundingSrcDesc", Field.E_FieldType.Str , Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_LoanInfo, "Warehouse Funder", "sWarehouseFunderDesc", Field.E_FieldType.Str , Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_LoanInfo, "Is Texas Home Equity", "sIsTexasHomeEquity", Field.E_FieldType.Boo , Field.E_ClassType.Txt
                    , new BoolRepMap( true  , "Yes" )
                    , new BoolRepMap( false , "No"  )
                    );

                eB += m_Schema.Add(Category_LoanInfo, "Is Document Draw External", "sIsDocDrawExternal", Field.E_FieldType.Boo , Field.E_ClassType.Txt
                    , new BoolRepMap( true  , "Yes" )
                    , new BoolRepMap( false , "No"  )
                    );

                eB += m_Schema.Add(Category_LoanInfo, "Has Broker Prequal Notes", "sHasLpeNotesFromBrokerToUw"
                    , Field.E_FieldType.Boo , Field.E_ClassType.Txt
                    , new BoolRepMap( true  , "Yes" )
                    , new BoolRepMap( false , "No"  )
                    );

                eB += m_Schema.Add(Category_LoanInfo, "Amortization Type", "sFinMethT", Field.E_FieldType.Enu , Field.E_ClassType.Txt
                    , new EnumRepMap( E_sFinMethT.ARM       , "ARM"               )
                    , new EnumRepMap( E_sFinMethT.Fixed     , "Fixed"             )
                    , new EnumRepMap( E_sFinMethT.Graduated , "Graduated Payment" )
                    );

                eB += m_Schema.Add(Category_LoanInfo, "Amortization Type Description", "sFinMethDesc", Field.E_FieldType.Str , Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_LoanInfo, "Primary Borrower Name", "sPrimBorrowerFullNm", Field.E_FieldType.Str , Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_LoanInfo, "Lead Source", "sLeadSrcDesc", Field.E_FieldType.Str , Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_LoanInfo, "Original Loan Amount", "s1stMtgOrigLAmt", Field.E_FieldType.Dec , Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_LoanInfo, "Appraised Value", "sApprVal", Field.E_FieldType.Dec , Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_LoanInfo, "Margin", "sRAdjMarginR", Field.E_FieldType.Dec , Field.E_ClassType.Pct, "N3");
                eB += m_Schema.Add(Category_LoanInfo, "Index", "sRAdjIndexR", Field.E_FieldType.Dec , Field.E_ClassType.Pct, "N3");
                eB += m_Schema.Add(Category_LoanInfo, "Initial Cap", "sRAdj1stCapR", Field.E_FieldType.Dec , Field.E_ClassType.Pct, "N3");
                eB += m_Schema.Add(Category_LoanInfo, "Initial Adjust Period", "sRAdj1stCapMon", Field.E_FieldType.Int, Field.E_ClassType.Cnt,  "#,#;-#,#;0");
                eB += m_Schema.Add(Category_LoanInfo, "Periodic Cap", "sRAdjCapR", Field.E_FieldType.Dec , Field.E_ClassType.Pct, "N3");
                eB += m_Schema.Add(Category_LoanInfo, "Periodic Adjust Period", "sRAdjCapMon", Field.E_FieldType.Int, Field.E_ClassType.Cnt, "#,#;-#,#;0");
                eB += m_Schema.Add(Category_LoanInfo, "Lifetime Adj Cap", "sRAdjLifeCapR", Field.E_FieldType.Dec , Field.E_ClassType.Pct, "N3");

                eB += m_Schema.Add(Category_LoanInfo, "Fannie Mae ARM Index", "sArmIndexT", Field.E_FieldType.Enu, Field.E_ClassType.Txt
                    , new EnumRepMap(E_sArmIndexT.LeaveBlank, "")
                    , new EnumRepMap(E_sArmIndexT.WeeklyAvgCMT, "Weekly Average CMT")
                    , new EnumRepMap(E_sArmIndexT.MonthlyAvgCMT, "Monthly Average CMT")
                    , new EnumRepMap(E_sArmIndexT.WeeklyAvgTAAI, "Weekly Average TAAI")
                    , new EnumRepMap(E_sArmIndexT.WeeklyAvgTAABD, "Weekly Average TAABD")
                    , new EnumRepMap(E_sArmIndexT.WeeklyAvgSMTI, "Weekly Average SMTI")
                    , new EnumRepMap(E_sArmIndexT.DailyCDRate, "Daily CD Rate")
                    , new EnumRepMap(E_sArmIndexT.WeeklyAvgCDRate, "Weekly Average CD Rate")
                    , new EnumRepMap(E_sArmIndexT.WeeklyAvgPrimeRate, "Weekly Average Prime Rate")
                    , new EnumRepMap(E_sArmIndexT.TBillDailyValue, "T-Bill Daily Value")
                    , new EnumRepMap(E_sArmIndexT.EleventhDistrictCOF, "11th District COF")
                    , new EnumRepMap(E_sArmIndexT.NationalMonthlyMedianCostOfFunds, "National Monthly Median Cost of Funds")
                    , new EnumRepMap(E_sArmIndexT.WallStreetJournalLIBOR, "Wall Street Journal LIBOR")
                    , new EnumRepMap(E_sArmIndexT.FannieMaeLIBOR, "Fannie Mae LIBOR")
                    );
                    
                eB += m_Schema.Add(Category_LoanInfo, "Freddie Mac ARM Index", "sFreddieArmIndexT", Field.E_FieldType.Enu, Field.E_ClassType.Txt
                    , new EnumRepMap(E_sFreddieArmIndexT.LeaveBlank, "")
                    , new EnumRepMap(E_sFreddieArmIndexT.OneYearTreasury, "One Year Treasury")
                    , new EnumRepMap(E_sFreddieArmIndexT.ThreeYearTreasury, "Three Year Treasury")
                    , new EnumRepMap(E_sFreddieArmIndexT.SixMonthTreasury, "Six Month Treasury")
                    , new EnumRepMap(E_sFreddieArmIndexT.EleventhDistrictCostOfFunds, "11th District Cost Of Funds")
                    , new EnumRepMap(E_sFreddieArmIndexT.NationalMonthlyMedianCostOfFunds, "National Monthly Median Cost Of Funds")
                    , new EnumRepMap(E_sFreddieArmIndexT.LIBOR, "LIBOR")
                    , new EnumRepMap(E_sFreddieArmIndexT.Other, "Other")

                    );
                eB += m_Schema.Add(Category_LoanInfo, "Prepayment Penalty", "sPpmtPenaltyMon", Field.E_FieldType.Int , Field.E_ClassType.Cnt, "#,#;-#,#;0");
                eB += m_Schema.Add(Category_LoanInfo, "Credit Score I", "sCreditScoreType1", Field.E_FieldType.Int , Field.E_ClassType.Cnt, "#,#;-#,#;0");
                eB += m_Schema.Add(Category_LoanInfo, "Credit Score II", "sCreditScoreType2", Field.E_FieldType.Int , Field.E_ClassType.Cnt, "#,#;-#,#;0");
                eB += m_Schema.Add(Category_LoanInfo, "Credit Score II (Soft)", "sCreditScoreType2Soft", Field.E_FieldType.Int , Field.E_ClassType.Cnt, "#,#;-#,#;0");
                eB += m_Schema.Add(Category_LoanInfo, "Credit Score III", "sCreditScoreType3", Field.E_FieldType.Int , Field.E_ClassType.Cnt, "#,#;-#,#;0");
                eB += m_Schema.Add(Category_LoanInfo, "Internal Notes", "sTrNotes", Field.E_FieldType.Str , Field.E_ClassType.LongText);
                eB += m_Schema.Add(Category_LoanInfo, "Loan Count", "sLUnit", "1", Field.E_FieldType.Int , Field.E_ClassType.Cnt, "#,#;-#,#;0");
                eB += m_Schema.Add(Category_LoanInfo, "Trust Account Payable Total", "sTrustPayableTotal", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_LoanInfo, "Trust Account Receivable Total", "sTrustReceivableTotal", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C"); //opm 30889 fs 07/01/09
                eB += m_Schema.Add(Category_LoanInfo, "Trust Account Balance", "sTrustBalance", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");

                //opm 44264 fs 01/26/10
                eB += m_Schema.Add(Category_LoanInfo, "Max DTI", "sMaxDti", Field.E_FieldType.Dec, Field.E_ClassType.Pct, "N3");
                eB += m_Schema.Add(Category_LoanInfo, "Max Rate", "sMaxR", Field.E_FieldType.Dec, Field.E_ClassType.Pct, "N3");
                eB += m_Schema.Add(Category_LoanInfo, "Qualifying Score", "sCreditScoreLpeQual", Field.E_FieldType.Int , Field.E_ClassType.Cnt, "#,#;-#,#;0");
                eB += m_Schema.Add(Category_LoanInfo, "Appraisal Exp Date", "sApprRprtExpD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");

                eB += m_Schema.Add(Category_LoanInfo, "Waiver Form Received Date", "sApprWaiverReceivedD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanInfo, "Borrower Consent to Receive Electronic Copy Date", "sApprECopyConsentReceivedD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanInfo, "Appraisal Delivery Due Date", "sApprDeliveryDueD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanInfo, "Is Appraisal Sent to Borrower", "sIsApprSentToBorr", Field.E_FieldType.Boo, Field.E_ClassType.Txt
                   , new BoolRepMap(true, "Yes")
                   , new BoolRepMap(false, "No")
                   );

                eB += m_Schema.Add(Category_LoanInfo, "Type of Appraisal", "sTypeOfAppraisal", Field.E_FieldType.Str, Field.E_ClassType.Txt
                   , new EnumRepMap(E_TypeOfAppraisal.Full, "Full")
                   , new EnumRepMap(E_TypeOfAppraisal.DriveBy, "Drive By")
                   , new EnumRepMap(E_TypeOfAppraisal.MarketRentAnalysis, "Market Rent Analysis")
                   , new EnumRepMap(E_TypeOfAppraisal.None, "")
                   ); // opm 75193 mp
                eB += m_Schema.Add(Category_LoanInfo, "Income Doc Exp Date", "sIncomeDocExpD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanInfo, "Credit Report Exp Date", "sCrExpD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                
                eB += m_Schema.Add(Category_LoanInfo, "Monthly Payment", "sMonthlyPmt", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C"); //opm 50783 10/17/10
                eB += m_Schema.Add(Category_LoanInfo, "Broker Rate Lock Investor Price", "sBrokerLockInvestorPrice", Field.E_FieldType.Dec, Field.E_ClassType.Pct, "N3");
                eB += m_Schema.Add(Category_LoanInfo, "Purpose of Refinance", "sRefPurpose", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_LoanInfo, "Loan Channel", "sBranchChannelT", Field.E_FieldType.Enu, Field.E_ClassType.Txt
                   , new EnumRepMap(E_BranchChannelT.Blank, "")
                   , new EnumRepMap(E_BranchChannelT.Retail, "Retail")
                   , new EnumRepMap(E_BranchChannelT.Wholesale, "Wholesale")
                   , new EnumRepMap(E_BranchChannelT.Correspondent, "Correspondent")
                   , new EnumRepMap(E_BranchChannelT.Broker, "Broker")
                   );
                eB += m_Schema.Add(Category_LoanInfo, "Correspondent Process Type", "sCorrespondentProcessT", Field.E_FieldType.Enu, Field.E_ClassType.Txt
                   , new EnumRepMap(E_sCorrespondentProcessT.Blank, "")
                   , new EnumRepMap(E_sCorrespondentProcessT.Delegated, "Delegated")
                   , new EnumRepMap(E_sCorrespondentProcessT.PriorApproved, "Prior Approved")
                   , new EnumRepMap(E_sCorrespondentProcessT.MiniCorrespondent, "Mini-Correspondent")
                   , new EnumRepMap(E_sCorrespondentProcessT.Bulk, "Bulk")
                   , new EnumRepMap(E_sCorrespondentProcessT.MiniBulk, "Mini-Bulk")
                   );
                eB += m_Schema.Add(Category_LoanInfo, "Product Code", "sLpProductCode", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_LoanInfo, "AUS findings pulled", "sAusFindingsPull", Field.E_FieldType.Boo, Field.E_ClassType.Txt
                   , new BoolRepMap(true, "Yes")
                   , new BoolRepMap(false, "No")
                   );
                eB += m_Schema.Add(Category_LoanInfo, "Mortgage Insurance Provider", "sMiCompanyNmT", Field.E_FieldType.Enu, Field.E_ClassType.Txt
                    , new EnumRepMap(E_sMiCompanyNmT.LeaveBlank, "")
                    , new EnumRepMap(E_sMiCompanyNmT.Arch, "Arch MI")
                    , new EnumRepMap(E_sMiCompanyNmT.CMG, "CMG")
                    , new EnumRepMap(E_sMiCompanyNmT.Essent, "Essent")
                    , new EnumRepMap(E_sMiCompanyNmT.Genworth, "Genworth")
                    , new EnumRepMap(E_sMiCompanyNmT.MGIC, "MGIC")
                    , new EnumRepMap(E_sMiCompanyNmT.Radian, "Radian")
                    , new EnumRepMap(E_sMiCompanyNmT.UnitedGuaranty, "United Guaranty")
                    , new EnumRepMap(E_sMiCompanyNmT.Amerin, "Amerin")
                    , new EnumRepMap(E_sMiCompanyNmT.CAHLIF, "CAHLIF")
                    , new EnumRepMap(E_sMiCompanyNmT.CMGPre94, "CMG Pre-Sep 94")
                    , new EnumRepMap(E_sMiCompanyNmT.Commonwealth, "Commonwealth")
                    , new EnumRepMap(E_sMiCompanyNmT.MDHousing, "MD Housing")
                    , new EnumRepMap(E_sMiCompanyNmT.MIF, "MIF")
                    , new EnumRepMap(E_sMiCompanyNmT.PMI, "PMI")
                    , new EnumRepMap(E_sMiCompanyNmT.RMIC, "RMIC")
                    , new EnumRepMap(E_sMiCompanyNmT.RMICNC, "RMIC-NC")
                    , new EnumRepMap(E_sMiCompanyNmT.SONYMA, "SONYMA")
                    , new EnumRepMap(E_sMiCompanyNmT.Triad, "Triad")
                    , new EnumRepMap(E_sMiCompanyNmT.Verex, "Verex")
                    , new EnumRepMap(E_sMiCompanyNmT.WiscMtgAssr, "Wisc Mtg Assr")
                    , new EnumRepMap(E_sMiCompanyNmT.FHA, "FHA")
                    , new EnumRepMap(E_sMiCompanyNmT.VA, "VA")
                    , new EnumRepMap(E_sMiCompanyNmT.USDA, "USDA")
                    , new EnumRepMap(E_sMiCompanyNmT.NationalMI, "National MI")
                    , new EnumRepMap(E_sMiCompanyNmT.MassHousing, "MassHousing")
                    );
                eB += m_Schema.Add(Category_LoanInfo, "Mortgage Insurance Certificate ID", "sMiCertID", Field.E_FieldType.Str, Field.E_ClassType.Txt);


				eB += m_Schema.Add(Category_LoanInfo, "Mortgage Insurance Cert Issued Date", "sMICertIssuedD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanInfo, "Mortgage Insurance Type", "sMiInsuranceT", Field.E_FieldType.Enu, Field.E_ClassType.Txt
                    , new EnumRepMap(E_sMiInsuranceT.BorrowerPaid, "Borrower Paid")
                    , new EnumRepMap(E_sMiInsuranceT.InvestorPaid, "Investor Paid")
                    , new EnumRepMap(E_sMiInsuranceT.LenderPaid, "Lender Paid")
                    , new EnumRepMap(E_sMiInsuranceT.None, "")
                    );
                eB += m_Schema.Add(Category_LoanInfo, "Mortgage Insurance Coverage Percent", "sMiLenderPaidCoverage", Field.E_FieldType.Dec, Field.E_ClassType.Pct, "N3");

                eB += m_Schema.Add(Category_LoanInfo, "Mortgage Insurance Commitment Requested Date", "sMiCommitmentRequestedD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanInfo, "Mortgage Insurance Commitment Received Date", "sMiCommitmentReceivedD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanInfo, "Mortgage Insurance Commitment Expriation Date", "sMiCommitmentExpirationD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanInfo, "MI Payment Due Date", "sMiPmtDueD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanInfo, "MI Payment Made Date", "sMiPmtMadeD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanInfo, "Cancel at Midpoint", "sProMInsMidptCancel", Field.E_FieldType.Boo, Field.E_ClassType.Txt
                    , new BoolRepMap(true, "Yes")
                    , new BoolRepMap(false, "No"));
                eB += m_Schema.Add(Category_LoanInfo, "Cancel at LTV", "sProMInsCancelLtv", Field.E_FieldType.Dec, Field.E_ClassType.Pct, "N3");
				

                eB += m_Schema.Add(Category_LoanInfo, "Mortgage Pool ID", "sMortgagePoolId", Field.E_FieldType.Int, Field.E_ClassType.Cnt);
                eB += m_Schema.Add(Category_LoanInfo, "Pool Number by Agency", "PoolNumberByAgency", "dbo.RetrievePoolNumberByAgency(MORTGAGE_POOL.AgencyT,MORTGAGE_POOL.PoolTypeCode,MORTGAGE_POOL.BasePoolNumber,MORTGAGE_POOL.IssueTypeCode,MORTGAGE_POOL.PoolPrefix)", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_LoanInfo, "Pool Investor", "PoolInvestorName", "MORTGAGE_POOL.InvestorName", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_LoanInfo, "Pool Commitment Number", "PoolCommitmentNum", "MORTGAGE_POOL.CommitmentNum", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_LoanInfo, "Pool Commitment Date", "PoolCommitmentD", "MORTGAGE_POOL.CommitmentDate", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanInfo, "Pool Commitment Expires", "PoolCommitmentExpD", "MORTGAGE_POOL.CommitmentExpires", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanInfo, "Pool Trade Numbers", "PoolTradeNumbers", "MORTGAGE_POOL.PoolTradeNumbers", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_LoanInfo, "TIL Amount Financed", "sFinancedAmt", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_LoanInfo, "TIL Finance Charge", "sFinCharge", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_LoanInfo, "TIL Late Charges Percent Amt", "sLateChargePc" , Field.E_FieldType.Dec, Field.E_ClassType.Pct, "N3");
                eB += m_Schema.Add(Category_LoanInfo, "TIL Late Charges Percent", "sLateChargePcCR", Field.E_FieldType.Dec, Field.E_ClassType.Pct, "N3");
                eB += m_Schema.Add(Category_LoanInfo, "LP Loan ID", "sFreddieLoanId", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_LoanInfo, "Multiple Parcels", "sHasMultipleParcels", Field.E_FieldType.Boo, Field.E_ClassType.Txt
                    , new BoolRepMap(true, "Yes")
                    , new BoolRepMap(false, "No")
                    );
                eB += m_Schema.Add(Category_LoanInfo, "Qualified Mortgage Status", "sQMStatusT", Field.E_FieldType.Enu, Field.E_ClassType.Txt,
                    new EnumRepMap(E_sQMStatusT.Blank, CPageBase.sQMStatusT_map_rep(E_sQMStatusT.Blank)),
                    new EnumRepMap(E_sQMStatusT.Eligible, CPageBase.sQMStatusT_map_rep(E_sQMStatusT.Eligible)),
                    new EnumRepMap(E_sQMStatusT.Ineligible, CPageBase.sQMStatusT_map_rep(E_sQMStatusT.Ineligible)),
                    new EnumRepMap(E_sQMStatusT.ProvisionallyEligible, CPageBase.sQMStatusT_map_rep(E_sQMStatusT.ProvisionallyEligible)));

    
                eB += m_Schema.Add(Category_LoanInfo, "Higher-Priced Indicator", "sHighPricedMortgageT", Field.E_FieldType.Enu, Field.E_ClassType.Txt
                    , new EnumRepMap(E_HighPricedMortgageT.Unknown, "Unknown")
                    , new EnumRepMap(E_HighPricedMortgageT.None, "None")
                    , new EnumRepMap(E_HighPricedMortgageT.HigherPricedQm, "Higher-priced QM/HPML")
                    , new EnumRepMap(E_HighPricedMortgageT.HPML, "HPML")
                    , new EnumRepMap(E_HighPricedMortgageT.HigherPricedQmNotHpml, "Higher-priced QM/not HPML")
                    );

                eB += m_Schema.Add(Category_LoanInfo, "Late Days", "sLateDays", Field.E_FieldType.Int, Field.E_ClassType.Cnt, "#,#;-#,#;0");
                eB += m_Schema.Add(Category_LoanInfo, "Other Financing (P&I) Monthly Pmt", "sProOFinPmt", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_LoanInfo, "HOA", "sProHoAssocDues", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_LoanInfo, "Other Expense Monthly Pmt", "sProOHExp", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_LoanInfo, "Prepayment Penalty Exists", "sGfeHavePpmtPenalty", Field.E_FieldType.Boo, Field.E_ClassType.Txt
                    , new BoolRepMap(true, "Yes")
                    , new BoolRepMap(false, "No")
                    );
                eB += m_Schema.Add(Category_LoanInfo, "Section of the Act", "sFHAHousingActSection", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_LoanInfo, "Round", "sRAdjRoundToR", Field.E_FieldType.Dec, Field.E_ClassType.Pct, "N3");
                eB += m_Schema.Add(Category_LoanInfo, "Rate Floor", "sRAdjFloorR", Field.E_FieldType.Dec, Field.E_ClassType.Pct, "N3");
                eB += m_Schema.Add(Category_LoanInfo, "Property Type", "sGseSpT", Field.E_FieldType.Enu, Field.E_ClassType.Txt
                    , new EnumRepMap(E_sGseSpT.Attached, "Attached")
                    , new EnumRepMap(E_sGseSpT.Condominium, "Condominium")
                    , new EnumRepMap(E_sGseSpT.Cooperative, "Cooperative")
                    , new EnumRepMap(E_sGseSpT.Detached, "Detached")
                    , new EnumRepMap(E_sGseSpT.DetachedCondominium, "Detached Condominium")
                    , new EnumRepMap(E_sGseSpT.HighRiseCondominium, "HighRise Condominium")
                    , new EnumRepMap(E_sGseSpT.LeaveBlank, "")
                    , new EnumRepMap(E_sGseSpT.ManufacturedHomeCondominium, "Manufactured Home Condominium")
                    , new EnumRepMap(E_sGseSpT.ManufacturedHomeMultiwide, "Manufactured Home Multiwide")
                    , new EnumRepMap(E_sGseSpT.ManufacturedHousing, "Manufactured Housing")
                    , new EnumRepMap(E_sGseSpT.ManufacturedHousingSingleWide, "Manufactured Housing Single Wide")
                    , new EnumRepMap(E_sGseSpT.Modular, "Modular")
                    , new EnumRepMap(E_sGseSpT.PUD, "PUD")
                    );
                eB += m_Schema.Add(Category_LoanInfo, "Property Rights", "sEstateHeldT", Field.E_FieldType.Enu, Field.E_ClassType.Txt
                    , new EnumRepMap(E_sEstateHeldT.FeeSimple, "Fee Simple")
                    , new EnumRepMap(E_sEstateHeldT.LeaseHold, "Lease Hold")
                    );
                eB += m_Schema.Add(Category_LoanInfo, "Estimated Prepaid Items", "sTotEstPp1003", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_LoanInfo, "Lending Staff Notes", "sLendingStaffNotes", Field.E_FieldType.Str, Field.E_ClassType.LongText);
                //eB += m_Schema.Add(Category_LoanInfo, "Rate Floor", "sRAdjFloorR", Field.E_FieldType.Dec, Field.E_ClassType.Pct, "N3");


				eB += m_Schema.Add(Category_LoanInfo, "FHA Case Number for Current Loan", "sSpFhaCaseNumCurrentLoan", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_LoanInfo, "FHA Case Number for Previous Loan", "sFHAPreviousCaseNum", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_LoanInfo, "Balloon Payment Amount", "sGfeBalloonPmt", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");

                eB += m_Schema.Add(Category_LoanInfo, "GFE Interest Rate Available Through Date", "sGfeNoteIRAvailTillD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanInfo, "1003 Section VII Item L Amount1", "sOCredit1Amt", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_LoanInfo, "Cash from / to Borr", "sTransNetCash", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_LoanInfo, "Is DU Refi Plus", "sProdIsDuRefiPlus", Field.E_FieldType.Boo, Field.E_ClassType.Txt
                    , new BoolRepMap(true, "Yes"),
                    new BoolRepMap(false, "No")
                    );
                eB += m_Schema.Add(Category_LoanInfo, "Loan Commitment Prepared Date", "sMortgageLoanCommitmentPrepareDate", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanInfo, "Alternate Lender", "sAltLenderName", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_LoanInfo, "Needs Initial Disclosures?", "sNeedInitialDisc", Field.E_FieldType.Boo, Field.E_ClassType.Txt, new BoolRepMap(true, "Yes"), new BoolRepMap(false, "No"));
                eB += m_Schema.Add(Category_LoanInfo, "Needs Redisclosure?", "sNeedRedisc", Field.E_FieldType.Boo, Field.E_ClassType.Txt, new BoolRepMap(true, "Yes"), new BoolRepMap(false, "No"));
                eB += m_Schema.Add(Category_LoanInfo, "Type of Disclosure Needed", "sDisclosureNeededBrief", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_LoanInfo, "Disclosure Status", "sDisclosureNeededT", Field.E_FieldType.Enu, Field.E_ClassType.Txt,
                    new EnumRepMap(E_sDisclosureNeededT.None, ""),
                    new EnumRepMap(E_sDisclosureNeededT.InitDisc_RESPAAppReceived, "Initial Disclosures - RESPA Application Received"),
                    new EnumRepMap(E_sDisclosureNeededT.InitDisc_LoanRegistered, "Initial Disclosures - Loan Registered"),
                    new EnumRepMap(E_sDisclosureNeededT.EConsentDec_PaperDiscReqd, "E-Consent Declined - Paper Disclosure Required"),
                    new EnumRepMap(E_sDisclosureNeededT.EConsentNotReceived_PaperDiscReqd, "E-Consent Not Received - Paper Disclosure Required"),
                    new EnumRepMap(E_sDisclosureNeededT.APROutOfTolerance_RediscReqd, "APR Out of Tolerance, Redisclosure Required"),
                    new EnumRepMap(E_sDisclosureNeededT.CC_LoanAmtChanged, "Changed Circumstance - Loan Amount Change"),
                    new EnumRepMap(E_sDisclosureNeededT.CC_LoanLocked, "Changed Circumstance - Loan Locked"),
                    new EnumRepMap(E_sDisclosureNeededT.CC_LockExtended, "Changed Circumstance - Lock Extended"),
                    new EnumRepMap(E_sDisclosureNeededT.CC_Relocked, "Changed Circumstance - Relocked"),
                    new EnumRepMap(E_sDisclosureNeededT.CC_FloatDownLock, "Changed Circumstance - Float Down Lock"),
                    new EnumRepMap(E_sDisclosureNeededT.CC_GFEChargeDiscountChanged, "Changed Circumstance - GFE Charge/Discount Changed"),
                    new EnumRepMap(E_sDisclosureNeededT.CC_PropertyValueChanged, "Changed Circumstance - Property Value Changed"),
                    new EnumRepMap(E_sDisclosureNeededT.CC_UWCreditScoreChanged, "Changed Circumstance - U/W Credit Score Changed"),
                    new EnumRepMap(E_sDisclosureNeededT.CC_ProgramChanged, "Changed Circumstance - Program Changed"),
                    new EnumRepMap(E_sDisclosureNeededT.AwaitingEConsent, "Awaiting E-Consent"),
                    new EnumRepMap(E_sDisclosureNeededT.CC_ImpoundChanged, "Changed Circumstance - Impound Required Field Changed"),
                    new EnumRepMap(E_sDisclosureNeededT.CC_PurchasePriceChanged, "Changed Circumstance - Purchase Price Changed"),
                    new EnumRepMap(E_sDisclosureNeededT.InitDisc_RESPAAppReceivedVerbally, "Initial Disclosures - Consumer Portal Verbal Submission"),
                    new EnumRepMap(E_sDisclosureNeededT.Custom, $"Changed Circumstance - {ConstApp.CustomCocDefaultDescription}"));

                eB += m_Schema.Add(Category_LoanInfo, "Disclosure Needed Description", "sDisclosureNeededTDescription", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_LoanInfo, "Disclosure Due Date", "sDisclosuresDueD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanInfo, "E-signed documents on loan file", "sHasESignedDocumentsT", Field.E_FieldType.Enu, Field.E_ClassType.Txt, 
                        new EnumRepMap(E_sHasESignedDocumentsT.Undetermined, "Undetermined"),
                        new EnumRepMap(E_sHasESignedDocumentsT.No, "No"),
                        new EnumRepMap(E_sHasESignedDocumentsT.Yes, "Yes")
                    );

                eB += m_Schema.Add(
                    Category_LoanInfo,
                    "Automatic Disclosure Status",
                    "sAutoDisclosureStatusT",
                    Field.E_FieldType.Enu,
                    Field.E_ClassType.Txt,
                    new EnumRepMap(E_sAutoDisclosureStatusT.NA, "N/A"),
                    new EnumRepMap(E_sAutoDisclosureStatusT.Scheduled, "Scheduled"),
                    new EnumRepMap(E_sAutoDisclosureStatusT.Failed, "Automatic disclosure failed"));

                // opm 132968
                eB += m_Schema.Add(Category_LoanInfo, "Upfront Mortgage Insurance Premium Rate", "sFfUfmipR", Field.E_FieldType.Dec, Field.E_ClassType.Pct, "N3");
                eB += m_Schema.Add(Category_LoanInfo, "Monthly Mortgage Insurance Annual Rate", "sProMInsR", Field.E_FieldType.Dec, Field.E_ClassType.Pct, "N3");
                eB += m_Schema.Add(Category_LoanInfo, "Minimum Payments before MI Cancellation", "sProMInsCancelMinPmts", Field.E_FieldType.Int, Field.E_ClassType.Cnt, "#,#;-#,#;0");
                eB += m_Schema.Add(Category_LoanInfo, "MI Initial Rate Term", "sProMInsMon", Field.E_FieldType.Int, Field.E_ClassType.Cnt, "#,#;-#,#;0");

                eB += m_Schema.Add(Category_LoanInfo, "Employee Loan", "sIsEmployeeLoan", Field.E_FieldType.Boo, Field.E_ClassType.Txt
                    , new BoolRepMap(true, "Yes")
                    , new BoolRepMap(false, "No")
                    );

                eB += m_Schema.Add(Category_LoanInfo, "Lender Paid Single Premium Rate", "sLenderUfmipR", Field.E_FieldType.Dec, Field.E_ClassType.Pct, "N3");
                eB += m_Schema.Add(Category_LoanInfo, "Lender Paid Single Premium", "sLenderUfmip", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_LoanInfo, "Is Line of Credit?", "sIsLineOfCredit", Field.E_FieldType.Boo, Field.E_ClassType.Txt, new BoolRepMap(true, "Yes"), new BoolRepMap(false, "No"));

                eB += m_Schema.Add(Category_LoanInfo, "Credit Line Amount", "sCreditLineAmt", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");

                eB += m_Schema.Add(Category_LoanInfo, "# Of Rate Renegotiations On File", "sFERateRenegotiationCount", Field.E_FieldType.Int, Field.E_ClassType.Cnt, "#,#;-#,#;0");
                eB += m_Schema.Add(Category_LoanInfo, "# Of Extensions (Current Lock)", "sFECurrentLockExtensionCount", Field.E_FieldType.Int, Field.E_ClassType.Cnt, "#,#;-#,#;0");
                eB += m_Schema.Add(Category_LoanInfo, "# Of Extensions (All Locks On File)", "sFETotalLockExtensionCount", Field.E_FieldType.Int, Field.E_ClassType.Cnt, "#,#;-#,#;0");
                eB += m_Schema.Add(Category_LoanInfo, "# Of Rate Re-Locks On File", "sFETotalRateReLockCount", Field.E_FieldType.Int, Field.E_ClassType.Cnt, "#,#;-#,#;0");

                eB += m_Schema.Add(Category_LoanInfo, "Loan File Type", "sLoanFileT", Field.E_FieldType.Enu, Field.E_ClassType.Txt,
                    new EnumRepMap(E_sLoanFileT.Loan, "Loan"),
                    new EnumRepMap(E_sLoanFileT.Sandbox, "Sandboxed"),
                    new EnumRepMap(E_sLoanFileT.QuickPricer2Sandboxed, "QuickPricer 2"),
                    new EnumRepMap(E_sLoanFileT.Test, "Test"));

                // OPM 190201 - Consumer Portal Creation/Submission Tracking
                eB += m_Schema.Add(Category_LoanInfo, "Consumer Portal Application Creation Date", "sConsumerPortalCreationD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");

                eB += m_Schema.Add(Category_LoanInfo, "Consumer Portal Creation Type", "sConsumerPortalCreationT", Field.E_FieldType.Enu, Field.E_ClassType.Txt,
                    new EnumRepMap(E_sConsumerPortalCreationT.NotApplicable, ""),
                    new EnumRepMap(E_sConsumerPortalCreationT.FullApplication, "Full Application"),
                    new EnumRepMap(E_sConsumerPortalCreationT.ShortApplication, "Short Application "),
                    new EnumRepMap(E_sConsumerPortalCreationT.Respa6Only, "RESPA 6 Only Application"));

                eB += m_Schema.Add(Category_LoanInfo, "Consumer Portal Application Submitted Date", "sConsumerPortalSubmittedD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");

                eB += m_Schema.Add(Category_LoanInfo, "Consumer Portal Verbal Submission Date", "sConsumerPortalVerbalSubmissionD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                
                eB += m_Schema.Add(Category_LoanInfo, "Consumer Portal Verbal Submission Comments", "sConsumerPortalVerbalSubmissionComments", Field.E_FieldType.Str, Field.E_ClassType.LongText);

                //OPM 211478 - Loan Estimate fields
                eB += m_Schema.Add(Category_LoanInfo, "Is TRID Loan Estimate Rate Locked?", "sTRIDIsLoanEstimateRateLocked", Field.E_FieldType.Boo, Field.E_ClassType.Txt, new BoolRepMap(true, "Yes"), new BoolRepMap(false, "No"));
                eB += m_Schema.Add(Category_LoanInfo, "TRID Loan Estimate Rate Available Through Date", "sTRIDLoanEstimateNoteIRAvailTillD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanInfo, "TRID Loan Estimate Lock Status Setting Method", "sTRIDLoanEstimateSetLockStatusMethodT", Field.E_FieldType.Boo, Field.E_ClassType.Txt, new BoolRepMap(true, "Yes"), new BoolRepMap(false, "No"));

                // OPM 227237, ML, 12/14/2015
                eB += m_Schema.Add(Category_LoanInfo, "Asset Exp Date", "sAssetExpD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanInfo, "Bond Doc Exp Date", "sBondDocExpD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");

                // OPM 235653, 12/30/2015, ML
                eB += m_Schema.Add(Category_LoanInfo, "Last Disclosed Initial Credit", "sLenderLastDisclosedInitialCredit_Neg", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_LoanInfo, "Actual initial charge(+)/credit(-)", "sLenderActualInitialCreditAmt_Neg", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");

                eB += m_Schema.Add(Category_LoanInfo, "Overnight Process Report for Pricing", "sAutoPriceProcessResultT", Field.E_FieldType.Enu, Field.E_ClassType.Txt,
                    new EnumRepMap(E_AutoPriceProcessResultT.Blank, ""),
                    new EnumRepMap(E_AutoPriceProcessResultT.NoChangeDetected, "No Change Detected"),
                    new EnumRepMap(E_AutoPriceProcessResultT.PriceChangeDetected, "Price Change Detected"),
                    new EnumRepMap(E_AutoPriceProcessResultT.ReportCleared, "Report Cleared"),
                    new EnumRepMap(E_AutoPriceProcessResultT.UnableToDetermine, "Unable To Determine"));

                eB += m_Schema.Add(Category_LoanInfo, "Overnight Process Report for Eligibility", "sAutoEligibilityProcessResultT", Field.E_FieldType.Enu, Field.E_ClassType.Txt,
                    new EnumRepMap(E_AutoEligibilityProcessResultT.Blank, ""),
                    new EnumRepMap(E_AutoEligibilityProcessResultT.Eligible, "Eligible"),
                    new EnumRepMap(E_AutoEligibilityProcessResultT.Ineligible, "Ineligible"),
                    new EnumRepMap(E_AutoEligibilityProcessResultT.ReportCleared, "Report Cleared"),
                    new EnumRepMap(E_AutoEligibilityProcessResultT.UnableToDetermine, "Unable To Determine"));

                eB += m_Schema.Add(Category_LoanInfo, "Not Subject to Ability to Repay", "sIsExemptFromAtr", Field.E_FieldType.Boo, Field.E_ClassType.Txt, new BoolRepMap(true, "Yes"), new BoolRepMap(false, "No"));

                eB += m_Schema.Add(Category_LoanInfo, "Is Renovation Loan?", "sIsRenovationLoan", Field.E_FieldType.Boo, Field.E_ClassType.Txt, new BoolRepMap(true, "Yes"), new BoolRepMap(false, "No"));

                eB += m_Schema.Add(Category_LoanInfo, "Loan Product Type", "sLpProductType", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_LoanInfo, "Loan Migration Version", "sLoanVersionT", Field.E_FieldType.Enu, Field.E_ClassType.Txt, Enum.GetValues(typeof(LoanVersionT)).Cast<LoanVersionT>().Select(v => new EnumRepMap(v, v.ToString("d"))).ToArray());

                eB += m_Schema.Add(Category_LoanInfo, "Target GSE Application Version", "sGseTargetApplicationT", Field.E_FieldType.Enu, Field.E_ClassType.Txt,
                    new EnumRepMap(GseTargetApplicationT.Blank, string.Empty),
                    new EnumRepMap(GseTargetApplicationT.Legacy, "Legacy"),
                    new EnumRepMap(GseTargetApplicationT.Ulad2019, "ULAD 2019"));

                eB += m_Schema.Add(Category_LoanInfo, "Is Using a Non-QM Program?", "sIsUsingNonQmProgram", Field.E_FieldType.Boo, Field.E_ClassType.Txt, new BoolRepMap(true, "Yes"), new BoolRepMap(false, "No"));
                eB += m_Schema.Add(Category_LoanInfo, "Was Created Through Non-QM Portal?", "sWasCreatedThroughNonQmPortal", Field.E_FieldType.Boo, Field.E_ClassType.Txt, new BoolRepMap(true, "Yes"), new BoolRepMap(false, "No"));
                #endregion

                #region ( Loan Status Fields )

                eB += m_Schema.Add(Category_LoanStatus, "Loan Category", "sStatusLoanCategoryT", Field.E_FieldType.Enu , Field.E_ClassType.Txt
                    , new EnumRepMap( E_sStatusLoanCategoryT.Loan_Active           , "Active Loan"   )
                    , new EnumRepMap( E_sStatusLoanCategoryT.Loan_Inactive         , "Inactive Loan" )
                    , new EnumRepMap( E_sStatusLoanCategoryT.Not_Valid_Loan_Status , "Other Loan"    )
                    );

                var statusRepMaps = (from m in GenerateStatusRepMaps()
                                     orderby m.Val
                                     select m).ToArray();

                eB += m_Schema.Add(Category_LoanStatus, "Loan Status", "sStatusT", Field.E_FieldType.Enu , Field.E_ClassType.Txt, 
                    statusRepMaps
                );

                var progressRepMaps = (from m in GenerateStatusRepMaps()
                                     orderby CPageBase.s_OrderByStatusT[(E_sStatusT)m.EnumCode]
                                     select m).ToArray();

                eB += m_Schema.Add(Category_LoanStatus, "Loan Progress", "sStatusProgressT", "sStatusT", Field.E_FieldType.Enu , Field.E_ClassType.Txt,
                    progressRepMaps
                    );

                eB += m_Schema.Add(Category_LoanStatus, "Loan Status Date", "sStatusD", Field.E_FieldType.Dtm , Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Loan Status Month", "sStatusMonth", "datepart( mm , cast( sStatusD as datetime ) )", Field.E_FieldType.Int , Field.E_ClassType.Txt
                    , new IntegerRepMap( 1  , "January"   )
                    , new IntegerRepMap( 2  , "February"  )
                    , new IntegerRepMap( 3  , "March"     )
                    , new IntegerRepMap( 4  , "April"     )
                    , new IntegerRepMap( 5  , "May"       )
                    , new IntegerRepMap( 6  , "June"      )
                    , new IntegerRepMap( 7  , "July"      )
                    , new IntegerRepMap( 8  , "August"    )
                    , new IntegerRepMap( 9  , "September" )
                    , new IntegerRepMap( 10 , "October"   )
                    , new IntegerRepMap( 11 , "November"  )
                    , new IntegerRepMap( 12 , "December"  )
                    );

                eB += m_Schema.Add(Category_LoanStatus, "Loan Status Year", "sStatusYear", "datepart( yyyy , cast( sStatusD as datetime ) )", Field.E_FieldType.Int , Field.E_ClassType.Txt);
                // ignore time component in sStatusDays - opm 204617
                eB += m_Schema.Add(Category_LoanStatus, "Loan Status Days", "sStatusDays", "datediff( dd, cast( sStatusD as datetime ), getdate() )", Field.E_FieldType.Int, Field.E_ClassType.Cnt, "#,#;-#,#;0");
                eB += m_Schema.Add(Category_LoanStatus, "Loan Status Locked", "sStatusLckd", Field.E_FieldType.Boo , Field.E_ClassType.Txt
                    , new BoolRepMap( true  , "Locked"     )
                    , new BoolRepMap( false , "Not Locked" )
                    );

                eB += m_Schema.Add(Category_LoanStatus, "Secondary Status", "sSecondStatus", Field.E_FieldType.Enu, Field.E_ClassType.Txt,
                    new EnumRepMap(E_sSecondStatusT.None, "None"),
                    new EnumRepMap(E_sSecondStatusT.Loan_Disbursed, "Investor Funds Disbursed"),
                    new EnumRepMap(E_sSecondStatusT.Loan_Funded, "Loan Funded"),
                    new EnumRepMap(E_sSecondStatusT.Loan_Purchased, "Loan Sold"),
                    new EnumRepMap(E_sSecondStatusT.Loan_Reconciled, "Loan Reconciled"));//opm 47976 fs 03/17/10

                eB += m_Schema.Add(Category_LoanStatus, "Opened Days", "sOpenedDays", "cast( cast( case sStatusLoanCategoryT when '0' then getdate() when '1' then sLoanInactiveD else null end as datetime ) - cast( sOpenedD as datetime ) as int )", Field.E_FieldType.Int, Field.E_ClassType.Cnt, "#,#;-#,#;0");
                eB += m_Schema.Add(Category_LoanStatus, "Last Active Date", "sLastActiveD", "cast( case sStatusLoanCategoryT when '0' then getdate() when '1' then sLoanInactiveD else null end as datetime )", Field.E_FieldType.Dtm , Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "New Lead Date", "sLeadD", Field.E_FieldType.Dtm , Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "New Lead Date - Time", "sLeadDTime", "sLeadD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "hh:mm tt");
                eB += m_Schema.Add(Category_LoanStatus, "New Lead Comments", "sLeadN", Field.E_FieldType.Str, Field.E_ClassType.LongText);
                eB += m_Schema.Add(Category_LoanStatus, "Created Date", "sCreatedD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Created Date - Time", "sCreatedDTime", "sCreatedD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "hh:mm tt");
                eB += m_Schema.Add(Category_LoanStatus, "Opened Date", "sOpenedD", Field.E_FieldType.Dtm , Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Opened Date - Time", "sOpenedDTime", "sOpenedD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "hh:mm tt");
                eB += m_Schema.Add(Category_LoanStatus, "Opened Comments", "sOpenedN", Field.E_FieldType.Str, Field.E_ClassType.LongText);
                eB += m_Schema.Add(Category_LoanStatus, "Prequalified Date", "sPreQualD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Prequalified Date - Time", "sPreQualDTime", "sPreQualD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "hh:mm tt");
                eB += m_Schema.Add(Category_LoanStatus, "Prequalified Comments", "sPreQualN", Field.E_FieldType.Str, Field.E_ClassType.LongText);
                eB += m_Schema.Add(Category_LoanStatus, "Registered Date", "sSubmitD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy"); //opm 33577 fs 07/29/09
                eB += m_Schema.Add(Category_LoanStatus, "Registered Date - Time", "sSubmitDTime", "sSubmitD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "hh:mm tt"); 
                eB += m_Schema.Add(Category_LoanStatus, "Registered Comments", "sSubmitN", Field.E_FieldType.Str, Field.E_ClassType.LongText);
                eB += m_Schema.Add(Category_LoanStatus, "Processing Date", "sProcessingD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Processing Date - Time", "sProcessingDTime", "sProcessingD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "hh:mm tt");
                eB += m_Schema.Add(Category_LoanStatus, "Processing Comments", "sProcessingN", Field.E_FieldType.Str, Field.E_ClassType.LongText);
                eB += m_Schema.Add(Category_LoanStatus, "Preapproved Date", "sPreApprovD", Field.E_FieldType.Dtm , Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Preapproved Date - Time", "sPreApprovDTime", "sPreApprovD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "hh:mm tt");
                eB += m_Schema.Add(Category_LoanStatus, "Preapproved Comments", "sPreApprovN", Field.E_FieldType.Str, Field.E_ClassType.LongText);
                eB += m_Schema.Add(Category_LoanStatus, "Estimated Close Date", "sEstCloseD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Estimated Close Date - Time", "sEstCloseDTime", "sEstCloseD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "hh:mm tt");
                eB += m_Schema.Add(Category_LoanStatus, "Estimated Close Comments", "sEstCloseN", Field.E_FieldType.Str, Field.E_ClassType.LongText);
                eB += m_Schema.Add(Category_LoanStatus, "Submitted Date", "sLoanSubmittedD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Submitted Date - Time", "sLoanSubmittedDTime", "sLoanSubmittedD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "hh:mm tt");
                eB += m_Schema.Add(Category_LoanStatus, "Submitted Comments", "sLoanSubmittedN", Field.E_FieldType.Str, Field.E_ClassType.LongText);
                eB += m_Schema.Add(Category_LoanStatus, "Underwriting Date", "sUnderwritingD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Underwriting Date - Time", "sUnderwritingDTime", "sUnderwritingD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "hh:mm tt");
                eB += m_Schema.Add(Category_LoanStatus, "Underwriting Comments", "sUnderwritingN", Field.E_FieldType.Str, Field.E_ClassType.LongText);
                eB += m_Schema.Add(Category_LoanStatus, "Approved Date", "sApprovD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Approved Date - Time", "sApprovDTime", "sApprovD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "hh:mm tt");
                eB += m_Schema.Add(Category_LoanStatus, "Approved Comments", "sApprovN", Field.E_FieldType.Str, Field.E_ClassType.LongText);
                eB += m_Schema.Add(Category_LoanStatus, "Approval Exp Date", "sAppExpD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Final Underwriting Date", "sFinalUnderwritingD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Final Underwriting Date - Time", "sFinalUnderwritingDTime", "sFinalUnderwritingD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "hh:mm tt");
                eB += m_Schema.Add(Category_LoanStatus, "Final Underwriting Comments", "sFinalUnderwritingN", Field.E_FieldType.Str, Field.E_ClassType.LongText);
                eB += m_Schema.Add(Category_LoanStatus, "Clear to Close Date", "sClearToCloseD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Clear to Close Date - Time", "sClearToCloseDTime", "sClearToCloseD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "hh:mm tt");
                eB += m_Schema.Add(Category_LoanStatus, "Clear to Close Comments", "sClearToCloseN", Field.E_FieldType.Str, Field.E_ClassType.LongText);
                eB += m_Schema.Add(Category_LoanStatus, "Docs Out Date", "sDocsD", Field.E_FieldType.Dtm , Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Docs Out Date - Time", "sDocsDTime", "sDocsD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "hh:mm tt");
                eB += m_Schema.Add(Category_LoanStatus, "Docs Out Comments", "sDocsN", Field.E_FieldType.Str, Field.E_ClassType.LongText);
                eB += m_Schema.Add(Category_LoanStatus, "Docs Back Date", "sDocsBackD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Docs Back Date - Time", "sDocsBackDTime", "sDocsBackD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "hh:mm tt");
                eB += m_Schema.Add(Category_LoanStatus, "Docs Back Comments", "sDocsBackN", Field.E_FieldType.Str, Field.E_ClassType.LongText);
                eB += m_Schema.Add(Category_LoanStatus, "Scheduled Funding", "sSchedFundD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Scheduled Funding - Time", "sSchedFundDTime", "sSchedFundD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "hh:mm tt");
                eB += m_Schema.Add(Category_LoanStatus, "Scheduled Funding Notes", "sSchedFundN", Field.E_FieldType.Str, Field.E_ClassType.LongText);
                eB += m_Schema.Add(Category_LoanStatus, "Ten Business Days from Scheduled Fund D", "sTenBusinessDaysFromSchedFundD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Funding Conditions Date", "sFundingConditionsD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Funding Conditions Date - Time", "sFundingConditionsDTime", "sFundingConditionsD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "hh:mm tt");
                eB += m_Schema.Add(Category_LoanStatus, "Funding Conditions Comments", "sFundingConditionsN", Field.E_FieldType.Str, Field.E_ClassType.LongText);
                eB += m_Schema.Add(Category_LoanStatus, "Funds Ordered Date", "sFundsOrderedD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy"); // OPM 109299 GF
                eB += m_Schema.Add(Category_LoanStatus, "Funded Date", "sFundD", Field.E_FieldType.Dtm , Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Funded Date - Time", "sFundDTime", "sFundD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "hh:mm tt");
                eB += m_Schema.Add(Category_LoanStatus, "Funded Comments", "sFundN", Field.E_FieldType.Str, Field.E_ClassType.LongText);
                eB += m_Schema.Add(Category_LoanStatus, "Recorded Date", "sRecordedD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Recorded Date - Time", "sRecordedDTime", "sRecordedD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "hh:mm tt");
                eB += m_Schema.Add(Category_LoanStatus, "Recorded Comments", "sRecordedN", Field.E_FieldType.Str, Field.E_ClassType.LongText);
                eB += m_Schema.Add(Category_LoanStatus, "MERS Registration Date", "sMersRegistrationD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "MERS TOB Date", "sMersTobD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "MERS TOS Date", "sMersTosD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Final Docs Date", "sFinalDocsD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Final Docs Date - Time", "sFinalDocsDTime", "sFinalDocsD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "hh:mm tt");
                eB += m_Schema.Add(Category_LoanStatus, "Final Docs Comments", "sFinalDocsN", Field.E_FieldType.Str, Field.E_ClassType.LongText);
                eB += m_Schema.Add(Category_LoanStatus, "Closed Date", "sClosedD", Field.E_FieldType.Dtm , Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Closed Date - Time", "sClosedDTime", "sClosedD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "hh:mm tt");
                eB += m_Schema.Add(Category_LoanStatus, "Closed Comments", "sClosedN", Field.E_FieldType.Str, Field.E_ClassType.LongText);
                eB += m_Schema.Add(Category_LoanStatus, "On-hold Date", "sOnHoldD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "On-hold Date - Time", "sOnHoldDTime", "sOnHoldD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "hh:mm tt");
                eB += m_Schema.Add(Category_LoanStatus, "On-hold Comments", "sOnHoldN", Field.E_FieldType.Str, Field.E_ClassType.LongText); //opm 33820 fs 08/07/09
                eB += m_Schema.Add(Category_LoanStatus, "Canceled Date", "sCanceledD", Field.E_FieldType.Dtm , Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Canceled Date - Time", "sCanceledDTime", "sCanceledD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "hh:mm tt");
                eB += m_Schema.Add(Category_LoanStatus, "Canceled Comments", "sCanceledN", Field.E_FieldType.Str, Field.E_ClassType.LongText); //opm 49795 fs 06/10/10
                eB += m_Schema.Add(Category_LoanStatus, "Loan Denied Date", "sRejectD", Field.E_FieldType.Dtm , Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Loan Denied Date - Time", "sRejectDTime", "sRejectD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "hh:mm tt");
                eB += m_Schema.Add(Category_LoanStatus, "Loan Denied Comments", "sRejectN", Field.E_FieldType.Str, Field.E_ClassType.LongText); //opm 49795 fs 06/10/10
                eB += m_Schema.Add(Category_LoanStatus, "Suspended Date", "sSuspendedD", Field.E_FieldType.Dtm , Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Suspended Date - Time", "sSuspendedDTime", "sSuspendedD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "hh:mm tt");
                eB += m_Schema.Add(Category_LoanStatus, "Suspended Comments", "sSuspendedN", Field.E_FieldType.Str, Field.E_ClassType.LongText); //opm 49795 fs 06/10/10
                eB += m_Schema.Add(Category_LoanStatus, "Loan Shipped Date", "sShippedToInvestorD", Field.E_FieldType.Dtm , Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Loan Shipped Date - Time", "sShippedToInvestorDTime", "sShippedToInvestorD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "hh:mm tt");
                eB += m_Schema.Add(Category_LoanStatus, "Loan Shipped Comments", "sShippedToInvestorN", Field.E_FieldType.Str, Field.E_ClassType.LongText);
                eB += m_Schema.Add(Category_LoanStatus, "Investor Conditions Date", "sSuspendedByInvestorD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy"); // opm 90006
                eB += m_Schema.Add(Category_LoanStatus, "Investor Conditions Date - Time", "sSuspendedByInvestorDTime", "sSuspendedByInvestorD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "hh:mm tt");
                eB += m_Schema.Add(Category_LoanStatus, "Investor Conditions Comments", "sSuspendedByInvestorN", Field.E_FieldType.Str, Field.E_ClassType.LongText);
                eB += m_Schema.Add(Category_LoanStatus, "Investor Conditions Sent Date", "sCondSentToInvestorD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy"); // opm 90006
                eB += m_Schema.Add(Category_LoanStatus, "Investor Conditions Sent Date - Time", "sCondSentToInvestorDTime", "sCondSentToInvestorD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "hh:mm tt");
                eB += m_Schema.Add(Category_LoanStatus, "Investor Conditions Sent Comments", "sCondSentToInvestorN", Field.E_FieldType.Str, Field.E_ClassType.LongText);
                eB += m_Schema.Add(Category_LoanStatus, "Additional Conditions Sent Date", "sAdditionalCondSentD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy"); // opm 90006
                eB += m_Schema.Add(Category_LoanStatus, "Loan Sold Date", "sLPurchaseD", Field.E_FieldType.Dtm , Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Loan Sold Date - Time", "sLPurchaseDTime", "sLPurchaseD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "hh:mm tt");
                eB += m_Schema.Add(Category_LoanStatus, "Loan Sold Comments", "sLPurchasedN", Field.E_FieldType.Str, Field.E_ClassType.LongText);
                eB += m_Schema.Add(Category_LoanStatus, "Goodbye Letter Date", "sGoodByLetterD", Field.E_FieldType.Dtm , Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "GFE Prepared Date", "sGfeTilPrepareDate", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy"); //opm 27821 fs 02/17/09
                eB += m_Schema.Add(Category_LoanStatus, "Initial GFE Disclosure Date", "sGfeInitialDisclosureD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "GFE Re-disclosure Date", "sGfeRedisclosureD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "TIL Prepared Date", "sTilPrepareDate", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy"); //opm 60033
                eB += m_Schema.Add(Category_LoanStatus, "Initial TIL Disclosure Date", "sTilInitialDisclosureD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "TIL Re-disclosure Date", "sTilRedisclosureD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "1003 Interview Date", "sApp1003InterviewerPrepareDate", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");//opm 60033
                eB += m_Schema.Add(Category_LoanStatus, "Investor funds disbursed date", "sDisbursementD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");//opm 47976 fs 03/17/10
                eB += m_Schema.Add(Category_LoanStatus, "Loan reconciled date", "sReconciledD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");//opm 47976 fs 03/17/10
                eB += m_Schema.Add(Category_LoanStatus, "First Payment Date", "sSchedDueD1", Field.E_FieldType.Dtm , Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Second Payment Date", "sSchedDueD2", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Third Payment Date", "sSchedDueD3", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Rate Locked Date", "sRLckdD", Field.E_FieldType.Dtm , Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Lock Comments", "sRLckdN", Field.E_FieldType.Str, Field.E_ClassType.Txt); //opm 12719 fs 02/03/09
                eB += m_Schema.Add(Category_LoanStatus, "Lock Expiration Date", "sRLckdExpiredD", Field.E_FieldType.Dtm , Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Lock Expiration Comments", "sRLckdExpiredN", Field.E_FieldType.Str, Field.E_ClassType.Txt); //opm 12719 fs 02/03/09
                eB += m_Schema.Add(Category_LoanStatus, "Custom Event #1 Description", "sU1LStatDesc", Field.E_FieldType.Str , Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_LoanStatus, "Custom Event #1 Date", "sU1LStatD", Field.E_FieldType.Dtm , Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Custom Event #1 Notes", "sU1LStatN", Field.E_FieldType.Str , Field.E_ClassType.LongText);
                eB += m_Schema.Add(Category_LoanStatus, "Custom Event #2 Description", "sU2LStatDesc", Field.E_FieldType.Str , Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_LoanStatus, "Custom Event #2 Date", "sU2LStatD", Field.E_FieldType.Dtm , Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Custom Event #2 Notes", "sU2LStatN", Field.E_FieldType.Str , Field.E_ClassType.LongText);
                eB += m_Schema.Add(Category_LoanStatus, "Custom Event #3 Description", "sU3LStatDesc", Field.E_FieldType.Str , Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_LoanStatus, "Custom Event #3 Date", "sU3LStatD", Field.E_FieldType.Dtm , Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Custom Event #3 Notes", "sU3LStatN", Field.E_FieldType.Str , Field.E_ClassType.LongText);
                eB += m_Schema.Add(Category_LoanStatus, "Custom Event #4 Description", "sU4LStatDesc", Field.E_FieldType.Str , Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_LoanStatus, "Custom Event #4 Date", "sU4LStatD", Field.E_FieldType.Dtm , Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Custom Event #4 Notes", "sU4LStatN", Field.E_FieldType.Str , Field.E_ClassType.LongText);
                eB += m_Schema.Add(Category_LoanStatus, "Purchase Contract Date", "sPurchaseContractDate", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Financing Contingency Expiration Date", "sFinancingContingencyExpD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Financing Contingency Extension Expiration Date", "sFinancingContingencyExtensionExpD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Financing Contingency Deadline Date", "sFinancingContingencyDeadlineD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Prelim Report Ordered Date", "sPrelimRprtOd", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Prelim Report Due Date", "sPrelimRprtDueD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Prelim Report Document Date", "sPrelimRprtDocumentD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy"); // OPM 80416 gf
                eB += m_Schema.Add(Category_LoanStatus, "Prelim Report Received Date", "sPrelimRprtRd", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Prelim Report Comments", "sPrelimRprtN", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_LoanStatus, "Closing Services Ordered Date", "sClosingServOd", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Closing Services Due Date", "sClosingServDueD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Closing Services Document Date", "sClosingServDocumentD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Closing Services Received Date", "sClosingServRd", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Closing Services Comments", "sClosingServN", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_LoanStatus, "Appraisal Report Ordered Date", "sApprRprtOd", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Appraisal Report Due Date", "sApprRprtDueD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Appraisal Report Received Date", "sApprRprtRd", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Appraisal Submitted to UCDP Date", "sSpSubmittedToUcdpD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy"); // OPM 80416
                eB += m_Schema.Add(Category_LoanStatus, "Appraisal Report Comments", "sApprRprtN", Field.E_FieldType.Str, Field.E_ClassType.LongText);
                eB += m_Schema.Add(Category_LoanStatus, "App Submitted Date", "sAppSubmittedD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy"); //OPM 53478 02/04/11
                eB += m_Schema.Add(Category_LoanStatus, "App Received by Lender Date", "sAppReceivedByLenderD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "App Submitted Comments", "sAppSubmittedN", Field.E_FieldType.Str, Field.E_ClassType.Txt); // OPM 80416 gf
                eB += m_Schema.Add(Category_LoanStatus, "Intent to Proceed Date", "sIntentToProceedD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy"); //OPM 213437
                eB += m_Schema.Add(Category_LoanStatus, "Intent to Proceed Comments", "sIntentToProceedN", Field.E_FieldType.Str, Field.E_ClassType.Txt); //OPM 213437
				eB += m_Schema.Add(Category_LoanStatus, "TIL/GFE Ordered Date", "sTilGfeOd", Field.E_FieldType.Dtm , Field.E_ClassType.Cal, "M/d/yyyy");
				eB += m_Schema.Add(Category_LoanStatus, "TIL/GFE Due Date", "sTilGfeDueD", Field.E_FieldType.Dtm , Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "TIL/GFE Document Date", "sTilGfeDocumentD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy"); // OPM 80416 gf
                eB += m_Schema.Add(Category_LoanStatus, "TIL/GFE Received Date", "sTilGfeRd", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
				eB += m_Schema.Add(Category_LoanStatus, "TIL/GFE Comments", "sTilGfeN", Field.E_FieldType.Str , Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_LoanStatus, "Flood Certificate Ordered Date", "sFloodCertOd", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Flood Certificate Due Date", "sFloodCertDueD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Flood Certificate Received Date", "sFloodCertRd", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Flood Certificate Comments", "sFloodCertN", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_LoanStatus, "USPS Check Ordered Date", "sUSPSCheckOd", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "USPS Check Due Date", "sUSPSCheckDueD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "USPS Check Document Date", "sUSPSCheckDocumentD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "USPS Check Received Date", "sUSPSCheckRd", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "USPS Check Comments", "sUSPSCheckN", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_LoanStatus, "Payoff/Demand Statement Ordered Date", "sPayDemStmntOd", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Payoff/Demand Statement Due Date", "sPayDemStmntDueD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Payoff/Demand Statement Document Date", "sPayDemStmntDocumentD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Payoff/Demand Statement Received Date", "sPayDemStmntRd", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Payoff/Demand Statement Comments", "sPayDemStmntN", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_LoanStatus, "CAIVRS Ordered Date", "sCAIVRSOd", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "CAIVRS Due Date", "sCAIVRSDueD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "CAIVRS Document Date", "sCAIVRSDocumentD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "CAIVRS Received Date", "sCAIVRSRd", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "CAIVRS Comments", "sCAIVRSN", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_LoanStatus, "Fraud Services Ordered Date", "sFraudServOd", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Fraud Services Due Date", "sFraudServDueD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Fraud Services Document Date", "sFraudServDocumentD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Fraud Services Received Date", "sFraudServRd", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Fraud Services Comments", "sFraudServN", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_LoanStatus, "AVM Ordered Date", "sAVMOd", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "AVM Due Date", "sAVMDueD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "AVM Document Date", "sAVMDocumentD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "AVM Received Date", "sAVMRd", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "AVM Comments", "sAVMN", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_LoanStatus, "HOA Certification Ordered Date", "sHOACertOd", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "HOA Certification Due Date", "sHOACertDueD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "HOA Certification Document Date", "sHOACertDocumentD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "HOA Certification Received Date", "sHOACertRd", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "HOA Certification Comments", "sHOACertN", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_LoanStatus, "Estimated HUD Ordered Date", "sEstHUDOd", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Estimated HUD Due Date", "sEstHUDDueD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Estimated HUD Document Date", "sEstHUDDocumentD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Estimated HUD Received Date", "sEstHUDRd", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Estimated HUD Comments", "sEstHUDN", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_LoanStatus, "CPL/ICL Ordered Date", "sCplAndIclOd", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "CPL/ICL Due Date", "sCplAndIclDueD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "CPL/ICL Document Date", "sCplAndIclDocumentD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "CPL/ICL Received Date", "sCplAndIclRd", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "CPL/ICL Comments", "sCplAndIclN", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_LoanStatus, "Wire Instructions Ordered Date", "sWireInstructOd", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Wire Instructions Due Date", "sWireInstructDueD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Wire Instructions Document Date", "sWireInstructDocumentD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Wire Instructions Received Date", "sWireInstructRd", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Wire Instructions Comments", "sWireInstructN", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_LoanStatus, "Insurance(s) Ordered Date", "sInsurancesOd", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Insurance(s) Due Date", "sInsurancesDueD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Insurance(s) Document Date", "sInsurancesDocumentD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Insurance(s) Received Date", "sInsurancesRd", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Insurance(s) Comments", "sInsurancesN", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                



                eB += m_Schema.Add(Category_LoanStatus, "Pre-Processing Date", "sPreProcessingD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Pre-Processing Date - Time", "sPreProcessingDTime", "sPreProcessingD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "hh:mm tt");
                eB += m_Schema.Add(Category_LoanStatus, "Pre-Processing Comments", "sPreProcessingN", Field.E_FieldType.Str, Field.E_ClassType.LongText);

                eB += m_Schema.Add(Category_LoanStatus, "Document Check Date", "sDocumentCheckD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Document Check Date - Time", "sDocumentCheckDTime", "sDocumentCheckD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "hh:mm tt");
                eB += m_Schema.Add(Category_LoanStatus, "Document Check Comments", "sDocumentCheckN", Field.E_FieldType.Str, Field.E_ClassType.LongText);

                eB += m_Schema.Add(Category_LoanStatus, "Document Check Failed Date", "sDocumentCheckFailedD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Document Check Failed Date - Time", "sDocumentCheckFailedDTime", "sDocumentCheckFailedD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "hh:mm tt");
                eB += m_Schema.Add(Category_LoanStatus, "Document Check Failed Comments", "sDocumentCheckFailedN", Field.E_FieldType.Str, Field.E_ClassType.LongText);

                eB += m_Schema.Add(Category_LoanStatus, "Pre-Underwriting Date", "sPreUnderwritingD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Pre-Underwriting Date - Time", "sPreUnderwritingDTime", "sPreUnderwritingD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "hh:mm tt");
                eB += m_Schema.Add(Category_LoanStatus, "Pre-Underwriting Comments", "sPreUnderwritingN", Field.E_FieldType.Str, Field.E_ClassType.LongText);

                eB += m_Schema.Add(Category_LoanStatus, "Condition Review Date", "sConditionReviewD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Condition Review Date - Time", "sConditionReviewDTime", "sConditionReviewD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "hh:mm tt");
                eB += m_Schema.Add(Category_LoanStatus, "Condition Review Comments", "sConditionReviewN", Field.E_FieldType.Str, Field.E_ClassType.LongText);

                eB += m_Schema.Add(Category_LoanStatus, "Pre-Doc QC Date", "sPreDocQCD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Pre-Doc QC Date - Time", "sPreDocQCDTime", "sPreDocQCD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "hh:mm tt");
                eB += m_Schema.Add(Category_LoanStatus, "Pre-Doc QC Comments", "sPreDocQCN", Field.E_FieldType.Str, Field.E_ClassType.LongText);

                eB += m_Schema.Add(Category_LoanStatus, "Docs Ordered Date", "sDocsOrderedD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Docs Ordered Date - Time", "sDocsOrderedDTime", "sDocsOrderedD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "hh:mm tt");
                eB += m_Schema.Add(Category_LoanStatus, "Docs Ordered Comments", "sDocsOrderedN", Field.E_FieldType.Str, Field.E_ClassType.LongText);

                eB += m_Schema.Add(Category_LoanStatus, "Docs Drawn Date", "sDocsDrawnD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Docs Drawn Date - Time", "sDocsDrawnDTime", "sDocsDrawnD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "hh:mm tt");
                eB += m_Schema.Add(Category_LoanStatus, "Docs Drawn Comments", "sDocsDrawnN", Field.E_FieldType.Str, Field.E_ClassType.LongText);

                eB += m_Schema.Add(Category_LoanStatus, "Ready For Sale Date", "sReadyForSaleD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Ready For Sale Date - Time", "sReadyForSaleDTime", "sReadyForSaleD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "hh:mm tt");
                eB += m_Schema.Add(Category_LoanStatus, "Ready For Sale Comments", "sReadyForSaleN", Field.E_FieldType.Str, Field.E_ClassType.LongText);

                eB += m_Schema.Add(Category_LoanStatus, "Submitted For Purchase Review Date", "sSubmittedForPurchaseReviewD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Submitted For Purchase Review Date - Time", "sSubmittedForPurchaseReviewDTime", "sSubmittedForPurchaseReviewD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "hh:mm tt");
                eB += m_Schema.Add(Category_LoanStatus, "Submitted For Purchase Review Comments", "sSubmittedForPurchaseReviewN", Field.E_FieldType.Str, Field.E_ClassType.LongText);

                eB += m_Schema.Add(Category_LoanStatus, "In Purchase Review Date", "sInPurchaseReviewD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "In Purchase Review Date - Time", "sInPurchaseReviewDTime", "sInPurchaseReviewD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "hh:mm tt");
                eB += m_Schema.Add(Category_LoanStatus, "In Purchase Review Comments", "sInPurchaseReviewN", Field.E_FieldType.Str, Field.E_ClassType.LongText);

                eB += m_Schema.Add(Category_LoanStatus, "Pre-Purchase Conditions Date", "sPrePurchaseConditionsD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Pre-Purchase Conditions Date - Time", "sPrePurchaseConditionsDTime", "sPrePurchaseConditionsD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "hh:mm tt");
                eB += m_Schema.Add(Category_LoanStatus, "Pre-Purchase Conditions Comments", "sPrePurchaseConditionsN", Field.E_FieldType.Str, Field.E_ClassType.LongText);

                eB += m_Schema.Add(Category_LoanStatus, "Submitted For Final Purchase Review Date", "sSubmittedForFinalPurchaseD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Submitted For Final Purchase Review Date - Time", "sSubmittedForFinalPurchaseDTime", "sSubmittedForFinalPurchaseD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "hh:mm tt");
                eB += m_Schema.Add(Category_LoanStatus, "Submitted For Final Purchase Review Comments", "sSubmittedForFinalPurchaseN", Field.E_FieldType.Str, Field.E_ClassType.LongText);

                eB += m_Schema.Add(Category_LoanStatus, "In Final Purchase Review Date", "sInFinalPurchaseReviewD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "In Final Purchase Review Date - Time", "sInFinalPurchaseReviewDTime", "sInFinalPurchaseReviewD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "hh:mm tt");
                eB += m_Schema.Add(Category_LoanStatus, "In Final Purchase Review Comments", "sInFinalPurchaseReviewN", Field.E_FieldType.Str, Field.E_ClassType.LongText);

                eB += m_Schema.Add(Category_LoanStatus, "Clear To Purchase Date", "sClearToPurchaseD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Clear To Purchase Date - Time", "sClearToPurchaseDTime", "sClearToPurchaseD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "hh:mm tt");
                eB += m_Schema.Add(Category_LoanStatus, "Clear To Purchase Comments", "sClearToPurchaseN", Field.E_FieldType.Str, Field.E_ClassType.LongText);

                eB += m_Schema.Add(Category_LoanStatus, "Loan Purchased Date", "sPurchasedD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Loan Purchased Date - Time", "sPurchasedDTime", "sPurchasedD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "hh:mm tt");
                eB += m_Schema.Add(Category_LoanStatus, "Loan Purchased Comments", "sPurchasedN", Field.E_FieldType.Str, Field.E_ClassType.LongText);

                eB += m_Schema.Add(Category_LoanStatus, "Counter Offer Approved Date", "sCounterOfferD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Counter Offer Approved Date - Time", "sCounterOfferDTime", "sCounterOfferD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "hh:mm tt");
                eB += m_Schema.Add(Category_LoanStatus, "Counter Offer Approved Comments", "sCounterOfferN", Field.E_FieldType.Str, Field.E_ClassType.LongText);

                eB += m_Schema.Add(Category_LoanStatus, "Loan Withdrawn Date", "sWithdrawnD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Loan Withdrawn Date - Time", "sWithdrawnDTime", "sWithdrawnD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "hh:mm tt");
                eB += m_Schema.Add(Category_LoanStatus, "Loan Withdrawn Comments", "sWithdrawnN", Field.E_FieldType.Str, Field.E_ClassType.LongText);

                eB += m_Schema.Add(Category_LoanStatus, "Loan Archived Date", "sArchivedD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Loan Archived Date - Time", "sArchivedDTime", "sArchivedD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "hh:mm tt");
                eB += m_Schema.Add(Category_LoanStatus, "Loan Archived Comments", "sArchivedN", Field.E_FieldType.Str, Field.E_ClassType.LongText);

                eB += m_Schema.Add(Category_LoanStatus, "Lead Declined Date", "sLeadDeclinedD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyy");
                eB += m_Schema.Add(Category_LoanStatus, "Lead Canceled Date", "sLeadCanceledD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyy");
                eB += m_Schema.Add(Category_LoanStatus, "Lead Other Date", "sLeadOtherD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyy");


                //OPM 51484
                eB += m_Schema.Add(Category_LoanStatus, "Custom Document #1 Description", "sU1DocStatDesc", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_LoanStatus, "Custom Document #1 Ordered Date", "sU1DocStatOd", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Custom Document #1 Due Date", "sU1DocStatDueD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Custom Document #1 Document Date", "sU1DocDocumentD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy"); // OPM 80416 gf
                eB += m_Schema.Add(Category_LoanStatus, "Custom Document #1 Received Date", "sU1DocStatRd", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Custom Document #1 Comments", "sU1DocStatN", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_LoanStatus, "Custom Document #2 Description", "sU2DocStatDesc", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_LoanStatus, "Custom Document #2 Ordered Date", "sU2DocStatOd", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Custom Document #2 Due Date", "sU2DocStatDueD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Custom Document #2 Document Date", "sU2DocDocumentD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy"); // OPM 80416 gf
                eB += m_Schema.Add(Category_LoanStatus, "Custom Document #2 Received Date", "sU2DocStatRd", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Custom Document #2 Comments", "sU2DocStatN", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_LoanStatus, "Custom Document #3 Description", "sU3DocStatDesc", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_LoanStatus, "Custom Document #3 Ordered Date", "sU3DocStatOd", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Custom Document #3 Due Date", "sU3DocStatDueD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Custom Document #3 Document Date", "sU3DocDocumentD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy"); // OPM 80416 gf
                eB += m_Schema.Add(Category_LoanStatus, "Custom Document #3 Received Date", "sU3DocStatRd", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Custom Document #3 Comments", "sU3DocStatN", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_LoanStatus, "Custom Document #4 Description", "sU4DocStatDesc", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_LoanStatus, "Custom Document #4 Ordered Date", "sU4DocStatOd", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Custom Document #4 Due Date", "sU4DocStatDueD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Custom Document #4 Document Date", "sU4DocDocumentD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy"); // OPM 80416 gf
                eB += m_Schema.Add(Category_LoanStatus, "Custom Document #4 Received Date", "sU4DocStatRd", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Custom Document #4 Comments", "sU4DocStatN", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_LoanStatus, "Custom Document #5 Description", "sU5DocStatDesc", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_LoanStatus, "Custom Document #5 Ordered Date", "sU5DocStatOd", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Custom Document #5 Due Date", "sU5DocStatDueD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Custom Document #5 Document Date", "sU5DocDocumentD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy"); // OPM 80416 gf
                eB += m_Schema.Add(Category_LoanStatus, "Custom Document #5 Received Date", "sU5DocStatRd", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Custom Document #5 Comments", "sU5DocStatN", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                // OPM 80416 gf Added Loan Package through Collateral Package
                eB += m_Schema.Add(Category_LoanStatus, "Loan Package Ordered Date", "sLoanPackageOrderedD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Loan Package Document Date", "sLoanPackageDocumentD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Loan Package Received Date", "sLoanPackageReceivedD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Loan Package Comments", "sLoanPackageN", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_LoanStatus, "Loan Package Shipped Date", "sLoanPackageShippedD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");

                eB += m_Schema.Add(Category_LoanStatus, "Mortgage / Deed of Trust Ordered Date", "sMortgageDOTOrderedD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Mortgage / Deed of Trust Document Date", "sMortgageDOTDocumentD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Mortgage / Deed of Trust Received Date", "sMortgageDOTReceivedD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Mortgage / Deed of Trust Comments", "sMortgageDOTN", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_LoanStatus, "Mortgage / Deed of Trust Shipped Date", "sMortgageDOTShippedD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");

                eB += m_Schema.Add(Category_LoanStatus, "Note Ordered Date", "sNoteOrderedD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy"); // OPM 80416 gf                
                eB += m_Schema.Add(Category_LoanStatus, "Note Document Date", "sDocumentNoteD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Note Received Date", "sNoteReceivedD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy"); // OPM 80416 gf
                eB += m_Schema.Add(Category_LoanStatus, "Note Comments", "sNoteN", Field.E_FieldType.Str, Field.E_ClassType.Txt); // OPM 80416 gf
                eB += m_Schema.Add(Category_LoanStatus, "Note Shipped Date", "sNoteShippedD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy"); // OPM 80416 gf

                eB += m_Schema.Add(Category_LoanStatus, "Final HUD-1 Settlement Stmt Ordered Date", "sFinalHUD1SttlmtStmtOrderedD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Final HUD-1 Settlement Stmt Document Date", "sFinalHUD1SttlmtStmtDocumentD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Final HUD-1 Settlement Stmt Received Date", "sFinalHUD1SttlmtStmtReceivedD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Final HUD-1 Settlement Stmt Comments", "sFinalHUD1SttlmtStmtN", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_LoanStatus, "Final HUD-1 Settlement Stmt Shipped Date", "sFinalHUD1SttlmtStmtShippedD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");

                eB += m_Schema.Add(Category_LoanStatus, "Title Ins. Policy Ordered Date", "sTitleInsPolicyOrderedD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Title Ins. Policy Document Date", "sTitleInsPolicyDocumentD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Title Ins. Policy Received Date", "sTitleInsPolicyReceivedD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Title Ins. Policy Comments", "sTitleInsPolicyN", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_LoanStatus, "Title Ins. Policy Shipped Date", "sTitleInsPolicyShippedD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");

                eB += m_Schema.Add(Category_LoanStatus, "Mortgage Ins. Certificate Ordered Date", "sMiCertOrderedD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Mortgage Ins. Certificate Received Date", "sMiCertReceivedD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Mortgage Ins. Certificate Comments", "sMiCertN", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_LoanStatus, "Mortgage Ins. Certificate Shipped Date", "sMiCertShippedD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");

                eB += m_Schema.Add(Category_LoanStatus, "Final Hazard Ins. Policy Ordered Date", "sFinalHazInsPolicyOrderedD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Final Hazard Ins. Policy Document Date", "sFinalHazInsPolicyDocumentD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Final Hazard Ins. Policy Received Date", "sFinalHazInsPolicyReceivedD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Final Hazard Ins. Policy Comments", "sFinalHazInsPolicyN", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_LoanStatus, "Final Hazard Ins. Policy Shipped Date", "sFinalHazInsPolicyShippedD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");

                eB += m_Schema.Add(Category_LoanStatus, "Final Flood Ins. Policy Ordered Date", "sFinalFloodInsPolicyOrderedD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Final Flood Ins. Policy Document Date", "sFinalFloodInsPolicyDocumentD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Final Flood Ins. Policy Received Date", "sFinalFloodInsPolicyReceivedD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Final Flood Ins. Policy Comments", "sFinalFloodInsPolicyN", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_LoanStatus, "Final Flood Ins. Policy Shipped Date", "sFinalFloodInsPolicyShippedD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");

                eB += m_Schema.Add(Category_LoanStatus, "Collateral Package Ordered Date", "sCollateralPkgOrderedD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Collateral Package Shipped Date", "sCollateralPkgShippedD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");

                //OPM 149891
                eB += m_Schema.Add(Category_LoanStatus, "Collateral Package Shipping Method", "sCollateralPkgMethod", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_LoanStatus, "Collateral Package Tracking", "sCollateralPkgTracking", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                //Case 67950
                eB += m_Schema.Add(Category_LoanStatus, "Credit Report Comments", "aCrN", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                //OPM 61860
                eB += m_Schema.Add(Category_LoanStatus, "Custom Report Type #1 Description", "aU1DocStatDesc", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_LoanStatus, "Custom Report Type #1 Ordered Date", "aU1DocStatOd", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Custom Report Type #1 Due Date", "aU1DocStatDueD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Custom Report Type #1 Received Date", "aU1DocStatRd", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Custom Report Type #1 Comments", "aU1DocStatN", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_LoanStatus, "Custom Report Type #2 Description", "aU2DocStatDesc", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_LoanStatus, "Custom Report Type #2 Ordered Date", "aU2DocStatOd", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Custom Report Type #2 Due Date", "aU2DocStatDueD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Custom Report Type #2 Received Date", "aU2DocStatRd", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Custom Report Type #2 Comments", "aU2DocStatN", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_LoanStatus, "DocMagic Document Date", "sDocMagicDocumentD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Document Expiration Date", "sDocExpirationD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");

                eB += m_Schema.Add(Category_LoanStatus, "Has unscreened docs", "sUnscreenedDocsInFile", Field.E_FieldType.Boo, Field.E_ClassType.Txt
                    , new BoolRepMap(true, "Yes")
                    , new BoolRepMap(false, "No")
                    );
                eB += m_Schema.Add(Category_LoanStatus, "New EDocs Uploaded Date", "sNewEdocsUploadedD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");

                // opm 132968
                eB += m_Schema.Add(Category_LoanStatus, "FHA Case Assignment Date", "sCaseAssignmentD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "UDN Ordered Date", "sUDNOrderedD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "UDN Deactivated Date", "sUDNDeactivatedD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");

                // OPM 226417 - 09/23/2015 - JE
                eB += m_Schema.Add(Category_LoanStatus, "Best Known Closing Date", "sDocMagicClosingD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");

                eB += m_Schema.Add(Category_LoanStatus, "Per-diem Interest Start Date", "sConsummationD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyy");
                eB += m_Schema.Add(Category_LoanStatus, "Disbursement Date", "sDocMagicDisbursementD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");

                eB += m_Schema.Add(Category_LoanStatus, "QC Completed Date", "sQCCompDate", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "QC Completed Date - Time", "sQCCompDateTime", "sQCCompDate", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "hh:mm tt");

                // OPM 367039
                eB += m_Schema.Add(Category_LoanStatus, "Prelim Report Expiration Date", "sPrelimRprtExpD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_LoanStatus, "Pre-Processing Touches", "sNumTouchesPreProcessing", Field.E_FieldType.Int, Field.E_ClassType.Cnt, "#,#;-#,#;0");
                eB += m_Schema.Add(Category_LoanStatus, "Processing Touches", "sNumTouchesProcessing", Field.E_FieldType.Int, Field.E_ClassType.Cnt, "#,#;-#,#;0");
                eB += m_Schema.Add(Category_LoanStatus, "Submitted Touches", "sNumTouchesSubmitted", Field.E_FieldType.Int, Field.E_ClassType.Cnt, "#,#;-#,#;0");
                eB += m_Schema.Add(Category_LoanStatus, "Condition Review Touches", "sNumTouchesConditionReview", Field.E_FieldType.Int, Field.E_ClassType.Cnt, "#,#;-#,#;0");

                #endregion

                #region ( Commission Fields )

                eB += m_Schema.Add(Category_Commissions, "Gross Profit", "sGrossProfit", Field.E_FieldType.Dec , Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_Commissions, "Commission Total", "sCommissionTotal", Field.E_FieldType.Dec , Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_Commissions, "Net Profit", "sNetProfit", Field.E_FieldType.Dec , Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_Commissions, "Accounting Period Month", "sAccPeriodMon", Field.E_FieldType.Int , Field.E_ClassType.Txt
                    , new IntegerRepMap( 1  , "January"   )
                    , new IntegerRepMap( 2  , "February"  )
                    , new IntegerRepMap( 3  , "March"     )
                    , new IntegerRepMap( 4  , "April"     )
                    , new IntegerRepMap( 5  , "May"       )
                    , new IntegerRepMap( 6  , "June"      )
                    , new IntegerRepMap( 7  , "July"      )
                    , new IntegerRepMap( 8  , "August"    )
                    , new IntegerRepMap( 9  , "September" )
                    , new IntegerRepMap( 10 , "October"   )
                    , new IntegerRepMap( 11 , "November"  )
                    , new IntegerRepMap( 12 , "December"  )
                    );

                eB += m_Schema.Add(Category_Commissions,"Accounting Period Year", "sAccPeriodYr", Field.E_FieldType.Int , Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_Commissions, "Probability of Closing", "sCloseProbability", Field.E_FieldType.Dec , Field.E_ClassType.Pct, "N3");
                eB += m_Schema.Add(Category_Commissions, "Probable Profit", "sProbableProfit", Field.E_FieldType.Dec , Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_Commissions, "Borrower Points", "sProfitGrossBor", Field.E_FieldType.Dec , Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_Commissions, "Disclosed broker commission", "sBrokerCommissionsTotal", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");

                //OPM 55072
                eB += m_Schema.Add(Category_Commissions, "Official Loan Officer Commission", "sAgentLoanOfficerCommission", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_Commissions, "Official Loan Officer Commission Paid", "sAgentLoanOfficerCommissionPaid", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_Commissions, "Official Manager Commission", "sAgentManagerCommission", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_Commissions, "Official Manager Commission Paid", "sAgentManagerCommissionPaid", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_Commissions, "Official Underwriter Commission", "sAgentUnderwriterCommission", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_Commissions, "Official Underwriter Commission Paid", "sAgentUnderwriterCommissionPaid", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_Commissions, "Official Processor Commission", "sAgentProcessorCommission", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_Commissions, "Official Processor Commission Paid", "sAgentProcessorCommissionPaid", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                                
                //OPM 63883 : Originator Comp: Custom reports fields                
                eB += m_Schema.Add(Category_Commissions, "Originator Compensation Amount", "sOriginatorCompensationAmount", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_Commissions, "Originator Compensation Paid By", "sOriginatorCompensationPaymentSourceT", Field.E_FieldType.Enu, Field.E_ClassType.Txt
                    , new EnumRepMap(E_sOriginatorCompensationPaymentSourceT.LenderPaid, "Lender Paid")
                    , new EnumRepMap(E_sOriginatorCompensationPaymentSourceT.BorrowerPaid, "Borrower Paid")
                    , new EnumRepMap(E_sOriginatorCompensationPaymentSourceT.SourceNotSpecified, "Not Specified")
                    );
                eB += m_Schema.Add(Category_Commissions, "Originator Compensation Set Date", "sOriginatorCompensationPlanAppliedD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_Commissions, "Originator Compensation Plan Effective Date", "sOriginatorCompensationEffectiveD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");

                //OPM 69937
                eB += m_Schema.Add(Category_Commissions, "Originator Base Compensation Percentage", "sOriginatorCompensationPercent", Field.E_FieldType.Dec, Field.E_ClassType.Pct, "N3");
                eB += m_Schema.Add(Category_Commissions, "Originator Compensation Base Amount", "sOriginatorCompensationAdjBaseAmount", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_Commissions, "Originator Compensation Fixed Amount", "sOriginatorCompensationFixedAmount", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
				eB += m_Schema.Add(Category_Commissions, "Total Lender Paid Compensation", "sOriginatorCompensationTotalAmount", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");


				#endregion

                #region ( GFE Fees Fields) 
                //                eB += m_Schema.Add(Category_GfeFees, "801 Loan Origination Fee", "sLOrigF", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_GfeFees, "802 Credit or Charge", "sLDiscnt", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_GfeFees, "802 Credit or Charge Percent", "sLDiscntPc", Field.E_FieldType.Dec, Field.E_ClassType.Pct, "N3");
                eB += m_Schema.Add(Category_GfeFees, "804 Appraisal Fee", "sApprF", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");                
                eB += m_Schema.Add(Category_GfeFees, "805 Credit Report", "sCrF", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");                
                eB += m_Schema.Add(Category_GfeFees, "806 Tax Service Fee", "sTxServF", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_GfeFees, "807 Flood Certification", "sFloodCertificationF", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_GfeFees, "808 Mortgage Broker Fee", "sMBrokF", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_GfeFees, "809 Lender Inspection Fee", "sInspectF", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");                
                eB += m_Schema.Add(Category_GfeFees, "810 Processing Fee", "sProcF", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_GfeFees, "811 Underwriting Fee", "sUwF", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_GfeFees, "812 Wire Transfer", "sWireF", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_GfeFees, "800 Additional Item #1 Description", "s800U1FDesc", Field.E_FieldType.Str, Field.E_ClassType.LongText);
                eB += m_Schema.Add(Category_GfeFees, "800 Additional Item #1 Fee", "s800U1F", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_GfeFees, "800 Additional Item #2 Description", "s800U2FDesc", Field.E_FieldType.Str, Field.E_ClassType.LongText);
                eB += m_Schema.Add(Category_GfeFees, "800 Additional Item #2 Fee", "s800U2F", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_GfeFees, "800 Additional Item #3 Description", "s800U3FDesc", Field.E_FieldType.Str, Field.E_ClassType.LongText);
                eB += m_Schema.Add(Category_GfeFees, "800 Additional Item #3 Fee", "s800U3F", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_GfeFees, "800 Additional Item #4 Description", "s800U4FDesc", Field.E_FieldType.Str, Field.E_ClassType.LongText);
                eB += m_Schema.Add(Category_GfeFees, "800 Additional Item #4 Fee", "s800U4F", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_GfeFees, "800 Additional Item #5 Description", "s800U5FDesc", Field.E_FieldType.Str, Field.E_ClassType.LongText);
                eB += m_Schema.Add(Category_GfeFees, "800 Additional Item #5 Fee", "s800U5F", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_GfeFees, "901 Interest", "sIPia", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_GfeFees, "902 Mortgage Insurance Premium", "sMipPia", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_GfeFees, "903 Haz Ins", "sHazInsPia", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_GfeFees, "904 Description", "s904PiaDesc", Field.E_FieldType.Str, Field.E_ClassType.LongText);
                eB += m_Schema.Add(Category_GfeFees, "904 Fee", "s904Pia", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_GfeFees, "905 VA Funding Fee", "sVaFf", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_GfeFees, "906 Description", "s900U1PiaDesc", Field.E_FieldType.Str, Field.E_ClassType.LongText);
                eB += m_Schema.Add(Category_GfeFees, "906 Fee", "s900U1Pia", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");

                eB += m_Schema.Add(Category_GfeFees, "1008 Fee", "s1006Rsrv", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_GfeFees, "1009 Fee", "s1007Rsrv", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");

                eB += m_Schema.Add(Category_GfeFees, "1102 Closing/Escrow Fee", "sEscrowF", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");//opm 32834 fs 08/20/09
                eB += m_Schema.Add(Category_GfeFees, "1103 Owner's Title Insurance", "sOwnerTitleInsF", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_GfeFees, "1104 Lender's Title Insurance", "sTitleInsF", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_GfeFees, "1109 Doc Preparation Fee", "sDocPrepF", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_GfeFees, "1110 Notary Fees", "sNotaryF", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_GfeFees, "1111 Attorney Fee", "sAttorneyF", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_GfeFees, "1112 Description", "sU1TcDesc", Field.E_FieldType.Str, Field.E_ClassType.LongText);
                eB += m_Schema.Add(Category_GfeFees, "1112 Fee", "sU1Tc", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_GfeFees, "1113 Description", "sU2TcDesc", Field.E_FieldType.Str, Field.E_ClassType.LongText);
                eB += m_Schema.Add(Category_GfeFees, "1113 Fee", "sU2Tc", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_GfeFees, "1114 Description", "sU3TcDesc", Field.E_FieldType.Str, Field.E_ClassType.LongText);
                eB += m_Schema.Add(Category_GfeFees, "1114 Fee", "sU3Tc", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_GfeFees, "1115 Description", "sU4TcDesc", Field.E_FieldType.Str, Field.E_ClassType.LongText);
                eB += m_Schema.Add(Category_GfeFees, "1115 Fee", "sU4Tc", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                
                eB += m_Schema.Add(Category_GfeFees, "1201 Recording Fees", "sRecF", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_GfeFees, "1204 City/County Tax Stamps", "sCountyRtc", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_GfeFees, "1205 State Tax Stamps", "sStateRtc", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_GfeFees, "1206 Description", "sU1GovRtcDesc", Field.E_FieldType.Str, Field.E_ClassType.LongText);
                eB += m_Schema.Add(Category_GfeFees, "1206 Fee", "sU1GovRtc", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_GfeFees, "1207 Description", "sU2GovRtcDesc", Field.E_FieldType.Str, Field.E_ClassType.LongText);
                eB += m_Schema.Add(Category_GfeFees, "1207 Fee", "sU2GovRtc", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_GfeFees, "1208 Description", "sU3GovRtcDesc", Field.E_FieldType.Str, Field.E_ClassType.LongText);
                eB += m_Schema.Add(Category_GfeFees, "1208 Fee", "sU3GovRtc", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");

                eB += m_Schema.Add(Category_GfeFees, "1302 Pest Inspection", "sPestInspectF", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_GfeFees, "1300 Additional Item #1 Description", "sU1ScDesc", Field.E_FieldType.Str, Field.E_ClassType.LongText);
                eB += m_Schema.Add(Category_GfeFees, "1300 Additional Item #1 Fee", "sU1Sc", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_GfeFees, "1300 Additional Item #2 Description", "sU2ScDesc", Field.E_FieldType.Str, Field.E_ClassType.LongText);
                eB += m_Schema.Add(Category_GfeFees, "1300 Additional Item #2 Fee", "sU2Sc", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_GfeFees, "1300 Additional Item #3 Description", "sU3ScDesc", Field.E_FieldType.Str, Field.E_ClassType.LongText);
                eB += m_Schema.Add(Category_GfeFees, "1300 Additional Item #3 Fee", "sU3Sc", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_GfeFees, "1300 Additional Item #4 Description", "sU4ScDesc", Field.E_FieldType.Str, Field.E_ClassType.LongText);
                eB += m_Schema.Add(Category_GfeFees, "1300 Additional Item #4 Fee", "sU4Sc", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_GfeFees, "1300 Additional Item #5 Description", "sU5ScDesc", Field.E_FieldType.Str, Field.E_ClassType.LongText);
                eB += m_Schema.Add(Category_GfeFees, "1300 Additional Item #5 Fee", "sU5Sc", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_GfeFees, "Our Origination Charge", "sGfeOriginationF", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
				eB += m_Schema.Add(Category_GfeFees, "Originator Compensation Percent", "sGfeOriginatorCompFPc", Field.E_FieldType.Dec, Field.E_ClassType.Pct, "N3");
                eB += m_Schema.Add(Category_GfeFees, "Required Services Total Fee", "sGfeRequiredServicesTotalFee", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_GfeFees, "Lender Title Total Fee", "sGfeLenderTitleTotalFee", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_GfeFees, "Owner Title Total Fee", "sGfeOwnerTitleTotalFee", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_GfeFees, "Services You Shop Total Fee", "sGfeServicesYouShopTotalFee", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_GfeFees, "Government Recording Total Fee", "sGfeGovtRecTotalFee", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_GfeFees, "Transfer Tax Total Fee", "sGfeTransferTaxTotalFee", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_GfeFees, "Initial Impound Deposit", "sGfeInitialImpoundDeposit", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_GfeFees, "Daily Interest Total Fee", "sGfeDailyInterestTotalFee", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_GfeFees, "Homeowner Insurance Total Fee", "sGfeHomeOwnerInsuranceTotalFee", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_GfeFees, "Total Estimated Settlement Charges", "sGfeTotalEstimateSettlementCharge", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                #endregion

                #region ( Fee Fields )
                
                eB += m_Schema.Add(Category_Fees, "Total Fees", "sProfitTotalFee", Field.E_FieldType.Dec , Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_Fees, "Commission Rebate Fee", "sProfitRebate", Field.E_FieldType.Dec , Field.E_ClassType.Csh, "C");
                //eB += m_Schema.Add(Category_Fees, "Process Fee", "sProcF", Field.E_FieldType.Dec , Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_Fees, "MSP Fee", "sProfitExternalFee", Field.E_FieldType.Dec , Field.E_ClassType.Csh, "C");
				eB += m_Schema.Add(Category_Fees, "YSP due to Lender", "sDemandYieldSpreadPremiumLenderAmt", Field.E_FieldType.Dec , Field.E_ClassType.Csh, "C"); //opm 23346 fs 09/24/08
				eB += m_Schema.Add(Category_Fees, "Origination Fee", "sLOrigF", Field.E_FieldType.Dec , Field.E_ClassType.Csh, "C");
				eB += m_Schema.Add(Category_Fees, "Origination Fee Percent", "sLOrigFPc", Field.E_FieldType.Dec , Field.E_ClassType.Pct, "N3");
                eB += m_Schema.Add(Category_Fees, "Broker Compensation 1", "sBrokComp1", Field.E_FieldType.Dec , Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_Fees, "Broker Compensation 1 Percent", "sBrokComp1Pc", Field.E_FieldType.Dec , Field.E_ClassType.Pct, "N3");
                eB += m_Schema.Add(Category_Fees, "Broker Compensation 2", "sBrokComp2", Field.E_FieldType.Dec , Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_Fees, "Total Lender Price Adjustments", "sBrokerLockTotalLenderAdj", Field.E_FieldType.Dec, Field.E_ClassType.Pct, "N3");
                eB += m_Schema.Add(Category_Fees, "Fees Paid to Broker", "sTotCcPtb", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_Fees, "Broker Fees Collected", "sBrokerFeesCollected", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_Fees, "Lender Fees Collected", "sLenderFeesCollected", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                
                #endregion

                #region ( TRID Disclosure Fields )

                eB += m_Schema.Add(Category_TRIDDisclosures, "Initial Loan Estimate Created Date", "sInitialLoanEstimateCreatedD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_TRIDDisclosures, "Initial Loan Estimate Issued Date", "sInitialLoanEstimateIssuedD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_TRIDDisclosures, "Initial Loan Estimate Received Date", "sInitialLoanEstimateReceivedD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_TRIDDisclosures, "Initial Loan Estimate Delivery Method", "sInitialLoanEstimateDeliveryMethodT", Field.E_FieldType.Enu, Field.E_ClassType.Txt
                    , new EnumRepMap(E_DeliveryMethodT.LeaveEmpty, "Left Blank")
                    , new EnumRepMap(E_DeliveryMethodT.Email, "Email")
                    , new EnumRepMap(E_DeliveryMethodT.Fax, "Fax")
                    , new EnumRepMap(E_DeliveryMethodT.InPerson, "In Person")
                    , new EnumRepMap(E_DeliveryMethodT.Mail, "Mail")
                    , new EnumRepMap(E_DeliveryMethodT.Overnight, "Overnight")
                    );

                eB += m_Schema.Add(Category_TRIDDisclosures, "Last disclosed Loan Estimate Created Date", "sLastDisclosedLoanEstimateCreatedD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_TRIDDisclosures, "Last disclosed Loan Estimate Issued Date", "sLastDisclosedLoanEstimateIssuedD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_TRIDDisclosures, "Last disclosed Loan Estimate Received Date", "sLastDisclosedLoanEstimateReceivedD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_TRIDDisclosures, "Last disclosed Loan Estimate Delivery Method", "sLastDisclosedLoanEstimateDeliveryMethodT", Field.E_FieldType.Enu, Field.E_ClassType.Txt
                    , new EnumRepMap(E_DeliveryMethodT.LeaveEmpty, "Left Blank")
                    , new EnumRepMap(E_DeliveryMethodT.Email, "Email")
                    , new EnumRepMap(E_DeliveryMethodT.Fax, "Fax")
                    , new EnumRepMap(E_DeliveryMethodT.InPerson, "In Person")
                    , new EnumRepMap(E_DeliveryMethodT.Mail, "Mail")
                    , new EnumRepMap(E_DeliveryMethodT.Overnight, "Overnight")
                    );

                eB += m_Schema.Add(Category_TRIDDisclosures, "Initial Closing Disclosure Created Date", "sInitialClosingDisclosureCreatedD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_TRIDDisclosures, "Initial Closing Disclosure Issued Date", "sInitialClosingDisclosureIssuedD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_TRIDDisclosures, "Initial Closing Disclosure Received Date", "sInitialClosingDisclosureReceivedD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_TRIDDisclosures, "Initial Closing Disclosure Delivery Method", "sInitialClosingDisclosureDeliveryMethodT", Field.E_FieldType.Enu, Field.E_ClassType.Txt
                    , new EnumRepMap(E_DeliveryMethodT.LeaveEmpty, "Left Blank")
                    , new EnumRepMap(E_DeliveryMethodT.Email, "Email")
                    , new EnumRepMap(E_DeliveryMethodT.Fax, "Fax")
                    , new EnumRepMap(E_DeliveryMethodT.InPerson, "In Person")
                    , new EnumRepMap(E_DeliveryMethodT.Mail, "Mail")
                    , new EnumRepMap(E_DeliveryMethodT.Overnight, "Overnight")
                    );

                eB += m_Schema.Add(Category_TRIDDisclosures, "Preview Closing Disclosure Created Date", "sPreviewClosingDisclosureCreatedD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_TRIDDisclosures, "Preview Closing Disclosure Issued Date", "sPreviewClosingDisclosureIssuedD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_TRIDDisclosures, "Preview Closing Disclosure Received Date", "sPreviewClosingDisclosureReceivedD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_TRIDDisclosures, "Preview Closing Disclosure Delivery Method", "sPreviewClosingDisclosureDeliveryMethodT", Field.E_FieldType.Enu, Field.E_ClassType.Txt
                    , new EnumRepMap(E_DeliveryMethodT.LeaveEmpty, "Left Blank")
                    , new EnumRepMap(E_DeliveryMethodT.Email, "Email")
                    , new EnumRepMap(E_DeliveryMethodT.Fax, "Fax")
                    , new EnumRepMap(E_DeliveryMethodT.InPerson, "In Person")
                    , new EnumRepMap(E_DeliveryMethodT.Mail, "Mail")
                    , new EnumRepMap(E_DeliveryMethodT.Overnight, "Overnight")
                    );

                eB += m_Schema.Add(Category_TRIDDisclosures, "Final Closing Disclosure Created Date", "sFinalClosingDisclosureCreatedD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_TRIDDisclosures, "Final Closing Disclosure Issued Date", "sFinalClosingDisclosureIssuedD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_TRIDDisclosures, "Final Closing Disclosure Received Date", "sFinalClosingDisclosureReceivedD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_TRIDDisclosures, "Final Closing Disclosure Delivery Method", "sFinalClosingDisclosureDeliveryMethodT", Field.E_FieldType.Enu, Field.E_ClassType.Txt
                    , new EnumRepMap(E_DeliveryMethodT.LeaveEmpty, "Left Blank")
                    , new EnumRepMap(E_DeliveryMethodT.Email, "Email")
                    , new EnumRepMap(E_DeliveryMethodT.Fax, "Fax")
                    , new EnumRepMap(E_DeliveryMethodT.InPerson, "In Person")
                    , new EnumRepMap(E_DeliveryMethodT.Mail, "Mail")
                    , new EnumRepMap(E_DeliveryMethodT.Overnight, "Overnight")
                    );

                eB += m_Schema.Add(Category_TRIDDisclosures, "Last Closing Disclosure Before Consummation Created Date", "sLastClosingDisclosureBeforeConsummationCreatedD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_TRIDDisclosures, "Last Closing Disclosure Before Consummation Issued Date", "sLastClosingDisclosureBeforeConsummationIssuedD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_TRIDDisclosures, "Last Closing Disclosure Before Consummation Received Date", "sLastClosingDisclosureBeforeConsummationReceivedD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_TRIDDisclosures, "Last Closing Disclosure Before Consummation Delivery Method ", "sLastClosingDisclosureBeforeConsummationDeliveryMethodT", Field.E_FieldType.Enu, Field.E_ClassType.Txt
                    , new EnumRepMap(E_DeliveryMethodT.LeaveEmpty, "Left Blank")
                    , new EnumRepMap(E_DeliveryMethodT.Email, "Email")
                    , new EnumRepMap(E_DeliveryMethodT.Fax, "Fax")
                    , new EnumRepMap(E_DeliveryMethodT.InPerson, "In Person")
                    , new EnumRepMap(E_DeliveryMethodT.Mail, "Mail")
                    , new EnumRepMap(E_DeliveryMethodT.Overnight, "Overnight")
                    );

                // OPM 225000, 9/8/2015, ML
                eB += m_Schema.Add(Category_TRIDDisclosures, "Deadline (from Closing Date) to Mail or Deliver Initial Loan Estimate", "sIntialLoanEstimateMailedOrReceivedDeadlineForCurrentClosingD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_TRIDDisclosures, "Deadline to Mail Initial Closing Disclosure", "sInitialClosingDisclosureMailedDeadlineD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_TRIDDisclosures, "Deadline for Borrower to Receive Initial Closing Disclosure", "sInitialClosingDisclosureReceivedDeadlineD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");

                // OPM 226117, 9/17/2015, ML
                eB += m_Schema.Add(Category_TRIDDisclosures, "Estimated Closing Costs Expiration Date", "sLoanEstScAvailTillD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");

                // OPM 226417 - 09/23/2015 - JE
                eB += m_Schema.Add(Category_TRIDDisclosures, "Deadline for Borrower to Receive Revised Loan Estimate", "sRevisedLoanEstimateReceivedDeadlineForCurrentClosingD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");

                // OPM 232029 - 11/11/2015 - JE
                eB += m_Schema.Add(Category_TRIDDisclosures, "Tolerance Cure - 0%", "sToleranceZeroPercentCure", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_TRIDDisclosures, "Tolerance Cure - 10%", "sToleranceTenPercentCure", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_TRIDDisclosures, "Tolerance Cure - Total", "sToleranceCure", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");

                eB += m_Schema.Add(Category_TRIDDisclosures, "Deadline to Mail or Deliver Initial Loan Estimate", "sInitialLoanEstimateMailedOrReceivedDeadlineForCurrentAppD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");

                eB += m_Schema.Add(Category_TRIDDisclosures, "Has Completed Homeowner Counseling Organization List", "sHasCompletedHomeownerCounselingOrganizationList", Field.E_FieldType.Boo, Field.E_ClassType.Txt
                    , new BoolRepMap(true, "Yes")
                    , new BoolRepMap(false, "No")
                    );

                eB += m_Schema.Add(Category_TRIDDisclosures, "Status for Originator Portal Request for Initial Disclosure Generation", "sTpoRequestForInitialDisclosureGenerationStatusT", Field.E_FieldType.Enu, Field.E_ClassType.Txt
                    , new EnumRepMap(TpoRequestForInitialDisclosureGenerationEventType.NoRequest, "None / NA")
                    , new EnumRepMap(TpoRequestForInitialDisclosureGenerationEventType.Requested, "Active")
                    , new EnumRepMap(TpoRequestForInitialDisclosureGenerationEventType.Cancelled, "Cancelled")
                    , new EnumRepMap(TpoRequestForInitialDisclosureGenerationEventType.Completed, "Completed")
                    );

                eB += m_Schema.Add(Category_TRIDDisclosures, "Originator Portal Request for Initial Disclosure Generation Status Date", "sTpoRequestForInitialDisclosureGenerationStatusD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");

                eB += m_Schema.Add(Category_TRIDDisclosures, "Originator Portal Request for Initial Disclosure Generation Status Date - Time", "sTpoRequestForInitialDisclosureGenerationStatusDTime", "sTpoRequestForInitialDisclosureGenerationStatusD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "hh:mm tt");

                eB += m_Schema.Add(Category_TRIDDisclosures, "Has Active Originator Portal Request for Redisclosure", "sHasActiveTpoRequestForRedisclosure", Field.E_FieldType.Boo, Field.E_ClassType.Txt
                    , new BoolRepMap(true, "Yes")
                    , new BoolRepMap(false, "No")
                    );

                eB += m_Schema.Add(Category_TRIDDisclosures, "Earliest Active Originator Portal Request for Redisclosure Status Date", "sEarliestActiveTpoRequestForRedisclosureStatusD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");

                eB += m_Schema.Add(Category_TRIDDisclosures, "Earliest Active Originator Portal Request for Redisclosure Status Date - Time", "sEarliestActiveTpoRequestForRedisclosureStatusDTime", "sEarliestActiveTpoRequestForRedisclosureStatusD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "hh:mm tt");

                eB += m_Schema.Add(Category_TRIDDisclosures, "Status for Originator Portal Request for Initial Closing Disclosure Generation", "sTpoRequestForInitialClosingDisclosureGenerationStatusT", Field.E_FieldType.Enu, Field.E_ClassType.Txt
                    , new EnumRepMap(TpoRequestForInitialClosingDisclosureGenerationEventType.NoRequest, "None / NA")
                    , new EnumRepMap(TpoRequestForInitialClosingDisclosureGenerationEventType.Active, "Active")
                    , new EnumRepMap(TpoRequestForInitialClosingDisclosureGenerationEventType.Cancelled, "Cancelled")
                    , new EnumRepMap(TpoRequestForInitialClosingDisclosureGenerationEventType.Completed, "Completed")
                    );

                eB += m_Schema.Add(Category_TRIDDisclosures, "Originator Portal Request for Initial Closing Disclosure Generation Status Date", "sTpoRequestForInitialClosingDisclosureGenerationStatusD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");

                eB += m_Schema.Add(Category_TRIDDisclosures, "Originator Portal Request for Initial Closing Disclosure Generation Status Date - Time", "sTpoRequestForInitialClosingDisclosureGenerationStatusDTime", "sTpoRequestForInitialClosingDisclosureGenerationStatusD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "hh:mm tt");

                eB += m_Schema.Add(Category_TRIDDisclosures, "TRID Target Regulation Version", "sTridTargetRegulationVersionT", Field.E_FieldType.Enu, Field.E_ClassType.Txt
                    , new EnumRepMap(TridTargetRegulationVersionT.Blank, string.Empty)
                    , new EnumRepMap(TridTargetRegulationVersionT.TRID2015, "2015 TRID")
                    , new EnumRepMap(TridTargetRegulationVersionT.TRID2017, "2017 TRID")
                    );
                #endregion

                #region ( Initial Escrow Account Fields )
                // OPM 235653, 1/5/2016, ML
                eB += m_Schema.Add(Category_InitialEscrowAccount, "Mortgage Insurance Reserve Amount", "sMInsRsrv", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_InitialEscrowAccount, "Hazard Insurance Reserve Amount", "sHazInsRsrv", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_InitialEscrowAccount, "Flood Insurance Reserve Amount", "sFloodInsRsrv", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_InitialEscrowAccount, "Windstorm Insurance Reserve Amount", "sWindstormExpenseReserveAmt", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_InitialEscrowAccount, "Condo HO-6 Insurance Reserve Amount", "sCondoHO6ExpenseReserveAmt", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_InitialEscrowAccount, "Real Estate Tax Reserve Amount", "sRealETxRsrv", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_InitialEscrowAccount, "School Tax Reserve Amount", "sSchoolTxRsrv", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_InitialEscrowAccount, "Other Tax 1 Reserve Amount", "sOtherTax1ExpenseReserveAmt", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_InitialEscrowAccount, "Other Tax 2 Reserve Amount", "sOtherTax2ExpenseReserveAmt", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_InitialEscrowAccount, "Other Tax 3 Reserve Amount", "sOtherTax3ExpenseReserveAmt", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_InitialEscrowAccount, "Other Tax 4 Reserve Amount", "sOtherTax4ExpenseReserveAmt", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_InitialEscrowAccount, "HOA Dues Reserve Amount", "sHOADuesExpenseReserveAmt", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_InitialEscrowAccount, "Ground Rent Reserve Amount", "sGroundRentExpenseReserveAmt", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_InitialEscrowAccount, "Aggregate Escrow Adjustment", "sAggregateAdjRsrv", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                #endregion

                #region ( Employee Information Fields )
                //eB += m_Schema.Add(Category_AssignedEmployees, "Branch Name", "sBranchNm", Field.E_FieldType.Str , Field.E_ClassType.Txt);
                //eB += m_Schema.Add(Category_AssignedEmployees, "Branch Code", "sBranchCode", Field.E_FieldType.Str, Field.E_ClassType.Txt); //opm 28432 fs 03/12/09

                eB += m_Schema.Add(Category_AssignedEmployees, "Branch Name", "sBranchNm", "BRANCH.BranchNm", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_AssignedEmployees, "Branch Code", "sBranchCode", "BRANCH.BranchCode",Field.E_FieldType.Str, Field.E_ClassType.Txt); //opm 28432 fs 03/12/09
                eB += m_Schema.Add(Category_AssignedEmployees, "Branch Division", "sBranchDivision", "BRANCH.Division",Field.E_FieldType.Str, Field.E_ClassType.Txt); //opm 174623 gf 4/14/14

                eB += m_Schema.Add(Category_AssignedEmployees, "Assigned Loan Officer Name", "sEmployeeLoanRepName", Field.E_FieldType.Str , Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_AssignedEmployees, "Assigned Loan Officer Team", "sTeamLoanRepName", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_AssignedEmployees, "Assigned Loan Officer Phone", "sEmployeeLoanRepPhone", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_AssignedEmployees, "Assigned Loan Officer Email", "sEmployeeLoanRepEmail", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_AssignedEmployees, "Assigned Loan Officer Start D", "sEmployeeLoanRepStartDate", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_AssignedEmployees, "Assigned Loan Officer Termination", "sEmployeeLoanRepTermination", Field.E_FieldType.Dtm, Field.E_ClassType.Txt, "M/d/yyyy");
                eB += m_Schema.Add(Category_AssignedEmployees, "Assigned Loan Officer Company Tier", "sPmlCompanyTierDesc", Field.E_FieldType.Str, Field.E_ClassType.Txt); // OPM 190542

                eB += m_Schema.Add(Category_AssignedEmployees, "Loan Rep Originating Company ID", "sEmployeeLoanRepCompanyID", "PML_BROKER.CompanyId", Field.E_FieldType.Str, Field.E_ClassType.Txt); //opm 191618 em 10/07/14

                eB += m_Schema.Add(Category_AssignedEmployees, "Assigned Processor (External) Name", "sEmployeeBrokerProcessorName", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_AssignedEmployees, "Assigned Processor (External) Phone", "sEmployeeBrokerProcessorPhone ", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_AssignedEmployees, "Assigned Processor (External) Email", "sEmployeeBrokerProcessorEmail", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_AssignedEmployees, "Assigned Secondary (External) Name", "sEmployeeExternalSecondaryName", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_AssignedEmployees, "Assigned Secondary (External) Phone", "sEmployeeExternalSecondaryPhone ", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_AssignedEmployees, "Assigned Secondary (External) Email", "sEmployeeExternalSecondaryEmail", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_AssignedEmployees, "Assigned Post-Closer (External) Name", "sEmployeeExternalPostCloserName", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_AssignedEmployees, "Assigned Post-Closer (External) Phone", "sEmployeeExternalPostCloserPhone ", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_AssignedEmployees, "Assigned Post-Closer (External) Email", "sEmployeeExternalPostCloserEmail", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_AssignedEmployees, "Assigned Originating Company Status for Loan Channel", "sPmlBrokerStatusT", Field.E_FieldType.Enu, Field.E_ClassType.Txt
                   , new EnumRepMap(OCStatusType.Blank, ConstApp.FriendlyOCStatusTypes[OCStatusType.Blank])
                   , new EnumRepMap(OCStatusType.Approved, ConstApp.FriendlyOCStatusTypes[OCStatusType.Approved])
                   , new EnumRepMap(OCStatusType.Suspended, ConstApp.FriendlyOCStatusTypes[OCStatusType.Suspended])
                   , new EnumRepMap(OCStatusType.Inactive, ConstApp.FriendlyOCStatusTypes[OCStatusType.Inactive])
                   , new EnumRepMap(OCStatusType.Terminated, ConstApp.FriendlyOCStatusTypes[OCStatusType.Terminated])
                   , new EnumRepMap(OCStatusType.Pending, ConstApp.FriendlyOCStatusTypes[OCStatusType.Pending])
                   , new EnumRepMap(OCStatusType.Declined, ConstApp.FriendlyOCStatusTypes[OCStatusType.Declined])
                   , new EnumRepMap(OCStatusType.Watchlist, ConstApp.FriendlyOCStatusTypes[OCStatusType.Watchlist])
                   );

                eB += m_Schema.Add(Category_AssignedEmployees, "Assigned Originating Company Name", "sOriginatingCompanyName", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_AssignedEmployees, "Assigned Originating Company Index Number", "sOriginatingCompanyIndexNumber", Field.E_FieldType.Int, Field.E_ClassType.Cnt, "#,#;-#,#;0");

                eB += m_Schema.Add(Category_AssignedEmployees, "Processor Name", "sEmployeeProcessorName", Field.E_FieldType.Str , Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_AssignedEmployees, "Processor Team", "sTeamProcessorName", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_AssignedEmployees, "Processor Phone", "sEmployeeProcessorPhone", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_AssignedEmployees, "Processor Email", "sEmployeeProcessorEmail", Field.E_FieldType.Str , Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_AssignedEmployees, "Loan Opener Name", "sEmployeeLoanOpenerName", Field.E_FieldType.Str , Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_AssignedEmployees, "Loan Opener Team", "sTeamLoanOpenerName", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_AssignedEmployees, "Loan Opener Phone", "sEmployeeLoanOpenerPhone", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_AssignedEmployees, "Loan Opener Email", "sEmployeeLoanOpenerEmail", Field.E_FieldType.Str , Field.E_ClassType.Txt);
                //OPM 67478 - make the closer agent available in custom reports
                eB += m_Schema.Add(Category_AssignedEmployees, "Loan Closer Name", "sEmployeeCloserName", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_AssignedEmployees, "Loan Closer Team", "sTeamCloserName", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_AssignedEmployees, "Loan Closer Phone", "sEmployeeCloserPhone", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_AssignedEmployees, "Loan Closer Email", "sEmployeeCloserEmail", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_AssignedEmployees, "Call Center Agent Name", "sEmployeeCallCenterAgentName", Field.E_FieldType.Str , Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_AssignedEmployees, "Call Center Agent Team", "sTeamCallCenterAgentName", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_AssignedEmployees, "Call Center Agent Phone", "sEmployeeCallCenterPhone", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_AssignedEmployees, "Call Center Agent Email", "sEmployeeCallCenterAgentEmail", Field.E_FieldType.Str , Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_AssignedEmployees, "Real Estate Agent Name", "sEmployeeRealEstateAgentName", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_AssignedEmployees, "Real Estate Agent Team", "sTeamRealEstateAgentName", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_AssignedEmployees, "Real Estate Agent Phone", "sEmployeeRealEstateAgentPhone", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_AssignedEmployees, "Real Estate Agent Email", "sEmployeeRealEstateAgentEmail", Field.E_FieldType.Str , Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_AssignedEmployees, "Lender Account Executive Name", "sEmployeeLenderAccExecName", Field.E_FieldType.Str , Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_AssignedEmployees, "Lender Account Executive Team", "sTeamLenderAccExecName", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_AssignedEmployees, "Lender Account Executive Phone", "sEmployeeLenderAccExecPhone", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_AssignedEmployees, "Lender Account Executive Email", "sEmployeeLenderAccExecEmail", Field.E_FieldType.Str , Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_AssignedEmployees, "Underwriter Name", "sEmployeeUnderwriterName", Field.E_FieldType.Str , Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_AssignedEmployees, "Underwriter Team", "sTeamUnderwriterName", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_AssignedEmployees, "Underwriter Phone", "sEmployeeUnderwriterPhone", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_AssignedEmployees, "Underwriter Email", "sEmployeeUnderwriterEmail", Field.E_FieldType.Str , Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_AssignedEmployees, "Lock Desk Name", "sEmployeeLockDeskName", Field.E_FieldType.Str , Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_AssignedEmployees, "Lock Desk Team", "sTeamLockDeskName", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_AssignedEmployees, "Lock Desk Phone", "sEmployeeLockDeskPhone", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_AssignedEmployees, "Lock Desk Email", "sEmployeeLockDeskEmail", Field.E_FieldType.Str , Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_AssignedEmployees, "Manager Name", "sEmployeeManagerName", Field.E_FieldType.Str , Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_AssignedEmployees, "Manager Team", "sTeamManagerName", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_AssignedEmployees, "Manager Phone", "sEmployeeManagerPhone", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_AssignedEmployees, "Manager Email", "sEmployeeManagerEmail", Field.E_FieldType.Str , Field.E_ClassType.Txt);

                // OPM 108148 gf - added Shipper, Funder, Post-Closer, Insuring, Collateral Agent
                eB += m_Schema.Add(Category_AssignedEmployees, "Shipper Name", "sEmployeeShipperName", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_AssignedEmployees, "Shipper Team", "sTeamShipperName", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_AssignedEmployees, "Shipper Phone", "sEmployeeShipperPhone", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_AssignedEmployees, "Shipper Email", "sEmployeeShipperEmail", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_AssignedEmployees, "Funder Name", "sEmployeeFunderName", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_AssignedEmployees, "Funder Team", "sTeamFunderName", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_AssignedEmployees, "Funder Phone", "sEmployeeFunderPhone", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_AssignedEmployees, "Funder Email", "sEmployeeFunderEmail", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_AssignedEmployees, "Post-Closer Name", "sEmployeePostCloserName", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_AssignedEmployees, "Post-Closer Team", "sTeamPostCloserName", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_AssignedEmployees, "Post-Closer Phone", "sEmployeePostCloserPhone", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_AssignedEmployees, "Post-Closer Email", "sEmployeePostCloserEmail", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_AssignedEmployees, "Insuring Name", "sEmployeeInsuringName", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_AssignedEmployees, "Insuring Team", "sTeamInsuringName", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_AssignedEmployees, "Insuring Phone", "sEmployeeInsuringPhone", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_AssignedEmployees, "Insuring Email", "sEmployeeInsuringEmail", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_AssignedEmployees, "Collateral Agent Name", "sEmployeeCollateralAgentName", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_AssignedEmployees, "Collateral Agent Team", "sTeamCollateralAgentName", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_AssignedEmployees, "Collateral Agent Phone", "sEmployeeCollateralAgentPhone", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_AssignedEmployees, "Collateral Agent Email", "sEmployeeCollateralAgentEmail", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_AssignedEmployees, "Doc Drawer Name", "sEmployeeDocDrawerName", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_AssignedEmployees, "Doc Drawer Team", "sTeamDocDrawerName", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_AssignedEmployees, "Doc Drawer Phone", "sEmployeeDocDrawerPhone", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_AssignedEmployees, "Doc Drawer Email", "sEmployeeDocDrawerEmail", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                #region OPM 145015
                eB += m_Schema.Add(Category_AssignedEmployees, "Credit Auditor Name", "sEmployeeCreditAuditorName", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_AssignedEmployees, "Credit Auditor Team", "sTeamCreditAuditorName", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_AssignedEmployees, "Credit Auditor Phone", "sEmployeeCreditAuditorPhone", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_AssignedEmployees, "Credit Auditor Email", "sEmployeeCreditAuditorEmail", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_AssignedEmployees, "Disclosure Desk Name", "sEmployeeDisclosureDeskName", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_AssignedEmployees, "Disclosure Desk Team", "sTeamDisclosureDeskName", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_AssignedEmployees, "Disclosure Desk Phone", "sEmployeeDisclosureDeskPhone", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_AssignedEmployees, "Disclosure Desk Email", "sEmployeeDisclosureDeskEmail", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_AssignedEmployees, "Junior Processor Name", "sEmployeeJuniorProcessorName", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_AssignedEmployees, "Junior Processor Team", "sTeamJuniorProcessorName", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_AssignedEmployees, "Junior Processor Phone", "sEmployeeJuniorProcessorPhone", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_AssignedEmployees, "Junior Processor Email", "sEmployeeJuniorProcessorEmail", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_AssignedEmployees, "Junior Underwriter Name", "sEmployeeJuniorUnderwriterName", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_AssignedEmployees, "Junior Underwriter Team", "sTeamJuniorUnderwriterName", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_AssignedEmployees, "Junior Underwriter Phone", "sEmployeeJuniorUnderwriterPhone", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_AssignedEmployees, "Junior Underwriter Email", "sEmployeeJuniorUnderwriterEmail", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_AssignedEmployees, "Legal Auditor Name", "sEmployeeLegalAuditorName", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_AssignedEmployees, "Legal Auditor Team", "sTeamLegalAuditorName", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_AssignedEmployees, "Legal Auditor Phone", "sEmployeeLegalAuditorPhone", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_AssignedEmployees, "Legal Auditor Email", "sEmployeeLegalAuditorEmail", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_AssignedEmployees, "Loan Officer Assistant Name", "sEmployeeLoanOfficerAssistantName", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_AssignedEmployees, "Loan Officer Assistant Team", "sTeamLoanOfficerAssistantName", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_AssignedEmployees, "Loan Officer Assistant Phone", "sEmployeeLoanOfficerAssistantPhone", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_AssignedEmployees, "Loan Officer Assistant Email", "sEmployeeLoanOfficerAssistantEmail", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_AssignedEmployees, "Purchaser Name", "sEmployeePurchaserName", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_AssignedEmployees, "Purchaser Team", "sTeamPurchaserName", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_AssignedEmployees, "Purchaser Phone", "sEmployeePurchaserPhone", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_AssignedEmployees, "Purchaser Email", "sEmployeePurchaserEmail", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_AssignedEmployees, "QC Compliance Name", "sEmployeeQCComplianceName", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_AssignedEmployees, "QC Compliance Team", "sTeamQCComplianceName", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_AssignedEmployees, "QC Compliance Phone", "sEmployeeQCCompliancePhone", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_AssignedEmployees, "QC Compliance Email", "sEmployeeQCComplianceEmail", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_AssignedEmployees, "Secondary Name", "sEmployeeSecondaryName", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_AssignedEmployees, "Secondary Team", "sTeamSecondaryName", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_AssignedEmployees, "Secondary Phone", "sEmployeeSecondaryPhone", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_AssignedEmployees, "Secondary Email", "sEmployeeSecondaryEmail", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_AssignedEmployees, "Servicing Name", "sEmployeeServicingName", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_AssignedEmployees, "Servicing Team", "sTeamServicingName", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_AssignedEmployees, "Servicing Phone", "sEmployeeServicingPhone", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_AssignedEmployees, "Servicing Email", "sEmployeeServicingEmail", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                #endregion OPM 145015

                #endregion

                #region ( Agent Information Fields )

                eB += m_Schema.Add(Category_OfficialAgents, "Official Loan Officer Name", "sAgentLoanOfficerName", Field.E_FieldType.Str , Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Official Loan Officer Phone", "sAgentLoanOfficerPhone", Field.E_FieldType.Str , Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Official Loan Officer Email", "sAgentLoanOfficerEmail", Field.E_FieldType.Str , Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Official Loan Officer Company Name", "sAgentLoanOfficerCompanyName", Field.E_FieldType.Str , Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Official Loan Officer Employee ID", "sAgentLoanOfficerEmployeeIDInCompany", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Investor Agent Name", "sAgentInvestorAgentName", Field.E_FieldType.Str , Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Investor Company Name", "sAgentInvestorCompanyName", Field.E_FieldType.Str , Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Investor Sold Date", "sAgentInvestorSoldD", Field.E_FieldType.Dtm , Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_OfficialAgents, "Investor Basis Points", "sAgentInvestorBasisPoints", Field.E_FieldType.Dec , Field.E_ClassType.Pct, "N3");
                eB += m_Schema.Add(Category_OfficialAgents, "Investor Case Number", "sAgentInvestorAgentCaseNum", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                //OPM 55072
                eB += m_Schema.Add(Category_OfficialAgents, "Official Manager Name", "sAgentManagerName", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Official Manager Phone", "sAgentManagerPhone", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Official Manager Email", "sAgentManagerEmail", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Official Manager Employee ID", "sAgentManagerEmployeeIDInCompany", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                
                eB += m_Schema.Add(Category_OfficialAgents, "Official Underwriter Name", "sAgentUnderwriterName", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Official Underwriter Phone", "sAgentUnderwriterPhone", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Official Underwriter Email", "sAgentUnderwriterEmail", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                
                eB += m_Schema.Add(Category_OfficialAgents, "Official Processor Name", "sAgentProcessorName", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Official Processor Phone", "sAgentProcessorPhone", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Official Processor Email", "sAgentProcessorEmail", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Official Processor Employee ID", "sAgentProcessorEmployeeIDInCompany", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                // OPM 108148 gf 1/23/13 - added Shipper, Funder, Post-Closer, Insuring, Collateral Agent, Doc Drawer
                eB += m_Schema.Add(Category_OfficialAgents, "Official Shipper Name", "sAgentShipperName", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Official Shipper Phone", "sAgentShipperPhone", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Official Shipper Email", "sAgentShipperEmail", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_OfficialAgents, "Official Funder Name", "sAgentFunderName", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Official Funder Phone", "sAgentFunderPhone", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Official Funder Email", "sAgentFunderEmail", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_OfficialAgents, "Official Post-Closer Name", "sAgentPostCloserName", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Official Post-Closer Phone", "sAgentPostCloserPhone", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Official Post-Closer Email", "sAgentPostCloserEmail", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_OfficialAgents, "Official Insuring Name", "sAgentInsuringName", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Official Insuring Phone", "sAgentInsuringPhone", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Official Insuring Email", "sAgentInsuringEmail", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_OfficialAgents, "Official Collateral Agent Name", "sAgentCollateralAgentName", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Official Collateral Agent Phone", "sAgentCollateralAgentPhone", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Official Collateral Agent Email", "sAgentCollateralAgentEmail", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_OfficialAgents, "Official Doc Drawer Name", "sAgentDocDrawerName", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Official Doc Drawer Phone", "sAgentDocDrawerPhone", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Official Doc Drawer Email", "sAgentDocDrawerEmail", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                #region OPM 145015
                eB += m_Schema.Add(Category_OfficialAgents, "Official Credit Auditor Name", "sAgentCreditAuditorName", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Official Credit Auditor Phone", "sAgentCreditAuditorPhone", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Official Credit Auditor Email", "sAgentCreditAuditorEmail", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_OfficialAgents, "Official Disclosure Desk Name", "sAgentDisclosureDeskName", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Official Disclosure Desk Phone", "sAgentDisclosureDeskPhone", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Official Disclosure Desk Email", "sAgentDisclosureDeskEmail", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_OfficialAgents, "Official Junior Processor Name", "sAgentJuniorProcessorName", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Official Junior Processor Phone", "sAgentJuniorProcessorPhone", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Official Junior Processor Email", "sAgentJuniorProcessorEmail", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_OfficialAgents, "Official Junior Underwriter Name", "sAgentJuniorUnderwriterName", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Official Junior Underwriter Phone", "sAgentJuniorUnderwriterPhone", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Official Junior Underwriter Email", "sAgentJuniorUnderwriterEmail", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_OfficialAgents, "Official Legal Auditor Name", "sAgentLegalAuditorName", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Official Legal Auditor Phone", "sAgentLegalAuditorPhone", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Official Legal Auditor Email", "sAgentLegalAuditorEmail", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_OfficialAgents, "Official Loan Officer Assistant Name", "sAgentLoanOfficerAssistantName", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Official Loan Officer Assistant Phone", "sAgentLoanOfficerAssistantPhone", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Official Loan Officer Assistant Email", "sAgentLoanOfficerAssistantEmail", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_OfficialAgents, "Official Purchaser Name", "sAgentPurchaserName", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Official Purchaser Phone", "sAgentPurchaserPhone", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Official Purchaser Email", "sAgentPurchaserEmail", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_OfficialAgents, "Official QC Compliance Name", "sAgentQCComplianceName", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Official QC Compliance Phone", "sAgentQCCompliancePhone", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Official QC Compliance Email", "sAgentQCComplianceEmail", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_OfficialAgents, "Official Secondary Name", "sAgentSecondaryName", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Official Secondary Phone", "sAgentSecondaryPhone", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Official Secondary Email", "sAgentSecondaryEmail", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_OfficialAgents, "Official Servicing Name", "sAgentServicingName", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Official Servicing Phone", "sAgentServicingPhone", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Official Servicing Email", "sAgentServicingEmail", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                #endregion OPM 145015

                eB += m_Schema.Add(Category_OfficialAgents, "Lender Agent Name", "sAgentLenderAgentName", Field.E_FieldType.Str , Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Lender Agent Phone", "sAgentLenderAgentPhone", Field.E_FieldType.Str , Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Lender Agent Email", "sAgentLenderAgentEmail", Field.E_FieldType.Str , Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Lender Agent Case Number", "sAgentLenderAgentCaseNum", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Lender Company Name", "sAgentLenderCompanyName", Field.E_FieldType.Str , Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_OfficialAgents, "Appraiser Agent Name", "sAgentAppraiserAgentName", Field.E_FieldType.Str , Field.E_ClassType.Txt);
				eB += m_Schema.Add(Category_OfficialAgents, "Appraiser Agent Phone", "sAgentAppraiserPhone", Field.E_FieldType.Str , Field.E_ClassType.Txt); //opm 17863 fs 08/14/08
                eB += m_Schema.Add(Category_OfficialAgents, "Appraiser Company Name", "sAgentAppraiserCompanyName", Field.E_FieldType.Str , Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Appraiser City", "sAgentAppraiserCity", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Appraiser State", "sAgentAppraiserState", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                
                eB += m_Schema.Add(Category_OfficialAgents, "Escrow Agent Name", "sAgentEscrowAgentName", Field.E_FieldType.Str , Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Escrow Company Name", "sAgentEscrowCompanyName", Field.E_FieldType.Str , Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Escrow Agent Phone", "sAgentEscrowPhone", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Escrow Agent Email", "sAgentEscrowEmail", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Escrow Street Address", "sAgentEscrowStreetAddr", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Escrow City", "sAgentEscrowCity", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Escrow State", "sAgentEscrowState", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Escrow Zip", "sAgentEscrowZip", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Escrow Case Number", "sAgentEscrowCaseNum", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_OfficialAgents, "Buyer Agent Name", "sAgentBuyerAgentName", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Buyer Agent Phone", "sAgentBuyerPhone", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Buyer Agent Email", "sAgentBuyerEmail", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Buyer Agent Company Name", "sAgentBuyerCompanyName", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Buyer Agent Address", "sAgentBuyerStreetAddr", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Buyer Agent City", "sAgentBuyerCity", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Buyer Agent State", "sAgentBuyerState", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Buyer Agent Zip", "sAgentBuyerZip", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Buyer Agent Fax", "sAgentBuyerFax", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_OfficialAgents, "Referral Agent Name", "sAgentReferralAgentName", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Referral Agent Phone", "sAgentReferralPhone", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Referral Agent Email", "sAgentReferralEmail", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Referral Agent Company Name", "sAgentReferralCompanyName", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Referral Agent Address", "sAgentReferralStreetAddr", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Referral Agent City", "sAgentReferralCity", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Referral Agent State", "sAgentReferralState", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Referral Agent Zip", "sAgentReferralZip", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Referral Agent Fax", "sAgentReferralFax", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_OfficialAgents, "Closing Agent Name", "sAgentCloserName", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Closing Agent Address", "sAgentCloserStreetAddr", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Closing Agent City", "sAgentCloserCity", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Closing Agent State", "sAgentCloserState", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Closing Agent Zip", "sAgentCloserZip", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Closing Agent Phone Number", "sAgentCloserPhone", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Closing Agent Case Number", "sAgentCloserCaseNum", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_OfficialAgents, "Listing Agent Name", "sAgentReListingName", Field.E_FieldType.Str , Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Listing Agent Phone", "sAgentReListingPhone", Field.E_FieldType.Str , Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Listing Agent Email", "sAgentReListingEmail", Field.E_FieldType.Str , Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Listing Agent Company Name", "sAgentReListingCompanyName", Field.E_FieldType.Str , Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Listing Agent Address", "sAgentListingAgentStreetAddr", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Listing Agent City", "sAgentListingAgentCity", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Listing Agent State", "sAgentListingAgentState", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Listing Agent Zip", "sAgentListingAgentZip", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Listing Agent Fax", "sAgentListingAgentFax", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                
                eB += m_Schema.Add(Category_OfficialAgents, "Selling Agent Name", "sAgentReSellingName", Field.E_FieldType.Str , Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Selling Agent Phone", "sAgentReSellingPhone", Field.E_FieldType.Str , Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Selling Agent Email", "sAgentReSellingEmail", Field.E_FieldType.Str , Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Selling Agent Company Name", "sAgentReSellingCompanyName", Field.E_FieldType.Str , Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Selling Agent Address", "sAgentSellingAgentStreetAddr", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Selling Agent City", "sAgentSellingAgentCity", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Selling Agent State", "sAgentSellingAgentState", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Selling Agent Zip", "sAgentSellingAgentZip", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Selling Agent Fax", "sAgentSellingAgentFax", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_OfficialAgents, "Title Company Name", "sAgentTitleCompanyName", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Title Agent Name", "sAgentTitleAgentName", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Title Agent Phone", "sAgentTitlePhone", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Title Agent Email", "sAgentTitleEmail", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Title Street Address", "sAgentTitleStreetAddr", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Title City", "sAgentTitleCity", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Title State", "sAgentTitleState", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Title Zip", "sAgentTitleZip", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Title Case Number", "sAgentTitleCaseNum", Field.E_FieldType.Str, Field.E_ClassType.Txt);


                eB += m_Schema.Add(Category_OfficialAgents, "Homeowner Insurance Company Name", "sAgentHazardInsuranceCompanyName", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Homeowner Insurance Agent Name", "sAgentHazardInsuranceAgentName", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Homeowner Insurance Agent Phone", "sAgentHazardInsurancePhone", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Homeowner Insurance Agent Email", "sAgentHazardInsuranceEmail", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Homeowner Insurance Agent Case Number", "sAgentHazardInsuranceCaseNum", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_OfficialAgents, "Other Agent Name", "sAgentOtherAgentName", Field.E_FieldType.Str , Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Other Agent Description", "sAgentOtherAgentRoleTDesc", Field.E_FieldType.Str , Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Other Agent Company Name", "sAgentOtherCompanyName", Field.E_FieldType.Str , Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_OfficialAgents, "Loan Originator Identifier", "sApp1003InterviewerLoanOriginatorIdentifier", Field.E_FieldType.Str, Field.E_ClassType.Txt);
				eB += m_Schema.Add(Category_OfficialAgents, "1003 Interviewer Employer Name", "sApp1003InterviewerCompanyName", Field.E_FieldType.Str , Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "1003 Loan Origination Company Identifier", "sApp1003InterviewerLoanOriginationCompanyIdentifier", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_OfficialAgents, "1003 Interviewer License Number", "sApp1003InterviewerLicenseNumOfAgent", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "1003 Loan Origination Company License Number", "sApp1003InterviewerLicenseNumOfCompany", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_OfficialAgents, "1003 Interviewer License Number Is Expired", "sApp1003InterviewerLicenseNumOfAgentIsExpired", Field.E_FieldType.Boo, Field.E_ClassType.Txt, new BoolRepMap(true, "Yes"), new BoolRepMap(false, "No"));
                eB += m_Schema.Add(Category_OfficialAgents, "1003 Loan Origination Company License Number Is Expired", "sApp1003InterviewerLicenseNumOfCompanyIsExpired", Field.E_FieldType.Boo, Field.E_ClassType.Txt, new BoolRepMap(true, "Yes"), new BoolRepMap(false, "No"));

                eB += m_Schema.Add(Category_OfficialAgents, "Broker Company Name", "sAgentBrokerCompanyName", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Broker Agent Name", "sAgentBrokerAgentName", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Broker Agent Phone", "sAgentBrokerPhone", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Broker Agent Email", "sAgentBrokerEmail", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Broker Street Address", "sAgentBrokerStreetAddr", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Broker City", "sAgentBrokerCity", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Broker State", "sAgentBrokerState", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Broker Zip", "sAgentBrokerZip", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_OfficialAgents, "Subservicer Company Name", "sAgentSubservicerCompanyName", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Subservicer Street Address", "sAgentSubservicerStreetAddr", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Subservicer City", "sAgentSubservicerCity", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Subservicer State", "sAgentSubservicerState", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_OfficialAgents, "Subservicer Zip", "sAgentSubservicerZip", Field.E_FieldType.Str, Field.E_ClassType.Txt);
				#endregion

                #region ( Subject Property Details Fields )

                eB += m_Schema.Add(Category_SubjProperty, "Subj Prop Purpose", "aOccT", Field.E_FieldType.Enu , Field.E_ClassType.Txt
                    , new EnumRepMap( E_aOccT.Investment         , "Investment"          )
                    , new EnumRepMap( E_aOccT.PrimaryResidence   , "Primary Residence"   )
                    , new EnumRepMap( E_aOccT.SecondaryResidence , "Secondary Residence" )
                    );



                eB += m_Schema.Add(Category_SubjProperty, "Subj Prop Address", "sSpAddr", Field.E_FieldType.Str , Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_SubjProperty, "Subj Prop City", "sSpCity", Field.E_FieldType.Str , Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_SubjProperty, "Subj Prop State", "sSpState", Field.E_FieldType.Str , Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_SubjProperty, "Subj Prop Zip", "sSpZip", Field.E_FieldType.Str , Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_SubjProperty, "Subj Prop County", "sSpCounty", Field.E_FieldType.Str , Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_SubjProperty, "Subj Prop Years Built", "sYrBuilt", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_SubjProperty, "Subj Prop No. of Units", "sUnitsNum", Field.E_FieldType.Int, Field.E_ClassType.Cnt, "#,#;-#,#;0");
                eB += m_Schema.Add(Category_SubjProperty, "Subj Prop Valuation Effective Date", "sSpValuationEffectiveD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_SubjProperty, "Project Name", "sProjNm", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_SubjProperty, "FNMA/FHLMC/FHLB Refinance Program", "sSpGseRefiProgramT", Field.E_FieldType.Enu, Field.E_ClassType.Txt
                    , new EnumRepMap(E_sSpGseRefiProgramT.None, "None / Not Applicable")
                    , new EnumRepMap(E_sSpGseRefiProgramT.DURefiPlus, "DU Refi Plus")
                    , new EnumRepMap(E_sSpGseRefiProgramT.RefiPlus, "Refi Plus")
                    , new EnumRepMap(E_sSpGseRefiProgramT.ReliefRefiOpenAccess, "Relief Refinance - Open Access")
                    , new EnumRepMap(E_sSpGseRefiProgramT.ReliefRefiSameServicer, "Relief Refinance - Same Servicer")
                    , new EnumRepMap(E_sSpGseRefiProgramT.FHLBDisasterResponse, "FHLB Disaster Response")
                    );

                var loanBeingRefinancedAmortT = Enum.GetValues(typeof(E_sLoanBeingRefinancedAmortizationT)).Cast<E_sLoanBeingRefinancedAmortizationT>().Select(e => new EnumRepMap(e, e.GetDescription())).ToArray();
                eB += m_Schema.Add(Category_SubjProperty, "Existing Loan: Amortization Type", "sLoanBeingRefinancedAmortizationT", Field.E_FieldType.Enu, Field.E_ClassType.Txt, loanBeingRefinancedAmortT);
                var loanBeingRefinancedLienPosT = Enum.GetValues(typeof(E_sLoanBeingRefinancedLienPosT)).Cast<E_sLoanBeingRefinancedLienPosT>().Select(e => new EnumRepMap(e, e.GetDescription())).ToArray();
                eB += m_Schema.Add(Category_SubjProperty, "Existing Loan: Lien Position", "sLoanBeingRefinancedLienPosT", Field.E_FieldType.Enu, Field.E_ClassType.Txt, loanBeingRefinancedLienPosT);

                eB += m_Schema.Add(Category_SubjProperty, "Existing Loan: Interest Rate", "sLoanBeingRefinancedInterestRate", Field.E_FieldType.Dec, Field.E_ClassType.Pct, "N3");
                eB += m_Schema.Add(Category_SubjProperty, "Existing Loan: Remaining Term (mths)", "sLoanBeingRefinancedRemainingTerm", Field.E_FieldType.Int, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_SubjProperty, "Existing Loan: Total of Payments", "sLoanBeingRefinancedTotalOfPayments", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_SubjProperty, "Existing Loan: LTV", "sLoanBeingRefinancedLtvR", Field.E_FieldType.Dec, Field.E_ClassType.Pct, "N3");
                eB += m_Schema.Add(Category_SubjProperty, "Existing Loan: Was used to construct, alter, or repair the home", "sLoanBeingRefinancedUsedToConstructAlterRepair", Field.E_FieldType.Boo, Field.E_ClassType.Txt, new BoolRepMap(true, "Yes"), new BoolRepMap(false, "No"));
                #endregion

                #region ( Borrower Details Fields )

                eB += m_Schema.Add(Category_BorrowerDetails, "Borr First Name", "aBFirstNm", Field.E_FieldType.Str , Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_BorrowerDetails, "Borr Preferred Name", "sPrimaryBorrPreferredNm", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_BorrowerDetails, "Borr Last Name", "aBLastNm", Field.E_FieldType.Str , Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_BorrowerDetails, "Borr Middle Name", "aBMidNm", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_BorrowerDetails, "Borr Suffix", "aBSuffix", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_BorrowerDetails, "Borr SSN", "aBSsn", Field.E_FieldType.Str , Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_BorrowerDetails, "Has Borrower SSN", "aBHasSsn", "cast(case when aBSsnEncrypted IS NULL then 0 else 1 end as int)", Field.E_FieldType.Int, Field.E_ClassType.Txt, new IntegerRepMap(1, "Yes"), new IntegerRepMap(0, "No"));
                eB += m_Schema.Add(Category_BorrowerDetails, "Borr Age", "aBAge", Field.E_FieldType.Int , Field.E_ClassType.Cnt, "#,#;-#,#;0");
                eB += m_Schema.Add(Category_BorrowerDetails, "Borr Date of Birth", "aBDob", Field.E_FieldType.Dtm , Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_BorrowerDetails, "Borr Next Birthday", "aBNextBirthday"
                    , "dateadd( yyyy , year( getdate() ) - year( aBDob ) + ( floor( ( sign( datediff( dd , getdate() , dateadd( yyyy , year( getdate() ) - year( aBDob ) , aBDob ) ) ) - 1 ) / ( -2 ) ) ) , aBDob )"
                    , Field.E_FieldType.Dtm , Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_BorrowerDetails, "Has Coborrower", "aBHasSpouse", "cast(case when aCSsnEncrypted IS NULL then 0 else 1 end as int)", Field.E_FieldType.Int, Field.E_ClassType.Txt, new IntegerRepMap(1, "Yes"), new IntegerRepMap(0, "No")); //opm 27414 fs 01/26/09
                eB += m_Schema.Add(Category_BorrowerDetails, "Borr Marital Status", "aBMaritalStatT", Field.E_FieldType.Enu, Field.E_ClassType.Txt
                    , new EnumRepMap(E_aBMaritalStatT.LeaveBlank, "")
                    , new EnumRepMap(E_aBMaritalStatT.Married, "Married")
                    , new EnumRepMap(E_aBMaritalStatT.NotMarried, "Not Married")
                    , new EnumRepMap(E_aBMaritalStatT.Separated, "Separated"));
                eB += m_Schema.Add(Category_BorrowerDetails, "Borr Yrs. School", "aBSchoolYrs", Field.E_FieldType.Int, Field.E_ClassType.Cnt, "#,#;-#,#;0");
                eB += m_Schema.Add(Category_BorrowerDetails, "Borr No. of Deps", "aBDependNum", Field.E_FieldType.Int, Field.E_ClassType.Cnt, "#,#;-#,#;0");
				eB += m_Schema.Add(Category_BorrowerDetails, "Borr Full Name", "aBFullNm"
                    , "(aBLastNm + ', ' + aBFirstNm + ' ' + aBMidNm)"
                    , Field.E_FieldType.Str, Field.E_ClassType.Txt);
                
                eB += m_Schema.Add(Category_BorrowerDetails, "Coborr First Name", "aCFirstNm", Field.E_FieldType.Str , Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_BorrowerDetails, "Coborr Preferred Name", "sPrimaryCoBorrPreferredNm", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_BorrowerDetails, "Coborr Last Name", "aCLastNm", Field.E_FieldType.Str , Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_BorrowerDetails, "Coborr Middle Name", "aCMidNm", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_BorrowerDetails, "Coborr Suffix", "aCSuffix", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_BorrowerDetails, "Coborr SSN", "aCSsn", Field.E_FieldType.Str , Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_BorrowerDetails, "Coborr Age", "aCAge", Field.E_FieldType.Int , Field.E_ClassType.Cnt, "#,#;-#,#;0");
				
                eB += m_Schema.Add(Category_BorrowerDetails, "Coborr Date of Birth", "aCDob", Field.E_FieldType.Dtm , Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_BorrowerDetails, "Coborr Next Birthday", "aCNextBirthday", "dateadd( yyyy , year( getdate() ) - year( aCDob ) + ( floor( ( sign( datediff( dd , getdate() , dateadd( yyyy , year( getdate() ) - year( aCDob ) , aCDob ) ) ) - 1 ) / ( -2 ) ) ) , aCDob )", Field.E_FieldType.Dtm , Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_BorrowerDetails, "Coborr Yrs. School", "aCSchoolYrs", Field.E_FieldType.Int, Field.E_ClassType.Cnt, "#,#;-#,#;0");
                eB += m_Schema.Add(Category_BorrowerDetails, "Coborr No. of Deps", "aCDependNum", Field.E_FieldType.Int, Field.E_ClassType.Cnt, "#,#;-#,#;0");
                
                eB += m_Schema.Add(Category_BorrowerDetails, "Borr Address", "aBAddr", Field.E_FieldType.Str , Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_BorrowerDetails, "Borr City", "aBCity", Field.E_FieldType.Str , Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_BorrowerDetails, "Borr State", "aBState", Field.E_FieldType.Str , Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_BorrowerDetails, "Borr Zip", "aBZip", Field.E_FieldType.Str , Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_BorrowerDetails, "Borr Own/Rent", "aBAddrT", Field.E_FieldType.Enu, Field.E_ClassType.Txt
                    , new EnumRepMap(E_aBAddrT.LeaveBlank, "")
                    , new EnumRepMap(E_aBAddrT.LivingRentFree, "Living Rent Free")
                    , new EnumRepMap(E_aBAddrT.Own, "Own")
                    , new EnumRepMap(E_aBAddrT.Rent, "Rent"));
                eB += m_Schema.Add(Category_BorrowerDetails, "Borr # Yrs at Addr", "aBAddrYrs", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                
                eB += m_Schema.Add(Category_BorrowerDetails, "Coborr Address", "aCAddr", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_BorrowerDetails, "Coborr City", "aCCity", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_BorrowerDetails, "Coborr State", "aCState", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_BorrowerDetails, "Coborr Zip", "aCZip", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_BorrowerDetails, "Coborr Own/Rent", "aCAddrT", Field.E_FieldType.Enu, Field.E_ClassType.Txt
                    , new EnumRepMap(E_aBAddrT.LeaveBlank, "")
                    , new EnumRepMap(E_aBAddrT.LivingRentFree, "Living Rent Free")
                    , new EnumRepMap(E_aBAddrT.Own, "Own")
                    , new EnumRepMap(E_aBAddrT.Rent, "Rent"));
                eB += m_Schema.Add(Category_BorrowerDetails, "Coborr # Yrs at Addr", "aCAddrYrs", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                
                eB += m_Schema.Add(Category_BorrowerDetails, "Borr Home Phone", "aBHPhone", Field.E_FieldType.Str , Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_BorrowerDetails, "Borr Cell Phone", "aBCellphone", Field.E_FieldType.Str, Field.E_ClassType.Txt); //opm 31196 fs 05/06/09
                eB += m_Schema.Add(Category_BorrowerDetails, "Borr Business Phone", "aBBusPhone", Field.E_FieldType.Str , Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_BorrowerDetails, "Coborr Home Phone", "aCHPhone", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_BorrowerDetails, "Coborr Cell Phone", "aCCellphone", Field.E_FieldType.Str, Field.E_ClassType.Txt); //opm 31196 fs 05/06/09
                eB += m_Schema.Add(Category_BorrowerDetails, "Borr Email", "aBEmail", Field.E_FieldType.Str , Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_BorrowerDetails, "Coborr Email", "aCEmail", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_BorrowerDetails, "Borr Experian Credit Score", "aBExperianScore", Field.E_FieldType.Int , Field.E_ClassType.Cnt, "#,#;-#,#;0");
                eB += m_Schema.Add(Category_BorrowerDetails, "Coborr Experian Credit Score", "aCExperianScore", Field.E_FieldType.Int , Field.E_ClassType.Cnt, "#,#;-#,#;0");
                eB += m_Schema.Add(Category_BorrowerDetails, "Borr TransUnion Credit Score", "aBTransUnionScore", Field.E_FieldType.Int , Field.E_ClassType.Cnt, "#,#;-#,#;0");
                eB += m_Schema.Add(Category_BorrowerDetails, "Coborr TransUnion Credit Score", "aCTransUnionScore", Field.E_FieldType.Int , Field.E_ClassType.Cnt, "#,#;-#,#;0");
                eB += m_Schema.Add(Category_BorrowerDetails, "Borr Equifax Credit Score", "aBEquifaxScore", Field.E_FieldType.Int , Field.E_ClassType.Cnt, "#,#;-#,#;0");
                eB += m_Schema.Add(Category_BorrowerDetails, "Coborr Equifax Credit Score", "aCEquifaxScore", Field.E_FieldType.Int , Field.E_ClassType.Cnt, "#,#;-#,#;0");

                eB += m_Schema.Add(Category_BorrowerDetails, "Is Borr Self-Employed", "aBIsSelfEmplmt", Field.E_FieldType.Boo, Field.E_ClassType.Txt
                    , new BoolRepMap(true, "Yes")
                    , new BoolRepMap(false, "No")
                    ); //opm 17674 fs 07/16/09

                eB += m_Schema.Add(Category_BorrowerDetails, "Borr Primary Employer Name", "aBEmplrNm", Field.E_FieldType.Str , Field.E_ClassType.Txt);                
                eB += m_Schema.Add(Category_BorrowerDetails, "Borr Primary Employer Addr", "aBEmplrAddr", Field.E_FieldType.Str , Field.E_ClassType.Txt);                
                eB += m_Schema.Add(Category_BorrowerDetails, "Borr Primary Employer City", "aBEmplrCity", Field.E_FieldType.Str , Field.E_ClassType.Txt);                
                eB += m_Schema.Add(Category_BorrowerDetails, "Borr Primary Employer State", "aBEmplrState", Field.E_FieldType.Str , Field.E_ClassType.Txt);                
                eB += m_Schema.Add(Category_BorrowerDetails, "Borr Primary Employer Zip", "aBEmplrZip", Field.E_FieldType.Str , Field.E_ClassType.Txt);                
                eB += m_Schema.Add(Category_BorrowerDetails, "Borr Primary Employer Phone", "aBEmplrBusPhone", Field.E_FieldType.Str , Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_BorrowerDetails, "Is Coborr Self-Employed", "aCIsSelfEmplmt", Field.E_FieldType.Boo, Field.E_ClassType.Txt
                    , new BoolRepMap(true, "Yes")
                    , new BoolRepMap(false, "No")
                    ); //opm 17674 fs 07/16/09

                eB += m_Schema.Add(Category_BorrowerDetails, "Coborr Primary Employer Name", "aCEmplrNm", Field.E_FieldType.Str , Field.E_ClassType.Txt);                
                eB += m_Schema.Add(Category_BorrowerDetails, "Coborr Primary Employer Addr", "aCEmplrAddr", Field.E_FieldType.Str , Field.E_ClassType.Txt);                
                eB += m_Schema.Add(Category_BorrowerDetails, "Coborr Primary Employer City", "aCEmplrCity", Field.E_FieldType.Str , Field.E_ClassType.Txt);                
                eB += m_Schema.Add(Category_BorrowerDetails, "Coborr Primary Employer State", "aCEmplrState", Field.E_FieldType.Str , Field.E_ClassType.Txt);                
                eB += m_Schema.Add(Category_BorrowerDetails, "Coborr Primary Employer Zip", "aCEmplrZip", Field.E_FieldType.Str , Field.E_ClassType.Txt);                
                eB += m_Schema.Add(Category_BorrowerDetails, "Coborr Primary Employer Phone", "aCEmplrBusPhone", Field.E_FieldType.Str , Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_BorrowerDetails, "Borr Verif of Employment Ordered Date", "aBEmplVerifSent", Field.E_FieldType.Dtm, Field.E_ClassType.Cal); //opm 51836 fs 06/17/10
                eB += m_Schema.Add(Category_BorrowerDetails, "Borr Verif of Employment Received Date", "aBEmplVerifRecv", Field.E_FieldType.Dtm, Field.E_ClassType.Cal); //opm 51836 fs 06/17/10
                eB += m_Schema.Add(Category_BorrowerDetails, "Borr Verif of Deposits Ordered Date", "aBAssetsVerifSent", Field.E_FieldType.Dtm, Field.E_ClassType.Cal); //opm 51836 fs 06/17/10
                eB += m_Schema.Add(Category_BorrowerDetails, "Borr Verif of Deposits Received Date", "aBAssetsVerifRecv", Field.E_FieldType.Dtm, Field.E_ClassType.Cal); //opm 51836 fs 06/17/10

                eB += m_Schema.Add(Category_BorrowerDetails, "Borr Total Monthly Income", "aBTotI", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_BorrowerDetails, "CoBorr Total Monthly Income", "aCTotI", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");

                eB += m_Schema.Add(Category_BorrowerDetails, "All Borrowers Have Authorized Credit Check", nameof(CPageData.sAllBorrowersHaveAuthorizedCredit), Field.E_FieldType.Boo, Field.E_ClassType.Txt, new BoolRepMap(true, "Yes"), new BoolRepMap(false, "No"));

                eB += m_Schema.Add(Category_BorrowerDetails, "Primary Borrower Member ID", "sPrimaryBorrCoreSystemId", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                #endregion

                #region ( HMDA Audit Fields )

                eB += m_Schema.Add(Category_HmdaAuditing, "Universal Loan Identifier", "sUniversalLoanIdentifier", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_HmdaAuditing, "HMDA Application Date", "sHmdaApplicationDate", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");

                eB += m_Schema.Add( Category_HmdaAuditing, "Excluded from Report", "sHmdaExcludedFromReport", Field.E_FieldType.Boo , Field.E_ClassType.Txt
                    , new BoolRepMap( true  , "Yes" )
                    , new BoolRepMap( false , "No"  )
                    );

                eB += m_Schema.Add(Category_HmdaAuditing, "Preapproval Type", "sHmdaPreapprovalT", Field.E_FieldType.Enu , Field.E_ClassType.Txt
                    , new EnumRepMap( E_sHmdaPreapprovalT.LeaveBlank              , "Left Blank"                )
                    , new EnumRepMap( E_sHmdaPreapprovalT.PreapprovalRequested    , "Preapproval Requested"     )
                    , new EnumRepMap( E_sHmdaPreapprovalT.PreapprovalNotRequested , "Preapproval Not Requested" )
                    , new EnumRepMap( E_sHmdaPreapprovalT.NotApplicable           , "Not Applicable"            )
                    );

                eB += m_Schema.Add(Category_HmdaAuditing, "Average Prime Offer Rate", "sQMAveragePrimeOfferR", Field.E_FieldType.Dec , Field.E_ClassType.Pct, "N3");

                eB += m_Schema.Add(Category_HmdaAuditing, "APR Rate Spread", "sHmdaAprRateSpread", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_HmdaAuditing, "Report as Home Improvement Loan", "sHmdaReportAsHomeImprov", Field.E_FieldType.Boo , Field.E_ClassType.Txt
                    , new BoolRepMap( true  , "Yes" )
                    , new BoolRepMap( false , "No"  )
                    );

                eB += m_Schema.Add(Category_HmdaAuditing, "Report as HOEPA Loan", "sHmdaReportAsHoepaLoan", Field.E_FieldType.Boo , Field.E_ClassType.Txt
                    , new BoolRepMap( true  , "Yes" )
                    , new BoolRepMap( false , "No"  )
                    );

                eB += m_Schema.Add(Category_HmdaAuditing, "MSA Number", "sHmdaMsaNum", Field.E_FieldType.Str , Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_HmdaAuditing, "County Code", "sHmdaCountyCode", Field.E_FieldType.Str , Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_HmdaAuditing, "State Code", "sHmdaStateCode", Field.E_FieldType.Str , Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_HmdaAuditing, "Census Tract", "sHmdaCensusTract", Field.E_FieldType.Str , Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_HmdaAuditing, "Lien Status", "sHmdaLienT", Field.E_FieldType.Enu , Field.E_ClassType.Txt
                    , new EnumRepMap( E_sHmdaLienT.LeaveBlank                , "Left Blank"          )
                    , new EnumRepMap( E_sHmdaLienT.FirstLien                 , "First Lien"          )
                    , new EnumRepMap( E_sHmdaLienT.SubordinateLien           , "Subordinate Lien"    )
                    , new EnumRepMap( E_sHmdaLienT.NotSecuredByLien          , "Not Secured By Lien" )
                    , new EnumRepMap( E_sHmdaLienT.NotApplicablePurchaseLoan , "Not Applicable"      )
                    );

                eB += m_Schema.Add(Category_HmdaAuditing, "HMDA Property Type", "sHmdaPropT", Field.E_FieldType.Enu , Field.E_ClassType.Txt
                    , new EnumRepMap( E_sHmdaPropT.OneTo4Family        , "1 - 4 Family"         )
                    , new EnumRepMap( E_sHmdaPropT.ManufacturedHousing , "Manufactured Housing" )
                    , new EnumRepMap( E_sHmdaPropT.MultiFamiliy        , "Multi-Family"         )
                    , new EnumRepMap( E_sHmdaPropT.LeaveBlank          , "Left Blank"           )
                    );

                eB += m_Schema.Add(Category_HmdaAuditing, "Purchaser Type", "sHmdaPurchaser2004T", Field.E_FieldType.Enu , Field.E_ClassType.Txt
                    , new EnumRepMap( E_sHmdaPurchaser2004T.LoanWasNotSold        , "Loan Was Not Sold"                     )
                    , new EnumRepMap( E_sHmdaPurchaser2004T.FNMA                  , "FNMA"                                  )
                    , new EnumRepMap( E_sHmdaPurchaser2004T.GNMA                  , "GNMA"                                  )
                    , new EnumRepMap( E_sHmdaPurchaser2004T.FHLMC                 , "FHLMC")
                    , new EnumRepMap( E_sHmdaPurchaser2004T.FarmerMac             , "FarmerMac"                             )
                    , new EnumRepMap( E_sHmdaPurchaser2004T.PrivateSecuritization , "Private Securitization"                )
                    , new EnumRepMap( E_sHmdaPurchaser2004T.CommercialBank        , "Commercial Bank"                       )
                    , new EnumRepMap( E_sHmdaPurchaser2004T.LifeInsCreditUnion    , "Life Insurance Co., Finance Co., etc." )
                    , new EnumRepMap( E_sHmdaPurchaser2004T.AffiliateInstitution  , "Affiliate Institution"                 )
                    , new EnumRepMap( E_sHmdaPurchaser2004T.OtherType             , "Other"                                 )
                    , new EnumRepMap( E_sHmdaPurchaser2004T.LeaveBlank            , "Left Blank"                            )
                    );

                eB += m_Schema.Add(Category_HmdaAuditing, "Purchaser", "sHmdaPurchaser", Field.E_FieldType.Str , Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_HmdaAuditing, "Action Taken", "sHmdaActionTaken", Field.E_FieldType.Str , Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_HmdaAuditing, "Action Taken Date", "sHmdaActionD", Field.E_FieldType.Dtm , Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_HmdaAuditing, "Denial Reason #1", "sHmdaDenialReason1", Field.E_FieldType.Str , Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_HmdaAuditing, "Denial Reason #2", "sHmdaDenialReason2", Field.E_FieldType.Str , Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_HmdaAuditing, "Denial Reason #3", "sHmdaDenialReason3", Field.E_FieldType.Str , Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_HmdaAuditing, "Loan Denied", "sHmdaLoanDenied", Field.E_FieldType.Boo , Field.E_ClassType.Txt
                    , new BoolRepMap( true  , "Yes" )
                    , new BoolRepMap( false , "No"  )
                    );

                eB += m_Schema.Add(Category_HmdaAuditing, "Loan Denied By", "sHmdaLoanDeniedBy", Field.E_FieldType.Str , Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_HmdaAuditing, "Denied Form Done", "sHmdaDeniedFormDone", Field.E_FieldType.Boo , Field.E_ClassType.Txt
                    , new BoolRepMap( true  , "Yes" )
                    , new BoolRepMap( false , "No"  )
                    );

                eB += m_Schema.Add(Category_HmdaAuditing, "Denied Form Done By", "sHmdaDeniedFormDoneBy", Field.E_FieldType.Str , Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_HmdaAuditing, "Denied Form Done Date", "sHmdaDeniedFormDoneD", Field.E_FieldType.Dtm , Field.E_ClassType.Cal, "M/d/yyyy");

                eB += m_Schema.Add(Category_HmdaAuditing, "Counter Offer Made", "sHmdaCounterOfferMade", Field.E_FieldType.Boo , Field.E_ClassType.Txt
                    , new BoolRepMap( true  , "Yes" )
                    , new BoolRepMap( false , "No"  )
                    );

                eB += m_Schema.Add(Category_HmdaAuditing, "Counter Offer Made By", "sHmdaCounterOfferMadeBy", Field.E_FieldType.Str , Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_HmdaAuditing, "Counter Offer Made Date", "sHmdaCounterOfferMadeD", Field.E_FieldType.Dtm , Field.E_ClassType.Cal, "M/d/yyyy");

                eB += m_Schema.Add(Category_HmdaAuditing, "Counter Offer Details", "sHmdaCounterOfferDetails", Field.E_FieldType.Str , Field.E_ClassType.LongText);

                eB += m_Schema.Add(Category_HmdaAuditing, "Borr Gender", "aBGender", Field.E_FieldType.Enu , Field.E_ClassType.Txt
                    , new EnumRepMap( E_GenderT.Male        , "Male"           )
                    , new EnumRepMap( E_GenderT.Female      , "Female"         )
                    , new EnumRepMap( E_GenderT.NA          , "Not Applicable" )
                    , new EnumRepMap( E_GenderT.Unfurnished , "Unfurnished"    )
                    , new EnumRepMap( E_GenderT.LeaveBlank  , "Left Blank"     )
                    , new EnumRepMap( E_GenderT.MaleAndFemale, "Male and Female" )
                    , new EnumRepMap(E_GenderT.MaleAndNotFurnished, "Not Furnished but Male best guess")
                    , new EnumRepMap(E_GenderT.FemaleAndNotFurnished, "Not Furnished but Female best guess")
                    , new EnumRepMap(E_GenderT.MaleFemaleNotFurnished, "Not Furnished but Male and Female best guess")
                    );

                eB += m_Schema.Add(Category_HmdaAuditing, "Borr Hispanic Type", "aBHispanicT", Field.E_FieldType.Enu , Field.E_ClassType.Txt
                    , new EnumRepMap( E_aHispanicT.Hispanic    , "Hispanic"     )
                    , new EnumRepMap( E_aHispanicT.NotHispanic , "Not Hispanic" )
                    , new EnumRepMap( E_aHispanicT.LeaveBlank  , "Left Blank"   )
                    , new EnumRepMap( E_aHispanicT.BothHispanicAndNotHispanic, "Both Hispanic or Latino and Not Hispanic or Latino")
                    );

                eB += m_Schema.Add(Category_HmdaAuditing, "Borr is American Indian", "aBIsAmericanIndian", Field.E_FieldType.Boo , Field.E_ClassType.Txt
                    , new BoolRepMap( true  , "Yes" )
                    , new BoolRepMap( false , "No"  )
                    );

                eB += m_Schema.Add(Category_HmdaAuditing, "Borr is Asian", "aBIsAsian", Field.E_FieldType.Boo , Field.E_ClassType.Txt
                    , new BoolRepMap( true  , "Yes" )
                    , new BoolRepMap( false , "No"  )
                    );

                eB += m_Schema.Add(Category_HmdaAuditing, "Borr is Black", "aBIsBlack", Field.E_FieldType.Boo , Field.E_ClassType.Txt
                    , new BoolRepMap( true  , "Yes" )
                    , new BoolRepMap( false , "No"  )
                    );

                eB += m_Schema.Add(Category_HmdaAuditing, "Borr is Pacific Islander", "aBIsPacificIslander", Field.E_FieldType.Boo , Field.E_ClassType.Txt
                    , new BoolRepMap( true  , "Yes" )
                    , new BoolRepMap( false , "No"  )
                    );

                eB += m_Schema.Add(Category_HmdaAuditing, "Borr is White", "aBIsWhite", Field.E_FieldType.Boo , Field.E_ClassType.Txt
                    , new BoolRepMap( true  , "Yes" )
                    , new BoolRepMap( false , "No"  )
                    );

                eB += m_Schema.Add(Category_HmdaAuditing, "Borr Did Not Furnish", "aBNoFurnish", Field.E_FieldType.Boo , Field.E_ClassType.Txt
                    , new BoolRepMap( true  , "Yes" )
                    , new BoolRepMap( false , "No"  )
                    );

                eB += m_Schema.Add(Category_HmdaAuditing, "Borr Other Race Description", "aBORaceDesc", Field.E_FieldType.Str , Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_HmdaAuditing, "Borr Race", "aBRaceT", Field.E_FieldType.Enu , Field.E_ClassType.Txt
                    , new EnumRepMap( E_aBRaceT.AmericanIndian , "American Indian" )
                    , new EnumRepMap( E_aBRaceT.Asian          , "Asian"           )
                    , new EnumRepMap( E_aBRaceT.Black          , "Black"           )
                    , new EnumRepMap( E_aBRaceT.Hispanic       , "Hispanic"        )
                    , new EnumRepMap( E_aBRaceT.White          , "White"           )
                    , new EnumRepMap( E_aBRaceT.Other          , "Other"           )
                    , new EnumRepMap( E_aBRaceT.NotFurnished   , "Not Furnished"   )
                    , new EnumRepMap( E_aBRaceT.NotApplicable  , "Not Applicable"  )
                    , new EnumRepMap( E_aBRaceT.LeaveBlank     , "Left Blank"      )
                    );

                eB += m_Schema.Add(Category_HmdaAuditing, "Coborr Gender", "aCGender", Field.E_FieldType.Enu , Field.E_ClassType.Txt
                    , new EnumRepMap( E_GenderT.Male        , "Male"           )
                    , new EnumRepMap( E_GenderT.Female      , "Female"         )
                    , new EnumRepMap( E_GenderT.NA          , "Not Applicable" )
                    , new EnumRepMap( E_GenderT.Unfurnished , "Unfurnished"    )
                    , new EnumRepMap( E_GenderT.LeaveBlank  , "Left Blank"     )
                    , new EnumRepMap(E_GenderT.MaleAndFemale, "Male and Female")
                    , new EnumRepMap(E_GenderT.MaleAndNotFurnished, "Not Furnished but Male best guess")
                    , new EnumRepMap(E_GenderT.FemaleAndNotFurnished, "Not Furnished but Female best guess")
                    , new EnumRepMap(E_GenderT.MaleFemaleNotFurnished, "Not Furnished but Male and Female best guess")
                    );

                eB += m_Schema.Add(Category_HmdaAuditing, "Coborr Hispanic Type", "aCHispanicT", Field.E_FieldType.Enu , Field.E_ClassType.Txt
                    , new EnumRepMap( E_aHispanicT.Hispanic    , "Hispanic"     )
                    , new EnumRepMap( E_aHispanicT.NotHispanic , "Not Hispanic" )
                    , new EnumRepMap( E_aHispanicT.LeaveBlank  , "Left Blank"   )
                    , new EnumRepMap(E_aHispanicT.BothHispanicAndNotHispanic, "Both Hispanic or Latino and Not Hispanic or Latino")
                    );

                eB += m_Schema.Add(Category_HmdaAuditing, "Coborr is American Indian", "aCIsAmericanIndian", Field.E_FieldType.Boo , Field.E_ClassType.Txt
                    , new BoolRepMap( true  , "Yes" )
                    , new BoolRepMap( false , "No"  )
                    );

                eB += m_Schema.Add(Category_HmdaAuditing, "Coborr is Asian", "aCIsAsian", Field.E_FieldType.Boo , Field.E_ClassType.Txt
                    , new BoolRepMap( true  , "Yes" )
                    , new BoolRepMap( false , "No"  )
                    );

                eB += m_Schema.Add(Category_HmdaAuditing, "Coborr is Black", "aCIsBlack", Field.E_FieldType.Boo , Field.E_ClassType.Txt
                    , new BoolRepMap( true  , "Yes" )
                    , new BoolRepMap( false , "No"  )
                    );

                eB += m_Schema.Add(Category_HmdaAuditing, "Coborr is Pacific Islander", "aCIsPacificIslander", Field.E_FieldType.Boo , Field.E_ClassType.Txt
                    , new BoolRepMap( true  , "Yes" )
                    , new BoolRepMap( false , "No"  )
                    );

                eB += m_Schema.Add(Category_HmdaAuditing, "Coborr is White", "aCIsWhite", Field.E_FieldType.Boo , Field.E_ClassType.Txt
                    , new BoolRepMap( true  , "Yes" )
                    , new BoolRepMap( false , "No"  )
                    );

                eB += m_Schema.Add(Category_HmdaAuditing, "Coborr Did Not Furnish", "aCNoFurnish", Field.E_FieldType.Boo , Field.E_ClassType.Txt
                    , new BoolRepMap( true  , "Yes" )
                    , new BoolRepMap( false , "No"  )
                    );

                eB += m_Schema.Add(Category_HmdaAuditing, "Coborr Other Race Description", "aCORaceDesc", Field.E_FieldType.Str , Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_HmdaAuditing, "Coborr Race", "aCRaceT", Field.E_FieldType.Enu , Field.E_ClassType.Txt
                    , new EnumRepMap( E_aBRaceT.AmericanIndian , "American Indian" )
                    , new EnumRepMap( E_aBRaceT.Asian          , "Asian"           )
                    , new EnumRepMap( E_aBRaceT.Black          , "Black"           )
                    , new EnumRepMap( E_aBRaceT.Hispanic       , "Hispanic"        )
                    , new EnumRepMap( E_aBRaceT.White          , "White"           )
                    , new EnumRepMap( E_aBRaceT.Other          , "Other"           )
                    , new EnumRepMap( E_aBRaceT.NotFurnished   , "Not Furnished"   )
                    , new EnumRepMap( E_aBRaceT.NotApplicable  , "Not Applicable"  )
                    , new EnumRepMap( E_aBRaceT.LeaveBlank     , "Left Blank"      )
                    );

                eB += m_Schema.Add(Category_HmdaAuditing, "Borr/Coborr Total Monthly Income", "sLTotI", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");

				eB += m_Schema.Add(Category_HmdaAuditing, "Interviewer Method", "aIntrvwrMethodT", Field.E_FieldType.Enu , Field.E_ClassType.Txt
                    , new EnumRepMap( E_aIntrvwrMethodT.FaceToFace  , "Face To Face" )
                    , new EnumRepMap( E_aIntrvwrMethodT.ByMail      , "By Mail"      )
                    , new EnumRepMap( E_aIntrvwrMethodT.ByTelephone , "By Telephone" )
                    , new EnumRepMap( E_aIntrvwrMethodT.Internet    , "Internet"     )
                    , new EnumRepMap( E_aIntrvwrMethodT.LeaveBlank  , "Left Blank"   )
                    );

                // 2nd applicant
                eB += m_Schema.Add(Category_HmdaAuditing, "Has 2nd App", "sHas2ndApplicant", Field.E_FieldType.Boo, Field.E_ClassType.Txt
                    , new BoolRepMap(true, "Yes")
                    , new BoolRepMap(false, "No")
                    );

                eB += m_Schema.Add(Category_HmdaAuditing, "2nd App Borr Did Not Furnish", "s2ndAppBNoFurnish", Field.E_FieldType.Boo, Field.E_ClassType.Txt
                    , new BoolRepMap(true, "Yes")
                    , new BoolRepMap(false, "No")
                    );

                eB += m_Schema.Add(Category_HmdaAuditing, "2nd App Borr Hispanic Type", "s2ndAppBHispanicT", Field.E_FieldType.Enu, Field.E_ClassType.Txt
                    , new EnumRepMap(E_aHispanicT.Hispanic, "Hispanic")
                    , new EnumRepMap(E_aHispanicT.NotHispanic, "Not Hispanic")
                    , new EnumRepMap(E_aHispanicT.LeaveBlank, "Left Blank")
                    , new EnumRepMap(E_aHispanicT.BothHispanicAndNotHispanic, "Both Hispanic or Latino and Not Hispanic or Latino")
                    );

                eB += m_Schema.Add(Category_HmdaAuditing, "2nd App Borr is American Indian", "s2ndAppBIsAmericanIndian", Field.E_FieldType.Boo, Field.E_ClassType.Txt
                    , new BoolRepMap(true, "Yes")
                    , new BoolRepMap(false, "No")
                    );

                eB += m_Schema.Add(Category_HmdaAuditing, "2nd App Borr is Asian", "s2ndAppBIsAsian", Field.E_FieldType.Boo, Field.E_ClassType.Txt
                    , new BoolRepMap(true, "Yes")
                    , new BoolRepMap(false, "No")
                    );

                eB += m_Schema.Add(Category_HmdaAuditing, "2nd App Borr is Black", "s2ndAppBIsBlack", Field.E_FieldType.Boo, Field.E_ClassType.Txt
                    , new BoolRepMap(true, "Yes")
                    , new BoolRepMap(false, "No")
                    );

                eB += m_Schema.Add(Category_HmdaAuditing, "2nd App Borr is Pacific Islander", "s2ndAppBIsPacificIslander", Field.E_FieldType.Boo, Field.E_ClassType.Txt
                    , new BoolRepMap(true, "Yes")
                    , new BoolRepMap(false, "No")
                    );

                eB += m_Schema.Add(Category_HmdaAuditing, "2nd App Borr is White", "s2ndAppBIsWhite", Field.E_FieldType.Boo, Field.E_ClassType.Txt
                    , new BoolRepMap(true, "Yes")
                    , new BoolRepMap(false, "No")
                    );

                eB += m_Schema.Add(Category_HmdaAuditing, "2nd App Borr Gender", "s2ndAppBGender", Field.E_FieldType.Enu, Field.E_ClassType.Txt
                    , new EnumRepMap(E_GenderT.Male, "Male")
                    , new EnumRepMap(E_GenderT.Female, "Female")
                    , new EnumRepMap(E_GenderT.NA, "Not Applicable")
                    , new EnumRepMap(E_GenderT.Unfurnished, "Unfurnished")
                    , new EnumRepMap(E_GenderT.LeaveBlank, "Left Blank")
                    , new EnumRepMap(E_GenderT.MaleAndFemale, "Male and Female")
                    , new EnumRepMap(E_GenderT.MaleAndNotFurnished, "Not Furnished but Male best guess")
                    , new EnumRepMap(E_GenderT.FemaleAndNotFurnished, "Not Furnished but Female best guess")
                    , new EnumRepMap(E_GenderT.MaleFemaleNotFurnished, "Not Furnished but Male and Female best guess")
                    );
                
                eB += m_Schema.Add(Category_HmdaAuditing, "2018 HMDA Data Complete", "sHmdaIs2018DataComplete", Field.E_FieldType.Boo, Field.E_ClassType.Txt
                    , new BoolRepMap(true, "Yes")
                    , new BoolRepMap(false, "No")
                    );

                #endregion

                // 70127
                #region ( NMLS Expanded Call Report ) 
                eB += m_Schema.Add(Category_NMLSExpandedCallReport, "NMLS Application Amount", "sNMLSApplicationAmount", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");


                eB += m_Schema.Add(Category_NMLSExpandedCallReport, "Subj Prop Occ.", "sOccT", Field.E_FieldType.Enu, Field.E_ClassType.Txt
                    , new EnumRepMap(E_sOccT.Investment, "Investment")
                    , new EnumRepMap(E_sOccT.PrimaryResidence, "Primary Residence")
                    , new EnumRepMap(E_sOccT.SecondaryResidence, "Secondary Residence")
                    );

                eB += m_Schema.Add(Category_NMLSExpandedCallReport, "Mortgage Loan Type", "sMortgageLoanT", Field.E_FieldType.Enu, Field.E_ClassType.Txt
                    , new EnumRepMap(E_MortgageLoanT.ClosedEndSecond, "Closed-End Second")
                    , new EnumRepMap(E_MortgageLoanT.Construction, "Construction")
                    , new EnumRepMap(E_MortgageLoanT.FundedHELOC, "Funded HELOC")
                    , new EnumRepMap(E_MortgageLoanT.Government_FHA_VA_RHS, "Government (FHA/VA/RHS)")
                    , new EnumRepMap(E_MortgageLoanT.Other1To4UnitResidential, "Other 1-4 Unit Residential")
                    , new EnumRepMap(E_MortgageLoanT.PrimeConforming, "Prime Conforming")
                    , new EnumRepMap(E_MortgageLoanT.PrimeNonConforming, "Prime Non-Conforming")
                    );

                eB += m_Schema.Add(Category_NMLSExpandedCallReport, "Jumbo Indicator", "sJumboT", Field.E_FieldType.Enu, Field.E_ClassType.Txt
                    , new EnumRepMap(E_JumboT.NonJumbo, "Non-Jumbo")
                    , new EnumRepMap(E_JumboT.Jumbo, "Jumbo")
                    );

                eB += m_Schema.Add(Category_NMLSExpandedCallReport, "Documentation Type", "sDocumentationT", Field.E_FieldType.Enu, Field.E_ClassType.Txt
                    , new EnumRepMap(E_DocumentationT.FullDoc, "Full Doc")
                    , new EnumRepMap(E_DocumentationT.AltDoc, "Alt Doc")
                    );

                eB += m_Schema.Add(Category_NMLSExpandedCallReport, "Has a Prepayment Penalty", "sHasPrepaymentPenalty", Field.E_FieldType.Boo, Field.E_ClassType.Txt
                    , new BoolRepMap(true, "Yes")
                    , new BoolRepMap(false, "No")
                    );

                eB += m_Schema.Add(Category_NMLSExpandedCallReport, "NMLS Loan Purpose", "sNMLSLoanPurposeT", Field.E_FieldType.Enu, Field.E_ClassType.Txt
                        , new EnumRepMap(E_NMLSLoanPurposeT.Purchase,"Purchase")
                        , new EnumRepMap(E_NMLSLoanPurposeT.RefinanceCashOutRefinance,"Refinance Cash-Out Refinance")
                        , new EnumRepMap(E_NMLSLoanPurposeT.RefinanceOtherUnknown,"Refinance Other/Unknown")
                        , new EnumRepMap(E_NMLSLoanPurposeT.RefinanceRateTerm,"Refinance Rate-Term")
                        , new EnumRepMap(E_NMLSLoanPurposeT.RefinanceRestructure,"Refinance Restructure")
                    );

                eB += m_Schema.Add(Category_NMLSExpandedCallReport, "Has Private Mortgage Insurance", "sHasPrivateMortgageInsurance", Field.E_FieldType.Boo, Field.E_ClassType.Txt
                    , new BoolRepMap(true, "Yes")
                    , new BoolRepMap(false, "No")
                    );

                eB += m_Schema.Add(Category_NMLSExpandedCallReport, "Has Piggyback Financing", "sHasPiggybackFinancing", Field.E_FieldType.Boo, Field.E_ClassType.Txt
                    , new BoolRepMap(true, "Yes")
                    , new BoolRepMap(false, "No")
                    );

                eB += m_Schema.Add(Category_NMLSExpandedCallReport, "Loan Sale Disposition Type", "sLoanSaleDispositionT", Field.E_FieldType.Enu, Field.E_ClassType.Txt
                        , new EnumRepMap(E_LoanSaleDispositionT.Blank,"Blank")
                        , new EnumRepMap(E_LoanSaleDispositionT.KeptInPortfolio_HeldForInvestment,"Kept in Portfolio/Held for Investment")
                        , new EnumRepMap(E_LoanSaleDispositionT.SoldThroughNonAgencySecuritizationWithSaleTreatment,"Sold Through Non-Agency Securitization with Sale Treatment")
                        , new EnumRepMap(E_LoanSaleDispositionT.SoldThroughNonAgencySecuritizationWithOutSaleTreatment,"Sold Through Non-Agency Securitization w/o Sale Treatment")
                        , new EnumRepMap(E_LoanSaleDispositionT.SoldToOther_Affiliate,"Sold to Other (Affiliate)")
                        , new EnumRepMap(E_LoanSaleDispositionT.SoldToOther_NonAffiliate,"Sold to Other (Non-Affiliate)")
                        , new EnumRepMap(E_LoanSaleDispositionT.SoldToSecondaryMarketAgencies_FNMA_FHLMC_GNMA, "Sold to Secondary Market Agencies (FNMA, FHLMC, GNMA)")
                    );

                eB += m_Schema.Add(Category_NMLSExpandedCallReport, "Days in Warehouse", "sDaysInWarehouse", Field.E_FieldType.Int, Field.E_ClassType.Cnt, "#,#;-#,#;0");

                eB += m_Schema.Add(Category_NMLSExpandedCallReport, "Servicing by us start date", "sServicingByUsStartD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");

                eB += m_Schema.Add(Category_NMLSExpandedCallReport, "Servicing by us end date", "sServicingByUsEndD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");

                eB += m_Schema.Add(Category_NMLSExpandedCallReport, "Servicing transfer in date", "sNMLSServicingTransferInD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");

                eB += m_Schema.Add(Category_NMLSExpandedCallReport, "Servicing transfer out date", "sNMLSServicingTransferOutD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");

                eB += m_Schema.Add(Category_NMLSExpandedCallReport, "Days delinquent period", "sNMLSPeriodForDaysDelinquentT", Field.E_FieldType.Enu, Field.E_ClassType.Txt
                    , new EnumRepMap(LendersOffice.NmlsCallReport.E_McrQuarterT.Q1, "Q1 (January-March)")
                    , new EnumRepMap(LendersOffice.NmlsCallReport.E_McrQuarterT.Q2, "Q2 (April-June)")
                    , new EnumRepMap(LendersOffice.NmlsCallReport.E_McrQuarterT.Q3, "Q3 (July-September)")
                    , new EnumRepMap(LendersOffice.NmlsCallReport.E_McrQuarterT.Q4, "Q4 (October-December)"));

                eB += m_Schema.Add(Category_NMLSExpandedCallReport, "Days delinquent as of period end", "sNMLSDaysDelinquentAsOfPeriodEnd", Field.E_FieldType.Int, Field.E_ClassType.Cnt, "#,#;-#,#;0");

                eB += m_Schema.Add(Category_NMLSExpandedCallReport, "NMLS servicing intent", "sNMLSServicingIntentT", Field.E_FieldType.Enu, Field.E_ClassType.Txt
                    , new EnumRepMap(E_ServicingStatus.Released, "sell")
                    , new EnumRepMap(E_ServicingStatus.Retained, "retain")
                    );

                // OPM 219346, 9/16/2015, ML
                eB += m_Schema.Add(Category_NMLSExpandedCallReport, "Net Change in Application Amount", "sNMLSNetChangeApplicationAmount", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");

                eB += m_Schema.Add(Category_NMLSExpandedCallReport, "NMLS Application Date", "sNmlsApplicationDate", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyy");

                // OPM 237867, 2/10/2017, ML
                eB += m_Schema.Add(Category_NMLSExpandedCallReport, "NMLS LTV", "sNMLSLtvR", Field.E_FieldType.Dec, Field.E_ClassType.Pct, "N3");
                eB += m_Schema.Add(Category_NMLSExpandedCallReport, "NMLS CLTV", "sNMLSCLtvR", Field.E_FieldType.Dec, Field.E_ClassType.Pct, "N3");
                #endregion

                #region ( Calendar Date Macros )

                eB += m_Schema.Mac(new Today());
                eB += m_Schema.Mac(new Yesterday());
                eB += m_Schema.Mac(new Tomorrow());
                eB += m_Schema.Mac( new ThisMonday());
                eB += m_Schema.Mac(new LastMonday());
                eB += m_Schema.Mac(new NextMonday());
                eB += m_Schema.Mac( new ThisFriday());                
                eB += m_Schema.Mac( new LastFriday());                
                eB += m_Schema.Mac( new NextFriday());
                eB += m_Schema.Mac( new ThisMonthBeginning());
                eB += m_Schema.Mac( new ThisMonthEnd());
                eB += m_Schema.Mac( new LastMonthBeginning());                
                eB += m_Schema.Mac( new LastMonthEnd());
                eB += m_Schema.Mac( new NextMonthBeginning());
                eB += m_Schema.Mac( new NextMonthEnd());
                eB += m_Schema.Mac(new CurrentQuarterBeginning());
                eB += m_Schema.Mac(new CurrentQuarterEnd());
                eB += m_Schema.Mac(new LastQuarterBeginning());
                eB += m_Schema.Mac(new LastQuarterEnd());
                eB += m_Schema.Mac(new ThisYearBeginning());
                eB += m_Schema.Mac(new ThisYearEnd());
                eB += m_Schema.Mac( new LastYearBeginning());
                eB += m_Schema.Mac( new LastYearEnd());
                
                for (int i = 1; i <= 30; i++)
                {
                    eB += m_Schema.Mac(new LastBusinessDays(i));
                }

                eB += m_Schema.Mac(new LastCalendarDays(5));
                eB += m_Schema.Mac(new LastCalendarDays(10));
                eB += m_Schema.Mac(new LastCalendarDays(15));
                eB += m_Schema.Mac(new LastCalendarDays(30));
                eB += m_Schema.Mac(new LastCalendarDays(60));
                eB += m_Schema.Mac(new LastCalendarDays(90));

                #endregion

                #region ( PML Only )
                eB += m_Schema.Add(Category_PML, "Price Group", "sProdLpePriceGroupNm", Field.E_FieldType.Str, Field.E_ClassType.Txt); //opm 27079 fs 09/24/09
                eB += m_Schema.Add(Category_PML, "Is Rural", "sProdIsSpInRuralArea", Field.E_FieldType.Boo , Field.E_ClassType.Txt
                    , new BoolRepMap( true  , "Yes" )
                    , new BoolRepMap( false , "No"  )
                    );
                eB += m_Schema.Add(Category_PML, "Lender Fee Buyout", "sLenderFeeBuyoutRequestedT", Field.E_FieldType.Enu, Field.E_ClassType.Txt
                    , new EnumRepMap(E_sLenderFeeBuyoutRequestedT.No, "No")
                    , new EnumRepMap(E_sLenderFeeBuyoutRequestedT.Yes, "Yes")
                    , new EnumRepMap(E_sLenderFeeBuyoutRequestedT.Waived, "Waived")
                    );

                eB += m_Schema.Add(Category_PML, "Reserves Available", "sProdAvailReserveMonths", Field.E_FieldType.Int, Field.E_ClassType.Cnt, "#,#;-#,#;0");
                eB += m_Schema.Add(Category_PML, "Cashout Amount", "sProdCashoutAmt", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_PML, "AU Response", "sProd3rdPartyUwResultT",  Field.E_FieldType.Enu, Field.E_ClassType.Txt
                    , new EnumRepMap(E_sProd3rdPartyUwResultT.NA, "None/Not Submitted")
                    , new EnumRepMap(E_sProd3rdPartyUwResultT.DU_ApproveEligible, "DU Approve/Eligible")
                    , new EnumRepMap(E_sProd3rdPartyUwResultT.DU_ApproveIneligible, "DU Approve/Ineligible")
                    , new EnumRepMap(E_sProd3rdPartyUwResultT.DU_ReferEligible, "DU Refer/Eligible")
                    , new EnumRepMap(E_sProd3rdPartyUwResultT.DU_ReferIneligible, "DU Refer/Ineligible")
                    , new EnumRepMap(E_sProd3rdPartyUwResultT.DU_ReferWCautionEligible, "DU Refer with Caution/Eligible")
                    , new EnumRepMap(E_sProd3rdPartyUwResultT.DU_ReferWCautionIneligible, "DU Refer with Caution/Ineligible")
                    , new EnumRepMap(E_sProd3rdPartyUwResultT.DU_EAIEligible, "DU EA I/Eligible")
                    , new EnumRepMap(E_sProd3rdPartyUwResultT.DU_EAIIEligible, "DU EA II/Eligible")
                    , new EnumRepMap(E_sProd3rdPartyUwResultT.DU_EAIIIEligible, "DU EA III/Eligible")
                    , new EnumRepMap(E_sProd3rdPartyUwResultT.LP_AcceptEligible, "LP Accept/Eligible")
                    , new EnumRepMap(E_sProd3rdPartyUwResultT.LP_AcceptIneligible, "LP Accept/Ineligible")
                    , new EnumRepMap(E_sProd3rdPartyUwResultT.LP_CautionEligible, "LP Caution/Eligible")
                    , new EnumRepMap(E_sProd3rdPartyUwResultT.LP_CautionIneligible, "LP Caution/Ineligible")
                    , new EnumRepMap(E_sProd3rdPartyUwResultT.Lp_AMinus_Level1, "LP A- Level 1")
                    , new EnumRepMap(E_sProd3rdPartyUwResultT.Lp_AMinus_Level2, "LP A- Level 2")
                    , new EnumRepMap(E_sProd3rdPartyUwResultT.Lp_AMinus_Level3, "LP A- Level 3")
                    , new EnumRepMap(E_sProd3rdPartyUwResultT.Lp_AMinus_Level4, "LP A- Level 4")
                    , new EnumRepMap(E_sProd3rdPartyUwResultT.Lp_AMinus_Level5, "LP A- Level 5")	
                    , new EnumRepMap(E_sProd3rdPartyUwResultT.Lp_Refer, "LP Refer")
                    , new EnumRepMap(E_sProd3rdPartyUwResultT.OutOfScope, "Out of Scope")
                    , new EnumRepMap(E_sProd3rdPartyUwResultT.Total_ApproveEligible, "TOTAL Approve/Eligible")
                    , new EnumRepMap(E_sProd3rdPartyUwResultT.Total_ApproveIneligible, "TOTAL Approve/Ineligible")
                    , new EnumRepMap(E_sProd3rdPartyUwResultT.Total_ReferEligible, "TOTAL Refer/Eligible")
                    , new EnumRepMap(E_sProd3rdPartyUwResultT.Total_ReferIneligible, "TOTAL Refer/Ineligible")
                    , new EnumRepMap(E_sProd3rdPartyUwResultT.GUS_AcceptEligible, "GUS Accept/Eligible")
                    , new EnumRepMap(E_sProd3rdPartyUwResultT.GUS_AcceptIneligible, "GUS Accept/Ineligible")
                    , new EnumRepMap(E_sProd3rdPartyUwResultT.GUS_ReferEligible, "GUS Refer/Eligible")
                    , new EnumRepMap(E_sProd3rdPartyUwResultT.GUS_ReferIneligible, "GUS Refer/Ineligible")
                    , new EnumRepMap(E_sProd3rdPartyUwResultT.GUS_ReferWCautionEligible, "GUS Refer with Caution/Eligible")
                    , new EnumRepMap(E_sProd3rdPartyUwResultT.GUS_ReferWCautionIneligible, "GUS Refer with Caution/Ineligible")
                    );
                eB += m_Schema.Add(Category_PML, "Structure Type", "sProdSpStructureT", Field.E_FieldType.Enu, Field.E_ClassType.Txt
                    , new EnumRepMap(E_sProdSpStructureT.Attached, "Attached")
                    , new EnumRepMap(E_sProdSpStructureT.Detached, "Detached")
                    );
                eB += m_Schema.Add(Category_PML, "Doc Type", "sProdDocT", Field.E_FieldType.Enu , Field.E_ClassType.Txt
                    , new EnumRepMap( E_sProdDocT.Full   , "Full Document"       )
                    , new EnumRepMap( E_sProdDocT.Alt    , "Alt"                 )
                    , new EnumRepMap( E_sProdDocT.Light  , "Lite"                )
                    , new EnumRepMap( E_sProdDocT.NINA   , "NINA"                )
                    , new EnumRepMap( E_sProdDocT.NIVA   , "No Ratio - No Income, Verified Assets")
                    , new EnumRepMap( E_sProdDocT.NISA   , "NISA"                )
                    , new EnumRepMap( E_sProdDocT.SISA   , "NIV (SISA)"          )
                    , new EnumRepMap( E_sProdDocT.SIVA   , "NIV (SIVA)"          )
                    , new EnumRepMap( E_sProdDocT.VISA   , "VISA"                )
                    , new EnumRepMap( E_sProdDocT.NINANE , "No Doc"              )
                    , new EnumRepMap( E_sProdDocT.NIVANE , "No Doc Verif Assets" )
                    , new EnumRepMap( E_sProdDocT.VINA   , "VINA"                )
                    , new EnumRepMap( E_sProdDocT.Streamline, "Streamline")
                    , new EnumRepMap(E_sProdDocT._12MoPersonalBankStatements, "12 Mo. Personal Bank Statements")
                    , new EnumRepMap(E_sProdDocT._24MoPersonalBankStatements, "24 Mo. Personal Bank Statements")
                    , new EnumRepMap(E_sProdDocT._12MoBusinessBankStatements, "12 Mo. Business Bank Statements")
                    , new EnumRepMap(E_sProdDocT._24MoBusinessBankStatements, "24 Mo. Business Bank Statements")
                    , new EnumRepMap(E_sProdDocT.OtherBankStatements, "Other Bank Statements")
                    , new EnumRepMap(E_sProdDocT._1YrTaxReturns, "1 Yr. Tax Returns")
                    , new EnumRepMap(E_sProdDocT.Voe, "VOE")
                    , new EnumRepMap(E_sProdDocT.AssetUtilization, "Asset Utilization")
                    , new EnumRepMap(E_sProdDocT.DebtServiceCoverage, "Debt Service Coverage (DSCR)")
                    , new EnumRepMap(E_sProdDocT.NoIncome, "No Ratio"));
                eB += m_Schema.Add(Category_PML, "Subj Prop Type", "sProdSpT", Field.E_FieldType.Enu , Field.E_ClassType.Txt
                    , new EnumRepMap( E_sProdSpT.SFR          , "SFR" )
                    , new EnumRepMap( E_sProdSpT.TwoUnits     , "2 Units"        )
                    , new EnumRepMap( E_sProdSpT.ThreeUnits   , "3 Units"        )
                    , new EnumRepMap( E_sProdSpT.FourUnits    , "4 Units"        )
                    , new EnumRepMap( E_sProdSpT.PUD          , "PUD"            )
                    , new EnumRepMap( E_sProdSpT.Commercial   , "Commercial"     )
                    , new EnumRepMap( E_sProdSpT.Condo        , "Condo"          )
                    , new EnumRepMap( E_sProdSpT.CoOp         , "Co-Op"          )
                    , new EnumRepMap( E_sProdSpT.Manufactured , "Manufactured"   )
                    , new EnumRepMap( E_sProdSpT.MixedUse     , "Mixed Use"      )
                    , new EnumRepMap( E_sProdSpT.Townhouse    , "Attached PUD"   )
                    , new EnumRepMap( E_sProdSpT.Modular      , "Modular"        )
                    , new EnumRepMap( E_sProdSpT.Rowhouse     , "Rowhouse"       )
                    );
                eB += m_Schema.Add(Category_PML, "Is Impound", "sProdImpound", Field.E_FieldType.Boo, Field.E_ClassType.Txt
                    , new BoolRepMap(true, "Yes"), new BoolRepMap(false, "No"));
                eB += m_Schema.Add(Category_PML, "Borrower Citizenship", "aProdBCitizenT", Field.E_FieldType.Enu, Field.E_ClassType.Txt
                    , new EnumRepMap(E_aProdCitizenT.USCitizen, "US Citizen")
                    , new EnumRepMap(E_aProdCitizenT.PermanentResident, "Permanent Resident")
                    , new EnumRepMap(E_aProdCitizenT.NonpermanentResident, "Non-permanent Resident")
                    , new EnumRepMap(E_aProdCitizenT.ForeignNational, "Non-Resident Alien (Foreign National)"));
                eB += m_Schema.Add(Category_PML, "CoBorrower Citizenship", "aProdCCitizenT", Field.E_FieldType.Enu, Field.E_ClassType.Txt
                    , new EnumRepMap(E_aProdCitizenT.USCitizen, "US Citizen")
                    , new EnumRepMap(E_aProdCitizenT.PermanentResident, "Permanent Resident")
                    , new EnumRepMap(E_aProdCitizenT.NonpermanentResident, "Non-permanent Resident")
                    , new EnumRepMap(E_aProdCitizenT.ForeignNational, "Non-Resident Alien (Foreign National)"));

                eB += m_Schema.Add(Category_PML, "FTHB", "sHas1stTimeBuyer", Field.E_FieldType.Boo, Field.E_ClassType.Txt
                    , new BoolRepMap(true, "Yes")
                    , new BoolRepMap(false, "No")
                );

                #endregion

                #region ( Credit Information ) 
                eB += m_Schema.Add(Category_CreditInformation, "Mortgage Lates 30 in Last 12 Months", "aMortgageLates30In12MonthsPmlClientReport", Field.E_FieldType.Int , Field.E_ClassType.Cnt, "#,#;-#,#;0");
                eB += m_Schema.Add(Category_CreditInformation, "Mortgage Lates 60 in Last 12 Months", "aMortgageLates60In12MonthsPmlClientReport", Field.E_FieldType.Int , Field.E_ClassType.Cnt, "#,#;-#,#;0");
                eB += m_Schema.Add(Category_CreditInformation, "Mortgage Lates 90 in Last 12 Months", "aMortgageLates90In12MonthsPmlClientReport", Field.E_FieldType.Int , Field.E_ClassType.Cnt, "#,#;-#,#;0");
                eB += m_Schema.Add(Category_CreditInformation, "Mortgage Lates 120 in Last 12 Months", "aMortgageLates120In12MonthsPmlClientReport", Field.E_FieldType.Int , Field.E_ClassType.Cnt, "#,#;-#,#;0");

                eB += m_Schema.Add(Category_CreditInformation, "Latest Foreclosure Filed Date", "aFileDOfLastForeclosure", Field.E_FieldType.Dtm , Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_CreditInformation, "Latest Foreclosure Discharged Date", "aDischargedDOfLastForeclosesure", Field.E_FieldType.Dtm , Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_CreditInformation, "Latest Bk 7 Filed Date", "aFileDOfLastBk7", Field.E_FieldType.Dtm , Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_CreditInformation, "Latest Bk 7 Discharged Date", "aDischargedDOfLastBk7", Field.E_FieldType.Dtm , Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_CreditInformation, "Latest Bk 13 Filed Date", "aFileDOfLastBk13", Field.E_FieldType.Dtm , Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_CreditInformation, "Latest Bk 13 Discharged Date", "aDischargedDOfLastBk13", Field.E_FieldType.Dtm , Field.E_ClassType.Cal, "M/d/yyyy");

                #endregion

                #region ( Custom Fields )
				// 11/01/07 mf. OPM 18245.  We add 20 sets of Custom Fields.
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 1 Description", "sCustomField1Desc", Field.E_FieldType.Str , Field.E_ClassType.Txt);
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 1 Date", "sCustomField1D", Field.E_FieldType.Dtm , Field.E_ClassType.Cal, "M/d/yyyy");
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 1 Amount", "sCustomField1Money", Field.E_FieldType.Dec , Field.E_ClassType.Csh, "C");
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 1 Percent", "sCustomField1Pc", Field.E_FieldType.Dec , Field.E_ClassType.Pct, "N3");
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 1 Checked", "sCustomField1Bit", Field.E_FieldType.Boo , Field.E_ClassType.Txt
					, new BoolRepMap( true  , "Yes" )
					, new BoolRepMap( false , "No"  )
					);
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 1 Notes", "sCustomField1Notes", Field.E_FieldType.Str , Field.E_ClassType.Txt);

				eB += m_Schema.Add(Category_CustomFields, "Custom Field 2 Description", "sCustomField2Desc", Field.E_FieldType.Str , Field.E_ClassType.Txt);
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 2 Date", "sCustomField2D", Field.E_FieldType.Dtm , Field.E_ClassType.Cal, "M/d/yyyy");
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 2 Amount", "sCustomField2Money", Field.E_FieldType.Dec , Field.E_ClassType.Csh, "C");
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 2 Percent", "sCustomField2Pc", Field.E_FieldType.Dec , Field.E_ClassType.Pct, "N3");
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 2 Checked", "sCustomField2Bit", Field.E_FieldType.Boo , Field.E_ClassType.Txt
					, new BoolRepMap( true  , "Yes" )
					, new BoolRepMap( false , "No"  )
					);
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 2 Notes", "sCustomField2Notes", Field.E_FieldType.Str , Field.E_ClassType.Txt);

				eB += m_Schema.Add(Category_CustomFields, "Custom Field 3 Description", "sCustomField3Desc", Field.E_FieldType.Str , Field.E_ClassType.Txt);
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 3 Date", "sCustomField3D", Field.E_FieldType.Dtm , Field.E_ClassType.Cal, "M/d/yyyy");
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 3 Amount", "sCustomField3Money", Field.E_FieldType.Dec , Field.E_ClassType.Csh, "C");
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 3 Percent", "sCustomField3Pc", Field.E_FieldType.Dec , Field.E_ClassType.Pct, "N3");
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 3 Checked", "sCustomField3Bit", Field.E_FieldType.Boo , Field.E_ClassType.Txt
					, new BoolRepMap( true  , "Yes" )
					, new BoolRepMap( false , "No"  )
					);
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 3 Notes", "sCustomField3Notes", Field.E_FieldType.Str , Field.E_ClassType.Txt);

				eB += m_Schema.Add(Category_CustomFields, "Custom Field 4 Description", "sCustomField4Desc", Field.E_FieldType.Str , Field.E_ClassType.Txt);
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 4 Date", "sCustomField4D", Field.E_FieldType.Dtm , Field.E_ClassType.Cal, "M/d/yyyy");
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 4 Amount", "sCustomField4Money", Field.E_FieldType.Dec , Field.E_ClassType.Csh, "C");
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 4 Percent", "sCustomField4Pc", Field.E_FieldType.Dec , Field.E_ClassType.Pct, "N3");
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 4 Checked", "sCustomField4Bit", Field.E_FieldType.Boo , Field.E_ClassType.Txt
					, new BoolRepMap( true  , "Yes" )
					, new BoolRepMap( false , "No"  )
					);
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 4 Notes", "sCustomField4Notes", Field.E_FieldType.Str , Field.E_ClassType.Txt);

				eB += m_Schema.Add(Category_CustomFields, "Custom Field 5 Description", "sCustomField5Desc", Field.E_FieldType.Str , Field.E_ClassType.Txt);
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 5 Date", "sCustomField5D", Field.E_FieldType.Dtm , Field.E_ClassType.Cal, "M/d/yyyy");
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 5 Amount", "sCustomField5Money", Field.E_FieldType.Dec , Field.E_ClassType.Csh, "C");
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 5 Percent", "sCustomField5Pc", Field.E_FieldType.Dec , Field.E_ClassType.Pct, "N3");
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 5 Checked", "sCustomField5Bit", Field.E_FieldType.Boo , Field.E_ClassType.Txt
					, new BoolRepMap( true  , "Yes" )
					, new BoolRepMap( false , "No"  )
					);
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 5 Notes", "sCustomField5Notes", Field.E_FieldType.Str , Field.E_ClassType.Txt);

				eB += m_Schema.Add(Category_CustomFields, "Custom Field 6 Description", "sCustomField6Desc", Field.E_FieldType.Str , Field.E_ClassType.Txt);
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 6 Date", "sCustomField6D", Field.E_FieldType.Dtm , Field.E_ClassType.Cal, "M/d/yyyy");
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 6 Amount", "sCustomField6Money", Field.E_FieldType.Dec , Field.E_ClassType.Csh, "C");
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 6 Percent", "sCustomField6Pc", Field.E_FieldType.Dec , Field.E_ClassType.Pct, "N3");
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 6 Checked", "sCustomField6Bit", Field.E_FieldType.Boo , Field.E_ClassType.Txt
					, new BoolRepMap( true  , "Yes" )
					, new BoolRepMap( false , "No"  )
					);
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 6 Notes", "sCustomField6Notes", Field.E_FieldType.Str , Field.E_ClassType.Txt);

				eB += m_Schema.Add(Category_CustomFields, "Custom Field 7 Description", "sCustomField7Desc", Field.E_FieldType.Str , Field.E_ClassType.Txt);
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 7 Date", "sCustomField7D", Field.E_FieldType.Dtm , Field.E_ClassType.Cal, "M/d/yyyy");
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 7 Amount", "sCustomField7Money", Field.E_FieldType.Dec , Field.E_ClassType.Csh, "C");
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 7 Percent", "sCustomField7Pc", Field.E_FieldType.Dec , Field.E_ClassType.Pct, "N3");
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 7 Checked", "sCustomField7Bit", Field.E_FieldType.Boo , Field.E_ClassType.Txt
					, new BoolRepMap( true  , "Yes" )
					, new BoolRepMap( false , "No"  )
					);
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 7 Notes", "sCustomField7Notes", Field.E_FieldType.Str , Field.E_ClassType.Txt);

				eB += m_Schema.Add(Category_CustomFields, "Custom Field 8 Description", "sCustomField8Desc", Field.E_FieldType.Str , Field.E_ClassType.Txt);
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 8 Date", "sCustomField8D", Field.E_FieldType.Dtm , Field.E_ClassType.Cal, "M/d/yyyy");
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 8 Amount", "sCustomField8Money", Field.E_FieldType.Dec , Field.E_ClassType.Csh, "C");
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 8 Percent", "sCustomField8Pc", Field.E_FieldType.Dec , Field.E_ClassType.Pct, "N3");
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 8 Checked", "sCustomField8Bit", Field.E_FieldType.Boo , Field.E_ClassType.Txt
					, new BoolRepMap( true  , "Yes" )
					, new BoolRepMap( false , "No"  )
					);
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 8 Notes", "sCustomField8Notes", Field.E_FieldType.Str , Field.E_ClassType.Txt);

				eB += m_Schema.Add(Category_CustomFields, "Custom Field 9 Description", "sCustomField9Desc", Field.E_FieldType.Str , Field.E_ClassType.Txt);
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 9 Date", "sCustomField9D", Field.E_FieldType.Dtm , Field.E_ClassType.Cal, "M/d/yyyy");
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 9 Amount", "sCustomField9Money", Field.E_FieldType.Dec , Field.E_ClassType.Csh, "C");
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 9 Percent", "sCustomField9Pc", Field.E_FieldType.Dec , Field.E_ClassType.Pct, "N3");
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 9 Checked", "sCustomField9Bit", Field.E_FieldType.Boo , Field.E_ClassType.Txt
					, new BoolRepMap( true  , "Yes" )
					, new BoolRepMap( false , "No"  )
					);
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 9 Notes", "sCustomField9Notes", Field.E_FieldType.Str , Field.E_ClassType.Txt);

				eB += m_Schema.Add(Category_CustomFields, "Custom Field 10 Description", "sCustomField10Desc", Field.E_FieldType.Str , Field.E_ClassType.Txt);
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 10 Date", "sCustomField10D", Field.E_FieldType.Dtm , Field.E_ClassType.Cal, "M/d/yyyy");
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 10 Amount", "sCustomField10Money", Field.E_FieldType.Dec , Field.E_ClassType.Csh, "C");
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 10 Percent", "sCustomField10Pc", Field.E_FieldType.Dec , Field.E_ClassType.Pct, "N3");
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 10 Checked", "sCustomField10Bit", Field.E_FieldType.Boo , Field.E_ClassType.Txt
					, new BoolRepMap( true  , "Yes" )
					, new BoolRepMap( false , "No"  )
					);
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 10 Notes", "sCustomField10Notes", Field.E_FieldType.Str , Field.E_ClassType.Txt);

				eB += m_Schema.Add(Category_CustomFields, "Custom Field 11 Description", "sCustomField11Desc", Field.E_FieldType.Str , Field.E_ClassType.Txt);
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 11 Date", "sCustomField11D", Field.E_FieldType.Dtm , Field.E_ClassType.Cal, "M/d/yyyy");
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 11 Amount", "sCustomField11Money", Field.E_FieldType.Dec , Field.E_ClassType.Csh, "C");
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 11 Percent", "sCustomField11Pc", Field.E_FieldType.Dec , Field.E_ClassType.Pct, "N3");
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 11 Checked", "sCustomField11Bit", Field.E_FieldType.Boo , Field.E_ClassType.Txt
					, new BoolRepMap( true  , "Yes" )
					, new BoolRepMap( false , "No"  )
					);
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 11 Notes", "sCustomField11Notes", Field.E_FieldType.Str , Field.E_ClassType.Txt);

				eB += m_Schema.Add(Category_CustomFields, "Custom Field 12 Description", "sCustomField12Desc", Field.E_FieldType.Str , Field.E_ClassType.Txt);
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 12 Date", "sCustomField12D", Field.E_FieldType.Dtm , Field.E_ClassType.Cal, "M/d/yyyy");
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 12 Amount", "sCustomField12Money", Field.E_FieldType.Dec , Field.E_ClassType.Csh, "C");
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 12 Percent", "sCustomField12Pc", Field.E_FieldType.Dec , Field.E_ClassType.Pct, "N3");
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 12 Checked", "sCustomField12Bit", Field.E_FieldType.Boo , Field.E_ClassType.Txt
					, new BoolRepMap( true  , "Yes" )
					, new BoolRepMap( false , "No"  )
					);
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 12 Notes", "sCustomField12Notes", Field.E_FieldType.Str , Field.E_ClassType.Txt);

				eB += m_Schema.Add(Category_CustomFields, "Custom Field 13 Description", "sCustomField13Desc", Field.E_FieldType.Str , Field.E_ClassType.Txt);
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 13 Date", "sCustomField13D", Field.E_FieldType.Dtm , Field.E_ClassType.Cal, "M/d/yyyy");
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 13 Amount", "sCustomField13Money", Field.E_FieldType.Dec , Field.E_ClassType.Csh, "C");
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 13 Percent", "sCustomField13Pc", Field.E_FieldType.Dec , Field.E_ClassType.Pct, "N3");
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 13 Checked", "sCustomField13Bit", Field.E_FieldType.Boo , Field.E_ClassType.Txt
					, new BoolRepMap( true  , "Yes" )
					, new BoolRepMap( false , "No"  )
					);
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 13 Notes", "sCustomField13Notes", Field.E_FieldType.Str , Field.E_ClassType.Txt);

				eB += m_Schema.Add(Category_CustomFields, "Custom Field 14 Description", "sCustomField14Desc", Field.E_FieldType.Str , Field.E_ClassType.Txt);
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 14 Date", "sCustomField14D", Field.E_FieldType.Dtm , Field.E_ClassType.Cal, "M/d/yyyy");
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 14 Amount", "sCustomField14Money", Field.E_FieldType.Dec , Field.E_ClassType.Csh, "C");
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 14 Percent", "sCustomField14Pc", Field.E_FieldType.Dec , Field.E_ClassType.Pct, "N3");
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 14 Checked", "sCustomField14Bit", Field.E_FieldType.Boo , Field.E_ClassType.Txt
					, new BoolRepMap( true  , "Yes" )
					, new BoolRepMap( false , "No"  )
					);
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 14 Notes", "sCustomField14Notes", Field.E_FieldType.Str , Field.E_ClassType.Txt);

				eB += m_Schema.Add(Category_CustomFields, "Custom Field 15 Description", "sCustomField15Desc", Field.E_FieldType.Str , Field.E_ClassType.Txt);
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 15 Date", "sCustomField15D", Field.E_FieldType.Dtm , Field.E_ClassType.Cal, "M/d/yyyy");
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 15 Amount", "sCustomField15Money", Field.E_FieldType.Dec , Field.E_ClassType.Csh, "C");
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 15 Percent", "sCustomField15Pc", Field.E_FieldType.Dec , Field.E_ClassType.Pct, "N3");
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 15 Checked", "sCustomField15Bit", Field.E_FieldType.Boo , Field.E_ClassType.Txt
					, new BoolRepMap( true  , "Yes" )
					, new BoolRepMap( false , "No"  )
					);
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 15 Notes", "sCustomField15Notes", Field.E_FieldType.Str , Field.E_ClassType.Txt);

				eB += m_Schema.Add(Category_CustomFields, "Custom Field 16 Description", "sCustomField16Desc", Field.E_FieldType.Str , Field.E_ClassType.Txt);
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 16 Date", "sCustomField16D", Field.E_FieldType.Dtm , Field.E_ClassType.Cal, "M/d/yyyy");
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 16 Amount", "sCustomField16Money", Field.E_FieldType.Dec , Field.E_ClassType.Csh, "C");
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 16 Percent", "sCustomField16Pc", Field.E_FieldType.Dec , Field.E_ClassType.Pct, "N3");
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 16 Checked", "sCustomField16Bit", Field.E_FieldType.Boo , Field.E_ClassType.Txt
					, new BoolRepMap( true  , "Yes" )
					, new BoolRepMap( false , "No"  )
					);
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 16 Notes", "sCustomField16Notes", Field.E_FieldType.Str , Field.E_ClassType.Txt);

				eB += m_Schema.Add(Category_CustomFields, "Custom Field 17 Description", "sCustomField17Desc", Field.E_FieldType.Str , Field.E_ClassType.Txt);
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 17 Date", "sCustomField17D", Field.E_FieldType.Dtm , Field.E_ClassType.Cal, "M/d/yyyy");
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 17 Amount", "sCustomField17Money", Field.E_FieldType.Dec , Field.E_ClassType.Csh, "C");
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 17 Percent", "sCustomField17Pc", Field.E_FieldType.Dec , Field.E_ClassType.Pct, "N3");
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 17 Checked", "sCustomField17Bit", Field.E_FieldType.Boo , Field.E_ClassType.Txt
					, new BoolRepMap( true  , "Yes" )
					, new BoolRepMap( false , "No"  )
					);
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 17 Notes", "sCustomField17Notes", Field.E_FieldType.Str , Field.E_ClassType.Txt);

				eB += m_Schema.Add(Category_CustomFields, "Custom Field 18 Description", "sCustomField18Desc", Field.E_FieldType.Str , Field.E_ClassType.Txt);
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 18 Date", "sCustomField18D", Field.E_FieldType.Dtm , Field.E_ClassType.Cal, "M/d/yyyy");
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 18 Amount", "sCustomField18Money", Field.E_FieldType.Dec , Field.E_ClassType.Csh, "C");
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 18 Percent", "sCustomField18Pc", Field.E_FieldType.Dec , Field.E_ClassType.Pct, "N3");
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 18 Checked", "sCustomField18Bit", Field.E_FieldType.Boo , Field.E_ClassType.Txt
					, new BoolRepMap( true  , "Yes" )
					, new BoolRepMap( false , "No"  )
					);
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 18 Notes", "sCustomField18Notes", Field.E_FieldType.Str , Field.E_ClassType.Txt);

				eB += m_Schema.Add(Category_CustomFields, "Custom Field 19 Description", "sCustomField19Desc", Field.E_FieldType.Str , Field.E_ClassType.Txt);
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 19 Date", "sCustomField19D", Field.E_FieldType.Dtm , Field.E_ClassType.Cal, "M/d/yyyy");
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 19 Amount", "sCustomField19Money", Field.E_FieldType.Dec , Field.E_ClassType.Csh, "C");
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 19 Percent", "sCustomField19Pc", Field.E_FieldType.Dec , Field.E_ClassType.Pct, "N3");
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 19 Checked", "sCustomField19Bit", Field.E_FieldType.Boo , Field.E_ClassType.Txt
					, new BoolRepMap( true  , "Yes" )
					, new BoolRepMap( false , "No"  )
					);
				eB += m_Schema.Add(Category_CustomFields, "Custom Field 19 Notes", "sCustomField19Notes", Field.E_FieldType.Str , Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_CustomFields, "Custom Field 20 Description", "sCustomField20Desc", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 20 Date", "sCustomField20D", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 20 Amount", "sCustomField20Money", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 20 Percent", "sCustomField20Pc", Field.E_FieldType.Dec, Field.E_ClassType.Pct, "N3");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 20 Checked", "sCustomField20Bit", Field.E_FieldType.Boo, Field.E_ClassType.Txt
                    , new BoolRepMap(true, "Yes")
                    , new BoolRepMap(false, "No")
                    );
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 20 Notes", "sCustomField20Notes", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_CustomFields, "Custom Field 21 Description", "sCustomField21Desc", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 21 Date", "sCustomField21D", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 21 Amount", "sCustomField21Money", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 21 Percent", "sCustomField21Pc", Field.E_FieldType.Dec, Field.E_ClassType.Pct, "N3");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 21 Checked", "sCustomField21Bit", Field.E_FieldType.Boo, Field.E_ClassType.Txt
                    , new BoolRepMap(true, "Yes")
                    , new BoolRepMap(false, "No")
                    );
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 21 Notes", "sCustomField21Notes", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_CustomFields, "Custom Field 22 Description", "sCustomField22Desc", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 22 Date", "sCustomField22D", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 22 Amount", "sCustomField22Money", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 22 Percent", "sCustomField22Pc", Field.E_FieldType.Dec, Field.E_ClassType.Pct, "N3");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 22 Checked", "sCustomField22Bit", Field.E_FieldType.Boo, Field.E_ClassType.Txt
                    , new BoolRepMap(true, "Yes")
                    , new BoolRepMap(false, "No")
                    );
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 22 Notes", "sCustomField22Notes", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_CustomFields, "Custom Field 23 Description", "sCustomField23Desc", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 23 Date", "sCustomField23D", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 23 Amount", "sCustomField23Money", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 23 Percent", "sCustomField23Pc", Field.E_FieldType.Dec, Field.E_ClassType.Pct, "N3");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 23 Checked", "sCustomField23Bit", Field.E_FieldType.Boo, Field.E_ClassType.Txt
                    , new BoolRepMap(true, "Yes")
                    , new BoolRepMap(false, "No")
                    );
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 23 Notes", "sCustomField23Notes", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_CustomFields, "Custom Field 24 Description", "sCustomField24Desc", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 24 Date", "sCustomField24D", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 24 Amount", "sCustomField24Money", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 24 Percent", "sCustomField24Pc", Field.E_FieldType.Dec, Field.E_ClassType.Pct, "N3");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 24 Checked", "sCustomField24Bit", Field.E_FieldType.Boo, Field.E_ClassType.Txt
                    , new BoolRepMap(true, "Yes")
                    , new BoolRepMap(false, "No")
                    );
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 24 Notes", "sCustomField24Notes", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_CustomFields, "Custom Field 25 Description", "sCustomField25Desc", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 25 Date", "sCustomField25D", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 25 Amount", "sCustomField25Money", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 25 Percent", "sCustomField25Pc", Field.E_FieldType.Dec, Field.E_ClassType.Pct, "N3");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 25 Checked", "sCustomField25Bit", Field.E_FieldType.Boo, Field.E_ClassType.Txt
                    , new BoolRepMap(true, "Yes")
                    , new BoolRepMap(false, "No")
                    );
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 25 Notes", "sCustomField25Notes", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_CustomFields, "Custom Field 26 Description", "sCustomField26Desc", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 26 Date", "sCustomField26D", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 26 Amount", "sCustomField26Money", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 26 Percent", "sCustomField26Pc", Field.E_FieldType.Dec, Field.E_ClassType.Pct, "N3");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 26 Checked", "sCustomField26Bit", Field.E_FieldType.Boo, Field.E_ClassType.Txt
                    , new BoolRepMap(true, "Yes")
                    , new BoolRepMap(false, "No")
                    );
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 26 Notes", "sCustomField26Notes", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_CustomFields, "Custom Field 27 Description", "sCustomField27Desc", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 27 Date", "sCustomField27D", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 27 Amount", "sCustomField27Money", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 27 Percent", "sCustomField27Pc", Field.E_FieldType.Dec, Field.E_ClassType.Pct, "N3");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 27 Checked", "sCustomField27Bit", Field.E_FieldType.Boo, Field.E_ClassType.Txt
                    , new BoolRepMap(true, "Yes")
                    , new BoolRepMap(false, "No")
                    );
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 27 Notes", "sCustomField27Notes", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_CustomFields, "Custom Field 28 Description", "sCustomField28Desc", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 28 Date", "sCustomField28D", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 28 Amount", "sCustomField28Money", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 28 Percent", "sCustomField28Pc", Field.E_FieldType.Dec, Field.E_ClassType.Pct, "N3");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 28 Checked", "sCustomField28Bit", Field.E_FieldType.Boo, Field.E_ClassType.Txt
                    , new BoolRepMap(true, "Yes")
                    , new BoolRepMap(false, "No")
                    );
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 28 Notes", "sCustomField28Notes", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_CustomFields, "Custom Field 29 Description", "sCustomField29Desc", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 29 Date", "sCustomField29D", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 29 Amount", "sCustomField29Money", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 29 Percent", "sCustomField29Pc", Field.E_FieldType.Dec, Field.E_ClassType.Pct, "N3");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 29 Checked", "sCustomField29Bit", Field.E_FieldType.Boo, Field.E_ClassType.Txt
                    , new BoolRepMap(true, "Yes")
                    , new BoolRepMap(false, "No")
                    );
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 29 Notes", "sCustomField29Notes", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_CustomFields, "Custom Field 30 Description", "sCustomField30Desc", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 30 Date", "sCustomField30D", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 30 Amount", "sCustomField30Money", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 30 Percent", "sCustomField30Pc", Field.E_FieldType.Dec, Field.E_ClassType.Pct, "N3");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 30 Checked", "sCustomField30Bit", Field.E_FieldType.Boo, Field.E_ClassType.Txt
                    , new BoolRepMap(true, "Yes")
                    , new BoolRepMap(false, "No")
                    );
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 30 Notes", "sCustomField30Notes", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_CustomFields, "Custom Field 31 Description", "sCustomField31Desc", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 31 Date", "sCustomField31D", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 31 Amount", "sCustomField31Money", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 31 Percent", "sCustomField31Pc", Field.E_FieldType.Dec, Field.E_ClassType.Pct, "N3");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 31 Checked", "sCustomField31Bit", Field.E_FieldType.Boo, Field.E_ClassType.Txt
                    , new BoolRepMap(true, "Yes")
                    , new BoolRepMap(false, "No")
                    );
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 31 Notes", "sCustomField31Notes", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_CustomFields, "Custom Field 32 Description", "sCustomField32Desc", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 32 Date", "sCustomField32D", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 32 Amount", "sCustomField32Money", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 32 Percent", "sCustomField32Pc", Field.E_FieldType.Dec, Field.E_ClassType.Pct, "N3");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 32 Checked", "sCustomField32Bit", Field.E_FieldType.Boo, Field.E_ClassType.Txt
                    , new BoolRepMap(true, "Yes")
                    , new BoolRepMap(false, "No")
                    );
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 32 Notes", "sCustomField32Notes", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_CustomFields, "Custom Field 33 Description", "sCustomField33Desc", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 33 Date", "sCustomField33D", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 33 Amount", "sCustomField33Money", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 33 Percent", "sCustomField33Pc", Field.E_FieldType.Dec, Field.E_ClassType.Pct, "N3");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 33 Checked", "sCustomField33Bit", Field.E_FieldType.Boo, Field.E_ClassType.Txt
                    , new BoolRepMap(true, "Yes")
                    , new BoolRepMap(false, "No")
                    );
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 33 Notes", "sCustomField33Notes", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_CustomFields, "Custom Field 34 Description", "sCustomField34Desc", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 34 Date", "sCustomField34D", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 34 Amount", "sCustomField34Money", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 34 Percent", "sCustomField34Pc", Field.E_FieldType.Dec, Field.E_ClassType.Pct, "N3");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 34 Checked", "sCustomField34Bit", Field.E_FieldType.Boo, Field.E_ClassType.Txt
                    , new BoolRepMap(true, "Yes")
                    , new BoolRepMap(false, "No")
                    );
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 34 Notes", "sCustomField34Notes", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_CustomFields, "Custom Field 35 Description", "sCustomField35Desc", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 35 Date", "sCustomField35D", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 35 Amount", "sCustomField35Money", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 35 Percent", "sCustomField35Pc", Field.E_FieldType.Dec, Field.E_ClassType.Pct, "N3");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 35 Checked", "sCustomField35Bit", Field.E_FieldType.Boo, Field.E_ClassType.Txt
                    , new BoolRepMap(true, "Yes")
                    , new BoolRepMap(false, "No")
                    );
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 35 Notes", "sCustomField35Notes", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_CustomFields, "Custom Field 36 Description", "sCustomField36Desc", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 36 Date", "sCustomField36D", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 36 Amount", "sCustomField36Money", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 36 Percent", "sCustomField36Pc", Field.E_FieldType.Dec, Field.E_ClassType.Pct, "N3");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 36 Checked", "sCustomField36Bit", Field.E_FieldType.Boo, Field.E_ClassType.Txt
                    , new BoolRepMap(true, "Yes")
                    , new BoolRepMap(false, "No")
                    );
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 36 Notes", "sCustomField36Notes", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_CustomFields, "Custom Field 37 Description", "sCustomField37Desc", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 37 Date", "sCustomField37D", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 37 Amount", "sCustomField37Money", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 37 Percent", "sCustomField37Pc", Field.E_FieldType.Dec, Field.E_ClassType.Pct, "N3");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 37 Checked", "sCustomField37Bit", Field.E_FieldType.Boo, Field.E_ClassType.Txt
                    , new BoolRepMap(true, "Yes")
                    , new BoolRepMap(false, "No")
                    );
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 37 Notes", "sCustomField37Notes", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_CustomFields, "Custom Field 38 Description", "sCustomField38Desc", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 38 Date", "sCustomField38D", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 38 Amount", "sCustomField38Money", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 38 Percent", "sCustomField38Pc", Field.E_FieldType.Dec, Field.E_ClassType.Pct, "N3");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 38 Checked", "sCustomField38Bit", Field.E_FieldType.Boo, Field.E_ClassType.Txt
                    , new BoolRepMap(true, "Yes")
                    , new BoolRepMap(false, "No")
                    );
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 38 Notes", "sCustomField38Notes", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_CustomFields, "Custom Field 39 Description", "sCustomField39Desc", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 39 Date", "sCustomField39D", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 39 Amount", "sCustomField39Money", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 39 Percent", "sCustomField39Pc", Field.E_FieldType.Dec, Field.E_ClassType.Pct, "N3");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 39 Checked", "sCustomField39Bit", Field.E_FieldType.Boo, Field.E_ClassType.Txt
                    , new BoolRepMap(true, "Yes")
                    , new BoolRepMap(false, "No")
                    );
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 39 Notes", "sCustomField39Notes", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_CustomFields, "Custom Field 40 Description", "sCustomField40Desc", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 40 Date", "sCustomField40D", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 40 Amount", "sCustomField40Money", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 40 Percent", "sCustomField40Pc", Field.E_FieldType.Dec, Field.E_ClassType.Pct, "N3");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 40 Checked", "sCustomField40Bit", Field.E_FieldType.Boo, Field.E_ClassType.Txt
                    , new BoolRepMap(true, "Yes")
                    , new BoolRepMap(false, "No")
                    );
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 40 Notes", "sCustomField40Notes", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_CustomFields, "Custom Field 41 Description", "sCustomField41Desc", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 41 Date", "sCustomField41D", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 41 Amount", "sCustomField41Money", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 41 Percent", "sCustomField41Pc", Field.E_FieldType.Dec, Field.E_ClassType.Pct, "N3");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 41 Checked", "sCustomField41Bit", Field.E_FieldType.Boo, Field.E_ClassType.Txt
                    , new BoolRepMap(true, "Yes")
                    , new BoolRepMap(false, "No")
                    );
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 41 Notes", "sCustomField41Notes", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_CustomFields, "Custom Field 42 Description", "sCustomField42Desc", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 42 Date", "sCustomField42D", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 42 Amount", "sCustomField42Money", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 42 Percent", "sCustomField42Pc", Field.E_FieldType.Dec, Field.E_ClassType.Pct, "N3");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 42 Checked", "sCustomField42Bit", Field.E_FieldType.Boo, Field.E_ClassType.Txt
                    , new BoolRepMap(true, "Yes")
                    , new BoolRepMap(false, "No")
                    );
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 42 Notes", "sCustomField42Notes", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_CustomFields, "Custom Field 43 Description", "sCustomField43Desc", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 43 Date", "sCustomField43D", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 43 Amount", "sCustomField43Money", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 43 Percent", "sCustomField43Pc", Field.E_FieldType.Dec, Field.E_ClassType.Pct, "N3");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 43 Checked", "sCustomField43Bit", Field.E_FieldType.Boo, Field.E_ClassType.Txt
                    , new BoolRepMap(true, "Yes")
                    , new BoolRepMap(false, "No")
                    );
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 43 Notes", "sCustomField43Notes", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_CustomFields, "Custom Field 44 Description", "sCustomField44Desc", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 44 Date", "sCustomField44D", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 44 Amount", "sCustomField44Money", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 44 Percent", "sCustomField44Pc", Field.E_FieldType.Dec, Field.E_ClassType.Pct, "N3");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 44 Checked", "sCustomField44Bit", Field.E_FieldType.Boo, Field.E_ClassType.Txt
                    , new BoolRepMap(true, "Yes")
                    , new BoolRepMap(false, "No")
                    );
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 44 Notes", "sCustomField44Notes", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_CustomFields, "Custom Field 45 Description", "sCustomField45Desc", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 45 Date", "sCustomField45D", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 45 Amount", "sCustomField45Money", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 45 Percent", "sCustomField45Pc", Field.E_FieldType.Dec, Field.E_ClassType.Pct, "N3");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 45 Checked", "sCustomField45Bit", Field.E_FieldType.Boo, Field.E_ClassType.Txt
                    , new BoolRepMap(true, "Yes")
                    , new BoolRepMap(false, "No")
                    );
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 45 Notes", "sCustomField45Notes", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_CustomFields, "Custom Field 46 Description", "sCustomField46Desc", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 46 Date", "sCustomField46D", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 46 Amount", "sCustomField46Money", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 46 Percent", "sCustomField46Pc", Field.E_FieldType.Dec, Field.E_ClassType.Pct, "N3");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 46 Checked", "sCustomField46Bit", Field.E_FieldType.Boo, Field.E_ClassType.Txt
                    , new BoolRepMap(true, "Yes")
                    , new BoolRepMap(false, "No")
                    );
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 46 Notes", "sCustomField46Notes", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_CustomFields, "Custom Field 47 Description", "sCustomField47Desc", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 47 Date", "sCustomField47D", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 47 Amount", "sCustomField47Money", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 47 Percent", "sCustomField47Pc", Field.E_FieldType.Dec, Field.E_ClassType.Pct, "N3");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 47 Checked", "sCustomField47Bit", Field.E_FieldType.Boo, Field.E_ClassType.Txt
                    , new BoolRepMap(true, "Yes")
                    , new BoolRepMap(false, "No")
                    );
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 47 Notes", "sCustomField47Notes", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_CustomFields, "Custom Field 48 Description", "sCustomField48Desc", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 48 Date", "sCustomField48D", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 48 Amount", "sCustomField48Money", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 48 Percent", "sCustomField48Pc", Field.E_FieldType.Dec, Field.E_ClassType.Pct, "N3");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 48 Checked", "sCustomField48Bit", Field.E_FieldType.Boo, Field.E_ClassType.Txt
                    , new BoolRepMap(true, "Yes")
                    , new BoolRepMap(false, "No")
                    );
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 48 Notes", "sCustomField48Notes", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_CustomFields, "Custom Field 49 Description", "sCustomField49Desc", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 49 Date", "sCustomField49D", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 49 Amount", "sCustomField49Money", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 49 Percent", "sCustomField49Pc", Field.E_FieldType.Dec, Field.E_ClassType.Pct, "N3");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 49 Checked", "sCustomField49Bit", Field.E_FieldType.Boo, Field.E_ClassType.Txt
                    , new BoolRepMap(true, "Yes")
                    , new BoolRepMap(false, "No")
                    );
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 49 Notes", "sCustomField49Notes", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_CustomFields, "Custom Field 50 Description", "sCustomField50Desc", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 50 Date", "sCustomField50D", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 50 Amount", "sCustomField50Money", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 50 Percent", "sCustomField50Pc", Field.E_FieldType.Dec, Field.E_ClassType.Pct, "N3");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 50 Checked", "sCustomField50Bit", Field.E_FieldType.Boo, Field.E_ClassType.Txt
                    , new BoolRepMap(true, "Yes")
                    , new BoolRepMap(false, "No")
                    );
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 50 Notes", "sCustomField50Notes", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_CustomFields, "Custom Field 51 Description", "sCustomField51Desc", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 51 Date", "sCustomField51D", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 51 Amount", "sCustomField51Money", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 51 Percent", "sCustomField51Pc", Field.E_FieldType.Dec, Field.E_ClassType.Pct, "N3");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 51 Checked", "sCustomField51Bit", Field.E_FieldType.Boo, Field.E_ClassType.Txt
                    , new BoolRepMap(true, "Yes")
                    , new BoolRepMap(false, "No")
                    );
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 51 Notes", "sCustomField51Notes", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_CustomFields, "Custom Field 52 Description", "sCustomField52Desc", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 52 Date", "sCustomField52D", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 52 Amount", "sCustomField52Money", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 52 Percent", "sCustomField52Pc", Field.E_FieldType.Dec, Field.E_ClassType.Pct, "N3");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 52 Checked", "sCustomField52Bit", Field.E_FieldType.Boo, Field.E_ClassType.Txt
                    , new BoolRepMap(true, "Yes")
                    , new BoolRepMap(false, "No")
                    );
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 52 Notes", "sCustomField52Notes", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_CustomFields, "Custom Field 53 Description", "sCustomField53Desc", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 53 Date", "sCustomField53D", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 53 Amount", "sCustomField53Money", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 53 Percent", "sCustomField53Pc", Field.E_FieldType.Dec, Field.E_ClassType.Pct, "N3");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 53 Checked", "sCustomField53Bit", Field.E_FieldType.Boo, Field.E_ClassType.Txt
                    , new BoolRepMap(true, "Yes")
                    , new BoolRepMap(false, "No")
                    );
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 53 Notes", "sCustomField53Notes", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_CustomFields, "Custom Field 54 Description", "sCustomField54Desc", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 54 Date", "sCustomField54D", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 54 Amount", "sCustomField54Money", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 54 Percent", "sCustomField54Pc", Field.E_FieldType.Dec, Field.E_ClassType.Pct, "N3");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 54 Checked", "sCustomField54Bit", Field.E_FieldType.Boo, Field.E_ClassType.Txt
                    , new BoolRepMap(true, "Yes")
                    , new BoolRepMap(false, "No")
                    );
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 54 Notes", "sCustomField54Notes", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_CustomFields, "Custom Field 55 Description", "sCustomField55Desc", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 55 Date", "sCustomField55D", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 55 Amount", "sCustomField55Money", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 55 Percent", "sCustomField55Pc", Field.E_FieldType.Dec, Field.E_ClassType.Pct, "N3");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 55 Checked", "sCustomField55Bit", Field.E_FieldType.Boo, Field.E_ClassType.Txt
                    , new BoolRepMap(true, "Yes")
                    , new BoolRepMap(false, "No")
                    );
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 55 Notes", "sCustomField55Notes", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_CustomFields, "Custom Field 56 Description", "sCustomField56Desc", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 56 Date", "sCustomField56D", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 56 Amount", "sCustomField56Money", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 56 Percent", "sCustomField56Pc", Field.E_FieldType.Dec, Field.E_ClassType.Pct, "N3");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 56 Checked", "sCustomField56Bit", Field.E_FieldType.Boo, Field.E_ClassType.Txt
                    , new BoolRepMap(true, "Yes")
                    , new BoolRepMap(false, "No")
                    );
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 56 Notes", "sCustomField56Notes", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_CustomFields, "Custom Field 57 Description", "sCustomField57Desc", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 57 Date", "sCustomField57D", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 57 Amount", "sCustomField57Money", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 57 Percent", "sCustomField57Pc", Field.E_FieldType.Dec, Field.E_ClassType.Pct, "N3");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 57 Checked", "sCustomField57Bit", Field.E_FieldType.Boo, Field.E_ClassType.Txt
                    , new BoolRepMap(true, "Yes")
                    , new BoolRepMap(false, "No")
                    );
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 57 Notes", "sCustomField57Notes", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_CustomFields, "Custom Field 58 Description", "sCustomField58Desc", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 58 Date", "sCustomField58D", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 58 Amount", "sCustomField58Money", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 58 Percent", "sCustomField58Pc", Field.E_FieldType.Dec, Field.E_ClassType.Pct, "N3");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 58 Checked", "sCustomField58Bit", Field.E_FieldType.Boo, Field.E_ClassType.Txt
                    , new BoolRepMap(true, "Yes")
                    , new BoolRepMap(false, "No")
                    );
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 58 Notes", "sCustomField58Notes", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_CustomFields, "Custom Field 59 Description", "sCustomField59Desc", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 59 Date", "sCustomField59D", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 59 Amount", "sCustomField59Money", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 59 Percent", "sCustomField59Pc", Field.E_FieldType.Dec, Field.E_ClassType.Pct, "N3");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 59 Checked", "sCustomField59Bit", Field.E_FieldType.Boo, Field.E_ClassType.Txt
                    , new BoolRepMap(true, "Yes")
                    , new BoolRepMap(false, "No")
                    );
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 59 Notes", "sCustomField59Notes", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_CustomFields, "Custom Field 60 Description", "sCustomField60Desc", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 60 Date", "sCustomField60D", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 60 Amount", "sCustomField60Money", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 60 Percent", "sCustomField60Pc", Field.E_FieldType.Dec, Field.E_ClassType.Pct, "N3");
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 60 Checked", "sCustomField60Bit", Field.E_FieldType.Boo, Field.E_ClassType.Txt
                    , new BoolRepMap(true, "Yes")
                    , new BoolRepMap(false, "No")
                    );
                eB += m_Schema.Add(Category_CustomFields, "Custom Field 60 Notes", "sCustomField60Notes", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                eB += m_Schema.Add(Category_CustomFields, "Custom PML Field 1", "sCustomPMLField1CR", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_CustomFields, "Custom PML Field 2", "sCustomPMLField2CR", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_CustomFields, "Custom PML Field 3", "sCustomPMLField3CR", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_CustomFields, "Custom PML Field 4", "sCustomPMLField4CR", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_CustomFields, "Custom PML Field 5", "sCustomPMLField5CR", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_CustomFields, "Custom PML Field 6", "sCustomPMLField6CR", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_CustomFields, "Custom PML Field 7", "sCustomPMLField7CR", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_CustomFields, "Custom PML Field 8", "sCustomPMLField8CR", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_CustomFields, "Custom PML Field 9", "sCustomPMLField9CR", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_CustomFields, "Custom PML Field 10", "sCustomPMLField10CR", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_CustomFields, "Custom PML Field 11", "sCustomPMLField11CR", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_CustomFields, "Custom PML Field 12", "sCustomPMLField12CR", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_CustomFields, "Custom PML Field 13", "sCustomPMLField13CR", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_CustomFields, "Custom PML Field 14", "sCustomPMLField14CR", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_CustomFields, "Custom PML Field 15", "sCustomPMLField15CR", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_CustomFields, "Custom PML Field 16", "sCustomPMLField16CR", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_CustomFields, "Custom PML Field 17", "sCustomPMLField17CR", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_CustomFields, "Custom PML Field 18", "sCustomPMLField18CR", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_CustomFields, "Custom PML Field 19", "sCustomPMLField19CR", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_CustomFields, "Custom PML Field 20", "sCustomPMLField20CR", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                #endregion

                #region ( Banking Features )
                //opm 47976 fs 03/17/10

                // INVESTOR RATE LOCK
                eB += m_Schema.Add(Category_InvestorRateLock, "Investor Rate Lock Investor Name", "sInvestorLockLpInvestorNm", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_InvestorRateLock, "Investor Rate Lock Program Description", "sInvestorLockLpTemplateNm", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_InvestorRateLock, "Investor Rate Lock Status", "sInvestorLockRateLockStatusT", Field.E_FieldType.Enu, Field.E_ClassType.Txt,
                    new EnumRepMap(E_sInvestorLockRateLockStatusT.NotLocked, "Not Locked"),
                    new EnumRepMap(E_sInvestorLockRateLockStatusT.Locked, "Locked"),
                    new EnumRepMap(E_sInvestorLockRateLockStatusT.LockSuspended, "Lock Suspended"));
                eB += m_Schema.Add(Category_InvestorRateLock, "Investor Rate Lock Period", "sInvestorLockRLckdDays", Field.E_FieldType.Int, Field.E_ClassType.Cnt, "#,#;-#,#;0");
                eB += m_Schema.Add(Category_InvestorRateLock, "Investor Rate Lock Date", "sInvestorLockRLckdD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_InvestorRateLock, "Investor Rate Lock Expiration", "sInvestorLockRLckExpiredD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_InvestorRateLock, "Investor Delivery Expiration", "sInvestorLockDeliveryExpiredD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_InvestorRateLock, "Investor Rate Lock Loan #", "sInvestorLockLoanNum", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_InvestorRateLock, "Investor Loan Program ID", "sInvestorLockProgramId", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_InvestorRateLock, "Investor Rate Lock Confirmation #", "sInvestorLockConfNum", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_InvestorRateLock, "Investor Rate Sheet ID", "sInvestorLockRateSheetID", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_InvestorRateLock, "Investor Commitment Type", "sInvestorLockCommitmentT", Field.E_FieldType.Enu, Field.E_ClassType.Txt,
                    new EnumRepMap(E_sInvestorLockCommitmentT.BestEfforts, "Best Effort"),
                    new EnumRepMap(E_sInvestorLockCommitmentT.Mandatory, "Mandatory"),
                    new EnumRepMap(E_sInvestorLockCommitmentT.Hedged, "Hedged"),
                    new EnumRepMap(E_sInvestorLockCommitmentT.Securitized, "Securitized"));
                eB += m_Schema.Add(Category_InvestorRateLock, "Gross Projected Profit/Loss", "sInvestorLockProjectedProfit", Field.E_FieldType.Dec, Field.E_ClassType.Pct, "N3");
                eB += m_Schema.Add(Category_InvestorRateLock, "Gross Projected Profit/Loss Amount", "sInvestorLockProjectedProfitAmt", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_InvestorRateLock, "Investor Rate Lock Price", "sInvestorLockBrokComp1PcPrice", Field.E_FieldType.Dec, Field.E_ClassType.Pct, "N3"); //opm 51352 fs 06/17/10
                eB += m_Schema.Add(Category_InvestorRateLock, "Investor Rate Lock Fee", "sInvestorLockBrokComp1Pc", Field.E_FieldType.Dec, Field.E_ClassType.Pct, "N3"); //opm 51352 fs 06/17/10
                eB += m_Schema.Add(Category_InvestorRateLock, "Projected Total Price Amount", "sProjectedTotalPrice", Field.E_FieldType.Dec, Field.E_ClassType.Pct, "N3"); //opm 57128  10/22/10
                eB += m_Schema.Add(Category_InvestorRateLock, "Investor Base Rate Lock Price", "sInvestorLockBaseBrokComp1PcPrice", Field.E_FieldType.Dec, Field.E_ClassType.Pct, "N3");
				eB += m_Schema.Add(Category_InvestorRateLock, "Investor Rate Sheet Effective Time", "sInvestorLockRateSheetEffectiveTime", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                //Rate lock custom fields
                eB += m_Schema.Add(Category_InvestorRateLock, "Investor Lock Custom Amount 1", "sU1LockFieldAmt", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_InvestorRateLock, "Investor Lock Custom Date 1", "sU1LockFieldD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_InvestorRateLock, "Investor Lock Custom Desc 1", "sU1LockFieldDesc", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_InvestorRateLock, "Investor Lock Custom Percent 1", "sU1LockFieldPc", Field.E_FieldType.Dec, Field.E_ClassType.Pct, "N3");
                eB += m_Schema.Add(Category_InvestorRateLock, "Investor Lock Custom Amount 2", "sU2LockFieldAmt", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_InvestorRateLock, "Investor Lock Custom Date 2", "sU2LockFieldD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_InvestorRateLock, "Investor Lock Custom Desc 2", "sU2LockFieldDesc", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_InvestorRateLock, "Investor Lock Custom Percent 2", "sU2LockFieldPc", Field.E_FieldType.Dec, Field.E_ClassType.Pct, "N3");

                eB += m_Schema.Add(Category_InvestorRateLock, "# Of Investor Extensions (Current Lock)", "sBECurrentLockExtensionCount", Field.E_FieldType.Int, Field.E_ClassType.Cnt);
                eB += m_Schema.Add(Category_InvestorRateLock, "# Of Investor Extensions (All Locks On File)", "sBETotalLockExtensionCount", Field.E_FieldType.Int, Field.E_ClassType.Cnt);
                eB += m_Schema.Add(Category_InvestorRateLock, "# Of Investor Rate Re-Locks On File", "sBETotalRateReLockCount", Field.E_FieldType.Int, Field.E_ClassType.Cnt);


                //SETTLEMENT_CHARGES                
                eB += m_Schema.Add(Category_Settlement, "SC 801 Loan Origination Fee", "sSettlementLOrigF", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C"); //opm 51352 fs 06/17/10
                eB += m_Schema.Add(Category_Settlement, "SC Net Broker Commission", "sSettlementBrokTotalCommission", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C"); //opm 51354 fs 06/11/10
                eB += m_Schema.Add(Category_Settlement, "SC Broker Credit", "sSettlementBrokerCredit", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C"); //opm 51354 fs 06/11/10
                eB += m_Schema.Add(Category_Settlement, "SC 802 Credit or Charge", "sSettlementLDiscnt", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C"); //opm 51352 fs 06/17/10
                eB += m_Schema.Add(Category_Settlement, "SC 802 Discount Points", "sSettlementDiscountPointF", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C"); //opm 88004 mp 07/12/12
                eB += m_Schema.Add(Category_Settlement, "SC 804 Appraisal fee", "sSettlementApprF", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C"); //opm 57128  10/22/10
                eB += m_Schema.Add(Category_Settlement, "SC 805 Credit Fee", "sSettlementCrF", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C"); // opm 78349 vm 4/9/12
                eB += m_Schema.Add(Category_Settlement, "SC 806 Tax Service Fee", "sSettlementTxServF", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_Settlement, "SC 807 Flood Certification", "sSettlementFloodCertificationF", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_Settlement, "SC 808 Mortgage Broker Fee", "sSettlementMBrokF", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_Settlement, "SC 809 Lender's Inspection Fee", "sSettlementInspectF", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
				eB += m_Schema.Add(Category_Settlement, "SC 810 Processing Fee", "sSettlementProcF", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C"); //opm 51352 fs 06/17/10
                eB += m_Schema.Add(Category_Settlement, "SC 811 Underwriting Fee", "sSettlementUwF", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C"); //opm 51352 fs 06/17/10
                eB += m_Schema.Add(Category_Settlement, "SC 812 Wire Transfer", "sSettlementWireF", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_Settlement, "SC 813 Additional Item #1 Fee", "sSettlement800U1F", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C"); //opm 54676  10/27/10
                eB += m_Schema.Add(Category_Settlement, "SC 813 Additional Item #1 Description", "sSettlement800U1FDesc", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C"); //opm 54676  10/27/10
                eB += m_Schema.Add(Category_Settlement, "SC 814 Additional Item #1 Fee", "sSettlement800U2F", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C"); //opm 69217
                eB += m_Schema.Add(Category_Settlement, "SC 814 Additional Item #1 Description", "sSettlement800U2FDesc", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");//opm 69217
                eB += m_Schema.Add(Category_Settlement, "SC 815 Additional Item #1 Fee", "sSettlement800U3F", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C"); //opm 69217
                eB += m_Schema.Add(Category_Settlement, "SC 815 Additional Item #1 Description", "sSettlement800U3FDesc", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");//opm 69217
                eB += m_Schema.Add(Category_Settlement, "SC 816 Additional Item #1 Fee", "sSettlement800U4F", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C"); //opm 69217
                eB += m_Schema.Add(Category_Settlement, "SC 816 Additional Item #1 Description", "sSettlement800U4FDesc", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");//opm 69217
                eB += m_Schema.Add(Category_Settlement, "SC 817 Additional Item #1 Fee", "sSettlement800U5F", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C"); //opm 69217
                eB += m_Schema.Add(Category_Settlement, "SC 817 Additional Item #1 Description", "sSettlement800U5FDesc", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");//opm 69217
                
                eB += m_Schema.Add(Category_Settlement, "SC 901 Prepaid Interest", "sSettlementIPia", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C"); //opm 51352 fs 06/17/10                
				eB += m_Schema.Add(Category_Settlement, "SC 903 haz Ins", "sSettlementHazInsPia", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_Settlement, "SC 904 Description", "sSettlement904PiaDesc", Field.E_FieldType.Str, Field.E_ClassType.LongText);
                eB += m_Schema.Add(Category_Settlement, "SC 904 Fee", "sSettlement904Pia", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_Settlement, "SC 906 Description", "sSettlement900U1PiaDesc", Field.E_FieldType.Str, Field.E_ClassType.LongText);
                eB += m_Schema.Add(Category_Settlement, "SC 906 Fee", "sSettlement900U1Pia", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");

                eB += m_Schema.Add(Category_Settlement, "SC Reserves Deposited with Lender", "sInitialDeposit_Escrow", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C"); //opm 51352 fs 06/17/10
                
                eB += m_Schema.Add(Category_Settlement, "SC 1002 Hazard Insurance Reserve", "sSettlementHazInsRsrv", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C"); //opm 54676  10/27/10
                eB += m_Schema.Add(Category_Settlement, "SC 1003 Mortgage Insurance Reserve", "sSettlementMInsRsrv", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C"); //opm 54676  10/27/10
                eB += m_Schema.Add(Category_Settlement, "SC 1004 Tax Reserve", "sSettlementRealETxRsrv", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C"); //opm 54676  10/27/10
                eB += m_Schema.Add(Category_Settlement, "SC 1005 School Tax Reserve", "sSettlementSchoolTxRsrv", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C"); //opm 54676  10/27/10
                eB += m_Schema.Add(Category_Settlement, "SC 1005 School Tax Reserve, Monthly amount", "sProSchoolTx", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C"); // opm 88576 mp 7/27/12
                eB += m_Schema.Add(Category_Settlement, "SC 1006 Flood Insurance Reserve", "sSettlementFloodInsRsrv", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C"); //opm 54676  10/27/10
                eB += m_Schema.Add(Category_Settlement, "SC 1006 Flood Insurance Reserve, Monthly amount", "sProFloodIns", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C"); // opm 88576 mp 7/27/12
                eB += m_Schema.Add(Category_Settlement, "SC 1007 Aggregate Adjustment", "sSettlementAggregateAdjRsrv", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C"); //opm 54676  10/27/10
                eB += m_Schema.Add(Category_Settlement, "SC 1008", "sSettlement1008Rsrv", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C"); // opm 88576 mp 7/27/12
                eB += m_Schema.Add(Category_Settlement, "SC 1008, Monthly amount", "s1006ProHExp", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C"); // opm 88576 mp 7/27/12
                eB += m_Schema.Add(Category_Settlement, "SC 1008 Description", "s1006ProHExpDesc", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C"); // opm 88576 mp 7/27/12
                eB += m_Schema.Add(Category_Settlement, "SC 1009", "sSettlement1009Rsrv", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C"); // opm 88576 mp 7/27/12
                eB += m_Schema.Add(Category_Settlement, "SC 1009, Monthly amount", "s1007ProHExp", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C"); // opm 88576 mp 7/27/12
                eB += m_Schema.Add(Category_Settlement, "SC 1009 Description", "s1007ProHExpDesc", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C"); // opm 88576 mp 7/27/12

                eB += m_Schema.Add(Category_Settlement, "SC 1102 Closing/Escrow Fee", "sSettlementEscrowF", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_Settlement, "SC 1103 Owner's Title Insurance", "sSettlementOwnerTitleInsF", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_Settlement, "SC 1104 Lender's Title Insurance", "sSettlementTitleInsF", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_Settlement, "SC 1109 Doc Preparation Fee", "sSettlementDocPrepF", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_Settlement, "SC 1110 Notary Fees", "sSettlementNotaryF", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_Settlement, "SC 1111 Attorney Fees", "sSettlementAttorneyF", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_Settlement, "SC 1112 Description", "sSettlementU1TcDesc", Field.E_FieldType.Str, Field.E_ClassType.LongText);
                eB += m_Schema.Add(Category_Settlement, "SC 1112 Fee", "sSettlementU1Tc", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_Settlement, "SC 1113 Description", "sSettlementU2TcDesc", Field.E_FieldType.Str, Field.E_ClassType.LongText);
                eB += m_Schema.Add(Category_Settlement, "SC 1113 Fee", "sSettlementU2Tc", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_Settlement, "SC 1114 Description", "sSettlementU3TcDesc", Field.E_FieldType.Str, Field.E_ClassType.LongText);
                eB += m_Schema.Add(Category_Settlement, "SC 1114 Fee", "sSettlementU3Tc", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_Settlement, "SC 1115 Description", "sSettlementU4TcDesc", Field.E_FieldType.Str, Field.E_ClassType.LongText);
                eB += m_Schema.Add(Category_Settlement, "SC 1115 Fee", "sSettlementU4Tc", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");

                eB += m_Schema.Add(Category_Settlement, "SC 1201 Recording Fees", "sSettlementRecF", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_Settlement, "SC 1204 City/County Tax Stamps", "sSettlementCountyRtc", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_Settlement, "SC 1205 State Tax Stamps", "sSettlementStateRtc", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_Settlement, "SC 1206 Description", "sSettlementU1GovRtcDesc", Field.E_FieldType.Str, Field.E_ClassType.LongText);
                eB += m_Schema.Add(Category_Settlement, "SC 1206 Fee", "sSettlementU1GovRtc", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_Settlement, "SC 1207 Description", "sSettlementU2GovRtcDesc", Field.E_FieldType.Str, Field.E_ClassType.LongText);
                eB += m_Schema.Add(Category_Settlement, "SC 1207 Fee", "sSettlementU2GovRtc", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_Settlement, "SC 1208 Description", "sSettlementU3GovRtcDesc", Field.E_FieldType.Str, Field.E_ClassType.LongText);
                eB += m_Schema.Add(Category_Settlement, "SC 1208 Fee", "sSettlementU3GovRtc", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");

                eB += m_Schema.Add(Category_Settlement, "SC 1302 Pest Inspection", "sSettlementPestInspectF", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_Settlement, "SC 1303 Description", "sSettlementU1ScDesc", Field.E_FieldType.Str, Field.E_ClassType.LongText);
                eB += m_Schema.Add(Category_Settlement, "SC 1303 Fee", "sSettlementU1Sc", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_Settlement, "SC 1304 Description", "sSettlementU2ScDesc", Field.E_FieldType.Str, Field.E_ClassType.LongText);
                eB += m_Schema.Add(Category_Settlement, "SC 1304 Fee", "sSettlementU2Sc", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_Settlement, "SC 1305 Description", "sSettlementU3ScDesc", Field.E_FieldType.Str, Field.E_ClassType.LongText);
                eB += m_Schema.Add(Category_Settlement, "SC 1305 Fee", "sSettlementU3Sc", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_Settlement, "SC 1306 Description", "sSettlementU4ScDesc", Field.E_FieldType.Str, Field.E_ClassType.LongText);
                eB += m_Schema.Add(Category_Settlement, "SC 1306 Fee", "sSettlementU4Sc", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_Settlement, "SC 1307 Description", "sSettlementU5ScDesc", Field.E_FieldType.Str, Field.E_ClassType.LongText);
                eB += m_Schema.Add(Category_Settlement, "SC 1307 Fee", "sSettlementU5Sc", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");

                eB += m_Schema.Add(Category_Settlement, "SC Total Title Charges", "sSettlementTotalTitleCharges", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C"); // opm 90492 mp 8/27/12
                eB += m_Schema.Add(Category_Settlement, "SC Total Government Recording and Transfer Charges", "sSettlementTotalGovernmentRecordingAndTransferCharges", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C"); // opm 90492 mp 8/27/12
                eB += m_Schema.Add(Category_Settlement, "SC Total Additional Settlement Charges", "sSettlementTotalAdditionalSettlementCharges", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C"); // opm 90492 mp 8/27/12
                eB += m_Schema.Add(Category_Settlement, "SC Discount Points Percent", "sSettlementDiscountPointFPc", Field.E_FieldType.Dec, Field.E_ClassType.Pct, "N3");

                eB += m_Schema.Add(Category_Settlement, "SC Total Settlement Charges", "sSettlementTotalEstimateSettlementCharge", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");

                //FUNDING
                eB += m_Schema.Add(Category_Funding, "Funds sent to closing", "sTotalAmtFund", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C"); //opm 51354 fs 06/11/10
                eB += m_Schema.Add(Category_Funding, "Disbursement Method", "sFundingDisbursementMethodT", Field.E_FieldType.Enu, Field.E_ClassType.Txt
                    , new EnumRepMap(E_sDisbursementMethodT.Blank, "")
                    , new EnumRepMap(E_sDisbursementMethodT.Check, "Check")
                    , new EnumRepMap(E_sDisbursementMethodT.Hold, "Hold")
                    , new EnumRepMap(E_sDisbursementMethodT.Wire, "Wire")
                    ); // OPM 109299 GF
                eB += m_Schema.Add(Category_Funding, "Warehouse line max funding %", "sWarehouseMaxFundPc", Field.E_FieldType.Dec, Field.E_ClassType.Pct, "N3");
                eB += m_Schema.Add(Category_Funding, "Amount required to fund", "sAmtReqToFund", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_Funding, "Amount funded from warehouse line", "sAmtFundFromWarehouseLine", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_Funding, "Funds required for shortfall", "sFundReqForShortfall", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_Funding, "Check due from closing", "sChkDueFromClosing", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_Funding, "Was check from closing received?", "sChkDueFromClosingRcvd", Field.E_FieldType.Boo, Field.E_ClassType.Txt
                    , new BoolRepMap(true, "Yes")
                    , new BoolRepMap(false, "No")
                    );
                eB += m_Schema.Add(Category_Funding, "Shipped to warehouse date", "sShippedToWarehouseD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_Funding, "Funding tracking number", "sTrackingN", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_Funding, "Items Paid to Lender at Closing", "sSettlementChargesDedFromLoanProc", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C"); //opm 51352 fs 06/17/10
                eB += m_Schema.Add(Category_Funding, "Items Paid by Lender at Closing", "sSettlementTotalFundByLenderAtClosing", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C"); //opm 51352 fs 06/17/10
                eB += m_Schema.Add(Category_Funding, "Other Funding Adjustments", "sOtherFundAdj", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C"); //opm 51352 fs 06/17/10
                eB += m_Schema.Add(Category_Funding, "Loan Proceeds to Bank Name", "sFundingBankName", Field.E_FieldType.Str, Field.E_ClassType.Txt); //OPM 89787 RWN v
                eB += m_Schema.Add(Category_Funding, "Loan Proceeds to Bank Address", "sFundingBankCityState", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_Funding, "Loan Proceeds to ABA Number", "sFundingABANumber", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_Funding, "Loan Proceeds to Account Number", "sFundingAccountNumber", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_Funding, "Loan Proceeds to Account Name", "sFundingAccountName", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_Funding, "Loan Proceeds Further Credit to Account Name", "sFundingFurtherCreditToAccountName", Field.E_FieldType.Str, Field.E_ClassType.Txt); // OPM 109299 GF
                eB += m_Schema.Add(Category_Funding, "Loan Proceeds Further Credit to Account Number", "sFundingFurtherCreditToAccountNumber", Field.E_FieldType.Str, Field.E_ClassType.Txt); // OPM 109299 GF
                eB += m_Schema.Add(Category_Funding, "Funding Request Batch Number", "sFundingRequestBatchNumber", Field.E_FieldType.Str, Field.E_ClassType.Txt); // OPM 109299 GF
                eB += m_Schema.Add(Category_Funding, "Additional Wire Instructions 1", "sFundingAdditionalInstructionsLine1", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_Funding, "Additional Wire Instructions 2", "sFundingAdditionalInstructionsLine2", Field.E_FieldType.Str, Field.E_ClassType.Txt);//OPM 89787 RWN ^
                eB += m_Schema.Add(Category_Funding, "LOMA or LOMR", "sLomaLomrT", Field.E_FieldType.Enu, Field.E_ClassType.Txt
                    , new EnumRepMap(E_LomaLomrT.None, "None")
                    , new EnumRepMap(E_LomaLomrT.LOMA, "LOMA")
                    , new EnumRepMap(E_LomaLomrT.LOMR, "LOMR")
                    );
                eB += m_Schema.Add(Category_Funding, "Flood Cert Partial Zone", "sNfipIsPartialZone", Field.E_FieldType.Boo, Field.E_ClassType.Txt
                    , new BoolRepMap(true, "Yes")
                    , new BoolRepMap(false, "No")
                    );
                eB += m_Schema.Add(Category_Funding, "Flood Cert Mapping Company", "sNfipMappingCompany", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_Funding, "Flood Cert Date Community Entered Program", "sFfiaDateCommunityEnteredProgramD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_Funding, "Flood Cert Life of Loan upgraded", "sFloodCertificationIsLOLUpgraded", Field.E_FieldType.Boo, Field.E_ClassType.Txt
                    , new BoolRepMap(true, "Yes")
                    , new BoolRepMap(false, "No")
                    );
                eB += m_Schema.Add(Category_Funding, "Flood Cert Number", "sFloodCertId", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_Funding, "Flood Cert Determination Date", "sFloodCertificationDeterminationD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_Funding, "Flood Zone", "sNfipFloodZoneId", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_Funding, "Flood Cert Participation Status", "sFloodCertificationParticipationStatus", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_Funding, "Flood Cert NFIP Map Panel Effective/Revised Date", "sFloodCertificationMapD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_Funding, "Flood Cert Panel Number", "sFloodCertificationPanelNums", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_Funding, "Flood Cert Panel Suffix", "sFloodCertificationPanelSuffix", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_Funding, "Flood Cert Community Number", "sFloodCertificationCommunityNum", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_Funding, "Funding Notes", "sFundNotes", Field.E_FieldType.Str, Field.E_ClassType.LongText);

                //SERVICING
                eB += m_Schema.Add(Category_Servicing, "Net principal from servicing", "sServicingCollectedAndDisbursedFunds_Principal", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_Servicing, "Principal due from servicing", "sServicingDueFunds_Principal", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_Servicing, "Principal received and due from servicing", "sServicingCollectedAndDisbursedDueFunds_Principal", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_Servicing, "Net interest from servicing", "sServicingCollectedAndDisbursedFunds_Interest", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_Servicing, "Interest due from servicing", "sServicingDueFunds_Interest", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_Servicing, "Interest received and due from servicing", "sServicingCollectedAndDisbursedDueFunds_Interest", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_Servicing, "Net escrow from servicing", "sServicingCollectedAndDisbursedFunds_Escrow", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_Servicing, "Escrow received from servicing", "sServicingCollectedFunds_Escrow", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_Servicing, "Escrow due from servicing", "sServicingDueFunds_Escrow", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_Servicing, "Escrow received and due from servicing", "sServicingCollectedAndDisbursedDueFunds_Escrow", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_Servicing, "Net other received from servicing", "sServicingCollectedAndDisbursedFunds_Other", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_Servicing, "Other due from servicing", "sServicingDueFunds_Other", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_Servicing, "Other received and due from servicing", "sServicingCollectedAndDisbursedDueFunds_Other", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_Servicing, "Next payment due date", "sServicingNextPmtDue", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_Servicing, "Unpaid principal balance", "sServicingUnpaidPrincipalBalance", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_Servicing, "Escrow Pmt", "sServicingEscrowPmt", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_Servicing, "Next Payment Amount", "sServicingNextPmtAmt", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_Servicing, "Flood Ins Payment Type", "sFloodInsPolicyPaymentTypeT", Field.E_FieldType.Enu, Field.E_ClassType.Txt
                    , new EnumRepMap(E_InsPolicyPaymentTypeT.NonEscrowed, "Non-escrowed")
                    , new EnumRepMap(E_InsPolicyPaymentTypeT.Escrowed, "Escrowed")
                    );
                eB += m_Schema.Add(Category_Servicing, "Flood Ins Policy Payment Date", "sFloodInsPaymentDueD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_Servicing, "Flood Ins Policy Expiration Date", "sFloodInsPolicyExpirationD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_Servicing, "Flood Ins Payee Code", "sFloodInsPolicyPayeeCode", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_Servicing, "Flood Ins Policy Number", "sFloodInsPolicyNum", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_Servicing, "Flood Ins Payment Due Amt", "sFloodInsPaymentDueAmt", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_Servicing, "Haz Ins Payment Type", "sHazInsPolicyPaymentTypeT", Field.E_FieldType.Enu, Field.E_ClassType.Txt
                    , new EnumRepMap(E_InsPolicyPaymentTypeT.NonEscrowed, "Non-escrowed")
                    , new EnumRepMap(E_InsPolicyPaymentTypeT.Escrowed, "Escrowed")
                    );
                eB += m_Schema.Add(Category_Servicing, "Haz Ins Policy Payment Date", "sHazInsPaymentDueD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_Servicing, "Haz Ins Policy Expiration Date", "sHazInsPolicyExpirationD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_Servicing, "Haz Ins Payee Code", "sHazInsPolicyPayeeCode", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_Servicing, "Haz Ins Policy Number", "sHazInsPolicyNum", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_Servicing, "Haz Ins Payment Due Amt", "sHazInsPaymentDueAmt", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_Servicing, "Condo Ins Payment Type", "sCondoHO6InsPolicyPaymentTypeT", Field.E_FieldType.Enu, Field.E_ClassType.Txt
                    , new EnumRepMap(E_InsPolicyPaymentTypeT.NonEscrowed, "Non-escrowed")
                    , new EnumRepMap(E_InsPolicyPaymentTypeT.Escrowed, "Escrowed")
                    );

                eB += m_Schema.Add(Category_Servicing, "Real Estate Taxes Due Date", "sTaxTableRealEtxDueD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_Servicing, "Real Estate Taxes Paid Date", "sTaxTableRealEtxPaidD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");

                eB += m_Schema.Add(Category_Servicing, "School Taxes Due Date", "sTaxTableSchoolDueD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_Servicing, "School Taxes Paid Date", "sTaxTableSchoolPaidD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");

                eB += m_Schema.Add(Category_Servicing, "Other Tax Authority 1 Due Date", "sTaxTable1008DueD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_Servicing, "Other Tax Authority 1 Paid Date", "sTaxTable1008PaidD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");

                eB += m_Schema.Add(Category_Servicing, "Other Tax Authority 2 Due Date", "sTaxTable1009DueD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_Servicing, "Other Tax Authority 2 Paid Date", "sTaxTable1009PaidD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");

                eB += m_Schema.Add(Category_Servicing, "Other Tax Authority 3 Due Date", "sTaxTableU3DueD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_Servicing, "Other Tax Authority 3 Paid Date", "sTaxTableU3PaidD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");

                eB += m_Schema.Add(Category_Servicing, "Other Tax Authority 4 Due Date", "sTaxTableU4DueD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_Servicing, "Other Tax Authority 4 Paid Date", "sTaxTableU4PaidD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");

                // OPM 149891
                eB += m_Schema.Add(Category_Servicing, "Subservicer loan number", "sSubservicerLoanNm", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_Servicing, "Servicing Effective Transfer Date", "sGLServTransEffD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_Servicing, "Subservicer MERS ID", "sSubservicerMersId", Field.E_FieldType.Str, Field.E_ClassType.Txt);

                // OPM 297931
                eB += m_Schema.Add(Category_Servicing, "Payoff Statement Payoff Date", "sPayoffStatementPayoffD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");

                // OPM 50507
                eB += m_Schema.Add(Category_Servicing, "Filing Year", "s1098FilingYear", Field.E_FieldType.Int, Field.E_ClassType.Cnt, "#,#;-#,#;0");
                eB += m_Schema.Add(Category_Servicing, "Current Year Interest and Points", "s1098CurrentYearInterestAndPoints", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_Servicing, "Previous Year Interest and Points", "s1098PreviousYearInterestAndPoints", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");

                //TRANSACTION
                eB += m_Schema.Add(Category_Transactions, "Total pmts received", "sTransactionsTotPmtsRecvd", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_Transactions, "Total pmts made", "sTransactionsTotPmtsMade", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_Transactions, "Net pmts to date", "sTransactionsNetPmtsToDate", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_Transactions, "Total pmts receivable", "sTransactionsTotPmtsReceivable", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_Transactions, "Total pmts payable", "sTransactionsTotPmtsPayable", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_Transactions, "Net pmts due", "sTransactionsNetPmtsDue", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");

                //PURCHASE ADVICE
                eB += m_Schema.Add(Category_PurchaseAdvice, "Purchase advice unpaid principal balance", "sPurchaseAdviceUnpaidPBal", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_PurchaseAdvice, "Purchase advice interest adjustments", "sPurchaseAdviceInterestTotalAdj", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_PurchaseAdvice, "Purchase advice escrow amt due investor", "sPurchaseAdviceEscrowAmtDueInv", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_PurchaseAdvice, "Purchase advice total fees", "sPurchaseAdviceFeesTotal", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_PurchaseAdvice, "Purchase advice other adjustment description", "sPurchaseAdviceSummaryOtherAdjDesc", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_PurchaseAdvice, "Purchase advice other adjustment", "sPurchaseAdviceSummaryOtherAdjVal", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_PurchaseAdvice, "Purchase advice total due seller", "sPurchaseAdviceSummaryTotalDueSeller", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_PurchaseAdvice, "Purchase advice investor name", "sPurchaseAdviceSummaryInvNm", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_PurchaseAdvice, "Purchase advice investor program name", "sPurchaseAdviceSummaryInvPgNm", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_PurchaseAdvice, "Purchase advice investor loan #", "sPurchaseAdviceSummaryInvLoanNm", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_PurchaseAdvice, "Servicing disposition", "sPurchaseAdviceSummaryServicingStatus", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_PurchaseAdvice, "1st payment due investor date", "sInvSchedDueD1", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");

				eB += m_Schema.Add(Category_PurchaseAdvice, "Purchase Advice Base Price - Price", "sPurchaseAdviceBasePrice_Field1", Field.E_FieldType.Dec, Field.E_ClassType.Txt, "N3"); //Should be N6; waiting on OPM 86234
                eB += m_Schema.Add(Category_PurchaseAdvice, "Purchase Advice Base Price - Amount", "sPurchaseAdviceBasePrice_Field2", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C"); //opm 51352 fs 06/17/10

				eB += m_Schema.Add(Category_PurchaseAdvice, "Purchase Advice Adjustments", "sPurchaseAdviceAdjustments_Field2", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C"); //opm 51352 fs 06/17/10
                eB += m_Schema.Add(Category_PurchaseAdvice, "Purchase Advice SRP", "sPurchaseAdviceSRP_Field2", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C"); //opm 51352 fs 06/17/10
                eB += m_Schema.Add(Category_PurchaseAdvice, "Purchase Advice Investor Main Address", "sPurchaseAdviceSummaryInvMainAddr", Field.E_FieldType.Str, Field.E_ClassType.Txt); // opm 95063 gf
                eB += m_Schema.Add(Category_PurchaseAdvice, "Purchase Advice Investor Payment Address", "sPurchaseAdviceSummaryInvPmtAddr", Field.E_FieldType.Str, Field.E_ClassType.Txt); // opm 95063 gf
                eB += m_Schema.Add(Category_PurchaseAdvice, "Purchase Advice Net Price - Price", "sPurchaseAdviceNetPrice_Field1", Field.E_FieldType.Dec, Field.E_ClassType.Txt, "N6");
                eB += m_Schema.Add(Category_PurchaseAdvice, "Purchase Advice Net Price - Amount", "sPurchaseAdviceNetPrice_Field2", Field.E_FieldType.Dec, Field.E_ClassType.Txt, "N6");
                eB += m_Schema.Add(Category_PurchaseAdvice, "Purchase Advice Total Price - Price", "sPurchaseAdviceTotalPrice_Field1", Field.E_FieldType.Dec, Field.E_ClassType.Txt, "N6");
                eB += m_Schema.Add(Category_PurchaseAdvice, "Purchase Advice Total Price - Amount", "sPurchaseAdviceTotalPrice_Field2", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_PurchaseAdvice, "Total Price % Adj", "sPurchaseAdviceTotalPrice_Field3", Field.E_FieldType.Dec, Field.E_ClassType.Pct, "N3");


                //DISBURSEMENT
                eB += m_Schema.Add(Category_Disbursement, "Warehouse line fees", "sDisbursementWarehouseLineFees", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_Disbursement, "Warehouse line interest", "sDisbursementWarehouseLineInt", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_Disbursement, "Disbursement other adjustment", "sDisbursementOther", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_Disbursement, "Net funds received from disbursement", "sDisbursementNetReceived", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");

                //ACCOUNTING
                eB += m_Schema.Add(Category_Accounting, "Total cash received", "sTotalCash", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_Accounting, "Total payables", "sTotalPayables", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_Accounting, "Total receivables", "sTotalReceivables", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_Accounting, "Total gain", "sTotalGain", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_Accounting, "Total % gain", "sTotalGainPercent", Field.E_FieldType.Dec, Field.E_ClassType.Pct, "N3");
                eB += m_Schema.Add(Category_Accounting, "Loans held for sale- Servicing", "sServicingLHS", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_Accounting, "Repurchase Date", "sRepurchD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_Accounting, "First Pmt After Repurchase Due Date", "sFirstPmtAfterRepurchDueD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_Accounting, "Unpaid Balance At Repurchase Date", "sUnpaidBalAtRepurch", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                #endregion
                // ...

                #region Tasks                
                
                eB += m_Schema.Add(Category_Tasks, "Active Tasks", "sActiveTasksDisplay", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_Tasks, "Tasks Due / Past Due", "sTaskDueAndPastDueDisplay", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_Tasks, "Tasks Past Due", "sNumOfOutStandingDuePastDueTasks", Field.E_FieldType.Int, Field.E_ClassType.Cnt, "#,#;-#,#;0");
                eB += m_Schema.Add(Category_Tasks, "Active Tasks Count", "sNumOfActiveTasks", Field.E_FieldType.Int, Field.E_ClassType.Cnt, "#,#;-#,#;0");
                eB += m_Schema.Add(Category_Tasks, "Active Tasks Due Today", "sNumOfActiveDueTodayTasks", Field.E_FieldType.Int, Field.E_ClassType.Cnt, "#,#;-#,#;0");
                eB += m_Schema.Add(Category_Tasks, "Active Tasks Past Due", "sNumOfActivePastDueTasks", Field.E_FieldType.Int, Field.E_ClassType.Cnt, "#,#;-#,#;0");
                eB += m_Schema.Add(Category_Tasks, "Outstanding Tasks", "sNumOfOutstandingTasks", Field.E_FieldType.Int, Field.E_ClassType.Cnt, "#,#;-#,#;0");
                eB += m_Schema.Add(Category_Tasks, "Outstanding Tasks Due Today", "sNumOfOutStandingDueTodayTasks", Field.E_FieldType.Int, Field.E_ClassType.Cnt, "#,#;-#,#;0");
                eB += m_Schema.Add(Category_Tasks, "Outstanding Tasks Past Due", "sNumOfOutStandingPastDueTasks", Field.E_FieldType.Int, Field.E_ClassType.Cnt, "#,#;-#,#;0");
                eB += m_Schema.Add(Category_Tasks, "Closed Tasks", "sNumOfClosedTasks", Field.E_FieldType.Int, Field.E_ClassType.Cnt, "#,#;-#,#;0");
                
                #endregion

                #region ( Field Name Translations )

                eB += m_Schema.Set( "sLAmt" , "sLAmtCalc");

                #endregion

                #region (Special fields)

                var borrowerSocialDependentFields = new Dictionary<string, string>()
                {
                    ["aBSsnEncrypted"] = "aBSsnEncrypted",
                    ["sEncryptionKey"] = "sEncryptionKey",
                    ["sEncryptionMigrationVersion"] = "sEncryptionMigrationVersion",
                };
                eB += m_Schema.AddSpecial(
                    Category_LoanInfo, 
                    "aBSsn", 
                    "CASE WHEN aBSsnLastFour <> '' THEN '***-**-' + aBSsnLastFour ELSE '' END as aBSsn", 
                    Field.E_FieldType.Str, 
                    Field.E_ClassType.Txt, 
                    borrowerSocialDependentFields);

                var coborrowerSocialDependentFields = new Dictionary<string, string>()
                {
                    ["aCSsnEncrypted"] = "aCSsnEncrypted",
                    ["sEncryptionKey"] = "sEncryptionKey",
                    ["sEncryptionMigrationVersion"] = "sEncryptionMigrationVersion",
                };
                eB += m_Schema.AddSpecial(
                    Category_LoanInfo, 
                    "aCSsn", 
                    "CASE WHEN aCSsnLastFour <> '' THEN '***-**-' + aCSsnLastFour ELSE '' END as aCSsn", 
                    Field.E_FieldType.Str, 
                    Field.E_ClassType.Txt, 
                    coborrowerSocialDependentFields);

                #endregion

                #region shipping
                eB += m_Schema.Add(Category_Shipping, "GSE Commitment ID", "sGseDeliveryCommitmentId", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_Shipping, "Guarantee Fee", "sGseDeliveryGuaranteeF", Field.E_FieldType.Dec, Field.E_ClassType.Csh, "C");
                eB += m_Schema.Add(Category_Shipping, "Document Custodian Name", "sGseDeliveryDocumentCustodianName", Field.E_FieldType.Str, Field.E_ClassType.Txt);
                eB += m_Schema.Add(Category_Shipping, "Required Docs Not Ordered Count", "sRequiredDocsNotOrderedCount", Field.E_FieldType.Int, Field.E_ClassType.Cnt, "#,#;-#,#;0");
                eB += m_Schema.Add(Category_Shipping, "Required Docs Not Received Count", "sRequiredDocsNotReceivedCount", Field.E_FieldType.Int, Field.E_ClassType.Cnt, "#,#;-#,#;0");
                eB += m_Schema.Add(Category_Shipping, "Required Docs Not Shipped Count", "sRequiredDocsNotShippedCount", Field.E_FieldType.Int, Field.E_ClassType.Cnt, "#,#;-#,#;0");
                eB += m_Schema.Add(Category_Shipping, "Required Docs Not Reviewed Count", "sRequiredDocsNotReviewedCount", Field.E_FieldType.Int, Field.E_ClassType.Cnt, "#,#;-#,#;0");
                eB += m_Schema.Add(Category_Shipping, "Loan Documents Archived Date", "sLoanDocsArchiveD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_Shipping, "Trailing Documents Archived Date", "sTrailingDocsArchiveD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");

                eB += m_Schema.Add(Category_Shipping, "Disclosure Regulation Type", "sDisclosureRegulationT", Field.E_FieldType.Enu, Field.E_ClassType.Txt
                   , new EnumRepMap(E_sDisclosureRegulationT.Blank, "")
                   , new EnumRepMap(E_sDisclosureRegulationT.GFE, "GFE")
                   , new EnumRepMap(E_sDisclosureRegulationT.TRID, "TRID")
                   );

                eB += m_Schema.Add(Category_Shipping, "Has UCD File Been Delivered", "sHasUcdFileBeenDelivered", Field.E_FieldType.Boo, Field.E_ClassType.Txt, new BoolRepMap(true, "Yes"), new BoolRepMap(false, "No"));
                eB += m_Schema.Add(Category_Shipping, "Last UCD Delivery Date", "sLastUcdDeliveryD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                eB += m_Schema.Add(Category_Shipping, "First UCD Delivery Date", "sFirstUcdDeliveryD", Field.E_FieldType.Dtm, Field.E_ClassType.Cal, "M/d/yyyy");
                #endregion

                // Make sure these fields are selected with each extent
                // retrieved from the database so we can pull out the
                // access control details for each loan of the report.
                // See case 2354 for more details.

                #region ( Access Control Fields )

                m_ControlFields.Add( "sBrokerId"                    , "cBrokerId"                    );
                m_ControlFields.Add( "sBranchId"                    , "cBranchId"                    );
                m_ControlFields.Add( "sLNm"                         , "cLNm"                         );
                m_ControlFields.Add( "sPrimBorrowerFullNm"          , "cPrimBorrowerFullNm"          );
                m_ControlFields.Add( "sStatusT"                     , "cStatusT"                     );
                m_ControlFields.Add( "sLienPosT"                    , "cLienPosT"                    );
                m_ControlFields.Add( "sOpenedD"                     , "cOpenedD"                     );
                m_ControlFields.Add( "sNoteIRSubmitted"             , "cNoteIRSubmitted"             );
                m_ControlFields.Add( "sIsRateLocked"                , "cIsRateLocked"                );
                m_ControlFields.Add( "IsTemplate"                   , "IsTemplate"                   );
                m_ControlFields.Add( "IsValid"                      , "IsValid"                      );
                m_ControlFields.Add( "sEmployeeManagerId"           , "sEmployeeManagerId"           );
                m_ControlFields.Add( "sEmployeeUnderwriterId"       , "sEmployeeUnderwriterId"       );
                m_ControlFields.Add( "sEmployeeLockDeskId"          , "sEmployeeLockDeskId"          );
                m_ControlFields.Add( "sEmployeeProcessorId"         , "sEmployeeProcessorId"         );
                m_ControlFields.Add( "sEmployeeLoanOpenerId"        , "sEmployeeLoanOpenerId"        );
                m_ControlFields.Add( "sEmployeeLoanRepId"           , "sEmployeeLoanRepId"           );
                m_ControlFields.Add( "sEmployeeLenderAccExecId"     , "sEmployeeLenderAccExecId"     );
                m_ControlFields.Add( "sEmployeeRealEstateAgentId"   , "sEmployeeRealEstateAgentId"   );
                m_ControlFields.Add( "sEmployeeCallCenterAgentId"   , "sEmployeeCallCenterAgentId"   );
                m_ControlFields.Add( "sEmployeeCloserId"            , "sEmployeeCloserId"            ); //opm 53218 fs 06/28/10
                m_ControlFields.Add( "sEmployeeShipperId"           , "sEmployeeShipperId"           );
                m_ControlFields.Add( "sEmployeeFunderId"            , "sEmployeeFunderId"            );
                m_ControlFields.Add( "sEmployeePostCloserId"        , "sEmployeePostCloserId"        );
                m_ControlFields.Add( "sEmployeeInsuringId"          , "sEmployeeInsuringId"          );
                m_ControlFields.Add( "sEmployeeCollateralAgentId"   , "sEmployeeCollateralAgentId"   );
                m_ControlFields.Add( "sEmployeeDocDrawerId"         , "sEmployeeDocDrawerId"         );
                m_ControlFields.Add( "sEmployeeCreditAuditorId"     , "sEmployeeCreditAuditorId"     );
                m_ControlFields.Add( "sEmployeeDisclosureDeskId"    , "sEmployeeDisclosureDeskId"    );
                m_ControlFields.Add( "sEmployeeJuniorProcessorId"   , "sEmployeeJuniorProcessorId"   );
                m_ControlFields.Add( "sEmployeeJuniorUnderwriterId" , "sEmployeeJuniorUnderwriterId" );
                m_ControlFields.Add( "sEmployeeLegalAuditorId"      , "sEmployeeLegalAuditorId"      );
                m_ControlFields.Add( "sEmployeeLoanOfficerAssistantId", "sEmployeeLoanOfficerAssistantId");
                m_ControlFields.Add( "sEmployeePurchaserId"         , "sEmployeePurchaserId"         );
                m_ControlFields.Add( "sEmployeeQCComplianceId"      , "sEmployeeQCComplianceId"      );
                m_ControlFields.Add( "sEmployeeSecondaryId"         , "sEmployeeSecondaryId"         );
                m_ControlFields.Add( "sEmployeeServicingId"         , "sEmployeeServicingId"         );
                m_ControlFields.Add( "PmlExternalManagerEmployeeId" , "PmlExternalManagerEmployeeId" );
                m_ControlFields.Add("sRateLockStatusT", "cRateLockStatusT");


                m_ControlFields.Add("sTeamManagerId", "sTeamManagerId");
                m_ControlFields.Add("sTeamUnderwriterId", "sTeamUnderwriterId");
                m_ControlFields.Add("sTeamLockDeskId", "sTeamLockDeskId");
                m_ControlFields.Add("sTeamProcessorId", "sTeamProcessorId");
                m_ControlFields.Add("sTeamLoanOpenerId", "sTeamLoanOpenerId");
                m_ControlFields.Add("sTeamLoanRepId", "sTeamLoanRepId");
                m_ControlFields.Add("sTeamLenderAccExecId", "sTeamLenderAccExecId");
                m_ControlFields.Add("sTeamRealEstateAgentId", "sTeamRealEstateAgentId");
                m_ControlFields.Add("sTeamCallCenterAgentId", "sTeamCallCenterAgentId");
                m_ControlFields.Add("sTeamCloserId", "sTeamCloserId"); //opm 53218 fs 06/28/10
                m_ControlFields.Add("sTeamShipperId", "sTeamShipperId");
                m_ControlFields.Add("sTeamFunderId", "sTeamFunderId");
                m_ControlFields.Add("sTeamPostCloserId", "sTeamPostCloserId");
                m_ControlFields.Add("sTeamInsuringId", "sTeamInsuringId");
                m_ControlFields.Add("sTeamCollateralAgentId", "sTeamCollateralAgentId");
                m_ControlFields.Add("sTeamDocDrawerId", "sTeamDocDrawerId");
                m_ControlFields.Add("sTeamCreditAuditorId", "sTeamCreditAuditorId");
                m_ControlFields.Add("sTeamDisclosureDeskId", "sTeamDisclosureDeskId");
                m_ControlFields.Add("sTeamJuniorProcessorId", "sTeamJuniorProcessorId");
                m_ControlFields.Add("sTeamJuniorUnderwriterId", "sTeamJuniorUnderwriterId");
                m_ControlFields.Add("sTeamLegalAuditorId", "sTeamLegalAuditorId");
                m_ControlFields.Add("sTeamLoanOfficerAssistantId", "sTeamLoanOfficerAssistantId");
                m_ControlFields.Add("sTeamPurchaserId", "sTeamPurchaserId");
                m_ControlFields.Add("sTeamQCComplianceId", "sTeamQCComplianceId");
                m_ControlFields.Add("sTeamSecondaryId", "sTeamSecondaryId");
                m_ControlFields.Add("sTeamServicingId", "sTeamServicingId");

				#endregion

                // If any errors were reported, we log them and move
                // on with initialization.

                foreach( Exception eX in eB )
                {
                    Tools.LogError( eX );
                }
            }
            catch( Exception e )
            {
                // Write out our misfortune.

                Tools.LogError("Loan reporting: Failed to initialize schema." , e );
            }
        }

        /// <summary>
        /// Gets the ID of the default PML pipeline report ID.
        /// </summary>
        /// <returns>
        /// The ID of the default PML pipeline report ID.
        /// </returns>
        private static Guid GetDefaultPmlPipelineReportId()
        {
            SqlParameter[] parameters = {
                                             new SqlParameter("@AdminBrokerId", BrokerDB.AdminBr)
                                         };

            return (Guid)StoredProcedureHelper.ExecuteScalar(DataSrc.LOShareROnly, "GetAdminBrokerPipelineQueryTemplateId", parameters);
        }

        /// <summary>
        /// Gets the ID of the default non-PML pipeline report ID.
        /// </summary>
        /// <returns>
        /// The ID of the default non-PML pipeline report ID.
        /// </returns>
        private static Guid GetDefaultNonPmlPipelineReportId()
        {
            // 7/15/2014 dd - Note: The variable isPmlEnabledParameter IS NOT the same as new SqlParameter("@AdminBrokerId", 0)
            SqlParameter isPmlEnabledParameter = new SqlParameter("@IsPmlEnabled", SqlDbType.Int);
            isPmlEnabledParameter.Value = 0;

            SqlParameter[] parameters = {
                                             new SqlParameter("@AdminBrokerId", BrokerDB.AdminBr),
                                             isPmlEnabledParameter
                                         };

            return (Guid)StoredProcedureHelper.ExecuteScalar(DataSrc.LOShareROnly, "GetAdminBrokerPipelineQueryTemplateId", parameters);
        }

        private static IEnumerable<EnumRepMap> GenerateStatusRepMaps()
        {
            // dont' worry about the order, just make sure it has all of 'em.
            return new List<EnumRepMap>{
                      new EnumRepMap( E_sStatusT.Loan_Approved          , "Approved"            )
                    , new EnumRepMap( E_sStatusT.Loan_ClearToClose      , "Clear to Close"      )
                    , new EnumRepMap( E_sStatusT.Loan_ClearToPurchase   , "Clear To Purchase"   )
                    , new EnumRepMap( E_sStatusT.Loan_ConditionReview   , "Condition Review"    )
                    , new EnumRepMap( E_sStatusT.Loan_CounterOffer      , "Counter Offer Approved")
                    , new EnumRepMap( E_sStatusT.Loan_DocsBack          , "Docs Back"           )
                    , new EnumRepMap( E_sStatusT.Loan_DocsDrawn         , "Docs Drawn"          )
                    , new EnumRepMap( E_sStatusT.Loan_DocsOrdered       , "Docs Ordered"        )
                    , new EnumRepMap( E_sStatusT.Loan_Docs              , "Docs Out"            )
                    , new EnumRepMap( E_sStatusT.Loan_DocumentCheck     , "Document Check"      )
                    , new EnumRepMap( E_sStatusT.Loan_DocumentCheckFailed, "Document Check Failed")
                    , new EnumRepMap( E_sStatusT.Loan_FinalDocs         , "Final Docs"          )
                    , new EnumRepMap( E_sStatusT.Loan_FinalUnderwriting , "Final Underwriting"  )
                    , new EnumRepMap( E_sStatusT.Loan_Funded            , "Funded"              )
                    , new EnumRepMap( E_sStatusT.Loan_FundingConditions , "Funding Conditions"  )
                    , new EnumRepMap( E_sStatusT.Loan_InFinalPurchaseReview, "In Final Purchase Review")
                    , new EnumRepMap( E_sStatusT.Loan_InPurchaseReview  , "In Purchase Review"  )
                    , new EnumRepMap( E_sStatusT.Loan_Underwriting      , "In Underwriting"     )
                    , new EnumRepMap( E_sStatusT.Loan_InvestorConditions, "Investor Conditions" )       //  tied to sSuspendedByInvestorD
                    , new EnumRepMap( E_sStatusT.Loan_InvestorConditionsSent, "Investor Conditions Sent")   //  tied to sCondSentToInvestorD
                    , new EnumRepMap( E_sStatusT.Lead_Canceled          , "Lead Canceled"       )
                    , new EnumRepMap( E_sStatusT.Lead_Declined          , "Lead Declined"       )
                    , new EnumRepMap( E_sStatusT.Lead_New               , "Lead New"            )
                    , new EnumRepMap( E_sStatusT.Lead_Other             , "Lead Other"          )
                    , new EnumRepMap( E_sStatusT.Loan_Archived          , "Loan Archived"       )
                    , new EnumRepMap( E_sStatusT.Loan_Canceled          , "Loan Canceled"       )
                    , new EnumRepMap( E_sStatusT.Loan_Closed            , "Loan Closed"         )
                    , new EnumRepMap( E_sStatusT.Loan_OnHold            , "Loan On-hold"        )
                    , new EnumRepMap( E_sStatusT.Loan_Open              , "Loan Open"           )
                    , new EnumRepMap( E_sStatusT.Loan_Other             , "Loan Other"          )
                    , new EnumRepMap( E_sStatusT.Loan_Purchased         , "Loan Purchased"      ) // don't confuse with the old Loan_LoanPurchased.
                    , new EnumRepMap( E_sStatusT.Loan_Rejected          , "Loan Denied"         )
                    , new EnumRepMap( E_sStatusT.Loan_LoanPurchased     , "Loan Sold"           )
                    , new EnumRepMap( E_sStatusT.Loan_LoanSubmitted     , "Loan Submitted"      )
                    , new EnumRepMap( E_sStatusT.Loan_Suspended         , "Loan Suspended"      )
                    , new EnumRepMap( E_sStatusT.Loan_WebConsumer       , "Loan Web Consumer"   ) // // 9/30/2010 dd - Add this loan status for engine validation to work. But I don't think we use this status.
                    , new EnumRepMap( E_sStatusT.Loan_Withdrawn         , "Loan Withdrawn"      )
                    , new EnumRepMap( E_sStatusT.Loan_Preapproval       , "Pre-approved"        )
                    , new EnumRepMap( E_sStatusT.Loan_PreDocQC          , "Pre-doc QC"          )
                    , new EnumRepMap( E_sStatusT.Loan_PreProcessing     , "Pre-processing"      )
                    , new EnumRepMap( E_sStatusT.Loan_PrePurchaseConditions, "Pre-purchase Conditions")
                    , new EnumRepMap( E_sStatusT.Loan_Prequal           , "Pre-qual"            )
                    , new EnumRepMap( E_sStatusT.Loan_PreUnderwriting   , "Pre-underwriting"    )
                    , new EnumRepMap( E_sStatusT.Loan_Processing        , "Processing"          )
                    , new EnumRepMap( E_sStatusT.Loan_ReadyForSale      , "Ready For Sale"      )
                    , new EnumRepMap( E_sStatusT.Loan_Recorded          , "Recorded"            )
                    , new EnumRepMap( E_sStatusT.Loan_Registered        , "Registered"          ) //opm 33577 fs 07/29/09
                    , new EnumRepMap( E_sStatusT.Loan_Shipped           , "Loan Shipped"        )
                    , new EnumRepMap( E_sStatusT.Loan_SubmittedForFinalPurchaseReview, "Submitted For Final Purchase Review")
                    , new EnumRepMap( E_sStatusT.Loan_SubmittedForPurchaseReview, "Submitted For Purchase Review")
            };
        }

        public FieldLookup Fields
        {
            get { return m_Schema; }
        }

        public static Schema Schema
        {
            get { return m_Schema; }
        }

        /// <summary>
        /// Gets a value indicating whether the specified field ID is
        /// a field that requires special handling.
        /// </summary>
        /// <param name="fieldId">
        /// The ID of the special field.
        /// </param>
        /// <returns>
        /// True if the field requires special handling, false otherwise.
        /// </returns>
        public static bool IsSpecialField(string fieldId)
        {
            return m_Schema.LookupSpecialField(fieldId) != null;
        }

        /// <summary>
        /// Calculate the user's view and build the report's working
        /// set from the query.  We return the extent as an enumerable
        /// cache of reportable loan objects.
        /// </summary>
        /// <param name="rQuery">
        /// Query to process and build our select statement from the
        /// given filter and conditions.
        /// </param>
        /// <param name="userInfo">
        /// The user principal loading the report.
        /// </param>
        /// <param name="extentScope">
        /// The scope of access to the report.
        /// </param>
        /// <param name="maskSsn">
        /// True to mask SSN values, false otherwise.
        /// </param>
        /// <param name="userId">
        /// The ID of the user running the report, if different
        /// from <paramref name="userInfo"/>.
        /// </param>
        /// <returns>
        /// Returns data table of working set.
        /// </returns>
        private SimpleRawResultDataTable GetExtent(Query rQuery, AbstractUserPrincipal userInfo, E_ReportExtentScopeT extentScope, bool maskSsn, Guid? userId = null, bool enforceMaxTableSize = false)
        {
            try
            {
                var useTableWithEncryptionHandling = !maskSsn && rQuery.HasEncryptedFields;

                SimpleRawResultDataTable resultDataTable;
                if (useTableWithEncryptionHandling)
                {
                    resultDataTable = new EncryptedDataHandlingResultDataTable(rQuery.Label, userInfo, userId ?? userInfo.UserId);
                }
                else
                {
                    resultDataTable = new SimpleRawResultDataTable(userInfo);
                }

                var sqlQueriesHolder = GetQueryStrings(rQuery, userInfo, extentScope, maskSsn, userId);

                if (enforceMaxTableSize && ConstStage.MaxReportSizeBeforeFailCustomReport != 0)
                {
                    var rowCount = resultDataTable.GetCount(sqlQueriesHolder.SqlQueryForCountQuery.ToString(), sqlQueriesHolder.Params);
                    var size = rowCount * rQuery.Columns.Count;
                    if (size > ConstStage.MaxReportSizeBeforeFailCustomReport)
                    {
                        throw new CBaseException("Your report is too big too be run manually.  Consider adding conditions or removing columns to reduce the report size.",
                            $"Report's size was {size} ({rowCount} rows, {rQuery.Columns.Count} columns) which exceeds {nameof(ConstStage.MaxReportSizeBeforeFailCustomReport)} ({ConstStage.MaxReportSizeBeforeFailCustomReport})");
                    }
                }

                var sw = new System.Diagnostics.Stopwatch();
                sw.Start();
                resultDataTable.LoadResult(sqlQueriesHolder.SqlQueryForTableQuery.ToString(), sqlQueriesHolder.Params);
                sw.Stop();
                var message = SerializationHelper.JsonNetAnonymousSerialize(new
                {
                    NumRows = resultDataTable.RowCount,
                    NumColumns = rQuery.Columns.Count,
                    Size = rQuery.Columns.Count * resultDataTable.RowCount,
                    QueryName = rQuery.Label,
                    Scope = extentScope.ToString(),
                    UserId = userId
                });

                Tools.LogInfo(
                    category: "CustomReportLoadTiming", 
                    message: message, 
                    timeProcessingInMs: (int)sw.ElapsedMilliseconds);

                return resultDataTable;
            }
            catch( CBaseException cb)
            {
                Tools.LogError(string.Format("Loan reporting: Failed to get extent of {0} for {1}.", rQuery.Id, userInfo.EmployeeId), cb);
                throw;
            }
            catch( Exception e )
            {
                Tools.LogError( string.Format( "Loan reporting: Failed to get extent of {0} for {1}.", rQuery.Id, userInfo.EmployeeId) , e);
            }

            return null;
        }

        private class SqlQuerysHolder
        {
            public StringBuilder SqlQueryForTableQuery;
            public StringBuilder SqlQueryForCountQuery;
            public System.Collections.Specialized.StringDictionary Params;
        }

        private SqlQuerysHolder GetQueryStrings(Query rQuery, AbstractUserPrincipal userInfo, E_ReportExtentScopeT extentScope, bool maskSsn, Guid? userId = null)
        {
            // Get all the reportable loan objects after we calculate the dataset.

            // Initialize the query command.  This guy generates the final sql
            // command on the fly, after we initialize all the proper inputs.
            SqlQueryTranslation sXlate = new SqlQueryTranslation();

            if (ConstStage.AvoidLockForCustomReportSqlQueryGeneration)
            {
                sXlate.Translate(userInfo, rQuery, m_ControlFields, m_Schema, maskSsn);
            }
            else
            {
                lock (m_Schema)
                {
                    sXlate.Translate(userInfo, rQuery, m_ControlFields, m_Schema, maskSsn);
                }
            }

            //opm 32079 fs 09/07/09
            bool bNeedsBranch = rQuery.Columns.bNeedsBranch || rQuery.Relates.bNeedsBranch;
            bool bNeedsMortgagePool = rQuery.Columns.bNeedsMortgagePool || rQuery.Relates.bNeedsMortgagePool;

            //opm 191618 ejm 10/10/2014 - To get CompanyID present in Agents page
            bool bNeedsPMLBroker = rQuery.Columns.bNeedsPMLBroker || rQuery.Relates.bNeedsPMLBroker;

            string joinClause = "LOAN_FILE_CACHE WITH(NOLOCK) JOIN LOAN_FILE_CACHE_2 WITH(NOLOCK) ON LOAN_FILE_CACHE.sLId = LOAN_FILE_CACHE_2.sLId JOIN LOAN_FILE_CACHE_3 WITH(NOLOCK) ON LOAN_FILE_CACHE.sLId = LOAN_FILE_CACHE_3.sLId JOIN LOAN_FILE_CACHE_4 WITH(NOLOCK) ON LOAN_FILE_CACHE.sLid = LOAN_FILE_CACHE_4.sLId ";
            if (bNeedsBranch)
            {
                joinClause += " JOIN Branch WITH(NOLOCK) ON LOAN_FILE_CACHE.sBranchId = BRANCH.BranchId ";
            }

            if (bNeedsMortgagePool)
            {
                joinClause += " LEFT OUTER JOIN MORTGAGE_POOL WITH(NOLOCK) ON LOAN_FILE_CACHE_2.sMortgagePoolId = MORTGAGE_POOL.PoolId ";
            }

            if (bNeedsPMLBroker)
            {
                joinClause += " LEFT OUTER JOIN PML_BROKER WITH(NOLOCK) ON LOAN_FILE_CACHE.sBrokerId = PML_BROKER.BrokerId AND LOAN_FILE_CACHE.sPmlBrokerId = PML_BROKER.PmlBrokerId ";
            }

            if (userInfo.Type == "B")
            {
                if (extentScope == E_ReportExtentScopeT.Broker || userInfo.HasPermission(Permission.BrokerLevelAccess) == true && extentScope == E_ReportExtentScopeT.Default)
                {
                    sXlate.Source = string.Format("{0} where ( sBrokerId = '{1}' ) and ( IsValid = 1 ) and ( IsTemplate = 0 ) and ( sLoanFileT IS NULL OR sLoanFileT = 0)", joinClause, userInfo.BrokerId);
                }
                else if (extentScope == E_ReportExtentScopeT.Branch || userInfo.HasPermission(Permission.BranchLevelAccess) == true && extentScope == E_ReportExtentScopeT.Default)
                {

                    // Branch-level access gets you team access even if
                    // that loan is not in your branch.  OPM 178119
                    string teamWhere = "";
                    if (userInfo.GetTeams().Count != 0)
                    {
                        var teams = userInfo.GetTeams();
                        string teamList = "";
                        foreach (var team in teams)
                        {

                            teamList += (teamList.Length != 0 ? ", " : " ") + DbAccessUtils.SQLString(team.ToString());
                        }

                        if (teamList.Length != 0)
                        {
                            teamWhere = string.Format(" ( sTeamLenderAccExecId IN {0} or sTeamLockDeskId IN {0} or sTeamUnderwriterId IN {0} or sTeamRealEstateAgentId IN {0} or sTeamCallCenterAgentId IN {0} or sTeamLoanOpenerId IN {0} or sTeamLoanRepId IN {0} or sTeamProcessorId IN {0} or sTeamManagerId IN {0} or sTeamCloserId IN {0} or sTeamShipperId IN {0} or sTeamFunderId IN {0} or sTeamPostCloserId IN {0} or sTeamInsuringId IN {0} or sTeamCollateralAgentId IN {0} or sTeamDocDrawerId IN {0} or sTeamCreditAuditorId IN {0} or sTeamDisclosureDeskId IN {0} or sTeamJuniorProcessorId IN {0} or sTeamJuniorUnderwriterId IN {0} or sTeamLegalAuditorId IN {0} or sTeamLoanOfficerAssistantId IN {0} or sTeamPurchaserId IN {0} or sTeamQCComplianceId IN {0} or sTeamSecondaryId IN {0} or sTeamServicingId IN {0} ) OR ",
                                " ( " + teamList + " ) ");
                        }
                    }

                    sXlate.Source = string.Format
                        ("{0} where ( sBrokerId = '{1}' ) and ( sBranchId = '{2}' or {4} sEmployeeLenderAccExecId = '{3}' or sEmployeeLockDeskId = '{3}' or sEmployeeUnderwriterId = '{3}' or sEmployeeRealEstateAgentId = '{3}' or sEmployeeCallCenterAgentId = '{3}' or sEmployeeLoanOpenerId = '{3}' or sEmployeeLoanRepId = '{3}' or sEmployeeProcessorId = '{3}' or sEmployeeManagerId = '{3}' or sEmployeeCloserId = '{3}' or sEmployeeShipperId = '{3}' or sEmployeeFunderId = '{3}' or sEmployeePostCloserId = '{3}' or sEmployeeInsuringId = '{3}' or sEmployeeCollateralAgentId = '{3}' or sEmployeeDocDrawerId = '{3}' or sEmployeeCreditAuditorId = '{3}' or sEmployeeDisclosureDeskId = '{3}' or sEmployeeJuniorProcessorId = '{3}' or sEmployeeJuniorUnderwriterId = '{3}' or sEmployeeLegalAuditorId = '{3}' or sEmployeeLoanOfficerAssistantId = '{3}' or sEmployeePurchaserId = '{3}' or sEmployeeQCComplianceId = '{3}' or sEmployeeSecondaryId = '{3}' or sEmployeeServicingId = '{3}' ) and ( IsValid = 1 ) and ( IsTemplate = 0 ) and ( sLoanFileT IS NULL OR sLoanFileT = 0)"
                        , joinClause, userInfo.BrokerId, userInfo.BranchId, userInfo.EmployeeId, teamWhere
                        );
                }
                else if (extentScope == E_ReportExtentScopeT.Assign || userInfo.HasPermission(Permission.IndividualLevelAccess) == true && extentScope == E_ReportExtentScopeT.Default)
                {
                    sXlate.Source = string.Format
                        ("{0} where ( sBrokerId = '{1}' ) and ( sEmployeeLenderAccExecId = '{2}' or sEmployeeLockDeskId = '{2}' or sEmployeeUnderwriterId = '{2}' or sEmployeeRealEstateAgentId = '{2}' or sEmployeeCallCenterAgentId = '{2}' or sEmployeeLoanOpenerId = '{2}' or sEmployeeLoanRepId = '{2}' or sEmployeeProcessorId = '{2}' or sEmployeeManagerId = '{2}' or sEmployeeCloserId = '{2}' or sEmployeeShipperId = '{2}' or sEmployeeFunderId = '{2}' or sEmployeePostCloserId = '{2}' or sEmployeeInsuringId = '{2}' or sEmployeeCollateralAgentId = '{2}' or sEmployeeDocDrawerId = '{2}' or sEmployeeCreditAuditorId = '{2}' or sEmployeeDisclosureDeskId = '{2}' or sEmployeeJuniorProcessorId = '{2}' or sEmployeeJuniorUnderwriterId = '{2}' or sEmployeeLegalAuditorId = '{2}' or sEmployeeLoanOfficerAssistantId = '{2}' or sEmployeePurchaserId = '{2}' or sEmployeeQCComplianceId = '{2}' or sEmployeeSecondaryId = '{2}' or sEmployeeServicingId = '{2}') and ( IsValid = 1 ) and ( IsTemplate = 0 ) and ( sLoanFileT IS NULL OR sLoanFileT = 0)"
                        , joinClause, userInfo.BrokerId, userInfo.EmployeeId
                        );
                }
                else if (extentScope == E_ReportExtentScopeT.Team || userInfo.HasPermission(Permission.TeamLevelAccess) == true && extentScope == E_ReportExtentScopeT.Default)
                {
                    var teams = userInfo.GetTeams();
                    string teamList = "";
                    foreach (var team in teams)
                    {

                        teamList += (teamList.Length != 0 ? ", " : " ") + DbAccessUtils.SQLString(team.ToString());
                    }

                    string teamWhere = "";

                    if (teamList.Length != 0)
                    {
                        teamWhere = string.Format(" ( sTeamLenderAccExecId IN {0} or sTeamLockDeskId IN {0} or sTeamUnderwriterId IN {0} or sTeamRealEstateAgentId IN {0} or sTeamCallCenterAgentId IN {0} or sTeamLoanOpenerId IN {0} or sTeamLoanRepId IN {0} or sTeamProcessorId IN {0} or sTeamManagerId IN {0} or sTeamCloserId IN {0} or sTeamShipperId IN {0} or sTeamFunderId IN {0} or sTeamPostCloserId IN {0} or sTeamInsuringId IN {0} or sTeamCollateralAgentId IN {0} or sTeamDocDrawerId IN {0} or sTeamCreditAuditorId IN {0} or sTeamDisclosureDeskId IN {0} or sTeamJuniorProcessorId IN {0} or sTeamJuniorUnderwriterId IN {0} or sTeamLegalAuditorId IN {0} or sTeamLoanOfficerAssistantId IN {0} or sTeamPurchaserId IN {0} or sTeamQCComplianceId IN {0} or sTeamSecondaryId IN {0} or sTeamServicingId IN {0} ) OR ",
                            " ( " + teamList + " ) ");
                    }

                    sXlate.Source = string.Format
                        ("{0} where ( sBrokerId = '{1}' ) and ( {3} ( sEmployeeLenderAccExecId = '{2}' or sEmployeeLockDeskId = '{2}' or sEmployeeUnderwriterId = '{2}' or sEmployeeRealEstateAgentId = '{2}' or sEmployeeCallCenterAgentId = '{2}' or sEmployeeLoanOpenerId = '{2}' or sEmployeeLoanRepId = '{2}' or sEmployeeProcessorId = '{2}' or sEmployeeManagerId = '{2}' or sEmployeeCloserId = '{2}' or sEmployeeShipperId = '{2}' or sEmployeeFunderId = '{2}' or sEmployeePostCloserId = '{2}' or sEmployeeInsuringId = '{2}' or sEmployeeCollateralAgentId = '{2}' or sEmployeeDocDrawerId = '{2}' or sEmployeeCreditAuditorId = '{2}' or sEmployeeDisclosureDeskId = '{2}' or sEmployeeJuniorProcessorId = '{2}' or sEmployeeJuniorUnderwriterId = '{2}' or sEmployeeLegalAuditorId = '{2}' or sEmployeeLoanOfficerAssistantId = '{2}' or sEmployeePurchaserId = '{2}' or sEmployeeQCComplianceId = '{2}' or sEmployeeSecondaryId = '{2}' or sEmployeeServicingId = '{2}')) and ( IsValid = 1 ) and ( IsTemplate = 0 ) and ( sLoanFileT IS NULL OR sLoanFileT = 0)"
                        , joinClause
                        , userInfo.BrokerId
                        , userInfo.EmployeeId,
                        teamWhere
                        );
                }
                else if (extentScope == E_ReportExtentScopeT.SpecificLoans)
                {
                    StringBuilder LoanIds = new StringBuilder("(");
                    if (rQuery.RestrictToLoanIds.Count == 0)
                    {
                        LoanIds.Append("null, ");
                    }
                    else
                    {
                        foreach (var id in rQuery.RestrictToLoanIds)
                        {
                            LoanIds.Append("'" + id.ToString() + "', ");
                        }
                    }
                    LoanIds.Length -= 2;
                    LoanIds.Append(")");

                    sXlate.Source = string.Format("{0} where (sBrokerId = '{1}') and ( IsValid = 1 ) and ( IsTemplate = 0 )  and ( sLoanFileT IS NULL OR sLoanFileT = 0) and LOAN_FILE_CACHE.sLId in {2}",
                                        joinClause, userInfo.BrokerId, LoanIds);
                }
                else
                {
                    // Oops!

                    throw new CBaseException(ErrorMessages.Generic, "Employee lacks permission to report loan files.");
                }
            }
            else if (userInfo.Type == "P")
            {
                if (extentScope == E_ReportExtentScopeT.Assign || (extentScope == E_ReportExtentScopeT.Default && userInfo.PmlLevelAccess == E_PmlLoanLevelAccess.Individual))
                {
                    sXlate.Source = string.Format("{0} where ( sBrokerId = '{1}' ) and ( IsValid = 1 ) and ( IsTemplate = 0 ) and ( sLoanFileT IS NULL OR sLoanFileT = 0 ) and ( sPmlBrokerId = '{2}' ) and ( sEmployeeLoanRepId = '{3}' OR sEmployeeBrokerProcessorId = '{3}' OR sEmployeeExternalSecondaryId = '{3}' OR sEmployeeExternalPostCloserId = '{3}' )", joinClause, userInfo.BrokerId, userInfo.PmlBrokerId, userInfo.EmployeeId);
                }
                else if (extentScope == E_ReportExtentScopeT.Default)
                {
                    if (userInfo.PmlLevelAccess == E_PmlLoanLevelAccess.Corporate)
                    {
                        sXlate.Source = string.Format("{0} where ( sBrokerId = '{1}' ) and ( IsValid = 1 ) and ( IsTemplate = 0 ) and ( sLoanFileT IS NULL OR sLoanFileT = 0 ) and (sPmlBrokerId = '{2}')", joinClause, userInfo.BrokerId, userInfo.PmlBrokerId);
                    }
                    else if (userInfo.PmlLevelAccess == E_PmlLoanLevelAccess.Supervisor)
                    {
                        sXlate.Source = string.Format("{0} where ( sBrokerId = '{1}' ) and ( IsValid = 1 ) and ( IsTemplate = 0 ) and ( sLoanFileT IS NULL OR sLoanFileT = 0 ) and ( ( ( sEmployeeLoanRepId = '{3}' OR sEmployeeBrokerProcessorId = '{3}' OR sEmployeeExternalSecondaryId = '{3}' OR sEmployeeExternalPostCloserId = '{3}' ) AND sPmlBrokerId = '{2}' ) OR PmlExternalManagerEmployeeId = '{3}' OR PmlExternalBrokerProcessorManagerEmployeeId = '{3}' OR PmlExternalSecondaryManagerEmployeeId = '{3}' OR PmlExternalPostCloserManagerEmployeeId = '{3}' )", joinClause, userInfo.BrokerId, userInfo.PmlBrokerId, userInfo.EmployeeId);
                    }
                    else
                    {
                        throw new UnhandledEnumException(userInfo.PmlLevelAccess);
                    }
                }
                else
                {
                    // Oops!
                    throw new CBaseException(ErrorMessages.Generic, "Employee lacks permission to report loan files.");
                }
            }

            // Generate the SQL Query to execute.
            // 5/9/2005 kb - Note to self: when you embed keys and helper
            // values in the sql, make sure you relabel them to something
            // not found within the set of reportable fields, or you will
            // collide with the report's column when it shows up.
            //
            // 7/11/2005 kb - We somehow allow empty reports.  Not to be!
            // Still, if it happens because of bugs, we should allow it to
            // be run, and return the base-indexing fields.
            var queriesHolder = new SqlQuerysHolder();

            StringBuilder sb = new StringBuilder();
            StringBuilder sbForCount = new StringBuilder();


            if (sXlate.Layout != "" && sXlate.Source != "")
            {
                string rowLimit = "";

                if (m_maxSqlRowCount < int.MaxValue)
                {
                    rowLimit = "TOP " + m_maxSqlRowCount;
                }
                sb.AppendFormat("select {2} LOAN_FILE_CACHE.sLId as Id , sStatusT as Status , {0} from {1}", sXlate.Layout, sXlate.Source, rowLimit);
            }
            else if (sXlate.Source != "")
            {
                sb.AppendFormat("select LOAN_FILE_CACHE.sLId as Id , sStatusT as Status from {0}", sXlate.Source);
            }
            else
            {
                throw new ArgumentException("Query translation lacks layout and source.");
            }

            sbForCount.AppendFormat("select count(*) as Count from {0}", sXlate.Source);

            if (sXlate.Clause != String.Empty)
            {
                sb.AppendFormat(" and {0}", sXlate.Clause);
                sbForCount.AppendFormat(" and {0}", sXlate.Clause);
            }

            if (sXlate.Orders != String.Empty)
            {
                sb.AppendFormat(" order by {0}", sXlate.Orders);
                // no append for count.
            }

            queriesHolder.SqlQueryForCountQuery = sbForCount;
            queriesHolder.SqlQueryForTableQuery = sb;
            queriesHolder.Params = sXlate.Params;
            return queriesHolder;
        }

        /// <summary>
        /// Shows the size of the table in terms of rows, columns, or rows by columns (which is useful for memory).
        /// </summary>
        public class TableSize
        {
            public int NumRows;
            public int NumColumns;
            public int RowsByColumns => NumRows * NumColumns;
        }

        /// <summary>
        /// Size is in rows * columns.
        /// </summary>
        /// <param name="rQ"></param>
        /// <param name="uI"></param>
        /// <param name="maskSsn"></param>
        /// <param name="scope"></param>
        /// <param name="isForCountOnly"></param>
        /// <returns></returns>
        public TableSize GetReportSize(Query rQ, AbstractUserPrincipal uI, bool maskSsn, E_ReportExtentScopeT scope, Guid? userId = null)
        {
            SimpleRawResultDataTable resultDataTable;
            var useTableWithEncryptionHandling = !maskSsn && rQ.HasEncryptedFields;
            if (useTableWithEncryptionHandling)
            {
                resultDataTable = new EncryptedDataHandlingResultDataTable(rQ.Label, uI, userId ?? uI.UserId);
            }
            else
            {
                resultDataTable = new SimpleRawResultDataTable(uI);
            }

            var sqlQueriesHolder = this.GetQueryStrings(rQ, uI, scope, maskSsn, userId: null);

            int rowCount = resultDataTable.GetCount(sqlQueriesHolder.SqlQueryForCountQuery.ToString(), sqlQueriesHolder.Params);

            return new TableSize()
            {
                NumRows = rowCount,
                NumColumns = rQ.Columns.Count
            };
        }
        /// <summary>
        /// Should only be used by external functions without access to its own UserPrincipal.
        /// It allows to run reports as other users
        /// From LO just use the other version of RunReport().
        /// Directly access the database and generate the report
        /// given the specified reportId and EmployeeId.
        /// </summary>
        /// <param name="reportId">
        /// Query id from the query to process and run.
        /// </param>
        /// <param name="userId">
        /// User id necessary to create the AbstractUserPrincipal object.
        /// </param>
        /// <returns>
        /// Report of tables of rows.
        /// </returns>
        public Report RunReport(Guid brokerId, Guid reportId, Guid userId, E_ReportExtentScopeT scope, bool enforceReportSizeCap = false)
        {
            if (userId == Guid.Empty)
            {
                throw new ArgumentException("Invalid userId.");
            }

            AbstractUserPrincipal uI = PrincipalFactory.RetrievePrincipalForUser(brokerId, userId, "?");
            return RunReport(brokerId, reportId, uI, scope, enforceReportSizeCap:enforceReportSizeCap);
        }

        public Report RunReport(Guid brokerId, Guid reportId, AbstractUserPrincipal principal, E_ReportExtentScopeT scope, bool enforceReportSizeCap = false)
        {
            if (principal == null)
            {
                throw new ArgumentException("User not found.");
            }

            Query rQ = null;
            if (reportId == Guid.Empty)
            {
                throw new ArgumentException("Invalid reportId.");
            }

            foreach (DbConnectionInfo connInfo in DbConnectionInfo.ListAll())
            {
                SqlParameter[] parameters = {
                                            new SqlParameter("@QueryId", reportId)
                                        };
                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(connInfo, "GetReportQueryContentById", parameters))
                {
                    if (reader.Read())
                    {
                        rQ = reader["XmlContent"].ToString();
                        break;
                    }
                }
            }

            if (rQ == null)
            {
                throw new ArgumentException("Report not found.", "'" + reportId + "'");
            }

            return RunReport(rQ, principal, maskSsn: !principal.HasPermission(Permission.AllowExportingFullSsnViaCustomReports), scope: scope, enforceReportSizeCap: enforceReportSizeCap);
        }

        /// <summary>
        /// Directly access the database and generate the report
        /// given the specified query.
        /// </summary>
        /// <param name="rQ">
        /// Query to process and run.
        /// </param>
        /// <param name="uBrokerId">
        /// Security filter -- needs more mature implementation.
        /// </param>
        /// <returns>
        /// Report of tables of rows.
        /// </returns>

        public Report RunReport( Query rQ , AbstractUserPrincipal uI, bool maskSsn, E_ReportExtentScopeT scope, bool enforceReportSizeCap = false)
        {
            // Run the report and return the rows.

            Guid reportId   = Guid.Empty;
            Guid employeeId = Guid.Empty;

            try
            {
                // Execute the report using our default processor and load
                // the security credentials so we can get the extent.

                Processor rP = new Processor();
                Report rE = new Report();

                reportId = rQ.Id;
                employeeId = uI.EmployeeId;

                //opm 43325 fs 03/10/10 Check user credentials based on per-field security model
                BrokerUserPrincipal bP = uI as BrokerUserPrincipal;
                if (bP == null || (rQ.hasSecureFields && !rQ.CanUserRun(bP)))
                {
                    string userMsg = "User does not have the permissions to run report: '" + rQ.Label.Title + "' which contains restricted fields.";
                    string devMsg = "User '" + uI.LoginNm + "' (" + uI.UserId + ") from broker '" + uI.BrokerId + "' cannot run report '" + rQ.Label.Title + "' (" + rQ.Id + ") which contains restricted fields.";
                    throw new PermissionException(userMsg, devMsg);
                }


                if (rQ.Options.Has("UseUnitCols") == true)
                {
                    // 2/2/2005 kb - We inject a unit column with the count
                    // function if the query has the option turned on.  This
                    // needs to be moved out of the option pool and become
                    // part of the designer's ui.

                    Column uCol = rQ.Columns.Find("sLUnit");

                    if (uCol == null)
                    {
                        Query rU = new Query(rQ);

                        rU.Columns.Add(uCol = new Column(m_Schema.LookupById("sLUnit"), true));

                        rQ = rU;
                    }

                    uCol.Functions.Add(E_FunctionType.Ct);
                }

                var reportData = this.GetExtent(rQ, uI, scope, maskSsn, userId: null, enforceMaxTableSize: enforceReportSizeCap);
                rE = rP.CrossIt(rQ, m_Catch, m_Schema, reportData, m_maxViewableRowCount);

                if (rE == null)
                {
                    throw new CBaseException(ErrorMessages.Generic, "Finished report processing and returned null!");
                }
                else
                {
                    rE.Label.Title = rQ.Label.Title;
                }

                return rE;
            }
            catch (Exception e)
            {
                // Oops!

                Tools.LogError(String.Format
                    ("Loan reporting: Failed to run report {0} for {1}."
                    , reportId
                    , employeeId
                    ), e
                    );

                if (e is PermissionException || e is CBaseException)
                {
                    throw;
                }
            }

            return null;
        }


        /// <summary>
        /// Get the equivalent of a pipeline report of the employee's
        /// loans.  We limit the lookup to just what is assigned.
        /// </summary>
        /// <returns>
        /// Report of tables of rows.
        /// </returns>

        public Report Pipeline( Guid reportId , AbstractUserPrincipal uI , string altSorting, E_ReportExtentScopeT scope )
        {
            // Access the cache of loan files and report on what is assigned
            // according to the specified query.
            try
            {
                // Load the query from the database.  We don't care about
                // broker boundaries.

                if (uI == null)
                {
                    throw new ArgumentNullException(nameof(uI));
                }
                
                Query rQ = new Query();

                if (reportId == Guid.Empty)
                {
                    E_RoleT[] roleTPriority = {
                                                  E_RoleT.Manager,
                                                  E_RoleT.Underwriter,
                                                  E_RoleT.JuniorUnderwriter,
                                                  E_RoleT.QCCompliance,
                                                  E_RoleT.LockDesk,
                                                  E_RoleT.DisclosureDesk,
                                                  E_RoleT.Funder,
                                                  E_RoleT.PostCloser,
                                                  E_RoleT.Shipper,
                                                  E_RoleT.Closer,
                                                  E_RoleT.DocDrawer,
                                                  E_RoleT.Insuring,
                                                  E_RoleT.Processor,
                                                  E_RoleT.JuniorProcessor,
                                                  E_RoleT.LoanOfficer,
                                                  E_RoleT.LoanOfficerAssistant,
                                                  E_RoleT.LoanOpener,
                                                  E_RoleT.RealEstateAgent,
                                                  E_RoleT.CallCenterAgent,
                                                  E_RoleT.CollateralAgent,
                                                  E_RoleT.LenderAccountExecutive,
                                                  E_RoleT.CreditAuditor,
                                                  E_RoleT.LegalAuditor,
                                                  E_RoleT.Purchaser,
                                                  E_RoleT.Secondary,
                                                  E_RoleT.Accountant,
                                                  E_RoleT.Servicing,
                                                  E_RoleT.Administrator
                                              };

                    foreach (E_RoleT roleT in roleTPriority)
                    {

                        if (BrokerUser.HasRole(roleT))
                        {
                            reportId = Role.Get(roleT).DefaultPipelineReportId;
                            break;
                        }
                    }
                }
                SqlParameter[] parameters = {
                                               new SqlParameter("@QueryId", reportId)
                                           };
                Guid brokerId = uI.BrokerId;

                if (reportId == LoanReporting.DefaultSimplePipelineReportId ||
                    reportId == LoanReporting.DefaultSimplePipelineNonPMLReportId)
                {
                    // 9/26/2014 dd - These two default reports in AdminBrokerId
                    brokerId = LendersOffice.Admin.BrokerDB.AdminBr;
                }
                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "GetReportQueryContentById", parameters))
                {
                    if (reader.Read())
                    {
                        rQ = reader["XmlContent"].ToString();
                    }
                    else
                    {
                        throw new ArgumentException("Report not found.", "'" + reportId + "'");
                    }
                }

                return Pipeline(rQ, uI, altSorting, scope);
            }
            catch ( ArgumentException )
            {
                // Pass-thru!

                throw;
            }
            catch( Exception e )
            {
                // Oops!
                Tools.LogError( string.Format( "Loan reporting: Failed to run report {0} with '{1}'.", reportId, altSorting) , e );
                if (e is PermissionException)
                {
                    throw;
                }
            }

            return null;
        }

        public Report Pipeline(Query query, AbstractUserPrincipal principal, string altSorting, E_ReportExtentScopeT scope)
        {
            if (altSorting != null && altSorting.TrimWhitespaceAndBOM() != "")
            {
                // Get the passed in sorting specification and redo the
                // query's sorting before we run it.

                String[] sortSpec = altSorting.Split(':');

                query.Sorting.Clear();

                if (sortSpec.Length > 1 && sortSpec[1].ToLower().TrimWhitespaceAndBOM() == "desc")
                {
                    query.Sorting.Add(sortSpec[0].TrimWhitespaceAndBOM(), 1);
                }
                else
                {
                    query.Sorting.Add(sortSpec[0].TrimWhitespaceAndBOM());
                }
            }

            if (query.hasSecureFields && !query.CanUserRun(principal))
            {
                string userMsg = "User does not have the permissions to run report: '" + query.Label.Title + "' which contains restricted fields.";
                string devMsg = "User '" + principal.LoginNm + "' (" + principal.UserId + ") from broker '" + principal.BrokerId + "' cannot run report '" + query.Label.Title + "' (" + query.Id + ") which contains restricted fields.";
                throw new PermissionException(userMsg, devMsg);
            }

            // Run the query and carry the result to the caller.

            Processor rP = new Processor();

            var rawDataTable = GetExtent(query, principal, scope, maskSsn: true);
            var rE = rP.Process(query, m_Catch, m_Schema, rawDataTable, m_maxViewableRowCount);

            if (rE != null)
            {
                rE.Label = query.Label;
            }
            else
            {
                throw new CBaseException(ErrorMessages.Generic, "Finished report processing and returned null!");
            }

            return rE;
        }

        /// <summary>
        /// Generate the disclosure pipeline report. Should contain only loans
        /// that the user has disclosure permissions for and read access to.
        /// </summary>
        /// <param name="principal">The principal to check for disclosure permissions.</param>
        /// <param name="dueTodayOnly">If true, include only loans that are due on or before today.</param>
        /// <returns></returns>
        public Report DisclosurePipeline(AbstractUserPrincipal principal, bool dueTodayOnly)
        {
            Guid reportId = ConstApp.BaseDisclosurePipelineReportId;
            var query = new Query();

            SqlParameter[] parameters = {
                                            new SqlParameter("@QueryId", reportId)
                                        };

            // 4/3/2015 dd - The base disclosure pipeline report id will be locate in shared db.
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShareROnly, "GetReportQueryContentById", parameters))
            {
                if (reader.Read())
                {
                    query = reader["XmlContent"].ToString();
                }
                else
                {
                    throw new ArgumentException("Disclosure Pipeline report not found.", "'" + reportId + "'");
                }
            }

            var existingConditions = query.Relates;

            // We are going to need to add conditions to the report based on the user's permissions.
            bool initialRetail = principal.HasPermission(Permission.ResponsibleForInitialDiscRetail);
            bool initialWholesale = principal.HasPermission(Permission.ResponsibleForInitialDiscWholesale);
            bool redisc = principal.HasPermission(Permission.ResponsibleForRedisclosures);

            // This will be the outer set of OR conditions for the user permissions.
            var permissionConditions = new Conditions(E_ConditionsType.Or);

            if (initialRetail && initialWholesale)
            {
                permissionConditions.Add(new Condition(E_ConditionType.Eq, "sNeedInitialDisc", 1));
            }
            else if (initialRetail)
            {
                // If they have access to retail initial disclosures,
                // then need to add a new condition set that ANDs 
                // sNeedInitialDisc and sBranchChannelT == Retail
                var initialRetailConditions = new Conditions(E_ConditionsType.An);
                initialRetailConditions.Add(new Condition(E_ConditionType.Eq, "sNeedInitialDisc", 1));
                initialRetailConditions.Add(new Condition(E_ConditionType.Eq, "sBranchChannelT", E_BranchChannelT.Retail));
                permissionConditions.Add(initialRetailConditions);
            }
            else if (initialWholesale)
            {
                // If they have access to wholesale initial disclosures,
                // then need to add a new condition set that ANDs 
                // sNeedInitialDisc and sBranchChannelT != Retail
                var initialWholesaleConditions = new Conditions(E_ConditionsType.An);
                initialWholesaleConditions.Add(new Condition(E_ConditionType.Eq, "sNeedInitialDisc", 1));
                initialWholesaleConditions.Add(new Condition(E_ConditionType.Ne, "sBranchChannelT", E_BranchChannelT.Retail));
                permissionConditions.Add(initialWholesaleConditions);
            }

            if (redisc)
            {
                permissionConditions.Add(new Condition(E_ConditionType.Eq, "sNeedRedisc", 1));
            }

            // Also need to add constraint on disclosures due date if we only want loans
            // that are due.
            Conditions generatedConditions = null;
            if (dueTodayOnly)
            {
                // We are going to have to AND the user permissions conditions with
                // the constraint on sDisclosuresDueD.
                var outerAndConditions = new Conditions(E_ConditionsType.An);
                outerAndConditions.Add(permissionConditions);
                outerAndConditions.Add(new Condition(E_ConditionType.Lt, "sDisclosuresDueD", DateTime.Today.AddDays(1)));

                generatedConditions = outerAndConditions;
            }
            else
            {
                generatedConditions = permissionConditions;
            }

            // 5/14/2014 gf - opm 181298, instead of enforcing that all conditions 
            // are added through the code, support ANDing conditions added from 
            // report editor with the permissions.
            var finalConditions = new Conditions(E_ConditionsType.An);
            finalConditions.Add(existingConditions);
            finalConditions.Add(generatedConditions);
            query.Relates = finalConditions;

            var brokerDB = BrokerDB.RetrieveById(principal.BrokerId);

            if (brokerDB.IsEnableDocuTechAutoDisclosure)
            {
                query.Columns.Add(new Column(m_Schema.LookupById("sAutoDisclosureStatusT"), true));
            }

            if (brokerDB.UseCustomCocFieldList)
            {
                query.Columns.Add(new Column(m_Schema.LookupById("sDisclosureNeededTDescription"), true));
            }

            return RunReport(query, principal, true, E_ReportExtentScopeT.Default);
        }

        /// <summary>
        /// Show a published report.  We always default to corporate-level
        /// scope.  Down the road, we can just pass in the extent scope
        /// level that the caller wants.
        /// </summary>
        /// <returns>
        /// Report of tables of rows.
        /// </returns>

        public Report Publish(Guid reportId, Guid brokerId, AbstractUserPrincipal principalRunningReport)
        {
            // Access the cache of loan files and report on what is assigned
            // according to the specified query.

            Guid branchId = Guid.Empty;
            Guid authorId = Guid.Empty;
            Guid accessId = Guid.Empty;

            try
            {
                // Load the query from the database.  We don't care about
                // broker boundaries.

                Report rE = new Report();
                Query rQ = new Query();

                SqlParameter[] parameters = {
                                                new SqlParameter("@BrokerId", brokerId), 
                                                new SqlParameter("@QueryId", reportId)
                                            };

                using (IDataReader sR = StoredProcedureHelper.ExecuteReader(brokerId, "GetReportQuery", parameters))
                {
                    if (sR.Read() == true)
                    {
                        String namedAs = (String)sR["NamePublishedAs"];

                        authorId = (Guid)sR["EmployeeId"];

                        rQ = sR["XmlContent"].ToString();

                        rQ.Label.Title = namedAs;
                    }
                    else
                    {
                        // Missing!

                        throw new ArgumentException("Report not found.", "ReportId");
                    }
                }

                if (authorId != Guid.Empty)
                {
                    SqlParameter[] employeeParameters = {
                                                            new SqlParameter("@EmployeeId", authorId)
                                                        };
                    branchId = (Guid)StoredProcedureHelper.ExecuteScalar(brokerId, "GetBranchByEmployeeId", employeeParameters);
                }

                if (authorId != Guid.Empty)
                {
                    SqlParameter[] employeeParameters = {
                                                            new SqlParameter("@EmployeeId", authorId)
                                                        };
                    accessId = (Guid)StoredProcedureHelper.ExecuteScalar(brokerId, "GetUserByEmployeeId", employeeParameters);
                }

                BrokerUserPrincipal bP = BrokerUserPrincipal.CurrentPrincipal;
                if (bP == null || (rQ.hasSecureFields && !rQ.CanUserRun(bP)))
                {
                    string loginNm = bP != null ? bP.LoginNm : "<empty>";
                    string userId = bP != null ? bP.UserId.ToString() : "<empty>";

                    string userMsg = "User does not have the permissions to run report: '" + rQ.Label.Title + "' which contains restricted fields.";
                    string devMsg = "User '" + loginNm + "' (" + userId + ") from broker '" + brokerId + "' cannot run report '" + rQ.Label.Title + "' (" + rQ.Id + ") which contains restricted fields.";
                    throw new PermissionException(userMsg, devMsg);
                }

                // Run the query and carry the result to the caller.
                var maskSsn = !principalRunningReport.HasPermission(Permission.AllowExportingFullSsnViaCustomReports);

                var authorPrincipal = PrincipalFactory.Create(brokerId, accessId, "B", true /* allowDuplicateLogin */, false /* isStoreToCookie */);
                var reportData = this.GetExtent(rQ, authorPrincipal, E_ReportExtentScopeT.Default, maskSsn, principalRunningReport.UserId);

                Processor rP = new Processor();
                rE = rP.CrossIt(rQ, m_Catch, m_Schema, reportData, m_maxViewableRowCount);

                if (rE != null)
                {
                    rE.Label = rQ.Label;
                }
                else
                {
                    throw new CBaseException(ErrorMessages.Generic, "Finished report processing and returned null!");
                }

                return rE;
            }
            catch (ArgumentException)
            {
                // Pass-thru!

                throw;
            }
            catch (PermissionException e)
            {
                Tools.LogError(e);
                throw;
            }
            catch (Exception e)
            {
                // Oops!

                Tools.LogError(String.Format
                    ("Loan reporting: Failed to run published report {0} as {1} for {2}."
                    , reportId
                    , authorId
                    , brokerId
                    ), e);
            }

            return null;
        }

    }

	#region ( Macro Implementations )

    /// <summary>
    /// Reserved macro field for loan reporting schema.
    /// </summary>

    internal class ThisMonday : Field
    {
		#region ( Macro Details )

        public override string Selection
        {
            // Generate member.

            get
            {
                DateTime dt = DateTime.Now;

                while( dt.DayOfWeek != DayOfWeek.Monday )
                {
                    dt = dt.AddDays( -1 );
                }

                return "cast( '" + dt.ToShortDateString() + "' as datetime )";
            }
        }

        internal ThisMonday()
            : base
            ( "Calendar Dates"
            , "This Week's Monday"
            , "cThisMonday"
            , Field.E_FieldType.Dtm , Field.E_ClassType.Cal
            , "M/d/yyyy"
            )
        {
        }

		#endregion

    }

    /// <summary>
    /// Reserved macro field for loan reporting schema.
    /// </summary>

    internal class Today : Field
    {
		#region ( Macro Details )

        public override string Selection
        {
            // Generate member.

            get
            {
                DateTime dt = DateTime.Now;

                return "cast( '" + dt.ToShortDateString() + "' as datetime )";
            }
        }

        internal Today()
            : base
            ( "Calendar Dates"
            , "Today"
            , "cToday"
            , Field.E_FieldType.Dtm , Field.E_ClassType.Cal
            , "M/d/yyyy"
            )
        {
        }

		#endregion

    }

    /// <summary>
    /// Reserved macro field for loan reporting schema.
    /// </summary>

    internal class Yesterday : Field
    {
        #region ( Macro Details )

        public override string Selection
        {
            // Generate member.

            get
            {
                DateTime dt = DateTime.Now;
                dt = dt.AddDays(-1);

                return "cast( '" + dt.ToShortDateString() + "' as datetime )";
            }
        }

        internal Yesterday()
            : base
            ("Calendar Dates"
            , "Yesterday"
            , "cYesterday"
            , Field.E_FieldType.Dtm, Field.E_ClassType.Cal
            , "M/d/yyyy"
            )
        {
        }

        #endregion

    }


    internal class Tomorrow : Field
    {
        #region ( Macro Details )

        public override string Selection
        {
            // Generate member.

            get
            {
                DateTime dt = DateTime.Now;
                dt = dt.AddDays(1);

                return "cast( '" + dt.ToShortDateString() + "' as datetime )";
            }
        }

        internal Tomorrow()
            : base
            ("Calendar Dates"
            , "Tomorrow"
            , "cTomorrow"
            , Field.E_FieldType.Dtm, Field.E_ClassType.Cal
            , "M/d/yyyy"
            )
        {
        }

        #endregion

    }

    /// <summary>
    /// Reserved macro field for loan reporting schema.
    /// <summary>

    internal class ThisFriday : Field
    {
		#region ( Macro Details )

        public override string Selection
        {
            // Generate member.

            get
            {
                DateTime dt = DateTime.Now;

                while( dt.DayOfWeek != DayOfWeek.Friday )
                {
                    dt = dt.AddDays( +1 );
                }

                return "cast( '" + dt.ToShortDateString() + "' as datetime )";
            }
        }

        internal ThisFriday()
            : base
            ( "Calendar Dates"
            , "This Week's Friday"
            , "cThisFriday"
            , Field.E_FieldType.Dtm , Field.E_ClassType.Cal
            , "M/d/yyyy"
            )
        {
        }

		#endregion

    }

    /// <summary>
    /// Reserved macro field for loan reporting schema.
    /// <summary>

    internal class LastMonday : Field
    {
		#region ( Macro Details )

        public override string Selection
        {
            // Generate member.

            get
            {
                DateTime dt = DateTime.Today;

                dt = dt.AddDays( -7 );

                while( dt.DayOfWeek != DayOfWeek.Monday )
                {
                    dt = dt.AddDays( -1 );
                }

                return "cast( '" + dt.ToShortDateString() + "' as datetime )";
            }
        }

        internal LastMonday()
            : base
            ( "Calendar Dates"
            , "Last Week's Monday"
            , "cLastMonday"
            , Field.E_FieldType.Dtm , Field.E_ClassType.Cal
            , "M/d/yyyy"
            )
        {
        }

		#endregion

    }

    /// <summary>
    /// Reserved macro field for loan reporting schema.
    /// <summary>

    internal class LastFriday : Field
    {
		#region ( Macro Details )

        public override string Selection
        {
            // Generate member.

            get
            {
                DateTime dt = DateTime.Today;

                dt = dt.AddDays( -1 );

                while( dt.DayOfWeek != DayOfWeek.Friday )
                {
                    dt = dt.AddDays( -1 );
                }

                return "cast( '" + dt.ToShortDateString() + "' as datetime )";
            }
        }

        internal LastFriday()
            : base
            ( "Calendar Dates"
            , "Last Week's Friday"
            , "cLastFriday"
            , Field.E_FieldType.Dtm , Field.E_ClassType.Cal
            , "M/d/yyyy"
            )
        {
        }

		#endregion

    }

    /// <summary>
    /// Reserved macro field for loan reporting schema.
    /// <summary>

    internal class NextMonday : Field
    {
		#region ( Macro Details )

        public override string Selection
        {
            // Generate member.

            get
            {
                DateTime dt = DateTime.Now;

                dt = dt.AddDays( +1 );

                while( dt.DayOfWeek != DayOfWeek.Monday )
                {
                    dt = dt.AddDays( +1 );
                }

                return "cast( '" + dt.ToShortDateString() + "' as datetime )";
            }
        }

        internal NextMonday()
            : base
            ( "Calendar Dates"
            , "Next Week's Monday"
            , "cNextMonday"
            , Field.E_FieldType.Dtm , Field.E_ClassType.Cal
            , "M/d/yyyy"
            )
        {
        }

		#endregion

    }

    /// <summary>
    /// Reserved macro field for loan reporting schema.
    /// <summary>

    internal class NextFriday : Field
    {
		#region ( Macro Details )

        public override string Selection
        {
            // Generate member.

            get
            {
                DateTime dt = DateTime.Now;

                dt = dt.AddDays( +7 );

                while( dt.DayOfWeek != DayOfWeek.Friday )
                {
                    dt = dt.AddDays( +1 );
                }

                return "cast( '" + dt.ToShortDateString() + "' as datetime )";
            }
        }

        internal NextFriday()
            : base
            ( "Calendar Dates"
            , "Next Week's Friday"
            , "cNextFriday"
            , Field.E_FieldType.Dtm , Field.E_ClassType.Cal
            , "M/d/yyyy"
            )
        {
        }

		#endregion

    }

    /// <summary>
    /// Reserved macro field for loan reporting schema.
    /// <summary>

    internal class ThisMonthBeginning : Field
    {
		#region ( Macro Details )

        public override string Selection
        {
            // Generate member.

            get
            {
                DateTime dt = DateTime.Today;

                while( dt.Day != 1 )
                {
                    dt = dt.AddDays( -1 );
                }

                return "cast( '" + dt.ToShortDateString() + "' as datetime )";
            }
        }

        internal ThisMonthBeginning()
            : base
            ( "Calendar Dates"
            , "Beginning of This Month"
            , "cThisMonthBeginning"
            , Field.E_FieldType.Dtm , Field.E_ClassType.Cal
            , "M/d/yyyy"
            )
        {
        }

		#endregion

    }

    /// <summary>
    /// Reserved macro field for loan reporting schema.
    /// <summary>

    internal class ThisMonthEnd : Field
    {
		#region ( Macro Details )

        public override string Selection
        {
            // Generate member.

            get
            {
                DateTime dt = DateTime.Today;

                dt = dt.AddMonths( +1 );

                while( dt.Day != 1 )
                {
                    dt = dt.AddDays( -1 );
                }

                dt = dt.AddDays( -1 );

                return "cast( '" + dt.ToShortDateString() + "' as datetime )";
            }
        }

        internal ThisMonthEnd()
            : base
            ( "Calendar Dates"
            , "End of This Month"
            , "cThisMonthEnd"
            , Field.E_FieldType.Dtm , Field.E_ClassType.Cal
            , "M/d/yyyy"
            )
        {
        }

		#endregion

    }

    /// <summary>
    /// Reserved macro field for loan reporting schema.
    /// <summary>

    internal class LastMonthBeginning : Field
    {
		#region ( Macro Details )

        public override string Selection
        {
            // Generate member.

            get
            {
                DateTime dt = DateTime.Today;

                dt = dt.AddMonths( -1 );

                while( dt.Day != 1 )
                {
                    dt = dt.AddDays( -1 );
                }

                return "cast( '" + dt.ToShortDateString() + "' as datetime )";
            }
        }

        internal LastMonthBeginning()
            : base
            ( "Calendar Dates"
            , "Beginning of Last Month"
            , "cLastMonthBeginning"
            , Field.E_FieldType.Dtm , Field.E_ClassType.Cal
            , "M/d/yyyy"
            )
        {
        }

		#endregion

    }

    /// <summary>
    /// Reserved macro field for loan reporting schema.
    /// <summary>

    internal class LastMonthEnd : Field
    {
		#region ( Macro Details )

        public override string Selection
        {
            // Generate member.

            get
            {
                DateTime dt = DateTime.Today;

                while( dt.Day != 1 )
                {
                    dt = dt.AddDays( -1 );
                }

                dt = dt.AddDays( -1 );

                return "cast( '" + dt.ToShortDateString() + "' as datetime )";
            }
        }

        internal LastMonthEnd()
            : base
            ( "Calendar Dates"
            , "End of Last Month"
            , "cLastMonthEnd"
            , Field.E_FieldType.Dtm , Field.E_ClassType.Cal
            , "M/d/yyyy"
            )
        {
        }

		#endregion

    }

    /// <summary>
    /// Reserved macro field for loan reporting schema.
    /// <summary>

    internal class NextMonthBeginning : Field
    {
		#region ( Macro Details )

        public override string Selection
        {
            // Generate member.

            get
            {
                DateTime dt = DateTime.Today;

                dt = dt.AddMonths( +1 );

                while( dt.Day != 1 )
                {
                    dt = dt.AddDays( -1 );
                }

                return "cast( '" + dt.ToShortDateString() + "' as datetime )";
            }
        }

        internal NextMonthBeginning()
            : base
            ( "Calendar Dates"
            , "Beginning of Next Month"
            , "cNextMonthBeginning"
            , Field.E_FieldType.Dtm , Field.E_ClassType.Cal
            , "M/d/yyyy"
            )
        {
        }

		#endregion

    }

    /// <summary>
    /// Reserved macro field for loan reporting schema.
    /// <summary>

    internal class NextMonthEnd : Field
    {
		#region ( Macro Details )

        public override string Selection
        {
            // Generate member.

            get
            {
                DateTime dt = DateTime.Today;

                dt = dt.AddMonths( +2 );

                while( dt.Day != 1 )
                {
                    dt = dt.AddDays( -1 );
                }

                dt = dt.AddDays( -1 );

                return "cast( '" + dt.ToShortDateString() + "' as datetime )";
            }
        }

        internal NextMonthEnd()
            : base
            ( "Calendar Dates"
            , "End of Next Month"
            , "cNextMonthEnd"
            , Field.E_FieldType.Dtm , Field.E_ClassType.Cal
            , "M/d/yyyy"
            )
        {
        }

		#endregion

    }

    internal class CurrentQuarterBeginning : Field
    {
        #region ( Macro Details )

        public override string Selection
        {
            // Generate member.

            get
            {
                
                DateTime dt = DateTime.Today;

                int quarter_begin_month = 3 * ((dt.Month - 1) / 3) + 1;

                DateTime dt_out = new DateTime(dt.Year, quarter_begin_month, 1);

                return "cast( '" + dt_out.ToShortDateString() + "' as datetime )";
            }
        }

        internal CurrentQuarterBeginning()
            : base
            ("Calendar Dates"
            , "Start of Current Cal. Quarter"
            , "cCurrentQuarterBeginning"
            , Field.E_FieldType.Dtm, Field.E_ClassType.Cal
            , "M/d/yyyy"
            )
        {
        }

        #endregion

    }

    internal class CurrentQuarterEnd : Field
    {
        #region ( Macro Details )

        public override string Selection
        {
            // Generate member.

            get
            {
                DateTime dt = DateTime.Today;

                int quarter_begin_month = 3 * ((dt.Month - 1) / 3) + 1;

                DateTime dt_out = new DateTime(dt.Year, quarter_begin_month, 1).AddMonths(3).AddDays(-1); //A day before next quarter beginning 

                return "cast( '" + dt_out.ToShortDateString() + "' as datetime )";
            }
        }

        internal CurrentQuarterEnd()
            : base
            ("Calendar Dates"
            , "End of Current Cal. Quarter"
            , "cCurrentQuarterEnd"
            , Field.E_FieldType.Dtm, Field.E_ClassType.Cal
            , "M/d/yyyy"
            )
        {
        }

        #endregion

    }


    internal class LastQuarterBeginning : Field
    {
        #region ( Macro Details )

        public override string Selection
        {
            // Generate member.
            get
            {                

                DateTime dt = DateTime.Today;                
                int quarter_begin_month = 3 * ((dt.Month - 1) / 3) + 1;
                DateTime dt_out = new DateTime(dt.Year, quarter_begin_month, 1).AddMonths(-3);

                return "cast( '" + dt_out.ToShortDateString() + "' as datetime )";
            }
        }

        internal LastQuarterBeginning()
            : base
            ("Calendar Dates"
            , "Start of Last Cal. Quarter"
            , "cLastQuarterBeginning"
            , Field.E_FieldType.Dtm, Field.E_ClassType.Cal
            , "M/d/yyyy"
            )
        {
        }

        #endregion

    }

    internal class LastQuarterEnd : Field
    {
        #region ( Macro Details )

        public override string Selection
        {
            // Generate member.
            get
            {

                DateTime dt = DateTime.Today;

                int quarter_begin_month = 3 * ((dt.Month - 1) / 3) + 1;
                DateTime dt_out = new DateTime(dt.Year, quarter_begin_month, 1).AddDays(-1); // A day before the beginning of the current quarter

                return "cast( '" + dt_out.ToShortDateString() + "' as datetime )";
            }
        }

        internal LastQuarterEnd()
            : base
            ("Calendar Dates"
            , "End of Last Cal. Quarter"
            , "cLastQuarterEnd"
            , Field.E_FieldType.Dtm, Field.E_ClassType.Cal
            , "M/d/yyyy"
            )
        {
        }

        #endregion
    }


    /// <summary>
    /// Reserved macro field for loan reporting schema.
    /// <summary>

    internal class LastYearBeginning : Field
    {
		#region ( Macro Details )

        public override string Selection
        {
            // Generate member.

            get
            {
                DateTime dt = DateTime.Today;

                while( dt.Month != 1 )
                {
                    dt = dt.AddMonths( -1 );
                }

                while( dt.Day != 1 )
                {
                    dt = dt.AddDays( -1 );
                }

                dt = dt.AddYears( -1 );

                return "cast( '" + dt.ToShortDateString() + "' as datetime )";
            }
        }

        internal LastYearBeginning()
            : base
            ( "Calendar Dates"
            , "Beginning of Last Year"
            , "cLastYearBeginning"
            , Field.E_FieldType.Dtm , Field.E_ClassType.Cal
            , "M/d/yyyy"
            )
        {
        }

		#endregion

    }

    /// <summary>
    /// Reserved macro field for loan reporting schema.
    /// <summary>

    internal class LastYearEnd : Field
    {
		#region ( Macro Details )

        public override string Selection
        {
            // Generate member.

            get
            {
                DateTime dt = DateTime.Today;

                while( dt.Month != 1 )
                {
                    dt = dt.AddMonths( -1 );
                }

                while( dt.Day != 1 )
                {
                    dt = dt.AddDays( -1 );
                }

                dt = dt.AddDays( -1 );

                return "cast( '" + dt.ToShortDateString() + "' as datetime )";
            }
        }

        internal LastYearEnd()
            : base
            ( "Calendar Dates"
            , "End of Last Year"
            , "cLastYearEnd"
            , Field.E_FieldType.Dtm , Field.E_ClassType.Cal
            , "M/d/yyyy"
            )
        {
        }

		#endregion

    }

    /// <summary>
    /// Reserved macro field for loan reporting schema.
    /// <summary>

    internal class ThisYearBeginning : Field
    {
		#region ( Macro Details )

        public override string Selection
        {
            // Generate member.

            get
            {
                DateTime dt = DateTime.Today;

                while( dt.Month != 1 )
                {
                    dt = dt.AddMonths( -1 );
                }

                while( dt.Day != 1 )
                {
                    dt = dt.AddDays( -1 );
                }

                return "cast( '" + dt.ToShortDateString() + "' as datetime )";
            }
        }

        internal ThisYearBeginning()
            : base
            ( "Calendar Dates"
            , "Beginning of This Year"
            , "cThisYearBeginning"
            , Field.E_FieldType.Dtm , Field.E_ClassType.Cal
            , "M/d/yyyy"
            )
        {
        }

		#endregion

    }

    /// <summary>
    /// Reserved macro field for loan reporting schema.
    /// <summary>

    internal class ThisYearEnd : Field
    {
		#region ( Macro Details )

        public override string Selection
        {
            // Generate member.

            get
            {
                DateTime dt = DateTime.Today;

                dt = dt.AddMonths( +1 );

                while( dt.Month != 1 )
                {
                    dt = dt.AddMonths( +1 );
                }

                while( dt.Day != 1 )
                {
                    dt = dt.AddDays( -1 );
                }

                dt = dt.AddDays( -1 );

                return "cast( '" + dt.ToShortDateString() + "' as datetime )";
            }
        }

        internal ThisYearEnd()
            : base
            ( "Calendar Dates"
            , "End of This Year"
            , "cThisYearEnd"
            , Field.E_FieldType.Dtm , Field.E_ClassType.Cal
            , "M/d/yyyy"
            )
        {
        }

		#endregion

    }

    internal class LastCalendarDays : Field
    {
        private int m_numberOfDays = 0;

        public override string Selection
        {
            get
            {
                DateTime dt = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                dt = dt.AddDays(-1 * m_numberOfDays);
                return "cast('" + dt.ToShortDateString() + "' as datetime)";
            }
        }

        internal LastCalendarDays(int numberOfDays)
            : base("Calendar Dates", "Last " + numberOfDays + " calendar days", "cLastCalendar" + numberOfDays + "days",
            Field.E_FieldType.Dtm, E_ClassType.Cal, "M/d/yyyy")
        {
            if (m_numberOfDays < 0)
            {
                throw new ArgumentException("Number of days cannot be negative");
            }
            m_numberOfDays = numberOfDays;
        }
    }

    internal class LastBusinessDays : Field
    {
        private int m_numberOfDays = 0;

        public override string Selection
        {
            get
            {
                return GetSelectionByBrokerId(Guid.Empty);
            }
        }

        public string GetSelectionByBrokerId(Guid brokerId)
        {
            // 11/17/2010 dd - Return the date of the last business days. 
            // Business day is consider to be Mon - Friday and is not in the Banking Holiday
            // and is not in lender closure list.
            DateTime dt = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            
            dt = BankingHoliday.GetDateByPreviousBusinessDays(dt, m_numberOfDays, false, brokerId);
            return "cast('" + dt.ToShortDateString() + "' as datetime)";

        }
        internal LastBusinessDays(int numberOfDays)
            : base("Calendar Dates", "Last " + numberOfDays + " business days", "cLastBusiness" + numberOfDays + "Days",
            Field.E_FieldType.Dtm, E_ClassType.Cal, "M/d/yyyy")
        {
            if (m_numberOfDays < 0)
            {
                throw new ArgumentException("Number of days cannot be negative");
            }
            m_numberOfDays = numberOfDays;
        }
    }

	#endregion

	#region ( Exception Catcher )

    /// <summary>
    /// Keep instance of catching handler so we can properly
    /// record errors without forcing tracing in the query
    /// processor.
    /// </summary>

    internal class LoanReportingCatch : ExceptionCatcher
    {
        /// <summary>
        /// Handle caught exceptions and determine if the
        /// offending function should continue.
        /// </summary>
        /// <param name="exCaught">
        /// Exception to process.
        /// </param>
        /// <returns>
        /// True to continue, false to throw.
        /// </returns>

        public Boolean Catch( Exception exCaught )
        {
            // Record this error with our logger.

            Tools.LogError( "Loan reporting: Caught error." );
            Tools.LogError( exCaught );

            return true;
        }

    }

	#endregion

	#region ( Secured Rows )

    public class SecuredAccessInfo : LoanAccessInfo
    {
        /// <summary>
        /// Pull the access control details from the given row.
        /// We expect the fields we need to be stored according
        /// to the names we specified during getting the extent.
        /// </summary>

        public LoanAccessInfo InitializeReportRow( SimpleRawResultDataTable resultDataTable, int rowIndex )
        {
            object o;

            m_BrokerId = resultDataTable.GetGuidValue(rowIndex, "cBrokerId", Guid.Empty);
            m_BranchId = resultDataTable.GetGuidValue(rowIndex, "cBranchId", Guid.Empty);

            o = resultDataTable.GetValue(rowIndex,  "cLNm" );

            if( o != null && o is DBNull == false )
            {
                m_LoanName = ( String ) o;
            }
            else
            {
                m_LoanName = "";
            }

            //o = resultDataTable.GetValue(rowIndex, "cPrimBorrowerFullNm" );

            //if( o != null && o is DBNull == false )
            //{
            //    m_Borrower = ( String ) o;
            //}
            //else
            //{
            //    m_Borrower = "";
            //}

            m_LoanStatus = (E_sStatusT)resultDataTable.GetIntValue(rowIndex, "cStatusT", (int)E_sStatusT.Loan_Other);
            m_LienPosition = (E_sLienPosT)resultDataTable.GetIntValue(rowIndex, "cLienPosT", (int)E_sLienPosT.First);

            //o = resultDataTable.GetValue(rowIndex, "cOpenedD" );

            //if( o != null && o is DBNull == false )
            //{
            //    m_OpenedDate = ( DateTime ) o;
            //}
            //else
            //{
            //    m_OpenedDate = DateTime.MinValue;
            //}

            o = resultDataTable.GetValue(rowIndex, "cNoteIRSubmitted" );

            if( o != null && o is DBNull == false )
            {
                m_NoteIRSubmitted = ( Decimal ) o;
            }
            else
            {
                m_NoteIRSubmitted = Decimal.MinusOne;
            }

            m_sRateLockStatusT = (E_sRateLockStatusT)resultDataTable.GetIntValue(rowIndex, "cRateLockStatusT", (int)E_sRateLockStatusT.NotLocked);

            m_ExternalManagerId = resultDataTable.GetGuidValue(rowIndex, "PmlExternalManagerEmployeeId", Guid.Empty);

            o = resultDataTable.GetValue(rowIndex, "IsTemplate" );

            if( o != null && o is DBNull == false )
            {
                m_IsTemplate = ( Boolean ) o;
            }
            else
            {
                m_IsTemplate = false;
            }

            o = resultDataTable.GetValue(rowIndex, "IsValid");

            if( o != null && o is DBNull == false )
            {
                m_IsValid = ( Boolean ) o;
            }
            else
            {
                m_IsValid = false;
            }

            o = resultDataTable.GetValue(rowIndex, "sEmployeeManagerId" );

            if( o != null && o is DBNull == false )
            {
                m_Assignments.Add( E_RoleT.Manager , ( Guid ) o );
            }

            o = resultDataTable.GetValue(rowIndex, "sEmployeeUnderwriterId");

            if( o != null && o is DBNull == false )
            {
                m_Assignments.Add( E_RoleT.Underwriter, ( Guid ) o );
            }

            o = resultDataTable.GetValue(rowIndex, "sEmployeeLockDeskId" );

            if( o != null && o is DBNull == false )
            {
                m_Assignments.Add( E_RoleT.LockDesk , ( Guid ) o );
            }

            o = resultDataTable.GetValue(rowIndex, "sEmployeeProcessorId" );

            if( o != null && o is DBNull == false )
            {
                m_Assignments.Add( E_RoleT.Processor, ( Guid ) o );
            }

            o = resultDataTable.GetValue(rowIndex, "sEmployeeLoanOpenerId" );

            if( o != null && o is DBNull == false )
            {
                m_Assignments.Add( E_RoleT.LoanOpener, ( Guid ) o );
            }

            o = resultDataTable.GetValue(rowIndex, "sEmployeeLoanRepId");

            if( o != null && o is DBNull == false )
            {
                m_Assignments.Add( E_RoleT.LoanOfficer, ( Guid ) o );
            }

            o = resultDataTable.GetValue(rowIndex, "sEmployeeLenderAccExecId");

            if( o != null && o is DBNull == false )
            {
                m_Assignments.Add( E_RoleT.LenderAccountExecutive, ( Guid ) o );
            }

            o = resultDataTable.GetValue(rowIndex, "sEmployeeRealEstateAgentId");

            if( o != null && o is DBNull == false )
            {
                m_Assignments.Add( E_RoleT.RealEstateAgent, ( Guid ) o );
            }

            o = resultDataTable.GetValue(rowIndex, "sEmployeeCallCenterAgentId");

            if( o != null && o is DBNull == false )
            {
                m_Assignments.Add( E_RoleT.CallCenterAgent, ( Guid ) o );
            }

            o = resultDataTable.GetValue(rowIndex, "sEmployeeCloserId");

            if (o != null && o is DBNull == false)
            {
                m_Assignments.Add(E_RoleT.Closer, (Guid)o);
            }

            o = resultDataTable.GetValue(rowIndex, "sEmployeeShipperId");

            if (o != null && o is DBNull == false)
            {
                m_Assignments.Add(E_RoleT.Shipper, (Guid)o);
            }

            o = resultDataTable.GetValue(rowIndex, "sEmployeeFunderId");

            if (o != null && o is DBNull == false)
            {
                m_Assignments.Add(E_RoleT.Funder, (Guid)o);
            }

            o = resultDataTable.GetValue(rowIndex, "sEmployeePostCloserId");

            if (o != null && o is DBNull == false)
            {
                m_Assignments.Add(E_RoleT.PostCloser, (Guid)o);
            }

            o = resultDataTable.GetValue(rowIndex, "sEmployeeInsuringId");

            if (o != null && o is DBNull == false)
            {
                m_Assignments.Add(E_RoleT.Insuring, (Guid)o);
            }

            o = resultDataTable.GetValue(rowIndex, "sEmployeeCollateralAgentId");

            if (o != null && o is DBNull == false)
            {
                m_Assignments.Add(E_RoleT.CollateralAgent, (Guid)o);
            }

            o = resultDataTable.GetValue(rowIndex, "sEmployeeDocDrawerId");

            if (o != null && o is DBNull == false)
            {
                m_Assignments.Add(E_RoleT.DocDrawer, (Guid)o);
            }

            o = resultDataTable.GetValue(rowIndex, "sEmployeeCreditAuditorId");

            if (o != null && o is DBNull == false)
            {
                m_Assignments.Add(E_RoleT.CreditAuditor, (Guid)o);
            }

            o = resultDataTable.GetValue(rowIndex, "sEmployeeDisclosureDeskId");

            if (o != null && o is DBNull == false)
            {
                m_Assignments.Add(E_RoleT.DisclosureDesk, (Guid)o);
            }

            o = resultDataTable.GetValue(rowIndex, "sEmployeeJuniorProcessorId");

            if (o != null && o is DBNull == false)
            {
                m_Assignments.Add(E_RoleT.JuniorProcessor, (Guid)o);
            }

            o = resultDataTable.GetValue(rowIndex, "sEmployeeJuniorUnderwriterId");

            if (o != null && o is DBNull == false)
            {
                m_Assignments.Add(E_RoleT.JuniorUnderwriter, (Guid)o);
            }

            o = resultDataTable.GetValue(rowIndex, "sEmployeeLegalAuditorId");

            if (o != null && o is DBNull == false)
            {
                m_Assignments.Add(E_RoleT.LegalAuditor, (Guid)o);
            }

            o = resultDataTable.GetValue(rowIndex, "sEmployeeLoanOfficerAssistantId");

            if (o != null && o is DBNull == false)
            {
                m_Assignments.Add(E_RoleT.LoanOfficerAssistant, (Guid)o);
            }

            o = resultDataTable.GetValue(rowIndex, "sEmployeePurchaserId");

            if (o != null && o is DBNull == false)
            {
                m_Assignments.Add(E_RoleT.Purchaser, (Guid)o);
            }
            
            o = resultDataTable.GetValue(rowIndex, "sEmployeeQCComplianceId");

            if (o != null && o is DBNull == false)
            {
                m_Assignments.Add(E_RoleT.QCCompliance, (Guid)o);
            }

            o = resultDataTable.GetValue(rowIndex, "sEmployeeSecondaryId");

            if (o != null && o is DBNull == false)
            {
                m_Assignments.Add(E_RoleT.Secondary, (Guid)o);
            }

            o = resultDataTable.GetValue(rowIndex, "sEmployeeServicingId");

            if (o != null && o is DBNull == false)
            {
                m_Assignments.Add(E_RoleT.Servicing, (Guid)o);
            }

            m_LoanId = (Guid) resultDataTable.GetValue(rowIndex, "Id");

            return this;
        }


    }

    #endregion

}