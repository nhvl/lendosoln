﻿namespace LendersOffice.Reports
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using Common;
    using DataAccess;
    using LendersOffice.ObjLib.Security;
    using LendersOffice.QueryProcessor;
    using LendersOffice.Security;

    /// <summary>
    /// Provides a query result data table that handles encrypted data.
    /// </summary>
    public class EncryptedDataHandlingResultDataTable : SimpleRawResultDataTable
    {
        /// <summary>
        /// The time to cache supplemental report data.
        /// </summary>
        private static readonly TimeSpan SupplementalDataCacheDuration = TimeSpan.FromMinutes(30);

        /// <summary>
        /// The query identifier for the data in the table.
        /// </summary>
        private readonly ProcessingLabel label;

        /// <summary>
        /// The ID of the user running the query for the data contained in the table.
        /// </summary>
        /// <remarks>
        /// In most cases, this ID will be the same as <seealso cref="UserPrincipal.UserId"/>.
        /// For published reports, however, the ID of the user running the report, not
        /// the user that published the report, should be used.
        /// </remarks>
        private readonly Guid userId;

        /// <summary>
        /// A dictionary mapping a loan in a report to supplemental data for the loan.
        /// </summary>
        private Dictionary<Guid, SupplementalReportData> supplementalReportData;

        /// <summary>
        /// Initializes a new instance of the <see cref="EncryptedDataHandlingResultDataTable"/> class.
        /// </summary>
        /// <param name="label">
        /// The query identifier for the data in the table.
        /// </param>
        /// <param name="userPrincipal">
        /// The user running the report.
        /// </param>
        /// <param name="userId">
        /// The ID of the user running the report.
        /// </param>
        public EncryptedDataHandlingResultDataTable(ProcessingLabel label, AbstractUserPrincipal userPrincipal, Guid userId)
            : base(userPrincipal)
        {
            this.label = label;
            this.userId = userId;
            this.supplementalReportData = new Dictionary<Guid, SupplementalReportData>();
        }

        /// <summary>
        /// Processes additional reader data.
        /// </summary>
        /// <param name="reader">
        /// The reader containing loan data.
        /// </param>
        protected override void ProcessAdditionalReaderData(IDataReader reader)
        {
            var loanId = (Guid)reader["Id"];
            var supplementalData = new SupplementalReportData()
            {
                sEncryptionKey = reader.GetSafeValue("sEncryptionKey", default(Guid?)),
                sEncryptionMigrationVersion = reader.GetNullableIntEnum<EncryptionMigrationVersion>("sEncryptionMigrationVersion") ?? EncryptionMigrationVersion.Unmigrated,
                aBSsnEncrypted = reader.GetSafeValue("aBSsnEncrypted", default(byte[])),
                aCSsnEncrypted = reader.GetSafeValue("aCSsnEncrypted", default(byte[])),
            };

            this.supplementalReportData.Add(loanId, supplementalData);
        }

        /// <summary>
        /// Finalizes data for the table.
        /// </summary>
        protected override void FinalizeData()
        {
            var cacheKey = ReportHelper.GetEncryptedDataCacheKey(this.userId, this.label);
            var serializedSupplementalData = SerializationHelper.JsonNetSerialize(this.supplementalReportData);

            var wasUpdated = AutoExpiredTextCache.UpdateCache(serializedSupplementalData, cacheKey);
            if (!wasUpdated)
            {
                AutoExpiredTextCache.AddToCache(serializedSupplementalData, SupplementalDataCacheDuration, cacheKey);
            }
        }
    }
}
