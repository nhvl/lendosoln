///
/// Author: David Dao
///
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Data.Common;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.ConfigSystem;
using LendersOffice.ConfigSystem.Operations;
using LendersOffice.Constants;
using LendersOffice.QueryProcessor;
using LendersOffice.Security;

namespace LendersOffice.Reports
{
    public class SimpleRawResultDataTable
    {
        public AbstractUserPrincipal UserPrincipal { get; protected set; }
        protected Dictionary<string, int> HashColumnIndex { get; set; } = new Dictionary<string, int>(50, StringComparer.OrdinalIgnoreCase);
        protected ArrayList Result { get; set; } = new ArrayList();

        private BrokerDB m_brokerDb;
        private DataTable m_dataTable;

        public SimpleRawResultDataTable(AbstractUserPrincipal userPrincipal)
		{
            m_brokerDb = userPrincipal.BrokerDB;
            this.UserPrincipal = userPrincipal;
            m_dataTable = new DataTable();
        }

        public void LoadResult(string sqlQuery, StringDictionary parameters) 
        {
            int nRetry = 3;
            DbConnectionInfo connInfo = UserPrincipal.ConnectionInfo;
            using (var sqlConnection = connInfo.GetReadOnlyConnection()) 
            {
                while (--nRetry >= 0) 
                {
                    try 
                    {
                        using (DbCommand cmd = sqlConnection.CreateCommand())
                        {
                            cmd.CommandText = sqlQuery;

                            foreach (DictionaryEntry entry in parameters)
                            {
                                cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter(entry.Key.ToString(), entry.Value));
                            }
                            
                            cmd.Connection.Open();
                            
                            bool firstRecord = true;
                            using (DbDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection)) 
                            {
                                while (reader.Read()) 
                                {
                                    var count = reader.FieldCount;
                                    object[] row = new object[count];
                                    for (int i = 0; i < count; i++) 
                                    {
                                        if (firstRecord)
                                        {
                                            HashColumnIndex.Add(reader.GetName(i), i);

                                            DataColumn dataColumn = m_dataTable.Columns.Add();
                                            dataColumn.ColumnName = reader.GetName(i);
                                            dataColumn.DataType = reader.GetFieldType(i);
                                        }

                                        string value; 
                                        //av 7/25/2013. WHen you set the global field the desc field can be out of sync for old loans. 
                                        //go ahead and pull the value from bnrokerdb object. OPM 128508
                                        if (m_brokerDb.GetCustomFieldDescription(reader.GetName(i), out value) )
                                        {
                                            row[i] = value;
                                        }
                                        else
                                        {
                                            row[i] = reader[i];
                                        }
                                    }

                                    firstRecord = false;
                                    Result.Add(row);
                                    this.ProcessAdditionalReaderData(reader);
                                }
                            }
                        }

                        this.FinalizeData();
                        return;
                    } 
                    catch (Exception exc)
                    {
                        Tools.LogError(exc);
                        if (nRetry == 0)
                            throw;
                    }
                }
            }
        }

        /// <summary>
        /// Similar to LoadResult, but only gets the count.
        /// </summary>
        /// <param name="sqlQuery">The query to get the count.</param>
        /// <param name="parameters">The parameter names and their values.</param>
        /// <returns>The number of records matching the query.</returns>
        public int GetCount(string sqlQuery, StringDictionary parameters)
        {
            int numRetries = 3;
            int nRetry = numRetries;
            DbConnectionInfo connInfo = UserPrincipal.ConnectionInfo;
            using (var sqlConnection = connInfo.GetReadOnlyConnection())
            {
                while (--nRetry >= 0)
                {
                    try
                    {
                        var sqlParameters = new List<System.Data.SqlClient.SqlParameter>();
                        foreach (DictionaryEntry entry in parameters)
                        {
                            sqlParameters.Add(new System.Data.SqlClient.SqlParameter(entry.Key.ToString(), entry.Value));
                        }

                        int resultingCount = -1;
                        DBSelectUtility.ProcessDBData(connInfo, sqlQuery, timeout: null, parameters: sqlParameters, readerCode:(reader) =>
                        {
                            if (reader.Read())
                            {
                                resultingCount = (int)reader["Count"];
                            }
                            else
                            {
                                throw new CBaseException("Could not get the size of the custom report.  Please retry", $"Could not get the size of the custom report.  {nRetry} tries left.");
                            }
                        });

                        return resultingCount;
                    }
                    catch (Exception exc)
                    {
                        Tools.LogError(exc);
                        if (nRetry == 0)
                            throw;
                    }
                }
            }

            throw new CBaseException("Could not get the size of the custom report.  Please retry", $"Could not get the size of the custom report after {numRetries} tries.");
        }

        public int GetIntValue(int rowIndex, string columnName, int defaultValue)
        {
            object o = GetValue(rowIndex, columnName);
            if (o != null && o is DBNull == false)
            {
                return (int)o;
            }
            else
            {
                return defaultValue;
            }
        }
        public Guid GetGuidValue(int rowIndex, string columnName, Guid defaultValue)
        {
            object o = GetValue(rowIndex, columnName);

            if (o != null && o is DBNull == false)
            {
                return (Guid)o;
            }
            else
            {
                return defaultValue;
            }
        }
        public object GetValue(int rowIndex, string columnName) 
        {
            int colIndex = HashColumnIndex[columnName];
            return ((object[]) Result[rowIndex])[colIndex];
        }
        public int RowCount 
        {
            get { return Result.Count; }
        }

        /// <summary>
        /// Gets the data for the row at the specified index.
        /// </summary>
        /// <param name="rowIndex">
        /// The index of the row.
        /// </param>
        /// <returns>
        /// The data for the row.
        /// </returns>
        public DataRow GetRowAtIndex(int rowIndex)
        {
            DataRow row = m_dataTable.NewRow();

            foreach (var columnName in HashColumnIndex.Keys)
            {
                row[columnName] = GetValue(rowIndex, columnName);
            }

            return row;
        }

        /// <summary>
        /// Processes additional reader data.
        /// </summary>
        /// <param name="reader">
        /// The reader containing loan data.
        /// </param>
        protected virtual void ProcessAdditionalReaderData(IDataReader reader)
        {
        }

        /// <summary>
        /// Finalizes data for the table.
        /// </summary>
        protected virtual void FinalizeData()
        {
        }
    }
}
