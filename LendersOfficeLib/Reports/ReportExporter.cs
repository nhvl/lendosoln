﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.QueryProcessor;
using LendersOffice.Security;
using LqbGrammar.DataTypes;

namespace LendersOffice.Reports
{
    public class ReportExporter
    {
        /// <summary>
        /// Convert report to CSV string.
        /// </summary>
        /// <param name="report">
        /// Report to convert into CSV.
        /// </param>
        /// <param name="maskSsn">
        /// True to mask SSN content, false otherwise.
        /// </param>
        /// <returns>
        /// Text content of conversion.
        /// </returns>
        public static string ConvertToCsvAsString(AbstractUserPrincipal principal, Report report)
        {
            string outputFile = ConvertToCsvAsFile(principal, report);

            string output = string.Empty;
            using (StreamReader sr = new StreamReader(outputFile))
            {
                output = sr.ReadToEnd();
            }

            return output;
        }

        /// <summary>
        /// Convert report to CSV string and store in local file.
        /// </summary>
        /// <param name="report">
        /// Report to convert into CSV.
        /// </param>
        /// <param name="maskSsn">
        /// True to mask SSN content, false otherwise.
        /// </param>
        /// <returns>
        /// Path to the local file.
        /// </returns>
        public static string ConvertToCsvAsFile(AbstractUserPrincipal principal, Report report)
        {
            string outputFile = null;

            if (report == null)
                throw new ArgumentNullException("Cannot convert null report to CSV.");
            try
            {
                outputFile = ConvertTablesToCsvAsFile(principal, report.Label, report.Tables, report.Columns, report.Results, report.Label.Title);
                LogOnSensitiveDataExport(principal, report);
                return outputFile;
            }
            catch (Exception exc)
            {
                Tools.LogError(exc);
                throw;
            }
        }

        /// <summary>
        /// Convert report to text file.
        /// </summary>
        /// <param name="report">
        /// Report to convert into text.
        /// </param>
        /// <param name="maskSsn">
        /// True to mask SSN content, false otherwise.
        /// </param>
        /// <returns>
        /// Text content of conversion.
        /// </returns>
        public static string ConvertToTxtAsFile(AbstractUserPrincipal principal, Report report)
        {
            string str = null;

            if (report == null)
                throw new ArgumentNullException("Cannot convert null report to TXT.");
            try
            {
                str = ConvertToTxtAsFile(principal, report.Label, report.Tables, report.Columns, report.Results, report.Label.Title);
                LogOnSensitiveDataExport(principal, report);
                return str;
            }
            catch (Exception exc)
            {
                Tools.LogError(exc);
                throw;
            }
        }

        private static string ConvertTablesToCsvAsFile(AbstractUserPrincipal principal, ProcessingLabel reportLabel, IEnumerable rReport, Columns aColumns, CalculationResults aResults, string sTitle)
        {
            // Handle conversion and return text in string.
            try
            {
                string outputFile = TempFileUtils.NewTempFilePath();

                SupplementalDataContainer supplementalDataContainer = null;
                if (principal.HasPermission(Permission.AllowExportingFullSsnViaCustomReports))
                {
                    supplementalDataContainer = RetrieveSupplementalData(principal, reportLabel, aColumns);
                }

                using (StreamWriter sw = new StreamWriter(outputFile, false))
                {
                    sw.WriteLine("Report: " + sTitle);
                    sw.WriteLine();

                    foreach (Rows rRows in rReport)
                    {
                        ConvertRowToCsv(sw, rRows, aColumns, supplementalDataContainer);
                    }

                    if (aResults.Count > 0)
                    {
                        // Now finish with the aggregation results for
                        // the entire report.

                        int c;

                        sw.WriteLine("Cumulative Results");

                        for (c = 0; c < aColumns.Count; ++c)
                        {
                            // Write a column header.  We place a tab
                            // before every following header.

                            if (c > 0)
                            {
                                sw.Write(",");
                            }

                            sw.Write(string.Format("\"{0}\"", aColumns[c].Name));
                        }

                        sw.WriteLine();

                        for (c = 0; c < aColumns.Count; ++c)
                        {
                            // Write a column header.  We place a tab
                            // before every following header.

                            if (c > 0)
                            {
                                sw.Write(",");
                            }

                            sw.Write(new string('-', aColumns[c].Name.Length));
                        }

                        sw.WriteLine();

                        for (c = 0; c < aColumns.Count; ++c)
                        {
                            // We search for all results that pertain to
                            // the current column.

                            int n = 0;

                            if (c > 0)
                            {
                                sw.Write(",");
                            }

                            foreach (CalculationResult result in aResults)
                            {
                                if (result.Column != c)
                                {
                                    continue;
                                }

                                if (n == 0)
                                {
                                    sw.Write("\"");
                                }
                                else
                                {
                                    sw.Write(" /");
                                }

                                switch (result.What)
                                {
                                    case E_FunctionType.Av: sw.Write(" Avg");
                                        break;

                                    case E_FunctionType.Sm: sw.Write(" Tot");
                                        break;

                                    case E_FunctionType.Mn: sw.Write(" Min");
                                        break;

                                    case E_FunctionType.Mx: sw.Write(" Max");
                                        break;

                                    case E_FunctionType.Ct: sw.Write(" Num");
                                        break;
                                }

                                sw.Write(" = " + result.GetFormattedResult("N3"));

                                ++n;
                            }

                            if (n > 0)
                            {
                                sw.Write("\"");
                            }
                        }
                    }
                }

                return outputFile;
            }
            catch (Exception e)
            {
                // Pass on the love.

                throw e;
            }
        }

        private static void ConvertRowToCsv(StreamWriter sw, Rows rRows, Columns aColumns, SupplementalDataContainer supplementalDataContainer)
        {
            // Recursively render all nested tables and put a subtotal
            // table at the end of each contained leaf table.

            foreach (Object rO in rRows)
            {
                Rows rS = rO as Rows;

                if (rS != null)
                {
                    ConvertRowToCsv(sw, rS, aColumns, supplementalDataContainer);
                }
                else
                {
                    // Write the columns as headers for this table.
                    // Each table may be grouped.  We display the
                    // grouping values if present.

                    int c = 0;

                    foreach (LookupLabel group in rRows.GroupBy)
                    {
                        sw.WriteLine(string.Format("\"[{0}] {1}: {2}\"", c.ToString().PadLeft(3), group.Label, group.Value));

                        ++c;
                    }

                    for (c = 0; c < aColumns.Count; ++c)
                    {
                        // Write a column header.  We place a tab
                        // before every following header.

                        if (c > 0)
                        {
                            sw.Write(",");
                        }

                        sw.Write(string.Format("\"{0}\"", aColumns[c].Name));
                    }

                    sw.WriteLine();

                    for (c = 0; c < aColumns.Count; ++c)
                    {
                        // Write a column header.  We place a tab
                        // before every following header.

                        if (c > 0)
                        {
                            sw.Write(",");
                        }

                        sw.Write(new string('-', aColumns[c].Name.Length));
                    }

                    sw.WriteLine();

                    // Write out each row of the current table.  We
                    // only show what is visible.  The first n cells
                    // of each row correspond to the n columns of
                    // the report.  Note that our testing with excel
                    // 2000 shows that wrapping elements with double
                    // quotes seems to protect nested commas that
                    // occur in the content of the cell.

                    foreach (Row row in rRows)
                    {
                        // Write out each element of the current
                        // row.  We display in representation format.

                        if (supplementalDataContainer != null)
                        {
                            MergeEncryptedDataToRow(row, supplementalDataContainer);
                        }

                        for (c = 0; c < aColumns.Count; ++c)
                        {
                            string cell;

                            if (c > 0)
                            {
                                sw.Write(",");
                            }

                            cell = String.Format("{0:" + aColumns[c].Format + "}", row[c].Data).Replace("\"", "\"\"");

                            if (cell.Length > 0)
                            {
                                sw.Write(string.Format("\"{0}\"", cell));
                            }
                        }

                        sw.WriteLine();
                    }

                    // The calculation results, if any, need to be
                    // displayed as well.  We line up the results
                    // with the corresponding column.

                    if (rRows.Results.Count > 0)
                    {
                        for (c = 0; c < aColumns.Count; ++c)
                        {
                            // We search for all results that pertain to
                            // the current column.

                            int n = 0;

                            if (c > 0)
                            {
                                sw.Write(",");
                            }

                            foreach (CalculationResult result in rRows.Results)
                            {
                                if (result.Column != c)
                                {
                                    continue;
                                }

                                if (n == 0)
                                {
                                    sw.Write("\"");
                                }
                                else
                                {
                                    sw.Write(" /");
                                }

                                switch (result.What)
                                {
                                    case E_FunctionType.Av: sw.Write(" Avg");
                                        break;

                                    case E_FunctionType.Sm: sw.Write(" Tot");
                                        break;

                                    case E_FunctionType.Mn: sw.Write(" Min");
                                        break;

                                    case E_FunctionType.Mx: sw.Write(" Max");
                                        break;

                                    case E_FunctionType.Ct: sw.Write(" Num");
                                        break;
                                }

                                sw.Write(string.Format(" = {0}", result.GetFormattedResult("N3")));

                                ++n;
                            }

                            if (n > 0)
                            {
                                sw.Write("\"");
                            }
                        }

                        if (c > 0)
                        {
                            sw.WriteLine();
                        }
                    }

                    sw.WriteLine();

                    return;
                }
            }

            if (rRows.GroupBy.Count > 0 && rRows.Results.Count > 0)
            {
                // Show subtotal calculations organized by column.

                int c = 0;

                sw.WriteLine("Subtotals for");

                foreach (LookupLabel group in rRows.GroupBy)
                {
                    sw.Write(string.Format("\"[{0}] {1}: {2}\"", c.ToString().PadLeft(3), group.Label, group.Value));

                    ++c;
                }

                for (c = 0; c < aColumns.Count; ++c)
                {
                    // Write a column header.  We place a tab
                    // before every following header.

                    if (c > 0)
                    {
                        sw.Write(",");
                    }

                    sw.Write(string.Format("\"{0}\"", aColumns[c].Name));
                }

                sw.WriteLine();

                for (c = 0; c < aColumns.Count; ++c)
                {
                    // Write a column header.  We place a tab
                    // before every following header.

                    if (c > 0)
                    {
                        sw.Write(",");
                    }

                    sw.Write(new string('-', aColumns[c].Name.Length));
                }

                sw.WriteLine();

                for (c = 0; c < aColumns.Count; ++c)
                {
                    // We search for all results that pertain to
                    // the current column.

                    int n = 0;

                    if (c > 0)
                    {
                        sw.Write(",");
                    }

                    foreach (CalculationResult result in rRows.Results)
                    {
                        if (result.Column != c)
                        {
                            continue;
                        }

                        if (n == 0)
                        {
                            sw.Write("\"");
                        }
                        else
                        {
                            sw.Write(" /");
                        }

                        switch (result.What)
                        {
                            case E_FunctionType.Av: sw.Write(" Avg");
                                break;

                            case E_FunctionType.Sm: sw.Write(" Tot");
                                break;

                            case E_FunctionType.Mn: sw.Write(" Min");
                                break;

                            case E_FunctionType.Mx: sw.Write(" Max");
                                break;

                            case E_FunctionType.Ct: sw.Write(" Num");
                                break;
                        }

                        sw.Write(string.Format(" = {0}", result.GetFormattedResult("N3")));

                        ++n;
                    }

                    if (n > 0)
                    {
                        sw.Write("\"");
                    }
                }

                if (c > 0)
                {
                    sw.WriteLine();
                }

                sw.WriteLine();

                return;
            }
        }

        /// <summary>
        /// Convert report to text file.
        /// </summary>
        /// <param name="rReport">
        /// Report to convert into text.
        /// </param>
        /// <returns>
        /// Text content of conversion.
        /// </returns>
        private static string ConvertToTxtAsFile(AbstractUserPrincipal principal, ProcessingLabel reportLabel, IEnumerable rReport, Columns aColumns, CalculationResults aResults, String sTitle)
        {
            // Handle conversion and return text in string.

            try
            {
                // Initialize a new string and generate the full
                // text description of the report.

                string outputFile = TempFileUtils.NewTempFilePath();

                SupplementalDataContainer supplementalDataContainer = null;
                if (principal.HasPermission(Permission.AllowExportingFullSsnViaCustomReports))
                {
                    supplementalDataContainer = RetrieveSupplementalData(principal, reportLabel, aColumns);
                }

                using (StreamWriter sw = new StreamWriter(outputFile, false))
                {
                    sw.WriteLine("Report: " + sTitle);
                    sw.WriteLine();

                    foreach (Rows rRows in rReport)
                    {
                        ConvertRowToTxt(sw, rRows, aColumns, supplementalDataContainer);
                    }

                    if (aResults.Count > 0)
                    {
                        // Now finish with the aggregation results for
                        // the entire report.

                        int c;

                        sw.WriteLine();
                        sw.WriteLine("Cumulative Results");

                        for (c = 0; c < aColumns.Count; ++c)
                        {
                            // Write a column header.  We place a tab
                            // before every following header.

                            if (c > 0)
                            {
                                sw.Write("\t");
                            }

                            sw.Write(aColumns[c].Name);
                        }

                        sw.WriteLine();

                        for (c = 0; c < aColumns.Count; ++c)
                        {
                            // Write a column header.  We place a tab
                            // before every following header.

                            if (c > 0)
                            {
                                sw.Write("\t");
                            }

                            sw.Write(new string('-', aColumns[c].Name.Length));
                        }

                        sw.WriteLine();

                        for (c = 0; c < aColumns.Count; ++c)
                        {
                            // We search for all results that pertain to
                            // the current column.

                            int n = 0;

                            if (c > 0)
                            {
                                sw.Write("\t");
                            }

                            foreach (CalculationResult result in aResults)
                            {
                                if (result.Column != c)
                                {
                                    continue;
                                }

                                if (n > 0)
                                {
                                    sw.Write(" /");
                                }

                                switch (result.What)
                                {
                                    case E_FunctionType.Av: sw.Write(" Avg");
                                        break;

                                    case E_FunctionType.Sm: sw.Write(" Tot");
                                        break;

                                    case E_FunctionType.Mn: sw.Write(" Min");
                                        break;

                                    case E_FunctionType.Mx: sw.Write(" Max");
                                        break;

                                    case E_FunctionType.Ct: sw.Write(" Num");
                                        break;
                                }

                                sw.Write(string.Format(" = {0}", result.GetFormattedResult("N3")));

                                ++n;
                            }
                        }
                    }
                }
                
                return outputFile;
            }
            catch (Exception e)
            {
                // Pass on the love.

                throw e;
            }
        }

        private static void ConvertRowToTxt(StreamWriter sw, Rows rRows, Columns aColumns, SupplementalDataContainer supplementalDataContainer)
        {
            // Recursively render all nested tables and put a subtotal
            // table at the end of each contained leaf table.

            foreach (Object rO in rRows)
            {
                Rows rS = rO as Rows;

                if (rS != null)
                {
                    ConvertRowToTxt(sw, rS, aColumns, supplementalDataContainer);
                }
                else
                {
                    // Write the columns as headers for this table.
                    // Each table may be grouped.  We display the
                    // grouping values if present.

                    int c = 0;

                    foreach (LookupLabel group in rRows.GroupBy)
                    {
                        sw.WriteLine(string.Format("[{0}] {1}: {2}", c.ToString().PadLeft(3), group.Label, group.Value));
                        
                        ++c;
                    }

                    for (c = 0; c < aColumns.Count; ++c)
                    {
                        // Write a column header.  We place a tab
                        // before every following header.

                        if (c > 0)
                        {
                            sw.Write("\t");
                        }

                        sw.Write(aColumns[c].Name);
                    }

                    sw.WriteLine();

                    for (c = 0; c < aColumns.Count; ++c)
                    {
                        // Write a column header.  We place a tab
                        // before every following header.

                        if (c > 0)
                        {
                            sw.Write("\t");
                        }

                        sw.Write(new string('-', aColumns[c].Name.Length));
                    }

                    sw.WriteLine();
                    
                    // Write out each row of the current table.  We
                    // only show what is visible.  The first n cells
                    // of each row correspond to the n columns of
                    // the report.

                    foreach (Row row in rRows)
                    {
                        // Write out each element of the current
                        // row.  We display in representation format.

                        if (supplementalDataContainer != null)
                        {
                            MergeEncryptedDataToRow(row, supplementalDataContainer);
                        }

                        for (c = 0; c < aColumns.Count; ++c)
                        {
                            string cell;

                            if (c > 0)
                            {
                                sw.Write("\t");
                            }

                            cell = String.Format("{0:" + aColumns[c].Format + "}", row[c].Data);

                            sw.Write(cell.Replace("\t", " "));
                        }

                        sw.WriteLine();
                    }

                    // The calculation results, if any, need to be
                    // displayed as well.  We line up the results
                    // with the corresponding column.

                    if (rRows.Results.Count > 0)
                    {
                        for (c = 0; c < aColumns.Count; ++c)
                        {
                            // We search for all results that pertain to
                            // the current column.

                            int n = 0;

                            if (c > 0)
                            {
                                sw.Write("\t");
                            }

                            foreach (CalculationResult result in rRows.Results)
                            {
                                if (result.Column != c)
                                {
                                    continue;
                                }

                                if (n > 0)
                                {
                                    sw.Write(" /");
                                }

                                switch (result.What)
                                {
                                    case E_FunctionType.Av: sw.Write(" Avg");
                                        break;

                                    case E_FunctionType.Sm: sw.Write(" Tot");
                                        break;

                                    case E_FunctionType.Mn: sw.Write(" Min");
                                        break;

                                    case E_FunctionType.Mx: sw.Write(" Max");
                                        break;

                                    case E_FunctionType.Ct: sw.Write(" Num");
                                        break;
                                }

                                sw.Write(string.Format(" = {0}", result.GetFormattedResult("N3")));

                                ++n;
                            }
                        }

                        if (c > 0)
                        {
                            sw.WriteLine();
                        }
                    }

                    sw.WriteLine();

                    return;
                }
            }

            if (rRows.GroupBy.Count > 0 && rRows.Results.Count > 0)
            {
                // Show subtotal calculations organized by column.

                int c = 0;

                sw.WriteLine("Subtotals for");

                foreach (LookupLabel group in rRows.GroupBy)
                {
                    sw.WriteLine(string.Format("[{0}] {1}: {2}", c.ToString().PadLeft(3), group.Label, group.Value));
                    
                    ++c;
                }

                for (c = 0; c < aColumns.Count; ++c)
                {
                    // Write a column header.  We place a tab
                    // before every following header.

                    if (c > 0)
                    {
                        sw.Write("\t");
                    }

                    sw.Write(aColumns[c].Name);
                }

                sw.WriteLine();
                
                for (c = 0; c < aColumns.Count; ++c)
                {
                    // Write a column header.  We place a tab
                    // before every following header.

                    if (c > 0)
                    {
                        sw.Write("\t");
                    }

                    sw.Write(new string('-', aColumns[c].Name.Length));
                }

                sw.WriteLine();

                // The calculation results, if any, need to be
                // displayed as well.  We line up the results
                // with the corresponding column.

                for (c = 0; c < aColumns.Count; ++c)
                {
                    // We search for all results that pertain to
                    // the current column.

                    int n = 0;

                    if (c > 0)
                    {
                        sw.Write("\t");
                    }

                    foreach (CalculationResult result in rRows.Results)
                    {
                        if (result.Column != c)
                        {
                            continue;
                        }

                        if (n > 0)
                        {
                            sw.Write(" /");
                        }

                        switch (result.What)
                        {
                            case E_FunctionType.Av: sw.Write(" Avg");
                                break;

                            case E_FunctionType.Sm: sw.Write(" Tot");
                                break;

                            case E_FunctionType.Mn: sw.Write(" Min");
                                break;

                            case E_FunctionType.Mx: sw.Write(" Max");
                                break;

                            case E_FunctionType.Ct: sw.Write(" Num");
                                break;
                        }

                        sw.Write(string.Format(" = {0}", result.GetFormattedResult("N3")));

                        ++n;
                    }
                }

                if (c > 0)
                {
                    sw.WriteLine();
                }

                sw.WriteLine();
            }

            return;
        }

        private static SupplementalDataContainer RetrieveSupplementalData(AbstractUserPrincipal principal, ProcessingLabel reportLabel, Columns columns)
        {
            var borrowerSocialColumnIndex = columns.FindIndex("aBSsn");
            var coborrowerSocialColumnIndex = columns.FindIndex("aCssn");

            if (borrowerSocialColumnIndex == -1 && coborrowerSocialColumnIndex == -1)
            {
                return null;
            }

            var cacheKey = ReportHelper.GetEncryptedDataCacheKey(principal.UserId, reportLabel);
            var serializedSupplementalData = AutoExpiredTextCache.GetFromCache(cacheKey);

            if (serializedSupplementalData == null)
            {
                throw new CBaseException(ErrorMessages.UnableToLoadReport, "Cache expired or invalid for key " + cacheKey);
            }

            var supplementalDataDictionary = SerializationHelper.JsonNetDeserialize<Dictionary<Guid, SupplementalReportData>>(serializedSupplementalData);
            return new SupplementalDataContainer()
            {
                SupplementalData = supplementalDataDictionary,
                BorrowerSocialColumnIndex = borrowerSocialColumnIndex,
                CoborrowerSocialColumnIndex = coborrowerSocialColumnIndex
            };
        }

        private static void MergeEncryptedDataToRow(Row row, SupplementalDataContainer supplementalDataContainer)
        {
            SupplementalReportData reportData;
            if (!supplementalDataContainer.SupplementalData.TryGetValue(row.Key, out reportData))
            {
                throw new CBaseException(ErrorMessages.UnableToLoadReport, "Could not find supplemental data for row with key " + row.Key);
            }

            if (reportData.sEncryptionMigrationVersion == ObjLib.Security.EncryptionMigrationVersion.Unmigrated)
            {
                if (supplementalDataContainer.BorrowerSocialColumnIndex != -1)
                {
                    row.Items[supplementalDataContainer.BorrowerSocialColumnIndex].Data = reportData.aBSsn;
                }

                if (supplementalDataContainer.CoborrowerSocialColumnIndex != -1)
                {
                    row.Items[supplementalDataContainer.CoborrowerSocialColumnIndex].Data = reportData.aCSsn;
                }
            }
            else
            {
                var encryptionKey = EncryptionKeyIdentifier.Create(reportData.sEncryptionKey.Value).Value;

                if (supplementalDataContainer.BorrowerSocialColumnIndex != -1)
                {
                    var decryptedBorrowerSocial = LendersOffice.Drivers.Encryption.EncryptionHelper.DecryptString(encryptionKey, reportData.aBSsnEncrypted);
                    row.Items[supplementalDataContainer.BorrowerSocialColumnIndex].Data = decryptedBorrowerSocial;
                }

                if (supplementalDataContainer.CoborrowerSocialColumnIndex != -1)
                {
                    var decryptedCoborrowerSocial = LendersOffice.Drivers.Encryption.EncryptionHelper.DecryptString(encryptionKey, reportData.aCSsnEncrypted);
                    row.Items[supplementalDataContainer.CoborrowerSocialColumnIndex].Data = decryptedCoborrowerSocial;
                }
            }
        }

        private static void LogOnSensitiveDataExport(AbstractUserPrincipal principal, Report report)
        {
            if (!principal.HasPermission(Permission.AllowExportingFullSsnViaCustomReports))
            {
                return;
            }

            var hasBorrowerSsn = report.Columns.Has("aBSsn");
            var hasCoborrowerSsn = report.Columns.Has("aCSsn");

            string dataLabel = null;
            if (!hasBorrowerSsn && !hasCoborrowerSsn)
            {
                return;
            }
            else if (hasBorrowerSsn && hasCoborrowerSsn)
            {
                dataLabel = "borrower and co-borrower";
            }
            else
            {
                dataLabel = hasBorrowerSsn ? "borrower" : "co-borrower";
            }

            var log = $"User {principal.DisplayName} ({principal.UserId}) exported report {report.Label.Title} containing unmasked {dataLabel} SSNs at {DateTime.Now}. ";
            var affectedLoans = "Loans in report: " + Environment.NewLine + string.Join(Environment.NewLine, report.Flatten().Select(row => row.Key));
            Tools.LogInfo("SensitiveReportExport", log + affectedLoans);

            SecurityEventLogHelper.CreateSensitiveDataExportLog(principal, SensitiveReportType.CustomReport, report.Label.Title);
        }

        private class SupplementalDataContainer
        {
            public Dictionary<Guid, SupplementalReportData> SupplementalData { get; set; }
            public int BorrowerSocialColumnIndex { get; set; }
            public int CoborrowerSocialColumnIndex { get; set; }
        }
    }
}
