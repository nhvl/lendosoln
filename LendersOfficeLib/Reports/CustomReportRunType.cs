﻿namespace LendersOffice.Reports
{
    /// <summary>
    /// The run type for a custom report.
    /// </summary>
    public enum CustomReportRunType
    {
        /// <summary>
        /// The report is being run for the pipeline.
        /// </summary>
        Pipeline = 0
    }
}
