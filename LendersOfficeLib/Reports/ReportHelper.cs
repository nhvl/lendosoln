﻿namespace LendersOffice.Reports
{
    using System;
    using DataAccess;
    using LendersOffice.QueryProcessor;

    /// <summary>
    /// Provides a set of helper methods for custom reports.
    /// </summary>
    public static class ReportHelper
    {
        /// <summary>
        /// Gets the key for the encrypted data cache.
        /// </summary>
        /// <param name="userId">
        /// The ID of the user running the report.
        /// </param>
        /// <param name="reportLabel">
        /// The label for the report.
        /// </param>
        /// <returns>
        /// The cache key.
        /// </returns>
        /// <remarks>
        /// Adapted from https://stackoverflow.com/a/13617375 .
        /// </remarks>
        public static string GetEncryptedDataCacheKey(Guid userId, ProcessingLabel reportLabel)
        {
            var invalids = System.IO.Path.GetInvalidFileNameChars();

            var cacheKey = userId.ToString("N") + "_" + reportLabel.Title;
            var sanitizedKey = string.Join("_", cacheKey.Split(invalids, StringSplitOptions.RemoveEmptyEntries)).TrimEnd('.');
            return Tools.TruncateString(sanitizedKey, AutoExpiredTextCache.KEY_LENGTH_MAX);
        }
    }
}
