﻿namespace LendersOffice.Reports
{
    using System;
    using ObjLib.Security;

    /// <summary>
    /// Provides a simple container for supplemental report data.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "Match loan and app naming.")]
    public class SupplementalReportData
    {
        /// <summary>
        /// Gets or sets the encryption key for a loan in the report.
        /// </summary>
        public Guid? sEncryptionKey { get; set; }

        /// <summary>
        /// Gets or sets the encryption migration version for a loan in the report.
        /// </summary>
        public EncryptionMigrationVersion sEncryptionMigrationVersion { get; set; }

        /// <summary>
        /// Gets or sets the unencrypted borrower SSN for a loan in the report.
        /// </summary>
        public string aBSsn { get; set; }

        /// <summary>
        /// Gets or sets the encrypted borrower SSN for a loan in the report.
        /// </summary>
        public byte[] aBSsnEncrypted { get; set; }

        /// <summary>
        /// Gets or sets the unencrypted co-borrower SSN for a loan in the report.
        /// </summary>
        public string aCSsn { get; set; }

        /// <summary>
        /// Gets or sets the encrypted co-borrower SSN for a loan in the report.
        /// </summary>
        public byte[] aCSsnEncrypted { get; set; }
    }
}
