﻿namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using DataAccess.Core.Construction;
    using LendersOffice.Common;

    /// <summary>
    /// Contains the result of the "Fed Calendar" calculation for a time span
    /// to be fed into the APR calculation.
    /// </summary>
    public class FedCalendarPeriods
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FedCalendarPeriods"/> class.
        /// </summary>
        /// <param name="unitPeriods">The number of unit periods.</param>
        /// <param name="fractionalPeriods">The number of fractional periods.</param>
        /// <param name="fractionsPerUnitPeriod">The number of fractional periods per unit period.</param>
        /// <param name="hasAdditionalOddDays">Indicates that the time span contains days not accounted for by the unit and fractional periods.</param>
        public FedCalendarPeriods(int unitPeriods, int fractionalPeriods, int fractionsPerUnitPeriod, bool hasAdditionalOddDays)
        {
            // Normalize inputs
            if (fractionalPeriods >= fractionsPerUnitPeriod || fractionalPeriods < 0)
            {
                this.UnitPeriods = unitPeriods + GaussianQuot(fractionalPeriods, fractionsPerUnitPeriod);
                this.FractionalPeriods = GaussianMod(fractionalPeriods, fractionsPerUnitPeriod);
                ////this.UnitPeriods = unitPeriods + (fractionalPeriods / fractionsPerUnitPeriod);
                ////this.FractionalPeriods = fractionalPeriods % fractionsPerUnitPeriod;
            }
            else
            {
                this.UnitPeriods = unitPeriods;
                this.FractionalPeriods = fractionalPeriods;
            }

            this.FractionsPerUnitPeriod = fractionsPerUnitPeriod;
            this.HasAdditionalOddDays = hasAdditionalOddDays;
        }

        /// <summary>
        /// Gets the value of the unit periods for the given time span and APR target.
        /// </summary>
        public int UnitPeriods { get; }

        /// <summary>
        /// Gets the value of the fractional periods for the given time span and APR target.
        /// </summary>
        public int FractionalPeriods { get; }

        /// <summary>
        /// Gets the value of the fractional periods per unit period for the given time span and APR target.
        /// </summary>
        public int FractionsPerUnitPeriod { get; }
        
        /// <summary>
        /// Gets a value indicating whether or not there are days in the time span not accounted for by the unit and fractional periods.
        /// </summary>
        public bool HasAdditionalOddDays { get; }

        /// <summary>
        /// Calculates the Fed Calendar results for the given span of dates for a construction-only
        /// loan (or phase of a loan) of the given interest accrual type.
        /// </summary>
        /// <param name="startDate">The beginning of the time span.</param>
        /// <param name="endDate">The end of the time span.</param>
        /// <param name="accrualType">The interest accrual type.</param>
        /// <returns>The Fed Calendar periods.</returns>
        public static FedCalendarPeriods ConstructionOnlyPeriodBetween(CDateTime startDate, CDateTime endDate, ConstructionPhaseIntAccrual accrualType)
        {
            return ConstructionOnlyPeriodBetween(startDate.DateTimeForComputation, endDate.DateTimeForComputation, accrualType);
        }

        /// <summary>
        /// Calculates the Fed Calendar results for the given span of dates for a construction-only
        /// loan (or phase of a loan) of the given interest accrual type.
        /// </summary>
        /// <param name="startDate">The beginning of the time span.</param>
        /// <param name="endDate">The end of the time span.</param>
        /// <param name="accrualType">The interest accrual type.</param>
        /// <returns>The Fed Calendar periods.</returns>
        public static FedCalendarPeriods ConstructionOnlyPeriodBetween(DateTime startDate, DateTime endDate, ConstructionPhaseIntAccrual accrualType)
        {
            switch (accrualType)
            {
                case ConstructionPhaseIntAccrual.ActualDays_365_360:
                case ConstructionPhaseIntAccrual.ActualDays_365_365:
                    return WholeYearsAndDaysBetween(startDate, endDate);
                case ConstructionPhaseIntAccrual.Monthly_360_360:
                    return WholeYearsAndMonthsBetween(startDate, endDate);
                case ConstructionPhaseIntAccrual.Blank:
                    throw new CBaseException(ErrorMessages.Generic, "Cannot calculate construction APR if the construction phase interest accrual is Blank.");
                default:
                    throw new UnhandledEnumException(accrualType);
            }
        }

        /// <summary>
        /// Calculates the Fed Calendar results for the given span of dates for the construction period
        /// of a construction-to-permanent loan disclosed as a single transaction.
        /// </summary>
        /// <param name="startDate">The beginning of the time span.</param>
        /// <param name="endDate">The end of the time span.</param>
        /// <returns>The Fed Calendar periods.</returns>
        public static FedCalendarPeriods ConstructionPermPeriodsBetween(CDateTime startDate, CDateTime endDate)
        {
            return ConstructionPermPeriodsBetween(startDate.DateTimeForComputation, endDate.DateTimeForComputation);
        }

        /// <summary>
        /// Calculates the Fed Calendar results for the given span of dates for the construction period
        /// of a construction-to-permanent loan disclosed as a single transaction.
        /// </summary>
        /// <param name="startDate">The beginning of the time span.</param>
        /// <param name="endDate">The end of the time span.</param>
        /// <returns>The Fed Calendar periods.</returns>
        public static FedCalendarPeriods ConstructionPermPeriodsBetween(DateTime startDate, DateTime endDate)
        {
            return WholeMonthsAndDaysBetween(startDate, endDate);
        }

        /// <summary>
        /// Given a day, find the same day of the month a given number
        /// of months away. If the given day is the end of the month,
        /// instead find the end of the month a given number of months
        /// away.
        /// </summary>
        /// <example>The date 2018-02-15 plus 6 months would be 2018-08-15.</example>
        /// <example>The date 2018-02-28 plus 6 months would be 2018-08-31.</example>
        /// <param name="baseDate">The base date.</param>
        /// <param name="months">The number of months to add.</param>
        /// <returns>The date a specified number of months distant, respecting end of month.</returns>
        public static DateTime ProjectByMonthRespectingEndOfMonth(DateTime baseDate, int months)
        {
            bool dateIsEndOfMonth = baseDate.AddDays(1).Month > baseDate.Month;

            if (dateIsEndOfMonth)
            {
                return baseDate.AddDays(1).AddMonths(months).AddDays(-1);
            }
            else
            {
                return baseDate.AddMonths(months);
            }
        }

        /// <summary>
        /// Calculates the Fed Calendar months and days results for the given span of dates.
        /// </summary>
        /// <param name="startDate">The beginning of the time span.</param>
        /// <param name="endDate">The end of the time span.</param>
        /// <returns>The Fed Calendar periods.</returns>
        public static FedCalendarPeriods WholeMonthsAndDaysBetween(DateTime startDate, DateTime endDate)
        {
            int months = ((endDate.Year - startDate.Year) * 12) + endDate.Month - startDate.Month;
            DateTime endDateInStartDateMonth;
            int days;

            endDateInStartDateMonth = ProjectByMonthRespectingEndOfMonth(endDate, -months);

            if (endDateInStartDateMonth < startDate)
            {
                // Oops, went too far. Back up a month.
                months--;
                endDateInStartDateMonth = ProjectByMonthRespectingEndOfMonth(endDate, -months);
            }

            days = endDateInStartDateMonth.Subtract(startDate).Days;

            return new FedCalendarPeriods(months, days, 30, false);
        }

        /// <summary>
        /// Calculates the Fed Calendar years and months results for the given span of dates.
        /// </summary>
        /// <param name="startDate">The beginning of the time span.</param>
        /// <param name="endDate">The end of the time span.</param>
        /// <returns>The Fed Calendar periods.</returns>
        public static FedCalendarPeriods WholeYearsAndMonthsBetween(DateTime startDate, DateTime endDate)
        {
            var monthsPeriods = WholeMonthsAndDaysBetween(startDate, endDate);

            return new FedCalendarPeriods(monthsPeriods.UnitPeriods / 12, monthsPeriods.UnitPeriods % 12, 12, monthsPeriods.FractionalPeriods > 0);
        }

        /// <summary>
        /// Calculates the Fed Calendar years and days results for the given span of dates.
        /// </summary>
        /// <param name="startDate">The beginning of the time span.</param>
        /// <param name="endDate">The end of the time span.</param>
        /// <returns>The Fed Calendar periods.</returns>
        public static FedCalendarPeriods WholeYearsAndDaysBetween(DateTime startDate, DateTime endDate)
        {
            int years = endDate.Year - startDate.Year;
            DateTime endDateInStartDateYear = endDate.AddYears(-years);
            int days;

            // If the month and day of startDate are later in the year than endDate...
            if (endDateInStartDateYear < startDate)
            {
                // Borrow a year from the year total to give to the projected end date.
                years--;
                days = endDateInStartDateYear.AddYears(1).Subtract(startDate).Days;
            }
            else
            {
                days = endDateInStartDateYear.Subtract(startDate).Days;
            }

            return new FedCalendarPeriods(years, days, 365, false);
        }

        /// <summary>
        /// Returns the integer remainder between 0 and (divisor - 1), inclusive, such that
        /// dividend = divisor * quotient + remainder.
        /// </summary>
        /// <param name="dividend">The dividend.</param>
        /// <param name="divisor">The non-negative divisor.</param>
        /// <returns>The modulus of the dividend and divisor.</returns>
        public static int GaussianMod(int dividend, int divisor)
        {
            if (divisor < 0)
            {
                throw new ArgumentException($"{nameof(GaussianMod)} not defined with negative divisor");
            }

            int mod = dividend % divisor;

            if (mod < 0)
            {
                return mod + divisor;
            }
            else
            {
                return mod;
            }
        }

        /// <summary>
        /// Returns the integer quotient of the dividend and divisor such that the remainder
        /// is between 0 and (divisor - 1), inclusive.
        /// </summary>
        /// <param name="dividend">The dividend.</param>
        /// <param name="divisor">The non-negative divisor.</param>
        /// <returns>The specified quotient.</returns>
        public static int GaussianQuot(int dividend, int divisor)
        {
            if (divisor < 0)
            {
                throw new ArgumentException($"{nameof(GaussianQuot)} not defined with negative divisor");
            }

            int quot = dividend / divisor;

            if (dividend < 0)
            {
                return quot - 1;
            }
            else
            {
                return quot;
            }
        }

        /// <summary>
        /// Adds the specified number of unit periods to the existing periods.
        /// </summary>
        /// <param name="extraUnitPeriods">The number of unit periods to add.</param>
        /// <returns>A new instance of the periods with the specified unti periods added.</returns>
        public FedCalendarPeriods AddUnitPeriods(int extraUnitPeriods)
        {
            return new FedCalendarPeriods(this.UnitPeriods + extraUnitPeriods, this.FractionalPeriods, this.FractionsPerUnitPeriod, this.HasAdditionalOddDays);
        }

        /// <summary>
        /// Adds the specified number of fractional periods to the existing periods,
        /// increasing or decreasing the unit periods as needed to deal with overflow
        /// or underflow.
        /// </summary>
        /// <param name="extraFractionalPeriods">The number of fractional periods to add.</param>
        /// <returns>A new instance of the periods with the specified fractional periods added.</returns>
        public FedCalendarPeriods AddFractionalPeriods(int extraFractionalPeriods)
        {
            return new FedCalendarPeriods(this.UnitPeriods, this.FractionalPeriods + extraFractionalPeriods, this.FractionsPerUnitPeriod, this.HasAdditionalOddDays);
        }

        /// <summary>
        /// Adds the specified unit and fractional periods to the existing periods,
        /// accounting for overflow or underflow in the fractional periods.
        /// </summary>
        /// <param name="periods">The periods (with the same number of fractions per unit) to add.</param>
        /// <returns>A new instance of the periods with the specified periods added.</returns>
        public FedCalendarPeriods AddFedCalendarPeriods(FedCalendarPeriods periods)
        {
            if (this.FractionsPerUnitPeriod != periods.FractionsPerUnitPeriod)
            {
                throw new ArgumentException("Cannot add FedCalendarPeriods that have different fractions per unit.");
            }

            return this.AddFractionalPeriods((periods.UnitPeriods * this.FractionsPerUnitPeriod) + periods.FractionalPeriods);
        }

        /// <summary>
        /// Halves the specified periods, dealing with half unit periods and any
        /// resulting overflow/underflow in the fractional periods.
        /// </summary>
        /// <returns>A new instance of the result of dividing the periods in half.</returns>
        public FedCalendarPeriods HalvePeriods()
        {
            int halfUnits = this.UnitPeriods / 2;
            int halfFractions = (GaussianMod(this.UnitPeriods, 2) * this.FractionsPerUnitPeriod / 2) + ((this.FractionalPeriods + 1) / 2);

            return new FedCalendarPeriods(halfUnits, halfFractions, this.FractionsPerUnitPeriod, this.HasAdditionalOddDays);
        }
    }
}
