﻿namespace DataAccess
{
    /// <summary>
    /// If you add a new filetype here, ensure you update the referenced switch statements, and procedures e.g. [tablename]_ExistsWithName,
    /// [tablename]_GetAll, [tablename]_Add
    /// These could all go in one file table, but opm 463659 only requires two file types so I've postponed that abstraction.
    /// </summary>
    public enum SharedDbBackedFileType
    {
        /// <summary>
        /// LoXml files are xml files that are used by the LOFormatExporter.  They describe the desired loan and app-level fields.
        /// </summary>
        LoXml = 0,

        /// <summary>
        /// Xsl files are files that transform other files, e.g. xml with loan data to xml formatted for a vendor request.
        /// </summary>
        Xsl = 1
    }
}
