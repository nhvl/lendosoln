/// Author: David Dao

using System;

namespace DataAccess
{
    public enum FormatDirection
    {
        ToDb = 0,
        ToRep = 1,
        ToXmlFieldDb = 2
    }
}
