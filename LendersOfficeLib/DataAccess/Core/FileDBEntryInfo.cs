﻿// <copyright file="FileDBEntryInfo.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Scott Kibler
//    Date:   8/19/2014
// </summary>
namespace DataAccess
{
    using System;
    
    /// <summary>
    /// A minimum amount needed to fully specify where the file db entry is stored. <para></para>
    /// That is the filedb and the key.  That's it.
    /// </summary>
    public class FileDBEntryInfo
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FileDBEntryInfo" /> class. <para></para>
        /// </summary>
        /// <param name="fileDBType">The type of FileDB the file would be.</param>
        /// <param name="key">Cannot be null or empty.</param>
        public FileDBEntryInfo(E_FileDB fileDBType, string key)
        {
            if (string.IsNullOrEmpty(key))
            {
                throw new ArgumentException("key cannot be null or empty.");
            }

            this.FileDBType = fileDBType;
            this.Key = key;
        }

        /// <summary>
        /// Gets the type of fileDB that the file belongs to.
        /// </summary>
        /// <value>The type of fileDB as an enum.</value>
        public E_FileDB FileDBType { get; private set; }

        /// <summary>
        /// Gets the value of the key where the file is.
        /// </summary>
        /// <value>The key to the file as a string.</value>
        public string Key { get; private set; }

        /// <summary>
        /// A string representation of the file location.
        /// </summary>
        /// <returns>The string representation of the file location.</returns>
        public override string ToString()
        {
            return "FileDB type " + this.FileDBType.ToString("g") + " key " + this.Key;
        }

        /// <summary>
        /// Deep equality.
        /// </summary>
        /// <param name="obj">The other object we compare to.</param>
        /// <returns>True iff the other object is deeply equal.</returns>
        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }

            if (!(obj is FileDBEntryInfo))
            {
                return false;
            }

            return this.Equals((FileDBEntryInfo)obj);
        }

        /// <summary>
        /// Deep equality.
        /// </summary>
        /// <param name="other">The other object we compare to.</param>
        /// <returns>True iff the other object is deeply equal.</returns>
        public bool Equals(FileDBEntryInfo other)
        {
            if (other == null)
            {
                return false;
            }

            return
                this.FileDBType == other.FileDBType &&
                this.Key == other.Key;
        }

        /// <summary>
        /// A hashcode for this instance.
        /// </summary>
        /// <returns>A hashcode using tuple's GetHashCode method.</returns>
        public override int GetHashCode()
        {
            return Tuple.Create(this.FileDBType, this.Key).GetHashCode();
        }
    }
}
