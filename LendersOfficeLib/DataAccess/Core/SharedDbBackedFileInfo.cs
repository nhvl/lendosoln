﻿namespace DataAccess
{
    /// <summary>
    /// A class to store the basic info about a file that's been backed by our shared db.
    /// </summary>
    public class SharedDbBackedFileInfo
    {
        /// <summary>
        /// Gets or sets the type of file that's been backed.
        /// </summary>
        /// <value>The type of the file.</value>
        public SharedDbBackedFileType FileType { get; set; }

        /// <summary>
        /// Gets or sets the id of file that's been backed.
        /// </summary>
        /// <value>The id of the file.</value>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the name of file that's been backed.
        /// </summary>
        /// <value>The name of the file.</value>
        public string FileName { get; set; }
    }
}
