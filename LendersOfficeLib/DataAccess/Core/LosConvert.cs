/// Author: David Dao

using System;
using System.Globalization;
using System.Text.RegularExpressions;
using LendersOffice.Common;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using LqbGrammar.DataTypes;

namespace DataAccess
{
    public class LosConvert : ILosConvert
    {
        /// <summary>
        /// Represents the string used to report "Not Applicable" to HMDA.
        /// </summary>
        public const string HmdaNotApplicableString = "NA";

        /// <summary>
        /// Represents the code used by HMDA to represent "Not Applicable" for certain count fields.
        /// </summary>
        public const int HmdaNotApplicableCode = 8888;

        /// <summary>
        /// Represents the code used by HMDA to indicate that a loan has no coapplicant.
        /// </summary>
        public const int HmdaNoCoapplicantCode = 9999;

        /// <summary>
        /// Represents the date used to report "Not Applicable" to HMDA.
        /// </summary>
        public static readonly DateTime HmdaNotApplicableDateTime = new DateTime(1901, 1, 1);

        private TargetNumberFormatInfo targetNumberFormat;

        private FormatTarget m_formatTarget;

        /// <summary>
        /// Preferred method for getting hold of an instance of LosConvert.
        /// </summary>
        /// <remarks>
        /// The existing public constructors should be marked as private and all 
        /// code that calls them should be converted to using this factory method
        /// instead.  In the future we can then swap out a better implementation
        /// of the ILosConvert interface.
        /// </remarks>
        /// <param name="formatTarget">The target for the formatting/parsing.</param>
        /// <returns>An implementation of the ILosConvert interface.</returns>
        internal static ILosConvert Create(FormatTarget? formatTarget)
        {
            return formatTarget == null ? new LosConvert() : new LosConvert(formatTarget.Value);
        }

        public LosConvert() : this(FormatTarget.Webform)
        {
        }

        public LosConvert(FormatTarget formatTarget)
        {
            m_formatTarget = formatTarget;
            targetNumberFormat = TargetNumberFormatInfo.Create(formatTarget);
        }
        
        public FormatTarget FormatTargetCurrent
        {
            get { return this.m_formatTarget; }
        }

        #region InvalidStringProperties

        public string InvalidRateString
        {
            get
            {
                // 5/18/2009 dd - This method return when a string representation and a rate field throw exception in calculation.
                return string.Empty; // 5/18/2009 dd - We may allow user to set different value in the future.
            }
        }
        public string InvalidMoneyString
        {
            get
            {
                // 5/18/2009 dd - This method return when a string representation and a money field throw exception in calculation.
                return string.Empty; // 5/18/2009 dd - We may allow user to set different value in the future.
            }
        }
        public string InvalidDecimalString
        {
            get
            {
                return string.Empty;
            }
        }
        public string InvalidCountString
        {
            get
            {
                return string.Empty;
            }
        }
        public string InvalidDateString
        {
            get
            {
                return string.Empty;
            }
        }

		#endregion InvalidStringProperties

        #region SsnPhone

        public string ToSsnFormat( string ssnStrVal, FormatDirection direction )
        {
            string ssn = ssnStrVal;
            if( null == ssn )
            {
                ssn = "";
            } 
            else 
            {
                ssn = ssn.Replace(" ", "");
            }

            string result = ssn;
            switch( direction )
            {
                case FormatDirection.ToDb:
                {
                    ssn = ssn.Replace( "-", "" );
                    ssn = ssn.Replace( " ", "" );
                    if (ssn.Length == 9) 
                    {
                        result = ssn.Substring(0, 3) + "-" + ssn.Substring(3, 2) + "-" + ssn.Substring(5);
                    }
                    break;
                }
                case FormatDirection.ToRep:
                {
                    switch( m_formatTarget )
                    {
                        case FormatTarget.FannieMaeMornetPlus:
                        case FormatTarget.FreddieMacAUS:
                        case FormatTarget.LON_Mismo:
                        case FormatTarget.DocMagic:
                        case FormatTarget.MismoClosing:
                        case FormatTarget.GinnieNet:
                            result = ssn.TrimWhitespaceAndBOM().Replace( "-", "" );
                            break;
                    }
                    break;
                }
            }
            return result;
        }

        public string ToPhoneNumFormat( string phoneNumberStrVal, FormatDirection direction )
        {
            string phoneNumber = phoneNumberStrVal;
            if( null == phoneNumber )
            {
                phoneNumber = "";
            }

            string result = phoneNumber;
            switch( direction )
            {
                case FormatDirection.ToDb:
                {
                    phoneNumber = phoneNumber.Replace( ")", "" );
                    phoneNumber = phoneNumber.Replace( "(", "" );
                    phoneNumber = phoneNumber.Replace( "-", "" );
                    phoneNumber = phoneNumber.Replace( " ", "" );
                    if(phoneNumber.Length >= 10) 
                    {
                        result = "(" + phoneNumber.Substring(0, 3) + ") " + phoneNumber.Substring(3, 3) + "-" + phoneNumber.Substring(6);
                    }
                    break;
                }
                case FormatDirection.ToRep:
                {
                    switch( m_formatTarget )
                    {
                        case FormatTarget.FannieMaeMornetPlus:
                        case FormatTarget.FreddieMacAUS:
                        case FormatTarget.LON_Mismo:
                            result = Regex.Replace(phoneNumber.TrimWhitespaceAndBOM(), @"\D", string.Empty);
                            result = result.Substring( 0, Math.Min( 10, result.Length ) );
                            break;
                            // 4/24/2007 nw - OPM 12658 - Point Export should use format "111-111-1111"
                        case FormatTarget.PointNative:
                            phoneNumber = phoneNumber.Replace( ")", "" );
                            phoneNumber = phoneNumber.Replace( "(", "" );
                            phoneNumber = phoneNumber.Replace( "-", "" );
                            phoneNumber = phoneNumber.Replace( " ", "" );
                            if(phoneNumber.Length >= 10) 
                            {
                                result = phoneNumber.Substring(0, 3) + "-" + phoneNumber.Substring(3, 3) + "-" + phoneNumber.Substring(6);
                            }
                            break;
                    }
                    break;
                }
            }
            return result;
        }

        #endregion SsnPhone

        #region DateTime

        private static CultureInfo s_cultureInfo = new CultureInfo(1033);

        private static string[] s_backupDateTimeFormats = new string[] {
                                                                           @"MM/dd/yyyy",   @"MM-dd-yyyy",  @"MM.dd.yyyy",  @"MM\dd\yyyy",
                                                                           @"MM/dd/yy", @"MM-dd-yy",    @"MM.dd.yy",    @"MM\dd\yy",
                                                                           @"MM/d/yyyy",    @"MM-d-yyyy",   @"MM.d.yyyy",   @"MM\d\yyyy",
                                                                           @"MM/d/yy",      @"MM-d-yy",     @"MM.d.yy",     @"MM\d\yy",
                                                                           @"MM/yyyy",      @"MM-yyyy",     @"MM.yyyy",     @"MM\yyyy",
                                                                           @"MM/yy",        @"MM-yy",       @"MM.yy",       @"MM\yy",
                                                                           @"M/dd/yyyy",    @"M-dd-yyyy",   @"M.dd.yyyy",   @"M\dd\yyyy",
                                                                           @"M/dd/yy",      @"M-dd-yy",     @"M.dd.yy",     @"M\dd\yy",
                                                                           @"M/d/yyyy", @"M-d-yyyy",    @"M.d.yyyy",    @"M\d\yyyy",
                                                                           @"M/d/yy",       @"M-d-yy",      @"M.d.yy",      @"M\d\yy",
                                                                           @"M/yyyy",       @"M-yyyy",      @"M.yyyy",      @"M\yyyy",
                                                                           @"M/yy",     @"M-yy",        @"M.yy",        @"M\yy",
                                                                           @"yyyy-MM-dd"};

        public delegate DateTime DelegateDateValue();

        public static string PeriodLength(int months)
        {
            StringBuilder sb = new StringBuilder(40);

            if (months > 11)
            {
                sb.Append(months / 12 + " year");
                if (months > 23)
                {
                    sb.Append("s");
                }
            }
            if (months % 12 != 0)
            {
                if (months > 11)
                {
                    sb.Append(" and ");
                }
                sb.Append(months % 12 + " month");
                if (months % 12 != 1)
                {
                    sb.Append("s");
                }
            }

            return sb.ToString();
        }

        public static string PeriodLengthInMonthsOrYearsOnly(int months)
        {
            StringBuilder sb = new StringBuilder(40);

            if (months % 12 == 0)
            {
                sb.Append(months / 12 + " year");
                if (months > 23)
                {
                    sb.Append("s");
                }
            }
            else
            {
                sb.Append(months + " month");
                if (months != 1)
                {
                    sb.Append("s");
                }
            }

            return sb.ToString();
        }

        public string ConvertYearToMonth(string yrs)
        {
            decimal value;
            if (!decimal.TryParse(yrs, out value))
            {
                value = 0;
            }
            int months = Convert.ToInt32(value * 12);
            return ToCountString(months);
        }

        public string ConvertMonthToYear(string months)
        {
            return this.ConvertMonthToYearToAccuracy(months, 2);
        }

        public string ConvertMonthToYearToAccuracy(string months, int digits)
        {
            int value;
            if (!int.TryParse(months, out value))
            {
                value = 0;
            }
            decimal years = (decimal)value / 12M;
            return Math.Round(years, digits).ToString();
        }

        public string ToDateTimeString(CDateTime dt)
        {
            return dt.ToString(this);
        }

        public string ToDateTimeString(DateTime dtSrc, bool displayTime = false)
        {
            try
            {
                if (0 == dtSrc.CompareTo(CDateTime.InvalidDateTime))
                {
                    return "";
                }
                else if (dtSrc.Year == 1900 && dtSrc.Month == 1 && dtSrc.Day == 1)
                {
                    return "";
                }
                else if (dtSrc.CompareTo(DateTime.MinValue) == 0)
                {
                    return "";
                }

                switch( m_formatTarget )
                {
                    // kep this in sync with CDateTime(string dateTime, LosConvert losConvert) constructor
                    case FormatTarget.FannieMaeMornetPlus:
                    case FormatTarget.FreddieMacAUS:
                    case FormatTarget.GinnieNet:
                        return dtSrc.ToString( "yyyyMMdd" );//If this doesn't work, try "%yyyy%MM%dd"
                    case FormatTarget.LON_Mismo:
                    case FormatTarget.MismoClosing:
                        if (displayTime)
                            return dtSrc.ToString("yyyy-MM-ddTHH:mm:sszzz");
                        else
                            return dtSrc.ToString("yyyy-MM-dd");
                    case FormatTarget.DocMagic:
                        return dtSrc.ToString("yyyy/MM/dd");
                    case FormatTarget.DocuTech:
                    case FormatTarget.XsltExport:
                        return dtSrc.ToString("MM/dd/yyyy");


                }
                return displayTime ? dtSrc.ToString() : dtSrc.ToShortDateString();
            }
            catch( Exception ex )
            {
                LogError( "???", "DateTime", "string", ex.StackTrace );
                return "";
            }
        }

        /// <summary>
        /// Takes a DateTimeOffset structure and returns its string representation
        /// </summary>
        /// <param name="dtoSrc">The DateTimeOffset stucture.</param>
        /// <param name="displayTime">Boolean indicating whether to display time in string representation.</param>
        /// <returns>String representation of DateTimeOffset structure.</returns>
        public string ToDateTimeString(DateTimeOffset dtoSrc, bool displayTime)
        {
            try
            {
                if (0 == dtoSrc.DateTime.CompareTo(CDateTime.InvalidDateTime))
                {
                    return "";
                }
                else if (dtoSrc.Year == 1900 && dtoSrc.Month == 1 && dtoSrc.Day == 1)
                {
                    return "";
                }
                else if (dtoSrc.CompareTo(DateTimeOffset.MinValue) == 0)
                {
                    return "";
                }

                switch (m_formatTarget)
                {
                    // kep this in sync with CDateTime(string dateTime, LosConvert losConvert) constructor
                    case FormatTarget.FannieMaeMornetPlus:
                    case FormatTarget.FreddieMacAUS:
                    case FormatTarget.GinnieNet:
                        return dtoSrc.ToString("yyyyMMdd");//If this doesn't work, try "%yyyy%MM%dd"
                    case FormatTarget.LON_Mismo:
                    case FormatTarget.MismoClosing:
                        if (displayTime)
                            return dtoSrc.ToString("yyyy-MM-ddTHH:mm:sszzz");
                        else
                            return dtoSrc.ToString("yyyy-MM-dd");
                    case FormatTarget.DocMagic:
                        return dtoSrc.ToString("yyyy/MM/dd");
                    case FormatTarget.DocuTech:
                    case FormatTarget.XsltExport:
                        return dtoSrc.ToString("MM/dd/yyyy");


                }
                return dtoSrc.DateTime.ToShortDateString();
            }
            catch (Exception ex)
            {
                LogError("???", "DateTimeOffset", "string", ex.StackTrace);
                return "";
            }
        }

        public string ToDateTimeString(DelegateDateValue value)
        {
            return ToDateTimeString(value, false);
        }

        public string ToDateTimeString(DelegateDateValue value, bool displayTime)
        {
            try
            {
                return ToDateTimeString(value.Invoke(), displayTime);
            }
            catch (PermissionException)
            {
                throw;
            }
            catch
            {
                return InvalidDateString;
            }
        }        

        /// <summary>
        /// Try to return formated datetime. If it's not a valid date, it will return the raw string value.
        /// </summary>
        /// <param name="dtSrc"></param>
        /// <returns></returns>
        public string ToDateTimeVarCharString( string dtSrc )
        {

            try
            {
                if( "" == dtSrc || null == dtSrc)
                    return "";

                DateTime dt = DateTime.ParseExact( dtSrc, s_backupDateTimeFormats,s_cultureInfo, 
                    DateTimeStyles.AllowInnerWhite | DateTimeStyles.AllowLeadingWhite | DateTimeStyles.AllowTrailingWhite
                    | DateTimeStyles.AllowWhiteSpaces | DateTimeStyles.NoCurrentDateDefault );

                if( 0 == dt.CompareTo( CDateTime.InvalidDateTime ) )
                    return "";

                switch( m_formatTarget )
                {
                    case FormatTarget.FannieMaeMornetPlus:
                    case FormatTarget.FreddieMacAUS:
                    case FormatTarget.GinnieNet:
                        return dt.ToString( "yyyyMMdd" );//If this doesn't work, try "%yyyy%MM%dd"
                    case FormatTarget.LON_Mismo:
                    case FormatTarget.MismoClosing:
                        return dt.ToString( "yyyy-MM-dd" );
                    case FormatTarget.DocuTech:
                        return dt.ToString("MM/dd/yyyy");
                    default:
                        return dt.ToShortDateString();
                }		
            }
            catch
            {
                switch( m_formatTarget )
                {
                    case FormatTarget.FannieMaeMornetPlus:
                    case FormatTarget.FreddieMacAUS:
                    case FormatTarget.LON_Mismo:
                    case FormatTarget.MismoClosing:
                    case FormatTarget.XsltExport:
                    case FormatTarget.GinnieNet:
                        return "";
                    default:
                        return dtSrc;
                }
            }
        }

        public string ToDateTimeString_TimeOnly(DateTime dtSrc)
        {
            try
            {
                if (0 == dtSrc.CompareTo(CDateTime.InvalidDateTime))
                {
                    return "";
                }
                else if (dtSrc.Year == 1900 && dtSrc.Month == 1 && dtSrc.Day == 1)
                {
                    return "";
                }
                else if (dtSrc.CompareTo(DateTime.MinValue) == 0)
                {
                    return "";
                }

                return Tools.GetTimeDescription(dtSrc); // 00:00 -> 12:00 AM PST / PDT
            }
            catch (Exception ex)
            {
                LogError("???", "DateTime", "string", ex.StackTrace);
                return "";
            }
        }

        #endregion DateTime

        #region Money

        public string ToMoneyString( decimal money, FormatDirection direction )
        {
            return ToMoneyString(money, direction, targetNumberFormat.FormatMoney, MidpointRounding.ToEven);
        }

        public string ToMoneyString4DecimalDigits(decimal money, FormatDirection direction)
        {
            return ToMoneyString(money, direction, targetNumberFormat.FormatMoney4DecimalDigits, MidpointRounding.ToEven);
        }

        public string ToMoneyString6DecimalDigits(decimal money, FormatDirection direction)
        {
            return ToMoneyString(money, direction, targetNumberFormat.FormatMoney6DecimalDigits, MidpointRounding.ToEven);
        }

        public string ToMoneyStringRoundAwayFromZero(decimal money, FormatDirection direction)
        {
            return ToMoneyString(money, direction, targetNumberFormat.FormatMoney, MidpointRounding.AwayFromZero);
        }

        private string ToMoneyString(decimal money, FormatDirection direction, NumberFormatInfo formatInfo, MidpointRounding midpointRounding)
        {
            try
            {
                switch (direction)
                {
                    case FormatDirection.ToRep:
                        if (0 == money)
                        {
                            switch (m_formatTarget)
                            {
                                case FormatTarget.PrintoutNormalFields: // fall throuh
                                case FormatTarget.FannieMaeMornetPlus:
                                case FormatTarget.PointNative:
                                    return "";
                            }
                        }
                        if (formatInfo.CurrencyDecimalDigits > 0)
                        {
                            // 2/4/2011 dd - Perform banker rounding for money _rep fields.
                            money = decimal.Round(money, formatInfo.CurrencyDecimalDigits, midpointRounding);
                        }
                        string s = money.ToString("C", formatInfo);
                        if (FormatTarget.FannieMaeMornetPlus == m_formatTarget)
                        {
                            const string fannieMoneyFormat = "ZZZZZZZZZZZZ.ZZ";
                            s = s.PadLeft(fannieMoneyFormat.Length, ' ');
                        }
                        return s;
                    case FormatDirection.ToXmlFieldDb:
                        if (0 == money)
                            return "";
                        return money.ToString("C", formatInfo);
                    default:// fall-through
                    case FormatDirection.ToDb: // Shouldn't be happening, we don't store money string in db.
                        LogError(money.ToString(), "money", "string", "LosConvert.ToMoneyString, Formatdirection ToDB");
                        return money.ToString("C", formatInfo);

                }
            }
            catch (Exception ex)
            {
                LogError(money.ToString(), "decimal(money)", "string", ex.StackTrace);
                return "";
            }
        }

        public string ToMoneyVarCharStringRep(string rawVal)
        {
            decimal money = 0;
            try
            {
                money = ToMoney(rawVal, false);
            }
            catch
            {
                switch (m_formatTarget)
                {
                    default:
                        LogError("Encounter unhandled FormatTarget in LosConvert.ToMoneyVarCharStringRep()");
                        return "";
                    case FormatTarget.PriceEngineEval:
                    case FormatTarget.FannieMaeMornetPlus:
                    case FormatTarget.FreddieMacAUS:
                    case FormatTarget.LON_Mismo:
                    case FormatTarget.MismoClosing:
                    case FormatTarget.GinnieNet:
                    case FormatTarget.XsltExport:
                        return "";
                    case FormatTarget.PointNative:
                    case FormatTarget.PrintoutImportantFields:
                    case FormatTarget.PrintoutNormalFields:
                    case FormatTarget.Webform:
                    case FormatTarget.ReportResult:
                    case FormatTarget.FannieMaeMornetPlusImportantFields:
                    case FormatTarget.LqbDataService:
                        return rawVal;
                }
            }
            return ToMoneyString(money, FormatDirection.ToRep);
        }

        /// <summary>
        /// Convert a money string to a decimal.
        /// If encounter exception, suppress it and return 0.
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public decimal ToMoney(string str)
        {
            return ToMoney(str, true);
        }

        private decimal ToMoney(string str, bool bSuppressException)
        {
            try
            {
                if (null == str || str.Length == 0)
                    return 0;

                // 8/11/2015 - For Performance.
                if (str == "$0.00")
                {
                    return 0;
                }

                return Decimal.Parse(str, NumberStyles.Currency);
            }
            catch (Exception exc)
            {
                if (bSuppressException)
                    return 0;

                throw exc;
            }
        }

        #endregion Money

        #region Decimal

        /// <summary>
        /// This function will remove any character that are not 0-9, '.', or ','
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public string SanitizedDecimalString(string str)
        {
            if (string.IsNullOrEmpty(str))
                return string.Empty;

            char[] buffer = new char[str.Length];
            int index = 0;
            foreach (char ch in str)
            {
                if (char.IsDigit(ch) || ch == '.' || ch == ',' || ch == '(' || ch == ')' || ch == '-')
                {
                    buffer[index++] = ch;
                }
            }
            return new string(buffer, 0, index);
        }

        public string ToDecimalString_sSpCuScore(decimal value)
        {
            if (value == 0m)
            {
                // If there is no score, it should not be displayed since 0.0 is not a valid value.
                return string.Empty;
            }
            else if (value == 999m)
            {
                // If a score could not be calculated, the value will be 999.
                return "999";
            }
            else
            {
                // Any other score should be output to one decimal place.
                return value.ToString("#.#");
            }
        }

        public string ToDecimalString(decimal val, FormatDirection direction)
        {
            try
            {
                if (0 == val)
                {
                    switch (m_formatTarget)
                    {
                        case FormatTarget.PrintoutNormalFields:
                        case FormatTarget.FannieMaeMornetPlus:
                        case FormatTarget.FreddieMacAUS:
                        case FormatTarget.LON_Mismo:
                        case FormatTarget.MismoClosing:
                        case FormatTarget.PointNative:
                            return "";
                        case FormatTarget.XsltExport:
                            return "0";
                    }
                }

                if (targetNumberFormat.FormatRate.NumberDecimalDigits > 0)
                    val = Math.Round(val, targetNumberFormat.FormatDecimal.NumberDecimalDigits);

                string s = val.ToString("F", targetNumberFormat.FormatDecimal);
                if (FormatTarget.FannieMaeMornetPlus == m_formatTarget)
                {
                    const string fannieRateFormat = "ZZZ.ZZZ";
                    s = s.PadLeft(fannieRateFormat.Length, ' ');
                }
                return s;
            }
            catch (Exception ex)
            {
                LogError(val.ToString(), "decimal", "string", ex.StackTrace);
                return "";
            }
        }

        /// <summary>
        /// Convert a decimal string into a decimal.
        /// For non-money, non-percent decimals, such as measurements.
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public decimal ToDecimal(string str)
        {
            if (string.IsNullOrEmpty(str))
                return 0.0M;

            decimal ret;

            if (!decimal.TryParse(SanitizedDecimalString(str), out ret))
            {
                ret = 0.0M;
            }
            return ret;

        }

        #endregion Decimal

        #region Float
        /// <summary>
        /// Format a float value.
        /// </summary>
        /// <param name="g">The float value.</param>
        /// <returns>The formatted float value.</returns>
        public string ToFloatString(float f)
        {
            return f.ToString();
        }
        #endregion

        #region Rates

        public string ToRateStringNoPercent(decimal rate)
        {
            try
            {
                if( 0 == rate )
                {
                    switch( m_formatTarget )
                    {
                        case FormatTarget.PrintoutNormalFields:
                        case FormatTarget.FannieMaeMornetPlus:
                        case FormatTarget.FreddieMacAUS:
                        case FormatTarget.LON_Mismo:
                        case FormatTarget.MismoClosing:
                        case FormatTarget.PointNative:
                            return "";
                        case FormatTarget.XsltExport:
                            return "0";
                    }
                }

                if(targetNumberFormat.FormatRate.NumberDecimalDigits > 0 )
                    rate = Math.Round( rate, targetNumberFormat.FormatRate.NumberDecimalDigits );
                string s = rate.ToString( "F", targetNumberFormat.FormatRate);
                if( FormatTarget.FannieMaeMornetPlus == m_formatTarget )
                {
                    const string fannieRateFormat = "ZZZ.ZZZ";
                    s = s.PadLeft( fannieRateFormat.Length, ' ' );
                }
                return s;
            }
            catch (Exception ex)
            {
                LogError(rate.ToString(), "decimal(rate)", "string", ex.StackTrace);
                return "";
            }
        }

        public string ToRateString(decimal? rate)
        {
            if (rate.HasValue) return ToRateString(rate.Value);
            return "";
        }

        public string ToRateString( decimal rate )
        {
            try
            {
                if( 0 == rate )
                {
                    switch( m_formatTarget )
                    {
                        case FormatTarget.PrintoutNormalFields:
                        case FormatTarget.FannieMaeMornetPlus:
                        case FormatTarget.FreddieMacAUS:
                        case FormatTarget.LON_Mismo:
                        case FormatTarget.MismoClosing:
                        case FormatTarget.PointNative:
                            return "";
                        case FormatTarget.XsltExport:
                            return "0";
                    }
                }

                if(targetNumberFormat.FormatRate.NumberDecimalDigits > 0 )
                    rate = Math.Round( rate, targetNumberFormat.FormatRate.NumberDecimalDigits );
                string s = rate.ToString( "F", targetNumberFormat.FormatRate);
                if( FormatTarget.FannieMaeMornetPlus == m_formatTarget )
                {
                    const string fannieRateFormat = "ZZZ.ZZZ";
                    s = s.PadLeft( fannieRateFormat.Length, ' ' );
                } 
                else if (FormatTarget.Webform == m_formatTarget || this.m_formatTarget == FormatTarget.LqbDataService) 
                {
                    // Append '%' at the end. dd 6/17/2003
                    s = s + "%";
                }
                return s;
            }
            catch( Exception ex )
            {
                LogError( rate.ToString(), "decimal(rate)", "string", ex.StackTrace );
                return "";
            }
        }

        public string ToRateString4DecimalDigits(decimal rate)
        {
            try
            {
                if (0 == rate)
                {
                    switch (m_formatTarget)
                    {
                        case FormatTarget.PrintoutNormalFields:
                        case FormatTarget.FannieMaeMornetPlus:
                        case FormatTarget.FreddieMacAUS:
                        case FormatTarget.LON_Mismo:
                        case FormatTarget.MismoClosing:
                        case FormatTarget.PointNative:
                            return "";
                        case FormatTarget.XsltExport:
                            return "0";
                    }
                }

                NumberFormatInfo format = targetNumberFormat.FormatRate4DecimalDigits;
                if (m_formatTarget == FormatTarget.FannieMaeMornetPlus)
                {
                    // 7/7/2011 dd - FNMA format only accept 3 decimal digits.
                    format = targetNumberFormat.FormatRate;
                }

                if (format.NumberDecimalDigits > 0)
                {
                    rate = Math.Round(rate, format.NumberDecimalDigits);
                }
                string s = rate.ToString("F", format);
                if (FormatTarget.FannieMaeMornetPlus == m_formatTarget)
                {
                    const string fannieRateFormat = "ZZZ.ZZZ";
                    s = s.PadLeft(fannieRateFormat.Length, ' ');
                }
                else if (FormatTarget.Webform == m_formatTarget || this.m_formatTarget == FormatTarget.LqbDataService)
                {
                    // Append '%' at the end. dd 6/17/2003
                    s = s + "%";
                }
                return s;
            }
            catch (Exception ex)
            {
                LogError(rate.ToString(), "decimal(rate)", "string", ex.StackTrace);
                return "";
            }
        }

        public string ToRateString6DecimalDigits(decimal rate)
        {
            try
            {
                if (0 == rate)
                {
                    switch (m_formatTarget)
                    {
                        case FormatTarget.PrintoutNormalFields:
                        case FormatTarget.FannieMaeMornetPlus:
                        case FormatTarget.FreddieMacAUS:
                        case FormatTarget.LON_Mismo:
                        case FormatTarget.MismoClosing:
                        case FormatTarget.PointNative:
                            return "";
                        case FormatTarget.XsltExport:
                            return "0";
                    }
                }

                NumberFormatInfo format = targetNumberFormat.FormatRate6DecimalDigits;
                if (m_formatTarget == FormatTarget.FannieMaeMornetPlus)
                {
                    // 7/7/2011 dd - FNMA format only accept 3 decimal digits.
                    format = targetNumberFormat.FormatRate;
                }

                if (format.NumberDecimalDigits > 0)
                {
                    rate = Math.Round(rate, format.NumberDecimalDigits);
                }
                string s = rate.ToString("F", format);
                if (FormatTarget.FannieMaeMornetPlus == m_formatTarget)
                {
                    const string fannieRateFormat = "ZZZ.ZZZ";
                    s = s.PadLeft(fannieRateFormat.Length, ' ');
                }
                else if (FormatTarget.Webform == m_formatTarget || this.m_formatTarget == FormatTarget.LqbDataService)
                {
                    // Append '%' at the end. dd 6/17/2003
                    s = s + "%";
                }
                return s;
            }
            catch (Exception ex)
            {
                LogError(rate.ToString(), "decimal(rate)", "string", ex.StackTrace);
                return "";
            }
        }

        public string ToRateString9DecimalDigits(decimal rate)
        {
            try
            {
                if (0 == rate)
                {
                    switch (m_formatTarget)
                    {
                        case FormatTarget.PrintoutNormalFields:
                        case FormatTarget.FannieMaeMornetPlus:
                        case FormatTarget.FreddieMacAUS:
                        case FormatTarget.LON_Mismo:
                        case FormatTarget.MismoClosing:
                        case FormatTarget.PointNative:
                            return "";
                        case FormatTarget.XsltExport:
                            return "0";
                    }
                }

                NumberFormatInfo format = targetNumberFormat.FormatRate9DecimalDigits;
                if (m_formatTarget == FormatTarget.FannieMaeMornetPlus)
                {
                    // 7/7/2011 dd - FNMA format only accept 3 decimal digits.
                    format = targetNumberFormat.FormatRate;
                }

                if (format.NumberDecimalDigits > 0)
                {
                    rate = Math.Round(rate, format.NumberDecimalDigits);
                }
                string s = rate.ToString("F", format);
                if (FormatTarget.FannieMaeMornetPlus == m_formatTarget)
                {
                    const string fannieRateFormat = "ZZZ.ZZZ";
                    s = s.PadLeft(fannieRateFormat.Length, ' ');
                }
                else if (FormatTarget.Webform == m_formatTarget || this.m_formatTarget == FormatTarget.LqbDataService)
                {
                    // Append '%' at the end. dd 6/17/2003
                    s = s + "%";
                }
                return s;
            }
            catch (Exception ex)
            {
                LogError(rate.ToString(), "decimal(rate)", "string", ex.StackTrace);
                return "";
            }
        }

        /// <summary>
        /// Convert a percentage string to a decimal.
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public decimal ToRate(string str)
        {
            if (string.IsNullOrEmpty(str))
            {
                return 0.0M;
            }

            // 8/11/2015 - For Performance.
            if (str == "0.000%")
            {
                return 0.0M;
            }

            decimal ret;

            if (!decimal.TryParse(SanitizedDecimalString(str), out ret))
            {
                ret = 0.0M;
            }
            return ret;

        }

        #endregion Rates

        #region Counts

        public string ToCountStringCreditScore(int value, bool isRunningPricingEngine)
        {
            try
            {
                int score = value;
                if (score < 0)
                    return "";
                if (score == 0)
                {
                    // 11/12/04 (tn): Allow users to do NO FICO with pricing engine
                    if (isRunningPricingEngine)
                        return "0";
                    return "";
                }

                return ToCountString(score);
            }
            catch
            {
                if (isRunningPricingEngine)
                    return "0";
                return "";
            }
        }

        public string ToCountVarCharStringRep(string rawVal)
        {
            int count = 0;
            try
            {
                count = ToCount(rawVal, false);
            }
            catch
            {
                switch (m_formatTarget)
                {
                    default:
                        LogError("Encounter unhandled FormatTarget in LosConvert.ToCountVarCharStringRep()");
                        return "";
                    case FormatTarget.PriceEngineEval:
                    case FormatTarget.FannieMaeMornetPlus:
                    case FormatTarget.FreddieMacAUS:
                    case FormatTarget.LON_Mismo:
                    case FormatTarget.MismoClosing:
                    case FormatTarget.DocuTech:
                    case FormatTarget.XsltExport:
                    case FormatTarget.GinnieNet:
                    case FormatTarget.DocMagic:
                        return "";
                    case FormatTarget.PointNative:
                    case FormatTarget.PrintoutImportantFields:
                    case FormatTarget.FannieMaeMornetPlusImportantFields:
                    case FormatTarget.PrintoutNormalFields:
                    case FormatTarget.Webform:
                    case FormatTarget.ReportResult:
                    case FormatTarget.LqbDataService:
                        return rawVal;
                }
            }
            return ToCountString(count);
        }

        public string ToCountString( int count )
        {
            try
            {
                if( 0 == count )
                {
                    switch( m_formatTarget )
                    {
                        case FormatTarget.PrintoutNormalFields:
                        case FormatTarget.FannieMaeMornetPlus:
                        case FormatTarget.LON_Mismo:
                        case FormatTarget.MismoClosing:
                        case FormatTarget.PointNative:
                            return "";
                        case FormatTarget.FreddieMacAUS:
                        case FormatTarget.XsltExport:
                            return "0";
                    }
                }
                return count.ToString( "F", targetNumberFormat.FormatCount);
            }
            catch( Exception ex )
            {
                LogError( count.ToString(), "int(count)", "string", ex.StackTrace );
                return "";
            }
        }

        public string ToBigCountString( long count )
        {
            try
            {
                if( 0 == count )
                {
                    switch( m_formatTarget )
                    {
                        case FormatTarget.PrintoutNormalFields:
                        case FormatTarget.FannieMaeMornetPlus:
                        case FormatTarget.FreddieMacAUS:
                        case FormatTarget.LON_Mismo:
                        case FormatTarget.MismoClosing:
                        case FormatTarget.PointNative:
                            return "";
                        case FormatTarget.XsltExport:
                            return "0";
                    }
                }
                return count.ToString( "F", targetNumberFormat.FormatCount);
            }
            catch( Exception ex )
            {
                LogError( count.ToString(), "int(count)", "string", ex.StackTrace );
                return "";
            }
        }

        public string ToNullableBigCountString(long? count)
        {
            return count.HasValue ? this.ToBigCountString(count.Value) : string.Empty;
        }

        /// <summary>
        /// </summary>
        /// <param name="str">return 0 if str is empty or not a number</param>
        /// <returns></returns>
        public int ToCount(string str)
        {
            if (string.IsNullOrEmpty(str) || str == "0")
            {
                return 0;
            }

            // 8/11/2015 - For Performance.
            if (str == "1")
            {
                return 1;
            }

            return (int)Math.Round(ToRate(str));
        }

        private int ToCount(string str, bool bSuppressException)
        {
            try
            {
                if (null == str || str.Length == 0)
                    return 0;
                string modStr = SanitizedDecimalString(str);
                decimal rawDecimal = Convert.ToDecimal(modStr);
                return (int)Math.Round(rawDecimal);
            }
            catch (Exception exc)
            {
                if (bSuppressException)
                    return 0;

                throw exc;
            }

        }

        /// <summary>
        /// </summary>
        /// <param name="str">return 0 if str is empty or not a number</param>
        /// <returns></returns>
        public long ToBigCount(string str)
        {
            return (long)Math.Round(ToRate(str));
        }

        #endregion Count

        #region HMDA

        public static bool IsNotApplicableHmdaString(string value, bool preserveNotApplicableCode = false)
        {
            return string.Equals(HmdaNotApplicableString, value, StringComparison.OrdinalIgnoreCase) 
                || (preserveNotApplicableCode && string.Equals(value, HmdaNotApplicableCode.ToString(), StringComparison.Ordinal));
        }

        public string ToStringWithHmdaNa(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return HmdaNotApplicableString;
            }

            return value;
        }

        public string ToDateStringWithHmdaNa(CDateTime value)
        {
            if (value == null || (value.IsValid && value.DateTimeForComputation == HmdaNotApplicableDateTime))
            {
                return HmdaNotApplicableString;
            }

            return value.ToString(this);
        }

        public string ToCountStringWithHmdaNa(int? value, bool preserveNotApplicableCode = false)
        {
            if (!value.HasValue || (!preserveNotApplicableCode && value == HmdaNotApplicableCode))
            {
                return HmdaNotApplicableString;
            }

            return this.ToCountString(value.Value);
        }

        /// <summary>
        /// Assuming -1 to be a sentinel value that says to return blank.
        /// </summary>
        public string ToCountStringWithHmdaNaAndBlank(int? value, bool preserveNotApplicableCode = false)
        {
            if (!value.HasValue || (!preserveNotApplicableCode && value == HmdaNotApplicableCode))
            {
                return HmdaNotApplicableString;
            }

            if (value == int.MinValue)
            {
                return string.Empty;
            }

            return this.ToCountString(value.Value);
        }

        public string ToMoneyStringWithHmdaNa(decimal? value)
        {
            if (!value.HasValue)
            {
                return HmdaNotApplicableString;
            }

            return this.ToMoneyString(value.Value, FormatDirection.ToRep);
        }      

        public string ToMoneyStringWithHmdaNaAndBlank(decimal? value)
        {
            if (!value.HasValue)
            {
                return HmdaNotApplicableString;
            }
            else if (value.Value == 0)
            {
                return string.Empty;
            }
            else
            {
                return this.ToMoneyString(value.Value, FormatDirection.ToRep);
            }
        }

        public string ToRateStringWithHmdaNa(decimal? value)
        {
            if (!value.HasValue)
            {
                return HmdaNotApplicableString;
            }

            return this.ToRateString(value.Value);
        }

        public CDateTime ToDateWithHmdaNa(string value)
        {
            if (IsNotApplicableHmdaString(value))
            {
                return CDateTime.Create(HmdaNotApplicableDateTime);
            }

            return CDateTime.Create(value, this);
        }

        public int? ToCountWithHmdaNa(string value)
        {
            if (IsNotApplicableHmdaString(value))
            {
                return null;
            }

            return this.ToCount(value);
        }

        #endregion HMDA

        #region BitsBoolsTrisGuids

        /// <summary>
        /// Convert a Guid string to a Guid.
        /// If encounter exception, suppress it and return Guid.Empty.
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public Guid ToGuid(string str)
        {
            return ToGuid(str, true);
        }

        private Guid ToGuid(string str, bool bSuppressException)
        {
            try
            {
                if (null == str || str.Length == 0)
                    return Guid.Empty;
                return new Guid(str);
            }
            catch (Exception exc)
            {
                if (bSuppressException)
                    return Guid.Empty;

                throw exc;
            }
        }

        /// <summary>
        /// Format a GUID value.
        /// </summary>
        /// <param name="g">The GUID value.</param>
        /// <returns>The formatted GUID value.</returns>
        public string ToGuidString(Guid g)
        {
            return g.ToString();
        }

        public string ToBitString(bool value)
        {
            if (this.FormatTargetCurrent == FormatTarget.XsltExport)
            {
                return LendersOffice.XsltExportReport.XsltExportWorker.GetBoolString(value);
            }
            else
            {
                return value ? "Yes" : "No";
            }
        }
        public bool ToBit(string str)
        {
            return string.Equals(str, "Yes", StringComparison.OrdinalIgnoreCase)
                || string.Equals(str, "Y", StringComparison.OrdinalIgnoreCase)
                || string.Equals(str, "1", StringComparison.OrdinalIgnoreCase)
                || string.Equals(str, bool.TrueString, StringComparison.OrdinalIgnoreCase);
        }

        public string ToTriString(E_TriState v)
        {
            switch (v)
            {
                case E_TriState.Blank:
                    return string.Empty;
                case E_TriState.Yes:
                    return "Yes";
                case E_TriState.No:
                    return "No";
                default:
                    throw new UnhandledEnumException(v);
            }
        }
        public E_TriState ToTri(string str)
        {
            if (string.IsNullOrEmpty(str))
            {
                return E_TriState.Blank;
            }

            E_TriState tri;
            try
            {
                tri = (E_TriState)Enum.Parse(typeof(E_TriState), str, true);
                if (!Enum.IsDefined(typeof(E_TriState), tri))
                {
                    tri = E_TriState.Blank;
                }
            }
            catch (ArgumentException)
            {
                tri = E_TriState.Blank;
            }

            return tri;
        }

        #endregion BitsBoolsTrisGuids

        #region GfeItemProps

        private const int APR_bit = 1 << 0;
        private const int BF_bit = 1 << 3;
        private const int ToBr_bit = 1 << 4;
        private const int FhaAllow_bit = 1 << 12;

        public const int BORR_PAID_OUTOFPOCKET = 0;
        public const int BORR_PAID_FINANCED = 1;
        public const int SELLER_PAID = 2;
        public const int LENDER_PAID = 3;
        public const int BROKER_PAID = 4;

        /// <summary>
        /// Returns a copy of the settlement props that uses GFE values for values ignored/not used by Settlement Charges
        /// <para>(Currently, APR, BF, ToBroker, FhaAllow)</para>
        /// </summary>
        public static int GfeItemProps_CopyGfeOnlyFieldsFrom(int gfe, int settlement)
        {
            return GfeItemProps_CopyFrom(gfe, settlement, APR_bit | BF_bit | ToBr_bit | FhaAllow_bit);
        }

        private static int GfeItemProps_CopyFrom(int gfe, int settlement, int propsToCopyOver)
        {
            settlement &= ~propsToCopyOver;    // effectively clears the props we're copying (who knows what the settlement page assumed)
            settlement |= gfe & propsToCopyOver;    // gfe & propsToCopyOver selects only the props that we want from the gfe
            return settlement;
        }

        public static bool GfeItemProps_Apr( int props )
        {
            return ( props & 0x0001 ) != 0;
        }

        public static bool GfeItemProps_ToBr( int props )
        {
            return ( props & 0x0010 ) != 0;
        }

        public static bool GfeItemProps_AprAndPdByBorrower(int props)
        {
            // 11/20/2009 dd - This is equivalent to GfeItemProps_Apr(props) && GfeItemProps_Payer(props) <= 1
            return ((props & 0x0001) != 0) && (((props & 0x0F00) >> 8) <= 1);
        }

        /// <summary>
        /// 0: Paid by borrower, out of pocket.
        /// 1: Paid by borrower but he/she will finance it.
        /// 2: Paid by seller
        /// 3: Paid by lender
        /// 4: Paid by broker
        /// </summary>
        public static int GfeItemProps_Payer( int props )
        {
            return ( props & 0x0F00 ) >> 8;
        }

        public static int GfeItemProps_UpdatePayer(int props, int newPayer)
        {
            if (newPayer < 0 || newPayer > 15)
            {
                throw new CBaseException(ErrorMessages.Generic, "Payer is out of range [Expect Payer to be 0-15]. Actual Payer is " + newPayer);
            }

            int mask = ~0x0F00;    //fills an int with all 1s except for 9-12
            var result = props & mask;   //blank out bits 9-12  (this works because 9-12 are zero so they will be zero in the new number the other bits will be whatever they were before)
            result |= (newPayer & 0xF) << 8;  //clear the other bits (may not be necessary) then shift over the payer so it takes bits 9-12 and or it with the last result.
            return result;
        }

        public static bool GfeItemProps_ThisPartyIsAffiliate(int props)
        {
            if (GfeItemProps_PaidToThirdParty(props))
            {
                return (props & 0x4000) != 0;
            }
            else
            {
                return false;
            }
        }

        public static int GfeItemProps_UpdateThisPartyIsAffiliate(int props, bool isThirdPartyAffiliate)
        {
            int mask = ~0x4000;
            var result = props & mask;
            int bit = isThirdPartyAffiliate ? 0x4000 : 0;
            return result | bit;
        }

        public static bool GfeItemProps_PaidToThirdParty(int props)
        {
            return (props & 0x8000) != 0;
        }

        public static int GfeItemProps_UpdatePaidToThirdParty(int props, bool isPaidToThirdParty)
        {
            int mask = ~0x8000;
            var result = props & mask;
            int bit = isPaidToThirdParty ? 0x8000 : 0;
            return result | bit;
        }

        public static bool GfeItemProps_ShowQmWarning(int props)
        {
            return (props & 0x0040) != 0;
        }

        public static int GfeItemProps_UpdateShowQmWarning(int props, bool isShowQmWarning)
        {
            int mask = ~0x0040;
            var result = props & mask;
            int bit = isShowQmWarning ? 0x0040 : 0;
            return result | bit;
        }

        public static int GfeItemProps_UpdateToBr(int props, bool newToBr)
        {
            int mask = ~0x0010;    // fills an int with all 1s except for 5
            var result = props & mask;   // blank out bit 5
            int newToBrInt = newToBr ? 1 : 0; // convert bool to int
            result |= (newToBrInt & 0x1) << 4;
            return result;
        }

        public static int GfeItemProps_UpdateApr(int props, bool newApr)
        {
            int mask = ~0x0001;
            var result = props & mask;
            int newAprInt = newApr ? 1 : 0;
            result |= newAprInt;
            return result;
        }

        // 10/16/2009 dd - Return true if mark as "Borr Pd"
        public static bool IsBorrowerPaid(int props)
        {
            int i = GfeItemProps_Payer(props);
            return i == 0 || i == 1; // 10/16/2009 dd - We are treating borrower finance the same as borrower pd.
        }

        public static bool GfeItemProps_FhaAllow( int props )
        {
            return ( props & 0x1000 ) != 0;
        }

        public static int GfeItemProps_UpdateFhaAllow(int props, bool newFhaAllow)
        {
            int mask = ~0x1000;
            var result = props & mask;
            int fhaAllow = newFhaAllow ? 0x1000 : 0;
            result |= fhaAllow;
            return result;
        }

        public static bool GfeItemProps_Poc( int props )
        {
            return ( props & 0x0002 ) != 0;
        }
        public static bool GfeItemProps_Borr(int props)
        {
            return ( props & 0x0020 ) != 0; // this is at the 5th bit, so it's 32 in HEX.
        }


        public static int GfeItemProps_UpdatePoc(int props, bool poc)
        {
            int mask = ~0x0002;
            var result = props & mask;
            int newPoc = poc ? 0x0002 : 0;
            result |= newPoc;
            return result;
        }

        public static bool GfeItemProps_NotPocAndPdByLender(int props)
        {
            // 2/18/2010 db - This is equivalent to GfeItemProps_Poc(props) && GfeItemProps_Payer(props) >= 3
            // 7/29/2010 vm - Method was returning true even when the item was paid by the broker.
            //                So, made a change to have it return false in that scenario.
            return !GfeItemProps_Poc(props) && GfeItemProps_PdByLenderOnly(props);
        }

        public static bool GfeItemProps_PdByLenderOnly(int props)
        {
            // 6/21/2010 fs - This is equivalent to GfeItemProps_Payer(props) == 3
            bool paidByLender = (((props & 0x0F00) >> 8) == 3);

            return paidByLender;
        }

        public static bool GfeItemProps_PdBySellerOnly(int props)
        {
            // 6/21/2010 fs - This is equivalent to GfeItemProps_Payer(props) == 2
            bool paidByLender = (((props & 0x0F00) >> 8) == SELLER_PAID);

            return paidByLender;
        }

        public static bool GfeItemProps_Dflp(int props)
        {
            return (props & 0x2000) != 0;
        }

        public static int GfeItemProps_UpdateDflp(int props, bool isDflp)
        {
            int mask = ~0x2000;
            var result = props & mask;
            int newDflp = isDflp ? 0x2000 : 0;
            result |= newDflp;
            return result;
        }

        public static bool GfeItemProps_BF(int props)
        {
            return (props & 0x0008) != 0;
        }

        public static int GfeItemProps_UpdateBF(int props, bool isBF)
        {
            int mask = ~0x0008;
            var result = props & mask;
            int newBF = isBF ? 0x0008 : 0;
            result |= newBF;
            return result;
        }

        public static bool GfeItemProps_GBF(int props)
        {
            return (props & 0x0004) != 0;
        }

        public static int ConvertToLegacyFeeProps(BorrowerClosingCostFee fee)
        {
            if (fee == null)
            {
                throw CBaseException.GenericException("fee cannot be null");
            }

            var firstPayment = fee.GetPaymentsWithCachedTotal(0).First();

            bool apr = fee.IsApr;
            bool toBr = fee.Beneficiary == E_AgentRoleT.Broker;
            bool fhaAllow = fee.IsFhaAllowable;
            bool poc = firstPayment.GfeClosingCostFeePaymentTimingT == E_GfeClosingCostFeePaymentTimingT.OutsideOfClosing;
            bool borr = fee.DidShop;
            bool trdPty = fee.IsThirdParty;
            bool aff = fee.IsAffiliate;
            bool qmWarn = false; // ????
            int payer = 0; // 0 - borr pd, 1 - borr fin, 2 - seller, 3 - lender, 4 -broker
            bool dflp = fee.Dflp;
            bool gbf = false;
            bool bf = fee.IsBonaFide;
            switch (firstPayment.PaidByT)
            {
                case E_ClosingCostFeePaymentPaidByT.LeaveBlank:
                    payer = 0;
                    break;
                case E_ClosingCostFeePaymentPaidByT.Borrower:
                    payer = 0;
                    break;
                case E_ClosingCostFeePaymentPaidByT.Seller:
                    payer = 2;
                    break;
                case E_ClosingCostFeePaymentPaidByT.BorrowerFinance:
                    payer = 1;
                    break;
                case E_ClosingCostFeePaymentPaidByT.Lender:
                    payer = 3;
                    break;
                case E_ClosingCostFeePaymentPaidByT.Broker:
                    payer = 4;
                    break;
                case E_ClosingCostFeePaymentPaidByT.Other:
                    payer = 0;
                    break;
                default:
                    throw new UnhandledEnumException(firstPayment.PaidByT);
            }

            return GfeItemProps_Pack(apr, toBr, payer, fhaAllow, poc, dflp, gbf, bf, borr, trdPty, aff, qmWarn);

        }

        /// <summary>
        /// Set the GFE legacy fee props to the new structure.
        /// </summary>
        /// <param name="fee"></param>
        /// <param name="props"></param>
        public static void SetFeeWithLegacyFeeProps(BorrowerClosingCostFee fee, int props)
        {
            if (fee == null)
            {
                throw CBaseException.GenericException("fee cannot be null");
            }

            // OPM 209868 - Don't update props if value hasn't changed.
            if (props == ConvertToLegacyFeeProps(fee))
            {
                return; // NO-OP
            }

            var firstPayment = fee.Payments.First();

            fee.IsApr = GfeItemProps_Apr(props);
            if (GfeItemProps_ToBr(props) && fee.Beneficiary != E_AgentRoleT.Broker)
            {
                fee.Beneficiary = E_AgentRoleT.Broker;

                // Since we are copying values from the old GFE into the new GFE, we want to disable automation so we don't clobber any migrated values.
                // Note: Disable automation after Beneficiary is set, since setting Beneficiary re-enables automation.
                fee.DisableBeneficiaryAutomation = true;
                fee.ClearBeneficiary();
            }
            else if (!GfeItemProps_ToBr(props) && fee.Beneficiary == E_AgentRoleT.Broker)
            {
                // 1/8/2015 dd - If beneficiary is currently set to broker then switch to other.
                fee.Beneficiary = E_AgentRoleT.Other;

                // Since we are copying values from the old GFE into the new GFE, we want to disable automation so we don't clobber any migrated values.
                // Note: Disable automation after Beneficiary is set, since setting Beneficiary re-enables automation.
                fee.DisableBeneficiaryAutomation = true;
                fee.ClearBeneficiary();
            }

            fee.IsFhaAllowable = GfeItemProps_FhaAllow(props);

            if (GfeItemProps_Poc(props))
            {
                firstPayment.GfeClosingCostFeePaymentTimingT = E_GfeClosingCostFeePaymentTimingT.OutsideOfClosing;
            }
            else
            {
                firstPayment.GfeClosingCostFeePaymentTimingT = E_GfeClosingCostFeePaymentTimingT.AtClosing;
            }

            fee.DidShop = GfeItemProps_Borr(props);
            fee.IsThirdParty = GfeItemProps_PaidToThirdParty(props);

            fee.IsAffiliate = GfeItemProps_ThisPartyIsAffiliate(props);
            
            //bool qmWarn = false; // ????
            //bool dflp = false;
            //bool gbf = false;
            fee.IsBonaFide = GfeItemProps_BF(props);

            fee.Dflp = GfeItemProps_Dflp(props);

            int payer = GfeItemProps_Payer(props); // 0 - borr pd, 1 - borr fin, 2 - seller, 3 - lender, 4 -broker

            if (payer == 0)
            {
                firstPayment.PaidByT = E_ClosingCostFeePaymentPaidByT.Borrower;
            }
            else if (payer == 1)
            {
                firstPayment.PaidByT = E_ClosingCostFeePaymentPaidByT.BorrowerFinance;
            }
            else if (payer == 2)
            {
                firstPayment.PaidByT = E_ClosingCostFeePaymentPaidByT.Seller;
            }
            else if (payer == 3)
            {
                firstPayment.PaidByT = E_ClosingCostFeePaymentPaidByT.Lender;
            }
            else if (payer == 4)
            {
                firstPayment.PaidByT = E_ClosingCostFeePaymentPaidByT.Broker;
            }
            else
            {
                firstPayment.PaidByT = E_ClosingCostFeePaymentPaidByT.Other; 
            }

        }
        /// <summary>
        /// Props will have the following format
        /// 1st Bit - Is APR.
        /// 2nd Bit - Is POC.
        /// 3rd Bit - Is GBF.
        /// 4th Bit - Is BF.
        /// 5th Bit - To Broker.
        /// 13th Bit - FHA Allow.
        /// 9 - 12th Bit - Integer Payer Type.
        /// 14th Bit - Is deducted from loan proceeds.
        /// 
        /// </summary>
        /// <param name="apr"></param>
        /// <param name="toBr"></param>
        /// <param name="payer"></param>
        /// <param name="fhaAllow"></param>
        /// <param name="poc"></param>
        /// <param name="dlfp">Deducted from loan proceeds</param>
        /// <returns></returns>
        public static int GfeItemProps_Pack(bool apr, bool toBr, int payer, bool fhaAllow, bool poc, bool dflp, bool gbf, bool bf, bool borr, bool trdPty, bool aff, bool qmWarn)
        {
            // +------------------------------------------------------+
            // #                /---------\                           #
            // #  0 x      F    |    F    |    F         F            #
            // #  0 b   1 1 1 1 | 1 1 1 1 | 1 1 1 1   1 1 1 1         #
            // #        | | | | |  PAYER  |   | | |   | | | |         #
            // #        | | | | \---------/   | | |   | | | \- APR    #
            // #        | | | |               | | |   | | \--- Poc    #
            // #        | | | \-FhaAllow      | | |   | \----- GBF    #
            // #        | | \---DFLP          | | |   \------- BF     #
            // #        | \-----AFF           | | \----------- ToBr   #
            // #        \-------TrdPty        | \------------  Borr   #
            // #                              \--------------- QmWarn #
            // +------------------------------------------------------+
            if (payer < 0 || payer > 15)
            {
                throw new CBaseException(ErrorMessages.Generic, "Payer is out of range [Expect Payer to be 0-15]. Actual Payer is " + payer);
            }
            int result = 0;
            result |= Convert.ToInt32(apr); //  << 0;
            result |= Convert.ToInt32(poc)      << 1;
            result |= Convert.ToInt32(gbf)      << 2;
            result |= Convert.ToInt32(bf)       << 3;
            result |= Convert.ToInt32(toBr)     << 4;
            result |= Convert.ToInt16(borr)     << 5;
            result |= Convert.ToInt16(qmWarn)   << 6;
            // free 7
            result |= (payer & 0xF)             << 8;
            // payer 9
            // payer 10
            // payer 11
            result |= Convert.ToInt32(fhaAllow) << 12;
            result |= Convert.ToInt32(dflp)     << 13;
            result |= Convert.ToInt32(aff)      << 14;
            result |= Convert.ToInt32(trdPty)   << 15;
            
            return result;
        }

        public static int GfeItemProps_Pack(bool apr, bool toBr, int payer, bool fhaAllow, bool poc, bool dflp, bool gbf, bool bf, bool trdPty, bool aff, bool qmWarn)
        {
            return GfeItemProps_Pack(apr, toBr, payer, fhaAllow, poc, dflp, gbf, bf, false/*borr*/, trdPty, aff, qmWarn);
        }

        public static int GfeItemProps_Pack(bool apr, bool toBr, int payer, bool fhaAllow, bool poc, bool dflp, bool trdPty, bool aff, bool qmWarn)
        {
            return GfeItemProps_Pack(apr, toBr, payer, fhaAllow, poc, dflp, false /*gbf*/, false /*bf*/, trdPty, aff, qmWarn);
        }

        public static int GfeItemProps_Pack(bool apr, bool toBr, int payer, bool fhaAllow, bool poc)
        {
            return GfeItemProps_Pack(apr, toBr, payer, fhaAllow, poc, false /*dflp*/, false /*trdPty*/, false /*aff*/, false /*qmWarn*/);
        }

        public static int GfeItemProps_Copy(int other)
        {
            return other;
        }

        #endregion GfeItemProps

        #region EmployerIdentificationNumber

        public string ToEmployerTaxIdentificationNumberString(EmployerTaxIdentificationNumber? ein)
        {
            if (!ein.HasValue)
            {
                return string.Empty;
            }

            // Will be a string of 9 digits.
            var rawEin = ein.ToString();

            switch (m_formatTarget)
            {
                case FormatTarget.FannieMaeMornetPlus:
                case FormatTarget.FannieMaeMornetPlusImportantFields:
                    return rawEin;
                default:
                    return rawEin.Insert(2, "-");
            }
        }

        #endregion

        private void LogError(string srcValue, string srcType, string dstType, string callStack)
        {
            if (!string.IsNullOrEmpty(srcValue))
            {
                Tools.LogError("Data: Fails to convert " + srcValue + " of type " + srcType + " to type " + dstType + ". Call Stack:" + callStack);
            }
        }

        private void LogError(string msg)
        {
            if (!string.IsNullOrEmpty(msg))
            {
                Tools.LogError("Error msg: " + msg);
            }
        }
    }
}
