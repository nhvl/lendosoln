﻿namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using LendersOffice.Common;
    using LendingQB.Core.Data;

    using ConsumerIdentifier = LqbGrammar.DataTypes.DataObjectIdentifier<LqbGrammar.DataTypes.DataObjectKind.Consumer, System.Guid>;

    /// <summary>
    /// Defines the methods necessary for migrating housing history data on a loan file.
    /// </summary>
    public class HousingHistoryEntriesCollectionMigration
    {
        /// <summary>
        /// Migrates a loan to use the new <see cref="CPageData.HousingHistoryEntries"/> collection.
        /// </summary>
        /// <param name="loan">The loan to migrate.</param>
        /// <exception cref="CBaseException">The data could not be converted to the new format.</exception>
        internal static void MigrateLoan(CPageBase loan)
        {
            List<Tuple<ConsumerIdentifier, HousingHistoryEntry>> legacyHousingHistory = new List<Tuple<ConsumerIdentifier, HousingHistoryEntry>>(6 * loan.nApps);
            List<string> conversionErrors = new List<string>();
            foreach (Tuple<ConsumerIdentifier, LegacyAddressData> pair in loan.Apps.SelectMany(app => ExtractAddresses(app)).Where(pair => HasNonBlankValue(pair.Item2)))
            {
                HousingHistoryEntry entry = CreateHousingHistoryEntry(pair.Item2);
                legacyHousingHistory.Add(Tuple.Create(pair.Item1, entry));
                conversionErrors.AddIfNotNullOrEmpty(GetHousingHistoryEntryConversionErrorMessage(entry, pair.Item2));
            }

            List<Action> staticFieldUpdates = new List<Action>(4 * loan.nApps);
            foreach (LegacyAddressData staticAddressData in loan.Apps.SelectMany(app => ExtractStaticAddresses(app)).Where(addressData => HasNonBlankValue(addressData)))
            {
                PostalAddress address = CreatePostalAddress(staticAddressData);
                staticFieldUpdates.Add(() => staticAddressData.SetAddressToStaticField(address));
                conversionErrors.AddIfNotNullOrEmpty(GetHousingHistoryEntryConversionErrorMessage(new HousingHistoryEntry { Address = address }, staticAddressData));
            }

            if (conversionErrors.Count != 0)
            {
                throw new CBaseException(string.Join(" ", conversionErrors), "Unable to migrate loan");
            }

            loan.ClearHousingHistoryEntries(); // Existing housing history entries means an internal user put this loan back in an unmigrated state
            foreach (Tuple<ConsumerIdentifier, HousingHistoryEntry> pair in legacyHousingHistory)
            {
                loan.AddHousingHistoryEntry(pair.Item1, pair.Item2);
            }

            foreach (Action staticFieldUpdate in staticFieldUpdates)
            {
                staticFieldUpdate();
            }
        }

        /// <summary>
        /// Creates an instance of <see cref="HousingHistoryEntry"/> from the specified <see cref="LegacyAddressData"/>.
        /// </summary>
        /// <param name="addressData">The address data.</param>
        /// <returns>The housing history entry.</returns>
        private static HousingHistoryEntry CreateHousingHistoryEntry(LegacyAddressData addressData)
        {
            var housingHistoryEntry = new HousingHistoryEntry
            {
                IsPresentAddress = addressData.IsPresentAddress,
            };
            HousingHistoryFieldShim.SetStreetAddress(housingHistoryEntry, addressData.StreetAddress);
            HousingHistoryFieldShim.SetCity(housingHistoryEntry, addressData.City);
            HousingHistoryFieldShim.SetState(housingHistoryEntry, addressData.State);
            HousingHistoryFieldShim.SetZipcode(housingHistoryEntry, addressData.Zipcode);
            HousingHistoryFieldShim.SetAddressType(housingHistoryEntry, addressData.AddressType);
            HousingHistoryFieldShim.SetYearsAtAddress(housingHistoryEntry, addressData.YearsAtAddress);
            return housingHistoryEntry;
        }

        /// <summary>
        /// Creates an address instance from the legacy address data.
        /// </summary>
        /// <param name="addressData">The legacy address data.</param>
        /// <returns>The address instance.</returns>
        private static PostalAddress CreatePostalAddress(LegacyAddressData addressData)
        {
            PostalAddress address = null;
            address = HousingHistoryFieldShim.UpdateStreetAddress(address, addressData.StreetAddress);
            address = HousingHistoryFieldShim.UpdateCity(address, addressData.City);
            address = HousingHistoryFieldShim.UpdateState(address, addressData.State);
            address = HousingHistoryFieldShim.UpdateZipcode(address, addressData.Zipcode);
            return address;
        }

        /// <summary>
        /// Gets an error message for how <see cref="addressData"/> was converted into <see cref="entry"/>.
        /// </summary>
        /// <param name="entry">The housing history entry.</param>
        /// <param name="addressData">The legacy address data.</param>
        /// <returns>The error message string for the conversion, or null if there were no issues.</returns>
        private static string GetHousingHistoryEntryConversionErrorMessage(HousingHistoryEntry entry, LegacyAddressData addressData)
        {
            List<string> fieldNames = new List<string>(6);
            if (HousingHistoryFieldShim.GetStreetAddress(entry) != addressData.StreetAddress)
            {
                fieldNames.Add("Street");
            }

            if (HousingHistoryFieldShim.GetCity(entry) != addressData.City)
            {
                fieldNames.Add("City");
            }

            if (HousingHistoryFieldShim.GetState(entry) != addressData.State)
            {
                fieldNames.Add("State");
            }

            if (HousingHistoryFieldShim.GetZipcode(entry) != addressData.Zipcode)
            {
                fieldNames.Add("Zipcode");
            }

            if (HousingHistoryFieldShim.GetAddressType(entry) != addressData.AddressType)
            {
                fieldNames.Add("Own/Rent");
            }

            // 2019-01 tj - This one is special, since aBAddr 2.3 converts to 28 months, which converts back to 2.33.
            // Thus, the only check we can really do is to make sure the new value isn't blank if the old value wasn't
            if (string.IsNullOrEmpty(HousingHistoryFieldShim.GetYearsAtAddress(entry)) && !string.IsNullOrEmpty(addressData.YearsAtAddress))
            {
                fieldNames.Add("Years at Address");
            }

            if (fieldNames.Count != 0)
            {
                return addressData.AddressName + " contained an invalid value for " + string.Join(", ", fieldNames) + ".";
            }

            return null;
        }

        /// <summary>
        /// Extracts a list of all legacy address items from a legacy application.
        /// </summary>
        /// <param name="app">The legacy application.</param>
        /// <returns>The list of legacy address items.</returns>
        private static List<Tuple<ConsumerIdentifier, LegacyAddressData>> ExtractAddresses(CAppBase app)
        {
            string spaceBName = !string.IsNullOrWhiteSpace(app.aBNm) ? " " + app.aBNm : null;
            string spaceCName = !string.IsNullOrWhiteSpace(app.aCNm) ? " " + app.aCNm : null;
            return new List<Tuple<ConsumerIdentifier, LegacyAddressData>>(6)
            {
                Tuple.Create(
                    app.aBConsumerIdentifier,
                    new LegacyAddressData
                    {
                        AddressName = "Present Address for borrower" + spaceBName,
                        IsPresentAddress = true,
                        StreetAddress = app.aBAddr,
                        City = app.aBCity,
                        State = app.aBState,
                        Zipcode = app.aBZip,
                        AddressType = app.aBAddrT,
                        YearsAtAddress = app.aBAddrYrs,
                    }),
                Tuple.Create(
                    app.aBConsumerIdentifier,
                    new LegacyAddressData
                    {
                        AddressName = "Former Address #1 for borrower" + spaceBName,
                        IsPresentAddress = false,
                        StreetAddress = app.aBPrev1Addr,
                        City = app.aBPrev1City,
                        State = app.aBPrev1State,
                        Zipcode = app.aBPrev1Zip,
                        AddressType = (E_aBAddrT)app.aBPrev1AddrT,
                        YearsAtAddress = app.aBPrev1AddrYrs,
                    }),
                Tuple.Create(
                    app.aBConsumerIdentifier,
                    new LegacyAddressData
                    {
                        AddressName = "Former Address #2 for borrower" + spaceBName,
                        IsPresentAddress = false,
                        StreetAddress = app.aBPrev2Addr,
                        City = app.aBPrev2City,
                        State = app.aBPrev2State,
                        Zipcode = app.aBPrev2Zip,
                        AddressType = (E_aBAddrT)app.aBPrev2AddrT,
                        YearsAtAddress = app.aBPrev2AddrYrs,
                    }),
                Tuple.Create(
                    app.aCConsumerIdentifier,
                    new LegacyAddressData
                    {
                        AddressName = "Present Address for co-borrower" + spaceCName,
                        IsPresentAddress = true,
                        StreetAddress = app.aCAddr,
                        City = app.aCCity,
                        State = app.aCState,
                        Zipcode = app.aCZip,
                        AddressType = (E_aBAddrT)app.aCAddrT,
                        YearsAtAddress = app.aCAddrYrs,
                    }),
                Tuple.Create(
                    app.aCConsumerIdentifier,
                    new LegacyAddressData
                    {
                        AddressName = "Former Address #1 for co-borrower" + spaceCName,
                        IsPresentAddress = false,
                        StreetAddress = app.aCPrev1Addr,
                        City = app.aCPrev1City,
                        State = app.aCPrev1State,
                        Zipcode = app.aCPrev1Zip,
                        AddressType = (E_aBAddrT)app.aCPrev1AddrT,
                        YearsAtAddress = app.aCPrev1AddrYrs,
                    }),
                Tuple.Create(
                    app.aCConsumerIdentifier,
                    new LegacyAddressData
                    {
                        AddressName = "Former Address #2 for co-borrower" + spaceCName,
                        IsPresentAddress = false,
                        StreetAddress = app.aCPrev2Addr,
                        City = app.aCPrev2City,
                        State = app.aCPrev2State,
                        Zipcode = app.aCPrev2Zip,
                        AddressType = (E_aBAddrT)app.aCPrev2AddrT,
                        YearsAtAddress = app.aCPrev2AddrYrs,
                    }),
            };
        }

        /// <summary>
        /// Extracts a list of all the static legacy address items we want to update from a legacy application.
        /// </summary>
        /// <param name="app">The legacy application.</param>
        /// <returns>The list of legacy address items.</returns>
        private static List<LegacyAddressData> ExtractStaticAddresses(CAppBase app)
        {
            string spaceBName = !string.IsNullOrWhiteSpace(app.aBNm) ? " " + app.aBNm : null;
            string spaceCName = !string.IsNullOrWhiteSpace(app.aCNm) ? " " + app.aCNm : null;
            return new List<LegacyAddressData>(4)
            {
                new LegacyAddressData
                {
                    AddressName = "Mailing Address for borrower" + spaceBName,
                    StreetAddress = app.aBAddrMail,
                    City = app.aBCityMail,
                    State = app.aBStateMail,
                    Zipcode = app.aBZipMail,
                    SetAddressToStaticField = a => app.aBMailingAddressData = a,
                },
                new LegacyAddressData
                {
                    AddressName = "Mailing Address for co-borrower" + spaceCName,
                    StreetAddress = app.aCAddrMail,
                    City = app.aCCityMail,
                    State = app.aCStateMail,
                    Zipcode = app.aCZipMail,
                    SetAddressToStaticField = a => app.aCMailingAddressData = a,
                },
                new LegacyAddressData
                {
                    AddressName = "Address After Closing for borrower" + spaceBName,
                    StreetAddress = app.aBAddrPost,
                    City = app.aBCityPost,
                    State = app.aBStatePost,
                    Zipcode = app.aBZipPost,
                    SetAddressToStaticField = a => app.aBPostClosingAddressData = a,
                },
                new LegacyAddressData
                {
                    AddressName = "Address After Closing for co-borrower" + spaceCName,
                    StreetAddress = app.aCAddrPost,
                    City = app.aCCityPost,
                    State = app.aCStatePost,
                    Zipcode = app.aCZipPost,
                    SetAddressToStaticField = a => app.aCPostClosingAddressData = a,
                },
            };
        }

        /// <summary>
        /// Returns true if the address is not entirely blank.
        /// </summary>
        /// <param name="addressData">The address information to check.</param>
        /// <returns><see langword="true"/> if there is a non-blank/default value on <paramref name="addressData"/>, <see langword="false"/> otherwise.</returns>
        private static bool HasNonBlankValue(LegacyAddressData addressData)
        {
            return !string.IsNullOrWhiteSpace(addressData.StreetAddress)
                || !string.IsNullOrWhiteSpace(addressData.City)
                || !string.IsNullOrWhiteSpace(addressData.State)
                || !string.IsNullOrWhiteSpace(addressData.Zipcode)
                || addressData.AddressType != E_aBAddrT.LeaveBlank
                || !string.IsNullOrWhiteSpace(addressData.YearsAtAddress);
        }

        /// <summary>
        /// Stores address data from the legacy fields.
        /// </summary>
        private class LegacyAddressData
        {
            /// <summary>
            /// Gets or sets a human readable name for the address for error messages.
            /// </summary>
            public string AddressName { get; set; }

            /// <summary>
            /// Gets or sets a value indicating whether the address is a consumer's present address.
            /// </summary>
            public bool IsPresentAddress { get; set; }

            /// <summary>
            /// Gets or sets the street address component of the address.
            /// </summary>
            public string StreetAddress { get; set; }

            /// <summary>
            /// Gets or sets the city component of the address.
            /// </summary>
            public string City { get; set; }

            /// <summary>
            /// Gets or sets the state component of the address.
            /// </summary>
            public string State { get; set; }

            /// <summary>
            /// Gets or sets the zipcode component of the address.
            /// </summary>
            public string Zipcode { get; set; }

            /// <summary>
            /// Gets or sets the address type.
            /// </summary>
            public E_aBAddrT AddressType { get; set; } = E_aBAddrT.LeaveBlank;

            /// <summary>
            /// Gets or sets the string representation of the years at the address.
            /// </summary>
            public string YearsAtAddress { get; set; }

            /// <summary>
            /// Gets or sets an action to set an address value to a static field.
            /// </summary>
            public Action<PostalAddress> SetAddressToStaticField { get; set; }
        }
    }
}
